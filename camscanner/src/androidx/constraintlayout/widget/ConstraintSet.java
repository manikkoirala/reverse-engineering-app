// 
// Decompiled by Procyon v0.6.0
// 

package androidx.constraintlayout.widget;

import android.annotation.SuppressLint;
import java.io.Writer;
import android.graphics.Color;
import java.util.Locale;
import android.content.res.XmlResourceParser;
import org.xmlpull.v1.XmlPullParserException;
import java.io.IOException;
import java.io.PrintStream;
import java.util.Set;
import androidx.constraintlayout.motion.widget.MotionScene;
import android.view.ViewGroup;
import android.view.LayoutInflater;
import android.view.ViewGroup$LayoutParams;
import java.util.Collection;
import java.util.HashSet;
import androidx.constraintlayout.core.widgets.HelperWidget;
import android.util.SparseArray;
import androidx.constraintlayout.core.widgets.ConstraintWidget;
import java.util.Iterator;
import java.util.Map;
import java.util.ArrayList;
import androidx.constraintlayout.motion.widget.MotionLayout;
import androidx.constraintlayout.core.motion.utils.Easing;
import androidx.constraintlayout.motion.widget.Debug;
import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.util.Arrays;
import android.view.View;
import android.util.AttributeSet;
import android.util.Xml;
import org.xmlpull.v1.XmlPullParser;
import android.content.Context;
import android.content.res.TypedArray;
import java.util.HashMap;
import android.util.SparseIntArray;

public class ConstraintSet
{
    private static final int ALPHA = 43;
    private static final int ANIMATE_CIRCLE_ANGLE_TO = 82;
    private static final int ANIMATE_RELATIVE_TO = 64;
    private static final int BARRIER_ALLOWS_GONE_WIDGETS = 75;
    private static final int BARRIER_DIRECTION = 72;
    private static final int BARRIER_MARGIN = 73;
    private static final int BARRIER_TYPE = 1;
    public static final int BASELINE = 5;
    private static final int BASELINE_MARGIN = 93;
    private static final int BASELINE_TO_BASELINE = 1;
    private static final int BASELINE_TO_BOTTOM = 92;
    private static final int BASELINE_TO_TOP = 91;
    public static final int BOTTOM = 4;
    private static final int BOTTOM_MARGIN = 2;
    private static final int BOTTOM_TO_BOTTOM = 3;
    private static final int BOTTOM_TO_TOP = 4;
    public static final int CHAIN_PACKED = 2;
    public static final int CHAIN_SPREAD = 0;
    public static final int CHAIN_SPREAD_INSIDE = 1;
    private static final int CHAIN_USE_RTL = 71;
    private static final int CIRCLE = 61;
    private static final int CIRCLE_ANGLE = 63;
    private static final int CIRCLE_RADIUS = 62;
    public static final int CIRCLE_REFERENCE = 8;
    private static final int CONSTRAINED_HEIGHT = 81;
    private static final int CONSTRAINED_WIDTH = 80;
    private static final int CONSTRAINT_REFERENCED_IDS = 74;
    private static final int CONSTRAINT_TAG = 77;
    private static final boolean DEBUG = false;
    private static final int DIMENSION_RATIO = 5;
    private static final int DRAW_PATH = 66;
    private static final int EDITOR_ABSOLUTE_X = 6;
    private static final int EDITOR_ABSOLUTE_Y = 7;
    private static final int ELEVATION = 44;
    public static final int END = 7;
    private static final int END_MARGIN = 8;
    private static final int END_TO_END = 9;
    private static final int END_TO_START = 10;
    private static final String ERROR_MESSAGE = "XML parser error must be within a Constraint ";
    public static final int GONE = 8;
    private static final int GONE_BASELINE_MARGIN = 94;
    private static final int GONE_BOTTOM_MARGIN = 11;
    private static final int GONE_END_MARGIN = 12;
    private static final int GONE_LEFT_MARGIN = 13;
    private static final int GONE_RIGHT_MARGIN = 14;
    private static final int GONE_START_MARGIN = 15;
    private static final int GONE_TOP_MARGIN = 16;
    private static final int GUIDE_BEGIN = 17;
    private static final int GUIDE_END = 18;
    private static final int GUIDE_PERCENT = 19;
    private static final int HEIGHT_DEFAULT = 55;
    private static final int HEIGHT_MAX = 57;
    private static final int HEIGHT_MIN = 59;
    private static final int HEIGHT_PERCENT = 70;
    public static final int HORIZONTAL = 0;
    private static final int HORIZONTAL_BIAS = 20;
    public static final int HORIZONTAL_GUIDELINE = 0;
    private static final int HORIZONTAL_STYLE = 41;
    private static final int HORIZONTAL_WEIGHT = 39;
    private static final int INTERNAL_MATCH_CONSTRAINT = -3;
    private static final int INTERNAL_MATCH_PARENT = -1;
    private static final int INTERNAL_WRAP_CONTENT = -2;
    private static final int INTERNAL_WRAP_CONTENT_CONSTRAINED = -4;
    public static final int INVISIBLE = 4;
    private static final String KEY_PERCENT_PARENT = "parent";
    private static final String KEY_RATIO = "ratio";
    private static final String KEY_WEIGHT = "weight";
    private static final int LAYOUT_CONSTRAINT_HEIGHT = 96;
    private static final int LAYOUT_CONSTRAINT_WIDTH = 95;
    private static final int LAYOUT_HEIGHT = 21;
    private static final int LAYOUT_VISIBILITY = 22;
    private static final int LAYOUT_WIDTH = 23;
    private static final int LAYOUT_WRAP_BEHAVIOR = 97;
    public static final int LEFT = 1;
    private static final int LEFT_MARGIN = 24;
    private static final int LEFT_TO_LEFT = 25;
    private static final int LEFT_TO_RIGHT = 26;
    public static final int MATCH_CONSTRAINT = 0;
    public static final int MATCH_CONSTRAINT_PERCENT = 2;
    public static final int MATCH_CONSTRAINT_SPREAD = 0;
    public static final int MATCH_CONSTRAINT_WRAP = 1;
    private static final int MOTION_STAGGER = 79;
    private static final int MOTION_TARGET = 98;
    private static final int ORIENTATION = 27;
    public static final int PARENT_ID = 0;
    private static final int PATH_MOTION_ARC = 76;
    private static final int PROGRESS = 68;
    private static final int QUANTIZE_MOTION_INTERPOLATOR = 86;
    private static final int QUANTIZE_MOTION_INTERPOLATOR_ID = 89;
    private static final int QUANTIZE_MOTION_INTERPOLATOR_STR = 90;
    private static final int QUANTIZE_MOTION_INTERPOLATOR_TYPE = 88;
    private static final int QUANTIZE_MOTION_PHASE = 85;
    private static final int QUANTIZE_MOTION_STEPS = 84;
    public static final int RIGHT = 2;
    private static final int RIGHT_MARGIN = 28;
    private static final int RIGHT_TO_LEFT = 29;
    private static final int RIGHT_TO_RIGHT = 30;
    public static final int ROTATE_LEFT_OF_PORTRATE = 4;
    public static final int ROTATE_NONE = 0;
    public static final int ROTATE_PORTRATE_OF_LEFT = 2;
    public static final int ROTATE_PORTRATE_OF_RIGHT = 1;
    public static final int ROTATE_RIGHT_OF_PORTRATE = 3;
    private static final int ROTATION = 60;
    private static final int ROTATION_X = 45;
    private static final int ROTATION_Y = 46;
    private static final int SCALE_X = 47;
    private static final int SCALE_Y = 48;
    public static final int START = 6;
    private static final int START_MARGIN = 31;
    private static final int START_TO_END = 32;
    private static final int START_TO_START = 33;
    private static final String TAG = "ConstraintSet";
    public static final int TOP = 3;
    private static final int TOP_MARGIN = 34;
    private static final int TOP_TO_BOTTOM = 35;
    private static final int TOP_TO_TOP = 36;
    private static final int TRANSFORM_PIVOT_TARGET = 83;
    private static final int TRANSFORM_PIVOT_X = 49;
    private static final int TRANSFORM_PIVOT_Y = 50;
    private static final int TRANSITION_EASING = 65;
    private static final int TRANSITION_PATH_ROTATE = 67;
    private static final int TRANSLATION_X = 51;
    private static final int TRANSLATION_Y = 52;
    private static final int TRANSLATION_Z = 53;
    public static final int UNSET = -1;
    private static final int UNUSED = 87;
    public static final int VERTICAL = 1;
    private static final int VERTICAL_BIAS = 37;
    public static final int VERTICAL_GUIDELINE = 1;
    private static final int VERTICAL_STYLE = 42;
    private static final int VERTICAL_WEIGHT = 40;
    private static final int VIEW_ID = 38;
    private static final int[] VISIBILITY_FLAGS;
    private static final int VISIBILITY_MODE = 78;
    public static final int VISIBILITY_MODE_IGNORE = 1;
    public static final int VISIBILITY_MODE_NORMAL = 0;
    public static final int VISIBLE = 0;
    private static final int WIDTH_DEFAULT = 54;
    private static final int WIDTH_MAX = 56;
    private static final int WIDTH_MIN = 58;
    private static final int WIDTH_PERCENT = 69;
    public static final int WRAP_CONTENT = -2;
    private static SparseIntArray mapToConstant;
    private static SparseIntArray overrideMapToConstant;
    public String derivedState;
    private HashMap<Integer, Constraint> mConstraints;
    private boolean mForceId;
    public String mIdString;
    public int mRotate;
    private HashMap<String, ConstraintAttribute> mSavedAttributes;
    private boolean mValidate;
    
    static {
        VISIBILITY_FLAGS = new int[] { 0, 4, 8 };
        ConstraintSet.mapToConstant = new SparseIntArray();
        ConstraintSet.overrideMapToConstant = new SparseIntArray();
        ConstraintSet.mapToConstant.append(R.styleable.Constraint_layout_constraintLeft_toLeftOf, 25);
        ConstraintSet.mapToConstant.append(R.styleable.Constraint_layout_constraintLeft_toRightOf, 26);
        ConstraintSet.mapToConstant.append(R.styleable.Constraint_layout_constraintRight_toLeftOf, 29);
        ConstraintSet.mapToConstant.append(R.styleable.Constraint_layout_constraintRight_toRightOf, 30);
        ConstraintSet.mapToConstant.append(R.styleable.Constraint_layout_constraintTop_toTopOf, 36);
        ConstraintSet.mapToConstant.append(R.styleable.Constraint_layout_constraintTop_toBottomOf, 35);
        ConstraintSet.mapToConstant.append(R.styleable.Constraint_layout_constraintBottom_toTopOf, 4);
        ConstraintSet.mapToConstant.append(R.styleable.Constraint_layout_constraintBottom_toBottomOf, 3);
        ConstraintSet.mapToConstant.append(R.styleable.Constraint_layout_constraintBaseline_toBaselineOf, 1);
        ConstraintSet.mapToConstant.append(R.styleable.Constraint_layout_constraintBaseline_toTopOf, 91);
        ConstraintSet.mapToConstant.append(R.styleable.Constraint_layout_constraintBaseline_toBottomOf, 92);
        ConstraintSet.mapToConstant.append(R.styleable.Constraint_layout_editor_absoluteX, 6);
        ConstraintSet.mapToConstant.append(R.styleable.Constraint_layout_editor_absoluteY, 7);
        ConstraintSet.mapToConstant.append(R.styleable.Constraint_layout_constraintGuide_begin, 17);
        ConstraintSet.mapToConstant.append(R.styleable.Constraint_layout_constraintGuide_end, 18);
        ConstraintSet.mapToConstant.append(R.styleable.Constraint_layout_constraintGuide_percent, 19);
        ConstraintSet.mapToConstant.append(R.styleable.Constraint_android_orientation, 27);
        ConstraintSet.mapToConstant.append(R.styleable.Constraint_layout_constraintStart_toEndOf, 32);
        ConstraintSet.mapToConstant.append(R.styleable.Constraint_layout_constraintStart_toStartOf, 33);
        ConstraintSet.mapToConstant.append(R.styleable.Constraint_layout_constraintEnd_toStartOf, 10);
        ConstraintSet.mapToConstant.append(R.styleable.Constraint_layout_constraintEnd_toEndOf, 9);
        ConstraintSet.mapToConstant.append(R.styleable.Constraint_layout_goneMarginLeft, 13);
        ConstraintSet.mapToConstant.append(R.styleable.Constraint_layout_goneMarginTop, 16);
        ConstraintSet.mapToConstant.append(R.styleable.Constraint_layout_goneMarginRight, 14);
        ConstraintSet.mapToConstant.append(R.styleable.Constraint_layout_goneMarginBottom, 11);
        ConstraintSet.mapToConstant.append(R.styleable.Constraint_layout_goneMarginStart, 15);
        ConstraintSet.mapToConstant.append(R.styleable.Constraint_layout_goneMarginEnd, 12);
        ConstraintSet.mapToConstant.append(R.styleable.Constraint_layout_constraintVertical_weight, 40);
        ConstraintSet.mapToConstant.append(R.styleable.Constraint_layout_constraintHorizontal_weight, 39);
        ConstraintSet.mapToConstant.append(R.styleable.Constraint_layout_constraintHorizontal_chainStyle, 41);
        ConstraintSet.mapToConstant.append(R.styleable.Constraint_layout_constraintVertical_chainStyle, 42);
        ConstraintSet.mapToConstant.append(R.styleable.Constraint_layout_constraintHorizontal_bias, 20);
        ConstraintSet.mapToConstant.append(R.styleable.Constraint_layout_constraintVertical_bias, 37);
        ConstraintSet.mapToConstant.append(R.styleable.Constraint_layout_constraintDimensionRatio, 5);
        ConstraintSet.mapToConstant.append(R.styleable.Constraint_layout_constraintLeft_creator, 87);
        ConstraintSet.mapToConstant.append(R.styleable.Constraint_layout_constraintTop_creator, 87);
        ConstraintSet.mapToConstant.append(R.styleable.Constraint_layout_constraintRight_creator, 87);
        ConstraintSet.mapToConstant.append(R.styleable.Constraint_layout_constraintBottom_creator, 87);
        ConstraintSet.mapToConstant.append(R.styleable.Constraint_layout_constraintBaseline_creator, 87);
        ConstraintSet.mapToConstant.append(R.styleable.Constraint_android_layout_marginLeft, 24);
        ConstraintSet.mapToConstant.append(R.styleable.Constraint_android_layout_marginRight, 28);
        ConstraintSet.mapToConstant.append(R.styleable.Constraint_android_layout_marginStart, 31);
        ConstraintSet.mapToConstant.append(R.styleable.Constraint_android_layout_marginEnd, 8);
        ConstraintSet.mapToConstant.append(R.styleable.Constraint_android_layout_marginTop, 34);
        ConstraintSet.mapToConstant.append(R.styleable.Constraint_android_layout_marginBottom, 2);
        ConstraintSet.mapToConstant.append(R.styleable.Constraint_android_layout_width, 23);
        ConstraintSet.mapToConstant.append(R.styleable.Constraint_android_layout_height, 21);
        ConstraintSet.mapToConstant.append(R.styleable.Constraint_layout_constraintWidth, 95);
        ConstraintSet.mapToConstant.append(R.styleable.Constraint_layout_constraintHeight, 96);
        ConstraintSet.mapToConstant.append(R.styleable.Constraint_android_visibility, 22);
        ConstraintSet.mapToConstant.append(R.styleable.Constraint_android_alpha, 43);
        ConstraintSet.mapToConstant.append(R.styleable.Constraint_android_elevation, 44);
        ConstraintSet.mapToConstant.append(R.styleable.Constraint_android_rotationX, 45);
        ConstraintSet.mapToConstant.append(R.styleable.Constraint_android_rotationY, 46);
        ConstraintSet.mapToConstant.append(R.styleable.Constraint_android_rotation, 60);
        ConstraintSet.mapToConstant.append(R.styleable.Constraint_android_scaleX, 47);
        ConstraintSet.mapToConstant.append(R.styleable.Constraint_android_scaleY, 48);
        ConstraintSet.mapToConstant.append(R.styleable.Constraint_android_transformPivotX, 49);
        ConstraintSet.mapToConstant.append(R.styleable.Constraint_android_transformPivotY, 50);
        ConstraintSet.mapToConstant.append(R.styleable.Constraint_android_translationX, 51);
        ConstraintSet.mapToConstant.append(R.styleable.Constraint_android_translationY, 52);
        ConstraintSet.mapToConstant.append(R.styleable.Constraint_android_translationZ, 53);
        ConstraintSet.mapToConstant.append(R.styleable.Constraint_layout_constraintWidth_default, 54);
        ConstraintSet.mapToConstant.append(R.styleable.Constraint_layout_constraintHeight_default, 55);
        ConstraintSet.mapToConstant.append(R.styleable.Constraint_layout_constraintWidth_max, 56);
        ConstraintSet.mapToConstant.append(R.styleable.Constraint_layout_constraintHeight_max, 57);
        ConstraintSet.mapToConstant.append(R.styleable.Constraint_layout_constraintWidth_min, 58);
        ConstraintSet.mapToConstant.append(R.styleable.Constraint_layout_constraintHeight_min, 59);
        ConstraintSet.mapToConstant.append(R.styleable.Constraint_layout_constraintCircle, 61);
        ConstraintSet.mapToConstant.append(R.styleable.Constraint_layout_constraintCircleRadius, 62);
        ConstraintSet.mapToConstant.append(R.styleable.Constraint_layout_constraintCircleAngle, 63);
        ConstraintSet.mapToConstant.append(R.styleable.Constraint_animateRelativeTo, 64);
        ConstraintSet.mapToConstant.append(R.styleable.Constraint_transitionEasing, 65);
        ConstraintSet.mapToConstant.append(R.styleable.Constraint_drawPath, 66);
        ConstraintSet.mapToConstant.append(R.styleable.Constraint_transitionPathRotate, 67);
        ConstraintSet.mapToConstant.append(R.styleable.Constraint_motionStagger, 79);
        ConstraintSet.mapToConstant.append(R.styleable.Constraint_android_id, 38);
        ConstraintSet.mapToConstant.append(R.styleable.Constraint_motionProgress, 68);
        ConstraintSet.mapToConstant.append(R.styleable.Constraint_layout_constraintWidth_percent, 69);
        ConstraintSet.mapToConstant.append(R.styleable.Constraint_layout_constraintHeight_percent, 70);
        ConstraintSet.mapToConstant.append(R.styleable.Constraint_layout_wrapBehaviorInParent, 97);
        ConstraintSet.mapToConstant.append(R.styleable.Constraint_chainUseRtl, 71);
        ConstraintSet.mapToConstant.append(R.styleable.Constraint_barrierDirection, 72);
        ConstraintSet.mapToConstant.append(R.styleable.Constraint_barrierMargin, 73);
        ConstraintSet.mapToConstant.append(R.styleable.Constraint_constraint_referenced_ids, 74);
        ConstraintSet.mapToConstant.append(R.styleable.Constraint_barrierAllowsGoneWidgets, 75);
        ConstraintSet.mapToConstant.append(R.styleable.Constraint_pathMotionArc, 76);
        ConstraintSet.mapToConstant.append(R.styleable.Constraint_layout_constraintTag, 77);
        ConstraintSet.mapToConstant.append(R.styleable.Constraint_visibilityMode, 78);
        ConstraintSet.mapToConstant.append(R.styleable.Constraint_layout_constrainedWidth, 80);
        ConstraintSet.mapToConstant.append(R.styleable.Constraint_layout_constrainedHeight, 81);
        ConstraintSet.mapToConstant.append(R.styleable.Constraint_polarRelativeTo, 82);
        ConstraintSet.mapToConstant.append(R.styleable.Constraint_transformPivotTarget, 83);
        ConstraintSet.mapToConstant.append(R.styleable.Constraint_quantizeMotionSteps, 84);
        ConstraintSet.mapToConstant.append(R.styleable.Constraint_quantizeMotionPhase, 85);
        ConstraintSet.mapToConstant.append(R.styleable.Constraint_quantizeMotionInterpolator, 86);
        final SparseIntArray overrideMapToConstant = ConstraintSet.overrideMapToConstant;
        final int constraintOverride_layout_editor_absoluteY = R.styleable.ConstraintOverride_layout_editor_absoluteY;
        overrideMapToConstant.append(constraintOverride_layout_editor_absoluteY, 6);
        ConstraintSet.overrideMapToConstant.append(constraintOverride_layout_editor_absoluteY, 7);
        ConstraintSet.overrideMapToConstant.append(R.styleable.ConstraintOverride_android_orientation, 27);
        ConstraintSet.overrideMapToConstant.append(R.styleable.ConstraintOverride_layout_goneMarginLeft, 13);
        ConstraintSet.overrideMapToConstant.append(R.styleable.ConstraintOverride_layout_goneMarginTop, 16);
        ConstraintSet.overrideMapToConstant.append(R.styleable.ConstraintOverride_layout_goneMarginRight, 14);
        ConstraintSet.overrideMapToConstant.append(R.styleable.ConstraintOverride_layout_goneMarginBottom, 11);
        ConstraintSet.overrideMapToConstant.append(R.styleable.ConstraintOverride_layout_goneMarginStart, 15);
        ConstraintSet.overrideMapToConstant.append(R.styleable.ConstraintOverride_layout_goneMarginEnd, 12);
        ConstraintSet.overrideMapToConstant.append(R.styleable.ConstraintOverride_layout_constraintVertical_weight, 40);
        ConstraintSet.overrideMapToConstant.append(R.styleable.ConstraintOverride_layout_constraintHorizontal_weight, 39);
        ConstraintSet.overrideMapToConstant.append(R.styleable.ConstraintOverride_layout_constraintHorizontal_chainStyle, 41);
        ConstraintSet.overrideMapToConstant.append(R.styleable.ConstraintOverride_layout_constraintVertical_chainStyle, 42);
        ConstraintSet.overrideMapToConstant.append(R.styleable.ConstraintOverride_layout_constraintHorizontal_bias, 20);
        ConstraintSet.overrideMapToConstant.append(R.styleable.ConstraintOverride_layout_constraintVertical_bias, 37);
        ConstraintSet.overrideMapToConstant.append(R.styleable.ConstraintOverride_layout_constraintDimensionRatio, 5);
        ConstraintSet.overrideMapToConstant.append(R.styleable.ConstraintOverride_layout_constraintLeft_creator, 87);
        ConstraintSet.overrideMapToConstant.append(R.styleable.ConstraintOverride_layout_constraintTop_creator, 87);
        ConstraintSet.overrideMapToConstant.append(R.styleable.ConstraintOverride_layout_constraintRight_creator, 87);
        ConstraintSet.overrideMapToConstant.append(R.styleable.ConstraintOverride_layout_constraintBottom_creator, 87);
        ConstraintSet.overrideMapToConstant.append(R.styleable.ConstraintOverride_layout_constraintBaseline_creator, 87);
        ConstraintSet.overrideMapToConstant.append(R.styleable.ConstraintOverride_android_layout_marginLeft, 24);
        ConstraintSet.overrideMapToConstant.append(R.styleable.ConstraintOverride_android_layout_marginRight, 28);
        ConstraintSet.overrideMapToConstant.append(R.styleable.ConstraintOverride_android_layout_marginStart, 31);
        ConstraintSet.overrideMapToConstant.append(R.styleable.ConstraintOverride_android_layout_marginEnd, 8);
        ConstraintSet.overrideMapToConstant.append(R.styleable.ConstraintOverride_android_layout_marginTop, 34);
        ConstraintSet.overrideMapToConstant.append(R.styleable.ConstraintOverride_android_layout_marginBottom, 2);
        ConstraintSet.overrideMapToConstant.append(R.styleable.ConstraintOverride_android_layout_width, 23);
        ConstraintSet.overrideMapToConstant.append(R.styleable.ConstraintOverride_android_layout_height, 21);
        ConstraintSet.overrideMapToConstant.append(R.styleable.ConstraintOverride_layout_constraintWidth, 95);
        ConstraintSet.overrideMapToConstant.append(R.styleable.ConstraintOverride_layout_constraintHeight, 96);
        ConstraintSet.overrideMapToConstant.append(R.styleable.ConstraintOverride_android_visibility, 22);
        ConstraintSet.overrideMapToConstant.append(R.styleable.ConstraintOverride_android_alpha, 43);
        ConstraintSet.overrideMapToConstant.append(R.styleable.ConstraintOverride_android_elevation, 44);
        ConstraintSet.overrideMapToConstant.append(R.styleable.ConstraintOverride_android_rotationX, 45);
        ConstraintSet.overrideMapToConstant.append(R.styleable.ConstraintOverride_android_rotationY, 46);
        ConstraintSet.overrideMapToConstant.append(R.styleable.ConstraintOverride_android_rotation, 60);
        ConstraintSet.overrideMapToConstant.append(R.styleable.ConstraintOverride_android_scaleX, 47);
        ConstraintSet.overrideMapToConstant.append(R.styleable.ConstraintOverride_android_scaleY, 48);
        ConstraintSet.overrideMapToConstant.append(R.styleable.ConstraintOverride_android_transformPivotX, 49);
        ConstraintSet.overrideMapToConstant.append(R.styleable.ConstraintOverride_android_transformPivotY, 50);
        ConstraintSet.overrideMapToConstant.append(R.styleable.ConstraintOverride_android_translationX, 51);
        ConstraintSet.overrideMapToConstant.append(R.styleable.ConstraintOverride_android_translationY, 52);
        ConstraintSet.overrideMapToConstant.append(R.styleable.ConstraintOverride_android_translationZ, 53);
        ConstraintSet.overrideMapToConstant.append(R.styleable.ConstraintOverride_layout_constraintWidth_default, 54);
        ConstraintSet.overrideMapToConstant.append(R.styleable.ConstraintOverride_layout_constraintHeight_default, 55);
        ConstraintSet.overrideMapToConstant.append(R.styleable.ConstraintOverride_layout_constraintWidth_max, 56);
        ConstraintSet.overrideMapToConstant.append(R.styleable.ConstraintOverride_layout_constraintHeight_max, 57);
        ConstraintSet.overrideMapToConstant.append(R.styleable.ConstraintOverride_layout_constraintWidth_min, 58);
        ConstraintSet.overrideMapToConstant.append(R.styleable.ConstraintOverride_layout_constraintHeight_min, 59);
        ConstraintSet.overrideMapToConstant.append(R.styleable.ConstraintOverride_layout_constraintCircleRadius, 62);
        ConstraintSet.overrideMapToConstant.append(R.styleable.ConstraintOverride_layout_constraintCircleAngle, 63);
        ConstraintSet.overrideMapToConstant.append(R.styleable.ConstraintOverride_animateRelativeTo, 64);
        ConstraintSet.overrideMapToConstant.append(R.styleable.ConstraintOverride_transitionEasing, 65);
        ConstraintSet.overrideMapToConstant.append(R.styleable.ConstraintOverride_drawPath, 66);
        ConstraintSet.overrideMapToConstant.append(R.styleable.ConstraintOverride_transitionPathRotate, 67);
        ConstraintSet.overrideMapToConstant.append(R.styleable.ConstraintOverride_motionStagger, 79);
        ConstraintSet.overrideMapToConstant.append(R.styleable.ConstraintOverride_android_id, 38);
        ConstraintSet.overrideMapToConstant.append(R.styleable.ConstraintOverride_motionTarget, 98);
        ConstraintSet.overrideMapToConstant.append(R.styleable.ConstraintOverride_motionProgress, 68);
        ConstraintSet.overrideMapToConstant.append(R.styleable.ConstraintOverride_layout_constraintWidth_percent, 69);
        ConstraintSet.overrideMapToConstant.append(R.styleable.ConstraintOverride_layout_constraintHeight_percent, 70);
        ConstraintSet.overrideMapToConstant.append(R.styleable.ConstraintOverride_chainUseRtl, 71);
        ConstraintSet.overrideMapToConstant.append(R.styleable.ConstraintOverride_barrierDirection, 72);
        ConstraintSet.overrideMapToConstant.append(R.styleable.ConstraintOverride_barrierMargin, 73);
        ConstraintSet.overrideMapToConstant.append(R.styleable.ConstraintOverride_constraint_referenced_ids, 74);
        ConstraintSet.overrideMapToConstant.append(R.styleable.ConstraintOverride_barrierAllowsGoneWidgets, 75);
        ConstraintSet.overrideMapToConstant.append(R.styleable.ConstraintOverride_pathMotionArc, 76);
        ConstraintSet.overrideMapToConstant.append(R.styleable.ConstraintOverride_layout_constraintTag, 77);
        ConstraintSet.overrideMapToConstant.append(R.styleable.ConstraintOverride_visibilityMode, 78);
        ConstraintSet.overrideMapToConstant.append(R.styleable.ConstraintOverride_layout_constrainedWidth, 80);
        ConstraintSet.overrideMapToConstant.append(R.styleable.ConstraintOverride_layout_constrainedHeight, 81);
        ConstraintSet.overrideMapToConstant.append(R.styleable.ConstraintOverride_polarRelativeTo, 82);
        ConstraintSet.overrideMapToConstant.append(R.styleable.ConstraintOverride_transformPivotTarget, 83);
        ConstraintSet.overrideMapToConstant.append(R.styleable.ConstraintOverride_quantizeMotionSteps, 84);
        ConstraintSet.overrideMapToConstant.append(R.styleable.ConstraintOverride_quantizeMotionPhase, 85);
        ConstraintSet.overrideMapToConstant.append(R.styleable.ConstraintOverride_quantizeMotionInterpolator, 86);
        ConstraintSet.overrideMapToConstant.append(R.styleable.ConstraintOverride_layout_wrapBehaviorInParent, 97);
    }
    
    public ConstraintSet() {
        this.derivedState = "";
        this.mRotate = 0;
        this.mSavedAttributes = new HashMap<String, ConstraintAttribute>();
        this.mForceId = true;
        this.mConstraints = new HashMap<Integer, Constraint>();
    }
    
    private void addAttributes(final ConstraintAttribute.AttributeType attributeType, final String... array) {
        for (int i = 0; i < array.length; ++i) {
            if (this.mSavedAttributes.containsKey(array[i])) {
                final ConstraintAttribute constraintAttribute = this.mSavedAttributes.get(array[i]);
                if (constraintAttribute != null) {
                    if (constraintAttribute.getType() != attributeType) {
                        final StringBuilder sb = new StringBuilder();
                        sb.append("ConstraintAttribute is already a ");
                        sb.append(constraintAttribute.getType().name());
                        throw new IllegalArgumentException(sb.toString());
                    }
                }
            }
            else {
                this.mSavedAttributes.put(array[i], new ConstraintAttribute(array[i], attributeType));
            }
        }
    }
    
    public static Constraint buildDelta(final Context context, final XmlPullParser xmlPullParser) {
        final AttributeSet attributeSet = Xml.asAttributeSet(xmlPullParser);
        final Constraint constraint = new Constraint();
        final TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, R.styleable.ConstraintOverride);
        populateOverride(context, constraint, obtainStyledAttributes);
        obtainStyledAttributes.recycle();
        return constraint;
    }
    
    private int[] convertReferenceString(final View view, String original) {
        final String[] split = original.split(",");
        final Context context = view.getContext();
        original = (String)(Object)new int[split.length];
        int i;
        int newLength;
        for (i = 0, newLength = 0; i < split.length; ++i, ++newLength) {
            final String trim = split[i].trim();
            int int1;
            try {
                int1 = R.id.class.getField(trim).getInt(null);
            }
            catch (final Exception ex) {
                int1 = 0;
            }
            int identifier = int1;
            if (int1 == 0) {
                identifier = context.getResources().getIdentifier(trim, "id", context.getPackageName());
            }
            int intValue;
            if ((intValue = identifier) == 0) {
                intValue = identifier;
                if (view.isInEditMode()) {
                    intValue = identifier;
                    if (view.getParent() instanceof ConstraintLayout) {
                        final Object designInformation = ((ConstraintLayout)view.getParent()).getDesignInformation(0, trim);
                        intValue = identifier;
                        if (designInformation != null) {
                            intValue = identifier;
                            if (designInformation instanceof Integer) {
                                intValue = (int)designInformation;
                            }
                        }
                    }
                }
            }
            original[newLength] = intValue;
        }
        Object copy = original;
        if (newLength != split.length) {
            copy = Arrays.copyOf((int[])(Object)original, newLength);
        }
        return (int[])copy;
    }
    
    private void createHorizontalChain(int i, int n, final int n2, final int n3, final int[] array, final float[] array2, int horizontalChainStyle, final int n4, final int n5) {
        if (array.length < 2) {
            throw new IllegalArgumentException("must have 2 or more widgets in a chain");
        }
        if (array2 != null && array2.length != array.length) {
            throw new IllegalArgumentException("must have 2 or more widgets in a chain");
        }
        if (array2 != null) {
            this.get(array[0]).layout.horizontalWeight = array2[0];
        }
        this.get(array[0]).layout.horizontalChainStyle = horizontalChainStyle;
        this.connect(array[0], n4, i, n, -1);
        for (i = 1; i < array.length; ++i) {
            horizontalChainStyle = array[i];
            n = i - 1;
            this.connect(horizontalChainStyle, n4, array[n], n5, -1);
            this.connect(array[n], n5, array[i], n4, -1);
            if (array2 != null) {
                this.get(array[i]).layout.horizontalWeight = array2[i];
            }
        }
        this.connect(array[array.length - 1], n5, n2, n3, -1);
    }
    
    private Constraint fillFromAttributeList(final Context context, final AttributeSet set, final boolean b) {
        final Constraint constraint = new Constraint();
        int[] array;
        if (b) {
            array = R.styleable.ConstraintOverride;
        }
        else {
            array = R.styleable.Constraint;
        }
        final TypedArray obtainStyledAttributes = context.obtainStyledAttributes(set, array);
        this.populateConstraint(context, constraint, obtainStyledAttributes, b);
        obtainStyledAttributes.recycle();
        return constraint;
    }
    
    private Constraint get(final int i) {
        if (!this.mConstraints.containsKey(i)) {
            this.mConstraints.put(i, new Constraint());
        }
        return this.mConstraints.get(i);
    }
    
    static String getDebugName(final int n) {
        for (final Field field : ConstraintSet.class.getDeclaredFields()) {
            if (field.getName().contains("_") && field.getType() == Integer.TYPE && Modifier.isStatic(field.getModifiers()) && Modifier.isFinal(field.getModifiers())) {
                try {
                    if (field.getInt(null) == n) {
                        return field.getName();
                    }
                }
                catch (final IllegalAccessException ex) {
                    ex.printStackTrace();
                }
            }
        }
        return "UNKNOWN";
    }
    
    static String getLine(final Context context, final int n, final XmlPullParser xmlPullParser) {
        final StringBuilder sb = new StringBuilder();
        sb.append(".(");
        sb.append(Debug.getName(context, n));
        sb.append(".xml:");
        sb.append(xmlPullParser.getLineNumber());
        sb.append(") \"");
        sb.append(xmlPullParser.getName());
        sb.append("\"");
        return sb.toString();
    }
    
    private static int lookupID(final TypedArray typedArray, final int n, int n2) {
        if ((n2 = typedArray.getResourceId(n, n2)) == -1) {
            n2 = typedArray.getInt(n, -1);
        }
        return n2;
    }
    
    static void parseDimensionConstraints(final Object o, final TypedArray typedArray, int dimensionPixelSize, final int n) {
        if (o == null) {
            return;
        }
        final int type = typedArray.peekValue(dimensionPixelSize).type;
        if (type != 3) {
            final int n2 = 0;
            boolean b = false;
            Label_0099: {
                if (type != 5) {
                    final int int1 = typedArray.getInt(dimensionPixelSize, 0);
                    if (int1 == -4) {
                        b = true;
                        dimensionPixelSize = -2;
                        break Label_0099;
                    }
                    dimensionPixelSize = n2;
                    if (int1 != -3 && (dimensionPixelSize = int1) != -2 && (dimensionPixelSize = int1) != -1) {
                        dimensionPixelSize = n2;
                    }
                }
                else {
                    dimensionPixelSize = typedArray.getDimensionPixelSize(dimensionPixelSize, 0);
                }
                b = false;
            }
            if (o instanceof ConstraintLayout.LayoutParams) {
                final ConstraintLayout.LayoutParams layoutParams = (ConstraintLayout.LayoutParams)o;
                if (n == 0) {
                    layoutParams.width = dimensionPixelSize;
                    layoutParams.constrainedWidth = b;
                }
                else {
                    layoutParams.height = dimensionPixelSize;
                    layoutParams.constrainedHeight = b;
                }
            }
            else if (o instanceof Layout) {
                final Layout layout = (Layout)o;
                if (n == 0) {
                    layout.mWidth = dimensionPixelSize;
                    layout.constrainedWidth = b;
                }
                else {
                    layout.mHeight = dimensionPixelSize;
                    layout.constrainedHeight = b;
                }
            }
            else if (o instanceof Delta) {
                final Delta delta = (Delta)o;
                if (n == 0) {
                    delta.add(23, dimensionPixelSize);
                    delta.add(80, b);
                }
                else {
                    delta.add(21, dimensionPixelSize);
                    delta.add(81, b);
                }
            }
            return;
        }
        parseDimensionConstraintsString(o, typedArray.getString(dimensionPixelSize), n);
    }
    
    static void parseDimensionConstraintsString(final Object o, String trim, final int n) {
        if (trim == null) {
            return;
        }
        final int index = trim.indexOf(61);
        final int length = trim.length();
        if (index <= 0 || index >= length - 1) {
            return;
        }
        final String substring = trim.substring(0, index);
        final String substring2 = trim.substring(index + 1);
        if (substring2.length() <= 0) {
            return;
        }
        trim = substring.trim();
        final String trim2 = substring2.trim();
        Label_0308: {
            if ("ratio".equalsIgnoreCase(trim)) {
                if (o instanceof ConstraintLayout.LayoutParams) {
                    final ConstraintLayout.LayoutParams layoutParams = (ConstraintLayout.LayoutParams)o;
                    if (n == 0) {
                        layoutParams.width = 0;
                    }
                    else {
                        layoutParams.height = 0;
                    }
                    parseDimensionRatioString(layoutParams, trim2);
                    return;
                }
                if (o instanceof Layout) {
                    ((Layout)o).dimensionRatio = trim2;
                    return;
                }
                if (o instanceof Delta) {
                    ((Delta)o).add(5, trim2);
                }
                return;
            }
            else if (!"weight".equalsIgnoreCase(trim)) {
                break Label_0308;
            }
            try {
                final float float1 = Float.parseFloat(trim2);
                if (o instanceof ConstraintLayout.LayoutParams) {
                    final ConstraintLayout.LayoutParams layoutParams2 = (ConstraintLayout.LayoutParams)o;
                    if (n == 0) {
                        layoutParams2.width = 0;
                        layoutParams2.horizontalWeight = float1;
                    }
                    else {
                        layoutParams2.height = 0;
                        layoutParams2.verticalWeight = float1;
                    }
                }
                else if (o instanceof Layout) {
                    final Layout layout = (Layout)o;
                    if (n == 0) {
                        layout.mWidth = 0;
                        layout.horizontalWeight = float1;
                    }
                    else {
                        layout.mHeight = 0;
                        layout.verticalWeight = float1;
                    }
                }
                else if (o instanceof Delta) {
                    final Delta delta = (Delta)o;
                    if (n == 0) {
                        delta.add(23, 0);
                        delta.add(39, float1);
                    }
                    else {
                        delta.add(21, 0);
                        delta.add(40, float1);
                    }
                }
                Label_0482: {
                    return;
                }
                Delta delta2 = null;
                Label_0468:
                delta2.add(21, 0);
                delta2.add(55, 2);
                return;
                Label_0435:
                iftrue(Label_0482:)(!(o instanceof Delta));
                float max;
                ConstraintLayout.LayoutParams layoutParams3;
                while (true) {
                    Block_19: {
                    Block_24:
                        while (true) {
                            Block_23: {
                                break Block_23;
                                iftrue(Label_0482:)(!"parent".equalsIgnoreCase(trim));
                                Block_18: {
                                    break Block_18;
                                    final Layout layout2;
                                    layout2.mWidth = 0;
                                    layout2.widthPercent = max;
                                    layout2.widthDefault = 2;
                                    return;
                                }
                                max = Math.max(0.0f, Math.min(1.0f, Float.parseFloat(trim2)));
                                iftrue(Label_0383:)(!(o instanceof ConstraintLayout.LayoutParams));
                                break Block_19;
                            }
                            delta2 = (Delta)o;
                            iftrue(Label_0468:)(n != 0);
                            break Block_24;
                            Label_0383:
                            iftrue(Label_0435:)(!(o instanceof Layout));
                            final Layout layout2 = (Layout)o;
                            iftrue(Label_0417:)(n != 0);
                            continue;
                        }
                        delta2.add(23, 0);
                        delta2.add(54, 2);
                        return;
                        Layout layout2 = null;
                        Label_0417:
                        layout2.mHeight = 0;
                        layout2.heightPercent = max;
                        layout2.heightDefault = 2;
                        return;
                        layoutParams3.width = 0;
                        layoutParams3.matchConstraintPercentWidth = max;
                        layoutParams3.matchConstraintDefaultWidth = 2;
                        return;
                    }
                    layoutParams3 = (ConstraintLayout.LayoutParams)o;
                    iftrue(Label_0365:)(n != 0);
                    continue;
                }
                Label_0365:
                layoutParams3.height = 0;
                layoutParams3.matchConstraintPercentHeight = max;
                layoutParams3.matchConstraintDefaultHeight = 2;
            }
            catch (final NumberFormatException ex) {}
        }
    }
    
    static void parseDimensionRatioString(final ConstraintLayout.LayoutParams layoutParams, final String dimensionRatio) {
        final float n = Float.NaN;
        final int n2 = -1;
        float dimensionRatioValue = n;
        int dimensionRatioSide = n2;
    Block_14_Outer:
        while (true) {
            if (dimensionRatio == null) {
                break Label_0294;
            }
            final int length = dimensionRatio.length();
            final int index = dimensionRatio.indexOf(44);
            final int n3 = 0;
            int n4 = n2;
            int n5 = n3;
            if (index > 0) {
                n4 = n2;
                n5 = n3;
                if (index < length - 1) {
                    final String substring = dimensionRatio.substring(0, index);
                    if (substring.equalsIgnoreCase("W")) {
                        n4 = 0;
                    }
                    else {
                        n4 = n2;
                        if (substring.equalsIgnoreCase("H")) {
                            n4 = 1;
                        }
                    }
                    n5 = index + 1;
                }
            }
            final int index2 = dimensionRatio.indexOf(58);
            Label_0262: {
                if (index2 < 0 || index2 >= length - 1) {
                    break Label_0262;
                }
                final String substring2 = dimensionRatio.substring(n5, index2);
                final String substring3 = dimensionRatio.substring(index2 + 1);
                dimensionRatioValue = n;
                dimensionRatioSide = n4;
                if (substring2.length() <= 0) {
                    break Label_0294;
                }
                dimensionRatioValue = n;
                dimensionRatioSide = n4;
                if (substring3.length() <= 0) {
                    break Label_0294;
                }
                try {
                    final float float1 = Float.parseFloat(substring2);
                    final float float2 = Float.parseFloat(substring3);
                    dimensionRatioValue = n;
                    dimensionRatioSide = n4;
                    if (float1 > 0.0f) {
                        dimensionRatioValue = n;
                        dimensionRatioSide = n4;
                        if (float2 > 0.0f) {
                            if (n4 == 1) {
                                dimensionRatioValue = Math.abs(float2 / float1);
                                dimensionRatioSide = n4;
                            }
                            else {
                                dimensionRatioValue = Math.abs(float1 / float2);
                                dimensionRatioSide = n4;
                            }
                        }
                    }
                    layoutParams.dimensionRatio = dimensionRatio;
                    layoutParams.dimensionRatioValue = dimensionRatioValue;
                    layoutParams.dimensionRatioSide = dimensionRatioSide;
                    return;
                    while (true) {
                        final String substring4;
                        dimensionRatioValue = Float.parseFloat(substring4);
                        dimensionRatioSide = n4;
                        continue Block_14_Outer;
                        substring4 = dimensionRatio.substring(n5);
                        dimensionRatioValue = n;
                        dimensionRatioSide = n4;
                        iftrue(Label_0294:)(substring4.length() <= 0);
                        continue;
                    }
                }
                catch (final NumberFormatException ex) {
                    dimensionRatioValue = n;
                    dimensionRatioSide = n4;
                    continue;
                }
            }
            break;
        }
    }
    
    private void populateConstraint(final Context context, final Constraint constraint, final TypedArray typedArray, final boolean b) {
        if (b) {
            populateOverride(context, constraint, typedArray);
            return;
        }
        final int indexCount = typedArray.getIndexCount();
        int i = 0;
    Label_2746_Outer:
        while (i < indexCount) {
            final int index = typedArray.getIndex(i);
            if (index != R.styleable.Constraint_android_id && R.styleable.Constraint_android_layout_marginStart != index && R.styleable.Constraint_android_layout_marginEnd != index) {
                constraint.motion.mApply = true;
                constraint.layout.mApply = true;
                constraint.propertySet.mApply = true;
                constraint.transform.mApply = true;
            }
            while (true) {
                switch (ConstraintSet.mapToConstant.get(index)) {
                    default: {
                        final StringBuilder sb = new StringBuilder();
                        sb.append("Unknown attribute 0x");
                        sb.append(Integer.toHexString(index));
                        sb.append("   ");
                        sb.append(ConstraintSet.mapToConstant.get(index));
                        break Label_2746;
                    }
                    case 86: {
                        final int type = typedArray.peekValue(index).type;
                        if (type == 1) {
                            constraint.motion.mQuantizeInterpolatorID = typedArray.getResourceId(index, -1);
                            final Motion motion = constraint.motion;
                            if (motion.mQuantizeInterpolatorID != -1) {
                                motion.mQuantizeInterpolatorType = -2;
                            }
                            break Label_2746;
                        }
                        else {
                            if (type != 3) {
                                final Motion motion2 = constraint.motion;
                                motion2.mQuantizeInterpolatorType = typedArray.getInteger(index, motion2.mQuantizeInterpolatorID);
                                break Label_2746;
                            }
                            constraint.motion.mQuantizeInterpolatorString = typedArray.getString(index);
                            if (constraint.motion.mQuantizeInterpolatorString.indexOf("/") > 0) {
                                constraint.motion.mQuantizeInterpolatorID = typedArray.getResourceId(index, -1);
                                constraint.motion.mQuantizeInterpolatorType = -2;
                                break Label_2746;
                            }
                            constraint.motion.mQuantizeInterpolatorType = -1;
                            break Label_2746;
                        }
                        break;
                    }
                    case 65: {
                        if (typedArray.peekValue(index).type == 3) {
                            constraint.motion.mTransitionEasing = typedArray.getString(index);
                            break Label_2746;
                        }
                        constraint.motion.mTransitionEasing = Easing.NAMED_EASING[typedArray.getInteger(index, 0)];
                        break Label_2746;
                    }
                    case 71: {
                        ++i;
                        continue Label_2746_Outer;
                    }
                    case 97: {
                        final Layout layout = constraint.layout;
                        layout.mWrapBehavior = typedArray.getInt(index, layout.mWrapBehavior);
                        continue;
                    }
                    case 96: {
                        parseDimensionConstraints(constraint.layout, typedArray, index, 1);
                        continue;
                    }
                    case 95: {
                        parseDimensionConstraints(constraint.layout, typedArray, index, 0);
                        continue;
                    }
                    case 94: {
                        final Layout layout2 = constraint.layout;
                        layout2.goneBaselineMargin = typedArray.getDimensionPixelSize(index, layout2.goneBaselineMargin);
                        continue;
                    }
                    case 93: {
                        final Layout layout3 = constraint.layout;
                        layout3.baselineMargin = typedArray.getDimensionPixelSize(index, layout3.baselineMargin);
                        continue;
                    }
                    case 92: {
                        final Layout layout4 = constraint.layout;
                        layout4.baselineToBottom = lookupID(typedArray, index, layout4.baselineToBottom);
                        continue;
                    }
                    case 91: {
                        final Layout layout5 = constraint.layout;
                        layout5.baselineToTop = lookupID(typedArray, index, layout5.baselineToTop);
                        continue;
                    }
                    case 87: {
                        final StringBuilder sb2 = new StringBuilder();
                        sb2.append("unused attribute 0x");
                        sb2.append(Integer.toHexString(index));
                        sb2.append("   ");
                        sb2.append(ConstraintSet.mapToConstant.get(index));
                        continue;
                    }
                    case 85: {
                        final Motion motion3 = constraint.motion;
                        motion3.mQuantizeMotionPhase = typedArray.getFloat(index, motion3.mQuantizeMotionPhase);
                        continue;
                    }
                    case 84: {
                        final Motion motion4 = constraint.motion;
                        motion4.mQuantizeMotionSteps = typedArray.getInteger(index, motion4.mQuantizeMotionSteps);
                        continue;
                    }
                    case 83: {
                        final Transform transform = constraint.transform;
                        transform.transformPivotTarget = lookupID(typedArray, index, transform.transformPivotTarget);
                        continue;
                    }
                    case 82: {
                        final Motion motion5 = constraint.motion;
                        motion5.mAnimateCircleAngleTo = typedArray.getInteger(index, motion5.mAnimateCircleAngleTo);
                        continue;
                    }
                    case 81: {
                        final Layout layout6 = constraint.layout;
                        layout6.constrainedHeight = typedArray.getBoolean(index, layout6.constrainedHeight);
                        continue;
                    }
                    case 80: {
                        final Layout layout7 = constraint.layout;
                        layout7.constrainedWidth = typedArray.getBoolean(index, layout7.constrainedWidth);
                        continue;
                    }
                    case 79: {
                        final Motion motion6 = constraint.motion;
                        motion6.mMotionStagger = typedArray.getFloat(index, motion6.mMotionStagger);
                        continue;
                    }
                    case 78: {
                        final PropertySet propertySet = constraint.propertySet;
                        propertySet.mVisibilityMode = typedArray.getInt(index, propertySet.mVisibilityMode);
                        continue;
                    }
                    case 77: {
                        constraint.layout.mConstraintTag = typedArray.getString(index);
                        continue;
                    }
                    case 76: {
                        final Motion motion7 = constraint.motion;
                        motion7.mPathMotionArc = typedArray.getInt(index, motion7.mPathMotionArc);
                        continue;
                    }
                    case 75: {
                        final Layout layout8 = constraint.layout;
                        layout8.mBarrierAllowsGoneWidgets = typedArray.getBoolean(index, layout8.mBarrierAllowsGoneWidgets);
                        continue;
                    }
                    case 74: {
                        constraint.layout.mReferenceIdString = typedArray.getString(index);
                        continue;
                    }
                    case 73: {
                        final Layout layout9 = constraint.layout;
                        layout9.mBarrierMargin = typedArray.getDimensionPixelSize(index, layout9.mBarrierMargin);
                        continue;
                    }
                    case 72: {
                        final Layout layout10 = constraint.layout;
                        layout10.mBarrierDirection = typedArray.getInt(index, layout10.mBarrierDirection);
                        continue;
                    }
                    case 70: {
                        constraint.layout.heightPercent = typedArray.getFloat(index, 1.0f);
                        continue;
                    }
                    case 69: {
                        constraint.layout.widthPercent = typedArray.getFloat(index, 1.0f);
                        continue;
                    }
                    case 68: {
                        final PropertySet propertySet2 = constraint.propertySet;
                        propertySet2.mProgress = typedArray.getFloat(index, propertySet2.mProgress);
                        continue;
                    }
                    case 67: {
                        final Motion motion8 = constraint.motion;
                        motion8.mPathRotate = typedArray.getFloat(index, motion8.mPathRotate);
                        continue;
                    }
                    case 66: {
                        constraint.motion.mDrawPath = typedArray.getInt(index, 0);
                        continue;
                    }
                    case 64: {
                        final Motion motion9 = constraint.motion;
                        motion9.mAnimateRelativeTo = lookupID(typedArray, index, motion9.mAnimateRelativeTo);
                        continue;
                    }
                    case 63: {
                        final Layout layout11 = constraint.layout;
                        layout11.circleAngle = typedArray.getFloat(index, layout11.circleAngle);
                        continue;
                    }
                    case 62: {
                        final Layout layout12 = constraint.layout;
                        layout12.circleRadius = typedArray.getDimensionPixelSize(index, layout12.circleRadius);
                        continue;
                    }
                    case 61: {
                        final Layout layout13 = constraint.layout;
                        layout13.circleConstraint = lookupID(typedArray, index, layout13.circleConstraint);
                        continue;
                    }
                    case 60: {
                        final Transform transform2 = constraint.transform;
                        transform2.rotation = typedArray.getFloat(index, transform2.rotation);
                        continue;
                    }
                    case 59: {
                        final Layout layout14 = constraint.layout;
                        layout14.heightMin = typedArray.getDimensionPixelSize(index, layout14.heightMin);
                        continue;
                    }
                    case 58: {
                        final Layout layout15 = constraint.layout;
                        layout15.widthMin = typedArray.getDimensionPixelSize(index, layout15.widthMin);
                        continue;
                    }
                    case 57: {
                        final Layout layout16 = constraint.layout;
                        layout16.heightMax = typedArray.getDimensionPixelSize(index, layout16.heightMax);
                        continue;
                    }
                    case 56: {
                        final Layout layout17 = constraint.layout;
                        layout17.widthMax = typedArray.getDimensionPixelSize(index, layout17.widthMax);
                        continue;
                    }
                    case 55: {
                        final Layout layout18 = constraint.layout;
                        layout18.heightDefault = typedArray.getInt(index, layout18.heightDefault);
                        continue;
                    }
                    case 54: {
                        final Layout layout19 = constraint.layout;
                        layout19.widthDefault = typedArray.getInt(index, layout19.widthDefault);
                        continue;
                    }
                    case 53: {
                        final Transform transform3 = constraint.transform;
                        transform3.translationZ = typedArray.getDimension(index, transform3.translationZ);
                        continue;
                    }
                    case 52: {
                        final Transform transform4 = constraint.transform;
                        transform4.translationY = typedArray.getDimension(index, transform4.translationY);
                        continue;
                    }
                    case 51: {
                        final Transform transform5 = constraint.transform;
                        transform5.translationX = typedArray.getDimension(index, transform5.translationX);
                        continue;
                    }
                    case 50: {
                        final Transform transform6 = constraint.transform;
                        transform6.transformPivotY = typedArray.getDimension(index, transform6.transformPivotY);
                        continue;
                    }
                    case 49: {
                        final Transform transform7 = constraint.transform;
                        transform7.transformPivotX = typedArray.getDimension(index, transform7.transformPivotX);
                        continue;
                    }
                    case 48: {
                        final Transform transform8 = constraint.transform;
                        transform8.scaleY = typedArray.getFloat(index, transform8.scaleY);
                        continue;
                    }
                    case 47: {
                        final Transform transform9 = constraint.transform;
                        transform9.scaleX = typedArray.getFloat(index, transform9.scaleX);
                        continue;
                    }
                    case 46: {
                        final Transform transform10 = constraint.transform;
                        transform10.rotationY = typedArray.getFloat(index, transform10.rotationY);
                        continue;
                    }
                    case 45: {
                        final Transform transform11 = constraint.transform;
                        transform11.rotationX = typedArray.getFloat(index, transform11.rotationX);
                        continue;
                    }
                    case 44: {
                        final Transform transform12 = constraint.transform;
                        transform12.applyElevation = true;
                        transform12.elevation = typedArray.getDimension(index, transform12.elevation);
                        continue;
                    }
                    case 43: {
                        final PropertySet propertySet3 = constraint.propertySet;
                        propertySet3.alpha = typedArray.getFloat(index, propertySet3.alpha);
                        continue;
                    }
                    case 42: {
                        final Layout layout20 = constraint.layout;
                        layout20.verticalChainStyle = typedArray.getInt(index, layout20.verticalChainStyle);
                        continue;
                    }
                    case 41: {
                        final Layout layout21 = constraint.layout;
                        layout21.horizontalChainStyle = typedArray.getInt(index, layout21.horizontalChainStyle);
                        continue;
                    }
                    case 40: {
                        final Layout layout22 = constraint.layout;
                        layout22.verticalWeight = typedArray.getFloat(index, layout22.verticalWeight);
                        continue;
                    }
                    case 39: {
                        final Layout layout23 = constraint.layout;
                        layout23.horizontalWeight = typedArray.getFloat(index, layout23.horizontalWeight);
                        continue;
                    }
                    case 38: {
                        constraint.mViewId = typedArray.getResourceId(index, constraint.mViewId);
                        continue;
                    }
                    case 37: {
                        final Layout layout24 = constraint.layout;
                        layout24.verticalBias = typedArray.getFloat(index, layout24.verticalBias);
                        continue;
                    }
                    case 36: {
                        final Layout layout25 = constraint.layout;
                        layout25.topToTop = lookupID(typedArray, index, layout25.topToTop);
                        continue;
                    }
                    case 35: {
                        final Layout layout26 = constraint.layout;
                        layout26.topToBottom = lookupID(typedArray, index, layout26.topToBottom);
                        continue;
                    }
                    case 34: {
                        final Layout layout27 = constraint.layout;
                        layout27.topMargin = typedArray.getDimensionPixelSize(index, layout27.topMargin);
                        continue;
                    }
                    case 33: {
                        final Layout layout28 = constraint.layout;
                        layout28.startToStart = lookupID(typedArray, index, layout28.startToStart);
                        continue;
                    }
                    case 32: {
                        final Layout layout29 = constraint.layout;
                        layout29.startToEnd = lookupID(typedArray, index, layout29.startToEnd);
                        continue;
                    }
                    case 31: {
                        final Layout layout30 = constraint.layout;
                        layout30.startMargin = typedArray.getDimensionPixelSize(index, layout30.startMargin);
                        continue;
                    }
                    case 30: {
                        final Layout layout31 = constraint.layout;
                        layout31.rightToRight = lookupID(typedArray, index, layout31.rightToRight);
                        continue;
                    }
                    case 29: {
                        final Layout layout32 = constraint.layout;
                        layout32.rightToLeft = lookupID(typedArray, index, layout32.rightToLeft);
                        continue;
                    }
                    case 28: {
                        final Layout layout33 = constraint.layout;
                        layout33.rightMargin = typedArray.getDimensionPixelSize(index, layout33.rightMargin);
                        continue;
                    }
                    case 27: {
                        final Layout layout34 = constraint.layout;
                        layout34.orientation = typedArray.getInt(index, layout34.orientation);
                        continue;
                    }
                    case 26: {
                        final Layout layout35 = constraint.layout;
                        layout35.leftToRight = lookupID(typedArray, index, layout35.leftToRight);
                        continue;
                    }
                    case 25: {
                        final Layout layout36 = constraint.layout;
                        layout36.leftToLeft = lookupID(typedArray, index, layout36.leftToLeft);
                        continue;
                    }
                    case 24: {
                        final Layout layout37 = constraint.layout;
                        layout37.leftMargin = typedArray.getDimensionPixelSize(index, layout37.leftMargin);
                        continue;
                    }
                    case 23: {
                        final Layout layout38 = constraint.layout;
                        layout38.mWidth = typedArray.getLayoutDimension(index, layout38.mWidth);
                        continue;
                    }
                    case 22: {
                        final PropertySet propertySet4 = constraint.propertySet;
                        propertySet4.visibility = typedArray.getInt(index, propertySet4.visibility);
                        final PropertySet propertySet5 = constraint.propertySet;
                        propertySet5.visibility = ConstraintSet.VISIBILITY_FLAGS[propertySet5.visibility];
                        continue;
                    }
                    case 21: {
                        final Layout layout39 = constraint.layout;
                        layout39.mHeight = typedArray.getLayoutDimension(index, layout39.mHeight);
                        continue;
                    }
                    case 20: {
                        final Layout layout40 = constraint.layout;
                        layout40.horizontalBias = typedArray.getFloat(index, layout40.horizontalBias);
                        continue;
                    }
                    case 19: {
                        final Layout layout41 = constraint.layout;
                        layout41.guidePercent = typedArray.getFloat(index, layout41.guidePercent);
                        continue;
                    }
                    case 18: {
                        final Layout layout42 = constraint.layout;
                        layout42.guideEnd = typedArray.getDimensionPixelOffset(index, layout42.guideEnd);
                        continue;
                    }
                    case 17: {
                        final Layout layout43 = constraint.layout;
                        layout43.guideBegin = typedArray.getDimensionPixelOffset(index, layout43.guideBegin);
                        continue;
                    }
                    case 16: {
                        final Layout layout44 = constraint.layout;
                        layout44.goneTopMargin = typedArray.getDimensionPixelSize(index, layout44.goneTopMargin);
                        continue;
                    }
                    case 15: {
                        final Layout layout45 = constraint.layout;
                        layout45.goneStartMargin = typedArray.getDimensionPixelSize(index, layout45.goneStartMargin);
                        continue;
                    }
                    case 14: {
                        final Layout layout46 = constraint.layout;
                        layout46.goneRightMargin = typedArray.getDimensionPixelSize(index, layout46.goneRightMargin);
                        continue;
                    }
                    case 13: {
                        final Layout layout47 = constraint.layout;
                        layout47.goneLeftMargin = typedArray.getDimensionPixelSize(index, layout47.goneLeftMargin);
                        continue;
                    }
                    case 12: {
                        final Layout layout48 = constraint.layout;
                        layout48.goneEndMargin = typedArray.getDimensionPixelSize(index, layout48.goneEndMargin);
                        continue;
                    }
                    case 11: {
                        final Layout layout49 = constraint.layout;
                        layout49.goneBottomMargin = typedArray.getDimensionPixelSize(index, layout49.goneBottomMargin);
                        continue;
                    }
                    case 10: {
                        final Layout layout50 = constraint.layout;
                        layout50.endToStart = lookupID(typedArray, index, layout50.endToStart);
                        continue;
                    }
                    case 9: {
                        final Layout layout51 = constraint.layout;
                        layout51.endToEnd = lookupID(typedArray, index, layout51.endToEnd);
                        continue;
                    }
                    case 8: {
                        final Layout layout52 = constraint.layout;
                        layout52.endMargin = typedArray.getDimensionPixelSize(index, layout52.endMargin);
                        continue;
                    }
                    case 7: {
                        final Layout layout53 = constraint.layout;
                        layout53.editorAbsoluteY = typedArray.getDimensionPixelOffset(index, layout53.editorAbsoluteY);
                        continue;
                    }
                    case 6: {
                        final Layout layout54 = constraint.layout;
                        layout54.editorAbsoluteX = typedArray.getDimensionPixelOffset(index, layout54.editorAbsoluteX);
                        continue;
                    }
                    case 5: {
                        constraint.layout.dimensionRatio = typedArray.getString(index);
                        continue;
                    }
                    case 4: {
                        final Layout layout55 = constraint.layout;
                        layout55.bottomToTop = lookupID(typedArray, index, layout55.bottomToTop);
                        continue;
                    }
                    case 3: {
                        final Layout layout56 = constraint.layout;
                        layout56.bottomToBottom = lookupID(typedArray, index, layout56.bottomToBottom);
                        continue;
                    }
                    case 2: {
                        final Layout layout57 = constraint.layout;
                        layout57.bottomMargin = typedArray.getDimensionPixelSize(index, layout57.bottomMargin);
                        continue;
                    }
                    case 1: {
                        final Layout layout58 = constraint.layout;
                        layout58.baselineToBaseline = lookupID(typedArray, index, layout58.baselineToBaseline);
                        continue;
                    }
                }
                break;
            }
        }
    }
    
    private static void populateOverride(final Context context, final Constraint constraint, final TypedArray typedArray) {
        final int indexCount = typedArray.getIndexCount();
        final Delta mDelta = new Delta();
        constraint.mDelta = mDelta;
        constraint.motion.mApply = false;
        constraint.layout.mApply = false;
        constraint.propertySet.mApply = false;
        constraint.transform.mApply = false;
        int i = 0;
    Label_2528_Outer:
        while (i < indexCount) {
            final int index = typedArray.getIndex(i);
            while (true) {
                switch (ConstraintSet.overrideMapToConstant.get(index)) {
                    default: {
                        final StringBuilder sb = new StringBuilder();
                        sb.append("Unknown attribute 0x");
                        sb.append(Integer.toHexString(index));
                        sb.append("   ");
                        sb.append(ConstraintSet.mapToConstant.get(index));
                        break Label_2528;
                    }
                    case 98: {
                        if (MotionLayout.IS_IN_EDIT_MODE) {
                            if ((constraint.mViewId = typedArray.getResourceId(index, constraint.mViewId)) == -1) {
                                constraint.mTargetString = typedArray.getString(index);
                            }
                            break Label_2528;
                        }
                        else {
                            if (typedArray.peekValue(index).type == 3) {
                                constraint.mTargetString = typedArray.getString(index);
                                break Label_2528;
                            }
                            constraint.mViewId = typedArray.getResourceId(index, constraint.mViewId);
                            break Label_2528;
                        }
                        break;
                    }
                    case 86: {
                        final int type = typedArray.peekValue(index).type;
                        if (type == 1) {
                            mDelta.add(89, constraint.motion.mQuantizeInterpolatorID = typedArray.getResourceId(index, -1));
                            final Motion motion = constraint.motion;
                            if (motion.mQuantizeInterpolatorID != -1) {
                                mDelta.add(88, motion.mQuantizeInterpolatorType = -2);
                            }
                            break Label_2528;
                        }
                        else {
                            if (type != 3) {
                                final Motion motion2 = constraint.motion;
                                motion2.mQuantizeInterpolatorType = typedArray.getInteger(index, motion2.mQuantizeInterpolatorID);
                                mDelta.add(88, constraint.motion.mQuantizeInterpolatorType);
                                break Label_2528;
                            }
                            mDelta.add(90, constraint.motion.mQuantizeInterpolatorString = typedArray.getString(index));
                            if (constraint.motion.mQuantizeInterpolatorString.indexOf("/") > 0) {
                                mDelta.add(89, constraint.motion.mQuantizeInterpolatorID = typedArray.getResourceId(index, -1));
                                mDelta.add(88, constraint.motion.mQuantizeInterpolatorType = -2);
                                break Label_2528;
                            }
                            mDelta.add(88, constraint.motion.mQuantizeInterpolatorType = -1);
                            break Label_2528;
                        }
                        break;
                    }
                    case 65: {
                        if (typedArray.peekValue(index).type == 3) {
                            mDelta.add(65, typedArray.getString(index));
                            break Label_2528;
                        }
                        mDelta.add(65, Easing.NAMED_EASING[typedArray.getInteger(index, 0)]);
                        break Label_2528;
                    }
                    case 71: {
                        ++i;
                        continue Label_2528_Outer;
                    }
                    case 97: {
                        mDelta.add(97, typedArray.getInt(index, constraint.layout.mWrapBehavior));
                        continue;
                    }
                    case 96: {
                        parseDimensionConstraints(mDelta, typedArray, index, 1);
                        continue;
                    }
                    case 95: {
                        parseDimensionConstraints(mDelta, typedArray, index, 0);
                        continue;
                    }
                    case 94: {
                        mDelta.add(94, typedArray.getDimensionPixelSize(index, constraint.layout.goneBaselineMargin));
                        continue;
                    }
                    case 93: {
                        mDelta.add(93, typedArray.getDimensionPixelSize(index, constraint.layout.baselineMargin));
                        continue;
                    }
                    case 87: {
                        final StringBuilder sb2 = new StringBuilder();
                        sb2.append("unused attribute 0x");
                        sb2.append(Integer.toHexString(index));
                        sb2.append("   ");
                        sb2.append(ConstraintSet.mapToConstant.get(index));
                        continue;
                    }
                    case 85: {
                        mDelta.add(85, typedArray.getFloat(index, constraint.motion.mQuantizeMotionPhase));
                        continue;
                    }
                    case 84: {
                        mDelta.add(84, typedArray.getInteger(index, constraint.motion.mQuantizeMotionSteps));
                        continue;
                    }
                    case 83: {
                        mDelta.add(83, lookupID(typedArray, index, constraint.transform.transformPivotTarget));
                        continue;
                    }
                    case 82: {
                        mDelta.add(82, typedArray.getInteger(index, constraint.motion.mAnimateCircleAngleTo));
                        continue;
                    }
                    case 81: {
                        mDelta.add(81, typedArray.getBoolean(index, constraint.layout.constrainedHeight));
                        continue;
                    }
                    case 80: {
                        mDelta.add(80, typedArray.getBoolean(index, constraint.layout.constrainedWidth));
                        continue;
                    }
                    case 79: {
                        mDelta.add(79, typedArray.getFloat(index, constraint.motion.mMotionStagger));
                        continue;
                    }
                    case 78: {
                        mDelta.add(78, typedArray.getInt(index, constraint.propertySet.mVisibilityMode));
                        continue;
                    }
                    case 77: {
                        mDelta.add(77, typedArray.getString(index));
                        continue;
                    }
                    case 76: {
                        mDelta.add(76, typedArray.getInt(index, constraint.motion.mPathMotionArc));
                        continue;
                    }
                    case 75: {
                        mDelta.add(75, typedArray.getBoolean(index, constraint.layout.mBarrierAllowsGoneWidgets));
                        continue;
                    }
                    case 74: {
                        mDelta.add(74, typedArray.getString(index));
                        continue;
                    }
                    case 73: {
                        mDelta.add(73, typedArray.getDimensionPixelSize(index, constraint.layout.mBarrierMargin));
                        continue;
                    }
                    case 72: {
                        mDelta.add(72, typedArray.getInt(index, constraint.layout.mBarrierDirection));
                        continue;
                    }
                    case 70: {
                        mDelta.add(70, typedArray.getFloat(index, 1.0f));
                        continue;
                    }
                    case 69: {
                        mDelta.add(69, typedArray.getFloat(index, 1.0f));
                        continue;
                    }
                    case 68: {
                        mDelta.add(68, typedArray.getFloat(index, constraint.propertySet.mProgress));
                        continue;
                    }
                    case 67: {
                        mDelta.add(67, typedArray.getFloat(index, constraint.motion.mPathRotate));
                        continue;
                    }
                    case 66: {
                        mDelta.add(66, typedArray.getInt(index, 0));
                        continue;
                    }
                    case 64: {
                        mDelta.add(64, lookupID(typedArray, index, constraint.motion.mAnimateRelativeTo));
                        continue;
                    }
                    case 63: {
                        mDelta.add(63, typedArray.getFloat(index, constraint.layout.circleAngle));
                        continue;
                    }
                    case 62: {
                        mDelta.add(62, typedArray.getDimensionPixelSize(index, constraint.layout.circleRadius));
                        continue;
                    }
                    case 60: {
                        mDelta.add(60, typedArray.getFloat(index, constraint.transform.rotation));
                        continue;
                    }
                    case 59: {
                        mDelta.add(59, typedArray.getDimensionPixelSize(index, constraint.layout.heightMin));
                        continue;
                    }
                    case 58: {
                        mDelta.add(58, typedArray.getDimensionPixelSize(index, constraint.layout.widthMin));
                        continue;
                    }
                    case 57: {
                        mDelta.add(57, typedArray.getDimensionPixelSize(index, constraint.layout.heightMax));
                        continue;
                    }
                    case 56: {
                        mDelta.add(56, typedArray.getDimensionPixelSize(index, constraint.layout.widthMax));
                        continue;
                    }
                    case 55: {
                        mDelta.add(55, typedArray.getInt(index, constraint.layout.heightDefault));
                        continue;
                    }
                    case 54: {
                        mDelta.add(54, typedArray.getInt(index, constraint.layout.widthDefault));
                        continue;
                    }
                    case 53: {
                        mDelta.add(53, typedArray.getDimension(index, constraint.transform.translationZ));
                        continue;
                    }
                    case 52: {
                        mDelta.add(52, typedArray.getDimension(index, constraint.transform.translationY));
                        continue;
                    }
                    case 51: {
                        mDelta.add(51, typedArray.getDimension(index, constraint.transform.translationX));
                        continue;
                    }
                    case 50: {
                        mDelta.add(50, typedArray.getDimension(index, constraint.transform.transformPivotY));
                        continue;
                    }
                    case 49: {
                        mDelta.add(49, typedArray.getDimension(index, constraint.transform.transformPivotX));
                        continue;
                    }
                    case 48: {
                        mDelta.add(48, typedArray.getFloat(index, constraint.transform.scaleY));
                        continue;
                    }
                    case 47: {
                        mDelta.add(47, typedArray.getFloat(index, constraint.transform.scaleX));
                        continue;
                    }
                    case 46: {
                        mDelta.add(46, typedArray.getFloat(index, constraint.transform.rotationY));
                        continue;
                    }
                    case 45: {
                        mDelta.add(45, typedArray.getFloat(index, constraint.transform.rotationX));
                        continue;
                    }
                    case 44: {
                        mDelta.add(44, true);
                        mDelta.add(44, typedArray.getDimension(index, constraint.transform.elevation));
                        continue;
                    }
                    case 43: {
                        mDelta.add(43, typedArray.getFloat(index, constraint.propertySet.alpha));
                        continue;
                    }
                    case 42: {
                        mDelta.add(42, typedArray.getInt(index, constraint.layout.verticalChainStyle));
                        continue;
                    }
                    case 41: {
                        mDelta.add(41, typedArray.getInt(index, constraint.layout.horizontalChainStyle));
                        continue;
                    }
                    case 40: {
                        mDelta.add(40, typedArray.getFloat(index, constraint.layout.verticalWeight));
                        continue;
                    }
                    case 39: {
                        mDelta.add(39, typedArray.getFloat(index, constraint.layout.horizontalWeight));
                        continue;
                    }
                    case 38: {
                        mDelta.add(38, constraint.mViewId = typedArray.getResourceId(index, constraint.mViewId));
                        continue;
                    }
                    case 37: {
                        mDelta.add(37, typedArray.getFloat(index, constraint.layout.verticalBias));
                        continue;
                    }
                    case 34: {
                        mDelta.add(34, typedArray.getDimensionPixelSize(index, constraint.layout.topMargin));
                        continue;
                    }
                    case 31: {
                        mDelta.add(31, typedArray.getDimensionPixelSize(index, constraint.layout.startMargin));
                        continue;
                    }
                    case 28: {
                        mDelta.add(28, typedArray.getDimensionPixelSize(index, constraint.layout.rightMargin));
                        continue;
                    }
                    case 27: {
                        mDelta.add(27, typedArray.getInt(index, constraint.layout.orientation));
                        continue;
                    }
                    case 24: {
                        mDelta.add(24, typedArray.getDimensionPixelSize(index, constraint.layout.leftMargin));
                        continue;
                    }
                    case 23: {
                        mDelta.add(23, typedArray.getLayoutDimension(index, constraint.layout.mWidth));
                        continue;
                    }
                    case 22: {
                        mDelta.add(22, ConstraintSet.VISIBILITY_FLAGS[typedArray.getInt(index, constraint.propertySet.visibility)]);
                        continue;
                    }
                    case 21: {
                        mDelta.add(21, typedArray.getLayoutDimension(index, constraint.layout.mHeight));
                        continue;
                    }
                    case 20: {
                        mDelta.add(20, typedArray.getFloat(index, constraint.layout.horizontalBias));
                        continue;
                    }
                    case 19: {
                        mDelta.add(19, typedArray.getFloat(index, constraint.layout.guidePercent));
                        continue;
                    }
                    case 18: {
                        mDelta.add(18, typedArray.getDimensionPixelOffset(index, constraint.layout.guideEnd));
                        continue;
                    }
                    case 17: {
                        mDelta.add(17, typedArray.getDimensionPixelOffset(index, constraint.layout.guideBegin));
                        continue;
                    }
                    case 16: {
                        mDelta.add(16, typedArray.getDimensionPixelSize(index, constraint.layout.goneTopMargin));
                        continue;
                    }
                    case 15: {
                        mDelta.add(15, typedArray.getDimensionPixelSize(index, constraint.layout.goneStartMargin));
                        continue;
                    }
                    case 14: {
                        mDelta.add(14, typedArray.getDimensionPixelSize(index, constraint.layout.goneRightMargin));
                        continue;
                    }
                    case 13: {
                        mDelta.add(13, typedArray.getDimensionPixelSize(index, constraint.layout.goneLeftMargin));
                        continue;
                    }
                    case 12: {
                        mDelta.add(12, typedArray.getDimensionPixelSize(index, constraint.layout.goneEndMargin));
                        continue;
                    }
                    case 11: {
                        mDelta.add(11, typedArray.getDimensionPixelSize(index, constraint.layout.goneBottomMargin));
                        continue;
                    }
                    case 8: {
                        mDelta.add(8, typedArray.getDimensionPixelSize(index, constraint.layout.endMargin));
                        continue;
                    }
                    case 7: {
                        mDelta.add(7, typedArray.getDimensionPixelOffset(index, constraint.layout.editorAbsoluteY));
                        continue;
                    }
                    case 6: {
                        mDelta.add(6, typedArray.getDimensionPixelOffset(index, constraint.layout.editorAbsoluteX));
                        continue;
                    }
                    case 5: {
                        mDelta.add(5, typedArray.getString(index));
                        continue;
                    }
                    case 2: {
                        mDelta.add(2, typedArray.getDimensionPixelSize(index, constraint.layout.bottomMargin));
                        continue;
                    }
                }
                break;
            }
        }
    }
    
    private static void setDeltaValue(final Constraint constraint, final int n, final float n2) {
        Label_0415: {
            if (n != 19) {
                if (n != 20) {
                    if (n != 37) {
                        if (n != 60) {
                            if (n != 63) {
                                if (n != 79) {
                                    if (n != 85) {
                                        if (n != 39) {
                                            if (n != 40) {
                                                switch (n) {
                                                    default: {
                                                        switch (n) {
                                                            default: {
                                                                break Label_0415;
                                                            }
                                                            case 70: {
                                                                constraint.layout.heightPercent = n2;
                                                                break Label_0415;
                                                            }
                                                            case 69: {
                                                                constraint.layout.widthPercent = n2;
                                                                break Label_0415;
                                                            }
                                                            case 68: {
                                                                constraint.propertySet.mProgress = n2;
                                                                break Label_0415;
                                                            }
                                                            case 67: {
                                                                constraint.motion.mPathRotate = n2;
                                                                break Label_0415;
                                                            }
                                                        }
                                                        break;
                                                    }
                                                    case 53: {
                                                        constraint.transform.translationZ = n2;
                                                        break;
                                                    }
                                                    case 52: {
                                                        constraint.transform.translationY = n2;
                                                        break;
                                                    }
                                                    case 51: {
                                                        constraint.transform.translationX = n2;
                                                        break;
                                                    }
                                                    case 50: {
                                                        constraint.transform.transformPivotY = n2;
                                                        break;
                                                    }
                                                    case 49: {
                                                        constraint.transform.transformPivotX = n2;
                                                        break;
                                                    }
                                                    case 48: {
                                                        constraint.transform.scaleY = n2;
                                                        break;
                                                    }
                                                    case 47: {
                                                        constraint.transform.scaleX = n2;
                                                        break;
                                                    }
                                                    case 46: {
                                                        constraint.transform.rotationY = n2;
                                                        break;
                                                    }
                                                    case 45: {
                                                        constraint.transform.rotationX = n2;
                                                        break;
                                                    }
                                                    case 44: {
                                                        final Transform transform = constraint.transform;
                                                        transform.elevation = n2;
                                                        transform.applyElevation = true;
                                                        break;
                                                    }
                                                    case 43: {
                                                        constraint.propertySet.alpha = n2;
                                                        break;
                                                    }
                                                }
                                            }
                                            else {
                                                constraint.layout.verticalWeight = n2;
                                            }
                                        }
                                        else {
                                            constraint.layout.horizontalWeight = n2;
                                        }
                                    }
                                    else {
                                        constraint.motion.mQuantizeMotionPhase = n2;
                                    }
                                }
                                else {
                                    constraint.motion.mMotionStagger = n2;
                                }
                            }
                            else {
                                constraint.layout.circleAngle = n2;
                            }
                        }
                        else {
                            constraint.transform.rotation = n2;
                        }
                    }
                    else {
                        constraint.layout.verticalBias = n2;
                    }
                }
                else {
                    constraint.layout.horizontalBias = n2;
                }
            }
            else {
                constraint.layout.guidePercent = n2;
            }
        }
    }
    
    private static void setDeltaValue(final Constraint constraint, final int n, final int editorAbsoluteX) {
        Label_0832: {
            if (n != 6) {
                if (n != 7) {
                    if (n != 8) {
                        if (n != 27) {
                            if (n != 28) {
                                if (n != 41) {
                                    if (n != 42) {
                                        if (n != 61) {
                                            if (n != 62) {
                                                if (n != 72) {
                                                    if (n != 73) {
                                                        if (n != 88) {
                                                            if (n != 89) {
                                                                switch (n) {
                                                                    default: {
                                                                        switch (n) {
                                                                            default: {
                                                                                switch (n) {
                                                                                    default: {
                                                                                        switch (n) {
                                                                                            default: {
                                                                                                break Label_0832;
                                                                                            }
                                                                                            case 84: {
                                                                                                constraint.motion.mQuantizeMotionSteps = editorAbsoluteX;
                                                                                                break Label_0832;
                                                                                            }
                                                                                            case 83: {
                                                                                                constraint.transform.transformPivotTarget = editorAbsoluteX;
                                                                                                break Label_0832;
                                                                                            }
                                                                                            case 82: {
                                                                                                constraint.motion.mAnimateCircleAngleTo = editorAbsoluteX;
                                                                                                break Label_0832;
                                                                                            }
                                                                                        }
                                                                                        break;
                                                                                    }
                                                                                    case 59: {
                                                                                        constraint.layout.heightMin = editorAbsoluteX;
                                                                                        break Label_0832;
                                                                                    }
                                                                                    case 58: {
                                                                                        constraint.layout.widthMin = editorAbsoluteX;
                                                                                        break Label_0832;
                                                                                    }
                                                                                    case 57: {
                                                                                        constraint.layout.heightMax = editorAbsoluteX;
                                                                                        break Label_0832;
                                                                                    }
                                                                                    case 56: {
                                                                                        constraint.layout.widthMax = editorAbsoluteX;
                                                                                        break Label_0832;
                                                                                    }
                                                                                    case 55: {
                                                                                        constraint.layout.heightDefault = editorAbsoluteX;
                                                                                        break Label_0832;
                                                                                    }
                                                                                    case 54: {
                                                                                        constraint.layout.widthDefault = editorAbsoluteX;
                                                                                        break Label_0832;
                                                                                    }
                                                                                }
                                                                                break;
                                                                            }
                                                                            case 24: {
                                                                                constraint.layout.leftMargin = editorAbsoluteX;
                                                                                break Label_0832;
                                                                            }
                                                                            case 23: {
                                                                                constraint.layout.mWidth = editorAbsoluteX;
                                                                                break Label_0832;
                                                                            }
                                                                            case 22: {
                                                                                constraint.propertySet.visibility = editorAbsoluteX;
                                                                                break Label_0832;
                                                                            }
                                                                            case 21: {
                                                                                constraint.layout.mHeight = editorAbsoluteX;
                                                                                break Label_0832;
                                                                            }
                                                                        }
                                                                        break;
                                                                    }
                                                                    case 97: {
                                                                        constraint.layout.mWrapBehavior = editorAbsoluteX;
                                                                        break;
                                                                    }
                                                                    case 94: {
                                                                        constraint.layout.goneBaselineMargin = editorAbsoluteX;
                                                                        break;
                                                                    }
                                                                    case 93: {
                                                                        constraint.layout.baselineMargin = editorAbsoluteX;
                                                                        break;
                                                                    }
                                                                    case 78: {
                                                                        constraint.propertySet.mVisibilityMode = editorAbsoluteX;
                                                                        break;
                                                                    }
                                                                    case 76: {
                                                                        constraint.motion.mPathMotionArc = editorAbsoluteX;
                                                                        break;
                                                                    }
                                                                    case 66: {
                                                                        constraint.motion.mDrawPath = editorAbsoluteX;
                                                                        break;
                                                                    }
                                                                    case 64: {
                                                                        constraint.motion.mAnimateRelativeTo = editorAbsoluteX;
                                                                        break;
                                                                    }
                                                                    case 38: {
                                                                        constraint.mViewId = editorAbsoluteX;
                                                                        break;
                                                                    }
                                                                    case 34: {
                                                                        constraint.layout.topMargin = editorAbsoluteX;
                                                                        break;
                                                                    }
                                                                    case 31: {
                                                                        constraint.layout.startMargin = editorAbsoluteX;
                                                                        break;
                                                                    }
                                                                    case 18: {
                                                                        constraint.layout.guideEnd = editorAbsoluteX;
                                                                        break;
                                                                    }
                                                                    case 17: {
                                                                        constraint.layout.guideBegin = editorAbsoluteX;
                                                                        break;
                                                                    }
                                                                    case 16: {
                                                                        constraint.layout.goneTopMargin = editorAbsoluteX;
                                                                        break;
                                                                    }
                                                                    case 15: {
                                                                        constraint.layout.goneStartMargin = editorAbsoluteX;
                                                                        break;
                                                                    }
                                                                    case 14: {
                                                                        constraint.layout.goneRightMargin = editorAbsoluteX;
                                                                        break;
                                                                    }
                                                                    case 13: {
                                                                        constraint.layout.goneLeftMargin = editorAbsoluteX;
                                                                        break;
                                                                    }
                                                                    case 12: {
                                                                        constraint.layout.goneEndMargin = editorAbsoluteX;
                                                                        break;
                                                                    }
                                                                    case 11: {
                                                                        constraint.layout.goneBottomMargin = editorAbsoluteX;
                                                                        break;
                                                                    }
                                                                    case 2: {
                                                                        constraint.layout.bottomMargin = editorAbsoluteX;
                                                                        break;
                                                                    }
                                                                }
                                                            }
                                                            else {
                                                                constraint.motion.mQuantizeInterpolatorID = editorAbsoluteX;
                                                            }
                                                        }
                                                        else {
                                                            constraint.motion.mQuantizeInterpolatorType = editorAbsoluteX;
                                                        }
                                                    }
                                                    else {
                                                        constraint.layout.mBarrierMargin = editorAbsoluteX;
                                                    }
                                                }
                                                else {
                                                    constraint.layout.mBarrierDirection = editorAbsoluteX;
                                                }
                                            }
                                            else {
                                                constraint.layout.circleRadius = editorAbsoluteX;
                                            }
                                        }
                                        else {
                                            constraint.layout.circleConstraint = editorAbsoluteX;
                                        }
                                    }
                                    else {
                                        constraint.layout.verticalChainStyle = editorAbsoluteX;
                                    }
                                }
                                else {
                                    constraint.layout.horizontalChainStyle = editorAbsoluteX;
                                }
                            }
                            else {
                                constraint.layout.rightMargin = editorAbsoluteX;
                            }
                        }
                        else {
                            constraint.layout.orientation = editorAbsoluteX;
                        }
                    }
                    else {
                        constraint.layout.endMargin = editorAbsoluteX;
                    }
                }
                else {
                    constraint.layout.editorAbsoluteY = editorAbsoluteX;
                }
            }
            else {
                constraint.layout.editorAbsoluteX = editorAbsoluteX;
            }
        }
    }
    
    private static void setDeltaValue(final Constraint constraint, final int n, final String dimensionRatio) {
        if (n != 5) {
            if (n != 65) {
                if (n != 74) {
                    if (n != 77) {
                        if (n == 90) {
                            constraint.motion.mQuantizeInterpolatorString = dimensionRatio;
                        }
                    }
                    else {
                        constraint.layout.mConstraintTag = dimensionRatio;
                    }
                }
                else {
                    constraint.layout.mReferenceIdString = dimensionRatio;
                }
            }
            else {
                constraint.motion.mTransitionEasing = dimensionRatio;
            }
        }
        else {
            constraint.layout.dimensionRatio = dimensionRatio;
        }
    }
    
    private static void setDeltaValue(final Constraint constraint, final int n, final boolean b) {
        if (n != 44) {
            if (n != 75) {
                if (n != 80) {
                    if (n == 81) {
                        constraint.layout.constrainedHeight = b;
                    }
                }
                else {
                    constraint.layout.constrainedWidth = b;
                }
            }
            else {
                constraint.layout.mBarrierAllowsGoneWidgets = b;
            }
        }
        else {
            constraint.transform.applyElevation = b;
        }
    }
    
    private String sideToString(final int n) {
        switch (n) {
            default: {
                return "undefined";
            }
            case 7: {
                return "end";
            }
            case 6: {
                return "start";
            }
            case 5: {
                return "baseline";
            }
            case 4: {
                return "bottom";
            }
            case 3: {
                return "top";
            }
            case 2: {
                return "right";
            }
            case 1: {
                return "left";
            }
        }
    }
    
    private static String[] splitString(final String s) {
        final char[] charArray = s.toCharArray();
        final ArrayList list = new ArrayList();
        int i = 0;
        int n = 0;
        int n2 = 0;
        while (i < charArray.length) {
            final char c = charArray[i];
            int n3;
            int n4;
            if (c == ',' && n2 == 0) {
                list.add(new String(charArray, n, i - n));
                n3 = i + 1;
                n4 = n2;
            }
            else {
                n3 = n;
                n4 = n2;
                if (c == '\"') {
                    n4 = (n2 ^ 0x1);
                    n3 = n;
                }
            }
            ++i;
            n = n3;
            n2 = n4;
        }
        list.add(new String(charArray, n, charArray.length - n));
        return list.toArray(new String[list.size()]);
    }
    
    public void addColorAttributes(final String... array) {
        this.addAttributes(ConstraintAttribute.AttributeType.COLOR_TYPE, array);
    }
    
    public void addFloatAttributes(final String... array) {
        this.addAttributes(ConstraintAttribute.AttributeType.FLOAT_TYPE, array);
    }
    
    public void addIntAttributes(final String... array) {
        this.addAttributes(ConstraintAttribute.AttributeType.INT_TYPE, array);
    }
    
    public void addStringAttributes(final String... array) {
        this.addAttributes(ConstraintAttribute.AttributeType.STRING_TYPE, array);
    }
    
    public void addToHorizontalChain(final int n, final int n2, final int n3) {
        int n4;
        if (n2 == 0) {
            n4 = 1;
        }
        else {
            n4 = 2;
        }
        this.connect(n, 1, n2, n4, 0);
        int n5;
        if (n3 == 0) {
            n5 = 2;
        }
        else {
            n5 = 1;
        }
        this.connect(n, 2, n3, n5, 0);
        if (n2 != 0) {
            this.connect(n2, 2, n, 1, 0);
        }
        if (n3 != 0) {
            this.connect(n3, 1, n, 2, 0);
        }
    }
    
    public void addToHorizontalChainRTL(final int n, final int n2, final int n3) {
        int n4;
        if (n2 == 0) {
            n4 = 6;
        }
        else {
            n4 = 7;
        }
        this.connect(n, 6, n2, n4, 0);
        int n5;
        if (n3 == 0) {
            n5 = 7;
        }
        else {
            n5 = 6;
        }
        this.connect(n, 7, n3, n5, 0);
        if (n2 != 0) {
            this.connect(n2, 7, n, 6, 0);
        }
        if (n3 != 0) {
            this.connect(n3, 6, n, 7, 0);
        }
    }
    
    public void addToVerticalChain(final int n, final int n2, final int n3) {
        int n4;
        if (n2 == 0) {
            n4 = 3;
        }
        else {
            n4 = 4;
        }
        this.connect(n, 3, n2, n4, 0);
        int n5;
        if (n3 == 0) {
            n5 = 4;
        }
        else {
            n5 = 3;
        }
        this.connect(n, 4, n3, n5, 0);
        if (n2 != 0) {
            this.connect(n2, 4, n, 3, 0);
        }
        if (n3 != 0) {
            this.connect(n3, 3, n, 4, 0);
        }
    }
    
    public void applyCustomAttributes(final ConstraintLayout constraintLayout) {
        for (int childCount = constraintLayout.getChildCount(), i = 0; i < childCount; ++i) {
            final View child = constraintLayout.getChildAt(i);
            final int id = child.getId();
            if (!this.mConstraints.containsKey(id)) {
                final StringBuilder sb = new StringBuilder();
                sb.append("id unknown ");
                sb.append(Debug.getName(child));
            }
            else {
                if (this.mForceId && id == -1) {
                    throw new RuntimeException("All children of ConstraintLayout must have ids to use ConstraintSet");
                }
                if (this.mConstraints.containsKey(id)) {
                    final Constraint constraint = this.mConstraints.get(id);
                    if (constraint != null) {
                        ConstraintAttribute.setAttributes(child, constraint.mCustomConstraints);
                    }
                }
            }
        }
    }
    
    public void applyDeltaFrom(final ConstraintSet set) {
        for (final Constraint constraint : set.mConstraints.values()) {
            if (constraint.mDelta != null) {
                if (constraint.mTargetString != null) {
                    final Iterator<Integer> iterator2 = this.mConstraints.keySet().iterator();
                    while (iterator2.hasNext()) {
                        final Constraint constraint2 = this.getConstraint(iterator2.next());
                        final String mConstraintTag = constraint2.layout.mConstraintTag;
                        if (mConstraintTag != null && constraint.mTargetString.matches(mConstraintTag)) {
                            constraint.mDelta.applyDelta(constraint2);
                            constraint2.mCustomConstraints.putAll((Map<? extends String, ? extends ConstraintAttribute>)constraint.mCustomConstraints.clone());
                        }
                    }
                }
                else {
                    constraint.mDelta.applyDelta(this.getConstraint(constraint.mViewId));
                }
            }
        }
    }
    
    public void applyTo(final ConstraintLayout constraintLayout) {
        this.applyToInternal(constraintLayout, true);
        constraintLayout.setConstraintSet(null);
        constraintLayout.requestLayout();
    }
    
    public void applyToHelper(final ConstraintHelper constraintHelper, final ConstraintWidget constraintWidget, final ConstraintLayout.LayoutParams layoutParams, final SparseArray<ConstraintWidget> sparseArray) {
        final int id = constraintHelper.getId();
        if (this.mConstraints.containsKey(id)) {
            final Constraint constraint = this.mConstraints.get(id);
            if (constraint != null && constraintWidget instanceof HelperWidget) {
                constraintHelper.loadParameters(constraint, (HelperWidget)constraintWidget, layoutParams, sparseArray);
            }
        }
    }
    
    void applyToInternal(final ConstraintLayout constraintLayout, final boolean b) {
        final int childCount = constraintLayout.getChildCount();
        final HashSet set = new HashSet((Collection<? extends E>)this.mConstraints.keySet());
        final int n = 0;
        for (int i = 0; i < childCount; ++i) {
            final View child = constraintLayout.getChildAt(i);
            final int id = child.getId();
            if (!this.mConstraints.containsKey(id)) {
                final StringBuilder sb = new StringBuilder();
                sb.append("id unknown ");
                sb.append(Debug.getName(child));
            }
            else {
                if (this.mForceId && id == -1) {
                    throw new RuntimeException("All children of ConstraintLayout must have ids to use ConstraintSet");
                }
                if (id != -1) {
                    if (this.mConstraints.containsKey(id)) {
                        set.remove(id);
                        final Constraint constraint = this.mConstraints.get(id);
                        if (constraint != null) {
                            if (child instanceof Barrier) {
                                constraint.layout.mHelperType = 1;
                                final Barrier barrier = (Barrier)child;
                                barrier.setId(id);
                                barrier.setType(constraint.layout.mBarrierDirection);
                                barrier.setMargin(constraint.layout.mBarrierMargin);
                                barrier.setAllowsGoneWidget(constraint.layout.mBarrierAllowsGoneWidgets);
                                final Layout layout = constraint.layout;
                                final int[] mReferenceIds = layout.mReferenceIds;
                                if (mReferenceIds != null) {
                                    barrier.setReferencedIds(mReferenceIds);
                                }
                                else {
                                    final String mReferenceIdString = layout.mReferenceIdString;
                                    if (mReferenceIdString != null) {
                                        layout.mReferenceIds = this.convertReferenceString(barrier, mReferenceIdString);
                                        barrier.setReferencedIds(constraint.layout.mReferenceIds);
                                    }
                                }
                            }
                            final ConstraintLayout.LayoutParams layoutParams = (ConstraintLayout.LayoutParams)child.getLayoutParams();
                            layoutParams.validate();
                            constraint.applyTo(layoutParams);
                            if (b) {
                                ConstraintAttribute.setAttributes(child, constraint.mCustomConstraints);
                            }
                            child.setLayoutParams((ViewGroup$LayoutParams)layoutParams);
                            final PropertySet propertySet = constraint.propertySet;
                            if (propertySet.mVisibilityMode == 0) {
                                child.setVisibility(propertySet.visibility);
                            }
                            child.setAlpha(constraint.propertySet.alpha);
                            child.setRotation(constraint.transform.rotation);
                            child.setRotationX(constraint.transform.rotationX);
                            child.setRotationY(constraint.transform.rotationY);
                            child.setScaleX(constraint.transform.scaleX);
                            child.setScaleY(constraint.transform.scaleY);
                            final Transform transform = constraint.transform;
                            if (transform.transformPivotTarget != -1) {
                                final View viewById = ((View)child.getParent()).findViewById(constraint.transform.transformPivotTarget);
                                if (viewById != null) {
                                    final float n2 = (viewById.getTop() + viewById.getBottom()) / 2.0f;
                                    final float n3 = (viewById.getLeft() + viewById.getRight()) / 2.0f;
                                    if (child.getRight() - child.getLeft() > 0 && child.getBottom() - child.getTop() > 0) {
                                        final float n4 = (float)child.getLeft();
                                        final float n5 = (float)child.getTop();
                                        child.setPivotX(n3 - n4);
                                        child.setPivotY(n2 - n5);
                                    }
                                }
                            }
                            else {
                                if (!Float.isNaN(transform.transformPivotX)) {
                                    child.setPivotX(constraint.transform.transformPivotX);
                                }
                                if (!Float.isNaN(constraint.transform.transformPivotY)) {
                                    child.setPivotY(constraint.transform.transformPivotY);
                                }
                            }
                            child.setTranslationX(constraint.transform.translationX);
                            child.setTranslationY(constraint.transform.translationY);
                            child.setTranslationZ(constraint.transform.translationZ);
                            final Transform transform2 = constraint.transform;
                            if (transform2.applyElevation) {
                                child.setElevation(transform2.elevation);
                            }
                        }
                    }
                    else {
                        final StringBuilder sb2 = new StringBuilder();
                        sb2.append("WARNING NO CONSTRAINTS for view ");
                        sb2.append(id);
                    }
                }
            }
        }
        final Iterator iterator = set.iterator();
        int j;
        while (true) {
            j = n;
            if (!iterator.hasNext()) {
                break;
            }
            final Integer key = (Integer)iterator.next();
            final Constraint constraint2 = this.mConstraints.get(key);
            if (constraint2 == null) {
                continue;
            }
            if (constraint2.layout.mHelperType == 1) {
                final Barrier barrier2 = new Barrier(((View)constraintLayout).getContext());
                barrier2.setId((int)key);
                final Layout layout2 = constraint2.layout;
                final int[] mReferenceIds2 = layout2.mReferenceIds;
                if (mReferenceIds2 != null) {
                    barrier2.setReferencedIds(mReferenceIds2);
                }
                else {
                    final String mReferenceIdString2 = layout2.mReferenceIdString;
                    if (mReferenceIdString2 != null) {
                        layout2.mReferenceIds = this.convertReferenceString(barrier2, mReferenceIdString2);
                        barrier2.setReferencedIds(constraint2.layout.mReferenceIds);
                    }
                }
                barrier2.setType(constraint2.layout.mBarrierDirection);
                barrier2.setMargin(constraint2.layout.mBarrierMargin);
                final ConstraintLayout.LayoutParams generateDefaultLayoutParams = constraintLayout.generateDefaultLayoutParams();
                barrier2.validateParams();
                constraint2.applyTo(generateDefaultLayoutParams);
                constraintLayout.addView((View)barrier2, (ViewGroup$LayoutParams)generateDefaultLayoutParams);
            }
            if (!constraint2.layout.mIsGuideline) {
                continue;
            }
            final Guideline guideline = new Guideline(((View)constraintLayout).getContext());
            guideline.setId((int)key);
            final ConstraintLayout.LayoutParams generateDefaultLayoutParams2 = constraintLayout.generateDefaultLayoutParams();
            constraint2.applyTo(generateDefaultLayoutParams2);
            constraintLayout.addView((View)guideline, (ViewGroup$LayoutParams)generateDefaultLayoutParams2);
        }
        while (j < childCount) {
            final View child2 = constraintLayout.getChildAt(j);
            if (child2 instanceof ConstraintHelper) {
                ((ConstraintHelper)child2).applyLayoutFeaturesInConstraintSet(constraintLayout);
            }
            ++j;
        }
    }
    
    public void applyToLayoutParams(final int n, final ConstraintLayout.LayoutParams layoutParams) {
        if (this.mConstraints.containsKey(n)) {
            final Constraint constraint = this.mConstraints.get(n);
            if (constraint != null) {
                constraint.applyTo(layoutParams);
            }
        }
    }
    
    public void applyToWithoutCustom(final ConstraintLayout constraintLayout) {
        this.applyToInternal(constraintLayout, false);
        constraintLayout.setConstraintSet(null);
    }
    
    public void center(final int i, final int n, final int n2, final int n3, final int n4, final int n5, final int n6, final float horizontalBias) {
        if (n3 < 0) {
            throw new IllegalArgumentException("margin must be > 0");
        }
        if (n6 < 0) {
            throw new IllegalArgumentException("margin must be > 0");
        }
        if (horizontalBias > 0.0f && horizontalBias <= 1.0f) {
            if (n2 != 1 && n2 != 2) {
                if (n2 != 6 && n2 != 7) {
                    this.connect(i, 3, n, n2, n3);
                    this.connect(i, 4, n4, n5, n6);
                    final Constraint constraint = this.mConstraints.get(i);
                    if (constraint != null) {
                        constraint.layout.verticalBias = horizontalBias;
                    }
                }
                else {
                    this.connect(i, 6, n, n2, n3);
                    this.connect(i, 7, n4, n5, n6);
                    final Constraint constraint2 = this.mConstraints.get(i);
                    if (constraint2 != null) {
                        constraint2.layout.horizontalBias = horizontalBias;
                    }
                }
            }
            else {
                this.connect(i, 1, n, n2, n3);
                this.connect(i, 2, n4, n5, n6);
                final Constraint constraint3 = this.mConstraints.get(i);
                if (constraint3 != null) {
                    constraint3.layout.horizontalBias = horizontalBias;
                }
            }
            return;
        }
        throw new IllegalArgumentException("bias must be between 0 and 1 inclusive");
    }
    
    public void centerHorizontally(final int n, final int n2) {
        if (n2 == 0) {
            this.center(n, 0, 1, 0, 0, 2, 0, 0.5f);
        }
        else {
            this.center(n, n2, 2, 0, n2, 1, 0, 0.5f);
        }
    }
    
    public void centerHorizontally(final int i, final int n, final int n2, final int n3, final int n4, final int n5, final int n6, final float horizontalBias) {
        this.connect(i, 1, n, n2, n3);
        this.connect(i, 2, n4, n5, n6);
        final Constraint constraint = this.mConstraints.get(i);
        if (constraint != null) {
            constraint.layout.horizontalBias = horizontalBias;
        }
    }
    
    public void centerHorizontallyRtl(final int n, final int n2) {
        if (n2 == 0) {
            this.center(n, 0, 6, 0, 0, 7, 0, 0.5f);
        }
        else {
            this.center(n, n2, 7, 0, n2, 6, 0, 0.5f);
        }
    }
    
    public void centerHorizontallyRtl(final int i, final int n, final int n2, final int n3, final int n4, final int n5, final int n6, final float horizontalBias) {
        this.connect(i, 6, n, n2, n3);
        this.connect(i, 7, n4, n5, n6);
        final Constraint constraint = this.mConstraints.get(i);
        if (constraint != null) {
            constraint.layout.horizontalBias = horizontalBias;
        }
    }
    
    public void centerVertically(final int n, final int n2) {
        if (n2 == 0) {
            this.center(n, 0, 3, 0, 0, 4, 0, 0.5f);
        }
        else {
            this.center(n, n2, 4, 0, n2, 3, 0, 0.5f);
        }
    }
    
    public void centerVertically(final int i, final int n, final int n2, final int n3, final int n4, final int n5, final int n6, final float verticalBias) {
        this.connect(i, 3, n, n2, n3);
        this.connect(i, 4, n4, n5, n6);
        final Constraint constraint = this.mConstraints.get(i);
        if (constraint != null) {
            constraint.layout.verticalBias = verticalBias;
        }
    }
    
    public void clear(final int i) {
        this.mConstraints.remove(i);
    }
    
    public void clear(final int n, final int n2) {
        if (this.mConstraints.containsKey(n)) {
            final Constraint constraint = this.mConstraints.get(n);
            if (constraint == null) {
                return;
            }
            switch (n2) {
                default: {
                    throw new IllegalArgumentException("unknown constraint");
                }
                case 8: {
                    final Layout layout = constraint.layout;
                    layout.circleAngle = -1.0f;
                    layout.circleRadius = -1;
                    layout.circleConstraint = -1;
                    break;
                }
                case 7: {
                    final Layout layout2 = constraint.layout;
                    layout2.endToStart = -1;
                    layout2.endToEnd = -1;
                    layout2.endMargin = 0;
                    layout2.goneEndMargin = Integer.MIN_VALUE;
                    break;
                }
                case 6: {
                    final Layout layout3 = constraint.layout;
                    layout3.startToEnd = -1;
                    layout3.startToStart = -1;
                    layout3.startMargin = 0;
                    layout3.goneStartMargin = Integer.MIN_VALUE;
                    break;
                }
                case 5: {
                    final Layout layout4 = constraint.layout;
                    layout4.baselineToBaseline = -1;
                    layout4.baselineToTop = -1;
                    layout4.baselineToBottom = -1;
                    layout4.baselineMargin = 0;
                    layout4.goneBaselineMargin = Integer.MIN_VALUE;
                    break;
                }
                case 4: {
                    final Layout layout5 = constraint.layout;
                    layout5.bottomToTop = -1;
                    layout5.bottomToBottom = -1;
                    layout5.bottomMargin = 0;
                    layout5.goneBottomMargin = Integer.MIN_VALUE;
                    break;
                }
                case 3: {
                    final Layout layout6 = constraint.layout;
                    layout6.topToBottom = -1;
                    layout6.topToTop = -1;
                    layout6.topMargin = 0;
                    layout6.goneTopMargin = Integer.MIN_VALUE;
                    break;
                }
                case 2: {
                    final Layout layout7 = constraint.layout;
                    layout7.rightToRight = -1;
                    layout7.rightToLeft = -1;
                    layout7.rightMargin = -1;
                    layout7.goneRightMargin = Integer.MIN_VALUE;
                    break;
                }
                case 1: {
                    final Layout layout8 = constraint.layout;
                    layout8.leftToRight = -1;
                    layout8.leftToLeft = -1;
                    layout8.leftMargin = -1;
                    layout8.goneLeftMargin = Integer.MIN_VALUE;
                    break;
                }
            }
        }
    }
    
    public void clone(final Context context, final int n) {
        this.clone((ConstraintLayout)LayoutInflater.from(context).inflate(n, (ViewGroup)null));
    }
    
    public void clone(final ConstraintLayout constraintLayout) {
        final int childCount = constraintLayout.getChildCount();
        this.mConstraints.clear();
        for (int i = 0; i < childCount; ++i) {
            final View child = constraintLayout.getChildAt(i);
            final ConstraintLayout.LayoutParams layoutParams = (ConstraintLayout.LayoutParams)child.getLayoutParams();
            final int id = child.getId();
            if (this.mForceId && id == -1) {
                throw new RuntimeException("All children of ConstraintLayout must have ids to use ConstraintSet");
            }
            if (!this.mConstraints.containsKey(id)) {
                this.mConstraints.put(id, new Constraint());
            }
            final Constraint constraint = this.mConstraints.get(id);
            if (constraint != null) {
                constraint.mCustomConstraints = ConstraintAttribute.extractAttributes(this.mSavedAttributes, child);
                constraint.fillFrom(id, layoutParams);
                constraint.propertySet.visibility = child.getVisibility();
                constraint.propertySet.alpha = child.getAlpha();
                constraint.transform.rotation = child.getRotation();
                constraint.transform.rotationX = child.getRotationX();
                constraint.transform.rotationY = child.getRotationY();
                constraint.transform.scaleX = child.getScaleX();
                constraint.transform.scaleY = child.getScaleY();
                final float pivotX = child.getPivotX();
                final float pivotY = child.getPivotY();
                if (pivotX != 0.0 || pivotY != 0.0) {
                    final Transform transform = constraint.transform;
                    transform.transformPivotX = pivotX;
                    transform.transformPivotY = pivotY;
                }
                constraint.transform.translationX = child.getTranslationX();
                constraint.transform.translationY = child.getTranslationY();
                constraint.transform.translationZ = child.getTranslationZ();
                final Transform transform2 = constraint.transform;
                if (transform2.applyElevation) {
                    transform2.elevation = child.getElevation();
                }
                if (child instanceof Barrier) {
                    final Barrier barrier = (Barrier)child;
                    constraint.layout.mBarrierAllowsGoneWidgets = barrier.getAllowsGoneWidget();
                    constraint.layout.mReferenceIds = barrier.getReferencedIds();
                    constraint.layout.mBarrierDirection = barrier.getType();
                    constraint.layout.mBarrierMargin = barrier.getMargin();
                }
            }
        }
    }
    
    public void clone(final ConstraintSet set) {
        this.mConstraints.clear();
        for (final Integer n : set.mConstraints.keySet()) {
            final Constraint constraint = set.mConstraints.get(n);
            if (constraint == null) {
                continue;
            }
            this.mConstraints.put(n, constraint.clone());
        }
    }
    
    public void clone(final Constraints constraints) {
        final int childCount = constraints.getChildCount();
        this.mConstraints.clear();
        for (int i = 0; i < childCount; ++i) {
            final View child = constraints.getChildAt(i);
            final Constraints.LayoutParams layoutParams = (Constraints.LayoutParams)child.getLayoutParams();
            final int id = child.getId();
            if (this.mForceId && id == -1) {
                throw new RuntimeException("All children of ConstraintLayout must have ids to use ConstraintSet");
            }
            if (!this.mConstraints.containsKey(id)) {
                this.mConstraints.put(id, new Constraint());
            }
            final Constraint constraint = this.mConstraints.get(id);
            if (constraint != null) {
                if (child instanceof ConstraintHelper) {
                    constraint.fillFromConstraints((ConstraintHelper)child, id, layoutParams);
                }
                constraint.fillFromConstraints(id, layoutParams);
            }
        }
    }
    
    public void connect(final int i, final int n, final int leftToRight, final int n2) {
        if (!this.mConstraints.containsKey(i)) {
            this.mConstraints.put(i, new Constraint());
        }
        final Constraint constraint = this.mConstraints.get(i);
        if (constraint == null) {
            return;
        }
        switch (n) {
            default: {
                final StringBuilder sb = new StringBuilder();
                sb.append(this.sideToString(n));
                sb.append(" to ");
                sb.append(this.sideToString(n2));
                sb.append(" unknown");
                throw new IllegalArgumentException(sb.toString());
            }
            case 7: {
                if (n2 == 7) {
                    final Layout layout = constraint.layout;
                    layout.endToEnd = leftToRight;
                    layout.endToStart = -1;
                    break;
                }
                if (n2 == 6) {
                    final Layout layout2 = constraint.layout;
                    layout2.endToStart = leftToRight;
                    layout2.endToEnd = -1;
                    break;
                }
                final StringBuilder sb2 = new StringBuilder();
                sb2.append("right to ");
                sb2.append(this.sideToString(n2));
                sb2.append(" undefined");
                throw new IllegalArgumentException(sb2.toString());
            }
            case 6: {
                if (n2 == 6) {
                    final Layout layout3 = constraint.layout;
                    layout3.startToStart = leftToRight;
                    layout3.startToEnd = -1;
                    break;
                }
                if (n2 == 7) {
                    final Layout layout4 = constraint.layout;
                    layout4.startToEnd = leftToRight;
                    layout4.startToStart = -1;
                    break;
                }
                final StringBuilder sb3 = new StringBuilder();
                sb3.append("right to ");
                sb3.append(this.sideToString(n2));
                sb3.append(" undefined");
                throw new IllegalArgumentException(sb3.toString());
            }
            case 5: {
                if (n2 == 5) {
                    final Layout layout5 = constraint.layout;
                    layout5.baselineToBaseline = leftToRight;
                    layout5.bottomToBottom = -1;
                    layout5.bottomToTop = -1;
                    layout5.topToTop = -1;
                    layout5.topToBottom = -1;
                    break;
                }
                if (n2 == 3) {
                    final Layout layout6 = constraint.layout;
                    layout6.baselineToTop = leftToRight;
                    layout6.bottomToBottom = -1;
                    layout6.bottomToTop = -1;
                    layout6.topToTop = -1;
                    layout6.topToBottom = -1;
                    break;
                }
                if (n2 == 4) {
                    final Layout layout7 = constraint.layout;
                    layout7.baselineToBottom = leftToRight;
                    layout7.bottomToBottom = -1;
                    layout7.bottomToTop = -1;
                    layout7.topToTop = -1;
                    layout7.topToBottom = -1;
                    break;
                }
                final StringBuilder sb4 = new StringBuilder();
                sb4.append("right to ");
                sb4.append(this.sideToString(n2));
                sb4.append(" undefined");
                throw new IllegalArgumentException(sb4.toString());
            }
            case 4: {
                if (n2 == 4) {
                    final Layout layout8 = constraint.layout;
                    layout8.bottomToBottom = leftToRight;
                    layout8.bottomToTop = -1;
                    layout8.baselineToBaseline = -1;
                    layout8.baselineToTop = -1;
                    layout8.baselineToBottom = -1;
                    break;
                }
                if (n2 == 3) {
                    final Layout layout9 = constraint.layout;
                    layout9.bottomToTop = leftToRight;
                    layout9.bottomToBottom = -1;
                    layout9.baselineToBaseline = -1;
                    layout9.baselineToTop = -1;
                    layout9.baselineToBottom = -1;
                    break;
                }
                final StringBuilder sb5 = new StringBuilder();
                sb5.append("right to ");
                sb5.append(this.sideToString(n2));
                sb5.append(" undefined");
                throw new IllegalArgumentException(sb5.toString());
            }
            case 3: {
                if (n2 == 3) {
                    final Layout layout10 = constraint.layout;
                    layout10.topToTop = leftToRight;
                    layout10.topToBottom = -1;
                    layout10.baselineToBaseline = -1;
                    layout10.baselineToTop = -1;
                    layout10.baselineToBottom = -1;
                    break;
                }
                if (n2 == 4) {
                    final Layout layout11 = constraint.layout;
                    layout11.topToBottom = leftToRight;
                    layout11.topToTop = -1;
                    layout11.baselineToBaseline = -1;
                    layout11.baselineToTop = -1;
                    layout11.baselineToBottom = -1;
                    break;
                }
                final StringBuilder sb6 = new StringBuilder();
                sb6.append("right to ");
                sb6.append(this.sideToString(n2));
                sb6.append(" undefined");
                throw new IllegalArgumentException(sb6.toString());
            }
            case 2: {
                if (n2 == 1) {
                    final Layout layout12 = constraint.layout;
                    layout12.rightToLeft = leftToRight;
                    layout12.rightToRight = -1;
                    break;
                }
                if (n2 == 2) {
                    final Layout layout13 = constraint.layout;
                    layout13.rightToRight = leftToRight;
                    layout13.rightToLeft = -1;
                    break;
                }
                final StringBuilder sb7 = new StringBuilder();
                sb7.append("right to ");
                sb7.append(this.sideToString(n2));
                sb7.append(" undefined");
                throw new IllegalArgumentException(sb7.toString());
            }
            case 1: {
                if (n2 == 1) {
                    final Layout layout14 = constraint.layout;
                    layout14.leftToLeft = leftToRight;
                    layout14.leftToRight = -1;
                    break;
                }
                if (n2 == 2) {
                    final Layout layout15 = constraint.layout;
                    layout15.leftToRight = leftToRight;
                    layout15.leftToLeft = -1;
                    break;
                }
                final StringBuilder sb8 = new StringBuilder();
                sb8.append("left to ");
                sb8.append(this.sideToString(n2));
                sb8.append(" undefined");
                throw new IllegalArgumentException(sb8.toString());
            }
        }
    }
    
    public void connect(final int i, final int n, final int leftToRight, final int n2, final int n3) {
        if (!this.mConstraints.containsKey(i)) {
            this.mConstraints.put(i, new Constraint());
        }
        final Constraint constraint = this.mConstraints.get(i);
        if (constraint == null) {
            return;
        }
        switch (n) {
            default: {
                final StringBuilder sb = new StringBuilder();
                sb.append(this.sideToString(n));
                sb.append(" to ");
                sb.append(this.sideToString(n2));
                sb.append(" unknown");
                throw new IllegalArgumentException(sb.toString());
            }
            case 7: {
                if (n2 == 7) {
                    final Layout layout = constraint.layout;
                    layout.endToEnd = leftToRight;
                    layout.endToStart = -1;
                }
                else {
                    if (n2 != 6) {
                        final StringBuilder sb2 = new StringBuilder();
                        sb2.append("right to ");
                        sb2.append(this.sideToString(n2));
                        sb2.append(" undefined");
                        throw new IllegalArgumentException(sb2.toString());
                    }
                    final Layout layout2 = constraint.layout;
                    layout2.endToStart = leftToRight;
                    layout2.endToEnd = -1;
                }
                constraint.layout.endMargin = n3;
                break;
            }
            case 6: {
                if (n2 == 6) {
                    final Layout layout3 = constraint.layout;
                    layout3.startToStart = leftToRight;
                    layout3.startToEnd = -1;
                }
                else {
                    if (n2 != 7) {
                        final StringBuilder sb3 = new StringBuilder();
                        sb3.append("right to ");
                        sb3.append(this.sideToString(n2));
                        sb3.append(" undefined");
                        throw new IllegalArgumentException(sb3.toString());
                    }
                    final Layout layout4 = constraint.layout;
                    layout4.startToEnd = leftToRight;
                    layout4.startToStart = -1;
                }
                constraint.layout.startMargin = n3;
                break;
            }
            case 5: {
                if (n2 == 5) {
                    final Layout layout5 = constraint.layout;
                    layout5.baselineToBaseline = leftToRight;
                    layout5.bottomToBottom = -1;
                    layout5.bottomToTop = -1;
                    layout5.topToTop = -1;
                    layout5.topToBottom = -1;
                    break;
                }
                if (n2 == 3) {
                    final Layout layout6 = constraint.layout;
                    layout6.baselineToTop = leftToRight;
                    layout6.bottomToBottom = -1;
                    layout6.bottomToTop = -1;
                    layout6.topToTop = -1;
                    layout6.topToBottom = -1;
                    break;
                }
                if (n2 == 4) {
                    final Layout layout7 = constraint.layout;
                    layout7.baselineToBottom = leftToRight;
                    layout7.bottomToBottom = -1;
                    layout7.bottomToTop = -1;
                    layout7.topToTop = -1;
                    layout7.topToBottom = -1;
                    break;
                }
                final StringBuilder sb4 = new StringBuilder();
                sb4.append("right to ");
                sb4.append(this.sideToString(n2));
                sb4.append(" undefined");
                throw new IllegalArgumentException(sb4.toString());
            }
            case 4: {
                if (n2 == 4) {
                    final Layout layout8 = constraint.layout;
                    layout8.bottomToBottom = leftToRight;
                    layout8.bottomToTop = -1;
                    layout8.baselineToBaseline = -1;
                    layout8.baselineToTop = -1;
                    layout8.baselineToBottom = -1;
                }
                else {
                    if (n2 != 3) {
                        final StringBuilder sb5 = new StringBuilder();
                        sb5.append("right to ");
                        sb5.append(this.sideToString(n2));
                        sb5.append(" undefined");
                        throw new IllegalArgumentException(sb5.toString());
                    }
                    final Layout layout9 = constraint.layout;
                    layout9.bottomToTop = leftToRight;
                    layout9.bottomToBottom = -1;
                    layout9.baselineToBaseline = -1;
                    layout9.baselineToTop = -1;
                    layout9.baselineToBottom = -1;
                }
                constraint.layout.bottomMargin = n3;
                break;
            }
            case 3: {
                if (n2 == 3) {
                    final Layout layout10 = constraint.layout;
                    layout10.topToTop = leftToRight;
                    layout10.topToBottom = -1;
                    layout10.baselineToBaseline = -1;
                    layout10.baselineToTop = -1;
                    layout10.baselineToBottom = -1;
                }
                else {
                    if (n2 != 4) {
                        final StringBuilder sb6 = new StringBuilder();
                        sb6.append("right to ");
                        sb6.append(this.sideToString(n2));
                        sb6.append(" undefined");
                        throw new IllegalArgumentException(sb6.toString());
                    }
                    final Layout layout11 = constraint.layout;
                    layout11.topToBottom = leftToRight;
                    layout11.topToTop = -1;
                    layout11.baselineToBaseline = -1;
                    layout11.baselineToTop = -1;
                    layout11.baselineToBottom = -1;
                }
                constraint.layout.topMargin = n3;
                break;
            }
            case 2: {
                if (n2 == 1) {
                    final Layout layout12 = constraint.layout;
                    layout12.rightToLeft = leftToRight;
                    layout12.rightToRight = -1;
                }
                else {
                    if (n2 != 2) {
                        final StringBuilder sb7 = new StringBuilder();
                        sb7.append("right to ");
                        sb7.append(this.sideToString(n2));
                        sb7.append(" undefined");
                        throw new IllegalArgumentException(sb7.toString());
                    }
                    final Layout layout13 = constraint.layout;
                    layout13.rightToRight = leftToRight;
                    layout13.rightToLeft = -1;
                }
                constraint.layout.rightMargin = n3;
                break;
            }
            case 1: {
                if (n2 == 1) {
                    final Layout layout14 = constraint.layout;
                    layout14.leftToLeft = leftToRight;
                    layout14.leftToRight = -1;
                }
                else {
                    if (n2 != 2) {
                        final StringBuilder sb8 = new StringBuilder();
                        sb8.append("Left to ");
                        sb8.append(this.sideToString(n2));
                        sb8.append(" undefined");
                        throw new IllegalArgumentException(sb8.toString());
                    }
                    final Layout layout15 = constraint.layout;
                    layout15.leftToRight = leftToRight;
                    layout15.leftToLeft = -1;
                }
                constraint.layout.leftMargin = n3;
                break;
            }
        }
    }
    
    public void constrainCircle(final int n, final int circleConstraint, final int circleRadius, final float circleAngle) {
        final Layout layout = this.get(n).layout;
        layout.circleConstraint = circleConstraint;
        layout.circleRadius = circleRadius;
        layout.circleAngle = circleAngle;
    }
    
    public void constrainDefaultHeight(final int n, final int heightDefault) {
        this.get(n).layout.heightDefault = heightDefault;
    }
    
    public void constrainDefaultWidth(final int n, final int widthDefault) {
        this.get(n).layout.widthDefault = widthDefault;
    }
    
    public void constrainHeight(final int n, final int mHeight) {
        this.get(n).layout.mHeight = mHeight;
    }
    
    public void constrainMaxHeight(final int n, final int heightMax) {
        this.get(n).layout.heightMax = heightMax;
    }
    
    public void constrainMaxWidth(final int n, final int widthMax) {
        this.get(n).layout.widthMax = widthMax;
    }
    
    public void constrainMinHeight(final int n, final int heightMin) {
        this.get(n).layout.heightMin = heightMin;
    }
    
    public void constrainMinWidth(final int n, final int widthMin) {
        this.get(n).layout.widthMin = widthMin;
    }
    
    public void constrainPercentHeight(final int n, final float heightPercent) {
        this.get(n).layout.heightPercent = heightPercent;
    }
    
    public void constrainPercentWidth(final int n, final float widthPercent) {
        this.get(n).layout.widthPercent = widthPercent;
    }
    
    public void constrainWidth(final int n, final int mWidth) {
        this.get(n).layout.mWidth = mWidth;
    }
    
    public void constrainedHeight(final int n, final boolean constrainedHeight) {
        this.get(n).layout.constrainedHeight = constrainedHeight;
    }
    
    public void constrainedWidth(final int n, final boolean constrainedWidth) {
        this.get(n).layout.constrainedWidth = constrainedWidth;
    }
    
    public void create(final int n, final int orientation) {
        final Layout layout = this.get(n).layout;
        layout.mIsGuideline = true;
        layout.orientation = orientation;
    }
    
    public void createBarrier(final int n, final int mBarrierDirection, final int mBarrierMargin, final int... mReferenceIds) {
        final Layout layout = this.get(n).layout;
        layout.mHelperType = 1;
        layout.mBarrierDirection = mBarrierDirection;
        layout.mBarrierMargin = mBarrierMargin;
        layout.mIsGuideline = false;
        layout.mReferenceIds = mReferenceIds;
    }
    
    public void createHorizontalChain(final int n, final int n2, final int n3, final int n4, final int[] array, final float[] array2, final int n5) {
        this.createHorizontalChain(n, n2, n3, n4, array, array2, n5, 1, 2);
    }
    
    public void createHorizontalChainRtl(final int n, final int n2, final int n3, final int n4, final int[] array, final float[] array2, final int n5) {
        this.createHorizontalChain(n, n2, n3, n4, array, array2, n5, 6, 7);
    }
    
    public void createVerticalChain(int i, int n, final int n2, final int n3, final int[] array, final float[] array2, int verticalChainStyle) {
        if (array.length < 2) {
            throw new IllegalArgumentException("must have 2 or more widgets in a chain");
        }
        if (array2 != null && array2.length != array.length) {
            throw new IllegalArgumentException("must have 2 or more widgets in a chain");
        }
        if (array2 != null) {
            this.get(array[0]).layout.verticalWeight = array2[0];
        }
        this.get(array[0]).layout.verticalChainStyle = verticalChainStyle;
        this.connect(array[0], 3, i, n, 0);
        for (i = 1; i < array.length; ++i) {
            verticalChainStyle = array[i];
            n = i - 1;
            this.connect(verticalChainStyle, 3, array[n], 4, 0);
            this.connect(array[n], 4, array[i], 3, 0);
            if (array2 != null) {
                this.get(array[i]).layout.verticalWeight = array2[i];
            }
        }
        this.connect(array[array.length - 1], 4, n2, n3, 0);
    }
    
    public void dump(final MotionScene motionScene, final int... array) {
        final Set<Integer> keySet = this.mConstraints.keySet();
        final int length = array.length;
        final int n = 0;
        HashSet set2;
        if (length != 0) {
            final HashSet set = new HashSet();
            final int length2 = array.length;
            int n2 = 0;
            while (true) {
                set2 = set;
                if (n2 >= length2) {
                    break;
                }
                set.add(array[n2]);
                ++n2;
            }
        }
        else {
            set2 = new HashSet((Collection<? extends E>)keySet);
        }
        final PrintStream out = System.out;
        final StringBuilder sb = new StringBuilder();
        sb.append(set2.size());
        sb.append(" constraints");
        out.println(sb.toString());
        final StringBuilder sb2 = new StringBuilder();
        final Integer[] array2 = (Integer[])set2.toArray(new Integer[0]);
        for (int length3 = array2.length, i = n; i < length3; ++i) {
            final Integer n3 = array2[i];
            final Constraint constraint = this.mConstraints.get(n3);
            if (constraint != null) {
                sb2.append("<Constraint id=");
                sb2.append(n3);
                sb2.append(" \n");
                constraint.layout.dump(motionScene, sb2);
                sb2.append("/>\n");
            }
        }
        System.out.println(sb2.toString());
    }
    
    public boolean getApplyElevation(final int n) {
        return this.get(n).transform.applyElevation;
    }
    
    public Constraint getConstraint(final int n) {
        if (this.mConstraints.containsKey(n)) {
            return this.mConstraints.get(n);
        }
        return null;
    }
    
    public HashMap<String, ConstraintAttribute> getCustomAttributeSet() {
        return this.mSavedAttributes;
    }
    
    public int getHeight(final int n) {
        return this.get(n).layout.mHeight;
    }
    
    public int[] getKnownIds() {
        final Set<Integer> keySet = this.mConstraints.keySet();
        int i = 0;
        final Integer[] array = keySet.toArray(new Integer[0]);
        final int length = array.length;
        final int[] array2 = new int[length];
        while (i < length) {
            array2[i] = array[i];
            ++i;
        }
        return array2;
    }
    
    public Constraint getParameters(final int n) {
        return this.get(n);
    }
    
    public int[] getReferencedIds(final int n) {
        final int[] mReferenceIds = this.get(n).layout.mReferenceIds;
        if (mReferenceIds == null) {
            return new int[0];
        }
        return Arrays.copyOf(mReferenceIds, mReferenceIds.length);
    }
    
    public int getVisibility(final int n) {
        return this.get(n).propertySet.visibility;
    }
    
    public int getVisibilityMode(final int n) {
        return this.get(n).propertySet.mVisibilityMode;
    }
    
    public int getWidth(final int n) {
        return this.get(n).layout.mWidth;
    }
    
    public boolean isForceId() {
        return this.mForceId;
    }
    
    public void load(final Context context, int i) {
        final XmlResourceParser xml = context.getResources().getXml(i);
        try {
            String name;
            Constraint fillFromAttributeList;
            for (i = ((XmlPullParser)xml).getEventType(); i != 1; i = ((XmlPullParser)xml).next()) {
                if (i != 0) {
                    if (i == 2) {
                        name = ((XmlPullParser)xml).getName();
                        fillFromAttributeList = this.fillFromAttributeList(context, Xml.asAttributeSet((XmlPullParser)xml), false);
                        if (name.equalsIgnoreCase("Guideline")) {
                            fillFromAttributeList.layout.mIsGuideline = true;
                        }
                        this.mConstraints.put(fillFromAttributeList.mViewId, fillFromAttributeList);
                    }
                }
                else {
                    ((XmlPullParser)xml).getName();
                }
            }
        }
        catch (final IOException ex) {
            ex.printStackTrace();
        }
        catch (final XmlPullParserException ex2) {
            ex2.printStackTrace();
        }
    }
    
    public void load(final Context context, final XmlPullParser xmlPullParser) {
        try {
            int i = xmlPullParser.getEventType();
            Constraint value = null;
            Label_0863: {
                Label_0797: {
                    Label_0731: {
                        Label_0665: {
                            Label_0601: {
                                while (i != 1) {
                                    if (i != 0) {
                                        final int n = -1;
                                        if (i != 2) {
                                            if (i == 3) {
                                                final String lowerCase = xmlPullParser.getName().toLowerCase(Locale.ROOT);
                                                int n2 = 0;
                                                switch (lowerCase.hashCode()) {
                                                    default: {
                                                        n2 = n;
                                                        break;
                                                    }
                                                    case 2146106725: {
                                                        n2 = n;
                                                        if (lowerCase.equals("constraintset")) {
                                                            n2 = 0;
                                                            break;
                                                        }
                                                        break;
                                                    }
                                                    case 426575017: {
                                                        n2 = n;
                                                        if (lowerCase.equals("constraintoverride")) {
                                                            n2 = 2;
                                                            break;
                                                        }
                                                        break;
                                                    }
                                                    case -190376483: {
                                                        n2 = n;
                                                        if (lowerCase.equals("constraint")) {
                                                            n2 = 1;
                                                            break;
                                                        }
                                                        break;
                                                    }
                                                    case -2075718416: {
                                                        n2 = n;
                                                        if (lowerCase.equals("guideline")) {
                                                            n2 = 3;
                                                            break;
                                                        }
                                                        break;
                                                    }
                                                }
                                                if (n2 == 0) {
                                                    return;
                                                }
                                                if (n2 == 1 || n2 == 2 || n2 == 3) {
                                                    this.mConstraints.put(value.mViewId, value);
                                                    value = null;
                                                }
                                            }
                                        }
                                        else {
                                            final String name = xmlPullParser.getName();
                                            int n3 = 0;
                                            switch (name.hashCode()) {
                                                default: {
                                                    n3 = n;
                                                    break;
                                                }
                                                case 1803088381: {
                                                    n3 = n;
                                                    if (name.equals("Constraint")) {
                                                        n3 = 0;
                                                        break;
                                                    }
                                                    break;
                                                }
                                                case 1791837707: {
                                                    n3 = n;
                                                    if (name.equals("CustomAttribute")) {
                                                        n3 = 8;
                                                        break;
                                                    }
                                                    break;
                                                }
                                                case 1331510167: {
                                                    n3 = n;
                                                    if (name.equals("Barrier")) {
                                                        n3 = 3;
                                                        break;
                                                    }
                                                    break;
                                                }
                                                case 366511058: {
                                                    n3 = n;
                                                    if (name.equals("CustomMethod")) {
                                                        n3 = 9;
                                                        break;
                                                    }
                                                    break;
                                                }
                                                case -71750448: {
                                                    n3 = n;
                                                    if (name.equals("Guideline")) {
                                                        n3 = 2;
                                                        break;
                                                    }
                                                    break;
                                                }
                                                case -1238332596: {
                                                    n3 = n;
                                                    if (name.equals("Transform")) {
                                                        n3 = 5;
                                                        break;
                                                    }
                                                    break;
                                                }
                                                case -1269513683: {
                                                    n3 = n;
                                                    if (name.equals("PropertySet")) {
                                                        n3 = 4;
                                                        break;
                                                    }
                                                    break;
                                                }
                                                case -1962203927: {
                                                    n3 = n;
                                                    if (name.equals("ConstraintOverride")) {
                                                        n3 = 1;
                                                        break;
                                                    }
                                                    break;
                                                }
                                                case -1984451626: {
                                                    n3 = n;
                                                    if (name.equals("Motion")) {
                                                        n3 = 7;
                                                        break;
                                                    }
                                                    break;
                                                }
                                                case -2025855158: {
                                                    final boolean equals = name.equals("Layout");
                                                    n3 = n;
                                                    if (equals) {
                                                        n3 = 6;
                                                        break;
                                                    }
                                                    break;
                                                }
                                            }
                                            switch (n3) {
                                                case 8:
                                                case 9: {
                                                    if (value != null) {
                                                        ConstraintAttribute.parse(context, xmlPullParser, value.mCustomConstraints);
                                                        break;
                                                    }
                                                    break Label_0601;
                                                }
                                                case 7: {
                                                    if (value != null) {
                                                        value.motion.fillFromAttributeList(context, Xml.asAttributeSet(xmlPullParser));
                                                        break;
                                                    }
                                                    break Label_0665;
                                                }
                                                case 6: {
                                                    if (value != null) {
                                                        value.layout.fillFromAttributeList(context, Xml.asAttributeSet(xmlPullParser));
                                                        break;
                                                    }
                                                    break Label_0731;
                                                }
                                                case 5: {
                                                    if (value != null) {
                                                        value.transform.fillFromAttributeList(context, Xml.asAttributeSet(xmlPullParser));
                                                        break;
                                                    }
                                                    break Label_0797;
                                                }
                                                case 4: {
                                                    if (value != null) {
                                                        value.propertySet.fillFromAttributeList(context, Xml.asAttributeSet(xmlPullParser));
                                                        break;
                                                    }
                                                    break Label_0863;
                                                }
                                                case 3: {
                                                    value = this.fillFromAttributeList(context, Xml.asAttributeSet(xmlPullParser), false);
                                                    value.layout.mHelperType = 1;
                                                    break;
                                                }
                                                case 2: {
                                                    value = this.fillFromAttributeList(context, Xml.asAttributeSet(xmlPullParser), false);
                                                    final Layout layout = value.layout;
                                                    layout.mIsGuideline = true;
                                                    layout.mApply = true;
                                                    break;
                                                }
                                                case 1: {
                                                    value = this.fillFromAttributeList(context, Xml.asAttributeSet(xmlPullParser), true);
                                                    break;
                                                }
                                                case 0: {
                                                    value = this.fillFromAttributeList(context, Xml.asAttributeSet(xmlPullParser), false);
                                                    break;
                                                }
                                            }
                                        }
                                    }
                                    else {
                                        xmlPullParser.getName();
                                    }
                                    i = xmlPullParser.next();
                                }
                                return;
                            }
                            final StringBuilder sb = new StringBuilder();
                            sb.append("XML parser error must be within a Constraint ");
                            sb.append(xmlPullParser.getLineNumber());
                            throw new RuntimeException(sb.toString());
                        }
                        final StringBuilder sb2 = new StringBuilder();
                        sb2.append("XML parser error must be within a Constraint ");
                        sb2.append(xmlPullParser.getLineNumber());
                        throw new RuntimeException(sb2.toString());
                    }
                    final StringBuilder sb3 = new StringBuilder();
                    sb3.append("XML parser error must be within a Constraint ");
                    sb3.append(xmlPullParser.getLineNumber());
                    throw new RuntimeException(sb3.toString());
                }
                final StringBuilder sb4 = new StringBuilder();
                sb4.append("XML parser error must be within a Constraint ");
                sb4.append(xmlPullParser.getLineNumber());
                throw new RuntimeException(sb4.toString());
            }
            final StringBuilder sb5 = new StringBuilder();
            sb5.append("XML parser error must be within a Constraint ");
            sb5.append(xmlPullParser.getLineNumber());
            throw new RuntimeException(sb5.toString());
        }
        catch (final IOException ex) {
            ex.printStackTrace();
        }
        catch (final XmlPullParserException ex2) {
            ex2.printStackTrace();
        }
    }
    
    public void parseColorAttributes(final Constraint constraint, final String s) {
        final String[] split = s.split(",");
        for (int i = 0; i < split.length; ++i) {
            final String[] split2 = split[i].split("=");
            if (split2.length != 2) {
                final StringBuilder sb = new StringBuilder();
                sb.append(" Unable to parse ");
                sb.append(split[i]);
            }
            else {
                constraint.setColorValue(split2[0], Color.parseColor(split2[1]));
            }
        }
    }
    
    public void parseFloatAttributes(final Constraint constraint, final String s) {
        final String[] split = s.split(",");
        for (int i = 0; i < split.length; ++i) {
            final String[] split2 = split[i].split("=");
            if (split2.length != 2) {
                final StringBuilder sb = new StringBuilder();
                sb.append(" Unable to parse ");
                sb.append(split[i]);
            }
            else {
                constraint.setFloatValue(split2[0], Float.parseFloat(split2[1]));
            }
        }
    }
    
    public void parseIntAttributes(final Constraint constraint, final String s) {
        final String[] split = s.split(",");
        for (int i = 0; i < split.length; ++i) {
            final String[] split2 = split[i].split("=");
            if (split2.length != 2) {
                final StringBuilder sb = new StringBuilder();
                sb.append(" Unable to parse ");
                sb.append(split[i]);
            }
            else {
                constraint.setFloatValue(split2[0], Integer.decode(split2[1]));
            }
        }
    }
    
    public void parseStringAttributes(final Constraint constraint, final String s) {
        final String[] splitString = splitString(s);
        for (int i = 0; i < splitString.length; ++i) {
            final String[] split = splitString[i].split("=");
            final StringBuilder sb = new StringBuilder();
            sb.append(" Unable to parse ");
            sb.append(splitString[i]);
            constraint.setStringValue(split[0], split[1]);
        }
    }
    
    public void readFallback(final ConstraintLayout constraintLayout) {
        for (int childCount = constraintLayout.getChildCount(), i = 0; i < childCount; ++i) {
            final View child = constraintLayout.getChildAt(i);
            final ConstraintLayout.LayoutParams layoutParams = (ConstraintLayout.LayoutParams)child.getLayoutParams();
            final int id = child.getId();
            if (this.mForceId && id == -1) {
                throw new RuntimeException("All children of ConstraintLayout must have ids to use ConstraintSet");
            }
            if (!this.mConstraints.containsKey(id)) {
                this.mConstraints.put(id, new Constraint());
            }
            final Constraint constraint = this.mConstraints.get(id);
            if (constraint != null) {
                if (!constraint.layout.mApply) {
                    constraint.fillFrom(id, layoutParams);
                    if (child instanceof ConstraintHelper) {
                        constraint.layout.mReferenceIds = ((ConstraintHelper)child).getReferencedIds();
                        if (child instanceof Barrier) {
                            final Barrier barrier = (Barrier)child;
                            constraint.layout.mBarrierAllowsGoneWidgets = barrier.getAllowsGoneWidget();
                            constraint.layout.mBarrierDirection = barrier.getType();
                            constraint.layout.mBarrierMargin = barrier.getMargin();
                        }
                    }
                    constraint.layout.mApply = true;
                }
                final PropertySet propertySet = constraint.propertySet;
                if (!propertySet.mApply) {
                    propertySet.visibility = child.getVisibility();
                    constraint.propertySet.alpha = child.getAlpha();
                    constraint.propertySet.mApply = true;
                }
                final Transform transform = constraint.transform;
                if (!transform.mApply) {
                    transform.mApply = true;
                    transform.rotation = child.getRotation();
                    constraint.transform.rotationX = child.getRotationX();
                    constraint.transform.rotationY = child.getRotationY();
                    constraint.transform.scaleX = child.getScaleX();
                    constraint.transform.scaleY = child.getScaleY();
                    final float pivotX = child.getPivotX();
                    final float pivotY = child.getPivotY();
                    if (pivotX != 0.0 || pivotY != 0.0) {
                        final Transform transform2 = constraint.transform;
                        transform2.transformPivotX = pivotX;
                        transform2.transformPivotY = pivotY;
                    }
                    constraint.transform.translationX = child.getTranslationX();
                    constraint.transform.translationY = child.getTranslationY();
                    constraint.transform.translationZ = child.getTranslationZ();
                    final Transform transform3 = constraint.transform;
                    if (transform3.applyElevation) {
                        transform3.elevation = child.getElevation();
                    }
                }
            }
        }
    }
    
    public void readFallback(final ConstraintSet set) {
        for (final Integer key : set.mConstraints.keySet()) {
            final int intValue = key;
            final Constraint constraint = set.mConstraints.get(key);
            if (!this.mConstraints.containsKey(intValue)) {
                this.mConstraints.put(intValue, new Constraint());
            }
            final Constraint constraint2 = this.mConstraints.get(intValue);
            if (constraint2 == null) {
                continue;
            }
            final Layout layout = constraint2.layout;
            if (!layout.mApply) {
                layout.copyFrom(constraint.layout);
            }
            final PropertySet propertySet = constraint2.propertySet;
            if (!propertySet.mApply) {
                propertySet.copyFrom(constraint.propertySet);
            }
            final Transform transform = constraint2.transform;
            if (!transform.mApply) {
                transform.copyFrom(constraint.transform);
            }
            final Motion motion = constraint2.motion;
            if (!motion.mApply) {
                motion.copyFrom(constraint.motion);
            }
            for (final String key2 : constraint.mCustomConstraints.keySet()) {
                if (!constraint2.mCustomConstraints.containsKey(key2)) {
                    constraint2.mCustomConstraints.put(key2, constraint.mCustomConstraints.get(key2));
                }
            }
        }
    }
    
    public void removeAttribute(final String key) {
        this.mSavedAttributes.remove(key);
    }
    
    public void removeFromHorizontalChain(final int n) {
        if (this.mConstraints.containsKey(n)) {
            final Constraint constraint = this.mConstraints.get(n);
            if (constraint == null) {
                return;
            }
            final Layout layout = constraint.layout;
            final int leftToRight = layout.leftToRight;
            final int rightToLeft = layout.rightToLeft;
            if (leftToRight == -1 && rightToLeft == -1) {
                final int startToEnd = layout.startToEnd;
                final int endToStart = layout.endToStart;
                if (startToEnd != -1 || endToStart != -1) {
                    if (startToEnd != -1 && endToStart != -1) {
                        this.connect(startToEnd, 7, endToStart, 6, 0);
                        this.connect(endToStart, 6, leftToRight, 7, 0);
                    }
                    else if (endToStart != -1) {
                        final int rightToRight = layout.rightToRight;
                        if (rightToRight != -1) {
                            this.connect(leftToRight, 7, rightToRight, 7, 0);
                        }
                        else {
                            final int leftToLeft = layout.leftToLeft;
                            if (leftToLeft != -1) {
                                this.connect(endToStart, 6, leftToLeft, 6, 0);
                            }
                        }
                    }
                }
                this.clear(n, 6);
                this.clear(n, 7);
            }
            else {
                if (leftToRight != -1 && rightToLeft != -1) {
                    this.connect(leftToRight, 2, rightToLeft, 1, 0);
                    this.connect(rightToLeft, 1, leftToRight, 2, 0);
                }
                else {
                    final int rightToRight2 = layout.rightToRight;
                    if (rightToRight2 != -1) {
                        this.connect(leftToRight, 2, rightToRight2, 2, 0);
                    }
                    else {
                        final int leftToLeft2 = layout.leftToLeft;
                        if (leftToLeft2 != -1) {
                            this.connect(rightToLeft, 1, leftToLeft2, 1, 0);
                        }
                    }
                }
                this.clear(n, 1);
                this.clear(n, 2);
            }
        }
    }
    
    public void removeFromVerticalChain(final int n) {
        if (this.mConstraints.containsKey(n)) {
            final Constraint constraint = this.mConstraints.get(n);
            if (constraint == null) {
                return;
            }
            final Layout layout = constraint.layout;
            final int topToBottom = layout.topToBottom;
            final int bottomToTop = layout.bottomToTop;
            if (topToBottom != -1 || bottomToTop != -1) {
                if (topToBottom != -1 && bottomToTop != -1) {
                    this.connect(topToBottom, 4, bottomToTop, 3, 0);
                    this.connect(bottomToTop, 3, topToBottom, 4, 0);
                }
                else {
                    final int bottomToBottom = layout.bottomToBottom;
                    if (bottomToBottom != -1) {
                        this.connect(topToBottom, 4, bottomToBottom, 4, 0);
                    }
                    else {
                        final int topToTop = layout.topToTop;
                        if (topToTop != -1) {
                            this.connect(bottomToTop, 3, topToTop, 3, 0);
                        }
                    }
                }
            }
        }
        this.clear(n, 3);
        this.clear(n, 4);
    }
    
    public void setAlpha(final int n, final float alpha) {
        this.get(n).propertySet.alpha = alpha;
    }
    
    public void setApplyElevation(final int n, final boolean applyElevation) {
        this.get(n).transform.applyElevation = applyElevation;
    }
    
    public void setBarrierType(final int n, final int mHelperType) {
        this.get(n).layout.mHelperType = mHelperType;
    }
    
    public void setColorValue(final int n, final String s, final int n2) {
        this.get(n).setColorValue(s, n2);
    }
    
    public void setDimensionRatio(final int n, final String dimensionRatio) {
        this.get(n).layout.dimensionRatio = dimensionRatio;
    }
    
    public void setEditorAbsoluteX(final int n, final int editorAbsoluteX) {
        this.get(n).layout.editorAbsoluteX = editorAbsoluteX;
    }
    
    public void setEditorAbsoluteY(final int n, final int editorAbsoluteY) {
        this.get(n).layout.editorAbsoluteY = editorAbsoluteY;
    }
    
    public void setElevation(final int n, final float elevation) {
        this.get(n).transform.elevation = elevation;
        this.get(n).transform.applyElevation = true;
    }
    
    public void setFloatValue(final int n, final String s, final float n2) {
        this.get(n).setFloatValue(s, n2);
    }
    
    public void setForceId(final boolean mForceId) {
        this.mForceId = mForceId;
    }
    
    public void setGoneMargin(final int n, final int n2, final int goneLeftMargin) {
        final Constraint value = this.get(n);
        switch (n2) {
            default: {
                throw new IllegalArgumentException("unknown constraint");
            }
            case 7: {
                value.layout.goneEndMargin = goneLeftMargin;
                break;
            }
            case 6: {
                value.layout.goneStartMargin = goneLeftMargin;
                break;
            }
            case 5: {
                value.layout.goneBaselineMargin = goneLeftMargin;
                break;
            }
            case 4: {
                value.layout.goneBottomMargin = goneLeftMargin;
                break;
            }
            case 3: {
                value.layout.goneTopMargin = goneLeftMargin;
                break;
            }
            case 2: {
                value.layout.goneRightMargin = goneLeftMargin;
                break;
            }
            case 1: {
                value.layout.goneLeftMargin = goneLeftMargin;
                break;
            }
        }
    }
    
    public void setGuidelineBegin(final int n, final int guideBegin) {
        this.get(n).layout.guideBegin = guideBegin;
        this.get(n).layout.guideEnd = -1;
        this.get(n).layout.guidePercent = -1.0f;
    }
    
    public void setGuidelineEnd(final int n, final int guideEnd) {
        this.get(n).layout.guideEnd = guideEnd;
        this.get(n).layout.guideBegin = -1;
        this.get(n).layout.guidePercent = -1.0f;
    }
    
    public void setGuidelinePercent(final int n, final float guidePercent) {
        this.get(n).layout.guidePercent = guidePercent;
        this.get(n).layout.guideEnd = -1;
        this.get(n).layout.guideBegin = -1;
    }
    
    public void setHorizontalBias(final int n, final float horizontalBias) {
        this.get(n).layout.horizontalBias = horizontalBias;
    }
    
    public void setHorizontalChainStyle(final int n, final int horizontalChainStyle) {
        this.get(n).layout.horizontalChainStyle = horizontalChainStyle;
    }
    
    public void setHorizontalWeight(final int n, final float horizontalWeight) {
        this.get(n).layout.horizontalWeight = horizontalWeight;
    }
    
    public void setIntValue(final int n, final String s, final int n2) {
        this.get(n).setIntValue(s, n2);
    }
    
    public void setLayoutWrapBehavior(final int n, final int mWrapBehavior) {
        if (mWrapBehavior >= 0 && mWrapBehavior <= 3) {
            this.get(n).layout.mWrapBehavior = mWrapBehavior;
        }
    }
    
    public void setMargin(final int n, final int n2, final int leftMargin) {
        final Constraint value = this.get(n);
        switch (n2) {
            default: {
                throw new IllegalArgumentException("unknown constraint");
            }
            case 7: {
                value.layout.endMargin = leftMargin;
                break;
            }
            case 6: {
                value.layout.startMargin = leftMargin;
                break;
            }
            case 5: {
                value.layout.baselineMargin = leftMargin;
                break;
            }
            case 4: {
                value.layout.bottomMargin = leftMargin;
                break;
            }
            case 3: {
                value.layout.topMargin = leftMargin;
                break;
            }
            case 2: {
                value.layout.rightMargin = leftMargin;
                break;
            }
            case 1: {
                value.layout.leftMargin = leftMargin;
                break;
            }
        }
    }
    
    public void setReferencedIds(final int n, final int... mReferenceIds) {
        this.get(n).layout.mReferenceIds = mReferenceIds;
    }
    
    public void setRotation(final int n, final float rotation) {
        this.get(n).transform.rotation = rotation;
    }
    
    public void setRotationX(final int n, final float rotationX) {
        this.get(n).transform.rotationX = rotationX;
    }
    
    public void setRotationY(final int n, final float rotationY) {
        this.get(n).transform.rotationY = rotationY;
    }
    
    public void setScaleX(final int n, final float scaleX) {
        this.get(n).transform.scaleX = scaleX;
    }
    
    public void setScaleY(final int n, final float scaleY) {
        this.get(n).transform.scaleY = scaleY;
    }
    
    public void setStringValue(final int n, final String s, final String s2) {
        this.get(n).setStringValue(s, s2);
    }
    
    public void setTransformPivot(final int n, final float transformPivotX, final float transformPivotY) {
        final Transform transform = this.get(n).transform;
        transform.transformPivotY = transformPivotY;
        transform.transformPivotX = transformPivotX;
    }
    
    public void setTransformPivotX(final int n, final float transformPivotX) {
        this.get(n).transform.transformPivotX = transformPivotX;
    }
    
    public void setTransformPivotY(final int n, final float transformPivotY) {
        this.get(n).transform.transformPivotY = transformPivotY;
    }
    
    public void setTranslation(final int n, final float translationX, final float translationY) {
        final Transform transform = this.get(n).transform;
        transform.translationX = translationX;
        transform.translationY = translationY;
    }
    
    public void setTranslationX(final int n, final float translationX) {
        this.get(n).transform.translationX = translationX;
    }
    
    public void setTranslationY(final int n, final float translationY) {
        this.get(n).transform.translationY = translationY;
    }
    
    public void setTranslationZ(final int n, final float translationZ) {
        this.get(n).transform.translationZ = translationZ;
    }
    
    public void setValidateOnParse(final boolean mValidate) {
        this.mValidate = mValidate;
    }
    
    public void setVerticalBias(final int n, final float verticalBias) {
        this.get(n).layout.verticalBias = verticalBias;
    }
    
    public void setVerticalChainStyle(final int n, final int verticalChainStyle) {
        this.get(n).layout.verticalChainStyle = verticalChainStyle;
    }
    
    public void setVerticalWeight(final int n, final float verticalWeight) {
        this.get(n).layout.verticalWeight = verticalWeight;
    }
    
    public void setVisibility(final int n, final int visibility) {
        this.get(n).propertySet.visibility = visibility;
    }
    
    public void setVisibilityMode(final int n, final int mVisibilityMode) {
        this.get(n).propertySet.mVisibilityMode = mVisibilityMode;
    }
    
    public void writeState(final Writer writer, final ConstraintLayout constraintLayout, final int n) throws IOException {
        writer.write("\n---------------------------------------------\n");
        if ((n & 0x1) == 0x1) {
            new WriteXmlEngine(writer, constraintLayout, n).writeLayout();
        }
        else {
            new WriteJsonEngine(writer, constraintLayout, n).writeLayout();
        }
        writer.write("\n---------------------------------------------\n");
    }
    
    public static class Constraint
    {
        public final Layout layout;
        public HashMap<String, ConstraintAttribute> mCustomConstraints;
        Delta mDelta;
        String mTargetString;
        int mViewId;
        public final Motion motion;
        public final PropertySet propertySet;
        public final Transform transform;
        
        public Constraint() {
            this.propertySet = new PropertySet();
            this.motion = new Motion();
            this.layout = new Layout();
            this.transform = new Transform();
            this.mCustomConstraints = new HashMap<String, ConstraintAttribute>();
        }
        
        private void fillFrom(final int mViewId, final ConstraintLayout.LayoutParams layoutParams) {
            this.mViewId = mViewId;
            final Layout layout = this.layout;
            layout.leftToLeft = layoutParams.leftToLeft;
            layout.leftToRight = layoutParams.leftToRight;
            layout.rightToLeft = layoutParams.rightToLeft;
            layout.rightToRight = layoutParams.rightToRight;
            layout.topToTop = layoutParams.topToTop;
            layout.topToBottom = layoutParams.topToBottom;
            layout.bottomToTop = layoutParams.bottomToTop;
            layout.bottomToBottom = layoutParams.bottomToBottom;
            layout.baselineToBaseline = layoutParams.baselineToBaseline;
            layout.baselineToTop = layoutParams.baselineToTop;
            layout.baselineToBottom = layoutParams.baselineToBottom;
            layout.startToEnd = layoutParams.startToEnd;
            layout.startToStart = layoutParams.startToStart;
            layout.endToStart = layoutParams.endToStart;
            layout.endToEnd = layoutParams.endToEnd;
            layout.horizontalBias = layoutParams.horizontalBias;
            layout.verticalBias = layoutParams.verticalBias;
            layout.dimensionRatio = layoutParams.dimensionRatio;
            layout.circleConstraint = layoutParams.circleConstraint;
            layout.circleRadius = layoutParams.circleRadius;
            layout.circleAngle = layoutParams.circleAngle;
            layout.editorAbsoluteX = layoutParams.editorAbsoluteX;
            layout.editorAbsoluteY = layoutParams.editorAbsoluteY;
            layout.orientation = layoutParams.orientation;
            layout.guidePercent = layoutParams.guidePercent;
            layout.guideBegin = layoutParams.guideBegin;
            layout.guideEnd = layoutParams.guideEnd;
            layout.mWidth = layoutParams.width;
            layout.mHeight = layoutParams.height;
            layout.leftMargin = layoutParams.leftMargin;
            layout.rightMargin = layoutParams.rightMargin;
            layout.topMargin = layoutParams.topMargin;
            layout.bottomMargin = layoutParams.bottomMargin;
            layout.baselineMargin = layoutParams.baselineMargin;
            layout.verticalWeight = layoutParams.verticalWeight;
            layout.horizontalWeight = layoutParams.horizontalWeight;
            layout.verticalChainStyle = layoutParams.verticalChainStyle;
            layout.horizontalChainStyle = layoutParams.horizontalChainStyle;
            layout.constrainedWidth = layoutParams.constrainedWidth;
            layout.constrainedHeight = layoutParams.constrainedHeight;
            layout.widthDefault = layoutParams.matchConstraintDefaultWidth;
            layout.heightDefault = layoutParams.matchConstraintDefaultHeight;
            layout.widthMax = layoutParams.matchConstraintMaxWidth;
            layout.heightMax = layoutParams.matchConstraintMaxHeight;
            layout.widthMin = layoutParams.matchConstraintMinWidth;
            layout.heightMin = layoutParams.matchConstraintMinHeight;
            layout.widthPercent = layoutParams.matchConstraintPercentWidth;
            layout.heightPercent = layoutParams.matchConstraintPercentHeight;
            layout.mConstraintTag = layoutParams.constraintTag;
            layout.goneTopMargin = layoutParams.goneTopMargin;
            layout.goneBottomMargin = layoutParams.goneBottomMargin;
            layout.goneLeftMargin = layoutParams.goneLeftMargin;
            layout.goneRightMargin = layoutParams.goneRightMargin;
            layout.goneStartMargin = layoutParams.goneStartMargin;
            layout.goneEndMargin = layoutParams.goneEndMargin;
            layout.goneBaselineMargin = layoutParams.goneBaselineMargin;
            layout.mWrapBehavior = layoutParams.wrapBehaviorInParent;
            layout.endMargin = layoutParams.getMarginEnd();
            this.layout.startMargin = layoutParams.getMarginStart();
        }
        
        private void fillFromConstraints(final int n, final Constraints.LayoutParams layoutParams) {
            this.fillFrom(n, layoutParams);
            this.propertySet.alpha = layoutParams.alpha;
            final Transform transform = this.transform;
            transform.rotation = layoutParams.rotation;
            transform.rotationX = layoutParams.rotationX;
            transform.rotationY = layoutParams.rotationY;
            transform.scaleX = layoutParams.scaleX;
            transform.scaleY = layoutParams.scaleY;
            transform.transformPivotX = layoutParams.transformPivotX;
            transform.transformPivotY = layoutParams.transformPivotY;
            transform.translationX = layoutParams.translationX;
            transform.translationY = layoutParams.translationY;
            transform.translationZ = layoutParams.translationZ;
            transform.elevation = layoutParams.elevation;
            transform.applyElevation = layoutParams.applyElevation;
        }
        
        private void fillFromConstraints(final ConstraintHelper constraintHelper, final int n, final Constraints.LayoutParams layoutParams) {
            this.fillFromConstraints(n, layoutParams);
            if (constraintHelper instanceof Barrier) {
                final Layout layout = this.layout;
                layout.mHelperType = 1;
                final Barrier barrier = (Barrier)constraintHelper;
                layout.mBarrierDirection = barrier.getType();
                this.layout.mReferenceIds = barrier.getReferencedIds();
                this.layout.mBarrierMargin = barrier.getMargin();
            }
        }
        
        private ConstraintAttribute get(final String key, final ConstraintAttribute.AttributeType attributeType) {
            ConstraintAttribute constraintAttribute;
            if (this.mCustomConstraints.containsKey(key)) {
                constraintAttribute = this.mCustomConstraints.get(key);
                if (constraintAttribute.getType() != attributeType) {
                    final StringBuilder sb = new StringBuilder();
                    sb.append("ConstraintAttribute is already a ");
                    sb.append(constraintAttribute.getType().name());
                    throw new IllegalArgumentException(sb.toString());
                }
            }
            else {
                final ConstraintAttribute value = new ConstraintAttribute(key, attributeType);
                this.mCustomConstraints.put(key, value);
                constraintAttribute = value;
            }
            return constraintAttribute;
        }
        
        private void setColorValue(final String s, final int colorValue) {
            this.get(s, ConstraintAttribute.AttributeType.COLOR_TYPE).setColorValue(colorValue);
        }
        
        private void setFloatValue(final String s, final float floatValue) {
            this.get(s, ConstraintAttribute.AttributeType.FLOAT_TYPE).setFloatValue(floatValue);
        }
        
        private void setIntValue(final String s, final int intValue) {
            this.get(s, ConstraintAttribute.AttributeType.INT_TYPE).setIntValue(intValue);
        }
        
        private void setStringValue(final String s, final String stringValue) {
            this.get(s, ConstraintAttribute.AttributeType.STRING_TYPE).setStringValue(stringValue);
        }
        
        public void applyDelta(final Constraint constraint) {
            final Delta mDelta = this.mDelta;
            if (mDelta != null) {
                mDelta.applyDelta(constraint);
            }
        }
        
        public void applyTo(final ConstraintLayout.LayoutParams layoutParams) {
            final Layout layout = this.layout;
            layoutParams.leftToLeft = layout.leftToLeft;
            layoutParams.leftToRight = layout.leftToRight;
            layoutParams.rightToLeft = layout.rightToLeft;
            layoutParams.rightToRight = layout.rightToRight;
            layoutParams.topToTop = layout.topToTop;
            layoutParams.topToBottom = layout.topToBottom;
            layoutParams.bottomToTop = layout.bottomToTop;
            layoutParams.bottomToBottom = layout.bottomToBottom;
            layoutParams.baselineToBaseline = layout.baselineToBaseline;
            layoutParams.baselineToTop = layout.baselineToTop;
            layoutParams.baselineToBottom = layout.baselineToBottom;
            layoutParams.startToEnd = layout.startToEnd;
            layoutParams.startToStart = layout.startToStart;
            layoutParams.endToStart = layout.endToStart;
            layoutParams.endToEnd = layout.endToEnd;
            layoutParams.leftMargin = layout.leftMargin;
            layoutParams.rightMargin = layout.rightMargin;
            layoutParams.topMargin = layout.topMargin;
            layoutParams.bottomMargin = layout.bottomMargin;
            layoutParams.goneStartMargin = layout.goneStartMargin;
            layoutParams.goneEndMargin = layout.goneEndMargin;
            layoutParams.goneTopMargin = layout.goneTopMargin;
            layoutParams.goneBottomMargin = layout.goneBottomMargin;
            layoutParams.horizontalBias = layout.horizontalBias;
            layoutParams.verticalBias = layout.verticalBias;
            layoutParams.circleConstraint = layout.circleConstraint;
            layoutParams.circleRadius = layout.circleRadius;
            layoutParams.circleAngle = layout.circleAngle;
            layoutParams.dimensionRatio = layout.dimensionRatio;
            layoutParams.editorAbsoluteX = layout.editorAbsoluteX;
            layoutParams.editorAbsoluteY = layout.editorAbsoluteY;
            layoutParams.verticalWeight = layout.verticalWeight;
            layoutParams.horizontalWeight = layout.horizontalWeight;
            layoutParams.verticalChainStyle = layout.verticalChainStyle;
            layoutParams.horizontalChainStyle = layout.horizontalChainStyle;
            layoutParams.constrainedWidth = layout.constrainedWidth;
            layoutParams.constrainedHeight = layout.constrainedHeight;
            layoutParams.matchConstraintDefaultWidth = layout.widthDefault;
            layoutParams.matchConstraintDefaultHeight = layout.heightDefault;
            layoutParams.matchConstraintMaxWidth = layout.widthMax;
            layoutParams.matchConstraintMaxHeight = layout.heightMax;
            layoutParams.matchConstraintMinWidth = layout.widthMin;
            layoutParams.matchConstraintMinHeight = layout.heightMin;
            layoutParams.matchConstraintPercentWidth = layout.widthPercent;
            layoutParams.matchConstraintPercentHeight = layout.heightPercent;
            layoutParams.orientation = layout.orientation;
            layoutParams.guidePercent = layout.guidePercent;
            layoutParams.guideBegin = layout.guideBegin;
            layoutParams.guideEnd = layout.guideEnd;
            layoutParams.width = layout.mWidth;
            layoutParams.height = layout.mHeight;
            final String mConstraintTag = layout.mConstraintTag;
            if (mConstraintTag != null) {
                layoutParams.constraintTag = mConstraintTag;
            }
            layoutParams.wrapBehaviorInParent = layout.mWrapBehavior;
            layoutParams.setMarginStart(layout.startMargin);
            layoutParams.setMarginEnd(this.layout.endMargin);
            layoutParams.validate();
        }
        
        public Constraint clone() {
            final Constraint constraint = new Constraint();
            constraint.layout.copyFrom(this.layout);
            constraint.motion.copyFrom(this.motion);
            constraint.propertySet.copyFrom(this.propertySet);
            constraint.transform.copyFrom(this.transform);
            constraint.mViewId = this.mViewId;
            constraint.mDelta = this.mDelta;
            return constraint;
        }
        
        public void printDelta(final String s) {
            final Delta mDelta = this.mDelta;
            if (mDelta != null) {
                mDelta.printDelta(s);
            }
        }
        
        static class Delta
        {
            private static final int INITIAL_BOOLEAN = 4;
            private static final int INITIAL_FLOAT = 10;
            private static final int INITIAL_INT = 10;
            private static final int INITIAL_STRING = 5;
            int mCountBoolean;
            int mCountFloat;
            int mCountInt;
            int mCountString;
            int[] mTypeBoolean;
            int[] mTypeFloat;
            int[] mTypeInt;
            int[] mTypeString;
            boolean[] mValueBoolean;
            float[] mValueFloat;
            int[] mValueInt;
            String[] mValueString;
            
            Delta() {
                this.mTypeInt = new int[10];
                this.mValueInt = new int[10];
                this.mCountInt = 0;
                this.mTypeFloat = new int[10];
                this.mValueFloat = new float[10];
                this.mCountFloat = 0;
                this.mTypeString = new int[5];
                this.mValueString = new String[5];
                this.mCountString = 0;
                this.mTypeBoolean = new int[4];
                this.mValueBoolean = new boolean[4];
                this.mCountBoolean = 0;
            }
            
            void add(final int n, final float n2) {
                final int mCountFloat = this.mCountFloat;
                final int[] mTypeFloat = this.mTypeFloat;
                if (mCountFloat >= mTypeFloat.length) {
                    this.mTypeFloat = Arrays.copyOf(mTypeFloat, mTypeFloat.length * 2);
                    final float[] mValueFloat = this.mValueFloat;
                    this.mValueFloat = Arrays.copyOf(mValueFloat, mValueFloat.length * 2);
                }
                final int[] mTypeFloat2 = this.mTypeFloat;
                final int mCountFloat2 = this.mCountFloat;
                mTypeFloat2[mCountFloat2] = n;
                final float[] mValueFloat2 = this.mValueFloat;
                this.mCountFloat = mCountFloat2 + 1;
                mValueFloat2[mCountFloat2] = n2;
            }
            
            void add(final int n, final int n2) {
                final int mCountInt = this.mCountInt;
                final int[] mTypeInt = this.mTypeInt;
                if (mCountInt >= mTypeInt.length) {
                    this.mTypeInt = Arrays.copyOf(mTypeInt, mTypeInt.length * 2);
                    final int[] mValueInt = this.mValueInt;
                    this.mValueInt = Arrays.copyOf(mValueInt, mValueInt.length * 2);
                }
                final int[] mTypeInt2 = this.mTypeInt;
                final int mCountInt2 = this.mCountInt;
                mTypeInt2[mCountInt2] = n;
                final int[] mValueInt2 = this.mValueInt;
                this.mCountInt = mCountInt2 + 1;
                mValueInt2[mCountInt2] = n2;
            }
            
            void add(final int n, final String s) {
                final int mCountString = this.mCountString;
                final int[] mTypeString = this.mTypeString;
                if (mCountString >= mTypeString.length) {
                    this.mTypeString = Arrays.copyOf(mTypeString, mTypeString.length * 2);
                    final String[] mValueString = this.mValueString;
                    this.mValueString = Arrays.copyOf(mValueString, mValueString.length * 2);
                }
                final int[] mTypeString2 = this.mTypeString;
                final int mCountString2 = this.mCountString;
                mTypeString2[mCountString2] = n;
                final String[] mValueString2 = this.mValueString;
                this.mCountString = mCountString2 + 1;
                mValueString2[mCountString2] = s;
            }
            
            void add(final int n, final boolean b) {
                final int mCountBoolean = this.mCountBoolean;
                final int[] mTypeBoolean = this.mTypeBoolean;
                if (mCountBoolean >= mTypeBoolean.length) {
                    this.mTypeBoolean = Arrays.copyOf(mTypeBoolean, mTypeBoolean.length * 2);
                    final boolean[] mValueBoolean = this.mValueBoolean;
                    this.mValueBoolean = Arrays.copyOf(mValueBoolean, mValueBoolean.length * 2);
                }
                final int[] mTypeBoolean2 = this.mTypeBoolean;
                final int mCountBoolean2 = this.mCountBoolean;
                mTypeBoolean2[mCountBoolean2] = n;
                final boolean[] mValueBoolean2 = this.mValueBoolean;
                this.mCountBoolean = mCountBoolean2 + 1;
                mValueBoolean2[mCountBoolean2] = b;
            }
            
            void applyDelta(final Constraint constraint) {
                final int n = 0;
                for (int i = 0; i < this.mCountInt; ++i) {
                    setDeltaValue(constraint, this.mTypeInt[i], this.mValueInt[i]);
                }
                for (int j = 0; j < this.mCountFloat; ++j) {
                    setDeltaValue(constraint, this.mTypeFloat[j], this.mValueFloat[j]);
                }
                int n2 = 0;
                int k;
                while (true) {
                    k = n;
                    if (n2 >= this.mCountString) {
                        break;
                    }
                    setDeltaValue(constraint, this.mTypeString[n2], this.mValueString[n2]);
                    ++n2;
                }
                while (k < this.mCountBoolean) {
                    setDeltaValue(constraint, this.mTypeBoolean[k], this.mValueBoolean[k]);
                    ++k;
                }
            }
            
            @SuppressLint({ "LogConditional" })
            void printDelta(final String s) {
                final int n = 0;
                for (int i = 0; i < this.mCountInt; ++i) {
                    final StringBuilder sb = new StringBuilder();
                    sb.append(this.mTypeInt[i]);
                    sb.append(" = ");
                    sb.append(this.mValueInt[i]);
                }
                for (int j = 0; j < this.mCountFloat; ++j) {
                    final StringBuilder sb2 = new StringBuilder();
                    sb2.append(this.mTypeFloat[j]);
                    sb2.append(" = ");
                    sb2.append(this.mValueFloat[j]);
                }
                int n2 = 0;
                int k;
                while (true) {
                    k = n;
                    if (n2 >= this.mCountString) {
                        break;
                    }
                    final StringBuilder sb3 = new StringBuilder();
                    sb3.append(this.mTypeString[n2]);
                    sb3.append(" = ");
                    sb3.append(this.mValueString[n2]);
                    ++n2;
                }
                while (k < this.mCountBoolean) {
                    final StringBuilder sb4 = new StringBuilder();
                    sb4.append(this.mTypeBoolean[k]);
                    sb4.append(" = ");
                    sb4.append(this.mValueBoolean[k]);
                    ++k;
                }
            }
        }
    }
    
    public static class Layout
    {
        private static final int BARRIER_ALLOWS_GONE_WIDGETS = 75;
        private static final int BARRIER_DIRECTION = 72;
        private static final int BARRIER_MARGIN = 73;
        private static final int BASELINE_TO_BASELINE = 1;
        private static final int BOTTOM_MARGIN = 2;
        private static final int BOTTOM_TO_BOTTOM = 3;
        private static final int BOTTOM_TO_TOP = 4;
        private static final int CHAIN_USE_RTL = 71;
        private static final int CIRCLE = 61;
        private static final int CIRCLE_ANGLE = 63;
        private static final int CIRCLE_RADIUS = 62;
        private static final int CONSTRAINT_REFERENCED_IDS = 74;
        private static final int DIMENSION_RATIO = 5;
        private static final int EDITOR_ABSOLUTE_X = 6;
        private static final int EDITOR_ABSOLUTE_Y = 7;
        private static final int END_MARGIN = 8;
        private static final int END_TO_END = 9;
        private static final int END_TO_START = 10;
        private static final int GONE_BOTTOM_MARGIN = 11;
        private static final int GONE_END_MARGIN = 12;
        private static final int GONE_LEFT_MARGIN = 13;
        private static final int GONE_RIGHT_MARGIN = 14;
        private static final int GONE_START_MARGIN = 15;
        private static final int GONE_TOP_MARGIN = 16;
        private static final int GUIDE_BEGIN = 17;
        private static final int GUIDE_END = 18;
        private static final int GUIDE_PERCENT = 19;
        private static final int HEIGHT_PERCENT = 70;
        private static final int HORIZONTAL_BIAS = 20;
        private static final int HORIZONTAL_STYLE = 39;
        private static final int HORIZONTAL_WEIGHT = 37;
        private static final int LAYOUT_CONSTRAINT_HEIGHT = 42;
        private static final int LAYOUT_CONSTRAINT_WIDTH = 41;
        private static final int LAYOUT_HEIGHT = 21;
        private static final int LAYOUT_WIDTH = 22;
        private static final int LEFT_MARGIN = 23;
        private static final int LEFT_TO_LEFT = 24;
        private static final int LEFT_TO_RIGHT = 25;
        private static final int ORIENTATION = 26;
        private static final int RIGHT_MARGIN = 27;
        private static final int RIGHT_TO_LEFT = 28;
        private static final int RIGHT_TO_RIGHT = 29;
        private static final int START_MARGIN = 30;
        private static final int START_TO_END = 31;
        private static final int START_TO_START = 32;
        private static final int TOP_MARGIN = 33;
        private static final int TOP_TO_BOTTOM = 34;
        private static final int TOP_TO_TOP = 35;
        public static final int UNSET = -1;
        public static final int UNSET_GONE_MARGIN = Integer.MIN_VALUE;
        private static final int UNUSED = 76;
        private static final int VERTICAL_BIAS = 36;
        private static final int VERTICAL_STYLE = 40;
        private static final int VERTICAL_WEIGHT = 38;
        private static final int WIDTH_PERCENT = 69;
        private static SparseIntArray mapToConstant;
        public int baselineMargin;
        public int baselineToBaseline;
        public int baselineToBottom;
        public int baselineToTop;
        public int bottomMargin;
        public int bottomToBottom;
        public int bottomToTop;
        public float circleAngle;
        public int circleConstraint;
        public int circleRadius;
        public boolean constrainedHeight;
        public boolean constrainedWidth;
        public String dimensionRatio;
        public int editorAbsoluteX;
        public int editorAbsoluteY;
        public int endMargin;
        public int endToEnd;
        public int endToStart;
        public int goneBaselineMargin;
        public int goneBottomMargin;
        public int goneEndMargin;
        public int goneLeftMargin;
        public int goneRightMargin;
        public int goneStartMargin;
        public int goneTopMargin;
        public int guideBegin;
        public int guideEnd;
        public float guidePercent;
        public int heightDefault;
        public int heightMax;
        public int heightMin;
        public float heightPercent;
        public float horizontalBias;
        public int horizontalChainStyle;
        public float horizontalWeight;
        public int leftMargin;
        public int leftToLeft;
        public int leftToRight;
        public boolean mApply;
        public boolean mBarrierAllowsGoneWidgets;
        public int mBarrierDirection;
        public int mBarrierMargin;
        public String mConstraintTag;
        public int mHeight;
        public int mHelperType;
        public boolean mIsGuideline;
        public boolean mOverride;
        public String mReferenceIdString;
        public int[] mReferenceIds;
        public int mWidth;
        public int mWrapBehavior;
        public int orientation;
        public int rightMargin;
        public int rightToLeft;
        public int rightToRight;
        public int startMargin;
        public int startToEnd;
        public int startToStart;
        public int topMargin;
        public int topToBottom;
        public int topToTop;
        public float verticalBias;
        public int verticalChainStyle;
        public float verticalWeight;
        public int widthDefault;
        public int widthMax;
        public int widthMin;
        public float widthPercent;
        
        static {
            (Layout.mapToConstant = new SparseIntArray()).append(R.styleable.Layout_layout_constraintLeft_toLeftOf, 24);
            Layout.mapToConstant.append(R.styleable.Layout_layout_constraintLeft_toRightOf, 25);
            Layout.mapToConstant.append(R.styleable.Layout_layout_constraintRight_toLeftOf, 28);
            Layout.mapToConstant.append(R.styleable.Layout_layout_constraintRight_toRightOf, 29);
            Layout.mapToConstant.append(R.styleable.Layout_layout_constraintTop_toTopOf, 35);
            Layout.mapToConstant.append(R.styleable.Layout_layout_constraintTop_toBottomOf, 34);
            Layout.mapToConstant.append(R.styleable.Layout_layout_constraintBottom_toTopOf, 4);
            Layout.mapToConstant.append(R.styleable.Layout_layout_constraintBottom_toBottomOf, 3);
            Layout.mapToConstant.append(R.styleable.Layout_layout_constraintBaseline_toBaselineOf, 1);
            Layout.mapToConstant.append(R.styleable.Layout_layout_editor_absoluteX, 6);
            Layout.mapToConstant.append(R.styleable.Layout_layout_editor_absoluteY, 7);
            Layout.mapToConstant.append(R.styleable.Layout_layout_constraintGuide_begin, 17);
            Layout.mapToConstant.append(R.styleable.Layout_layout_constraintGuide_end, 18);
            Layout.mapToConstant.append(R.styleable.Layout_layout_constraintGuide_percent, 19);
            Layout.mapToConstant.append(R.styleable.Layout_android_orientation, 26);
            Layout.mapToConstant.append(R.styleable.Layout_layout_constraintStart_toEndOf, 31);
            Layout.mapToConstant.append(R.styleable.Layout_layout_constraintStart_toStartOf, 32);
            Layout.mapToConstant.append(R.styleable.Layout_layout_constraintEnd_toStartOf, 10);
            Layout.mapToConstant.append(R.styleable.Layout_layout_constraintEnd_toEndOf, 9);
            Layout.mapToConstant.append(R.styleable.Layout_layout_goneMarginLeft, 13);
            Layout.mapToConstant.append(R.styleable.Layout_layout_goneMarginTop, 16);
            Layout.mapToConstant.append(R.styleable.Layout_layout_goneMarginRight, 14);
            Layout.mapToConstant.append(R.styleable.Layout_layout_goneMarginBottom, 11);
            Layout.mapToConstant.append(R.styleable.Layout_layout_goneMarginStart, 15);
            Layout.mapToConstant.append(R.styleable.Layout_layout_goneMarginEnd, 12);
            Layout.mapToConstant.append(R.styleable.Layout_layout_constraintVertical_weight, 38);
            Layout.mapToConstant.append(R.styleable.Layout_layout_constraintHorizontal_weight, 37);
            Layout.mapToConstant.append(R.styleable.Layout_layout_constraintHorizontal_chainStyle, 39);
            Layout.mapToConstant.append(R.styleable.Layout_layout_constraintVertical_chainStyle, 40);
            Layout.mapToConstant.append(R.styleable.Layout_layout_constraintHorizontal_bias, 20);
            Layout.mapToConstant.append(R.styleable.Layout_layout_constraintVertical_bias, 36);
            Layout.mapToConstant.append(R.styleable.Layout_layout_constraintDimensionRatio, 5);
            Layout.mapToConstant.append(R.styleable.Layout_layout_constraintLeft_creator, 76);
            Layout.mapToConstant.append(R.styleable.Layout_layout_constraintTop_creator, 76);
            Layout.mapToConstant.append(R.styleable.Layout_layout_constraintRight_creator, 76);
            Layout.mapToConstant.append(R.styleable.Layout_layout_constraintBottom_creator, 76);
            Layout.mapToConstant.append(R.styleable.Layout_layout_constraintBaseline_creator, 76);
            Layout.mapToConstant.append(R.styleable.Layout_android_layout_marginLeft, 23);
            Layout.mapToConstant.append(R.styleable.Layout_android_layout_marginRight, 27);
            Layout.mapToConstant.append(R.styleable.Layout_android_layout_marginStart, 30);
            Layout.mapToConstant.append(R.styleable.Layout_android_layout_marginEnd, 8);
            Layout.mapToConstant.append(R.styleable.Layout_android_layout_marginTop, 33);
            Layout.mapToConstant.append(R.styleable.Layout_android_layout_marginBottom, 2);
            Layout.mapToConstant.append(R.styleable.Layout_android_layout_width, 22);
            Layout.mapToConstant.append(R.styleable.Layout_android_layout_height, 21);
            Layout.mapToConstant.append(R.styleable.Layout_layout_constraintWidth, 41);
            Layout.mapToConstant.append(R.styleable.Layout_layout_constraintHeight, 42);
            Layout.mapToConstant.append(R.styleable.Layout_layout_constrainedWidth, 41);
            Layout.mapToConstant.append(R.styleable.Layout_layout_constrainedHeight, 42);
            Layout.mapToConstant.append(R.styleable.Layout_layout_wrapBehaviorInParent, 97);
            Layout.mapToConstant.append(R.styleable.Layout_layout_constraintCircle, 61);
            Layout.mapToConstant.append(R.styleable.Layout_layout_constraintCircleRadius, 62);
            Layout.mapToConstant.append(R.styleable.Layout_layout_constraintCircleAngle, 63);
            Layout.mapToConstant.append(R.styleable.Layout_layout_constraintWidth_percent, 69);
            Layout.mapToConstant.append(R.styleable.Layout_layout_constraintHeight_percent, 70);
            Layout.mapToConstant.append(R.styleable.Layout_chainUseRtl, 71);
            Layout.mapToConstant.append(R.styleable.Layout_barrierDirection, 72);
            Layout.mapToConstant.append(R.styleable.Layout_barrierMargin, 73);
            Layout.mapToConstant.append(R.styleable.Layout_constraint_referenced_ids, 74);
            Layout.mapToConstant.append(R.styleable.Layout_barrierAllowsGoneWidgets, 75);
        }
        
        public Layout() {
            this.mIsGuideline = false;
            this.mApply = false;
            this.mOverride = false;
            this.guideBegin = -1;
            this.guideEnd = -1;
            this.guidePercent = -1.0f;
            this.leftToLeft = -1;
            this.leftToRight = -1;
            this.rightToLeft = -1;
            this.rightToRight = -1;
            this.topToTop = -1;
            this.topToBottom = -1;
            this.bottomToTop = -1;
            this.bottomToBottom = -1;
            this.baselineToBaseline = -1;
            this.baselineToTop = -1;
            this.baselineToBottom = -1;
            this.startToEnd = -1;
            this.startToStart = -1;
            this.endToStart = -1;
            this.endToEnd = -1;
            this.horizontalBias = 0.5f;
            this.verticalBias = 0.5f;
            this.dimensionRatio = null;
            this.circleConstraint = -1;
            this.circleRadius = 0;
            this.circleAngle = 0.0f;
            this.editorAbsoluteX = -1;
            this.editorAbsoluteY = -1;
            this.orientation = -1;
            this.leftMargin = 0;
            this.rightMargin = 0;
            this.topMargin = 0;
            this.bottomMargin = 0;
            this.endMargin = 0;
            this.startMargin = 0;
            this.baselineMargin = 0;
            this.goneLeftMargin = Integer.MIN_VALUE;
            this.goneTopMargin = Integer.MIN_VALUE;
            this.goneRightMargin = Integer.MIN_VALUE;
            this.goneBottomMargin = Integer.MIN_VALUE;
            this.goneEndMargin = Integer.MIN_VALUE;
            this.goneStartMargin = Integer.MIN_VALUE;
            this.goneBaselineMargin = Integer.MIN_VALUE;
            this.verticalWeight = -1.0f;
            this.horizontalWeight = -1.0f;
            this.horizontalChainStyle = 0;
            this.verticalChainStyle = 0;
            this.widthDefault = 0;
            this.heightDefault = 0;
            this.widthMax = -1;
            this.heightMax = -1;
            this.widthMin = -1;
            this.heightMin = -1;
            this.widthPercent = 1.0f;
            this.heightPercent = 1.0f;
            this.mBarrierDirection = -1;
            this.mBarrierMargin = 0;
            this.mHelperType = -1;
            this.constrainedWidth = false;
            this.constrainedHeight = false;
            this.mBarrierAllowsGoneWidgets = true;
            this.mWrapBehavior = 0;
        }
        
        public void copyFrom(final Layout layout) {
            this.mIsGuideline = layout.mIsGuideline;
            this.mWidth = layout.mWidth;
            this.mApply = layout.mApply;
            this.mHeight = layout.mHeight;
            this.guideBegin = layout.guideBegin;
            this.guideEnd = layout.guideEnd;
            this.guidePercent = layout.guidePercent;
            this.leftToLeft = layout.leftToLeft;
            this.leftToRight = layout.leftToRight;
            this.rightToLeft = layout.rightToLeft;
            this.rightToRight = layout.rightToRight;
            this.topToTop = layout.topToTop;
            this.topToBottom = layout.topToBottom;
            this.bottomToTop = layout.bottomToTop;
            this.bottomToBottom = layout.bottomToBottom;
            this.baselineToBaseline = layout.baselineToBaseline;
            this.baselineToTop = layout.baselineToTop;
            this.baselineToBottom = layout.baselineToBottom;
            this.startToEnd = layout.startToEnd;
            this.startToStart = layout.startToStart;
            this.endToStart = layout.endToStart;
            this.endToEnd = layout.endToEnd;
            this.horizontalBias = layout.horizontalBias;
            this.verticalBias = layout.verticalBias;
            this.dimensionRatio = layout.dimensionRatio;
            this.circleConstraint = layout.circleConstraint;
            this.circleRadius = layout.circleRadius;
            this.circleAngle = layout.circleAngle;
            this.editorAbsoluteX = layout.editorAbsoluteX;
            this.editorAbsoluteY = layout.editorAbsoluteY;
            this.orientation = layout.orientation;
            this.leftMargin = layout.leftMargin;
            this.rightMargin = layout.rightMargin;
            this.topMargin = layout.topMargin;
            this.bottomMargin = layout.bottomMargin;
            this.endMargin = layout.endMargin;
            this.startMargin = layout.startMargin;
            this.baselineMargin = layout.baselineMargin;
            this.goneLeftMargin = layout.goneLeftMargin;
            this.goneTopMargin = layout.goneTopMargin;
            this.goneRightMargin = layout.goneRightMargin;
            this.goneBottomMargin = layout.goneBottomMargin;
            this.goneEndMargin = layout.goneEndMargin;
            this.goneStartMargin = layout.goneStartMargin;
            this.goneBaselineMargin = layout.goneBaselineMargin;
            this.verticalWeight = layout.verticalWeight;
            this.horizontalWeight = layout.horizontalWeight;
            this.horizontalChainStyle = layout.horizontalChainStyle;
            this.verticalChainStyle = layout.verticalChainStyle;
            this.widthDefault = layout.widthDefault;
            this.heightDefault = layout.heightDefault;
            this.widthMax = layout.widthMax;
            this.heightMax = layout.heightMax;
            this.widthMin = layout.widthMin;
            this.heightMin = layout.heightMin;
            this.widthPercent = layout.widthPercent;
            this.heightPercent = layout.heightPercent;
            this.mBarrierDirection = layout.mBarrierDirection;
            this.mBarrierMargin = layout.mBarrierMargin;
            this.mHelperType = layout.mHelperType;
            this.mConstraintTag = layout.mConstraintTag;
            final int[] mReferenceIds = layout.mReferenceIds;
            if (mReferenceIds != null) {
                this.mReferenceIds = Arrays.copyOf(mReferenceIds, mReferenceIds.length);
            }
            else {
                this.mReferenceIds = null;
            }
            this.mReferenceIdString = layout.mReferenceIdString;
            this.constrainedWidth = layout.constrainedWidth;
            this.constrainedHeight = layout.constrainedHeight;
            this.mBarrierAllowsGoneWidgets = layout.mBarrierAllowsGoneWidgets;
            this.mWrapBehavior = layout.mWrapBehavior;
        }
        
        public void dump(final MotionScene motionScene, final StringBuilder sb) {
            final Field[] declaredFields = this.getClass().getDeclaredFields();
            sb.append("\n");
            for (int i = 0; i < declaredFields.length; ++i) {
                final Field field = declaredFields[i];
                final String name = field.getName();
                if (!Modifier.isStatic(field.getModifiers())) {
                    try {
                        final Object value = field.get(this);
                        final Class<?> type = field.getType();
                        if (type == Integer.TYPE) {
                            Integer obj = (Integer)value;
                            if (obj != -1) {
                                final String lookUpConstraintName = motionScene.lookUpConstraintName(obj);
                                sb.append("    ");
                                sb.append(name);
                                sb.append(" = \"");
                                if (lookUpConstraintName != null) {
                                    obj = (Integer)lookUpConstraintName;
                                }
                                sb.append(obj);
                                sb.append("\"\n");
                            }
                        }
                        else if (type == Float.TYPE) {
                            final Float obj2 = (Float)value;
                            if (obj2 != -1.0f) {
                                sb.append("    ");
                                sb.append(name);
                                sb.append(" = \"");
                                sb.append(obj2);
                                sb.append("\"\n");
                            }
                        }
                    }
                    catch (final IllegalAccessException ex) {
                        ex.printStackTrace();
                    }
                }
            }
        }
        
        void fillFromAttributeList(final Context context, final AttributeSet set) {
            final TypedArray obtainStyledAttributes = context.obtainStyledAttributes(set, R.styleable.Layout);
            this.mApply = true;
            for (int indexCount = obtainStyledAttributes.getIndexCount(), i = 0; i < indexCount; ++i) {
                final int index = obtainStyledAttributes.getIndex(i);
                final int value = Layout.mapToConstant.get(index);
                if (value != 80) {
                    if (value != 81) {
                        if (value != 97) {
                            switch (value) {
                                default: {
                                    switch (value) {
                                        default: {
                                            switch (value) {
                                                default: {
                                                    switch (value) {
                                                        default: {
                                                            switch (value) {
                                                                default: {
                                                                    final StringBuilder sb = new StringBuilder();
                                                                    sb.append("Unknown attribute 0x");
                                                                    sb.append(Integer.toHexString(index));
                                                                    sb.append("   ");
                                                                    sb.append(Layout.mapToConstant.get(index));
                                                                    continue;
                                                                }
                                                                case 94: {
                                                                    this.goneBaselineMargin = obtainStyledAttributes.getDimensionPixelSize(index, this.goneBaselineMargin);
                                                                    continue;
                                                                }
                                                                case 93: {
                                                                    this.baselineMargin = obtainStyledAttributes.getDimensionPixelSize(index, this.baselineMargin);
                                                                    continue;
                                                                }
                                                                case 92: {
                                                                    this.baselineToBottom = lookupID(obtainStyledAttributes, index, this.baselineToBottom);
                                                                    continue;
                                                                }
                                                                case 91: {
                                                                    this.baselineToTop = lookupID(obtainStyledAttributes, index, this.baselineToTop);
                                                                    continue;
                                                                }
                                                            }
                                                            break;
                                                        }
                                                        case 71: {
                                                            continue;
                                                        }
                                                        case 77: {
                                                            this.mConstraintTag = obtainStyledAttributes.getString(index);
                                                            continue;
                                                        }
                                                        case 76: {
                                                            final StringBuilder sb2 = new StringBuilder();
                                                            sb2.append("unused attribute 0x");
                                                            sb2.append(Integer.toHexString(index));
                                                            sb2.append("   ");
                                                            sb2.append(Layout.mapToConstant.get(index));
                                                            continue;
                                                        }
                                                        case 75: {
                                                            this.mBarrierAllowsGoneWidgets = obtainStyledAttributes.getBoolean(index, this.mBarrierAllowsGoneWidgets);
                                                            continue;
                                                        }
                                                        case 74: {
                                                            this.mReferenceIdString = obtainStyledAttributes.getString(index);
                                                            continue;
                                                        }
                                                        case 73: {
                                                            this.mBarrierMargin = obtainStyledAttributes.getDimensionPixelSize(index, this.mBarrierMargin);
                                                            continue;
                                                        }
                                                        case 72: {
                                                            this.mBarrierDirection = obtainStyledAttributes.getInt(index, this.mBarrierDirection);
                                                            continue;
                                                        }
                                                        case 70: {
                                                            this.heightPercent = obtainStyledAttributes.getFloat(index, 1.0f);
                                                            continue;
                                                        }
                                                        case 69: {
                                                            this.widthPercent = obtainStyledAttributes.getFloat(index, 1.0f);
                                                            continue;
                                                        }
                                                    }
                                                    break;
                                                }
                                                case 63: {
                                                    this.circleAngle = obtainStyledAttributes.getFloat(index, this.circleAngle);
                                                    continue;
                                                }
                                                case 62: {
                                                    this.circleRadius = obtainStyledAttributes.getDimensionPixelSize(index, this.circleRadius);
                                                    continue;
                                                }
                                                case 61: {
                                                    this.circleConstraint = lookupID(obtainStyledAttributes, index, this.circleConstraint);
                                                    continue;
                                                }
                                            }
                                            break;
                                        }
                                        case 59: {
                                            this.heightMin = obtainStyledAttributes.getDimensionPixelSize(index, this.heightMin);
                                            continue;
                                        }
                                        case 58: {
                                            this.widthMin = obtainStyledAttributes.getDimensionPixelSize(index, this.widthMin);
                                            continue;
                                        }
                                        case 57: {
                                            this.heightMax = obtainStyledAttributes.getDimensionPixelSize(index, this.heightMax);
                                            continue;
                                        }
                                        case 56: {
                                            this.widthMax = obtainStyledAttributes.getDimensionPixelSize(index, this.widthMax);
                                            continue;
                                        }
                                        case 55: {
                                            this.heightDefault = obtainStyledAttributes.getInt(index, this.heightDefault);
                                            continue;
                                        }
                                        case 54: {
                                            this.widthDefault = obtainStyledAttributes.getInt(index, this.widthDefault);
                                            continue;
                                        }
                                    }
                                    break;
                                }
                                case 42: {
                                    ConstraintSet.parseDimensionConstraints(this, obtainStyledAttributes, index, 1);
                                    break;
                                }
                                case 41: {
                                    ConstraintSet.parseDimensionConstraints(this, obtainStyledAttributes, index, 0);
                                    break;
                                }
                                case 40: {
                                    this.verticalChainStyle = obtainStyledAttributes.getInt(index, this.verticalChainStyle);
                                    break;
                                }
                                case 39: {
                                    this.horizontalChainStyle = obtainStyledAttributes.getInt(index, this.horizontalChainStyle);
                                    break;
                                }
                                case 38: {
                                    this.verticalWeight = obtainStyledAttributes.getFloat(index, this.verticalWeight);
                                    break;
                                }
                                case 37: {
                                    this.horizontalWeight = obtainStyledAttributes.getFloat(index, this.horizontalWeight);
                                    break;
                                }
                                case 36: {
                                    this.verticalBias = obtainStyledAttributes.getFloat(index, this.verticalBias);
                                    break;
                                }
                                case 35: {
                                    this.topToTop = lookupID(obtainStyledAttributes, index, this.topToTop);
                                    break;
                                }
                                case 34: {
                                    this.topToBottom = lookupID(obtainStyledAttributes, index, this.topToBottom);
                                    break;
                                }
                                case 33: {
                                    this.topMargin = obtainStyledAttributes.getDimensionPixelSize(index, this.topMargin);
                                    break;
                                }
                                case 32: {
                                    this.startToStart = lookupID(obtainStyledAttributes, index, this.startToStart);
                                    break;
                                }
                                case 31: {
                                    this.startToEnd = lookupID(obtainStyledAttributes, index, this.startToEnd);
                                    break;
                                }
                                case 30: {
                                    this.startMargin = obtainStyledAttributes.getDimensionPixelSize(index, this.startMargin);
                                    break;
                                }
                                case 29: {
                                    this.rightToRight = lookupID(obtainStyledAttributes, index, this.rightToRight);
                                    break;
                                }
                                case 28: {
                                    this.rightToLeft = lookupID(obtainStyledAttributes, index, this.rightToLeft);
                                    break;
                                }
                                case 27: {
                                    this.rightMargin = obtainStyledAttributes.getDimensionPixelSize(index, this.rightMargin);
                                    break;
                                }
                                case 26: {
                                    this.orientation = obtainStyledAttributes.getInt(index, this.orientation);
                                    break;
                                }
                                case 25: {
                                    this.leftToRight = lookupID(obtainStyledAttributes, index, this.leftToRight);
                                    break;
                                }
                                case 24: {
                                    this.leftToLeft = lookupID(obtainStyledAttributes, index, this.leftToLeft);
                                    break;
                                }
                                case 23: {
                                    this.leftMargin = obtainStyledAttributes.getDimensionPixelSize(index, this.leftMargin);
                                    break;
                                }
                                case 22: {
                                    this.mWidth = obtainStyledAttributes.getLayoutDimension(index, this.mWidth);
                                    break;
                                }
                                case 21: {
                                    this.mHeight = obtainStyledAttributes.getLayoutDimension(index, this.mHeight);
                                    break;
                                }
                                case 20: {
                                    this.horizontalBias = obtainStyledAttributes.getFloat(index, this.horizontalBias);
                                    break;
                                }
                                case 19: {
                                    this.guidePercent = obtainStyledAttributes.getFloat(index, this.guidePercent);
                                    break;
                                }
                                case 18: {
                                    this.guideEnd = obtainStyledAttributes.getDimensionPixelOffset(index, this.guideEnd);
                                    break;
                                }
                                case 17: {
                                    this.guideBegin = obtainStyledAttributes.getDimensionPixelOffset(index, this.guideBegin);
                                    break;
                                }
                                case 16: {
                                    this.goneTopMargin = obtainStyledAttributes.getDimensionPixelSize(index, this.goneTopMargin);
                                    break;
                                }
                                case 15: {
                                    this.goneStartMargin = obtainStyledAttributes.getDimensionPixelSize(index, this.goneStartMargin);
                                    break;
                                }
                                case 14: {
                                    this.goneRightMargin = obtainStyledAttributes.getDimensionPixelSize(index, this.goneRightMargin);
                                    break;
                                }
                                case 13: {
                                    this.goneLeftMargin = obtainStyledAttributes.getDimensionPixelSize(index, this.goneLeftMargin);
                                    break;
                                }
                                case 12: {
                                    this.goneEndMargin = obtainStyledAttributes.getDimensionPixelSize(index, this.goneEndMargin);
                                    break;
                                }
                                case 11: {
                                    this.goneBottomMargin = obtainStyledAttributes.getDimensionPixelSize(index, this.goneBottomMargin);
                                    break;
                                }
                                case 10: {
                                    this.endToStart = lookupID(obtainStyledAttributes, index, this.endToStart);
                                    break;
                                }
                                case 9: {
                                    this.endToEnd = lookupID(obtainStyledAttributes, index, this.endToEnd);
                                    break;
                                }
                                case 8: {
                                    this.endMargin = obtainStyledAttributes.getDimensionPixelSize(index, this.endMargin);
                                    break;
                                }
                                case 7: {
                                    this.editorAbsoluteY = obtainStyledAttributes.getDimensionPixelOffset(index, this.editorAbsoluteY);
                                    break;
                                }
                                case 6: {
                                    this.editorAbsoluteX = obtainStyledAttributes.getDimensionPixelOffset(index, this.editorAbsoluteX);
                                    break;
                                }
                                case 5: {
                                    this.dimensionRatio = obtainStyledAttributes.getString(index);
                                    break;
                                }
                                case 4: {
                                    this.bottomToTop = lookupID(obtainStyledAttributes, index, this.bottomToTop);
                                    break;
                                }
                                case 3: {
                                    this.bottomToBottom = lookupID(obtainStyledAttributes, index, this.bottomToBottom);
                                    break;
                                }
                                case 2: {
                                    this.bottomMargin = obtainStyledAttributes.getDimensionPixelSize(index, this.bottomMargin);
                                    break;
                                }
                                case 1: {
                                    this.baselineToBaseline = lookupID(obtainStyledAttributes, index, this.baselineToBaseline);
                                    break;
                                }
                            }
                        }
                        else {
                            this.mWrapBehavior = obtainStyledAttributes.getInt(index, this.mWrapBehavior);
                        }
                    }
                    else {
                        this.constrainedHeight = obtainStyledAttributes.getBoolean(index, this.constrainedHeight);
                    }
                }
                else {
                    this.constrainedWidth = obtainStyledAttributes.getBoolean(index, this.constrainedWidth);
                }
            }
            obtainStyledAttributes.recycle();
        }
    }
    
    public static class Motion
    {
        private static final int ANIMATE_CIRCLE_ANGLE_TO = 6;
        private static final int ANIMATE_RELATIVE_TO = 5;
        private static final int INTERPOLATOR_REFERENCE_ID = -2;
        private static final int INTERPOLATOR_UNDEFINED = -3;
        private static final int MOTION_DRAW_PATH = 4;
        private static final int MOTION_STAGGER = 7;
        private static final int PATH_MOTION_ARC = 2;
        private static final int QUANTIZE_MOTION_INTERPOLATOR = 10;
        private static final int QUANTIZE_MOTION_PHASE = 9;
        private static final int QUANTIZE_MOTION_STEPS = 8;
        private static final int SPLINE_STRING = -1;
        private static final int TRANSITION_EASING = 3;
        private static final int TRANSITION_PATH_ROTATE = 1;
        private static SparseIntArray mapToConstant;
        public int mAnimateCircleAngleTo;
        public int mAnimateRelativeTo;
        public boolean mApply;
        public int mDrawPath;
        public float mMotionStagger;
        public int mPathMotionArc;
        public float mPathRotate;
        public int mPolarRelativeTo;
        public int mQuantizeInterpolatorID;
        public String mQuantizeInterpolatorString;
        public int mQuantizeInterpolatorType;
        public float mQuantizeMotionPhase;
        public int mQuantizeMotionSteps;
        public String mTransitionEasing;
        
        static {
            (Motion.mapToConstant = new SparseIntArray()).append(R.styleable.Motion_motionPathRotate, 1);
            Motion.mapToConstant.append(R.styleable.Motion_pathMotionArc, 2);
            Motion.mapToConstant.append(R.styleable.Motion_transitionEasing, 3);
            Motion.mapToConstant.append(R.styleable.Motion_drawPath, 4);
            Motion.mapToConstant.append(R.styleable.Motion_animateRelativeTo, 5);
            Motion.mapToConstant.append(R.styleable.Motion_animateCircleAngleTo, 6);
            Motion.mapToConstant.append(R.styleable.Motion_motionStagger, 7);
            Motion.mapToConstant.append(R.styleable.Motion_quantizeMotionSteps, 8);
            Motion.mapToConstant.append(R.styleable.Motion_quantizeMotionPhase, 9);
            Motion.mapToConstant.append(R.styleable.Motion_quantizeMotionInterpolator, 10);
        }
        
        public Motion() {
            this.mApply = false;
            this.mAnimateRelativeTo = -1;
            this.mAnimateCircleAngleTo = 0;
            this.mTransitionEasing = null;
            this.mPathMotionArc = -1;
            this.mDrawPath = 0;
            this.mMotionStagger = Float.NaN;
            this.mPolarRelativeTo = -1;
            this.mPathRotate = Float.NaN;
            this.mQuantizeMotionPhase = Float.NaN;
            this.mQuantizeMotionSteps = -1;
            this.mQuantizeInterpolatorString = null;
            this.mQuantizeInterpolatorType = -3;
            this.mQuantizeInterpolatorID = -1;
        }
        
        public void copyFrom(final Motion motion) {
            this.mApply = motion.mApply;
            this.mAnimateRelativeTo = motion.mAnimateRelativeTo;
            this.mTransitionEasing = motion.mTransitionEasing;
            this.mPathMotionArc = motion.mPathMotionArc;
            this.mDrawPath = motion.mDrawPath;
            this.mPathRotate = motion.mPathRotate;
            this.mMotionStagger = motion.mMotionStagger;
            this.mPolarRelativeTo = motion.mPolarRelativeTo;
        }
        
        void fillFromAttributeList(final Context context, final AttributeSet set) {
            final TypedArray obtainStyledAttributes = context.obtainStyledAttributes(set, R.styleable.Motion);
            this.mApply = true;
            for (int indexCount = obtainStyledAttributes.getIndexCount(), i = 0; i < indexCount; ++i) {
                final int index = obtainStyledAttributes.getIndex(i);
                switch (Motion.mapToConstant.get(index)) {
                    case 10: {
                        final int type = obtainStyledAttributes.peekValue(index).type;
                        if (type == 1) {
                            if ((this.mQuantizeInterpolatorID = obtainStyledAttributes.getResourceId(index, -1)) != -1) {
                                this.mQuantizeInterpolatorType = -2;
                                break;
                            }
                            break;
                        }
                        else {
                            if (type != 3) {
                                this.mQuantizeInterpolatorType = obtainStyledAttributes.getInteger(index, this.mQuantizeInterpolatorID);
                                break;
                            }
                            final String string = obtainStyledAttributes.getString(index);
                            this.mQuantizeInterpolatorString = string;
                            if (string.indexOf("/") > 0) {
                                this.mQuantizeInterpolatorID = obtainStyledAttributes.getResourceId(index, -1);
                                this.mQuantizeInterpolatorType = -2;
                                break;
                            }
                            this.mQuantizeInterpolatorType = -1;
                            break;
                        }
                        break;
                    }
                    case 9: {
                        this.mQuantizeMotionPhase = obtainStyledAttributes.getFloat(index, this.mQuantizeMotionPhase);
                        break;
                    }
                    case 8: {
                        this.mQuantizeMotionSteps = obtainStyledAttributes.getInteger(index, this.mQuantizeMotionSteps);
                        break;
                    }
                    case 7: {
                        this.mMotionStagger = obtainStyledAttributes.getFloat(index, this.mMotionStagger);
                        break;
                    }
                    case 6: {
                        this.mAnimateCircleAngleTo = obtainStyledAttributes.getInteger(index, this.mAnimateCircleAngleTo);
                        break;
                    }
                    case 5: {
                        this.mAnimateRelativeTo = lookupID(obtainStyledAttributes, index, this.mAnimateRelativeTo);
                        break;
                    }
                    case 4: {
                        this.mDrawPath = obtainStyledAttributes.getInt(index, 0);
                        break;
                    }
                    case 3: {
                        if (obtainStyledAttributes.peekValue(index).type == 3) {
                            this.mTransitionEasing = obtainStyledAttributes.getString(index);
                            break;
                        }
                        this.mTransitionEasing = Easing.NAMED_EASING[obtainStyledAttributes.getInteger(index, 0)];
                        break;
                    }
                    case 2: {
                        this.mPathMotionArc = obtainStyledAttributes.getInt(index, this.mPathMotionArc);
                        break;
                    }
                    case 1: {
                        this.mPathRotate = obtainStyledAttributes.getFloat(index, this.mPathRotate);
                        break;
                    }
                }
            }
            obtainStyledAttributes.recycle();
        }
    }
    
    public static class PropertySet
    {
        public float alpha;
        public boolean mApply;
        public float mProgress;
        public int mVisibilityMode;
        public int visibility;
        
        public PropertySet() {
            this.mApply = false;
            this.visibility = 0;
            this.mVisibilityMode = 0;
            this.alpha = 1.0f;
            this.mProgress = Float.NaN;
        }
        
        public void copyFrom(final PropertySet set) {
            this.mApply = set.mApply;
            this.visibility = set.visibility;
            this.alpha = set.alpha;
            this.mProgress = set.mProgress;
            this.mVisibilityMode = set.mVisibilityMode;
        }
        
        void fillFromAttributeList(final Context context, final AttributeSet set) {
            final TypedArray obtainStyledAttributes = context.obtainStyledAttributes(set, R.styleable.PropertySet);
            this.mApply = true;
            for (int indexCount = obtainStyledAttributes.getIndexCount(), i = 0; i < indexCount; ++i) {
                final int index = obtainStyledAttributes.getIndex(i);
                if (index == R.styleable.PropertySet_android_alpha) {
                    this.alpha = obtainStyledAttributes.getFloat(index, this.alpha);
                }
                else if (index == R.styleable.PropertySet_android_visibility) {
                    this.visibility = obtainStyledAttributes.getInt(index, this.visibility);
                    this.visibility = ConstraintSet.VISIBILITY_FLAGS[this.visibility];
                }
                else if (index == R.styleable.PropertySet_visibilityMode) {
                    this.mVisibilityMode = obtainStyledAttributes.getInt(index, this.mVisibilityMode);
                }
                else if (index == R.styleable.PropertySet_motionProgress) {
                    this.mProgress = obtainStyledAttributes.getFloat(index, this.mProgress);
                }
            }
            obtainStyledAttributes.recycle();
        }
    }
    
    public static class Transform
    {
        private static final int ELEVATION = 11;
        private static final int ROTATION = 1;
        private static final int ROTATION_X = 2;
        private static final int ROTATION_Y = 3;
        private static final int SCALE_X = 4;
        private static final int SCALE_Y = 5;
        private static final int TRANSFORM_PIVOT_TARGET = 12;
        private static final int TRANSFORM_PIVOT_X = 6;
        private static final int TRANSFORM_PIVOT_Y = 7;
        private static final int TRANSLATION_X = 8;
        private static final int TRANSLATION_Y = 9;
        private static final int TRANSLATION_Z = 10;
        private static SparseIntArray mapToConstant;
        public boolean applyElevation;
        public float elevation;
        public boolean mApply;
        public float rotation;
        public float rotationX;
        public float rotationY;
        public float scaleX;
        public float scaleY;
        public int transformPivotTarget;
        public float transformPivotX;
        public float transformPivotY;
        public float translationX;
        public float translationY;
        public float translationZ;
        
        static {
            (Transform.mapToConstant = new SparseIntArray()).append(R.styleable.Transform_android_rotation, 1);
            Transform.mapToConstant.append(R.styleable.Transform_android_rotationX, 2);
            Transform.mapToConstant.append(R.styleable.Transform_android_rotationY, 3);
            Transform.mapToConstant.append(R.styleable.Transform_android_scaleX, 4);
            Transform.mapToConstant.append(R.styleable.Transform_android_scaleY, 5);
            Transform.mapToConstant.append(R.styleable.Transform_android_transformPivotX, 6);
            Transform.mapToConstant.append(R.styleable.Transform_android_transformPivotY, 7);
            Transform.mapToConstant.append(R.styleable.Transform_android_translationX, 8);
            Transform.mapToConstant.append(R.styleable.Transform_android_translationY, 9);
            Transform.mapToConstant.append(R.styleable.Transform_android_translationZ, 10);
            Transform.mapToConstant.append(R.styleable.Transform_android_elevation, 11);
            Transform.mapToConstant.append(R.styleable.Transform_transformPivotTarget, 12);
        }
        
        public Transform() {
            this.mApply = false;
            this.rotation = 0.0f;
            this.rotationX = 0.0f;
            this.rotationY = 0.0f;
            this.scaleX = 1.0f;
            this.scaleY = 1.0f;
            this.transformPivotX = Float.NaN;
            this.transformPivotY = Float.NaN;
            this.transformPivotTarget = -1;
            this.translationX = 0.0f;
            this.translationY = 0.0f;
            this.translationZ = 0.0f;
            this.applyElevation = false;
            this.elevation = 0.0f;
        }
        
        public void copyFrom(final Transform transform) {
            this.mApply = transform.mApply;
            this.rotation = transform.rotation;
            this.rotationX = transform.rotationX;
            this.rotationY = transform.rotationY;
            this.scaleX = transform.scaleX;
            this.scaleY = transform.scaleY;
            this.transformPivotX = transform.transformPivotX;
            this.transformPivotY = transform.transformPivotY;
            this.transformPivotTarget = transform.transformPivotTarget;
            this.translationX = transform.translationX;
            this.translationY = transform.translationY;
            this.translationZ = transform.translationZ;
            this.applyElevation = transform.applyElevation;
            this.elevation = transform.elevation;
        }
        
        void fillFromAttributeList(final Context context, final AttributeSet set) {
            final TypedArray obtainStyledAttributes = context.obtainStyledAttributes(set, R.styleable.Transform);
            this.mApply = true;
            for (int indexCount = obtainStyledAttributes.getIndexCount(), i = 0; i < indexCount; ++i) {
                final int index = obtainStyledAttributes.getIndex(i);
                switch (Transform.mapToConstant.get(index)) {
                    case 12: {
                        this.transformPivotTarget = lookupID(obtainStyledAttributes, index, this.transformPivotTarget);
                        break;
                    }
                    case 11: {
                        this.applyElevation = true;
                        this.elevation = obtainStyledAttributes.getDimension(index, this.elevation);
                        break;
                    }
                    case 10: {
                        this.translationZ = obtainStyledAttributes.getDimension(index, this.translationZ);
                        break;
                    }
                    case 9: {
                        this.translationY = obtainStyledAttributes.getDimension(index, this.translationY);
                        break;
                    }
                    case 8: {
                        this.translationX = obtainStyledAttributes.getDimension(index, this.translationX);
                        break;
                    }
                    case 7: {
                        this.transformPivotY = obtainStyledAttributes.getDimension(index, this.transformPivotY);
                        break;
                    }
                    case 6: {
                        this.transformPivotX = obtainStyledAttributes.getDimension(index, this.transformPivotX);
                        break;
                    }
                    case 5: {
                        this.scaleY = obtainStyledAttributes.getFloat(index, this.scaleY);
                        break;
                    }
                    case 4: {
                        this.scaleX = obtainStyledAttributes.getFloat(index, this.scaleX);
                        break;
                    }
                    case 3: {
                        this.rotationY = obtainStyledAttributes.getFloat(index, this.rotationY);
                        break;
                    }
                    case 2: {
                        this.rotationX = obtainStyledAttributes.getFloat(index, this.rotationX);
                        break;
                    }
                    case 1: {
                        this.rotation = obtainStyledAttributes.getFloat(index, this.rotation);
                        break;
                    }
                }
            }
            obtainStyledAttributes.recycle();
        }
    }
    
    class WriteJsonEngine
    {
        private static final String SPACE = "       ";
        final String BASELINE;
        final String BOTTOM;
        final String END;
        final String LEFT;
        final String RIGHT;
        final String START;
        final String TOP;
        Context context;
        int flags;
        HashMap<Integer, String> idMap;
        ConstraintLayout layout;
        final ConstraintSet this$0;
        int unknownCount;
        Writer writer;
        
        WriteJsonEngine(final ConstraintSet this$0, final Writer writer, final ConstraintLayout layout, final int flags) throws IOException {
            this.this$0 = this$0;
            this.unknownCount = 0;
            this.LEFT = "'left'";
            this.RIGHT = "'right'";
            this.BASELINE = "'baseline'";
            this.BOTTOM = "'bottom'";
            this.TOP = "'top'";
            this.START = "'start'";
            this.END = "'end'";
            this.idMap = new HashMap<Integer, String>();
            this.writer = writer;
            this.layout = layout;
            this.context = ((View)layout).getContext();
            this.flags = flags;
        }
        
        private void writeDimension(final String s, final int i, final int n, final float n2, final int j, final int k, final boolean b) throws IOException {
            if (i == 0) {
                if (k != -1 || j != -1) {
                    if (n != 0) {
                        if (n == 1) {
                            final Writer writer = this.writer;
                            final StringBuilder sb = new StringBuilder();
                            sb.append("       ");
                            sb.append(s);
                            sb.append(": {'wrap' ,");
                            sb.append(j);
                            sb.append(", ");
                            sb.append(k);
                            sb.append("}\n");
                            writer.write(sb.toString());
                            return;
                        }
                        if (n == 2) {
                            final Writer writer2 = this.writer;
                            final StringBuilder sb2 = new StringBuilder();
                            sb2.append("       ");
                            sb2.append(s);
                            sb2.append(": {'");
                            sb2.append(n2);
                            sb2.append("'% ,");
                            sb2.append(j);
                            sb2.append(", ");
                            sb2.append(k);
                            sb2.append("}\n");
                            writer2.write(sb2.toString());
                        }
                    }
                    else {
                        final Writer writer3 = this.writer;
                        final StringBuilder sb3 = new StringBuilder();
                        sb3.append("       ");
                        sb3.append(s);
                        sb3.append(": {'spread' ,");
                        sb3.append(j);
                        sb3.append(", ");
                        sb3.append(k);
                        sb3.append("}\n");
                        writer3.write(sb3.toString());
                    }
                    return;
                }
                if (n == 1) {
                    final Writer writer4 = this.writer;
                    final StringBuilder sb4 = new StringBuilder();
                    sb4.append("       ");
                    sb4.append(s);
                    sb4.append(": '???????????',\n");
                    writer4.write(sb4.toString());
                    return;
                }
                if (n == 2) {
                    final Writer writer5 = this.writer;
                    final StringBuilder sb5 = new StringBuilder();
                    sb5.append("       ");
                    sb5.append(s);
                    sb5.append(": '");
                    sb5.append(n2);
                    sb5.append("%',\n");
                    writer5.write(sb5.toString());
                }
            }
            else if (i == -2) {
                final Writer writer6 = this.writer;
                final StringBuilder sb6 = new StringBuilder();
                sb6.append("       ");
                sb6.append(s);
                sb6.append(": 'wrap'\n");
                writer6.write(sb6.toString());
            }
            else if (i == -1) {
                final Writer writer7 = this.writer;
                final StringBuilder sb7 = new StringBuilder();
                sb7.append("       ");
                sb7.append(s);
                sb7.append(": 'parent'\n");
                writer7.write(sb7.toString());
            }
            else {
                final Writer writer8 = this.writer;
                final StringBuilder sb8 = new StringBuilder();
                sb8.append("       ");
                sb8.append(s);
                sb8.append(": ");
                sb8.append(i);
                sb8.append(",\n");
                writer8.write(sb8.toString());
            }
        }
        
        private void writeGuideline(final int n, final int n2, final int n3, final float n4) {
        }
        
        String getName(final int i) {
            if (this.idMap.containsKey(i)) {
                final StringBuilder sb = new StringBuilder();
                sb.append("'");
                sb.append(this.idMap.get(i));
                sb.append("'");
                return sb.toString();
            }
            if (i == 0) {
                return "'parent'";
            }
            final String lookup = this.lookup(i);
            this.idMap.put(i, lookup);
            final StringBuilder sb2 = new StringBuilder();
            sb2.append("'");
            sb2.append(lookup);
            sb2.append("'");
            return sb2.toString();
        }
        
        String lookup(int n) {
            Label_0017: {
                if (n == -1) {
                    break Label_0017;
                }
                try {
                    return this.context.getResources().getResourceEntryName(n);
                    final StringBuilder sb = new StringBuilder();
                    sb.append("unknown");
                    n = this.unknownCount + 1;
                    sb.append(this.unknownCount = n);
                    return sb.toString();
                }
                catch (final Exception ex) {
                    final StringBuilder sb2 = new StringBuilder();
                    sb2.append("unknown");
                    n = this.unknownCount + 1;
                    sb2.append(this.unknownCount = n);
                    return sb2.toString();
                }
            }
        }
        
        void writeCircle(final int n, final float f, final int i) throws IOException {
            if (n == -1) {
                return;
            }
            this.writer.write("       circle");
            this.writer.write(":[");
            this.writer.write(this.getName(n));
            final Writer writer = this.writer;
            final StringBuilder sb = new StringBuilder();
            sb.append(", ");
            sb.append(f);
            writer.write(sb.toString());
            final Writer writer2 = this.writer;
            final StringBuilder sb2 = new StringBuilder();
            sb2.append(i);
            sb2.append("]");
            writer2.write(sb2.toString());
        }
        
        void writeConstraint(final String str, final int n, final String str2, final int i, final int n2) throws IOException {
            if (n == -1) {
                return;
            }
            final Writer writer = this.writer;
            final StringBuilder sb = new StringBuilder();
            sb.append("       ");
            sb.append(str);
            writer.write(sb.toString());
            this.writer.write(":[");
            this.writer.write(this.getName(n));
            this.writer.write(" , ");
            this.writer.write(str2);
            if (i != 0) {
                final Writer writer2 = this.writer;
                final StringBuilder sb2 = new StringBuilder();
                sb2.append(" , ");
                sb2.append(i);
                writer2.write(sb2.toString());
            }
            this.writer.write("],\n");
        }
        
        void writeLayout() throws IOException {
            this.writer.write("\n'ConstraintSet':{\n");
            for (final Integer key : this.this$0.mConstraints.keySet()) {
                final Constraint constraint = this.this$0.mConstraints.get(key);
                final String name = this.getName(key);
                final Writer writer = this.writer;
                final StringBuilder sb = new StringBuilder();
                sb.append(name);
                sb.append(":{\n");
                writer.write(sb.toString());
                final Layout layout = constraint.layout;
                this.writeDimension("height", layout.mHeight, layout.heightDefault, layout.heightPercent, layout.heightMin, layout.heightMax, layout.constrainedHeight);
                this.writeDimension("width", layout.mWidth, layout.widthDefault, layout.widthPercent, layout.widthMin, layout.widthMax, layout.constrainedWidth);
                this.writeConstraint("'left'", layout.leftToLeft, "'left'", layout.leftMargin, layout.goneLeftMargin);
                this.writeConstraint("'left'", layout.leftToRight, "'right'", layout.leftMargin, layout.goneLeftMargin);
                this.writeConstraint("'right'", layout.rightToLeft, "'left'", layout.rightMargin, layout.goneRightMargin);
                this.writeConstraint("'right'", layout.rightToRight, "'right'", layout.rightMargin, layout.goneRightMargin);
                this.writeConstraint("'baseline'", layout.baselineToBaseline, "'baseline'", -1, layout.goneBaselineMargin);
                this.writeConstraint("'baseline'", layout.baselineToTop, "'top'", -1, layout.goneBaselineMargin);
                this.writeConstraint("'baseline'", layout.baselineToBottom, "'bottom'", -1, layout.goneBaselineMargin);
                this.writeConstraint("'top'", layout.topToBottom, "'bottom'", layout.topMargin, layout.goneTopMargin);
                this.writeConstraint("'top'", layout.topToTop, "'top'", layout.topMargin, layout.goneTopMargin);
                this.writeConstraint("'bottom'", layout.bottomToBottom, "'bottom'", layout.bottomMargin, layout.goneBottomMargin);
                this.writeConstraint("'bottom'", layout.bottomToTop, "'top'", layout.bottomMargin, layout.goneBottomMargin);
                this.writeConstraint("'start'", layout.startToStart, "'start'", layout.startMargin, layout.goneStartMargin);
                this.writeConstraint("'start'", layout.startToEnd, "'end'", layout.startMargin, layout.goneStartMargin);
                this.writeConstraint("'end'", layout.endToStart, "'start'", layout.endMargin, layout.goneEndMargin);
                this.writeConstraint("'end'", layout.endToEnd, "'end'", layout.endMargin, layout.goneEndMargin);
                this.writeVariable("'horizontalBias'", layout.horizontalBias, 0.5f);
                this.writeVariable("'verticalBias'", layout.verticalBias, 0.5f);
                this.writeCircle(layout.circleConstraint, layout.circleAngle, layout.circleRadius);
                this.writeGuideline(layout.orientation, layout.guideBegin, layout.guideEnd, layout.guidePercent);
                this.writeVariable("'dimensionRatio'", layout.dimensionRatio);
                this.writeVariable("'barrierMargin'", layout.mBarrierMargin);
                this.writeVariable("'type'", layout.mHelperType);
                this.writeVariable("'ReferenceId'", layout.mReferenceIdString);
                this.writeVariable("'mBarrierAllowsGoneWidgets'", layout.mBarrierAllowsGoneWidgets, true);
                this.writeVariable("'WrapBehavior'", layout.mWrapBehavior);
                this.writeVariable("'verticalWeight'", layout.verticalWeight);
                this.writeVariable("'horizontalWeight'", layout.horizontalWeight);
                this.writeVariable("'horizontalChainStyle'", layout.horizontalChainStyle);
                this.writeVariable("'verticalChainStyle'", layout.verticalChainStyle);
                this.writeVariable("'barrierDirection'", layout.mBarrierDirection);
                final int[] mReferenceIds = layout.mReferenceIds;
                if (mReferenceIds != null) {
                    this.writeVariable("'ReferenceIds'", mReferenceIds);
                }
                this.writer.write("}\n");
            }
            this.writer.write("}\n");
        }
        
        void writeVariable(final String str, final float f) throws IOException {
            if (f == -1.0f) {
                return;
            }
            final Writer writer = this.writer;
            final StringBuilder sb = new StringBuilder();
            sb.append("       ");
            sb.append(str);
            writer.write(sb.toString());
            final Writer writer2 = this.writer;
            final StringBuilder sb2 = new StringBuilder();
            sb2.append(": ");
            sb2.append(f);
            writer2.write(sb2.toString());
            this.writer.write(",\n");
        }
        
        void writeVariable(final String str, final float f, final float n) throws IOException {
            if (f == n) {
                return;
            }
            final Writer writer = this.writer;
            final StringBuilder sb = new StringBuilder();
            sb.append("       ");
            sb.append(str);
            writer.write(sb.toString());
            final Writer writer2 = this.writer;
            final StringBuilder sb2 = new StringBuilder();
            sb2.append(": ");
            sb2.append(f);
            writer2.write(sb2.toString());
            this.writer.write(",\n");
        }
        
        void writeVariable(final String str, final int i) throws IOException {
            if (i != 0) {
                if (i != -1) {
                    final Writer writer = this.writer;
                    final StringBuilder sb = new StringBuilder();
                    sb.append("       ");
                    sb.append(str);
                    writer.write(sb.toString());
                    this.writer.write(":");
                    final Writer writer2 = this.writer;
                    final StringBuilder sb2 = new StringBuilder();
                    sb2.append(", ");
                    sb2.append(i);
                    writer2.write(sb2.toString());
                    this.writer.write("\n");
                }
            }
        }
        
        void writeVariable(final String str, final String str2) throws IOException {
            if (str2 == null) {
                return;
            }
            final Writer writer = this.writer;
            final StringBuilder sb = new StringBuilder();
            sb.append("       ");
            sb.append(str);
            writer.write(sb.toString());
            this.writer.write(":");
            final Writer writer2 = this.writer;
            final StringBuilder sb2 = new StringBuilder();
            sb2.append(", ");
            sb2.append(str2);
            writer2.write(sb2.toString());
            this.writer.write("\n");
        }
        
        void writeVariable(final String str, final boolean b) throws IOException {
            if (!b) {
                return;
            }
            final Writer writer = this.writer;
            final StringBuilder sb = new StringBuilder();
            sb.append("       ");
            sb.append(str);
            writer.write(sb.toString());
            final Writer writer2 = this.writer;
            final StringBuilder sb2 = new StringBuilder();
            sb2.append(": ");
            sb2.append(b);
            writer2.write(sb2.toString());
            this.writer.write(",\n");
        }
        
        void writeVariable(final String str, final boolean b, final boolean b2) throws IOException {
            if (b == b2) {
                return;
            }
            final Writer writer = this.writer;
            final StringBuilder sb = new StringBuilder();
            sb.append("       ");
            sb.append(str);
            writer.write(sb.toString());
            final Writer writer2 = this.writer;
            final StringBuilder sb2 = new StringBuilder();
            sb2.append(": ");
            sb2.append(b);
            writer2.write(sb2.toString());
            this.writer.write(",\n");
        }
        
        void writeVariable(String s, final int[] array) throws IOException {
            if (array == null) {
                return;
            }
            final Writer writer = this.writer;
            final StringBuilder sb = new StringBuilder();
            sb.append("       ");
            sb.append(s);
            writer.write(sb.toString());
            this.writer.write(": ");
            for (int i = 0; i < array.length; ++i) {
                final Writer writer2 = this.writer;
                final StringBuilder sb2 = new StringBuilder();
                if (i == 0) {
                    s = "[";
                }
                else {
                    s = ", ";
                }
                sb2.append(s);
                sb2.append(this.getName(array[i]));
                writer2.write(sb2.toString());
            }
            this.writer.write("],\n");
        }
    }
    
    class WriteXmlEngine
    {
        private static final String SPACE = "\n       ";
        final String BASELINE;
        final String BOTTOM;
        final String END;
        final String LEFT;
        final String RIGHT;
        final String START;
        final String TOP;
        Context context;
        int flags;
        HashMap<Integer, String> idMap;
        ConstraintLayout layout;
        final ConstraintSet this$0;
        int unknownCount;
        Writer writer;
        
        WriteXmlEngine(final ConstraintSet this$0, final Writer writer, final ConstraintLayout layout, final int flags) throws IOException {
            this.this$0 = this$0;
            this.unknownCount = 0;
            this.LEFT = "'left'";
            this.RIGHT = "'right'";
            this.BASELINE = "'baseline'";
            this.BOTTOM = "'bottom'";
            this.TOP = "'top'";
            this.START = "'start'";
            this.END = "'end'";
            this.idMap = new HashMap<Integer, String>();
            this.writer = writer;
            this.layout = layout;
            this.context = ((View)layout).getContext();
            this.flags = flags;
        }
        
        private void writeBaseDimension(final String str, final int i, final int n) throws IOException {
            if (i != n) {
                if (i == -2) {
                    final Writer writer = this.writer;
                    final StringBuilder sb = new StringBuilder();
                    sb.append("\n       ");
                    sb.append(str);
                    sb.append("=\"wrap_content\"");
                    writer.write(sb.toString());
                }
                else if (i == -1) {
                    final Writer writer2 = this.writer;
                    final StringBuilder sb2 = new StringBuilder();
                    sb2.append("\n       ");
                    sb2.append(str);
                    sb2.append("=\"match_parent\"");
                    writer2.write(sb2.toString());
                }
                else {
                    final Writer writer3 = this.writer;
                    final StringBuilder sb3 = new StringBuilder();
                    sb3.append("\n       ");
                    sb3.append(str);
                    sb3.append("=\"");
                    sb3.append(i);
                    sb3.append("dp\"");
                    writer3.write(sb3.toString());
                }
            }
        }
        
        private void writeBoolen(final String str, final boolean b, final boolean b2) throws IOException {
            if (b != b2) {
                final Writer writer = this.writer;
                final StringBuilder sb = new StringBuilder();
                sb.append("\n       ");
                sb.append(str);
                sb.append("=\"");
                sb.append(b);
                sb.append("dp\"");
                writer.write(sb.toString());
            }
        }
        
        private void writeDimension(final String str, final int i, final int n) throws IOException {
            if (i != n) {
                final Writer writer = this.writer;
                final StringBuilder sb = new StringBuilder();
                sb.append("\n       ");
                sb.append(str);
                sb.append("=\"");
                sb.append(i);
                sb.append("dp\"");
                writer.write(sb.toString());
            }
        }
        
        private void writeEnum(final String str, final int n, final String[] array, final int n2) throws IOException {
            if (n != n2) {
                final Writer writer = this.writer;
                final StringBuilder sb = new StringBuilder();
                sb.append("\n       ");
                sb.append(str);
                sb.append("=\"");
                sb.append(array[n]);
                sb.append("\"");
                writer.write(sb.toString());
            }
        }
        
        String getName(final int i) {
            if (this.idMap.containsKey(i)) {
                final StringBuilder sb = new StringBuilder();
                sb.append("@+id/");
                sb.append(this.idMap.get(i));
                sb.append("");
                return sb.toString();
            }
            if (i == 0) {
                return "parent";
            }
            final String lookup = this.lookup(i);
            this.idMap.put(i, lookup);
            final StringBuilder sb2 = new StringBuilder();
            sb2.append("@+id/");
            sb2.append(lookup);
            sb2.append("");
            return sb2.toString();
        }
        
        String lookup(int n) {
            Label_0017: {
                if (n == -1) {
                    break Label_0017;
                }
                try {
                    return this.context.getResources().getResourceEntryName(n);
                    final StringBuilder sb = new StringBuilder();
                    sb.append("unknown");
                    n = this.unknownCount + 1;
                    sb.append(this.unknownCount = n);
                    return sb.toString();
                }
                catch (final Exception ex) {
                    final StringBuilder sb2 = new StringBuilder();
                    sb2.append("unknown");
                    n = this.unknownCount + 1;
                    sb2.append(this.unknownCount = n);
                    return sb2.toString();
                }
            }
        }
        
        void writeCircle(final int n, final float f, final int i) throws IOException {
            if (n == -1) {
                return;
            }
            this.writer.write("circle");
            this.writer.write(":[");
            this.writer.write(this.getName(n));
            final Writer writer = this.writer;
            final StringBuilder sb = new StringBuilder();
            sb.append(", ");
            sb.append(f);
            writer.write(sb.toString());
            final Writer writer2 = this.writer;
            final StringBuilder sb2 = new StringBuilder();
            sb2.append(i);
            sb2.append("]");
            writer2.write(sb2.toString());
        }
        
        void writeConstraint(final String str, final int n, final String str2, final int i, final int n2) throws IOException {
            if (n == -1) {
                return;
            }
            final Writer writer = this.writer;
            final StringBuilder sb = new StringBuilder();
            sb.append("\n       ");
            sb.append(str);
            writer.write(sb.toString());
            this.writer.write(":[");
            this.writer.write(this.getName(n));
            this.writer.write(" , ");
            this.writer.write(str2);
            if (i != 0) {
                final Writer writer2 = this.writer;
                final StringBuilder sb2 = new StringBuilder();
                sb2.append(" , ");
                sb2.append(i);
                writer2.write(sb2.toString());
            }
            this.writer.write("],\n");
        }
        
        void writeLayout() throws IOException {
            this.writer.write("\n<ConstraintSet>\n");
            for (final Integer key : this.this$0.mConstraints.keySet()) {
                final Constraint constraint = this.this$0.mConstraints.get(key);
                final String name = this.getName(key);
                this.writer.write("  <Constraint");
                final Writer writer = this.writer;
                final StringBuilder sb = new StringBuilder();
                sb.append("\n       android:id=\"");
                sb.append(name);
                sb.append("\"");
                writer.write(sb.toString());
                final Layout layout = constraint.layout;
                this.writeBaseDimension("android:layout_width", layout.mWidth, -5);
                this.writeBaseDimension("android:layout_height", layout.mHeight, -5);
                this.writeVariable("app:layout_constraintGuide_begin", (float)layout.guideBegin, -1.0f);
                this.writeVariable("app:layout_constraintGuide_end", (float)layout.guideEnd, -1.0f);
                this.writeVariable("app:layout_constraintGuide_percent", layout.guidePercent, -1.0f);
                this.writeVariable("app:layout_constraintHorizontal_bias", layout.horizontalBias, 0.5f);
                this.writeVariable("app:layout_constraintVertical_bias", layout.verticalBias, 0.5f);
                this.writeVariable("app:layout_constraintDimensionRatio", layout.dimensionRatio, null);
                this.writeXmlConstraint("app:layout_constraintCircle", layout.circleConstraint);
                this.writeVariable("app:layout_constraintCircleRadius", (float)layout.circleRadius, 0.0f);
                this.writeVariable("app:layout_constraintCircleAngle", layout.circleAngle, 0.0f);
                this.writeVariable("android:orientation", (float)layout.orientation, -1.0f);
                this.writeVariable("app:layout_constraintVertical_weight", layout.verticalWeight, -1.0f);
                this.writeVariable("app:layout_constraintHorizontal_weight", layout.horizontalWeight, -1.0f);
                this.writeVariable("app:layout_constraintHorizontal_chainStyle", (float)layout.horizontalChainStyle, 0.0f);
                this.writeVariable("app:layout_constraintVertical_chainStyle", (float)layout.verticalChainStyle, 0.0f);
                this.writeVariable("app:barrierDirection", (float)layout.mBarrierDirection, -1.0f);
                this.writeVariable("app:barrierMargin", (float)layout.mBarrierMargin, 0.0f);
                this.writeDimension("app:layout_marginLeft", layout.leftMargin, 0);
                this.writeDimension("app:layout_goneMarginLeft", layout.goneLeftMargin, Integer.MIN_VALUE);
                this.writeDimension("app:layout_marginRight", layout.rightMargin, 0);
                this.writeDimension("app:layout_goneMarginRight", layout.goneRightMargin, Integer.MIN_VALUE);
                this.writeDimension("app:layout_marginStart", layout.startMargin, 0);
                this.writeDimension("app:layout_goneMarginStart", layout.goneStartMargin, Integer.MIN_VALUE);
                this.writeDimension("app:layout_marginEnd", layout.endMargin, 0);
                this.writeDimension("app:layout_goneMarginEnd", layout.goneEndMargin, Integer.MIN_VALUE);
                this.writeDimension("app:layout_marginTop", layout.topMargin, 0);
                this.writeDimension("app:layout_goneMarginTop", layout.goneTopMargin, Integer.MIN_VALUE);
                this.writeDimension("app:layout_marginBottom", layout.bottomMargin, 0);
                this.writeDimension("app:layout_goneMarginBottom", layout.goneBottomMargin, Integer.MIN_VALUE);
                this.writeDimension("app:goneBaselineMargin", layout.goneBaselineMargin, Integer.MIN_VALUE);
                this.writeDimension("app:baselineMargin", layout.baselineMargin, 0);
                this.writeBoolen("app:layout_constrainedWidth", layout.constrainedWidth, false);
                this.writeBoolen("app:layout_constrainedHeight", layout.constrainedHeight, false);
                this.writeBoolen("app:barrierAllowsGoneWidgets", layout.mBarrierAllowsGoneWidgets, true);
                this.writeVariable("app:layout_wrapBehaviorInParent", (float)layout.mWrapBehavior, 0.0f);
                this.writeXmlConstraint("app:baselineToBaseline", layout.baselineToBaseline);
                this.writeXmlConstraint("app:baselineToBottom", layout.baselineToBottom);
                this.writeXmlConstraint("app:baselineToTop", layout.baselineToTop);
                this.writeXmlConstraint("app:layout_constraintBottom_toBottomOf", layout.bottomToBottom);
                this.writeXmlConstraint("app:layout_constraintBottom_toTopOf", layout.bottomToTop);
                this.writeXmlConstraint("app:layout_constraintEnd_toEndOf", layout.endToEnd);
                this.writeXmlConstraint("app:layout_constraintEnd_toStartOf", layout.endToStart);
                this.writeXmlConstraint("app:layout_constraintLeft_toLeftOf", layout.leftToLeft);
                this.writeXmlConstraint("app:layout_constraintLeft_toRightOf", layout.leftToRight);
                this.writeXmlConstraint("app:layout_constraintRight_toLeftOf", layout.rightToLeft);
                this.writeXmlConstraint("app:layout_constraintRight_toRightOf", layout.rightToRight);
                this.writeXmlConstraint("app:layout_constraintStart_toEndOf", layout.startToEnd);
                this.writeXmlConstraint("app:layout_constraintStart_toStartOf", layout.startToStart);
                this.writeXmlConstraint("app:layout_constraintTop_toBottomOf", layout.topToBottom);
                this.writeXmlConstraint("app:layout_constraintTop_toTopOf", layout.topToTop);
                final String[] array = { "spread", "wrap", "percent" };
                this.writeEnum("app:layout_constraintHeight_default", layout.heightDefault, array, 0);
                this.writeVariable("app:layout_constraintHeight_percent", layout.heightPercent, 1.0f);
                this.writeDimension("app:layout_constraintHeight_min", layout.heightMin, 0);
                this.writeDimension("app:layout_constraintHeight_max", layout.heightMax, 0);
                this.writeBoolen("android:layout_constrainedHeight", layout.constrainedHeight, false);
                this.writeEnum("app:layout_constraintWidth_default", layout.widthDefault, array, 0);
                this.writeVariable("app:layout_constraintWidth_percent", layout.widthPercent, 1.0f);
                this.writeDimension("app:layout_constraintWidth_min", layout.widthMin, 0);
                this.writeDimension("app:layout_constraintWidth_max", layout.widthMax, 0);
                this.writeBoolen("android:layout_constrainedWidth", layout.constrainedWidth, false);
                this.writeVariable("app:layout_constraintVertical_weight", layout.verticalWeight, -1.0f);
                this.writeVariable("app:layout_constraintHorizontal_weight", layout.horizontalWeight, -1.0f);
                this.writeVariable("app:layout_constraintHorizontal_chainStyle", layout.horizontalChainStyle);
                this.writeVariable("app:layout_constraintVertical_chainStyle", layout.verticalChainStyle);
                this.writeEnum("app:barrierDirection", layout.mBarrierDirection, new String[] { "left", "right", "top", "bottom", "start", "end" }, -1);
                this.writeVariable("app:layout_constraintTag", layout.mConstraintTag, null);
                final int[] mReferenceIds = layout.mReferenceIds;
                if (mReferenceIds != null) {
                    this.writeVariable("'ReferenceIds'", mReferenceIds);
                }
                this.writer.write(" />\n");
            }
            this.writer.write("</ConstraintSet>\n");
        }
        
        void writeVariable(final String str, final float f, final float n) throws IOException {
            if (f == n) {
                return;
            }
            final Writer writer = this.writer;
            final StringBuilder sb = new StringBuilder();
            sb.append("\n       ");
            sb.append(str);
            writer.write(sb.toString());
            final Writer writer2 = this.writer;
            final StringBuilder sb2 = new StringBuilder();
            sb2.append("=\"");
            sb2.append(f);
            sb2.append("\"");
            writer2.write(sb2.toString());
        }
        
        void writeVariable(final String str, final int i) throws IOException {
            if (i != 0) {
                if (i != -1) {
                    final Writer writer = this.writer;
                    final StringBuilder sb = new StringBuilder();
                    sb.append("\n       ");
                    sb.append(str);
                    sb.append("=\"");
                    sb.append(i);
                    sb.append("\"\n");
                    writer.write(sb.toString());
                }
            }
        }
        
        void writeVariable(final String str, final String str2) throws IOException {
            if (str2 == null) {
                return;
            }
            this.writer.write(str);
            this.writer.write(":");
            final Writer writer = this.writer;
            final StringBuilder sb = new StringBuilder();
            sb.append(", ");
            sb.append(str2);
            writer.write(sb.toString());
            this.writer.write("\n");
        }
        
        void writeVariable(final String str, final String str2, final String anObject) throws IOException {
            if (str2 != null) {
                if (!str2.equals(anObject)) {
                    final Writer writer = this.writer;
                    final StringBuilder sb = new StringBuilder();
                    sb.append("\n       ");
                    sb.append(str);
                    writer.write(sb.toString());
                    final Writer writer2 = this.writer;
                    final StringBuilder sb2 = new StringBuilder();
                    sb2.append("=\"");
                    sb2.append(str2);
                    sb2.append("\"");
                    writer2.write(sb2.toString());
                }
            }
        }
        
        void writeVariable(String s, final int[] array) throws IOException {
            if (array == null) {
                return;
            }
            final Writer writer = this.writer;
            final StringBuilder sb = new StringBuilder();
            sb.append("\n       ");
            sb.append(s);
            writer.write(sb.toString());
            this.writer.write(":");
            for (int i = 0; i < array.length; ++i) {
                final Writer writer2 = this.writer;
                final StringBuilder sb2 = new StringBuilder();
                if (i == 0) {
                    s = "[";
                }
                else {
                    s = ", ";
                }
                sb2.append(s);
                sb2.append(this.getName(array[i]));
                writer2.write(sb2.toString());
            }
            this.writer.write("],\n");
        }
        
        void writeXmlConstraint(final String str, final int n) throws IOException {
            if (n == -1) {
                return;
            }
            final Writer writer = this.writer;
            final StringBuilder sb = new StringBuilder();
            sb.append("\n       ");
            sb.append(str);
            writer.write(sb.toString());
            final Writer writer2 = this.writer;
            final StringBuilder sb2 = new StringBuilder();
            sb2.append("=\"");
            sb2.append(this.getName(n));
            sb2.append("\"");
            writer2.write(sb2.toString());
        }
    }
}

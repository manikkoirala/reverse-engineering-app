// 
// Decompiled by Procyon v0.6.0
// 

package androidx.constraintlayout.widget;

import android.view.ViewGroup;
import android.view.ViewGroup$LayoutParams;
import android.view.View;

public class ConstraintProperties
{
    public static final int BASELINE = 5;
    public static final int BOTTOM = 4;
    public static final int END = 7;
    public static final int LEFT = 1;
    public static final int MATCH_CONSTRAINT = 0;
    public static final int MATCH_CONSTRAINT_SPREAD = 0;
    public static final int MATCH_CONSTRAINT_WRAP = 1;
    public static final int PARENT_ID = 0;
    public static final int RIGHT = 2;
    public static final int START = 6;
    public static final int TOP = 3;
    public static final int UNSET = -1;
    public static final int WRAP_CONTENT = -2;
    ConstraintLayout.LayoutParams mParams;
    View mView;
    
    public ConstraintProperties(final View mView) {
        final ViewGroup$LayoutParams layoutParams = mView.getLayoutParams();
        if (layoutParams instanceof ConstraintLayout.LayoutParams) {
            this.mParams = (ConstraintLayout.LayoutParams)layoutParams;
            this.mView = mView;
            return;
        }
        throw new RuntimeException("Only children of ConstraintLayout.LayoutParams supported");
    }
    
    private String sideToString(final int n) {
        switch (n) {
            default: {
                return "undefined";
            }
            case 7: {
                return "end";
            }
            case 6: {
                return "start";
            }
            case 5: {
                return "baseline";
            }
            case 4: {
                return "bottom";
            }
            case 3: {
                return "top";
            }
            case 2: {
                return "right";
            }
            case 1: {
                return "left";
            }
        }
    }
    
    public ConstraintProperties addToHorizontalChain(final int n, final int n2) {
        int n3;
        if (n == 0) {
            n3 = 1;
        }
        else {
            n3 = 2;
        }
        this.connect(1, n, n3, 0);
        int n4;
        if (n2 == 0) {
            n4 = 2;
        }
        else {
            n4 = 1;
        }
        this.connect(2, n2, n4, 0);
        if (n != 0) {
            new ConstraintProperties(((View)this.mView.getParent()).findViewById(n)).connect(2, this.mView.getId(), 1, 0);
        }
        if (n2 != 0) {
            new ConstraintProperties(((View)this.mView.getParent()).findViewById(n2)).connect(1, this.mView.getId(), 2, 0);
        }
        return this;
    }
    
    public ConstraintProperties addToHorizontalChainRTL(final int n, final int n2) {
        int n3;
        if (n == 0) {
            n3 = 6;
        }
        else {
            n3 = 7;
        }
        this.connect(6, n, n3, 0);
        int n4;
        if (n2 == 0) {
            n4 = 7;
        }
        else {
            n4 = 6;
        }
        this.connect(7, n2, n4, 0);
        if (n != 0) {
            new ConstraintProperties(((View)this.mView.getParent()).findViewById(n)).connect(7, this.mView.getId(), 6, 0);
        }
        if (n2 != 0) {
            new ConstraintProperties(((View)this.mView.getParent()).findViewById(n2)).connect(6, this.mView.getId(), 7, 0);
        }
        return this;
    }
    
    public ConstraintProperties addToVerticalChain(final int n, final int n2) {
        int n3;
        if (n == 0) {
            n3 = 3;
        }
        else {
            n3 = 4;
        }
        this.connect(3, n, n3, 0);
        int n4;
        if (n2 == 0) {
            n4 = 4;
        }
        else {
            n4 = 3;
        }
        this.connect(4, n2, n4, 0);
        if (n != 0) {
            new ConstraintProperties(((View)this.mView.getParent()).findViewById(n)).connect(4, this.mView.getId(), 3, 0);
        }
        if (n2 != 0) {
            new ConstraintProperties(((View)this.mView.getParent()).findViewById(n2)).connect(3, this.mView.getId(), 4, 0);
        }
        return this;
    }
    
    public ConstraintProperties alpha(final float alpha) {
        this.mView.setAlpha(alpha);
        return this;
    }
    
    public void apply() {
    }
    
    public ConstraintProperties center(final int n, final int n2, final int n3, final int n4, final int n5, final int n6, final float horizontalBias) {
        if (n3 < 0) {
            throw new IllegalArgumentException("margin must be > 0");
        }
        if (n6 < 0) {
            throw new IllegalArgumentException("margin must be > 0");
        }
        if (horizontalBias > 0.0f && horizontalBias <= 1.0f) {
            if (n2 != 1 && n2 != 2) {
                if (n2 != 6 && n2 != 7) {
                    this.connect(3, n, n2, n3);
                    this.connect(4, n4, n5, n6);
                    this.mParams.verticalBias = horizontalBias;
                }
                else {
                    this.connect(6, n, n2, n3);
                    this.connect(7, n4, n5, n6);
                    this.mParams.horizontalBias = horizontalBias;
                }
            }
            else {
                this.connect(1, n, n2, n3);
                this.connect(2, n4, n5, n6);
                this.mParams.horizontalBias = horizontalBias;
            }
            return this;
        }
        throw new IllegalArgumentException("bias must be between 0 and 1 inclusive");
    }
    
    public ConstraintProperties centerHorizontally(final int n) {
        if (n == 0) {
            this.center(0, 1, 0, 0, 2, 0, 0.5f);
        }
        else {
            this.center(n, 2, 0, n, 1, 0, 0.5f);
        }
        return this;
    }
    
    public ConstraintProperties centerHorizontally(final int n, final int n2, final int n3, final int n4, final int n5, final int n6, final float horizontalBias) {
        this.connect(1, n, n2, n3);
        this.connect(2, n4, n5, n6);
        this.mParams.horizontalBias = horizontalBias;
        return this;
    }
    
    public ConstraintProperties centerHorizontallyRtl(final int n) {
        if (n == 0) {
            this.center(0, 6, 0, 0, 7, 0, 0.5f);
        }
        else {
            this.center(n, 7, 0, n, 6, 0, 0.5f);
        }
        return this;
    }
    
    public ConstraintProperties centerHorizontallyRtl(final int n, final int n2, final int n3, final int n4, final int n5, final int n6, final float horizontalBias) {
        this.connect(6, n, n2, n3);
        this.connect(7, n4, n5, n6);
        this.mParams.horizontalBias = horizontalBias;
        return this;
    }
    
    public ConstraintProperties centerVertically(final int n) {
        if (n == 0) {
            this.center(0, 3, 0, 0, 4, 0, 0.5f);
        }
        else {
            this.center(n, 4, 0, n, 3, 0, 0.5f);
        }
        return this;
    }
    
    public ConstraintProperties centerVertically(final int n, final int n2, final int n3, final int n4, final int n5, final int n6, final float verticalBias) {
        this.connect(3, n, n2, n3);
        this.connect(4, n4, n5, n6);
        this.mParams.verticalBias = verticalBias;
        return this;
    }
    
    public ConstraintProperties connect(final int n, final int leftToRight, final int n2, final int leftMargin) {
        switch (n) {
            default: {
                final StringBuilder sb = new StringBuilder();
                sb.append(this.sideToString(n));
                sb.append(" to ");
                sb.append(this.sideToString(n2));
                sb.append(" unknown");
                throw new IllegalArgumentException(sb.toString());
            }
            case 7: {
                if (n2 == 7) {
                    final ConstraintLayout.LayoutParams mParams = this.mParams;
                    mParams.endToEnd = leftToRight;
                    mParams.endToStart = -1;
                }
                else {
                    if (n2 != 6) {
                        final StringBuilder sb2 = new StringBuilder();
                        sb2.append("right to ");
                        sb2.append(this.sideToString(n2));
                        sb2.append(" undefined");
                        throw new IllegalArgumentException(sb2.toString());
                    }
                    final ConstraintLayout.LayoutParams mParams2 = this.mParams;
                    mParams2.endToStart = leftToRight;
                    mParams2.endToEnd = -1;
                }
                this.mParams.setMarginEnd(leftMargin);
                break;
            }
            case 6: {
                if (n2 == 6) {
                    final ConstraintLayout.LayoutParams mParams3 = this.mParams;
                    mParams3.startToStart = leftToRight;
                    mParams3.startToEnd = -1;
                }
                else {
                    if (n2 != 7) {
                        final StringBuilder sb3 = new StringBuilder();
                        sb3.append("right to ");
                        sb3.append(this.sideToString(n2));
                        sb3.append(" undefined");
                        throw new IllegalArgumentException(sb3.toString());
                    }
                    final ConstraintLayout.LayoutParams mParams4 = this.mParams;
                    mParams4.startToEnd = leftToRight;
                    mParams4.startToStart = -1;
                }
                this.mParams.setMarginStart(leftMargin);
                break;
            }
            case 5: {
                if (n2 == 5) {
                    final ConstraintLayout.LayoutParams mParams5 = this.mParams;
                    mParams5.baselineToBaseline = leftToRight;
                    mParams5.bottomToBottom = -1;
                    mParams5.bottomToTop = -1;
                    mParams5.topToTop = -1;
                    mParams5.topToBottom = -1;
                }
                if (n2 == 3) {
                    final ConstraintLayout.LayoutParams mParams6 = this.mParams;
                    mParams6.baselineToTop = leftToRight;
                    mParams6.bottomToBottom = -1;
                    mParams6.bottomToTop = -1;
                    mParams6.topToTop = -1;
                    mParams6.topToBottom = -1;
                }
                else {
                    if (n2 != 4) {
                        final StringBuilder sb4 = new StringBuilder();
                        sb4.append("right to ");
                        sb4.append(this.sideToString(n2));
                        sb4.append(" undefined");
                        throw new IllegalArgumentException(sb4.toString());
                    }
                    final ConstraintLayout.LayoutParams mParams7 = this.mParams;
                    mParams7.baselineToBottom = leftToRight;
                    mParams7.bottomToBottom = -1;
                    mParams7.bottomToTop = -1;
                    mParams7.topToTop = -1;
                    mParams7.topToBottom = -1;
                }
                this.mParams.baselineMargin = leftMargin;
                break;
            }
            case 4: {
                if (n2 == 4) {
                    final ConstraintLayout.LayoutParams mParams8 = this.mParams;
                    mParams8.bottomToBottom = leftToRight;
                    mParams8.bottomToTop = -1;
                    mParams8.baselineToBaseline = -1;
                    mParams8.baselineToTop = -1;
                    mParams8.baselineToBottom = -1;
                }
                else {
                    if (n2 != 3) {
                        final StringBuilder sb5 = new StringBuilder();
                        sb5.append("right to ");
                        sb5.append(this.sideToString(n2));
                        sb5.append(" undefined");
                        throw new IllegalArgumentException(sb5.toString());
                    }
                    final ConstraintLayout.LayoutParams mParams9 = this.mParams;
                    mParams9.bottomToTop = leftToRight;
                    mParams9.bottomToBottom = -1;
                    mParams9.baselineToBaseline = -1;
                    mParams9.baselineToTop = -1;
                    mParams9.baselineToBottom = -1;
                }
                this.mParams.bottomMargin = leftMargin;
                break;
            }
            case 3: {
                if (n2 == 3) {
                    final ConstraintLayout.LayoutParams mParams10 = this.mParams;
                    mParams10.topToTop = leftToRight;
                    mParams10.topToBottom = -1;
                    mParams10.baselineToBaseline = -1;
                    mParams10.baselineToTop = -1;
                    mParams10.baselineToBottom = -1;
                }
                else {
                    if (n2 != 4) {
                        final StringBuilder sb6 = new StringBuilder();
                        sb6.append("right to ");
                        sb6.append(this.sideToString(n2));
                        sb6.append(" undefined");
                        throw new IllegalArgumentException(sb6.toString());
                    }
                    final ConstraintLayout.LayoutParams mParams11 = this.mParams;
                    mParams11.topToBottom = leftToRight;
                    mParams11.topToTop = -1;
                    mParams11.baselineToBaseline = -1;
                    mParams11.baselineToTop = -1;
                    mParams11.baselineToBottom = -1;
                }
                this.mParams.topMargin = leftMargin;
                break;
            }
            case 2: {
                if (n2 == 1) {
                    final ConstraintLayout.LayoutParams mParams12 = this.mParams;
                    mParams12.rightToLeft = leftToRight;
                    mParams12.rightToRight = -1;
                }
                else {
                    if (n2 != 2) {
                        final StringBuilder sb7 = new StringBuilder();
                        sb7.append("right to ");
                        sb7.append(this.sideToString(n2));
                        sb7.append(" undefined");
                        throw new IllegalArgumentException(sb7.toString());
                    }
                    final ConstraintLayout.LayoutParams mParams13 = this.mParams;
                    mParams13.rightToRight = leftToRight;
                    mParams13.rightToLeft = -1;
                }
                this.mParams.rightMargin = leftMargin;
                break;
            }
            case 1: {
                if (n2 == 1) {
                    final ConstraintLayout.LayoutParams mParams14 = this.mParams;
                    mParams14.leftToLeft = leftToRight;
                    mParams14.leftToRight = -1;
                }
                else {
                    if (n2 != 2) {
                        final StringBuilder sb8 = new StringBuilder();
                        sb8.append("Left to ");
                        sb8.append(this.sideToString(n2));
                        sb8.append(" undefined");
                        throw new IllegalArgumentException(sb8.toString());
                    }
                    final ConstraintLayout.LayoutParams mParams15 = this.mParams;
                    mParams15.leftToRight = leftToRight;
                    mParams15.leftToLeft = -1;
                }
                this.mParams.leftMargin = leftMargin;
                break;
            }
        }
        return this;
    }
    
    public ConstraintProperties constrainDefaultHeight(final int matchConstraintDefaultHeight) {
        this.mParams.matchConstraintDefaultHeight = matchConstraintDefaultHeight;
        return this;
    }
    
    public ConstraintProperties constrainDefaultWidth(final int matchConstraintDefaultWidth) {
        this.mParams.matchConstraintDefaultWidth = matchConstraintDefaultWidth;
        return this;
    }
    
    public ConstraintProperties constrainHeight(final int height) {
        this.mParams.height = height;
        return this;
    }
    
    public ConstraintProperties constrainMaxHeight(final int matchConstraintMaxHeight) {
        this.mParams.matchConstraintMaxHeight = matchConstraintMaxHeight;
        return this;
    }
    
    public ConstraintProperties constrainMaxWidth(final int matchConstraintMaxWidth) {
        this.mParams.matchConstraintMaxWidth = matchConstraintMaxWidth;
        return this;
    }
    
    public ConstraintProperties constrainMinHeight(final int matchConstraintMinHeight) {
        this.mParams.matchConstraintMinHeight = matchConstraintMinHeight;
        return this;
    }
    
    public ConstraintProperties constrainMinWidth(final int matchConstraintMinWidth) {
        this.mParams.matchConstraintMinWidth = matchConstraintMinWidth;
        return this;
    }
    
    public ConstraintProperties constrainWidth(final int width) {
        this.mParams.width = width;
        return this;
    }
    
    public ConstraintProperties dimensionRatio(final String dimensionRatio) {
        this.mParams.dimensionRatio = dimensionRatio;
        return this;
    }
    
    public ConstraintProperties elevation(final float elevation) {
        this.mView.setElevation(elevation);
        return this;
    }
    
    public ConstraintProperties goneMargin(final int n, final int n2) {
        switch (n) {
            default: {
                throw new IllegalArgumentException("unknown constraint");
            }
            case 7: {
                this.mParams.goneEndMargin = n2;
                break;
            }
            case 6: {
                this.mParams.goneStartMargin = n2;
                break;
            }
            case 5: {
                throw new IllegalArgumentException("baseline does not support margins");
            }
            case 4: {
                this.mParams.goneBottomMargin = n2;
                break;
            }
            case 3: {
                this.mParams.goneTopMargin = n2;
                break;
            }
            case 2: {
                this.mParams.goneRightMargin = n2;
                break;
            }
            case 1: {
                this.mParams.goneLeftMargin = n2;
                break;
            }
        }
        return this;
    }
    
    public ConstraintProperties horizontalBias(final float horizontalBias) {
        this.mParams.horizontalBias = horizontalBias;
        return this;
    }
    
    public ConstraintProperties horizontalChainStyle(final int horizontalChainStyle) {
        this.mParams.horizontalChainStyle = horizontalChainStyle;
        return this;
    }
    
    public ConstraintProperties horizontalWeight(final float horizontalWeight) {
        this.mParams.horizontalWeight = horizontalWeight;
        return this;
    }
    
    public ConstraintProperties margin(final int n, final int n2) {
        switch (n) {
            default: {
                throw new IllegalArgumentException("unknown constraint");
            }
            case 7: {
                this.mParams.setMarginEnd(n2);
                break;
            }
            case 6: {
                this.mParams.setMarginStart(n2);
                break;
            }
            case 5: {
                throw new IllegalArgumentException("baseline does not support margins");
            }
            case 4: {
                this.mParams.bottomMargin = n2;
                break;
            }
            case 3: {
                this.mParams.topMargin = n2;
                break;
            }
            case 2: {
                this.mParams.rightMargin = n2;
                break;
            }
            case 1: {
                this.mParams.leftMargin = n2;
                break;
            }
        }
        return this;
    }
    
    public ConstraintProperties removeConstraints(final int n) {
        switch (n) {
            default: {
                throw new IllegalArgumentException("unknown constraint");
            }
            case 7: {
                final ConstraintLayout.LayoutParams mParams = this.mParams;
                mParams.endToStart = -1;
                mParams.setMarginEnd(mParams.endToEnd = -1);
                this.mParams.goneEndMargin = Integer.MIN_VALUE;
                break;
            }
            case 6: {
                final ConstraintLayout.LayoutParams mParams2 = this.mParams;
                mParams2.startToEnd = -1;
                mParams2.setMarginStart(mParams2.startToStart = -1);
                this.mParams.goneStartMargin = Integer.MIN_VALUE;
                break;
            }
            case 5: {
                this.mParams.baselineToBaseline = -1;
                break;
            }
            case 4: {
                final ConstraintLayout.LayoutParams mParams3 = this.mParams;
                mParams3.bottomToTop = -1;
                mParams3.bottomToBottom = -1;
                mParams3.bottomMargin = -1;
                mParams3.goneBottomMargin = Integer.MIN_VALUE;
                break;
            }
            case 3: {
                final ConstraintLayout.LayoutParams mParams4 = this.mParams;
                mParams4.topToBottom = -1;
                mParams4.topToTop = -1;
                mParams4.topMargin = -1;
                mParams4.goneTopMargin = Integer.MIN_VALUE;
                break;
            }
            case 2: {
                final ConstraintLayout.LayoutParams mParams5 = this.mParams;
                mParams5.rightToRight = -1;
                mParams5.rightToLeft = -1;
                mParams5.rightMargin = -1;
                mParams5.goneRightMargin = Integer.MIN_VALUE;
                break;
            }
            case 1: {
                final ConstraintLayout.LayoutParams mParams6 = this.mParams;
                mParams6.leftToRight = -1;
                mParams6.leftToLeft = -1;
                mParams6.leftMargin = -1;
                mParams6.goneLeftMargin = Integer.MIN_VALUE;
                break;
            }
        }
        return this;
    }
    
    public ConstraintProperties removeFromHorizontalChain() {
        final ConstraintLayout.LayoutParams mParams = this.mParams;
        final int leftToRight = mParams.leftToRight;
        final int rightToLeft = mParams.rightToLeft;
        if (leftToRight == -1 && rightToLeft == -1) {
            final int startToEnd = mParams.startToEnd;
            final int endToStart = mParams.endToStart;
            if (startToEnd != -1 || endToStart != -1) {
                final ConstraintProperties constraintProperties = new ConstraintProperties(((View)this.mView.getParent()).findViewById(startToEnd));
                final ConstraintProperties constraintProperties2 = new ConstraintProperties(((View)this.mView.getParent()).findViewById(endToStart));
                final ConstraintLayout.LayoutParams mParams2 = this.mParams;
                if (startToEnd != -1 && endToStart != -1) {
                    constraintProperties.connect(7, endToStart, 6, 0);
                    constraintProperties2.connect(6, leftToRight, 7, 0);
                }
                else if (leftToRight != -1 || endToStart != -1) {
                    final int rightToRight = mParams2.rightToRight;
                    if (rightToRight != -1) {
                        constraintProperties.connect(7, rightToRight, 7, 0);
                    }
                    else {
                        final int leftToLeft = mParams2.leftToLeft;
                        if (leftToLeft != -1) {
                            constraintProperties2.connect(6, leftToLeft, 6, 0);
                        }
                    }
                }
            }
            this.removeConstraints(6);
            this.removeConstraints(7);
        }
        else {
            final ConstraintProperties constraintProperties3 = new ConstraintProperties(((View)this.mView.getParent()).findViewById(leftToRight));
            final ConstraintProperties constraintProperties4 = new ConstraintProperties(((View)this.mView.getParent()).findViewById(rightToLeft));
            final ConstraintLayout.LayoutParams mParams3 = this.mParams;
            if (leftToRight != -1 && rightToLeft != -1) {
                constraintProperties3.connect(2, rightToLeft, 1, 0);
                constraintProperties4.connect(1, leftToRight, 2, 0);
            }
            else if (leftToRight != -1 || rightToLeft != -1) {
                final int rightToRight2 = mParams3.rightToRight;
                if (rightToRight2 != -1) {
                    constraintProperties3.connect(2, rightToRight2, 2, 0);
                }
                else {
                    final int leftToLeft2 = mParams3.leftToLeft;
                    if (leftToLeft2 != -1) {
                        constraintProperties4.connect(1, leftToLeft2, 1, 0);
                    }
                }
            }
            this.removeConstraints(1);
            this.removeConstraints(2);
        }
        return this;
    }
    
    public ConstraintProperties removeFromVerticalChain() {
        final ConstraintLayout.LayoutParams mParams = this.mParams;
        final int topToBottom = mParams.topToBottom;
        final int bottomToTop = mParams.bottomToTop;
        if (topToBottom != -1 || bottomToTop != -1) {
            final ConstraintProperties constraintProperties = new ConstraintProperties(((View)this.mView.getParent()).findViewById(topToBottom));
            final ConstraintProperties constraintProperties2 = new ConstraintProperties(((View)this.mView.getParent()).findViewById(bottomToTop));
            final ConstraintLayout.LayoutParams mParams2 = this.mParams;
            if (topToBottom != -1 && bottomToTop != -1) {
                constraintProperties.connect(4, bottomToTop, 3, 0);
                constraintProperties2.connect(3, topToBottom, 4, 0);
            }
            else if (topToBottom != -1 || bottomToTop != -1) {
                final int bottomToBottom = mParams2.bottomToBottom;
                if (bottomToBottom != -1) {
                    constraintProperties.connect(4, bottomToBottom, 4, 0);
                }
                else {
                    final int topToTop = mParams2.topToTop;
                    if (topToTop != -1) {
                        constraintProperties2.connect(3, topToTop, 3, 0);
                    }
                }
            }
        }
        this.removeConstraints(3);
        this.removeConstraints(4);
        return this;
    }
    
    public ConstraintProperties rotation(final float rotation) {
        this.mView.setRotation(rotation);
        return this;
    }
    
    public ConstraintProperties rotationX(final float rotationX) {
        this.mView.setRotationX(rotationX);
        return this;
    }
    
    public ConstraintProperties rotationY(final float rotationY) {
        this.mView.setRotationY(rotationY);
        return this;
    }
    
    public ConstraintProperties scaleX(final float scaleY) {
        this.mView.setScaleY(scaleY);
        return this;
    }
    
    public ConstraintProperties scaleY(final float n) {
        return this;
    }
    
    public ConstraintProperties transformPivot(final float pivotX, final float pivotY) {
        this.mView.setPivotX(pivotX);
        this.mView.setPivotY(pivotY);
        return this;
    }
    
    public ConstraintProperties transformPivotX(final float pivotX) {
        this.mView.setPivotX(pivotX);
        return this;
    }
    
    public ConstraintProperties transformPivotY(final float pivotY) {
        this.mView.setPivotY(pivotY);
        return this;
    }
    
    public ConstraintProperties translation(final float translationX, final float translationY) {
        this.mView.setTranslationX(translationX);
        this.mView.setTranslationY(translationY);
        return this;
    }
    
    public ConstraintProperties translationX(final float translationX) {
        this.mView.setTranslationX(translationX);
        return this;
    }
    
    public ConstraintProperties translationY(final float translationY) {
        this.mView.setTranslationY(translationY);
        return this;
    }
    
    public ConstraintProperties translationZ(final float translationZ) {
        this.mView.setTranslationZ(translationZ);
        return this;
    }
    
    public ConstraintProperties verticalBias(final float verticalBias) {
        this.mParams.verticalBias = verticalBias;
        return this;
    }
    
    public ConstraintProperties verticalChainStyle(final int verticalChainStyle) {
        this.mParams.verticalChainStyle = verticalChainStyle;
        return this;
    }
    
    public ConstraintProperties verticalWeight(final float verticalWeight) {
        this.mParams.verticalWeight = verticalWeight;
        return this;
    }
    
    public ConstraintProperties visibility(final int visibility) {
        this.mView.setVisibility(visibility);
        return this;
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.constraintlayout.widget;

import java.util.Iterator;
import java.util.Collection;
import java.util.ArrayList;
import java.lang.ref.WeakReference;
import java.util.HashSet;
import java.util.HashMap;
import android.util.SparseIntArray;

public class SharedValues
{
    public static final int UNSET = -1;
    private SparseIntArray mValues;
    private HashMap<Integer, HashSet<WeakReference<SharedValuesListener>>> mValuesListeners;
    
    public SharedValues() {
        this.mValues = new SparseIntArray();
        this.mValuesListeners = new HashMap<Integer, HashSet<WeakReference<SharedValuesListener>>>();
    }
    
    public void addListener(final int n, final SharedValuesListener referent) {
        HashSet value;
        if ((value = this.mValuesListeners.get(n)) == null) {
            value = new HashSet();
            this.mValuesListeners.put(n, value);
        }
        value.add(new WeakReference(referent));
    }
    
    public void clearListeners() {
        this.mValuesListeners.clear();
    }
    
    public void fireNewValue(final int i, final int n) {
        final int value = this.mValues.get(i, -1);
        if (value == n) {
            return;
        }
        this.mValues.put(i, n);
        final HashSet set = this.mValuesListeners.get(i);
        if (set == null) {
            return;
        }
        final Iterator iterator = set.iterator();
        boolean b = false;
        while (iterator.hasNext()) {
            final SharedValuesListener sharedValuesListener = ((WeakReference<SharedValuesListener>)iterator.next()).get();
            if (sharedValuesListener != null) {
                sharedValuesListener.onNewValue(i, n, value);
            }
            else {
                b = true;
            }
        }
        if (b) {
            final ArrayList c = new ArrayList();
            for (final WeakReference weakReference : set) {
                if (weakReference.get() == null) {
                    c.add(weakReference);
                }
            }
            set.removeAll(c);
        }
    }
    
    public int getValue(final int n) {
        return this.mValues.get(n, -1);
    }
    
    public void removeListener(final int i, final SharedValuesListener sharedValuesListener) {
        final HashSet set = this.mValuesListeners.get(i);
        if (set == null) {
            return;
        }
        final ArrayList c = new ArrayList();
        for (final WeakReference weakReference : set) {
            final SharedValuesListener sharedValuesListener2 = (SharedValuesListener)weakReference.get();
            if (sharedValuesListener2 == null || sharedValuesListener2 == sharedValuesListener) {
                c.add(weakReference);
            }
        }
        set.removeAll(c);
    }
    
    public void removeListener(final SharedValuesListener sharedValuesListener) {
        final Iterator<Integer> iterator = this.mValuesListeners.keySet().iterator();
        while (iterator.hasNext()) {
            this.removeListener(iterator.next(), sharedValuesListener);
        }
    }
    
    public interface SharedValuesListener
    {
        void onNewValue(final int p0, final int p1, final int p2);
    }
}

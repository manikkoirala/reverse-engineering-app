// 
// Decompiled by Procyon v0.6.0
// 

package androidx.constraintlayout.widget;

import android.util.AttributeSet;
import android.content.Context;

public class Group extends ConstraintHelper
{
    public Group(final Context context) {
        super(context);
    }
    
    public Group(final Context context, final AttributeSet set) {
        super(context, set);
    }
    
    public Group(final Context context, final AttributeSet set, final int n) {
        super(context, set, n);
    }
    
    @Override
    protected void applyLayoutFeaturesInConstraintSet(final ConstraintLayout constraintLayout) {
        this.applyLayoutFeatures(constraintLayout);
    }
    
    @Override
    protected void init(final AttributeSet set) {
        super.init(set);
        super.mUseViewMeasure = false;
    }
    
    public void onAttachedToWindow() {
        super.onAttachedToWindow();
        this.applyLayoutFeatures();
    }
    
    public void setElevation(final float elevation) {
        super.setElevation(elevation);
        this.applyLayoutFeatures();
    }
    
    public void setVisibility(final int visibility) {
        super.setVisibility(visibility);
        this.applyLayoutFeatures();
    }
    
    @Override
    public void updatePostLayout(final ConstraintLayout constraintLayout) {
        final ConstraintLayout.LayoutParams layoutParams = (ConstraintLayout.LayoutParams)this.getLayoutParams();
        layoutParams.widget.setWidth(0);
        layoutParams.widget.setHeight(0);
    }
}

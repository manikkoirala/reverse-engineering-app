// 
// Decompiled by Procyon v0.6.0
// 

package androidx.constraintlayout.widget;

import android.content.res.TypedArray;
import android.util.Xml;
import java.util.ArrayList;
import android.content.res.XmlResourceParser;
import org.xmlpull.v1.XmlPullParserException;
import java.io.IOException;
import org.xmlpull.v1.XmlPullParser;
import android.content.Context;
import android.util.SparseArray;

public class ConstraintLayoutStates
{
    private static final boolean DEBUG = false;
    public static final String TAG = "ConstraintLayoutStates";
    private final ConstraintLayout mConstraintLayout;
    private SparseArray<ConstraintSet> mConstraintSetMap;
    private ConstraintsChangedListener mConstraintsChangedListener;
    int mCurrentConstraintNumber;
    int mCurrentStateId;
    ConstraintSet mDefaultConstraintSet;
    private SparseArray<State> mStateList;
    
    ConstraintLayoutStates(final Context context, final ConstraintLayout mConstraintLayout, final int n) {
        this.mCurrentStateId = -1;
        this.mCurrentConstraintNumber = -1;
        this.mStateList = (SparseArray<State>)new SparseArray();
        this.mConstraintSetMap = (SparseArray<ConstraintSet>)new SparseArray();
        this.mConstraintsChangedListener = null;
        this.mConstraintLayout = mConstraintLayout;
        this.load(context, n);
    }
    
    private void load(final Context context, int n) {
        final XmlResourceParser xml = context.getResources().getXml(n);
        try {
            n = ((XmlPullParser)xml).getEventType();
            State state = null;
            while (true) {
                final int n2 = 1;
                if (n == 1) {
                    break;
                }
                State state2;
                if (n != 0) {
                    if (n != 2) {
                        state2 = state;
                    }
                    else {
                        final String name = ((XmlPullParser)xml).getName();
                        Label_0188: {
                            switch (name.hashCode()) {
                                case 1901439077: {
                                    if (name.equals("Variant")) {
                                        n = 3;
                                        break Label_0188;
                                    }
                                    break;
                                }
                                case 1657696882: {
                                    if (name.equals("layoutDescription")) {
                                        n = 0;
                                        break Label_0188;
                                    }
                                    break;
                                }
                                case 1382829617: {
                                    if (name.equals("StateSet")) {
                                        n = n2;
                                        break Label_0188;
                                    }
                                    break;
                                }
                                case 80204913: {
                                    if (name.equals("State")) {
                                        n = 2;
                                        break Label_0188;
                                    }
                                    break;
                                }
                                case -1349929691: {
                                    if (name.equals("ConstraintSet")) {
                                        n = 4;
                                        break Label_0188;
                                    }
                                    break;
                                }
                            }
                            n = -1;
                        }
                        if (n != 2) {
                            if (n != 3) {
                                if (n != 4) {
                                    state2 = state;
                                }
                                else {
                                    this.parseConstraintSet(context, (XmlPullParser)xml);
                                    state2 = state;
                                }
                            }
                            else {
                                final Variant variant = new Variant(context, (XmlPullParser)xml);
                                if ((state2 = state) != null) {
                                    state.add(variant);
                                    state2 = state;
                                }
                            }
                        }
                        else {
                            state2 = new State(context, (XmlPullParser)xml);
                            this.mStateList.put(state2.mId, (Object)state2);
                        }
                    }
                }
                else {
                    ((XmlPullParser)xml).getName();
                    state2 = state;
                }
                n = ((XmlPullParser)xml).next();
                state = state2;
            }
        }
        catch (final IOException ex) {
            ex.printStackTrace();
        }
        catch (final XmlPullParserException ex2) {
            ex2.printStackTrace();
        }
    }
    
    private void parseConstraintSet(final Context context, final XmlPullParser xmlPullParser) {
        final ConstraintSet set = new ConstraintSet();
        for (int attributeCount = xmlPullParser.getAttributeCount(), i = 0; i < attributeCount; ++i) {
            final String attributeName = xmlPullParser.getAttributeName(i);
            final String attributeValue = xmlPullParser.getAttributeValue(i);
            if (attributeName != null) {
                if (attributeValue != null) {
                    if ("id".equals(attributeName)) {
                        int identifier;
                        if (attributeValue.contains("/")) {
                            identifier = context.getResources().getIdentifier(attributeValue.substring(attributeValue.indexOf(47) + 1), "id", context.getPackageName());
                        }
                        else {
                            identifier = -1;
                        }
                        int int1 = identifier;
                        if (identifier == -1) {
                            int1 = identifier;
                            if (attributeValue.length() > 1) {
                                int1 = Integer.parseInt(attributeValue.substring(1));
                            }
                        }
                        set.load(context, xmlPullParser);
                        this.mConstraintSetMap.put(int1, (Object)set);
                        break;
                    }
                }
            }
        }
    }
    
    public boolean needsToChange(int mCurrentConstraintNumber, final float n, final float n2) {
        final int mCurrentStateId = this.mCurrentStateId;
        if (mCurrentStateId != mCurrentConstraintNumber) {
            return true;
        }
        Object o;
        if (mCurrentConstraintNumber == -1) {
            o = this.mStateList.valueAt(0);
        }
        else {
            o = this.mStateList.get(mCurrentStateId);
        }
        final State state = (State)o;
        mCurrentConstraintNumber = this.mCurrentConstraintNumber;
        return (mCurrentConstraintNumber == -1 || !((Variant)state.mVariants.get(mCurrentConstraintNumber)).match(n, n2)) && this.mCurrentConstraintNumber != state.findMatch(n, n2);
    }
    
    public void setOnConstraintsChanged(final ConstraintsChangedListener mConstraintsChangedListener) {
        this.mConstraintsChangedListener = mConstraintsChangedListener;
    }
    
    public void updateConstraints(int i, final float f, final float f2) {
        final int mCurrentStateId = this.mCurrentStateId;
        if (mCurrentStateId == i) {
            State state;
            if (i == -1) {
                state = (State)this.mStateList.valueAt(0);
            }
            else {
                state = (State)this.mStateList.get(mCurrentStateId);
            }
            i = this.mCurrentConstraintNumber;
            if (i != -1 && ((Variant)state.mVariants.get(i)).match(f, f2)) {
                return;
            }
            final int match = state.findMatch(f, f2);
            if (this.mCurrentConstraintNumber == match) {
                return;
            }
            ConstraintSet set;
            if (match == -1) {
                set = this.mDefaultConstraintSet;
            }
            else {
                set = ((Variant)state.mVariants.get(match)).mConstraintSet;
            }
            if (match == -1) {
                i = state.mConstraintID;
            }
            else {
                i = ((Variant)state.mVariants.get(match)).mConstraintID;
            }
            if (set == null) {
                return;
            }
            this.mCurrentConstraintNumber = match;
            final ConstraintsChangedListener mConstraintsChangedListener = this.mConstraintsChangedListener;
            if (mConstraintsChangedListener != null) {
                mConstraintsChangedListener.preLayoutChange(-1, i);
            }
            set.applyTo(this.mConstraintLayout);
            final ConstraintsChangedListener mConstraintsChangedListener2 = this.mConstraintsChangedListener;
            if (mConstraintsChangedListener2 != null) {
                mConstraintsChangedListener2.postLayoutChange(-1, i);
            }
        }
        else {
            this.mCurrentStateId = i;
            final State state2 = (State)this.mStateList.get(i);
            final int match2 = state2.findMatch(f, f2);
            ConstraintSet set2;
            if (match2 == -1) {
                set2 = state2.mConstraintSet;
            }
            else {
                set2 = ((Variant)state2.mVariants.get(match2)).mConstraintSet;
            }
            int n;
            if (match2 == -1) {
                n = state2.mConstraintID;
            }
            else {
                n = ((Variant)state2.mVariants.get(match2)).mConstraintID;
            }
            if (set2 == null) {
                final StringBuilder sb = new StringBuilder();
                sb.append("NO Constraint set found ! id=");
                sb.append(i);
                sb.append(", dim =");
                sb.append(f);
                sb.append(", ");
                sb.append(f2);
                return;
            }
            this.mCurrentConstraintNumber = match2;
            final ConstraintsChangedListener mConstraintsChangedListener3 = this.mConstraintsChangedListener;
            if (mConstraintsChangedListener3 != null) {
                mConstraintsChangedListener3.preLayoutChange(i, n);
            }
            set2.applyTo(this.mConstraintLayout);
            final ConstraintsChangedListener mConstraintsChangedListener4 = this.mConstraintsChangedListener;
            if (mConstraintsChangedListener4 != null) {
                mConstraintsChangedListener4.postLayoutChange(i, n);
            }
        }
    }
    
    static class State
    {
        int mConstraintID;
        ConstraintSet mConstraintSet;
        int mId;
        ArrayList<Variant> mVariants;
        
        public State(final Context context, final XmlPullParser xmlPullParser) {
            this.mVariants = new ArrayList<Variant>();
            this.mConstraintID = -1;
            final TypedArray obtainStyledAttributes = context.obtainStyledAttributes(Xml.asAttributeSet(xmlPullParser), R.styleable.State);
            for (int indexCount = obtainStyledAttributes.getIndexCount(), i = 0; i < indexCount; ++i) {
                final int index = obtainStyledAttributes.getIndex(i);
                if (index == R.styleable.State_android_id) {
                    this.mId = obtainStyledAttributes.getResourceId(index, this.mId);
                }
                else if (index == R.styleable.State_constraints) {
                    this.mConstraintID = obtainStyledAttributes.getResourceId(index, this.mConstraintID);
                    final String resourceTypeName = context.getResources().getResourceTypeName(this.mConstraintID);
                    context.getResources().getResourceName(this.mConstraintID);
                    if ("layout".equals(resourceTypeName)) {
                        (this.mConstraintSet = new ConstraintSet()).clone(context, this.mConstraintID);
                    }
                }
            }
            obtainStyledAttributes.recycle();
        }
        
        void add(final Variant e) {
            this.mVariants.add(e);
        }
        
        public int findMatch(final float n, final float n2) {
            for (int i = 0; i < this.mVariants.size(); ++i) {
                if (this.mVariants.get(i).match(n, n2)) {
                    return i;
                }
            }
            return -1;
        }
    }
    
    static class Variant
    {
        int mConstraintID;
        ConstraintSet mConstraintSet;
        int mId;
        float mMaxHeight;
        float mMaxWidth;
        float mMinHeight;
        float mMinWidth;
        
        public Variant(final Context context, final XmlPullParser xmlPullParser) {
            this.mMinWidth = Float.NaN;
            this.mMinHeight = Float.NaN;
            this.mMaxWidth = Float.NaN;
            this.mMaxHeight = Float.NaN;
            this.mConstraintID = -1;
            final TypedArray obtainStyledAttributes = context.obtainStyledAttributes(Xml.asAttributeSet(xmlPullParser), R.styleable.Variant);
            for (int indexCount = obtainStyledAttributes.getIndexCount(), i = 0; i < indexCount; ++i) {
                final int index = obtainStyledAttributes.getIndex(i);
                if (index == R.styleable.Variant_constraints) {
                    this.mConstraintID = obtainStyledAttributes.getResourceId(index, this.mConstraintID);
                    final String resourceTypeName = context.getResources().getResourceTypeName(this.mConstraintID);
                    context.getResources().getResourceName(this.mConstraintID);
                    if ("layout".equals(resourceTypeName)) {
                        (this.mConstraintSet = new ConstraintSet()).clone(context, this.mConstraintID);
                    }
                }
                else if (index == R.styleable.Variant_region_heightLessThan) {
                    this.mMaxHeight = obtainStyledAttributes.getDimension(index, this.mMaxHeight);
                }
                else if (index == R.styleable.Variant_region_heightMoreThan) {
                    this.mMinHeight = obtainStyledAttributes.getDimension(index, this.mMinHeight);
                }
                else if (index == R.styleable.Variant_region_widthLessThan) {
                    this.mMaxWidth = obtainStyledAttributes.getDimension(index, this.mMaxWidth);
                }
                else if (index == R.styleable.Variant_region_widthMoreThan) {
                    this.mMinWidth = obtainStyledAttributes.getDimension(index, this.mMinWidth);
                }
            }
            obtainStyledAttributes.recycle();
        }
        
        boolean match(final float n, final float n2) {
            return (Float.isNaN(this.mMinWidth) || n >= this.mMinWidth) && (Float.isNaN(this.mMinHeight) || n2 >= this.mMinHeight) && (Float.isNaN(this.mMaxWidth) || n <= this.mMaxWidth) && (Float.isNaN(this.mMaxHeight) || n2 <= this.mMaxHeight);
        }
    }
}

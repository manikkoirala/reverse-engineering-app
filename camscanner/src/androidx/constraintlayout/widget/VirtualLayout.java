// 
// Decompiled by Procyon v0.6.0
// 

package androidx.constraintlayout.widget;

import android.view.View;
import android.view.ViewParent;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.content.Context;

public abstract class VirtualLayout extends ConstraintHelper
{
    private boolean mApplyElevationOnAttach;
    private boolean mApplyVisibilityOnAttach;
    
    public VirtualLayout(final Context context) {
        super(context);
    }
    
    public VirtualLayout(final Context context, final AttributeSet set) {
        super(context, set);
    }
    
    public VirtualLayout(final Context context, final AttributeSet set, final int n) {
        super(context, set, n);
    }
    
    @Override
    protected void applyLayoutFeaturesInConstraintSet(final ConstraintLayout constraintLayout) {
        this.applyLayoutFeatures(constraintLayout);
    }
    
    @Override
    protected void init(final AttributeSet set) {
        super.init(set);
        if (set != null) {
            final TypedArray obtainStyledAttributes = this.getContext().obtainStyledAttributes(set, R.styleable.ConstraintLayout_Layout);
            for (int indexCount = obtainStyledAttributes.getIndexCount(), i = 0; i < indexCount; ++i) {
                final int index = obtainStyledAttributes.getIndex(i);
                if (index == R.styleable.ConstraintLayout_Layout_android_visibility) {
                    this.mApplyVisibilityOnAttach = true;
                }
                else if (index == R.styleable.ConstraintLayout_Layout_android_elevation) {
                    this.mApplyElevationOnAttach = true;
                }
            }
            obtainStyledAttributes.recycle();
        }
    }
    
    public void onAttachedToWindow() {
        super.onAttachedToWindow();
        if (this.mApplyVisibilityOnAttach || this.mApplyElevationOnAttach) {
            final ViewParent parent = this.getParent();
            if (parent instanceof ConstraintLayout) {
                final ConstraintLayout constraintLayout = (ConstraintLayout)parent;
                final int visibility = this.getVisibility();
                final float elevation = this.getElevation();
                for (int i = 0; i < super.mCount; ++i) {
                    final View viewById = constraintLayout.getViewById(super.mIds[i]);
                    if (viewById != null) {
                        if (this.mApplyVisibilityOnAttach) {
                            viewById.setVisibility(visibility);
                        }
                        if (this.mApplyElevationOnAttach && elevation > 0.0f) {
                            viewById.setTranslationZ(viewById.getTranslationZ() + elevation);
                        }
                    }
                }
            }
        }
    }
    
    public void onMeasure(final androidx.constraintlayout.core.widgets.VirtualLayout virtualLayout, final int n, final int n2) {
    }
    
    public void setElevation(final float elevation) {
        super.setElevation(elevation);
        this.applyLayoutFeatures();
    }
    
    public void setVisibility(final int visibility) {
        super.setVisibility(visibility);
        this.applyLayoutFeatures();
    }
}

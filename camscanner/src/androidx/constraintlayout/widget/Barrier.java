// 
// Decompiled by Procyon v0.6.0
// 

package androidx.constraintlayout.widget;

import androidx.constraintlayout.core.widgets.ConstraintWidgetContainer;
import android.util.SparseArray;
import androidx.constraintlayout.core.widgets.HelperWidget;
import android.content.res.TypedArray;
import androidx.constraintlayout.core.widgets.ConstraintWidget;
import android.util.AttributeSet;
import android.content.Context;

public class Barrier extends ConstraintHelper
{
    public static final int BOTTOM = 3;
    public static final int END = 6;
    public static final int LEFT = 0;
    public static final int RIGHT = 1;
    public static final int START = 5;
    public static final int TOP = 2;
    private androidx.constraintlayout.core.widgets.Barrier mBarrier;
    private int mIndicatedType;
    private int mResolvedType;
    
    public Barrier(final Context context) {
        super(context);
        super.setVisibility(8);
    }
    
    public Barrier(final Context context, final AttributeSet set) {
        super(context, set);
        super.setVisibility(8);
    }
    
    public Barrier(final Context context, final AttributeSet set, final int n) {
        super(context, set, n);
        super.setVisibility(8);
    }
    
    private void updateType(final ConstraintWidget constraintWidget, int mResolvedType, final boolean b) {
        this.mResolvedType = mResolvedType;
        if (b) {
            mResolvedType = this.mIndicatedType;
            if (mResolvedType == 5) {
                this.mResolvedType = 1;
            }
            else if (mResolvedType == 6) {
                this.mResolvedType = 0;
            }
        }
        else {
            mResolvedType = this.mIndicatedType;
            if (mResolvedType == 5) {
                this.mResolvedType = 0;
            }
            else if (mResolvedType == 6) {
                this.mResolvedType = 1;
            }
        }
        if (constraintWidget instanceof androidx.constraintlayout.core.widgets.Barrier) {
            ((androidx.constraintlayout.core.widgets.Barrier)constraintWidget).setBarrierType(this.mResolvedType);
        }
    }
    
    @Deprecated
    public boolean allowsGoneWidget() {
        return this.mBarrier.getAllowsGoneWidget();
    }
    
    public boolean getAllowsGoneWidget() {
        return this.mBarrier.getAllowsGoneWidget();
    }
    
    public int getMargin() {
        return this.mBarrier.getMargin();
    }
    
    public int getType() {
        return this.mIndicatedType;
    }
    
    @Override
    protected void init(final AttributeSet set) {
        super.init(set);
        this.mBarrier = new androidx.constraintlayout.core.widgets.Barrier();
        if (set != null) {
            final TypedArray obtainStyledAttributes = this.getContext().obtainStyledAttributes(set, R.styleable.ConstraintLayout_Layout);
            for (int indexCount = obtainStyledAttributes.getIndexCount(), i = 0; i < indexCount; ++i) {
                final int index = obtainStyledAttributes.getIndex(i);
                if (index == R.styleable.ConstraintLayout_Layout_barrierDirection) {
                    this.setType(obtainStyledAttributes.getInt(index, 0));
                }
                else if (index == R.styleable.ConstraintLayout_Layout_barrierAllowsGoneWidgets) {
                    this.mBarrier.setAllowsGoneWidget(obtainStyledAttributes.getBoolean(index, true));
                }
                else if (index == R.styleable.ConstraintLayout_Layout_barrierMargin) {
                    this.mBarrier.setMargin(obtainStyledAttributes.getDimensionPixelSize(index, 0));
                }
            }
            obtainStyledAttributes.recycle();
        }
        super.mHelperWidget = this.mBarrier;
        this.validateParams();
    }
    
    @Override
    public void loadParameters(final ConstraintSet.Constraint constraint, final HelperWidget helperWidget, final ConstraintLayout.LayoutParams layoutParams, final SparseArray<ConstraintWidget> sparseArray) {
        super.loadParameters(constraint, helperWidget, layoutParams, sparseArray);
        if (helperWidget instanceof androidx.constraintlayout.core.widgets.Barrier) {
            final androidx.constraintlayout.core.widgets.Barrier barrier = (androidx.constraintlayout.core.widgets.Barrier)helperWidget;
            this.updateType(barrier, constraint.layout.mBarrierDirection, ((ConstraintWidgetContainer)helperWidget.getParent()).isRtl());
            barrier.setAllowsGoneWidget(constraint.layout.mBarrierAllowsGoneWidgets);
            barrier.setMargin(constraint.layout.mBarrierMargin);
        }
    }
    
    @Override
    public void resolveRtl(final ConstraintWidget constraintWidget, final boolean b) {
        this.updateType(constraintWidget, this.mIndicatedType, b);
    }
    
    public void setAllowsGoneWidget(final boolean allowsGoneWidget) {
        this.mBarrier.setAllowsGoneWidget(allowsGoneWidget);
    }
    
    public void setDpMargin(int margin) {
        margin = (int)(margin * this.getResources().getDisplayMetrics().density + 0.5f);
        this.mBarrier.setMargin(margin);
    }
    
    public void setMargin(final int margin) {
        this.mBarrier.setMargin(margin);
    }
    
    public void setType(final int mIndicatedType) {
        this.mIndicatedType = mIndicatedType;
    }
}

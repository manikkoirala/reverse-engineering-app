// 
// Decompiled by Procyon v0.6.0
// 

package androidx.constraintlayout.motion.widget;

import android.animation.TimeInterpolator;
import android.graphics.Rect;
import androidx.constraintlayout.core.motion.utils.KeyCache;
import androidx.constraintlayout.widget.ConstraintLayout;
import android.view.animation.AnticipateInterpolator;
import android.view.animation.OvershootInterpolator;
import android.view.animation.BounceInterpolator;
import android.view.animation.DecelerateInterpolator;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.AccelerateDecelerateInterpolator;
import androidx.constraintlayout.core.motion.utils.Easing;
import android.view.animation.AnimationUtils;
import android.view.animation.Interpolator;
import java.util.Map;
import java.util.Iterator;
import java.util.ArrayList;
import android.content.res.TypedArray;
import androidx.constraintlayout.widget.R;
import android.util.Xml;
import android.view.View;
import org.xmlpull.v1.XmlPullParserException;
import java.io.IOException;
import androidx.constraintlayout.widget.ConstraintAttribute;
import org.xmlpull.v1.XmlPullParser;
import android.content.Context;
import androidx.constraintlayout.widget.ConstraintSet;

public class ViewTransition
{
    static final int ANTICIPATE = 6;
    static final int BOUNCE = 4;
    public static final String CONSTRAINT_OVERRIDE = "ConstraintOverride";
    public static final String CUSTOM_ATTRIBUTE = "CustomAttribute";
    public static final String CUSTOM_METHOD = "CustomMethod";
    static final int EASE_IN = 1;
    static final int EASE_IN_OUT = 0;
    static final int EASE_OUT = 2;
    private static final int INTERPOLATOR_REFERENCE_ID = -2;
    public static final String KEY_FRAME_SET_TAG = "KeyFrameSet";
    static final int LINEAR = 3;
    public static final int ONSTATE_ACTION_DOWN = 1;
    public static final int ONSTATE_ACTION_DOWN_UP = 3;
    public static final int ONSTATE_ACTION_UP = 2;
    public static final int ONSTATE_SHARED_VALUE_SET = 4;
    public static final int ONSTATE_SHARED_VALUE_UNSET = 5;
    static final int OVERSHOOT = 5;
    private static final int SPLINE_STRING = -1;
    private static String TAG = "ViewTransition";
    private static final int UNSET = -1;
    static final int VIEWTRANSITIONMODE_ALLSTATES = 1;
    static final int VIEWTRANSITIONMODE_CURRENTSTATE = 0;
    static final int VIEWTRANSITIONMODE_NOSTATE = 2;
    public static final String VIEW_TRANSITION_TAG = "ViewTransition";
    private int mClearsTag;
    ConstraintSet.Constraint mConstraintDelta;
    Context mContext;
    private int mDefaultInterpolator;
    private int mDefaultInterpolatorID;
    private String mDefaultInterpolatorString;
    private boolean mDisabled;
    private int mDuration;
    private int mId;
    private int mIfTagNotSet;
    private int mIfTagSet;
    KeyFrames mKeyFrames;
    private int mOnStateTransition;
    private int mPathMotionArc;
    private int mSetsTag;
    private int mSharedValueCurrent;
    private int mSharedValueID;
    private int mSharedValueTarget;
    private int mTargetId;
    private String mTargetString;
    private int mUpDuration;
    int mViewTransitionMode;
    ConstraintSet set;
    
    ViewTransition(final Context mContext, final XmlPullParser xmlPullParser) {
        this.mOnStateTransition = -1;
        this.mDisabled = false;
        this.mPathMotionArc = 0;
        this.mDuration = -1;
        this.mUpDuration = -1;
        this.mDefaultInterpolator = 0;
        this.mDefaultInterpolatorString = null;
        this.mDefaultInterpolatorID = -1;
        this.mSetsTag = -1;
        this.mClearsTag = -1;
        this.mIfTagSet = -1;
        this.mIfTagNotSet = -1;
        this.mSharedValueTarget = -1;
        this.mSharedValueID = -1;
        this.mSharedValueCurrent = -1;
        this.mContext = mContext;
        try {
            for (int i = xmlPullParser.getEventType(); i != 1; i = xmlPullParser.next()) {
                if (i != 2) {
                    if (i == 3) {
                        if ("ViewTransition".equals(xmlPullParser.getName())) {
                            return;
                        }
                    }
                }
                else {
                    final String name = xmlPullParser.getName();
                    int n = 0;
                    Label_0268: {
                        switch (name.hashCode()) {
                            case 1791837707: {
                                if (name.equals("CustomAttribute")) {
                                    n = 3;
                                    break Label_0268;
                                }
                                break;
                            }
                            case 366511058: {
                                if (name.equals("CustomMethod")) {
                                    n = 4;
                                    break Label_0268;
                                }
                                break;
                            }
                            case 61998586: {
                                if (name.equals("ViewTransition")) {
                                    n = 0;
                                    break Label_0268;
                                }
                                break;
                            }
                            case -1239391468: {
                                if (name.equals("KeyFrameSet")) {
                                    n = 1;
                                    break Label_0268;
                                }
                                break;
                            }
                            case -1962203927: {
                                if (name.equals("ConstraintOverride")) {
                                    n = 2;
                                    break Label_0268;
                                }
                                break;
                            }
                        }
                        n = -1;
                    }
                    if (n != 0) {
                        if (n != 1) {
                            if (n != 2) {
                                if (n != 3 && n != 4) {
                                    final StringBuilder sb = new StringBuilder();
                                    sb.append(Debug.getLoc());
                                    sb.append(" unknown tag ");
                                    sb.append(name);
                                    final StringBuilder sb2 = new StringBuilder();
                                    sb2.append(".xml:");
                                    sb2.append(xmlPullParser.getLineNumber());
                                }
                                else {
                                    ConstraintAttribute.parse(mContext, xmlPullParser, this.mConstraintDelta.mCustomConstraints);
                                }
                            }
                            else {
                                this.mConstraintDelta = ConstraintSet.buildDelta(mContext, xmlPullParser);
                            }
                        }
                        else {
                            this.mKeyFrames = new KeyFrames(mContext, xmlPullParser);
                        }
                    }
                    else {
                        this.parseViewTransitionTags(mContext, xmlPullParser);
                    }
                }
            }
        }
        catch (final IOException ex) {
            ex.printStackTrace();
        }
        catch (final XmlPullParserException ex2) {
            ex2.printStackTrace();
        }
    }
    
    private void parseViewTransitionTags(final Context context, final XmlPullParser xmlPullParser) {
        final TypedArray obtainStyledAttributes = context.obtainStyledAttributes(Xml.asAttributeSet(xmlPullParser), R.styleable.ViewTransition);
        for (int indexCount = obtainStyledAttributes.getIndexCount(), i = 0; i < indexCount; ++i) {
            final int index = obtainStyledAttributes.getIndex(i);
            if (index == R.styleable.ViewTransition_android_id) {
                this.mId = obtainStyledAttributes.getResourceId(index, this.mId);
            }
            else if (index == R.styleable.ViewTransition_motionTarget) {
                if (MotionLayout.IS_IN_EDIT_MODE) {
                    if ((this.mTargetId = obtainStyledAttributes.getResourceId(index, this.mTargetId)) == -1) {
                        this.mTargetString = obtainStyledAttributes.getString(index);
                    }
                }
                else if (obtainStyledAttributes.peekValue(index).type == 3) {
                    this.mTargetString = obtainStyledAttributes.getString(index);
                }
                else {
                    this.mTargetId = obtainStyledAttributes.getResourceId(index, this.mTargetId);
                }
            }
            else if (index == R.styleable.ViewTransition_onStateTransition) {
                this.mOnStateTransition = obtainStyledAttributes.getInt(index, this.mOnStateTransition);
            }
            else if (index == R.styleable.ViewTransition_transitionDisable) {
                this.mDisabled = obtainStyledAttributes.getBoolean(index, this.mDisabled);
            }
            else if (index == R.styleable.ViewTransition_pathMotionArc) {
                this.mPathMotionArc = obtainStyledAttributes.getInt(index, this.mPathMotionArc);
            }
            else if (index == R.styleable.ViewTransition_duration) {
                this.mDuration = obtainStyledAttributes.getInt(index, this.mDuration);
            }
            else if (index == R.styleable.ViewTransition_upDuration) {
                this.mUpDuration = obtainStyledAttributes.getInt(index, this.mUpDuration);
            }
            else if (index == R.styleable.ViewTransition_viewTransitionMode) {
                this.mViewTransitionMode = obtainStyledAttributes.getInt(index, this.mViewTransitionMode);
            }
            else if (index == R.styleable.ViewTransition_motionInterpolator) {
                final int type = obtainStyledAttributes.peekValue(index).type;
                if (type == 1) {
                    if ((this.mDefaultInterpolatorID = obtainStyledAttributes.getResourceId(index, -1)) != -1) {
                        this.mDefaultInterpolator = -2;
                    }
                }
                else if (type == 3) {
                    final String string = obtainStyledAttributes.getString(index);
                    this.mDefaultInterpolatorString = string;
                    if (string != null && string.indexOf("/") > 0) {
                        this.mDefaultInterpolatorID = obtainStyledAttributes.getResourceId(index, -1);
                        this.mDefaultInterpolator = -2;
                    }
                    else {
                        this.mDefaultInterpolator = -1;
                    }
                }
                else {
                    this.mDefaultInterpolator = obtainStyledAttributes.getInteger(index, this.mDefaultInterpolator);
                }
            }
            else if (index == R.styleable.ViewTransition_setsTag) {
                this.mSetsTag = obtainStyledAttributes.getResourceId(index, this.mSetsTag);
            }
            else if (index == R.styleable.ViewTransition_clearsTag) {
                this.mClearsTag = obtainStyledAttributes.getResourceId(index, this.mClearsTag);
            }
            else if (index == R.styleable.ViewTransition_ifTagSet) {
                this.mIfTagSet = obtainStyledAttributes.getResourceId(index, this.mIfTagSet);
            }
            else if (index == R.styleable.ViewTransition_ifTagNotSet) {
                this.mIfTagNotSet = obtainStyledAttributes.getResourceId(index, this.mIfTagNotSet);
            }
            else if (index == R.styleable.ViewTransition_SharedValueId) {
                this.mSharedValueID = obtainStyledAttributes.getResourceId(index, this.mSharedValueID);
            }
            else if (index == R.styleable.ViewTransition_SharedValue) {
                this.mSharedValueTarget = obtainStyledAttributes.getInteger(index, this.mSharedValueTarget);
            }
        }
        obtainStyledAttributes.recycle();
    }
    
    private void updateTransition(final MotionScene.Transition transition, final View view) {
        final int mDuration = this.mDuration;
        if (mDuration != -1) {
            transition.setDuration(mDuration);
        }
        transition.setPathMotionArc(this.mPathMotionArc);
        transition.setInterpolatorInfo(this.mDefaultInterpolator, this.mDefaultInterpolatorString, this.mDefaultInterpolatorID);
        final int id = view.getId();
        final KeyFrames mKeyFrames = this.mKeyFrames;
        if (mKeyFrames != null) {
            final ArrayList<Key> keyFramesForView = mKeyFrames.getKeyFramesForView(-1);
            final KeyFrames keyFrames = new KeyFrames();
            final Iterator<Key> iterator = keyFramesForView.iterator();
            while (iterator.hasNext()) {
                keyFrames.addKey(iterator.next().clone().setViewId(id));
            }
            transition.addKeyFrame(keyFrames);
        }
    }
    
    void applyIndependentTransition(final ViewTransitionController viewTransitionController, final MotionLayout motionLayout, final View bothStates) {
        final MotionController motionController = new MotionController(bothStates);
        motionController.setBothStates(bothStates);
        this.mKeyFrames.addAllFrames(motionController);
        motionController.setup(((View)motionLayout).getWidth(), ((View)motionLayout).getHeight(), (float)this.mDuration, System.nanoTime());
        new Animate(viewTransitionController, motionController, this.mDuration, this.mUpDuration, this.mOnStateTransition, this.getInterpolator(((View)motionLayout).getContext()), this.mSetsTag, this.mClearsTag);
    }
    
    void applyTransition(final ViewTransitionController viewTransitionController, final MotionLayout motionLayout, int i, final ConstraintSet set, final View... array) {
        if (this.mDisabled) {
            return;
        }
        final int mViewTransitionMode = this.mViewTransitionMode;
        final int n = 0;
        if (mViewTransitionMode == 2) {
            this.applyIndependentTransition(viewTransitionController, motionLayout, array[0]);
            return;
        }
        if (mViewTransitionMode == 1) {
            final int[] constraintSetIds = motionLayout.getConstraintSetIds();
            for (int j = 0; j < constraintSetIds.length; ++j) {
                final int n2 = constraintSetIds[j];
                if (n2 != i) {
                    final ConstraintSet constraintSet = motionLayout.getConstraintSet(n2);
                    for (int length = array.length, k = 0; k < length; ++k) {
                        final ConstraintSet.Constraint constraint = constraintSet.getConstraint(array[k].getId());
                        final ConstraintSet.Constraint mConstraintDelta = this.mConstraintDelta;
                        if (mConstraintDelta != null) {
                            mConstraintDelta.applyDelta(constraint);
                            constraint.mCustomConstraints.putAll(this.mConstraintDelta.mCustomConstraints);
                        }
                    }
                }
            }
        }
        final ConstraintSet set2 = new ConstraintSet();
        set2.clone(set);
        for (int length2 = array.length, l = 0; l < length2; ++l) {
            final ConstraintSet.Constraint constraint2 = set2.getConstraint(array[l].getId());
            final ConstraintSet.Constraint mConstraintDelta2 = this.mConstraintDelta;
            if (mConstraintDelta2 != null) {
                mConstraintDelta2.applyDelta(constraint2);
                constraint2.mCustomConstraints.putAll(this.mConstraintDelta.mCustomConstraints);
            }
        }
        motionLayout.updateState(i, set2);
        final int view_transition = R.id.view_transition;
        motionLayout.updateState(view_transition, set);
        motionLayout.setState(view_transition, -1, -1);
        final MotionScene.Transition transition = new MotionScene.Transition(-1, motionLayout.mScene, view_transition, i);
        int length3;
        for (length3 = array.length, i = n; i < length3; ++i) {
            this.updateTransition(transition, array[i]);
        }
        motionLayout.setTransition(transition);
        motionLayout.transitionToEnd(new \u3007080(this, array));
    }
    
    boolean checkTags(final View view) {
        final int mIfTagSet = this.mIfTagSet;
        final boolean b = false;
        final boolean b2 = mIfTagSet == -1 || view.getTag(mIfTagSet) != null;
        final int mIfTagNotSet = this.mIfTagNotSet;
        final boolean b3 = mIfTagNotSet == -1 || view.getTag(mIfTagNotSet) == null;
        boolean b4 = b;
        if (b2) {
            b4 = b;
            if (b3) {
                b4 = true;
            }
        }
        return b4;
    }
    
    int getId() {
        return this.mId;
    }
    
    Interpolator getInterpolator(final Context context) {
        final int mDefaultInterpolator = this.mDefaultInterpolator;
        if (mDefaultInterpolator == -2) {
            return AnimationUtils.loadInterpolator(context, this.mDefaultInterpolatorID);
        }
        if (mDefaultInterpolator == -1) {
            return (Interpolator)new Interpolator(this, Easing.getInterpolator(this.mDefaultInterpolatorString)) {
                final ViewTransition this$0;
                final Easing val$easing;
                
                public float getInterpolation(final float n) {
                    return (float)this.val$easing.get(n);
                }
            };
        }
        if (mDefaultInterpolator == 0) {
            return (Interpolator)new AccelerateDecelerateInterpolator();
        }
        if (mDefaultInterpolator == 1) {
            return (Interpolator)new AccelerateInterpolator();
        }
        if (mDefaultInterpolator == 2) {
            return (Interpolator)new DecelerateInterpolator();
        }
        if (mDefaultInterpolator == 4) {
            return (Interpolator)new BounceInterpolator();
        }
        if (mDefaultInterpolator == 5) {
            return (Interpolator)new OvershootInterpolator();
        }
        if (mDefaultInterpolator != 6) {
            return null;
        }
        return (Interpolator)new AnticipateInterpolator();
    }
    
    public int getSharedValue() {
        return this.mSharedValueTarget;
    }
    
    public int getSharedValueCurrent() {
        return this.mSharedValueCurrent;
    }
    
    public int getSharedValueID() {
        return this.mSharedValueID;
    }
    
    public int getStateTransition() {
        return this.mOnStateTransition;
    }
    
    boolean isEnabled() {
        return this.mDisabled ^ true;
    }
    
    boolean matchesView(final View view) {
        if (view == null) {
            return false;
        }
        if (this.mTargetId == -1 && this.mTargetString == null) {
            return false;
        }
        if (!this.checkTags(view)) {
            return false;
        }
        if (view.getId() == this.mTargetId) {
            return true;
        }
        if (this.mTargetString == null) {
            return false;
        }
        if (view.getLayoutParams() instanceof ConstraintLayout.LayoutParams) {
            final String constraintTag = ((ConstraintLayout.LayoutParams)view.getLayoutParams()).constraintTag;
            if (constraintTag != null && constraintTag.matches(this.mTargetString)) {
                return true;
            }
        }
        return false;
    }
    
    void setEnabled(final boolean b) {
        this.mDisabled = (b ^ true);
    }
    
    void setId(final int mId) {
        this.mId = mId;
    }
    
    public void setSharedValue(final int mSharedValueTarget) {
        this.mSharedValueTarget = mSharedValueTarget;
    }
    
    public void setSharedValueCurrent(final int mSharedValueCurrent) {
        this.mSharedValueCurrent = mSharedValueCurrent;
    }
    
    public void setSharedValueID(final int mSharedValueID) {
        this.mSharedValueID = mSharedValueID;
    }
    
    public void setStateTransition(final int mOnStateTransition) {
        this.mOnStateTransition = mOnStateTransition;
    }
    
    boolean supports(final int n) {
        final int mOnStateTransition = this.mOnStateTransition;
        final boolean b = false;
        final boolean b2 = false;
        boolean b3 = false;
        if (mOnStateTransition == 1) {
            if (n == 0) {
                b3 = true;
            }
            return b3;
        }
        if (mOnStateTransition == 2) {
            boolean b4 = b;
            if (n == 1) {
                b4 = true;
            }
            return b4;
        }
        boolean b5 = b2;
        if (mOnStateTransition == 3) {
            b5 = b2;
            if (n == 0) {
                b5 = true;
            }
        }
        return b5;
    }
    
    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append("ViewTransition(");
        sb.append(Debug.getName(this.mContext, this.mId));
        sb.append(")");
        return sb.toString();
    }
    
    static class Animate
    {
        boolean hold_at_100;
        KeyCache mCache;
        private final int mClearsTag;
        float mDpositionDt;
        int mDuration;
        Interpolator mInterpolator;
        long mLastRender;
        MotionController mMC;
        float mPosition;
        private final int mSetsTag;
        long mStart;
        Rect mTempRec;
        int mUpDuration;
        ViewTransitionController mVtController;
        boolean reverse;
        
        Animate(final ViewTransitionController mVtController, final MotionController mmc, final int mDuration, final int mUpDuration, final int n, final Interpolator mInterpolator, final int mSetsTag, final int mClearsTag) {
            this.mCache = new KeyCache();
            this.reverse = false;
            this.mTempRec = new Rect();
            this.hold_at_100 = false;
            this.mVtController = mVtController;
            this.mMC = mmc;
            this.mDuration = mDuration;
            this.mUpDuration = mUpDuration;
            final long nanoTime = System.nanoTime();
            this.mStart = nanoTime;
            this.mLastRender = nanoTime;
            this.mVtController.addAnimation(this);
            this.mInterpolator = mInterpolator;
            this.mSetsTag = mSetsTag;
            this.mClearsTag = mClearsTag;
            if (n == 3) {
                this.hold_at_100 = true;
            }
            float mDpositionDt;
            if (mDuration == 0) {
                mDpositionDt = Float.MAX_VALUE;
            }
            else {
                mDpositionDt = 1.0f / mDuration;
            }
            this.mDpositionDt = mDpositionDt;
            this.mutate();
        }
        
        void mutate() {
            if (this.reverse) {
                this.mutateReverse();
            }
            else {
                this.mutateForward();
            }
        }
        
        void mutateForward() {
            final long nanoTime = System.nanoTime();
            final long mLastRender = this.mLastRender;
            this.mLastRender = nanoTime;
            final float mPosition = this.mPosition + (float)((nanoTime - mLastRender) * 1.0E-6) * this.mDpositionDt;
            this.mPosition = mPosition;
            if (mPosition >= 1.0f) {
                this.mPosition = 1.0f;
            }
            final Interpolator mInterpolator = this.mInterpolator;
            float n;
            if (mInterpolator == null) {
                n = this.mPosition;
            }
            else {
                n = ((TimeInterpolator)mInterpolator).getInterpolation(this.mPosition);
            }
            final MotionController mmc = this.mMC;
            final boolean interpolate = mmc.interpolate(mmc.mView, n, nanoTime, this.mCache);
            if (this.mPosition >= 1.0f) {
                if (this.mSetsTag != -1) {
                    this.mMC.getView().setTag(this.mSetsTag, (Object)System.nanoTime());
                }
                if (this.mClearsTag != -1) {
                    this.mMC.getView().setTag(this.mClearsTag, (Object)null);
                }
                if (!this.hold_at_100) {
                    this.mVtController.removeAnimation(this);
                }
            }
            if (this.mPosition < 1.0f || interpolate) {
                this.mVtController.invalidate();
            }
        }
        
        void mutateReverse() {
            final long nanoTime = System.nanoTime();
            final long mLastRender = this.mLastRender;
            this.mLastRender = nanoTime;
            final float mPosition = this.mPosition - (float)((nanoTime - mLastRender) * 1.0E-6) * this.mDpositionDt;
            this.mPosition = mPosition;
            if (mPosition < 0.0f) {
                this.mPosition = 0.0f;
            }
            final Interpolator mInterpolator = this.mInterpolator;
            float n;
            if (mInterpolator == null) {
                n = this.mPosition;
            }
            else {
                n = ((TimeInterpolator)mInterpolator).getInterpolation(this.mPosition);
            }
            final MotionController mmc = this.mMC;
            final boolean interpolate = mmc.interpolate(mmc.mView, n, nanoTime, this.mCache);
            if (this.mPosition <= 0.0f) {
                if (this.mSetsTag != -1) {
                    this.mMC.getView().setTag(this.mSetsTag, (Object)System.nanoTime());
                }
                if (this.mClearsTag != -1) {
                    this.mMC.getView().setTag(this.mClearsTag, (Object)null);
                }
                this.mVtController.removeAnimation(this);
            }
            if (this.mPosition > 0.0f || interpolate) {
                this.mVtController.invalidate();
            }
        }
        
        public void reactTo(final int n, final float n2, final float n3) {
            if (n != 1) {
                if (n == 2) {
                    this.mMC.getView().getHitRect(this.mTempRec);
                    if (!this.mTempRec.contains((int)n2, (int)n3) && !this.reverse) {
                        this.reverse(true);
                    }
                }
                return;
            }
            if (!this.reverse) {
                this.reverse(true);
            }
        }
        
        void reverse(final boolean reverse) {
            this.reverse = reverse;
            if (reverse) {
                final int mUpDuration = this.mUpDuration;
                if (mUpDuration != -1) {
                    float mDpositionDt;
                    if (mUpDuration == 0) {
                        mDpositionDt = Float.MAX_VALUE;
                    }
                    else {
                        mDpositionDt = 1.0f / mUpDuration;
                    }
                    this.mDpositionDt = mDpositionDt;
                }
            }
            this.mVtController.invalidate();
            this.mLastRender = System.nanoTime();
        }
    }
}

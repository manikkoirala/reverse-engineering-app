// 
// Decompiled by Procyon v0.6.0
// 

package androidx.constraintlayout.motion.widget;

import android.graphics.Rect;
import java.util.HashSet;
import androidx.constraintlayout.widget.ConstraintSet;
import android.view.View;
import java.util.Iterator;
import androidx.constraintlayout.motion.utils.ViewSpline;
import java.util.HashMap;
import androidx.constraintlayout.core.motion.utils.Easing;
import androidx.constraintlayout.widget.ConstraintAttribute;
import java.util.LinkedHashMap;

class MotionConstrainedPoint implements Comparable<MotionConstrainedPoint>
{
    static final int CARTESIAN = 2;
    public static final boolean DEBUG = false;
    static final int PERPENDICULAR = 1;
    public static final String TAG = "MotionPaths";
    static String[] names;
    private float alpha;
    private boolean applyElevation;
    LinkedHashMap<String, ConstraintAttribute> attributes;
    private float elevation;
    private float height;
    private int mAnimateRelativeTo;
    private int mDrawPath;
    private Easing mKeyFrameEasing;
    int mMode;
    private float mPathRotate;
    private float mPivotX;
    private float mPivotY;
    private float mProgress;
    double[] mTempDelta;
    double[] mTempValue;
    int mVisibilityMode;
    private float position;
    private float rotation;
    private float rotationX;
    public float rotationY;
    private float scaleX;
    private float scaleY;
    private float translationX;
    private float translationY;
    private float translationZ;
    int visibility;
    private float width;
    private float x;
    private float y;
    
    static {
        MotionConstrainedPoint.names = new String[] { "position", "x", "y", "width", "height", "pathRotate" };
    }
    
    public MotionConstrainedPoint() {
        this.alpha = 1.0f;
        this.mVisibilityMode = 0;
        this.applyElevation = false;
        this.elevation = 0.0f;
        this.rotation = 0.0f;
        this.rotationX = 0.0f;
        this.rotationY = 0.0f;
        this.scaleX = 1.0f;
        this.scaleY = 1.0f;
        this.mPivotX = Float.NaN;
        this.mPivotY = Float.NaN;
        this.translationX = 0.0f;
        this.translationY = 0.0f;
        this.translationZ = 0.0f;
        this.mDrawPath = 0;
        this.mPathRotate = Float.NaN;
        this.mProgress = Float.NaN;
        this.mAnimateRelativeTo = -1;
        this.attributes = new LinkedHashMap<String, ConstraintAttribute>();
        this.mMode = 0;
        this.mTempValue = new double[18];
        this.mTempDelta = new double[18];
    }
    
    private boolean diff(final float n, final float n2) {
        final boolean naN = Float.isNaN(n);
        final boolean b = true;
        boolean b2 = true;
        if (!naN && !Float.isNaN(n2)) {
            if (Math.abs(n - n2) <= 1.0E-6f) {
                b2 = false;
            }
            return b2;
        }
        return Float.isNaN(n) != Float.isNaN(n2) && b;
    }
    
    public void addValues(final HashMap<String, ViewSpline> hashMap, final int i) {
        for (final String str : hashMap.keySet()) {
            final ViewSpline obj = hashMap.get(str);
            str.hashCode();
            final int hashCode = str.hashCode();
            int n = -1;
            switch (hashCode) {
                case 92909918: {
                    if (!str.equals("alpha")) {
                        break;
                    }
                    n = 13;
                    break;
                }
                case 37232917: {
                    if (!str.equals("transitionPathRotate")) {
                        break;
                    }
                    n = 12;
                    break;
                }
                case -4379043: {
                    if (!str.equals("elevation")) {
                        break;
                    }
                    n = 11;
                    break;
                }
                case -40300674: {
                    if (!str.equals("rotation")) {
                        break;
                    }
                    n = 10;
                    break;
                }
                case -760884509: {
                    if (!str.equals("transformPivotY")) {
                        break;
                    }
                    n = 9;
                    break;
                }
                case -760884510: {
                    if (!str.equals("transformPivotX")) {
                        break;
                    }
                    n = 8;
                    break;
                }
                case -908189617: {
                    if (!str.equals("scaleY")) {
                        break;
                    }
                    n = 7;
                    break;
                }
                case -908189618: {
                    if (!str.equals("scaleX")) {
                        break;
                    }
                    n = 6;
                    break;
                }
                case -1001078227: {
                    if (!str.equals("progress")) {
                        break;
                    }
                    n = 5;
                    break;
                }
                case -1225497655: {
                    if (!str.equals("translationZ")) {
                        break;
                    }
                    n = 4;
                    break;
                }
                case -1225497656: {
                    if (!str.equals("translationY")) {
                        break;
                    }
                    n = 3;
                    break;
                }
                case -1225497657: {
                    if (!str.equals("translationX")) {
                        break;
                    }
                    n = 2;
                    break;
                }
                case -1249320805: {
                    if (!str.equals("rotationY")) {
                        break;
                    }
                    n = 1;
                    break;
                }
                case -1249320806: {
                    if (!str.equals("rotationX")) {
                        break;
                    }
                    n = 0;
                    break;
                }
            }
            float n2 = 1.0f;
            final float n3 = 0.0f;
            final float n4 = 0.0f;
            final float n5 = 0.0f;
            final float n6 = 0.0f;
            final float n7 = 0.0f;
            final float n8 = 0.0f;
            final float n9 = 0.0f;
            final float n10 = 0.0f;
            final float n11 = 0.0f;
            final float n12 = 0.0f;
            final float n13 = 0.0f;
            switch (n) {
                default: {
                    if (!str.startsWith("CUSTOM")) {
                        final StringBuilder sb = new StringBuilder();
                        sb.append("UNKNOWN spline ");
                        sb.append(str);
                        continue;
                    }
                    final String s = str.split(",")[1];
                    if (!this.attributes.containsKey(s)) {
                        continue;
                    }
                    final ConstraintAttribute constraintAttribute = this.attributes.get(s);
                    if (obj instanceof ViewSpline.CustomSet) {
                        ((ViewSpline.CustomSet)obj).setPoint(i, constraintAttribute);
                        continue;
                    }
                    final StringBuilder sb2 = new StringBuilder();
                    sb2.append(str);
                    sb2.append(" ViewSpline not a CustomSet frame = ");
                    sb2.append(i);
                    sb2.append(", value");
                    sb2.append(constraintAttribute.getValueToInterpolate());
                    sb2.append(obj);
                    continue;
                }
                case 13: {
                    if (!Float.isNaN(this.alpha)) {
                        n2 = this.alpha;
                    }
                    obj.setPoint(i, n2);
                    continue;
                }
                case 12: {
                    float mPathRotate;
                    if (Float.isNaN(this.mPathRotate)) {
                        mPathRotate = n13;
                    }
                    else {
                        mPathRotate = this.mPathRotate;
                    }
                    obj.setPoint(i, mPathRotate);
                    continue;
                }
                case 11: {
                    float elevation;
                    if (Float.isNaN(this.elevation)) {
                        elevation = n3;
                    }
                    else {
                        elevation = this.elevation;
                    }
                    obj.setPoint(i, elevation);
                    continue;
                }
                case 10: {
                    float rotation;
                    if (Float.isNaN(this.rotation)) {
                        rotation = n4;
                    }
                    else {
                        rotation = this.rotation;
                    }
                    obj.setPoint(i, rotation);
                    continue;
                }
                case 9: {
                    float mPivotY;
                    if (Float.isNaN(this.mPivotY)) {
                        mPivotY = n5;
                    }
                    else {
                        mPivotY = this.mPivotY;
                    }
                    obj.setPoint(i, mPivotY);
                    continue;
                }
                case 8: {
                    float mPivotX;
                    if (Float.isNaN(this.mPivotX)) {
                        mPivotX = n6;
                    }
                    else {
                        mPivotX = this.mPivotX;
                    }
                    obj.setPoint(i, mPivotX);
                    continue;
                }
                case 7: {
                    if (!Float.isNaN(this.scaleY)) {
                        n2 = this.scaleY;
                    }
                    obj.setPoint(i, n2);
                    continue;
                }
                case 6: {
                    if (!Float.isNaN(this.scaleX)) {
                        n2 = this.scaleX;
                    }
                    obj.setPoint(i, n2);
                    continue;
                }
                case 5: {
                    float mProgress;
                    if (Float.isNaN(this.mProgress)) {
                        mProgress = n7;
                    }
                    else {
                        mProgress = this.mProgress;
                    }
                    obj.setPoint(i, mProgress);
                    continue;
                }
                case 4: {
                    float translationZ;
                    if (Float.isNaN(this.translationZ)) {
                        translationZ = n8;
                    }
                    else {
                        translationZ = this.translationZ;
                    }
                    obj.setPoint(i, translationZ);
                    continue;
                }
                case 3: {
                    float translationY;
                    if (Float.isNaN(this.translationY)) {
                        translationY = n9;
                    }
                    else {
                        translationY = this.translationY;
                    }
                    obj.setPoint(i, translationY);
                    continue;
                }
                case 2: {
                    float translationX;
                    if (Float.isNaN(this.translationX)) {
                        translationX = n10;
                    }
                    else {
                        translationX = this.translationX;
                    }
                    obj.setPoint(i, translationX);
                    continue;
                }
                case 1: {
                    float rotationY;
                    if (Float.isNaN(this.rotationY)) {
                        rotationY = n11;
                    }
                    else {
                        rotationY = this.rotationY;
                    }
                    obj.setPoint(i, rotationY);
                    continue;
                }
                case 0: {
                    float rotationX;
                    if (Float.isNaN(this.rotationX)) {
                        rotationX = n12;
                    }
                    else {
                        rotationX = this.rotationX;
                    }
                    obj.setPoint(i, rotationX);
                    continue;
                }
            }
        }
    }
    
    public void applyParameters(final View view) {
        this.visibility = view.getVisibility();
        float alpha;
        if (view.getVisibility() != 0) {
            alpha = 0.0f;
        }
        else {
            alpha = view.getAlpha();
        }
        this.alpha = alpha;
        this.applyElevation = false;
        this.elevation = view.getElevation();
        this.rotation = view.getRotation();
        this.rotationX = view.getRotationX();
        this.rotationY = view.getRotationY();
        this.scaleX = view.getScaleX();
        this.scaleY = view.getScaleY();
        this.mPivotX = view.getPivotX();
        this.mPivotY = view.getPivotY();
        this.translationX = view.getTranslationX();
        this.translationY = view.getTranslationY();
        this.translationZ = view.getTranslationZ();
    }
    
    public void applyParameters(final ConstraintSet.Constraint constraint) {
        final ConstraintSet.PropertySet propertySet = constraint.propertySet;
        final int mVisibilityMode = propertySet.mVisibilityMode;
        this.mVisibilityMode = mVisibilityMode;
        final int visibility = propertySet.visibility;
        this.visibility = visibility;
        float alpha;
        if (visibility != 0 && mVisibilityMode == 0) {
            alpha = 0.0f;
        }
        else {
            alpha = propertySet.alpha;
        }
        this.alpha = alpha;
        final ConstraintSet.Transform transform = constraint.transform;
        this.applyElevation = transform.applyElevation;
        this.elevation = transform.elevation;
        this.rotation = transform.rotation;
        this.rotationX = transform.rotationX;
        this.rotationY = transform.rotationY;
        this.scaleX = transform.scaleX;
        this.scaleY = transform.scaleY;
        this.mPivotX = transform.transformPivotX;
        this.mPivotY = transform.transformPivotY;
        this.translationX = transform.translationX;
        this.translationY = transform.translationY;
        this.translationZ = transform.translationZ;
        this.mKeyFrameEasing = Easing.getInterpolator(constraint.motion.mTransitionEasing);
        final ConstraintSet.Motion motion = constraint.motion;
        this.mPathRotate = motion.mPathRotate;
        this.mDrawPath = motion.mDrawPath;
        this.mAnimateRelativeTo = motion.mAnimateRelativeTo;
        this.mProgress = constraint.propertySet.mProgress;
        for (final String s : constraint.mCustomConstraints.keySet()) {
            final ConstraintAttribute value = constraint.mCustomConstraints.get(s);
            if (value.isContinuous()) {
                this.attributes.put(s, value);
            }
        }
    }
    
    @Override
    public int compareTo(final MotionConstrainedPoint motionConstrainedPoint) {
        return Float.compare(this.position, motionConstrainedPoint.position);
    }
    
    void different(final MotionConstrainedPoint motionConstrainedPoint, final HashSet<String> set) {
        if (this.diff(this.alpha, motionConstrainedPoint.alpha)) {
            set.add("alpha");
        }
        if (this.diff(this.elevation, motionConstrainedPoint.elevation)) {
            set.add("elevation");
        }
        final int visibility = this.visibility;
        final int visibility2 = motionConstrainedPoint.visibility;
        if (visibility != visibility2 && this.mVisibilityMode == 0 && (visibility == 0 || visibility2 == 0)) {
            set.add("alpha");
        }
        if (this.diff(this.rotation, motionConstrainedPoint.rotation)) {
            set.add("rotation");
        }
        if (!Float.isNaN(this.mPathRotate) || !Float.isNaN(motionConstrainedPoint.mPathRotate)) {
            set.add("transitionPathRotate");
        }
        if (!Float.isNaN(this.mProgress) || !Float.isNaN(motionConstrainedPoint.mProgress)) {
            set.add("progress");
        }
        if (this.diff(this.rotationX, motionConstrainedPoint.rotationX)) {
            set.add("rotationX");
        }
        if (this.diff(this.rotationY, motionConstrainedPoint.rotationY)) {
            set.add("rotationY");
        }
        if (this.diff(this.mPivotX, motionConstrainedPoint.mPivotX)) {
            set.add("transformPivotX");
        }
        if (this.diff(this.mPivotY, motionConstrainedPoint.mPivotY)) {
            set.add("transformPivotY");
        }
        if (this.diff(this.scaleX, motionConstrainedPoint.scaleX)) {
            set.add("scaleX");
        }
        if (this.diff(this.scaleY, motionConstrainedPoint.scaleY)) {
            set.add("scaleY");
        }
        if (this.diff(this.translationX, motionConstrainedPoint.translationX)) {
            set.add("translationX");
        }
        if (this.diff(this.translationY, motionConstrainedPoint.translationY)) {
            set.add("translationY");
        }
        if (this.diff(this.translationZ, motionConstrainedPoint.translationZ)) {
            set.add("translationZ");
        }
    }
    
    void different(final MotionConstrainedPoint motionConstrainedPoint, final boolean[] array, final String[] array2) {
        array[0] |= this.diff(this.position, motionConstrainedPoint.position);
        array[1] |= this.diff(this.x, motionConstrainedPoint.x);
        array[2] |= this.diff(this.y, motionConstrainedPoint.y);
        array[3] |= this.diff(this.width, motionConstrainedPoint.width);
        array[4] |= this.diff(this.height, motionConstrainedPoint.height);
    }
    
    void fillStandard(final double[] array, final int[] array2) {
        final float position = this.position;
        int i = 0;
        final float x = this.x;
        final float y = this.y;
        final float width = this.width;
        final float height = this.height;
        final float alpha = this.alpha;
        final float elevation = this.elevation;
        final float rotation = this.rotation;
        final float rotationX = this.rotationX;
        final float rotationY = this.rotationY;
        final float scaleX = this.scaleX;
        final float scaleY = this.scaleY;
        final float mPivotX = this.mPivotX;
        final float mPivotY = this.mPivotY;
        final float translationX = this.translationX;
        final float translationY = this.translationY;
        final float translationZ = this.translationZ;
        final float mPathRotate = this.mPathRotate;
        int n = 0;
        while (i < array2.length) {
            final int n2 = array2[i];
            int n3 = n;
            if (n2 < 18) {
                array[n] = (new float[] { position, x, y, width, height, alpha, elevation, rotation, rotationX, rotationY, scaleX, scaleY, mPivotX, mPivotY, translationX, translationY, translationZ, mPathRotate })[n2];
                n3 = n + 1;
            }
            ++i;
            n = n3;
        }
    }
    
    int getCustomData(final String key, final double[] array, int n) {
        final ConstraintAttribute constraintAttribute = this.attributes.get(key);
        if (constraintAttribute.numberOfInterpolatedValues() == 1) {
            array[n] = constraintAttribute.getValueToInterpolate();
            return 1;
        }
        final int numberOfInterpolatedValues = constraintAttribute.numberOfInterpolatedValues();
        final float[] array2 = new float[numberOfInterpolatedValues];
        constraintAttribute.getValuesToInterpolate(array2);
        for (int i = 0; i < numberOfInterpolatedValues; ++i, ++n) {
            array[n] = array2[i];
        }
        return numberOfInterpolatedValues;
    }
    
    int getCustomDataCount(final String key) {
        return this.attributes.get(key).numberOfInterpolatedValues();
    }
    
    boolean hasCustomData(final String key) {
        return this.attributes.containsKey(key);
    }
    
    void setBounds(final float x, final float y, final float width, final float height) {
        this.x = x;
        this.y = y;
        this.width = width;
        this.height = height;
    }
    
    public void setState(final Rect rect, final View view, final int n, final float n2) {
        this.setBounds((float)rect.left, (float)rect.top, (float)rect.width(), (float)rect.height());
        this.applyParameters(view);
        this.mPivotX = Float.NaN;
        this.mPivotY = Float.NaN;
        if (n != 1) {
            if (n == 2) {
                this.rotation = n2 + 90.0f;
            }
        }
        else {
            this.rotation = n2 - 90.0f;
        }
    }
    
    public void setState(final Rect rect, final ConstraintSet set, final int n, final int n2) {
        this.setBounds((float)rect.left, (float)rect.top, (float)rect.width(), (float)rect.height());
        this.applyParameters(set.getParameters(n2));
        Label_0095: {
            if (n != 1) {
                if (n != 2) {
                    if (n == 3) {
                        break Label_0095;
                    }
                    if (n != 4) {
                        return;
                    }
                }
                final float rotation = this.rotation + 90.0f;
                this.rotation = rotation;
                if (rotation > 180.0f) {
                    this.rotation = rotation - 360.0f;
                }
                return;
            }
        }
        this.rotation -= 90.0f;
    }
    
    public void setState(final View view) {
        this.setBounds(view.getX(), view.getY(), (float)view.getWidth(), (float)view.getHeight());
        this.applyParameters(view);
    }
}

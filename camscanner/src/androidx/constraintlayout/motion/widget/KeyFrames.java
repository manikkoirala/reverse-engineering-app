// 
// Decompiled by Procyon v0.6.0
// 

package androidx.constraintlayout.motion.widget;

import java.util.Set;
import java.util.Iterator;
import androidx.constraintlayout.widget.ConstraintLayout;
import org.xmlpull.v1.XmlPullParser;
import android.content.Context;
import java.util.ArrayList;
import java.lang.reflect.Constructor;
import java.util.HashMap;

public class KeyFrames
{
    private static final String CUSTOM_ATTRIBUTE = "CustomAttribute";
    private static final String CUSTOM_METHOD = "CustomMethod";
    private static final String TAG = "KeyFrames";
    public static final int UNSET = -1;
    static HashMap<String, Constructor<? extends Key>> sKeyMakers;
    private HashMap<Integer, ArrayList<Key>> mFramesMap;
    
    static {
        final HashMap<String, Constructor<? extends Key>> hashMap = KeyFrames.sKeyMakers = new HashMap<String, Constructor<? extends Key>>();
        try {
            hashMap.put("KeyAttribute", KeyAttributes.class.getConstructor((Class<?>[])new Class[0]));
            KeyFrames.sKeyMakers.put("KeyPosition", KeyPosition.class.getConstructor((Class<?>[])new Class[0]));
            KeyFrames.sKeyMakers.put("KeyCycle", KeyCycle.class.getConstructor((Class<?>[])new Class[0]));
            KeyFrames.sKeyMakers.put("KeyTimeCycle", KeyTimeCycle.class.getConstructor((Class<?>[])new Class[0]));
            KeyFrames.sKeyMakers.put("KeyTrigger", KeyTrigger.class.getConstructor((Class<?>[])new Class[0]));
        }
        catch (final NoSuchMethodException ex) {}
    }
    
    public KeyFrames() {
        this.mFramesMap = new HashMap<Integer, ArrayList<Key>>();
    }
    
    public KeyFrames(final Context p0, final XmlPullParser p1) {
        // 
        // This method could not be decompiled.
        // 
        // Original Bytecode:
        // 
        //     1: invokespecial   java/lang/Object.<init>:()V
        //     4: aload_0        
        //     5: new             Ljava/util/HashMap;
        //     8: dup            
        //     9: invokespecial   java/util/HashMap.<init>:()V
        //    12: putfield        androidx/constraintlayout/motion/widget/KeyFrames.mFramesMap:Ljava/util/HashMap;
        //    15: aload_2        
        //    16: invokeinterface org/xmlpull/v1/XmlPullParser.getEventType:()I
        //    21: istore_3       
        //    22: aconst_null    
        //    23: astore          5
        //    25: iload_3        
        //    26: iconst_1       
        //    27: if_icmpeq       318
        //    30: iload_3        
        //    31: iconst_2       
        //    32: if_icmpeq       66
        //    35: iload_3        
        //    36: iconst_3       
        //    37: if_icmpeq       47
        //    40: aload           5
        //    42: astore          6
        //    44: goto            291
        //    47: aload           5
        //    49: astore          6
        //    51: ldc             "KeyFrameSet"
        //    53: aload_2        
        //    54: invokeinterface org/xmlpull/v1/XmlPullParser.getName:()Ljava/lang/String;
        //    59: invokevirtual   java/lang/String.equals:(Ljava/lang/Object;)Z
        //    62: ifeq            291
        //    65: return         
        //    66: aload_2        
        //    67: invokeinterface org/xmlpull/v1/XmlPullParser.getName:()Ljava/lang/String;
        //    72: astore          7
        //    74: getstatic       androidx/constraintlayout/motion/widget/KeyFrames.sKeyMakers:Ljava/util/HashMap;
        //    77: aload           7
        //    79: invokevirtual   java/util/HashMap.containsKey:(Ljava/lang/Object;)Z
        //    82: istore          4
        //    84: iload           4
        //    86: ifeq            192
        //    89: getstatic       androidx/constraintlayout/motion/widget/KeyFrames.sKeyMakers:Ljava/util/HashMap;
        //    92: aload           7
        //    94: invokevirtual   java/util/HashMap.get:(Ljava/lang/Object;)Ljava/lang/Object;
        //    97: checkcast       Ljava/lang/reflect/Constructor;
        //   100: astore          6
        //   102: aload           6
        //   104: ifnull          140
        //   107: aload           6
        //   109: iconst_0       
        //   110: anewarray       Ljava/lang/Object;
        //   113: invokevirtual   java/lang/reflect/Constructor.newInstance:([Ljava/lang/Object;)Ljava/lang/Object;
        //   116: checkcast       Landroidx/constraintlayout/motion/widget/Key;
        //   119: astore          6
        //   121: aload           6
        //   123: aload_1        
        //   124: aload_2        
        //   125: invokestatic    android/util/Xml.asAttributeSet:(Lorg/xmlpull/v1/XmlPullParser;)Landroid/util/AttributeSet;
        //   128: invokevirtual   androidx/constraintlayout/motion/widget/Key.load:(Landroid/content/Context;Landroid/util/AttributeSet;)V
        //   131: aload_0        
        //   132: aload           6
        //   134: invokevirtual   androidx/constraintlayout/motion/widget/KeyFrames.addKey:(Landroidx/constraintlayout/motion/widget/Key;)V
        //   137: goto            291
        //   140: new             Ljava/lang/NullPointerException;
        //   143: astore          6
        //   145: new             Ljava/lang/StringBuilder;
        //   148: astore          8
        //   150: aload           8
        //   152: invokespecial   java/lang/StringBuilder.<init>:()V
        //   155: aload           8
        //   157: ldc             "Keymaker for "
        //   159: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   162: pop            
        //   163: aload           8
        //   165: aload           7
        //   167: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   170: pop            
        //   171: aload           8
        //   173: ldc             " not found"
        //   175: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   178: pop            
        //   179: aload           6
        //   181: aload           8
        //   183: invokevirtual   java/lang/StringBuilder.toString:()Ljava/lang/String;
        //   186: invokespecial   java/lang/NullPointerException.<init>:(Ljava/lang/String;)V
        //   189: aload           6
        //   191: athrow         
        //   192: aload           7
        //   194: ldc             "CustomAttribute"
        //   196: invokevirtual   java/lang/String.equalsIgnoreCase:(Ljava/lang/String;)Z
        //   199: ifeq            241
        //   202: aload           5
        //   204: astore          6
        //   206: aload           5
        //   208: ifnull          291
        //   211: aload           5
        //   213: getfield        androidx/constraintlayout/motion/widget/Key.mCustomConstraints:Ljava/util/HashMap;
        //   216: astore          7
        //   218: aload           5
        //   220: astore          6
        //   222: aload           7
        //   224: ifnull          291
        //   227: aload_1        
        //   228: aload_2        
        //   229: aload           7
        //   231: invokestatic    androidx/constraintlayout/widget/ConstraintAttribute.parse:(Landroid/content/Context;Lorg/xmlpull/v1/XmlPullParser;Ljava/util/HashMap;)V
        //   234: aload           5
        //   236: astore          6
        //   238: goto            291
        //   241: aload           5
        //   243: astore          6
        //   245: aload           7
        //   247: ldc             "CustomMethod"
        //   249: invokevirtual   java/lang/String.equalsIgnoreCase:(Ljava/lang/String;)Z
        //   252: ifeq            291
        //   255: aload           5
        //   257: astore          6
        //   259: aload           5
        //   261: ifnull          291
        //   264: aload           5
        //   266: getfield        androidx/constraintlayout/motion/widget/Key.mCustomConstraints:Ljava/util/HashMap;
        //   269: astore          7
        //   271: aload           5
        //   273: astore          6
        //   275: aload           7
        //   277: ifnull          291
        //   280: aload_1        
        //   281: aload_2        
        //   282: aload           7
        //   284: invokestatic    androidx/constraintlayout/widget/ConstraintAttribute.parse:(Landroid/content/Context;Lorg/xmlpull/v1/XmlPullParser;Ljava/util/HashMap;)V
        //   287: aload           5
        //   289: astore          6
        //   291: aload_2        
        //   292: invokeinterface org/xmlpull/v1/XmlPullParser.next:()I
        //   297: istore_3       
        //   298: aload           6
        //   300: astore          5
        //   302: goto            25
        //   305: astore_1       
        //   306: aload_1        
        //   307: invokevirtual   java/lang/Throwable.printStackTrace:()V
        //   310: goto            318
        //   313: astore_1       
        //   314: aload_1        
        //   315: invokevirtual   org/xmlpull/v1/XmlPullParserException.printStackTrace:()V
        //   318: return         
        //   319: astore          6
        //   321: aload           5
        //   323: astore          6
        //   325: goto            291
        //   328: astore          5
        //   330: goto            137
        //    Exceptions:
        //  Try           Handler
        //  Start  End    Start  End    Type                                   
        //  -----  -----  -----  -----  ---------------------------------------
        //  15     22     313    318    Lorg/xmlpull/v1/XmlPullParserException;
        //  15     22     305    313    Ljava/io/IOException;
        //  51     65     313    318    Lorg/xmlpull/v1/XmlPullParserException;
        //  51     65     305    313    Ljava/io/IOException;
        //  66     84     313    318    Lorg/xmlpull/v1/XmlPullParserException;
        //  66     84     305    313    Ljava/io/IOException;
        //  89     102    319    328    Ljava/lang/Exception;
        //  107    121    319    328    Ljava/lang/Exception;
        //  121    137    328    333    Ljava/lang/Exception;
        //  140    192    319    328    Ljava/lang/Exception;
        //  192    202    313    318    Lorg/xmlpull/v1/XmlPullParserException;
        //  192    202    305    313    Ljava/io/IOException;
        //  211    218    313    318    Lorg/xmlpull/v1/XmlPullParserException;
        //  211    218    305    313    Ljava/io/IOException;
        //  227    234    313    318    Lorg/xmlpull/v1/XmlPullParserException;
        //  227    234    305    313    Ljava/io/IOException;
        //  245    255    313    318    Lorg/xmlpull/v1/XmlPullParserException;
        //  245    255    305    313    Ljava/io/IOException;
        //  264    271    313    318    Lorg/xmlpull/v1/XmlPullParserException;
        //  264    271    305    313    Ljava/io/IOException;
        //  280    287    313    318    Lorg/xmlpull/v1/XmlPullParserException;
        //  280    287    305    313    Ljava/io/IOException;
        //  291    298    313    318    Lorg/xmlpull/v1/XmlPullParserException;
        //  291    298    305    313    Ljava/io/IOException;
        // 
        // The error that occurred was:
        // 
        // java.lang.IndexOutOfBoundsException: Index 152 out of bounds for length 152
        //     at java.base/jdk.internal.util.Preconditions.outOfBounds(Preconditions.java:64)
        //     at java.base/jdk.internal.util.Preconditions.outOfBoundsCheckIndex(Preconditions.java:70)
        //     at java.base/jdk.internal.util.Preconditions.checkIndex(Preconditions.java:266)
        //     at java.base/java.util.Objects.checkIndex(Objects.java:361)
        //     at java.base/java.util.ArrayList.get(ArrayList.java:427)
        //     at com.strobel.decompiler.ast.AstBuilder.convertToAst(AstBuilder.java:3362)
        //     at com.strobel.decompiler.ast.AstBuilder.build(AstBuilder.java:112)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:203)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:93)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethodBody(AstBuilder.java:868)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createConstructor(AstBuilder.java:799)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addTypeMembers(AstBuilder.java:635)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeCore(AstBuilder.java:605)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeNoCache(AstBuilder.java:195)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createType(AstBuilder.java:162)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addType(AstBuilder.java:137)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.buildAst(JavaLanguage.java:71)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.decompileType(JavaLanguage.java:59)
        //     at com.strobel.decompiler.DecompilerDriver.decompileType(DecompilerDriver.java:333)
        //     at com.strobel.decompiler.DecompilerDriver.decompileJar(DecompilerDriver.java:254)
        //     at com.strobel.decompiler.DecompilerDriver.main(DecompilerDriver.java:144)
        // 
        throw new IllegalStateException("An error occurred while decompiling this method.");
    }
    
    static String name(final int n, final Context context) {
        return context.getResources().getResourceEntryName(n);
    }
    
    public void addAllFrames(final MotionController motionController) {
        final ArrayList list = this.mFramesMap.get(-1);
        if (list != null) {
            motionController.addKeys(list);
        }
    }
    
    public void addFrames(final MotionController motionController) {
        final ArrayList list = this.mFramesMap.get(motionController.mId);
        if (list != null) {
            motionController.addKeys(list);
        }
        final ArrayList list2 = this.mFramesMap.get(-1);
        if (list2 != null) {
            for (final Key key : list2) {
                if (key.matches(((ConstraintLayout.LayoutParams)motionController.mView.getLayoutParams()).constraintTag)) {
                    motionController.addKey(key);
                }
            }
        }
    }
    
    public void addKey(final Key e) {
        if (!this.mFramesMap.containsKey(e.mTargetId)) {
            this.mFramesMap.put(e.mTargetId, new ArrayList<Key>());
        }
        final ArrayList list = this.mFramesMap.get(e.mTargetId);
        if (list != null) {
            list.add(e);
        }
    }
    
    public ArrayList<Key> getKeyFramesForView(final int i) {
        return this.mFramesMap.get(i);
    }
    
    public Set<Integer> getKeys() {
        return this.mFramesMap.keySet();
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.constraintlayout.motion.widget;

import android.view.animation.Interpolator;

public abstract class MotionInterpolator implements Interpolator
{
    public abstract float getInterpolation(final float p0);
    
    public abstract float getVelocity();
}

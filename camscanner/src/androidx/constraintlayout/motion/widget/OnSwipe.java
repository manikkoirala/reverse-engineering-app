// 
// Decompiled by Procyon v0.6.0
// 

package androidx.constraintlayout.motion.widget;

public class OnSwipe
{
    public static final int COMPLETE_MODE_CONTINUOUS_VELOCITY = 0;
    public static final int COMPLETE_MODE_SPRING = 1;
    public static final int DRAG_ANTICLOCKWISE = 7;
    public static final int DRAG_CLOCKWISE = 6;
    public static final int DRAG_DOWN = 1;
    public static final int DRAG_END = 5;
    public static final int DRAG_LEFT = 2;
    public static final int DRAG_RIGHT = 3;
    public static final int DRAG_START = 4;
    public static final int DRAG_UP = 0;
    public static final int FLAG_DISABLE_POST_SCROLL = 1;
    public static final int FLAG_DISABLE_SCROLL = 2;
    public static final int ON_UP_AUTOCOMPLETE = 0;
    public static final int ON_UP_AUTOCOMPLETE_TO_END = 2;
    public static final int ON_UP_AUTOCOMPLETE_TO_START = 1;
    public static final int ON_UP_DECELERATE = 4;
    public static final int ON_UP_DECELERATE_AND_COMPLETE = 5;
    public static final int ON_UP_NEVER_TO_END = 7;
    public static final int ON_UP_NEVER_TO_START = 6;
    public static final int ON_UP_STOP = 3;
    public static final int SIDE_BOTTOM = 3;
    public static final int SIDE_END = 6;
    public static final int SIDE_LEFT = 1;
    public static final int SIDE_MIDDLE = 4;
    public static final int SIDE_RIGHT = 2;
    public static final int SIDE_START = 5;
    public static final int SIDE_TOP = 0;
    public static final int SPRING_BOUNDARY_BOUNCEBOTH = 3;
    public static final int SPRING_BOUNDARY_BOUNCEEND = 2;
    public static final int SPRING_BOUNDARY_BOUNCESTART = 1;
    public static final int SPRING_BOUNDARY_OVERSHOOT = 0;
    private int mAutoCompleteMode;
    private int mDragDirection;
    private float mDragScale;
    private float mDragThreshold;
    private int mFlags;
    private int mLimitBoundsTo;
    private float mMaxAcceleration;
    private float mMaxVelocity;
    private boolean mMoveWhenScrollAtTop;
    private int mOnTouchUp;
    private int mRotationCenterId;
    private int mSpringBoundary;
    private float mSpringDamping;
    private float mSpringMass;
    private float mSpringStiffness;
    private float mSpringStopThreshold;
    private int mTouchAnchorId;
    private int mTouchAnchorSide;
    private int mTouchRegionId;
    
    public OnSwipe() {
        this.mDragDirection = 0;
        this.mTouchAnchorSide = 0;
        this.mTouchAnchorId = -1;
        this.mTouchRegionId = -1;
        this.mLimitBoundsTo = -1;
        this.mOnTouchUp = 0;
        this.mRotationCenterId = -1;
        this.mMaxVelocity = 4.0f;
        this.mMaxAcceleration = 1.2f;
        this.mMoveWhenScrollAtTop = true;
        this.mDragScale = 1.0f;
        this.mFlags = 0;
        this.mDragThreshold = 10.0f;
        this.mSpringDamping = Float.NaN;
        this.mSpringMass = 1.0f;
        this.mSpringStiffness = Float.NaN;
        this.mSpringStopThreshold = Float.NaN;
        this.mSpringBoundary = 0;
        this.mAutoCompleteMode = 0;
    }
    
    public int getAutoCompleteMode() {
        return this.mAutoCompleteMode;
    }
    
    public int getDragDirection() {
        return this.mDragDirection;
    }
    
    public float getDragScale() {
        return this.mDragScale;
    }
    
    public float getDragThreshold() {
        return this.mDragThreshold;
    }
    
    public int getLimitBoundsTo() {
        return this.mLimitBoundsTo;
    }
    
    public float getMaxAcceleration() {
        return this.mMaxAcceleration;
    }
    
    public float getMaxVelocity() {
        return this.mMaxVelocity;
    }
    
    public boolean getMoveWhenScrollAtTop() {
        return this.mMoveWhenScrollAtTop;
    }
    
    public int getNestedScrollFlags() {
        return this.mFlags;
    }
    
    public int getOnTouchUp() {
        return this.mOnTouchUp;
    }
    
    public int getRotationCenterId() {
        return this.mRotationCenterId;
    }
    
    public int getSpringBoundary() {
        return this.mSpringBoundary;
    }
    
    public float getSpringDamping() {
        return this.mSpringDamping;
    }
    
    public float getSpringMass() {
        return this.mSpringMass;
    }
    
    public float getSpringStiffness() {
        return this.mSpringStiffness;
    }
    
    public float getSpringStopThreshold() {
        return this.mSpringStopThreshold;
    }
    
    public int getTouchAnchorId() {
        return this.mTouchAnchorId;
    }
    
    public int getTouchAnchorSide() {
        return this.mTouchAnchorSide;
    }
    
    public int getTouchRegionId() {
        return this.mTouchRegionId;
    }
    
    public void setAutoCompleteMode(final int mAutoCompleteMode) {
        this.mAutoCompleteMode = mAutoCompleteMode;
    }
    
    public OnSwipe setDragDirection(final int mDragDirection) {
        this.mDragDirection = mDragDirection;
        return this;
    }
    
    public OnSwipe setDragScale(final int n) {
        this.mDragScale = (float)n;
        return this;
    }
    
    public OnSwipe setDragThreshold(final int n) {
        this.mDragThreshold = (float)n;
        return this;
    }
    
    public OnSwipe setLimitBoundsTo(final int mLimitBoundsTo) {
        this.mLimitBoundsTo = mLimitBoundsTo;
        return this;
    }
    
    public OnSwipe setMaxAcceleration(final int n) {
        this.mMaxAcceleration = (float)n;
        return this;
    }
    
    public OnSwipe setMaxVelocity(final int n) {
        this.mMaxVelocity = (float)n;
        return this;
    }
    
    public OnSwipe setMoveWhenScrollAtTop(final boolean mMoveWhenScrollAtTop) {
        this.mMoveWhenScrollAtTop = mMoveWhenScrollAtTop;
        return this;
    }
    
    public OnSwipe setNestedScrollFlags(final int mFlags) {
        this.mFlags = mFlags;
        return this;
    }
    
    public OnSwipe setOnTouchUp(final int mOnTouchUp) {
        this.mOnTouchUp = mOnTouchUp;
        return this;
    }
    
    public OnSwipe setRotateCenter(final int mRotationCenterId) {
        this.mRotationCenterId = mRotationCenterId;
        return this;
    }
    
    public OnSwipe setSpringBoundary(final int mSpringBoundary) {
        this.mSpringBoundary = mSpringBoundary;
        return this;
    }
    
    public OnSwipe setSpringDamping(final float mSpringDamping) {
        this.mSpringDamping = mSpringDamping;
        return this;
    }
    
    public OnSwipe setSpringMass(final float mSpringMass) {
        this.mSpringMass = mSpringMass;
        return this;
    }
    
    public OnSwipe setSpringStiffness(final float mSpringStiffness) {
        this.mSpringStiffness = mSpringStiffness;
        return this;
    }
    
    public OnSwipe setSpringStopThreshold(final float mSpringStopThreshold) {
        this.mSpringStopThreshold = mSpringStopThreshold;
        return this;
    }
    
    public OnSwipe setTouchAnchorId(final int mTouchAnchorId) {
        this.mTouchAnchorId = mTouchAnchorId;
        return this;
    }
    
    public OnSwipe setTouchAnchorSide(final int mTouchAnchorSide) {
        this.mTouchAnchorSide = mTouchAnchorSide;
        return this;
    }
    
    public OnSwipe setTouchRegionId(final int mTouchRegionId) {
        this.mTouchRegionId = mTouchRegionId;
        return this;
    }
}

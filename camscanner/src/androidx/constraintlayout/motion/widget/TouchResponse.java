// 
// Decompiled by Procyon v0.6.0
// 

package androidx.constraintlayout.motion.widget;

import android.view.View$OnTouchListener;
import androidx.core.widget.NestedScrollView;
import android.view.MotionEvent;
import android.view.View;
import android.graphics.RectF;
import android.view.ViewGroup;
import android.util.AttributeSet;
import androidx.constraintlayout.widget.R;
import android.content.res.TypedArray;
import android.util.Xml;
import org.xmlpull.v1.XmlPullParser;
import android.content.Context;

class TouchResponse
{
    public static final int COMPLETE_MODE_CONTINUOUS_VELOCITY = 0;
    public static final int COMPLETE_MODE_SPRING = 1;
    private static final boolean DEBUG = false;
    private static final float EPSILON = 1.0E-7f;
    static final int FLAG_DISABLE_POST_SCROLL = 1;
    static final int FLAG_DISABLE_SCROLL = 2;
    static final int FLAG_SUPPORT_SCROLL_UP = 4;
    private static final int SEC_TO_MILLISECONDS = 1000;
    private static final int SIDE_BOTTOM = 3;
    private static final int SIDE_END = 6;
    private static final int SIDE_LEFT = 1;
    private static final int SIDE_MIDDLE = 4;
    private static final int SIDE_RIGHT = 2;
    private static final int SIDE_START = 5;
    private static final int SIDE_TOP = 0;
    private static final String TAG = "TouchResponse";
    private static final float[][] TOUCH_DIRECTION;
    private static final int TOUCH_DOWN = 1;
    private static final int TOUCH_END = 5;
    private static final int TOUCH_LEFT = 2;
    private static final int TOUCH_RIGHT = 3;
    private static final float[][] TOUCH_SIDES;
    private static final int TOUCH_START = 4;
    private static final int TOUCH_UP = 0;
    private float[] mAnchorDpDt;
    private int mAutoCompleteMode;
    private float mDragScale;
    private boolean mDragStarted;
    private float mDragThreshold;
    private int mFlags;
    boolean mIsRotateMode;
    private float mLastTouchX;
    private float mLastTouchY;
    private int mLimitBoundsTo;
    private float mMaxAcceleration;
    private float mMaxVelocity;
    private final MotionLayout mMotionLayout;
    private boolean mMoveWhenScrollAtTop;
    private int mOnTouchUp;
    float mRotateCenterX;
    float mRotateCenterY;
    private int mRotationCenterId;
    private int mSpringBoundary;
    private float mSpringDamping;
    private float mSpringMass;
    private float mSpringStiffness;
    private float mSpringStopThreshold;
    private int[] mTempLoc;
    private int mTouchAnchorId;
    private int mTouchAnchorSide;
    private float mTouchAnchorX;
    private float mTouchAnchorY;
    private float mTouchDirectionX;
    private float mTouchDirectionY;
    private int mTouchRegionId;
    private int mTouchSide;
    
    static {
        TOUCH_SIDES = new float[][] { { 0.5f, 0.0f }, { 0.0f, 0.5f }, { 1.0f, 0.5f }, { 0.5f, 1.0f }, { 0.5f, 0.5f }, { 0.0f, 0.5f }, { 1.0f, 0.5f } };
        TOUCH_DIRECTION = new float[][] { { 0.0f, -1.0f }, { 0.0f, 1.0f }, { -1.0f, 0.0f }, { 1.0f, 0.0f }, { -1.0f, 0.0f }, { 1.0f, 0.0f } };
    }
    
    TouchResponse(final Context context, final MotionLayout mMotionLayout, final XmlPullParser xmlPullParser) {
        this.mTouchAnchorSide = 0;
        this.mTouchSide = 0;
        this.mOnTouchUp = 0;
        this.mTouchAnchorId = -1;
        this.mTouchRegionId = -1;
        this.mLimitBoundsTo = -1;
        this.mTouchAnchorY = 0.5f;
        this.mTouchAnchorX = 0.5f;
        this.mRotateCenterX = 0.5f;
        this.mRotateCenterY = 0.5f;
        this.mRotationCenterId = -1;
        this.mIsRotateMode = false;
        this.mTouchDirectionX = 0.0f;
        this.mTouchDirectionY = 1.0f;
        this.mDragStarted = false;
        this.mAnchorDpDt = new float[2];
        this.mTempLoc = new int[2];
        this.mMaxVelocity = 4.0f;
        this.mMaxAcceleration = 1.2f;
        this.mMoveWhenScrollAtTop = true;
        this.mDragScale = 1.0f;
        this.mFlags = 0;
        this.mDragThreshold = 10.0f;
        this.mSpringDamping = 10.0f;
        this.mSpringMass = 1.0f;
        this.mSpringStiffness = Float.NaN;
        this.mSpringStopThreshold = Float.NaN;
        this.mSpringBoundary = 0;
        this.mAutoCompleteMode = 0;
        this.mMotionLayout = mMotionLayout;
        this.fillFromAttributeList(context, Xml.asAttributeSet(xmlPullParser));
    }
    
    public TouchResponse(final MotionLayout mMotionLayout, final OnSwipe onSwipe) {
        this.mTouchAnchorSide = 0;
        this.mTouchSide = 0;
        this.mOnTouchUp = 0;
        this.mTouchAnchorId = -1;
        this.mTouchRegionId = -1;
        this.mLimitBoundsTo = -1;
        this.mTouchAnchorY = 0.5f;
        this.mTouchAnchorX = 0.5f;
        this.mRotateCenterX = 0.5f;
        this.mRotateCenterY = 0.5f;
        this.mRotationCenterId = -1;
        this.mIsRotateMode = false;
        this.mTouchDirectionX = 0.0f;
        this.mTouchDirectionY = 1.0f;
        this.mDragStarted = false;
        this.mAnchorDpDt = new float[2];
        this.mTempLoc = new int[2];
        this.mMaxVelocity = 4.0f;
        this.mMaxAcceleration = 1.2f;
        this.mMoveWhenScrollAtTop = true;
        this.mDragScale = 1.0f;
        this.mFlags = 0;
        this.mDragThreshold = 10.0f;
        this.mSpringDamping = 10.0f;
        this.mSpringMass = 1.0f;
        this.mSpringStiffness = Float.NaN;
        this.mSpringStopThreshold = Float.NaN;
        this.mSpringBoundary = 0;
        this.mAutoCompleteMode = 0;
        this.mMotionLayout = mMotionLayout;
        this.mTouchAnchorId = onSwipe.getTouchAnchorId();
        final int touchAnchorSide = onSwipe.getTouchAnchorSide();
        this.mTouchAnchorSide = touchAnchorSide;
        if (touchAnchorSide != -1) {
            final float[] array = TouchResponse.TOUCH_SIDES[touchAnchorSide];
            this.mTouchAnchorX = array[0];
            this.mTouchAnchorY = array[1];
        }
        final int dragDirection = onSwipe.getDragDirection();
        this.mTouchSide = dragDirection;
        final float[][] touch_DIRECTION = TouchResponse.TOUCH_DIRECTION;
        if (dragDirection < touch_DIRECTION.length) {
            final float[] array2 = touch_DIRECTION[dragDirection];
            this.mTouchDirectionX = array2[0];
            this.mTouchDirectionY = array2[1];
        }
        else {
            this.mTouchDirectionY = Float.NaN;
            this.mTouchDirectionX = Float.NaN;
            this.mIsRotateMode = true;
        }
        this.mMaxVelocity = onSwipe.getMaxVelocity();
        this.mMaxAcceleration = onSwipe.getMaxAcceleration();
        this.mMoveWhenScrollAtTop = onSwipe.getMoveWhenScrollAtTop();
        this.mDragScale = onSwipe.getDragScale();
        this.mDragThreshold = onSwipe.getDragThreshold();
        this.mTouchRegionId = onSwipe.getTouchRegionId();
        this.mOnTouchUp = onSwipe.getOnTouchUp();
        this.mFlags = onSwipe.getNestedScrollFlags();
        this.mLimitBoundsTo = onSwipe.getLimitBoundsTo();
        this.mRotationCenterId = onSwipe.getRotationCenterId();
        this.mSpringBoundary = onSwipe.getSpringBoundary();
        this.mSpringDamping = onSwipe.getSpringDamping();
        this.mSpringMass = onSwipe.getSpringMass();
        this.mSpringStiffness = onSwipe.getSpringStiffness();
        this.mSpringStopThreshold = onSwipe.getSpringStopThreshold();
        this.mAutoCompleteMode = onSwipe.getAutoCompleteMode();
    }
    
    private void fill(final TypedArray typedArray) {
        for (int indexCount = typedArray.getIndexCount(), i = 0; i < indexCount; ++i) {
            final int index = typedArray.getIndex(i);
            if (index == R.styleable.OnSwipe_touchAnchorId) {
                this.mTouchAnchorId = typedArray.getResourceId(index, this.mTouchAnchorId);
            }
            else if (index == R.styleable.OnSwipe_touchAnchorSide) {
                final int int1 = typedArray.getInt(index, this.mTouchAnchorSide);
                this.mTouchAnchorSide = int1;
                final float[] array = TouchResponse.TOUCH_SIDES[int1];
                this.mTouchAnchorX = array[0];
                this.mTouchAnchorY = array[1];
            }
            else if (index == R.styleable.OnSwipe_dragDirection) {
                final int int2 = typedArray.getInt(index, this.mTouchSide);
                this.mTouchSide = int2;
                final float[][] touch_DIRECTION = TouchResponse.TOUCH_DIRECTION;
                if (int2 < touch_DIRECTION.length) {
                    final float[] array2 = touch_DIRECTION[int2];
                    this.mTouchDirectionX = array2[0];
                    this.mTouchDirectionY = array2[1];
                }
                else {
                    this.mTouchDirectionY = Float.NaN;
                    this.mTouchDirectionX = Float.NaN;
                    this.mIsRotateMode = true;
                }
            }
            else if (index == R.styleable.OnSwipe_maxVelocity) {
                this.mMaxVelocity = typedArray.getFloat(index, this.mMaxVelocity);
            }
            else if (index == R.styleable.OnSwipe_maxAcceleration) {
                this.mMaxAcceleration = typedArray.getFloat(index, this.mMaxAcceleration);
            }
            else if (index == R.styleable.OnSwipe_moveWhenScrollAtTop) {
                this.mMoveWhenScrollAtTop = typedArray.getBoolean(index, this.mMoveWhenScrollAtTop);
            }
            else if (index == R.styleable.OnSwipe_dragScale) {
                this.mDragScale = typedArray.getFloat(index, this.mDragScale);
            }
            else if (index == R.styleable.OnSwipe_dragThreshold) {
                this.mDragThreshold = typedArray.getFloat(index, this.mDragThreshold);
            }
            else if (index == R.styleable.OnSwipe_touchRegionId) {
                this.mTouchRegionId = typedArray.getResourceId(index, this.mTouchRegionId);
            }
            else if (index == R.styleable.OnSwipe_onTouchUp) {
                this.mOnTouchUp = typedArray.getInt(index, this.mOnTouchUp);
            }
            else if (index == R.styleable.OnSwipe_nestedScrollFlags) {
                this.mFlags = typedArray.getInteger(index, 0);
            }
            else if (index == R.styleable.OnSwipe_limitBoundsTo) {
                this.mLimitBoundsTo = typedArray.getResourceId(index, 0);
            }
            else if (index == R.styleable.OnSwipe_rotationCenterId) {
                this.mRotationCenterId = typedArray.getResourceId(index, this.mRotationCenterId);
            }
            else if (index == R.styleable.OnSwipe_springDamping) {
                this.mSpringDamping = typedArray.getFloat(index, this.mSpringDamping);
            }
            else if (index == R.styleable.OnSwipe_springMass) {
                this.mSpringMass = typedArray.getFloat(index, this.mSpringMass);
            }
            else if (index == R.styleable.OnSwipe_springStiffness) {
                this.mSpringStiffness = typedArray.getFloat(index, this.mSpringStiffness);
            }
            else if (index == R.styleable.OnSwipe_springStopThreshold) {
                this.mSpringStopThreshold = typedArray.getFloat(index, this.mSpringStopThreshold);
            }
            else if (index == R.styleable.OnSwipe_springBoundary) {
                this.mSpringBoundary = typedArray.getInt(index, this.mSpringBoundary);
            }
            else if (index == R.styleable.OnSwipe_autoCompleteMode) {
                this.mAutoCompleteMode = typedArray.getInt(index, this.mAutoCompleteMode);
            }
        }
    }
    
    private void fillFromAttributeList(final Context context, final AttributeSet set) {
        final TypedArray obtainStyledAttributes = context.obtainStyledAttributes(set, R.styleable.OnSwipe);
        this.fill(obtainStyledAttributes);
        obtainStyledAttributes.recycle();
    }
    
    float dot(final float n, final float n2) {
        return n * this.mTouchDirectionX + n2 * this.mTouchDirectionY;
    }
    
    public int getAnchorId() {
        return this.mTouchAnchorId;
    }
    
    public int getAutoCompleteMode() {
        return this.mAutoCompleteMode;
    }
    
    public int getFlags() {
        return this.mFlags;
    }
    
    RectF getLimitBoundsTo(final ViewGroup viewGroup, final RectF rectF) {
        final int mLimitBoundsTo = this.mLimitBoundsTo;
        if (mLimitBoundsTo == -1) {
            return null;
        }
        final View viewById = ((View)viewGroup).findViewById(mLimitBoundsTo);
        if (viewById == null) {
            return null;
        }
        rectF.set((float)viewById.getLeft(), (float)viewById.getTop(), (float)viewById.getRight(), (float)viewById.getBottom());
        return rectF;
    }
    
    int getLimitBoundsToId() {
        return this.mLimitBoundsTo;
    }
    
    float getMaxAcceleration() {
        return this.mMaxAcceleration;
    }
    
    public float getMaxVelocity() {
        return this.mMaxVelocity;
    }
    
    boolean getMoveWhenScrollAtTop() {
        return this.mMoveWhenScrollAtTop;
    }
    
    float getProgressDirection(float n, final float n2) {
        this.mMotionLayout.getAnchorDpDt(this.mTouchAnchorId, this.mMotionLayout.getProgress(), this.mTouchAnchorX, this.mTouchAnchorY, this.mAnchorDpDt);
        final float mTouchDirectionX = this.mTouchDirectionX;
        if (mTouchDirectionX != 0.0f) {
            final float[] mAnchorDpDt = this.mAnchorDpDt;
            if (mAnchorDpDt[0] == 0.0f) {
                mAnchorDpDt[0] = 1.0E-7f;
            }
            n = n * mTouchDirectionX / mAnchorDpDt[0];
        }
        else {
            final float[] mAnchorDpDt2 = this.mAnchorDpDt;
            if (mAnchorDpDt2[1] == 0.0f) {
                mAnchorDpDt2[1] = 1.0E-7f;
            }
            n = n2 * this.mTouchDirectionY / mAnchorDpDt2[1];
        }
        return n;
    }
    
    public int getSpringBoundary() {
        return this.mSpringBoundary;
    }
    
    public float getSpringDamping() {
        return this.mSpringDamping;
    }
    
    public float getSpringMass() {
        return this.mSpringMass;
    }
    
    public float getSpringStiffness() {
        return this.mSpringStiffness;
    }
    
    public float getSpringStopThreshold() {
        return this.mSpringStopThreshold;
    }
    
    RectF getTouchRegion(final ViewGroup viewGroup, final RectF rectF) {
        final int mTouchRegionId = this.mTouchRegionId;
        if (mTouchRegionId == -1) {
            return null;
        }
        final View viewById = ((View)viewGroup).findViewById(mTouchRegionId);
        if (viewById == null) {
            return null;
        }
        rectF.set((float)viewById.getLeft(), (float)viewById.getTop(), (float)viewById.getRight(), (float)viewById.getBottom());
        return rectF;
    }
    
    int getTouchRegionId() {
        return this.mTouchRegionId;
    }
    
    void processTouchEvent(final MotionEvent motionEvent, final MotionLayout.MotionTracker motionTracker, int n, final MotionScene motionScene) {
        if (this.mIsRotateMode) {
            this.processTouchRotateEvent(motionEvent, motionTracker, n, motionScene);
            return;
        }
        motionTracker.addMovement(motionEvent);
        n = motionEvent.getAction();
        if (n != 0) {
            if (n != 1) {
                if (n == 2) {
                    final float n2 = motionEvent.getRawY() - this.mLastTouchY;
                    final float n3 = motionEvent.getRawX() - this.mLastTouchX;
                    if (Math.abs(this.mTouchDirectionX * n3 + this.mTouchDirectionY * n2) > this.mDragThreshold || this.mDragStarted) {
                        final float progress = this.mMotionLayout.getProgress();
                        if (!this.mDragStarted) {
                            this.mDragStarted = true;
                            this.mMotionLayout.setProgress(progress);
                        }
                        n = this.mTouchAnchorId;
                        if (n != -1) {
                            this.mMotionLayout.getAnchorDpDt(n, progress, this.mTouchAnchorX, this.mTouchAnchorY, this.mAnchorDpDt);
                        }
                        else {
                            final float n4 = (float)Math.min(((View)this.mMotionLayout).getWidth(), ((View)this.mMotionLayout).getHeight());
                            final float[] mAnchorDpDt = this.mAnchorDpDt;
                            mAnchorDpDt[1] = this.mTouchDirectionY * n4;
                            mAnchorDpDt[0] = n4 * this.mTouchDirectionX;
                        }
                        final float mTouchDirectionX = this.mTouchDirectionX;
                        final float[] mAnchorDpDt2 = this.mAnchorDpDt;
                        if (Math.abs((mTouchDirectionX * mAnchorDpDt2[0] + this.mTouchDirectionY * mAnchorDpDt2[1]) * this.mDragScale) < 0.01) {
                            final float[] mAnchorDpDt3 = this.mAnchorDpDt;
                            mAnchorDpDt3[1] = (mAnchorDpDt3[0] = 0.01f);
                        }
                        float n5;
                        if (this.mTouchDirectionX != 0.0f) {
                            n5 = n3 / this.mAnchorDpDt[0];
                        }
                        else {
                            n5 = n2 / this.mAnchorDpDt[1];
                        }
                        float a2;
                        final float a = a2 = Math.max(Math.min(progress + n5, 1.0f), 0.0f);
                        if (this.mOnTouchUp == 6) {
                            a2 = Math.max(a, 0.01f);
                        }
                        float min = a2;
                        if (this.mOnTouchUp == 7) {
                            min = Math.min(a2, 0.99f);
                        }
                        final float progress2 = this.mMotionLayout.getProgress();
                        if (min != progress2) {
                            n = fcmpl(progress2, 0.0f);
                            if (n == 0 || progress2 == 1.0f) {
                                this.mMotionLayout.endTrigger(n == 0);
                            }
                            this.mMotionLayout.setProgress(min);
                            motionTracker.computeCurrentVelocity(1000);
                            final float xVelocity = motionTracker.getXVelocity();
                            final float yVelocity = motionTracker.getYVelocity();
                            float mLastVelocity;
                            if (this.mTouchDirectionX != 0.0f) {
                                mLastVelocity = xVelocity / this.mAnchorDpDt[0];
                            }
                            else {
                                mLastVelocity = yVelocity / this.mAnchorDpDt[1];
                            }
                            this.mMotionLayout.mLastVelocity = mLastVelocity;
                        }
                        else {
                            this.mMotionLayout.mLastVelocity = 0.0f;
                        }
                        this.mLastTouchX = motionEvent.getRawX();
                        this.mLastTouchY = motionEvent.getRawY();
                    }
                }
            }
            else {
                this.mDragStarted = false;
                motionTracker.computeCurrentVelocity(1000);
                final float xVelocity2 = motionTracker.getXVelocity();
                final float yVelocity2 = motionTracker.getYVelocity();
                final float progress3 = this.mMotionLayout.getProgress();
                n = this.mTouchAnchorId;
                if (n != -1) {
                    this.mMotionLayout.getAnchorDpDt(n, progress3, this.mTouchAnchorX, this.mTouchAnchorY, this.mAnchorDpDt);
                }
                else {
                    final float n6 = (float)Math.min(((View)this.mMotionLayout).getWidth(), ((View)this.mMotionLayout).getHeight());
                    final float[] mAnchorDpDt4 = this.mAnchorDpDt;
                    mAnchorDpDt4[1] = this.mTouchDirectionY * n6;
                    mAnchorDpDt4[0] = n6 * this.mTouchDirectionX;
                }
                final float mTouchDirectionX2 = this.mTouchDirectionX;
                final float[] mAnchorDpDt5 = this.mAnchorDpDt;
                final float n7 = mAnchorDpDt5[0];
                final float n8 = mAnchorDpDt5[1];
                float n9;
                if (mTouchDirectionX2 != 0.0f) {
                    n9 = xVelocity2 / n7;
                }
                else {
                    n9 = yVelocity2 / n8;
                }
                float n10;
                if (!Float.isNaN(n9)) {
                    n10 = n9 / 3.0f + progress3;
                }
                else {
                    n10 = progress3;
                }
                if (n10 != 0.0f && n10 != 1.0f) {
                    n = this.mOnTouchUp;
                    if (n != 3) {
                        float n11;
                        if (n10 < 0.5) {
                            n11 = 0.0f;
                        }
                        else {
                            n11 = 1.0f;
                        }
                        float abs = n9;
                        if (n == 6) {
                            abs = n9;
                            if (progress3 + n9 < 0.0f) {
                                abs = Math.abs(n9);
                            }
                            n11 = 1.0f;
                        }
                        float n12 = abs;
                        if (this.mOnTouchUp == 7) {
                            n12 = abs;
                            if (progress3 + abs > 1.0f) {
                                n12 = -Math.abs(abs);
                            }
                            n11 = 0.0f;
                        }
                        this.mMotionLayout.touchAnimateTo(this.mOnTouchUp, n11, n12);
                        if (0.0f >= progress3 || 1.0f <= progress3) {
                            this.mMotionLayout.setState(MotionLayout.TransitionState.FINISHED);
                        }
                        return;
                    }
                }
                if (0.0f >= n10 || 1.0f <= n10) {
                    this.mMotionLayout.setState(MotionLayout.TransitionState.FINISHED);
                }
            }
        }
        else {
            this.mLastTouchX = motionEvent.getRawX();
            this.mLastTouchY = motionEvent.getRawY();
            this.mDragStarted = false;
        }
    }
    
    void processTouchRotateEvent(final MotionEvent motionEvent, final MotionLayout.MotionTracker motionTracker, int n, final MotionScene motionScene) {
        motionTracker.addMovement(motionEvent);
        n = motionEvent.getAction();
        boolean b = false;
        if (n != 0) {
            if (n != 1) {
                if (n == 2) {
                    motionEvent.getRawY();
                    motionEvent.getRawX();
                    final float n2 = ((View)this.mMotionLayout).getWidth() / 2.0f;
                    final float n3 = ((View)this.mMotionLayout).getHeight() / 2.0f;
                    n = this.mRotationCenterId;
                    float n6;
                    float n7;
                    if (n != -1) {
                        final View viewById = ((View)this.mMotionLayout).findViewById(n);
                        ((View)this.mMotionLayout).getLocationOnScreen(this.mTempLoc);
                        final float n4 = (float)this.mTempLoc[0];
                        final float n5 = (viewById.getLeft() + viewById.getRight()) / 2.0f;
                        n6 = (viewById.getTop() + viewById.getBottom()) / 2.0f + this.mTempLoc[1];
                        n7 = n4 + n5;
                    }
                    else {
                        n = this.mTouchAnchorId;
                        n7 = n2;
                        n6 = n3;
                        if (n != -1) {
                            final View viewById2 = ((View)this.mMotionLayout).findViewById(this.mMotionLayout.getMotionController(n).getAnimateRelativeTo());
                            if (viewById2 == null) {
                                n7 = n2;
                                n6 = n3;
                            }
                            else {
                                ((View)this.mMotionLayout).getLocationOnScreen(this.mTempLoc);
                                n7 = this.mTempLoc[0] + (viewById2.getLeft() + viewById2.getRight()) / 2.0f;
                                n6 = this.mTempLoc[1] + (viewById2.getTop() + viewById2.getBottom()) / 2.0f;
                            }
                        }
                    }
                    final float rawX = motionEvent.getRawX();
                    final float rawY = motionEvent.getRawY();
                    final double atan2 = Math.atan2(motionEvent.getRawY() - n6, motionEvent.getRawX() - n7);
                    final float n8 = (float)((atan2 - Math.atan2(this.mLastTouchY - n6, this.mLastTouchX - n7)) * 180.0 / 3.141592653589793);
                    float a;
                    if (n8 > 330.0f) {
                        a = n8 - 360.0f;
                    }
                    else {
                        a = n8;
                        if (n8 < -330.0f) {
                            a = n8 + 360.0f;
                        }
                    }
                    if (Math.abs(a) > 0.01 || this.mDragStarted) {
                        final float progress = this.mMotionLayout.getProgress();
                        if (!this.mDragStarted) {
                            this.mDragStarted = true;
                            this.mMotionLayout.setProgress(progress);
                        }
                        n = this.mTouchAnchorId;
                        if (n != -1) {
                            this.mMotionLayout.getAnchorDpDt(n, progress, this.mTouchAnchorX, this.mTouchAnchorY, this.mAnchorDpDt);
                            final float[] mAnchorDpDt = this.mAnchorDpDt;
                            mAnchorDpDt[1] = (float)Math.toDegrees(mAnchorDpDt[1]);
                        }
                        else {
                            this.mAnchorDpDt[1] = 360.0f;
                        }
                        final float max = Math.max(Math.min(progress + a * this.mDragScale / this.mAnchorDpDt[1], 1.0f), 0.0f);
                        final float progress2 = this.mMotionLayout.getProgress();
                        if (max != progress2) {
                            n = fcmpl(progress2, 0.0f);
                            if (n == 0 || progress2 == 1.0f) {
                                final MotionLayout mMotionLayout = this.mMotionLayout;
                                if (n == 0) {
                                    b = true;
                                }
                                mMotionLayout.endTrigger(b);
                            }
                            this.mMotionLayout.setProgress(max);
                            motionTracker.computeCurrentVelocity(1000);
                            final float xVelocity = motionTracker.getXVelocity();
                            final double n9 = motionTracker.getYVelocity();
                            final double n10 = xVelocity;
                            this.mMotionLayout.mLastVelocity = (float)Math.toDegrees((float)(Math.hypot(n9, n10) * Math.sin(Math.atan2(n9, n10) - atan2) / Math.hypot(rawX - n7, rawY - n6)));
                        }
                        else {
                            this.mMotionLayout.mLastVelocity = 0.0f;
                        }
                        this.mLastTouchX = motionEvent.getRawX();
                        this.mLastTouchY = motionEvent.getRawY();
                    }
                }
            }
            else {
                this.mDragStarted = false;
                motionTracker.computeCurrentVelocity(16);
                final float xVelocity2 = motionTracker.getXVelocity();
                final float yVelocity = motionTracker.getYVelocity();
                final float progress3 = this.mMotionLayout.getProgress();
                float n11 = ((View)this.mMotionLayout).getWidth() / 2.0f;
                float n12 = ((View)this.mMotionLayout).getHeight() / 2.0f;
                n = this.mRotationCenterId;
                Label_0937: {
                    float n13;
                    int n14;
                    if (n != -1) {
                        final View viewById3 = ((View)this.mMotionLayout).findViewById(n);
                        ((View)this.mMotionLayout).getLocationOnScreen(this.mTempLoc);
                        n11 = this.mTempLoc[0] + (viewById3.getLeft() + viewById3.getRight()) / 2.0f;
                        n13 = (float)this.mTempLoc[1];
                        n14 = viewById3.getTop();
                        n = viewById3.getBottom();
                    }
                    else {
                        n = this.mTouchAnchorId;
                        if (n == -1) {
                            break Label_0937;
                        }
                        final View viewById4 = ((View)this.mMotionLayout).findViewById(this.mMotionLayout.getMotionController(n).getAnimateRelativeTo());
                        ((View)this.mMotionLayout).getLocationOnScreen(this.mTempLoc);
                        n11 = this.mTempLoc[0] + (viewById4.getLeft() + viewById4.getRight()) / 2.0f;
                        n13 = (float)this.mTempLoc[1];
                        n14 = viewById4.getTop();
                        n = viewById4.getBottom();
                    }
                    n12 = n13 + (n14 + n) / 2.0f;
                }
                final float n15 = motionEvent.getRawX() - n11;
                final float n16 = motionEvent.getRawY() - n12;
                final double degrees = Math.toDegrees(Math.atan2(n16, n15));
                n = this.mTouchAnchorId;
                if (n != -1) {
                    this.mMotionLayout.getAnchorDpDt(n, progress3, this.mTouchAnchorX, this.mTouchAnchorY, this.mAnchorDpDt);
                    final float[] mAnchorDpDt2 = this.mAnchorDpDt;
                    mAnchorDpDt2[1] = (float)Math.toDegrees(mAnchorDpDt2[1]);
                }
                else {
                    this.mAnchorDpDt[1] = 360.0f;
                }
                final float v = (float)(Math.toDegrees(Math.atan2(yVelocity + n16, xVelocity2 + n15)) - degrees) * 62.5f;
                float n17;
                if (!Float.isNaN(v)) {
                    n17 = v * 3.0f * this.mDragScale / this.mAnchorDpDt[1] + progress3;
                }
                else {
                    n17 = progress3;
                }
                if (n17 != 0.0f && n17 != 1.0f) {
                    n = this.mOnTouchUp;
                    if (n != 3) {
                        final float a2 = v * this.mDragScale / this.mAnchorDpDt[1];
                        float n18;
                        if (n17 < 0.5) {
                            n18 = 0.0f;
                        }
                        else {
                            n18 = 1.0f;
                        }
                        float abs = a2;
                        if (n == 6) {
                            abs = a2;
                            if (progress3 + a2 < 0.0f) {
                                abs = Math.abs(a2);
                            }
                            n18 = 1.0f;
                        }
                        float n19 = abs;
                        float n20 = n18;
                        if (this.mOnTouchUp == 7) {
                            float n21 = abs;
                            if (progress3 + abs > 1.0f) {
                                n21 = -Math.abs(abs);
                            }
                            n20 = 0.0f;
                            n19 = n21;
                        }
                        this.mMotionLayout.touchAnimateTo(this.mOnTouchUp, n20, n19 * 3.0f);
                        if (0.0f >= progress3 || 1.0f <= progress3) {
                            this.mMotionLayout.setState(MotionLayout.TransitionState.FINISHED);
                        }
                        return;
                    }
                }
                if (0.0f >= n17 || 1.0f <= n17) {
                    this.mMotionLayout.setState(MotionLayout.TransitionState.FINISHED);
                }
            }
        }
        else {
            this.mLastTouchX = motionEvent.getRawX();
            this.mLastTouchY = motionEvent.getRawY();
            this.mDragStarted = false;
        }
    }
    
    void scrollMove(float max, final float n) {
        final float progress = this.mMotionLayout.getProgress();
        if (!this.mDragStarted) {
            this.mDragStarted = true;
            this.mMotionLayout.setProgress(progress);
        }
        this.mMotionLayout.getAnchorDpDt(this.mTouchAnchorId, progress, this.mTouchAnchorX, this.mTouchAnchorY, this.mAnchorDpDt);
        final float mTouchDirectionX = this.mTouchDirectionX;
        final float[] mAnchorDpDt = this.mAnchorDpDt;
        if (Math.abs(mTouchDirectionX * mAnchorDpDt[0] + this.mTouchDirectionY * mAnchorDpDt[1]) < 0.01) {
            final float[] mAnchorDpDt2 = this.mAnchorDpDt;
            mAnchorDpDt2[1] = (mAnchorDpDt2[0] = 0.01f);
        }
        final float mTouchDirectionX2 = this.mTouchDirectionX;
        if (mTouchDirectionX2 != 0.0f) {
            max = max * mTouchDirectionX2 / this.mAnchorDpDt[0];
        }
        else {
            max = n * this.mTouchDirectionY / this.mAnchorDpDt[1];
        }
        max = Math.max(Math.min(progress + max, 1.0f), 0.0f);
        if (max != this.mMotionLayout.getProgress()) {
            this.mMotionLayout.setProgress(max);
        }
    }
    
    void scrollUp(float v, float n) {
        int n2 = 0;
        this.mDragStarted = false;
        final float progress = this.mMotionLayout.getProgress();
        this.mMotionLayout.getAnchorDpDt(this.mTouchAnchorId, progress, this.mTouchAnchorX, this.mTouchAnchorY, this.mAnchorDpDt);
        final float mTouchDirectionX = this.mTouchDirectionX;
        final float[] mAnchorDpDt = this.mAnchorDpDt;
        final float n3 = mAnchorDpDt[0];
        final float mTouchDirectionY = this.mTouchDirectionY;
        final float n4 = mAnchorDpDt[1];
        final float n5 = 0.0f;
        if (mTouchDirectionX != 0.0f) {
            v = v * mTouchDirectionX / n3;
        }
        else {
            v = n * mTouchDirectionY / n4;
        }
        n = progress;
        if (!Float.isNaN(v)) {
            n = progress + v / 3.0f;
        }
        if (n != 0.0f) {
            final boolean b = n != 1.0f;
            final int mOnTouchUp = this.mOnTouchUp;
            if (mOnTouchUp != 3) {
                n2 = 1;
            }
            if ((n2 & (b ? 1 : 0)) != 0x0) {
                final MotionLayout mMotionLayout = this.mMotionLayout;
                if (n < 0.5) {
                    n = n5;
                }
                else {
                    n = 1.0f;
                }
                mMotionLayout.touchAnimateTo(mOnTouchUp, n, v);
            }
        }
    }
    
    public void setAnchorId(final int mTouchAnchorId) {
        this.mTouchAnchorId = mTouchAnchorId;
    }
    
    void setAutoCompleteMode(final int mAutoCompleteMode) {
        this.mAutoCompleteMode = mAutoCompleteMode;
    }
    
    void setDown(final float mLastTouchX, final float mLastTouchY) {
        this.mLastTouchX = mLastTouchX;
        this.mLastTouchY = mLastTouchY;
    }
    
    public void setMaxAcceleration(final float mMaxAcceleration) {
        this.mMaxAcceleration = mMaxAcceleration;
    }
    
    public void setMaxVelocity(final float mMaxVelocity) {
        this.mMaxVelocity = mMaxVelocity;
    }
    
    public void setRTL(final boolean b) {
        if (b) {
            final float[][] touch_DIRECTION = TouchResponse.TOUCH_DIRECTION;
            touch_DIRECTION[4] = touch_DIRECTION[3];
            touch_DIRECTION[5] = touch_DIRECTION[2];
            final float[][] touch_SIDES = TouchResponse.TOUCH_SIDES;
            touch_SIDES[5] = touch_SIDES[2];
            touch_SIDES[6] = touch_SIDES[1];
        }
        else {
            final float[][] touch_DIRECTION2 = TouchResponse.TOUCH_DIRECTION;
            touch_DIRECTION2[4] = touch_DIRECTION2[2];
            touch_DIRECTION2[5] = touch_DIRECTION2[3];
            final float[][] touch_SIDES2 = TouchResponse.TOUCH_SIDES;
            touch_SIDES2[5] = touch_SIDES2[1];
            touch_SIDES2[6] = touch_SIDES2[2];
        }
        final float[] array = TouchResponse.TOUCH_SIDES[this.mTouchAnchorSide];
        this.mTouchAnchorX = array[0];
        this.mTouchAnchorY = array[1];
        final int mTouchSide = this.mTouchSide;
        final float[][] touch_DIRECTION3 = TouchResponse.TOUCH_DIRECTION;
        if (mTouchSide >= touch_DIRECTION3.length) {
            return;
        }
        final float[] array2 = touch_DIRECTION3[mTouchSide];
        this.mTouchDirectionX = array2[0];
        this.mTouchDirectionY = array2[1];
    }
    
    public void setTouchAnchorLocation(final float mTouchAnchorX, final float mTouchAnchorY) {
        this.mTouchAnchorX = mTouchAnchorX;
        this.mTouchAnchorY = mTouchAnchorY;
    }
    
    public void setTouchUpMode(final int mOnTouchUp) {
        this.mOnTouchUp = mOnTouchUp;
    }
    
    void setUpTouchEvent(final float mLastTouchX, final float mLastTouchY) {
        this.mLastTouchX = mLastTouchX;
        this.mLastTouchY = mLastTouchY;
        this.mDragStarted = false;
    }
    
    void setupTouch() {
        final int mTouchAnchorId = this.mTouchAnchorId;
        Object o;
        if (mTouchAnchorId != -1) {
            final View viewById = ((View)this.mMotionLayout).findViewById(mTouchAnchorId);
            if ((o = viewById) == null) {
                final StringBuilder sb = new StringBuilder();
                sb.append("cannot find TouchAnchorId @id/");
                sb.append(Debug.getName(((View)this.mMotionLayout).getContext(), this.mTouchAnchorId));
                o = viewById;
            }
        }
        else {
            o = null;
        }
        if (o instanceof NestedScrollView) {
            final NestedScrollView nestedScrollView = (NestedScrollView)o;
            ((View)nestedScrollView).setOnTouchListener((View$OnTouchListener)new View$OnTouchListener(this) {
                final TouchResponse this$0;
                
                public boolean onTouch(final View view, final MotionEvent motionEvent) {
                    return false;
                }
            });
            nestedScrollView.setOnScrollChangeListener((NestedScrollView.OnScrollChangeListener)new NestedScrollView.OnScrollChangeListener(this) {
                final TouchResponse this$0;
                
                @Override
                public void onScrollChange(final NestedScrollView nestedScrollView, final int n, final int n2, final int n3, final int n4) {
                }
            });
        }
    }
    
    @Override
    public String toString() {
        String string;
        if (Float.isNaN(this.mTouchDirectionX)) {
            string = "rotation";
        }
        else {
            final StringBuilder sb = new StringBuilder();
            sb.append(this.mTouchDirectionX);
            sb.append(" , ");
            sb.append(this.mTouchDirectionY);
            string = sb.toString();
        }
        return string;
    }
}

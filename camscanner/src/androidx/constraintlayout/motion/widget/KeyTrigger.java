// 
// Decompiled by Procyon v0.6.0
// 

package androidx.constraintlayout.motion.widget;

import android.content.res.TypedArray;
import android.util.SparseIntArray;
import androidx.constraintlayout.widget.R;
import android.util.AttributeSet;
import android.content.Context;
import java.util.HashSet;
import android.view.ViewGroup;
import androidx.constraintlayout.motion.utils.ViewSpline;
import java.util.Iterator;
import java.util.Locale;
import androidx.constraintlayout.widget.ConstraintAttribute;
import android.view.View;
import java.lang.reflect.Method;
import java.util.HashMap;
import android.graphics.RectF;

public class KeyTrigger extends Key
{
    public static final String CROSS = "CROSS";
    public static final int KEY_TYPE = 5;
    static final String NAME = "KeyTrigger";
    public static final String NEGATIVE_CROSS = "negativeCross";
    public static final String POSITIVE_CROSS = "positiveCross";
    public static final String POST_LAYOUT = "postLayout";
    private static final String TAG = "KeyTrigger";
    public static final String TRIGGER_COLLISION_ID = "triggerCollisionId";
    public static final String TRIGGER_COLLISION_VIEW = "triggerCollisionView";
    public static final String TRIGGER_ID = "triggerID";
    public static final String TRIGGER_RECEIVER = "triggerReceiver";
    public static final String TRIGGER_SLACK = "triggerSlack";
    public static final String VIEW_TRANSITION_ON_CROSS = "viewTransitionOnCross";
    public static final String VIEW_TRANSITION_ON_NEGATIVE_CROSS = "viewTransitionOnNegativeCross";
    public static final String VIEW_TRANSITION_ON_POSITIVE_CROSS = "viewTransitionOnPositiveCross";
    RectF mCollisionRect;
    private String mCross;
    private int mCurveFit;
    private boolean mFireCrossReset;
    private float mFireLastPos;
    private boolean mFireNegativeReset;
    private boolean mFirePositiveReset;
    private float mFireThreshold;
    HashMap<String, Method> mMethodHashMap;
    private String mNegativeCross;
    private String mPositiveCross;
    private boolean mPostLayout;
    RectF mTargetRect;
    private int mTriggerCollisionId;
    private View mTriggerCollisionView;
    private int mTriggerID;
    private int mTriggerReceiver;
    float mTriggerSlack;
    int mViewTransitionOnCross;
    int mViewTransitionOnNegativeCross;
    int mViewTransitionOnPositiveCross;
    
    public KeyTrigger() {
        this.mCurveFit = -1;
        this.mCross = null;
        final int unset = Key.UNSET;
        this.mTriggerReceiver = unset;
        this.mNegativeCross = null;
        this.mPositiveCross = null;
        this.mTriggerID = unset;
        this.mTriggerCollisionId = unset;
        this.mTriggerCollisionView = null;
        this.mTriggerSlack = 0.1f;
        this.mFireCrossReset = true;
        this.mFireNegativeReset = true;
        this.mFirePositiveReset = true;
        this.mFireThreshold = Float.NaN;
        this.mPostLayout = false;
        this.mViewTransitionOnNegativeCross = unset;
        this.mViewTransitionOnPositiveCross = unset;
        this.mViewTransitionOnCross = unset;
        this.mCollisionRect = new RectF();
        this.mTargetRect = new RectF();
        this.mMethodHashMap = new HashMap<String, Method>();
        super.mType = 5;
        super.mCustomConstraints = new HashMap<String, ConstraintAttribute>();
    }
    
    private void fire(final String s, final View obj) {
        if (s == null) {
            return;
        }
        if (s.startsWith(".")) {
            this.fireCustom(s, obj);
            return;
        }
        Method method;
        if (this.mMethodHashMap.containsKey(s)) {
            if ((method = this.mMethodHashMap.get(s)) == null) {
                return;
            }
        }
        else {
            method = null;
        }
        Method method2;
        if ((method2 = method) == null) {
            try {
                method2 = obj.getClass().getMethod(s, (Class<?>[])new Class[0]);
                this.mMethodHashMap.put(s, method2);
            }
            catch (final NoSuchMethodException ex) {
                this.mMethodHashMap.put(s, null);
                final StringBuilder sb = new StringBuilder();
                sb.append("Could not find method \"");
                sb.append(s);
                sb.append("\"on class ");
                sb.append(obj.getClass().getSimpleName());
                sb.append(" ");
                sb.append(Debug.getName(obj));
                return;
            }
        }
        try {
            method2.invoke(obj, new Object[0]);
        }
        catch (final Exception ex2) {
            final StringBuilder sb2 = new StringBuilder();
            sb2.append("Exception in call \"");
            sb2.append(this.mCross);
            sb2.append("\"on class ");
            sb2.append(obj.getClass().getSimpleName());
            sb2.append(" ");
            sb2.append(Debug.getName(obj));
        }
    }
    
    private void fireCustom(final String s, final View view) {
        final boolean b = s.length() == 1;
        String lowerCase = s;
        if (!b) {
            lowerCase = s.substring(1).toLowerCase(Locale.ROOT);
        }
        for (final String key : super.mCustomConstraints.keySet()) {
            final String lowerCase2 = key.toLowerCase(Locale.ROOT);
            if (b || lowerCase2.matches(lowerCase)) {
                final ConstraintAttribute constraintAttribute = super.mCustomConstraints.get(key);
                if (constraintAttribute == null) {
                    continue;
                }
                constraintAttribute.applyCustom(view);
            }
        }
    }
    
    private void setUpRect(final RectF rectF, final View view, final boolean b) {
        rectF.top = (float)view.getTop();
        rectF.bottom = (float)view.getBottom();
        rectF.left = (float)view.getLeft();
        rectF.right = (float)view.getRight();
        if (b) {
            view.getMatrix().mapRect(rectF);
        }
    }
    
    @Override
    public void addValues(final HashMap<String, ViewSpline> hashMap) {
    }
    
    @Override
    public Key clone() {
        return new KeyTrigger().copy(this);
    }
    
    public void conditionallyFire(final float mFireLastPos, final View view) {
        boolean b = false;
        boolean b2 = false;
        boolean b3 = false;
        Label_0398: {
            if (this.mTriggerCollisionId != Key.UNSET) {
                if (this.mTriggerCollisionView == null) {
                    this.mTriggerCollisionView = ((View)view.getParent()).findViewById(this.mTriggerCollisionId);
                }
                this.setUpRect(this.mCollisionRect, this.mTriggerCollisionView, this.mPostLayout);
                this.setUpRect(this.mTargetRect, view, this.mPostLayout);
                if (this.mCollisionRect.intersect(this.mTargetRect)) {
                    if (this.mFireCrossReset) {
                        this.mFireCrossReset = false;
                        b = true;
                    }
                    else {
                        b = false;
                    }
                    if (this.mFirePositiveReset) {
                        this.mFirePositiveReset = false;
                        b2 = true;
                    }
                    else {
                        b2 = false;
                    }
                    this.mFireNegativeReset = true;
                    b3 = false;
                    break Label_0398;
                }
                if (!this.mFireCrossReset) {
                    this.mFireCrossReset = true;
                    b = true;
                }
                else {
                    b = false;
                }
                if (this.mFireNegativeReset) {
                    this.mFireNegativeReset = false;
                    b3 = true;
                }
                else {
                    b3 = false;
                }
                this.mFirePositiveReset = true;
            }
            else {
                Label_0244: {
                    if (this.mFireCrossReset) {
                        final float mFireThreshold = this.mFireThreshold;
                        if ((mFireLastPos - mFireThreshold) * (this.mFireLastPos - mFireThreshold) < 0.0f) {
                            this.mFireCrossReset = false;
                            b = true;
                            break Label_0244;
                        }
                    }
                    else if (Math.abs(mFireLastPos - this.mFireThreshold) > this.mTriggerSlack) {
                        this.mFireCrossReset = true;
                    }
                    b = false;
                }
                Label_0318: {
                    if (this.mFireNegativeReset) {
                        final float mFireThreshold2 = this.mFireThreshold;
                        final float n = mFireLastPos - mFireThreshold2;
                        if ((this.mFireLastPos - mFireThreshold2) * n < 0.0f && n < 0.0f) {
                            this.mFireNegativeReset = false;
                            b3 = true;
                            break Label_0318;
                        }
                    }
                    else if (Math.abs(mFireLastPos - this.mFireThreshold) > this.mTriggerSlack) {
                        this.mFireNegativeReset = true;
                    }
                    b3 = false;
                }
                if (this.mFirePositiveReset) {
                    final float mFireThreshold3 = this.mFireThreshold;
                    final float n2 = mFireLastPos - mFireThreshold3;
                    if ((this.mFireLastPos - mFireThreshold3) * n2 < 0.0f && n2 > 0.0f) {
                        this.mFirePositiveReset = false;
                        b2 = true;
                    }
                    else {
                        b2 = false;
                    }
                    break Label_0398;
                }
                if (Math.abs(mFireLastPos - this.mFireThreshold) > this.mTriggerSlack) {
                    this.mFirePositiveReset = true;
                }
            }
            b2 = false;
        }
        this.mFireLastPos = mFireLastPos;
        if (b3 || b || b2) {
            ((MotionLayout)view.getParent()).fireTrigger(this.mTriggerID, b2, mFireLastPos);
        }
        View viewById;
        if (this.mTriggerReceiver == Key.UNSET) {
            viewById = view;
        }
        else {
            viewById = ((View)view.getParent()).findViewById(this.mTriggerReceiver);
        }
        if (b3) {
            final String mNegativeCross = this.mNegativeCross;
            if (mNegativeCross != null) {
                this.fire(mNegativeCross, viewById);
            }
            if (this.mViewTransitionOnNegativeCross != Key.UNSET) {
                ((MotionLayout)view.getParent()).viewTransition(this.mViewTransitionOnNegativeCross, viewById);
            }
        }
        if (b2) {
            final String mPositiveCross = this.mPositiveCross;
            if (mPositiveCross != null) {
                this.fire(mPositiveCross, viewById);
            }
            if (this.mViewTransitionOnPositiveCross != Key.UNSET) {
                ((MotionLayout)view.getParent()).viewTransition(this.mViewTransitionOnPositiveCross, viewById);
            }
        }
        if (b) {
            final String mCross = this.mCross;
            if (mCross != null) {
                this.fire(mCross, viewById);
            }
            if (this.mViewTransitionOnCross != Key.UNSET) {
                ((MotionLayout)view.getParent()).viewTransition(this.mViewTransitionOnCross, viewById);
            }
        }
    }
    
    @Override
    public Key copy(final Key key) {
        super.copy(key);
        final KeyTrigger keyTrigger = (KeyTrigger)key;
        this.mCurveFit = keyTrigger.mCurveFit;
        this.mCross = keyTrigger.mCross;
        this.mTriggerReceiver = keyTrigger.mTriggerReceiver;
        this.mNegativeCross = keyTrigger.mNegativeCross;
        this.mPositiveCross = keyTrigger.mPositiveCross;
        this.mTriggerID = keyTrigger.mTriggerID;
        this.mTriggerCollisionId = keyTrigger.mTriggerCollisionId;
        this.mTriggerCollisionView = keyTrigger.mTriggerCollisionView;
        this.mTriggerSlack = keyTrigger.mTriggerSlack;
        this.mFireCrossReset = keyTrigger.mFireCrossReset;
        this.mFireNegativeReset = keyTrigger.mFireNegativeReset;
        this.mFirePositiveReset = keyTrigger.mFirePositiveReset;
        this.mFireThreshold = keyTrigger.mFireThreshold;
        this.mFireLastPos = keyTrigger.mFireLastPos;
        this.mPostLayout = keyTrigger.mPostLayout;
        this.mCollisionRect = keyTrigger.mCollisionRect;
        this.mTargetRect = keyTrigger.mTargetRect;
        this.mMethodHashMap = keyTrigger.mMethodHashMap;
        return this;
    }
    
    public void getAttributeNames(final HashSet<String> set) {
    }
    
    int getCurveFit() {
        return this.mCurveFit;
    }
    
    public void load(final Context context, final AttributeSet set) {
        Loader.read(this, context.obtainStyledAttributes(set, R.styleable.KeyTrigger), context);
    }
    
    @Override
    public void setValue(final String s, final Object o) {
        s.hashCode();
        final int hashCode = s.hashCode();
        int n = -1;
        switch (hashCode) {
            case 1535404999: {
                if (!s.equals("triggerReceiver")) {
                    break;
                }
                n = 11;
                break;
            }
            case 1401391082: {
                if (!s.equals("postLayout")) {
                    break;
                }
                n = 10;
                break;
            }
            case 1301930599: {
                if (!s.equals("viewTransitionOnCross")) {
                    break;
                }
                n = 9;
                break;
            }
            case 364489912: {
                if (!s.equals("triggerSlack")) {
                    break;
                }
                n = 8;
                break;
            }
            case 64397344: {
                if (!s.equals("CROSS")) {
                    break;
                }
                n = 7;
                break;
            }
            case -9754574: {
                if (!s.equals("viewTransitionOnNegativeCross")) {
                    break;
                }
                n = 6;
                break;
            }
            case -76025313: {
                if (!s.equals("triggerCollisionView")) {
                    break;
                }
                n = 5;
                break;
            }
            case -638126837: {
                if (!s.equals("negativeCross")) {
                    break;
                }
                n = 4;
                break;
            }
            case -648752941: {
                if (!s.equals("triggerID")) {
                    break;
                }
                n = 3;
                break;
            }
            case -786670827: {
                if (!s.equals("triggerCollisionId")) {
                    break;
                }
                n = 2;
                break;
            }
            case -966421266: {
                if (!s.equals("viewTransitionOnPositiveCross")) {
                    break;
                }
                n = 1;
                break;
            }
            case -1594793529: {
                if (!s.equals("positiveCross")) {
                    break;
                }
                n = 0;
                break;
            }
        }
        switch (n) {
            case 11: {
                this.mTriggerReceiver = this.toInt(o);
                break;
            }
            case 10: {
                this.mPostLayout = this.toBoolean(o);
                break;
            }
            case 9: {
                this.mViewTransitionOnCross = this.toInt(o);
                break;
            }
            case 8: {
                this.mTriggerSlack = this.toFloat(o);
                break;
            }
            case 7: {
                this.mCross = o.toString();
                break;
            }
            case 6: {
                this.mViewTransitionOnNegativeCross = this.toInt(o);
                break;
            }
            case 5: {
                this.mTriggerCollisionView = (View)o;
                break;
            }
            case 4: {
                this.mNegativeCross = o.toString();
                break;
            }
            case 3: {
                this.mTriggerID = this.toInt(o);
                break;
            }
            case 2: {
                this.mTriggerCollisionId = this.toInt(o);
                break;
            }
            case 1: {
                this.mViewTransitionOnPositiveCross = this.toInt(o);
                break;
            }
            case 0: {
                this.mPositiveCross = o.toString();
                break;
            }
        }
    }
    
    private static class Loader
    {
        private static final int COLLISION = 9;
        private static final int CROSS = 4;
        private static final int FRAME_POS = 8;
        private static final int NEGATIVE_CROSS = 1;
        private static final int POSITIVE_CROSS = 2;
        private static final int POST_LAYOUT = 10;
        private static final int TARGET_ID = 7;
        private static final int TRIGGER_ID = 6;
        private static final int TRIGGER_RECEIVER = 11;
        private static final int TRIGGER_SLACK = 5;
        private static final int VT_CROSS = 12;
        private static final int VT_NEGATIVE_CROSS = 13;
        private static final int VT_POSITIVE_CROSS = 14;
        private static SparseIntArray mAttrMap;
        
        static {
            (Loader.mAttrMap = new SparseIntArray()).append(R.styleable.KeyTrigger_framePosition, 8);
            Loader.mAttrMap.append(R.styleable.KeyTrigger_onCross, 4);
            Loader.mAttrMap.append(R.styleable.KeyTrigger_onNegativeCross, 1);
            Loader.mAttrMap.append(R.styleable.KeyTrigger_onPositiveCross, 2);
            Loader.mAttrMap.append(R.styleable.KeyTrigger_motionTarget, 7);
            Loader.mAttrMap.append(R.styleable.KeyTrigger_triggerId, 6);
            Loader.mAttrMap.append(R.styleable.KeyTrigger_triggerSlack, 5);
            Loader.mAttrMap.append(R.styleable.KeyTrigger_motion_triggerOnCollision, 9);
            Loader.mAttrMap.append(R.styleable.KeyTrigger_motion_postLayoutCollision, 10);
            Loader.mAttrMap.append(R.styleable.KeyTrigger_triggerReceiver, 11);
            Loader.mAttrMap.append(R.styleable.KeyTrigger_viewTransitionOnCross, 12);
            Loader.mAttrMap.append(R.styleable.KeyTrigger_viewTransitionOnNegativeCross, 13);
            Loader.mAttrMap.append(R.styleable.KeyTrigger_viewTransitionOnPositiveCross, 14);
        }
        
        public static void read(final KeyTrigger keyTrigger, final TypedArray typedArray, final Context context) {
            for (int indexCount = typedArray.getIndexCount(), i = 0; i < indexCount; ++i) {
                final int index = typedArray.getIndex(i);
                switch (Loader.mAttrMap.get(index)) {
                    default: {
                        final StringBuilder sb = new StringBuilder();
                        sb.append("unused attribute 0x");
                        sb.append(Integer.toHexString(index));
                        sb.append("   ");
                        sb.append(Loader.mAttrMap.get(index));
                        break;
                    }
                    case 14: {
                        keyTrigger.mViewTransitionOnPositiveCross = typedArray.getResourceId(index, keyTrigger.mViewTransitionOnPositiveCross);
                        break;
                    }
                    case 13: {
                        keyTrigger.mViewTransitionOnNegativeCross = typedArray.getResourceId(index, keyTrigger.mViewTransitionOnNegativeCross);
                        break;
                    }
                    case 12: {
                        keyTrigger.mViewTransitionOnCross = typedArray.getResourceId(index, keyTrigger.mViewTransitionOnCross);
                        break;
                    }
                    case 11: {
                        keyTrigger.mTriggerReceiver = typedArray.getResourceId(index, keyTrigger.mTriggerReceiver);
                        break;
                    }
                    case 10: {
                        keyTrigger.mPostLayout = typedArray.getBoolean(index, keyTrigger.mPostLayout);
                        break;
                    }
                    case 9: {
                        keyTrigger.mTriggerCollisionId = typedArray.getResourceId(index, keyTrigger.mTriggerCollisionId);
                        break;
                    }
                    case 8: {
                        final int integer = typedArray.getInteger(index, keyTrigger.mFramePosition);
                        keyTrigger.mFramePosition = integer;
                        keyTrigger.mFireThreshold = (integer + 0.5f) / 100.0f;
                        break;
                    }
                    case 7: {
                        if (MotionLayout.IS_IN_EDIT_MODE) {
                            if ((keyTrigger.mTargetId = typedArray.getResourceId(index, keyTrigger.mTargetId)) == -1) {
                                keyTrigger.mTargetString = typedArray.getString(index);
                                break;
                            }
                            break;
                        }
                        else {
                            if (typedArray.peekValue(index).type == 3) {
                                keyTrigger.mTargetString = typedArray.getString(index);
                                break;
                            }
                            keyTrigger.mTargetId = typedArray.getResourceId(index, keyTrigger.mTargetId);
                            break;
                        }
                        break;
                    }
                    case 6: {
                        keyTrigger.mTriggerID = typedArray.getResourceId(index, keyTrigger.mTriggerID);
                        break;
                    }
                    case 5: {
                        keyTrigger.mTriggerSlack = typedArray.getFloat(index, keyTrigger.mTriggerSlack);
                        break;
                    }
                    case 4: {
                        keyTrigger.mCross = typedArray.getString(index);
                        break;
                    }
                    case 2: {
                        keyTrigger.mPositiveCross = typedArray.getString(index);
                        break;
                    }
                    case 1: {
                        keyTrigger.mNegativeCross = typedArray.getString(index);
                        break;
                    }
                }
            }
        }
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.constraintlayout.motion.widget;

import android.content.res.TypedArray;
import android.util.SparseIntArray;
import androidx.constraintlayout.widget.R;
import android.util.AttributeSet;
import android.content.Context;
import java.util.HashSet;
import androidx.constraintlayout.core.motion.utils.SplineSet;
import androidx.constraintlayout.motion.utils.ViewSpline;
import java.util.Iterator;
import androidx.constraintlayout.motion.utils.ViewOscillator;
import androidx.constraintlayout.widget.ConstraintAttribute;
import java.util.HashMap;

public class KeyCycle extends Key
{
    public static final int KEY_TYPE = 4;
    static final String NAME = "KeyCycle";
    public static final int SHAPE_BOUNCE = 6;
    public static final int SHAPE_COS_WAVE = 5;
    public static final int SHAPE_REVERSE_SAW_WAVE = 4;
    public static final int SHAPE_SAW_WAVE = 3;
    public static final int SHAPE_SIN_WAVE = 0;
    public static final int SHAPE_SQUARE_WAVE = 1;
    public static final int SHAPE_TRIANGLE_WAVE = 2;
    private static final String TAG = "KeyCycle";
    public static final String WAVE_OFFSET = "waveOffset";
    public static final String WAVE_PERIOD = "wavePeriod";
    public static final String WAVE_PHASE = "wavePhase";
    public static final String WAVE_SHAPE = "waveShape";
    private float mAlpha;
    private int mCurveFit;
    private String mCustomWaveShape;
    private float mElevation;
    private float mProgress;
    private float mRotation;
    private float mRotationX;
    private float mRotationY;
    private float mScaleX;
    private float mScaleY;
    private String mTransitionEasing;
    private float mTransitionPathRotate;
    private float mTranslationX;
    private float mTranslationY;
    private float mTranslationZ;
    private float mWaveOffset;
    private float mWavePeriod;
    private float mWavePhase;
    private int mWaveShape;
    private int mWaveVariesBy;
    
    public KeyCycle() {
        this.mTransitionEasing = null;
        this.mCurveFit = 0;
        this.mWaveShape = -1;
        this.mCustomWaveShape = null;
        this.mWavePeriod = Float.NaN;
        this.mWaveOffset = 0.0f;
        this.mWavePhase = 0.0f;
        this.mProgress = Float.NaN;
        this.mWaveVariesBy = -1;
        this.mAlpha = Float.NaN;
        this.mElevation = Float.NaN;
        this.mRotation = Float.NaN;
        this.mTransitionPathRotate = Float.NaN;
        this.mRotationX = Float.NaN;
        this.mRotationY = Float.NaN;
        this.mScaleX = Float.NaN;
        this.mScaleY = Float.NaN;
        this.mTranslationX = Float.NaN;
        this.mTranslationY = Float.NaN;
        this.mTranslationZ = Float.NaN;
        super.mType = 4;
        super.mCustomConstraints = new HashMap<String, ConstraintAttribute>();
    }
    
    public void addCycleValues(final HashMap<String, ViewOscillator> hashMap) {
        for (final String s : hashMap.keySet()) {
            if (s.startsWith("CUSTOM")) {
                final ConstraintAttribute constraintAttribute = super.mCustomConstraints.get(s.substring(7));
                if (constraintAttribute == null) {
                    continue;
                }
                if (constraintAttribute.getType() != ConstraintAttribute.AttributeType.FLOAT_TYPE) {
                    continue;
                }
                final ViewOscillator viewOscillator = hashMap.get(s);
                if (viewOscillator == null) {
                    continue;
                }
                viewOscillator.setPoint(super.mFramePosition, this.mWaveShape, this.mCustomWaveShape, this.mWaveVariesBy, this.mWavePeriod, this.mWaveOffset, this.mWavePhase, constraintAttribute.getValueToInterpolate(), constraintAttribute);
            }
            else {
                final float value = this.getValue(s);
                if (Float.isNaN(value)) {
                    continue;
                }
                final ViewOscillator viewOscillator2 = hashMap.get(s);
                if (viewOscillator2 == null) {
                    continue;
                }
                viewOscillator2.setPoint(super.mFramePosition, this.mWaveShape, this.mCustomWaveShape, this.mWaveVariesBy, this.mWavePeriod, this.mWaveOffset, this.mWavePhase, value);
            }
        }
    }
    
    @Override
    public void addValues(final HashMap<String, ViewSpline> hashMap) {
        final StringBuilder sb = new StringBuilder();
        sb.append("add ");
        sb.append(hashMap.size());
        sb.append(" values");
        Debug.logStack("KeyCycle", sb.toString(), 2);
        for (final String s : hashMap.keySet()) {
            final SplineSet set = hashMap.get(s);
            if (set == null) {
                continue;
            }
            s.hashCode();
            final int hashCode = s.hashCode();
            int n = -1;
            switch (hashCode) {
                case 1530034690: {
                    if (!s.equals("wavePhase")) {
                        break;
                    }
                    n = 13;
                    break;
                }
                case 156108012: {
                    if (!s.equals("waveOffset")) {
                        break;
                    }
                    n = 12;
                    break;
                }
                case 92909918: {
                    if (!s.equals("alpha")) {
                        break;
                    }
                    n = 11;
                    break;
                }
                case 37232917: {
                    if (!s.equals("transitionPathRotate")) {
                        break;
                    }
                    n = 10;
                    break;
                }
                case -4379043: {
                    if (!s.equals("elevation")) {
                        break;
                    }
                    n = 9;
                    break;
                }
                case -40300674: {
                    if (!s.equals("rotation")) {
                        break;
                    }
                    n = 8;
                    break;
                }
                case -908189617: {
                    if (!s.equals("scaleY")) {
                        break;
                    }
                    n = 7;
                    break;
                }
                case -908189618: {
                    if (!s.equals("scaleX")) {
                        break;
                    }
                    n = 6;
                    break;
                }
                case -1001078227: {
                    if (!s.equals("progress")) {
                        break;
                    }
                    n = 5;
                    break;
                }
                case -1225497655: {
                    if (!s.equals("translationZ")) {
                        break;
                    }
                    n = 4;
                    break;
                }
                case -1225497656: {
                    if (!s.equals("translationY")) {
                        break;
                    }
                    n = 3;
                    break;
                }
                case -1225497657: {
                    if (!s.equals("translationX")) {
                        break;
                    }
                    n = 2;
                    break;
                }
                case -1249320805: {
                    if (!s.equals("rotationY")) {
                        break;
                    }
                    n = 1;
                    break;
                }
                case -1249320806: {
                    if (!s.equals("rotationX")) {
                        break;
                    }
                    n = 0;
                    break;
                }
            }
            switch (n) {
                default: {
                    if (!s.startsWith("CUSTOM")) {
                        final StringBuilder sb2 = new StringBuilder();
                        sb2.append("  UNKNOWN  ");
                        sb2.append(s);
                        continue;
                    }
                    continue;
                }
                case 13: {
                    set.setPoint(super.mFramePosition, this.mWavePhase);
                    continue;
                }
                case 12: {
                    set.setPoint(super.mFramePosition, this.mWaveOffset);
                    continue;
                }
                case 11: {
                    set.setPoint(super.mFramePosition, this.mAlpha);
                    continue;
                }
                case 10: {
                    set.setPoint(super.mFramePosition, this.mTransitionPathRotate);
                    continue;
                }
                case 9: {
                    set.setPoint(super.mFramePosition, this.mElevation);
                    continue;
                }
                case 8: {
                    set.setPoint(super.mFramePosition, this.mRotation);
                    continue;
                }
                case 7: {
                    set.setPoint(super.mFramePosition, this.mScaleY);
                    continue;
                }
                case 6: {
                    set.setPoint(super.mFramePosition, this.mScaleX);
                    continue;
                }
                case 5: {
                    set.setPoint(super.mFramePosition, this.mProgress);
                    continue;
                }
                case 4: {
                    set.setPoint(super.mFramePosition, this.mTranslationZ);
                    continue;
                }
                case 3: {
                    set.setPoint(super.mFramePosition, this.mTranslationY);
                    continue;
                }
                case 2: {
                    set.setPoint(super.mFramePosition, this.mTranslationX);
                    continue;
                }
                case 1: {
                    set.setPoint(super.mFramePosition, this.mRotationY);
                    continue;
                }
                case 0: {
                    set.setPoint(super.mFramePosition, this.mRotationX);
                    continue;
                }
            }
        }
    }
    
    @Override
    public Key clone() {
        return new KeyCycle().copy(this);
    }
    
    @Override
    public Key copy(final Key key) {
        super.copy(key);
        final KeyCycle keyCycle = (KeyCycle)key;
        this.mTransitionEasing = keyCycle.mTransitionEasing;
        this.mCurveFit = keyCycle.mCurveFit;
        this.mWaveShape = keyCycle.mWaveShape;
        this.mCustomWaveShape = keyCycle.mCustomWaveShape;
        this.mWavePeriod = keyCycle.mWavePeriod;
        this.mWaveOffset = keyCycle.mWaveOffset;
        this.mWavePhase = keyCycle.mWavePhase;
        this.mProgress = keyCycle.mProgress;
        this.mWaveVariesBy = keyCycle.mWaveVariesBy;
        this.mAlpha = keyCycle.mAlpha;
        this.mElevation = keyCycle.mElevation;
        this.mRotation = keyCycle.mRotation;
        this.mTransitionPathRotate = keyCycle.mTransitionPathRotate;
        this.mRotationX = keyCycle.mRotationX;
        this.mRotationY = keyCycle.mRotationY;
        this.mScaleX = keyCycle.mScaleX;
        this.mScaleY = keyCycle.mScaleY;
        this.mTranslationX = keyCycle.mTranslationX;
        this.mTranslationY = keyCycle.mTranslationY;
        this.mTranslationZ = keyCycle.mTranslationZ;
        return this;
    }
    
    public void getAttributeNames(final HashSet<String> set) {
        if (!Float.isNaN(this.mAlpha)) {
            set.add("alpha");
        }
        if (!Float.isNaN(this.mElevation)) {
            set.add("elevation");
        }
        if (!Float.isNaN(this.mRotation)) {
            set.add("rotation");
        }
        if (!Float.isNaN(this.mRotationX)) {
            set.add("rotationX");
        }
        if (!Float.isNaN(this.mRotationY)) {
            set.add("rotationY");
        }
        if (!Float.isNaN(this.mScaleX)) {
            set.add("scaleX");
        }
        if (!Float.isNaN(this.mScaleY)) {
            set.add("scaleY");
        }
        if (!Float.isNaN(this.mTransitionPathRotate)) {
            set.add("transitionPathRotate");
        }
        if (!Float.isNaN(this.mTranslationX)) {
            set.add("translationX");
        }
        if (!Float.isNaN(this.mTranslationY)) {
            set.add("translationY");
        }
        if (!Float.isNaN(this.mTranslationZ)) {
            set.add("translationZ");
        }
        if (super.mCustomConstraints.size() > 0) {
            for (final String str : super.mCustomConstraints.keySet()) {
                final StringBuilder sb = new StringBuilder();
                sb.append("CUSTOM,");
                sb.append(str);
                set.add(sb.toString());
            }
        }
    }
    
    public float getValue(final String str) {
        str.hashCode();
        final int hashCode = str.hashCode();
        int n = -1;
        switch (hashCode) {
            case 1530034690: {
                if (!str.equals("wavePhase")) {
                    break;
                }
                n = 13;
                break;
            }
            case 156108012: {
                if (!str.equals("waveOffset")) {
                    break;
                }
                n = 12;
                break;
            }
            case 92909918: {
                if (!str.equals("alpha")) {
                    break;
                }
                n = 11;
                break;
            }
            case 37232917: {
                if (!str.equals("transitionPathRotate")) {
                    break;
                }
                n = 10;
                break;
            }
            case -4379043: {
                if (!str.equals("elevation")) {
                    break;
                }
                n = 9;
                break;
            }
            case -40300674: {
                if (!str.equals("rotation")) {
                    break;
                }
                n = 8;
                break;
            }
            case -908189617: {
                if (!str.equals("scaleY")) {
                    break;
                }
                n = 7;
                break;
            }
            case -908189618: {
                if (!str.equals("scaleX")) {
                    break;
                }
                n = 6;
                break;
            }
            case -1001078227: {
                if (!str.equals("progress")) {
                    break;
                }
                n = 5;
                break;
            }
            case -1225497655: {
                if (!str.equals("translationZ")) {
                    break;
                }
                n = 4;
                break;
            }
            case -1225497656: {
                if (!str.equals("translationY")) {
                    break;
                }
                n = 3;
                break;
            }
            case -1225497657: {
                if (!str.equals("translationX")) {
                    break;
                }
                n = 2;
                break;
            }
            case -1249320805: {
                if (!str.equals("rotationY")) {
                    break;
                }
                n = 1;
                break;
            }
            case -1249320806: {
                if (!str.equals("rotationX")) {
                    break;
                }
                n = 0;
                break;
            }
        }
        switch (n) {
            default: {
                if (!str.startsWith("CUSTOM")) {
                    final StringBuilder sb = new StringBuilder();
                    sb.append("  UNKNOWN  ");
                    sb.append(str);
                }
                return Float.NaN;
            }
            case 13: {
                return this.mWavePhase;
            }
            case 12: {
                return this.mWaveOffset;
            }
            case 11: {
                return this.mAlpha;
            }
            case 10: {
                return this.mTransitionPathRotate;
            }
            case 9: {
                return this.mElevation;
            }
            case 8: {
                return this.mRotation;
            }
            case 7: {
                return this.mScaleY;
            }
            case 6: {
                return this.mScaleX;
            }
            case 5: {
                return this.mProgress;
            }
            case 4: {
                return this.mTranslationZ;
            }
            case 3: {
                return this.mTranslationY;
            }
            case 2: {
                return this.mTranslationX;
            }
            case 1: {
                return this.mRotationY;
            }
            case 0: {
                return this.mRotationX;
            }
        }
    }
    
    public void load(final Context context, final AttributeSet set) {
        read(this, context.obtainStyledAttributes(set, R.styleable.KeyCycle));
    }
    
    @Override
    public void setValue(final String s, final Object o) {
        s.hashCode();
        final int hashCode = s.hashCode();
        int n = -1;
        switch (hashCode) {
            case 1532805160: {
                if (!s.equals("waveShape")) {
                    break;
                }
                n = 17;
                break;
            }
            case 1530034690: {
                if (!s.equals("wavePhase")) {
                    break;
                }
                n = 16;
                break;
            }
            case 579057826: {
                if (!s.equals("curveFit")) {
                    break;
                }
                n = 15;
                break;
            }
            case 184161818: {
                if (!s.equals("wavePeriod")) {
                    break;
                }
                n = 14;
                break;
            }
            case 156108012: {
                if (!s.equals("waveOffset")) {
                    break;
                }
                n = 13;
                break;
            }
            case 92909918: {
                if (!s.equals("alpha")) {
                    break;
                }
                n = 12;
                break;
            }
            case 37232917: {
                if (!s.equals("transitionPathRotate")) {
                    break;
                }
                n = 11;
                break;
            }
            case -4379043: {
                if (!s.equals("elevation")) {
                    break;
                }
                n = 10;
                break;
            }
            case -40300674: {
                if (!s.equals("rotation")) {
                    break;
                }
                n = 9;
                break;
            }
            case -908189617: {
                if (!s.equals("scaleY")) {
                    break;
                }
                n = 8;
                break;
            }
            case -908189618: {
                if (!s.equals("scaleX")) {
                    break;
                }
                n = 7;
                break;
            }
            case -1225497655: {
                if (!s.equals("translationZ")) {
                    break;
                }
                n = 6;
                break;
            }
            case -1225497656: {
                if (!s.equals("translationY")) {
                    break;
                }
                n = 5;
                break;
            }
            case -1225497657: {
                if (!s.equals("translationX")) {
                    break;
                }
                n = 4;
                break;
            }
            case -1249320805: {
                if (!s.equals("rotationY")) {
                    break;
                }
                n = 3;
                break;
            }
            case -1249320806: {
                if (!s.equals("rotationX")) {
                    break;
                }
                n = 2;
                break;
            }
            case -1812823328: {
                if (!s.equals("transitionEasing")) {
                    break;
                }
                n = 1;
                break;
            }
            case -1913008125: {
                if (!s.equals("motionProgress")) {
                    break;
                }
                n = 0;
                break;
            }
        }
        switch (n) {
            case 17: {
                if (o instanceof Integer) {
                    this.mWaveShape = this.toInt(o);
                    break;
                }
                this.mWaveShape = 7;
                this.mCustomWaveShape = o.toString();
                break;
            }
            case 16: {
                this.mWavePhase = this.toFloat(o);
                break;
            }
            case 15: {
                this.mCurveFit = this.toInt(o);
                break;
            }
            case 14: {
                this.mWavePeriod = this.toFloat(o);
                break;
            }
            case 13: {
                this.mWaveOffset = this.toFloat(o);
                break;
            }
            case 12: {
                this.mAlpha = this.toFloat(o);
                break;
            }
            case 11: {
                this.mTransitionPathRotate = this.toFloat(o);
                break;
            }
            case 10: {
                this.mElevation = this.toFloat(o);
                break;
            }
            case 9: {
                this.mRotation = this.toFloat(o);
                break;
            }
            case 8: {
                this.mScaleY = this.toFloat(o);
                break;
            }
            case 7: {
                this.mScaleX = this.toFloat(o);
                break;
            }
            case 6: {
                this.mTranslationZ = this.toFloat(o);
                break;
            }
            case 5: {
                this.mTranslationY = this.toFloat(o);
                break;
            }
            case 4: {
                this.mTranslationX = this.toFloat(o);
                break;
            }
            case 3: {
                this.mRotationY = this.toFloat(o);
                break;
            }
            case 2: {
                this.mRotationX = this.toFloat(o);
                break;
            }
            case 1: {
                this.mTransitionEasing = o.toString();
                break;
            }
            case 0: {
                this.mProgress = this.toFloat(o);
                break;
            }
        }
    }
    
    private static class Loader
    {
        private static final int ANDROID_ALPHA = 9;
        private static final int ANDROID_ELEVATION = 10;
        private static final int ANDROID_ROTATION = 11;
        private static final int ANDROID_ROTATION_X = 12;
        private static final int ANDROID_ROTATION_Y = 13;
        private static final int ANDROID_SCALE_X = 15;
        private static final int ANDROID_SCALE_Y = 16;
        private static final int ANDROID_TRANSLATION_X = 17;
        private static final int ANDROID_TRANSLATION_Y = 18;
        private static final int ANDROID_TRANSLATION_Z = 19;
        private static final int CURVE_FIT = 4;
        private static final int FRAME_POSITION = 2;
        private static final int PROGRESS = 20;
        private static final int TARGET_ID = 1;
        private static final int TRANSITION_EASING = 3;
        private static final int TRANSITION_PATH_ROTATE = 14;
        private static final int WAVE_OFFSET = 7;
        private static final int WAVE_PERIOD = 6;
        private static final int WAVE_PHASE = 21;
        private static final int WAVE_SHAPE = 5;
        private static final int WAVE_VARIES_BY = 8;
        private static SparseIntArray mAttrMap;
        
        static {
            (Loader.mAttrMap = new SparseIntArray()).append(R.styleable.KeyCycle_motionTarget, 1);
            Loader.mAttrMap.append(R.styleable.KeyCycle_framePosition, 2);
            Loader.mAttrMap.append(R.styleable.KeyCycle_transitionEasing, 3);
            Loader.mAttrMap.append(R.styleable.KeyCycle_curveFit, 4);
            Loader.mAttrMap.append(R.styleable.KeyCycle_waveShape, 5);
            Loader.mAttrMap.append(R.styleable.KeyCycle_wavePeriod, 6);
            Loader.mAttrMap.append(R.styleable.KeyCycle_waveOffset, 7);
            Loader.mAttrMap.append(R.styleable.KeyCycle_waveVariesBy, 8);
            Loader.mAttrMap.append(R.styleable.KeyCycle_android_alpha, 9);
            Loader.mAttrMap.append(R.styleable.KeyCycle_android_elevation, 10);
            Loader.mAttrMap.append(R.styleable.KeyCycle_android_rotation, 11);
            Loader.mAttrMap.append(R.styleable.KeyCycle_android_rotationX, 12);
            Loader.mAttrMap.append(R.styleable.KeyCycle_android_rotationY, 13);
            Loader.mAttrMap.append(R.styleable.KeyCycle_transitionPathRotate, 14);
            Loader.mAttrMap.append(R.styleable.KeyCycle_android_scaleX, 15);
            Loader.mAttrMap.append(R.styleable.KeyCycle_android_scaleY, 16);
            Loader.mAttrMap.append(R.styleable.KeyCycle_android_translationX, 17);
            Loader.mAttrMap.append(R.styleable.KeyCycle_android_translationY, 18);
            Loader.mAttrMap.append(R.styleable.KeyCycle_android_translationZ, 19);
            Loader.mAttrMap.append(R.styleable.KeyCycle_motionProgress, 20);
            Loader.mAttrMap.append(R.styleable.KeyCycle_wavePhase, 21);
        }
        
        private static void read(final KeyCycle keyCycle, final TypedArray typedArray) {
            for (int indexCount = typedArray.getIndexCount(), i = 0; i < indexCount; ++i) {
                final int index = typedArray.getIndex(i);
                switch (Loader.mAttrMap.get(index)) {
                    default: {
                        final StringBuilder sb = new StringBuilder();
                        sb.append("unused attribute 0x");
                        sb.append(Integer.toHexString(index));
                        sb.append("   ");
                        sb.append(Loader.mAttrMap.get(index));
                        break;
                    }
                    case 21: {
                        keyCycle.mWavePhase = typedArray.getFloat(index, keyCycle.mWavePhase) / 360.0f;
                        break;
                    }
                    case 20: {
                        keyCycle.mProgress = typedArray.getFloat(index, keyCycle.mProgress);
                        break;
                    }
                    case 19: {
                        keyCycle.mTranslationZ = typedArray.getDimension(index, keyCycle.mTranslationZ);
                        break;
                    }
                    case 18: {
                        keyCycle.mTranslationY = typedArray.getDimension(index, keyCycle.mTranslationY);
                        break;
                    }
                    case 17: {
                        keyCycle.mTranslationX = typedArray.getDimension(index, keyCycle.mTranslationX);
                        break;
                    }
                    case 16: {
                        keyCycle.mScaleY = typedArray.getFloat(index, keyCycle.mScaleY);
                        break;
                    }
                    case 15: {
                        keyCycle.mScaleX = typedArray.getFloat(index, keyCycle.mScaleX);
                        break;
                    }
                    case 14: {
                        keyCycle.mTransitionPathRotate = typedArray.getFloat(index, keyCycle.mTransitionPathRotate);
                        break;
                    }
                    case 13: {
                        keyCycle.mRotationY = typedArray.getFloat(index, keyCycle.mRotationY);
                        break;
                    }
                    case 12: {
                        keyCycle.mRotationX = typedArray.getFloat(index, keyCycle.mRotationX);
                        break;
                    }
                    case 11: {
                        keyCycle.mRotation = typedArray.getFloat(index, keyCycle.mRotation);
                        break;
                    }
                    case 10: {
                        keyCycle.mElevation = typedArray.getDimension(index, keyCycle.mElevation);
                        break;
                    }
                    case 9: {
                        keyCycle.mAlpha = typedArray.getFloat(index, keyCycle.mAlpha);
                        break;
                    }
                    case 8: {
                        keyCycle.mWaveVariesBy = typedArray.getInt(index, keyCycle.mWaveVariesBy);
                        break;
                    }
                    case 7: {
                        if (typedArray.peekValue(index).type == 5) {
                            keyCycle.mWaveOffset = typedArray.getDimension(index, keyCycle.mWaveOffset);
                            break;
                        }
                        keyCycle.mWaveOffset = typedArray.getFloat(index, keyCycle.mWaveOffset);
                        break;
                    }
                    case 6: {
                        keyCycle.mWavePeriod = typedArray.getFloat(index, keyCycle.mWavePeriod);
                        break;
                    }
                    case 5: {
                        if (typedArray.peekValue(index).type == 3) {
                            keyCycle.mCustomWaveShape = typedArray.getString(index);
                            keyCycle.mWaveShape = 7;
                            break;
                        }
                        keyCycle.mWaveShape = typedArray.getInt(index, keyCycle.mWaveShape);
                        break;
                    }
                    case 4: {
                        keyCycle.mCurveFit = typedArray.getInteger(index, keyCycle.mCurveFit);
                        break;
                    }
                    case 3: {
                        keyCycle.mTransitionEasing = typedArray.getString(index);
                        break;
                    }
                    case 2: {
                        keyCycle.mFramePosition = typedArray.getInt(index, keyCycle.mFramePosition);
                        break;
                    }
                    case 1: {
                        if (MotionLayout.IS_IN_EDIT_MODE) {
                            if ((keyCycle.mTargetId = typedArray.getResourceId(index, keyCycle.mTargetId)) == -1) {
                                keyCycle.mTargetString = typedArray.getString(index);
                                break;
                            }
                            break;
                        }
                        else {
                            if (typedArray.peekValue(index).type == 3) {
                                keyCycle.mTargetString = typedArray.getString(index);
                                break;
                            }
                            keyCycle.mTargetId = typedArray.getResourceId(index, keyCycle.mTargetId);
                            break;
                        }
                        break;
                    }
                }
            }
        }
    }
}

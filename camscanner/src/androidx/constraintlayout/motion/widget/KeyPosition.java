// 
// Decompiled by Procyon v0.6.0
// 

package androidx.constraintlayout.motion.widget;

import androidx.constraintlayout.core.motion.utils.Easing;
import android.content.res.TypedArray;
import android.util.SparseIntArray;
import android.view.ViewGroup;
import android.view.View;
import androidx.constraintlayout.widget.R;
import android.util.AttributeSet;
import android.content.Context;
import android.graphics.RectF;
import androidx.constraintlayout.motion.utils.ViewSpline;
import java.util.HashMap;

public class KeyPosition extends KeyPositionBase
{
    public static final String DRAWPATH = "drawPath";
    static final int KEY_TYPE = 2;
    static final String NAME = "KeyPosition";
    public static final String PERCENT_HEIGHT = "percentHeight";
    public static final String PERCENT_WIDTH = "percentWidth";
    public static final String PERCENT_X = "percentX";
    public static final String PERCENT_Y = "percentY";
    public static final String SIZE_PERCENT = "sizePercent";
    private static final String TAG = "KeyPosition";
    public static final String TRANSITION_EASING = "transitionEasing";
    public static final int TYPE_CARTESIAN = 0;
    public static final int TYPE_PATH = 1;
    public static final int TYPE_SCREEN = 2;
    float mAltPercentX;
    float mAltPercentY;
    private float mCalculatedPositionX;
    private float mCalculatedPositionY;
    int mDrawPath;
    int mPathMotionArc;
    float mPercentHeight;
    float mPercentWidth;
    float mPercentX;
    float mPercentY;
    int mPositionType;
    String mTransitionEasing;
    
    public KeyPosition() {
        this.mTransitionEasing = null;
        this.mPathMotionArc = Key.UNSET;
        this.mDrawPath = 0;
        this.mPercentWidth = Float.NaN;
        this.mPercentHeight = Float.NaN;
        this.mPercentX = Float.NaN;
        this.mPercentY = Float.NaN;
        this.mAltPercentX = Float.NaN;
        this.mAltPercentY = Float.NaN;
        this.mPositionType = 0;
        this.mCalculatedPositionX = Float.NaN;
        this.mCalculatedPositionY = Float.NaN;
        super.mType = 2;
    }
    
    private void calcCartesianPosition(final float n, final float n2, float mPercentX, float mAltPercentY) {
        final float n3 = mPercentX - n;
        final float n4 = mAltPercentY - n2;
        final boolean naN = Float.isNaN(this.mPercentX);
        float mAltPercentX = 0.0f;
        if (naN) {
            mPercentX = 0.0f;
        }
        else {
            mPercentX = this.mPercentX;
        }
        if (Float.isNaN(this.mAltPercentY)) {
            mAltPercentY = 0.0f;
        }
        else {
            mAltPercentY = this.mAltPercentY;
        }
        float mPercentY;
        if (Float.isNaN(this.mPercentY)) {
            mPercentY = 0.0f;
        }
        else {
            mPercentY = this.mPercentY;
        }
        if (!Float.isNaN(this.mAltPercentX)) {
            mAltPercentX = this.mAltPercentX;
        }
        this.mCalculatedPositionX = (float)(int)(n + mPercentX * n3 + mAltPercentX * n4);
        this.mCalculatedPositionY = (float)(int)(n2 + n3 * mAltPercentY + n4 * mPercentY);
    }
    
    private void calcPathPosition(final float n, final float n2, float n3, float mPercentY) {
        n3 -= n;
        final float n4 = mPercentY - n2;
        final float n5 = -n4;
        final float mPercentX = this.mPercentX;
        mPercentY = this.mPercentY;
        this.mCalculatedPositionX = n + n3 * mPercentX + n5 * mPercentY;
        this.mCalculatedPositionY = n2 + n4 * mPercentX + n3 * mPercentY;
    }
    
    private void calcScreenPosition(final int n, final int n2) {
        final float n3 = (float)(n - 0);
        final float mPercentX = this.mPercentX;
        final float n4 = 0;
        this.mCalculatedPositionX = n3 * mPercentX + n4;
        this.mCalculatedPositionY = (n2 - 0) * mPercentX + n4;
    }
    
    @Override
    public void addValues(final HashMap<String, ViewSpline> hashMap) {
    }
    
    @Override
    void calcPosition(final int n, final int n2, final float n3, final float n4, final float n5, final float n6) {
        final int mPositionType = this.mPositionType;
        if (mPositionType == 1) {
            this.calcPathPosition(n3, n4, n5, n6);
            return;
        }
        if (mPositionType != 2) {
            this.calcCartesianPosition(n3, n4, n5, n6);
            return;
        }
        this.calcScreenPosition(n, n2);
    }
    
    @Override
    public Key clone() {
        return new KeyPosition().copy(this);
    }
    
    @Override
    public Key copy(final Key key) {
        super.copy(key);
        final KeyPosition keyPosition = (KeyPosition)key;
        this.mTransitionEasing = keyPosition.mTransitionEasing;
        this.mPathMotionArc = keyPosition.mPathMotionArc;
        this.mDrawPath = keyPosition.mDrawPath;
        this.mPercentWidth = keyPosition.mPercentWidth;
        this.mPercentHeight = Float.NaN;
        this.mPercentX = keyPosition.mPercentX;
        this.mPercentY = keyPosition.mPercentY;
        this.mAltPercentX = keyPosition.mAltPercentX;
        this.mAltPercentY = keyPosition.mAltPercentY;
        this.mCalculatedPositionX = keyPosition.mCalculatedPositionX;
        this.mCalculatedPositionY = keyPosition.mCalculatedPositionY;
        return this;
    }
    
    @Override
    float getPositionX() {
        return this.mCalculatedPositionX;
    }
    
    @Override
    float getPositionY() {
        return this.mCalculatedPositionY;
    }
    
    @Override
    public boolean intersects(final int n, final int n2, final RectF rectF, final RectF rectF2, final float n3, final float n4) {
        this.calcPosition(n, n2, rectF.centerX(), rectF.centerY(), rectF2.centerX(), rectF2.centerY());
        return Math.abs(n3 - this.mCalculatedPositionX) < 20.0f && Math.abs(n4 - this.mCalculatedPositionY) < 20.0f;
    }
    
    public void load(final Context context, final AttributeSet set) {
        read(this, context.obtainStyledAttributes(set, R.styleable.KeyPosition));
    }
    
    public void positionAttributes(final View view, final RectF rectF, final RectF rectF2, final float n, final float n2, final String[] array, final float[] array2) {
        final int mPositionType = this.mPositionType;
        if (mPositionType == 1) {
            this.positionPathAttributes(rectF, rectF2, n, n2, array, array2);
            return;
        }
        if (mPositionType != 2) {
            this.positionCartAttributes(rectF, rectF2, n, n2, array, array2);
            return;
        }
        this.positionScreenAttributes(view, rectF, rectF2, n, n2, array, array2);
    }
    
    void positionCartAttributes(final RectF rectF, final RectF rectF2, final float n, final float n2, final String[] array, final float[] array2) {
        final float centerX = rectF.centerX();
        final float centerY = rectF.centerY();
        final float centerX2 = rectF2.centerX();
        final float centerY2 = rectF2.centerY();
        final float n3 = centerX2 - centerX;
        final float n4 = centerY2 - centerY;
        final String anObject = array[0];
        if (anObject != null) {
            if ("percentX".equals(anObject)) {
                array2[0] = (n - centerX) / n3;
                array2[1] = (n2 - centerY) / n4;
            }
            else {
                array2[1] = (n - centerX) / n3;
                array2[0] = (n2 - centerY) / n4;
            }
        }
        else {
            array[0] = "percentX";
            array2[0] = (n - centerX) / n3;
            array[1] = "percentY";
            array2[1] = (n2 - centerY) / n4;
        }
    }
    
    void positionPathAttributes(final RectF rectF, final RectF rectF2, float n, float n2, final String[] array, final float[] array2) {
        final float centerX = rectF.centerX();
        final float centerY = rectF.centerY();
        final float centerX2 = rectF2.centerX();
        final float centerY2 = rectF2.centerY();
        final float n3 = centerX2 - centerX;
        final float n4 = centerY2 - centerY;
        final float n5 = (float)Math.hypot(n3, n4);
        if (n5 < 1.0E-4) {
            System.out.println("distance ~ 0");
            array2[1] = (array2[0] = 0.0f);
            return;
        }
        final float n6 = n3 / n5;
        final float n7 = n4 / n5;
        n2 -= centerY;
        final float n8 = n - centerX;
        n = (n6 * n2 - n8 * n7) / n5;
        n2 = (n6 * n8 + n7 * n2) / n5;
        final String anObject = array[0];
        if (anObject != null) {
            if ("percentX".equals(anObject)) {
                array2[0] = n2;
                array2[1] = n;
            }
        }
        else {
            array[0] = "percentX";
            array[1] = "percentY";
            array2[0] = n2;
            array2[1] = n;
        }
    }
    
    void positionScreenAttributes(final View view, final RectF rectF, final RectF rectF2, final float n, final float n2, final String[] array, final float[] array2) {
        rectF.centerX();
        rectF.centerY();
        rectF2.centerX();
        rectF2.centerY();
        final ViewGroup viewGroup = (ViewGroup)view.getParent();
        final int width = ((View)viewGroup).getWidth();
        final int height = ((View)viewGroup).getHeight();
        final String anObject = array[0];
        if (anObject != null) {
            if ("percentX".equals(anObject)) {
                array2[0] = n / width;
                array2[1] = n2 / height;
            }
            else {
                array2[1] = n / width;
                array2[0] = n2 / height;
            }
        }
        else {
            array[0] = "percentX";
            array2[0] = n / width;
            array[1] = "percentY";
            array2[1] = n2 / height;
        }
    }
    
    public void setType(final int mPositionType) {
        this.mPositionType = mPositionType;
    }
    
    @Override
    public void setValue(final String s, final Object o) {
        s.hashCode();
        final int hashCode = s.hashCode();
        int n = -1;
        switch (hashCode) {
            case 428090548: {
                if (!s.equals("percentY")) {
                    break;
                }
                n = 6;
                break;
            }
            case 428090547: {
                if (!s.equals("percentX")) {
                    break;
                }
                n = 5;
                break;
            }
            case -200259324: {
                if (!s.equals("sizePercent")) {
                    break;
                }
                n = 4;
                break;
            }
            case -827014263: {
                if (!s.equals("drawPath")) {
                    break;
                }
                n = 3;
                break;
            }
            case -1017587252: {
                if (!s.equals("percentHeight")) {
                    break;
                }
                n = 2;
                break;
            }
            case -1127236479: {
                if (!s.equals("percentWidth")) {
                    break;
                }
                n = 1;
                break;
            }
            case -1812823328: {
                if (!s.equals("transitionEasing")) {
                    break;
                }
                n = 0;
                break;
            }
        }
        switch (n) {
            case 6: {
                this.mPercentY = this.toFloat(o);
                break;
            }
            case 5: {
                this.mPercentX = this.toFloat(o);
                break;
            }
            case 4: {
                final float float1 = this.toFloat(o);
                this.mPercentWidth = float1;
                this.mPercentHeight = float1;
                break;
            }
            case 3: {
                this.mDrawPath = this.toInt(o);
                break;
            }
            case 2: {
                this.mPercentHeight = this.toFloat(o);
                break;
            }
            case 1: {
                this.mPercentWidth = this.toFloat(o);
                break;
            }
            case 0: {
                this.mTransitionEasing = o.toString();
                break;
            }
        }
    }
    
    private static class Loader
    {
        private static final int CURVE_FIT = 4;
        private static final int DRAW_PATH = 5;
        private static final int FRAME_POSITION = 2;
        private static final int PATH_MOTION_ARC = 10;
        private static final int PERCENT_HEIGHT = 12;
        private static final int PERCENT_WIDTH = 11;
        private static final int PERCENT_X = 6;
        private static final int PERCENT_Y = 7;
        private static final int SIZE_PERCENT = 8;
        private static final int TARGET_ID = 1;
        private static final int TRANSITION_EASING = 3;
        private static final int TYPE = 9;
        private static SparseIntArray mAttrMap;
        
        static {
            (Loader.mAttrMap = new SparseIntArray()).append(R.styleable.KeyPosition_motionTarget, 1);
            Loader.mAttrMap.append(R.styleable.KeyPosition_framePosition, 2);
            Loader.mAttrMap.append(R.styleable.KeyPosition_transitionEasing, 3);
            Loader.mAttrMap.append(R.styleable.KeyPosition_curveFit, 4);
            Loader.mAttrMap.append(R.styleable.KeyPosition_drawPath, 5);
            Loader.mAttrMap.append(R.styleable.KeyPosition_percentX, 6);
            Loader.mAttrMap.append(R.styleable.KeyPosition_percentY, 7);
            Loader.mAttrMap.append(R.styleable.KeyPosition_keyPositionType, 9);
            Loader.mAttrMap.append(R.styleable.KeyPosition_sizePercent, 8);
            Loader.mAttrMap.append(R.styleable.KeyPosition_percentWidth, 11);
            Loader.mAttrMap.append(R.styleable.KeyPosition_percentHeight, 12);
            Loader.mAttrMap.append(R.styleable.KeyPosition_pathMotionArc, 10);
        }
        
        private static void read(final KeyPosition keyPosition, final TypedArray typedArray) {
            for (int indexCount = typedArray.getIndexCount(), i = 0; i < indexCount; ++i) {
                final int index = typedArray.getIndex(i);
                switch (Loader.mAttrMap.get(index)) {
                    default: {
                        final StringBuilder sb = new StringBuilder();
                        sb.append("unused attribute 0x");
                        sb.append(Integer.toHexString(index));
                        sb.append("   ");
                        sb.append(Loader.mAttrMap.get(index));
                        break;
                    }
                    case 12: {
                        keyPosition.mPercentHeight = typedArray.getFloat(index, keyPosition.mPercentHeight);
                        break;
                    }
                    case 11: {
                        keyPosition.mPercentWidth = typedArray.getFloat(index, keyPosition.mPercentWidth);
                        break;
                    }
                    case 10: {
                        keyPosition.mPathMotionArc = typedArray.getInt(index, keyPosition.mPathMotionArc);
                        break;
                    }
                    case 9: {
                        keyPosition.mPositionType = typedArray.getInt(index, keyPosition.mPositionType);
                        break;
                    }
                    case 8: {
                        final float float1 = typedArray.getFloat(index, keyPosition.mPercentHeight);
                        keyPosition.mPercentWidth = float1;
                        keyPosition.mPercentHeight = float1;
                        break;
                    }
                    case 7: {
                        keyPosition.mPercentY = typedArray.getFloat(index, keyPosition.mPercentY);
                        break;
                    }
                    case 6: {
                        keyPosition.mPercentX = typedArray.getFloat(index, keyPosition.mPercentX);
                        break;
                    }
                    case 5: {
                        keyPosition.mDrawPath = typedArray.getInt(index, keyPosition.mDrawPath);
                        break;
                    }
                    case 4: {
                        keyPosition.mCurveFit = typedArray.getInteger(index, keyPosition.mCurveFit);
                        break;
                    }
                    case 3: {
                        if (typedArray.peekValue(index).type == 3) {
                            keyPosition.mTransitionEasing = typedArray.getString(index);
                            break;
                        }
                        keyPosition.mTransitionEasing = Easing.NAMED_EASING[typedArray.getInteger(index, 0)];
                        break;
                    }
                    case 2: {
                        keyPosition.mFramePosition = typedArray.getInt(index, keyPosition.mFramePosition);
                        break;
                    }
                    case 1: {
                        if (MotionLayout.IS_IN_EDIT_MODE) {
                            if ((keyPosition.mTargetId = typedArray.getResourceId(index, keyPosition.mTargetId)) == -1) {
                                keyPosition.mTargetString = typedArray.getString(index);
                                break;
                            }
                            break;
                        }
                        else {
                            if (typedArray.peekValue(index).type == 3) {
                                keyPosition.mTargetString = typedArray.getString(index);
                                break;
                            }
                            keyPosition.mTargetId = typedArray.getResourceId(index, keyPosition.mTargetId);
                            break;
                        }
                        break;
                    }
                }
            }
            final int mFramePosition = keyPosition.mFramePosition;
        }
    }
}

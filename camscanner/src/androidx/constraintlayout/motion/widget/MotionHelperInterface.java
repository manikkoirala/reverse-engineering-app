// 
// Decompiled by Procyon v0.6.0
// 

package androidx.constraintlayout.motion.widget;

import android.view.View;
import java.util.HashMap;
import android.graphics.Canvas;

public interface MotionHelperInterface extends Animatable, TransitionListener
{
    boolean isDecorator();
    
    boolean isUseOnHide();
    
    boolean isUsedOnShow();
    
    void onFinishedMotionScene(final MotionLayout p0);
    
    void onPostDraw(final Canvas p0);
    
    void onPreDraw(final Canvas p0);
    
    void onPreSetup(final MotionLayout p0, final HashMap<View, MotionController> p1);
}

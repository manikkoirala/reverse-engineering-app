// 
// Decompiled by Procyon v0.6.0
// 

package androidx.constraintlayout.motion.widget;

import android.view.View;
import android.graphics.RectF;
import java.util.HashSet;

abstract class KeyPositionBase extends Key
{
    protected static final float SELECTION_SLOPE = 20.0f;
    int mCurveFit;
    
    KeyPositionBase() {
        this.mCurveFit = Key.UNSET;
    }
    
    abstract void calcPosition(final int p0, final int p1, final float p2, final float p3, final float p4, final float p5);
    
    @Override
    void getAttributeNames(final HashSet<String> set) {
    }
    
    abstract float getPositionX();
    
    abstract float getPositionY();
    
    public abstract boolean intersects(final int p0, final int p1, final RectF p2, final RectF p3, final float p4, final float p5);
    
    abstract void positionAttributes(final View p0, final RectF p1, final RectF p2, final float p3, final float p4, final String[] p5, final float[] p6);
}

// 
// Decompiled by Procyon v0.6.0
// 

package androidx.constraintlayout.motion.widget;

import android.view.View$OnClickListener;
import android.util.AttributeSet;
import java.util.Map;
import android.view.View;
import android.view.animation.AnticipateInterpolator;
import android.view.animation.OvershootInterpolator;
import android.view.animation.BounceInterpolator;
import android.view.animation.DecelerateInterpolator;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.AccelerateDecelerateInterpolator;
import androidx.constraintlayout.core.motion.utils.Easing;
import android.view.animation.AnimationUtils;
import android.view.animation.Interpolator;
import java.util.List;
import android.view.ViewGroup;
import android.graphics.RectF;
import java.util.Iterator;
import androidx.constraintlayout.widget.ConstraintLayout;
import android.content.res.TypedArray;
import android.util.Xml;
import android.content.res.XmlResourceParser;
import org.xmlpull.v1.XmlPullParserException;
import java.io.IOException;
import org.xmlpull.v1.XmlPullParser;
import java.io.PrintStream;
import androidx.constraintlayout.widget.R;
import android.content.Context;
import androidx.constraintlayout.widget.StateSet;
import android.view.MotionEvent;
import android.util.SparseIntArray;
import androidx.constraintlayout.widget.ConstraintSet;
import android.util.SparseArray;
import java.util.HashMap;
import java.util.ArrayList;

public class MotionScene
{
    static final int ANTICIPATE = 6;
    static final int BOUNCE = 4;
    private static final String CONSTRAINTSET_TAG = "ConstraintSet";
    private static final boolean DEBUG = false;
    static final int EASE_IN = 1;
    static final int EASE_IN_OUT = 0;
    static final int EASE_OUT = 2;
    private static final String INCLUDE_TAG = "include";
    private static final String INCLUDE_TAG_UC = "Include";
    private static final int INTERPOLATOR_REFERENCE_ID = -2;
    private static final String KEYFRAMESET_TAG = "KeyFrameSet";
    public static final int LAYOUT_HONOR_REQUEST = 1;
    public static final int LAYOUT_IGNORE_REQUEST = 0;
    static final int LINEAR = 3;
    private static final int MIN_DURATION = 8;
    private static final String MOTIONSCENE_TAG = "MotionScene";
    private static final String ONCLICK_TAG = "OnClick";
    private static final String ONSWIPE_TAG = "OnSwipe";
    static final int OVERSHOOT = 5;
    private static final int SPLINE_STRING = -1;
    private static final String STATESET_TAG = "StateSet";
    private static final String TAG = "MotionScene";
    static final int TRANSITION_BACKWARD = 0;
    static final int TRANSITION_FORWARD = 1;
    private static final String TRANSITION_TAG = "Transition";
    public static final int UNSET = -1;
    private static final String VIEW_TRANSITION = "ViewTransition";
    private boolean DEBUG_DESKTOP;
    private ArrayList<Transition> mAbstractTransitionList;
    private HashMap<String, Integer> mConstraintSetIdMap;
    private SparseArray<ConstraintSet> mConstraintSetMap;
    Transition mCurrentTransition;
    private int mDefaultDuration;
    private Transition mDefaultTransition;
    private SparseIntArray mDeriveMap;
    private boolean mDisableAutoTransition;
    private boolean mIgnoreTouch;
    private MotionEvent mLastTouchDown;
    float mLastTouchX;
    float mLastTouchY;
    private int mLayoutDuringTransition;
    private final MotionLayout mMotionLayout;
    private boolean mMotionOutsideRegion;
    private boolean mRtl;
    StateSet mStateSet;
    private ArrayList<Transition> mTransitionList;
    private MotionLayout.MotionTracker mVelocityTracker;
    final ViewTransitionController mViewTransitionController;
    
    MotionScene(final Context context, final MotionLayout mMotionLayout, int motion_base) {
        this.mStateSet = null;
        this.mCurrentTransition = null;
        this.mDisableAutoTransition = false;
        this.mTransitionList = new ArrayList<Transition>();
        this.mDefaultTransition = null;
        this.mAbstractTransitionList = new ArrayList<Transition>();
        this.mConstraintSetMap = (SparseArray<ConstraintSet>)new SparseArray();
        this.mConstraintSetIdMap = new HashMap<String, Integer>();
        this.mDeriveMap = new SparseIntArray();
        this.DEBUG_DESKTOP = false;
        this.mDefaultDuration = 400;
        this.mLayoutDuringTransition = 0;
        this.mIgnoreTouch = false;
        this.mMotionOutsideRegion = false;
        this.mMotionLayout = mMotionLayout;
        this.mViewTransitionController = new ViewTransitionController(mMotionLayout);
        this.load(context, motion_base);
        final SparseArray<ConstraintSet> mConstraintSetMap = this.mConstraintSetMap;
        motion_base = R.id.motion_base;
        mConstraintSetMap.put(motion_base, (Object)new ConstraintSet());
        this.mConstraintSetIdMap.put("motion_base", motion_base);
    }
    
    public MotionScene(final MotionLayout mMotionLayout) {
        this.mStateSet = null;
        this.mCurrentTransition = null;
        this.mDisableAutoTransition = false;
        this.mTransitionList = new ArrayList<Transition>();
        this.mDefaultTransition = null;
        this.mAbstractTransitionList = new ArrayList<Transition>();
        this.mConstraintSetMap = (SparseArray<ConstraintSet>)new SparseArray();
        this.mConstraintSetIdMap = new HashMap<String, Integer>();
        this.mDeriveMap = new SparseIntArray();
        this.DEBUG_DESKTOP = false;
        this.mDefaultDuration = 400;
        this.mLayoutDuringTransition = 0;
        this.mIgnoreTouch = false;
        this.mMotionOutsideRegion = false;
        this.mMotionLayout = mMotionLayout;
        this.mViewTransitionController = new ViewTransitionController(mMotionLayout);
    }
    
    private int getId(final Context context, final String s) {
        int identifier;
        if (s.contains("/")) {
            final int i = identifier = context.getResources().getIdentifier(s.substring(s.indexOf(47) + 1), "id", context.getPackageName());
            if (this.DEBUG_DESKTOP) {
                final PrintStream out = System.out;
                final StringBuilder sb = new StringBuilder();
                sb.append("id getMap res = ");
                sb.append(i);
                out.println(sb.toString());
                identifier = i;
            }
        }
        else {
            identifier = -1;
        }
        int int1;
        if ((int1 = identifier) == -1) {
            int1 = identifier;
            if (s.length() > 1) {
                int1 = Integer.parseInt(s.substring(1));
            }
        }
        return int1;
    }
    
    private int getIndex(final Transition transition) {
        final int access$300 = transition.mId;
        if (access$300 != -1) {
            for (int i = 0; i < this.mTransitionList.size(); ++i) {
                if (this.mTransitionList.get(i).mId == access$300) {
                    return i;
                }
            }
            return -1;
        }
        throw new IllegalArgumentException("The transition must have an id");
    }
    
    static String getLine(final Context context, final int n, final XmlPullParser xmlPullParser) {
        final StringBuilder sb = new StringBuilder();
        sb.append(".(");
        sb.append(Debug.getName(context, n));
        sb.append(".xml:");
        sb.append(xmlPullParser.getLineNumber());
        sb.append(") \"");
        sb.append(xmlPullParser.getName());
        sb.append("\"");
        return sb.toString();
    }
    
    private int getRealID(final int n) {
        final StateSet mStateSet = this.mStateSet;
        if (mStateSet != null) {
            final int stateGetConstraintID = mStateSet.stateGetConstraintID(n, -1, -1);
            if (stateGetConstraintID != -1) {
                return stateGetConstraintID;
            }
        }
        return n;
    }
    
    private boolean hasCycleDependency(final int n) {
        for (int i = this.mDeriveMap.get(n), size = this.mDeriveMap.size(); i > 0; i = this.mDeriveMap.get(i), --size) {
            if (i == n) {
                return true;
            }
            if (size < 0) {
                return true;
            }
        }
        return false;
    }
    
    private boolean isProcessingTouch() {
        return this.mVelocityTracker != null;
    }
    
    private void load(final Context context, final int n) {
        final XmlResourceParser xml = context.getResources().getXml(n);
        try {
            int n2 = ((XmlPullParser)xml).getEventType();
            Transition transition = null;
            while (true) {
                final int n3 = 1;
                if (n2 == 1) {
                    break;
                }
                Transition transition2;
                if (n2 != 0) {
                    if (n2 != 2) {
                        transition2 = transition;
                    }
                    else {
                        final String name = ((XmlPullParser)xml).getName();
                        if (this.DEBUG_DESKTOP) {
                            final PrintStream out = System.out;
                            final StringBuilder sb = new StringBuilder();
                            sb.append("parsing = ");
                            sb.append(name);
                            out.println(sb.toString());
                        }
                        int n4 = 0;
                        Label_0360: {
                            switch (name.hashCode()) {
                                case 1942574248: {
                                    if (name.equals("include")) {
                                        n4 = 6;
                                        break Label_0360;
                                    }
                                    break;
                                }
                                case 1382829617: {
                                    if (name.equals("StateSet")) {
                                        n4 = 4;
                                        break Label_0360;
                                    }
                                    break;
                                }
                                case 793277014: {
                                    if (name.equals("MotionScene")) {
                                        n4 = 0;
                                        break Label_0360;
                                    }
                                    break;
                                }
                                case 327855227: {
                                    if (name.equals("OnSwipe")) {
                                        n4 = 2;
                                        break Label_0360;
                                    }
                                    break;
                                }
                                case 312750793: {
                                    if (name.equals("OnClick")) {
                                        n4 = 3;
                                        break Label_0360;
                                    }
                                    break;
                                }
                                case 269306229: {
                                    if (name.equals("Transition")) {
                                        n4 = n3;
                                        break Label_0360;
                                    }
                                    break;
                                }
                                case 61998586: {
                                    if (name.equals("ViewTransition")) {
                                        n4 = 9;
                                        break Label_0360;
                                    }
                                    break;
                                }
                                case -687739768: {
                                    if (name.equals("Include")) {
                                        n4 = 7;
                                        break Label_0360;
                                    }
                                    break;
                                }
                                case -1239391468: {
                                    if (name.equals("KeyFrameSet")) {
                                        n4 = 8;
                                        break Label_0360;
                                    }
                                    break;
                                }
                                case -1349929691: {
                                    if (name.equals("ConstraintSet")) {
                                        n4 = 5;
                                        break Label_0360;
                                    }
                                    break;
                                }
                            }
                            n4 = -1;
                        }
                        switch (n4) {
                            default: {
                                transition2 = transition;
                                break;
                            }
                            case 9: {
                                this.mViewTransitionController.add(new ViewTransition(context, (XmlPullParser)xml));
                                transition2 = transition;
                                break;
                            }
                            case 8: {
                                final KeyFrames e = new KeyFrames(context, (XmlPullParser)xml);
                                transition2 = transition;
                                if (transition != null) {
                                    transition.mKeyFramesList.add(e);
                                    transition2 = transition;
                                    break;
                                }
                                break;
                            }
                            case 6:
                            case 7: {
                                this.parseInclude(context, (XmlPullParser)xml);
                                transition2 = transition;
                                break;
                            }
                            case 5: {
                                this.parseConstraintSet(context, (XmlPullParser)xml);
                                transition2 = transition;
                                break;
                            }
                            case 4: {
                                this.mStateSet = new StateSet(context, (XmlPullParser)xml);
                                transition2 = transition;
                                break;
                            }
                            case 3: {
                                transition2 = transition;
                                if (transition != null) {
                                    transition.addOnClick(context, (XmlPullParser)xml);
                                    transition2 = transition;
                                    break;
                                }
                                break;
                            }
                            case 2: {
                                if (transition == null) {
                                    final String resourceEntryName = context.getResources().getResourceEntryName(n);
                                    final int lineNumber = ((XmlPullParser)xml).getLineNumber();
                                    final StringBuilder sb2 = new StringBuilder();
                                    sb2.append(" OnSwipe (");
                                    sb2.append(resourceEntryName);
                                    sb2.append(".xml:");
                                    sb2.append(lineNumber);
                                    sb2.append(")");
                                }
                                if ((transition2 = transition) != null) {
                                    transition.mTouchResponse = new TouchResponse(context, this.mMotionLayout, (XmlPullParser)xml);
                                    transition2 = transition;
                                    break;
                                }
                                break;
                            }
                            case 1: {
                                final ArrayList<Transition> mTransitionList = this.mTransitionList;
                                final Transition o = new Transition(this, context, (XmlPullParser)xml);
                                mTransitionList.add(o);
                                if (this.mCurrentTransition == null && !o.mIsAbstract) {
                                    this.mCurrentTransition = o;
                                    if (o.mTouchResponse != null) {
                                        this.mCurrentTransition.mTouchResponse.setRTL(this.mRtl);
                                    }
                                }
                                transition2 = o;
                                if (o.mIsAbstract) {
                                    if (o.mConstraintSetEnd == -1) {
                                        this.mDefaultTransition = o;
                                    }
                                    else {
                                        this.mAbstractTransitionList.add(o);
                                    }
                                    this.mTransitionList.remove(o);
                                    transition2 = o;
                                    break;
                                }
                                break;
                            }
                            case 0: {
                                this.parseMotionSceneTags(context, (XmlPullParser)xml);
                                transition2 = transition;
                                break;
                            }
                        }
                    }
                }
                else {
                    ((XmlPullParser)xml).getName();
                    transition2 = transition;
                }
                n2 = ((XmlPullParser)xml).next();
                transition = transition2;
            }
        }
        catch (final IOException ex) {
            ex.printStackTrace();
        }
        catch (final XmlPullParserException ex2) {
            ex2.printStackTrace();
        }
    }
    
    private int parseConstraintSet(final Context context, final XmlPullParser xmlPullParser) {
        final ConstraintSet set = new ConstraintSet();
        set.setForceId(false);
        final int attributeCount = xmlPullParser.getAttributeCount();
        int n = 0;
        int id = -1;
        int id2 = -1;
        while (true) {
            int n2 = 1;
            if (n >= attributeCount) {
                break;
            }
            final String attributeName = xmlPullParser.getAttributeName(n);
            final String attributeValue = xmlPullParser.getAttributeValue(n);
            if (this.DEBUG_DESKTOP) {
                final PrintStream out = System.out;
                final StringBuilder sb = new StringBuilder();
                sb.append("id string = ");
                sb.append(attributeValue);
                out.println(sb.toString());
            }
            attributeName.hashCode();
            Label_0209: {
                switch (attributeName) {
                    case "id": {
                        n2 = 2;
                        break Label_0209;
                    }
                    case "deriveConstraintsFrom": {
                        break Label_0209;
                    }
                    case "ConstraintRotate": {
                        n2 = 0;
                        break Label_0209;
                    }
                    default:
                        break;
                }
                n2 = -1;
            }
            switch (n2) {
                case 2: {
                    id = this.getId(context, attributeValue);
                    this.mConstraintSetIdMap.put(stripID(attributeValue), id);
                    set.mIdString = Debug.getName(context, id);
                    break;
                }
                case 1: {
                    id2 = this.getId(context, attributeValue);
                    break;
                }
                case 0: {
                    set.mRotate = Integer.parseInt(attributeValue);
                    break;
                }
            }
            ++n;
        }
        if (id != -1) {
            if (this.mMotionLayout.mDebugPath != 0) {
                set.setValidateOnParse(true);
            }
            set.load(context, xmlPullParser);
            if (id2 != -1) {
                this.mDeriveMap.put(id, id2);
            }
            this.mConstraintSetMap.put(id, (Object)set);
        }
        return id;
    }
    
    private int parseInclude(final Context context, int i) {
        final XmlResourceParser xml = context.getResources().getXml(i);
        try {
            String name;
            for (i = ((XmlPullParser)xml).getEventType(); i != 1; i = ((XmlPullParser)xml).next()) {
                name = ((XmlPullParser)xml).getName();
                if (2 == i && "ConstraintSet".equals(name)) {
                    return this.parseConstraintSet(context, (XmlPullParser)xml);
                }
            }
            return -1;
        }
        catch (final IOException ex) {
            ex.printStackTrace();
        }
        catch (final XmlPullParserException ex2) {
            ex2.printStackTrace();
        }
        return -1;
    }
    
    private void parseInclude(final Context context, final XmlPullParser xmlPullParser) {
        final TypedArray obtainStyledAttributes = context.obtainStyledAttributes(Xml.asAttributeSet(xmlPullParser), R.styleable.include);
        for (int indexCount = obtainStyledAttributes.getIndexCount(), i = 0; i < indexCount; ++i) {
            final int index = obtainStyledAttributes.getIndex(i);
            if (index == R.styleable.include_constraintSet) {
                this.parseInclude(context, obtainStyledAttributes.getResourceId(index, -1));
            }
        }
        obtainStyledAttributes.recycle();
    }
    
    private void parseMotionSceneTags(final Context context, final XmlPullParser xmlPullParser) {
        final TypedArray obtainStyledAttributes = context.obtainStyledAttributes(Xml.asAttributeSet(xmlPullParser), R.styleable.MotionScene);
        for (int indexCount = obtainStyledAttributes.getIndexCount(), i = 0; i < indexCount; ++i) {
            final int index = obtainStyledAttributes.getIndex(i);
            if (index == R.styleable.MotionScene_defaultDuration) {
                if ((this.mDefaultDuration = obtainStyledAttributes.getInt(index, this.mDefaultDuration)) < 8) {
                    this.mDefaultDuration = 8;
                }
            }
            else if (index == R.styleable.MotionScene_layoutDuringTransition) {
                this.mLayoutDuringTransition = obtainStyledAttributes.getInteger(index, 0);
            }
        }
        obtainStyledAttributes.recycle();
    }
    
    private void readConstraintChain(int value, final MotionLayout motionLayout) {
        final ConstraintSet set = (ConstraintSet)this.mConstraintSetMap.get(value);
        set.derivedState = set.mIdString;
        value = this.mDeriveMap.get(value);
        if (value > 0) {
            this.readConstraintChain(value, motionLayout);
            final ConstraintSet set2 = (ConstraintSet)this.mConstraintSetMap.get(value);
            if (set2 == null) {
                final StringBuilder sb = new StringBuilder();
                sb.append("ERROR! invalid deriveConstraintsFrom: @id/");
                sb.append(Debug.getName(((View)this.mMotionLayout).getContext(), value));
                return;
            }
            final StringBuilder sb2 = new StringBuilder();
            sb2.append(set.derivedState);
            sb2.append("/");
            sb2.append(set2.derivedState);
            set.derivedState = sb2.toString();
            set.readFallback(set2);
        }
        else {
            final StringBuilder sb3 = new StringBuilder();
            sb3.append(set.derivedState);
            sb3.append("  layout");
            set.derivedState = sb3.toString();
            set.readFallback(motionLayout);
        }
        set.applyDeltaFrom(set);
    }
    
    public static String stripID(final String s) {
        if (s == null) {
            return "";
        }
        final int index = s.indexOf(47);
        if (index < 0) {
            return s;
        }
        return s.substring(index + 1);
    }
    
    public void addOnClickListeners(final MotionLayout motionLayout, final int n) {
        for (final Transition transition : this.mTransitionList) {
            if (transition.mOnClicks.size() > 0) {
                final Iterator iterator2 = transition.mOnClicks.iterator();
                while (iterator2.hasNext()) {
                    ((TransitionOnClick)iterator2.next()).removeOnClickListeners(motionLayout);
                }
            }
        }
        for (final Transition transition2 : this.mAbstractTransitionList) {
            if (transition2.mOnClicks.size() > 0) {
                final Iterator iterator4 = transition2.mOnClicks.iterator();
                while (iterator4.hasNext()) {
                    ((TransitionOnClick)iterator4.next()).removeOnClickListeners(motionLayout);
                }
            }
        }
        for (final Transition transition3 : this.mTransitionList) {
            if (transition3.mOnClicks.size() > 0) {
                final Iterator iterator6 = transition3.mOnClicks.iterator();
                while (iterator6.hasNext()) {
                    ((TransitionOnClick)iterator6.next()).addOnClickListeners(motionLayout, n, transition3);
                }
            }
        }
        for (final Transition transition4 : this.mAbstractTransitionList) {
            if (transition4.mOnClicks.size() > 0) {
                final Iterator iterator8 = transition4.mOnClicks.iterator();
                while (iterator8.hasNext()) {
                    ((TransitionOnClick)iterator8.next()).addOnClickListeners(motionLayout, n, transition4);
                }
            }
        }
    }
    
    public void addTransition(final Transition transition) {
        final int index = this.getIndex(transition);
        if (index == -1) {
            this.mTransitionList.add(transition);
        }
        else {
            this.mTransitionList.set(index, transition);
        }
    }
    
    public boolean applyViewTransition(final int n, final MotionController motionController) {
        return this.mViewTransitionController.applyViewTransition(n, motionController);
    }
    
    boolean autoTransition(final MotionLayout motionLayout, final int n) {
        if (this.isProcessingTouch()) {
            return false;
        }
        if (this.mDisableAutoTransition) {
            return false;
        }
        for (final Transition transition : this.mTransitionList) {
            if (transition.mAutoTransition == 0) {
                continue;
            }
            final Transition mCurrentTransition = this.mCurrentTransition;
            if (mCurrentTransition == transition && mCurrentTransition.isTransitionFlag(2)) {
                continue;
            }
            if (n == transition.mConstraintSetStart && (transition.mAutoTransition == 4 || transition.mAutoTransition == 2)) {
                final MotionLayout.TransitionState finished = MotionLayout.TransitionState.FINISHED;
                motionLayout.setState(finished);
                motionLayout.setTransition(transition);
                if (transition.mAutoTransition == 4) {
                    motionLayout.transitionToEnd();
                    motionLayout.setState(MotionLayout.TransitionState.SETUP);
                    motionLayout.setState(MotionLayout.TransitionState.MOVING);
                }
                else {
                    motionLayout.setProgress(1.0f);
                    motionLayout.evaluate(true);
                    motionLayout.setState(MotionLayout.TransitionState.SETUP);
                    motionLayout.setState(MotionLayout.TransitionState.MOVING);
                    motionLayout.setState(finished);
                    motionLayout.onNewStateAttachHandlers();
                }
                return true;
            }
            if (n == transition.mConstraintSetEnd && (transition.mAutoTransition == 3 || transition.mAutoTransition == 1)) {
                final MotionLayout.TransitionState finished2 = MotionLayout.TransitionState.FINISHED;
                motionLayout.setState(finished2);
                motionLayout.setTransition(transition);
                if (transition.mAutoTransition == 3) {
                    motionLayout.transitionToStart();
                    motionLayout.setState(MotionLayout.TransitionState.SETUP);
                    motionLayout.setState(MotionLayout.TransitionState.MOVING);
                }
                else {
                    motionLayout.setProgress(0.0f);
                    motionLayout.evaluate(true);
                    motionLayout.setState(MotionLayout.TransitionState.SETUP);
                    motionLayout.setState(MotionLayout.TransitionState.MOVING);
                    motionLayout.setState(finished2);
                    motionLayout.onNewStateAttachHandlers();
                }
                return true;
            }
        }
        return false;
    }
    
    public Transition bestTransitionFor(final int n, final float n2, final float n3, final MotionEvent motionEvent) {
        if (n != -1) {
            final List<Transition> transitionsWithState = this.getTransitionsWithState(n);
            final RectF rectF = new RectF();
            final Iterator<Transition> iterator = transitionsWithState.iterator();
            float n4 = 0.0f;
            Transition transition = null;
            while (iterator.hasNext()) {
                final Transition transition2 = iterator.next();
                if (transition2.mDisable) {
                    continue;
                }
                if (transition2.mTouchResponse == null) {
                    continue;
                }
                transition2.mTouchResponse.setRTL(this.mRtl);
                final RectF touchRegion = transition2.mTouchResponse.getTouchRegion(this.mMotionLayout, rectF);
                if (touchRegion != null && motionEvent != null && !touchRegion.contains(motionEvent.getX(), motionEvent.getY())) {
                    continue;
                }
                final RectF limitBoundsTo = transition2.mTouchResponse.getLimitBoundsTo(this.mMotionLayout, rectF);
                if (limitBoundsTo != null && motionEvent != null && !limitBoundsTo.contains(motionEvent.getX(), motionEvent.getY())) {
                    continue;
                }
                float dot = transition2.mTouchResponse.dot(n2, n3);
                if (transition2.mTouchResponse.mIsRotateMode) {
                    dot = dot;
                    if (motionEvent != null) {
                        final float n5 = motionEvent.getX() - transition2.mTouchResponse.mRotateCenterX;
                        final float n6 = motionEvent.getY() - transition2.mTouchResponse.mRotateCenterY;
                        dot = (float)(Math.atan2(n3 + n6, n2 + n5) - Math.atan2(n5, n6)) * 10.0f;
                    }
                }
                float n7;
                if (transition2.mConstraintSetEnd == n) {
                    n7 = -1.0f;
                }
                else {
                    n7 = 1.1f;
                }
                final float n8 = dot * n7;
                if (n8 <= n4) {
                    continue;
                }
                transition = transition2;
                n4 = n8;
            }
            return transition;
        }
        return this.mCurrentTransition;
    }
    
    public void disableAutoTransition(final boolean mDisableAutoTransition) {
        this.mDisableAutoTransition = mDisableAutoTransition;
    }
    
    public void enableViewTransition(final int n, final boolean b) {
        this.mViewTransitionController.enableViewTransition(n, b);
    }
    
    public int gatPathMotionArc() {
        final Transition mCurrentTransition = this.mCurrentTransition;
        int access$1900;
        if (mCurrentTransition != null) {
            access$1900 = mCurrentTransition.mPathMotionArc;
        }
        else {
            access$1900 = -1;
        }
        return access$1900;
    }
    
    int getAutoCompleteMode() {
        final Transition mCurrentTransition = this.mCurrentTransition;
        if (mCurrentTransition != null && mCurrentTransition.mTouchResponse != null) {
            return this.mCurrentTransition.mTouchResponse.getAutoCompleteMode();
        }
        return 0;
    }
    
    ConstraintSet getConstraintSet(final int n) {
        return this.getConstraintSet(n, -1, -1);
    }
    
    ConstraintSet getConstraintSet(final int i, int stateGetConstraintID, final int n) {
        if (this.DEBUG_DESKTOP) {
            final PrintStream out = System.out;
            final StringBuilder sb = new StringBuilder();
            sb.append("id ");
            sb.append(i);
            out.println(sb.toString());
            final PrintStream out2 = System.out;
            final StringBuilder sb2 = new StringBuilder();
            sb2.append("size ");
            sb2.append(this.mConstraintSetMap.size());
            out2.println(sb2.toString());
        }
        final StateSet mStateSet = this.mStateSet;
        int n2 = i;
        if (mStateSet != null) {
            stateGetConstraintID = mStateSet.stateGetConstraintID(i, stateGetConstraintID, n);
            n2 = i;
            if (stateGetConstraintID != -1) {
                n2 = stateGetConstraintID;
            }
        }
        if (this.mConstraintSetMap.get(n2) == null) {
            final StringBuilder sb3 = new StringBuilder();
            sb3.append("Warning could not find ConstraintSet id/");
            sb3.append(Debug.getName(((View)this.mMotionLayout).getContext(), n2));
            sb3.append(" In MotionScene");
            final SparseArray<ConstraintSet> mConstraintSetMap = this.mConstraintSetMap;
            return (ConstraintSet)mConstraintSetMap.get(mConstraintSetMap.keyAt(0));
        }
        return (ConstraintSet)this.mConstraintSetMap.get(n2);
    }
    
    public ConstraintSet getConstraintSet(final Context context, final String s) {
        if (this.DEBUG_DESKTOP) {
            final PrintStream out = System.out;
            final StringBuilder sb = new StringBuilder();
            sb.append("id ");
            sb.append(s);
            out.println(sb.toString());
            final PrintStream out2 = System.out;
            final StringBuilder sb2 = new StringBuilder();
            sb2.append("size ");
            sb2.append(this.mConstraintSetMap.size());
            out2.println(sb2.toString());
        }
        for (int i = 0; i < this.mConstraintSetMap.size(); ++i) {
            final int key = this.mConstraintSetMap.keyAt(i);
            final String resourceName = context.getResources().getResourceName(key);
            if (this.DEBUG_DESKTOP) {
                final PrintStream out3 = System.out;
                final StringBuilder sb3 = new StringBuilder();
                sb3.append("Id for <");
                sb3.append(i);
                sb3.append("> is <");
                sb3.append(resourceName);
                sb3.append("> looking for <");
                sb3.append(s);
                sb3.append(">");
                out3.println(sb3.toString());
            }
            if (s.equals(resourceName)) {
                return (ConstraintSet)this.mConstraintSetMap.get(key);
            }
        }
        return null;
    }
    
    public int[] getConstraintSetIds() {
        final int size = this.mConstraintSetMap.size();
        final int[] array = new int[size];
        for (int i = 0; i < size; ++i) {
            array[i] = this.mConstraintSetMap.keyAt(i);
        }
        return array;
    }
    
    public ArrayList<Transition> getDefinedTransitions() {
        return this.mTransitionList;
    }
    
    public int getDuration() {
        final Transition mCurrentTransition = this.mCurrentTransition;
        if (mCurrentTransition != null) {
            return mCurrentTransition.mDuration;
        }
        return this.mDefaultDuration;
    }
    
    int getEndId() {
        final Transition mCurrentTransition = this.mCurrentTransition;
        if (mCurrentTransition == null) {
            return -1;
        }
        return mCurrentTransition.mConstraintSetEnd;
    }
    
    public Interpolator getInterpolator() {
        final int access$1500 = this.mCurrentTransition.mDefaultInterpolator;
        if (access$1500 == -2) {
            return AnimationUtils.loadInterpolator(((View)this.mMotionLayout).getContext(), this.mCurrentTransition.mDefaultInterpolatorID);
        }
        if (access$1500 == -1) {
            return (Interpolator)new Interpolator(this, Easing.getInterpolator(this.mCurrentTransition.mDefaultInterpolatorString)) {
                final MotionScene this$0;
                final Easing val$easing;
                
                public float getInterpolation(final float n) {
                    return (float)this.val$easing.get(n);
                }
            };
        }
        if (access$1500 == 0) {
            return (Interpolator)new AccelerateDecelerateInterpolator();
        }
        if (access$1500 == 1) {
            return (Interpolator)new AccelerateInterpolator();
        }
        if (access$1500 == 2) {
            return (Interpolator)new DecelerateInterpolator();
        }
        if (access$1500 == 4) {
            return (Interpolator)new BounceInterpolator();
        }
        if (access$1500 == 5) {
            return (Interpolator)new OvershootInterpolator();
        }
        if (access$1500 != 6) {
            return null;
        }
        return (Interpolator)new AnticipateInterpolator();
    }
    
    Key getKeyFrame(final Context context, final int n, final int n2, final int n3) {
        final Transition mCurrentTransition = this.mCurrentTransition;
        if (mCurrentTransition == null) {
            return null;
        }
        for (final KeyFrames keyFrames : mCurrentTransition.mKeyFramesList) {
            for (final Integer n4 : keyFrames.getKeys()) {
                if (n2 == n4) {
                    for (final Key key : keyFrames.getKeyFramesForView(n4)) {
                        if (key.mFramePosition == n3 && key.mType == n) {
                            return key;
                        }
                    }
                }
            }
        }
        return null;
    }
    
    public void getKeyFrames(final MotionController motionController) {
        final Transition mCurrentTransition = this.mCurrentTransition;
        if (mCurrentTransition == null) {
            final Transition mDefaultTransition = this.mDefaultTransition;
            if (mDefaultTransition != null) {
                final Iterator iterator = mDefaultTransition.mKeyFramesList.iterator();
                while (iterator.hasNext()) {
                    ((KeyFrames)iterator.next()).addFrames(motionController);
                }
            }
            return;
        }
        final Iterator iterator2 = mCurrentTransition.mKeyFramesList.iterator();
        while (iterator2.hasNext()) {
            ((KeyFrames)iterator2.next()).addFrames(motionController);
        }
    }
    
    float getMaxAcceleration() {
        final Transition mCurrentTransition = this.mCurrentTransition;
        if (mCurrentTransition != null && mCurrentTransition.mTouchResponse != null) {
            return this.mCurrentTransition.mTouchResponse.getMaxAcceleration();
        }
        return 0.0f;
    }
    
    float getMaxVelocity() {
        final Transition mCurrentTransition = this.mCurrentTransition;
        if (mCurrentTransition != null && mCurrentTransition.mTouchResponse != null) {
            return this.mCurrentTransition.mTouchResponse.getMaxVelocity();
        }
        return 0.0f;
    }
    
    boolean getMoveWhenScrollAtTop() {
        final Transition mCurrentTransition = this.mCurrentTransition;
        return mCurrentTransition != null && mCurrentTransition.mTouchResponse != null && this.mCurrentTransition.mTouchResponse.getMoveWhenScrollAtTop();
    }
    
    public float getPathPercent(final View view, final int n) {
        return 0.0f;
    }
    
    float getProgressDirection(final float n, final float n2) {
        final Transition mCurrentTransition = this.mCurrentTransition;
        if (mCurrentTransition != null && mCurrentTransition.mTouchResponse != null) {
            return this.mCurrentTransition.mTouchResponse.getProgressDirection(n, n2);
        }
        return 0.0f;
    }
    
    int getSpringBoundary() {
        final Transition mCurrentTransition = this.mCurrentTransition;
        if (mCurrentTransition != null && mCurrentTransition.mTouchResponse != null) {
            return this.mCurrentTransition.mTouchResponse.getSpringBoundary();
        }
        return 0;
    }
    
    float getSpringDamping() {
        final Transition mCurrentTransition = this.mCurrentTransition;
        if (mCurrentTransition != null && mCurrentTransition.mTouchResponse != null) {
            return this.mCurrentTransition.mTouchResponse.getSpringDamping();
        }
        return 0.0f;
    }
    
    float getSpringMass() {
        final Transition mCurrentTransition = this.mCurrentTransition;
        if (mCurrentTransition != null && mCurrentTransition.mTouchResponse != null) {
            return this.mCurrentTransition.mTouchResponse.getSpringMass();
        }
        return 0.0f;
    }
    
    float getSpringStiffiness() {
        final Transition mCurrentTransition = this.mCurrentTransition;
        if (mCurrentTransition != null && mCurrentTransition.mTouchResponse != null) {
            return this.mCurrentTransition.mTouchResponse.getSpringStiffness();
        }
        return 0.0f;
    }
    
    float getSpringStopThreshold() {
        final Transition mCurrentTransition = this.mCurrentTransition;
        if (mCurrentTransition != null && mCurrentTransition.mTouchResponse != null) {
            return this.mCurrentTransition.mTouchResponse.getSpringStopThreshold();
        }
        return 0.0f;
    }
    
    public float getStaggered() {
        final Transition mCurrentTransition = this.mCurrentTransition;
        if (mCurrentTransition != null) {
            return mCurrentTransition.mStagger;
        }
        return 0.0f;
    }
    
    int getStartId() {
        final Transition mCurrentTransition = this.mCurrentTransition;
        if (mCurrentTransition == null) {
            return -1;
        }
        return mCurrentTransition.mConstraintSetStart;
    }
    
    public Transition getTransitionById(final int n) {
        for (final Transition transition : this.mTransitionList) {
            if (transition.mId == n) {
                return transition;
            }
        }
        return null;
    }
    
    int getTransitionDirection(final int n) {
        final Iterator<Transition> iterator = this.mTransitionList.iterator();
        while (iterator.hasNext()) {
            if (iterator.next().mConstraintSetStart == n) {
                return 0;
            }
        }
        return 1;
    }
    
    public List<Transition> getTransitionsWithState(int realID) {
        realID = this.getRealID(realID);
        final ArrayList list = new ArrayList();
        for (final Transition e : this.mTransitionList) {
            if (e.mConstraintSetStart == realID || e.mConstraintSetEnd == realID) {
                list.add(e);
            }
        }
        return list;
    }
    
    boolean hasKeyFramePosition(final View view, final int n) {
        final Transition mCurrentTransition = this.mCurrentTransition;
        if (mCurrentTransition == null) {
            return false;
        }
        final Iterator iterator = mCurrentTransition.mKeyFramesList.iterator();
        while (iterator.hasNext()) {
            final Iterator<Key> iterator2 = ((KeyFrames)iterator.next()).getKeyFramesForView(view.getId()).iterator();
            while (iterator2.hasNext()) {
                if (iterator2.next().mFramePosition == n) {
                    return true;
                }
            }
        }
        return false;
    }
    
    public boolean isViewTransitionEnabled(final int n) {
        return this.mViewTransitionController.isViewTransitionEnabled(n);
    }
    
    public int lookUpConstraintId(final String key) {
        final Integer n = this.mConstraintSetIdMap.get(key);
        if (n == null) {
            return 0;
        }
        return n;
    }
    
    public String lookUpConstraintName(final int n) {
        for (final Map.Entry<K, Integer> entry : this.mConstraintSetIdMap.entrySet()) {
            final Integer n2 = entry.getValue();
            if (n2 == null) {
                continue;
            }
            if (n2 == n) {
                return (String)entry.getKey();
            }
        }
        return null;
    }
    
    protected void onLayout(final boolean b, final int n, final int n2, final int n3, final int n4) {
    }
    
    void processScrollMove(final float n, final float n2) {
        final Transition mCurrentTransition = this.mCurrentTransition;
        if (mCurrentTransition != null && mCurrentTransition.mTouchResponse != null) {
            this.mCurrentTransition.mTouchResponse.scrollMove(n, n2);
        }
    }
    
    void processScrollUp(final float n, final float n2) {
        final Transition mCurrentTransition = this.mCurrentTransition;
        if (mCurrentTransition != null && mCurrentTransition.mTouchResponse != null) {
            this.mCurrentTransition.mTouchResponse.scrollUp(n, n2);
        }
    }
    
    void processTouchEvent(final MotionEvent mLastTouchDown, int mCurrentState, final MotionLayout motionLayout) {
        final RectF rectF = new RectF();
        if (this.mVelocityTracker == null) {
            this.mVelocityTracker = this.mMotionLayout.obtainVelocityTracker();
        }
        this.mVelocityTracker.addMovement(mLastTouchDown);
        Label_0395: {
            if (mCurrentState != -1) {
                final int action = mLastTouchDown.getAction();
                final boolean b = false;
                if (action == 0) {
                    this.mLastTouchX = mLastTouchDown.getRawX();
                    this.mLastTouchY = mLastTouchDown.getRawY();
                    this.mLastTouchDown = mLastTouchDown;
                    this.mIgnoreTouch = false;
                    if (this.mCurrentTransition.mTouchResponse != null) {
                        final RectF limitBoundsTo = this.mCurrentTransition.mTouchResponse.getLimitBoundsTo(this.mMotionLayout, rectF);
                        if (limitBoundsTo != null && !limitBoundsTo.contains(this.mLastTouchDown.getX(), this.mLastTouchDown.getY())) {
                            this.mLastTouchDown = null;
                            this.mIgnoreTouch = true;
                            return;
                        }
                        final RectF touchRegion = this.mCurrentTransition.mTouchResponse.getTouchRegion(this.mMotionLayout, rectF);
                        if (touchRegion != null && !touchRegion.contains(this.mLastTouchDown.getX(), this.mLastTouchDown.getY())) {
                            this.mMotionOutsideRegion = true;
                        }
                        else {
                            this.mMotionOutsideRegion = false;
                        }
                        this.mCurrentTransition.mTouchResponse.setDown(this.mLastTouchX, this.mLastTouchY);
                    }
                    return;
                }
                if (action == 2) {
                    if (!this.mIgnoreTouch) {
                        final float n = mLastTouchDown.getRawY() - this.mLastTouchY;
                        final float n2 = mLastTouchDown.getRawX() - this.mLastTouchX;
                        if (n2 != 0.0 || n != 0.0) {
                            final MotionEvent mLastTouchDown2 = this.mLastTouchDown;
                            if (mLastTouchDown2 != null) {
                                final Transition bestTransition = this.bestTransitionFor(mCurrentState, n2, n, mLastTouchDown2);
                                if (bestTransition != null) {
                                    motionLayout.setTransition(bestTransition);
                                    final RectF touchRegion2 = this.mCurrentTransition.mTouchResponse.getTouchRegion(this.mMotionLayout, rectF);
                                    boolean mMotionOutsideRegion = b;
                                    if (touchRegion2 != null) {
                                        mMotionOutsideRegion = b;
                                        if (!touchRegion2.contains(this.mLastTouchDown.getX(), this.mLastTouchDown.getY())) {
                                            mMotionOutsideRegion = true;
                                        }
                                    }
                                    this.mMotionOutsideRegion = mMotionOutsideRegion;
                                    this.mCurrentTransition.mTouchResponse.setUpTouchEvent(this.mLastTouchX, this.mLastTouchY);
                                }
                                break Label_0395;
                            }
                        }
                        return;
                    }
                }
            }
        }
        if (this.mIgnoreTouch) {
            return;
        }
        final Transition mCurrentTransition = this.mCurrentTransition;
        if (mCurrentTransition != null && mCurrentTransition.mTouchResponse != null && !this.mMotionOutsideRegion) {
            this.mCurrentTransition.mTouchResponse.processTouchEvent(mLastTouchDown, this.mVelocityTracker, mCurrentState, this);
        }
        this.mLastTouchX = mLastTouchDown.getRawX();
        this.mLastTouchY = mLastTouchDown.getRawY();
        if (mLastTouchDown.getAction() == 1) {
            final MotionLayout.MotionTracker mVelocityTracker = this.mVelocityTracker;
            if (mVelocityTracker != null) {
                mVelocityTracker.recycle();
                this.mVelocityTracker = null;
                mCurrentState = motionLayout.mCurrentState;
                if (mCurrentState != -1) {
                    this.autoTransition(motionLayout, mCurrentState);
                }
            }
        }
    }
    
    void readFallback(final MotionLayout motionLayout) {
        for (int i = 0; i < this.mConstraintSetMap.size(); ++i) {
            final int key = this.mConstraintSetMap.keyAt(i);
            if (this.hasCycleDependency(key)) {
                return;
            }
            this.readConstraintChain(key, motionLayout);
        }
    }
    
    public void removeTransition(final Transition transition) {
        final int index = this.getIndex(transition);
        if (index != -1) {
            this.mTransitionList.remove(index);
        }
    }
    
    public void setConstraintSet(final int n, final ConstraintSet set) {
        this.mConstraintSetMap.put(n, (Object)set);
    }
    
    public void setDuration(final int n) {
        final Transition mCurrentTransition = this.mCurrentTransition;
        if (mCurrentTransition != null) {
            mCurrentTransition.setDuration(n);
        }
        else {
            this.mDefaultDuration = n;
        }
    }
    
    public void setKeyframe(final View view, final int n, final String s, final Object o) {
        final Transition mCurrentTransition = this.mCurrentTransition;
        if (mCurrentTransition == null) {
            return;
        }
        final Iterator iterator = mCurrentTransition.mKeyFramesList.iterator();
        while (iterator.hasNext()) {
            final Iterator<Key> iterator2 = ((KeyFrames)iterator.next()).getKeyFramesForView(view.getId()).iterator();
            while (iterator2.hasNext()) {
                if (iterator2.next().mFramePosition == n) {
                    if (o != null) {
                        ((Float)o).floatValue();
                    }
                    s.equalsIgnoreCase("app:PerpendicularPath_percent");
                }
            }
        }
    }
    
    public void setRtl(final boolean mRtl) {
        this.mRtl = mRtl;
        final Transition mCurrentTransition = this.mCurrentTransition;
        if (mCurrentTransition != null && mCurrentTransition.mTouchResponse != null) {
            this.mCurrentTransition.mTouchResponse.setRTL(this.mRtl);
        }
    }
    
    void setTransition(final int n, final int n2) {
        final StateSet mStateSet = this.mStateSet;
        int stateGetConstraintID = 0;
        int stateGetConstraintID2 = 0;
        Label_0063: {
            int n3;
            if (mStateSet != null) {
                stateGetConstraintID = mStateSet.stateGetConstraintID(n, -1, -1);
                if (stateGetConstraintID == -1) {
                    stateGetConstraintID = n;
                }
                stateGetConstraintID2 = this.mStateSet.stateGetConstraintID(n2, -1, -1);
                n3 = stateGetConstraintID;
                if (stateGetConstraintID2 != -1) {
                    break Label_0063;
                }
            }
            else {
                n3 = n;
            }
            stateGetConstraintID2 = n2;
            stateGetConstraintID = n3;
        }
        final Transition mCurrentTransition = this.mCurrentTransition;
        if (mCurrentTransition != null && mCurrentTransition.mConstraintSetEnd == n2 && this.mCurrentTransition.mConstraintSetStart == n) {
            return;
        }
        for (final Transition mCurrentTransition2 : this.mTransitionList) {
            if ((mCurrentTransition2.mConstraintSetEnd == stateGetConstraintID2 && mCurrentTransition2.mConstraintSetStart == stateGetConstraintID) || (mCurrentTransition2.mConstraintSetEnd == n2 && mCurrentTransition2.mConstraintSetStart == n)) {
                this.mCurrentTransition = mCurrentTransition2;
                if (mCurrentTransition2 != null && mCurrentTransition2.mTouchResponse != null) {
                    this.mCurrentTransition.mTouchResponse.setRTL(this.mRtl);
                }
                return;
            }
        }
        Transition mDefaultTransition = this.mDefaultTransition;
        for (final Transition transition : this.mAbstractTransitionList) {
            if (transition.mConstraintSetEnd == n2) {
                mDefaultTransition = transition;
            }
        }
        final Transition transition2 = new Transition(this, mDefaultTransition);
        transition2.mConstraintSetStart = stateGetConstraintID;
        transition2.mConstraintSetEnd = stateGetConstraintID2;
        if (stateGetConstraintID != -1) {
            this.mTransitionList.add(transition2);
        }
        this.mCurrentTransition = transition2;
    }
    
    public void setTransition(final Transition mCurrentTransition) {
        this.mCurrentTransition = mCurrentTransition;
        if (mCurrentTransition != null && mCurrentTransition.mTouchResponse != null) {
            this.mCurrentTransition.mTouchResponse.setRTL(this.mRtl);
        }
    }
    
    void setupTouch() {
        final Transition mCurrentTransition = this.mCurrentTransition;
        if (mCurrentTransition != null && mCurrentTransition.mTouchResponse != null) {
            this.mCurrentTransition.mTouchResponse.setupTouch();
        }
    }
    
    boolean supportTouch() {
        final Iterator<Transition> iterator = this.mTransitionList.iterator();
        do {
            final boolean hasNext = iterator.hasNext();
            boolean b = true;
            if (hasNext) {
                continue;
            }
            final Transition mCurrentTransition = this.mCurrentTransition;
            if (mCurrentTransition == null || mCurrentTransition.mTouchResponse == null) {
                b = false;
            }
            return b;
        } while (iterator.next().mTouchResponse == null);
        return true;
    }
    
    public boolean validateLayout(final MotionLayout motionLayout) {
        return motionLayout == this.mMotionLayout && motionLayout.mScene == this;
    }
    
    public void viewTransition(final int n, final View... array) {
        this.mViewTransitionController.viewTransition(n, array);
    }
    
    public static class Transition
    {
        public static final int AUTO_ANIMATE_TO_END = 4;
        public static final int AUTO_ANIMATE_TO_START = 3;
        public static final int AUTO_JUMP_TO_END = 2;
        public static final int AUTO_JUMP_TO_START = 1;
        public static final int AUTO_NONE = 0;
        static final int TRANSITION_FLAG_FIRST_DRAW = 1;
        static final int TRANSITION_FLAG_INTRA_AUTO = 2;
        private int mAutoTransition;
        private int mConstraintSetEnd;
        private int mConstraintSetStart;
        private int mDefaultInterpolator;
        private int mDefaultInterpolatorID;
        private String mDefaultInterpolatorString;
        private boolean mDisable;
        private int mDuration;
        private int mId;
        private boolean mIsAbstract;
        private ArrayList<KeyFrames> mKeyFramesList;
        private int mLayoutDuringTransition;
        private final MotionScene mMotionScene;
        private ArrayList<TransitionOnClick> mOnClicks;
        private int mPathMotionArc;
        private float mStagger;
        private TouchResponse mTouchResponse;
        private int mTransitionFlags;
        
        public Transition(final int mId, final MotionScene mMotionScene, final int mConstraintSetStart, final int mConstraintSetEnd) {
            this.mId = -1;
            this.mIsAbstract = false;
            this.mConstraintSetEnd = -1;
            this.mConstraintSetStart = -1;
            this.mDefaultInterpolator = 0;
            this.mDefaultInterpolatorString = null;
            this.mDefaultInterpolatorID = -1;
            this.mDuration = 400;
            this.mStagger = 0.0f;
            this.mKeyFramesList = new ArrayList<KeyFrames>();
            this.mTouchResponse = null;
            this.mOnClicks = new ArrayList<TransitionOnClick>();
            this.mAutoTransition = 0;
            this.mDisable = false;
            this.mPathMotionArc = -1;
            this.mLayoutDuringTransition = 0;
            this.mTransitionFlags = 0;
            this.mId = mId;
            this.mMotionScene = mMotionScene;
            this.mConstraintSetStart = mConstraintSetStart;
            this.mConstraintSetEnd = mConstraintSetEnd;
            this.mDuration = mMotionScene.mDefaultDuration;
            this.mLayoutDuringTransition = mMotionScene.mLayoutDuringTransition;
        }
        
        Transition(final MotionScene mMotionScene, final Context context, final XmlPullParser xmlPullParser) {
            this.mId = -1;
            this.mIsAbstract = false;
            this.mConstraintSetEnd = -1;
            this.mConstraintSetStart = -1;
            this.mDefaultInterpolator = 0;
            this.mDefaultInterpolatorString = null;
            this.mDefaultInterpolatorID = -1;
            this.mDuration = 400;
            this.mStagger = 0.0f;
            this.mKeyFramesList = new ArrayList<KeyFrames>();
            this.mTouchResponse = null;
            this.mOnClicks = new ArrayList<TransitionOnClick>();
            this.mAutoTransition = 0;
            this.mDisable = false;
            this.mPathMotionArc = -1;
            this.mLayoutDuringTransition = 0;
            this.mTransitionFlags = 0;
            this.mDuration = mMotionScene.mDefaultDuration;
            this.mLayoutDuringTransition = mMotionScene.mLayoutDuringTransition;
            this.fillFromAttributeList(this.mMotionScene = mMotionScene, context, Xml.asAttributeSet(xmlPullParser));
        }
        
        Transition(final MotionScene mMotionScene, final Transition transition) {
            this.mId = -1;
            this.mIsAbstract = false;
            this.mConstraintSetEnd = -1;
            this.mConstraintSetStart = -1;
            this.mDefaultInterpolator = 0;
            this.mDefaultInterpolatorString = null;
            this.mDefaultInterpolatorID = -1;
            this.mDuration = 400;
            this.mStagger = 0.0f;
            this.mKeyFramesList = new ArrayList<KeyFrames>();
            this.mTouchResponse = null;
            this.mOnClicks = new ArrayList<TransitionOnClick>();
            this.mAutoTransition = 0;
            this.mDisable = false;
            this.mPathMotionArc = -1;
            this.mLayoutDuringTransition = 0;
            this.mTransitionFlags = 0;
            this.mMotionScene = mMotionScene;
            this.mDuration = mMotionScene.mDefaultDuration;
            if (transition != null) {
                this.mPathMotionArc = transition.mPathMotionArc;
                this.mDefaultInterpolator = transition.mDefaultInterpolator;
                this.mDefaultInterpolatorString = transition.mDefaultInterpolatorString;
                this.mDefaultInterpolatorID = transition.mDefaultInterpolatorID;
                this.mDuration = transition.mDuration;
                this.mKeyFramesList = transition.mKeyFramesList;
                this.mStagger = transition.mStagger;
                this.mLayoutDuringTransition = transition.mLayoutDuringTransition;
            }
        }
        
        private void fill(final MotionScene motionScene, final Context context, final TypedArray typedArray) {
            for (int indexCount = typedArray.getIndexCount(), i = 0; i < indexCount; ++i) {
                final int index = typedArray.getIndex(i);
                if (index == R.styleable.Transition_constraintSetEnd) {
                    this.mConstraintSetEnd = typedArray.getResourceId(index, -1);
                    final String resourceTypeName = context.getResources().getResourceTypeName(this.mConstraintSetEnd);
                    if ("layout".equals(resourceTypeName)) {
                        final ConstraintSet set = new ConstraintSet();
                        set.load(context, this.mConstraintSetEnd);
                        motionScene.mConstraintSetMap.append(this.mConstraintSetEnd, (Object)set);
                    }
                    else if ("xml".equals(resourceTypeName)) {
                        this.mConstraintSetEnd = motionScene.parseInclude(context, this.mConstraintSetEnd);
                    }
                }
                else if (index == R.styleable.Transition_constraintSetStart) {
                    this.mConstraintSetStart = typedArray.getResourceId(index, this.mConstraintSetStart);
                    final String resourceTypeName2 = context.getResources().getResourceTypeName(this.mConstraintSetStart);
                    if ("layout".equals(resourceTypeName2)) {
                        final ConstraintSet set2 = new ConstraintSet();
                        set2.load(context, this.mConstraintSetStart);
                        motionScene.mConstraintSetMap.append(this.mConstraintSetStart, (Object)set2);
                    }
                    else if ("xml".equals(resourceTypeName2)) {
                        this.mConstraintSetStart = motionScene.parseInclude(context, this.mConstraintSetStart);
                    }
                }
                else if (index == R.styleable.Transition_motionInterpolator) {
                    final int type = typedArray.peekValue(index).type;
                    if (type == 1) {
                        if ((this.mDefaultInterpolatorID = typedArray.getResourceId(index, -1)) != -1) {
                            this.mDefaultInterpolator = -2;
                        }
                    }
                    else if (type == 3) {
                        final String string = typedArray.getString(index);
                        if ((this.mDefaultInterpolatorString = string) != null) {
                            if (string.indexOf("/") > 0) {
                                this.mDefaultInterpolatorID = typedArray.getResourceId(index, -1);
                                this.mDefaultInterpolator = -2;
                            }
                            else {
                                this.mDefaultInterpolator = -1;
                            }
                        }
                    }
                    else {
                        this.mDefaultInterpolator = typedArray.getInteger(index, this.mDefaultInterpolator);
                    }
                }
                else if (index == R.styleable.Transition_duration) {
                    if ((this.mDuration = typedArray.getInt(index, this.mDuration)) < 8) {
                        this.mDuration = 8;
                    }
                }
                else if (index == R.styleable.Transition_staggered) {
                    this.mStagger = typedArray.getFloat(index, this.mStagger);
                }
                else if (index == R.styleable.Transition_autoTransition) {
                    this.mAutoTransition = typedArray.getInteger(index, this.mAutoTransition);
                }
                else if (index == R.styleable.Transition_android_id) {
                    this.mId = typedArray.getResourceId(index, this.mId);
                }
                else if (index == R.styleable.Transition_transitionDisable) {
                    this.mDisable = typedArray.getBoolean(index, this.mDisable);
                }
                else if (index == R.styleable.Transition_pathMotionArc) {
                    this.mPathMotionArc = typedArray.getInteger(index, -1);
                }
                else if (index == R.styleable.Transition_layoutDuringTransition) {
                    this.mLayoutDuringTransition = typedArray.getInteger(index, 0);
                }
                else if (index == R.styleable.Transition_transitionFlags) {
                    this.mTransitionFlags = typedArray.getInteger(index, 0);
                }
            }
            if (this.mConstraintSetStart == -1) {
                this.mIsAbstract = true;
            }
        }
        
        private void fillFromAttributeList(final MotionScene motionScene, final Context context, final AttributeSet set) {
            final TypedArray obtainStyledAttributes = context.obtainStyledAttributes(set, R.styleable.Transition);
            this.fill(motionScene, context, obtainStyledAttributes);
            obtainStyledAttributes.recycle();
        }
        
        public void addKeyFrame(final KeyFrames e) {
            this.mKeyFramesList.add(e);
        }
        
        public void addOnClick(final int n, final int mMode) {
            for (final TransitionOnClick transitionOnClick : this.mOnClicks) {
                if (transitionOnClick.mTargetId == n) {
                    transitionOnClick.mMode = mMode;
                    return;
                }
            }
            this.mOnClicks.add(new TransitionOnClick(this, n, mMode));
        }
        
        public void addOnClick(final Context context, final XmlPullParser xmlPullParser) {
            this.mOnClicks.add(new TransitionOnClick(context, this, xmlPullParser));
        }
        
        public String debugString(final Context context) {
            String resourceEntryName;
            if (this.mConstraintSetStart == -1) {
                resourceEntryName = "null";
            }
            else {
                resourceEntryName = context.getResources().getResourceEntryName(this.mConstraintSetStart);
            }
            String s;
            if (this.mConstraintSetEnd == -1) {
                final StringBuilder sb = new StringBuilder();
                sb.append(resourceEntryName);
                sb.append(" -> null");
                s = sb.toString();
            }
            else {
                final StringBuilder sb2 = new StringBuilder();
                sb2.append(resourceEntryName);
                sb2.append(" -> ");
                sb2.append(context.getResources().getResourceEntryName(this.mConstraintSetEnd));
                s = sb2.toString();
            }
            return s;
        }
        
        public int getAutoTransition() {
            return this.mAutoTransition;
        }
        
        public int getDuration() {
            return this.mDuration;
        }
        
        public int getEndConstraintSetId() {
            return this.mConstraintSetEnd;
        }
        
        public int getId() {
            return this.mId;
        }
        
        public List<KeyFrames> getKeyFrameList() {
            return this.mKeyFramesList;
        }
        
        public int getLayoutDuringTransition() {
            return this.mLayoutDuringTransition;
        }
        
        public List<TransitionOnClick> getOnClickList() {
            return this.mOnClicks;
        }
        
        public int getPathMotionArc() {
            return this.mPathMotionArc;
        }
        
        public float getStagger() {
            return this.mStagger;
        }
        
        public int getStartConstraintSetId() {
            return this.mConstraintSetStart;
        }
        
        public TouchResponse getTouchResponse() {
            return this.mTouchResponse;
        }
        
        public boolean isEnabled() {
            return this.mDisable ^ true;
        }
        
        public boolean isTransitionFlag(final int n) {
            return (n & this.mTransitionFlags) != 0x0;
        }
        
        public void removeOnClick(final int n) {
            while (true) {
                for (final TransitionOnClick o : this.mOnClicks) {
                    if (o.mTargetId == n) {
                        if (o != null) {
                            this.mOnClicks.remove(o);
                        }
                        return;
                    }
                }
                TransitionOnClick o = null;
                continue;
            }
        }
        
        public void setAutoTransition(final int mAutoTransition) {
            this.mAutoTransition = mAutoTransition;
        }
        
        public void setDuration(final int a) {
            this.mDuration = Math.max(a, 8);
        }
        
        public void setEnable(final boolean enabled) {
            this.setEnabled(enabled);
        }
        
        public void setEnabled(final boolean b) {
            this.mDisable = (b ^ true);
        }
        
        public void setInterpolatorInfo(final int mDefaultInterpolator, final String mDefaultInterpolatorString, final int mDefaultInterpolatorID) {
            this.mDefaultInterpolator = mDefaultInterpolator;
            this.mDefaultInterpolatorString = mDefaultInterpolatorString;
            this.mDefaultInterpolatorID = mDefaultInterpolatorID;
        }
        
        public void setLayoutDuringTransition(final int mLayoutDuringTransition) {
            this.mLayoutDuringTransition = mLayoutDuringTransition;
        }
        
        public void setOnSwipe(final OnSwipe onSwipe) {
            TouchResponse mTouchResponse;
            if (onSwipe == null) {
                mTouchResponse = null;
            }
            else {
                mTouchResponse = new TouchResponse(this.mMotionScene.mMotionLayout, onSwipe);
            }
            this.mTouchResponse = mTouchResponse;
        }
        
        public void setOnTouchUp(final int touchUpMode) {
            final TouchResponse touchResponse = this.getTouchResponse();
            if (touchResponse != null) {
                touchResponse.setTouchUpMode(touchUpMode);
            }
        }
        
        public void setPathMotionArc(final int mPathMotionArc) {
            this.mPathMotionArc = mPathMotionArc;
        }
        
        public void setStagger(final float mStagger) {
            this.mStagger = mStagger;
        }
        
        public void setTransitionFlag(final int mTransitionFlags) {
            this.mTransitionFlags = mTransitionFlags;
        }
        
        public static class TransitionOnClick implements View$OnClickListener
        {
            public static final int ANIM_TOGGLE = 17;
            public static final int ANIM_TO_END = 1;
            public static final int ANIM_TO_START = 16;
            public static final int JUMP_TO_END = 256;
            public static final int JUMP_TO_START = 4096;
            int mMode;
            int mTargetId;
            private final Transition mTransition;
            
            public TransitionOnClick(final Context context, final Transition mTransition, final XmlPullParser xmlPullParser) {
                this.mTargetId = -1;
                this.mMode = 17;
                this.mTransition = mTransition;
                final TypedArray obtainStyledAttributes = context.obtainStyledAttributes(Xml.asAttributeSet(xmlPullParser), R.styleable.OnClick);
                for (int indexCount = obtainStyledAttributes.getIndexCount(), i = 0; i < indexCount; ++i) {
                    final int index = obtainStyledAttributes.getIndex(i);
                    if (index == R.styleable.OnClick_targetId) {
                        this.mTargetId = obtainStyledAttributes.getResourceId(index, this.mTargetId);
                    }
                    else if (index == R.styleable.OnClick_clickAction) {
                        this.mMode = obtainStyledAttributes.getInt(index, this.mMode);
                    }
                }
                obtainStyledAttributes.recycle();
            }
            
            public TransitionOnClick(final Transition mTransition, final int mTargetId, final int mMode) {
                this.mTransition = mTransition;
                this.mTargetId = mTargetId;
                this.mMode = mMode;
            }
            
            public void addOnClickListeners(MotionLayout viewById, final int n, final Transition transition) {
                final int mTargetId = this.mTargetId;
                if (mTargetId != -1) {
                    viewById = (MotionLayout)((View)viewById).findViewById(mTargetId);
                }
                if (viewById == null) {
                    final StringBuilder sb = new StringBuilder();
                    sb.append("OnClick could not find id ");
                    sb.append(this.mTargetId);
                    return;
                }
                final int access$100 = transition.mConstraintSetStart;
                final int access$101 = transition.mConstraintSetEnd;
                if (access$100 == -1) {
                    ((View)viewById).setOnClickListener((View$OnClickListener)this);
                    return;
                }
                final int mMode = this.mMode;
                final boolean b = false;
                final boolean b2 = (mMode & 0x1) != 0x0 && n == access$100;
                final boolean b3 = (mMode & 0x100) != 0x0 && n == access$100;
                final boolean b4 = (mMode & 0x1) != 0x0 && n == access$100;
                final boolean b5 = (mMode & 0x10) != 0x0 && n == access$101;
                boolean b6 = b;
                if ((mMode & 0x1000) != 0x0) {
                    b6 = b;
                    if (n == access$101) {
                        b6 = true;
                    }
                }
                if (b4 | (b2 | b3) | b5 | b6) {
                    ((View)viewById).setOnClickListener((View$OnClickListener)this);
                }
            }
            
            boolean isTransitionViable(final Transition transition, final MotionLayout motionLayout) {
                final Transition mTransition = this.mTransition;
                final boolean b = true;
                boolean b2 = true;
                if (mTransition == transition) {
                    return true;
                }
                final int access$000 = mTransition.mConstraintSetEnd;
                final int access$2 = this.mTransition.mConstraintSetStart;
                if (access$2 == -1) {
                    if (motionLayout.mCurrentState == access$000) {
                        b2 = false;
                    }
                    return b2;
                }
                final int mCurrentState = motionLayout.mCurrentState;
                boolean b3 = b;
                if (mCurrentState != access$2) {
                    b3 = (mCurrentState == access$000 && b);
                }
                return b3;
            }
            
            public void onClick(final View view) {
                final MotionLayout access$700 = this.mTransition.mMotionScene.mMotionLayout;
                if (!access$700.isInteractionEnabled()) {
                    return;
                }
                if (this.mTransition.mConstraintSetStart != -1) {
                    final Transition mCurrentTransition = this.mTransition.mMotionScene.mCurrentTransition;
                    final int mMode = this.mMode;
                    final int n = 0;
                    final boolean b = (mMode & 0x1) != 0x0 || (mMode & 0x100) != 0x0;
                    final boolean b2 = (mMode & 0x10) != 0x0 || (mMode & 0x1000) != 0x0;
                    boolean b3;
                    int n2;
                    if (b && b2) {
                        final Transition mCurrentTransition2 = this.mTransition.mMotionScene.mCurrentTransition;
                        final Transition mTransition = this.mTransition;
                        if (mCurrentTransition2 != mTransition) {
                            access$700.setTransition(mTransition);
                        }
                        b3 = b2;
                        n2 = n;
                        if (access$700.getCurrentState() != access$700.getEndState()) {
                            if (access$700.getProgress() > 0.5f) {
                                b3 = b2;
                                n2 = n;
                            }
                            else {
                                b3 = false;
                                n2 = (b ? 1 : 0);
                            }
                        }
                    }
                    else {
                        n2 = (b ? 1 : 0);
                        b3 = b2;
                    }
                    if (this.isTransitionViable(mCurrentTransition, access$700)) {
                        if (n2 != 0 && (this.mMode & 0x1) != 0x0) {
                            access$700.setTransition(this.mTransition);
                            access$700.transitionToEnd();
                        }
                        else if (b3 && (this.mMode & 0x10) != 0x0) {
                            access$700.setTransition(this.mTransition);
                            access$700.transitionToStart();
                        }
                        else if (n2 != 0 && (this.mMode & 0x100) != 0x0) {
                            access$700.setTransition(this.mTransition);
                            access$700.setProgress(1.0f);
                        }
                        else if (b3 && (this.mMode & 0x1000) != 0x0) {
                            access$700.setTransition(this.mTransition);
                            access$700.setProgress(0.0f);
                        }
                    }
                    return;
                }
                final int currentState = access$700.getCurrentState();
                if (currentState == -1) {
                    access$700.transitionToState(this.mTransition.mConstraintSetEnd);
                    return;
                }
                final Transition transition = new Transition(this.mTransition.mMotionScene, this.mTransition);
                transition.mConstraintSetStart = currentState;
                transition.mConstraintSetEnd = this.mTransition.mConstraintSetEnd;
                access$700.setTransition(transition);
                access$700.transitionToEnd();
            }
            
            public void removeOnClickListeners(final MotionLayout motionLayout) {
                final int mTargetId = this.mTargetId;
                if (mTargetId == -1) {
                    return;
                }
                final View viewById = ((View)motionLayout).findViewById(mTargetId);
                if (viewById == null) {
                    final StringBuilder sb = new StringBuilder();
                    sb.append(" (*)  could not find id ");
                    sb.append(this.mTargetId);
                    return;
                }
                viewById.setOnClickListener((View$OnClickListener)null);
            }
        }
    }
}

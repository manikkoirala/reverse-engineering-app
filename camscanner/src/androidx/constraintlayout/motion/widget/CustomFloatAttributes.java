// 
// Decompiled by Procyon v0.6.0
// 

package androidx.constraintlayout.motion.widget;

public interface CustomFloatAttributes
{
    float get(final String p0);
    
    String[] getListOfAttributes();
    
    void set(final String p0, final float p1);
}

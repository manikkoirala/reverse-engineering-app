// 
// Decompiled by Procyon v0.6.0
// 

package androidx.constraintlayout.motion.utils;

import android.view.View;

public class ViewState
{
    public int bottom;
    public int left;
    public int right;
    public float rotation;
    public int top;
    
    public void getState(final View view) {
        this.left = view.getLeft();
        this.top = view.getTop();
        this.right = view.getRight();
        this.bottom = view.getBottom();
        this.rotation = view.getRotation();
    }
    
    public int height() {
        return this.bottom - this.top;
    }
    
    public int width() {
        return this.right - this.left;
    }
}

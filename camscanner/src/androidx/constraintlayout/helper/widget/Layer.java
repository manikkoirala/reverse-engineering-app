// 
// Decompiled by Procyon v0.6.0
// 

package androidx.constraintlayout.helper.widget;

import androidx.constraintlayout.core.widgets.ConstraintWidget;
import android.content.res.TypedArray;
import androidx.constraintlayout.widget.R;
import android.util.AttributeSet;
import android.content.Context;
import android.view.View;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.constraintlayout.widget.ConstraintHelper;

public class Layer extends ConstraintHelper
{
    private static final String TAG = "Layer";
    private boolean mApplyElevationOnAttach;
    private boolean mApplyVisibilityOnAttach;
    protected float mComputedCenterX;
    protected float mComputedCenterY;
    protected float mComputedMaxX;
    protected float mComputedMaxY;
    protected float mComputedMinX;
    protected float mComputedMinY;
    ConstraintLayout mContainer;
    private float mGroupRotateAngle;
    boolean mNeedBounds;
    private float mRotationCenterX;
    private float mRotationCenterY;
    private float mScaleX;
    private float mScaleY;
    private float mShiftX;
    private float mShiftY;
    View[] mViews;
    
    public Layer(final Context context) {
        super(context);
        this.mRotationCenterX = Float.NaN;
        this.mRotationCenterY = Float.NaN;
        this.mGroupRotateAngle = Float.NaN;
        this.mScaleX = 1.0f;
        this.mScaleY = 1.0f;
        this.mComputedCenterX = Float.NaN;
        this.mComputedCenterY = Float.NaN;
        this.mComputedMaxX = Float.NaN;
        this.mComputedMaxY = Float.NaN;
        this.mComputedMinX = Float.NaN;
        this.mComputedMinY = Float.NaN;
        this.mNeedBounds = true;
        this.mViews = null;
        this.mShiftX = 0.0f;
        this.mShiftY = 0.0f;
    }
    
    public Layer(final Context context, final AttributeSet set) {
        super(context, set);
        this.mRotationCenterX = Float.NaN;
        this.mRotationCenterY = Float.NaN;
        this.mGroupRotateAngle = Float.NaN;
        this.mScaleX = 1.0f;
        this.mScaleY = 1.0f;
        this.mComputedCenterX = Float.NaN;
        this.mComputedCenterY = Float.NaN;
        this.mComputedMaxX = Float.NaN;
        this.mComputedMaxY = Float.NaN;
        this.mComputedMinX = Float.NaN;
        this.mComputedMinY = Float.NaN;
        this.mNeedBounds = true;
        this.mViews = null;
        this.mShiftX = 0.0f;
        this.mShiftY = 0.0f;
    }
    
    public Layer(final Context context, final AttributeSet set, final int n) {
        super(context, set, n);
        this.mRotationCenterX = Float.NaN;
        this.mRotationCenterY = Float.NaN;
        this.mGroupRotateAngle = Float.NaN;
        this.mScaleX = 1.0f;
        this.mScaleY = 1.0f;
        this.mComputedCenterX = Float.NaN;
        this.mComputedCenterY = Float.NaN;
        this.mComputedMaxX = Float.NaN;
        this.mComputedMaxY = Float.NaN;
        this.mComputedMinX = Float.NaN;
        this.mComputedMinY = Float.NaN;
        this.mNeedBounds = true;
        this.mViews = null;
        this.mShiftX = 0.0f;
        this.mShiftY = 0.0f;
    }
    
    private void reCacheViews() {
        if (this.mContainer == null) {
            return;
        }
        final int mCount = super.mCount;
        if (mCount == 0) {
            return;
        }
        final View[] mViews = this.mViews;
        if (mViews == null || mViews.length != mCount) {
            this.mViews = new View[mCount];
        }
        for (int i = 0; i < super.mCount; ++i) {
            this.mViews[i] = this.mContainer.getViewById(super.mIds[i]);
        }
    }
    
    private void transform() {
        if (this.mContainer == null) {
            return;
        }
        if (this.mViews == null) {
            this.reCacheViews();
        }
        this.calcCenters();
        double radians;
        if (Float.isNaN(this.mGroupRotateAngle)) {
            radians = 0.0;
        }
        else {
            radians = Math.toRadians(this.mGroupRotateAngle);
        }
        final float n = (float)Math.sin(radians);
        final float n2 = (float)Math.cos(radians);
        final float mScaleX = this.mScaleX;
        final float mScaleY = this.mScaleY;
        final float n3 = -mScaleY;
        for (int i = 0; i < super.mCount; ++i) {
            final View view = this.mViews[i];
            final int n4 = (view.getLeft() + view.getRight()) / 2;
            final int n5 = (view.getTop() + view.getBottom()) / 2;
            final float n6 = n4 - this.mComputedCenterX;
            final float n7 = n5 - this.mComputedCenterY;
            final float mShiftX = this.mShiftX;
            final float mShiftY = this.mShiftY;
            view.setTranslationX(mScaleX * n2 * n6 + n3 * n * n7 - n6 + mShiftX);
            view.setTranslationY(n6 * (mScaleX * n) + mScaleY * n2 * n7 - n7 + mShiftY);
            view.setScaleY(this.mScaleY);
            view.setScaleX(this.mScaleX);
            if (!Float.isNaN(this.mGroupRotateAngle)) {
                view.setRotation(this.mGroupRotateAngle);
            }
        }
    }
    
    @Override
    protected void applyLayoutFeaturesInConstraintSet(final ConstraintLayout constraintLayout) {
        this.applyLayoutFeatures(constraintLayout);
    }
    
    protected void calcCenters() {
        if (this.mContainer == null) {
            return;
        }
        if (!this.mNeedBounds && !Float.isNaN(this.mComputedCenterX) && !Float.isNaN(this.mComputedCenterY)) {
            return;
        }
        if (!Float.isNaN(this.mRotationCenterX) && !Float.isNaN(this.mRotationCenterY)) {
            this.mComputedCenterY = this.mRotationCenterY;
            this.mComputedCenterX = this.mRotationCenterX;
        }
        else {
            final View[] views = this.getViews(this.mContainer);
            int i = 0;
            int a = views[0].getLeft();
            int a2 = views[0].getTop();
            int a3 = views[0].getRight();
            int a4 = views[0].getBottom();
            while (i < super.mCount) {
                final View view = views[i];
                a = Math.min(a, view.getLeft());
                a2 = Math.min(a2, view.getTop());
                a3 = Math.max(a3, view.getRight());
                a4 = Math.max(a4, view.getBottom());
                ++i;
            }
            this.mComputedMaxX = (float)a3;
            this.mComputedMaxY = (float)a4;
            this.mComputedMinX = (float)a;
            this.mComputedMinY = (float)a2;
            if (Float.isNaN(this.mRotationCenterX)) {
                this.mComputedCenterX = (float)((a + a3) / 2);
            }
            else {
                this.mComputedCenterX = this.mRotationCenterX;
            }
            if (Float.isNaN(this.mRotationCenterY)) {
                this.mComputedCenterY = (float)((a2 + a4) / 2);
            }
            else {
                this.mComputedCenterY = this.mRotationCenterY;
            }
        }
    }
    
    @Override
    protected void init(final AttributeSet set) {
        super.init(set);
        int i = 0;
        super.mUseViewMeasure = false;
        if (set != null) {
            TypedArray obtainStyledAttributes;
            for (obtainStyledAttributes = this.getContext().obtainStyledAttributes(set, R.styleable.ConstraintLayout_Layout); i < obtainStyledAttributes.getIndexCount(); ++i) {
                final int index = obtainStyledAttributes.getIndex(i);
                if (index == R.styleable.ConstraintLayout_Layout_android_visibility) {
                    this.mApplyVisibilityOnAttach = true;
                }
                else if (index == R.styleable.ConstraintLayout_Layout_android_elevation) {
                    this.mApplyElevationOnAttach = true;
                }
            }
            obtainStyledAttributes.recycle();
        }
    }
    
    @Override
    protected void onAttachedToWindow() {
        super.onAttachedToWindow();
        this.mContainer = (ConstraintLayout)this.getParent();
        if (this.mApplyVisibilityOnAttach || this.mApplyElevationOnAttach) {
            final int visibility = this.getVisibility();
            final float elevation = this.getElevation();
            for (int i = 0; i < super.mCount; ++i) {
                final View viewById = this.mContainer.getViewById(super.mIds[i]);
                if (viewById != null) {
                    if (this.mApplyVisibilityOnAttach) {
                        viewById.setVisibility(visibility);
                    }
                    if (this.mApplyElevationOnAttach && elevation > 0.0f) {
                        viewById.setTranslationZ(viewById.getTranslationZ() + elevation);
                    }
                }
            }
        }
    }
    
    public void setElevation(final float elevation) {
        super.setElevation(elevation);
        this.applyLayoutFeatures();
    }
    
    public void setPivotX(final float mRotationCenterX) {
        this.mRotationCenterX = mRotationCenterX;
        this.transform();
    }
    
    public void setPivotY(final float mRotationCenterY) {
        this.mRotationCenterY = mRotationCenterY;
        this.transform();
    }
    
    public void setRotation(final float mGroupRotateAngle) {
        this.mGroupRotateAngle = mGroupRotateAngle;
        this.transform();
    }
    
    public void setScaleX(final float mScaleX) {
        this.mScaleX = mScaleX;
        this.transform();
    }
    
    public void setScaleY(final float mScaleY) {
        this.mScaleY = mScaleY;
        this.transform();
    }
    
    public void setTranslationX(final float mShiftX) {
        this.mShiftX = mShiftX;
        this.transform();
    }
    
    public void setTranslationY(final float mShiftY) {
        this.mShiftY = mShiftY;
        this.transform();
    }
    
    public void setVisibility(final int visibility) {
        super.setVisibility(visibility);
        this.applyLayoutFeatures();
    }
    
    @Override
    public void updatePostLayout(final ConstraintLayout constraintLayout) {
        this.reCacheViews();
        this.mComputedCenterX = Float.NaN;
        this.mComputedCenterY = Float.NaN;
        final ConstraintWidget constraintWidget = ((ConstraintLayout.LayoutParams)this.getLayoutParams()).getConstraintWidget();
        constraintWidget.setWidth(0);
        constraintWidget.setHeight(0);
        this.calcCenters();
        this.layout((int)this.mComputedMinX - this.getPaddingLeft(), (int)this.mComputedMinY - this.getPaddingTop(), (int)this.mComputedMaxX + this.getPaddingRight(), (int)this.mComputedMaxY + this.getPaddingBottom());
        this.transform();
    }
    
    @Override
    public void updatePreDraw(final ConstraintLayout mContainer) {
        this.mContainer = mContainer;
        final float rotation = this.getRotation();
        if (rotation == 0.0f) {
            if (!Float.isNaN(this.mGroupRotateAngle)) {
                this.mGroupRotateAngle = rotation;
            }
        }
        else {
            this.mGroupRotateAngle = rotation;
        }
    }
}

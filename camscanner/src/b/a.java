// 
// Decompiled by Procyon v0.6.0
// 

package b;

import com.intsig.document.nativelib.PDFium$CharItem;
import com.intsig.document.nativelib.PDFium$SearchResultCallback;
import com.intsig.document.DocumentLoader$d;
import java.io.OutputStream;
import android.graphics.Picture;
import android.graphics.Bitmap$Config;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.util.Iterator;
import com.intsig.document.nativelib.PDFium$Signature;
import a.e;
import android.util.Size;
import a.d;
import a.c;
import java.util.HashMap;
import java.util.Collection;
import java.util.ArrayList;
import a.b;
import com.intsig.document.DocumentLoader$c;
import android.graphics.Rect;
import android.graphics.Bitmap;
import com.intsig.document.DocumentLoader$b;
import android.graphics.RectF;
import com.intsig.document.DocumentLoader$a;
import com.intsig.document.nativelib.PDFium;
import android.content.Context;
import com.intsig.document.DocumentLoader;

public final class a implements DocumentLoader
{
    public long \u3007080;
    public byte[] \u3007o00\u3007\u3007Oo;
    
    public a(final Context context) {
        this.\u3007080 = 0L;
        PDFium.InitLibrary(context);
    }
    
    public final int AddAnnotation(final int n, final int n2, final float[] array, final float n3, final int n4) {
        final long \u3007080 = this.\u3007080;
        if (n2 == 15) {
            PDFium.CreateInkAnnotation(\u3007080, n, array, n4, n3, "");
        }
        else {
            PDFium.CreateAnnotation(\u3007080, n, n2, array, n4);
        }
        return 0;
    }
    
    public final int AddShapeAnnotation(final DocumentLoader$a documentLoader$a) {
        final long \u3007080 = this.\u3007080;
        final int pageNum = ((b)documentLoader$a).pageNum;
        final int type = ((b)documentLoader$a).type;
        final String text = ((b)documentLoader$a).text;
        final RectF rect = ((b)documentLoader$a).rect;
        final float left = rect.left;
        final float top = rect.top;
        final float right = rect.right;
        final float bottom = rect.bottom;
        final float \u3007o00\u3007\u3007Oo = documentLoader$a.\u3007o00\u3007\u3007Oo;
        final int \u300781 = documentLoader$a.\u3007080;
        return PDFium.CreateTextAnnotation(\u3007080, pageNum, type, text, left, top, right, bottom, \u3007o00\u3007\u3007Oo, \u300781, \u300781);
    }
    
    public final int AddStampAnnotation(final DocumentLoader$b documentLoader$b) {
        final Bitmap \u3007080 = documentLoader$b.\u3007080;
        if (\u3007080 != null) {
            final long \u300781 = this.\u3007080;
            final int pageNum = ((b)documentLoader$b).pageNum;
            final RectF rect = ((b)documentLoader$b).rect;
            PDFium.InsertImageStamp(\u300781, pageNum, \u3007080, rect.left, rect.top, rect.right, rect.bottom, documentLoader$b.\u3007o00\u3007\u3007Oo);
        }
        return -1;
    }
    
    public final int AddTextAnnotation(final int n, final Rect rect, final String s, final float n2, final int n3, final boolean b) {
        if (b) {
            PDFium.InsertText0(this.\u3007080, n, s, (float)rect.left, (float)rect.top, "Helvetica", n2, -1711341568, 0);
        }
        else {
            PDFium.CreateTextAnnotation(this.\u3007080, n, 1, s, (float)rect.left, (float)rect.top, (float)rect.right, (float)rect.bottom, 0.0f, n3, 0);
        }
        return 0;
    }
    
    public final int AddTextAnnotation(final DocumentLoader$c documentLoader$c) {
        final long \u3007080 = this.\u3007080;
        final int pageNum = ((b)documentLoader$c).pageNum;
        final String text = ((b)documentLoader$c).text;
        final String \u3007o00\u3007\u3007Oo = documentLoader$c.\u3007o00\u3007\u3007Oo;
        final float \u300781 = documentLoader$c.\u3007080;
        final int \u3007o\u3007 = documentLoader$c.\u3007o\u3007;
        final RectF rect = ((b)documentLoader$c).rect;
        PDFium.InsertTextStamp(\u3007080, pageNum, text, \u3007o00\u3007\u3007Oo, \u300781, \u3007o\u3007, rect.left, rect.top, rect.right, rect.bottom, documentLoader$c.O8);
        return 0;
    }
    
    public final int AddWatermark(final String s, final String s2, final int n, final int n2) {
        if (s2 != null) {
            final String s3 = s2;
            if (!s2.isEmpty()) {
                return PDFium.InsertWatermark(this.\u3007080, s, s3, n, n2);
            }
        }
        final String s3 = "Helvetica";
        return PDFium.InsertWatermark(this.\u3007080, s, s3, n, n2);
    }
    
    public final void Close() {
        final long \u3007080 = this.\u3007080;
        if (\u3007080 != 0L) {
            this.\u3007080 = 0L;
            PDFium.CloseDocument(\u3007080);
        }
        if (this.\u3007o00\u3007\u3007Oo != null) {
            this.\u3007o00\u3007\u3007Oo = null;
        }
    }
    
    public final boolean DeleteAnnot(final b b) {
        PDFium.DeleteAnnot(this.\u3007080, b.pageNum, b.id);
        return false;
    }
    
    public final ArrayList<b> GetAnnotations(final int n) {
        final ArrayList c = new ArrayList();
        PDFium.GetAnnotations(this.\u3007080, n, c);
        final ArrayList list = new ArrayList();
        list.addAll(c);
        return list;
    }
    
    public final long GetDocumentHandler() {
        return this.\u3007080;
    }
    
    public final HashMap<String, String> GetMetaInfo() {
        final HashMap hashMap = new HashMap();
        for (int i = 0; i < 7; ++i) {
            final String s = (new String[] { "Author", "Title", "ModDate", "Creator", "CreationDate", "Producer", "Subject" })[i];
            final String getMetaInfo = PDFium.GetMetaInfo(this.\u3007080, s);
            if (getMetaInfo != null) {
                hashMap.put(s, getMetaInfo);
                final StringBuilder sb = new StringBuilder();
                sb.append("meta ");
                sb.append(s);
                sb.append(":");
                sb.append(getMetaInfo);
            }
        }
        return hashMap;
    }
    
    public final ArrayList<c> GetOutlines() {
        final ArrayList c = new ArrayList();
        final long currentTimeMillis = System.currentTimeMillis();
        PDFium.GetOutlines(this.\u3007080, c);
        final long currentTimeMillis2 = System.currentTimeMillis();
        final StringBuilder \u3007080 = a.a.\u3007080("GetOutlines ");
        \u3007080.append(currentTimeMillis2 - currentTimeMillis);
        \u3007080.append("ms");
        final ArrayList list = new ArrayList();
        list.addAll(c);
        return list;
    }
    
    public final int GetPageCount() {
        return PDFium.GetPageCount(this.\u3007080);
    }
    
    public final ArrayList<d> GetPageLinks(final int n) {
        final ArrayList c = new ArrayList();
        final long currentTimeMillis = System.currentTimeMillis();
        PDFium.GetPageUrlLinks(this.\u3007080, n, c);
        final ArrayList list = new ArrayList();
        list.addAll(c);
        final long currentTimeMillis2 = System.currentTimeMillis();
        final StringBuilder \u3007080 = a.a.\u3007080("GetPageLinks ");
        \u3007080.append(list.size());
        \u3007080.append(" ");
        \u3007080.append(currentTimeMillis2 - currentTimeMillis);
        \u3007080.append("ms");
        return list;
    }
    
    public final Size GetPageSize(final int n) {
        final double[] array = new double[2];
        PDFium.GetPageSize(this.\u3007080, n, 72, array);
        return new Size((int)array[0], (int)array[1]);
    }
    
    public final ArrayList<e> GetSignatures() {
        final ArrayList list = new ArrayList();
        final ArrayList c = new ArrayList();
        PDFium.GetSignatures(this.\u3007080, c);
        final StringBuilder sb = new StringBuilder();
        sb.append("GetSignatures ");
        sb.append(c.size());
        for (final PDFium$Signature pdFium$Signature : c) {
            final StringBuilder \u3007080 = a.a.\u3007080("\tSignature ");
            \u3007080.append(((e)pdFium$Signature).reason);
            \u3007080.append(",");
            \u3007080.append(((e)pdFium$Signature).date);
            \u3007080.append(",");
            \u3007080.append(((e)pdFium$Signature).subFilter);
            \u3007080.append(",");
            \u3007080.append(((e)pdFium$Signature).content.length);
        }
        list.addAll(c);
        return list;
    }
    
    public final int InsertImage(final int n, final Bitmap bitmap, final RectF rectF, final int n2) {
        return PDFium.InsertImageBitmap(this.\u3007080, n, bitmap, rectF.left, rectF.top, rectF.width(), rectF.height(), n2);
    }
    
    public final int InsertImageWatermark(final String s, final Bitmap bitmap, final RectF rectF, final int n) {
        return PDFium.InsertImageWatermark(this.\u3007080, s, bitmap, rectF.left, rectF.top, rectF.width(), rectF.height(), n);
    }
    
    public final int OpenDocument(final InputStream inputStream, final String s) {
        final ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        final byte[] array = new byte[1024];
        try {
            while (true) {
                final int read = inputStream.read(array);
                if (read < 1) {
                    break;
                }
                byteArrayOutputStream.write(array, 0, read);
            }
        }
        catch (final Exception ex) {
            ex.printStackTrace();
        }
        final byte[] byteArray = byteArrayOutputStream.toByteArray();
        this.\u3007o00\u3007\u3007Oo = byteArray;
        this.\u3007080 = PDFium.LoadMemDocument(byteArray, s);
        a.a.\u3007080("PdfLoader ").append(this.\u3007080);
        if (this.\u3007080 != 0L) {
            return 0;
        }
        if (PDFium.GetLastError() == 4) {
            return -2;
        }
        return -1;
    }
    
    public final int OpenDocument(final String s, final String s2) {
        this.\u3007080 = PDFium.LoadDocument(s, s2);
        a.a.\u3007080("PdfLoader ").append(this.\u3007080);
        if (this.\u3007080 != 0L) {
            return 0;
        }
        final int getLastError = PDFium.GetLastError();
        if (getLastError == 4) {
            return -2;
        }
        if (getLastError == 100) {
            return -100;
        }
        return -1;
    }
    
    public final Bitmap RenderPage(final int n, final int i, final int j, final RectF rectF, final boolean b, final int n2) {
        final long currentTimeMillis = System.currentTimeMillis();
        final StringBuilder sb = new StringBuilder();
        sb.append("RenderPage-Bitmap expect size: ");
        sb.append(i);
        sb.append(",");
        sb.append(j);
        Bitmap bitmap;
        long n4;
        int round;
        int round2;
        Bitmap bitmap2;
        if (rectF != null) {
            final float n3 = i / (float)this.GetPageSize(n).getWidth();
            bitmap = Bitmap.createBitmap(Math.round(rectF.width() * n3), Math.round(rectF.height() * n3), Bitmap$Config.ARGB_8888);
            n4 = this.\u3007080;
            round = Math.round(-rectF.left * n3);
            round2 = Math.round(-rectF.top * n3);
            bitmap2 = bitmap;
        }
        else {
            bitmap = Bitmap.createBitmap(i, j, Bitmap$Config.ARGB_8888);
            n4 = this.\u3007080;
            round = 0;
            round2 = 0;
            bitmap2 = bitmap;
        }
        PDFium.RenderPageJBitmap(n4, n, bitmap2, round, round2, i, j, n2, b);
        a.a.\u3007080("RenderPage-Bitmap ").append(System.currentTimeMillis() - currentTimeMillis);
        return bitmap;
    }
    
    public final Picture RenderPage(final int n, final boolean b, final float n2, final int n3) {
        return null;
    }
    
    public final int SaveAs(final String s, final String s2) {
        PDFium.SetPassword(this.\u3007080, s2, s2);
        final int saveAs = PDFium.SaveAs(this.\u3007080, s);
        final int getLastError = PDFium.GetLastError();
        final StringBuilder sb = new StringBuilder();
        sb.append("PDFium.SaveAs ret ");
        sb.append(saveAs);
        sb.append(" err ");
        sb.append(getLastError);
        return saveAs;
    }
    
    public final int SaveTo(final OutputStream outputStream, final String s) {
        PDFium.SetPassword(this.\u3007080, s, s);
        final int saveTo = PDFium.SaveTo(this.\u3007080, outputStream);
        final int getLastError = PDFium.GetLastError();
        final StringBuilder sb = new StringBuilder();
        sb.append("PDFium.SaveAs ret ");
        sb.append(saveTo);
        sb.append(" err ");
        sb.append(getLastError);
        return saveTo;
    }
    
    public final ArrayList<DocumentLoader$d> SearchText(final String s, final int n) {
        final ArrayList list = new ArrayList();
        PDFium.SearchText(this.\u3007080, s, n, (PDFium$SearchResultCallback)new PDFium$SearchResultCallback(this, list) {
            public final ArrayList \u3007080;
            public final a \u3007o00\u3007\u3007Oo;
            
            public final void onResult(final String str, final int n, final int n2, final float n3, final float n4, final float n5, final float n6) {
                final StringBuilder sb = new StringBuilder();
                sb.append("onResult: ");
                sb.append(str);
                this.\u3007080.add(new DocumentLoader$d(str, n2, n, new RectF(n3, n4, n5, n6)));
            }
        });
        return list;
    }
    
    public final ArrayList<DocumentLoader$d> SelectText(final int n, int n2, int n3) {
        final ArrayList list = new ArrayList();
        final long \u3007080 = this.\u3007080;
        if (n2 < n3) {
            final int n4 = n2;
            n2 = n3;
            n3 = n4;
        }
        PDFium.SelectText(\u3007080, n, n3, n2, list);
        final ArrayList<DocumentLoader$d> list2 = new ArrayList<DocumentLoader$d>();
        for (final PDFium$CharItem pdFium$CharItem : list) {
            final StringBuilder sb = new StringBuilder();
            sb.append(pdFium$CharItem.ch);
            sb.append("");
            list2.add(new DocumentLoader$d(sb.toString(), pdFium$CharItem.index, pdFium$CharItem.pageNum, new RectF(pdFium$CharItem.left, pdFium$CharItem.top, pdFium$CharItem.right, pdFium$CharItem.bottom)));
        }
        return list2;
    }
    
    public final DocumentLoader$d TextAtPos(final int n, final float n2, final float n3, final boolean b) {
        final DocumentLoader$d[] array = { null };
        PDFium.TextAtPos(this.\u3007080, n, n2, n3, b, (PDFium$SearchResultCallback)new PDFium$SearchResultCallback(this, array) {
            public final DocumentLoader$d[] \u3007080;
            public final a \u3007o00\u3007\u3007Oo;
            
            public final void onResult(final String s, final int n, final int n2, final float n3, final float n4, final float n5, final float n6) {
                this.\u3007080[0] = new DocumentLoader$d(s, n2, n, new RectF(n3, n4, n5, n6));
            }
        });
        return array[0];
    }
    
    public final void finalize() {
        super.finalize();
        PDFium.ReleaseLibrary();
    }
}

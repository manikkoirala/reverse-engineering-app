// 
// Decompiled by Procyon v0.6.0
// 

package b.c.a.a.a.a;

import com.bytedance.sdk.openadsdk.core.z;
import java.util.HashMap;
import com.bytedance.sdk.openadsdk.utils.a0;
import java.util.Map;
import java.util.Iterator;
import android.app.Activity;
import android.content.pm.ResolveInfo;
import android.content.Intent;
import android.net.Uri;
import android.text.TextUtils;
import org.json.JSONException;
import org.json.JSONObject;
import com.bytedance.sdk.openadsdk.core.o;
import com.bytedance.sdk.component.utils.m;
import com.bytedance.sdk.openadsdk.core.f0.q;
import android.content.Context;
import java.lang.ref.WeakReference;
import java.util.concurrent.atomic.AtomicBoolean;

public class b implements c
{
    protected static Boolean oO80;
    protected String O8;
    protected boolean Oo08;
    protected final AtomicBoolean o\u30070;
    private WeakReference<Context> \u3007080;
    protected com.bytedance.sdk.openadsdk.core.f0.c \u3007o00\u3007\u3007Oo;
    protected q \u3007o\u3007;
    protected int \u3007\u3007888;
    
    public b(final Context referent, final q \u3007o\u3007, final String s) {
        this.Oo08 = false;
        this.o\u30070 = new AtomicBoolean(false);
        this.\u3007080 = new WeakReference<Context>(referent);
        this.\u3007o\u3007 = \u3007o\u3007;
        this.\u3007o00\u3007\u3007Oo = \u3007o\u3007.h();
        this.O8 = s;
        final StringBuilder sb = new StringBuilder();
        sb.append("====tag===");
        sb.append(s);
        m.c("GPDownLoader", new Object[] { s, sb.toString() });
        if (o.a() == null) {
            o.a(referent);
        }
    }
    
    public static boolean O8(final Context context, final String str, final String str2, String replace, final q q) {
        final JSONObject jsonObject = new JSONObject();
        try {
            final Boolean oo80 = b.oO80;
            String s;
            if (oo80 != null && oo80) {
                s = "app";
            }
            else {
                s = "webview";
            }
            jsonObject.put("storeOpenType", (Object)s);
        }
        catch (final JSONException ex) {
            m.b("GPDownLoader", "gotoGooglePlayByPackageNameAndUrl json error", (Throwable)ex);
        }
        if (!TextUtils.isEmpty((CharSequence)replace) && replace.contains("_landingpage")) {
            replace = replace.replace("_landingpage", "");
        }
        if (!TextUtils.isEmpty((CharSequence)str) && str.contains("play.google.com/store/apps/details?id=")) {
            try {
                final Intent intent = new Intent("android.intent.action.VIEW", Uri.parse(str));
                intent.setFlags(268435456);
                context.startActivity(intent);
                m.d("GPDownLoader", "Goto Google Play");
                final StringBuilder sb = new StringBuilder();
                sb.append("download_url is : ->");
                sb.append(str);
                m.d("GPDownLoader", sb.toString());
                com.bytedance.sdk.openadsdk.d.c.b(q, replace, "store_open", jsonObject);
                m.c("GPDownLoader", new Object[] { replace, jsonObject.toString() });
                return true;
            }
            finally {}
        }
        if (context == null || str2 == null) {
            return false;
        }
        if (TextUtils.isEmpty((CharSequence)str2)) {
            return false;
        }
        final StringBuilder sb2 = new StringBuilder();
        sb2.append("gotoGooglePlay :market://details?id=");
        sb2.append(str2);
        m.d("GPDownLoader", sb2.toString());
        while (true) {
            try {
                final Intent intent2 = new Intent("android.intent.action.VIEW");
                final StringBuilder sb3 = new StringBuilder();
                sb3.append("market://details?id=");
                sb3.append(str2);
                final Uri parse = Uri.parse(sb3.toString());
                intent2.setData(parse);
                final Iterator iterator = context.getPackageManager().queryIntentActivities(intent2, 65536).iterator();
                while (iterator.hasNext()) {
                    if (((ResolveInfo)iterator.next()).activityInfo.packageName.equals("com.android.vending")) {
                        if (context.getPackageManager().getLaunchIntentForPackage("com.android.vending") == null) {
                            continue;
                        }
                        final Intent intent3 = new Intent("android.intent.action.VIEW");
                        intent3.setData(parse);
                        intent3.setPackage("com.android.vending");
                        if (!(context instanceof Activity)) {
                            intent3.setFlags(268435456);
                        }
                        context.startActivity(intent3);
                        jsonObject.put("storeOpenType", (Object)"app");
                        com.bytedance.sdk.openadsdk.d.c.b(q, replace, "store_open", jsonObject);
                        return true;
                    }
                }
                try {
                    final StringBuilder sb4 = new StringBuilder();
                    sb4.append("https://play.google.com/store/apps/details?id=");
                    sb4.append(str2);
                    final Intent intent4 = new Intent("android.intent.action.VIEW", Uri.parse(sb4.toString()));
                    intent4.setFlags(268435456);
                    context.startActivity(intent4);
                    jsonObject.put("storeOpenType", (Object)"webview");
                    com.bytedance.sdk.openadsdk.d.c.b(q, replace, "store_open", jsonObject);
                    return true;
                }
                finally {
                    return false;
                }
                return false;
            }
            finally {
                continue;
            }
            break;
        }
    }
    
    public static void \u3007080(final Context context) {
        final Intent intent = new Intent("android.intent.action.VIEW");
        intent.setData(Uri.parse("market://details"));
        Label_0082: {
            try {
                final Iterator iterator = context.getPackageManager().queryIntentActivities(intent, 65536).iterator();
                Block_4: {
                    while (iterator.hasNext()) {
                        if (((ResolveInfo)iterator.next()).activityInfo.packageName.equals("com.android.vending")) {
                            break Block_4;
                        }
                    }
                    break Label_0082;
                }
                b.oO80 = Boolean.TRUE;
                return;
            }
            finally {
                b.oO80 = Boolean.FALSE;
            }
        }
        b.oO80 = Boolean.FALSE;
    }
    
    private void \u3007o00\u3007\u3007Oo(final Map<String, Object> map) {
        final q \u3007o\u3007 = this.\u3007o\u3007;
        final boolean b = true;
        if (\u3007o\u3007 != null && \u3007o\u3007.G() == 0) {
            final q \u3007o\u30072 = this.\u3007o\u3007;
            map.put("auto_click", \u3007o\u30072 != null && !\u3007o\u30072.b1());
        }
        final q \u3007o\u30073 = this.\u3007o\u3007;
        if (\u3007o\u30073 != null && \u3007o\u30073.G() == 0) {
            map.put("dpl_probability_jump", this.\u3007\u3007888 >= 11 && b);
        }
    }
    
    public boolean Oo08() {
        // 
        // This method could not be decompiled.
        // 
        // Original Bytecode:
        // 
        //     1: getfield        b/c/a/a/a/a/b.\u3007o\u3007:Lcom/bytedance/sdk/openadsdk/core/f0/q;
        //     4: aload_0        
        //     5: getfield        b/c/a/a/a/a/b.O8:Ljava/lang/String;
        //     8: iconst_1       
        //     9: aconst_null    
        //    10: invokestatic    com/bytedance/sdk/openadsdk/core/d.a:(Lcom/bytedance/sdk/openadsdk/core/f0/q;Ljava/lang/String;ILorg/json/JSONObject;)V
        //    13: aload_0        
        //    14: getfield        b/c/a/a/a/a/b.\u3007o\u3007:Lcom/bytedance/sdk/openadsdk/core/f0/q;
        //    17: invokevirtual   com/bytedance/sdk/openadsdk/core/f0/q.z:()Lcom/bytedance/sdk/openadsdk/core/f0/j;
        //    20: ifnull          318
        //    23: aload_0        
        //    24: getfield        b/c/a/a/a/a/b.\u3007o\u3007:Lcom/bytedance/sdk/openadsdk/core/f0/q;
        //    27: invokevirtual   com/bytedance/sdk/openadsdk/core/f0/q.z:()Lcom/bytedance/sdk/openadsdk/core/f0/j;
        //    30: invokevirtual   com/bytedance/sdk/openadsdk/core/f0/j.a:()Ljava/lang/String;
        //    33: astore_1       
        //    34: aload_1        
        //    35: invokestatic    android/text/TextUtils.isEmpty:(Ljava/lang/CharSequence;)Z
        //    38: ifne            243
        //    41: aload_1        
        //    42: invokestatic    android/net/Uri.parse:(Ljava/lang/String;)Landroid/net/Uri;
        //    45: astore_2       
        //    46: new             Landroid/content/Intent;
        //    49: dup            
        //    50: ldc             "android.intent.action.VIEW"
        //    52: invokespecial   android/content/Intent.<init>:(Ljava/lang/String;)V
        //    55: astore_1       
        //    56: aload_1        
        //    57: aload_2        
        //    58: invokevirtual   android/content/Intent.setData:(Landroid/net/Uri;)Landroid/content/Intent;
        //    61: pop            
        //    62: aload_0        
        //    63: invokevirtual   b/c/a/a/a/a/b.o\u30070:()Landroid/content/Context;
        //    66: aload_1        
        //    67: invokestatic    com/bytedance/sdk/openadsdk/utils/a0.b:(Landroid/content/Context;Landroid/content/Intent;)Z
        //    70: ifeq            206
        //    73: aload_0        
        //    74: invokevirtual   b/c/a/a/a/a/b.o\u30070:()Landroid/content/Context;
        //    77: instanceof      Landroid/app/Activity;
        //    80: ifne            90
        //    83: aload_1        
        //    84: ldc             268435456
        //    86: invokevirtual   android/content/Intent.addFlags:(I)Landroid/content/Intent;
        //    89: pop            
        //    90: new             Ljava/util/HashMap;
        //    93: astore_2       
        //    94: aload_2        
        //    95: invokespecial   java/util/HashMap.<init>:()V
        //    98: aload_0        
        //    99: aload_2        
        //   100: invokespecial   b/c/a/a/a/a/b.\u3007o00\u3007\u3007Oo:(Ljava/util/Map;)V
        //   103: aload_0        
        //   104: getfield        b/c/a/a/a/a/b.\u3007o\u3007:Lcom/bytedance/sdk/openadsdk/core/f0/q;
        //   107: aload_0        
        //   108: getfield        b/c/a/a/a/a/b.O8:Ljava/lang/String;
        //   111: ldc_w           "open_url_app"
        //   114: aload_2        
        //   115: invokestatic    com/bytedance/sdk/openadsdk/d/c.a:(Lcom/bytedance/sdk/openadsdk/core/f0/q;Ljava/lang/String;Ljava/lang/String;Ljava/util/Map;)V
        //   118: aload_0        
        //   119: invokevirtual   b/c/a/a/a/a/b.o\u30070:()Landroid/content/Context;
        //   122: aload_1        
        //   123: invokevirtual   android/content/Context.startActivity:(Landroid/content/Intent;)V
        //   126: invokestatic    com/bytedance/sdk/openadsdk/d/l.a:()Lcom/bytedance/sdk/openadsdk/d/l;
        //   129: aload_2        
        //   130: invokevirtual   com/bytedance/sdk/openadsdk/d/l.a:(Ljava/util/Map;)Lcom/bytedance/sdk/openadsdk/d/l;
        //   133: aload_0        
        //   134: getfield        b/c/a/a/a/a/b.\u3007o\u3007:Lcom/bytedance/sdk/openadsdk/core/f0/q;
        //   137: aload_0        
        //   138: getfield        b/c/a/a/a/a/b.O8:Ljava/lang/String;
        //   141: invokevirtual   com/bytedance/sdk/openadsdk/d/l.a:(Lcom/bytedance/sdk/openadsdk/core/f0/q;Ljava/lang/String;)V
        //   144: iconst_1       
        //   145: ireturn        
        //   146: astore_2       
        //   147: new             Lorg/json/JSONObject;
        //   150: dup            
        //   151: invokespecial   org/json/JSONObject.<init>:()V
        //   154: astore_3       
        //   155: aload_3        
        //   156: ldc_w           "exception"
        //   159: aload_2        
        //   160: invokevirtual   java/lang/Throwable.getMessage:()Ljava/lang/String;
        //   163: invokevirtual   org/json/JSONObject.put:(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
        //   166: pop            
        //   167: aload_3        
        //   168: ldc_w           "intent"
        //   171: aload_1        
        //   172: invokevirtual   android/content/Intent.toString:()Ljava/lang/String;
        //   175: invokevirtual   org/json/JSONObject.put:(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
        //   178: pop            
        //   179: aload_0        
        //   180: getfield        b/c/a/a/a/a/b.\u3007o\u3007:Lcom/bytedance/sdk/openadsdk/core/f0/q;
        //   183: aload_0        
        //   184: getfield        b/c/a/a/a/a/b.O8:Ljava/lang/String;
        //   187: bipush          -4
        //   189: aload_3        
        //   190: invokestatic    com/bytedance/sdk/openadsdk/core/d.a:(Lcom/bytedance/sdk/openadsdk/core/f0/q;Ljava/lang/String;ILorg/json/JSONObject;)V
        //   193: ldc_w           "startActivityError1"
        //   196: ldc_w           "deepLink"
        //   199: aload_2        
        //   200: invokestatic    com/bytedance/sdk/openadsdk/ApmHelper.reportCustomError:(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
        //   203: goto            265
        //   206: new             Lorg/json/JSONObject;
        //   209: dup            
        //   210: invokespecial   org/json/JSONObject.<init>:()V
        //   213: astore_2       
        //   214: aload_2        
        //   215: ldc_w           "intent"
        //   218: aload_1        
        //   219: invokevirtual   android/content/Intent.toString:()Ljava/lang/String;
        //   222: invokevirtual   org/json/JSONObject.put:(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
        //   225: pop            
        //   226: aload_0        
        //   227: getfield        b/c/a/a/a/a/b.\u3007o\u3007:Lcom/bytedance/sdk/openadsdk/core/f0/q;
        //   230: aload_0        
        //   231: getfield        b/c/a/a/a/a/b.O8:Ljava/lang/String;
        //   234: bipush          -3
        //   236: aload_2        
        //   237: invokestatic    com/bytedance/sdk/openadsdk/core/d.a:(Lcom/bytedance/sdk/openadsdk/core/f0/q;Ljava/lang/String;ILorg/json/JSONObject;)V
        //   240: goto            265
        //   243: aload_0        
        //   244: getfield        b/c/a/a/a/a/b.\u3007o\u3007:Lcom/bytedance/sdk/openadsdk/core/f0/q;
        //   247: astore_1       
        //   248: aload_1        
        //   249: aload_0        
        //   250: getfield        b/c/a/a/a/a/b.O8:Ljava/lang/String;
        //   253: bipush          -2
        //   255: aload_1        
        //   256: invokevirtual   com/bytedance/sdk/openadsdk/core/f0/q.z:()Lcom/bytedance/sdk/openadsdk/core/f0/j;
        //   259: invokevirtual   com/bytedance/sdk/openadsdk/core/f0/j.d:()Lorg/json/JSONObject;
        //   262: invokestatic    com/bytedance/sdk/openadsdk/core/d.a:(Lcom/bytedance/sdk/openadsdk/core/f0/q;Ljava/lang/String;ILorg/json/JSONObject;)V
        //   265: aload_0        
        //   266: getfield        b/c/a/a/a/a/b.Oo08:Z
        //   269: ifeq            282
        //   272: aload_0        
        //   273: getfield        b/c/a/a/a/a/b.o\u30070:Ljava/util/concurrent/atomic/AtomicBoolean;
        //   276: invokevirtual   java/util/concurrent/atomic/AtomicBoolean.get:()Z
        //   279: ifeq            331
        //   282: aload_0        
        //   283: iconst_1       
        //   284: putfield        b/c/a/a/a/a/b.Oo08:Z
        //   287: new             Ljava/util/HashMap;
        //   290: dup            
        //   291: invokespecial   java/util/HashMap.<init>:()V
        //   294: astore_1       
        //   295: aload_0        
        //   296: aload_1        
        //   297: invokespecial   b/c/a/a/a/a/b.\u3007o00\u3007\u3007Oo:(Ljava/util/Map;)V
        //   300: aload_0        
        //   301: getfield        b/c/a/a/a/a/b.\u3007o\u3007:Lcom/bytedance/sdk/openadsdk/core/f0/q;
        //   304: aload_0        
        //   305: getfield        b/c/a/a/a/a/b.O8:Ljava/lang/String;
        //   308: ldc_w           "open_fallback_url"
        //   311: aload_1        
        //   312: invokestatic    com/bytedance/sdk/openadsdk/d/c.a:(Lcom/bytedance/sdk/openadsdk/core/f0/q;Ljava/lang/String;Ljava/lang/String;Ljava/util/Map;)V
        //   315: goto            331
        //   318: aload_0        
        //   319: getfield        b/c/a/a/a/a/b.\u3007o\u3007:Lcom/bytedance/sdk/openadsdk/core/f0/q;
        //   322: aload_0        
        //   323: getfield        b/c/a/a/a/a/b.O8:Ljava/lang/String;
        //   326: iconst_m1      
        //   327: aconst_null    
        //   328: invokestatic    com/bytedance/sdk/openadsdk/core/d.a:(Lcom/bytedance/sdk/openadsdk/core/f0/q;Ljava/lang/String;ILorg/json/JSONObject;)V
        //   331: iconst_0       
        //   332: ireturn        
        //   333: astore_1       
        //   334: goto            179
        //   337: astore_1       
        //   338: goto            226
        //    Exceptions:
        //  Try           Handler
        //  Start  End    Start  End    Type                 
        //  -----  -----  -----  -----  ---------------------
        //  90     144    146    206    Any
        //  155    179    333    337    Ljava/lang/Exception;
        //  214    226    337    341    Ljava/lang/Exception;
        // 
        // The error that occurred was:
        // 
        // java.lang.IllegalStateException: Expression is linked from several locations: Label_0179:
        //     at com.strobel.decompiler.ast.Error.expressionLinkedFromMultipleLocations(Error.java:27)
        //     at com.strobel.decompiler.ast.AstOptimizer.mergeDisparateObjectInitializations(AstOptimizer.java:2604)
        //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:235)
        //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:42)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:206)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:93)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethodBody(AstBuilder.java:868)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethod(AstBuilder.java:761)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addTypeMembers(AstBuilder.java:638)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeCore(AstBuilder.java:605)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeNoCache(AstBuilder.java:195)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createType(AstBuilder.java:162)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addType(AstBuilder.java:137)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.buildAst(JavaLanguage.java:71)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.decompileType(JavaLanguage.java:59)
        //     at com.strobel.decompiler.DecompilerDriver.decompileType(DecompilerDriver.java:333)
        //     at com.strobel.decompiler.DecompilerDriver.decompileJar(DecompilerDriver.java:254)
        //     at com.strobel.decompiler.DecompilerDriver.main(DecompilerDriver.java:144)
        // 
        throw new IllegalStateException("An error occurred while decompiling this method.");
    }
    
    @Override
    public void a(final int \u3007\u3007888) {
        this.\u3007\u3007888 = \u3007\u3007888;
    }
    
    @Override
    public void a(final boolean b) {
    }
    
    @Override
    public boolean a() {
        if (this.\u3007o00\u3007\u3007Oo == null) {
            return false;
        }
        final q \u3007o\u3007 = this.\u3007o\u3007;
        if (\u3007o\u3007 != null && \u3007o\u3007.m0() == 0) {
            return false;
        }
        final String e = this.\u3007o00\u3007\u3007Oo.e();
        if (!TextUtils.isEmpty((CharSequence)e) && a0.b(this.o\u30070(), e)) {
            final Intent a = a0.a(this.o\u30070(), e);
            if (a == null) {
                return false;
            }
            a.putExtra("START_ONLY_FOR_ANDROID", true);
            try {
                this.o\u30070().startActivity(a);
                final HashMap<String, Object> hashMap = new HashMap<String, Object>();
                this.\u3007o00\u3007\u3007Oo(hashMap);
                com.bytedance.sdk.openadsdk.d.c.a(this.\u3007o\u3007, this.O8, "click_open", (Map)hashMap);
                return true;
            }
            finally {
                final Throwable t;
                m.b("GPDownLoader", t.getMessage());
            }
        }
        return false;
    }
    
    @Override
    public void b() {
        if (this.o\u30070() == null) {
            return;
        }
        if (this.Oo08()) {
            this.o\u30070.set(true);
            this.\u3007o\u3007.d(true);
            return;
        }
        if (this.a()) {
            this.\u3007o\u3007.d(true);
            return;
        }
        if (this.c()) {
            this.\u3007o\u3007.d(true);
            return;
        }
        if (this.\u3007o\u3007.h() == null && this.\u3007o\u3007.H0() != null) {
            z.b(this.o\u30070(), this.\u3007o\u3007.H0(), this.\u3007o\u3007, a0.f(this.O8), this.O8, true);
            this.\u3007o\u3007.d(true);
        }
    }
    
    @Override
    public boolean c() {
        final AtomicBoolean o\u30070 = this.o\u30070;
        boolean b = true;
        o\u30070.set(true);
        if (this.\u3007o00\u3007\u3007Oo == null || !this.\u3007o\u3007(this.o\u30070(), this.\u3007o00\u3007\u3007Oo.d(), this.\u3007o00\u3007\u3007Oo.e())) {
            b = false;
        }
        return b;
    }
    
    protected Context o\u30070() {
        final WeakReference<Context> \u3007080 = this.\u3007080;
        Context a;
        if (\u3007080 != null && \u3007080.get() != null) {
            a = this.\u3007080.get();
        }
        else {
            a = o.a();
        }
        return a;
    }
    
    public boolean \u3007o\u3007(final Context context, final String s, final String s2) {
        return O8(context, s, s2, this.O8, this.\u3007o\u3007);
    }
}

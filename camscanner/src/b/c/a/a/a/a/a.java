// 
// Decompiled by Procyon v0.6.0
// 

package b.c.a.a.a.a;

import com.bytedance.sdk.openadsdk.ApmHelper;
import org.json.JSONObject;
import com.bytedance.sdk.openadsdk.core.d;
import com.bytedance.sdk.openadsdk.d.l;
import android.net.Uri;
import android.content.Intent;
import com.bytedance.sdk.openadsdk.d.c;
import java.util.HashMap;
import android.app.Activity;
import com.bytedance.sdk.openadsdk.utils.a0;
import android.text.TextUtils;
import java.util.Map;
import com.bytedance.sdk.openadsdk.core.f0.q;
import android.content.Context;

public class a extends b
{
    public a(final Context context, final q q, final String s) {
        super(context, q, s);
    }
    
    public static boolean oO80(final q q, final String s, final Context context, final String s2, final Map<String, Object> map) {
        if (q != null && q.m0() == 0) {
            return false;
        }
        try {
            if (TextUtils.isEmpty((CharSequence)s)) {
                return false;
            }
            final Intent a = a0.a(context, s);
            if (a == null) {
                return false;
            }
            a.putExtra("START_ONLY_FOR_ANDROID", true);
            if (!(context instanceof Activity)) {
                a.addFlags(268435456);
            }
            context.startActivity(a);
            Map<String, Object> map2;
            if ((map2 = map) == null) {
                map2 = new HashMap<String, Object>();
            }
            \u3007\u3007888(q, map2);
            com.bytedance.sdk.openadsdk.d.c.a(q, s2, "click_open", (Map)map2);
            return true;
        }
        finally {
            return false;
        }
    }
    
    public static boolean \u300780\u3007808\u3007O(final String s, final Context context, final String s2, final q q, Map<String, Object> jsonObject) {
        final Intent intent = null;
        Intent intent3 = null;
        Label_0120: {
            try {
                if (!TextUtils.isEmpty((CharSequence)s)) {
                    final Uri parse = Uri.parse(s);
                    final Intent intent2 = new Intent("android.intent.action.VIEW");
                    try {
                        intent2.setData(parse);
                        if (!(context instanceof Activity)) {
                            intent2.addFlags(268435456);
                        }
                        Map<String, Object> map;
                        if ((map = (Map<String, Object>)jsonObject) == null) {
                            map = new HashMap<String, Object>();
                        }
                        \u3007\u3007888(q, map);
                        com.bytedance.sdk.openadsdk.d.c.a(q, s2, "open_url_app", (Map)map);
                        context.startActivity(intent2);
                        l.a().a((Map)map).a(q, s2);
                        return true;
                    }
                    finally {
                        break Label_0120;
                    }
                }
                d.a(q, s2, -2, (JSONObject)null);
                return false;
            }
            finally {
                intent3 = intent;
            }
        }
        jsonObject = new JSONObject();
        while (true) {
            try {
                jsonObject.put("exception", (Object)((Throwable)s).getMessage());
                if (intent3 != null) {
                    jsonObject.put("intent", (Object)intent3.toString());
                }
                d.a(q, s2, -4, jsonObject);
                ApmHelper.reportCustomError("startActivityError", "deepLink", (Throwable)s);
                return false;
            }
            catch (final Exception ex) {
                continue;
            }
            break;
        }
    }
    
    private static void \u3007\u3007888(final q q, final Map<String, Object> map) {
        if (map == null) {
            return;
        }
        if (q != null && q.G() == 0) {
            map.put("auto_click", !q.b1());
        }
    }
    
    @Override
    public boolean Oo08() {
        d.a(super.\u3007o\u3007, super.O8, 1, (JSONObject)null);
        if (super.\u3007o\u3007.z() != null) {
            final HashMap hashMap = new HashMap();
            final q \u3007o\u3007 = super.\u3007o\u3007;
            if (\u3007o\u3007 != null && \u3007o\u3007.G() == 0) {
                hashMap.put("dpl_probability_jump", super.\u3007\u3007888 >= 11);
            }
            if (\u300780\u3007808\u3007O(super.\u3007o\u3007.z().a(), this.o\u30070(), super.O8, super.\u3007o\u3007, hashMap)) {
                return true;
            }
            if (!super.Oo08 || super.o\u30070.get()) {
                super.Oo08 = true;
                \u3007\u3007888(super.\u3007o\u3007, hashMap);
                com.bytedance.sdk.openadsdk.d.c.a(super.\u3007o\u3007, super.O8, "open_fallback_url", (Map)hashMap);
            }
        }
        else {
            d.a(super.\u3007o\u3007, super.O8, -1, (JSONObject)null);
        }
        return false;
    }
    
    @Override
    public boolean a() {
        final com.bytedance.sdk.openadsdk.core.f0.c \u3007o00\u3007\u3007Oo = super.\u3007o00\u3007\u3007Oo;
        boolean b = false;
        if (\u3007o00\u3007\u3007Oo == null) {
            return false;
        }
        final q \u3007o\u3007 = super.\u3007o\u3007;
        HashMap hashMap;
        if (\u3007o\u3007 != null && \u3007o\u3007.G() == 0) {
            hashMap = new HashMap();
            if (super.\u3007\u3007888 >= 11) {
                b = true;
            }
            hashMap.put("dpl_probability_jump", b);
        }
        else {
            hashMap = null;
        }
        return oO80(super.\u3007o\u3007, super.\u3007o00\u3007\u3007Oo.e(), this.o\u30070(), super.O8, hashMap);
    }
}

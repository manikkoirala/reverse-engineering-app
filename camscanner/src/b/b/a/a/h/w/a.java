// 
// Decompiled by Procyon v0.6.0
// 

package b.b.a.a.h.w;

import b.b.a.a.h.f;
import b.b.a.a.h.e;
import java.util.Iterator;
import java.util.List;
import java.util.HashMap;
import java.util.Map;
import java.net.HttpURLConnection;
import b.b.a.a.h.d;

public class a implements d<b.b.a.a.h.w.d>
{
    private Map<String, String> \u3007080(final HttpURLConnection httpURLConnection) {
        final HashMap hashMap = new HashMap();
        final Map<String, List<String>> headerFields = httpURLConnection.getHeaderFields();
        for (final String s : headerFields.keySet()) {
            final List list = headerFields.get(s);
            if (list != null && list.size() > 0) {
                hashMap.put(s, list.get(0));
            }
        }
        return hashMap;
    }
    
    public b.b.a.a.h.w.d<byte[]> \u3007o00\u3007\u3007Oo(final e p0) {
        // 
        // This method could not be decompiled.
        // 
        // Original Bytecode:
        // 
        //     1: astore          13
        //     3: aconst_null    
        //     4: astore          8
        //     6: aconst_null    
        //     7: astore          10
        //     9: iconst_0       
        //    10: istore_2       
        //    11: iconst_0       
        //    12: istore          4
        //    14: iconst_0       
        //    15: istore_3       
        //    16: iconst_0       
        //    17: istore          5
        //    19: new             Ljava/net/URL;
        //    22: astore          7
        //    24: aload           7
        //    26: aload_1        
        //    27: invokeinterface b/b/a/a/h/e.a:()Ljava/lang/String;
        //    32: invokespecial   java/net/URL.<init>:(Ljava/lang/String;)V
        //    35: aload           7
        //    37: invokevirtual   java/net/URL.openConnection:()Ljava/net/URLConnection;
        //    40: checkcast       Ljava/net/HttpURLConnection;
        //    43: astore          14
        //    45: aload           14
        //    47: ldc             "GET"
        //    49: invokevirtual   java/net/HttpURLConnection.setRequestMethod:(Ljava/lang/String;)V
        //    52: aload           14
        //    54: sipush          5000
        //    57: invokevirtual   java/net/URLConnection.setConnectTimeout:(I)V
        //    60: aload           14
        //    62: sipush          5000
        //    65: invokevirtual   java/net/URLConnection.setReadTimeout:(I)V
        //    68: aload           14
        //    70: invokevirtual   java/net/URLConnection.connect:()V
        //    73: aload           14
        //    75: invokevirtual   java/net/URLConnection.getInputStream:()Ljava/io/InputStream;
        //    78: astore          7
        //    80: sipush          1024
        //    83: newarray        B
        //    85: astore          12
        //    87: new             Ljava/io/ByteArrayOutputStream;
        //    90: astore          11
        //    92: aload           11
        //    94: invokespecial   java/io/ByteArrayOutputStream.<init>:()V
        //    97: iload           5
        //    99: istore_3       
        //   100: iload_2        
        //   101: istore          4
        //   103: aload           7
        //   105: astore          8
        //   107: aload           11
        //   109: astore          9
        //   111: aload           7
        //   113: aload           12
        //   115: invokevirtual   java/io/InputStream.read:([B)I
        //   118: istore          6
        //   120: iload           6
        //   122: iconst_m1      
        //   123: if_icmpeq       153
        //   126: iload           5
        //   128: istore_3       
        //   129: iload_2        
        //   130: istore          4
        //   132: aload           7
        //   134: astore          8
        //   136: aload           11
        //   138: astore          9
        //   140: aload           11
        //   142: aload           12
        //   144: iconst_0       
        //   145: iload           6
        //   147: invokevirtual   java/io/ByteArrayOutputStream.write:([BII)V
        //   150: goto            97
        //   153: sipush          200
        //   156: istore_2       
        //   157: iload_2        
        //   158: istore_3       
        //   159: iload_2        
        //   160: istore          4
        //   162: aload           7
        //   164: astore          8
        //   166: aload           11
        //   168: astore          9
        //   170: aload           11
        //   172: invokevirtual   java/io/ByteArrayOutputStream.toByteArray:()[B
        //   175: astore          12
        //   177: aload           7
        //   179: astore          8
        //   181: aload           11
        //   183: astore          9
        //   185: aload_1        
        //   186: invokeinterface b/b/a/a/h/e.c:()Z
        //   191: ifeq            210
        //   194: aload           7
        //   196: astore          8
        //   198: aload           11
        //   200: astore          9
        //   202: aload_0        
        //   203: aload           14
        //   205: invokespecial   b/b/a/a/h/w/a.\u3007080:(Ljava/net/HttpURLConnection;)Ljava/util/Map;
        //   208: astore          10
        //   210: aload           7
        //   212: invokestatic    b/b/a/a/h/x/k/b.\u3007080:(Ljava/io/Closeable;)V
        //   215: aload           11
        //   217: invokestatic    b/b/a/a/h/x/k/b.\u3007080:(Ljava/io/Closeable;)V
        //   220: ldc             "success"
        //   222: astore          9
        //   224: aload           12
        //   226: astore          8
        //   228: goto            548
        //   231: astore          8
        //   233: aload           7
        //   235: astore          10
        //   237: aload           12
        //   239: astore          7
        //   241: aload           11
        //   243: astore_1       
        //   244: aload           8
        //   246: astore          11
        //   248: goto            346
        //   251: astore          8
        //   253: aload           12
        //   255: astore_1       
        //   256: aload           11
        //   258: astore          10
        //   260: aload           8
        //   262: astore          11
        //   264: goto            447
        //   267: astore          8
        //   269: aconst_null    
        //   270: astore_1       
        //   271: iload_3        
        //   272: istore_2       
        //   273: aload           7
        //   275: astore          10
        //   277: aload_1        
        //   278: astore          7
        //   280: aload           11
        //   282: astore_1       
        //   283: aload           8
        //   285: astore          11
        //   287: goto            346
        //   290: astore          8
        //   292: aconst_null    
        //   293: astore_1       
        //   294: iload           4
        //   296: istore_2       
        //   297: aload           11
        //   299: astore          10
        //   301: aload           8
        //   303: astore          11
        //   305: goto            447
        //   308: astore_1       
        //   309: goto            572
        //   312: astore          11
        //   314: aload           7
        //   316: astore          10
        //   318: goto            338
        //   321: astore          11
        //   323: goto            440
        //   326: astore_1       
        //   327: aconst_null    
        //   328: astore          7
        //   330: goto            572
        //   333: astore          11
        //   335: aconst_null    
        //   336: astore          10
        //   338: aconst_null    
        //   339: astore          7
        //   341: aconst_null    
        //   342: astore_1       
        //   343: iload           4
        //   345: istore_2       
        //   346: aload           10
        //   348: astore          8
        //   350: aload_1        
        //   351: astore          9
        //   353: new             Ljava/lang/StringBuilder;
        //   356: astore          12
        //   358: aload           10
        //   360: astore          8
        //   362: aload_1        
        //   363: astore          9
        //   365: aload           12
        //   367: invokespecial   java/lang/StringBuilder.<init>:()V
        //   370: aload           10
        //   372: astore          8
        //   374: aload_1        
        //   375: astore          9
        //   377: aload           12
        //   379: ldc             "IOException:"
        //   381: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   384: pop            
        //   385: aload           10
        //   387: astore          8
        //   389: aload_1        
        //   390: astore          9
        //   392: aload           12
        //   394: aload           11
        //   396: invokevirtual   java/lang/Throwable.getMessage:()Ljava/lang/String;
        //   399: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   402: pop            
        //   403: aload           10
        //   405: astore          8
        //   407: aload_1        
        //   408: astore          9
        //   410: aload           11
        //   412: invokevirtual   java/lang/Throwable.getMessage:()Ljava/lang/String;
        //   415: astore          12
        //   417: aload           10
        //   419: astore          11
        //   421: aload           7
        //   423: astore          8
        //   425: aload_1        
        //   426: astore          10
        //   428: aload           12
        //   430: astore          9
        //   432: goto            534
        //   435: astore          11
        //   437: aconst_null    
        //   438: astore          7
        //   440: aconst_null    
        //   441: astore_1       
        //   442: aconst_null    
        //   443: astore          10
        //   445: iload_3        
        //   446: istore_2       
        //   447: aload           7
        //   449: astore          8
        //   451: aload           10
        //   453: astore          9
        //   455: new             Ljava/lang/StringBuilder;
        //   458: astore          12
        //   460: aload           7
        //   462: astore          8
        //   464: aload           10
        //   466: astore          9
        //   468: aload           12
        //   470: invokespecial   java/lang/StringBuilder.<init>:()V
        //   473: aload           7
        //   475: astore          8
        //   477: aload           10
        //   479: astore          9
        //   481: aload           12
        //   483: ldc             "MalformedURLException:"
        //   485: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   488: pop            
        //   489: aload           7
        //   491: astore          8
        //   493: aload           10
        //   495: astore          9
        //   497: aload           12
        //   499: aload           11
        //   501: invokevirtual   java/lang/Throwable.getMessage:()Ljava/lang/String;
        //   504: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   507: pop            
        //   508: aload           7
        //   510: astore          8
        //   512: aload           10
        //   514: astore          9
        //   516: aload           11
        //   518: invokevirtual   java/lang/Throwable.getMessage:()Ljava/lang/String;
        //   521: astore          11
        //   523: aload           11
        //   525: astore          9
        //   527: aload_1        
        //   528: astore          8
        //   530: aload           7
        //   532: astore          11
        //   534: aload           11
        //   536: invokestatic    b/b/a/a/h/x/k/b.\u3007080:(Ljava/io/Closeable;)V
        //   539: aload           10
        //   541: invokestatic    b/b/a/a/h/x/k/b.\u3007080:(Ljava/io/Closeable;)V
        //   544: aload           13
        //   546: astore          10
        //   548: new             Lb/b/a/a/h/w/d;
        //   551: dup            
        //   552: iload_2        
        //   553: aload           8
        //   555: aload           9
        //   557: aload           10
        //   559: invokespecial   b/b/a/a/h/w/d.<init>:(ILjava/lang/Object;Ljava/lang/String;Ljava/util/Map;)V
        //   562: areturn        
        //   563: astore_1       
        //   564: aload           8
        //   566: astore          7
        //   568: aload           9
        //   570: astore          8
        //   572: aload           7
        //   574: invokestatic    b/b/a/a/h/x/k/b.\u3007080:(Ljava/io/Closeable;)V
        //   577: aload           8
        //   579: invokestatic    b/b/a/a/h/x/k/b.\u3007080:(Ljava/io/Closeable;)V
        //   582: aload_1        
        //   583: athrow         
        //    Signature:
        //  (Lb/b/a/a/h/e;)Lb/b/a/a/h/w/d<[B>;
        //    Exceptions:
        //  Try           Handler
        //  Start  End    Start  End    Type                            
        //  -----  -----  -----  -----  --------------------------------
        //  19     80     435    440    Ljava/net/MalformedURLException;
        //  19     80     333    338    Ljava/io/IOException;
        //  19     80     326    333    Any
        //  80     97     321    326    Ljava/net/MalformedURLException;
        //  80     97     312    321    Ljava/io/IOException;
        //  80     97     308    312    Any
        //  111    120    290    308    Ljava/net/MalformedURLException;
        //  111    120    267    290    Ljava/io/IOException;
        //  111    120    563    572    Any
        //  140    150    290    308    Ljava/net/MalformedURLException;
        //  140    150    267    290    Ljava/io/IOException;
        //  140    150    563    572    Any
        //  170    177    290    308    Ljava/net/MalformedURLException;
        //  170    177    267    290    Ljava/io/IOException;
        //  170    177    563    572    Any
        //  185    194    251    267    Ljava/net/MalformedURLException;
        //  185    194    231    251    Ljava/io/IOException;
        //  185    194    563    572    Any
        //  202    210    251    267    Ljava/net/MalformedURLException;
        //  202    210    231    251    Ljava/io/IOException;
        //  202    210    563    572    Any
        //  353    358    563    572    Any
        //  365    370    563    572    Any
        //  377    385    563    572    Any
        //  392    403    563    572    Any
        //  410    417    563    572    Any
        //  455    460    563    572    Any
        //  468    473    563    572    Any
        //  481    489    563    572    Any
        //  497    508    563    572    Any
        //  516    523    563    572    Any
        // 
        // The error that occurred was:
        // 
        // java.lang.IllegalStateException: Expression is linked from several locations: Label_0153:
        //     at com.strobel.decompiler.ast.Error.expressionLinkedFromMultipleLocations(Error.java:27)
        //     at com.strobel.decompiler.ast.AstOptimizer.mergeDisparateObjectInitializations(AstOptimizer.java:2604)
        //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:235)
        //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:42)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:206)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:93)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethodBody(AstBuilder.java:868)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethod(AstBuilder.java:761)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addTypeMembers(AstBuilder.java:638)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeCore(AstBuilder.java:605)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeNoCache(AstBuilder.java:195)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createType(AstBuilder.java:162)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addType(AstBuilder.java:137)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.buildAst(JavaLanguage.java:71)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.decompileType(JavaLanguage.java:59)
        //     at com.strobel.decompiler.DecompilerDriver.decompileType(DecompilerDriver.java:333)
        //     at com.strobel.decompiler.DecompilerDriver.decompileJar(DecompilerDriver.java:254)
        //     at com.strobel.decompiler.DecompilerDriver.main(DecompilerDriver.java:144)
        // 
        throw new IllegalStateException("An error occurred while decompiling this method.");
    }
}

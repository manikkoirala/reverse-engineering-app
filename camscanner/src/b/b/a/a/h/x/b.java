// 
// Decompiled by Procyon v0.6.0
// 

package b.b.a.a.h.x;

import b.b.a.a.h.a;
import java.util.Iterator;
import java.util.Collection;
import java.io.ByteArrayInputStream;
import b.b.a.a.h.r;
import android.text.TextUtils;
import java.io.InputStream;
import b.b.a.a.h.j;
import b.b.a.a.h.m;
import android.content.Context;
import b.b.a.a.h.n;

public class b implements n
{
    private volatile f \u3007080;
    
    private b() {
    }
    
    public static n \u3007080(final Context context, final m m) {
        final b b = new b();
        b.\u3007o00\u3007\u3007Oo(context, m);
        return b;
    }
    
    private void \u3007o00\u3007\u3007Oo(final Context context, final m m) {
        m \u3007080 = m;
        if (m == null) {
            \u3007080 = e.\u3007080(context);
        }
        this.\u3007080 = new f(context, \u3007080);
    }
    
    @Override
    public j a(final String s) {
        return new c.c(this.\u3007080).\u30078o8o\u3007(s);
    }
    
    @Override
    public InputStream a(final String s, final String s2) {
        if (this.\u3007080 != null) {
            String \u3007080 = s2;
            if (TextUtils.isEmpty((CharSequence)s2)) {
                if (TextUtils.isEmpty((CharSequence)s)) {
                    return null;
                }
                \u3007080 = b.b.a.a.h.x.k.c.\u3007080(s);
            }
            final Collection<r> oooo8o0\u3007 = this.\u3007080.Oooo8o0\u3007();
            if (oooo8o0\u3007 != null) {
                final Iterator<r> iterator = oooo8o0\u3007.iterator();
                while (iterator.hasNext()) {
                    final byte[] buf = iterator.next().get(\u3007080);
                    if (buf != null) {
                        return new ByteArrayInputStream(buf);
                    }
                }
            }
            final Collection<b.b.a.a.h.c> \u3007o8o08O = this.\u3007080.\u3007O8o08O();
            if (\u3007o8o08O != null) {
                final Iterator<b.b.a.a.h.c> iterator2 = \u3007o8o08O.iterator();
                while (iterator2.hasNext()) {
                    final InputStream a = iterator2.next().a(\u3007080);
                    if (a != null) {
                        return a;
                    }
                }
            }
        }
        return null;
    }
    
    @Override
    public boolean a(final String s, final String s2, final String s3) {
        if (this.\u3007080 != null) {
            if (TextUtils.isEmpty((CharSequence)s3)) {
                return false;
            }
            String \u3007080 = s2;
            if (TextUtils.isEmpty((CharSequence)s2)) {
                if (TextUtils.isEmpty((CharSequence)s)) {
                    return false;
                }
                \u3007080 = b.b.a.a.h.x.k.c.\u3007080(s);
            }
            final b.b.a.a.h.c \u3007o00\u3007\u3007Oo = this.\u3007080.\u3007o00\u3007\u3007Oo(s3);
            if (\u3007o00\u3007\u3007Oo != null) {
                return ((a<String, V>)\u3007o00\u3007\u3007Oo).contains(\u3007080);
            }
        }
        return false;
    }
}

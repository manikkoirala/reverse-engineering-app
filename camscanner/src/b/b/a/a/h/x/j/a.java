// 
// Decompiled by Procyon v0.6.0
// 

package b.b.a.a.h.x.j;

import android.graphics.BitmapFactory;
import android.graphics.BitmapFactory$Options;
import android.graphics.Bitmap;
import android.widget.ImageView$ScaleType;
import android.graphics.Bitmap$Config;

public class a
{
    public static final Bitmap$Config oO80;
    public static final ImageView$ScaleType \u3007\u3007888;
    private final int O8;
    private final int Oo08;
    private final ImageView$ScaleType o\u30070;
    private final Bitmap$Config \u3007080;
    private int \u3007o00\u3007\u3007Oo;
    private int \u3007o\u3007;
    
    static {
        \u3007\u3007888 = ImageView$ScaleType.CENTER_INSIDE;
        oO80 = Bitmap$Config.ARGB_4444;
    }
    
    public a(final int \u3007o00\u3007\u3007Oo, final int \u3007o\u3007, final ImageView$ScaleType o\u30070, final Bitmap$Config \u3007080, final int o8, final int oo08) {
        this.\u3007080 = \u3007080;
        this.\u3007o00\u3007\u3007Oo = \u3007o00\u3007\u3007Oo;
        this.\u3007o\u3007 = \u3007o\u3007;
        this.o\u30070 = o\u30070;
        this.O8 = o8;
        this.Oo08 = oo08;
        this.O8(\u3007o00\u3007\u3007Oo, \u3007o\u3007);
    }
    
    private void O8(final int n, final int n2) {
        if (n > 3840 && n2 > 3840) {
            if (n > n2) {
                this.\u3007o00\u3007\u3007Oo = 3840;
                this.\u3007o\u3007 = n2 * 3840 / n;
            }
            else {
                this.\u3007o00\u3007\u3007Oo = n * 3840 / n2;
                this.\u3007o\u3007 = 3840;
            }
            return;
        }
        if (n > 3840) {
            this.\u3007o00\u3007\u3007Oo = 3840;
            this.\u3007o\u3007 = n2 * 3840 / n;
            return;
        }
        if (n2 > 3840) {
            this.\u3007o00\u3007\u3007Oo = n * 3840 / n2;
            this.\u3007o\u3007 = 3840;
        }
    }
    
    static int \u3007080(final int n, final int n2, final int n3, final int n4, final int n5, final int n6) {
        double n7;
        final double a = n7 = Math.min(n / (double)n3, n2 / (double)n4);
        if (n5 > 0) {
            n7 = a;
            if (n6 > 0) {
                n7 = Math.max(a, Math.min(Math.max(n, n2) / (double)Math.max(n5, n6), Math.min(n, n2) / (double)Math.min(n5, n6)));
            }
        }
        float n8 = 1.0f;
        while (true) {
            final float n9 = 2.0f * n8;
            if (n9 > n7) {
                break;
            }
            n8 = n9;
        }
        return (int)n8;
    }
    
    private static int \u3007o00\u3007\u3007Oo(int n, final int n2, final int n3, final int n4, final ImageView$ScaleType imageView$ScaleType) {
        if (n == 0 && n2 == 0) {
            return n3;
        }
        if (imageView$ScaleType == ImageView$ScaleType.FIT_XY) {
            if (n == 0) {
                return n3;
            }
            return n;
        }
        else {
            if (n == 0) {
                return (int)(n3 * (n2 / (double)n4));
            }
            if (n2 == 0) {
                return n;
            }
            final double n5 = n4 / (double)n3;
            if (imageView$ScaleType == ImageView$ScaleType.CENTER_CROP) {
                final double n6 = n;
                final double n7 = n2;
                if (n6 * n5 < n7) {
                    n = (int)(n7 / n5);
                }
                return n;
            }
            final double n8 = n;
            final double n9 = n2;
            if (n8 * n5 > n9) {
                n = (int)(n9 / n5);
            }
            return n;
        }
    }
    
    public Bitmap \u3007o\u3007(final byte[] array) {
        final BitmapFactory$Options bitmapFactory$Options = new BitmapFactory$Options();
        Bitmap bitmap = null;
        Label_0203: {
            if (this.\u3007o00\u3007\u3007Oo == 0 && this.\u3007o\u3007 == 0) {
                bitmapFactory$Options.inPreferredConfig = this.\u3007080;
                bitmap = BitmapFactory.decodeByteArray(array, 0, array.length, bitmapFactory$Options);
            }
            else {
                bitmapFactory$Options.inJustDecodeBounds = true;
                BitmapFactory.decodeByteArray(array, 0, array.length, bitmapFactory$Options);
                final int outWidth = bitmapFactory$Options.outWidth;
                final int outHeight = bitmapFactory$Options.outHeight;
                final int \u3007o00\u3007\u3007Oo = \u3007o00\u3007\u3007Oo(this.\u3007o00\u3007\u3007Oo, this.\u3007o\u3007, outWidth, outHeight, this.o\u30070);
                final int \u3007o00\u3007\u3007Oo2 = \u3007o00\u3007\u3007Oo(this.\u3007o\u3007, this.\u3007o00\u3007\u3007Oo, outHeight, outWidth, this.o\u30070);
                bitmapFactory$Options.inJustDecodeBounds = false;
                bitmapFactory$Options.inSampleSize = \u3007080(outWidth, outHeight, \u3007o00\u3007\u3007Oo, \u3007o00\u3007\u3007Oo2, this.O8, this.Oo08);
                final Bitmap decodeByteArray = BitmapFactory.decodeByteArray(array, 0, array.length, bitmapFactory$Options);
                if ((bitmap = decodeByteArray) != null) {
                    if (decodeByteArray.getWidth() <= \u3007o00\u3007\u3007Oo) {
                        bitmap = decodeByteArray;
                        if (decodeByteArray.getHeight() <= \u3007o00\u3007\u3007Oo2) {
                            break Label_0203;
                        }
                    }
                    bitmap = Bitmap.createScaledBitmap(decodeByteArray, \u3007o00\u3007\u3007Oo, \u3007o00\u3007\u3007Oo2, true);
                    if (bitmap != decodeByteArray) {
                        decodeByteArray.recycle();
                    }
                }
            }
        }
        if (bitmap != null && bitmap.getByteCount() > 104857600) {
            final int n = bitmap.getWidth() / 2;
            final int n2 = bitmap.getHeight() / 2;
            if (n > 0 && n2 > 0) {
                final Bitmap scaledBitmap = Bitmap.createScaledBitmap(bitmap, n, n2, true);
                if (scaledBitmap != bitmap) {
                    bitmap.recycle();
                }
                return scaledBitmap;
            }
        }
        return bitmap;
    }
}

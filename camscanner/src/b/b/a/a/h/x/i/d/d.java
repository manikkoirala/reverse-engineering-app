// 
// Decompiled by Procyon v0.6.0
// 

package b.b.a.a.h.x.i.d;

import java.io.IOException;
import java.io.File;
import java.nio.charset.Charset;

final class d
{
    static final Charset \u3007080;
    
    static {
        \u3007080 = Charset.forName("US-ASCII");
        Charset.forName("UTF-8");
    }
    
    static void \u3007080(File file) throws IOException {
        final File[] listFiles = file.listFiles();
        if (listFiles != null) {
            for (int length = listFiles.length, i = 0; i < length; ++i) {
                file = listFiles[i];
                if (file.isDirectory()) {
                    \u3007080(file);
                }
                if (!file.delete()) {
                    final StringBuilder sb = new StringBuilder();
                    sb.append("failed to delete file: ");
                    sb.append(file);
                    throw new IOException(sb.toString());
                }
            }
            return;
        }
        final StringBuilder sb2 = new StringBuilder();
        sb2.append("not a readable directory: ");
        sb2.append(file);
        throw new IOException(sb2.toString());
    }
}

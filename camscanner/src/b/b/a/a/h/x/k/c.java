// 
// Decompiled by Procyon v0.6.0
// 

package b.b.a.a.h.x.k;

import java.security.MessageDigest;

public class c
{
    private static final char[] \u3007080;
    
    static {
        \u3007080 = new char[] { '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'a', 'b', 'c', 'd', 'e', 'f' };
    }
    
    public static String \u3007080(String \u3007o00\u3007\u3007Oo) {
        Label_0040: {
            if (\u3007o00\u3007\u3007Oo == null) {
                break Label_0040;
            }
            try {
                if (\u3007o00\u3007\u3007Oo.length() == 0) {
                    return null;
                }
                final MessageDigest instance = MessageDigest.getInstance("MD5");
                instance.update(\u3007o00\u3007\u3007Oo.getBytes("UTF-8"));
                \u3007o00\u3007\u3007Oo = \u3007o00\u3007\u3007Oo(instance.digest());
                return \u3007o00\u3007\u3007Oo;
            }
            catch (final Exception ex) {
                return null;
            }
        }
    }
    
    public static String \u3007o00\u3007\u3007Oo(final byte[] array) {
        if (array != null) {
            return \u3007o\u3007(array, 0, array.length);
        }
        throw new NullPointerException("bytes is null");
    }
    
    public static String \u3007o\u3007(final byte[] array, final int n, final int n2) {
        if (array == null) {
            throw new NullPointerException("bytes is null");
        }
        if (n >= 0 && n + n2 <= array.length) {
            final int count = n2 * 2;
            final char[] value = new char[count];
            int i = 0;
            int n3 = 0;
            while (i < n2) {
                final int n4 = array[i + n] & 0xFF;
                final int n5 = n3 + 1;
                final char[] \u3007080 = c.\u3007080;
                value[n3] = \u3007080[n4 >> 4];
                n3 = n5 + 1;
                value[n5] = \u3007080[n4 & 0xF];
                ++i;
            }
            return new String(value, 0, count);
        }
        throw new IndexOutOfBoundsException();
    }
}

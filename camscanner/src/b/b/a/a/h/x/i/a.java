// 
// Decompiled by Procyon v0.6.0
// 

package b.b.a.a.h.x.i;

import android.os.StatFs;
import android.os.Environment;
import android.content.Context;
import java.io.File;
import b.b.a.a.h.b;

public class a implements b, Cloneable
{
    private static volatile b O8o08O8O;
    private boolean OO;
    private long o0;
    private File o\u300700O;
    private boolean \u300708O\u300700\u3007o;
    private int \u3007OOo8\u30070;
    
    public a(final int n, final long n2, final File file) {
        this(n, n2, n != 0, n2 != 0L, file);
    }
    
    public a(final int \u3007oOo8\u30070, final long o0, final boolean oo, final boolean \u300708O\u300700\u3007o, final File o\u300700O) {
        this.o0 = o0;
        this.\u3007OOo8\u30070 = \u3007oOo8\u30070;
        this.OO = oo;
        this.\u300708O\u300700\u3007o = \u300708O\u300700\u3007o;
        this.o\u300700O = o\u300700O;
    }
    
    public static b O8() {
        return a.O8o08O8O;
    }
    
    public static b \u3007080(final File file) {
        file.mkdirs();
        int a;
        long a2;
        if (b.b.a.a.h.x.i.a.O8o08O8O == null) {
            a = Math.min(Runtime.getRuntime().maxMemory().intValue() / 16, 31457280);
            a2 = Math.min(\u3007o\u3007() / 16L, 41943040L);
        }
        else {
            a = Math.min(b.b.a.a.h.x.i.a.O8o08O8O.f() / 2, 31457280);
            a2 = Math.min(b.b.a.a.h.x.i.a.O8o08O8O.d() / 2L, 41943040L);
        }
        return new a(Math.max(a, 26214400), Math.max(a2, 20971520L), file);
    }
    
    public static void \u3007o00\u3007\u3007Oo(final Context context, final b o8o08O8O) {
        if (o8o08O8O != null) {
            a.O8o08O8O = o8o08O8O;
            return;
        }
        a.O8o08O8O = \u3007080(new File(context.getCacheDir(), "image"));
    }
    
    private static long \u3007o\u3007() {
        final StatFs statFs = new StatFs(Environment.getDataDirectory().getPath());
        return statFs.getAvailableBlocks() * (long)statFs.getBlockSize();
    }
    
    @Override
    public boolean a() {
        return this.OO;
    }
    
    @Override
    public File c() {
        return this.o\u300700O;
    }
    
    @Override
    public long d() {
        return this.o0;
    }
    
    @Override
    public boolean e() {
        return this.\u300708O\u300700\u3007o;
    }
    
    @Override
    public int f() {
        return this.\u3007OOo8\u30070;
    }
}

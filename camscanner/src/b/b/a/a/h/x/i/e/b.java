// 
// Decompiled by Procyon v0.6.0
// 

package b.b.a.a.h.x.i.e;

import android.graphics.Bitmap;
import b.b.a.a.h.x.i.c;
import b.b.a.a.h.q;

public class b implements q
{
    private int \u3007080;
    private c<String, Bitmap> \u3007o00\u3007\u3007Oo;
    
    public b(final int n, final int \u3007080) {
        this.\u3007080 = \u3007080;
        this.\u3007o00\u3007\u3007Oo = new c<String, Bitmap>(this, n) {
            protected int oO80(final String s, final Bitmap bitmap) {
                if (bitmap == null) {
                    return 0;
                }
                return b.\u3007080(bitmap);
            }
        };
    }
    
    public static int \u3007080(final Bitmap bitmap) {
        if (bitmap == null) {
            return 0;
        }
        return bitmap.getAllocationByteCount();
    }
    
    public boolean \u3007o00\u3007\u3007Oo(final String s, final Bitmap bitmap) {
        if (s != null && bitmap != null) {
            this.\u3007o00\u3007\u3007Oo.\u3007o00\u3007\u3007Oo(s, bitmap);
            return true;
        }
        return false;
    }
    
    public Bitmap \u3007o\u3007(final String s) {
        return this.\u3007o00\u3007\u3007Oo.o\u30070(s);
    }
}

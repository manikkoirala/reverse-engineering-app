// 
// Decompiled by Procyon v0.6.0
// 

package b.b.a.a.h.x.i;

import java.util.Map;
import java.lang.ref.SoftReference;
import java.util.LinkedHashMap;

public class c<K, V>
{
    private int O8;
    private int Oo08;
    private int oO80;
    private int o\u30070;
    private final LinkedHashMap<K, SoftReference<V>> \u3007080;
    private int \u3007o00\u3007\u3007Oo;
    private int \u3007o\u3007;
    private int \u3007\u3007888;
    
    public c(final int \u3007o\u3007) {
        if (\u3007o\u3007 > 0) {
            this.\u3007o\u3007 = \u3007o\u3007;
            this.\u3007080 = new LinkedHashMap<K, SoftReference<V>>(0, 0.75f, true);
            return;
        }
        throw new IllegalArgumentException("maxSize <= 0");
    }
    
    private int Oo08(final K obj, final V obj2) {
        final int \u3007\u3007888 = this.\u3007\u3007888(obj, obj2);
        if (\u3007\u3007888 >= 0) {
            return \u3007\u3007888;
        }
        final StringBuilder sb = new StringBuilder();
        sb.append("Negative size: ");
        sb.append(obj);
        sb.append("=");
        sb.append(obj2);
        throw new IllegalStateException(sb.toString());
    }
    
    protected void O8(final boolean b, final K k, final V v, final V v2) {
    }
    
    public final V o\u30070(final K k) {
        if (k != null) {
            synchronized (this) {
                final SoftReference softReference = this.\u3007080.get(k);
                Object o;
                if (softReference != null) {
                    o = softReference.get();
                    if (o != null) {
                        ++this.\u3007\u3007888;
                        return (V)o;
                    }
                    this.\u3007080.remove(k);
                }
                else {
                    o = null;
                }
                ++this.oO80;
                monitorexit(this);
                final V \u3007080 = this.\u3007080(k);
                if (\u3007080 == null) {
                    return null;
                }
                synchronized (this) {
                    ++this.Oo08;
                    final SoftReference<V> value = this.\u3007080.put(k, new SoftReference<V>(\u3007080));
                    if (value != null) {
                        o = value.get();
                    }
                    if (o != null) {
                        this.\u3007080.put(k, value);
                    }
                    else {
                        this.\u3007o00\u3007\u3007Oo += this.Oo08(k, \u3007080);
                    }
                    monitorexit(this);
                    if (o != null) {
                        this.O8(false, k, \u3007080, (V)o);
                        return (V)o;
                    }
                    this.\u3007o\u3007(this.\u3007o\u3007);
                    return \u3007080;
                }
            }
        }
        throw new NullPointerException("key == null");
    }
    
    @Override
    public final String toString() {
        synchronized (this) {
            final int \u3007\u3007888 = this.\u3007\u3007888;
            final int n = this.oO80 + \u3007\u3007888;
            int i;
            if (n != 0) {
                i = \u3007\u3007888 * 100 / n;
            }
            else {
                i = 0;
            }
            return String.format("LruCache[maxSize=%d,hits=%d,misses=%d,hitRate=%d%%]", this.\u3007o\u3007, this.\u3007\u3007888, this.oO80, i);
        }
    }
    
    protected V \u3007080(final K k) {
        return null;
    }
    
    public final V \u3007o00\u3007\u3007Oo(final K key, final V referent) {
        if (key != null && referent != null) {
            synchronized (this) {
                ++this.O8;
                this.\u3007o00\u3007\u3007Oo += this.Oo08(key, referent);
                final SoftReference<V> softReference = this.\u3007080.put(key, new SoftReference<V>(referent));
                V v;
                if (softReference != null) {
                    final V value = softReference.get();
                    if ((v = value) != null) {
                        this.\u3007o00\u3007\u3007Oo -= this.Oo08(key, value);
                        v = value;
                    }
                }
                else {
                    v = null;
                }
                monitorexit(this);
                if (v != null) {
                    this.O8(false, key, v, referent);
                }
                this.\u3007o\u3007(this.\u3007o\u3007);
                return v;
            }
        }
        throw new NullPointerException("key == null || value == null");
    }
    
    public void \u3007o\u3007(final int i) {
        while (true) {
            synchronized (this) {
                if (this.\u3007o00\u3007\u3007Oo >= 0 && (!this.\u3007080.isEmpty() || this.\u3007o00\u3007\u3007Oo == 0)) {
                    if (this.\u3007o00\u3007\u3007Oo <= i) {
                        monitorexit(this);
                    }
                    else {
                        final Map.Entry entry = (Map.Entry)this.\u3007080.entrySet().iterator().next();
                        if (entry != null) {
                            final Object key = entry.getKey();
                            final SoftReference softReference = (SoftReference)entry.getValue();
                            this.\u3007080.remove(key);
                            Object value;
                            if (softReference != null) {
                                value = softReference.get();
                                this.\u3007o00\u3007\u3007Oo -= this.Oo08((K)key, (V)value);
                            }
                            else {
                                value = null;
                            }
                            ++this.o\u30070;
                            monitorexit(this);
                            this.O8(true, (K)key, (V)value, null);
                            continue;
                        }
                        monitorexit(this);
                    }
                }
                else {
                    final StringBuilder sb = new StringBuilder();
                    sb.append("oom maybe occured, clear cache. size= ");
                    sb.append(this.\u3007o00\u3007\u3007Oo);
                    sb.append(", maxSize: ");
                    sb.append(i);
                    this.\u3007o00\u3007\u3007Oo = 0;
                    this.\u3007080.clear();
                }
            }
        }
    }
    
    protected int \u3007\u3007888(final K k, final V v) {
        throw null;
    }
}

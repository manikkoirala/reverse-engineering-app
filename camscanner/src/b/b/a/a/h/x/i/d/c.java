// 
// Decompiled by Procyon v0.6.0
// 

package b.b.a.a.h.x.i.d;

import java.io.UnsupportedEncodingException;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.EOFException;
import java.nio.charset.Charset;
import java.io.InputStream;
import java.io.Closeable;

class c implements Closeable
{
    private byte[] OO;
    private final InputStream o0;
    private int o\u300700O;
    private int \u300708O\u300700\u3007o;
    private final Charset \u3007OOo8\u30070;
    
    public c(final InputStream o0, final int n, final Charset \u3007oOo8\u30070) {
        if (o0 == null || \u3007oOo8\u30070 == null) {
            throw null;
        }
        if (n < 0) {
            throw new IllegalArgumentException("capacity <= 0");
        }
        if (\u3007oOo8\u30070.equals(d.\u3007080)) {
            this.o0 = o0;
            this.\u3007OOo8\u30070 = \u3007oOo8\u30070;
            this.OO = new byte[n];
            return;
        }
        throw new IllegalArgumentException("Unsupported encoding");
    }
    
    public c(final InputStream inputStream, final Charset charset) {
        this(inputStream, 8192, charset);
    }
    
    private void \u3007\u3007888() throws IOException {
        final InputStream o0 = this.o0;
        final byte[] oo = this.OO;
        final int read = o0.read(oo, 0, oo.length);
        if (read != -1) {
            this.\u300708O\u300700\u3007o = 0;
            this.o\u300700O = read;
            return;
        }
        throw new EOFException();
    }
    
    @Override
    public void close() throws IOException {
        synchronized (this.o0) {
            if (this.OO != null) {
                this.OO = null;
                this.o0.close();
            }
        }
    }
    
    public boolean oO80() {
        return this.o\u300700O == -1;
    }
    
    public String \u30078o8o\u3007() throws IOException {
        synchronized (this.o0) {
            if (this.OO != null) {
                if (this.\u300708O\u300700\u3007o >= this.o\u300700O) {
                    this.\u3007\u3007888();
                }
                for (int i = this.\u300708O\u300700\u3007o; i != this.o\u300700O; ++i) {
                    final byte[] oo = this.OO;
                    if (oo[i] == 10) {
                        final int \u300708O\u300700\u3007o = this.\u300708O\u300700\u3007o;
                        int n = 0;
                        Label_0087: {
                            if (i != \u300708O\u300700\u3007o) {
                                n = i - 1;
                                if (oo[n] == 13) {
                                    break Label_0087;
                                }
                            }
                            n = i;
                        }
                        final String s = new String(oo, \u300708O\u300700\u3007o, n - \u300708O\u300700\u3007o, this.\u3007OOo8\u30070.name());
                        this.\u300708O\u300700\u3007o = i + 1;
                        return s;
                    }
                }
                final ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream(this, this.o\u300700O - this.\u300708O\u300700\u3007o + 80) {
                    final c o0;
                    
                    @Override
                    public String toString() {
                        int count;
                        final int n = count = super.count;
                        if (n > 0) {
                            final byte[] buf = super.buf;
                            final int n2 = n - 1;
                            count = n;
                            if (buf[n2] == 13) {
                                count = n2;
                            }
                        }
                        try {
                            return new String(super.buf, 0, count, this.o0.\u3007OOo8\u30070.name());
                        }
                        catch (final UnsupportedEncodingException detailMessage) {
                            throw new AssertionError((Object)detailMessage);
                        }
                    }
                };
                int j = 0;
                byte[] oo3 = null;
            Block_10:
                while (true) {
                    final byte[] oo2 = this.OO;
                    final int \u300708O\u300700\u3007o2 = this.\u300708O\u300700\u3007o;
                    byteArrayOutputStream.write(oo2, \u300708O\u300700\u3007o2, this.o\u300700O - \u300708O\u300700\u3007o2);
                    this.o\u300700O = -1;
                    this.\u3007\u3007888();
                    for (j = this.\u300708O\u300700\u3007o; j != this.o\u300700O; ++j) {
                        oo3 = this.OO;
                        if (oo3[j] == 10) {
                            break Block_10;
                        }
                    }
                }
                final int \u300708O\u300700\u3007o3 = this.\u300708O\u300700\u3007o;
                if (j != \u300708O\u300700\u3007o3) {
                    byteArrayOutputStream.write(oo3, \u300708O\u300700\u3007o3, j - \u300708O\u300700\u3007o3);
                }
                this.\u300708O\u300700\u3007o = j + 1;
                return byteArrayOutputStream.toString();
            }
            throw new IOException("LineReader is closed");
        }
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package b.b.a.a.h.x.i.d;

import java.util.Arrays;
import java.io.FilterOutputStream;
import java.io.FileNotFoundException;
import java.util.Collection;
import java.util.ArrayList;
import java.util.Map;
import java.util.Iterator;
import b.b.a.a.h.x.k.b;
import java.io.EOFException;
import java.io.BufferedWriter;
import java.io.OutputStreamWriter;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.io.Writer;
import java.util.LinkedHashMap;
import java.io.File;
import java.io.OutputStream;
import java.util.regex.Pattern;
import java.io.Closeable;

public final class a implements Closeable
{
    static final Pattern O0O;
    public static final OutputStream o8oOOo;
    private long O8o08O8O;
    private final File OO;
    private int OO\u300700\u30078oO;
    private final File o0;
    private long o8\u3007OO0\u30070o;
    private final LinkedHashMap<String, d> oOo0;
    private Writer oOo\u30078o008;
    final ExecutorService ooo0\u3007\u3007O;
    private final int o\u300700O;
    private final int \u3007080OO8\u30070;
    private final File \u300708O\u300700\u3007o;
    private long \u30070O;
    private long \u30078\u3007oO\u3007\u30078o;
    private final File \u3007OOo8\u30070;
    private final Callable<Void> \u3007\u300708O;
    
    static {
        O0O = Pattern.compile("[a-z0-9_-]{1,120}");
        o8oOOo = new OutputStream() {
            @Override
            public void write(final int n) throws IOException {
            }
        };
    }
    
    private a(final File file, final int o\u300700O, final int \u3007080OO8\u30070, final long o8o08O8O, final ExecutorService ooo0\u3007\u3007O) {
        this.\u30070O = 0L;
        this.oOo0 = new LinkedHashMap<String, d>(0, 0.75f, true);
        this.o8\u3007OO0\u30070o = -1L;
        this.\u30078\u3007oO\u3007\u30078o = 0L;
        this.\u3007\u300708O = new Callable<Void>() {
            final a o0;
            
            public Void \u3007080() throws Exception {
                synchronized (this.o0) {
                    if (this.o0.oOo\u30078o008 == null) {
                        return null;
                    }
                    this.o0.o\u3007O();
                    if (this.o0.\u3007\u3007\u30070\u3007\u30070()) {
                        this.o0.o\u30078oOO88();
                        this.o0.OO\u300700\u30078oO = 0;
                    }
                    return null;
                }
            }
        };
        this.o0 = file;
        this.o\u300700O = o\u300700O;
        this.\u3007OOo8\u30070 = new File(file, "journal");
        this.OO = new File(file, "journal.tmp");
        this.\u300708O\u300700\u3007o = new File(file, "journal.bkp");
        this.\u3007080OO8\u30070 = \u3007080OO8\u30070;
        this.O8o08O8O = o8o08O8O;
        this.ooo0\u3007\u3007O = ooo0\u3007\u3007O;
    }
    
    private void O08000(final String s) {
        if (a.O0O.matcher(s).matches()) {
            return;
        }
        final StringBuilder sb = new StringBuilder();
        sb.append("keys must match regex [a-z0-9_-]{1,120}: \"");
        sb.append(s);
        sb.append("\"");
        throw new IllegalArgumentException(sb.toString());
    }
    
    private void O8\u3007o(final String s) throws IOException {
        final int index = s.indexOf(32);
        if (index != -1) {
            final int beginIndex = index + 1;
            final int index2 = s.indexOf(32, beginIndex);
            String s2;
            if (index2 == -1) {
                final String key = s2 = s.substring(beginIndex);
                if (index == 6) {
                    s2 = key;
                    if (s.startsWith("REMOVE")) {
                        this.oOo0.remove(key);
                        return;
                    }
                }
            }
            else {
                s2 = s.substring(beginIndex, index2);
            }
            d value;
            if ((value = this.oOo0.get(s2)) == null) {
                value = new d(s2);
                this.oOo0.put(s2, value);
            }
            if (index2 != -1 && index == 5 && s.startsWith("CLEAN")) {
                final String[] split = s.substring(index2 + 1).split(" ");
                value.\u3007o\u3007 = true;
                value.O8 = null;
                value.\u30078o8o\u3007(split);
            }
            else if (index2 == -1 && index == 5 && s.startsWith("DIRTY")) {
                value.O8 = new c(value);
            }
            else if (index2 != -1 || index != 4 || !s.startsWith("READ")) {
                final StringBuilder sb = new StringBuilder();
                sb.append("unexpected journal line: ");
                sb.append(s);
                throw new IOException(sb.toString());
            }
            return;
        }
        final StringBuilder sb2 = new StringBuilder();
        sb2.append("unexpected journal line: ");
        sb2.append(s);
        throw new IOException(sb2.toString());
    }
    
    private void OO0o\u3007\u3007() {
        if (this.oOo\u30078o008 != null) {
            return;
        }
        throw new IllegalStateException("cache is closed");
    }
    
    private void O\u3007O\u3007oO() throws IOException {
        final b.b.a.a.h.x.i.d.c c = new b.b.a.a.h.x.i.d.c(new FileInputStream(this.\u3007OOo8\u30070), b.b.a.a.h.x.i.d.d.\u3007080);
        try {
            final String \u30078o8o\u3007 = c.\u30078o8o\u3007();
            final String \u30078o8o\u30072 = c.\u30078o8o\u3007();
            final String \u30078o8o\u30073 = c.\u30078o8o\u3007();
            final String \u30078o8o\u30074 = c.\u30078o8o\u3007();
            final String \u30078o8o\u30075 = c.\u30078o8o\u3007();
            if ("libcore.io.DiskLruCache".equals(\u30078o8o\u3007) && "1".equals(\u30078o8o\u30072) && Integer.toString(this.o\u300700O).equals(\u30078o8o\u30073) && Integer.toString(this.\u3007080OO8\u30070).equals(\u30078o8o\u30074) && "".equals(\u30078o8o\u30075)) {
                int n = 0;
                try {
                    while (true) {
                        this.O8\u3007o(c.\u30078o8o\u3007());
                        ++n;
                    }
                }
                catch (final EOFException ex) {
                    this.OO\u300700\u30078oO = n - this.oOo0.size();
                    if (c.oO80()) {
                        this.o\u30078oOO88();
                    }
                    else {
                        this.oOo\u30078o008 = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(this.\u3007OOo8\u30070, true), b.b.a.a.h.x.i.d.d.\u3007080));
                    }
                    return;
                }
            }
            final StringBuilder sb = new StringBuilder();
            sb.append("unexpected journal header: [");
            sb.append(\u30078o8o\u3007);
            sb.append(", ");
            sb.append(\u30078o8o\u30072);
            sb.append(", ");
            sb.append(\u30078o8o\u30074);
            sb.append(", ");
            sb.append(\u30078o8o\u30075);
            sb.append("]");
            throw new IOException(sb.toString());
        }
        finally {
            b.\u3007080(c);
        }
    }
    
    private c oO80(final String str, final long n) throws IOException {
        synchronized (this) {
            this.OO0o\u3007\u3007();
            this.O08000(str);
            d value = this.oOo0.get(str);
            if (n != -1L && (value == null || value.Oo08 != n)) {
                return null;
            }
            if (value == null) {
                value = new d(str);
                this.oOo0.put(str, value);
            }
            else if (value.O8 != null) {
                return null;
            }
            final c c = new c(value);
            value.O8 = c;
            final Writer oOo\u30078o008 = this.oOo\u30078o008;
            final StringBuilder sb = new StringBuilder();
            sb.append("DIRTY ");
            sb.append(str);
            sb.append('\n');
            oOo\u30078o008.write(sb.toString());
            this.oOo\u30078o008.flush();
            return c;
        }
    }
    
    private void o\u30078oOO88() throws IOException {
        synchronized (this) {
            final Writer oOo\u30078o008 = this.oOo\u30078o008;
            if (oOo\u30078o008 != null) {
                oOo\u30078o008.close();
            }
            BufferedWriter oOo\u30078o9 = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(this.OO), b.b.a.a.h.x.i.d.d.\u3007080));
            try {
                oOo\u30078o9.write("libcore.io.DiskLruCache");
                oOo\u30078o9.write("\n");
                oOo\u30078o9.write("1");
                oOo\u30078o9.write("\n");
                oOo\u30078o9.write(Integer.toString(this.o\u300700O));
                oOo\u30078o9.write("\n");
                oOo\u30078o9.write(Integer.toString(this.\u3007080OO8\u30070));
                oOo\u30078o9.write("\n");
                oOo\u30078o9.write("\n");
                for (final d d : this.oOo0.values()) {
                    if (d.O8 != null) {
                        final StringBuilder sb = new StringBuilder();
                        sb.append("DIRTY ");
                        sb.append(d.\u3007080);
                        sb.append('\n');
                        oOo\u30078o9.write(sb.toString());
                    }
                    else {
                        final StringBuilder sb2 = new StringBuilder();
                        sb2.append("CLEAN ");
                        sb2.append(d.\u3007080);
                        sb2.append(d.Oo08());
                        sb2.append('\n');
                        oOo\u30078o9.write(sb2.toString());
                    }
                }
                oOo\u30078o9.close();
                if (this.\u3007OOo8\u30070.exists()) {
                    \u3007\u30078O0\u30078(this.\u3007OOo8\u30070, this.\u300708O\u300700\u3007o, true);
                }
                \u3007\u30078O0\u30078(this.OO, this.\u3007OOo8\u30070, false);
                this.\u300708O\u300700\u3007o.delete();
                oOo\u30078o9 = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(this.\u3007OOo8\u30070, true), b.b.a.a.h.x.i.d.d.\u3007080));
                this.oOo\u30078o008 = oOo\u30078o9;
            }
            finally {
                oOo\u30078o9.close();
            }
        }
    }
    
    private void o\u3007O() throws IOException {
        long o8o08O8O = this.O8o08O8O;
        final long o8\u3007OO0\u30070o = this.o8\u3007OO0\u30070o;
        if (o8\u3007OO0\u30070o >= 0L) {
            o8o08O8O = o8\u3007OO0\u30070o;
        }
        while (this.\u30070O > o8o08O8O) {
            this.oO(((Map.Entry)this.oOo0.entrySet().iterator().next()).getKey());
        }
        this.o8\u3007OO0\u30070o = -1L;
    }
    
    private void \u30078() throws IOException {
        \u3007O00(this.OO);
        final Iterator<d> iterator = this.oOo0.values().iterator();
        while (iterator.hasNext()) {
            final d d = iterator.next();
            final c oooo8o0\u3007 = d.O8;
            final int n = 0;
            int i = 0;
            if (oooo8o0\u3007 == null) {
                while (i < this.\u3007080OO8\u30070) {
                    this.\u30070O += d.\u3007o00\u3007\u3007Oo[i];
                    ++i;
                }
            }
            else {
                d.O8 = null;
                for (int j = n; j < this.\u3007080OO8\u30070; ++j) {
                    \u3007O00(d.\u3007o\u3007(j));
                    \u3007O00(d.\u300780\u3007808\u3007O(j));
                }
                iterator.remove();
            }
        }
    }
    
    public static a \u30078o8o\u3007(final File obj, final int n, final int n2, final long n3, final ExecutorService executorService) throws IOException {
        if (n3 <= 0L) {
            throw new IllegalArgumentException("maxSize <= 0");
        }
        if (n2 > 0) {
            final File file = new File(obj, "journal.bkp");
            if (file.exists()) {
                final File file2 = new File(obj, "journal");
                if (file2.exists()) {
                    file.delete();
                }
                else {
                    \u3007\u30078O0\u30078(file, file2, false);
                }
            }
            final a a = new a(obj, n, n2, n3, executorService);
            if (a.\u3007OOo8\u30070.exists()) {
                try {
                    a.O\u3007O\u3007oO();
                    a.\u30078();
                    return a;
                }
                catch (final IOException ex) {
                    final StringBuilder sb = new StringBuilder();
                    sb.append(obj);
                    sb.append(" is corrupt: ");
                    sb.append(ex.getMessage());
                    sb.append(", removing");
                    a.\u3007oo\u3007();
                }
            }
            obj.mkdirs();
            final a a2 = new a(obj, n, n2, n3, executorService);
            a2.o\u30078oOO88();
            return a2;
        }
        throw new IllegalArgumentException("valueCount <= 0");
    }
    
    private static void \u3007O00(final File file) throws IOException {
        if (file.exists() && !file.delete()) {
            throw new IOException();
        }
    }
    
    private void \u3007\u3007808\u3007(final c c, final boolean b) throws IOException {
        synchronized (this) {
            final d \u3007080 = c.\u3007080;
            if (\u3007080.O8 == c) {
                int i;
                final int n = i = 0;
                if (b) {
                    i = n;
                    if (!\u3007080.\u3007o\u3007) {
                        int j = 0;
                        while (true) {
                            i = n;
                            if (j >= this.\u3007080OO8\u30070) {
                                break;
                            }
                            if (!c.\u3007o00\u3007\u3007Oo[j]) {
                                c.\u3007o\u3007();
                                final StringBuilder sb = new StringBuilder();
                                sb.append("Newly created entry didn't create value for index ");
                                sb.append(j);
                                throw new IllegalStateException(sb.toString());
                            }
                            if (!\u3007080.\u300780\u3007808\u3007O(j).exists()) {
                                c.\u3007o\u3007();
                                return;
                            }
                            ++j;
                        }
                    }
                }
                while (i < this.\u3007080OO8\u30070) {
                    final File \u300780\u3007808\u3007O = \u3007080.\u300780\u3007808\u3007O(i);
                    if (b) {
                        if (\u300780\u3007808\u3007O.exists()) {
                            final File \u3007o\u3007 = \u3007080.\u3007o\u3007(i);
                            \u300780\u3007808\u3007O.renameTo(\u3007o\u3007);
                            final long n2 = \u3007080.\u3007o00\u3007\u3007Oo[i];
                            final long length = \u3007o\u3007.length();
                            \u3007080.\u3007o00\u3007\u3007Oo[i] = length;
                            this.\u30070O = this.\u30070O - n2 + length;
                        }
                    }
                    else {
                        \u3007O00(\u300780\u3007808\u3007O);
                    }
                    ++i;
                }
                ++this.OO\u300700\u30078oO;
                \u3007080.O8 = null;
                if (\u3007080.\u3007o\u3007 | b) {
                    \u3007080.\u3007o\u3007 = true;
                    final Writer oOo\u30078o008 = this.oOo\u30078o008;
                    final StringBuilder sb2 = new StringBuilder();
                    sb2.append("CLEAN ");
                    sb2.append(\u3007080.\u3007080);
                    sb2.append(\u3007080.Oo08());
                    sb2.append('\n');
                    oOo\u30078o008.write(sb2.toString());
                    if (b) {
                        final long \u30078\u3007oO\u3007\u30078o = this.\u30078\u3007oO\u3007\u30078o;
                        this.\u30078\u3007oO\u3007\u30078o = 1L + \u30078\u3007oO\u3007\u30078o;
                        \u3007080.Oo08 = \u30078\u3007oO\u3007\u30078o;
                    }
                }
                else {
                    this.oOo0.remove(\u3007080.\u3007080);
                    final Writer oOo\u30078o9 = this.oOo\u30078o008;
                    final StringBuilder sb3 = new StringBuilder();
                    sb3.append("REMOVE ");
                    sb3.append(\u3007080.\u3007080);
                    sb3.append('\n');
                    oOo\u30078o9.write(sb3.toString());
                }
                this.oOo\u30078o008.flush();
                if (this.\u30070O > this.O8o08O8O || this.\u3007\u3007\u30070\u3007\u30070()) {
                    this.ooo0\u3007\u3007O.submit(this.\u3007\u300708O);
                }
                return;
            }
            throw new IllegalStateException();
        }
    }
    
    private static void \u3007\u30078O0\u30078(final File file, final File dest, final boolean b) throws IOException {
        if (b) {
            \u3007O00(dest);
        }
        if (file.renameTo(dest)) {
            return;
        }
        throw new IOException();
    }
    
    private boolean \u3007\u3007\u30070\u3007\u30070() {
        final int oo\u300700\u30078oO = this.OO\u300700\u30078oO;
        return oo\u300700\u30078oO >= 2000 && oo\u300700\u30078oO >= this.oOo0.size();
    }
    
    public void OOO\u3007O0() throws IOException {
        synchronized (this) {
            this.OO0o\u3007\u3007();
            this.o\u3007O();
            this.oOo\u30078o008.flush();
        }
    }
    
    @Override
    public void close() throws IOException {
        synchronized (this) {
            if (this.oOo\u30078o008 == null) {
                return;
            }
            for (final d d : new ArrayList(this.oOo0.values())) {
                if (d.O8 != null) {
                    d.O8.\u3007o\u3007();
                }
            }
            this.o\u3007O();
            this.oOo\u30078o008.close();
            this.oOo\u30078o008 = null;
        }
    }
    
    public e o800o8O(final String p0) throws IOException {
        // 
        // This method could not be decompiled.
        // 
        // Original Bytecode:
        // 
        //     1: monitorenter   
        //     2: aload_0        
        //     3: invokespecial   b/b/a/a/h/x/i/d/a.OO0o\u3007\u3007:()V
        //     6: aload_0        
        //     7: aload_1        
        //     8: invokespecial   b/b/a/a/h/x/i/d/a.O08000:(Ljava/lang/String;)V
        //    11: aload_0        
        //    12: getfield        b/b/a/a/h/x/i/d/a.oOo0:Ljava/util/LinkedHashMap;
        //    15: aload_1        
        //    16: invokevirtual   java/util/LinkedHashMap.get:(Ljava/lang/Object;)Ljava/lang/Object;
        //    19: checkcast       Lb/b/a/a/h/x/i/d/a$d;
        //    22: astore          7
        //    24: aload           7
        //    26: ifnonnull       33
        //    29: aload_0        
        //    30: monitorexit    
        //    31: aconst_null    
        //    32: areturn        
        //    33: aload           7
        //    35: invokestatic    b/b/a/a/h/x/i/d/a$d.OO0o\u3007\u3007:(Lb/b/a/a/h/x/i/d/a$d;)Z
        //    38: istore          4
        //    40: iload           4
        //    42: ifne            49
        //    45: aload_0        
        //    46: monitorexit    
        //    47: aconst_null    
        //    48: areturn        
        //    49: aload_0        
        //    50: getfield        b/b/a/a/h/x/i/d/a.\u3007080OO8\u30070:I
        //    53: anewarray       Ljava/io/InputStream;
        //    56: astore          5
        //    58: iconst_0       
        //    59: istore_3       
        //    60: iconst_0       
        //    61: istore_2       
        //    62: iload_2        
        //    63: aload_0        
        //    64: getfield        b/b/a/a/h/x/i/d/a.\u3007080OO8\u30070:I
        //    67: if_icmpge       93
        //    70: aload           5
        //    72: iload_2        
        //    73: new             Ljava/io/FileInputStream;
        //    76: dup            
        //    77: aload           7
        //    79: iload_2        
        //    80: invokevirtual   b/b/a/a/h/x/i/d/a$d.\u3007o\u3007:(I)Ljava/io/File;
        //    83: invokespecial   java/io/FileInputStream.<init>:(Ljava/io/File;)V
        //    86: aastore        
        //    87: iinc            2, 1
        //    90: goto            62
        //    93: aload_0        
        //    94: aload_0        
        //    95: getfield        b/b/a/a/h/x/i/d/a.OO\u300700\u30078oO:I
        //    98: iconst_1       
        //    99: iadd           
        //   100: putfield        b/b/a/a/h/x/i/d/a.OO\u300700\u30078oO:I
        //   103: aload_0        
        //   104: getfield        b/b/a/a/h/x/i/d/a.oOo\u30078o008:Ljava/io/Writer;
        //   107: astore          6
        //   109: new             Ljava/lang/StringBuilder;
        //   112: astore          8
        //   114: aload           8
        //   116: invokespecial   java/lang/StringBuilder.<init>:()V
        //   119: aload           8
        //   121: ldc_w           "READ "
        //   124: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   127: pop            
        //   128: aload           8
        //   130: aload_1        
        //   131: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   134: pop            
        //   135: aload           8
        //   137: bipush          10
        //   139: invokevirtual   java/lang/StringBuilder.append:(C)Ljava/lang/StringBuilder;
        //   142: pop            
        //   143: aload           6
        //   145: aload           8
        //   147: invokevirtual   java/lang/StringBuilder.toString:()Ljava/lang/String;
        //   150: invokevirtual   java/io/Writer.append:(Ljava/lang/CharSequence;)Ljava/io/Writer;
        //   153: pop            
        //   154: aload_0        
        //   155: invokespecial   b/b/a/a/h/x/i/d/a.\u3007\u3007\u30070\u3007\u30070:()Z
        //   158: ifeq            175
        //   161: aload_0        
        //   162: getfield        b/b/a/a/h/x/i/d/a.ooo0\u3007\u3007O:Ljava/util/concurrent/ExecutorService;
        //   165: aload_0        
        //   166: getfield        b/b/a/a/h/x/i/d/a.\u3007\u300708O:Ljava/util/concurrent/Callable;
        //   169: invokeinterface java/util/concurrent/ExecutorService.submit:(Ljava/util/concurrent/Callable;)Ljava/util/concurrent/Future;
        //   174: pop            
        //   175: new             Lb/b/a/a/h/x/i/d/a$e;
        //   178: dup            
        //   179: aload_0        
        //   180: aload_1        
        //   181: aload           7
        //   183: invokestatic    b/b/a/a/h/x/i/d/a$d.\u3007O8o08O:(Lb/b/a/a/h/x/i/d/a$d;)J
        //   186: aload           5
        //   188: aload           7
        //   190: invokestatic    b/b/a/a/h/x/i/d/a$d.oO80:(Lb/b/a/a/h/x/i/d/a$d;)[J
        //   193: aconst_null    
        //   194: invokespecial   b/b/a/a/h/x/i/d/a$e.<init>:(Lb/b/a/a/h/x/i/d/a;Ljava/lang/String;J[Ljava/io/InputStream;[JLb/b/a/a/h/x/i/d/a$a;)V
        //   197: astore_1       
        //   198: aload_0        
        //   199: monitorexit    
        //   200: aload_1        
        //   201: areturn        
        //   202: iload_2        
        //   203: aload_0        
        //   204: getfield        b/b/a/a/h/x/i/d/a.\u3007080OO8\u30070:I
        //   207: if_icmpge       229
        //   210: aload           5
        //   212: iload_2        
        //   213: aaload         
        //   214: astore_1       
        //   215: aload_1        
        //   216: ifnull          229
        //   219: aload_1        
        //   220: invokestatic    b/b/a/a/h/x/k/b.\u3007080:(Ljava/io/Closeable;)V
        //   223: iinc            2, 1
        //   226: goto            202
        //   229: aload_0        
        //   230: monitorexit    
        //   231: aconst_null    
        //   232: areturn        
        //   233: astore_1       
        //   234: aload_0        
        //   235: monitorexit    
        //   236: aload_1        
        //   237: athrow         
        //   238: astore_1       
        //   239: iload_3        
        //   240: istore_2       
        //   241: goto            202
        //    Exceptions:
        //  throws java.io.IOException
        //    Exceptions:
        //  Try           Handler
        //  Start  End    Start  End    Type                           
        //  -----  -----  -----  -----  -------------------------------
        //  2      24     233    238    Any
        //  33     40     233    238    Any
        //  49     58     233    238    Any
        //  62     87     238    233    Ljava/io/FileNotFoundException;
        //  62     87     233    238    Any
        //  93     175    233    238    Any
        //  175    198    233    238    Any
        //  202    210    233    238    Any
        //  219    223    233    238    Any
        // 
        // The error that occurred was:
        // 
        // java.lang.IllegalStateException: Expression is linked from several locations: Label_0062:
        //     at com.strobel.decompiler.ast.Error.expressionLinkedFromMultipleLocations(Error.java:27)
        //     at com.strobel.decompiler.ast.AstOptimizer.mergeDisparateObjectInitializations(AstOptimizer.java:2604)
        //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:235)
        //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:42)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:206)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:93)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethodBody(AstBuilder.java:868)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethod(AstBuilder.java:761)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addTypeMembers(AstBuilder.java:638)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeCore(AstBuilder.java:605)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeNoCache(AstBuilder.java:195)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createType(AstBuilder.java:162)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addType(AstBuilder.java:137)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.buildAst(JavaLanguage.java:71)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.decompileType(JavaLanguage.java:59)
        //     at com.strobel.decompiler.DecompilerDriver.decompileType(DecompilerDriver.java:333)
        //     at com.strobel.decompiler.DecompilerDriver.decompileJar(DecompilerDriver.java:254)
        //     at com.strobel.decompiler.DecompilerDriver.main(DecompilerDriver.java:144)
        // 
        throw new IllegalStateException("An error occurred while decompiling this method.");
    }
    
    public boolean oO(final String key) throws IOException {
        synchronized (this) {
            this.OO0o\u3007\u3007();
            this.O08000(key);
            final d d = this.oOo0.get(key);
            int i = 0;
            if (d != null && d.O8 == null) {
                while (i < this.\u3007080OO8\u30070) {
                    final File \u3007o\u3007 = d.\u3007o\u3007(i);
                    if (\u3007o\u3007.exists() && !\u3007o\u3007.delete()) {
                        final StringBuilder sb = new StringBuilder();
                        sb.append("failed to delete ");
                        sb.append(\u3007o\u3007);
                        throw new IOException(sb.toString());
                    }
                    this.\u30070O -= d.\u3007o00\u3007\u3007Oo[i];
                    d.\u3007o00\u3007\u3007Oo[i] = 0L;
                    ++i;
                }
                ++this.OO\u300700\u30078oO;
                final Writer oOo\u30078o008 = this.oOo\u30078o008;
                final StringBuilder sb2 = new StringBuilder();
                sb2.append("REMOVE ");
                sb2.append(key);
                sb2.append('\n');
                oOo\u30078o008.append((CharSequence)sb2.toString());
                this.oOo0.remove(key);
                if (this.\u3007\u3007\u30070\u3007\u30070()) {
                    this.ooo0\u3007\u3007O.submit(this.\u3007\u300708O);
                }
                return true;
            }
            return false;
        }
    }
    
    public void \u3007oo\u3007() throws IOException {
        this.close();
        b.b.a.a.h.x.i.d.d.\u3007080(this.o0);
    }
    
    public c \u3007\u3007888(final String s) throws IOException {
        return this.oO80(s, -1L);
    }
    
    public final class c
    {
        final b.b.a.a.h.x.i.d.a O8;
        private final d \u3007080;
        private final boolean[] \u3007o00\u3007\u3007Oo;
        private boolean \u3007o\u3007;
        
        private c(final b.b.a.a.h.x.i.d.a o8, final d \u3007080) {
            this.O8 = o8;
            this.\u3007080 = \u3007080;
            boolean[] \u3007o00\u3007\u3007Oo;
            if (\u3007080.\u3007o\u3007) {
                \u3007o00\u3007\u3007Oo = null;
            }
            else {
                \u3007o00\u3007\u3007Oo = new boolean[o8.\u3007080OO8\u30070];
            }
            this.\u3007o00\u3007\u3007Oo = \u3007o00\u3007\u3007Oo;
        }
        
        public void Oo08() throws IOException {
            if (this.\u3007o\u3007) {
                this.O8.\u3007\u3007808\u3007(this, false);
                this.O8.oO(this.\u3007080.\u3007080);
            }
            else {
                this.O8.\u3007\u3007808\u3007(this, true);
            }
        }
        
        public OutputStream \u3007o00\u3007\u3007Oo(final int i) throws IOException {
            if (i >= 0 && i < this.O8.\u3007080OO8\u30070) {
                synchronized (this.O8) {
                    if (this.\u3007080.O8 == this) {
                        if (!this.\u3007080.\u3007o\u3007) {
                            this.\u3007o00\u3007\u3007Oo[i] = true;
                        }
                        final File \u300780\u3007808\u3007O = this.\u3007080.\u300780\u3007808\u3007O(i);
                        Label_0095: {
                            try {
                                final FileOutputStream fileOutputStream = new FileOutputStream(\u300780\u3007808\u3007O);
                                break Label_0095;
                            }
                            catch (final FileNotFoundException ex) {
                                this.O8.o0.mkdirs();
                                try {
                                    final FileOutputStream fileOutputStream = new FileOutputStream(\u300780\u3007808\u3007O);
                                    return new a((OutputStream)fileOutputStream);
                                }
                                catch (final FileNotFoundException ex2) {
                                    return b.b.a.a.h.x.i.d.a.o8oOOo;
                                }
                            }
                        }
                    }
                    throw new IllegalStateException();
                }
            }
            final StringBuilder sb = new StringBuilder();
            sb.append("Expected index ");
            sb.append(i);
            sb.append(" to be greater than 0 and less than the maximum value count of ");
            sb.append(this.O8.\u3007080OO8\u30070);
            throw new IllegalArgumentException(sb.toString());
        }
        
        public void \u3007o\u3007() throws IOException {
            this.O8.\u3007\u3007808\u3007(this, false);
        }
        
        private class a extends FilterOutputStream
        {
            final c o0;
            
            private a(final c o0, final OutputStream out) {
                this.o0 = o0;
                super(out);
            }
            
            @Override
            public void close() {
                try {
                    super.out.close();
                }
                catch (final IOException ex) {
                    this.o0.\u3007o\u3007 = true;
                }
            }
            
            @Override
            public void flush() {
                try {
                    super.out.flush();
                }
                catch (final IOException ex) {
                    this.o0.\u3007o\u3007 = true;
                }
            }
            
            @Override
            public void write(final int n) {
                try {
                    super.out.write(n);
                }
                catch (final IOException ex) {
                    this.o0.\u3007o\u3007 = true;
                }
            }
            
            @Override
            public void write(final byte[] b, final int off, final int len) {
                try {
                    super.out.write(b, off, len);
                }
                catch (final IOException ex) {
                    this.o0.\u3007o\u3007 = true;
                }
            }
        }
    }
    
    private final class d
    {
        private c O8;
        private long Oo08;
        final a o\u30070;
        private final String \u3007080;
        private final long[] \u3007o00\u3007\u3007Oo;
        private boolean \u3007o\u3007;
        
        private d(final a o\u30070, final String \u3007080) {
            this.o\u30070 = o\u30070;
            this.\u3007080 = \u3007080;
            this.\u3007o00\u3007\u3007Oo = new long[o\u30070.\u3007080OO8\u30070];
        }
        
        private IOException O8(final String[] a) throws IOException {
            final StringBuilder sb = new StringBuilder();
            sb.append("unexpected journal line: ");
            sb.append(Arrays.toString(a));
            throw new IOException(sb.toString());
        }
        
        private void \u30078o8o\u3007(final String[] array) throws IOException {
            if (array.length == this.o\u30070.\u3007080OO8\u30070) {
                int i = 0;
                try {
                    while (i < array.length) {
                        this.\u3007o00\u3007\u3007Oo[i] = Long.parseLong(array[i]);
                        ++i;
                    }
                    return;
                }
                catch (final NumberFormatException ex) {
                    this.O8(array);
                    throw null;
                }
            }
            this.O8(array);
            throw null;
        }
        
        public String Oo08() throws IOException {
            final StringBuilder sb = new StringBuilder();
            for (final long lng : this.\u3007o00\u3007\u3007Oo) {
                sb.append(' ');
                sb.append(lng);
            }
            return sb.toString();
        }
        
        public File \u300780\u3007808\u3007O(final int i) {
            final File \u300700\u30078 = this.o\u30070.o0;
            final StringBuilder sb = new StringBuilder();
            sb.append(this.\u3007080);
            sb.append(".");
            sb.append(i);
            sb.append(".tmp");
            return new File(\u300700\u30078, sb.toString());
        }
        
        public File \u3007o\u3007(final int i) {
            final File \u300700\u30078 = this.o\u30070.o0;
            final StringBuilder sb = new StringBuilder();
            sb.append(this.\u3007080);
            sb.append(".");
            sb.append(i);
            return new File(\u300700\u30078, sb.toString());
        }
    }
    
    public final class e implements Closeable
    {
        private final InputStream[] o0;
        
        private e(final a a, final String s, final long n, final InputStream[] o0, final long[] array) {
            this.o0 = o0;
        }
        
        public InputStream Oo08(final int n) {
            return this.o0[n];
        }
        
        @Override
        public void close() {
            final InputStream[] o0 = this.o0;
            for (int length = o0.length, i = 0; i < length; ++i) {
                b.\u3007080(o0[i]);
            }
        }
    }
}

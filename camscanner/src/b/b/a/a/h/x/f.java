// 
// Decompiled by Procyon v0.6.0
// 

package b.b.a.a.h.x;

import java.io.File;
import java.util.Collection;
import android.graphics.Bitmap$Config;
import android.widget.ImageView$ScaleType;
import b.b.a.a.h.x.i.e.e;
import b.b.a.a.h.v.b;
import b.b.a.a.h.x.i.a;
import java.util.HashMap;
import java.util.concurrent.ConcurrentHashMap;
import android.content.Context;
import b.b.a.a.h.l;
import b.b.a.a.h.q;
import b.b.a.a.h.m;
import b.b.a.a.h.p;
import java.util.List;
import b.b.a.a.h.d;
import java.util.concurrent.ExecutorService;
import b.b.a.a.h.c;
import b.b.a.a.h.r;
import java.util.Map;

public class f
{
    private Map<String, r> O8;
    private Map<String, c> Oo08;
    private ExecutorService oO80;
    private d o\u30070;
    private Map<String, List<b.b.a.a.h.x.c>> \u3007080;
    private p \u300780\u3007808\u3007O;
    private final m \u3007o00\u3007\u3007Oo;
    private Map<String, q> \u3007o\u3007;
    private l \u3007\u3007888;
    
    public f(final Context context, final m m) {
        this.\u3007080 = new ConcurrentHashMap<String, List<b.b.a.a.h.x.c>>();
        this.\u3007o\u3007 = new HashMap<String, q>();
        this.O8 = new HashMap<String, r>();
        this.Oo08 = new HashMap<String, c>();
        this.\u3007o00\u3007\u3007Oo = h.\u3007080(m);
        a.\u3007o00\u3007\u3007Oo(context, m.c());
    }
    
    private ExecutorService OO0o\u3007\u3007\u3007\u30070() {
        final ExecutorService h = this.\u3007o00\u3007\u3007Oo.h();
        if (h != null) {
            return h;
        }
        return b.b.a.a.h.v.c.\u3007080();
    }
    
    private l Oo08() {
        final l b = this.\u3007o00\u3007\u3007Oo.b();
        if (b != null) {
            return b;
        }
        return b.b.a.a.h.v.b.\u3007080();
    }
    
    private r oO80(final b.b.a.a.h.b b) {
        final r g = this.\u3007o00\u3007\u3007Oo.g();
        if (g != null) {
            return g;
        }
        return e.\u3007080(b.f());
    }
    
    private q o\u30070(final b.b.a.a.h.b b) {
        final q d = this.\u3007o00\u3007\u3007Oo.d();
        if (d != null) {
            return b.b.a.a.h.x.i.e.a.\u3007o00\u3007\u3007Oo(d);
        }
        return b.b.a.a.h.x.i.e.a.\u3007080(b.f());
    }
    
    private c \u3007080(final b.b.a.a.h.b b) {
        final c a = this.\u3007o00\u3007\u3007Oo.a();
        if (a != null) {
            return a;
        }
        return new b.b.a.a.h.x.i.d.b(b.c(), b.d(), this.\u30070\u3007O0088o());
    }
    
    private d \u3007o\u3007() {
        d d;
        if ((d = this.\u3007o00\u3007\u3007Oo.f()) == null) {
            d = b.b.a.a.h.w.b.\u3007080();
        }
        return d;
    }
    
    private p \u3007\u3007888() {
        p e;
        if ((e = this.\u3007o00\u3007\u3007Oo.e()) == null) {
            e = new g();
        }
        return e;
    }
    
    public b.b.a.a.h.x.j.a O8(final b.b.a.a.h.x.c c) {
        ImageView$ScaleType imageView$ScaleType;
        if ((imageView$ScaleType = c.d()) == null) {
            imageView$ScaleType = b.b.a.a.h.x.j.a.\u3007\u3007888;
        }
        Bitmap$Config bitmap$Config;
        if ((bitmap$Config = c.\u3007\u30078O0\u30078()) == null) {
            bitmap$Config = b.b.a.a.h.x.j.a.oO80;
        }
        return new b.b.a.a.h.x.j.a(c.b(), c.c(), imageView$ScaleType, bitmap$Config, c.\u3007oOO8O8(), c.O8ooOoo\u3007());
    }
    
    public r OO0o\u3007\u3007(final b.b.a.a.h.b b) {
        b.b.a.a.h.b o8 = b;
        if (b == null) {
            o8 = a.O8();
        }
        final String string = o8.c().toString();
        r oo80;
        if ((oo80 = this.O8.get(string)) == null) {
            oo80 = this.oO80(o8);
            this.O8.put(string, oo80);
        }
        return oo80;
    }
    
    public Collection<r> Oooo8o0\u3007() {
        return this.O8.values();
    }
    
    public ExecutorService \u30070\u3007O0088o() {
        if (this.oO80 == null) {
            this.oO80 = this.OO0o\u3007\u3007\u3007\u30070();
        }
        return this.oO80;
    }
    
    public c \u300780\u3007808\u3007O(final b.b.a.a.h.b b) {
        b.b.a.a.h.b o8 = b;
        if (b == null) {
            o8 = a.O8();
        }
        final String string = o8.c().toString();
        c \u3007080;
        if ((\u3007080 = this.Oo08.get(string)) == null) {
            \u3007080 = this.\u3007080(o8);
            this.Oo08.put(string, \u3007080);
        }
        return \u3007080;
    }
    
    public q \u30078o8o\u3007(final b.b.a.a.h.b b) {
        b.b.a.a.h.b o8 = b;
        if (b == null) {
            o8 = a.O8();
        }
        final String string = o8.c().toString();
        q o\u30070;
        if ((o\u30070 = this.\u3007o\u3007.get(string)) == null) {
            o\u30070 = this.o\u30070(o8);
            this.\u3007o\u3007.put(string, o\u30070);
        }
        return o\u30070;
    }
    
    public l \u3007O00() {
        if (this.\u3007\u3007888 == null) {
            this.\u3007\u3007888 = this.Oo08();
        }
        return this.\u3007\u3007888;
    }
    
    public Collection<c> \u3007O8o08O() {
        return this.Oo08.values();
    }
    
    public d \u3007O\u3007() {
        if (this.o\u30070 == null) {
            this.o\u30070 = this.\u3007o\u3007();
        }
        return this.o\u30070;
    }
    
    public c \u3007o00\u3007\u3007Oo(final String pathname) {
        return this.\u300780\u3007808\u3007O(a.\u3007080(new File(pathname)));
    }
    
    public Map<String, List<b.b.a.a.h.x.c>> \u3007\u3007808\u3007() {
        return this.\u3007080;
    }
    
    public p \u3007\u30078O0\u30078() {
        if (this.\u300780\u3007808\u3007O == null) {
            this.\u300780\u3007808\u3007O = this.\u3007\u3007888();
        }
        return this.\u300780\u3007808\u3007O;
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package b.b.a.a.h.x;

import android.view.View;
import b.b.a.a.h.j;
import android.graphics.Bitmap;
import b.b.a.a.h.k;
import java.util.concurrent.ExecutorService;
import java.io.File;
import android.text.TextUtils;
import android.os.Looper;
import java.util.concurrent.LinkedBlockingQueue;
import android.os.Handler;
import b.b.a.a.h.s;
import b.b.a.a.h.b;
import b.b.a.a.h.t;
import java.util.Queue;
import android.widget.ImageView;
import java.lang.ref.WeakReference;
import b.b.a.a.h.h;
import android.graphics.Bitmap$Config;
import b.b.a.a.h.g;
import android.widget.ImageView$ScaleType;
import b.b.a.a.h.u;
import b.b.a.a.h.o;
import b.b.a.a.h.i;

public class c implements i
{
    private o O8;
    private boolean OO0o\u3007\u3007;
    private u OO0o\u3007\u3007\u3007\u30070;
    private ImageView$ScaleType Oo08;
    private g OoO8;
    private boolean Oooo8o0\u3007;
    private int o800o8O;
    private int oO80;
    private a oo88o8O;
    private Bitmap$Config o\u30070;
    private int o\u3007O8\u3007\u3007o;
    private int \u300700;
    private String \u3007080;
    private boolean \u30070\u3007O0088o;
    private h \u300780\u3007808\u3007O;
    private WeakReference<ImageView> \u30078o8o\u3007;
    private Queue<b.b.a.a.h.y.i> \u3007O00;
    private f \u3007O888o0o;
    private volatile boolean \u3007O8o08O;
    private t \u3007O\u3007;
    private String \u3007o00\u3007\u3007Oo;
    private b.b.a.a.h.b \u3007oo\u3007;
    private String \u3007o\u3007;
    private s \u3007\u3007808\u3007;
    private int \u3007\u3007888;
    private final Handler \u3007\u30078O0\u30078;
    
    private c(final c c) {
        this.\u3007O00 = new LinkedBlockingQueue<b.b.a.a.h.y.i>();
        this.\u3007\u30078O0\u30078 = new Handler(Looper.getMainLooper());
        this.\u30070\u3007O0088o = true;
        this.\u3007080 = c.O8;
        this.O8 = new b(c.\u3007080);
        this.\u30078o8o\u3007 = new WeakReference<ImageView>(c.\u3007o00\u3007\u3007Oo);
        this.Oo08 = c.Oo08;
        this.o\u30070 = c.o\u30070;
        this.\u3007\u3007888 = c.\u3007\u3007888;
        this.oO80 = c.oO80;
        u oo0o\u3007\u3007\u3007\u30070;
        if (c.\u300780\u3007808\u3007O == null) {
            oo0o\u3007\u3007\u3007\u30070 = u.a;
        }
        else {
            oo0o\u3007\u3007\u3007\u30070 = c.\u300780\u3007808\u3007O;
        }
        this.OO0o\u3007\u3007\u3007\u30070 = oo0o\u3007\u3007\u3007\u30070;
        t \u3007o\u3007;
        if (c.OO0o\u3007\u3007\u3007\u30070 == null) {
            \u3007o\u3007 = t.b;
        }
        else {
            \u3007o\u3007 = c.OO0o\u3007\u3007\u3007\u30070;
        }
        this.\u3007O\u3007 = \u3007o\u3007;
        this.\u3007\u3007808\u3007 = c.\u30078o8o\u3007;
        this.\u3007oo\u3007 = this.\u3007080(c);
        if (!TextUtils.isEmpty((CharSequence)c.\u3007o\u3007)) {
            this.\u3007\u3007888(c.\u3007o\u3007);
            this.\u3007O8o08O(c.\u3007o\u3007);
        }
        this.OO0o\u3007\u3007 = c.\u3007O8o08O;
        this.Oooo8o0\u3007 = c.OO0o\u3007\u3007;
        this.\u3007O888o0o = c.\u3007O\u3007;
        this.\u300780\u3007808\u3007O = c.\u3007O00;
        this.\u300700 = c.\u30070\u3007O0088o;
        this.o\u3007O8\u3007\u3007o = c.\u3007\u30078O0\u30078;
        this.\u3007O00.add(new c());
    }
    
    private b.b.a.a.h.b \u3007080(final c c) {
        if (c.\u3007\u3007808\u3007 != null) {
            return c.\u3007\u3007808\u3007;
        }
        if (!TextUtils.isEmpty((CharSequence)c.Oooo8o0\u3007)) {
            return b.b.a.a.h.x.i.a.\u3007080(new File(c.Oooo8o0\u3007));
        }
        return b.b.a.a.h.x.i.a.O8();
    }
    
    private i \u3007o() {
        try {
            final f \u3007o888o0o = this.\u3007O888o0o;
            if (\u3007o888o0o == null) {
                final o o8 = this.O8;
                if (o8 != null) {
                    o8.a(1005, "not init !", null);
                }
                return this;
            }
            final ExecutorService \u30070\u3007O0088o = \u3007o888o0o.\u30070\u3007O0088o();
            if (\u30070\u3007O0088o != null) {
                \u30070\u3007O0088o.submit(new Runnable(this) {
                    final c o0;
                    
                    @Override
                    public void run() {
                        try {
                            while (!this.o0.\u3007O8o08O) {
                                final b.b.a.a.h.y.i i = this.o0.\u3007O00.poll();
                                if (i == null) {
                                    break;
                                }
                                if (this.o0.\u3007\u3007808\u3007 != null) {
                                    this.o0.\u3007\u3007808\u3007.b(i.a(), this.o0);
                                }
                                i.\u3007080(this.o0);
                                if (this.o0.\u3007\u3007808\u3007 == null) {
                                    continue;
                                }
                                this.o0.\u3007\u3007808\u3007.a(i.a(), this.o0);
                            }
                            if (this.o0.\u3007O8o08O) {
                                this.o0.\u3007o\u3007(1003, "canceled", null);
                            }
                        }
                        finally {
                            final Throwable t;
                            this.o0.\u3007o\u3007(2000, t.getMessage(), t);
                            if (this.o0.\u3007\u3007808\u3007 != null) {
                                this.o0.\u3007\u3007808\u3007.a("exception", this.o0);
                            }
                        }
                    }
                });
            }
        }
        catch (final Exception ex) {
            ex.getMessage();
        }
        return this;
    }
    
    private void \u3007o\u3007(final int n, final String s, final Throwable t) {
        new b.b.a.a.h.y.h(n, s, t).\u3007080(this);
        this.\u3007O00.clear();
    }
    
    public void O8(final g ooO8) {
        this.OoO8 = ooO8;
    }
    
    public int O8ooOoo\u3007() {
        return this.\u300700;
    }
    
    public boolean O8\u3007o() {
        return this.Oooo8o0\u3007;
    }
    
    public boolean OO0o\u3007\u3007\u3007\u30070(final b.b.a.a.h.y.i i) {
        return !this.\u3007O8o08O && this.\u3007O00.add(i);
    }
    
    public u OOO\u3007O0() {
        return this.OO0o\u3007\u3007\u3007\u30070;
    }
    
    public void Oo08(final a oo88o8O) {
        this.oo88o8O = oo88o8O;
    }
    
    public o O\u30078O8\u3007008() {
        return this.O8;
    }
    
    @Override
    public String a() {
        return this.\u3007080;
    }
    
    @Override
    public int b() {
        return this.\u3007\u3007888;
    }
    
    @Override
    public int c() {
        return this.oO80;
    }
    
    @Override
    public ImageView$ScaleType d() {
        return this.Oo08;
    }
    
    @Override
    public String e() {
        return this.\u3007o00\u3007\u3007Oo;
    }
    
    public f o800o8O() {
        return this.\u3007O888o0o;
    }
    
    public void oO80(final boolean \u30070\u3007O0088o) {
        this.\u30070\u3007O0088o = \u30070\u3007O0088o;
    }
    
    public a oo88o8O() {
        return this.oo88o8O;
    }
    
    public boolean oo\u3007() {
        return this.\u30070\u3007O0088o;
    }
    
    public String o\u3007\u30070\u3007() {
        final StringBuilder sb = new StringBuilder();
        sb.append(this.e());
        sb.append(this.OOO\u3007O0());
        return sb.toString();
    }
    
    public g \u300700() {
        return this.OoO8;
    }
    
    public String \u30070000OOO() {
        return this.\u3007o\u3007;
    }
    
    public boolean \u300700\u30078() {
        return this.OO0o\u3007\u3007;
    }
    
    public void \u3007O8o08O(final String \u3007o\u3007) {
        this.\u3007o\u3007 = \u3007o\u3007;
    }
    
    public b.b.a.a.h.b \u3007O\u3007() {
        return this.\u3007oo\u3007;
    }
    
    public void \u3007o00\u3007\u3007Oo(final int o800o8O) {
        this.o800o8O = o800o8O;
    }
    
    public int \u3007oOO8O8() {
        return this.o\u3007O8\u3007\u3007o;
    }
    
    public int \u3007oo\u3007() {
        return this.o800o8O;
    }
    
    public void \u3007\u3007888(final String \u3007o00\u3007\u3007Oo) {
        final WeakReference<ImageView> \u30078o8o\u3007 = this.\u30078o8o\u3007;
        if (\u30078o8o\u3007 != null && \u30078o8o\u3007.get() != null) {
            ((View)this.\u30078o8o\u3007.get()).setTag(1094453505, (Object)\u3007o00\u3007\u3007Oo);
        }
        this.\u3007o00\u3007\u3007Oo = \u3007o00\u3007\u3007Oo;
    }
    
    public Bitmap$Config \u3007\u30078O0\u30078() {
        return this.o\u30070;
    }
    
    private class b implements o
    {
        private o \u3007080;
        final c \u3007o00\u3007\u3007Oo;
        
        public b(final c \u3007o00\u3007\u3007Oo, final o \u3007080) {
            this.\u3007o00\u3007\u3007Oo = \u3007o00\u3007\u3007Oo;
            this.\u3007080 = \u3007080;
        }
        
        private boolean \u3007o00\u3007\u3007Oo(final ImageView imageView) {
            boolean b2;
            final boolean b = b2 = false;
            if (imageView != null) {
                final Object tag = ((View)imageView).getTag(1094453505);
                b2 = b;
                if (tag != null) {
                    b2 = b;
                    if (tag.equals(this.\u3007o00\u3007\u3007Oo.\u3007o00\u3007\u3007Oo)) {
                        b2 = true;
                    }
                }
            }
            return b2;
        }
        
        @Override
        public void a(final int n, final String s, final Throwable t) {
            if (this.\u3007o00\u3007\u3007Oo.\u3007O\u3007 == t.b) {
                this.\u3007o00\u3007\u3007Oo.\u3007\u30078O0\u30078.post((Runnable)new Runnable(this, n, s, t) {
                    final Throwable OO;
                    final int o0;
                    final b \u300708O\u300700\u3007o;
                    final String \u3007OOo8\u30070;
                    
                    @Override
                    public void run() {
                        if (this.\u300708O\u300700\u3007o.\u3007080 != null) {
                            this.\u300708O\u300700\u3007o.\u3007080.a(this.o0, this.\u3007OOo8\u30070, this.OO);
                        }
                    }
                });
            }
            else {
                final o \u3007080 = this.\u3007080;
                if (\u3007080 != null) {
                    \u3007080.a(n, s, t);
                }
            }
        }
        
        @Override
        public void a(final k k) {
            final ImageView imageView = (ImageView)this.\u3007o00\u3007\u3007Oo.\u30078o8o\u3007.get();
            if (imageView != null && this.\u3007o00\u3007\u3007Oo.OO0o\u3007\u3007\u3007\u30070 != u.c && this.\u3007o00\u3007\u3007Oo(imageView) && k.c() instanceof Bitmap) {
                this.\u3007o00\u3007\u3007Oo.\u3007\u30078O0\u30078.post((Runnable)new Runnable(this, imageView, k.c()) {
                    final ImageView o0;
                    final Bitmap \u3007OOo8\u30070;
                    
                    @Override
                    public void run() {
                        this.o0.setImageBitmap(this.\u3007OOo8\u30070);
                    }
                });
            }
            try {
                if (this.\u3007o00\u3007\u3007Oo.\u300780\u3007808\u3007O != null && k.c() instanceof Bitmap) {
                    final Bitmap a = this.\u3007o00\u3007\u3007Oo.\u300780\u3007808\u3007O.a(k.c());
                    if (a != null) {
                        k.a(a);
                    }
                }
            }
            finally {}
            if (this.\u3007o00\u3007\u3007Oo.\u3007O\u3007 == t.b) {
                this.\u3007o00\u3007\u3007Oo.\u3007\u30078O0\u30078.postAtFrontOfQueue((Runnable)new Runnable(this, k) {
                    final k o0;
                    final b \u3007OOo8\u30070;
                    
                    @Override
                    public void run() {
                        if (this.\u3007OOo8\u30070.\u3007080 != null) {
                            this.\u3007OOo8\u30070.\u3007080.a(this.o0);
                        }
                    }
                });
            }
            else {
                final o \u3007080 = this.\u3007080;
                if (\u3007080 != null) {
                    \u3007080.a(k);
                }
            }
        }
    }
    
    public static class c implements j
    {
        private String O8;
        private boolean OO0o\u3007\u3007;
        private t OO0o\u3007\u3007\u3007\u30070;
        private ImageView$ScaleType Oo08;
        private String Oooo8o0\u3007;
        private int oO80;
        private Bitmap$Config o\u30070;
        private o \u3007080;
        private int \u30070\u3007O0088o;
        private u \u300780\u3007808\u3007O;
        private s \u30078o8o\u3007;
        private h \u3007O00;
        private boolean \u3007O8o08O;
        private f \u3007O\u3007;
        private ImageView \u3007o00\u3007\u3007Oo;
        private String \u3007o\u3007;
        private b.b.a.a.h.b \u3007\u3007808\u3007;
        private int \u3007\u3007888;
        private int \u3007\u30078O0\u30078;
        
        public c(final f \u3007o\u3007) {
            this.\u3007O\u3007 = \u3007o\u3007;
        }
        
        @Override
        public i O8(final o \u3007080) {
            this.\u3007080 = \u3007080;
            return new b.b.a.a.h.x.c(this, null).\u3007o();
        }
        
        @Override
        public i Oo08(final o o, final t oo0o\u3007\u3007\u3007\u30070) {
            this.OO0o\u3007\u3007\u3007\u30070 = oo0o\u3007\u3007\u3007\u30070;
            return this.O8(o);
        }
        
        @Override
        public j a(final int oo80) {
            this.oO80 = oo80;
            return this;
        }
        
        @Override
        public j a(final String \u3007o\u3007) {
            this.\u3007o\u3007 = \u3007o\u3007;
            return this;
        }
        
        @Override
        public j a(final boolean oo0o\u3007\u3007) {
            this.OO0o\u3007\u3007 = oo0o\u3007\u3007;
            return this;
        }
        
        @Override
        public j b(final int \u3007\u3007888) {
            this.\u3007\u3007888 = \u3007\u3007888;
            return this;
        }
        
        @Override
        public j b(final String oooo8o0\u3007) {
            this.Oooo8o0\u3007 = oooo8o0\u3007;
            return this;
        }
        
        @Override
        public j c(final int \u3007\u30078O0\u30078) {
            this.\u3007\u30078O0\u30078 = \u3007\u30078O0\u30078;
            return this;
        }
        
        @Override
        public j d(final int \u30070\u3007O0088o) {
            this.\u30070\u3007O0088o = \u30070\u3007O0088o;
            return this;
        }
        
        @Override
        public j oO80(final u \u300780\u3007808\u3007O) {
            this.\u300780\u3007808\u3007O = \u300780\u3007808\u3007O;
            return this;
        }
        
        @Override
        public j o\u30070(final ImageView$ScaleType oo08) {
            this.Oo08 = oo08;
            return this;
        }
        
        @Override
        public i \u3007080(final ImageView \u3007o00\u3007\u3007Oo) {
            this.\u3007o00\u3007\u3007Oo = \u3007o00\u3007\u3007Oo;
            return new b.b.a.a.h.x.c(this, null).\u3007o();
        }
        
        public j \u30078o8o\u3007(final String o8) {
            this.O8 = o8;
            return this;
        }
        
        @Override
        public j \u3007o00\u3007\u3007Oo(final h \u3007o00) {
            this.\u3007O00 = \u3007o00;
            return this;
        }
        
        @Override
        public j \u3007o\u3007(final s \u30078o8o\u3007) {
            this.\u30078o8o\u3007 = \u30078o8o\u3007;
            return this;
        }
        
        @Override
        public j \u3007\u3007888(final Bitmap$Config o\u30070) {
            this.o\u30070 = o\u30070;
            return this;
        }
    }
}

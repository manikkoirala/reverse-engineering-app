// 
// Decompiled by Procyon v0.6.0
// 

package b.b.a.a.h.y;

import android.graphics.Bitmap;
import b.b.a.a.h.x.c;
import b.b.a.a.h.f;

public class e extends a
{
    private byte[] \u3007080;
    private f \u3007o00\u3007\u3007Oo;
    
    public e(final byte[] \u3007080, final f \u3007o00\u3007\u3007Oo) {
        this.\u3007080 = \u3007080;
        this.\u3007o00\u3007\u3007Oo = \u3007o00\u3007\u3007Oo;
    }
    
    private void \u3007o00\u3007\u3007Oo(final int n, final String s, final Throwable t, final c c) {
        if (this.\u3007o00\u3007\u3007Oo == null) {
            c.OO0o\u3007\u3007\u3007\u30070(new k());
        }
        else {
            c.OO0o\u3007\u3007\u3007\u30070(new h(n, s, t));
        }
    }
    
    @Override
    public String a() {
        return "decode";
    }
    
    @Override
    public void \u3007080(final c c) {
        final b.b.a.a.h.x.f o800o8O = c.o800o8O();
        final b.b.a.a.h.x.j.a o8 = o800o8O.O8(c);
        try {
            final Bitmap \u3007o\u3007 = o8.\u3007o\u3007(this.\u3007080);
            if (\u3007o\u3007 != null) {
                c.OO0o\u3007\u3007\u3007\u30070(new m<Object>(\u3007o\u3007, this.\u3007o00\u3007\u3007Oo, false));
                o800o8O.\u30078o8o\u3007(c.\u3007O\u3007()).a(c.e(), \u3007o\u3007);
            }
            else {
                this.\u3007o00\u3007\u3007Oo(1002, "decode failed bitmap null", null, c);
            }
        }
        finally {
            final StringBuilder sb = new StringBuilder();
            sb.append("decode failed:");
            final Throwable t;
            sb.append(t.getMessage());
            this.\u3007o00\u3007\u3007Oo(1002, sb.toString(), t, c);
        }
    }
}

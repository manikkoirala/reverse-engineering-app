// 
// Decompiled by Procyon v0.6.0
// 

package b.b.a.a.h.v;

import java.util.concurrent.ThreadFactory;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.TimeUnit;

public class c
{
    private static final TimeUnit \u3007080;
    
    static {
        \u3007080 = TimeUnit.SECONDS;
    }
    
    public static ExecutorService \u3007080() {
        final ThreadPoolExecutor threadPoolExecutor = new ThreadPoolExecutor(1, 2, 30L, c.\u3007080, new LinkedBlockingQueue<Runnable>(), new a("default"));
        threadPoolExecutor.allowCoreThreadTimeOut(true);
        return threadPoolExecutor;
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package b.b.a.a.h.v;

import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.ThreadFactory;

public class a implements ThreadFactory
{
    private final ThreadGroup \u3007080;
    private final AtomicInteger \u3007o00\u3007\u3007Oo;
    
    public a(final String str) {
        this.\u3007o00\u3007\u3007Oo = new AtomicInteger(1);
        final StringBuilder sb = new StringBuilder();
        sb.append("tt_img_");
        sb.append(str);
        this.\u3007080 = new ThreadGroup(sb.toString());
    }
    
    @Override
    public Thread newThread(final Runnable target) {
        final ThreadGroup \u3007080 = this.\u3007080;
        final StringBuilder sb = new StringBuilder();
        sb.append("tt_img_");
        sb.append(this.\u3007o00\u3007\u3007Oo.getAndIncrement());
        final Thread thread = new Thread(\u3007080, target, sb.toString());
        if (thread.isDaemon()) {
            thread.setDaemon(false);
        }
        return thread;
    }
}

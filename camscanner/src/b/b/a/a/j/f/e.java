// 
// Decompiled by Procyon v0.6.0
// 

package b.b.a.a.j.f;

import android.net.NetworkInfo;
import android.net.NetworkInfo$State;
import android.net.ConnectivityManager;
import android.content.Context;

public class e
{
    public static boolean \u3007080(final Context context) {
        try {
            final ConnectivityManager connectivityManager = (ConnectivityManager)context.getSystemService("connectivity");
            if (connectivityManager == null) {
                return false;
            }
            final NetworkInfo[] allNetworkInfo = connectivityManager.getAllNetworkInfo();
            if (allNetworkInfo != null) {
                for (final NetworkInfo networkInfo : allNetworkInfo) {
                    if (networkInfo.getState() == NetworkInfo$State.CONNECTED || networkInfo.getState() == NetworkInfo$State.CONNECTING) {
                        return true;
                    }
                }
                return false;
            }
        }
        finally {
            final Throwable t;
            t.toString();
        }
        return false;
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package b.b.a.a.j;

import java.util.ArrayList;
import b.b.a.a.j.d.d;
import b.b.a.a.j.e.g;
import b.b.a.a.j.f.b;
import com.bytedance.sdk.component.utils.r;
import android.content.Context;
import java.util.Iterator;
import java.util.List;
import b.b.a.a.f.a.h;
import java.util.concurrent.TimeUnit;
import b.b.a.a.j.e.f;
import b.b.a.a.f.a.j;

public class a
{
    private j \u3007080;
    private f \u3007o00\u3007\u3007Oo;
    private int \u3007o\u3007;
    
    private a(final b b) {
        final j.a a = new j.a();
        final long n = b.\u3007080;
        final TimeUnit milliseconds = TimeUnit.MILLISECONDS;
        final j.a o8 = a.\u3007080(n, milliseconds).Oo08(b.\u3007o\u3007, milliseconds).O8(b.\u3007o00\u3007\u3007Oo, milliseconds);
        if (b.O8) {
            o8.\u3007o00\u3007\u3007Oo(this.\u3007o00\u3007\u3007Oo = new f());
        }
        final List<h> oo08 = b.Oo08;
        if (oo08 != null && oo08.size() > 0) {
            final Iterator<h> iterator = b.Oo08.iterator();
            while (iterator.hasNext()) {
                o8.\u3007o00\u3007\u3007Oo(iterator.next());
            }
        }
        this.\u3007080 = o8.\u3007o\u3007();
    }
    
    private static boolean O8(final Context context) {
        final String a = r.a(context);
        return a != null && (a.endsWith(":push") || a.endsWith(":pushservice"));
    }
    
    public static void oO80() {
        b.b.a.a.j.f.b.\u3007080(b.b.a.a.j.f.b.b.a);
    }
    
    public b.b.a.a.j.d.b Oo08() {
        return new b.b.a.a.j.d.b(this.\u3007080);
    }
    
    public j o\u30070() {
        return this.\u3007080;
    }
    
    public b.b.a.a.j.d.a \u3007080() {
        return new b.b.a.a.j.d.a(this.\u3007080);
    }
    
    public void \u3007o00\u3007\u3007Oo(final Context context, final boolean b) {
        b.b.a.a.j.e.a.\u300780\u3007808\u3007O(true);
        if (O8(context) || (!r.c(context) && b)) {
            g.\u3007o\u3007().\u3007080(this.\u3007o\u3007, context).\u3007oo\u3007();
            g.\u3007o\u3007().\u3007080(this.\u3007o\u3007, context).\u300700();
        }
        if (!r.c(context)) {
            return;
        }
        g.\u3007o\u3007().\u3007080(this.\u3007o\u3007, context).\u3007oo\u3007();
        g.\u3007o\u3007().\u3007080(this.\u3007o\u3007, context).\u300700();
    }
    
    public void \u3007o\u3007(final Context context, final boolean b, final b.b.a.a.j.e.b b2) {
        if (context == null) {
            throw new IllegalArgumentException("tryInitAdTTNet context is null");
        }
        if (b2 != null) {
            final int e = b2.e();
            this.\u3007o\u3007 = e;
            final f \u3007o00\u3007\u3007Oo = this.\u3007o00\u3007\u3007Oo;
            if (\u3007o00\u3007\u3007Oo != null) {
                \u3007o00\u3007\u3007Oo.\u3007o00\u3007\u3007Oo(e);
            }
            g.\u3007o\u3007().\u3007o00\u3007\u3007Oo(this.\u3007o\u3007).\u3007\u3007808\u3007(b);
            g.\u3007o\u3007().\u3007o00\u3007\u3007Oo(this.\u3007o\u3007).\u3007\u3007888(b2);
            g.\u3007o\u3007().\u3007o00\u3007\u3007Oo(this.\u3007o\u3007).\u3007o\u3007(context, r.c(context));
            return;
        }
        throw new IllegalArgumentException("tryInitAdTTNet ITTAdNetDepend is null");
    }
    
    public d \u3007\u3007888() {
        return new d(this.\u3007080);
    }
    
    public static final class b
    {
        boolean O8;
        final List<h> Oo08;
        int \u3007080;
        int \u3007o00\u3007\u3007Oo;
        int \u3007o\u3007;
        
        public b() {
            this.O8 = true;
            this.Oo08 = new ArrayList<h>();
            this.\u3007080 = 10000;
            this.\u3007o00\u3007\u3007Oo = 10000;
            this.\u3007o\u3007 = 10000;
        }
        
        private static int \u3007080(final String str, long millis, final TimeUnit timeUnit) {
            final long n = lcmp(millis, 0L);
            if (n < 0) {
                final StringBuilder sb = new StringBuilder();
                sb.append(str);
                sb.append(" < 0");
                throw new IllegalArgumentException(sb.toString());
            }
            if (timeUnit == null) {
                throw new NullPointerException("unit == null");
            }
            millis = timeUnit.toMillis(millis);
            if (millis > 2147483647L) {
                final StringBuilder sb2 = new StringBuilder();
                sb2.append(str);
                sb2.append(" too large.");
                throw new IllegalArgumentException(sb2.toString());
            }
            if (millis == 0L && n > 0) {
                final StringBuilder sb3 = new StringBuilder();
                sb3.append(str);
                sb3.append(" too small.");
                throw new IllegalArgumentException(sb3.toString());
            }
            return (int)millis;
        }
        
        public a O8() {
            return new a(this, null);
        }
        
        public b Oo08(final long n, final TimeUnit timeUnit) {
            this.\u3007o00\u3007\u3007Oo = \u3007080("timeout", n, timeUnit);
            return this;
        }
        
        public b o\u30070(final long n, final TimeUnit timeUnit) {
            this.\u3007o\u3007 = \u3007080("timeout", n, timeUnit);
            return this;
        }
        
        public b \u3007o00\u3007\u3007Oo(final long n, final TimeUnit timeUnit) {
            this.\u3007080 = \u3007080("timeout", n, timeUnit);
            return this;
        }
        
        public b \u3007o\u3007(final boolean o8) {
            this.O8 = o8;
            return this;
        }
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package b.b.a.a.j.e;

import java.net.URL;
import android.content.SharedPreferences;
import java.net.InetAddress;
import b.b.a.a.f.a.l;
import java.util.Random;
import b.b.a.a.f.a.n;
import android.text.TextUtils;
import android.os.SystemClock;
import android.os.Message;
import android.os.Looper;
import java.util.Map;
import android.os.Handler;
import android.content.Context;
import java.util.HashMap;

public class e
{
    private b O8;
    private int OO0o\u3007\u3007;
    private int OO0o\u3007\u3007\u3007\u30070;
    private boolean Oo08;
    private HashMap<String, Integer> Oooo8o0\u3007;
    private int oO80;
    private Context o\u30070;
    private long \u3007080;
    Handler \u30070\u3007O0088o;
    private long \u300780\u3007808\u3007O;
    private HashMap<String, Integer> \u30078o8o\u3007;
    private Map<String, Integer> \u3007O00;
    private HashMap<String, Integer> \u3007O8o08O;
    private boolean \u3007O\u3007;
    private a \u3007o00\u3007\u3007Oo;
    private boolean \u3007o\u3007;
    private HashMap<String, Integer> \u3007\u3007808\u3007;
    private d \u3007\u3007888;
    private int \u3007\u30078O0\u30078;
    
    private e() {
        this.\u3007080 = 0L;
        this.Oo08 = false;
        this.oO80 = 0;
        this.\u300780\u3007808\u3007O = 19700101000L;
        this.OO0o\u3007\u3007\u3007\u30070 = 0;
        this.\u30078o8o\u3007 = new HashMap<String, Integer>();
        this.\u3007O8o08O = new HashMap<String, Integer>();
        this.OO0o\u3007\u3007 = 0;
        this.Oooo8o0\u3007 = new HashMap<String, Integer>();
        this.\u3007\u3007808\u3007 = new HashMap<String, Integer>();
        this.\u3007O\u3007 = true;
        this.\u3007O00 = new HashMap<String, Integer>();
        this.\u30070\u3007O0088o = new Handler(Looper.getMainLooper()) {
            final e \u3007080;
            
            public void handleMessage(final Message message) {
                if (message.what == 10000) {
                    this.\u3007080.OO0o\u3007\u3007\u3007\u30070(message.arg1 != 0);
                }
            }
        };
    }
    
    public e(final int \u3007\u30078O0\u30078) {
        this.\u3007080 = 0L;
        this.Oo08 = false;
        this.oO80 = 0;
        this.\u300780\u3007808\u3007O = 19700101000L;
        this.OO0o\u3007\u3007\u3007\u30070 = 0;
        this.\u30078o8o\u3007 = new HashMap<String, Integer>();
        this.\u3007O8o08O = new HashMap<String, Integer>();
        this.OO0o\u3007\u3007 = 0;
        this.Oooo8o0\u3007 = new HashMap<String, Integer>();
        this.\u3007\u3007808\u3007 = new HashMap<String, Integer>();
        this.\u3007O\u3007 = true;
        this.\u3007O00 = new HashMap<String, Integer>();
        this.\u30070\u3007O0088o = new Handler(Looper.getMainLooper()) {
            final e \u3007080;
            
            public void handleMessage(final Message message) {
                if (message.what == 10000) {
                    this.\u3007080.OO0o\u3007\u3007\u3007\u30070(message.arg1 != 0);
                }
            }
        };
        this.\u3007\u30078O0\u30078 = \u3007\u30078O0\u30078;
    }
    
    private void OO0o\u3007\u3007\u3007\u30070(final boolean b) {
        final c \u3007o00 = this.\u3007O00();
        if (\u3007o00 == null) {
            return;
        }
        final StringBuilder sb = new StringBuilder();
        sb.append("doUpdateRemote, ");
        sb.append(b);
        b.b.a.a.j.f.b.\u3007o00\u3007\u3007Oo("TNCManager", sb.toString());
        final long elapsedRealtime = SystemClock.elapsedRealtime();
        if (!b && this.\u3007080 + \u3007o00.\u30078o8o\u3007 * 1000L > elapsedRealtime) {
            b.b.a.a.j.f.b.\u3007o00\u3007\u3007Oo("TNCManager", "doUpdateRemote, time limit");
            return;
        }
        this.\u3007080 = elapsedRealtime;
        g.\u3007o\u3007().\u3007080(this.\u3007\u30078O0\u30078, this.o\u30070).OO0o\u3007\u3007\u3007\u30070();
    }
    
    private void OoO8(final String s) {
        if (TextUtils.isEmpty((CharSequence)s)) {
            return;
        }
        if (!this.\u3007O00.containsKey(s)) {
            return;
        }
        this.\u3007O00.put(s, 0);
    }
    
    private void o\u30070(final n n, final String str) {
        if (n == null) {
            return;
        }
        if (!this.\u3007O\u3007) {
            return;
        }
        final String \u3007\u3007888 = n.\u3007\u3007888("tnc-cmd", null);
        if (TextUtils.isEmpty((CharSequence)\u3007\u3007888)) {
            final StringBuilder sb = new StringBuilder();
            sb.append("handleTncProbe, no probeProto, ");
            sb.append(str);
            b.b.a.a.j.f.b.\u3007o00\u3007\u3007Oo("TNCManager", sb.toString());
            return;
        }
        final String[] split = \u3007\u3007888.split("@");
        if (split == null || split.length != 2) {
            final StringBuilder sb2 = new StringBuilder();
            sb2.append("handleTncProbe, probeProto err, ");
            sb2.append(str);
            b.b.a.a.j.f.b.\u3007o00\u3007\u3007Oo("TNCManager", sb2.toString());
            return;
        }
        final long n2 = 0L;
        int n3 = 0;
        try {
            Integer.parseInt(split[0]);
            try {
                final long long1 = Long.parseLong(split[1]);
            }
            finally {}
        }
        finally {
            n3 = 0;
        }
        final Throwable t;
        t.printStackTrace();
        final StringBuilder sb3 = new StringBuilder();
        sb3.append("handleTncProbe, probeProto except, ");
        sb3.append(str);
        b.b.a.a.j.f.b.\u3007o00\u3007\u3007Oo("TNCManager", sb3.toString());
        final long long1 = 0L;
        final StringBuilder sb4 = new StringBuilder();
        sb4.append("handleTncProbe, local: ");
        sb4.append(this.oO80);
        sb4.append("@");
        sb4.append(this.\u300780\u3007808\u3007O);
        sb4.append(" svr: ");
        sb4.append(n3);
        sb4.append("@");
        sb4.append(long1);
        sb4.append(" ");
        sb4.append(str);
        b.b.a.a.j.f.b.\u3007o00\u3007\u3007Oo("TNCManager", sb4.toString());
        if (long1 <= this.\u300780\u3007808\u3007O) {
            return;
        }
        this.oO80 = n3;
        this.\u300780\u3007808\u3007O = long1;
        this.o\u30070.getSharedPreferences(this.\u3007O888o0o(), 0).edit().putInt("tnc_probe_cmd", n3).putLong("tnc_probe_version", long1).apply();
        if (this.oO80 == 10000) {
            final c \u3007o00 = this.\u3007O00();
            if (\u3007o00 == null) {
                return;
            }
            final Random random = new Random(System.currentTimeMillis());
            final int \u3007o8o08O = \u3007o00.\u3007O8o08O;
            long lng = n2;
            if (\u3007o8o08O > 0) {
                lng = random.nextInt(\u3007o8o08O) * 1000L;
            }
            final StringBuilder sb5 = new StringBuilder();
            sb5.append("handleTncProbe, updateConfig delay: ");
            sb5.append(lng);
            sb5.append(" ");
            sb5.append(str);
            b.b.a.a.j.f.b.\u3007o00\u3007\u3007Oo("TNCManager", sb5.toString());
            this.\u30078o8o\u3007(true, lng);
        }
    }
    
    private void \u300700() {
        b.b.a.a.j.f.b.\u3007o00\u3007\u3007Oo("TNCManager", "resetTNCControlState");
        this.OO0o\u3007\u3007\u3007\u30070 = 0;
        this.\u30078o8o\u3007.clear();
        this.\u3007O8o08O.clear();
        this.OO0o\u3007\u3007 = 0;
        this.Oooo8o0\u3007.clear();
        this.\u3007\u3007808\u3007.clear();
    }
    
    private void \u300780\u3007808\u3007O(final String s) {
        if (TextUtils.isEmpty((CharSequence)s)) {
            return;
        }
        final Map<String, String> o800o8O = this.o800o8O();
        if (o800o8O != null) {
            if (o800o8O.containsValue(s)) {
                if (this.\u3007O00.get(s) == null) {
                    this.\u3007O00.put(s, 1);
                }
                else {
                    this.\u3007O00.put(s, this.\u3007O00.get(s) + 1);
                }
            }
        }
    }
    
    private void \u30078o8o\u3007(final boolean arg1, final long n) {
        if (this.\u30070\u3007O0088o.hasMessages(10000)) {
            return;
        }
        final Message obtainMessage = this.\u30070\u3007O0088o.obtainMessage();
        obtainMessage.what = 10000;
        obtainMessage.arg1 = (arg1 ? 1 : 0);
        if (n > 0L) {
            this.\u30070\u3007O0088o.sendMessageDelayed(obtainMessage, n);
        }
        else {
            this.\u30070\u3007O0088o.sendMessage(obtainMessage);
        }
    }
    
    private boolean \u3007O8o08O(final int i) {
        if (i >= 100 && i < 1000) {
            final c \u3007o00 = this.\u3007O00();
            if (\u3007o00 != null && !TextUtils.isEmpty((CharSequence)\u3007o00.OO0o\u3007\u3007)) {
                final String oo0o\u3007\u3007 = \u3007o00.OO0o\u3007\u3007;
                final StringBuilder sb = new StringBuilder();
                sb.append("");
                sb.append(i);
                if (oo0o\u3007\u3007.contains(sb.toString())) {
                    return true;
                }
            }
            return false;
        }
        return true;
    }
    
    private boolean \u3007O\u3007(final int n) {
        return n >= 200 && n < 400;
    }
    
    private String \u3007o00\u3007\u3007Oo(final l l) {
        String hostAddress;
        final String s = hostAddress = "";
        if (l == null) {
            return hostAddress;
        }
        hostAddress = s;
        if (l.oO80() == null) {
            return hostAddress;
        }
        if (l.oO80().\u3007\u30078O0\u30078() == null) {
            hostAddress = s;
            return hostAddress;
        }
        try {
            hostAddress = InetAddress.getByName(l.oO80().\u3007\u30078O0\u30078().getHost()).getHostAddress();
            return hostAddress;
        }
        catch (final Exception ex) {
            hostAddress = s;
            return hostAddress;
        }
    }
    
    private void \u3007oo\u3007() {
        final SharedPreferences sharedPreferences = this.o\u30070.getSharedPreferences(this.\u3007O888o0o(), 0);
        this.oO80 = sharedPreferences.getInt("tnc_probe_cmd", 0);
        this.\u300780\u3007808\u3007O = sharedPreferences.getLong("tnc_probe_version", 19700101000L);
    }
    
    private boolean \u3007\u30078O0\u30078(final String str) {
        final Map<String, String> o800o8O = this.o800o8O();
        if (o800o8O == null) {
            return false;
        }
        final String s = o800o8O.get(str);
        if (!TextUtils.isEmpty((CharSequence)s)) {
            if (this.\u3007O00.get(s) != null) {
                if (this.\u3007O00.get(s) >= 3) {
                    final StringBuilder sb = new StringBuilder();
                    sb.append("handleHostMapping, TNC host faild num over limit: ");
                    sb.append(str);
                    b.b.a.a.j.f.b.\u3007o00\u3007\u3007Oo("TNCManager", sb.toString());
                    return true;
                }
            }
        }
        return false;
    }
    
    public void O8(final l l, final n n) {
        monitorenter(this);
        if (l != null) {
            if (n != null) {
                try {
                    if (!this.\u3007O\u3007) {
                        return;
                    }
                    if (!b.b.a.a.j.f.e.\u3007080(this.o\u30070)) {
                        return;
                    }
                    URL \u3007\u30078O0\u30078;
                    try {
                        \u3007\u30078O0\u30078 = l.oO80().\u3007\u30078O0\u30078();
                    }
                    catch (final Exception ex) {
                        \u3007\u30078O0\u30078 = null;
                    }
                    if (\u3007\u30078O0\u30078 == null) {
                        return;
                    }
                    final String protocol = \u3007\u30078O0\u30078.getProtocol();
                    final String host = \u3007\u30078O0\u30078.getHost();
                    final String path = \u3007\u30078O0\u30078.getPath();
                    final String \u3007o00\u3007\u3007Oo = this.\u3007o00\u3007\u3007Oo(l);
                    final int oo80 = n.oO80();
                    if (!"http".equals(protocol) && !"https".equals(protocol)) {
                        return;
                    }
                    if (TextUtils.isEmpty((CharSequence)\u3007o00\u3007\u3007Oo)) {
                        return;
                    }
                    final StringBuilder sb = new StringBuilder();
                    sb.append("onResponse, url: ");
                    sb.append(protocol);
                    sb.append("://");
                    sb.append(host);
                    sb.append("#");
                    sb.append(\u3007o00\u3007\u3007Oo);
                    sb.append("#");
                    sb.append(oo80);
                    b.b.a.a.j.f.b.\u3007o00\u3007\u3007Oo("TNCManager", sb.toString());
                    final c \u3007o00 = this.\u3007O00();
                    if (\u3007o00 != null && \u3007o00.\u3007o00\u3007\u3007Oo) {
                        this.o\u30070(n, host);
                    }
                    if (\u3007o00 == null) {
                        return;
                    }
                    final StringBuilder sb2 = new StringBuilder();
                    sb2.append("onResponse, url matched: ");
                    sb2.append(protocol);
                    sb2.append("://");
                    sb2.append(host);
                    sb2.append("#");
                    sb2.append(\u3007o00\u3007\u3007Oo);
                    sb2.append("#");
                    sb2.append(oo80);
                    sb2.append(" ");
                    sb2.append(this.OO0o\u3007\u3007\u3007\u30070);
                    sb2.append("#");
                    sb2.append(this.\u30078o8o\u3007.size());
                    sb2.append("#");
                    sb2.append(this.\u3007O8o08O.size());
                    sb2.append(" ");
                    sb2.append(this.OO0o\u3007\u3007);
                    sb2.append("#");
                    sb2.append(this.Oooo8o0\u3007.size());
                    sb2.append("#");
                    sb2.append(this.\u3007\u3007808\u3007.size());
                    b.b.a.a.j.f.b.\u3007o00\u3007\u3007Oo("TNCManager", sb2.toString());
                    if (oo80 > 0) {
                        if (this.\u3007O\u3007(oo80)) {
                            if (this.OO0o\u3007\u3007\u3007\u30070 > 0 || this.OO0o\u3007\u3007 > 0) {
                                this.\u300700();
                            }
                            this.OoO8(host);
                        }
                        else if (!this.\u3007O8o08O(oo80)) {
                            ++this.OO0o\u3007\u3007;
                            this.Oooo8o0\u3007.put(path, 0);
                            this.\u3007\u3007808\u3007.put(\u3007o00\u3007\u3007Oo, 0);
                            if (this.OO0o\u3007\u3007 >= \u3007o00.oO80 && this.Oooo8o0\u3007.size() >= \u3007o00.\u300780\u3007808\u3007O && this.\u3007\u3007808\u3007.size() >= \u3007o00.OO0o\u3007\u3007\u3007\u30070) {
                                final StringBuilder sb3 = new StringBuilder();
                                sb3.append("onResponse, url doUpdate: ");
                                sb3.append(protocol);
                                sb3.append("://");
                                sb3.append(host);
                                sb3.append("#");
                                sb3.append(\u3007o00\u3007\u3007Oo);
                                sb3.append("#");
                                sb3.append(oo80);
                                b.b.a.a.j.f.b.\u3007o00\u3007\u3007Oo("TNCManager", sb3.toString());
                                this.\u30078o8o\u3007(false, 0L);
                                this.\u300700();
                            }
                            this.\u300780\u3007808\u3007O(host);
                        }
                    }
                    return;
                }
                finally {
                    monitorexit(this);
                }
            }
        }
        monitorexit(this);
    }
    
    public b OO0o\u3007\u3007() {
        return this.O8;
    }
    
    public void Oo08(final l l, final Exception ex) {
        monitorenter(this);
        if (l != null) {
            try {
                if (l.oO80() != null) {
                    if (ex != null) {
                        if (!this.\u3007O\u3007) {
                            return;
                        }
                        if (!b.b.a.a.j.f.e.\u3007080(this.o\u30070)) {
                            return;
                        }
                        URL \u3007\u30078O0\u30078;
                        try {
                            \u3007\u30078O0\u30078 = l.oO80().\u3007\u30078O0\u30078();
                        }
                        catch (final Exception ex) {
                            \u3007\u30078O0\u30078 = null;
                        }
                        if (\u3007\u30078O0\u30078 == null) {
                            return;
                        }
                        final String protocol = \u3007\u30078O0\u30078.getProtocol();
                        final String host = \u3007\u30078O0\u30078.getHost();
                        final String path = \u3007\u30078O0\u30078.getPath();
                        final String \u3007o00\u3007\u3007Oo = this.\u3007o00\u3007\u3007Oo(l);
                        if (!"http".equals(protocol) && !"https".equals(protocol)) {
                            return;
                        }
                        final c \u3007o00 = this.\u3007O00();
                        if (\u3007o00 == null) {
                            return;
                        }
                        final StringBuilder sb = new StringBuilder();
                        sb.append("onError, url matched: ");
                        sb.append(protocol);
                        sb.append("://");
                        sb.append(host);
                        sb.append("#");
                        sb.append(\u3007o00\u3007\u3007Oo);
                        sb.append("# ");
                        sb.append(this.OO0o\u3007\u3007\u3007\u30070);
                        sb.append("#");
                        sb.append(this.\u30078o8o\u3007.size());
                        sb.append("#");
                        sb.append(this.\u3007O8o08O.size());
                        sb.append(" ");
                        sb.append(this.OO0o\u3007\u3007);
                        sb.append("#");
                        sb.append(this.Oooo8o0\u3007.size());
                        sb.append("#");
                        sb.append(this.\u3007\u3007808\u3007.size());
                        b.b.a.a.j.f.b.\u3007o00\u3007\u3007Oo("TNCManager", sb.toString());
                        ++this.OO0o\u3007\u3007\u3007\u30070;
                        this.\u30078o8o\u3007.put(path, 0);
                        this.\u3007O8o08O.put(\u3007o00\u3007\u3007Oo, 0);
                        if (this.OO0o\u3007\u3007\u3007\u30070 >= \u3007o00.Oo08 && this.\u30078o8o\u3007.size() >= \u3007o00.o\u30070 && this.\u3007O8o08O.size() >= \u3007o00.\u3007\u3007888) {
                            final StringBuilder sb2 = new StringBuilder();
                            sb2.append("onError, url doUpate: ");
                            sb2.append(protocol);
                            sb2.append("://");
                            sb2.append(host);
                            sb2.append("#");
                            sb2.append(\u3007o00\u3007\u3007Oo);
                            b.b.a.a.j.f.b.\u3007o00\u3007\u3007Oo("TNCManager", sb2.toString());
                            this.\u30078o8o\u3007(false, 0L);
                            this.\u300700();
                        }
                        this.\u300780\u3007808\u3007O(host);
                        return;
                    }
                }
            }
            finally {
                monitorexit(this);
            }
        }
        monitorexit(this);
    }
    
    public String Oooo8o0\u3007(final String s) {
        if (!TextUtils.isEmpty((CharSequence)s) && !s.contains("/network/get_network") && !s.contains("/get_domains/v4")) {
            if (!s.contains("/ies/speed")) {
                final String s2 = null;
                String str = null;
                String s3 = null;
                try {
                    final URL url = new URL(s);
                    url.getProtocol();
                    try {
                        url.getHost();
                    }
                    finally {}
                }
                finally {
                    str = null;
                    final String s4;
                    s3 = s4;
                }
                ((Throwable)s3).printStackTrace();
                final String s4 = str;
                str = s2;
                if (!TextUtils.isEmpty((CharSequence)s4) && ("http".equals(s4) || "https".equals(s4))) {
                    if (!TextUtils.isEmpty((CharSequence)str)) {
                        if (this.\u3007\u30078O0\u30078(str)) {
                            final StringBuilder sb = new StringBuilder();
                            sb.append("handleHostMapping, TNC host faild num over limit: ");
                            sb.append(str);
                            b.b.a.a.j.f.b.\u3007o00\u3007\u3007Oo("TNCManager", sb.toString());
                            return s;
                        }
                        final Map<String, String> o800o8O = this.o800o8O();
                        if (o800o8O != null && o800o8O.containsKey(str)) {
                            final String str2 = o800o8O.get(str);
                            if (TextUtils.isEmpty((CharSequence)str2)) {
                                return s;
                            }
                            final StringBuilder sb2 = new StringBuilder();
                            sb2.append("handleHostMapping, match, origin: ");
                            sb2.append(s);
                            b.b.a.a.j.f.b.\u3007o00\u3007\u3007Oo("TNCManager", sb2.toString());
                            final StringBuilder sb3 = new StringBuilder();
                            sb3.append(s4);
                            sb3.append("://");
                            sb3.append(str);
                            final String string = sb3.toString();
                            final StringBuilder sb4 = new StringBuilder();
                            sb4.append(s4);
                            sb4.append("://");
                            sb4.append(str2);
                            final String string2 = sb4.toString();
                            String replaceFirst = s;
                            if (s.startsWith(string)) {
                                replaceFirst = s.replaceFirst(string, string2);
                            }
                            final StringBuilder sb5 = new StringBuilder();
                            sb5.append("handleHostMapping, target: ");
                            sb5.append(replaceFirst);
                            b.b.a.a.j.f.b.\u3007o00\u3007\u3007Oo("TNCManager", sb5.toString());
                            return replaceFirst;
                        }
                        else {
                            final StringBuilder sb6 = new StringBuilder();
                            sb6.append("handleHostMapping, nomatch: ");
                            sb6.append(str);
                            b.b.a.a.j.f.b.\u3007o00\u3007\u3007Oo("TNCManager", sb6.toString());
                        }
                    }
                }
            }
        }
        return s;
    }
    
    public Map<String, String> o800o8O() {
        final c \u3007o00 = this.\u3007O00();
        if (\u3007o00 != null) {
            return \u3007o00.O8;
        }
        return null;
    }
    
    public boolean oo88o8O() {
        return this.\u3007o\u3007;
    }
    
    public void o\u3007O8\u3007\u3007o() {
        this.\u3007O00.clear();
    }
    
    public a \u3007080() {
        return this.\u3007o00\u3007\u3007Oo;
    }
    
    public d \u30070\u3007O0088o() {
        return this.\u3007\u3007888;
    }
    
    public c \u3007O00() {
        final d \u3007\u3007888 = this.\u3007\u3007888;
        if (\u3007\u3007888 != null) {
            return \u3007\u3007888.\u3007080();
        }
        return null;
    }
    
    public String \u3007O888o0o() {
        final StringBuilder sb = new StringBuilder();
        sb.append("ttnet_tnc_config");
        sb.append(this.\u3007\u30078O0\u30078);
        return sb.toString();
    }
    
    public void \u3007o\u3007(final Context o\u30070, final boolean b) {
        synchronized (this) {
            if (!this.Oo08) {
                this.o\u30070 = o\u30070;
                this.\u3007O\u3007 = b;
                this.\u3007\u3007888 = new d(o\u30070, b, this.\u3007\u30078O0\u30078);
                if (b) {
                    this.\u3007oo\u3007();
                }
                final StringBuilder sb = new StringBuilder();
                sb.append("initTnc, isMainProc: ");
                sb.append(b);
                sb.append(" probeCmd: ");
                sb.append(this.oO80);
                sb.append(" probeVersion: ");
                sb.append(this.\u300780\u3007808\u3007O);
                b.b.a.a.j.f.b.\u3007o00\u3007\u3007Oo("TNCManager", sb.toString());
                this.\u3007o00\u3007\u3007Oo = g.\u3007o\u3007().\u3007080(this.\u3007\u30078O0\u30078, this.o\u30070);
                this.Oo08 = true;
            }
        }
    }
    
    public void \u3007\u3007808\u3007(final boolean \u3007o\u3007) {
        this.\u3007o\u3007 = \u3007o\u3007;
    }
    
    public void \u3007\u3007888(final b o8) {
        this.O8 = o8;
    }
}

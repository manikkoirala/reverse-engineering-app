// 
// Decompiled by Procyon v0.6.0
// 

package b.b.a.a.j.e;

import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import b.b.a.a.j.f.e;
import android.content.SharedPreferences$Editor;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;
import android.location.Address;
import android.os.Build;
import android.net.Uri;
import java.util.concurrent.TimeUnit;
import java.io.IOException;
import org.json.JSONObject;
import b.b.a.a.j.b;
import b.b.a.a.j.d.c;
import android.text.TextUtils;
import com.bytedance.sdk.component.utils.r;
import android.content.Context;
import java.util.concurrent.atomic.AtomicBoolean;
import com.bytedance.sdk.component.utils.y;
import java.util.concurrent.ThreadPoolExecutor;
import com.bytedance.sdk.component.utils.y$a;

public class a implements y$a
{
    private static ThreadPoolExecutor ooo0\u3007\u3007O;
    private static boolean \u30078\u3007oO\u3007\u30078o;
    private long O8o08O8O;
    private boolean OO;
    private int OO\u300700\u30078oO;
    private final boolean o0;
    final y o8\u3007OO0\u30070o;
    private b.b.a.a.j.a oOo0;
    private volatile boolean oOo\u30078o008;
    private long o\u300700O;
    private AtomicBoolean \u3007080OO8\u30070;
    private boolean \u300708O\u300700\u3007o;
    private final Context \u30070O;
    private volatile boolean \u3007OOo8\u30070;
    
    public a(final Context \u30070O, final int oo\u300700\u30078oO) {
        this.\u3007OOo8\u30070 = false;
        this.OO = true;
        this.\u300708O\u300700\u3007o = false;
        this.o\u300700O = 0L;
        this.O8o08O8O = 0L;
        this.\u3007080OO8\u30070 = new AtomicBoolean(false);
        this.oOo\u30078o008 = false;
        this.o8\u3007OO0\u30070o = b.b.a.a.k.i.a.\u3007080().o\u30070((y$a)this, "tt-net");
        this.\u30070O = \u30070O;
        this.o0 = r.c(\u30070O);
        this.OO\u300700\u30078oO = oo\u300700\u30078oO;
    }
    
    private void O8(final int n) {
        final String[] \u3007o00 = this.\u3007O00();
        if (\u3007o00 == null || \u3007o00.length <= n) {
            this.Oooo8o0\u3007(102);
            return;
        }
        final String s = \u3007o00[n];
        if (TextUtils.isEmpty((CharSequence)s)) {
            this.Oooo8o0\u3007(102);
            return;
        }
        try {
            final String \u3007o\u3007 = this.\u3007o\u3007(s);
            if (TextUtils.isEmpty((CharSequence)\u3007o\u3007)) {
                this.Oooo8o0\u3007(102);
                return;
            }
            final b.b.a.a.j.d.b oo08 = this.OoO8().Oo08();
            oo08.o\u30070(\u3007o\u3007);
            this.o\u30070(oo08);
            oo08.\u3007\u3007888(new b.b.a.a.j.c.a(this, n) {
                final int \u3007080;
                final a \u3007o00\u3007\u3007Oo;
                
                @Override
                public void a(c c, final b b) {
                    Label_0125: {
                        if (b == null || !b.oO80()) {
                            break Label_0125;
                        }
                        final String s = null;
                        try {
                            c = (c)new JSONObject(b.\u3007080());
                        }
                        catch (final Exception ex) {
                            c = null;
                        }
                        if (c == null) {
                            this.\u3007o00\u3007\u3007Oo.O8(this.\u3007080 + 1);
                            return;
                        }
                        String string;
                        try {
                            string = ((JSONObject)c).getString("message");
                        }
                        catch (final Exception ex2) {
                            string = s;
                        }
                        if (!"success".equals(string)) {
                            this.\u3007o00\u3007\u3007Oo.O8(this.\u3007080 + 1);
                            return;
                        }
                        try {
                            if (this.\u3007o00\u3007\u3007Oo.\u3007O8o08O(c)) {
                                this.\u3007o00\u3007\u3007Oo.Oooo8o0\u3007(101);
                            }
                            else {
                                this.\u3007o00\u3007\u3007Oo.O8(this.\u3007080 + 1);
                            }
                            return;
                            this.\u3007o00\u3007\u3007Oo.O8(this.\u3007080 + 1);
                        }
                        catch (final Exception ex3) {}
                    }
                }
                
                @Override
                public void a(final c c, final IOException ex) {
                    this.\u3007o00\u3007\u3007Oo.O8(this.\u3007080 + 1);
                }
            });
        }
        finally {
            final StringBuilder sb = new StringBuilder();
            sb.append("try app config exception: ");
            final Throwable obj;
            sb.append(obj);
            b.b.a.a.j.f.b.\u3007o00\u3007\u3007Oo("AppConfig", sb.toString());
        }
    }
    
    public static void Oo08(final Context context, final int n) {
        if (!a.\u30078\u3007oO\u3007\u30078o) {
            return;
        }
        final a \u3007080 = g.\u3007o\u3007().\u3007080(n, context);
        if (\u3007080 != null) {
            if (r.c(context)) {
                \u3007080.\u3007O\u3007(true);
            }
            else {
                \u3007080.\u300700();
            }
        }
    }
    
    private b.b.a.a.j.a OoO8() {
        if (this.oOo0 == null) {
            final b.b.a.a.j.a.b b = new b.b.a.a.j.a.b();
            final TimeUnit seconds = TimeUnit.SECONDS;
            this.oOo0 = b.\u3007o00\u3007\u3007Oo(10L, seconds).Oo08(10L, seconds).o\u30070(10L, seconds).O8();
        }
        return this.oOo0;
    }
    
    private void Oooo8o0\u3007(final int n) {
        final y o8\u3007OO0\u30070o = this.o8\u3007OO0\u30070o;
        if (o8\u3007OO0\u30070o != null) {
            ((Handler)o8\u3007OO0\u30070o).sendEmptyMessage(n);
        }
    }
    
    public static void oO80(final ThreadPoolExecutor ooo0\u3007\u3007O) {
        a.ooo0\u3007\u3007O = ooo0\u3007\u3007O;
    }
    
    private void o\u30070(final b.b.a.a.j.d.b b) {
        if (b == null) {
            return;
        }
        Address a;
        if (g.\u3007o\u3007().\u3007o00\u3007\u3007Oo(this.OO\u300700\u30078oO).OO0o\u3007\u3007() != null) {
            a = g.\u3007o\u3007().\u3007o00\u3007\u3007Oo(this.OO\u300700\u30078oO).OO0o\u3007\u3007().a(this.\u30070O);
        }
        else {
            a = null;
        }
        if (a != null && a.hasLatitude() && a.hasLongitude()) {
            final StringBuilder sb = new StringBuilder();
            sb.append(a.getLatitude());
            sb.append("");
            b.OO0o\u3007\u3007\u3007\u30070("latitude", sb.toString());
            final StringBuilder sb2 = new StringBuilder();
            sb2.append(a.getLongitude());
            sb2.append("");
            b.OO0o\u3007\u3007\u3007\u30070("longitude", sb2.toString());
            final String locality = a.getLocality();
            if (!TextUtils.isEmpty((CharSequence)locality)) {
                b.OO0o\u3007\u3007\u3007\u30070("city", Uri.encode(locality));
            }
        }
        if (this.\u3007OOo8\u30070) {
            b.OO0o\u3007\u3007\u3007\u30070("force", "1");
        }
        try {
            b.OO0o\u3007\u3007\u3007\u30070("abi", Build.SUPPORTED_ABIS[0]);
        }
        finally {
            final Throwable t;
            t.printStackTrace();
        }
        if (g.\u3007o\u3007().\u3007o00\u3007\u3007Oo(this.OO\u300700\u30078oO).OO0o\u3007\u3007() != null) {
            final StringBuilder sb3 = new StringBuilder();
            sb3.append(g.\u3007o\u3007().\u3007o00\u3007\u3007Oo(this.OO\u300700\u30078oO).OO0o\u3007\u3007().e());
            sb3.append("");
            b.OO0o\u3007\u3007\u3007\u30070("aid", sb3.toString());
            b.OO0o\u3007\u3007\u3007\u30070("device_platform", g.\u3007o\u3007().\u3007o00\u3007\u3007Oo(this.OO\u300700\u30078oO).OO0o\u3007\u3007().d());
            b.OO0o\u3007\u3007\u3007\u30070("channel", g.\u3007o\u3007().\u3007o00\u3007\u3007Oo(this.OO\u300700\u30078oO).OO0o\u3007\u3007().f());
            final StringBuilder sb4 = new StringBuilder();
            sb4.append(g.\u3007o\u3007().\u3007o00\u3007\u3007Oo(this.OO\u300700\u30078oO).OO0o\u3007\u3007().b());
            sb4.append("");
            b.OO0o\u3007\u3007\u3007\u30070("version_code", sb4.toString());
            b.OO0o\u3007\u3007\u3007\u30070("custom_info_1", g.\u3007o\u3007().\u3007o00\u3007\u3007Oo(this.OO\u300700\u30078oO).OO0o\u3007\u3007().c());
        }
    }
    
    private boolean \u30070\u3007O0088o() {
        final String[] \u3007o00 = this.\u3007O00();
        if (\u3007o00 != null) {
            if (\u3007o00.length != 0) {
                this.O8(0);
            }
        }
        return false;
    }
    
    public static void \u300780\u3007808\u3007O(final boolean \u30078\u3007oO\u3007\u30078o) {
        a.\u30078\u3007oO\u3007\u30078o = \u30078\u3007oO\u3007\u30078o;
    }
    
    public static ThreadPoolExecutor \u3007O888o0o() {
        if (a.ooo0\u3007\u3007O == null) {
            synchronized (a.class) {
                if (a.ooo0\u3007\u3007O == null) {
                    (a.ooo0\u3007\u3007O = new ThreadPoolExecutor(2, 2, 20L, TimeUnit.SECONDS, new LinkedBlockingQueue<Runnable>())).allowCoreThreadTimeOut(true);
                }
            }
        }
        return a.ooo0\u3007\u3007O;
    }
    
    private boolean \u3007O8o08O(final Object o) throws Exception {
        JSONObject jsonObject;
        if (o instanceof String) {
            final String s = (String)o;
            if (TextUtils.isEmpty((CharSequence)s)) {
                return false;
            }
            if (!"success".equals((jsonObject = new JSONObject(s)).getString("message"))) {
                return false;
            }
        }
        else if (o instanceof JSONObject) {
            jsonObject = (JSONObject)o;
        }
        else {
            jsonObject = null;
        }
        if (jsonObject == null) {
            return false;
        }
        final JSONObject jsonObject2 = jsonObject.getJSONObject("data");
        synchronized (this) {
            final SharedPreferences$Editor edit = this.\u30070O.getSharedPreferences("ss_app_config", 0).edit();
            edit.putLong("last_refresh_time", System.currentTimeMillis());
            edit.apply();
            monitorexit(this);
            if (g.\u3007o\u3007().\u3007o00\u3007\u3007Oo(this.OO\u300700\u30078oO).\u30070\u3007O0088o() != null) {
                g.\u3007o\u3007().\u3007o00\u3007\u3007Oo(this.OO\u300700\u30078oO).\u30070\u3007O0088o().\u3007o00\u3007\u3007Oo(jsonObject2);
            }
            return true;
        }
    }
    
    private String \u3007o\u3007(final String str) {
        if (TextUtils.isEmpty((CharSequence)str)) {
            return null;
        }
        final StringBuilder sb = new StringBuilder();
        sb.append("https://");
        sb.append(str);
        sb.append("/get_domains/v4/");
        return sb.toString();
    }
    
    private void \u3007\u30078O0\u30078(final boolean b) {
        if (this.\u300708O\u300700\u3007o) {
            return;
        }
        if (this.OO) {
            this.OO = false;
            this.o\u300700O = 0L;
            this.O8o08O8O = 0L;
        }
        long n;
        if (b) {
            n = 360000L;
        }
        else {
            n = 43200000L;
        }
        final long currentTimeMillis = System.currentTimeMillis();
        if (currentTimeMillis - this.o\u300700O > n && (currentTimeMillis - this.O8o08O8O > 120000L || !this.oOo\u30078o008)) {
            this.OO0o\u3007\u3007\u3007\u30070();
        }
    }
    
    public boolean OO0o\u3007\u3007\u3007\u30070() {
        final StringBuilder sb = new StringBuilder();
        sb.append("doRefresh: updating state ");
        sb.append(this.\u3007080OO8\u30070.get());
        b.b.a.a.j.f.b.\u3007o00\u3007\u3007Oo("TNCManager", sb.toString());
        \u3007O888o0o().execute(new Runnable(this) {
            final a o0;
            
            @Override
            public void run() {
                final boolean \u3007080 = e.\u3007080(this.o0.\u30070O);
                if (\u3007080) {
                    this.o0.O8o08O8O = System.currentTimeMillis();
                    if (!this.o0.\u3007080OO8\u30070.compareAndSet(false, true)) {
                        b.b.a.a.j.f.b.\u3007o00\u3007\u3007Oo("TNCManager", "doRefresh, already running");
                        return;
                    }
                    this.o0.o800o8O(\u3007080);
                }
            }
        });
        return true;
    }
    
    public void a(final Message message) {
        final int what = message.what;
        if (what != 101) {
            if (what == 102) {
                this.\u300708O\u300700\u3007o = false;
                if (this.OO) {
                    this.\u300700();
                }
                b.b.a.a.j.f.b.\u3007o00\u3007\u3007Oo("TNCManager", "doRefresh, error");
                this.\u3007080OO8\u30070.set(false);
            }
        }
        else {
            this.\u300708O\u300700\u3007o = false;
            this.o\u300700O = System.currentTimeMillis();
            b.b.a.a.j.f.b.\u3007o00\u3007\u3007Oo("TNCManager", "doRefresh, succ");
            if (this.OO) {
                this.\u300700();
            }
            this.\u3007080OO8\u30070.set(false);
        }
    }
    
    void o800o8O(final boolean b) {
        b.b.a.a.j.f.b.\u3007o00\u3007\u3007Oo("TNCManager", "doRefresh, actual request");
        this.\u3007oo\u3007();
        this.\u300708O\u300700\u3007o = true;
        if (!b) {
            ((Handler)this.o8\u3007OO0\u30070o).sendEmptyMessage(102);
            return;
        }
        try {
            this.\u30070\u3007O0088o();
        }
        catch (final Exception ex) {
            this.\u3007080OO8\u30070.set(false);
        }
    }
    
    void oo88o8O() {
        // 
        // This method could not be decompiled.
        // 
        // Original Bytecode:
        // 
        //     2: invokestatic    java/lang/System.currentTimeMillis:()J
        //     5: aload_0        
        //     6: getfield        b/b/a/a/j/e/a.o\u300700O:J
        //     9: lsub           
        //    10: ldc2_w          3600000
        //    13: lcmp           
        //    14: ifle            56
        //    17: aload_0        
        //    18: invokestatic    java/lang/System.currentTimeMillis:()J
        //    21: putfield        b/b/a/a/j/e/a.o\u300700O:J
        //    24: invokestatic    b/b/a/a/j/e/g.\u3007o\u3007:()Lb/b/a/a/j/e/g;
        //    27: aload_0        
        //    28: getfield        b/b/a/a/j/e/a.OO\u300700\u30078oO:I
        //    31: invokevirtual   b/b/a/a/j/e/g.\u3007o00\u3007\u3007Oo:(I)Lb/b/a/a/j/e/e;
        //    34: invokevirtual   b/b/a/a/j/e/e.\u30070\u3007O0088o:()Lb/b/a/a/j/e/d;
        //    37: ifnull          56
        //    40: invokestatic    b/b/a/a/j/e/g.\u3007o\u3007:()Lb/b/a/a/j/e/g;
        //    43: aload_0        
        //    44: getfield        b/b/a/a/j/e/a.OO\u300700\u30078oO:I
        //    47: invokevirtual   b/b/a/a/j/e/g.\u3007o00\u3007\u3007Oo:(I)Lb/b/a/a/j/e/e;
        //    50: invokevirtual   b/b/a/a/j/e/e.\u30070\u3007O0088o:()Lb/b/a/a/j/e/d;
        //    53: invokevirtual   b/b/a/a/j/e/d.Oo08:()V
        //    56: aload_0        
        //    57: monitorexit    
        //    58: return         
        //    59: astore_1       
        //    60: aload_0        
        //    61: monitorexit    
        //    62: aload_1        
        //    63: athrow         
        //    64: astore_1       
        //    65: goto            56
        //    Exceptions:
        //  Try           Handler
        //  Start  End    Start  End    Type                 
        //  -----  -----  -----  -----  ---------------------
        //  2      24     59     64     Any
        //  24     56     64     68     Ljava/lang/Exception;
        //  24     56     59     64     Any
        // 
        // The error that occurred was:
        // 
        // java.lang.IllegalStateException: Expression is linked from several locations: Label_0056:
        //     at com.strobel.decompiler.ast.Error.expressionLinkedFromMultipleLocations(Error.java:27)
        //     at com.strobel.decompiler.ast.AstOptimizer.mergeDisparateObjectInitializations(AstOptimizer.java:2604)
        //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:235)
        //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:42)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:206)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:93)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethodBody(AstBuilder.java:868)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethod(AstBuilder.java:761)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addTypeMembers(AstBuilder.java:638)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeCore(AstBuilder.java:605)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeNoCache(AstBuilder.java:195)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createType(AstBuilder.java:162)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addType(AstBuilder.java:137)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.buildAst(JavaLanguage.java:71)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.decompileType(JavaLanguage.java:59)
        //     at com.strobel.decompiler.DecompilerDriver.decompileType(DecompilerDriver.java:333)
        //     at com.strobel.decompiler.DecompilerDriver.decompileJar(DecompilerDriver.java:254)
        //     at com.strobel.decompiler.DecompilerDriver.main(DecompilerDriver.java:144)
        // 
        throw new IllegalStateException("An error occurred while decompiling this method.");
    }
    
    public void o\u3007O8\u3007\u3007o() {
        if (Looper.myLooper() == Looper.getMainLooper()) {
            return;
        }
        try {
            if (this.o0) {
                this.\u3007oo\u3007();
            }
            else {
                this.oo88o8O();
            }
        }
        finally {}
    }
    
    public void \u300700() {
        this.\u3007O\u3007(false);
    }
    
    public String[] \u3007O00() {
        String[] a;
        if (g.\u3007o\u3007().\u3007o00\u3007\u3007Oo(this.OO\u300700\u30078oO).OO0o\u3007\u3007() != null) {
            a = g.\u3007o\u3007().\u3007o00\u3007\u3007Oo(this.OO\u300700\u30078oO).OO0o\u3007\u3007().a();
        }
        else {
            a = null;
        }
        if (a != null) {
            final String[] array = a;
            if (a.length > 0) {
                return array;
            }
        }
        return new String[0];
    }
    
    public void \u3007O\u3007(final boolean p0) {
        // 
        // This method could not be decompiled.
        // 
        // Original Bytecode:
        // 
        //     1: monitorenter   
        //     2: aload_0        
        //     3: getfield        b/b/a/a/j/e/a.o0:Z
        //     6: ifeq            17
        //     9: aload_0        
        //    10: iload_1        
        //    11: invokespecial   b/b/a/a/j/e/a.\u3007\u30078O0\u30078:(Z)V
        //    14: goto            51
        //    17: aload_0        
        //    18: getfield        b/b/a/a/j/e/a.o\u300700O:J
        //    21: lstore_2       
        //    22: lload_2        
        //    23: lconst_0       
        //    24: lcmp           
        //    25: ifgt            51
        //    28: invokestatic    b/b/a/a/j/e/a.\u3007O888o0o:()Ljava/util/concurrent/ThreadPoolExecutor;
        //    31: astore          4
        //    33: new             Lb/b/a/a/j/e/a$a;
        //    36: astore          5
        //    38: aload           5
        //    40: aload_0        
        //    41: invokespecial   b/b/a/a/j/e/a$a.<init>:(Lb/b/a/a/j/e/a;)V
        //    44: aload           4
        //    46: aload           5
        //    48: invokevirtual   java/util/concurrent/ThreadPoolExecutor.execute:(Ljava/lang/Runnable;)V
        //    51: aload_0        
        //    52: monitorexit    
        //    53: return         
        //    54: astore          4
        //    56: aload_0        
        //    57: monitorexit    
        //    58: aload           4
        //    60: athrow         
        //    61: astore          4
        //    63: goto            51
        //    Exceptions:
        //  Try           Handler
        //  Start  End    Start  End    Type
        //  -----  -----  -----  -----  ----
        //  2      14     54     61     Any
        //  17     22     54     61     Any
        //  28     51     61     66     Any
        // 
        // The error that occurred was:
        // 
        // java.lang.IllegalStateException: Expression is linked from several locations: Label_0051:
        //     at com.strobel.decompiler.ast.Error.expressionLinkedFromMultipleLocations(Error.java:27)
        //     at com.strobel.decompiler.ast.AstOptimizer.mergeDisparateObjectInitializations(AstOptimizer.java:2604)
        //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:235)
        //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:42)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:206)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:93)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethodBody(AstBuilder.java:868)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethod(AstBuilder.java:761)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addTypeMembers(AstBuilder.java:638)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeCore(AstBuilder.java:605)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeNoCache(AstBuilder.java:195)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createType(AstBuilder.java:162)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addType(AstBuilder.java:137)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.buildAst(JavaLanguage.java:71)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.decompileType(JavaLanguage.java:59)
        //     at com.strobel.decompiler.DecompilerDriver.decompileType(DecompilerDriver.java:333)
        //     at com.strobel.decompiler.DecompilerDriver.decompileJar(DecompilerDriver.java:254)
        //     at com.strobel.decompiler.DecompilerDriver.main(DecompilerDriver.java:144)
        // 
        throw new IllegalStateException("An error occurred while decompiling this method.");
    }
    
    public void \u3007oo\u3007() {
        // 
        // This method could not be decompiled.
        // 
        // Original Bytecode:
        // 
        //     1: monitorenter   
        //     2: aload_0        
        //     3: getfield        b/b/a/a/j/e/a.oOo\u30078o008:Z
        //     6: istore          7
        //     8: iload           7
        //    10: ifeq            16
        //    13: aload_0        
        //    14: monitorexit    
        //    15: return         
        //    16: aload_0        
        //    17: iconst_1       
        //    18: putfield        b/b/a/a/j/e/a.oOo\u30078o008:Z
        //    21: aload_0        
        //    22: getfield        b/b/a/a/j/e/a.\u30070O:Landroid/content/Context;
        //    25: ldc_w           "ss_app_config"
        //    28: iconst_0       
        //    29: invokevirtual   android/content/Context.getSharedPreferences:(Ljava/lang/String;I)Landroid/content/SharedPreferences;
        //    32: ldc_w           "last_refresh_time"
        //    35: lconst_0       
        //    36: invokeinterface android/content/SharedPreferences.getLong:(Ljava/lang/String;J)J
        //    41: lstore          5
        //    43: invokestatic    java/lang/System.currentTimeMillis:()J
        //    46: lstore_3       
        //    47: lload           5
        //    49: lstore_1       
        //    50: lload           5
        //    52: lload_3        
        //    53: lcmp           
        //    54: ifle            59
        //    57: lload_3        
        //    58: lstore_1       
        //    59: aload_0        
        //    60: lload_1        
        //    61: putfield        b/b/a/a/j/e/a.o\u300700O:J
        //    64: invokestatic    b/b/a/a/j/e/g.\u3007o\u3007:()Lb/b/a/a/j/e/g;
        //    67: aload_0        
        //    68: getfield        b/b/a/a/j/e/a.OO\u300700\u30078oO:I
        //    71: invokevirtual   b/b/a/a/j/e/g.\u3007o00\u3007\u3007Oo:(I)Lb/b/a/a/j/e/e;
        //    74: invokevirtual   b/b/a/a/j/e/e.\u30070\u3007O0088o:()Lb/b/a/a/j/e/d;
        //    77: ifnull          96
        //    80: invokestatic    b/b/a/a/j/e/g.\u3007o\u3007:()Lb/b/a/a/j/e/g;
        //    83: aload_0        
        //    84: getfield        b/b/a/a/j/e/a.OO\u300700\u30078oO:I
        //    87: invokevirtual   b/b/a/a/j/e/g.\u3007o00\u3007\u3007Oo:(I)Lb/b/a/a/j/e/e;
        //    90: invokevirtual   b/b/a/a/j/e/e.\u30070\u3007O0088o:()Lb/b/a/a/j/e/d;
        //    93: invokevirtual   b/b/a/a/j/e/d.O8:()V
        //    96: aload_0        
        //    97: monitorexit    
        //    98: return         
        //    99: astore          8
        //   101: aload_0        
        //   102: monitorexit    
        //   103: aload           8
        //   105: athrow         
        //   106: astore          8
        //   108: goto            96
        //    Exceptions:
        //  Try           Handler
        //  Start  End    Start  End    Type                 
        //  -----  -----  -----  -----  ---------------------
        //  2      8      99     106    Any
        //  16     47     99     106    Any
        //  59     64     99     106    Any
        //  64     96     106    111    Ljava/lang/Exception;
        //  64     96     99     106    Any
        // 
        // The error that occurred was:
        // 
        // java.lang.IllegalStateException: Expression is linked from several locations: Label_0096:
        //     at com.strobel.decompiler.ast.Error.expressionLinkedFromMultipleLocations(Error.java:27)
        //     at com.strobel.decompiler.ast.AstOptimizer.mergeDisparateObjectInitializations(AstOptimizer.java:2604)
        //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:235)
        //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:42)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:206)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:93)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethodBody(AstBuilder.java:868)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethod(AstBuilder.java:761)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addTypeMembers(AstBuilder.java:638)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeCore(AstBuilder.java:605)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeNoCache(AstBuilder.java:195)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createType(AstBuilder.java:162)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addType(AstBuilder.java:137)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.buildAst(JavaLanguage.java:71)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.decompileType(JavaLanguage.java:59)
        //     at com.strobel.decompiler.DecompilerDriver.decompileType(DecompilerDriver.java:333)
        //     at com.strobel.decompiler.DecompilerDriver.decompileJar(DecompilerDriver.java:254)
        //     at com.strobel.decompiler.DecompilerDriver.main(DecompilerDriver.java:144)
        // 
        throw new IllegalStateException("An error occurred while decompiling this method.");
    }
}

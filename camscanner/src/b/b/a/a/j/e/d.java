// 
// Decompiled by Procyon v0.6.0
// 

package b.b.a.a.j.e;

import java.util.ArrayList;
import b.b.a.a.j.f.b;
import java.util.Iterator;
import org.json.JSONArray;
import android.text.TextUtils;
import java.util.HashMap;
import org.json.JSONObject;
import android.content.Context;

public class d
{
    private static final Object Oo08;
    private int O8;
    private Context \u3007080;
    private c \u3007o00\u3007\u3007Oo;
    private boolean \u3007o\u3007;
    
    static {
        Oo08 = new Object();
    }
    
    public d(final Context \u3007080, final boolean \u3007o\u3007, final int o8) {
        this.\u3007080 = \u3007080;
        this.\u3007o\u3007 = \u3007o\u3007;
        this.O8 = o8;
        this.\u3007o00\u3007\u3007Oo = new c();
    }
    
    private c \u3007o\u3007(final JSONObject jsonObject) {
        try {
            final c c = new c();
            final boolean has = jsonObject.has("local_enable");
            final boolean b = true;
            if (has) {
                c.\u3007080 = (jsonObject.getInt("local_enable") != 0);
            }
            if (jsonObject.has("probe_enable")) {
                boolean \u3007o00\u3007\u3007Oo = b;
                if (jsonObject.getInt("probe_enable") == 0) {
                    \u3007o00\u3007\u3007Oo = false;
                }
                c.\u3007o00\u3007\u3007Oo = \u3007o00\u3007\u3007Oo;
            }
            if (jsonObject.has("local_host_filter")) {
                final JSONArray jsonArray = jsonObject.getJSONArray("local_host_filter");
                final HashMap<String, Integer> \u3007o\u3007 = new HashMap<String, Integer>();
                if (jsonArray.length() > 0) {
                    for (int i = 0; i < jsonArray.length(); ++i) {
                        final String string = jsonArray.getString(i);
                        if (!TextUtils.isEmpty((CharSequence)string)) {
                            \u3007o\u3007.put(string, 0);
                        }
                    }
                }
                c.\u3007o\u3007 = \u3007o\u3007;
            }
            else {
                c.\u3007o\u3007 = null;
            }
            if (jsonObject.has("host_replace_map")) {
                final JSONObject jsonObject2 = jsonObject.getJSONObject("host_replace_map");
                final HashMap<String, String> o8 = new HashMap<String, String>();
                if (jsonObject2.length() > 0) {
                    final Iterator keys = jsonObject2.keys();
                    while (keys.hasNext()) {
                        final String s = keys.next();
                        final String string2 = jsonObject2.getString(s);
                        if (!TextUtils.isEmpty((CharSequence)s)) {
                            if (TextUtils.isEmpty((CharSequence)string2)) {
                                continue;
                            }
                            o8.put(s, string2);
                        }
                    }
                }
                c.O8 = o8;
            }
            else {
                c.O8 = null;
            }
            c.Oo08 = jsonObject.optInt("req_to_cnt", c.Oo08);
            c.o\u30070 = jsonObject.optInt("req_to_api_cnt", c.o\u30070);
            c.\u3007\u3007888 = jsonObject.optInt("req_to_ip_cnt", c.\u3007\u3007888);
            c.oO80 = jsonObject.optInt("req_err_cnt", c.oO80);
            c.\u300780\u3007808\u3007O = jsonObject.optInt("req_err_api_cnt", c.\u300780\u3007808\u3007O);
            c.OO0o\u3007\u3007\u3007\u30070 = jsonObject.optInt("req_err_ip_cnt", c.OO0o\u3007\u3007\u3007\u30070);
            c.\u30078o8o\u3007 = jsonObject.optInt("update_interval", c.\u30078o8o\u3007);
            c.\u3007O8o08O = jsonObject.optInt("update_random_range", c.\u3007O8o08O);
            c.OO0o\u3007\u3007 = jsonObject.optString("http_code_black", c.OO0o\u3007\u3007);
            return c;
        }
        finally {
            final Throwable t;
            t.printStackTrace();
            return null;
        }
    }
    
    public void O8() {
        if (!this.\u3007o\u3007) {
            return;
        }
        final String string = this.\u3007080.getSharedPreferences(g.\u3007o\u3007().\u3007o00\u3007\u3007Oo(this.O8).\u3007O888o0o(), 0).getString("tnc_config_str", (String)null);
        if (TextUtils.isEmpty((CharSequence)string)) {
            b.\u3007o00\u3007\u3007Oo("TNCConfigHandler", "loadLocalConfig: no existed");
            return;
        }
        try {
            final c \u3007o\u3007 = this.\u3007o\u3007(new JSONObject(string));
            if (\u3007o\u3007 != null) {
                this.\u3007o00\u3007\u3007Oo = \u3007o\u3007;
            }
            final StringBuilder sb = new StringBuilder();
            sb.append("loadLocalConfig: ");
            String string2;
            if (\u3007o\u3007 == null) {
                string2 = "null";
            }
            else {
                string2 = \u3007o\u3007.toString();
            }
            sb.append(string2);
            b.\u3007o00\u3007\u3007Oo("TNCConfigHandler", sb.toString());
        }
        finally {
            final Throwable t;
            t.printStackTrace();
            final StringBuilder sb2 = new StringBuilder();
            sb2.append("loadLocalConfig: except: ");
            sb2.append(t.getMessage());
            b.\u3007o00\u3007\u3007Oo("TNCConfigHandler", sb2.toString());
        }
    }
    
    public void Oo08() {
        try {
            final String \u3007o00\u3007\u3007Oo = b.b.a.a.j.f.c.\u3007o00\u3007\u3007Oo(this.\u3007080, 1, this.O8);
            if (TextUtils.isEmpty((CharSequence)\u3007o00\u3007\u3007Oo)) {
                b.\u3007o00\u3007\u3007Oo("TNCConfigHandler", "loadLocalConfigForOtherProcess, data empty");
                return;
            }
            final c \u3007o\u3007 = this.\u3007o\u3007(new JSONObject(\u3007o00\u3007\u3007Oo));
            final StringBuilder sb = new StringBuilder();
            sb.append("loadLocalConfigForOtherProcess, config: ");
            String string;
            if (\u3007o\u3007 == null) {
                string = "null";
            }
            else {
                string = \u3007o\u3007.toString();
            }
            sb.append(string);
            b.\u3007o00\u3007\u3007Oo("TNCConfigHandler", sb.toString());
            if (\u3007o\u3007 != null) {
                this.\u3007o00\u3007\u3007Oo = \u3007o\u3007;
            }
        }
        finally {
            final StringBuilder sb2 = new StringBuilder();
            sb2.append("loadLocalConfigForOtherProcess, except: ");
            final Throwable t;
            sb2.append(t.getMessage());
            b.\u3007o00\u3007\u3007Oo("TNCConfigHandler", sb2.toString());
        }
    }
    
    public c \u3007080() {
        return this.\u3007o00\u3007\u3007Oo;
    }
    
    public void \u3007o00\u3007\u3007Oo(JSONObject jsonObject) {
        if (!this.\u3007o\u3007) {
            b.\u3007o00\u3007\u3007Oo("TNCConfigHandler", "handleConfigChanged: no mainProc");
            return;
        }
        g.\u3007o\u3007().\u3007o00\u3007\u3007Oo(this.O8).o\u3007O8\u3007\u3007o();
        try {
            final boolean b = jsonObject.optInt("ttnet_url_dispatcher_enabled", 0) > 0;
            final JSONArray optJSONArray = jsonObject.optJSONArray("ttnet_dispatch_actions");
            Object o = null;
            Label_0249: {
                if (g.\u3007o\u3007().\u3007o00\u3007\u3007Oo(this.O8).oo88o8O() && b && optJSONArray != null) {
                    final ArrayList<JSONObject> list = new ArrayList<JSONObject>();
                    for (int i = 0; i < optJSONArray.length(); ++i) {
                        final JSONObject jsonObject2 = ((JSONObject)optJSONArray.get(i)).getJSONObject("param");
                        if (jsonObject2.optString("service_name", "").equals("idc_selection")) {
                            list.add(jsonObject2.getJSONObject("strategy_info"));
                        }
                    }
                    if (!list.isEmpty()) {
                        final JSONObject jsonObject3 = new JSONObject();
                        final Iterator<JSONObject> iterator = list.iterator();
                        while (true) {
                            o = jsonObject3;
                            if (!iterator.hasNext()) {
                                break Label_0249;
                            }
                            final JSONObject jsonObject4 = iterator.next();
                            final Iterator keys = jsonObject4.keys();
                            while (keys.hasNext()) {
                                final String s = keys.next();
                                jsonObject3.put(s, (Object)jsonObject4.getString(s));
                            }
                        }
                    }
                }
                o = null;
            }
            final JSONObject optJSONObject = jsonObject.optJSONObject("tnc_config");
            if (optJSONObject == null && o == null) {
                b.b.a.a.j.f.b.\u3007o\u3007("TNCConfigHandler", " tnc host_replace_map config is null");
                jsonObject = optJSONObject;
            }
            else if (optJSONObject == null) {
                jsonObject = new JSONObject();
                jsonObject.put("host_replace_map", o);
            }
            else {
                jsonObject = optJSONObject;
                if (o != null) {
                    optJSONObject.put("host_replace_map", o);
                    jsonObject = optJSONObject;
                }
            }
            final c \u3007o\u3007 = this.\u3007o\u3007(jsonObject);
            final StringBuilder sb = new StringBuilder();
            sb.append("handleConfigChanged, newConfig: ");
            String string;
            if (\u3007o\u3007 == null) {
                string = "null";
            }
            else {
                string = \u3007o\u3007.toString();
            }
            sb.append(string);
            b.b.a.a.j.f.b.\u3007o00\u3007\u3007Oo("TNCConfigHandler", sb.toString());
            if (\u3007o\u3007 == null) {
                synchronized (d.Oo08) {
                    this.\u3007080.getSharedPreferences(g.\u3007o\u3007().\u3007o00\u3007\u3007Oo(this.O8).\u3007O888o0o(), 0).edit().putString("tnc_config_str", "").apply();
                    b.b.a.a.j.f.c.\u3007o\u3007(this.\u3007080, 1, "", this.O8);
                    return;
                }
            }
            this.\u3007o00\u3007\u3007Oo = \u3007o\u3007;
            final String string2 = jsonObject.toString();
            synchronized (d.Oo08) {
                this.\u3007080.getSharedPreferences(g.\u3007o\u3007().\u3007o00\u3007\u3007Oo(this.O8).\u3007O888o0o(), 0).edit().putString("tnc_config_str", string2).apply();
                b.b.a.a.j.f.c.\u3007o\u3007(this.\u3007080, 1, string2, this.O8);
            }
        }
        finally {
            try {
                final Throwable t;
                t.printStackTrace();
                this.\u3007o00\u3007\u3007Oo = new c();
                synchronized (d.Oo08) {
                    this.\u3007080.getSharedPreferences(g.\u3007o\u3007().\u3007o00\u3007\u3007Oo(this.O8).\u3007O888o0o(), 0).edit().putString("tnc_config_str", "").apply();
                    b.b.a.a.j.f.c.\u3007o\u3007(this.\u3007080, 1, "", this.O8);
                    monitorexit(d.Oo08);
                }
            }
            finally {
                synchronized (d.Oo08) {
                    this.\u3007080.getSharedPreferences(g.\u3007o\u3007().\u3007o00\u3007\u3007Oo(this.O8).\u3007O888o0o(), 0).edit().putString("tnc_config_str", "").apply();
                    b.b.a.a.j.f.c.\u3007o\u3007(this.\u3007080, 1, "", this.O8);
                    monitorexit(d.Oo08);
                }
            }
        }
    }
}

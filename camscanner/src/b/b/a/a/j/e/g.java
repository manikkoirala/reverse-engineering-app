// 
// Decompiled by Procyon v0.6.0
// 

package b.b.a.a.j.e;

import android.content.Context;
import java.util.HashMap;

public class g
{
    private static volatile g \u3007080;
    private static HashMap<Integer, e> \u3007o00\u3007\u3007Oo;
    private static HashMap<Integer, a> \u3007o\u3007;
    
    private g() {
        g.\u3007o00\u3007\u3007Oo = new HashMap<Integer, e>();
        g.\u3007o\u3007 = new HashMap<Integer, a>();
    }
    
    public static g \u3007o\u3007() {
        synchronized (g.class) {
            if (g.\u3007080 == null) {
                synchronized (g.class) {
                    if (g.\u3007080 == null) {
                        g.\u3007080 = new g();
                    }
                }
            }
            return g.\u3007080;
        }
    }
    
    public a \u3007080(final int n, final Context context) {
        a value;
        if ((value = g.\u3007o\u3007.get(n)) == null) {
            value = new a(context, n);
            g.\u3007o\u3007.put(n, value);
        }
        return value;
    }
    
    public e \u3007o00\u3007\u3007Oo(final int n) {
        e value;
        if ((value = g.\u3007o00\u3007\u3007Oo.get(n)) == null) {
            value = new e(n);
            g.\u3007o00\u3007\u3007Oo.put(n, value);
        }
        return value;
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package b.b.a.a.j.d;

import android.text.TextUtils;
import b.b.a.a.f.a.l;
import java.util.Iterator;
import java.util.UUID;
import java.util.HashMap;
import java.util.Map;
import b.b.a.a.f.a.j;

public abstract class c
{
    protected String O8;
    protected j \u3007080;
    protected String \u3007o00\u3007\u3007Oo;
    protected final Map<String, String> \u3007o\u3007;
    
    public c(final j \u3007080) {
        this.\u3007o00\u3007\u3007Oo = null;
        this.\u3007o\u3007 = new HashMap<String, String>();
        this.O8 = null;
        this.\u3007080 = \u3007080;
        this.\u3007o\u3007(UUID.randomUUID().toString());
    }
    
    public void O8(final String s, final String s2) {
        this.\u3007o\u3007.put(s, s2);
    }
    
    public void Oo08(final Map<String, String> map) {
        if (map != null) {
            for (final Map.Entry<String, V> entry : map.entrySet()) {
                this.\u3007o\u3007.put(entry.getKey(), (String)entry.getValue());
            }
        }
    }
    
    public void o\u30070(final String o8) {
        this.O8 = o8;
    }
    
    public String \u3007080() {
        return this.\u3007o00\u3007\u3007Oo;
    }
    
    protected void \u3007o00\u3007\u3007Oo(final l.a a) {
        if (a == null) {
            return;
        }
        if (this.\u3007o\u3007.size() > 0) {
            for (final Map.Entry<String, V> entry : this.\u3007o\u3007.entrySet()) {
                final String s = entry.getKey();
                if (TextUtils.isEmpty((CharSequence)s)) {
                    continue;
                }
                String s2;
                if ((s2 = (String)entry.getValue()) == null) {
                    s2 = "";
                }
                a.\u3007\u3007888(s, s2);
            }
        }
    }
    
    public void \u3007o\u3007(final String \u3007o00\u3007\u3007Oo) {
        this.\u3007o00\u3007\u3007Oo = \u3007o00\u3007\u3007Oo;
    }
}

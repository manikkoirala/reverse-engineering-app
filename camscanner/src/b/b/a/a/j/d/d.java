// 
// Decompiled by Procyon v0.6.0
// 

package b.b.a.a.j.d;

import java.io.IOException;
import b.b.a.a.j.c.a;
import b.b.a.a.f.a.f;
import b.b.a.a.f.a.n;
import java.util.HashMap;
import java.util.Map;
import b.b.a.a.f.a.l;
import b.b.a.a.j.b;
import android.text.TextUtils;
import b.b.a.a.f.a.i;
import org.json.JSONObject;
import b.b.a.a.f.a.j;
import b.b.a.a.f.a.m;

public class d extends c
{
    m Oo08;
    
    public d(final j j) {
        super(j);
        this.Oo08 = null;
    }
    
    private byte[] OO0o\u3007\u3007(final String p0) {
        // 
        // This method could not be decompiled.
        // 
        // Original Bytecode:
        // 
        //     1: astore          7
        //     3: aconst_null    
        //     4: astore          4
        //     6: aconst_null    
        //     7: astore          8
        //     9: aload_1        
        //    10: ifnull          271
        //    13: aload_1        
        //    14: invokevirtual   java/lang/String.length:()I
        //    17: ifne            23
        //    20: goto            271
        //    23: iconst_0       
        //    24: newarray        B
        //    26: astore          6
        //    28: new             Ljava/io/ByteArrayOutputStream;
        //    31: astore_2       
        //    32: aload_2        
        //    33: invokespecial   java/io/ByteArrayOutputStream.<init>:()V
        //    36: aload           8
        //    38: astore          4
        //    40: aload_2        
        //    41: astore_3       
        //    42: new             Ljava/util/zip/GZIPOutputStream;
        //    45: astore          5
        //    47: aload           8
        //    49: astore          4
        //    51: aload_2        
        //    52: astore_3       
        //    53: aload           5
        //    55: aload_2        
        //    56: invokespecial   java/util/zip/GZIPOutputStream.<init>:(Ljava/io/OutputStream;)V
        //    59: aload           5
        //    61: aload_1        
        //    62: ldc             "utf-8"
        //    64: invokevirtual   java/lang/String.getBytes:(Ljava/lang/String;)[B
        //    67: invokevirtual   java/io/OutputStream.write:([B)V
        //    70: aload           5
        //    72: invokevirtual   java/io/OutputStream.close:()V
        //    75: goto            88
        //    78: astore_1       
        //    79: ldc             "PostExecutor"
        //    81: aload_1        
        //    82: invokevirtual   java/lang/Object.toString:()Ljava/lang/String;
        //    85: invokestatic    com/bytedance/sdk/component/utils/m.b:(Ljava/lang/String;Ljava/lang/String;)V
        //    88: aload_2        
        //    89: invokevirtual   java/io/ByteArrayOutputStream.toByteArray:()[B
        //    92: astore_3       
        //    93: aload_3        
        //    94: astore_1       
        //    95: aload_2        
        //    96: invokevirtual   java/io/ByteArrayOutputStream.close:()V
        //    99: aload_3        
        //   100: astore_1       
        //   101: goto            218
        //   104: astore_1       
        //   105: aload           5
        //   107: astore          4
        //   109: goto            220
        //   112: astore_3       
        //   113: aload           5
        //   115: astore_1       
        //   116: aload_3        
        //   117: astore          5
        //   119: goto            149
        //   122: astore_1       
        //   123: aload_3        
        //   124: astore_2       
        //   125: goto            220
        //   128: astore          5
        //   130: aload           7
        //   132: astore_1       
        //   133: goto            149
        //   136: astore_1       
        //   137: aconst_null    
        //   138: astore_2       
        //   139: goto            220
        //   142: astore          5
        //   144: aconst_null    
        //   145: astore_2       
        //   146: aload           7
        //   148: astore_1       
        //   149: aload_1        
        //   150: astore          4
        //   152: aload_2        
        //   153: astore_3       
        //   154: ldc             "PostExecutor"
        //   156: aload           5
        //   158: invokevirtual   java/lang/Object.toString:()Ljava/lang/String;
        //   161: invokestatic    com/bytedance/sdk/component/utils/m.b:(Ljava/lang/String;Ljava/lang/String;)V
        //   164: aload_1        
        //   165: ifnull          185
        //   168: aload_1        
        //   169: invokevirtual   java/io/OutputStream.close:()V
        //   172: goto            185
        //   175: astore_1       
        //   176: ldc             "PostExecutor"
        //   178: aload_1        
        //   179: invokevirtual   java/lang/Object.toString:()Ljava/lang/String;
        //   182: invokestatic    com/bytedance/sdk/component/utils/m.b:(Ljava/lang/String;Ljava/lang/String;)V
        //   185: aload           6
        //   187: astore_1       
        //   188: aload_2        
        //   189: ifnull          218
        //   192: aload_2        
        //   193: invokevirtual   java/io/ByteArrayOutputStream.toByteArray:()[B
        //   196: astore_3       
        //   197: aload_3        
        //   198: astore_1       
        //   199: aload_2        
        //   200: invokevirtual   java/io/ByteArrayOutputStream.close:()V
        //   203: aload_3        
        //   204: astore_1       
        //   205: goto            218
        //   208: astore_2       
        //   209: ldc             "PostExecutor"
        //   211: aload_2        
        //   212: invokevirtual   java/lang/Object.toString:()Ljava/lang/String;
        //   215: invokestatic    com/bytedance/sdk/component/utils/m.b:(Ljava/lang/String;Ljava/lang/String;)V
        //   218: aload_1        
        //   219: areturn        
        //   220: aload           4
        //   222: ifnull          243
        //   225: aload           4
        //   227: invokevirtual   java/io/OutputStream.close:()V
        //   230: goto            243
        //   233: astore_3       
        //   234: ldc             "PostExecutor"
        //   236: aload_3        
        //   237: invokevirtual   java/lang/Object.toString:()Ljava/lang/String;
        //   240: invokestatic    com/bytedance/sdk/component/utils/m.b:(Ljava/lang/String;Ljava/lang/String;)V
        //   243: aload_2        
        //   244: ifnull          269
        //   247: aload_2        
        //   248: invokevirtual   java/io/ByteArrayOutputStream.toByteArray:()[B
        //   251: pop            
        //   252: aload_2        
        //   253: invokevirtual   java/io/ByteArrayOutputStream.close:()V
        //   256: goto            269
        //   259: astore_2       
        //   260: ldc             "PostExecutor"
        //   262: aload_2        
        //   263: invokevirtual   java/lang/Object.toString:()Ljava/lang/String;
        //   266: invokestatic    com/bytedance/sdk/component/utils/m.b:(Ljava/lang/String;Ljava/lang/String;)V
        //   269: aload_1        
        //   270: athrow         
        //   271: aconst_null    
        //   272: areturn        
        //    Exceptions:
        //  Try           Handler
        //  Start  End    Start  End    Type                 
        //  -----  -----  -----  -----  ---------------------
        //  28     36     142    149    Ljava/io/IOException;
        //  28     36     136    142    Any
        //  42     47     128    136    Ljava/io/IOException;
        //  42     47     122    128    Any
        //  53     59     128    136    Ljava/io/IOException;
        //  53     59     122    128    Any
        //  59     70     112    122    Ljava/io/IOException;
        //  59     70     104    112    Any
        //  70     75     78     88     Ljava/io/IOException;
        //  95     99     208    218    Ljava/io/IOException;
        //  154    164    122    128    Any
        //  168    172    175    185    Ljava/io/IOException;
        //  199    203    208    218    Ljava/io/IOException;
        //  225    230    233    243    Ljava/io/IOException;
        //  252    256    259    269    Ljava/io/IOException;
        // 
        // The error that occurred was:
        // 
        // java.util.ConcurrentModificationException
        //     at java.base/java.util.ArrayList$Itr.checkForComodification(ArrayList.java:1013)
        //     at java.base/java.util.ArrayList$Itr.next(ArrayList.java:967)
        //     at com.strobel.decompiler.ast.AstBuilder.convertLocalVariables(AstBuilder.java:2913)
        //     at com.strobel.decompiler.ast.AstBuilder.performStackAnalysis(AstBuilder.java:2501)
        //     at com.strobel.decompiler.ast.AstBuilder.build(AstBuilder.java:108)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:203)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:93)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethodBody(AstBuilder.java:868)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethod(AstBuilder.java:761)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addTypeMembers(AstBuilder.java:638)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeCore(AstBuilder.java:605)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeNoCache(AstBuilder.java:195)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createType(AstBuilder.java:162)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addType(AstBuilder.java:137)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.buildAst(JavaLanguage.java:71)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.decompileType(JavaLanguage.java:59)
        //     at com.strobel.decompiler.DecompilerDriver.decompileType(DecompilerDriver.java:333)
        //     at com.strobel.decompiler.DecompilerDriver.decompileJar(DecompilerDriver.java:254)
        //     at com.strobel.decompiler.DecompilerDriver.main(DecompilerDriver.java:144)
        // 
        throw new IllegalStateException("An error occurred while decompiling this method.");
    }
    
    public void OO0o\u3007\u3007\u3007\u30070(final JSONObject jsonObject) {
        String string;
        if (jsonObject != null) {
            string = jsonObject.toString();
        }
        else {
            string = "{}";
        }
        this.Oo08 = m.\u3007080(i.\u3007080("application/json; charset=utf-8"), string);
    }
    
    public void Oooo8o0\u3007(final String s) {
        String s2 = s;
        if (TextUtils.isEmpty((CharSequence)s)) {
            s2 = "{}";
        }
        this.Oo08 = m.\u3007080(i.\u3007080("application/json; charset=utf-8"), s2);
    }
    
    public void oO80(final String s, final boolean b) {
        if (b) {
            this.\u300780\u3007808\u3007O("application/json; charset=utf-8", this.OO0o\u3007\u3007(s));
            this.O8("Content-Encoding", "gzip");
        }
        else {
            this.Oooo8o0\u3007(s);
        }
    }
    
    public void \u300780\u3007808\u3007O(final String s, final byte[] array) {
        this.Oo08 = m.\u3007o00\u3007\u3007Oo(i.\u3007080(s), array);
    }
    
    public b \u30078o8o\u3007() {
        try {
            final l.a a = new l.a();
            if (TextUtils.isEmpty((CharSequence)super.O8)) {
                b.b.a.a.j.f.d.\u3007o00\u3007\u3007Oo("PostExecutor", "execute: Url is Empty");
                return new b(false, 5000, "URL_NULL_MSG", null, "URL_NULL_BODY", 1L, 1L);
            }
            a.Oo08(super.O8);
            if (this.Oo08 == null) {
                b.b.a.a.j.f.d.\u3007o00\u3007\u3007Oo("PostExecutor", "RequestBody is null, content type is not support!!");
                return new b(false, 5000, "BODY_NULL_MSG", null, "BODY_NULL_BODY", 1L, 1L);
            }
            this.\u3007o00\u3007\u3007Oo(a);
            a.O8(this.\u3007080());
            final n b = super.\u3007080.\u3007080(a.\u3007o\u3007(this.Oo08).oO80()).b();
            if (b != null) {
                final HashMap hashMap = new HashMap();
                final f \u30078o8o\u3007 = b.\u30078o8o\u3007();
                if (\u30078o8o\u3007 != null) {
                    for (int i = 0; i < \u30078o8o\u3007.\u3007080(); ++i) {
                        hashMap.put(\u30078o8o\u3007.\u3007o00\u3007\u3007Oo(i), \u30078o8o\u3007.\u3007o\u3007(i));
                    }
                }
                return new b(b.\u3007O8o08O(), b.oO80(), b.OO0o\u3007\u3007(), hashMap, b.Oo08().\u30078o8o\u3007(), b.\u3007O00(), b.\u3007O\u3007());
            }
            return null;
        }
        finally {
            final Throwable t;
            return new b(false, 5001, t.getMessage(), null, "BODY_NULL_BODY", 1L, 1L);
        }
    }
    
    public m \u3007O8o08O() {
        return this.Oo08;
    }
    
    public void \u3007\u3007888(final a a) {
        try {
            final l.a a2 = new l.a();
            if (TextUtils.isEmpty((CharSequence)super.O8)) {
                a.a(this, new IOException("Url is Empty"));
                return;
            }
            a2.Oo08(super.O8);
            if (this.Oo08 == null) {
                if (a != null) {
                    a.a(this, new IOException("RequestBody is null, content type is not support!!"));
                }
                return;
            }
            this.\u3007o00\u3007\u3007Oo(a2);
            a2.O8(this.\u3007080());
            super.\u3007080.\u3007080(a2.\u3007o\u3007(this.Oo08).oO80()).\u3007O00(new b.b.a.a.f.a.c(this, a) {
                final a \u3007080;
                final d \u3007o00\u3007\u3007Oo;
                
                @Override
                public void \u3007080(b.b.a.a.f.a.b b, final n n) throws IOException {
                    final a \u3007080 = this.\u3007080;
                    if (\u3007080 != null) {
                        if (n == null) {
                            \u3007080.a(this.\u3007o00\u3007\u3007Oo, new IOException("No response"));
                            return;
                        }
                        b = null;
                        b.b.a.a.f.a.b b3 = null;
                        IOException ex2 = null;
                        try {
                            final HashMap hashMap = new HashMap();
                            final f \u30078o8o\u3007 = n.\u30078o8o\u3007();
                            if (\u30078o8o\u3007 != null) {
                                for (int i = 0; i < \u30078o8o\u3007.\u3007080(); ++i) {
                                    hashMap.put(\u30078o8o\u3007.\u3007o00\u3007\u3007Oo(i), \u30078o8o\u3007.\u3007o\u3007(i));
                                }
                            }
                            final b b2 = new b(n.\u3007O8o08O(), n.oO80(), n.OO0o\u3007\u3007(), hashMap, n.Oo08().\u30078o8o\u3007(), n.\u3007O00(), n.\u3007O\u3007());
                            b = null;
                        }
                        finally {
                            final Throwable cause;
                            final IOException ex = new IOException(cause);
                            b3 = b;
                            ex2 = ex;
                        }
                        if (b3 != null) {
                            this.\u3007080.a(this.\u3007o00\u3007\u3007Oo, (b)b3);
                        }
                        else {
                            final a \u300781 = this.\u3007080;
                            final d \u3007o00\u3007\u3007Oo = this.\u3007o00\u3007\u3007Oo;
                            IOException ex3;
                            if ((ex3 = ex2) == null) {
                                ex3 = new IOException("Unexpected exception");
                            }
                            \u300781.a(\u3007o00\u3007\u3007Oo, ex3);
                        }
                    }
                }
                
                @Override
                public void \u3007o00\u3007\u3007Oo(final b.b.a.a.f.a.b b, final IOException ex) {
                    final a \u3007080 = this.\u3007080;
                    if (\u3007080 != null) {
                        \u3007080.a(this.\u3007o00\u3007\u3007Oo, ex);
                    }
                }
            });
        }
        finally {
            final Throwable t;
            t.printStackTrace();
            a.a(this, new IOException(t.getMessage()));
        }
    }
}

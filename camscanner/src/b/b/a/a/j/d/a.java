// 
// Decompiled by Procyon v0.6.0
// 

package b.b.a.a.j.d;

import b.b.a.a.f.a.n;
import java.io.IOException;
import b.b.a.a.f.a.l;
import b.b.a.a.j.b;
import android.text.TextUtils;
import java.util.Map;
import b.b.a.a.f.a.j;
import java.io.File;

public class a extends c
{
    public File Oo08;
    public File o\u30070;
    
    public a(final j j) {
        super(j);
    }
    
    private void \u30078o8o\u3007() {
        // 
        // This method could not be decompiled.
        // 
        // Original Bytecode:
        // 
        //     1: getfield        b/b/a/a/j/d/a.Oo08:Ljava/io/File;
        //     4: invokevirtual   java/io/File.delete:()Z
        //     7: pop            
        //     8: aload_0        
        //     9: getfield        b/b/a/a/j/d/a.o\u30070:Ljava/io/File;
        //    12: invokevirtual   java/io/File.delete:()Z
        //    15: pop            
        //    16: return         
        //    17: astore_1       
        //    18: goto            8
        //    21: astore_1       
        //    22: goto            16
        //    Exceptions:
        //  Try           Handler
        //  Start  End    Start  End    Type
        //  -----  -----  -----  -----  ----
        //  0      8      17     21     Any
        //  8      16     21     25     Any
        // 
        // The error that occurred was:
        // 
        // java.lang.IllegalStateException: Expression is linked from several locations: Label_0008:
        //     at com.strobel.decompiler.ast.Error.expressionLinkedFromMultipleLocations(Error.java:27)
        //     at com.strobel.decompiler.ast.AstOptimizer.mergeDisparateObjectInitializations(AstOptimizer.java:2604)
        //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:235)
        //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:42)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:206)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:93)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethodBody(AstBuilder.java:868)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethod(AstBuilder.java:761)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addTypeMembers(AstBuilder.java:638)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeCore(AstBuilder.java:605)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeNoCache(AstBuilder.java:195)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createType(AstBuilder.java:162)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addType(AstBuilder.java:137)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.buildAst(JavaLanguage.java:71)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.decompileType(JavaLanguage.java:59)
        //     at com.strobel.decompiler.DecompilerDriver.decompileType(DecompilerDriver.java:333)
        //     at com.strobel.decompiler.DecompilerDriver.decompileJar(DecompilerDriver.java:254)
        //     at com.strobel.decompiler.DecompilerDriver.main(DecompilerDriver.java:144)
        // 
        throw new IllegalStateException("An error occurred while decompiling this method.");
    }
    
    private static boolean \u3007O00(final Map<String, String> map) {
        return TextUtils.equals((CharSequence)map.get("Content-Encoding"), (CharSequence)"gzip");
    }
    
    private static long \u3007O\u3007(final Map<String, String> map) {
        String s;
        if (map.containsKey("content-length")) {
            s = map.get("content-length");
        }
        else if (map.containsKey("Content-Length")) {
            s = map.get("Content-Length");
        }
        else {
            s = null;
        }
        final boolean empty = TextUtils.isEmpty((CharSequence)s);
        final long n = 0L;
        if (empty) {
            return 0L;
        }
        long longValue = n;
        if (s == null) {
            return longValue;
        }
        try {
            longValue = Long.valueOf(s);
            return longValue;
        }
        finally {
            longValue = n;
            return longValue;
        }
    }
    
    private static boolean \u3007\u30078O0\u30078(final Map<String, String> map) {
        final boolean equals = TextUtils.equals((CharSequence)map.get("Accept-Ranges"), (CharSequence)"bytes");
        boolean b = true;
        if (equals) {
            return true;
        }
        if (TextUtils.equals((CharSequence)map.get("accept-ranges"), (CharSequence)"bytes")) {
            return true;
        }
        String s;
        if (TextUtils.isEmpty((CharSequence)(s = map.get("Content-Range")))) {
            s = map.get("content-range");
        }
        if (s == null || !s.startsWith("bytes")) {
            b = false;
        }
        return b;
    }
    
    public b OO0o\u3007\u3007() {
        // 
        // This method could not be decompiled.
        // 
        // Original Bytecode:
        // 
        //     1: getfield        b/b/a/a/j/d/a.Oo08:Ljava/io/File;
        //     4: astore          15
        //     6: aload           15
        //     8: ifnull          1340
        //    11: aload_0        
        //    12: getfield        b/b/a/a/j/d/a.o\u30070:Ljava/io/File;
        //    15: ifnonnull       21
        //    18: goto            1340
        //    21: aload           15
        //    23: invokevirtual   java/io/File.exists:()Z
        //    26: ifeq            79
        //    29: aload_0        
        //    30: getfield        b/b/a/a/j/d/a.Oo08:Ljava/io/File;
        //    33: invokevirtual   java/io/File.length:()J
        //    36: lconst_0       
        //    37: lcmp           
        //    38: ifeq            79
        //    41: invokestatic    java/lang/System.currentTimeMillis:()J
        //    44: lstore          5
        //    46: new             Lb/b/a/a/j/b;
        //    49: dup            
        //    50: iconst_1       
        //    51: sipush          200
        //    54: ldc             "Success"
        //    56: aconst_null    
        //    57: aconst_null    
        //    58: lload           5
        //    60: lload           5
        //    62: invokespecial   b/b/a/a/j/b.<init>:(ZILjava/lang/String;Ljava/util/Map;Ljava/lang/String;JJ)V
        //    65: astore          15
        //    67: aload           15
        //    69: aload_0        
        //    70: getfield        b/b/a/a/j/d/a.Oo08:Ljava/io/File;
        //    73: invokevirtual   b/b/a/a/j/b.\u3007o00\u3007\u3007Oo:(Ljava/io/File;)V
        //    76: aload           15
        //    78: areturn        
        //    79: aload_0        
        //    80: getfield        b/b/a/a/j/d/a.o\u30070:Ljava/io/File;
        //    83: invokevirtual   java/io/File.length:()J
        //    86: lstore          7
        //    88: lload           7
        //    90: lstore          5
        //    92: lload           7
        //    94: lconst_0       
        //    95: lcmp           
        //    96: ifge            102
        //    99: lconst_0       
        //   100: lstore          5
        //   102: new             Lb/b/a/a/f/a/l$a;
        //   105: dup            
        //   106: invokespecial   b/b/a/a/f/a/l$a.<init>:()V
        //   109: astore          16
        //   111: aload           16
        //   113: aload_0        
        //   114: invokevirtual   b/b/a/a/j/d/c.\u3007080:()Ljava/lang/String;
        //   117: invokevirtual   b/b/a/a/f/a/l$a.O8:(Ljava/lang/Object;)Lb/b/a/a/f/a/l$a;
        //   120: pop            
        //   121: new             Ljava/lang/StringBuilder;
        //   124: dup            
        //   125: invokespecial   java/lang/StringBuilder.<init>:()V
        //   128: astore          15
        //   130: aload           15
        //   132: ldc             "bytes="
        //   134: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   137: pop            
        //   138: aload           15
        //   140: lload           5
        //   142: invokevirtual   java/lang/StringBuilder.append:(J)Ljava/lang/StringBuilder;
        //   145: pop            
        //   146: aload           15
        //   148: ldc             "-"
        //   150: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   153: pop            
        //   154: aload_0        
        //   155: ldc             "Range"
        //   157: aload           15
        //   159: invokevirtual   java/lang/StringBuilder.toString:()Ljava/lang/String;
        //   162: invokevirtual   b/b/a/a/j/d/c.O8:(Ljava/lang/String;Ljava/lang/String;)V
        //   165: aload_0        
        //   166: getfield        b/b/a/a/j/d/c.O8:Ljava/lang/String;
        //   169: invokestatic    android/text/TextUtils.isEmpty:(Ljava/lang/CharSequence;)Z
        //   172: ifeq            177
        //   175: aconst_null    
        //   176: areturn        
        //   177: aload           16
        //   179: aload_0        
        //   180: getfield        b/b/a/a/j/d/c.O8:Ljava/lang/String;
        //   183: invokevirtual   b/b/a/a/f/a/l$a.Oo08:(Ljava/lang/String;)Lb/b/a/a/f/a/l$a;
        //   186: pop            
        //   187: aload_0        
        //   188: aload           16
        //   190: invokevirtual   b/b/a/a/j/d/c.\u3007o00\u3007\u3007Oo:(Lb/b/a/a/f/a/l$a;)V
        //   193: aload           16
        //   195: invokevirtual   b/b/a/a/f/a/l$a.\u300780\u3007808\u3007O:()Lb/b/a/a/f/a/l$a;
        //   198: invokevirtual   b/b/a/a/f/a/l$a.oO80:()Lb/b/a/a/f/a/l;
        //   201: astore          15
        //   203: aload_0        
        //   204: getfield        b/b/a/a/j/d/c.\u3007080:Lb/b/a/a/f/a/j;
        //   207: aload           15
        //   209: invokevirtual   b/b/a/a/f/a/j.\u3007080:(Lb/b/a/a/f/a/l;)Lb/b/a/a/f/a/b;
        //   212: invokeinterface b/b/a/a/f/a/b.b:()Lb/b/a/a/f/a/n;
        //   217: astore          16
        //   219: aload           16
        //   221: ifnull          1320
        //   224: aload           16
        //   226: invokevirtual   b/b/a/a/f/a/n.\u3007O8o08O:()Z
        //   229: ifeq            1320
        //   232: new             Ljava/util/HashMap;
        //   235: astore          21
        //   237: aload           21
        //   239: invokespecial   java/util/HashMap.<init>:()V
        //   242: aload           16
        //   244: invokevirtual   b/b/a/a/f/a/n.\u30078o8o\u3007:()Lb/b/a/a/f/a/f;
        //   247: astore          15
        //   249: aload           15
        //   251: ifnull          291
        //   254: iconst_0       
        //   255: istore_1       
        //   256: iload_1        
        //   257: aload           15
        //   259: invokevirtual   b/b/a/a/f/a/f.\u3007080:()I
        //   262: if_icmpge       291
        //   265: aload           21
        //   267: aload           15
        //   269: iload_1        
        //   270: invokevirtual   b/b/a/a/f/a/f.\u3007o00\u3007\u3007Oo:(I)Ljava/lang/String;
        //   273: aload           15
        //   275: iload_1        
        //   276: invokevirtual   b/b/a/a/f/a/f.\u3007o\u3007:(I)Ljava/lang/String;
        //   279: invokeinterface java/util/Map.put:(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
        //   284: pop            
        //   285: iinc            1, 1
        //   288: goto            256
        //   291: new             Lb/b/a/a/j/b;
        //   294: astore          20
        //   296: aload           20
        //   298: aload           16
        //   300: invokevirtual   b/b/a/a/f/a/n.\u3007O8o08O:()Z
        //   303: aload           16
        //   305: invokevirtual   b/b/a/a/f/a/n.oO80:()I
        //   308: aload           16
        //   310: invokevirtual   b/b/a/a/f/a/n.OO0o\u3007\u3007:()Ljava/lang/String;
        //   313: aload           21
        //   315: aconst_null    
        //   316: aload           16
        //   318: invokevirtual   b/b/a/a/f/a/n.\u3007O00:()J
        //   321: aload           16
        //   323: invokevirtual   b/b/a/a/f/a/n.\u3007O\u3007:()J
        //   326: invokespecial   b/b/a/a/j/b.<init>:(ZILjava/lang/String;Ljava/util/Map;Ljava/lang/String;JJ)V
        //   329: aload           16
        //   331: invokevirtual   b/b/a/a/f/a/n.Oo08:()Lb/b/a/a/f/a/o;
        //   334: invokevirtual   b/b/a/a/f/a/o.oO80:()J
        //   337: lstore          9
        //   339: lload           9
        //   341: lstore          7
        //   343: lload           9
        //   345: lconst_0       
        //   346: lcmp           
        //   347: ifgt            357
        //   350: aload           21
        //   352: invokestatic    b/b/a/a/j/d/a.\u3007O\u3007:(Ljava/util/Map;)J
        //   355: lstore          7
        //   357: aload_0        
        //   358: getfield        b/b/a/a/j/d/a.o\u30070:Ljava/io/File;
        //   361: invokevirtual   java/io/File.length:()J
        //   364: lstore          11
        //   366: aload           21
        //   368: invokestatic    b/b/a/a/j/d/a.\u3007\u30078O0\u30078:(Ljava/util/Map;)Z
        //   371: istore          14
        //   373: ldc             "DownloadExecutor"
        //   375: astore          19
        //   377: lload           7
        //   379: lstore          9
        //   381: iload           14
        //   383: ifeq            565
        //   386: lload           7
        //   388: lload           11
        //   390: ladd           
        //   391: lstore          7
        //   393: aload           21
        //   395: ldc             "Content-Range"
        //   397: invokeinterface java/util/Map.get:(Ljava/lang/Object;)Ljava/lang/Object;
        //   402: checkcast       Ljava/lang/String;
        //   405: astore          15
        //   407: lload           7
        //   409: lstore          9
        //   411: aload           15
        //   413: invokestatic    android/text/TextUtils.isEmpty:(Ljava/lang/CharSequence;)Z
        //   416: ifne            565
        //   419: new             Ljava/lang/StringBuilder;
        //   422: astore          17
        //   424: aload           17
        //   426: invokespecial   java/lang/StringBuilder.<init>:()V
        //   429: aload           17
        //   431: ldc             "bytes "
        //   433: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   436: pop            
        //   437: aload           17
        //   439: lload           11
        //   441: invokevirtual   java/lang/StringBuilder.append:(J)Ljava/lang/StringBuilder;
        //   444: pop            
        //   445: aload           17
        //   447: ldc             "-"
        //   449: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   452: pop            
        //   453: aload           17
        //   455: lload           7
        //   457: lconst_1       
        //   458: lsub           
        //   459: invokevirtual   java/lang/StringBuilder.append:(J)Ljava/lang/StringBuilder;
        //   462: pop            
        //   463: aload           17
        //   465: invokevirtual   java/lang/StringBuilder.toString:()Ljava/lang/String;
        //   468: astore          17
        //   470: aload           15
        //   472: aload           17
        //   474: invokestatic    android/text/TextUtils.indexOf:(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)I
        //   477: iconst_m1      
        //   478: if_icmpne       569
        //   481: new             Ljava/lang/StringBuilder;
        //   484: astore          16
        //   486: aload           16
        //   488: invokespecial   java/lang/StringBuilder.<init>:()V
        //   491: aload           16
        //   493: ldc             "execute: The Content-Range Header is invalid Assume["
        //   495: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   498: pop            
        //   499: aload           16
        //   501: aload           17
        //   503: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   506: pop            
        //   507: aload           16
        //   509: ldc             "] vs Real["
        //   511: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   514: pop            
        //   515: aload           16
        //   517: aload           15
        //   519: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   522: pop            
        //   523: aload           16
        //   525: ldc             "], please remove the temporary file ["
        //   527: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   530: pop            
        //   531: aload           16
        //   533: aload_0        
        //   534: getfield        b/b/a/a/j/d/a.o\u30070:Ljava/io/File;
        //   537: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/Object;)Ljava/lang/StringBuilder;
        //   540: pop            
        //   541: aload           16
        //   543: ldc             "]."
        //   545: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   548: pop            
        //   549: ldc             "DownloadExecutor"
        //   551: aload           16
        //   553: invokevirtual   java/lang/StringBuilder.toString:()Ljava/lang/String;
        //   556: invokestatic    b/b/a/a/j/f/d.\u3007o00\u3007\u3007Oo:(Ljava/lang/String;Ljava/lang/String;)V
        //   559: aload_0        
        //   560: invokespecial   b/b/a/a/j/d/a.\u30078o8o\u3007:()V
        //   563: aconst_null    
        //   564: areturn        
        //   565: lload           9
        //   567: lstore          7
        //   569: lload           7
        //   571: lconst_0       
        //   572: lcmp           
        //   573: ifle            635
        //   576: aload_0        
        //   577: getfield        b/b/a/a/j/d/a.o\u30070:Ljava/io/File;
        //   580: invokevirtual   java/io/File.exists:()Z
        //   583: ifeq            635
        //   586: aload_0        
        //   587: getfield        b/b/a/a/j/d/a.o\u30070:Ljava/io/File;
        //   590: invokevirtual   java/io/File.length:()J
        //   593: lload           7
        //   595: lcmp           
        //   596: ifne            635
        //   599: aload_0        
        //   600: getfield        b/b/a/a/j/d/a.o\u30070:Ljava/io/File;
        //   603: aload_0        
        //   604: getfield        b/b/a/a/j/d/a.Oo08:Ljava/io/File;
        //   607: invokevirtual   java/io/File.renameTo:(Ljava/io/File;)Z
        //   610: ifeq            625
        //   613: aload           20
        //   615: aload_0        
        //   616: getfield        b/b/a/a/j/d/a.Oo08:Ljava/io/File;
        //   619: invokevirtual   b/b/a/a/j/b.\u3007o00\u3007\u3007Oo:(Ljava/io/File;)V
        //   622: aload           20
        //   624: areturn        
        //   625: ldc             "DownloadExecutor"
        //   627: ldc_w           "Rename fail"
        //   630: invokestatic    b/b/a/a/j/f/d.\u3007o00\u3007\u3007Oo:(Ljava/lang/String;Ljava/lang/String;)V
        //   633: aconst_null    
        //   634: areturn        
        //   635: new             Ljava/io/RandomAccessFile;
        //   638: astore          15
        //   640: aload           15
        //   642: aload_0        
        //   643: getfield        b/b/a/a/j/d/a.o\u30070:Ljava/io/File;
        //   646: ldc_w           "rw"
        //   649: invokespecial   java/io/RandomAccessFile.<init>:(Ljava/io/File;Ljava/lang/String;)V
        //   652: iload           14
        //   654: ifeq            675
        //   657: aload           15
        //   659: lload           5
        //   661: invokevirtual   java/io/RandomAccessFile.seek:(J)V
        //   664: lload           5
        //   666: lstore          9
        //   668: aload           15
        //   670: astore          18
        //   672: goto            696
        //   675: aload           15
        //   677: lconst_0       
        //   678: invokevirtual   java/io/RandomAccessFile.setLength:(J)V
        //   681: goto            689
        //   684: astore          15
        //   686: aconst_null    
        //   687: astore          15
        //   689: lconst_0       
        //   690: lstore          9
        //   692: aload           15
        //   694: astore          18
        //   696: aload           16
        //   698: invokevirtual   b/b/a/a/f/a/n.Oo08:()Lb/b/a/a/f/a/o;
        //   701: invokevirtual   b/b/a/a/f/a/o.Oo08:()Ljava/io/InputStream;
        //   704: astore          17
        //   706: aload           17
        //   708: astore          16
        //   710: aload           17
        //   712: astore          15
        //   714: aload           21
        //   716: invokestatic    b/b/a/a/j/d/a.\u3007O00:(Ljava/util/Map;)Z
        //   719: ifeq            758
        //   722: aload           17
        //   724: astore          16
        //   726: aload           17
        //   728: astore          15
        //   730: aload           17
        //   732: instanceof      Ljava/util/zip/GZIPInputStream;
        //   735: ifne            758
        //   738: aload           17
        //   740: astore          15
        //   742: new             Ljava/util/zip/GZIPInputStream;
        //   745: astore          16
        //   747: aload           17
        //   749: astore          15
        //   751: aload           16
        //   753: aload           17
        //   755: invokespecial   java/util/zip/GZIPInputStream.<init>:(Ljava/io/InputStream;)V
        //   758: aload           16
        //   760: astore          15
        //   762: sipush          16384
        //   765: newarray        B
        //   767: astore          21
        //   769: iconst_0       
        //   770: istore_1       
        //   771: lconst_0       
        //   772: lstore          11
        //   774: aload           19
        //   776: astore          17
        //   778: aload           16
        //   780: astore          15
        //   782: aload           16
        //   784: aload           21
        //   786: iload_1        
        //   787: sipush          16384
        //   790: iload_1        
        //   791: isub           
        //   792: invokevirtual   java/io/InputStream.read:([BII)I
        //   795: istore          4
        //   797: iconst_1       
        //   798: istore_2       
        //   799: iload           4
        //   801: iconst_m1      
        //   802: if_icmpeq       894
        //   805: iload_1        
        //   806: iload           4
        //   808: iadd           
        //   809: istore_3       
        //   810: lload           11
        //   812: iload           4
        //   814: i2l            
        //   815: ladd           
        //   816: lstore          11
        //   818: iload_2        
        //   819: istore_1       
        //   820: lload           11
        //   822: ldc2_w          16384
        //   825: lrem           
        //   826: lconst_0       
        //   827: lcmp           
        //   828: ifeq            849
        //   831: lload           11
        //   833: lload           7
        //   835: lload           5
        //   837: lsub           
        //   838: lcmp           
        //   839: ifne            847
        //   842: iload_2        
        //   843: istore_1       
        //   844: goto            849
        //   847: iconst_0       
        //   848: istore_1       
        //   849: iload_1        
        //   850: ifeq            889
        //   853: aload           16
        //   855: astore          15
        //   857: aload           18
        //   859: lload           9
        //   861: invokevirtual   java/io/RandomAccessFile.seek:(J)V
        //   864: aload           16
        //   866: astore          15
        //   868: aload           18
        //   870: aload           21
        //   872: iconst_0       
        //   873: iload_3        
        //   874: invokevirtual   java/io/RandomAccessFile.write:([BII)V
        //   877: lload           9
        //   879: iload_3        
        //   880: i2l            
        //   881: ladd           
        //   882: lstore          9
        //   884: iconst_0       
        //   885: istore_1       
        //   886: goto            778
        //   889: iload_3        
        //   890: istore_1       
        //   891: goto            778
        //   894: iconst_0       
        //   895: istore          13
        //   897: iload           4
        //   899: ifeq            926
        //   902: aload           16
        //   904: astore          15
        //   906: aload           18
        //   908: lload           9
        //   910: invokevirtual   java/io/RandomAccessFile.seek:(J)V
        //   913: aload           16
        //   915: astore          15
        //   917: aload           18
        //   919: aload           21
        //   921: iconst_0       
        //   922: iload_1        
        //   923: invokevirtual   java/io/RandomAccessFile.write:([BII)V
        //   926: iload           14
        //   928: ifeq            938
        //   931: lload           5
        //   933: lconst_0       
        //   934: lcmp           
        //   935: ifne            951
        //   938: aload           16
        //   940: astore          15
        //   942: aload_0        
        //   943: getfield        b/b/a/a/j/d/a.o\u30070:Ljava/io/File;
        //   946: invokevirtual   java/io/File.length:()J
        //   949: lstore          7
        //   951: lload           7
        //   953: lconst_0       
        //   954: lcmp           
        //   955: ifle            1092
        //   958: aload           16
        //   960: astore          15
        //   962: aload_0        
        //   963: getfield        b/b/a/a/j/d/a.o\u30070:Ljava/io/File;
        //   966: invokevirtual   java/io/File.exists:()Z
        //   969: ifeq            1092
        //   972: aload           16
        //   974: astore          15
        //   976: aload_0        
        //   977: getfield        b/b/a/a/j/d/a.o\u30070:Ljava/io/File;
        //   980: invokevirtual   java/io/File.length:()J
        //   983: lload           7
        //   985: lcmp           
        //   986: ifne            1092
        //   989: aload           16
        //   991: astore          15
        //   993: aload_0        
        //   994: getfield        b/b/a/a/j/d/a.o\u30070:Ljava/io/File;
        //   997: aload_0        
        //   998: getfield        b/b/a/a/j/d/a.Oo08:Ljava/io/File;
        //  1001: invokevirtual   java/io/File.renameTo:(Ljava/io/File;)Z
        //  1004: ifeq            1055
        //  1007: aload           16
        //  1009: astore          15
        //  1011: aload           20
        //  1013: aload_0        
        //  1014: getfield        b/b/a/a/j/d/a.Oo08:Ljava/io/File;
        //  1017: invokevirtual   b/b/a/a/j/b.\u3007o00\u3007\u3007Oo:(Ljava/io/File;)V
        //  1020: aload           16
        //  1022: invokevirtual   java/io/InputStream.close:()V
        //  1025: goto            1036
        //  1028: astore          15
        //  1030: ldc_w           "Error occured when calling InputStream.close"
        //  1033: invokestatic    b/b/a/a/j/f/d.\u3007080:(Ljava/lang/String;)V
        //  1036: aload           18
        //  1038: invokevirtual   java/io/RandomAccessFile.close:()V
        //  1041: goto            1052
        //  1044: astore          15
        //  1046: ldc_w           "Error occured when calling tmpFile.close"
        //  1049: invokestatic    b/b/a/a/j/f/d.\u3007080:(Ljava/lang/String;)V
        //  1052: aload           20
        //  1054: areturn        
        //  1055: aload           16
        //  1057: invokevirtual   java/io/InputStream.close:()V
        //  1060: goto            1071
        //  1063: astore          15
        //  1065: ldc_w           "Error occured when calling InputStream.close"
        //  1068: invokestatic    b/b/a/a/j/f/d.\u3007080:(Ljava/lang/String;)V
        //  1071: aload           18
        //  1073: invokevirtual   java/io/RandomAccessFile.close:()V
        //  1076: goto            1090
        //  1079: astore          15
        //  1081: ldc_w           "Error occured when calling tmpFile.close"
        //  1084: invokestatic    b/b/a/a/j/f/d.\u3007080:(Ljava/lang/String;)V
        //  1087: goto            1076
        //  1090: aconst_null    
        //  1091: areturn        
        //  1092: aload           16
        //  1094: astore          15
        //  1096: new             Ljava/lang/StringBuilder;
        //  1099: astore          19
        //  1101: aload           16
        //  1103: astore          15
        //  1105: aload           19
        //  1107: invokespecial   java/lang/StringBuilder.<init>:()V
        //  1110: aload           16
        //  1112: astore          15
        //  1114: aload           19
        //  1116: ldc_w           " tempFile.length() == fileSize is"
        //  1119: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //  1122: pop            
        //  1123: aload           16
        //  1125: astore          15
        //  1127: aload_0        
        //  1128: getfield        b/b/a/a/j/d/a.o\u30070:Ljava/io/File;
        //  1131: invokevirtual   java/io/File.length:()J
        //  1134: lload           7
        //  1136: lcmp           
        //  1137: ifne            1143
        //  1140: iconst_1       
        //  1141: istore          13
        //  1143: aload           16
        //  1145: astore          15
        //  1147: aload           19
        //  1149: iload           13
        //  1151: invokevirtual   java/lang/StringBuilder.append:(Z)Ljava/lang/StringBuilder;
        //  1154: pop            
        //  1155: aload           16
        //  1157: astore          15
        //  1159: aload           17
        //  1161: aload           19
        //  1163: invokevirtual   java/lang/StringBuilder.toString:()Ljava/lang/String;
        //  1166: invokestatic    b/b/a/a/j/f/d.\u3007o00\u3007\u3007Oo:(Ljava/lang/String;Ljava/lang/String;)V
        //  1169: aload           16
        //  1171: invokevirtual   java/io/InputStream.close:()V
        //  1174: goto            1185
        //  1177: astore          15
        //  1179: ldc_w           "Error occured when calling InputStream.close"
        //  1182: invokestatic    b/b/a/a/j/f/d.\u3007080:(Ljava/lang/String;)V
        //  1185: aload           18
        //  1187: invokevirtual   java/io/RandomAccessFile.close:()V
        //  1190: goto            1204
        //  1193: astore          15
        //  1195: ldc_w           "Error occured when calling tmpFile.close"
        //  1198: invokestatic    b/b/a/a/j/f/d.\u3007080:(Ljava/lang/String;)V
        //  1201: goto            1190
        //  1204: aconst_null    
        //  1205: areturn        
        //  1206: astore          16
        //  1208: goto            1216
        //  1211: astore          16
        //  1213: aconst_null    
        //  1214: astore          15
        //  1216: ldc_w           "Error occured when FileRequest.parseHttpResponse"
        //  1219: invokestatic    b/b/a/a/j/f/d.\u3007080:(Ljava/lang/String;)V
        //  1222: aload           16
        //  1224: invokevirtual   java/lang/Throwable.printStackTrace:()V
        //  1227: iload           14
        //  1229: ifne            1236
        //  1232: aload_0        
        //  1233: invokespecial   b/b/a/a/j/d/a.\u30078o8o\u3007:()V
        //  1236: aload           15
        //  1238: ifnull          1257
        //  1241: aload           15
        //  1243: invokevirtual   java/io/InputStream.close:()V
        //  1246: goto            1257
        //  1249: astore          15
        //  1251: ldc_w           "Error occured when calling InputStream.close"
        //  1254: invokestatic    b/b/a/a/j/f/d.\u3007080:(Ljava/lang/String;)V
        //  1257: aload           18
        //  1259: invokevirtual   java/io/RandomAccessFile.close:()V
        //  1262: goto            1276
        //  1265: astore          15
        //  1267: ldc_w           "Error occured when calling tmpFile.close"
        //  1270: invokestatic    b/b/a/a/j/f/d.\u3007080:(Ljava/lang/String;)V
        //  1273: goto            1262
        //  1276: aconst_null    
        //  1277: areturn        
        //  1278: astore          16
        //  1280: aload           15
        //  1282: ifnull          1301
        //  1285: aload           15
        //  1287: invokevirtual   java/io/InputStream.close:()V
        //  1290: goto            1301
        //  1293: astore          15
        //  1295: ldc_w           "Error occured when calling InputStream.close"
        //  1298: invokestatic    b/b/a/a/j/f/d.\u3007080:(Ljava/lang/String;)V
        //  1301: aload           18
        //  1303: invokevirtual   java/io/RandomAccessFile.close:()V
        //  1306: goto            1317
        //  1309: astore          15
        //  1311: ldc_w           "Error occured when calling tmpFile.close"
        //  1314: invokestatic    b/b/a/a/j/f/d.\u3007080:(Ljava/lang/String;)V
        //  1317: aload           16
        //  1319: athrow         
        //  1320: goto            1334
        //  1323: astore          15
        //  1325: aload           15
        //  1327: invokevirtual   java/lang/Throwable.printStackTrace:()V
        //  1330: aload_0        
        //  1331: invokespecial   b/b/a/a/j/d/a.\u30078o8o\u3007:()V
        //  1334: aconst_null    
        //  1335: areturn        
        //  1336: astore          15
        //  1338: aconst_null    
        //  1339: areturn        
        //  1340: aconst_null    
        //  1341: areturn        
        //  1342: astore          17
        //  1344: goto            689
        //    Exceptions:
        //  Try           Handler
        //  Start  End    Start  End    Type                                
        //  -----  -----  -----  -----  ------------------------------------
        //  177    187    1336   1340   Ljava/lang/IllegalArgumentException;
        //  203    219    1323   1334   Ljava/io/IOException;
        //  224    249    1323   1334   Ljava/io/IOException;
        //  256    285    1323   1334   Ljava/io/IOException;
        //  291    339    1323   1334   Ljava/io/IOException;
        //  350    357    1323   1334   Ljava/io/IOException;
        //  357    373    1323   1334   Ljava/io/IOException;
        //  393    407    1323   1334   Ljava/io/IOException;
        //  411    470    1323   1334   Ljava/io/IOException;
        //  470    563    1323   1334   Ljava/io/IOException;
        //  576    622    1323   1334   Ljava/io/IOException;
        //  625    633    1323   1334   Ljava/io/IOException;
        //  635    652    684    689    Any
        //  657    664    1342   1347   Any
        //  675    681    1342   1347   Any
        //  696    706    1211   1216   Any
        //  714    722    1206   1211   Any
        //  730    738    1206   1211   Any
        //  742    747    1206   1211   Any
        //  751    758    1206   1211   Any
        //  762    769    1206   1211   Any
        //  782    797    1206   1211   Any
        //  857    864    1206   1211   Any
        //  868    877    1206   1211   Any
        //  906    913    1206   1211   Any
        //  917    926    1206   1211   Any
        //  942    951    1206   1211   Any
        //  962    972    1206   1211   Any
        //  976    989    1206   1211   Any
        //  993    1007   1206   1211   Any
        //  1011   1020   1206   1211   Any
        //  1020   1025   1028   1036   Any
        //  1030   1036   1323   1334   Ljava/io/IOException;
        //  1036   1041   1044   1052   Any
        //  1046   1052   1323   1334   Ljava/io/IOException;
        //  1055   1060   1063   1071   Any
        //  1065   1071   1323   1334   Ljava/io/IOException;
        //  1071   1076   1079   1090   Any
        //  1081   1087   1323   1334   Ljava/io/IOException;
        //  1096   1101   1206   1211   Any
        //  1105   1110   1206   1211   Any
        //  1114   1123   1206   1211   Any
        //  1127   1140   1206   1211   Any
        //  1147   1155   1206   1211   Any
        //  1159   1169   1206   1211   Any
        //  1169   1174   1177   1185   Any
        //  1179   1185   1323   1334   Ljava/io/IOException;
        //  1185   1190   1193   1204   Any
        //  1195   1201   1323   1334   Ljava/io/IOException;
        //  1216   1227   1278   1320   Any
        //  1232   1236   1278   1320   Any
        //  1241   1246   1249   1257   Any
        //  1251   1257   1323   1334   Ljava/io/IOException;
        //  1257   1262   1265   1276   Any
        //  1267   1273   1323   1334   Ljava/io/IOException;
        //  1285   1290   1293   1301   Any
        //  1295   1301   1323   1334   Ljava/io/IOException;
        //  1301   1306   1309   1317   Any
        //  1311   1317   1323   1334   Ljava/io/IOException;
        //  1317   1320   1323   1334   Ljava/io/IOException;
        // 
        // The error that occurred was:
        // 
        // java.lang.IllegalStateException: Expression is linked from several locations: Label_0675:
        //     at com.strobel.decompiler.ast.Error.expressionLinkedFromMultipleLocations(Error.java:27)
        //     at com.strobel.decompiler.ast.AstOptimizer.mergeDisparateObjectInitializations(AstOptimizer.java:2604)
        //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:235)
        //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:42)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:206)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:93)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethodBody(AstBuilder.java:868)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethod(AstBuilder.java:761)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addTypeMembers(AstBuilder.java:638)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeCore(AstBuilder.java:605)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeNoCache(AstBuilder.java:195)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createType(AstBuilder.java:162)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addType(AstBuilder.java:137)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.buildAst(JavaLanguage.java:71)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.decompileType(JavaLanguage.java:59)
        //     at com.strobel.decompiler.DecompilerDriver.decompileType(DecompilerDriver.java:333)
        //     at com.strobel.decompiler.DecompilerDriver.decompileJar(DecompilerDriver.java:254)
        //     at com.strobel.decompiler.DecompilerDriver.main(DecompilerDriver.java:144)
        // 
        throw new IllegalStateException("An error occurred while decompiling this method.");
    }
    
    public void oO80(final b.b.a.a.j.c.a a) {
        final File oo08 = this.Oo08;
        if (oo08 != null) {
            if (this.o\u30070 != null) {
                final boolean exists = oo08.exists();
                long lng = 0L;
                if (exists && this.Oo08.length() != 0L && a != null) {
                    final long currentTimeMillis = System.currentTimeMillis();
                    final b b = new b(true, 200, "Success", null, null, currentTimeMillis, currentTimeMillis);
                    b.\u3007o00\u3007\u3007Oo(this.Oo08);
                    a.a(this, b);
                    return;
                }
                final long length = this.o\u30070.length();
                if (length >= 0L) {
                    lng = length;
                }
                final l.a a2 = new l.a();
                a2.O8(this.\u3007080());
                final StringBuilder sb = new StringBuilder();
                sb.append("bytes=");
                sb.append(lng);
                sb.append("-");
                this.O8("Range", sb.toString());
                if (TextUtils.isEmpty((CharSequence)super.O8)) {
                    a.a(this, new IOException("Url is Empty"));
                    return;
                }
                try {
                    a2.Oo08(super.O8);
                    this.\u3007o00\u3007\u3007Oo(a2);
                    super.\u3007080.\u3007080(a2.\u300780\u3007808\u3007O().oO80()).\u3007O00(new b.b.a.a.f.a.c(this, a, lng) {
                        final b.b.a.a.j.c.a \u3007080;
                        final long \u3007o00\u3007\u3007Oo;
                        final a \u3007o\u3007;
                        
                        @Override
                        public void \u3007080(final b.b.a.a.f.a.b p0, final n p1) throws IOException {
                            // 
                            // This method could not be decompiled.
                            // 
                            // Original Bytecode:
                            // 
                            //     2: astore          17
                            //     4: ldc             "Error occured when calling tmpFile.close"
                            //     6: astore          22
                            //     8: aload_0        
                            //     9: getfield        b/b/a/a/j/d/a$a.\u3007080:Lb/b/a/a/j/c/a;
                            //    12: ifnull          1245
                            //    15: new             Ljava/util/HashMap;
                            //    18: dup            
                            //    19: invokespecial   java/util/HashMap.<init>:()V
                            //    22: astore          24
                            //    24: aload_2        
                            //    25: ifnull          1245
                            //    28: aload_2        
                            //    29: invokevirtual   b/b/a/a/f/a/n.\u30078o8o\u3007:()Lb/b/a/a/f/a/f;
                            //    32: astore_1       
                            //    33: aload_1        
                            //    34: ifnull          71
                            //    37: iconst_0       
                            //    38: istore_3       
                            //    39: iload_3        
                            //    40: aload_1        
                            //    41: invokevirtual   b/b/a/a/f/a/f.\u3007080:()I
                            //    44: if_icmpge       71
                            //    47: aload           24
                            //    49: aload_1        
                            //    50: iload_3        
                            //    51: invokevirtual   b/b/a/a/f/a/f.\u3007o00\u3007\u3007Oo:(I)Ljava/lang/String;
                            //    54: aload_1        
                            //    55: iload_3        
                            //    56: invokevirtual   b/b/a/a/f/a/f.\u3007o\u3007:(I)Ljava/lang/String;
                            //    59: invokeinterface java/util/Map.put:(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
                            //    64: pop            
                            //    65: iinc            3, 1
                            //    68: goto            39
                            //    71: new             Lb/b/a/a/j/b;
                            //    74: dup            
                            //    75: aload_2        
                            //    76: invokevirtual   b/b/a/a/f/a/n.\u3007O8o08O:()Z
                            //    79: aload_2        
                            //    80: invokevirtual   b/b/a/a/f/a/n.oO80:()I
                            //    83: aload_2        
                            //    84: invokevirtual   b/b/a/a/f/a/n.OO0o\u3007\u3007:()Ljava/lang/String;
                            //    87: aload           24
                            //    89: aconst_null    
                            //    90: aload_2        
                            //    91: invokevirtual   b/b/a/a/f/a/n.\u3007O00:()J
                            //    94: aload_2        
                            //    95: invokevirtual   b/b/a/a/f/a/n.\u3007O\u3007:()J
                            //    98: invokespecial   b/b/a/a/j/b.<init>:(ZILjava/lang/String;Ljava/util/Map;Ljava/lang/String;JJ)V
                            //   101: astore          23
                            //   103: aload_2        
                            //   104: invokevirtual   b/b/a/a/f/a/n.\u3007O8o08O:()Z
                            //   107: ifeq            1232
                            //   110: aload_2        
                            //   111: invokevirtual   b/b/a/a/f/a/n.Oo08:()Lb/b/a/a/f/a/o;
                            //   114: invokevirtual   b/b/a/a/f/a/o.oO80:()J
                            //   117: lstore          9
                            //   119: lload           9
                            //   121: lstore          7
                            //   123: lload           9
                            //   125: lconst_0       
                            //   126: lcmp           
                            //   127: ifgt            137
                            //   130: aload           24
                            //   132: invokestatic    b/b/a/a/j/d/a.OO0o\u3007\u3007\u3007\u30070:(Ljava/util/Map;)J
                            //   135: lstore          7
                            //   137: aload           24
                            //   139: invokestatic    b/b/a/a/j/d/a.Oooo8o0\u3007:(Ljava/util/Map;)Z
                            //   142: istore          16
                            //   144: lload           7
                            //   146: lstore          9
                            //   148: iload           16
                            //   150: ifeq            358
                            //   153: lload           7
                            //   155: aload_0        
                            //   156: getfield        b/b/a/a/j/d/a$a.\u3007o00\u3007\u3007Oo:J
                            //   159: ladd           
                            //   160: lstore          7
                            //   162: aload           24
                            //   164: ldc             "Content-Range"
                            //   166: invokeinterface java/util/Map.get:(Ljava/lang/Object;)Ljava/lang/Object;
                            //   171: checkcast       Ljava/lang/String;
                            //   174: astore_1       
                            //   175: lload           7
                            //   177: lstore          9
                            //   179: aload_1        
                            //   180: invokestatic    android/text/TextUtils.isEmpty:(Ljava/lang/CharSequence;)Z
                            //   183: ifne            358
                            //   186: new             Ljava/lang/StringBuilder;
                            //   189: dup            
                            //   190: invokespecial   java/lang/StringBuilder.<init>:()V
                            //   193: astore          18
                            //   195: aload           18
                            //   197: ldc             "bytes "
                            //   199: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
                            //   202: pop            
                            //   203: aload           18
                            //   205: aload_0        
                            //   206: getfield        b/b/a/a/j/d/a$a.\u3007o00\u3007\u3007Oo:J
                            //   209: invokevirtual   java/lang/StringBuilder.append:(J)Ljava/lang/StringBuilder;
                            //   212: pop            
                            //   213: aload           18
                            //   215: ldc             "-"
                            //   217: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
                            //   220: pop            
                            //   221: aload           18
                            //   223: lload           7
                            //   225: lconst_1       
                            //   226: lsub           
                            //   227: invokevirtual   java/lang/StringBuilder.append:(J)Ljava/lang/StringBuilder;
                            //   230: pop            
                            //   231: aload           18
                            //   233: invokevirtual   java/lang/StringBuilder.toString:()Ljava/lang/String;
                            //   236: astore          18
                            //   238: lload           7
                            //   240: lstore          9
                            //   242: aload_1        
                            //   243: aload           18
                            //   245: invokestatic    android/text/TextUtils.indexOf:(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)I
                            //   248: iconst_m1      
                            //   249: if_icmpne       358
                            //   252: aload_0        
                            //   253: getfield        b/b/a/a/j/d/a$a.\u3007o\u3007:Lb/b/a/a/j/d/a;
                            //   256: invokestatic    b/b/a/a/j/d/a.\u300780\u3007808\u3007O:(Lb/b/a/a/j/d/a;)V
                            //   259: aload_0        
                            //   260: getfield        b/b/a/a/j/d/a$a.\u3007080:Lb/b/a/a/j/c/a;
                            //   263: astore          19
                            //   265: aload_0        
                            //   266: getfield        b/b/a/a/j/d/a$a.\u3007o\u3007:Lb/b/a/a/j/d/a;
                            //   269: astore_2       
                            //   270: new             Ljava/lang/StringBuilder;
                            //   273: dup            
                            //   274: invokespecial   java/lang/StringBuilder.<init>:()V
                            //   277: astore          17
                            //   279: aload           17
                            //   281: ldc             "The Content-Range Header is invalid Assume["
                            //   283: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
                            //   286: pop            
                            //   287: aload           17
                            //   289: aload           18
                            //   291: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
                            //   294: pop            
                            //   295: aload           17
                            //   297: ldc             "] vs Real["
                            //   299: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
                            //   302: pop            
                            //   303: aload           17
                            //   305: aload_1        
                            //   306: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
                            //   309: pop            
                            //   310: aload           17
                            //   312: ldc             "], please remove the temporary file ["
                            //   314: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
                            //   317: pop            
                            //   318: aload           17
                            //   320: aload_0        
                            //   321: getfield        b/b/a/a/j/d/a$a.\u3007o\u3007:Lb/b/a/a/j/d/a;
                            //   324: getfield        b/b/a/a/j/d/a.o\u30070:Ljava/io/File;
                            //   327: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/Object;)Ljava/lang/StringBuilder;
                            //   330: pop            
                            //   331: aload           17
                            //   333: ldc             "]."
                            //   335: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
                            //   338: pop            
                            //   339: aload           19
                            //   341: aload_2        
                            //   342: new             Ljava/io/IOException;
                            //   345: dup            
                            //   346: aload           17
                            //   348: invokevirtual   java/lang/StringBuilder.toString:()Ljava/lang/String;
                            //   351: invokespecial   java/io/IOException.<init>:(Ljava/lang/String;)V
                            //   354: invokevirtual   b/b/a/a/j/c/a.a:(Lb/b/a/a/j/d/c;Ljava/io/IOException;)V
                            //   357: return         
                            //   358: lload           9
                            //   360: lstore          7
                            //   362: lload           7
                            //   364: lconst_0       
                            //   365: lcmp           
                            //   366: ifle            481
                            //   369: aload_0        
                            //   370: getfield        b/b/a/a/j/d/a$a.\u3007o\u3007:Lb/b/a/a/j/d/a;
                            //   373: getfield        b/b/a/a/j/d/a.o\u30070:Ljava/io/File;
                            //   376: invokevirtual   java/io/File.exists:()Z
                            //   379: ifeq            481
                            //   382: aload_0        
                            //   383: getfield        b/b/a/a/j/d/a$a.\u3007o\u3007:Lb/b/a/a/j/d/a;
                            //   386: getfield        b/b/a/a/j/d/a.o\u30070:Ljava/io/File;
                            //   389: invokevirtual   java/io/File.length:()J
                            //   392: lload           7
                            //   394: lcmp           
                            //   395: ifne            481
                            //   398: aload_0        
                            //   399: getfield        b/b/a/a/j/d/a$a.\u3007o\u3007:Lb/b/a/a/j/d/a;
                            //   402: astore_1       
                            //   403: aload_1        
                            //   404: getfield        b/b/a/a/j/d/a.o\u30070:Ljava/io/File;
                            //   407: aload_1        
                            //   408: getfield        b/b/a/a/j/d/a.Oo08:Ljava/io/File;
                            //   411: invokevirtual   java/io/File.renameTo:(Ljava/io/File;)Z
                            //   414: ifeq            460
                            //   417: aload_0        
                            //   418: getfield        b/b/a/a/j/d/a$a.\u3007o\u3007:Lb/b/a/a/j/d/a;
                            //   421: lload           7
                            //   423: lload           7
                            //   425: aload_0        
                            //   426: getfield        b/b/a/a/j/d/a$a.\u3007080:Lb/b/a/a/j/c/a;
                            //   429: invokevirtual   b/b/a/a/j/d/a.\u3007\u3007888:(JJLb/b/a/a/j/c/a;)V
                            //   432: aload           23
                            //   434: aload_0        
                            //   435: getfield        b/b/a/a/j/d/a$a.\u3007o\u3007:Lb/b/a/a/j/d/a;
                            //   438: getfield        b/b/a/a/j/d/a.Oo08:Ljava/io/File;
                            //   441: invokevirtual   b/b/a/a/j/b.\u3007o00\u3007\u3007Oo:(Ljava/io/File;)V
                            //   444: aload_0        
                            //   445: getfield        b/b/a/a/j/d/a$a.\u3007080:Lb/b/a/a/j/c/a;
                            //   448: aload_0        
                            //   449: getfield        b/b/a/a/j/d/a$a.\u3007o\u3007:Lb/b/a/a/j/d/a;
                            //   452: aload           23
                            //   454: invokevirtual   b/b/a/a/j/c/a.a:(Lb/b/a/a/j/d/c;Lb/b/a/a/j/b;)V
                            //   457: goto            480
                            //   460: aload_0        
                            //   461: getfield        b/b/a/a/j/d/a$a.\u3007080:Lb/b/a/a/j/c/a;
                            //   464: aload_0        
                            //   465: getfield        b/b/a/a/j/d/a$a.\u3007o\u3007:Lb/b/a/a/j/d/a;
                            //   468: new             Ljava/io/IOException;
                            //   471: dup            
                            //   472: ldc             "Rename fail"
                            //   474: invokespecial   java/io/IOException.<init>:(Ljava/lang/String;)V
                            //   477: invokevirtual   b/b/a/a/j/c/a.a:(Lb/b/a/a/j/d/c;Ljava/io/IOException;)V
                            //   480: return         
                            //   481: new             Ljava/io/RandomAccessFile;
                            //   484: astore_1       
                            //   485: aload_1        
                            //   486: aload_0        
                            //   487: getfield        b/b/a/a/j/d/a$a.\u3007o\u3007:Lb/b/a/a/j/d/a;
                            //   490: getfield        b/b/a/a/j/d/a.o\u30070:Ljava/io/File;
                            //   493: ldc             "rw"
                            //   495: invokespecial   java/io/RandomAccessFile.<init>:(Ljava/io/File;Ljava/lang/String;)V
                            //   498: iload           16
                            //   500: ifeq            523
                            //   503: aload_1        
                            //   504: aload_0        
                            //   505: getfield        b/b/a/a/j/d/a$a.\u3007o00\u3007\u3007Oo:J
                            //   508: invokevirtual   java/io/RandomAccessFile.seek:(J)V
                            //   511: aload_0        
                            //   512: getfield        b/b/a/a/j/d/a$a.\u3007o00\u3007\u3007Oo:J
                            //   515: lstore          9
                            //   517: aload_1        
                            //   518: astore          21
                            //   520: goto            540
                            //   523: aload_1        
                            //   524: lconst_0       
                            //   525: invokevirtual   java/io/RandomAccessFile.setLength:(J)V
                            //   528: goto            534
                            //   531: astore_1       
                            //   532: aconst_null    
                            //   533: astore_1       
                            //   534: lconst_0       
                            //   535: lstore          9
                            //   537: aload_1        
                            //   538: astore          21
                            //   540: aload_2        
                            //   541: invokevirtual   b/b/a/a/f/a/n.Oo08:()Lb/b/a/a/f/a/o;
                            //   544: invokevirtual   b/b/a/a/f/a/o.Oo08:()Ljava/io/InputStream;
                            //   547: astore_2       
                            //   548: aload_2        
                            //   549: astore_1       
                            //   550: aload           17
                            //   552: astore          20
                            //   554: aload           22
                            //   556: astore          19
                            //   558: aload_2        
                            //   559: astore          18
                            //   561: aload           24
                            //   563: invokestatic    b/b/a/a/j/d/a.\u3007\u3007808\u3007:(Ljava/util/Map;)Z
                            //   566: ifeq            620
                            //   569: aload_2        
                            //   570: astore_1       
                            //   571: aload           17
                            //   573: astore          20
                            //   575: aload           22
                            //   577: astore          19
                            //   579: aload_2        
                            //   580: astore          18
                            //   582: aload_2        
                            //   583: instanceof      Ljava/util/zip/GZIPInputStream;
                            //   586: ifne            620
                            //   589: aload           17
                            //   591: astore          20
                            //   593: aload           22
                            //   595: astore          19
                            //   597: aload_2        
                            //   598: astore          18
                            //   600: new             Ljava/util/zip/GZIPInputStream;
                            //   603: astore_1       
                            //   604: aload           17
                            //   606: astore          20
                            //   608: aload           22
                            //   610: astore          19
                            //   612: aload_2        
                            //   613: astore          18
                            //   615: aload_1        
                            //   616: aload_2        
                            //   617: invokespecial   java/util/zip/GZIPInputStream.<init>:(Ljava/io/InputStream;)V
                            //   620: aload           17
                            //   622: astore          20
                            //   624: aload           22
                            //   626: astore          19
                            //   628: aload_1        
                            //   629: astore          18
                            //   631: sipush          16384
                            //   634: newarray        B
                            //   636: astore          24
                            //   638: lconst_0       
                            //   639: lstore          13
                            //   641: iconst_0       
                            //   642: istore_3       
                            //   643: aload           22
                            //   645: astore_2       
                            //   646: aload           17
                            //   648: astore          20
                            //   650: aload_2        
                            //   651: astore          19
                            //   653: aload_1        
                            //   654: astore          18
                            //   656: aload_1        
                            //   657: aload           24
                            //   659: iload_3        
                            //   660: sipush          16384
                            //   663: iload_3        
                            //   664: isub           
                            //   665: invokevirtual   java/io/InputStream.read:([BII)I
                            //   668: istore          6
                            //   670: iconst_1       
                            //   671: istore          4
                            //   673: iload           6
                            //   675: iconst_m1      
                            //   676: if_icmpeq       792
                            //   679: iload_3        
                            //   680: iload           6
                            //   682: iadd           
                            //   683: istore          5
                            //   685: lload           13
                            //   687: iload           6
                            //   689: i2l            
                            //   690: ladd           
                            //   691: lstore          13
                            //   693: lload           13
                            //   695: ldc2_w          16384
                            //   698: lrem           
                            //   699: lconst_0       
                            //   700: lcmp           
                            //   701: ifeq            726
                            //   704: lload           13
                            //   706: lload           7
                            //   708: aload_0        
                            //   709: getfield        b/b/a/a/j/d/a$a.\u3007o00\u3007\u3007Oo:J
                            //   712: lsub           
                            //   713: lcmp           
                            //   714: ifne            720
                            //   717: goto            726
                            //   720: iconst_0       
                            //   721: istore          4
                            //   723: goto            726
                            //   726: iload           5
                            //   728: istore_3       
                            //   729: lload           9
                            //   731: lstore          11
                            //   733: iload           4
                            //   735: ifeq            765
                            //   738: aload           21
                            //   740: lload           9
                            //   742: invokevirtual   java/io/RandomAccessFile.seek:(J)V
                            //   745: aload           21
                            //   747: aload           24
                            //   749: iconst_0       
                            //   750: iload           5
                            //   752: invokevirtual   java/io/RandomAccessFile.write:([BII)V
                            //   755: lload           9
                            //   757: iload           5
                            //   759: i2l            
                            //   760: ladd           
                            //   761: lstore          11
                            //   763: iconst_0       
                            //   764: istore_3       
                            //   765: aload_0        
                            //   766: getfield        b/b/a/a/j/d/a$a.\u3007o\u3007:Lb/b/a/a/j/d/a;
                            //   769: aload_0        
                            //   770: getfield        b/b/a/a/j/d/a$a.\u3007o00\u3007\u3007Oo:J
                            //   773: lload           13
                            //   775: ladd           
                            //   776: lload           7
                            //   778: aload_0        
                            //   779: getfield        b/b/a/a/j/d/a$a.\u3007080:Lb/b/a/a/j/c/a;
                            //   782: invokevirtual   b/b/a/a/j/d/a.\u3007\u3007888:(JJLb/b/a/a/j/c/a;)V
                            //   785: lload           11
                            //   787: lstore          9
                            //   789: goto            646
                            //   792: iload           16
                            //   794: ifne            829
                            //   797: aload_0        
                            //   798: getfield        b/b/a/a/j/d/a$a.\u3007o\u3007:Lb/b/a/a/j/d/a;
                            //   801: getfield        b/b/a/a/j/d/a.o\u30070:Ljava/io/File;
                            //   804: invokevirtual   java/io/File.length:()J
                            //   807: lstore          7
                            //   809: goto            829
                            //   812: aload_2        
                            //   813: astore          19
                            //   815: aload           17
                            //   817: astore_2       
                            //   818: astore          17
                            //   820: aload_1        
                            //   821: astore          18
                            //   823: aload           19
                            //   825: astore_1       
                            //   826: goto            1104
                            //   829: lload           7
                            //   831: lconst_0       
                            //   832: lcmp           
                            //   833: ifle            966
                            //   836: aload_0        
                            //   837: getfield        b/b/a/a/j/d/a$a.\u3007o\u3007:Lb/b/a/a/j/d/a;
                            //   840: getfield        b/b/a/a/j/d/a.o\u30070:Ljava/io/File;
                            //   843: invokevirtual   java/io/File.exists:()Z
                            //   846: ifeq            966
                            //   849: aload_0        
                            //   850: getfield        b/b/a/a/j/d/a$a.\u3007o\u3007:Lb/b/a/a/j/d/a;
                            //   853: getfield        b/b/a/a/j/d/a.o\u30070:Ljava/io/File;
                            //   856: invokevirtual   java/io/File.length:()J
                            //   859: lload           7
                            //   861: lcmp           
                            //   862: ifne            966
                            //   865: aload_0        
                            //   866: getfield        b/b/a/a/j/d/a$a.\u3007o\u3007:Lb/b/a/a/j/d/a;
                            //   869: astore          18
                            //   871: aload           18
                            //   873: getfield        b/b/a/a/j/d/a.o\u30070:Ljava/io/File;
                            //   876: aload           18
                            //   878: getfield        b/b/a/a/j/d/a.Oo08:Ljava/io/File;
                            //   881: invokevirtual   java/io/File.renameTo:(Ljava/io/File;)Z
                            //   884: ifeq            930
                            //   887: aload_0        
                            //   888: getfield        b/b/a/a/j/d/a$a.\u3007o\u3007:Lb/b/a/a/j/d/a;
                            //   891: lload           7
                            //   893: lload           7
                            //   895: aload_0        
                            //   896: getfield        b/b/a/a/j/d/a$a.\u3007080:Lb/b/a/a/j/c/a;
                            //   899: invokevirtual   b/b/a/a/j/d/a.\u3007\u3007888:(JJLb/b/a/a/j/c/a;)V
                            //   902: aload           23
                            //   904: aload_0        
                            //   905: getfield        b/b/a/a/j/d/a$a.\u3007o\u3007:Lb/b/a/a/j/d/a;
                            //   908: getfield        b/b/a/a/j/d/a.Oo08:Ljava/io/File;
                            //   911: invokevirtual   b/b/a/a/j/b.\u3007o00\u3007\u3007Oo:(Ljava/io/File;)V
                            //   914: aload_0        
                            //   915: getfield        b/b/a/a/j/d/a$a.\u3007080:Lb/b/a/a/j/c/a;
                            //   918: aload_0        
                            //   919: getfield        b/b/a/a/j/d/a$a.\u3007o\u3007:Lb/b/a/a/j/d/a;
                            //   922: aload           23
                            //   924: invokevirtual   b/b/a/a/j/c/a.a:(Lb/b/a/a/j/d/c;Lb/b/a/a/j/b;)V
                            //   927: goto            1053
                            //   930: aload_0        
                            //   931: getfield        b/b/a/a/j/d/a$a.\u3007080:Lb/b/a/a/j/c/a;
                            //   934: astore          19
                            //   936: aload_0        
                            //   937: getfield        b/b/a/a/j/d/a$a.\u3007o\u3007:Lb/b/a/a/j/d/a;
                            //   940: astore          20
                            //   942: new             Ljava/io/IOException;
                            //   945: astore          18
                            //   947: aload           18
                            //   949: ldc             "Rename fail"
                            //   951: invokespecial   java/io/IOException.<init>:(Ljava/lang/String;)V
                            //   954: aload           19
                            //   956: aload           20
                            //   958: aload           18
                            //   960: invokevirtual   b/b/a/a/j/c/a.a:(Lb/b/a/a/j/d/c;Ljava/io/IOException;)V
                            //   963: goto            1053
                            //   966: aload_0        
                            //   967: getfield        b/b/a/a/j/d/a$a.\u3007080:Lb/b/a/a/j/c/a;
                            //   970: astore          19
                            //   972: aload_0        
                            //   973: getfield        b/b/a/a/j/d/a$a.\u3007o\u3007:Lb/b/a/a/j/d/a;
                            //   976: astore          18
                            //   978: new             Ljava/io/IOException;
                            //   981: astore          22
                            //   983: new             Ljava/lang/StringBuilder;
                            //   986: astore          20
                            //   988: aload           20
                            //   990: invokespecial   java/lang/StringBuilder.<init>:()V
                            //   993: aload           20
                            //   995: ldc             " tempFile.length() == fileSize is"
                            //   997: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
                            //  1000: pop            
                            //  1001: aload_0        
                            //  1002: getfield        b/b/a/a/j/d/a$a.\u3007o\u3007:Lb/b/a/a/j/d/a;
                            //  1005: getfield        b/b/a/a/j/d/a.o\u30070:Ljava/io/File;
                            //  1008: invokevirtual   java/io/File.length:()J
                            //  1011: lload           7
                            //  1013: lcmp           
                            //  1014: ifne            1023
                            //  1017: iconst_1       
                            //  1018: istore          15
                            //  1020: goto            1026
                            //  1023: iconst_0       
                            //  1024: istore          15
                            //  1026: aload           20
                            //  1028: iload           15
                            //  1030: invokevirtual   java/lang/StringBuilder.append:(Z)Ljava/lang/StringBuilder;
                            //  1033: pop            
                            //  1034: aload           22
                            //  1036: aload           20
                            //  1038: invokevirtual   java/lang/StringBuilder.toString:()Ljava/lang/String;
                            //  1041: invokespecial   java/io/IOException.<init>:(Ljava/lang/String;)V
                            //  1044: aload           19
                            //  1046: aload           18
                            //  1048: aload           22
                            //  1050: invokevirtual   b/b/a/a/j/c/a.a:(Lb/b/a/a/j/d/c;Ljava/io/IOException;)V
                            //  1053: aload_1        
                            //  1054: invokevirtual   java/io/InputStream.close:()V
                            //  1057: goto            1066
                            //  1060: astore_1       
                            //  1061: aload           17
                            //  1063: invokestatic    b/b/a/a/j/f/d.\u3007080:(Ljava/lang/String;)V
                            //  1066: aload           21
                            //  1068: invokevirtual   java/io/RandomAccessFile.close:()V
                            //  1071: goto            1245
                            //  1074: astore_1       
                            //  1075: aload_2        
                            //  1076: invokestatic    b/b/a/a/j/f/d.\u3007080:(Ljava/lang/String;)V
                            //  1079: goto            1245
                            //  1082: astore          17
                            //  1084: aload           20
                            //  1086: astore_2       
                            //  1087: aload           19
                            //  1089: astore_1       
                            //  1090: goto            1104
                            //  1093: astore          17
                            //  1095: ldc             "Error occured when calling InputStream.close"
                            //  1097: astore_2       
                            //  1098: ldc             "Error occured when calling tmpFile.close"
                            //  1100: astore_1       
                            //  1101: aconst_null    
                            //  1102: astore          18
                            //  1104: ldc             "Error occured when FileRequest.parseHttpResponse"
                            //  1106: invokestatic    b/b/a/a/j/f/d.\u3007080:(Ljava/lang/String;)V
                            //  1109: aload           17
                            //  1111: invokevirtual   java/lang/Throwable.printStackTrace:()V
                            //  1114: aload_0        
                            //  1115: getfield        b/b/a/a/j/d/a$a.\u3007080:Lb/b/a/a/j/c/a;
                            //  1118: astore          19
                            //  1120: aload_0        
                            //  1121: getfield        b/b/a/a/j/d/a$a.\u3007o\u3007:Lb/b/a/a/j/d/a;
                            //  1124: astore          22
                            //  1126: new             Ljava/io/IOException;
                            //  1129: astore          20
                            //  1131: aload           20
                            //  1133: aload           17
                            //  1135: invokevirtual   java/lang/Throwable.getMessage:()Ljava/lang/String;
                            //  1138: invokespecial   java/io/IOException.<init>:(Ljava/lang/String;)V
                            //  1141: aload           19
                            //  1143: aload           22
                            //  1145: aload           20
                            //  1147: invokevirtual   b/b/a/a/j/c/a.a:(Lb/b/a/a/j/d/c;Ljava/io/IOException;)V
                            //  1150: iload           16
                            //  1152: ifne            1162
                            //  1155: aload_0        
                            //  1156: getfield        b/b/a/a/j/d/a$a.\u3007o\u3007:Lb/b/a/a/j/d/a;
                            //  1159: invokestatic    b/b/a/a/j/d/a.\u300780\u3007808\u3007O:(Lb/b/a/a/j/d/a;)V
                            //  1162: aload           18
                            //  1164: ifnull          1181
                            //  1167: aload           18
                            //  1169: invokevirtual   java/io/InputStream.close:()V
                            //  1172: goto            1181
                            //  1175: astore          17
                            //  1177: aload_2        
                            //  1178: invokestatic    b/b/a/a/j/f/d.\u3007080:(Ljava/lang/String;)V
                            //  1181: aload           21
                            //  1183: invokevirtual   java/io/RandomAccessFile.close:()V
                            //  1186: goto            1194
                            //  1189: astore_2       
                            //  1190: aload_1        
                            //  1191: invokestatic    b/b/a/a/j/f/d.\u3007080:(Ljava/lang/String;)V
                            //  1194: return         
                            //  1195: astore          17
                            //  1197: aload           18
                            //  1199: ifnull          1216
                            //  1202: aload           18
                            //  1204: invokevirtual   java/io/InputStream.close:()V
                            //  1207: goto            1216
                            //  1210: astore          18
                            //  1212: aload_2        
                            //  1213: invokestatic    b/b/a/a/j/f/d.\u3007080:(Ljava/lang/String;)V
                            //  1216: aload           21
                            //  1218: invokevirtual   java/io/RandomAccessFile.close:()V
                            //  1221: goto            1229
                            //  1224: astore_2       
                            //  1225: aload_1        
                            //  1226: invokestatic    b/b/a/a/j/f/d.\u3007080:(Ljava/lang/String;)V
                            //  1229: aload           17
                            //  1231: athrow         
                            //  1232: aload_0        
                            //  1233: getfield        b/b/a/a/j/d/a$a.\u3007080:Lb/b/a/a/j/c/a;
                            //  1236: aload_0        
                            //  1237: getfield        b/b/a/a/j/d/a$a.\u3007o\u3007:Lb/b/a/a/j/d/a;
                            //  1240: aload           23
                            //  1242: invokevirtual   b/b/a/a/j/c/a.a:(Lb/b/a/a/j/d/c;Lb/b/a/a/j/b;)V
                            //  1245: return         
                            //  1246: astore          18
                            //  1248: goto            534
                            //    Exceptions:
                            //  throws java.io.IOException
                            //    Exceptions:
                            //  Try           Handler
                            //  Start  End    Start  End    Type
                            //  -----  -----  -----  -----  ----
                            //  481    498    531    534    Any
                            //  503    517    1246   1251   Any
                            //  523    528    1246   1251   Any
                            //  540    548    1093   1104   Any
                            //  561    569    1082   1093   Any
                            //  582    589    1082   1093   Any
                            //  600    604    1082   1093   Any
                            //  615    620    1082   1093   Any
                            //  631    638    1082   1093   Any
                            //  656    670    1082   1093   Any
                            //  704    717    812    829    Any
                            //  738    755    812    829    Any
                            //  765    785    812    829    Any
                            //  797    809    812    829    Any
                            //  836    927    812    829    Any
                            //  930    963    812    829    Any
                            //  966    1017   812    829    Any
                            //  1026   1053   812    829    Any
                            //  1053   1057   1060   1066   Any
                            //  1066   1071   1074   1082   Any
                            //  1104   1150   1195   1232   Any
                            //  1155   1162   1195   1232   Any
                            //  1167   1172   1175   1181   Any
                            //  1181   1186   1189   1194   Any
                            //  1202   1207   1210   1216   Any
                            //  1216   1221   1224   1229   Any
                            // 
                            // The error that occurred was:
                            // 
                            // java.lang.IllegalStateException: Expression is linked from several locations: Label_0523:
                            //     at com.strobel.decompiler.ast.Error.expressionLinkedFromMultipleLocations(Error.java:27)
                            //     at com.strobel.decompiler.ast.AstOptimizer.mergeDisparateObjectInitializations(AstOptimizer.java:2604)
                            //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:235)
                            //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:42)
                            //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:206)
                            //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:93)
                            //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethodBody(AstBuilder.java:868)
                            //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethod(AstBuilder.java:761)
                            //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addTypeMembers(AstBuilder.java:638)
                            //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeCore(AstBuilder.java:605)
                            //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeNoCache(AstBuilder.java:195)
                            //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createType(AstBuilder.java:162)
                            //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.transformCall(AstMethodBodyBuilder.java:1151)
                            //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.transformByteCode(AstMethodBodyBuilder.java:993)
                            //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.transformExpression(AstMethodBodyBuilder.java:534)
                            //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.transformByteCode(AstMethodBodyBuilder.java:548)
                            //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.transformExpression(AstMethodBodyBuilder.java:534)
                            //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.transformNode(AstMethodBodyBuilder.java:377)
                            //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.transformBlock(AstMethodBodyBuilder.java:318)
                            //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.transformNode(AstMethodBodyBuilder.java:479)
                            //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.transformBlock(AstMethodBodyBuilder.java:318)
                            //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.transformNode(AstMethodBodyBuilder.java:425)
                            //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.transformBlock(AstMethodBodyBuilder.java:318)
                            //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.transformNode(AstMethodBodyBuilder.java:425)
                            //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.transformBlock(AstMethodBodyBuilder.java:318)
                            //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:213)
                            //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:93)
                            //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethodBody(AstBuilder.java:868)
                            //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethod(AstBuilder.java:761)
                            //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addTypeMembers(AstBuilder.java:638)
                            //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeCore(AstBuilder.java:605)
                            //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeNoCache(AstBuilder.java:195)
                            //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createType(AstBuilder.java:162)
                            //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addType(AstBuilder.java:137)
                            //     at com.strobel.decompiler.languages.java.JavaLanguage.buildAst(JavaLanguage.java:71)
                            //     at com.strobel.decompiler.languages.java.JavaLanguage.decompileType(JavaLanguage.java:59)
                            //     at com.strobel.decompiler.DecompilerDriver.decompileType(DecompilerDriver.java:333)
                            //     at com.strobel.decompiler.DecompilerDriver.decompileJar(DecompilerDriver.java:254)
                            //     at com.strobel.decompiler.DecompilerDriver.main(DecompilerDriver.java:144)
                            // 
                            throw new IllegalStateException("An error occurred while decompiling this method.");
                        }
                        
                        @Override
                        public void \u3007o00\u3007\u3007Oo(final b.b.a.a.f.a.b b, final IOException ex) {
                            final b.b.a.a.j.c.a \u3007080 = this.\u3007080;
                            if (\u3007080 != null) {
                                \u3007080.a(this.\u3007o\u3007, ex);
                            }
                            this.\u3007o\u3007.\u30078o8o\u3007();
                        }
                    });
                    return;
                }
                catch (final IllegalArgumentException ex) {
                    a.a(this, new IOException("Url is not a valid HTTP or HTTPS URL"));
                    return;
                }
            }
        }
        if (a != null) {
            a.a(this, new IOException("File info is null, please exec setFileInfo(String dir, String fileName)"));
        }
    }
    
    public void \u3007O8o08O(final String parent, final String s) {
        final File file = new File(parent);
        if (file.isFile()) {
            file.delete();
        }
        if (!file.exists()) {
            file.mkdirs();
        }
        this.Oo08 = new File(parent, s);
        final StringBuilder sb = new StringBuilder();
        sb.append(s);
        sb.append(".temp");
        this.o\u30070 = new File(parent, sb.toString());
    }
    
    protected void \u3007\u3007888(final long n, final long n2, final b.b.a.a.j.c.a a) {
        if (a != null) {
            a.a(this, n, n2);
        }
    }
}

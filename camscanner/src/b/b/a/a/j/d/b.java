// 
// Decompiled by Procyon v0.6.0
// 

package b.b.a.a.j.d;

import java.io.IOException;
import b.b.a.a.f.a.o;
import b.b.a.a.f.a.f;
import b.b.a.a.f.a.n;
import java.util.Iterator;
import java.util.Set;
import java.net.URLEncoder;
import android.text.TextUtils;
import android.net.Uri;
import b.b.a.a.f.a.g;
import b.b.a.a.f.a.l;
import b.b.a.a.j.f.d;
import java.util.HashMap;
import b.b.a.a.f.a.j;
import java.util.Map;
import b.b.a.a.f.a.a;

public class b extends c
{
    public static final a oO80;
    private a Oo08;
    private boolean o\u30070;
    private Map<String, String> \u3007\u3007888;
    
    static {
        oO80 = new a.a().\u3007o00\u3007\u3007Oo().\u3007080();
        new a.a().\u3007080();
    }
    
    public b(final j j) {
        super(j);
        this.Oo08 = b.oO80;
        this.o\u30070 = false;
        this.\u3007\u3007888 = new HashMap<String, String>();
    }
    
    public void OO0o\u3007\u3007\u3007\u30070(final String s, final String s2) {
        if (s == null) {
            d.\u3007o00\u3007\u3007Oo("GetExecutor", "name cannot be null !!!");
            return;
        }
        this.\u3007\u3007888.put(s, s2);
    }
    
    public void oO80(final boolean o\u30070) {
        this.o\u30070 = o\u30070;
    }
    
    public b.b.a.a.j.b \u300780\u3007808\u3007O() {
        try {
            final l.a a = new l.a();
            final boolean o\u30070 = this.o\u30070;
            final String s = "";
            if (o\u30070) {
                a.Oo08(super.O8);
            }
            else {
                final g.a a2 = new g.a();
                final Uri parse = Uri.parse(super.O8);
                a2.\u30070\u3007O0088o(parse.getScheme());
                a2.\u3007O8o08O(parse.getHost());
                final String encodedPath = parse.getEncodedPath();
                if (!TextUtils.isEmpty((CharSequence)encodedPath)) {
                    String substring = encodedPath;
                    if (encodedPath.startsWith("/")) {
                        substring = encodedPath.substring(1);
                    }
                    a2.\u3007o00\u3007\u3007Oo(substring);
                }
                final Set queryParameterNames = parse.getQueryParameterNames();
                if (queryParameterNames != null && queryParameterNames.size() > 0) {
                    for (final String s2 : queryParameterNames) {
                        this.\u3007\u3007888.put(s2, parse.getQueryParameter(s2));
                    }
                }
                for (final Map.Entry<String, V> entry : this.\u3007\u3007888.entrySet()) {
                    final String s3 = entry.getKey();
                    final String s4 = (String)entry.getValue();
                    if (!TextUtils.isEmpty((CharSequence)s3)) {
                        final String encode = URLEncoder.encode(s3, "UTF-8");
                        String s5;
                        if ((s5 = s4) == null) {
                            s5 = "";
                        }
                        a2.\u3007o\u3007(encode, URLEncoder.encode(s5, "UTF-8"));
                    }
                }
                a.\u3007o00\u3007\u3007Oo(a2.Oo08());
            }
            this.\u3007o00\u3007\u3007Oo(a);
            a.\u3007080(this.Oo08);
            a.O8(this.\u3007080());
            final n b = super.\u3007080.\u3007080(a.\u300780\u3007808\u3007O().oO80()).b();
            if (b != null) {
                final HashMap hashMap = new HashMap();
                final f \u30078o8o\u3007 = b.\u30078o8o\u3007();
                if (\u30078o8o\u3007 != null) {
                    for (int i = 0; i < \u30078o8o\u3007.\u3007080(); ++i) {
                        hashMap.put(\u30078o8o\u3007.\u3007o00\u3007\u3007Oo(i), \u30078o8o\u3007.\u3007o\u3007(i));
                    }
                }
                final o oo08 = b.Oo08();
                String \u30078o8o\u30072;
                if (oo08 == null) {
                    \u30078o8o\u30072 = s;
                }
                else {
                    \u30078o8o\u30072 = oo08.\u30078o8o\u3007();
                }
                return new b.b.a.a.j.b(b.\u3007O8o08O(), b.oO80(), b.OO0o\u3007\u3007(), hashMap, \u30078o8o\u30072, b.\u3007O00(), b.\u3007O\u3007());
            }
        }
        finally {
            final Throwable t;
            t.printStackTrace();
        }
        return null;
    }
    
    public void \u3007\u3007888(final b.b.a.a.j.c.a a) {
        try {
            final l.a a2 = new l.a();
            if (this.o\u30070) {
                a2.Oo08(super.O8);
            }
            else {
                final g.a a3 = new g.a();
                final Uri parse = Uri.parse(super.O8);
                a3.\u30070\u3007O0088o(parse.getScheme());
                a3.\u3007O8o08O(parse.getHost());
                final String encodedPath = parse.getEncodedPath();
                if (!TextUtils.isEmpty((CharSequence)encodedPath)) {
                    String substring = encodedPath;
                    if (encodedPath.startsWith("/")) {
                        substring = encodedPath.substring(1);
                    }
                    a3.\u3007o00\u3007\u3007Oo(substring);
                }
                final Set queryParameterNames = parse.getQueryParameterNames();
                if (queryParameterNames != null && queryParameterNames.size() > 0) {
                    for (final String s : queryParameterNames) {
                        this.\u3007\u3007888.put(s, parse.getQueryParameter(s));
                    }
                }
                for (final Map.Entry<String, V> entry : this.\u3007\u3007888.entrySet()) {
                    final String s2 = entry.getKey();
                    final String s3 = (String)entry.getValue();
                    if (!TextUtils.isEmpty((CharSequence)s2)) {
                        final String encode = URLEncoder.encode(s2, "UTF-8");
                        String s4;
                        if ((s4 = s3) == null) {
                            s4 = "";
                        }
                        a3.\u3007o\u3007(encode, URLEncoder.encode(s4, "UTF-8"));
                    }
                }
                a2.\u3007o00\u3007\u3007Oo(a3.Oo08());
            }
            this.\u3007o00\u3007\u3007Oo(a2);
            a2.\u3007080(this.Oo08);
            a2.O8(this.\u3007080());
            super.\u3007080.\u3007080(a2.\u300780\u3007808\u3007O().oO80()).\u3007O00(new b.b.a.a.f.a.c(this, a) {
                final b.b.a.a.j.c.a \u3007080;
                final b \u3007o00\u3007\u3007Oo;
                
                @Override
                public void \u3007080(final b.b.a.a.f.a.b b, final n n) throws IOException {
                    if (this.\u3007080 != null) {
                        final HashMap hashMap = new HashMap();
                        if (n != null) {
                            final f \u30078o8o\u3007 = n.\u30078o8o\u3007();
                            if (\u30078o8o\u3007 != null) {
                                for (int i = 0; i < \u30078o8o\u3007.\u3007080(); ++i) {
                                    hashMap.put(\u30078o8o\u3007.\u3007o00\u3007\u3007Oo(i), \u30078o8o\u3007.\u3007o\u3007(i));
                                }
                            }
                            final o oo08 = n.Oo08();
                            String \u30078o8o\u30072;
                            if (oo08 == null) {
                                \u30078o8o\u30072 = "";
                            }
                            else {
                                \u30078o8o\u30072 = oo08.\u30078o8o\u3007();
                            }
                            this.\u3007080.a(this.\u3007o00\u3007\u3007Oo, new b.b.a.a.j.b(n.\u3007O8o08O(), n.oO80(), n.OO0o\u3007\u3007(), hashMap, \u30078o8o\u30072, n.\u3007O00(), n.\u3007O\u3007()));
                        }
                    }
                }
                
                @Override
                public void \u3007o00\u3007\u3007Oo(final b.b.a.a.f.a.b b, final IOException ex) {
                    final b.b.a.a.j.c.a \u3007080 = this.\u3007080;
                    if (\u3007080 != null) {
                        \u3007080.a(this.\u3007o00\u3007\u3007Oo, ex);
                    }
                }
            });
        }
        finally {
            final Throwable t;
            if (d.\u3007o\u3007()) {
                t.printStackTrace();
            }
            if (a != null) {
                a.a(this, new IOException(t.getMessage()));
            }
        }
    }
}

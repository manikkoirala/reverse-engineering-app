// 
// Decompiled by Procyon v0.6.0
// 

package b.b.a.a.f.a;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public abstract class l
{
    public j \u3007080;
    
    public abstract Map<String, List<String>> O8();
    
    public abstract String Oo08();
    
    public abstract g oO80();
    
    public a o\u30070() {
        return new a(this);
    }
    
    public abstract m \u3007080();
    
    public void \u3007o00\u3007\u3007Oo(final j \u3007080) {
        this.\u3007080 = \u3007080;
    }
    
    public abstract b.b.a.a.f.a.a \u3007o\u3007();
    
    public abstract Object \u3007\u3007888();
    
    public static class a
    {
        String O8;
        Object Oo08;
        m o\u30070;
        b.b.a.a.f.a.a \u3007080;
        Map<String, List<String>> \u3007o00\u3007\u3007Oo;
        g \u3007o\u3007;
        
        public a() {
            this.\u3007o00\u3007\u3007Oo = new HashMap<String, List<String>>();
        }
        
        a(final l l) {
            this.\u3007o\u3007 = l.oO80();
            this.O8 = l.Oo08();
            this.\u3007o00\u3007\u3007Oo = l.O8();
            this.Oo08 = l.\u3007\u3007888();
            this.o\u30070 = l.\u3007080();
            this.\u3007080 = l.\u3007o\u3007();
        }
        
        private a o\u30070(final String o8, final m o\u30070) {
            this.O8 = o8;
            this.o\u30070 = o\u30070;
            return this;
        }
        
        public a O8(final Object oo08) {
            this.Oo08 = oo08;
            return this;
        }
        
        public a OO0o\u3007\u3007\u3007\u30070(final String s, final String s2) {
            return this.\u3007\u3007888(s, s2);
        }
        
        public a Oo08(final String s) {
            return this.\u3007o00\u3007\u3007Oo(g.\u3007O8o08O(s));
        }
        
        public l oO80() {
            return new l(this) {
                final a \u3007o00\u3007\u3007Oo;
                
                @Override
                public Map O8() {
                    return this.\u3007o00\u3007\u3007Oo.\u3007o00\u3007\u3007Oo;
                }
                
                @Override
                public String Oo08() {
                    return this.\u3007o00\u3007\u3007Oo.O8;
                }
                
                @Override
                public g oO80() {
                    return this.\u3007o00\u3007\u3007Oo.\u3007o\u3007;
                }
                
                @Override
                public String toString() {
                    return "";
                }
                
                @Override
                public m \u3007080() {
                    return this.\u3007o00\u3007\u3007Oo.o\u30070;
                }
                
                @Override
                public b.b.a.a.f.a.a \u3007o\u3007() {
                    return this.\u3007o00\u3007\u3007Oo.\u3007080;
                }
                
                @Override
                public Object \u3007\u3007888() {
                    return this.\u3007o00\u3007\u3007Oo.Oo08;
                }
            };
        }
        
        public a \u3007080(final b.b.a.a.f.a.a \u3007080) {
            this.\u3007080 = \u3007080;
            return this;
        }
        
        public a \u300780\u3007808\u3007O() {
            return this.o\u30070("GET", null);
        }
        
        public a \u3007o00\u3007\u3007Oo(final g \u3007o\u3007) {
            this.\u3007o\u3007 = \u3007o\u3007;
            return this;
        }
        
        public a \u3007o\u3007(final m m) {
            return this.o\u30070("POST", m);
        }
        
        public a \u3007\u3007888(final String s, final String s2) {
            if (!this.\u3007o00\u3007\u3007Oo.containsKey(s)) {
                this.\u3007o00\u3007\u3007Oo.put(s, new ArrayList<String>());
            }
            this.\u3007o00\u3007\u3007Oo.get(s).add(s2);
            return this;
        }
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package b.b.a.a.f.a;

public enum k
{
    b("http/1.0"), 
    c("http/1.1"), 
    d("spdy/3.1"), 
    e("h2");
    
    private static final k[] f;
    private final String a;
    
    private k(final String a) {
        this.a = a;
    }
    
    @Override
    public String toString() {
        return this.a;
    }
}

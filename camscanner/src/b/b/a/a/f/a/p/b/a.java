// 
// Decompiled by Procyon v0.6.0
// 

package b.b.a.a.f.a.p.b;

import java.io.OutputStream;
import java.util.concurrent.TimeUnit;
import java.util.Iterator;
import java.util.Map;
import java.net.URL;
import java.net.HttpURLConnection;
import b.b.a.a.f.a.c;
import b.b.a.a.f.a.j;
import java.util.List;
import java.io.IOException;
import b.b.a.a.f.a.h;
import java.util.Collection;
import java.util.ArrayList;
import b.b.a.a.f.a.n;
import android.text.TextUtils;
import b.b.a.a.f.a.m;
import java.util.concurrent.atomic.AtomicBoolean;
import b.b.a.a.f.a.l;
import b.b.a.a.f.a.d;
import b.b.a.a.f.a.b;

public class a implements b
{
    d OO;
    l o0;
    private AtomicBoolean \u3007OOo8\u30070;
    
    a(final l o0, final d oo) {
        this.\u3007OOo8\u30070 = new AtomicBoolean(false);
        this.o0 = o0;
        this.OO = oo;
    }
    
    private boolean O8(final m m) {
        if (m != null) {
            final l o0 = this.o0;
            if (o0 != null) {
                return "POST".equalsIgnoreCase(o0.Oo08()) && m.O8 == b.b.a.a.f.a.m.a.a && !TextUtils.isEmpty((CharSequence)m.\u3007o00\u3007\u3007Oo);
            }
        }
        return false;
    }
    
    private boolean o\u30070() {
        return this.o0.O8() != null && this.o0.O8().containsKey("Content-Type");
    }
    
    private boolean \u3007o\u3007(final m m) {
        if (m != null) {
            final l o0 = this.o0;
            if (o0 != null) {
                if (!"POST".equalsIgnoreCase(o0.Oo08())) {
                    return false;
                }
                if (m.O8 != b.b.a.a.f.a.m.a.b) {
                    return false;
                }
                final byte[] \u3007o\u3007 = m.\u3007o\u3007;
                if (\u3007o\u3007 != null) {
                    if (\u3007o\u3007.length > 0) {
                        return true;
                    }
                }
            }
        }
        return false;
    }
    
    @Override
    public n b() throws IOException {
        this.OO.O8().remove(this);
        this.OO.Oo08().add(this);
        if (this.OO.O8().size() + this.OO.Oo08().size() <= this.OO.\u3007080()) {
            if (!this.\u3007OOo8\u30070.get()) {
                try {
                    final j \u3007080 = this.o0.\u3007080;
                    if (\u3007080 != null) {
                        final List<h> o0 = \u3007080.o0;
                        if (o0 != null && o0.size() > 0) {
                            final ArrayList list = new ArrayList(this.o0.\u3007080.o0);
                            list.add(new h(this) {
                                final a \u3007080;
                                
                                @Override
                                public n \u3007080(final h.a a) throws IOException {
                                    return this.\u3007080.\u3007o00\u3007\u3007Oo(a.a());
                                }
                            });
                            return ((h)list.get(0)).\u3007080((h.a)new b.b.a.a.f.a.p.b.b(list, this.o0));
                        }
                    }
                    return this.\u3007o00\u3007\u3007Oo(this.o0);
                }
                finally {
                    final Throwable t;
                    throw new IOException(t.getMessage());
                }
            }
        }
        this.OO.Oo08().remove(this);
        return null;
    }
    
    public b \u3007080() {
        return new a(this.o0, this.OO);
    }
    
    @Override
    public void \u3007O00(final c c) {
        this.OO.\u3007o\u3007().submit(new Runnable(this, c) {
            final c o0;
            final a \u3007OOo8\u30070;
            
            @Override
            public void run() {
                try {
                    final n b = this.\u3007OOo8\u30070.b();
                    if (b == null) {
                        this.o0.\u3007o00\u3007\u3007Oo(this.\u3007OOo8\u30070, new IOException("response is null"));
                    }
                    else {
                        this.o0.\u3007080(this.\u3007OOo8\u30070, b);
                    }
                }
                catch (final IOException ex) {
                    ex.printStackTrace();
                    this.o0.\u3007o00\u3007\u3007Oo(this.\u3007OOo8\u30070, ex);
                }
            }
        });
    }
    
    public n \u3007o00\u3007\u3007Oo(final l l) throws IOException {
        try {
            try {
                final HttpURLConnection httpURLConnection = (HttpURLConnection)new URL(l.oO80().\u3007\u30078O0\u30078().toString()).openConnection();
                if (l.O8() != null && l.O8().size() > 0) {
                    for (final Map.Entry<K, List> entry : l.O8().entrySet()) {
                        final Iterator iterator2 = entry.getValue().iterator();
                        while (iterator2.hasNext()) {
                            httpURLConnection.addRequestProperty((String)entry.getKey(), (String)iterator2.next());
                        }
                    }
                }
                final j \u3007080 = l.\u3007080;
                if (\u3007080 != null) {
                    final TimeUnit oo = \u3007080.OO;
                    if (oo != null) {
                        httpURLConnection.setConnectTimeout((int)oo.toMillis(\u3007080.\u3007OOo8\u30070));
                    }
                    final j \u300781 = l.\u3007080;
                    if (\u300781.OO != null) {
                        httpURLConnection.setReadTimeout((int)\u300781.o\u300700O.toMillis(\u300781.\u300708O\u300700\u3007o));
                    }
                }
                if (l.\u3007080() == null) {
                    httpURLConnection.setRequestMethod("GET");
                }
                else {
                    if (!this.o\u30070() && l.\u3007080().\u3007080 != null) {
                        httpURLConnection.addRequestProperty("Content-Type", l.\u3007080().\u3007080.\u3007o00\u3007\u3007Oo());
                    }
                    httpURLConnection.setRequestMethod(l.Oo08());
                    if ("POST".equalsIgnoreCase(l.Oo08())) {
                        final OutputStream outputStream = httpURLConnection.getOutputStream();
                        if (this.\u3007o\u3007(l.\u3007080())) {
                            outputStream.write(l.\u3007080().\u3007o\u3007);
                        }
                        else if (this.O8(l.\u3007080())) {
                            outputStream.write(l.\u3007080().\u3007o00\u3007\u3007Oo.getBytes());
                        }
                        outputStream.flush();
                        outputStream.close();
                    }
                }
                httpURLConnection.connect();
                httpURLConnection.getResponseCode();
                if (this.\u3007OOo8\u30070.get()) {
                    httpURLConnection.disconnect();
                    this.OO.Oo08().remove(this);
                    return null;
                }
                final f f = new f(httpURLConnection, l);
                this.OO.Oo08().remove(this);
                return f;
            }
            finally {}
        }
        catch (final Exception ex) {
            throw new IOException(ex.getMessage());
        }
        this.OO.Oo08().remove(this);
    }
}

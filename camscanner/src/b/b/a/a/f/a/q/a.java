// 
// Decompiled by Procyon v0.6.0
// 

package b.b.a.a.f.a.q;

import java.io.EOFException;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.charset.Charset;
import java.nio.channels.ByteChannel;
import java.nio.channels.WritableByteChannel;
import java.io.Flushable;
import java.nio.channels.ReadableByteChannel;
import java.io.Closeable;

public final class a implements Closeable, ReadableByteChannel, Flushable, WritableByteChannel, Cloneable, ByteChannel
{
    private static final byte[] OO;
    c o0;
    long \u3007OOo8\u30070;
    
    static {
        OO = new byte[] { 48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 97, 98, 99, 100, 101, 102 };
    }
    
    public final b O08000() {
        final long \u3007oOo8\u30070 = this.\u3007OOo8\u30070;
        if (\u3007oOo8\u30070 <= 2147483647L) {
            return this.\u3007\u3007808\u3007((int)\u3007oOo8\u30070);
        }
        final StringBuilder sb = new StringBuilder();
        sb.append("size > Integer.MAX_VALUE: ");
        sb.append(this.\u3007OOo8\u30070);
        throw new IllegalArgumentException(sb.toString());
    }
    
    public a O8\u3007o(final int n) {
        final c ooo\u3007O0 = this.OOO\u3007O0(1);
        ooo\u3007O0.\u3007080[ooo\u3007O0.\u3007o\u3007++] = (byte)n;
        ++this.\u3007OOo8\u30070;
        return this;
    }
    
    public a OO0o\u3007\u3007(final String s, final int beginIndex, final int endIndex, final Charset charset) {
        if (s == null) {
            throw new IllegalArgumentException("string == null");
        }
        if (beginIndex < 0) {
            final StringBuilder sb = new StringBuilder();
            sb.append("beginIndex < 0: ");
            sb.append(beginIndex);
            throw new IllegalAccessError(sb.toString());
        }
        if (endIndex < beginIndex) {
            final StringBuilder sb2 = new StringBuilder();
            sb2.append("endIndex < beginIndex: ");
            sb2.append(endIndex);
            sb2.append(" < ");
            sb2.append(beginIndex);
            throw new IllegalArgumentException(sb2.toString());
        }
        if (endIndex > s.length()) {
            final StringBuilder sb3 = new StringBuilder();
            sb3.append("endIndex > string.length: ");
            sb3.append(endIndex);
            sb3.append(" > ");
            sb3.append(s.length());
            throw new IllegalArgumentException(sb3.toString());
        }
        if (charset == null) {
            throw new IllegalArgumentException("charset == null");
        }
        if (charset.equals(f.\u3007080)) {
            return this.\u3007O8o08O(s, beginIndex, endIndex);
        }
        final byte[] bytes = s.substring(beginIndex, endIndex).getBytes(charset);
        return this.o\u3007\u30070\u3007(bytes, 0, bytes.length);
    }
    
    c OOO\u3007O0(final int n) {
        if (n < 1 || n > 8192) {
            throw new IllegalArgumentException();
        }
        final c o0 = this.o0;
        if (o0 == null) {
            final c \u3007080 = d.\u3007080();
            this.o0 = \u3007080;
            \u3007080.\u3007\u3007888 = \u3007080;
            return \u3007080.o\u30070 = \u3007080;
        }
        final c \u3007\u3007888 = o0.\u3007\u3007888;
        if (\u3007\u3007888.\u3007o\u3007 + n <= 8192) {
            final c \u3007o00\u3007\u3007Oo = \u3007\u3007888;
            if (\u3007\u3007888.Oo08) {
                return \u3007o00\u3007\u3007Oo;
            }
        }
        return \u3007\u3007888.\u3007o00\u3007\u3007Oo(d.\u3007080());
    }
    
    public a O\u30078O8\u3007008(long i) {
        if (i == 0L) {
            return this.O8\u3007o(48);
        }
        final int n = Long.numberOfTrailingZeros(Long.highestOneBit(i)) / 4 + 1;
        final c ooo\u3007O0 = this.OOO\u3007O0(n);
        final byte[] \u3007080 = ooo\u3007O0.\u3007080;
        for (int \u3007o\u3007 = ooo\u3007O0.\u3007o\u3007, j = \u3007o\u3007 + n - 1; j >= \u3007o\u3007; --j) {
            \u3007080[j] = a.OO[(int)(0xFL & i)];
            i >>>= 4;
        }
        ooo\u3007O0.\u3007o\u3007 += n;
        this.\u3007OOo8\u30070 += n;
        return this;
    }
    
    @Override
    public void close() {
    }
    
    @Override
    public boolean equals(final Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof a)) {
            return false;
        }
        final a a = (a)o;
        final long \u3007oOo8\u30070 = this.\u3007OOo8\u30070;
        if (\u3007oOo8\u30070 != a.\u3007OOo8\u30070) {
            return false;
        }
        long n = 0L;
        if (\u3007oOo8\u30070 == 0L) {
            return true;
        }
        c o2 = this.o0;
        c o3 = a.o0;
        int \u3007o00\u3007\u3007Oo = o2.\u3007o00\u3007\u3007Oo;
        int \u3007o00\u3007\u3007Oo2 = o3.\u3007o00\u3007\u3007Oo;
        while (n < this.\u3007OOo8\u30070) {
            final long n2 = Math.min(o2.\u3007o\u3007 - \u3007o00\u3007\u3007Oo, o3.\u3007o\u3007 - \u3007o00\u3007\u3007Oo2);
            for (int n3 = 0; n3 < n2; ++n3, ++\u3007o00\u3007\u3007Oo, ++\u3007o00\u3007\u3007Oo2) {
                if (o2.\u3007080[\u3007o00\u3007\u3007Oo] != o3.\u3007080[\u3007o00\u3007\u3007Oo2]) {
                    return false;
                }
            }
            c o\u30070 = o2;
            int \u3007o00\u3007\u3007Oo3;
            if ((\u3007o00\u3007\u3007Oo3 = \u3007o00\u3007\u3007Oo) == o2.\u3007o\u3007) {
                o\u30070 = o2.o\u30070;
                \u3007o00\u3007\u3007Oo3 = o\u30070.\u3007o00\u3007\u3007Oo;
            }
            int \u3007o00\u3007\u3007Oo4 = \u3007o00\u3007\u3007Oo2;
            c o\u30072 = o3;
            if (\u3007o00\u3007\u3007Oo2 == o3.\u3007o\u3007) {
                o\u30072 = o3.o\u30070;
                \u3007o00\u3007\u3007Oo4 = o\u30072.\u3007o00\u3007\u3007Oo;
            }
            n += n2;
            o2 = o\u30070;
            \u3007o00\u3007\u3007Oo = \u3007o00\u3007\u3007Oo3;
            \u3007o00\u3007\u3007Oo2 = \u3007o00\u3007\u3007Oo4;
            o3 = o\u30072;
        }
        return true;
    }
    
    @Override
    public void flush() {
    }
    
    @Override
    public int hashCode() {
        c c = this.o0;
        if (c == null) {
            return 0;
        }
        int n = 1;
        c c2;
        int n2;
        do {
            int i = c.\u3007o00\u3007\u3007Oo;
            final int \u3007o\u3007 = c.\u3007o\u3007;
            n2 = n;
            while (i < \u3007o\u3007) {
                n2 = n2 * 31 + c.\u3007080[i];
                ++i;
            }
            c2 = (c = c.o\u30070);
            n = n2;
        } while (c2 != this.o0);
        return n2;
    }
    
    @Override
    public boolean isOpen() {
        return true;
    }
    
    public a oO(final int i) {
        if (i < 128) {
            this.O8\u3007o(i);
        }
        else if (i < 2048) {
            this.O8\u3007o(i >> 6 | 0xC0);
            this.O8\u3007o((i & 0x3F) | 0x80);
        }
        else if (i < 65536) {
            if (i >= 55296 && i <= 57343) {
                this.O8\u3007o(63);
            }
            else {
                this.O8\u3007o(i >> 12 | 0xE0);
                this.O8\u3007o((i >> 6 & 0x3F) | 0x80);
                this.O8\u3007o((i & 0x3F) | 0x80);
            }
        }
        else {
            if (i > 1114111) {
                final StringBuilder sb = new StringBuilder();
                sb.append("Unexpected code point: ");
                sb.append(Integer.toHexString(i));
                throw new IllegalArgumentException(sb.toString());
            }
            this.O8\u3007o(i >> 18 | 0xF0);
            this.O8\u3007o((i >> 12 & 0x3F) | 0x80);
            this.O8\u3007o((i >> 6 & 0x3F) | 0x80);
            this.O8\u3007o((i & 0x3F) | 0x80);
        }
        return this;
    }
    
    public a oO80() {
        final a a = new a();
        if (this.\u3007OOo8\u30070 == 0L) {
            return a;
        }
        final c \u3007o\u3007 = this.o0.\u3007o\u3007();
        a.o0 = \u3007o\u3007;
        \u3007o\u3007.\u3007\u3007888 = \u3007o\u3007;
        \u3007o\u3007.o\u30070 = \u3007o\u3007;
        for (c c = this.o0.o\u30070; c != this.o0; c = c.o\u30070) {
            a.o0.\u3007\u3007888.\u3007o00\u3007\u3007Oo(c.\u3007o\u3007());
        }
        a.\u3007OOo8\u30070 = this.\u3007OOo8\u30070;
        return a;
    }
    
    public a o\u3007\u30070\u3007(final byte[] array, int i, int n) {
        if (array != null) {
            final long n2 = array.length;
            final long n3 = i;
            final long n4 = n;
            f.\u3007\u3007888(n2, n3, n4);
            c ooo\u3007O0;
            int min;
            for (n += i; i < n; i += min, ooo\u3007O0.\u3007o\u3007 += min) {
                ooo\u3007O0 = this.OOO\u3007O0(1);
                min = Math.min(n - i, 8192 - ooo\u3007O0.\u3007o\u3007);
                System.arraycopy(array, i, ooo\u3007O0.\u3007080, ooo\u3007O0.\u3007o\u3007, min);
            }
            this.\u3007OOo8\u30070 += n4;
            return this;
        }
        throw new IllegalArgumentException("source == null");
    }
    
    @Override
    public int read(final ByteBuffer byteBuffer) throws IOException {
        final c o0 = this.o0;
        if (o0 == null) {
            return -1;
        }
        final int min = Math.min(byteBuffer.remaining(), o0.\u3007o\u3007 - o0.\u3007o00\u3007\u3007Oo);
        byteBuffer.put(o0.\u3007080, o0.\u3007o00\u3007\u3007Oo, min);
        final int \u3007o00\u3007\u3007Oo = o0.\u3007o00\u3007\u3007Oo + min;
        o0.\u3007o00\u3007\u3007Oo = \u3007o00\u3007\u3007Oo;
        this.\u3007OOo8\u30070 -= min;
        if (\u3007o00\u3007\u3007Oo == o0.\u3007o\u3007) {
            this.o0 = o0.\u3007080();
            d.\u3007o00\u3007\u3007Oo(o0);
        }
        return min;
    }
    
    @Override
    public String toString() {
        return this.O08000().toString();
    }
    
    @Override
    public int write(final ByteBuffer byteBuffer) throws IOException {
        if (byteBuffer != null) {
            int i;
            int n;
            c ooo\u3007O0;
            int min;
            for (n = (i = byteBuffer.remaining()); i > 0; i -= min, ooo\u3007O0.\u3007o\u3007 += min) {
                ooo\u3007O0 = this.OOO\u3007O0(1);
                min = Math.min(i, 8192 - ooo\u3007O0.\u3007o\u3007);
                byteBuffer.get(ooo\u3007O0.\u3007080, ooo\u3007O0.\u3007o\u3007, min);
            }
            this.\u3007OOo8\u30070 += n;
            return n;
        }
        throw new IllegalArgumentException("source == null");
    }
    
    public boolean \u300700\u30078() {
        return this.\u3007OOo8\u30070 == 0L;
    }
    
    public String \u30078() {
        try {
            return this.\u3007O\u3007(this.\u3007OOo8\u30070, f.\u3007080);
        }
        catch (final EOFException detailMessage) {
            throw new AssertionError((Object)detailMessage);
        }
    }
    
    public a \u30078o8o\u3007(final String s) {
        return this.\u3007O8o08O(s, 0, s.length());
    }
    
    public a \u3007O8o08O(final String s, int i, final int j) {
        if (s == null) {
            throw new IllegalArgumentException("string == null");
        }
        if (i < 0) {
            final StringBuilder sb = new StringBuilder();
            sb.append("beginIndex < 0: ");
            sb.append(i);
            throw new IllegalArgumentException(sb.toString());
        }
        if (j < i) {
            final StringBuilder sb2 = new StringBuilder();
            sb2.append("endIndex < beginIndex: ");
            sb2.append(j);
            sb2.append(" < ");
            sb2.append(i);
            throw new IllegalArgumentException(sb2.toString());
        }
        if (j <= s.length()) {
            while (i < j) {
                final char char1 = s.charAt(i);
                if (char1 < '\u0080') {
                    final c ooo\u3007O0 = this.OOO\u3007O0(1);
                    final byte[] \u3007080 = ooo\u3007O0.\u3007080;
                    final int n = ooo\u3007O0.\u3007o\u3007 - i;
                    final int min = Math.min(j, 8192 - n);
                    final int n2 = i + 1;
                    \u3007080[i + n] = (byte)char1;
                    char char2;
                    int n3;
                    for (i = n2; i < min; i = n3) {
                        char2 = s.charAt(i);
                        if (char2 >= '\u0080') {
                            break;
                        }
                        n3 = i + 1;
                        \u3007080[i + n] = (byte)char2;
                    }
                    final int \u3007o\u3007 = ooo\u3007O0.\u3007o\u3007;
                    final int n4 = n + i - \u3007o\u3007;
                    ooo\u3007O0.\u3007o\u3007 = \u3007o\u3007 + n4;
                    this.\u3007OOo8\u30070 += n4;
                }
                else {
                    if (char1 < '\u0800') {
                        this.O8\u3007o(char1 >> 6 | 0xC0);
                        this.O8\u3007o((char1 & '?') | 0x80);
                    }
                    else if (char1 >= '\ud800' && char1 <= '\udfff') {
                        final int index = i + 1;
                        char char3;
                        if (index < j) {
                            char3 = s.charAt(index);
                        }
                        else {
                            char3 = '\0';
                        }
                        if (char1 <= '\udbff' && char3 >= '\udc00' && char3 <= '\udfff') {
                            final int n5 = ((char1 & 0xFFFF27FF) << 10 | (0xFFFF23FF & char3)) + 65536;
                            this.O8\u3007o(n5 >> 18 | 0xF0);
                            this.O8\u3007o((n5 >> 12 & 0x3F) | 0x80);
                            this.O8\u3007o((n5 >> 6 & 0x3F) | 0x80);
                            this.O8\u3007o((n5 & 0x3F) | 0x80);
                            i += 2;
                            continue;
                        }
                        this.O8\u3007o(63);
                        i = index;
                        continue;
                    }
                    else {
                        this.O8\u3007o(char1 >> 12 | 0xE0);
                        this.O8\u3007o((char1 >> 6 & 0x3F) | 0x80);
                        this.O8\u3007o((char1 & '?') | 0x80);
                    }
                    ++i;
                }
            }
            return this;
        }
        final StringBuilder sb3 = new StringBuilder();
        sb3.append("endIndex > string.length: ");
        sb3.append(j);
        sb3.append(" > ");
        sb3.append(s.length());
        throw new IllegalArgumentException(sb3.toString());
    }
    
    public String \u3007O\u3007(final long lng, final Charset charset) throws EOFException {
        f.\u3007\u3007888(this.\u3007OOo8\u30070, 0L, lng);
        if (charset == null) {
            throw new IllegalArgumentException("charset == null");
        }
        if (lng > 2147483647L) {
            final StringBuilder sb = new StringBuilder();
            sb.append("byteCount > Integer.MAX_VALUE: ");
            sb.append(lng);
            throw new IllegalArgumentException(sb.toString());
        }
        if (lng == 0L) {
            return "";
        }
        final c o0 = this.o0;
        final int \u3007o00\u3007\u3007Oo = o0.\u3007o00\u3007\u3007Oo;
        if (\u3007o00\u3007\u3007Oo + lng > o0.\u3007o\u3007) {
            return new String(this.\u3007oo\u3007(lng), charset);
        }
        final String s = new String(o0.\u3007080, \u3007o00\u3007\u3007Oo, (int)lng, charset);
        final int \u3007o00\u3007\u3007Oo2 = (int)(o0.\u3007o00\u3007\u3007Oo + lng);
        o0.\u3007o00\u3007\u3007Oo = \u3007o00\u3007\u3007Oo2;
        this.\u3007OOo8\u30070 -= lng;
        if (\u3007o00\u3007\u3007Oo2 == o0.\u3007o\u3007) {
            this.o0 = o0.\u3007080();
            d.\u3007o00\u3007\u3007Oo(o0);
        }
        return s;
    }
    
    public byte[] \u3007oo\u3007(final long lng) throws EOFException {
        f.\u3007\u3007888(this.\u3007OOo8\u30070, 0L, lng);
        if (lng <= 2147483647L) {
            final byte[] array = new byte[(int)lng];
            this.\u3007\u30078O0\u30078(array);
            return array;
        }
        final StringBuilder sb = new StringBuilder();
        sb.append("byteCount > Integer.MAX_VALUE: ");
        sb.append(lng);
        throw new IllegalArgumentException(sb.toString());
    }
    
    public final b \u3007\u3007808\u3007(final int n) {
        if (n == 0) {
            return b.f;
        }
        return new e(this, n);
    }
    
    public int \u3007\u3007888(final byte[] array, int \u3007o00\u3007\u3007Oo, int min) {
        f.\u3007\u3007888(array.length, \u3007o00\u3007\u3007Oo, min);
        final c o0 = this.o0;
        if (o0 == null) {
            return -1;
        }
        min = Math.min(min, o0.\u3007o\u3007 - o0.\u3007o00\u3007\u3007Oo);
        System.arraycopy(o0.\u3007080, o0.\u3007o00\u3007\u3007Oo, array, \u3007o00\u3007\u3007Oo, min);
        \u3007o00\u3007\u3007Oo = o0.\u3007o00\u3007\u3007Oo + min;
        o0.\u3007o00\u3007\u3007Oo = \u3007o00\u3007\u3007Oo;
        this.\u3007OOo8\u30070 -= min;
        if (\u3007o00\u3007\u3007Oo == o0.\u3007o\u3007) {
            this.o0 = o0.\u3007080();
            d.\u3007o00\u3007\u3007Oo(o0);
        }
        return min;
    }
    
    public void \u3007\u30078O0\u30078(final byte[] array) throws EOFException {
        int \u3007\u3007888;
        for (int i = 0; i < array.length; i += \u3007\u3007888) {
            \u3007\u3007888 = this.\u3007\u3007888(array, i, array.length - i);
            if (\u3007\u3007888 == -1) {
                throw new EOFException();
            }
        }
    }
    
    public byte \u3007\u3007\u30070\u3007\u30070() {
        final long \u3007oOo8\u30070 = this.\u3007OOo8\u30070;
        if (\u3007oOo8\u30070 != 0L) {
            final c o0 = this.o0;
            final int \u3007o00\u3007\u3007Oo = o0.\u3007o00\u3007\u3007Oo;
            final int \u3007o\u3007 = o0.\u3007o\u3007;
            final byte[] \u3007080 = o0.\u3007080;
            final int \u3007o00\u3007\u3007Oo2 = \u3007o00\u3007\u3007Oo + 1;
            final byte b = \u3007080[\u3007o00\u3007\u3007Oo];
            this.\u3007OOo8\u30070 = \u3007oOo8\u30070 - 1L;
            if (\u3007o00\u3007\u3007Oo2 == \u3007o\u3007) {
                this.o0 = o0.\u3007080();
                d.\u3007o00\u3007\u3007Oo(o0);
            }
            else {
                o0.\u3007o00\u3007\u3007Oo = \u3007o00\u3007\u3007Oo2;
            }
            return b;
        }
        throw new IllegalStateException("size == 0");
    }
}

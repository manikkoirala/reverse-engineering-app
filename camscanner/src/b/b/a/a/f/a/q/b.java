// 
// Decompiled by Procyon v0.6.0
// 

package b.b.a.a.f.a.q;

import java.util.Arrays;
import java.nio.charset.Charset;
import java.io.Serializable;

public class b implements Serializable, Comparable<b>
{
    public static final Charset e;
    public static final b f;
    static final char[] \u300708O\u300700\u3007o;
    transient String OO;
    final byte[] o0;
    transient int \u3007OOo8\u30070;
    
    static {
        \u300708O\u300700\u3007o = new char[] { '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'a', 'b', 'c', 'd', 'e', 'f' };
        e = Charset.forName("UTF-8");
        f = a(new byte[0]);
    }
    
    b(final byte[] o0) {
        this.o0 = o0;
    }
    
    public static b a(final byte... array) {
        if (array != null) {
            return new b(array.clone());
        }
        throw new IllegalArgumentException("data == null");
    }
    
    static int \u3007080(final String s, final int n) {
        final int length = s.length();
        int i = 0;
        int n2 = 0;
        while (i < length) {
            if (n2 == n) {
                return i;
            }
            final int codePoint = s.codePointAt(i);
            if ((Character.isISOControl(codePoint) && codePoint != 10 && codePoint != 13) || codePoint == 65533) {
                return -1;
            }
            ++n2;
            i += Character.charCount(codePoint);
        }
        return s.length();
    }
    
    public byte a(final int n) {
        return this.o0[n];
    }
    
    public int a(final b b) {
        final int c = this.c();
        final int c2 = b.c();
        final int min = Math.min(c, c2);
        int n = 0;
        while (true) {
            int n2 = -1;
            if (n < min) {
                final int n3 = this.a(n) & 0xFF;
                final int n4 = b.a(n) & 0xFF;
                if (n3 != n4) {
                    if (n3 >= n4) {
                        n2 = 1;
                    }
                    return n2;
                }
                ++n;
            }
            else {
                if (c == c2) {
                    return 0;
                }
                if (c >= c2) {
                    n2 = 1;
                }
                return n2;
            }
        }
    }
    
    public b a(final int n, final int n2) {
        if (n < 0) {
            throw new IllegalArgumentException("beginIndex < 0");
        }
        final byte[] o0 = this.o0;
        if (n2 > o0.length) {
            final StringBuilder sb = new StringBuilder();
            sb.append("endIndex > length(");
            sb.append(this.o0.length);
            sb.append(")");
            throw new IllegalArgumentException(sb.toString());
        }
        final int n3 = n2 - n;
        if (n3 < 0) {
            throw new IllegalArgumentException("endIndex < beginIndex");
        }
        if (n == 0 && n2 == o0.length) {
            return this;
        }
        final byte[] array = new byte[n3];
        System.arraycopy(o0, n, array, 0, n3);
        return new b(array);
    }
    
    public boolean a(final int n, final byte[] array, final int n2, final int n3) {
        if (n >= 0) {
            final byte[] o0 = this.o0;
            if (n <= o0.length - n3 && n2 >= 0 && n2 <= array.length - n3 && b.b.a.a.f.a.q.f.\u300780\u3007808\u3007O(o0, n, array, n2, n3)) {
                return true;
            }
        }
        return false;
    }
    
    public String b() {
        final byte[] o0 = this.o0;
        final char[] value = new char[o0.length * 2];
        final int length = o0.length;
        int i = 0;
        int n = 0;
        while (i < length) {
            final byte b = o0[i];
            final int n2 = n + 1;
            final char[] \u300708O\u300700\u3007o = b.b.a.a.f.a.q.b.\u300708O\u300700\u3007o;
            value[n] = \u300708O\u300700\u3007o[b >> 4 & 0xF];
            n = n2 + 1;
            value[n2] = \u300708O\u300700\u3007o[b & 0xF];
            ++i;
        }
        return new String(value);
    }
    
    public int c() {
        return this.o0.length;
    }
    
    public String d() {
        String oo = this.OO;
        if (oo == null) {
            oo = new String(this.o0, b.e);
            this.OO = oo;
        }
        return oo;
    }
    
    @Override
    public boolean equals(final Object o) {
        boolean b = true;
        if (o == this) {
            return true;
        }
        if (o instanceof b) {
            final b b2 = (b)o;
            final int c = b2.c();
            final byte[] o2 = this.o0;
            if (c == o2.length && b2.a(0, o2, 0, o2.length)) {
                return b;
            }
        }
        b = false;
        return b;
    }
    
    @Override
    public int hashCode() {
        int \u3007oOo8\u30070 = this.\u3007OOo8\u30070;
        if (\u3007oOo8\u30070 == 0) {
            \u3007oOo8\u30070 = Arrays.hashCode(this.o0);
            this.\u3007OOo8\u30070 = \u3007oOo8\u30070;
        }
        return \u3007oOo8\u30070;
    }
    
    @Override
    public String toString() {
        if (this.o0.length == 0) {
            return "[size=0]";
        }
        final String d = this.d();
        final int \u3007080 = \u3007080(d, 64);
        if (\u3007080 == -1) {
            String s;
            if (this.o0.length <= 64) {
                final StringBuilder sb = new StringBuilder();
                sb.append("[hex=");
                sb.append(this.b());
                sb.append("]");
                s = sb.toString();
            }
            else {
                final StringBuilder sb2 = new StringBuilder();
                sb2.append("[size=");
                sb2.append(this.o0.length);
                sb2.append(" hex=");
                sb2.append(this.a(0, 64).b());
                sb2.append("\u2026]");
                s = sb2.toString();
            }
            return s;
        }
        final String replace = d.substring(0, \u3007080).replace("\\", "\\\\").replace("\n", "\\n").replace("\r", "\\r");
        StringBuilder sb3;
        if (\u3007080 < d.length()) {
            sb3 = new StringBuilder();
            sb3.append("[size=");
            sb3.append(this.o0.length);
            sb3.append(" text=");
            sb3.append(replace);
            sb3.append("\u2026]");
        }
        else {
            sb3 = new StringBuilder();
            sb3.append("[text=");
            sb3.append(replace);
            sb3.append("]");
        }
        return sb3.toString();
    }
}

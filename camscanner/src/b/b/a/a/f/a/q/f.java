// 
// Decompiled by Procyon v0.6.0
// 

package b.b.a.a.f.a.q;

import java.net.UnknownHostException;
import java.util.Arrays;
import java.net.InetAddress;
import java.util.Locale;
import java.net.IDN;
import java.nio.charset.Charset;

public final class f
{
    public static final Charset \u3007080;
    
    static {
        \u3007080 = Charset.forName("UTF-8");
    }
    
    public static String O8(String lowerCase) {
        if (lowerCase.contains(":")) {
            InetAddress inetAddress;
            if (lowerCase.startsWith("[") && lowerCase.endsWith("]")) {
                inetAddress = o\u30070(lowerCase, 1, lowerCase.length() - 1);
            }
            else {
                inetAddress = o\u30070(lowerCase, 0, lowerCase.length());
            }
            if (inetAddress == null) {
                return null;
            }
            final byte[] address = inetAddress.getAddress();
            if (address.length == 16) {
                return Oo08(address);
            }
            final StringBuilder sb = new StringBuilder();
            sb.append("Invalid IPv6 address: '");
            sb.append(lowerCase);
            sb.append("'");
            throw new AssertionError((Object)sb.toString());
        }
        else {
            try {
                lowerCase = IDN.toASCII(lowerCase).toLowerCase(Locale.US);
                if (lowerCase.isEmpty()) {
                    return null;
                }
                if (\u30078o8o\u3007(lowerCase)) {
                    return null;
                }
                return lowerCase;
            }
            catch (final IllegalArgumentException ex) {
                return null;
            }
        }
    }
    
    public static int OO0o\u3007\u3007\u3007\u30070(final String s, int i, final int n) {
        while (i < n) {
            final char char1 = s.charAt(i);
            if (char1 != '\t' && char1 != '\n' && char1 != '\f' && char1 != '\r' && char1 != ' ') {
                return i;
            }
            ++i;
        }
        return n;
    }
    
    private static String Oo08(final byte[] array) {
        int n = -1;
        final int n2 = 0;
        int i = 0;
        int n3 = 0;
        while (i < array.length) {
            int n4;
            for (n4 = i; n4 < 16 && array[n4] == 0 && array[n4 + 1] == 0; n4 += 2) {}
            final int n5 = n4 - i;
            int n6 = n;
            int n7;
            if (n5 > (n7 = n3)) {
                n6 = n;
                n7 = n3;
                if (n5 >= 4) {
                    n7 = n5;
                    n6 = i;
                }
            }
            i = n4 + 2;
            n = n6;
            n3 = n7;
        }
        final a a = new a();
        int j = n2;
        while (j < array.length) {
            if (j == n) {
                a.O8\u3007o(58);
                final int n8 = j + n3;
                if ((j = n8) != 16) {
                    continue;
                }
                a.O8\u3007o(58);
                j = n8;
            }
            else {
                if (j > 0) {
                    a.O8\u3007o(58);
                }
                a.O\u30078O8\u3007008((array[j] & 0xFF) << 8 | (array[j + 1] & 0xFF));
                j += 2;
            }
        }
        return a.\u30078();
    }
    
    private static boolean oO80(final String s, int n, final int n2, final byte[] array, final int n3) {
        int n4 = n3;
        int i = n;
        while (i < n2) {
            if (n4 == array.length) {
                return false;
            }
            n = i;
            if (n4 != n3) {
                if (s.charAt(i) != '.') {
                    return false;
                }
                n = i + 1;
            }
            i = n;
            int n5 = 0;
            while (i < n2) {
                final char char1 = s.charAt(i);
                if (char1 < '0') {
                    break;
                }
                if (char1 > '9') {
                    break;
                }
                if (n5 == 0 && n != i) {
                    return false;
                }
                n5 = n5 * 10 + char1 - 48;
                if (n5 > 255) {
                    return false;
                }
                ++i;
            }
            if (i - n == 0) {
                return false;
            }
            array[n4] = (byte)n5;
            ++n4;
        }
        return n4 == n3 + 4;
    }
    
    private static InetAddress o\u30070(final String s, int toffset, final int n) {
        final byte[] array = new byte[16];
        int n2 = 0;
        int n3 = -1;
        int n4 = -1;
        int n5;
        int fromIndex;
        while (true) {
            n5 = n2;
            fromIndex = n3;
            if (toffset >= n) {
                break;
            }
            if (n2 == 16) {
                return null;
            }
            final int n6 = toffset + 2;
            if (n6 <= n && s.regionMatches(toffset, "::", 0, 2)) {
                if (n3 != -1) {
                    return null;
                }
                n5 = (toffset = n2 + 2);
                if (n6 == n) {
                    fromIndex = toffset;
                    break;
                }
                final int n7 = n6;
                n2 = n5;
                n3 = toffset;
                toffset = n7;
            }
            else {
                int n8 = toffset;
                if (n2 != 0) {
                    if (s.regionMatches(toffset, ":", 0, 1)) {
                        n8 = toffset + 1;
                    }
                    else {
                        if (!s.regionMatches(toffset, ".", 0, 1)) {
                            return null;
                        }
                        if (!oO80(s, n4, n, array, n2 - 2)) {
                            return null;
                        }
                        n5 = n2 + 2;
                        fromIndex = n3;
                        break;
                    }
                }
                toffset = n8;
            }
            int i = toffset;
            int n9 = 0;
            while (i < n) {
                final int \u3007080 = \u3007080(s.charAt(i));
                if (\u3007080 == -1) {
                    break;
                }
                n9 = (n9 << 4) + \u3007080;
                ++i;
            }
            final int n10 = i - toffset;
            if (n10 == 0 || n10 > 4) {
                return null;
            }
            final int n11 = n2 + 1;
            array[n2] = (byte)(n9 >>> 8 & 0xFF);
            n2 = n11 + 1;
            array[n11] = (byte)(n9 & 0xFF);
            n4 = toffset;
            toffset = i;
        }
        if (n5 != 16) {
            if (fromIndex == -1) {
                return null;
            }
            toffset = n5 - fromIndex;
            System.arraycopy(array, fromIndex, array, 16 - toffset, toffset);
            Arrays.fill(array, fromIndex, 16 - n5 + fromIndex, (byte)0);
        }
        try {
            return InetAddress.getByAddress(array);
        }
        catch (final UnknownHostException ex) {
            throw new AssertionError();
        }
    }
    
    public static int \u3007080(final char c) {
        if (c >= '0' && c <= '9') {
            return c - '0';
        }
        char c2 = 'a';
        if (c < 'a' || c > 'f') {
            c2 = 'A';
            if (c < 'A' || c > 'F') {
                return -1;
            }
        }
        return c - c2 + 10;
    }
    
    public static boolean \u300780\u3007808\u3007O(final byte[] array, final int n, final byte[] array2, final int n2, final int n3) {
        for (int i = 0; i < n3; ++i) {
            if (array[i + n] != array2[i + n2]) {
                return false;
            }
        }
        return true;
    }
    
    private static boolean \u30078o8o\u3007(final String s) {
        for (int i = 0; i < s.length(); ++i) {
            final char char1 = s.charAt(i);
            if (char1 <= '\u001f' || char1 >= '\u007f') {
                return true;
            }
            if (" #%/:?@[\\]".indexOf(char1) != -1) {
                return true;
            }
        }
        return false;
    }
    
    public static int \u3007O8o08O(final String s, final int n, int i) {
        --i;
        while (i >= n) {
            final char char1 = s.charAt(i);
            if (char1 != '\t' && char1 != '\n' && char1 != '\f' && char1 != '\r' && char1 != ' ') {
                return i + 1;
            }
            --i;
        }
        return n;
    }
    
    public static int \u3007o00\u3007\u3007Oo(final String s, int i, final int n, final char c) {
        while (i < n) {
            if (s.charAt(i) == c) {
                return i;
            }
            ++i;
        }
        return n;
    }
    
    public static int \u3007o\u3007(final String s, int i, final int n, final String s2) {
        while (i < n) {
            if (s2.indexOf(s.charAt(i)) != -1) {
                return i;
            }
            ++i;
        }
        return n;
    }
    
    public static void \u3007\u3007888(final long n, final long n2, final long n3) {
        if ((n2 | n3) >= 0L && n2 <= n && n - n2 >= n3) {
            return;
        }
        throw new ArrayIndexOutOfBoundsException();
    }
}

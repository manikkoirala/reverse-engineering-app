// 
// Decompiled by Procyon v0.6.0
// 

package b.b.a.a.f.a.p.b;

import java.io.ByteArrayOutputStream;
import java.io.Reader;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import b.b.a.a.f.a.o;

public class g extends o
{
    HttpURLConnection o0;
    InputStream \u3007OOo8\u30070;
    
    g(final HttpURLConnection o0) throws IOException {
        this.o0 = o0;
        this.\u3007OOo8\u30070 = new e(o0.getInputStream(), o0);
    }
    
    @Override
    public InputStream Oo08() {
        return this.\u3007OOo8\u30070;
    }
    
    @Override
    public void close() {
        try {
            this.\u3007OOo8\u30070.close();
            this.o0.disconnect();
        }
        catch (final Exception ex) {}
    }
    
    @Override
    public long oO80() {
        try {
            return this.o0.getContentLength();
        }
        catch (final Exception ex) {
            return 0L;
        }
    }
    
    @Override
    public String \u30078o8o\u3007() {
        try {
            final BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(this.\u3007OOo8\u30070));
            final StringBuffer sb = new StringBuffer();
            while (true) {
                final String line = bufferedReader.readLine();
                if (line == null) {
                    break;
                }
                final StringBuilder sb2 = new StringBuilder();
                sb2.append(line);
                sb2.append("\n");
                sb.append(sb2.toString());
            }
            final String string = sb.toString();
            this.close();
            return string;
        }
        catch (final Exception ex) {
            return "";
        }
    }
    
    @Override
    public byte[] \u3007\u3007888() {
        try {
            final byte[] array = new byte[1024];
            final ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
            while (true) {
                final int read = this.\u3007OOo8\u30070.read(array);
                if (read == -1) {
                    break;
                }
                byteArrayOutputStream.write(array, 0, read);
            }
            return byteArrayOutputStream.toByteArray();
        }
        catch (final Exception ex) {
            return new byte[0];
        }
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package b.b.a.a.f.a.q;

import java.util.Arrays;

final class e extends b
{
    final transient int[] O8o08O8O;
    final transient byte[][] o\u300700O;
    
    e(final a a, final int n) {
        super(null);
        b.b.a.a.f.a.q.f.\u3007\u3007888(a.\u3007OOo8\u30070, 0L, n);
        c c = a.o0;
        final int n2 = 0;
        int i;
        int n3;
        int \u3007o\u3007;
        int \u3007o00\u3007\u3007Oo;
        for (i = 0, n3 = 0; i < n; i += \u3007o\u3007 - \u3007o00\u3007\u3007Oo, ++n3, c = c.o\u30070) {
            \u3007o\u3007 = c.\u3007o\u3007;
            \u3007o00\u3007\u3007Oo = c.\u3007o00\u3007\u3007Oo;
            if (\u3007o\u3007 == \u3007o00\u3007\u3007Oo) {
                throw new AssertionError((Object)"s.limit == s.pos");
            }
        }
        this.o\u300700O = new byte[n3][];
        this.O8o08O8O = new int[n3 * 2];
        c c2 = a.o0;
        int n4 = 0;
        int j = n2;
        while (j < n) {
            final byte[][] o\u300700O = this.o\u300700O;
            o\u300700O[n4] = c2.\u3007080;
            final int \u3007o\u30072 = c2.\u3007o\u3007;
            final int \u3007o00\u3007\u3007Oo2 = c2.\u3007o00\u3007\u3007Oo;
            if ((j += \u3007o\u30072 - \u3007o00\u3007\u3007Oo2) > n) {
                j = n;
            }
            final int[] o8o08O8O = this.O8o08O8O;
            o8o08O8O[n4] = j;
            o8o08O8O[o\u300700O.length + n4] = \u3007o00\u3007\u3007Oo2;
            c2.O8 = true;
            ++n4;
            c2 = c2.o\u30070;
        }
    }
    
    private b Oo08() {
        return new b(this.O8());
    }
    
    private int \u3007o\u3007(int binarySearch) {
        binarySearch = Arrays.binarySearch(this.O8o08O8O, 0, this.o\u300700O.length, binarySearch + 1);
        if (binarySearch < 0) {
            binarySearch ^= -1;
        }
        return binarySearch;
    }
    
    public byte[] O8() {
        final int[] o8o08O8O = this.O8o08O8O;
        final byte[][] o\u300700O = this.o\u300700O;
        final byte[] array = new byte[o8o08O8O[o\u300700O.length - 1]];
        final int length = o\u300700O.length;
        int i = 0;
        int n = 0;
        while (i < length) {
            final int[] o8o08O8O2 = this.O8o08O8O;
            final int n2 = o8o08O8O2[length + i];
            final int n3 = o8o08O8O2[i];
            System.arraycopy(this.o\u300700O[i], n2, array, n, n3 - n);
            ++i;
            n = n3;
        }
        return array;
    }
    
    @Override
    public byte a(final int n) {
        b.b.a.a.f.a.q.f.\u3007\u3007888(this.O8o08O8O[this.o\u300700O.length - 1], n, 1L);
        final int \u3007o\u3007 = this.\u3007o\u3007(n);
        int n2;
        if (\u3007o\u3007 == 0) {
            n2 = 0;
        }
        else {
            n2 = this.O8o08O8O[\u3007o\u3007 - 1];
        }
        final int[] o8o08O8O = this.O8o08O8O;
        final byte[][] o\u300700O = this.o\u300700O;
        return o\u300700O[\u3007o\u3007][n - n2 + o8o08O8O[o\u300700O.length + \u3007o\u3007]];
    }
    
    @Override
    public b a(final int n, final int n2) {
        return this.Oo08().a(n, n2);
    }
    
    @Override
    public boolean a(int n, final byte[] array, int n2, int i) {
        if (n >= 0 && n <= this.c() - i && n2 >= 0 && n2 <= array.length - i) {
            final int \u3007o\u3007 = this.\u3007o\u3007(n);
            int n3 = n;
            int n4;
            int min;
            int[] o8o08O8O;
            byte[][] o\u300700O;
            for (n = \u3007o\u3007; i > 0; i -= min, ++n) {
                if (n == 0) {
                    n4 = 0;
                }
                else {
                    n4 = this.O8o08O8O[n - 1];
                }
                min = Math.min(i, this.O8o08O8O[n] - n4 + n4 - n3);
                o8o08O8O = this.O8o08O8O;
                o\u300700O = this.o\u300700O;
                if (!b.b.a.a.f.a.q.f.\u300780\u3007808\u3007O(o\u300700O[n], n3 - n4 + o8o08O8O[o\u300700O.length + n], array, n2, min)) {
                    return false;
                }
                n3 += min;
                n2 += min;
            }
            return true;
        }
        return false;
    }
    
    @Override
    public String b() {
        return this.Oo08().b();
    }
    
    @Override
    public int c() {
        return this.O8o08O8O[this.o\u300700O.length - 1];
    }
    
    @Override
    public String d() {
        return this.Oo08().d();
    }
    
    @Override
    public boolean equals(final Object o) {
        boolean b = true;
        if (o == this) {
            return true;
        }
        if (o instanceof b) {
            final b b2 = (b)o;
            if (b2.c() == this.c() && this.\u3007o00\u3007\u3007Oo(0, b2, 0, this.c())) {
                return b;
            }
        }
        b = false;
        return b;
    }
    
    @Override
    public int hashCode() {
        final int \u3007oOo8\u30070 = super.\u3007OOo8\u30070;
        if (\u3007oOo8\u30070 != 0) {
            return \u3007oOo8\u30070;
        }
        final int length = this.o\u300700O.length;
        int i = 0;
        int n = 0;
        int \u3007oOo8\u30072 = 1;
        while (i < length) {
            final byte[] array = this.o\u300700O[i];
            final int[] o8o08O8O = this.O8o08O8O;
            final int n2 = o8o08O8O[length + i];
            final int n3 = o8o08O8O[i];
            for (int j = n2; j < n3 - n + n2; ++j) {
                \u3007oOo8\u30072 = \u3007oOo8\u30072 * 31 + array[j];
            }
            ++i;
            n = n3;
        }
        return super.\u3007OOo8\u30070 = \u3007oOo8\u30072;
    }
    
    @Override
    public String toString() {
        return this.Oo08().toString();
    }
    
    public boolean \u3007o00\u3007\u3007Oo(int n, final b b, int n2, int i) {
        if (n >= 0 && n <= this.c() - i) {
            int min;
            for (int \u3007o\u3007 = this.\u3007o\u3007(n); i > 0; i -= min, ++\u3007o\u3007) {
                int n3;
                if (\u3007o\u3007 == 0) {
                    n3 = 0;
                }
                else {
                    n3 = this.O8o08O8O[\u3007o\u3007 - 1];
                }
                min = Math.min(i, this.O8o08O8O[\u3007o\u3007] - n3 + n3 - n);
                final int[] o8o08O8O = this.O8o08O8O;
                final byte[][] o\u300700O = this.o\u300700O;
                if (!b.a(n2, o\u300700O[\u3007o\u3007], n - n3 + o8o08O8O[o\u300700O.length + \u3007o\u3007], min)) {
                    return false;
                }
                n += min;
                n2 += min;
            }
            return true;
        }
        return false;
    }
}

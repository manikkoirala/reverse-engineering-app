// 
// Decompiled by Procyon v0.6.0
// 

package b.b.a.a.f.a.p.b;

import android.text.TextUtils;
import b.b.a.a.f.a.k;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.ArrayList;
import b.b.a.a.f.a.o;
import java.io.IOException;
import b.b.a.a.f.a.l;
import java.net.HttpURLConnection;
import b.b.a.a.f.a.n;

public class f extends n
{
    HttpURLConnection o0;
    
    public f(final HttpURLConnection o0, final l l) {
        this.o0 = o0;
    }
    
    @Override
    public String OO0o\u3007\u3007() throws IOException {
        return this.o0.getResponseMessage();
    }
    
    @Override
    public o Oo08() {
        try {
            return new g(this.o0);
        }
        catch (final Exception ex) {
            return null;
        }
    }
    
    @Override
    public void close() {
        try {
            this.Oo08().close();
        }
        catch (final Exception ex) {}
    }
    
    @Override
    public int oO80() {
        try {
            return this.o0.getResponseCode();
        }
        catch (final Exception ex) {
            return -1;
        }
    }
    
    @Override
    public String toString() {
        return "";
    }
    
    @Override
    public b.b.a.a.f.a.f \u30078o8o\u3007() {
        final ArrayList list = new ArrayList();
        for (final Map.Entry<K, List> entry : this.o0.getHeaderFields().entrySet()) {
            for (final String s : entry.getValue()) {
                if ("Content-Range".equalsIgnoreCase((String)entry.getKey()) && this.oO80() == 206) {
                    continue;
                }
                list.add(entry.getKey());
                list.add(s);
            }
        }
        return new b.b.a.a.f.a.f((String[])list.toArray(new String[list.size()]));
    }
    
    @Override
    public long \u3007O00() {
        return 0L;
    }
    
    @Override
    public boolean \u3007O8o08O() {
        return this.oO80() >= 200 && this.oO80() < 300;
    }
    
    @Override
    public long \u3007O\u3007() {
        return 0L;
    }
    
    @Override
    public k \u3007\u3007808\u3007() {
        return k.c;
    }
    
    @Override
    public String \u3007\u3007888(final String s, final String s2) {
        if (!TextUtils.isEmpty((CharSequence)this.\u3007\u30078O0\u30078(s))) {
            return this.\u3007\u30078O0\u30078(s);
        }
        return s2;
    }
    
    public String \u3007\u30078O0\u30078(final String name) {
        return this.o0.getHeaderField(name);
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package b.b.a.a.f.a.p.b;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.io.InputStream;

public class e extends InputStream
{
    InputStream o0;
    HttpURLConnection \u3007OOo8\u30070;
    
    public e(final InputStream o0, final HttpURLConnection \u3007oOo8\u30070) {
        this.o0 = o0;
        this.\u3007OOo8\u30070 = \u3007oOo8\u30070;
    }
    
    @Override
    public int available() throws IOException {
        final InputStream o0 = this.o0;
        if (o0 != null) {
            return o0.available();
        }
        return 0;
    }
    
    @Override
    public void close() throws IOException {
        final InputStream o0 = this.o0;
        if (o0 != null) {
            o0.close();
            this.o0 = null;
        }
        final HttpURLConnection \u3007oOo8\u30070 = this.\u3007OOo8\u30070;
        if (\u3007oOo8\u30070 != null) {
            \u3007oOo8\u30070.disconnect();
            this.\u3007OOo8\u30070 = null;
        }
    }
    
    @Override
    public void mark(final int readlimit) {
        synchronized (this) {
            final InputStream o0 = this.o0;
            if (o0 != null) {
                o0.mark(readlimit);
            }
        }
    }
    
    @Override
    public boolean markSupported() {
        final InputStream o0 = this.o0;
        return o0 != null && o0.markSupported();
    }
    
    @Override
    public int read() throws IOException {
        final InputStream o0 = this.o0;
        if (o0 != null) {
            return o0.read();
        }
        return 0;
    }
    
    @Override
    public int read(final byte[] b) throws IOException {
        final InputStream o0 = this.o0;
        if (o0 != null) {
            return o0.read(b);
        }
        return 0;
    }
    
    @Override
    public int read(final byte[] b, final int off, final int len) throws IOException {
        final InputStream o0 = this.o0;
        if (o0 != null) {
            return o0.read(b, off, len);
        }
        return 0;
    }
    
    @Override
    public void reset() throws IOException {
        final InputStream o0 = this.o0;
        if (o0 != null) {
            o0.reset();
        }
    }
    
    @Override
    public long skip(final long n) throws IOException {
        final InputStream o0 = this.o0;
        if (o0 != null) {
            return o0.skip(n);
        }
        return 0L;
    }
}

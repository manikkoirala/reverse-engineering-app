// 
// Decompiled by Procyon v0.6.0
// 

package b.b.a.a.f.a;

import java.util.Collection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Collections;
import java.util.ArrayList;
import b.b.a.a.f.a.q.f;
import java.nio.charset.Charset;
import b.b.a.a.f.a.q.a;
import java.util.List;

public final class g
{
    private static final char[] oO80;
    final String O8;
    final int Oo08;
    private final List<String> o\u30070;
    final String \u3007080;
    private final String \u3007o00\u3007\u3007Oo;
    private final String \u3007o\u3007;
    private final String \u3007\u3007888;
    
    static {
        oO80 = new char[] { '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F' };
    }
    
    g(final a a) {
        this.\u3007080 = a.\u3007080;
        this.\u3007o00\u3007\u3007Oo = o\u30070(a.\u3007o00\u3007\u3007Oo, false);
        this.\u3007o\u3007 = o\u30070(a.\u3007o\u3007, false);
        this.O8 = a.O8;
        this.Oo08 = a.oO80();
        this.\u3007\u3007888(a.o\u30070, false);
        final List<String> \u3007\u3007888 = a.\u3007\u3007888;
        List<String> \u3007\u3007889;
        if (\u3007\u3007888 != null) {
            \u3007\u3007889 = this.\u3007\u3007888(\u3007\u3007888, true);
        }
        else {
            \u3007\u3007889 = null;
        }
        this.o\u30070 = \u3007\u3007889;
        final String oo80 = a.oO80;
        if (oo80 != null) {
            o\u30070(oo80, false);
        }
        this.\u3007\u3007888 = a.toString();
    }
    
    static String O8(final String s, final int beginIndex, final int endIndex, final boolean b) {
        for (int i = beginIndex; i < endIndex; ++i) {
            final char char1 = s.charAt(i);
            if (char1 == '%' || (char1 == '+' && b)) {
                final b.b.a.a.f.a.q.a a = new b.b.a.a.f.a.q.a();
                a.\u3007O8o08O(s, beginIndex, i);
                \u300780\u3007808\u3007O(a, s, i, endIndex, b);
                return a.\u30078();
            }
        }
        return s.substring(beginIndex, endIndex);
    }
    
    static void OO0o\u3007\u3007\u3007\u30070(final StringBuilder sb, final List<String> list) {
        for (int size = list.size(), i = 0; i < size; i += 2) {
            final String str = list.get(i);
            final String str2 = list.get(i + 1);
            if (i > 0) {
                sb.append('&');
            }
            sb.append(str);
            if (str2 != null) {
                sb.append('=');
                sb.append(str2);
            }
        }
    }
    
    static String Oo08(final String s, final String s2, final boolean b, final boolean b2, final boolean b3, final boolean b4) {
        return \u3007o\u3007(s, 0, s.length(), s2, b, b2, b3, b4, null);
    }
    
    static void Oooo8o0\u3007(final StringBuilder sb, final List<String> list) {
        for (int size = list.size(), i = 0; i < size; ++i) {
            sb.append('/');
            sb.append((String)list.get(i));
        }
    }
    
    static void oO80(final b.b.a.a.f.a.q.a a, final String s, int i, final int n, final String s2, final boolean b, final boolean b2, final boolean b3, final boolean b4, final Charset charset) {
        b.b.a.a.f.a.q.a a2 = null;
        while (i < n) {
            final int codePoint = s.codePointAt(i);
            b.b.a.a.f.a.q.a a3 = null;
            Label_0321: {
                if (b) {
                    a3 = a2;
                    if (codePoint == 9) {
                        break Label_0321;
                    }
                    a3 = a2;
                    if (codePoint == 10) {
                        break Label_0321;
                    }
                    a3 = a2;
                    if (codePoint == 12) {
                        break Label_0321;
                    }
                    if (codePoint == 13) {
                        a3 = a2;
                        break Label_0321;
                    }
                }
                if (codePoint == 43 && b3) {
                    String s3;
                    if (b) {
                        s3 = "+";
                    }
                    else {
                        s3 = "%2B";
                    }
                    a.\u30078o8o\u3007(s3);
                    a3 = a2;
                }
                else {
                    Label_0190: {
                        if (codePoint >= 32 && codePoint != 127 && (codePoint < 128 || !b4) && s2.indexOf(codePoint) == -1) {
                            if (codePoint == 37) {
                                if (!b) {
                                    break Label_0190;
                                }
                                if (b2 && !\u30078o8o\u3007(s, i, n)) {
                                    break Label_0190;
                                }
                            }
                            a.oO(codePoint);
                            a3 = a2;
                            break Label_0321;
                        }
                    }
                    b.b.a.a.f.a.q.a a4;
                    if ((a4 = a2) == null) {
                        a4 = new b.b.a.a.f.a.q.a();
                    }
                    if (charset != null && !charset.equals(f.\u3007080)) {
                        a4.OO0o\u3007\u3007(s, i, Character.charCount(codePoint) + i, charset);
                    }
                    else {
                        a4.oO(codePoint);
                    }
                    while (true) {
                        a3 = a4;
                        if (a4.\u300700\u30078()) {
                            break;
                        }
                        final int n2 = a4.\u3007\u3007\u30070\u3007\u30070() & 0xFF;
                        a.O8\u3007o(37);
                        final char[] oo80 = g.oO80;
                        a.O8\u3007o(oo80[n2 >> 4 & 0xF]);
                        a.O8\u3007o(oo80[n2 & 0xF]);
                    }
                }
            }
            i += Character.charCount(codePoint);
            a2 = a3;
        }
    }
    
    static String o\u30070(final String s, final boolean b) {
        return O8(s, 0, s.length(), b);
    }
    
    public static int \u3007080(final String s) {
        if (s.equals("http")) {
            return 80;
        }
        if (s.equals("https")) {
            return 443;
        }
        return -1;
    }
    
    static void \u300780\u3007808\u3007O(final b.b.a.a.f.a.q.a a, final String s, int i, final int n, final boolean b) {
        while (i < n) {
            final int codePoint = s.codePointAt(i);
            Label_0112: {
                Label_0105: {
                    if (codePoint == 37) {
                        final int index = i + 2;
                        if (index < n) {
                            final int \u3007080 = f.\u3007080(s.charAt(i + 1));
                            final int \u300781 = f.\u3007080(s.charAt(index));
                            if (\u3007080 != -1 && \u300781 != -1) {
                                a.O8\u3007o((\u3007080 << 4) + \u300781);
                                i = index;
                                break Label_0112;
                            }
                            break Label_0105;
                        }
                    }
                    if (codePoint == 43 && b) {
                        a.O8\u3007o(32);
                        break Label_0112;
                    }
                }
                a.oO(codePoint);
            }
            i += Character.charCount(codePoint);
        }
    }
    
    static boolean \u30078o8o\u3007(final String s, final int index, final int n) {
        final int index2 = index + 2;
        if (index2 < n && s.charAt(index) == '%') {
            final boolean b = true;
            if (f.\u3007080(s.charAt(index + 1)) != -1 && f.\u3007080(s.charAt(index2)) != -1) {
                return b;
            }
        }
        return false;
    }
    
    public static g \u3007O8o08O(final String s) {
        final a a = new a();
        g oo08 = null;
        if (a.\u3007080(null, s) == g.a.a.o0) {
            oo08 = a.Oo08();
        }
        return oo08;
    }
    
    static List<String> \u3007O\u3007(final String s) {
        final ArrayList list = new ArrayList();
        int n;
        for (int i = 0; i <= s.length(); i = n + 1) {
            if ((n = s.indexOf(38, i)) == -1) {
                n = s.length();
            }
            final int index = s.indexOf(61, i);
            if (index != -1 && index <= n) {
                list.add(s.substring(i, index));
                list.add(s.substring(index + 1, n));
            }
            else {
                list.add(s.substring(i, n));
                list.add(null);
            }
        }
        return list;
    }
    
    static String \u3007o\u3007(final String s, final int beginIndex, final int endIndex, final String s2, final boolean b, final boolean b2, final boolean b3, final boolean b4, final Charset charset) {
        int codePoint;
        for (int i = beginIndex; i < endIndex; i += Character.charCount(codePoint)) {
            codePoint = s.codePointAt(i);
            if (codePoint < 32 || codePoint == 127 || (codePoint >= 128 && b4) || s2.indexOf(codePoint) != -1 || (codePoint == 37 && (!b || (b2 && !\u30078o8o\u3007(s, i, endIndex)))) || (codePoint == 43 && b3)) {
                final b.b.a.a.f.a.q.a a = new b.b.a.a.f.a.q.a();
                a.\u3007O8o08O(s, beginIndex, i);
                oO80(a, s, i, endIndex, s2, b, b2, b3, b4, charset);
                return a.\u30078();
            }
        }
        return s.substring(beginIndex, endIndex);
    }
    
    private List<String> \u3007\u3007888(final List<String> list, final boolean b) {
        final int size = list.size();
        final ArrayList list2 = new ArrayList(size);
        for (int i = 0; i < size; ++i) {
            final String s = list.get(i);
            String o\u30070;
            if (s != null) {
                o\u30070 = o\u30070(s, b);
            }
            else {
                o\u30070 = null;
            }
            list2.add((Object)o\u30070);
        }
        return Collections.unmodifiableList((List<? extends String>)list2);
    }
    
    public List<String> OO0o\u3007\u3007() {
        int i = this.\u3007\u3007888.indexOf(47, this.\u3007080.length() + 3);
        final String \u3007\u3007888 = this.\u3007\u3007888;
        final int \u3007o\u3007 = f.\u3007o\u3007(\u3007\u3007888, i, \u3007\u3007888.length(), "?#");
        final ArrayList list = new ArrayList();
        while (i < \u3007o\u3007) {
            final int beginIndex = i + 1;
            i = f.\u3007o00\u3007\u3007Oo(this.\u3007\u3007888, beginIndex, \u3007o\u3007, '/');
            list.add(this.\u3007\u3007888.substring(beginIndex, i));
        }
        return list;
    }
    
    @Override
    public boolean equals(final Object o) {
        return o instanceof g && ((g)o).\u3007\u3007888.equals(this.\u3007\u3007888);
    }
    
    @Override
    public int hashCode() {
        return this.\u3007\u3007888.hashCode();
    }
    
    @Override
    public String toString() {
        return this.\u3007\u3007888;
    }
    
    public String \u3007O00() {
        if (this.\u3007o00\u3007\u3007Oo.isEmpty()) {
            return "";
        }
        final int beginIndex = this.\u3007080.length() + 3;
        final String \u3007\u3007888 = this.\u3007\u3007888;
        return this.\u3007\u3007888.substring(beginIndex, f.\u3007o\u3007(\u3007\u3007888, beginIndex, \u3007\u3007888.length(), ":@"));
    }
    
    public String \u3007o00\u3007\u3007Oo() {
        if (this.\u3007o\u3007.isEmpty()) {
            return "";
        }
        return this.\u3007\u3007888.substring(this.\u3007\u3007888.indexOf(58, this.\u3007080.length() + 3) + 1, this.\u3007\u3007888.indexOf(64));
    }
    
    public String \u3007\u3007808\u3007() {
        if (this.o\u30070 == null) {
            return null;
        }
        final int beginIndex = this.\u3007\u3007888.indexOf(63) + 1;
        final String \u3007\u3007888 = this.\u3007\u3007888;
        return this.\u3007\u3007888.substring(beginIndex, f.\u3007o00\u3007\u3007Oo(\u3007\u3007888, beginIndex, \u3007\u3007888.length(), '#'));
    }
    
    public URL \u3007\u30078O0\u30078() {
        try {
            return new URL(this.\u3007\u3007888);
        }
        catch (final MalformedURLException cause) {
            throw new RuntimeException(cause);
        }
    }
    
    public static final class a
    {
        String O8;
        int Oo08;
        String oO80;
        final List<String> o\u30070;
        String \u3007080;
        String \u3007o00\u3007\u3007Oo;
        String \u3007o\u3007;
        List<String> \u3007\u3007888;
        
        public a() {
            this.\u3007o00\u3007\u3007Oo = "";
            this.\u3007o\u3007 = "";
            this.Oo08 = -1;
            (this.o\u30070 = new ArrayList<String>()).add("");
        }
        
        private g.a O8(final String s, final boolean b) {
            int n = 0;
            int i;
            do {
                i = f.\u3007o\u3007(s, n, s.length(), "/\\");
                this.\u3007\u3007888(s, n, i, i < s.length(), b);
                n = ++i;
            } while (i <= s.length());
            return this;
        }
        
        private void OO0o\u3007\u3007() {
            final List<String> o\u30070 = this.o\u30070;
            if (((String)o\u30070.remove(o\u30070.size() - 1)).isEmpty() && !this.o\u30070.isEmpty()) {
                final List<String> o\u30072 = this.o\u30070;
                o\u30072.set(o\u30072.size() - 1, "");
            }
            else {
                this.o\u30070.add("");
            }
        }
        
        private void Oooo8o0\u3007(final String s, int i, final int n) {
            if (i == n) {
                return;
            }
            final char char1 = s.charAt(i);
            while (true) {
                Label_0079: {
                    if (char1 == '/' || char1 == '\\') {
                        this.o\u30070.clear();
                        this.o\u30070.add("");
                        break Label_0079;
                    }
                    final List<String> o\u30070 = this.o\u30070;
                    o\u30070.set(o\u30070.size() - 1, "");
                    while (i < n) {
                        final int \u3007o\u3007 = f.\u3007o\u3007(s, i, n, "/\\");
                        final boolean b = \u3007o\u3007 < n;
                        this.\u3007\u3007888(s, i, \u3007o\u3007, b, true);
                        i = \u3007o\u3007;
                        if (b) {
                            i = \u3007o\u3007;
                            break Label_0079;
                        }
                    }
                    return;
                }
                ++i;
                continue;
            }
        }
        
        private static String o\u30070(final String s, final int n, final int n2) {
            return f.O8(g.O8(s, n, n2, false));
        }
        
        private static int \u300780\u3007808\u3007O(final String s, int int1, final int n) {
            try {
                int1 = Integer.parseInt(g.\u3007o\u3007(s, int1, n, "", false, false, false, true, null));
                if (int1 > 0 && int1 <= 65535) {
                    return int1;
                }
                return -1;
            }
            catch (final NumberFormatException ex) {
                return -1;
            }
        }
        
        private static int \u30078o8o\u3007(final String s, int i, final int n) {
            while (i < n) {
                final char char1 = s.charAt(i);
                if (char1 == ':') {
                    return i;
                }
                int n2 = i;
                int n3 = 0;
                Label_0058: {
                    if (char1 != '[') {
                        n3 = i;
                    }
                    else {
                        do {
                            i = n2 + 1;
                            if ((n3 = i) >= n) {
                                break Label_0058;
                            }
                            n2 = i;
                        } while (s.charAt(i) != ']');
                        n3 = i;
                    }
                }
                i = n3 + 1;
            }
            return n;
        }
        
        private boolean \u3007O00(final String s) {
            return s.equals("..") || s.equalsIgnoreCase("%2e.") || s.equalsIgnoreCase(".%2e") || s.equalsIgnoreCase("%2e%2e");
        }
        
        private static int \u3007O\u3007(final String s, int n, final int n2) {
            if (n2 - n < 2) {
                return -1;
            }
            final char char1 = s.charAt(n);
            while (true) {
                Label_0032: {
                    if (char1 < 'a') {
                        break Label_0032;
                    }
                    int n3 = n;
                    if (char1 > 'z') {
                        break Label_0032;
                    }
                    while (true) {
                        n = n3 + 1;
                        if (n >= n2) {
                            return -1;
                        }
                        final char char2 = s.charAt(n);
                        if (char2 >= 'a') {
                            n3 = n;
                            if (char2 <= 'z') {
                                continue;
                            }
                        }
                        if (char2 >= 'A') {
                            n3 = n;
                            if (char2 <= 'Z') {
                                continue;
                            }
                        }
                        if (char2 >= '0') {
                            n3 = n;
                            if (char2 <= '9') {
                                continue;
                            }
                        }
                        n3 = n;
                        if (char2 == '+') {
                            continue;
                        }
                        n3 = n;
                        if (char2 == '-') {
                            continue;
                        }
                        if (char2 == '.') {
                            n3 = n;
                        }
                        else {
                            if (char2 == ':') {
                                return n;
                            }
                            return -1;
                        }
                    }
                }
                if (char1 >= 'A') {
                    final int n3 = n;
                    if (char1 <= 'Z') {
                        continue;
                    }
                }
                break;
            }
            return -1;
        }
        
        private boolean \u3007\u3007808\u3007(final String s) {
            return s.equals(".") || s.equalsIgnoreCase("%2e");
        }
        
        private void \u3007\u3007888(String \u3007o\u3007, final int n, final int n2, final boolean b, final boolean b2) {
            \u3007o\u3007 = g.\u3007o\u3007(\u3007o\u3007, n, n2, " \"<>^`{}|/\\?#", b2, false, false, true, null);
            if (this.\u3007\u3007808\u3007(\u3007o\u3007)) {
                return;
            }
            if (this.\u3007O00(\u3007o\u3007)) {
                this.OO0o\u3007\u3007();
                return;
            }
            final List<String> o\u30070 = this.o\u30070;
            if (o\u30070.get(o\u30070.size() - 1).isEmpty()) {
                final List<String> o\u30072 = this.o\u30070;
                o\u30072.set(o\u30072.size() - 1, \u3007o\u3007);
            }
            else {
                this.o\u30070.add(\u3007o\u3007);
            }
            if (b) {
                this.o\u30070.add("");
            }
        }
        
        private static int \u3007\u30078O0\u30078(final String s, int i, final int n) {
            int n2 = 0;
            while (i < n) {
                final char char1 = s.charAt(i);
                if (char1 != '\\' && char1 != '/') {
                    break;
                }
                ++n2;
                ++i;
            }
            return n2;
        }
        
        public g.a OO0o\u3007\u3007\u3007\u30070(final String s) {
            List<String> \u3007o\u3007;
            if (s != null) {
                \u3007o\u3007 = g.\u3007O\u3007(g.Oo08(s, " \"'<>#", true, false, true, true));
            }
            else {
                \u3007o\u3007 = null;
            }
            this.\u3007\u3007888 = \u3007o\u3007;
            return this;
        }
        
        public g Oo08() {
            if (this.\u3007080 == null) {
                throw new IllegalStateException("scheme == null");
            }
            if (this.O8 != null) {
                return new g(this);
            }
            throw new IllegalStateException("host == null");
        }
        
        int oO80() {
            int n = this.Oo08;
            if (n == -1) {
                n = g.\u3007080(this.\u3007080);
            }
            return n;
        }
        
        @Override
        public String toString() {
            final StringBuilder sb = new StringBuilder();
            sb.append(this.\u3007080);
            sb.append("://");
            if (!this.\u3007o00\u3007\u3007Oo.isEmpty() || !this.\u3007o\u3007.isEmpty()) {
                sb.append(this.\u3007o00\u3007\u3007Oo);
                if (!this.\u3007o\u3007.isEmpty()) {
                    sb.append(':');
                    sb.append(this.\u3007o\u3007);
                }
                sb.append('@');
            }
            if (this.O8.indexOf(58) != -1) {
                sb.append('[');
                sb.append(this.O8);
                sb.append(']');
            }
            else {
                sb.append(this.O8);
            }
            final int oo80 = this.oO80();
            if (oo80 != g.\u3007080(this.\u3007080)) {
                sb.append(':');
                sb.append(oo80);
            }
            g.Oooo8o0\u3007(sb, this.o\u30070);
            if (this.\u3007\u3007888 != null) {
                sb.append('?');
                g.OO0o\u3007\u3007\u3007\u30070(sb, this.\u3007\u3007888);
            }
            if (this.oO80 != null) {
                sb.append('#');
                sb.append(this.oO80);
            }
            return sb.toString();
        }
        
        g.a.a \u3007080(final g g, final String s) {
            int oo0o\u3007\u3007\u3007\u30070 = f.OO0o\u3007\u3007\u3007\u30070(s, 0, s.length());
            final int \u3007o8o08O = f.\u3007O8o08O(s, oo0o\u3007\u3007\u3007\u30070, s.length());
            if (\u3007O\u3007(s, oo0o\u3007\u3007\u3007\u30070, \u3007o8o08O) != -1) {
                if (s.regionMatches(true, oo0o\u3007\u3007\u3007\u30070, "https:", 0, 6)) {
                    this.\u3007080 = "https";
                    oo0o\u3007\u3007\u3007\u30070 += 6;
                }
                else {
                    if (!s.regionMatches(true, oo0o\u3007\u3007\u3007\u30070, "http:", 0, 5)) {
                        return a.OO;
                    }
                    this.\u3007080 = "http";
                    oo0o\u3007\u3007\u3007\u30070 += 5;
                }
            }
            else {
                if (g == null) {
                    return a.\u3007OOo8\u30070;
                }
                this.\u3007080 = g.\u3007080;
            }
            final int \u3007\u30078O0\u30078 = \u3007\u30078O0\u30078(s, oo0o\u3007\u3007\u3007\u30070, \u3007o8o08O);
            int n = 0;
            Label_0589: {
                if (\u3007\u30078O0\u30078 < 2 && g != null && g.\u3007080.equals(this.\u3007080)) {
                    this.\u3007o00\u3007\u3007Oo = g.\u3007O00();
                    this.\u3007o\u3007 = g.\u3007o00\u3007\u3007Oo();
                    this.O8 = g.O8;
                    this.Oo08 = g.Oo08;
                    this.o\u30070.clear();
                    this.o\u30070.addAll(g.OO0o\u3007\u3007());
                    if (oo0o\u3007\u3007\u3007\u30070 != \u3007o8o08O) {
                        n = oo0o\u3007\u3007\u3007\u30070;
                        if (s.charAt(oo0o\u3007\u3007\u3007\u30070) != '#') {
                            break Label_0589;
                        }
                    }
                    this.OO0o\u3007\u3007\u3007\u30070(g.\u3007\u3007808\u3007());
                    n = oo0o\u3007\u3007\u3007\u30070;
                }
                else {
                    int n2 = oo0o\u3007\u3007\u3007\u30070 + \u3007\u30078O0\u30078;
                    int n3 = 0;
                    int n4 = 0;
                    int \u3007o\u3007;
                    while (true) {
                        \u3007o\u3007 = f.\u3007o\u3007(s, n2, \u3007o8o08O, "@/\\?#");
                        int char1;
                        if (\u3007o\u3007 != \u3007o8o08O) {
                            char1 = s.charAt(\u3007o\u3007);
                        }
                        else {
                            char1 = -1;
                        }
                        if (char1 == -1 || char1 == 35 || char1 == 47 || char1 == 92 || char1 == 63) {
                            break;
                        }
                        if (char1 != 64) {
                            continue;
                        }
                        if (n3 == 0) {
                            final int \u3007o00\u3007\u3007Oo = f.\u3007o00\u3007\u3007Oo(s, n2, \u3007o\u3007, ':');
                            String s2 = g.\u3007o\u3007(s, n2, \u3007o00\u3007\u3007Oo, " \"':;<=>@[]^`{}|/\\?#", true, false, false, true, null);
                            if (n4 != 0) {
                                final StringBuilder sb = new StringBuilder();
                                sb.append(this.\u3007o00\u3007\u3007Oo);
                                sb.append("%40");
                                sb.append(s2);
                                s2 = sb.toString();
                            }
                            this.\u3007o00\u3007\u3007Oo = s2;
                            if (\u3007o00\u3007\u3007Oo != \u3007o\u3007) {
                                this.\u3007o\u3007 = g.\u3007o\u3007(s, \u3007o00\u3007\u3007Oo + 1, \u3007o\u3007, " \"':;<=>@[]^`{}|/\\?#", true, false, false, true, null);
                                n3 = 1;
                            }
                            n4 = 1;
                        }
                        else {
                            final StringBuilder sb2 = new StringBuilder();
                            sb2.append(this.\u3007o\u3007);
                            sb2.append("%40");
                            sb2.append(g.\u3007o\u3007(s, n2, \u3007o\u3007, " \"':;<=>@[]^`{}|/\\?#", true, false, false, true, null));
                            this.\u3007o\u3007 = sb2.toString();
                        }
                        n2 = \u3007o\u3007 + 1;
                    }
                    final int \u30078o8o\u3007 = \u30078o8o\u3007(s, n2, \u3007o\u3007);
                    final int n5 = \u30078o8o\u3007 + 1;
                    if (n5 < \u3007o\u3007) {
                        this.O8 = o\u30070(s, n2, \u30078o8o\u3007);
                        if ((this.Oo08 = \u300780\u3007808\u3007O(s, n5, \u3007o\u3007)) == -1) {
                            return a.\u300708O\u300700\u3007o;
                        }
                    }
                    else {
                        this.O8 = o\u30070(s, n2, \u30078o8o\u3007);
                        this.Oo08 = g.\u3007080(this.\u3007080);
                    }
                    if (this.O8 == null) {
                        return a.o\u300700O;
                    }
                    n = \u3007o\u3007;
                }
            }
            final int \u3007o\u30072 = f.\u3007o\u3007(s, n, \u3007o8o08O, "?#");
            this.Oooo8o0\u3007(s, n, \u3007o\u30072);
            int \u3007o00\u3007\u3007Oo2;
            if ((\u3007o00\u3007\u3007Oo2 = \u3007o\u30072) < \u3007o8o08O) {
                \u3007o00\u3007\u3007Oo2 = \u3007o\u30072;
                if (s.charAt(\u3007o\u30072) == '?') {
                    \u3007o00\u3007\u3007Oo2 = f.\u3007o00\u3007\u3007Oo(s, \u3007o\u30072, \u3007o8o08O, '#');
                    this.\u3007\u3007888 = g.\u3007O\u3007(g.\u3007o\u3007(s, \u3007o\u30072 + 1, \u3007o00\u3007\u3007Oo2, " \"'<>#", true, false, true, true, null));
                }
            }
            if (\u3007o00\u3007\u3007Oo2 < \u3007o8o08O && s.charAt(\u3007o00\u3007\u3007Oo2) == '#') {
                this.oO80 = g.\u3007o\u3007(s, 1 + \u3007o00\u3007\u3007Oo2, \u3007o8o08O, "", true, false, false, false, null);
            }
            return a.o0;
        }
        
        public g.a \u30070\u3007O0088o(final String str) {
            if (str != null) {
                if (str.equalsIgnoreCase("http")) {
                    this.\u3007080 = "http";
                }
                else {
                    if (!str.equalsIgnoreCase("https")) {
                        final StringBuilder sb = new StringBuilder();
                        sb.append("unexpected scheme: ");
                        sb.append(str);
                        throw new IllegalArgumentException(sb.toString());
                    }
                    this.\u3007080 = "https";
                }
                return this;
            }
            throw new NullPointerException("scheme == null");
        }
        
        public g.a \u3007O8o08O(final String str) {
            if (str == null) {
                throw new NullPointerException("host == null");
            }
            final String o\u30070 = o\u30070(str, 0, str.length());
            if (o\u30070 != null) {
                this.O8 = o\u30070;
                return this;
            }
            final StringBuilder sb = new StringBuilder();
            sb.append("unexpected host: ");
            sb.append(str);
            throw new IllegalArgumentException(sb.toString());
        }
        
        public g.a \u3007o00\u3007\u3007Oo(final String s) {
            if (s != null) {
                return this.O8(s, true);
            }
            throw new NullPointerException("encodedPathSegments == null");
        }
        
        public g.a \u3007o\u3007(String oo08, final String s) {
            if (oo08 != null) {
                if (this.\u3007\u3007888 == null) {
                    this.\u3007\u3007888 = new ArrayList<String>();
                }
                this.\u3007\u3007888.add(g.Oo08(oo08, " \"'<>#&=", true, false, true, true));
                final List<String> \u3007\u3007888 = this.\u3007\u3007888;
                if (s != null) {
                    oo08 = g.Oo08(s, " \"'<>#&=", true, false, true, true);
                }
                else {
                    oo08 = null;
                }
                \u3007\u3007888.add(oo08);
                return this;
            }
            throw new NullPointerException("encodedName == null");
        }
        
        enum a
        {
            private static final a[] O8o08O8O;
            
            OO, 
            o0, 
            o\u300700O, 
            \u300708O\u300700\u3007o, 
            \u3007OOo8\u30070;
        }
    }
}

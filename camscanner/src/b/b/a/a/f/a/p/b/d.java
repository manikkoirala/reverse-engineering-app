// 
// Decompiled by Procyon v0.6.0
// 

package b.b.a.a.f.a.p.b;

import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.SynchronousQueue;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.CopyOnWriteArrayList;
import b.b.a.a.f.a.b;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.atomic.AtomicInteger;

public class d extends b.b.a.a.f.a.d
{
    private AtomicInteger O8;
    private ExecutorService \u3007080;
    private List<b> \u3007o00\u3007\u3007Oo;
    private List<b> \u3007o\u3007;
    
    public d() {
        this.\u3007o00\u3007\u3007Oo = new CopyOnWriteArrayList<b>();
        this.\u3007o\u3007 = new CopyOnWriteArrayList<b>();
        this.O8 = new AtomicInteger(64);
        if (this.\u3007080 == null) {
            this.\u3007080 = new ThreadPoolExecutor(0, Integer.MAX_VALUE, 20L, TimeUnit.SECONDS, new SynchronousQueue<Runnable>(), new ThreadFactory(this) {
                @Override
                public Thread newThread(final Runnable target) {
                    final Thread thread = new Thread(target, "systemHttp Dispatcher");
                    thread.setDaemon(false);
                    thread.setPriority(10);
                    return thread;
                }
            });
        }
    }
    
    @Override
    public List<b> O8() {
        return this.\u3007o00\u3007\u3007Oo;
    }
    
    @Override
    public List<b> Oo08() {
        return this.\u3007o\u3007;
    }
    
    @Override
    public int \u3007080() {
        return this.O8.get();
    }
    
    @Override
    public void \u3007o00\u3007\u3007Oo(final int newValue) {
        this.O8.set(newValue);
    }
    
    @Override
    public ExecutorService \u3007o\u3007() {
        return this.\u3007080;
    }
}

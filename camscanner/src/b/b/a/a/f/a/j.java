// 
// Decompiled by Procyon v0.6.0
// 

package b.b.a.a.f.a;

import b.b.a.a.f.a.p.a;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

public abstract class j implements Cloneable
{
    public long O8o08O8O;
    public TimeUnit OO;
    public List<h> o0;
    public TimeUnit o\u300700O;
    public TimeUnit \u3007080OO8\u30070;
    public long \u300708O\u300700\u3007o;
    public long \u3007OOo8\u30070;
    
    public j(final a a) {
        this.\u3007OOo8\u30070 = a.\u3007o00\u3007\u3007Oo;
        this.\u300708O\u300700\u3007o = a.O8;
        this.O8o08O8O = a.o\u30070;
        final List<h> \u3007080 = a.\u3007080;
        this.OO = a.\u3007o\u3007;
        this.o\u300700O = a.Oo08;
        this.\u3007080OO8\u30070 = a.\u3007\u3007888;
        this.o0 = \u3007080;
    }
    
    public abstract b \u3007080(final l p0);
    
    public abstract d \u3007o00\u3007\u3007Oo();
    
    public a \u3007o\u3007() {
        return new a(this);
    }
    
    public static final class a
    {
        public long O8;
        public TimeUnit Oo08;
        public long o\u30070;
        public final List<h> \u3007080;
        public long \u3007o00\u3007\u3007Oo;
        public TimeUnit \u3007o\u3007;
        public TimeUnit \u3007\u3007888;
        
        public a() {
            this.\u3007080 = new ArrayList<h>();
            this.\u3007o00\u3007\u3007Oo = 10000L;
            final TimeUnit milliseconds = TimeUnit.MILLISECONDS;
            this.\u3007o\u3007 = milliseconds;
            this.O8 = 10000L;
            this.Oo08 = milliseconds;
            this.o\u30070 = 10000L;
            this.\u3007\u3007888 = milliseconds;
        }
        
        public a(final j j) {
            this.\u3007080 = new ArrayList<h>();
            this.\u3007o00\u3007\u3007Oo = 10000L;
            final TimeUnit milliseconds = TimeUnit.MILLISECONDS;
            this.\u3007o\u3007 = milliseconds;
            this.O8 = 10000L;
            this.Oo08 = milliseconds;
            this.o\u30070 = 10000L;
            this.\u3007\u3007888 = milliseconds;
            this.\u3007o00\u3007\u3007Oo = j.\u3007OOo8\u30070;
            this.\u3007o\u3007 = j.OO;
            this.O8 = j.\u300708O\u300700\u3007o;
            this.Oo08 = j.o\u300700O;
            this.o\u30070 = j.O8o08O8O;
            this.\u3007\u3007888 = j.\u3007080OO8\u30070;
        }
        
        public a(final String s) {
            this.\u3007080 = new ArrayList<h>();
            this.\u3007o00\u3007\u3007Oo = 10000L;
            final TimeUnit milliseconds = TimeUnit.MILLISECONDS;
            this.\u3007o\u3007 = milliseconds;
            this.O8 = 10000L;
            this.Oo08 = milliseconds;
            this.o\u30070 = 10000L;
            this.\u3007\u3007888 = milliseconds;
        }
        
        public a O8(final long o8, final TimeUnit oo08) {
            this.O8 = o8;
            this.Oo08 = oo08;
            return this;
        }
        
        public a Oo08(final long o\u30070, final TimeUnit \u3007\u3007888) {
            this.o\u30070 = o\u30070;
            this.\u3007\u3007888 = \u3007\u3007888;
            return this;
        }
        
        public a \u3007080(final long \u3007o00\u3007\u3007Oo, final TimeUnit \u3007o\u3007) {
            this.\u3007o00\u3007\u3007Oo = \u3007o00\u3007\u3007Oo;
            this.\u3007o\u3007 = \u3007o\u3007;
            return this;
        }
        
        public a \u3007o00\u3007\u3007Oo(final h h) {
            this.\u3007080.add(h);
            return this;
        }
        
        public j \u3007o\u3007() {
            return b.b.a.a.f.a.p.a.\u3007080(this);
        }
    }
}

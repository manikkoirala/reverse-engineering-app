// 
// Decompiled by Procyon v0.6.0
// 

package b.b.a.a.g;

import java.security.spec.AlgorithmParameterSpec;
import javax.crypto.spec.IvParameterSpec;
import com.bytedance.sdk.component.utils.m;
import android.util.Base64;
import java.security.Key;
import javax.crypto.Cipher;
import android.text.TextUtils;
import javax.crypto.spec.SecretKeySpec;

public class a
{
    private static String \u3007080;
    
    public static byte[] O8(final byte[] array, final int[] array2) {
        if (array != null && array.length != 0 && array2 != null && array2.length != 0) {
            final byte[] array3 = new byte[array.length];
            for (int i = 0; i < array.length; ++i) {
                array3[i] = (byte)(array[i] ^ array2[i % array2.length]);
            }
            return array3;
        }
        return array;
    }
    
    @Deprecated
    public static String Oo08(String encodeToString, final String s) {
        try {
            final SecretKeySpec key = new SecretKeySpec(s.getBytes(), "AES");
            if (TextUtils.isEmpty((CharSequence)a.\u3007080)) {
                a.\u3007080 = \u3007080("AES/CBC/PKCS5Padding");
            }
            final Cipher instance = Cipher.getInstance(a.\u3007080);
            instance.init(1, key);
            encodeToString = Base64.encodeToString(instance.doFinal(encodeToString.getBytes("utf-8")), 0);
            return encodeToString;
        }
        finally {
            final Throwable t;
            m.a(t.getMessage());
            return null;
        }
    }
    
    public static String o\u30070(String encodeToString, final String s, final String s2) {
        final SecretKeySpec key = new SecretKeySpec(s2.getBytes(), "AES");
        try {
            final Cipher instance = Cipher.getInstance("AES/CBC/PKCS5Padding");
            instance.init(1, key, new IvParameterSpec(s.getBytes()));
            encodeToString = Base64.encodeToString(instance.doFinal(encodeToString.getBytes("utf-8")), 0);
            return encodeToString;
        }
        finally {
            final Throwable t;
            m.b(t.getMessage());
            return null;
        }
    }
    
    public static String \u3007080(final String s) {
        final int[] array = new int[s.length()];
        array[array[4] = 6] = (array[5] = 1);
        return new String(O8(s.getBytes(), array));
    }
    
    @Deprecated
    public static String \u3007o00\u3007\u3007Oo(final String s, final String s2) {
        if (TextUtils.isEmpty((CharSequence)s)) {
            return null;
        }
        try {
            final byte[] decode = Base64.decode(s, 0);
            final SecretKeySpec key = new SecretKeySpec(s2.getBytes(), "AES");
            if (TextUtils.isEmpty((CharSequence)a.\u3007080)) {
                a.\u3007080 = \u3007080("AES/CBC/PKCS5Padding");
            }
            final Cipher instance = Cipher.getInstance(a.\u3007080);
            instance.init(2, key);
            return new String(instance.doFinal(decode));
        }
        catch (final Exception ex) {
            ex.printStackTrace();
            return null;
        }
    }
    
    public static String \u3007o\u3007(String s, final String s2, final String s3) {
        if (TextUtils.isEmpty((CharSequence)s)) {
            return null;
        }
        try {
            final byte[] decode = Base64.decode(s, 0);
            final SecretKeySpec key = new SecretKeySpec(s3.getBytes(), "AES");
            final Cipher instance = Cipher.getInstance("AES/CBC/PKCS5Padding");
            instance.init(2, key, new IvParameterSpec(s2.getBytes()));
            s = new String(instance.doFinal(decode));
            return s;
        }
        finally {
            final Throwable t;
            m.b(t.getMessage());
            return null;
        }
    }
}

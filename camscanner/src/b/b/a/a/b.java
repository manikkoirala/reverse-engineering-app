// 
// Decompiled by Procyon v0.6.0
// 

package b.b.a.a;

import java.util.Dictionary;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;
import androidx.annotation.Nullable;
import java.util.Iterator;
import java.util.HashMap;
import java.util.Map;
import android.content.SharedPreferences$Editor;
import java.util.concurrent.CountDownLatch;
import androidx.annotation.RequiresApi;
import android.text.TextUtils;
import org.jetbrains.annotations.NotNull;
import android.content.Context;
import java.util.Properties;
import androidx.annotation.GuardedBy;
import java.io.File;
import android.util.ArrayMap;
import java.util.concurrent.ExecutorService;

public class b
{
    private static ExecutorService OO0o\u3007\u3007;
    private static boolean OO0o\u3007\u3007\u3007\u30070 = false;
    @GuardedBy("TTPropHelper.class")
    private static ArrayMap<String, File> \u30078o8o\u3007;
    private static ArrayMap<File, b> \u3007O8o08O;
    private volatile boolean O8;
    @GuardedBy("mLoadLock")
    private int Oo08;
    private final File oO80;
    @GuardedBy("this")
    private long o\u30070;
    private final Object \u3007080;
    private final File \u300780\u3007808\u3007O;
    private final Object \u3007o00\u3007\u3007Oo;
    @GuardedBy("mLoadLock")
    private Properties \u3007o\u3007;
    @GuardedBy("mWriteLock")
    private long \u3007\u3007888;
    
    private b(final File oo80) {
        final Object \u3007080 = new Object();
        this.\u3007080 = \u3007080;
        this.\u3007o00\u3007\u3007Oo = new Object();
        this.\u3007o\u3007 = new Properties();
        this.O8 = false;
        this.Oo08 = 0;
        this.oO80 = oo80;
        this.\u300780\u3007808\u3007O = o\u30070(oo80);
        synchronized (\u3007080) {
            this.O8 = false;
            monitorexit(\u3007080);
            final ExecutorService oo0o\u3007\u3007 = b.OO0o\u3007\u3007;
            if (oo0o\u3007\u3007 == null) {
                new Thread(this, "TTPropHelper") {
                    final b o0;
                    
                    @Override
                    public void run() {
                        this.o0.\u3007O888o0o();
                    }
                }.start();
            }
            else {
                oo0o\u3007\u3007.execute(new Runnable(this) {
                    final b o0;
                    
                    @Override
                    public void run() {
                        this.o0.\u3007O888o0o();
                    }
                });
            }
        }
    }
    
    @RequiresApi(api = 19)
    public static b O8(@NotNull final Context context, final String s) {
        String child = s;
        if (TextUtils.isEmpty((CharSequence)s)) {
            child = "tt_prop";
        }
        synchronized (b.class) {
            if (b.\u30078o8o\u3007 == null) {
                b.\u30078o8o\u3007 = (ArrayMap<String, File>)new ArrayMap();
            }
            File file;
            if ((file = (File)b.\u30078o8o\u3007.get((Object)child)) == null) {
                file = new File(context.getFilesDir(), child);
                b.\u30078o8o\u3007.put((Object)child, (Object)file);
            }
            monitorexit(b.class);
            synchronized (b.class) {
                if (b.\u3007O8o08O == null) {
                    b.\u3007O8o08O = (ArrayMap<File, b>)new ArrayMap();
                }
                final b b = (b)b.b.a.a.b.\u3007O8o08O.get((Object)file);
                if (b == null) {
                    final b b2 = new b(file);
                    b.b.a.a.b.\u3007O8o08O.put((Object)file, (Object)b2);
                    return b2;
                }
                return b;
            }
        }
    }
    
    private void OO0o\u3007\u3007\u3007\u30070(final e e, final boolean b) {
        final Runnable runnable = new Runnable(this, e, b) {
            final b OO;
            final e o0;
            final boolean \u3007OOo8\u30070;
            
            @Override
            public void run() {
                synchronized (this.OO.\u3007o00\u3007\u3007Oo) {
                    this.OO.\u3007O00(this.o0, this.\u3007OOo8\u30070);
                    monitorexit(this.OO.\u3007o00\u3007\u3007Oo);
                    synchronized (this.OO.\u3007080) {
                        this.OO.Oo08--;
                    }
                }
            }
        };
        if (b) {
            synchronized (this.\u3007080) {
                final boolean b2 = this.Oo08 == 1;
                monitorexit(this.\u3007080);
                if (b2) {
                    runnable.run();
                    return;
                }
            }
        }
        c.\u3007o00\u3007\u3007Oo(runnable, true ^ b);
    }
    
    static File o\u30070(final File file) {
        final StringBuilder sb = new StringBuilder();
        sb.append(file.getPath());
        sb.append(".bak");
        return new File(sb.toString());
    }
    
    @GuardedBy("mWriteLock")
    private void \u3007O00(final e p0, final boolean p1) {
        // 
        // This method could not be decompiled.
        // 
        // Original Bytecode:
        // 
        //     3: ifeq            14
        //     6: invokestatic    java/lang/System.currentTimeMillis:()J
        //     9: lstore          6
        //    11: goto            17
        //    14: lconst_0       
        //    15: lstore          6
        //    17: aload_0        
        //    18: getfield        b/b/a/a/b.oO80:Ljava/io/File;
        //    21: invokevirtual   java/io/File.exists:()Z
        //    24: istore          16
        //    26: getstatic       b/b/a/a/b.OO0o\u3007\u3007\u3007\u30070:Z
        //    29: ifeq            44
        //    32: invokestatic    java/lang/System.currentTimeMillis:()J
        //    35: lstore          8
        //    37: lload           8
        //    39: lstore          4
        //    41: goto            50
        //    44: lconst_0       
        //    45: lstore          8
        //    47: lconst_0       
        //    48: lstore          4
        //    50: lload           4
        //    52: lstore          10
        //    54: iload           16
        //    56: ifeq            238
        //    59: aload_0        
        //    60: getfield        b/b/a/a/b.\u3007\u3007888:J
        //    63: aload_1        
        //    64: getfield        b/b/a/a/b$e.\u3007080:J
        //    67: lcmp           
        //    68: ifge            120
        //    71: iload_2        
        //    72: ifeq            80
        //    75: iconst_1       
        //    76: istore_3       
        //    77: goto            122
        //    80: aload_0        
        //    81: getfield        b/b/a/a/b.\u3007080:Ljava/lang/Object;
        //    84: astore          17
        //    86: aload           17
        //    88: monitorenter   
        //    89: aload_0        
        //    90: getfield        b/b/a/a/b.o\u30070:J
        //    93: aload_1        
        //    94: getfield        b/b/a/a/b$e.\u3007080:J
        //    97: lcmp           
        //    98: ifne            106
        //   101: iconst_1       
        //   102: istore_3       
        //   103: goto            108
        //   106: iconst_0       
        //   107: istore_3       
        //   108: aload           17
        //   110: monitorexit    
        //   111: goto            122
        //   114: astore_1       
        //   115: aload           17
        //   117: monitorexit    
        //   118: aload_1        
        //   119: athrow         
        //   120: iconst_0       
        //   121: istore_3       
        //   122: iload_3        
        //   123: ifne            133
        //   126: aload_1        
        //   127: iconst_0       
        //   128: iconst_1       
        //   129: invokevirtual   b/b/a/a/b$e.\u3007080:(ZZ)V
        //   132: return         
        //   133: aload_0        
        //   134: getfield        b/b/a/a/b.\u300780\u3007808\u3007O:Ljava/io/File;
        //   137: invokevirtual   java/io/File.exists:()Z
        //   140: istore_2       
        //   141: getstatic       b/b/a/a/b.OO0o\u3007\u3007\u3007\u30070:Z
        //   144: ifeq            152
        //   147: invokestatic    java/lang/System.currentTimeMillis:()J
        //   150: lstore          4
        //   152: iload_2        
        //   153: ifne            226
        //   156: lload           4
        //   158: lstore          10
        //   160: aload_0        
        //   161: getfield        b/b/a/a/b.oO80:Ljava/io/File;
        //   164: aload_0        
        //   165: getfield        b/b/a/a/b.\u300780\u3007808\u3007O:Ljava/io/File;
        //   168: invokevirtual   java/io/File.renameTo:(Ljava/io/File;)Z
        //   171: ifne            238
        //   174: new             Ljava/lang/StringBuilder;
        //   177: dup            
        //   178: invokespecial   java/lang/StringBuilder.<init>:()V
        //   181: astore          17
        //   183: aload           17
        //   185: ldc             "Couldn't rename file "
        //   187: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   190: pop            
        //   191: aload           17
        //   193: aload_0        
        //   194: getfield        b/b/a/a/b.oO80:Ljava/io/File;
        //   197: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/Object;)Ljava/lang/StringBuilder;
        //   200: pop            
        //   201: aload           17
        //   203: ldc             " to backup file "
        //   205: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   208: pop            
        //   209: aload           17
        //   211: aload_0        
        //   212: getfield        b/b/a/a/b.\u300780\u3007808\u3007O:Ljava/io/File;
        //   215: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/Object;)Ljava/lang/StringBuilder;
        //   218: pop            
        //   219: aload_1        
        //   220: iconst_0       
        //   221: iconst_0       
        //   222: invokevirtual   b/b/a/a/b$e.\u3007080:(ZZ)V
        //   225: return         
        //   226: aload_0        
        //   227: getfield        b/b/a/a/b.oO80:Ljava/io/File;
        //   230: invokevirtual   java/io/File.delete:()Z
        //   233: pop            
        //   234: lload           4
        //   236: lstore          10
        //   238: aload_0        
        //   239: getfield        b/b/a/a/b.\u3007o00\u3007\u3007Oo:Ljava/lang/Object;
        //   242: astore          21
        //   244: aload           21
        //   246: monitorenter   
        //   247: aconst_null    
        //   248: astore          19
        //   250: aconst_null    
        //   251: astore          20
        //   253: aload           20
        //   255: astore          17
        //   257: new             Ljava/io/FileOutputStream;
        //   260: astore          18
        //   262: aload           20
        //   264: astore          17
        //   266: aload           18
        //   268: aload_0        
        //   269: getfield        b/b/a/a/b.oO80:Ljava/io/File;
        //   272: invokespecial   java/io/FileOutputStream.<init>:(Ljava/io/File;)V
        //   275: getstatic       b/b/a/a/b.OO0o\u3007\u3007\u3007\u30070:Z
        //   278: ifeq            289
        //   281: invokestatic    java/lang/System.currentTimeMillis:()J
        //   284: lstore          4
        //   286: goto            292
        //   289: lconst_0       
        //   290: lstore          4
        //   292: aload_1        
        //   293: getfield        b/b/a/a/b$e.\u3007o00\u3007\u3007Oo:Ljava/util/Properties;
        //   296: aload           18
        //   298: aconst_null    
        //   299: invokevirtual   java/util/Properties.store:(Ljava/io/OutputStream;Ljava/lang/String;)V
        //   302: getstatic       b/b/a/a/b.OO0o\u3007\u3007\u3007\u30070:Z
        //   305: ifeq            375
        //   308: new             Ljava/lang/StringBuilder;
        //   311: astore          17
        //   313: aload           17
        //   315: invokespecial   java/lang/StringBuilder.<init>:()V
        //   318: aload           17
        //   320: ldc             "save: "
        //   322: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   325: pop            
        //   326: aload           17
        //   328: aload_1        
        //   329: getfield        b/b/a/a/b$e.\u3007o00\u3007\u3007Oo:Ljava/util/Properties;
        //   332: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/Object;)Ljava/lang/StringBuilder;
        //   335: pop            
        //   336: new             Ljava/lang/StringBuilder;
        //   339: astore          17
        //   341: aload           17
        //   343: invokespecial   java/lang/StringBuilder.<init>:()V
        //   346: aload           17
        //   348: ldc             "saveToLocal: save to"
        //   350: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   353: pop            
        //   354: aload           17
        //   356: aload_0        
        //   357: getfield        b/b/a/a/b.oO80:Ljava/io/File;
        //   360: invokevirtual   java/io/File.getAbsolutePath:()Ljava/lang/String;
        //   363: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   366: pop            
        //   367: aload           17
        //   369: ldc             "success"
        //   371: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   374: pop            
        //   375: aload           18
        //   377: invokevirtual   java/io/FileOutputStream.close:()V
        //   380: lload           4
        //   382: lstore          12
        //   384: goto            476
        //   387: astore          17
        //   389: aload           17
        //   391: invokevirtual   java/lang/Throwable.getMessage:()Ljava/lang/String;
        //   394: pop            
        //   395: lload           4
        //   397: lstore          12
        //   399: goto            476
        //   402: astore          17
        //   404: aload           18
        //   406: astore          19
        //   408: goto            659
        //   411: astore          17
        //   413: lconst_0       
        //   414: lstore          4
        //   416: goto            433
        //   419: astore          18
        //   421: goto            651
        //   424: astore          17
        //   426: lconst_0       
        //   427: lstore          4
        //   429: aload           19
        //   431: astore          18
        //   433: aload           18
        //   435: astore          17
        //   437: aload_1        
        //   438: iconst_0       
        //   439: iconst_0       
        //   440: invokevirtual   b/b/a/a/b$e.\u3007080:(ZZ)V
        //   443: lload           4
        //   445: lstore          12
        //   447: aload           18
        //   449: ifnull          476
        //   452: aload           18
        //   454: invokevirtual   java/io/FileOutputStream.close:()V
        //   457: lload           4
        //   459: lstore          12
        //   461: goto            476
        //   464: astore          17
        //   466: aload           17
        //   468: invokevirtual   java/lang/Throwable.getMessage:()Ljava/lang/String;
        //   471: pop            
        //   472: lload           4
        //   474: lstore          12
        //   476: aload           21
        //   478: monitorexit    
        //   479: getstatic       b/b/a/a/b.OO0o\u3007\u3007\u3007\u30070:Z
        //   482: ifeq            493
        //   485: invokestatic    java/lang/System.currentTimeMillis:()J
        //   488: lstore          4
        //   490: goto            496
        //   493: lconst_0       
        //   494: lstore          4
        //   496: aload_0        
        //   497: getfield        b/b/a/a/b.\u300780\u3007808\u3007O:Ljava/io/File;
        //   500: invokevirtual   java/io/File.delete:()Z
        //   503: pop            
        //   504: getstatic       b/b/a/a/b.OO0o\u3007\u3007\u3007\u30070:Z
        //   507: ifeq            518
        //   510: invokestatic    java/lang/System.currentTimeMillis:()J
        //   513: lstore          14
        //   515: goto            521
        //   518: lconst_0       
        //   519: lstore          14
        //   521: aload_0        
        //   522: aload_1        
        //   523: getfield        b/b/a/a/b$e.\u3007080:J
        //   526: putfield        b/b/a/a/b.\u3007\u3007888:J
        //   529: aload_1        
        //   530: iconst_1       
        //   531: iconst_1       
        //   532: invokevirtual   b/b/a/a/b$e.\u3007080:(ZZ)V
        //   535: getstatic       b/b/a/a/b.OO0o\u3007\u3007\u3007\u30070:Z
        //   538: ifeq            650
        //   541: new             Ljava/lang/StringBuilder;
        //   544: astore          17
        //   546: aload           17
        //   548: invokespecial   java/lang/StringBuilder.<init>:()V
        //   551: aload           17
        //   553: ldc             "write: "
        //   555: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   558: pop            
        //   559: aload           17
        //   561: lload           8
        //   563: lload           6
        //   565: lsub           
        //   566: invokevirtual   java/lang/StringBuilder.append:(J)Ljava/lang/StringBuilder;
        //   569: pop            
        //   570: aload           17
        //   572: ldc_w           "/"
        //   575: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   578: pop            
        //   579: aload           17
        //   581: lload           10
        //   583: lload           6
        //   585: lsub           
        //   586: invokevirtual   java/lang/StringBuilder.append:(J)Ljava/lang/StringBuilder;
        //   589: pop            
        //   590: aload           17
        //   592: ldc_w           "/"
        //   595: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   598: pop            
        //   599: aload           17
        //   601: lload           12
        //   603: lload           6
        //   605: lsub           
        //   606: invokevirtual   java/lang/StringBuilder.append:(J)Ljava/lang/StringBuilder;
        //   609: pop            
        //   610: aload           17
        //   612: ldc_w           "/"
        //   615: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   618: pop            
        //   619: aload           17
        //   621: lload           4
        //   623: lload           6
        //   625: lsub           
        //   626: invokevirtual   java/lang/StringBuilder.append:(J)Ljava/lang/StringBuilder;
        //   629: pop            
        //   630: aload           17
        //   632: ldc_w           "/"
        //   635: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   638: pop            
        //   639: aload           17
        //   641: lload           14
        //   643: lload           6
        //   645: lsub           
        //   646: invokevirtual   java/lang/StringBuilder.append:(J)Ljava/lang/StringBuilder;
        //   649: pop            
        //   650: return         
        //   651: aload           17
        //   653: astore          19
        //   655: aload           18
        //   657: astore          17
        //   659: aload           19
        //   661: ifnull          680
        //   664: aload           19
        //   666: invokevirtual   java/io/FileOutputStream.close:()V
        //   669: goto            680
        //   672: astore          18
        //   674: aload           18
        //   676: invokevirtual   java/lang/Throwable.getMessage:()Ljava/lang/String;
        //   679: pop            
        //   680: aload           17
        //   682: athrow         
        //   683: astore          17
        //   685: aload           21
        //   687: monitorexit    
        //   688: aload           17
        //   690: athrow         
        //   691: astore          17
        //   693: aload_0        
        //   694: getfield        b/b/a/a/b.oO80:Ljava/io/File;
        //   697: invokevirtual   java/io/File.exists:()Z
        //   700: ifeq            741
        //   703: aload_0        
        //   704: getfield        b/b/a/a/b.oO80:Ljava/io/File;
        //   707: invokevirtual   java/io/File.delete:()Z
        //   710: ifne            741
        //   713: new             Ljava/lang/StringBuilder;
        //   716: dup            
        //   717: invokespecial   java/lang/StringBuilder.<init>:()V
        //   720: astore          17
        //   722: aload           17
        //   724: ldc_w           "Couldn't clean up partially-written file "
        //   727: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   730: pop            
        //   731: aload           17
        //   733: aload_0        
        //   734: getfield        b/b/a/a/b.oO80:Ljava/io/File;
        //   737: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/Object;)Ljava/lang/StringBuilder;
        //   740: pop            
        //   741: aload_1        
        //   742: iconst_0       
        //   743: iconst_0       
        //   744: invokevirtual   b/b/a/a/b$e.\u3007080:(ZZ)V
        //   747: return         
        //   748: astore          17
        //   750: goto            416
        //    Exceptions:
        //  Try           Handler
        //  Start  End    Start  End    Type                 
        //  -----  -----  -----  -----  ---------------------
        //  89     101    114    120    Any
        //  108    111    114    120    Any
        //  115    118    114    120    Any
        //  238    247    691    748    Any
        //  257    262    424    433    Ljava/lang/Exception;
        //  257    262    419    424    Any
        //  266    275    424    433    Ljava/lang/Exception;
        //  266    275    419    424    Any
        //  275    286    411    416    Ljava/lang/Exception;
        //  275    286    402    411    Any
        //  292    375    748    753    Ljava/lang/Exception;
        //  292    375    402    411    Any
        //  375    380    387    402    Any
        //  389    395    683    691    Any
        //  437    443    419    424    Any
        //  452    457    464    476    Any
        //  466    472    683    691    Any
        //  476    479    683    691    Any
        //  479    490    691    748    Any
        //  496    515    691    748    Any
        //  521    650    691    748    Any
        //  664    669    672    680    Any
        //  674    680    683    691    Any
        //  680    683    683    691    Any
        //  685    688    683    691    Any
        //  688    691    691    748    Any
        // 
        // The error that occurred was:
        // 
        // java.lang.UnsupportedOperationException
        //     at java.base/java.util.Collections$1.remove(Collections.java:4820)
        //     at java.base/java.util.AbstractCollection.removeAll(AbstractCollection.java:369)
        //     at com.strobel.decompiler.ast.AstBuilder.convertLocalVariables(AstBuilder.java:3018)
        //     at com.strobel.decompiler.ast.AstBuilder.performStackAnalysis(AstBuilder.java:2501)
        //     at com.strobel.decompiler.ast.AstBuilder.build(AstBuilder.java:108)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:203)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:93)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethodBody(AstBuilder.java:868)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethod(AstBuilder.java:761)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addTypeMembers(AstBuilder.java:638)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeCore(AstBuilder.java:605)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeNoCache(AstBuilder.java:195)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createType(AstBuilder.java:162)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addType(AstBuilder.java:137)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.buildAst(JavaLanguage.java:71)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.decompileType(JavaLanguage.java:59)
        //     at com.strobel.decompiler.DecompilerDriver.decompileType(DecompilerDriver.java:333)
        //     at com.strobel.decompiler.DecompilerDriver.decompileJar(DecompilerDriver.java:254)
        //     at com.strobel.decompiler.DecompilerDriver.main(DecompilerDriver.java:144)
        // 
        throw new IllegalStateException("An error occurred while decompiling this method.");
    }
    
    private void \u3007O\u3007() {
        while (!this.O8) {
            try {
                this.\u3007080.wait();
            }
            catch (final InterruptedException ex) {}
        }
    }
    
    public boolean OO0o\u3007\u3007(final String key) {
        final Object \u3007080 = this.\u3007080;
        monitorenter(\u3007080);
        try {
            try {
                this.\u3007O\u3007();
                final boolean containsKey = this.\u3007o\u3007.containsKey(key);
                monitorexit(\u3007080);
                return containsKey;
            }
            finally {
                monitorexit(\u3007080);
            }
        }
        catch (final NumberFormatException ex) {}
    }
    
    public f OoO8() {
        return new f();
    }
    
    public boolean Oooo8o0\u3007(final String key, final boolean b) {
        if (TextUtils.isEmpty((CharSequence)key)) {
            return b;
        }
        final Object \u3007080 = this.\u3007080;
        monitorenter(\u3007080);
        try {
            try {
                this.\u3007O\u3007();
                final boolean boolean1 = Boolean.parseBoolean(this.\u3007o\u3007.getProperty(key, String.valueOf(b)));
                monitorexit(\u3007080);
                return boolean1;
            }
            finally {
                monitorexit(\u3007080);
            }
        }
        catch (final NumberFormatException ex) {}
    }
    
    public float \u3007080(final String key, final float f) {
        if (TextUtils.isEmpty((CharSequence)key)) {
            return f;
        }
        final Object \u3007080 = this.\u3007080;
        monitorenter(\u3007080);
        try {
            try {
                this.\u3007O\u3007();
                final float float1 = Float.parseFloat(this.\u3007o\u3007.getProperty(key, String.valueOf(f)));
                monitorexit(\u3007080);
                return float1;
            }
            finally {
                monitorexit(\u3007080);
            }
        }
        catch (final NumberFormatException ex) {}
    }
    
    public void \u3007O888o0o() {
        // 
        // This method could not be decompiled.
        // 
        // Original Bytecode:
        // 
        //     1: getfield        b/b/a/a/b.\u3007080:Ljava/lang/Object;
        //     4: astore          5
        //     6: aload           5
        //     8: monitorenter   
        //     9: aload_0        
        //    10: getfield        b/b/a/a/b.O8:Z
        //    13: ifeq            24
        //    16: getstatic       b/b/a/a/b.OO0o\u3007\u3007\u3007\u30070:Z
        //    19: istore_1       
        //    20: aload           5
        //    22: monitorexit    
        //    23: return         
        //    24: aload_0        
        //    25: getfield        b/b/a/a/b.\u300780\u3007808\u3007O:Ljava/io/File;
        //    28: invokevirtual   java/io/File.exists:()Z
        //    31: ifeq            54
        //    34: aload_0        
        //    35: getfield        b/b/a/a/b.oO80:Ljava/io/File;
        //    38: invokevirtual   java/io/File.delete:()Z
        //    41: pop            
        //    42: aload_0        
        //    43: getfield        b/b/a/a/b.\u300780\u3007808\u3007O:Ljava/io/File;
        //    46: aload_0        
        //    47: getfield        b/b/a/a/b.oO80:Ljava/io/File;
        //    50: invokevirtual   java/io/File.renameTo:(Ljava/io/File;)Z
        //    53: pop            
        //    54: getstatic       b/b/a/a/b.OO0o\u3007\u3007\u3007\u30070:Z
        //    57: ifeq            108
        //    60: new             Ljava/lang/StringBuilder;
        //    63: astore_2       
        //    64: aload_2        
        //    65: invokespecial   java/lang/StringBuilder.<init>:()V
        //    68: aload_2        
        //    69: ldc_w           "reload: "
        //    72: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //    75: pop            
        //    76: aload_2        
        //    77: aload_0        
        //    78: getfield        b/b/a/a/b.oO80:Ljava/io/File;
        //    81: invokevirtual   java/io/File.getAbsolutePath:()Ljava/lang/String;
        //    84: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //    87: pop            
        //    88: aload_2        
        //    89: ldc_w           ", exist? "
        //    92: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //    95: pop            
        //    96: aload_2        
        //    97: aload_0        
        //    98: getfield        b/b/a/a/b.oO80:Ljava/io/File;
        //   101: invokevirtual   java/io/File.exists:()Z
        //   104: invokevirtual   java/lang/StringBuilder.append:(Z)Ljava/lang/StringBuilder;
        //   107: pop            
        //   108: aload_0        
        //   109: getfield        b/b/a/a/b.oO80:Ljava/io/File;
        //   112: invokevirtual   java/io/File.exists:()Z
        //   115: ifeq            285
        //   118: new             Ljava/util/Properties;
        //   121: astore          6
        //   123: aload           6
        //   125: invokespecial   java/util/Properties.<init>:()V
        //   128: aconst_null    
        //   129: astore_3       
        //   130: aconst_null    
        //   131: astore          4
        //   133: new             Ljava/io/FileInputStream;
        //   136: astore_2       
        //   137: aload_2        
        //   138: aload_0        
        //   139: getfield        b/b/a/a/b.oO80:Ljava/io/File;
        //   142: invokespecial   java/io/FileInputStream.<init>:(Ljava/io/File;)V
        //   145: aload           6
        //   147: aload_2        
        //   148: invokevirtual   java/util/Properties.load:(Ljava/io/InputStream;)V
        //   151: getstatic       b/b/a/a/b.OO0o\u3007\u3007\u3007\u30070:Z
        //   154: ifeq            203
        //   157: new             Ljava/lang/StringBuilder;
        //   160: astore_3       
        //   161: aload_3        
        //   162: invokespecial   java/lang/StringBuilder.<init>:()V
        //   165: aload_3        
        //   166: ldc_w           "reload: find "
        //   169: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   172: pop            
        //   173: aload_3        
        //   174: aload           6
        //   176: invokevirtual   java/util/Dictionary.size:()I
        //   179: invokevirtual   java/lang/StringBuilder.append:(I)Ljava/lang/StringBuilder;
        //   182: pop            
        //   183: aload_3        
        //   184: ldc_w           " ,items from "
        //   187: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   190: pop            
        //   191: aload_3        
        //   192: aload_0        
        //   193: getfield        b/b/a/a/b.oO80:Ljava/io/File;
        //   196: invokevirtual   java/io/File.getAbsolutePath:()Ljava/lang/String;
        //   199: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   202: pop            
        //   203: aload           6
        //   205: invokevirtual   java/util/Dictionary.isEmpty:()Z
        //   208: ifne            217
        //   211: aload_0        
        //   212: aload           6
        //   214: putfield        b/b/a/a/b.\u3007o\u3007:Ljava/util/Properties;
        //   217: aload_2        
        //   218: invokevirtual   java/io/FileInputStream.close:()V
        //   221: goto            285
        //   224: astore_2       
        //   225: aload_2        
        //   226: invokevirtual   java/lang/Throwable.getMessage:()Ljava/lang/String;
        //   229: pop            
        //   230: goto            285
        //   233: astore_3       
        //   234: aload_2        
        //   235: astore          4
        //   237: aload_3        
        //   238: astore_2       
        //   239: goto            247
        //   242: astore_3       
        //   243: goto            268
        //   246: astore_2       
        //   247: aload           4
        //   249: ifnull          266
        //   252: aload           4
        //   254: invokevirtual   java/io/FileInputStream.close:()V
        //   257: goto            266
        //   260: astore_3       
        //   261: aload_3        
        //   262: invokevirtual   java/lang/Throwable.getMessage:()Ljava/lang/String;
        //   265: pop            
        //   266: aload_2        
        //   267: athrow         
        //   268: aload_2        
        //   269: ifnull          285
        //   272: aload_2        
        //   273: invokevirtual   java/io/FileInputStream.close:()V
        //   276: goto            285
        //   279: astore_2       
        //   280: aload_2        
        //   281: invokevirtual   java/lang/Throwable.getMessage:()Ljava/lang/String;
        //   284: pop            
        //   285: aload_0        
        //   286: iconst_1       
        //   287: putfield        b/b/a/a/b.O8:Z
        //   290: aload_0        
        //   291: getfield        b/b/a/a/b.\u3007080:Ljava/lang/Object;
        //   294: invokevirtual   java/lang/Object.notifyAll:()V
        //   297: aload           5
        //   299: monitorexit    
        //   300: return         
        //   301: astore_2       
        //   302: aload           5
        //   304: monitorexit    
        //   305: aload_2        
        //   306: athrow         
        //   307: astore_2       
        //   308: aload_3        
        //   309: astore_2       
        //   310: goto            268
        //    Exceptions:
        //  Try           Handler
        //  Start  End    Start  End    Type                 
        //  -----  -----  -----  -----  ---------------------
        //  9      23     301    307    Any
        //  24     54     301    307    Any
        //  54     108    301    307    Any
        //  108    128    301    307    Any
        //  133    145    307    313    Ljava/lang/Exception;
        //  133    145    246    247    Any
        //  145    203    242    246    Ljava/lang/Exception;
        //  145    203    233    242    Any
        //  203    217    242    246    Ljava/lang/Exception;
        //  203    217    233    242    Any
        //  217    221    224    233    Any
        //  225    230    301    307    Any
        //  252    257    260    266    Any
        //  261    266    301    307    Any
        //  266    268    301    307    Any
        //  272    276    279    285    Any
        //  280    285    301    307    Any
        //  285    300    301    307    Any
        //  302    305    301    307    Any
        // 
        // The error that occurred was:
        // 
        // java.lang.IndexOutOfBoundsException: Index 160 out of bounds for length 160
        //     at java.base/jdk.internal.util.Preconditions.outOfBounds(Preconditions.java:64)
        //     at java.base/jdk.internal.util.Preconditions.outOfBoundsCheckIndex(Preconditions.java:70)
        //     at java.base/jdk.internal.util.Preconditions.checkIndex(Preconditions.java:266)
        //     at java.base/java.util.Objects.checkIndex(Objects.java:361)
        //     at java.base/java.util.ArrayList.get(ArrayList.java:427)
        //     at com.strobel.decompiler.ast.AstBuilder.convertToAst(AstBuilder.java:3362)
        //     at com.strobel.decompiler.ast.AstBuilder.convertToAst(AstBuilder.java:3476)
        //     at com.strobel.decompiler.ast.AstBuilder.build(AstBuilder.java:112)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:203)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:93)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethodBody(AstBuilder.java:868)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethod(AstBuilder.java:761)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addTypeMembers(AstBuilder.java:638)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeCore(AstBuilder.java:605)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeNoCache(AstBuilder.java:195)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createType(AstBuilder.java:162)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addType(AstBuilder.java:137)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.buildAst(JavaLanguage.java:71)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.decompileType(JavaLanguage.java:59)
        //     at com.strobel.decompiler.DecompilerDriver.decompileType(DecompilerDriver.java:333)
        //     at com.strobel.decompiler.DecompilerDriver.decompileJar(DecompilerDriver.java:254)
        //     at com.strobel.decompiler.DecompilerDriver.main(DecompilerDriver.java:144)
        // 
        throw new IllegalStateException("An error occurred while decompiling this method.");
    }
    
    public int \u3007o00\u3007\u3007Oo(final String key, final int i) {
        if (TextUtils.isEmpty((CharSequence)key)) {
            return i;
        }
        final Object \u3007080 = this.\u3007080;
        monitorenter(\u3007080);
        try {
            try {
                this.\u3007O\u3007();
                final int int1 = Integer.parseInt(this.\u3007o\u3007.getProperty(key, String.valueOf(i)));
                monitorexit(\u3007080);
                return int1;
            }
            finally {
                monitorexit(\u3007080);
            }
        }
        catch (final NumberFormatException ex) {}
    }
    
    public long \u3007o\u3007(final String key, final long l) {
        if (TextUtils.isEmpty((CharSequence)key)) {
            return l;
        }
        final Object \u3007080 = this.\u3007080;
        monitorenter(\u3007080);
        try {
            try {
                this.\u3007O\u3007();
                final long long1 = Long.parseLong(this.\u3007o\u3007.getProperty(key, String.valueOf(l)));
                monitorexit(\u3007080);
                return long1;
            }
            finally {
                monitorexit(\u3007080);
            }
        }
        catch (final NumberFormatException ex) {}
    }
    
    public String \u3007\u3007888(String property, final String defaultValue) {
        if (TextUtils.isEmpty((CharSequence)property)) {
            return defaultValue;
        }
        synchronized (this.\u3007080) {
            this.\u3007O\u3007();
            property = this.\u3007o\u3007.getProperty(property, defaultValue);
            return property;
        }
    }
    
    public static class d
    {
        public static void \u3007080(@NotNull final ExecutorService executorService) {
            b.OO0o\u3007\u3007 = executorService;
        }
    }
    
    private static class e
    {
        @GuardedBy("mWritingToDiskLock")
        volatile boolean O8;
        final long \u3007080;
        final Properties \u3007o00\u3007\u3007Oo;
        final CountDownLatch \u3007o\u3007;
        
        private e(final long \u3007080, final Properties \u3007o00\u3007\u3007Oo) {
            this.\u3007o\u3007 = new CountDownLatch(1);
            this.O8 = false;
            this.\u3007080 = \u3007080;
            this.\u3007o00\u3007\u3007Oo = \u3007o00\u3007\u3007Oo;
        }
        
        void \u3007080(final boolean b, final boolean o8) {
            this.O8 = o8;
            this.\u3007o\u3007.countDown();
        }
    }
    
    public class f implements SharedPreferences$Editor
    {
        final b O8;
        private final Object \u3007080;
        @GuardedBy("mEditorLock")
        private final Map<String, Object> \u3007o00\u3007\u3007Oo;
        @GuardedBy("mEditorLock")
        private boolean \u3007o\u3007;
        
        public f(final b o8) {
            this.O8 = o8;
            this.\u3007080 = new Object();
            this.\u3007o00\u3007\u3007Oo = new HashMap<String, Object>();
            this.\u3007o\u3007 = false;
        }
        
        private e \u300780\u3007808\u3007O() {
            synchronized (this.O8.\u3007080) {
                if (this.O8.Oo08 > 0) {
                    final Properties properties = new Properties();
                    properties.putAll(this.O8.\u3007o\u3007);
                    this.O8.\u3007o\u3007 = properties;
                }
                final Properties \u3007oo\u3007 = this.O8.\u3007o\u3007;
                this.O8.Oo08++;
                synchronized (this.\u3007080) {
                    final boolean \u3007o\u3007 = this.\u3007o\u3007;
                    boolean b = false;
                    if (\u3007o\u3007) {
                        if (!\u3007oo\u3007.isEmpty()) {
                            \u3007oo\u3007.clear();
                            b = true;
                        }
                        else {
                            b = false;
                        }
                        this.\u3007o\u3007 = false;
                    }
                    for (final Map.Entry<String, V> entry : this.\u3007o00\u3007\u3007Oo.entrySet()) {
                        final String s = entry.getKey();
                        final V value = entry.getValue();
                        if (value != this && value != null) {
                            if (\u3007oo\u3007.containsKey(s)) {
                                final Object value2 = ((Dictionary<String, Object>)\u3007oo\u3007).get(s);
                                if (value2 != null && value2.equals(String.valueOf(value))) {
                                    continue;
                                }
                            }
                            ((Dictionary<String, String>)\u3007oo\u3007).put(s, String.valueOf(value));
                        }
                        else {
                            if (!\u3007oo\u3007.containsKey(s)) {
                                continue;
                            }
                            ((Dictionary<String, Object>)\u3007oo\u3007).remove(s);
                        }
                        b = true;
                    }
                    this.\u3007o00\u3007\u3007Oo.clear();
                    if (b) {
                        this.O8.o\u30070 = this.O8.o\u30070;
                    }
                    final long o\u3007O8\u3007\u3007o = this.O8.o\u30070;
                    monitorexit(this.\u3007080);
                    monitorexit(this.O8.\u3007080);
                    return new e(o\u3007O8\u3007\u3007o, \u3007oo\u3007);
                }
            }
        }
        
        public f O8(final String s, final int i) {
            synchronized (this.\u3007080) {
                this.\u3007o00\u3007\u3007Oo.put(s, i);
                return this;
            }
        }
        
        public f Oo08(final String s, final long l) {
            synchronized (this.\u3007080) {
                this.\u3007o00\u3007\u3007Oo.put(s, l);
                return this;
            }
        }
        
        public void apply() {
            this.O8.OO0o\u3007\u3007\u3007\u30070(this.\u300780\u3007808\u3007O(), false);
        }
        
        public boolean commit() {
            long currentTimeMillis;
            if (b.OO0o\u3007\u3007\u3007\u30070) {
                currentTimeMillis = System.currentTimeMillis();
            }
            else {
                currentTimeMillis = 0L;
            }
            final e \u300780\u3007808\u3007O = this.\u300780\u3007808\u3007O();
            this.O8.OO0o\u3007\u3007\u3007\u30070(\u300780\u3007808\u3007O, true);
            try {
                \u300780\u3007808\u3007O.\u3007o\u3007.await();
                if (b.OO0o\u3007\u3007\u3007\u30070) {
                    final StringBuilder sb = new StringBuilder();
                    sb.append(this.O8.oO80.getName());
                    sb.append(":");
                    sb.append(\u300780\u3007808\u3007O.\u3007080);
                    sb.append(" committed after ");
                    sb.append(System.currentTimeMillis() - currentTimeMillis);
                    sb.append(" ms");
                }
                return \u300780\u3007808\u3007O.O8;
            }
            catch (final InterruptedException ex) {
                return false;
            }
            finally {
                if (b.OO0o\u3007\u3007\u3007\u30070) {
                    final StringBuilder sb2 = new StringBuilder();
                    sb2.append(this.O8.oO80.getName());
                    sb2.append(":");
                    sb2.append(\u300780\u3007808\u3007O.\u3007080);
                    sb2.append(" committed after ");
                    sb2.append(System.currentTimeMillis() - currentTimeMillis);
                    sb2.append(" ms");
                }
            }
        }
        
        public f oO80(final String s, final boolean b) {
            synchronized (this.\u3007080) {
                this.\u3007o00\u3007\u3007Oo.put(s, b);
                return this;
            }
        }
        
        public f o\u30070(final String s, @Nullable final String s2) {
            synchronized (this.\u3007080) {
                this.\u3007o00\u3007\u3007Oo.put(s, s2);
                return this;
            }
        }
        
        public f \u3007080() {
            synchronized (this.\u3007080) {
                this.\u3007o\u3007 = true;
                return this;
            }
        }
        
        public f \u3007o00\u3007\u3007Oo(final String s) {
            synchronized (this.\u3007080) {
                this.\u3007o00\u3007\u3007Oo.put(s, this);
                return this;
            }
        }
        
        public f \u3007o\u3007(final String s, final float f) {
            synchronized (this.\u3007080) {
                this.\u3007o00\u3007\u3007Oo.put(s, f);
                return this;
            }
        }
        
        public f \u3007\u3007888(final String s, @Nullable final Set<String> c) {
            synchronized (this.\u3007080) {
                final Map<String, Object> \u3007o00\u3007\u3007Oo = this.\u3007o00\u3007\u3007Oo;
                HashSet set;
                if (c == null) {
                    set = null;
                }
                else {
                    set = new HashSet(c);
                }
                \u3007o00\u3007\u3007Oo.put(s, set);
                return this;
            }
        }
    }
}

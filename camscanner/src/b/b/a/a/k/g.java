// 
// Decompiled by Procyon v0.6.0
// 

package b.b.a.a.k;

public abstract class g implements Runnable, Comparable<g>
{
    private int a;
    private String b;
    
    public g(final String b) {
        this.a = 5;
        this.b = b;
    }
    
    public g(final String b, int a) {
        this.a = 0;
        if (a == 0) {
            a = 5;
        }
        this.a = a;
        this.b = b;
    }
    
    @Override
    public int compareTo(final g g) {
        if (this.getPriority() < g.getPriority()) {
            return 1;
        }
        if (this.getPriority() >= g.getPriority()) {
            return -1;
        }
        return 0;
    }
    
    public String getName() {
        return this.b;
    }
    
    public int getPriority() {
        return this.a;
    }
    
    public void setPriority(final int a) {
        this.a = a;
    }
}

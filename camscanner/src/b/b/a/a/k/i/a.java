// 
// Decompiled by Procyon v0.6.0
// 

package b.b.a.a.k.i;

import android.os.HandlerThread;
import com.bytedance.sdk.component.utils.y$a;
import com.bytedance.sdk.component.utils.y;

public class a
{
    private final d<b.b.a.a.k.i.b> \u3007080;
    private y \u3007o00\u3007\u3007Oo;
    
    private a() {
        this.\u3007080 = (d<b.b.a.a.k.i.b>)d.\u3007o00\u3007\u3007Oo(2);
    }
    
    public static a \u3007080() {
        return b.\u3007080;
    }
    
    private b.b.a.a.k.i.b \u3007o00\u3007\u3007Oo(final y$a y$a, final String s) {
        final HandlerThread handlerThread = new HandlerThread(s);
        ((Thread)handlerThread).start();
        return new b.b.a.a.k.i.b(handlerThread, y$a);
    }
    
    public boolean O8(final y y) {
        if (y instanceof b.b.a.a.k.i.b) {
            final b.b.a.a.k.i.b b = (b.b.a.a.k.i.b)y;
            if (!this.\u3007080.\u3007o\u3007(b)) {
                b.\u3007o\u3007();
            }
            return true;
        }
        return false;
    }
    
    public y Oo08() {
        if (this.\u3007o00\u3007\u3007Oo == null) {
            synchronized (a.class) {
                if (this.\u3007o00\u3007\u3007Oo == null) {
                    this.\u3007o00\u3007\u3007Oo = this.\u3007o\u3007("csj_io_handler");
                }
            }
        }
        return this.\u3007o00\u3007\u3007Oo;
    }
    
    public y o\u30070(final y$a y$a, final String s) {
        final b.b.a.a.k.i.b b = this.\u3007080.\u3007080();
        b.b.a.a.k.i.b \u3007o00\u3007\u3007Oo;
        if (b != null) {
            b.\u3007080(y$a);
            b.\u3007o00\u3007\u3007Oo(s);
            \u3007o00\u3007\u3007Oo = b;
        }
        else {
            \u3007o00\u3007\u3007Oo = this.\u3007o00\u3007\u3007Oo(y$a, s);
        }
        return \u3007o00\u3007\u3007Oo;
    }
    
    public y \u3007o\u3007(final String s) {
        return this.o\u30070(null, s);
    }
    
    private static class b
    {
        private static final a \u3007080;
        
        static {
            \u3007080 = new a(null);
        }
    }
}

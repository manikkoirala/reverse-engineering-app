// 
// Decompiled by Procyon v0.6.0
// 

package b.b.a.a.k.i;

import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.BlockingQueue;

public class d<T extends c>
{
    private int \u3007080;
    private BlockingQueue<T> \u3007o00\u3007\u3007Oo;
    
    private d(final int \u3007080) {
        this.\u3007o00\u3007\u3007Oo = new LinkedBlockingQueue<T>();
        this.\u3007080 = \u3007080;
    }
    
    public static d \u3007o00\u3007\u3007Oo(final int n) {
        return new d(n);
    }
    
    public T \u3007080() {
        return this.\u3007o00\u3007\u3007Oo.poll();
    }
    
    public boolean \u3007o\u3007(final T t) {
        if (t == null) {
            return false;
        }
        t.a();
        return this.\u3007o00\u3007\u3007Oo.size() < this.\u3007080 && this.\u3007o00\u3007\u3007Oo.offer(t);
    }
}

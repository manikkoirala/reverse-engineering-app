// 
// Decompiled by Procyon v0.6.0
// 

package b.b.a.a.k.i;

import android.os.Handler;
import java.lang.ref.WeakReference;
import com.bytedance.sdk.component.utils.y$a;
import android.os.HandlerThread;
import com.bytedance.sdk.component.utils.y;

public class b extends y implements c
{
    private final HandlerThread o0;
    
    b(final HandlerThread o0, final y$a y$a) {
        super(o0.getLooper(), y$a);
        this.o0 = o0;
    }
    
    public void a() {
        ((Handler)this).removeCallbacksAndMessages((Object)null);
        final WeakReference a = super.a;
        if (a != null) {
            a.clear();
            super.a = null;
        }
    }
    
    public void \u3007080(final y$a referent) {
        super.a = new WeakReference((T)referent);
    }
    
    public void \u3007o00\u3007\u3007Oo(final String name) {
        final HandlerThread o0 = this.o0;
        if (o0 != null) {
            ((Thread)o0).setName(name);
        }
    }
    
    public void \u3007o\u3007() {
        final HandlerThread o0 = this.o0;
        if (o0 != null) {
            o0.quit();
        }
    }
}

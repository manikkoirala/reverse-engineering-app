// 
// Decompiled by Procyon v0.6.0
// 

package b.b.a.a.k;

import com.bytedance.sdk.component.utils.m;
import android.os.SystemClock;

class b implements Runnable, Comparable
{
    private long OO;
    private g o0;
    private a \u3007OOo8\u30070;
    
    public b(final g o0, final a \u3007oOo8\u30070) {
        this.OO = 0L;
        this.o0 = o0;
        this.\u3007OOo8\u30070 = \u3007oOo8\u30070;
        this.OO = SystemClock.uptimeMillis();
    }
    
    private void \u3007o00\u3007\u3007Oo(final String str, final String str2, final long lng) {
        final StringBuilder sb = new StringBuilder();
        sb.append("pool is ");
        sb.append(str);
        sb.append("  name is ");
        sb.append(str2);
        sb.append("is timeout,cost ");
        sb.append(lng);
        m.b("DelegateRunnable", sb.toString());
    }
    
    @Override
    public int compareTo(final Object o) {
        if (o instanceof b) {
            return this.o0.compareTo(((b)o).\u3007080());
        }
        return 0;
    }
    
    @Override
    public boolean equals(final Object o) {
        if (o instanceof b) {
            final g o2 = this.o0;
            if (o2 != null && o2.equals(((b)o).\u3007080())) {
                return true;
            }
        }
        return false;
    }
    
    @Override
    public int hashCode() {
        return this.o0.hashCode();
    }
    
    @Override
    public void run() {
        final long uptimeMillis = SystemClock.uptimeMillis();
        final long oo = this.OO;
        Thread.currentThread();
        final g o0 = this.o0;
        if (o0 != null) {
            o0.run();
        }
        final long l = SystemClock.uptimeMillis() - uptimeMillis;
        if (this.\u3007OOo8\u30070 != null) {
            d.\u3007080();
        }
        if (m.b()) {
            int n = 0;
            final a \u3007oOo8\u30070 = this.\u3007OOo8\u30070;
            String s = "null";
            String \u3007080;
            if (\u3007oOo8\u30070 != null) {
                \u3007080 = \u3007oOo8\u30070.\u3007080();
            }
            else {
                \u3007080 = "null";
            }
            final g o2 = this.o0;
            String name;
            if (o2 != null) {
                name = o2.getName();
            }
            else {
                name = "null";
            }
            m.c("DelegateRunnable", new Object[] { "run: pool  = ", \u3007080, " waitTime =", uptimeMillis - oo, " taskCost = ", l, " name=", name });
            final String \u300781 = this.\u3007OOo8\u30070.\u3007080();
            \u300781.hashCode();
            Label_0334: {
                switch (\u300781) {
                    case "computation": {
                        n = 4;
                        break Label_0334;
                    }
                    case "init": {
                        n = 3;
                        break Label_0334;
                    }
                    case "log": {
                        n = 2;
                        break Label_0334;
                    }
                    case "io": {
                        n = 1;
                        break Label_0334;
                    }
                    case "ad": {
                        break Label_0334;
                    }
                    default:
                        break;
                }
                n = -1;
            }
            switch (n) {
                case 4: {
                    if (l > 1000L) {
                        final a \u3007oOo8\u30072 = this.\u3007OOo8\u30070;
                        String \u300782;
                        if (\u3007oOo8\u30072 != null) {
                            \u300782 = \u3007oOo8\u30072.\u3007080();
                        }
                        else {
                            \u300782 = "null";
                        }
                        final g o3 = this.o0;
                        if (o3 != null) {
                            s = o3.getName();
                        }
                        this.\u3007o00\u3007\u3007Oo(\u300782, s, l);
                        break;
                    }
                    break;
                }
                case 2: {
                    if (l > 3000L) {
                        final a \u3007oOo8\u30073 = this.\u3007OOo8\u30070;
                        String \u300783;
                        if (\u3007oOo8\u30073 != null) {
                            \u300783 = \u3007oOo8\u30073.\u3007080();
                        }
                        else {
                            \u300783 = "null";
                        }
                        final g o4 = this.o0;
                        if (o4 != null) {
                            s = o4.getName();
                        }
                        this.\u3007o00\u3007\u3007Oo(\u300783, s, l);
                        break;
                    }
                    break;
                }
                case 1: {
                    if (l > 5000L) {
                        final a \u3007oOo8\u30074 = this.\u3007OOo8\u30070;
                        String \u300784;
                        if (\u3007oOo8\u30074 != null) {
                            \u300784 = \u3007oOo8\u30074.\u3007080();
                        }
                        else {
                            \u300784 = "null";
                        }
                        final g o5 = this.o0;
                        if (o5 != null) {
                            s = o5.getName();
                        }
                        this.\u3007o00\u3007\u3007Oo(\u300784, s, l);
                        break;
                    }
                    break;
                }
                case 0:
                case 3: {
                    if (l > 2000L) {
                        final a \u3007oOo8\u30075 = this.\u3007OOo8\u30070;
                        String \u300785;
                        if (\u3007oOo8\u30075 != null) {
                            \u300785 = \u3007oOo8\u30075.\u3007080();
                        }
                        else {
                            \u300785 = "null";
                        }
                        final g o6 = this.o0;
                        if (o6 != null) {
                            s = o6.getName();
                        }
                        this.\u3007o00\u3007\u3007Oo(\u300785, s, l);
                        break;
                    }
                    break;
                }
            }
        }
    }
    
    public g \u3007080() {
        return this.o0;
    }
}

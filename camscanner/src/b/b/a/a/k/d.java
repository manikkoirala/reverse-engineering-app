// 
// Decompiled by Procyon v0.6.0
// 

package b.b.a.a.k;

import java.util.Iterator;
import b.b.a.a.k.j.a;
import java.util.Map;
import com.bytedance.sdk.component.utils.m;
import java.util.HashMap;
import android.os.Looper;
import android.text.TextUtils;
import java.util.concurrent.atomic.AtomicInteger;

public class d
{
    private static int O8;
    private static int Oo08;
    private static AtomicInteger \u3007080;
    public static final String[] \u3007o00\u3007\u3007Oo;
    public static final String[] \u3007o\u3007;
    
    static {
        d.\u3007080 = new AtomicInteger(0);
        \u3007o00\u3007\u3007Oo = new String[] { "com.bytedance.sdk", "com.bykv.vk", "com.ss", "tt_pangle" };
        \u3007o\u3007 = new String[] { "tt_pangle", "bd_tracker" };
        d.O8 = 0;
        d.Oo08 = 0;
    }
    
    public static void \u3007080() {
        try {
            \u3007o\u3007();
        }
        finally {}
    }
    
    private static boolean \u3007o00\u3007\u3007Oo(final String s, final String[] array) {
        if (!TextUtils.isEmpty((CharSequence)s)) {
            if (array != null) {
                for (int length = array.length, i = 0; i < length; ++i) {
                    if (s.contains(array[i])) {
                        return true;
                    }
                }
            }
        }
        return false;
    }
    
    private static void \u3007o\u3007() {
        final c o800o8O = e.o800o8O();
        if (o800o8O == null) {
            return;
        }
        final int addAndGet = d.\u3007080.addAndGet(1);
        final int \u3007o\u3007 = e.\u3007o\u3007;
        if (\u3007o\u3007 >= 0 && addAndGet % \u3007o\u3007 == 0) {
            if (Looper.getMainLooper() != Looper.myLooper()) {
                final Map<Thread, StackTraceElement[]> allStackTraces = Thread.getAllStackTraces();
                final HashMap hashMap = new HashMap();
                if (allStackTraces == null) {
                    return;
                }
                final boolean b = m.b();
                final int size = allStackTraces.size();
                if (size > d.Oo08) {
                    d.Oo08 = size;
                }
                final Iterator iterator = allStackTraces.entrySet().iterator();
                int n = 0;
                int n2 = 0;
                while (iterator.hasNext()) {
                    final Map.Entry<Thread, V> entry = (Map.Entry<Thread, V>)iterator.next();
                    final int i = n2 + 1;
                    final Thread thread = entry.getKey();
                    final StackTraceElement[] array = (Object)entry.getValue();
                    final StringBuilder sb = new StringBuilder("\n");
                    if (b) {
                        final StringBuilder sb2 = new StringBuilder();
                        sb2.append("Thread Name is : ");
                        sb2.append(thread.getName());
                        sb.append(sb2.toString());
                        sb.append("\n");
                    }
                    final int length = array.length;
                    String str = null;
                    int n3;
                    for (int j = 0; j < length; ++j, n = n3) {
                        final String string = array[j].toString();
                        if (b) {
                            final StringBuilder sb3 = new StringBuilder();
                            sb3.append(string);
                            sb3.append("\n");
                            sb.append(sb3.toString());
                        }
                        if (TextUtils.isEmpty((CharSequence)str)) {
                            if (!\u3007o00\u3007\u3007Oo(string, d.\u3007o00\u3007\u3007Oo)) {
                                n3 = n;
                                if (!\u3007o00\u3007\u3007Oo(thread.getName(), d.\u3007o\u3007)) {
                                    continue;
                                }
                            }
                            n3 = n + 1;
                            str = string;
                        }
                        else {
                            n3 = n;
                        }
                    }
                    if (b) {
                        if (!TextUtils.isEmpty((CharSequence)str)) {
                            final StringBuilder sb4 = new StringBuilder();
                            sb4.append(str);
                            sb4.append("&");
                            sb4.append(thread.getName());
                            final String string2 = sb4.toString();
                            a value = hashMap.get(string2);
                            if (value != null) {
                                value.\u3007o00\u3007\u3007Oo(value.\u3007080() + 1);
                            }
                            else {
                                value = new a(string2, 1, sb.toString(), thread.getName());
                            }
                            hashMap.put(string2, value);
                        }
                        if (!TextUtils.isEmpty((CharSequence)sb.toString())) {
                            final StringBuilder sb5 = new StringBuilder();
                            sb5.append("Thread index = ");
                            sb5.append(i);
                            sb5.append("   &&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&");
                        }
                    }
                    n2 = i;
                }
                if (n > d.O8) {
                    d.O8 = n;
                }
                if (b) {
                    final StringBuilder sb6 = new StringBuilder();
                    sb6.append("SDK current threads=");
                    sb6.append(n);
                    sb6.append(", SDK Max threads=");
                    sb6.append(d.O8);
                    sb6.append(", Application threads = ");
                    sb6.append(size);
                    sb6.append(", Application max threads = ");
                    sb6.append(d.Oo08);
                    final Iterator iterator2 = hashMap.entrySet().iterator();
                    while (iterator2.hasNext()) {
                        ((Map.Entry<K, a>)iterator2.next()).getValue().toString();
                    }
                }
                o800o8O.a(new b.b.a.a.k.j.a(n, d.O8, size, d.Oo08));
            }
        }
    }
    
    public static class a
    {
        public int \u3007080;
        public String \u3007o00\u3007\u3007Oo;
        public String \u3007o\u3007;
        
        public a(final String \u3007o\u3007, final int \u3007080, final String s, final String \u3007o00\u3007\u3007Oo) {
            this.\u3007o\u3007 = \u3007o\u3007;
            this.\u3007080 = \u3007080;
            this.\u3007o00\u3007\u3007Oo = \u3007o00\u3007\u3007Oo;
        }
        
        @Override
        public String toString() {
            final StringBuilder sb = new StringBuilder();
            sb.append("ThreadModel{times=");
            sb.append(this.\u3007080);
            sb.append(", name='");
            sb.append(this.\u3007o00\u3007\u3007Oo);
            sb.append('\'');
            sb.append(", lastStackStack='");
            sb.append(this.\u3007o\u3007);
            sb.append('\'');
            sb.append('}');
            return sb.toString();
        }
        
        public int \u3007080() {
            return this.\u3007080;
        }
        
        public void \u3007o00\u3007\u3007Oo(final int \u3007080) {
            this.\u3007080 = \u3007080;
        }
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package b.b.a.a.k;

import java.util.concurrent.LinkedBlockingQueue;
import java.util.Collections;
import java.util.List;
import android.text.TextUtils;
import com.bytedance.sdk.component.utils.m;
import android.os.Looper;
import java.util.concurrent.RejectedExecutionHandler;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.ThreadPoolExecutor;

class a extends ThreadPoolExecutor
{
    private String o0;
    
    public a(final String o0, final int corePoolSize, final int maximumPoolSize, final long keepAliveTime, final TimeUnit unit, final BlockingQueue<Runnable> workQueue, final ThreadFactory threadFactory, final RejectedExecutionHandler handler) {
        super(corePoolSize, maximumPoolSize, keepAliveTime, unit, workQueue, threadFactory, handler);
        this.o0 = o0;
    }
    
    private void O8(final Runnable runnable, final Throwable t) {
        if (Looper.myLooper() != Looper.getMainLooper()) {
            try {
                runnable.run();
            }
            finally {
                final Throwable t2;
                m.b("", "try exc failed", t2);
            }
        }
    }
    
    private void Oo08(final BlockingQueue<Runnable> blockingQueue, final int corePoolSize) {
        if (this.getCorePoolSize() != corePoolSize && blockingQueue != null && blockingQueue.size() <= 0) {
            try {
                this.setCorePoolSize(corePoolSize);
                m.c("ADThreadPoolExecutor", new Object[] { "reduceCoreThreadSize: reduce poolType =  ", this.o0, " coreSize=", this.getCorePoolSize(), "  maxSize=", this.getMaximumPoolSize() });
            }
            catch (final Exception ex) {
                m.d("ADThreadPoolExecutor", ex.getMessage());
            }
        }
    }
    
    private void o\u30070(final BlockingQueue<Runnable> blockingQueue, final int corePoolSize, final int n) {
        if (this.getCorePoolSize() != corePoolSize && blockingQueue != null && blockingQueue.size() >= n) {
            try {
                this.setCorePoolSize(corePoolSize);
                m.c("ADThreadPoolExecutor", new Object[] { "increaseCoreThreadSize: increase poolType =  ", this.o0, " coreSize=", this.getCorePoolSize(), "  maxSize=", this.getMaximumPoolSize() });
            }
            catch (final Exception ex) {
                m.d("ADThreadPoolExecutor", ex.getMessage());
            }
        }
    }
    
    private void \u3007o00\u3007\u3007Oo(final Runnable command) {
        try {
            super.execute(command);
        }
        catch (final OutOfMemoryError outOfMemoryError) {
            this.\u3007o\u3007(command, outOfMemoryError);
        }
        finally {
            final Throwable t;
            this.O8(command, t);
        }
    }
    
    private void \u3007o\u3007(final Runnable runnable, final OutOfMemoryError outOfMemoryError) {
        this.O8(runnable, outOfMemoryError);
    }
    
    @Override
    protected void afterExecute(final Runnable r, final Throwable t) {
        super.afterExecute(r, t);
        if (e.oo88o8O() && !TextUtils.isEmpty((CharSequence)this.o0)) {
            final BlockingQueue<Runnable> queue = this.getQueue();
            if (queue != null) {
                final String o0 = this.o0;
                o0.hashCode();
                final int hashCode = o0.hashCode();
                int n = -1;
                switch (hashCode) {
                    case 2993840: {
                        if (!o0.equals("aidl")) {
                            break;
                        }
                        n = 2;
                        break;
                    }
                    case 107332: {
                        if (!o0.equals("log")) {
                            break;
                        }
                        n = 1;
                        break;
                    }
                    case 3366: {
                        if (!o0.equals("io")) {
                            break;
                        }
                        n = 0;
                        break;
                    }
                }
                switch (n) {
                    case 2: {
                        this.Oo08(queue, 2);
                        break;
                    }
                    case 1: {
                        this.Oo08(queue, 4);
                        break;
                    }
                    case 0: {
                        this.Oo08(queue, 2);
                        break;
                    }
                }
            }
        }
    }
    
    @Override
    public void execute(final Runnable runnable) {
        if (runnable instanceof g) {
            this.\u3007o00\u3007\u3007Oo(new b.b.a.a.k.b((g)runnable, this));
        }
        else {
            this.\u3007o00\u3007\u3007Oo(new b.b.a.a.k.b(new g(this, "unknown", runnable) {
                final Runnable o0;
                
                @Override
                public void run() {
                    this.o0.run();
                }
            }, this));
        }
        if (e.oo88o8O() && !TextUtils.isEmpty((CharSequence)this.o0)) {
            final BlockingQueue<Runnable> queue = this.getQueue();
            if (queue != null) {
                final String o0 = this.o0;
                o0.hashCode();
                final int hashCode = o0.hashCode();
                int n = -1;
                switch (hashCode) {
                    case 2993840: {
                        if (!o0.equals("aidl")) {
                            break;
                        }
                        n = 2;
                        break;
                    }
                    case 107332: {
                        if (!o0.equals("log")) {
                            break;
                        }
                        n = 1;
                        break;
                    }
                    case 3366: {
                        if (!o0.equals("io")) {
                            break;
                        }
                        n = 0;
                        break;
                    }
                }
                switch (n) {
                    case 2: {
                        this.o\u30070(queue, 5, 5);
                        break;
                    }
                    case 1: {
                        this.o\u30070(queue, 8, 8);
                        break;
                    }
                    case 0: {
                        this.o\u30070(queue, e.\u3007080 + 2, this.getCorePoolSize() * 2);
                        break;
                    }
                }
            }
        }
    }
    
    @Override
    public void shutdown() {
        if (!"io".equals(this.o0)) {
            if (!"aidl".equals(this.o0)) {
                super.shutdown();
            }
        }
    }
    
    @Override
    public List<Runnable> shutdownNow() {
        if (!"io".equals(this.o0) && !"aidl".equals(this.o0)) {
            return super.shutdownNow();
        }
        return Collections.emptyList();
    }
    
    public String \u3007080() {
        return this.o0;
    }
    
    public static class b
    {
        private TimeUnit O8;
        private int Oo08;
        private RejectedExecutionHandler oO80;
        private BlockingQueue<Runnable> o\u30070;
        private String \u3007080;
        private int \u300780\u3007808\u3007O;
        private int \u3007o00\u3007\u3007Oo;
        private long \u3007o\u3007;
        private ThreadFactory \u3007\u3007888;
        
        public b() {
            this.\u3007080 = "io";
            this.\u3007o00\u3007\u3007Oo = 1;
            this.\u3007o\u3007 = 30L;
            this.O8 = TimeUnit.SECONDS;
            this.Oo08 = Integer.MAX_VALUE;
            this.o\u30070 = null;
            this.\u3007\u3007888 = null;
            this.\u300780\u3007808\u3007O = 5;
        }
        
        public b O8(final BlockingQueue<Runnable> o\u30070) {
            this.o\u30070 = o\u30070;
            return this;
        }
        
        public b Oo08(final RejectedExecutionHandler oo80) {
            this.oO80 = oo80;
            return this;
        }
        
        public b oO80(final int \u300780\u3007808\u3007O) {
            this.\u300780\u3007808\u3007O = \u300780\u3007808\u3007O;
            return this;
        }
        
        public b o\u30070(final TimeUnit o8) {
            this.O8 = o8;
            return this;
        }
        
        public b \u3007080(final int \u3007o00\u3007\u3007Oo) {
            this.\u3007o00\u3007\u3007Oo = \u3007o00\u3007\u3007Oo;
            return this;
        }
        
        public b \u3007o00\u3007\u3007Oo(final long \u3007o\u3007) {
            this.\u3007o\u3007 = \u3007o\u3007;
            return this;
        }
        
        public b \u3007o\u3007(final String \u3007080) {
            this.\u3007080 = \u3007080;
            return this;
        }
        
        public a \u3007\u3007888() {
            if (this.\u3007\u3007888 == null) {
                this.\u3007\u3007888 = new h(this.\u300780\u3007808\u3007O, this.\u3007080);
            }
            if (this.oO80 == null) {
                this.oO80 = e.\u300780\u3007808\u3007O();
            }
            if (this.o\u30070 == null) {
                this.o\u30070 = new LinkedBlockingQueue<Runnable>();
            }
            return new a(this.\u3007080, this.\u3007o00\u3007\u3007Oo, this.Oo08, this.\u3007o\u3007, this.O8, this.o\u30070, this.\u3007\u3007888, this.oO80);
        }
    }
}

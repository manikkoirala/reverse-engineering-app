// 
// Decompiled by Procyon v0.6.0
// 

package b.b.a.a.k;

import java.util.concurrent.SynchronousQueue;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.Executors;
import java.util.concurrent.RejectedExecutionHandler;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.PriorityBlockingQueue;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ThreadPoolExecutor;

public class e
{
    private static volatile ThreadPoolExecutor O8;
    private static volatile ScheduledExecutorService OO0o\u3007\u3007\u3007\u30070;
    private static volatile ThreadPoolExecutor Oo08;
    private static volatile ThreadPoolExecutor oO80;
    private static volatile ThreadPoolExecutor o\u30070;
    public static final int \u3007080;
    private static volatile ThreadPoolExecutor \u300780\u3007808\u3007O;
    public static boolean \u30078o8o\u3007;
    public static c \u3007o00\u3007\u3007Oo;
    public static int \u3007o\u3007;
    private static volatile ThreadPoolExecutor \u3007\u3007888;
    
    static {
        \u3007080 = Runtime.getRuntime().availableProcessors();
        e.\u3007o\u3007 = 120;
        e.\u30078o8o\u3007 = true;
    }
    
    public static void O8(final g command) {
        if (e.o\u30070 == null) {
            \u3007o00\u3007\u3007Oo(5);
        }
        if (command != null && e.o\u30070 != null) {
            e.o\u30070.execute(command);
        }
    }
    
    public static void OO0o\u3007\u3007(final int \u3007o\u3007) {
        e.\u3007o\u3007 = \u3007o\u3007;
    }
    
    public static void OO0o\u3007\u3007\u3007\u30070(final g command) {
        if (e.\u300780\u3007808\u3007O == null) {
            \u3007080();
        }
        if (command != null && e.\u300780\u3007808\u3007O != null) {
            e.\u300780\u3007808\u3007O.execute(command);
        }
    }
    
    public static void Oo08(final g g, final int priority) {
        if (g != null) {
            g.setPriority(priority);
        }
        OO0o\u3007\u3007\u3007\u30070(g);
    }
    
    public static ExecutorService OoO8() {
        if (e.\u3007\u3007888 == null) {
            synchronized (e.class) {
                if (e.\u3007\u3007888 == null) {
                    (e.\u3007\u3007888 = new a.b().\u3007o\u3007("log").oO80(10).\u3007080(4).\u3007o00\u3007\u3007Oo(20L).o\u30070(TimeUnit.SECONDS).O8(new PriorityBlockingQueue<Runnable>()).Oo08(\u300780\u3007808\u3007O()).\u3007\u3007888()).allowCoreThreadTimeOut(true);
                }
            }
        }
        return e.\u3007\u3007888;
    }
    
    public static void Oooo8o0\u3007(final g command) {
        if (e.Oo08 == null) {
            \u3007O\u3007();
        }
        if (e.Oo08 != null) {
            e.Oo08.execute(command);
        }
    }
    
    public static c o800o8O() {
        return e.\u3007o00\u3007\u3007Oo;
    }
    
    public static ExecutorService oO80(final int n) {
        if (e.Oo08 == null) {
            synchronized (e.class) {
                if (e.Oo08 == null) {
                    (e.Oo08 = new a.b().\u3007o\u3007("io").\u3007080(2).oO80(n).\u3007o00\u3007\u3007Oo(20L).o\u30070(TimeUnit.SECONDS).O8(new LinkedBlockingQueue<Runnable>()).Oo08(\u300780\u3007808\u3007O()).\u3007\u3007888()).allowCoreThreadTimeOut(true);
                }
            }
        }
        return e.Oo08;
    }
    
    public static boolean oo88o8O() {
        return e.\u30078o8o\u3007;
    }
    
    public static void o\u30070(final g command, final int priority, final int n) {
        if (e.Oo08 == null) {
            oO80(n);
        }
        if (command != null && e.Oo08 != null) {
            command.setPriority(priority);
            e.Oo08.execute(command);
        }
    }
    
    public static ExecutorService \u3007080() {
        if (e.\u300780\u3007808\u3007O == null) {
            synchronized (e.class) {
                if (e.\u300780\u3007808\u3007O == null) {
                    (e.\u300780\u3007808\u3007O = new a.b().\u3007o\u3007("aidl").oO80(10).\u3007080(2).\u3007o00\u3007\u3007Oo(30L).o\u30070(TimeUnit.SECONDS).O8(new PriorityBlockingQueue<Runnable>()).Oo08(\u300780\u3007808\u3007O()).\u3007\u3007888()).allowCoreThreadTimeOut(true);
                }
            }
        }
        return e.\u300780\u3007808\u3007O;
    }
    
    public static void \u30070\u3007O0088o(final g command) {
        if (e.\u3007\u3007888 == null) {
            OoO8();
        }
        if (command != null && e.\u3007\u3007888 != null) {
            e.\u3007\u3007888.execute(command);
        }
    }
    
    public static RejectedExecutionHandler \u300780\u3007808\u3007O() {
        return new RejectedExecutionHandler() {
            @Override
            public void rejectedExecution(final Runnable runnable, final ThreadPoolExecutor threadPoolExecutor) {
            }
        };
    }
    
    public static void \u30078o8o\u3007(final g g, final int n) {
        Oooo8o0\u3007(g);
    }
    
    public static void \u3007O00(final g command) {
        if (e.O8 == null) {
            \u3007\u30078O0\u30078();
        }
        if (command != null && e.O8 != null) {
            e.O8.execute(command);
        }
    }
    
    public static ScheduledExecutorService \u3007O888o0o() {
        if (e.OO0o\u3007\u3007\u3007\u30070 == null) {
            synchronized (e.class) {
                if (e.OO0o\u3007\u3007\u3007\u30070 == null) {
                    e.OO0o\u3007\u3007\u3007\u30070 = Executors.newSingleThreadScheduledExecutor(new h(5, "scheduled"));
                }
            }
        }
        return e.OO0o\u3007\u3007\u3007\u30070;
    }
    
    public static ExecutorService \u3007O8o08O() {
        if (e.oO80 == null) {
            synchronized (e.class) {
                if (e.oO80 == null) {
                    (e.oO80 = new a.b().\u3007o\u3007("computation").\u3007080(3).oO80(10).\u3007o00\u3007\u3007Oo(20L).o\u30070(TimeUnit.SECONDS).O8(new PriorityBlockingQueue<Runnable>()).Oo08(\u300780\u3007808\u3007O()).\u3007\u3007888()).allowCoreThreadTimeOut(true);
                }
            }
        }
        return e.oO80;
    }
    
    public static ExecutorService \u3007O\u3007() {
        return oO80(10);
    }
    
    public static ExecutorService \u3007o00\u3007\u3007Oo(final int n) {
        if (e.o\u30070 == null) {
            synchronized (e.class) {
                if (e.o\u30070 == null) {
                    (e.o\u30070 = new a.b().\u3007o\u3007("ad").\u3007080(2).oO80(n).\u3007o00\u3007\u3007Oo(20L).o\u30070(TimeUnit.SECONDS).O8(new LinkedBlockingQueue<Runnable>()).Oo08(\u300780\u3007808\u3007O()).\u3007\u3007888()).allowCoreThreadTimeOut(true);
                }
            }
        }
        return e.o\u30070;
    }
    
    public static void \u3007o\u3007(final c \u3007o00\u3007\u3007Oo) {
        e.\u3007o00\u3007\u3007Oo = \u3007o00\u3007\u3007Oo;
    }
    
    public static void \u3007\u3007808\u3007(final g g, final int priority) {
        if (g != null) {
            g.setPriority(priority);
        }
        \u30070\u3007O0088o(g);
    }
    
    public static void \u3007\u3007888(final boolean \u30078o8o\u3007) {
        e.\u30078o8o\u3007 = \u30078o8o\u3007;
    }
    
    public static ExecutorService \u3007\u30078O0\u30078() {
        if (e.O8 == null) {
            synchronized (e.class) {
                if (e.O8 == null) {
                    e.O8 = new a.b().\u3007o\u3007("init").\u3007080(0).oO80(10).\u3007o00\u3007\u3007Oo(5L).o\u30070(TimeUnit.SECONDS).O8(new SynchronousQueue<Runnable>()).Oo08(\u300780\u3007808\u3007O()).\u3007\u3007888();
                }
            }
        }
        return e.O8;
    }
}

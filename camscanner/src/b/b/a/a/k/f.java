// 
// Decompiled by Procyon v0.6.0
// 

package b.b.a.a.k;

import java.util.concurrent.Callable;
import java.util.concurrent.FutureTask;

public class f<V> extends FutureTask<V> implements Comparable<f<V>>
{
    private int o0;
    
    public f(final Runnable runnable, final V result, int o0, final int n) {
        super(runnable, result);
        if (o0 == -1) {
            o0 = 5;
        }
        this.o0 = o0;
    }
    
    public f(final Callable<V> callable, int o0, final int n) {
        super(callable);
        if (o0 == -1) {
            o0 = 5;
        }
        this.o0 = o0;
    }
    
    public int \u3007080() {
        return this.o0;
    }
    
    public int \u3007o00\u3007\u3007Oo(final f f) {
        if (this.\u3007080() < f.\u3007080()) {
            return 1;
        }
        if (this.\u3007080() > f.\u3007080()) {
            return -1;
        }
        return 0;
    }
}

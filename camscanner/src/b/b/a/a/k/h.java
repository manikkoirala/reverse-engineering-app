// 
// Decompiled by Procyon v0.6.0
// 

package b.b.a.a.k;

import java.util.concurrent.ThreadFactory;

public class h implements ThreadFactory
{
    private final ThreadGroup \u3007080;
    private final String \u3007o00\u3007\u3007Oo;
    private int \u3007o\u3007;
    
    public h(final int \u3007o\u3007, final String s) {
        this.\u3007o\u3007 = \u3007o\u3007;
        final StringBuilder sb = new StringBuilder();
        sb.append("csj_g_");
        sb.append(s);
        this.\u3007080 = new ThreadGroup(sb.toString());
        final StringBuilder sb2 = new StringBuilder();
        sb2.append("csj_");
        sb2.append(s);
        this.\u3007o00\u3007\u3007Oo = sb2.toString();
    }
    
    @Override
    public Thread newThread(final Runnable target) {
        final Thread thread = new Thread(this.\u3007080, target, this.\u3007o00\u3007\u3007Oo);
        if (thread.isDaemon()) {
            thread.setDaemon(false);
        }
        final int \u3007o\u3007 = this.\u3007o\u3007;
        if (\u3007o\u3007 > 10 || \u3007o\u3007 < 1) {
            this.\u3007o\u3007 = 5;
        }
        thread.setPriority(this.\u3007o\u3007);
        return thread;
    }
}

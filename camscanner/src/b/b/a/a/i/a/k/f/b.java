// 
// Decompiled by Procyon v0.6.0
// 

package b.b.a.a.i.a.k.f;

import b.b.a.a.i.a.d;
import java.util.ArrayList;
import android.database.Cursor;
import android.content.ContentValues;
import android.text.TextUtils;
import b.b.a.a.i.a.m.a;
import android.content.Context;
import java.util.Iterator;
import b.b.a.a.i.a.j.g.f;
import java.util.List;
import android.net.Uri;
import b.b.a.a.i.a.i;
import b.b.a.a.i.a.l.c;
import b.b.a.a.i.a.g;

public class b
{
    public static g \u3007080;
    
    public static void OO0o\u3007\u3007() {
        c.\u3007080("EventProviderImpl#start");
        if (i.\u3007\u30078O0\u30078().OO0o\u3007\u3007() == null) {
            return;
        }
        try {
            final g oo08 = Oo08(i.\u3007\u30078O0\u30078().OO0o\u3007\u3007());
            if (oo08 != null) {
                final StringBuilder sb = new StringBuilder();
                sb.append(o\u30070());
                sb.append("adLogStart");
                final Uri parse = Uri.parse(sb.toString());
                c.\u3007080("EventProviderImpl#gettype");
                oo08.a(parse);
            }
        }
        finally {}
    }
    
    public static void OO0o\u3007\u3007\u3007\u30070(final String obj, final List<String> list, final boolean b) {
        if (list == null) {
            return;
        }
        if (list.isEmpty()) {
            return;
        }
        try {
            final StringBuilder sb = new StringBuilder();
            final Iterator iterator = list.iterator();
            while (iterator.hasNext()) {
                sb.append(f.\u3007o00\u3007\u3007Oo((String)iterator.next()));
                sb.append(",");
            }
            final String \u3007o00\u3007\u3007Oo = f.\u3007o00\u3007\u3007Oo(sb.toString());
            final StringBuilder sb2 = new StringBuilder();
            sb2.append("?did=");
            sb2.append(String.valueOf(obj));
            sb2.append("&track=");
            sb2.append(String.valueOf(\u3007o00\u3007\u3007Oo));
            sb2.append("&replace=");
            sb2.append(String.valueOf(b));
            final String string = sb2.toString();
            final g oo08 = Oo08(i.\u3007\u30078O0\u30078().OO0o\u3007\u3007());
            if (oo08 != null) {
                final StringBuilder sb3 = new StringBuilder();
                sb3.append(o\u30070());
                sb3.append("trackAdUrl");
                sb3.append(string);
                oo08.a(Uri.parse(sb3.toString()));
            }
        }
        finally {}
    }
    
    public static g Oo08(final Context context) {
        try {
            c.\u3007080("getResolver");
            if (b.\u3007080 == null) {
                b.\u3007080 = i.\u3007\u30078O0\u30078().\u3007080().m();
            }
            return b.\u3007080;
        }
        catch (final Exception ex) {
            return b.\u3007080;
        }
    }
    
    public static void Oooo8o0\u3007() {
        if (i.\u3007\u30078O0\u30078().OO0o\u3007\u3007() == null) {
            return;
        }
        try {
            final g oo08 = Oo08(i.\u3007\u30078O0\u30078().OO0o\u3007\u3007());
            if (oo08 != null) {
                final StringBuilder sb = new StringBuilder();
                sb.append(o\u30070());
                sb.append("adLogStop");
                oo08.a(Uri.parse(sb.toString()));
            }
        }
        finally {}
    }
    
    public static void oO80(final a a) {
        if (a == null) {
            return;
        }
        try {
            c.\u3007080("dispatch event getResolver before");
            final g oo08 = Oo08(i.\u3007\u30078O0\u30078().OO0o\u3007\u3007());
            c.\u3007080("dispatch event getResolver end");
            if (oo08 != null) {
                final String \u3007o00\u3007\u3007Oo = f.\u3007o00\u3007\u3007Oo(a.h());
                final StringBuilder sb = new StringBuilder();
                sb.append(o\u30070());
                sb.append("adLogDispatch");
                sb.append("?event=");
                sb.append(\u3007o00\u3007\u3007Oo);
                final Uri parse = Uri.parse(sb.toString());
                c.\u3007080("dispatch event getType:");
                oo08.a(parse);
                c.\u3007080("dispatch event getType end ");
            }
        }
        finally {
            final StringBuilder sb2 = new StringBuilder();
            sb2.append("dispatch event Throwable:");
            final Throwable t;
            sb2.append(t.toString());
            c.\u3007o\u3007(sb2.toString());
        }
    }
    
    private static String o\u30070() {
        final StringBuilder sb = new StringBuilder();
        sb.append(b.b.a.a.i.a.j.g.g.\u3007o00\u3007\u3007Oo);
        sb.append("/");
        sb.append("ad_log_event");
        sb.append("/");
        return sb.toString();
    }
    
    public static void \u300780\u3007808\u3007O(final String obj) {
        if (i.\u3007\u30078O0\u30078().\u3007080().d() == 0 && TextUtils.isEmpty((CharSequence)obj)) {
            return;
        }
        try {
            final g oo08 = Oo08(i.\u3007\u30078O0\u30078().OO0o\u3007\u3007());
            if (oo08 != null) {
                final StringBuilder sb = new StringBuilder();
                sb.append(o\u30070());
                sb.append("trackAdFailed");
                sb.append("?did=");
                sb.append(String.valueOf(obj));
                oo08.a(Uri.parse(sb.toString()));
            }
        }
        finally {}
    }
    
    public Uri O8(final Uri uri, final ContentValues contentValues) {
        return null;
    }
    
    public int \u3007080(final Uri uri, final ContentValues contentValues, final String s, final String[] array) {
        return 0;
    }
    
    public String \u30078o8o\u3007() {
        return "ad_log_event";
    }
    
    public void \u3007O8o08O() {
    }
    
    public int \u3007o00\u3007\u3007Oo(final Uri uri, final String s, final String[] array) {
        return 0;
    }
    
    public Cursor \u3007o\u3007(final Uri uri, final String[] array, final String s, final String[] array2, final String s2) {
        return null;
    }
    
    public String \u3007\u3007888(final Uri uri) {
        c.\u3007080("OverSeasEventProviderImpl#gettype()");
        final String[] split = uri.getPath().split("/");
        int n = 2;
        final String s = split[2];
        s.hashCode();
        final int hashCode = s.hashCode();
        final int n2 = 0;
        Label_0179: {
            switch (hashCode) {
                case 1131732929: {
                    if (!s.equals("trackAdUrl")) {
                        break;
                    }
                    n = 4;
                    break Label_0179;
                }
                case 1025736635: {
                    if (!s.equals("adLogDispatch")) {
                        break;
                    }
                    n = 3;
                    break Label_0179;
                }
                case 964299715: {
                    if (!s.equals("adLogStop")) {
                        break;
                    }
                    break Label_0179;
                }
                case -171493183: {
                    if (!s.equals("adLogStart")) {
                        break;
                    }
                    n = 1;
                    break Label_0179;
                }
                case -482705237: {
                    if (!s.equals("trackAdFailed")) {
                        break;
                    }
                    n = 0;
                    break Label_0179;
                }
            }
            n = -1;
        }
        Label_0400: {
            switch (n) {
                default: {
                    break Label_0400;
                }
                case 3: {
                    break Label_0400;
                }
                case 2: {
                    break Label_0400;
                }
                case 1: {
                    break Label_0400;
                }
                case 4:
                case 0: {
                    Label_0415: {
                        break Label_0415;
                        c.\u3007o00\u3007\u3007Oo("EventProviderImpl", "dispatch FUN_TRACK_URL");
                        try {
                            final String queryParameter = uri.getQueryParameter("did");
                            final boolean booleanValue = Boolean.valueOf(uri.getQueryParameter("replace"));
                            final String[] split2 = f.\u3007080(uri.getQueryParameter("track")).split(",");
                            if (split2.length > 0) {
                                final ArrayList list = new ArrayList();
                                for (int length = split2.length, i = n2; i < length; ++i) {
                                    final String \u3007080 = f.\u3007080(split2[i]);
                                    if (!TextUtils.isEmpty((CharSequence)\u3007080)) {
                                        list.add(\u3007080);
                                    }
                                }
                                b.b.a.a.i.a.o.a.\u3007080().\u3007080(queryParameter, list, booleanValue);
                            }
                            return null;
                            while (true) {
                                final String queryParameter2;
                                final a \u3007o\u3007 = b.b.a.a.i.a.m.c.a.\u3007o\u3007(f.\u3007080(queryParameter2));
                                iftrue(Label_0431:)(\u3007o\u3007 == null);
                                d.\u3007080.OoO8(\u3007o\u3007);
                                b.b.a.a.i.a.o.a.\u3007080().a(uri.getQueryParameter("did"));
                                c.\u3007o00\u3007\u3007Oo("EventProviderImpl", "dispatch FUN_AD_EVENT_DISPATCH");
                                queryParameter2 = uri.getQueryParameter("event");
                                iftrue(Label_0431:)(TextUtils.isEmpty((CharSequence)queryParameter2));
                                continue;
                            }
                            c.\u3007o00\u3007\u3007Oo("EventProviderImpl", "====ad event function will be stop====");
                            i.\u3007\u30078O0\u30078().o\u3007O8\u3007\u3007o();
                            c.\u3007080("EventProviderImpl====ad event function will be start====");
                            i.\u3007\u30078O0\u30078().\u3007oo\u3007();
                        }
                        finally {
                            return null;
                        }
                    }
                    break;
                }
            }
        }
    }
}

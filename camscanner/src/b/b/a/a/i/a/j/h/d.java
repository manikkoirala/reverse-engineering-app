// 
// Decompiled by Procyon v0.6.0
// 

package b.b.a.a.i.a.j.h;

import b.b.a.a.i.a.l.c;
import java.util.Collection;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.Queue;
import b.b.a.a.i.a.m.a;

public abstract class d<T extends a>
{
    private b.b.a.a.i.a.m.d.a \u3007080;
    private Queue<T> \u3007o00\u3007\u3007Oo;
    private String \u3007o\u3007;
    
    public d(final b.b.a.a.i.a.m.d.a \u3007080, final Queue<String> queue, final String \u3007o\u3007) {
        this.\u3007o00\u3007\u3007Oo = new ConcurrentLinkedQueue<T>();
        this.\u3007080 = \u3007080;
        this.\u3007o\u3007 = \u3007o\u3007;
    }
    
    public List<a> O8(int i, int o8) {
        synchronized (this) {
            if (this.\u3007o\u3007(i, o8)) {
                final ArrayList list = new ArrayList(this.\u3007080.Oo08());
                do {
                    final a a = this.\u3007o00\u3007\u3007Oo.poll();
                    if (a == null) {
                        break;
                    }
                    list.add(a);
                    i = list.size();
                    o8 = this.\u3007080.O8();
                } while (i != o8);
                return list;
            }
            return null;
        }
    }
    
    public void \u3007080(final int n, final List<T> list) {
        monitorenter(this);
        Label_0038: {
            if (n == -1 || n == 200 || n == 509) {
                break Label_0038;
            }
            try {
                this.\u3007o00\u3007\u3007Oo.addAll((Collection<?>)list);
                return;
                final StringBuilder sb = new StringBuilder();
                sb.append(this.\u3007o\u3007);
                sb.append(" memory size\uff1a");
                sb.append(this.\u3007o00\u3007\u3007Oo.size());
                c.\u3007\u3007888(sb.toString());
            }
            finally {
                monitorexit(this);
            }
        }
    }
    
    public void \u3007o00\u3007\u3007Oo(final T t) {
        final Queue<T> \u3007o00\u3007\u3007Oo = this.\u3007o00\u3007\u3007Oo;
        if (\u3007o00\u3007\u3007Oo != null && t != null) {
            \u3007o00\u3007\u3007Oo.offer(t);
        }
    }
    
    public boolean \u3007o\u3007(final int i, int oo08) {
        synchronized (this) {
            final int size = this.\u3007o00\u3007\u3007Oo.size();
            oo08 = this.\u3007080.Oo08();
            final StringBuilder sb = new StringBuilder();
            sb.append(this.\u3007o\u3007);
            sb.append(" size:");
            sb.append(size);
            sb.append(" cacheCount:");
            sb.append(oo08);
            sb.append(" message:");
            sb.append(i);
            c.\u3007\u3007888(sb.toString());
            boolean b = false;
            final boolean b2 = false;
            final boolean b3 = false;
            if (i != 2 && i != 1) {
                boolean b4 = b3;
                if (size >= oo08) {
                    b4 = true;
                }
                return b4;
            }
            if (b.b.a.a.i.a.l.a.OoO8()) {
                if (size >= 1) {
                    b = true;
                }
                return b;
            }
            boolean b5 = b2;
            if (size >= oo08) {
                b5 = true;
            }
            return b5;
        }
    }
}

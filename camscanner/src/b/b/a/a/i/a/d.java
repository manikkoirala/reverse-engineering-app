// 
// Decompiled by Procyon v0.6.0
// 

package b.b.a.a.i.a;

import android.text.TextUtils;
import com.bytedance.sdk.component.utils.r;
import android.content.Context;
import java.util.concurrent.Executor;
import b.b.a.a.i.a.n.e;
import b.b.a.a.i.a.l.c;
import android.os.Looper;
import b.b.a.a.i.a.k.f.b;
import b.b.a.a.i.a.k.f.a;
import java.util.List;

public class d
{
    public static final d \u3007080;
    
    static {
        \u3007080 = new d();
    }
    
    private void OO0o\u3007\u3007(final String s, final List<String> list, final boolean b, final int n) {
        if (n == 0) {
            a.O8(s, list, b);
        }
        else if (n == 1) {
            b.OO0o\u3007\u3007\u3007\u30070(s, list, b);
        }
    }
    
    private boolean Oooo8o0\u3007() {
        return Thread.currentThread() == Looper.getMainLooper().getThread();
    }
    
    private void oO80(final b.b.a.a.i.a.m.a a) {
        final f \u3007080 = i.\u3007\u30078O0\u30078().\u3007080();
        if (a != null && \u3007080 != null && i.\u3007\u30078O0\u30078().OO0o\u3007\u3007() != null) {
            if (\u3007080.a() != null) {
                if (i.\u3007\u30078O0\u30078().oo88o8O()) {
                    final boolean \u3007\u3007808\u3007 = this.\u3007\u3007808\u3007(i.\u3007\u30078O0\u30078().OO0o\u3007\u3007(), \u3007080);
                    final StringBuilder sb = new StringBuilder();
                    sb.append("dispatchEvent mainProcess:");
                    sb.append(\u3007\u3007808\u3007);
                    c.\u3007080(sb.toString());
                    if (\u3007\u3007808\u3007) {
                        i.\u3007\u30078O0\u30078().o\u30070(a);
                        return;
                    }
                    final StringBuilder sb2 = new StringBuilder();
                    sb2.append("sub thread dispatch:");
                    sb2.append(this.Oooo8o0\u3007());
                    c.\u3007080(sb2.toString());
                    if (this.Oooo8o0\u3007()) {
                        \u3007080.a().execute(new e(this, "dispatchEvent", a, \u3007080) {
                            final f OO;
                            final d \u300708O\u300700\u3007o;
                            final b.b.a.a.i.a.m.a \u3007OOo8\u30070;
                            
                            @Override
                            public void run() {
                                this.\u300708O\u300700\u3007o.\u300780\u3007808\u3007O(this.\u3007OOo8\u30070, this.OO.d());
                            }
                        });
                    }
                    else {
                        this.\u300780\u3007808\u3007O(a, \u3007080.d());
                    }
                }
                else {
                    i.\u3007\u30078O0\u30078().o\u30070(a);
                }
            }
        }
    }
    
    private void \u3007080(final int n) {
        if (n == 0) {
            a.o\u30070();
        }
        else if (n == 1) {
            b.OO0o\u3007\u3007();
        }
    }
    
    private void \u300780\u3007808\u3007O(final b.b.a.a.i.a.m.a a, final int n) {
        if (n == 0) {
            a.\u3007o00\u3007\u3007Oo(a);
        }
        else if (n == 1) {
            b.oO80(a);
        }
    }
    
    private void \u30078o8o\u3007(final String s, final int n) {
        if (n == 0) {
            a.\u3007o\u3007(s);
        }
        else if (n == 1) {
            b.\u300780\u3007808\u3007O(s);
        }
    }
    
    private void \u3007O00(final int n) {
        if (n == 0) {
            a.\u3007\u3007888();
        }
        else if (n == 1) {
            b.Oooo8o0\u3007();
        }
    }
    
    private void \u3007o00\u3007\u3007Oo(final b.b.a.a.i.a.a a) {
        if (Looper.myLooper() != Looper.getMainLooper()) {
            b.b.a.a.i.a.l.a.\u3007oo\u3007();
        }
        else {
            final f \u3007o00\u3007\u3007Oo = a.\u3007o00\u3007\u3007Oo();
            if (\u3007o00\u3007\u3007Oo != null) {
                final Executor b = \u3007o00\u3007\u3007Oo.b();
                if (b != null) {
                    b.execute(new Runnable(this) {
                        @Override
                        public void run() {
                            b.b.a.a.i.a.l.a.\u3007oo\u3007();
                        }
                    });
                }
            }
        }
    }
    
    private void \u3007o\u3007(final b.b.a.a.i.a.a a, final Context context) {
        b.b.a.a.i.a.c.\u3007080(context, "context == null");
        b.b.a.a.i.a.c.\u3007080(a, "AdLogConfig == null");
        b.b.a.a.i.a.c.\u3007080(a.\u3007o00\u3007\u3007Oo(), "AdLogDepend ==null");
    }
    
    private boolean \u3007\u3007808\u3007(final Context context, final f f) {
        if (context != null) {
            if (f != null) {
                if (f.d() == 2) {
                    return true;
                }
                if (f.d() == 1) {
                    return f.n();
                }
                try {
                    return r.c(context);
                }
                finally {
                    final Throwable t;
                    c.\u3007o\u3007(t.getMessage());
                    return true;
                }
            }
        }
        return false;
    }
    
    public void OO0o\u3007\u3007\u3007\u30070(final String s) {
        final f \u3007080 = i.\u3007\u30078O0\u30078().\u3007080();
        if (\u3007080 != null && i.\u3007\u30078O0\u30078().OO0o\u3007\u3007() != null) {
            if (\u3007080.a() != null) {
                if (!\u3007080.f()) {
                    return;
                }
                if (\u3007080.d() == 0 && TextUtils.isEmpty((CharSequence)s)) {
                    return;
                }
                if (i.\u3007\u30078O0\u30078().oo88o8O() && !this.\u3007\u3007808\u3007(i.\u3007\u30078O0\u30078().OO0o\u3007\u3007(), \u3007080)) {
                    if (this.Oooo8o0\u3007()) {
                        \u3007080.a().execute(new e(this, "trackFailed", s, \u3007080) {
                            final f OO;
                            final d \u300708O\u300700\u3007o;
                            final String \u3007OOo8\u30070;
                            
                            @Override
                            public void run() {
                                this.\u300708O\u300700\u3007o.\u30078o8o\u3007(this.\u3007OOo8\u30070, this.OO.d());
                            }
                        });
                    }
                    else {
                        this.\u30078o8o\u3007(s, \u3007080.d());
                    }
                    return;
                }
                i.\u3007\u30078O0\u30078().oO80(s);
            }
        }
    }
    
    public void OoO8(final b.b.a.a.i.a.m.a a) {
        this.oO80(a);
    }
    
    public void o800o8O() {
        final f \u3007080 = i.\u3007\u30078O0\u30078().\u3007080();
        if (\u3007080 != null && i.\u3007\u30078O0\u30078().OO0o\u3007\u3007() != null) {
            if (\u3007080.a() != null) {
                if (i.\u3007\u30078O0\u30078().oo88o8O()) {
                    if (this.\u3007\u3007808\u3007(i.\u3007\u30078O0\u30078().OO0o\u3007\u3007(), \u3007080)) {
                        i.\u3007\u30078O0\u30078().o\u3007O8\u3007\u3007o();
                        return;
                    }
                    if (this.Oooo8o0\u3007()) {
                        \u3007080.a().execute(new e(this, "stop", \u3007080) {
                            final d OO;
                            final f \u3007OOo8\u30070;
                            
                            @Override
                            public void run() {
                                this.OO.\u3007O00(this.\u3007OOo8\u30070.d());
                            }
                        });
                    }
                    else {
                        this.\u3007O00(\u3007080.d());
                    }
                }
                else {
                    i.\u3007\u30078O0\u30078().o\u3007O8\u3007\u3007o();
                }
            }
        }
    }
    
    public void \u3007O888o0o() {
    }
    
    public void \u3007O8o08O(final String s, final List<String> list, final boolean b) {
        final f \u3007080 = i.\u3007\u30078O0\u30078().\u3007080();
        if (\u3007080 != null && i.\u3007\u30078O0\u30078().OO0o\u3007\u3007() != null) {
            if (\u3007080.a() != null) {
                if (!\u3007080.f()) {
                    return;
                }
                if (\u3007080.d() == 1) {
                    if (list == null || list.isEmpty()) {
                        return;
                    }
                }
                else if (\u3007080.d() == 0 && (TextUtils.isEmpty((CharSequence)s) || list == null || list.isEmpty())) {
                    return;
                }
                if (i.\u3007\u30078O0\u30078().oo88o8O() && !this.\u3007\u3007808\u3007(i.\u3007\u30078O0\u30078().OO0o\u3007\u3007(), \u3007080)) {
                    if (this.Oooo8o0\u3007()) {
                        \u3007080.a().execute(new e(this, "trackFailed", s, list, b, \u3007080) {
                            final d O8o08O8O;
                            final List OO;
                            final f o\u300700O;
                            final boolean \u300708O\u300700\u3007o;
                            final String \u3007OOo8\u30070;
                            
                            @Override
                            public void run() {
                                this.O8o08O8O.OO0o\u3007\u3007(this.\u3007OOo8\u30070, this.OO, this.\u300708O\u300700\u3007o, this.o\u300700O.d());
                            }
                        });
                    }
                    else {
                        this.OO0o\u3007\u3007(s, list, b, \u3007080.d());
                    }
                    return;
                }
                i.\u3007\u30078O0\u30078().\u300780\u3007808\u3007O(s, list, b);
            }
        }
    }
    
    public void \u3007O\u3007() {
        c.\u3007080("EventMultiUtils start");
        final f \u3007080 = i.\u3007\u30078O0\u30078().\u3007080();
        if (\u3007080 != null && i.\u3007\u30078O0\u30078().OO0o\u3007\u3007() != null) {
            if (\u3007080.a() != null) {
                if (i.\u3007\u30078O0\u30078().oo88o8O()) {
                    if (this.\u3007\u3007808\u3007(i.\u3007\u30078O0\u30078().OO0o\u3007\u3007(), \u3007080)) {
                        i.\u3007\u30078O0\u30078().\u3007oo\u3007();
                        return;
                    }
                    if (this.Oooo8o0\u3007()) {
                        \u3007080.a().execute(new e(this, "start", \u3007080) {
                            final d OO;
                            final f \u3007OOo8\u30070;
                            
                            @Override
                            public void run() {
                                c.\u3007080("TTExecutor start");
                                this.OO.\u3007080(this.\u3007OOo8\u30070.d());
                            }
                        });
                    }
                    else {
                        this.\u3007080(\u3007080.d());
                    }
                }
                else {
                    i.\u3007\u30078O0\u30078().\u3007oo\u3007();
                }
            }
        }
    }
    
    public void \u3007\u30078O0\u30078(final b.b.a.a.i.a.a a, final Context context) {
        this.\u3007o\u3007(a, context);
        i.\u3007\u30078O0\u30078().\u3007o00\u3007\u3007Oo(context);
        i.\u3007\u30078O0\u30078().Oo08(a.\u3007\u30078O0\u30078());
        i.\u3007\u30078O0\u30078().\u3007O8o08O(a.\u3007\u3007808\u3007());
        i.\u3007\u30078O0\u30078().Oooo8o0\u3007(a.\u3007O\u3007());
        i.\u3007\u30078O0\u30078().\u3007\u3007888(a.OO0o\u3007\u3007());
        i.\u3007\u30078O0\u30078().\u3007O\u3007(a.\u3007O00());
        final i \u3007\u30078O0\u30078 = i.\u3007\u30078O0\u30078();
        b.b.a.a.i.a.j.e e;
        if (a.Oooo8o0\u3007() == null) {
            e = b.b.a.a.i.a.j.g.e.\u3007o00\u3007\u3007Oo;
        }
        else {
            e = a.Oooo8o0\u3007();
        }
        \u3007\u30078O0\u30078.O8(e);
        i.\u3007\u30078O0\u30078().OO0o\u3007\u3007\u3007\u30070(a.\u30070\u3007O0088o());
        i.\u3007\u30078O0\u30078().\u3007o\u3007(a.\u3007o00\u3007\u3007Oo());
        b.b.a.a.i.a.k.g.c.\u3007oOO8O8(a.oO80());
        b.b.a.a.i.a.k.g.c.OOO\u3007O0(a.\u30078o8o\u3007());
        this.\u3007o00\u3007\u3007Oo(a);
    }
}

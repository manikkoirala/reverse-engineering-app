// 
// Decompiled by Procyon v0.6.0
// 

package b.b.a.a.i.a.k;

import b.b.a.a.i.a.i;
import java.util.concurrent.Executor;
import b.b.a.a.i.a.n.e;
import android.os.Looper;
import b.b.a.a.i.a.l.b;
import b.b.a.a.i.a.f;
import java.util.concurrent.atomic.AtomicLong;
import b.b.a.a.i.a.k.g.c;
import java.util.concurrent.PriorityBlockingQueue;
import java.util.Comparator;
import android.os.Handler;
import b.b.a.a.i.a.k.e.a;

public class d
{
    public static long OO0o\u3007\u3007\u3007\u30070;
    public static final a oO80;
    public static final long \u300780\u3007808\u3007O;
    public static final d \u3007\u3007888;
    private volatile Handler O8;
    private final Comparator<b.b.a.a.i.a.m.a> Oo08;
    private final PriorityBlockingQueue<b.b.a.a.i.a.m.a> o\u30070;
    private volatile c \u3007080;
    public volatile boolean \u3007o00\u3007\u3007Oo;
    public volatile boolean \u3007o\u3007;
    
    static {
        \u3007\u3007888 = new d();
        oO80 = new a();
        new AtomicLong(0L);
        new AtomicLong(0L);
        \u300780\u3007808\u3007O = System.currentTimeMillis();
        d.OO0o\u3007\u3007\u3007\u30070 = 0L;
    }
    
    private d() {
        this.\u3007o00\u3007\u3007Oo = false;
        this.\u3007o\u3007 = false;
        final Comparator<b.b.a.a.i.a.m.a> comparator = new Comparator<b.b.a.a.i.a.m.a>() {
            final d o0;
            
            public int \u3007080(final b.b.a.a.i.a.m.a a, final b.b.a.a.i.a.m.a a2) {
                return this.o0.\u3007o00\u3007\u3007Oo(a, a2);
            }
        };
        this.Oo08 = comparator;
        this.o\u30070 = new PriorityBlockingQueue<b.b.a.a.i.a.m.a>(8, (Comparator<? super Object>)comparator);
    }
    
    private void O8(final f f, final long n) {
        final c \u3007080 = this.\u3007080;
        if (f != null) {
            if (\u3007080 != null) {
                final a oo80 = d.oO80;
                \u3007080.\u30078o8o\u3007(f.a(oo80.O8ooOoo\u3007(n)), true);
                oo80.\u300708O8o\u30070();
            }
        }
    }
    
    private void Oo08(final f f, final b.b.a.a.i.a.m.a a) {
        if (f != null) {
            try {
                if (f.e()) {
                    long b;
                    if (a != null && a.d() != null) {
                        b = a.d().b();
                    }
                    else {
                        b = 0L;
                    }
                    if (b == 1L) {
                        d.OO0o\u3007\u3007\u3007\u30070 = System.currentTimeMillis();
                    }
                    final AtomicLong oo8Oo00oo = d.oO80.Oo8Oo00oo();
                    b.b.a.a.i.a.l.b.\u3007080(oo8Oo00oo, 1);
                    if (oo8Oo00oo.get() == 200L) {
                        if (Looper.getMainLooper() == Looper.myLooper()) {
                            Executor executor;
                            if ((executor = f.a()) == null) {
                                executor = f.b();
                            }
                            if (executor != null) {
                                executor.execute(new e(this, "report", f, b) {
                                    final long OO;
                                    final d \u300708O\u300700\u3007o;
                                    final f \u3007OOo8\u30070;
                                    
                                    @Override
                                    public void run() {
                                        this.\u300708O\u300700\u3007o.O8(this.\u3007OOo8\u30070, this.OO);
                                    }
                                });
                            }
                        }
                        else {
                            final StringBuilder sb = new StringBuilder();
                            sb.append("==> monitor upload index1:");
                            sb.append(b);
                            b.b.a.a.i.a.l.c.\u3007080(sb.toString());
                            this.O8(f, b);
                        }
                    }
                }
            }
            catch (final Exception ex) {
                ex.printStackTrace();
            }
        }
    }
    
    private int \u3007o00\u3007\u3007Oo(final b.b.a.a.i.a.m.a a, final b.b.a.a.i.a.m.a a2) {
        if (a == null) {
            if (a2 == null) {
                return 0;
            }
            return -1;
        }
        else {
            if (a2 == null) {
                return 1;
            }
            if (a.c() != a2.c()) {
                return a.c() - a2.c();
            }
            long a3;
            long b;
            if (a.d() != null) {
                a3 = a.d().a();
                b = a.d().b();
            }
            else {
                a3 = 0L;
                b = 0L;
            }
            long a4;
            long b2;
            if (a2.d() != null) {
                a4 = a2.d().a();
                b2 = a2.d().b();
            }
            else {
                b2 = 0L;
                a4 = 0L;
            }
            if (a3 == 0L || a4 == 0L) {
                return 0;
            }
            final long a5 = a3 - a4;
            if (Math.abs(a5) > 2147483647L) {
                return 0;
            }
            if (a5 != 0L) {
                return (int)a5;
            }
            if (b != 0L && b2 != 0L) {
                return (int)(b - b2);
            }
            return 0;
        }
    }
    
    public PriorityBlockingQueue<b.b.a.a.i.a.m.a> OO0o\u3007\u3007\u3007\u30070() {
        return this.o\u30070;
    }
    
    public boolean oO80() {
        monitorenter(this);
        try {
            if (this.\u3007080 != null && ((Thread)this.\u3007080).isAlive()) {
                final StringBuilder sb = new StringBuilder();
                sb.append("LogThread state:");
                sb.append(((Thread)this.\u3007080).getState());
                b.b.a.a.i.a.l.c.\u3007080(sb.toString());
            }
            else if (!b.b.a.a.i.a.b.o\u30070()) {
                b.b.a.a.i.a.l.c.\u3007080("--start LogThread--");
                ((Thread)(this.\u3007080 = new c(this.o\u30070))).start();
                monitorexit(this);
                return true;
            }
            monitorexit(this);
            return false;
        }
        finally {
            try {
                final Throwable t;
                b.b.a.a.i.a.l.c.\u3007o\u3007(t.getMessage());
                return false;
            }
            finally {
                monitorexit(this);
            }
        }
    }
    
    public void \u300780\u3007808\u3007O() {
        b.\u3007080(d.oO80.\u3007o\u3007(), 1);
        b.b.a.a.i.a.l.c.\u3007\u3007888("flushMemoryAndDB()");
        final c \u3007080 = this.\u3007080;
        if (Looper.myLooper() == Looper.getMainLooper()) {
            final f \u300781 = i.\u3007\u30078O0\u30078().\u3007080();
            if (\u300781 != null) {
                Executor executor;
                if ((executor = \u300781.a()) == null) {
                    executor = \u300781.b();
                }
                if (executor != null) {
                    executor.execute(new e(this, "flush", \u3007080) {
                        final c \u3007OOo8\u30070;
                        
                        @Override
                        public void run() {
                            final c \u3007oOo8\u30070 = this.\u3007OOo8\u30070;
                            if (\u3007oOo8\u30070 != null) {
                                \u3007oOo8\u30070.O8ooOoo\u3007(2);
                            }
                        }
                    });
                }
            }
            else {
                b.b.a.a.i.a.l.c.\u3007\u3007888("discard flush");
            }
        }
        else if (\u3007080 != null) {
            \u3007080.O8ooOoo\u3007(2);
        }
    }
    
    public void \u30078o8o\u3007() {
        this.oO80();
        this.\u300780\u3007808\u3007O();
    }
    
    public void \u3007O8o08O() {
        synchronized (this) {
            if (this.\u3007080 != null && ((Thread)this.\u3007080).isAlive()) {
                if (this.O8 != null) {
                    this.O8.removeCallbacksAndMessages((Object)null);
                }
                this.\u3007080.\u3007\u30078O0\u30078(false);
                this.\u3007080.quitSafely();
                this.\u3007080 = null;
            }
        }
    }
    
    public void \u3007o\u3007(final Handler o8) {
        this.O8 = o8;
    }
    
    public void \u3007\u3007888(final b.b.a.a.i.a.m.a a, final int n) {
        this.oO80();
        final f \u3007080 = i.\u3007\u30078O0\u30078().\u3007080();
        final c \u300781 = this.\u3007080;
        if (\u300781 != null) {
            this.Oo08(\u3007080, a);
            \u300781.\u30078o8o\u3007(a, a.c() == 4);
        }
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package b.b.a.a.i.a.j.g;

import b.b.a.a.i.a.m.a;
import java.util.List;
import android.content.ContentValues;
import android.database.SQLException;
import android.database.Cursor;
import b.b.a.a.i.a.l.c;
import b.b.a.a.i.a.i;
import android.database.sqlite.SQLiteDatabase;
import android.database.AbstractCursor;
import android.content.Context;

class b
{
    private c \u3007080;
    private Context \u3007o00\u3007\u3007Oo;
    
    b(final Context context) {
        try {
            this.\u3007o00\u3007\u3007Oo = context.getApplicationContext();
            if (this.\u3007080 == null) {
                this.\u3007080 = new c();
            }
        }
        finally {}
    }
    
    public c \u3007080() {
        return this.\u3007080;
    }
    
    private class b extends AbstractCursor
    {
        private b(final b.b.a.a.i.a.j.g.b b) {
        }
        
        public String[] getColumnNames() {
            return new String[0];
        }
        
        public int getCount() {
            return 0;
        }
        
        public double getDouble(final int n) {
            return 0.0;
        }
        
        public float getFloat(final int n) {
            return 0.0f;
        }
        
        public int getInt(final int n) {
            return 0;
        }
        
        public long getLong(final int n) {
            return 0L;
        }
        
        public short getShort(final int n) {
            return 0;
        }
        
        public String getString(final int n) {
            return null;
        }
        
        public boolean isNull(final int n) {
            return true;
        }
    }
    
    public class c
    {
        private volatile SQLiteDatabase \u3007080;
        final b \u3007o00\u3007\u3007Oo;
        
        public c(final b \u3007o00\u3007\u3007Oo) {
            this.\u3007o00\u3007\u3007Oo = \u3007o00\u3007\u3007Oo;
            this.\u3007080 = null;
        }
        
        private void Oo08() {
            try {
                if (this.\u3007080 == null || !this.\u3007080.isOpen()) {
                    synchronized (this) {
                        if (this.\u3007080 == null || !this.\u3007080.isOpen()) {
                            (this.\u3007080 = i.\u3007\u30078O0\u30078().\u3007\u3007808\u3007().a(i.\u3007\u30078O0\u30078().OO0o\u3007\u3007())).setLockingEnabled(false);
                            b.b.a.a.i.a.l.c.\u3007080("---------------DB CREATE  SUCCESS------------");
                        }
                    }
                }
            }
            finally {
                final Throwable t;
                t.printStackTrace();
                if (this.oO80()) {
                    return;
                }
            }
        }
        
        private boolean oO80() {
            final SQLiteDatabase \u3007080 = this.\u3007080;
            return \u3007080 != null && \u3007080.inTransaction();
        }
        
        public Cursor O8(final String s, final String[] array, final String s2, final String[] array2, final String s3, final String s4, final String s5) {
            b b = null;
            try {
                this.Oo08();
                this.\u3007080.query(s, array, s2, array2, s3, s4, s5);
            }
            finally {
                final Throwable t;
                t.printStackTrace();
                b = new b();
                if (this.oO80()) {
                    return;
                }
            }
            return (Cursor)b;
        }
        
        public void o\u30070(final String s) throws SQLException {
            try {
                this.Oo08();
                this.\u3007080.execSQL(s);
            }
            finally {
                if (this.oO80()) {
                    return;
                }
            }
        }
        
        public int \u3007080(final String s, final ContentValues contentValues, final String s2, final String[] array) {
            int update;
            try {
                this.Oo08();
                update = this.\u3007080.update(s, contentValues, s2, array);
            }
            catch (final Exception ex) {
                ex.printStackTrace();
                if (this.oO80()) {
                    throw ex;
                }
                update = 0;
            }
            return update;
        }
        
        public int \u3007o00\u3007\u3007Oo(final String s, final String s2, final String[] array) {
            int delete;
            try {
                this.Oo08();
                delete = this.\u3007080.delete(s, s2, array);
            }
            catch (final Exception ex) {
                ex.printStackTrace();
                if (this.oO80()) {
                    throw ex;
                }
                delete = 0;
            }
            return delete;
        }
        
        public long \u3007o\u3007(final String s, final String s2, final ContentValues contentValues) {
            long insert;
            try {
                this.Oo08();
                insert = this.\u3007080.insert(s, s2, contentValues);
            }
            catch (final Exception ex) {
                ex.printStackTrace();
                if (this.oO80()) {
                    throw ex;
                }
                insert = -1L;
            }
            return insert;
        }
        
        public void \u3007\u3007888(final String p0, final String p1, final List<a> p2) {
            // 
            // This method could not be decompiled.
            // 
            // Original Bytecode:
            // 
            //     1: monitorenter   
            //     2: aload_0        
            //     3: invokespecial   b/b/a/a/i/a/j/g/b$c.Oo08:()V
            //     6: aload_0        
            //     7: getfield        b/b/a/a/i/a/j/g/b$c.\u3007080:Landroid/database/sqlite/SQLiteDatabase;
            //    10: invokevirtual   android/database/sqlite/SQLiteDatabase.beginTransaction:()V
            //    13: new             Landroid/content/ContentValues;
            //    16: astore          6
            //    18: aload           6
            //    20: invokespecial   android/content/ContentValues.<init>:()V
            //    23: iconst_0       
            //    24: istore          4
            //    26: iload           4
            //    28: aload_3        
            //    29: invokeinterface java/util/List.size:()I
            //    34: if_icmpge       182
            //    37: aload_3        
            //    38: iload           4
            //    40: invokeinterface java/util/List.get:(I)Ljava/lang/Object;
            //    45: checkcast       Lb/b/a/a/i/a/m/a;
            //    48: astore          8
            //    50: aload           8
            //    52: ifnonnull       58
            //    55: goto            176
            //    58: aload           8
            //    60: invokeinterface b/b/a/a/i/a/m/a.b:()Lorg/json/JSONObject;
            //    65: astore          7
            //    67: aload           7
            //    69: ifnonnull       75
            //    72: goto            176
            //    75: aload           6
            //    77: ldc             "id"
            //    79: aload           8
            //    81: invokeinterface b/b/a/a/i/a/m/a.i:()Ljava/lang/String;
            //    86: invokevirtual   android/content/ContentValues.put:(Ljava/lang/String;Ljava/lang/String;)V
            //    89: invokestatic    b/b/a/a/i/a/i.\u3007\u30078O0\u30078:()Lb/b/a/a/i/a/i;
            //    92: invokevirtual   b/b/a/a/i/a/i.\u3007080:()Lb/b/a/a/i/a/f;
            //    95: aload           7
            //    97: invokevirtual   org/json/JSONObject.toString:()Ljava/lang/String;
            //   100: invokeinterface b/b/a/a/i/a/f.b:(Ljava/lang/String;)Ljava/lang/String;
            //   105: astore          7
            //   107: aload           7
            //   109: invokestatic    android/text/TextUtils.isEmpty:(Ljava/lang/CharSequence;)Z
            //   112: ifne            171
            //   115: aload           6
            //   117: ldc             "value"
            //   119: aload           7
            //   121: invokevirtual   android/content/ContentValues.put:(Ljava/lang/String;Ljava/lang/String;)V
            //   124: aload           6
            //   126: ldc             "gen_time"
            //   128: invokestatic    java/lang/System.currentTimeMillis:()J
            //   131: invokestatic    java/lang/Long.valueOf:(J)Ljava/lang/Long;
            //   134: invokevirtual   android/content/ContentValues.put:(Ljava/lang/String;Ljava/lang/Long;)V
            //   137: aload           6
            //   139: ldc             "retry"
            //   141: iconst_0       
            //   142: invokestatic    java/lang/Integer.valueOf:(I)Ljava/lang/Integer;
            //   145: invokevirtual   android/content/ContentValues.put:(Ljava/lang/String;Ljava/lang/Integer;)V
            //   148: aload           6
            //   150: ldc             "encrypt"
            //   152: iconst_1       
            //   153: invokestatic    java/lang/Integer.valueOf:(I)Ljava/lang/Integer;
            //   156: invokevirtual   android/content/ContentValues.put:(Ljava/lang/String;Ljava/lang/Integer;)V
            //   159: aload_0        
            //   160: getfield        b/b/a/a/i/a/j/g/b$c.\u3007080:Landroid/database/sqlite/SQLiteDatabase;
            //   163: aload_1        
            //   164: aload_2        
            //   165: aload           6
            //   167: invokevirtual   android/database/sqlite/SQLiteDatabase.insert:(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J
            //   170: pop2           
            //   171: aload           6
            //   173: invokevirtual   android/content/ContentValues.clear:()V
            //   176: iinc            4, 1
            //   179: goto            26
            //   182: aload_0        
            //   183: getfield        b/b/a/a/i/a/j/g/b$c.\u3007080:Landroid/database/sqlite/SQLiteDatabase;
            //   186: invokevirtual   android/database/sqlite/SQLiteDatabase.setTransactionSuccessful:()V
            //   189: new             Ljava/lang/StringBuilder;
            //   192: astore_2       
            //   193: aload_2        
            //   194: invokespecial   java/lang/StringBuilder.<init>:()V
            //   197: aload_2        
            //   198: aload_1        
            //   199: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
            //   202: pop            
            //   203: aload_2        
            //   204: ldc             " insert list size="
            //   206: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
            //   209: pop            
            //   210: aload_2        
            //   211: aload_3        
            //   212: invokeinterface java/util/List.size:()I
            //   217: invokevirtual   java/lang/StringBuilder.append:(I)Ljava/lang/StringBuilder;
            //   220: pop            
            //   221: ldc             "DBHelper"
            //   223: aload_2        
            //   224: invokevirtual   java/lang/StringBuilder.toString:()Ljava/lang/String;
            //   227: invokestatic    b/b/a/a/i/a/l/c.O8:(Ljava/lang/String;Ljava/lang/String;)V
            //   230: aload_0        
            //   231: getfield        b/b/a/a/i/a/j/g/b$c.\u3007080:Landroid/database/sqlite/SQLiteDatabase;
            //   234: ifnull          321
            //   237: goto            314
            //   240: astore_1       
            //   241: goto            326
            //   244: astore_2       
            //   245: new             Ljava/lang/StringBuilder;
            //   248: astore          6
            //   250: aload           6
            //   252: invokespecial   java/lang/StringBuilder.<init>:()V
            //   255: aload           6
            //   257: aload_1        
            //   258: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
            //   261: pop            
            //   262: aload           6
            //   264: ldc             " insert list error="
            //   266: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
            //   269: pop            
            //   270: aload           6
            //   272: aload_3        
            //   273: invokeinterface java/util/List.size:()I
            //   278: invokevirtual   java/lang/StringBuilder.append:(I)Ljava/lang/StringBuilder;
            //   281: pop            
            //   282: ldc             "DBHelper"
            //   284: aload           6
            //   286: invokevirtual   java/lang/StringBuilder.toString:()Ljava/lang/String;
            //   289: invokestatic    b/b/a/a/i/a/l/c.O8:(Ljava/lang/String;Ljava/lang/String;)V
            //   292: aload_2        
            //   293: invokevirtual   java/lang/Throwable.printStackTrace:()V
            //   296: aload_0        
            //   297: invokespecial   b/b/a/a/i/a/j/g/b$c.oO80:()Z
            //   300: istore          5
            //   302: iload           5
            //   304: ifne            324
            //   307: aload_0        
            //   308: getfield        b/b/a/a/i/a/j/g/b$c.\u3007080:Landroid/database/sqlite/SQLiteDatabase;
            //   311: ifnull          321
            //   314: aload_0        
            //   315: getfield        b/b/a/a/i/a/j/g/b$c.\u3007080:Landroid/database/sqlite/SQLiteDatabase;
            //   318: invokevirtual   android/database/sqlite/SQLiteDatabase.endTransaction:()V
            //   321: aload_0        
            //   322: monitorexit    
            //   323: return         
            //   324: aload_2        
            //   325: athrow         
            //   326: aload_0        
            //   327: getfield        b/b/a/a/i/a/j/g/b$c.\u3007080:Landroid/database/sqlite/SQLiteDatabase;
            //   330: ifnull          340
            //   333: aload_0        
            //   334: getfield        b/b/a/a/i/a/j/g/b$c.\u3007080:Landroid/database/sqlite/SQLiteDatabase;
            //   337: invokevirtual   android/database/sqlite/SQLiteDatabase.endTransaction:()V
            //   340: aload_1        
            //   341: athrow         
            //   342: astore_1       
            //   343: aload_0        
            //   344: monitorexit    
            //   345: aload_1        
            //   346: athrow         
            //    Signature:
            //  (Ljava/lang/String;Ljava/lang/String;Ljava/util/List<Lb/b/a/a/i/a/m/a;>;)V
            //    Exceptions:
            //  Try           Handler
            //  Start  End    Start  End    Type                 
            //  -----  -----  -----  -----  ---------------------
            //  2      23     244    314    Ljava/lang/Exception;
            //  2      23     240    342    Any
            //  26     50     244    314    Ljava/lang/Exception;
            //  26     50     240    342    Any
            //  58     67     244    314    Ljava/lang/Exception;
            //  58     67     240    342    Any
            //  75     124    244    314    Ljava/lang/Exception;
            //  75     124    240    342    Any
            //  124    137    244    314    Ljava/lang/Exception;
            //  124    137    240    342    Any
            //  137    148    244    314    Ljava/lang/Exception;
            //  137    148    240    342    Any
            //  148    171    244    314    Ljava/lang/Exception;
            //  148    171    240    342    Any
            //  171    176    244    314    Ljava/lang/Exception;
            //  171    176    240    342    Any
            //  182    189    244    314    Ljava/lang/Exception;
            //  182    189    240    342    Any
            //  189    230    244    314    Ljava/lang/Exception;
            //  189    230    240    342    Any
            //  230    237    342    347    Any
            //  245    302    240    342    Any
            //  307    314    342    347    Any
            //  314    321    342    347    Any
            //  324    326    240    342    Any
            //  326    340    342    347    Any
            //  340    342    342    347    Any
            // 
            // The error that occurred was:
            // 
            // java.lang.IllegalStateException: Expression is linked from several locations: Label_0314:
            //     at com.strobel.decompiler.ast.Error.expressionLinkedFromMultipleLocations(Error.java:27)
            //     at com.strobel.decompiler.ast.AstOptimizer.mergeDisparateObjectInitializations(AstOptimizer.java:2604)
            //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:235)
            //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:42)
            //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:206)
            //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:93)
            //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethodBody(AstBuilder.java:868)
            //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethod(AstBuilder.java:761)
            //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addTypeMembers(AstBuilder.java:638)
            //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeCore(AstBuilder.java:605)
            //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeNoCache(AstBuilder.java:195)
            //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addTypeMembers(AstBuilder.java:662)
            //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeCore(AstBuilder.java:605)
            //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeNoCache(AstBuilder.java:195)
            //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createType(AstBuilder.java:162)
            //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addType(AstBuilder.java:137)
            //     at com.strobel.decompiler.languages.java.JavaLanguage.buildAst(JavaLanguage.java:71)
            //     at com.strobel.decompiler.languages.java.JavaLanguage.decompileType(JavaLanguage.java:59)
            //     at com.strobel.decompiler.DecompilerDriver.decompileType(DecompilerDriver.java:333)
            //     at com.strobel.decompiler.DecompilerDriver.decompileJar(DecompilerDriver.java:254)
            //     at com.strobel.decompiler.DecompilerDriver.main(DecompilerDriver.java:144)
            // 
            throw new IllegalStateException("An error occurred while decompiling this method.");
        }
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package b.b.a.a.i.a.j;

import b.b.a.a.i.a.i;
import java.util.Iterator;
import android.text.TextUtils;
import java.util.Collection;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.Queue;

public class b extends a
{
    private final d \u3007080;
    private final c \u3007o00\u3007\u3007Oo;
    private final Queue<String> \u3007o\u3007;
    
    public b() {
        final ConcurrentLinkedQueue \u3007o\u3007 = new ConcurrentLinkedQueue();
        this.\u3007o\u3007 = \u3007o\u3007;
        this.\u3007080 = new f(\u3007o\u3007);
        this.\u3007o00\u3007\u3007Oo = new c();
    }
    
    @Override
    public boolean a(final int n, final boolean b) {
        synchronized (this) {
            if (this.\u3007080.a(n, b)) {
                b.b.a.a.i.a.l.c.\u3007\u3007888("memory meet");
                b.\u3007080(b.b.a.a.i.a.k.d.oO80.Oo08(), 1);
                return true;
            }
            if ((n == 1 || n == 2) && this.\u3007o00\u3007\u3007Oo.a(n, b)) {
                b.b.a.a.i.a.l.c.\u3007\u3007888("db meet");
                b.\u3007080(b.b.a.a.i.a.k.d.oO80.\u300780(), 1);
                return true;
            }
            return false;
        }
    }
    
    @Override
    public List<b.b.a.a.i.a.m.a> \u3007080(int n, final int n2, final List<String> list) {
        synchronized (this) {
            final List<b.b.a.a.i.a.m.a> \u3007080 = this.\u3007080.\u3007080(n, n2, list);
            List<b.b.a.a.i.a.m.a> \u300781 = null;
            Label_0718: {
                if (\u3007080 != null && \u3007080.size()) {
                    final StringBuilder sb = new StringBuilder();
                    sb.append("memory get ");
                    sb.append(\u3007080.size());
                    b.b.a.a.i.a.l.c.\u3007080(sb.toString());
                    if (n != 1) {
                        \u300781 = \u3007080;
                        if (n != 2) {
                            break Label_0718;
                        }
                    }
                    final List<b.b.a.a.i.a.m.a> oo08 = this.\u3007o00\u3007\u3007Oo.Oo08(\u3007080.get(0), \u3007080.size());
                    \u300781 = \u3007080;
                    if (oo08 != null) {
                        \u300781 = \u3007080;
                        if (oo08.size() != 0) {
                            final StringBuilder sb2 = new StringBuilder();
                            sb2.append("db get ");
                            sb2.append(oo08.size());
                            b.b.a.a.i.a.l.c.\u3007080(sb2.toString());
                            final HashMap<String, b.b.a.a.i.a.m.a> hashMap = new HashMap<String, b.b.a.a.i.a.m.a>();
                            for (final b.b.a.a.i.a.m.a value : oo08) {
                                hashMap.put(value.i(), value);
                            }
                            final ArrayList list2 = new ArrayList(this.\u3007o\u3007);
                        Label_0237:
                            for (final b.b.a.a.i.a.m.a a : oo08) {
                                final Iterator iterator3 = list2.iterator();
                                while (true) {
                                    while (iterator3.hasNext()) {
                                        if (TextUtils.equals((CharSequence)a.i(), (CharSequence)iterator3.next())) {
                                            b.b.a.a.i.a.l.c.\u3007080(" duplicate delete ");
                                            n = 1;
                                            if (n != 0) {
                                                hashMap.remove(a.i());
                                                continue Label_0237;
                                            }
                                            continue Label_0237;
                                        }
                                    }
                                    n = 0;
                                    continue;
                                }
                            }
                            for (final b.b.a.a.i.a.m.a value2 : \u3007080) {
                                hashMap.put(value2.i(), value2);
                            }
                            \u3007080.clear();
                            final Iterator<String> iterator5 = hashMap.keySet().iterator();
                            while (true) {
                                \u300781 = \u3007080;
                                if (!iterator5.hasNext()) {
                                    break;
                                }
                                \u3007080.add(hashMap.get(iterator5.next()));
                            }
                        }
                    }
                }
                else {
                    final ArrayList list3 = new ArrayList(this.\u3007o\u3007);
                    if (list != null && !list.isEmpty()) {
                        list3.addAll(list);
                    }
                    \u300781 = this.\u3007o00\u3007\u3007Oo.\u3007080(n, n2, list3);
                    if (\u300781 != null && \u300781.size() != 0) {
                        final HashMap<String, b.b.a.a.i.a.m.a> hashMap2 = new HashMap<String, b.b.a.a.i.a.m.a>();
                        for (final b.b.a.a.i.a.m.a value3 : \u300781) {
                            hashMap2.put(value3.i(), value3);
                        }
                        final StringBuilder sb3 = new StringBuilder();
                        sb3.append("allSendingQueue:");
                        sb3.append(list3.size());
                        b.b.a.a.i.a.l.c.\u3007080(sb3.toString());
                        if (list3.size() != 0) {
                            for (final String s : list3) {
                                if (hashMap2.get(s) != null) {
                                    b.b.a.a.i.a.l.c.\u3007080("db duplicate delete");
                                    hashMap2.remove(s);
                                }
                            }
                        }
                        \u300781.clear();
                        final Iterator<String> iterator8 = hashMap2.keySet().iterator();
                        while (iterator8.hasNext()) {
                            \u300781.add(hashMap2.get(iterator8.next()));
                        }
                    }
                }
            }
            if (\u300781 != null && !\u300781.isEmpty()) {
                final Iterator<b.b.a.a.i.a.m.a> iterator9 = \u300781.iterator();
                while (iterator9.hasNext()) {
                    this.\u3007o\u3007.offer(iterator9.next().i());
                }
                return \u300781;
            }
            return new ArrayList<b.b.a.a.i.a.m.a>();
        }
    }
    
    @Override
    public void \u3007o00\u3007\u3007Oo(final b.b.a.a.i.a.m.a a, final int n) {
        monitorenter(this);
        Label_0048: {
            if (n == 5) {
                break Label_0048;
            }
            try {
                if (i.\u3007\u30078O0\u30078().\u3007080().a(i.\u3007\u30078O0\u30078().OO0o\u3007\u3007())) {
                    final d \u3007080 = this.\u3007080;
                    if (\u3007080 != null && a != null) {
                        \u3007080.\u3007o00\u3007\u3007Oo(a, n);
                    }
                }
                final c \u3007o00\u3007\u3007Oo = this.\u3007o00\u3007\u3007Oo;
                if (\u3007o00\u3007\u3007Oo != null && a != null) {
                    \u3007o00\u3007\u3007Oo.\u3007o00\u3007\u3007Oo(a, n);
                }
            }
            finally {
                monitorexit(this);
            }
        }
    }
    
    @Override
    public void \u3007o\u3007(final int n, final List<b.b.a.a.i.a.m.a> list) {
        synchronized (this) {
            final Iterator<b.b.a.a.i.a.m.a> iterator = list.iterator();
            while (iterator.hasNext()) {
                this.\u3007o\u3007.remove(iterator.next().i());
            }
            final d \u3007080 = this.\u3007080;
            if (\u3007080 != null) {
                \u3007080.\u3007o\u3007(n, list);
            }
            final c \u3007o00\u3007\u3007Oo = this.\u3007o00\u3007\u3007Oo;
            if (\u3007o00\u3007\u3007Oo != null) {
                \u3007o00\u3007\u3007Oo.\u3007o\u3007(n, list);
            }
        }
    }
}

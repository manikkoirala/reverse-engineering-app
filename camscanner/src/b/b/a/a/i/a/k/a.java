// 
// Decompiled by Procyon v0.6.0
// 

package b.b.a.a.i.a.k;

import b.b.a.a.i.a.l.c;
import android.content.Context;
import b.b.a.a.i.a.j.e;
import android.text.TextUtils;
import b.b.a.a.i.a.i;

public class a
{
    public static boolean O8() {
        final e \u3007\u3007808\u3007 = i.\u3007\u30078O0\u30078().\u3007\u3007808\u3007();
        return \u3007\u3007808\u3007 != null && !TextUtils.isEmpty((CharSequence)\u3007\u3007808\u3007.d());
    }
    
    public static boolean Oo08() {
        final e \u3007\u3007808\u3007 = i.\u3007\u30078O0\u30078().\u3007\u3007808\u3007();
        return \u3007\u3007808\u3007 != null && !TextUtils.isEmpty((CharSequence)\u3007\u3007808\u3007.a());
    }
    
    public static boolean oO80() {
        final e \u3007\u3007808\u3007 = i.\u3007\u30078O0\u30078().\u3007\u3007808\u3007();
        return \u3007\u3007808\u3007 != null && !TextUtils.isEmpty((CharSequence)\u3007\u3007808\u3007.c());
    }
    
    public static boolean o\u30070() {
        final e \u3007\u3007808\u3007 = i.\u3007\u30078O0\u30078().\u3007\u3007808\u3007();
        return \u3007\u3007808\u3007 != null && !TextUtils.isEmpty((CharSequence)\u3007\u3007808\u3007.f());
    }
    
    public static long \u3007080(final int n, final Context context) {
        final long \u3007o\u3007 = \u3007o\u3007(n, context);
        final StringBuilder sb = new StringBuilder();
        sb.append("ad limit by memory:");
        sb.append(\u3007o\u3007);
        c.\u3007080(sb.toString());
        return \u3007o\u3007;
    }
    
    public static boolean \u3007o00\u3007\u3007Oo() {
        final e \u3007\u3007808\u3007 = i.\u3007\u30078O0\u30078().\u3007\u3007808\u3007();
        return \u3007\u3007808\u3007 != null && !TextUtils.isEmpty((CharSequence)\u3007\u3007808\u3007.b());
    }
    
    private static long \u3007o\u3007(final int n, final Context context) {
        if (context == null) {
            return n;
        }
        final Runtime runtime = Runtime.getRuntime();
        final long n2 = runtime.freeMemory() / 1048576L;
        final long n3 = runtime.maxMemory() / 1048576L - runtime.totalMemory() / 1048576L;
        if (n3 <= 0L) {
            if (n2 <= 2L) {
                return 1L;
            }
            if (n2 <= 10L) {
                return Math.min(n, 10);
            }
            return Math.min(n2 / 2L * 10L, n);
        }
        else {
            final long n4 = (n2 + n3 - 10L) / 2L;
            if (n4 <= 2L) {
                return 1L;
            }
            if (n4 <= 10L) {
                return Math.min(n, 10);
            }
            return Math.min(n4 * 10L, n);
        }
    }
    
    public static boolean \u3007\u3007888() {
        final e \u3007\u3007808\u3007 = i.\u3007\u30078O0\u30078().\u3007\u3007808\u3007();
        return \u3007\u3007808\u3007 != null && !TextUtils.isEmpty((CharSequence)\u3007\u3007808\u3007.e());
    }
}

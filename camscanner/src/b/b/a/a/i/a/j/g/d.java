// 
// Decompiled by Procyon v0.6.0
// 

package b.b.a.a.i.a.j.g;

import b.b.a.a.i.a.j.g.h.f;
import b.b.a.a.i.a.j.g.h.g;
import b.b.a.a.i.a.j.g.h.a;
import b.b.a.a.i.a.l.c;
import android.database.Cursor;
import java.util.Iterator;
import java.util.ArrayList;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteDatabase$CursorFactory;
import android.content.Context;
import android.database.sqlite.SQLiteOpenHelper;

public class d extends SQLiteOpenHelper
{
    final Context o0;
    
    public d(final Context o0) {
        super(o0, "ttadlog.db", (SQLiteDatabase$CursorFactory)null, 1);
        this.o0 = o0;
    }
    
    private void Oo08(final SQLiteDatabase sqLiteDatabase) {
        final ArrayList<String> oo80 = this.oO80(sqLiteDatabase);
        if (oo80 != null && oo80.size() > 0) {
            final Iterator iterator = oo80.iterator();
            while (iterator.hasNext()) {
                sqLiteDatabase.execSQL(String.format("DROP TABLE IF EXISTS %s ;", iterator.next()));
            }
        }
    }
    
    private ArrayList<String> oO80(final SQLiteDatabase sqLiteDatabase) {
        final ArrayList list = new ArrayList();
        try {
            final Cursor rawQuery = sqLiteDatabase.rawQuery("select name from sqlite_master where type='table' order by name", (String[])null);
            if (rawQuery != null) {
                while (rawQuery.moveToNext()) {
                    final String string = rawQuery.getString(0);
                    if (!string.equals("android_metadata") && !string.equals("sqlite_sequence")) {
                        list.add(string);
                    }
                }
                rawQuery.close();
            }
            return list;
        }
        catch (final Exception ex) {
            return list;
        }
    }
    
    private void \u3007\u3007888(final SQLiteDatabase sqLiteDatabase) {
        c.\u3007o00\u3007\u3007Oo("DatabaseHelper", "initDB........");
        sqLiteDatabase.execSQL(a.Oooo8o0\u3007());
        sqLiteDatabase.execSQL(b.b.a.a.i.a.j.g.h.d.\u30070\u3007O0088o());
        sqLiteDatabase.execSQL(g.Oooo8o0\u3007());
        sqLiteDatabase.execSQL(f.\u30070\u3007O0088o());
        sqLiteDatabase.execSQL(b.b.a.a.i.a.o.f.O8());
    }
    
    public void onCreate(final SQLiteDatabase sqLiteDatabase) {
        try {
            this.\u3007\u3007888(sqLiteDatabase);
        }
        finally {}
    }
    
    public void onUpgrade(final SQLiteDatabase sqLiteDatabase, final int i, final int j) {
        try {
            final StringBuilder sb = new StringBuilder();
            sb.append("onUpgrade....Database version upgrade.....old:");
            sb.append(i);
            sb.append(",new:");
            sb.append(j);
            c.\u3007o00\u3007\u3007Oo("DatabaseHelper", sb.toString());
            if (i > j) {
                this.Oo08(sqLiteDatabase);
                this.\u3007\u3007888(sqLiteDatabase);
                c.\u3007o00\u3007\u3007Oo("DatabaseHelper", "onUpgrade...Reverse installation Database reset - create table.....");
            }
            else {
                this.\u3007\u3007888(sqLiteDatabase);
            }
        }
        finally {}
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package b.b.a.a.i.a;

import android.os.Parcelable;
import android.os.Parcel;
import android.os.IBinder;
import android.os.Binder;
import java.util.Map;
import android.os.RemoteException;
import android.content.ContentValues;
import android.net.Uri;
import android.os.IInterface;

public interface g extends IInterface
{
    int a(final Uri p0, final ContentValues p1, final String p2, final String[] p3) throws RemoteException;
    
    int a(final Uri p0, final String p1, final String[] p2) throws RemoteException;
    
    String a(final Uri p0) throws RemoteException;
    
    String a(final Uri p0, final ContentValues p1) throws RemoteException;
    
    Map a(final Uri p0, final String[] p1, final String p2, final String[] p3, final String p4) throws RemoteException;
    
    public abstract static class a extends Binder implements g
    {
        public a() {
            this.attachInterface((IInterface)this, "com.bytedance.sdk.component.log.impl.IListenerEventManager");
        }
        
        public static g a() {
            return g.a.a.\u3007OOo8\u30070;
        }
        
        public static g a(final IBinder binder) {
            if (binder == null) {
                return null;
            }
            final IInterface queryLocalInterface = binder.queryLocalInterface("com.bytedance.sdk.component.log.impl.IListenerEventManager");
            if (queryLocalInterface != null && queryLocalInterface instanceof g) {
                return (g)queryLocalInterface;
            }
            return new g.a.a(binder);
        }
        
        public IBinder asBinder() {
            return (IBinder)this;
        }
        
        public boolean onTransact(int n, final Parcel parcel, final Parcel parcel2, final int n2) throws RemoteException {
            if (n == 1598968902) {
                parcel2.writeString("com.bytedance.sdk.component.log.impl.IListenerEventManager");
                return true;
            }
            final Uri uri = null;
            ContentValues contentValues = null;
            final Uri uri2 = null;
            Uri uri3 = null;
            final ContentValues contentValues2 = null;
            if (n == 1) {
                parcel.enforceInterface("com.bytedance.sdk.component.log.impl.IListenerEventManager");
                if (parcel.readInt() != 0) {
                    uri3 = (Uri)Uri.CREATOR.createFromParcel(parcel);
                }
                final Map a = this.a(uri3, parcel.createStringArray(), parcel.readString(), parcel.createStringArray(), parcel.readString());
                parcel2.writeNoException();
                parcel2.writeMap(a);
                return true;
            }
            if (n == 2) {
                parcel.enforceInterface("com.bytedance.sdk.component.log.impl.IListenerEventManager");
                Uri uri4 = uri2;
                if (parcel.readInt() != 0) {
                    uri4 = (Uri)Uri.CREATOR.createFromParcel(parcel);
                }
                final String a2 = this.a(uri4);
                parcel2.writeNoException();
                parcel2.writeString(a2);
                return true;
            }
            if (n == 3) {
                parcel.enforceInterface("com.bytedance.sdk.component.log.impl.IListenerEventManager");
                Uri uri5;
                if (parcel.readInt() != 0) {
                    uri5 = (Uri)Uri.CREATOR.createFromParcel(parcel);
                }
                else {
                    uri5 = null;
                }
                if (parcel.readInt() != 0) {
                    contentValues = (ContentValues)ContentValues.CREATOR.createFromParcel(parcel);
                }
                final String a3 = this.a(uri5, contentValues);
                parcel2.writeNoException();
                parcel2.writeString(a3);
                return true;
            }
            if (n == 4) {
                parcel.enforceInterface("com.bytedance.sdk.component.log.impl.IListenerEventManager");
                Uri uri6 = uri;
                if (parcel.readInt() != 0) {
                    uri6 = (Uri)Uri.CREATOR.createFromParcel(parcel);
                }
                n = this.a(uri6, parcel.readString(), parcel.createStringArray());
                parcel2.writeNoException();
                parcel2.writeInt(n);
                return true;
            }
            if (n != 5) {
                return super.onTransact(n, parcel, parcel2, n2);
            }
            parcel.enforceInterface("com.bytedance.sdk.component.log.impl.IListenerEventManager");
            Uri uri7;
            if (parcel.readInt() != 0) {
                uri7 = (Uri)Uri.CREATOR.createFromParcel(parcel);
            }
            else {
                uri7 = null;
            }
            ContentValues contentValues3 = contentValues2;
            if (parcel.readInt() != 0) {
                contentValues3 = (ContentValues)ContentValues.CREATOR.createFromParcel(parcel);
            }
            n = this.a(uri7, contentValues3, parcel.readString(), parcel.createStringArray());
            parcel2.writeNoException();
            parcel2.writeInt(n);
            return true;
        }
        
        private static class a implements g
        {
            public static g \u3007OOo8\u30070;
            private IBinder o0;
            
            a(final IBinder o0) {
                this.o0 = o0;
            }
            
            @Override
            public int a(final Uri uri, final ContentValues contentValues, final String s, final String[] array) throws RemoteException {
                final Parcel obtain = Parcel.obtain();
                final Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.bytedance.sdk.component.log.impl.IListenerEventManager");
                    if (uri != null) {
                        obtain.writeInt(1);
                        ((Parcelable)uri).writeToParcel(obtain, 0);
                    }
                    else {
                        obtain.writeInt(0);
                    }
                    if (contentValues != null) {
                        obtain.writeInt(1);
                        contentValues.writeToParcel(obtain, 0);
                    }
                    else {
                        obtain.writeInt(0);
                    }
                    obtain.writeString(s);
                    obtain.writeStringArray(array);
                    if (!this.o0.transact(5, obtain, obtain2, 0) && g.a.a() != null) {
                        return g.a.a().a(uri, contentValues, s, array);
                    }
                    obtain2.readException();
                    return obtain2.readInt();
                }
                finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }
            
            @Override
            public int a(final Uri uri, final String s, final String[] array) throws RemoteException {
                final Parcel obtain = Parcel.obtain();
                final Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.bytedance.sdk.component.log.impl.IListenerEventManager");
                    if (uri != null) {
                        obtain.writeInt(1);
                        ((Parcelable)uri).writeToParcel(obtain, 0);
                    }
                    else {
                        obtain.writeInt(0);
                    }
                    obtain.writeString(s);
                    obtain.writeStringArray(array);
                    if (!this.o0.transact(4, obtain, obtain2, 0) && g.a.a() != null) {
                        return g.a.a().a(uri, s, array);
                    }
                    obtain2.readException();
                    return obtain2.readInt();
                }
                finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }
            
            @Override
            public String a(final Uri uri) throws RemoteException {
                final Parcel obtain = Parcel.obtain();
                final Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.bytedance.sdk.component.log.impl.IListenerEventManager");
                    if (uri != null) {
                        obtain.writeInt(1);
                        ((Parcelable)uri).writeToParcel(obtain, 0);
                    }
                    else {
                        obtain.writeInt(0);
                    }
                    if (!this.o0.transact(2, obtain, obtain2, 0) && g.a.a() != null) {
                        return g.a.a().a(uri);
                    }
                    obtain2.readException();
                    return obtain2.readString();
                }
                finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }
            
            @Override
            public String a(final Uri uri, final ContentValues contentValues) throws RemoteException {
                final Parcel obtain = Parcel.obtain();
                final Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.bytedance.sdk.component.log.impl.IListenerEventManager");
                    if (uri != null) {
                        obtain.writeInt(1);
                        ((Parcelable)uri).writeToParcel(obtain, 0);
                    }
                    else {
                        obtain.writeInt(0);
                    }
                    if (contentValues != null) {
                        obtain.writeInt(1);
                        contentValues.writeToParcel(obtain, 0);
                    }
                    else {
                        obtain.writeInt(0);
                    }
                    if (!this.o0.transact(3, obtain, obtain2, 0) && g.a.a() != null) {
                        return g.a.a().a(uri, contentValues);
                    }
                    obtain2.readException();
                    return obtain2.readString();
                }
                finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }
            
            @Override
            public Map a(final Uri uri, final String[] array, final String s, final String[] array2, final String s2) throws RemoteException {
                final Parcel obtain = Parcel.obtain();
                final Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.bytedance.sdk.component.log.impl.IListenerEventManager");
                    if (uri != null) {
                        obtain.writeInt(1);
                        ((Parcelable)uri).writeToParcel(obtain, 0);
                    }
                    else {
                        obtain.writeInt(0);
                    }
                    obtain.writeStringArray(array);
                    obtain.writeString(s);
                    obtain.writeStringArray(array2);
                    obtain.writeString(s2);
                    if (!this.o0.transact(1, obtain, obtain2, 0) && g.a.a() != null) {
                        return g.a.a().a(uri, array, s, array2, s2);
                    }
                    obtain2.readException();
                    return obtain2.readHashMap(this.getClass().getClassLoader());
                }
                finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }
            
            public IBinder asBinder() {
                return this.o0;
            }
        }
    }
}

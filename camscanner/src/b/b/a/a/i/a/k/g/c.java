// 
// Decompiled by Procyon v0.6.0
// 

package b.b.a.a.i.a.k.g;

import android.os.Message;
import java.util.concurrent.Executor;
import org.json.JSONArray;
import org.json.JSONObject;
import java.util.concurrent.TimeUnit;
import java.util.Iterator;
import b.b.a.a.i.a.e;
import b.b.a.a.i.a.f;
import b.b.a.a.i.a.i;
import java.util.Collection;
import b.b.a.a.i.a.j.b;
import java.util.ArrayList;
import java.util.concurrent.PriorityBlockingQueue;
import java.util.concurrent.atomic.AtomicInteger;
import b.b.a.a.i.a.m.a;
import java.util.List;
import android.os.Handler;
import b.b.a.a.i.a.j.d;
import android.os.Handler$Callback;
import android.os.HandlerThread;

public class c extends HandlerThread implements Handler$Callback
{
    private static int O0O = 200;
    private static int \u3007\u300708O = 10;
    private volatile int O8o08O8O;
    protected d OO;
    private volatile Handler OO\u300700\u30078oO;
    private volatile boolean o0;
    private final List<a> o8\u3007OO0\u30070o;
    private final AtomicInteger oOo0;
    private final AtomicInteger oOo\u30078o008;
    private final AtomicInteger ooo0\u3007\u3007O;
    private final PriorityBlockingQueue<a> o\u300700O;
    private volatile long \u3007080OO8\u30070;
    private b.b.a.a.i.a.k.c \u300708O\u300700\u3007o;
    private volatile long \u30070O;
    private final AtomicInteger \u30078\u3007oO\u3007\u30078o;
    private final Object \u3007OOo8\u30070;
    
    public c(final PriorityBlockingQueue<a> o\u300700O) {
        super("csj_log");
        this.o0 = true;
        this.\u3007OOo8\u30070 = new Object();
        this.\u3007080OO8\u30070 = 0L;
        this.\u30070O = 0L;
        this.oOo\u30078o008 = new AtomicInteger(0);
        this.oOo0 = new AtomicInteger(0);
        this.o8\u3007OO0\u30070o = new ArrayList<a>();
        this.\u30078\u3007oO\u3007\u30078o = new AtomicInteger(0);
        this.ooo0\u3007\u3007O = new AtomicInteger(0);
        this.o\u300700O = o\u300700O;
        this.OO = new b();
    }
    
    private boolean O8\u3007o() {
        return b.b.a.a.i.a.k.d.\u3007\u3007888.\u3007o00\u3007\u3007Oo && (this.O8o08O8O == 4 || this.O8o08O8O == 7 || this.O8o08O8O == 6 || this.O8o08O8O == 5 || this.O8o08O8O == 2);
    }
    
    private void OO0o\u3007\u3007(final List<a> list) {
        this.o8\u3007OO0\u30070o.addAll(list);
        final StringBuilder sb = new StringBuilder();
        sb.append("a batch applog generation cur=");
        sb.append(this.o8\u3007OO0\u30070o.size());
        b.b.a.a.i.a.l.c.O8("PADLT", sb.toString());
        final f \u3007080 = i.\u3007\u30078O0\u30078().\u3007080();
        if (\u3007080 != null) {
            \u3007080.h();
        }
        if (this.o8\u3007OO0\u30070o.size() >= c.\u3007\u300708O) {
            if (this.OO\u300700\u30078oO.hasMessages(11)) {
                this.OO\u300700\u30078oO.removeMessages(11);
            }
            final ArrayList list2 = new ArrayList((Collection<? extends E>)this.o8\u3007OO0\u30070o);
            this.o8\u3007OO0\u30070o.clear();
            this.\u3007O00(list2, false, "max_size_dispatch");
            this.\u3007o00\u3007\u3007Oo();
            final StringBuilder sb2 = new StringBuilder();
            sb2.append("batch applog report ( size ) ");
            sb2.append(c.\u3007\u300708O);
            b.b.a.a.i.a.l.c.O8("PADLT", sb2.toString());
        }
        else if (this.o\u300700O.size() == 0) {
            this.\u3007\u30078O0\u30078(false);
            if (this.OO\u300700\u30078oO.hasMessages(11)) {
                this.OO\u300700\u30078oO.removeMessages(11);
            }
            if (this.OO\u300700\u30078oO.hasMessages(1)) {
                this.OO\u300700\u30078oO.removeMessages(1);
            }
            final long lng = c.O0O;
            if (\u3007080 != null) {
                \u3007080.h();
            }
            this.OO\u300700\u30078oO.sendEmptyMessageDelayed(11, lng);
            final StringBuilder sb3 = new StringBuilder();
            sb3.append("batch applog report delay ( time )");
            sb3.append(lng);
            b.b.a.a.i.a.l.c.O8("PADLT", sb3.toString());
        }
        else {
            final StringBuilder sb4 = new StringBuilder();
            sb4.append("uploadBatchOptimize nothing\uff1a");
            sb4.append(this.o\u300700O.size());
            sb4.append("  ");
            sb4.append(this.o0);
            b.b.a.a.i.a.l.c.\u3007080(sb4.toString());
        }
    }
    
    private void OO0o\u3007\u3007\u3007\u30070(final a a, final int n) {
        this.oOo\u30078o008.set(0);
        b.b.a.a.i.a.l.c.\u3007080("handleThreadMessage()");
        if (n == 0) {
            this.O8o08O8O = ((b.b.a.a.i.a.m.b)a).O8();
            if (this.O8o08O8O != 6) {
                b.b.a.a.i.a.l.b.\u3007080(b.b.a.a.i.a.k.d.oO80.\u3007o(), 1);
                this.oo88o8O(a);
            }
        }
        else {
            final b.b.a.a.i.a.m.b b = (b.b.a.a.i.a.m.b)a;
            if (b.O8() == 1) {
                this.O8o08O8O = 1;
                this.oo88o8O(a);
            }
            else if (b.O8() == 2) {
                final StringBuilder sb = new StringBuilder();
                sb.append("before size:");
                sb.append(n);
                b.b.a.a.i.a.l.c.\u3007080(sb.toString());
                this.oo\u3007();
                final StringBuilder sb2 = new StringBuilder();
                sb2.append("after size :");
                sb2.append(n);
                b.b.a.a.i.a.l.c.\u3007080(sb2.toString());
                this.O8o08O8O = 2;
                this.oo88o8O(a);
            }
        }
    }
    
    public static void OOO\u3007O0(final int n) {
        c.O0O = n;
        final StringBuilder sb = new StringBuilder();
        sb.append("applog_interval=");
        sb.append(n);
        b.b.a.a.i.a.l.c.O8("PADLT", sb.toString());
    }
    
    private void Oo08(final int n, final List<a> list, final long n2) {
        final Object \u3007oOo8\u30070 = this.\u3007OOo8\u30070;
        monitorenter(\u3007oOo8\u30070);
        if (list == null) {
            return;
        }
        try {
            if (this.OO\u300700\u30078oO == null) {
                return;
            }
            b.b.a.a.i.a.l.a.\u3007o\u3007(n, list, n2);
            this.OO.\u3007o\u3007(n, list);
            final f \u3007080 = i.\u3007\u30078O0\u30078().\u3007080();
            if (\u3007080 != null) {
                \u3007080.h();
            }
            Label_0409: {
                Label_0315: {
                    if (n != -2) {
                        if (n != -1) {
                            if (n == 0) {
                                break Label_0315;
                            }
                            if (n != 200) {
                                if (n != 509) {
                                    break Label_0409;
                                }
                                final b.b.a.a.i.a.k.d \u3007\u3007888 = b.b.a.a.i.a.k.d.\u3007\u3007888;
                                \u3007\u3007888.\u3007o00\u3007\u3007Oo = true;
                                \u3007\u3007888.\u3007o\u3007 = false;
                                b.b.a.a.i.a.l.c.\u3007080("-----------------  server busy start---------------- ");
                                if (this.OO\u300700\u30078oO.hasMessages(2)) {
                                    b.b.a.a.i.a.l.c.\u3007080("already server busy message");
                                    return;
                                }
                                if (System.currentTimeMillis() - this.\u3007080OO8\u30070 < 30000L) {
                                    b.b.a.a.i.a.l.c.\u3007080("already server busy\uff0ctoo short");
                                    return;
                                }
                                this.\u3007080OO8\u30070 = System.currentTimeMillis();
                                b.b.a.a.i.a.l.c.\u3007080("-----------------  server busy send---------------- ");
                                if (this.OO\u300700\u30078oO.hasMessages(3)) {
                                    this.OO\u300700\u30078oO.removeMessages(3);
                                }
                                this.O8(2, 30000L);
                                break Label_0409;
                            }
                        }
                        final b.b.a.a.i.a.k.d \u3007\u3007889 = b.b.a.a.i.a.k.d.\u3007\u3007888;
                        if (\u3007\u3007889.\u3007o00\u3007\u3007Oo || \u3007\u3007889.\u3007o\u3007) {
                            \u3007\u3007889.\u3007o00\u3007\u3007Oo = false;
                            \u3007\u3007889.\u3007o\u3007 = false;
                            if (this.OO\u300700\u30078oO.hasMessages(2)) {
                                this.OO\u300700\u30078oO.removeMessages(2);
                            }
                            if (this.OO\u300700\u30078oO.hasMessages(3)) {
                                this.OO\u300700\u30078oO.removeMessages(3);
                            }
                            this.\u30070O = 0L;
                            this.\u3007080OO8\u30070 = 0L;
                            this.\u30078\u3007oO\u3007\u30078o.set(0);
                            this.ooo0\u3007\u3007O.set(0);
                            b.b.a.a.i.a.l.c.\u3007080("--dispatchResult flush--");
                            b.b.a.a.i.a.l.b.\u3007080(b.b.a.a.i.a.k.d.oO80.O8\u3007o(), 1);
                            this.O8ooOoo\u3007(2);
                        }
                        break Label_0409;
                    }
                }
                final b.b.a.a.i.a.k.d \u3007\u3007890 = b.b.a.a.i.a.k.d.\u3007\u3007888;
                \u3007\u3007890.\u3007o00\u3007\u3007Oo = false;
                \u3007\u3007890.\u3007o\u3007 = true;
                if (this.OO\u300700\u30078oO.hasMessages(3)) {
                    b.b.a.a.i.a.l.c.\u3007080("already routine error message");
                    return;
                }
                if (System.currentTimeMillis() - this.\u30070O < 15000L) {
                    b.b.a.a.i.a.l.c.\u3007080("already routine error,too short");
                    return;
                }
                this.\u30070O = System.currentTimeMillis();
                if (this.OO\u300700\u30078oO.hasMessages(2)) {
                    this.OO\u300700\u30078oO.removeMessages(2);
                }
                this.O8(3, 15000L);
            }
            if (this.O8o08O8O == 2) {
                b.b.a.a.i.a.l.c.\u3007080("send notify");
                this.\u3007OOo8\u30070.notify();
            }
            monitorexit(\u3007oOo8\u30070);
            final StringBuilder sb = new StringBuilder();
            sb.append("upload thread reuse or close: ");
            sb.append(this.o0);
            sb.append(" queue:");
            sb.append(this.o\u300700O.size());
            sb.append(" ");
            sb.append(this.O8o08O8O);
            b.b.a.a.i.a.l.c.Oo08(sb.toString());
        }
        finally {
            monitorexit(\u3007oOo8\u30070);
        }
    }
    
    private void Oooo8o0\u3007(final List<a> list, final String s) {
        this.\u3007O8o08O(s);
        this.\u3007O00(list, false, s);
        this.\u3007o00\u3007\u3007Oo();
    }
    
    private void O\u30078O8\u3007008() {
        try {
            if (this.o\u300700O.size() == 0 && this.OO\u300700\u30078oO.hasMessages(11) && this.\u30070000OOO()) {
                this.\u3007\u30078O0\u30078(false);
            }
        }
        catch (final Exception ex) {
            b.b.a.a.i.a.l.c.\u3007o\u3007(ex.getMessage());
        }
    }
    
    private void oo88o8O(final a a) {
        final boolean o8\u3007o = this.O8\u3007o();
        int i;
        final int n = i = 0;
        Label_0129: {
            if (o8\u3007o) {
                final StringBuilder sb = new StringBuilder();
                sb.append("upload cancel:");
                sb.append(b.b.a.a.i.a.l.a.\u3007o00\u3007\u3007Oo(this.O8o08O8O));
                b.b.a.a.i.a.l.c.\u3007\u3007888(sb.toString());
                b.b.a.a.i.a.l.b.\u3007080(b.b.a.a.i.a.k.d.oO80.oO00OOO(), 1);
                if (this.o\u300700O.size() == 0) {
                    if (!this.OO\u300700\u30078oO.hasMessages(2)) {
                        b.b.a.a.i.a.k.d.\u3007\u3007888.\u3007o00\u3007\u3007Oo = false;
                        this.\u30070O = 0L;
                        this.\u3007080OO8\u30070 = 0L;
                        this.\u30078\u3007oO\u3007\u30078o.set(0);
                        this.ooo0\u3007\u3007O.set(0);
                        i = n;
                        break Label_0129;
                    }
                    this.\u3007\u30078O0\u30078(false);
                }
                return;
            }
            boolean ooO8;
            int j;
            do {
                ooO8 = this.OoO8(this.O8o08O8O, b.b.a.a.i.a.k.d.\u3007\u3007888.\u3007o00\u3007\u3007Oo);
                b.b.a.a.i.a.l.a.OO0o\u3007\u3007\u3007\u30070(ooO8, this.O8o08O8O, a);
                b.b.a.a.i.a.l.b.\u3007080(b.b.a.a.i.a.k.d.oO80.o\u3007O(), 1);
                if (ooO8) {
                    final List<a> \u3007080 = this.OO.\u3007080(this.O8o08O8O, -1, null);
                    if (\u3007080 != null) {
                        final StringBuilder sb2 = new StringBuilder();
                        sb2.append("upload size=");
                        sb2.append(\u3007080.size());
                        sb2.append("  times=");
                        sb2.append(i);
                        b.b.a.a.i.a.l.c.\u3007\u3007888(sb2.toString());
                        this.\u3007oo\u3007(\u3007080);
                    }
                    else {
                        b.b.a.a.i.a.l.c.Oo08("no need upload");
                        this.O\u30078O8\u3007008();
                    }
                }
                else {
                    this.O\u30078O8\u3007008();
                }
                j = i + 1;
                final StringBuilder sb3 = new StringBuilder();
                sb3.append("times=");
                sb3.append(j);
                b.b.a.a.i.a.l.c.Oo08(sb3.toString());
            } while (ooO8 && (i = j) <= 6);
        }
    }
    
    private void oo\u3007() {
        if (this.o\u300700O.size() >= 100) {
            for (int i = 0; i < 100; ++i) {
                final a a = this.o\u300700O.poll();
                if (a instanceof b.b.a.a.i.a.m.b) {
                    b.b.a.a.i.a.l.c.\u3007080("ignore tm");
                }
                else if (a != null) {
                    this.\u300780\u3007808\u3007O(a);
                }
                else {
                    b.b.a.a.i.a.l.c.\u3007o\u3007("event == null");
                }
            }
        }
    }
    
    private void o\u30070(final b.b.a.a.i.a.k.g.b b, final List<a> list) {
        if (b != null && b.\u3007080) {
            final List<e> \u3007080 = b.b.a.a.i.a.b.\u3007080();
            if (list != null && \u3007080 != null && \u3007080.size() != 0) {
                for (final a a : list) {
                    if (a.c() == 1) {
                        final String \u3007\u3007808\u3007 = b.b.a.a.i.a.l.a.\u3007\u3007808\u3007(a);
                        final String o800o8O = b.b.a.a.i.a.l.a.o800o8O(a);
                        for (final e e : \u3007080) {
                            if (e != null) {
                                e.a(\u3007\u3007808\u3007, o800o8O);
                            }
                        }
                    }
                }
            }
        }
    }
    
    private void o\u3007O8\u3007\u3007o(final List<a> list, final boolean b, final long n) {
        this.oOo0.incrementAndGet();
        b.b.a.a.i.a.l.b.\u3007080(b.b.a.a.i.a.k.d.oO80.\u30078o8o\u3007(), 1);
        try {
            this.\u300708O\u300700\u3007o.a(list, new b.b.a.a.i.a.k.b(this, b, n) {
                final boolean \u3007080;
                final long \u3007o00\u3007\u3007Oo;
                final c \u3007o\u3007;
                
                @Override
                public void a(final List<b.b.a.a.i.a.k.g.a> list) {
                    try {
                        this.\u3007o\u3007.oOo0.decrementAndGet();
                        if (list != null && list.size() != 0) {
                            for (int size = list.size(), i = 0; i < size; ++i) {
                                final b.b.a.a.i.a.k.g.a a = list.get(i);
                                if (a != null) {
                                    this.\u3007o\u3007.\u30070\u3007O0088o(this.\u3007080, a.\u3007o00\u3007\u3007Oo(), a.\u3007080(), this.\u3007o00\u3007\u3007Oo);
                                }
                            }
                        }
                    }
                    catch (final Exception ex) {
                        ex.printStackTrace();
                    }
                }
            });
        }
        catch (final Exception ex) {
            final StringBuilder sb = new StringBuilder();
            sb.append("outer exception\uff1a");
            sb.append(ex.getMessage());
            b.b.a.a.i.a.l.c.\u3007o\u3007(sb.toString());
            b.b.a.a.i.a.l.b.\u3007080(b.b.a.a.i.a.k.d.oO80.\u300780\u3007808\u3007O(), 1);
            this.oOo0.decrementAndGet();
        }
    }
    
    private void o\u3007\u30070\u3007() {
        while (this.\u30070000OOO()) {
            try {
                final b.b.a.a.i.a.k.e.a oo80 = b.b.a.a.i.a.k.d.oO80;
                b.b.a.a.i.a.l.b.\u3007080(oo80.o\u30070(), 1);
                final a a = this.o\u300700O.poll(60000L, TimeUnit.MILLISECONDS);
                final int size = this.o\u300700O.size();
                final StringBuilder sb = new StringBuilder();
                sb.append("poll size:");
                sb.append(size);
                b.b.a.a.i.a.l.c.\u3007080(sb.toString());
                if (a instanceof b.b.a.a.i.a.m.b) {
                    this.OO0o\u3007\u3007\u3007\u30070(a, size);
                }
                else if (a == null) {
                    final int incrementAndGet = this.oOo\u30078o008.incrementAndGet();
                    b.b.a.a.i.a.l.b.\u3007080(oo80.o\u30078oOO88(), 1);
                    if (this.\u300700(incrementAndGet)) {
                        this.\u3007O888o0o();
                        break;
                    }
                    if (incrementAndGet >= 4) {
                        continue;
                    }
                    final StringBuilder sb2 = new StringBuilder();
                    sb2.append("timeoutCount:");
                    sb2.append(incrementAndGet);
                    b.b.a.a.i.a.l.c.\u3007080(sb2.toString());
                    this.O8o08O8O = 1;
                    this.oo88o8O(null);
                }
                else {
                    this.\u300780\u3007808\u3007O(a);
                    this.oo88o8O(a);
                }
            }
            finally {
                final StringBuilder sb3 = new StringBuilder();
                sb3.append("run exception:");
                final Throwable t;
                sb3.append(t.getMessage());
                b.b.a.a.i.a.l.c.\u3007o\u3007(sb3.toString());
                b.b.a.a.i.a.l.b.\u3007080(b.b.a.a.i.a.k.d.oO80.\u300780\u3007808\u3007O(), 1);
            }
        }
    }
    
    private boolean \u300700(final int n) {
        if (n >= 4 && this.oOo0.get() == 0) {
            final b.b.a.a.i.a.k.d \u3007\u3007888 = b.b.a.a.i.a.k.d.\u3007\u3007888;
            if (!\u3007\u3007888.\u3007o00\u3007\u3007Oo && !\u3007\u3007888.\u3007o\u3007) {
                return true;
            }
        }
        return false;
    }
    
    private void \u300700\u30078() {
        if (!((Thread)this).isAlive()) {
            b.b.a.a.i.a.l.c.\u3007080("th dead");
            b.b.a.a.i.a.k.d.\u3007\u3007888.oO80();
        }
        else if (!this.\u30070000OOO()) {
            b.b.a.a.i.a.l.c.\u3007080("monitor  mLogThread ");
            this.O8ooOoo\u3007(6);
        }
    }
    
    private void \u30070\u3007O0088o(final boolean b, final b.b.a.a.i.a.k.g.b b2, final List<a> list, final long n) {
        if (!b && b2 != null) {
            final int \u3007o00\u3007\u3007Oo = b2.\u3007o00\u3007\u3007Oo;
            final boolean o8 = b2.O8;
            final int n2 = -2;
            int n3;
            if (o8) {
                n3 = -1;
            }
            else if ((n3 = \u3007o00\u3007\u3007Oo) < 0) {
                n3 = -2;
            }
            int n4;
            if (n3 == 510 || (n4 = n3) == 511) {
                n4 = -2;
            }
            int i = 0;
            Label_0116: {
                if (!b2.\u3007080) {
                    if (n4 >= 500) {
                        i = n2;
                        if (n4 < 509) {
                            break Label_0116;
                        }
                    }
                    if (n4 > 513) {
                        i = n2;
                        break Label_0116;
                    }
                }
                i = n4;
            }
            if (list != null) {
                final StringBuilder sb = new StringBuilder();
                sb.append("preprocessResult code is ");
                sb.append(i);
                sb.append(" sz:");
                sb.append(list.size());
                sb.append("  count:");
                sb.append(this.oOo0.get());
                b.b.a.a.i.a.l.c.\u3007080(sb.toString());
            }
            this.Oo08(i, list, n);
        }
    }
    
    private void \u300780\u3007808\u3007O(final a a) {
        this.oOo\u30078o008.set(0);
        final b.b.a.a.i.a.k.d \u3007\u3007888 = b.b.a.a.i.a.k.d.\u3007\u3007888;
        if (\u3007\u3007888.\u3007o00\u3007\u3007Oo) {
            this.O8o08O8O = 5;
        }
        else if (\u3007\u3007888.\u3007o\u3007) {
            this.O8o08O8O = 7;
        }
        else {
            this.O8o08O8O = 4;
        }
        b.b.a.a.i.a.l.b.\u3007080(b.b.a.a.i.a.k.d.oO80.o8oO\u3007(), 1);
        this.OO.\u3007o00\u3007\u3007Oo(a, this.O8o08O8O);
        b.b.a.a.i.a.l.a.\u3007oOO8O8(a);
    }
    
    private void \u3007O00(final List<a> list, final boolean b, final String s) {
        final long currentTimeMillis = System.currentTimeMillis();
        b.b.a.a.i.a.l.a.\u3007\u3007888(list, this.O8o08O8O, s);
        final b.b.a.a.i.a.k.c \u3007o888o0o = i.\u3007\u30078O0\u30078().\u3007O888o0o();
        this.\u300708O\u300700\u3007o = \u3007o888o0o;
        if (\u3007o888o0o != null) {
            this.o\u3007O8\u3007\u3007o(list, b, currentTimeMillis);
        }
        else {
            this.\u3007\u3007808\u3007(list, b, currentTimeMillis);
        }
    }
    
    private void \u3007O888o0o() {
        b.b.a.a.i.a.l.b.\u3007080(b.b.a.a.i.a.k.d.oO80.\u300700(), 1);
        this.\u3007\u30078O0\u30078(false);
        b.b.a.a.i.a.k.d.\u3007\u3007888.\u3007O8o08O();
        b.b.a.a.i.a.l.c.\u3007\u3007888("exit log thread");
    }
    
    private void \u3007O8o08O(final String s) {
        if (this.OO\u300700\u30078oO.hasMessages(11)) {
            this.OO\u300700\u30078oO.removeMessages(11);
        }
        if (this.o8\u3007OO0\u30070o.size() != 0) {
            final ArrayList list = new ArrayList((Collection<? extends E>)this.o8\u3007OO0\u30070o);
            this.o8\u3007OO0\u30070o.clear();
            final StringBuilder sb = new StringBuilder();
            sb.append("before_");
            sb.append(s);
            this.\u3007O00(list, false, sb.toString());
            this.\u3007o00\u3007\u3007Oo();
            final StringBuilder sb2 = new StringBuilder();
            sb2.append("applog batch reporting size = ");
            sb2.append(list.size());
            b.b.a.a.i.a.l.c.o\u30070("PADLT", sb2.toString());
        }
        else {
            final StringBuilder sb3 = new StringBuilder();
            sb3.append("ensureUploadOptBatch empty\uff1a");
            sb3.append(s);
            b.b.a.a.i.a.l.c.\u3007080(sb3.toString());
        }
    }
    
    private void \u3007O\u3007(final List<a> list, final boolean b, final long n, final int n2) {
        try {
            final a a = list.get(0);
            b.b.a.a.i.a.l.b.\u3007080(b.b.a.a.i.a.k.d.oO80.\u30078o8o\u3007(), 1);
            b.b.a.a.i.a.k.g.b a3;
            if (a.f() == 0) {
                final b.b.a.a.i.a.k.g.b a2 = i.\u30070\u3007O0088o().a(list);
                this.o\u30070(a2, list);
                if ((a3 = a2) != null) {
                    b.b.a.a.i.a.l.a.oO80(list, a2.\u3007o\u3007);
                    a3 = a2;
                }
            }
            else {
                final JSONObject jsonObject = new JSONObject();
                try {
                    final JSONArray jsonArray = new JSONArray();
                    final Iterator<a> iterator = list.iterator();
                    while (iterator.hasNext()) {
                        jsonArray.put((Object)iterator.next().b());
                    }
                    jsonObject.put("stats_list", (Object)jsonArray);
                }
                catch (final Exception ex) {
                    final StringBuilder sb = new StringBuilder();
                    sb.append("json exception:");
                    sb.append(ex.getMessage());
                    b.b.a.a.i.a.l.c.\u3007o\u3007(sb.toString());
                }
                a3 = i.\u30070\u3007O0088o().a(jsonObject);
            }
            this.oOo0.decrementAndGet();
            this.\u30070\u3007O0088o(b, a3, list, n);
        }
        finally {
            final StringBuilder sb2 = new StringBuilder();
            sb2.append("inner exception:");
            final Throwable t;
            sb2.append(t.getMessage());
            b.b.a.a.i.a.l.c.\u3007o\u3007(sb2.toString());
            b.b.a.a.i.a.l.b.\u3007080(b.b.a.a.i.a.k.d.oO80.\u300780\u3007808\u3007O(), 1);
            this.oOo0.decrementAndGet();
        }
    }
    
    private void \u3007o() {
        b.b.a.a.i.a.l.c.\u3007080("sendServerBusyOrRoutineErrorRetryMessage");
        this.\u300700\u30078();
        b.b.a.a.i.a.l.b.\u3007080(b.b.a.a.i.a.k.d.oO80.\u3007\u3007888(), 1);
        this.O8ooOoo\u3007(1);
    }
    
    private void \u3007o00\u3007\u3007Oo() {
        if (this.OO\u300700\u30078oO.hasMessages(11)) {
            this.O\u30078O8\u3007008();
        }
        else {
            this.\u3007o\u3007(1);
        }
        final StringBuilder sb = new StringBuilder();
        sb.append("afterUpload message:");
        sb.append(this.O8o08O8O);
        b.b.a.a.i.a.l.c.\u3007080(sb.toString());
        final b.b.a.a.i.a.k.e.a oo80 = b.b.a.a.i.a.k.d.oO80;
        b.b.a.a.i.a.l.b.\u3007080(oo80.\u3007\u30070o(), 1);
        if (this.O8o08O8O == 2) {
            b.b.a.a.i.a.l.b.\u3007080(oo80.o0ooO(), 1);
            final Object \u3007oOo8\u30070 = this.\u3007OOo8\u30070;
            monitorenter(\u3007oOo8\u30070);
            try {
                Label_0368: {
                    try {
                        final long nanoTime = System.nanoTime();
                        this.\u3007OOo8\u30070.wait(5000L);
                        final long lng = System.nanoTime() - nanoTime;
                        final StringBuilder sb2 = new StringBuilder();
                        sb2.append("afterUpload delta:");
                        sb2.append(lng);
                        sb2.append(" start:");
                        sb2.append(nanoTime);
                        sb2.append(" condition:");
                        final b.b.a.a.i.a.k.d \u3007\u3007888 = b.b.a.a.i.a.k.d.\u3007\u3007888;
                        sb2.append(\u3007\u3007888.\u3007o00\u3007\u3007Oo || \u3007\u3007888.\u3007o\u3007);
                        b.b.a.a.i.a.l.c.\u3007080(sb2.toString());
                        if (lng >= 5000000000L || 5000000000L - lng < 50000000L) {
                            b.b.a.a.i.a.l.c.\u3007o\u3007("afterUpload wait timeout");
                            b.b.a.a.i.a.l.b.\u3007080(oo80.o\u30078(), 1);
                            monitorexit(\u3007oOo8\u30070);
                            return;
                        }
                        if (!\u3007\u3007888.\u3007o00\u3007\u3007Oo && !\u3007\u3007888.\u3007o\u3007) {
                            b.b.a.a.i.a.l.c.\u3007\u3007888("afterUpload meet notifyRunOnce again");
                            b.b.a.a.i.a.l.b.\u3007080(oo80.\u3007\u3007\u30070\u3007\u30070(), 1);
                            this.O8ooOoo\u3007(2);
                            break Label_0368;
                        }
                        b.b.a.a.i.a.l.b.\u3007080(oo80.o800o8O(), 1);
                        b.b.a.a.i.a.l.c.\u3007o\u3007("afterUpload wait serverBusy");
                        monitorexit(\u3007oOo8\u30070);
                    }
                    finally {
                        monitorexit(\u3007oOo8\u30070);
                        monitorexit(\u3007oOo8\u30070);
                    }
                }
            }
            catch (final InterruptedException ex) {}
        }
    }
    
    public static void \u3007oOO8O8(final int n) {
        c.\u3007\u300708O = n;
        final StringBuilder sb = new StringBuilder();
        sb.append("config size=");
        sb.append(n);
        b.b.a.a.i.a.l.c.O8("PADLT", sb.toString());
    }
    
    private void \u3007oo\u3007(final List<a> list) {
        if (list.size() != 0) {
            b.b.a.a.i.a.l.a.o\u30070(list, this.o\u300700O.size());
            if (list.size() <= 1 && !b.b.a.a.i.a.l.a.OoO8()) {
                final a a = list.get(0);
                if (a != null) {
                    if (a.c() == 1) {
                        this.Oooo8o0\u3007(list, "highPriority");
                        b.b.a.a.i.a.l.c.O8("PADLT", "Single high priority \uff08 applog \uff09");
                    }
                    else if (a.f() == 0 && a.c() == 2) {
                        if (a.g() == 3) {
                            this.Oooo8o0\u3007(list, "version_v3");
                        }
                        else {
                            this.OO0o\u3007\u3007(list);
                        }
                    }
                    else if (a.f() == 1) {
                        b.b.a.a.i.a.l.c.O8("PADLT", "Stats batch report \uff08 stats \uff09");
                        this.Oooo8o0\u3007(list, "stats");
                    }
                    else if (a.f() == 3) {
                        this.Oooo8o0\u3007(list, "adType_v3");
                    }
                    else if (a.f() == 2) {
                        b.b.a.a.i.a.l.c.O8("PADLT", "Single high priority \uff08 stats \uff09");
                        this.Oooo8o0\u3007(list, "other");
                    }
                    else {
                        b.b.a.a.i.a.l.c.\u3007080("upload adLogEvent adType error");
                    }
                }
                else {
                    b.b.a.a.i.a.l.c.\u3007080("upload adLogEvent is null");
                }
            }
            else {
                b.b.a.a.i.a.l.c.O8("PADLT", "Batch report\uff08 local or stats \uff09");
                this.Oooo8o0\u3007(list, "batchRead");
            }
        }
        else {
            this.O\u30078O8\u3007008();
            b.b.a.a.i.a.l.c.\u3007080("upload list is empty");
        }
    }
    
    private void \u3007o\u3007(final int n) {
        if (!this.\u30070000OOO()) {
            if (this.OO\u300700\u30078oO == null) {
                return;
            }
            final b.b.a.a.i.a.k.e.a oo80 = b.b.a.a.i.a.k.d.oO80;
            b.b.a.a.i.a.l.b.\u3007080(oo80.Oooo8o0\u3007(), 1);
            if (!this.OO\u300700\u30078oO.hasMessages(1)) {
                if (n == 1) {
                    b.b.a.a.i.a.l.b.\u3007080(oo80.o8(), 1);
                }
                else if (n == 2) {
                    b.b.a.a.i.a.l.b.\u3007080(oo80.OOO(), 1);
                }
                else if (n == 3) {
                    b.b.a.a.i.a.l.b.\u3007080(oo80.oO80(), 1);
                }
                this.OO\u300700\u30078oO.sendEmptyMessage(1);
            }
        }
        else {
            b.b.a.a.i.a.l.b.\u3007080(b.b.a.a.i.a.k.d.oO80.ooo\u30078oO(), 1);
        }
    }
    
    private void \u3007\u3007808\u3007(final List<a> list, final boolean b, final long n) {
        final f \u3007080 = i.\u3007\u30078O0\u30078().\u3007080();
        if (\u3007080 != null) {
            Executor executor = \u3007080.b();
            if (list.get(0).c() == 1) {
                executor = \u3007080.a();
            }
            if (executor == null) {
                return;
            }
            this.oOo0.incrementAndGet();
            executor.execute(new b.b.a.a.i.a.n.e(this, "csj_log_upload", list, b, n) {
                final boolean OO;
                final c o\u300700O;
                final long \u300708O\u300700\u3007o;
                final List \u3007OOo8\u30070;
                
                @Override
                public void run() {
                    final c o\u300700O = this.o\u300700O;
                    o\u300700O.\u3007O\u3007(this.\u3007OOo8\u30070, this.OO, this.\u300708O\u300700\u3007o, o\u300700O.O8o08O8O);
                }
            });
        }
    }
    
    public void O8(final int i, long lng) {
        if (this.OO\u300700\u30078oO == null) {
            b.b.a.a.i.a.l.c.\u3007o\u3007("mHandler == null");
            return;
        }
        final Message obtain = Message.obtain();
        if ((obtain.what = i) == 2) {
            final int incrementAndGet = this.\u30078\u3007oO\u3007\u30078o.incrementAndGet();
            lng *= (incrementAndGet - 1) % 4 + 1;
            final StringBuilder sb = new StringBuilder();
            sb.append("sendMonitorMessage:");
            sb.append(i);
            sb.append("  busy:");
            sb.append(incrementAndGet);
            sb.append("  l:");
            sb.append(lng);
            b.b.a.a.i.a.l.c.\u3007080(sb.toString());
            this.OO\u300700\u30078oO.sendMessageDelayed(obtain, lng);
        }
        else if (i == 3) {
            final int incrementAndGet2 = this.ooo0\u3007\u3007O.incrementAndGet();
            final StringBuilder sb2 = new StringBuilder();
            sb2.append("sendMonitorMessage:");
            sb2.append(i);
            sb2.append("  error:");
            sb2.append(incrementAndGet2);
            b.b.a.a.i.a.l.c.\u3007080(sb2.toString());
            this.OO\u300700\u30078oO.sendMessageDelayed(obtain, ((incrementAndGet2 - 1) % 4 + 1) * lng);
        }
        else {
            b.b.a.a.i.a.l.c.\u3007o\u3007("sendMonitorMessage error state");
        }
    }
    
    public void O8ooOoo\u3007(final int i) {
        try {
            final boolean ooO8 = this.OoO8(i, b.b.a.a.i.a.k.d.\u3007\u3007888.\u3007o00\u3007\u3007Oo);
            final StringBuilder sb = new StringBuilder();
            sb.append("notify flush : ");
            sb.append(ooO8);
            sb.append(" ");
            sb.append(i);
            b.b.a.a.i.a.l.c.\u3007\u3007888(sb.toString());
            if (i == 6 || ooO8) {
                final b.b.a.a.i.a.m.b e = new b.b.a.a.i.a.m.b();
                e.\u3007o\u3007(i);
                this.o\u300700O.add(e);
                this.\u3007o\u3007(3);
            }
        }
        finally {
            final Throwable t;
            b.b.a.a.i.a.l.c.\u3007o\u3007(t.getMessage());
        }
    }
    
    public boolean OoO8(final int n, final boolean b) {
        final f \u3007080 = i.\u3007\u30078O0\u30078().\u3007080();
        if (\u3007080 != null && \u3007080.a(i.\u3007\u30078O0\u30078().OO0o\u3007\u3007())) {
            return this.OO.a(n, b);
        }
        b.b.a.a.i.a.l.c.\u3007o\u3007("AdThread NET IS NOT AVAILABLE!!!");
        return false;
    }
    
    public boolean handleMessage(final Message message) {
        final int what = message.what;
        Label_0085: {
            if (what == 1) {
                break Label_0085;
            }
            Label_0072: {
                if (what == 2 || what == 3) {
                    break Label_0072;
                }
                if (what != 11) {
                    return true;
                }
                try {
                    b.b.a.a.i.a.l.c.\u3007080("opt upload");
                    final ArrayList<a> list = new ArrayList<a>(this.o8\u3007OO0\u30070o);
                    this.o8\u3007OO0\u30070o.clear();
                    this.\u3007O00(list, false, "timeout_dispatch");
                    this.\u3007o00\u3007\u3007Oo();
                    return true;
                    b.b.a.a.i.a.l.c.\u3007080("HANDLER_MESSAGE_INIT");
                    b.b.a.a.i.a.l.b.\u3007080(b.b.a.a.i.a.k.d.oO80.\u3007O\u300780o08O(), 1);
                    this.\u3007\u30078O0\u30078(true);
                    this.o\u3007\u30070\u3007();
                    return true;
                    b.b.a.a.i.a.l.c.\u3007080("-----------------server busy handleMessage---------------- ");
                    this.\u3007o();
                }
                finally {
                    final StringBuilder sb = new StringBuilder();
                    sb.append("error:");
                    final Throwable t;
                    sb.append(t.getMessage());
                    b.b.a.a.i.a.l.c.\u3007o\u3007(sb.toString());
                }
            }
        }
        return true;
    }
    
    protected void onLooperPrepared() {
        super.onLooperPrepared();
        this.OO\u300700\u30078oO = new Handler(this.getLooper(), (Handler$Callback)this);
        b.b.a.a.i.a.k.d.\u3007\u3007888.\u3007o\u3007(this.OO\u300700\u30078oO);
        this.OO\u300700\u30078oO.sendEmptyMessage(1);
        b.b.a.a.i.a.l.c.\u3007080("onLooperPrepared");
    }
    
    public boolean \u30070000OOO() {
        return this.o0;
    }
    
    public void \u30078o8o\u3007(final a a, final boolean b) {
        if (a == null) {
            return;
        }
        final StringBuilder sb = new StringBuilder();
        sb.append("ignore result : ");
        sb.append(b);
        sb.append(":");
        sb.append(this.o0);
        sb.append(" adType: ");
        sb.append(a.f());
        b.b.a.a.i.a.l.c.\u3007080(sb.toString());
        if (b) {
            if (this.OO\u300700\u30078oO != null) {
                final ArrayList list = new ArrayList(1);
                list.add(a);
                this.\u3007O00(list, true, "ignore_result_dispatch");
            }
            else {
                b.b.a.a.i.a.l.c.\u3007o\u3007("handler is null\uff0cignore is true");
            }
        }
        else {
            this.o\u300700O.add(a);
            this.\u3007o\u3007(2);
        }
    }
    
    public void \u3007\u30078O0\u30078(final boolean o0) {
        this.o0 = o0;
    }
}

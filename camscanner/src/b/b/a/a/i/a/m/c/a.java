// 
// Decompiled by Procyon v0.6.0
// 

package b.b.a.a.i.a.m.c;

import android.text.TextUtils;
import org.json.JSONException;
import org.json.JSONObject;

public class a implements b.b.a.a.i.a.m.a
{
    private long O8;
    private String OO0o\u3007\u3007\u3007\u30070;
    private long Oo08;
    protected JSONObject oO80;
    private String o\u30070;
    private b \u3007080;
    private byte \u300780\u3007808\u3007O;
    private byte \u3007o00\u3007\u3007Oo;
    private byte \u3007o\u3007;
    private String \u3007\u3007888;
    
    private a() {
    }
    
    public a(final String \u3007\u3007888, final b \u3007080) {
        this.\u3007\u3007888 = \u3007\u3007888;
        this.\u3007080 = \u3007080;
    }
    
    public a(final String \u3007\u3007888, final JSONObject oo80) {
        this.\u3007\u3007888 = \u3007\u3007888;
        this.oO80 = oo80;
    }
    
    public static b.b.a.a.i.a.m.a \u3007o\u3007(final String s) {
        try {
            final JSONObject jsonObject = new JSONObject(s);
            final int optInt = jsonObject.optInt("type");
            final int optInt2 = jsonObject.optInt("priority");
            final a a = new a();
            a.\u3007080((byte)optInt);
            a.\u3007o00\u3007\u3007Oo((byte)optInt2);
            a.a(jsonObject.optJSONObject("event"));
            a.a(jsonObject.optString("localId"));
            a.b(jsonObject.optString("genTime"));
            return a;
        }
        catch (final JSONException ex) {
            ((Throwable)ex).printStackTrace();
            return null;
        }
    }
    
    public void O8(final byte \u300780\u3007808\u3007O) {
        this.\u300780\u3007808\u3007O = \u300780\u3007808\u3007O;
    }
    
    public String Oo08() {
        return this.o\u30070;
    }
    
    @Override
    public long a() {
        return this.O8;
    }
    
    @Override
    public void a(final long n) {
    }
    
    @Override
    public void a(final String \u3007\u3007888) {
        this.\u3007\u3007888 = \u3007\u3007888;
    }
    
    @Override
    public void a(final JSONObject oo80) {
        this.oO80 = oo80;
    }
    
    @Override
    public JSONObject b() {
        synchronized (this) {
            if (this.oO80 == null) {
                final b \u3007080 = this.\u3007080;
                if (\u3007080 != null) {
                    this.oO80 = \u3007080.a(this.o\u30070());
                }
            }
            return this.oO80;
        }
    }
    
    @Override
    public void b(final long oo08) {
        this.Oo08 = oo08;
    }
    
    @Override
    public void b(final String o\u30070) {
        this.o\u30070 = o\u30070;
    }
    
    @Override
    public byte c() {
        return this.\u3007o\u3007;
    }
    
    @Override
    public void c(final long o8) {
        this.O8 = o8;
    }
    
    @Override
    public b d() {
        return this.\u3007080;
    }
    
    @Override
    public long e() {
        return this.Oo08;
    }
    
    @Override
    public byte f() {
        return this.\u3007o00\u3007\u3007Oo;
    }
    
    @Override
    public byte g() {
        return this.\u300780\u3007808\u3007O;
    }
    
    @Override
    public String h() {
        Label_0078: {
            if (TextUtils.isEmpty((CharSequence)this.\u3007\u3007888)) {
                break Label_0078;
            }
            final JSONObject jsonObject = new JSONObject();
            try {
                jsonObject.put("localId", (Object)this.\u3007\u3007888);
                jsonObject.put("event", (Object)this.b());
                jsonObject.put("genTime", (Object)this.Oo08());
                jsonObject.put("priority", (int)this.\u3007o\u3007);
                jsonObject.put("type", (int)this.\u3007o00\u3007\u3007Oo);
                return jsonObject.toString();
                return null;
            }
            finally {
                return jsonObject.toString();
            }
        }
    }
    
    @Override
    public String i() {
        return this.\u3007\u3007888;
    }
    
    public String o\u30070() {
        return this.OO0o\u3007\u3007\u3007\u30070;
    }
    
    @Override
    public void \u3007080(final byte \u3007o00\u3007\u3007Oo) {
        this.\u3007o00\u3007\u3007Oo = \u3007o00\u3007\u3007Oo;
    }
    
    @Override
    public void \u3007o00\u3007\u3007Oo(final byte \u3007o\u3007) {
        this.\u3007o\u3007 = \u3007o\u3007;
    }
}

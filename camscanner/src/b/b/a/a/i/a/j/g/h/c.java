// 
// Decompiled by Procyon v0.6.0
// 

package b.b.a.a.i.a.j.g.h;

import java.util.Iterator;
import android.text.TextUtils;
import java.util.Collection;
import java.util.ArrayList;
import b.b.a.a.i.a.m.a;
import java.util.List;
import android.content.Context;

public abstract class c
{
    private Runnable O8;
    private final Context \u3007080;
    protected final List<a> \u3007o00\u3007\u3007Oo;
    private boolean \u3007o\u3007;
    
    public c(final Context \u3007080) {
        this.\u3007o00\u3007\u3007Oo = new ArrayList<a>();
        this.\u3007o\u3007 = false;
        this.O8 = new Runnable() {
            final c o0;
            
            @Override
            public void run() {
                synchronized (this.o0) {
                    final ArrayList<a> list = new ArrayList<a>(this.o0.\u3007o00\u3007\u3007Oo);
                    this.o0.\u3007o00\u3007\u3007Oo.clear();
                    this.o0.\u3007o\u3007 = false;
                    monitorexit(this.o0);
                    this.o0.o\u30070(list);
                }
            }
        };
        this.\u3007080 = \u3007080;
    }
    
    private void \u3007\u3007888() {
        if (!this.\u3007o\u3007) {
            b.b.a.a.i.a.p.a.\u3007080().postDelayed(this.O8, (long)b.b.a.a.i.a.p.a.\u3007o00\u3007\u3007Oo());
            this.\u3007o\u3007 = true;
        }
    }
    
    public abstract String Oo08();
    
    public void o\u30070(final List<a> list) {
        b.b.a.a.i.a.j.g.c.o\u30070(this.\u3007080(), this.Oo08(), list);
    }
    
    public Context \u3007080() {
        return this.\u3007080;
    }
    
    public void \u3007o00\u3007\u3007Oo(final a a) {
        synchronized (this) {
            if (a.b() != null && !TextUtils.isEmpty((CharSequence)a.i())) {
                this.\u3007o00\u3007\u3007Oo.add(a);
                this.\u3007\u3007888();
            }
        }
    }
    
    protected void \u3007o\u3007(final List<String> list) {
        if (list != null) {
            if (!list.isEmpty()) {
                try {
                    final Iterator<a> iterator = this.\u3007o00\u3007\u3007Oo.iterator();
                    while (iterator.hasNext()) {
                        final a a = iterator.next();
                        if (a != null) {
                            final String i = a.i();
                            if (TextUtils.isEmpty((CharSequence)i) || !list.contains(i)) {
                                continue;
                            }
                            iterator.remove();
                        }
                    }
                }
                finally {
                    final StringBuilder sb = new StringBuilder();
                    sb.append(this.Oo08());
                    sb.append("deleteMemList: ");
                    final Throwable t;
                    sb.append(t.getMessage());
                    b.b.a.a.i.a.l.c.O8("DBInsertMemRepo", sb.toString());
                }
            }
        }
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package b.b.a.a.i.a.p;

import android.os.Handler;
import android.os.HandlerThread;

public class a
{
    private static volatile HandlerThread \u3007080;
    private static volatile Handler \u3007o00\u3007\u3007Oo;
    private static int \u3007o\u3007 = 3000;
    
    static {
        ((Thread)(a.\u3007080 = new HandlerThread("csj_ad_log", 10))).start();
    }
    
    public static Handler \u3007080() {
        Label_0123: {
            if (a.\u3007080 != null) {
                if (((Thread)a.\u3007080).isAlive()) {
                    if (a.\u3007o00\u3007\u3007Oo != null) {
                        break Label_0123;
                    }
                    synchronized (a.class) {
                        Label_0051: {
                            if (a.\u3007o00\u3007\u3007Oo == null) {
                                a.\u3007o00\u3007\u3007Oo = new Handler(a.\u3007080.getLooper());
                                break Label_0051;
                            }
                            break Label_0051;
                        }
                        break Label_0123;
                    }
                }
            }
            synchronized (a.class) {
                if (a.\u3007080 == null || !((Thread)a.\u3007080).isAlive()) {
                    ((Thread)(a.\u3007080 = new HandlerThread("csj_init_handle", -1))).start();
                    a.\u3007o00\u3007\u3007Oo = new Handler(a.\u3007080.getLooper());
                }
                return a.\u3007o00\u3007\u3007Oo;
            }
        }
    }
    
    public static int \u3007o00\u3007\u3007Oo() {
        if (a.\u3007o\u3007 <= 0) {
            a.\u3007o\u3007 = 3000;
        }
        return a.\u3007o\u3007;
    }
}

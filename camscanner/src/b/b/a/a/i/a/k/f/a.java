// 
// Decompiled by Procyon v0.6.0
// 

package b.b.a.a.i.a.k.f;

import b.b.a.a.i.a.i;
import b.b.a.a.i.a.l.c;
import b.b.a.a.i.a.j.g.g;
import android.content.ContentResolver;
import java.util.Iterator;
import android.net.Uri;
import b.b.a.a.i.a.j.g.f;
import android.text.TextUtils;
import java.util.List;

public class a
{
    public static void O8(String string, final List<String> list, final boolean b) {
        if (TextUtils.isEmpty((CharSequence)string) || list == null) {
            return;
        }
        if (list.isEmpty()) {
            return;
        }
        try {
            final StringBuilder sb = new StringBuilder();
            final Iterator iterator = list.iterator();
            while (iterator.hasNext()) {
                sb.append(f.\u3007o00\u3007\u3007Oo((String)iterator.next()));
                sb.append(",");
            }
            final String \u3007o00\u3007\u3007Oo = f.\u3007o00\u3007\u3007Oo(sb.toString());
            final StringBuilder sb2 = new StringBuilder();
            sb2.append("?did=");
            sb2.append(String.valueOf(string));
            sb2.append("&track=");
            sb2.append(String.valueOf(\u3007o00\u3007\u3007Oo));
            sb2.append("&replace=");
            sb2.append(String.valueOf(b));
            string = sb2.toString();
            final ContentResolver \u3007080 = \u3007080();
            if (\u3007080 != null) {
                final StringBuilder sb3 = new StringBuilder();
                sb3.append(Oo08());
                sb3.append("trackAdUrl");
                sb3.append(string);
                \u3007080.getType(Uri.parse(sb3.toString()));
            }
        }
        finally {}
    }
    
    private static String Oo08() {
        final StringBuilder sb = new StringBuilder();
        sb.append(g.\u3007o00\u3007\u3007Oo);
        sb.append("/");
        sb.append("ad_log_event");
        sb.append("/");
        return sb.toString();
    }
    
    public static void o\u30070() {
        c.\u3007080("start()");
        if (i.\u3007\u30078O0\u30078().OO0o\u3007\u3007() == null) {
            return;
        }
        try {
            final ContentResolver \u3007080 = \u3007080();
            if (\u3007080 != null) {
                final StringBuilder sb = new StringBuilder();
                sb.append(Oo08());
                sb.append("adLogStart");
                \u3007080.getType(Uri.parse(sb.toString()));
            }
        }
        finally {}
    }
    
    private static ContentResolver \u3007080() {
        try {
            if (i.\u3007\u30078O0\u30078().OO0o\u3007\u3007() != null) {
                return i.\u3007\u30078O0\u30078().OO0o\u3007\u3007().getContentResolver();
            }
            return null;
        }
        finally {
            return null;
        }
    }
    
    public static void \u3007o00\u3007\u3007Oo(final b.b.a.a.i.a.m.a a) {
        if (a == null) {
            return;
        }
        try {
            final ContentResolver \u3007080 = \u3007080();
            if (\u3007080 != null) {
                final String \u3007o00\u3007\u3007Oo = f.\u3007o00\u3007\u3007Oo(a.h());
                final StringBuilder sb = new StringBuilder();
                sb.append(Oo08());
                sb.append("adLogDispatch");
                sb.append("?event=");
                sb.append(\u3007o00\u3007\u3007Oo);
                \u3007080.getType(Uri.parse(sb.toString()));
                c.\u3007080("dispatch event getType end ");
            }
        }
        finally {
            final StringBuilder sb2 = new StringBuilder();
            sb2.append("dispatch event Throwable:");
            final Throwable t;
            sb2.append(t.toString());
            c.\u3007o\u3007(sb2.toString());
        }
    }
    
    public static void \u3007o\u3007(final String obj) {
        if (TextUtils.isEmpty((CharSequence)obj)) {
            return;
        }
        try {
            final ContentResolver \u3007080 = \u3007080();
            if (\u3007080 != null) {
                final StringBuilder sb = new StringBuilder();
                sb.append(Oo08());
                sb.append("trackAdFailed");
                sb.append("?did=");
                sb.append(String.valueOf(obj));
                \u3007080.getType(Uri.parse(sb.toString()));
            }
        }
        finally {}
    }
    
    public static void \u3007\u3007888() {
        if (i.\u3007\u30078O0\u30078().OO0o\u3007\u3007() == null) {
            return;
        }
        try {
            final ContentResolver \u3007080 = \u3007080();
            if (\u3007080 != null) {
                final StringBuilder sb = new StringBuilder();
                sb.append(Oo08());
                sb.append("adLogStop");
                \u3007080.getType(Uri.parse(sb.toString()));
            }
        }
        finally {}
    }
}

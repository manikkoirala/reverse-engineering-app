// 
// Decompiled by Procyon v0.6.0
// 

package b.b.a.a.i.a.j.g;

import android.database.Cursor;
import java.util.List;
import android.content.ContentValues;
import android.net.Uri;
import android.text.TextUtils;
import android.content.Context;

public class c
{
    public static void O8(final Context context, final String s) {
        if (TextUtils.isEmpty((CharSequence)s)) {
            return;
        }
        try {
            a.\u3007o00\u3007\u3007Oo(context).\u3007080().o\u30070(Uri.decode(s));
        }
        finally {
            b.b.a.a.i.a.l.c.\u3007080("execSQL ignore");
        }
    }
    
    public static void Oo08(final Context context, final String s, final ContentValues contentValues) {
        if (contentValues != null) {
            if (!TextUtils.isEmpty((CharSequence)s)) {
                try {
                    a.\u3007o00\u3007\u3007Oo(context).\u3007080().\u3007o\u3007(s, null, contentValues);
                }
                finally {
                    b.b.a.a.i.a.l.c.\u3007080("insert ignore");
                }
            }
        }
    }
    
    public static void o\u30070(final Context context, final String s, final List<b.b.a.a.i.a.m.a> list) {
        if (list != null) {
            if (!TextUtils.isEmpty((CharSequence)s)) {
                try {
                    a.\u3007o00\u3007\u3007Oo(context).\u3007080().\u3007\u3007888(s, null, list);
                }
                finally {
                    b.b.a.a.i.a.l.c.\u3007080("insert ignore");
                }
            }
        }
    }
    
    public static int \u3007080(final Context context, final String s, final ContentValues contentValues, final String s2, final String[] array) {
        if (contentValues != null) {
            if (!TextUtils.isEmpty((CharSequence)s)) {
                try {
                    return a.\u3007o00\u3007\u3007Oo(context).\u3007080().\u3007080(s, contentValues, s2, array);
                }
                finally {
                    b.b.a.a.i.a.l.c.\u3007080("update ignore");
                }
            }
        }
        return 0;
    }
    
    public static int \u3007o00\u3007\u3007Oo(final Context context, final String s, final String s2, final String[] array) {
        if (TextUtils.isEmpty((CharSequence)s)) {
            return 0;
        }
        try {
            b.b.a.a.i.a.l.c.\u3007080("DBMultiUtils  delete start");
            return a.\u3007o00\u3007\u3007Oo(context).\u3007080().\u3007o00\u3007\u3007Oo(s, s2, array);
        }
        finally {
            b.b.a.a.i.a.l.c.\u3007080("delete ignore");
            return 0;
        }
    }
    
    public static Cursor \u3007o\u3007(final Context context, final String s, final String[] array, final String s2, final String[] array2, final String s3, final String s4, final String s5) {
        if (TextUtils.isEmpty((CharSequence)s)) {
            return null;
        }
        try {
            return a.\u3007o00\u3007\u3007Oo(context).\u3007080().O8(s, array, s2, array2, null, null, s5);
        }
        finally {
            b.b.a.a.i.a.l.c.\u3007080("query ignore");
            return null;
        }
    }
}

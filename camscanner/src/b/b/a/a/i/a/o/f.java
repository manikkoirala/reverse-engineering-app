// 
// Decompiled by Procyon v0.6.0
// 

package b.b.a.a.i.a.o;

import android.content.ContentValues;
import android.database.Cursor;
import b.b.a.a.i.a.j.g.c;
import java.util.LinkedList;
import java.util.List;
import android.content.Context;

public class f implements e
{
    private Context \u3007080;
    
    public f(final Context \u3007080) {
        this.\u3007080 = \u3007080;
    }
    
    public static String O8() {
        final StringBuilder sb = new StringBuilder();
        sb.append("CREATE TABLE IF NOT EXISTS ");
        sb.append("trackurl");
        sb.append(" (_id INTEGER PRIMARY KEY AUTOINCREMENT,");
        sb.append("id");
        sb.append(" TEXT UNIQUE,");
        sb.append("url");
        sb.append(" TEXT ,");
        sb.append("replaceholder");
        sb.append(" INTEGER default 0, ");
        sb.append("retry");
        sb.append(" INTEGER default 0");
        sb.append(")");
        return sb.toString();
    }
    
    @Override
    public List<d> a() {
        final LinkedList list = new LinkedList();
        final Cursor \u3007o\u3007 = c.\u3007o\u3007(this.\u3007080, "trackurl", null, null, null, null, null, null);
        if (\u3007o\u3007 != null) {
            try {
                while (\u3007o\u3007.moveToNext()) {
                    list.add(new d(\u3007o\u3007.getString(\u3007o\u3007.getColumnIndex("id")), \u3007o\u3007.getString(\u3007o\u3007.getColumnIndex("url")), \u3007o\u3007.getInt(\u3007o\u3007.getColumnIndex("replaceholder")) > 0, \u3007o\u3007.getInt(\u3007o\u3007.getColumnIndex("retry"))));
                }
                \u3007o\u3007.close();
            }
            finally {
                try {
                    \u3007o\u3007.close();
                }
                finally {
                    \u3007o\u3007.close();
                }
            }
        }
        return list;
    }
    
    @Override
    public void \u3007080(final d d) {
        final ContentValues contentValues = new ContentValues();
        contentValues.put("id", d.\u3007080());
        contentValues.put("url", d.O8());
        contentValues.put("replaceholder", Integer.valueOf((int)(d.Oo08() ? 1 : 0)));
        contentValues.put("retry", Integer.valueOf(d.\u3007o\u3007()));
        c.\u3007080(this.\u3007080, "trackurl", contentValues, "id=?", new String[] { d.\u3007080() });
    }
    
    @Override
    public void \u3007o00\u3007\u3007Oo(final d d) {
        final ContentValues contentValues = new ContentValues();
        contentValues.put("id", d.\u3007080());
        contentValues.put("url", d.O8());
        contentValues.put("replaceholder", Integer.valueOf((int)(d.Oo08() ? 1 : 0)));
        contentValues.put("retry", Integer.valueOf(d.\u3007o\u3007()));
        c.Oo08(this.\u3007080, "trackurl", contentValues);
    }
    
    @Override
    public void \u3007o\u3007(final d d) {
        c.\u3007o00\u3007\u3007Oo(this.\u3007080, "trackurl", "id=?", new String[] { d.\u3007080() });
    }
}

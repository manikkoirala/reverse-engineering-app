// 
// Decompiled by Procyon v0.6.0
// 

package b.b.a.a.i.a.l;

import b.b.a.a.i.a.k.d;
import java.util.Iterator;
import java.util.List;
import org.json.JSONException;
import b.b.a.a.i.a.f;
import org.json.JSONObject;
import android.text.TextUtils;
import b.b.a.a.i.a.i;
import java.util.LinkedList;
import java.util.HashMap;

public class a
{
    private static HashMap<String, Integer> O8;
    private static final LinkedList<String> \u3007080;
    private static final LinkedList<String> \u3007o00\u3007\u3007Oo;
    private static final LinkedList<String> \u3007o\u3007;
    
    static {
        new LinkedList();
        \u3007080 = new LinkedList<String>();
        \u3007o00\u3007\u3007Oo = new LinkedList<String>();
        \u3007o\u3007 = new LinkedList<String>();
        new HashMap();
        a.O8 = null;
    }
    
    public static void O8(final b.b.a.a.i.a.m.a a) {
        try {
            if (a.f() == 0 && i.\u3007\u30078O0\u30078().\u3007080() != null && i.\u3007\u30078O0\u30078().\u3007080().l()) {
                final JSONObject b = a.b();
                final String \u3007\u3007808\u3007 = \u3007\u3007808\u3007(a);
                if (!\u30078o8o\u3007(\u3007\u3007808\u3007)) {
                    o800o8O(a);
                    final String optString = b.optString("ad_extra_data");
                    if (!TextUtils.isEmpty((CharSequence)optString)) {
                        final JSONObject jsonObject = new JSONObject(optString);
                        if (TextUtils.isEmpty((CharSequence)jsonObject.optString("will_send_labels"))) {
                            final StringBuilder sb = new StringBuilder();
                            sb.append(\u3007\u3007808\u3007);
                            sb.append("_");
                            sb.append(o\u3007O8\u3007\u3007o(a));
                            jsonObject.put("will_send_labels", (Object)\u30070\u3007O0088o(sb.toString()));
                            jsonObject.put("send_success_valid_labels", (Object)\u3007080());
                        }
                        b.put("ad_extra_data", (Object)jsonObject.toString());
                    }
                    else {
                        final JSONObject jsonObject2 = new JSONObject();
                        final StringBuilder sb2 = new StringBuilder();
                        sb2.append(\u3007\u3007808\u3007);
                        sb2.append("_");
                        sb2.append(o\u3007O8\u3007\u3007o(a));
                        jsonObject2.put("will_send_labels", (Object)\u30070\u3007O0088o(sb2.toString()));
                        jsonObject2.put("send_success_valid_labels", (Object)\u3007080());
                        b.put("ad_extra_data", (Object)jsonObject2.toString());
                    }
                }
            }
        }
        catch (final Exception ex) {
            c.\u3007o\u3007(ex.getMessage());
        }
    }
    
    public static String O8ooOoo\u3007(final b.b.a.a.i.a.m.a a) {
        if (a == null || a.b() == null) {
            return null;
        }
        if (OoO8()) {
            return null;
        }
        return a.b().optString("type");
    }
    
    private static void OO0o\u3007\u3007(final String s) {
        synchronized (a.class) {
            final LinkedList<String> \u3007o\u3007 = a.\u3007o\u3007;
            if (\u3007o\u3007.size() >= 10L) {
                \u3007o\u3007.removeFirst();
                \u3007o\u3007.add(s);
            }
            else {
                \u3007o\u3007.add(s);
            }
        }
    }
    
    public static void OO0o\u3007\u3007\u3007\u30070(final boolean b, final int n, final b.b.a.a.i.a.m.a a) {
        final f \u3007080 = i.\u3007\u30078O0\u30078().\u3007080();
        if (OoO8()) {
            return;
        }
        if (\u3007080 != null && \u3007080.c()) {
            final StringBuilder sb = new StringBuilder();
            sb.append("needUpload:");
            sb.append(b);
            sb.append(",message:");
            sb.append(\u3007o00\u3007\u3007Oo(n));
            final String o8ooOoo\u3007 = O8ooOoo\u3007(a);
            if (!TextUtils.isEmpty((CharSequence)o8ooOoo\u3007)) {
                sb.append(",type:");
                sb.append(o8ooOoo\u3007);
            }
            final String \u3007\u3007808\u3007 = \u3007\u3007808\u3007(a);
            if (!TextUtils.isEmpty((CharSequence)\u3007\u3007808\u3007)) {
                sb.append(",label:");
                sb.append(\u3007\u3007808\u3007);
            }
            c.o\u30070("_save", sb.toString());
        }
    }
    
    private static void Oo08(final b.b.a.a.i.a.m.a a, final String str, final f f) {
        final String \u3007\u3007808\u3007 = \u3007\u3007808\u3007(a);
        if (!\u30078o8o\u3007(\u3007\u3007808\u3007)) {
            final String o800o8O = o800o8O(a);
            if (a.f() == 0 && f.l()) {
                final StringBuilder sb = new StringBuilder();
                sb.append(\u3007\u3007808\u3007);
                sb.append("_");
                sb.append(o\u3007O8\u3007\u3007o(a));
                sb.append("_");
                sb.append(o800o8O);
                sb.append("_");
                sb.append(str);
                OO0o\u3007\u3007(sb.toString());
            }
        }
    }
    
    public static boolean OoO8() {
        final f \u3007080 = i.\u3007\u30078O0\u30078().\u3007080();
        return \u3007080 != null && \u3007080.d() == 2;
    }
    
    private static boolean Oooo8o0\u3007() {
        final f \u3007080 = i.\u3007\u30078O0\u30078().\u3007080();
        return \u3007080 != null && \u3007080.d() == 0;
    }
    
    public static int O\u30078O8\u3007008(final b.b.a.a.i.a.m.a a) {
        if (a != null) {
            if (a.b() != null) {
                if (!Oooo8o0\u3007()) {
                    return -1;
                }
                final String optString = a.b().optString("event_extra");
                try {
                    return new JSONObject(optString).optInt("stats_index");
                }
                catch (final JSONException ex) {
                    ((Throwable)ex).printStackTrace();
                }
            }
        }
        return -1;
    }
    
    public static String o800o8O(final b.b.a.a.i.a.m.a a) {
        if (a != null) {
            if (a.b() != null) {
                if (OoO8()) {
                    return null;
                }
                final String optString = a.b().optString("log_extra");
                if (!TextUtils.isEmpty((CharSequence)optString)) {
                    try {
                        return new JSONObject(optString).optString("req_id");
                    }
                    catch (final JSONException ex) {
                        ((Throwable)ex).printStackTrace();
                    }
                }
            }
        }
        return null;
    }
    
    public static void oO80(final List<b.b.a.a.i.a.m.a> list, final String s) {
        try {
            final f \u3007080 = i.\u3007\u30078O0\u30078().\u3007080();
            if (\u3007080 == null) {
                return;
            }
            if (\u3007080.e() && list != null) {
                for (final b.b.a.a.i.a.m.a a : list) {
                    if (a == null) {
                        continue;
                    }
                    Oo08(a, s, \u3007080);
                }
            }
        }
        catch (final Exception ex) {
            c.\u3007o\u3007(ex.getMessage());
        }
    }
    
    public static long oo88o8O(final b.b.a.a.i.a.m.a a) {
        if (a != null) {
            if (a.b() != null) {
                if (!Oooo8o0\u3007()) {
                    return 0L;
                }
                try {
                    return new JSONObject(a.b().optString("ad_extra_data")).optLong("sdk_event_index");
                }
                catch (final Exception ex) {
                    c.\u3007o\u3007(ex.getMessage());
                }
            }
        }
        return 0L;
    }
    
    public static void o\u30070(final List<b.b.a.a.i.a.m.a> list, final int n) {
        try {
            if (i.\u3007\u30078O0\u30078().\u3007080().e()) {
                for (final b.b.a.a.i.a.m.a a : list) {
                    if (a != null && a.e() != 0L) {
                        final long currentTimeMillis = System.currentTimeMillis();
                        final long e = a.e();
                        final b.b.a.a.i.a.k.e.a oo80 = d.oO80;
                        oo80.o\u30070OOo\u30070().incrementAndGet();
                        oo80.\u3007O\u3007().getAndAdd(currentTimeMillis - e);
                        a.a(System.currentTimeMillis());
                    }
                    if (a == null) {
                        continue;
                    }
                    O8(a);
                }
                d.oO80.Ooo().getAndAdd(list.size());
            }
        }
        catch (final Exception ex) {
            ex.printStackTrace();
        }
    }
    
    public static long o\u3007O8\u3007\u3007o(final b.b.a.a.i.a.m.a a) {
        if (a != null) {
            if (a.b() != null) {
                if (!Oooo8o0\u3007()) {
                    return 0L;
                }
                try {
                    return new JSONObject(a.b().optString("ad_extra_data")).optLong("sdk_event_valid_index");
                }
                catch (final Exception ex) {
                    c.\u3007o\u3007(ex.getMessage());
                }
            }
        }
        return 0L;
    }
    
    private static String \u300700(final b.b.a.a.i.a.m.a a) {
        if (a != null) {
            if (a.b() != null) {
                if (!Oooo8o0\u3007()) {
                    return null;
                }
                final JSONObject b = a.b();
                String s;
                if (a.f() == 1) {
                    s = b.optString("event_extra");
                }
                else {
                    s = b.optString("ad_extra_data");
                }
                try {
                    return new JSONObject(s).optString("sdk_session_id");
                }
                catch (final JSONException ex) {
                    c.\u3007o\u3007(((Throwable)ex).getMessage());
                }
            }
        }
        return null;
    }
    
    private static String \u3007080() {
        synchronized (a.class) {
            final StringBuilder sb = new StringBuilder();
            final Iterator<Object> iterator = a.\u3007o\u3007.iterator();
            while (iterator.hasNext()) {
                sb.append(iterator.next());
                sb.append(",");
            }
            return sb.toString();
        }
    }
    
    private static String \u30070\u3007O0088o(String string) {
        synchronized (a.class) {
            final LinkedList<String> \u3007o00\u3007\u3007Oo = a.\u3007o00\u3007\u3007Oo;
            if (\u3007o00\u3007\u3007Oo.size() >= 10L) {
                \u3007o00\u3007\u3007Oo.removeFirst();
                \u3007o00\u3007\u3007Oo.add(string);
            }
            else {
                \u3007o00\u3007\u3007Oo.add(string);
            }
            final StringBuilder sb = new StringBuilder();
            final Iterator iterator = \u3007o00\u3007\u3007Oo.iterator();
            while (iterator.hasNext()) {
                sb.append((String)iterator.next());
                sb.append(",");
            }
            string = sb.toString();
            return string;
        }
    }
    
    public static void \u300780\u3007808\u3007O(final JSONObject jsonObject, final b.b.a.a.i.a.m.c.a a) {
        final f \u3007080 = i.\u3007\u30078O0\u30078().\u3007080();
        if (\u3007080 != null && \u3007080.c() && \u3007O00()) {
            final StringBuilder sb = new StringBuilder();
            sb.append("log_show_query :");
            sb.append(jsonObject.optString("label"));
            sb.append(" ");
            sb.append(a.c());
            c.\u3007080(sb.toString());
        }
    }
    
    public static boolean \u30078o8o\u3007(final String key) {
        final HashMap<String, Integer> o8 = a.O8;
        return o8 != null && key != null && o8.containsKey(key);
    }
    
    public static boolean \u3007O00() {
        return Oooo8o0\u3007() || \u3007O888o0o();
    }
    
    private static boolean \u3007O888o0o() {
        final f \u3007080 = i.\u3007\u30078O0\u30078().\u3007080();
        if (\u3007080 != null) {
            final int d = \u3007080.d();
            final boolean b = true;
            if (d == 1) {
                return b;
            }
        }
        return false;
    }
    
    public static void \u3007O8o08O(final b.b.a.a.i.a.m.a a) {
        try {
            if (!Oooo8o0\u3007()) {
                return;
            }
            final f \u3007080 = i.\u3007\u30078O0\u30078().\u3007080();
            if (!TextUtils.isEmpty((CharSequence)a.i())) {
                b.\u3007080(d.oO80.\u3007oOO8O8(), 1);
            }
            if (\u3007080 != null && \u3007080.c()) {
                if (a.f() == 1) {
                    final StringBuilder sb = new StringBuilder();
                    sb.append("stats,[");
                    sb.append("type:");
                    sb.append(O8ooOoo\u3007(a));
                    sb.append(",index:");
                    sb.append(O\u30078O8\u3007008(a));
                    sb.append(",localId:");
                    sb.append(a.i());
                    sb.append(",sessionId:");
                    sb.append(\u300700(a));
                    sb.append("]");
                    c.o\u30070("_delete", sb.toString());
                }
                else if (a.f() == 0) {
                    if (a.g() == 3) {
                        if (a.b() != null) {
                            final String optString = a.b().optString("event");
                            final StringBuilder sb2 = new StringBuilder();
                            sb2.append("v3,[");
                            sb2.append("label:");
                            sb2.append(optString);
                            final long oo88o8O = oo88o8O(a);
                            if (oo88o8O != 0L) {
                                sb2.append(", index:");
                                sb2.append(oo88o8O);
                            }
                            final long o\u3007O8\u3007\u3007o = o\u3007O8\u3007\u3007o(a);
                            if (o\u3007O8\u3007\u3007o != 0L) {
                                sb2.append(", validIndex:");
                                sb2.append(o\u3007O8\u3007\u3007o);
                            }
                            sb2.append(", localId:");
                            sb2.append(a.i());
                            sb2.append(", sessionId:");
                            sb2.append(\u300700(a));
                            sb2.append("]");
                            c.o\u30070("_delete", sb2.toString());
                        }
                        else {
                            c.o\u30070("_delete", "v3_error");
                        }
                    }
                    else {
                        final StringBuilder sb3 = new StringBuilder();
                        final long o\u3007O8\u3007\u3007o2 = o\u3007O8\u3007\u3007o(a);
                        final long n = lcmp(o\u3007O8\u3007\u3007o2, 0L);
                        if (n != 0) {
                            sb3.append("v1_core [");
                            sb3.append("label:");
                            sb3.append(\u3007\u3007808\u3007(a));
                        }
                        else {
                            sb3.append("v1_debug [");
                            sb3.append("label:");
                            sb3.append(\u3007\u3007808\u3007(a));
                        }
                        final long oo88o8O2 = oo88o8O(a);
                        if (oo88o8O2 != 0L) {
                            sb3.append(", index:");
                            sb3.append(oo88o8O2);
                        }
                        if (n != 0) {
                            sb3.append(", validIndex:");
                            sb3.append(o\u3007O8\u3007\u3007o2);
                        }
                        sb3.append(", localId:");
                        sb3.append(a.i());
                        sb3.append(", sessionId:");
                        sb3.append(\u300700(a));
                        sb3.append("]");
                        c.o\u30070("_delete", sb3.toString());
                    }
                }
            }
        }
        catch (final Exception ex) {
            c.\u3007o\u3007(ex.getMessage());
        }
    }
    
    private static String \u3007O\u3007(String string) {
        synchronized (a.class) {
            final LinkedList<String> \u3007080 = a.\u3007080;
            if (\u3007080.size() >= 10L) {
                \u3007080.removeFirst();
                \u3007080.add(string);
            }
            else {
                \u3007080.add(string);
            }
            final StringBuilder sb = new StringBuilder();
            final Iterator iterator = \u3007080.iterator();
            while (iterator.hasNext()) {
                sb.append((String)iterator.next());
                sb.append(",");
            }
            string = sb.toString();
            return string;
        }
    }
    
    public static String \u3007o00\u3007\u3007Oo(final int n) {
        switch (n) {
            default: {
                return "default";
            }
            case 7: {
                return "net error";
            }
            case 6: {
                return "empty message";
            }
            case 5: {
                return "server busy";
            }
            case 4: {
                return "new event";
            }
            case 3: {
                return "flush memory";
            }
            case 2: {
                return "flush memory db";
            }
            case 1: {
                return "flush once";
            }
        }
    }
    
    public static void \u3007oOO8O8(final b.b.a.a.i.a.m.a a) {
        try {
            if (i.\u3007\u30078O0\u30078().\u3007080().e()) {
                d.oO80.o\u3007\u30070\u3007(System.currentTimeMillis() - a.a());
                a.b(System.currentTimeMillis());
                if (a.f() == 0 && i.\u3007\u30078O0\u30078().\u3007080() != null && i.\u3007\u30078O0\u30078().\u3007080().l()) {
                    final String \u3007\u3007808\u3007 = \u3007\u3007808\u3007(a);
                    if (!\u30078o8o\u3007(\u3007\u3007808\u3007)) {
                        final JSONObject b = a.b();
                        final String optString = a.b().optString("ad_extra_data");
                        if (!TextUtils.isEmpty((CharSequence)optString)) {
                            final JSONObject jsonObject = new JSONObject(optString);
                            if (TextUtils.isEmpty((CharSequence)jsonObject.optString("save_success_labels"))) {
                                final StringBuilder sb = new StringBuilder();
                                sb.append(\u3007\u3007808\u3007);
                                sb.append("_");
                                sb.append(o\u3007O8\u3007\u3007o(a));
                                jsonObject.put("save_success_labels", (Object)\u3007O\u3007(sb.toString()));
                            }
                            b.put("ad_extra_data", (Object)jsonObject.toString());
                        }
                        else {
                            final JSONObject jsonObject2 = new JSONObject();
                            final StringBuilder sb2 = new StringBuilder();
                            sb2.append(\u3007\u3007808\u3007);
                            sb2.append("_");
                            sb2.append(o\u3007O8\u3007\u3007o(a));
                            jsonObject2.put("save_success_labels", (Object)\u3007O\u3007(sb2.toString()));
                            b.put("ad_extra_data", (Object)jsonObject2.toString());
                        }
                    }
                }
            }
        }
        catch (final Exception ex) {}
    }
    
    public static void \u3007oo\u3007() {
        if (a.O8 != null) {
            return;
        }
        int i = 0;
        a.O8 = new HashMap<String, Integer>(446);
        while (i < 446) {
            a.O8.put((new String[] { "first_view", "open_splash", "ad_landing_webview_init", "show_failed_topview", "adstyle_template_show", "splash_init_monitor_first", "download_video_succeed", "shake_skip", "receive", "video_over_auto", "render_time", "splash_ad", "preload_start", "mute", "covered", "download_image_failed", "splash_init_monitor_all", "preload_success_time", "download_video_start_sdk", "download_video_count", "not_showing_reason", "download_image_succeed", "load_video_success", "launch_covered", "download_video_prepare", "download_video_start", "boarding", "ad_wap_stat", "splash_pick", "preload_fail", "should_show", "adstyle_template_fill", "unmute", "preload_success", "show_failed", "stop_showing_monitor", "download_video_no_download", "track_url", "download_creative_duration", "adstyle_template_render", "download_video_count_splash_sdk", "landing_preload_finish", "adstyle_template_load", "load_ad_duration", "client_false_show", "client_false", "download_video_failed", "data_invalid", "topview_boarding", "topview_start_download", "topview_show_confirmed", "splash_start_download", "topview_show_rejected", "splash_no_download", "redownload_video_count", "topview_other_show", "topview_no_download", "ad_selected", "invalid_model", "topview_deliver", "ad_no_selected", "topview_ad_download_retry_label", "request", "response", "parse_finished", "front_performance", "ad_resp", "ad_resp_nodata", "preload_finish", "transit_show", "splash_switch", "block_splash_F2", "render_picture_time", "network_type", "play_start_error", "load_video_error", "render_picture_timeout", "py_loading_success", "download_status", "first_screen_load_finish", "landing_preload_failed", "data_received", "preload_result", "show_result", "reponse", "valid_time", "brand_satefy_context", "topview_ad_link_fail_label", "end_feed_request", "start_feed_request", "set_feed_data", "delayinstall_conflict_with_back_dialog", "clean_fetch_apk_head_failed", "cleanspace_download_after_quite_clean", "fps_too_low", "open_policy", "landing_perf_stats", "preload_topview", "show_effect_start", "dislike_monitor", "hour_show", "hour_skip", "triggered", "click_sound_switch", "enter_loft", "download_resume", "install_view_result", "contiguous_ad_event", "contiguous_ad_remove_event", "report_monitor", "open_landing_blank", "dynamic_ad", "report_load_failed", "ad_download_failed", "download_video_start_first_sdk", "splash_receive", "video_play", "clean_fetch_apk_head_switch_close", "label_external_permission", "pause_reserve_wifi_switch_status", "landing_download_dialog_show", "download_connect", "download_uncompleted", "pause_reserve_wifi_dialog_show", "download_io", "pause_reserve_wifi_confirm", "skvc_load_time", "segment_io", "click_no", "pause_reserve_wifi_cancel_on_wifi", "udp_stop", "mma_url", "error_save_sp", "download_notification_try_show", "ttd_pref_monitor", "item_above_the_fold_stay_time", "ttdownloader_unity", "bdad_query_log", "bdad_load_finish", "bdad_load", "bdad_load_fail", "undefined", "valid_stock", "show_filter", "splash_pk_result", "endcard_page_info", "page_on_create", "statistics_feed_docker", "show_search_card_word", "ad_new_video_render_start_label", "ad_new_video_play_start_label", "ad_new_video_ad_patch_data_set_null_label", "ad_new_video_ad_patch_play_label", "ad_new_video_ad_patch_render_label", "debug_touch_start", "try_second_request", "egg_unzip_success", "tap_2", "anti_0_result", "anti_2_result", "egg_unzip_no_start", "preload_no_start", "bind_impression_212202", "guide_auth_dialog_cancel", "show_im_entry", "sub_reco_impression_v2", "sync_request_log_mask", "no_send_sync_request", "load_timeout", "send_sync_request", "sync_request_not_show", "show_subv_tt_video_food", "track", "custom_event", "rd_landing_page_stat", "update_local_data", "showlimit", "upload_result", "debug_othershow", "debug_otherclick", "ad_show_time", "push_launch", "union_send_duplicate", "mnpl_js_finish_load", "mnpl_resource_finish_load", "mnpl_material_render_timeout", "mnpl_render_timing", "mnpl_vedio_interactive_timegap", "click_non_rectify_area", "start_impression", "end_impression", "picture_render_time", "splash_stop_show", "skip_post", "skan_show_start", "skan_show_end", "load_video_start", "rifle_ad_monitor", "download_video_redownload", "splash_video_quality", "splash_video_end", "splash_video_pause", "splash_video_failed", "adtrace_start_clear", "adtrace_clear_past_data", "adtrace_end_clear", "adtrace_write_success", "adtrace_write_failed", "adtrace_read_result", "adtrace_read_success", "adtrace_read_failed", "pick_model", "cache_model", "adtrace_reparse_file", "deeplink_failed_all", "ad_live_degenerate", "ad_live_miss", "live_play_fail", "sko_show_success", "sko_show_fail", "commerce_apps_open", "commerce_apps_jump", "pic_card_show", "live_ad_card_render_finish", "adtrace_select", "received_card_status", "live_ad_page_load_success", "mp_download_result", "download_video_cancel", "jump_count", "adtrace_try_show", "show_cart_entrance", "live_ad_page_load_fail", "click_interacted", "pop_up", "pop_up_cancel", "stream_loadtime", "mnpl_guide_comp_render", "thirdquartile", "customer_feed_pause", "customer_play_start", "customer_feed_break", "click_area_log", "customer_feed_continue", "customer_feed_play", "mnpl_resource_start_preload", "mnpl_resource_finish_preload", "customer_feed_over", "get_preload_ad", "web_inspect_status", "web_report_status", "preload_begin", "preload_end", "open_begin", "open_end", "pangle_live_sdk_monitor", "success", "rifle_load_state", "rifle_uri_load_state", "component_init", "component_release", "ad_lynx_download_sendAdLog", "dynamic2_render", "lynx_card_show", "pop_up_download", "live_shelf_commodity_show", "unity_fe_click", "enter_ads_explain", "adx_ads_switch", "personal_ads_switch", "qc_product_picture_cancel", "qc_product_picture_save", "qc_product_picture_press", "qc_product_detail_show", "qc_price_instruction_click", "qc_edit_sku_num_click", "service_description_page_duration", "enter_business_qualification_page ", "service_description_page_show", "order_words_fe", "qc_payment_mode_show", "qc_click_ali_pay", "qc_district_addr_click", "qc_auto_information_add", "qc_dial_consult_cancel_btn_click", "qc_dial_consult_show", "qc_maomadeng_click", "qc_maomadeng_show", "slide_product_big_picture", "qc_service_description_close", "appstore_manager_request", "preload_video_result", "preload_video_start", "adtrace_bind", "topview_ad_link_match_event", "skip_leisure_interact_render", "click_start_download", "ad_lynx_landing_page_exception", "lynx_page_res_download_monitor_event", "live_fail", "live_over", "render_live_picture_success", "render_live_picture_fail", "live_play_success", "live_play_close", "item_play_pver", "ad_gap_info", "item_play_over", "has_period_first_chance", "enter_live_auto", "mnpl_material_video_scene_show", "ad_rerank", "in_web_click", "post_request_failed", "destroy", "bidding_load", "bidding_receive", "in_web_scroll", "tobsdk_livesdk_live_show", "xigua_ad_rerank", "applink_unity", "top_ad_show", "top_button_show", "skip_button_show", "skip_click", "shake_show", "skip_result", "show_personal_compliance_button_click", "personal_compliance_click", "ad_click_result", "ad_preload_video", "popup_show", "topview_feed_down", "qr_scan", "qr_show", "topview_popup_show", "topview_feed_over", "topview_feed_show", "feed_down", "engine_ad_send", "permission_click", "policy_click", "download_start_click", "mini_playable_style_report", "load_detect", "aweme_show_info", "click_convert_anchor_detail_page", "click_anchor_gift_button", "show_anchor_gift_page", "click_anchor_gift_card", "show_anchor_gift_card", "anchor_convert_button", "show_anchor_page", "search_result_click", "sdk_session_launch", "not_use_app_link_sdk", "click_ios_check", "auto_open", "bind_click_area", "page_load", "show_finish", "next_fresh", "play_ready", "splash_pk_time", "unshow", "feed_show_failed", "othershow_cancel", "lu_cache", "realtime_splash_result", "channel_override_result", "internal_jump_live_status", "mnpl_video_play_backward", "splash_enter_foreground", "splash_enter_background", "button_light", "long_press", "webview_material_missing_key_error", "live_life_project_click_card", "mnpl_click_event", "show_anchor_convert_button", "bdar_log_info", "bdar_ad_request", "bdar_lynx_template_load_time", "bdar_lynx_fallback", "bdar_fetch_template_data", "bdar_lynx_render_time", "bdar_video_play_effective", "bdar_video_first_frame", "bdar_lynx_jsb_error", "invalidate_back_url_monitor_event", "lynx_page_plugin_exception_event", "live_custom_interaction", "pinch", "if_splash_card", "splash_card_show", "card_show_fail", "splash_card_click", "splash_card_close", "wind_icon_click", "excluded", "show_error", "toutiao_ad_receive", "show_ad", "toutiao_ad_excluded", "close_card", "lynx_status", "qpon_join", "apk_download_user", "comment_key_word_show", "v3_show_ad", "show_wish_button", "enterSection", "single_comment_show", "enter_product_detail", "xigua_ad_request", "qpon_apply", "splash_total_duration", "splash_render_duration", "download_template_duration", "homepage_hot", "homepage_follow", "homepage_fresh", "video_play_success", "general_search", "video_render_cost", "single_ad_render_cost", "unexpected_accurate_pause", "mnpl_interact_skip", "web_report_request_url", "web_report_init_status", "first_request", "video_ended", "mnpl_script_error", "open_wechat_failed_shake", "open_wechat_shacke", "open_wechat_success_shake", "options_popup", "close_pers_ads_type", "check_closed_type", "ad_guide_panel", "learn_ads", "learn_adx_ads", "learn_pers_ads", "resume_closed_type", "twist", "open_wechat_shake" })[i], 1);
            ++i;
        }
    }
    
    public static void \u3007o\u3007(final int i, final List<b.b.a.a.i.a.m.a> list, long delta) {
        if (i.\u3007\u30078O0\u30078().\u3007080().e()) {
            delta = System.currentTimeMillis() - delta;
            final StringBuilder sb = new StringBuilder();
            sb.append("uploadCost:");
            sb.append(delta);
            c.\u3007080(sb.toString());
            if (i == 200) {
                final b.b.a.a.i.a.k.e.a oo80 = d.oO80;
                oo80.OOO\u3007O0().getAndAdd(delta);
                oo80.OO0o\u3007\u3007().incrementAndGet();
                oo80.OoO8().getAndAdd(list.size());
                oo80.\u3007O00().getAndAdd(list.size());
            }
            else {
                final StringBuilder sb2 = new StringBuilder();
                sb2.append("-------AdThread code is ");
                sb2.append(i);
                sb2.append(" error  ------------");
                c.\u3007o\u3007(sb2.toString());
                if (i == -1) {
                    d.oO80.\u30070\u3007O0088o().getAndAdd(list.size());
                }
                else {
                    d.oO80.\u3007\u30078O0\u30078().getAndAdd(list.size());
                }
                final b.b.a.a.i.a.k.e.a oo81 = d.oO80;
                oo81.O0o\u3007\u3007Oo().getAndAdd(delta);
                oo81.\u3007O8o08O().incrementAndGet();
            }
        }
    }
    
    public static String \u3007\u3007808\u3007(final b.b.a.a.i.a.m.a a) {
        if (a == null || a.b() == null) {
            return null;
        }
        if (OoO8()) {
            return null;
        }
        if (a.g() == 3) {
            return a.b().optString("event");
        }
        return a.b().optString("label");
    }
    
    public static void \u3007\u3007888(final List<b.b.a.a.i.a.m.a> list, final int n, final String s) {
        final f \u3007080 = i.\u3007\u30078O0\u30078().\u3007080();
        if (\u3007080 != null && \u3007080.c() && list != null) {
            if (!OoO8()) {
                final StringBuilder sb = new StringBuilder();
                final Iterator iterator = list.iterator();
                boolean b = false;
                while (iterator.hasNext()) {
                    final b.b.a.a.i.a.m.a a = (b.b.a.a.i.a.m.a)iterator.next();
                    if (a.f() == 0) {
                        final JSONObject b2 = a.b();
                        String s2 = \u3007\u3007808\u3007(a);
                        if (a.g() == 3) {
                            if (b2 != null) {
                                s2 = b2.optString("event");
                            }
                            sb.append(" [v3:");
                            sb.append(s2);
                            sb.append("] ");
                        }
                        else {
                            final long oo88o8O = oo88o8O(a);
                            final long o\u3007O8\u3007\u3007o = o\u3007O8\u3007\u3007o(a);
                            final int \u3007\u30078O0\u30078 = \u3007\u30078O0\u30078(a);
                            sb.append(" [");
                            sb.append(oo88o8O);
                            sb.append("_");
                            sb.append(s2);
                            if (o\u3007O8\u3007\u3007o != 0L) {
                                sb.append("_");
                                sb.append(o\u3007O8\u3007\u3007o);
                            }
                            if (\u3007\u30078O0\u30078 == 0) {
                                sb.append("] ");
                            }
                            else {
                                sb.append("_");
                                sb.append(\u3007\u30078O0\u30078);
                                sb.append("] ");
                            }
                        }
                        b = true;
                    }
                    else {
                        if (a.f() != 1) {
                            continue;
                        }
                        final String o8ooOoo\u3007 = O8ooOoo\u3007(a);
                        final int o\u30078O8\u3007008 = O\u30078O8\u3007008(a);
                        sb.append(" [");
                        sb.append(o\u30078O8\u3007008);
                        sb.append("_");
                        sb.append(o8ooOoo\u3007);
                        sb.append("] ");
                    }
                }
                if (b) {
                    final StringBuilder sb2 = new StringBuilder();
                    sb2.append("ads:");
                    sb2.append((Object)sb);
                    sb2.append(\u3007o00\u3007\u3007Oo(n));
                    sb2.append(",");
                    sb2.append(s);
                    sb2.append(",total:");
                    sb2.append(list.size());
                    c.o\u30070("_upload", sb2.toString());
                }
                else {
                    final StringBuilder sb3 = new StringBuilder();
                    sb3.append("stats:");
                    sb3.append((Object)sb);
                    sb3.append(\u3007o00\u3007\u3007Oo(n));
                    sb3.append(",");
                    sb3.append(s);
                    sb3.append(",total:");
                    sb3.append(list.size());
                    c.o\u30070("_upload", sb3.toString());
                }
            }
        }
    }
    
    public static int \u3007\u30078O0\u30078(final b.b.a.a.i.a.m.a a) {
        monitorenter(a.class);
        if (a != null) {
            try {
                if (a.b() != null) {
                    if (!Oooo8o0\u3007()) {
                        return 0;
                    }
                    try {
                        return new JSONObject(a.b().optString("ad_extra_data")).optInt("sdk_event_self_count");
                    }
                    catch (final Exception ex) {
                        ex.printStackTrace();
                        return 0;
                    }
                }
            }
            finally {
                monitorexit(a.class);
            }
        }
        monitorexit(a.class);
        return 0;
    }
}

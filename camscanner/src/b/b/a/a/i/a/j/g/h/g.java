// 
// Decompiled by Procyon v0.6.0
// 

package b.b.a.a.i.a.j.g.h;

import android.database.Cursor;
import b.b.a.a.i.a.l.b;
import b.b.a.a.i.a.k.d;
import java.util.Iterator;
import java.util.LinkedList;
import android.text.TextUtils;
import b.b.a.a.i.a.i;
import java.util.ArrayList;
import android.content.Context;
import java.util.List;
import b.b.a.a.i.a.m.d.a;

public class g extends c
{
    private a Oo08;
    protected List<String> o\u30070;
    
    public g(final Context context, final a oo08) {
        super(context);
        this.o\u30070 = new ArrayList<String>();
        this.Oo08 = oo08;
        if (oo08 == null) {
            this.Oo08 = a.\u3007080();
        }
    }
    
    public static String Oooo8o0\u3007() {
        final StringBuilder sb = new StringBuilder();
        sb.append("CREATE TABLE IF NOT EXISTS ");
        sb.append(i.\u3007\u30078O0\u30078().\u3007\u3007808\u3007().c());
        sb.append(" (_id INTEGER PRIMARY KEY AUTOINCREMENT,");
        sb.append("id");
        sb.append(" TEXT UNIQUE,");
        sb.append("value");
        sb.append(" TEXT ,");
        sb.append("gen_time");
        sb.append(" TEXT , ");
        sb.append("retry");
        sb.append(" INTEGER default 0 , ");
        sb.append("encrypt");
        sb.append(" INTEGER default 0");
        sb.append(")");
        return sb.toString();
    }
    
    private static String oO80(String s, final String s2) {
        if (TextUtils.isEmpty((CharSequence)s)) {
            s = s2;
        }
        return s;
    }
    
    private static String \u300780\u3007808\u3007O(final String s, final List<?> list, int a, final boolean b) {
        String s2;
        if (b) {
            s2 = " IN ";
        }
        else {
            s2 = " NOT IN ";
        }
        String str;
        if (b) {
            str = " OR ";
        }
        else {
            str = " AND ";
        }
        final int min = Math.min(a, 1000);
        final int size = list.size();
        if (size % min == 0) {
            a = size / min;
        }
        else {
            a = size / min + 1;
        }
        final StringBuilder sb = new StringBuilder();
        for (int i = 0; i < a; ++i) {
            final int n = i * min;
            final String oo80 = oO80(TextUtils.join((CharSequence)"','", (Iterable)list.subList(n, Math.min(n + min, size))), "");
            if (i != 0) {
                sb.append(str);
            }
            sb.append(s);
            sb.append(s2);
            sb.append("('");
            sb.append(oo80);
            sb.append("')");
        }
        final String string = sb.toString();
        final StringBuilder sb2 = new StringBuilder();
        sb2.append(s);
        sb2.append(s2);
        sb2.append("('')");
        return oO80(string, sb2.toString());
    }
    
    public void OO0o\u3007\u3007(final List<b.b.a.a.i.a.m.a> list) {
        if (list != null) {
            if (list.size() != 0) {
                final LinkedList list2 = new LinkedList();
                for (final b.b.a.a.i.a.m.a a : list) {
                    list2.add(a.i());
                    b.b.a.a.i.a.l.a.\u3007O8o08O(a);
                }
                final StringBuilder sb = new StringBuilder();
                sb.append(this.Oo08());
                sb.append(" stats repo delete: ");
                sb.append(list2.size());
                b.b.a.a.i.a.l.c.\u3007o00\u3007\u3007Oo("PADLT", sb.toString());
                final StringBuilder sb2 = new StringBuilder();
                sb2.append("DELETE FROM ");
                sb2.append(this.Oo08());
                sb2.append(" WHERE ");
                sb2.append(\u300780\u3007808\u3007O("id", list2, 1000, true));
                b.b.a.a.i.a.j.g.c.O8(this.\u3007080(), sb2.toString());
                this.\u3007o\u3007(list2);
            }
        }
    }
    
    public List<b.b.a.a.i.a.m.a> OO0o\u3007\u3007\u3007\u30070(final int p0, final String p1) {
        // 
        // This method could not be decompiled.
        // 
        // Original Bytecode:
        // 
        //     1: aload_0        
        //     2: invokevirtual   b/b/a/a/i/a/j/g/h/c.\u3007080:()Landroid/content/Context;
        //     5: invokestatic    b/b/a/a/i/a/k/a.\u3007080:(ILandroid/content/Context;)J
        //     8: lstore_3       
        //     9: lload_3        
        //    10: lconst_0       
        //    11: lcmp           
        //    12: ifle            62
        //    15: aload_2        
        //    16: invokestatic    android/text/TextUtils.isEmpty:(Ljava/lang/CharSequence;)Z
        //    19: ifne            62
        //    22: new             Ljava/lang/StringBuilder;
        //    25: dup            
        //    26: invokespecial   java/lang/StringBuilder.<init>:()V
        //    29: astore          6
        //    31: aload           6
        //    33: aload_2        
        //    34: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //    37: pop            
        //    38: aload           6
        //    40: ldc             " DESC limit "
        //    42: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //    45: pop            
        //    46: aload           6
        //    48: lload_3        
        //    49: invokevirtual   java/lang/StringBuilder.append:(J)Ljava/lang/StringBuilder;
        //    52: pop            
        //    53: aload           6
        //    55: invokevirtual   java/lang/StringBuilder.toString:()Ljava/lang/String;
        //    58: astore_2       
        //    59: goto            64
        //    62: aconst_null    
        //    63: astore_2       
        //    64: new             Ljava/util/LinkedList;
        //    67: dup            
        //    68: invokespecial   java/util/LinkedList.<init>:()V
        //    71: astore          7
        //    73: aload_0        
        //    74: getfield        b/b/a/a/i/a/j/g/h/g.o\u30070:Ljava/util/List;
        //    77: invokeinterface java/util/List.clear:()V
        //    82: aload_0        
        //    83: invokevirtual   b/b/a/a/i/a/j/g/h/c.\u3007080:()Landroid/content/Context;
        //    86: aload_0        
        //    87: invokevirtual   b/b/a/a/i/a/j/g/h/g.Oo08:()Ljava/lang/String;
        //    90: iconst_3       
        //    91: anewarray       Ljava/lang/String;
        //    94: dup            
        //    95: iconst_0       
        //    96: ldc             "id"
        //    98: aastore        
        //    99: dup            
        //   100: iconst_1       
        //   101: ldc             "value"
        //   103: aastore        
        //   104: dup            
        //   105: iconst_2       
        //   106: ldc             "encrypt"
        //   108: aastore        
        //   109: aconst_null    
        //   110: aconst_null    
        //   111: aconst_null    
        //   112: aconst_null    
        //   113: aload_2        
        //   114: invokestatic    b/b/a/a/i/a/j/g/c.\u3007o\u3007:(Landroid/content/Context;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
        //   117: astore          8
        //   119: aload           8
        //   121: ifnull          362
        //   124: aload           8
        //   126: invokeinterface android/database/Cursor.moveToNext:()Z
        //   131: istore          5
        //   133: iload           5
        //   135: ifeq            284
        //   138: aload           8
        //   140: aload           8
        //   142: ldc             "id"
        //   144: invokeinterface android/database/Cursor.getColumnIndex:(Ljava/lang/String;)I
        //   149: invokeinterface android/database/Cursor.getString:(I)Ljava/lang/String;
        //   154: astore          9
        //   156: aload           8
        //   158: aload           8
        //   160: ldc             "value"
        //   162: invokeinterface android/database/Cursor.getColumnIndex:(Ljava/lang/String;)I
        //   167: invokeinterface android/database/Cursor.getString:(I)Ljava/lang/String;
        //   172: astore          6
        //   174: aload           6
        //   176: astore_2       
        //   177: aload           8
        //   179: aload           8
        //   181: ldc             "encrypt"
        //   183: invokeinterface android/database/Cursor.getColumnIndex:(Ljava/lang/String;)I
        //   188: invokeinterface android/database/Cursor.getInt:(I)I
        //   193: iconst_1       
        //   194: if_icmpne       211
        //   197: invokestatic    b/b/a/a/i/a/i.\u3007\u30078O0\u30078:()Lb/b/a/a/i/a/i;
        //   200: invokevirtual   b/b/a/a/i/a/i.\u3007080:()Lb/b/a/a/i/a/f;
        //   203: aload           6
        //   205: invokeinterface b/b/a/a/i/a/f.a:(Ljava/lang/String;)Ljava/lang/String;
        //   210: astore_2       
        //   211: aload_2        
        //   212: invokestatic    android/text/TextUtils.isEmpty:(Ljava/lang/CharSequence;)Z
        //   215: ifeq            233
        //   218: aload_0        
        //   219: getfield        b/b/a/a/i/a/j/g/h/g.o\u30070:Ljava/util/List;
        //   222: aload           9
        //   224: invokeinterface java/util/List.add:(Ljava/lang/Object;)Z
        //   229: pop            
        //   230: goto            124
        //   233: new             Lorg/json/JSONObject;
        //   236: astore          6
        //   238: aload           6
        //   240: aload_2        
        //   241: invokespecial   org/json/JSONObject.<init>:(Ljava/lang/String;)V
        //   244: new             Lb/b/a/a/i/a/m/c/a;
        //   247: astore_2       
        //   248: aload_2        
        //   249: aload           9
        //   251: aload           6
        //   253: invokespecial   b/b/a/a/i/a/m/c/a.<init>:(Ljava/lang/String;Lorg/json/JSONObject;)V
        //   256: aload_2        
        //   257: aload_0        
        //   258: invokevirtual   b/b/a/a/i/a/j/g/h/g.\u3007O00:()B
        //   261: invokevirtual   b/b/a/a/i/a/m/c/a.\u3007o00\u3007\u3007Oo:(B)V
        //   264: aload_2        
        //   265: aload_0        
        //   266: invokevirtual   b/b/a/a/i/a/j/g/h/g.\u3007O\u3007:()B
        //   269: invokevirtual   b/b/a/a/i/a/m/c/a.\u3007080:(B)V
        //   272: aload           7
        //   274: aload_2        
        //   275: invokeinterface java/util/List.add:(Ljava/lang/Object;)Z
        //   280: pop            
        //   281: goto            124
        //   284: aload           8
        //   286: invokeinterface android/database/Cursor.close:()V
        //   291: aload_0        
        //   292: getfield        b/b/a/a/i/a/j/g/h/g.o\u30070:Ljava/util/List;
        //   295: invokeinterface java/util/List.isEmpty:()Z
        //   300: ifne            362
        //   303: aload_0        
        //   304: aload_0        
        //   305: getfield        b/b/a/a/i/a/j/g/h/g.o\u30070:Ljava/util/List;
        //   308: invokevirtual   b/b/a/a/i/a/j/g/h/g.\u3007\u3007808\u3007:(Ljava/util/List;)V
        //   311: aload_0        
        //   312: getfield        b/b/a/a/i/a/j/g/h/g.o\u30070:Ljava/util/List;
        //   315: invokeinterface java/util/List.clear:()V
        //   320: goto            362
        //   323: astore_2       
        //   324: aload           8
        //   326: invokeinterface android/database/Cursor.close:()V
        //   331: aload_0        
        //   332: getfield        b/b/a/a/i/a/j/g/h/g.o\u30070:Ljava/util/List;
        //   335: invokeinterface java/util/List.isEmpty:()Z
        //   340: ifne            360
        //   343: aload_0        
        //   344: aload_0        
        //   345: getfield        b/b/a/a/i/a/j/g/h/g.o\u30070:Ljava/util/List;
        //   348: invokevirtual   b/b/a/a/i/a/j/g/h/g.\u3007\u3007808\u3007:(Ljava/util/List;)V
        //   351: aload_0        
        //   352: getfield        b/b/a/a/i/a/j/g/h/g.o\u30070:Ljava/util/List;
        //   355: invokeinterface java/util/List.clear:()V
        //   360: aload_2        
        //   361: athrow         
        //   362: aload           7
        //   364: areturn        
        //   365: astore_2       
        //   366: goto            124
        //   369: astore_2       
        //   370: goto            362
        //   373: astore          6
        //   375: goto            360
        //    Signature:
        //  (ILjava/lang/String;)Ljava/util/List<Lb/b/a/a/i/a/m/a;>;
        //    Exceptions:
        //  Try           Handler
        //  Start  End    Start  End    Type                 
        //  -----  -----  -----  -----  ---------------------
        //  124    133    323    362    Any
        //  138    174    365    369    Any
        //  177    211    365    369    Any
        //  211    230    365    369    Any
        //  233    281    365    369    Any
        //  284    320    369    373    Ljava/lang/Exception;
        //  324    360    373    378    Ljava/lang/Exception;
        // 
        // The error that occurred was:
        // 
        // java.lang.IndexOutOfBoundsException: Index 170 out of bounds for length 170
        //     at java.base/jdk.internal.util.Preconditions.outOfBounds(Preconditions.java:64)
        //     at java.base/jdk.internal.util.Preconditions.outOfBoundsCheckIndex(Preconditions.java:70)
        //     at java.base/jdk.internal.util.Preconditions.checkIndex(Preconditions.java:266)
        //     at java.base/java.util.Objects.checkIndex(Objects.java:361)
        //     at java.base/java.util.ArrayList.get(ArrayList.java:427)
        //     at com.strobel.decompiler.ast.AstBuilder.convertToAst(AstBuilder.java:3362)
        //     at com.strobel.decompiler.ast.AstBuilder.build(AstBuilder.java:112)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:203)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:93)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethodBody(AstBuilder.java:868)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethod(AstBuilder.java:761)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addTypeMembers(AstBuilder.java:638)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeCore(AstBuilder.java:605)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeNoCache(AstBuilder.java:195)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createType(AstBuilder.java:162)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addType(AstBuilder.java:137)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.buildAst(JavaLanguage.java:71)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.decompileType(JavaLanguage.java:59)
        //     at com.strobel.decompiler.DecompilerDriver.decompileType(DecompilerDriver.java:333)
        //     at com.strobel.decompiler.DecompilerDriver.decompileJar(DecompilerDriver.java:254)
        //     at com.strobel.decompiler.DecompilerDriver.main(DecompilerDriver.java:144)
        // 
        throw new IllegalStateException("An error occurred while decompiling this method.");
    }
    
    @Override
    public String Oo08() {
        return i.\u3007\u30078O0\u30078().\u3007\u3007808\u3007().c();
    }
    
    public List<b.b.a.a.i.a.m.a> \u30078o8o\u3007(final String s) {
        final a oo08 = this.Oo08;
        if (oo08 == null) {
            return new ArrayList<b.b.a.a.i.a.m.a>();
        }
        return this.OO0o\u3007\u3007\u3007\u30070(oo08.O8(), s);
    }
    
    public byte \u3007O00() {
        return 2;
    }
    
    public boolean \u3007O8o08O(final int n) {
        final a oo08 = this.Oo08;
        boolean b = false;
        if (oo08 == null) {
            return false;
        }
        if (this.\u3007\u30078O0\u30078() >= this.Oo08.Oo08()) {
            b = true;
        }
        return b;
    }
    
    public byte \u3007O\u3007() {
        return 1;
    }
    
    protected void \u3007\u3007808\u3007(final List<String> list) {
        final StringBuilder sb = new StringBuilder();
        sb.append(this.Oo08());
        sb.append(" stats repo delete: ");
        sb.append(list.size());
        b.b.a.a.i.a.l.c.\u3007o00\u3007\u3007Oo("PADLT", sb.toString());
        final StringBuilder sb2 = new StringBuilder();
        sb2.append("DELETE FROM ");
        sb2.append(this.Oo08());
        sb2.append(" WHERE ");
        sb2.append(\u300780\u3007808\u3007O("id", list, 1000, true));
        b.b.a.a.i.a.j.g.c.O8(this.\u3007080(), sb2.toString());
        b.\u3007080(d.oO80.\u3007oOO8O8(), list.size());
        this.\u3007o\u3007(list);
    }
    
    public int \u3007\u30078O0\u30078() {
        final int n = 0;
        int n2 = 0;
        int int1 = 0;
        Label_0086: {
            Cursor cursor = null;
            Label_0079: {
                try {
                    final Cursor \u3007o\u3007 = b.b.a.a.i.a.j.g.c.\u3007o\u3007(this.\u3007080(), this.Oo08(), new String[] { "count(1)" }, null, null, null, null, null);
                    if (\u3007o\u3007 != null) {
                        try {
                            \u3007o\u3007.moveToFirst();
                            int1 = \u3007o\u3007.getInt(0);
                        }
                        finally {
                            break Label_0079;
                        }
                    }
                    n2 = int1;
                    if (\u3007o\u3007 != null) {
                        break Label_0086;
                    }
                    return n2;
                }
                finally {
                    cursor = null;
                }
            }
            if (cursor == null) {
                return n2;
            }
            int1 = n;
            try {
                cursor.close();
                n2 = int1;
                return n2;
            }
            catch (final Exception ex) {
                n2 = int1;
                return n2;
            }
        }
    }
}

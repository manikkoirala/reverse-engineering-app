// 
// Decompiled by Procyon v0.6.0
// 

package b.b.a.a.i.a.o;

import android.text.TextUtils;
import android.annotation.SuppressLint;
import java.util.UUID;
import java.security.SecureRandom;
import Oo08.\u3007080;
import android.os.Build$VERSION;
import java.util.Random;
import java.util.Iterator;
import b.b.a.a.i.a.f;
import b.b.a.a.i.a.i;
import java.util.List;
import android.content.Context;

public class c implements b.b.a.a.i.a.o.b
{
    private final Context \u3007080;
    private final e \u3007o00\u3007\u3007Oo;
    
    public c(final Context \u3007080, final e \u3007o00\u3007\u3007Oo) {
        this.\u3007080 = \u3007080;
        this.\u3007o00\u3007\u3007Oo = \u3007o00\u3007\u3007Oo;
    }
    
    private void Oo08(final List<d> list, final String s) {
        if (list != null && list.size() != 0) {
            final f \u3007080 = i.\u3007\u30078O0\u30078().\u3007080();
            for (final d d : list) {
                if (\u3007080 != null && \u3007080.a() != null) {
                    \u3007080.a().execute(new b(d, s));
                }
            }
        }
    }
    
    private static Random \u3007\u3007888() {
        if (Build$VERSION.SDK_INT >= 26) {
            try {
                return \u3007080.\u3007080();
            }
            finally {
                return new SecureRandom();
            }
        }
        return new SecureRandom();
    }
    
    @Override
    public void a(final String s) {
        final f \u3007080 = i.\u3007\u30078O0\u30078().\u3007080();
        if (\u3007080 != null) {
            if (i.\u3007\u30078O0\u30078().OO0o\u3007\u3007() != null) {
                if (!\u3007080.g()) {
                    return;
                }
                final b.b.a.a.i.a.n.e e = new b.b.a.a.i.a.n.e(this, "trackFailedUrls", s) {
                    final c OO;
                    final String \u3007OOo8\u30070;
                    
                    @Override
                    public void run() {
                        this.OO.Oo08(this.OO.\u3007o00\u3007\u3007Oo.a(), this.\u3007OOo8\u30070);
                    }
                };
                e.\u3007o\u3007(1);
                if (\u3007080.a() != null) {
                    \u3007080.a().execute(e);
                }
            }
        }
    }
    
    public Context o\u30070() {
        Context context;
        if ((context = this.\u3007080) == null) {
            context = i.\u3007\u30078O0\u30078().OO0o\u3007\u3007();
        }
        return context;
    }
    
    @Override
    public void \u3007080(final String s, final List<String> list, final boolean b) {
        final f \u3007080 = i.\u3007\u30078O0\u30078().\u3007080();
        if (\u3007080 != null && i.\u3007\u30078O0\u30078().OO0o\u3007\u3007() != null) {
            if (\u3007080.a() != null) {
                if (!\u3007080.g()) {
                    return;
                }
                if (list != null && list.size() != 0) {
                    for (final String s2 : list) {
                        final StringBuilder sb = new StringBuilder();
                        sb.append(UUID.randomUUID().toString());
                        sb.append("_");
                        sb.append(System.currentTimeMillis());
                        \u3007080.a().execute(new b(new d(sb.toString(), s2, b, 5), s));
                    }
                }
            }
        }
    }
    
    @SuppressLint({ "StaticFieldLeak" })
    private class b extends e
    {
        private final String OO;
        final c \u300708O\u300700\u3007o;
        private final d \u3007OOo8\u30070;
        
        private b(final c \u300708O\u300700\u3007o, final d \u3007oOo8\u30070, final String oo) {
            this.\u300708O\u300700\u3007o = \u300708O\u300700\u3007o;
            super("AdsStats");
            this.\u3007OOo8\u30070 = \u3007oOo8\u30070;
            this.OO = oo;
        }
        
        private String o\u30070(final String s) {
            String replace = s;
            if (!TextUtils.isEmpty((CharSequence)s)) {
                String replace2 = null;
                Label_0056: {
                    if (!s.contains("{TS}")) {
                        replace2 = s;
                        if (!s.contains("__TS__")) {
                            break Label_0056;
                        }
                    }
                    final long currentTimeMillis = System.currentTimeMillis();
                    replace2 = s.replace("{TS}", String.valueOf(currentTimeMillis)).replace("__TS__", String.valueOf(currentTimeMillis));
                }
                if (!replace2.contains("{UID}")) {
                    replace = replace2;
                    if (!replace2.contains("__UID__")) {
                        return replace;
                    }
                }
                replace = replace2;
                if (!TextUtils.isEmpty((CharSequence)this.OO)) {
                    replace = replace2.replace("{UID}", this.OO).replace("__UID__", this.OO);
                }
            }
            return replace;
        }
        
        String O8(String s) {
            String s2 = s;
            if (!TextUtils.isEmpty((CharSequence)s)) {
                s2 = s;
                try {
                    final Random \u3007o\u3007 = \u3007\u3007888();
                    s2 = s;
                    s = (s2 = s.replace("[ss_random]", String.valueOf(\u3007o\u3007.nextLong())));
                    s = (s2 = s.replace("[ss_timestamp]", String.valueOf(System.currentTimeMillis())));
                }
                catch (final Exception ex) {
                    ex.printStackTrace();
                }
            }
            return s2;
        }
        
        boolean Oo08(final String s) {
            return !TextUtils.isEmpty((CharSequence)s) && (s.startsWith("http://") || s.startsWith("https://"));
        }
        
        @Override
        public void run() {
            final f \u3007080 = i.\u3007\u30078O0\u30078().\u3007080();
            if (\u3007080 != null) {
                if (i.\u3007\u30078O0\u30078().OO0o\u3007\u3007() != null) {
                    if (!\u3007080.g()) {
                        return;
                    }
                    if (!this.Oo08(this.\u3007OOo8\u30070.O8())) {
                        return;
                    }
                    if (this.\u3007OOo8\u30070.\u3007o\u3007() == 0) {
                        this.\u300708O\u300700\u3007o.\u3007o00\u3007\u3007Oo.\u3007o\u3007(this.\u3007OOo8\u30070);
                        return;
                    }
                    while (this.\u3007OOo8\u30070.\u3007o\u3007() > 0) {
                        try {
                            \u3007080.j();
                            if (this.\u3007OOo8\u30070.\u3007o\u3007() == 5) {
                                this.\u300708O\u300700\u3007o.\u3007o00\u3007\u3007Oo.\u3007o00\u3007\u3007Oo(this.\u3007OOo8\u30070);
                            }
                            if (!\u3007080.a(this.\u300708O\u300700\u3007o.o\u30070())) {
                                break;
                            }
                            final long currentTimeMillis = System.currentTimeMillis();
                            String s = this.\u3007OOo8\u30070.O8();
                            if (\u3007080.d() == 0) {
                                s = this.o\u30070(this.\u3007OOo8\u30070.O8());
                                if (this.\u3007OOo8\u30070.Oo08()) {
                                    s = this.O8(s);
                                }
                            }
                            final b.b.a.a.i.a.n.c i = \u3007080.i();
                            if (i == null) {
                                return;
                            }
                            i.a("User-Agent", \u3007080.k());
                            i.a(s);
                            b.b.a.a.i.a.n.d d = null;
                            try {
                                final b.b.a.a.i.a.n.d b = i.b();
                                try {
                                    \u3007080.a(b.b());
                                }
                                finally {}
                            }
                            finally {
                                d = null;
                            }
                            if (d != null && d.b()) {
                                this.\u300708O\u300700\u3007o.\u3007o00\u3007\u3007Oo.\u3007o\u3007(this.\u3007OOo8\u30070);
                                final StringBuilder sb = new StringBuilder();
                                sb.append("track success : ");
                                sb.append(this.\u3007OOo8\u30070.O8());
                                b.b.a.a.i.a.l.c.\u3007o00\u3007\u3007Oo("trackurl", sb.toString());
                                \u3007080.a(true, 200, System.currentTimeMillis() - currentTimeMillis);
                                break;
                            }
                            final StringBuilder sb2 = new StringBuilder();
                            sb2.append("track fail : ");
                            sb2.append(this.\u3007OOo8\u30070.O8());
                            b.b.a.a.i.a.l.c.\u3007o00\u3007\u3007Oo("trackurl", sb2.toString());
                            final d \u3007oOo8\u30070 = this.\u3007OOo8\u30070;
                            \u3007oOo8\u30070.\u3007o00\u3007\u3007Oo(\u3007oOo8\u30070.\u3007o\u3007() - 1);
                            if (this.\u3007OOo8\u30070.\u3007o\u3007() == 0) {
                                this.\u300708O\u300700\u3007o.\u3007o00\u3007\u3007Oo.\u3007o\u3007(this.\u3007OOo8\u30070);
                                final StringBuilder sb3 = new StringBuilder();
                                sb3.append("track fail and delete : ");
                                sb3.append(this.\u3007OOo8\u30070.O8());
                                b.b.a.a.i.a.l.c.\u3007o00\u3007\u3007Oo("trackurl", sb3.toString());
                                break;
                            }
                            this.\u300708O\u300700\u3007o.\u3007o00\u3007\u3007Oo.\u3007080(this.\u3007OOo8\u30070);
                            if (d != null) {
                                \u3007080.a(false, d.a(), System.currentTimeMillis());
                            }
                            else {
                                \u3007080.a(false, 0, System.currentTimeMillis());
                            }
                        }
                        finally {}
                    }
                }
            }
        }
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package b.b.a.a.i.a;

import b.b.a.a.i.a.n.c;
import android.content.Context;
import java.util.concurrent.Executor;
import b.b.a.a.i.a.m.a;
import org.json.JSONObject;

public interface f
{
    a a(final JSONObject p0);
    
    String a(final String p0);
    
    Executor a();
    
    void a(final boolean p0);
    
    void a(final boolean p0, final int p1, final long p2);
    
    boolean a(final Context p0);
    
    String b(final String p0);
    
    Executor b();
    
    boolean c();
    
    int d();
    
    boolean e();
    
    boolean f();
    
    boolean g();
    
    h h();
    
    c i();
    
    void j();
    
    String k();
    
    boolean l();
    
    g m();
    
    boolean n();
    
    String o();
}

// 
// Decompiled by Procyon v0.6.0
// 

package b.b.a.a.i.a.j;

import java.util.Iterator;
import android.text.TextUtils;
import java.util.List;
import android.content.Context;
import b.b.a.a.i.a.i;
import b.b.a.a.i.a.j.g.h.b;
import b.b.a.a.i.a.j.g.h.e;
import b.b.a.a.i.a.j.g.h.f;
import b.b.a.a.i.a.m.d.a;
import b.b.a.a.i.a.j.g.h.g;

public class c implements d
{
    private g O8;
    private a OO0o\u3007\u3007\u3007\u30070;
    private f Oo08;
    private a oO80;
    private e o\u30070;
    private b.b.a.a.i.a.j.g.h.d \u3007080;
    private a \u300780\u3007808\u3007O;
    private a \u30078o8o\u3007;
    private a \u3007O8o08O;
    private b.b.a.a.i.a.j.g.h.a \u3007o00\u3007\u3007Oo;
    private b \u3007o\u3007;
    private a \u3007\u3007888;
    
    public c() {
        final Context oo0o\u3007\u3007 = i.\u3007\u30078O0\u30078().OO0o\u3007\u3007();
        if (b.b.a.a.i.a.k.a.o\u30070()) {
            final a \u3007o00 = i.\u3007\u30078O0\u30078().\u3007O00();
            this.\u3007\u3007888 = \u3007o00;
            this.\u3007080 = new b.b.a.a.i.a.j.g.h.d(oo0o\u3007\u3007, \u3007o00);
        }
        if (b.b.a.a.i.a.k.a.O8()) {
            final a ooO8 = i.\u3007\u30078O0\u30078().OoO8();
            this.\u300780\u3007808\u3007O = ooO8;
            this.\u3007o\u3007 = new b(oo0o\u3007\u3007, ooO8);
        }
        if (b.b.a.a.i.a.k.a.\u3007o00\u3007\u3007Oo()) {
            final a ooO9 = i.\u3007\u30078O0\u30078().OoO8();
            this.oO80 = ooO9;
            this.\u3007o00\u3007\u3007Oo = new b.b.a.a.i.a.j.g.h.a(oo0o\u3007\u3007, ooO9);
        }
        if (b.b.a.a.i.a.k.a.oO80()) {
            final a ooO10 = i.\u3007\u30078O0\u30078().OoO8();
            this.OO0o\u3007\u3007\u3007\u30070 = ooO10;
            this.O8 = new g(oo0o\u3007\u3007, ooO10);
        }
        if (b.b.a.a.i.a.k.a.Oo08()) {
            final a \u30078o8o\u3007 = i.\u3007\u30078O0\u30078().\u30078o8o\u3007();
            this.\u30078o8o\u3007 = \u30078o8o\u3007;
            this.Oo08 = new f(oo0o\u3007\u3007, \u30078o8o\u3007);
        }
        if (b.b.a.a.i.a.k.a.\u3007\u3007888()) {
            final a o800o8O = i.\u3007\u30078O0\u30078().o800o8O();
            this.\u3007O8o08O = o800o8O;
            this.o\u30070 = new e(oo0o\u3007\u3007, o800o8O);
        }
    }
    
    private boolean O8(final List<b.b.a.a.i.a.m.a> list, final List<String> list2) {
        if (list != null && !list.isEmpty() && list2 != null && !list2.isEmpty()) {
            try {
                final Iterator iterator = list.iterator();
                while (iterator.hasNext()) {
                    final b.b.a.a.i.a.m.a a = (b.b.a.a.i.a.m.a)iterator.next();
                    if (a != null) {
                        final String i = a.i();
                        if (TextUtils.isEmpty((CharSequence)i) || !list2.contains(i)) {
                            continue;
                        }
                        iterator.remove();
                    }
                }
            }
            finally {
                final StringBuilder sb = new StringBuilder();
                sb.append("deleteMemList: ");
                final Throwable t;
                sb.append(t.getMessage());
                b.b.a.a.i.a.l.c.O8("DBCacheStrategy", sb.toString());
            }
        }
        return list != null && !list.isEmpty();
    }
    
    public List<b.b.a.a.i.a.m.a> Oo08(final b.b.a.a.i.a.m.a a, final int n) {
        if (a.f() != 0 || a.c() != 1 || !b.b.a.a.i.a.k.a.o\u30070()) {
            if (a.f() == 3 && a.c() == 2 && b.b.a.a.i.a.k.a.O8()) {
                if (this.\u300780\u3007808\u3007O.O8() > n) {
                    return this.\u3007o\u3007.OO0o\u3007\u3007\u3007\u30070(this.\u300780\u3007808\u3007O.O8() - n, "_id");
                }
            }
            else if (a.f() == 0 && a.c() == 2 && b.b.a.a.i.a.k.a.\u3007o00\u3007\u3007Oo()) {
                if (this.oO80.O8() > n) {
                    final List<b.b.a.a.i.a.m.a> oo0o\u3007\u3007\u3007\u30070 = this.\u3007o00\u3007\u3007Oo.OO0o\u3007\u3007\u3007\u30070(this.oO80.O8() - n, "_id");
                    if (oo0o\u3007\u3007\u3007\u30070 != null && oo0o\u3007\u3007\u3007\u30070.size()) {
                        b.b.a.a.i.a.l.b.\u3007080(b.b.a.a.i.a.k.d.oO80.\u300700\u30078(), 1);
                    }
                    return oo0o\u3007\u3007\u3007\u30070;
                }
            }
            else if (a.f() == 1 && a.c() == 2 && b.b.a.a.i.a.k.a.oO80()) {
                if (this.OO0o\u3007\u3007\u3007\u30070.O8() > n) {
                    final List<b.b.a.a.i.a.m.a> oo0o\u3007\u3007\u3007\u30072 = this.O8.OO0o\u3007\u3007\u3007\u30070(this.OO0o\u3007\u3007\u3007\u30070.O8() - n, "_id");
                    if (oo0o\u3007\u3007\u3007\u30072 != null && oo0o\u3007\u3007\u3007\u30072.size()) {
                        b.b.a.a.i.a.l.b.\u3007080(b.b.a.a.i.a.k.d.oO80.o\u3007O8\u3007\u3007o(), 1);
                    }
                    return oo0o\u3007\u3007\u3007\u30072;
                }
            }
            else if (a.f() == 1 && a.c() == 3 && b.b.a.a.i.a.k.a.Oo08()) {
                if (this.\u30078o8o\u3007.O8() > n) {
                    final List<b.b.a.a.i.a.m.a> oo0o\u3007\u3007\u3007\u30073 = this.Oo08.OO0o\u3007\u3007\u3007\u30070(this.\u30078o8o\u3007.O8() - n, "_id");
                    if (oo0o\u3007\u3007\u3007\u30073 != null && oo0o\u3007\u3007\u3007\u30073.size()) {
                        b.b.a.a.i.a.l.b.\u3007080(b.b.a.a.i.a.k.d.oO80.O\u3007O\u3007oO(), 1);
                    }
                    return oo0o\u3007\u3007\u3007\u30073;
                }
            }
            else if (a.f() == 2 && a.c() == 3 && b.b.a.a.i.a.k.a.\u3007\u3007888() && this.\u3007O8o08O.O8() > n) {
                return this.o\u30070.OO0o\u3007\u3007\u3007\u30070(this.\u3007O8o08O.O8() - n, "_id");
            }
            return null;
        }
        if (this.\u3007\u3007888.O8() > n) {
            final List<b.b.a.a.i.a.m.a> oo0o\u3007\u3007\u3007\u30074 = this.\u3007080.OO0o\u3007\u3007\u3007\u30070(this.\u3007\u3007888.O8() - n, "_id");
            if (oo0o\u3007\u3007\u3007\u30074 != null && oo0o\u3007\u3007\u3007\u30074.size()) {
                b.b.a.a.i.a.l.b.\u3007080(b.b.a.a.i.a.k.d.oO80.\u3007o00\u3007\u3007Oo(), 1);
            }
            return oo0o\u3007\u3007\u3007\u30074;
        }
        return null;
    }
    
    @Override
    public boolean a(final int n, final boolean b) {
        if (b.b.a.a.i.a.k.a.o\u30070()) {
            final b.b.a.a.i.a.j.g.h.d \u3007080 = this.\u3007080;
            if (\u3007080 != null && \u3007080.\u3007O8o08O(n)) {
                b.b.a.a.i.a.l.b.\u3007080(b.b.a.a.i.a.k.d.oO80.o0O0(), 1);
                return true;
            }
        }
        if (b.b.a.a.i.a.k.a.O8()) {
            final b \u3007o\u3007 = this.\u3007o\u3007;
            if (\u3007o\u3007 != null && \u3007o\u3007.\u3007O8o08O(n)) {
                return true;
            }
        }
        if (b.b.a.a.i.a.k.a.\u3007o00\u3007\u3007Oo()) {
            final b.b.a.a.i.a.j.g.h.a \u3007o00\u3007\u3007Oo = this.\u3007o00\u3007\u3007Oo;
            if (\u3007o00\u3007\u3007Oo != null && \u3007o00\u3007\u3007Oo.\u3007O8o08O(n)) {
                b.b.a.a.i.a.l.b.\u3007080(b.b.a.a.i.a.k.d.oO80.\u30070000OOO(), 1);
                return true;
            }
        }
        if (b.b.a.a.i.a.k.a.oO80()) {
            final g o8 = this.O8;
            if (o8 != null && o8.\u3007O8o08O(n)) {
                b.b.a.a.i.a.l.b.\u3007080(b.b.a.a.i.a.k.d.oO80.oo88o8O(), 1);
                return true;
            }
        }
        if (b.b.a.a.i.a.k.a.Oo08()) {
            final f oo08 = this.Oo08;
            if (oo08 != null && oo08.\u3007O8o08O(n)) {
                b.b.a.a.i.a.l.b.\u3007080(b.b.a.a.i.a.k.d.oO80.O08000(), 1);
                return true;
            }
        }
        if (b.b.a.a.i.a.k.a.\u3007\u3007888()) {
            final e o\u30070 = this.o\u30070;
            if (o\u30070 != null && o\u30070.\u3007O8o08O(n)) {
                return true;
            }
        }
        return false;
    }
    
    @Override
    public List<b.b.a.a.i.a.m.a> \u3007080(final int n, final int n2, final List<String> list) {
        if (b.b.a.a.i.a.k.a.o\u30070()) {
            final List<b.b.a.a.i.a.m.a> \u30078o8o\u3007 = this.\u3007080.\u30078o8o\u3007("_id");
            if (this.O8(\u30078o8o\u3007, list)) {
                final StringBuilder sb = new StringBuilder();
                sb.append("high db get size:");
                sb.append(\u30078o8o\u3007.size());
                b.b.a.a.i.a.l.c.\u3007080(sb.toString());
                b.b.a.a.i.a.l.b.\u3007080(b.b.a.a.i.a.k.d.oO80.OO8oO0o\u3007(), 1);
                return \u30078o8o\u3007;
            }
        }
        if (b.b.a.a.i.a.k.a.O8()) {
            final List<b.b.a.a.i.a.m.a> \u30078o8o\u30072 = this.\u3007o\u3007.\u30078o8o\u3007("_id");
            if (this.O8(\u30078o8o\u30072, list)) {
                final StringBuilder sb2 = new StringBuilder();
                sb2.append("v3ad db get :");
                sb2.append(\u30078o8o\u30072.size());
                b.b.a.a.i.a.l.c.\u3007080(sb2.toString());
                return \u30078o8o\u30072;
            }
        }
        if (b.b.a.a.i.a.k.a.\u3007o00\u3007\u3007Oo()) {
            final List<b.b.a.a.i.a.m.a> \u30078o8o\u30073 = this.\u3007o00\u3007\u3007Oo.\u30078o8o\u3007("_id");
            if (this.O8(\u30078o8o\u30073, list)) {
                final StringBuilder sb3 = new StringBuilder();
                sb3.append("adevent db get :");
                sb3.append(\u30078o8o\u30073.size());
                b.b.a.a.i.a.l.c.\u3007080(sb3.toString());
                b.b.a.a.i.a.l.b.\u3007080(b.b.a.a.i.a.k.d.oO80.O\u30078O8\u3007008(), 1);
                return \u30078o8o\u30073;
            }
        }
        if (b.b.a.a.i.a.k.a.oO80()) {
            final List<b.b.a.a.i.a.m.a> \u30078o8o\u30074 = this.O8.\u30078o8o\u3007("_id");
            if (this.O8(\u30078o8o\u30074, list)) {
                final StringBuilder sb4 = new StringBuilder();
                sb4.append("real stats db get :");
                sb4.append(\u30078o8o\u30074.size());
                b.b.a.a.i.a.l.c.\u3007080(sb4.toString());
                b.b.a.a.i.a.l.b.\u3007080(b.b.a.a.i.a.k.d.oO80.\u3007O888o0o(), 1);
                return \u30078o8o\u30074;
            }
        }
        if (b.b.a.a.i.a.k.a.Oo08()) {
            final List<b.b.a.a.i.a.m.a> \u30078o8o\u30075 = this.Oo08.\u30078o8o\u3007("_id");
            if (this.O8(\u30078o8o\u30075, list)) {
                final StringBuilder sb5 = new StringBuilder();
                sb5.append("batch db get :");
                sb5.append(\u30078o8o\u30075.size());
                b.b.a.a.i.a.l.c.\u3007080(sb5.toString());
                b.b.a.a.i.a.l.b.\u3007080(b.b.a.a.i.a.k.d.oO80.\u30078(), 1);
                return \u30078o8o\u30075;
            }
        }
        if (b.b.a.a.i.a.k.a.\u3007\u3007888()) {
            final List<b.b.a.a.i.a.m.a> \u30078o8o\u30076 = this.o\u30070.\u30078o8o\u3007("_id");
            if (this.O8(\u30078o8o\u30076, list)) {
                final StringBuilder sb6 = new StringBuilder();
                sb6.append("other db get :");
                sb6.append(\u30078o8o\u30076.size());
                b.b.a.a.i.a.l.c.\u3007080(sb6.toString());
                return \u30078o8o\u30076;
            }
        }
        return null;
    }
    
    @Override
    public void \u3007o00\u3007\u3007Oo(final b.b.a.a.i.a.m.a a, final int n) {
        if (a == null) {
            return;
        }
        try {
            a.b(System.currentTimeMillis());
            if (a.f() == 0 && a.c() == 1) {
                if (b.b.a.a.i.a.k.a.o\u30070()) {
                    this.\u3007080.\u3007o00\u3007\u3007Oo(a);
                }
            }
            else if (a.f() == 3 && a.c() == 2) {
                if (b.b.a.a.i.a.k.a.O8()) {
                    this.\u3007o\u3007.\u3007o00\u3007\u3007Oo(a);
                }
            }
            else if (a.f() == 0 && a.c() == 2) {
                if (b.b.a.a.i.a.k.a.\u3007o00\u3007\u3007Oo()) {
                    this.\u3007o00\u3007\u3007Oo.\u3007o00\u3007\u3007Oo(a);
                }
            }
            else if (a.f() == 1 && a.c() == 2) {
                if (b.b.a.a.i.a.k.a.oO80()) {
                    this.O8.\u3007o00\u3007\u3007Oo(a);
                }
            }
            else if (a.f() == 1 && a.c() == 3) {
                if (b.b.a.a.i.a.k.a.Oo08()) {
                    this.Oo08.\u3007o00\u3007\u3007Oo(a);
                }
            }
            else if (a.f() == 2 && a.c() == 3 && b.b.a.a.i.a.k.a.\u3007\u3007888()) {
                this.o\u30070.\u3007o00\u3007\u3007Oo(a);
            }
        }
        finally {
            final Throwable t;
            t.printStackTrace();
            b.b.a.a.i.a.l.b.\u3007080(b.b.a.a.i.a.k.d.oO80.OO0o\u3007\u3007\u3007\u30070(), 1);
        }
    }
    
    @Override
    public void \u3007o\u3007(final int n, final List<b.b.a.a.i.a.m.a> list) {
        b.b.a.a.i.a.l.c.\u3007080("dbCache handleResult start");
        if (list != null && list.size() != 0 && list.get(0) != null) {
            final b.b.a.a.i.a.m.a a = (b.b.a.a.i.a.m.a)list.get(0);
            if (n == 200 || n == -1) {
                final b.b.a.a.i.a.k.e.a oo80 = b.b.a.a.i.a.k.d.oO80;
                b.b.a.a.i.a.l.b.\u3007080(oo80.oO(), list.size());
                if (n != 200) {
                    b.b.a.a.i.a.l.b.\u3007080(oo80.O8(), list.size());
                }
                if (a.f() == 0 && a.c() == 1) {
                    if (b.b.a.a.i.a.k.a.o\u30070()) {
                        this.\u3007080.OO0o\u3007\u3007(list);
                    }
                }
                else if (a.f() == 3 && a.c() == 2) {
                    if (b.b.a.a.i.a.k.a.O8()) {
                        this.\u3007o\u3007.OO0o\u3007\u3007(list);
                    }
                }
                else if (a.f() == 0 && a.c() == 2) {
                    if (b.b.a.a.i.a.k.a.\u3007o00\u3007\u3007Oo()) {
                        this.\u3007o00\u3007\u3007Oo.OO0o\u3007\u3007(list);
                    }
                }
                else if (a.f() == 1 && a.c() == 2) {
                    if (b.b.a.a.i.a.k.a.oO80()) {
                        this.O8.OO0o\u3007\u3007(list);
                    }
                }
                else if (a.f() == 1 && a.c() == 3) {
                    if (b.b.a.a.i.a.k.a.Oo08()) {
                        this.Oo08.OO0o\u3007\u3007(list);
                    }
                }
                else if (a.f() == 2 && a.c() == 3 && b.b.a.a.i.a.k.a.\u3007\u3007888()) {
                    this.o\u30070.OO0o\u3007\u3007(list);
                }
            }
        }
        b.b.a.a.i.a.l.c.\u3007080("dbCache handleResult end");
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package b.b.a.a.i.a.j.g.h;

import b.b.a.a.i.a.l.b;
import b.b.a.a.i.a.k.d;
import b.b.a.a.i.a.j.e;
import java.util.Iterator;
import java.util.LinkedList;
import android.text.TextUtils;
import b.b.a.a.i.a.i;
import java.util.ArrayList;
import java.util.List;
import android.content.Context;

public class a extends c
{
    private final Context Oo08;
    private b.b.a.a.i.a.m.d.a o\u30070;
    protected List<String> \u3007\u3007888;
    
    public a(final Context oo08, final b.b.a.a.i.a.m.d.a o\u30070) {
        super(oo08);
        this.\u3007\u3007888 = new ArrayList<String>();
        this.Oo08 = oo08;
        this.o\u30070 = o\u30070;
        if (o\u30070 == null) {
            this.o\u30070 = b.b.a.a.i.a.m.d.a.\u3007080();
        }
    }
    
    public static String Oooo8o0\u3007() {
        final StringBuilder sb = new StringBuilder();
        sb.append("CREATE TABLE IF NOT EXISTS ");
        sb.append(i.\u3007\u30078O0\u30078().\u3007\u3007808\u3007().b());
        sb.append(" (_id INTEGER PRIMARY KEY AUTOINCREMENT,");
        sb.append("id");
        sb.append(" TEXT UNIQUE,");
        sb.append("value");
        sb.append(" TEXT ,");
        sb.append("gen_time");
        sb.append(" TEXT , ");
        sb.append("retry");
        sb.append(" INTEGER default 0 , ");
        sb.append("encrypt");
        sb.append(" INTEGER default 0");
        sb.append(")");
        return sb.toString();
    }
    
    private static String oO80(String s, final String s2) {
        if (TextUtils.isEmpty((CharSequence)s)) {
            s = s2;
        }
        return s;
    }
    
    private static String \u300780\u3007808\u3007O(final String s, final List<?> list, int a, final boolean b) {
        String s2;
        if (b) {
            s2 = " IN ";
        }
        else {
            s2 = " NOT IN ";
        }
        String str;
        if (b) {
            str = " OR ";
        }
        else {
            str = " AND ";
        }
        final int min = Math.min(a, 1000);
        final int size = list.size();
        if (size % min == 0) {
            a = size / min;
        }
        else {
            a = size / min + 1;
        }
        final StringBuilder sb = new StringBuilder();
        for (int i = 0; i < a; ++i) {
            final int n = i * min;
            final String oo80 = oO80(TextUtils.join((CharSequence)"','", (Iterable)list.subList(n, Math.min(n + min, size))), "");
            if (i != 0) {
                sb.append(str);
            }
            sb.append(s);
            sb.append(s2);
            sb.append("('");
            sb.append(oo80);
            sb.append("')");
        }
        final String string = sb.toString();
        final StringBuilder sb2 = new StringBuilder();
        sb2.append(s);
        sb2.append(s2);
        sb2.append("('')");
        return oO80(string, sb2.toString());
    }
    
    public void OO0o\u3007\u3007(final List<b.b.a.a.i.a.m.a> list) {
        if (list != null) {
            if (list.size() != 0) {
                final LinkedList list2 = new LinkedList();
                for (final b.b.a.a.i.a.m.a a : list) {
                    list2.add(a.i());
                    b.b.a.a.i.a.l.a.\u3007O8o08O(a);
                }
                final StringBuilder sb = new StringBuilder();
                sb.append(this.Oo08());
                sb.append(" adevent repo delete: ");
                sb.append(list2.size());
                b.b.a.a.i.a.l.c.\u3007o00\u3007\u3007Oo("PADLT", sb.toString());
                final StringBuilder sb2 = new StringBuilder();
                sb2.append("DELETE FROM ");
                sb2.append(this.Oo08());
                sb2.append(" WHERE ");
                sb2.append(\u300780\u3007808\u3007O("id", list2, 1000, true));
                b.b.a.a.i.a.j.g.c.O8(this.\u3007080(), sb2.toString());
                this.\u3007o\u3007(list2);
            }
        }
    }
    
    public List<b.b.a.a.i.a.m.a> OO0o\u3007\u3007\u3007\u30070(final int p0, final String p1) {
        // 
        // This method could not be decompiled.
        // 
        // Original Bytecode:
        // 
        //     1: aload_0        
        //     2: invokevirtual   b/b/a/a/i/a/j/g/h/c.\u3007080:()Landroid/content/Context;
        //     5: invokestatic    b/b/a/a/i/a/k/a.\u3007080:(ILandroid/content/Context;)J
        //     8: lstore          4
        //    10: new             Ljava/lang/StringBuilder;
        //    13: dup            
        //    14: invokespecial   java/lang/StringBuilder.<init>:()V
        //    17: astore          6
        //    19: aload           6
        //    21: ldc             ""
        //    23: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //    26: pop            
        //    27: aload           6
        //    29: aload_0        
        //    30: invokevirtual   b/b/a/a/i/a/j/g/h/a.Oo08:()Ljava/lang/String;
        //    33: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //    36: pop            
        //    37: aload           6
        //    39: ldc             " query db max :"
        //    41: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //    44: pop            
        //    45: aload           6
        //    47: lload           4
        //    49: invokevirtual   java/lang/StringBuilder.append:(J)Ljava/lang/StringBuilder;
        //    52: pop            
        //    53: aload           6
        //    55: ldc             " limit:"
        //    57: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //    60: pop            
        //    61: aload           6
        //    63: iload_1        
        //    64: invokevirtual   java/lang/StringBuilder.append:(I)Ljava/lang/StringBuilder;
        //    67: pop            
        //    68: aload           6
        //    70: invokevirtual   java/lang/StringBuilder.toString:()Ljava/lang/String;
        //    73: invokestatic    b/b/a/a/i/a/l/c.\u3007\u3007888:(Ljava/lang/String;)V
        //    76: lload           4
        //    78: lconst_0       
        //    79: lcmp           
        //    80: ifle            131
        //    83: aload_2        
        //    84: invokestatic    android/text/TextUtils.isEmpty:(Ljava/lang/CharSequence;)Z
        //    87: ifne            131
        //    90: new             Ljava/lang/StringBuilder;
        //    93: dup            
        //    94: invokespecial   java/lang/StringBuilder.<init>:()V
        //    97: astore          6
        //    99: aload           6
        //   101: aload_2        
        //   102: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   105: pop            
        //   106: aload           6
        //   108: ldc             " DESC limit "
        //   110: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   113: pop            
        //   114: aload           6
        //   116: lload           4
        //   118: invokevirtual   java/lang/StringBuilder.append:(J)Ljava/lang/StringBuilder;
        //   121: pop            
        //   122: aload           6
        //   124: invokevirtual   java/lang/StringBuilder.toString:()Ljava/lang/String;
        //   127: astore_2       
        //   128: goto            133
        //   131: aconst_null    
        //   132: astore_2       
        //   133: new             Ljava/util/LinkedList;
        //   136: dup            
        //   137: invokespecial   java/util/LinkedList.<init>:()V
        //   140: astore          7
        //   142: aload_0        
        //   143: getfield        b/b/a/a/i/a/j/g/h/a.\u3007\u3007888:Ljava/util/List;
        //   146: invokeinterface java/util/List.clear:()V
        //   151: aload_0        
        //   152: invokevirtual   b/b/a/a/i/a/j/g/h/c.\u3007080:()Landroid/content/Context;
        //   155: aload_0        
        //   156: invokevirtual   b/b/a/a/i/a/j/g/h/a.Oo08:()Ljava/lang/String;
        //   159: iconst_3       
        //   160: anewarray       Ljava/lang/String;
        //   163: dup            
        //   164: iconst_0       
        //   165: ldc             "id"
        //   167: aastore        
        //   168: dup            
        //   169: iconst_1       
        //   170: ldc             "value"
        //   172: aastore        
        //   173: dup            
        //   174: iconst_2       
        //   175: ldc             "encrypt"
        //   177: aastore        
        //   178: aconst_null    
        //   179: aconst_null    
        //   180: aconst_null    
        //   181: aconst_null    
        //   182: aload_2        
        //   183: invokestatic    b/b/a/a/i/a/j/g/c.\u3007o\u3007:(Landroid/content/Context;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
        //   186: astore          8
        //   188: aload           8
        //   190: ifnull          456
        //   193: invokestatic    b/b/a/a/i/a/i.\u3007\u30078O0\u30078:()Lb/b/a/a/i/a/i;
        //   196: invokevirtual   b/b/a/a/i/a/i.\u3007080:()Lb/b/a/a/i/a/f;
        //   199: astore          9
        //   201: aload           8
        //   203: invokeinterface android/database/Cursor.moveToNext:()Z
        //   208: istore_3       
        //   209: iload_3        
        //   210: ifeq            378
        //   213: aload           8
        //   215: aload           8
        //   217: ldc             "id"
        //   219: invokeinterface android/database/Cursor.getColumnIndex:(Ljava/lang/String;)I
        //   224: invokeinterface android/database/Cursor.getString:(I)Ljava/lang/String;
        //   229: astore          10
        //   231: aload           8
        //   233: aload           8
        //   235: ldc             "value"
        //   237: invokeinterface android/database/Cursor.getColumnIndex:(Ljava/lang/String;)I
        //   242: invokeinterface android/database/Cursor.getString:(I)Ljava/lang/String;
        //   247: astore          6
        //   249: aload           6
        //   251: astore_2       
        //   252: aload           8
        //   254: aload           8
        //   256: ldc             "encrypt"
        //   258: invokeinterface android/database/Cursor.getColumnIndex:(Ljava/lang/String;)I
        //   263: invokeinterface android/database/Cursor.getInt:(I)I
        //   268: iconst_1       
        //   269: if_icmpne       282
        //   272: aload           9
        //   274: aload           6
        //   276: invokeinterface b/b/a/a/i/a/f.a:(Ljava/lang/String;)Ljava/lang/String;
        //   281: astore_2       
        //   282: aload_2        
        //   283: invokestatic    android/text/TextUtils.isEmpty:(Ljava/lang/CharSequence;)Z
        //   286: ifeq            310
        //   289: ldc_w           "log_show_query : value is null"
        //   292: invokestatic    b/b/a/a/i/a/l/c.\u3007080:(Ljava/lang/String;)V
        //   295: aload_0        
        //   296: getfield        b/b/a/a/i/a/j/g/h/a.\u3007\u3007888:Ljava/util/List;
        //   299: aload           10
        //   301: invokeinterface java/util/List.add:(Ljava/lang/Object;)Z
        //   306: pop            
        //   307: goto            201
        //   310: new             Lorg/json/JSONObject;
        //   313: astore          6
        //   315: aload           6
        //   317: aload_2        
        //   318: invokespecial   org/json/JSONObject.<init>:(Ljava/lang/String;)V
        //   321: new             Lb/b/a/a/i/a/m/c/a;
        //   324: astore_2       
        //   325: aload_2        
        //   326: aload           10
        //   328: aload           6
        //   330: invokespecial   b/b/a/a/i/a/m/c/a.<init>:(Ljava/lang/String;Lorg/json/JSONObject;)V
        //   333: aload_2        
        //   334: aload_0        
        //   335: invokevirtual   b/b/a/a/i/a/j/g/h/a.\u3007O\u3007:()B
        //   338: invokevirtual   b/b/a/a/i/a/m/c/a.\u3007080:(B)V
        //   341: aload_2        
        //   342: aload_0        
        //   343: invokevirtual   b/b/a/a/i/a/j/g/h/a.\u3007O00:()B
        //   346: invokevirtual   b/b/a/a/i/a/m/c/a.\u3007o00\u3007\u3007Oo:(B)V
        //   349: aload           6
        //   351: aload_2        
        //   352: invokestatic    b/b/a/a/i/a/l/a.\u300780\u3007808\u3007O:(Lorg/json/JSONObject;Lb/b/a/a/i/a/m/c/a;)V
        //   355: aload           7
        //   357: aload_2        
        //   358: invokeinterface java/util/List.add:(Ljava/lang/Object;)Z
        //   363: pop            
        //   364: goto            201
        //   367: astore_2       
        //   368: aload_2        
        //   369: invokevirtual   java/lang/Throwable.getMessage:()Ljava/lang/String;
        //   372: invokestatic    b/b/a/a/i/a/l/c.\u3007o\u3007:(Ljava/lang/String;)V
        //   375: goto            201
        //   378: aload           8
        //   380: invokeinterface android/database/Cursor.close:()V
        //   385: aload_0        
        //   386: getfield        b/b/a/a/i/a/j/g/h/a.\u3007\u3007888:Ljava/util/List;
        //   389: invokeinterface java/util/List.isEmpty:()Z
        //   394: ifne            456
        //   397: aload_0        
        //   398: aload_0        
        //   399: getfield        b/b/a/a/i/a/j/g/h/a.\u3007\u3007888:Ljava/util/List;
        //   402: invokevirtual   b/b/a/a/i/a/j/g/h/a.\u3007\u3007808\u3007:(Ljava/util/List;)V
        //   405: aload_0        
        //   406: getfield        b/b/a/a/i/a/j/g/h/a.\u3007\u3007888:Ljava/util/List;
        //   409: invokeinterface java/util/List.clear:()V
        //   414: goto            456
        //   417: astore_2       
        //   418: aload           8
        //   420: invokeinterface android/database/Cursor.close:()V
        //   425: aload_0        
        //   426: getfield        b/b/a/a/i/a/j/g/h/a.\u3007\u3007888:Ljava/util/List;
        //   429: invokeinterface java/util/List.isEmpty:()Z
        //   434: ifne            454
        //   437: aload_0        
        //   438: aload_0        
        //   439: getfield        b/b/a/a/i/a/j/g/h/a.\u3007\u3007888:Ljava/util/List;
        //   442: invokevirtual   b/b/a/a/i/a/j/g/h/a.\u3007\u3007808\u3007:(Ljava/util/List;)V
        //   445: aload_0        
        //   446: getfield        b/b/a/a/i/a/j/g/h/a.\u3007\u3007888:Ljava/util/List;
        //   449: invokeinterface java/util/List.clear:()V
        //   454: aload_2        
        //   455: athrow         
        //   456: new             Ljava/lang/StringBuilder;
        //   459: dup            
        //   460: invokespecial   java/lang/StringBuilder.<init>:()V
        //   463: astore_2       
        //   464: aload_2        
        //   465: ldc             ""
        //   467: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   470: pop            
        //   471: aload_2        
        //   472: aload_0        
        //   473: invokevirtual   b/b/a/a/i/a/j/g/h/a.Oo08:()Ljava/lang/String;
        //   476: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   479: pop            
        //   480: aload_2        
        //   481: ldc_w           " query db actually size :"
        //   484: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   487: pop            
        //   488: aload_2        
        //   489: aload           7
        //   491: invokeinterface java/util/List.size:()I
        //   496: invokevirtual   java/lang/StringBuilder.append:(I)Ljava/lang/StringBuilder;
        //   499: pop            
        //   500: aload_2        
        //   501: invokevirtual   java/lang/StringBuilder.toString:()Ljava/lang/String;
        //   504: invokestatic    b/b/a/a/i/a/l/c.\u3007\u3007888:(Ljava/lang/String;)V
        //   507: aload           7
        //   509: areturn        
        //   510: astore_2       
        //   511: goto            456
        //   514: astore          6
        //   516: goto            454
        //    Signature:
        //  (ILjava/lang/String;)Ljava/util/List<Lb/b/a/a/i/a/m/a;>;
        //    Exceptions:
        //  Try           Handler
        //  Start  End    Start  End    Type                 
        //  -----  -----  -----  -----  ---------------------
        //  193    201    417    456    Any
        //  201    209    417    456    Any
        //  213    249    367    378    Any
        //  252    282    367    378    Any
        //  282    307    367    378    Any
        //  310    364    367    378    Any
        //  368    375    417    456    Any
        //  378    414    510    514    Ljava/lang/Exception;
        //  418    454    514    519    Ljava/lang/Exception;
        // 
        // The error that occurred was:
        // 
        // java.lang.IndexOutOfBoundsException: Index 237 out of bounds for length 237
        //     at java.base/jdk.internal.util.Preconditions.outOfBounds(Preconditions.java:64)
        //     at java.base/jdk.internal.util.Preconditions.outOfBoundsCheckIndex(Preconditions.java:70)
        //     at java.base/jdk.internal.util.Preconditions.checkIndex(Preconditions.java:266)
        //     at java.base/java.util.Objects.checkIndex(Objects.java:361)
        //     at java.base/java.util.ArrayList.get(ArrayList.java:427)
        //     at com.strobel.decompiler.ast.AstBuilder.convertToAst(AstBuilder.java:3362)
        //     at com.strobel.decompiler.ast.AstBuilder.build(AstBuilder.java:112)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:203)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:93)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethodBody(AstBuilder.java:868)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethod(AstBuilder.java:761)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addTypeMembers(AstBuilder.java:638)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeCore(AstBuilder.java:605)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeNoCache(AstBuilder.java:195)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createType(AstBuilder.java:162)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addType(AstBuilder.java:137)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.buildAst(JavaLanguage.java:71)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.decompileType(JavaLanguage.java:59)
        //     at com.strobel.decompiler.DecompilerDriver.decompileType(DecompilerDriver.java:333)
        //     at com.strobel.decompiler.DecompilerDriver.decompileJar(DecompilerDriver.java:254)
        //     at com.strobel.decompiler.DecompilerDriver.main(DecompilerDriver.java:144)
        // 
        throw new IllegalStateException("An error occurred while decompiling this method.");
    }
    
    @Override
    public String Oo08() {
        final e \u3007\u3007808\u3007 = i.\u3007\u30078O0\u30078().\u3007\u3007808\u3007();
        if (\u3007\u3007808\u3007 != null) {
            return \u3007\u3007808\u3007.b();
        }
        return null;
    }
    
    public List<b.b.a.a.i.a.m.a> \u30078o8o\u3007(final String s) {
        final b.b.a.a.i.a.m.d.a o\u30070 = this.o\u30070;
        if (o\u30070 == null) {
            return new ArrayList<b.b.a.a.i.a.m.a>();
        }
        return this.OO0o\u3007\u3007\u3007\u30070(o\u30070.O8(), s);
    }
    
    public byte \u3007O00() {
        return 2;
    }
    
    public boolean \u3007O8o08O(final int i) {
        final b.b.a.a.i.a.m.d.a o\u30070 = this.o\u30070;
        boolean b = false;
        final boolean b2 = false;
        if (o\u30070 == null) {
            return false;
        }
        final int \u3007\u30078O0\u30078 = this.\u3007\u30078O0\u30078();
        final int oo08 = this.o\u30070.Oo08();
        final StringBuilder sb = new StringBuilder();
        sb.append("");
        sb.append(this.Oo08());
        sb.append(" check dbCount:");
        sb.append(\u3007\u30078O0\u30078);
        sb.append(" MaxCacheCount:");
        sb.append(oo08);
        sb.append(" message:");
        sb.append(i);
        b.b.a.a.i.a.l.c.\u3007\u3007888(sb.toString());
        if (b.b.a.a.i.a.l.a.OoO8() && (i == 1 || i == 2)) {
            boolean b3 = b2;
            if (\u3007\u30078O0\u30078 >= 1) {
                b3 = true;
            }
            return b3;
        }
        if (\u3007\u30078O0\u30078 >= oo08) {
            b = true;
        }
        return b;
    }
    
    public byte \u3007O\u3007() {
        return 0;
    }
    
    protected void \u3007\u3007808\u3007(final List<String> list) {
        final StringBuilder sb = new StringBuilder();
        sb.append(this.Oo08());
        sb.append(" adevent repo delete: ");
        sb.append(list.size());
        b.b.a.a.i.a.l.c.\u3007o00\u3007\u3007Oo("PADLT", sb.toString());
        final StringBuilder sb2 = new StringBuilder();
        sb2.append("DELETE FROM ");
        sb2.append(this.Oo08());
        sb2.append(" WHERE ");
        sb2.append(\u300780\u3007808\u3007O("id", list, 1000, true));
        b.b.a.a.i.a.j.g.c.O8(this.\u3007080(), sb2.toString());
        b.\u3007080(d.oO80.\u3007oOO8O8(), list.size());
        this.\u3007o\u3007(list);
    }
    
    public int \u3007\u30078O0\u30078() {
        // 
        // This method could not be decompiled.
        // 
        // Original Bytecode:
        // 
        //     1: astore          6
        //     3: aconst_null    
        //     4: astore          4
        //     6: iconst_0       
        //     7: istore_3       
        //     8: iconst_0       
        //     9: istore_2       
        //    10: iconst_0       
        //    11: istore_1       
        //    12: aload_0        
        //    13: invokevirtual   b/b/a/a/i/a/j/g/h/c.\u3007080:()Landroid/content/Context;
        //    16: aload_0        
        //    17: invokevirtual   b/b/a/a/i/a/j/g/h/a.Oo08:()Ljava/lang/String;
        //    20: iconst_1       
        //    21: anewarray       Ljava/lang/String;
        //    24: dup            
        //    25: iconst_0       
        //    26: ldc_w           "count(1)"
        //    29: aastore        
        //    30: aconst_null    
        //    31: aconst_null    
        //    32: aconst_null    
        //    33: aconst_null    
        //    34: aconst_null    
        //    35: invokestatic    b/b/a/a/i/a/j/g/c.\u3007o\u3007:(Landroid/content/Context;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
        //    38: astore          5
        //    40: aload           5
        //    42: ifnull          86
        //    45: aload           5
        //    47: astore          4
        //    49: aload           5
        //    51: astore          6
        //    53: aload           5
        //    55: invokeinterface android/database/Cursor.moveToFirst:()Z
        //    60: pop            
        //    61: aload           5
        //    63: astore          4
        //    65: aload           5
        //    67: astore          6
        //    69: aload           5
        //    71: iconst_0       
        //    72: invokeinterface android/database/Cursor.getInt:(I)I
        //    77: istore_1       
        //    78: goto            86
        //    81: astore          5
        //    83: goto            117
        //    86: iload_1        
        //    87: istore_2       
        //    88: aload           5
        //    90: ifnull          133
        //    93: aload           5
        //    95: astore          4
        //    97: goto            124
        //   100: astore          4
        //   102: aload           6
        //   104: ifnull          114
        //   107: aload           6
        //   109: invokeinterface android/database/Cursor.close:()V
        //   114: aload           4
        //   116: athrow         
        //   117: aload           4
        //   119: ifnull          133
        //   122: iload_3        
        //   123: istore_1       
        //   124: aload           4
        //   126: invokeinterface android/database/Cursor.close:()V
        //   131: iload_1        
        //   132: istore_2       
        //   133: iload_2        
        //   134: ireturn        
        //   135: astore          5
        //   137: goto            114
        //   140: astore          4
        //   142: iload_1        
        //   143: istore_2       
        //   144: goto            133
        //    Exceptions:
        //  Try           Handler
        //  Start  End    Start  End    Type                 
        //  -----  -----  -----  -----  ---------------------
        //  12     40     81     124    Ljava/lang/Exception;
        //  12     40     100    117    Any
        //  53     61     81     124    Ljava/lang/Exception;
        //  53     61     100    117    Any
        //  69     78     81     124    Ljava/lang/Exception;
        //  69     78     100    117    Any
        //  107    114    135    140    Ljava/lang/Exception;
        //  124    131    140    147    Ljava/lang/Exception;
        // 
        // The error that occurred was:
        // 
        // java.lang.IllegalStateException: Expression is linked from several locations: Label_0114:
        //     at com.strobel.decompiler.ast.Error.expressionLinkedFromMultipleLocations(Error.java:27)
        //     at com.strobel.decompiler.ast.AstOptimizer.mergeDisparateObjectInitializations(AstOptimizer.java:2604)
        //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:235)
        //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:42)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:206)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:93)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethodBody(AstBuilder.java:868)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethod(AstBuilder.java:761)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addTypeMembers(AstBuilder.java:638)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeCore(AstBuilder.java:605)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeNoCache(AstBuilder.java:195)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createType(AstBuilder.java:162)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addType(AstBuilder.java:137)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.buildAst(JavaLanguage.java:71)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.decompileType(JavaLanguage.java:59)
        //     at com.strobel.decompiler.DecompilerDriver.decompileType(DecompilerDriver.java:333)
        //     at com.strobel.decompiler.DecompilerDriver.decompileJar(DecompilerDriver.java:254)
        //     at com.strobel.decompiler.DecompilerDriver.main(DecompilerDriver.java:144)
        // 
        throw new IllegalStateException("An error occurred while decompiling this method.");
    }
}

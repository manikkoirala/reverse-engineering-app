// 
// Decompiled by Procyon v0.6.0
// 

package b.b.a.a.d.d.i;

import java.util.Iterator;
import java.util.ArrayList;
import java.util.List;

public class i
{
    public static float \u3007080(final float n) {
        return (float)Math.ceil(n * 16.0f / 16.0f);
    }
    
    public static List<a.a> \u3007o00\u3007\u3007Oo(float n, final List<a.a> list) {
        final ArrayList list2 = new ArrayList();
        final Iterator<a.a> iterator = list.iterator();
        while (iterator.hasNext()) {
            list2.add(iterator.next().clone());
        }
        final Iterator iterator2 = list2.iterator();
        final int n2 = 0;
        boolean b = true;
        int n3 = 0;
        int n4 = 0;
        while (iterator2.hasNext()) {
            final a.a a = (a.a)iterator2.next();
            if (a.\u3007OOo8\u30070) {
                n3 += (int)a.o0;
            }
            else {
                n4 += (int)a.o0;
                b = false;
            }
        }
        if (b && n > n3) {
            return list2;
        }
        final float n5 = (float)n3;
        final float n6 = fcmpg(n, n5);
        float n7;
        if (n6 < 0) {
            n7 = n / n5;
        }
        else {
            n7 = 1.0f;
        }
        final float n8 = fcmpl(n, n5);
        float n9;
        if (n8 > 0) {
            n9 = (n - n5) / n4;
        }
        else {
            n9 = 0.0f;
        }
        if (n9 > 1.0f) {
            final ArrayList list3 = new ArrayList();
            final Iterator iterator3 = list2.iterator();
            int n10 = 0;
            while (iterator3.hasNext()) {
                final a.a a2 = (a.a)iterator3.next();
                int n11 = n10;
                if (!a2.\u3007OOo8\u30070) {
                    final float oo = a2.OO;
                    n11 = n10;
                    if (oo != 0.0f) {
                        n11 = n10;
                        if (a2.o0 * n9 > oo) {
                            a2.o0 = oo;
                            a2.\u3007OOo8\u30070 = true;
                            n11 = 1;
                        }
                    }
                }
                list3.add(a2);
                n10 = n11;
            }
            if (n10 != 0) {
                return \u3007o00\u3007\u3007Oo(n, list3);
            }
        }
        final Iterator iterator4 = list2.iterator();
        int n12 = 0;
        while (iterator4.hasNext()) {
            final a.a a3 = (a.a)iterator4.next();
            if (a3.\u3007OOo8\u30070) {
                a3.o0 = \u3007080(a3.o0 * n7);
            }
            else {
                a3.o0 = \u3007080(a3.o0 * n9);
            }
            n12 += (int)a3.o0;
        }
        final float n13 = (float)n12;
        if (n13 < n) {
            float n14 = n - n13;
            for (int n15 = n2; n15 < list2.size() && n14 > 0.0f; n15 = (n15 + 1) % list2.size(), n14 = n) {
                final a.a a4 = (a.a)list2.get(n15);
                if (n6 >= 0 || !a4.\u3007OOo8\u30070) {
                    n = n14;
                    if (n8 <= 0) {
                        continue;
                    }
                    n = n14;
                    if (a4.\u3007OOo8\u30070) {
                        continue;
                    }
                }
                a4.o0 += 0.0625f;
                n = n14 - 0.0625f;
            }
        }
        return list2;
    }
}

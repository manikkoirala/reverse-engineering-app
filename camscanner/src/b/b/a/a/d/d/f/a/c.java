// 
// Decompiled by Procyon v0.6.0
// 

package b.b.a.a.d.d.f.a;

import android.view.ViewGroup;
import b.b.a.a.d.d.h.a;
import android.view.View;

public class c
{
    private static volatile c \u3007080;
    
    private c() {
    }
    
    public static c \u3007080() {
        if (c.\u3007080 == null) {
            synchronized (c.class) {
                if (c.\u3007080 == null) {
                    c.\u3007080 = new c();
                }
            }
        }
        return c.\u3007080;
    }
    
    public d \u3007o00\u3007\u3007Oo(final View view, final a a) {
        d d = null;
        if (a == null) {
            return null;
        }
        if (view.getParent() != null) {
            ((ViewGroup)view.getParent()).setClipChildren(false);
        }
        if (view.getParent().getParent() != null) {
            ((ViewGroup)view.getParent().getParent()).setClipChildren(false);
        }
        if ("scale".equals(a.o8())) {
            return new j(view, a);
        }
        if ("translate".equals(a.o8())) {
            return new n(view, a);
        }
        if ("ripple".equals(a.o8())) {
            return new g(view, a);
        }
        if ("marquee".equals(a.o8())) {
            return new f(view, a);
        }
        if ("waggle".equals(a.o8())) {
            return new o(view, a);
        }
        if ("shine".equals(a.o8())) {
            return new k(view, a);
        }
        if ("swing".equals(a.o8())) {
            return new m(view, a);
        }
        if ("fade".equals(a.o8())) {
            return new b.b.a.a.d.d.f.a.a(view, a);
        }
        if ("rubIn".equals(a.o8())) {
            return new i(view, a);
        }
        if ("rotate".equals(a.o8())) {
            return new h(view, a);
        }
        if ("cutIn".equals(a.o8())) {
            return new e(view, a);
        }
        if ("stretch".equals(a.o8())) {
            d = new l(view, a);
        }
        return d;
    }
}

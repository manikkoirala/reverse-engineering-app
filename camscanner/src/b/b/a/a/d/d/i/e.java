// 
// Decompiled by Procyon v0.6.0
// 

package b.b.a.a.d.d.i;

import org.json.JSONArray;
import java.util.ArrayList;
import java.util.List;
import b.b.a.a.d.f.m;
import b.b.a.a.d.d.h.f;
import org.json.JSONException;
import b.b.a.a.d.d.h.h;
import android.text.TextUtils;
import b.b.a.a.d.d.h.c;
import org.json.JSONObject;
import b.b.a.a.d.d.h.d;
import java.util.HashMap;

public class e
{
    private static HashMap<String, String> \u3007\u3007888;
    private a O8;
    private b Oo08;
    private d o\u30070;
    private JSONObject \u3007080;
    private JSONObject \u3007o00\u3007\u3007Oo;
    private c \u3007o\u3007;
    
    static {
        (e.\u3007\u3007888 = new HashMap<String, String>()).put("subtitle", "description");
        e.\u3007\u3007888.put("source", "source|app.app_name");
        e.\u3007\u3007888.put("screenshot", "dynamic_creative.screenshot");
    }
    
    public e(final JSONObject \u3007080, final JSONObject \u3007o00\u3007\u3007Oo, final JSONObject jsonObject, final JSONObject jsonObject2) {
        this.\u3007080 = \u3007080;
        this.\u3007o00\u3007\u3007Oo = \u3007o00\u3007\u3007Oo;
        this.\u3007o\u3007 = new c(\u3007o00\u3007\u3007Oo);
        this.O8 = a.\u3007080(jsonObject);
        this.o\u30070 = d.\u3007080(jsonObject2);
    }
    
    private String O8() {
        final c \u3007o\u3007 = this.\u3007o\u3007;
        if (\u3007o\u3007 == null) {
            return "";
        }
        return String.valueOf(\u3007o\u3007.\u3007080("adx_name"));
    }
    
    private String Oo08(final String s) {
        if (TextUtils.isEmpty((CharSequence)s)) {
            return "";
        }
        for (final String s2 : s.split("\\|")) {
            if (this.\u3007o\u3007.\u3007o\u3007(s2)) {
                final String value = String.valueOf(this.\u3007o\u3007.\u3007080(s2));
                if (!TextUtils.isEmpty((CharSequence)value)) {
                    return value;
                }
            }
        }
        return "";
    }
    
    private void oO80(final h h) {
        if (h == null) {
            return;
        }
        final int \u3007\u3007888 = b.b.a.a.d.g.d.\u3007\u3007888(b.b.a.a.d.c.\u3007080(), (float)b.b.a.a.d.g.d.oO80(b.b.a.a.d.c.\u3007080()));
        final a o8 = this.O8;
        float n;
        if (o8.\u3007o\u3007) {
            n = o8.\u3007080;
        }
        else {
            n = Math.min(o8.\u3007080, (float)\u3007\u3007888);
        }
        if (this.O8.\u3007o00\u3007\u3007Oo == 0.0f) {
            h.\u30070\u3007O0088o(n);
            h.\u30070000OOO().\u3007O8o08O().O\u30078oOo8O("auto");
            h.\u3007o\u3007(0.0f);
        }
        else {
            h.\u30070\u3007O0088o(n);
            final int \u3007\u3007889 = b.b.a.a.d.g.d.\u3007\u3007888(b.b.a.a.d.c.\u3007080(), (float)b.b.a.a.d.g.d.o\u30070(b.b.a.a.d.c.\u3007080()));
            final a o9 = this.O8;
            float n2;
            if (o9.\u3007o\u3007) {
                n2 = o9.\u3007o00\u3007\u3007Oo;
            }
            else {
                n2 = Math.min(o9.\u3007o00\u3007\u3007Oo, (float)\u3007\u3007889);
            }
            h.\u3007o\u3007(n2);
            h.\u30070000OOO().\u3007O8o08O().O\u30078oOo8O("fixed");
        }
    }
    
    private void o\u30070(final b.b.a.a.d.d.h.e e, int lastIndex) {
        if (lastIndex != 5 && lastIndex != 15 && lastIndex != 50 && lastIndex != 154) {
            e.oO80("image");
            final String oo80 = b.b.a.a.d.d.i.h.oO80("image");
            e.\u3007O8o08O().\u3007o\u30078(oo80);
            e.\u300780\u3007808\u3007O().\u3007o\u30078(oo80);
            final String \u3007o00\u3007\u3007Oo = b.b.a.a.d.d.i.h.\u3007o00\u3007\u3007Oo("image", "clickArea");
            if (!TextUtils.isEmpty((CharSequence)\u3007o00\u3007\u3007Oo)) {
                e.\u3007O8o08O().O8O\u300788oO0(\u3007o00\u3007\u3007Oo);
                e.\u300780\u3007808\u3007O().O8O\u300788oO0(\u3007o00\u3007\u3007Oo);
            }
            e.\u3007o\u3007(oo80);
            if (oo80 != null) {
                lastIndex = oo80.lastIndexOf(".");
                if (lastIndex > 0) {
                    final String substring = oo80.substring(0, lastIndex);
                    final JSONObject jsonObject = new JSONObject();
                    try {
                        final StringBuilder sb = new StringBuilder();
                        sb.append(substring);
                        sb.append(".width");
                        jsonObject.put("width", (Object)this.Oo08(sb.toString()));
                        final StringBuilder sb2 = new StringBuilder();
                        sb2.append(substring);
                        sb2.append(".height");
                        jsonObject.put("height", (Object)this.Oo08(sb2.toString()));
                    }
                    catch (final JSONException ex) {
                        ((Throwable)ex).printStackTrace();
                    }
                    e.o\u30070(jsonObject.toString());
                }
            }
            e.\u3007O8o08O().\u3007O8o08O();
        }
        else {
            e.oO80("video");
            final String oo81 = b.b.a.a.d.d.i.h.oO80("video");
            e.\u3007O8o08O().\u3007o\u30078(oo81);
            final String \u3007o00\u3007\u3007Oo2 = b.b.a.a.d.d.i.h.\u3007o00\u3007\u3007Oo("video", "clickArea");
            if (!TextUtils.isEmpty((CharSequence)\u3007o00\u3007\u3007Oo2)) {
                e.\u3007O8o08O().O8O\u300788oO0(\u3007o00\u3007\u3007Oo2);
                e.\u300780\u3007808\u3007O().O8O\u300788oO0(\u3007o00\u3007\u3007Oo2);
            }
            e.\u300780\u3007808\u3007O().\u3007o\u30078(oo81);
            e.\u3007o\u3007(oo81);
            e.\u3007O8o08O().Oooo8o0\u3007();
        }
    }
    
    private void \u3007\u3007888(final f f) {
        if (f == null) {
            return;
        }
        String ooO8;
        final String s = ooO8 = f.OoO8();
        if (b.b.a.a.d.c.\u3007o\u3007()) {
            String oo08;
            if ("zh".equals(oo08 = b.b.a.a.d.g.d.Oo08(b.b.a.a.d.c.\u3007080()))) {
                oo08 = "cn";
            }
            ooO8 = s;
            if (!TextUtils.isEmpty((CharSequence)oo08)) {
                ooO8 = s;
                if (f.\u3007O00() != null) {
                    final String optString = f.\u3007O00().optString(oo08);
                    ooO8 = s;
                    if (!TextUtils.isEmpty((CharSequence)optString)) {
                        ooO8 = optString;
                    }
                }
            }
        }
        if (TextUtils.isEmpty((CharSequence)ooO8)) {
            return;
        }
        final int index = ooO8.indexOf("{{");
        final int index2 = ooO8.indexOf("}}");
        if (index >= 0 && index2 >= 0 && index2 >= index) {
            final String oo9 = this.Oo08(ooO8.substring(index + 2, index2));
            final StringBuilder sb = new StringBuilder(ooO8.substring(0, index));
            if (!TextUtils.isEmpty((CharSequence)oo9)) {
                sb.append(oo9);
            }
            sb.append(ooO8.substring(index2 + 2));
            f.oo0O\u30070\u3007\u3007\u3007(sb.toString());
            return;
        }
        f.oo0O\u30070\u3007\u3007\u3007(ooO8);
    }
    
    public h \u3007080(final double n, final int n2, final double n3, final String s, final m m) {
        this.\u3007o\u3007.\u3007o00\u3007\u3007Oo();
        JSONObject jsonObject;
        try {
            jsonObject = new JSONObject(this.o\u30070.\u3007o00\u3007\u3007Oo);
        }
        catch (final JSONException ex) {
            ((Throwable)ex).printStackTrace();
            jsonObject = null;
        }
        final h \u3007o\u3007 = this.\u3007o\u3007(b.b.a.a.d.d.i.c.\u3007o00\u3007\u3007Oo(this.\u3007080, jsonObject), null);
        this.oO80(\u3007o\u3007);
        final b.b.a.a.d.d.i.d d = new b.b.a.a.d.d.i.d(n, n2, n3, s, m);
        final b.b.a.a.d.d.i.d.a a = new b.b.a.a.d.d.i.d.a();
        final a o8 = this.O8;
        a.\u3007080 = o8.\u3007080;
        a.\u3007o00\u3007\u3007Oo = o8.\u3007o00\u3007\u3007Oo;
        d.\u3007\u3007888(a);
        d.o\u30070(\u3007o\u3007, 0.0f, 0.0f);
        d.O8();
        final b.b.a.a.d.d.h.b \u3007o00\u3007\u3007Oo = d.\u3007o00\u3007\u3007Oo;
        if (\u3007o00\u3007\u3007Oo.O8 == 65536.0f) {
            return null;
        }
        return \u3007o00\u3007\u3007Oo.o\u30070;
    }
    
    public h \u3007o00\u3007\u3007Oo(JSONObject jsonObject) {
        final String optString = jsonObject.optString("type");
        final String optString2 = jsonObject.optString("id");
        final JSONObject optJSONObject = jsonObject.optJSONObject("values");
        b.b.a.a.d.d.i.h.o\u30070(optString, optJSONObject);
        jsonObject = b.b.a.a.d.d.i.h.\u3007o\u3007(optString, b.b.a.a.d.d.i.h.O8(jsonObject.optJSONArray("sceneValues")), optJSONObject);
        final h h = new h();
        if (TextUtils.isEmpty((CharSequence)optString2)) {
            h.\u30078o8o\u3007(String.valueOf(h.hashCode()));
        }
        else {
            h.\u30078o8o\u3007(optString2);
        }
        if (optJSONObject != null) {
            h.o800o8O((float)optJSONObject.optDouble("x"));
            h.oo88o8O((float)optJSONObject.optDouble("y"));
            h.\u30070\u3007O0088o((float)optJSONObject.optDouble("width"));
            h.\u3007o\u3007((float)optJSONObject.optDouble("height"));
            h.OO0o\u3007\u3007\u3007\u30070((float)optJSONObject.optInt("remainWidth"));
            final b.b.a.a.d.d.h.e e = new b.b.a.a.d.d.h.e();
            e.oO80(optString);
            e.\u3007o\u3007(optJSONObject.optString("data"));
            e.o\u30070(optJSONObject.optString("dataExtraInfo"));
            final f \u3007o = f.\u3007o(optJSONObject);
            e.Oo08(\u3007o);
            final f \u3007o2 = f.\u3007o(jsonObject);
            if (\u3007o2 == null) {
                e.\u3007o00\u3007\u3007Oo(\u3007o);
            }
            else {
                e.\u3007o00\u3007\u3007Oo(\u3007o2);
            }
            this.\u3007\u3007888(\u3007o);
            this.\u3007\u3007888(\u3007o2);
            if (TextUtils.equals((CharSequence)optString, (CharSequence)"video-image-budget")) {
                jsonObject = this.\u3007o00\u3007\u3007Oo;
                if (jsonObject != null) {
                    this.o\u30070(e, jsonObject.optInt("image_mode"));
                }
            }
            final String \u30078o8o\u3007 = e.\u30078o8o\u3007();
            final f \u3007o8o08O = e.\u3007O8o08O();
            if (b.b.a.a.d.d.i.e.\u3007\u3007888.containsKey(\u30078o8o\u3007) && !\u3007o8o08O.O88\u3007\u3007o0O()) {
                \u3007o8o08O.\u3007o\u30078(b.b.a.a.d.d.i.e.\u3007\u3007888.get(\u30078o8o\u3007));
            }
            String s;
            if (\u3007o8o08O.O88\u3007\u3007o0O()) {
                s = e.\u3007080();
            }
            else {
                s = this.Oo08(e.\u3007080());
            }
            String oo08 = s;
            if (b.b.a.a.d.c.\u3007o\u3007()) {
                if (TextUtils.equals((CharSequence)\u30078o8o\u3007, (CharSequence)"star") || TextUtils.equals((CharSequence)\u30078o8o\u3007, (CharSequence)"text_star")) {
                    s = this.Oo08("dynamic_creative.score_exact_i18n|");
                }
                if (TextUtils.equals((CharSequence)\u30078o8o\u3007, (CharSequence)"score-count") || TextUtils.equals((CharSequence)\u30078o8o\u3007, (CharSequence)"score-count-type-1") || TextUtils.equals((CharSequence)\u30078o8o\u3007, (CharSequence)"score-count-type-2")) {
                    s = this.Oo08("dynamic_creative.comment_num_i18n|");
                }
                oo08 = s;
                if ("root".equals(\u30078o8o\u3007)) {
                    oo08 = s;
                    if (\u3007o.O\u30070()) {
                        oo08 = this.Oo08("image.0.url");
                    }
                }
            }
            if (!TextUtils.isEmpty((CharSequence)this.O8()) && (TextUtils.equals((CharSequence)"logo-union", (CharSequence)optString) || TextUtils.equals((CharSequence)"logo", (CharSequence)optString))) {
                final StringBuilder sb = new StringBuilder();
                sb.append(oo08);
                sb.append("adx:");
                sb.append(this.O8());
                e.\u3007o\u3007(sb.toString());
            }
            else {
                e.\u3007o\u3007(oo08);
            }
            h.O8(e);
        }
        return h;
    }
    
    public h \u3007o\u3007(final JSONObject jsonObject, final h h) {
        if (jsonObject == null) {
            return null;
        }
        final String optString = jsonObject.optString("type");
        JSONObject jsonObject2 = jsonObject;
        if (TextUtils.equals((CharSequence)optString, (CharSequence)"custom-component-vessel")) {
            final int optInt = jsonObject.optInt("componentId");
            final d o\u30070 = this.o\u30070;
            jsonObject2 = jsonObject;
            if (o\u30070 != null) {
                final b oo08 = new b();
                this.Oo08 = oo08;
                final JSONObject \u3007080 = oo08.\u3007080(o\u30070.\u3007080, optInt, jsonObject);
                jsonObject2 = jsonObject;
                if (\u3007080 != null) {
                    jsonObject2 = \u3007080;
                }
            }
        }
        final h \u3007o00\u3007\u3007Oo = this.\u3007o00\u3007\u3007Oo(jsonObject2);
        \u3007o00\u3007\u3007Oo.Oo08(h);
        final JSONArray optJSONArray = jsonObject2.optJSONArray("children");
        if (optJSONArray == null) {
            \u3007o00\u3007\u3007Oo.\u3007O8o08O(null);
            return \u3007o00\u3007\u3007Oo;
        }
        final ArrayList list = new ArrayList();
        final ArrayList list2 = new ArrayList();
        for (int i = 0; i < optJSONArray.length(); ++i) {
            final JSONArray optJSONArray2 = optJSONArray.optJSONArray(i);
            if (optJSONArray2 != null) {
                final ArrayList list3 = new ArrayList();
                int n;
                if (TextUtils.equals((CharSequence)optString, (CharSequence)"tag-group")) {
                    n = \u3007o00\u3007\u3007Oo.\u30070000OOO().\u3007O8o08O().Oo0oO\u3007O\u3007O();
                }
                else {
                    n = optJSONArray2.length();
                }
                for (int j = 0; j < n; ++j) {
                    final h \u3007o\u3007 = this.\u3007o\u3007(optJSONArray2.optJSONObject(j), \u3007o00\u3007\u3007Oo);
                    if (b.b.a.a.d.c.\u3007o\u3007() && "skip-with-time".equals(\u3007o00\u3007\u3007Oo.\u30070000OOO().\u30078o8o\u3007()) && !"transparent".equals(\u3007o00\u3007\u3007Oo.\u3007oOO8O8()) && !TextUtils.isEmpty((CharSequence)\u3007o00\u3007\u3007Oo.\u3007oOO8O8())) {
                        \u3007o\u3007.\u3007\u3007808\u3007(\u3007o00\u3007\u3007Oo.\u3007oOO8O8());
                    }
                    list.add(\u3007o\u3007);
                    list3.add(\u3007o\u3007);
                }
                list2.add(list3);
            }
        }
        if (list.size() > 0) {
            \u3007o00\u3007\u3007Oo.\u3007O8o08O(list);
        }
        if (list2.size() > 0) {
            \u3007o00\u3007\u3007Oo.\u3007\u3007888(list2);
        }
        return \u3007o00\u3007\u3007Oo;
    }
    
    static class a
    {
        float \u3007080;
        float \u3007o00\u3007\u3007Oo;
        boolean \u3007o\u3007;
        
        public a() {
        }
        
        public static a \u3007080(final JSONObject jsonObject) {
            final a a = new a();
            if (jsonObject != null) {
                a.\u3007080 = (float)jsonObject.optDouble("width");
                a.\u3007o00\u3007\u3007Oo = (float)jsonObject.optDouble("height");
                a.\u3007o\u3007 = jsonObject.optBoolean("isLandscape");
            }
            return a;
        }
    }
}

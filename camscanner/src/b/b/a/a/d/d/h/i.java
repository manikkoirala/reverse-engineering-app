// 
// Decompiled by Procyon v0.6.0
// 

package b.b.a.a.d.d.h;

import java.util.Arrays;

public class i
{
    public float \u3007080;
    public float \u3007o00\u3007\u3007Oo;
    
    public i(final float \u3007080, final float \u3007o00\u3007\u3007Oo) {
        this.\u3007080 = \u3007080;
        this.\u3007o00\u3007\u3007Oo = \u3007o00\u3007\u3007Oo;
    }
    
    @Override
    public boolean equals(final Object o) {
        boolean b = true;
        if (this == o) {
            return true;
        }
        if (o != null && i.class == o.getClass()) {
            final i i = (i)o;
            if (Float.compare(i.\u3007080, this.\u3007080) != 0 || Float.compare(i.\u3007o00\u3007\u3007Oo, this.\u3007o00\u3007\u3007Oo) != 0) {
                b = false;
            }
            return b;
        }
        return false;
    }
    
    @Override
    public int hashCode() {
        return Arrays.hashCode(new Object[] { this.\u3007080, this.\u3007o00\u3007\u3007Oo });
    }
}

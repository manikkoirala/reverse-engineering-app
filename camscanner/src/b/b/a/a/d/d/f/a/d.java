// 
// Decompiled by Procyon v0.6.0
// 

package b.b.a.a.d.d.f.a;

import com.bytedance.sdk.component.adexpress.dynamic.dynamicview.DynamicBaseWidgetImp;
import android.animation.ValueAnimator;
import android.animation.ValueAnimator$AnimatorUpdateListener;
import android.view.animation.LinearInterpolator;
import android.view.animation.DecelerateInterpolator;
import android.view.animation.AccelerateInterpolator;
import android.animation.TimeInterpolator;
import android.view.animation.AccelerateDecelerateInterpolator;
import java.util.Iterator;
import java.util.concurrent.TimeUnit;
import android.animation.Animator;
import android.animation.Animator$AnimatorListener;
import java.util.HashSet;
import b.b.a.a.d.d.h.a;
import android.view.View;
import android.animation.ObjectAnimator;
import java.util.List;
import java.util.concurrent.ScheduledFuture;
import java.util.Set;
import com.bytedance.sdk.component.adexpress.dynamic.dynamicview.e;

public abstract class d implements e
{
    private Set<ScheduledFuture<?>> OO;
    public List<ObjectAnimator> o0;
    public View \u300708O\u300700\u3007o;
    a \u3007OOo8\u30070;
    
    public d(final View \u300708O\u300700\u3007o, final a \u3007oOo8\u30070) {
        this.\u300708O\u300700\u3007o = \u300708O\u300700\u3007o;
        this.\u3007OOo8\u30070 = \u3007oOo8\u30070;
        this.OO = new HashSet<ScheduledFuture<?>>();
        this.o0 = this.\u3007o00\u3007\u3007Oo();
    }
    
    public void O8() {
        final List<ObjectAnimator> o0 = this.o0;
        if (o0 == null) {
            return;
        }
        for (final ObjectAnimator objectAnimator : o0) {
            objectAnimator.start();
            if (this.\u3007OOo8\u30070.O\u30078O8\u3007008() > 0.0) {
                ((Animator)objectAnimator).addListener((Animator$AnimatorListener)new Animator$AnimatorListener(this, objectAnimator) {
                    final ObjectAnimator \u3007080;
                    final d \u3007o00\u3007\u3007Oo;
                    
                    public void onAnimationCancel(final Animator animator) {
                    }
                    
                    public void onAnimationEnd(final Animator animator) {
                    }
                    
                    public void onAnimationRepeat(final Animator animator) {
                        ((Animator)this.\u3007080).pause();
                        final c c = this.\u3007o00\u3007\u3007Oo.new c(this.\u3007080);
                        final ScheduledFuture<?> schedule = b.b.a.a.k.e.\u3007O888o0o().schedule(c, (long)(this.\u3007o00\u3007\u3007Oo.\u3007OOo8\u30070.O\u30078O8\u3007008() * 1000.0), TimeUnit.MILLISECONDS);
                        c.\u3007080(schedule);
                        this.\u3007o00\u3007\u3007Oo.OO.add(schedule);
                    }
                    
                    public void onAnimationStart(final Animator animator) {
                    }
                });
            }
        }
    }
    
    public void b() {
        final List<ObjectAnimator> o0 = this.o0;
        if (o0 == null) {
            return;
        }
        for (final ObjectAnimator objectAnimator : o0) {
            ((Animator)objectAnimator).cancel();
            ((ValueAnimator)objectAnimator).removeAllUpdateListeners();
        }
        final Iterator<ScheduledFuture<?>> iterator2 = this.OO.iterator();
        while (iterator2.hasNext()) {
            iterator2.next().cancel(true);
        }
    }
    
    ObjectAnimator \u3007080(final ObjectAnimator objectAnimator) {
        ((Animator)objectAnimator).setStartDelay((long)(this.\u3007OOo8\u30070.o\u30070() * 1000.0));
        if (this.\u3007OOo8\u30070.\u3007oOO8O8() > 0) {
            ((ValueAnimator)objectAnimator).setRepeatCount(this.\u3007OOo8\u30070.\u3007oOO8O8() - 1);
        }
        else {
            ((ValueAnimator)objectAnimator).setRepeatCount(-1);
        }
        if (!"normal".equals(this.\u3007OOo8\u30070.OO0o\u3007\u3007\u3007\u30070())) {
            if (!"alternate".equals(this.\u3007OOo8\u30070.OO0o\u3007\u3007\u3007\u30070()) && !"alternate-reverse".equals(this.\u3007OOo8\u30070.OO0o\u3007\u3007\u3007\u30070())) {
                ((ValueAnimator)objectAnimator).setRepeatMode(1);
            }
            else {
                ((ValueAnimator)objectAnimator).setRepeatMode(2);
            }
        }
        if ("ease-in-out".equals(this.\u3007OOo8\u30070.\u3007o())) {
            ((Animator)objectAnimator).setInterpolator((TimeInterpolator)new AccelerateDecelerateInterpolator());
        }
        else if ("ease-in".equals(this.\u3007OOo8\u30070.OO0o\u3007\u3007\u3007\u30070())) {
            ((Animator)objectAnimator).setInterpolator((TimeInterpolator)new AccelerateInterpolator());
        }
        else if ("ease-out".equals(this.\u3007OOo8\u30070.OO0o\u3007\u3007\u3007\u30070())) {
            ((Animator)objectAnimator).setInterpolator((TimeInterpolator)new DecelerateInterpolator());
        }
        else {
            ((Animator)objectAnimator).setInterpolator((TimeInterpolator)new LinearInterpolator());
        }
        ((ValueAnimator)objectAnimator).addUpdateListener((ValueAnimator$AnimatorUpdateListener)new ValueAnimator$AnimatorUpdateListener(this, objectAnimator) {
            final ObjectAnimator \u3007080;
            final d \u3007o00\u3007\u3007Oo;
            
            public void onAnimationUpdate(final ValueAnimator valueAnimator) {
                if (valueAnimator.getCurrentPlayTime() > 0L) {
                    this.\u3007o00\u3007\u3007Oo.\u300708O\u300700\u3007o.setVisibility(0);
                    if (this.\u3007o00\u3007\u3007Oo.\u300708O\u300700\u3007o.getParent() instanceof DynamicBaseWidgetImp) {
                        ((View)this.\u3007o00\u3007\u3007Oo.\u300708O\u300700\u3007o.getParent()).setVisibility(0);
                    }
                    ((ValueAnimator)this.\u3007080).removeAllUpdateListeners();
                }
            }
        });
        return objectAnimator;
    }
    
    abstract List<ObjectAnimator> \u3007o00\u3007\u3007Oo();
    
    public class c implements Runnable
    {
        final d OO;
        ObjectAnimator o0;
        ScheduledFuture<?> \u3007OOo8\u30070;
        
        c(final d oo, final ObjectAnimator o0) {
            this.OO = oo;
            this.o0 = o0;
        }
        
        @Override
        public void run() {
            if (b.b.a.a.d.e.a.a.oO80().o\u30070() != null) {
                b.b.a.a.d.e.a.a.oO80().o\u30070().j().post((Runnable)new Runnable(this) {
                    final c o0;
                    
                    @Override
                    public void run() {
                        ((Animator)this.o0.o0).resume();
                    }
                });
                if (this.\u3007OOo8\u30070 != null) {
                    this.OO.OO.remove(this.\u3007OOo8\u30070);
                }
            }
        }
        
        public void \u3007080(final ScheduledFuture<?> \u3007oOo8\u30070) {
            this.\u3007OOo8\u30070 = \u3007oOo8\u30070;
        }
    }
}

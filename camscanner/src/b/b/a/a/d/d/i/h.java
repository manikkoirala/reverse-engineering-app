// 
// Decompiled by Procyon v0.6.0
// 

package b.b.a.a.d.d.i;

import b.b.a.a.d.a;
import java.util.Iterator;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONArray;

public class h
{
    public static JSONObject O8(final JSONArray jsonArray) {
        if (jsonArray == null || jsonArray.length() <= 0) {
            return null;
        }
        final JSONObject optJSONObject = jsonArray.optJSONObject(0);
        if (optJSONObject == null) {
            return null;
        }
        return optJSONObject.optJSONObject("values");
    }
    
    public static JSONObject Oo08(final JSONObject... array) {
        final JSONObject jsonObject = new JSONObject();
        for (final JSONObject jsonObject2 : array) {
            if (jsonObject2 != null) {
                final Iterator keys = jsonObject2.keys();
                while (keys.hasNext()) {
                    final String s = keys.next();
                    try {
                        jsonObject.put(s, jsonObject2.opt(s));
                    }
                    catch (final JSONException ex) {
                        ((Throwable)ex).printStackTrace();
                    }
                }
            }
        }
        return jsonObject;
    }
    
    public static String oO80(final String s) {
        final JSONObject o\u30078O8\u3007008 = a.O\u30078O8\u3007008(s);
        if (o\u30078O8\u3007008 == null) {
            return null;
        }
        final JSONObject optJSONObject = o\u30078O8\u3007008.optJSONObject("values");
        if (optJSONObject == null) {
            return null;
        }
        return optJSONObject.optString("data");
    }
    
    public static void o\u30070(final String s, JSONObject optJSONObject) {
        final JSONObject o\u30078O8\u3007008 = a.O\u30078O8\u3007008(s);
        if (o\u30078O8\u3007008 == null) {
            return;
        }
        JSONObject jsonObject;
        if ((jsonObject = optJSONObject) == null) {
            jsonObject = new JSONObject();
        }
        optJSONObject = o\u30078O8\u3007008.optJSONObject("values");
        if (optJSONObject == null) {
            return;
        }
        \u3007\u3007888(optJSONObject, jsonObject);
    }
    
    public static String \u3007080(final String str) {
        String string = str;
        if (str.indexOf(46) < 0) {
            final StringBuilder sb = new StringBuilder();
            sb.append(str);
            sb.append(".png");
            string = sb.toString();
        }
        final StringBuilder sb2 = new StringBuilder();
        sb2.append("https://sf16-static.i18n-pglstatp.com/obj/ad-pattern-sg/static/images/");
        sb2.append(string);
        return sb2.toString();
    }
    
    public static String \u3007o00\u3007\u3007Oo(final String s, final String s2) {
        final JSONObject o\u30078O8\u3007008 = a.O\u30078O8\u3007008(s);
        if (o\u30078O8\u3007008 == null) {
            return null;
        }
        final JSONObject optJSONObject = o\u30078O8\u3007008.optJSONObject("values");
        if (optJSONObject == null) {
            return null;
        }
        return optJSONObject.optString(s2);
    }
    
    public static JSONObject \u3007o\u3007(final String s, final JSONObject jsonObject, final JSONObject jsonObject2) {
        final JSONObject o\u30078O8\u3007008 = a.O\u30078O8\u3007008(s);
        if (o\u30078O8\u3007008 == null) {
            return null;
        }
        JSONObject jsonObject3;
        if ((jsonObject3 = jsonObject) == null) {
            jsonObject3 = new JSONObject();
        }
        return Oo08(jsonObject2, o\u30078O8\u3007008.optJSONObject("themeValues"), jsonObject3);
    }
    
    private static void \u3007\u3007888(final JSONObject jsonObject, JSONObject keys) {
        JSONObject jsonObject2 = keys;
        if (keys == null) {
            jsonObject2 = new JSONObject();
        }
        if (jsonObject == null) {
            return;
        }
        keys = (JSONObject)jsonObject.keys();
        while (((Iterator)keys).hasNext()) {
            final String s = ((Iterator<String>)keys).next();
            if (!jsonObject2.has(s)) {
                try {
                    jsonObject2.put(s, jsonObject.opt(s));
                }
                catch (final JSONException ex) {
                    ((Throwable)ex).printStackTrace();
                }
            }
        }
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package b.b.a.a.d.d.f.a;

import android.annotation.SuppressLint;
import java.util.ArrayList;
import b.b.a.a.d.g.b;
import android.animation.ObjectAnimator;
import java.util.List;
import b.b.a.a.d.d.h.a;
import android.view.View;

public class k extends d
{
    public k(final View view, final a a) {
        super(view, a);
    }
    
    @SuppressLint({ "ObjectAnimatorBinding" })
    @Override
    List<ObjectAnimator> \u3007o00\u3007\u3007Oo() {
        super.\u300708O\u300700\u3007o.setTag(b.b.a.a.d.d.a.\u3007o00\u3007\u3007Oo, (Object)super.\u3007OOo8\u30070.\u3007\u30078O0\u30078());
        final View \u300708O\u300700\u3007o = super.\u300708O\u300700\u3007o;
        boolean b;
        int n;
        if (\u300708O\u300700\u3007o != null && b.b.a.a.d.g.b.\u3007080(\u300708O\u300700\u3007o.getContext())) {
            b = false;
            n = 1;
        }
        else {
            b = true;
            n = 0;
        }
        final ObjectAnimator setDuration = ObjectAnimator.ofFloat((Object)super.\u300708O\u300700\u3007o, "shineValue", new float[] { (float)n, (float)(b ? 1 : 0) }).setDuration((long)(int)(super.\u3007OOo8\u30070.Oooo8o0\u3007() * 1000.0));
        final ArrayList list = new ArrayList();
        list.add(this.\u3007080(setDuration));
        return list;
    }
}

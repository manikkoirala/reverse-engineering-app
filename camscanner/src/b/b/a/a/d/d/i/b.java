// 
// Decompiled by Procyon v0.6.0
// 

package b.b.a.a.d.d.i;

import b.b.a.a.d.d.h.d;
import java.util.List;
import java.util.Iterator;
import org.json.JSONException;
import android.text.TextUtils;
import org.json.JSONObject;

public class b
{
    private static JSONObject \u3007o00\u3007\u3007Oo(final JSONObject jsonObject, final JSONObject jsonObject2) {
        final JSONObject jsonObject3 = new JSONObject();
        try {
            final JSONObject optJSONObject = jsonObject.optJSONObject("customComponentDefaultValues");
            final JSONObject optJSONObject2 = jsonObject.optJSONObject("values");
            final Iterator keys = optJSONObject2.keys();
            while (keys.hasNext()) {
                final String s = keys.next();
                optJSONObject.put(s, optJSONObject2.opt(s));
            }
            final Iterator keys2 = jsonObject.keys();
            while (keys2.hasNext()) {
                final String s2 = keys2.next();
                if (TextUtils.equals((CharSequence)s2, (CharSequence)"customComponentDefaultValues")) {
                    continue;
                }
                if (TextUtils.equals((CharSequence)s2, (CharSequence)"values")) {
                    jsonObject3.put(s2, (Object)optJSONObject);
                }
                else {
                    jsonObject3.put(s2, jsonObject.opt(s2));
                }
            }
            jsonObject3.put("type", (Object)"vessel");
            final JSONObject optJSONObject3 = jsonObject2.optJSONObject("values");
            final JSONObject optJSONObject4 = jsonObject3.optJSONObject("values");
            if (optJSONObject3 != null && optJSONObject4 != null) {
                final Iterator keys3 = optJSONObject3.keys();
                while (keys3.hasNext()) {
                    final String anObject = keys3.next();
                    if (!"clickArea".equals(anObject)) {
                        optJSONObject4.put(anObject, optJSONObject3.opt(anObject));
                    }
                }
            }
            return jsonObject3;
        }
        catch (final JSONException ex) {
            return jsonObject3;
        }
    }
    
    public JSONObject \u3007080(final List<d.a> list, final int n, final JSONObject jsonObject) {
        if (list != null && list.size() > 0) {
            while (true) {
                for (final d.a a : list) {
                    if (a == null) {
                        continue;
                    }
                    if (a.\u3007080 != n) {
                        continue;
                    }
                    if (a == null) {
                        return null;
                    }
                    final JSONObject \u3007o00\u3007\u3007Oo = a.\u3007o00\u3007\u3007Oo;
                    if (\u3007o00\u3007\u3007Oo == null) {
                        return null;
                    }
                    return \u3007o00\u3007\u3007Oo(\u3007o00\u3007\u3007Oo, jsonObject);
                }
                d.a a = null;
                continue;
            }
        }
        return null;
    }
}

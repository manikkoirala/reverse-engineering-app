// 
// Decompiled by Procyon v0.6.0
// 

package b.b.a.a.d.d.h;

import java.util.Iterator;
import java.util.ArrayList;
import android.text.TextUtils;
import org.json.JSONObject;
import org.json.JSONArray;
import java.util.HashMap;
import java.util.Map;
import java.util.List;

public class h
{
    private float O8;
    private String OO0o\u3007\u3007;
    private List<h> OO0o\u3007\u3007\u3007\u30070;
    private float Oo08;
    private Map<Integer, String> Oooo8o0\u3007;
    private float oO80;
    private float o\u30070;
    private String \u3007080;
    private e \u300780\u3007808\u3007O;
    private h \u30078o8o\u3007;
    private List<List<h>> \u3007O8o08O;
    private float \u3007o00\u3007\u3007Oo;
    private float \u3007o\u3007;
    private float \u3007\u3007888;
    
    public h() {
        this.Oooo8o0\u3007 = new HashMap<Integer, String>();
    }
    
    public void O8(final e \u300780\u3007808\u3007O) {
        this.\u300780\u3007808\u3007O = \u300780\u3007808\u3007O;
    }
    
    public h O8ooOoo\u3007() {
        return this.\u30078o8o\u3007;
    }
    
    public float O8\u3007o() {
        return this.\u3007o00\u3007\u3007Oo;
    }
    
    public Map<Integer, String> OO0o\u3007\u3007() {
        return this.Oooo8o0\u3007;
    }
    
    public void OO0o\u3007\u3007\u3007\u30070(final float oo80) {
        this.oO80 = oo80;
    }
    
    public float OOO\u3007O0() {
        return this.Oo08;
    }
    
    public void Oo08(final h \u30078o8o\u3007) {
        this.\u30078o8o\u3007 = \u30078o8o\u3007;
    }
    
    public float OoO8() {
        return this.\u3007\u3007888;
    }
    
    public void Oooo8o0\u3007(final float o8) {
        this.O8 = o8;
    }
    
    public int O\u30078O8\u3007008() {
        final f \u3007o8o08O = this.\u300780\u3007808\u3007O.\u3007O8o08O();
        return \u3007o8o08O.\u300700\u30078() + \u3007o8o08O.OOO\u3007O0();
    }
    
    public boolean o0ooO() {
        final List<h> oo0o\u3007\u3007\u3007\u30070 = this.OO0o\u3007\u3007\u3007\u30070;
        return oo0o\u3007\u3007\u3007\u30070 == null || oo0o\u3007\u3007\u3007\u30070.size() <= 0;
    }
    
    public void o800o8O(final float \u3007o00\u3007\u3007Oo) {
        this.\u3007o00\u3007\u3007Oo = \u3007o00\u3007\u3007Oo;
    }
    
    public void oO80(final JSONArray jsonArray) {
        if (jsonArray == null) {
            return;
        }
        try {
            if (jsonArray.length() != 0) {
                for (int i = 0; i < jsonArray.length(); ++i) {
                    final JSONObject optJSONObject = jsonArray.optJSONObject(i);
                    this.Oooo8o0\u3007.put(optJSONObject.optInt("id"), optJSONObject.optString("value"));
                }
            }
        }
        finally {}
    }
    
    public void oo88o8O(final float \u3007o\u3007) {
        this.\u3007o\u3007 = \u3007o\u3007;
    }
    
    public float oo\u3007() {
        return this.o\u30070;
    }
    
    public void o\u30070(final String oo0o\u3007\u3007) {
        this.OO0o\u3007\u3007 = oo0o\u3007\u3007;
    }
    
    public boolean o\u30078() {
        return TextUtils.equals((CharSequence)this.\u300780\u3007808\u3007O.\u3007O8o08O().\u3007\u30070\u30070o8(), (CharSequence)"flex");
    }
    
    public float o\u3007O8\u3007\u3007o() {
        final f \u3007o8o08O = this.\u300780\u3007808\u3007O.\u3007O8o08O();
        return this.\u300700() + \u3007o8o08O.oO00OOO() + \u3007o8o08O.OO8oO0o\u3007() + \u3007o8o08O.\u30078o8O\u3007O() * 2.0f;
    }
    
    public float o\u3007\u30070\u3007() {
        return this.O8;
    }
    
    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append("DynamicLayoutUnit{id='");
        sb.append(this.\u3007080);
        sb.append('\'');
        sb.append(", x=");
        sb.append(this.\u3007o00\u3007\u3007Oo);
        sb.append(", y=");
        sb.append(this.\u3007o\u3007);
        sb.append(", width=");
        sb.append(this.o\u30070);
        sb.append(", height=");
        sb.append(this.\u3007\u3007888);
        sb.append(", remainWidth=");
        sb.append(this.oO80);
        sb.append(", rootBrick=");
        sb.append(this.\u300780\u3007808\u3007O);
        sb.append(", childrenBrickUnits=");
        sb.append(this.OO0o\u3007\u3007\u3007\u30070);
        sb.append('}');
        return sb.toString();
    }
    
    public int \u300700() {
        final f \u3007o8o08O = this.\u300780\u3007808\u3007O.\u3007O8o08O();
        return \u3007o8o08O.oo\u3007() + \u3007o8o08O.O8\u3007o();
    }
    
    public e \u30070000OOO() {
        return this.\u300780\u3007808\u3007O;
    }
    
    public float \u300700\u30078() {
        return this.\u3007o\u3007;
    }
    
    public String \u3007080(final int i) {
        final StringBuilder sb = new StringBuilder();
        sb.append(this.\u300780\u3007808\u3007O.\u30078o8o\u3007());
        sb.append(":");
        sb.append(this.\u3007080);
        if (this.\u300780\u3007808\u3007O.\u3007O8o08O() != null) {
            sb.append(":");
            sb.append(this.\u300780\u3007808\u3007O.\u3007O8o08O().O000());
        }
        sb.append(":");
        sb.append(i);
        return sb.toString();
    }
    
    public void \u30070\u3007O0088o(final float o\u30070) {
        this.o\u30070 = o\u30070;
    }
    
    public String \u300780\u3007808\u3007O() {
        return this.OO0o\u3007\u3007;
    }
    
    public void \u30078o8o\u3007(final String \u3007080) {
        this.\u3007080 = \u3007080;
    }
    
    public void \u3007O00(final float oo08) {
        this.Oo08 = oo08;
    }
    
    public String \u3007O888o0o() {
        return this.\u3007080;
    }
    
    public void \u3007O8o08O(final List<h> oo0o\u3007\u3007\u3007\u30070) {
        this.OO0o\u3007\u3007\u3007\u30070 = oo0o\u3007\u3007\u3007\u30070;
    }
    
    public List<List<h>> \u3007O\u3007() {
        return this.\u3007O8o08O;
    }
    
    public boolean \u3007o() {
        return this.\u300780\u3007808\u3007O.\u3007O8o08O().O8oOo80() < 0 || this.\u300780\u3007808\u3007O.\u3007O8o08O().O0\u3007oo() < 0 || this.\u300780\u3007808\u3007O.\u3007O8o08O().O8ooOoo\u3007() < 0 || this.\u300780\u3007808\u3007O.\u3007O8o08O().oo() < 0;
    }
    
    public void \u3007o00\u3007\u3007Oo() {
        final List<List<h>> \u3007o8o08O = this.\u3007O8o08O;
        if (\u3007o8o08O != null) {
            if (\u3007o8o08O.size() > 0) {
                final ArrayList \u3007o8o08O2 = new ArrayList();
                for (final List list : this.\u3007O8o08O) {
                    if (list != null && list.size() > 0) {
                        \u3007o8o08O2.add(list);
                    }
                }
                this.\u3007O8o08O = \u3007o8o08O2;
            }
        }
    }
    
    public String \u3007oOO8O8() {
        return this.\u300780\u3007808\u3007O.\u3007O8o08O().o0O0();
    }
    
    public float \u3007oo\u3007() {
        final f \u3007o8o08O = this.\u300780\u3007808\u3007O.\u3007O8o08O();
        return this.O\u30078O8\u3007008() + \u3007o8o08O.Oo\u3007O() + \u3007o8o08O.oO() + \u3007o8o08O.\u30078o8O\u3007O() * 2.0f;
    }
    
    public void \u3007o\u3007(final float \u3007\u3007888) {
        this.\u3007\u3007888 = \u3007\u3007888;
    }
    
    public void \u3007\u3007808\u3007(final String s) {
        this.\u300780\u3007808\u3007O.\u3007O8o08O().O0O8OO088(s);
    }
    
    public void \u3007\u3007888(final List<List<h>> \u3007o8o08O) {
        this.\u3007O8o08O = \u3007o8o08O;
    }
    
    public List<h> \u3007\u30078O0\u30078() {
        return this.OO0o\u3007\u3007\u3007\u30070;
    }
}

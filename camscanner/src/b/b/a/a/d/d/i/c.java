// 
// Decompiled by Procyon v0.6.0
// 

package b.b.a.a.d.d.i;

import java.util.Iterator;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONArray;

public class c
{
    private static JSONArray \u3007080(final JSONArray jsonArray, final JSONArray jsonArray2) {
        JSONArray jsonArray3 = jsonArray;
        if (jsonArray2 == null) {
            return jsonArray3;
        }
        if (jsonArray2.length() <= 0) {
            jsonArray3 = jsonArray;
            return jsonArray3;
        }
        Label_0172: {
            if (jsonArray == null || jsonArray.length() <= 0) {
                break Label_0172;
            }
            jsonArray3 = new JSONArray();
            int i = 0;
            try {
                while (i < jsonArray.length()) {
                    final Object opt = jsonArray.opt(i);
                    final Object opt2 = jsonArray2.opt(i);
                    if (opt2 == null) {
                        jsonArray3.put(i, opt);
                    }
                    else if (opt instanceof JSONObject && opt2 instanceof JSONObject) {
                        jsonArray3.put(i, (Object)\u3007o00\u3007\u3007Oo((JSONObject)opt, (JSONObject)opt2));
                    }
                    else if (opt instanceof JSONArray && opt2 instanceof JSONArray) {
                        jsonArray3.put(i, (Object)\u3007080((JSONArray)opt, (JSONArray)opt2));
                    }
                    else {
                        jsonArray3.put(i, opt2);
                    }
                    ++i;
                }
                return jsonArray3;
                jsonArray3 = null;
                return jsonArray3;
            }
            catch (final JSONException ex) {
                return jsonArray3;
            }
        }
    }
    
    public static JSONObject \u3007o00\u3007\u3007Oo(final JSONObject jsonObject, final JSONObject jsonObject2) {
        final JSONObject jsonObject3 = new JSONObject();
        if (jsonObject2 == null || jsonObject2.length() <= 0 || jsonObject == null) {
            return jsonObject;
        }
        try {
            final Iterator keys = jsonObject.keys();
            while (keys.hasNext()) {
                final String s = keys.next();
                final Object opt = jsonObject.opt(s);
                final Object opt2 = jsonObject2.opt(s);
                if (opt2 == null) {
                    jsonObject3.put(s, opt);
                }
                else if (opt instanceof JSONObject && opt2 instanceof JSONObject) {
                    jsonObject3.put(s, (Object)\u3007o00\u3007\u3007Oo((JSONObject)opt, (JSONObject)opt2));
                }
                else if (opt instanceof JSONArray && opt2 instanceof JSONArray) {
                    jsonObject3.put(s, (Object)\u3007080((JSONArray)opt, (JSONArray)opt2));
                }
                else {
                    jsonObject3.put(s, opt2);
                }
            }
            final Iterator keys2 = jsonObject2.keys();
            while (keys2.hasNext()) {
                final String s2 = keys2.next();
                if (!jsonObject.has(s2)) {
                    jsonObject3.put(s2, jsonObject2.opt(s2));
                }
            }
            return jsonObject3;
        }
        catch (final JSONException ex) {
            return jsonObject3;
        }
    }
}

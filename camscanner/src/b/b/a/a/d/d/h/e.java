// 
// Decompiled by Procyon v0.6.0
// 

package b.b.a.a.d.d.h;

import android.text.TextUtils;
import java.util.HashMap;
import java.util.Map;

public class e
{
    public static final Map<String, Integer> o\u30070;
    private f O8;
    private String Oo08;
    private String \u3007080;
    private String \u3007o00\u3007\u3007Oo;
    private f \u3007o\u3007;
    
    static {
        final HashMap o\u3007 = new HashMap();
        (o\u30070 = o\u3007).put("root", 8);
        final Integer value = 6;
        o\u3007.put("footer", value);
        o\u3007.put("empty", value);
        final Integer value2 = 0;
        o\u3007.put("title", value2);
        o\u3007.put("subtitle", value2);
        o\u3007.put("source", value2);
        o\u3007.put("score-count", value2);
        o\u3007.put("text_star", value2);
        o\u3007.put("text", value2);
        o\u3007.put("tag-group", 17);
        o\u3007.put("app-version", value2);
        o\u3007.put("development-name", value2);
        o\u3007.put("privacy-detail", 23);
        final Integer value3 = 1;
        o\u3007.put("image", value3);
        o\u3007.put("image-wide", value3);
        o\u3007.put("image-square", value3);
        o\u3007.put("image-long", value3);
        o\u3007.put("image-splash", value3);
        o\u3007.put("image-cover", value3);
        o\u3007.put("app-icon", value3);
        o\u3007.put("icon-download", value3);
        o\u3007.put("logoad", 4);
        o\u3007.put("logounion", 5);
        o\u3007.put("logo-union", 9);
        final Integer value4 = 3;
        o\u3007.put("dislike", value4);
        o\u3007.put("close", value4);
        o\u3007.put("close-fill", value4);
        o\u3007.put("webview-close", 22);
        o\u3007.put("feedback-dislike", 12);
        final Integer value5 = 2;
        o\u3007.put("button", value5);
        o\u3007.put("downloadWithIcon", value5);
        o\u3007.put("downloadButton", value5);
        o\u3007.put("fillButton", value5);
        o\u3007.put("laceButton", value5);
        o\u3007.put("cardButton", value5);
        o\u3007.put("colourMixtureButton", value5);
        o\u3007.put("arrowButton", value3);
        o\u3007.put("download-progress-button", value5);
        o\u3007.put("vessel", value);
        o\u3007.put("image-group", value);
        o\u3007.put("custom-component-vessel", value);
        o\u3007.put("carousel", 24);
        o\u3007.put("carousel-vessel", 26);
        o\u3007.put("leisure-interact", 25);
        final Integer value6 = 7;
        o\u3007.put("video-hd", value6);
        o\u3007.put("video", value6);
        o\u3007.put("video-vd", value6);
        o\u3007.put("video-sq", value6);
        o\u3007.put("muted", 10);
        o\u3007.put("star", 11);
        o\u3007.put("skip-countdowns", 19);
        o\u3007.put("skip-with-countdowns-skip-btn", 21);
        final Integer value7 = 13;
        o\u3007.put("skip-with-countdowns-video-countdown", value7);
        o\u3007.put("skip-with-countdowns-skip-countdown", 20);
        o\u3007.put("skip-with-time", 14);
        o\u3007.put("skip-with-time-countdown", value7);
        o\u3007.put("skip-with-time-skip-btn", 15);
        o\u3007.put("skip", 27);
        o\u3007.put("timedown", value7);
        o\u3007.put("icon", 16);
        o\u3007.put("scoreCountWithIcon", value);
        o\u3007.put("split-line", 18);
        o\u3007.put("creative-playable-bait", value2);
        o\u3007.put("score-count-type-2", value2);
    }
    
    public String O8() {
        return this.Oo08;
    }
    
    public int OO0o\u3007\u3007\u3007\u30070() {
        return this.\u3007o\u3007.O8oOo80();
    }
    
    public void Oo08(final f \u3007o\u3007) {
        this.\u3007o\u3007 = \u3007o\u3007;
    }
    
    public void oO80(final String \u3007080) {
        this.\u3007080 = \u3007080;
    }
    
    public void o\u30070(final String oo08) {
        this.Oo08 = oo08;
    }
    
    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append("DynamicLayoutBrick{type='");
        sb.append(this.\u3007080);
        sb.append('\'');
        sb.append(", data='");
        sb.append(this.\u3007o00\u3007\u3007Oo);
        sb.append('\'');
        sb.append(", value=");
        sb.append(this.\u3007o\u3007);
        sb.append(", themeValue=");
        sb.append(this.O8);
        sb.append(", dataExtraInfo='");
        sb.append(this.Oo08);
        sb.append('\'');
        sb.append('}');
        return sb.toString();
    }
    
    public String \u3007080() {
        return this.\u3007o00\u3007\u3007Oo;
    }
    
    public f \u300780\u3007808\u3007O() {
        return this.O8;
    }
    
    public String \u30078o8o\u3007() {
        return this.\u3007080;
    }
    
    public f \u3007O8o08O() {
        return this.\u3007o\u3007;
    }
    
    public void \u3007o00\u3007\u3007Oo(final f o8) {
        this.O8 = o8;
    }
    
    public void \u3007o\u3007(final String \u3007o00\u3007\u3007Oo) {
        this.\u3007o00\u3007\u3007Oo = \u3007o00\u3007\u3007Oo;
    }
    
    public int \u3007\u3007888() {
        if (TextUtils.isEmpty((CharSequence)this.\u3007080)) {
            return 0;
        }
        if (this.\u3007080.equals("logo")) {
            final StringBuilder sb = new StringBuilder();
            sb.append(this.\u3007080);
            sb.append(this.\u3007o00\u3007\u3007Oo);
            final String string = sb.toString();
            this.\u3007080 = string;
            if (string.contains("logoad")) {
                return 4;
            }
            if (this.\u3007080.contains("logounion")) {
                return 5;
            }
        }
        final Map<String, Integer> o\u30070 = e.o\u30070;
        if (o\u30070.get(this.\u3007080) != null) {
            return o\u30070.get(this.\u3007080);
        }
        return -1;
    }
}

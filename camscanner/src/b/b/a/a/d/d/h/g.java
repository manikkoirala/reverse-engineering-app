// 
// Decompiled by Procyon v0.6.0
// 

package b.b.a.a.d.d.h;

import b.b.a.a.d.d.i.j;
import android.graphics.Color;
import android.text.TextUtils;
import b.b.a.a.d.c;

public class g
{
    private e O8;
    private String Oo08;
    public int \u3007080;
    public String \u3007o00\u3007\u3007Oo;
    private f \u3007o\u3007;
    
    public g(final e o8) {
        this.O8 = o8;
        this.\u3007080 = o8.\u3007\u3007888();
        this.\u3007o00\u3007\u3007Oo = o8.\u3007080();
        this.Oo08 = o8.O8();
        if (c.\u3007o00\u3007\u3007Oo() == 1) {
            this.\u3007o\u3007 = o8.\u300780\u3007808\u3007O();
        }
        else {
            this.\u3007o\u3007 = o8.\u3007O8o08O();
        }
        if (c.\u3007o\u3007()) {
            this.\u3007o\u3007 = o8.\u3007O8o08O();
        }
    }
    
    public static int OOO\u3007O0(final String s) {
        if (TextUtils.isEmpty((CharSequence)s)) {
            return -16777216;
        }
        if (s.equals("transparent")) {
            return 0;
        }
        if (s.charAt(0) == '#' && s.length() == 7) {
            return Color.parseColor(s);
        }
        if (s.charAt(0) == '#' && s.length() == 9) {
            return Color.parseColor(s);
        }
        if (!s.startsWith("rgba")) {
            return -16777216;
        }
        final String[] split = s.substring(s.indexOf("(") + 1, s.indexOf(")")).split(",");
        if (split != null && split.length == 4) {
            return (int)(Float.parseFloat(split[3]) * 255.0f + 0.5f) << 24 | (int)Float.parseFloat(split[0]) << 16 | (int)Float.parseFloat(split[1]) << 8 | (int)Float.parseFloat(split[2]) | 0x0;
        }
        return -16777216;
    }
    
    private boolean o\u3007O8\u3007\u3007o() {
        final boolean \u3007o\u3007 = c.\u3007o\u3007();
        final boolean b = true;
        if (\u3007o\u3007 && (this.O8.\u30078o8o\u3007().contains("logo-union") || this.O8.\u30078o8o\u3007().contains("logounion") || this.O8.\u30078o8o\u3007().contains("logoad"))) {
            return true;
        }
        boolean b2 = b;
        if (!"logo-union".equals(this.O8.\u30078o8o\u3007())) {
            b2 = b;
            if (!"logounion".equals(this.O8.\u30078o8o\u3007())) {
                b2 = ("logoad".equals(this.O8.\u30078o8o\u3007()) && b);
            }
        }
        return b2;
    }
    
    public static float[] \u30070000OOO(final String s) {
        final String[] split = s.substring(s.indexOf("(") + 1, s.indexOf(")")).split(",");
        if (split != null && split.length == 4) {
            return new float[] { Float.parseFloat(split[0]), Float.parseFloat(split[1]), Float.parseFloat(split[2]), Float.parseFloat(split[3]) };
        }
        return new float[] { 0.0f, 0.0f, 0.0f, 0.0f };
    }
    
    private boolean \u3007O888o0o() {
        return !c.\u3007o\u3007() && ((!TextUtils.isEmpty((CharSequence)this.\u3007o00\u3007\u3007Oo) && this.\u3007o00\u3007\u3007Oo.contains("adx:")) || j.\u300780\u3007808\u3007O());
    }
    
    public String O000() {
        return this.\u3007o\u3007.\u3007\u3007808\u3007();
    }
    
    public String O08000() {
        return this.\u3007o\u3007.OOo88OOo();
    }
    
    public String O8() {
        return this.\u3007o\u3007.\u3007O888o0o();
    }
    
    public void O8ooOoo\u3007(final float n) {
        this.\u3007o\u3007.o8(n);
    }
    
    public int O8\u3007o() {
        return this.\u3007o\u3007.Oo();
    }
    
    public int OO0o\u3007\u3007() {
        return this.\u3007o\u3007.O00();
    }
    
    public int OO0o\u3007\u3007\u3007\u30070() {
        return (int)this.\u3007o\u3007.oO00OOO();
    }
    
    public boolean Oo08() {
        return this.\u3007o\u3007.oo88o8O();
    }
    
    public float Oo8Oo00oo() {
        return this.\u3007o\u3007.OO0\u3007\u30078();
    }
    
    public double OoO8() {
        return this.\u3007o\u3007.\u3007oo();
    }
    
    public boolean Ooo() {
        return this.\u3007o\u3007.\u3007\u30078O0\u30078();
    }
    
    public double Oooo8o0\u3007() {
        if (this.\u3007080 != 11) {
            return -1.0;
        }
        try {
            final double double1 = Double.parseDouble(this.\u3007o00\u3007\u3007Oo);
            final boolean \u3007o\u3007 = c.\u3007o\u3007();
            double n = double1;
            if (!\u3007o\u3007) {
                n = (int)double1;
            }
            return n;
        }
        catch (final NumberFormatException ex) {
            return -1.0;
        }
    }
    
    public int O\u30078O8\u3007008() {
        return OOO\u3007O0(this.\u3007o\u3007.o0O0());
    }
    
    public float O\u3007O\u3007oO() {
        return this.\u3007o\u3007.\u3007080();
    }
    
    public int o0ooO() {
        return this.\u3007o\u3007.O0oO008();
    }
    
    public int o8() {
        return OOO\u3007O0(this.\u3007o\u3007.\u30078o());
    }
    
    public int o800o8O() {
        return this.\u3007o\u3007.oO8008O();
    }
    
    public int o8oO\u3007() {
        final int \u3007o\u3007 = this.\u3007O\u3007();
        if (\u3007o\u3007 == 4) {
            return 17;
        }
        if (\u3007o\u3007 == 3) {
            return 8388613;
        }
        return 8388611;
    }
    
    public String oO() {
        final int \u3007080 = this.\u3007080;
        if (\u3007080 != 2 && \u3007080 != 13) {
            return "";
        }
        return this.\u3007o00\u3007\u3007Oo;
    }
    
    public String oO00OOO() {
        return this.\u3007o\u3007.OO0o\u3007\u3007();
    }
    
    public int oO80() {
        return this.\u3007o\u3007.o8O0();
    }
    
    public boolean oo88o8O() {
        return this.\u3007o\u3007.o\u3007O8\u3007\u3007o();
    }
    
    public boolean oo\u3007() {
        return this.\u3007o\u3007.O\u30070();
    }
    
    public int o\u30070() {
        return this.\u3007o\u3007.\u3007oo\u3007();
    }
    
    public float o\u30070OOo\u30070() {
        return this.\u3007o\u3007.\u30078o8O\u3007O();
    }
    
    public int o\u30078() {
        return this.\u3007o\u3007.o0();
    }
    
    public String o\u30078oOO88() {
        return this.\u3007o\u3007.OO0o\u3007\u3007\u3007\u30070();
    }
    
    public String o\u3007O() {
        if (this.\u3007080 == 1) {
            return this.\u3007o00\u3007\u3007Oo;
        }
        return "";
    }
    
    public double o\u3007\u30070\u3007() {
        return this.\u3007o\u3007.o8O\u3007();
    }
    
    public boolean \u300700() {
        return this.\u3007o\u3007.\u30078O0O808\u3007();
    }
    
    public String \u300700\u30078() {
        return this.\u3007o\u3007.O\u3007Oooo\u3007\u3007();
    }
    
    public int \u3007080() {
        return this.\u3007o\u3007.\u30070\u3007O0088o();
    }
    
    public int \u300708O8o\u30070() {
        return this.\u3007o\u3007.\u3007008\u3007oo();
    }
    
    public int \u30070\u3007O0088o() {
        return OOO\u3007O0(this.\u3007o\u3007.o08O());
    }
    
    public int \u30078() {
        final String o88O8 = this.\u3007o\u3007.o88O8();
        if (!"skip-with-time-skip-btn".equals(this.O8.\u30078o8o\u3007()) && !"skip".equals(this.O8.\u30078o8o\u3007()) && !TextUtils.equals((CharSequence)"skip-with-countdowns-skip-btn", (CharSequence)this.O8.\u30078o8o\u3007())) {
            if (!"skip-with-time-countdown".equals(this.O8.\u30078o8o\u3007())) {
                if (!"skip-with-time".equals(this.O8.\u30078o8o\u3007())) {
                    if (this.\u3007080 == 10 && TextUtils.equals((CharSequence)this.\u3007o\u3007.OOo88OOo(), (CharSequence)"click")) {
                        return 5;
                    }
                    if (this.o\u3007O8\u3007\u3007o() && this.\u3007O888o0o()) {
                        return 0;
                    }
                    if (this.o\u3007O8\u3007\u3007o()) {
                        return 7;
                    }
                    if ("feedback-dislike".equals(this.O8.\u30078o8o\u3007())) {
                        return 3;
                    }
                    if (!TextUtils.isEmpty((CharSequence)o88O8)) {
                        if (!o88O8.equals("none")) {
                            if (!o88O8.equals("video") && (this.O8.\u3007\u3007888() != 7 || !TextUtils.equals((CharSequence)o88O8, (CharSequence)"normal"))) {
                                if (o88O8.equals("normal")) {
                                    return 1;
                                }
                                if (o88O8.equals("creative")) {
                                    return 2;
                                }
                                if ("slide".equals(this.\u3007o\u3007.OOo88OOo())) {
                                    return 2;
                                }
                                return 0;
                            }
                            else {
                                if (c.\u3007o\u3007() && this.O8.\u3007O8o08O() != null && this.O8.\u3007O8o08O().OOo0O()) {
                                    return 11;
                                }
                                return 4;
                            }
                        }
                    }
                }
            }
            return 0;
        }
        return 6;
    }
    
    public int \u300780() {
        return this.\u3007o\u3007.\u3007O\u3007();
    }
    
    public int \u300780\u3007808\u3007O() {
        return (int)this.\u3007o\u3007.oO();
    }
    
    public int \u30078o8o\u3007() {
        return (int)this.\u3007o\u3007.OO8oO0o\u3007();
    }
    
    public String \u30078\u30070\u3007o\u3007O() {
        return this.Oo08;
    }
    
    public int \u3007O00() {
        return this.\u3007o\u3007.OO\u30070008O8();
    }
    
    public int \u3007O8o08O() {
        return (int)this.\u3007o\u3007.Oo\u3007O();
    }
    
    public int \u3007O\u3007() {
        final String oOo = this.\u3007o\u3007.oOo();
        if ("left".equals(oOo)) {
            return 17;
        }
        if ("center".equals(oOo)) {
            return 4;
        }
        if ("right".equals(oOo)) {
            return 3;
        }
        return 2;
    }
    
    public String \u3007o() {
        return this.\u3007o\u3007.Oo\u3007O8o\u30078();
    }
    
    public String \u3007o00\u3007\u3007Oo() {
        return this.\u3007o\u3007.OoO8();
    }
    
    public boolean \u3007oOO8O8(final int n) {
        final e o8 = this.O8;
        boolean b = false;
        if (o8 == null) {
            return false;
        }
        if (n == 1) {
            this.\u3007o\u3007 = o8.\u300780\u3007808\u3007O();
        }
        else {
            this.\u3007o\u3007 = o8.\u3007O8o08O();
        }
        if (this.\u3007o\u3007 != null) {
            b = true;
        }
        return b;
    }
    
    public boolean \u3007oo\u3007() {
        return this.\u3007o\u3007.\u3007080O0();
    }
    
    public int \u3007o\u3007() {
        return this.\u3007o\u3007.o800o8O();
    }
    
    public int \u3007\u30070o() {
        return this.\u3007o\u3007.o\u3007\u30070\u300788();
    }
    
    public String \u3007\u3007808\u3007() {
        if (this.\u3007080 == 0) {
            return this.\u3007o00\u3007\u3007Oo;
        }
        return "";
    }
    
    public int \u3007\u3007888() {
        return this.\u3007o\u3007.\u30070000OOO();
    }
    
    public int \u3007\u30078O0\u30078() {
        return this.\u3007o\u3007.OOoo();
    }
    
    public int \u3007\u3007\u30070\u3007\u30070() {
        return this.\u3007o\u3007.\u3007o8OO0();
    }
}

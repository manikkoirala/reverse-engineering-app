// 
// Decompiled by Procyon v0.6.0
// 

package b.b.a.a.d.d.f.a;

import java.util.ArrayList;
import android.animation.ObjectAnimator;
import java.util.List;
import b.b.a.a.d.d.h.a;
import android.view.View;

public class e extends d
{
    public e(final View view, final a a) {
        super(view, a);
    }
    
    @Override
    List<ObjectAnimator> \u3007o00\u3007\u3007Oo() {
        final int width = super.\u300708O\u300700\u3007o.getLayoutParams().width;
        final View \u300708O\u300700\u3007o = super.\u300708O\u300700\u3007o;
        final float translationX = (float)width;
        \u300708O\u300700\u3007o.setTranslationX(translationX);
        final ObjectAnimator setDuration = ObjectAnimator.ofFloat((Object)super.\u300708O\u300700\u3007o, "translationX", new float[] { translationX, 0.0f }).setDuration((long)(int)(super.\u3007OOo8\u30070.Oooo8o0\u3007() * 1000.0));
        final ObjectAnimator setDuration2 = ObjectAnimator.ofFloat((Object)super.\u300708O\u300700\u3007o, "alpha", new float[] { 0.0f, 1.0f }).setDuration((long)(int)(super.\u3007OOo8\u30070.Oooo8o0\u3007() * 1000.0));
        final ArrayList list = new ArrayList();
        list.add(this.\u3007080(setDuration));
        list.add(this.\u3007080(setDuration2));
        return list;
    }
}

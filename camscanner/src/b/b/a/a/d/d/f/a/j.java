// 
// Decompiled by Procyon v0.6.0
// 

package b.b.a.a.d.d.f.a;

import java.util.ArrayList;
import android.animation.ObjectAnimator;
import java.util.List;
import com.bytedance.sdk.component.adexpress.dynamic.dynamicview.DynamicBaseWidget;
import android.view.ViewGroup;
import b.b.a.a.d.d.h.a;
import android.view.View;

public class j extends d
{
    public j(final View view, final a a) {
        super(view, a);
        final ViewGroup viewGroup = (ViewGroup)view.getParent();
        if (viewGroup != null) {
            viewGroup.setClipChildren(false);
            viewGroup.setClipToPadding(false);
            final ViewGroup viewGroup2 = (ViewGroup)((View)viewGroup).getParent();
            if (viewGroup2 != null && viewGroup2 instanceof DynamicBaseWidget) {
                viewGroup2.setClipChildren(false);
                viewGroup2.setClipToPadding(false);
                final ViewGroup viewGroup3 = (ViewGroup)((View)viewGroup2).getParent();
                if (viewGroup3 != null && viewGroup3 instanceof DynamicBaseWidget) {
                    viewGroup3.setClipChildren(false);
                    viewGroup3.setClipToPadding(false);
                }
            }
        }
    }
    
    @Override
    List<ObjectAnimator> \u3007o00\u3007\u3007Oo() {
        float n = (float)super.\u3007OOo8\u30070.oo\u3007();
        float n2 = (float)super.\u3007OOo8\u30070.O8\u3007o();
        final String oo0o\u3007\u3007\u3007\u30070 = super.\u3007OOo8\u30070.OO0o\u3007\u3007\u3007\u30070();
        final boolean equals = "reverse".equals(oo0o\u3007\u3007\u3007\u30070);
        float n3 = 1.0f;
        float n4;
        if (!equals && !"alternate-reverse".equals(oo0o\u3007\u3007\u3007\u30070)) {
            n3 = n;
            n4 = n2;
            n = 1.0f;
            n2 = 1.0f;
        }
        else {
            n4 = 1.0f;
        }
        super.\u300708O\u300700\u3007o.setTag(b.b.a.a.d.d.a.O8, (Object)super.\u3007OOo8\u30070.OOO\u3007O0());
        final ObjectAnimator setDuration = ObjectAnimator.ofFloat((Object)super.\u300708O\u300700\u3007o, "scaleX", new float[] { n, n3 }).setDuration((long)(int)(super.\u3007OOo8\u30070.Oooo8o0\u3007() * 1000.0));
        final ObjectAnimator setDuration2 = ObjectAnimator.ofFloat((Object)super.\u300708O\u300700\u3007o, "scaleY", new float[] { n2, n4 }).setDuration((long)(int)(super.\u3007OOo8\u30070.Oooo8o0\u3007() * 1000.0));
        final ArrayList list = new ArrayList();
        list.add(this.\u3007080(setDuration));
        list.add(this.\u3007080(setDuration2));
        return list;
    }
}

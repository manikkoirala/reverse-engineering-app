// 
// Decompiled by Procyon v0.6.0
// 

package b.b.a.a.d.d.i;

import android.view.View;
import android.widget.TextView;
import b.b.a.a.d.f.m;
import b.b.a.a.d.d.h.h;
import android.text.TextUtils;
import b.b.a.a.d.g.d;
import b.b.a.a.d.c;
import org.json.JSONObject;
import java.util.Collections;
import java.util.Collection;
import java.util.HashSet;
import java.util.Arrays;
import java.util.Set;

public class j
{
    private static final Set<String> \u3007080;
    private static String \u3007o00\u3007\u3007Oo;
    
    static {
        \u3007080 = Collections.unmodifiableSet((Set<? extends String>)new HashSet<String>(Arrays.asList("dislike", "close", "close-fill", "webview-close")));
    }
    
    public static a.c O8(final String s, final String s2, final boolean b) {
        final a.c c = new a.c();
        try {
            final JSONObject jsonObject = new JSONObject(s2);
            final int[] oo0o\u3007\u3007\u3007\u30070 = OO0o\u3007\u3007\u3007\u30070(s, (float)oO80(s2), b);
            c.\u3007080 = (float)oo0o\u3007\u3007\u3007\u30070[0];
            c.\u3007o00\u3007\u3007Oo = (float)oo0o\u3007\u3007\u3007\u30070[1];
            if (jsonObject.optDouble("lineHeight", 1.0) == 0.0) {
                c.\u3007o00\u3007\u3007Oo = 0.0f;
            }
        }
        catch (final Exception ex) {
            ex.printStackTrace();
        }
        return c;
    }
    
    public static int[] OO0o\u3007\u3007\u3007\u30070(final String s, final float n, final boolean b) {
        final int[] \u3007\u3007888 = \u3007\u3007888(s, n, b);
        return new int[] { d.\u3007\u3007888(c.\u3007080(), (float)\u3007\u3007888[0]), d.\u3007\u3007888(c.\u3007080(), (float)\u3007\u3007888[1]) };
    }
    
    public static String Oo08() {
        return j.\u3007o00\u3007\u3007Oo;
    }
    
    public static double oO80(final String s) {
        try {
            return Double.parseDouble(new JSONObject(s).optString("fontSize"));
        }
        finally {
            return 0.0;
        }
    }
    
    public static String o\u30070(final String s) {
        if (TextUtils.isEmpty((CharSequence)s)) {
            return "";
        }
        final String[] split = s.split("adx:");
        if (split != null && split.length >= 2) {
            return split[1];
        }
        return "";
    }
    
    private static a.c \u3007080(final a.c c, final String s, final String s2, final String s3) {
        if (s.contains("union")) {
            c.\u3007080 = 0.0f;
            c.\u3007o00\u3007\u3007Oo = 0.0f;
        }
        else {
            String o\u30070 = s3;
            if (TextUtils.isEmpty((CharSequence)s3)) {
                o\u30070 = o\u30070(s);
            }
            if (!TextUtils.isEmpty((CharSequence)o\u30070)) {
                return \u3007o00\u3007\u3007Oo(o\u30070, s2);
            }
            c.\u3007080 = 0.0f;
            c.\u3007o00\u3007\u3007Oo = 0.0f;
        }
        return c;
    }
    
    public static boolean \u300780\u3007808\u3007O() {
        return !TextUtils.isEmpty((CharSequence)j.\u3007o00\u3007\u3007Oo);
    }
    
    public static a.c \u3007o00\u3007\u3007Oo(final String s, final String s2) {
        return O8(s, s2, false);
    }
    
    public static a.c \u3007o\u3007(final String p0, final String p1, final String p2, final boolean p3, final boolean p4, final int p5, final h p6, final double p7, final int p8, final double p9, final String p10, final m p11) {
        // 
        // This method could not be decompiled.
        // 
        // Original Bytecode:
        // 
        //     1: astore          6
        //     3: aload           13
        //     5: invokevirtual   b/b/a/a/d/f/m.\u3007080:()Ljava/lang/String;
        //     8: astore          20
        //    10: aload           13
        //    12: invokevirtual   b/b/a/a/d/f/m.o\u30070:()I
        //    15: istore          19
        //    17: invokestatic    b/b/a/a/d/c.\u3007o\u3007:()Z
        //    20: ifeq            75
        //    23: iload           9
        //    25: iconst_4       
        //    26: if_icmpeq       75
        //    29: aload_1        
        //    30: ldc             "text_star"
        //    32: invokestatic    android/text/TextUtils.equals:(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z
        //    35: ifne            65
        //    38: aload_1        
        //    39: ldc             "score-count"
        //    41: invokestatic    android/text/TextUtils.equals:(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z
        //    44: ifne            65
        //    47: aload_1        
        //    48: ldc             "score-count-type-1"
        //    50: invokestatic    android/text/TextUtils.equals:(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z
        //    53: ifne            65
        //    56: aload_1        
        //    57: ldc             "score-count-type-2"
        //    59: invokestatic    android/text/TextUtils.equals:(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z
        //    62: ifeq            75
        //    65: new             Lb/b/a/a/d/d/i/a$c;
        //    68: dup            
        //    69: fconst_0       
        //    70: fconst_0       
        //    71: invokespecial   b/b/a/a/d/d/i/a$c.<init>:(FF)V
        //    74: areturn        
        //    75: new             Lb/b/a/a/d/d/i/a$c;
        //    78: dup            
        //    79: invokespecial   b/b/a/a/d/d/i/a$c.<init>:()V
        //    82: astore          12
        //    84: aload           6
        //    86: ldc             "<svg"
        //    88: invokevirtual   java/lang/String.startsWith:(Ljava/lang/String;)Z
        //    91: ifne            1646
        //    94: getstatic       b/b/a/a/d/d/i/j.\u3007080:Ljava/util/Set;
        //    97: aload_1        
        //    98: invokeinterface java/util/Set.contains:(Ljava/lang/Object;)Z
        //   103: ifeq            109
        //   106: goto            1646
        //   109: ldc             "logo"
        //   111: aload_1        
        //   112: invokevirtual   java/lang/String.equals:(Ljava/lang/Object;)Z
        //   115: ifeq            284
        //   118: invokestatic    b/b/a/a/d/c.\u3007o\u3007:()Z
        //   121: ifne            176
        //   124: aload_0        
        //   125: invokestatic    android/text/TextUtils.isEmpty:(Ljava/lang/CharSequence;)Z
        //   128: ifne            141
        //   131: aload           6
        //   133: ldc             "adx:"
        //   135: invokevirtual   java/lang/String.contains:(Ljava/lang/CharSequence;)Z
        //   138: ifne            147
        //   141: invokestatic    b/b/a/a/d/d/i/j.\u300780\u3007808\u3007O:()Z
        //   144: ifeq            176
        //   147: invokestatic    b/b/a/a/d/d/i/j.\u300780\u3007808\u3007O:()Z
        //   150: ifeq            165
        //   153: aload           12
        //   155: aload           6
        //   157: aload_2        
        //   158: getstatic       b/b/a/a/d/d/i/j.\u3007o00\u3007\u3007Oo:Ljava/lang/String;
        //   161: invokestatic    b/b/a/a/d/d/i/j.\u3007080:(Lb/b/a/a/d/d/i/a$c;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lb/b/a/a/d/d/i/a$c;
        //   164: areturn        
        //   165: aload           12
        //   167: aload           6
        //   169: aload_2        
        //   170: ldc             ""
        //   172: invokestatic    b/b/a/a/d/d/i/j.\u3007080:(Lb/b/a/a/d/d/i/a$c;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lb/b/a/a/d/d/i/a$c;
        //   175: areturn        
        //   176: ldc             "union"
        //   178: aload           6
        //   180: invokevirtual   java/lang/String.equals:(Ljava/lang/Object;)Z
        //   183: ifeq            193
        //   186: ldc             10.0
        //   188: fstore          14
        //   190: goto            197
        //   193: ldc             20.0
        //   195: fstore          14
        //   197: aload           12
        //   199: fload           14
        //   201: putfield        b/b/a/a/d/d/i/a$c.\u3007080:F
        //   204: aload           12
        //   206: ldc             10.0
        //   208: putfield        b/b/a/a/d/d/i/a$c.\u3007o00\u3007\u3007Oo:F
        //   211: invokestatic    b/b/a/a/d/c.\u3007o\u3007:()Z
        //   214: ifeq            281
        //   217: new             Ljava/lang/StringBuilder;
        //   220: dup            
        //   221: invokespecial   java/lang/StringBuilder.<init>:()V
        //   224: astore_0       
        //   225: aload_0        
        //   226: aload_1        
        //   227: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   230: pop            
        //   231: aload_0        
        //   232: aload           6
        //   234: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   237: pop            
        //   238: aload_0        
        //   239: invokevirtual   java/lang/StringBuilder.toString:()Ljava/lang/String;
        //   242: astore_0       
        //   243: aload_2        
        //   244: invokestatic    b/b/a/a/d/d/i/j.oO80:(Ljava/lang/String;)D
        //   247: d2f            
        //   248: fstore          14
        //   250: aload_0        
        //   251: ldc             "logoad"
        //   253: invokevirtual   java/lang/String.contains:(Ljava/lang/CharSequence;)Z
        //   256: ifeq            274
        //   259: ldc             "AD"
        //   261: aload_2        
        //   262: invokestatic    b/b/a/a/d/d/i/j.\u3007o00\u3007\u3007Oo:(Ljava/lang/String;Ljava/lang/String;)Lb/b/a/a/d/d/i/a$c;
        //   265: astore_0       
        //   266: aload_0        
        //   267: fload           14
        //   269: putfield        b/b/a/a/d/d/i/a$c.\u3007o00\u3007\u3007Oo:F
        //   272: aload_0        
        //   273: areturn        
        //   274: aload           12
        //   276: fload           14
        //   278: putfield        b/b/a/a/d/d/i/a$c.\u3007o00\u3007\u3007Oo:F
        //   281: aload           12
        //   283: areturn        
        //   284: aload           6
        //   286: astore_0       
        //   287: ldc             "development-name"
        //   289: aload_1        
        //   290: invokevirtual   java/lang/String.equals:(Ljava/lang/Object;)Z
        //   293: ifeq            329
        //   296: new             Ljava/lang/StringBuilder;
        //   299: dup            
        //   300: invokespecial   java/lang/StringBuilder.<init>:()V
        //   303: astore_0       
        //   304: aload_0        
        //   305: invokestatic    b/b/a/a/d/c.\u3007080:()Landroid/content/Context;
        //   308: ldc             "tt_text_privacy_development"
        //   310: invokestatic    com/bytedance/sdk/component/utils/t.k:(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;
        //   313: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   316: pop            
        //   317: aload_0        
        //   318: aload           6
        //   320: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   323: pop            
        //   324: aload_0        
        //   325: invokevirtual   java/lang/StringBuilder.toString:()Ljava/lang/String;
        //   328: astore_0       
        //   329: aload_0        
        //   330: astore          6
        //   332: ldc             "app-version"
        //   334: aload_1        
        //   335: invokevirtual   java/lang/String.equals:(Ljava/lang/Object;)Z
        //   338: ifeq            378
        //   341: new             Ljava/lang/StringBuilder;
        //   344: dup            
        //   345: invokespecial   java/lang/StringBuilder.<init>:()V
        //   348: astore          6
        //   350: aload           6
        //   352: invokestatic    b/b/a/a/d/c.\u3007080:()Landroid/content/Context;
        //   355: ldc             "tt_text_privacy_app_version"
        //   357: invokestatic    com/bytedance/sdk/component/utils/t.k:(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;
        //   360: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   363: pop            
        //   364: aload           6
        //   366: aload_0        
        //   367: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   370: pop            
        //   371: aload           6
        //   373: invokevirtual   java/lang/StringBuilder.toString:()Ljava/lang/String;
        //   376: astore          6
        //   378: ldc             "score-count"
        //   380: aload_1        
        //   381: invokevirtual   java/lang/String.equals:(Ljava/lang/Object;)Z
        //   384: ifeq            483
        //   387: aload           6
        //   389: invokestatic    java/lang/Integer.parseInt:(Ljava/lang/String;)I
        //   392: istore          5
        //   394: goto            401
        //   397: astore_0       
        //   398: iconst_0       
        //   399: istore          5
        //   401: invokestatic    b/b/a/a/d/c.\u3007o\u3007:()Z
        //   404: ifeq            422
        //   407: iload           5
        //   409: ifge            422
        //   412: new             Lb/b/a/a/d/d/i/a$c;
        //   415: dup            
        //   416: fconst_0       
        //   417: fconst_0       
        //   418: invokespecial   b/b/a/a/d/d/i/a$c.<init>:(FF)V
        //   421: areturn        
        //   422: invokestatic    b/b/a/a/d/c.\u3007080:()Landroid/content/Context;
        //   425: ldc             "tt_comment_num"
        //   427: invokestatic    com/bytedance/sdk/component/utils/t.k:(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;
        //   430: iconst_1       
        //   431: anewarray       Ljava/lang/Object;
        //   434: dup            
        //   435: iconst_0       
        //   436: iload           5
        //   438: invokestatic    java/lang/Integer.valueOf:(I)Ljava/lang/Integer;
        //   441: aastore        
        //   442: invokestatic    java/lang/String.format:(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;
        //   445: astore_0       
        //   446: new             Ljava/lang/StringBuilder;
        //   449: dup            
        //   450: invokespecial   java/lang/StringBuilder.<init>:()V
        //   453: astore_1       
        //   454: aload_1        
        //   455: ldc             "("
        //   457: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   460: pop            
        //   461: aload_1        
        //   462: aload_0        
        //   463: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   466: pop            
        //   467: aload_1        
        //   468: ldc             ")"
        //   470: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   473: pop            
        //   474: aload_1        
        //   475: invokevirtual   java/lang/StringBuilder.toString:()Ljava/lang/String;
        //   478: aload_2        
        //   479: invokestatic    b/b/a/a/d/d/i/j.\u3007o00\u3007\u3007Oo:(Ljava/lang/String;Ljava/lang/String;)Lb/b/a/a/d/d/i/a$c;
        //   482: areturn        
        //   483: ldc             "score-count-type-2"
        //   485: aload_1        
        //   486: invokevirtual   java/lang/String.equals:(Ljava/lang/Object;)Z
        //   489: ifeq            595
        //   492: aload           6
        //   494: invokestatic    java/lang/Integer.parseInt:(Ljava/lang/String;)I
        //   497: istore          5
        //   499: goto            506
        //   502: astore_0       
        //   503: iconst_0       
        //   504: istore          5
        //   506: invokestatic    b/b/a/a/d/c.\u3007o\u3007:()Z
        //   509: ifeq            527
        //   512: iload           5
        //   514: ifge            527
        //   517: new             Lb/b/a/a/d/d/i/a$c;
        //   520: dup            
        //   521: fconst_0       
        //   522: fconst_0       
        //   523: invokespecial   b/b/a/a/d/d/i/a$c.<init>:(FF)V
        //   526: areturn        
        //   527: new             Ljava/text/DecimalFormat;
        //   530: dup            
        //   531: ldc             "###,###,###"
        //   533: invokespecial   java/text/DecimalFormat.<init>:(Ljava/lang/String;)V
        //   536: iload           5
        //   538: i2l            
        //   539: invokevirtual   java/text/NumberFormat.format:(J)Ljava/lang/String;
        //   542: iconst_1       
        //   543: anewarray       Ljava/lang/Object;
        //   546: dup            
        //   547: iconst_0       
        //   548: iload           5
        //   550: invokestatic    java/lang/Integer.valueOf:(I)Ljava/lang/Integer;
        //   553: aastore        
        //   554: invokestatic    java/lang/String.format:(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;
        //   557: astore_1       
        //   558: new             Ljava/lang/StringBuilder;
        //   561: dup            
        //   562: invokespecial   java/lang/StringBuilder.<init>:()V
        //   565: astore_0       
        //   566: aload_0        
        //   567: ldc             "("
        //   569: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   572: pop            
        //   573: aload_0        
        //   574: aload_1        
        //   575: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   578: pop            
        //   579: aload_0        
        //   580: ldc             ")"
        //   582: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   585: pop            
        //   586: aload_0        
        //   587: invokevirtual   java/lang/StringBuilder.toString:()Ljava/lang/String;
        //   590: aload_2        
        //   591: invokestatic    b/b/a/a/d/d/i/j.\u3007o00\u3007\u3007Oo:(Ljava/lang/String;Ljava/lang/String;)Lb/b/a/a/d/d/i/a$c;
        //   594: areturn        
        //   595: ldc             "feedback-dislike"
        //   597: aload_1        
        //   598: invokevirtual   java/lang/String.equals:(Ljava/lang/Object;)Z
        //   601: ifeq            633
        //   604: invokestatic    b/b/a/a/d/c.\u3007080:()Landroid/content/Context;
        //   607: ldc             "tt_reward_feedback"
        //   609: invokestatic    com/bytedance/sdk/component/utils/t.k:(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;
        //   612: aload_2        
        //   613: invokestatic    b/b/a/a/d/d/i/j.\u3007o00\u3007\u3007Oo:(Ljava/lang/String;Ljava/lang/String;)Lb/b/a/a/d/d/i/a$c;
        //   616: astore_0       
        //   617: invokestatic    b/b/a/a/d/c.\u3007o\u3007:()Z
        //   620: ifeq            631
        //   623: aload_0        
        //   624: aload_0        
        //   625: getfield        b/b/a/a/d/d/i/a$c.\u3007o00\u3007\u3007Oo:F
        //   628: putfield        b/b/a/a/d/d/i/a$c.\u3007080:F
        //   631: aload_0        
        //   632: areturn        
        //   633: ldc             "skip-with-time-countdown"
        //   635: aload_1        
        //   636: invokevirtual   java/lang/String.equals:(Ljava/lang/Object;)Z
        //   639: ifne            1536
        //   642: ldc             "skip-with-countdowns-video-countdown"
        //   644: aload_1        
        //   645: invokestatic    android/text/TextUtils.equals:(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z
        //   648: ifeq            654
        //   651: goto            1536
        //   654: ldc_w           "skip-with-countdowns-skip-btn"
        //   657: aload_1        
        //   658: invokestatic    android/text/TextUtils.equals:(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z
        //   661: ifeq            703
        //   664: new             Ljava/lang/StringBuilder;
        //   667: dup            
        //   668: invokespecial   java/lang/StringBuilder.<init>:()V
        //   671: astore_0       
        //   672: aload_0        
        //   673: ldc_w           "| "
        //   676: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   679: pop            
        //   680: aload_0        
        //   681: invokestatic    b/b/a/a/d/c.\u3007080:()Landroid/content/Context;
        //   684: ldc_w           "tt_reward_screen_skip_tx"
        //   687: invokestatic    com/bytedance/sdk/component/utils/t.k:(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;
        //   690: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   693: pop            
        //   694: aload_0        
        //   695: invokevirtual   java/lang/StringBuilder.toString:()Ljava/lang/String;
        //   698: aload_2        
        //   699: invokestatic    b/b/a/a/d/d/i/j.\u3007o00\u3007\u3007Oo:(Ljava/lang/String;Ljava/lang/String;)Lb/b/a/a/d/d/i/a$c;
        //   702: areturn        
        //   703: ldc_w           "skip-with-countdowns-skip-countdown"
        //   706: aload_1        
        //   707: invokestatic    android/text/TextUtils.equals:(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z
        //   710: ifeq            767
        //   713: invokestatic    b/b/a/a/d/c.\u3007080:()Landroid/content/Context;
        //   716: ldc_w           "tt_reward_full_skip_count_down"
        //   719: invokestatic    com/bytedance/sdk/component/utils/t.k:(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;
        //   722: iconst_1       
        //   723: anewarray       Ljava/lang/Object;
        //   726: dup            
        //   727: iconst_0       
        //   728: ldc_w           "00"
        //   731: aastore        
        //   732: invokestatic    java/lang/String.format:(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;
        //   735: astore_0       
        //   736: new             Ljava/lang/StringBuilder;
        //   739: dup            
        //   740: invokespecial   java/lang/StringBuilder.<init>:()V
        //   743: astore_1       
        //   744: aload_1        
        //   745: ldc_w           "| "
        //   748: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   751: pop            
        //   752: aload_1        
        //   753: aload_0        
        //   754: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   757: pop            
        //   758: aload_1        
        //   759: invokevirtual   java/lang/StringBuilder.toString:()Ljava/lang/String;
        //   762: aload_2        
        //   763: invokestatic    b/b/a/a/d/d/i/j.\u3007o00\u3007\u3007Oo:(Ljava/lang/String;Ljava/lang/String;)Lb/b/a/a/d/d/i/a$c;
        //   766: areturn        
        //   767: ldc_w           "skip-with-time-skip-btn"
        //   770: aload_1        
        //   771: invokevirtual   java/lang/String.equals:(Ljava/lang/Object;)Z
        //   774: ifeq            866
        //   777: new             Ljava/lang/StringBuilder;
        //   780: dup            
        //   781: invokespecial   java/lang/StringBuilder.<init>:()V
        //   784: astore_0       
        //   785: aload_0        
        //   786: ldc_w           "| "
        //   789: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   792: pop            
        //   793: aload_0        
        //   794: invokestatic    b/b/a/a/d/c.\u3007080:()Landroid/content/Context;
        //   797: ldc_w           "tt_reward_screen_skip_tx"
        //   800: invokestatic    com/bytedance/sdk/component/utils/t.k:(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;
        //   803: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   806: pop            
        //   807: aload_0        
        //   808: invokevirtual   java/lang/StringBuilder.toString:()Ljava/lang/String;
        //   811: aload_2        
        //   812: invokestatic    b/b/a/a/d/d/i/j.\u3007o00\u3007\u3007Oo:(Ljava/lang/String;Ljava/lang/String;)Lb/b/a/a/d/d/i/a$c;
        //   815: astore_0       
        //   816: invokestatic    b/b/a/a/d/c.\u3007o\u3007:()Z
        //   819: ifeq            864
        //   822: new             Lorg/json/JSONObject;
        //   825: astore_1       
        //   826: aload_1        
        //   827: aload_2        
        //   828: invokespecial   org/json/JSONObject.<init>:(Ljava/lang/String;)V
        //   831: aload_1        
        //   832: ldc             "lineHeight"
        //   834: invokevirtual   org/json/JSONObject.optDouble:(Ljava/lang/String;)D
        //   837: dstore          7
        //   839: aload_0        
        //   840: aload_0        
        //   841: getfield        b/b/a/a/d/d/i/a$c.\u3007o00\u3007\u3007Oo:F
        //   844: f2d            
        //   845: dload           7
        //   847: dmul           
        //   848: ldc2_w          1.2
        //   851: ddiv           
        //   852: d2f            
        //   853: putfield        b/b/a/a/d/d/i/a$c.\u3007o00\u3007\u3007Oo:F
        //   856: aload_0        
        //   857: aload_0        
        //   858: getfield        b/b/a/a/d/d/i/a$c.\u3007o00\u3007\u3007Oo:F
        //   861: putfield        b/b/a/a/d/d/i/a$c.\u3007080:F
        //   864: aload_0        
        //   865: areturn        
        //   866: ldc_w           "skip"
        //   869: aload_1        
        //   870: invokevirtual   java/lang/String.equals:(Ljava/lang/Object;)Z
        //   873: ifeq            890
        //   876: invokestatic    b/b/a/a/d/c.\u3007080:()Landroid/content/Context;
        //   879: ldc_w           "tt_reward_screen_skip_tx"
        //   882: invokestatic    com/bytedance/sdk/component/utils/t.k:(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;
        //   885: aload_2        
        //   886: invokestatic    b/b/a/a/d/d/i/j.\u3007o00\u3007\u3007Oo:(Ljava/lang/String;Ljava/lang/String;)Lb/b/a/a/d/d/i/a$c;
        //   889: areturn        
        //   890: ldc_w           "timedown"
        //   893: aload_1        
        //   894: invokevirtual   java/lang/String.equals:(Ljava/lang/Object;)Z
        //   897: ifeq            908
        //   900: ldc_w           "0.0"
        //   903: aload_2        
        //   904: invokestatic    b/b/a/a/d/d/i/j.\u3007o00\u3007\u3007Oo:(Ljava/lang/String;Ljava/lang/String;)Lb/b/a/a/d/d/i/a$c;
        //   907: areturn        
        //   908: ldc             "text_star"
        //   910: aload_1        
        //   911: invokevirtual   java/lang/String.equals:(Ljava/lang/Object;)Z
        //   914: ifeq            957
        //   917: invokestatic    b/b/a/a/d/c.\u3007o\u3007:()Z
        //   920: ifeq            949
        //   923: dload           10
        //   925: dconst_0       
        //   926: dcmpg          
        //   927: iflt            939
        //   930: dload           10
        //   932: ldc2_w          5.0
        //   935: dcmpl          
        //   936: ifle            949
        //   939: new             Lb/b/a/a/d/d/i/a$c;
        //   942: dup            
        //   943: fconst_0       
        //   944: fconst_0       
        //   945: invokespecial   b/b/a/a/d/d/i/a$c.<init>:(FF)V
        //   948: areturn        
        //   949: ldc_w           "0.0"
        //   952: aload_2        
        //   953: invokestatic    b/b/a/a/d/d/i/j.\u3007o00\u3007\u3007Oo:(Ljava/lang/String;Ljava/lang/String;)Lb/b/a/a/d/d/i/a$c;
        //   956: areturn        
        //   957: ldc_w           "privacy-detail"
        //   960: aload_1        
        //   961: invokestatic    android/text/TextUtils.equals:(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z
        //   964: ifeq            975
        //   967: ldc_w           "Permission list | Privacy policy"
        //   970: aload_2        
        //   971: invokestatic    b/b/a/a/d/d/i/j.\u3007o00\u3007\u3007Oo:(Ljava/lang/String;Ljava/lang/String;)Lb/b/a/a/d/d/i/a$c;
        //   974: areturn        
        //   975: ldc_w           "arrowButton"
        //   978: aload_1        
        //   979: invokevirtual   java/lang/String.equals:(Ljava/lang/Object;)Z
        //   982: ifeq            993
        //   985: ldc_w           "Download"
        //   988: aload_2        
        //   989: invokestatic    b/b/a/a/d/d/i/j.\u3007o00\u3007\u3007Oo:(Ljava/lang/String;Ljava/lang/String;)Lb/b/a/a/d/d/i/a$c;
        //   992: areturn        
        //   993: ldc_w           "title"
        //   996: aload_1        
        //   997: invokevirtual   java/lang/String.equals:(Ljava/lang/Object;)Z
        //  1000: ifeq            1021
        //  1003: aload           6
        //  1005: bipush          10
        //  1007: bipush          32
        //  1009: invokevirtual   java/lang/String.replace:(CC)Ljava/lang/String;
        //  1012: aload_2        
        //  1013: iconst_1       
        //  1014: invokestatic    b/b/a/a/d/d/i/j.O8:(Ljava/lang/String;Ljava/lang/String;Z)Lb/b/a/a/d/d/i/a$c;
        //  1017: astore_0       
        //  1018: aload_0        
        //  1019: areturn        
        //  1020: astore_0       
        //  1021: ldc_w           "fillButton"
        //  1024: aload_1        
        //  1025: invokevirtual   java/lang/String.equals:(Ljava/lang/Object;)Z
        //  1028: ifne            1529
        //  1031: ldc_w           "text"
        //  1034: aload_1        
        //  1035: invokevirtual   java/lang/String.equals:(Ljava/lang/Object;)Z
        //  1038: ifne            1529
        //  1041: ldc_w           "button"
        //  1044: aload_1        
        //  1045: invokevirtual   java/lang/String.equals:(Ljava/lang/Object;)Z
        //  1048: ifne            1529
        //  1051: ldc_w           "downloadWithIcon"
        //  1054: aload_1        
        //  1055: invokevirtual   java/lang/String.equals:(Ljava/lang/Object;)Z
        //  1058: ifne            1529
        //  1061: ldc_w           "downloadButton"
        //  1064: aload_1        
        //  1065: invokevirtual   java/lang/String.equals:(Ljava/lang/Object;)Z
        //  1068: ifne            1529
        //  1071: ldc_w           "laceButton"
        //  1074: aload_1        
        //  1075: invokevirtual   java/lang/String.equals:(Ljava/lang/Object;)Z
        //  1078: ifne            1529
        //  1081: ldc_w           "cardButton"
        //  1084: aload_1        
        //  1085: invokevirtual   java/lang/String.equals:(Ljava/lang/Object;)Z
        //  1088: ifne            1529
        //  1091: ldc_w           "colourMixtureButton"
        //  1094: aload_1        
        //  1095: invokevirtual   java/lang/String.equals:(Ljava/lang/Object;)Z
        //  1098: ifne            1529
        //  1101: ldc_w           "arrowButton"
        //  1104: aload_1        
        //  1105: invokevirtual   java/lang/String.equals:(Ljava/lang/Object;)Z
        //  1108: ifne            1529
        //  1111: ldc_w           "source"
        //  1114: aload_1        
        //  1115: invokevirtual   java/lang/String.equals:(Ljava/lang/Object;)Z
        //  1118: ifne            1529
        //  1121: ldc             "app-version"
        //  1123: aload_1        
        //  1124: invokestatic    android/text/TextUtils.equals:(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z
        //  1127: ifne            1529
        //  1130: ldc             "development-name"
        //  1132: aload_1        
        //  1133: invokestatic    android/text/TextUtils.equals:(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z
        //  1136: ifeq            1142
        //  1139: goto            1529
        //  1142: new             Lorg/json/JSONObject;
        //  1145: astore_0       
        //  1146: aload_0        
        //  1147: aload_2        
        //  1148: invokespecial   org/json/JSONObject.<init>:(Ljava/lang/String;)V
        //  1151: aload           6
        //  1153: invokevirtual   java/lang/String.length:()I
        //  1156: istore          19
        //  1158: aload_0        
        //  1159: ldc             "fontSize"
        //  1161: invokevirtual   org/json/JSONObject.optDouble:(Ljava/lang/String;)D
        //  1164: d2f            
        //  1165: fstore          18
        //  1167: aload_0        
        //  1168: ldc_w           "letterSpacing"
        //  1171: invokevirtual   org/json/JSONObject.optDouble:(Ljava/lang/String;)D
        //  1174: d2f            
        //  1175: fstore          16
        //  1177: aload_0        
        //  1178: ldc             "lineHeight"
        //  1180: invokevirtual   org/json/JSONObject.optDouble:(Ljava/lang/String;)D
        //  1183: d2f            
        //  1184: fstore          17
        //  1186: aload_0        
        //  1187: ldc_w           "maxWidth"
        //  1190: invokevirtual   org/json/JSONObject.optDouble:(Ljava/lang/String;)D
        //  1193: dstore          7
        //  1195: dload           7
        //  1197: d2f            
        //  1198: fstore          14
        //  1200: iload           19
        //  1202: i2f            
        //  1203: fload           18
        //  1205: fload           16
        //  1207: fadd           
        //  1208: fmul           
        //  1209: fload           16
        //  1211: fsub           
        //  1212: fstore          15
        //  1214: new             Ljava/lang/StringBuilder;
        //  1217: astore_0       
        //  1218: aload_0        
        //  1219: invokespecial   java/lang/StringBuilder.<init>:()V
        //  1222: aload_0        
        //  1223: ldc_w           "getDomSizeFromNative letterSpacing=="
        //  1226: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //  1229: pop            
        //  1230: aload_0        
        //  1231: fload           16
        //  1233: invokevirtual   java/lang/StringBuilder.append:(F)Ljava/lang/StringBuilder;
        //  1236: pop            
        //  1237: aload_0        
        //  1238: ldc_w           ",lineHeight=="
        //  1241: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //  1244: pop            
        //  1245: aload_0        
        //  1246: fload           17
        //  1248: invokevirtual   java/lang/StringBuilder.append:(F)Ljava/lang/StringBuilder;
        //  1251: pop            
        //  1252: aload_0        
        //  1253: ldc_w           ",maxWidth =="
        //  1256: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //  1259: pop            
        //  1260: aload_0        
        //  1261: fload           14
        //  1263: invokevirtual   java/lang/StringBuilder.append:(F)Ljava/lang/StringBuilder;
        //  1266: pop            
        //  1267: aload_0        
        //  1268: ldc_w           ",totalStrLength"
        //  1271: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //  1274: pop            
        //  1275: aload_0        
        //  1276: fload           15
        //  1278: invokevirtual   java/lang/StringBuilder.append:(F)Ljava/lang/StringBuilder;
        //  1281: pop            
        //  1282: ldc_w           "DynamicBaseWidget"
        //  1285: aload_0        
        //  1286: invokevirtual   java/lang/StringBuilder.toString:()Ljava/lang/String;
        //  1289: invokestatic    com/bytedance/sdk/component/utils/m.b:(Ljava/lang/String;Ljava/lang/String;)V
        //  1292: ldc_w           "muted"
        //  1295: aload_1        
        //  1296: invokevirtual   java/lang/String.equals:(Ljava/lang/Object;)Z
        //  1299: ifeq            1319
        //  1302: aload           12
        //  1304: fload           18
        //  1306: putfield        b/b/a/a/d/d/i/a$c.\u3007080:F
        //  1309: aload           12
        //  1311: fload           18
        //  1313: putfield        b/b/a/a/d/d/i/a$c.\u3007o00\u3007\u3007Oo:F
        //  1316: aload           12
        //  1318: areturn        
        //  1319: ldc_w           "star"
        //  1322: aload_1        
        //  1323: invokevirtual   java/lang/String.equals:(Ljava/lang/Object;)Z
        //  1326: ifeq            1387
        //  1329: invokestatic    b/b/a/a/d/c.\u3007o\u3007:()Z
        //  1332: ifeq            1367
        //  1335: dload           10
        //  1337: dconst_0       
        //  1338: dcmpg          
        //  1339: iflt            1357
        //  1342: dload           10
        //  1344: ldc2_w          5.0
        //  1347: dcmpl          
        //  1348: ifgt            1357
        //  1351: iload           9
        //  1353: iconst_4       
        //  1354: if_icmpeq       1367
        //  1357: new             Lb/b/a/a/d/d/i/a$c;
        //  1360: dup            
        //  1361: fconst_0       
        //  1362: fconst_0       
        //  1363: invokespecial   b/b/a/a/d/d/i/a$c.<init>:(FF)V
        //  1366: areturn        
        //  1367: ldc_w           "str"
        //  1370: aload_2        
        //  1371: invokestatic    b/b/a/a/d/d/i/j.\u3007o00\u3007\u3007Oo:(Ljava/lang/String;Ljava/lang/String;)Lb/b/a/a/d/d/i/a$c;
        //  1374: astore_0       
        //  1375: aload_0        
        //  1376: fload           18
        //  1378: ldc_w           5.0
        //  1381: fmul           
        //  1382: putfield        b/b/a/a/d/d/i/a$c.\u3007080:F
        //  1385: aload_0        
        //  1386: areturn        
        //  1387: ldc_w           "icon"
        //  1390: aload_1        
        //  1391: invokevirtual   java/lang/String.equals:(Ljava/lang/Object;)Z
        //  1394: ifeq            1414
        //  1397: aload           12
        //  1399: fload           18
        //  1401: putfield        b/b/a/a/d/d/i/a$c.\u3007080:F
        //  1404: aload           12
        //  1406: fload           18
        //  1408: putfield        b/b/a/a/d/d/i/a$c.\u3007o00\u3007\u3007Oo:F
        //  1411: aload           12
        //  1413: areturn        
        //  1414: iload_3        
        //  1415: ifeq            1472
        //  1418: fload           15
        //  1420: fload           14
        //  1422: fdiv           
        //  1423: f2i            
        //  1424: iconst_1       
        //  1425: iadd           
        //  1426: istore          19
        //  1428: iload           19
        //  1430: istore          9
        //  1432: iload           4
        //  1434: ifeq            1452
        //  1437: iload           19
        //  1439: istore          9
        //  1441: iload           19
        //  1443: iload           5
        //  1445: if_icmplt       1452
        //  1448: iload           5
        //  1450: istore          9
        //  1452: fload           17
        //  1454: fload           18
        //  1456: fmul           
        //  1457: iload           9
        //  1459: i2f            
        //  1460: fmul           
        //  1461: f2d            
        //  1462: ldc2_w          1.2
        //  1465: dmul           
        //  1466: d2f            
        //  1467: fstore          15
        //  1469: goto            1504
        //  1472: fload           17
        //  1474: fload           18
        //  1476: fmul           
        //  1477: f2d            
        //  1478: ldc2_w          1.2
        //  1481: dmul           
        //  1482: d2f            
        //  1483: fstore          16
        //  1485: fload           15
        //  1487: fload           14
        //  1489: fcmpl          
        //  1490: ifle            1496
        //  1493: goto            1500
        //  1496: fload           15
        //  1498: fstore          14
        //  1500: fload           16
        //  1502: fstore          15
        //  1504: aload           12
        //  1506: fload           14
        //  1508: putfield        b/b/a/a/d/d/i/a$c.\u3007080:F
        //  1511: aload           12
        //  1513: fload           15
        //  1515: putfield        b/b/a/a/d/d/i/a$c.\u3007o00\u3007\u3007Oo:F
        //  1518: goto            1526
        //  1521: astore_0       
        //  1522: aload_0        
        //  1523: invokevirtual   java/lang/Throwable.printStackTrace:()V
        //  1526: aload           12
        //  1528: areturn        
        //  1529: aload           6
        //  1531: aload_2        
        //  1532: invokestatic    b/b/a/a/d/d/i/j.\u3007o00\u3007\u3007Oo:(Ljava/lang/String;Ljava/lang/String;)Lb/b/a/a/d/d/i/a$c;
        //  1535: areturn        
        //  1536: aload           13
        //  1538: invokevirtual   b/b/a/a/d/f/m.\u3007o00\u3007\u3007Oo:()Z
        //  1541: ifeq            1621
        //  1544: aload           20
        //  1546: invokestatic    b/b/a/a/d/g/c.\u3007080:(Ljava/lang/String;)Z
        //  1549: ifeq            1621
        //  1552: dload           7
        //  1554: ldc2_w          0.5
        //  1557: dadd           
        //  1558: d2i            
        //  1559: iload           19
        //  1561: isub           
        //  1562: bipush          10
        //  1564: if_icmpge       1594
        //  1567: invokestatic    b/b/a/a/d/c.\u3007080:()Landroid/content/Context;
        //  1570: ldc_w           "tt_reward_full_skip"
        //  1573: invokestatic    com/bytedance/sdk/component/utils/t.k:(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;
        //  1576: iconst_1       
        //  1577: anewarray       Ljava/lang/Object;
        //  1580: dup            
        //  1581: iconst_0       
        //  1582: ldc_w           "0"
        //  1585: aastore        
        //  1586: invokestatic    java/lang/String.format:(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;
        //  1589: aload_2        
        //  1590: invokestatic    b/b/a/a/d/d/i/j.\u3007o00\u3007\u3007Oo:(Ljava/lang/String;Ljava/lang/String;)Lb/b/a/a/d/d/i/a$c;
        //  1593: areturn        
        //  1594: invokestatic    b/b/a/a/d/c.\u3007080:()Landroid/content/Context;
        //  1597: ldc_w           "tt_reward_full_skip"
        //  1600: invokestatic    com/bytedance/sdk/component/utils/t.k:(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;
        //  1603: iconst_1       
        //  1604: anewarray       Ljava/lang/Object;
        //  1607: dup            
        //  1608: iconst_0       
        //  1609: ldc_w           "00"
        //  1612: aastore        
        //  1613: invokestatic    java/lang/String.format:(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;
        //  1616: aload_2        
        //  1617: invokestatic    b/b/a/a/d/d/i/j.\u3007o00\u3007\u3007Oo:(Ljava/lang/String;Ljava/lang/String;)Lb/b/a/a/d/d/i/a$c;
        //  1620: areturn        
        //  1621: dload           7
        //  1623: ldc2_w          10.0
        //  1626: dcmpg          
        //  1627: ifge            1638
        //  1630: ldc_w           "0S"
        //  1633: aload_2        
        //  1634: invokestatic    b/b/a/a/d/d/i/j.\u3007o00\u3007\u3007Oo:(Ljava/lang/String;Ljava/lang/String;)Lb/b/a/a/d/d/i/a$c;
        //  1637: areturn        
        //  1638: ldc_w           "00S"
        //  1641: aload_2        
        //  1642: invokestatic    b/b/a/a/d/d/i/j.\u3007o00\u3007\u3007Oo:(Ljava/lang/String;Ljava/lang/String;)Lb/b/a/a/d/d/i/a$c;
        //  1645: areturn        
        //  1646: ldc             "close"
        //  1648: aload_1        
        //  1649: invokevirtual   java/lang/String.equals:(Ljava/lang/Object;)Z
        //  1652: ifeq            1690
        //  1655: new             Lorg/json/JSONObject;
        //  1658: astore_0       
        //  1659: aload_0        
        //  1660: aload_2        
        //  1661: invokespecial   org/json/JSONObject.<init>:(Ljava/lang/String;)V
        //  1664: aload_0        
        //  1665: ldc             "fontSize"
        //  1667: invokevirtual   org/json/JSONObject.optDouble:(Ljava/lang/String;)D
        //  1670: d2f            
        //  1671: fstore          14
        //  1673: aload           12
        //  1675: fload           14
        //  1677: putfield        b/b/a/a/d/d/i/a$c.\u3007080:F
        //  1680: aload           12
        //  1682: fload           14
        //  1684: putfield        b/b/a/a/d/d/i/a$c.\u3007o00\u3007\u3007Oo:F
        //  1687: aload           12
        //  1689: areturn        
        //  1690: aload           12
        //  1692: ldc             10.0
        //  1694: putfield        b/b/a/a/d/d/i/a$c.\u3007080:F
        //  1697: aload           12
        //  1699: ldc             10.0
        //  1701: putfield        b/b/a/a/d/d/i/a$c.\u3007o00\u3007\u3007Oo:F
        //  1704: aload           12
        //  1706: areturn        
        //  1707: astore_1       
        //  1708: goto            856
        //  1711: astore_0       
        //  1712: goto            1690
        //    Exceptions:
        //  Try           Handler
        //  Start  End    Start  End    Type                             
        //  -----  -----  -----  -----  ---------------------------------
        //  387    394    397    401    Ljava/lang/NumberFormatException;
        //  492    499    502    506    Ljava/lang/NumberFormatException;
        //  822    856    1707   1711   Any
        //  1003   1018   1020   1021   Ljava/lang/Exception;
        //  1142   1195   1521   1526   Lorg/json/JSONException;
        //  1214   1316   1521   1526   Lorg/json/JSONException;
        //  1319   1335   1521   1526   Lorg/json/JSONException;
        //  1357   1367   1521   1526   Lorg/json/JSONException;
        //  1367   1385   1521   1526   Lorg/json/JSONException;
        //  1387   1411   1521   1526   Lorg/json/JSONException;
        //  1504   1518   1521   1526   Lorg/json/JSONException;
        //  1646   1687   1711   1715   Ljava/lang/Exception;
        // 
        // The error that occurred was:
        // 
        // java.lang.IllegalStateException: Expression is linked from several locations: Label_1646:
        //     at com.strobel.decompiler.ast.Error.expressionLinkedFromMultipleLocations(Error.java:27)
        //     at com.strobel.decompiler.ast.AstOptimizer.mergeDisparateObjectInitializations(AstOptimizer.java:2604)
        //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:235)
        //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:42)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:206)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:93)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethodBody(AstBuilder.java:868)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethod(AstBuilder.java:761)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addTypeMembers(AstBuilder.java:638)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeCore(AstBuilder.java:605)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeNoCache(AstBuilder.java:195)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createType(AstBuilder.java:162)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addType(AstBuilder.java:137)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.buildAst(JavaLanguage.java:71)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.decompileType(JavaLanguage.java:59)
        //     at com.strobel.decompiler.DecompilerDriver.decompileType(DecompilerDriver.java:333)
        //     at com.strobel.decompiler.DecompilerDriver.decompileJar(DecompilerDriver.java:254)
        //     at com.strobel.decompiler.DecompilerDriver.main(DecompilerDriver.java:144)
        // 
        throw new IllegalStateException("An error occurred while decompiling this method.");
    }
    
    public static int[] \u3007\u3007888(final String text, final float textSize, final boolean b) {
        try {
            final TextView textView = new TextView(c.\u3007080());
            textView.setTextSize(textSize);
            textView.setText((CharSequence)text);
            textView.setIncludeFontPadding(false);
            if (b) {
                textView.setSingleLine();
            }
            ((View)textView).measure(-2, -2);
            return new int[] { ((View)textView).getMeasuredWidth() + 2, ((View)textView).getMeasuredHeight() + 2 };
        }
        catch (final Exception ex) {
            return new int[] { 0, 0 };
        }
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package b.b.a.a.d.d.g;

import android.view.ViewGroup$LayoutParams;
import android.widget.FrameLayout$LayoutParams;
import b.b.a.a.d.f.n;
import com.bytedance.sdk.component.utils.i;
import java.util.concurrent.TimeUnit;
import java.util.Map;
import android.os.Handler;
import android.os.Looper;
import b.b.a.a.d.d.j.b;
import java.util.Collections;
import java.util.Comparator;
import b.b.a.a.d.d.i.f;
import java.util.Iterator;
import java.util.List;
import com.bytedance.sdk.component.adexpress.dynamic.dynamicview.e;
import android.view.ViewGroup;
import android.view.View;
import com.bytedance.sdk.component.adexpress.theme.ThemeStatusBroadcastReceiver;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.ScheduledFuture;
import b.b.a.a.d.f.m;
import b.b.a.a.d.f.h;
import android.content.Context;
import b.b.a.a.d.d.i.g;
import b.b.a.a.d.f.k;
import com.bytedance.sdk.component.adexpress.dynamic.dynamicview.DynamicRootView;
import b.b.a.a.d.f.d;

public class a implements b.b.a.a.d.f.d<DynamicRootView>, k
{
    private DynamicRootView a;
    private g b;
    private Context c;
    private b.b.a.a.d.f.g d;
    private h e;
    private m f;
    private ScheduledFuture<?> g;
    private AtomicBoolean h;
    
    public a(final Context c, final ThemeStatusBroadcastReceiver themeStatusBroadcastReceiver, final boolean b, final g b2, final m m, final b.b.a.a.d.d.j.a a) {
        this.h = new AtomicBoolean(false);
        this.c = c;
        final DynamicRootView a2 = new DynamicRootView(c, themeStatusBroadcastReceiver, b, m, a);
        this.a = a2;
        this.b = b2;
        this.f = m;
        a2.setRenderListener((k)this);
        this.f = m;
    }
    
    private void a() {
        try {
            final ScheduledFuture<?> g = this.g;
            if (g != null && !g.isCancelled()) {
                this.g.cancel(false);
                this.g = null;
            }
            com.bytedance.sdk.component.utils.m.a("DynamicRender", "WebView Render cancel timeout timer");
        }
        finally {
            final Throwable t;
            t.printStackTrace();
        }
    }
    
    private void a(final View view) {
        if (view == null) {
            return;
        }
        if (view instanceof ViewGroup) {
            int n = 0;
            while (true) {
                final ViewGroup viewGroup = (ViewGroup)view;
                if (n >= viewGroup.getChildCount()) {
                    break;
                }
                this.a(viewGroup.getChildAt(n));
                ++n;
            }
        }
        if (view instanceof e) {
            ((e)view).b();
        }
    }
    
    private void a(final b.b.a.a.d.d.h.h h) {
        if (h == null) {
            return;
        }
        final List<b.b.a.a.d.d.h.h> \u3007\u30078O0\u30078 = h.\u3007\u30078O0\u30078();
        float n2;
        if (\u3007\u30078O0\u30078 != null && \u3007\u30078O0\u30078.size() > 0) {
            final Iterator iterator = \u3007\u30078O0\u30078.iterator();
            float n = 0.0f;
            while (true) {
                n2 = n;
                if (!iterator.hasNext()) {
                    break;
                }
                final b.b.a.a.d.d.h.h h2 = (b.b.a.a.d.d.h.h)iterator.next();
                float n4 = 0.0f;
                float n5 = 0.0f;
                Label_0203: {
                    if (h2.\u300700\u30078() <= h.\u300700\u30078() - h2.OoO8()) {
                        final List<b.b.a.a.d.d.h.h> \u3007\u30078O0\u30079 = h2.\u3007\u30078O0\u30078();
                        if (\u3007\u30078O0\u30079 != null && \u3007\u30078O0\u30079.size() > 0) {
                            final Iterator iterator2 = \u3007\u30078O0\u30079.iterator();
                            float n3 = 0.0f;
                            while (true) {
                                n4 = n;
                                n5 = n3;
                                if (!iterator2.hasNext()) {
                                    break Label_0203;
                                }
                                final b.b.a.a.d.d.h.h h3 = (b.b.a.a.d.d.h.h)iterator2.next();
                                if (!h3.\u30070000OOO().\u30078o8o\u3007().equals("logo-union")) {
                                    continue;
                                }
                                n3 = (float)h3.\u30070000OOO().OO0o\u3007\u3007\u3007\u30070();
                                n = -n3 + h.\u300700\u30078() - h2.\u300700\u30078() + h2.\u30070000OOO().\u3007O8o08O().\u3007oO8O0\u3007\u3007O();
                            }
                        }
                    }
                    n5 = 0.0f;
                    n4 = n;
                }
                this.a(h2);
                n = n4;
                if (n5 > -15.0f) {
                    continue;
                }
                h2.\u3007o\u3007(h2.OoO8() - n5);
                h2.oo88o8O(h2.\u300700\u30078() + n5);
                final Iterator<b.b.a.a.d.d.h.h> iterator3 = h2.\u3007\u30078O0\u30078().iterator();
                while (true) {
                    n = n4;
                    if (!iterator3.hasNext()) {
                        break;
                    }
                    final b.b.a.a.d.d.h.h h4 = iterator3.next();
                    h4.oo88o8O(h4.\u300700\u30078() - n5);
                }
            }
        }
        else {
            n2 = 0.0f;
        }
        final b.b.a.a.d.d.h.h o8ooOoo\u3007 = h.O8ooOoo\u3007();
        if (o8ooOoo\u3007 == null) {
            return;
        }
        final float o8\u3007o = h.O8\u3007o();
        final float o8\u3007o2 = o8ooOoo\u3007.O8\u3007o();
        final float \u300700\u30078 = h.\u300700\u30078();
        final float \u300700\u30079 = o8ooOoo\u3007.\u300700\u30078();
        h.o800o8O(o8\u3007o - o8\u3007o2);
        h.oo88o8O(\u300700\u30078 - \u300700\u30079);
        if (n2 > 0.0f) {
            h.oo88o8O(h.\u300700\u30078() - n2);
            h.\u3007o\u3007(h.OoO8() + n2);
            for (final b.b.a.a.d.d.h.h h5 : h.\u3007\u30078O0\u30078()) {
                h5.oo88o8O(h5.\u300700\u30078() + n2);
            }
        }
    }
    
    private void b(final b.b.a.a.d.d.h.h h) {
        if (h == null) {
            int n;
            if (this.b instanceof f) {
                n = 123;
            }
            else {
                n = 113;
            }
            this.a.b(n);
            return;
        }
        this.f.\u3007\u3007808\u3007().b(this.b());
        try {
            this.a.a(h, this.b());
        }
        catch (final Exception ex) {
            int n2;
            if (this.b instanceof f) {
                n2 = 128;
            }
            else {
                n2 = 118;
            }
            this.a.b(n2);
        }
    }
    
    private void c(final b.b.a.a.d.d.h.h h) {
        if (h == null) {
            return;
        }
        final List<b.b.a.a.d.d.h.h> \u3007\u30078O0\u30078 = h.\u3007\u30078O0\u30078();
        if (\u3007\u30078O0\u30078 != null) {
            if (\u3007\u30078O0\u30078.size() > 0) {
                Collections.sort((List<Object>)\u3007\u30078O0\u30078, (Comparator<? super Object>)new Comparator<b.b.a.a.d.d.h.h>(this) {
                    public int \u3007080(final b.b.a.a.d.d.h.h h, final b.b.a.a.d.d.h.h h2) {
                        final b.b.a.a.d.d.h.f \u3007o8o08O = h.\u30070000OOO().\u3007O8o08O();
                        final b.b.a.a.d.d.h.f \u3007o8o08O2 = h2.\u30070000OOO().\u3007O8o08O();
                        if (\u3007o8o08O == null || \u3007o8o08O2 == null) {
                            return 0;
                        }
                        if (\u3007o8o08O.o88O\u30078() >= \u3007o8o08O2.o88O\u30078()) {
                            return 1;
                        }
                        return -1;
                    }
                });
                for (final b.b.a.a.d.d.h.h h2 : \u3007\u30078O0\u30078) {
                    if (h2 == null) {
                        continue;
                    }
                    this.c(h2);
                }
            }
        }
    }
    
    private boolean f() {
        final DynamicRootView a = this.a;
        return a != null && ((ViewGroup)a).getChildCount() != 0;
    }
    
    private void h() {
        this.f.\u3007\u3007808\u3007().a(this.b());
        if (!b.b.a.a.d.e.b.a.o\u30070(this.f.\u3007O00())) {
            int n;
            if (this.b instanceof f) {
                n = 123;
            }
            else {
                n = 113;
            }
            this.a.b(n);
            return;
        }
        this.b.\u3007o00\u3007\u3007Oo(new b(this) {
            final a \u3007080;
            
            @Override
            public void \u3007080(final b.b.a.a.d.d.h.h h) {
                this.\u3007080.a();
                this.\u3007080.f.\u3007\u3007808\u3007().g(this.\u3007080.b());
                this.\u3007080.c(h);
                this.\u3007080.a(h);
                new Handler(Looper.getMainLooper()).post((Runnable)new Runnable(this, h) {
                    final b.b.a.a.d.d.h.h o0;
                    final a$b \u3007OOo8\u30070;
                    
                    @Override
                    public void run() {
                        this.\u3007OOo8\u30070.\u3007080.b(this.o0);
                    }
                });
                if (this.\u3007080.a != null && h != null) {
                    this.\u3007080.a.setBgColor(h.\u300780\u3007808\u3007O());
                    this.\u3007080.a.setBgMaterialCenterCalcColor((Map)h.OO0o\u3007\u3007());
                }
            }
        });
        this.b.\u3007080(this.f);
    }
    
    @Override
    public void a(final View view, final int n, final b.b.a.a.d.b b) {
        final h e = this.e;
        if (e != null) {
            e.a(view, n, b);
        }
    }
    
    @Override
    public void a(final b.b.a.a.d.f.g d) {
        this.d = d;
        final int \u3007\u30078O0\u30078 = this.f.\u3007\u30078O0\u30078();
        if (\u3007\u30078O0\u30078 < 0) {
            int n;
            if (this.b instanceof f) {
                n = 127;
            }
            else {
                n = 117;
            }
            this.a.b(n);
        }
        else {
            this.g = b.b.a.a.k.e.\u3007O888o0o().schedule(new d(2), \u3007\u30078O0\u30078, TimeUnit.MILLISECONDS);
            i.b().postDelayed((Runnable)new Runnable(this) {
                final a o0;
                
                @Override
                public void run() {
                    this.o0.h();
                }
            }, this.f.\u3007o\u3007());
        }
    }
    
    public void a(final h e) {
        this.e = e;
    }
    
    @Override
    public void a(final n n) {
        if (this.h.get()) {
            return;
        }
        this.h.set(true);
        if (n.\u30070000OOO() && this.f()) {
            ((View)this.a).setLayoutParams((ViewGroup$LayoutParams)new FrameLayout$LayoutParams(-1, -1));
            this.d.a((View)this.d(), n);
        }
        else {
            this.d.a(n.OO0o\u3007\u3007());
        }
    }
    
    @Override
    public int b() {
        if (this.b instanceof f) {
            return 3;
        }
        return 2;
    }
    
    public DynamicRootView c() {
        return this.a;
    }
    
    public DynamicRootView d() {
        return this.c();
    }
    
    public void g() {
        this.a((View)this.d());
    }
    
    private class d implements Runnable
    {
        private int o0;
        final a \u3007OOo8\u30070;
        
        public d(final a \u3007oOo8\u30070, final int o0) {
            this.\u3007OOo8\u30070 = \u3007oOo8\u30070;
            this.o0 = o0;
        }
        
        @Override
        public void run() {
            if (this.o0 == 2) {
                com.bytedance.sdk.component.utils.m.a("DynamicRender", "Dynamic parse time out");
                int n;
                if (this.\u3007OOo8\u30070.b instanceof f) {
                    n = 127;
                }
                else {
                    n = 117;
                }
                this.\u3007OOo8\u30070.a.b(n);
            }
        }
    }
}

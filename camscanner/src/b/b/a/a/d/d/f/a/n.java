// 
// Decompiled by Procyon v0.6.0
// 

package b.b.a.a.d.d.f.a;

import java.util.ArrayList;
import b.b.a.a.d.g.b;
import b.b.a.a.d.c;
import android.animation.ObjectAnimator;
import java.util.List;
import b.b.a.a.d.d.h.a;
import android.view.View;

public class n extends d
{
    public n(final View view, final a a) {
        super(view, a);
    }
    
    @Override
    List<ObjectAnimator> \u3007o00\u3007\u3007Oo() {
        float \u3007o00\u3007\u3007Oo = b.b.a.a.d.g.d.\u3007o00\u3007\u3007Oo(b.b.a.a.d.c.\u3007080(), (float)super.\u3007OOo8\u30070.o0ooO());
        float \u3007o00\u3007\u3007Oo2 = b.b.a.a.d.g.d.\u3007o00\u3007\u3007Oo(b.b.a.a.d.c.\u3007080(), (float)super.\u3007OOo8\u30070.o\u30078());
        final boolean equals = "reverse".equals(super.\u3007OOo8\u30070.OO0o\u3007\u3007\u3007\u30070());
        float n = 0.0f;
        float translationY;
        if (equals) {
            translationY = 0.0f;
        }
        else {
            n = \u3007o00\u3007\u3007Oo;
            translationY = \u3007o00\u3007\u3007Oo2;
            \u3007o00\u3007\u3007Oo = 0.0f;
            \u3007o00\u3007\u3007Oo2 = 0.0f;
        }
        float n2 = \u3007o00\u3007\u3007Oo;
        float translationX = n;
        if (b.\u3007080(super.\u300708O\u300700\u3007o.getContext())) {
            translationX = -n;
            n2 = -\u3007o00\u3007\u3007Oo;
        }
        super.\u300708O\u300700\u3007o.setTranslationX(translationX);
        super.\u300708O\u300700\u3007o.setTranslationY(translationY);
        final ObjectAnimator setDuration = ObjectAnimator.ofFloat((Object)super.\u300708O\u300700\u3007o, "translationX", new float[] { translationX, n2 }).setDuration((long)(int)(super.\u3007OOo8\u30070.Oooo8o0\u3007() * 1000.0));
        final ObjectAnimator setDuration2 = ObjectAnimator.ofFloat((Object)super.\u300708O\u300700\u3007o, "translationY", new float[] { translationY, \u3007o00\u3007\u3007Oo2 }).setDuration((long)(int)(super.\u3007OOo8\u30070.Oooo8o0\u3007() * 1000.0));
        final ArrayList list = new ArrayList();
        list.add(this.\u3007080(setDuration));
        list.add(this.\u3007080(setDuration2));
        return list;
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package b.b.a.a.d.d.f.a;

import android.view.ViewGroup$LayoutParams;
import android.view.ViewGroup;
import android.animation.Animator;
import android.animation.Animator$AnimatorListener;
import java.util.ArrayList;
import com.bytedance.sdk.component.adexpress.dynamic.dynamicview.DynamicBaseWidget;
import android.widget.ImageView;
import android.animation.ObjectAnimator;
import java.util.List;
import b.b.a.a.d.d.h.a;
import android.view.View;

public class i extends d
{
    private float O8o08O8O;
    private b o\u300700O;
    private float \u3007080OO8\u30070;
    
    public i(final View view, final a a) {
        super(view, a);
    }
    
    @Override
    List<ObjectAnimator> \u3007o00\u3007\u3007Oo() {
        final View \u300708O\u300700\u3007o = super.\u300708O\u300700\u3007o;
        if (\u300708O\u300700\u3007o instanceof ImageView && \u300708O\u300700\u3007o.getParent() instanceof DynamicBaseWidget) {
            super.\u300708O\u300700\u3007o = (View)super.\u300708O\u300700\u3007o.getParent();
        }
        super.\u300708O\u300700\u3007o.setAlpha(0.0f);
        final ObjectAnimator setDuration = ObjectAnimator.ofFloat((Object)super.\u300708O\u300700\u3007o, "alpha", new float[] { 0.0f, 1.0f }).setDuration((long)(int)(super.\u3007OOo8\u30070.Oooo8o0\u3007() * 1000.0));
        final View \u300708O\u300700\u3007o2 = super.\u300708O\u300700\u3007o;
        this.o\u300700O = new b(\u300708O\u300700\u3007o2);
        final int height = \u300708O\u300700\u3007o2.getLayoutParams().height;
        this.O8o08O8O = (float)height;
        this.\u3007080OO8\u30070 = (float)super.\u300708O\u300700\u3007o.getLayoutParams().width;
        String s;
        int n;
        if (!"left".equals(super.\u3007OOo8\u30070.o\u3007\u30070\u3007()) && !"right".equals(super.\u3007OOo8\u30070.o\u3007\u30070\u3007())) {
            s = "height";
            n = height;
        }
        else {
            n = (int)this.\u3007080OO8\u30070;
            s = "width";
        }
        final ObjectAnimator setDuration2 = ObjectAnimator.ofInt((Object)this.o\u300700O, s, new int[] { 0, n }).setDuration((long)(int)(super.\u3007OOo8\u30070.Oooo8o0\u3007() * 1000.0));
        final ArrayList list = new ArrayList();
        list.add(this.\u3007080(setDuration));
        list.add(this.\u3007080(setDuration2));
        ((Animator)list.get(0)).addListener((Animator$AnimatorListener)new Animator$AnimatorListener(this, height) {
            final int \u3007080;
            final i \u3007o00\u3007\u3007Oo;
            
            public void onAnimationCancel(final Animator animator) {
            }
            
            public void onAnimationEnd(final Animator animator) {
                this.\u3007o00\u3007\u3007Oo.o\u300700O.\u3007080(this.\u3007080);
            }
            
            public void onAnimationEnd(final Animator animator, final boolean b) {
            }
            
            public void onAnimationRepeat(final Animator animator) {
            }
            
            public void onAnimationStart(final Animator animator) {
            }
            
            public void onAnimationStart(final Animator animator, final boolean b) {
            }
        });
        return list;
    }
    
    private class b
    {
        private View \u3007080;
        final i \u3007o00\u3007\u3007Oo;
        
        public b(final i \u3007o00\u3007\u3007Oo, final View \u3007080) {
            this.\u3007o00\u3007\u3007Oo = \u3007o00\u3007\u3007Oo;
            this.\u3007080 = \u3007080;
        }
        
        public void \u3007080(final int height) {
            if ("top".equals(this.\u3007o00\u3007\u3007Oo.\u3007OOo8\u30070.o\u3007\u30070\u3007())) {
                if (this.\u3007o00\u3007\u3007Oo.\u300708O\u300700\u3007o instanceof ViewGroup) {
                    for (int i = 0; i < ((ViewGroup)this.\u3007o00\u3007\u3007Oo.\u300708O\u300700\u3007o).getChildCount(); ++i) {
                        ((ViewGroup)this.\u3007o00\u3007\u3007Oo.\u300708O\u300700\u3007o).getChildAt(i).setTranslationY(height - this.\u3007o00\u3007\u3007Oo.O8o08O8O);
                    }
                }
                final i \u3007o00\u3007\u3007Oo = this.\u3007o00\u3007\u3007Oo;
                \u3007o00\u3007\u3007Oo.\u300708O\u300700\u3007o.setTranslationY(\u3007o00\u3007\u3007Oo.O8o08O8O - height);
            }
            else {
                final ViewGroup$LayoutParams layoutParams = this.\u3007080.getLayoutParams();
                layoutParams.height = height;
                this.\u3007080.setLayoutParams(layoutParams);
                this.\u3007080.requestLayout();
            }
        }
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package b.b.a.a.d.d.h;

import java.util.Iterator;
import org.json.JSONException;
import org.json.JSONArray;
import android.text.TextUtils;
import org.json.JSONObject;
import java.util.HashMap;

public class c
{
    private HashMap<String, Object> \u3007080;
    private JSONObject \u3007o00\u3007\u3007Oo;
    
    public c(final JSONObject \u3007o00\u3007\u3007Oo) {
        this.\u3007080 = new HashMap<String, Object>();
        this.\u3007o00\u3007\u3007Oo = \u3007o00\u3007\u3007Oo;
    }
    
    public Object \u3007080(final String s) {
        if (this.\u3007080.containsKey(s)) {
            return this.\u3007080.get(s);
        }
        return null;
    }
    
    public void \u3007o00\u3007\u3007Oo() {
        final Iterator keys = this.\u3007o00\u3007\u3007Oo.keys();
        while (keys.hasNext()) {
            final String s = keys.next();
            final Object opt = this.\u3007o00\u3007\u3007Oo.opt(s);
            final boolean equals = TextUtils.equals((CharSequence)"image", (CharSequence)s);
            int i = 0;
            if (equals) {
                if (!(opt instanceof JSONArray)) {
                    continue;
                }
                while (true) {
                    final JSONArray jsonArray = (JSONArray)opt;
                    if (i >= jsonArray.length()) {
                        break;
                    }
                    final JSONObject optJSONObject = jsonArray.optJSONObject(i);
                    if (optJSONObject != null) {
                        final Iterator keys2 = optJSONObject.keys();
                        while (keys2.hasNext()) {
                            final String str = keys2.next();
                            final Object opt2 = optJSONObject.opt(str);
                            final HashMap<String, Object> \u3007080 = this.\u3007080;
                            final StringBuilder sb = new StringBuilder();
                            sb.append(s);
                            sb.append(".");
                            sb.append(i);
                            sb.append(".");
                            sb.append(str);
                            \u3007080.put(sb.toString(), opt2);
                        }
                    }
                    ++i;
                }
            }
            else {
                if (TextUtils.equals((CharSequence)"dynamic_creative", (CharSequence)s)) {
                    if (!(opt instanceof String)) {
                        continue;
                    }
                    try {
                        final JSONObject jsonObject = new JSONObject((String)opt);
                        final Iterator keys3 = jsonObject.keys();
                        while (keys3.hasNext()) {
                            final String s2 = keys3.next();
                            final Object opt3 = jsonObject.opt(s2);
                            if (opt3 instanceof JSONArray && !TextUtils.equals((CharSequence)s2, (CharSequence)"short_phrase") && !TextUtils.equals((CharSequence)s2, (CharSequence)"long_phrase")) {
                                for (int j = 0; j < ((JSONArray)opt3).length(); ++j) {
                                    final HashMap<String, Object> \u300781 = this.\u3007080;
                                    final StringBuilder sb2 = new StringBuilder();
                                    sb2.append(s);
                                    sb2.append(".");
                                    sb2.append(s2);
                                    sb2.append(".");
                                    sb2.append(j);
                                    \u300781.put(sb2.toString(), ((JSONArray)opt3).opt(j));
                                }
                            }
                            else {
                                final HashMap<String, Object> \u300782 = this.\u3007080;
                                final StringBuilder sb3 = new StringBuilder();
                                sb3.append(s);
                                sb3.append(".");
                                sb3.append(s2);
                                \u300782.put(sb3.toString(), opt3);
                            }
                        }
                        continue;
                    }
                    catch (final JSONException ex) {
                        ((Throwable)ex).printStackTrace();
                        continue;
                    }
                }
                if (opt instanceof JSONObject) {
                    if (opt == null) {
                        continue;
                    }
                    final JSONObject jsonObject2 = (JSONObject)opt;
                    final Iterator keys4 = jsonObject2.keys();
                    while (keys4.hasNext()) {
                        final String str2 = keys4.next();
                        final Object opt4 = jsonObject2.opt(str2);
                        final HashMap<String, Object> \u300783 = this.\u3007080;
                        final StringBuilder sb4 = new StringBuilder();
                        sb4.append(s);
                        sb4.append(".");
                        sb4.append(str2);
                        \u300783.put(sb4.toString(), opt4);
                    }
                }
                else {
                    this.\u3007080.put(s, opt);
                    if (!(opt instanceof String)) {
                        continue;
                    }
                    this.\u3007080.put(s, opt);
                }
            }
        }
    }
    
    public boolean \u3007o\u3007(final String key) {
        return this.\u3007080.containsKey(key);
    }
}

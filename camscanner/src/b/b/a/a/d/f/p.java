// 
// Decompiled by Procyon v0.6.0
// 

package b.b.a.a.d.f;

import android.view.View;
import java.util.concurrent.TimeUnit;
import b.b.a.a.k.e;
import b.b.a.a.d.h.a;
import android.content.Context;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.ScheduledFuture;

public class p implements j
{
    private m O8;
    private ScheduledFuture<?> Oo08;
    private AtomicBoolean o\u30070;
    private Context \u3007080;
    private b.b.a.a.d.h.a \u3007o00\u3007\u3007Oo;
    private h \u3007o\u3007;
    
    public p(final Context \u3007080, final m o8, final b.b.a.a.d.h.a \u3007o00\u3007\u3007Oo, final h \u3007o\u3007) {
        this.\u3007080 = \u3007080;
        this.O8 = o8;
        this.\u3007o\u3007 = \u3007o\u3007;
        this.o\u30070 = new AtomicBoolean(false);
        (this.\u3007o00\u3007\u3007Oo = \u3007o00\u3007\u3007Oo).a(this.\u3007o\u3007);
    }
    
    private void o\u30070() {
        try {
            final ScheduledFuture<?> oo08 = this.Oo08;
            if (oo08 != null && !oo08.isCancelled()) {
                this.Oo08.cancel(false);
                this.Oo08 = null;
            }
            com.bytedance.sdk.component.utils.m.a("RenderInterceptor", "WebView Render cancel timeout timer");
        }
        finally {
            final Throwable t;
            t.printStackTrace();
        }
    }
    
    private void \u3007o00\u3007\u3007Oo(final a a, final int n) {
        if (a.c()) {
            return;
        }
        if (this.o\u30070.get()) {
            return;
        }
        this.o\u30070();
        this.O8.\u3007\u3007808\u3007().d(n);
        if (a.\u3007o00\u3007\u3007Oo(this)) {
            a.\u3007o\u3007(this);
        }
        else {
            final o a2 = a.a();
            if (a2 == null) {
                return;
            }
            a2.b(n);
        }
        this.o\u30070.getAndSet(true);
    }
    
    @Override
    public void a() {
        this.\u3007o00\u3007\u3007Oo.a();
    }
    
    @Override
    public void b() {
        this.\u3007o00\u3007\u3007Oo.c();
    }
    
    @Override
    public void release() {
        this.\u3007o00\u3007\u3007Oo.k();
        this.o\u30070();
    }
    
    @Override
    public boolean \u3007080(final a a) {
        final int \u3007\u30078O0\u30078 = this.O8.\u3007\u30078O0\u30078();
        if (\u3007\u30078O0\u30078 < 0) {
            this.\u3007o00\u3007\u3007Oo(a, 107);
        }
        else {
            this.Oo08 = e.\u3007O888o0o().schedule(new b(1, a), \u3007\u30078O0\u30078, TimeUnit.MILLISECONDS);
            this.\u3007o00\u3007\u3007Oo.a(new g(this, a) {
                final a \u3007080;
                final p \u3007o00\u3007\u3007Oo;
                
                @Override
                public void a(final int n) {
                    this.\u3007o00\u3007\u3007Oo.\u3007o00\u3007\u3007Oo(this.\u3007080, n);
                }
                
                @Override
                public void a(final View view, final n n) {
                    this.\u3007o00\u3007\u3007Oo.o\u30070();
                    if (this.\u3007080.c()) {
                        return;
                    }
                    final o a = this.\u3007080.a();
                    if (a == null) {
                        return;
                    }
                    a.a(this.\u3007o00\u3007\u3007Oo.\u3007o00\u3007\u3007Oo, n);
                    this.\u3007080.a(true);
                }
            });
        }
        return true;
    }
    
    public b.b.a.a.d.h.a \u3007\u3007888() {
        return this.\u3007o00\u3007\u3007Oo;
    }
    
    private class b implements Runnable
    {
        final p OO;
        private int o0;
        a \u3007OOo8\u30070;
        
        public b(final p oo, final int o0, final a \u3007oOo8\u30070) {
            this.OO = oo;
            this.o0 = o0;
            this.\u3007OOo8\u30070 = \u3007oOo8\u30070;
        }
        
        @Override
        public void run() {
            if (this.o0 == 1) {
                com.bytedance.sdk.component.utils.m.a("RenderInterceptor", "WebView Render timeout");
                this.OO.\u3007o00\u3007\u3007Oo.a(true);
                this.OO.\u3007o00\u3007\u3007Oo(this.\u3007OOo8\u30070, 107);
            }
        }
    }
}

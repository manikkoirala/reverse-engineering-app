// 
// Decompiled by Procyon v0.6.0
// 

package b.b.a.a.d.f;

import android.view.View;
import b.b.a.a.d.d.d;
import b.b.a.a.d.d.i.f;
import b.b.a.a.d.d.i.g;
import com.bytedance.sdk.component.adexpress.theme.ThemeStatusBroadcastReceiver;
import b.b.a.a.d.d.g.a;
import android.content.Context;

public class b implements j
{
    private h O8;
    private m Oo08;
    private int o\u30070;
    private Context \u3007080;
    private b.b.a.a.d.d.g.a \u3007o00\u3007\u3007Oo;
    private ThemeStatusBroadcastReceiver \u3007o\u3007;
    
    public b(final Context \u3007080, final m oo08, final ThemeStatusBroadcastReceiver \u3007o\u3007, final boolean b, final g g, final h o8, final b.b.a.a.d.d.j.a a, final b.b.a.a.d.d.g.a \u3007o00\u3007\u3007Oo) {
        this.\u3007080 = \u3007080;
        this.Oo08 = oo08;
        this.\u3007o\u3007 = \u3007o\u3007;
        this.O8 = o8;
        if (\u3007o00\u3007\u3007Oo != null) {
            this.\u3007o00\u3007\u3007Oo = \u3007o00\u3007\u3007Oo;
        }
        else {
            this.\u3007o00\u3007\u3007Oo = new b.b.a.a.d.d.g.a(\u3007080, \u3007o\u3007, b, g, oo08, a);
        }
        this.\u3007o00\u3007\u3007Oo.a(this.O8);
        if (g instanceof f) {
            this.o\u30070 = 3;
        }
        else {
            this.o\u30070 = 2;
        }
    }
    
    public d O8() {
        final b.b.a.a.d.d.g.a \u3007o00\u3007\u3007Oo = this.\u3007o00\u3007\u3007Oo;
        if (\u3007o00\u3007\u3007Oo != null) {
            return (d)\u3007o00\u3007\u3007Oo.c();
        }
        return null;
    }
    
    @Override
    public void a() {
    }
    
    @Override
    public void b() {
    }
    
    @Override
    public void release() {
        final b.b.a.a.d.d.g.a \u3007o00\u3007\u3007Oo = this.\u3007o00\u3007\u3007Oo;
        if (\u3007o00\u3007\u3007Oo != null) {
            \u3007o00\u3007\u3007Oo.g();
        }
    }
    
    @Override
    public boolean \u3007080(final a a) {
        this.Oo08.\u3007\u3007808\u3007().e(this.o\u30070);
        this.\u3007o00\u3007\u3007Oo.a(new b.b.a.a.d.f.g(this, a) {
            final a \u3007080;
            final b \u3007o00\u3007\u3007Oo;
            
            @Override
            public void a(final int n) {
                this.\u3007o00\u3007\u3007Oo.Oo08.\u3007\u3007808\u3007().a(this.\u3007o00\u3007\u3007Oo.o\u30070, n, this.\u3007080.\u3007o00\u3007\u3007Oo(this.\u3007o00\u3007\u3007Oo));
                if (this.\u3007080.\u3007o00\u3007\u3007Oo(this.\u3007o00\u3007\u3007Oo)) {
                    this.\u3007080.\u3007o\u3007(this.\u3007o00\u3007\u3007Oo);
                }
                else {
                    final o a = this.\u3007080.a();
                    if (a == null) {
                        return;
                    }
                    a.b(n);
                }
            }
            
            @Override
            public void a(final View view, final n n) {
                if (this.\u3007080.c()) {
                    return;
                }
                this.\u3007o00\u3007\u3007Oo.Oo08.\u3007\u3007808\u3007().f(this.\u3007o00\u3007\u3007Oo.o\u30070);
                this.\u3007o00\u3007\u3007Oo.Oo08.\u3007\u3007808\u3007().c(this.\u3007o00\u3007\u3007Oo.o\u30070);
                this.\u3007o00\u3007\u3007Oo.Oo08.\u3007\u3007808\u3007().g();
                final o a = this.\u3007080.a();
                if (a == null) {
                    return;
                }
                a.a((b.b.a.a.d.f.d<? extends View>)this.\u3007o00\u3007\u3007Oo.\u3007o00\u3007\u3007Oo, n);
                this.\u3007080.a(true);
            }
        });
        return true;
    }
}

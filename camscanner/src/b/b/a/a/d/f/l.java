// 
// Decompiled by Procyon v0.6.0
// 

package b.b.a.a.d.f;

import java.util.Iterator;
import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;

public class l implements a
{
    private AtomicBoolean O8;
    private List<j> \u3007080;
    o \u3007o00\u3007\u3007Oo;
    private i \u3007o\u3007;
    
    public l(final List<j> \u3007080, final i \u3007o\u3007) {
        this.O8 = new AtomicBoolean(false);
        this.\u3007080 = \u3007080;
        this.\u3007o\u3007 = \u3007o\u3007;
    }
    
    @Override
    public o a() {
        return this.\u3007o00\u3007\u3007Oo;
    }
    
    @Override
    public void a(final boolean newValue) {
        this.O8.getAndSet(newValue);
    }
    
    @Override
    public void b() {
        this.\u3007o\u3007.f();
        final Iterator<j> iterator = this.\u3007080.iterator();
        while (iterator.hasNext() && !iterator.next().\u3007080((j.a)this)) {}
    }
    
    @Override
    public boolean c() {
        return this.O8.get();
    }
    
    @Override
    public void \u3007080(final o \u3007o00\u3007\u3007Oo) {
        this.\u3007o00\u3007\u3007Oo = \u3007o00\u3007\u3007Oo;
    }
    
    @Override
    public boolean \u3007o00\u3007\u3007Oo(final j j) {
        final int index = this.\u3007080.indexOf(j);
        final int size = this.\u3007080.size();
        boolean b = true;
        if (index >= size - 1 || index < 0) {
            b = false;
        }
        return b;
    }
    
    @Override
    public void \u3007o\u3007(final j j) {
        int index;
        if ((index = this.\u3007080.indexOf(j)) < 0) {
            return;
        }
        int n;
        do {
            n = index + 1;
            if (n >= this.\u3007080.size()) {
                break;
            }
            index = n;
        } while (!this.\u3007080.get(n).\u3007080((j.a)this));
    }
}

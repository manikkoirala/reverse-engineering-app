// 
// Decompiled by Procyon v0.6.0
// 

package b.b.a.a.d;

import org.json.JSONObject;
import java.util.HashMap;

public class a
{
    public static final HashMap<String, JSONObject> \u3007080;
    
    static {
        \u3007080 = new HashMap<String, JSONObject>();
    }
    
    public static void O000(final String key) {
        try {
            a.\u3007080.put(key, new JSONObject("{\"values\":{\"borderColor\":\"#1A73E8\",\"data\":\"button_text\",\"paddingRight\":4,\"borderSize\":1,\"useAtomFontFamily\":false,\"align\":\"0\",\"timingStart\":0,\"marginRight\":0,\"paddingBottom\":10,\"fontFamily\":\"PingFangSC-Regular\",\"atomFontFamilyPreview\":1,\"atomBgStartColorPreview\":1,\"borderStyle\":\"solid\",\"fontWeight\":400,\"lineCount\":2,\"height\":40,\"textMaterialCenterCalcColor\":-999,\"bindEventBy\":\"none\",\"textAlign\":\"center\",\"useAtomBgEndColor\":false,\"fontModel\":\"\",\"i18n\":{\"de\":\"\",\"hi\":\"\",\"ru\":\"\",\"pt\":\"\",\"japan\":\"\",\"zhHant\":\"\",\"en\":\"\",\"cn\":\"\",\"it\":\"\",\"fr\":\"\",\"es\":\"\",\"korea\":\"\",\"vi\":\"\",\"th\":\"\",\"id\":\"\"},\"marginLeft\":0,\"textFlowType\":\"0\",\"useAtomBgColor\":false,\"atomBgEndColor\":[0],\"atomBgEndColorPreview\":1,\"borderRadius\":2,\"clickTigger\":\"click\",\"heightMode\":\"auto\",\"marginBottom\":0,\"fontSize\":16,\"position\":\"0\",\"paddingLeft\":4,\"colorCg\":0,\"atomBgStartColor\":[0],\"color\":\"#ffffff\",\"lineLimit\":false,\"atomBtnAnimation\":[0],\"useAtomBtnAnimation\":false,\"isDataFixed\":false,\"atomButtonText\":true,\"bgColor\":\"#1A73E8\",\"iconType\":\"0\",\"allowTextFlow\":false,\"paddingTop\":10,\"materialCenterData\":-999,\"bgMaterialCenterCalcColor\":-999,\"atomBgColorPreview\":1,\"showIcon\":false,\"lineFeed\":false,\"clickArea\":\"creative\",\"widthMode\":\"flex\",\"atomFontFamily\":[0],\"lineThrough\":false,\"letterSpacing\":0,\"borderMaterialCenterCalcColor\":-999,\"atomBgColor\":[0],\"width\":1,\"useAtomBgStartColor\":false,\"lineHeight\":1.2,\"atomBtnAnimationPreview\":1,\"textFlowDuration\":3,\"marginTop\":0,\"timingEnd\":65536},\"areaType\":2,\"clickType\":\"button\"}"));
        }
        catch (final Exception ex) {
            ex.printStackTrace();
        }
    }
    
    public static void O08000(final String key) {
        try {
            a.\u3007080.put(key, new JSONObject("{\"values\":{\"borderColor\":\"#2a90d7\",\"data\":\"dynamic_creative.developer_name\",\"paddingRight\":4,\"borderSize\":0,\"useAtomFontFamily\":false,\"align\":\"0\",\"timingStart\":0,\"marginRight\":0,\"paddingBottom\":4,\"fontFamily\":\"PingFangSC-Regular\",\"atomFontFamilyPreview\":1,\"atomBgStartColorPreview\":1,\"borderStyle\":\"solid\",\"fontWeight\":400,\"lineCount\":2,\"height\":40,\"textMaterialCenterCalcColor\":-999,\"bindEventBy\":\"none\",\"textAlign\":\"center\",\"useAtomBgEndColor\":false,\"fontModel\":\"\",\"i18n\":{\"de\":\"\",\"hi\":\"\",\"ru\":\"\",\"pt\":\"\",\"japan\":\"\",\"zhHant\":\"\",\"en\":\"\",\"cn\":\"\",\"it\":\"\",\"fr\":\"\",\"es\":\"\",\"korea\":\"\",\"vi\":\"\",\"th\":\"\",\"id\":\"\"},\"marginLeft\":0,\"textFlowType\":\"0\",\"useAtomBgColor\":false,\"atomBgEndColor\":[0],\"atomBgEndColorPreview\":1,\"borderRadius\":0,\"clickTigger\":\"click\",\"heightMode\":\"auto\",\"marginBottom\":0,\"fontSize\":12,\"position\":\"0\",\"paddingLeft\":4,\"colorCg\":0,\"atomBgStartColor\":[0],\"color\":\"#B0B0B0\",\"lineLimit\":false,\"atomBtnAnimation\":[0],\"useAtomBtnAnimation\":false,\"isDataFixed\":false,\"bgColor\":\"transparent\",\"allowTextFlow\":false,\"paddingTop\":4,\"materialCenterData\":-999,\"bgMaterialCenterCalcColor\":-999,\"atomBgColorPreview\":1,\"lineFeed\":false,\"clickArea\":\"creative\",\"widthMode\":\"auto\",\"atomFontFamily\":[0],\"lineThrough\":false,\"letterSpacing\":0,\"borderMaterialCenterCalcColor\":-999,\"atomBgColor\":[0],\"width\":320,\"useAtomBgStartColor\":false,\"lineHeight\":1.2,\"atomBtnAnimationPreview\":1,\"textFlowDuration\":3,\"marginTop\":0,\"timingEnd\":65536},\"areaType\":2,\"themeValues\":{\"color\":\"#ffffff\"},\"clickType\":\"\"}"));
        }
        catch (final Exception ex) {
            ex.printStackTrace();
        }
    }
    
    public static void O0o\u3007\u3007Oo(final String key) {
        try {
            a.\u3007080.put(key, new JSONObject("{\"values\":{\"imageObjectFit\":\"none\",\"borderColor\":\"#000000\",\"data\":\"image.0.url\",\"paddingRight\":0,\"borderSize\":0,\"interactHiddenTime\":5,\"fixBorderStyleType\":0,\"align\":\"0\",\"isDataFixed\":false,\"timingStart\":0,\"marginRight\":0,\"paddingBottom\":0,\"interactSlideDirection\":\"2\",\"bgColor\":\"transparent\",\"bgGauseBlurRadius\":3,\"paddingTop\":0,\"materialCenterData\":-999,\"borderStyle\":\"solid\",\"bgMaterialCenterCalcColor\":-999,\"imageType\":\"static-image\",\"height\":0.5625,\"bindEventBy\":\"none\",\"justifyHorizontal\":\"space-around\",\"clickArea\":\"creative\",\"widthMode\":\"flex\",\"alignItems\":\"flex-start\",\"bgGauseBlur\":false,\"interactText\":\"\",\"interactI18n\":{\"de\":\"\",\"hi\":\"\",\"ru\":\"\",\"pt\":\"\",\"japan\":\"\",\"zhHant\":\"\",\"en\":\"\",\"cn\":\"\",\"it\":\"\",\"fr\":\"\",\"es\":\"\",\"korea\":\"\",\"vi\":\"\",\"th\":\"\",\"id\":\"\"},\"marginLeft\":0,\"borderMaterialCenterCalcColor\":-999,\"interactValidate\":false,\"borderRadius\":0,\"interactType\":\"\",\"clickTigger\":\"click\",\"justifyVertical\":\"flex-start\",\"width\":1,\"heightMode\":\"scale\",\"interactVisibleTime\":0,\"marginBottom\":0,\"position\":\"0\",\"paddingLeft\":0,\"marginTop\":0,\"timingEnd\":65536,\"useLeisureInteract\":false},\"areaType\":2,\"themeValues\":{},\"clickType\":\"image\"}"));
        }
        catch (final Exception ex) {
            ex.printStackTrace();
        }
    }
    
    public static void O8(final String key) {
        try {
            a.\u3007080.put(key, new JSONObject("{\"values\":{\"borderColor\":\"#2a90d7\",\"data\":\"ad\",\"paddingRight\":2,\"borderSize\":1,\"useAtomFontFamily\":false,\"align\":\"0\",\"timingStart\":0,\"marginRight\":0,\"paddingBottom\":1,\"fontFamily\":\"PingFangSC-Regular\",\"atomFontFamilyPreview\":1,\"atomBgStartColorPreview\":1,\"borderStyle\":\"solid\",\"fontWeight\":400,\"lineCount\":2,\"height\":40,\"textMaterialCenterCalcColor\":-999,\"bindEventBy\":\"none\",\"textAlign\":\"center\",\"useAtomBgEndColor\":false,\"fontModel\":\"\",\"i18n\":{\"de\":\"\",\"hi\":\"\",\"ru\":\"\",\"pt\":\"\",\"japan\":\"\",\"zhHant\":\"\",\"en\":\"\",\"cn\":\"\",\"it\":\"\",\"fr\":\"\",\"es\":\"\",\"korea\":\"\",\"vi\":\"\",\"th\":\"\",\"id\":\"\"},\"marginLeft\":0,\"textFlowType\":\"0\",\"useAtomBgColor\":false,\"atomBgEndColor\":[0],\"atomBgEndColorPreview\":1,\"borderRadius\":3,\"clickTigger\":\"click\",\"heightMode\":\"auto\",\"marginBottom\":0,\"fontSize\":10,\"position\":\"0\",\"paddingLeft\":2,\"colorCg\":0,\"atomBgStartColor\":[0],\"color\":\"#2a90d7\",\"lineLimit\":false,\"atomBtnAnimation\":[0],\"useAtomBtnAnimation\":false,\"isDataFixed\":true,\"bgColor\":\"transparent\",\"allowTextFlow\":false,\"paddingTop\":1,\"materialCenterData\":-999,\"openPrivacy\":false,\"bgMaterialCenterCalcColor\":-999,\"atomBgColorPreview\":1,\"lineFeed\":false,\"clickArea\":\"creative\",\"widthMode\":\"auto\",\"atomFontFamily\":[0],\"lineThrough\":false,\"letterSpacing\":0,\"borderMaterialCenterCalcColor\":-999,\"atomBgColor\":[0],\"width\":320,\"useAtomBgStartColor\":false,\"lineHeight\":1,\"atomBtnAnimationPreview\":1,\"textFlowDuration\":3,\"marginTop\":0,\"timingEnd\":65536},\"areaType\":2,\"clickType\":\"union_logo\"}"));
        }
        catch (final Exception ex) {
            ex.printStackTrace();
        }
    }
    
    public static void O8ooOoo\u3007(final String key) {
        try {
            a.\u3007080.put(key, new JSONObject("{\"values\":{\"borderColor\":\"#000000\",\"data\":\"video.video_duration\",\"paddingRight\":0,\"borderSize\":0,\"useAtomFontFamily\":false,\"align\":\"0\",\"timingStart\":0,\"marginRight\":0,\"paddingBottom\":0,\"fontFamily\":\"PingFangSC-Regular\",\"atomFontFamilyPreview\":1,\"atomBgStartColorPreview\":1,\"borderStyle\":\"solid\",\"fontWeight\":400,\"lineCount\":2,\"height\":40,\"textMaterialCenterCalcColor\":-999,\"bindEventBy\":\"none\",\"textAlign\":\"center\",\"useAtomBgEndColor\":false,\"fontModel\":\"\",\"i18n\":{\"de\":\"\",\"hi\":\"\",\"ru\":\"\",\"pt\":\"\",\"japan\":\"\",\"zhHant\":\"\",\"en\":\"\",\"cn\":\"\",\"it\":\"\",\"fr\":\"\",\"es\":\"\",\"korea\":\"\",\"vi\":\"\",\"th\":\"\",\"id\":\"\"},\"marginLeft\":0,\"textFlowType\":\"0\",\"useAtomBgColor\":false,\"atomBgEndColor\":[0],\"atomBgEndColorPreview\":1,\"borderRadius\":0,\"clickTigger\":\"click\",\"heightMode\":\"auto\",\"marginBottom\":0,\"fontSize\":10,\"position\":\"0\",\"paddingLeft\":0,\"colorCg\":0,\"atomBgStartColor\":[0],\"color\":\"#161823\",\"lineLimit\":false,\"atomBtnAnimation\":[0],\"useAtomBtnAnimation\":false,\"isDataFixed\":false,\"bgColor\":\"transparent\",\"allowTextFlow\":false,\"paddingTop\":0,\"materialCenterData\":-999,\"bgMaterialCenterCalcColor\":-999,\"atomBgColorPreview\":1,\"lineFeed\":false,\"clickArea\":\"creative\",\"widthMode\":\"auto\",\"atomFontFamily\":[0],\"lineThrough\":false,\"letterSpacing\":0,\"borderMaterialCenterCalcColor\":-999,\"atomBgColor\":[0],\"width\":320,\"useAtomBgStartColor\":false,\"lineHeight\":1.2,\"atomBtnAnimationPreview\":1,\"textFlowDuration\":3,\"marginTop\":0,\"timingEnd\":65536},\"areaType\":2,\"clickType\":\"time_down\"}"));
        }
        catch (final Exception ex) {
            ex.printStackTrace();
        }
    }
    
    public static void O8\u3007o(final String key) {
        try {
            a.\u3007080.put(key, new JSONObject("{\"values\":{\"borderColor\":\"#000000\",\"data\":\"video\",\"bulletScreenStart\":0,\"paddingRight\":0,\"borderSize\":0,\"interactHiddenTime\":5,\"fixBorderStyleType\":0,\"align\":\"0\",\"showPlayButton\":true,\"isDataFixed\":false,\"timingStart\":0,\"bulletScreenData\":\"dynamic_creative.danmu\",\"marginRight\":0,\"bulletScreenEnd\":0,\"paddingBottom\":0,\"interactSlideDirection\":\"2\",\"bgColor\":\"transparent\",\"paddingTop\":0,\"materialCenterData\":-999,\"borderStyle\":\"solid\",\"bgMaterialCenterCalcColor\":-999,\"height\":1,\"bindEventBy\":\"none\",\"clickArea\":\"video\",\"widthMode\":\"flex\",\"interactText\":\"\",\"interactI18n\":{\"de\":\"\",\"hi\":\"\",\"ru\":\"\",\"pt\":\"\",\"japan\":\"\",\"zhHant\":\"\",\"en\":\"\",\"cn\":\"\",\"it\":\"\",\"fr\":\"\",\"es\":\"\",\"korea\":\"\",\"vi\":\"\",\"th\":\"\",\"id\":\"\"},\"marginLeft\":0,\"borderMaterialCenterCalcColor\":-999,\"showTimeProgress\":true,\"interactValidate\":false,\"borderRadius\":0,\"interactType\":\"\",\"clickTigger\":\"click\",\"showBulletScreen\":false,\"width\":1,\"heightMode\":\"scale\",\"interactVisibleTime\":0,\"marginBottom\":0,\"position\":\"0\",\"paddingLeft\":0,\"marginTop\":0,\"timingEnd\":65536},\"areaType\":99,\"themeValues\":{},\"clickType\":\"video\"}"));
        }
        catch (final Exception ex) {
            ex.printStackTrace();
        }
    }
    
    public static void OO0o\u3007\u3007(final String key) {
        try {
            a.\u3007080.put(key, new JSONObject("{\"values\":{\"borderColor\":\"#000000\",\"data\":\"\",\"paddingRight\":8,\"borderSize\":0,\"interactHiddenTime\":5,\"align\":\"0\",\"appStoreFullScreen\":false,\"isDataFixed\":false,\"timingStart\":0,\"marginRight\":0,\"paddingBottom\":4,\"interactSlideDirection\":\"2\",\"bgColor\":\"rgba(22, 24, 35, 0.4)\",\"paddingTop\":4,\"materialCenterData\":-999,\"borderStyle\":\"solid\",\"bgMaterialCenterCalcColor\":-999,\"atomBgColorPreview\":1,\"height\":40,\"bindEventBy\":\"none\",\"justifyHorizontal\":\"space-around\",\"clickArea\":\"creative\",\"widthMode\":\"auto\",\"alignItems\":\"flex-start\",\"interactText\":\"\",\"isAppStoreAutoOpen\":false,\"interactI18n\":{\"de\":\"\",\"hi\":\"\",\"ru\":\"\",\"pt\":\"\",\"japan\":\"\",\"zhHant\":\"\",\"en\":\"\",\"cn\":\"\",\"it\":\"\",\"fr\":\"\",\"es\":\"\",\"korea\":\"\",\"vi\":\"\",\"th\":\"\",\"id\":\"\"},\"marginLeft\":0,\"borderMaterialCenterCalcColor\":-999,\"useAtomBgColor\":false,\"atomBgColor\":[0],\"interactValidate\":false,\"borderRadius\":14,\"interactType\":\"\",\"clickTigger\":\"click\",\"justifyVertical\":\"flex-start\",\"popAppstore\":false,\"width\":320,\"heightMode\":\"auto\",\"interactVisibleTime\":0,\"marginBottom\":0,\"position\":\"0\",\"paddingLeft\":8,\"marginTop\":0,\"timingEnd\":65536},\"areaType\":0,\"themeValues\":{},\"clickType\":\"\"}"));
        }
        catch (final Exception ex) {
            ex.printStackTrace();
        }
    }
    
    public static void OO0o\u3007\u3007\u3007\u30070(final String key) {
        try {
            a.\u3007080.put(key, new JSONObject("{\"values\":{\"borderColor\":\"#000000\",\"data\":\"app.comment_num\",\"paddingRight\":2,\"borderSize\":0,\"useAtomFontFamily\":false,\"align\":\"0\",\"timingStart\":0,\"marginRight\":0,\"paddingBottom\":2,\"fontFamily\":\"PingFangSC-Regular\",\"atomFontFamilyPreview\":1,\"atomBgStartColorPreview\":1,\"borderStyle\":\"solid\",\"fontWeight\":400,\"lineCount\":2,\"height\":40,\"textMaterialCenterCalcColor\":-999,\"bindEventBy\":\"none\",\"textAlign\":\"center\",\"useAtomBgEndColor\":false,\"fontModel\":\"\",\"i18n\":{\"de\":\"\",\"hi\":\"\",\"ru\":\"\",\"pt\":\"\",\"japan\":\"\",\"zhHant\":\"\",\"en\":\"\",\"cn\":\"\",\"it\":\"\",\"fr\":\"\",\"es\":\"\",\"korea\":\"\",\"vi\":\"\",\"th\":\"\",\"id\":\"\"},\"marginLeft\":0,\"textFlowType\":\"0\",\"useAtomBgColor\":false,\"atomBgEndColor\":[0],\"atomBgEndColorPreview\":1,\"borderRadius\":0,\"clickTigger\":\"click\",\"heightMode\":\"auto\",\"marginBottom\":0,\"fontSize\":10,\"position\":\"0\",\"paddingLeft\":2,\"colorCg\":0,\"atomBgStartColor\":[0],\"color\":\"#999999\",\"lineLimit\":false,\"atomBtnAnimation\":[0],\"useAtomBtnAnimation\":false,\"isDataFixed\":false,\"bgColor\":\"transparent\",\"allowTextFlow\":false,\"paddingTop\":2,\"materialCenterData\":-999,\"bgMaterialCenterCalcColor\":-999,\"atomBgColorPreview\":1,\"lineFeed\":false,\"clickArea\":\"creative\",\"widthMode\":\"auto\",\"atomFontFamily\":[0],\"lineThrough\":false,\"letterSpacing\":0,\"borderMaterialCenterCalcColor\":-999,\"atomBgColor\":[0],\"width\":320,\"useAtomBgStartColor\":false,\"lineHeight\":1,\"atomBtnAnimationPreview\":1,\"textFlowDuration\":3,\"marginTop\":0,\"timingEnd\":65536},\"areaType\":2,\"themeValues\":{\"color\":\"#ffffff\"},\"clickType\":\"score_count\"}"));
        }
        catch (final Exception ex) {
            ex.printStackTrace();
        }
    }
    
    public static void OO8oO0o\u3007(final String key) {
        try {
            a.\u3007080.put(key, new JSONObject("{\"values\":{\"imageObjectFit\":\"none\",\"borderColor\":\"#000000\",\"data\":\"image.0.url\",\"paddingRight\":0,\"borderSize\":0,\"interactHiddenTime\":5,\"fixBorderStyleType\":0,\"align\":\"0\",\"isDataFixed\":false,\"timingStart\":0,\"marginRight\":0,\"paddingBottom\":0,\"interactSlideDirection\":\"2\",\"bgColor\":\"transparent\",\"bgGauseBlurRadius\":3,\"paddingTop\":0,\"materialCenterData\":-999,\"borderStyle\":\"solid\",\"bgMaterialCenterCalcColor\":-999,\"imageType\":\"static-image\",\"height\":1,\"bindEventBy\":\"none\",\"justifyHorizontal\":\"space-around\",\"clickArea\":\"creative\",\"widthMode\":\"flex\",\"alignItems\":\"flex-start\",\"bgGauseBlur\":false,\"interactText\":\"\",\"interactI18n\":{\"de\":\"\",\"hi\":\"\",\"ru\":\"\",\"pt\":\"\",\"japan\":\"\",\"zhHant\":\"\",\"en\":\"\",\"cn\":\"\",\"it\":\"\",\"fr\":\"\",\"es\":\"\",\"korea\":\"\",\"vi\":\"\",\"th\":\"\",\"id\":\"\"},\"marginLeft\":0,\"borderMaterialCenterCalcColor\":-999,\"interactValidate\":false,\"borderRadius\":0,\"interactType\":\"\",\"clickTigger\":\"click\",\"justifyVertical\":\"flex-start\",\"width\":1,\"heightMode\":\"flex\",\"interactVisibleTime\":0,\"marginBottom\":0,\"position\":\"0\",\"paddingLeft\":0,\"marginTop\":0,\"timingEnd\":65536,\"useLeisureInteract\":false},\"areaType\":2,\"themeValues\":{},\"clickType\":\"image\"}"));
        }
        catch (final Exception ex) {
            ex.printStackTrace();
        }
    }
    
    public static void OOO(final String key) {
        try {
            a.\u3007080.put(key, new JSONObject("{\"values\":{\"imageObjectFit\":\"none\",\"borderColor\":\"#000000\",\"data\":\"image.0.url\",\"paddingRight\":0,\"borderSize\":0,\"interactHiddenTime\":5,\"fixBorderStyleType\":0,\"align\":\"0\",\"isDataFixed\":false,\"timingStart\":0,\"marginRight\":0,\"paddingBottom\":0,\"interactSlideDirection\":\"2\",\"bgColor\":\"transparent\",\"bgGauseBlurRadius\":3,\"paddingTop\":0,\"materialCenterData\":-999,\"borderStyle\":\"solid\",\"bgMaterialCenterCalcColor\":-999,\"imageType\":\"static-image\",\"height\":1.52,\"bindEventBy\":\"none\",\"justifyHorizontal\":\"space-around\",\"clickArea\":\"creative\",\"widthMode\":\"flex\",\"alignItems\":\"flex-start\",\"bgGauseBlur\":false,\"interactText\":\"\",\"interactI18n\":{\"de\":\"\",\"hi\":\"\",\"ru\":\"\",\"pt\":\"\",\"japan\":\"\",\"zhHant\":\"\",\"en\":\"\",\"cn\":\"\",\"it\":\"\",\"fr\":\"\",\"es\":\"\",\"korea\":\"\",\"vi\":\"\",\"th\":\"\",\"id\":\"\"},\"marginLeft\":0,\"borderMaterialCenterCalcColor\":-999,\"interactValidate\":false,\"borderRadius\":0,\"interactType\":\"\",\"clickTigger\":\"click\",\"justifyVertical\":\"flex-start\",\"width\":1,\"heightMode\":\"scale\",\"interactVisibleTime\":0,\"marginBottom\":0,\"position\":\"0\",\"paddingLeft\":0,\"marginTop\":0,\"timingEnd\":65536,\"useLeisureInteract\":false},\"areaType\":2,\"themeValues\":{},\"clickType\":\"image\"}"));
        }
        catch (final Exception ex) {
            ex.printStackTrace();
        }
    }
    
    public static void OOO\u3007O0(final String key) {
        try {
            a.\u3007080.put(key, new JSONObject("{\"values\":{\"borderColor\":\"#000000\",\"data\":\"\",\"paddingRight\":0,\"borderSize\":0,\"interactHiddenTime\":5,\"align\":\"0\",\"appStoreFullScreen\":false,\"isDataFixed\":false,\"timingStart\":0,\"marginRight\":0,\"paddingBottom\":0,\"interactSlideDirection\":\"2\",\"bgColor\":\"transparent\",\"paddingTop\":0,\"materialCenterData\":-999,\"borderStyle\":\"solid\",\"bgMaterialCenterCalcColor\":-999,\"atomBgColorPreview\":1,\"height\":80,\"bindEventBy\":\"none\",\"justifyHorizontal\":\"space-around\",\"clickArea\":\"creative\",\"widthMode\":\"flex\",\"alignItems\":\"flex-start\",\"interactText\":\"\",\"isAppStoreAutoOpen\":false,\"interactI18n\":{\"de\":\"\",\"hi\":\"\",\"ru\":\"\",\"pt\":\"\",\"japan\":\"\",\"zhHant\":\"\",\"en\":\"\",\"cn\":\"\",\"it\":\"\",\"fr\":\"\",\"es\":\"\",\"korea\":\"\",\"vi\":\"\",\"th\":\"\",\"id\":\"\"},\"marginLeft\":0,\"borderMaterialCenterCalcColor\":-999,\"useAtomBgColor\":false,\"atomBgColor\":[0],\"interactValidate\":false,\"borderRadius\":0,\"interactType\":\"\",\"clickTigger\":\"click\",\"justifyVertical\":\"flex-start\",\"popAppstore\":false,\"width\":1,\"heightMode\":\"fixed\",\"interactVisibleTime\":0,\"marginBottom\":0,\"position\":\"0\",\"paddingLeft\":0,\"marginTop\":0,\"timingEnd\":65536},\"areaType\":2,\"themeValues\":{},\"clickType\":\"\"}"));
        }
        catch (final Exception ex) {
            ex.printStackTrace();
        }
    }
    
    public static void Oo08(final String key) {
        try {
            a.\u3007080.put(key, new JSONObject("{\"values\":{\"borderColor\":\"#000000\",\"data\":\"\",\"paddingRight\":0,\"borderSize\":0,\"interactHiddenTime\":5,\"align\":\"0\",\"appStoreFullScreen\":false,\"isDataFixed\":false,\"timingStart\":0,\"marginRight\":0,\"paddingBottom\":3,\"interactSlideDirection\":\"2\",\"bgColor\":\"rgba(22, 24, 35, 0.2)\",\"paddingTop\":3,\"materialCenterData\":-999,\"openPrivacy\":false,\"borderStyle\":\"solid\",\"bgMaterialCenterCalcColor\":-999,\"atomBgColorPreview\":1,\"height\":40,\"bindEventBy\":\"none\",\"justifyHorizontal\":\"space-around\",\"clickArea\":\"creative\",\"widthMode\":\"auto\",\"alignItems\":\"flex-start\",\"interactText\":\"\",\"isAppStoreAutoOpen\":false,\"interactI18n\":{\"de\":\"\",\"hi\":\"\",\"ru\":\"\",\"pt\":\"\",\"japan\":\"\",\"zhHant\":\"\",\"en\":\"\",\"cn\":\"\",\"it\":\"\",\"fr\":\"\",\"es\":\"\",\"korea\":\"\",\"vi\":\"\",\"th\":\"\",\"id\":\"\"},\"marginLeft\":0,\"borderMaterialCenterCalcColor\":-999,\"useAtomBgColor\":false,\"atomBgColor\":[0],\"interactValidate\":false,\"borderRadius\":2,\"interactType\":\"\",\"clickTigger\":\"click\",\"justifyVertical\":\"flex-start\",\"popAppstore\":false,\"width\":320,\"heightMode\":\"auto\",\"interactVisibleTime\":0,\"marginBottom\":0,\"position\":\"0\",\"paddingLeft\":0,\"marginTop\":0,\"timingEnd\":65536},\"areaType\":2,\"themeValues\":{},\"clickType\":\"\"}"));
        }
        catch (final Exception ex) {
            ex.printStackTrace();
        }
    }
    
    public static void Oo8Oo00oo(final String key) {
        try {
            a.\u3007080.put(key, new JSONObject("{\"values\":{\"borderColor\":\"#000000\",\"data\":\"video\",\"bulletScreenStart\":0,\"paddingRight\":0,\"borderSize\":0,\"interactHiddenTime\":5,\"fixBorderStyleType\":0,\"align\":\"0\",\"showPlayButton\":true,\"isDataFixed\":false,\"timingStart\":0,\"bulletScreenData\":\"dynamic_creative.danmu\",\"marginRight\":0,\"bulletScreenEnd\":0,\"paddingBottom\":0,\"interactSlideDirection\":\"2\",\"bgColor\":\"transparent\",\"paddingTop\":0,\"materialCenterData\":-999,\"borderStyle\":\"solid\",\"bgMaterialCenterCalcColor\":-999,\"height\":1,\"bindEventBy\":\"none\",\"clickArea\":\"video\",\"widthMode\":\"flex\",\"interactText\":\"\",\"interactI18n\":{\"de\":\"\",\"hi\":\"\",\"ru\":\"\",\"pt\":\"\",\"japan\":\"\",\"zhHant\":\"\",\"en\":\"\",\"cn\":\"\",\"it\":\"\",\"fr\":\"\",\"es\":\"\",\"korea\":\"\",\"vi\":\"\",\"th\":\"\",\"id\":\"\"},\"marginLeft\":0,\"borderMaterialCenterCalcColor\":-999,\"showTimeProgress\":true,\"interactValidate\":false,\"borderRadius\":0,\"interactType\":\"\",\"clickTigger\":\"click\",\"showBulletScreen\":false,\"width\":1,\"heightMode\":\"scale\",\"interactVisibleTime\":0,\"marginBottom\":0,\"position\":\"0\",\"paddingLeft\":0,\"marginTop\":0,\"timingEnd\":65536},\"areaType\":99,\"themeValues\":{},\"clickType\":\"video\"}"));
        }
        catch (final Exception ex) {
            ex.printStackTrace();
        }
    }
    
    public static void OoO8(final String key) {
        try {
            a.\u3007080.put(key, new JSONObject("{\"values\":{\"borderColor\":\"#000000\",\"data\":\"source|app.app_name\",\"paddingRight\":2,\"borderSize\":0,\"useAtomFontFamily\":false,\"align\":\"0\",\"timingStart\":0,\"marginRight\":0,\"paddingBottom\":2,\"fontFamily\":\"PingFangSC-Regular\",\"atomFontFamilyPreview\":1,\"atomBgStartColorPreview\":1,\"borderStyle\":\"solid\",\"fontWeight\":400,\"lineCount\":2,\"height\":40,\"textMaterialCenterCalcColor\":-999,\"bindEventBy\":\"none\",\"textAlign\":\"left\",\"useAtomBgEndColor\":false,\"fontModel\":\"\",\"i18n\":{\"de\":\"\",\"hi\":\"\",\"ru\":\"\",\"pt\":\"\",\"japan\":\"\",\"zhHant\":\"\",\"en\":\"\",\"cn\":\"\",\"it\":\"\",\"fr\":\"\",\"es\":\"\",\"korea\":\"\",\"vi\":\"\",\"th\":\"\",\"id\":\"\"},\"marginLeft\":0,\"textFlowType\":\"0\",\"useAtomBgColor\":false,\"atomBgEndColor\":[0],\"atomBgEndColorPreview\":1,\"borderRadius\":0,\"clickTigger\":\"click\",\"heightMode\":\"auto\",\"marginBottom\":0,\"fontSize\":10,\"position\":\"0\",\"paddingLeft\":2,\"colorCg\":0,\"atomBgStartColor\":[0],\"color\":\"#999999\",\"lineLimit\":false,\"atomBtnAnimation\":[0],\"useAtomBtnAnimation\":false,\"isDataFixed\":false,\"bgColor\":\"transparent\",\"allowTextFlow\":false,\"paddingTop\":2,\"materialCenterData\":-999,\"bgMaterialCenterCalcColor\":-999,\"atomBgColorPreview\":1,\"lineFeed\":false,\"clickArea\":\"creative\",\"widthMode\":\"auto\",\"atomFontFamily\":[0],\"lineThrough\":false,\"letterSpacing\":0,\"borderMaterialCenterCalcColor\":-999,\"atomBgColor\":[0],\"width\":320,\"useAtomBgStartColor\":false,\"lineHeight\":1,\"atomBtnAnimationPreview\":1,\"textFlowDuration\":3,\"marginTop\":0,\"timingEnd\":65536},\"areaType\":2,\"themeValues\":{\"color\":\"#ffffff\"},\"clickType\":\"source\"}"));
        }
        catch (final Exception ex) {
            ex.printStackTrace();
        }
    }
    
    public static void Ooo(final String key) {
        try {
            a.\u3007080.put(key, new JSONObject("{\"values\":{\"borderColor\":\"#000000\",\"data\":\"Please enter the content\",\"paddingRight\":0,\"borderSize\":0,\"useAtomFontFamily\":false,\"align\":\"0\",\"timingStart\":0,\"marginRight\":0,\"paddingBottom\":0,\"fontFamily\":\"PingFangSC-Regular\",\"atomFontFamilyPreview\":1,\"atomBgStartColorPreview\":1,\"borderStyle\":\"solid\",\"fontWeight\":400,\"lineCount\":2,\"height\":40,\"textMaterialCenterCalcColor\":-999,\"bindEventBy\":\"none\",\"textAlign\":\"center\",\"useAtomBgEndColor\":false,\"fontModel\":\"\",\"i18n\":{\"de\":\"\",\"hi\":\"\",\"ru\":\"\",\"pt\":\"\",\"japan\":\"\",\"zhHant\":\"\",\"en\":\"\",\"cn\":\"\",\"it\":\"\",\"fr\":\"\",\"es\":\"\",\"korea\":\"\",\"vi\":\"\",\"th\":\"\",\"id\":\"\"},\"marginLeft\":0,\"textFlowType\":\"0\",\"useAtomBgColor\":false,\"atomBgEndColor\":[0],\"atomBgEndColorPreview\":1,\"borderRadius\":0,\"clickTigger\":\"click\",\"heightMode\":\"auto\",\"marginBottom\":0,\"fontSize\":10,\"position\":\"0\",\"paddingLeft\":0,\"colorCg\":0,\"atomBgStartColor\":[0],\"color\":\"#ffffff\",\"lineLimit\":false,\"atomBtnAnimation\":[0],\"useAtomBtnAnimation\":false,\"isDataFixed\":true,\"bgColor\":\"transparent\",\"allowTextFlow\":false,\"paddingTop\":0,\"materialCenterData\":-999,\"bgMaterialCenterCalcColor\":-999,\"atomBgColorPreview\":1,\"lineFeed\":false,\"clickArea\":\"creative\",\"widthMode\":\"auto\",\"atomFontFamily\":[0],\"lineThrough\":false,\"letterSpacing\":0,\"borderMaterialCenterCalcColor\":-999,\"atomBgColor\":[0],\"width\":320,\"useAtomBgStartColor\":false,\"lineHeight\":1,\"atomBtnAnimationPreview\":1,\"textFlowDuration\":3,\"marginTop\":0,\"timingEnd\":65536},\"areaType\":2,\"clickType\":\"\"}"));
        }
        catch (final Exception ex) {
            ex.printStackTrace();
        }
    }
    
    public static void Oooo8o0\u3007(final String key) {
        try {
            a.\u3007080.put(key, new JSONObject("{\"values\":{\"borderColor\":\"rgba(22, 24, 35, 0.34)\",\"data\":\"text\",\"paddingRight\":0,\"borderSize\":0,\"useAtomFontFamily\":false,\"align\":\"0\",\"timingStart\":0,\"marginRight\":0,\"paddingBottom\":0,\"fontFamily\":\"PingFangSC-Regular\",\"atomFontFamilyPreview\":1,\"atomBgStartColorPreview\":1,\"borderStyle\":\"solid\",\"fontWeight\":400,\"lineCount\":2,\"height\":40,\"textMaterialCenterCalcColor\":-999,\"bindEventBy\":\"none\",\"textAlign\":\"left\",\"useAtomBgEndColor\":false,\"fontModel\":\"\",\"i18n\":{\"de\":\"\",\"hi\":\"\",\"ru\":\"\",\"pt\":\"\",\"japan\":\"\",\"zhHant\":\"\",\"en\":\"\",\"cn\":\"\",\"it\":\"\",\"fr\":\"\",\"es\":\"\",\"korea\":\"\",\"vi\":\"\",\"th\":\"\",\"id\":\"\"},\"marginLeft\":0,\"textFlowType\":\"0\",\"useAtomBgColor\":false,\"atomBgEndColor\":[0],\"atomBgEndColorPreview\":1,\"borderRadius\":0,\"clickTigger\":\"click\",\"heightMode\":\"auto\",\"marginBottom\":0,\"fontSize\":14,\"position\":\"0\",\"paddingLeft\":4,\"colorCg\":0,\"atomBgStartColor\":[0],\"color\":\"#FFFFFF\",\"lineLimit\":false,\"atomBtnAnimation\":[0],\"useAtomBtnAnimation\":false,\"isDataFixed\":true,\"bgColor\":\"transparent\",\"allowTextFlow\":false,\"paddingTop\":0,\"materialCenterData\":-999,\"bgMaterialCenterCalcColor\":-999,\"atomBgColorPreview\":1,\"lineFeed\":false,\"clickArea\":\"creative\",\"widthMode\":\"auto\",\"atomFontFamily\":[0],\"lineThrough\":false,\"letterSpacing\":0,\"borderMaterialCenterCalcColor\":-999,\"atomBgColor\":[0],\"width\":320,\"useAtomBgStartColor\":false,\"lineHeight\":1.4,\"atomBtnAnimationPreview\":1,\"textFlowDuration\":3,\"marginTop\":0,\"timingEnd\":65536},\"areaType\":0,\"clickType\":\"skip\"}"));
        }
        catch (final Exception ex) {
            ex.printStackTrace();
        }
    }
    
    public static JSONObject O\u30078O8\u3007008(final String s) {
        final HashMap<String, JSONObject> \u3007080 = a.\u3007080;
        if (!\u3007080.containsKey(s)) {
            s.hashCode();
            final int hashCode = s.hashCode();
            int n = -1;
            switch (hashCode) {
                case 1837706125: {
                    if (!s.equals("download-progress-button")) {
                        break;
                    }
                    n = 59;
                    break;
                }
                case 1776020536: {
                    if (!s.equals("score-count-type-2")) {
                        break;
                    }
                    n = 58;
                    break;
                }
                case 1736639533: {
                    if (!s.equals("logo-union")) {
                        break;
                    }
                    n = 57;
                    break;
                }
                case 1671642405: {
                    if (!s.equals("dislike")) {
                        break;
                    }
                    n = 56;
                    break;
                }
                case 1388616859: {
                    if (!s.equals("arrowButton")) {
                        break;
                    }
                    n = 55;
                    break;
                }
                case 1387328781: {
                    if (!s.equals("skip-with-countdowns-video-countdown")) {
                        break;
                    }
                    n = 54;
                    break;
                }
                case 1192797642: {
                    if (!s.equals("skip-with-time-countdown")) {
                        break;
                    }
                    n = 53;
                    break;
                }
                case 1151339840: {
                    if (!s.equals("video-vd")) {
                        break;
                    }
                    n = 52;
                    break;
                }
                case 1151339760: {
                    if (!s.equals("video-sq")) {
                        break;
                    }
                    n = 51;
                    break;
                }
                case 1151339406: {
                    if (!s.equals("video-hd")) {
                        break;
                    }
                    n = 50;
                    break;
                }
                case 1121325221: {
                    if (!s.equals("app-icon")) {
                        break;
                    }
                    n = 49;
                    break;
                }
                case 1071669445: {
                    if (!s.equals("image-cover")) {
                        break;
                    }
                    n = 48;
                    break;
                }
                case 918720292: {
                    if (!s.equals("webview-close")) {
                        break;
                    }
                    n = 47;
                    break;
                }
                case 786002716: {
                    if (!s.equals("scoreCountWithIcon")) {
                        break;
                    }
                    n = 46;
                    break;
                }
                case 648097420: {
                    if (!s.equals("tag-group")) {
                        break;
                    }
                    n = 45;
                    break;
                }
                case 567145442: {
                    if (!s.equals("custom-component-vessel")) {
                        break;
                    }
                    n = 44;
                    break;
                }
                case 408677908: {
                    if (!s.equals("score-count")) {
                        break;
                    }
                    n = 43;
                    break;
                }
                case 356256733: {
                    if (!s.equals("development-name")) {
                        break;
                    }
                    n = 42;
                    break;
                }
                case 255899357: {
                    if (!s.equals("feedback-dislike")) {
                        break;
                    }
                    n = 41;
                    break;
                }
                case 164692578: {
                    if (!s.equals("cardButton")) {
                        break;
                    }
                    n = 40;
                    break;
                }
                case 159077850: {
                    if (!s.equals("downloadButton")) {
                        break;
                    }
                    n = 39;
                    break;
                }
                case 112202875: {
                    if (!s.equals("video")) {
                        break;
                    }
                    n = 38;
                    break;
                }
                case 110371416: {
                    if (!s.equals("title")) {
                        break;
                    }
                    n = 37;
                    break;
                }
                case 104264043: {
                    if (!s.equals("muted")) {
                        break;
                    }
                    n = 36;
                    break;
                }
                case 100313435: {
                    if (!s.equals("image")) {
                        break;
                    }
                    n = 35;
                    break;
                }
                case 96634189: {
                    if (!s.equals("empty")) {
                        break;
                    }
                    n = 34;
                    break;
                }
                case 94756344: {
                    if (!s.equals("close")) {
                        break;
                    }
                    n = 33;
                    break;
                }
                case 3556653: {
                    if (!s.equals("text")) {
                        break;
                    }
                    n = 32;
                    break;
                }
                case 3540562: {
                    if (!s.equals("star")) {
                        break;
                    }
                    n = 31;
                    break;
                }
                case 3532159: {
                    if (!s.equals("skip")) {
                        break;
                    }
                    n = 30;
                    break;
                }
                case 3506402: {
                    if (!s.equals("root")) {
                        break;
                    }
                    n = 29;
                    break;
                }
                case 3327403: {
                    if (!s.equals("logo")) {
                        break;
                    }
                    n = 28;
                    break;
                }
                case 3226745: {
                    if (!s.equals("icon")) {
                        break;
                    }
                    n = 27;
                    break;
                }
                case -41742922: {
                    if (!s.equals("colourMixtureButton")) {
                        break;
                    }
                    n = 26;
                    break;
                }
                case -257533963: {
                    if (!s.equals("skip-with-countdowns-skip-countdown")) {
                        break;
                    }
                    n = 25;
                    break;
                }
                case -386230585: {
                    if (!s.equals("split-line")) {
                        break;
                    }
                    n = 24;
                    break;
                }
                case -413916384: {
                    if (!s.equals("skip-with-countdowns-skip-btn")) {
                        break;
                    }
                    n = 23;
                    break;
                }
                case -444669584: {
                    if (!s.equals("skip-countdowns")) {
                        break;
                    }
                    n = 22;
                    break;
                }
                case -664030199: {
                    if (!s.equals("video-image-budget")) {
                        break;
                    }
                    n = 21;
                    break;
                }
                case -678105649: {
                    if (!s.equals("image-square")) {
                        break;
                    }
                    n = 20;
                    break;
                }
                case -679297255: {
                    if (!s.equals("image-splash")) {
                        break;
                    }
                    n = 19;
                    break;
                }
                case -819912138: {
                    if (!s.equals("vessel")) {
                        break;
                    }
                    n = 18;
                    break;
                }
                case -851192266: {
                    if (!s.equals("privacy-detail")) {
                        break;
                    }
                    n = 17;
                    break;
                }
                case -896505829: {
                    if (!s.equals("source")) {
                        break;
                    }
                    n = 16;
                    break;
                }
                case -1037586908: {
                    if (!s.equals("text_star")) {
                        break;
                    }
                    n = 15;
                    break;
                }
                case -1061447019: {
                    if (!s.equals("fillButton")) {
                        break;
                    }
                    n = 14;
                    break;
                }
                case -1088776535: {
                    if (!s.equals("laceButton")) {
                        break;
                    }
                    n = 13;
                    break;
                }
                case -1186265252: {
                    if (!s.equals("icon-download")) {
                        break;
                    }
                    n = 12;
                    break;
                }
                case -1268861541: {
                    if (!s.equals("footer")) {
                        break;
                    }
                    n = 11;
                    break;
                }
                case -1377687758: {
                    if (!s.equals("button")) {
                        break;
                    }
                    n = 10;
                    break;
                }
                case -1463100843: {
                    if (!s.equals("skip-with-time-skip-btn")) {
                        break;
                    }
                    n = 9;
                    break;
                }
                case -1488861179: {
                    if (!s.equals("image-wide")) {
                        break;
                    }
                    n = 8;
                    break;
                }
                case -1489182802: {
                    if (!s.equals("image-long")) {
                        break;
                    }
                    n = 7;
                    break;
                }
                case -1498158101: {
                    if (!s.equals("creative-playable-bait")) {
                        break;
                    }
                    n = 6;
                    break;
                }
                case -1578061049: {
                    if (!s.equals("downloadWithIcon")) {
                        break;
                    }
                    n = 5;
                    break;
                }
                case -1746959194: {
                    if (!s.equals("skip-with-time")) {
                        break;
                    }
                    n = 4;
                    break;
                }
                case -2060497896: {
                    if (!s.equals("subtitle")) {
                        break;
                    }
                    n = 3;
                    break;
                }
                case -2076882705: {
                    if (!s.equals("timedown")) {
                        break;
                    }
                    n = 2;
                    break;
                }
                case -2108148680: {
                    if (!s.equals("close-fill")) {
                        break;
                    }
                    n = 1;
                    break;
                }
                case -2143041236: {
                    if (!s.equals("app-version")) {
                        break;
                    }
                    n = 0;
                    break;
                }
            }
            switch (n) {
                case 59: {
                    o\u30078oOO88(s);
                    break;
                }
                case 58: {
                    \u30078o8o\u3007(s);
                    break;
                }
                case 57: {
                    Oo08(s);
                    break;
                }
                case 56: {
                    \u30078\u30070\u3007o\u3007O(s);
                    break;
                }
                case 55: {
                    oo\u3007(s);
                    break;
                }
                case 54: {
                    \u3007O\u3007(s);
                    break;
                }
                case 53: {
                    \u3007\u30078O0\u30078(s);
                    break;
                }
                case 52: {
                    o\u30070OOo\u30070(s);
                    break;
                }
                case 51: {
                    Oo8Oo00oo(s);
                    break;
                }
                case 50: {
                    \u3007o(s);
                    break;
                }
                case 49: {
                    \u3007oOO8O8(s);
                    break;
                }
                case 48: {
                    ooo\u30078oO(s);
                    break;
                }
                case 47: {
                    \u300708O8o\u30070(s);
                    break;
                }
                case 46: {
                    \u300780\u3007808\u3007O(s);
                    break;
                }
                case 45: {
                    \u3007oo\u3007(s);
                    break;
                }
                case 44: {
                    \u30078(s);
                    break;
                }
                case 43: {
                    OO0o\u3007\u3007\u3007\u30070(s);
                    break;
                }
                case 42: {
                    O08000(s);
                    break;
                }
                case 41: {
                    oO00OOO(s);
                    break;
                }
                case 40: {
                    o0ooO(s);
                    break;
                }
                case 39: {
                    O\u3007O\u3007oO(s);
                    break;
                }
                case 38: {
                    O8\u3007o(s);
                    break;
                }
                case 37: {
                    \u30070000OOO(s);
                    break;
                }
                case 36: {
                    o\u30070(s);
                    break;
                }
                case 35: {
                    OOO(s);
                    break;
                }
                case 34: {
                    o\u3007O(s);
                    break;
                }
                case 33: {
                    o8(s);
                    break;
                }
                case 32: {
                    o\u3007O8\u3007\u3007o(s);
                    break;
                }
                case 31: {
                    \u3007O888o0o(s);
                    break;
                }
                case 30: {
                    \u3007O8o08O(s);
                    break;
                }
                case 29: {
                    oO80(s);
                    break;
                }
                case 28: {
                    O8(s);
                    break;
                }
                case 27: {
                    Ooo(s);
                    break;
                }
                case 26: {
                    \u3007\u30070o(s);
                    break;
                }
                case 25: {
                    \u3007\u3007808\u3007(s);
                    break;
                }
                case 24: {
                    o800o8O(s);
                    break;
                }
                case 23: {
                    Oooo8o0\u3007(s);
                    break;
                }
                case 22: {
                    OO0o\u3007\u3007(s);
                    break;
                }
                case 21: {
                    o\u30078(s);
                    break;
                }
                case 20: {
                    \u3007080(s);
                    break;
                }
                case 19: {
                    OO8oO0o\u3007(s);
                    break;
                }
                case 18: {
                    OOO\u3007O0(s);
                    break;
                }
                case 17: {
                    \u3007\u3007888(s);
                    break;
                }
                case 16: {
                    OoO8(s);
                    break;
                }
                case 15: {
                    \u300700(s);
                    break;
                }
                case 14: {
                    O000(s);
                    break;
                }
                case 13: {
                    \u3007o\u3007(s);
                    break;
                }
                case 12: {
                    \u3007O\u300780o08O(s);
                    break;
                }
                case 11: {
                    \u300780(s);
                    break;
                }
                case 10: {
                    \u300700\u30078(s);
                    break;
                }
                case 9: {
                    \u30070\u3007O0088o(s);
                    break;
                }
                case 8: {
                    \u3007o00\u3007\u3007Oo(s);
                    break;
                }
                case 7: {
                    O0o\u3007\u3007Oo(s);
                    break;
                }
                case 6: {
                    oO(s);
                    break;
                }
                case 5: {
                    o8oO\u3007(s);
                    break;
                }
                case 4: {
                    \u3007O00(s);
                    break;
                }
                case 3: {
                    oo88o8O(s);
                    break;
                }
                case 2: {
                    O8ooOoo\u3007(s);
                    break;
                }
                case 1: {
                    \u3007\u3007\u30070\u3007\u30070(s);
                    break;
                }
                case 0: {
                    o\u3007\u30070\u3007(s);
                    break;
                }
            }
        }
        return (JSONObject)\u3007080.get(s);
    }
    
    public static void O\u3007O\u3007oO(final String key) {
        try {
            a.\u3007080.put(key, new JSONObject("{\"values\":{\"borderColor\":\"#1A73E8\",\"data\":\"Download\",\"paddingRight\":0,\"borderSize\":1,\"useAtomFontFamily\":false,\"idleColor\":\"#f85959\",\"align\":\"0\",\"timingStart\":0,\"marginRight\":20,\"paddingBottom\":12,\"fontFamily\":\"PingFangSC-Regular\",\"atomFontFamilyPreview\":1,\"downloadTextFinished\":\"Install now\",\"atomBgStartColorPreview\":1,\"downloadTextColor\":\"#ffffff\",\"borderStyle\":\"solid\",\"fontWeight\":400,\"lineCount\":2,\"height\":40,\"downloadTextInstalled\":\"Open now\",\"textMaterialCenterCalcColor\":-999,\"bindEventBy\":\"none\",\"textAlign\":\"center\",\"downloadTextPaused\":\"Continue downloading\",\"useAtomBgEndColor\":false,\"fontModel\":\"\",\"i18n\":{\"de\":\"\",\"hi\":\"\",\"ru\":\"\",\"pt\":\"\",\"japan\":\"\",\"zhHant\":\"\",\"en\":\"\",\"cn\":\"\",\"it\":\"\",\"fr\":\"\",\"es\":\"\",\"korea\":\"\",\"vi\":\"\",\"th\":\"\",\"id\":\"\"},\"marginLeft\":20,\"textFlowType\":\"0\",\"useAtomBgColor\":false,\"atomBgEndColor\":[0],\"atomBgEndColorPreview\":1,\"borderRadius\":2,\"clickTigger\":\"click\",\"heightMode\":\"auto\",\"marginBottom\":0,\"fontSize\":16,\"position\":\"0\",\"paddingLeft\":0,\"colorCg\":0,\"atomBgStartColor\":[0],\"color\":\"#1A73E8\",\"lineLimit\":false,\"downloadTextFailed\":\"Download again\",\"atomBtnAnimation\":[0],\"reachedColor\":\"#f85959\",\"useAtomBtnAnimation\":false,\"isDataFixed\":true,\"atomButtonText\":true,\"bgColor\":\"transparent\",\"iconType\":\"0\",\"allowTextFlow\":false,\"paddingTop\":12,\"materialCenterData\":-999,\"bgMaterialCenterCalcColor\":-999,\"atomBgColorPreview\":1,\"showIcon\":false,\"lineFeed\":false,\"clickArea\":\"creative\",\"widthMode\":\"flex\",\"atomFontFamily\":[0],\"lineThrough\":false,\"idleTextColor\":\"#ffffff\",\"letterSpacing\":0,\"unreachedColor\":\"#1d1d1d\",\"borderMaterialCenterCalcColor\":-999,\"atomBgColor\":[0],\"downloadTextStart\":\"Click download\",\"downloadTextActive\":\"Downloading\",\"width\":1,\"useAtomBgStartColor\":false,\"downloadTextIdle\":\"Click download\",\"lineHeight\":1.2,\"atomBtnAnimationPreview\":1,\"textFlowDuration\":3,\"marginTop\":0,\"timingEnd\":65536},\"areaType\":2,\"clickType\":\"button\"}"));
        }
        catch (final Exception ex) {
            ex.printStackTrace();
        }
    }
    
    public static void o0ooO(final String key) {
        try {
            a.\u3007080.put(key, new JSONObject("{\"values\":{\"borderColor\":\"#2a90d7\",\"data\":\"button_text\",\"paddingRight\":10,\"borderSize\":0,\"useAtomFontFamily\":false,\"align\":\"0\",\"timingStart\":0,\"marginRight\":0,\"paddingBottom\":4,\"fontFamily\":\"PingFangSC-Regular\",\"atomFontFamilyPreview\":1,\"atomBgStartColorPreview\":1,\"borderStyle\":\"solid\",\"fontWeight\":400,\"lineCount\":2,\"height\":40,\"textMaterialCenterCalcColor\":-999,\"bindEventBy\":\"none\",\"textAlign\":\"center\",\"useAtomBgEndColor\":false,\"fontModel\":\"\",\"i18n\":{\"de\":\"\",\"hi\":\"\",\"ru\":\"\",\"pt\":\"\",\"japan\":\"\",\"zhHant\":\"\",\"en\":\"\",\"cn\":\"\",\"it\":\"\",\"fr\":\"\",\"es\":\"\",\"korea\":\"\",\"vi\":\"\",\"th\":\"\",\"id\":\"\"},\"marginLeft\":0,\"textFlowType\":\"0\",\"useAtomBgColor\":false,\"atomBgEndColor\":[0],\"atomBgEndColorPreview\":1,\"borderRadius\":0,\"clickTigger\":\"click\",\"heightMode\":\"auto\",\"marginBottom\":0,\"fontSize\":12,\"position\":\"0\",\"paddingLeft\":10,\"colorCg\":0,\"atomBgStartColor\":[0],\"color\":\"#FFFFFF\",\"lineLimit\":false,\"atomBtnAnimation\":[0],\"useAtomBtnAnimation\":false,\"isDataFixed\":false,\"atomButtonText\":true,\"bgColor\":\"#1A73E8\",\"iconType\":\"0\",\"allowTextFlow\":false,\"paddingTop\":4,\"materialCenterData\":-999,\"bgMaterialCenterCalcColor\":-999,\"atomBgColorPreview\":1,\"showIcon\":false,\"lineFeed\":false,\"clickArea\":\"creative\",\"widthMode\":\"auto\",\"atomFontFamily\":[0],\"lineThrough\":false,\"letterSpacing\":0,\"borderMaterialCenterCalcColor\":-999,\"atomBgColor\":[0],\"width\":320,\"useAtomBgStartColor\":false,\"lineHeight\":1.2,\"atomBtnAnimationPreview\":1,\"textFlowDuration\":3,\"marginTop\":0,\"timingEnd\":65536},\"areaType\":2,\"clickType\":\"button\"}"));
        }
        catch (final Exception ex) {
            ex.printStackTrace();
        }
    }
    
    public static void o8(final String key) {
        try {
            a.\u3007080.put(key, new JSONObject("{\"values\":{\"borderColor\":\"rgba(22, 24, 35, 0.34)\",\"data\":\"\",\"paddingRight\":6,\"borderSize\":0,\"useAtomFontFamily\":false,\"align\":\"0\",\"timingStart\":0,\"marginRight\":0,\"paddingBottom\":6,\"fontFamily\":\"PingFangSC-Regular\",\"atomFontFamilyPreview\":1,\"atomBgStartColorPreview\":1,\"borderStyle\":\"solid\",\"fontWeight\":400,\"lineCount\":2,\"height\":40,\"textMaterialCenterCalcColor\":-999,\"bindEventBy\":\"none\",\"textAlign\":\"center\",\"useAtomBgEndColor\":false,\"fontModel\":\"\",\"i18n\":{\"de\":\"\",\"hi\":\"\",\"ru\":\"\",\"pt\":\"\",\"japan\":\"\",\"zhHant\":\"\",\"en\":\"\",\"cn\":\"\",\"it\":\"\",\"fr\":\"\",\"es\":\"\",\"korea\":\"\",\"vi\":\"\",\"th\":\"\",\"id\":\"\"},\"marginLeft\":0,\"textFlowType\":\"0\",\"useAtomBgColor\":false,\"atomBgEndColor\":[0],\"atomBgEndColorPreview\":1,\"borderRadius\":14,\"clickTigger\":\"click\",\"heightMode\":\"auto\",\"marginBottom\":0,\"fontSize\":16,\"position\":\"0\",\"paddingLeft\":6,\"colorCg\":0,\"atomBgStartColor\":[0],\"color\":\"rgba(22, 24, 35, 0.34)\",\"lineLimit\":false,\"atomBtnAnimation\":[0],\"useAtomBtnAnimation\":false,\"isDataFixed\":true,\"bgColor\":\"transparent\",\"allowTextFlow\":false,\"paddingTop\":6,\"materialCenterData\":-999,\"bgMaterialCenterCalcColor\":-999,\"atomBgColorPreview\":1,\"lineFeed\":true,\"clickArea\":\"creative\",\"widthMode\":\"auto\",\"atomFontFamily\":[0],\"lineThrough\":false,\"letterSpacing\":0,\"borderMaterialCenterCalcColor\":-999,\"atomBgColor\":[0],\"width\":320,\"useAtomBgStartColor\":false,\"lineHeight\":1,\"atomBtnAnimationPreview\":1,\"textFlowDuration\":3,\"marginTop\":0,\"timingEnd\":65536},\"areaType\":3,\"themeValues\":{\"borderColor\":\"rgba(255, 255, 255, 70)\",\"color\":\"rgba(255, 255, 255, 70)\"},\"clickType\":\"dislike\"}"));
        }
        catch (final Exception ex) {
            ex.printStackTrace();
        }
    }
    
    public static void o800o8O(final String key) {
        try {
            a.\u3007080.put(key, new JSONObject("{\"values\":{\"widthMode\":\"flex\",\"paddingBottom\":0,\"bgColor\":\"#333333\",\"paddingRight\":0,\"width\":1,\"heightMode\":\"fixed\",\"borderSize\":0,\"paddingTop\":0,\"position\":\"0\",\"paddingLeft\":0,\"height\":1},\"areaType\":1,\"themeValues\":{},\"clickType\":\"\"}"));
        }
        catch (final Exception ex) {
            ex.printStackTrace();
        }
    }
    
    public static void o8oO\u3007(final String key) {
        try {
            a.\u3007080.put(key, new JSONObject("{\"values\":{\"borderColor\":\"#000000\",\"data\":\"\",\"paddingRight\":0,\"borderSize\":0,\"interactHiddenTime\":5,\"align\":\"0\",\"appStoreFullScreen\":false,\"isDataFixed\":false,\"timingStart\":0,\"marginRight\":0,\"paddingBottom\":0,\"interactSlideDirection\":\"2\",\"bgColor\":\"transparent\",\"paddingTop\":0,\"materialCenterData\":-999,\"borderStyle\":\"solid\",\"bgMaterialCenterCalcColor\":-999,\"atomBgColorPreview\":1,\"height\":40,\"bindEventBy\":\"none\",\"justifyHorizontal\":\"space-around\",\"clickArea\":\"creative\",\"widthMode\":\"auto\",\"alignItems\":\"flex-start\",\"interactText\":\"\",\"isAppStoreAutoOpen\":false,\"interactI18n\":{\"de\":\"\",\"hi\":\"\",\"ru\":\"\",\"pt\":\"\",\"japan\":\"\",\"zhHant\":\"\",\"en\":\"\",\"cn\":\"\",\"it\":\"\",\"fr\":\"\",\"es\":\"\",\"korea\":\"\",\"vi\":\"\",\"th\":\"\",\"id\":\"\"},\"marginLeft\":0,\"borderMaterialCenterCalcColor\":-999,\"useAtomBgColor\":false,\"atomBgColor\":[0],\"interactValidate\":false,\"borderRadius\":0,\"interactType\":\"\",\"clickTigger\":\"click\",\"justifyVertical\":\"flex-start\",\"popAppstore\":false,\"width\":320,\"heightMode\":\"auto\",\"interactVisibleTime\":0,\"marginBottom\":0,\"position\":\"0\",\"paddingLeft\":0,\"marginTop\":0,\"timingEnd\":65536},\"areaType\":2,\"themeValues\":{},\"clickType\":\"\"}"));
        }
        catch (final Exception ex) {
            ex.printStackTrace();
        }
    }
    
    public static void oO(final String key) {
        try {
            a.\u3007080.put(key, new JSONObject("{\"values\":{\"borderColor\":\"#000000\",\"data\":\"dynamic_creative.show_playable\",\"paddingRight\":0,\"bgImgUrl\":\"playable-bait\",\"borderSize\":0,\"interactHiddenTime\":5,\"align\":\"0\",\"appStoreFullScreen\":false,\"isDataFixed\":false,\"timingStart\":0,\"marginRight\":0,\"paddingBottom\":0,\"interactSlideDirection\":\"2\",\"bgColor\":\"transparent\",\"paddingTop\":0,\"materialCenterData\":-999,\"borderStyle\":\"solid\",\"bgMaterialCenterCalcColor\":-999,\"atomBgColorPreview\":1,\"height\":28,\"bindEventBy\":\"none\",\"justifyHorizontal\":\"space-around\",\"clickArea\":\"creative\",\"widthMode\":\"fixed\",\"alignItems\":\"flex-start\",\"interactText\":\"\",\"isAppStoreAutoOpen\":false,\"interactI18n\":{\"de\":\"\",\"hi\":\"\",\"ru\":\"\",\"pt\":\"\",\"japan\":\"\",\"zhHant\":\"\",\"en\":\"\",\"cn\":\"\",\"it\":\"\",\"fr\":\"\",\"es\":\"\",\"korea\":\"\",\"vi\":\"\",\"th\":\"\",\"id\":\"\"},\"marginLeft\":0,\"borderMaterialCenterCalcColor\":-999,\"useAtomBgColor\":false,\"atomBgColor\":[0],\"interactValidate\":false,\"borderRadius\":0,\"interactType\":\"\",\"clickTigger\":\"click\",\"justifyVertical\":\"flex-start\",\"popAppstore\":false,\"width\":94,\"heightMode\":\"fixed\",\"interactVisibleTime\":0,\"marginBottom\":0,\"position\":\"0\",\"paddingLeft\":0,\"marginTop\":0,\"timingEnd\":65536},\"areaType\":2,\"themeValues\":{},\"clickType\":\"mnpl_guide_comp_click\"}"));
        }
        catch (final Exception ex) {
            ex.printStackTrace();
        }
    }
    
    public static void oO00OOO(final String key) {
        try {
            a.\u3007080.put(key, new JSONObject("{\"values\":{\"borderColor\":\"rgba(22, 24, 35, 0.34)\",\"data\":\"\",\"paddingRight\":5,\"borderSize\":0,\"useAtomFontFamily\":false,\"align\":\"0\",\"timingStart\":0,\"marginRight\":0,\"paddingBottom\":5,\"fontFamily\":\"PingFangSC-Regular\",\"atomFontFamilyPreview\":1,\"atomBgStartColorPreview\":1,\"borderStyle\":\"solid\",\"fontWeight\":400,\"lineCount\":2,\"height\":40,\"textMaterialCenterCalcColor\":-999,\"bindEventBy\":\"none\",\"textAlign\":\"center\",\"useAtomBgEndColor\":false,\"fontModel\":\"\",\"i18n\":{\"de\":\"\",\"hi\":\"\",\"ru\":\"\",\"pt\":\"\",\"japan\":\"\",\"zhHant\":\"\",\"en\":\"\",\"cn\":\"\",\"it\":\"\",\"fr\":\"\",\"es\":\"\",\"korea\":\"\",\"vi\":\"\",\"th\":\"\",\"id\":\"\"},\"marginLeft\":0,\"textFlowType\":\"0\",\"useAtomBgColor\":false,\"atomBgEndColor\":[0],\"atomBgEndColorPreview\":1,\"borderRadius\":14,\"clickTigger\":\"click\",\"heightMode\":\"auto\",\"marginBottom\":0,\"fontSize\":14,\"position\":\"0\",\"paddingLeft\":5,\"colorCg\":0,\"atomBgStartColor\":[0],\"color\":\"#ffffff\",\"lineLimit\":false,\"atomBtnAnimation\":[0],\"useAtomBtnAnimation\":false,\"isDataFixed\":true,\"bgColor\":\"rgba(22, 24, 35, 0.5)\",\"allowTextFlow\":false,\"paddingTop\":5,\"materialCenterData\":-999,\"bgMaterialCenterCalcColor\":-999,\"atomBgColorPreview\":1,\"lineFeed\":true,\"clickArea\":\"creative\",\"widthMode\":\"auto\",\"atomFontFamily\":[0],\"lineThrough\":false,\"letterSpacing\":0,\"borderMaterialCenterCalcColor\":-999,\"atomBgColor\":[0],\"width\":320,\"useAtomBgStartColor\":false,\"lineHeight\":1.3,\"atomBtnAnimationPreview\":1,\"textFlowDuration\":3,\"marginTop\":0,\"timingEnd\":65536},\"areaType\":3,\"themeValues\":{\"bgColor\":\"#9B9B9B\"},\"clickType\":\"dislike\"}"));
        }
        catch (final Exception ex) {
            ex.printStackTrace();
        }
    }
    
    public static void oO80(final String key) {
        try {
            a.\u3007080.put(key, new JSONObject("{\"values\":{\"paddingRight\":0,\"borderSize\":0,\"interactHiddenTime\":5,\"paddingBottom\":0,\"interactSlideDirection\":\"2\",\"bgColor\":\"#ffffff\",\"paddingTop\":0,\"borderStyle\":\"solid\",\"height\":375,\"justifyHorizontal\":\"space-around\",\"bindEventBy\":\"none\",\"clickArea\":\"creative\",\"widthMode\":\"fixed\",\"alignItems\":\"flex-start\",\"pageMode\":\"0\",\"interactText\":\"\",\"interactI18n\":{\"de\":\"\",\"hi\":\"\",\"ru\":\"\",\"pt\":\"\",\"japan\":\"\",\"zhHant\":\"\",\"en\":\"\",\"cn\":\"\",\"it\":\"\",\"fr\":\"\",\"es\":\"\",\"korea\":\"\",\"vi\":\"\",\"th\":\"\",\"id\":\"\"},\"interactValidate\":false,\"borderRadius\":0,\"interactType\":\"\",\"clickTigger\":\"click\",\"justifyVertical\":\"flex-start\",\"width\":375,\"heightMode\":\"fixed\",\"interactVisibleTime\":0,\"paddingLeft\":0},\"areaType\":2,\"themeValues\":{\"bgColor\":\"rgba(255,255,255,0)\"},\"clickType\":\"\"}"));
        }
        catch (final Exception ex) {
            ex.printStackTrace();
        }
    }
    
    public static void oo88o8O(final String key) {
        try {
            a.\u3007080.put(key, new JSONObject("{\"values\":{\"borderColor\":\"#000000\",\"data\":\"description\",\"paddingRight\":12,\"borderSize\":0,\"useAtomFontFamily\":false,\"align\":\"0\",\"timingStart\":0,\"marginRight\":0,\"paddingBottom\":10,\"fontFamily\":\"PingFangSC-Regular\",\"atomFontFamilyPreview\":1,\"atomBgStartColorPreview\":1,\"borderStyle\":\"solid\",\"fontWeight\":500,\"lineCount\":2,\"height\":40,\"textMaterialCenterCalcColor\":-999,\"bindEventBy\":\"none\",\"textAlign\":\"left\",\"useAtomBgEndColor\":false,\"fontModel\":\"\",\"i18n\":{\"de\":\"\",\"hi\":\"\",\"ru\":\"\",\"pt\":\"\",\"japan\":\"\",\"zhHant\":\"\",\"en\":\"\",\"cn\":\"\",\"it\":\"\",\"fr\":\"\",\"es\":\"\",\"korea\":\"\",\"vi\":\"\",\"th\":\"\",\"id\":\"\"},\"marginLeft\":0,\"textFlowType\":\"0\",\"useAtomBgColor\":false,\"atomBgEndColor\":[0],\"atomBgEndColorPreview\":1,\"borderRadius\":0,\"clickTigger\":\"click\",\"heightMode\":\"auto\",\"marginBottom\":0,\"fontSize\":15,\"position\":\"0\",\"paddingLeft\":12,\"colorCg\":0,\"atomBgStartColor\":[0],\"color\":\"#999999\",\"lineLimit\":false,\"atomBtnAnimation\":[0],\"useAtomBtnAnimation\":false,\"isDataFixed\":false,\"bgColor\":\"transparent\",\"allowTextFlow\":false,\"paddingTop\":10,\"materialCenterData\":-999,\"bgMaterialCenterCalcColor\":-999,\"atomBgColorPreview\":1,\"lineFeed\":false,\"clickArea\":\"creative\",\"widthMode\":\"flex\",\"atomFontFamily\":[0],\"lineThrough\":false,\"letterSpacing\":0,\"borderMaterialCenterCalcColor\":-999,\"atomBgColor\":[0],\"width\":1,\"useAtomBgStartColor\":false,\"lineHeight\":1.2,\"atomBtnAnimationPreview\":1,\"textFlowDuration\":3,\"marginTop\":0,\"timingEnd\":65536},\"areaType\":2,\"themeValues\":{\"color\":\"#ffffff\"},\"clickType\":\"subtitle\"}"));
        }
        catch (final Exception ex) {
            ex.printStackTrace();
        }
    }
    
    public static void ooo\u30078oO(final String key) {
        try {
            a.\u3007080.put(key, new JSONObject("{\"values\":{\"imageObjectFit\":\"none\",\"borderColor\":\"#000000\",\"data\":\"\",\"paddingRight\":0,\"borderSize\":0,\"interactHiddenTime\":5,\"fixBorderStyleType\":0,\"align\":\"0\",\"isDataFixed\":false,\"timingStart\":0,\"marginRight\":0,\"paddingBottom\":0,\"interactSlideDirection\":\"2\",\"bgColor\":\"transparent\",\"bgGauseBlurRadius\":3,\"paddingTop\":0,\"materialCenterData\":-999,\"borderStyle\":\"solid\",\"bgMaterialCenterCalcColor\":-999,\"imageType\":\"static-image\",\"height\":1,\"bindEventBy\":\"none\",\"justifyHorizontal\":\"space-around\",\"clickArea\":\"creative\",\"widthMode\":\"flex\",\"alignItems\":\"flex-start\",\"bgGauseBlur\":false,\"interactText\":\"\",\"interactI18n\":{\"de\":\"\",\"hi\":\"\",\"ru\":\"\",\"pt\":\"\",\"japan\":\"\",\"zhHant\":\"\",\"en\":\"\",\"cn\":\"\",\"it\":\"\",\"fr\":\"\",\"es\":\"\",\"korea\":\"\",\"vi\":\"\",\"th\":\"\",\"id\":\"\"},\"marginLeft\":0,\"borderMaterialCenterCalcColor\":-999,\"interactValidate\":false,\"borderRadius\":0,\"interactType\":\"\",\"clickTigger\":\"click\",\"justifyVertical\":\"flex-start\",\"width\":1,\"heightMode\":\"flex\",\"interactVisibleTime\":0,\"marginBottom\":0,\"position\":\"0\",\"paddingLeft\":0,\"marginTop\":0,\"timingEnd\":65536,\"useLeisureInteract\":false},\"areaType\":2,\"themeValues\":{},\"clickType\":\"image\"}"));
        }
        catch (final Exception ex) {
            ex.printStackTrace();
        }
    }
    
    public static void oo\u3007(final String key) {
        try {
            a.\u3007080.put(key, new JSONObject("{\"values\":{\"borderColor\":\"#2a90d7\",\"data\":\"button_text\",\"paddingRight\":0,\"borderSize\":0,\"useAtomFontFamily\":false,\"align\":\"0\",\"timingStart\":0,\"marginRight\":0,\"paddingBottom\":0,\"fontFamily\":\"PingFangSC-Regular\",\"atomFontFamilyPreview\":1,\"atomBgStartColorPreview\":1,\"borderStyle\":\"solid\",\"fontWeight\":400,\"lineCount\":2,\"height\":40,\"textMaterialCenterCalcColor\":-999,\"bindEventBy\":\"none\",\"textAlign\":\"center\",\"useAtomBgEndColor\":false,\"fontModel\":\"\",\"i18n\":{\"de\":\"\",\"hi\":\"\",\"ru\":\"\",\"pt\":\"\",\"japan\":\"\",\"zhHant\":\"\",\"en\":\"\",\"cn\":\"\",\"it\":\"\",\"fr\":\"\",\"es\":\"\",\"korea\":\"\",\"vi\":\"\",\"th\":\"\",\"id\":\"\"},\"marginLeft\":0,\"textFlowType\":\"0\",\"useAtomBgColor\":false,\"atomBgEndColor\":[0],\"atomBgEndColorPreview\":1,\"borderRadius\":0,\"clickTigger\":\"click\",\"heightMode\":\"auto\",\"marginBottom\":0,\"fontSize\":12,\"position\":\"0\",\"paddingLeft\":0,\"colorCg\":0,\"atomBgStartColor\":[0],\"color\":\"#FFFFFF\",\"lineLimit\":false,\"atomBtnAnimation\":[0],\"useAtomBtnAnimation\":false,\"isDataFixed\":false,\"atomButtonText\":true,\"bgColor\":\"#ff113b\",\"iconType\":\"0\",\"allowTextFlow\":false,\"paddingTop\":0,\"materialCenterData\":-999,\"bgMaterialCenterCalcColor\":-999,\"atomBgColorPreview\":1,\"showIcon\":false,\"lineFeed\":false,\"clickArea\":\"creative\",\"widthMode\":\"auto\",\"atomFontFamily\":[0],\"lineThrough\":false,\"letterSpacing\":0,\"borderMaterialCenterCalcColor\":-999,\"atomBgColor\":[0],\"width\":320,\"useAtomBgStartColor\":false,\"lineHeight\":1.2,\"atomBtnAnimationPreview\":1,\"textFlowDuration\":3,\"marginTop\":0,\"timingEnd\":65536},\"areaType\":2,\"clickType\":\"button\"}"));
        }
        catch (final Exception ex) {
            ex.printStackTrace();
        }
    }
    
    public static void o\u30070(final String key) {
        try {
            a.\u3007080.put(key, new JSONObject("{\"values\":{\"borderColor\":\"#000000\",\"data\":\"Please enter the content\",\"paddingRight\":4,\"borderSize\":0,\"useAtomFontFamily\":false,\"align\":\"0\",\"timingStart\":0,\"marginRight\":0,\"paddingBottom\":4,\"fontFamily\":\"PingFangSC-Regular\",\"atomFontFamilyPreview\":1,\"atomBgStartColorPreview\":1,\"borderStyle\":\"solid\",\"fontWeight\":400,\"lineCount\":2,\"height\":40,\"textMaterialCenterCalcColor\":-999,\"bindEventBy\":\"none\",\"textAlign\":\"center\",\"useAtomBgEndColor\":false,\"fontModel\":\"\",\"i18n\":{\"de\":\"\",\"hi\":\"\",\"ru\":\"\",\"pt\":\"\",\"japan\":\"\",\"zhHant\":\"\",\"en\":\"\",\"cn\":\"\",\"it\":\"\",\"fr\":\"\",\"es\":\"\",\"korea\":\"\",\"vi\":\"\",\"th\":\"\",\"id\":\"\"},\"marginLeft\":0,\"textFlowType\":\"0\",\"useAtomBgColor\":false,\"atomBgEndColor\":[0],\"atomBgEndColorPreview\":1,\"borderRadius\":20,\"clickTigger\":\"click\",\"heightMode\":\"auto\",\"marginBottom\":0,\"fontSize\":20,\"position\":\"0\",\"paddingLeft\":4,\"colorCg\":0,\"atomBgStartColor\":[0],\"color\":\"#ffffff\",\"lineLimit\":false,\"atomBtnAnimation\":[0],\"useAtomBtnAnimation\":false,\"isDataFixed\":true,\"bgColor\":\"rgba(22, 24, 35, 0.5)\",\"allowTextFlow\":false,\"paddingTop\":4,\"materialCenterData\":-999,\"bgMaterialCenterCalcColor\":-999,\"atomBgColorPreview\":1,\"lineFeed\":false,\"clickArea\":\"none\",\"widthMode\":\"auto\",\"atomFontFamily\":[0],\"lineThrough\":false,\"letterSpacing\":0,\"borderMaterialCenterCalcColor\":-999,\"atomBgColor\":[0],\"width\":320,\"useAtomBgStartColor\":false,\"lineHeight\":1,\"atomBtnAnimationPreview\":1,\"textFlowDuration\":3,\"marginTop\":0,\"timingEnd\":65536},\"areaType\":0,\"clickType\":\"muted\"}"));
        }
        catch (final Exception ex) {
            ex.printStackTrace();
        }
    }
    
    public static void o\u30070OOo\u30070(final String key) {
        try {
            a.\u3007080.put(key, new JSONObject("{\"values\":{\"borderColor\":\"#000000\",\"data\":\"video\",\"bulletScreenStart\":0,\"paddingRight\":0,\"borderSize\":0,\"interactHiddenTime\":5,\"fixBorderStyleType\":0,\"align\":\"0\",\"showPlayButton\":true,\"isDataFixed\":false,\"timingStart\":0,\"bulletScreenData\":\"dynamic_creative.danmu\",\"marginRight\":0,\"bulletScreenEnd\":0,\"paddingBottom\":0,\"interactSlideDirection\":\"2\",\"bgColor\":\"transparent\",\"paddingTop\":0,\"materialCenterData\":-999,\"borderStyle\":\"solid\",\"bgMaterialCenterCalcColor\":-999,\"height\":0.5625,\"bindEventBy\":\"none\",\"clickArea\":\"video\",\"widthMode\":\"flex\",\"interactText\":\"\",\"interactI18n\":{\"de\":\"\",\"hi\":\"\",\"ru\":\"\",\"pt\":\"\",\"japan\":\"\",\"zhHant\":\"\",\"en\":\"\",\"cn\":\"\",\"it\":\"\",\"fr\":\"\",\"es\":\"\",\"korea\":\"\",\"vi\":\"\",\"th\":\"\",\"id\":\"\"},\"marginLeft\":0,\"borderMaterialCenterCalcColor\":-999,\"showTimeProgress\":true,\"interactValidate\":false,\"borderRadius\":0,\"interactType\":\"\",\"clickTigger\":\"click\",\"showBulletScreen\":false,\"width\":1,\"heightMode\":\"scale\",\"interactVisibleTime\":0,\"marginBottom\":0,\"position\":\"0\",\"paddingLeft\":0,\"marginTop\":0,\"timingEnd\":65536},\"areaType\":99,\"themeValues\":{},\"clickType\":\"video\"}"));
        }
        catch (final Exception ex) {
            ex.printStackTrace();
        }
    }
    
    public static void o\u30078(final String key) {
        try {
            a.\u3007080.put(key, new JSONObject("{\"values\":{\"imageObjectFit\":\"contain\",\"borderColor\":\"#000000\",\"data\":\"image.0\",\"paddingRight\":0,\"borderSize\":0,\"interactHiddenTime\":5,\"align\":\"0\",\"showPlayButton\":true,\"timingStart\":0,\"marginRight\":0,\"bulletScreenEnd\":0,\"paddingBottom\":0,\"interactSlideDirection\":\"2\",\"borderStyle\":\"solid\",\"imageType\":\"static-image\",\"height\":1.52,\"bindEventBy\":\"none\",\"image\":{\"imageObjectFit\":\"contain\",\"clickArea\":\"creative\",\"data\":\"image.0\",\"clickTigger\":\"click\",\"materialCenterData\":-999,\"imageType\":\"static-image\",\"isDataFixed\":false},\"alignItems\":\"flex-start\",\"interactI18n\":{\"de\":\"\",\"hi\":\"\",\"ru\":\"\",\"pt\":\"\",\"japan\":\"\",\"zhHant\":\"\",\"en\":\"\",\"cn\":\"\",\"it\":\"\",\"fr\":\"\",\"es\":\"\",\"korea\":\"\",\"vi\":\"\",\"th\":\"\",\"id\":\"\"},\"marginLeft\":0,\"borderRadius\":0,\"clickTigger\":\"click\",\"justifyVertical\":\"flex-start\",\"heightMode\":\"scale\",\"marginBottom\":0,\"position\":\"0\",\"paddingLeft\":0,\"bulletScreenStart\":0,\"fixBorderStyleType\":0,\"video\":{\"showTimeProgress\":false,\"bulletScreenEnd\":0,\"clickArea\":\"video\",\"data\":\"video\",\"showBulletScreen\":false,\"bulletScreenStart\":0,\"clickTigger\":\"click\",\"materialCenterData\":-999,\"showPlayButton\":true,\"isDataFixed\":false,\"bulletScreenData\":\"dynamic_creative.danmu\"},\"isDataFixed\":false,\"bulletScreenData\":\"dynamic_creative.danmu\",\"bgColor\":\"transparent\",\"paddingTop\":0,\"materialCenterData\":-999,\"bgMaterialCenterCalcColor\":-999,\"justifyHorizontal\":\"space-around\",\"clickArea\":\"creative\",\"widthMode\":\"flex\",\"interactText\":\"\",\"borderMaterialCenterCalcColor\":-999,\"showTimeProgress\":true,\"interactValidate\":false,\"interactType\":\"\",\"showBulletScreen\":false,\"width\":1,\"interactVisibleTime\":0,\"marginTop\":0,\"timingEnd\":65536},\"areaType\":2,\"themeValues\":{},\"clickType\":\"image\"}"));
        }
        catch (final Exception ex) {
            ex.printStackTrace();
        }
    }
    
    public static void o\u30078oOO88(final String key) {
        try {
            a.\u3007080.put(key, new JSONObject("{\"values\":{\"borderColor\":\"#1A73E8\",\"data\":\"button_text\",\"paddingRight\":4,\"borderSize\":0,\"useAtomFontFamily\":false,\"align\":\"0\",\"timingStart\":0,\"marginRight\":0,\"paddingBottom\":10,\"fontFamily\":\"PingFangSC-Regular\",\"atomFontFamilyPreview\":1,\"atomBgStartColorPreview\":1,\"borderStyle\":\"solid\",\"fontWeight\":400,\"lineCount\":2,\"height\":40,\"textMaterialCenterCalcColor\":-999,\"bindEventBy\":\"none\",\"textAlign\":\"center\",\"useAtomBgEndColor\":false,\"fontModel\":\"\",\"i18n\":{\"de\":\"\",\"hi\":\"\",\"ru\":\"\",\"pt\":\"\",\"japan\":\"\",\"zhHant\":\"\",\"en\":\"\",\"cn\":\"\",\"it\":\"\",\"fr\":\"\",\"es\":\"\",\"korea\":\"\",\"vi\":\"\",\"th\":\"\",\"id\":\"\"},\"marginLeft\":0,\"textFlowType\":\"0\",\"useAtomBgColor\":false,\"atomBgEndColor\":[0],\"atomBgEndColorPreview\":1,\"borderRadius\":2,\"clickTigger\":\"click\",\"heightMode\":\"auto\",\"marginBottom\":0,\"fontSize\":12,\"position\":\"0\",\"paddingLeft\":4,\"colorCg\":0,\"atomBgStartColor\":[0],\"color\":\"#ffffff\",\"lineLimit\":false,\"atomBtnAnimation\":[0],\"useAtomBtnAnimation\":false,\"isDataFixed\":false,\"atomButtonText\":true,\"bgColor\":\"#1A73E8\",\"iconType\":\"0\",\"allowTextFlow\":false,\"paddingTop\":10,\"materialCenterData\":-999,\"bgMaterialCenterCalcColor\":-999,\"atomBgColorPreview\":1,\"showIcon\":false,\"lineFeed\":false,\"clickArea\":\"creative\",\"widthMode\":\"flex\",\"atomFontFamily\":[0],\"lineThrough\":false,\"letterSpacing\":0,\"borderMaterialCenterCalcColor\":-999,\"atomBgColor\":[0],\"width\":1,\"useAtomBgStartColor\":false,\"lineHeight\":1.2,\"atomBtnAnimationPreview\":1,\"textFlowDuration\":3,\"marginTop\":0,\"timingEnd\":65536},\"areaType\":2,\"clickType\":\"button\"}"));
        }
        catch (final Exception ex) {
            ex.printStackTrace();
        }
    }
    
    public static void o\u3007O(final String key) {
        try {
            a.\u3007080.put(key, new JSONObject("{\"values\":{\"borderColor\":\"#000000\",\"data\":\"\",\"paddingRight\":12,\"borderSize\":0,\"align\":\"0\",\"isDataFixed\":false,\"timingStart\":0,\"marginRight\":0,\"paddingBottom\":10,\"bgColor\":\"transparent\",\"paddingTop\":10,\"materialCenterData\":-999,\"borderStyle\":\"solid\",\"bgMaterialCenterCalcColor\":-999,\"height\":40,\"bindEventBy\":\"none\",\"clickArea\":\"creative\",\"widthMode\":\"flex\",\"marginLeft\":0,\"borderMaterialCenterCalcColor\":-999,\"borderRadius\":0,\"clickTigger\":\"click\",\"width\":1,\"heightMode\":\"fixed\",\"marginBottom\":0,\"position\":\"0\",\"paddingLeft\":12,\"marginTop\":0,\"timingEnd\":65536},\"areaType\":2,\"themeValues\":{},\"clickType\":\"\"}"));
        }
        catch (final Exception ex) {
            ex.printStackTrace();
        }
    }
    
    public static void o\u3007O8\u3007\u3007o(final String key) {
        try {
            a.\u3007080.put(key, new JSONObject("{\"values\":{\"borderColor\":\"#000000\",\"data\":\"text\",\"paddingRight\":12,\"borderSize\":0,\"useAtomFontFamily\":false,\"align\":\"0\",\"timingStart\":0,\"marginRight\":0,\"paddingBottom\":10,\"fontFamily\":\"PingFangSC-Regular\",\"atomFontFamilyPreview\":1,\"atomBgStartColorPreview\":1,\"borderStyle\":\"solid\",\"fontWeight\":400,\"lineCount\":2,\"height\":40,\"textMaterialCenterCalcColor\":-999,\"bindEventBy\":\"none\",\"textAlign\":\"left\",\"useAtomBgEndColor\":false,\"fontModel\":\"\",\"i18n\":{\"de\":\"\",\"hi\":\"\",\"ru\":\"\",\"pt\":\"\",\"japan\":\"\",\"zhHant\":\"\",\"en\":\"\",\"cn\":\"\",\"it\":\"\",\"fr\":\"\",\"es\":\"\",\"korea\":\"\",\"vi\":\"\",\"th\":\"\",\"id\":\"\"},\"marginLeft\":0,\"textFlowType\":\"0\",\"useAtomBgColor\":false,\"atomBgEndColor\":[0],\"atomBgEndColorPreview\":1,\"borderRadius\":0,\"clickTigger\":\"click\",\"heightMode\":\"auto\",\"marginBottom\":0,\"fontSize\":12,\"position\":\"0\",\"paddingLeft\":12,\"colorCg\":0,\"atomBgStartColor\":[0],\"color\":\"#161823\",\"lineLimit\":false,\"atomBtnAnimation\":[0],\"useAtomBtnAnimation\":false,\"isDataFixed\":false,\"bgColor\":\"transparent\",\"allowTextFlow\":false,\"paddingTop\":10,\"materialCenterData\":-999,\"bgMaterialCenterCalcColor\":-999,\"atomBgColorPreview\":1,\"lineFeed\":false,\"clickArea\":\"creative\",\"widthMode\":\"flex\",\"atomFontFamily\":[0],\"lineThrough\":false,\"letterSpacing\":0,\"borderMaterialCenterCalcColor\":-999,\"atomBgColor\":[0],\"width\":1,\"useAtomBgStartColor\":false,\"lineHeight\":1.2,\"atomBtnAnimationPreview\":1,\"textFlowDuration\":3,\"marginTop\":0,\"timingEnd\":65536},\"areaType\":2,\"themeValues\":{\"color\":\"#ffffff\"},\"clickType\":\"\"}"));
        }
        catch (final Exception ex) {
            ex.printStackTrace();
        }
    }
    
    public static void o\u3007\u30070\u3007(final String key) {
        try {
            a.\u3007080.put(key, new JSONObject("{\"values\":{\"borderColor\":\"#2a90d7\",\"data\":\"dynamic_creative.app_version\",\"paddingRight\":4,\"borderSize\":0,\"useAtomFontFamily\":false,\"align\":\"0\",\"timingStart\":0,\"marginRight\":0,\"paddingBottom\":4,\"fontFamily\":\"PingFangSC-Regular\",\"atomFontFamilyPreview\":1,\"atomBgStartColorPreview\":1,\"borderStyle\":\"solid\",\"fontWeight\":400,\"lineCount\":2,\"height\":40,\"textMaterialCenterCalcColor\":-999,\"bindEventBy\":\"none\",\"textAlign\":\"center\",\"useAtomBgEndColor\":false,\"fontModel\":\"\",\"i18n\":{\"de\":\"\",\"hi\":\"\",\"ru\":\"\",\"pt\":\"\",\"japan\":\"\",\"zhHant\":\"\",\"en\":\"\",\"cn\":\"\",\"it\":\"\",\"fr\":\"\",\"es\":\"\",\"korea\":\"\",\"vi\":\"\",\"th\":\"\",\"id\":\"\"},\"marginLeft\":0,\"textFlowType\":\"0\",\"useAtomBgColor\":false,\"atomBgEndColor\":[0],\"atomBgEndColorPreview\":1,\"borderRadius\":0,\"clickTigger\":\"click\",\"heightMode\":\"auto\",\"marginBottom\":0,\"fontSize\":12,\"position\":\"0\",\"paddingLeft\":4,\"colorCg\":0,\"atomBgStartColor\":[0],\"color\":\"#B0B0B0\",\"lineLimit\":false,\"atomBtnAnimation\":[0],\"useAtomBtnAnimation\":false,\"isDataFixed\":false,\"bgColor\":\"transparent\",\"allowTextFlow\":false,\"paddingTop\":4,\"materialCenterData\":-999,\"bgMaterialCenterCalcColor\":-999,\"atomBgColorPreview\":1,\"lineFeed\":false,\"clickArea\":\"creative\",\"widthMode\":\"auto\",\"atomFontFamily\":[0],\"lineThrough\":false,\"letterSpacing\":0,\"borderMaterialCenterCalcColor\":-999,\"atomBgColor\":[0],\"width\":320,\"useAtomBgStartColor\":false,\"lineHeight\":1.2,\"atomBtnAnimationPreview\":1,\"textFlowDuration\":3,\"marginTop\":0,\"timingEnd\":65536},\"areaType\":2,\"themeValues\":{\"color\":\"#ffffff\"},\"clickType\":\"\"}"));
        }
        catch (final Exception ex) {
            ex.printStackTrace();
        }
    }
    
    public static void \u300700(final String key) {
        try {
            a.\u3007080.put(key, new JSONObject("{\"values\":{\"borderColor\":\"#000000\",\"data\":\"app.score\",\"paddingRight\":2,\"borderSize\":0,\"useAtomFontFamily\":false,\"align\":\"0\",\"timingStart\":0,\"marginRight\":0,\"paddingBottom\":2,\"fontFamily\":\"PingFangSC-Regular\",\"atomFontFamilyPreview\":1,\"atomBgStartColorPreview\":1,\"borderStyle\":\"solid\",\"fontWeight\":400,\"lineCount\":2,\"height\":40,\"textMaterialCenterCalcColor\":-999,\"bindEventBy\":\"none\",\"textAlign\":\"center\",\"useAtomBgEndColor\":false,\"fontModel\":\"\",\"i18n\":{\"de\":\"\",\"hi\":\"\",\"ru\":\"\",\"pt\":\"\",\"japan\":\"\",\"zhHant\":\"\",\"en\":\"\",\"cn\":\"\",\"it\":\"\",\"fr\":\"\",\"es\":\"\",\"korea\":\"\",\"vi\":\"\",\"th\":\"\",\"id\":\"\"},\"marginLeft\":0,\"textFlowType\":\"0\",\"useAtomBgColor\":false,\"atomBgEndColor\":[0],\"atomBgEndColorPreview\":1,\"borderRadius\":0,\"clickTigger\":\"click\",\"heightMode\":\"auto\",\"marginBottom\":0,\"fontSize\":12,\"position\":\"0\",\"paddingLeft\":2,\"colorCg\":0,\"atomBgStartColor\":[0],\"color\":\"#161823\",\"lineLimit\":false,\"atomBtnAnimation\":[0],\"useAtomBtnAnimation\":false,\"isDataFixed\":false,\"bgColor\":\"transparent\",\"allowTextFlow\":false,\"paddingTop\":2,\"materialCenterData\":-999,\"bgMaterialCenterCalcColor\":-999,\"atomBgColorPreview\":1,\"lineFeed\":false,\"clickArea\":\"creative\",\"widthMode\":\"auto\",\"atomFontFamily\":[0],\"lineThrough\":false,\"letterSpacing\":0,\"borderMaterialCenterCalcColor\":-999,\"atomBgColor\":[0],\"width\":320,\"useAtomBgStartColor\":false,\"lineHeight\":1,\"atomBtnAnimationPreview\":1,\"textFlowDuration\":3,\"marginTop\":0,\"timingEnd\":65536},\"areaType\":2,\"themeValues\":{\"color\":\"#ffffff\"},\"clickType\":\"star\"}"));
        }
        catch (final Exception ex) {
            ex.printStackTrace();
        }
    }
    
    public static void \u30070000OOO(final String key) {
        try {
            a.\u3007080.put(key, new JSONObject("{\"values\":{\"borderColor\":\"#000000\",\"data\":\"title\",\"paddingRight\":12,\"borderSize\":0,\"useAtomFontFamily\":false,\"align\":\"0\",\"timingStart\":0,\"highlightWord\":false,\"marginRight\":0,\"paddingBottom\":10,\"fontFamily\":\"PingFangSC-Regular\",\"atomFontFamilyPreview\":1,\"atomBgStartColorPreview\":1,\"borderStyle\":\"solid\",\"fontWeight\":500,\"lineCount\":2,\"height\":40,\"textMaterialCenterCalcColor\":-999,\"bindEventBy\":\"none\",\"textAlign\":\"left\",\"useAtomBgEndColor\":false,\"fontModel\":\"\",\"i18n\":{\"de\":\"\",\"hi\":\"\",\"ru\":\"\",\"pt\":\"\",\"japan\":\"\",\"zhHant\":\"\",\"en\":\"\",\"cn\":\"\",\"it\":\"\",\"fr\":\"\",\"es\":\"\",\"korea\":\"\",\"vi\":\"\",\"th\":\"\",\"id\":\"\"},\"marginLeft\":0,\"textFlowType\":\"0\",\"useAtomBgColor\":false,\"atomBgEndColor\":[0],\"atomBgEndColorPreview\":1,\"borderRadius\":0,\"clickTigger\":\"click\",\"heightMode\":\"auto\",\"marginBottom\":0,\"fontSize\":19,\"quoteWord\":false,\"position\":\"0\",\"paddingLeft\":12,\"colorCg\":0,\"atomBgStartColor\":[0],\"color\":\"#222222\",\"lineLimit\":false,\"atomBtnAnimation\":[0],\"useAtomBtnAnimation\":false,\"isDataFixed\":false,\"bgColor\":\"transparent\",\"allowTextFlow\":false,\"paddingTop\":10,\"materialCenterData\":-999,\"bgMaterialCenterCalcColor\":-999,\"atomBgColorPreview\":1,\"lineFeed\":false,\"clickArea\":\"creative\",\"widthMode\":\"flex\",\"atomFontFamily\":[0],\"lineThrough\":false,\"letterSpacing\":0,\"borderMaterialCenterCalcColor\":-999,\"atomBgColor\":[0],\"width\":1,\"useAtomBgStartColor\":false,\"lineHeight\":1.4,\"atomBtnAnimationPreview\":1,\"textFlowDuration\":3,\"marginTop\":0,\"timingEnd\":65536},\"areaType\":2,\"themeValues\":{\"color\":\"#ffffff\"},\"clickType\":\"title\"}"));
        }
        catch (final Exception ex) {
            ex.printStackTrace();
        }
    }
    
    public static void \u300700\u30078(final String key) {
        try {
            a.\u3007080.put(key, new JSONObject("{\"values\":{\"borderColor\":\"#1A73E8\",\"data\":\"button_text\",\"paddingRight\":4,\"borderSize\":1,\"useAtomFontFamily\":false,\"align\":\"0\",\"timingStart\":0,\"marginRight\":0,\"paddingBottom\":4,\"fontFamily\":\"PingFangSC-Regular\",\"atomFontFamilyPreview\":1,\"atomBgStartColorPreview\":1,\"borderStyle\":\"solid\",\"fontWeight\":400,\"lineCount\":2,\"height\":40,\"textMaterialCenterCalcColor\":-999,\"bindEventBy\":\"none\",\"textAlign\":\"center\",\"useAtomBgEndColor\":false,\"fontModel\":\"\",\"i18n\":{\"de\":\"\",\"hi\":\"\",\"ru\":\"\",\"pt\":\"\",\"japan\":\"\",\"zhHant\":\"\",\"en\":\"\",\"cn\":\"\",\"it\":\"\",\"fr\":\"\",\"es\":\"\",\"korea\":\"\",\"vi\":\"\",\"th\":\"\",\"id\":\"\"},\"marginLeft\":0,\"textFlowType\":\"0\",\"useAtomBgColor\":false,\"atomBgEndColor\":[0],\"atomBgEndColorPreview\":1,\"borderRadius\":2,\"clickTigger\":\"click\",\"heightMode\":\"auto\",\"marginBottom\":0,\"fontSize\":12,\"position\":\"0\",\"paddingLeft\":4,\"colorCg\":0,\"atomBgStartColor\":[0],\"color\":\"#1A73E8\",\"lineLimit\":false,\"atomBtnAnimation\":[0],\"useAtomBtnAnimation\":false,\"isDataFixed\":false,\"atomButtonText\":true,\"bgColor\":\"transparent\",\"iconType\":\"0\",\"allowTextFlow\":false,\"paddingTop\":4,\"materialCenterData\":-999,\"bgMaterialCenterCalcColor\":-999,\"atomBgColorPreview\":1,\"showIcon\":false,\"lineFeed\":false,\"clickArea\":\"creative\",\"widthMode\":\"auto\",\"atomFontFamily\":[0],\"lineThrough\":false,\"letterSpacing\":0,\"borderMaterialCenterCalcColor\":-999,\"atomBgColor\":[0],\"width\":320,\"useAtomBgStartColor\":false,\"lineHeight\":1.2,\"atomBtnAnimationPreview\":1,\"textFlowDuration\":3,\"marginTop\":0,\"timingEnd\":65536},\"areaType\":2,\"clickType\":\"button\"}"));
        }
        catch (final Exception ex) {
            ex.printStackTrace();
        }
    }
    
    public static void \u3007080(final String key) {
        try {
            a.\u3007080.put(key, new JSONObject("{\"values\":{\"imageObjectFit\":\"none\",\"borderColor\":\"#000000\",\"data\":\"image.0.url\",\"paddingRight\":0,\"borderSize\":0,\"interactHiddenTime\":5,\"fixBorderStyleType\":0,\"align\":\"0\",\"isDataFixed\":false,\"timingStart\":0,\"marginRight\":0,\"paddingBottom\":0,\"interactSlideDirection\":\"2\",\"bgColor\":\"transparent\",\"bgGauseBlurRadius\":3,\"paddingTop\":0,\"materialCenterData\":-999,\"borderStyle\":\"solid\",\"bgMaterialCenterCalcColor\":-999,\"imageType\":\"static-image\",\"height\":1,\"bindEventBy\":\"none\",\"justifyHorizontal\":\"space-around\",\"clickArea\":\"creative\",\"widthMode\":\"flex\",\"alignItems\":\"flex-start\",\"bgGauseBlur\":false,\"interactText\":\"\",\"interactI18n\":{\"de\":\"\",\"hi\":\"\",\"ru\":\"\",\"pt\":\"\",\"japan\":\"\",\"zhHant\":\"\",\"en\":\"\",\"cn\":\"\",\"it\":\"\",\"fr\":\"\",\"es\":\"\",\"korea\":\"\",\"vi\":\"\",\"th\":\"\",\"id\":\"\"},\"marginLeft\":0,\"borderMaterialCenterCalcColor\":-999,\"interactValidate\":false,\"borderRadius\":0,\"interactType\":\"\",\"clickTigger\":\"click\",\"justifyVertical\":\"flex-start\",\"width\":1,\"heightMode\":\"scale\",\"interactVisibleTime\":0,\"marginBottom\":0,\"position\":\"0\",\"paddingLeft\":0,\"marginTop\":0,\"timingEnd\":65536,\"useLeisureInteract\":false},\"areaType\":2,\"themeValues\":{},\"clickType\":\"image\"}"));
        }
        catch (final Exception ex) {
            ex.printStackTrace();
        }
    }
    
    public static void \u300708O8o\u30070(final String key) {
        try {
            a.\u3007080.put(key, new JSONObject("{\"values\":{\"borderColor\":\"rgba(22, 24, 35, 0.34)\",\"data\":\"\",\"paddingRight\":6,\"borderSize\":0,\"useAtomFontFamily\":false,\"align\":\"0\",\"timingStart\":0,\"marginRight\":0,\"paddingBottom\":6,\"fontFamily\":\"PingFangSC-Regular\",\"atomFontFamilyPreview\":1,\"atomBgStartColorPreview\":1,\"borderStyle\":\"solid\",\"fontWeight\":400,\"lineCount\":2,\"height\":40,\"textMaterialCenterCalcColor\":-999,\"bindEventBy\":\"none\",\"textAlign\":\"center\",\"useAtomBgEndColor\":false,\"fontModel\":\"\",\"i18n\":{\"de\":\"\",\"hi\":\"\",\"ru\":\"\",\"pt\":\"\",\"japan\":\"\",\"zhHant\":\"\",\"en\":\"\",\"cn\":\"\",\"it\":\"\",\"fr\":\"\",\"es\":\"\",\"korea\":\"\",\"vi\":\"\",\"th\":\"\",\"id\":\"\"},\"marginLeft\":0,\"textFlowType\":\"0\",\"useAtomBgColor\":false,\"atomBgEndColor\":[0],\"atomBgEndColorPreview\":1,\"borderRadius\":14,\"clickTigger\":\"click\",\"heightMode\":\"auto\",\"marginBottom\":0,\"fontSize\":16,\"position\":\"0\",\"paddingLeft\":6,\"colorCg\":0,\"atomBgStartColor\":[0],\"color\":\"#ffffff\",\"lineLimit\":false,\"atomBtnAnimation\":[0],\"useAtomBtnAnimation\":false,\"isDataFixed\":true,\"bgColor\":\"rgba(22, 24, 35, 0.4)\",\"allowTextFlow\":false,\"paddingTop\":6,\"materialCenterData\":-999,\"bgMaterialCenterCalcColor\":-999,\"atomBgColorPreview\":1,\"lineFeed\":true,\"clickArea\":\"creative\",\"widthMode\":\"auto\",\"atomFontFamily\":[0],\"lineThrough\":false,\"letterSpacing\":0,\"borderMaterialCenterCalcColor\":-999,\"atomBgColor\":[0],\"width\":320,\"useAtomBgStartColor\":false,\"lineHeight\":1,\"atomBtnAnimationPreview\":1,\"textFlowDuration\":3,\"marginTop\":0,\"timingEnd\":65536},\"areaType\":2,\"clickType\":\"\"}"));
        }
        catch (final Exception ex) {
            ex.printStackTrace();
        }
    }
    
    public static void \u30070\u3007O0088o(final String key) {
        try {
            a.\u3007080.put(key, new JSONObject("{\"values\":{\"borderColor\":\"rgba(255, 255, 255, 0.2)\",\"data\":\"text\",\"paddingRight\":6,\"borderSize\":0,\"useAtomFontFamily\":false,\"rules\":[],\"align\":\"0\",\"timingStart\":0,\"marginRight\":0,\"paddingBottom\":6,\"fontFamily\":\"PingFangSC-Regular\",\"atomFontFamilyPreview\":1,\"atomBgStartColorPreview\":1,\"borderStyle\":\"solid\",\"fontWeight\":400,\"lineCount\":2,\"height\":40,\"textMaterialCenterCalcColor\":-999,\"triggerActions\":[],\"bindEventBy\":\"none\",\"textMaterialCenterCalcColorSource\":1,\"textAlign\":\"left\",\"useAtomBgEndColor\":false,\"fontModel\":\"\",\"i18n\":{\"aa\":\"\",\"de\":\"\",\"hi\":\"\",\"ru\":\"\",\"pt\":\"\",\"japan\":\"\",\"zhHant\":\"\",\"en\":\"\",\"cn\":\"\",\"it\":\"\",\"fr\":\"\",\"my\":\"\",\"es\":\"\",\"korea\":\"\",\"vi\":\"\",\"th\":\"\",\"id\":\"\",\"pl\":\"\",\"tr\":\"\"},\"marginLeft\":8,\"textFlowType\":\"0\",\"useAtomBgColor\":false,\"atomBgEndColor\":[0],\"triggerConditions\":[],\"atomBgEndColorPreview\":1,\"borderRadius\":14,\"clickTigger\":\"click\",\"heightMode\":\"auto\",\"marginBottom\":0,\"fontSize\":16,\"position\":\"0\",\"paddingLeft\":6,\"colorCg\":0,\"borderMaterialCenterCalcColorSource\":1,\"atomBgStartColor\":[0],\"textFlowSpeed\":3,\"color\":\"#FFFFFF\",\"lineLimit\":false,\"atomBtnAnimation\":[0],\"useAtomBtnAnimation\":false,\"isDataFixed\":true,\"bgColor\":\"rgba(22, 24, 35, 0.5)\",\"allowTextFlow\":false,\"paddingTop\":6,\"materialCenterData\":-999,\"bgMaterialCenterCalcColor\":-999,\"atomBgColorPreview\":1,\"openPlayableLandingPage\":false,\"lineFeed\":false,\"clickArea\":\"creative\",\"widthMode\":\"auto\",\"atomFontFamily\":[0],\"lineThrough\":false,\"letterSpacing\":0,\"borderMaterialCenterCalcColor\":-999,\"textFormat\":\"0\",\"atomBgColor\":[0],\"width\":320,\"useAtomBgStartColor\":false,\"enableRequestUrl\":true,\"bgMaterialCenterCalcColorSource\":1,\"lineHeight\":1,\"atomBtnAnimationPreview\":1,\"textFlowDuration\":3,\"marginTop\":0,\"timingEnd\":65536},\"areaType\":0,\"themeValues\":{\"theme\":1,\"i18n\":{}},\"clickType\":\"skip\"}"));
        }
        catch (final Exception ex) {
            ex.printStackTrace();
        }
    }
    
    public static void \u30078(final String key) {
        try {
            a.\u3007080.put(key, new JSONObject("{\"values\":{\"borderColor\":\"#000000\",\"data\":\"\",\"paddingRight\":0,\"borderSize\":0,\"interactHiddenTime\":5,\"align\":\"0\",\"isDataFixed\":false,\"timingStart\":0,\"marginRight\":0,\"paddingBottom\":0,\"interactSlideDirection\":\"2\",\"bgColor\":\"transparent\",\"paddingTop\":0,\"materialCenterData\":-999,\"borderStyle\":\"solid\",\"bgMaterialCenterCalcColor\":-999,\"atomBgColorPreview\":1,\"height\":80,\"bindEventBy\":\"none\",\"justifyHorizontal\":\"space-around\",\"clickArea\":\"creative\",\"widthMode\":\"flex\",\"alignItems\":\"flex-start\",\"interactText\":\"\",\"interactI18n\":{\"de\":\"\",\"hi\":\"\",\"ru\":\"\",\"pt\":\"\",\"japan\":\"\",\"zhHant\":\"\",\"en\":\"\",\"cn\":\"\",\"it\":\"\",\"fr\":\"\",\"es\":\"\",\"korea\":\"\",\"vi\":\"\",\"th\":\"\",\"id\":\"\"},\"marginLeft\":0,\"borderMaterialCenterCalcColor\":-999,\"useAtomBgColor\":false,\"atomBgColor\":[0],\"interactValidate\":false,\"borderRadius\":0,\"interactType\":\"\",\"clickTigger\":\"click\",\"justifyVertical\":\"flex-start\",\"width\":1,\"heightMode\":\"fixed\",\"interactVisibleTime\":0,\"marginBottom\":0,\"position\":\"0\",\"paddingLeft\":0,\"marginTop\":0,\"timingEnd\":65536},\"areaType\":2,\"themeValues\":{},\"clickType\":\"\"}"));
        }
        catch (final Exception ex) {
            ex.printStackTrace();
        }
    }
    
    public static void \u300780(final String key) {
        try {
            a.\u3007080.put(key, new JSONObject("{\"values\":{\"borderColor\":\"#000000\",\"data\":\"\",\"paddingRight\":12,\"borderSize\":0,\"interactHiddenTime\":5,\"align\":\"0\",\"appStoreFullScreen\":false,\"isDataFixed\":false,\"timingStart\":0,\"marginRight\":0,\"paddingBottom\":10,\"interactSlideDirection\":\"2\",\"bgColor\":\"transparent\",\"paddingTop\":10,\"materialCenterData\":-999,\"borderStyle\":\"solid\",\"bgMaterialCenterCalcColor\":-999,\"atomBgColorPreview\":1,\"height\":40,\"bindEventBy\":\"none\",\"justifyHorizontal\":\"space-around\",\"clickArea\":\"creative\",\"widthMode\":\"flex\",\"alignItems\":\"flex-start\",\"interactText\":\"\",\"isAppStoreAutoOpen\":false,\"interactI18n\":{\"de\":\"\",\"hi\":\"\",\"ru\":\"\",\"pt\":\"\",\"japan\":\"\",\"zhHant\":\"\",\"en\":\"\",\"cn\":\"\",\"it\":\"\",\"fr\":\"\",\"es\":\"\",\"korea\":\"\",\"vi\":\"\",\"th\":\"\",\"id\":\"\"},\"marginLeft\":0,\"borderMaterialCenterCalcColor\":-999,\"useAtomBgColor\":false,\"atomBgColor\":[0],\"interactValidate\":false,\"borderRadius\":0,\"interactType\":\"\",\"clickTigger\":\"click\",\"justifyVertical\":\"flex-start\",\"popAppstore\":false,\"width\":1,\"heightMode\":\"auto\",\"interactVisibleTime\":0,\"marginBottom\":0,\"position\":\"0\",\"paddingLeft\":12,\"marginTop\":0,\"timingEnd\":65536},\"areaType\":2,\"themeValues\":{},\"clickType\":\"\"}"));
        }
        catch (final Exception ex) {
            ex.printStackTrace();
        }
    }
    
    public static void \u300780\u3007808\u3007O(final String key) {
        try {
            a.\u3007080.put(key, new JSONObject("{\"values\":{\"borderColor\":\"#000000\",\"data\":\"\",\"paddingRight\":0,\"borderSize\":0,\"interactHiddenTime\":5,\"align\":\"0\",\"appStoreFullScreen\":false,\"isDataFixed\":false,\"timingStart\":0,\"marginRight\":0,\"paddingBottom\":0,\"interactSlideDirection\":\"2\",\"bgColor\":\"transparent\",\"paddingTop\":0,\"materialCenterData\":-999,\"borderStyle\":\"solid\",\"bgMaterialCenterCalcColor\":-999,\"atomBgColorPreview\":1,\"height\":40,\"bindEventBy\":\"none\",\"justifyHorizontal\":\"space-around\",\"clickArea\":\"creative\",\"widthMode\":\"auto\",\"alignItems\":\"flex-start\",\"interactText\":\"\",\"isAppStoreAutoOpen\":false,\"interactI18n\":{\"de\":\"\",\"hi\":\"\",\"ru\":\"\",\"pt\":\"\",\"japan\":\"\",\"zhHant\":\"\",\"en\":\"\",\"cn\":\"\",\"it\":\"\",\"fr\":\"\",\"es\":\"\",\"korea\":\"\",\"vi\":\"\",\"th\":\"\",\"id\":\"\"},\"marginLeft\":0,\"borderMaterialCenterCalcColor\":-999,\"useAtomBgColor\":false,\"atomBgColor\":[0],\"interactValidate\":false,\"borderRadius\":0,\"interactType\":\"\",\"clickTigger\":\"click\",\"justifyVertical\":\"flex-start\",\"popAppstore\":false,\"width\":320,\"heightMode\":\"auto\",\"interactVisibleTime\":0,\"marginBottom\":0,\"position\":\"0\",\"paddingLeft\":0,\"marginTop\":0,\"timingEnd\":65536},\"areaType\":2,\"themeValues\":{},\"clickType\":\"\"}"));
        }
        catch (final Exception ex) {
            ex.printStackTrace();
        }
    }
    
    public static void \u30078o8o\u3007(final String key) {
        try {
            a.\u3007080.put(key, new JSONObject("{\"values\":{\"borderColor\":\"#000000\",\"data\":\"app.comment_num\",\"paddingRight\":2,\"borderSize\":0,\"useAtomFontFamily\":false,\"align\":\"0\",\"timingStart\":0,\"marginRight\":0,\"paddingBottom\":2,\"fontFamily\":\"PingFangSC-Regular\",\"atomFontFamilyPreview\":1,\"atomBgStartColorPreview\":1,\"borderStyle\":\"solid\",\"fontWeight\":400,\"lineCount\":2,\"height\":40,\"textMaterialCenterCalcColor\":-999,\"bindEventBy\":\"none\",\"textAlign\":\"center\",\"useAtomBgEndColor\":false,\"fontModel\":\"\",\"i18n\":{\"de\":\"\",\"hi\":\"\",\"ru\":\"\",\"pt\":\"\",\"japan\":\"\",\"zhHant\":\"\",\"en\":\"\",\"cn\":\"\",\"it\":\"\",\"fr\":\"\",\"es\":\"\",\"korea\":\"\",\"vi\":\"\",\"th\":\"\",\"id\":\"\"},\"marginLeft\":0,\"textFlowType\":\"0\",\"useAtomBgColor\":false,\"atomBgEndColor\":[0],\"atomBgEndColorPreview\":1,\"borderRadius\":0,\"clickTigger\":\"click\",\"heightMode\":\"auto\",\"marginBottom\":0,\"fontSize\":10,\"position\":\"0\",\"paddingLeft\":2,\"colorCg\":0,\"atomBgStartColor\":[0],\"color\":\"#999999\",\"lineLimit\":false,\"atomBtnAnimation\":[0],\"useAtomBtnAnimation\":false,\"isDataFixed\":false,\"bgColor\":\"transparent\",\"allowTextFlow\":false,\"paddingTop\":2,\"materialCenterData\":-999,\"bgMaterialCenterCalcColor\":-999,\"atomBgColorPreview\":1,\"lineFeed\":false,\"clickArea\":\"creative\",\"widthMode\":\"auto\",\"atomFontFamily\":[0],\"lineThrough\":false,\"letterSpacing\":0,\"borderMaterialCenterCalcColor\":-999,\"atomBgColor\":[0],\"width\":320,\"useAtomBgStartColor\":false,\"lineHeight\":1,\"atomBtnAnimationPreview\":1,\"textFlowDuration\":3,\"marginTop\":0,\"timingEnd\":65536},\"areaType\":2,\"clickType\":\"score_count\"}"));
        }
        catch (final Exception ex) {
            ex.printStackTrace();
        }
    }
    
    public static void \u30078\u30070\u3007o\u3007O(final String key) {
        try {
            a.\u3007080.put(key, new JSONObject("{\"values\":{\"borderColor\":\"rgba(22, 24, 35, 0.34)\",\"data\":\"\",\"paddingRight\":3,\"borderSize\":1,\"useAtomFontFamily\":false,\"align\":\"0\",\"timingStart\":0,\"marginRight\":0,\"paddingBottom\":1,\"fontFamily\":\"PingFangSC-Regular\",\"atomFontFamilyPreview\":1,\"atomBgStartColorPreview\":1,\"borderStyle\":\"solid\",\"fontWeight\":400,\"lineCount\":2,\"height\":40,\"textMaterialCenterCalcColor\":-999,\"bindEventBy\":\"none\",\"textAlign\":\"left\",\"useAtomBgEndColor\":false,\"fontModel\":\"\",\"i18n\":{\"de\":\"\",\"hi\":\"\",\"ru\":\"\",\"pt\":\"\",\"japan\":\"\",\"zhHant\":\"\",\"en\":\"\",\"cn\":\"\",\"it\":\"\",\"fr\":\"\",\"es\":\"\",\"korea\":\"\",\"vi\":\"\",\"th\":\"\",\"id\":\"\"},\"marginLeft\":0,\"textFlowType\":\"0\",\"useAtomBgColor\":false,\"atomBgEndColor\":[0],\"atomBgEndColorPreview\":1,\"borderRadius\":8,\"clickTigger\":\"click\",\"heightMode\":\"auto\",\"marginBottom\":0,\"fontSize\":8,\"position\":\"0\",\"paddingLeft\":3,\"colorCg\":0,\"atomBgStartColor\":[0],\"color\":\"rgba(22, 24, 35, 0.34)\",\"lineLimit\":false,\"atomBtnAnimation\":[0],\"useAtomBtnAnimation\":false,\"isDataFixed\":true,\"bgColor\":\"transparent\",\"allowTextFlow\":false,\"paddingTop\":1,\"materialCenterData\":-999,\"bgMaterialCenterCalcColor\":-999,\"atomBgColorPreview\":1,\"lineFeed\":false,\"clickArea\":\"creative\",\"widthMode\":\"auto\",\"atomFontFamily\":[0],\"lineThrough\":false,\"letterSpacing\":0,\"borderMaterialCenterCalcColor\":-999,\"atomBgColor\":[0],\"width\":320,\"useAtomBgStartColor\":false,\"lineHeight\":1,\"atomBtnAnimationPreview\":1,\"textFlowDuration\":3,\"marginTop\":0,\"timingEnd\":65536},\"areaType\":3,\"themeValues\":{\"borderColor\":\"rgba(255, 255, 255, 70)\",\"color\":\"rgba(255, 255, 255, 70)\"},\"clickType\":\"dislike\"}"));
        }
        catch (final Exception ex) {
            ex.printStackTrace();
        }
    }
    
    public static void \u3007O00(final String key) {
        try {
            a.\u3007080.put(key, new JSONObject("{\"values\":{\"borderColor\":\"#000000\",\"data\":\"\",\"paddingRight\":0,\"borderSize\":0,\"rules\":[],\"interactHiddenTime\":5,\"align\":\"0\",\"interactPressDuration\":1500,\"timingStart\":0,\"marginRight\":0,\"paddingBottom\":0,\"interactSlideDirection\":\"2\",\"borderStyle\":\"solid\",\"height\":40,\"triggerActions\":[],\"bindEventBy\":\"none\",\"alignItems\":\"flex-start\",\"textAlign\":\"center\",\"interactI18n\":{\"aa\":\"\",\"de\":\"\",\"hi\":\"\",\"ru\":\"\",\"pt\":\"\",\"japan\":\"\",\"zhHant\":\"\",\"en\":\"\",\"cn\":\"\",\"it\":\"\",\"fr\":\"\",\"my\":\"\",\"es\":\"\",\"korea\":\"\",\"vi\":\"\",\"th\":\"\",\"id\":\"\",\"pl\":\"\",\"tr\":\"\"},\"marginLeft\":0,\"useAtomBgColor\":false,\"triggerConditions\":[],\"borderRadius\":0,\"clickTigger\":\"click\",\"justifyVertical\":\"flex-start\",\"popAppstore\":false,\"heightMode\":\"auto\",\"marginBottom\":0,\"fontSize\":12,\"position\":\"0\",\"paddingLeft\":0,\"borderMaterialCenterCalcColorSource\":1,\"appStoreFullScreen\":false,\"isDataFixed\":false,\"bgColor\":\"transparent\",\"interactWontHide\":false,\"paddingTop\":0,\"materialCenterData\":-999,\"bgMaterialCenterCalcColor\":-999,\"atomBgColorPreview\":1,\"openPlayableLandingPage\":false,\"justifyHorizontal\":\"space-around\",\"clickArea\":\"creative\",\"widthMode\":\"auto\",\"interactText\":\"\",\"isAppStoreAutoOpen\":false,\"interactSlideThreshold\":0,\"interactTextFixed\":true,\"showHasDataType\":\"\",\"borderMaterialCenterCalcColor\":-999,\"atomBgColor\":[0],\"interactValidate\":false,\"interactType\":\"\",\"width\":320,\"interactVisibleTime\":0,\"interactBottomDistance\":120,\"enableRequestUrl\":true,\"bgMaterialCenterCalcColorSource\":1,\"marginTop\":0,\"timingEnd\":65536},\"areaType\":0,\"themeValues\":{\"theme\":1,\"interactI18n\":{}},\"clickType\":\"\"}"));
        }
        catch (final Exception ex) {
            ex.printStackTrace();
        }
    }
    
    public static void \u3007O888o0o(final String key) {
        try {
            a.\u3007080.put(key, new JSONObject("{\"values\":{\"borderColor\":\"#000000\",\"data\":\"app.score\",\"paddingRight\":2,\"borderSize\":0,\"useAtomFontFamily\":false,\"align\":\"0\",\"timingStart\":0,\"marginRight\":0,\"paddingBottom\":2,\"fontFamily\":\"PingFangSC-Regular\",\"atomFontFamilyPreview\":1,\"atomBgStartColorPreview\":1,\"borderStyle\":\"solid\",\"fontWeight\":400,\"lineCount\":2,\"height\":40,\"textMaterialCenterCalcColor\":-999,\"bindEventBy\":\"none\",\"textAlign\":\"center\",\"useAtomBgEndColor\":false,\"fontModel\":\"\",\"i18n\":{\"de\":\"\",\"hi\":\"\",\"ru\":\"\",\"pt\":\"\",\"japan\":\"\",\"zhHant\":\"\",\"en\":\"\",\"cn\":\"\",\"it\":\"\",\"fr\":\"\",\"es\":\"\",\"korea\":\"\",\"vi\":\"\",\"th\":\"\",\"id\":\"\"},\"marginLeft\":0,\"textFlowType\":\"0\",\"useAtomBgColor\":false,\"atomBgEndColor\":[0],\"atomBgEndColorPreview\":1,\"borderRadius\":0,\"clickTigger\":\"click\",\"heightMode\":\"auto\",\"marginBottom\":0,\"fontSize\":20,\"position\":\"0\",\"paddingLeft\":2,\"colorCg\":0,\"atomBgStartColor\":[0],\"color\":\"#FFC642\",\"lineLimit\":false,\"atomBtnAnimation\":[0],\"useAtomBtnAnimation\":false,\"isDataFixed\":false,\"bgColor\":\"transparent\",\"allowTextFlow\":false,\"paddingTop\":2,\"materialCenterData\":-999,\"bgMaterialCenterCalcColor\":-999,\"atomBgColorPreview\":1,\"starIconType\":\"1\",\"lineFeed\":false,\"clickArea\":\"creative\",\"widthMode\":\"auto\",\"atomFontFamily\":[0],\"lineThrough\":false,\"letterSpacing\":0,\"starMargin\":0,\"borderMaterialCenterCalcColor\":-999,\"atomBgColor\":[0],\"width\":320,\"useAtomBgStartColor\":false,\"lineHeight\":1,\"atomBtnAnimationPreview\":1,\"textFlowDuration\":3,\"marginTop\":0,\"timingEnd\":65536},\"areaType\":2,\"clickType\":\"star\"}"));
        }
        catch (final Exception ex) {
            ex.printStackTrace();
        }
    }
    
    public static void \u3007O8o08O(final String key) {
        try {
            a.\u3007080.put(key, new JSONObject("{\"values\":{\"borderColor\":\"rgba(22, 24, 35, 0.34)\",\"data\":\"text\",\"paddingRight\":3,\"borderSize\":1,\"useAtomFontFamily\":false,\"align\":\"0\",\"timingStart\":0,\"marginRight\":0,\"paddingBottom\":1,\"fontFamily\":\"PingFangSC-Regular\",\"atomFontFamilyPreview\":1,\"atomBgStartColorPreview\":1,\"borderStyle\":\"solid\",\"fontWeight\":400,\"lineCount\":2,\"height\":40,\"textMaterialCenterCalcColor\":-999,\"bindEventBy\":\"none\",\"textAlign\":\"left\",\"useAtomBgEndColor\":false,\"fontModel\":\"\",\"i18n\":{\"de\":\"\",\"hi\":\"\",\"ru\":\"\",\"pt\":\"\",\"japan\":\"\",\"zhHant\":\"\",\"en\":\"\",\"cn\":\"\",\"it\":\"\",\"fr\":\"\",\"es\":\"\",\"korea\":\"\",\"vi\":\"\",\"th\":\"\",\"id\":\"\"},\"marginLeft\":0,\"textFlowType\":\"0\",\"useAtomBgColor\":false,\"atomBgEndColor\":[0],\"atomBgEndColorPreview\":1,\"borderRadius\":8,\"clickTigger\":\"click\",\"heightMode\":\"auto\",\"marginBottom\":0,\"fontSize\":8,\"position\":\"0\",\"paddingLeft\":3,\"colorCg\":0,\"atomBgStartColor\":[0],\"color\":\"rgba(22, 24, 35, 0.34)\",\"lineLimit\":false,\"atomBtnAnimation\":[0],\"useAtomBtnAnimation\":false,\"isDataFixed\":true,\"bgColor\":\"transparent\",\"allowTextFlow\":false,\"paddingTop\":1,\"materialCenterData\":-999,\"bgMaterialCenterCalcColor\":-999,\"atomBgColorPreview\":1,\"lineFeed\":false,\"clickArea\":\"creative\",\"widthMode\":\"auto\",\"atomFontFamily\":[0],\"lineThrough\":false,\"letterSpacing\":0,\"borderMaterialCenterCalcColor\":-999,\"atomBgColor\":[0],\"width\":320,\"useAtomBgStartColor\":false,\"lineHeight\":1,\"atomBtnAnimationPreview\":1,\"textFlowDuration\":3,\"marginTop\":0,\"timingEnd\":65536},\"areaType\":0,\"themeValues\":{\"borderColor\":\"rgba(255, 255, 255, 70)\",\"color\":\"rgba(255, 255, 255, 70)\",\"bgColor\":\"rgba(255, 255, 255, 0)\"},\"clickType\":\"skip\"}"));
        }
        catch (final Exception ex) {
            ex.printStackTrace();
        }
    }
    
    public static void \u3007O\u3007(final String key) {
        try {
            a.\u3007080.put(key, new JSONObject("{\"values\":{\"borderColor\":\"#000000\",\"data\":\"video.video_duration\",\"paddingRight\":0,\"borderSize\":0,\"useAtomFontFamily\":false,\"align\":\"0\",\"timingStart\":0,\"marginRight\":0,\"paddingBottom\":0,\"fontFamily\":\"PingFangSC-Regular\",\"atomFontFamilyPreview\":1,\"atomBgStartColorPreview\":1,\"borderStyle\":\"solid\",\"fontWeight\":400,\"lineCount\":2,\"height\":40,\"textMaterialCenterCalcColor\":-999,\"bindEventBy\":\"none\",\"textAlign\":\"right\",\"useAtomBgEndColor\":false,\"fontModel\":\"\",\"i18n\":{\"de\":\"\",\"hi\":\"\",\"ru\":\"\",\"pt\":\"\",\"japan\":\"\",\"zhHant\":\"\",\"en\":\"\",\"cn\":\"\",\"it\":\"\",\"fr\":\"\",\"es\":\"\",\"korea\":\"\",\"vi\":\"\",\"th\":\"\",\"id\":\"\"},\"marginLeft\":0,\"textFlowType\":\"0\",\"useAtomBgColor\":false,\"atomBgEndColor\":[0],\"atomBgEndColorPreview\":1,\"borderRadius\":0,\"clickTigger\":\"click\",\"heightMode\":\"auto\",\"marginBottom\":0,\"fontSize\":14,\"position\":\"0\",\"paddingLeft\":0,\"colorCg\":0,\"atomBgStartColor\":[0],\"color\":\"#FFFFFF\",\"lineLimit\":false,\"atomBtnAnimation\":[0],\"useAtomBtnAnimation\":false,\"isDataFixed\":false,\"bgColor\":\"transparent\",\"allowTextFlow\":false,\"paddingTop\":0,\"materialCenterData\":-999,\"bgMaterialCenterCalcColor\":-999,\"atomBgColorPreview\":1,\"lineFeed\":false,\"clickArea\":\"creative\",\"widthMode\":\"auto\",\"atomFontFamily\":[0],\"lineThrough\":false,\"letterSpacing\":0,\"borderMaterialCenterCalcColor\":-999,\"atomBgColor\":[0],\"width\":320,\"useAtomBgStartColor\":false,\"lineHeight\":1.4,\"atomBtnAnimationPreview\":1,\"textFlowDuration\":3,\"marginTop\":0,\"timingEnd\":65536},\"areaType\":0,\"clickType\":\"time_down\"}"));
        }
        catch (final Exception ex) {
            ex.printStackTrace();
        }
    }
    
    public static void \u3007O\u300780o08O(final String key) {
        try {
            a.\u3007080.put(key, new JSONObject("{\"values\":{\"borderColor\":\"#000000\",\"data\":\"\",\"paddingRight\":0,\"borderSize\":0,\"interactHiddenTime\":5,\"align\":\"0\",\"appStoreFullScreen\":false,\"isDataFixed\":false,\"timingStart\":0,\"marginRight\":0,\"paddingBottom\":0,\"interactSlideDirection\":\"2\",\"bgColor\":\"transparent\",\"paddingTop\":0,\"materialCenterData\":-999,\"borderStyle\":\"solid\",\"bgMaterialCenterCalcColor\":-999,\"atomBgColorPreview\":1,\"height\":40,\"bindEventBy\":\"none\",\"justifyHorizontal\":\"space-around\",\"clickArea\":\"creative\",\"widthMode\":\"auto\",\"alignItems\":\"flex-start\",\"interactText\":\"\",\"isAppStoreAutoOpen\":false,\"interactI18n\":{\"de\":\"\",\"hi\":\"\",\"ru\":\"\",\"pt\":\"\",\"japan\":\"\",\"zhHant\":\"\",\"en\":\"\",\"cn\":\"\",\"it\":\"\",\"fr\":\"\",\"es\":\"\",\"korea\":\"\",\"vi\":\"\",\"th\":\"\",\"id\":\"\"},\"marginLeft\":0,\"borderMaterialCenterCalcColor\":-999,\"useAtomBgColor\":false,\"atomBgColor\":[0],\"interactValidate\":false,\"borderRadius\":0,\"interactType\":\"\",\"clickTigger\":\"click\",\"justifyVertical\":\"flex-start\",\"popAppstore\":false,\"width\":320,\"heightMode\":\"auto\",\"interactVisibleTime\":0,\"marginBottom\":0,\"position\":\"0\",\"paddingLeft\":0,\"marginTop\":0,\"timingEnd\":65536},\"areaType\":2,\"themeValues\":{},\"clickType\":\"\"}"));
        }
        catch (final Exception ex) {
            ex.printStackTrace();
        }
    }
    
    public static void \u3007o(final String key) {
        try {
            a.\u3007080.put(key, new JSONObject("{\"values\":{\"borderColor\":\"#000000\",\"data\":\"video\",\"bulletScreenStart\":0,\"paddingRight\":0,\"borderSize\":0,\"interactHiddenTime\":5,\"fixBorderStyleType\":0,\"align\":\"0\",\"showPlayButton\":true,\"isDataFixed\":false,\"timingStart\":0,\"bulletScreenData\":\"dynamic_creative.danmu\",\"marginRight\":0,\"bulletScreenEnd\":0,\"paddingBottom\":0,\"interactSlideDirection\":\"2\",\"bgColor\":\"transparent\",\"paddingTop\":0,\"materialCenterData\":-999,\"borderStyle\":\"solid\",\"bgMaterialCenterCalcColor\":-999,\"height\":1.78,\"bindEventBy\":\"none\",\"clickArea\":\"video\",\"widthMode\":\"flex\",\"interactText\":\"\",\"interactI18n\":{\"de\":\"\",\"hi\":\"\",\"ru\":\"\",\"pt\":\"\",\"japan\":\"\",\"zhHant\":\"\",\"en\":\"\",\"cn\":\"\",\"it\":\"\",\"fr\":\"\",\"es\":\"\",\"korea\":\"\",\"vi\":\"\",\"th\":\"\",\"id\":\"\"},\"marginLeft\":0,\"borderMaterialCenterCalcColor\":-999,\"showTimeProgress\":true,\"interactValidate\":false,\"borderRadius\":0,\"interactType\":\"\",\"clickTigger\":\"click\",\"showBulletScreen\":false,\"width\":1,\"heightMode\":\"scale\",\"interactVisibleTime\":0,\"marginBottom\":0,\"position\":\"0\",\"paddingLeft\":0,\"marginTop\":0,\"timingEnd\":65536},\"areaType\":99,\"themeValues\":{},\"clickType\":\"video\"}"));
        }
        catch (final Exception ex) {
            ex.printStackTrace();
        }
    }
    
    public static void \u3007o00\u3007\u3007Oo(final String key) {
        try {
            a.\u3007080.put(key, new JSONObject("{\"values\":{\"imageObjectFit\":\"none\",\"borderColor\":\"#000000\",\"data\":\"image.0.url\",\"paddingRight\":0,\"borderSize\":0,\"interactHiddenTime\":5,\"fixBorderStyleType\":0,\"align\":\"0\",\"isDataFixed\":false,\"timingStart\":0,\"marginRight\":0,\"paddingBottom\":0,\"interactSlideDirection\":\"2\",\"bgColor\":\"transparent\",\"bgGauseBlurRadius\":3,\"paddingTop\":0,\"materialCenterData\":-999,\"borderStyle\":\"solid\",\"bgMaterialCenterCalcColor\":-999,\"imageType\":\"static-image\",\"height\":1.78,\"bindEventBy\":\"none\",\"justifyHorizontal\":\"space-around\",\"clickArea\":\"creative\",\"widthMode\":\"flex\",\"alignItems\":\"flex-start\",\"bgGauseBlur\":false,\"interactText\":\"\",\"interactI18n\":{\"de\":\"\",\"hi\":\"\",\"ru\":\"\",\"pt\":\"\",\"japan\":\"\",\"zhHant\":\"\",\"en\":\"\",\"cn\":\"\",\"it\":\"\",\"fr\":\"\",\"es\":\"\",\"korea\":\"\",\"vi\":\"\",\"th\":\"\",\"id\":\"\"},\"marginLeft\":0,\"borderMaterialCenterCalcColor\":-999,\"interactValidate\":false,\"borderRadius\":0,\"interactType\":\"\",\"clickTigger\":\"click\",\"justifyVertical\":\"flex-start\",\"width\":1,\"heightMode\":\"scale\",\"interactVisibleTime\":0,\"marginBottom\":0,\"position\":\"0\",\"paddingLeft\":0,\"marginTop\":0,\"timingEnd\":65536,\"useLeisureInteract\":false},\"areaType\":2,\"themeValues\":{},\"clickType\":\"image\"}"));
        }
        catch (final Exception ex) {
            ex.printStackTrace();
        }
    }
    
    public static void \u3007oOO8O8(final String key) {
        try {
            a.\u3007080.put(key, new JSONObject("{\"values\":{\"imageObjectFit\":\"none\",\"borderColor\":\"#000000\",\"data\":\"icon\",\"paddingRight\":0,\"borderSize\":0,\"interactHiddenTime\":5,\"fixBorderStyleType\":0,\"align\":\"0\",\"isDataFixed\":false,\"timingStart\":0,\"marginRight\":0,\"paddingBottom\":0,\"interactSlideDirection\":\"2\",\"bgColor\":\"transparent\",\"bgGauseBlurRadius\":3,\"paddingTop\":0,\"materialCenterData\":-999,\"borderStyle\":\"solid\",\"bgMaterialCenterCalcColor\":-999,\"imageType\":\"static-image\",\"height\":1,\"bindEventBy\":\"none\",\"justifyHorizontal\":\"space-around\",\"clickArea\":\"creative\",\"widthMode\":\"fixed\",\"alignItems\":\"flex-start\",\"bgGauseBlur\":false,\"interactText\":\"\",\"interactI18n\":{\"de\":\"\",\"hi\":\"\",\"ru\":\"\",\"pt\":\"\",\"japan\":\"\",\"zhHant\":\"\",\"en\":\"\",\"cn\":\"\",\"it\":\"\",\"fr\":\"\",\"es\":\"\",\"korea\":\"\",\"vi\":\"\",\"th\":\"\",\"id\":\"\"},\"marginLeft\":0,\"borderMaterialCenterCalcColor\":-999,\"interactValidate\":false,\"borderRadius\":0,\"interactType\":\"\",\"clickTigger\":\"click\",\"justifyVertical\":\"flex-start\",\"width\":60,\"heightMode\":\"scale\",\"interactVisibleTime\":0,\"marginBottom\":0,\"position\":\"0\",\"paddingLeft\":0,\"marginTop\":0,\"timingEnd\":65536,\"useLeisureInteract\":false},\"areaType\":2,\"themeValues\":{},\"clickType\":\"icon\"}"));
        }
        catch (final Exception ex) {
            ex.printStackTrace();
        }
    }
    
    public static void \u3007oo\u3007(final String key) {
        try {
            a.\u3007080.put(key, new JSONObject("{\"values\":{\"borderColor\":\"#000000\",\"data\":\"\",\"paddingRight\":5,\"borderSize\":0,\"interactHiddenTime\":5,\"align\":\"0\",\"timingStart\":0,\"marginRight\":0,\"paddingBottom\":2,\"interactSlideDirection\":\"2\",\"fontFamily\":\"PingFangSC-Regular\",\"borderStyle\":\"solid\",\"height\":80,\"bindEventBy\":\"none\",\"alignItems\":\"flex-start\",\"interactI18n\":{\"de\":\"\",\"hi\":\"\",\"ru\":\"\",\"pt\":\"\",\"japan\":\"\",\"zhHant\":\"\",\"en\":\"\",\"cn\":\"\",\"it\":\"\",\"fr\":\"\",\"es\":\"\",\"korea\":\"\",\"vi\":\"\",\"th\":\"\",\"id\":\"\"},\"marginLeft\":0,\"useAtomBgColor\":false,\"borderRadius\":0,\"clickTigger\":\"click\",\"justifyVertical\":\"flex-start\",\"popAppstore\":false,\"heightMode\":\"auto\",\"marginBottom\":0,\"position\":\"0\",\"paddingLeft\":5,\"tagData\":\"dynamic_creative.creative_tags\",\"appStoreFullScreen\":false,\"isDataFixed\":false,\"tagStyleType\":\"backgroundColor\",\"bgColor\":\"transparent\",\"paddingTop\":2,\"materialCenterData\":-999,\"tagMaxCount\":5,\"bgMaterialCenterCalcColor\":-999,\"atomBgColorPreview\":1,\"justifyHorizontal\":\"flex-start\",\"clickArea\":\"creative\",\"widthMode\":\"fixed\",\"interactText\":\"\",\"isAppStoreAutoOpen\":false,\"borderMaterialCenterCalcColor\":-999,\"atomBgColor\":[0],\"interactValidate\":false,\"interactType\":\"\",\"width\":375,\"interactVisibleTime\":0,\"marginTop\":0,\"timingEnd\":65536},\"areaType\":2,\"themeValues\":{},\"clickType\":\"tag\"}"));
        }
        catch (final Exception ex) {
            ex.printStackTrace();
        }
    }
    
    public static void \u3007o\u3007(final String key) {
        try {
            a.\u3007080.put(key, new JSONObject("{\"values\":{\"borderColor\":\"#2a90d7\",\"data\":\"button_text\",\"paddingRight\":10,\"borderSize\":0,\"useAtomFontFamily\":false,\"align\":\"0\",\"timingStart\":0,\"marginRight\":0,\"paddingBottom\":4,\"fontFamily\":\"PingFangSC-Regular\",\"atomFontFamilyPreview\":1,\"atomBgStartColorPreview\":1,\"borderStyle\":\"solid\",\"fontWeight\":400,\"lineCount\":2,\"height\":40,\"textMaterialCenterCalcColor\":-999,\"bindEventBy\":\"none\",\"textAlign\":\"center\",\"useAtomBgEndColor\":false,\"fontModel\":\"\",\"i18n\":{\"de\":\"\",\"hi\":\"\",\"ru\":\"\",\"pt\":\"\",\"japan\":\"\",\"zhHant\":\"\",\"en\":\"\",\"cn\":\"\",\"it\":\"\",\"fr\":\"\",\"es\":\"\",\"korea\":\"\",\"vi\":\"\",\"th\":\"\",\"id\":\"\"},\"marginLeft\":0,\"textFlowType\":\"0\",\"useAtomBgColor\":false,\"atomBgEndColor\":[0],\"atomBgEndColorPreview\":1,\"borderRadius\":0,\"clickTigger\":\"click\",\"heightMode\":\"auto\",\"marginBottom\":0,\"fontSize\":12,\"position\":\"0\",\"paddingLeft\":10,\"colorCg\":0,\"atomBgStartColor\":[0],\"color\":\"#FFFFFF\",\"lineLimit\":false,\"atomBtnAnimation\":[0],\"useAtomBtnAnimation\":false,\"isDataFixed\":false,\"atomButtonText\":true,\"bgColor\":\"#895434\",\"iconType\":\"0\",\"allowTextFlow\":false,\"paddingTop\":4,\"materialCenterData\":-999,\"bgMaterialCenterCalcColor\":-999,\"atomBgColorPreview\":1,\"showIcon\":false,\"lineFeed\":false,\"clickArea\":\"creative\",\"widthMode\":\"auto\",\"atomFontFamily\":[0],\"lineThrough\":false,\"letterSpacing\":0,\"borderMaterialCenterCalcColor\":-999,\"atomBgColor\":[0],\"width\":320,\"useAtomBgStartColor\":false,\"lineHeight\":1.2,\"atomBtnAnimationPreview\":1,\"textFlowDuration\":3,\"marginTop\":0,\"timingEnd\":65536},\"areaType\":2,\"clickType\":\"button\"}"));
        }
        catch (final Exception ex) {
            ex.printStackTrace();
        }
    }
    
    public static void \u3007\u30070o(final String key) {
        try {
            a.\u3007080.put(key, new JSONObject("{\"values\":{\"borderColor\":\"#2a90d7\",\"data\":\"button_text\",\"paddingRight\":10,\"borderSize\":0,\"useAtomFontFamily\":false,\"align\":\"0\",\"timingStart\":0,\"marginRight\":0,\"paddingBottom\":4,\"fontFamily\":\"PingFangSC-Regular\",\"atomFontFamilyPreview\":1,\"atomBgStartColorPreview\":1,\"borderStyle\":\"solid\",\"fontWeight\":400,\"lineCount\":2,\"height\":40,\"textMaterialCenterCalcColor\":-999,\"bindEventBy\":\"none\",\"textAlign\":\"center\",\"useAtomBgEndColor\":false,\"fontModel\":\"\",\"i18n\":{\"de\":\"\",\"hi\":\"\",\"ru\":\"\",\"pt\":\"\",\"japan\":\"\",\"zhHant\":\"\",\"en\":\"\",\"cn\":\"\",\"it\":\"\",\"fr\":\"\",\"es\":\"\",\"korea\":\"\",\"vi\":\"\",\"th\":\"\",\"id\":\"\"},\"marginLeft\":0,\"textFlowType\":\"0\",\"useAtomBgColor\":false,\"atomBgEndColor\":[0],\"atomBgEndColorPreview\":1,\"borderRadius\":0,\"clickTigger\":\"click\",\"heightMode\":\"auto\",\"marginBottom\":0,\"fontSize\":12,\"position\":\"0\",\"paddingLeft\":10,\"colorCg\":0,\"atomBgStartColor\":[0],\"color\":\"#FFFFFF\",\"lineLimit\":false,\"atomBtnAnimation\":[0],\"useAtomBtnAnimation\":false,\"isDataFixed\":false,\"atomButtonText\":true,\"bgColor\":\"#00e7ff\",\"iconType\":\"0\",\"allowTextFlow\":false,\"paddingTop\":4,\"materialCenterData\":-999,\"bgMaterialCenterCalcColor\":-999,\"atomBgColorPreview\":1,\"showIcon\":false,\"lineFeed\":false,\"clickArea\":\"creative\",\"widthMode\":\"auto\",\"atomFontFamily\":[0],\"lineThrough\":false,\"letterSpacing\":0,\"borderMaterialCenterCalcColor\":-999,\"atomBgColor\":[0],\"width\":320,\"useAtomBgStartColor\":false,\"lineHeight\":1.2,\"atomBtnAnimationPreview\":1,\"textFlowDuration\":3,\"marginTop\":0,\"timingEnd\":65536},\"areaType\":2,\"clickType\":\"button\"}"));
        }
        catch (final Exception ex) {
            ex.printStackTrace();
        }
    }
    
    public static void \u3007\u3007808\u3007(final String key) {
        try {
            a.\u3007080.put(key, new JSONObject("{\"values\":{\"borderColor\":\"#000000\",\"data\":\"video.video_duration\",\"paddingRight\":0,\"borderSize\":0,\"useAtomFontFamily\":false,\"align\":\"0\",\"timingStart\":0,\"marginRight\":0,\"paddingBottom\":0,\"fontFamily\":\"PingFangSC-Regular\",\"atomFontFamilyPreview\":1,\"atomBgStartColorPreview\":1,\"borderStyle\":\"solid\",\"fontWeight\":400,\"lineCount\":2,\"height\":40,\"textMaterialCenterCalcColor\":-999,\"bindEventBy\":\"none\",\"textAlign\":\"left\",\"useAtomBgEndColor\":false,\"fontModel\":\"\",\"i18n\":{\"de\":\"\",\"hi\":\"\",\"ru\":\"\",\"pt\":\"\",\"japan\":\"\",\"zhHant\":\"\",\"en\":\"\",\"cn\":\"\",\"it\":\"\",\"fr\":\"\",\"es\":\"\",\"korea\":\"\",\"vi\":\"\",\"th\":\"\",\"id\":\"\"},\"marginLeft\":0,\"textFlowType\":\"0\",\"useAtomBgColor\":false,\"atomBgEndColor\":[0],\"atomBgEndColorPreview\":1,\"borderRadius\":0,\"clickTigger\":\"click\",\"heightMode\":\"auto\",\"marginBottom\":0,\"fontSize\":14,\"position\":\"0\",\"paddingLeft\":4,\"colorCg\":0,\"atomBgStartColor\":[0],\"color\":\"#FFFFFF\",\"lineLimit\":false,\"atomBtnAnimation\":[0],\"useAtomBtnAnimation\":false,\"isDataFixed\":false,\"bgColor\":\"transparent\",\"allowTextFlow\":false,\"paddingTop\":0,\"materialCenterData\":-999,\"bgMaterialCenterCalcColor\":-999,\"atomBgColorPreview\":1,\"lineFeed\":false,\"clickArea\":\"creative\",\"widthMode\":\"auto\",\"atomFontFamily\":[0],\"lineThrough\":false,\"letterSpacing\":0,\"borderMaterialCenterCalcColor\":-999,\"atomBgColor\":[0],\"width\":320,\"useAtomBgStartColor\":false,\"lineHeight\":1.4,\"atomBtnAnimationPreview\":1,\"textFlowDuration\":3,\"marginTop\":0,\"timingEnd\":65536},\"areaType\":0,\"clickType\":\"time_down\"}"));
        }
        catch (final Exception ex) {
            ex.printStackTrace();
        }
    }
    
    public static void \u3007\u3007888(final String key) {
        try {
            a.\u3007080.put(key, new JSONObject("{\"values\":{\"borderColor\":\"#2a90d7\",\"data\":\"\",\"paddingRight\":4,\"borderSize\":0,\"useAtomFontFamily\":false,\"align\":\"0\",\"timingStart\":0,\"marginRight\":0,\"paddingBottom\":4,\"fontFamily\":\"PingFangSC-Regular\",\"atomFontFamilyPreview\":1,\"atomBgStartColorPreview\":1,\"borderStyle\":\"solid\",\"fontWeight\":400,\"lineCount\":2,\"height\":40,\"textMaterialCenterCalcColor\":-999,\"bindEventBy\":\"none\",\"textAlign\":\"center\",\"useAtomBgEndColor\":false,\"fontModel\":\"\",\"i18n\":{\"de\":\"\",\"hi\":\"\",\"ru\":\"\",\"pt\":\"\",\"japan\":\"\",\"zhHant\":\"\",\"en\":\"\",\"cn\":\"\",\"it\":\"\",\"fr\":\"\",\"es\":\"\",\"korea\":\"\",\"vi\":\"\",\"th\":\"\",\"id\":\"\"},\"marginLeft\":0,\"textFlowType\":\"0\",\"useAtomBgColor\":false,\"atomBgEndColor\":[0],\"atomBgEndColorPreview\":1,\"borderRadius\":0,\"clickTigger\":\"click\",\"heightMode\":\"auto\",\"marginBottom\":0,\"fontSize\":12,\"position\":\"0\",\"paddingLeft\":4,\"colorCg\":0,\"atomBgStartColor\":[0],\"color\":\"#B0B0B0\",\"lineLimit\":false,\"atomBtnAnimation\":[0],\"useAtomBtnAnimation\":false,\"isDataFixed\":true,\"bgColor\":\"transparent\",\"allowTextFlow\":false,\"paddingTop\":4,\"materialCenterData\":-999,\"bgMaterialCenterCalcColor\":-999,\"atomBgColorPreview\":1,\"lineFeed\":false,\"clickArea\":\"creative\",\"widthMode\":\"auto\",\"atomFontFamily\":[0],\"lineThrough\":false,\"letterSpacing\":0,\"borderMaterialCenterCalcColor\":-999,\"atomBgColor\":[0],\"width\":320,\"useAtomBgStartColor\":false,\"lineHeight\":1.2,\"atomBtnAnimationPreview\":1,\"textFlowDuration\":3,\"marginTop\":0,\"timingEnd\":65536},\"areaType\":2,\"themeValues\":{\"color\":\"#ffffff\"},\"clickType\":\"\"}"));
        }
        catch (final Exception ex) {
            ex.printStackTrace();
        }
    }
    
    public static void \u3007\u30078O0\u30078(final String key) {
        try {
            a.\u3007080.put(key, new JSONObject("{\"values\":{\"borderColor\":\"#000000\",\"data\":\"video.video_duration\",\"paddingRight\":8,\"borderSize\":0,\"useAtomFontFamily\":false,\"rules\":[],\"align\":\"0\",\"timingStart\":0,\"marginRight\":0,\"paddingBottom\":5,\"fontFamily\":\"PingFangSC-Regular\",\"atomFontFamilyPreview\":1,\"atomBgStartColorPreview\":1,\"borderStyle\":\"solid\",\"fontWeight\":400,\"lineCount\":2,\"height\":40,\"textMaterialCenterCalcColor\":-999,\"triggerActions\":[],\"bindEventBy\":\"none\",\"textMaterialCenterCalcColorSource\":1,\"textAlign\":\"center\",\"useAtomBgEndColor\":false,\"fontModel\":\"\",\"i18n\":{\"aa\":\"\",\"de\":\"\",\"hi\":\"\",\"ru\":\"\",\"pt\":\"\",\"japan\":\"\",\"zhHant\":\"\",\"en\":\"\",\"cn\":\"\",\"it\":\"\",\"fr\":\"\",\"my\":\"\",\"es\":\"\",\"korea\":\"\",\"vi\":\"\",\"th\":\"\",\"id\":\"\",\"pl\":\"\",\"tr\":\"\"},\"marginLeft\":0,\"textFlowType\":\"0\",\"useAtomBgColor\":false,\"atomBgEndColor\":[0],\"triggerConditions\":[],\"atomBgEndColorPreview\":1,\"borderRadius\":14,\"clickTigger\":\"click\",\"heightMode\":\"auto\",\"marginBottom\":0,\"fontSize\":14,\"position\":\"0\",\"paddingLeft\":8,\"colorCg\":0,\"borderMaterialCenterCalcColorSource\":1,\"atomBgStartColor\":[0],\"textFlowSpeed\":3,\"color\":\"#FFFFFF\",\"lineLimit\":false,\"atomBtnAnimation\":[0],\"useAtomBtnAnimation\":false,\"isDataFixed\":false,\"bgColor\":\"rgba(22, 24, 35, 0.5)\",\"allowTextFlow\":false,\"paddingTop\":5,\"materialCenterData\":-999,\"bgMaterialCenterCalcColor\":-999,\"atomBgColorPreview\":1,\"openPlayableLandingPage\":false,\"lineFeed\":false,\"clickArea\":\"creative\",\"widthMode\":\"auto\",\"atomFontFamily\":[0],\"lineThrough\":false,\"letterSpacing\":0,\"borderMaterialCenterCalcColor\":-999,\"textFormat\":\"0\",\"atomBgColor\":[0],\"width\":320,\"useAtomBgStartColor\":false,\"enableRequestUrl\":true,\"bgMaterialCenterCalcColorSource\":1,\"lineHeight\":1.3,\"atomBtnAnimationPreview\":1,\"textFlowDuration\":3,\"marginTop\":0,\"timingEnd\":65536},\"areaType\":0,\"themeValues\":{\"theme\":1,\"i18n\":{}},\"clickType\":\"time_down\"}"));
        }
        catch (final Exception ex) {
            ex.printStackTrace();
        }
    }
    
    public static void \u3007\u3007\u30070\u3007\u30070(final String key) {
        try {
            a.\u3007080.put(key, new JSONObject("{\"values\":{\"borderColor\":\"rgba(22, 24, 35, 0.34)\",\"data\":\"\",\"paddingRight\":6,\"borderSize\":0,\"useAtomFontFamily\":false,\"align\":\"0\",\"timingStart\":0,\"marginRight\":0,\"paddingBottom\":6,\"fontFamily\":\"PingFangSC-Regular\",\"atomFontFamilyPreview\":1,\"atomBgStartColorPreview\":1,\"borderStyle\":\"solid\",\"fontWeight\":400,\"lineCount\":2,\"height\":40,\"textMaterialCenterCalcColor\":-999,\"bindEventBy\":\"none\",\"textAlign\":\"center\",\"useAtomBgEndColor\":false,\"fontModel\":\"\",\"i18n\":{\"de\":\"\",\"hi\":\"\",\"ru\":\"\",\"pt\":\"\",\"japan\":\"\",\"zhHant\":\"\",\"en\":\"\",\"cn\":\"\",\"it\":\"\",\"fr\":\"\",\"es\":\"\",\"korea\":\"\",\"vi\":\"\",\"th\":\"\",\"id\":\"\"},\"marginLeft\":0,\"textFlowType\":\"0\",\"useAtomBgColor\":false,\"atomBgEndColor\":[0],\"atomBgEndColorPreview\":1,\"borderRadius\":16,\"clickTigger\":\"click\",\"heightMode\":\"auto\",\"marginBottom\":0,\"fontSize\":16,\"position\":\"0\",\"paddingLeft\":6,\"colorCg\":0,\"atomBgStartColor\":[0],\"color\":\"#ffffff\",\"lineLimit\":false,\"atomBtnAnimation\":[0],\"useAtomBtnAnimation\":false,\"isDataFixed\":true,\"bgColor\":\"rgba(22, 24, 35, 0.4)\",\"allowTextFlow\":false,\"paddingTop\":6,\"materialCenterData\":-999,\"bgMaterialCenterCalcColor\":-999,\"atomBgColorPreview\":1,\"lineFeed\":true,\"clickArea\":\"creative\",\"widthMode\":\"auto\",\"atomFontFamily\":[0],\"lineThrough\":false,\"letterSpacing\":0,\"borderMaterialCenterCalcColor\":-999,\"atomBgColor\":[0],\"width\":320,\"useAtomBgStartColor\":false,\"lineHeight\":1,\"atomBtnAnimationPreview\":1,\"textFlowDuration\":3,\"marginTop\":0,\"timingEnd\":65536},\"areaType\":3,\"themeValues\":{\"bgColor\":\"#9B9B9B\"},\"clickType\":\"dislike\"}"));
        }
        catch (final Exception ex) {
            ex.printStackTrace();
        }
    }
}

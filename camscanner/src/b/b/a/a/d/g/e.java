// 
// Decompiled by Procyon v0.6.0
// 

package b.b.a.a.d.g;

import android.net.Uri;
import android.text.TextUtils;

public class e
{
    public static a \u3007080(String path) {
        a e;
        final a a = e = b.b.a.a.d.g.e.a.e;
        if (TextUtils.isEmpty((CharSequence)path)) {
            return e;
        }
        try {
            path = Uri.parse(path).getPath();
            e = a;
            if (path != null) {
                a a2;
                if (path.endsWith(".css")) {
                    a2 = b.b.a.a.d.g.e.a.c;
                }
                else if (path.endsWith(".js")) {
                    a2 = b.b.a.a.d.g.e.a.d;
                }
                else {
                    e = a;
                    if (path.endsWith(".jpg")) {
                        return e;
                    }
                    e = a;
                    if (path.endsWith(".gif")) {
                        return e;
                    }
                    e = a;
                    if (path.endsWith(".png")) {
                        return e;
                    }
                    e = a;
                    if (path.endsWith(".jpeg")) {
                        return e;
                    }
                    e = a;
                    if (path.endsWith(".webp")) {
                        return e;
                    }
                    e = a;
                    if (path.endsWith(".bmp")) {
                        return e;
                    }
                    if (path.endsWith(".ico")) {
                        e = a;
                        return e;
                    }
                    e = a;
                    if (!path.endsWith(".html")) {
                        return e;
                    }
                    a2 = b.b.a.a.d.g.e.a.b;
                }
                e = a2;
            }
            return e;
        }
        finally {
            e = a;
            return e;
        }
    }
    
    public enum a
    {
        b("text/html"), 
        c("text/css"), 
        d("application/x-javascript"), 
        e("image/*");
        
        private static final a[] f;
        private String a;
        
        private a(final String a) {
            this.a = a;
        }
        
        public String b() {
            return this.a;
        }
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package b.b.a.a.d.g;

import android.text.TextUtils;
import android.content.Context;

public class b
{
    public static boolean \u3007080(final Context context) {
        boolean b = false;
        if (context == null) {
            return false;
        }
        if (TextUtils.getLayoutDirectionFromLocale(context.getResources().getConfiguration().locale) != 1) {
            return false;
        }
        if ((context.getApplicationInfo().flags & 0x400000) == 0x400000) {
            b = true;
        }
        return b;
    }
}

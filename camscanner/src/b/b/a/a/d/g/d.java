// 
// Decompiled by Procyon v0.6.0
// 

package b.b.a.a.d.g;

import java.lang.reflect.Method;
import android.view.Display;
import android.util.DisplayMetrics;
import android.view.WindowManager;
import java.util.Locale;
import androidx.core.os.o\u30070;
import androidx.appcompat.app.Oo08;
import android.os.Build$VERSION;
import b.b.a.a.d.e.a.a;
import android.content.Context;

public class d
{
    private static boolean \u3007080;
    
    public static float O8(final Context context, final float n) {
        Context e = context;
        if (context == null) {
            e = a.oO80().o\u30070().e();
        }
        return n * \u3007080(e);
    }
    
    public static String Oo08(final Context context) {
        String s = null;
        try {
            Locale locale;
            if (Build$VERSION.SDK_INT >= 24) {
                locale = o\u30070.\u3007080(Oo08.\u3007080(context.getResources().getConfiguration()), 0);
            }
            else {
                locale = Locale.getDefault();
            }
            locale.getLanguage();
        }
        finally {
            s = "";
        }
        return s;
    }
    
    public static int oO80(final Context context) {
        Context e = context;
        if (context == null) {
            e = a.oO80().o\u30070().e();
        }
        return e.getResources().getDisplayMetrics().widthPixels;
    }
    
    public static int o\u30070(final Context context) {
        Context e = context;
        if (context == null) {
            e = a.oO80().o\u30070().e();
        }
        final Display defaultDisplay = ((WindowManager)e.getSystemService("window")).getDefaultDisplay();
        final DisplayMetrics displayMetrics = new DisplayMetrics();
        defaultDisplay.getRealMetrics(displayMetrics);
        return displayMetrics.heightPixels;
    }
    
    private static float \u3007080(final Context context) {
        try {
            if (d.\u3007080) {
                final Class<?> loadClass = context.getClassLoader().loadClass("android.util.DisplayMetrics");
                final Method declaredMethod = loadClass.getDeclaredMethod("getDeviceDensity", (Class[])new Class[0]);
                declaredMethod.setAccessible(true);
                return (int)declaredMethod.invoke(loadClass, new Object[0]) / 160.0f;
            }
            return context.getResources().getDisplayMetrics().density;
        }
        catch (final Exception ex) {
            return context.getResources().getDisplayMetrics().density;
        }
    }
    
    public static float \u3007o00\u3007\u3007Oo(final Context context, final float n) {
        Context e = context;
        if (context == null) {
            e = a.oO80().o\u30070().e();
        }
        return n * \u3007080(e) + 0.5f;
    }
    
    public static int \u3007o\u3007(final float n, final float n2, final float n3, final float n4) {
        return (int)(n * 255.0f + 0.5f) << 24 | (int)(n2 * 255.0f + 0.5f) << 16 | (int)(n3 * 255.0f + 0.5f) << 8 | (int)(n4 * 255.0f + 0.5f);
    }
    
    public static int \u3007\u3007888(final Context context, final float n) {
        Context e = context;
        if (context == null) {
            e = a.oO80().o\u30070().e();
        }
        float \u3007080;
        if ((\u3007080 = \u3007080(e)) <= 0.0f) {
            \u3007080 = 1.0f;
        }
        return (int)(n / \u3007080 + 0.5f);
    }
}

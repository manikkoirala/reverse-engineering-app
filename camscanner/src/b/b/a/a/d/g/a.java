// 
// Decompiled by Procyon v0.6.0
// 

package b.b.a.a.d.g;

import com.bytedance.component.sdk.annotation.RequiresApi;
import android.renderscript.Allocation;
import android.renderscript.ScriptIntrinsicBlur;
import android.renderscript.Element;
import android.renderscript.RenderScript;
import android.graphics.Bitmap;
import android.content.Context;

public class a
{
    @RequiresApi(api = 17)
    public static Bitmap \u3007080(final Context context, Bitmap bitmap, final int n) {
        try {
            final Bitmap scaledBitmap = Bitmap.createScaledBitmap(bitmap, Math.round(bitmap.getWidth() * 0.2f), Math.round(bitmap.getHeight() * 0.2f), false);
            bitmap = Bitmap.createBitmap(scaledBitmap);
            final RenderScript create = RenderScript.create(context);
            if (create == null) {
                return null;
            }
            final ScriptIntrinsicBlur create2 = ScriptIntrinsicBlur.create(create, Element.U8_4(create));
            final Allocation fromBitmap = Allocation.createFromBitmap(create, scaledBitmap);
            final Allocation fromBitmap2 = Allocation.createFromBitmap(create, bitmap);
            create2.setRadius((float)n);
            create2.setInput(fromBitmap);
            create2.forEach(fromBitmap2);
            fromBitmap2.copyTo(bitmap);
            return bitmap;
        }
        finally {
            final Throwable t;
            t.printStackTrace();
            return null;
        }
    }
}

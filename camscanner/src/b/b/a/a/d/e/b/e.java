// 
// Decompiled by Procyon v0.6.0
// 

package b.b.a.a.d.e.b;

import b.b.a.a.k.g;
import java.util.Iterator;
import java.util.List;
import com.bytedance.sdk.component.utils.m;
import java.util.Set;
import java.util.Map;
import java.util.HashSet;
import java.util.TreeMap;
import org.json.JSONObject;
import android.text.TextUtils;
import b.b.a.a.d.e.c.b;
import java.util.concurrent.atomic.AtomicBoolean;

public class e
{
    private static volatile e \u3007o00\u3007\u3007Oo;
    private AtomicBoolean \u3007080;
    
    private e() {
        this.\u3007080 = new AtomicBoolean(false);
    }
    
    private void OO0o\u3007\u3007\u3007\u30070(final String s, final String s2, final String s3, final String s4, final String s5, final String s6) {
        d.oO80().O8(new b().oO80(s).O8(s2).o\u30070(s3).OO0o\u3007\u3007\u3007\u30070(s4).\u3007o00\u3007\u3007Oo(s5).\u3007O8o08O(s6).\u3007080(System.currentTimeMillis()));
        this.\u3007o00\u3007\u3007Oo();
    }
    
    private void Oo08(final String s, final String s2, final String s3) {
        if (TextUtils.isEmpty((CharSequence)s)) {
            return;
        }
        final JSONObject \u3007080 = this.\u3007080(s);
        if (\u3007080 != null) {
            final String optString = \u3007080.optString("md5");
            final String optString2 = \u3007080.optString("version");
            final String optString3 = \u3007080.optString("data");
            if (!TextUtils.isEmpty((CharSequence)optString) && !TextUtils.isEmpty((CharSequence)optString2)) {
                if (!TextUtils.isEmpty((CharSequence)optString3)) {
                    final b \u300781 = new b().oO80(s2).O8(s3).o\u30070(optString).OO0o\u3007\u3007\u3007\u30070(s).\u3007o00\u3007\u3007Oo(optString3).\u3007O8o08O(optString2).\u3007080(System.currentTimeMillis());
                    d.oO80().O8(\u300781);
                    this.\u3007o00\u3007\u3007Oo();
                    if (f.Oo08(optString2)) {
                        \u300781.\u3007O8o08O(optString2);
                        c.\u3007O\u3007().\u3007\u3007888(true);
                    }
                }
            }
        }
    }
    
    public static e oO80() {
        if (e.\u3007o00\u3007\u3007Oo == null) {
            synchronized (e.class) {
                if (e.\u3007o00\u3007\u3007Oo == null) {
                    e.\u3007o00\u3007\u3007Oo = new e();
                }
            }
        }
        return e.\u3007o00\u3007\u3007Oo;
    }
    
    private void o\u30070(final String s, final String s2, final String s3, final String s4, final String s5, final String s6) {
        synchronized (this) {
            if (this.\u300780\u3007808\u3007O(s) != null) {
                if (TextUtils.isEmpty((CharSequence)s4) || TextUtils.isEmpty((CharSequence)s3)) {
                    return;
                }
                this.OO0o\u3007\u3007\u3007\u30070(s6, s, s3, s2, s4, s5);
            }
            else if (TextUtils.isEmpty((CharSequence)s4)) {
                this.Oo08(s2, s6, s);
            }
            else if (TextUtils.isEmpty((CharSequence)s3)) {
                this.Oo08(s2, s6, s);
            }
            else {
                this.OO0o\u3007\u3007\u3007\u30070(s6, s, s3, s2, s4, s5);
            }
            final boolean oo08 = f.Oo08(s5);
            if (!a.\u3007\u3007808\u3007() || oo08) {
                c.\u3007O\u3007().\u3007\u3007888(true);
            }
        }
    }
    
    private JSONObject \u3007080(final String s) {
        if (b.b.a.a.d.e.a.a.oO80().o\u30070() == null) {
            return null;
        }
        final b.b.a.a.j.d.b d = b.b.a.a.d.e.a.a.oO80().o\u30070().d();
        d.o\u30070(s);
        final b.b.a.a.j.b \u300780\u3007808\u3007O = d.\u300780\u3007808\u3007O();
        if (\u300780\u3007808\u3007O != null) {
            try {
                if (\u300780\u3007808\u3007O.oO80() && \u300780\u3007808\u3007O.\u3007080() != null) {
                    return new JSONObject(\u300780\u3007808\u3007O.\u3007080());
                }
            }
            catch (final Exception ex) {
                ex.printStackTrace();
            }
        }
        return null;
    }
    
    private void \u3007o00\u3007\u3007Oo() {
        if (b.b.a.a.d.e.a.a.oO80().o\u30070() == null) {
            return;
        }
        int c;
        if ((c = b.b.a.a.d.e.a.a.oO80().o\u30070().c()) <= 0) {
            c = 100;
        }
        final List<b> \u3007o00\u3007\u3007Oo = d.oO80().\u3007o00\u3007\u3007Oo();
        int size = 0;
        if (\u3007o00\u3007\u3007Oo != null && !\u3007o00\u3007\u3007Oo.isEmpty() && c < \u3007o00\u3007\u3007Oo.size()) {
            final TreeMap treeMap = new TreeMap();
            for (final b b : \u3007o00\u3007\u3007Oo) {
                treeMap.put(b.\u30078o8o\u3007(), b);
            }
            final HashSet<String> set = new HashSet<String>();
            final int n = (int)(\u3007o00\u3007\u3007Oo.size() - c * 0.75f);
            final Iterator iterator2 = treeMap.entrySet().iterator();
            int n2 = 0;
            while (iterator2.hasNext()) {
                final Map.Entry<Long, V> entry = (Map.Entry<Long, V>)iterator2.next();
                if (entry == null) {
                    continue;
                }
                if (n2 >= n) {
                    continue;
                }
                ++n2;
                entry.getKey().longValue();
                final b b2 = (b)entry.getValue();
                if (b2 == null) {
                    continue;
                }
                set.add(b2.Oo08());
            }
            this.\u3007\u3007888(set);
            this.\u3007080.set(false);
            return;
        }
        if (\u3007o00\u3007\u3007Oo != null) {
            size = \u3007o00\u3007\u3007Oo.size();
        }
        final StringBuilder sb = new StringBuilder();
        sb.append("end doCheckAndDeleteTask maxTplCnt,local size");
        sb.append(c);
        sb.append(", Number of templates currently stored");
        sb.append(size);
        m.a("TmplDiffManager", sb.toString());
    }
    
    public void O8(final b.b.a.a.d.e.c.c c, final String s) {
        if (c == null) {
            m.b("TmplDiffManager", "saveTemplate error: tplInfo == null");
            return;
        }
        final String \u3007080 = c.\u3007080;
        final String \u3007o\u3007 = c.\u3007o\u3007;
        final String \u3007o00\u3007\u3007Oo = c.\u3007o00\u3007\u3007Oo;
        final String o8 = c.O8;
        final String oo08 = c.Oo08;
        String appId;
        if (b.b.a.a.d.e.a.a.oO80().o\u30070() != null) {
            appId = b.b.a.a.d.e.a.a.oO80().o\u30070().getAppId();
        }
        else {
            appId = "";
        }
        if (!TextUtils.isEmpty((CharSequence)s)) {
            appId = s;
        }
        if (TextUtils.isEmpty((CharSequence)\u3007080)) {
            m.b("TmplDiffManager", "saveTemplate error:tmpId is empty");
            return;
        }
        b.b.a.a.k.e.\u30078o8o\u3007(new g(this, "saveTemplate", \u3007080, \u3007o\u3007, \u3007o00\u3007\u3007Oo, o8, oo08, appId) {
            final String O8o08O8O;
            final String OO;
            final String o0;
            final String o\u300700O;
            final e \u3007080OO8\u30070;
            final String \u300708O\u300700\u3007o;
            final String \u3007OOo8\u30070;
            
            @Override
            public void run() {
                this.\u3007080OO8\u30070.o\u30070(this.o0, this.\u3007OOo8\u30070, this.OO, this.\u300708O\u300700\u3007o, this.o\u300700O, this.O8o08O8O);
            }
        }, 10);
    }
    
    public b \u300780\u3007808\u3007O(final String s) {
        if (TextUtils.isEmpty((CharSequence)s)) {
            return null;
        }
        return d.oO80().\u3007080(s);
    }
    
    public Set<String> \u30078o8o\u3007(final String s) {
        if (TextUtils.isEmpty((CharSequence)s)) {
            return null;
        }
        return d.oO80().\u3007\u3007888(s);
    }
    
    public void \u3007\u3007888(final Set<String> set) {
        try {
            d.oO80().Oo08(set);
        }
        finally {
            final Throwable t;
            m.a("TmplDiffManager", t.getMessage());
        }
    }
}

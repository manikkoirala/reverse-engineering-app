// 
// Decompiled by Procyon v0.6.0
// 

package b.b.a.a.d.e.b;

import com.bytedance.sdk.component.utils.e;
import java.io.File;
import java.util.Iterator;
import android.text.TextUtils;
import b.b.a.a.d.e.c.a;

public class f
{
    private static a \u3007080;
    
    public static a O8() {
        synchronized (f.class) {
            return f.\u3007080;
        }
    }
    
    public static boolean Oo08(final String s) {
        final boolean b = false;
        boolean b2 = false;
        try {
            if (O8() == null || TextUtils.isEmpty((CharSequence)O8().\u3007O8o08O())) {
                return true;
            }
            if (TextUtils.isEmpty((CharSequence)s)) {
                return false;
            }
            final String \u3007o8o08O = O8().\u3007O8o08O();
            final String[] split = s.split("\\.");
            final String[] split2 = \u3007o8o08O.split("\\.");
            for (int min = Math.min(split.length, split2.length), i = 0; i < min; ++i) {
                final int n = split[i].length() - split2[i].length();
                if (n != 0) {
                    boolean b3 = b;
                    if (n > 0) {
                        b3 = true;
                    }
                    return b3;
                }
                final int compareTo = split[i].compareTo(split2[i]);
                if (compareTo > 0) {
                    return true;
                }
                if (compareTo < 0) {
                    return false;
                }
                if (i == min - 1) {
                    if (split.length > split2.length) {
                        b2 = true;
                    }
                    return b2;
                }
            }
            return false;
        }
        finally {
            return false;
        }
    }
    
    public static void o\u30070() {
        // 
        // This method could not be decompiled.
        // 
        // Original Bytecode:
        // 
        //     1: astore_2       
        //     2: aconst_null    
        //     3: astore_0       
        //     4: invokestatic    b/b/a/a/d/e/b/c.\u3007\u30078O0\u30078:()Ljava/io/File;
        //     7: astore_3       
        //     8: new             Ljava/io/File;
        //    11: astore_1       
        //    12: aload_1        
        //    13: aload_3        
        //    14: ldc             "temp_pkg_info.json"
        //    16: invokespecial   java/io/File.<init>:(Ljava/io/File;Ljava/lang/String;)V
        //    19: aload_1        
        //    20: invokevirtual   java/io/File.length:()J
        //    23: invokestatic    java/lang/Long.valueOf:(J)Ljava/lang/Long;
        //    26: astore_3       
        //    27: aload_3        
        //    28: invokevirtual   java/lang/Long.longValue:()J
        //    31: lconst_0       
        //    32: lcmp           
        //    33: ifle            147
        //    36: aload_1        
        //    37: invokevirtual   java/io/File.exists:()Z
        //    40: ifeq            147
        //    43: aload_1        
        //    44: invokevirtual   java/io/File.isFile:()Z
        //    47: ifeq            147
        //    50: aload_3        
        //    51: invokevirtual   java/lang/Long.intValue:()I
        //    54: newarray        B
        //    56: astore_3       
        //    57: new             Ljava/io/FileInputStream;
        //    60: astore_0       
        //    61: aload_0        
        //    62: aload_1        
        //    63: invokespecial   java/io/FileInputStream.<init>:(Ljava/io/File;)V
        //    66: aload_0        
        //    67: aload_3        
        //    68: invokevirtual   java/io/InputStream.read:([B)I
        //    71: pop            
        //    72: new             Ljava/lang/String;
        //    75: astore_1       
        //    76: aload_1        
        //    77: aload_3        
        //    78: ldc             "utf-8"
        //    80: invokespecial   java/lang/String.<init>:([BLjava/lang/String;)V
        //    83: new             Lorg/json/JSONObject;
        //    86: astore_2       
        //    87: aload_2        
        //    88: aload_1        
        //    89: invokespecial   org/json/JSONObject.<init>:(Ljava/lang/String;)V
        //    92: aload_2        
        //    93: invokestatic    b/b/a/a/d/e/c/a.\u3007o00\u3007\u3007Oo:(Lorg/json/JSONObject;)Lb/b/a/a/d/e/c/a;
        //    96: astore_1       
        //    97: aload_1        
        //    98: ifnull          140
        //   101: aload_1        
        //   102: putstatic       b/b/a/a/d/e/b/f.\u3007080:Lb/b/a/a/d/e/c/a;
        //   105: new             Ljava/lang/StringBuilder;
        //   108: astore_1       
        //   109: aload_1        
        //   110: invokespecial   java/lang/StringBuilder.<init>:()V
        //   113: aload_1        
        //   114: ldc             "old version read success: "
        //   116: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   119: pop            
        //   120: aload_1        
        //   121: getstatic       b/b/a/a/d/e/b/f.\u3007080:Lb/b/a/a/d/e/c/a;
        //   124: invokevirtual   b/b/a/a/d/e/c/a.\u3007O8o08O:()Ljava/lang/String;
        //   127: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   130: pop            
        //   131: ldc             "Version"
        //   133: aload_1        
        //   134: invokevirtual   java/lang/StringBuilder.toString:()Ljava/lang/String;
        //   137: invokestatic    com/bytedance/sdk/component/utils/m.a:(Ljava/lang/String;Ljava/lang/String;)V
        //   140: goto            154
        //   143: astore_1       
        //   144: goto            164
        //   147: ldc             "Version"
        //   149: ldc             "version pkg json file does not exist"
        //   151: invokestatic    com/bytedance/sdk/component/utils/m.a:(Ljava/lang/String;Ljava/lang/String;)V
        //   154: aload_0        
        //   155: ifnull          180
        //   158: goto            176
        //   161: astore_1       
        //   162: aload_2        
        //   163: astore_0       
        //   164: ldc             "Version"
        //   166: ldc             "version init error"
        //   168: aload_1        
        //   169: invokestatic    com/bytedance/sdk/component/utils/m.b:(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
        //   172: aload_0        
        //   173: ifnull          180
        //   176: aload_0        
        //   177: invokevirtual   java/io/InputStream.close:()V
        //   180: return         
        //   181: astore_1       
        //   182: aload_0        
        //   183: ifnull          190
        //   186: aload_0        
        //   187: invokevirtual   java/io/InputStream.close:()V
        //   190: aload_1        
        //   191: athrow         
        //   192: astore_0       
        //   193: goto            180
        //   196: astore_0       
        //   197: goto            190
        //    Exceptions:
        //  Try           Handler
        //  Start  End    Start  End    Type                 
        //  -----  -----  -----  -----  ---------------------
        //  4      66     161    164    Any
        //  66     97     143    147    Any
        //  101    140    143    147    Any
        //  147    154    161    164    Any
        //  164    172    181    192    Any
        //  176    180    192    196    Ljava/io/IOException;
        //  186    190    196    200    Ljava/io/IOException;
        // 
        // The error that occurred was:
        // 
        // java.lang.IndexOutOfBoundsException: Index 107 out of bounds for length 107
        //     at java.base/jdk.internal.util.Preconditions.outOfBounds(Preconditions.java:64)
        //     at java.base/jdk.internal.util.Preconditions.outOfBoundsCheckIndex(Preconditions.java:70)
        //     at java.base/jdk.internal.util.Preconditions.checkIndex(Preconditions.java:266)
        //     at java.base/java.util.Objects.checkIndex(Objects.java:361)
        //     at java.base/java.util.ArrayList.get(ArrayList.java:427)
        //     at com.strobel.decompiler.ast.AstBuilder.convertToAst(AstBuilder.java:3362)
        //     at com.strobel.decompiler.ast.AstBuilder.build(AstBuilder.java:112)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:203)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:93)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethodBody(AstBuilder.java:868)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethod(AstBuilder.java:761)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addTypeMembers(AstBuilder.java:638)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeCore(AstBuilder.java:605)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeNoCache(AstBuilder.java:195)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createType(AstBuilder.java:162)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addType(AstBuilder.java:137)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.buildAst(JavaLanguage.java:71)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.decompileType(JavaLanguage.java:59)
        //     at com.strobel.decompiler.DecompilerDriver.decompileType(DecompilerDriver.java:333)
        //     at com.strobel.decompiler.DecompilerDriver.decompileJar(DecompilerDriver.java:254)
        //     at com.strobel.decompiler.DecompilerDriver.main(DecompilerDriver.java:144)
        // 
        throw new IllegalStateException("An error occurred while decompiling this method.");
    }
    
    public static a.a \u3007080(final String anObject) {
        if (TextUtils.isEmpty((CharSequence)anObject)) {
            return null;
        }
        if (O8() != null) {
            if (O8().OO0o\u3007\u3007\u3007\u30070() != null) {
                if (O8().OO0o\u3007\u3007()) {
                    for (final a.a a : O8().OO0o\u3007\u3007\u3007\u30070()) {
                        if (a.o\u30070() != null && a.o\u30070().equals(anObject)) {
                            return a;
                        }
                    }
                }
            }
        }
        return null;
    }
    
    public static void \u3007o00\u3007\u3007Oo() {
        final a o8 = O8();
        if (o8 == null) {
            return;
        }
        final File \u3007\u30078O0\u30078 = c.\u3007\u30078O0\u30078();
        try {
            new File(\u3007\u30078O0\u30078, "temp_pkg_info.json").delete();
        }
        finally {}
        if (o8.OO0o\u3007\u3007\u3007\u30070() != null) {
            for (final a.a a : o8.OO0o\u3007\u3007\u3007\u30070()) {
                try {
                    new File(\u3007\u30078O0\u30078, e.a(a.o\u30070())).delete();
                }
                finally {}
            }
        }
        f.\u3007080 = null;
    }
    
    public static void \u3007o\u3007(final a \u3007080) {
        monitorenter(f.class);
        if (\u3007080 != null) {
            try {
                if (\u3007080.OO0o\u3007\u3007()) {
                    f.\u3007080 = \u3007080;
                }
            }
            finally {
                monitorexit(f.class);
            }
        }
        monitorexit(f.class);
    }
    
    public static void \u3007\u3007888() {
        // 
        // This method could not be decompiled.
        // 
        // Original Bytecode:
        // 
        //     3: astore_0       
        //     4: aload_0        
        //     5: ifnonnull       16
        //     8: ldc             "Version"
        //    10: ldc             "version save error1"
        //    12: invokestatic    com/bytedance/sdk/component/utils/m.a:(Ljava/lang/String;Ljava/lang/String;)V
        //    15: return         
        //    16: aload_0        
        //    17: invokevirtual   b/b/a/a/d/e/c/a.Oooo8o0\u3007:()Ljava/lang/String;
        //    20: astore_2       
        //    21: aload_2        
        //    22: invokestatic    android/text/TextUtils.isEmpty:(Ljava/lang/CharSequence;)Z
        //    25: ifeq            36
        //    28: ldc             "Version"
        //    30: ldc             "version save error2"
        //    32: invokestatic    com/bytedance/sdk/component/utils/m.a:(Ljava/lang/String;Ljava/lang/String;)V
        //    35: return         
        //    36: new             Ljava/io/File;
        //    39: dup            
        //    40: invokestatic    b/b/a/a/d/e/b/c.\u3007\u30078O0\u30078:()Ljava/io/File;
        //    43: ldc             "temp_pkg_info.json"
        //    45: invokespecial   java/io/File.<init>:(Ljava/io/File;Ljava/lang/String;)V
        //    48: astore_1       
        //    49: new             Ljava/lang/StringBuilder;
        //    52: dup            
        //    53: invokespecial   java/lang/StringBuilder.<init>:()V
        //    56: astore_0       
        //    57: aload_0        
        //    58: aload_1        
        //    59: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/Object;)Ljava/lang/StringBuilder;
        //    62: pop            
        //    63: aload_0        
        //    64: ldc             ".tmp"
        //    66: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //    69: pop            
        //    70: new             Ljava/io/File;
        //    73: dup            
        //    74: aload_0        
        //    75: invokevirtual   java/lang/StringBuilder.toString:()Ljava/lang/String;
        //    78: invokespecial   java/io/File.<init>:(Ljava/lang/String;)V
        //    81: astore_3       
        //    82: aload_3        
        //    83: invokevirtual   java/io/File.exists:()Z
        //    86: ifeq            94
        //    89: aload_3        
        //    90: invokevirtual   java/io/File.delete:()Z
        //    93: pop            
        //    94: new             Ljava/io/FileOutputStream;
        //    97: astore_0       
        //    98: aload_0        
        //    99: aload_3        
        //   100: invokespecial   java/io/FileOutputStream.<init>:(Ljava/io/File;)V
        //   103: aload_0        
        //   104: aload_2        
        //   105: ldc             "utf-8"
        //   107: invokevirtual   java/lang/String.getBytes:(Ljava/lang/String;)[B
        //   110: invokevirtual   java/io/FileOutputStream.write:([B)V
        //   113: aload_1        
        //   114: invokevirtual   java/io/File.exists:()Z
        //   117: ifeq            125
        //   120: aload_1        
        //   121: invokevirtual   java/io/File.delete:()Z
        //   124: pop            
        //   125: aload_3        
        //   126: aload_1        
        //   127: invokevirtual   java/io/File.renameTo:(Ljava/io/File;)Z
        //   130: pop            
        //   131: aload_0        
        //   132: invokevirtual   java/io/FileOutputStream.close:()V
        //   135: goto            161
        //   138: astore_1       
        //   139: goto            145
        //   142: astore_1       
        //   143: aconst_null    
        //   144: astore_0       
        //   145: ldc             "Version"
        //   147: ldc             "version save error3"
        //   149: aload_1        
        //   150: invokestatic    com/bytedance/sdk/component/utils/m.b:(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
        //   153: aload_0        
        //   154: ifnull          161
        //   157: aload_0        
        //   158: invokevirtual   java/io/FileOutputStream.close:()V
        //   161: return         
        //   162: astore_1       
        //   163: aload_0        
        //   164: ifnull          171
        //   167: aload_0        
        //   168: invokevirtual   java/io/FileOutputStream.close:()V
        //   171: aload_1        
        //   172: athrow         
        //   173: astore_0       
        //   174: goto            161
        //   177: astore_0       
        //   178: goto            171
        //    Exceptions:
        //  Try           Handler
        //  Start  End    Start  End    Type                 
        //  -----  -----  -----  -----  ---------------------
        //  94     103    142    145    Any
        //  103    125    138    142    Any
        //  125    131    138    142    Any
        //  131    135    173    177    Ljava/io/IOException;
        //  145    153    162    173    Any
        //  157    161    173    177    Ljava/io/IOException;
        //  167    171    177    181    Ljava/io/IOException;
        // 
        // The error that occurred was:
        // 
        // java.lang.IllegalStateException: Expression is linked from several locations: Label_0171:
        //     at com.strobel.decompiler.ast.Error.expressionLinkedFromMultipleLocations(Error.java:27)
        //     at com.strobel.decompiler.ast.AstOptimizer.mergeDisparateObjectInitializations(AstOptimizer.java:2604)
        //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:235)
        //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:42)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:206)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:93)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethodBody(AstBuilder.java:868)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethod(AstBuilder.java:761)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addTypeMembers(AstBuilder.java:638)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeCore(AstBuilder.java:605)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeNoCache(AstBuilder.java:195)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createType(AstBuilder.java:162)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addType(AstBuilder.java:137)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.buildAst(JavaLanguage.java:71)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.decompileType(JavaLanguage.java:59)
        //     at com.strobel.decompiler.DecompilerDriver.decompileType(DecompilerDriver.java:333)
        //     at com.strobel.decompiler.DecompilerDriver.decompileJar(DecompilerDriver.java:254)
        //     at com.strobel.decompiler.DecompilerDriver.main(DecompilerDriver.java:144)
        // 
        throw new IllegalStateException("An error occurred while decompiling this method.");
    }
}

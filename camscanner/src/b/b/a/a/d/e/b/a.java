// 
// Decompiled by Procyon v0.6.0
// 

package b.b.a.a.d.e.b;

import java.util.Iterator;
import java.util.List;
import android.util.Pair;
import java.io.File;
import com.bytedance.sdk.component.utils.m;
import java.io.InputStream;
import java.io.FileInputStream;
import android.webkit.WebResourceResponse;
import org.json.JSONObject;
import b.b.a.a.d.e.c.c;
import android.text.TextUtils;
import java.util.Set;
import b.b.a.a.k.e;
import b.b.a.a.k.g;
import b.b.a.a.d.e.c.b;

public class a
{
    static Object \u3007080;
    
    static {
        a.\u3007080 = new Object();
    }
    
    private static void O8(final b b) {
        e.\u30078o8o\u3007(new g("updateTmplTime", b) {
            final b o0;
            
            @Override
            public void run() {
                synchronized (a.\u3007080) {
                    d.oO80().O8(this.o0);
                }
            }
        }, 10);
    }
    
    public static Set<String> OO0o\u3007\u3007(final String s) {
        return b.b.a.a.d.e.b.e.oO80().\u30078o8o\u3007(s);
    }
    
    public static b OO0o\u3007\u3007\u3007\u30070(final String s) {
        if (TextUtils.isEmpty((CharSequence)s)) {
            return null;
        }
        final b \u300780\u3007808\u3007O = b.b.a.a.d.e.b.e.oO80().\u300780\u3007808\u3007O(s);
        if (\u300780\u3007808\u3007O != null) {
            \u300780\u3007808\u3007O.\u3007080(System.currentTimeMillis());
            O8(\u300780\u3007808\u3007O);
        }
        return \u300780\u3007808\u3007O;
    }
    
    public static void Oo08(final c c) {
        b.b.a.a.d.e.b.e.oO80().O8(c, c.o\u30070);
    }
    
    public static void Oooo8o0\u3007() {
        b.b.a.a.d.e.b.c.\u3007O\u3007();
    }
    
    public static String oO80() {
        if (\u300780\u3007808\u3007O() == null) {
            return null;
        }
        return \u300780\u3007808\u3007O().\u3007o\u3007();
    }
    
    public static boolean o\u30070(final JSONObject jsonObject) {
        final boolean b = false;
        if (jsonObject == null) {
            return false;
        }
        final Object opt = jsonObject.opt("template_Plugin");
        boolean b2 = b;
        if (opt != null) {
            b2 = b;
            if (!TextUtils.isEmpty((CharSequence)opt.toString())) {
                b2 = true;
            }
        }
        return b2;
    }
    
    public static WebResourceResponse \u3007080(final String s, final b.b.a.a.d.g.e.a a, final String s2) {
        File file;
        if ((file = \u3007o00\u3007\u3007Oo(s)) == null) {
            file = \u3007\u3007888(s);
        }
        if (file != null) {
            try {
                final WebResourceResponse webResourceResponse = new WebResourceResponse(a.b(), "utf-8", (InputStream)new FileInputStream(file));
                final Throwable t;
                return (WebResourceResponse)t;
            }
            finally {
                final Throwable t;
                m.b("TTDynamic", "get html WebResourceResponse error", t);
            }
        }
        final Throwable t = null;
        return (WebResourceResponse)t;
    }
    
    public static b.b.a.a.d.e.c.a \u300780\u3007808\u3007O() {
        return b.b.a.a.d.e.b.c.\u3007O\u3007().\u3007O00();
    }
    
    public static b \u30078o8o\u3007(final String s) {
        return b.b.a.a.d.e.b.e.oO80().\u300780\u3007808\u3007O(s);
    }
    
    public static String \u3007O8o08O() {
        return d.o\u30070();
    }
    
    private static File \u3007o00\u3007\u3007Oo(final String anObject) {
        final b.b.a.a.d.e.c.a.b oo80 = \u300780\u3007808\u3007O().oO80();
        final File file = null;
        if (oo80 == null) {
            return null;
        }
        final List<Pair<String, String>> \u3007o00\u3007\u3007Oo = oo80.\u3007o00\u3007\u3007Oo();
        File file2 = file;
        if (\u3007o00\u3007\u3007Oo != null) {
            if (\u3007o00\u3007\u3007Oo.size() <= 0) {
                file2 = file;
            }
            else {
                final Iterator<Pair<String, String>> iterator = \u3007o00\u3007\u3007Oo.iterator();
                Object second;
                Pair pair;
                do {
                    file2 = file;
                    if (!iterator.hasNext()) {
                        return file2;
                    }
                    pair = iterator.next();
                    second = pair.second;
                } while (second == null || !((String)second).equals(anObject));
                file2 = new File(b.b.a.a.d.e.b.c.\u3007\u30078O0\u30078(), (String)pair.first);
            }
        }
        return file2;
    }
    
    public static void \u3007o\u3007() {
        try {
            f.\u3007o00\u3007\u3007Oo();
            final File \u3007\u30078O0\u30078 = b.b.a.a.d.e.b.c.\u3007\u30078O0\u30078();
            if (\u3007\u30078O0\u30078 != null && \u3007\u30078O0\u30078.exists()) {
                if (\u3007\u30078O0\u30078.getParentFile() != null) {
                    com.bytedance.sdk.component.utils.g.a(\u3007\u30078O0\u30078.getParentFile());
                }
                else {
                    com.bytedance.sdk.component.utils.g.a(\u3007\u30078O0\u30078);
                }
            }
        }
        finally {}
    }
    
    public static boolean \u3007\u3007808\u3007() {
        return b.b.a.a.d.e.b.c.\u3007O\u3007().OoO8();
    }
    
    private static File \u3007\u3007888(String a) {
        final boolean \u3007\u3007808\u3007 = \u3007\u3007808\u3007();
        File file2;
        final File file = file2 = null;
        if (\u3007\u3007808\u3007) {
            final Iterator<b.b.a.a.d.e.c.a.a> iterator = \u300780\u3007808\u3007O().OO0o\u3007\u3007\u3007\u30070().iterator();
            b.b.a.a.d.e.c.a.a a2;
            do {
                file2 = file;
                if (!iterator.hasNext()) {
                    return file2;
                }
                a2 = (b.b.a.a.d.e.c.a.a)iterator.next();
            } while (a2.o\u30070() == null || !a2.o\u30070().equals(a));
            a = com.bytedance.sdk.component.utils.e.a(a2.o\u30070());
            final File file3 = new File(b.b.a.a.d.e.b.c.\u3007\u30078O0\u30078(), a);
            final String a3 = com.bytedance.sdk.component.utils.e.a(file3);
            file2 = file;
            if (a2.O8() != null) {
                if (!a2.O8().equals(a3)) {
                    file2 = file;
                }
                else {
                    file2 = file3;
                }
            }
        }
        return file2;
    }
}

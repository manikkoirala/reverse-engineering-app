// 
// Decompiled by Procyon v0.6.0
// 

package b.b.a.a.d.e.b;

import java.util.ArrayList;
import java.util.List;
import com.bytedance.sdk.component.utils.m;
import android.database.Cursor;
import android.content.ContentValues;
import b.b.a.a.d.e.a.a;
import android.text.TextUtils;
import java.util.Collections;
import java.util.HashSet;
import b.b.a.a.d.e.c.b;
import android.util.LruCache;
import java.util.Set;

public class d
{
    private static volatile d O8;
    public static int Oo08 = 20;
    private Set<String> \u3007080;
    private final Object \u3007o00\u3007\u3007Oo;
    private LruCache<String, b> \u3007o\u3007;
    
    private d() {
        this.\u3007o00\u3007\u3007Oo = new Object();
        this.\u3007o\u3007 = new LruCache<String, b>(d.Oo08) {
            protected int \u3007080(final String s, final b b) {
                return 1;
            }
        };
        this.\u3007080 = Collections.synchronizedSet(new HashSet<String>());
    }
    
    public static d oO80() {
        if (d.O8 == null) {
            synchronized (d.class) {
                if (d.O8 == null) {
                    d.O8 = new d();
                }
            }
        }
        return d.O8;
    }
    
    public static String o\u30070() {
        final StringBuilder sb = new StringBuilder();
        sb.append("CREATE TABLE IF NOT EXISTS ");
        sb.append("template_diff_new");
        sb.append(" (_id INTEGER PRIMARY KEY AUTOINCREMENT,");
        sb.append("rit");
        sb.append(" TEXT ,");
        sb.append("id");
        sb.append(" TEXT UNIQUE,");
        sb.append("md5");
        sb.append(" TEXT ,");
        sb.append("url");
        sb.append(" TEXT , ");
        sb.append("data");
        sb.append(" TEXT , ");
        sb.append("version");
        sb.append(" TEXT , ");
        sb.append("update_time");
        sb.append(" TEXT");
        sb.append(")");
        return sb.toString();
    }
    
    private void \u300780\u3007808\u3007O(final String s) {
        if (TextUtils.isEmpty((CharSequence)s)) {
            return;
        }
        final LruCache<String, b> \u3007o\u3007 = this.\u3007o\u3007;
        if (\u3007o\u3007 != null) {
            if (\u3007o\u3007.size() > 0) {
                synchronized (this.\u3007o00\u3007\u3007Oo) {
                    this.\u3007o\u3007.remove((Object)s);
                }
            }
        }
    }
    
    public static void \u3007o\u3007(final int oo08) {
        d.Oo08 = oo08;
    }
    
    void O8(final b b) {
        if (b != null) {
            if (a.oO80().\u3007080() != null) {
                if (TextUtils.isEmpty((CharSequence)b.Oo08())) {
                    return;
                }
                final Cursor a = b.b.a.a.d.e.a.a.oO80().\u3007080().a("template_diff_new", null, "id=?", new String[] { b.Oo08() }, null, null, null);
                final boolean b2 = a != null && a.getCount() > 0;
                if (a != null) {
                    try {
                        a.close();
                    }
                    finally {}
                }
                final ContentValues contentValues = new ContentValues();
                contentValues.put("rit", b.\u300780\u3007808\u3007O());
                contentValues.put("id", b.Oo08());
                contentValues.put("md5", b.\u3007\u3007888());
                contentValues.put("url", b.OO0o\u3007\u3007());
                contentValues.put("data", b.\u3007o\u3007());
                contentValues.put("version", b.Oooo8o0\u3007());
                contentValues.put("update_time", b.\u30078o8o\u3007());
                if (b2) {
                    b.b.a.a.d.e.a.a.oO80().\u3007080().a("template_diff_new", contentValues, "id=?", new String[] { b.Oo08() });
                }
                else {
                    b.b.a.a.d.e.a.a.oO80().\u3007080().a("template_diff_new", contentValues);
                }
                synchronized (this.\u3007o00\u3007\u3007Oo) {
                    this.\u3007o\u3007.put((Object)b.Oo08(), (Object)b);
                    monitorexit(this.\u3007o00\u3007\u3007Oo);
                    this.\u3007080.add(b.Oo08());
                }
            }
        }
    }
    
    void Oo08(final Set<String> set) {
        if (set != null && !set.isEmpty()) {
            if (a.oO80().\u3007080() != null) {
                final String[] array = set.toArray(new String[set.size()]);
                if (array.length > 0) {
                    for (int i = 0; i < array.length; ++i) {
                        this.\u300780\u3007808\u3007O(array[i]);
                        a.oO80().\u3007080().a("template_diff_new", "id=?", new String[] { array[i] });
                    }
                }
            }
        }
    }
    
    b \u3007080(String a) {
        if (!TextUtils.isEmpty((CharSequence)a)) {
            if (a.oO80().\u3007080() != null) {
                final Object \u3007o00\u3007\u3007Oo = this.\u3007o00\u3007\u3007Oo;
                synchronized (\u3007o00\u3007\u3007Oo) {
                    final b b = (b)this.\u3007o\u3007.get((Object)String.valueOf(a));
                    monitorexit(\u3007o00\u3007\u3007Oo);
                    if (b != null) {
                        return b;
                    }
                    a = (String)a.oO80().\u3007080().a("template_diff_new", null, "id=?", new String[] { a }, null, null, null);
                    if (a != null) {
                        try {
                            if (((Cursor)a).moveToFirst()) {
                                while (true) {
                                    final String string = ((Cursor)a).getString(((Cursor)a).getColumnIndex("rit"));
                                    final String string2 = ((Cursor)a).getString(((Cursor)a).getColumnIndex("id"));
                                    final b \u3007080 = new b().oO80(string).O8(string2).o\u30070(((Cursor)a).getString(((Cursor)a).getColumnIndex("md5"))).OO0o\u3007\u3007\u3007\u30070(((Cursor)a).getString(((Cursor)a).getColumnIndex("url"))).\u3007o00\u3007\u3007Oo(((Cursor)a).getString(((Cursor)a).getColumnIndex("data"))).\u3007O8o08O(((Cursor)a).getString(((Cursor)a).getColumnIndex("version"))).\u3007080(((Cursor)a).getLong(((Cursor)a).getColumnIndex("update_time")));
                                    synchronized (this.\u3007o00\u3007\u3007Oo) {
                                        this.\u3007o\u3007.put((Object)string2, (Object)\u3007080);
                                        monitorexit(this.\u3007o00\u3007\u3007Oo);
                                        this.\u3007080.add(string2);
                                        if (!((Cursor)a).moveToNext()) {
                                            ((Cursor)a).close();
                                            return \u3007080;
                                        }
                                        continue;
                                    }
                                    break;
                                }
                            }
                            ((Cursor)a).close();
                        }
                        finally {
                            try {
                                m.b("TmplDbHelper", "getTemplate error", (Throwable)\u3007o00\u3007\u3007Oo);
                            }
                            finally {
                                ((Cursor)a).close();
                            }
                        }
                    }
                    return null;
                }
            }
        }
        return null;
    }
    
    List<b> \u3007o00\u3007\u3007Oo() {
        if (a.oO80().\u3007080() == null) {
            return null;
        }
        final ArrayList list = new ArrayList();
        final Cursor a = b.b.a.a.d.e.a.a.oO80().\u3007080().a("template_diff_new", null, null, null, null, null, null);
        if (a != null) {
            Label_0293: {
                final Throwable t2;
                try {
                    while (a.moveToNext()) {
                        final String string = a.getString(a.getColumnIndex("rit"));
                        final String string2 = a.getString(a.getColumnIndex("id"));
                        list.add(new b().oO80(string).O8(string2).o\u30070(a.getString(a.getColumnIndex("md5"))).OO0o\u3007\u3007\u3007\u30070(a.getString(a.getColumnIndex("url"))).\u3007o00\u3007\u3007Oo(a.getString(a.getColumnIndex("data"))).\u3007O8o08O(a.getString(a.getColumnIndex("version"))).\u3007080(a.getLong(a.getColumnIndex("update_time"))));
                        synchronized (this.\u3007o00\u3007\u3007Oo) {
                            this.\u3007o\u3007.put((Object)string2, list.get(list.size() - 1));
                            monitorexit(this.\u3007o00\u3007\u3007Oo);
                            this.\u3007080.add(string2);
                        }
                    }
                    break Label_0293;
                }
                finally {
                    final String s = "TmplDbHelper";
                    final String s2 = "getTemplate error";
                    final Throwable t = t2;
                    m.b(s, s2, t);
                }
                try {
                    final String s = "TmplDbHelper";
                    final String s2 = "getTemplate error";
                    final Throwable t = t2;
                    m.b(s, s2, t);
                }
                finally {
                    a.close();
                }
            }
        }
        return list;
    }
    
    Set<String> \u3007\u3007888(String a) {
        if (!TextUtils.isEmpty((CharSequence)a)) {
            if (a.oO80().\u3007080() != null) {
                final HashSet set = new HashSet();
                a = (String)a.oO80().\u3007080().a("template_diff_new", null, "rit=?", new String[] { a }, null, null, null);
                if (a != null) {
                    try {
                        if (((Cursor)a).moveToFirst()) {
                            do {
                                set.add(((Cursor)a).getString(((Cursor)a).getColumnIndex("id")));
                            } while (((Cursor)a).moveToNext());
                            return set;
                        }
                    }
                    catch (final Exception ex) {
                        ((Cursor)a).close();
                    }
                    finally {
                        ((Cursor)a).close();
                    }
                }
            }
        }
        return null;
    }
}

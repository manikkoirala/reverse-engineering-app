// 
// Decompiled by Procyon v0.6.0
// 

package b.b.a.a.d.e.b;

import android.text.TextUtils;
import com.bytedance.sdk.component.utils.z;
import com.bytedance.sdk.component.utils.r;
import b.b.a.a.k.g;
import b.b.a.a.j.b;
import com.bytedance.sdk.component.utils.m;
import java.util.Collection;
import java.util.ArrayList;
import com.bytedance.sdk.component.utils.e;
import java.util.Iterator;
import java.util.List;
import android.util.Pair;
import b.b.a.a.d.e.a.a;
import java.util.concurrent.atomic.AtomicLong;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicBoolean;
import java.io.File;

public class c
{
    private static volatile c oO80;
    private static File \u3007\u3007888;
    private AtomicBoolean O8;
    private AtomicInteger Oo08;
    private AtomicLong o\u30070;
    private AtomicBoolean \u3007080;
    private AtomicBoolean \u3007o00\u3007\u3007Oo;
    private boolean \u3007o\u3007;
    
    private c() {
        this.\u3007080 = new AtomicBoolean(true);
        this.\u3007o00\u3007\u3007Oo = new AtomicBoolean(false);
        this.\u3007o\u3007 = false;
        this.O8 = new AtomicBoolean(false);
        this.Oo08 = new AtomicInteger(0);
        this.o\u30070 = new AtomicLong();
        this.\u30070\u3007O0088o();
    }
    
    private void O8(final int n) {
        if (a.oO80().\u300780\u3007808\u3007O() != null) {
            a.oO80().\u300780\u3007808\u3007O().a(n);
        }
    }
    
    private void Oo08(final int n, final String s) {
        if (a.oO80().\u300780\u3007808\u3007O() != null) {
            a.oO80().\u300780\u3007808\u3007O().a(n, s);
        }
    }
    
    private void Oooo8o0\u3007() {
        if (this.Oo08.getAndSet(0) > 0 && System.currentTimeMillis() - this.o\u30070.get() > 600000L) {
            this.o800o8O();
        }
    }
    
    private boolean oO80(final b.b.a.a.d.e.c.a.b b) {
        if (b == null) {
            return false;
        }
        final List<Pair<String, String>> \u3007o00\u3007\u3007Oo = b.\u3007o00\u3007\u3007Oo();
        if (\u3007o00\u3007\u3007Oo != null && \u3007o00\u3007\u3007Oo.size() > 0) {
            final Iterator iterator = \u3007o00\u3007\u3007Oo.iterator();
            while (iterator.hasNext()) {
                final File file = new File(\u3007\u30078O0\u30078(), (String)((Pair)iterator.next()).first);
                if (!file.exists() || !file.isFile()) {
                    return false;
                }
            }
        }
        return true;
    }
    
    private void o\u30070(List<b.b.a.a.d.e.c.a.a> iterator) {
        if (iterator != null) {
            if (!((List)iterator).isEmpty()) {
                iterator = (Iterator<b.b.a.a.d.e.c.a.a>)((List<Object>)iterator).iterator();
                while (iterator.hasNext()) {
                    final File obj = new File(\u3007\u30078O0\u30078(), e.a(((b.b.a.a.d.e.c.a.a)iterator.next()).o\u30070()));
                    final StringBuilder sb = new StringBuilder();
                    sb.append(obj);
                    sb.append(".tmp");
                    final File file = new File(sb.toString());
                    if (obj.exists()) {
                        try {
                            obj.delete();
                        }
                        finally {}
                    }
                    if (file.exists()) {
                        try {
                            file.delete();
                        }
                        finally {}
                    }
                }
            }
        }
    }
    
    private List<b.b.a.a.d.e.c.a.a> \u3007080(b.b.a.a.d.e.c.a iterator, b.b.a.a.d.e.c.a a) {
        final ArrayList list = new ArrayList();
        final ArrayList list2 = new ArrayList();
        final ArrayList list3 = new ArrayList();
        if (a != null && !a.OO0o\u3007\u3007\u3007\u30070().isEmpty()) {
            if (iterator.OO0o\u3007\u3007\u3007\u30070().isEmpty()) {
                list.addAll(a.OO0o\u3007\u3007\u3007\u30070());
                m.a("TemplateManager", "loadTemplate update2");
            }
            else {
                for (final b.b.a.a.d.e.c.a.a a2 : iterator.OO0o\u3007\u3007\u3007\u30070()) {
                    if (!a.OO0o\u3007\u3007\u3007\u30070().contains(a2)) {
                        list2.add(a2);
                    }
                    else {
                        final b.b.a.a.d.e.c.a.a \u3007080 = f.\u3007080(a2.o\u30070());
                        if (\u3007080 == null || a2.O8() == null || a2.O8().equals(\u3007080.O8())) {
                            continue;
                        }
                        list2.add(a2);
                    }
                }
                for (final b.b.a.a.d.e.c.a.a a3 : a.OO0o\u3007\u3007\u3007\u30070()) {
                    if (!iterator.OO0o\u3007\u3007\u3007\u30070().contains(a3)) {
                        list.add(a3);
                    }
                }
                m.a("TemplateManager", "loadTemplate update3");
            }
        }
        else {
            list2.addAll(iterator.OO0o\u3007\u3007\u3007\u30070());
            m.a("TemplateManager", "loadTemplate update1");
        }
        iterator = (b.b.a.a.d.e.c.a)list2.iterator();
        while (((Iterator)iterator).hasNext()) {
            a = (b.b.a.a.d.e.c.a)((Iterator<b.b.a.a.d.e.c.a.a>)iterator).next();
            final String o\u30070 = ((b.b.a.a.d.e.c.a.a)a).o\u30070();
            final String a4 = e.a(o\u30070);
            final File obj = new File(\u3007\u30078O0\u30078(), a4);
            final StringBuilder sb = new StringBuilder();
            sb.append(obj);
            sb.append(".tmp");
            final File file = new File(sb.toString());
            if (obj.exists()) {
                try {
                    obj.delete();
                }
                finally {}
            }
            if (file.exists()) {
                try {
                    file.delete();
                }
                finally {}
            }
            final b.b.a.a.j.d.a g = a.oO80().o\u30070().g();
            g.o\u30070(o\u30070);
            g.\u3007O8o08O(\u3007\u30078O0\u30078().getAbsolutePath(), a4);
            final b oo0o\u3007\u3007 = g.OO0o\u3007\u3007();
            list3.add(a);
            if (oo0o\u3007\u3007 == null || !oo0o\u3007\u3007.oO80() || oo0o\u3007\u3007.Oo08() == null || !oo0o\u3007\u3007.Oo08().exists()) {
                Label_0562: {
                    if (oo0o\u3007\u3007 == null) {
                        final int \u3007o\u3007 = -1;
                        final String \u3007\u3007888 = "response is null";
                        break Label_0562;
                    }
                    try {
                        int \u3007o\u3007;
                        String \u3007\u3007888;
                        if (oo0o\u3007\u3007.Oo08() != null && oo0o\u3007\u3007.Oo08().exists()) {
                            \u3007o\u3007 = oo0o\u3007\u3007.\u3007o\u3007();
                            \u3007\u3007888 = oo0o\u3007\u3007.\u3007\u3007888();
                        }
                        else {
                            \u3007o\u3007 = -2;
                            \u3007\u3007888 = "file is null";
                        }
                        this.Oo08(\u3007o\u3007, \u3007\u3007888);
                    }
                    finally {
                        final Throwable t;
                        m.b("TemplateManager", new Object[] { t });
                    }
                }
                this.\u3007o00\u3007\u3007Oo.set(false);
                this.o\u30070(list3);
                m.a("TemplateManager", "loadTemplate error5");
                return null;
            }
            m.a("TemplateManager", "loadTemplate success");
        }
        return list;
    }
    
    private void \u30070\u3007O0088o() {
        b.b.a.a.k.e.\u3007O00(new g(this, "init") {
            final c o0;
            
            @Override
            public void run() {
                f.o\u30070();
                this.o0.\u3007080.set(false);
                this.o0.\u3007o\u3007();
                this.o0.o800o8O();
                if (b.b.a.a.d.e.a.a.oO80().o\u30070() != null && r.c(b.b.a.a.d.e.a.a.oO80().o\u30070().e())) {
                    b.b.a.a.d.e.a.a.oO80().o\u30070().j().post((Runnable)new Runnable(this) {
                        @Override
                        public void run() {
                            if (b.b.a.a.d.e.a.a.oO80().o\u30070() != null) {
                                b.b.a.a.d.e.a.a.oO80().o\u30070().i();
                            }
                        }
                    });
                }
            }
        });
    }
    
    private boolean \u300780\u3007808\u3007O(final String s) {
        final String a = e.a(s);
        final File absoluteFile = \u3007\u30078O0\u30078().getAbsoluteFile();
        final StringBuilder sb = new StringBuilder();
        sb.append(a);
        sb.append(".zip");
        final File file = new File(absoluteFile, sb.toString());
        final b.b.a.a.j.d.a g = b.b.a.a.d.e.a.a.oO80().o\u30070().g();
        g.o\u30070(s);
        g.\u3007O8o08O(file.getParent(), file.getName());
        final b oo0o\u3007\u3007 = g.OO0o\u3007\u3007();
        if (oo0o\u3007\u3007.oO80() && oo0o\u3007\u3007.Oo08() != null) {
            if (oo0o\u3007\u3007.Oo08().exists()) {
                final File oo08 = oo0o\u3007\u3007.Oo08();
                try {
                    z.a(oo08.getAbsolutePath(), file.getParent());
                    if (oo08.exists()) {
                        oo08.delete();
                    }
                    return true;
                }
                catch (final Exception ex) {
                    ex.printStackTrace();
                }
            }
        }
        return false;
    }
    
    private void \u3007O8o08O(List<b.b.a.a.d.e.c.a.a> iterator) {
        if (iterator != null) {
            if (!((List)iterator).isEmpty()) {
                iterator = (Iterator<b.b.a.a.d.e.c.a.a>)((List<Object>)iterator).iterator();
                while (iterator.hasNext()) {
                    final File obj = new File(\u3007\u30078O0\u30078(), e.a(((b.b.a.a.d.e.c.a.a)iterator.next()).o\u30070()));
                    final StringBuilder sb = new StringBuilder();
                    sb.append(obj);
                    sb.append(".tmp");
                    final File file = new File(sb.toString());
                    if (obj.exists()) {
                        try {
                            obj.delete();
                        }
                        finally {}
                    }
                    if (file.exists()) {
                        try {
                            file.delete();
                        }
                        finally {}
                    }
                }
            }
        }
    }
    
    public static c \u3007O\u3007() {
        if (c.oO80 == null) {
            synchronized (c.class) {
                if (c.oO80 == null) {
                    c.oO80 = new c();
                }
            }
        }
        return c.oO80;
    }
    
    private void \u3007o\u3007() {
        m.a("TemplateManager", "check template usable1");
        final b.b.a.a.d.e.c.a o8 = f.O8();
        if (o8 != null && o8.OO0o\u3007\u3007()) {
            final boolean b = this.oO80(o8.oO80()) || this.\u3007\u3007808\u3007(o8.OO0o\u3007\u3007\u3007\u30070());
            if (!b) {
                f.\u3007o00\u3007\u3007Oo();
            }
            final StringBuilder sb = new StringBuilder();
            sb.append("check template usable4: ");
            sb.append(b);
            m.a("TemplateManager", sb.toString());
            this.\u3007o\u3007 = b;
            return;
        }
        m.a("TemplateManager", "check template usable2");
    }
    
    private boolean \u3007\u3007808\u3007(final List<b.b.a.a.d.e.c.a.a> list) {
        if (list != null && list.size() > 0) {
            for (final b.b.a.a.d.e.c.a.a a : list) {
                final File file = new File(\u3007\u30078O0\u30078(), e.a(a.o\u30070()));
                final String a2 = e.a(file);
                if (!file.exists() || !file.isFile() || a.O8() == null || !a.O8().equals(a2)) {
                    return false;
                }
            }
            return true;
        }
        return false;
    }
    
    public static File \u3007\u30078O0\u30078() {
        if (c.\u3007\u3007888 == null) {
            try {
                final File \u3007\u3007888 = new File(new File(b.b.a.a.d.e.b.b.\u3007o00\u3007\u3007Oo(), "tt_tmpl_pkg"), "template");
                \u3007\u3007888.mkdirs();
                c.\u3007\u3007888 = \u3007\u3007888;
            }
            finally {
                final Throwable t;
                m.b("TemplateManager", "getTemplateDir error", t);
            }
        }
        return c.\u3007\u3007888;
    }
    
    public void OO0o\u3007\u3007(final boolean newValue) {
        this.O8.set(newValue);
    }
    
    public void OO0o\u3007\u3007\u3007\u30070() {
        this.O8.set(true);
        this.\u3007o\u3007 = false;
        this.\u3007o00\u3007\u3007Oo.set(false);
    }
    
    public boolean OoO8() {
        return this.\u3007o\u3007;
    }
    
    public void o800o8O() {
        this.\u3007\u3007888(false);
    }
    
    public b.b.a.a.d.e.c.a \u3007O00() {
        return f.O8();
    }
    
    public void \u3007O888o0o() {
        this.\u30070\u3007O0088o();
    }
    
    public void \u3007\u3007888(final boolean b) {
        if (this.\u3007080.get()) {
            m.a("TemplateManager", "loadTemplate error1");
            return;
        }
        try {
            if (this.\u3007o00\u3007\u3007Oo.get()) {
                if (b) {
                    this.Oo08.getAndIncrement();
                }
                final StringBuilder sb = new StringBuilder();
                sb.append("loadTemplate error2: ");
                sb.append(b);
                m.a("TemplateManager", sb.toString());
                return;
            }
            final AtomicBoolean \u3007o00\u3007\u3007Oo = this.\u3007o00\u3007\u3007Oo;
            final int n = 1;
            \u3007o00\u3007\u3007Oo.set(true);
            final b.b.a.a.d.e.c.a a = b.b.a.a.d.e.a.a.oO80().o\u30070().a();
            final b.b.a.a.d.e.c.a o8 = f.O8();
            if (a == null || !a.OO0o\u3007\u3007()) {
                this.\u3007o00\u3007\u3007Oo.set(false);
                this.O8(109);
                m.a("TemplateManager", "loadTemplate error3");
                return;
            }
            final boolean oo08 = f.Oo08(a.\u3007O8o08O());
            if (!oo08) {
                this.\u3007o00\u3007\u3007Oo.set(false);
                this.o\u30070.set(System.currentTimeMillis());
                m.a("TemplateManager", "loadTemplate error4");
                return;
            }
            if (oo08 && b.b.a.a.d.e.a.a.oO80().o\u30070() != null) {
                b.b.a.a.d.e.a.a.oO80().o\u30070().j().post((Runnable)new Runnable(this) {
                    @Override
                    public void run() {
                        b.b.a.a.d.h.e.\u3007O8o08O().OO0o\u3007\u3007\u3007\u30070();
                    }
                });
            }
            int n2 = (a.oO80() != null && !TextUtils.isEmpty((CharSequence)a.oO80().Oo08()) && this.\u300780\u3007808\u3007O(a.oO80().Oo08())) ? 1 : 0;
            List<b.b.a.a.d.e.c.a.a> \u3007080;
            if (n2 == 0) {
                \u3007080 = this.\u3007080(a, o8);
                if (\u3007080 != null) {
                    n2 = n;
                }
                else {
                    n2 = 0;
                }
            }
            else {
                \u3007080 = null;
            }
            if (n2 != 0 && (this.\u3007\u3007808\u3007(a.OO0o\u3007\u3007\u3007\u30070()) || this.oO80(a.oO80()))) {
                f.\u3007o\u3007(a);
                f.\u3007\u3007888();
                this.\u3007O8o08O(\u3007080);
            }
            final StringBuilder sb2 = new StringBuilder();
            sb2.append("loadTemplate update success: ");
            sb2.append(a.\u3007O8o08O());
            m.a("TemplateManager", sb2.toString());
            this.\u3007o\u3007();
            this.\u3007o00\u3007\u3007Oo.set(false);
            this.o\u30070.set(System.currentTimeMillis());
            this.Oooo8o0\u3007();
        }
        finally {
            final Throwable t;
            m.a("TemplateManager", "loadTemplate error: ", t);
        }
    }
}

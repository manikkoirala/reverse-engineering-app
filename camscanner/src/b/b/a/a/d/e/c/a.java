// 
// Decompiled by Procyon v0.6.0
// 

package b.b.a.a.d.e.c;

import android.text.TextUtils;
import java.util.Iterator;
import org.json.JSONArray;
import android.util.Pair;
import java.util.ArrayList;
import org.json.JSONObject;
import java.util.List;

public class a
{
    private List<a> O8;
    private b Oo08;
    private String \u3007080;
    private String \u3007o00\u3007\u3007Oo;
    private String \u3007o\u3007;
    
    public static a \u3007080(final String s) {
        if (s == null) {
            return null;
        }
        try {
            return \u3007o00\u3007\u3007Oo(new JSONObject(s));
        }
        catch (final Exception ex) {
            return null;
        }
    }
    
    public static a \u3007o00\u3007\u3007Oo(final JSONObject jsonObject) {
        final a a = null;
        if (jsonObject == null) {
            return null;
        }
        final a a2 = new a();
        a2.\u300780\u3007808\u3007O(jsonObject.optString("name"));
        a2.\u30078o8o\u3007(jsonObject.optString("version"));
        a2.\u3007\u3007888(jsonObject.optString("main"));
        final JSONArray optJSONArray = jsonObject.optJSONArray("resources");
        final ArrayList list = new ArrayList();
        if (optJSONArray != null && optJSONArray.length() > 0) {
            for (int i = 0; i < optJSONArray.length(); ++i) {
                final JSONObject optJSONObject = optJSONArray.optJSONObject(i);
                final a a3 = new a();
                a3.Oo08(optJSONObject.optString("url"));
                a3.\u3007o\u3007(optJSONObject.optString("md5"));
                a3.\u3007o00\u3007\u3007Oo(optJSONObject.optInt("level"));
                list.add(a3);
            }
        }
        a2.Oo08(list);
        if (jsonObject.has("resources_archive")) {
            final JSONObject optJSONObject2 = jsonObject.optJSONObject("resources_archive");
            if (optJSONObject2 != null) {
                final b b = new b();
                b.\u3007\u3007888(optJSONObject2.optString("url"));
                b.\u3007o\u3007(optJSONObject2.optString("md5"));
                final JSONObject optJSONObject3 = optJSONObject2.optJSONObject("map");
                if (optJSONObject3 != null) {
                    final Iterator keys = optJSONObject3.keys();
                    final ArrayList<Pair<String, String>> list2 = new ArrayList<Pair<String, String>>();
                    while (keys.hasNext()) {
                        final String s = keys.next();
                        list2.add((Pair<String, String>)new Pair((Object)s, (Object)optJSONObject3.optString(s)));
                    }
                    b.O8(list2);
                }
                a2.O8(b);
            }
        }
        a a4;
        if (!a2.OO0o\u3007\u3007()) {
            a4 = a;
        }
        else {
            a4 = a2;
        }
        return a4;
    }
    
    public void O8(final b oo08) {
        this.Oo08 = oo08;
    }
    
    public boolean OO0o\u3007\u3007() {
        return !TextUtils.isEmpty((CharSequence)this.\u3007o\u3007()) && !TextUtils.isEmpty((CharSequence)this.\u3007O8o08O()) && !TextUtils.isEmpty((CharSequence)this.o\u30070());
    }
    
    public List<a> OO0o\u3007\u3007\u3007\u30070() {
        if (this.O8 == null) {
            this.O8 = new ArrayList<a>();
        }
        return this.O8;
    }
    
    public void Oo08(final List<a> list) {
        List<a> o8 = list;
        if (list == null) {
            o8 = new ArrayList<a>();
        }
        this.O8 = o8;
    }
    
    public String Oooo8o0\u3007() {
        Label_0303: {
            if (!this.OO0o\u3007\u3007()) {
                break Label_0303;
            }
            try {
                final JSONObject jsonObject = new JSONObject();
                jsonObject.putOpt("name", (Object)this.o\u30070());
                jsonObject.putOpt("version", (Object)this.\u3007O8o08O());
                jsonObject.putOpt("main", (Object)this.\u3007o\u3007());
                final JSONArray jsonArray = new JSONArray();
                if (this.OO0o\u3007\u3007\u3007\u30070() != null) {
                    for (final a a : this.OO0o\u3007\u3007\u3007\u30070()) {
                        final JSONObject jsonObject2 = new JSONObject();
                        jsonObject2.putOpt("url", (Object)a.o\u30070());
                        jsonObject2.putOpt("md5", (Object)a.O8());
                        jsonObject2.putOpt("level", (Object)a.\u3007080());
                        jsonArray.put((Object)jsonObject2);
                    }
                }
                jsonObject.putOpt("resources", (Object)jsonArray);
                final b oo80 = this.oO80();
                if (oo80 != null) {
                    final JSONObject jsonObject3 = new JSONObject();
                    jsonObject3.put("url", (Object)oo80.\u3007080);
                    jsonObject3.put("md5", (Object)oo80.\u3007o00\u3007\u3007Oo);
                    final JSONObject jsonObject4 = new JSONObject();
                    final List<Pair<String, String>> \u3007o00\u3007\u3007Oo = oo80.\u3007o00\u3007\u3007Oo();
                    if (\u3007o00\u3007\u3007Oo != null) {
                        for (final Pair pair : \u3007o00\u3007\u3007Oo) {
                            jsonObject4.put((String)pair.first, pair.second);
                        }
                    }
                    jsonObject3.put("map", (Object)jsonObject4);
                    jsonObject.putOpt("resources_archive", (Object)jsonObject3);
                }
                return jsonObject.toString();
                return null;
            }
            finally {
                return null;
            }
        }
    }
    
    public b oO80() {
        return this.Oo08;
    }
    
    public String o\u30070() {
        return this.\u3007080;
    }
    
    public void \u300780\u3007808\u3007O(final String \u3007080) {
        this.\u3007080 = \u3007080;
    }
    
    public void \u30078o8o\u3007(final String \u3007o00\u3007\u3007Oo) {
        this.\u3007o00\u3007\u3007Oo = \u3007o00\u3007\u3007Oo;
    }
    
    public String \u3007O8o08O() {
        return this.\u3007o00\u3007\u3007Oo;
    }
    
    public String \u3007o\u3007() {
        return this.\u3007o\u3007;
    }
    
    public void \u3007\u3007888(final String \u3007o\u3007) {
        this.\u3007o\u3007 = \u3007o\u3007;
    }
    
    public static class a
    {
        private String \u3007080;
        private String \u3007o00\u3007\u3007Oo;
        private int \u3007o\u3007;
        
        public String O8() {
            return this.\u3007o00\u3007\u3007Oo;
        }
        
        public void Oo08(final String \u3007080) {
            this.\u3007080 = \u3007080;
        }
        
        @Override
        public boolean equals(final Object obj) {
            if (obj instanceof a) {
                final String \u3007080 = this.\u3007080;
                return \u3007080 != null && \u3007080.equals(((a)obj).o\u30070());
            }
            return super.equals(obj);
        }
        
        public String o\u30070() {
            return this.\u3007080;
        }
        
        public int \u3007080() {
            return this.\u3007o\u3007;
        }
        
        public void \u3007o00\u3007\u3007Oo(final int \u3007o\u3007) {
            this.\u3007o\u3007 = \u3007o\u3007;
        }
        
        public void \u3007o\u3007(final String \u3007o00\u3007\u3007Oo) {
            this.\u3007o00\u3007\u3007Oo = \u3007o00\u3007\u3007Oo;
        }
    }
    
    public static class b
    {
        private String \u3007080;
        private String \u3007o00\u3007\u3007Oo;
        private List<Pair<String, String>> \u3007o\u3007;
        
        public void O8(final List<Pair<String, String>> \u3007o\u3007) {
            this.\u3007o\u3007 = \u3007o\u3007;
        }
        
        public String Oo08() {
            return this.\u3007080;
        }
        
        public List<Pair<String, String>> \u3007o00\u3007\u3007Oo() {
            return this.\u3007o\u3007;
        }
        
        public void \u3007o\u3007(final String \u3007o00\u3007\u3007Oo) {
            this.\u3007o00\u3007\u3007Oo = \u3007o00\u3007\u3007Oo;
        }
        
        public void \u3007\u3007888(final String \u3007080) {
            this.\u3007080 = \u3007080;
        }
    }
}

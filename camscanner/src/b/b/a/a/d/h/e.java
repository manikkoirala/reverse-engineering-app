// 
// Decompiled by Procyon v0.6.0
// 

package b.b.a.a.d.h;

import android.text.TextUtils;
import b.b.a.a.e.z;
import android.webkit.WebView;
import com.bytedance.component.sdk.annotation.UiThread;
import com.bytedance.sdk.component.utils.m;
import java.util.Iterator;
import android.annotation.SuppressLint;
import android.webkit.WebSettings$LayoutAlgorithm;
import android.webkit.DownloadListener;
import android.webkit.WebViewClient;
import android.webkit.WebChromeClient;
import b.b.a.a.d.e.a.a;
import java.util.HashMap;
import java.util.ArrayList;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.Map;
import com.bytedance.sdk.component.widget.SSWebView;
import java.util.List;

public class e
{
    private static int O8 = 10;
    private static volatile e Oo08;
    private List<SSWebView> \u3007080;
    private Map<Integer, c> \u3007o00\u3007\u3007Oo;
    private Map<Integer, d> \u3007o\u3007;
    
    private e() {
        new AtomicBoolean(false);
        this.\u3007080 = new ArrayList<SSWebView>();
        this.\u3007o00\u3007\u3007Oo = new HashMap<Integer, c>();
        this.\u3007o\u3007 = new HashMap<Integer, d>();
        final b.b.a.a.d.e.a.c o\u30070 = a.oO80().o\u30070();
        if (o\u30070 != null) {
            e.O8 = o\u30070.b();
        }
    }
    
    private void \u30078o8o\u3007(final SSWebView ssWebView) {
        ssWebView.removeAllViews();
        ssWebView.u();
        ssWebView.setWebChromeClient((WebChromeClient)null);
        ssWebView.setWebViewClient((WebViewClient)null);
        ssWebView.setDownloadListener((DownloadListener)null);
        ssWebView.setJavaScriptEnabled(true);
        ssWebView.setAppCacheEnabled(false);
        ssWebView.setSupportZoom(false);
        ssWebView.setUseWideViewPort(true);
        ssWebView.setJavaScriptCanOpenWindowsAutomatically(true);
        ssWebView.setDomStorageEnabled(true);
        ssWebView.setBuiltInZoomControls(false);
        ssWebView.setLayoutAlgorithm(WebSettings$LayoutAlgorithm.NORMAL);
        ssWebView.setLoadWithOverviewMode(false);
        ssWebView.setUserAgentString("android_client");
        ssWebView.setDefaultTextEncodingName("UTF-8");
        ssWebView.setDefaultFontSize(16);
    }
    
    public static e \u3007O8o08O() {
        if (e.Oo08 == null) {
            synchronized (e.class) {
                if (e.Oo08 == null) {
                    e.Oo08 = new e();
                }
            }
        }
        return e.Oo08;
    }
    
    @SuppressLint({ "JavascriptInterface" })
    public void O8(final SSWebView ssWebView, final b b) {
        if (ssWebView != null) {
            if (b != null) {
                final c c = this.\u3007o00\u3007\u3007Oo.get(ssWebView.hashCode());
                c c2;
                if (c != null) {
                    c.\u3007080(b);
                    c2 = c;
                }
                else {
                    c2 = new c(b);
                    this.\u3007o00\u3007\u3007Oo.put(ssWebView.hashCode(), c2);
                }
                ssWebView.a((Object)c2, "SDK_INJECT_GLOBAL");
            }
        }
    }
    
    public void OO0o\u3007\u3007(final SSWebView ssWebView) {
        if (ssWebView == null) {
            return;
        }
        final c c = this.\u3007o00\u3007\u3007Oo.get(ssWebView.hashCode());
        if (c != null) {
            c.\u3007080(null);
        }
        ssWebView.d("SDK_INJECT_GLOBAL");
    }
    
    public void OO0o\u3007\u3007\u3007\u30070() {
        for (final SSWebView ssWebView : this.\u3007080) {
            if (ssWebView != null) {
                ssWebView.g();
            }
        }
        this.\u3007080.clear();
    }
    
    public boolean Oo08(final SSWebView ssWebView) {
        if (ssWebView == null) {
            return false;
        }
        m.a("WebViewPool", "WebView render fail and abandon");
        ssWebView.g();
        return true;
    }
    
    public int oO80() {
        return this.\u3007080.size();
    }
    
    public int o\u30070() {
        return this.\u3007080.size();
    }
    
    public SSWebView \u3007080() {
        if (this.oO80() <= 0) {
            return null;
        }
        final SSWebView ssWebView = this.\u3007080.remove(0);
        if (ssWebView == null) {
            return null;
        }
        final StringBuilder sb = new StringBuilder();
        sb.append("get WebView from pool; current available count: ");
        sb.append(this.oO80());
        m.a("WebViewPool", sb.toString());
        return ssWebView;
    }
    
    @UiThread
    public void \u300780\u3007808\u3007O(final SSWebView ssWebView) {
        if (ssWebView == null) {
            return;
        }
        this.\u30078o8o\u3007(ssWebView);
        ssWebView.d("SDK_INJECT_GLOBAL");
        this.OO0o\u3007\u3007(ssWebView);
        this.\u3007\u3007888(ssWebView);
    }
    
    @SuppressLint({ "JavascriptInterface" })
    public void \u3007o00\u3007\u3007Oo(final WebView webView, final z z, final String s) {
        if (webView != null && z != null) {
            if (!TextUtils.isEmpty((CharSequence)s)) {
                final d d = this.\u3007o\u3007.get(webView.hashCode());
                d d2;
                if (d != null) {
                    d.\u3007080(z);
                    d2 = d;
                }
                else {
                    d2 = new d(z);
                    this.\u3007o\u3007.put(webView.hashCode(), d2);
                }
                webView.addJavascriptInterface((Object)d2, s);
            }
        }
    }
    
    public void \u3007o\u3007(final WebView webView, final String s) {
        if (webView != null) {
            if (!TextUtils.isEmpty((CharSequence)s)) {
                final d d = this.\u3007o\u3007.get(webView.hashCode());
                if (d != null) {
                    d.\u3007080(null);
                }
                webView.removeJavascriptInterface(s);
            }
        }
    }
    
    public void \u3007\u3007888(final SSWebView ssWebView) {
        if (ssWebView == null) {
            return;
        }
        if (this.\u3007080.size() >= e.O8) {
            m.a("WebViewPool", "WebView pool is full\uff0cdestroy webview");
            ssWebView.g();
        }
        else if (!this.\u3007080.contains(ssWebView)) {
            this.\u3007080.add(ssWebView);
            final StringBuilder sb = new StringBuilder();
            sb.append("recycle WebView\uff0ccurrent available count: ");
            sb.append(this.oO80());
            m.a("WebViewPool", sb.toString());
        }
    }
}

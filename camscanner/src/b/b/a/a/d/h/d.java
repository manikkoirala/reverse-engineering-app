// 
// Decompiled by Procyon v0.6.0
// 

package b.b.a.a.d.h;

import android.webkit.JavascriptInterface;
import b.b.a.a.e.z;
import java.lang.ref.WeakReference;

public class d
{
    private WeakReference<z> \u3007080;
    
    public d(final z referent) {
        this.\u3007080 = new WeakReference<z>(referent);
    }
    
    @JavascriptInterface
    public void invokeMethod(final String s) {
        final WeakReference<z> \u3007080 = this.\u3007080;
        if (\u3007080 != null && \u3007080.get() != null) {
            this.\u3007080.get().invokeMethod(s);
        }
    }
    
    public void \u3007080(final z referent) {
        this.\u3007080 = new WeakReference<z>(referent);
    }
}

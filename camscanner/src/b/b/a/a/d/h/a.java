// 
// Decompiled by Procyon v0.6.0
// 

package b.b.a.a.d.h;

import android.view.ViewGroup;
import android.app.Activity;
import android.os.Handler;
import android.os.Looper;
import com.bytedance.sdk.component.utils.l;
import android.text.TextUtils;
import android.view.View;
import b.b.a.a.d.f.n;
import com.bytedance.component.sdk.annotation.UiThread;
import android.view.ViewGroup$LayoutParams;
import android.widget.FrameLayout$LayoutParams;
import b.b.a.a.d.c;
import com.bytedance.sdk.component.adexpress.theme.ThemeStatusBroadcastReceiver;
import java.util.concurrent.atomic.AtomicBoolean;
import b.b.a.a.d.e.c.b;
import com.bytedance.sdk.component.widget.SSWebView;
import b.b.a.a.d.f.m;
import b.b.a.a.d.f.h;
import b.b.a.a.d.f.g;
import org.json.JSONObject;
import android.content.Context;
import b.b.a.a.d.f.k;
import b.b.a.a.d.f.d;

public abstract class a implements d, k, com.bytedance.sdk.component.adexpress.theme.a
{
    private Context a;
    private JSONObject b;
    private String c;
    private volatile g d;
    private boolean e;
    protected boolean f;
    private h g;
    private m h;
    protected SSWebView i;
    private boolean j;
    protected int k;
    protected b l;
    protected AtomicBoolean m;
    
    public a(final Context a, final m h, final ThemeStatusBroadcastReceiver themeStatusBroadcastReceiver) {
        this.e = false;
        this.k = 8;
        this.m = new AtomicBoolean(false);
        this.a = a;
        (this.h = h).\u3007080();
        themeStatusBroadcastReceiver.a((com.bytedance.sdk.component.adexpress.theme.a)this);
        final SSWebView \u3007080 = b.b.a.a.d.h.e.\u3007O8o08O().\u3007080();
        this.i = \u3007080;
        if (\u3007080 == null) {
            if (b.b.a.a.d.c.\u3007080() != null) {
                this.i = new SSWebView(b.b.a.a.d.c.\u3007080());
            }
        }
        else {
            this.e = true;
        }
    }
    
    @UiThread
    private void a(final float n, final float n2) {
        this.h.\u3007\u3007808\u3007().d();
        final int width = (int)b.b.a.a.d.g.d.\u3007o00\u3007\u3007Oo(this.a, n);
        final int height = (int)b.b.a.a.d.g.d.\u3007o00\u3007\u3007Oo(this.a, n2);
        FrameLayout$LayoutParams layoutParams;
        if ((layoutParams = (FrameLayout$LayoutParams)((View)this.f()).getLayoutParams()) == null) {
            layoutParams = new FrameLayout$LayoutParams(width, height);
        }
        layoutParams.width = width;
        layoutParams.height = height;
        ((View)this.f()).setLayoutParams((ViewGroup$LayoutParams)layoutParams);
    }
    
    private void a(final n n, final float n2, final float n3) {
        if (this.f && !this.j) {
            this.a(n2, n3);
            this.b(this.k);
            if (this.d != null) {
                this.d.a((View)this.f(), n);
            }
        }
        else {
            b.b.a.a.d.h.e.\u3007O8o08O().Oo08(this.i);
            this.c(n.OO0o\u3007\u3007());
        }
    }
    
    private void c(final int n) {
        if (this.d != null) {
            this.d.a(n);
        }
    }
    
    public void a() {
    }
    
    @Override
    public void a(final View view, final int n, final b.b.a.a.d.b b) {
        final h g = this.g;
        if (g != null) {
            g.a(view, n, b);
        }
    }
    
    @Override
    public void a(final g d) {
        this.d = d;
        if (this.f() == null || this.f().getWebView() == null) {
            this.d.a(102);
            return;
        }
        if (!b.b.a.a.d.e.b.a.\u3007\u3007808\u3007()) {
            this.d.a(102);
            return;
        }
        if (TextUtils.isEmpty((CharSequence)this.c)) {
            this.d.a(102);
            return;
        }
        if (this.l == null && !b.b.a.a.d.e.b.a.o\u30070(this.b)) {
            this.d.a(103);
            return;
        }
        this.h.\u3007\u3007808\u3007().a(this.e);
        if (this.e) {
            try {
                this.i.f();
                this.h.\u3007\u3007808\u3007().c();
                com.bytedance.sdk.component.utils.l.a(this.i.getWebView(), "javascript:window.SDK_RESET_RENDER();window.SDK_TRIGGER_RENDER();");
            }
            catch (final Exception ex) {
                com.bytedance.sdk.component.utils.m.a("WebViewRender", "reuse webview load fail ");
                b.b.a.a.d.h.e.\u3007O8o08O().Oo08(this.i);
                this.d.a(102);
            }
        }
        else {
            final SSWebView f = this.f();
            f.f();
            this.h.\u3007\u3007808\u3007().c();
            f.c(this.c);
        }
    }
    
    public void a(final h g) {
        this.g = g;
    }
    
    @Override
    public void a(final n n) {
        if (n == null) {
            if (this.d != null) {
                this.d.a(105);
            }
            return;
        }
        final boolean \u30070000OOO = n.\u30070000OOO();
        final float n2 = (float)n.oO80();
        final float n3 = (float)n.\u3007080();
        if (n2 > 0.0f && n3 > 0.0f) {
            this.f = \u30070000OOO;
            if (Looper.myLooper() == Looper.getMainLooper()) {
                this.a(n, n2, n3);
            }
            else {
                new Handler(Looper.getMainLooper()).post((Runnable)new Runnable(this, n, n2, n3) {
                    final float OO;
                    final n o0;
                    final a \u300708O\u300700\u3007o;
                    final float \u3007OOo8\u30070;
                    
                    @Override
                    public void run() {
                        this.\u300708O\u300700\u3007o.a(this.o0, this.\u3007OOo8\u30070, this.OO);
                    }
                });
            }
            return;
        }
        if (this.d != null) {
            this.d.a(105);
        }
    }
    
    public void a(final String c) {
        this.c = c;
    }
    
    public void a(final JSONObject b) {
        this.b = b;
    }
    
    public void a(final boolean j) {
        this.j = j;
    }
    
    @Override
    public int b() {
        return 0;
    }
    
    public abstract void b(final int p0);
    
    public void c() {
    }
    
    public SSWebView d() {
        return this.f();
    }
    
    public abstract SSWebView f();
    
    public m g() {
        return this.h;
    }
    
    public void h() {
        this.j();
        final Activity a = com.bytedance.sdk.component.utils.b.a((View)this.i);
        if (a != null) {
            a.hashCode();
        }
    }
    
    public abstract void i();
    
    protected void j() {
    }
    
    public void k() {
        if (this.m.get()) {
            return;
        }
        this.m.set(true);
        this.i();
        if (((View)this.i).getParent() != null) {
            ((ViewGroup)((View)this.i).getParent()).removeView((View)this.i);
        }
        if (this.f) {
            b.b.a.a.d.h.e.\u3007O8o08O().\u300780\u3007808\u3007O(this.i);
        }
        else {
            b.b.a.a.d.h.e.\u3007O8o08O().Oo08(this.i);
        }
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package b.b.a.a.d.h;

import android.webkit.JavascriptInterface;
import java.lang.ref.WeakReference;

public class c
{
    private WeakReference<b> \u3007080;
    
    public c(final b referent) {
        this.\u3007080 = new WeakReference<b>(referent);
    }
    
    @JavascriptInterface
    public void adAnalysisData(final String s) {
        final WeakReference<b> \u3007080 = this.\u3007080;
        if (\u3007080 != null && \u3007080.get() != null) {
            this.\u3007080.get().b(s);
        }
    }
    
    @JavascriptInterface
    public String adInfo() {
        final WeakReference<b> \u3007080 = this.\u3007080;
        if (\u3007080 != null && \u3007080.get() != null) {
            return this.\u3007080.get().adInfo();
        }
        return "";
    }
    
    @JavascriptInterface
    public String appInfo() {
        final WeakReference<b> \u3007080 = this.\u3007080;
        if (\u3007080 != null && \u3007080.get() != null) {
            return this.\u3007080.get().appInfo();
        }
        return "";
    }
    
    @JavascriptInterface
    public void changeVideoState(final String s) {
        final WeakReference<b> \u3007080 = this.\u3007080;
        if (\u3007080 != null && \u3007080.get() != null) {
            this.\u3007080.get().changeVideoState(s);
        }
    }
    
    @JavascriptInterface
    public void chooseAdResult(final String s) {
        final WeakReference<b> \u3007080 = this.\u3007080;
        if (\u3007080 != null && \u3007080.get() != null) {
            this.\u3007080.get().chooseAdResult(s);
        }
    }
    
    @JavascriptInterface
    public void clickEvent(final String s) {
        final WeakReference<b> \u3007080 = this.\u3007080;
        if (\u3007080 != null && \u3007080.get() != null) {
            this.\u3007080.get().clickEvent(s);
        }
    }
    
    @JavascriptInterface
    public void dynamicTrack(final String s) {
        final WeakReference<b> \u3007080 = this.\u3007080;
        if (\u3007080 != null && \u3007080.get() != null) {
            this.\u3007080.get().dynamicTrack(s);
        }
    }
    
    @JavascriptInterface
    public String getCurrentVideoState() {
        final WeakReference<b> \u3007080 = this.\u3007080;
        if (\u3007080 != null && \u3007080.get() != null) {
            return this.\u3007080.get().getCurrentVideoState();
        }
        return "";
    }
    
    @JavascriptInterface
    public String getTemplateInfo() {
        final WeakReference<b> \u3007080 = this.\u3007080;
        if (\u3007080 != null && \u3007080.get() != null) {
            return this.\u3007080.get().getTemplateInfo();
        }
        return "";
    }
    
    @JavascriptInterface
    public void initRenderFinish() {
        final WeakReference<b> \u3007080 = this.\u3007080;
        if (\u3007080 != null && \u3007080.get() != null) {
            this.\u3007080.get().initRenderFinish();
        }
    }
    
    @JavascriptInterface
    public void muteVideo(final String s) {
        final WeakReference<b> \u3007080 = this.\u3007080;
        if (\u3007080 != null && \u3007080.get() != null) {
            this.\u3007080.get().muteVideo(s);
        }
    }
    
    @JavascriptInterface
    public void renderDidFinish(final String s) {
        final WeakReference<b> \u3007080 = this.\u3007080;
        if (\u3007080 != null && \u3007080.get() != null) {
            this.\u3007080.get().renderDidFinish(s);
        }
    }
    
    @JavascriptInterface
    public void requestPauseVideo(final String s) {
        final WeakReference<b> \u3007080 = this.\u3007080;
        if (\u3007080 != null && \u3007080.get() != null) {
            this.\u3007080.get().a(s);
        }
    }
    
    @JavascriptInterface
    public void skipVideo() {
        final WeakReference<b> \u3007080 = this.\u3007080;
        if (\u3007080 != null && \u3007080.get() != null) {
            this.\u3007080.get().skipVideo();
        }
    }
    
    public void \u3007080(final b referent) {
        this.\u3007080 = new WeakReference<b>(referent);
    }
}

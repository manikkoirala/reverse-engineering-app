// 
// Decompiled by Procyon v0.6.0
// 

package b.b.a.a.d.h;

public interface b
{
    void a(final String p0);
    
    String adInfo();
    
    String appInfo();
    
    void b(final String p0);
    
    void changeVideoState(final String p0);
    
    void chooseAdResult(final String p0);
    
    void clickEvent(final String p0);
    
    void dynamicTrack(final String p0);
    
    String getCurrentVideoState();
    
    String getTemplateInfo();
    
    void initRenderFinish();
    
    void muteVideo(final String p0);
    
    void renderDidFinish(final String p0);
    
    void skipVideo();
}

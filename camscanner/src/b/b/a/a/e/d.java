// 
// Decompiled by Procyon v0.6.0
// 

package b.b.a.a.e;

import com.bytedance.component.sdk.annotation.CallSuper;

public abstract class d<P, R> extends b.b.a.a.e.b<P, R>
{
    private boolean b;
    private a c;
    
    public d() {
        this.b = true;
    }
    
    private boolean c() {
        if (!this.b) {
            final StringBuilder sb = new StringBuilder();
            sb.append("Jsb async call already finished: ");
            sb.append(this.a());
            sb.append(", hashcode: ");
            sb.append(this.hashCode());
            i.\u3007080(new IllegalStateException(sb.toString()));
            return false;
        }
        return true;
    }
    
    protected abstract void a(final P p0, final f p1) throws Exception;
    
    void a(final P p3, final f f, final a c) throws Exception {
        this.c = c;
        this.a(p3, f);
    }
    
    protected final void a(final Throwable t) {
        if (this.c()) {
            this.c.a(t);
            this.e();
        }
    }
    
    protected final void d() {
        this.a((Throwable)null);
    }
    
    @CallSuper
    protected void e() {
        this.b = false;
    }
    
    protected abstract void f();
    
    void g() {
        this.f();
        this.e();
    }
    
    interface a
    {
        void a(final Throwable p0);
    }
    
    public interface b
    {
        d a();
    }
}

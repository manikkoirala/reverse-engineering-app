// 
// Decompiled by Procyon v0.6.0
// 

package b.b.a.a.e;

import java.util.Iterator;
import android.net.Uri;
import com.bytedance.component.sdk.annotation.MainThread;
import java.util.Collection;
import java.util.LinkedHashSet;
import java.util.Set;

class u
{
    private final Set<String> \u3007080;
    private final Set<String> \u3007o00\u3007\u3007Oo;
    
    u(final v v, final Set<String> c, final Set<String> c2) {
        if (c != null && !c.isEmpty()) {
            this.\u3007080 = new LinkedHashSet<String>(c);
        }
        else {
            this.\u3007080 = new LinkedHashSet<String>();
        }
        if (c2 != null && !c2.isEmpty()) {
            this.\u3007o00\u3007\u3007Oo = new LinkedHashSet<String>(c2);
        }
        else {
            this.\u3007o00\u3007\u3007Oo = new LinkedHashSet<String>();
        }
    }
    
    private x \u3007o00\u3007\u3007Oo(final String s, final b b, final boolean b2) {
        return null;
    }
    
    void O8(final k k) {
    }
    
    void Oo08(final w$a w$a) {
    }
    
    final x o\u30070(final String s, final b b) {
        synchronized (this) {
            return this.\u3007o00\u3007\u3007Oo(s, b, false);
        }
    }
    
    @MainThread
    final x \u3007080(final String s, final b b) throws v.a {
        synchronized (this) {
            return this.\u3007o00\u3007\u3007Oo(s, b, true);
        }
    }
    
    @MainThread
    final x \u3007o\u3007(final boolean b, final String s, final b b2) throws v.a {
        synchronized (this) {
            final Uri parse = Uri.parse(s);
            final String host = parse.getHost();
            x a = null;
            if (host == null) {
                return null;
            }
            if (this.\u3007o00\u3007\u3007Oo.contains(b2.a())) {
                a = x.a;
            }
            final Iterator<String> iterator = this.\u3007080.iterator();
            while (true) {
                StringBuilder sb;
                do {
                    final x c = a;
                    if (iterator.hasNext()) {
                        final String s2 = iterator.next();
                        if (parse.getHost().equals(s2)) {
                            break;
                        }
                        sb = new StringBuilder();
                        sb.append(".");
                        sb.append(s2);
                    }
                    else {
                        x x;
                        if (b) {
                            x = this.\u3007080(s, b2);
                        }
                        else {
                            x = this.o\u30070(s, b2);
                        }
                        monitorexit(this);
                        if (x != null) {
                            return x;
                        }
                        return c;
                    }
                } while (!host.endsWith(sb.toString()));
                final x c = x.c;
                continue;
            }
        }
    }
    
    void \u3007\u3007888(final w$a w$a) {
    }
}

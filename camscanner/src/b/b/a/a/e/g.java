// 
// Decompiled by Procyon v0.6.0
// 

package b.b.a.a.e;

import java.util.Iterator;
import org.json.JSONException;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import com.bytedance.component.sdk.annotation.MainThread;
import java.util.HashSet;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Set;
import java.util.List;
import java.util.Map;

class g implements w$a
{
    private final Map<String, d.b> O8;
    private final List<q> Oo08;
    private final boolean oO80;
    private final Set<d> o\u30070;
    private final h \u3007080;
    private final a \u300780\u3007808\u3007O;
    private final u \u3007o00\u3007\u3007Oo;
    private final Map<String, b> \u3007o\u3007;
    private final boolean \u3007\u3007888;
    
    g(final j j, final a \u300780\u3007808\u3007O, final v v) {
        this.\u3007o\u3007 = new HashMap<String, b>();
        this.O8 = new HashMap<String, d.b>();
        this.Oo08 = new ArrayList<q>();
        this.o\u30070 = new HashSet<d>();
        this.\u300780\u3007808\u3007O = \u300780\u3007808\u3007O;
        this.\u3007080 = j.O8;
        final u \u3007o00\u3007\u3007Oo = new u(v, j.\u30078o8o\u3007, j.\u3007O8o08O);
        (this.\u3007o00\u3007\u3007Oo = \u3007o00\u3007\u3007Oo).Oo08(this);
        \u3007o00\u3007\u3007Oo.O8(null);
        this.\u3007\u3007888 = j.oO80;
        this.oO80 = j.Oooo8o0\u3007;
    }
    
    @MainThread
    private c O8(final q q, final e e, final f f) throws Exception {
        return new c(true, y.\u3007o00\u3007\u3007Oo(this.\u3007080.\u3007o\u3007(e.a(this.\u30078o8o\u3007(q.Oo08, e), f))));
    }
    
    private static Type[] OO0o\u3007\u3007\u3007\u30070(final Object o) {
        final Type genericSuperclass = o.getClass().getGenericSuperclass();
        if (genericSuperclass != null) {
            return ((ParameterizedType)genericSuperclass).getActualTypeArguments();
        }
        throw new IllegalStateException("Method is not parameterized?!");
    }
    
    private x o\u30070(final String s, final b b) {
        if (this.oO80) {
            return x.c;
        }
        return this.\u3007o00\u3007\u3007Oo.\u3007o\u3007(this.\u3007\u3007888, s, b);
    }
    
    private Object \u30078o8o\u3007(final String s, final b b) throws JSONException {
        return this.\u3007080.\u3007o00\u3007\u3007Oo(s, OO0o\u3007\u3007\u3007\u30070(b)[0]);
    }
    
    @MainThread
    private c \u3007o00\u3007\u3007Oo(final q q, final b.b.a.a.e.c c, final x x) throws Exception {
        c.\u3007080(q, new t(q.O8, x, (t.a)new t.a(this, q) {}));
        return new c(false, y.\u3007080());
    }
    
    @MainThread
    private c \u3007o\u3007(final q q, final d d, final f f) throws Exception {
        this.o\u30070.add(d);
        d.a(this.\u30078o8o\u3007(q.Oo08, d), f, (d.a)new d.a(this, q, d) {
            final q \u3007080;
            final d \u3007o00\u3007\u3007Oo;
            final g \u3007o\u3007;
            
            @Override
            public void a(final Throwable t) {
                if (this.\u3007o\u3007.\u300780\u3007808\u3007O == null) {
                    return;
                }
                this.\u3007o\u3007.\u300780\u3007808\u3007O.a(y.\u3007o\u3007(t), this.\u3007080);
                this.\u3007o\u3007.o\u30070.remove(this.\u3007o00\u3007\u3007Oo);
            }
        });
        return new c(false, y.\u3007080());
    }
    
    @MainThread
    c Oo08(final q q, final f f) throws Exception {
        final b b = this.\u3007o\u3007.get(q.O8);
        if (b != null) {
            final x o\u30070 = this.o\u30070(f.\u3007o00\u3007\u3007Oo, b);
            if ((f.\u3007o\u3007 = o\u30070) == null) {
                final StringBuilder sb = new StringBuilder();
                sb.append("Permission denied, call: ");
                sb.append(q);
                i.\u3007o00\u3007\u3007Oo(sb.toString());
                throw new s(-1);
            }
            if (b instanceof e) {
                final StringBuilder sb2 = new StringBuilder();
                sb2.append("Processing stateless call: ");
                sb2.append(q);
                i.\u3007o00\u3007\u3007Oo(sb2.toString());
                return this.O8(q, (e)b, f);
            }
            if (b instanceof b.b.a.a.e.c) {
                final StringBuilder sb3 = new StringBuilder();
                sb3.append("Processing raw call: ");
                sb3.append(q);
                i.\u3007o00\u3007\u3007Oo(sb3.toString());
                return this.\u3007o00\u3007\u3007Oo(q, (b.b.a.a.e.c)b, o\u30070);
            }
        }
        final d.b b2 = this.O8.get(q.O8);
        if (b2 == null) {
            final StringBuilder sb4 = new StringBuilder();
            sb4.append("Received call: ");
            sb4.append(q);
            sb4.append(", but not registered.");
            i.Oo08(sb4.toString());
            return null;
        }
        final d a = b2.a();
        a.a(q.O8);
        if ((f.\u3007o\u3007 = this.o\u30070(f.\u3007o00\u3007\u3007Oo, a)) != null) {
            final StringBuilder sb5 = new StringBuilder();
            sb5.append("Processing stateful call: ");
            sb5.append(q);
            i.\u3007o00\u3007\u3007Oo(sb5.toString());
            return this.\u3007o\u3007(q, a, f);
        }
        final StringBuilder sb6 = new StringBuilder();
        sb6.append("Permission denied, call: ");
        sb6.append(q);
        i.\u3007o00\u3007\u3007Oo(sb6.toString());
        a.e();
        throw new s(-1);
    }
    
    void oO80(final String str, final d.b b) {
        this.O8.put(str, b);
        final StringBuilder sb = new StringBuilder();
        sb.append("JsBridge stateful method registered: ");
        sb.append(str);
        i.\u3007o00\u3007\u3007Oo(sb.toString());
    }
    
    void \u300780\u3007808\u3007O(final String str, final e<?, ?> e) {
        e.a(str);
        this.\u3007o\u3007.put(str, e);
        final StringBuilder sb = new StringBuilder();
        sb.append("JsBridge stateless method registered: ");
        sb.append(str);
        i.\u3007o00\u3007\u3007Oo(sb.toString());
    }
    
    void \u3007\u3007888() {
        final Iterator<d> iterator = (Iterator<d>)this.o\u30070.iterator();
        while (iterator.hasNext()) {
            iterator.next().g();
        }
        this.o\u30070.clear();
        this.\u3007o\u3007.clear();
        this.O8.clear();
        this.\u3007o00\u3007\u3007Oo.\u3007\u3007888(this);
    }
    
    static final class c
    {
        boolean \u3007080;
        String \u3007o00\u3007\u3007Oo;
        
        private c(final boolean \u3007080, final String \u3007o00\u3007\u3007Oo) {
            this.\u3007080 = \u3007080;
            this.\u3007o00\u3007\u3007Oo = \u3007o00\u3007\u3007Oo;
        }
    }
}

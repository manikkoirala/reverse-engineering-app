// 
// Decompiled by Procyon v0.6.0
// 

package b.b.a.a.e;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import java.lang.reflect.Type;

class h
{
    private l \u3007080;
    
    private h(final l \u3007080) {
        this.\u3007080 = \u3007080;
    }
    
    private static void O8(final String str) {
        if (!str.startsWith("{") || !str.endsWith("}")) {
            final StringBuilder sb = new StringBuilder();
            sb.append("Param is not allowed to be List or JSONArray, rawString:\n ");
            sb.append(str);
            i.\u3007080(new IllegalArgumentException(sb.toString()));
        }
    }
    
    static h \u3007080(final l l) {
        return new h(l);
    }
    
     <T> T \u3007o00\u3007\u3007Oo(final String s, final Type type) throws JSONException {
        O8(s);
        if (!type.equals(JSONObject.class) && (!(type instanceof Class) || !JSONObject.class.isAssignableFrom((Class<?>)type))) {
            return this.\u3007080.a(s, type);
        }
        return (T)new JSONObject(s);
    }
    
     <T> String \u3007o\u3007(final T t) {
        if (t == null) {
            return "{}";
        }
        String s;
        if (!(t instanceof JSONObject) && !(t instanceof JSONArray)) {
            s = this.\u3007080.a(t);
        }
        else {
            s = t.toString();
        }
        O8(s);
        return s;
    }
}

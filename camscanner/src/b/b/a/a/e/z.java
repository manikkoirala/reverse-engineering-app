// 
// Decompiled by Procyon v0.6.0
// 

package b.b.a.a.e;

import android.view.View;
import android.webkit.JavascriptInterface;
import android.util.Base64;
import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Looper;
import android.webkit.ValueCallback;
import android.text.TextUtils;
import android.webkit.WebView;

public class z extends a
{
    static final boolean j = true;
    protected String h;
    protected WebView i;
    
    private void a(final String s, final String str) {
        if (super.e) {
            return;
        }
        if (TextUtils.isEmpty((CharSequence)str)) {
            return;
        }
        final Runnable runnable = new Runnable(this, str) {
            final String o0;
            final z \u3007OOo8\u30070;
            
            @Override
            public void run() {
                if (this.\u3007OOo8\u30070.e) {
                    return;
                }
                try {
                    final StringBuilder sb = new StringBuilder();
                    sb.append("Invoking Jsb using evaluateJavascript: ");
                    sb.append(this.o0);
                    b.b.a.a.e.i.\u3007o00\u3007\u3007Oo(sb.toString());
                    this.\u3007OOo8\u30070.i.evaluateJavascript(this.o0, (ValueCallback)null);
                }
                finally {
                    final Throwable t;
                    t.printStackTrace();
                }
            }
        };
        if (Looper.myLooper() != Looper.getMainLooper()) {
            final StringBuilder sb = new StringBuilder();
            sb.append("Received call on sub-thread, posting to main thread: ");
            sb.append(str);
            b.b.a.a.e.i.\u3007o00\u3007\u3007Oo(sb.toString());
            super.c.post((Runnable)runnable);
        }
        else {
            runnable.run();
        }
    }
    
    @Override
    protected Context a(final j j) {
        final Context oo08 = j.Oo08;
        if (oo08 != null) {
            return oo08;
        }
        final WebView \u3007080 = j.\u3007080;
        if (\u3007080 != null) {
            return ((View)\u3007080).getContext();
        }
        throw new IllegalStateException("WebView cannot be null!");
    }
    
    @Override
    protected String a() {
        return this.i.getUrl();
    }
    
    @Override
    protected void b() {
        super.b();
        this.d();
    }
    
    @SuppressLint({ "JavascriptInterface", "AddJavascriptInterface" })
    @Override
    protected void b(final j j) {
        this.i = j.\u3007080;
        this.h = j.\u3007o\u3007;
        if (!j.OO0o\u3007\u3007) {
            this.c();
        }
    }
    
    @Override
    protected void b(final String str) {
        final StringBuilder sb = new StringBuilder();
        sb.append("javascript:");
        sb.append(this.h);
        sb.append("._handleMessageFromToutiao(");
        sb.append(str);
        sb.append(")");
        this.a(str, sb.toString());
    }
    
    @Override
    protected void b(final String s, final q q) {
        if (q != null && !TextUtils.isEmpty((CharSequence)q.oO80)) {
            final String oo80 = q.oO80;
            this.a(s, String.format("javascript:(function(){   const iframe = document.querySelector(atob('%s'));   if (iframe && iframe.contentWindow) {        iframe.contentWindow.postMessage(%s, atob('%s'));   }})()", Base64.encodeToString(String.format("iframe[src=\"%s\"", oo80).getBytes(), 2), s, Base64.encodeToString(oo80.getBytes(), 2)));
        }
        else {
            super.b(s, q);
        }
    }
    
    @SuppressLint({ "AddJavascriptInterface" })
    protected void c() {
        if (!z.j && this.i == null) {
            throw new AssertionError();
        }
        this.i.addJavascriptInterface((Object)this, this.h);
    }
    
    protected void d() {
        this.i.removeJavascriptInterface(this.h);
    }
    
    @JavascriptInterface
    public void invokeMethod(final String s) {
        super.invokeMethod(s);
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package b.b.a.a.e;

import java.util.Iterator;
import com.bytedance.component.sdk.annotation.UiThread;
import android.webkit.WebView;
import java.util.ArrayList;
import java.util.List;

public class r
{
    private final a \u3007080;
    private final List<n> \u3007o00\u3007\u3007Oo;
    private volatile boolean \u3007o\u3007;
    
    r(final j j) {
        final ArrayList \u3007o00\u3007\u3007Oo = new ArrayList();
        this.\u3007o00\u3007\u3007Oo = \u3007o00\u3007\u3007Oo;
        this.\u3007o\u3007 = false;
        final boolean oo80 = j.oO80;
        if (j.\u3007080 != null) {
            final a \u3007o00\u3007\u3007Oo2 = j.\u3007o00\u3007\u3007Oo;
            if (\u3007o00\u3007\u3007Oo2 == null) {
                this.\u3007080 = new z();
            }
            else {
                this.\u3007080 = \u3007o00\u3007\u3007Oo2;
            }
        }
        else {
            this.\u3007080 = j.\u3007o00\u3007\u3007Oo;
        }
        this.\u3007080.a(j, null);
        \u3007o00\u3007\u3007Oo.add(j.\u300780\u3007808\u3007O);
        i.O8(j.o\u30070);
        y.O8(j.\u3007\u3007888);
    }
    
    private void o\u30070() {
        if (this.\u3007o\u3007) {
            i.\u3007080(new IllegalStateException("JsBridge2 is already released!!!"));
        }
    }
    
    public static j \u3007080(final WebView webView) {
        return new j(webView);
    }
    
    @UiThread
    public r O8(final String s, final String s2, final d.b b) {
        this.o\u30070();
        this.\u3007080.f.oO80(s, b);
        return this;
    }
    
    @UiThread
    public r Oo08(final String s, final String s2, final e<?, ?> e) {
        this.o\u30070();
        this.\u3007080.f.\u300780\u3007808\u3007O(s, e);
        return this;
    }
    
    public r \u3007o00\u3007\u3007Oo(final String s, final d.b b) {
        return this.O8(s, null, b);
    }
    
    public r \u3007o\u3007(final String s, final e<?, ?> e) {
        return this.Oo08(s, null, e);
    }
    
    public void \u3007\u3007888() {
        if (this.\u3007o\u3007) {
            return;
        }
        this.\u3007080.b();
        this.\u3007o\u3007 = true;
        for (final n n : this.\u3007o00\u3007\u3007Oo) {
            if (n != null) {
                n.a();
            }
        }
    }
}

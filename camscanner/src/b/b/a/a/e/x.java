// 
// Decompiled by Procyon v0.6.0
// 

package b.b.a.a.e;

public enum x
{
    a, 
    b, 
    c;
    
    private static final x[] d;
    
    @Override
    public String toString() {
        if (this == x.c) {
            return "private";
        }
        if (this == x.b) {
            return "protected";
        }
        return "public";
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package b.b.a.a.e;

import com.bytedance.component.sdk.annotation.AnyThread;
import java.util.Iterator;
import com.bytedance.component.sdk.annotation.MainThread;
import org.json.JSONException;
import org.json.JSONObject;
import android.text.TextUtils;
import java.util.HashMap;
import android.os.Looper;
import java.util.Map;
import android.os.Handler;
import android.content.Context;

public abstract class a
{
    protected Context a;
    protected m b;
    protected Handler c;
    protected String d;
    protected volatile boolean e;
    g f;
    private final Map<String, g> g;
    
    protected a() {
        this.c = new Handler(Looper.getMainLooper());
        this.e = false;
        this.g = new HashMap<String, g>();
    }
    
    private g a(final String s) {
        g f;
        if (!TextUtils.equals((CharSequence)s, (CharSequence)this.d) && !TextUtils.isEmpty((CharSequence)s)) {
            f = this.g.get(s);
        }
        else {
            f = this.f;
        }
        return f;
    }
    
    private q a(final JSONObject jsonObject) {
        if (this.e) {
            return null;
        }
        final String optString = jsonObject.optString("__callback_id");
        final String optString2 = jsonObject.optString("func");
        if (this.a() == null) {
            return null;
        }
        try {
            final String string = jsonObject.getString("__msg_type");
            String optString3 = null;
            try {
                final Object opt = jsonObject.opt("params");
                if (opt != null) {
                    if (opt instanceof JSONObject) {
                        String.valueOf(opt);
                    }
                    else if (opt instanceof String) {
                        final String s = (String)opt;
                    }
                    else {
                        String.valueOf(opt);
                    }
                }
            }
            finally {
                optString3 = jsonObject.optString("params");
            }
            return q.\u3007080().Oooo8o0\u3007(jsonObject.getString("JSSDK")).\u3007O8o08O(string).o\u30070(optString2).OO0o\u3007\u3007\u3007\u30070(optString3).\u3007080(optString).oO80(jsonObject.optString("namespace")).O8(jsonObject.optString("__iframe_url")).\u3007o00\u3007\u3007Oo();
        }
        catch (final JSONException ex) {
            i.\u3007o\u3007("Failed to create call.", (Throwable)ex);
            return q.\u3007o00\u3007\u3007Oo(optString, -1);
        }
    }
    
    protected abstract Context a(final j p0);
    
    protected abstract String a();
    
    final void a(final j j, final v v) {
        this.a = this.a(j);
        final h o8 = j.O8;
        this.f = new g(j, this, v);
        this.d = j.OO0o\u3007\u3007\u3007\u30070;
        this.b(j);
    }
    
    @MainThread
    protected final void a(final q obj) {
        if (this.e) {
            return;
        }
        final String a = this.a();
        if (a == null) {
            return;
        }
        final g a2 = this.a(obj.\u3007\u3007888);
        if (a2 == null) {
            final StringBuilder sb = new StringBuilder();
            sb.append("Received call with unknown namespace, ");
            sb.append(obj);
            i.Oo08(sb.toString());
            final StringBuilder sb2 = new StringBuilder();
            sb2.append("Namespace ");
            sb2.append(obj.\u3007\u3007888);
            sb2.append(" unknown.");
            this.a(y.\u3007o\u3007(new s(-4, sb2.toString())), obj);
            return;
        }
        final f f = new f();
        f.\u3007o00\u3007\u3007Oo = a;
        f.\u3007080 = this.a;
        try {
            final g.c oo08 = a2.Oo08(obj, f);
            if (oo08 == null) {
                final StringBuilder sb3 = new StringBuilder();
                sb3.append("Received call but not registered, ");
                sb3.append(obj);
                i.Oo08(sb3.toString());
                final StringBuilder sb4 = new StringBuilder();
                sb4.append("Function ");
                sb4.append(obj.O8);
                sb4.append(" is not registered.");
                this.a(y.\u3007o\u3007(new s(-2, sb4.toString())), obj);
                return;
            }
            if (oo08.\u3007080) {
                this.a(oo08.\u3007o00\u3007\u3007Oo, obj);
            }
        }
        catch (final Exception ex) {
            final StringBuilder sb5 = new StringBuilder();
            sb5.append("call finished with error, ");
            sb5.append(obj);
            i.o\u30070(sb5.toString(), ex);
            this.a(y.\u3007o\u3007(ex), obj);
        }
    }
    
    final void a(final String s, final q q) {
        if (this.e) {
            return;
        }
        if (TextUtils.isEmpty((CharSequence)q.o\u30070)) {
            final StringBuilder sb = new StringBuilder();
            sb.append("By passing js callback due to empty callback: ");
            sb.append(s);
            i.\u3007o00\u3007\u3007Oo(sb.toString());
            return;
        }
        if (!s.startsWith("{") || !s.endsWith("}")) {
            final StringBuilder sb2 = new StringBuilder();
            sb2.append("Illegal callback data: ");
            sb2.append(s);
            i.\u3007080(new IllegalArgumentException(sb2.toString()));
        }
        final StringBuilder sb3 = new StringBuilder();
        sb3.append("Invoking js callback: ");
        sb3.append(q.o\u30070);
        i.\u3007o00\u3007\u3007Oo(sb3.toString());
        JSONObject jsonObject;
        try {
            jsonObject = new JSONObject(s);
        }
        catch (final Exception ex) {
            jsonObject = new JSONObject();
        }
        this.b(p.\u3007080().\u3007o00\u3007\u3007Oo("__msg_type", "callback").\u3007o00\u3007\u3007Oo("__callback_id", q.o\u30070).\u3007o00\u3007\u3007Oo("__params", jsonObject).\u3007o\u3007(), q);
    }
    
    protected void b() {
        this.f.\u3007\u3007888();
        final Iterator<g> iterator = this.g.values().iterator();
        while (iterator.hasNext()) {
            iterator.next().\u3007\u3007888();
        }
        this.c.removeCallbacksAndMessages((Object)null);
        this.e = true;
    }
    
    protected abstract void b(final j p0);
    
    @AnyThread
    protected abstract void b(final String p0);
    
    protected void b(final String s, final q q) {
        this.b(s);
    }
    
    protected void invokeMethod(final String str) {
        if (this.e) {
            return;
        }
        final StringBuilder sb = new StringBuilder();
        sb.append("Received call: ");
        sb.append(str);
        i.\u3007o00\u3007\u3007Oo(sb.toString());
        this.c.post((Runnable)new Runnable(this, str) {
            final String o0;
            final a \u3007OOo8\u30070;
            
            @Override
            public void run() {
                if (this.\u3007OOo8\u30070.e) {
                    return;
                }
                q a;
                try {
                    a = this.\u3007OOo8\u30070.a(new JSONObject(this.o0));
                }
                catch (final JSONException ex) {
                    i.\u3007o\u3007("Exception thrown while parsing function.", (Throwable)ex);
                    a = null;
                }
                if (q.\u3007o\u3007(a)) {
                    final StringBuilder sb = new StringBuilder();
                    sb.append("By pass invalid call: ");
                    sb.append(a);
                    i.\u3007o00\u3007\u3007Oo(sb.toString());
                    if (a != null) {
                        this.\u3007OOo8\u30070.a(y.\u3007o\u3007(new s(a.\u3007080, "Failed to parse invocation.")), a);
                    }
                    return;
                }
                this.\u3007OOo8\u30070.a(a);
            }
        });
    }
}

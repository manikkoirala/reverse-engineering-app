// 
// Decompiled by Procyon v0.6.0
// 

package b.b.a.a.e;

import java.util.Iterator;
import org.json.JSONException;
import org.json.JSONObject;
import android.text.TextUtils;
import java.util.concurrent.ConcurrentHashMap;
import java.util.Map;

public final class p
{
    private final Map<String, Object> \u3007080;
    
    private p() {
        this.\u3007080 = new ConcurrentHashMap<String, Object>();
    }
    
    public static p \u3007080() {
        return new p();
    }
    
    public p \u3007o00\u3007\u3007Oo(final String s, final Object o) {
        if (!TextUtils.isEmpty((CharSequence)s)) {
            if (o != null) {
                this.\u3007080.put(s, o);
            }
        }
        return this;
    }
    
    public String \u3007o\u3007() {
        final JSONObject jsonObject = new JSONObject();
        try {
            for (final Map.Entry<String, V> entry : this.\u3007080.entrySet()) {
                jsonObject.put((String)entry.getKey(), (Object)entry.getValue());
            }
            return jsonObject.toString();
        }
        catch (final JSONException ex) {
            return "";
        }
    }
}

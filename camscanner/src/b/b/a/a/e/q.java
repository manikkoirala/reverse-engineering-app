// 
// Decompiled by Procyon v0.6.0
// 

package b.b.a.a.e;

import android.text.TextUtils;

public class q
{
    public final String O8;
    public final String Oo08;
    public final String oO80;
    public final String o\u30070;
    public final int \u3007080;
    public final String \u3007o00\u3007\u3007Oo;
    public final String \u3007o\u3007;
    public final String \u3007\u3007888;
    
    private q(final b b) {
        this.\u3007o00\u3007\u3007Oo = b.\u3007080;
        this.\u3007o\u3007 = b.\u3007o00\u3007\u3007Oo;
        this.O8 = b.\u3007o\u3007;
        this.Oo08 = b.O8;
        this.o\u30070 = b.Oo08;
        this.\u3007\u3007888 = b.o\u30070;
        this.\u3007080 = 1;
        this.oO80 = b.\u3007\u3007888;
    }
    
    private q(final String o\u30070, final int \u3007080) {
        this.\u3007o00\u3007\u3007Oo = null;
        this.\u3007o\u3007 = null;
        this.O8 = null;
        this.Oo08 = null;
        this.o\u30070 = o\u30070;
        this.\u3007\u3007888 = null;
        this.\u3007080 = \u3007080;
        this.oO80 = null;
    }
    
    public static b \u3007080() {
        return new b();
    }
    
    public static q \u3007o00\u3007\u3007Oo(final String s, final int n) {
        return new q(s, n);
    }
    
    public static boolean \u3007o\u3007(final q q) {
        boolean b2;
        final boolean b = b2 = true;
        if (q != null) {
            b2 = b;
            if (q.\u3007080 == 1) {
                b2 = b;
                if (!TextUtils.isEmpty((CharSequence)q.O8)) {
                    b2 = (TextUtils.isEmpty((CharSequence)q.Oo08) && b);
                }
            }
        }
        return b2;
    }
    
    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append("methodName: ");
        sb.append(this.O8);
        sb.append(", params: ");
        sb.append(this.Oo08);
        sb.append(", callbackId: ");
        sb.append(this.o\u30070);
        sb.append(", type: ");
        sb.append(this.\u3007o\u3007);
        sb.append(", version: ");
        sb.append(this.\u3007o00\u3007\u3007Oo);
        sb.append(", ");
        return sb.toString();
    }
    
    public static final class b
    {
        private String O8;
        private String Oo08;
        private String o\u30070;
        private String \u3007080;
        private String \u3007o00\u3007\u3007Oo;
        private String \u3007o\u3007;
        private String \u3007\u3007888;
        
        private b() {
        }
        
        public b O8(final String \u3007\u3007888) {
            this.\u3007\u3007888 = \u3007\u3007888;
            return this;
        }
        
        public b OO0o\u3007\u3007\u3007\u30070(final String o8) {
            this.O8 = o8;
            return this;
        }
        
        public b Oooo8o0\u3007(final String \u3007080) {
            this.\u3007080 = \u3007080;
            return this;
        }
        
        public b oO80(final String o\u30070) {
            this.o\u30070 = o\u30070;
            return this;
        }
        
        public b o\u30070(final String \u3007o\u3007) {
            this.\u3007o\u3007 = \u3007o\u3007;
            return this;
        }
        
        public b \u3007080(final String oo08) {
            this.Oo08 = oo08;
            return this;
        }
        
        public b \u3007O8o08O(final String \u3007o00\u3007\u3007Oo) {
            this.\u3007o00\u3007\u3007Oo = \u3007o00\u3007\u3007Oo;
            return this;
        }
        
        public q \u3007o00\u3007\u3007Oo() {
            return new q(this, null);
        }
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package b.b.a.a;

import java.util.Iterator;
import android.os.Message;
import android.os.Looper;
import android.os.HandlerThread;
import java.util.LinkedList;
import androidx.annotation.GuardedBy;
import android.os.Handler;

public class c
{
    private static Object O8;
    private static final Object \u3007080;
    @GuardedBy("sLock")
    private static Handler \u3007o00\u3007\u3007Oo;
    @GuardedBy("sLock")
    private static final LinkedList<Runnable> \u3007o\u3007;
    
    static {
        \u3007080 = new Object();
        \u3007o\u3007 = new LinkedList<Runnable>();
        c.O8 = new Object();
    }
    
    private static void O8() {
        synchronized (c.O8) {
            Object o = c.\u3007080;
            synchronized (o) {
                final LinkedList<Runnable> \u3007o\u3007 = c.\u3007o\u3007;
                final LinkedList list = (LinkedList)\u3007o\u3007.clone();
                \u3007o\u3007.clear();
                \u3007o\u3007().removeMessages(1);
                monitorexit(o);
                if (list.size() > 0) {
                    o = list.iterator();
                    while (((Iterator)o).hasNext()) {
                        ((Runnable)((Iterator)o).next()).run();
                    }
                }
            }
        }
    }
    
    public static void \u3007o00\u3007\u3007Oo(final Runnable e, final boolean b) {
        final Handler \u3007o\u3007 = \u3007o\u3007();
        synchronized (c.\u3007080) {
            c.\u3007o\u3007.add(e);
            if (b) {
                \u3007o\u3007.sendEmptyMessageDelayed(1, 100L);
            }
            else {
                \u3007o\u3007.sendEmptyMessage(1);
            }
        }
    }
    
    private static Handler \u3007o\u3007() {
        synchronized (c.\u3007080) {
            if (c.\u3007o00\u3007\u3007Oo == null) {
                final HandlerThread handlerThread = new HandlerThread("queued-work-looper", -2);
                ((Thread)handlerThread).start();
                c.\u3007o00\u3007\u3007Oo = new a(handlerThread.getLooper());
            }
            return c.\u3007o00\u3007\u3007Oo;
        }
    }
    
    private static class a extends Handler
    {
        a(final Looper looper) {
            super(looper);
        }
        
        public void handleMessage(final Message message) {
            if (message.what == 1) {
                O8();
            }
        }
    }
}

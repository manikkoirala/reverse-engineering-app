// 
// Decompiled by Procyon v0.6.0
// 

package b.a.a.a.a.a.b.d;

import java.net.ServerSocket;
import b.b.a.a.k.g;
import java.net.Socket;
import java.util.Collection;
import java.io.RandomAccessFile;
import java.util.Iterator;
import java.util.Set;
import java.util.ArrayList;
import b.a.a.a.a.a.b.c.i;
import java.util.List;
import java.util.Map;
import java.io.Closeable;
import b.a.a.a.a.a.b.c.p.e;
import org.json.JSONObject;
import android.text.TextUtils;
import b.a.a.a.a.a.b.c.m.c;
import android.os.Looper;
import java.nio.charset.Charset;
import android.os.Handler;

public final class a
{
    private static final Handler \u3007080;
    public static final Charset \u3007o00\u3007\u3007Oo;
    
    static {
        \u3007080 = new Handler(Looper.getMainLooper());
        \u3007o00\u3007\u3007Oo = Charset.forName("UTF-8");
    }
    
    public static b.a.a.a.a.a.b.c.m.a O8(final b.a.a.a.a.a.b.c.p.a a, final c c, final String s, final int n) {
        b.a.a.a.a.a.b.c.m.a \u3007o\u3007;
        final b.a.a.a.a.a.b.c.m.a a2 = \u3007o\u3007 = c.\u3007o\u3007(s, n);
        if (a2 == null) {
            final int \u3007o00\u3007\u3007Oo = \u3007o00\u3007\u3007Oo(a);
            final String o8 = a.O8("Content-Type", null);
            \u3007o\u3007 = a2;
            if (\u3007o00\u3007\u3007Oo > 0) {
                \u3007o\u3007 = a2;
                if (!TextUtils.isEmpty((CharSequence)o8)) {
                    final e o\u30070 = a.o\u30070();
                    String \u3007080;
                    String o800o8O;
                    if (o\u30070 != null) {
                        \u3007080 = o\u30070.\u3007080;
                        o800o8O = o800o8O(o\u30070.\u3007o00\u3007\u3007Oo);
                    }
                    else {
                        o800o8O = "";
                        \u3007080 = "";
                    }
                    final String ooO8 = OoO8(a.\u3007\u3007888());
                    String s2 = null;
                    try {
                        final JSONObject jsonObject = new JSONObject();
                        jsonObject.put("requestUrl", (Object)\u3007080);
                        jsonObject.put("requestHeaders", (Object)o800o8O);
                        jsonObject.put("responseHeaders", (Object)ooO8);
                        jsonObject.toString();
                    }
                    finally {
                        s2 = "";
                    }
                    \u3007o\u3007 = new b.a.a.a.a.a.b.c.m.a(s, o8, \u3007o00\u3007\u3007Oo, n, s2);
                    c.o\u30070(\u3007o\u3007);
                }
            }
        }
        return \u3007o\u3007;
    }
    
    public static void OO0o\u3007\u3007(final Closeable closeable) {
        if (closeable == null) {
            return;
        }
        try {
            closeable.close();
        }
        finally {}
    }
    
    public static List<i.b> OO0o\u3007\u3007\u3007\u30070(final Map<String, String> map) {
        if (map != null) {
            if (!map.isEmpty()) {
                try {
                    final Set entrySet = map.entrySet();
                    final ArrayList list = new ArrayList();
                    for (final Map.Entry entry : entrySet) {
                        list.add(new i.b((String)entry.getKey(), (String)entry.getValue()));
                    }
                    return list;
                }
                finally {
                    final Throwable t;
                    t.printStackTrace();
                }
            }
        }
        return null;
    }
    
    public static String Oo08(final int n, final int n2) {
        final String \u30070\u3007O0088o = \u30070\u3007O0088o(n, n2);
        String string;
        if (\u30070\u3007O0088o == null) {
            string = null;
        }
        else {
            final StringBuilder sb = new StringBuilder();
            sb.append("bytes=");
            sb.append(\u30070\u3007O0088o);
            string = sb.toString();
        }
        return string;
    }
    
    public static String OoO8(final List<i.b> list) {
        if (list != null && list.size() != 0) {
            final StringBuilder sb = new StringBuilder();
            for (int size = list.size(), i = 0; i < size; ++i) {
                final i.b b = list.get(0);
                if (b != null) {
                    sb.append(b.\u3007080);
                    sb.append(": ");
                    sb.append(b.\u3007o00\u3007\u3007Oo);
                    sb.append("\r\n");
                }
            }
            return sb.toString();
        }
        return "";
    }
    
    public static void Oooo8o0\u3007(final RandomAccessFile randomAccessFile) {
        if (randomAccessFile == null) {
            return;
        }
        try {
            randomAccessFile.getFD().sync();
            randomAccessFile.close();
        }
        finally {}
    }
    
    public static String o800o8O(final Map<String, String> map) {
        if (map != null && map.size() != 0) {
            final StringBuilder sb = new StringBuilder();
            for (final Map.Entry<Object, V> entry : map.entrySet()) {
                sb.append(entry.getKey());
                sb.append(": ");
                sb.append(entry.getValue());
                sb.append("\r\n");
            }
            return sb.toString();
        }
        return "";
    }
    
    public static String oO80(final b.a.a.a.a.a.b.c.p.a a, final boolean b, final boolean b2) {
        if (a == null) {
            final boolean \u3007o\u3007 = b.a.a.a.a.a.b.c.e.\u3007o\u3007;
            return "response null";
        }
        if (!a.oO80()) {
            if (b.a.a.a.a.a.b.c.e.\u3007o\u3007) {
                final StringBuilder sb = new StringBuilder();
                sb.append("response code: ");
                sb.append(a.\u3007080());
            }
            final StringBuilder sb2 = new StringBuilder();
            sb2.append("response code: ");
            sb2.append(a.\u3007080());
            return sb2.toString();
        }
        final String o8 = a.O8("Content-Type", null);
        if (!oo88o8O(o8)) {
            if (b.a.a.a.a.a.b.c.e.\u3007o\u3007) {
                final StringBuilder sb3 = new StringBuilder();
                sb3.append("Content-Type: ");
                sb3.append(o8);
            }
            final StringBuilder sb4 = new StringBuilder();
            sb4.append("Content-Type: ");
            sb4.append(o8);
            return sb4.toString();
        }
        final int \u3007o00\u3007\u3007Oo = \u3007o00\u3007\u3007Oo(a);
        if (\u3007o00\u3007\u3007Oo <= 0) {
            if (b.a.a.a.a.a.b.c.e.\u3007o\u3007) {
                final StringBuilder sb5 = new StringBuilder();
                sb5.append("Content-Length: ");
                sb5.append(\u3007o00\u3007\u3007Oo);
            }
            final StringBuilder sb6 = new StringBuilder();
            sb6.append("Content-Length: ");
            sb6.append(\u3007o00\u3007\u3007Oo);
            return sb6.toString();
        }
        if (b) {
            final String o9 = a.O8("Accept-Ranges", null);
            if (o9 == null || !o9.contains("bytes")) {
                if (b.a.a.a.a.a.b.c.e.\u3007o\u3007) {
                    final StringBuilder sb7 = new StringBuilder();
                    sb7.append("Accept-Ranges: ");
                    sb7.append(o9);
                }
                final StringBuilder sb8 = new StringBuilder();
                sb8.append("Accept-Ranges: ");
                sb8.append(o9);
                return sb8.toString();
            }
        }
        if (b2 && a.Oo08() == null) {
            final boolean \u3007o\u30072 = b.a.a.a.a.a.b.c.e.\u3007o\u3007;
            return "response body null";
        }
        return null;
    }
    
    public static boolean oo88o8O(final String s) {
        return s != null && (s.startsWith("video/") || "application/octet-stream".equals(s) || "binary/octet-stream".equals(s));
    }
    
    public static String o\u30070(final b.a.a.a.a.a.b.c.m.a a, final int i) {
        final StringBuilder sb = new StringBuilder();
        if (i <= 0) {
            sb.append("HTTP/1.1 200 OK");
            sb.append("\r\n");
        }
        else {
            sb.append("HTTP/1.1 206 Partial Content");
            sb.append("\r\n");
        }
        sb.append("Accept-Ranges: bytes");
        sb.append("\r\n");
        sb.append("Content-Type: ");
        sb.append(a.\u3007o00\u3007\u3007Oo);
        sb.append("\r\n");
        if (i <= 0) {
            sb.append("Content-Length: ");
            sb.append(a.\u3007o\u3007);
            sb.append("\r\n");
        }
        else {
            sb.append("Content-Range: bytes ");
            sb.append(i);
            sb.append("-");
            sb.append(a.\u3007o\u3007 - 1);
            sb.append("/");
            sb.append(a.\u3007o\u3007);
            sb.append("\r\n");
            sb.append("Content-Length: ");
            sb.append(a.\u3007o\u3007 - i);
            sb.append("\r\n");
        }
        sb.append("Connection: close");
        sb.append("\r\n");
        sb.append("\r\n");
        final String string = sb.toString();
        final boolean \u3007o\u3007 = b.a.a.a.a.a.b.c.e.\u3007o\u3007;
        return string;
    }
    
    public static int \u3007080() {
        return Math.max(Runtime.getRuntime().availableProcessors(), 1);
    }
    
    public static String \u30070\u3007O0088o(final int n, final int n2) {
        if (n >= 0 && n2 > 0) {
            final StringBuilder sb = new StringBuilder();
            sb.append(n);
            sb.append("-");
            sb.append(n2);
            return sb.toString();
        }
        if (n > 0) {
            final StringBuilder sb2 = new StringBuilder();
            sb2.append(n);
            sb2.append("-");
            return sb2.toString();
        }
        if (n < 0 && n2 > 0) {
            final StringBuilder sb3 = new StringBuilder();
            sb3.append("-");
            sb3.append(n2);
            return sb3.toString();
        }
        return null;
    }
    
    public static List<i.b> \u300780\u3007808\u3007O(final List<i.b> list) {
        if (list != null && list.size() != 0) {
            final boolean \u3007o\u3007 = b.a.a.a.a.a.b.c.e.\u3007o\u3007;
            final int n = 0;
            if (\u3007o\u3007) {
                for (int size = list.size(), i = 0; i < size; ++i) {
                    final i.b b = list.get(i);
                    if (b != null) {
                        final StringBuilder sb = new StringBuilder();
                        sb.append(b.\u3007080);
                        sb.append(": ");
                        sb.append(b.\u3007080);
                    }
                }
            }
            final ArrayList list2 = new ArrayList();
            for (final i.b b2 : list) {
                if ("Host".equals(b2.\u3007080) || "Keep-Alive".equals(b2.\u3007080) || "Connection".equals(b2.\u3007080) || "Proxy-Connection".equals(b2.\u3007080)) {
                    list2.add(b2);
                }
            }
            list.removeAll(list2);
            if (b.a.a.a.a.a.b.c.e.\u3007o\u3007) {
                for (int size2 = list.size(), j = n; j < size2; ++j) {
                    final i.b b3 = list.get(j);
                    if (b3 != null) {
                        final StringBuilder sb2 = new StringBuilder();
                        sb2.append(b3.\u3007080);
                        sb2.append(": ");
                        sb2.append(b3.\u3007o00\u3007\u3007Oo);
                    }
                }
            }
            return list;
        }
        return null;
    }
    
    public static List<String> \u30078o8o\u3007(final String... array) {
        ArrayList<String> list2;
        final ArrayList<String> list = list2 = null;
        if (array != null) {
            if (array.length == 0) {
                list2 = list;
            }
            else {
                list2 = new ArrayList<String>(array.length);
                for (final String e : array) {
                    if (\u3007\u30078O0\u30078(e)) {
                        list2.add(e);
                    }
                }
                if (list2.isEmpty()) {
                    list2 = list;
                }
            }
        }
        return list2;
    }
    
    public static void \u3007O00(final Socket socket) {
        if (socket == null) {
            return;
        }
        try {
            socket.close();
        }
        finally {}
    }
    
    public static boolean \u3007O888o0o() {
        return Thread.currentThread() == Looper.getMainLooper().getThread();
    }
    
    public static void \u3007O8o08O(final g g) {
        if (g != null) {
            if (\u3007O888o0o()) {
                b.b.a.a.k.e.Oooo8o0\u3007(g);
                final boolean \u3007o\u3007 = b.a.a.a.a.a.b.c.e.\u3007o\u3007;
            }
            else {
                g.run();
                final boolean \u3007o\u30072 = b.a.a.a.a.a.b.c.e.\u3007o\u3007;
            }
        }
    }
    
    public static void \u3007O\u3007(final ServerSocket serverSocket) {
        if (serverSocket == null) {
            return;
        }
        try {
            serverSocket.close();
        }
        finally {}
    }
    
    public static int \u3007o00\u3007\u3007Oo(final b.a.a.a.a.a.b.c.p.a a) {
        if (a == null) {
            return -1;
        }
        if (a.\u3007080() == 200) {
            return \u3007o\u3007(a.O8("Content-Length", null), -1);
        }
        if (a.\u3007080() == 206) {
            final String o8 = a.O8("Content-Range", null);
            if (!TextUtils.isEmpty((CharSequence)o8)) {
                final int lastIndex = o8.lastIndexOf("/");
                if (lastIndex >= 0 && lastIndex < o8.length() - 1) {
                    return \u3007o\u3007(o8.substring(lastIndex + 1), -1);
                }
            }
        }
        return -1;
    }
    
    public static int \u3007oo\u3007(final String s) {
        return \u3007o\u3007(s, 0);
    }
    
    public static int \u3007o\u3007(final String s, final int n) {
        if (TextUtils.isEmpty((CharSequence)s)) {
            return n;
        }
        try {
            return Integer.parseInt(s);
        }
        catch (final NumberFormatException ex) {
            return n;
        }
    }
    
    public static void \u3007\u3007808\u3007(final Runnable runnable) {
        if (runnable != null) {
            if (\u3007O888o0o()) {
                runnable.run();
            }
            else {
                a.\u3007080.post(runnable);
            }
        }
    }
    
    public static String \u3007\u3007888(final b.a.a.a.a.a.b.c.p.a a, final int a2) {
        if (a != null && a.oO80()) {
            final StringBuilder sb = new StringBuilder();
            sb.append(a.OO0o\u3007\u3007\u3007\u30070().toUpperCase());
            sb.append(' ');
            sb.append(a.\u3007080());
            sb.append(' ');
            sb.append(a.\u300780\u3007808\u3007O());
            sb.append("\r\n");
            if (b.a.a.a.a.a.b.c.e.\u3007o\u3007) {
                final StringBuilder sb2 = new StringBuilder();
                sb2.append(a.OO0o\u3007\u3007\u3007\u30070().toUpperCase());
                sb2.append(" ");
                sb2.append(a.\u3007080());
                sb2.append(" ");
                sb2.append(a.\u300780\u3007808\u3007O());
            }
            final List<i.b> \u300780\u3007808\u3007O = \u300780\u3007808\u3007O(a.\u3007\u3007888());
            int n = 1;
            int n2 = 1;
            if (\u300780\u3007808\u3007O != null) {
                final int size = \u300780\u3007808\u3007O.size();
                int n3 = 0;
                while (true) {
                    n = n2;
                    if (n3 >= size) {
                        break;
                    }
                    final i.b b = \u300780\u3007808\u3007O.get(n3);
                    int n4 = n2;
                    Label_0295: {
                        if (b != null) {
                            final String \u3007080 = b.\u3007080;
                            final String \u3007o00\u3007\u3007Oo = b.\u3007o00\u3007\u3007Oo;
                            sb.append(\u3007080);
                            sb.append(": ");
                            sb.append(\u3007o00\u3007\u3007Oo);
                            sb.append("\r\n");
                            if (!"Content-Range".equalsIgnoreCase(\u3007080)) {
                                n4 = n2;
                                if (!"Accept-Ranges".equalsIgnoreCase(\u3007080)) {
                                    break Label_0295;
                                }
                                n4 = n2;
                                if (!"bytes".equalsIgnoreCase(\u3007o00\u3007\u3007Oo)) {
                                    break Label_0295;
                                }
                            }
                            n4 = 0;
                        }
                    }
                    ++n3;
                    n2 = n4;
                }
            }
            if (n != 0) {
                final int \u3007o00\u3007\u3007Oo2 = \u3007o00\u3007\u3007Oo(a);
                if (\u3007o00\u3007\u3007Oo2 > 0) {
                    sb.append("Content-Range: bytes ");
                    sb.append(Math.max(a2, 0));
                    sb.append("-");
                    sb.append(\u3007o00\u3007\u3007Oo2 - 1);
                    sb.append("/");
                    sb.append(\u3007o00\u3007\u3007Oo2);
                    sb.append("\r\n");
                }
            }
            sb.append("Connection: close");
            sb.append("\r\n");
            sb.append("\r\n");
            final String string = sb.toString();
            final boolean \u3007o\u3007 = b.a.a.a.a.a.b.c.e.\u3007o\u3007;
            return string;
        }
        return null;
    }
    
    public static boolean \u3007\u30078O0\u30078(final String s) {
        return s != null && (s.startsWith("http://") || s.startsWith("https://"));
    }
}

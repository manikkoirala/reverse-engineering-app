// 
// Decompiled by Procyon v0.6.0
// 

package b.a.a.a.a.a.b.e;

public abstract class a implements c
{
    private f O8;
    private g Oo08;
    protected boolean oO80;
    private c o\u30070;
    private e \u3007080;
    private b \u3007o00\u3007\u3007Oo;
    private b.a.a.a.a.a.b.e.c.a \u3007o\u3007;
    private d \u3007\u3007888;
    
    public a() {
        this.oO80 = false;
    }
    
    @Override
    public final void O8(final b \u3007o00\u3007\u3007Oo) {
        this.\u3007o00\u3007\u3007Oo = \u3007o00\u3007\u3007Oo;
    }
    
    protected final boolean OO0o\u3007\u3007(final int n, final int n2) {
        final boolean b = false;
        try {
            final c o\u30070 = this.o\u30070;
            boolean b2 = b;
            if (o\u30070 != null) {
                final boolean b3 = o\u30070.b(this, n, n2);
                b2 = b;
                if (b3) {
                    b2 = true;
                }
            }
            return b2;
        }
        finally {
            final Throwable t;
            b.a.a.a.a.a.a.i.c.OO0o\u3007\u3007\u3007\u30070("AbstractMediaPlayer", "AbstractMediaPlayer.notifyOnError error: ", t);
            return false;
        }
    }
    
    @Override
    public final void Oo08(final g oo08) {
        this.Oo08 = oo08;
    }
    
    protected final boolean Oooo8o0\u3007(final int n, final int n2) {
        final boolean b = false;
        try {
            final d \u3007\u3007888 = this.\u3007\u3007888;
            boolean b2 = b;
            if (\u3007\u3007888 != null) {
                final boolean a = \u3007\u3007888.a(this, n, n2);
                b2 = b;
                if (a) {
                    b2 = true;
                }
            }
            return b2;
        }
        finally {
            final Throwable t;
            b.a.a.a.a.a.a.i.c.OO0o\u3007\u3007\u3007\u30070("AbstractMediaPlayer", "AbstractMediaPlayer.notifyOnInfo error: ", t);
            return false;
        }
    }
    
    @Override
    public void d(final boolean oo80) {
        this.oO80 = oo80;
    }
    
    @Override
    public final void o\u30070(final f o8) {
        this.O8 = o8;
    }
    
    @Override
    public final void \u3007080(final d \u3007\u3007888) {
        this.\u3007\u3007888 = \u3007\u3007888;
    }
    
    protected final void \u30078o8o\u3007(final int n) {
        try {
            final b.a.a.a.a.a.b.e.c.a \u3007o\u3007 = this.\u3007o\u3007;
            if (\u3007o\u3007 != null) {
                \u3007o\u3007.a(this, n);
            }
        }
        finally {
            final Throwable t;
            b.a.a.a.a.a.a.i.c.OO0o\u3007\u3007\u3007\u30070("AbstractMediaPlayer", "AbstractMediaPlayer.notifyOnBufferingUpdate error: ", t);
        }
    }
    
    protected final void \u3007O00() {
        try {
            final f o8 = this.O8;
            if (o8 != null) {
                o8.b(this);
            }
        }
        finally {
            final Throwable t;
            b.a.a.a.a.a.a.i.c.OO0o\u3007\u3007\u3007\u30070("AbstractMediaPlayer", "AbstractMediaPlayer.notifyOnSeekComplete error: ", t);
        }
    }
    
    protected final void \u3007O8o08O(final int n, final int n2, final int n3, final int n4) {
        try {
            final g oo08 = this.Oo08;
            if (oo08 != null) {
                oo08.a(this, n, n2, n3, n4);
            }
        }
        finally {
            final Throwable t;
            b.a.a.a.a.a.a.i.c.OO0o\u3007\u3007\u3007\u30070("AbstractMediaPlayer", "AbstractMediaPlayer.notifyOnVideoSizeChanged error: ", t);
        }
    }
    
    protected final void \u3007O\u3007() {
        try {
            final e \u3007080 = this.\u3007080;
            if (\u3007080 != null) {
                \u3007080.a(this);
            }
        }
        finally {
            final Throwable t;
            b.a.a.a.a.a.a.i.c.OO0o\u3007\u3007\u3007\u30070("AbstractMediaPlayer", "AbstractMediaPlayer.notifyOnPrepared error: ", t);
        }
    }
    
    @Override
    public final void \u3007o00\u3007\u3007Oo(final c o\u30070) {
        this.o\u30070 = o\u30070;
    }
    
    @Override
    public final void \u3007o\u3007(final b.a.a.a.a.a.b.e.c.a \u3007o\u3007) {
        this.\u3007o\u3007 = \u3007o\u3007;
    }
    
    protected final void \u3007\u3007808\u3007() {
        try {
            final b \u3007o00\u3007\u3007Oo = this.\u3007o00\u3007\u3007Oo;
            if (\u3007o00\u3007\u3007Oo != null) {
                \u3007o00\u3007\u3007Oo.c(this);
            }
        }
        finally {
            final Throwable t;
            b.a.a.a.a.a.a.i.c.OO0o\u3007\u3007\u3007\u30070("AbstractMediaPlayer", "AbstractMediaPlayer.notifyOnCompletion error: ", t);
        }
    }
    
    @Override
    public final void \u3007\u3007888(final e \u3007080) {
        this.\u3007080 = \u3007080;
    }
    
    public void \u3007\u30078O0\u30078() {
        this.\u3007080 = null;
        this.\u3007o\u3007 = null;
        this.\u3007o00\u3007\u3007Oo = null;
        this.O8 = null;
        this.Oo08 = null;
        this.o\u30070 = null;
        this.\u3007\u3007888 = null;
    }
}

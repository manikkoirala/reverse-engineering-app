// 
// Decompiled by Procyon v0.6.0
// 

package b.a.a.a.a.a.b.b.b;

import b.b.a.a.f.a.o;
import java.io.InputStream;
import b.b.a.a.f.a.n;
import b.b.a.a.f.a.l;
import java.util.concurrent.TimeUnit;
import b.b.a.a.f.a.j;
import java.net.SocketTimeoutException;
import java.io.IOException;
import android.content.Context;
import java.io.RandomAccessFile;
import java.io.File;

public class b implements c
{
    private File O8;
    private final b.a.a.a.a.a.a.f.c OO0o\u3007\u3007\u3007\u30070;
    private long Oo08;
    private volatile boolean oO80;
    private volatile long o\u30070;
    private volatile long \u3007080;
    private RandomAccessFile \u300780\u3007808\u3007O;
    private final Object \u3007o00\u3007\u3007Oo;
    private File \u3007o\u3007;
    private volatile boolean \u3007\u3007888;
    
    public b(final Context context, final b.a.a.a.a.a.a.f.c oo0o\u3007\u3007\u3007\u30070) {
        this.\u3007080 = -2147483648L;
        this.\u3007o00\u3007\u3007Oo = new Object();
        this.Oo08 = 0L;
        this.o\u30070 = -1L;
        this.\u3007\u3007888 = false;
        this.oO80 = false;
        this.\u300780\u3007808\u3007O = null;
        this.OO0o\u3007\u3007\u3007\u30070 = oo0o\u3007\u3007\u3007\u30070;
        try {
            this.\u3007o\u3007 = b.a.a.a.a.a.b.f.b.O8(oo0o\u3007\u3007\u3007\u30070.b(), oo0o\u3007\u3007\u3007\u30070.e());
            this.O8 = b.a.a.a.a.a.b.f.b.\u3007o\u3007(oo0o\u3007\u3007\u3007\u30070.b(), oo0o\u3007\u3007\u3007\u30070.e());
            if (this.\u30078o8o\u3007()) {
                this.\u300780\u3007808\u3007O = new RandomAccessFile(this.O8, "r");
            }
            else {
                this.\u300780\u3007808\u3007O = new RandomAccessFile(this.\u3007o\u3007, "rw");
            }
            if (!this.\u30078o8o\u3007()) {
                this.Oo08 = this.\u3007o\u3007.length();
                this.\u300780\u3007808\u3007O();
            }
        }
        finally {
            b.a.a.a.a.a.a.i.c.\u30078o8o\u3007("CSJ_MediaDLPlay", "Error using file ", oo0o\u3007\u3007\u3007\u30070.m(), " as disc cache");
        }
    }
    
    private boolean \u30078o8o\u3007() {
        return this.O8.exists();
    }
    
    private long \u3007o00\u3007\u3007Oo() {
        if (this.\u30078o8o\u3007()) {
            return this.O8.length();
        }
        return this.\u3007o\u3007.length();
    }
    
    private void \u3007\u3007888() throws IOException {
        synchronized (this.\u3007o00\u3007\u3007Oo) {
            if (this.\u30078o8o\u3007()) {
                b.a.a.a.a.a.a.i.c.\u30078o8o\u3007("CSJ_MediaDLPlay", "complete: isCompleted ", this.OO0o\u3007\u3007\u3007\u30070.m(), this.OO0o\u3007\u3007\u3007\u30070.e());
                return;
            }
            try {
                if (this.\u3007o\u3007.renameTo(this.O8)) {
                    final RandomAccessFile \u300780\u3007808\u3007O = this.\u300780\u3007808\u3007O;
                    if (\u300780\u3007808\u3007O != null) {
                        \u300780\u3007808\u3007O.close();
                    }
                    this.\u300780\u3007808\u3007O = new RandomAccessFile(this.O8, "rw");
                    b.a.a.a.a.a.a.i.c.\u30078o8o\u3007("CSJ_MediaDLPlay", "complete: rename ", this.OO0o\u3007\u3007\u3007\u30070.e(), this.OO0o\u3007\u3007\u3007\u30070.m());
                    return;
                }
                final StringBuilder sb = new StringBuilder();
                sb.append("Error renaming file ");
                sb.append(this.\u3007o\u3007);
                sb.append(" to ");
                sb.append(this.O8);
                sb.append(" for completion!");
                throw new IOException(sb.toString());
            }
            finally {
                try {
                    final Throwable t;
                    t.printStackTrace();
                    b.a.a.a.a.a.a.i.c.\u3007\u3007808\u3007("CSJ_MediaDLPlay", t.getMessage());
                }
                finally {}
            }
        }
    }
    
    @Override
    public void close() {
        Label_0056: {
            final Throwable t2;
            try {
                if (!this.\u3007\u3007888) {
                    this.\u300780\u3007808\u3007O.close();
                }
                final File \u3007o\u3007 = this.\u3007o\u3007;
                if (\u3007o\u3007 != null) {
                    \u3007o\u3007.setLastModified(System.currentTimeMillis());
                }
                final File o8 = this.O8;
                if (o8 != null) {
                    o8.setLastModified(System.currentTimeMillis());
                    break Label_0056;
                }
                break Label_0056;
            }
            finally {
                final Throwable t = t2;
                t.printStackTrace();
            }
            try {
                final Throwable t = t2;
                t.printStackTrace();
                this.\u3007\u3007888 = true;
            }
            finally {}
        }
    }
    
    @Override
    public long length() throws IOException {
        Label_0108: {
            if (this.\u30078o8o\u3007()) {
                this.\u3007080 = this.O8.length();
                break Label_0108;
            }
            final Object \u3007o00\u3007\u3007Oo = this.\u3007o00\u3007\u3007Oo;
            monitorenter(\u3007o00\u3007\u3007Oo);
            int n = 0;
            try {
                while (this.\u3007080 == -2147483648L) {
                    try {
                        b.a.a.a.a.a.a.i.c.\u3007\u3007808\u3007("CSJ_MediaDLPlay", "totalLength: wait");
                        final int n2 = n + 15;
                        this.\u3007o00\u3007\u3007Oo.wait(5L);
                        n = n2;
                        if (n2 > 20000) {
                            return -1L;
                        }
                        continue;
                    }
                    catch (final InterruptedException ex) {
                        ex.printStackTrace();
                        throw new IOException("total length InterruptException");
                    }
                    break;
                }
                monitorexit(\u3007o00\u3007\u3007Oo);
                b.a.a.a.a.a.a.i.c.\u30078o8o\u3007("CSJ_MediaDLPlay", "totalLength= ", this.\u3007080);
                return this.\u3007080;
            }
            finally {
                monitorexit(\u3007o00\u3007\u3007Oo);
            }
        }
    }
    
    @Override
    public int \u3007080(final long l, final byte[] b, final int off, final int len) throws IOException {
        try {
            if (l == this.\u3007080) {
                return -1;
            }
            int n = 0;
            int read = 0;
            while (!this.\u3007\u3007888) {
                synchronized (this.\u3007o00\u3007\u3007Oo) {
                    final long \u3007o00\u3007\u3007Oo = this.\u3007o00\u3007\u3007Oo();
                    if (l < \u3007o00\u3007\u3007Oo) {
                        final StringBuilder sb = new StringBuilder();
                        sb.append("read:  read ");
                        sb.append(l);
                        sb.append(" success");
                        b.a.a.a.a.a.a.i.c.\u3007\u3007808\u3007("CSJ_MediaDLPlay", sb.toString());
                        this.\u300780\u3007808\u3007O.seek(l);
                        read = this.\u300780\u3007808\u3007O.read(b, off, len);
                    }
                    else {
                        b.a.a.a.a.a.a.i.c.\u30078o8o\u3007("CSJ_MediaDLPlay", "read: wait at ", l, "  file size = ", \u3007o00\u3007\u3007Oo);
                        n += 33;
                        this.\u3007o00\u3007\u3007Oo.wait(33L);
                    }
                    monitorexit(this.\u3007o00\u3007\u3007Oo);
                    if (read > 0) {
                        return read;
                    }
                    if (n < 20000) {
                        continue;
                    }
                    throw new SocketTimeoutException();
                }
                break;
            }
            return -1;
        }
        finally {
            try {
                final Throwable t;
                if (t instanceof IOException) {
                    throw (IOException)t;
                }
                throw new IOException();
            }
            finally {}
        }
    }
    
    public void \u300780\u3007808\u3007O() {
        Object \u3007o\u3007;
        if (b.a.a.a.a.a.a.c.o\u30070() != null) {
            \u3007o\u3007 = b.a.a.a.a.a.a.c.o\u30070().\u3007o\u3007();
        }
        else {
            \u3007o\u3007 = new j.a("v_cache");
        }
        final long n = this.OO0o\u3007\u3007\u3007\u30070.c();
        final TimeUnit milliseconds = TimeUnit.MILLISECONDS;
        ((j.a)\u3007o\u3007).\u3007080(n, milliseconds).O8(this.OO0o\u3007\u3007\u3007\u30070.k(), milliseconds).Oo08(this.OO0o\u3007\u3007\u3007\u30070.r(), milliseconds);
        final j \u3007o\u30072 = ((j.a)\u3007o\u3007).\u3007o\u3007();
        b.a.a.a.a.a.a.i.c.\u30078o8o\u3007("CSJ_MediaDLPlay", "RANGE, bytes=", this.Oo08, " file hash=", this.OO0o\u3007\u3007\u3007\u30070.e());
        final l.a a = new l.a();
        final StringBuilder sb = new StringBuilder();
        sb.append("bytes=");
        sb.append(this.Oo08);
        sb.append("-");
        \u3007o\u30072.\u3007080(a.OO0o\u3007\u3007\u3007\u30070("RANGE", sb.toString()).Oo08(this.OO0o\u3007\u3007\u3007\u30070.m()).\u300780\u3007808\u3007O().oO80()).\u3007O00(new b.b.a.a.f.a.c(this) {
            final b \u3007080;
            
            @Override
            public void \u3007080(b.b.a.a.f.a.b oo08, final n n) throws IOException {
                if (n != null) {
                    final InputStream inputStream = null;
                    final InputStream inputStream2 = null;
                    Object o = null;
                    final b.b.a.a.f.a.b b = null;
                    Label_0797: {
                        o o2;
                        try {
                            this.\u3007080.oO80 = n.\u3007O8o08O();
                            o oo9 = null;
                            Label_0716: {
                                if (this.\u3007080.oO80) {
                                    oo9 = n.Oo08();
                                    oo08 = b;
                                    o = inputStream;
                                    try {
                                        if (this.\u3007080.oO80) {
                                            oo08 = b;
                                            if (oo9 != null) {
                                                o = inputStream;
                                                this.\u3007080.\u3007080 = oo9.oO80() + this.\u3007080.Oo08;
                                                o = inputStream;
                                                oo08 = (b.b.a.a.f.a.b)oo9.Oo08();
                                            }
                                        }
                                        if (oo08 == null) {
                                            Label_0122: {
                                                if (oo08 != null) {
                                                    Label_0177: {
                                                        try {
                                                            ((InputStream)oo08).close();
                                                        }
                                                        finally {
                                                            break Label_0177;
                                                        }
                                                        break Label_0122;
                                                    }
                                                    final Throwable t;
                                                    t.printStackTrace();
                                                    return;
                                                }
                                            }
                                            if (oo9 != null) {
                                                oo9.close();
                                            }
                                            n.close();
                                            if (this.\u3007080.oO80 && this.\u3007080.\u3007o\u3007.length() == this.\u3007080.\u3007080) {
                                                this.\u3007080.\u3007\u3007888();
                                            }
                                            return;
                                        }
                                        o = oo08;
                                        final byte[] b2 = new byte[8192];
                                        o = oo08;
                                        long oo10 = this.\u3007080.Oo08;
                                        long l = 0L;
                                        int off = 0;
                                        boolean b3;
                                        while (true) {
                                            o = oo08;
                                            final int read = ((InputStream)oo08).read(b2, off, 8192 - off);
                                            b3 = true;
                                            if (read == -1) {
                                                break;
                                            }
                                            final int i = off + read;
                                            final long j = l + read;
                                            boolean b4 = false;
                                            Label_0307: {
                                                if (j % 8192L != 0L) {
                                                    o = oo08;
                                                    final long o\u30070 = this.\u3007080.\u3007080;
                                                    o = oo08;
                                                    if (j != o\u30070 - this.\u3007080.Oo08) {
                                                        b4 = false;
                                                        break Label_0307;
                                                    }
                                                }
                                                b4 = true;
                                            }
                                            o = oo08;
                                            b.a.a.a.a.a.a.i.c.\u30078o8o\u3007("CSJ_MediaDLPlay", "Write segment,execAppend =", b4, " offset=", i, " totalLength = ", this.\u3007080.\u3007080, " saveSize =", j, " startSaved=", this.\u3007080.Oo08, " fileHash=", this.\u3007080.OO0o\u3007\u3007\u3007\u30070.e(), " url=", this.\u3007080.OO0o\u3007\u3007\u3007\u30070.m());
                                            long n2 = oo10;
                                            off = i;
                                            if (b4) {
                                                o = oo08;
                                                final Object \u3007o8o08O = this.\u3007080.\u3007o00\u3007\u3007Oo;
                                                o = oo08;
                                                monitorenter(\u3007o8o08O);
                                                try {
                                                    b.a.a.a.a.a.b.f.b.\u3007o00\u3007\u3007Oo(this.\u3007080.\u300780\u3007808\u3007O, b2, oo10.intValue(), i, this.\u3007080.OO0o\u3007\u3007\u3007\u30070.e());
                                                    monitorexit(\u3007o8o08O);
                                                    n2 = oo10 + i;
                                                    off = 0;
                                                }
                                                finally {
                                                    monitorexit(\u3007o8o08O);
                                                    o = oo08;
                                                }
                                            }
                                            oo10 = n2;
                                            l = j;
                                        }
                                        o = oo08;
                                        final long oo11 = this.\u3007080.Oo08;
                                        o = oo08;
                                        final long o\u30072 = this.\u3007080.\u3007080;
                                        o = oo08;
                                        if (l != this.\u3007080.\u3007080 - this.\u3007080.Oo08) {
                                            b3 = false;
                                        }
                                        o = oo08;
                                        b.a.a.a.a.a.a.i.c.\u30078o8o\u3007("CSJ_MediaDLPlay", "Write segment,Write over, startIndex =", oo11, " totalLength = ", o\u30072, " saveSize = ", l, " writeEndSegment =", b3, " url=", this.\u3007080.OO0o\u3007\u3007\u3007\u30070.m());
                                        break Label_0716;
                                    }
                                    finally {
                                        break Label_0797;
                                    }
                                }
                                this.\u3007080.oO80 = false;
                                final b \u3007080 = this.\u3007080;
                                \u3007080.\u3007080 = \u3007080.o\u30070;
                                oo9 = null;
                            }
                            Label_0731: {
                                if (inputStream2 != null) {
                                    Label_0786: {
                                        try {
                                            inputStream2.close();
                                        }
                                        finally {
                                            break Label_0786;
                                        }
                                        break Label_0731;
                                    }
                                    final Throwable t2;
                                    t2.printStackTrace();
                                    return;
                                }
                            }
                            if (oo9 != null) {
                                oo9.close();
                            }
                            n.close();
                            if (this.\u3007080.oO80 && this.\u3007080.\u3007o\u3007.length() == this.\u3007080.\u3007080) {
                                this.\u3007080.\u3007\u3007888();
                            }
                            return;
                        }
                        finally {
                            o2 = null;
                        }
                        try {
                            this.\u3007080.oO80 = false;
                            final b \u300781 = this.\u3007080;
                            \u300781.\u3007080 = \u300781.o\u30070;
                            final Throwable t3;
                            t3.printStackTrace();
                            return;
                        }
                        finally {
                            Label_0983: {
                                Label_0924: {
                                    if (o != null) {
                                        Label_0979: {
                                            try {
                                                ((InputStream)o).close();
                                            }
                                            finally {
                                                break Label_0979;
                                            }
                                            break Label_0924;
                                        }
                                        ((Throwable)n).printStackTrace();
                                        break Label_0983;
                                    }
                                }
                                if (o2 != null) {
                                    o2.close();
                                }
                                n.close();
                                if (this.\u3007080.oO80 && this.\u3007080.\u3007o\u3007.length() == this.\u3007080.\u3007080) {
                                    this.\u3007080.\u3007\u3007888();
                                }
                            }
                        }
                    }
                }
                this.\u3007080.oO80 = false;
                final b \u300782 = this.\u3007080;
                \u300782.\u3007080 = \u300782.o\u30070;
            }
            
            @Override
            public void \u3007o00\u3007\u3007Oo(final b.b.a.a.f.a.b b, final IOException ex) {
                this.\u3007080.oO80 = false;
                this.\u3007080.\u3007080 = -1L;
            }
        });
    }
}

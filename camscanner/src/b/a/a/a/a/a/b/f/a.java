// 
// Decompiled by Procyon v0.6.0
// 

package b.a.a.a.a.a.b.f;

import android.view.View;
import android.os.Build$VERSION;

public class a
{
    private static final int \u3007080;
    
    static {
        \u3007080 = Build$VERSION.SDK_INT;
    }
    
    public static int \u3007080(final long n, final long n2) {
        int b;
        if (n2 > 0L) {
            b = (int)(n * 1.0 / n2 * 100.0);
        }
        else {
            b = 0;
        }
        return Math.min(Math.max(0, b), 100);
    }
    
    public static String \u3007o00\u3007\u3007Oo(long n) {
        final StringBuilder sb = new StringBuilder();
        final long n2 = n / 60000L;
        n = n % 3600000L % 60000L / 1000L;
        if (n2 >= 10L) {
            sb.append(n2);
        }
        else if (n2 > 0L) {
            sb.append(0);
            sb.append(n2);
        }
        else {
            sb.append(0);
            sb.append(0);
        }
        sb.append(":");
        if (n >= 10L) {
            sb.append(n);
        }
        else if (n > 0L) {
            sb.append(0);
            sb.append(n);
        }
        else {
            sb.append(0);
            sb.append(0);
        }
        return sb.toString();
    }
    
    public static void \u3007o\u3007(final View view, final boolean b) {
        if (view == null) {
            return;
        }
        if (b) {
            view.setSystemUiVisibility(0);
        }
        else {
            final int \u3007080 = a.\u3007080;
            if (\u3007080 >= 19) {
                view.setSystemUiVisibility(3846);
            }
            else if (\u3007080 >= 16) {
                view.setSystemUiVisibility(5);
            }
            else {
                view.setSystemUiVisibility(1);
            }
        }
    }
}

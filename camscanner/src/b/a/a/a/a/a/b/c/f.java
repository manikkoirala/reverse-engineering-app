// 
// Decompiled by Procyon v0.6.0
// 

package b.a.a.a.a.a.b.c;

import java.util.List;
import b.a.a.a.a.a.a.i.b;
import android.text.TextUtils;
import java.util.Iterator;
import java.util.concurrent.Callable;
import java.util.Collection;
import java.util.ArrayList;
import android.util.Base64;
import java.io.OutputStream;
import java.io.Reader;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.HashSet;
import java.net.Socket;
import java.io.IOException;
import android.util.Log;
import b.a.a.a.a.a.b.d.a;
import java.net.InetAddress;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicBoolean;
import java.net.ServerSocket;
import java.util.Set;
import android.util.SparseArray;
import b.a.a.a.a.a.b.c.m.c;

public class f
{
    private static volatile f OO0o\u3007\u3007\u3007\u30070;
    private volatile b.a.a.a.a.a.b.c.m.c O8;
    private volatile b.a.a.a.a.a.b.c.l.c Oo08;
    private final Runnable oO80;
    private final SparseArray<Set<g>> o\u30070;
    private volatile ServerSocket \u3007080;
    private final AtomicBoolean \u300780\u3007808\u3007O;
    private volatile int \u3007o00\u3007\u3007Oo;
    private final AtomicInteger \u3007o\u3007;
    private final g.e \u3007\u3007888;
    
    private f() {
        this.\u3007o\u3007 = new AtomicInteger(0);
        final SparseArray o\u30070 = new SparseArray(2);
        this.o\u30070 = (SparseArray<Set<g>>)o\u30070;
        this.\u3007\u3007888 = new g.e() {
            final f \u3007080;
            
            @Override
            public void \u3007080(final g obj) {
                if (b.a.a.a.a.a.b.c.e.\u3007o\u3007) {
                    final StringBuilder sb = new StringBuilder();
                    sb.append("afterExecute, ProxyTask: ");
                    sb.append(obj);
                }
                final int \u3007\u3007888 = obj.\u3007\u3007888();
                synchronized (this.\u3007080.o\u30070) {
                    final Set set = (Set)this.\u3007080.o\u30070.get(\u3007\u3007888);
                    if (set != null) {
                        set.remove(obj);
                    }
                }
            }
            
            @Override
            public void \u3007o00\u3007\u3007Oo(final g g) {
                synchronized (this.\u3007080.o\u30070) {
                    final Set set = (Set)this.\u3007080.o\u30070.get(g.\u3007\u3007888());
                    if (set != null) {
                        set.add(g);
                    }
                }
            }
        };
        this.oO80 = new Runnable() {
            final f o0;
            
            @Override
            public void run() {
                try {
                    final f o0 = this.o0;
                    final InetAddress byName = InetAddress.getByName(this.o0.o800o8O());
                    final int n = 0;
                    o0.\u3007080 = new ServerSocket(0, 50, byName);
                    final f o2 = this.o0;
                    o2.\u3007o00\u3007\u3007Oo = o2.\u3007080.getLocalPort();
                    if (this.o0.\u3007o00\u3007\u3007Oo == -1) {
                        \u3007O8o08O("socket not bound", "");
                        this.o0.Oooo8o0\u3007();
                        return;
                    }
                    j.\u3007080(this.o0.o800o8O(), this.o0.\u3007o00\u3007\u3007Oo);
                    if (!this.o0.\u3007oo\u3007()) {
                        return;
                    }
                    b.a.a.a.a.a.a.i.c.\u30078o8o\u3007("ProxyServer", "run:  state = ", this.o0.\u3007o\u3007);
                    if (!this.o0.\u3007o\u3007.compareAndSet(0, 1)) {
                        return;
                    }
                    b.a.a.a.a.a.a.i.c.\u30078o8o\u3007("ProxyServer", "run:  state = ", this.o0.\u3007o\u3007);
                    int n2 = n;
                    if (e.\u3007o\u3007) {
                        b.a.a.a.a.a.a.i.c.\u3007\u3007808\u3007("ProxyServer", "proxy server start!");
                        n2 = n;
                    }
                    try {
                        while (this.o0.\u3007o\u3007.get() == 1) {
                            try {
                                final Socket accept = this.o0.\u3007080.accept();
                                final b.a.a.a.a.a.b.c.m.c oo88o8O = this.o0.O8;
                                if (oo88o8O != null) {
                                    b.b.a.a.k.e.\u3007O\u3007().execute(new b.b.a.a.k.g(this, "ProxyTask", 10, new g.c().\u3007o00\u3007\u3007Oo(oo88o8O).\u3007o\u3007(accept).\u3007080(this.o0.\u3007\u3007888).O8()) {
                                        final b.a.a.a.a.a.b.c.g o0;
                                        
                                        @Override
                                        public void run() {
                                            this.o0.run();
                                        }
                                    });
                                    continue;
                                }
                                a.\u3007O00(accept);
                                continue;
                            }
                            catch (final IOException ex) {
                                ex.printStackTrace();
                                \u3007O8o08O("accept error", Log.getStackTraceString((Throwable)ex));
                                if (++n2 > 3) {
                                    break;
                                }
                                continue;
                            }
                        }
                    }
                    finally {
                        final Throwable t;
                        final String stackTraceString = Log.getStackTraceString(t);
                        final StringBuilder sb = new StringBuilder();
                        sb.append("proxy server crashed!  ");
                        sb.append(stackTraceString);
                        \u3007O8o08O("error", stackTraceString);
                    }
                    if (e.\u3007o\u3007) {
                        b.a.a.a.a.a.a.i.c.\u3007\u3007808\u3007("ProxyServer", "proxy server closed!");
                    }
                    this.o0.Oooo8o0\u3007();
                }
                catch (final IOException ex2) {
                    if (e.\u3007o\u3007) {
                        final StringBuilder sb2 = new StringBuilder();
                        sb2.append("create ServerSocket error!  ");
                        sb2.append(Log.getStackTraceString((Throwable)ex2));
                    }
                    \u3007O8o08O("create ServerSocket error", Log.getStackTraceString((Throwable)ex2));
                    this.o0.Oooo8o0\u3007();
                }
            }
        };
        this.\u300780\u3007808\u3007O = new AtomicBoolean();
        o\u30070.put(0, (Object)new HashSet());
        o\u30070.put(1, (Object)new HashSet());
    }
    
    private void Oo08() {
        Socket accept = null;
        try {
            try {
                final Socket socket = accept = this.\u3007080.accept();
                socket.setSoTimeout(2000);
                accept = socket;
                accept = socket;
                accept = socket;
                final InputStreamReader in = new InputStreamReader(socket.getInputStream());
                accept = socket;
                final BufferedReader bufferedReader = new BufferedReader(in);
                accept = socket;
                if ("Ping".equals(bufferedReader.readLine())) {
                    accept = socket;
                    final OutputStream outputStream = socket.getOutputStream();
                    accept = socket;
                    outputStream.write("OK\n".getBytes(a.\u3007o00\u3007\u3007Oo));
                    accept = socket;
                    outputStream.flush();
                }
                a.\u3007O00(socket);
            }
            finally {}
        }
        catch (final IOException ex) {
            ex.printStackTrace();
            \u3007O8o08O("ping error", Log.getStackTraceString((Throwable)ex));
            final Socket socket2;
            a.\u3007O00(socket2);
        }
        b.a.a.a.a.a.a.i.c.\u3007\u3007808\u3007("ProxyServer", "answerPing: ");
        return;
        a.\u3007O00(accept);
    }
    
    private void Oooo8o0\u3007() {
        if (this.\u3007o\u3007.compareAndSet(1, 2) || this.\u3007o\u3007.compareAndSet(0, 2)) {
            a.\u3007O\u3007(this.\u3007080);
            this.\u30078o8o\u3007();
        }
    }
    
    private String o800o8O() {
        return new String(Base64.decode("MTI3LjAuMC4x".getBytes(), 0));
    }
    
    public static f \u30070\u3007O0088o() {
        if (f.OO0o\u3007\u3007\u3007\u30070 == null) {
            synchronized (f.class) {
                if (f.OO0o\u3007\u3007\u3007\u30070 == null) {
                    f.OO0o\u3007\u3007\u3007\u30070 = new f();
                }
            }
        }
        return f.OO0o\u3007\u3007\u3007\u30070;
    }
    
    private void \u30078o8o\u3007() {
        final ArrayList list = new ArrayList();
        Object o = this.o\u30070;
        synchronized (o) {
            for (int size = this.o\u30070.size(), i = 0; i < size; ++i) {
                final SparseArray<Set<g>> o\u30070 = this.o\u30070;
                final Set c = (Set)o\u30070.get(o\u30070.keyAt(i));
                if (c != null) {
                    list.addAll(c);
                    c.clear();
                }
            }
            monitorexit(o);
            o = list.iterator();
            while (((Iterator)o).hasNext()) {
                ((g)((Iterator)o).next()).Oo08();
            }
        }
    }
    
    private static void \u3007O8o08O(final String s, final String s2) {
    }
    
    private boolean \u3007oo\u3007() {
        final b.b.a.a.k.f f = new b.b.a.a.k.f((Callable<V>)new c(this.o800o8O(), this.\u3007o00\u3007\u3007Oo), 5, 1);
        b.b.a.a.k.e.\u3007O\u3007().submit(f);
        this.Oo08();
        try {
            if (!(boolean)f.get()) {
                \u3007O8o08O("ping error", "");
                this.Oooo8o0\u3007();
                return false;
            }
            b.a.a.a.a.a.a.i.c.\u3007\u3007808\u3007("ProxyServer", "pingTest: ");
            if (e.\u3007o\u3007) {
                b.a.a.a.a.a.a.i.c.\u3007\u3007808\u3007("ProxyServer", "Ping OK!");
            }
            return true;
        }
        finally {
            final Throwable t;
            t.printStackTrace();
            \u3007O8o08O("ping error", Log.getStackTraceString(t));
            this.Oooo8o0\u3007();
            return false;
        }
    }
    
    void o\u30070(final b.a.a.a.a.a.b.c.l.c oo08) {
        this.Oo08 = oo08;
    }
    
    public void \u300700() {
        if (this.\u300780\u3007808\u3007O.compareAndSet(false, true)) {
            final Thread thread = new Thread(this.oO80);
            thread.setName("csj_proxy_server");
            thread.start();
        }
    }
    
    boolean \u300780\u3007808\u3007O(final int n, final String s) {
        if (s == null) {
            return false;
        }
        synchronized (this.o\u30070) {
            final Set set = (Set)this.o\u30070.get(n);
            if (set != null) {
                for (final g g : set) {
                    if (g != null && s.equals(g.\u3007080OO8\u30070)) {
                        return true;
                    }
                }
            }
            return false;
        }
    }
    
    public String \u3007o\u3007(final boolean b, final boolean b2, String s, final String... array) {
        b.a.a.a.a.a.b.c.l.c oo08 = null;
        if (array == null || array.length == 0) {
            \u3007O8o08O("url", "url is empty");
            return null;
        }
        if (TextUtils.isEmpty((CharSequence)s)) {
            \u3007O8o08O("key", "key is empty");
            return array[0];
        }
        if (this.O8 == null) {
            \u3007O8o08O("db", "VideoProxyDB is null");
            return array[0];
        }
        if (!b) {
            oo08 = this.Oo08;
        }
        if (oo08 == null) {
            \u3007O8o08O("cache", "Cache is null");
            return array[0];
        }
        final int value = this.\u3007o\u3007.get();
        if (value != 1) {
            final StringBuilder sb = new StringBuilder();
            sb.append("ProxyServer is not running, ");
            sb.append(value);
            \u3007O8o08O("state", sb.toString());
            return array[0];
        }
        final List<String> \u30078o8o\u3007 = a.\u30078o8o\u3007(array);
        if (\u30078o8o\u3007 == null) {
            \u3007O8o08O("url", "url not start with http/https");
            return array[0];
        }
        String \u3007080;
        if (b2) {
            \u3007080 = s;
        }
        else {
            \u3007080 = b.\u3007080(s);
        }
        s = i.\u3007o00\u3007\u3007Oo(s, \u3007080, \u30078o8o\u3007);
        if (s == null) {
            \u3007O8o08O("url", "combine proxy url error");
            return array[0];
        }
        if (b) {
            final StringBuilder sb2 = new StringBuilder();
            sb2.append("https://");
            sb2.append(this.o800o8O());
            sb2.append(":");
            sb2.append(this.\u3007o00\u3007\u3007Oo);
            sb2.append("?f=");
            sb2.append(1);
            sb2.append("&");
            sb2.append(s);
            s = sb2.toString();
        }
        else {
            final StringBuilder sb3 = new StringBuilder();
            sb3.append("https://");
            sb3.append(this.o800o8O());
            sb3.append(":");
            sb3.append(this.\u3007o00\u3007\u3007Oo);
            sb3.append("?");
            sb3.append(s);
            s = sb3.toString();
        }
        return s.replaceFirst("s", "");
    }
    
    b.a.a.a.a.a.b.c.c \u3007\u3007808\u3007() {
        return null;
    }
    
    void \u3007\u3007888(final b.a.a.a.a.a.b.c.m.c o8) {
        this.O8 = o8;
    }
    
    b.a.a.a.a.a.b.c.c \u3007\u30078O0\u30078() {
        return null;
    }
    
    private static final class c implements Callable<Boolean>
    {
        private final String o0;
        private final int \u3007OOo8\u30070;
        
        c(final String o0, final int \u3007oOo8\u30070) {
            this.o0 = o0;
            this.\u3007OOo8\u30070 = \u3007oOo8\u30070;
        }
        
        public Boolean \u3007080() {
            Label_0158: {
                Socket socket2;
                try {
                    b.a.a.a.a.a.a.i.c.\u3007\u3007808\u3007("ProxyServer", "call: ");
                    final Socket socket = new Socket(this.o0, this.\u3007OOo8\u30070);
                    try {
                        socket.setSoTimeout(2000);
                        final OutputStream outputStream = socket.getOutputStream();
                        outputStream.write("Ping\n".getBytes(a.\u3007o00\u3007\u3007Oo));
                        outputStream.flush();
                        if ("OK".equals(new BufferedReader(new InputStreamReader(socket.getInputStream())).readLine())) {
                            final Boolean true = Boolean.TRUE;
                            a.\u3007O00(socket);
                            return true;
                        }
                        a.\u3007O00(socket);
                        break Label_0158;
                    }
                    finally {}
                }
                finally {
                    socket2 = null;
                }
                try {
                    final Throwable t;
                    t.printStackTrace();
                    final StringBuilder sb = new StringBuilder();
                    sb.append("call: ");
                    sb.append(t.getMessage());
                    b.a.a.a.a.a.a.i.c.\u3007\u3007808\u3007("ProxyServer", sb.toString());
                    \u3007O8o08O("ping error", Log.getStackTraceString(t));
                    a.\u3007O00(socket2);
                    return Boolean.FALSE;
                }
                finally {
                    a.\u3007O00(socket2);
                }
            }
        }
    }
}

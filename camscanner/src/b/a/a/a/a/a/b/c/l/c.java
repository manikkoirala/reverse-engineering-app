// 
// Decompiled by Procyon v0.6.0
// 

package b.a.a.a.a.a.b.c.l;

import android.content.Context;
import b.a.a.a.a.a.b.c.d;
import android.text.TextUtils;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Comparator;
import java.util.ArrayList;
import java.util.HashMap;
import java.io.IOException;
import android.os.Looper;
import b.b.a.a.k.e;
import b.b.a.a.k.g;
import java.util.Map;
import java.util.Collections;
import java.util.concurrent.ConcurrentHashMap;
import java.util.LinkedHashMap;
import android.os.Handler;
import java.io.File;
import java.util.Set;
import java.util.concurrent.locks.ReentrantReadWriteLock;

public class c extends a
{
    private final ReentrantReadWriteLock.ReadLock O8;
    private final Runnable OO0o\u3007\u3007\u3007\u30070;
    private final ReentrantReadWriteLock.WriteLock Oo08;
    private volatile float oO80;
    private final Set<f> o\u30070;
    public final File \u3007080;
    private final g \u300780\u3007808\u3007O;
    private final Handler \u30078o8o\u3007;
    private final LinkedHashMap<String, File> \u3007o00\u3007\u3007Oo;
    private final ReentrantReadWriteLock \u3007o\u3007;
    private volatile long \u3007\u3007888;
    
    public c(final File \u3007080) throws IOException {
        this.\u3007o00\u3007\u3007Oo = new LinkedHashMap<String, File>(0, 0.75f, true);
        final ReentrantReadWriteLock \u3007o\u3007 = new ReentrantReadWriteLock();
        this.\u3007o\u3007 = \u3007o\u3007;
        this.O8 = \u3007o\u3007.readLock();
        this.Oo08 = \u3007o\u3007.writeLock();
        this.o\u30070 = Collections.newSetFromMap(new ConcurrentHashMap<f, Boolean>());
        this.\u3007\u3007888 = 104857600L;
        this.oO80 = 0.5f;
        this.\u300780\u3007808\u3007O = new g();
        this.OO0o\u3007\u3007\u3007\u30070 = new Runnable() {
            final c o0;
            
            @Override
            public void run() {
                e.Oooo8o0\u3007(new b.b.a.a.k.g(this, "cleanupCmd", 1) {
                    final c$a o0;
                    
                    @Override
                    public void run() {
                        final c o0 = this.o0.o0;
                        o0.\u3007O8o08O(o0.\u3007\u3007888);
                    }
                });
            }
        };
        this.\u30078o8o\u3007 = new Handler(Looper.getMainLooper());
        if (\u3007080 != null && \u3007080.exists() && \u3007080.isDirectory() && \u3007080.canRead() && \u3007080.canWrite()) {
            this.\u3007080 = \u3007080;
            e.Oooo8o0\u3007(new b.b.a.a.k.g(this, "DiskLruCache", 5) {
                final c o0;
                
                @Override
                public void run() {
                    this.o0.Oooo8o0\u3007();
                }
            });
            return;
        }
        String string;
        if (\u3007080 == null) {
            string = " dir null";
        }
        else {
            final StringBuilder sb = new StringBuilder();
            sb.append("exists: ");
            sb.append(\u3007080.exists());
            sb.append(", isDirectory: ");
            sb.append(\u3007080.isDirectory());
            sb.append(", canRead: ");
            sb.append(\u3007080.canRead());
            sb.append(", canWrite: ");
            sb.append(\u3007080.canWrite());
            string = sb.toString();
        }
        final StringBuilder sb2 = new StringBuilder();
        sb2.append("dir error!  ");
        sb2.append(string);
        throw new IOException(sb2.toString());
    }
    
    private void Oooo8o0\u3007() {
        this.Oo08.lock();
        try {
            final File[] listFiles = this.\u3007080.listFiles();
            if (listFiles != null && listFiles.length > 0) {
                final HashMap hashMap = new HashMap<File, Long>(listFiles.length);
                final ArrayList list = new ArrayList<File>(listFiles.length);
                for (final File file : listFiles) {
                    if (file.isFile()) {
                        list.add(file);
                        hashMap.put(file, file.lastModified());
                    }
                }
                Collections.sort((List<Object>)list, (Comparator<? super Object>)new Comparator<File>(this, hashMap) {
                    final HashMap o0;
                    
                    public int \u3007080(final File key, final File key2) {
                        final long n = lcmp(this.o0.get(key) - this.o0.get(key2), 0L);
                        if (n < 0) {
                            return -1;
                        }
                        if (n > 0) {
                            return 1;
                        }
                        return 0;
                    }
                });
                for (final File value : list) {
                    this.\u3007o00\u3007\u3007Oo.put(this.o\u30070(value), value);
                }
            }
            this.Oo08.unlock();
            this.\u3007\u3007888();
        }
        finally {
            this.Oo08.unlock();
        }
    }
    
    private String o\u30070(final File file) {
        return file.getName();
    }
    
    private void \u3007O8o08O(long n) {
        final HashSet set = new HashSet();
        this.Oo08.lock();
        Set<String> set3 = null;
        try {
            final Iterator<Map.Entry<String, File>> iterator = this.\u3007o00\u3007\u3007Oo.entrySet().iterator();
            long n2 = 0L;
            while (iterator.hasNext()) {
                n2 += ((Map.Entry<K, File>)iterator.next()).getValue().length();
            }
            if (n2 <= n) {
                this.Oo08.unlock();
                return;
            }
            final long n3 = (long)(n * this.oO80);
            final HashSet<String> set2 = new HashSet<String>();
            try {
                for (final Map.Entry<String, File> entry : this.\u3007o00\u3007\u3007Oo.entrySet()) {
                    final File file = entry.getValue();
                    if (file != null && file.exists()) {
                        n = n2;
                        if (!this.\u300780\u3007808\u3007O.\u3007o00\u3007\u3007Oo(this.o\u30070(file))) {
                            final long length = file.length();
                            final StringBuilder sb = new StringBuilder();
                            sb.append(file.getAbsolutePath());
                            sb.append("-tmp");
                            final File file2 = new File(sb.toString());
                            n = n2;
                            if (file.renameTo(file2)) {
                                set.add(file2);
                                n = n2 - length;
                                set2.add(entry.getKey());
                            }
                        }
                    }
                    else {
                        set2.add(entry.getKey());
                        n = n2;
                    }
                    n2 = n;
                    if (n <= n3) {
                        break;
                    }
                }
                final Iterator<String> iterator3 = set2.iterator();
                while (iterator3.hasNext()) {
                    this.\u3007o00\u3007\u3007Oo.remove(iterator3.next());
                }
            }
            finally {}
        }
        finally {
            set3 = null;
        }
        try {
            final Set<String> set4;
            ((Throwable)set4).printStackTrace();
            set4 = set3;
            this.Oo08.unlock();
            final Iterator<f> iterator4 = this.o\u30070.iterator();
            while (iterator4.hasNext()) {
                iterator4.next().\u3007080(set4);
            }
            e.Oooo8o0\u3007(new b.b.a.a.k.g(this, "trimSize", 1, set) {
                final HashSet o0;
                
                @Override
                public void run() {
                    for (final File file : this.o0) {
                        try {
                            file.delete();
                        }
                        finally {}
                    }
                }
            });
        }
        finally {
            this.Oo08.unlock();
        }
    }
    
    private void \u3007\u3007888() {
        this.\u30078o8o\u3007.removeCallbacks(this.OO0o\u3007\u3007\u3007\u30070);
        this.\u30078o8o\u3007.postDelayed(this.OO0o\u3007\u3007\u3007\u30070, 10000L);
    }
    
    @Override
    public File O8(final String key) {
        if (this.O8.tryLock()) {
            final File file = this.\u3007o00\u3007\u3007Oo.get(key);
            this.O8.unlock();
            return file;
        }
        return null;
    }
    
    public void oO80(final long \u3007\u3007888) {
        this.\u3007\u3007888 = \u3007\u3007888;
        this.\u3007\u3007888();
    }
    
    @Override
    public void \u3007080(final String s) {
        if (!TextUtils.isEmpty((CharSequence)s)) {
            this.\u300780\u3007808\u3007O.\u3007080(s);
        }
    }
    
    public void \u300780\u3007808\u3007O(final f f) {
        if (f != null) {
            this.o\u30070.add(f);
        }
    }
    
    public void \u30078o8o\u3007() {
        d.\u3007O\u3007().\u3007o\u3007();
        final Context oo08 = b.a.a.a.a.a.b.c.e.Oo08();
        if (oo08 != null) {
            b.a.a.a.a.a.b.c.m.c.O8(oo08).\u300780\u3007808\u3007O(0);
        }
        this.\u30078o8o\u3007.removeCallbacks(this.OO0o\u3007\u3007\u3007\u30070);
        e.Oooo8o0\u3007(new b.b.a.a.k.g(this, "clear", 1) {
            final c o0;
            
            @Override
            public void run() {
                this.o0.\u3007O8o08O(0L);
            }
        });
    }
    
    @Override
    public File \u3007o00\u3007\u3007Oo(final String key) {
        this.O8.lock();
        final File file = this.\u3007o00\u3007\u3007Oo.get(key);
        this.O8.unlock();
        if (file != null) {
            return file;
        }
        final File value = new File(this.\u3007080, key);
        this.Oo08.lock();
        this.\u3007o00\u3007\u3007Oo.put(key, value);
        this.Oo08.unlock();
        final Iterator<f> iterator = this.o\u30070.iterator();
        while (iterator.hasNext()) {
            iterator.next().a(key);
        }
        this.\u3007\u3007888();
        return value;
    }
    
    @Override
    public void \u3007o\u3007(final String s) {
        if (!TextUtils.isEmpty((CharSequence)s)) {
            this.\u300780\u3007808\u3007O.\u3007o\u3007(s);
        }
    }
    
    public interface f
    {
        void a(final String p0);
        
        void \u3007080(final Set<String> p0);
    }
    
    private static final class g
    {
        private final Map<String, Integer> \u3007080;
        
        private g() {
            this.\u3007080 = new HashMap<String, Integer>();
        }
        
        void \u3007080(final String s) {
            synchronized (this) {
                if (!TextUtils.isEmpty((CharSequence)s)) {
                    final Integer n = this.\u3007080.get(s);
                    if (n == null) {
                        this.\u3007080.put(s, 1);
                    }
                    else {
                        this.\u3007080.put(s, n + 1);
                    }
                }
            }
        }
        
        boolean \u3007o00\u3007\u3007Oo(final String s) {
            synchronized (this) {
                return !TextUtils.isEmpty((CharSequence)s) && this.\u3007080.containsKey(s);
            }
        }
        
        void \u3007o\u3007(final String s) {
            synchronized (this) {
                if (!TextUtils.isEmpty((CharSequence)s)) {
                    final Integer n = this.\u3007080.get(s);
                    if (n != null) {
                        if (n == 1) {
                            this.\u3007080.remove(s);
                        }
                        else {
                            this.\u3007080.put(s, n - 1);
                        }
                    }
                }
            }
        }
    }
}

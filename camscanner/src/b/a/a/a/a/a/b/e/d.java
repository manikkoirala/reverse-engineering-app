// 
// Decompiled by Procyon v0.6.0
// 

package b.a.a.a.a.a.b.e;

import android.os.Handler;
import android.net.Uri;
import java.io.File;
import android.text.TextUtils;
import android.os.Message;
import java.util.Collection;
import java.io.FileInputStream;
import java.util.Iterator;
import android.os.SystemClock;
import android.os.Build$VERSION;
import java.util.concurrent.CopyOnWriteArrayList;
import java.lang.ref.WeakReference;
import java.util.List;
import java.util.ArrayList;
import com.bytedance.sdk.component.utils.y;
import android.view.SurfaceHolder;
import android.graphics.SurfaceTexture;
import android.view.Surface;
import java.util.concurrent.atomic.AtomicBoolean;
import android.util.SparseIntArray;
import b.a.a.a.a.a.a.a;
import com.bytedance.sdk.component.utils.y$a;

public class d implements y$a, e, b, c.c, g, c.a, f, b.a.a.a.a.a.b.e.c.d, a
{
    private static final SparseIntArray J;
    private volatile int A;
    private AtomicBoolean B;
    private Surface C;
    private final Runnable D;
    private final o E;
    private volatile boolean F;
    private long G;
    private long H;
    private boolean I;
    private SurfaceTexture a;
    private SurfaceHolder b;
    private int c;
    private int d;
    private boolean e;
    private volatile c f;
    private boolean g;
    private boolean h;
    private boolean i;
    private volatile int j;
    private long k;
    private y l;
    private boolean m;
    private long n;
    private long o;
    private long p;
    private long q;
    private long r;
    private boolean s;
    private ArrayList<Runnable> t;
    private int u;
    private String v;
    private boolean w;
    private final List<WeakReference<a>> x;
    private b.a.a.a.a.a.a.f.c y;
    private boolean z;
    
    static {
        J = new SparseIntArray();
    }
    
    public d() {
        this.c = 0;
        this.e = false;
        this.f = null;
        this.g = false;
        this.j = 201;
        this.k = -1L;
        this.m = false;
        this.n = 0L;
        this.o = Long.MIN_VALUE;
        this.p = 0L;
        this.q = 0L;
        this.r = 0L;
        this.u = 0;
        this.x = new CopyOnWriteArrayList<WeakReference<a>>();
        this.y = null;
        this.z = false;
        this.A = 200;
        this.B = new AtomicBoolean(false);
        this.C = null;
        this.D = new Runnable() {
            final d o0;
            
            @Override
            public void run() {
                if (this.o0.f == null) {
                    return;
                }
                final long n = this.o0.n();
                if (n > 0L && Build$VERSION.SDK_INT >= 23 && this.o0.h() && this.o0.o != Long.MIN_VALUE) {
                    try {
                        if (this.o0.o == n) {
                            if (!this.o0.m && this.o0.p >= 400L) {
                                this.o0.a(701, 800);
                                this.o0.m = true;
                            }
                            final d o0 = this.o0;
                            o0.p += this.o0.A;
                        }
                        else {
                            if (this.o0.m) {
                                final d o2 = this.o0;
                                o2.n += this.o0.p;
                                this.o0.a(702, 800);
                                b.a.a.a.a.a.a.i.c.\u30078o8o\u3007("CSJ_VIDEO_MEDIA", "handleMsg:  bufferingDuration =", this.o0.n, "  bufferCount =", this.o0.c);
                            }
                            this.o0.p = 0L;
                            this.o0.m = false;
                        }
                    }
                    finally {
                        final StringBuilder sb = new StringBuilder();
                        sb.append("error:");
                        final Throwable t;
                        sb.append(t.getMessage());
                        b.a.a.a.a.a.a.i.c.\u3007\u3007808\u3007("CSJ_VIDEO_MEDIA", sb.toString());
                    }
                }
                if (this.o0.r() > 0L) {
                    if (this.o0.o != n) {
                        if (b.a.a.a.a.a.a.c.\u300780\u3007808\u3007O()) {
                            b.a.a.a.a.a.a.i.c.\u30078o8o\u3007("CSJ_VIDEO_MEDIA", "run: lastCur = ", this.o0.o, "  curPosition = ", n);
                        }
                        final d o3 = this.o0;
                        o3.a(n, o3.r());
                    }
                    this.o0.o = n;
                }
                if (!this.o0.d()) {
                    if (this.o0.l != null) {
                        ((Handler)this.o0.l).postDelayed((Runnable)this, (long)this.o0.A);
                    }
                }
                else {
                    final d o4 = this.o0;
                    o4.a(o4.r(), this.o0.r());
                }
            }
        };
        this.E = new o();
        this.G = 0L;
        this.H = 0L;
        this.I = false;
        this.u = 0;
        this.l = b.b.a.a.k.i.a.\u3007080().o\u30070((y$a)this, "csj_SSMediaPlayerWrapper");
        this.I = true;
        this.t();
    }
    
    private void B() {
        b.a.a.a.a.a.a.i.c.\u3007\u3007808\u3007("CSJ_VIDEO_MEDIA", "releaseMediaPlayer: ");
        if (this.f == null) {
            return;
        }
        try {
            this.f.i();
        }
        finally {
            final Throwable t;
            b.a.a.a.a.a.a.i.c.o\u30070("CSJ_VIDEO_MEDIA", "releaseMediaplayer error1: ", t);
        }
        this.f.O8(null);
        this.f.Oo08(null);
        this.f.\u3007o\u3007(null);
        this.f.\u3007080(null);
        this.f.\u3007o00\u3007\u3007Oo(null);
        this.f.\u3007\u3007888(null);
        this.f.o\u30070(null);
        try {
            this.f.release();
        }
        finally {
            final Throwable t2;
            b.a.a.a.a.a.a.i.c.o\u30070("CSJ_VIDEO_MEDIA", "releaseMediaplayer error2: ", t2);
        }
    }
    
    private void C() {
        this.n = 0L;
        this.c = 0;
        this.p = 0L;
        this.m = false;
        this.o = Long.MIN_VALUE;
    }
    
    private void a() {
        final ArrayList<Runnable> t = this.t;
        if (t != null) {
            if (!t.isEmpty()) {
                this.t.clear();
            }
        }
    }
    
    private void a(final int n, final int n2) {
        if (n == 701) {
            this.G = SystemClock.elapsedRealtime();
            ++this.c;
            for (final WeakReference weakReference : this.x) {
                if (weakReference != null && weakReference.get() != null) {
                    ((a)weakReference.get()).a(this, Integer.MAX_VALUE, 0, 0);
                }
            }
            b.a.a.a.a.a.a.i.c.\u30078o8o\u3007("CSJ_VIDEO_MEDIA", "bufferCount = ", this.c);
        }
        else if (n == 702) {
            if (this.G > 0L) {
                this.H += SystemClock.elapsedRealtime() - this.G;
                this.G = 0L;
            }
            for (final WeakReference weakReference2 : this.x) {
                if (weakReference2 != null && weakReference2.get() != null) {
                    ((a)weakReference2.get()).a(this, Integer.MAX_VALUE);
                }
            }
            b.a.a.a.a.a.a.i.c.\u30078o8o\u3007("CSJ_VIDEO_MEDIA", "bufferCount = ", this.c, " mBufferTotalTime = ", this.H);
        }
        else if (this.I && n == 3) {
            final StringBuilder sb = new StringBuilder();
            sb.append("hasPendingPauseCommand:");
            sb.append(this.F);
            b.a.a.a.a.a.a.i.c.\u3007\u3007808\u3007("CSJ_VIDEO_MEDIA", sb.toString());
            this.k();
            this.j();
            this.a(this.z);
            b.a.a.a.a.a.a.i.c.\u3007\u3007808\u3007("CSJ_VIDEO_MEDIA", "onRenderStart");
        }
    }
    
    private void a(final long n) {
        this.E.\u3007080(n);
        if (this.w) {
            this.b(this.E);
        }
        else if (this.a(this.y)) {
            this.b(this.E);
        }
        else {
            this.a(this.E);
        }
    }
    
    private void a(final long n, final long n2) {
        for (final WeakReference weakReference : this.x) {
            if (weakReference != null && weakReference.get() != null) {
                ((a)weakReference.get()).a(this, n, n2);
            }
        }
    }
    
    private void a(final Runnable e) {
        try {
            b.a.a.a.a.a.a.i.c.Oo08("CSJ_VIDEO_MEDIA", "enqueueAction()");
            if (this.t == null) {
                this.t = new ArrayList<Runnable>();
            }
            this.t.add(e);
        }
        finally {
            final Throwable t;
            b.a.a.a.a.a.a.i.c.\u300780\u3007808\u3007O("CSJ_VIDEO_MEDIA", t.getMessage());
        }
    }
    
    private void a(final String name) throws Throwable {
        final FileInputStream fileInputStream = new FileInputStream(name);
        this.f.OO0o\u3007\u3007\u3007\u30070(fileInputStream.getFD());
        fileInputStream.close();
    }
    
    private boolean a(final b.a.a.a.a.a.a.f.c c) {
        return c != null && c.s();
    }
    
    private void b(final Runnable runnable) {
        if (runnable != null) {
            if (!this.g()) {
                if (!this.i) {
                    runnable.run();
                }
                else {
                    this.a(runnable);
                }
            }
        }
    }
    
    private boolean b(final int i, final int j) {
        final StringBuilder sb = new StringBuilder();
        sb.append("OnError - Error code: ");
        sb.append(i);
        sb.append(" Extra code: ");
        sb.append(j);
        b.a.a.a.a.a.a.i.c.Oo08("CSJ_VIDEO_MEDIA", sb.toString());
        final boolean b = true;
        final boolean b2 = i == -1010 || i == -1007 || i == -1004 || i == -110 || i == 100 || i == 200;
        boolean b3 = b;
        if (j != 1) {
            b3 = b;
            if (j != 700) {
                b3 = b;
                if (j != 800) {
                    b3 = b2;
                }
            }
        }
        return b3;
    }
    
    private void j() {
        final long elapsedRealtime = SystemClock.elapsedRealtime();
        final long r = this.r;
        for (final WeakReference weakReference : this.x) {
            if (weakReference != null && weakReference.get() != null) {
                ((a)weakReference.get()).a(this, elapsedRealtime - r);
            }
        }
        this.e = true;
    }
    
    private void k() {
        final ArrayList<Runnable> t = this.t;
        final boolean b = t == null || t.isEmpty();
        final StringBuilder sb = new StringBuilder();
        sb.append("isPendingAction:");
        sb.append(b);
        b.a.a.a.a.a.a.i.c.Oo08("CSJ_VIDEO_MEDIA", sb.toString());
        if (b) {
            return;
        }
        this.l();
    }
    
    private void l() {
        if (this.h) {
            return;
        }
        this.h = true;
        final Iterator iterator = new ArrayList(this.t).iterator();
        while (iterator.hasNext()) {
            ((Runnable)iterator.next()).run();
        }
        this.t.clear();
        this.h = false;
    }
    
    private void s() {
        final SparseIntArray j = b.a.a.a.a.a.b.e.d.J;
        final Integer value = j.get(this.u);
        if (value == null) {
            j.put(this.u, 1);
        }
        else {
            j.put(this.u, (int)(value + 1));
        }
    }
    
    private void t() {
        final StringBuilder sb = new StringBuilder();
        sb.append("initMediaPlayer: ");
        sb.append(this.l != null);
        b.a.a.a.a.a.a.i.c.Oo08("CSJ_VIDEO_MEDIA", sb.toString());
        final y l = this.l;
        if (l != null) {
            ((Handler)l).post((Runnable)new Runnable(this) {
                final d o0;
                
                @Override
                public void run() {
                    if (this.o0.f == null) {
                        try {
                            this.o0.f = new b.a.a.a.a.a.b.e.b();
                        }
                        finally {
                            final Throwable t;
                            b.a.a.a.a.a.a.i.c.\u300780\u3007808\u3007O("CSJ_VIDEO_MEDIA", t.getMessage());
                        }
                        if (this.o0.f == null) {
                            return;
                        }
                        final StringBuilder sb = new StringBuilder();
                        sb.append("initMediaPlayer mMediaPlayer is null :");
                        sb.append(this.o0.f == null);
                        b.a.a.a.a.a.a.i.c.Oo08("CSJ_VIDEO_MEDIA", sb.toString());
                        this.o0.v = "0";
                        this.o0.f.\u3007\u3007888((e)this.o0);
                        this.o0.f.O8((b)this.o0);
                        this.o0.f.\u3007o00\u3007\u3007Oo((c.c)this.o0);
                        this.o0.f.\u3007o\u3007((c.a)this.o0);
                        this.o0.f.o\u30070((f)this.o0);
                        this.o0.f.\u3007080((b.a.a.a.a.a.b.e.c.d)this.o0);
                        this.o0.f.Oo08((g)this.o0);
                        try {
                            this.o0.f.b(false);
                        }
                        finally {
                            final Throwable t2;
                            b.a.a.a.a.a.a.i.c.o\u30070("CSJ_VIDEO_MEDIA", "setLooping error: ", t2);
                        }
                        this.o0.g = false;
                    }
                }
            });
        }
    }
    
    private void v() {
        final y l = this.l;
        if (l != null) {
            if (((Handler)l).getLooper() != null) {
                ((Handler)this.l).post((Runnable)new Runnable(this) {
                    final d o0;
                    
                    @Override
                    public void run() {
                        if (this.o0.l != null && ((Handler)this.o0.l).getLooper() != null) {
                            try {
                                b.a.a.a.a.a.a.i.c.Oo08("CSJ_VIDEO_MEDIA", "onDestory............");
                                b.b.a.a.k.i.a.\u3007080().O8(this.o0.l);
                                this.o0.l = null;
                            }
                            finally {
                                final Throwable t;
                                b.a.a.a.a.a.a.i.c.o\u30070("CSJ_VIDEO_MEDIA", "onDestroy error: ", t);
                            }
                        }
                    }
                });
            }
        }
    }
    
    private void x() {
        final y l = this.l;
        if (l != null) {
            ((Handler)l).post((Runnable)new Runnable(this) {
                final d o0;
                
                @Override
                public void run() {
                    try {
                        this.o0.f.d();
                        this.o0.j = 207;
                        this.o0.F = false;
                    }
                    finally {
                        final Throwable t;
                        b.a.a.a.a.a.a.i.c.o\u30070("CSJ_VIDEO_MEDIA", "pauseBeforePlayIfNeed error: ", t);
                    }
                }
            });
        }
    }
    
    private void z() {
        b.a.a.a.a.a.a.i.c.Oo08("CSJ_VIDEO_MEDIA", "[video] MediaPlayerProxy#start first play prepare invoke !");
        this.b(new Runnable(this) {
            final d o0;
            
            @Override
            public void run() {
                if (this.o0.l != null) {
                    ((Handler)this.o0.l).sendEmptyMessage(104);
                    b.a.a.a.a.a.a.i.c.Oo08("CSJ_VIDEO_MEDIA", "[video] MediaPlayerProxy#start first play prepare invoke ! sendMsg --> OP_PREPARE_ASYNC");
                }
            }
        });
    }
    
    public void A() {
        if (this.g()) {
            return;
        }
        this.i = true;
        this.a();
        final y l = this.l;
        if (l != null) {
            try {
                ((Handler)l).removeCallbacksAndMessages((Object)null);
                if (this.f != null) {
                    ((Handler)this.l).sendEmptyMessage(103);
                }
                this.v();
            }
            finally {
                try {
                    final Throwable t;
                    b.a.a.a.a.a.a.i.c.o\u30070("CSJ_VIDEO_MEDIA", "release error: ", t);
                }
                finally {
                    this.v();
                }
            }
        }
    }
    
    public void D() {
        if (this.g()) {
            return;
        }
        final StringBuilder sb = new StringBuilder();
        sb.append("[video] MediaPlayerProxy#restart:");
        sb.append(this.j);
        b.a.a.a.a.a.a.i.c.Oo08("CSJ_VIDEO_MEDIA", sb.toString());
        if (this.f == null) {
            return;
        }
        this.B.set(true);
        if (this.j != 206) {
            this.C();
            this.F = false;
            this.E.\u3007o00\u3007\u3007Oo(true);
            this.a(0L);
            final y l = this.l;
            if (l != null) {
                ((Handler)l).removeCallbacks(this.D);
                ((Handler)this.l).postDelayed(this.D, (long)this.A);
            }
        }
    }
    
    public void a(final int d) {
        this.d = d;
    }
    
    public void a(final SurfaceTexture a) {
        if (this.g()) {
            return;
        }
        this.a = a;
        this.b(true);
        this.b(new Runnable(this, a) {
            final SurfaceTexture o0;
            final d \u3007OOo8\u30070;
            
            @Override
            public void run() {
                b.a.a.a.a.a.a.i.c.Oo08("CSJ_VIDEO_MEDIA", "setSurface() runnable exec");
                this.\u3007OOo8\u30070.t();
                if (this.\u3007OOo8\u30070.l != null) {
                    ((Handler)this.\u3007OOo8\u30070.l).obtainMessage(111, (Object)this.o0).sendToTarget();
                }
            }
        });
    }
    
    public void a(final Message message) {
        final int j = this.j;
        final int what = message.what;
        final StringBuilder sb = new StringBuilder();
        sb.append("[video]  execute , mCurrentState = ");
        sb.append(this.j);
        sb.append(" handlerMsg=");
        sb.append(what);
        b.a.a.a.a.a.a.i.c.Oo08("CSJ_VIDEO_MEDIA", sb.toString());
        final c f = this.f;
        int n2;
        final int n = n2 = 0;
        Label_1469: {
            if (f != null) {
                switch (message.what) {
                    default: {
                        n2 = n;
                        break Label_1469;
                    }
                    case 111: {
                        b.a.a.a.a.a.a.i.c.Oo08("CSJ_VIDEO_MEDIA", "OP_SET_SURFACE");
                        try {
                            this.C = new Surface((SurfaceTexture)message.obj);
                            this.f.a(this.C);
                            this.f.a(true);
                            this.k();
                        }
                        finally {
                            final Throwable t;
                            b.a.a.a.a.a.a.i.c.o\u30070("CSJ_VIDEO_MEDIA", "OP_SET_SURFACE error: ", t);
                            n2 = n;
                        }
                        break Label_1469;
                    }
                    case 110: {
                        b.a.a.a.a.a.a.i.c.Oo08("CSJ_VIDEO_MEDIA", "OP_SET_DISPLAY");
                        try {
                            this.f.oO80((SurfaceHolder)message.obj);
                            this.f.a(true);
                            this.k();
                        }
                        finally {
                            final Throwable t2;
                            b.a.a.a.a.a.a.i.c.o\u30070("CSJ_VIDEO_MEDIA", "OP_SET_DISPLAY error: ", t2);
                            n2 = n;
                        }
                        break Label_1469;
                    }
                    case 107: {
                        b.a.a.a.a.a.a.i.c.Oo08("CSJ_VIDEO_MEDIA", "OP_SET_DATASOURCE");
                        this.C();
                        if (this.j != 201) {
                            if (this.j != 203) {
                                break;
                            }
                        }
                        try {
                            final b.a.a.a.a.a.a.f.c c = (b.a.a.a.a.a.a.f.c)message.obj;
                            if (TextUtils.isEmpty((CharSequence)c.b())) {
                                c.c(b.a.a.a.a.a.a.c.oO80());
                            }
                            final File file = new File(c.b(), c.e());
                            if (file.exists()) {
                                final StringBuilder sb2 = new StringBuilder();
                                sb2.append("setDataSource\uff1a try paly local:");
                                sb2.append(file.getAbsolutePath());
                                b.a.a.a.a.a.a.i.c.Oo08("CSJ_VIDEO_MEDIA", sb2.toString());
                                if (b.a.a.a.a.a.a.c.OO0o\u3007\u3007\u3007\u30070()) {
                                    this.a(file.getAbsolutePath());
                                }
                                else {
                                    this.f.a(file.getAbsolutePath());
                                }
                            }
                            else {
                                final StringBuilder sb3 = new StringBuilder();
                                sb3.append("setDataSource\uff1a paly net:");
                                sb3.append(c.m());
                                b.a.a.a.a.a.a.i.c.Oo08("CSJ_VIDEO_MEDIA", sb3.toString());
                                if (c.i == 1 && Build$VERSION.SDK_INT < 23) {
                                    this.f.a(c.m());
                                    final StringBuilder sb4 = new StringBuilder();
                                    sb4.append("setDataSource\uff1a  url");
                                    sb4.append(c.m());
                                    b.a.a.a.a.a.a.i.c.Oo08("CSJ_VIDEO_MEDIA", sb4.toString());
                                }
                                else if (Build$VERSION.SDK_INT >= 23) {
                                    this.f.a(c);
                                    final StringBuilder sb5 = new StringBuilder();
                                    sb5.append("setDataSource\uff1a MediaDataSource url");
                                    sb5.append(c.m());
                                    b.a.a.a.a.a.a.i.c.Oo08("CSJ_VIDEO_MEDIA", sb5.toString());
                                }
                                else {
                                    final String o8 = b.a.a.a.a.a.b.c.q.a.\u3007o\u3007().O8(c);
                                    b.a.a.a.a.a.a.i.c.\u3007\u3007888("CSJ_VIDEO_MEDIA", "setDataSource\uff1a  local url = ", o8);
                                    if (o8 != null && b.a.a.a.a.a.a.c.OO0o\u3007\u3007\u3007\u30070() && o8.startsWith("file")) {
                                        this.a(Uri.parse(o8).getPath());
                                    }
                                    else {
                                        this.f.a(o8);
                                    }
                                }
                            }
                            this.j = 202;
                        }
                        finally {
                            final Throwable t3;
                            b.a.a.a.a.a.a.i.c.o\u30070("CSJ_VIDEO_MEDIA", "OP_SET_DATASOURCE error: ", t3);
                            n2 = n;
                        }
                        break Label_1469;
                    }
                    case 106: {
                        b.a.a.a.a.a.a.i.c.Oo08("CSJ_VIDEO_MEDIA", "OP_SEEKTO");
                        if (this.j != 206 && this.j != 207) {
                            if (this.j != 209) {
                                break;
                            }
                        }
                        try {
                            this.f.a((long)message.obj, this.d);
                        }
                        finally {
                            final Throwable t4;
                            b.a.a.a.a.a.a.i.c.o\u30070("CSJ_VIDEO_MEDIA", "OP_SEEKTO error: ", t4);
                            n2 = n;
                        }
                        break Label_1469;
                    }
                    case 105: {
                        b.a.a.a.a.a.a.i.c.Oo08("CSJ_VIDEO_MEDIA", "OP_STOP");
                        if (this.j != 205 && this.j != 206 && this.j != 208 && this.j != 207) {
                            if (this.j != 209) {
                                break;
                            }
                        }
                        try {
                            this.f.e();
                            this.j = 208;
                        }
                        finally {
                            final Throwable t5;
                            b.a.a.a.a.a.a.i.c.o\u30070("CSJ_VIDEO_MEDIA", "OP_STOP error: ", t5);
                            n2 = n;
                        }
                        break Label_1469;
                    }
                    case 104: {
                        b.a.a.a.a.a.a.i.c.Oo08("CSJ_VIDEO_MEDIA", "OP_PREPARE_ASYNC");
                        if (this.j != 202) {
                            if (this.j != 208) {
                                break;
                            }
                        }
                        try {
                            this.f.g();
                            b.a.a.a.a.a.a.i.c.Oo08("CSJ_VIDEO_MEDIA", "[video] OP_PREPARE_ASYNC execute , mMediaPlayer real prepareAsync !");
                        }
                        finally {
                            final Throwable t6;
                            b.a.a.a.a.a.a.i.c.OO0o\u3007\u3007\u3007\u30070("CSJ_VIDEO_MEDIA", "OP_PREPARE_ASYNC error: ", t6);
                            n2 = n;
                        }
                        break Label_1469;
                    }
                    case 103: {
                        b.a.a.a.a.a.a.i.c.Oo08("CSJ_VIDEO_MEDIA", "OP_RELEASE");
                        try {
                            this.B();
                            b.a.a.a.a.a.a.i.c.Oo08("CSJ_VIDEO_MEDIA", "[video] OP_RELEASE execute , releaseMediaplayer !");
                        }
                        finally {
                            final Throwable t7;
                            b.a.a.a.a.a.a.i.c.OO0o\u3007\u3007\u3007\u30070("CSJ_VIDEO_MEDIA", "OP_RELEASE error: ", t7);
                        }
                        for (final WeakReference weakReference : this.x) {
                            if (weakReference != null && weakReference.get() != null) {
                                ((a)weakReference.get()).b(this);
                            }
                        }
                        this.j = 203;
                        n2 = n;
                        break Label_1469;
                    }
                    case 102: {
                        b.a.a.a.a.a.a.i.c.Oo08("CSJ_VIDEO_MEDIA", "OP_RESET");
                        try {
                            this.f.i();
                            b.a.a.a.a.a.a.i.c.Oo08("CSJ_VIDEO_MEDIA", "[video] OP_RESET execute!");
                            this.j = 201;
                        }
                        finally {
                            final Throwable t8;
                            b.a.a.a.a.a.a.i.c.o\u30070("CSJ_VIDEO_MEDIA", "OP_RESET error: ", t8);
                            n2 = n;
                        }
                        break Label_1469;
                    }
                    case 101: {
                        b.a.a.a.a.a.a.i.c.Oo08("CSJ_VIDEO_MEDIA", "OP_PAUSE");
                        if (this.m) {
                            this.n += this.p;
                        }
                        this.m = false;
                        this.p = 0L;
                        this.o = Long.MIN_VALUE;
                        if (this.j != 206 && this.j != 207) {
                            if (this.j != 209) {
                                break;
                            }
                        }
                        try {
                            b.a.a.a.a.a.a.i.c.Oo08("CSJ_VIDEO_MEDIA", "[video] OP_PAUSE execute , mMediaPlayer  OP_PAUSE !");
                            this.f.d();
                            this.j = 207;
                            this.F = false;
                            for (final WeakReference weakReference2 : this.x) {
                                if (weakReference2 != null && weakReference2.get() != null) {
                                    ((a)weakReference2.get()).d(this);
                                }
                            }
                            break Label_1469;
                        }
                        finally {
                            final Throwable t9;
                            b.a.a.a.a.a.a.i.c.o\u30070("CSJ_VIDEO_MEDIA", "OP_PAUSE error: ", t9);
                            n2 = n;
                            break Label_1469;
                        }
                    }
                    case 100: {
                        b.a.a.a.a.a.a.i.c.Oo08("CSJ_VIDEO_MEDIA", "OP_START");
                        if (this.j != 205 && this.j != 207) {
                            if (this.j != 209) {
                                break;
                            }
                        }
                        try {
                            this.f.f();
                            this.r = SystemClock.elapsedRealtime();
                            b.a.a.a.a.a.a.i.c.Oo08("CSJ_VIDEO_MEDIA", "[video] OP_START execute , mMediaPlayer real start !");
                            this.j = 206;
                            if (this.k > 0L) {
                                final StringBuilder sb6 = new StringBuilder();
                                sb6.append("[video] OP_START, seekTo:");
                                sb6.append(this.k);
                                b.a.a.a.a.a.a.i.c.Oo08("CSJ_VIDEO_MEDIA", sb6.toString());
                                this.f.a(this.k, this.d);
                                this.k = -1L;
                            }
                            if (this.y != null) {
                                this.a(this.z);
                            }
                        }
                        finally {
                            final Throwable t10;
                            b.a.a.a.a.a.a.i.c.o\u30070("CSJ_VIDEO_MEDIA", "OP_START error: ", t10);
                            n2 = n;
                        }
                        break Label_1469;
                    }
                }
                n2 = 1;
            }
        }
        if (n2 != 0) {
            b.a.a.a.a.a.a.i.c.Oo08("CSJ_VIDEO_MEDIA", "wrongState");
            this.j = 200;
            if (!this.g) {
                final b.a.a.a.a.a.a.f.a a = new b.a.a.a.a.a.a.f.a(308, what);
                final StringBuilder sb7 = new StringBuilder();
                sb7.append(j);
                sb7.append(",");
                sb7.append(what);
                a.\u3007o00\u3007\u3007Oo(sb7.toString());
                for (final WeakReference weakReference3 : this.x) {
                    if (weakReference3 != null && weakReference3.get() != null) {
                        ((a)weakReference3.get()).a(this, a);
                    }
                }
                this.g = true;
            }
        }
    }
    
    public void a(final SurfaceHolder b) {
        if (this.g()) {
            return;
        }
        this.b = b;
        this.b(true);
        this.b(new Runnable(this, b) {
            final SurfaceHolder o0;
            final d \u3007OOo8\u30070;
            
            @Override
            public void run() {
                b.a.a.a.a.a.a.i.c.Oo08("CSJ_VIDEO_MEDIA", "setDisplay() runnable exec");
                this.\u3007OOo8\u30070.t();
                if (this.\u3007OOo8\u30070.l != null) {
                    ((Handler)this.\u3007OOo8\u30070.l).obtainMessage(110, (Object)this.o0).sendToTarget();
                }
            }
        });
    }
    
    public void a(final a referent) {
        if (referent == null) {
            return;
        }
        for (final WeakReference weakReference : this.x) {
            if (weakReference != null && weakReference.get() == referent) {
                return;
            }
        }
        this.x.add(new WeakReference<a>(referent));
    }
    
    public void a(final c c) {
        if (this.g()) {
            return;
        }
        this.j = 205;
        try {
            final b.a.a.a.a.a.a.f.c y = this.y;
            if (y != null) {
                final float h = y.h();
                if (h > 0.0f) {
                    final b.a.a.a.a.a.a.b b = new b.a.a.a.a.a.a.b();
                    b.\u3007o00\u3007\u3007Oo(h);
                    this.f.\u300780\u3007808\u3007O(b);
                }
            }
        }
        finally {
            final Throwable t;
            b.a.a.a.a.a.a.i.c.o\u30070("CSJ_VIDEO_MEDIA", "speed error: ", t);
        }
        if (this.l != null) {
            if (this.F) {
                this.x();
            }
            else {
                b.a.a.a.a.a.a.i.c.\u300780\u3007808\u3007O("CSJ_VIDEO_MEDIA", "onPrepared op_Start");
                final y l = this.l;
                ((Handler)l).sendMessage(((Handler)l).obtainMessage(100, -1, -1));
            }
        }
        b.a.a.a.a.a.b.e.d.J.delete(this.u);
        final StringBuilder sb = new StringBuilder();
        sb.append("onPrepared:");
        sb.append(this.I);
        sb.append(" ");
        sb.append(this.s);
        b.a.a.a.a.a.a.i.c.\u300780\u3007808\u3007O("CSJ_VIDEO_MEDIA", sb.toString());
        if (!this.I && !this.s) {
            this.j();
            this.s = true;
        }
        for (final WeakReference weakReference : this.x) {
            if (weakReference != null && weakReference.get() != null) {
                ((a)weakReference.get()).c(this);
            }
        }
    }
    
    public void a(final c c, final int n) {
        if (this.f != c) {
            return;
        }
        for (final WeakReference weakReference : this.x) {
            if (weakReference != null && weakReference.get() != null) {
                ((a)weakReference.get()).b(this, n);
            }
        }
    }
    
    public void a(final c c, final int n, final int n2, final int n3, final int n4) {
        for (final WeakReference weakReference : this.x) {
            if (weakReference != null && weakReference.get() != null) {
                ((a)weakReference.get()).a(this, n, n2);
            }
        }
    }
    
    public void a(final boolean b) {
        if (this.g()) {
            return;
        }
        final y l = this.l;
        if (l == null) {
            b.a.a.a.a.a.a.i.c.\u3007\u3007808\u3007("CSJ_VIDEO_MEDIA", "quietPlay set opHandler is null");
            return;
        }
        ((Handler)l).post((Runnable)new Runnable(this, b) {
            final boolean o0;
            final d \u3007OOo8\u30070;
            
            @Override
            public void run() {
                if (!this.\u3007OOo8\u30070.g()) {
                    if (this.\u3007OOo8\u30070.f != null) {
                        try {
                            this.\u3007OOo8\u30070.z = this.o0;
                            this.\u3007OOo8\u30070.f.c(this.o0);
                        }
                        finally {
                            final Throwable t;
                            b.a.a.a.a.a.a.i.c.o\u30070("CSJ_VIDEO_MEDIA", "setQuietPlay error: ", t);
                        }
                    }
                }
            }
        });
    }
    
    public void a(final boolean b, final long n, final boolean b2) {
        final StringBuilder sb = new StringBuilder();
        sb.append("[video] MediaPlayerProxy#start firstSeekToPosition=");
        sb.append(n);
        sb.append(",isFirst :");
        sb.append(b);
        sb.append(",isPauseOtherMusicVolume=");
        sb.append(b2);
        sb.append(" ");
        sb.append(this.j);
        sb.append(" ");
        sb.append(this.f == null);
        b.a.a.a.a.a.a.i.c.Oo08("CSJ_VIDEO_MEDIA", sb.toString());
        if (this.g()) {
            return;
        }
        this.t();
        this.z = b2;
        this.B.set(true);
        this.F = false;
        this.a(b2);
        if (b) {
            b.a.a.a.a.a.a.i.c.Oo08("CSJ_VIDEO_MEDIA", "[video] first start , SSMediaPlayer  start method !");
            this.k = n;
            this.z();
        }
        else {
            this.a(n);
        }
        final y l = this.l;
        if (l != null) {
            ((Handler)l).removeCallbacks(this.D);
            ((Handler)this.l).postDelayed(this.D, (long)this.A);
        }
    }
    
    public boolean a(final c c, final int i, final int j) {
        final StringBuilder sb = new StringBuilder();
        sb.append("what,extra:");
        sb.append(i);
        sb.append(",");
        sb.append(j);
        b.a.a.a.a.a.a.i.c.\u300780\u3007808\u3007O("CSJ_VIDEO_MEDIA", sb.toString());
        if (this.f != c) {
            return false;
        }
        if (j == -1004) {
            final b.a.a.a.a.a.a.f.a a = new b.a.a.a.a.a.a.f.a(i, j);
            for (final WeakReference weakReference : this.x) {
                if (weakReference != null && weakReference.get() != null) {
                    ((a)weakReference.get()).a(this, a);
                }
            }
        }
        this.a(i, j);
        return false;
    }
    
    public int b() {
        if (this.f != null && !this.g()) {
            return this.f.b();
        }
        return 0;
    }
    
    public void b(final int a) {
        if (this.g()) {
            return;
        }
        this.A = a;
    }
    
    public void b(final long n) {
        if (this.g()) {
            return;
        }
        if (this.j == 207 || this.j == 206 || this.j == 209) {
            this.b(new Runnable(this, n) {
                final long o0;
                final d \u3007OOo8\u30070;
                
                @Override
                public void run() {
                    if (this.\u3007OOo8\u30070.l != null) {
                        ((Handler)this.\u3007OOo8\u30070.l).obtainMessage(106, (Object)this.o0).sendToTarget();
                    }
                }
            });
        }
    }
    
    public void b(final b.a.a.a.a.a.a.f.c y) {
        if (this.g()) {
            return;
        }
        if ((this.y = y) != null) {
            this.I = (this.I && !y.s());
        }
        this.b(new Runnable(this, y) {
            final b.a.a.a.a.a.a.f.c o0;
            final d \u3007OOo8\u30070;
            
            @Override
            public void run() {
                b.a.a.a.a.a.a.i.c.Oo08("CSJ_VIDEO_MEDIA", "setDataSource() runnable exec ");
                this.\u3007OOo8\u30070.t();
                if (this.\u3007OOo8\u30070.l != null) {
                    ((Handler)this.\u3007OOo8\u30070.l).obtainMessage(107, (Object)this.o0).sendToTarget();
                }
            }
        });
    }
    
    public void b(final c c) {
        for (final WeakReference weakReference : this.x) {
            if (weakReference != null && weakReference.get() != null) {
                ((a)weakReference.get()).a(this, true);
            }
        }
    }
    
    public void b(final boolean w) {
        if (this.g()) {
            return;
        }
        this.w = w;
        if (this.f != null) {
            this.f.d(w);
        }
        else {
            final y l = this.l;
            if (l != null) {
                ((Handler)l).post((Runnable)new Runnable(this, w) {
                    final boolean o0;
                    final d \u3007OOo8\u30070;
                    
                    @Override
                    public void run() {
                        if (this.\u3007OOo8\u30070.f != null) {
                            this.\u3007OOo8\u30070.f.d(this.o0);
                        }
                    }
                });
            }
        }
    }
    
    public boolean b(final c c, final int i, final int j) {
        final StringBuilder sb = new StringBuilder();
        sb.append("what=");
        sb.append(i);
        sb.append("extra=");
        sb.append(j);
        b.a.a.a.a.a.a.i.c.\u300780\u3007808\u3007O("CSJ_VIDEO_MEDIA", sb.toString());
        this.s();
        this.j = 200;
        final y l = this.l;
        if (l != null) {
            ((Handler)l).removeCallbacks(this.D);
        }
        if (this.b(i, j)) {
            this.v();
        }
        if (!this.B.get()) {
            return true;
        }
        this.B.set(false);
        final b.a.a.a.a.a.a.f.a a = new b.a.a.a.a.a.a.f.a(i, j);
        for (final WeakReference weakReference : this.x) {
            if (weakReference != null && weakReference.get() != null) {
                ((a)weakReference.get()).a(this, a);
            }
        }
        return true;
    }
    
    public int c() {
        if (this.f != null && !this.g()) {
            return this.f.c();
        }
        return 0;
    }
    
    public void c(final c c) {
        this.j = 209;
        b.a.a.a.a.a.b.e.d.J.delete(this.u);
        final y l = this.l;
        if (l != null) {
            ((Handler)l).removeCallbacks(this.D);
        }
        for (final WeakReference weakReference : this.x) {
            if (weakReference != null && weakReference.get() != null) {
                ((a)weakReference.get()).e(this);
            }
        }
    }
    
    public boolean d() {
        return this.j == 209;
    }
    
    public boolean e() {
        return this.e;
    }
    
    public boolean f() {
        return this.u() || this.h() || this.i();
    }
    
    public boolean g() {
        return this.i;
    }
    
    public boolean h() {
        if (this.j != 206) {
            final y l = this.l;
            if (l == null || !((Handler)l).hasMessages(100)) {
                return false;
            }
        }
        if (!this.F) {
            return true;
        }
        return false;
    }
    
    public boolean i() {
        if (this.j == 207 || this.F) {
            final y l = this.l;
            if (l != null && !((Handler)l).hasMessages(100)) {
                return true;
            }
        }
        return false;
    }
    
    public int m() {
        return this.c;
    }
    
    public long n() {
        if (this.g()) {
            return 0L;
        }
        if (this.j != 206) {
            if (this.j != 207) {
                return 0L;
            }
        }
        try {
            return this.f.h();
        }
        finally {
            return 0L;
        }
    }
    
    public SurfaceHolder o() {
        return this.b;
    }
    
    public SurfaceTexture p() {
        return this.a;
    }
    
    public long q() {
        if (Build$VERSION.SDK_INT >= 23) {
            if (this.m) {
                final long p = this.p;
                if (p > 0L) {
                    return this.n + p;
                }
            }
            return this.n;
        }
        return this.H;
    }
    
    public long r() {
        final long q = this.q;
        if (q != 0L) {
            return q;
        }
        Label_0046: {
            if (this.j != 206) {
                if (this.j != 207) {
                    break Label_0046;
                }
            }
            try {
                this.q = this.f.a();
                return this.q;
            }
            finally {
                return this.q;
            }
        }
    }
    
    public boolean u() {
        return this.j == 205;
    }
    
    public void w() {
        b.a.a.a.a.a.a.i.c.\u3007\u3007808\u3007("CSJ_VIDEO_MEDIA", "pause: from outer");
        if (this.g()) {
            return;
        }
        final y l = this.l;
        if (l != null) {
            ((Handler)l).removeMessages(100);
            this.F = true;
            if (!this.I) {
                if (!this.s && !this.a(this.y)) {
                    this.a(new Runnable(this) {
                        final d o0;
                        
                        @Override
                        public void run() {
                            if (this.o0.l != null) {
                                ((Handler)this.o0.l).sendEmptyMessage(101);
                            }
                        }
                    });
                }
                else {
                    final y i = this.l;
                    if (i != null) {
                        ((Handler)i).sendEmptyMessage(101);
                    }
                }
            }
            else if (!this.e && !this.a(this.y)) {
                this.a(new Runnable(this) {
                    final d o0;
                    
                    @Override
                    public void run() {
                        if (this.o0.l != null) {
                            ((Handler)this.o0.l).sendEmptyMessage(101);
                        }
                    }
                });
            }
            else {
                final y j = this.l;
                if (j != null) {
                    ((Handler)j).sendEmptyMessage(101);
                }
            }
        }
    }
    
    public void y() {
        if (this.g()) {
            return;
        }
        if (this.l != null) {
            this.B.set(true);
            ((Handler)this.l).post((Runnable)new Runnable(this) {
                final d o0;
                
                @Override
                public void run() {
                    if (this.o0.i() && this.o0.f != null) {
                        try {
                            this.o0.f.f();
                            b.a.a.a.a.a.a.i.c.\u3007\u3007808\u3007("CSJ_VIDEO_MEDIA", "resume play exec start ");
                            for (final WeakReference weakReference : this.o0.x) {
                                if (weakReference != null && weakReference.get() != null) {
                                    ((a)weakReference.get()).a(this.o0);
                                }
                            }
                            this.o0.j = 206;
                        }
                        finally {
                            final Throwable t;
                            b.a.a.a.a.a.a.i.c.\u30078o8o\u3007("CSJ_VIDEO_MEDIA", "play: catch exception ", t.getMessage());
                        }
                    }
                }
            });
        }
    }
    
    class o implements Runnable
    {
        final d OO;
        private long o0;
        private boolean \u3007OOo8\u30070;
        
        o(final d oo) {
            this.OO = oo;
        }
        
        @Override
        public void run() {
            if (this.OO.f != null) {
                try {
                    if (!this.\u3007OOo8\u30070) {
                        this.OO.k = Math.max(this.o0, this.OO.f.h());
                    }
                    final StringBuilder sb = new StringBuilder();
                    sb.append("[video] MediaPlayerProxy#start, OpStartTask:");
                    sb.append(this.OO.k);
                    b.a.a.a.a.a.a.i.c.Oo08("CSJ_VIDEO_MEDIA", sb.toString());
                }
                finally {
                    final StringBuilder sb2 = new StringBuilder();
                    sb2.append("[video] MediaPlayerProxy#start  error: getCurrentPosition :");
                    final Throwable obj;
                    sb2.append(obj);
                    b.a.a.a.a.a.a.i.c.Oo08("CSJ_VIDEO_MEDIA", sb2.toString());
                }
            }
            if (this.OO.l != null) {
                ((Handler)this.OO.l).sendEmptyMessageDelayed(100, 0L);
            }
            b.a.a.a.a.a.a.i.c.Oo08("CSJ_VIDEO_MEDIA", "[video] MediaPlayerProxy#start not first play ! sendMsg --> OP_START , video start to play !");
        }
        
        public void \u3007080(final long o0) {
            this.o0 = o0;
        }
        
        public void \u3007o00\u3007\u3007Oo(final boolean \u3007oOo8\u30070) {
            this.\u3007OOo8\u30070 = \u3007oOo8\u30070;
        }
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package b.a.a.a.a.a.b.b;

import android.text.TextUtils;
import java.io.IOException;
import b.a.a.a.a.a.b.b.b.b;
import b.a.a.a.a.a.b.b.b.c;
import android.content.Context;
import java.util.concurrent.ConcurrentHashMap;
import android.media.MediaDataSource;

public class a extends MediaDataSource
{
    public static final ConcurrentHashMap<String, a> o\u300700O;
    private final Context OO;
    private final c o0;
    private final b.a.a.a.a.a.a.f.c \u300708O\u300700\u3007o;
    private long \u3007OOo8\u30070;
    
    static {
        o\u300700O = new ConcurrentHashMap<String, a>();
    }
    
    public a(final Context oo, final b.a.a.a.a.a.a.f.c \u300708O\u300700\u3007o) {
        this.\u3007OOo8\u30070 = -2147483648L;
        this.OO = oo;
        this.\u300708O\u300700\u3007o = \u300708O\u300700\u3007o;
        this.o0 = new b(oo, \u300708O\u300700\u3007o);
    }
    
    public static a \u3007\u3007888(final Context context, final b.a.a.a.a.a.a.f.c c) {
        final a value = new a(context, c);
        a.o\u300700O.put(c.e(), value);
        return value;
    }
    
    public b.a.a.a.a.a.a.f.c Oo08() {
        return this.\u300708O\u300700\u3007o;
    }
    
    public void close() throws IOException {
        b.a.a.a.a.a.a.i.c.\u30078o8o\u3007("SdkMediaDataSource", "close: ", this.\u300708O\u300700\u3007o.m());
        final c o0 = this.o0;
        if (o0 != null) {
            o0.close();
        }
        a.o\u300700O.remove(this.\u300708O\u300700\u3007o.e());
    }
    
    public long getSize() throws IOException {
        if (this.\u3007OOo8\u30070 == -2147483648L) {
            if (this.OO == null || TextUtils.isEmpty((CharSequence)this.\u300708O\u300700\u3007o.m())) {
                return -1L;
            }
            this.\u3007OOo8\u30070 = this.o0.length();
            final StringBuilder sb = new StringBuilder();
            sb.append("getSize: ");
            sb.append(this.\u3007OOo8\u30070);
            b.a.a.a.a.a.a.i.c.\u3007\u3007808\u3007("SdkMediaDataSource", sb.toString());
        }
        return this.\u3007OOo8\u30070;
    }
    
    public int readAt(final long lng, final byte[] array, final int i, int \u3007080) throws IOException {
        \u3007080 = this.o0.\u3007080(lng, array, i, \u3007080);
        final StringBuilder sb = new StringBuilder();
        sb.append("readAt: position = ");
        sb.append(lng);
        sb.append("  buffer.length =");
        sb.append(array.length);
        sb.append("  offset = ");
        sb.append(i);
        sb.append(" size =");
        sb.append(\u3007080);
        sb.append("  current = ");
        sb.append(Thread.currentThread());
        b.a.a.a.a.a.a.i.c.\u3007\u3007808\u3007("SdkMediaDataSource", sb.toString());
        return \u3007080;
    }
}

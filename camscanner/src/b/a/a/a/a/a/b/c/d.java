// 
// Decompiled by Procyon v0.6.0
// 

package b.a.a.a.a.a.b.c;

import java.util.concurrent.LinkedBlockingDeque;
import java.util.ArrayList;
import b.b.a.a.k.g;
import android.text.TextUtils;
import java.util.Iterator;
import java.util.Collection;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.RejectedExecutionHandler;
import android.os.Process;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.TimeUnit;
import b.a.a.a.a.a.b.d.a;
import java.util.HashMap;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.HashSet;
import java.util.Map;
import android.util.SparseArray;
import b.a.a.a.a.a.b.c.m.c;
import java.util.concurrent.ExecutorService;

public class d
{
    private static volatile d \u30078o8o\u3007;
    private final ExecutorService O8;
    private volatile boolean OO0o\u3007\u3007\u3007\u30070;
    private volatile c Oo08;
    private final b.b oO80;
    private volatile b.a.a.a.a.a.b.c.l.c o\u30070;
    private volatile int \u3007080;
    private volatile String \u300780\u3007808\u3007O;
    private final SparseArray<Map<String, b>> \u3007o00\u3007\u3007Oo;
    private final g<Runnable> \u3007o\u3007;
    private final HashSet<f> \u3007\u3007888;
    
    private d() {
        this.\u3007080 = 163840;
        final SparseArray \u3007o00\u3007\u3007Oo = new SparseArray(2);
        this.\u3007o00\u3007\u3007Oo = (SparseArray<Map<String, b>>)\u3007o00\u3007\u3007Oo;
        this.\u3007\u3007888 = new HashSet<f>();
        this.oO80 = new b.b() {
            final d \u3007080;
            
            @Override
            public void \u3007080(final b.a.a.a.a.a.b.c.b b) {
                final int \u3007\u3007888 = b.\u3007\u3007888();
                Object \u3007080 = this.\u3007080.\u3007o00\u3007\u3007Oo;
                synchronized (\u3007080) {
                    final Map map = (Map)this.\u3007080.\u3007o00\u3007\u3007Oo.get(\u3007\u3007888);
                    if (map != null) {
                        map.remove(b.\u3007080OO8\u30070);
                    }
                    monitorexit(\u3007080);
                    if (e.\u3007o\u3007) {
                        \u3007080 = new StringBuilder();
                        ((StringBuilder)\u3007080).append("afterExecute, key: ");
                        ((StringBuilder)\u3007080).append(b.\u3007080OO8\u30070);
                    }
                }
            }
        };
        final g \u3007o\u3007 = new g();
        this.\u3007o\u3007 = \u3007o\u3007;
        final ExecutorService \u3007o00\u3007\u3007Oo2 = \u3007o00\u3007\u3007Oo(\u3007o\u3007);
        this.O8 = \u3007o00\u3007\u3007Oo2;
        \u3007o\u3007.\u3007080((ThreadPoolExecutor)\u3007o00\u3007\u3007Oo2);
        \u3007o00\u3007\u3007Oo.put(0, (Object)new HashMap());
        \u3007o00\u3007\u3007Oo.put(1, (Object)new HashMap());
    }
    
    public static d \u3007O\u3007() {
        if (d.\u30078o8o\u3007 == null) {
            synchronized (d.class) {
                if (d.\u30078o8o\u3007 == null) {
                    d.\u30078o8o\u3007 = new d();
                }
            }
        }
        return d.\u30078o8o\u3007;
    }
    
    private static ExecutorService \u3007o00\u3007\u3007Oo(final g<Runnable> workQueue) {
        int \u3007080 = a.\u3007080();
        if (\u3007080 < 1) {
            \u3007080 = 1;
        }
        else if (\u3007080 > 4) {
            \u3007080 = 4;
        }
        return new ThreadPoolExecutor(0, \u3007080, 60L, TimeUnit.SECONDS, workQueue, new ThreadFactory() {
            @Override
            public Thread newThread(final Runnable runnable) {
                final Thread thread = new Thread(this, runnable) {
                    @Override
                    public void run() {
                        try {
                            Process.setThreadPriority(10);
                        }
                        finally {
                            final Throwable t;
                            t.printStackTrace();
                        }
                        super.run();
                    }
                };
                final StringBuilder sb = new StringBuilder();
                sb.append("csj_video_preload_");
                sb.append(thread.getId());
                thread.setName(sb.toString());
                thread.setDaemon(true);
                if (e.\u3007o\u3007) {
                    final StringBuilder sb2 = new StringBuilder();
                    sb2.append("new preload thead: ");
                    sb2.append(thread.getName());
                }
                return thread;
            }
        }, new RejectedExecutionHandler(workQueue) {
            final g \u3007080;
            
            @Override
            public void rejectedExecution(final Runnable e, final ThreadPoolExecutor threadPoolExecutor) {
                try {
                    this.\u3007080.offerFirst(e);
                    final boolean \u3007o\u3007 = e.\u3007o\u3007;
                }
                finally {
                    final Throwable t;
                    t.printStackTrace();
                }
            }
        });
    }
    
    public void O8(final int n) {
        if (n > 0) {
            this.\u3007080 = n;
        }
        if (e.\u3007o\u3007) {
            final StringBuilder sb = new StringBuilder();
            sb.append("MaxPreloadSize: ");
            sb.append(n);
        }
    }
    
    b.a.a.a.a.a.b.c.c OO0o\u3007\u3007() {
        return null;
    }
    
    public void OO0o\u3007\u3007\u3007\u30070(final boolean p0, final boolean p1, final int p2, final String p3, final Map<String, String> p4, final String... p5) {
        // 
        // This method could not be decompiled.
        // 
        // Original Bytecode:
        // 
        //     3: istore          8
        //     5: iload_1        
        //     6: ifeq            15
        //     9: aconst_null    
        //    10: astore          12
        //    12: goto            21
        //    15: aload_0        
        //    16: getfield        b/a/a/a/a/a/b/c/d.o\u30070:Lb/a/a/a/a/a/b/c/l/c;
        //    19: astore          12
        //    21: aload_0        
        //    22: getfield        b/a/a/a/a/a/b/c/d.Oo08:Lb/a/a/a/a/a/b/c/m/c;
        //    25: astore          16
        //    27: aload           12
        //    29: ifnull          880
        //    32: aload           16
        //    34: ifnonnull       40
        //    37: goto            880
        //    40: aload           4
        //    42: invokestatic    android/text/TextUtils.isEmpty:(Ljava/lang/CharSequence;)Z
        //    45: ifne            880
        //    48: aload           6
        //    50: ifnull          880
        //    53: aload           6
        //    55: arraylength    
        //    56: ifgt            62
        //    59: goto            880
        //    62: iload_3        
        //    63: ifgt            74
        //    66: aload_0        
        //    67: getfield        b/a/a/a/a/a/b/c/d.\u3007080:I
        //    70: istore_3       
        //    71: goto            74
        //    74: iload_2        
        //    75: ifeq            85
        //    78: aload           4
        //    80: astore          11
        //    82: goto            92
        //    85: aload           4
        //    87: invokestatic    b/a/a/a/a/a/a/i/b.\u3007080:(Ljava/lang/String;)Ljava/lang/String;
        //    90: astore          11
        //    92: aload           12
        //    94: aload           11
        //    96: invokevirtual   b/a/a/a/a/a/b/c/l/a.O8:(Ljava/lang/String;)Ljava/io/File;
        //    99: astore          9
        //   101: aload           9
        //   103: ifnull          166
        //   106: aload           9
        //   108: invokevirtual   java/io/File.length:()J
        //   111: iload_3        
        //   112: i2l            
        //   113: lcmp           
        //   114: iflt            166
        //   117: iload           8
        //   119: ifeq            165
        //   122: new             Ljava/lang/StringBuilder;
        //   125: dup            
        //   126: invokespecial   java/lang/StringBuilder.<init>:()V
        //   129: astore          4
        //   131: aload           4
        //   133: ldc             "no need preload, file size: "
        //   135: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   138: pop            
        //   139: aload           4
        //   141: aload           9
        //   143: invokevirtual   java/io/File.length:()J
        //   146: invokevirtual   java/lang/StringBuilder.append:(J)Ljava/lang/StringBuilder;
        //   149: pop            
        //   150: aload           4
        //   152: ldc             ", need preload size: "
        //   154: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   157: pop            
        //   158: aload           4
        //   160: iload_3        
        //   161: invokevirtual   java/lang/StringBuilder.append:(I)Ljava/lang/StringBuilder;
        //   164: pop            
        //   165: return         
        //   166: invokestatic    b/a/a/a/a/a/b/c/f.\u30070\u3007O0088o:()Lb/a/a/a/a/a/b/c/f;
        //   169: iload_1        
        //   170: invokestatic    b/a/a/a/a/a/b/c/m/b.\u3007080:(Z)I
        //   173: aload           11
        //   175: invokevirtual   b/a/a/a/a/a/b/c/f.\u300780\u3007808\u3007O:(ILjava/lang/String;)Z
        //   178: ifeq            212
        //   181: iload           8
        //   183: ifeq            211
        //   186: new             Ljava/lang/StringBuilder;
        //   189: dup            
        //   190: invokespecial   java/lang/StringBuilder.<init>:()V
        //   193: astore          5
        //   195: aload           5
        //   197: ldc             "has running proxy task, skip preload for key: "
        //   199: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   202: pop            
        //   203: aload           5
        //   205: aload           4
        //   207: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   210: pop            
        //   211: return         
        //   212: aload_0        
        //   213: getfield        b/a/a/a/a/a/b/c/d.\u3007o00\u3007\u3007Oo:Landroid/util/SparseArray;
        //   216: astore          10
        //   218: aload           10
        //   220: monitorenter   
        //   221: aload_0        
        //   222: getfield        b/a/a/a/a/a/b/c/d.\u3007o00\u3007\u3007Oo:Landroid/util/SparseArray;
        //   225: iload_1        
        //   226: invokevirtual   android/util/SparseArray.get:(I)Ljava/lang/Object;
        //   229: checkcast       Ljava/util/Map;
        //   232: astore          14
        //   234: aload           14
        //   236: aload           11
        //   238: invokeinterface java/util/Map.containsKey:(Ljava/lang/Object;)Z
        //   243: ifeq            250
        //   246: aload           10
        //   248: monitorexit    
        //   249: return         
        //   250: new             Lb/a/a/a/a/a/b/c/d$f;
        //   253: astore          15
        //   255: aload           10
        //   257: astore          9
        //   259: aload           15
        //   261: iload_1        
        //   262: iload_2        
        //   263: iload_3        
        //   264: aload           4
        //   266: aload           5
        //   268: aload           6
        //   270: invokespecial   b/a/a/a/a/a/b/c/d$f.<init>:(ZZILjava/lang/String;Ljava/util/Map;[Ljava/lang/String;)V
        //   273: aload           10
        //   275: astore          9
        //   277: aload_0        
        //   278: getfield        b/a/a/a/a/a/b/c/d.\u300780\u3007808\u3007O:Ljava/lang/String;
        //   281: astore          13
        //   283: aload           13
        //   285: ifnull          572
        //   288: aload           10
        //   290: astore          9
        //   292: getstatic       b/a/a/a/a/a/b/c/e.\u300780\u3007808\u3007O:I
        //   295: istore          7
        //   297: iload           7
        //   299: iconst_3       
        //   300: if_icmpne       412
        //   303: aload           10
        //   305: astore          9
        //   307: aload_0        
        //   308: getfield        b/a/a/a/a/a/b/c/d.\u3007\u3007888:Ljava/util/HashSet;
        //   311: astore          5
        //   313: aload           10
        //   315: astore          9
        //   317: aload           5
        //   319: monitorenter   
        //   320: aload_0        
        //   321: getfield        b/a/a/a/a/a/b/c/d.\u3007\u3007888:Ljava/util/HashSet;
        //   324: aload           15
        //   326: invokevirtual   java/util/HashSet.add:(Ljava/lang/Object;)Z
        //   329: pop            
        //   330: aload           5
        //   332: monitorexit    
        //   333: iload           8
        //   335: ifeq            392
        //   338: aload           10
        //   340: astore          9
        //   342: new             Ljava/lang/StringBuilder;
        //   345: astore          5
        //   347: aload           10
        //   349: astore          9
        //   351: aload           5
        //   353: invokespecial   java/lang/StringBuilder.<init>:()V
        //   356: aload           10
        //   358: astore          9
        //   360: aload           5
        //   362: ldc             "cancel preload: "
        //   364: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   367: pop            
        //   368: aload           10
        //   370: astore          9
        //   372: aload           5
        //   374: aload           4
        //   376: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   379: pop            
        //   380: aload           10
        //   382: astore          9
        //   384: aload           5
        //   386: ldc             ", add to pending queue"
        //   388: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   391: pop            
        //   392: aload           10
        //   394: astore          9
        //   396: aload           10
        //   398: monitorexit    
        //   399: return         
        //   400: astore          4
        //   402: aload           5
        //   404: monitorexit    
        //   405: aload           10
        //   407: astore          9
        //   409: aload           4
        //   411: athrow         
        //   412: iload           7
        //   414: iconst_2       
        //   415: if_icmpne       473
        //   418: iload           8
        //   420: ifeq            465
        //   423: aload           10
        //   425: astore          9
        //   427: new             Ljava/lang/StringBuilder;
        //   430: astore          5
        //   432: aload           10
        //   434: astore          9
        //   436: aload           5
        //   438: invokespecial   java/lang/StringBuilder.<init>:()V
        //   441: aload           10
        //   443: astore          9
        //   445: aload           5
        //   447: ldc             "cancel preload: "
        //   449: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   452: pop            
        //   453: aload           10
        //   455: astore          9
        //   457: aload           5
        //   459: aload           4
        //   461: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   464: pop            
        //   465: aload           10
        //   467: astore          9
        //   469: aload           10
        //   471: monitorexit    
        //   472: return         
        //   473: iload           7
        //   475: iconst_1       
        //   476: if_icmpne       572
        //   479: aload           10
        //   481: astore          9
        //   483: aload_0        
        //   484: getfield        b/a/a/a/a/a/b/c/d.OO0o\u3007\u3007\u3007\u30070:Z
        //   487: iload_1        
        //   488: if_icmpne       572
        //   491: aload           10
        //   493: astore          9
        //   495: aload           13
        //   497: aload           11
        //   499: invokevirtual   java/lang/String.equals:(Ljava/lang/Object;)Z
        //   502: ifeq            572
        //   505: iload           8
        //   507: ifeq            564
        //   510: aload           10
        //   512: astore          9
        //   514: new             Ljava/lang/StringBuilder;
        //   517: astore          5
        //   519: aload           10
        //   521: astore          9
        //   523: aload           5
        //   525: invokespecial   java/lang/StringBuilder.<init>:()V
        //   528: aload           10
        //   530: astore          9
        //   532: aload           5
        //   534: ldc             "cancel preload: "
        //   536: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   539: pop            
        //   540: aload           10
        //   542: astore          9
        //   544: aload           5
        //   546: aload           4
        //   548: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   551: pop            
        //   552: aload           10
        //   554: astore          9
        //   556: aload           5
        //   558: ldc             ", it is playing"
        //   560: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   563: pop            
        //   564: aload           10
        //   566: astore          9
        //   568: aload           10
        //   570: monitorexit    
        //   571: return         
        //   572: aload           10
        //   574: astore          9
        //   576: aload           5
        //   578: invokestatic    b/a/a/a/a/a/b/d/a.OO0o\u3007\u3007\u3007\u30070:(Ljava/util/Map;)Ljava/util/List;
        //   581: invokestatic    b/a/a/a/a/a/b/d/a.\u300780\u3007808\u3007O:(Ljava/util/List;)Ljava/util/List;
        //   584: astore          17
        //   586: aload           17
        //   588: ifnull          711
        //   591: aload           10
        //   593: astore          9
        //   595: new             Ljava/util/ArrayList;
        //   598: astore          13
        //   600: aload           10
        //   602: astore          9
        //   604: aload           13
        //   606: aload           17
        //   608: invokeinterface java/util/List.size:()I
        //   613: invokespecial   java/util/ArrayList.<init>:(I)V
        //   616: aload           10
        //   618: astore          9
        //   620: aload           17
        //   622: invokeinterface java/util/List.size:()I
        //   627: istore          7
        //   629: iconst_0       
        //   630: istore_1       
        //   631: aload           13
        //   633: astore          5
        //   635: iload_1        
        //   636: iload           7
        //   638: if_icmpge       714
        //   641: aload           10
        //   643: astore          9
        //   645: aload           17
        //   647: iload_1        
        //   648: invokeinterface java/util/List.get:(I)Ljava/lang/Object;
        //   653: checkcast       Lb/a/a/a/a/a/b/c/i$b;
        //   656: astore          5
        //   658: aload           5
        //   660: ifnull          705
        //   663: aload           10
        //   665: astore          9
        //   667: new             Lb/a/a/a/a/a/b/c/i$b;
        //   670: astore          18
        //   672: aload           10
        //   674: astore          9
        //   676: aload           18
        //   678: aload           5
        //   680: getfield        b/a/a/a/a/a/b/c/i$b.\u3007080:Ljava/lang/String;
        //   683: aload           5
        //   685: getfield        b/a/a/a/a/a/b/c/i$b.\u3007o00\u3007\u3007Oo:Ljava/lang/String;
        //   688: invokespecial   b/a/a/a/a/a/b/c/i$b.<init>:(Ljava/lang/String;Ljava/lang/String;)V
        //   691: aload           10
        //   693: astore          9
        //   695: aload           13
        //   697: aload           18
        //   699: invokeinterface java/util/List.add:(Ljava/lang/Object;)Z
        //   704: pop            
        //   705: iinc            1, 1
        //   708: goto            631
        //   711: aconst_null    
        //   712: astore          5
        //   714: aload           10
        //   716: astore          9
        //   718: new             Lb/a/a/a/a/a/b/c/b$a;
        //   721: astore          13
        //   723: aload           10
        //   725: astore          9
        //   727: aload           13
        //   729: invokespecial   b/a/a/a/a/a/b/c/b$a.<init>:()V
        //   732: aload           10
        //   734: astore          9
        //   736: aload           13
        //   738: aload           12
        //   740: invokevirtual   b/a/a/a/a/a/b/c/b$a.Oo08:(Lb/a/a/a/a/a/b/c/l/a;)Lb/a/a/a/a/a/b/c/b$a;
        //   743: aload           16
        //   745: invokevirtual   b/a/a/a/a/a/b/c/b$a.o\u30070:(Lb/a/a/a/a/a/b/c/m/c;)Lb/a/a/a/a/a/b/c/b$a;
        //   748: aload           4
        //   750: invokevirtual   b/a/a/a/a/a/b/c/b$a.\u30078o8o\u3007:(Ljava/lang/String;)Lb/a/a/a/a/a/b/c/b$a;
        //   753: aload           11
        //   755: invokevirtual   b/a/a/a/a/a/b/c/b$a.oO80:(Ljava/lang/String;)Lb/a/a/a/a/a/b/c/b$a;
        //   758: astore          4
        //   760: aload           10
        //   762: astore          9
        //   764: new             Lb/a/a/a/a/a/b/c/k;
        //   767: astore          12
        //   769: aload           10
        //   771: astore          9
        //   773: aload           12
        //   775: aload           6
        //   777: invokestatic    b/a/a/a/a/a/b/d/a.\u30078o8o\u3007:([Ljava/lang/String;)Ljava/util/List;
        //   780: invokespecial   b/a/a/a/a/a/b/c/k.<init>:(Ljava/util/List;)V
        //   783: aload           10
        //   785: astore          9
        //   787: aload           4
        //   789: aload           12
        //   791: invokevirtual   b/a/a/a/a/a/b/c/b$a.O8:(Lb/a/a/a/a/a/b/c/k;)Lb/a/a/a/a/a/b/c/b$a;
        //   794: aload           5
        //   796: invokevirtual   b/a/a/a/a/a/b/c/b$a.\u300780\u3007808\u3007O:(Ljava/util/List;)Lb/a/a/a/a/a/b/c/b$a;
        //   799: iload_3        
        //   800: invokevirtual   b/a/a/a/a/a/b/c/b$a.\u3007080:(I)Lb/a/a/a/a/a/b/c/b$a;
        //   803: aload_0        
        //   804: getfield        b/a/a/a/a/a/b/c/d.oO80:Lb/a/a/a/a/a/b/c/b$b;
        //   807: invokevirtual   b/a/a/a/a/a/b/c/b$a.\u3007o00\u3007\u3007Oo:(Lb/a/a/a/a/a/b/c/b$b;)Lb/a/a/a/a/a/b/c/b$a;
        //   810: aload           15
        //   812: invokevirtual   b/a/a/a/a/a/b/c/b$a.\u3007\u3007888:(Ljava/lang/Object;)Lb/a/a/a/a/a/b/c/b$a;
        //   815: invokevirtual   b/a/a/a/a/a/b/c/b$a.OO0o\u3007\u3007\u3007\u30070:()Lb/a/a/a/a/a/b/c/b;
        //   818: astore          4
        //   820: aload           10
        //   822: astore          9
        //   824: aload           14
        //   826: aload           11
        //   828: aload           4
        //   830: invokeinterface java/util/Map.put:(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
        //   835: pop            
        //   836: aload           10
        //   838: astore          9
        //   840: aload           10
        //   842: monitorexit    
        //   843: aload_0        
        //   844: getfield        b/a/a/a/a/a/b/c/d.O8:Ljava/util/concurrent/ExecutorService;
        //   847: aload           4
        //   849: invokeinterface java/util/concurrent/Executor.execute:(Ljava/lang/Runnable;)V
        //   854: return         
        //   855: astore          4
        //   857: aload           10
        //   859: astore          5
        //   861: aload           5
        //   863: astore          9
        //   865: aload           5
        //   867: monitorexit    
        //   868: aload           4
        //   870: athrow         
        //   871: astore          4
        //   873: aload           9
        //   875: astore          5
        //   877: goto            861
        //   880: return         
        //    Signature:
        //  (ZZILjava/lang/String;Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;[Ljava/lang/String;)V
        //    Exceptions:
        //  Try           Handler
        //  Start  End    Start  End    Type
        //  -----  -----  -----  -----  ----
        //  221    249    855    861    Any
        //  250    255    855    861    Any
        //  259    273    871    880    Any
        //  277    283    871    880    Any
        //  292    297    871    880    Any
        //  307    313    871    880    Any
        //  317    320    871    880    Any
        //  320    333    400    412    Any
        //  342    347    871    880    Any
        //  351    356    871    880    Any
        //  360    368    871    880    Any
        //  372    380    871    880    Any
        //  384    392    871    880    Any
        //  396    399    871    880    Any
        //  402    405    400    412    Any
        //  409    412    871    880    Any
        //  427    432    871    880    Any
        //  436    441    871    880    Any
        //  445    453    871    880    Any
        //  457    465    871    880    Any
        //  469    472    871    880    Any
        //  483    491    871    880    Any
        //  495    505    871    880    Any
        //  514    519    871    880    Any
        //  523    528    871    880    Any
        //  532    540    871    880    Any
        //  544    552    871    880    Any
        //  556    564    871    880    Any
        //  568    571    871    880    Any
        //  576    586    871    880    Any
        //  595    600    871    880    Any
        //  604    616    871    880    Any
        //  620    629    871    880    Any
        //  645    658    871    880    Any
        //  667    672    871    880    Any
        //  676    691    871    880    Any
        //  695    705    871    880    Any
        //  718    723    871    880    Any
        //  727    732    871    880    Any
        //  736    760    871    880    Any
        //  764    769    871    880    Any
        //  773    783    871    880    Any
        //  787    820    871    880    Any
        //  824    836    871    880    Any
        //  840    843    871    880    Any
        //  865    868    871    880    Any
        // 
        // The error that occurred was:
        // 
        // java.lang.IllegalStateException: Expression is linked from several locations: Label_0392:
        //     at com.strobel.decompiler.ast.Error.expressionLinkedFromMultipleLocations(Error.java:27)
        //     at com.strobel.decompiler.ast.AstOptimizer.mergeDisparateObjectInitializations(AstOptimizer.java:2604)
        //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:235)
        //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:42)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:206)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:93)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethodBody(AstBuilder.java:868)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethod(AstBuilder.java:761)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addTypeMembers(AstBuilder.java:638)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeCore(AstBuilder.java:605)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeNoCache(AstBuilder.java:195)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createType(AstBuilder.java:162)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addType(AstBuilder.java:137)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.buildAst(JavaLanguage.java:71)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.decompileType(JavaLanguage.java:59)
        //     at com.strobel.decompiler.DecompilerDriver.decompileType(DecompilerDriver.java:333)
        //     at com.strobel.decompiler.DecompilerDriver.decompileJar(DecompilerDriver.java:254)
        //     at com.strobel.decompiler.DecompilerDriver.main(DecompilerDriver.java:144)
        // 
        throw new IllegalStateException("An error occurred while decompiling this method.");
    }
    
    public void Oo08(final long n, final long n2, final long n3) {
        monitorenter(this);
        monitorexit(this);
    }
    
    public void oO80(final String s) {
        this.\u3007O8o08O(false, false, s);
    }
    
    void o\u30070(final b.a.a.a.a.a.b.c.l.c o\u30070) {
        this.o\u30070 = o\u30070;
    }
    
    void \u300780\u3007808\u3007O(final boolean oo0o\u3007\u3007\u3007\u30070, final String s) {
        this.\u300780\u3007808\u3007O = s;
        this.OO0o\u3007\u3007\u3007\u30070 = oo0o\u3007\u3007\u3007\u30070;
        if (e.\u3007o\u3007) {
            final StringBuilder sb = new StringBuilder();
            sb.append("setCurrentPlayKey, ");
            sb.append(s);
        }
        b.a.a.a.a.a.b.c.a a = null;
        final HashSet set = null;
        Object o = null;
        if (s == null) {
            Object \u3007\u3007888 = this.\u3007\u3007888;
            monitorenter(\u3007\u3007888);
            Object o2 = o;
            try {
                if (!this.\u3007\u3007888.isEmpty()) {
                    o2 = new HashSet(this.\u3007\u3007888);
                    this.\u3007\u3007888.clear();
                }
                monitorexit(\u3007\u3007888);
                if (o2 != null) {
                    final Iterator iterator = ((HashSet)o2).iterator();
                    while (iterator.hasNext()) {
                        \u3007\u3007888 = iterator.next();
                        this.OO0o\u3007\u3007\u3007\u30070(((f)\u3007\u3007888).\u3007080, ((f)\u3007\u3007888).\u3007o00\u3007\u3007Oo, ((f)\u3007\u3007888).\u3007o\u3007, ((f)\u3007\u3007888).O8, ((f)\u3007\u3007888).Oo08, ((f)\u3007\u3007888).o\u30070);
                        if (e.\u3007o\u3007) {
                            o = new StringBuilder();
                            ((StringBuilder)o).append("setCurrentPlayKey, resume preload: ");
                            ((StringBuilder)o).append(((f)\u3007\u3007888).O8);
                        }
                    }
                }
                return;
            }
            finally {
                monitorexit(\u3007\u3007888);
            }
        }
        final int \u300780\u3007808\u3007O = e.\u300780\u3007808\u3007O;
        if (\u300780\u3007808\u3007O != 3) {
            if (\u300780\u3007808\u3007O != 2) {
                if (\u300780\u3007808\u3007O != 1) {
                    return;
                }
                synchronized (this.\u3007o00\u3007\u3007Oo) {
                    final Map map = (Map)this.\u3007o00\u3007\u3007Oo.get(b.a.a.a.a.a.b.c.m.b.\u3007080(oo0o\u3007\u3007\u3007\u30070));
                    if (map != null) {
                        a = (b)map.remove(s);
                    }
                    monitorexit(this.\u3007o00\u3007\u3007Oo);
                    if (a != null) {
                        a.Oo08();
                    }
                    return;
                }
            }
        }
        o = this.\u3007o00\u3007\u3007Oo;
        synchronized (o) {
            final int size = this.\u3007o00\u3007\u3007Oo.size();
            int i = 0;
            HashSet set2 = set;
            while (i < size) {
                final SparseArray<Map<String, b>> \u3007o00\u3007\u3007Oo = this.\u3007o00\u3007\u3007Oo;
                final Map map2 = (Map)\u3007o00\u3007\u3007Oo.get(\u3007o00\u3007\u3007Oo.keyAt(i));
                HashSet set3 = set2;
                if (map2 != null) {
                    final Collection values = map2.values();
                    set3 = set2;
                    if (values != null) {
                        set3 = set2;
                        if (!values.isEmpty()) {
                            if ((set3 = set2) == null) {
                                set3 = new HashSet();
                            }
                            set3.addAll(values);
                        }
                    }
                    map2.clear();
                }
                ++i;
                set2 = set3;
            }
            monitorexit(o);
            if (set2 != null && !set2.isEmpty()) {
                for (final b b : set2) {
                    b.Oo08();
                    if (e.\u3007o\u3007) {
                        o = new StringBuilder();
                        ((StringBuilder)o).append("setCurrentPlayKey, cancel preload: ");
                        ((StringBuilder)o).append(b.O8o08O8O);
                    }
                }
                if (\u300780\u3007808\u3007O == 3) {
                    final HashSet<f> \u3007\u3007889 = this.\u3007\u3007888;
                    monitorenter(\u3007\u3007889);
                    try {
                        o = set2.iterator();
                        while (((Iterator)o).hasNext()) {
                            final f e = (f)((Iterator<b>)o).next().o8oOOo;
                            if (e != null) {
                                this.\u3007\u3007888.add(e);
                            }
                        }
                        monitorexit(\u3007\u3007889);
                    }
                    finally {}
                }
            }
        }
    }
    
    public void \u30078o8o\u3007(final boolean b, final boolean b2, final int n, final String s, final String... array) {
        this.OO0o\u3007\u3007\u3007\u30070(b, b2, n, s, null, array);
    }
    
    public void \u3007O8o08O(final boolean b, final boolean b2, final String s) {
        if (TextUtils.isEmpty((CharSequence)s)) {
            return;
        }
        a.\u3007O8o08O(new b.b.a.a.k.g(this, "cancel b b S", b, b2, s) {
            final String OO;
            final boolean o0;
            final d \u300708O\u300700\u3007o;
            final boolean \u3007OOo8\u30070;
            
            @Override
            public void run() {
                synchronized (this.\u300708O\u300700\u3007o.\u3007o00\u3007\u3007Oo) {
                    final Map map = (Map)this.\u300708O\u300700\u3007o.\u3007o00\u3007\u3007Oo.get(b.a.a.a.a.a.b.c.m.b.\u3007080(this.o0));
                    b b;
                    if (map != null) {
                        String s;
                        if (this.\u3007OOo8\u30070) {
                            s = this.OO;
                        }
                        else {
                            s = b.a.a.a.a.a.a.i.b.\u3007080(this.OO);
                        }
                        b = map.remove(s);
                    }
                    else {
                        b = null;
                    }
                    monitorexit(this.\u300708O\u300700\u3007o.\u3007o00\u3007\u3007Oo);
                    if (b != null) {
                        b.Oo08();
                    }
                }
            }
        });
    }
    
    public void \u3007o\u3007() {
        a.\u3007O8o08O(new b.b.a.a.k.g(this, "cancelAll") {
            final d o0;
            
            @Override
            public void run() {
                final ArrayList list = new ArrayList();
                Object \u3007080 = this.o0.\u3007o00\u3007\u3007Oo;
                synchronized (\u3007080) {
                    for (int size = this.o0.\u3007o00\u3007\u3007Oo.size(), i = 0; i < size; ++i) {
                        final Map map = (Map)this.o0.\u3007o00\u3007\u3007Oo.get(this.o0.\u3007o00\u3007\u3007Oo.keyAt(i));
                        if (map != null) {
                            list.addAll(map.values());
                            map.clear();
                        }
                    }
                    this.o0.\u3007o\u3007.clear();
                    monitorexit(\u3007080);
                    final Iterator iterator = list.iterator();
                    while (iterator.hasNext()) {
                        \u3007080 = iterator.next();
                        ((b.a.a.a.a.a.b.c.a)\u3007080).Oo08();
                        if (e.\u3007o\u3007) {
                            final StringBuilder sb = new StringBuilder();
                            sb.append("PreloadTask: ");
                            sb.append(\u3007080);
                            sb.append(", canceled!!!");
                        }
                    }
                }
            }
        });
    }
    
    b.a.a.a.a.a.b.c.c \u3007\u3007808\u3007() {
        return null;
    }
    
    void \u3007\u3007888(final c oo08) {
        this.Oo08 = oo08;
    }
    
    private static final class f
    {
        final String O8;
        final Map<String, String> Oo08;
        final String[] o\u30070;
        final boolean \u3007080;
        final boolean \u3007o00\u3007\u3007Oo;
        final int \u3007o\u3007;
        
        f(final boolean \u3007080, final boolean \u3007o00\u3007\u3007Oo, final int \u3007o\u3007, final String o8, final Map<String, String> oo08, final String[] o\u30070) {
            this.\u3007080 = \u3007080;
            this.\u3007o00\u3007\u3007Oo = \u3007o00\u3007\u3007Oo;
            this.\u3007o\u3007 = \u3007o\u3007;
            this.O8 = o8;
            this.Oo08 = oo08;
            this.o\u30070 = o\u30070;
        }
        
        @Override
        public boolean equals(final Object o) {
            if (this == o) {
                return true;
            }
            if (o != null && f.class == o.getClass()) {
                final f f = (f)o;
                return this.\u3007080 == f.\u3007080 && this.\u3007o00\u3007\u3007Oo == f.\u3007o00\u3007\u3007Oo && this.\u3007o\u3007 == f.\u3007o\u3007 && this.O8.equals(f.O8);
            }
            return false;
        }
        
        @Override
        public int hashCode() {
            return (((this.\u3007080 ? 1 : 0) * 31 + (this.\u3007o00\u3007\u3007Oo ? 1 : 0)) * 31 + this.\u3007o\u3007) * 31 + this.O8.hashCode();
        }
    }
    
    private static final class g<T> extends LinkedBlockingDeque<T>
    {
        private ThreadPoolExecutor o0;
        
        @Override
        public boolean offer(final T e) {
            synchronized (this) {
                final int poolSize = this.o0.getPoolSize();
                final int activeCount = this.o0.getActiveCount();
                final int maximumPoolSize = this.o0.getMaximumPoolSize();
                if (activeCount >= poolSize && poolSize < maximumPoolSize) {
                    final boolean \u3007o\u3007 = e.\u3007o\u3007;
                    return false;
                }
                return this.offerFirst(e);
            }
        }
        
        public void \u3007080(final ThreadPoolExecutor o0) {
            synchronized (this) {
                if (this.o0 != null) {
                    throw new IllegalStateException("You can only call setExecutor() once!");
                }
                if (o0 != null) {
                    this.o0 = o0;
                    return;
                }
                throw new NullPointerException("executor argument can't be null!");
            }
        }
    }
}

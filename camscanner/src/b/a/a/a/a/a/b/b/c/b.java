// 
// Decompiled by Procyon v0.6.0
// 

package b.a.a.a.a.a.b.b.c;

import java.io.InputStream;
import b.b.a.a.f.a.o;
import java.io.RandomAccessFile;
import b.b.a.a.f.a.n;
import b.b.a.a.f.a.l;
import java.util.concurrent.TimeUnit;
import b.b.a.a.f.a.j;
import java.io.IOException;
import java.io.Closeable;
import java.util.Iterator;
import java.util.ArrayList;
import b.a.a.a.a.a.a.f.c;
import android.content.Context;
import b.a.a.a.a.a.a.h.a;
import java.util.List;
import java.io.File;

public class b
{
    private File O8;
    private File Oo08;
    private final List<a.a> o\u30070;
    private Context \u3007080;
    private c \u3007o00\u3007\u3007Oo;
    private volatile boolean \u3007o\u3007;
    private volatile boolean \u3007\u3007888;
    
    public b(final Context \u3007080, final c \u3007o00\u3007\u3007Oo) {
        this.\u3007o\u3007 = false;
        this.O8 = null;
        this.Oo08 = null;
        this.o\u30070 = new ArrayList<a.a>();
        this.\u3007\u3007888 = false;
        this.\u3007080 = \u3007080;
        this.\u3007o00\u3007\u3007Oo = \u3007o00\u3007\u3007Oo;
        this.O8 = b.a.a.a.a.a.b.f.b.O8(\u3007o00\u3007\u3007Oo.b(), \u3007o00\u3007\u3007Oo.e());
        this.Oo08 = b.a.a.a.a.a.b.f.b.\u3007o\u3007(\u3007o00\u3007\u3007Oo.b(), \u3007o00\u3007\u3007Oo.e());
    }
    
    private void O8(final c c, final int n, final String s) {
        synchronized (a.a.class) {
            for (final a.a a : this.o\u30070) {
                if (a != null) {
                    a.a(c, n, s);
                }
            }
        }
    }
    
    private void OO0o\u3007\u3007(final c c, final int n) {
        synchronized (a.a.class) {
            for (final a.a a : this.o\u30070) {
                if (a != null) {
                    a.a(c, n);
                }
            }
        }
    }
    
    private boolean OoO8() {
        final boolean exists = this.Oo08.exists();
        boolean b = true;
        if (exists) {
            return true;
        }
        if (this.\u3007o00\u3007\u3007Oo.u()) {
            return false;
        }
        if (this.O8.length() >= this.\u3007o00\u3007\u3007Oo.j()) {
            return true;
        }
        if (this.\u3007o00\u3007\u3007Oo.g() <= 0 || this.O8.length() < this.\u3007o00\u3007\u3007Oo.g()) {
            b = false;
        }
        return b;
    }
    
    private void \u300780\u3007808\u3007O(final Closeable closeable) {
        if (closeable == null) {
            return;
        }
        try {
            closeable.close();
        }
        catch (final Exception ex) {}
    }
    
    private void \u3007O8o08O() {
        final Throwable t2;
        try {
            if (this.O8.renameTo(this.Oo08)) {
                return;
            }
            final StringBuilder sb = new StringBuilder();
            sb.append("Error renaming file ");
            sb.append(this.O8);
            sb.append(" to ");
            sb.append(this.Oo08);
            sb.append(" for completion!");
            throw new IOException(sb.toString());
        }
        finally {
            final Throwable t = t2;
            t.printStackTrace();
            final String s = "VideoPreload";
            final Throwable t3 = t2;
            final String s2 = t3.getMessage();
            b.a.a.a.a.a.a.i.c.\u3007\u3007808\u3007(s, s2);
        }
        try {
            final Throwable t = t2;
            t.printStackTrace();
            final String s = "VideoPreload";
            final Throwable t3 = t2;
            final String s2 = t3.getMessage();
            b.a.a.a.a.a.a.i.c.\u3007\u3007808\u3007(s, s2);
        }
        finally {}
    }
    
    private void \u3007o00\u3007\u3007Oo() {
        try {
            this.Oo08.delete();
            this.O8.delete();
        }
        finally {
            final Throwable t;
            t.printStackTrace();
        }
    }
    
    private void \u3007o\u3007(final c c, final int n) {
        synchronized (a.a.class) {
            for (final a.a a : this.o\u30070) {
                if (a != null) {
                    a.b(c, n);
                }
            }
        }
    }
    
    private void \u3007\u3007808\u3007() {
        j.a \u3007o\u3007;
        if (b.a.a.a.a.a.a.c.o\u30070() != null) {
            \u3007o\u3007 = b.a.a.a.a.a.a.c.o\u30070().\u3007o\u3007();
        }
        else {
            \u3007o\u3007 = new j.a("v_preload");
        }
        final long n = this.\u3007o00\u3007\u3007Oo.c();
        final TimeUnit milliseconds = TimeUnit.MILLISECONDS;
        \u3007o\u3007.\u3007080(n, milliseconds).O8(this.\u3007o00\u3007\u3007Oo.k(), milliseconds).Oo08(this.\u3007o00\u3007\u3007Oo.r(), milliseconds);
        final j \u3007o\u30072 = \u3007o\u3007.\u3007o\u3007();
        final l.a a = new l.a();
        final long length = this.O8.length();
        final int j = this.\u3007o00\u3007\u3007Oo.j();
        final boolean u = this.\u3007o00\u3007\u3007Oo.u();
        final int g = this.\u3007o00\u3007\u3007Oo.g();
        int i = j;
        boolean b = u;
        if (g > 0) {
            if (g >= this.\u3007o00\u3007\u3007Oo.p()) {
                b = true;
                i = j;
            }
            else {
                i = g;
                b = u;
            }
        }
        if (b) {
            final StringBuilder sb = new StringBuilder();
            sb.append("bytes=");
            sb.append(length);
            sb.append("-");
            a.OO0o\u3007\u3007\u3007\u30070("RANGE", sb.toString()).Oo08(this.\u3007o00\u3007\u3007Oo.m()).\u300780\u3007808\u3007O().oO80();
        }
        else {
            final StringBuilder sb2 = new StringBuilder();
            sb2.append("bytes=");
            sb2.append(length);
            sb2.append("-");
            sb2.append(i);
            a.OO0o\u3007\u3007\u3007\u30070("RANGE", sb2.toString()).Oo08(this.\u3007o00\u3007\u3007Oo.m()).\u300780\u3007808\u3007O().oO80();
        }
        \u3007o\u30072.\u3007080(a.oO80()).\u3007O00(new b.b.a.a.f.a.c(this, length) {
            final long \u3007080;
            final b \u3007o00\u3007\u3007Oo;
            
            @Override
            public void \u3007080(b.b.a.a.f.a.b b, final n n) throws IOException {
                long \u3007080 = this.\u3007080;
                Object o = null;
                Closeable oo08 = null;
                Object o2 = null;
                Label_0859: {
                    Label_0723: {
                        if (n == null) {
                            break Label_0723;
                        }
                        try {
                            final boolean \u3007o8o08O = n.\u3007O8o08O();
                            Label_0140: {
                                if (!\u3007o8o08O) {
                                    try {
                                        final b \u3007o00\u3007\u3007Oo = this.\u3007o00\u3007\u3007Oo;
                                        \u3007o00\u3007\u3007Oo.O8(\u3007o00\u3007\u3007Oo.\u3007o00\u3007\u3007Oo, n.oO80(), n.OO0o\u3007\u3007());
                                        this.\u3007o00\u3007\u3007Oo.\u300780\u3007808\u3007O(null);
                                        this.\u3007o00\u3007\u3007Oo.\u300780\u3007808\u3007O(null);
                                        this.\u3007o00\u3007\u3007Oo.\u300780\u3007808\u3007O(null);
                                        this.\u3007o00\u3007\u3007Oo.\u300780\u3007808\u3007O(n);
                                        b.a.a.a.a.a.a.i.c.\u30078o8o\u3007("VideoPreload", "Pre finally ", this.\u3007o00\u3007\u3007Oo.\u3007o00\u3007\u3007Oo.m(), " Preload size=", this.\u3007o00\u3007\u3007Oo.\u3007o00\u3007\u3007Oo.j());
                                        b.a.a.a.a.a.b.b.c.c.\u3007o00\u3007\u3007Oo(this.\u3007o00\u3007\u3007Oo.\u3007o00\u3007\u3007Oo);
                                        return;
                                    }
                                    finally {
                                        oo08 = null;
                                    }
                                }
                                else {
                                    oo08 = n.Oo08();
                                    Label_0753: {
                                        try {
                                            o2 = n.Oo08();
                                            long \u300781 = 0L;
                                            Label_0211: {
                                                if (\u3007o8o08O && o2 != null) {
                                                    try {
                                                        \u300781 = this.\u3007080;
                                                        final long oo9 = ((o)o2).oO80();
                                                        ((o)o2).Oo08();
                                                        \u300781 += oo9;
                                                        break Label_0211;
                                                    }
                                                    finally {
                                                        oo08 = (Closeable)o2;
                                                        break Label_0140;
                                                    }
                                                }
                                                b = null;
                                                \u300781 = 0L;
                                            }
                                            Label_0703: {
                                                if (b == null) {
                                                    try {
                                                        final b \u3007o00\u3007\u3007Oo2 = this.\u3007o00\u3007\u3007Oo;
                                                        \u3007o00\u3007\u3007Oo2.O8(\u3007o00\u3007\u3007Oo2.\u3007o00\u3007\u3007Oo, n.oO80(), n.OO0o\u3007\u3007());
                                                        this.\u3007o00\u3007\u3007Oo.\u300780\u3007808\u3007O(null);
                                                        this.\u3007o00\u3007\u3007Oo.\u300780\u3007808\u3007O((Closeable)b);
                                                        this.\u3007o00\u3007\u3007Oo.\u300780\u3007808\u3007O((Closeable)o2);
                                                        this.\u3007o00\u3007\u3007Oo.\u300780\u3007808\u3007O(n);
                                                        b.a.a.a.a.a.a.i.c.\u30078o8o\u3007("VideoPreload", "Pre finally ", this.\u3007o00\u3007\u3007Oo.\u3007o00\u3007\u3007Oo.m(), " Preload size=", this.\u3007o00\u3007\u3007Oo.\u3007o00\u3007\u3007Oo.j());
                                                        b.a.a.a.a.a.b.b.c.c.\u3007o00\u3007\u3007Oo(this.\u3007o00\u3007\u3007Oo.\u3007o00\u3007\u3007Oo);
                                                        return;
                                                    }
                                                    finally {
                                                        break Label_0703;
                                                    }
                                                }
                                                try {
                                                    final RandomAccessFile randomAccessFile = new RandomAccessFile(this.\u3007o00\u3007\u3007Oo.O8, "rw");
                                                    try {
                                                        final byte[] b2 = new byte[8192];
                                                        long n2 = 0L;
                                                        int off = 0;
                                                        while (true) {
                                                            final int read = ((InputStream)b).read(b2, off, 8192 - off);
                                                            if (read == -1) {
                                                                if (this.\u3007o00\u3007\u3007Oo.\u3007o00\u3007\u3007Oo.u() && \u300781 == this.\u3007o00\u3007\u3007Oo.O8.length()) {
                                                                    this.\u3007o00\u3007\u3007Oo.\u3007O8o08O();
                                                                }
                                                                final b \u3007o00\u3007\u3007Oo3 = this.\u3007o00\u3007\u3007Oo;
                                                                \u3007o00\u3007\u3007Oo3.OO0o\u3007\u3007(\u3007o00\u3007\u3007Oo3.\u3007o00\u3007\u3007Oo, n.oO80());
                                                                break Label_0753;
                                                            }
                                                            if (this.\u3007o00\u3007\u3007Oo.\u3007o\u3007) {
                                                                final b \u3007o00\u3007\u3007Oo4 = this.\u3007o00\u3007\u3007Oo;
                                                                \u3007o00\u3007\u3007Oo4.\u3007o\u3007(\u3007o00\u3007\u3007Oo4.\u3007o00\u3007\u3007Oo, n.oO80());
                                                                this.\u3007o00\u3007\u3007Oo.\u300780\u3007808\u3007O(randomAccessFile);
                                                                this.\u3007o00\u3007\u3007Oo.\u300780\u3007808\u3007O((Closeable)b);
                                                                this.\u3007o00\u3007\u3007Oo.\u300780\u3007808\u3007O((Closeable)o2);
                                                                this.\u3007o00\u3007\u3007Oo.\u300780\u3007808\u3007O(n);
                                                                b.a.a.a.a.a.a.i.c.\u30078o8o\u3007("VideoPreload", "Pre finally ", this.\u3007o00\u3007\u3007Oo.\u3007o00\u3007\u3007Oo.m(), " Preload size=", this.\u3007o00\u3007\u3007Oo.\u3007o00\u3007\u3007Oo.j());
                                                                b.a.a.a.a.a.b.b.c.c.\u3007o00\u3007\u3007Oo(this.\u3007o00\u3007\u3007Oo.\u3007o00\u3007\u3007Oo);
                                                                return;
                                                            }
                                                            final int n3 = off + read;
                                                            final long n4 = n2 + read;
                                                            final boolean b3 = n4 % 8192L == 0L || n4 == \u300781 - this.\u3007080;
                                                            long n5 = \u3007080;
                                                            off = n3;
                                                            if (b3) {
                                                                b.\u3007o00\u3007\u3007Oo(randomAccessFile, b2, \u3007080.intValue(), n3, this.\u3007o00\u3007\u3007Oo.\u3007o00\u3007\u3007Oo.e());
                                                                n5 = \u3007080 + n3;
                                                                off = 0;
                                                            }
                                                            \u3007080 = n5;
                                                            n2 = n4;
                                                        }
                                                    }
                                                    finally {
                                                        Object o3 = b;
                                                        b = (b.b.a.a.f.a.b)o;
                                                        o = randomAccessFile;
                                                        o3 = o2;
                                                    }
                                                }
                                                finally {}
                                            }
                                            o = b;
                                            oo08 = (Closeable)o2;
                                            break Label_0140;
                                        }
                                        finally {
                                            o2 = null;
                                            break Label_0859;
                                        }
                                        final b \u3007o00\u3007\u3007Oo5 = this.\u3007o00\u3007\u3007Oo;
                                        o2 = \u3007o00\u3007\u3007Oo5.\u3007o00\u3007\u3007Oo;
                                        try {
                                            \u3007o00\u3007\u3007Oo5.O8((b.a.a.a.a.a.a.f.c)o2, 601, "Network link failed.");
                                            final Closeable closeable = null;
                                            oo08 = null;
                                            o2 = null;
                                            this.\u3007o00\u3007\u3007Oo.\u300780\u3007808\u3007O(oo08);
                                            this.\u3007o00\u3007\u3007Oo.\u300780\u3007808\u3007O(closeable);
                                            this.\u3007o00\u3007\u3007Oo.\u300780\u3007808\u3007O((Closeable)o2);
                                            this.\u3007o00\u3007\u3007Oo.\u300780\u3007808\u3007O(n);
                                            b.a.a.a.a.a.a.i.c.\u30078o8o\u3007("VideoPreload", "Pre finally ", this.\u3007o00\u3007\u3007Oo.\u3007o00\u3007\u3007Oo.m(), " Preload size=", this.\u3007o00\u3007\u3007Oo.\u3007o00\u3007\u3007Oo.j());
                                            b.a.a.a.a.a.b.b.c.c.\u3007o00\u3007\u3007Oo(this.\u3007o00\u3007\u3007Oo.\u3007o00\u3007\u3007Oo);
                                        }
                                        finally {}
                                    }
                                }
                            }
                            o2 = o;
                            break Label_0859;
                        }
                        finally {}
                    }
                    o2 = null;
                    oo08 = null;
                }
                o = null;
                Object o3 = oo08;
                oo08 = (Closeable)o2;
                int oo10 = 601;
                try {
                    final Throwable t;
                    t.printStackTrace();
                    this.\u3007o00\u3007\u3007Oo.\u3007o00\u3007\u3007Oo();
                    final b \u3007o00\u3007\u3007Oo6 = this.\u3007o00\u3007\u3007Oo;
                    final b.a.a.a.a.a.a.f.c \u300782 = \u3007o00\u3007\u3007Oo6.\u3007o00\u3007\u3007Oo;
                    if (n != null) {
                        oo10 = n.oO80();
                    }
                    \u3007o00\u3007\u3007Oo6.O8(\u300782, oo10, t.getMessage());
                    this.\u3007o00\u3007\u3007Oo.\u300780\u3007808\u3007O((Closeable)o);
                    this.\u3007o00\u3007\u3007Oo.\u300780\u3007808\u3007O(oo08);
                    this.\u3007o00\u3007\u3007Oo.\u300780\u3007808\u3007O((Closeable)o3);
                    this.\u3007o00\u3007\u3007Oo.\u300780\u3007808\u3007O(n);
                    b.a.a.a.a.a.a.i.c.\u30078o8o\u3007("VideoPreload", "Pre finally ", this.\u3007o00\u3007\u3007Oo.\u3007o00\u3007\u3007Oo.m(), " Preload size=", this.\u3007o00\u3007\u3007Oo.\u3007o00\u3007\u3007Oo.j());
                    b.a.a.a.a.a.b.b.c.c.\u3007o00\u3007\u3007Oo(this.\u3007o00\u3007\u3007Oo.\u3007o00\u3007\u3007Oo);
                }
                finally {
                    this.\u3007o00\u3007\u3007Oo.\u300780\u3007808\u3007O((Closeable)o);
                    this.\u3007o00\u3007\u3007Oo.\u300780\u3007808\u3007O(oo08);
                    this.\u3007o00\u3007\u3007Oo.\u300780\u3007808\u3007O((Closeable)o3);
                    this.\u3007o00\u3007\u3007Oo.\u300780\u3007808\u3007O(n);
                    b.a.a.a.a.a.a.i.c.\u30078o8o\u3007("VideoPreload", "Pre finally ", this.\u3007o00\u3007\u3007Oo.\u3007o00\u3007\u3007Oo.m(), " Preload size=", this.\u3007o00\u3007\u3007Oo.\u3007o00\u3007\u3007Oo.j());
                    b.a.a.a.a.a.b.b.c.c.\u3007o00\u3007\u3007Oo(this.\u3007o00\u3007\u3007Oo.\u3007o00\u3007\u3007Oo);
                }
            }
            
            @Override
            public void \u3007o00\u3007\u3007Oo(final b.b.a.a.f.a.b b, final IOException ex) {
                final b \u3007o00\u3007\u3007Oo = this.\u3007o00\u3007\u3007Oo;
                \u3007o00\u3007\u3007Oo.O8(\u3007o00\u3007\u3007Oo.\u3007o00\u3007\u3007Oo, 601, ex.getMessage());
                b.a.a.a.a.a.b.b.c.c.\u3007o00\u3007\u3007Oo(this.\u3007o00\u3007\u3007Oo.\u3007o00\u3007\u3007Oo);
            }
        });
    }
    
    public void OO0o\u3007\u3007\u3007\u30070(final boolean \u3007o\u3007) {
        this.\u3007o\u3007 = \u3007o\u3007;
    }
    
    public void Oo08(final a.a a) {
        if (this.\u3007\u3007888) {
            synchronized (a.a.class) {
                this.o\u30070.add(a);
                return;
            }
        }
        this.o\u30070.add(a);
        if (this.OoO8()) {
            b.a.a.a.a.a.a.i.c.\u3007\u3007808\u3007("VideoPreload", "Cache file is exist");
            this.\u3007o00\u3007\u3007Oo.e(1);
            this.OO0o\u3007\u3007(this.\u3007o00\u3007\u3007Oo, 200);
            b.a.a.a.a.a.b.b.c.c.\u3007o00\u3007\u3007Oo(this.\u3007o00\u3007\u3007Oo);
            return;
        }
        this.\u3007\u3007888 = true;
        this.\u3007o00\u3007\u3007Oo.e(0);
        this.\u3007\u3007808\u3007();
    }
    
    public c \u3007O00() {
        return this.\u3007o00\u3007\u3007Oo;
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package b.a.a.a.a.a.b.c;

import java.util.Iterator;
import android.net.Uri;
import java.io.IOException;
import android.text.TextUtils;
import java.util.ArrayList;
import java.io.Reader;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import b.a.a.a.a.a.b.d.a;
import java.io.InputStream;
import java.util.List;

public class i
{
    public final c \u3007080;
    public final List<b> \u3007o00\u3007\u3007Oo;
    public final a \u3007o\u3007;
    
    public i(final c \u3007080, final List<b> \u3007o00\u3007\u3007Oo, final a \u3007o\u3007) {
        this.\u3007080 = \u3007080;
        this.\u3007o00\u3007\u3007Oo = \u3007o00\u3007\u3007Oo;
        this.\u3007o\u3007 = \u3007o\u3007;
    }
    
    public static i \u3007080(final InputStream in) throws IOException, d {
        final BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(in, b.a.a.a.a.a.b.d.a.\u3007o00\u3007\u3007Oo));
        final ArrayList list = new ArrayList();
        c \u3007080 = null;
        while (true) {
            final String line = bufferedReader.readLine();
            if (TextUtils.isEmpty((CharSequence)line)) {
                break;
            }
            final String trim = line.trim();
            if (\u3007080 == null) {
                \u3007080 = c.\u3007080(trim);
            }
            else {
                list.add(b.\u3007080(trim));
            }
        }
        if (\u3007080 != null) {
            return new i(\u3007080, list, a.\u3007080(\u3007080, list));
        }
        throw new d("request line is null");
    }
    
    static String \u3007o00\u3007\u3007Oo(final String s, final String s2, final List<String> list) {
        final StringBuilder sb = new StringBuilder(512);
        String \u3007o\u3007 = null;
        String s3;
        do {
            if (\u3007o\u3007 != null) {
                if (list.size() == 1) {
                    return null;
                }
                list.remove(list.size() - 1);
            }
            s3 = (\u3007o\u3007 = \u3007o\u3007(sb, s, s2, list));
        } while (s3.length() > 3072);
        return s3;
    }
    
    private static String \u3007o\u3007(final StringBuilder sb, final String s, final String s2, final List<String> list) {
        final int length = sb.length();
        int i = 0;
        sb.delete(0, length);
        sb.append("rk");
        sb.append("=");
        sb.append(Uri.encode(s));
        sb.append("&");
        sb.append("k");
        sb.append("=");
        sb.append(Uri.encode(s2));
        while (i < list.size()) {
            sb.append("&");
            sb.append("u");
            sb.append(i);
            sb.append("=");
            sb.append(Uri.encode((String)list.get(i)));
            ++i;
        }
        return sb.toString();
    }
    
    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append("Request{requestLine=");
        sb.append(this.\u3007080);
        sb.append(", headers=");
        sb.append(this.\u3007o00\u3007\u3007Oo);
        sb.append(", extra=");
        sb.append(this.\u3007o\u3007);
        sb.append('}');
        return sb.toString();
    }
    
    static final class d extends Exception
    {
        d(final String message) {
            super(message);
        }
    }
    
    static final class a
    {
        final int O8;
        final int Oo08;
        final List<String> o\u30070;
        final int \u3007080;
        final String \u3007o00\u3007\u3007Oo;
        final String \u3007o\u3007;
        
        private a(final int \u3007080, final String \u3007o00\u3007\u3007Oo, final String \u3007o\u3007, final int o8, final int oo08, final String s, final List<String> o\u30070) {
            this.\u3007080 = \u3007080;
            this.\u3007o00\u3007\u3007Oo = \u3007o00\u3007\u3007Oo;
            this.\u3007o\u3007 = \u3007o\u3007;
            this.O8 = o8;
            this.Oo08 = oo08;
            this.o\u30070 = o\u30070;
        }
        
        static a \u3007080(final c c, final List<b> list) throws d {
            final int index = c.\u3007o00\u3007\u3007Oo.indexOf("?");
            if (index == -1) {
                final StringBuilder sb = new StringBuilder();
                sb.append("path format error, path: ");
                sb.append(c.\u3007o00\u3007\u3007Oo);
                throw new d(sb.toString());
            }
            final ArrayList list2 = new ArrayList();
            final String[] split = c.\u3007o00\u3007\u3007Oo.substring(index + 1).split("&");
            final int length = split.length;
            final String s = null;
            String s2 = null;
            String s3 = null;
            int n = 0;
            int n2;
            String decode;
            String decode2;
            for (int i = 0; i < length; ++i, n = n2, s2 = decode, s3 = decode2) {
                final String[] split2 = split[i].split("=");
                if (split2.length != 2) {
                    n2 = n;
                    decode = s2;
                    decode2 = s3;
                }
                else if ("rk".equals(split2[0])) {
                    decode = Uri.decode(split2[1]);
                    n2 = n;
                    decode2 = s3;
                }
                else if ("k".equals(split2[0])) {
                    decode2 = Uri.decode(split2[1]);
                    n2 = n;
                    decode = s2;
                }
                else if (split2[0].startsWith("u")) {
                    list2.add(Uri.decode(split2[1]));
                    n2 = n;
                    decode = s2;
                    decode2 = s3;
                }
                else {
                    n2 = n;
                    decode = s2;
                    decode2 = s3;
                    if ("f".equals(split2[0])) {
                        n2 = n;
                        decode = s2;
                        decode2 = s3;
                        if (b.a.a.a.a.a.b.d.a.\u3007oo\u3007(split2[1]) == 1) {
                            n2 = 1;
                            decode2 = s3;
                            decode = s2;
                        }
                    }
                }
            }
            if (TextUtils.isEmpty((CharSequence)s2) || TextUtils.isEmpty((CharSequence)s3)) {
                final StringBuilder sb2 = new StringBuilder();
                sb2.append("rawKey or key is empty, path: ");
                sb2.append(c.\u3007o00\u3007\u3007Oo);
                throw new d(sb2.toString());
            }
            int int1;
            int int2;
            String \u3007o00\u3007\u3007Oo;
            if (list != null) {
                final Iterator<b> iterator = list.iterator();
                int1 = 0;
                int2 = 0;
                \u3007o00\u3007\u3007Oo = s;
                while (iterator.hasNext()) {
                    final b b = iterator.next();
                    if (b == null) {
                        continue;
                    }
                    if (!"Range".equalsIgnoreCase(b.\u3007080)) {
                        continue;
                    }
                    final int index2 = b.\u3007o00\u3007\u3007Oo.indexOf("=");
                    if (index2 == -1) {
                        final StringBuilder sb3 = new StringBuilder();
                        sb3.append("Range format error, Range: ");
                        sb3.append(b.\u3007o00\u3007\u3007Oo);
                        throw new d(sb3.toString());
                    }
                    if (!"bytes".equalsIgnoreCase(b.\u3007o00\u3007\u3007Oo.substring(0, index2).trim())) {
                        final StringBuilder sb4 = new StringBuilder();
                        sb4.append("Range format error, Range: ");
                        sb4.append(b.\u3007o00\u3007\u3007Oo);
                        throw new d(sb4.toString());
                    }
                    final String substring = b.\u3007o00\u3007\u3007Oo.substring(index2 + 1);
                    if (!substring.contains(",")) {
                        final int index3 = substring.indexOf("-");
                        if (index3 != -1) {
                            final String trim = substring.substring(0, index3).trim();
                            final String trim2 = substring.substring(index3 + 1).trim();
                            try {
                                if (trim.length() > 0) {
                                    int1 = Integer.parseInt(trim);
                                }
                                if (trim2.length() > 0) {
                                    int2 = Integer.parseInt(trim2);
                                    if (int1 > int2) {
                                        final StringBuilder sb5 = new StringBuilder();
                                        sb5.append("Range format error, Range: ");
                                        sb5.append(b.\u3007o00\u3007\u3007Oo);
                                        throw new d(sb5.toString());
                                    }
                                }
                                \u3007o00\u3007\u3007Oo = b.\u3007o00\u3007\u3007Oo;
                                continue;
                            }
                            catch (final NumberFormatException ex) {
                                final StringBuilder sb6 = new StringBuilder();
                                sb6.append("Range format error, Range: ");
                                sb6.append(b.\u3007o00\u3007\u3007Oo);
                                throw new d(sb6.toString());
                            }
                        }
                        final StringBuilder sb7 = new StringBuilder();
                        sb7.append("Range format error, Range: ");
                        sb7.append(b.\u3007o00\u3007\u3007Oo);
                        throw new d(sb7.toString());
                    }
                    final StringBuilder sb8 = new StringBuilder();
                    sb8.append("Range format error, Range: ");
                    sb8.append(b.\u3007o00\u3007\u3007Oo);
                    throw new d(sb8.toString());
                }
            }
            else {
                \u3007o00\u3007\u3007Oo = null;
                int2 = 0;
                int1 = 0;
            }
            if (!list2.isEmpty()) {
                return new a(n, s2, s3, int1, int2, \u3007o00\u3007\u3007Oo, list2);
            }
            final StringBuilder sb9 = new StringBuilder();
            sb9.append("no url found: path: ");
            sb9.append(c.\u3007o00\u3007\u3007Oo);
            throw new d(sb9.toString());
        }
        
        @Override
        public String toString() {
            final StringBuilder sb = new StringBuilder();
            sb.append("Extra{flag=");
            sb.append(this.\u3007080);
            sb.append(", rawKey='");
            sb.append(this.\u3007o00\u3007\u3007Oo);
            sb.append('\'');
            sb.append(", key='");
            sb.append(this.\u3007o\u3007);
            sb.append('\'');
            sb.append(", from=");
            sb.append(this.O8);
            sb.append(", to=");
            sb.append(this.Oo08);
            sb.append(", urls=");
            sb.append(this.o\u30070);
            sb.append('}');
            return sb.toString();
        }
    }
    
    public static final class b
    {
        public final String \u3007080;
        public final String \u3007o00\u3007\u3007Oo;
        
        public b(final String \u3007080, final String \u3007o00\u3007\u3007Oo) {
            this.\u3007080 = \u3007080;
            this.\u3007o00\u3007\u3007Oo = \u3007o00\u3007\u3007Oo;
        }
        
        static b \u3007080(final String s) throws d {
            final int index = s.indexOf(":");
            if (index == -1) {
                final StringBuilder sb = new StringBuilder();
                sb.append("request header format error, header: ");
                sb.append(s);
                throw new d(sb.toString());
            }
            final String trim = s.substring(0, index).trim();
            final String trim2 = s.substring(index + 1).trim();
            if (trim.length() != 0 && trim2.length() != 0) {
                return new b(trim, trim2);
            }
            final StringBuilder sb2 = new StringBuilder();
            sb2.append("request header format error, header: ");
            sb2.append(s);
            throw new d(sb2.toString());
        }
        
        @Override
        public String toString() {
            final StringBuilder sb = new StringBuilder();
            sb.append("Header{name='");
            sb.append(this.\u3007080);
            sb.append('\'');
            sb.append(", value='");
            sb.append(this.\u3007o00\u3007\u3007Oo);
            sb.append('\'');
            sb.append('}');
            return sb.toString();
        }
    }
    
    static final class c
    {
        final String \u3007080;
        final String \u3007o00\u3007\u3007Oo;
        final String \u3007o\u3007;
        
        private c(final String \u3007080, final String \u3007o00\u3007\u3007Oo, final String \u3007o\u3007) {
            this.\u3007080 = \u3007080;
            this.\u3007o00\u3007\u3007Oo = \u3007o00\u3007\u3007Oo;
            this.\u3007o\u3007 = \u3007o\u3007;
        }
        
        static c \u3007080(final String str) throws d {
            final int index = str.indexOf(32);
            if (index == -1) {
                final StringBuilder sb = new StringBuilder();
                sb.append("request line format error, line: ");
                sb.append(str);
                throw new d(sb.toString());
            }
            final int lastIndex = str.lastIndexOf(32);
            if (lastIndex <= index) {
                final StringBuilder sb2 = new StringBuilder();
                sb2.append("request line format error, line: ");
                sb2.append(str);
                throw new d(sb2.toString());
            }
            final String trim = str.substring(0, index).trim();
            final String trim2 = str.substring(index + 1, lastIndex).trim();
            final String trim3 = str.substring(lastIndex + 1).trim();
            if (trim.length() != 0 && trim2.length() != 0 && trim3.length() != 0) {
                return new c(trim, trim2, trim3);
            }
            final StringBuilder sb3 = new StringBuilder();
            sb3.append("request line format error, line: ");
            sb3.append(str);
            throw new d(sb3.toString());
        }
        
        @Override
        public String toString() {
            final StringBuilder sb = new StringBuilder();
            sb.append("RequestLine{method='");
            sb.append(this.\u3007080);
            sb.append('\'');
            sb.append(", path='");
            sb.append(this.\u3007o00\u3007\u3007Oo);
            sb.append('\'');
            sb.append(", version='");
            sb.append(this.\u3007o\u3007);
            sb.append('\'');
            sb.append('}');
            return sb.toString();
        }
    }
}

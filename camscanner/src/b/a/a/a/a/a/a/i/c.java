// 
// Decompiled by Procyon v0.6.0
// 

package b.a.a.a.a.a.a.i;

import android.text.TextUtils;

public class c
{
    private static boolean \u3007080 = false;
    private static int \u3007o00\u3007\u3007Oo = 4;
    private static String \u3007o\u3007 = "";
    
    public static void O8(final int \u3007o00\u3007\u3007Oo) {
        c.\u3007o00\u3007\u3007Oo = \u3007o00\u3007\u3007Oo;
    }
    
    public static void OO0o\u3007\u3007() {
        c.\u3007080 = true;
        O8(3);
    }
    
    public static void OO0o\u3007\u3007\u3007\u30070(final String s, final String s2, final Throwable t) {
        if (!c.\u3007080) {
            return;
        }
        if (s2 == null && t == null) {
            return;
        }
        if (c.\u3007o00\u3007\u3007Oo <= 6) {
            \u3007080(s);
        }
    }
    
    public static void Oo08(final String s, final String s2) {
        if (!c.\u3007080) {
            return;
        }
        if (s2 == null) {
            return;
        }
        if (c.\u3007o00\u3007\u3007Oo <= 3) {
            \u3007080(s);
        }
    }
    
    public static void Oooo8o0\u3007(final String \u3007o\u3007) {
        c.\u3007o\u3007 = \u3007o\u3007;
    }
    
    public static void oO80(final String s) {
        if (!c.\u3007080) {
            return;
        }
        \u3007\u3007808\u3007("Logger", s);
    }
    
    public static void o\u30070(final String s, final String s2, final Throwable t) {
        if (!c.\u3007080) {
            return;
        }
        if (s2 == null && t == null) {
            return;
        }
        if (c.\u3007o00\u3007\u3007Oo <= 3) {
            \u3007080(s);
        }
    }
    
    private static String \u3007080(final String str) {
        if (TextUtils.isEmpty((CharSequence)c.\u3007o\u3007)) {
            return str;
        }
        final StringBuilder sb = new StringBuilder();
        sb.append("[");
        sb.append(c.\u3007o\u3007);
        sb.append("]-[");
        sb.append(str);
        sb.append("]");
        return \u3007o00\u3007\u3007Oo(sb.toString());
    }
    
    public static void \u300780\u3007808\u3007O(final String s, final String s2) {
        if (!c.\u3007080) {
            return;
        }
        if (s2 == null) {
            return;
        }
        if (c.\u3007o00\u3007\u3007Oo <= 6) {
            \u3007080(s);
        }
    }
    
    public static void \u30078o8o\u3007(final String s, final Object... array) {
        if (!c.\u3007080) {
            return;
        }
        if (array == null) {
            return;
        }
        if (c.\u3007o00\u3007\u3007Oo <= 4) {
            \u3007080(s);
            \u3007o00\u3007\u3007Oo(array);
        }
    }
    
    public static boolean \u3007O8o08O() {
        return c.\u3007080;
    }
    
    public static void \u3007O\u3007(final String s, final String s2, final Throwable t) {
        if (!c.\u3007080) {
            return;
        }
        if (s2 == null && t == null) {
            return;
        }
        if (c.\u3007o00\u3007\u3007Oo <= 4) {
            \u3007080(s);
        }
    }
    
    private static String \u3007o00\u3007\u3007Oo(final Object... array) {
        if (array != null && array.length != 0) {
            final StringBuilder sb = new StringBuilder();
            for (final Object o : array) {
                if (o != null) {
                    sb.append(o.toString());
                }
                else {
                    sb.append(" null ");
                }
                sb.append(" ");
            }
            return sb.toString();
        }
        return "";
    }
    
    public static void \u3007o\u3007() {
        c.\u3007080 = false;
        O8(7);
    }
    
    public static void \u3007\u3007808\u3007(final String s, final String s2) {
        if (!c.\u3007080) {
            return;
        }
        if (s2 == null) {
            return;
        }
        if (c.\u3007o00\u3007\u3007Oo <= 4) {
            \u3007080(s);
        }
    }
    
    public static void \u3007\u3007888(final String s, final Object... array) {
        if (!c.\u3007080) {
            return;
        }
        if (array == null) {
            return;
        }
        if (c.\u3007o00\u3007\u3007Oo <= 3) {
            \u3007080(s);
            \u3007o00\u3007\u3007Oo(array);
        }
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package b.a.a.a.a.a.b.c;

import b.a.a.a.a.a.b.c.l.b;
import java.util.Collection;
import java.util.Set;
import com.bytedance.sdk.component.utils.m;
import b.a.a.a.a.a.b.c.l.c;
import android.annotation.SuppressLint;
import android.content.Context;

public class e
{
    @SuppressLint({ "StaticFieldLeak" })
    private static volatile Context O8;
    public static volatile Integer OO0o\u3007\u3007\u3007\u30070;
    public static volatile boolean Oo08;
    static volatile int oO80;
    static volatile boolean o\u30070;
    static volatile c \u3007080;
    public static volatile int \u300780\u3007808\u3007O;
    private static volatile b.a.a.a.a.a.b.c.m.c \u3007o00\u3007\u3007Oo;
    public static final boolean \u3007o\u3007;
    static volatile boolean \u3007\u3007888;
    
    static {
        \u3007o\u3007 = m.c();
        e.o\u30070 = true;
        e.oO80 = 0;
        e.\u300780\u3007808\u3007O = 3;
    }
    
    public static void O8(final boolean o\u30070) {
        e.o\u30070 = o\u30070;
    }
    
    public static Context Oo08() {
        return e.O8;
    }
    
    public static c oO80() {
        return e.\u3007080;
    }
    
    public static void o\u30070(final boolean \u3007\u3007888) {
        e.\u3007\u3007888 = \u3007\u3007888;
    }
    
    public static void \u3007o00\u3007\u3007Oo(final int oo80) {
        e.oO80 = oo80;
    }
    
    public static void \u3007o\u3007(final c \u3007080, final Context context) {
        if (\u3007080 == null || context == null) {
            throw new IllegalArgumentException("DiskLruCache and Context can't be null !!!");
        }
        e.O8 = context.getApplicationContext();
        if (e.\u3007080 != null) {
            return;
        }
        e.\u3007080 = \u3007080;
        e.\u3007o00\u3007\u3007Oo = b.a.a.a.a.a.b.c.m.c.O8(context);
        e.\u3007080.\u300780\u3007808\u3007O((c.f)new c.f() {
            @Override
            public void a(final String str) {
                if (e.\u3007o\u3007) {
                    final StringBuilder sb = new StringBuilder();
                    sb.append("new cache created: ");
                    sb.append(str);
                }
            }
            
            @Override
            public void \u3007080(final Set<String> obj) {
                e.\u3007o00\u3007\u3007Oo.\u3007\u3007888(obj, 0);
                if (e.\u3007o\u3007) {
                    final StringBuilder sb = new StringBuilder();
                    sb.append("cache file removed, ");
                    sb.append(obj);
                }
            }
        });
        final f \u30070\u3007O0088o = f.\u30070\u3007O0088o();
        \u30070\u3007O0088o.o\u30070(\u3007080);
        \u30070\u3007O0088o.\u3007\u3007888(e.\u3007o00\u3007\u3007Oo);
        final d \u3007o\u3007 = d.\u3007O\u3007();
        \u3007o\u3007.o\u30070(\u3007080);
        \u3007o\u3007.\u3007\u3007888(e.\u3007o00\u3007\u3007Oo);
    }
    
    public static b \u3007\u3007888() {
        return null;
    }
}

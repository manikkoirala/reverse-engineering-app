// 
// Decompiled by Procyon v0.6.0
// 

package b.a.a.a.a.a.a.f;

import android.os.Build$VERSION;
import android.text.TextUtils;
import org.json.JSONObject;
import java.util.HashMap;
import java.io.Serializable;

public class c implements Serializable
{
    private b a;
    private b b;
    private String c;
    private int d;
    private int e;
    private long f;
    private boolean g;
    private boolean h;
    public int i;
    private int j;
    private int k;
    public final HashMap<String, Object> l;
    private int m;
    private int n;
    private int o;
    private int p;
    public int q;
    private JSONObject r;
    
    public c(final String c, final b a, final b b, final int j, final int k) {
        this.j = 0;
        this.k = 0;
        this.l = new HashMap<String, Object>();
        this.m = 10000;
        this.n = 10000;
        this.o = 10000;
        this.p = 0;
        this.r = new JSONObject();
        this.c = c;
        this.a = a;
        this.b = b;
        this.j = j;
        this.k = k;
    }
    
    public Object a(final String key) {
        synchronized (this) {
            return this.l.get(key);
        }
    }
    
    public void a(final int m) {
        this.m = m;
    }
    
    public void a(final long f) {
        this.f = f;
    }
    
    public void a(final String key, final Object value) {
        synchronized (this) {
            this.l.put(key, value);
        }
    }
    
    public void a(final boolean g) {
        this.g = g;
    }
    
    public String b() {
        return this.c;
    }
    
    public void b(final int e) {
        this.e = e;
    }
    
    public void b(final String s) {
    }
    
    public int c() {
        return this.m;
    }
    
    public void c(final int n) {
        this.n = n;
    }
    
    public void c(final String c) {
        this.c = c;
    }
    
    public long d() {
        return this.f;
    }
    
    public void d(final int i) {
        this.i = i;
    }
    
    public void d(final String s) {
    }
    
    public String e() {
        if (this.t()) {
            return this.b.oo88o8O();
        }
        final b a = this.a;
        if (a != null) {
            return a.oo88o8O();
        }
        return null;
    }
    
    public void e(final int p) {
        this.p = p;
    }
    
    public void e(final String s) {
    }
    
    public int f() {
        return this.e;
    }
    
    public void f(final int d) {
        this.d = d;
    }
    
    public int g() {
        return this.r.optInt("pitaya_cache_size", 0);
    }
    
    public void g(final int o) {
        this.o = o;
    }
    
    public float h() {
        if (this.t()) {
            return this.b.o\u3007O8\u3007\u3007o();
        }
        final b a = this.a;
        if (a != null) {
            return a.o\u3007O8\u3007\u3007o();
        }
        return -1.0f;
    }
    
    public int i() {
        return this.j;
    }
    
    public int j() {
        if (this.t()) {
            return this.b.\u3007o();
        }
        final b a = this.a;
        if (a != null) {
            return a.\u3007o();
        }
        return 0;
    }
    
    public int k() {
        return this.n;
    }
    
    public int l() {
        return this.p;
    }
    
    public String m() {
        if (this.t()) {
            return this.b.o\u30078();
        }
        final b a = this.a;
        if (a != null) {
            return a.o\u30078();
        }
        return null;
    }
    
    public b n() {
        return this.a;
    }
    
    public b o() {
        return this.b;
    }
    
    public long p() {
        if (this.t()) {
            return this.b.O8\u3007o();
        }
        final b a = this.a;
        if (a != null) {
            return a.O8\u3007o();
        }
        return 0L;
    }
    
    public int q() {
        return this.d;
    }
    
    public int r() {
        return this.o;
    }
    
    public boolean s() {
        return this.h;
    }
    
    public boolean t() {
        final int k = this.k;
        boolean b = true;
        if (k == 1) {
            final b b2 = this.b;
            if (b2 != null && !TextUtils.isEmpty((CharSequence)b2.o\u30078())) {
                if (b.a.a.a.a.a.a.c.\u3007\u3007888() == 2) {
                    if (Build$VERSION.SDK_INT >= 26) {
                        return b;
                    }
                }
                else if (this.j == 1) {
                    return b;
                }
            }
        }
        b = false;
        return b;
    }
    
    public boolean u() {
        if (this.t()) {
            return this.b.o8();
        }
        final b a = this.a;
        return a == null || a.o8();
    }
    
    public boolean v() {
        return this.g;
    }
}

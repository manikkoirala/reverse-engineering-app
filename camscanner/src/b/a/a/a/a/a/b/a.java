// 
// Decompiled by Procyon v0.6.0
// 

package b.a.a.a.a.a.b;

import android.os.Build$VERSION;
import android.content.Context;
import b.a.a.a.a.a.a.i.c;
import org.json.JSONObject;
import b.a.a.a.a.a.a.d.b;

public class a
{
    public static int O8 = 10;
    public static int Oo08 = 10;
    private static b \u3007080;
    public static int \u3007o00\u3007\u3007Oo = 10;
    public static int \u3007o\u3007 = 10;
    
    public static void O8(final JSONObject jsonObject) {
        if (jsonObject == null) {
            return;
        }
        try {
            a.\u3007o00\u3007\u3007Oo = jsonObject.optInt("splash", 10);
            a.\u3007o\u3007 = jsonObject.optInt("reward", 10);
            a.O8 = jsonObject.optInt("brand", 10);
            final int n = a.Oo08 = jsonObject.optInt("other", 10);
            if (a.\u3007o00\u3007\u3007Oo < 0) {
                a.\u3007o00\u3007\u3007Oo = 10;
            }
            if (a.\u3007o\u3007 < 0) {
                a.\u3007o\u3007 = 10;
            }
            if (a.O8 < 0) {
                a.O8 = 10;
            }
            if (n < 0) {
                a.Oo08 = 10;
            }
            c.\u30078o8o\u3007("MediaConfig", "splash=", a.\u3007o00\u3007\u3007Oo, ",reward=", a.\u3007o\u3007, ",brand=", a.O8, ",other=", a.Oo08);
        }
        finally {
            final Throwable t;
            c.\u300780\u3007808\u3007O("MediaConfig", t.getMessage());
        }
    }
    
    public static int Oo08() {
        return a.O8;
    }
    
    public static int oO80() {
        return a.\u3007o00\u3007\u3007Oo;
    }
    
    public static int o\u30070() {
        return a.Oo08;
    }
    
    public static void \u3007080() {
        final b \u3007080 = a.\u3007080;
        if (\u3007080 != null) {
            \u3007080.c();
        }
    }
    
    public static void \u3007o00\u3007\u3007Oo(final Context context) {
        b.a.a.a.a.a.a.i.a.\u3007080(context);
        if (Build$VERSION.SDK_INT < 23) {
            b.a.a.a.a.a.b.c.q.a.\u3007o\u3007();
        }
    }
    
    public static void \u3007o\u3007(final b \u3007080) {
        a.\u3007080 = \u3007080;
    }
    
    public static int \u3007\u3007888() {
        return a.\u3007o\u3007;
    }
}

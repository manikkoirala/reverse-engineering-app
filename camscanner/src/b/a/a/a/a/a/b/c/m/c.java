// 
// Decompiled by Procyon v0.6.0
// 

package b.a.a.a.a.a.b.c.m;

import android.database.sqlite.SQLiteProgram;
import android.database.sqlite.SQLiteDatabase;
import java.util.Iterator;
import java.util.Collection;
import android.database.Cursor;
import android.text.TextUtils;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import b.b.a.a.k.h;
import java.util.concurrent.LinkedBlockingDeque;
import java.util.concurrent.TimeUnit;
import android.content.Context;
import java.util.concurrent.Executor;
import java.util.Map;
import android.util.SparseArray;
import android.database.sqlite.SQLiteStatement;

public class c
{
    private static volatile c Oo08;
    private volatile SQLiteStatement O8;
    private final SparseArray<Map<String, a>> \u3007080;
    private final d \u3007o00\u3007\u3007Oo;
    private final Executor \u3007o\u3007;
    
    private c(final Context context) {
        final SparseArray \u3007080 = new SparseArray(2);
        this.\u3007080 = (SparseArray<Map<String, a>>)\u3007080;
        this.\u3007o\u3007 = new ThreadPoolExecutor(0, 1, 60L, TimeUnit.SECONDS, new LinkedBlockingDeque<Runnable>(), new h(5, "video_proxy_db"));
        this.\u3007o00\u3007\u3007Oo = new d(context.getApplicationContext());
        \u3007080.put(0, (Object)new ConcurrentHashMap());
        \u3007080.put(1, (Object)new ConcurrentHashMap());
    }
    
    public static c O8(final Context context) {
        if (c.Oo08 == null) {
            synchronized (c.class) {
                if (c.Oo08 == null) {
                    c.Oo08 = new c(context);
                }
            }
        }
        return c.Oo08;
    }
    
    private String Oo08(final int n) {
        if (n <= 0) {
            return "";
        }
        final StringBuilder sb = new StringBuilder(n << 1);
        sb.append("?");
        for (int i = 1; i < n; ++i) {
            sb.append(",?");
        }
        return sb.toString();
    }
    
    public void o\u30070(final a a) {
        if (a != null) {
            final Map map = (Map)this.\u3007080.get(a.O8);
            if (map != null) {
                map.put(a.\u3007080, a);
            }
            this.\u3007o\u3007.execute(new Runnable(this, a) {
                final a o0;
                final c \u3007OOo8\u30070;
                
                @Override
                public void run() {
                    try {
                        if (this.\u3007OOo8\u30070.O8 == null) {
                            final c \u3007oOo8\u30070 = this.\u3007OOo8\u30070;
                            \u3007oOo8\u30070.O8 = \u3007oOo8\u30070.\u3007o00\u3007\u3007Oo.getWritableDatabase().compileStatement("INSERT INTO video_http_header_t (key,mime,contentLength,flag,extra) VALUES(?,?,?,?,?)");
                        }
                        else {
                            ((SQLiteProgram)this.\u3007OOo8\u30070.O8).clearBindings();
                        }
                        ((SQLiteProgram)this.\u3007OOo8\u30070.O8).bindString(1, this.o0.\u3007080);
                        ((SQLiteProgram)this.\u3007OOo8\u30070.O8).bindString(2, this.o0.\u3007o00\u3007\u3007Oo);
                        ((SQLiteProgram)this.\u3007OOo8\u30070.O8).bindLong(3, (long)this.o0.\u3007o\u3007);
                        ((SQLiteProgram)this.\u3007OOo8\u30070.O8).bindLong(4, (long)this.o0.O8);
                        ((SQLiteProgram)this.\u3007OOo8\u30070.O8).bindString(5, this.o0.Oo08);
                        this.\u3007OOo8\u30070.O8.executeInsert();
                    }
                    finally {}
                }
            });
        }
    }
    
    public void \u300780\u3007808\u3007O(final int n) {
        final Map map = (Map)this.\u3007080.get(n);
        if (map != null) {
            map.clear();
        }
        this.\u3007o\u3007.execute(new Runnable(this, n) {
            final int o0;
            final c \u3007OOo8\u30070;
            
            @Override
            public void run() {
                try {
                    this.\u3007OOo8\u30070.\u3007o00\u3007\u3007Oo.getWritableDatabase().delete("video_http_header_t", "flag=?", new String[] { String.valueOf(this.o0) });
                }
                finally {}
            }
        });
    }
    
    public a \u3007o\u3007(final String s, final int i) {
        if (TextUtils.isEmpty((CharSequence)s)) {
            return null;
        }
        final Map map = (Map)this.\u3007080.get(i);
        a a;
        if (map == null) {
            a = null;
        }
        else {
            a = map.get(s);
        }
        if (a != null) {
            return a;
        }
        try {
            final Cursor query = this.\u3007o00\u3007\u3007Oo.getReadableDatabase().query("video_http_header_t", (String[])null, "key=? AND flag=?", new String[] { s, String.valueOf(i) }, (String)null, (String)null, (String)null, "1");
            a a2 = a;
            if (query != null) {
                a2 = a;
                if (query.getCount() > 0) {
                    a2 = a;
                    if (query.moveToNext()) {
                        a2 = new a(query.getString(query.getColumnIndex("key")), query.getString(query.getColumnIndex("mime")), query.getInt(query.getColumnIndex("contentLength")), i, query.getString(query.getColumnIndex("extra")));
                    }
                }
                query.close();
            }
            if (a2 != null && map != null) {
                map.put(s, a2);
            }
            return a2;
        }
        finally {
            return null;
        }
    }
    
    public void \u3007\u3007888(final Collection<String> collection, final int i) {
        if (collection == null || collection.isEmpty()) {
            return;
        }
        final int n = collection.size() + 1;
        final String[] array = new String[n];
        final Map map = (Map)this.\u3007080.get(i);
        final Iterator iterator = collection.iterator();
        int n2 = -1;
        while (iterator.hasNext()) {
            final String s = (String)iterator.next();
            if (map != null) {
                map.remove(s);
            }
            ++n2;
            array[n2] = s;
        }
        array[n2 + 1] = String.valueOf(i);
        try {
            final SQLiteDatabase writableDatabase = this.\u3007o00\u3007\u3007Oo.getWritableDatabase();
            final StringBuilder sb = new StringBuilder();
            sb.append("key IN(");
            sb.append(this.Oo08(n));
            sb.append(") AND ");
            sb.append("flag");
            sb.append("=?");
            writableDatabase.delete("video_http_header_t", sb.toString(), array);
        }
        finally {}
    }
}

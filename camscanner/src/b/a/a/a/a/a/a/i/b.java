// 
// Decompiled by Procyon v0.6.0
// 

package b.a.a.a.a.a.a.i;

import java.security.NoSuchAlgorithmException;
import java.nio.charset.Charset;
import android.text.TextUtils;
import java.security.MessageDigest;

public class b
{
    private static final MessageDigest \u3007080;
    private static final char[] \u3007o00\u3007\u3007Oo;
    
    static {
        \u3007080 = \u3007o\u3007();
        \u3007o00\u3007\u3007Oo = new char[] { '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F' };
    }
    
    private b() {
    }
    
    public static String \u3007080(final String s) {
        final MessageDigest \u3007080 = b.\u3007080;
        if (\u3007080 != null) {
            if (!TextUtils.isEmpty((CharSequence)s)) {
                final byte[] bytes = s.getBytes(Charset.forName("UTF-8"));
                synchronized (b.class) {
                    final byte[] digest = \u3007080.digest(bytes);
                    monitorexit(b.class);
                    return \u3007o00\u3007\u3007Oo(digest);
                }
            }
        }
        return "";
    }
    
    public static String \u3007o00\u3007\u3007Oo(final byte[] array) {
        if (array != null && array.length != 0) {
            final char[] value = new char[array.length << 1];
            final int length = array.length;
            int i = 0;
            int n = 0;
            while (i < length) {
                final byte b = array[i];
                final int n2 = n + 1;
                final char[] \u3007o00\u3007\u3007Oo = b.a.a.a.a.a.a.i.b.\u3007o00\u3007\u3007Oo;
                value[n] = \u3007o00\u3007\u3007Oo[(b & 0xF0) >> 4];
                n = n2 + 1;
                value[n2] = \u3007o00\u3007\u3007Oo[b & 0xF];
                ++i;
            }
            return new String(value);
        }
        return null;
    }
    
    private static MessageDigest \u3007o\u3007() {
        try {
            return MessageDigest.getInstance("md5");
        }
        catch (final NoSuchAlgorithmException ex) {
            return null;
        }
    }
}

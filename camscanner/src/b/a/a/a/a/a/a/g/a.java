// 
// Decompiled by Procyon v0.6.0
// 

package b.a.a.a.a.a.a.g;

import android.view.View;
import android.view.SurfaceHolder;
import android.graphics.SurfaceTexture;

public interface a
{
    void a(final b p0, final int p1);
    
    void a(final b p0, final int p1, final boolean p2);
    
    void a(final b p0, final SurfaceTexture p1);
    
    void a(final b p0, final SurfaceHolder p1);
    
    void a(final b p0, final SurfaceHolder p1, final int p2, final int p3, final int p4);
    
    void a(final b p0, final View p1);
    
    void a(final b p0, final View p1, final boolean p2, final boolean p3);
    
    void b(final b p0, final int p1);
    
    void b(final b p0, final SurfaceTexture p1);
    
    void b(final b p0, final SurfaceHolder p1);
    
    void b(final b p0, final View p1);
    
    void c(final b p0, final View p1);
    
    void d(final b p0, final View p1);
    
    void e(final b p0, final View p1);
    
    void f(final b p0, final View p1);
}

// 
// Decompiled by Procyon v0.6.0
// 

package b.a.a.a.a.a.b.c.q;

import java.util.ArrayList;
import com.bytedance.sdk.component.utils.m;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.Queue;
import b.a.a.a.a.a.b.c.d;
import b.a.a.a.a.a.b.c.e;
import b.a.a.a.a.a.b.c.f;
import android.text.TextUtils;
import java.io.IOException;
import java.io.File;
import java.util.HashMap;
import b.a.a.a.a.a.a.i.c;

public class a
{
    private b \u3007080;
    
    static {
        b.a.a.a.a.a.a.i.c.\u3007O8o08O();
    }
    
    private a() {
        new HashMap();
        this.\u3007080();
    }
    
    private static b.a.a.a.a.a.b.c.l.c Oo08() {
        final File file = new File(b.a.a.a.a.a.a.c.\u3007080().getCacheDir(), "proxy_cache");
        if (!file.exists()) {
            file.mkdirs();
        }
        b.a.a.a.a.a.b.c.l.c c;
        try {
            c = new b.a.a.a.a.a.b.c.l.c(file);
            try {
                c.oO80(104857600L);
            }
            catch (final IOException ex) {}
        }
        catch (final IOException ex) {
            c = null;
        }
        final IOException ex;
        ex.printStackTrace();
        return c;
    }
    
    public static a \u3007o\u3007() {
        return c.\u3007080;
    }
    
    public String O8(final b.a.a.a.a.a.a.f.c c) {
        if (c == null) {
            return null;
        }
        final boolean b = TextUtils.isEmpty((CharSequence)c.e()) ^ true;
        String s;
        if (b) {
            s = c.e();
        }
        else {
            s = c.m();
        }
        return f.\u30070\u3007O0088o().\u3007o\u3007(false, b, s, c.m());
    }
    
    public boolean \u3007080() {
        if (this.\u3007080 != null) {
            return true;
        }
        final b.a.a.a.a.a.b.c.l.c oo08 = Oo08();
        if (oo08 == null) {
            return false;
        }
        e.O8(true);
        e.o\u30070(true);
        e.\u3007o00\u3007\u3007Oo(1);
        f.\u30070\u3007O0088o().\u300700();
        try {
            (this.\u3007080 = new b()).setName("csj_video_cache_preloader");
            this.\u3007080.start();
            e.\u3007o\u3007(oo08, b.a.a.a.a.a.a.c.\u3007080());
            d.\u3007O\u3007().Oo08(30000L, 30000L, 30000L);
            d.\u3007O\u3007().O8(10485759);
            return true;
        }
        catch (final Exception ex) {
            return false;
        }
    }
    
    public boolean \u3007o00\u3007\u3007Oo(final b.a.a.a.a.a.a.f.c c) {
        if (this.\u3007080()) {
            this.\u3007080.\u3007o\u3007(c);
            return true;
        }
        return false;
    }
    
    class b extends Thread
    {
        private final Queue<a> OO;
        private Queue<a> o0;
        private Queue<a> \u300708O\u300700\u3007o;
        private boolean \u3007OOo8\u30070;
        
        public b(final b.a.a.a.a.a.b.c.q.a a) {
            this.OO = new ArrayBlockingQueue<a>(10);
            this.o0 = new LinkedBlockingQueue<a>();
            this.\u3007OOo8\u30070 = true;
            this.\u300708O\u300700\u3007o = new LinkedBlockingQueue<a>();
        }
        
        private void O8(final a a) {
            synchronized (this) {
                this.\u3007o00\u3007\u3007Oo();
                this.\u300708O\u300700\u3007o.add(a);
                this.notify();
            }
        }
        
        private void Oo08() {
        }
        
        private void oO80(final a a) {
            this.Oo08();
            a.\u3007o\u3007 = null;
            a.\u3007o00\u3007\u3007Oo = null;
            a.\u3007080 = -1;
            a.o\u30070 = null;
            this.OO.offer(a);
        }
        
        private void o\u30070(final a a) {
            this.Oo08();
            if (a == null) {
                return;
            }
            this.o0.offer(a);
            this.notify();
        }
        
        private a \u3007080(final int \u3007080, final b.a.a.a.a.a.a.f.c o\u30070) {
            this.\u3007o00\u3007\u3007Oo();
            final StringBuilder sb = new StringBuilder();
            sb.append("pool: ");
            sb.append(this.OO.size());
            m.a("VideoCachePreloader", sb.toString());
            a a;
            if ((a = this.OO.poll()) == null) {
                a = new a();
            }
            a.\u3007080 = \u3007080;
            a.o\u30070 = o\u30070;
            return a;
        }
        
        private void \u3007o00\u3007\u3007Oo() {
        }
        
        private void \u3007\u3007888() {
            this.Oo08();
            while (true) {
                final a a = this.\u300708O\u300700\u3007o.poll();
                if (a == null) {
                    break;
                }
                a.\u3007o00\u3007\u3007Oo = a.o\u30070.m();
                a.\u3007o\u3007 = new String[] { a.o\u30070.m() };
                int o8 = a.o\u30070.g();
                if (o8 <= 0) {
                    o8 = a.o\u30070.j();
                }
                a.O8 = o8;
                a.Oo08 = a.o\u30070.e();
                if (!TextUtils.isEmpty((CharSequence)a.o\u30070.e())) {
                    a.\u3007o00\u3007\u3007Oo = a.o\u30070.e();
                }
                a.o\u30070 = null;
                this.o\u30070(a);
            }
        }
        
        @Override
        public void run() {
            while (this.\u3007OOo8\u30070) {
                synchronized (this) {
                    if (!this.\u300708O\u300700\u3007o.isEmpty()) {
                        this.\u3007\u3007888();
                    }
                    while (!this.o0.isEmpty()) {
                        final a a = this.o0.poll();
                        if (a == null) {
                            continue;
                        }
                        final int \u3007080 = a.\u3007080;
                        int i = 0;
                        if (\u3007080 != 0) {
                            if (\u3007080 != 1) {
                                if (\u3007080 != 2) {
                                    if (\u3007080 != 3) {
                                        if (\u3007080 == 4) {
                                            d.\u3007O\u3007().\u3007o\u3007();
                                            this.\u3007OOo8\u30070 = false;
                                        }
                                    }
                                    else {
                                        d.\u3007O\u3007().\u3007o\u3007();
                                        e.\u3007\u3007888();
                                        if (e.oO80() != null) {
                                            e.oO80().\u30078o8o\u3007();
                                        }
                                    }
                                }
                                else {
                                    d.\u3007O\u3007().\u3007o\u3007();
                                }
                            }
                            else {
                                d.\u3007O\u3007().oO80(a.\u3007o00\u3007\u3007Oo);
                            }
                        }
                        else {
                            final String[] \u3007o\u3007 = a.\u3007o\u3007;
                            if (\u3007o\u3007 != null && \u3007o\u3007.length > 0) {
                                final ArrayList<String> list = new ArrayList<String>();
                                for (String[] \u3007o\u30072 = a.\u3007o\u3007; i < \u3007o\u30072.length; ++i) {
                                    final String e = \u3007o\u30072[i];
                                    if (b.a.a.a.a.a.b.d.a.\u3007\u30078O0\u30078(e)) {
                                        list.add(e);
                                    }
                                }
                                d.\u3007O\u3007().\u30078o8o\u3007(false, TextUtils.isEmpty((CharSequence)a.Oo08) ^ true, a.O8, a.\u3007o00\u3007\u3007Oo, (String[])list.toArray(new String[list.size()]));
                            }
                        }
                        this.oO80(a);
                    }
                    try {
                        this.wait();
                    }
                    catch (final InterruptedException ex) {
                        ex.printStackTrace();
                    }
                    continue;
                }
                break;
            }
        }
        
        public void \u3007o\u3007(final b.a.a.a.a.a.a.f.c c) {
            this.O8(this.\u3007080(0, c));
        }
        
        private class a
        {
            public int O8;
            public String Oo08;
            public b.a.a.a.a.a.a.f.c o\u30070;
            public int \u3007080;
            public String \u3007o00\u3007\u3007Oo;
            public String[] \u3007o\u3007;
            
            public a(final b b) {
            }
        }
    }
    
    private static class c
    {
        private static final a \u3007080;
        
        static {
            \u3007080 = new a(null);
        }
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package b.a.a.a.a.a.a.g;

import android.view.View;
import android.content.Context;
import java.lang.ref.WeakReference;
import android.graphics.drawable.Drawable;
import b.a.a.a.a.a.a.e.a;

public interface b<T> extends a
{
    void a(final Drawable p0);
    
    void a(final T p0, final WeakReference<Context> p1, final boolean p2);
    
    void a(final boolean p0);
    
    void c();
    
    void e();
    
    View getMediaView();
}

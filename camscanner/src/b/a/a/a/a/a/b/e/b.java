// 
// Decompiled by Procyon v0.6.0
// 

package b.a.a.a.a.a.b.e;

import java.lang.reflect.Field;
import java.lang.reflect.AccessibleObject;
import java.lang.ref.WeakReference;
import android.view.SurfaceHolder;
import android.text.TextUtils;
import android.net.Uri;
import com.bytedance.component.sdk.annotation.RequiresApi;
import android.media.MediaDataSource;
import android.annotation.TargetApi;
import java.io.FileDescriptor;
import java.lang.reflect.AnnotatedElement;
import android.os.Handler;
import android.content.Context;
import android.os.Build$VERSION;
import android.media.MediaPlayer$OnInfoListener;
import android.media.MediaPlayer$OnErrorListener;
import android.media.MediaPlayer$OnVideoSizeChangedListener;
import android.media.MediaPlayer$OnSeekCompleteListener;
import android.media.MediaPlayer$OnCompletionListener;
import android.media.MediaPlayer$OnBufferingUpdateListener;
import android.media.MediaPlayer$OnPreparedListener;
import b.a.a.a.a.a.a.i.c;
import android.view.Surface;
import android.media.MediaPlayer;

public class b extends b.a.a.a.a.a.b.e.a
{
    private final Object OO0o\u3007\u3007;
    private final a OO0o\u3007\u3007\u3007\u30070;
    private volatile boolean Oooo8o0\u3007;
    private final MediaPlayer \u300780\u3007808\u3007O;
    private b.a.a.a.a.a.b.b.a \u30078o8o\u3007;
    private Surface \u3007O8o08O;
    
    public b() {
        final Object oo0o\u3007\u3007 = new Object();
        synchronized (this.OO0o\u3007\u3007 = oo0o\u3007\u3007) {
            final MediaPlayer \u300780\u3007808\u3007O = new MediaPlayer();
            this.\u300780\u3007808\u3007O = \u300780\u3007808\u3007O;
            monitorexit(oo0o\u3007\u3007);
            this.\u30070\u3007O0088o(\u300780\u3007808\u3007O);
            try {
                \u300780\u3007808\u3007O.setAudioStreamType(3);
            }
            finally {
                c.OO0o\u3007\u3007\u3007\u30070("CSJ_VIDEO", "setAudioStreamType error: ", (Throwable)oo0o\u3007\u3007);
            }
            this.OO0o\u3007\u3007\u3007\u30070 = new a(this);
            this.OoO8();
        }
    }
    
    private void OoO8() {
        this.\u300780\u3007808\u3007O.setOnPreparedListener((MediaPlayer$OnPreparedListener)this.OO0o\u3007\u3007\u3007\u30070);
        this.\u300780\u3007808\u3007O.setOnBufferingUpdateListener((MediaPlayer$OnBufferingUpdateListener)this.OO0o\u3007\u3007\u3007\u30070);
        this.\u300780\u3007808\u3007O.setOnCompletionListener((MediaPlayer$OnCompletionListener)this.OO0o\u3007\u3007\u3007\u30070);
        this.\u300780\u3007808\u3007O.setOnSeekCompleteListener((MediaPlayer$OnSeekCompleteListener)this.OO0o\u3007\u3007\u3007\u30070);
        this.\u300780\u3007808\u3007O.setOnVideoSizeChangedListener((MediaPlayer$OnVideoSizeChangedListener)this.OO0o\u3007\u3007\u3007\u30070);
        this.\u300780\u3007808\u3007O.setOnErrorListener((MediaPlayer$OnErrorListener)this.OO0o\u3007\u3007\u3007\u30070);
        this.\u300780\u3007808\u3007O.setOnInfoListener((MediaPlayer$OnInfoListener)this.OO0o\u3007\u3007\u3007\u30070);
    }
    
    private void o800o8O() {
        if (Build$VERSION.SDK_INT >= 23) {
            final b.a.a.a.a.a.b.b.a \u30078o8o\u3007 = this.\u30078o8o\u3007;
            if (\u30078o8o\u3007 != null) {
                try {
                    \u30078o8o\u3007.close();
                }
                finally {
                    final Throwable t;
                    c.OO0o\u3007\u3007\u3007\u30070("CSJ_VIDEO", "releaseMediaDataSource error: ", t);
                }
                this.\u30078o8o\u3007 = null;
            }
        }
    }
    
    private void \u30070\u3007O0088o(final MediaPlayer obj) {
        if (Build$VERSION.SDK_INT >= 28) {
            return;
        }
        try {
            final Class<?> forName = Class.forName("android.media.MediaTimeProvider");
            final Class<?> forName2 = Class.forName("android.media.SubtitleController");
            final Class<?> forName3 = Class.forName("android.media.SubtitleController$Anchor");
            final Object instance = forName2.getConstructor(Context.class, forName, Class.forName("android.media.SubtitleController$Listener")).newInstance(b.a.a.a.a.a.a.c.\u3007080(), null, null);
            AnnotatedElement annotatedElement = forName2.getDeclaredField("mHandler");
            ((AccessibleObject)annotatedElement).setAccessible(true);
            try {
                ((Field)annotatedElement).set(instance, new Handler());
                ((AccessibleObject)annotatedElement).setAccessible(false);
                annotatedElement = obj.getClass();
                ((Class)annotatedElement).getMethod("setSubtitleAnchor", forName2, forName3).invoke(obj, instance, null);
            }
            finally {
                try {
                    final Throwable t;
                    c.OO0o\u3007\u3007\u3007\u30070("CSJ_VIDEO", "subtitleInstance error: ", t);
                }
                finally {
                    ((AccessibleObject)annotatedElement).setAccessible(false);
                }
            }
        }
        finally {
            final Throwable t2;
            c.OO0o\u3007\u3007\u3007\u30070("CSJ_VIDEO", "setSubtitleController error: ", t2);
        }
    }
    
    private void \u3007O888o0o() {
        try {
            final Surface \u3007o8o08O = this.\u3007O8o08O;
            if (\u3007o8o08O != null) {
                \u3007o8o08O.release();
                this.\u3007O8o08O = null;
            }
        }
        finally {}
    }
    
    @Override
    public void OO0o\u3007\u3007\u3007\u30070(final FileDescriptor dataSource) throws Throwable {
        this.\u300780\u3007808\u3007O.setDataSource(dataSource);
    }
    
    @Override
    public long a() {
        try {
            return this.\u300780\u3007808\u3007O.getDuration();
        }
        finally {
            final Throwable t;
            c.OO0o\u3007\u3007\u3007\u30070("CSJ_VIDEO", "getDuration error: ", t);
            return 0L;
        }
    }
    
    @Override
    public void a(final long n, final int n2) throws Throwable {
        if (Build$VERSION.SDK_INT >= 26) {
            if (n2 != 0) {
                if (n2 != 1) {
                    if (n2 != 2) {
                        if (n2 != 3) {
                            this.\u300780\u3007808\u3007O.seekTo((int)n);
                        }
                        else {
                            b.a.a.a.a.a.b.e.\u3007080.\u3007080(this.\u300780\u3007808\u3007O, (long)(int)n, 3);
                        }
                    }
                    else {
                        b.a.a.a.a.a.b.e.\u3007080.\u3007080(this.\u300780\u3007808\u3007O, (long)(int)n, 2);
                    }
                }
                else {
                    b.a.a.a.a.a.b.e.\u3007080.\u3007080(this.\u300780\u3007808\u3007O, (long)(int)n, 1);
                }
            }
            else {
                b.a.a.a.a.a.b.e.\u3007080.\u3007080(this.\u300780\u3007808\u3007O, (long)(int)n, 0);
            }
        }
        else {
            this.\u300780\u3007808\u3007O.seekTo((int)n);
        }
    }
    
    @TargetApi(14)
    @Override
    public void a(final Surface surface) {
        this.\u3007O888o0o();
        this.\u3007O8o08O = surface;
        this.\u300780\u3007808\u3007O.setSurface(surface);
    }
    
    @RequiresApi(api = 23)
    @Override
    public void a(final b.a.a.a.a.a.a.f.c c) {
        synchronized (this) {
            this.\u30078o8o\u3007 = b.a.a.a.a.a.b.b.a.\u3007\u3007888(b.a.a.a.a.a.a.c.\u3007080(), c);
            b.a.a.a.a.a.b.b.c.c.\u3007o00\u3007\u3007Oo(c);
            b.a.a.a.a.a.b.e.\u3007o00\u3007\u3007Oo.\u3007080(this.\u300780\u3007808\u3007O, (MediaDataSource)this.\u30078o8o\u3007);
        }
    }
    
    @Override
    public void a(final String dataSource) throws Throwable {
        final Uri parse = Uri.parse(dataSource);
        final String scheme = parse.getScheme();
        if (!TextUtils.isEmpty((CharSequence)scheme) && scheme.equalsIgnoreCase("file")) {
            this.\u300780\u3007808\u3007O.setDataSource(parse.getPath());
        }
        else {
            this.\u300780\u3007808\u3007O.setDataSource(dataSource);
        }
    }
    
    @Override
    public void a(final boolean screenOnWhilePlaying) throws Throwable {
        this.\u300780\u3007808\u3007O.setScreenOnWhilePlaying(screenOnWhilePlaying);
    }
    
    @Override
    public int b() {
        final MediaPlayer \u300780\u3007808\u3007O = this.\u300780\u3007808\u3007O;
        if (\u300780\u3007808\u3007O != null) {
            return \u300780\u3007808\u3007O.getVideoWidth();
        }
        return 0;
    }
    
    @Override
    public void b(final boolean looping) throws Throwable {
        this.\u300780\u3007808\u3007O.setLooping(looping);
    }
    
    @Override
    public int c() {
        final MediaPlayer \u300780\u3007808\u3007O = this.\u300780\u3007808\u3007O;
        if (\u300780\u3007808\u3007O != null) {
            return \u300780\u3007808\u3007O.getVideoHeight();
        }
        return 0;
    }
    
    @Override
    public void c(final boolean b) throws Throwable {
        final MediaPlayer \u300780\u3007808\u3007O = this.\u300780\u3007808\u3007O;
        if (\u300780\u3007808\u3007O == null) {
            return;
        }
        if (b) {
            \u300780\u3007808\u3007O.setVolume(0.0f, 0.0f);
        }
        else {
            \u300780\u3007808\u3007O.setVolume(1.0f, 1.0f);
        }
    }
    
    @Override
    public void d() throws Throwable {
        this.\u300780\u3007808\u3007O.pause();
    }
    
    @Override
    public void e() throws Throwable {
        this.\u300780\u3007808\u3007O.stop();
    }
    
    @Override
    public void f() throws Throwable {
        this.\u300780\u3007808\u3007O.start();
    }
    
    @Override
    protected void finalize() throws Throwable {
        super.finalize();
        this.\u3007O888o0o();
    }
    
    @Override
    public void g() {
        final MediaPlayer \u300780\u3007808\u3007O = this.\u300780\u3007808\u3007O;
        if (\u300780\u3007808\u3007O != null) {
            \u300780\u3007808\u3007O.prepareAsync();
        }
    }
    
    @Override
    public long h() {
        try {
            return this.\u300780\u3007808\u3007O.getCurrentPosition();
        }
        finally {
            final Throwable t;
            c.OO0o\u3007\u3007\u3007\u30070("CSJ_VIDEO", "getCurrentPosition error: ", t);
            return 0L;
        }
    }
    
    @Override
    public void i() throws Throwable {
        try {
            this.\u300780\u3007808\u3007O.reset();
        }
        finally {
            final Throwable t;
            c.OO0o\u3007\u3007\u3007\u30070("CSJ_VIDEO", "reset error: ", t);
        }
        this.o800o8O();
        this.\u3007\u30078O0\u30078();
        this.OoO8();
    }
    
    @Override
    public void oO80(final SurfaceHolder display) throws Throwable {
        final Object oo0o\u3007\u3007 = this.OO0o\u3007\u3007;
        monitorenter(oo0o\u3007\u3007);
        try {
            if (!this.Oooo8o0\u3007 && display != null && display.getSurface() != null && super.oO80) {
                this.\u300780\u3007808\u3007O.setDisplay(display);
            }
        }
        finally {
            final Throwable t;
            t.printStackTrace();
        }
        try {
            monitorexit(oo0o\u3007\u3007);
        }
        finally {
            monitorexit(oo0o\u3007\u3007);
        }
    }
    
    @Override
    public void release() throws Throwable {
        synchronized (this.OO0o\u3007\u3007) {
            if (!this.Oooo8o0\u3007) {
                this.\u300780\u3007808\u3007O.release();
                this.Oooo8o0\u3007 = true;
                this.\u3007O888o0o();
                this.o800o8O();
                this.\u3007\u30078O0\u30078();
                this.OoO8();
            }
        }
    }
    
    @RequiresApi(api = 23)
    @Override
    public void \u300780\u3007808\u3007O(final b.a.a.a.a.a.a.b b) throws Throwable {
        if (Build$VERSION.SDK_INT >= 23) {
            b.a.a.a.a.a.b.e.Oo08.\u3007080(this.\u300780\u3007808\u3007O, b.a.a.a.a.a.b.e.O8.\u3007080(b.a.a.a.a.a.b.e.\u3007o\u3007.\u3007080(this.\u300780\u3007808\u3007O), b.\u3007080()));
        }
    }
    
    private static class a implements MediaPlayer$OnPreparedListener, MediaPlayer$OnCompletionListener, MediaPlayer$OnBufferingUpdateListener, MediaPlayer$OnSeekCompleteListener, MediaPlayer$OnVideoSizeChangedListener, MediaPlayer$OnErrorListener, MediaPlayer$OnInfoListener
    {
        private final WeakReference<b> o0;
        
        public a(final b referent) {
            this.o0 = new WeakReference<b>(referent);
        }
        
        public void onBufferingUpdate(final MediaPlayer mediaPlayer, final int n) {
            try {
                final b b = this.o0.get();
                if (b != null) {
                    b.\u30078o8o\u3007(n);
                }
            }
            finally {
                final Throwable t;
                c.OO0o\u3007\u3007\u3007\u30070("CSJ_VIDEO", "AndroidMediaPlayerListenerHolder.onBufferingUpdate error: ", t);
            }
        }
        
        public void onCompletion(final MediaPlayer mediaPlayer) {
            try {
                final b b = this.o0.get();
                if (b != null) {
                    b.\u3007\u3007808\u3007();
                }
            }
            finally {
                final Throwable t;
                c.OO0o\u3007\u3007\u3007\u30070("CSJ_VIDEO", "AndroidMediaPlayerListenerHolder.onCompletion error: ", t);
            }
        }
        
        public boolean onError(final MediaPlayer mediaPlayer, final int i, final int j) {
            final boolean b = false;
            try {
                c.\u30078o8o\u3007("CSJ_VIDEO", "onError: ", i, j);
                final b b2 = this.o0.get();
                boolean b3 = b;
                if (b2 != null) {
                    final boolean oo0o\u3007\u3007 = b2.OO0o\u3007\u3007(i, j);
                    b3 = b;
                    if (oo0o\u3007\u3007) {
                        b3 = true;
                    }
                }
                return b3;
            }
            finally {
                final Throwable t;
                c.OO0o\u3007\u3007\u3007\u30070("CSJ_VIDEO", "AndroidMediaPlayerListenerHolder.onError error: ", t);
                return false;
            }
        }
        
        public boolean onInfo(final MediaPlayer mediaPlayer, final int n, final int n2) {
            final boolean b = false;
            try {
                c.\u3007\u3007808\u3007("CSJ_VIDEO", "onInfo: ");
                final b b2 = this.o0.get();
                boolean b3 = b;
                if (b2 != null) {
                    final boolean oooo8o0\u3007 = b2.Oooo8o0\u3007(n, n2);
                    b3 = b;
                    if (oooo8o0\u3007) {
                        b3 = true;
                    }
                }
                return b3;
            }
            finally {
                final Throwable t;
                c.OO0o\u3007\u3007\u3007\u30070("CSJ_VIDEO", "AndroidMediaPlayerListenerHolder.onInfo error: ", t);
                return false;
            }
        }
        
        public void onPrepared(final MediaPlayer mediaPlayer) {
            try {
                final b b = this.o0.get();
                if (b != null) {
                    b.\u3007O\u3007();
                }
            }
            finally {
                final Throwable t;
                c.OO0o\u3007\u3007\u3007\u30070("CSJ_VIDEO", "AndroidMediaPlayerListenerHolder.onPrepared error: ", t);
            }
        }
        
        public void onSeekComplete(final MediaPlayer mediaPlayer) {
            try {
                final b b = this.o0.get();
                if (b != null) {
                    b.\u3007O00();
                }
            }
            finally {
                final Throwable t;
                c.OO0o\u3007\u3007\u3007\u30070("CSJ_VIDEO", "AndroidMediaPlayerListenerHolder.onSeekComplete error: ", t);
            }
        }
        
        public void onVideoSizeChanged(final MediaPlayer mediaPlayer, final int n, final int n2) {
            try {
                final b b = this.o0.get();
                if (b != null) {
                    b.\u3007O8o08O(n, n2, 1, 1);
                }
            }
            finally {
                final Throwable t;
                c.OO0o\u3007\u3007\u3007\u30070("CSJ_VIDEO", "AndroidMediaPlayerListenerHolder.onVideoSizeChanged error: ", t);
            }
        }
    }
}

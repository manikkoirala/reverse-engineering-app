// 
// Decompiled by Procyon v0.6.0
// 

package b.a.a.a.a.a.b.c;

import b.a.a.a.a.a.b.d.a;
import java.io.IOException;
import java.io.FileNotFoundException;
import java.io.File;
import java.io.RandomAccessFile;

class h
{
    private final RandomAccessFile \u3007080;
    
    h(final File file, final String mode) throws a {
        try {
            this.\u3007080 = new RandomAccessFile(file, mode);
        }
        catch (final FileNotFoundException ex) {
            throw new a(ex);
        }
    }
    
    void O8(final byte[] b, final int off, final int len) throws a {
        try {
            this.\u3007080.write(b, off, len);
        }
        catch (final IOException ex) {
            throw new a(ex);
        }
    }
    
    int \u3007080(final byte[] b) throws a {
        try {
            return this.\u3007080.read(b);
        }
        catch (final IOException ex) {
            throw new a(ex);
        }
    }
    
    void \u3007o00\u3007\u3007Oo() {
        b.a.a.a.a.a.b.d.a.Oooo8o0\u3007(this.\u3007080);
    }
    
    void \u3007o\u3007(final long pos) throws a {
        try {
            this.\u3007080.seek(pos);
        }
        catch (final IOException ex) {
            throw new a(ex);
        }
    }
    
    static class a extends Exception
    {
        a(final Throwable cause) {
            super(cause);
        }
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package b.a.a.a.a.a.b.c.m;

import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteDatabase$CursorFactory;
import android.content.Context;
import android.database.sqlite.SQLiteOpenHelper;

public class d extends SQLiteOpenHelper
{
    public d(final Context context) {
        super(context, "tt_open_sdk_video.db", (SQLiteDatabase$CursorFactory)null, 3);
    }
    
    public void onCreate(final SQLiteDatabase sqLiteDatabase) {
        sqLiteDatabase.execSQL("CREATE TABLE IF NOT EXISTS video_http_header_t(_id INTEGER PRIMARY KEY AUTOINCREMENT,key TEXT,mime TEXT,contentLength INTEGER,flag INTEGER,extra TEXT)");
    }
    
    public void onUpgrade(final SQLiteDatabase sqLiteDatabase, final int n, final int n2) {
        if (n == 1) {
            sqLiteDatabase.execSQL("ALTER TABLE video_http_header_t ADD COLUMN flag INTEGER DEFAULT 0");
            sqLiteDatabase.execSQL("ALTER TABLE video_http_header_t ADD COLUMN extra TEXT DEFAULT ''");
        }
        else if (n == 2) {
            sqLiteDatabase.execSQL("ALTER TABLE video_http_header_t ADD COLUMN extra TEXT DEFAULT ''");
        }
        else {
            sqLiteDatabase.execSQL("DROP TABLE IF EXISTS video_http_header_t");
            this.onCreate(sqLiteDatabase);
        }
    }
}

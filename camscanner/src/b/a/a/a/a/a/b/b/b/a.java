// 
// Decompiled by Procyon v0.6.0
// 

package b.a.a.a.a.a.b.b.b;

import android.os.Build$VERSION;
import android.text.TextUtils;
import java.util.ArrayList;
import java.util.List;
import java.util.Collections;
import java.util.Comparator;
import java.util.Arrays;
import java.io.File;
import java.util.Iterator;
import b.a.a.a.a.a.b.b.c.c;
import java.util.HashSet;
import java.util.Set;
import b.a.a.a.a.a.a.d.b;

public class a implements b
{
    private String O8;
    private String Oo08;
    private String oO80;
    private String o\u30070;
    private String \u3007080;
    private String \u300780\u3007808\u3007O;
    private String \u3007o00\u3007\u3007Oo;
    private String \u3007o\u3007;
    private String \u3007\u3007888;
    
    public a() {
        this.\u3007080 = "video_reward_full";
        this.\u3007o00\u3007\u3007Oo = "video_brand";
        this.\u3007o\u3007 = "video_splash";
        this.O8 = "video_default";
        this.Oo08 = null;
        this.o\u30070 = null;
        this.\u3007\u3007888 = null;
        this.oO80 = null;
        this.\u300780\u3007808\u3007O = null;
    }
    
    private Set<String> O8() {
        final HashSet set = new HashSet();
        for (final b.a.a.a.a.a.b.b.a a : b.a.a.a.a.a.b.b.a.o\u300700O.values()) {
            if (a != null && a.Oo08() != null) {
                final b.a.a.a.a.a.a.f.c oo08 = a.Oo08();
                set.add(b.a.a.a.a.a.b.f.b.O8(oo08.b(), oo08.e()).getAbsolutePath());
                set.add(b.a.a.a.a.a.b.f.b.\u3007o\u3007(oo08.b(), oo08.e()).getAbsolutePath());
            }
        }
        for (final b.a.a.a.a.a.b.b.c.b b : c.\u3007080.values()) {
            if (b != null && b.\u3007O00() != null) {
                final b.a.a.a.a.a.a.f.c \u3007o00 = b.\u3007O00();
                set.add(b.a.a.a.a.a.b.f.b.O8(\u3007o00.b(), \u3007o00.e()).getAbsolutePath());
                set.add(b.a.a.a.a.a.b.f.b.\u3007o\u3007(\u3007o00.b(), \u3007o00.e()).getAbsolutePath());
            }
        }
        return set;
    }
    
    private static void \u3007080(final File[] a, int i, final Set<String> set) {
        if (i < 0) {
            return;
        }
        if (a != null) {
            try {
                if (a.length > i) {
                    final List<File> list = Arrays.asList(a);
                    Collections.sort((List<Object>)list, (Comparator<? super Object>)new Comparator<File>() {
                        public int \u3007080(final File file, final File file2) {
                            final long n = lcmp(file2.lastModified() - file.lastModified(), 0L);
                            if (n == 0) {
                                return 0;
                            }
                            if (n < 0) {
                                return -1;
                            }
                            return 1;
                        }
                    });
                    while (i < list.size()) {
                        final File file = list.get(i);
                        if (set != null && !set.contains(file.getAbsolutePath())) {
                            ((File)list.get(i)).delete();
                        }
                        ++i;
                    }
                }
            }
            finally {
                final Throwable t;
                t.printStackTrace();
            }
        }
    }
    
    private List<b.a.a.a.a.a.a.d.a> \u3007o\u3007() {
        final ArrayList list = new ArrayList();
        list.add(new b.a.a.a.a.a.a.d.a(new File(this.b()).listFiles(), b.a.a.a.a.a.b.a.\u3007\u3007888()));
        list.add(new b.a.a.a.a.a.a.d.a(new File(this.a()).listFiles(), b.a.a.a.a.a.b.a.oO80()));
        list.add(new b.a.a.a.a.a.a.d.a(new File(this.\u3007o00\u3007\u3007Oo()).listFiles(), b.a.a.a.a.a.b.a.Oo08()));
        list.add(new b.a.a.a.a.a.a.d.a(new File(this.d()).listFiles(), b.a.a.a.a.a.b.a.o\u30070()));
        return list;
    }
    
    @Override
    public String a() {
        if (this.oO80 == null) {
            final StringBuilder sb = new StringBuilder();
            sb.append(this.Oo08);
            sb.append(File.separator);
            sb.append(this.\u3007o\u3007);
            this.oO80 = sb.toString();
            final File file = new File(this.oO80);
            if (!file.exists()) {
                file.mkdirs();
            }
        }
        return this.oO80;
    }
    
    @Override
    public void a(final String oo08) {
        this.Oo08 = oo08;
    }
    
    @Override
    public boolean a(final b.a.a.a.a.a.a.f.c c) {
        return !TextUtils.isEmpty((CharSequence)c.b()) && !TextUtils.isEmpty((CharSequence)c.e()) && new File(c.b(), c.e()).exists();
    }
    
    @Override
    public long b(final b.a.a.a.a.a.a.f.c c) {
        if (!TextUtils.isEmpty((CharSequence)c.b()) && !TextUtils.isEmpty((CharSequence)c.e())) {
            return b.a.a.a.a.a.b.f.b.\u3007080(c.b(), c.e());
        }
        return 0L;
    }
    
    @Override
    public String b() {
        if (this.o\u30070 == null) {
            final StringBuilder sb = new StringBuilder();
            sb.append(this.Oo08);
            sb.append(File.separator);
            sb.append(this.\u3007080);
            this.o\u30070 = sb.toString();
            final File file = new File(this.o\u30070);
            if (!file.exists()) {
                file.mkdirs();
            }
        }
        return this.o\u30070;
    }
    
    @Override
    public void c() {
        synchronized (this) {
            b.a.a.a.a.a.a.i.c.oO80("Exec clear video cache ");
            b.a.a.a.a.a.a.i.c.oO80(this.Oo08);
            final List<b.a.a.a.a.a.a.d.a> \u3007o\u3007 = this.\u3007o\u3007();
            if (Build$VERSION.SDK_INT >= 23) {
                final Iterator<b.a.a.a.a.a.a.d.a> iterator = \u3007o\u3007.iterator();
                Set set = null;
                while (iterator.hasNext()) {
                    final b.a.a.a.a.a.a.d.a a = iterator.next();
                    final File[] \u3007080 = a.\u3007080();
                    if (\u3007080 != null && \u3007080.length >= a.\u3007o00\u3007\u3007Oo()) {
                        Set<String> o8;
                        if ((o8 = set) == null) {
                            o8 = this.O8();
                        }
                        int n;
                        if ((n = a.\u3007o00\u3007\u3007Oo() - 2) < 0) {
                            n = 0;
                        }
                        \u3007080(a.\u3007080(), n, o8);
                        set = o8;
                    }
                }
            }
        }
    }
    
    @Override
    public String d() {
        if (this.\u300780\u3007808\u3007O == null) {
            final StringBuilder sb = new StringBuilder();
            sb.append(this.Oo08);
            sb.append(File.separator);
            sb.append(this.O8);
            this.\u300780\u3007808\u3007O = sb.toString();
            final File file = new File(this.\u300780\u3007808\u3007O);
            if (!file.exists()) {
                file.mkdirs();
            }
        }
        return this.\u300780\u3007808\u3007O;
    }
    
    public String \u3007o00\u3007\u3007Oo() {
        if (this.\u3007\u3007888 == null) {
            final StringBuilder sb = new StringBuilder();
            sb.append(this.Oo08);
            sb.append(File.separator);
            sb.append(this.\u3007o00\u3007\u3007Oo);
            this.\u3007\u3007888 = sb.toString();
            final File file = new File(this.\u3007\u3007888);
            if (!file.exists()) {
                file.mkdirs();
            }
        }
        return this.\u3007\u3007888;
    }
}

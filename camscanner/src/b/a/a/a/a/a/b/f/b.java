// 
// Decompiled by Procyon v0.6.0
// 

package b.a.a.a.a.a.b.f;

import java.io.IOException;
import java.io.RandomAccessFile;
import java.io.File;

public class b
{
    public static File O8(final String pathname, final String str) {
        final File parent = new File(pathname);
        if (parent.isFile()) {
            parent.delete();
        }
        if (!parent.exists()) {
            parent.mkdirs();
        }
        final StringBuilder sb = new StringBuilder();
        sb.append(str);
        sb.append(".temp");
        return new File(parent, sb.toString());
    }
    
    public static long \u3007080(final String s, final String s2) {
        final File \u3007o\u3007 = \u3007o\u3007(s, s2);
        if (\u3007o\u3007.exists()) {
            return \u3007o\u3007.length();
        }
        final File o8 = O8(s, s2);
        if (o8.exists()) {
            return o8.length();
        }
        return 0L;
    }
    
    public static void \u3007o00\u3007\u3007Oo(final RandomAccessFile randomAccessFile, final byte[] b, final int n, final int len, final String s) throws IOException {
        final long pos = n;
        try {
            randomAccessFile.seek(pos);
            randomAccessFile.write(b, 0, len);
        }
        finally {
            final Throwable t;
            t.printStackTrace();
        }
    }
    
    public static File \u3007o\u3007(final String pathname, final String child) {
        final File parent = new File(pathname);
        if (parent.isFile()) {
            parent.delete();
        }
        if (!parent.exists()) {
            parent.mkdirs();
        }
        return new File(parent, child);
    }
}

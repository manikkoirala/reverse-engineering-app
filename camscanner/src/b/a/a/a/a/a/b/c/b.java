// 
// Decompiled by Procyon v0.6.0
// 

package b.a.a.a.a.a.b.c;

import android.text.TextUtils;
import java.util.List;
import android.os.SystemClock;
import java.io.InputStream;
import java.io.File;
import java.io.Closeable;
import b.a.a.a.a.a.b.c.n.c;
import java.io.IOException;
import java.net.SocketTimeoutException;

class b extends b.a.a.a.a.a.b.c.a
{
    final Object O0O;
    final Object o8oOOo;
    private final int ooo0\u3007\u3007O;
    private volatile h.a \u3007O\u3007\u3007O8;
    private volatile b \u3007o0O;
    private final b \u3007\u300708O;
    
    b(final a a) {
        super(a.O8, a.Oo08);
        this.ooo0\u3007\u3007O = a.\u3007\u3007888;
        this.\u3007\u300708O = a.\u300780\u3007808\u3007O;
        this.O0O = this;
        super.O8o08O8O = a.\u3007080;
        super.\u3007080OO8\u30070 = a.\u3007o00\u3007\u3007Oo;
        super.o\u300700O = a.o\u30070;
        super.oOo\u30078o008 = a.\u3007o\u3007;
        super.\u30070O = a.oO80;
        this.o8oOOo = a.OO0o\u3007\u3007\u3007\u30070;
    }
    
    private boolean Oooo8o0\u3007() throws b.a.a.a.a.a.b.c.n.a {
        while (super.oOo\u30078o008.\u3007o\u3007()) {
            this.\u3007o00\u3007\u3007Oo();
            final k.a o8 = super.oOo\u30078o008.O8();
            try {
                this.\u30078o8o\u3007(o8);
                return true;
            }
            catch (final b \u3007o0O) {
                this.\u3007o0O = \u3007o0O;
            }
            catch (final h.a \u3007o\u3007\u3007O8) {
                this.\u3007O\u3007\u3007O8 = \u3007o\u3007\u3007O8;
                this.O8(this.OO0o\u3007\u3007\u3007\u30070(), super.O8o08O8O, \u3007o\u3007\u3007O8);
            }
            catch (final IOException ex) {
                if (ex instanceof SocketTimeoutException) {
                    o8.\u3007o00\u3007\u3007Oo();
                }
                if (this.oO80()) {
                    continue;
                }
                this.O8(this.OO0o\u3007\u3007\u3007\u30070(), super.O8o08O8O, ex);
                continue;
            }
            catch (final c c) {
                o8.\u3007080();
                this.O8(this.OO0o\u3007\u3007\u3007\u30070(), super.O8o08O8O, c);
                continue;
            }
            finally {
                return false;
            }
            break;
        }
        return false;
    }
    
    private void \u30078o8o\u3007(final k.a a) throws IOException, h.a, b.a.a.a.a.a.b.c.n.a, b {
        final File \u3007o00\u3007\u3007Oo = super.o0.\u3007o00\u3007\u3007Oo(super.\u3007080OO8\u30070);
        final long length = \u3007o00\u3007\u3007Oo.length();
        final int ooo0\u3007\u3007O = this.ooo0\u3007\u3007O;
        if (ooo0\u3007\u3007O > 0 && length >= ooo0\u3007\u3007O) {
            if (e.\u3007o\u3007) {
                final StringBuilder sb = new StringBuilder();
                sb.append("no necessary to download for ");
                sb.append(super.\u3007080OO8\u30070);
                sb.append(", cache file size: ");
                sb.append(length);
                sb.append(", max: ");
                sb.append(this.ooo0\u3007\u3007O);
            }
            return;
        }
        final int \u3007\u3007888 = this.\u3007\u3007888();
        final b.a.a.a.a.a.b.c.m.a \u3007o\u3007 = super.\u3007OOo8\u30070.\u3007o\u3007(super.\u3007080OO8\u30070, \u3007\u3007888);
        if (\u3007o\u3007 != null && length >= \u3007o\u3007.\u3007o\u3007) {
            if (e.\u3007o\u3007) {
                final StringBuilder sb2 = new StringBuilder();
                sb2.append("file download complete, key: ");
                sb2.append(super.\u3007080OO8\u30070);
            }
            return;
        }
        this.\u3007o00\u3007\u3007Oo();
        int n = (int)length;
        final b.a.a.a.a.a.b.c.p.a \u3007080 = this.\u3007080(a, n, this.ooo0\u3007\u3007O, "GET");
        if (\u3007080 == null) {
            return;
        }
        final int n2 = 1;
        h h2 = null;
        int n3 = 0;
        Label_0857: {
            try {
                this.\u3007o00\u3007\u3007Oo();
                final String oo80 = b.a.a.a.a.a.b.d.a.oO80(\u3007080, super.\u30070O == null && e.o\u30070, true);
                if (oo80 == null) {
                    final int \u3007o00\u3007\u3007Oo2 = b.a.a.a.a.a.b.d.a.\u3007o00\u3007\u3007Oo(\u3007080);
                    if (\u3007o\u3007 != null && \u3007o\u3007.\u3007o\u3007 != \u3007o00\u3007\u3007Oo2) {
                        if (e.\u3007o\u3007) {
                            final StringBuilder sb3 = new StringBuilder();
                            sb3.append("Content-Length not match, old: ");
                            sb3.append(\u3007o\u3007.\u3007o\u3007);
                            sb3.append(", ");
                            sb3.append(\u3007o00\u3007\u3007Oo2);
                            sb3.append(", key: ");
                            sb3.append(super.\u3007080OO8\u30070);
                        }
                        final StringBuilder sb4 = new StringBuilder();
                        sb4.append("Content-Length not match, old length: ");
                        sb4.append(\u3007o\u3007.\u3007o\u3007);
                        sb4.append(", new length: ");
                        sb4.append(\u3007o00\u3007\u3007Oo2);
                        sb4.append(", rawKey: ");
                        sb4.append(super.O8o08O8O);
                        sb4.append(", currentUrl: ");
                        sb4.append(a);
                        sb4.append(", previousInfo: ");
                        sb4.append(\u3007o\u3007.Oo08);
                        throw new b(sb4.toString());
                    }
                    b.a.a.a.a.a.b.d.a.O8(\u3007080, super.\u3007OOo8\u30070, super.\u3007080OO8\u30070, \u3007\u3007888);
                    final b.a.a.a.a.a.b.c.m.a \u3007o\u30072 = super.\u3007OOo8\u30070.\u3007o\u3007(super.\u3007080OO8\u30070, \u3007\u3007888);
                    int \u3007o\u30073;
                    if (\u3007o\u30072 == null) {
                        \u3007o\u30073 = 0;
                    }
                    else {
                        \u3007o\u30073 = \u3007o\u30072.\u3007o\u3007;
                    }
                    final InputStream oo81 = \u3007080.Oo08();
                    String s;
                    if (e.Oo08) {
                        s = "rwd";
                    }
                    else {
                        s = "rw";
                    }
                    final h h = new h(\u3007o00\u3007\u3007Oo, s);
                    try {
                        h.\u3007o\u3007(length);
                        if (e.\u3007o\u3007) {
                            final StringBuilder sb5 = new StringBuilder();
                            sb5.append("preload start from: ");
                            sb5.append(length);
                        }
                        final byte[] b = new byte[8192];
                        while (true) {
                            final int read = oo81.read(b);
                            if (read >= 0) {
                                this.\u3007o00\u3007\u3007Oo();
                                int i = n;
                                if (read > 0) {
                                    h.O8(b, 0, read);
                                    i = n + read;
                                    if (super.\u30070O != null) {
                                        synchronized (this.O0O) {
                                            this.O0O.notifyAll();
                                        }
                                    }
                                    super.OO.addAndGet(read);
                                    this.\u3007o\u3007(\u3007o\u30073, i);
                                }
                                final int ooo0\u3007\u3007O2 = this.ooo0\u3007\u3007O;
                                if (ooo0\u3007\u3007O2 > 0 && i >= ooo0\u3007\u3007O2) {
                                    if (e.\u3007o\u3007) {
                                        final StringBuilder sb6 = new StringBuilder();
                                        sb6.append("download, more data received, currentCacheFileSize: ");
                                        sb6.append(i);
                                        sb6.append(", max: ");
                                        sb6.append(this.ooo0\u3007\u3007O);
                                    }
                                    b.a.a.a.a.a.b.d.a.OO0o\u3007\u3007(\u3007080.Oo08());
                                    h.\u3007o00\u3007\u3007Oo();
                                    this.Oo08();
                                    return;
                                }
                                this.\u3007o00\u3007\u3007Oo();
                                n = i;
                            }
                            else {
                                try {
                                    this.o\u30070();
                                    final boolean \u3007o\u30074 = e.\u3007o\u3007;
                                    b.a.a.a.a.a.b.d.a.OO0o\u3007\u3007(\u3007080.Oo08());
                                    h.\u3007o00\u3007\u3007Oo();
                                    return;
                                }
                                finally {}
                            }
                        }
                    }
                    finally {
                        break Label_0857;
                    }
                }
                final StringBuilder sb7 = new StringBuilder();
                sb7.append(oo80);
                sb7.append(", rawKey: ");
                sb7.append(super.O8o08O8O);
                sb7.append(", url: ");
                sb7.append(a);
                throw new c(sb7.toString());
            }
            finally {
                h2 = null;
                n3 = n2;
            }
        }
        b.a.a.a.a.a.b.d.a.OO0o\u3007\u3007(\u3007080.Oo08());
        if (h2 != null) {
            h2.\u3007o00\u3007\u3007Oo();
        }
        if (n3 != 0) {
            this.Oo08();
            final boolean \u3007o\u30075 = e.\u3007o\u3007;
        }
    }
    
    b OO0o\u3007\u3007() {
        return this.\u3007o0O;
    }
    
    @Override
    public void run() {
        super.o0.\u3007080(super.\u3007080OO8\u30070);
        final long elapsedRealtime = SystemClock.elapsedRealtime();
        try {
            this.Oooo8o0\u3007();
        }
        catch (final b.a.a.a.a.a.b.c.n.a a) {}
        super.\u300708O\u300700\u3007o.set(SystemClock.elapsedRealtime() - elapsedRealtime);
        super.o0.\u3007o\u3007(super.\u3007080OO8\u30070);
        final b \u3007\u300708O = this.\u3007\u300708O;
        if (\u3007\u300708O != null) {
            \u3007\u300708O.\u3007080(this);
        }
    }
    
    h.a \u3007O8o08O() {
        return this.\u3007O\u3007\u3007O8;
    }
    
    static final class a
    {
        b.a.a.a.a.a.b.c.l.a O8;
        Object OO0o\u3007\u3007\u3007\u30070;
        b.a.a.a.a.a.b.c.m.c Oo08;
        i oO80;
        List<i.b> o\u30070;
        String \u3007080;
        b \u300780\u3007808\u3007O;
        String \u3007o00\u3007\u3007Oo;
        k \u3007o\u3007;
        int \u3007\u3007888;
        
        a O8(final k \u3007o\u3007) {
            if (\u3007o\u3007 != null) {
                this.\u3007o\u3007 = \u3007o\u3007;
                return this;
            }
            throw new IllegalArgumentException("urls is empty");
        }
        
        b OO0o\u3007\u3007\u3007\u30070() {
            if (this.O8 != null && this.Oo08 != null && !TextUtils.isEmpty((CharSequence)this.\u3007080) && !TextUtils.isEmpty((CharSequence)this.\u3007o00\u3007\u3007Oo) && this.\u3007o\u3007 != null) {
                return new b(this);
            }
            throw new IllegalArgumentException();
        }
        
        a Oo08(final b.a.a.a.a.a.b.c.l.a o8) {
            if (o8 != null) {
                this.O8 = o8;
                return this;
            }
            throw new IllegalArgumentException("cache == null");
        }
        
        a oO80(final String \u3007o00\u3007\u3007Oo) {
            if (!TextUtils.isEmpty((CharSequence)\u3007o00\u3007\u3007Oo)) {
                this.\u3007o00\u3007\u3007Oo = \u3007o00\u3007\u3007Oo;
                return this;
            }
            throw new IllegalArgumentException("key == null");
        }
        
        a o\u30070(final b.a.a.a.a.a.b.c.m.c oo08) {
            if (oo08 != null) {
                this.Oo08 = oo08;
                return this;
            }
            throw new IllegalArgumentException("db == null");
        }
        
        a \u3007080(final int \u3007\u3007888) {
            this.\u3007\u3007888 = \u3007\u3007888;
            return this;
        }
        
        a \u300780\u3007808\u3007O(final List<i.b> o\u30070) {
            this.o\u30070 = o\u30070;
            return this;
        }
        
        a \u30078o8o\u3007(final String \u3007080) {
            if (!TextUtils.isEmpty((CharSequence)\u3007080)) {
                this.\u3007080 = \u3007080;
                return this;
            }
            throw new IllegalArgumentException("rawKey == null");
        }
        
        a \u3007o00\u3007\u3007Oo(final b \u300780\u3007808\u3007O) {
            this.\u300780\u3007808\u3007O = \u300780\u3007808\u3007O;
            return this;
        }
        
        a \u3007o\u3007(final i oo80) {
            this.oO80 = oo80;
            return this;
        }
        
        a \u3007\u3007888(final Object oo0o\u3007\u3007\u3007\u30070) {
            this.OO0o\u3007\u3007\u3007\u30070 = oo0o\u3007\u3007\u3007\u30070;
            return this;
        }
    }
    
    public interface b
    {
        void \u3007080(final b.a.a.a.a.a.b.c.b p0);
    }
}

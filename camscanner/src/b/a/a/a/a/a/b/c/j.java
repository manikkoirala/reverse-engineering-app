// 
// Decompiled by Procyon v0.6.0
// 

package b.a.a.a.a.a.b.c;

import java.io.IOException;
import java.net.SocketAddress;
import java.net.URI;
import java.util.Collections;
import java.net.Proxy;
import java.util.List;
import java.net.ProxySelector;

class j extends ProxySelector
{
    private static final List<Proxy> O8;
    private final ProxySelector \u3007080;
    private final String \u3007o00\u3007\u3007Oo;
    private final int \u3007o\u3007;
    
    static {
        O8 = Collections.singletonList(Proxy.NO_PROXY);
    }
    
    private j(final String \u3007o00\u3007\u3007Oo, final int \u3007o\u3007) {
        this.\u3007080 = ProxySelector.getDefault();
        this.\u3007o00\u3007\u3007Oo = \u3007o00\u3007\u3007Oo;
        this.\u3007o\u3007 = \u3007o\u3007;
    }
    
    static void \u3007080(final String s, final int n) {
        ProxySelector.setDefault(new j(s, n));
    }
    
    @Override
    public void connectFailed(final URI uri, final SocketAddress socketAddress, final IOException ex) {
        this.\u3007080.connectFailed(uri, socketAddress, ex);
    }
    
    @Override
    public List<Proxy> select(final URI uri) {
        if (uri != null) {
            List<Proxy> list;
            if (this.\u3007o00\u3007\u3007Oo.equalsIgnoreCase(uri.getHost()) && this.\u3007o\u3007 == uri.getPort()) {
                list = j.O8;
            }
            else {
                list = this.\u3007080.select(uri);
            }
            return list;
        }
        throw new IllegalArgumentException("URI can't be null");
    }
}

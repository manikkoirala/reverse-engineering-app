// 
// Decompiled by Procyon v0.6.0
// 

package b.a.a.a.a.a.b.c;

import java.util.NoSuchElementException;
import java.util.Iterator;
import java.util.Collection;
import java.util.List;
import java.util.HashSet;
import java.util.ArrayList;
import java.util.Set;

public class k
{
    private static final Set<String> o\u30070;
    private static final Set<String> \u3007\u3007888;
    private int O8;
    private final int Oo08;
    private final ArrayList<a> \u3007080;
    private final int \u3007o00\u3007\u3007Oo;
    private int \u3007o\u3007;
    
    static {
        o\u30070 = new HashSet<String>();
        \u3007\u3007888 = new HashSet<String>();
    }
    
    k(final String s) {
        this.\u3007o\u3007 = -1;
        (this.\u3007080 = new ArrayList<a>(1)).add(new a(s));
        this.\u3007o00\u3007\u3007Oo = 1;
        this.Oo08 = 1;
    }
    
    k(final List<String> list) {
        this.\u3007o\u3007 = -1;
        if (!list.isEmpty()) {
            final int size = list.size();
            this.\u3007o00\u3007\u3007Oo = size;
            this.\u3007080 = new ArrayList<a>(size);
            final Iterator iterator = list.iterator();
            ArrayList<a> c = null;
            ArrayList<a> c2 = null;
            while (iterator.hasNext()) {
                final String s = (String)iterator.next();
                final a e = new a(s);
                if (k.o\u30070.contains(s)) {
                    ArrayList<a> list2;
                    if ((list2 = c2) == null) {
                        list2 = new ArrayList<a>();
                    }
                    list2.add(e);
                    c2 = list2;
                }
                else if (k.\u3007\u3007888.contains(s)) {
                    ArrayList<a> list3;
                    if ((list3 = c) == null) {
                        list3 = new ArrayList<a>();
                    }
                    list3.add(e);
                    c = list3;
                }
                else {
                    this.\u3007080.add(e);
                }
            }
            if (c != null) {
                this.\u3007080.addAll(c);
            }
            if (c2 != null) {
                this.\u3007080.addAll(c2);
            }
            final Integer oo0o\u3007\u3007\u3007\u30070 = e.OO0o\u3007\u3007\u3007\u30070;
            int intValue;
            if (oo0o\u3007\u3007\u3007\u30070 != null && oo0o\u3007\u3007\u3007\u30070 > 0) {
                intValue = oo0o\u3007\u3007\u3007\u30070;
            }
            else if (this.\u3007o00\u3007\u3007Oo >= 2) {
                intValue = 1;
            }
            else {
                intValue = 2;
            }
            this.Oo08 = intValue;
            return;
        }
        throw new IllegalArgumentException("urls can't be empty");
    }
    
    a O8() {
        if (this.\u3007o\u3007()) {
            final int n = this.\u3007o\u3007 + 1;
            if (n >= this.\u3007o00\u3007\u3007Oo - 1) {
                this.\u3007o\u3007 = -1;
                ++this.O8;
            }
            else {
                this.\u3007o\u3007 = n;
            }
            final a a = this.\u3007080.get(n);
            a.\u3007o00\u3007\u3007Oo = this.O8 * this.\u3007o00\u3007\u3007Oo + this.\u3007o\u3007;
            return a;
        }
        throw new NoSuchElementException();
    }
    
    boolean \u3007o\u3007() {
        return this.O8 < this.Oo08;
    }
    
    public class a
    {
        final String \u3007080;
        int \u3007o00\u3007\u3007Oo;
        
        a(final k k, final String \u3007080) {
            this.\u3007080 = \u3007080;
        }
        
        @Override
        public String toString() {
            return this.\u3007080;
        }
        
        void \u3007080() {
            k.o\u30070.add(this.\u3007080);
        }
        
        void \u3007o00\u3007\u3007Oo() {
            k.\u3007\u3007888.add(this.\u3007080);
        }
    }
}

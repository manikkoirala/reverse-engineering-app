// 
// Decompiled by Procyon v0.6.0
// 

package b.a.a.a.a.a.b.c;

import java.io.IOException;
import java.util.Iterator;
import b.a.a.a.a.a.b.c.p.b;
import java.util.HashMap;
import b.a.a.a.a.a.b.c.p.e;
import b.a.a.a.a.a.b.c.m.c;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicLong;

abstract class a implements Runnable
{
    private static final AtomicLong \u30078\u3007oO\u3007\u30078o;
    protected volatile String O8o08O8O;
    protected final AtomicInteger OO;
    private final AtomicInteger OO\u300700\u30078oO;
    protected volatile b.a.a.a.a.a.b.c.l.a o0;
    private int o8\u3007OO0\u30070o;
    protected volatile boolean oOo0;
    protected volatile k oOo\u30078o008;
    protected volatile List<i.b> o\u300700O;
    protected volatile String \u3007080OO8\u30070;
    protected final AtomicLong \u300708O\u300700\u3007o;
    protected volatile i \u30070O;
    protected final c \u3007OOo8\u30070;
    
    static {
        \u30078\u3007oO\u3007\u30078o = new AtomicLong();
    }
    
    public a(final b.a.a.a.a.a.b.c.l.a o0, final c \u3007oOo8\u30070) {
        this.OO = new AtomicInteger();
        this.\u300708O\u300700\u3007o = new AtomicLong();
        this.oOo0 = false;
        a.\u30078\u3007oO\u3007\u30078o.incrementAndGet();
        this.OO\u300700\u30078oO = new AtomicInteger(0);
        this.o8\u3007OO0\u30070o = -1;
        this.o0 = o0;
        this.\u3007OOo8\u30070 = \u3007oOo8\u30070;
    }
    
    protected void O8(final Boolean b, final String s, final Throwable t) {
    }
    
    protected boolean OO0o\u3007\u3007\u3007\u30070() {
        final int \u3007\u3007888 = this.\u3007\u3007888();
        boolean b = true;
        if (\u3007\u3007888 != 1) {
            b = false;
        }
        return b;
    }
    
    public void Oo08() {
        this.OO\u300700\u30078oO.compareAndSet(0, 1);
    }
    
    public boolean oO80() {
        final int value = this.OO\u300700\u30078oO.get();
        boolean b = true;
        if (value != 1) {
            b = false;
        }
        return b;
    }
    
    protected void o\u30070() {
        this.OO\u300700\u30078oO.compareAndSet(0, 2);
    }
    
    protected b.a.a.a.a.a.b.c.p.a \u3007080(final k.a a, int n, final int n2, final String anotherString) throws IOException {
        final b \u3007o00\u3007\u3007Oo = b.a.a.a.a.a.b.c.p.c.\u3007080().\u3007o00\u3007\u3007Oo();
        final e e = new e();
        final HashMap \u3007o00\u3007\u3007Oo2 = new HashMap();
        e.\u3007080 = a.\u3007080;
        "HEAD".equalsIgnoreCase(anotherString);
        final List<i.b> o\u300700O = this.o\u300700O;
        if (o\u300700O != null && !o\u300700O.isEmpty()) {
            for (final i.b b : o\u300700O) {
                if (!"Range".equalsIgnoreCase(b.\u3007080) && !"Connection".equalsIgnoreCase(b.\u3007080) && !"Proxy-Connection".equalsIgnoreCase(b.\u3007080)) {
                    if ("Host".equalsIgnoreCase(b.\u3007080)) {
                        continue;
                    }
                    \u3007o00\u3007\u3007Oo2.put(b.\u3007080, b.\u3007o00\u3007\u3007Oo);
                }
            }
        }
        final String oo08 = b.a.a.a.a.a.b.d.a.Oo08(n, n2);
        if (oo08 != null) {
            \u3007o00\u3007\u3007Oo2.put("Range", oo08);
        }
        if (b.a.a.a.a.a.b.c.e.\u3007\u3007888) {
            \u3007o00\u3007\u3007Oo2.put("Cache-Control", "no-cache");
        }
        final d \u3007o\u3007 = d.\u3007O\u3007();
        final f \u30070\u3007O0088o = f.\u30070\u3007O0088o();
        if (this.\u30070O == null) {
            n = 1;
        }
        else {
            n = 0;
        }
        if (n != 0) {
            \u3007o\u3007.OO0o\u3007\u3007();
        }
        else {
            \u30070\u3007O0088o.\u3007\u3007808\u3007();
        }
        if (n != 0) {
            \u3007o\u3007.\u3007\u3007808\u3007();
        }
        else {
            \u30070\u3007O0088o.\u3007\u30078O0\u30078();
        }
        e.\u3007o00\u3007\u3007Oo = \u3007o00\u3007\u3007Oo2;
        if (this.oOo0) {
            this.oOo0 = false;
            return null;
        }
        return \u3007o00\u3007\u3007Oo.\u3007080(e);
    }
    
    public boolean \u300780\u3007808\u3007O() {
        return this.OO\u300700\u30078oO.get() == 2;
    }
    
    protected void \u3007o00\u3007\u3007Oo() throws b.a.a.a.a.a.b.c.n.a {
        if (!this.oO80()) {
            return;
        }
        throw new b.a.a.a.a.a.b.c.n.a();
    }
    
    protected void \u3007o\u3007(int o8\u3007OO0\u30070o, int n) {
        if (o8\u3007OO0\u30070o > 0) {
            if (n >= 0) {
                final int oo80 = b.a.a.a.a.a.b.c.e.oO80;
                final int \u3007\u3007888 = this.\u3007\u3007888();
                if (oo80 != 1 && (oo80 != 2 || \u3007\u3007888 != 1)) {
                    return;
                }
                n = (int)(n / (float)o8\u3007OO0\u30070o * 100.0f);
                if ((o8\u3007OO0\u30070o = n) > 100) {
                    o8\u3007OO0\u30070o = 100;
                }
                synchronized (this) {
                    if (o8\u3007OO0\u30070o <= this.o8\u3007OO0\u30070o) {
                        return;
                    }
                    this.o8\u3007OO0\u30070o = o8\u3007OO0\u30070o;
                    monitorexit(this);
                    b.a.a.a.a.a.b.d.a.\u3007\u3007808\u3007(new Runnable(this) {
                        final a o0;
                        
                        @Override
                        public void run() {
                            this.o0.getClass();
                        }
                    });
                }
            }
        }
    }
    
    protected int \u3007\u3007888() {
        int \u3007080;
        if (this.\u30070O != null) {
            \u3007080 = this.\u30070O.\u3007o\u3007.\u3007080;
        }
        else {
            \u3007080 = 0;
        }
        return \u3007080;
    }
}

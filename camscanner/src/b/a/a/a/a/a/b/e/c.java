// 
// Decompiled by Procyon v0.6.0
// 

package b.a.a.a.a.a.b.e;

import b.a.a.a.a.a.a.b;
import android.view.SurfaceHolder;
import android.view.Surface;
import java.io.FileDescriptor;

public interface c
{
    void O8(final b p0);
    
    void OO0o\u3007\u3007\u3007\u30070(final FileDescriptor p0) throws Throwable;
    
    void Oo08(final g p0);
    
    long a() throws Throwable;
    
    void a(final long p0, final int p1) throws Throwable;
    
    void a(final Surface p0) throws Throwable;
    
    void a(final c p0);
    
    void a(final String p0) throws Throwable;
    
    void a(final boolean p0) throws Throwable;
    
    int b();
    
    void b(final boolean p0) throws Throwable;
    
    int c();
    
    void c(final boolean p0) throws Throwable;
    
    void d() throws Throwable;
    
    void d(final boolean p0);
    
    void e() throws Throwable;
    
    void f() throws Throwable;
    
    void g();
    
    long h() throws Throwable;
    
    void i() throws Throwable;
    
    void oO80(final SurfaceHolder p0) throws Throwable;
    
    void o\u30070(final f p0);
    
    void release() throws Throwable;
    
    void \u3007080(final d p0);
    
    void \u300780\u3007808\u3007O(final b.a.a.a.a.a.a.b p0) throws Throwable;
    
    void \u3007o00\u3007\u3007Oo(final c p0);
    
    void \u3007o\u3007(final a p0);
    
    void \u3007\u3007888(final e p0);
    
    public interface a
    {
        void a(final c p0, final int p1);
    }
    
    public interface b
    {
        void c(final c p0);
    }
    
    public interface c
    {
        boolean b(final b.a.a.a.a.a.b.e.c p0, final int p1, final int p2);
    }
    
    public interface d
    {
        boolean a(final c p0, final int p1, final int p2);
    }
    
    public interface e
    {
        void a(final c p0);
    }
    
    public interface f
    {
        void b(final c p0);
    }
    
    public interface g
    {
        void a(final c p0, final int p1, final int p2, final int p3, final int p4);
    }
}

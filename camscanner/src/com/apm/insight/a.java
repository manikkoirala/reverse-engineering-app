// 
// Decompiled by Procyon v0.6.0
// 

package com.apm.insight;

import androidx.annotation.Nullable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class a
{
    private Map<CrashType, List<AttachUserData>> a;
    private Map<CrashType, List<AttachUserData>> b;
    private Map<String, String> c;
    private ICrashFilter d;
    
    public a() {
        this.a = new HashMap<CrashType, List<AttachUserData>>();
        this.b = new HashMap<CrashType, List<AttachUserData>>();
        this.c = new HashMap<String, String>();
        this.d = null;
    }
    
    private void c(final CrashType crashType, final AttachUserData attachUserData) {
        List list2;
        if (this.a.get(crashType) == null) {
            final ArrayList list = new ArrayList();
            this.a.put(crashType, list);
            list2 = list;
        }
        else {
            list2 = this.a.get(crashType);
        }
        list2.add(attachUserData);
    }
    
    private void d(final CrashType crashType, final AttachUserData attachUserData) {
        List list2;
        if (this.b.get(crashType) == null) {
            final ArrayList list = new ArrayList();
            this.b.put(crashType, list);
            list2 = list;
        }
        else {
            list2 = this.b.get(crashType);
        }
        list2.add(attachUserData);
    }
    
    private void e(final CrashType crashType, final AttachUserData attachUserData) {
        final List list = this.a.get(crashType);
        if (list != null) {
            list.remove(attachUserData);
        }
    }
    
    private void f(final CrashType crashType, final AttachUserData attachUserData) {
        final List list = this.b.get(crashType);
        if (list != null) {
            list.remove(attachUserData);
        }
    }
    
    @Nullable
    public List<AttachUserData> a(final CrashType crashType) {
        return this.a.get(crashType);
    }
    
    public Map<String, String> a() {
        return this.c;
    }
    
    void a(final AttachUserData attachUserData, final CrashType crashType) {
        CrashType dart = crashType;
        if (crashType == CrashType.ALL) {
            this.c(CrashType.LAUNCH, attachUserData);
            this.c(CrashType.JAVA, attachUserData);
            this.c(CrashType.CUSTOM_JAVA, attachUserData);
            this.c(CrashType.NATIVE, attachUserData);
            this.c(CrashType.ANR, attachUserData);
            dart = CrashType.DART;
        }
        this.c(dart, attachUserData);
    }
    
    void a(final CrashType crashType, final AttachUserData attachUserData) {
        CrashType dart = crashType;
        if (crashType == CrashType.ALL) {
            this.e(CrashType.LAUNCH, attachUserData);
            this.e(CrashType.JAVA, attachUserData);
            this.e(CrashType.CUSTOM_JAVA, attachUserData);
            this.e(CrashType.NATIVE, attachUserData);
            this.e(CrashType.ANR, attachUserData);
            dart = CrashType.DART;
        }
        this.e(dart, attachUserData);
    }
    
    void a(final ICrashFilter d) {
        this.d = d;
    }
    
    void a(final Map<? extends String, ? extends String> map) {
        this.c.putAll(map);
    }
    
    @Nullable
    public ICrashFilter b() {
        return this.d;
    }
    
    @Nullable
    public List<AttachUserData> b(final CrashType crashType) {
        return this.b.get(crashType);
    }
    
    void b(final AttachUserData attachUserData, final CrashType crashType) {
        CrashType dart = crashType;
        if (crashType == CrashType.ALL) {
            this.d(CrashType.LAUNCH, attachUserData);
            this.d(CrashType.JAVA, attachUserData);
            this.d(CrashType.CUSTOM_JAVA, attachUserData);
            this.d(CrashType.NATIVE, attachUserData);
            this.d(CrashType.ANR, attachUserData);
            dart = CrashType.DART;
        }
        this.d(dart, attachUserData);
    }
    
    void b(final CrashType crashType, final AttachUserData attachUserData) {
        CrashType dart = crashType;
        if (crashType == CrashType.ALL) {
            this.f(CrashType.LAUNCH, attachUserData);
            this.f(CrashType.JAVA, attachUserData);
            this.f(CrashType.CUSTOM_JAVA, attachUserData);
            this.f(CrashType.NATIVE, attachUserData);
            this.f(CrashType.ANR, attachUserData);
            dart = CrashType.DART;
        }
        this.f(dart, attachUserData);
    }
}

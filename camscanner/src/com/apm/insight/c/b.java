// 
// Decompiled by Procyon v0.6.0
// 

package com.apm.insight.c;

import android.content.Context;
import java.io.File;
import com.apm.insight.i;
import com.apm.insight.runtime.a;
import java.util.concurrent.atomic.AtomicBoolean;

public class b
{
    private static AtomicBoolean a;
    
    static {
        b.a = new AtomicBoolean(false);
    }
    
    public static boolean a() {
        synchronized (b.a) {
            if (b.a.get()) {
                return false;
            }
            b.a.set(true);
            return b();
        }
    }
    
    public static boolean b() {
        if (!com.apm.insight.runtime.a.h()) {
            return false;
        }
        try {
            final File parent = new File(((Context)i.h()).getFilesDir(), "apminsight/crashCommand");
            parent.mkdirs();
            final StringBuilder sb = new StringBuilder();
            sb.append("0_");
            sb.append(System.currentTimeMillis());
            new File(parent, sb.toString()).createNewFile();
            return true;
        }
        finally {
            return false;
        }
    }
}

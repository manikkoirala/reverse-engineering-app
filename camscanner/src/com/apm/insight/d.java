// 
// Decompiled by Procyon v0.6.0
// 

package com.apm.insight;

import com.apm.insight.l.j;
import com.apm.insight.runtime.m;
import com.apm.insight.f.b;
import java.util.Map;
import com.apm.insight.f.a;

public class d
{
    public d() {
        a.a();
    }
    
    public void a(final String s) {
        this.a(s, "EnsureNotReachHere", null);
    }
    
    public void a(final String s, final String s2, final Map<String, String> map) {
        if (!i.i().isEnsureEnable()) {
            return;
        }
        b.a(Thread.currentThread().getStackTrace(), 5, s, s2, map);
    }
    
    public void a(final String s, final Throwable t) {
        m.a(t, s);
    }
    
    public void a(final Throwable t, final String s) {
        if (!this.a(t)) {
            return;
        }
        b.a(t, s, true);
    }
    
    public boolean a(final Throwable t) {
        return i.i().isEnsureEnable() && !j.a(t);
    }
}

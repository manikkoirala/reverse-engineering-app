// 
// Decompiled by Procyon v0.6.0
// 

package com.apm.insight.nativecrash;

import androidx.annotation.Keep;
import androidx.annotation.NonNull;
import java.util.Map;
import com.apm.insight.l.v;
import android.os.Looper;
import android.text.TextUtils;
import java.util.Iterator;
import com.apm.insight.c;
import com.apm.insight.CrashType;
import com.apm.insight.ICrashCallback;
import com.apm.insight.runtime.o;

public class b
{
    public static int a() {
        return 6;
    }
    
    private static void a(final String s, final String s2, final String s3) {
        for (final ICrashCallback crashCallback : o.a().d()) {
            try {
                if (crashCallback instanceof com.apm.insight.b) {
                    ((com.apm.insight.b)crashCallback).a(CrashType.NATIVE, s, s3, s2);
                }
                else {
                    crashCallback.onCrash(CrashType.NATIVE, s, null);
                }
            }
            finally {
                final Throwable t;
                c.a().a("NPTH_CATCH", t);
            }
        }
    }
    
    @NonNull
    private static String b(String a) {
        if (TextUtils.isEmpty((CharSequence)a)) {
            return "";
        }
        if ("main".equalsIgnoreCase(a)) {
            return v.a(Looper.getMainLooper().getThread().getStackTrace());
        }
        final ThreadGroup threadGroup = Looper.getMainLooper().getThread().getThreadGroup();
        final int activeCount = threadGroup.activeCount();
        final Thread[] list = new Thread[activeCount + activeCount / 2];
        for (int enumerate = threadGroup.enumerate(list), i = 0; i < enumerate; ++i) {
            final String name = list[i].getName();
            if (!TextUtils.isEmpty((CharSequence)name) && (name.equals(a) || name.startsWith(a) || name.endsWith(a))) {
                return v.a(list[i].getStackTrace());
            }
        }
        try {
            for (final Map.Entry<Thread, V> entry : Thread.getAllStackTraces().entrySet()) {
                final String name2 = entry.getKey().getName();
                if (name2.equals(a) || name2.startsWith(a) || name2.endsWith(a)) {
                    return v.a((StackTraceElement[])(Object)entry.getValue());
                }
            }
            return "";
            final Map.Entry<Thread, V> entry;
            a = v.a((StackTraceElement[])(Object)entry.getValue());
            return a;
        }
        finally {
            final Throwable t;
            c.a().a("NPTH_CATCH", t);
        }
        return "";
    }
    
    @Keep
    public static void onNativeCrash(final String p0) {
        // 
        // This method could not be decompiled.
        // 
        // Original Bytecode:
        // 
        //     3: lstore_3       
        //     4: ldc             "[onNativeCrash] enter"
        //     6: invokestatic    com/apm/insight/l/q.a:(Ljava/lang/Object;)V
        //     9: invokestatic    com/apm/insight/k/b.a:()Lcom/apm/insight/k/b;
        //    12: invokevirtual   com/apm/insight/k/b.b:()V
        //    15: new             Ljava/io/File;
        //    18: astore          5
        //    20: aload           5
        //    22: invokestatic    com/apm/insight/l/o.a:()Ljava/io/File;
        //    25: invokestatic    com/apm/insight/i.f:()Ljava/lang/String;
        //    28: invokespecial   java/io/File.<init>:(Ljava/io/File;Ljava/lang/String;)V
        //    31: aload           5
        //    33: invokestatic    com/apm/insight/l/o.e:(Ljava/io/File;)Ljava/io/File;
        //    36: astore          5
        //    38: invokestatic    com/apm/insight/runtime/a/f.a:()Lcom/apm/insight/runtime/a/f;
        //    41: astore          6
        //    43: getstatic       com/apm/insight/CrashType.NATIVE:Lcom/apm/insight/CrashType;
        //    46: astore          7
        //    48: new             Lcom/apm/insight/nativecrash/b$1;
        //    51: astore          8
        //    53: aload           8
        //    55: aload_0        
        //    56: aload           5
        //    58: lload_3        
        //    59: invokespecial   com/apm/insight/nativecrash/b$1.<init>:(Ljava/lang/String;Ljava/io/File;J)V
        //    62: aload           6
        //    64: aload           7
        //    66: aconst_null    
        //    67: aload           8
        //    69: iconst_1       
        //    70: invokevirtual   com/apm/insight/runtime/a/f.a:(Lcom/apm/insight/CrashType;Lcom/apm/insight/entity/a;Lcom/apm/insight/runtime/a/c$a;Z)Lcom/apm/insight/entity/a;
        //    73: astore          7
        //    75: aload           7
        //    77: invokevirtual   com/apm/insight/entity/a.h:()Lorg/json/JSONObject;
        //    80: astore          6
        //    82: aload           6
        //    84: ifnull          201
        //    87: aload           6
        //    89: invokevirtual   org/json/JSONObject.length:()I
        //    92: ifeq            201
        //    95: invokestatic    java/lang/System.currentTimeMillis:()J
        //    98: lstore_1       
        //    99: lload_1        
        //   100: lload_3        
        //   101: lsub           
        //   102: lstore_3       
        //   103: aload           6
        //   105: ldc             "java_end"
        //   107: lload_1        
        //   108: invokevirtual   org/json/JSONObject.put:(Ljava/lang/String;J)Lorg/json/JSONObject;
        //   111: pop            
        //   112: aload           7
        //   114: ldc             "crash_cost"
        //   116: lload_3        
        //   117: invokestatic    java/lang/String.valueOf:(J)Ljava/lang/String;
        //   120: invokevirtual   com/apm/insight/entity/a.b:(Ljava/lang/String;Ljava/lang/String;)Lcom/apm/insight/entity/a;
        //   123: pop            
        //   124: aload           7
        //   126: ldc             "crash_cost"
        //   128: lload_3        
        //   129: ldc2_w          1000
        //   132: ldiv           
        //   133: invokestatic    java/lang/String.valueOf:(J)Ljava/lang/String;
        //   136: invokevirtual   com/apm/insight/entity/a.a:(Ljava/lang/String;Ljava/lang/String;)Lcom/apm/insight/entity/a;
        //   139: pop            
        //   140: new             Ljava/io/File;
        //   143: astore          8
        //   145: new             Ljava/lang/StringBuilder;
        //   148: astore          7
        //   150: aload           7
        //   152: invokespecial   java/lang/StringBuilder.<init>:()V
        //   155: aload           7
        //   157: aload           5
        //   159: invokevirtual   java/io/File.getAbsolutePath:()Ljava/lang/String;
        //   162: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   165: pop            
        //   166: aload           7
        //   168: ldc_w           ".tmp"
        //   171: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   174: pop            
        //   175: aload           8
        //   177: aload           7
        //   179: invokevirtual   java/lang/StringBuilder.toString:()Ljava/lang/String;
        //   182: invokespecial   java/io/File.<init>:(Ljava/lang/String;)V
        //   185: aload           8
        //   187: aload           6
        //   189: iconst_0       
        //   190: invokestatic    com/apm/insight/l/i.a:(Ljava/io/File;Lorg/json/JSONObject;Z)V
        //   193: aload           8
        //   195: aload           5
        //   197: invokevirtual   java/io/File.renameTo:(Ljava/io/File;)Z
        //   200: pop            
        //   201: invokestatic    com/apm/insight/runtime/o.a:()Lcom/apm/insight/runtime/c;
        //   204: invokevirtual   com/apm/insight/runtime/c.d:()Ljava/util/List;
        //   207: invokeinterface java/util/List.isEmpty:()Z
        //   212: ifne            351
        //   215: new             Ljava/io/File;
        //   218: astore          5
        //   220: aload           5
        //   222: invokestatic    com/apm/insight/l/o.a:()Ljava/io/File;
        //   225: invokestatic    com/apm/insight/i.f:()Ljava/lang/String;
        //   228: invokespecial   java/io/File.<init>:(Ljava/io/File;Ljava/lang/String;)V
        //   231: new             Lcom/apm/insight/nativecrash/e;
        //   234: astore          6
        //   236: aload           6
        //   238: aload           5
        //   240: invokespecial   com/apm/insight/nativecrash/e.<init>:(Ljava/io/File;)V
        //   243: aload           6
        //   245: aload           5
        //   247: invokevirtual   com/apm/insight/nativecrash/e.b:(Ljava/io/File;)V
        //   250: aload           6
        //   252: invokevirtual   com/apm/insight/nativecrash/e.c:()Ljava/lang/String;
        //   255: astore          5
        //   257: aload           5
        //   259: aload           6
        //   261: invokevirtual   com/apm/insight/nativecrash/e.a:()Ljava/lang/String;
        //   264: aload_0        
        //   265: invokestatic    com/apm/insight/nativecrash/b.a:(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
        //   268: goto            351
        //   271: astore          5
        //   273: invokestatic    com/apm/insight/c.a:()Lcom/apm/insight/d;
        //   276: ldc             "NPTH_CATCH"
        //   278: aload           5
        //   280: invokevirtual   com/apm/insight/d.a:(Ljava/lang/String;Ljava/lang/Throwable;)V
        //   283: invokestatic    com/apm/insight/runtime/o.a:()Lcom/apm/insight/runtime/c;
        //   286: invokevirtual   com/apm/insight/runtime/c.d:()Ljava/util/List;
        //   289: invokeinterface java/util/List.isEmpty:()Z
        //   294: ifne            351
        //   297: new             Ljava/io/File;
        //   300: astore          5
        //   302: aload           5
        //   304: invokestatic    com/apm/insight/l/o.a:()Ljava/io/File;
        //   307: invokestatic    com/apm/insight/i.f:()Ljava/lang/String;
        //   310: invokespecial   java/io/File.<init>:(Ljava/io/File;Ljava/lang/String;)V
        //   313: new             Lcom/apm/insight/nativecrash/e;
        //   316: astore          6
        //   318: aload           6
        //   320: aload           5
        //   322: invokespecial   com/apm/insight/nativecrash/e.<init>:(Ljava/io/File;)V
        //   325: aload           6
        //   327: aload           5
        //   329: invokevirtual   com/apm/insight/nativecrash/e.b:(Ljava/io/File;)V
        //   332: aload           6
        //   334: invokevirtual   com/apm/insight/nativecrash/e.c:()Ljava/lang/String;
        //   337: astore          5
        //   339: goto            257
        //   342: astore          5
        //   344: ldc             ""
        //   346: aconst_null    
        //   347: aload_0        
        //   348: invokestatic    com/apm/insight/nativecrash/b.a:(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
        //   351: return         
        //   352: astore          5
        //   354: invokestatic    com/apm/insight/runtime/o.a:()Lcom/apm/insight/runtime/c;
        //   357: invokevirtual   com/apm/insight/runtime/c.d:()Ljava/util/List;
        //   360: invokeinterface java/util/List.isEmpty:()Z
        //   365: ifne            429
        //   368: new             Ljava/io/File;
        //   371: astore          7
        //   373: aload           7
        //   375: invokestatic    com/apm/insight/l/o.a:()Ljava/io/File;
        //   378: invokestatic    com/apm/insight/i.f:()Ljava/lang/String;
        //   381: invokespecial   java/io/File.<init>:(Ljava/io/File;Ljava/lang/String;)V
        //   384: new             Lcom/apm/insight/nativecrash/e;
        //   387: astore          6
        //   389: aload           6
        //   391: aload           7
        //   393: invokespecial   com/apm/insight/nativecrash/e.<init>:(Ljava/io/File;)V
        //   396: aload           6
        //   398: aload           7
        //   400: invokevirtual   com/apm/insight/nativecrash/e.b:(Ljava/io/File;)V
        //   403: aload           6
        //   405: invokevirtual   com/apm/insight/nativecrash/e.c:()Ljava/lang/String;
        //   408: aload           6
        //   410: invokevirtual   com/apm/insight/nativecrash/e.a:()Ljava/lang/String;
        //   413: aload_0        
        //   414: invokestatic    com/apm/insight/nativecrash/b.a:(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
        //   417: goto            429
        //   420: astore          6
        //   422: ldc             ""
        //   424: aconst_null    
        //   425: aload_0        
        //   426: invokestatic    com/apm/insight/nativecrash/b.a:(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
        //   429: aload           5
        //   431: athrow         
        //   432: astore          7
        //   434: goto            140
        //    Exceptions:
        //  Try           Handler
        //  Start  End    Start  End    Type
        //  -----  -----  -----  -----  ----
        //  9      82     271    432    Any
        //  87     99     271    432    Any
        //  103    140    432    437    Any
        //  140    201    271    432    Any
        //  201    257    342    351    Any
        //  257    268    342    351    Any
        //  273    283    352    432    Any
        //  283    339    342    351    Any
        //  354    417    420    429    Any
        // 
        // The error that occurred was:
        // 
        // java.lang.IllegalStateException: Expression is linked from several locations: Label_0140:
        //     at com.strobel.decompiler.ast.Error.expressionLinkedFromMultipleLocations(Error.java:27)
        //     at com.strobel.decompiler.ast.AstOptimizer.mergeDisparateObjectInitializations(AstOptimizer.java:2604)
        //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:235)
        //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:42)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:206)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:93)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethodBody(AstBuilder.java:868)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethod(AstBuilder.java:761)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addTypeMembers(AstBuilder.java:638)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeCore(AstBuilder.java:605)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeNoCache(AstBuilder.java:195)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createType(AstBuilder.java:162)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addType(AstBuilder.java:137)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.buildAst(JavaLanguage.java:71)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.decompileType(JavaLanguage.java:59)
        //     at com.strobel.decompiler.DecompilerDriver.decompileType(DecompilerDriver.java:333)
        //     at com.strobel.decompiler.DecompilerDriver.decompileJar(DecompilerDriver.java:254)
        //     at com.strobel.decompiler.DecompilerDriver.main(DecompilerDriver.java:144)
        // 
        throw new IllegalStateException("An error occurred while decompiling this method.");
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.apm.insight.nativecrash;

import androidx.annotation.Nullable;
import android.text.TextUtils;
import java.util.HashMap;
import com.apm.insight.l.o;
import java.io.File;
import java.util.Map;

public class a
{
    private Map<String, String> a;
    
    public a(File c) {
        c = o.c(c);
        if (c.exists()) {
            if (c.length() != 0L) {
                final String a = NativeImpl.a(c.getAbsolutePath());
                if (a == null) {
                    return;
                }
                final String[] split = a.split("\n");
                this.a = new HashMap<String, String>();
                for (int length = split.length, i = 0; i < length; ++i) {
                    final String[] split2 = split[i].split("=");
                    if (split2.length == 2) {
                        this.a.put(split2[0], split2[1]);
                    }
                }
            }
        }
    }
    
    public boolean a() {
        final Map<String, String> a = this.a;
        return a != null && !a.isEmpty() && !TextUtils.isEmpty((CharSequence)this.a.get("process_name")) && !TextUtils.isEmpty((CharSequence)this.a.get("crash_thread_name")) && !TextUtils.isEmpty((CharSequence)this.a.get("pid")) && !TextUtils.isEmpty((CharSequence)this.a.get("tid")) && !TextUtils.isEmpty((CharSequence)this.a.get("start_time")) && !TextUtils.isEmpty((CharSequence)this.a.get("crash_time")) && !TextUtils.isEmpty((CharSequence)this.a.get("signal_line"));
    }
    
    @Nullable
    public String b() {
        return this.a.get("signal_line");
    }
    
    public Map<String, String> c() {
        return this.a;
    }
}

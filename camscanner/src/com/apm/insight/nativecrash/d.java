// 
// Decompiled by Procyon v0.6.0
// 

package com.apm.insight.nativecrash;

import java.util.Iterator;
import java.io.IOException;
import java.util.ArrayList;
import android.text.TextUtils;
import com.apm.insight.l.i;
import java.util.List;
import java.util.HashMap;
import java.io.Closeable;
import com.apm.insight.l.k;
import com.apm.insight.c;
import java.io.Reader;
import java.io.BufferedReader;
import java.io.FileReader;
import androidx.annotation.NonNull;
import org.json.JSONArray;
import java.io.File;
import com.apm.insight.l.o;

public class d
{
    public static int a(final String s) {
        return ((c)new a(o.b(s))).a();
    }
    
    @NonNull
    public static JSONArray a(final File file, final File file2) {
        return new e(file2).a(new d(file).b());
    }
    
    public static int b(final String s) {
        return ((c)new f(o.c(s))).a();
    }
    
    public static int c(final String s) {
        return ((c)new b(o.d(s))).a();
    }
    
    public static class a extends c
    {
        a(final File file) {
            super(file);
            super.b = "Total FD Count:";
            super.c = ":";
            super.d = -2;
        }
    }
    
    public static class c
    {
        protected File a;
        protected String b;
        protected String c;
        protected int d;
        
        public c(final File a) {
            this.a = a;
        }
        
        public int a() {
            if (this.a.exists()) {
                if (this.a.isFile()) {
                    Label_0127: {
                        Closeable closeable;
                        int n2;
                        try {
                            final BufferedReader bufferedReader = new BufferedReader(new FileReader(this.a));
                            try {
                                while (true) {
                                    final String line = bufferedReader.readLine();
                                    if (line == null) {
                                        break Label_0127;
                                    }
                                    final int a = this.a(line);
                                    if (a != -1) {
                                        break Label_0127;
                                    }
                                    final int n = a;
                                }
                            }
                            finally {}
                        }
                        finally {
                            closeable = null;
                            n2 = -1;
                        }
                        try {
                            Closeable closeable2 = null;
                            com.apm.insight.c.a().a("NPTH_CATCH", (Throwable)closeable2);
                            int n3 = n2;
                            if (closeable != null) {
                                closeable2 = closeable;
                                k.a(closeable2);
                                n3 = n2;
                            }
                            return n3;
                        }
                        finally {
                            if (closeable != null) {
                                k.a(closeable);
                            }
                        }
                    }
                }
            }
            return -1;
        }
        
        public int a(final String s) {
            int d;
            int int1 = d = this.d;
            if (s.startsWith(this.b)) {
                final String[] split = s.split(this.c);
                try {
                    int1 = Integer.parseInt(split[1].trim());
                }
                catch (final NumberFormatException ex) {
                    com.apm.insight.c.a().a("NPTH_CATCH", ex);
                }
                d = int1;
                if (int1 < 0) {
                    d = -2;
                }
            }
            return d;
        }
    }
    
    public static class b extends c
    {
        b(final File file) {
            super(file);
            super.b = "VmSize:";
            super.c = "\\s+";
            super.d = -1;
        }
    }
    
    public static class d extends c
    {
        d(final File file) {
            super(file);
        }
        
        @NonNull
        public HashMap<String, List<String>> b() {
            final HashMap hashMap = new HashMap();
            try {
                final JSONArray b = i.b(super.a.getAbsolutePath());
                if (b == null) {
                    return hashMap;
                }
                for (int i = 0; i < b.length(); ++i) {
                    final String optString = b.optString(i);
                    if (!TextUtils.isEmpty((CharSequence)optString)) {
                        if (optString.startsWith("[tid:0") && optString.endsWith("sigstack:0x0]")) {
                            final int index = optString.indexOf("[routine:0x");
                            final int n = index + 11;
                            final int index2 = optString.indexOf(93, n);
                            String substring;
                            if (index > 0) {
                                substring = optString.substring(n, index2);
                            }
                            else {
                                substring = "unknown addr";
                            }
                            List value;
                            if ((value = hashMap.get(substring)) == null) {
                                value = new ArrayList();
                                hashMap.put(substring, value);
                            }
                            value.add(optString);
                        }
                    }
                }
                goto Label_0193;
            }
            catch (final IOException ex) {
                goto Label_0193;
            }
            finally {
                final Throwable t;
                c.a().a("NPTH_CATCH", t);
            }
        }
    }
    
    public static class e extends c
    {
        e(final File file) {
            super(file);
        }
        
        @NonNull
        public JSONArray a(final HashMap<String, List<String>> hashMap) {
            final JSONArray jsonArray = new JSONArray();
            if (hashMap.isEmpty()) {
                return jsonArray;
            }
            try {
                final JSONArray b = i.b(super.a.getAbsolutePath());
                if (b == null) {
                    return jsonArray;
                }
                for (int i = 0; i < b.length(); ++i) {
                    final String optString = b.optString(i);
                    if (!TextUtils.isEmpty((CharSequence)optString)) {
                        final int index = optString.indexOf(":");
                        if (index > 2) {
                            final String substring = optString.substring(2, index);
                            if (hashMap.containsKey(substring)) {
                                final List list = hashMap.get(substring);
                                if (list != null) {
                                    for (final String str : list) {
                                        final StringBuilder sb = new StringBuilder();
                                        sb.append(str);
                                        sb.append(" ");
                                        sb.append(optString);
                                        jsonArray.put((Object)sb.toString());
                                    }
                                    hashMap.remove(substring);
                                }
                            }
                        }
                    }
                }
                final Iterator<List> iterator2 = hashMap.values().iterator();
                while (iterator2.hasNext()) {
                    for (final String str2 : iterator2.next()) {
                        final StringBuilder sb2 = new StringBuilder();
                        sb2.append(str2);
                        sb2.append("  0x000000:unknown");
                        jsonArray.put((Object)sb2.toString());
                    }
                }
                goto Label_0316;
            }
            catch (final IOException ex) {
                goto Label_0316;
            }
            finally {
                final Throwable t;
                c.a().a("NPTH_CATCH", t);
            }
        }
    }
    
    public static class f extends c
    {
        f(final File file) {
            super(file);
            super.b = "Total Threads Count:";
            super.c = ":";
            super.d = -2;
        }
    }
}

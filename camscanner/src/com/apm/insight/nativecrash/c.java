// 
// Decompiled by Procyon v0.6.0
// 

package com.apm.insight.nativecrash;

import java.io.Closeable;
import com.apm.insight.l.k;
import java.io.Reader;
import java.io.BufferedReader;
import java.io.FileReader;
import android.os.Environment;
import com.apm.insight.ICrashFilter;
import androidx.annotation.Nullable;
import com.apm.insight.entity.Header;
import com.apm.insight.l.v;
import com.apm.insight.l.s;
import com.apm.insight.CrashType;
import com.apm.insight.l.w;
import java.util.Iterator;
import org.json.JSONException;
import org.json.JSONArray;
import com.apm.insight.l.i;
import java.util.HashMap;
import com.apm.insight.entity.d;
import java.io.File;
import com.apm.insight.l.o;
import java.util.Map;
import com.apm.insight.runtime.q;
import com.apm.insight.entity.a;
import org.json.JSONObject;
import android.content.Context;

public final class c
{
    private static Boolean d;
    private final Context a;
    private JSONObject b;
    private b c;
    
    public c(final Context a) {
        this.b = null;
        this.a = a;
    }
    
    private String a(final String s) {
        final StringBuilder sb = new StringBuilder();
        try {
            if (s.length() < 16) {
                sb.append(s);
            }
            else {
                sb.append(s.charAt(6));
                sb.append(s.charAt(7));
                sb.append(s.charAt(4));
                sb.append(s.charAt(5));
                sb.append(s.charAt(2));
                sb.append(s.charAt(3));
                sb.append(s.charAt(0));
                sb.append(s.charAt(1));
                sb.append(s.charAt(10));
                sb.append(s.charAt(11));
                sb.append(s.charAt(8));
                sb.append(s.charAt(9));
                sb.append(s.charAt(14));
                sb.append(s.charAt(15));
                sb.append(s.charAt(12));
                sb.append(s.charAt(13));
                if (s.length() >= 32) {
                    sb.append(s, 16, 32);
                    sb.append('0');
                }
            }
        }
        finally {
            final Throwable t;
            com.apm.insight.c.a().a("NPTH_CATCH", t);
        }
        return sb.toString().toUpperCase();
    }
    
    private void a(final com.apm.insight.entity.a a) {
        a.a(this.n());
        a.a("is_native_crash", 1);
        a.a("repack_time", System.currentTimeMillis());
        a.a("crash_uuid", (Object)this.c.b().getName());
        a.a("jiffy", q.a.a());
    }
    
    private void a(final Map<String, String> map) {
        final boolean exists = o.i(this.c.b()).exists();
        final String s = "true";
        String s2;
        if (exists) {
            s2 = "true";
        }
        else {
            s2 = "false";
        }
        map.put("has_fds_file", s2);
        final File h = o.h(this.c.b());
        String s3;
        if (h.exists() && h.length() > 128L) {
            s3 = "true";
        }
        else {
            s3 = "false";
        }
        map.put("has_logcat_file", s3);
        String s4;
        if (o.d(this.c.b()).exists()) {
            s4 = "true";
        }
        else {
            s4 = "false";
        }
        map.put("has_maps_file", s4);
        String s5;
        if (o.b(this.c.b()).exists()) {
            s5 = "true";
        }
        else {
            s5 = "false";
        }
        map.put("has_tombstone_file", s5);
        String s6;
        if (o.k(this.c.b()).exists()) {
            s6 = "true";
        }
        else {
            s6 = "false";
        }
        map.put("has_meminfo_file", s6);
        String s7;
        if (o.j(this.c.b()).exists()) {
            s7 = s;
        }
        else {
            s7 = "false";
        }
        map.put("has_threads_file", s7);
    }
    
    private void b(final com.apm.insight.entity.a a) {
        com.apm.insight.entity.d.b(a.h());
        final HashMap hashMap = new HashMap();
        if (m()) {
            hashMap.put("is_root", "true");
            a.a("is_root", (Object)"true");
        }
        else {
            hashMap.put("is_root", "false");
            a.a("is_root", (Object)"false");
        }
        this.a(hashMap);
        final int o = this.o();
        if (o > 0) {
            if (o > 960) {
                hashMap.put("fd_leak", "true");
            }
            else {
                hashMap.put("fd_leak", "false");
            }
            a.a("fd_count", o);
        }
        final int p = this.p();
        if (p > 0) {
            if (p > 350) {
                hashMap.put("threads_leak", "true");
            }
            else {
                hashMap.put("threads_leak", "false");
            }
            a.a("threads_count", p);
        }
        final int q = this.q();
        if (q > 0) {
            if (q > i()) {
                hashMap.put("memory_leak", "true");
            }
            else {
                hashMap.put("memory_leak", "false");
            }
            a.a("memory_size", q);
        }
        hashMap.put("sdk_version", "1.3.8.nourl-alpha.15");
        hashMap.put("has_java_stack", String.valueOf(a.h().opt("java_data") != null));
        final JSONArray a2 = com.apm.insight.nativecrash.d.a(com.apm.insight.l.o.l(this.c.d), com.apm.insight.l.o.m(this.c.d));
        hashMap.put("leak_threads_count", String.valueOf(a2.length()));
        while (true) {
            if (a2.length() <= 0) {
                break Label_0345;
            }
            try {
                i.a(com.apm.insight.l.o.n(this.c.d), a2, false);
                a.b();
                a.c();
                a.c(hashMap);
            }
            finally {
                continue;
            }
            break;
        }
    }
    
    private void c(final com.apm.insight.entity.a a) {
        final Map<String, String> b = this.c.b.b();
        if (b.isEmpty()) {
            return;
        }
        final JSONArray jsonArray = new JSONArray();
        for (final String s : b.keySet()) {
            final String a2 = this.a(b.get(s));
            try {
                final JSONObject jsonObject = new JSONObject();
                jsonObject.put("lib_name", (Object)s);
                jsonObject.put("lib_uuid", (Object)a2);
                jsonArray.put((Object)jsonObject);
            }
            catch (final JSONException ex) {
                com.apm.insight.c.a().a("NPTH_CATCH", (Throwable)ex);
            }
        }
        a.a("crash_lib_uuid", (Object)jsonArray);
    }
    
    private void d(final com.apm.insight.entity.a a) {
        final File e = o.e(this.c.b());
        if (!e.exists() && this.b == null) {
            a.b(w.a(com.apm.insight.i.g()));
            a.a("has_callback", "false");
            return;
        }
        try {
            JSONObject b;
            if ((b = this.b) == null) {
                b = new JSONObject(i.c(e.getAbsolutePath()));
            }
            a.c(b);
            a.a("has_callback", "true");
            if (a.h().opt("storage") == null) {
                a.b(w.a(com.apm.insight.i.g()));
            }
            s.a(a, a.i(), CrashType.NATIVE);
        }
        finally {
            final Throwable t;
            com.apm.insight.c.a().a("NPTH_CATCH", t);
        }
        final JSONObject h = a.h();
        final long n = -1L;
        final long optLong = h.optLong("crash_time", -1L);
        final long optLong2 = a.h().optLong("java_end", -1L);
        long l = n;
        if (optLong2 != -1L) {
            l = n;
            if (optLong != -1L) {
                l = optLong2 - optLong;
            }
        }
        try {
            a.b("total_cost", String.valueOf(l));
            a.a("total_cost", String.valueOf(l / 1000L));
        }
        finally {}
    }
    
    private void e(final com.apm.insight.entity.a a) {
        final File g = o.g(this.c.b());
        final String str;
        Label_0043: {
            if (g.exists()) {
                try {
                    v.a(g.getAbsolutePath());
                    break Label_0043;
                }
                finally {
                    com.apm.insight.c.a().a("NPTH_CATCH", (Throwable)str);
                }
            }
            str = "";
        }
        final File o = com.apm.insight.l.o.o(this.c.b());
        String str2 = str;
        if (o.exists()) {
            str2 = b(o);
            if (!str.isEmpty()) {
                final StringBuilder sb = new StringBuilder();
                sb.append(str);
                sb.append("\n");
                sb.append(str2);
                str2 = sb.toString();
            }
        }
        try {
            if (!str2.isEmpty()) {
                a.a("java_data", (Object)str2);
            }
        }
        finally {
            final Throwable t;
            com.apm.insight.c.a().a("NPTH_CATCH", t);
        }
    }
    
    private void f(final com.apm.insight.entity.a a) {
        final File a2 = o.a(this.c.b());
        if (!a2.exists()) {
            return;
        }
        try {
            a.a("native_log", (Object)i.b(i.a(a2.getAbsolutePath(), "\n"), "\n"));
        }
        finally {
            final Throwable t;
            com.apm.insight.c.a().a("NPTH_CATCH", t);
        }
    }
    
    private void g(final com.apm.insight.entity.a p0) {
        // 
        // This method could not be decompiled.
        // 
        // Original Bytecode:
        // 
        //     1: getfield        com/apm/insight/nativecrash/c.c:Lcom/apm/insight/nativecrash/c$b;
        //     4: invokevirtual   com/apm/insight/nativecrash/c$b.b:()Ljava/io/File;
        //     7: invokestatic    com/apm/insight/l/o.h:(Ljava/io/File;)Ljava/io/File;
        //    10: astore_2       
        //    11: aload_2        
        //    12: invokevirtual   java/io/File.exists:()Z
        //    15: ifne            43
        //    18: aload_2        
        //    19: invokevirtual   java/io/File.getAbsolutePath:()Ljava/lang/String;
        //    22: invokestatic    com/apm/insight/i.i:()Lcom/apm/insight/runtime/ConfigManager;
        //    25: invokevirtual   com/apm/insight/runtime/ConfigManager.getLogcatDumpCount:()I
        //    28: invokestatic    java/lang/String.valueOf:(I)Ljava/lang/String;
        //    31: invokestatic    com/apm/insight/i.i:()Lcom/apm/insight/runtime/ConfigManager;
        //    34: invokevirtual   com/apm/insight/runtime/ConfigManager.getLogcatLevel:()I
        //    37: invokestatic    java/lang/String.valueOf:(I)Ljava/lang/String;
        //    40: invokestatic    com/apm/insight/nativecrash/NativeImpl.a:(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
        //    43: new             Lorg/json/JSONArray;
        //    46: dup            
        //    47: invokespecial   org/json/JSONArray.<init>:()V
        //    50: astore          5
        //    52: new             Ljava/lang/StringBuilder;
        //    55: dup            
        //    56: invokespecial   java/lang/StringBuilder.<init>:()V
        //    59: astore_3       
        //    60: aload_3        
        //    61: ldc_w           " "
        //    64: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //    67: pop            
        //    68: aload_3        
        //    69: aload_0        
        //    70: getfield        com/apm/insight/nativecrash/c.c:Lcom/apm/insight/nativecrash/c$b;
        //    73: invokestatic    com/apm/insight/nativecrash/c$b.a:(Lcom/apm/insight/nativecrash/c$b;)Lcom/apm/insight/nativecrash/a;
        //    76: invokevirtual   com/apm/insight/nativecrash/a.c:()Ljava/util/Map;
        //    79: ldc_w           "pid"
        //    82: invokeinterface java/util/Map.get:(Ljava/lang/Object;)Ljava/lang/Object;
        //    87: checkcast       Ljava/lang/String;
        //    90: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //    93: pop            
        //    94: aload_3        
        //    95: ldc_w           " "
        //    98: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   101: pop            
        //   102: aload_3        
        //   103: invokevirtual   java/lang/StringBuilder.toString:()Ljava/lang/String;
        //   106: astore          6
        //   108: new             Ljava/io/BufferedReader;
        //   111: astore_3       
        //   112: new             Ljava/io/FileReader;
        //   115: astore          4
        //   117: aload           4
        //   119: aload_2        
        //   120: invokespecial   java/io/FileReader.<init>:(Ljava/io/File;)V
        //   123: aload_3        
        //   124: aload           4
        //   126: invokespecial   java/io/BufferedReader.<init>:(Ljava/io/Reader;)V
        //   129: aload_2        
        //   130: invokevirtual   java/io/File.length:()J
        //   133: ldc2_w          512000
        //   136: lcmp           
        //   137: ifle            153
        //   140: aload_3        
        //   141: aload_2        
        //   142: invokevirtual   java/io/File.length:()J
        //   145: ldc2_w          512000
        //   148: lsub           
        //   149: invokevirtual   java/io/BufferedReader.skip:(J)J
        //   152: pop2           
        //   153: aload_3        
        //   154: invokevirtual   java/io/BufferedReader.readLine:()Ljava/lang/String;
        //   157: astore          4
        //   159: aload_3        
        //   160: astore_2       
        //   161: aload           4
        //   163: ifnull          214
        //   166: aload           4
        //   168: invokevirtual   java/lang/String.length:()I
        //   171: bipush          32
        //   173: if_icmple       188
        //   176: aload           4
        //   178: iconst_0       
        //   179: bipush          31
        //   181: invokevirtual   java/lang/String.substring:(II)Ljava/lang/String;
        //   184: astore_2       
        //   185: goto            191
        //   188: aload           4
        //   190: astore_2       
        //   191: aload_2        
        //   192: aload           6
        //   194: invokevirtual   java/lang/String.contains:(Ljava/lang/CharSequence;)Z
        //   197: ifeq            153
        //   200: aload           5
        //   202: aload           4
        //   204: invokevirtual   org/json/JSONArray.put:(Ljava/lang/Object;)Lorg/json/JSONArray;
        //   207: pop            
        //   208: goto            153
        //   211: astore_2       
        //   212: aconst_null    
        //   213: astore_2       
        //   214: aload_2        
        //   215: invokestatic    com/apm/insight/l/k.a:(Ljava/io/Closeable;)V
        //   218: aload_1        
        //   219: ldc_w           "logcat"
        //   222: aload           5
        //   224: invokevirtual   com/apm/insight/entity/a.a:(Ljava/lang/String;Ljava/lang/Object;)V
        //   227: return         
        //   228: astore_2       
        //   229: aload_3        
        //   230: astore_2       
        //   231: goto            214
        //    Exceptions:
        //  Try           Handler
        //  Start  End    Start  End    Type
        //  -----  -----  -----  -----  ----
        //  108    129    211    214    Any
        //  129    153    228    234    Any
        //  153    159    228    234    Any
        //  166    185    228    234    Any
        //  191    208    228    234    Any
        // 
        // The error that occurred was:
        // 
        // java.lang.IllegalStateException: Expression is linked from several locations: Label_0153:
        //     at com.strobel.decompiler.ast.Error.expressionLinkedFromMultipleLocations(Error.java:27)
        //     at com.strobel.decompiler.ast.AstOptimizer.mergeDisparateObjectInitializations(AstOptimizer.java:2604)
        //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:235)
        //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:42)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:206)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:93)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethodBody(AstBuilder.java:868)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethod(AstBuilder.java:761)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addTypeMembers(AstBuilder.java:638)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeCore(AstBuilder.java:605)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeNoCache(AstBuilder.java:195)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createType(AstBuilder.java:162)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addType(AstBuilder.java:137)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.buildAst(JavaLanguage.java:71)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.decompileType(JavaLanguage.java:59)
        //     at com.strobel.decompiler.DecompilerDriver.decompileType(DecompilerDriver.java:333)
        //     at com.strobel.decompiler.DecompilerDriver.decompileJar(DecompilerDriver.java:254)
        //     at com.strobel.decompiler.DecompilerDriver.main(DecompilerDriver.java:144)
        // 
        throw new IllegalStateException("An error occurred while decompiling this method.");
    }
    
    private void h(final com.apm.insight.entity.a a) {
        final Map<String, String> a2 = this.a();
        if (a2 != null) {
            if (a != null) {
                final String s = a2.get("process_name");
                if (s != null) {
                    a.a("process_name", (Object)s);
                }
                final String nm = a2.get("start_time");
                if (nm != null) {
                    try {
                        a.a(Long.decode(nm));
                    }
                    finally {
                        final Throwable t;
                        com.apm.insight.c.a().a("NPTH_CATCH", t);
                    }
                }
                final String nm2 = a2.get("pid");
                if (nm2 != null) {
                    try {
                        a.a("pid", Long.decode(nm2));
                    }
                    finally {
                        final Throwable t2;
                        com.apm.insight.c.a().a("NPTH_CATCH", t2);
                    }
                }
                final String s2 = a2.get("crash_thread_name");
                if (s2 != null) {
                    a.a("crash_thread_name", (Object)s2);
                }
                final String nm3 = a2.get("crash_time");
                if (nm3 != null) {
                    try {
                        a.a("crash_time", Long.decode(nm3));
                    }
                    finally {
                        final Throwable t3;
                        com.apm.insight.c.a().a("NPTH_CATCH", t3);
                    }
                }
                a.a("data", (Object)this.b());
            }
        }
    }
    
    public static long i() {
        if (NativeImpl.e()) {
            return Long.MAX_VALUE;
        }
        long n;
        if (Header.a()) {
            n = 3891200L;
        }
        else {
            n = 2867200L;
        }
        return n;
    }
    
    public static boolean m() {
        final Boolean d = c.d;
        if (d != null) {
            return d;
        }
        for (int i = 0; i < 11; ++i) {
            final String pathname = (new String[] { "/data/local/su", "/data/local/bin/su", "/data/local/xbin/su", "/system/xbin/su", "/system/bin/su", "/system/bin/.ext/su", "/system/bin/failsafe/su", "/system/sd/xbin/su", "/system/usr/we-need-root/su", "/sbin/su", "/su/bin/su" })[i];
            try {
                if (new File(pathname).exists()) {
                    c.d = Boolean.TRUE;
                    return true;
                }
            }
            finally {
                final Throwable t;
                c.a().a("NPTH_CATCH", t);
            }
        }
        c.d = Boolean.FALSE;
        return false;
    }
    
    private Header n() {
        final Header header = new Header(this.a);
        final JSONObject a = com.apm.insight.runtime.s.a().a(this.c.a());
        if (a != null) {
            header.a(a);
            header.d();
            header.e();
        }
        Header.b(header);
        return header;
    }
    
    private int o() {
        return ((e)new c()).a();
    }
    
    private int p() {
        return ((e)new f()).a();
    }
    
    private int q() {
        return ((e)new d()).a();
    }
    
    public Map<String, String> a() {
        final b c = this.c;
        if (c != null) {
            return c.c.c();
        }
        return null;
    }
    
    public void a(final File file) {
        this.c = new b(file);
    }
    
    @Nullable
    public String b() {
        final b c = this.c;
        if (c != null) {
            final String c2 = c.b.c();
            if (c2 != null) {
                final String b = c2;
                if (!c2.isEmpty()) {
                    return b;
                }
            }
            return this.c.c.b();
        }
        return null;
    }
    
    public boolean c() {
        final b c = this.c;
        return c != null && c.c();
    }
    
    public JSONObject d() {
        final File f = o.f(this.c.b());
        if (!f.exists()) {
            return null;
        }
        try {
            final String c = i.c(f.getAbsolutePath());
            if (c != null && !c.isEmpty()) {
                return new JSONObject(c);
            }
        }
        finally {
            final Throwable t;
            com.apm.insight.c.a().a("NPTH_CATCH", t);
        }
        return null;
    }
    
    public void e() {
        // 
        // This method could not be decompiled.
        // 
        // Original Bytecode:
        // 
        //     1: getfield        com/apm/insight/nativecrash/c.c:Lcom/apm/insight/nativecrash/c$b;
        //     4: invokevirtual   com/apm/insight/nativecrash/c$b.b:()Ljava/io/File;
        //     7: invokestatic    com/apm/insight/l/o.e:(Ljava/io/File;)Ljava/io/File;
        //    10: astore          4
        //    12: new             Ljava/io/File;
        //    15: astore          5
        //    17: new             Ljava/lang/StringBuilder;
        //    20: astore          6
        //    22: aload           6
        //    24: invokespecial   java/lang/StringBuilder.<init>:()V
        //    27: aload           6
        //    29: aload           4
        //    31: invokevirtual   java/io/File.getAbsolutePath:()Ljava/lang/String;
        //    34: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //    37: pop            
        //    38: aload           6
        //    40: ldc_w           ".tmp'"
        //    43: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //    46: pop            
        //    47: aload           5
        //    49: aload           6
        //    51: invokevirtual   java/lang/StringBuilder.toString:()Ljava/lang/String;
        //    54: invokespecial   java/io/File.<init>:(Ljava/lang/String;)V
        //    57: aload           5
        //    59: invokevirtual   java/io/File.exists:()Z
        //    62: ifeq            71
        //    65: aload           5
        //    67: invokevirtual   java/io/File.delete:()Z
        //    70: pop            
        //    71: aload           4
        //    73: invokevirtual   java/io/File.exists:()Z
        //    76: istore_3       
        //    77: iconst_0       
        //    78: istore_2       
        //    79: iconst_0       
        //    80: istore_1       
        //    81: iload_3        
        //    82: ifeq            163
        //    85: iload_1        
        //    86: invokestatic    com/apm/insight/nativecrash/b.a:()I
        //    89: if_icmpge       473
        //    92: new             Ljava/io/File;
        //    95: astore          5
        //    97: new             Ljava/lang/StringBuilder;
        //   100: astore          6
        //   102: aload           6
        //   104: invokespecial   java/lang/StringBuilder.<init>:()V
        //   107: aload           6
        //   109: aload           4
        //   111: invokevirtual   java/io/File.getAbsolutePath:()Ljava/lang/String;
        //   114: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   117: pop            
        //   118: aload           6
        //   120: bipush          46
        //   122: invokevirtual   java/lang/StringBuilder.append:(C)Ljava/lang/StringBuilder;
        //   125: pop            
        //   126: aload           6
        //   128: iload_1        
        //   129: invokevirtual   java/lang/StringBuilder.append:(I)Ljava/lang/StringBuilder;
        //   132: pop            
        //   133: aload           5
        //   135: aload           6
        //   137: invokevirtual   java/lang/StringBuilder.toString:()Ljava/lang/String;
        //   140: invokespecial   java/io/File.<init>:(Ljava/lang/String;)V
        //   143: aload           5
        //   145: invokevirtual   java/io/File.exists:()Z
        //   148: ifeq            157
        //   151: aload           5
        //   153: invokevirtual   java/io/File.delete:()Z
        //   156: pop            
        //   157: iinc            1, 1
        //   160: goto            85
        //   163: new             Lcom/apm/insight/entity/a;
        //   166: astore          6
        //   168: aload           6
        //   170: invokespecial   com/apm/insight/entity/a.<init>:()V
        //   173: iconst_0       
        //   174: istore_1       
        //   175: iload_1        
        //   176: invokestatic    com/apm/insight/nativecrash/b.a:()I
        //   179: if_icmpge       312
        //   182: new             Ljava/io/File;
        //   185: astore          7
        //   187: new             Ljava/lang/StringBuilder;
        //   190: astore          8
        //   192: aload           8
        //   194: invokespecial   java/lang/StringBuilder.<init>:()V
        //   197: aload           8
        //   199: aload           4
        //   201: invokevirtual   java/io/File.getAbsolutePath:()Ljava/lang/String;
        //   204: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   207: pop            
        //   208: aload           8
        //   210: bipush          46
        //   212: invokevirtual   java/lang/StringBuilder.append:(C)Ljava/lang/StringBuilder;
        //   215: pop            
        //   216: aload           8
        //   218: iload_1        
        //   219: invokevirtual   java/lang/StringBuilder.append:(I)Ljava/lang/StringBuilder;
        //   222: pop            
        //   223: aload           7
        //   225: aload           8
        //   227: invokevirtual   java/lang/StringBuilder.toString:()Ljava/lang/String;
        //   230: invokespecial   java/io/File.<init>:(Ljava/lang/String;)V
        //   233: aload           7
        //   235: invokevirtual   java/io/File.exists:()Z
        //   238: istore_3       
        //   239: iload_3        
        //   240: ifne            246
        //   243: goto            306
        //   246: aload           7
        //   248: invokevirtual   java/io/File.getAbsolutePath:()Ljava/lang/String;
        //   251: invokestatic    com/apm/insight/l/i.c:(Ljava/lang/String;)Ljava/lang/String;
        //   254: astore          8
        //   256: aload           8
        //   258: invokestatic    android/text/TextUtils.isEmpty:(Ljava/lang/CharSequence;)Z
        //   261: ifne            306
        //   264: new             Lorg/json/JSONObject;
        //   267: astore          7
        //   269: aload           7
        //   271: aload           8
        //   273: invokespecial   org/json/JSONObject.<init>:(Ljava/lang/String;)V
        //   276: aload           7
        //   278: invokevirtual   org/json/JSONObject.length:()I
        //   281: ifle            306
        //   284: aload           6
        //   286: aload           7
        //   288: invokevirtual   com/apm/insight/entity/a.c:(Lorg/json/JSONObject;)V
        //   291: goto            306
        //   294: astore          7
        //   296: invokestatic    com/apm/insight/c.a:()Lcom/apm/insight/d;
        //   299: ldc             "NPTH_CATCH"
        //   301: aload           7
        //   303: invokevirtual   com/apm/insight/d.a:(Ljava/lang/String;Ljava/lang/Throwable;)V
        //   306: iinc            1, 1
        //   309: goto            175
        //   312: aload           6
        //   314: invokevirtual   com/apm/insight/entity/a.h:()Lorg/json/JSONObject;
        //   317: astore          7
        //   319: aload           7
        //   321: invokevirtual   org/json/JSONObject.length:()I
        //   324: ifeq            349
        //   327: aload           7
        //   329: ldc_w           "storage"
        //   332: invokevirtual   org/json/JSONObject.opt:(Ljava/lang/String;)Ljava/lang/Object;
        //   335: ifnonnull       349
        //   338: aload           7
        //   340: invokestatic    com/apm/insight/i.g:()Landroid/content/Context;
        //   343: invokestatic    com/apm/insight/l/w.a:(Landroid/content/Context;)Lorg/json/JSONObject;
        //   346: invokestatic    com/apm/insight/entity/a.a:(Lorg/json/JSONObject;Lorg/json/JSONObject;)V
        //   349: aload           7
        //   351: invokevirtual   org/json/JSONObject.length:()I
        //   354: ifeq            473
        //   357: aload_0        
        //   358: aload           7
        //   360: putfield        com/apm/insight/nativecrash/c.b:Lorg/json/JSONObject;
        //   363: aload           5
        //   365: aload           7
        //   367: iconst_0       
        //   368: invokestatic    com/apm/insight/l/i.b:(Ljava/io/File;Lorg/json/JSONObject;Z)V
        //   371: aload           5
        //   373: aload           4
        //   375: invokevirtual   java/io/File.renameTo:(Ljava/io/File;)Z
        //   378: ifeq            473
        //   381: iload_2        
        //   382: istore_1       
        //   383: iload_1        
        //   384: invokestatic    com/apm/insight/nativecrash/b.a:()I
        //   387: if_icmpge       473
        //   390: new             Ljava/io/File;
        //   393: astore          6
        //   395: new             Ljava/lang/StringBuilder;
        //   398: astore          5
        //   400: aload           5
        //   402: invokespecial   java/lang/StringBuilder.<init>:()V
        //   405: aload           5
        //   407: aload           4
        //   409: invokevirtual   java/io/File.getAbsolutePath:()Ljava/lang/String;
        //   412: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   415: pop            
        //   416: aload           5
        //   418: bipush          46
        //   420: invokevirtual   java/lang/StringBuilder.append:(C)Ljava/lang/StringBuilder;
        //   423: pop            
        //   424: aload           5
        //   426: iload_1        
        //   427: invokevirtual   java/lang/StringBuilder.append:(I)Ljava/lang/StringBuilder;
        //   430: pop            
        //   431: aload           6
        //   433: aload           5
        //   435: invokevirtual   java/lang/StringBuilder.toString:()Ljava/lang/String;
        //   438: invokespecial   java/io/File.<init>:(Ljava/lang/String;)V
        //   441: aload           6
        //   443: invokevirtual   java/io/File.exists:()Z
        //   446: ifeq            455
        //   449: aload           6
        //   451: invokevirtual   java/io/File.delete:()Z
        //   454: pop            
        //   455: iinc            1, 1
        //   458: goto            383
        //   461: astore          4
        //   463: invokestatic    com/apm/insight/c.a:()Lcom/apm/insight/d;
        //   466: ldc             "NPTH_CATCH"
        //   468: aload           4
        //   470: invokevirtual   com/apm/insight/d.a:(Ljava/lang/String;Ljava/lang/Throwable;)V
        //   473: return         
        //   474: astore          6
        //   476: goto            349
        //    Exceptions:
        //  Try           Handler
        //  Start  End    Start  End    Type                    
        //  -----  -----  -----  -----  ------------------------
        //  0      71     461    473    Ljava/io/IOException;
        //  71     77     461    473    Ljava/io/IOException;
        //  85     157    461    473    Ljava/io/IOException;
        //  163    173    461    473    Ljava/io/IOException;
        //  175    239    461    473    Ljava/io/IOException;
        //  246    291    294    306    Lorg/json/JSONException;
        //  246    291    461    473    Ljava/io/IOException;
        //  296    306    461    473    Ljava/io/IOException;
        //  312    319    461    473    Ljava/io/IOException;
        //  319    349    474    479    Any
        //  349    381    461    473    Ljava/io/IOException;
        //  383    455    461    473    Ljava/io/IOException;
        // 
        // The error that occurred was:
        // 
        // java.lang.IllegalStateException: Expression is linked from several locations: Label_0349:
        //     at com.strobel.decompiler.ast.Error.expressionLinkedFromMultipleLocations(Error.java:27)
        //     at com.strobel.decompiler.ast.AstOptimizer.mergeDisparateObjectInitializations(AstOptimizer.java:2604)
        //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:235)
        //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:42)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:206)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:93)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethodBody(AstBuilder.java:868)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethod(AstBuilder.java:761)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addTypeMembers(AstBuilder.java:638)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeCore(AstBuilder.java:605)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeNoCache(AstBuilder.java:195)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createType(AstBuilder.java:162)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addType(AstBuilder.java:137)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.buildAst(JavaLanguage.java:71)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.decompileType(JavaLanguage.java:59)
        //     at com.strobel.decompiler.DecompilerDriver.decompileType(DecompilerDriver.java:333)
        //     at com.strobel.decompiler.DecompilerDriver.decompileJar(DecompilerDriver.java:254)
        //     at com.strobel.decompiler.DecompilerDriver.main(DecompilerDriver.java:144)
        // 
        throw new IllegalStateException("An error occurred while decompiling this method.");
    }
    
    public boolean f() {
        final ICrashFilter b = com.apm.insight.i.b().b();
        if (b != null) {
            try {
                if (!b.onNativeCrashFilter(this.b(), "")) {
                    return false;
                }
            }
            finally {
                final Throwable t;
                com.apm.insight.c.a().a("NPTH_CATCH", t);
            }
        }
        return true;
    }
    
    public boolean g() {
        return com.apm.insight.e.a.a().a(o.f(this.c.b()).getAbsolutePath());
    }
    
    public void h() {
        com.apm.insight.e.a.a().a(com.apm.insight.e.a.a.a(o.f(this.c.b()).getAbsolutePath()));
    }
    
    public JSONObject j() {
        try {
            final com.apm.insight.entity.a a = new com.apm.insight.entity.a();
            this.a(a);
            this.h(a);
            this.c(a);
            this.d(a);
            this.e(a);
            this.g(a);
            this.f(a);
            this.b(a);
            final File f = o.f(this.c.b());
            final JSONObject h = a.h();
            i.a(f, h, false);
            return h;
        }
        finally {
            final Throwable t;
            com.apm.insight.c.a().a("NPTH_CATCH", t);
            return null;
        }
    }
    
    public boolean k() {
        return i.a(this.c.b());
    }
    
    public void l() {
        try {
            final String absolutePath = this.c.b().getAbsolutePath();
            final StringBuilder sb = new StringBuilder();
            sb.append(Environment.getExternalStorageDirectory().getAbsolutePath());
            sb.append("/");
            sb.append("localDebug");
            sb.append("/");
            sb.append(com.apm.insight.i.g().getPackageName());
            sb.append("/");
            sb.append(this.c.b().getName());
            sb.append(".zip");
            i.c(absolutePath, sb.toString());
        }
        finally {
            final Throwable t;
            com.apm.insight.c.a().a("NPTH_CATCH", t);
        }
    }
    
    private static class a
    {
        private static String b(File file) {
            Label_0163: {
                try {
                    final BufferedReader bufferedReader = new BufferedReader(new FileReader(file));
                    try {
                        final String line = bufferedReader.readLine();
                        if (line == null) {
                            k.a(bufferedReader);
                            return "";
                        }
                        if (line.startsWith("[FATAL:jni_android.cc") && line.contains("Please include Java exception stack in crash report ttwebview:")) {
                            final StringBuilder sb = new StringBuilder();
                            final int index = line.indexOf(" ttwebview:");
                            sb.append("Caused by: ");
                            sb.append("Please include Java exception stack in crash report");
                            sb.append("\n");
                            String str = line.substring(index + 11);
                            do {
                                sb.append(str);
                                sb.append("\n");
                                str = bufferedReader.readLine();
                            } while (str != null);
                            final String string = sb.toString();
                            k.a(bufferedReader);
                            return string;
                        }
                        break Label_0163;
                    }
                    finally {}
                }
                finally {
                    file = null;
                }
                try {
                    final Throwable t;
                    c.a().a("NPTH_CATCH", t);
                    k.a((Closeable)file);
                    return "";
                }
                finally {
                    k.a((Closeable)file);
                }
            }
        }
    }
    
    private class b
    {
        final c a;
        private final com.apm.insight.nativecrash.e b;
        private final com.apm.insight.nativecrash.a c;
        private final File d;
        private final File e;
        
        public b(final c a, final File d) {
            this.a = a;
            this.d = d;
            this.e = o.a(com.apm.insight.i.g(), d.getName());
            final com.apm.insight.nativecrash.a c = new com.apm.insight.nativecrash.a(d);
            this.c = c;
            final com.apm.insight.nativecrash.e b = new com.apm.insight.nativecrash.e(d);
            this.b = b;
            if (c.a() && b.a() == null) {
                b.a(d);
            }
        }
        
        public long a() {
            final Map<String, String> c = this.c.c();
            final Throwable s2;
            Label_0042: {
                Label_0040: {
                    if (c != null) {
                        Label_0053: {
                            try {
                                if (!c.isEmpty()) {
                                    final String s = c.get("start_time");
                                    break Label_0042;
                                }
                            }
                            finally {
                                break Label_0053;
                            }
                            break Label_0040;
                        }
                        com.apm.insight.c.a().a("NPTH_CATCH", s2);
                        return System.currentTimeMillis();
                    }
                }
                s2 = null;
            }
            if (s2 != null) {
                return Long.parseLong((String)s2);
            }
            return System.currentTimeMillis();
        }
        
        public File b() {
            return this.d;
        }
        
        public boolean c() {
            return this.c.a();
        }
    }
    
    public class c extends e
    {
        final com.apm.insight.nativecrash.c a;
        
        c(final com.apm.insight.nativecrash.c a) {
            this.a = a.super();
            super.c = "Total FD Count:";
            super.b = o.i(a.c.b());
            super.d = ":";
            super.e = -2;
        }
    }
    
    public class e
    {
        protected File b;
        protected String c;
        protected String d;
        protected int e;
        final c f;
        
        public e(final c f) {
            this.f = f;
        }
        
        public int a() {
            if (this.b.exists()) {
                if (this.b.isFile()) {
                    Label_0127: {
                        Closeable closeable;
                        int n2;
                        try {
                            final BufferedReader bufferedReader = new BufferedReader(new FileReader(this.b));
                            try {
                                while (true) {
                                    final String line = bufferedReader.readLine();
                                    if (line == null) {
                                        break Label_0127;
                                    }
                                    final int a = this.a(line);
                                    if (a != -1) {
                                        break Label_0127;
                                    }
                                    final int n = a;
                                }
                            }
                            finally {}
                        }
                        finally {
                            closeable = null;
                            n2 = -1;
                        }
                        try {
                            Closeable closeable2 = null;
                            com.apm.insight.c.a().a("NPTH_CATCH", (Throwable)closeable2);
                            int n3 = n2;
                            if (closeable != null) {
                                closeable2 = closeable;
                                k.a(closeable2);
                                n3 = n2;
                            }
                            return n3;
                        }
                        finally {
                            if (closeable != null) {
                                k.a(closeable);
                            }
                        }
                    }
                }
            }
            return -1;
        }
        
        public int a(final String s) {
            int e;
            int int1 = e = this.e;
            if (s.startsWith(this.c)) {
                final String[] split = s.split(this.d);
                try {
                    int1 = Integer.parseInt(split[1].trim());
                }
                catch (final NumberFormatException ex) {
                    com.apm.insight.c.a().a("NPTH_CATCH", ex);
                }
                e = int1;
                if (int1 < 0) {
                    e = -2;
                }
            }
            return e;
        }
    }
    
    public class d extends e
    {
        final c a;
        
        d(final c a) {
            this.a = a.super();
            super.c = "VmSize:";
            super.b = o.k(a.c.b());
            super.d = "\\s+";
            super.e = -1;
        }
    }
    
    public class f extends e
    {
        final c a;
        
        f(final c a) {
            this.a = a.super();
            super.c = "Total Threads Count:";
            super.b = o.j(a.c.b());
            super.d = ":";
            super.e = -2;
        }
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.apm.insight.nativecrash;

import com.apm.insight.b.g;
import com.apm.insight.b.d;
import com.apm.insight.b.f;
import com.apm.insight.l.x;
import com.apm.insight.c;
import androidx.annotation.Keep;
import com.apm.insight.i;
import android.os.Build$VERSION;
import com.apm.insight.h.b;
import androidx.annotation.NonNull;
import android.content.Context;
import com.apm.insight.l.u;
import com.apm.insight.l.o;
import java.io.File;
import android.text.TextUtils;

public class NativeImpl
{
    private static volatile boolean a = false;
    private static volatile boolean b = false;
    private static boolean c = true;
    
    public static int a(int doLock) {
        if (!NativeImpl.a) {
            return -1;
        }
        if (doLock < 0) {
            return -1;
        }
        try {
            doLock = doLock("", doLock);
            return doLock;
        }
        finally {
            return -1;
        }
    }
    
    public static String a(final String s) {
        if (!NativeImpl.a) {
            return null;
        }
        return doGetCrashHeader(s);
    }
    
    public static void a(final int n, final String s) {
        if (!NativeImpl.a) {
            return;
        }
        if (TextUtils.isEmpty((CharSequence)s)) {
            return;
        }
        try {
            doWriteFile(n, s, s.length());
        }
        finally {}
    }
    
    public static void a(final long n) {
    }
    
    public static void a(final File file) {
        if (!NativeImpl.a) {
            return;
        }
        doRebuildTombstone(o.c(file).getAbsolutePath(), o.b(file).getAbsolutePath(), o.d(file).getAbsolutePath());
    }
    
    public static void a(final String s, final String s2, final String s3) {
        if (!NativeImpl.a) {
            return;
        }
        try {
            doDumpLogcat(s, s2, s3);
        }
        finally {}
    }
    
    public static void a(final boolean c) {
        NativeImpl.c = c;
        if (!NativeImpl.a) {
            return;
        }
        doSetResendSigQuit(c ? 1 : 0);
    }
    
    public static boolean a() {
        if (NativeImpl.b) {
            return NativeImpl.a;
        }
        NativeImpl.b = true;
        if (!NativeImpl.a) {
            NativeImpl.a = u.a("apminsighta");
        }
        return NativeImpl.a;
    }
    
    public static boolean a(@NonNull final Context context) {
        final boolean a = a();
        if (a) {
            final StringBuilder sb = new StringBuilder();
            sb.append(o.j(context));
            sb.append("/apminsight");
            final String string = sb.toString();
            String s;
            if (new File(context.getApplicationInfo().nativeLibraryDir, "libapminsightb.so").exists()) {
                s = context.getApplicationInfo().nativeLibraryDir;
            }
            else {
                s = com.apm.insight.h.b.a();
                com.apm.insight.h.b.b("apminsightb");
            }
            doStart(Build$VERSION.SDK_INT, s, string, i.f(), i.l());
        }
        return a;
    }
    
    public static int b() {
        if (!NativeImpl.a) {
            return -1;
        }
        return doCreateCallbackThread();
    }
    
    public static void b(final int n) {
        if (!NativeImpl.a) {
            return;
        }
        try {
            doCloseFile(n);
        }
        finally {}
    }
    
    public static void b(final long n) {
        if (!NativeImpl.a) {
            return;
        }
        try {
            doSetAlogFlushAddr(n);
        }
        finally {}
    }
    
    public static void b(final String s) {
        if (!NativeImpl.a) {
            return;
        }
        doDumpHprof(s);
    }
    
    public static int c(final String s) {
        if (!NativeImpl.a) {
            return -1;
        }
        if (TextUtils.isEmpty((CharSequence)s)) {
            return -1;
        }
        try {
            return doLock(s, -1);
        }
        finally {
            return -1;
        }
    }
    
    public static long c(final int n) {
        if (!NativeImpl.a) {
            return 0L;
        }
        return doGetThreadCpuTime(n);
    }
    
    public static void c() {
    }
    
    public static void c(final long n) {
        if (!NativeImpl.a) {
            return;
        }
        try {
            doSetAlogLogDirAddr(n);
        }
        finally {}
    }
    
    public static void d(final String s) {
        if (!NativeImpl.a) {
            return;
        }
        try {
            doDumpMemInfo(s);
        }
        finally {}
    }
    
    public static boolean d() {
        if (!NativeImpl.a) {
            return false;
        }
        try {
            return doCheckNativeCrash();
        }
        finally {
            return false;
        }
    }
    
    @Keep
    private static native boolean doCheckNativeCrash();
    
    @Keep
    private static native void doCloseFile(final int p0);
    
    @Keep
    private static native int doCreateCallbackThread();
    
    @Keep
    private static native void doDump(final String p0);
    
    @Keep
    private static native void doDumpFds(final String p0);
    
    @Keep
    private static native void doDumpHprof(final String p0);
    
    @Keep
    private static native void doDumpLogcat(final String p0, final String p1, final String p2);
    
    @Keep
    private static native void doDumpMaps(final String p0);
    
    @Keep
    private static native void doDumpMemInfo(final String p0);
    
    @Keep
    private static native void doDumpThreads(final String p0);
    
    @Keep
    private static native long doGetAppCpuTime();
    
    @Keep
    private static native long doGetChildCpuTime();
    
    @Keep
    private static native String doGetCrashHeader(final String p0);
    
    @Keep
    private static native long doGetDeviceCpuTime();
    
    @Keep
    private static native int doGetFDCount();
    
    @Keep
    private static native String[] doGetFdDump(final int p0, final int p1, final int[] p2, final String[] p3);
    
    @Keep
    private static native long doGetFreeMemory();
    
    @Keep
    private static native long doGetThreadCpuTime(final int p0);
    
    @Keep
    private static native int doGetThreadsCount();
    
    @Keep
    private static native long doGetTotalMemory();
    
    @Keep
    private static native long doGetVMSize();
    
    @Keep
    private static native void doInitThreadDump();
    
    @Keep
    private static native int doLock(final String p0, final int p1);
    
    @Keep
    private static native int doOpenFile(final String p0);
    
    @Keep
    private static native void doRebuildTombstone(final String p0, final String p1, final String p2);
    
    @Keep
    private static native void doSetAlogConfigPath(final String p0);
    
    @Keep
    private static native void doSetAlogFlushAddr(final long p0);
    
    @Keep
    private static native void doSetAlogLogDirAddr(final long p0);
    
    @Keep
    private static native void doSetResendSigQuit(final int p0);
    
    @Keep
    private static native void doSetUploadEnd();
    
    @Keep
    private static native void doSignalMainThread();
    
    @Keep
    private static native int doStart(final int p0, final String p1, final String p2, final String p3, final int p4);
    
    @Keep
    private static native void doStartAnrMonitor(final int p0);
    
    @Keep
    private static native void doWriteFile(final int p0, final String p1, final int p2);
    
    public static void e(final String s) {
        if (!NativeImpl.a) {
            return;
        }
        try {
            doDumpFds(s);
        }
        finally {}
    }
    
    public static boolean e() {
        if (!NativeImpl.a) {
            return false;
        }
        try {
            return is64Bit();
        }
        finally {
            return false;
        }
    }
    
    public static void f() {
        x.a(new Runnable() {
            @Override
            public void run() {
                final Throwable t2;
                try {
                    l();
                    return;
                }
                finally {
                    final Throwable t = t2;
                    final String s = "NPTH_ANR_MONITOR_ERROR";
                    com.apm.insight.c.a(t, s);
                }
                try {
                    final Throwable t = t2;
                    final String s = "NPTH_ANR_MONITOR_ERROR";
                    com.apm.insight.c.a(t, s);
                }
                finally {}
            }
        }, "NPTH-AnrMonitor");
    }
    
    public static void f(final String s) {
        if (!NativeImpl.a) {
            return;
        }
        try {
            doDumpMaps(s);
        }
        finally {}
    }
    
    public static void g(final String s) {
        if (!NativeImpl.a) {
            return;
        }
        try {
            doDumpThreads(s);
        }
        finally {}
    }
    
    public static boolean g() {
        return NativeImpl.c;
    }
    
    public static int h(final String s) {
        if (!NativeImpl.a) {
            return -1;
        }
        try {
            return doOpenFile(s);
        }
        finally {
            return -1;
        }
    }
    
    public static void h() {
        if (!NativeImpl.a) {
            return;
        }
        doSignalMainThread();
    }
    
    @Keep
    private static void handleNativeCrash(final String s) {
        com.apm.insight.nativecrash.b.onNativeCrash(s);
    }
    
    public static void i() {
        if (!NativeImpl.a) {
            return;
        }
        doSetUploadEnd();
    }
    
    public static void i(final String s) {
        if (!NativeImpl.a) {
            return;
        }
        doDump(s);
    }
    
    @Keep
    private static native boolean is64Bit();
    
    public static void j() {
        if (!NativeImpl.a) {
            return;
        }
        doInitThreadDump();
    }
    
    private static void l() {
        if (!NativeImpl.a) {
            return;
        }
        try {
            doStartAnrMonitor(Build$VERSION.SDK_INT);
        }
        finally {}
    }
    
    @Keep
    private static void reportEventForAnrMonitor() {
        try {
            System.currentTimeMillis();
            i.j();
            f.b(true);
            d.b();
            g.a(i.g()).a().e();
        }
        finally {}
    }
}

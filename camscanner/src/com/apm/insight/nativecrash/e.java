// 
// Decompiled by Procyon v0.6.0
// 

package com.apm.insight.nativecrash;

import java.util.regex.Matcher;
import java.io.Closeable;
import com.apm.insight.l.k;
import com.apm.insight.c;
import java.io.Reader;
import java.io.BufferedReader;
import java.io.FileReader;
import com.apm.insight.l.o;
import java.util.HashMap;
import java.io.File;
import java.util.Map;
import java.util.regex.Pattern;

public class e
{
    private static final Pattern i;
    private static final Pattern j;
    private static final Pattern k;
    private static final Pattern l;
    private static final Pattern m;
    private String a;
    private String b;
    private String c;
    private String d;
    private String e;
    private String f;
    private String g;
    private Map<String, String> h;
    
    static {
        i = Pattern.compile("^pid:\\s(.*),\\stid:\\s(.*),\\sname:\\s(.*)\\s+>>>\\s(.*)\\s<<<$");
        j = Pattern.compile("^signal\\s(.*),\\scode\\s(.*),\\sfault\\saddr\\s(.*)$");
        k = Pattern.compile("^Abort message: (.*)$");
        l = Pattern.compile("^Crash message: (.*)$");
        m = Pattern.compile("^    \\/(\\w*)\\/.*\\/(.*\\.so)\\s\\(BuildId: ([a-f0-9]*)\\)$");
    }
    
    public e(final File file) {
        this.h = new HashMap<String, String>();
        this.c(o.b(file));
    }
    
    private void c(File file) {
        if (file.exists()) {
            if (file.length() != 0L) {
                Label_0766: {
                    try {
                        final BufferedReader bufferedReader = new BufferedReader(new FileReader(file));
                        int n = 0;
                        try {
                            while (true) {
                                final String line = bufferedReader.readLine();
                                if (line == null || n >= 64) {
                                    break Label_0766;
                                }
                                int n2 = 0;
                                Label_0733: {
                                    if (this.a == null && line.startsWith("pid: ")) {
                                        final Matcher matcher = com.apm.insight.nativecrash.e.i.matcher(line);
                                        n2 = n;
                                        if (matcher.find()) {
                                            n2 = n;
                                            if (matcher.groupCount() == 4) {
                                                this.a = matcher.group(1);
                                                this.b = matcher.group(2);
                                                this.d = matcher.group(3);
                                                this.c = matcher.group(4);
                                                n2 = n;
                                            }
                                        }
                                    }
                                    else if (this.e == null && line.startsWith("signal ")) {
                                        final Matcher matcher2 = com.apm.insight.nativecrash.e.j.matcher(line);
                                        n2 = n;
                                        if (matcher2.find()) {
                                            n2 = n;
                                            if (matcher2.groupCount() == 3) {
                                                final String replace = matcher2.group(1).replace(" ", "");
                                                final String replace2 = matcher2.group(2).replace(" ", "");
                                                final int index = replace2.indexOf("frompid");
                                                String string = replace2;
                                                if (index > 0) {
                                                    final StringBuilder sb = new StringBuilder();
                                                    sb.append(replace2.substring(0, index));
                                                    sb.append(")");
                                                    string = sb.toString();
                                                }
                                                final StringBuilder sb2 = new StringBuilder();
                                                sb2.append("Signal ");
                                                sb2.append(replace);
                                                sb2.append(", Code ");
                                                sb2.append(string);
                                                sb2.append("\n");
                                                this.e = sb2.toString();
                                                n2 = n;
                                            }
                                        }
                                    }
                                    else {
                                        StringBuilder sb3;
                                        if (this.f == null && line.startsWith("Abort ")) {
                                            final Matcher matcher3 = com.apm.insight.nativecrash.e.k.matcher(line);
                                            n2 = n;
                                            if (!matcher3.find()) {
                                                break Label_0733;
                                            }
                                            n2 = n;
                                            if (matcher3.groupCount() != 1) {
                                                break Label_0733;
                                            }
                                            sb3 = new StringBuilder();
                                            sb3.append("abort message: ");
                                            sb3.append(matcher3.group(1));
                                            sb3.append("\n");
                                        }
                                        else if (this.f == null && line.startsWith("Crash ")) {
                                            final Matcher matcher4 = com.apm.insight.nativecrash.e.l.matcher(line);
                                            n2 = n;
                                            if (!matcher4.find()) {
                                                break Label_0733;
                                            }
                                            n2 = n;
                                            if (matcher4.groupCount() != 1) {
                                                break Label_0733;
                                            }
                                            sb3 = new StringBuilder();
                                            sb3.append("crash message: ");
                                            sb3.append(matcher4.group(1));
                                            sb3.append("\n");
                                        }
                                        else {
                                            if (this.g == null && line.startsWith("backtrace:")) {
                                                final StringBuilder sb4 = new StringBuilder();
                                                while (true) {
                                                    final String line2 = bufferedReader.readLine();
                                                    if (line2 == null || !line2.startsWith("    #")) {
                                                        break;
                                                    }
                                                    sb4.append(line2.substring(4));
                                                    sb4.append('\n');
                                                }
                                                n2 = n + 1;
                                                this.g = sb4.toString();
                                                break Label_0733;
                                            }
                                            n2 = n;
                                            if (!this.h.isEmpty()) {
                                                break Label_0733;
                                            }
                                            n2 = n;
                                            if (line.startsWith("build id:")) {
                                                break;
                                            }
                                            break Label_0733;
                                        }
                                        this.f = sb3.toString();
                                        n2 = n;
                                    }
                                }
                                n = n2 + 1;
                            }
                            while (true) {
                                final String line3 = bufferedReader.readLine();
                                if (line3 == null) {
                                    break Label_0766;
                                }
                                if (!line3.contains("BuildId:")) {
                                    break;
                                }
                                final Matcher matcher5 = com.apm.insight.nativecrash.e.m.matcher(line3);
                                if (!matcher5.find()) {
                                    continue;
                                }
                                final String group = matcher5.group(1);
                                final String group2 = matcher5.group(2);
                                final String group3 = matcher5.group(3);
                                if (!group.equals("data")) {
                                    continue;
                                }
                                this.h.put(group2, group3);
                            }
                        }
                        finally {}
                    }
                    finally {
                        file = null;
                    }
                    try {
                        final Throwable t;
                        com.apm.insight.c.a().a("NPTH_CATCH", t);
                        com.apm.insight.l.k.a((Closeable)file);
                    }
                    finally {
                        com.apm.insight.l.k.a((Closeable)file);
                    }
                }
            }
        }
    }
    
    public String a() {
        return this.g;
    }
    
    public void a(final File file) {
        final File b = o.b(file);
        if (b.exists()) {
            final StringBuilder sb = new StringBuilder();
            sb.append(b.getAbsoluteFile());
            sb.append(".old");
            b.renameTo(new File(sb.toString()));
        }
        NativeImpl.a(file);
        this.c(o.b(file));
    }
    
    public Map<String, String> b() {
        return this.h;
    }
    
    public void b(final File file) {
        this.c(o.b(file));
    }
    
    public String c() {
        final StringBuilder sb = new StringBuilder();
        final String e = this.e;
        if (e != null) {
            sb.append(e);
        }
        final String f = this.f;
        if (f != null) {
            sb.append(f);
        }
        final String g = this.g;
        if (g != null) {
            sb.append(g);
        }
        return sb.toString();
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.apm.insight.g;

import com.apm.insight.l.v;
import com.apm.insight.ICrashCallback;
import android.os.SystemClock;
import com.apm.insight.l.r;
import com.apm.insight.ICrashFilter;
import android.os.Looper;
import com.apm.insight.k.d;
import com.apm.insight.runtime.a.f;
import com.apm.insight.i;
import com.apm.insight.runtime.p;
import java.util.Iterator;
import java.util.List;
import com.apm.insight.l.q;
import com.apm.insight.IOOMCallback;
import com.apm.insight.CrashType;
import com.apm.insight.runtime.o;
import java.io.File;
import org.json.JSONArray;
import java.util.concurrent.ConcurrentHashMap;
import java.util.ArrayList;

public class a implements UncaughtExceptionHandler
{
    private static a a;
    private static volatile boolean i = false;
    private static volatile ThreadLocal<Boolean> j;
    private static ArrayList<b> l;
    private UncaughtExceptionHandler b;
    private c c;
    private c d;
    private volatile int e;
    private volatile int f;
    private ConcurrentHashMap<String, Object> g;
    private ConcurrentHashMap<String, Object> h;
    private JSONArray k;
    
    static {
        com.apm.insight.g.a.j = new ThreadLocal<Boolean>();
        com.apm.insight.g.a.l = new ArrayList<b>();
    }
    
    private a() {
        this.e = 0;
        this.f = 0;
        this.g = new ConcurrentHashMap<String, Object>();
        this.h = new ConcurrentHashMap<String, Object>();
        this.d();
    }
    
    private static int a(final Throwable t, final Thread thread) {
        int index = 0;
        int n = 0;
        while (true) {
            if (index >= com.apm.insight.g.a.l.size()) {
                return n;
            }
            try {
                final b b = com.apm.insight.g.a.l.get(index);
                try {
                    n |= b.a(t, thread);
                }
                finally {
                    final Throwable t2;
                    com.apm.insight.c.a().a("NPTH_CATCH", t2);
                }
                ++index;
                continue;
            }
            finally {
                return n;
            }
        }
    }
    
    public static a a() {
        if (com.apm.insight.g.a.a == null) {
            com.apm.insight.g.a.a = new a();
        }
        return com.apm.insight.g.a.a;
    }
    
    private String a(final File p0, final boolean p1, final Throwable p2, final String p3, final Thread p4, final boolean p5) {
        // 
        // This method could not be decompiled.
        // 
        // Original Bytecode:
        // 
        //     1: invokevirtual   java/io/File.getAbsolutePath:()Ljava/lang/String;
        //     4: astore          9
        //     6: aload_0        
        //     7: getfield        com/apm/insight/g/a.h:Ljava/util/concurrent/ConcurrentHashMap;
        //    10: aload_1        
        //    11: invokevirtual   java/io/File.getName:()Ljava/lang/String;
        //    14: aload_1        
        //    15: invokevirtual   java/util/concurrent/ConcurrentHashMap.put:(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
        //    18: pop            
        //    19: aload_1        
        //    20: invokevirtual   java/io/File.getParentFile:()Ljava/io/File;
        //    23: invokevirtual   java/io/File.mkdirs:()Z
        //    26: pop            
        //    27: aload_1        
        //    28: invokevirtual   java/io/File.createNewFile:()Z
        //    31: pop            
        //    32: aload           9
        //    34: invokestatic    com/apm/insight/nativecrash/NativeImpl.c:(Ljava/lang/String;)I
        //    37: pop            
        //    38: goto            43
        //    41: astore          4
        //    43: aconst_null    
        //    44: astore          4
        //    46: aconst_null    
        //    47: astore          8
        //    49: iload           6
        //    51: ifeq            193
        //    54: aload           9
        //    56: invokestatic    com/apm/insight/nativecrash/NativeImpl.h:(Ljava/lang/String;)I
        //    59: istore          7
        //    61: aload           4
        //    63: astore_1       
        //    64: iload           7
        //    66: ifle            646
        //    69: iload           7
        //    71: invokestatic    com/apm/insight/i.g:()Landroid/content/Context;
        //    74: invokestatic    com/apm/insight/l/a.c:(Landroid/content/Context;)Ljava/lang/String;
        //    77: invokestatic    com/apm/insight/nativecrash/NativeImpl.a:(ILjava/lang/String;)V
        //    80: iload           7
        //    82: ldc             "\n"
        //    84: invokestatic    com/apm/insight/nativecrash/NativeImpl.a:(ILjava/lang/String;)V
        //    87: iload           7
        //    89: aload_3        
        //    90: invokevirtual   java/lang/Throwable.getMessage:()Ljava/lang/String;
        //    93: invokestatic    com/apm/insight/nativecrash/NativeImpl.a:(ILjava/lang/String;)V
        //    96: iload           7
        //    98: ldc             "\n"
        //   100: invokestatic    com/apm/insight/nativecrash/NativeImpl.a:(ILjava/lang/String;)V
        //   103: iload           7
        //   105: aload_3        
        //   106: invokevirtual   java/lang/Object.getClass:()Ljava/lang/Class;
        //   109: invokevirtual   java/lang/Class.getName:()Ljava/lang/String;
        //   112: invokestatic    com/apm/insight/nativecrash/NativeImpl.a:(ILjava/lang/String;)V
        //   115: aload_3        
        //   116: invokevirtual   java/lang/Throwable.getMessage:()Ljava/lang/String;
        //   119: ifnull          138
        //   122: iload           7
        //   124: ldc             ": "
        //   126: invokestatic    com/apm/insight/nativecrash/NativeImpl.a:(ILjava/lang/String;)V
        //   129: iload           7
        //   131: aload_3        
        //   132: invokevirtual   java/lang/Throwable.getMessage:()Ljava/lang/String;
        //   135: invokestatic    com/apm/insight/nativecrash/NativeImpl.a:(ILjava/lang/String;)V
        //   138: iload           7
        //   140: ldc             "\n"
        //   142: invokestatic    com/apm/insight/nativecrash/NativeImpl.a:(ILjava/lang/String;)V
        //   145: iload           7
        //   147: aload           5
        //   149: invokevirtual   java/lang/Thread.getName:()Ljava/lang/String;
        //   152: invokestatic    com/apm/insight/nativecrash/NativeImpl.a:(ILjava/lang/String;)V
        //   155: iload           7
        //   157: ldc             "\n"
        //   159: invokestatic    com/apm/insight/nativecrash/NativeImpl.a:(ILjava/lang/String;)V
        //   162: iload           7
        //   164: ldc             "stack:"
        //   166: invokestatic    com/apm/insight/nativecrash/NativeImpl.a:(ILjava/lang/String;)V
        //   169: iload           7
        //   171: ldc             "\n"
        //   173: invokestatic    com/apm/insight/nativecrash/NativeImpl.a:(ILjava/lang/String;)V
        //   176: aload_3        
        //   177: iload           7
        //   179: invokestatic    com/apm/insight/l/v.a:(Ljava/lang/Throwable;I)V
        //   182: iload           7
        //   184: invokestatic    com/apm/insight/nativecrash/NativeImpl.b:(I)V
        //   187: aload           4
        //   189: astore_1       
        //   190: goto            646
        //   193: new             Ljava/io/FileOutputStream;
        //   196: astore          9
        //   198: aload           9
        //   200: aload_1        
        //   201: iconst_1       
        //   202: invokespecial   java/io/FileOutputStream.<init>:(Ljava/io/File;Z)V
        //   205: ldc             32768
        //   207: invokestatic    com/apm/insight/l/r.a:(I)Z
        //   210: ifne            360
        //   213: new             Ljava/lang/StringBuilder;
        //   216: astore_1       
        //   217: aload_1        
        //   218: invokespecial   java/lang/StringBuilder.<init>:()V
        //   221: aload_1        
        //   222: invokestatic    com/apm/insight/i.g:()Landroid/content/Context;
        //   225: invokestatic    com/apm/insight/l/a.c:(Landroid/content/Context;)Ljava/lang/String;
        //   228: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   231: pop            
        //   232: aload_1        
        //   233: ldc             "\n"
        //   235: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   238: pop            
        //   239: aload           9
        //   241: aload_1        
        //   242: invokevirtual   java/lang/StringBuilder.toString:()Ljava/lang/String;
        //   245: invokevirtual   java/lang/String.getBytes:()[B
        //   248: invokevirtual   java/io/FileOutputStream.write:([B)V
        //   251: new             Ljava/lang/StringBuilder;
        //   254: astore_1       
        //   255: aload_1        
        //   256: invokespecial   java/lang/StringBuilder.<init>:()V
        //   259: aload_1        
        //   260: aload_3        
        //   261: invokevirtual   java/lang/Throwable.getMessage:()Ljava/lang/String;
        //   264: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   267: pop            
        //   268: aload_1        
        //   269: ldc             "\n"
        //   271: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   274: pop            
        //   275: aload           9
        //   277: aload_1        
        //   278: invokevirtual   java/lang/StringBuilder.toString:()Ljava/lang/String;
        //   281: invokevirtual   java/lang/String.getBytes:()[B
        //   284: invokevirtual   java/io/FileOutputStream.write:([B)V
        //   287: new             Ljava/lang/StringBuilder;
        //   290: astore_1       
        //   291: aload_1        
        //   292: invokespecial   java/lang/StringBuilder.<init>:()V
        //   295: aload_1        
        //   296: aload_3        
        //   297: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/Object;)Ljava/lang/StringBuilder;
        //   300: pop            
        //   301: aload_1        
        //   302: ldc             "\n"
        //   304: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   307: pop            
        //   308: aload           9
        //   310: aload_1        
        //   311: invokevirtual   java/lang/StringBuilder.toString:()Ljava/lang/String;
        //   314: invokevirtual   java/lang/String.getBytes:()[B
        //   317: invokevirtual   java/io/FileOutputStream.write:([B)V
        //   320: new             Ljava/lang/StringBuilder;
        //   323: astore_1       
        //   324: aload_1        
        //   325: invokespecial   java/lang/StringBuilder.<init>:()V
        //   328: aload_1        
        //   329: aload           5
        //   331: invokevirtual   java/lang/Thread.getName:()Ljava/lang/String;
        //   334: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   337: pop            
        //   338: aload_1        
        //   339: ldc             "\n"
        //   341: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   344: pop            
        //   345: aload           9
        //   347: aload_1        
        //   348: invokevirtual   java/lang/StringBuilder.toString:()Ljava/lang/String;
        //   351: invokevirtual   java/lang/String.getBytes:()[B
        //   354: invokevirtual   java/io/FileOutputStream.write:([B)V
        //   357: goto            372
        //   360: new             Ljava/lang/RuntimeException;
        //   363: astore_1       
        //   364: aload_1        
        //   365: ldc             "test exception before write stack"
        //   367: invokespecial   java/lang/RuntimeException.<init>:(Ljava/lang/String;)V
        //   370: aload_1        
        //   371: athrow         
        //   372: aload           9
        //   374: ldc             "stack:\n"
        //   376: invokevirtual   java/lang/String.getBytes:()[B
        //   379: invokevirtual   java/io/FileOutputStream.write:([B)V
        //   382: aload           8
        //   384: astore_1       
        //   385: sipush          8192
        //   388: invokestatic    com/apm/insight/l/r.a:(I)Z
        //   391: ifne            485
        //   394: aload           8
        //   396: astore_1       
        //   397: new             Ljava/io/PrintStream;
        //   400: astore          10
        //   402: aload           8
        //   404: astore_1       
        //   405: aload           10
        //   407: aload           9
        //   409: invokespecial   java/io/PrintStream.<init>:(Ljava/io/OutputStream;)V
        //   412: aload           8
        //   414: astore_1       
        //   415: invokestatic    android/os/Looper.getMainLooper:()Landroid/os/Looper;
        //   418: invokestatic    android/os/Looper.myLooper:()Landroid/os/Looper;
        //   421: if_acmpne       444
        //   424: aload           8
        //   426: astore_1       
        //   427: new             Lcom/apm/insight/g/a$1;
        //   430: astore          4
        //   432: aload           8
        //   434: astore_1       
        //   435: aload           4
        //   437: aload_0        
        //   438: invokespecial   com/apm/insight/g/a$1.<init>:(Lcom/apm/insight/g/a;)V
        //   441: goto            456
        //   444: aload           8
        //   446: astore_1       
        //   447: new             Lcom/apm/insight/l/e$a;
        //   450: dup            
        //   451: invokespecial   com/apm/insight/l/e$a.<init>:()V
        //   454: astore          4
        //   456: aload           8
        //   458: astore_1       
        //   459: aload_3        
        //   460: aload           5
        //   462: aload           10
        //   464: aload           4
        //   466: invokestatic    com/apm/insight/l/v.a:(Ljava/lang/Throwable;Ljava/lang/Thread;Ljava/io/PrintStream;Lcom/apm/insight/l/e$a;)Ljava/lang/String;
        //   469: astore          4
        //   471: aload           4
        //   473: astore_1       
        //   474: aload           9
        //   476: invokestatic    com/apm/insight/l/k.a:(Ljava/io/Closeable;)V
        //   479: aload           4
        //   481: astore_1       
        //   482: goto            641
        //   485: aload           8
        //   487: astore_1       
        //   488: new             Ljava/lang/RuntimeException;
        //   491: astore          4
        //   493: aload           8
        //   495: astore_1       
        //   496: aload           4
        //   498: ldc             "test exception npth write stack"
        //   500: invokespecial   java/lang/RuntimeException.<init>:(Ljava/lang/String;)V
        //   503: aload           8
        //   505: astore_1       
        //   506: aload           4
        //   508: athrow         
        //   509: astore          4
        //   511: sipush          16384
        //   514: invokestatic    com/apm/insight/l/r.a:(I)Z
        //   517: ifne            541
        //   520: new             Ljava/io/PrintStream;
        //   523: astore          5
        //   525: aload           5
        //   527: aload           9
        //   529: invokespecial   java/io/PrintStream.<init>:(Ljava/io/OutputStream;)V
        //   532: aload_3        
        //   533: aload           5
        //   535: invokevirtual   java/lang/Throwable.printStackTrace:(Ljava/io/PrintStream;)V
        //   538: goto            641
        //   541: new             Ljava/lang/RuntimeException;
        //   544: astore_3       
        //   545: aload_3        
        //   546: ldc             "test exception system write stack"
        //   548: invokespecial   java/lang/RuntimeException.<init>:(Ljava/lang/String;)V
        //   551: aload_3        
        //   552: athrow         
        //   553: astore_3       
        //   554: aload           9
        //   556: ldc             "err:\n"
        //   558: invokevirtual   java/lang/String.getBytes:()[B
        //   561: invokevirtual   java/io/FileOutputStream.write:([B)V
        //   564: new             Ljava/lang/StringBuilder;
        //   567: astore          5
        //   569: aload           5
        //   571: invokespecial   java/lang/StringBuilder.<init>:()V
        //   574: aload           5
        //   576: aload           4
        //   578: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/Object;)Ljava/lang/StringBuilder;
        //   581: pop            
        //   582: aload           5
        //   584: ldc             "\n"
        //   586: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   589: pop            
        //   590: aload           9
        //   592: aload           5
        //   594: invokevirtual   java/lang/StringBuilder.toString:()Ljava/lang/String;
        //   597: invokevirtual   java/lang/String.getBytes:()[B
        //   600: invokevirtual   java/io/FileOutputStream.write:([B)V
        //   603: new             Ljava/lang/StringBuilder;
        //   606: astore          4
        //   608: aload           4
        //   610: invokespecial   java/lang/StringBuilder.<init>:()V
        //   613: aload           4
        //   615: aload_3        
        //   616: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/Object;)Ljava/lang/StringBuilder;
        //   619: pop            
        //   620: aload           4
        //   622: ldc             "\n"
        //   624: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   627: pop            
        //   628: aload           9
        //   630: aload           4
        //   632: invokevirtual   java/lang/StringBuilder.toString:()Ljava/lang/String;
        //   635: invokevirtual   java/lang/String.getBytes:()[B
        //   638: invokevirtual   java/io/FileOutputStream.write:([B)V
        //   641: aload           9
        //   643: invokestatic    com/apm/insight/l/k.a:(Ljava/io/Closeable;)V
        //   646: aload_1        
        //   647: areturn        
        //   648: astore_1       
        //   649: goto            162
        //   652: astore_1       
        //   653: goto            176
        //   656: astore_1       
        //   657: aload           4
        //   659: astore_1       
        //   660: goto            646
        //   663: astore_1       
        //   664: goto            372
        //   667: astore_1       
        //   668: goto            382
        //   671: astore_3       
        //   672: goto            641
        //    Exceptions:
        //  Try           Handler
        //  Start  End    Start  End    Type
        //  -----  -----  -----  -----  ----
        //  19     38     41     43     Any
        //  69     138    648    652    Any
        //  138    162    648    652    Any
        //  162    176    652    656    Any
        //  193    205    656    663    Any
        //  205    357    663    667    Any
        //  360    372    663    667    Any
        //  372    382    667    671    Any
        //  385    394    509    641    Any
        //  397    402    509    641    Any
        //  405    412    509    641    Any
        //  415    424    509    641    Any
        //  427    432    509    641    Any
        //  435    441    509    641    Any
        //  447    456    509    641    Any
        //  459    471    509    641    Any
        //  474    479    509    641    Any
        //  488    493    509    641    Any
        //  496    503    509    641    Any
        //  506    509    509    641    Any
        //  511    538    553    641    Any
        //  541    553    553    641    Any
        //  554    641    671    675    Any
        // 
        // The error that occurred was:
        // 
        // java.lang.IndexOutOfBoundsException: Index 320 out of bounds for length 320
        //     at java.base/jdk.internal.util.Preconditions.outOfBounds(Preconditions.java:64)
        //     at java.base/jdk.internal.util.Preconditions.outOfBoundsCheckIndex(Preconditions.java:70)
        //     at java.base/jdk.internal.util.Preconditions.checkIndex(Preconditions.java:266)
        //     at java.base/java.util.Objects.checkIndex(Objects.java:361)
        //     at java.base/java.util.ArrayList.get(ArrayList.java:427)
        //     at com.strobel.decompiler.ast.AstBuilder.convertToAst(AstBuilder.java:3362)
        //     at com.strobel.decompiler.ast.AstBuilder.build(AstBuilder.java:112)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:203)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:93)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethodBody(AstBuilder.java:868)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethod(AstBuilder.java:761)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addTypeMembers(AstBuilder.java:638)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeCore(AstBuilder.java:605)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeNoCache(AstBuilder.java:195)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createType(AstBuilder.java:162)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addType(AstBuilder.java:137)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.buildAst(JavaLanguage.java:71)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.decompileType(JavaLanguage.java:59)
        //     at com.strobel.decompiler.DecompilerDriver.decompileType(DecompilerDriver.java:333)
        //     at com.strobel.decompiler.DecompilerDriver.decompileJar(DecompilerDriver.java:254)
        //     at com.strobel.decompiler.DecompilerDriver.main(DecompilerDriver.java:144)
        // 
        throw new IllegalStateException("An error occurred while decompiling this method.");
    }
    
    private Throwable a(final Thread p0, final Throwable p1) {
        // 
        // This method could not be decompiled.
        // 
        // Original Bytecode:
        // 
        //     1: getfield        com/apm/insight/g/a.e:I
        //     4: iconst_3       
        //     5: if_icmplt       18
        //     8: ldc             65536
        //    10: invokestatic    com/apm/insight/l/r.a:(I)Z
        //    13: ifne            18
        //    16: aconst_null    
        //    17: areturn        
        //    18: aload_0        
        //    19: aload_0        
        //    20: getfield        com/apm/insight/g/a.e:I
        //    23: iconst_1       
        //    24: iadd           
        //    25: putfield        com/apm/insight/g/a.e:I
        //    28: aload_0        
        //    29: aload_0        
        //    30: getfield        com/apm/insight/g/a.f:I
        //    33: iconst_1       
        //    34: iadd           
        //    35: putfield        com/apm/insight/g/a.f:I
        //    38: getstatic       com/apm/insight/g/a.i:Z
        //    41: ifeq            53
        //    44: getstatic       com/apm/insight/g/a.j:Ljava/lang/ThreadLocal;
        //    47: getstatic       java/lang/Boolean.TRUE:Ljava/lang/Boolean;
        //    50: invokevirtual   java/lang/ThreadLocal.set:(Ljava/lang/Object;)V
        //    53: iconst_1       
        //    54: putstatic       com/apm/insight/g/a.i:Z
        //    57: invokestatic    java/lang/System.currentTimeMillis:()J
        //    60: lstore          9
        //    62: lload           9
        //    64: invokestatic    com/apm/insight/g/a.a:(J)Z
        //    67: istore          7
        //    69: aload_2        
        //    70: invokestatic    com/apm/insight/l/v.c:(Ljava/lang/Throwable;)Z
        //    73: istore          5
        //    75: iload           5
        //    77: istore          4
        //    79: iload           5
        //    81: ifeq            119
        //    84: aload_2        
        //    85: invokestatic    com/apm/insight/l/v.d:(Ljava/lang/Throwable;)Z
        //    88: istore          6
        //    90: iload           5
        //    92: istore          4
        //    94: iload           6
        //    96: ifeq            119
        //    99: iconst_1       
        //   100: istore          6
        //   102: goto            126
        //   105: astore          11
        //   107: iload           5
        //   109: istore          4
        //   111: goto            119
        //   114: astore          11
        //   116: iconst_0       
        //   117: istore          4
        //   119: iconst_0       
        //   120: istore          6
        //   122: iload           4
        //   124: istore          5
        //   126: iload           7
        //   128: ifeq            139
        //   131: getstatic       com/apm/insight/CrashType.LAUNCH:Lcom/apm/insight/CrashType;
        //   134: astore          11
        //   136: goto            144
        //   139: getstatic       com/apm/insight/CrashType.JAVA:Lcom/apm/insight/CrashType;
        //   142: astore          11
        //   144: lload           9
        //   146: aload           11
        //   148: iload           5
        //   150: iconst_0       
        //   151: invokestatic    com/apm/insight/i.a:(JLcom/apm/insight/CrashType;ZZ)Ljava/lang/String;
        //   154: astore          11
        //   156: new             Ljava/io/File;
        //   159: astore          15
        //   161: aload           15
        //   163: invokestatic    com/apm/insight/i.g:()Landroid/content/Context;
        //   166: invokestatic    com/apm/insight/l/o.a:(Landroid/content/Context;)Ljava/io/File;
        //   169: aload           11
        //   171: invokespecial   java/io/File.<init>:(Ljava/io/File;Ljava/lang/String;)V
        //   174: new             Ljava/io/File;
        //   177: astore          13
        //   179: aload           13
        //   181: aload           15
        //   183: ldc_w           "logEventStack"
        //   186: invokespecial   java/io/File.<init>:(Ljava/io/File;Ljava/lang/String;)V
        //   189: aload           13
        //   191: astore          12
        //   193: aload_0        
        //   194: aload           13
        //   196: iload           5
        //   198: aload_2        
        //   199: aload           11
        //   201: aload_1        
        //   202: iload           6
        //   204: invokespecial   com/apm/insight/g/a.a:(Ljava/io/File;ZLjava/lang/Throwable;Ljava/lang/String;Ljava/lang/Thread;Z)Ljava/lang/String;
        //   207: astore          14
        //   209: aload_2        
        //   210: aload_1        
        //   211: invokestatic    com/apm/insight/g/a.a:(Ljava/lang/Throwable;Ljava/lang/Thread;)I
        //   214: iconst_1       
        //   215: iand           
        //   216: ifne            248
        //   219: aload           14
        //   221: ifnull          242
        //   224: invokestatic    com/apm/insight/i.i:()Lcom/apm/insight/runtime/ConfigManager;
        //   227: aload           14
        //   229: invokevirtual   com/apm/insight/runtime/ConfigManager.isCrashIgnored:(Ljava/lang/String;)Z
        //   232: istore          4
        //   234: iload           4
        //   236: ifeq            242
        //   239: goto            248
        //   242: iconst_0       
        //   243: istore          4
        //   245: goto            251
        //   248: iconst_1       
        //   249: istore          4
        //   251: aload_2        
        //   252: aload_1        
        //   253: aload           15
        //   255: invokestatic    com/apm/insight/entity/b.a:(Ljava/lang/Throwable;Ljava/lang/Thread;Ljava/io/File;)Lorg/json/JSONArray;
        //   258: astore          13
        //   260: aload_0        
        //   261: aload           13
        //   263: putfield        com/apm/insight/g/a.k:Lorg/json/JSONArray;
        //   266: aload           13
        //   268: ifnonnull       276
        //   271: iconst_1       
        //   272: istore_3       
        //   273: goto            278
        //   276: iconst_0       
        //   277: istore_3       
        //   278: iload_3        
        //   279: ifne            293
        //   282: iload           4
        //   284: ifeq            290
        //   287: goto            293
        //   290: goto            364
        //   293: iload           7
        //   295: ifeq            306
        //   298: getstatic       com/apm/insight/CrashType.LAUNCH:Lcom/apm/insight/CrashType;
        //   301: astore          11
        //   303: goto            311
        //   306: getstatic       com/apm/insight/CrashType.JAVA:Lcom/apm/insight/CrashType;
        //   309: astore          11
        //   311: lload           9
        //   313: aload           11
        //   315: iload           5
        //   317: iconst_1       
        //   318: invokestatic    com/apm/insight/i.a:(JLcom/apm/insight/CrashType;ZZ)Ljava/lang/String;
        //   321: astore          11
        //   323: new             Ljava/io/File;
        //   326: astore          13
        //   328: aload           13
        //   330: invokestatic    com/apm/insight/i.g:()Landroid/content/Context;
        //   333: invokestatic    com/apm/insight/l/o.a:(Landroid/content/Context;)Ljava/io/File;
        //   336: aload           11
        //   338: invokespecial   java/io/File.<init>:(Ljava/io/File;Ljava/lang/String;)V
        //   341: aload           15
        //   343: aload           13
        //   345: invokevirtual   java/io/File.renameTo:(Ljava/io/File;)Z
        //   348: pop            
        //   349: new             Ljava/io/File;
        //   352: astore          12
        //   354: aload           12
        //   356: aload           13
        //   358: ldc_w           "logEventStack"
        //   361: invokespecial   java/io/File.<init>:(Ljava/io/File;Ljava/lang/String;)V
        //   364: invokestatic    com/apm/insight/l/f.a:()V
        //   367: invokestatic    com/apm/insight/k/b.a:()Lcom/apm/insight/k/b;
        //   370: invokevirtual   com/apm/insight/k/b.b:()V
        //   373: aload_0        
        //   374: invokespecial   com/apm/insight/g/a.g:()Z
        //   377: istore          5
        //   379: iload           6
        //   381: ifeq            399
        //   384: iload           5
        //   386: ifeq            399
        //   389: aload_0        
        //   390: aload_1        
        //   391: aload_2        
        //   392: iload           7
        //   394: lload           9
        //   396: invokespecial   com/apm/insight/g/a.a:(Ljava/lang/Thread;Ljava/lang/Throwable;ZJ)V
        //   399: iconst_1       
        //   400: invokestatic    com/apm/insight/l/r.a:(I)Z
        //   403: ifeq            413
        //   406: aload_0        
        //   407: getfield        com/apm/insight/g/a.h:Ljava/util/concurrent/ConcurrentHashMap;
        //   410: invokevirtual   java/util/concurrent/ConcurrentHashMap.clear:()V
        //   413: iload           7
        //   415: ifeq            426
        //   418: getstatic       com/apm/insight/CrashType.LAUNCH:Lcom/apm/insight/CrashType;
        //   421: astore          13
        //   423: goto            431
        //   426: getstatic       com/apm/insight/CrashType.JAVA:Lcom/apm/insight/CrashType;
        //   429: astore          13
        //   431: new             Ljava/lang/StringBuilder;
        //   434: astore          15
        //   436: aload           15
        //   438: invokespecial   java/lang/StringBuilder.<init>:()V
        //   441: aload           15
        //   443: ldc_w           "[uncaughtException] isLaunchCrash="
        //   446: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   449: pop            
        //   450: aload           15
        //   452: iload           7
        //   454: invokevirtual   java/lang/StringBuilder.append:(Z)Ljava/lang/StringBuilder;
        //   457: pop            
        //   458: aload           15
        //   460: invokevirtual   java/lang/StringBuilder.toString:()Ljava/lang/String;
        //   463: invokestatic    com/apm/insight/l/q.a:(Ljava/lang/Object;)V
        //   466: invokestatic    com/apm/insight/a/a.a:()Lcom/apm/insight/a/a;
        //   469: aload           13
        //   471: lload           9
        //   473: aload           11
        //   475: invokevirtual   com/apm/insight/a/a.a:(Lcom/apm/insight/CrashType;JLjava/lang/String;)V
        //   478: aload_0        
        //   479: aload_1        
        //   480: aload_2        
        //   481: invokespecial   com/apm/insight/g/a.b:(Ljava/lang/Thread;Ljava/lang/Throwable;)Z
        //   484: istore          8
        //   486: iload           8
        //   488: ifeq            593
        //   491: aload_0        
        //   492: getfield        com/apm/insight/g/a.c:Lcom/apm/insight/g/c;
        //   495: astore          13
        //   497: aload           13
        //   499: ifnull          593
        //   502: iload           7
        //   504: ifeq            593
        //   507: aload           13
        //   509: aload_2        
        //   510: invokeinterface com/apm/insight/g/c.a:(Ljava/lang/Throwable;)Z
        //   515: ifeq            593
        //   518: aload_0        
        //   519: getfield        com/apm/insight/g/a.c:Lcom/apm/insight/g/c;
        //   522: astore          13
        //   524: aload           13
        //   526: lload           9
        //   528: aload_1        
        //   529: aload_2        
        //   530: aload           11
        //   532: aload           12
        //   534: aload           14
        //   536: iload           4
        //   538: invokeinterface com/apm/insight/g/c.a:(JLjava/lang/Thread;Ljava/lang/Throwable;Ljava/lang/String;Ljava/io/File;Ljava/lang/String;Z)V
        //   543: new             Ljava/lang/StringBuilder;
        //   546: astore          11
        //   548: aload           11
        //   550: invokespecial   java/lang/StringBuilder.<init>:()V
        //   553: aload           11
        //   555: ldc_w           "[uncaughtException] mLaunchCrashDisposer "
        //   558: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   561: pop            
        //   562: aload           11
        //   564: aload_2        
        //   565: invokevirtual   java/lang/Throwable.toString:()Ljava/lang/String;
        //   568: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   571: pop            
        //   572: aload           11
        //   574: invokevirtual   java/lang/StringBuilder.toString:()Ljava/lang/String;
        //   577: astore          11
        //   579: goto            677
        //   582: astore          11
        //   584: aload_2        
        //   585: astore          12
        //   587: aload_1        
        //   588: astore          12
        //   590: goto            814
        //   593: iload           8
        //   595: ifeq            701
        //   598: aload_0        
        //   599: getfield        com/apm/insight/g/a.d:Lcom/apm/insight/g/c;
        //   602: astore          13
        //   604: aload           13
        //   606: ifnull          701
        //   609: aload           13
        //   611: aload_2        
        //   612: invokeinterface com/apm/insight/g/c.a:(Ljava/lang/Throwable;)Z
        //   617: ifeq            701
        //   620: aload_0        
        //   621: getfield        com/apm/insight/g/a.d:Lcom/apm/insight/g/c;
        //   624: astore          13
        //   626: aload           13
        //   628: lload           9
        //   630: aload_1        
        //   631: aload_2        
        //   632: aload           11
        //   634: aload           12
        //   636: aload           14
        //   638: iload           4
        //   640: invokeinterface com/apm/insight/g/c.a:(JLjava/lang/Thread;Ljava/lang/Throwable;Ljava/lang/String;Ljava/io/File;Ljava/lang/String;Z)V
        //   645: new             Ljava/lang/StringBuilder;
        //   648: astore          11
        //   650: aload           11
        //   652: invokespecial   java/lang/StringBuilder.<init>:()V
        //   655: aload           11
        //   657: ldc_w           "[uncaughtException] mLaunchCrashDisposer "
        //   660: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   663: pop            
        //   664: aload           11
        //   666: aload_2        
        //   667: invokevirtual   java/lang/Throwable.toString:()Ljava/lang/String;
        //   670: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   673: pop            
        //   674: goto            572
        //   677: aload           11
        //   679: invokestatic    com/apm/insight/l/q.a:(Ljava/lang/Object;)V
        //   682: goto            701
        //   685: astore          11
        //   687: aload_1        
        //   688: astore          12
        //   690: aload_2        
        //   691: astore          12
        //   693: goto            814
        //   696: astore          11
        //   698: goto            584
        //   701: iload           4
        //   703: ifne            740
        //   706: iload           6
        //   708: ifeq            729
        //   711: iload           5
        //   713: ifne            729
        //   716: aload_0        
        //   717: aload_1        
        //   718: aload_2        
        //   719: iload           7
        //   721: lload           9
        //   723: invokespecial   com/apm/insight/g/a.a:(Ljava/lang/Thread;Ljava/lang/Throwable;ZJ)V
        //   726: goto            729
        //   729: aload_0        
        //   730: invokespecial   com/apm/insight/g/a.f:()V
        //   733: aload_0        
        //   734: invokespecial   com/apm/insight/g/a.e:()V
        //   737: goto            861
        //   740: aload_0        
        //   741: monitorenter   
        //   742: aload_0        
        //   743: aload_0        
        //   744: getfield        com/apm/insight/g/a.f:I
        //   747: iconst_1       
        //   748: isub           
        //   749: putfield        com/apm/insight/g/a.f:I
        //   752: aload_0        
        //   753: aload_0        
        //   754: getfield        com/apm/insight/g/a.e:I
        //   757: iconst_1       
        //   758: isub           
        //   759: putfield        com/apm/insight/g/a.e:I
        //   762: aload_0        
        //   763: monitorexit    
        //   764: aload_2        
        //   765: aload_1        
        //   766: invokestatic    com/apm/insight/g/a.b:(Ljava/lang/Throwable;Ljava/lang/Thread;)Ljava/lang/Throwable;
        //   769: areturn        
        //   770: astore_1       
        //   771: aload_0        
        //   772: monitorexit    
        //   773: aload_1        
        //   774: athrow         
        //   775: astore          11
        //   777: aload_2        
        //   778: astore          12
        //   780: aload_1        
        //   781: astore          12
        //   783: goto            814
        //   786: astore          11
        //   788: aload_2        
        //   789: astore          12
        //   791: aload_1        
        //   792: astore          12
        //   794: iconst_0       
        //   795: istore          5
        //   797: goto            814
        //   800: astore          11
        //   802: aload_2        
        //   803: astore          12
        //   805: aload_1        
        //   806: astore          12
        //   808: iconst_0       
        //   809: istore          5
        //   811: iconst_0       
        //   812: istore          4
        //   814: aload_2        
        //   815: astore          13
        //   817: aload_1        
        //   818: astore          12
        //   820: aload           11
        //   822: invokestatic    com/apm/insight/l/v.c:(Ljava/lang/Throwable;)Z
        //   825: ifne            833
        //   828: aload           11
        //   830: invokestatic    com/apm/insight/l/q.a:(Ljava/lang/Throwable;)V
        //   833: iload           4
        //   835: ifne            869
        //   838: iload           6
        //   840: ifeq            729
        //   843: iload           5
        //   845: ifne            729
        //   848: aload_0        
        //   849: aload_1        
        //   850: aload_2        
        //   851: iload           7
        //   853: lload           9
        //   855: invokespecial   com/apm/insight/g/a.a:(Ljava/lang/Thread;Ljava/lang/Throwable;ZJ)V
        //   858: goto            729
        //   861: aload_0        
        //   862: aload_1        
        //   863: aload_2        
        //   864: invokespecial   com/apm/insight/g/a.c:(Ljava/lang/Thread;Ljava/lang/Throwable;)V
        //   867: aconst_null    
        //   868: areturn        
        //   869: aload_0        
        //   870: monitorenter   
        //   871: aload_0        
        //   872: aload_0        
        //   873: getfield        com/apm/insight/g/a.f:I
        //   876: iconst_1       
        //   877: isub           
        //   878: putfield        com/apm/insight/g/a.f:I
        //   881: aload_0        
        //   882: aload_0        
        //   883: getfield        com/apm/insight/g/a.e:I
        //   886: iconst_1       
        //   887: isub           
        //   888: putfield        com/apm/insight/g/a.e:I
        //   891: aload_0        
        //   892: monitorexit    
        //   893: aload           13
        //   895: aload           12
        //   897: invokestatic    com/apm/insight/g/a.b:(Ljava/lang/Throwable;Ljava/lang/Thread;)Ljava/lang/Throwable;
        //   900: areturn        
        //   901: astore_1       
        //   902: aload_0        
        //   903: monitorexit    
        //   904: aload_1        
        //   905: athrow         
        //   906: astore          11
        //   908: iload           4
        //   910: ifne            950
        //   913: iload           6
        //   915: ifeq            933
        //   918: iload           5
        //   920: ifne            933
        //   923: aload_0        
        //   924: aload_1        
        //   925: aload_2        
        //   926: iload           7
        //   928: lload           9
        //   930: invokespecial   com/apm/insight/g/a.a:(Ljava/lang/Thread;Ljava/lang/Throwable;ZJ)V
        //   933: aload_0        
        //   934: invokespecial   com/apm/insight/g/a.f:()V
        //   937: aload_0        
        //   938: invokespecial   com/apm/insight/g/a.e:()V
        //   941: aload_0        
        //   942: aload_1        
        //   943: aload_2        
        //   944: invokespecial   com/apm/insight/g/a.c:(Ljava/lang/Thread;Ljava/lang/Throwable;)V
        //   947: aload           11
        //   949: athrow         
        //   950: aload_0        
        //   951: monitorenter   
        //   952: aload_0        
        //   953: aload_0        
        //   954: getfield        com/apm/insight/g/a.f:I
        //   957: iconst_1       
        //   958: isub           
        //   959: putfield        com/apm/insight/g/a.f:I
        //   962: aload_0        
        //   963: aload_0        
        //   964: getfield        com/apm/insight/g/a.e:I
        //   967: iconst_1       
        //   968: isub           
        //   969: putfield        com/apm/insight/g/a.e:I
        //   972: aload_0        
        //   973: monitorexit    
        //   974: aload           13
        //   976: aload           12
        //   978: invokestatic    com/apm/insight/g/a.b:(Ljava/lang/Throwable;Ljava/lang/Thread;)Ljava/lang/Throwable;
        //   981: areturn        
        //   982: astore_1       
        //   983: aload_0        
        //   984: monitorexit    
        //   985: aload_1        
        //   986: athrow         
        //   987: astore_1       
        //   988: goto            867
        //   991: astore_1       
        //   992: goto            947
        //    Exceptions:
        //  Try           Handler
        //  Start  End    Start  End    Type
        //  -----  -----  -----  -----  ----
        //  69     75     114    119    Any
        //  84     90     105    114    Any
        //  131    136    800    814    Any
        //  139    144    800    814    Any
        //  144    189    800    814    Any
        //  193    219    800    814    Any
        //  224    234    800    814    Any
        //  251    266    786    800    Any
        //  298    303    786    800    Any
        //  306    311    786    800    Any
        //  311    364    786    800    Any
        //  364    379    786    800    Any
        //  389    399    775    786    Any
        //  399    413    775    786    Any
        //  418    423    775    786    Any
        //  426    431    775    786    Any
        //  431    486    775    786    Any
        //  491    497    582    584    Any
        //  507    524    582    584    Any
        //  524    572    685    696    Any
        //  572    579    685    696    Any
        //  598    604    696    701    Any
        //  609    626    696    701    Any
        //  626    674    685    696    Any
        //  677    682    685    696    Any
        //  716    726    987    991    Any
        //  729    737    987    991    Any
        //  742    764    770    775    Any
        //  771    773    770    775    Any
        //  820    833    906    987    Any
        //  848    858    987    991    Any
        //  861    867    987    991    Any
        //  871    893    901    906    Any
        //  902    904    901    906    Any
        //  923    933    991    995    Any
        //  933    947    991    995    Any
        //  952    974    982    987    Any
        //  983    985    982    987    Any
        // 
        // The error that occurred was:
        // 
        // java.lang.IllegalStateException: Expression is linked from several locations: Label_0572:
        //     at com.strobel.decompiler.ast.Error.expressionLinkedFromMultipleLocations(Error.java:27)
        //     at com.strobel.decompiler.ast.AstOptimizer.mergeDisparateObjectInitializations(AstOptimizer.java:2604)
        //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:235)
        //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:42)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:206)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:93)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethodBody(AstBuilder.java:868)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethod(AstBuilder.java:761)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addTypeMembers(AstBuilder.java:638)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeCore(AstBuilder.java:605)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeNoCache(AstBuilder.java:195)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createType(AstBuilder.java:162)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addType(AstBuilder.java:137)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.buildAst(JavaLanguage.java:71)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.decompileType(JavaLanguage.java:59)
        //     at com.strobel.decompiler.DecompilerDriver.decompileType(DecompilerDriver.java:333)
        //     at com.strobel.decompiler.DecompilerDriver.decompileJar(DecompilerDriver.java:254)
        //     at com.strobel.decompiler.DecompilerDriver.main(DecompilerDriver.java:144)
        // 
        throw new IllegalStateException("An error occurred while decompiling this method.");
    }
    
    private void a(final Thread thread, final Throwable t, final boolean b, final long n) {
        final List<IOOMCallback> a = o.a().a();
        CrashType crashType;
        if (b) {
            crashType = CrashType.LAUNCH;
        }
        else {
            crashType = CrashType.JAVA;
        }
        for (final IOOMCallback ioomCallback : a) {
            try {
                if (ioomCallback instanceof com.apm.insight.b) {
                    ((com.apm.insight.b)ioomCallback).a(crashType, t, thread, n, this.k);
                }
                else {
                    ioomCallback.onCrash(crashType, t, thread, n);
                }
            }
            finally {
                final Throwable t2;
                q.b(t2);
            }
        }
    }
    
    public static void a(final Throwable t) {
        if (t == null) {
            return;
        }
        try {
            p.b().a(new Runnable(t) {
                final Throwable a;
                
                @Override
                public void run() {
                    try {
                        final com.apm.insight.entity.a a = com.apm.insight.entity.a.a(System.currentTimeMillis(), com.apm.insight.i.g(), null, this.a);
                        a.a("userdefine", 1);
                        final com.apm.insight.entity.a a2 = com.apm.insight.runtime.a.f.a().a(CrashType.CUSTOM_JAVA, a);
                        if (a2 != null) {
                            com.apm.insight.k.d.a().b(a2.h());
                        }
                    }
                    finally {}
                }
            });
        }
        finally {}
    }
    
    public static boolean a(final long n) {
        return e.a(n);
    }
    
    private static Throwable b(final Throwable t, final Thread thread) {
        int i = 0;
        while (i < com.apm.insight.g.a.l.size()) {
            try {
                final b b = com.apm.insight.g.a.l.get(i);
                try {
                    b.b(t, thread);
                    ++i;
                }
                finally {
                    return t;
                }
            }
            finally {}
            break;
        }
        if (Looper.getMainLooper() == Looper.myLooper()) {
            try {
                Looper.loop();
            }
            finally {
                return t;
            }
        }
        return null;
    }
    
    public static boolean b() {
        return com.apm.insight.g.a.i;
    }
    
    private boolean b(final Thread thread, final Throwable t) {
        final ICrashFilter b = com.apm.insight.i.b().b();
        boolean b3;
        final boolean b2 = b3 = true;
        if (b == null) {
            return b3;
        }
        try {
            b3 = (b.onJavaCrashFilter(t, thread) && b2);
            return b3;
        }
        finally {
            b3 = b2;
            return b3;
        }
    }
    
    public static void c(final String s) {
        if (s == null) {
            return;
        }
        try {
            p.b().a(new Runnable(s) {
                final String a;
                
                @Override
                public void run() {
                    try {
                        final com.apm.insight.entity.a a = new com.apm.insight.entity.a();
                        a.a("data", (Object)this.a);
                        a.a("userdefine", 1);
                        final com.apm.insight.entity.a a2 = com.apm.insight.runtime.a.f.a().a(CrashType.CUSTOM_JAVA, a);
                        if (a2 != null) {
                            com.apm.insight.k.d.a().b(a2.h());
                        }
                    }
                    finally {}
                }
            });
        }
        finally {}
    }
    
    private void c(final Thread thread, final Throwable t) {
        final UncaughtExceptionHandler b = this.b;
        if (b != null && b != this) {
            if (r.a(512)) {
                return;
            }
            this.b.uncaughtException(thread, t);
        }
    }
    
    public static boolean c() {
        final Boolean b = com.apm.insight.g.a.j.get();
        return b != null && b;
    }
    
    private void d() {
        final Thread.UncaughtExceptionHandler defaultUncaughtExceptionHandler = Thread.getDefaultUncaughtExceptionHandler();
        if (defaultUncaughtExceptionHandler != this) {
            this.b = defaultUncaughtExceptionHandler;
            Thread.setDefaultUncaughtExceptionHandler((Thread.UncaughtExceptionHandler)this);
        }
    }
    
    private void e() {
        synchronized (this) {
            --this.f;
            monitorexit(this);
            final long uptimeMillis = SystemClock.uptimeMillis();
            while (this.f != 0 && SystemClock.uptimeMillis() - uptimeMillis < 10000L) {
                SystemClock.sleep(50L);
            }
        }
    }
    
    private void f() {
        final File a = com.apm.insight.l.o.a(com.apm.insight.i.g());
        final File a2 = com.apm.insight.l.o.a();
        if (com.apm.insight.l.i.b(a) && com.apm.insight.l.i.b(a2)) {
            return;
        }
        final long uptimeMillis = SystemClock.uptimeMillis();
        while (!com.apm.insight.k.i.a() && SystemClock.uptimeMillis() - uptimeMillis < 10000L) {
            try {
                SystemClock.sleep(500L);
            }
            finally {}
        }
    }
    
    private boolean g() {
        final int a = com.apm.insight.runtime.a.a(new String[] { "exception_modules", "oom_callback" });
        boolean b = true;
        if (a != 1) {
            b = false;
        }
        return b;
    }
    
    public void a(final c c) {
        this.c = c;
    }
    
    public void a(final String key) {
        this.g.put(key, new Object());
    }
    
    public void a(final Thread thread, final Throwable t, final boolean b, final com.apm.insight.entity.a a) {
        List<ICrashCallback> list;
        CrashType crashType;
        if (b) {
            list = o.a().b();
            crashType = CrashType.LAUNCH;
        }
        else {
            list = o.a().c();
            crashType = CrashType.JAVA;
        }
        for (final ICrashCallback crashCallback : list) {
            final long uptimeMillis = SystemClock.uptimeMillis();
            try {
                if (crashCallback instanceof com.apm.insight.b) {
                    ((com.apm.insight.b)crashCallback).a(crashType, v.a(t), thread, this.k);
                }
                else {
                    crashCallback.onCrash(crashType, v.a(t), thread);
                }
                final StringBuilder sb = new StringBuilder();
                sb.append("callback_cost_");
                sb.append(((com.apm.insight.b)crashCallback).getClass().getName());
                a.b(sb.toString(), String.valueOf(SystemClock.uptimeMillis() - uptimeMillis));
            }
            finally {
                final Throwable t2;
                q.b(t2);
                final StringBuilder sb2 = new StringBuilder();
                sb2.append("callback_err_");
                sb2.append(((com.apm.insight.b)crashCallback).getClass().getName());
                a.b(sb2.toString(), String.valueOf(SystemClock.uptimeMillis() - uptimeMillis));
            }
        }
    }
    
    public void b(final c d) {
        this.d = d;
    }
    
    public boolean b(final String key) {
        return this.g.containsKey(key);
    }
    
    @Override
    public void uncaughtException(final Thread thread, Throwable a) {
        do {
            a = this.a(thread, a);
        } while (a != null);
    }
}

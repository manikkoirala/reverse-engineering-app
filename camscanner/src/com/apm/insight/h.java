// 
// Decompiled by Procyon v0.6.0
// 

package com.apm.insight;

import com.apm.insight.l.v;
import org.json.JSONArray;
import android.content.pm.PackageInfo;
import org.json.JSONException;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import java.util.List;
import android.text.TextUtils;
import com.apm.insight.l.l;
import java.util.Map;
import com.apm.insight.runtime.d;
import android.content.Context;
import org.json.JSONObject;
import com.apm.insight.k.k;
import com.apm.insight.entity.b;
import java.util.concurrent.ConcurrentHashMap;

public class h
{
    protected static MonitorCrash b;
    protected static volatile ConcurrentHashMap<String, h> c;
    protected MonitorCrash a;
    
    static {
        h.c = new ConcurrentHashMap<String, h>();
    }
    
    private h(final MonitorCrash a) {
        this.a = a;
        com.apm.insight.entity.b.a(this);
        com.apm.insight.j.b.d();
        k.e();
    }
    
    static h a(final String key) {
        return h.c.get(key);
    }
    
    public static Object a() {
        return h.b;
    }
    
    static void a(final Context context, final MonitorCrash b) {
        h.b = b;
        Npth.init(context, new ICommonParams(new h(b), b, i.a()) {
            final h a;
            final MonitorCrash b;
            final d c;
            
            @Override
            public Map<String, Object> getCommonParams() {
                return l.b(this.a.e());
            }
            
            @Override
            public String getDeviceId() {
                String s;
                if (TextUtils.isEmpty((CharSequence)this.b.mConfig.mDeviceId)) {
                    s = this.c.d();
                }
                else {
                    s = this.b.mConfig.mDeviceId;
                }
                return s;
            }
            
            @Override
            public List<String> getPatchInfo() {
                return null;
            }
            
            @Override
            public Map<String, Integer> getPluginInfo() {
                return null;
            }
            
            @Override
            public String getSessionId() {
                return null;
            }
            
            @Override
            public long getUserId() {
                return 0L;
            }
        });
    }
    
    static void a(final MonitorCrash monitorCrash) {
        final h value = new h(monitorCrash);
        if (monitorCrash != null && monitorCrash.mConfig != null) {
            h.c.put(monitorCrash.mConfig.mAid, value);
        }
    }
    
    @Nullable
    private JSONObject b(final CrashType crashType) {
        final AttachUserData mCustomData = this.a.mCustomData;
        if (mCustomData == null) {
            return null;
        }
        final Map<? extends String, ? extends String> userData = mCustomData.getUserData(crashType);
        if (userData == null) {
            return null;
        }
        return new JSONObject((Map)userData);
    }
    
    @NonNull
    private JSONObject c(final CrashType crashType) {
        return new JSONObject((Map)this.a.mTagMap);
    }
    
    @Nullable
    private JSONObject e() {
        final JSONObject jsonObject = new JSONObject();
        try {
            if (this.a.mConfig.mPackageName == null) {
                final Context g = i.g();
                final PackageInfo packageInfo = g.getPackageManager().getPackageInfo(g.getPackageName(), 128);
                if (packageInfo != null) {
                    final MonitorCrash.Config mConfig = this.a.mConfig;
                    if (mConfig.mVersionInt == -1L) {
                        mConfig.mVersionInt = packageInfo.versionCode;
                    }
                    if (mConfig.mVersionStr == null) {
                        mConfig.mVersionStr = packageInfo.versionName;
                    }
                }
            }
        }
        finally {}
        if (TextUtils.isEmpty((CharSequence)this.a.mConfig.mDeviceId) || "0".equals(this.a.mConfig.mDeviceId)) {
            this.a.mConfig.mDeviceId = i.c().a();
        }
        try {
            jsonObject.put("aid", (Object)String.valueOf(this.a.mConfig.mAid));
            jsonObject.put("update_version_code", this.a.mConfig.mVersionInt);
            jsonObject.put("version_code", this.a.mConfig.mVersionInt);
            jsonObject.put("app_version", (Object)this.a.mConfig.mVersionStr);
            jsonObject.put("channel", (Object)this.a.mConfig.mChannel);
            jsonObject.put("package", (Object)l.a(this.a.mConfig.mPackageName));
            jsonObject.put("device_id", (Object)this.a.mConfig.mDeviceId);
            jsonObject.put("user_id", (Object)this.a.mConfig.mUID);
            jsonObject.put("ssid", (Object)this.a.mConfig.mSSID);
            jsonObject.put("os", (Object)"Android");
            jsonObject.put("so_list", (Object)l.a(this.a.mConfig.mSoList));
            jsonObject.put("thread_list", (Object)l.a(this.a.mConfig.mThreadList));
            jsonObject.put("single_upload", (int)(this.d() ? 1 : 0));
            return jsonObject;
        }
        catch (final JSONException ex) {
            return jsonObject;
        }
    }
    
    @Nullable
    public JSONArray a(final StackTraceElement[] array, final Throwable t) {
        return this.a(array, t, null);
    }
    
    @Nullable
    public JSONArray a(final StackTraceElement[] array, final Throwable t, final String s) {
        final String[] mPackageName = this.a.mConfig.mPackageName;
        if (mPackageName == null) {
            return new JSONArray().put((Object)new v.a(0, array.length).a());
        }
        if (t != null && array != null) {
            final JSONArray a = v.a(array, mPackageName);
            if (s != null && this.a.mConfig.mThreadList != null && l.a(a)) {
                final String[] mThreadList = this.a.mConfig.mThreadList;
                for (int length = mThreadList.length, i = 0; i < length; ++i) {
                    if (TextUtils.equals((CharSequence)mThreadList[i], (CharSequence)s)) {
                        a.put((Object)new v.a(0, array.length).a());
                    }
                }
            }
            return a;
        }
        return null;
    }
    
    public JSONArray a(final String[] array) {
        if (this.a.config().mPackageName == null) {
            return new JSONArray().put((Object)new v.a(0, array.length).a());
        }
        return v.a(array, this.a.mConfig.mPackageName);
    }
    
    public JSONObject a(final CrashType crashType) {
        return this.a(crashType, null);
    }
    
    public JSONObject a(final CrashType crashType, final JSONArray jsonArray) {
        final JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("header", (Object)this.e());
            if (crashType != null) {
                jsonObject.put("custom", (Object)this.b(crashType));
                jsonObject.put("filters", (Object)this.c(crashType));
            }
            jsonObject.put("line_num", (Object)jsonArray);
            return jsonObject;
        }
        finally {
            return jsonObject;
        }
    }
    
    public boolean a(final Object o) {
        return this.a == o;
    }
    
    public boolean a(final String s, final String s2) {
        return this.a.config().mPackageName == null || com.apm.insight.entity.b.a(s, l.a(this.a.mConfig.mSoList), s2, l.a(this.a.mConfig.mThreadList));
    }
    
    public String b() {
        return this.a.mConfig.mAid;
    }
    
    public JSONObject c() {
        return this.e();
    }
    
    public boolean d() {
        return false;
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.apm.insight;

import com.apm.insight.runtime.ConfigManager;
import com.apm.insight.l.q;
import android.text.TextUtils;
import java.util.Map;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.apm.insight.runtime.o;
import android.content.Context;
import java.util.HashMap;

public class MonitorCrash
{
    private static volatile boolean sAppMonitorCrashInit = false;
    Config mConfig;
    AttachUserData mCustomData;
    AttachUserData mCustomLongData;
    HeaderParams mParams;
    HashMap<String, String> mTagMap;
    
    private MonitorCrash(final Context context, final String mAid, final long mVersionInt, final String mVersionStr) {
        this.mTagMap = new HashMap<String, String>();
        final Config mConfig = new Config();
        this.mConfig = mConfig;
        mConfig.mAid = mAid;
        mConfig.mVersionInt = mVersionInt;
        mConfig.mVersionStr = mVersionStr;
        h.a(context, this);
    }
    
    private MonitorCrash(final String mAid, final long mVersionInt, final String mVersionStr, final String... mPackageName) {
        this.mTagMap = new HashMap<String, String>();
        final Config mConfig = new Config();
        this.mConfig = mConfig;
        mConfig.mAid = mAid;
        mConfig.mVersionInt = mVersionInt;
        mConfig.mVersionStr = mVersionStr;
        mConfig.mPackageName = mPackageName;
        h.a(this);
    }
    
    @Nullable
    public static MonitorCrash init(final Context context, final String s, final long n, final String s2) {
        if (!MonitorCrash.sAppMonitorCrashInit) {
            synchronized (MonitorCrash.class) {
                if (!MonitorCrash.sAppMonitorCrashInit) {
                    o.a(context, MonitorCrash.sAppMonitorCrashInit = true, true, true, true, 0L);
                    return new MonitorCrash(context, s, n, s2);
                }
            }
        }
        return null;
    }
    
    public static MonitorCrash initSDK(final Context context, final String s, final long n, final String s2, final String packageName) {
        o.a(context, true, true, true, true, 0L);
        final MonitorCrash monitorCrash = new MonitorCrash(s, n, s2, new String[] { packageName });
        monitorCrash.config().setPackageName(packageName);
        return monitorCrash;
    }
    
    public static MonitorCrash initSDK(final Context context, final String s, final long n, final String s2, final String packageName, final String[] soList) {
        o.a(context, true, true, true, true, 0L);
        final MonitorCrash monitorCrash = new MonitorCrash(s, n, s2, new String[] { packageName });
        monitorCrash.config().setPackageName(packageName).setSoList(soList);
        return monitorCrash;
    }
    
    public static MonitorCrash initSDK(final Context context, final String s, final long n, final String s2, final String... packageName) {
        o.a(context, true, true, true, true, 0L);
        final MonitorCrash monitorCrash = new MonitorCrash(s, n, s2, packageName);
        monitorCrash.config().setPackageName(packageName);
        return monitorCrash;
    }
    
    public static MonitorCrash initSDK(final Context context, final String s, final long n, final String s2, final String[] packageName, final String[] soList) {
        o.a(context, true, true, true, true, 0L);
        final MonitorCrash monitorCrash = new MonitorCrash(s, n, s2, packageName);
        monitorCrash.config().setPackageName(packageName).setSoList(soList);
        return monitorCrash;
    }
    
    public MonitorCrash addTags(final String key, final String value) {
        this.mTagMap.put(key, value);
        return this;
    }
    
    @NonNull
    public Config config() {
        return this.mConfig;
    }
    
    public void registerCrashCallback(final ICrashCallback crashCallback, final CrashType crashType) {
        if (this == h.b) {
            o.a(crashCallback, crashType);
        }
        else {
            o.a((ICrashCallback)new b(this.mConfig.mAid, crashCallback), crashType);
        }
    }
    
    public void registerOOMCallback(final IOOMCallback ioomCallback) {
        if (this == h.b) {
            o.a(ioomCallback);
        }
        else {
            o.a(new b(this.mConfig.mAid, ioomCallback));
        }
    }
    
    public void reportCustomErr(final String s, final String s2, final Throwable t) {
        com.apm.insight.f.b.a(this, t, s, true, null, s2, "core_exception_monitor");
    }
    
    public MonitorCrash setCustomDataCallback(final AttachUserData mCustomData) {
        this.mCustomData = mCustomData;
        return this;
    }
    
    public MonitorCrash setReportUrl(String string) {
        if (TextUtils.isEmpty((CharSequence)string)) {
            return this;
        }
        int index = string.indexOf("://");
        if (index < 0) {
            final StringBuilder sb = new StringBuilder();
            sb.append("https://");
            sb.append(string);
            string = sb.toString();
            index = 8;
        }
        else {
            index += 3;
        }
        final int index2 = string.indexOf("/", index);
        String substring = string;
        if (index2 >= 0) {
            substring = string.substring(0, index2);
        }
        final StringBuilder sb2 = new StringBuilder();
        sb2.append("set url ");
        sb2.append(substring);
        q.a((Object)sb2.toString());
        final ConfigManager i = com.apm.insight.i.i();
        final StringBuilder sb3 = new StringBuilder();
        sb3.append(substring);
        sb3.append("/monitor/collect/c/exception");
        i.setLaunchCrashUrl(sb3.toString());
        final ConfigManager j = com.apm.insight.i.i();
        final StringBuilder sb4 = new StringBuilder();
        sb4.append(substring);
        sb4.append("/monitor/collect/c/crash");
        j.setJavaCrashUploadUrl(sb4.toString());
        final ConfigManager k = com.apm.insight.i.i();
        final StringBuilder sb5 = new StringBuilder();
        sb5.append(substring);
        sb5.append("/monitor/collect/c/native_bin_crash");
        k.setNativeCrashUrl(sb5.toString());
        final ConfigManager l = com.apm.insight.i.i();
        final StringBuilder sb6 = new StringBuilder();
        sb6.append(substring);
        sb6.append("/settings/get");
        l.setConfigUrl(sb6.toString());
        return this;
    }
    
    public MonitorCrash withOtherHeaders(@Nullable final HeaderParams mParams) {
        this.mParams = mParams;
        return this;
    }
    
    public class Config
    {
        String mAid;
        String mChannel;
        String mDeviceId;
        String[] mPackageName;
        String mSSID;
        String[] mSoList;
        String[] mThreadList;
        String mUID;
        long mVersionInt;
        String mVersionStr;
        final MonitorCrash this$0;
        
        public Config(final MonitorCrash this$0) {
            this.this$0 = this$0;
            this.mVersionInt = -1L;
        }
        
        public Config setChannel(final String mChannel) {
            this.mChannel = mChannel;
            com.apm.insight.j.b.d();
            return this;
        }
        
        public Config setDeviceId(final String mDeviceId) {
            this.mDeviceId = mDeviceId;
            com.apm.insight.j.b.d();
            return this;
        }
        
        public Config setPackageName(final String s) {
            return this.setPackageName(new String[] { s });
        }
        
        public Config setPackageName(final String... mPackageName) {
            this.mPackageName = mPackageName;
            com.apm.insight.j.b.d();
            return this;
        }
        
        public Config setSSID(final String mssid) {
            this.mSSID = mssid;
            com.apm.insight.j.b.d();
            return this;
        }
        
        public Config setSoList(final String[] mSoList) {
            this.mSoList = mSoList;
            com.apm.insight.j.b.d();
            return this;
        }
        
        public Config setThreadList(final String[] mThreadList) {
            this.mThreadList = mThreadList;
            com.apm.insight.j.b.d();
            return this;
        }
        
        public Config setUID(final String muid) {
            this.mUID = muid;
            com.apm.insight.j.b.d();
            return this;
        }
    }
    
    public interface HeaderParams
    {
        Map<String, Object> getCommonParams();
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.apm.insight.l;

import android.annotation.TargetApi;
import android.os.Debug$MemoryInfo;

public class c
{
    static final a a;
    
    static {
        a = (a)new b();
    }
    
    public static int a(final Debug$MemoryInfo debug$MemoryInfo) {
        return c.a.a(debug$MemoryInfo);
    }
    
    public static int b(final Debug$MemoryInfo debug$MemoryInfo) {
        return c.a.b(debug$MemoryInfo);
    }
    
    public static int c(final Debug$MemoryInfo debug$MemoryInfo) {
        return c.a.c(debug$MemoryInfo);
    }
    
    private static class a
    {
        public int a(final Debug$MemoryInfo debug$MemoryInfo) {
            return -1;
        }
        
        public int b(final Debug$MemoryInfo debug$MemoryInfo) {
            return -1;
        }
        
        public int c(final Debug$MemoryInfo debug$MemoryInfo) {
            return -1;
        }
    }
    
    @TargetApi(19)
    private static class b extends a
    {
        @Override
        public int a(final Debug$MemoryInfo debug$MemoryInfo) {
            return debug$MemoryInfo.getTotalPrivateClean();
        }
        
        @Override
        public int b(final Debug$MemoryInfo debug$MemoryInfo) {
            return debug$MemoryInfo.getTotalSharedClean();
        }
        
        @Override
        public int c(final Debug$MemoryInfo debug$MemoryInfo) {
            return debug$MemoryInfo.getTotalSwappablePss();
        }
    }
}

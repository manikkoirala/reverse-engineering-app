// 
// Decompiled by Procyon v0.6.0
// 

package com.apm.insight.l;

import java.nio.CharBuffer;
import androidx.annotation.NonNull;
import java.io.OutputStream;
import java.nio.charset.Charset;
import java.security.MessageDigest;
import java.io.PrintWriter;

public class e extends PrintWriter
{
    private MessageDigest a;
    private Charset b;
    private a c;
    
    public e(@NonNull final OutputStream out, final MessageDigest a, final a c) {
        super(out);
        this.b = null;
        this.a = a;
        this.c = c;
        if (a != null) {
            this.b = Charset.defaultCharset();
        }
    }
    
    @Override
    public void write(final int c) {
        super.write(c);
        final MessageDigest a = this.a;
        if (a != null) {
            a.update((byte)c);
        }
    }
    
    @Override
    public void write(@NonNull final String s, final int n, final int len) {
        super.write(s, n, len);
        if (this.a != null) {
            final a c = this.c;
            if (c == null || c.a(s)) {
                this.a.update(this.b.encode(CharBuffer.wrap(s, n, len + n)).array());
            }
        }
    }
    
    @Override
    public void write(@NonNull final char[] array, final int off, final int len) {
        super.write(array, off, len);
        final MessageDigest a = this.a;
        if (a != null) {
            a.update(this.b.encode(CharBuffer.wrap(array)).array());
        }
    }
    
    public static class a
    {
        public boolean a(final String s) {
            return true;
        }
    }
}

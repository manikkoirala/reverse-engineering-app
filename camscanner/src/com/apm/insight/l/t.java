// 
// Decompiled by Procyon v0.6.0
// 

package com.apm.insight.l;

import android.os.Build$VERSION;
import java.util.Locale;
import java.io.Closeable;
import java.io.Reader;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import android.os.Build;
import android.text.TextUtils;

public class t
{
    private static final CharSequence a;
    private static final CharSequence b;
    private static final CharSequence c;
    
    static {
        a = "sony";
        b = "amigo";
        c = "funtouch";
    }
    
    public static String a() {
        if (d.c()) {
            return j();
        }
        if (d.d()) {
            return l();
        }
        if (m()) {
            return n();
        }
        final String k = k();
        if (!TextUtils.isEmpty((CharSequence)k)) {
            return k;
        }
        if (e()) {
            return d();
        }
        if (f()) {
            return g();
        }
        if (c()) {
            return b();
        }
        final String h = h();
        if (!TextUtils.isEmpty((CharSequence)h)) {
            return h;
        }
        return Build.DISPLAY;
    }
    
    private static String a(String line) {
        Object o = "";
        Object runtime;
        Process exec;
        try {
            runtime = Runtime.getRuntime();
            final StringBuilder sb = new StringBuilder();
            sb.append("getprop ");
            sb.append((String)line);
            exec = ((Runtime)runtime).exec(sb.toString());
            runtime = new BufferedReader(new InputStreamReader(exec.getInputStream()), 1024);
            line = o;
            final Runtime runtime2 = (Runtime)runtime;
            final String line2 = ((BufferedReader)runtime2).readLine();
            line = line2;
            final Process process = exec;
            process.destroy();
            final Runtime runtime3 = (Runtime)runtime;
            k.a((Closeable)runtime3);
            return line2;
        }
        finally {
            final Object o2;
            line = o2;
            line = null;
        }
        while (true) {
            try {
                final Runtime runtime2 = (Runtime)runtime;
                final String line2 = (String)(line = ((BufferedReader)runtime2).readLine());
                final Process process = exec;
                process.destroy();
                final Runtime runtime3 = (Runtime)runtime;
                k.a((Closeable)runtime3);
                return line2;
                k.a((Closeable)line);
                return (String)o;
            }
            finally {
                o = line;
                line = runtime;
                continue;
            }
            break;
        }
    }
    
    public static String b() {
        final StringBuilder sb = new StringBuilder();
        sb.append(a("ro.build.uiversion"));
        sb.append("_");
        sb.append(Build.DISPLAY);
        return sb.toString();
    }
    
    public static boolean c() {
        final StringBuilder sb = new StringBuilder();
        sb.append(Build.MANUFACTURER);
        sb.append(Build.BRAND);
        final String string = sb.toString();
        final boolean empty = TextUtils.isEmpty((CharSequence)string);
        boolean b = false;
        if (empty) {
            return false;
        }
        final String lowerCase = string.toLowerCase(Locale.getDefault());
        if (lowerCase.contains("360") || lowerCase.contains("qiku")) {
            b = true;
        }
        return b;
    }
    
    public static String d() {
        final StringBuilder sb = new StringBuilder();
        sb.append(a("ro.vivo.os.build.display.id"));
        sb.append("_");
        sb.append(a("ro.vivo.product.version"));
        return sb.toString();
    }
    
    public static boolean e() {
        final String a = a("ro.vivo.os.build.display.id");
        return !TextUtils.isEmpty((CharSequence)a) && a.toLowerCase(Locale.getDefault()).contains(t.c);
    }
    
    public static boolean f() {
        final String display = Build.DISPLAY;
        return !TextUtils.isEmpty((CharSequence)display) && display.toLowerCase(Locale.getDefault()).contains(t.b);
    }
    
    public static String g() {
        final StringBuilder sb = new StringBuilder();
        sb.append(Build.DISPLAY);
        sb.append("_");
        sb.append(a("ro.gn.sv.version"));
        return sb.toString();
    }
    
    public static String h() {
        if (i()) {
            final StringBuilder sb = new StringBuilder();
            sb.append("eui_");
            sb.append(a("ro.letv.release.version"));
            sb.append("_");
            sb.append(Build.DISPLAY);
            return sb.toString();
        }
        return "";
    }
    
    public static boolean i() {
        return TextUtils.isEmpty((CharSequence)a("ro.letv.release.version")) ^ true;
    }
    
    public static String j() {
        if (d.c()) {
            final StringBuilder sb = new StringBuilder();
            sb.append("miui_");
            sb.append(a("ro.miui.ui.version.name"));
            sb.append("_");
            sb.append(Build$VERSION.INCREMENTAL);
            return sb.toString();
        }
        return "";
    }
    
    public static String k() {
        final String a = d.a();
        if (a != null && a.toLowerCase(Locale.getDefault()).contains("emotionui")) {
            final StringBuilder sb = new StringBuilder();
            sb.append(a);
            sb.append("_");
            sb.append(Build.DISPLAY);
            return sb.toString();
        }
        return "";
    }
    
    public static String l() {
        final String display = Build.DISPLAY;
        if (display != null && display.toLowerCase(Locale.getDefault()).contains("flyme")) {
            return display;
        }
        return "";
    }
    
    public static boolean m() {
        final String manufacturer = Build.MANUFACTURER;
        return !TextUtils.isEmpty((CharSequence)manufacturer) && manufacturer.toLowerCase(Locale.getDefault()).contains("oppo");
    }
    
    public static String n() {
        if (m()) {
            final StringBuilder sb = new StringBuilder();
            sb.append("coloros_");
            sb.append(a("ro.build.version.opporom"));
            sb.append("_");
            sb.append(Build.DISPLAY);
            return sb.toString();
        }
        return "";
    }
}

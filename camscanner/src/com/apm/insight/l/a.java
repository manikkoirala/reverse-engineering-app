// 
// Decompiled by Procyon v0.6.0
// 

package com.apm.insight.l;

import android.app.ActivityManager$RunningAppProcessInfo;
import android.app.ActivityManager$MemoryInfo;
import com.apm.insight.runtime.a.b;
import android.text.TextUtils;
import android.os.Build$VERSION;
import android.os.Debug;
import android.os.Debug$MemoryInfo;
import org.json.JSONObject;
import java.util.Iterator;
import java.util.List;
import android.os.SystemClock;
import com.apm.insight.b.f;
import android.os.Process;
import android.app.ActivityManager;
import android.app.ActivityManager$ProcessErrorStateInfo;
import android.content.Context;
import java.lang.reflect.Field;

public final class a
{
    private static String a;
    private static Class<?> b;
    private static Field c;
    private static Field d;
    private static boolean e = false;
    
    private static long a(final int n) {
        if (n < 0) {
            return 0L;
        }
        return n * 1024L;
    }
    
    public static ActivityManager$ProcessErrorStateInfo a(final Context context, final int n) {
        final ActivityManager activityManager = (ActivityManager)context.getSystemService("activity");
        if (activityManager == null) {
            return null;
        }
        final int myPid = Process.myPid();
        int i = 0;
        while (i < n) {
            final List processesInErrorState = activityManager.getProcessesInErrorState();
            if (processesInErrorState != null) {
                for (final ActivityManager$ProcessErrorStateInfo activityManager$ProcessErrorStateInfo : processesInErrorState) {
                    if (activityManager$ProcessErrorStateInfo.pid == myPid) {
                        if (activityManager$ProcessErrorStateInfo.condition != 2) {
                            continue;
                        }
                        return activityManager$ProcessErrorStateInfo;
                    }
                }
            }
            ++i;
            if (n == i) {
                break;
            }
            if (f.a()) {
                break;
            }
            SystemClock.sleep(200L);
        }
        return null;
    }
    
    private static String a() {
        // 
        // This method could not be decompiled.
        // 
        // Original Bytecode:
        // 
        //     3: astore_1       
        //     4: new             Ljava/io/InputStreamReader;
        //     7: astore_3       
        //     8: new             Ljava/io/FileInputStream;
        //    11: astore          4
        //    13: new             Ljava/lang/StringBuilder;
        //    16: astore_2       
        //    17: aload_2        
        //    18: invokespecial   java/lang/StringBuilder.<init>:()V
        //    21: aload_2        
        //    22: ldc             "/proc/"
        //    24: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //    27: pop            
        //    28: aload_2        
        //    29: invokestatic    android/os/Process.myPid:()I
        //    32: invokevirtual   java/lang/StringBuilder.append:(I)Ljava/lang/StringBuilder;
        //    35: pop            
        //    36: aload_2        
        //    37: ldc             "/cmdline"
        //    39: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //    42: pop            
        //    43: aload           4
        //    45: aload_2        
        //    46: invokevirtual   java/lang/StringBuilder.toString:()Ljava/lang/String;
        //    49: invokespecial   java/io/FileInputStream.<init>:(Ljava/lang/String;)V
        //    52: aload_3        
        //    53: aload           4
        //    55: ldc             "iso-8859-1"
        //    57: invokespecial   java/io/InputStreamReader.<init>:(Ljava/io/InputStream;Ljava/lang/String;)V
        //    60: aload_1        
        //    61: aload_3        
        //    62: invokespecial   java/io/BufferedReader.<init>:(Ljava/io/Reader;)V
        //    65: new             Ljava/lang/StringBuilder;
        //    68: astore_2       
        //    69: aload_2        
        //    70: invokespecial   java/lang/StringBuilder.<init>:()V
        //    73: aload_1        
        //    74: invokevirtual   java/io/BufferedReader.read:()I
        //    77: istore_0       
        //    78: iload_0        
        //    79: ifle            92
        //    82: aload_2        
        //    83: iload_0        
        //    84: i2c            
        //    85: invokevirtual   java/lang/StringBuilder.append:(C)Ljava/lang/StringBuilder;
        //    88: pop            
        //    89: goto            73
        //    92: aload_2        
        //    93: invokevirtual   java/lang/StringBuilder.toString:()Ljava/lang/String;
        //    96: astore_2       
        //    97: aload_1        
        //    98: invokestatic    com/apm/insight/l/k.a:(Ljava/io/Closeable;)V
        //   101: aload_2        
        //   102: areturn        
        //   103: astore_1       
        //   104: aconst_null    
        //   105: astore_1       
        //   106: aload_1        
        //   107: invokestatic    com/apm/insight/l/k.a:(Ljava/io/Closeable;)V
        //   110: aconst_null    
        //   111: areturn        
        //   112: astore_2       
        //   113: goto            106
        //    Exceptions:
        //  Try           Handler
        //  Start  End    Start  End    Type
        //  -----  -----  -----  -----  ----
        //  0      65     103    106    Any
        //  65     73     112    116    Any
        //  73     78     112    116    Any
        //  82     89     112    116    Any
        //  92     97     112    116    Any
        // 
        // The error that occurred was:
        // 
        // java.lang.IllegalStateException: Expression is linked from several locations: Label_0073:
        //     at com.strobel.decompiler.ast.Error.expressionLinkedFromMultipleLocations(Error.java:27)
        //     at com.strobel.decompiler.ast.AstOptimizer.mergeDisparateObjectInitializations(AstOptimizer.java:2604)
        //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:235)
        //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:42)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:206)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:93)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethodBody(AstBuilder.java:868)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethod(AstBuilder.java:761)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addTypeMembers(AstBuilder.java:638)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeCore(AstBuilder.java:605)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeNoCache(AstBuilder.java:195)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createType(AstBuilder.java:162)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addType(AstBuilder.java:137)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.buildAst(JavaLanguage.java:71)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.decompileType(JavaLanguage.java:59)
        //     at com.strobel.decompiler.DecompilerDriver.decompileType(DecompilerDriver.java:333)
        //     at com.strobel.decompiler.DecompilerDriver.decompileJar(DecompilerDriver.java:254)
        //     at com.strobel.decompiler.DecompilerDriver.main(DecompilerDriver.java:144)
        // 
        throw new IllegalStateException("An error occurred while decompiling this method.");
    }
    
    public static void a(final Context context, final JSONObject jsonObject) {
        try {
            a(jsonObject);
            final ActivityManager activityManager = (ActivityManager)context.getSystemService("activity");
            if (activityManager != null) {
                b(jsonObject, activityManager);
            }
            a(jsonObject, activityManager);
        }
        finally {}
    }
    
    public static void a(final String a) {
        a.a = a;
    }
    
    private static void a(final JSONObject jsonObject) {
        final Debug$MemoryInfo debug$MemoryInfo = new Debug$MemoryInfo();
        Debug.getMemoryInfo(debug$MemoryInfo);
        final JSONObject jsonObject2 = new JSONObject();
        jsonObject2.put("dalvikPrivateDirty", a(debug$MemoryInfo.dalvikPrivateDirty));
        jsonObject2.put("dalvikPss", a(debug$MemoryInfo.dalvikPss));
        jsonObject2.put("dalvikSharedDirty", a(debug$MemoryInfo.dalvikSharedDirty));
        jsonObject2.put("nativePrivateDirty", a(debug$MemoryInfo.nativePrivateDirty));
        jsonObject2.put("nativePss", a(debug$MemoryInfo.nativePss));
        jsonObject2.put("nativeSharedDirty", a(debug$MemoryInfo.nativeSharedDirty));
        jsonObject2.put("otherPrivateDirty", a(debug$MemoryInfo.otherPrivateDirty));
        jsonObject2.put("otherPss", a(debug$MemoryInfo.otherPss));
        jsonObject2.put("otherSharedDirty", debug$MemoryInfo.otherSharedDirty);
        while (true) {
            if (Build$VERSION.SDK_INT < 23) {
                break Label_0179;
            }
            try {
                final String \u3007080 = com.apm.insight.l.\u3007080.\u3007080(debug$MemoryInfo, "summary.graphics");
                if (!TextUtils.isEmpty((CharSequence)\u3007080)) {
                    jsonObject2.put("summary.graphics", a(Integer.parseInt(\u3007080)));
                }
                jsonObject2.put("totalPrivateClean", com.apm.insight.l.c.a(debug$MemoryInfo));
                jsonObject2.put("totalPrivateDirty", debug$MemoryInfo.getTotalPrivateDirty());
                jsonObject2.put("totalPss", a(debug$MemoryInfo.getTotalPss()));
                jsonObject2.put("totalSharedClean", com.apm.insight.l.c.b(debug$MemoryInfo));
                jsonObject2.put("totalSharedDirty", a(debug$MemoryInfo.getTotalSharedDirty()));
                jsonObject2.put("totalSwappablePss", a(com.apm.insight.l.c.c(debug$MemoryInfo)));
                jsonObject.put("memory_info", (Object)jsonObject2);
            }
            finally {
                continue;
            }
            break;
        }
    }
    
    private static void a(final JSONObject jsonObject, final ActivityManager activityManager) {
        final JSONObject jsonObject2 = new JSONObject();
        final long nativeHeapAllocatedSize = Debug.getNativeHeapAllocatedSize();
        final boolean b = true;
        com.apm.insight.entity.a.a(jsonObject, "filters", "native_heap_leak", String.valueOf(nativeHeapAllocatedSize > 209715200L));
        jsonObject2.put("native_heap_size", Debug.getNativeHeapSize());
        jsonObject2.put("native_heap_alloc_size", Debug.getNativeHeapAllocatedSize());
        jsonObject2.put("native_heap_free_size", Debug.getNativeHeapFreeSize());
        final Runtime runtime = Runtime.getRuntime();
        final long maxMemory = runtime.maxMemory();
        final long freeMemory = runtime.freeMemory();
        final long totalMemory = runtime.totalMemory();
        jsonObject2.put("max_memory", maxMemory);
        jsonObject2.put("free_memory", freeMemory);
        jsonObject2.put("total_memory", totalMemory);
        com.apm.insight.entity.a.a(jsonObject, "filters", "java_heap_leak", String.valueOf(totalMemory - freeMemory > maxMemory * 0.95f && b));
        if (activityManager != null) {
            jsonObject2.put("memory_class", activityManager.getMemoryClass());
            jsonObject2.put("large_memory_class", activityManager.getLargeMemoryClass());
        }
        jsonObject.put("app_memory_info", (Object)jsonObject2);
    }
    
    public static boolean a(final Context context) {
        if (context == null) {
            return com.apm.insight.runtime.a.b.d().f();
        }
        return com.apm.insight.runtime.a.b.d().f() || f(context);
    }
    
    private static void b(final JSONObject jsonObject, final ActivityManager activityManager) {
        final JSONObject jsonObject2 = new JSONObject();
        final ActivityManager$MemoryInfo activityManager$MemoryInfo = new ActivityManager$MemoryInfo();
        activityManager.getMemoryInfo(activityManager$MemoryInfo);
        jsonObject2.put("availMem", activityManager$MemoryInfo.availMem);
        jsonObject2.put("lowMemory", activityManager$MemoryInfo.lowMemory);
        jsonObject2.put("threshold", activityManager$MemoryInfo.threshold);
        jsonObject2.put("totalMem", n.a(activityManager$MemoryInfo));
        jsonObject.put("sys_memory_info", (Object)jsonObject2);
    }
    
    public static boolean b(final Context context) {
        final String c = c(context);
        final boolean b = false;
        if (c != null && c.contains(":")) {
            return false;
        }
        if (c != null && c.equals(context.getPackageName())) {
            return true;
        }
        boolean b2 = b;
        if (c != null) {
            b2 = b;
            if (c.equals(context.getApplicationInfo().processName)) {
                b2 = true;
            }
        }
        return b2;
    }
    
    public static String c(final Context context) {
        if (!TextUtils.isEmpty((CharSequence)com.apm.insight.l.a.a)) {
            return com.apm.insight.l.a.a;
        }
        try {
            final int myPid = Process.myPid();
            final ActivityManager activityManager = (ActivityManager)context.getSystemService("activity");
            if (activityManager != null) {
                for (final ActivityManager$RunningAppProcessInfo activityManager$RunningAppProcessInfo : activityManager.getRunningAppProcesses()) {
                    if (activityManager$RunningAppProcessInfo.pid == myPid) {
                        return com.apm.insight.l.a.a = activityManager$RunningAppProcessInfo.processName;
                    }
                }
            }
        }
        finally {}
        String s;
        if ((s = (com.apm.insight.l.a.a = a())) == null) {
            s = "";
        }
        return s;
    }
    
    public static String d(final Context context) {
        final Class<?> g = g(context);
        if (com.apm.insight.l.a.c == null && g != null) {
            try {
                com.apm.insight.l.a.c = g.getDeclaredField("VERSION_NAME");
            }
            catch (final NoSuchFieldException ex) {}
        }
        final Field c = com.apm.insight.l.a.c;
        if (c == null) {
            return "";
        }
        try {
            return String.valueOf(c.get(null));
        }
        finally {
            return "";
        }
    }
    
    public static int e(final Context context) {
        final Class<?> g = g(context);
        if (com.apm.insight.l.a.d == null && g != null) {
            try {
                com.apm.insight.l.a.d = g.getDeclaredField("VERSION_CODE");
            }
            catch (final NoSuchFieldException ex) {}
        }
        final Field d = com.apm.insight.l.a.d;
        if (d == null) {
            return -1;
        }
        try {
            return (int)d.get(null);
        }
        finally {
            return -1;
        }
    }
    
    private static boolean f(final Context context) {
        final String packageName = context.getPackageName();
        try {
            final ActivityManager activityManager = (ActivityManager)context.getSystemService("activity");
            if (activityManager == null) {
                return false;
            }
            final List runningAppProcesses = activityManager.getRunningAppProcesses();
            if (runningAppProcesses != null) {
                for (final ActivityManager$RunningAppProcessInfo activityManager$RunningAppProcessInfo : runningAppProcesses) {
                    if (activityManager$RunningAppProcessInfo.importance == 100) {
                        return packageName.equals(activityManager$RunningAppProcessInfo.pkgList[0]);
                    }
                }
            }
            return false;
        }
        finally {
            return false;
        }
    }
    
    private static Class<?> g(final Context context) {
        Label_0051: {
            if (com.apm.insight.l.a.b != null || com.apm.insight.l.a.e) {
                break Label_0051;
            }
            while (true) {
                try {
                    final StringBuilder sb = new StringBuilder();
                    sb.append(context.getPackageName());
                    sb.append(".BuildConfig");
                    com.apm.insight.l.a.b = Class.forName(sb.toString());
                    com.apm.insight.l.a.e = true;
                    return com.apm.insight.l.a.b;
                }
                catch (final ClassNotFoundException ex) {
                    continue;
                }
                break;
            }
        }
    }
}

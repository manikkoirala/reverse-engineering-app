// 
// Decompiled by Procyon v0.6.0
// 

package com.apm.insight.l;

import java.io.Writer;
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.Reader;
import java.io.BufferedReader;
import java.io.FileReader;
import com.apm.insight.runtime.a;
import androidx.annotation.NonNull;
import java.io.IOException;
import com.apm.insight.nativecrash.NativeImpl;
import com.apm.insight.i;
import java.io.File;

public class f
{
    @NonNull
    public static File a(String s, final int i, final int j) {
        s = (String)new File(o.a(i.g(), s), "logcat.txt");
        if (((File)s).exists() && ((File)s).length() > 0L) {
            return (File)s;
        }
        ((File)s).getParentFile().mkdirs();
        while (true) {
            try {
                ((File)s).createNewFile();
                NativeImpl.a(((File)s).getAbsolutePath(), String.valueOf(i), String.valueOf(j));
                return (File)s;
            }
            catch (final IOException ex) {
                continue;
            }
            break;
        }
    }
    
    public static void a() {
        try {
            a(i.f(), i.i().getLogcatDumpCount(), i.i().getLogcatLevel());
            if (i.u()) {
                b();
                d();
                c();
                e();
            }
        }
        finally {}
    }
    
    @NonNull
    public static File b() {
        final File file = new File(o.e(i.g()), "maps.txt");
        if (file.exists()) {
            return file;
        }
        file.getParentFile().mkdirs();
        while (true) {
            try {
                file.createNewFile();
                NativeImpl.f(file.getAbsolutePath());
                return file;
            }
            catch (final IOException ex) {
                continue;
            }
            break;
        }
    }
    
    @NonNull
    public static File c() {
        final File file = new File(o.e(i.g()), "meminfo.txt");
        if (file.exists()) {
            return file;
        }
        file.getParentFile().mkdirs();
        while (true) {
            try {
                file.createNewFile();
                NativeImpl.d(file.getAbsolutePath());
                return file;
            }
            catch (final IOException ex) {
                continue;
            }
            break;
        }
    }
    
    public static File d() {
        final File file = new File(o.e(i.g()), "fds.txt");
        if (file.exists()) {
            return file;
        }
        file.getParentFile().mkdirs();
        while (true) {
            try {
                file.createNewFile();
                NativeImpl.e(file.getAbsolutePath());
                return file;
            }
            catch (final IOException ex) {
                continue;
            }
            break;
        }
    }
    
    public static File e() {
        final File file = new File(o.e(i.g()), "threads.txt");
        if (file.exists()) {
            return file;
        }
        file.getParentFile().mkdirs();
        while (true) {
            try {
                file.createNewFile();
                NativeImpl.g(file.getAbsolutePath());
                return file;
            }
            catch (final IOException ex) {
                continue;
            }
            break;
        }
    }
    
    public static File f() {
        final File file = new File(o.e(i.g()), "anr_trace.txt");
        if (file.exists()) {
            return file;
        }
        if (!a.f()) {
            return file;
        }
        final File file2 = new File("/data/anr/traces.txt");
        if (!file2.exists()) {
            return file;
        }
        try {
            file.getParentFile().mkdirs();
            final BufferedReader bufferedReader = new BufferedReader(new FileReader(file2));
            try {
                final BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(file));
                int n = 0;
                try {
                    String line;
                    do {
                        line = bufferedReader.readLine();
                        if (line == null) {
                            goto Label_0179;
                        }
                        bufferedWriter.write(line);
                        bufferedWriter.write(10);
                    } while ((n += line.length()) < 1048576);
                }
                catch (final IOException ex) {}
            }
            catch (final IOException ex2) {}
        }
        catch (final IOException ex3) {}
    }
}

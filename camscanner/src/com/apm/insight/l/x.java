// 
// Decompiled by Procyon v0.6.0
// 

package com.apm.insight.l;

public final class x
{
    public static Thread a(final Runnable runnable, final String name) {
        if (runnable != null) {
            Thread thread;
            if (name == null) {
                thread = new Thread(runnable);
            }
            else {
                thread = new Thread(runnable, name);
            }
            thread.start();
            return thread;
        }
        return null;
    }
}

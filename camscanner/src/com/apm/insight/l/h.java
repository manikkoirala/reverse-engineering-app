// 
// Decompiled by Procyon v0.6.0
// 

package com.apm.insight.l;

import java.io.File;
import android.os.Process;

public final class h
{
    public static int a() {
        final int myPid = Process.myPid();
        int n = 0;
        try {
            final StringBuilder sb = new StringBuilder();
            sb.append("/proc/");
            sb.append(myPid);
            sb.append("/fd");
            final int length = new File(sb.toString()).listFiles().length;
        }
        finally {
            n = -1;
        }
        return n;
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.apm.insight.l;

import android.annotation.TargetApi;
import android.app.ActivityManager$MemoryInfo;

public class n
{
    static final a a;
    
    static {
        a = (a)new b();
    }
    
    public static long a(final ActivityManager$MemoryInfo activityManager$MemoryInfo) {
        return n.a.a(activityManager$MemoryInfo);
    }
    
    private static class a
    {
        public long a(final ActivityManager$MemoryInfo activityManager$MemoryInfo) {
            return 0L;
        }
    }
    
    @TargetApi(16)
    private static class b extends a
    {
        @Override
        public long a(final ActivityManager$MemoryInfo activityManager$MemoryInfo) {
            return activityManager$MemoryInfo.totalMem;
        }
    }
}

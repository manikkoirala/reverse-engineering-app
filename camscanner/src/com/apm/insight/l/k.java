// 
// Decompiled by Procyon v0.6.0
// 

package com.apm.insight.l;

import java.io.IOException;
import java.util.zip.ZipFile;
import com.apm.insight.h.a;
import java.io.Closeable;

public final class k
{
    public static void a(final Closeable closeable) {
        if (closeable == null) {
            return;
        }
        try {
            closeable.close();
        }
        finally {}
    }
    
    public static void a(final String s, final int i) {
        final Integer value = -1;
        a.a("android.os.FileUtils", "setPermissions", s, i, value, value);
    }
    
    public static void a(final ZipFile zipFile) {
        if (zipFile == null) {
            return;
        }
        try {
            zipFile.close();
        }
        catch (final IOException ex) {}
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.apm.insight.l;

import org.json.JSONObject;
import java.util.Iterator;
import java.util.Map;

public class r
{
    private static int a;
    
    public static int a(final Object obj, final int n) {
        if (obj == null) {
            return n;
        }
        if (obj instanceof Integer) {
            return (int)obj;
        }
        if (!(obj instanceof String)) {
            return n;
        }
        try {
            return Integer.parseInt(String.valueOf(obj));
        }
        finally {
            return n;
        }
    }
    
    public static Float a(final Map<? super String, Float> map) {
        final Iterator<Float> iterator = map.values().iterator();
        float f = 0.0f;
        while (iterator.hasNext()) {
            final Float n = iterator.next();
            if (n != null) {
                f += n;
            }
        }
        return f;
    }
    
    public static Long a(final Map<? super String, Long> map, final String s, final Long n) {
        if (s != null && map != null) {
            final Long n2 = map.get(s);
            Long value = n;
            if (n2 != null) {
                value = n2 + n;
            }
            map.put(s, value);
            return value;
        }
        return -1L;
    }
    
    public static void a(final JSONObject jsonObject) {
    }
    
    public static boolean a(final int n) {
        return false;
    }
    
    public static int b(final int n) {
        if (n == 0) {
            return 4;
        }
        if (n == 1) {
            return 8;
        }
        if (n == 2) {
            return 16;
        }
        if (n == 3) {
            return 32;
        }
        if (n == 4) {
            return 64;
        }
        if (n != 5) {
            return 0;
        }
        return 128;
    }
}

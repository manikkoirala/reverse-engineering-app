// 
// Decompiled by Procyon v0.6.0
// 

package com.apm.insight.l;

import org.json.JSONException;
import java.util.Iterator;
import org.json.JSONObject;
import org.json.JSONArray;
import java.util.ArrayList;
import java.util.List;
import java.io.Writer;

public class m
{
    final Writer a;
    private final List<a> b;
    
    public m(final Writer a) {
        this.b = new ArrayList<a>();
        this.a = a;
    }
    
    private void a(final a a) {
        final List<a> b = this.b;
        b.set(b.size() - 1, a);
    }
    
    private void a(final JSONArray jsonArray) {
        this.a();
        for (int i = 0; i < jsonArray.length(); ++i) {
            this.a(jsonArray.get(i));
        }
        this.b();
    }
    
    public static void a(final JSONArray jsonArray, final Writer writer) {
        new m(writer).a(jsonArray);
        writer.flush();
    }
    
    private void a(final JSONObject jsonObject) {
        this.c();
        final Iterator keys = jsonObject.keys();
        while (keys.hasNext()) {
            final String s = keys.next();
            this.a(s).a(jsonObject.get(s));
        }
        this.d();
    }
    
    public static void a(final JSONObject jsonObject, final Writer writer) {
        new m(writer).a(jsonObject);
        writer.flush();
    }
    
    private void b(final String s) {
        this.a.write("\"");
        for (int length = s.length(), i = 0; i < length; ++i) {
            final char char1 = s.charAt(i);
            Writer writer = null;
            String str = null;
            Label_0162: {
                if (char1 != '\f') {
                    if (char1 != '\r') {
                        if (char1 != '\"' && char1 != '/' && char1 != '\\') {
                            switch (char1) {
                                default: {
                                    if (char1 <= '\u001f') {
                                        this.a.write(String.format("\\u%04x", (int)char1));
                                        continue;
                                    }
                                    break;
                                }
                                case 10: {
                                    writer = this.a;
                                    str = "\\n";
                                    break Label_0162;
                                }
                                case 9: {
                                    writer = this.a;
                                    str = "\\t";
                                    break Label_0162;
                                }
                                case 8: {
                                    writer = this.a;
                                    str = "\\b";
                                    break Label_0162;
                                }
                            }
                        }
                        else {
                            this.a.write(92);
                        }
                        this.a.write(char1);
                        continue;
                    }
                    writer = this.a;
                    str = "\\r";
                }
                else {
                    writer = this.a;
                    str = "\\f";
                }
            }
            writer.write(str);
        }
        this.a.write("\"");
    }
    
    private a e() {
        final List<a> b = this.b;
        return b.get(b.size() - 1);
    }
    
    private void f() {
        final a e = this.e();
        if (e == m.a.e) {
            this.a.write(44);
        }
        else if (e != m.a.c) {
            throw new JSONException("Nesting problem");
        }
        this.a(m.a.d);
    }
    
    private void g() {
        if (this.b.isEmpty()) {
            return;
        }
        final a e = this.e();
        a a;
        if (e == m.a.a) {
            a = m.a.b;
        }
        else {
            if (e == m.a.b) {
                this.a.write(44);
                return;
            }
            if (e == m.a.d) {
                this.a.write(":");
                a = m.a.e;
            }
            else {
                if (e == m.a.f) {
                    return;
                }
                throw new JSONException("Nesting problem");
            }
        }
        this.a(a);
    }
    
    public m a() {
        return this.a(m.a.a, "[");
    }
    
    m a(final a a, final a a2, final String str) {
        this.e();
        final List<a> b = this.b;
        b.remove(b.size() - 1);
        this.a.write(str);
        return this;
    }
    
    m a(final a a, final String str) {
        this.g();
        this.b.add(a);
        this.a.write(str);
        return this;
    }
    
    public m a(final Object obj) {
        if (obj instanceof JSONArray) {
            this.a((JSONArray)obj);
            return this;
        }
        if (obj instanceof JSONObject) {
            this.a((JSONObject)obj);
            return this;
        }
        this.g();
        if (obj != null && obj != JSONObject.NULL) {
            String value;
            Writer writer;
            if (obj instanceof Boolean) {
                final Writer a = this.a;
                value = String.valueOf(obj);
                writer = a;
            }
            else {
                if (!(obj instanceof Number)) {
                    this.b(obj.toString());
                    return this;
                }
                final Writer a2 = this.a;
                final String numberToString = JSONObject.numberToString((Number)obj);
                writer = a2;
                value = numberToString;
            }
            writer.write(value);
        }
        else {
            this.a.write("null");
        }
        return this;
    }
    
    public m a(final String s) {
        this.f();
        this.b(s);
        return this;
    }
    
    public m b() {
        return this.a(m.a.a, m.a.b, "]");
    }
    
    public m c() {
        return this.a(m.a.c, "{");
    }
    
    public m d() {
        return this.a(m.a.c, m.a.e, "}");
    }
    
    @Override
    public String toString() {
        return "";
    }
    
    enum a
    {
        a, 
        b, 
        c, 
        d, 
        e, 
        f;
        
        private static final a[] g;
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.apm.insight.l;

import java.util.ArrayList;
import androidx.annotation.Nullable;
import org.json.JSONObject;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.Collections;
import java.util.IdentityHashMap;
import com.apm.insight.nativecrash.NativeImpl;
import org.json.JSONArray;
import java.io.OutputStream;
import java.security.MessageDigest;
import java.io.PrintStream;
import java.io.Writer;
import java.io.PrintWriter;
import java.io.StringWriter;
import androidx.annotation.NonNull;
import java.util.Iterator;
import java.io.Closeable;
import java.io.Reader;
import java.io.BufferedReader;
import java.io.FileReader;
import java.util.LinkedList;
import java.io.File;
import android.text.TextUtils;

public final class v
{
    private static final StackTraceElement a;
    
    static {
        a = new StackTraceElement("", "", "", 0);
    }
    
    public static String a(final String pathname) {
        final boolean empty = TextUtils.isEmpty((CharSequence)pathname);
        final Closeable closeable = null;
        if (empty) {
            return null;
        }
        final File file = new File(pathname);
        if (!file.exists()) {
            return null;
        }
        final LinkedList list = new LinkedList();
        final LinkedList list2 = new LinkedList();
        final StringBuilder sb = new StringBuilder();
        int n = 0;
        int n2 = 0;
        int i = 0;
        Closeable closeable2 = null;
        try {
            final BufferedReader bufferedReader = new BufferedReader(new FileReader(file));
            int n3 = 0;
            try {
                while (true) {
                    final String line = bufferedReader.readLine();
                    if (line == null) {
                        break;
                    }
                    if (n3 <= 256) {
                        list.add(line);
                        sb.append(line);
                        sb.append('\n');
                        n = n2;
                    }
                    else {
                        list2.add(line);
                        n = n2;
                        if (list2.size() > 256) {
                            list2.poll();
                            n = n2 + 1;
                        }
                    }
                    ++n3;
                    n2 = n;
                }
            }
            finally {}
        }
        finally {
            i = n;
            closeable2 = closeable;
        }
        k.a(closeable2);
        if (!list2.isEmpty()) {
            if (i != 0) {
                sb.append("\t... skip ");
                sb.append(i);
                sb.append(" lines\n");
            }
            final Iterator iterator = list2.iterator();
            while (iterator.hasNext()) {
                sb.append((String)iterator.next());
                sb.append('\n');
            }
        }
        return sb.toString();
    }
    
    @NonNull
    public static String a(@NonNull final Throwable t) {
        final StringWriter out = new StringWriter();
        final PrintWriter printWriter = new PrintWriter(out);
        try {
            a(t, printWriter);
            final String string = out.toString();
            printWriter.close();
            return string;
        }
        finally {
            printWriter.close();
            return "";
        }
    }
    
    @NonNull
    public static String a(@NonNull final Throwable t, Thread thread, PrintStream printStream, final e.a a) {
        try {
            MessageDigest.getInstance("MD5");
        }
        finally {
            thread = null;
        }
        printStream = (PrintStream)new e(printStream, (MessageDigest)thread, a);
        while (true) {
            try {
                a(t, (PrintWriter)printStream);
                ((PrintWriter)printStream).close();
                if (thread != null) {
                    return a(((MessageDigest)thread).digest());
                }
                return null;
            }
            finally {
                continue;
            }
            break;
        }
    }
    
    private static String a(final byte[] array) {
        if (array != null && array.length > 0) {
            final char[] array3;
            final char[] array2 = array3 = new char[16];
            array3[0] = '0';
            array3[1] = '1';
            array3[2] = '2';
            array3[3] = '3';
            array3[4] = '4';
            array3[5] = '5';
            array3[6] = '6';
            array3[7] = '7';
            array3[8] = '8';
            array3[9] = '9';
            array3[10] = 'a';
            array3[11] = 'b';
            array3[12] = 'c';
            array3[13] = 'd';
            array3[14] = 'e';
            array3[15] = 'f';
            final char[] value = new char[array.length * 2];
            final int length = array.length;
            int i = 0;
            int n = 0;
            while (i < length) {
                final byte b = array[i];
                final int n2 = n + 1;
                value[n] = array2[b >>> 4 & 0xF];
                n = n2 + 1;
                value[n2] = array2[b & 0xF];
                ++i;
            }
            return new String(value);
        }
        return "";
    }
    
    public static String a(final StackTraceElement[] array) {
        final StringBuilder sb = new StringBuilder();
        for (int length = array.length, i = 0; i < length; ++i) {
            a(array[i], sb);
        }
        return sb.toString();
    }
    
    public static StringBuilder a(final StackTraceElement stackTraceElement, final StringBuilder sb) {
        final String className = stackTraceElement.getClassName();
        sb.append("  at ");
        sb.append(className);
        sb.append(".");
        sb.append(stackTraceElement.getMethodName());
        sb.append("(");
        sb.append(stackTraceElement.getFileName());
        sb.append(":");
        sb.append(stackTraceElement.getLineNumber());
        sb.append(")\n");
        return sb;
    }
    
    public static JSONArray a(final StackTraceElement[] array, final String[] array2) {
        a a = new a(-1, -1);
        final JSONArray jsonArray = new JSONArray();
        a a2;
        for (int i = 0; i < array.length; ++i, a = a2) {
            if (a.a == -1) {
                a2 = a;
                if (a(array[i].getClassName(), array2)) {
                    a.a = i;
                    a.b = i;
                    a2 = a;
                }
            }
            else {
                a2 = a;
                if (!a(array[i].getClassName(), array2)) {
                    a.b = i;
                    jsonArray.put((Object)a.a());
                    a2 = new a(-1, -1);
                }
            }
        }
        if (a.a != -1) {
            a.b = array.length;
            jsonArray.put((Object)a.a());
        }
        return jsonArray;
    }
    
    public static JSONArray a(final String[] array, final String[] array2) {
        a a = new a(-1, -1);
        final JSONArray jsonArray = new JSONArray();
        a a2;
        for (int i = 0; i < array.length; ++i, a = a2) {
            if (a.a == -1) {
                a2 = a;
                if (a(array[i], array2)) {
                    a.a = i;
                    a.b = i;
                    a2 = a;
                }
            }
            else {
                a2 = a;
                if (!a(array[i], array2)) {
                    a.b = i;
                    jsonArray.put((Object)a.a());
                    a2 = new a(-1, -1);
                }
            }
        }
        if (a.a != -1) {
            a.b = array.length;
            jsonArray.put((Object)a.a());
        }
        return jsonArray;
    }
    
    private static void a(final StackTraceElement stackTraceElement, final int n) {
        try {
            a("\tat ", n);
            a(stackTraceElement.getClassName(), n);
            a(".", n);
            a(stackTraceElement.getMethodName(), n);
            String s = null;
            Label_0038: {
                if (stackTraceElement.isNativeMethod()) {
                    s = "(Native Method)";
                }
                else {
                    String s2 = null;
                    Label_0092: {
                        int i;
                        if (stackTraceElement.getFileName() != null) {
                            if (stackTraceElement.getLineNumber() < 0) {
                                a("(", n);
                                s2 = stackTraceElement.getFileName();
                                break Label_0092;
                            }
                            a("(", n);
                            a(stackTraceElement.getFileName(), n);
                            a(":", n);
                            i = stackTraceElement.getLineNumber();
                        }
                        else {
                            if (stackTraceElement.getLineNumber() < 0) {
                                s = "(Unknown Source)";
                                break Label_0038;
                            }
                            a("(Unknown Source:", n);
                            i = stackTraceElement.getLineNumber();
                        }
                        s2 = String.valueOf(i);
                    }
                    a(s2, n);
                    s = ")";
                }
            }
            a(s, n);
            a("\n", n);
        }
        finally {}
    }
    
    private static void a(final String s, final int n) {
        NativeImpl.a(n, s);
    }
    
    public static void a(@NonNull final Throwable t, final int n) {
        try {
            c(t, n);
        }
        finally {}
    }
    
    private static void a(final Throwable p0, final int p1, final String p2, final String p3) {
        // 
        // This method could not be decompiled.
        // 
        // Original Bytecode:
        // 
        //     1: invokevirtual   java/lang/Throwable.getStackTrace:()[Ljava/lang/StackTraceElement;
        //     4: astore          7
        //     6: iload_1        
        //     7: aload_3        
        //     8: invokestatic    com/apm/insight/nativecrash/NativeImpl.a:(ILjava/lang/String;)V
        //    11: iload_1        
        //    12: aload_2        
        //    13: invokestatic    com/apm/insight/nativecrash/NativeImpl.a:(ILjava/lang/String;)V
        //    16: aload_0        
        //    17: iload_1        
        //    18: invokestatic    com/apm/insight/l/v.b:(Ljava/lang/Throwable;I)V
        //    21: aload           7
        //    23: arraylength    
        //    24: istore          6
        //    26: iconst_0       
        //    27: istore          5
        //    29: iconst_0       
        //    30: istore          4
        //    32: iload           4
        //    34: iload           6
        //    36: if_icmpge       54
        //    39: aload           7
        //    41: iload           4
        //    43: aaload         
        //    44: iload_1        
        //    45: invokestatic    com/apm/insight/l/v.a:(Ljava/lang/StackTraceElement;I)V
        //    48: iinc            4, 1
        //    51: goto            32
        //    54: aload_0        
        //    55: invokevirtual   java/lang/Throwable.getSuppressed:()[Ljava/lang/Throwable;
        //    58: astore          8
        //    60: aload           8
        //    62: arraylength    
        //    63: istore          6
        //    65: iload           5
        //    67: istore          4
        //    69: iload           4
        //    71: iload           6
        //    73: if_icmpge       126
        //    76: aload           8
        //    78: iload           4
        //    80: aaload         
        //    81: astore_2       
        //    82: new             Ljava/lang/StringBuilder;
        //    85: dup            
        //    86: invokespecial   java/lang/StringBuilder.<init>:()V
        //    89: astore          7
        //    91: aload           7
        //    93: aload_3        
        //    94: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //    97: pop            
        //    98: aload           7
        //   100: ldc_w           "\t"
        //   103: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   106: pop            
        //   107: aload_2        
        //   108: iload_1        
        //   109: ldc_w           "Suppressed: "
        //   112: aload           7
        //   114: invokevirtual   java/lang/StringBuilder.toString:()Ljava/lang/String;
        //   117: invokestatic    com/apm/insight/l/v.a:(Ljava/lang/Throwable;ILjava/lang/String;Ljava/lang/String;)V
        //   120: iinc            4, 1
        //   123: goto            69
        //   126: aload_0        
        //   127: invokevirtual   java/lang/Throwable.getCause:()Ljava/lang/Throwable;
        //   130: astore_0       
        //   131: aload_0        
        //   132: ifnull          144
        //   135: aload_0        
        //   136: iload_1        
        //   137: ldc_w           "Caused by: "
        //   140: aload_3        
        //   141: invokestatic    com/apm/insight/l/v.a:(Ljava/lang/Throwable;ILjava/lang/String;Ljava/lang/String;)V
        //   144: return         
        //   145: astore_2       
        //   146: goto            16
        //    Exceptions:
        //  Try           Handler
        //  Start  End    Start  End    Type
        //  -----  -----  -----  -----  ----
        //  6      16     145    149    Any
        // 
        // The error that occurred was:
        // 
        // java.lang.NullPointerException: Cannot invoke "com.strobel.assembler.metadata.TypeReference.getSimpleType()" because "type" is null
        //     at com.strobel.assembler.ir.StackMappingVisitor.push(StackMappingVisitor.java:290)
        //     at com.strobel.assembler.ir.StackMappingVisitor$InstructionAnalyzer.execute(StackMappingVisitor.java:837)
        //     at com.strobel.assembler.ir.StackMappingVisitor$InstructionAnalyzer.visit(StackMappingVisitor.java:398)
        //     at com.strobel.decompiler.ast.AstBuilder.performStackAnalysis(AstBuilder.java:2086)
        //     at com.strobel.decompiler.ast.AstBuilder.build(AstBuilder.java:108)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:203)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:93)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethodBody(AstBuilder.java:868)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethod(AstBuilder.java:761)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addTypeMembers(AstBuilder.java:638)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeCore(AstBuilder.java:605)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeNoCache(AstBuilder.java:195)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createType(AstBuilder.java:162)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addType(AstBuilder.java:137)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.buildAst(JavaLanguage.java:71)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.decompileType(JavaLanguage.java:59)
        //     at com.strobel.decompiler.DecompilerDriver.decompileType(DecompilerDriver.java:333)
        //     at com.strobel.decompiler.DecompilerDriver.decompileJar(DecompilerDriver.java:254)
        //     at com.strobel.decompiler.DecompilerDriver.main(DecompilerDriver.java:144)
        // 
        throw new IllegalStateException("An error occurred while decompiling this method.");
    }
    
    private static void a(Throwable cause, final PrintWriter printWriter) {
        if (cause != null) {
            if (printWriter != null) {
                final Set<Throwable> setFromMap = Collections.newSetFromMap(new IdentityHashMap<Throwable, Boolean>());
                setFromMap.add(cause);
                printWriter.println(cause);
                final StackTraceElement[] stackTrace = cause.getStackTrace();
                final boolean b = stackTrace.length > 384;
                final int length = stackTrace.length;
                int i = 0;
                int n = 0;
                while (i < length) {
                    final StackTraceElement obj = stackTrace[i];
                    if (b && n > 256) {
                        final StringBuilder sb = new StringBuilder();
                        sb.append("\t... skip ");
                        sb.append(stackTrace.length - n - 128);
                        sb.append(" lines");
                        printWriter.println(sb.toString());
                        break;
                    }
                    final StringBuilder sb2 = new StringBuilder();
                    sb2.append("\tat ");
                    sb2.append(obj);
                    printWriter.println(sb2.toString());
                    ++n;
                    ++i;
                }
                if (b) {
                    for (int j = stackTrace.length - 128; j < stackTrace.length; ++j) {
                        final StringBuilder sb3 = new StringBuilder();
                        sb3.append("\tat ");
                        sb3.append(stackTrace[j]);
                        printWriter.println(sb3.toString());
                    }
                }
                final Throwable[] suppressed = cause.getSuppressed();
                for (int length2 = suppressed.length, k = 0; k < length2; ++k) {
                    a(suppressed[k], printWriter, stackTrace, "Suppressed: ", "\t", setFromMap, 128);
                }
                cause = cause.getCause();
                if (cause != null) {
                    a(cause, printWriter, stackTrace, "Caused by: ", "", setFromMap, 128);
                }
            }
        }
    }
    
    private static void a(Throwable cause, final PrintWriter printWriter, StackTraceElement[] stackTrace, final String str, final String s, final Set<Throwable> set, int n) {
        if (set.contains(cause)) {
            final StringBuilder sb = new StringBuilder();
            sb.append("\t[CIRCULAR REFERENCE:");
            sb.append(cause);
            sb.append("]");
            printWriter.println(sb.toString());
        }
        else {
            set.add(cause);
            stackTrace = cause.getStackTrace();
            final boolean b = stackTrace.length > n;
            final StringBuilder sb2 = new StringBuilder();
            sb2.append(s);
            sb2.append(str);
            sb2.append(cause);
            printWriter.println(sb2.toString());
            final int length = stackTrace.length;
            int i = 0;
            int n2 = 0;
            while (i < length) {
                final StackTraceElement obj = stackTrace[i];
                if (b && n2 > n) {
                    final StringBuilder sb3 = new StringBuilder();
                    sb3.append("\t... skip ");
                    sb3.append(stackTrace.length - n2 - n / 2);
                    sb3.append(" lines");
                    printWriter.println(sb3.toString());
                    break;
                }
                final StringBuilder sb4 = new StringBuilder();
                sb4.append("\tat ");
                sb4.append(obj);
                printWriter.println(sb4.toString());
                ++n2;
                ++i;
            }
            if (b) {
                for (int j = stackTrace.length - n / 2; j < stackTrace.length; ++j) {
                    final StringBuilder sb5 = new StringBuilder();
                    sb5.append("\tat ");
                    sb5.append(stackTrace[j]);
                    printWriter.println(sb5.toString());
                }
            }
            for (final Throwable t : cause.getSuppressed()) {
                final StringBuilder sb6 = new StringBuilder();
                sb6.append(s);
                sb6.append("\t");
                final String string = sb6.toString();
                int n3 = n / 2;
                if (n3 <= 10) {
                    n3 = 10;
                }
                a(t, printWriter, stackTrace, "Suppressed: ", string, set, n3);
            }
            cause = cause.getCause();
            if (cause != null) {
                n /= 2;
                if (n <= 10) {
                    n = 10;
                }
                a(cause, printWriter, stackTrace, "Caused by: ", s, set, n);
            }
        }
    }
    
    private static void a(Throwable cause, final List<StackTraceElement> list) {
        if (cause != null) {
            if (list != null) {
                final Set<Throwable> setFromMap = Collections.newSetFromMap(new IdentityHashMap<Throwable, Boolean>());
                setFromMap.add(cause);
                list.add(v.a);
                final StackTraceElement[] stackTrace = cause.getStackTrace();
                final boolean b = stackTrace.length > 384;
                final int length = stackTrace.length;
                int i = 0;
                int n = 0;
                while (i < length) {
                    final StackTraceElement stackTraceElement = stackTrace[i];
                    if (b && n > 256) {
                        list.add(v.a);
                        break;
                    }
                    list.add(stackTraceElement);
                    ++n;
                    ++i;
                }
                if (b) {
                    for (int j = stackTrace.length - 128; j < stackTrace.length; ++j) {
                        list.add(stackTrace[j]);
                    }
                }
                final Throwable[] suppressed = cause.getSuppressed();
                for (int length2 = suppressed.length, k = 0; k < length2; ++k) {
                    a(suppressed[k], list, stackTrace, "Suppressed: ", "\t", setFromMap, 128);
                }
                cause = cause.getCause();
                if (cause != null) {
                    a(cause, list, stackTrace, "Caused by: ", "", setFromMap, 128);
                }
            }
        }
    }
    
    private static void a(Throwable cause, final List<StackTraceElement> list, StackTraceElement[] stackTrace, final String s, final String str, final Set<Throwable> set, int n) {
        if (set.contains(cause)) {
            list.add(v.a);
        }
        else {
            set.add(cause);
            stackTrace = cause.getStackTrace();
            final boolean b = stackTrace.length > n;
            list.add(v.a);
            final int length = stackTrace.length;
            int i = 0;
            int n2 = 0;
            while (i < length) {
                final StackTraceElement stackTraceElement = stackTrace[i];
                if (b && n2 > n) {
                    list.add(v.a);
                    break;
                }
                list.add(stackTraceElement);
                ++n2;
                ++i;
            }
            if (b) {
                for (int j = stackTrace.length - n / 2; j < stackTrace.length; ++j) {
                    list.add(stackTrace[j]);
                }
            }
            for (final Throwable t : cause.getSuppressed()) {
                final StringBuilder sb = new StringBuilder();
                sb.append(str);
                sb.append("\t");
                final String string = sb.toString();
                int n3 = n / 2;
                if (n3 <= 10) {
                    n3 = 10;
                }
                a(t, list, stackTrace, "Suppressed: ", string, set, n3);
            }
            cause = cause.getCause();
            if (cause != null) {
                n /= 2;
                if (n <= 10) {
                    n = 10;
                }
                a(cause, list, stackTrace, "Caused by: ", str, set, n);
            }
        }
    }
    
    public static boolean a(final String s, final String[] array) {
        if (array != null) {
            if (!TextUtils.isEmpty((CharSequence)s)) {
                for (int length = array.length, i = 0; i < length; ++i) {
                    if (s.contains(array[i])) {
                        return true;
                    }
                }
            }
        }
        return false;
    }
    
    @Nullable
    public static JSONObject b(final String s) {
        try {
            final Map<Thread, StackTraceElement[]> allStackTraces = Thread.getAllStackTraces();
            final JSONObject jsonObject = new JSONObject();
            if (allStackTraces == null) {}
            jsonObject.put("thread_all_count", allStackTraces.size());
            final JSONArray jsonArray = new JSONArray();
            for (final Map.Entry<Thread, V> entry : allStackTraces.entrySet()) {
                final JSONObject jsonObject2 = new JSONObject();
                final Thread thread = entry.getKey();
                final String name = thread.getName();
                if (!c(name)) {
                    if (s != null) {
                        if (s.equals(name) || name.startsWith(s)) {
                            continue;
                        }
                        if (name.endsWith(s)) {
                            continue;
                        }
                    }
                    final StringBuilder sb = new StringBuilder();
                    sb.append(thread.getName());
                    sb.append("(");
                    sb.append(thread.getId());
                    sb.append(")");
                    jsonObject2.put("thread_name", (Object)sb.toString());
                    final StackTraceElement[] array = (Object)entry.getValue();
                    boolean b = false;
                    Label_0380: {
                        if (array != null) {
                            final JSONArray jsonArray2 = new JSONArray();
                            for (final StackTraceElement stackTraceElement : array) {
                                final String className = stackTraceElement.getClassName();
                                final StringBuilder sb2 = new StringBuilder();
                                sb2.append(className);
                                sb2.append(".");
                                sb2.append(stackTraceElement.getMethodName());
                                sb2.append("(");
                                sb2.append(stackTraceElement.getLineNumber());
                                sb2.append(")");
                                jsonArray2.put((Object)sb2.toString());
                            }
                            jsonObject2.put("thread_stack", (Object)jsonArray2);
                            if (jsonArray2.length() <= 0) {
                                b = false;
                                break Label_0380;
                            }
                        }
                        b = true;
                    }
                    if (!b) {
                        continue;
                    }
                    jsonArray.put((Object)jsonObject2);
                }
            }
            jsonObject.put("thread_stacks", (Object)jsonArray);
            return jsonObject;
        }
        finally {
            return null;
        }
    }
    
    private static void b(final Throwable t, final int n) {
        t.getClass();
        final String localizedMessage = t.getLocalizedMessage();
        try {
            a(t.getClass().getName(), n);
            if (localizedMessage != null) {
                a(": ", n);
                a(localizedMessage, n);
            }
            a("\n", n);
        }
        finally {}
    }
    
    public static StackTraceElement[] b(@NonNull final Throwable t) {
        final ArrayList list = new ArrayList();
        try {
            a(t, list);
            return (StackTraceElement[])list.toArray(new StackTraceElement[list.size()]);
        }
        finally {
            return (StackTraceElement[])list.toArray(new StackTraceElement[list.size()]);
        }
    }
    
    private static void c(Throwable cause, final int n) {
        if (cause != null) {
            if (n > 0) {
                b(cause, n);
                final StackTraceElement[] stackTrace = cause.getStackTrace();
                final int length = stackTrace.length;
                final int n2 = 0;
                for (int i = 0; i < length; ++i) {
                    a(stackTrace[i], n);
                }
                final Throwable[] suppressed = cause.getSuppressed();
                for (int length2 = suppressed.length, j = n2; j < length2; ++j) {
                    a(suppressed[j], n, "Suppressed: ", "\t");
                }
                cause = cause.getCause();
                if (cause != null) {
                    a(cause, n, "Caused by: ", "");
                }
            }
        }
    }
    
    private static boolean c(final String s) {
        final Set<String> a = j.a();
        if (!a.contains(s)) {
            for (final String prefix : a) {
                if (!TextUtils.isEmpty((CharSequence)s) && s.startsWith(prefix)) {
                    return true;
                }
            }
            return false;
        }
        return true;
    }
    
    public static boolean c(Throwable cause) {
        if (cause == null) {
            return false;
        }
        int n = 0;
        while (true) {
            if (cause == null) {
                return false;
            }
            try {
                if (cause instanceof OutOfMemoryError) {
                    return true;
                }
                if (n > 20) {
                    return false;
                }
                ++n;
                cause = cause.getCause();
                continue;
            }
            finally {
                return false;
            }
        }
    }
    
    public static boolean d(Throwable cause) {
        if (cause == null) {
            return false;
        }
        int n = 0;
        while (cause != null) {
            try {
                if (cause instanceof OutOfMemoryError && (cause.getMessage().contains("allocate") || cause.getMessage().contains("thrown"))) {}
                if (n > 20) {
                    return false;
                }
                ++n;
                cause = cause.getCause();
                continue;
            }
            finally {
                return true;
            }
            break;
        }
        return false;
    }
    
    public static class a
    {
        public int a;
        public int b;
        
        public a(final int a, final int b) {
            this.a = a;
            this.b = b;
        }
        
        public JSONObject a() {
            final JSONObject jsonObject = new JSONObject();
            try {
                jsonObject.put("start", this.a);
                jsonObject.put("end", this.b);
                return jsonObject;
            }
            finally {
                return jsonObject;
            }
        }
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.apm.insight.l;

import android.text.TextUtils;
import androidx.annotation.NonNull;
import android.content.Context;
import com.apm.insight.i;
import java.io.File;

public class o
{
    private static String a;
    private static File b;
    private static File c;
    private static File d;
    
    public static File a() {
        File file;
        if ((file = o.c) == null) {
            file = d(i.g());
        }
        return file;
    }
    
    public static File a(@NonNull final Context context) {
        return new File(j(context), "apminsight/CrashLogJava");
    }
    
    public static File a(@NonNull final Context context, final String str) {
        final StringBuilder sb = new StringBuilder();
        sb.append(j(context));
        sb.append('/');
        sb.append("apminsight/CrashCommonLog");
        sb.append('/');
        sb.append(str);
        return new File(sb.toString());
    }
    
    public static File a(final File parent) {
        return new File(parent, "flog.txt");
    }
    
    public static File a(final File parent, final String str) {
        final StringBuilder sb = new StringBuilder();
        sb.append(parent.getName());
        sb.append(str);
        return new File(parent, sb.toString());
    }
    
    public static String a(final String str) {
        final StringBuilder sb = new StringBuilder();
        sb.append("dart_");
        sb.append(str);
        return sb.toString();
    }
    
    public static File b(@NonNull final Context context) {
        return new File(j(context), "apminsight/CrashLogSimple");
    }
    
    public static File b(final File parent) {
        return new File(parent, "tombstone.txt");
    }
    
    public static File b(final String s) {
        return new File(a(i.g(), s), "fds.txt");
    }
    
    public static String b() {
        final StringBuilder sb = new StringBuilder();
        sb.append("anr_");
        sb.append(i.e());
        return sb.toString();
    }
    
    public static File c(@NonNull final Context context) {
        return new File(j(context), "apminsight/RuntimeContext");
    }
    
    public static File c(final File parent) {
        return new File(parent, "header.bin");
    }
    
    public static File c(final String s) {
        return new File(a(i.g(), s), "threads.txt");
    }
    
    public static String c() {
        return String.format("ensure_%s", i.e());
    }
    
    public static File d(@NonNull final Context context) {
        if (o.c == null) {
            Context g;
            if ((g = context) == null) {
                g = i.g();
            }
            o.c = new File(j(g), "apminsight/CrashLogNative");
        }
        return o.c;
    }
    
    public static File d(final File file) {
        return new File(a(i.g(), file.getName()), "maps.txt");
    }
    
    public static File d(final String s) {
        return new File(a(i.g(), s), "meminfo.txt");
    }
    
    public static File e(@NonNull final Context context) {
        if (o.d == null) {
            final StringBuilder sb = new StringBuilder();
            sb.append(j(context));
            sb.append('/');
            sb.append("apminsight/CrashCommonLog");
            sb.append('/');
            sb.append(i.f());
            o.d = new File(sb.toString());
        }
        return o.d;
    }
    
    public static File e(final File parent) {
        return new File(parent, "callback.json");
    }
    
    public static File e(final String s) {
        return new File(a(i.g(), s), "pthreads.txt");
    }
    
    public static File f(final Context context) {
        return new File(j(context), "apminsight/CrashCommonLog");
    }
    
    public static File f(final File parent) {
        return new File(parent, "upload.json");
    }
    
    public static File f(final String s) {
        return new File(a(i.g(), s), "rountines.txt");
    }
    
    public static File g(final Context context) {
        return new File(j(context), "apminsight/issueCrashTimes");
    }
    
    public static File g(final File parent) {
        return new File(parent, "javastack.txt");
    }
    
    public static File g(final String s) {
        return new File(a(i.g(), s), "leakd_threads.txt");
    }
    
    public static File h(final Context context) {
        final StringBuilder sb = new StringBuilder();
        sb.append(j(context));
        sb.append('/');
        sb.append("apminsight/issueCrashTimes");
        sb.append('/');
        sb.append("current.times");
        return new File(sb.toString());
    }
    
    public static File h(final File file) {
        return new File(a(i.g(), file.getName()), "logcat.txt");
    }
    
    public static File i(@NonNull final Context context) {
        return new File(j(context), "apminsight/alogCrash");
    }
    
    public static File i(final File file) {
        return new File(a(i.g(), file.getName()), "fds.txt");
    }
    
    public static File j(final File file) {
        return new File(a(i.g(), file.getName()), "threads.txt");
    }
    
    public static String j(@NonNull final Context context) {
        if (TextUtils.isEmpty((CharSequence)o.a)) {
            try {
                o.a = context.getFilesDir().getAbsolutePath();
            }
            catch (final Exception ex) {
                o.a = "/sdcard/";
                ex.printStackTrace();
            }
        }
        return o.a;
    }
    
    public static File k(final File file) {
        return new File(a(i.g(), file.getName()), "meminfo.txt");
    }
    
    public static File l(final File file) {
        return new File(a(i.g(), file.getName()), "pthreads.txt");
    }
    
    public static File m(final File file) {
        return new File(a(i.g(), file.getName()), "rountines.txt");
    }
    
    public static File n(final File file) {
        return new File(a(i.g(), file.getName()), "leakd_threads.txt");
    }
    
    public static File o(final File parent) {
        return new File(parent, "abortmsg.txt");
    }
}

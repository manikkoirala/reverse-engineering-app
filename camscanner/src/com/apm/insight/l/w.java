// 
// Decompiled by Procyon v0.6.0
// 

package com.apm.insight.l;

import com.apm.insight.i;
import android.os.Environment;
import org.json.JSONObject;
import android.content.Context;
import android.os.StatFs;
import java.io.File;

public class w
{
    public static long a(final File file) {
        try {
            return new StatFs(file.getPath()).getTotalBytes();
        }
        finally {
            return 0L;
        }
    }
    
    public static JSONObject a(Context context) {
        context = (Context)new JSONObject();
        try {
            ((JSONObject)context).put("inner_free", b());
            ((JSONObject)context).put("inner_total", c());
            ((JSONObject)context).put("sdcard_free", f());
            ((JSONObject)context).put("sdcard_total", g());
            ((JSONObject)context).put("inner_free_real", d());
            ((JSONObject)context).put("inner_total_real", e());
            return (JSONObject)context;
        }
        finally {
            return (JSONObject)context;
        }
    }
    
    public static boolean a() {
        return "mounted".equals(Environment.getExternalStorageState());
    }
    
    private static long b() {
        try {
            return b(Environment.getRootDirectory());
        }
        finally {
            return 0L;
        }
    }
    
    public static long b(final File file) {
        try {
            return new StatFs(file.getPath()).getFreeBytes();
        }
        finally {
            return 0L;
        }
    }
    
    private static long c() {
        try {
            return a(Environment.getRootDirectory());
        }
        finally {
            return 0L;
        }
    }
    
    private static long d() {
        try {
            return b(i.g().getFilesDir());
        }
        finally {
            return 0L;
        }
    }
    
    private static long e() {
        try {
            return a(i.g().getFilesDir());
        }
        finally {
            return 0L;
        }
    }
    
    private static long f() {
        try {
            if (a()) {
                return Environment.getExternalStorageDirectory().getFreeSpace();
            }
            return 0L;
        }
        finally {
            return 0L;
        }
    }
    
    private static long g() {
        try {
            if (a()) {
                return Environment.getExternalStorageDirectory().getTotalSpace();
            }
            return 0L;
        }
        finally {
            return 0L;
        }
    }
}

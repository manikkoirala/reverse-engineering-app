// 
// Decompiled by Procyon v0.6.0
// 

package com.apm.insight.l;

import java.util.HashMap;
import java.util.Iterator;
import org.json.JSONArray;
import androidx.annotation.Nullable;
import org.json.JSONObject;

public final class l
{
    public static int a(@Nullable JSONObject c, int optInt, final String... array) {
        c = c(c, array);
        if (c == null) {
            return optInt;
        }
        optInt = c.optInt(array[array.length - 1], optInt);
        final StringBuilder sb = new StringBuilder();
        sb.append("normal get jsonInt: ");
        sb.append(array[array.length - 1]);
        sb.append(" : ");
        sb.append(optInt);
        q.a("JSONUtil", sb.toString());
        return optInt;
    }
    
    public static JSONArray a(final int n, int i, final JSONArray jsonArray) {
        final int length = jsonArray.length();
        final int n2 = i + n;
        if (length <= n2) {
            return jsonArray;
        }
        final JSONArray jsonArray2 = new JSONArray();
        int n3 = 0;
        while (true) {
            i = n;
            if (n3 >= n) {
                break;
            }
            jsonArray2.put(jsonArray.opt(n3));
            ++n3;
        }
        while (i < n2) {
            jsonArray2.put(jsonArray.opt(jsonArray.length() - (n2 - i)));
            ++i;
        }
        return jsonArray2;
    }
    
    @Nullable
    public static JSONArray a(@Nullable JSONObject c, final String... array) {
        c = c(c, array);
        if (c == null) {
            return null;
        }
        final JSONArray optJSONArray = c.optJSONArray(array[array.length - 1]);
        final StringBuilder sb = new StringBuilder();
        sb.append("normal get configArray: ");
        sb.append(array[array.length - 1]);
        sb.append(" : ");
        sb.append(optJSONArray);
        q.a("ApmConfig", sb.toString());
        return optJSONArray;
    }
    
    @Nullable
    public static JSONArray a(@Nullable final String[] array) {
        if (array == null) {
            return null;
        }
        final JSONArray jsonArray = new JSONArray();
        for (int length = array.length, i = 0; i < length; ++i) {
            jsonArray.put((Object)array[i]);
        }
        return jsonArray;
    }
    
    public static void a(final JSONObject jsonObject, final JSONObject jsonObject2) {
        final Iterator keys = jsonObject2.keys();
        try {
            while (keys.hasNext()) {
                final String s = keys.next();
                if (!jsonObject.has(s)) {
                    jsonObject.put(s, jsonObject2.opt(s));
                }
            }
        }
        finally {}
    }
    
    public static boolean a(final JSONArray jsonArray) {
        return jsonArray == null || jsonArray.length() == 0;
    }
    
    public static boolean a(final JSONObject jsonObject) {
        return jsonObject == null || jsonObject.length() == 0;
    }
    
    public static boolean a(final JSONObject jsonObject, final String s) {
        return a(jsonObject) || a(jsonObject.optJSONArray(s));
    }
    
    @Nullable
    public static String b(@Nullable JSONObject c, final String... array) {
        c = c(c, array);
        if (c == null) {
            return null;
        }
        final String optString = c.optString(array[array.length - 1]);
        final StringBuilder sb = new StringBuilder();
        sb.append("normal get configArray: ");
        sb.append(array[array.length - 1]);
        sb.append(" : ");
        sb.append(optString);
        q.a("ApmConfig", sb.toString());
        return optString;
    }
    
    public static HashMap<String, Object> b(final JSONObject jsonObject) {
        final HashMap hashMap = new HashMap();
        if (jsonObject == null) {
            return hashMap;
        }
        final Iterator keys = jsonObject.keys();
        while (keys.hasNext()) {
            final String key = keys.next();
            hashMap.put(key, jsonObject.opt(key));
        }
        return hashMap;
    }
    
    @Nullable
    public static JSONObject c(@Nullable JSONObject optJSONObject, final String... array) {
        if (optJSONObject == null) {
            q.a("JSONUtil", "err get JsonFromParent: null json", new RuntimeException());
            return null;
        }
        for (int i = 0; i < array.length - 1; ++i) {
            optJSONObject = optJSONObject.optJSONObject(array[i]);
            if (optJSONObject == null) {
                final StringBuilder sb = new StringBuilder();
                sb.append("err get json: not found node:");
                sb.append(array[i]);
                q.a("JSONUtil", sb.toString());
                return null;
            }
        }
        return optJSONObject;
    }
}

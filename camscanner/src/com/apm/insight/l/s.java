// 
// Decompiled by Procyon v0.6.0
// 

package com.apm.insight.l;

import org.json.JSONException;
import android.text.TextUtils;
import com.apm.insight.i;
import org.json.JSONObject;
import com.apm.insight.CrashType;
import com.apm.insight.entity.Header;
import com.apm.insight.entity.a;

public final class s
{
    public static void a(final a a, final Header header, final CrashType crashType) {
        if (a != null) {
            a(a.h(), header, crashType);
        }
    }
    
    public static void a(JSONObject f, final Header header, final CrashType obj) {
        if (f != null) {
            if (obj != null) {
                final long optLong = f.optLong("crash_time");
                final String a = i.c().a();
                if (optLong > 0L && !TextUtils.isEmpty((CharSequence)obj.getName())) {
                    try {
                        final StringBuilder sb = new StringBuilder();
                        sb.append("android__");
                        sb.append(a);
                        sb.append("_");
                        sb.append(optLong);
                        sb.append("_");
                        sb.append(obj);
                        final String string = sb.toString();
                        if (header != null) {
                            f = header.f();
                            if (f == null) {
                                return;
                            }
                        }
                        f.put("unique_key", (Object)string);
                    }
                    catch (final JSONException ex) {
                        ((Throwable)ex).printStackTrace();
                    }
                }
            }
        }
    }
}

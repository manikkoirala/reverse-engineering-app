// 
// Decompiled by Procyon v0.6.0
// 

package com.apm.insight.l;

import javax.net.ssl.SSLException;
import java.net.ProtocolException;
import java.net.UnknownHostException;
import java.net.SocketException;
import java.net.PortUnreachableException;
import java.net.NoRouteToHostException;
import java.net.ConnectException;
import java.net.BindException;
import java.net.SocketTimeoutException;
import org.apache.http.conn.ConnectTimeoutException;
import java.util.HashSet;
import java.util.Set;

public final class j
{
    private static final Set<String> a;
    
    static {
        final HashSet a2 = new HashSet();
        (a = a2).add("HeapTaskDaemon");
        a2.add("ThreadPlus");
        a2.add("ApiDispatcher");
        a2.add("ApiLocalDispatcher");
        a2.add("AsyncLoader");
        a2.add("AsyncTask");
        a2.add("Binder");
        a2.add("PackageProcessor");
        a2.add("SettingsObserver");
        a2.add("WifiManager");
        a2.add("JavaBridge");
        a2.add("Compiler");
        a2.add("Signal Catcher");
        a2.add("GC");
        a2.add("ReferenceQueueDaemon");
        a2.add("FinalizerDaemon");
        a2.add("FinalizerWatchdogDaemon");
        a2.add("CookieSyncManager");
        a2.add("RefQueueWorker");
        a2.add("CleanupReference");
        a2.add("VideoManager");
        a2.add("DBHelper-AsyncOp");
        a2.add("InstalledAppTracker2");
        a2.add("AppData-AsyncOp");
        a2.add("IdleConnectionMonitor");
        a2.add("LogReaper");
        a2.add("ActionReaper");
        a2.add("Okio Watchdog");
        a2.add("CheckWaitingQueue");
        a2.add("NPTH-CrashTimer");
        a2.add("NPTH-JavaCallback");
        a2.add("NPTH-LocalParser");
        a2.add("ANR_FILE_MODIFY");
    }
    
    public static Set<String> a() {
        return j.a;
    }
    
    public static boolean a(final Throwable t) {
        if (t == null) {
            return true;
        }
        try {
            if (t instanceof ConnectTimeoutException) {
                return true;
            }
            if (t instanceof SocketTimeoutException) {
                return true;
            }
            if (t instanceof BindException) {
                return true;
            }
            if (t instanceof ConnectException) {
                return true;
            }
            if (t instanceof NoRouteToHostException) {
                return true;
            }
            if (t instanceof PortUnreachableException) {
                return true;
            }
            if (t instanceof SocketException) {
                return true;
            }
            if (t instanceof UnknownHostException) {
                return true;
            }
            if (t instanceof ProtocolException) {
                return true;
            }
            if (t instanceof SSLException) {
                return true;
            }
        }
        finally {
            t.printStackTrace();
        }
        return false;
    }
}

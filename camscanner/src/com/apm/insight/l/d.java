// 
// Decompiled by Procyon v0.6.0
// 

package com.apm.insight.l;

import java.io.FilenameFilter;
import java.io.File;
import java.util.regex.Matcher;
import com.apm.insight.k.e;
import android.os.Build;
import java.io.Closeable;
import java.io.Reader;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.Locale;
import android.text.TextUtils;
import java.util.regex.Pattern;

public class d
{
    private static boolean a = false;
    private static int b = -1;
    private static final Pattern c;
    
    static {
        c = Pattern.compile("^0-([\\d]+)$");
    }
    
    public static String a() {
        return b("ro.build.version.emui");
    }
    
    public static boolean a(final String s) {
        String a = s;
        if (TextUtils.isEmpty((CharSequence)s)) {
            a = a();
        }
        return (!TextUtils.isEmpty((CharSequence)a) && a.toLowerCase(Locale.getDefault()).startsWith("emotionui")) || b();
    }
    
    private static String b(String str) {
        String s = null;
        try {
            final Runtime runtime = Runtime.getRuntime();
            final StringBuilder sb = new StringBuilder();
            sb.append("getprop ");
            sb.append(str);
            final Object o;
            str = (String)(o = new BufferedReader(new InputStreamReader(runtime.exec(sb.toString()).getInputStream()), 1024));
            final String s2 = ((BufferedReader)o).readLine();
            try {
                final String s3 = str;
                ((BufferedReader)s3).close();
                final String s4 = str;
                k.a((Closeable)s4);
                return s2;
            }
            finally {
                final Object o2;
                s = (String)o2;
                s = s2;
            }
        }
        finally {
            final String s6;
            str = s6;
            str = null;
        }
        while (true) {
            try {
                final Object o = str;
                final String s2 = ((BufferedReader)o).readLine();
                final String s3 = str;
                ((BufferedReader)s3).close();
                final String s4 = str;
                k.a((Closeable)s4);
                return s2;
                k.a((Closeable)str);
                return s;
            }
            finally {
                continue;
            }
            break;
        }
    }
    
    public static boolean b() {
        final boolean b = false;
        try {
            final String brand = Build.BRAND;
            if (TextUtils.isEmpty((CharSequence)brand) || !brand.toLowerCase(Locale.getDefault()).startsWith("huawei")) {
                final String manufacturer = Build.MANUFACTURER;
                boolean b2 = b;
                if (TextUtils.isEmpty((CharSequence)manufacturer)) {
                    return b2;
                }
                final boolean startsWith = manufacturer.toLowerCase(Locale.getDefault()).startsWith("huawei");
                b2 = b;
                if (!startsWith) {
                    return b2;
                }
            }
            return true;
        }
        finally {
            return b;
        }
    }
    
    private static int c(final String p0) {
        // 
        // This method could not be decompiled.
        // 
        // Original Bytecode:
        // 
        //     3: astore_2       
        //     4: aload_2        
        //     5: aload_0        
        //     6: invokespecial   java/io/FileInputStream.<init>:(Ljava/lang/String;)V
        //     9: new             Ljava/io/BufferedReader;
        //    12: astore_0       
        //    13: new             Ljava/io/InputStreamReader;
        //    16: astore_3       
        //    17: aload_3        
        //    18: aload_2        
        //    19: invokespecial   java/io/InputStreamReader.<init>:(Ljava/io/InputStream;)V
        //    22: aload_0        
        //    23: aload_3        
        //    24: invokespecial   java/io/BufferedReader.<init>:(Ljava/io/Reader;)V
        //    27: aload_0        
        //    28: invokevirtual   java/io/BufferedReader.readLine:()Ljava/lang/String;
        //    31: astore_3       
        //    32: aload_0        
        //    33: astore_2       
        //    34: aload_3        
        //    35: ifnull          62
        //    38: aload_3        
        //    39: invokestatic    com/apm/insight/l/d.d:(Ljava/lang/String;)I
        //    42: istore_1       
        //    43: aload_0        
        //    44: invokevirtual   java/io/BufferedReader.close:()V
        //    47: iload_1        
        //    48: ireturn        
        //    49: astore_2       
        //    50: goto            56
        //    53: astore_0       
        //    54: aconst_null    
        //    55: astore_0       
        //    56: aload_0        
        //    57: ifnull          66
        //    60: aload_0        
        //    61: astore_2       
        //    62: aload_2        
        //    63: invokevirtual   java/io/BufferedReader.close:()V
        //    66: iconst_m1      
        //    67: ireturn        
        //    68: astore_0       
        //    69: goto            47
        //    72: astore_0       
        //    73: goto            66
        //    Exceptions:
        //  Try           Handler
        //  Start  End    Start  End    Type                 
        //  -----  -----  -----  -----  ---------------------
        //  0      27     53     56     Any
        //  27     32     49     53     Any
        //  38     43     49     53     Any
        //  43     47     68     72     Ljava/io/IOException;
        //  62     66     72     76     Ljava/io/IOException;
        // 
        // The error that occurred was:
        // 
        // java.lang.IllegalStateException: Expression is linked from several locations: Label_0047:
        //     at com.strobel.decompiler.ast.Error.expressionLinkedFromMultipleLocations(Error.java:27)
        //     at com.strobel.decompiler.ast.AstOptimizer.mergeDisparateObjectInitializations(AstOptimizer.java:2604)
        //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:235)
        //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:42)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:206)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:93)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethodBody(AstBuilder.java:868)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethod(AstBuilder.java:761)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addTypeMembers(AstBuilder.java:638)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeCore(AstBuilder.java:605)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeNoCache(AstBuilder.java:195)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createType(AstBuilder.java:162)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addType(AstBuilder.java:137)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.buildAst(JavaLanguage.java:71)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.decompileType(JavaLanguage.java:59)
        //     at com.strobel.decompiler.DecompilerDriver.decompileType(DecompilerDriver.java:333)
        //     at com.strobel.decompiler.DecompilerDriver.decompileJar(DecompilerDriver.java:254)
        //     at com.strobel.decompiler.DecompilerDriver.main(DecompilerDriver.java:144)
        // 
        throw new IllegalStateException("An error occurred while decompiling this method.");
    }
    
    public static boolean c() {
        if (!d.a) {
            try {
                Class.forName("miui.os.Build");
                e.a = true;
                return d.a = true;
            }
            catch (final Exception ex) {
                d.a = true;
            }
        }
        return e.a;
    }
    
    private static int d(String group) {
        final Matcher matcher = d.c.matcher(group);
        Label_0032: {
            if (!matcher.matches()) {
                break Label_0032;
            }
            group = matcher.group(1);
            try {
                int int1 = Integer.parseInt(group);
                ++int1;
                return int1;
                int1 = -1;
                return int1;
            }
            catch (final NumberFormatException ex) {
                return -1;
            }
        }
    }
    
    public static boolean d() {
        return Build.DISPLAY.contains("Flyme") || Build.USER.equals("flyme");
    }
    
    public static int e() {
        final int b = d.b;
        if (b > 0) {
            return b;
        }
        int n;
        if ((n = c("/sys/devices/system/cpu/possible")) <= 0) {
            n = c("/sys/devices/system/cpu/present");
        }
        int e;
        if ((e = n) <= 0) {
            e = e("/sys/devices/system/cpu/");
        }
        int availableProcessors;
        if ((availableProcessors = e) <= 0) {
            availableProcessors = Runtime.getRuntime().availableProcessors();
        }
        int b2;
        if ((b2 = availableProcessors) <= 0) {
            b2 = 1;
        }
        return d.b = b2;
    }
    
    private static int e(final String pathname) {
        try {
            final File[] listFiles = new File(pathname).listFiles(new FilenameFilter() {
                private final Pattern a = Pattern.compile("^cpu[\\d]+$");
                
                @Override
                public boolean accept(final File file, final String input) {
                    return this.a.matcher(input).matches();
                }
            });
            if (listFiles != null && listFiles.length > 0) {
                return listFiles.length;
            }
            return -1;
        }
        finally {
            return -1;
        }
    }
}

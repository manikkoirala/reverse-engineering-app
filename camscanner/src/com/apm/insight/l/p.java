// 
// Decompiled by Procyon v0.6.0
// 

package com.apm.insight.l;

import android.telephony.TelephonyManager;
import android.net.NetworkInfo;
import android.net.ConnectivityManager;
import com.apm.insight.k.e;
import android.content.Context;

public final class p
{
    public static String a(final Context context) {
        return a(c(context));
    }
    
    public static String a(final e.b b) {
        while (true) {
            try {
                String s = null;
                switch (p$1.a[b.ordinal()]) {
                    default: {
                        s = "";
                        break;
                    }
                    case 6: {
                        s = "5g";
                        break;
                    }
                    case 5: {
                        s = "mobile";
                        break;
                    }
                    case 4: {
                        s = "4g";
                        break;
                    }
                    case 3: {
                        s = "3g";
                        break;
                    }
                    case 2: {
                        s = "2g";
                        break;
                    }
                    case 1: {
                        s = "wifi";
                        break;
                    }
                }
                return s;
            }
            catch (final Exception ex) {
                continue;
            }
            break;
        }
    }
    
    public static boolean b(final Context context) {
        final boolean b = false;
        try {
            final ConnectivityManager connectivityManager = (ConnectivityManager)context.getSystemService("connectivity");
            if (connectivityManager == null) {
                return false;
            }
            final NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
            boolean b2 = b;
            if (activeNetworkInfo != null) {
                final boolean available = activeNetworkInfo.isAvailable();
                b2 = b;
                if (available) {
                    b2 = true;
                }
            }
            return b2;
        }
        catch (final Exception ex) {
            return b;
        }
    }
    
    private static e.b c(final Context context) {
        try {
            final ConnectivityManager connectivityManager = (ConnectivityManager)context.getSystemService("connectivity");
            if (connectivityManager == null) {
                return e.b.a;
            }
            final NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
            if (activeNetworkInfo != null) {
                if (activeNetworkInfo.isAvailable()) {
                    final int type = activeNetworkInfo.getType();
                    if (type != 0) {
                        return e.b.e;
                    }
                    if (type == 0) {
                        final int networkType = ((TelephonyManager)context.getSystemService("phone")).getNetworkType();
                        Label_0160: {
                            if (networkType != 3) {
                                if (networkType == 20) {
                                    return e.b.g;
                                }
                                if (networkType != 5 && networkType != 6) {
                                    switch (networkType) {
                                        default: {
                                            switch (networkType) {
                                                default: {
                                                    return e.b.f;
                                                }
                                                case 13: {
                                                    return e.b.f;
                                                }
                                                case 12:
                                                case 14:
                                                case 15: {
                                                    break Label_0160;
                                                }
                                            }
                                            break;
                                        }
                                        case 8:
                                        case 9:
                                        case 10: {
                                            break;
                                        }
                                    }
                                }
                            }
                        }
                        return e.b.d;
                    }
                }
            }
            return e.b.a;
        }
        finally {
            return e.b.b;
        }
    }
}

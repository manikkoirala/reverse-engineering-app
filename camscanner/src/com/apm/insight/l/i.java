// 
// Decompiled by Procyon v0.6.0
// 

package com.apm.insight.l;

import java.util.Iterator;
import com.apm.insight.nativecrash.NativeImpl;
import java.util.List;
import com.apm.insight.entity.Header;
import com.apm.insight.g.d;
import com.apm.insight.c;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;
import java.io.Writer;
import java.io.BufferedWriter;
import java.io.FileWriter;
import androidx.annotation.Nullable;
import java.io.OutputStream;
import java.util.Properties;
import java.util.Map;
import java.io.FileOutputStream;
import java.io.FileInputStream;
import org.json.JSONArray;
import androidx.annotation.NonNull;
import org.json.JSONException;
import java.io.Closeable;
import java.io.Reader;
import java.io.BufferedReader;
import java.io.FileReader;
import org.json.JSONObject;
import com.apm.insight.entity.e;
import com.apm.insight.CrashType;
import java.util.ArrayList;
import android.text.TextUtils;
import java.io.IOException;
import com.apm.insight.entity.a;
import java.io.File;

public class i
{
    public static a a(final File file, final boolean b) {
        final a a = new a();
        final boolean exists = file.exists();
        String string = "InvalidStack.NoStackAvailable: Catch a crash not OOM without stack.\n";
        String s = null;
        String s3 = null;
        Label_0521: {
            Label_0516: {
                if (exists) {
                    String a2;
                    try {
                        a2 = a(file.getAbsolutePath(), "\n");
                    }
                    catch (final IOException ex) {
                        a2 = null;
                    }
                    if (!TextUtils.isEmpty((CharSequence)a2)) {
                        final String[] split = a2.split("\n");
                        final ArrayList list = new ArrayList();
                        final StringBuilder sb = new StringBuilder();
                        final StringBuilder obj = new StringBuilder();
                        final int length = split.length;
                        int i = 0;
                        int n = 0;
                        int n2 = 0;
                        while (i < length) {
                            final String e = split[i];
                            if (n == 0 && e.startsWith("stack:")) {
                                n = 1;
                            }
                            else if (n2 == 0 && e.startsWith("err:")) {
                                n2 = 1;
                            }
                            else if (n2 != 0) {
                                obj.append(e);
                                obj.append("\n");
                            }
                            else if (n != 0) {
                                sb.append(e);
                                sb.append("\n");
                            }
                            else {
                                list.add(e);
                            }
                            ++i;
                        }
                        if (list.size() >= 1) {
                            s = list.get(0);
                        }
                        else {
                            s = null;
                        }
                        String str;
                        if (list.size() >= 2) {
                            str = list.get(1);
                        }
                        else {
                            str = null;
                        }
                        String str2;
                        if (list.size() >= 3) {
                            str2 = list.get(2);
                        }
                        else {
                            str2 = null;
                        }
                        String s2;
                        if (list.size() >= 4) {
                            s2 = list.get(3);
                        }
                        else {
                            s2 = null;
                        }
                        String str3 = null;
                        Label_0445: {
                            if (n != 0 && sb.length() > 0) {
                                str3 = sb.toString();
                            }
                            else {
                                StringBuilder sb3 = null;
                                Label_0420: {
                                    if (str2 != null) {
                                        final StringBuilder sb2 = new StringBuilder();
                                        sb2.append(str2);
                                        sb2.append("\nCaused by: ");
                                        sb3 = sb2;
                                        if (!b) {
                                            break Label_0420;
                                        }
                                        sb3 = sb2;
                                    }
                                    else if (str != null) {
                                        final StringBuilder sb4 = new StringBuilder();
                                        sb4.append(str);
                                        sb4.append("\nCaused by: ");
                                        sb3 = sb4;
                                        if (!b) {
                                            break Label_0420;
                                        }
                                        sb3 = sb4;
                                    }
                                    else {
                                        str3 = string;
                                        if (b) {
                                            str3 = "InvalidStack.NoStackAvailable: Catch a OOM Exception without stack.\n";
                                        }
                                        break Label_0445;
                                    }
                                    string = "InvalidStack.NoStackAvailable: Catch a OOM Exception without stack.\n";
                                }
                                sb3.append(string);
                                str3 = sb3.toString();
                            }
                        }
                        string = str3;
                        if (n2 != 0) {
                            string = str3;
                            if (obj.length() > 0) {
                                final StringBuilder sb5 = new StringBuilder();
                                sb5.append(str3);
                                sb5.append("\nCaused by: InvalidStack.CrashWhenWriteStack: Npth error when dumpping the stack:\n");
                                sb5.append((Object)obj);
                                string = sb5.toString();
                            }
                        }
                        s3 = s2;
                        break Label_0521;
                    }
                    if (!b) {
                        break Label_0516;
                    }
                }
                else if (!b) {
                    break Label_0516;
                }
                string = "InvalidStack.NoStackAvailable: Catch a OOM Exception without stack.\n";
            }
            s3 = null;
            s = null;
        }
        a.a("data", (Object)string);
        a.a("process_name", (Object)s);
        a.a("crash_thread_name", (Object)s3);
        a.a("isOOM", b);
        return a;
    }
    
    public static e a(final File file, final CrashType crashType) {
        final a d = d(file);
        final String name = file.getName();
        final String substring = name.substring(name.lastIndexOf(95) + 1);
        final JSONObject optJSONObject = d.h().optJSONObject("header");
        if (optJSONObject.optString("unique_key", (String)null) == null) {
            try {
                final StringBuilder sb = new StringBuilder();
                sb.append("android_");
                sb.append(com.apm.insight.i.c().a());
                sb.append("_");
                sb.append(substring);
                sb.append("_");
                sb.append(CrashType.LAUNCH);
                optJSONObject.put("unique_key", (Object)sb.toString());
            }
            finally {
                final Throwable t;
                t.printStackTrace();
            }
        }
        final e e = new e();
        String s;
        if (crashType == CrashType.LAUNCH) {
            s = com.apm.insight.k.e.e();
        }
        else {
            s = com.apm.insight.k.e.c();
        }
        e.a(s);
        e.a(d.h());
        e.a(com.apm.insight.k.e.a());
        return e;
    }
    
    public static String a(final File file, final String s) {
        return a(file, s, -1L);
    }
    
    public static String a(final File file, final String str, final long n) {
        final StringBuilder sb = new StringBuilder();
        Closeable closeable = null;
        try {
            final BufferedReader bufferedReader = new BufferedReader(new FileReader(file));
            Label_0051: {
                if (n <= 0L) {
                    break Label_0051;
                }
                try {
                    bufferedReader.skip(n);
                    bufferedReader.readLine();
                    while (true) {
                        final String line = bufferedReader.readLine();
                        if (line == null) {
                            break;
                        }
                        if (sb.length() != 0 && str != null) {
                            sb.append(str);
                        }
                        sb.append(line);
                    }
                    k.a(bufferedReader);
                    return sb.toString();
                }
                finally {}
            }
        }
        finally {
            closeable = null;
        }
        k.a(closeable);
    }
    
    public static String a(File parent, String child, final String s, final JSONObject jsonObject, final String s2, final boolean b) {
        if (!((File)parent).exists()) {
            ((File)parent).mkdirs();
        }
        child = (String)new File((File)parent, child);
        final JSONObject jsonObject2 = new JSONObject();
        try {
            jsonObject2.put("url", (Object)s);
            jsonObject2.put("body", (Object)jsonObject);
            parent = (IOException)s2;
            if (s2 == null) {
                parent = (IOException)"";
            }
            jsonObject2.put("dump_file", (Object)parent);
            jsonObject2.put("encrypt", b);
            a((File)child, jsonObject2, false);
            return ((File)child).getAbsolutePath();
        }
        catch (final IOException parent) {}
        catch (final JSONException ex) {}
        parent.printStackTrace();
        return ((File)child).getAbsolutePath();
    }
    
    public static String a(final File file, final String s, final String s2, final JSONObject jsonObject, final boolean b) {
        return a(file, s, s2, jsonObject, null, b);
    }
    
    public static String a(final String pathname, final String s) {
        if (TextUtils.isEmpty((CharSequence)pathname)) {
            return null;
        }
        return a(new File(pathname), s);
    }
    
    public static JSONArray a(@NonNull final File file, final long n) {
        final JSONArray jsonArray = new JSONArray();
        Closeable closeable = null;
        try {
            final BufferedReader bufferedReader = new BufferedReader(new FileReader(file));
            Label_0050: {
                if (n <= 0L) {
                    break Label_0050;
                }
                try {
                    bufferedReader.skip(n);
                    bufferedReader.readLine();
                    while (true) {
                        final String line = bufferedReader.readLine();
                        if (line == null) {
                            break;
                        }
                        jsonArray.put((Object)line);
                    }
                    k.a(bufferedReader);
                    return jsonArray;
                }
                finally {}
            }
        }
        finally {
            closeable = null;
        }
        k.a(closeable);
    }
    
    public static void a(File file, File file2) {
        if (file != null && file2 != null) {
            final Object o = null;
            byte[] array = null;
            final Exception ex2;
            Label_0149: {
                try {
                    ((File)file2).getParentFile().mkdirs();
                    final FileInputStream fileInputStream = new FileInputStream(file);
                    try {
                        file = (File)new FileOutputStream((File)file2);
                        try {
                            array = new byte[8192];
                            while (true) {
                                final int read = fileInputStream.read(array);
                                if (read <= 0) {
                                    break Label_0149;
                                }
                                ((FileOutputStream)file).write(array, 0, read);
                            }
                        }
                        catch (final Exception ex) {}
                    }
                    catch (final Exception file3) {}
                }
                catch (final Exception ex2) {
                    file = null;
                    file2 = array;
                }
                finally {
                    file = null;
                    file2 = o;
                    break Label_0149;
                }
                try {
                    ex2.printStackTrace();
                    final File file3 = file;
                    k.a((Closeable)file3);
                    k.a((Closeable)file2);
                    return;
                }
                finally {}
            }
            k.a((Closeable)file);
            k.a((Closeable)file2);
            throw ex2;
        }
    }
    
    public static void a(@NonNull final File file, @NonNull final String s, final boolean append) {
        if (TextUtils.isEmpty((CharSequence)s)) {
            return;
        }
        file.getParentFile().mkdirs();
        Closeable closeable;
        try {
            final FileOutputStream fileOutputStream = new FileOutputStream(file, append);
            try {
                fileOutputStream.write(s.getBytes());
                fileOutputStream.flush();
                k.a(fileOutputStream);
                return;
            }
            finally {}
        }
        finally {
            closeable = null;
        }
        k.a(closeable);
    }
    
    @Nullable
    public static void a(final File file, Map<String, String> iterator) {
        if (iterator == null || ((Map)iterator).isEmpty()) {
            return;
        }
        final Closeable closeable = null;
        final Closeable closeable2 = null;
        Closeable closeable3 = closeable;
        try {
            Closeable closeable4 = null;
            try {
                closeable3 = closeable;
                final Properties properties = new Properties();
                closeable3 = closeable;
                closeable3 = closeable;
                final FileOutputStream out = new FileOutputStream(file);
                try {
                    iterator = ((Map)iterator).entrySet().iterator();
                    while (iterator.hasNext()) {
                        final Map.Entry entry = (Map.Entry)iterator.next();
                        properties.setProperty((String)entry.getKey(), (String)entry.getValue());
                    }
                    properties.store(out, "no");
                    k.a(out);
                }
                catch (final IOException iterator) {}
                finally {
                    closeable3 = out;
                }
            }
            catch (final IOException iterator) {
                closeable4 = closeable2;
            }
            closeable3 = closeable4;
            q.b((Throwable)iterator);
            k.a(closeable4);
            return;
        }
        finally {}
        k.a(closeable3);
    }
    
    public static void a(@NonNull final File file, @NonNull final JSONArray jsonArray, final boolean b) {
        if (jsonArray == null) {
            return;
        }
        file.getParentFile().mkdirs();
        while (true) {
            BufferedWriter bufferedWriter;
            try {
                bufferedWriter = new BufferedWriter(new FileWriter(file));
                final JSONArray jsonArray2 = jsonArray;
                final BufferedWriter bufferedWriter2 = bufferedWriter;
                m.a(jsonArray2, bufferedWriter2);
                final Closeable closeable = bufferedWriter;
                break Label_0047;
            }
            finally {
                final Closeable closeable = null;
            }
            try {
                final JSONArray jsonArray2 = jsonArray;
                final BufferedWriter bufferedWriter2 = bufferedWriter;
                m.a(jsonArray2, bufferedWriter2);
                final Closeable closeable = bufferedWriter;
                k.a(closeable);
            }
            finally {
                final Closeable closeable = bufferedWriter;
                continue;
            }
            break;
        }
    }
    
    public static void a(@NonNull final File file, @NonNull final JSONObject jsonObject, final boolean b) {
        if (jsonObject == null) {
            return;
        }
        file.getParentFile().mkdirs();
        while (true) {
            BufferedWriter bufferedWriter;
            try {
                bufferedWriter = new BufferedWriter(new FileWriter(file));
                final JSONObject jsonObject2 = jsonObject;
                final BufferedWriter bufferedWriter2 = bufferedWriter;
                m.a(jsonObject2, bufferedWriter2);
                final Closeable closeable = bufferedWriter;
                break Label_0047;
            }
            finally {
                final Closeable closeable = null;
            }
            try {
                final JSONObject jsonObject2 = jsonObject;
                final BufferedWriter bufferedWriter2 = bufferedWriter;
                m.a(jsonObject2, bufferedWriter2);
                final Closeable closeable = bufferedWriter;
                k.a(closeable);
            }
            finally {
                final Closeable closeable = bufferedWriter;
                continue;
            }
            break;
        }
    }
    
    public static void a(final OutputStream out, final File... array) {
        Closeable closeable;
        try {
            final ZipOutputStream zipOutputStream = new ZipOutputStream(out);
            try {
                zipOutputStream.putNextEntry(new ZipEntry("/"));
                for (int length = array.length, i = 0; i < length; ++i) {
                    a(zipOutputStream, array[i]);
                }
                k.a(zipOutputStream);
                return;
            }
            finally {}
        }
        finally {
            closeable = null;
        }
        k.a(closeable);
    }
    
    private static void a(final String s, final File file) {
        Closeable closeable;
        try {
            new File(s).getParentFile().mkdirs();
            final ZipOutputStream zipOutputStream = new ZipOutputStream(new FileOutputStream(s));
            try {
                a(zipOutputStream, file, "");
                k.a(zipOutputStream);
                return;
            }
            finally {}
        }
        finally {
            closeable = null;
        }
        k.a(closeable);
    }
    
    private static void a(final ZipOutputStream zipOutputStream, final File file) {
        if (file != null) {
            if (file.exists()) {
                final boolean directory = file.isDirectory();
                int i = 0;
                File[] listFiles;
                if (directory) {
                    listFiles = file.listFiles();
                }
                else {
                    listFiles = new File[] { file };
                }
                if (listFiles == null) {
                    return;
                }
                while (i < listFiles.length) {
                    final File file2 = listFiles[i];
                    a(zipOutputStream, file2, file2.getName());
                    ++i;
                }
            }
        }
    }
    
    private static void a(final ZipOutputStream zipOutputStream, final File file, String name) {
        if (file == null || !file.exists()) {
            return;
        }
        final boolean directory = file.isDirectory();
        int i = 0;
        if (directory) {
            final File[] listFiles = file.listFiles();
            if (listFiles == null) {
                return;
            }
            final StringBuilder sb = new StringBuilder();
            sb.append(name);
            sb.append("/");
            zipOutputStream.putNextEntry(new ZipEntry(sb.toString()));
            String string;
            if (name.length() == 0) {
                string = "";
            }
            else {
                final StringBuilder sb2 = new StringBuilder();
                sb2.append(name);
                sb2.append("/");
                string = sb2.toString();
            }
            while (i < listFiles.length) {
                final File file2 = listFiles[i];
                name = (String)new StringBuilder();
                ((StringBuilder)name).append(string);
                ((StringBuilder)name).append(listFiles[i].getName());
                a(zipOutputStream, file2, ((StringBuilder)name).toString());
                ++i;
            }
            return;
        }
        zipOutputStream.putNextEntry(new ZipEntry(name));
        Closeable closeable;
        try {
            name = (String)new FileInputStream(file);
            try {
                final byte[] array = new byte[4096];
                while (true) {
                    final int read = ((FileInputStream)name).read(array);
                    if (-1 == read) {
                        break;
                    }
                    zipOutputStream.write(array, 0, read);
                }
                k.a((Closeable)name);
                return;
            }
            finally {}
        }
        finally {
            closeable = null;
        }
        k.a(closeable);
    }
    
    public static boolean a(@NonNull final File file) {
        final boolean exists = file.exists();
        boolean b = true;
        boolean b2 = true;
        if (!exists) {
            return true;
        }
        if (!file.canWrite()) {
            return false;
        }
        if (file.isFile()) {
            return file.delete();
        }
        if (file.isDirectory()) {
            final File[] listFiles = file.listFiles();
            for (int n = 0; listFiles != null && n < listFiles.length; ++n) {
                boolean b3;
                if (listFiles[n].isFile()) {
                    if (!listFiles[n].canWrite()) {
                        b2 = false;
                        continue;
                    }
                    b3 = listFiles[n].delete();
                }
                else {
                    b3 = a(listFiles[n]);
                }
                b2 &= b3;
            }
            b = (b2 & file.delete());
        }
        return b;
    }
    
    public static boolean a(final String pathname) {
        return !TextUtils.isEmpty((CharSequence)pathname) && a(new File(pathname));
    }
    
    public static boolean a(final JSONArray jsonArray) {
        return jsonArray == null || jsonArray.length() == 0;
    }
    
    public static JSONArray b(final String pathname) {
        if (TextUtils.isEmpty((CharSequence)pathname)) {
            return null;
        }
        return a(new File(pathname), -1L);
    }
    
    public static JSONArray b(final String s, final String regex) {
        final JSONArray jsonArray = new JSONArray();
        if (s != null && regex != null) {
            final String[] split = s.split(regex);
            for (int length = split.length, i = 0; i < length; ++i) {
                jsonArray.put((Object)split[i]);
            }
        }
        return jsonArray;
    }
    
    public static void b(@NonNull File file, @NonNull final JSONObject jsonObject, final boolean b) {
        if (jsonObject == null) {
            return;
        }
        file.getParentFile().mkdirs();
        try {
            final BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(file));
            try {
                m.a(jsonObject, bufferedWriter);
            }
            finally {}
        }
        finally {
            file = null;
        }
        while (true) {
            try {
                Label_0105: {
                    final Throwable t;
                    try {
                        jsonObject.put("err_write", (Object)t.toString());
                        a.a(jsonObject, "filters", "err_write", t.getLocalizedMessage());
                    }
                    finally {
                        break Label_0105;
                    }
                    c.a().a("NPTH_CATCH", t);
                    k.a((Closeable)file);
                    return;
                }
                k.a((Closeable)file);
            }
            catch (final JSONException ex) {
                continue;
            }
            break;
        }
    }
    
    public static boolean b(final File file) {
        final String[] list = file.list();
        return list == null || list.length == 0;
    }
    
    public static String c(final File file) {
        return a(file, "\n");
    }
    
    public static String c(final String s) {
        return a(s, "\n");
    }
    
    public static void c(final String pathname, final String s) {
        a(s, new File(pathname));
    }
    
    public static a d(File optJSONObject) {
        final a a = a(new File(optJSONObject, "logEventStack"), optJSONObject.getName().contains("oom"));
        int i = 0;
        final boolean b = false;
        Label_0122: {
            if (i >= d.a()) {
                break Label_0122;
            }
            final StringBuilder sb = new StringBuilder();
            sb.append(".");
            sb.append(i);
            final File a2 = o.a(optJSONObject, sb.toString());
            while (true) {
                if (!a2.exists()) {
                    break Label_0114;
                }
                try {
                    a.c(new JSONObject(c(a2.getAbsolutePath())));
                    ++i;
                    if (b) {
                        optJSONObject = (File)"step";
                    }
                    else {
                        optJSONObject = (File)"simple";
                    }
                    a.a("crash_type", (String)optJSONObject);
                    optJSONObject = (File)a.h().optJSONObject("header");
                    final JSONObject f = Header.a(com.apm.insight.i.g(), a.h().optLong("crash_time", 0L)).f();
                    if (optJSONObject == null) {
                        a.a(f);
                    }
                    else {
                        l.a((JSONObject)optJSONObject, f);
                    }
                    return a;
                }
                finally {
                    continue;
                }
                break;
            }
        }
    }
    
    public static e d(final String s) {
        try {
            final String c = c(s);
            if (c == null) {}
            final JSONObject jsonObject = new JSONObject(c);
            final e e = new e();
            e.a(jsonObject.optString("url"));
            e.a(jsonObject.optJSONObject("body"));
            e.b(jsonObject.optString("dump_file"));
            e.a(jsonObject.optBoolean("encrypt", false));
            return e;
        }
        finally {
            return null;
        }
    }
    
    public static e e(String optJSONArray) {
        try {
            optJSONArray = (JSONException)new JSONObject(c((String)optJSONArray));
            final e e = new e();
            e.d(((JSONObject)optJSONArray).optString("aid"));
            e.c(((JSONObject)optJSONArray).optString("did"));
            e.e(((JSONObject)optJSONArray).optString("processName"));
            final ArrayList<String> list = new ArrayList<String>();
            optJSONArray = (JSONException)((JSONObject)optJSONArray).optJSONArray("alogFiles");
            if (optJSONArray != null) {
                for (int i = 0; i < ((JSONArray)optJSONArray).length(); ++i) {
                    list.add(((JSONArray)optJSONArray).getString(i));
                }
                e.a(list);
            }
            return e;
        }
        catch (final JSONException optJSONArray) {}
        catch (final IOException ex) {}
        ((Throwable)optJSONArray).printStackTrace();
        return null;
    }
    
    @Nullable
    public static Map<String, String> e(final File p0) {
        // 
        // This method could not be decompiled.
        // 
        // Original Bytecode:
        // 
        //     1: astore_2       
        //     2: new             Ljava/util/Properties;
        //     5: astore_3       
        //     6: aload_3        
        //     7: invokespecial   java/util/Properties.<init>:()V
        //    10: new             Ljava/io/FileInputStream;
        //    13: astore_1       
        //    14: aload_1        
        //    15: aload_0        
        //    16: invokespecial   java/io/FileInputStream.<init>:(Ljava/io/File;)V
        //    19: aload_1        
        //    20: astore_0       
        //    21: aload_3        
        //    22: aload_1        
        //    23: invokevirtual   java/util/Properties.load:(Ljava/io/InputStream;)V
        //    26: aload_1        
        //    27: astore_0       
        //    28: aload_3        
        //    29: invokevirtual   java/util/Properties.stringPropertyNames:()Ljava/util/Set;
        //    32: astore          4
        //    34: aload_1        
        //    35: astore_0       
        //    36: new             Ljava/util/HashMap;
        //    39: astore_2       
        //    40: aload_1        
        //    41: astore_0       
        //    42: aload_2        
        //    43: invokespecial   java/util/HashMap.<init>:()V
        //    46: aload_1        
        //    47: astore_0       
        //    48: aload           4
        //    50: invokeinterface java/util/Set.iterator:()Ljava/util/Iterator;
        //    55: astore          5
        //    57: aload_1        
        //    58: astore_0       
        //    59: aload           5
        //    61: invokeinterface java/util/Iterator.hasNext:()Z
        //    66: ifeq            103
        //    69: aload_1        
        //    70: astore_0       
        //    71: aload           5
        //    73: invokeinterface java/util/Iterator.next:()Ljava/lang/Object;
        //    78: checkcast       Ljava/lang/String;
        //    81: astore          4
        //    83: aload_1        
        //    84: astore_0       
        //    85: aload_2        
        //    86: aload           4
        //    88: aload_3        
        //    89: aload           4
        //    91: invokevirtual   java/util/Properties.getProperty:(Ljava/lang/String;)Ljava/lang/String;
        //    94: invokeinterface java/util/Map.put:(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
        //    99: pop            
        //   100: goto            57
        //   103: aload_1        
        //   104: invokestatic    com/apm/insight/l/k.a:(Ljava/io/Closeable;)V
        //   107: aload_2        
        //   108: areturn        
        //   109: astore_2       
        //   110: goto            124
        //   113: astore_0       
        //   114: aload_2        
        //   115: astore_1       
        //   116: aload_0        
        //   117: astore_2       
        //   118: goto            141
        //   121: astore_2       
        //   122: aconst_null    
        //   123: astore_1       
        //   124: aload_1        
        //   125: astore_0       
        //   126: aload_2        
        //   127: invokestatic    com/apm/insight/l/q.b:(Ljava/lang/Throwable;)V
        //   130: aload_1        
        //   131: invokestatic    com/apm/insight/l/k.a:(Ljava/io/Closeable;)V
        //   134: aconst_null    
        //   135: areturn        
        //   136: astore_1       
        //   137: aload_1        
        //   138: astore_2       
        //   139: aload_0        
        //   140: astore_1       
        //   141: aload_1        
        //   142: invokestatic    com/apm/insight/l/k.a:(Ljava/io/Closeable;)V
        //   145: aload_2        
        //   146: athrow         
        //    Signature:
        //  (Ljava/io/File;)Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;
        //    Exceptions:
        //  Try           Handler
        //  Start  End    Start  End    Type                 
        //  -----  -----  -----  -----  ---------------------
        //  2      19     121    124    Ljava/io/IOException;
        //  2      19     113    121    Any
        //  21     26     109    113    Ljava/io/IOException;
        //  21     26     136    141    Any
        //  28     34     109    113    Ljava/io/IOException;
        //  28     34     136    141    Any
        //  36     40     109    113    Ljava/io/IOException;
        //  36     40     136    141    Any
        //  42     46     109    113    Ljava/io/IOException;
        //  42     46     136    141    Any
        //  48     57     109    113    Ljava/io/IOException;
        //  48     57     136    141    Any
        //  59     69     109    113    Ljava/io/IOException;
        //  59     69     136    141    Any
        //  71     83     109    113    Ljava/io/IOException;
        //  71     83     136    141    Any
        //  85     100    109    113    Ljava/io/IOException;
        //  85     100    136    141    Any
        //  126    130    136    141    Any
        // 
        // The error that occurred was:
        // 
        // java.lang.IllegalStateException: Expression is linked from several locations: Label_0057:
        //     at com.strobel.decompiler.ast.Error.expressionLinkedFromMultipleLocations(Error.java:27)
        //     at com.strobel.decompiler.ast.AstOptimizer.mergeDisparateObjectInitializations(AstOptimizer.java:2604)
        //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:235)
        //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:42)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:206)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:93)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethodBody(AstBuilder.java:868)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethod(AstBuilder.java:761)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addTypeMembers(AstBuilder.java:638)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeCore(AstBuilder.java:605)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeNoCache(AstBuilder.java:195)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createType(AstBuilder.java:162)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addType(AstBuilder.java:137)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.buildAst(JavaLanguage.java:71)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.decompileType(JavaLanguage.java:59)
        //     at com.strobel.decompiler.DecompilerDriver.decompileType(DecompilerDriver.java:333)
        //     at com.strobel.decompiler.DecompilerDriver.decompileJar(DecompilerDriver.java:254)
        //     at com.strobel.decompiler.DecompilerDriver.main(DecompilerDriver.java:144)
        // 
        throw new IllegalStateException("An error occurred while decompiling this method.");
    }
    
    public static void f(File parent) {
        parent = new File(parent, "lock");
        try {
            parent.createNewFile();
            NativeImpl.c(parent.getAbsolutePath());
        }
        finally {
            final Throwable t;
            c.a().a("NPTH_CATCH", t);
        }
    }
    
    public static boolean g(File parent) {
        if (!parent.isFile()) {
            parent = new File(parent, "lock");
        }
        if (!parent.exists()) {
            return false;
        }
        try {
            final int c = NativeImpl.c(parent.getAbsolutePath());
            if (c > 0) {
                NativeImpl.a(c);
                return false;
            }
            if (c < 0) {
                return true;
            }
        }
        finally {
            final Throwable t;
            c.a().a("NPTH_CATCH", t);
        }
        return false;
    }
}

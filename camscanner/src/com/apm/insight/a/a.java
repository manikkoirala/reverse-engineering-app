// 
// Decompiled by Procyon v0.6.0
// 

package com.apm.insight.a;

import androidx.annotation.Nullable;
import androidx.annotation.NonNull;
import com.apm.insight.runtime.p;
import com.apm.insight.CrashType;
import com.apm.insight.ICrashCallback;

public class a implements ICrashCallback
{
    private static volatile a d;
    private volatile String a;
    private volatile b b;
    private volatile c c;
    private volatile boolean e;
    
    private a() {
        this.e = false;
    }
    
    public static a a() {
        if (a.d == null) {
            synchronized (a.class) {
                if (a.d == null) {
                    a.d = new a();
                }
            }
        }
        return a.d;
    }
    
    public void a(final CrashType crashType, final long n, final String s) {
    }
    
    public void a(final String s) {
    }
    
    public void a(final String a, final b b, final c c) {
        this.a = a;
        this.b = b;
        this.c = c;
        if (!this.e) {
            this.e = true;
            p.b().a(new Runnable(this) {
                final a a;
                
                @Override
                public void run() {
                }
            });
        }
    }
    
    public void b() {
    }
    
    @Override
    public void onCrash(@NonNull final CrashType crashType, @Nullable final String s, @Nullable final Thread thread) {
        crashType.equals(CrashType.NATIVE);
    }
}

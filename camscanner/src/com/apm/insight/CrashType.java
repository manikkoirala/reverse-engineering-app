// 
// Decompiled by Procyon v0.6.0
// 

package com.apm.insight;

public enum CrashType
{
    private static final CrashType[] $VALUES;
    
    ALL("all"), 
    ANR("anr"), 
    ASAN("asan"), 
    BLOCK("block"), 
    CUSTOM_JAVA("custom_java"), 
    DART("dart"), 
    ENSURE("ensure"), 
    JAVA("java"), 
    LAUNCH("launch"), 
    NATIVE("native"), 
    OOM("oom"), 
    TSAN("tsan");
    
    private String mName;
    
    private CrashType(final String mName) {
        this.mName = mName;
    }
    
    public String getName() {
        return this.mName;
    }
    
    @Override
    public String toString() {
        return this.getName();
    }
}

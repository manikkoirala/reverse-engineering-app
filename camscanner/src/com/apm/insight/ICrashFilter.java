// 
// Decompiled by Procyon v0.6.0
// 

package com.apm.insight;

public interface ICrashFilter
{
    boolean onJavaCrashFilter(final Throwable p0, final Thread p1);
    
    boolean onNativeCrashFilter(final String p0, final String p1);
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.apm.insight.j;

import java.util.Iterator;
import com.apm.insight.l.q;
import java.util.ArrayList;
import android.content.Context;
import android.os.Handler;
import java.util.List;

public final class d
{
    private List<a> a;
    
    private d(final Handler handler, final Context context) {
        this.a = new ArrayList<a>(3);
        if (com.apm.insight.l.a.b(context)) {
            this.a.add(new c(handler, 0L, 15000L));
        }
    }
    
    public static d a(final Handler handler, final Context context) {
        return new d(handler, context);
    }
    
    public void a() {
        final StringBuilder sb = new StringBuilder();
        sb.append("[ScheduleTaskManager] execute, task size=");
        sb.append(this.a.size());
        q.a((Object)sb.toString());
        for (final a a : this.a) {
            try {
                a.a();
            }
            finally {}
        }
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.apm.insight.j;

import android.os.Handler;

public abstract class a implements Runnable
{
    private Handler a;
    private final long b;
    private final long c;
    
    a(final Handler a, final long b, final long c) {
        this.a = a;
        this.b = b;
        this.c = c;
    }
    
    void a() {
        final long b = this.b();
        final Handler a = this.a;
        if (b > 0L) {
            a.postDelayed((Runnable)this, this.b());
        }
        else {
            a.post((Runnable)this);
        }
    }
    
    void a(final long n) {
        if (n > 0L) {
            this.a.postDelayed((Runnable)this, n);
        }
        else {
            this.a.post((Runnable)this);
        }
    }
    
    long b() {
        return this.b;
    }
    
    long c() {
        return this.c;
    }
}

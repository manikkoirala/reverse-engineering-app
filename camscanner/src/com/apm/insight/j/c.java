// 
// Decompiled by Procyon v0.6.0
// 

package com.apm.insight.j;

import com.apm.insight.l.q;
import android.text.TextUtils;
import com.apm.insight.i;
import android.os.Handler;

public class c extends a
{
    c(final Handler handler, final long n, final long n2) {
        super(handler, n, n2);
    }
    
    @Override
    public void run() {
        if (i.c().b()) {
            return;
        }
        final String d = i.a().d();
        String string;
        if (!TextUtils.isEmpty((CharSequence)d) && !"0".equals(d)) {
            i.c().a(d);
            final StringBuilder sb = new StringBuilder();
            sb.append("[DeviceIdTask] did is ");
            sb.append(d);
            string = sb.toString();
        }
        else {
            this.a(this.c());
            string = "[DeviceIdTask] did is null, continue check.";
        }
        q.a((Object)string);
    }
}

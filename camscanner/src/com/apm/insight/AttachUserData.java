// 
// Decompiled by Procyon v0.6.0
// 

package com.apm.insight;

import androidx.annotation.Nullable;
import java.util.Map;

public interface AttachUserData
{
    @Nullable
    Map<? extends String, ? extends String> getUserData(final CrashType p0);
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.apm.insight.entity;

import java.util.List;
import org.json.JSONObject;

public class e
{
    private String a;
    private JSONObject b;
    private String c;
    private boolean d;
    private String e;
    private String f;
    private String g;
    private List<String> h;
    
    public String a() {
        return this.a;
    }
    
    public void a(final String a) {
        this.a = a;
    }
    
    public void a(final List<String> h) {
        this.h = h;
    }
    
    public void a(final JSONObject b) {
        this.b = b;
    }
    
    public void a(final boolean d) {
        this.d = d;
    }
    
    public JSONObject b() {
        return this.b;
    }
    
    public void b(final String c) {
        this.c = c;
    }
    
    public String c() {
        return this.c;
    }
    
    public void c(final String e) {
        this.e = e;
    }
    
    public String d() {
        return this.e;
    }
    
    public void d(final String f) {
        this.f = f;
    }
    
    public String e() {
        return this.f;
    }
    
    public void e(final String g) {
        this.g = g;
    }
    
    public String f() {
        return this.g;
    }
    
    public List<String> g() {
        return this.h;
    }
}

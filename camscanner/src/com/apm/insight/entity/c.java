// 
// Decompiled by Procyon v0.6.0
// 

package com.apm.insight.entity;

import com.apm.insight.i;
import androidx.annotation.Nullable;
import androidx.annotation.NonNull;

public class c extends a
{
    private String c;
    
    public c(final String c) {
        this.c = c;
    }
    
    @NonNull
    public static c a(@NonNull final StackTraceElement stackTraceElement, @NonNull final String s, @Nullable final String s2, @NonNull final String s3, final boolean i, final String s4, final String s5) {
        final c c = new c(s5);
        final String className = stackTraceElement.getClassName();
        final String methodName = stackTraceElement.getMethodName();
        final int lineNumber = stackTraceElement.getLineNumber();
        c.a("event_type", (Object)"exception");
        c.a("log_type", (Object)s5);
        c.a("timestamp", System.currentTimeMillis());
        c.a("crash_time", System.currentTimeMillis());
        c.a("class_ref", (Object)className);
        c.a("method", (Object)methodName);
        c.a("line_num", lineNumber);
        c.a("stack", (Object)s);
        c.a("exception_type", 1);
        c.a("ensure_type", (Object)s4);
        c.a("is_core", (int)(i ? 1 : 0));
        c.a("message", (Object)s2);
        c.a("process_name", (Object)com.apm.insight.l.a.c(i.g()));
        c.a("crash_thread_name", (Object)s3);
        d.b(c.h());
        return c;
    }
}

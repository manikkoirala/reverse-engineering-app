// 
// Decompiled by Procyon v0.6.0
// 

package com.apm.insight.entity;

import java.util.Iterator;
import androidx.annotation.Nullable;
import java.util.Map;
import android.telephony.TelephonyManager;
import org.json.JSONException;
import com.apm.insight.l.p;
import com.apm.insight.l.t;
import java.util.TimeZone;
import java.util.Locale;
import com.apm.insight.l.q;
import android.util.DisplayMetrics;
import android.annotation.SuppressLint;
import android.text.TextUtils;
import androidx.annotation.Keep;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.os.Build;
import android.os.Build$VERSION;
import com.apm.insight.i;
import com.apm.insight.runtime.s;
import org.json.JSONObject;
import android.content.Context;

public final class Header
{
    private static final String[] a;
    private static String d;
    private static int e = -1;
    private static int f = -1;
    private Context b;
    private JSONObject c;
    
    static {
        a = new String[] { "version_code", "manifest_version_code", "aid", "update_version_code" };
    }
    
    public Header(final Context b) {
        this.c = new JSONObject();
        this.b = b;
    }
    
    public static Header a(final Context context) {
        final Header header = new Header(context);
        header.e(header.f());
        return header;
    }
    
    public static Header a(Context a, final long n) {
        final s a2 = s.a();
        long currentTimeMillis = n;
        if (n == 0L) {
            currentTimeMillis = System.currentTimeMillis();
        }
        final JSONObject a3 = a2.a(currentTimeMillis);
        while (true) {
            if (a3 != null && a3.length() != 0) {
                a = (Context)new Header(i.g());
                break Label_0076;
            }
            a = (Context)a(i.g());
            ((Header)a).c();
            try {
                ((Header)a).f().put("errHeader", 1);
                b((Header)a);
                ((Header)a).a(a3);
                return (Header)a;
            }
            finally {
                continue;
            }
            break;
        }
    }
    
    public static Header a(final Header header) {
        addRuntimeHeader(header.f());
        return header;
    }
    
    public static boolean a() {
        if (Header.e == -1) {
            Header.e = (g().contains("64") ? 1 : 0);
        }
        final int e = Header.e;
        boolean b = true;
        if (e != 1) {
            b = false;
        }
        return b;
    }
    
    @Keep
    public static void addOtherHeader(final JSONObject jsonObject) {
        if (jsonObject == null) {
            return;
        }
        h(jsonObject);
        f(jsonObject);
        g(jsonObject);
        try {
            jsonObject.put("os", (Object)"Android");
            jsonObject.put("device_id", (Object)i.c().a());
            jsonObject.put("os_version", (Object)h());
            jsonObject.put("os_api", Build$VERSION.SDK_INT);
            final String model = Build.MODEL;
            final String brand = Build.BRAND;
            String string;
            if (model == null) {
                string = brand;
            }
            else {
                string = model;
                if (brand != null) {
                    string = model;
                    if (!model.contains(brand)) {
                        final StringBuilder sb = new StringBuilder();
                        sb.append(brand);
                        sb.append(' ');
                        sb.append(model);
                        string = sb.toString();
                    }
                }
            }
            jsonObject.put("device_model", (Object)string);
            jsonObject.put("device_brand", (Object)brand);
            jsonObject.put("device_manufacturer", (Object)Build.MANUFACTURER);
            jsonObject.put("cpu_abi", (Object)g());
            final Context g = i.g();
            final String packageName = g.getPackageName();
            jsonObject.put("package", (Object)packageName);
            final PackageInfo packageInfo = g.getPackageManager().getPackageInfo(packageName, 0);
            final ApplicationInfo applicationInfo = packageInfo.applicationInfo;
            if (applicationInfo != null) {
                final int labelRes = applicationInfo.labelRes;
                CharSequence charSequence;
                if (labelRes > 0) {
                    charSequence = g.getString(labelRes);
                }
                else {
                    charSequence = g.getPackageManager().getApplicationLabel(packageInfo.applicationInfo);
                }
                jsonObject.put("display_name", (Object)charSequence);
            }
        }
        finally {
            final Throwable t;
            t.printStackTrace();
        }
        com.apm.insight.entity.d.a(jsonObject);
    }
    
    public static void addRuntimeHeader(final JSONObject jsonObject) {
        i(jsonObject);
        j(jsonObject);
    }
    
    public static Header b(final Context context) {
        final Header a = a(context);
        a(a);
        b(a);
        a.c();
        a.d();
        a.e();
        return a;
    }
    
    public static void b(final Header header) {
        if (header == null) {
            return;
        }
        addOtherHeader(header.f());
    }
    
    public static boolean b() {
        if (Header.f == -1) {
            Header.f = (g().contains("86") ? 1 : 0);
        }
        final int f = Header.f;
        boolean b = true;
        if (f != 1) {
            b = false;
        }
        return b;
    }
    
    public static boolean b(final JSONObject jsonObject) {
        boolean b = false;
        if (jsonObject.optInt("unauthentic_version", 0) == 1) {
            b = true;
        }
        return b;
    }
    
    public static boolean c(final JSONObject jsonObject) {
        return jsonObject == null || jsonObject.length() == 0 || (jsonObject.opt("app_version") == null && jsonObject.opt("version_name") == null) || jsonObject.opt("version_code") == null || jsonObject.opt("update_version_code") == null;
    }
    
    public static boolean d(final JSONObject jsonObject) {
        if (jsonObject == null) {
            return true;
        }
        if (jsonObject.length() == 0) {
            return true;
        }
        final String optString = jsonObject.optString("aid");
        if (TextUtils.isEmpty((CharSequence)optString)) {
            return true;
        }
        try {
            return Integer.parseInt(optString) <= 0;
        }
        finally {
            return true;
        }
    }
    
    @SuppressLint({ "MissingPermission" })
    private void e(final JSONObject jsonObject) {
        try {
            jsonObject.put("sdk_version", 1030835);
            jsonObject.put("sdk_version_name", (Object)"1.3.8.nourl-alpha.15");
        }
        catch (final Exception ex) {}
    }
    
    private static void f(final JSONObject jsonObject) {
        try {
            final DisplayMetrics displayMetrics = i.g().getResources().getDisplayMetrics();
            final int densityDpi = displayMetrics.densityDpi;
            String s;
            if (densityDpi != 120) {
                if (densityDpi != 240) {
                    if (densityDpi != 320) {
                        s = "mdpi";
                    }
                    else {
                        s = "xhdpi";
                    }
                }
                else {
                    s = "hdpi";
                }
            }
            else {
                s = "ldpi";
            }
            jsonObject.put("density_dpi", densityDpi);
            jsonObject.put("display_density", (Object)s);
            final StringBuilder sb = new StringBuilder();
            sb.append(displayMetrics.heightPixels);
            sb.append("x");
            sb.append(displayMetrics.widthPixels);
            jsonObject.put("resolution", (Object)sb.toString());
        }
        catch (final Exception ex) {}
    }
    
    private static String g() {
        if (Header.d == null) {
            try {
                final StringBuilder sb = new StringBuilder();
                StringBuilder sb2;
                if (Build.SUPPORTED_ABIS.length > 0) {
                    int n = 0;
                    while (true) {
                        final String[] supported_ABIS = Build.SUPPORTED_ABIS;
                        sb2 = sb;
                        if (n >= supported_ABIS.length) {
                            break;
                        }
                        sb.append(supported_ABIS[n]);
                        if (n != supported_ABIS.length - 1) {
                            sb.append(", ");
                        }
                        ++n;
                    }
                }
                else {
                    sb2 = new StringBuilder(Build.CPU_ABI);
                }
                if (TextUtils.isEmpty((CharSequence)sb2.toString())) {
                    Header.d = "unknown";
                }
                Header.d = sb2.toString();
            }
            catch (final Exception ex) {
                q.b(ex);
                Header.d = "unknown";
            }
        }
        return Header.d;
    }
    
    private static void g(final JSONObject jsonObject) {
        try {
            final String language = i.g().getResources().getConfiguration().locale.getLanguage();
            if (!TextUtils.isEmpty((CharSequence)language)) {
                jsonObject.put("language", (Object)language);
            }
            final String country = Locale.getDefault().getCountry();
            if (!TextUtils.isEmpty((CharSequence)country)) {
                jsonObject.put("region", (Object)country);
            }
            int n;
            if ((n = TimeZone.getDefault().getRawOffset() / 3600000) < -12) {
                n = -12;
            }
            int n2;
            if ((n2 = n) > 12) {
                n2 = 12;
            }
            jsonObject.put("timezone", n2);
        }
        catch (final Exception ex) {}
    }
    
    private static String h() {
        final String release = Build$VERSION.RELEASE;
        if (release.contains(".")) {
            return release;
        }
        final StringBuilder sb = new StringBuilder();
        sb.append(release);
        sb.append(".0");
        return sb.toString();
    }
    
    private static void h(final JSONObject jsonObject) {
        final StringBuilder sb = new StringBuilder();
        try {
            Label_0079: {
                String str;
                if (com.apm.insight.l.d.c()) {
                    str = "MIUI-";
                }
                else if (com.apm.insight.l.d.d()) {
                    str = "FLYME-";
                }
                else {
                    final String a = com.apm.insight.l.d.a();
                    if (com.apm.insight.l.d.a(a)) {
                        sb.append("EMUI-");
                    }
                    if (TextUtils.isEmpty((CharSequence)a)) {
                        break Label_0079;
                    }
                    sb.append(a);
                    str = "-";
                }
                sb.append(str);
            }
            sb.append(Build$VERSION.INCREMENTAL);
            if (sb.length() > 0) {
                jsonObject.put("rom", (Object)sb.toString());
            }
            jsonObject.put("rom_version", (Object)t.a());
        }
        finally {}
    }
    
    private static void i(final JSONObject jsonObject) {
        try {
            jsonObject.put("access", (Object)p.a(i.g()));
        }
        catch (final JSONException ex) {
            ((Throwable)ex).printStackTrace();
        }
    }
    
    private static void j(final JSONObject jsonObject) {
        try {
            final TelephonyManager telephonyManager = (TelephonyManager)i.g().getSystemService("phone");
            if (telephonyManager != null) {
                final String networkOperatorName = telephonyManager.getNetworkOperatorName();
                if (!TextUtils.isEmpty((CharSequence)networkOperatorName)) {
                    jsonObject.put("carrier", (Object)networkOperatorName);
                }
                final String networkOperator = telephonyManager.getNetworkOperator();
                if (!TextUtils.isEmpty((CharSequence)networkOperator)) {
                    jsonObject.put("mcc_mnc", (Object)networkOperator);
                }
            }
        }
        catch (final Exception ex) {
            ex.printStackTrace();
        }
    }
    
    public JSONObject a(@Nullable final Map<String, Object> p0) {
        // 
        // This method could not be decompiled.
        // 
        // Original Bytecode:
        // 
        //     1: ifnonnull       9
        //     4: aload_0        
        //     5: getfield        com/apm/insight/entity/Header.c:Lorg/json/JSONObject;
        //     8: areturn        
        //     9: aload_1        
        //    10: invokeinterface java/util/Map.entrySet:()Ljava/util/Set;
        //    15: invokeinterface java/util/Set.iterator:()Ljava/util/Iterator;
        //    20: astore          6
        //    22: aload           6
        //    24: invokeinterface java/util/Iterator.hasNext:()Z
        //    29: ifeq            92
        //    32: aload           6
        //    34: invokeinterface java/util/Iterator.next:()Ljava/lang/Object;
        //    39: checkcast       Ljava/util/Map$Entry;
        //    42: astore          7
        //    44: aload_0        
        //    45: getfield        com/apm/insight/entity/Header.c:Lorg/json/JSONObject;
        //    48: aload           7
        //    50: invokeinterface java/util/Map$Entry.getKey:()Ljava/lang/Object;
        //    55: checkcast       Ljava/lang/String;
        //    58: invokevirtual   org/json/JSONObject.has:(Ljava/lang/String;)Z
        //    61: ifne            22
        //    64: aload_0        
        //    65: getfield        com/apm/insight/entity/Header.c:Lorg/json/JSONObject;
        //    68: aload           7
        //    70: invokeinterface java/util/Map$Entry.getKey:()Ljava/lang/Object;
        //    75: checkcast       Ljava/lang/String;
        //    78: aload           7
        //    80: invokeinterface java/util/Map$Entry.getValue:()Ljava/lang/Object;
        //    85: invokevirtual   org/json/JSONObject.put:(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
        //    88: pop            
        //    89: goto            22
        //    92: getstatic       com/apm/insight/entity/Header.a:[Ljava/lang/String;
        //    95: astore          8
        //    97: aload           8
        //    99: arraylength    
        //   100: istore_3       
        //   101: iconst_0       
        //   102: istore_2       
        //   103: iload_2        
        //   104: iload_3        
        //   105: if_icmpge       186
        //   108: aload           8
        //   110: iload_2        
        //   111: aaload         
        //   112: astore          7
        //   114: aload_1        
        //   115: aload           7
        //   117: invokeinterface java/util/Map.containsKey:(Ljava/lang/Object;)Z
        //   122: istore          5
        //   124: iload           5
        //   126: ifeq            180
        //   129: aload_1        
        //   130: aload           7
        //   132: invokeinterface java/util/Map.get:(Ljava/lang/Object;)Ljava/lang/Object;
        //   137: invokestatic    java/lang/String.valueOf:(Ljava/lang/Object;)Ljava/lang/String;
        //   140: invokestatic    java/lang/Integer.parseInt:(Ljava/lang/String;)I
        //   143: istore          4
        //   145: aload_0        
        //   146: getfield        com/apm/insight/entity/Header.c:Lorg/json/JSONObject;
        //   149: aload           7
        //   151: iload           4
        //   153: invokevirtual   org/json/JSONObject.put:(Ljava/lang/String;I)Lorg/json/JSONObject;
        //   156: pop            
        //   157: goto            180
        //   160: astore          6
        //   162: aload_0        
        //   163: getfield        com/apm/insight/entity/Header.c:Lorg/json/JSONObject;
        //   166: aload           7
        //   168: aload_1        
        //   169: aload           7
        //   171: invokeinterface java/util/Map.get:(Ljava/lang/Object;)Ljava/lang/Object;
        //   176: invokevirtual   org/json/JSONObject.put:(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
        //   179: pop            
        //   180: iinc            2, 1
        //   183: goto            103
        //   186: aload_1        
        //   187: ldc             "version_code"
        //   189: invokeinterface java/util/Map.containsKey:(Ljava/lang/Object;)Z
        //   194: ifeq            238
        //   197: aload_1        
        //   198: ldc             "manifest_version_code"
        //   200: invokeinterface java/util/Map.containsKey:(Ljava/lang/Object;)Z
        //   205: istore          5
        //   207: iload           5
        //   209: ifne            238
        //   212: aload_1        
        //   213: ldc             "version_code"
        //   215: invokeinterface java/util/Map.get:(Ljava/lang/Object;)Ljava/lang/Object;
        //   220: invokestatic    java/lang/String.valueOf:(Ljava/lang/Object;)Ljava/lang/String;
        //   223: invokestatic    java/lang/Integer.parseInt:(Ljava/lang/String;)I
        //   226: istore_2       
        //   227: aload_0        
        //   228: getfield        com/apm/insight/entity/Header.c:Lorg/json/JSONObject;
        //   231: ldc             "manifest_version_code"
        //   233: iload_2        
        //   234: invokevirtual   org/json/JSONObject.put:(Ljava/lang/String;I)Lorg/json/JSONObject;
        //   237: pop            
        //   238: aload_1        
        //   239: ldc_w           "iid"
        //   242: invokeinterface java/util/Map.containsKey:(Ljava/lang/Object;)Z
        //   247: ifeq            285
        //   250: aload_0        
        //   251: getfield        com/apm/insight/entity/Header.c:Lorg/json/JSONObject;
        //   254: astore          6
        //   256: aload           6
        //   258: ldc_w           "udid"
        //   261: aload_1        
        //   262: ldc_w           "iid"
        //   265: invokeinterface java/util/Map.get:(Ljava/lang/Object;)Ljava/lang/Object;
        //   270: invokevirtual   org/json/JSONObject.put:(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
        //   273: pop            
        //   274: aload_0        
        //   275: getfield        com/apm/insight/entity/Header.c:Lorg/json/JSONObject;
        //   278: ldc_w           "iid"
        //   281: invokevirtual   org/json/JSONObject.remove:(Ljava/lang/String;)Ljava/lang/Object;
        //   284: pop            
        //   285: aload_1        
        //   286: ldc_w           "version_name"
        //   289: invokeinterface java/util/Map.containsKey:(Ljava/lang/Object;)Z
        //   294: ifeq            331
        //   297: aload_0        
        //   298: getfield        com/apm/insight/entity/Header.c:Lorg/json/JSONObject;
        //   301: astore          6
        //   303: aload           6
        //   305: ldc             "app_version"
        //   307: aload_1        
        //   308: ldc_w           "version_name"
        //   311: invokeinterface java/util/Map.get:(Ljava/lang/Object;)Ljava/lang/Object;
        //   316: invokevirtual   org/json/JSONObject.put:(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
        //   319: pop            
        //   320: aload_0        
        //   321: getfield        com/apm/insight/entity/Header.c:Lorg/json/JSONObject;
        //   324: ldc_w           "version_name"
        //   327: invokevirtual   org/json/JSONObject.remove:(Ljava/lang/String;)Ljava/lang/Object;
        //   330: pop            
        //   331: aload_0        
        //   332: getfield        com/apm/insight/entity/Header.c:Lorg/json/JSONObject;
        //   335: areturn        
        //   336: astore_1       
        //   337: goto            331
        //   340: astore          6
        //   342: goto            238
        //    Signature:
        //  (Ljava/util/Map<Ljava/lang/String;Ljava/lang/Object;>;)Lorg/json/JSONObject;
        //    Exceptions:
        //  Try           Handler
        //  Start  End    Start  End    Type
        //  -----  -----  -----  -----  ----
        //  4      9      336    340    Any
        //  9      22     336    340    Any
        //  22     89     336    340    Any
        //  92     101    336    340    Any
        //  114    124    336    340    Any
        //  129    157    160    180    Any
        //  162    180    336    340    Any
        //  186    207    336    340    Any
        //  212    238    340    345    Any
        //  238    256    336    340    Any
        //  256    285    336    340    Any
        //  285    303    336    340    Any
        //  303    331    336    340    Any
        // 
        // The error that occurred was:
        // 
        // java.lang.IllegalStateException: Expression is linked from several locations: Label_0238:
        //     at com.strobel.decompiler.ast.Error.expressionLinkedFromMultipleLocations(Error.java:27)
        //     at com.strobel.decompiler.ast.AstOptimizer.mergeDisparateObjectInitializations(AstOptimizer.java:2604)
        //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:235)
        //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:42)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:206)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:93)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethodBody(AstBuilder.java:868)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethod(AstBuilder.java:761)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addTypeMembers(AstBuilder.java:638)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeCore(AstBuilder.java:605)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeNoCache(AstBuilder.java:195)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createType(AstBuilder.java:162)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addType(AstBuilder.java:137)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.buildAst(JavaLanguage.java:71)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.decompileType(JavaLanguage.java:59)
        //     at com.strobel.decompiler.DecompilerDriver.decompileType(DecompilerDriver.java:333)
        //     at com.strobel.decompiler.DecompilerDriver.decompileJar(DecompilerDriver.java:254)
        //     at com.strobel.decompiler.DecompilerDriver.main(DecompilerDriver.java:144)
        // 
        throw new IllegalStateException("An error occurred while decompiling this method.");
    }
    
    public JSONObject a(final JSONObject jsonObject) {
        if (jsonObject == null) {
            return this.c;
        }
        final Iterator keys = jsonObject.keys();
        while (keys.hasNext()) {
            final String s = keys.next();
            try {
                this.c.put(s, jsonObject.opt(s));
            }
            catch (final JSONException ex) {
                ((Throwable)ex).printStackTrace();
            }
        }
        return this.c;
    }
    
    public JSONObject c() {
        return this.a(i.a().a());
    }
    
    public JSONObject d() {
        try {
            this.c.put("device_id", (Object)i.c().a());
        }
        catch (final JSONException ex) {
            ((Throwable)ex).printStackTrace();
        }
        return this.c;
    }
    
    public JSONObject e() {
        try {
            final long f = i.a().f();
            if (f > 0L) {
                this.c.put("user_id", f);
            }
        }
        catch (final JSONException ex) {
            ((Throwable)ex).printStackTrace();
        }
        return this.c;
    }
    
    public JSONObject f() {
        return this.c;
    }
}

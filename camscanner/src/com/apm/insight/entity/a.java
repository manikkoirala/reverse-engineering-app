// 
// Decompiled by Procyon v0.6.0
// 

package com.apm.insight.entity;

import com.apm.insight.i;
import com.apm.insight.nativecrash.NativeImpl;
import com.apm.insight.l.l;
import com.apm.insight.l.q;
import java.util.List;
import com.apm.insight.runtime.a.b;
import java.util.Date;
import java.text.SimpleDateFormat;
import java.util.Locale;
import com.apm.insight.nativecrash.c;
import com.apm.insight.nativecrash.d;
import org.json.JSONException;
import org.json.JSONArray;
import com.apm.insight.l.o;
import java.util.Iterator;
import java.util.Map;
import com.apm.insight.l.v;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import android.content.Context;
import org.json.JSONObject;

public class a
{
    protected JSONObject a;
    protected Header b;
    
    public a() {
        this.a = new JSONObject();
    }
    
    public a(final JSONObject a) {
        this.a = a;
    }
    
    public static a a(final long l, final Context context, final String s) {
        final a a = new a();
        a.a("is_dart", 1);
        a.a("crash_time", l);
        a.a("process_name", (Object)com.apm.insight.l.a.c(context));
        a.a("data", (Object)s);
        com.apm.insight.l.a.a(context, a.h());
        return a;
    }
    
    public static a a(final long n, final Context context, @Nullable final Thread thread, @NonNull final Throwable t) {
        long currentTimeMillis = n;
        if (n == 0L) {
            currentTimeMillis = System.currentTimeMillis();
        }
        final a a = new a();
        a.a("isJava", 1);
        a.a("data", (Object)v.a(t));
        a.a("crash_time", currentTimeMillis);
        a.a("process_name", (Object)com.apm.insight.l.a.c(context));
        if (!com.apm.insight.l.a.b(context)) {
            a.a("remote_process", 1);
        }
        Object name;
        if (thread == null) {
            name = null;
        }
        else {
            name = thread.getName();
        }
        if (name != null) {
            a.a("crash_thread_name", name);
        }
        return a;
    }
    
    public static void a(final JSONObject jsonObject, final String s, final String s2, final String s3) {
        if (jsonObject == null) {
            return;
        }
        Label_0039: {
            JSONObject optJSONObject;
            if ((optJSONObject = jsonObject.optJSONObject(s)) != null) {
                break Label_0039;
            }
            try {
                optJSONObject = new JSONObject();
                jsonObject.put(s, (Object)optJSONObject);
                optJSONObject.put(s2, (Object)s3);
            }
            finally {}
        }
    }
    
    public static void a(final JSONObject jsonObject, final Throwable t) {
        String string = "npth_err_info";
        Label_0025: {
            if (jsonObject.opt("npth_err_info") != null) {
                break Label_0025;
            }
            try {
            Block_3_Outer:
                while (true) {
                    int n = 0;
                    while (true) {
                    Block_2:
                        while (true) {
                            jsonObject.put(string, (Object)v.a(t));
                            Label_0099: {
                                return;
                            }
                            iftrue(Label_0099:)(n >= 5);
                            break Block_2;
                            final StringBuilder sb = new StringBuilder();
                            sb.append("npth_err_info");
                            sb.append(n);
                            string = sb.toString();
                            continue Block_3_Outer;
                        }
                        final StringBuilder sb2 = new StringBuilder();
                        sb2.append("npth_err_info");
                        sb2.append(n);
                        iftrue(Label_0093:)(jsonObject.opt(sb2.toString()) != null);
                        continue;
                    }
                    Label_0093: {
                        ++n;
                    }
                    continue Block_3_Outer;
                    n = 0;
                    continue Block_3_Outer;
                }
            }
            finally {}
        }
    }
    
    public static void a(final JSONObject jsonObject, final Map<? extends String, ? extends String> map) {
        if (map == null) {
            return;
        }
        try {
            for (final Map.Entry<String, V> entry : map.entrySet()) {
                jsonObject.put((String)entry.getKey(), (Object)entry.getValue());
            }
        }
        finally {}
    }
    
    public static void a(final JSONObject jsonObject, final JSONObject jsonObject2) {
        try {
            jsonObject.put("storage", (Object)jsonObject2);
        }
        finally {}
        final long optLong = jsonObject2.optLong("inner_free");
        final long optLong2 = jsonObject2.optLong("sdcard_free");
        final long optLong3 = jsonObject2.optLong("inner_free_real");
        String s = "1M - 64M";
        String s2;
        if (optLong <= 1024L) {
            s2 = "0 - 1K";
        }
        else if (optLong <= 65536L) {
            s2 = "1K - 64K";
        }
        else if (optLong <= 524288L) {
            s2 = "64K - 512K";
        }
        else if (optLong <= 1048576L) {
            s2 = "512K - 1M";
        }
        else if (optLong <= 67108864L) {
            s2 = "1M - 64M";
        }
        else {
            s2 = "64M - ";
        }
        String s3;
        if (optLong3 <= 1024L) {
            s3 = "0 - 1K";
        }
        else if (optLong3 <= 65536L) {
            s3 = "1K - 64K";
        }
        else if (optLong3 <= 524288L) {
            s3 = "64K - 512K";
        }
        else if (optLong3 <= 1048576L) {
            s3 = "512K - 1M";
        }
        else if (optLong3 <= 67108864L) {
            s3 = "1M - 64M";
        }
        else {
            s3 = "64M - ";
        }
        if (optLong2 <= 1024L) {
            s = "0 - 1K";
        }
        else if (optLong2 <= 65536L) {
            s = "1K - 64K";
        }
        else if (optLong2 <= 524288L) {
            s = "64K - 512K";
        }
        else if (optLong2 <= 1048576L) {
            s = "512K - 1M";
        }
        else if (optLong2 > 67108864L) {
            s = "64M - ";
        }
        a(jsonObject, "filters", "inner_free", s2);
        a(jsonObject, "filters", "inner_free_real", s3);
        a(jsonObject, "filters", "sdcard_free", s);
    }
    
    public static boolean a(final String s) {
        return o.d(s).exists();
    }
    
    public static void b(final JSONObject jsonObject, final JSONObject jsonObject2) {
        if (jsonObject != null && jsonObject2 != null) {
            if (jsonObject2.length() > 0) {
                try {
                    final Iterator keys = jsonObject2.keys();
                    while (keys.hasNext()) {
                        final String s = keys.next();
                        final Object opt = jsonObject.opt(s);
                        Label_0059: {
                            if (opt != null) {
                                JSONObject jsonObject3 = null;
                                JSONObject jsonObject4 = null;
                                Label_0186: {
                                    if (opt instanceof JSONObject) {
                                        jsonObject3 = jsonObject.getJSONObject(s);
                                        jsonObject4 = jsonObject2.getJSONObject(s);
                                    }
                                    else {
                                        if (!(opt instanceof JSONArray)) {
                                            break Label_0059;
                                        }
                                        final JSONArray optJSONArray = jsonObject2.optJSONArray(s);
                                        if (optJSONArray != null) {
                                            final JSONArray jsonArray = (JSONArray)opt;
                                            final int length = jsonArray.length();
                                            int i;
                                            final int n = i = 0;
                                            if (length == 1) {
                                                i = n;
                                                if (jsonArray.opt(0) instanceof JSONObject) {
                                                    i = n;
                                                    if (optJSONArray.opt(0) instanceof JSONObject) {
                                                        jsonObject3 = jsonArray.getJSONObject(0);
                                                        jsonObject4 = optJSONArray.getJSONObject(0);
                                                        break Label_0186;
                                                    }
                                                }
                                            }
                                            while (i < optJSONArray.length()) {
                                                jsonArray.put(optJSONArray.get(i));
                                                ++i;
                                            }
                                            continue;
                                        }
                                        continue;
                                    }
                                }
                                b(jsonObject3, jsonObject4);
                                continue;
                            }
                        }
                        jsonObject.put(s, jsonObject2.opt(s));
                    }
                }
                catch (final JSONException ex) {
                    ((Throwable)ex).printStackTrace();
                }
            }
        }
    }
    
    public static boolean b(final String s) {
        return d.c(s) > c.i();
    }
    
    public static boolean c(final String s) {
        return d.a(s) > 960;
    }
    
    public static boolean d(final String s) {
        return d.b(s) > 350;
    }
    
    public a a(final int n, final String s) {
        try {
            this.a.put("miniapp_id", n);
            this.a.put("miniapp_version", (Object)s);
        }
        catch (final JSONException ex) {
            ((Throwable)ex).printStackTrace();
        }
        return this;
    }
    
    public a a(final long n) {
        try {
            this.a("app_start_time", n);
            this.a("app_start_time_readable", (Object)new SimpleDateFormat("yyyy_MM_dd_HH_mm_ss", Locale.getDefault()).format(new Date(n)));
        }
        catch (final Exception ex) {
            ex.printStackTrace();
        }
        return this;
    }
    
    public a a(final Header b) {
        this.a("header", b.f());
        this.b = b;
        return this;
    }
    
    public a a(final b b) {
        this.a("activity_trace", b.g());
        this.a("activity_track", b.i());
        return this;
    }
    
    public a a(final String s, final String s2) {
        final Object opt = this.h().opt("data");
        JSONObject jsonObject;
        if (opt instanceof JSONArray) {
            jsonObject = ((JSONArray)opt).optJSONObject(0);
        }
        else {
            jsonObject = this.h();
        }
        a(jsonObject, "filters", s, s2);
        return this;
    }
    
    public a a(final String s, final JSONArray jsonArray) {
        JSONObject optJSONObject;
        if ((optJSONObject = this.h().optJSONObject("custom_long")) == null) {
            optJSONObject = new JSONObject();
            this.a("custom_long", optJSONObject);
        }
        try {
            optJSONObject.put(s, (Object)jsonArray);
            return this;
        }
        catch (final JSONException ex) {
            return this;
        }
    }
    
    public a a(final List<String> list) {
        final JSONArray jsonArray = new JSONArray();
        if (list != null && !list.isEmpty()) {
            final Iterator iterator = list.iterator();
            while (iterator.hasNext()) {
                jsonArray.put((Object)iterator.next());
            }
            this.a("patch_info", (Object)jsonArray);
            return this;
        }
        this.a("patch_info", (Object)jsonArray);
        return this;
    }
    
    public a a(final Map<String, Integer> map) {
        final JSONArray jsonArray = new JSONArray();
        Label_0026: {
            if (map != null) {
                break Label_0026;
            }
            try {
                this.a.put("plugin_info", (Object)jsonArray);
                return this;
                final Iterator<String> iterator = map.keySet().iterator();
                while (true) {
                    Label_0039: {
                        break Label_0039;
                        final String s = iterator.next();
                        final JSONObject jsonObject = new JSONObject();
                        jsonObject.put("package_name", (Object)s);
                        jsonObject.put("version_code", (Object)map.get(s));
                        jsonArray.put((Object)jsonObject);
                        break Label_0039;
                        Label_0106: {
                            this.a.put("plugin_info", (Object)jsonArray);
                        }
                        return this;
                    }
                    iftrue(Label_0106:)(!iterator.hasNext());
                    continue;
                }
            }
            catch (final Exception ex) {
                return this;
            }
        }
    }
    
    public a a(final JSONObject jsonObject) {
        this.a("header", jsonObject);
        return this;
    }
    
    public void a(@NonNull final String s, @Nullable final Object o) {
        try {
            this.a.put(s, o);
        }
        catch (final Exception ex) {
            q.b(ex);
        }
    }
    
    public boolean a() {
        final Object opt = this.h().opt("data");
        JSONObject jsonObject;
        if (opt instanceof JSONArray) {
            jsonObject = ((JSONArray)opt).optJSONObject(0);
        }
        else {
            jsonObject = this.a;
        }
        return l.a(jsonObject, "logcat") ^ true;
    }
    
    public a b(final String s, final String s2) {
        final Object opt = this.h().opt("data");
        JSONObject jsonObject;
        if (opt instanceof JSONArray) {
            jsonObject = ((JSONArray)opt).optJSONObject(0);
        }
        else {
            jsonObject = this.h();
        }
        a(jsonObject, "custom", s, s2);
        return this;
    }
    
    public a b(final Map<Integer, String> map) {
        if (map != null && map.size() > 0) {
            final JSONObject jsonObject = new JSONObject();
            for (final Integer obj : map.keySet()) {
                try {
                    jsonObject.put(String.valueOf(obj), map.get(obj));
                }
                catch (final JSONException ex) {
                    q.b((Throwable)ex);
                }
            }
            try {
                this.a.put("sdk_info", (Object)jsonObject);
            }
            catch (final JSONException ex2) {
                ((Throwable)ex2).printStackTrace();
            }
        }
        return this;
    }
    
    public a b(final JSONObject jsonObject) {
        a(this.a, jsonObject);
        return this;
    }
    
    public void b() {
        this.a("has_logcat", String.valueOf(this.a()));
    }
    
    public a c(Map<? extends String, ? extends String> iterator) {
        if (iterator != null) {
            final JSONObject e = this.e("filters");
            iterator = ((Map<? extends String, ? extends String>)iterator).entrySet().iterator();
            while (iterator.hasNext()) {
                final Map.Entry entry = iterator.next();
                try {
                    e.put((String)entry.getKey(), entry.getValue());
                }
                catch (final JSONException ex) {}
            }
            this.a("filters", e);
        }
        return this;
    }
    
    public void c() {
        this.a("is_64_devices", String.valueOf(Header.a()));
        this.a("is_64_runtime", String.valueOf(NativeImpl.e()));
        this.a("is_x86_devices", String.valueOf(Header.b()));
    }
    
    public void c(@NonNull final JSONObject jsonObject) {
        b(this.a, jsonObject);
    }
    
    public boolean d() {
        return c(i.f());
    }
    
    public JSONObject e(final String s) {
        final Object opt = this.h().opt("data");
        JSONObject jsonObject;
        if (opt instanceof JSONArray) {
            jsonObject = ((JSONArray)opt).optJSONObject(0);
        }
        else {
            jsonObject = this.h();
        }
        if (jsonObject == null) {
            return new JSONObject();
        }
        JSONObject optJSONObject;
        if ((optJSONObject = jsonObject.optJSONObject(s)) == null) {
            optJSONObject = new JSONObject();
            this.a(s, optJSONObject);
        }
        return optJSONObject;
    }
    
    public boolean e() {
        return d(i.f());
    }
    
    public boolean f() {
        return b(i.f());
    }
    
    public boolean g() {
        return a(i.f());
    }
    
    public JSONObject h() {
        return this.a;
    }
    
    public Header i() {
        if (this.b == null) {
            this.a(this.b = new Header(i.g()));
        }
        return this.b;
    }
}

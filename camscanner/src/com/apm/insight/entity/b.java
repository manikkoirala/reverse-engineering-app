// 
// Decompiled by Procyon v0.6.0
// 

package com.apm.insight.entity;

import java.util.ArrayList;
import java.util.List;
import org.json.JSONException;
import java.io.IOException;
import com.apm.insight.l.i;
import com.apm.insight.l.q;
import com.apm.insight.l.v;
import androidx.annotation.Nullable;
import android.text.TextUtils;
import com.apm.insight.l.l;
import com.apm.insight.runtime.a;
import org.json.JSONObject;
import com.apm.insight.CrashType;
import androidx.annotation.NonNull;
import java.util.Iterator;
import org.json.JSONArray;
import java.io.File;
import java.util.concurrent.ConcurrentHashMap;
import com.apm.insight.h;
import java.util.concurrent.ConcurrentLinkedQueue;

public final class b
{
    private static ConcurrentLinkedQueue<h> a;
    private static ConcurrentHashMap<Integer, h> b;
    
    static {
        com.apm.insight.entity.b.a = new ConcurrentLinkedQueue<h>();
        com.apm.insight.entity.b.b = new ConcurrentHashMap<Integer, h>();
    }
    
    public static File a(final File parent) {
        return new File(parent, "all_data.json");
    }
    
    @NonNull
    public static JSONArray a() {
        final JSONArray jsonArray = new JSONArray();
        for (final h h : com.apm.insight.entity.b.a) {
            if (h == null) {
                break;
            }
            jsonArray.put((Object)h.c());
        }
        return jsonArray;
    }
    
    @NonNull
    public static JSONArray a(final Object o) {
        final JSONArray jsonArray = new JSONArray();
        for (final h h : com.apm.insight.entity.b.a) {
            if (h != null && h.a(o)) {
                jsonArray.put((Object)h.a(CrashType.JAVA, null));
                break;
            }
        }
        return jsonArray;
    }
    
    public static JSONArray a(Object o, final Throwable t, final StackTraceElement[] array) {
        Label_0091: {
            Block_3: {
                for (final h h : com.apm.insight.entity.b.a) {
                    if (h != null && h.a(o)) {
                        break Block_3;
                    }
                }
                break Label_0091;
            }
            final h h;
            final JSONArray a = h.a(array, t);
            o = new JSONArray();
            try {
                final JSONObject jsonObject = new JSONObject();
                jsonObject.put("aid", (Object)h.b());
                jsonObject.put("lines", (Object)a);
                ((JSONArray)o).put((Object)jsonObject);
                return (JSONArray)o;
                return null;
            }
            finally {
                return (JSONArray)o;
            }
        }
    }
    
    @NonNull
    public static JSONArray a(final String s) {
        final JSONArray jsonArray = new JSONArray();
        final String[] split = s.split("\n");
        for (final h h : com.apm.insight.entity.b.a) {
            if (h == null) {
                break;
            }
            if (!com.apm.insight.runtime.a.b(h.b())) {
                continue;
            }
            final JSONArray a = h.a(split);
            if (l.a(a)) {
                continue;
            }
            jsonArray.put((Object)h.a(CrashType.ANR, a));
        }
        return jsonArray;
    }
    
    @NonNull
    public static JSONArray a(final String s, final String s2, final JSONArray jsonArray) {
        final JSONArray jsonArray2 = new JSONArray();
        for (int i = 0; i < jsonArray.length(); ++i) {
            final JSONObject optJSONObject = jsonArray.optJSONObject(i);
            if (optJSONObject != null) {
                final JSONObject optJSONObject2 = optJSONObject.optJSONObject("header");
                if (optJSONObject2 != null) {
                    if (com.apm.insight.runtime.a.c(String.valueOf(optJSONObject2.opt("aid")))) {
                        if (!TextUtils.isEmpty((CharSequence)optJSONObject2.optString("package"))) {
                            if (!a(s, optJSONObject2.optJSONArray("so_list"), s2, optJSONObject2.optJSONArray("so_list"))) {
                                continue;
                            }
                        }
                        jsonArray2.put((Object)optJSONObject);
                    }
                }
            }
        }
        return jsonArray2;
    }
    
    @Nullable
    public static JSONArray a(final Throwable t, final Thread thread, @Nullable final File parent) {
        final JSONArray jsonArray = new JSONArray();
        final StackTraceElement[] b = v.b(t);
        final Iterator<h> iterator = com.apm.insight.entity.b.a.iterator();
        while (true) {
            final boolean hasNext = iterator.hasNext();
            String name = null;
            if (!hasNext) {
                break;
            }
            final h h = iterator.next();
            if (h == null) {
                break;
            }
            if (!com.apm.insight.runtime.a.a(h.b())) {
                final StringBuilder sb = new StringBuilder();
                sb.append("not enable javaCrash aid: ");
                sb.append(h.b());
                q.a((Object)sb.toString());
            }
            else {
                if (thread != null) {
                    name = thread.getName();
                }
                final JSONArray a = h.a(b, t, name);
                if (l.a(a)) {
                    continue;
                }
                jsonArray.put((Object)h.a(CrashType.JAVA, a));
            }
        }
        if (l.a(jsonArray)) {
            return null;
        }
        if (parent == null) {
            return jsonArray;
        }
        try {
            i.a(new File(parent, "all_data.json"), jsonArray, false);
            return jsonArray;
        }
        catch (final IOException ex) {
            return jsonArray;
        }
    }
    
    public static void a(@NonNull final h h) {
        com.apm.insight.entity.b.a.add(h);
        if (h.d()) {
            com.apm.insight.entity.b.b.put(4444, h);
        }
    }
    
    public static void a(final JSONObject jsonObject, JSONArray obj, final a a) {
        final StringBuilder sb = new StringBuilder();
        sb.append("uploadFromFile with allData ");
        sb.append(obj);
        q.a((Object)sb.toString());
        final JSONArray jsonArray = new JSONArray();
        for (int i = 0; i < obj.length(); ++i) {
            final JSONObject optJSONObject = obj.optJSONObject(i);
            if (optJSONObject == null) {
                break;
            }
            if (l.a(optJSONObject, 0, "header", "single_upload") == 1) {
                final JSONObject jsonObject2 = new JSONObject();
                a.b(jsonObject2, jsonObject);
                a.b(jsonObject2, optJSONObject);
                a.a(jsonObject2);
            }
            else {
                jsonArray.put((Object)optJSONObject);
            }
        }
        if (jsonArray.length() == 0) {
            return;
        }
        obj = (JSONArray)new JSONObject();
        a.b((JSONObject)obj, jsonObject);
        try {
            ((JSONObject)obj).put("all_data", (Object)jsonArray);
        }
        catch (final JSONException ex) {
            ((Throwable)ex).printStackTrace();
        }
        a.a((JSONObject)obj);
    }
    
    public static boolean a(String optString, final JSONArray jsonArray, final String s, final JSONArray jsonArray2) {
        if (!l.a(jsonArray)) {
            for (int i = 0; i < jsonArray.length(); ++i) {
                if (optString.contains(jsonArray.optString(i))) {
                    return true;
                }
            }
        }
        if (!l.a(jsonArray2)) {
            for (int j = 0; j < jsonArray2.length(); ++j) {
                optString = jsonArray2.optString(j);
                if (optString != null && optString.contains(s)) {
                    return true;
                }
            }
        }
        return false;
    }
    
    @Nullable
    public static String b(final Object o) {
        for (final h h : com.apm.insight.entity.b.a) {
            if (h != null && h.a(o)) {
                return h.b();
            }
        }
        return null;
    }
    
    @NonNull
    public static JSONArray b() {
        final JSONArray jsonArray = new JSONArray();
        for (final h h : com.apm.insight.entity.b.a) {
            if (h == null) {
                break;
            }
            jsonArray.put((Object)h.a((CrashType)null));
        }
        return jsonArray;
    }
    
    public static int c() {
        return com.apm.insight.entity.b.a.size();
    }
    
    public static List<String> d() {
        final ArrayList list = new ArrayList();
        for (final h h : com.apm.insight.entity.b.a) {
            if (h == null) {
                break;
            }
            list.add(h.b());
        }
        return list;
    }
    
    public interface a
    {
        void a(final JSONObject p0);
    }
}

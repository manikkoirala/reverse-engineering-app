// 
// Decompiled by Procyon v0.6.0
// 

package com.apm.insight.entity;

import com.apm.insight.l.l;
import java.util.Iterator;
import org.json.JSONException;
import java.io.IOException;
import com.apm.insight.i;
import android.os.Environment;
import org.json.JSONObject;

public class d
{
    private static String a;
    private static JSONObject b;
    
    private static void a() {
        if (d.a == null) {
            final StringBuilder sb = new StringBuilder();
            sb.append(Environment.getExternalStorageDirectory().getAbsolutePath());
            sb.append("/Android/data/");
            sb.append(i.g().getPackageName());
            sb.append("/AutomationTestInfo.json");
            d.a = sb.toString();
        }
        if (d.b == null) {
            JSONObject b;
            try {
                d.b = new JSONObject(com.apm.insight.l.i.c(d.a));
                return;
            }
            catch (final IOException ex) {
                b = new JSONObject();
            }
            catch (final JSONException ex2) {
                b = new JSONObject();
            }
            d.b = b;
        }
    }
    
    public static void a(final JSONObject jsonObject) {
        if (jsonObject == null) {
            return;
        }
        try {
            c(jsonObject);
        }
        finally {}
    }
    
    public static void b(final JSONObject jsonObject) {
        if (jsonObject == null) {
            return;
        }
        try {
            d(jsonObject);
        }
        finally {}
    }
    
    private static void c(final JSONObject jsonObject) {
        a();
        final JSONObject b = d.b;
        if (b == null) {
            return;
        }
        final Iterator keys = b.keys();
        while (keys.hasNext()) {
            final String anObject = keys.next();
            if ("slardar_filter".equals(anObject)) {
                continue;
            }
            final Object opt = d.b.opt(anObject);
            if (opt == null) {
                continue;
            }
            try {
                jsonObject.put(anObject, opt);
            }
            catch (final JSONException ex) {}
        }
    }
    
    private static void d(final JSONObject jsonObject) {
        a();
        final JSONObject b = d.b;
        if (b == null) {
            return;
        }
        final JSONObject optJSONObject = b.optJSONObject("slardar_filter");
        if (l.a(optJSONObject)) {
            return;
        }
        while (true) {
            JSONObject optJSONObject2;
            if ((optJSONObject2 = jsonObject.optJSONObject("filters")) != null) {
                break Label_0056;
            }
            optJSONObject2 = new JSONObject();
            try {
                jsonObject.put("filters", (Object)optJSONObject2);
                com.apm.insight.entity.a.b(optJSONObject2, optJSONObject);
            }
            catch (final JSONException ex) {
                continue;
            }
            break;
        }
    }
}

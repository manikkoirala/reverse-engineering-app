// 
// Decompiled by Procyon v0.6.0
// 

package com.apm.insight.f;

import com.apm.insight.l.q;
import com.apm.insight.k.g;
import com.apm.insight.entity.a;
import com.apm.insight.CrashType;
import com.apm.insight.runtime.a.f;
import android.text.TextUtils;
import java.util.Iterator;
import org.json.JSONObject;
import com.apm.insight.entity.c;
import com.apm.insight.runtime.p;
import androidx.annotation.NonNull;
import java.util.Map;
import androidx.annotation.Nullable;
import com.apm.insight.l.v;

public final class b
{
    @Nullable
    private static String a(final StackTraceElement[] array, int i) {
        if (array != null && array.length > 0) {
            final StringBuilder sb = new StringBuilder();
            while (i < array.length) {
                v.a(array[i], sb);
                ++i;
            }
            return sb.toString();
        }
        return null;
    }
    
    public static void a(final Object o, final Throwable t, final String s, final boolean b, final Map<String, String> map, final String s2, @NonNull final String s3) {
        try {
            p.b().a(new Runnable(o, t, s, b, map, s2, s3) {
                final Object a;
                final Throwable b;
                final String c;
                final boolean d;
                final Map e;
                final String f;
                final String g;
                
                @Override
                public void run() {
                    c(this.a, this.b, this.c, this.d, this.e, this.f, this.g);
                }
            });
        }
        finally {}
    }
    
    public static void a(final Throwable t, final String s, final boolean b) {
        a(t, s, b, "core_exception_monitor");
    }
    
    public static void a(final Throwable t, final String s, final boolean b, @NonNull final String s2) {
        a(t, s, b, null, s2);
    }
    
    public static void a(final Throwable t, final String s, final boolean b, final Map<String, String> map, @NonNull final String s2) {
        try {
            p.b().a(new Runnable(t, s, b, map, s2) {
                final Throwable a;
                final String b;
                final boolean c;
                final Map d;
                final String e;
                
                @Override
                public void run() {
                    b(null, this.a, this.b, this.c, this.d, this.e);
                }
            });
        }
        finally {}
    }
    
    private static void a(final Map<String, String> map, final c c) {
        try {
            final JSONObject jsonObject = new JSONObject();
            if (map != null) {
                for (final String s : map.keySet()) {
                    jsonObject.put(s, (Object)map.get(s));
                }
                c.a("custom", jsonObject);
            }
        }
        finally {}
    }
    
    public static void a(final StackTraceElement[] array, final int n, @Nullable final String s, final String s2, final Map<String, String> map) {
        try {
            p.b().a(new Runnable(array, n, s, s2, map) {
                final StackTraceElement[] a;
                final int b;
                final String c;
                final String d;
                final Map e;
                
                @Override
                public void run() {
                    b(this.a, this.b, this.c, this.d, "core_exception_monitor", this.e);
                }
            });
        }
        finally {}
    }
    
    private static void b(final Object o, final Throwable t, final String s, final boolean b, final Map<String, String> map, @NonNull final String s2) {
        c(o, t, s, b, map, "EnsureNotReachHere", s2);
    }
    
    private static void b(final StackTraceElement[] array, final int n, @Nullable final String str, final String s, @NonNull final String s2, final Map<String, String> map) {
        if (array != null) {
            try {
                if (array.length > n + 1) {
                    final StackTraceElement stackTraceElement = array[n];
                    if (stackTraceElement == null) {
                        return;
                    }
                    final String a = a(array, n);
                    if (TextUtils.isEmpty((CharSequence)a)) {
                        return;
                    }
                    final c a2 = c.a(stackTraceElement, a, str, Thread.currentThread().getName(), true, s, s2);
                    a(map, a2);
                    f.a().a(CrashType.ENSURE, a2);
                    g.a(a2);
                    final StringBuilder sb = new StringBuilder();
                    sb.append("[report] ");
                    sb.append(str);
                    q.b(sb.toString());
                }
            }
            finally {
                final Throwable t;
                q.b(t);
            }
        }
    }
    
    private static void c(Object o, final Throwable t, final String str, final boolean b, final Map<String, String> map, final String s, @NonNull final String s2) {
        if (t == null) {
            return;
        }
        try {
            final StackTraceElement[] stackTrace = t.getStackTrace();
            final StackTraceElement stackTraceElement = stackTrace[0];
            if (stackTraceElement == null) {
                return;
            }
            final String a = v.a(t);
            if (TextUtils.isEmpty((CharSequence)a)) {
                return;
            }
            final c a2 = c.a(stackTraceElement, a, str, Thread.currentThread().getName(), b, s, s2);
            if (o != null) {
                a2.a("exception_line_num", (Object)b.a(o, t, stackTrace));
            }
            a(map, a2);
            f.a().a(CrashType.ENSURE, a2);
            g.a(o, a2);
            o = new StringBuilder();
            ((StringBuilder)o).append("[reportException] ");
            ((StringBuilder)o).append(str);
            q.b(((StringBuilder)o).toString());
        }
        finally {
            final Throwable t2;
            q.b(t2);
        }
    }
}

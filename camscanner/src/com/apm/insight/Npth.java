// 
// Decompiled by Procyon v0.6.0
// 

package com.apm.insight;

import android.content.ContextWrapper;
import com.apm.insight.k.h;
import com.apm.insight.runtime.j;
import com.apm.insight.l.a;
import androidx.annotation.Nullable;
import com.apm.insight.l.r;
import android.content.Context;
import androidx.annotation.NonNull;
import android.app.Application;
import com.apm.insight.runtime.ConfigManager;
import com.apm.insight.a.c;
import com.apm.insight.a.b;
import com.apm.insight.runtime.o;
import java.util.Map;

public final class Npth
{
    private static boolean sInit;
    
    public static void addAttachLongUserData(final AttachUserData attachUserData, final CrashType crashType) {
        if (attachUserData != null) {
            i.b().b(attachUserData, crashType);
        }
    }
    
    public static void addAttachUserData(final AttachUserData attachUserData, final CrashType crashType) {
        if (attachUserData != null) {
            i.b().a(attachUserData, crashType);
        }
    }
    
    public static void addTags(final Map<? extends String, ? extends String> map) {
        if (map != null && !map.isEmpty()) {
            i.b().a(map);
        }
    }
    
    public static void checkInnerNpth(final boolean b) {
        o.c(b);
    }
    
    public static void dumpHprof(final String s) {
        o.c(s);
    }
    
    public static void enableALogCollector(final String s, final b b, final c c) {
        o.a(s, b, c);
    }
    
    public static void enableAnrInfo(final boolean b) {
        o.b(b);
    }
    
    public static void enableLoopMonitor(final boolean b) {
        o.a(b);
    }
    
    public static void enableNativeDump(final boolean b) {
        o.d(b);
    }
    
    public static void enableThreadsBoost() {
        i.a(1);
    }
    
    public static ConfigManager getConfigManager() {
        return i.i();
    }
    
    public static boolean hasCrash() {
        return o.k();
    }
    
    public static boolean hasCrashWhenJavaCrash() {
        return o.l();
    }
    
    public static boolean hasCrashWhenNativeCrash() {
        return o.m();
    }
    
    public static void init(@NonNull final Application application, @NonNull final Context context, @NonNull final ICommonParams commonParams, final boolean b, final boolean b2, final boolean b3, final boolean b4, final long n) {
        synchronized (Npth.class) {
            if (Npth.sInit) {
                return;
            }
            o.a(application, context, Npth.sInit = true, true, true, true, n);
            i.a(application, context, commonParams);
            final Map<String, Object> a = i.a().a();
            final MonitorCrash init = MonitorCrash.init(context, String.valueOf(r.a(a.get("aid"), 4444)), r.a(a.get("update_version_code"), 0), String.valueOf(a.get("app_version")));
            if (init != null) {
                init.config().setDeviceId(i.a().d()).setChannel(String.valueOf(a.get("channel")));
            }
        }
    }
    
    public static void init(@NonNull final Context context, @NonNull final ICommonParams commonParams) {
        synchronized (Npth.class) {
            init(context, commonParams, true, false, false);
        }
    }
    
    public static void init(@NonNull final Context context, @NonNull final ICommonParams commonParams, final boolean b, final boolean b2, final boolean b3) {
        synchronized (Npth.class) {
            init(context, commonParams, b, b, b2, b3);
        }
    }
    
    public static void init(@NonNull final Context context, @NonNull final ICommonParams commonParams, final boolean b, final boolean b2, final boolean b3, final boolean b4) {
        synchronized (Npth.class) {
            init(context, commonParams, b, b2, b3, b4, 0L);
        }
    }
    
    public static void init(@NonNull final Context context, @NonNull final ICommonParams commonParams, final boolean b, final boolean b2, final boolean b3, final boolean b4, final long n) {
        Context baseContext = context;
        synchronized (Npth.class) {
            while (true) {
                if (i.h() != null) {
                    final Object h = i.h();
                    break Label_0016;
                }
                Label_0055: {
                    if (!(baseContext instanceof Application)) {
                        break Label_0055;
                    }
                    Object h = baseContext;
                    if (((ContextWrapper)h).getBaseContext() != null) {
                        break Label_0016;
                    }
                    throw new IllegalArgumentException("The Application passed in when init has not been attached, please pass a attachBaseContext as param and call Npth.setApplication(Application) before init.");
                    Label_0093: {
                        break Label_0093;
                        try {
                            final Application application = (Application)context.getApplicationContext();
                            if (application == null) {
                                throw new IllegalArgumentException("Can not get the Application instance since a baseContext was passed in when init, please call Npth.setApplication(Application) before init.");
                            }
                            if (((ContextWrapper)application).getBaseContext() != null) {
                                baseContext = ((ContextWrapper)application).getBaseContext();
                                continue;
                            }
                            continue;
                            init((Application)h, baseContext, commonParams, b, b2, b3, b4, n);
                        }
                        finally {
                            h = new IllegalArgumentException("Can not get the Application instance since a baseContext was passed in when init, please call Npth.setApplication(Application) before init.");
                        }
                    }
                }
                break;
            }
        }
    }
    
    public static void initMiniApp(@NonNull final Context context, @NonNull final ICommonParams commonParams) {
        synchronized (Npth.class) {
            i.a(true);
            init(context, commonParams, true, false, true, true);
        }
    }
    
    public static void initMiniApp(@NonNull final Context context, @NonNull final ICommonParams commonParams, final int n, final String s) {
        synchronized (Npth.class) {
            i.a(true);
            i.b(n, s);
            init(context, commonParams, true, true, true, true);
        }
    }
    
    public static boolean isANREnable() {
        return o.c();
    }
    
    public static boolean isInit() {
        return Npth.sInit;
    }
    
    public static boolean isJavaCrashEnable() {
        return o.b();
    }
    
    public static boolean isNativeCrashEnable() {
        return o.d();
    }
    
    public static boolean isRunning() {
        return o.i();
    }
    
    public static boolean isStopUpload() {
        return o.n();
    }
    
    public static void openANRMonitor() {
        o.g();
    }
    
    public static void openJavaCrashMonitor() {
        o.f();
    }
    
    public static boolean openNativeCrashMonitor() {
        return o.h();
    }
    
    public static void registerCrashCallback(final ICrashCallback crashCallback, final CrashType crashType) {
        o.a(crashCallback, crashType);
    }
    
    public static void registerOOMCallback(final IOOMCallback ioomCallback) {
        o.a(ioomCallback);
    }
    
    public static void registerSdk(final int n, final String s) {
        i.a(n, s);
    }
    
    public static void removeAttachLongUserData(final AttachUserData attachUserData, final CrashType crashType) {
        if (attachUserData != null) {
            i.b().b(crashType, attachUserData);
        }
    }
    
    public static void removeAttachUserData(final AttachUserData attachUserData, final CrashType crashType) {
        if (attachUserData != null) {
            i.b().a(crashType, attachUserData);
        }
    }
    
    public static void reportDartError(final String s) {
        o.a(s);
    }
    
    public static void reportDartError(final String s, @Nullable final Map<? extends String, ? extends String> map, @Nullable final Map<String, String> map2, @Nullable final g g) {
        o.a(s, map, map2, g);
    }
    
    public static void reportDartError(final String s, @Nullable final Map<? extends String, ? extends String> map, @Nullable final Map<String, String> map2, @Nullable final Map<String, String> map3, @Nullable final g g) {
        o.a(s, map, map2, map3, g);
    }
    
    @Deprecated
    public static void reportError(final String s) {
        o.b(s);
    }
    
    @Deprecated
    public static void reportError(@NonNull final Throwable t) {
        o.a(t);
    }
    
    public static void setAlogFlushAddr(final long n) {
        o.a(n);
    }
    
    public static void setAlogFlushV2Addr(final long n) {
        o.b(n);
    }
    
    public static void setAlogLogDirAddr(final long n) {
        o.c(n);
    }
    
    public static void setAlogWriteAddr(final long n) {
    }
    
    public static void setAnrInfoFileObserver(final String s, final f f) {
        o.a(s, f);
    }
    
    public static void setApplication(final Application application) {
        i.a(application);
    }
    
    @Deprecated
    public static void setAttachUserData(final AttachUserData attachUserData, final CrashType crashType) {
        if (attachUserData != null) {
            i.b().a(attachUserData, crashType);
        }
    }
    
    public static void setBusiness(final String s) {
        if (s != null) {
            i.a(s);
        }
    }
    
    public static void setCrashFilter(final ICrashFilter crashFilter) {
        i.b().a(crashFilter);
    }
    
    public static void setCurProcessName(final String s) {
        a.a(s);
    }
    
    public static void setEncryptImpl(@NonNull final e e) {
        o.a(e);
    }
    
    public static void setLogcatImpl(final j j) {
        o.a(j);
    }
    
    public static void setRequestIntercept(final h h) {
        o.a(h);
    }
    
    public static void stopAnr() {
        o.j();
    }
    
    public static void stopUpload() {
        o.o();
    }
    
    public static void unregisterCrashCallback(final ICrashCallback crashCallback, final CrashType crashType) {
        o.b(crashCallback, crashType);
    }
    
    public static void unregisterOOMCallback(final IOOMCallback ioomCallback, final CrashType crashType) {
        o.a(ioomCallback, crashType);
    }
}

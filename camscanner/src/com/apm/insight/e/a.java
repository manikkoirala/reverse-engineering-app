// 
// Decompiled by Procyon v0.6.0
// 

package com.apm.insight.e;

import com.apm.insight.l.q;
import android.content.Context;
import com.apm.insight.i;
import android.database.sqlite.SQLiteDatabase;
import com.apm.insight.e.b.b;

public class a
{
    private static volatile a a;
    private b b;
    private SQLiteDatabase c;
    
    private a() {
    }
    
    public static a a() {
        if (com.apm.insight.e.a.a == null) {
            synchronized (a.class) {
                if (com.apm.insight.e.a.a == null) {
                    com.apm.insight.e.a.a = new a();
                }
            }
        }
        return com.apm.insight.e.a.a;
    }
    
    private void b() {
        if (this.b == null) {
            this.a(i.g());
        }
    }
    
    public void a(final Context context) {
        monitorenter(this);
        try {
            this.c = new com.apm.insight.e.b(context).getWritableDatabase();
        }
        finally {
            final Throwable t;
            q.b(t);
        }
        try {
            this.b = new b();
        }
        finally {
            monitorexit(this);
        }
    }
    
    public void a(final com.apm.insight.e.a.a a) {
        synchronized (this) {
            this.b();
            final b b = this.b;
            if (b != null) {
                b.a(this.c, a);
            }
        }
    }
    
    public boolean a(final String s) {
        synchronized (this) {
            this.b();
            final b b = this.b;
            return b != null && b.a(this.c, s);
        }
    }
}

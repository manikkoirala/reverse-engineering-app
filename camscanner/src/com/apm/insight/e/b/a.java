// 
// Decompiled by Procyon v0.6.0
// 

package com.apm.insight.e.b;

import com.apm.insight.l.q;
import java.util.Iterator;
import com.apm.insight.c;
import android.database.sqlite.SQLiteDatabase;
import java.util.HashMap;
import android.content.ContentValues;

public abstract class a<T>
{
    protected final String a;
    protected final String b;
    
    protected a(final String b) {
        this.a = "_id";
        this.b = b;
    }
    
    protected abstract ContentValues a(final T p0);
    
    protected abstract HashMap<String, String> a();
    
    public void a(final SQLiteDatabase sqLiteDatabase) {
        try {
            final StringBuilder sb = new StringBuilder();
            sb.append("CREATE TABLE ");
            sb.append(this.b);
            sb.append(" (_id INTEGER PRIMARY KEY AUTOINCREMENT, ");
            final HashMap<String, String> a = this.a();
            if (a != null) {
                for (final String s : a.keySet()) {
                    sb.append(s);
                    sb.append(" ");
                    sb.append(a.get(s));
                    sb.append(",");
                }
                sb.delete(sb.length() - 1, sb.length());
                sb.append(")");
                sqLiteDatabase.execSQL(sb.toString());
            }
        }
        finally {
            final Throwable t;
            c.a().a("NPTH_CATCH", t);
        }
    }
    
    public void a(final SQLiteDatabase sqLiteDatabase, final T t) {
        if (sqLiteDatabase != null && t != null) {
            try {
                sqLiteDatabase.insert(this.b, (String)null, this.a(t));
            }
            finally {
                final Throwable t2;
                q.b(t2);
            }
        }
    }
}

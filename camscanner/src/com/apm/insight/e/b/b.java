// 
// Decompiled by Procyon v0.6.0
// 

package com.apm.insight.e.b;

import android.database.Cursor;
import android.text.TextUtils;
import com.apm.insight.l.q;
import android.database.sqlite.SQLiteDatabase;
import java.util.HashMap;
import android.content.ContentValues;

public class b extends a<com.apm.insight.e.a.a>
{
    public b() {
        super("duplicatelog");
    }
    
    @Override
    protected ContentValues a(final com.apm.insight.e.a.a a) {
        final ContentValues contentValues = new ContentValues();
        contentValues.put("path", a.a);
        contentValues.put("insert_time", Long.valueOf(a.b));
        return contentValues;
    }
    
    @Override
    protected HashMap<String, String> a() {
        final HashMap hashMap = new HashMap();
        hashMap.put("path", "TEXT");
        hashMap.put("insert_time", "INTEGER");
        hashMap.put("ext1", "TEXT");
        hashMap.put("ext2", "TEXT");
        return hashMap;
    }
    
    @Override
    public void a(final SQLiteDatabase sqLiteDatabase, final com.apm.insight.e.a.a a) {
        if (a != null) {
            if (!this.a(sqLiteDatabase, a.a)) {
                super.a(sqLiteDatabase, a);
                try {
                    final StringBuilder sb = new StringBuilder();
                    sb.append("delete from ");
                    sb.append(super.b);
                    sb.append(" where ");
                    sb.append("_id");
                    sb.append(" in (select ");
                    sb.append("_id");
                    sb.append(" from ");
                    sb.append(super.b);
                    sb.append(" order by ");
                    sb.append("insert_time");
                    sb.append(" desc limit 1000 offset ");
                    sb.append(500);
                    sb.append(")");
                    sqLiteDatabase.execSQL(sb.toString());
                }
                catch (final Exception ex) {
                    q.b(ex);
                }
            }
        }
    }
    
    public boolean a(final SQLiteDatabase sqLiteDatabase, final String s) {
        boolean b2;
        final boolean b = b2 = false;
        if (sqLiteDatabase != null) {
            if (TextUtils.isEmpty((CharSequence)s)) {
                b2 = b;
            }
            else {
                int n = 0;
                try {
                    final Cursor query = sqLiteDatabase.query(super.b, (String[])null, "path=?", new String[] { s }, (String)null, (String)null, (String)null);
                    query.getCount();
                    try {
                        query.close();
                    }
                    finally {}
                }
                finally {
                    n = 0;
                }
                final Throwable t;
                q.b(t);
                b2 = b;
                if (n > 0) {
                    b2 = true;
                }
            }
        }
        return b2;
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.apm.insight.h;

import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Map;

public class a
{
    private static final Map<Class<?>, Class<?>> a;
    
    static {
        final Map<Class<?>, Class<?>> map = a = new HashMap<Class<?>, Class<?>>();
        final Class<Boolean> type = Boolean.TYPE;
        map.put(Boolean.class, type);
        map.put(Byte.class, Byte.TYPE);
        map.put(Character.class, Character.TYPE);
        map.put(Short.class, Short.TYPE);
        final Class<Integer> type2 = Integer.TYPE;
        map.put(Integer.class, type2);
        final Class<Float> type3 = Float.TYPE;
        map.put(Float.class, type3);
        final Class<Long> type4 = Long.TYPE;
        map.put(Long.class, type4);
        map.put(Double.class, Double.TYPE);
        map.put(type, type);
        final Class<Byte> type5 = Byte.TYPE;
        map.put(type5, type5);
        final Class<Character> type6 = Character.TYPE;
        map.put(type6, type6);
        final Class<Short> type7 = Short.TYPE;
        map.put(type7, type7);
        map.put(type2, type2);
        map.put(type3, type3);
        map.put(type4, type4);
        final Class<Double> type8 = Double.TYPE;
        map.put(type8, type8);
    }
    
    public static <T> T a(final Class<?> clazz, final String s, final Object... array) {
        return (T)a(clazz, s, a(array)).invoke(null, b(array));
    }
    
    public static <T> T a(final String s, final String str, final Object... array) {
        try {
            return a(Class.forName(s), str, array);
        }
        catch (final Exception ex) {
            final StringBuilder sb = new StringBuilder();
            sb.append("Meet exception when call Method '");
            sb.append(str);
            sb.append("' in ");
            sb.append(s);
            return null;
        }
    }
    
    private static Method a(final Class<?> clazz, final String s, final Class<?>... array) {
        final Method a = a(clazz.getDeclaredMethods(), s, array);
        if (a != null) {
            a.setAccessible(true);
            return a;
        }
        if (clazz.getSuperclass() != null) {
            return a(clazz.getSuperclass(), s, array);
        }
        throw new NoSuchMethodException();
    }
    
    private static Method a(final Method[] array, final String anObject, final Class<?>[] array2) {
        if (anObject != null) {
            for (final Method method : array) {
                if (method.getName().equals(anObject) && a(method.getParameterTypes(), array2)) {
                    return method;
                }
            }
            return null;
        }
        throw new NullPointerException("Method name must not be null.");
    }
    
    private static boolean a(final Class<?>[] array, final Class<?>[] array2) {
        boolean b = true;
        final boolean b2 = true;
        if (array == null) {
            boolean b3 = b2;
            if (array2 != null) {
                b3 = (array2.length == 0 && b2);
            }
            return b3;
        }
        if (array2 == null) {
            if (array.length != 0) {
                b = false;
            }
            return b;
        }
        if (array.length != array2.length) {
            return false;
        }
        for (int i = 0; i < array.length; ++i) {
            if (!array[i].isAssignableFrom(array2[i])) {
                final Map<Class<?>, Class<?>> a = com.apm.insight.h.a.a;
                if (!a.containsKey(array[i]) || !((Class<?>)a.get(array[i])).equals(a.get(array2[i]))) {
                    return false;
                }
            }
        }
        return true;
    }
    
    private static Class<?>[] a(final Object... array) {
        Class<?>[] array2 = null;
        if (array != null) {
            array2 = array2;
            if (array.length > 0) {
                final Class[] array3 = new Class[array.length];
                for (int i = 0; i < array.length; ++i) {
                    final Object o = array[i];
                    if (o != null && o instanceof a) {
                        array3[i] = ((a)o).a;
                    }
                    else {
                        Class<? extends a> class1;
                        if (o == null) {
                            class1 = null;
                        }
                        else {
                            class1 = ((a)o).getClass();
                        }
                        array3[i] = class1;
                    }
                }
                array2 = array3;
            }
        }
        return array2;
    }
    
    private static Object[] b(final Object... array) {
        Object[] array3;
        if (array != null && array.length > 0) {
            final Object[] array2 = new Object[array.length];
            int n = 0;
            while (true) {
                array3 = array2;
                if (n >= array.length) {
                    break;
                }
                final Object o = array[n];
                if (o != null && o instanceof a) {
                    array2[n] = ((a)o).b;
                }
                else {
                    array2[n] = o;
                }
                ++n;
            }
        }
        else {
            array3 = null;
        }
        return array3;
    }
    
    public static class a<T>
    {
        public final Class<? extends T> a;
        public final T b;
    }
}

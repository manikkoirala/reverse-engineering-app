// 
// Decompiled by Procyon v0.6.0
// 

package com.apm.insight.h;

import java.io.InputStream;
import java.util.zip.ZipEntry;
import java.io.FileOutputStream;
import java.io.Closeable;
import android.os.Build;
import java.util.zip.ZipFile;
import androidx.annotation.Nullable;
import java.lang.reflect.Field;
import android.content.pm.ApplicationInfo;
import com.apm.insight.l.k;
import com.apm.insight.l.i;
import dalvik.system.BaseDexClassLoader;
import java.io.File;
import android.content.Context;
import java.util.ArrayList;
import java.util.List;

public class c
{
    private static List<String> a;
    
    static {
        c.a = new ArrayList<String>();
    }
    
    @Nullable
    public static String a(final Context context, final String s, final File file) {
        final ApplicationInfo applicationInfo = context.getApplicationInfo();
        if (a(applicationInfo.sourceDir, s, file) == null) {
            return null;
        }
        final String[] splitSourceDirs = applicationInfo.splitSourceDirs;
        final int length = splitSourceDirs.length;
        final int n = 0;
        for (int i = 0; i < length; ++i) {
            if (a(splitSourceDirs[i], s, file) == null) {
                return null;
            }
        }
        String message = null;
        try {
            ClassLoader obj;
            for (obj = c.class.getClassLoader(); !(obj instanceof BaseDexClassLoader) && obj.getParent() != null; obj = obj.getParent()) {}
            if (obj instanceof BaseDexClassLoader) {
                final Field declaredField = BaseDexClassLoader.class.getDeclaredField("pathList");
                declaredField.setAccessible(true);
                final Object value = declaredField.get(obj);
                final Field declaredField2 = value.getClass().getDeclaredField("nativeLibraryDirectories");
                declaredField2.setAccessible(true);
                final String[] array = (String[])declaredField2.get(value);
                for (int length2 = array.length, j = n; j < length2; ++j) {
                    final File file2 = new File(array[j], System.mapLibraryName(s));
                    if (file2.exists()) {
                        i.a(file2, file);
                        k.a(file.getAbsolutePath(), 493);
                        return null;
                    }
                }
            }
        }
        finally {
            final Throwable t;
            message = t.getMessage();
        }
        return message;
    }
    
    public static String a(String pathname, String string, File file) {
        ZipConstants zipConstants = null;
        try {
            final ZipFile zipFile = new ZipFile(new File((String)pathname), 1);
            try {
                final StringBuilder sb = new StringBuilder();
                sb.append("lib/");
                final String cpu_ABI = Build.CPU_ABI;
                sb.append(cpu_ABI);
                sb.append("/");
                sb.append(System.mapLibraryName(string));
                ZipEntry entry;
                zipConstants = (entry = zipFile.getEntry(sb.toString()));
                if (zipConstants == null) {
                    int endIndex = cpu_ABI.indexOf(45);
                    final StringBuilder sb2 = new StringBuilder();
                    sb2.append("lib/");
                    if (endIndex <= 0) {
                        endIndex = cpu_ABI.length();
                    }
                    sb2.append(cpu_ABI.substring(0, endIndex));
                    sb2.append("/");
                    sb2.append(System.mapLibraryName(string));
                    string = sb2.toString();
                    entry = zipFile.getEntry(string);
                    if (entry == null) {
                        final StringBuilder sb3 = new StringBuilder();
                        sb3.append("Library entry not found:");
                        sb3.append(string);
                        pathname = sb3.toString();
                        k.a((Closeable)null);
                        k.a((Closeable)null);
                        k.a(zipFile);
                        return (String)pathname;
                    }
                }
                file.createNewFile();
                pathname = zipFile.getInputStream(entry);
                try {
                    final FileOutputStream fileOutputStream = new FileOutputStream(file);
                    try {
                        final byte[] array = new byte[4096];
                        while (true) {
                            final int read = ((InputStream)pathname).read(array);
                            if (read <= 0) {
                                break;
                            }
                            fileOutputStream.write(array, 0, read);
                        }
                        k.a(file.getAbsolutePath(), 493);
                        k.a(fileOutputStream);
                        k.a((Closeable)pathname);
                        k.a(zipFile);
                        return null;
                    }
                    finally {}
                }
                finally {}
            }
            finally {}
            zipConstants = null;
            zipConstants = zipFile;
        }
        finally {
            file = null;
            string = null;
        }
        try {
            final Throwable t;
            pathname = t.getMessage();
            return (String)pathname;
        }
        finally {
            k.a((Closeable)string);
            k.a((Closeable)file);
            k.a((ZipFile)zipConstants);
        }
    }
}

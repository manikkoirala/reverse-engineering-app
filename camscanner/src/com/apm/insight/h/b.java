// 
// Decompiled by Procyon v0.6.0
// 

package com.apm.insight.h;

import com.apm.insight.l.q;
import java.io.File;
import com.apm.insight.runtime.r;
import com.apm.insight.runtime.p;
import com.apm.insight.i;
import java.util.HashMap;

public class b
{
    private static HashMap<String, String> a;
    
    public static String a() {
        final StringBuilder sb = new StringBuilder();
        sb.append(i.g().getFilesDir());
        sb.append("/apminsight/selflib/");
        return sb.toString();
    }
    
    public static String a(final String str) {
        final StringBuilder sb = new StringBuilder();
        sb.append(i.g().getFilesDir());
        sb.append("/apminsight/selflib/");
        sb.append("lib");
        sb.append(str);
        sb.append(".so");
        return sb.toString();
    }
    
    public static void b(final String s) {
        p.b().a(new Runnable(s) {
            boolean a = false;
            final String b;
            
            @Override
            public void run() {
                d();
                if (f(this.b)) {
                    return;
                }
                r.a("updateSo", this.b);
                final File file = new File(com.apm.insight.h.b.a(this.b));
                file.getParentFile().mkdirs();
                if (file.exists()) {
                    file.delete();
                }
                final StringBuilder sb = new StringBuilder();
                sb.append("doUnpackLibrary: ");
                sb.append(this.b);
                q.a(sb.toString());
                Object o = null;
                try {
                    c.a(i.g(), this.b, file);
                }
                finally {
                    r.a("updateSoError", this.b);
                    final Throwable t;
                    com.apm.insight.c.a().a("NPTH_CATCH", t);
                    o = null;
                }
                Label_0174: {
                    if (o != null) {
                        break Label_0174;
                    }
                    com.apm.insight.h.b.a.put(file.getName(), "1.3.8.nourl-alpha.15");
                    while (true) {
                        try {
                            com.apm.insight.l.i.a(new File(e(this.b)), "1.3.8.nourl-alpha.15", false);
                            String s = this.b;
                            String s2 = "updateSoSuccess";
                        Block_9:
                            while (true) {
                                r.a(s2, s);
                                return;
                                iftrue(Label_0209:)(this.a);
                                break Block_9;
                                Label_0209: {
                                    s = this.b;
                                }
                                s2 = "updateSoFailed";
                                continue;
                            }
                            this.a = true;
                            r.a("updateSoPostRetry", this.b);
                            p.b().a(this, 3000L);
                        }
                        finally {
                            continue;
                        }
                        break;
                    }
                }
            }
        });
    }
    
    private static void d() {
        if (b.a != null) {
            return;
        }
        b.a = new HashMap<String, String>();
        final File parent = new File(i.g().getFilesDir(), "/apminsight/selflib/");
        final String[] list = parent.list();
        if (list == null) {
            return;
        }
        for (final String s : list) {
            if (s.endsWith(".ver")) {
                final String substring = s.substring(0, s.length() - 4);
                try {
                    final HashMap<String, String> a = b.a;
                    final StringBuilder sb = new StringBuilder();
                    sb.append(parent.getAbsolutePath());
                    sb.append("/");
                    sb.append(s);
                    a.put(substring, com.apm.insight.l.i.c(sb.toString()));
                }
                finally {
                    final Throwable t;
                    com.apm.insight.c.a().a("NPTH_CATCH", t);
                }
            }
            else if (!s.endsWith(".so")) {
                com.apm.insight.l.i.a(new File(parent, s));
            }
        }
    }
    
    private static String e(final String str) {
        final StringBuilder sb = new StringBuilder();
        sb.append(i.g().getFilesDir());
        sb.append("/apminsight/selflib/");
        sb.append(str);
        sb.append(".ver");
        return sb.toString();
    }
    
    private static boolean f(final String key) {
        return "1.3.8.nourl-alpha.15".equals(b.a.get(key)) && new File(a(key)).exists();
    }
}

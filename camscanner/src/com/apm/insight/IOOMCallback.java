// 
// Decompiled by Procyon v0.6.0
// 

package com.apm.insight;

import androidx.annotation.Nullable;
import androidx.annotation.NonNull;

public interface IOOMCallback
{
    void onCrash(@NonNull final CrashType p0, @Nullable final Throwable p1, @Nullable final Thread p2, final long p3);
}

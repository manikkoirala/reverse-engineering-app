// 
// Decompiled by Procyon v0.6.0
// 

package com.apm.insight.d;

import com.apm.insight.k.d;
import org.json.JSONObject;
import com.apm.insight.i;
import com.apm.insight.CrashType;
import com.apm.insight.runtime.a.f;
import com.apm.insight.runtime.p;
import androidx.annotation.Nullable;
import com.apm.insight.g;
import java.util.Map;

public class a
{
    public static void a(final String s) {
        a(s, null, null, null);
    }
    
    public static void a(final String s, @Nullable final Map<? extends String, ? extends String> map, @Nullable final Map<String, String> map2, @Nullable final g g) {
        a(s, map, map2, null, g);
    }
    
    public static void a(final String s, @Nullable final Map<? extends String, ? extends String> map, @Nullable final Map<String, String> map2, @Nullable final Map<String, String> map3, @Nullable final g g) {
        final long currentTimeMillis = System.currentTimeMillis();
        try {
            p.b().a(new Runnable(currentTimeMillis, s, map, map2, map3, g) {
                final long a;
                final String b;
                final Map c;
                final Map d;
                final Map e;
                final g f;
                
                @Override
                public void run() {
                    boolean b = false;
                    try {
                        final com.apm.insight.entity.a a = com.apm.insight.runtime.a.f.a().a(CrashType.DART, com.apm.insight.entity.a.a(this.a, i.g(), this.b));
                        if (this.c != null) {
                            JSONObject optJSONObject;
                            if ((optJSONObject = a.h().optJSONObject("custom")) == null) {
                                optJSONObject = new JSONObject();
                            }
                            com.apm.insight.entity.a.a(optJSONObject, this.c);
                            a.a("custom", optJSONObject);
                        }
                        if (this.d != null) {
                            JSONObject optJSONObject2;
                            if ((optJSONObject2 = a.h().optJSONObject("custom_long")) == null) {
                                optJSONObject2 = new JSONObject();
                            }
                            com.apm.insight.entity.a.a(optJSONObject2, this.d);
                            a.a("custom_long", optJSONObject2);
                        }
                        if (this.e != null) {
                            JSONObject optJSONObject3;
                            if ((optJSONObject3 = a.h().optJSONObject("filters")) == null) {
                                optJSONObject3 = new JSONObject();
                                a.a("filters", optJSONObject3);
                            }
                            com.apm.insight.entity.a.a(optJSONObject3, this.e);
                        }
                        com.apm.insight.k.d.a().a(this.a, a.h());
                    }
                    finally {
                        b = false;
                    }
                    final g f = this.f;
                    if (f == null) {
                        return;
                    }
                    try {
                        f.a(b);
                    }
                    finally {}
                }
            });
        }
        finally {}
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.apm.insight;

import android.text.TextUtils;
import com.apm.insight.l.l;
import org.json.JSONArray;
import androidx.annotation.Nullable;
import androidx.annotation.NonNull;

public class b implements ICrashCallback, IOOMCallback
{
    private IOOMCallback a;
    private ICrashCallback b;
    private String c;
    
    public b(final String c, final ICrashCallback b) {
        this.c = c;
        this.b = b;
    }
    
    public b(final String c, final IOOMCallback a) {
        this.c = c;
        this.a = a;
    }
    
    public void a(@NonNull final CrashType crashType, @Nullable final String s, @Nullable final String s2, final String s3) {
        if (this.b == null) {
            return;
        }
        final h a = h.a(this.c);
        if (a != null && a.a(s3, s2)) {
            this.b.onCrash(crashType, s, null);
        }
    }
    
    public void a(@NonNull final CrashType crashType, @Nullable final String s, @Nullable final Thread thread, final JSONArray jsonArray) {
        if (this.b == null) {
            return;
        }
        if (jsonArray != null) {
            for (int i = 0; i < jsonArray.length(); ++i) {
                if (TextUtils.equals((CharSequence)l.b(jsonArray.optJSONObject(i), "header", "aid"), (CharSequence)this.c)) {
                    this.b.onCrash(crashType, s, thread);
                }
            }
        }
    }
    
    public void a(@NonNull final CrashType crashType, @Nullable final Throwable t, @Nullable final Thread thread, final long n, final JSONArray jsonArray) {
        if (this.a == null) {
            return;
        }
        if (jsonArray != null) {
            for (int i = 0; i < jsonArray.length(); ++i) {
                if (TextUtils.equals((CharSequence)l.b(jsonArray.optJSONObject(i), "header", "aid"), (CharSequence)this.c)) {
                    this.a.onCrash(crashType, t, thread, n);
                }
            }
        }
    }
    
    @Override
    public void onCrash(@NonNull final CrashType crashType, @Nullable final String s, @Nullable final Thread thread) {
    }
    
    @Override
    public void onCrash(@NonNull final CrashType crashType, @Nullable final Throwable t, @Nullable final Thread thread, final long n) {
    }
}

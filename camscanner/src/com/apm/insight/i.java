// 
// Decompiled by Procyon v0.6.0
// 

package com.apm.insight;

import java.util.Random;
import com.apm.insight.runtime.t;
import java.util.concurrent.ConcurrentHashMap;
import com.apm.insight.runtime.ConfigManager;
import android.annotation.SuppressLint;
import com.apm.insight.runtime.d;
import android.app.Application;
import android.content.Context;

public final class i
{
    private static Context a;
    private static Application b;
    private static long c = 0L;
    private static String d = "default";
    private static boolean e = false;
    @SuppressLint({ "StaticFieldLeak" })
    private static d f;
    private static ConfigManager g;
    private static a h;
    private static volatile ConcurrentHashMap<Integer, String> i;
    private static t j;
    private static volatile String k;
    private static Object l;
    private static volatile int m;
    private static volatile String n;
    private static int o;
    private static boolean p;
    private static boolean q;
    private static boolean r;
    private static boolean s;
    
    static {
        com.apm.insight.i.g = new ConfigManager();
        com.apm.insight.i.h = new a();
        com.apm.insight.i.j = null;
        com.apm.insight.i.k = null;
        com.apm.insight.i.l = new Object();
        com.apm.insight.i.m = 0;
        com.apm.insight.i.o = 0;
        com.apm.insight.i.p = true;
        com.apm.insight.i.q = true;
        com.apm.insight.i.r = false;
        com.apm.insight.i.s = true;
    }
    
    public static d a() {
        if (com.apm.insight.i.f == null) {
            com.apm.insight.i.f = com.apm.insight.runtime.i.a(com.apm.insight.i.a);
        }
        return com.apm.insight.i.f;
    }
    
    public static String a(final long lng, final CrashType crashType, final boolean b, final boolean b2) {
        final StringBuilder sb = new StringBuilder();
        sb.append(lng);
        sb.append("_");
        sb.append(crashType.getName());
        sb.append('_');
        sb.append(f());
        sb.append('_');
        final String s = "normal_";
        String str;
        if (b) {
            str = "oom_";
        }
        else {
            str = "normal_";
        }
        sb.append(str);
        sb.append(j());
        sb.append('_');
        String str2 = s;
        if (b2) {
            str2 = "ignore_";
        }
        sb.append(str2);
        sb.append(Long.toHexString(new Random().nextLong()));
        sb.append("G");
        return sb.toString();
    }
    
    public static void a(final int o) {
        com.apm.insight.i.o = o;
    }
    
    static void a(final int i, final String value) {
        if (i.i == null) {
            synchronized (i.class) {
                if (i.i == null) {
                    i.i = new ConcurrentHashMap<Integer, String>();
                }
            }
        }
        i.i.put(i, value);
    }
    
    public static void a(final Application b) {
        if (b != null) {
            com.apm.insight.i.b = b;
        }
    }
    
    public static void a(final Application b, final Context a) {
        if (com.apm.insight.i.b == null) {
            com.apm.insight.i.c = System.currentTimeMillis();
            com.apm.insight.i.a = a;
            com.apm.insight.i.b = b;
            final StringBuilder sb = new StringBuilder();
            sb.append(Long.toHexString(new Random().nextLong()));
            sb.append("G");
            com.apm.insight.i.k = sb.toString();
        }
    }
    
    static void a(final Application application, final Context context, final ICommonParams commonParams) {
        a(application, context);
        com.apm.insight.i.f = new d(com.apm.insight.i.a, commonParams, a());
    }
    
    public static void a(final d f) {
        com.apm.insight.i.f = f;
    }
    
    static void a(final String d) {
        com.apm.insight.i.d = d;
    }
    
    static void a(final boolean e) {
        com.apm.insight.i.e = e;
    }
    
    public static a b() {
        return com.apm.insight.i.h;
    }
    
    static void b(final int m, final String n) {
        com.apm.insight.i.m = m;
        com.apm.insight.i.n = n;
    }
    
    public static void b(final boolean p) {
        com.apm.insight.i.p = p;
    }
    
    public static t c() {
        if (com.apm.insight.i.j == null) {
            synchronized (i.class) {
                com.apm.insight.i.j = new t(com.apm.insight.i.a);
            }
        }
        return com.apm.insight.i.j;
    }
    
    public static void c(final boolean q) {
        com.apm.insight.i.q = q;
    }
    
    public static void d(final boolean r) {
        com.apm.insight.i.r = r;
    }
    
    public static boolean d() {
        return i().isDebugMode() && n().contains("local_test");
    }
    
    public static String e() {
        final StringBuilder sb = new StringBuilder();
        sb.append(f());
        sb.append('_');
        sb.append(Long.toHexString(new Random().nextLong()));
        sb.append("G");
        return sb.toString();
    }
    
    public static void e(final boolean s) {
        com.apm.insight.i.s = s;
    }
    
    public static String f() {
        if (com.apm.insight.i.k == null) {
            synchronized (com.apm.insight.i.l) {
                if (com.apm.insight.i.k == null) {
                    final StringBuilder sb = new StringBuilder();
                    sb.append(Long.toHexString(new Random().nextLong()));
                    sb.append("U");
                    com.apm.insight.i.k = sb.toString();
                }
            }
        }
        return com.apm.insight.i.k;
    }
    
    public static Context g() {
        return com.apm.insight.i.a;
    }
    
    public static Application h() {
        return com.apm.insight.i.b;
    }
    
    public static ConfigManager i() {
        return com.apm.insight.i.g;
    }
    
    public static long j() {
        return com.apm.insight.i.c;
    }
    
    public static String k() {
        return com.apm.insight.i.d;
    }
    
    public static int l() {
        return com.apm.insight.i.o;
    }
    
    public static boolean m() {
        return com.apm.insight.i.e;
    }
    
    public static String n() {
        final Object value = a().a().get("channel");
        if (value == null) {
            return "unknown";
        }
        return String.valueOf(value);
    }
    
    public static ConcurrentHashMap<Integer, String> o() {
        return com.apm.insight.i.i;
    }
    
    public static int p() {
        return com.apm.insight.i.m;
    }
    
    public static String q() {
        return com.apm.insight.i.n;
    }
    
    public static boolean r() {
        return com.apm.insight.i.p;
    }
    
    public static boolean s() {
        return com.apm.insight.i.q;
    }
    
    public static boolean t() {
        return com.apm.insight.i.r;
    }
    
    public static boolean u() {
        return com.apm.insight.i.s;
    }
}

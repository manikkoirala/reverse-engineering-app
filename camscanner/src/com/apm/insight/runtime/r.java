// 
// Decompiled by Procyon v0.6.0
// 

package com.apm.insight.runtime;

import com.apm.insight.d;
import com.apm.insight.c;
import androidx.annotation.NonNull;
import org.json.JSONArray;
import java.io.IOException;
import android.text.TextUtils;
import java.util.HashMap;
import androidx.annotation.Nullable;
import com.apm.insight.l.o;
import com.apm.insight.l.a;
import com.apm.insight.i;
import java.io.File;

public class r
{
    private static File a;
    
    @Nullable
    private static File a() {
        if (r.a == null) {
            final String c = com.apm.insight.l.a.c(i.g());
            if (c == null) {
                return null;
            }
            final long currentTimeMillis = System.currentTimeMillis();
            final String j = o.j(i.g());
            final StringBuilder sb = new StringBuilder();
            sb.append("apminsight/ProcessTrack/");
            sb.append((currentTimeMillis - currentTimeMillis % 86400000L) / 86400000L);
            sb.append('/');
            sb.append(c.replace(':', '_'));
            sb.append(".txt");
            r.a = new File(j, sb.toString());
        }
        return r.a;
    }
    
    public static File a(final long n) {
        final String j = o.j(i.g());
        final StringBuilder sb = new StringBuilder();
        sb.append("apminsight/ProcessTrack/");
        sb.append((n - n % 86400000L) / 86400000L);
        return new File(j, sb.toString());
    }
    
    @NonNull
    public static HashMap<String, a> a(long length, final String prefix) {
        final String j = o.j(i.g());
        final StringBuilder sb = new StringBuilder();
        sb.append("apminsight/ProcessTrack/");
        sb.append((length - length % 86400000L) / 86400000L);
        final File parent = new File(j, sb.toString());
        final String[] list = parent.list();
        final HashMap hashMap = new HashMap();
        if (list == null) {
            return hashMap;
        }
        final int length2 = list.length;
        int n = 0;
    Label_0236_Outer:
        while (true) {
            if (n >= length2) {
                return hashMap;
            }
            final String child = list[n];
            final File file = new File(parent, child);
            length = file.length();
            if (length > 1048576L) {
                length -= 524288L;
            }
            else {
                length = 0L;
            }
            while (true) {
                try {
                    final JSONArray a = com.apm.insight.l.i.a(file, length);
                    for (int i = a.length() - 1; i >= 0; --i) {
                        final String optString = a.optString(i);
                        if (!TextUtils.isEmpty((CharSequence)optString)) {
                            if (optString.startsWith(prefix)) {
                                hashMap.put(child.replace('_', ':').replace(".txt", ""), new a(optString));
                                break;
                            }
                        }
                    }
                    ++n;
                    continue Label_0236_Outer;
                }
                catch (final IOException ex) {
                    continue;
                }
                break;
            }
            break;
        }
    }
    
    public static void a(final String str, final String str2) {
        try {
            final File a = a();
            if (a != null) {
                final StringBuilder sb = new StringBuilder();
                sb.append(str);
                sb.append(' ');
                sb.append(str2);
                sb.append(' ');
                sb.append(System.currentTimeMillis());
                sb.append('\n');
                com.apm.insight.l.i.a(a, sb.toString(), true);
            }
        }
        finally {}
    }
    
    public static class a
    {
        public String a;
        public String b;
        public long c;
        
        a(final String s) {
            final String[] split = s.split("\\s+");
            if (split.length == 3) {
                this.a = split[0];
                this.b = split[1];
                try {
                    this.c = Long.parseLong(split[2]);
                }
                finally {
                    final d a = c.a();
                    final StringBuilder sb = new StringBuilder();
                    sb.append("err ProcessTrack line:");
                    sb.append(s);
                    final Throwable cause;
                    a.a("NPTH_CATCH", new RuntimeException(sb.toString(), cause));
                }
            }
            else {
                final d a2 = c.a();
                final StringBuilder sb2 = new StringBuilder();
                sb2.append("err ProcessTrack line:");
                sb2.append(s);
                a2.a("NPTH_CATCH", new RuntimeException(sb2.toString()));
            }
        }
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.apm.insight.runtime;

import androidx.annotation.Nullable;
import com.apm.insight.i;
import com.apm.insight.entity.Header;

public class f
{
    private static final f b;
    private f a;
    
    static {
        b = new f() {
            Header a = null;
            
            @Nullable
            @Override
            public Object b(final String s) {
                if (this.a == null) {
                    this.a = Header.b(i.g());
                }
                return this.a.f().opt(s);
            }
        };
    }
    
    f() {
        this(f.b);
    }
    
    f(final f a) {
        this.a = a;
    }
    
    @Nullable
    public Object a(final String s) {
        final f a = this.a;
        if (a != null) {
            return a.a(s);
        }
        return null;
    }
    
    @Nullable
    public Object b(final String s) {
        final f a = this.a;
        if (a != null) {
            return a.b(s);
        }
        return null;
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.apm.insight.runtime;

import android.text.TextUtils;
import java.util.Map;
import androidx.annotation.Nullable;
import com.apm.insight.d;
import org.json.JSONException;
import com.apm.insight.l.q;
import java.util.regex.Pattern;
import java.io.FilenameFilter;
import java.util.Iterator;
import java.util.ArrayList;
import java.io.IOException;
import com.apm.insight.c;
import org.json.JSONArray;
import com.apm.insight.i;
import com.apm.insight.entity.Header;
import org.json.JSONObject;
import com.apm.insight.runtime.a.b;
import com.apm.insight.l.o;
import android.content.Context;
import java.io.File;

public class s
{
    private static s a;
    private File b;
    private File c;
    private File d;
    private Context e;
    private a f;
    
    private s(final Context e) {
        this.f = null;
        final File c = o.c(e);
        if (!c.exists() || (!c.isDirectory() && c.delete())) {
            c.mkdirs();
            com.apm.insight.runtime.a.b.a();
        }
        this.b = c;
        this.c = new File(c, "did");
        this.d = new File(c, "device_uuid");
        this.e = e;
    }
    
    public static int a(final JSONObject jsonObject, final JSONObject jsonObject2) {
        if (Header.c(jsonObject)) {
            return 2;
        }
        if (Header.c(jsonObject2)) {
            return 0;
        }
        if (String.valueOf(jsonObject2.opt("update_version_code")).equals(String.valueOf(jsonObject.opt("update_version_code"))) && Header.d(jsonObject)) {
            return 1;
        }
        return 2;
    }
    
    public static s a() {
        if (s.a == null) {
            s.a = new s(i.g());
        }
        return s.a;
    }
    
    private void a(final long n, final long n2, final JSONObject jsonObject, final JSONArray jsonArray) {
        final File b = this.b;
        final StringBuilder sb = new StringBuilder();
        sb.append("");
        sb.append(n);
        sb.append("-");
        sb.append(n2);
        sb.append(".ctx");
        final File file = new File(b, sb.toString());
        final File b2 = this.b;
        final StringBuilder sb2 = new StringBuilder();
        sb2.append("");
        sb2.append(n);
        sb2.append("-");
        sb2.append(n2);
        sb2.append(".allData");
        final File file2 = new File(b2, sb2.toString());
        try {
            com.apm.insight.l.i.a(file, jsonObject, false);
            com.apm.insight.l.i.a(file2, jsonArray, false);
            this.f = new a(file);
        }
        catch (final IOException ex) {
            com.apm.insight.c.a().a("NPTH_CATCH", ex);
        }
    }
    
    private a c() {
        if (this.f == null) {
            this.d(".ctx");
        }
        return this.f;
    }
    
    private void c(final long n) {
        try {
            final ArrayList<a> d = this.d("");
            if (d.size() <= 6) {
                return;
            }
            for (final a a : d) {
                if (a.b(n)) {
                    a.c();
                }
            }
        }
        finally {
            final Throwable t;
            com.apm.insight.c.a().a("NPTH_CATCH", t);
        }
    }
    
    private File d(final long n) {
        for (final a a : this.d(".ctx")) {
            if (n >= a.a && n <= a.b) {
                return a.c;
            }
        }
        return null;
    }
    
    private ArrayList<a> d(final String anObject) {
        final File[] listFiles = this.b.listFiles(new FilenameFilter(this, anObject) {
            final String a;
            final s b;
            
            @Override
            public boolean accept(final File file, final String input) {
                return input.endsWith(this.a) && Pattern.compile("^\\d{1,13}-\\d{1,13}.*").matcher(input).matches();
            }
        });
        final ArrayList list = new ArrayList();
        if (listFiles == null) {
            return list;
        }
        final StringBuilder sb = new StringBuilder();
        sb.append("foundRuntimeContextFiles ");
        sb.append(listFiles.length);
        q.a((Object)sb.toString());
        final int length = listFiles.length;
        int i = 0;
        a f = null;
        while (i < length) {
            final File file = listFiles[i];
            a a;
            try {
                final a e = new a(file);
                list.add(e);
                if (this.f == null && ".ctx".equals(anObject)) {
                    if (f != null) {
                        if (e.b >= f.b) {}
                    }
                }
            }
            finally {
                final Throwable t;
                com.apm.insight.c.a().a("NPTH_CATCH", t);
                a = f;
            }
            ++i;
            f = a;
        }
        if (this.f == null && f != null) {
            this.f = f;
        }
        return list;
    }
    
    private File e(final long n) {
        for (final a a : this.d(".allData")) {
            if (n >= a.a && n <= a.b) {
                return a.c;
            }
        }
        return null;
    }
    
    private File f(final long n) {
        final Iterator<a> iterator = this.d(".ctx").iterator();
        final File file = null;
        a a = null;
        while (iterator.hasNext()) {
            final a a2 = iterator.next();
            if (a == null || Math.abs(a.b - n) > Math.abs(a2.b - n)) {
                a = a2;
            }
        }
        File c;
        if (a == null) {
            c = file;
        }
        else {
            c = a.c;
        }
        return c;
    }
    
    private File g(final long n) {
        final Iterator<a> iterator = this.d(".allData").iterator();
        final File file = null;
        a a = null;
        while (iterator.hasNext()) {
            final a a2 = iterator.next();
            if (a == null || Math.abs(a.b - n) > Math.abs(a2.b - n)) {
                a = a2;
            }
        }
        File c;
        if (a == null) {
            c = file;
        }
        else {
            c = a.c;
        }
        return c;
    }
    
    public String a(String c) {
        try {
            c = com.apm.insight.l.i.c(this.d.getAbsolutePath());
            return c;
        }
        finally {
            return c;
        }
    }
    
    @Nullable
    public JSONObject a(final long n) {
        File file = this.d(n);
        boolean b;
        if (file == null) {
            file = this.f(n);
            b = true;
        }
        else {
            b = false;
        }
        Object str;
        final Object o = str = null;
        if (file != null) {
            try {
                final String c = com.apm.insight.l.i.c(file.getAbsolutePath());
                try {
                    final JSONObject jsonObject = new JSONObject(c);
                }
                finally {}
            }
            finally {
                str = null;
            }
            final d a = com.apm.insight.c.a();
            final StringBuilder sb = new StringBuilder();
            sb.append("content :");
            sb.append((String)str);
            final Throwable cause;
            a.a("NPTH_CATCH", new IOException(sb.toString(), cause));
            str = o;
        }
        if (str != null && b) {
            try {
                ((JSONObject)str).put("unauthentic_version", 1);
            }
            catch (final JSONException ex) {
                com.apm.insight.c.a().a("NPTH_CATCH", (Throwable)ex);
            }
        }
        return (JSONObject)str;
    }
    
    public void a(final Map<String, Object> map, final JSONArray jsonArray) {
        final JSONObject a = Header.a(this.e).a(map);
        if (Header.c(a)) {
            return;
        }
        final long currentTimeMillis = System.currentTimeMillis();
        final a c = this.c();
        if (c == null) {
            this.a(currentTimeMillis, currentTimeMillis, a, jsonArray);
            return;
        }
        final int a2 = a(c.b(), a);
        if (a2 != 1) {
            if (a2 != 2) {
                if (a2 == 3) {
                    c.a(currentTimeMillis);
                }
            }
            else {
                this.a(currentTimeMillis, currentTimeMillis, a, jsonArray);
            }
        }
        else {
            this.a(c.a, currentTimeMillis, a, jsonArray);
            com.apm.insight.l.i.a(c.c);
        }
        this.c(currentTimeMillis);
    }
    
    protected String b() {
        try {
            return com.apm.insight.l.i.c(this.c.getAbsolutePath());
        }
        finally {
            return "0";
        }
    }
    
    @Nullable
    public JSONArray b(final long n) {
        File file;
        if ((file = this.e(n)) == null) {
            file = this.g(n);
        }
        Object cause;
        final Object o = cause = null;
        if (file != null) {
            String str = null;
            try {
                final String c = com.apm.insight.l.i.c(file.getAbsolutePath());
                try {
                    final JSONArray jsonArray = new JSONArray(c);
                }
                finally {}
            }
            finally {
                str = null;
            }
            final d a = com.apm.insight.c.a();
            final StringBuilder sb = new StringBuilder();
            sb.append("content :");
            sb.append(str);
            a.a("NPTH_CATCH", new IOException(sb.toString(), (Throwable)cause));
            cause = o;
        }
        return (JSONArray)cause;
    }
    
    protected void b(final String s) {
        try {
            com.apm.insight.l.i.a(this.c, s, false);
        }
        finally {}
    }
    
    public void c(final String s) {
        try {
            com.apm.insight.l.i.a(this.d, s, false);
        }
        finally {}
    }
    
    private static class a
    {
        private long a;
        private long b;
        private File c;
        private JSONObject d;
        
        private a(final File c) {
            this.d = null;
            this.c = c;
            final String[] split = c.getName().split("-|\\.");
            long n;
            if (split.length >= 2) {
                this.a = Long.parseLong(split[0]);
                n = Long.parseLong(split[1]);
            }
            else {
                final String name = c.getName();
                if (TextUtils.isEmpty((CharSequence)name) || name.length() < 13) {
                    return;
                }
                final String substring = name.substring(0, 13);
                if (!TextUtils.isDigitsOnly((CharSequence)substring)) {
                    return;
                }
                n = Long.parseLong(substring);
                this.a = n;
            }
            this.b = n;
        }
        
        private String a() {
            final StringBuilder sb = new StringBuilder();
            sb.append(this.a);
            sb.append("-");
            sb.append(this.b);
            sb.append(".ctx");
            return sb.toString();
        }
        
        private void a(final long b) {
            this.b = b;
            this.c.renameTo(new File(this.c.getParent(), this.a()));
        }
        
        private JSONObject b() {
            if (this.d == null) {
                try {
                    this.d = new JSONObject(com.apm.insight.l.i.c(this.c.getAbsolutePath()));
                }
                finally {}
                if (this.d == null) {
                    this.d = new JSONObject();
                }
            }
            return this.d;
        }
        
        private boolean b(final long n) {
            final long a = this.a;
            if (a <= n || a - n <= 604800000L) {
                final long b = this.b;
                if ((b >= n || n - b <= 604800000L) && (this.c.lastModified() >= n || n - this.c.lastModified() <= 604800000L)) {
                    return false;
                }
            }
            return true;
        }
        
        private void c() {
            this.c.delete();
        }
    }
}

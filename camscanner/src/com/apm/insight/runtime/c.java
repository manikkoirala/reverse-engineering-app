// 
// Decompiled by Procyon v0.6.0
// 

package com.apm.insight.runtime;

import com.apm.insight.CrashType;
import androidx.annotation.NonNull;
import java.util.concurrent.CopyOnWriteArrayList;
import com.apm.insight.IOOMCallback;
import com.apm.insight.ICrashCallback;
import java.util.List;

public class c
{
    private final List<ICrashCallback> a;
    private final List<ICrashCallback> b;
    private final List<ICrashCallback> c;
    private final List<ICrashCallback> d;
    private final List<IOOMCallback> e;
    
    public c() {
        this.a = new CopyOnWriteArrayList<ICrashCallback>();
        this.b = new CopyOnWriteArrayList<ICrashCallback>();
        this.c = new CopyOnWriteArrayList<ICrashCallback>();
        this.d = new CopyOnWriteArrayList<ICrashCallback>();
        this.e = new CopyOnWriteArrayList<IOOMCallback>();
    }
    
    @NonNull
    public List<IOOMCallback> a() {
        return this.e;
    }
    
    void a(final ICrashCallback crashCallback, final CrashType crashType) {
        final int n = c$1.a[crashType.ordinal()];
        List<ICrashCallback> list = null;
        Label_0099: {
            if (n != 1) {
                if (n != 2) {
                    if (n == 3) {
                        list = this.b;
                        break Label_0099;
                    }
                    if (n == 4) {
                        list = this.a;
                        break Label_0099;
                    }
                    if (n != 5) {
                        return;
                    }
                    list = this.c;
                    break Label_0099;
                }
            }
            else {
                this.a.add(crashCallback);
                this.b.add(crashCallback);
                this.c.add(crashCallback);
            }
            list = this.d;
        }
        list.add(crashCallback);
    }
    
    void a(final IOOMCallback ioomCallback) {
        this.e.add(ioomCallback);
    }
    
    @NonNull
    public List<ICrashCallback> b() {
        return this.a;
    }
    
    void b(final ICrashCallback crashCallback, final CrashType crashType) {
        final int n = c$1.a[crashType.ordinal()];
        List<ICrashCallback> list = null;
        Label_0099: {
            if (n != 1) {
                if (n != 2) {
                    if (n == 3) {
                        list = this.b;
                        break Label_0099;
                    }
                    if (n == 4) {
                        list = this.a;
                        break Label_0099;
                    }
                    if (n != 5) {
                        return;
                    }
                    list = this.c;
                    break Label_0099;
                }
            }
            else {
                this.a.remove(crashCallback);
                this.b.remove(crashCallback);
                this.c.remove(crashCallback);
            }
            list = this.d;
        }
        list.remove(crashCallback);
    }
    
    void b(final IOOMCallback ioomCallback) {
        this.e.remove(ioomCallback);
    }
    
    @NonNull
    public List<ICrashCallback> c() {
        return this.b;
    }
    
    @NonNull
    public List<ICrashCallback> d() {
        return this.c;
    }
    
    @NonNull
    public List<ICrashCallback> e() {
        return this.d;
    }
}

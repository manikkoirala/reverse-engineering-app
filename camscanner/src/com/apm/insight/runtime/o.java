// 
// Decompiled by Procyon v0.6.0
// 

package com.apm.insight.runtime;

import android.content.ContextWrapper;
import android.os.Handler;
import com.apm.insight.g;
import androidx.annotation.Nullable;
import java.util.Map;
import com.apm.insight.f;
import java.io.File;
import android.text.TextUtils;
import com.apm.insight.k.h;
import com.apm.insight.e;
import com.apm.insight.IOOMCallback;
import com.apm.insight.CrashType;
import com.apm.insight.ICrashCallback;
import com.apm.insight.l.q;
import android.os.Looper;
import com.apm.insight.g.d;
import com.apm.insight.i.b;
import com.apm.insight.g.a;
import com.apm.insight.i;
import android.os.SystemClock;
import android.content.Context;
import androidx.annotation.NonNull;
import android.app.Application;
import com.apm.insight.nativecrash.NativeImpl;

public class o
{
    private static boolean a = false;
    private static boolean b = false;
    private static boolean c = false;
    private static boolean d = false;
    private static boolean e = false;
    private static c f;
    private static volatile boolean g;
    private static boolean h;
    
    static {
        o.f = new c();
        o.g = false;
        o.h = false;
    }
    
    public static c a() {
        return o.f;
    }
    
    public static void a(final long n) {
        NativeImpl.a(n);
    }
    
    public static void a(@NonNull final Application application, @NonNull final Context context, final boolean b, final boolean b2, final boolean b3, final boolean b4, long uptimeMillis) {
        synchronized (o.class) {
            uptimeMillis = SystemClock.uptimeMillis();
            if (o.a) {
                return;
            }
            o.a = true;
            if (context == null || application == null) {
                throw new IllegalArgumentException("context or Application must be not null.");
            }
            i.a(application, context);
            if (i.t() && (a((Context)application) || b((Context)application) || q())) {
                return;
            }
            if (b || b2) {
                final a a = com.apm.insight.g.a.a();
                if (b2) {
                    a.a(new b(context));
                }
                if (b) {
                    a.b(new d(context));
                }
                o.b = true;
            }
            NativeImpl.a();
            if (b3 && !(o.d = NativeImpl.a(context))) {
                o.e = true;
            }
            if (Looper.myLooper() == Looper.getMainLooper()) {
                o.g = true;
                NativeImpl.h();
            }
            g(b4);
            final StringBuilder sb = new StringBuilder();
            sb.append("Npth.init takes ");
            sb.append(SystemClock.uptimeMillis() - uptimeMillis);
            sb.append(" ms.");
            q.a((Object)sb.toString());
        }
    }
    
    public static void a(@NonNull Context baseContext, final boolean b, final boolean b2, final boolean b3, final boolean b4, final long n) {
        synchronized (o.class) {
            while (true) {
                if (i.h() != null) {
                    final Application h = i.h();
                    break Label_0014;
                }
                Label_0053: {
                    if (!(baseContext instanceof Application)) {
                        break Label_0053;
                    }
                    Application h = (Application)baseContext;
                    if (((ContextWrapper)h).getBaseContext() != null) {
                        break Label_0014;
                    }
                    throw new IllegalArgumentException("The Application passed in when init has not been attached, please pass a attachBaseContext as param and call Npth.setApplication(Application) before init.");
                    Label_0092: {
                        break Label_0092;
                        try {
                            final Application application = (Application)((Context)baseContext).getApplicationContext();
                            if (application == null) {
                                throw new IllegalArgumentException("Can not get the Application instance since a baseContext was passed in when init, please call Npth.setApplication(Application) before init.");
                            }
                            h = application;
                            if (((ContextWrapper)application).getBaseContext() != null) {
                                baseContext = ((ContextWrapper)application).getBaseContext();
                                h = application;
                                continue;
                            }
                            continue;
                            a(h, (Context)baseContext, b, b2, b3, b4, n);
                        }
                        finally {
                            baseContext = new IllegalArgumentException("Can not get the Application instance since a baseContext was passed in when init, please call Npth.setApplication(Application) before init.");
                        }
                    }
                }
                break;
            }
        }
    }
    
    public static void a(final ICrashCallback crashCallback, final CrashType crashType) {
        a().a(crashCallback, crashType);
    }
    
    public static void a(final IOOMCallback ioomCallback) {
        a().a(ioomCallback);
    }
    
    public static void a(final IOOMCallback ioomCallback, final CrashType crashType) {
        a().b(ioomCallback);
    }
    
    public static void a(@NonNull final e encryptImpl) {
        i.i().setEncryptImpl(encryptImpl);
    }
    
    public static void a(final h h) {
        com.apm.insight.k.e.a(h);
    }
    
    public static void a(final j j) {
        k.a(j);
    }
    
    public static void a(final String s) {
        if (!TextUtils.isEmpty((CharSequence)s)) {
            com.apm.insight.d.a.a(s);
        }
    }
    
    public static void a(final String pathname, final com.apm.insight.a.b b, final com.apm.insight.a.c c) {
        if (!TextUtils.isEmpty((CharSequence)pathname) && new File(pathname).exists()) {
            com.apm.insight.a.a.a().a(pathname, b, c);
        }
    }
    
    public static void a(final String s, final f f) {
        p.b().a(new Runnable(s, f) {
            final String a;
            final f b;
            
            @Override
            public void run() {
                if (com.apm.insight.l.a.b(i.g())) {
                    com.apm.insight.b.d.a(this.a, this.b);
                }
            }
        });
    }
    
    public static void a(final String s, @Nullable final Map<? extends String, ? extends String> map, @Nullable final Map<String, String> map2, @Nullable final g g) {
        if (!TextUtils.isEmpty((CharSequence)s)) {
            com.apm.insight.d.a.a(s, map, map2, g);
        }
    }
    
    public static void a(final String s, @Nullable final Map<? extends String, ? extends String> map, @Nullable final Map<String, String> map2, @Nullable final Map<String, String> map3, @Nullable final g g) {
        if (!TextUtils.isEmpty((CharSequence)s)) {
            com.apm.insight.d.a.a(s, map, map2, map3, g);
        }
    }
    
    @Deprecated
    public static void a(@NonNull final Throwable t) {
        if (!i.i().isReportErrorEnable()) {
            return;
        }
        com.apm.insight.g.a.a(t);
    }
    
    public static void a(final boolean b) {
        i.b(b);
    }
    
    private static boolean a(final Context context) {
        try {
            return new File(com.apm.insight.l.o.j(context), "npth").exists();
        }
        finally {
            return false;
        }
    }
    
    public static void b(final long n) {
        NativeImpl.b(n);
    }
    
    public static void b(final ICrashCallback crashCallback, final CrashType crashType) {
        a().b(crashCallback, crashType);
    }
    
    @Deprecated
    public static void b(final String s) {
        if (!i.i().isReportErrorEnable()) {
            return;
        }
        com.apm.insight.g.a.c(s);
    }
    
    public static void b(final boolean b) {
        i.c(b);
    }
    
    public static boolean b() {
        return o.b;
    }
    
    private static boolean b(final Context context) {
        try {
            return new File(context.getApplicationInfo().nativeLibraryDir, "libnpth.so").exists();
        }
        finally {
            return false;
        }
    }
    
    public static void c(final long n) {
        NativeImpl.c(n);
    }
    
    public static void c(final String s) {
        NativeImpl.b(s);
    }
    
    public static void c(final boolean b) {
        i.d(b);
    }
    
    public static boolean c() {
        return o.c;
    }
    
    public static void d(final boolean b) {
        i.e(b);
    }
    
    public static boolean d() {
        return o.d;
    }
    
    public static boolean e() {
        return o.a;
    }
    
    public static void f() {
        if (o.a && !o.b) {
            final Context g = i.g();
            final a a = com.apm.insight.g.a.a();
            a.a(new b(g));
            a.b(new d(g));
        }
    }
    
    public static void g() {
        if (o.a) {
            com.apm.insight.b.g.a(i.g()).c();
            o.c = true;
        }
    }
    
    private static void g(final boolean b) {
        p.b().a(new Runnable(b) {
            final boolean a;
            
            @Override
            public void run() {
                if (!o.g) {
                    new Handler(Looper.getMainLooper()).post((Runnable)new Runnable(this) {
                        final o$2 a;
                        
                        @Override
                        public void run() {
                            o.g = true;
                            NativeImpl.h();
                        }
                    });
                }
                h(this.a);
            }
        }, 0L);
    }
    
    private static void h(final boolean c) {
        final Context g = i.g();
        com.apm.insight.runtime.a.f.a();
        m.a();
        com.apm.insight.k.a("Npth.initAsync-createCallbackThread");
        final int b = NativeImpl.b();
        com.apm.insight.k.a();
        NativeImpl.c();
        Label_0066: {
            com.apm.insight.d d;
            String s;
            if (o.e) {
                d = com.apm.insight.c.a();
                s = "NativeLibraryLoad faild";
            }
            else {
                if (b >= 0) {
                    break Label_0066;
                }
                d = com.apm.insight.c.a();
                s = "createCallbackThread faild";
            }
            d.a(s);
        }
        com.apm.insight.k.a("Npth.initAsync-NpthDataManager");
        com.apm.insight.e.a.a().a(g);
        com.apm.insight.k.a();
        com.apm.insight.c.a();
        com.apm.insight.k.a("Npth.initAsync-LaunchScanner");
        com.apm.insight.k.i.a(g);
        com.apm.insight.k.a();
        if (c) {
            com.apm.insight.k.a("Npth.initAsync-CrashANRHandler");
            com.apm.insight.b.g.a(g).c();
            com.apm.insight.k.a();
            o.c = c;
        }
        com.apm.insight.k.a("Npth.initAsync-EventUploadQueue");
        com.apm.insight.k.g.a().b();
        com.apm.insight.k.a();
        com.apm.insight.k.a("Npth.initAsync-BlockMonitor");
        com.apm.insight.k.a();
        com.apm.insight.k.a("Npth.initAsync-OriginExceptionMonitor");
        com.apm.insight.k.a();
        NativeImpl.f();
        com.apm.insight.j.a();
        com.apm.insight.k.k.d();
        NativeImpl.j();
        r.a("afterNpthInitAsync", "noValue");
    }
    
    public static boolean h() {
        if (o.a && !o.d && !(o.d = NativeImpl.a(i.g()))) {
            o.e = true;
        }
        return o.d;
    }
    
    public static boolean i() {
        return com.apm.insight.b.c.c();
    }
    
    public static void j() {
        if (o.a) {
            com.apm.insight.b.g.a(i.g()).d();
            o.c = false;
        }
    }
    
    public static boolean k() {
        return com.apm.insight.g.a.b() || NativeImpl.d();
    }
    
    public static boolean l() {
        return com.apm.insight.g.a.c() || NativeImpl.d();
    }
    
    public static boolean m() {
        return com.apm.insight.g.a.b();
    }
    
    public static boolean n() {
        return o.h;
    }
    
    public static void o() {
        o.h = true;
    }
    
    private static boolean q() {
        return false;
    }
}

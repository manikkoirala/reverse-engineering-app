// 
// Decompiled by Procyon v0.6.0
// 

package com.apm.insight.runtime;

import java.io.IOException;
import java.util.Arrays;
import com.apm.insight.l.o;
import com.apm.insight.i;
import java.io.File;

public class b
{
    private static long a = -30000L;
    private static File b;
    
    public static String a(final long n, String c) {
        try {
            final String j = o.j(i.g());
            final StringBuilder sb = new StringBuilder();
            sb.append("apminsight/TrackInfo/");
            sb.append((n - n % 86400000L) / 86400000L);
            sb.append("/");
            sb.append(c);
            c = com.apm.insight.l.i.c(new File(j, sb.toString()));
            return c;
        }
        finally {
            final Throwable t;
            return t.getMessage();
        }
    }
    
    public static void a() {
        final File parent = new File(o.j(i.g()), "apminsight/TrackInfo/");
        final String[] list = parent.list();
        if (list == null) {
            return;
        }
        if (list.length > 5) {
            Arrays.sort(list);
            for (int i = 0; i < list.length - 5; ++i) {
                com.apm.insight.l.i.a(new File(parent, list[i]));
            }
        }
    }
    
    public static void a(final long a) {
        if (a - com.apm.insight.runtime.b.a < 30000L) {
            return;
        }
        com.apm.insight.runtime.b.a = a;
        try {
            com.apm.insight.l.i.a(b(), String.valueOf(System.currentTimeMillis()), false);
        }
        catch (final IOException ex) {}
    }
    
    private static File b() {
        if (com.apm.insight.runtime.b.b == null) {
            final long currentTimeMillis = System.currentTimeMillis();
            final String j = o.j(i.g());
            final StringBuilder sb = new StringBuilder();
            sb.append("apminsight/TrackInfo/");
            sb.append((currentTimeMillis - currentTimeMillis % 86400000L) / 86400000L);
            sb.append("/");
            sb.append(i.f());
            com.apm.insight.runtime.b.b = new File(j, sb.toString());
        }
        return com.apm.insight.runtime.b.b;
    }
}

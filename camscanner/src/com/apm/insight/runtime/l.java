// 
// Decompiled by Procyon v0.6.0
// 

package com.apm.insight.runtime;

import android.os.SystemClock;
import com.apm.insight.b.f;
import com.apm.insight.l.q;
import java.util.ArrayList;
import java.util.List;
import android.util.Printer;

public class l
{
    private static Printer a;
    private static l b;
    private static final Printer f;
    private long c;
    private final List<Printer> d;
    private final List<Printer> e;
    private boolean g;
    
    static {
        f = (Printer)new Printer() {
            public void println(final String s) {
                if (s == null) {
                    return;
                }
                if (s.charAt(0) == '>') {
                    l.a().a(s);
                }
                else if (s.charAt(0) == '<') {
                    l.a().b(s);
                }
                if (l.a != null && l.a != l.f) {
                    l.a.println(s);
                }
            }
        };
    }
    
    private l() {
        this.c = -1L;
        this.d = new ArrayList<Printer>();
        this.e = new ArrayList<Printer>();
        this.g = false;
    }
    
    public static l a() {
        if (l.b == null) {
            synchronized (l.class) {
                if (l.b == null) {
                    l.b = new l();
                }
            }
        }
        return l.b;
    }
    
    private static void a(final List<? extends Printer> list, final String s) {
        if (list != null) {
            if (!list.isEmpty()) {
                try {
                    for (int size = list.size(), i = 0; i < size; ++i) {
                        final Printer printer = list.get(i);
                        if (printer == null) {
                            break;
                        }
                        printer.println(s);
                    }
                }
                finally {
                    final Throwable t;
                    q.a(t);
                }
            }
        }
    }
    
    void a(final String s) {
        com.apm.insight.b.f.a(false);
        this.c = -1L;
        try {
            a(this.d, s);
        }
        catch (final Exception ex) {
            q.a(ex);
        }
    }
    
    void b(final String s) {
        this.c = SystemClock.uptimeMillis();
        try {
            a(this.e, s);
        }
        catch (final Exception ex) {
            q.b(ex);
        }
    }
    
    public boolean b() {
        return this.c != -1L && SystemClock.uptimeMillis() - this.c > 5000L;
    }
}

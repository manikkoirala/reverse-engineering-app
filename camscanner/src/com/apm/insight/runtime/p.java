// 
// Decompiled by Procyon v0.6.0
// 

package com.apm.insight.runtime;

import android.os.HandlerThread;
import android.os.Handler;

public class p
{
    private static volatile u a;
    private static volatile Handler b;
    
    public static HandlerThread a() {
        if (p.a == null) {
            synchronized (p.class) {
                if (p.a == null) {
                    (p.a = new u("default_npth_thread")).b();
                }
            }
        }
        return p.a.c();
    }
    
    public static u b() {
        if (p.a == null) {
            a();
        }
        return p.a;
    }
}

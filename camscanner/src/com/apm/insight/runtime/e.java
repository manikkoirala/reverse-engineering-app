// 
// Decompiled by Procyon v0.6.0
// 

package com.apm.insight.runtime;

import com.apm.insight.l.l;
import androidx.annotation.Nullable;
import com.apm.insight.l.q;
import org.json.JSONObject;
import java.util.HashMap;

public class e
{
    private static HashMap<String, e> a;
    private JSONObject b;
    private JSONObject c;
    private boolean d;
    private String e;
    
    static {
        e.a = new HashMap<String, e>();
    }
    
    public e(final JSONObject jsonObject, final String s) {
        this.b = null;
        this.c = null;
        this.d = false;
        this.e = s;
        this.a(jsonObject);
        com.apm.insight.runtime.e.a.put(this.e, this);
        final StringBuilder sb = new StringBuilder();
        sb.append("after update aid ");
        sb.append(s);
        q.a((Object)sb.toString());
    }
    
    public static void a(final String key, final JSONObject jsonObject) {
        final e e = com.apm.insight.runtime.e.a.get(key);
        if (e != null) {
            e.a(jsonObject);
        }
        else {
            new e(jsonObject, key);
        }
    }
    
    private void a(JSONObject optJSONObject) {
        this.b = optJSONObject;
        if (optJSONObject != null) {
            optJSONObject = optJSONObject.optJSONObject("error_module");
            if (optJSONObject != null) {
                final int optInt = optJSONObject.optInt("switcher");
                final int optInt2 = optJSONObject.optInt("err_sampling_rate");
                boolean d = true;
                if (optInt != 1 || optInt2 != 1) {
                    d = false;
                }
                this.d = d;
            }
        }
    }
    
    public static boolean b(final String key) {
        return e.a.get(key) != null;
    }
    
    @Nullable
    public static JSONObject c(final String key) {
        final e e = com.apm.insight.runtime.e.a.get(key);
        JSONObject a;
        if (e != null) {
            a = e.a();
        }
        else {
            a = null;
        }
        return a;
    }
    
    public static e d(final String key) {
        return e.a.get(key);
    }
    
    public static long e(final String key) {
        final e e = com.apm.insight.runtime.e.a.get(key);
        long n = 3600000L;
        if (e == null) {
            return n;
        }
        try {
            n = Long.decode(l.b(e.a(), "over_all", "get_settings_interval")) * 1000L;
            return n;
        }
        finally {
            n = n;
            return n;
        }
    }
    
    public static boolean f(final String key) {
        final e e = com.apm.insight.runtime.e.a.get(key);
        return e != null && e.b();
    }
    
    public static boolean g(final String key) {
        final e e = com.apm.insight.runtime.e.a.get(key);
        return e != null && e.c();
    }
    
    public static boolean h(final String key) {
        final e e = com.apm.insight.runtime.e.a.get(key);
        return e != null && e.d();
    }
    
    @Nullable
    public JSONObject a() {
        return this.b;
    }
    
    public boolean a(final String s) {
        return this.b != null && this.d;
    }
    
    public boolean b() {
        final JSONObject b = this.b;
        boolean b2 = false;
        if (b != null) {
            b2 = b2;
            if (1 == l.a(b, 0, "crash_module", "switcher")) {
                b2 = true;
            }
        }
        return b2;
    }
    
    public boolean c() {
        final JSONObject b = this.b;
        boolean b2 = false;
        if (b != null) {
            b2 = b2;
            if (1 == l.a(b, 0, "crash_module", "switcher")) {
                b2 = true;
            }
        }
        return b2;
    }
    
    public boolean d() {
        final JSONObject b = this.b;
        boolean b2 = false;
        if (b != null) {
            b2 = b2;
            if (1 == l.a(b, 0, "crash_module", "switcher")) {
                b2 = true;
            }
        }
        return b2;
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.apm.insight.runtime;

import android.os.BaseBundle;
import android.os.Bundle;
import android.content.pm.PackageInfo;
import com.apm.insight.l.a;
import com.apm.insight.l.v;
import java.util.HashMap;
import androidx.annotation.Nullable;
import java.util.Map;
import androidx.annotation.NonNull;
import com.apm.insight.ICommonParams;
import android.content.Context;

public class d
{
    private Context a;
    private ICommonParams b;
    private ICommonParams c;
    
    public d(@NonNull final Context context, @NonNull final ICommonParams commonParams) {
        this(context, commonParams, null);
    }
    
    public d(@NonNull final Context a, @NonNull final ICommonParams b, final d d) {
        this.a = a;
        this.b = b;
        ICommonParams b2;
        if (d == null) {
            b2 = null;
        }
        else {
            b2 = d.b;
        }
        this.c = b2;
    }
    
    @Nullable
    public static String a(final Map<String, Object> map, final String s) {
        if (map != null) {
            final Object value = map.get(s);
            if (value != null) {
                return String.valueOf(value);
            }
        }
        return null;
    }
    
    public static boolean a(final Map<String, Object> map) {
        return map == null || map.isEmpty() || (!map.containsKey("app_version") && !map.containsKey("version_name")) || !map.containsKey("version_code") || !map.containsKey("update_version_code");
    }
    
    public Map<String, Object> a() {
        final Map<String, Object> b = this.b();
        if (a(b, "aid") == null) {
            b.put("aid", 4444);
        }
        return b;
    }
    
    @NonNull
    public Map<String, Object> b() {
        final Integer n = null;
        Map<String, Integer> map = null;
        try {
            final ICommonParams c = this.c;
            Map<String, Object> commonParams;
            if (c != null) {
                commonParams = c.getCommonParams();
            }
            else {
                commonParams = new HashMap<String, Object>();
            }
            try {
                commonParams.putAll(this.b.getCommonParams());
            }
            finally {}
        }
        finally {
            map = null;
        }
        Map<String, Integer> map2 = map;
        if (map == null) {
            final HashMap<String, Integer> hashMap = (HashMap<String, Integer>)(map2 = new HashMap<String, Integer>(4));
            final Throwable t;
            if (t != null) {
                try {
                    hashMap.put("err_info", (Integer)v.a(t));
                }
                finally {
                    map2 = hashMap;
                }
            }
        }
        if (a((Map<String, Object>)map2)) {
            try {
                final PackageInfo packageInfo = this.a.getPackageManager().getPackageInfo(this.a.getPackageName(), 128);
                map2.put("version_name", (Integer)packageInfo.versionName);
                map2.put("version_code", packageInfo.versionCode);
                if (map2.get("update_version_code") == null) {
                    final Bundle metaData = packageInfo.applicationInfo.metaData;
                    Object value = n;
                    if (metaData != null) {
                        value = ((BaseBundle)metaData).get("UPDATE_VERSION_CODE");
                    }
                    Integer value2;
                    if ((value2 = (Integer)value) == null) {
                        value2 = map2.get("version_code");
                    }
                    map2.put("update_version_code", value2);
                    return (Map<String, Object>)map2;
                }
                return (Map<String, Object>)map2;
            }
            finally {
                map2.put("version_name", (Integer)com.apm.insight.l.a.d(this.a));
                map2.put("version_code", com.apm.insight.l.a.e(this.a));
                if (map2.get("update_version_code") == null) {
                    map2.put("update_version_code", map2.get("version_code"));
                    return (Map<String, Object>)map2;
                }
                return (Map<String, Object>)map2;
            }
            return (Map<String, Object>)map2;
        }
        try {
            final String versionName = this.a.getPackageManager().getPackageInfo(this.a.getPackageName(), 128).versionName;
            final StringBuilder sb = new StringBuilder();
            sb.append(this.a.getPackageName());
            sb.append(".BuildConfig");
            final String anObject = (String)Class.forName(sb.toString()).getDeclaredField("VERSION_NAME").get(null);
            if (versionName != null && !versionName.equals(anObject)) {
                map2.put("manifest_version", (Integer)versionName);
            }
            return (Map<String, Object>)map2;
        }
        finally {
            return (Map<String, Object>)map2;
        }
    }
    
    @NonNull
    public ICommonParams c() {
        return this.b;
    }
    
    public String d() {
        try {
            return this.b.getDeviceId();
        }
        finally {
            return "";
        }
    }
    
    public String e() {
        try {
            return String.valueOf(this.b.getCommonParams().get("aid"));
        }
        finally {
            return "4444";
        }
    }
    
    public long f() {
        try {
            return this.b.getUserId();
        }
        finally {
            return 0L;
        }
    }
}

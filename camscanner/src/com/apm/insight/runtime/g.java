// 
// Decompiled by Procyon v0.6.0
// 

package com.apm.insight.runtime;

import com.apm.insight.nativecrash.NativeImpl;
import java.util.Comparator;

public class g
{
    private static final Comparator<a> a;
    private static byte[] b;
    
    static {
        a = new Comparator<a>() {
            public final int a(final a a, final a a2) {
                final int n = a.b + a.c;
                final int n2 = a2.b + a2.c;
                if (n != n2) {
                    int n3;
                    if (n > n2) {
                        n3 = -1;
                    }
                    else {
                        n3 = 1;
                    }
                    return n3;
                }
                return 0;
            }
        };
        g.b = new byte[4096];
    }
    
    public static long a(final int n) {
        return NativeImpl.c(n) * q.a.a();
    }
    
    public static class a
    {
        public String a;
        public int b;
        public int c;
        
        a() {
            this.a = "unknown";
        }
    }
}

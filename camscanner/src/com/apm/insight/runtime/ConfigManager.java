// 
// Decompiled by Procyon v0.6.0
// 

package com.apm.insight.runtime;

import android.text.TextUtils;
import androidx.annotation.Nullable;
import com.apm.insight.k.a;
import com.apm.insight.l.j;
import java.util.Set;
import androidx.annotation.NonNull;
import com.apm.insight.j.b;
import com.apm.insight.i;
import android.content.Context;
import com.apm.insight.ICommonParams;
import com.apm.insight.l.g;
import java.util.concurrent.ThreadPoolExecutor;
import com.apm.insight.e;

public class ConfigManager
{
    public static final long BLOCK_MONITOR_INTERVAL = 1000L;
    private static final long BLOCK_MONITOR_MIN_INTERVAL = 10L;
    public static final String CONFIG_URL_SUFFIX = "/settings/get";
    public static final String EXCEPTION_URL_SUFFIX = "/monitor/collect/c/exception";
    public static final String JAVA_URL_SUFFIX = "/monitor/collect/c/crash";
    private static final long LAUNCH_CRASH_INTERVAL = 8000L;
    public static final String LAUNCH_URL_SUFFIX = "/monitor/collect/c/exception/dump_collection";
    public static final String LOG_TYPE_ALL_STACK = "npth_enable_all_thread_stack";
    public static final String NATIVE_URL_SUFFIX = "/monitor/collect/c/native_bin_crash";
    private String mAlogUploadUrl;
    private String mAsanReportUploadUrl;
    private boolean mBlockMonitorEnable;
    private long mBlockMonitorInterval;
    private String mConfigUrl;
    private String mCoreDumpUrl;
    private e mEncryptImpl;
    private boolean mEnsureEnable;
    private boolean mEnsureWithLogcat;
    private String mExceptionUploadUrl;
    private boolean mIsDebugMode;
    private String mJavaCrashUploadUrl;
    private long mLaunchCrashInterval;
    private String mLaunchCrashUploadUrl;
    private int mLogcatDumpCount;
    private int mLogcatLevel;
    private boolean mNativeCrashMiniDump;
    private String mNativeCrashUploadUrl;
    private String mNativeMemUrl;
    private ThreadPoolExecutor mThreadPoolExecutor;
    private boolean reportErrorEnable;
    
    public ConfigManager() {
        this.reportErrorEnable = true;
        this.mNativeMemUrl = "";
        this.mCoreDumpUrl = "";
        this.mJavaCrashUploadUrl = "";
        this.mLaunchCrashUploadUrl = "";
        this.mExceptionUploadUrl = "";
        this.mConfigUrl = "";
        this.mNativeCrashUploadUrl = "";
        this.mAlogUploadUrl = "";
        this.mAsanReportUploadUrl = "";
        this.mLaunchCrashInterval = 8000L;
        this.mEncryptImpl = new e() {
            final ConfigManager a;
            
            @Override
            public byte[] a(final byte[] array) {
                return g.a(array);
            }
        };
        this.mLogcatDumpCount = 512;
        this.mLogcatLevel = 1;
        this.mNativeCrashMiniDump = true;
        this.mEnsureEnable = true;
        this.mEnsureWithLogcat = false;
        this.mBlockMonitorInterval = 1000L;
        this.mBlockMonitorEnable = false;
        this.mIsDebugMode = false;
    }
    
    public static void setDefaultCommonParams(final ICommonParams commonParams, final Context context) {
        i.a(new d(context, commonParams));
    }
    
    public static void updateDid(final String s) {
        p.b().a(new Runnable(s) {
            final String a;
            
            @Override
            public void run() {
                i.c().a(this.a);
                b.d();
            }
        });
    }
    
    public String getAlogUploadUrl() {
        return this.mAlogUploadUrl;
    }
    
    public String getAsanReportUploadUrl() {
        return this.mAsanReportUploadUrl;
    }
    
    public long getBlockInterval() {
        return this.mBlockMonitorInterval;
    }
    
    public String getConfigUrl() {
        return this.mConfigUrl;
    }
    
    public String getCoreDumpUrl() {
        return this.mCoreDumpUrl;
    }
    
    @NonNull
    public e getEncryptImpl() {
        return this.mEncryptImpl;
    }
    
    public String getExceptionUploadUrl() {
        return this.mExceptionUploadUrl;
    }
    
    public Set<String> getFilterThreadSet() {
        return j.a();
    }
    
    public String getJavaCrashUploadUrl() {
        return this.mJavaCrashUploadUrl;
    }
    
    public long getLaunchCrashInterval() {
        return this.mLaunchCrashInterval;
    }
    
    public String getLaunchCrashUploadUrl() {
        return this.mLaunchCrashUploadUrl;
    }
    
    public int getLogcatDumpCount() {
        return this.mLogcatDumpCount;
    }
    
    public int getLogcatLevel() {
        return this.mLogcatLevel;
    }
    
    public String getNativeCrashUploadUrl() {
        return this.mNativeCrashUploadUrl;
    }
    
    public String getNativeMemUrl() {
        return this.mNativeMemUrl;
    }
    
    public ThreadPoolExecutor getThreadPoolExecutor() {
        return this.mThreadPoolExecutor;
    }
    
    public boolean isApmExists() {
        return a.c();
    }
    
    public boolean isBlockMonitorEnable() {
        return this.mBlockMonitorEnable;
    }
    
    public boolean isCrashIgnored(final String s) {
        try {
            final f f = new f(this, s) {
                final String a;
                final ConfigManager b;
                
                @Nullable
                @Override
                public Object a(final String s) {
                    if (s.equals("md5")) {
                        return this.a;
                    }
                    return super.a(s);
                }
            };
            if (n.a("java_crash_ignore", f)) {
                return true;
            }
            if (com.apm.insight.l.p.b(i.g())) {
                a.d();
                return n.a("java_crash_ignore", f);
            }
            return false;
        }
        finally {
            return false;
        }
    }
    
    public boolean isDebugMode() {
        return this.mIsDebugMode;
    }
    
    public boolean isEnsureEnable() {
        return this.mEnsureEnable;
    }
    
    public boolean isEnsureWithLogcat() {
        return this.mEnsureWithLogcat;
    }
    
    public boolean isNativeCrashMiniDump() {
        return this.mNativeCrashMiniDump;
    }
    
    public boolean isReportErrorEnable() {
        return this.reportErrorEnable;
    }
    
    public void setAlogUploadUrl(final String mAlogUploadUrl) {
        if (TextUtils.isEmpty((CharSequence)mAlogUploadUrl)) {
            return;
        }
        this.mAlogUploadUrl = mAlogUploadUrl;
    }
    
    public void setBlockMonitorEnable(final boolean mBlockMonitorEnable) {
        this.mBlockMonitorEnable = mBlockMonitorEnable;
    }
    
    public void setBlockMonitorInterval(final long mBlockMonitorInterval) {
        this.mBlockMonitorInterval = mBlockMonitorInterval;
    }
    
    public void setConfigUrl(final String mConfigUrl) {
        if (TextUtils.isEmpty((CharSequence)mConfigUrl)) {
            return;
        }
        this.mConfigUrl = mConfigUrl;
    }
    
    public void setCurrentProcessName(final String s) {
        com.apm.insight.l.a.a(s);
    }
    
    public void setDebugMode(final boolean mIsDebugMode) {
        this.mIsDebugMode = mIsDebugMode;
    }
    
    public void setEncryptImpl(final e mEncryptImpl) {
        if (mEncryptImpl != null) {
            this.mEncryptImpl = mEncryptImpl;
        }
    }
    
    public void setEnsureEnable(final boolean mEnsureEnable) {
        this.mEnsureEnable = mEnsureEnable;
    }
    
    public void setEnsureWithLogcat(final boolean mEnsureWithLogcat) {
        this.mEnsureWithLogcat = mEnsureWithLogcat;
    }
    
    public void setJavaCrashUploadUrl(final String mJavaCrashUploadUrl) {
        if (TextUtils.isEmpty((CharSequence)mJavaCrashUploadUrl)) {
            return;
        }
        this.mJavaCrashUploadUrl = mJavaCrashUploadUrl;
    }
    
    public void setLaunchCrashInterval(final long mLaunchCrashInterval) {
        if (mLaunchCrashInterval > 0L) {
            this.mLaunchCrashInterval = mLaunchCrashInterval;
        }
    }
    
    public void setLaunchCrashUrl(String s) {
        if (TextUtils.isEmpty((CharSequence)s)) {
            return;
        }
        this.mExceptionUploadUrl = s;
        final int index = s.indexOf("//");
        if (index == -1) {
            final StringBuilder sb = new StringBuilder();
            sb.append(s.substring(0, s.indexOf("/") + 1));
            sb.append("monitor/collect/c/exception/dump_collection");
            s = sb.toString();
        }
        else {
            final StringBuilder sb2 = new StringBuilder();
            sb2.append(s.substring(0, s.indexOf("/", index + 2) + 1));
            sb2.append("monitor/collect/c/exception/dump_collection");
            s = sb2.toString();
        }
        this.mLaunchCrashUploadUrl = s;
    }
    
    public void setLogcatDumpCount(final int mLogcatDumpCount) {
        if (mLogcatDumpCount > 0) {
            this.mLogcatDumpCount = mLogcatDumpCount;
        }
    }
    
    public void setLogcatLevel(final int mLogcatLevel) {
        if (mLogcatLevel >= 0 && mLogcatLevel <= 4) {
            this.mLogcatLevel = mLogcatLevel;
        }
    }
    
    public void setNativeCrashUrl(final String mNativeCrashUploadUrl) {
        if (TextUtils.isEmpty((CharSequence)mNativeCrashUploadUrl)) {
            return;
        }
        this.mNativeCrashUploadUrl = mNativeCrashUploadUrl;
    }
    
    public void setReportErrorEnable(final boolean reportErrorEnable) {
        this.reportErrorEnable = reportErrorEnable;
    }
    
    public void setThreadPoolExecutor(final ThreadPoolExecutor mThreadPoolExecutor) {
        this.mThreadPoolExecutor = mThreadPoolExecutor;
    }
}

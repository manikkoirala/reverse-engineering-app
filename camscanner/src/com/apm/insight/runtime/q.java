// 
// Decompiled by Procyon v0.6.0
// 

package com.apm.insight.runtime;

import android.system.Os;
import android.system.OsConstants;

public class q
{
    private static long a = -1L;
    
    public static class a
    {
        private static long a = -1L;
        
        public static long a() {
            if (q.a == -1L) {
                q.a = 1000L / b();
            }
            return q.a;
        }
        
        public static long a(long a) {
            final long a2 = q.a.a;
            if (a2 > 0L) {
                return a2;
            }
            final long sysconf = Os.sysconf(OsConstants._SC_CLK_TCK);
            if (sysconf > 0L) {
                a = sysconf;
            }
            return q.a.a = a;
        }
        
        private static long a(final String name, final long n) {
            try {
                return (long)Class.forName("libcore.io.Os").getMethod("sysconf", Integer.TYPE).invoke(Class.forName("libcore.io.Libcore").getField("os").get(null), Class.forName("libcore.io.OsConstants").getField(name).getInt(null));
            }
            finally {
                final Throwable t;
                t.printStackTrace();
                return n;
            }
        }
        
        public static long b() {
            return a(100L);
        }
    }
}

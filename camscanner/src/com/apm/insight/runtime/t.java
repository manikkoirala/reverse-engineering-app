// 
// Decompiled by Procyon v0.6.0
// 

package com.apm.insight.runtime;

import com.apm.insight.i;
import android.text.TextUtils;
import android.content.Context;

public final class t
{
    private String a;
    private int b;
    
    public t(final Context context) {
        this.a = null;
        this.b = -1;
    }
    
    public String a() {
        if (!TextUtils.isEmpty((CharSequence)this.a) && !"0".equals(this.a)) {
            return this.a;
        }
        final String d = i.a().d();
        this.a = d;
        if (!TextUtils.isEmpty((CharSequence)d) && !"0".equals(this.a)) {
            return this.a;
        }
        return this.a = s.a().b();
    }
    
    public void a(final String a) {
        this.a = a;
        s.a().b(a);
    }
    
    public boolean b() {
        return this.a != null;
    }
}

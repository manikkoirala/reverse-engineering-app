// 
// Decompiled by Procyon v0.6.0
// 

package com.apm.insight.runtime.a;

import android.content.Intent;
import android.content.BroadcastReceiver;
import android.content.IntentFilter;
import com.apm.insight.c;
import com.apm.insight.runtime.p;
import android.content.Context;

class d
{
    private int a;
    
    d(final Context context) {
        p.b().a(new Runnable(this, context) {
            final Context a;
            final d b;
            
            @Override
            public void run() {
                try {
                    this.b.a(this.a);
                }
                finally {
                    final Throwable t;
                    c.a().a("NPTH_CATCH", t);
                }
            }
        });
    }
    
    private void a(final Context context) {
        context.registerReceiver((BroadcastReceiver)new a(), new IntentFilter("android.intent.action.BATTERY_CHANGED"));
    }
    
    public int a() {
        return this.a;
    }
    
    private class a extends BroadcastReceiver
    {
        final d a;
        
        private a(final d a) {
            this.a = a;
        }
        
        public void onReceive(final Context context, final Intent intent) {
            try {
                if ("android.intent.action.BATTERY_CHANGED".equals(intent.getAction())) {
                    this.a.a = (int)(intent.getIntExtra("level", 0) * 100.0f / intent.getIntExtra("scale", 100));
                }
            }
            finally {}
        }
    }
}

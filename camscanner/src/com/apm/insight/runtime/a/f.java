// 
// Decompiled by Procyon v0.6.0
// 

package com.apm.insight.runtime.a;

import java.util.Iterator;
import com.apm.insight.entity.Header;
import org.json.JSONArray;
import java.util.List;
import androidx.annotation.Nullable;
import java.util.HashMap;
import androidx.annotation.NonNull;
import com.apm.insight.CrashType;
import java.util.Map;
import android.content.Context;
import android.annotation.SuppressLint;

public class f
{
    @SuppressLint({ "StaticFieldLeak" })
    private static volatile f a;
    private Context b;
    private Map<CrashType, c> c;
    private b d;
    private d e;
    
    private f(@NonNull final Context b) {
        this.c = new HashMap<CrashType, c>();
        this.b = b;
        try {
            this.d = com.apm.insight.runtime.a.b.d();
            this.e = new d(this.b);
        }
        finally {
            final Throwable t;
            com.apm.insight.c.a().a("NPTH_CATCH", t);
        }
    }
    
    @Nullable
    private c a(final CrashType crashType) {
        c c = this.c.get(crashType);
        if (c != null) {
            return c;
        }
        switch (f$1.a[crashType.ordinal()]) {
            case 8: {
                c = new i(this.b, this.d, this.e);
                break;
            }
            case 7: {
                c = new e(this.b, this.d, this.e);
                break;
            }
            case 6: {
                c = new g(this.b, this.d, this.e);
                break;
            }
            case 5: {
                c = new h(this.b, this.d, this.e);
                break;
            }
            case 4: {
                c = new a(this.b, this.d, this.e);
                break;
            }
            case 3: {
                c = new l(this.b, this.d, this.e);
                break;
            }
            case 2: {
                c = new k(this.b, this.d, this.e);
                break;
            }
            case 1: {
                c = new j(this.b, this.d, this.e);
                break;
            }
        }
        if (c != null) {
            this.c.put(crashType, c);
        }
        return c;
    }
    
    public static f a() {
        if (f.a == null) {
            final Context g = com.apm.insight.i.g();
            if (g == null) {
                throw new IllegalArgumentException("NpthBus not init");
            }
            f.a = new f(g);
        }
        return f.a;
    }
    
    public com.apm.insight.entity.a a(final CrashType crashType, final com.apm.insight.entity.a a) {
        if (crashType == null) {
            return a;
        }
        final c a2 = this.a(crashType);
        if (a2 != null) {
            return a2.a(a, null, false);
        }
        return a;
    }
    
    public com.apm.insight.entity.a a(final CrashType crashType, final com.apm.insight.entity.a a, @Nullable final c.a a2, final boolean b) {
        if (crashType == null) {
            return a;
        }
        final c a3 = this.a(crashType);
        if (a3 != null) {
            return a3.a(a, a2, b);
        }
        return a;
    }
    
    public com.apm.insight.entity.a a(final List<com.apm.insight.entity.a> list, final JSONArray jsonArray) {
        if (list != null && !list.isEmpty()) {
            final com.apm.insight.entity.a a = new com.apm.insight.entity.a();
            final JSONArray jsonArray2 = new JSONArray();
            final Iterator iterator = list.iterator();
            while (iterator.hasNext()) {
                jsonArray2.put((Object)((com.apm.insight.entity.a)iterator.next()).h());
            }
            a.a("data", (Object)jsonArray2);
            a.a("all_data", (Object)jsonArray);
            final Header a2 = Header.a(this.b);
            Header.a(a2);
            a2.c();
            a2.d();
            a2.e();
            Header.b(a2);
            a.a(a2);
            return a;
        }
        return null;
    }
}

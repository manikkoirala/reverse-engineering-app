// 
// Decompiled by Procyon v0.6.0
// 

package com.apm.insight.runtime.a;

import java.util.Iterator;
import com.apm.insight.l.w;
import java.util.Arrays;
import android.os.Process;
import com.apm.insight.l.v;
import androidx.annotation.Nullable;
import java.util.List;
import java.util.Map;
import com.apm.insight.l.h;
import android.os.SystemClock;
import com.apm.insight.AttachUserData;
import org.json.JSONObject;
import java.util.HashMap;
import com.apm.insight.entity.a;
import com.apm.insight.i;
import com.apm.insight.ICommonParams;
import android.content.Context;
import com.apm.insight.CrashType;

public abstract class c
{
    protected CrashType a;
    protected Context b;
    protected ICommonParams c;
    protected b d;
    protected d e;
    
    c(final CrashType a, final Context b, final b d, final d e) {
        this.a = a;
        this.b = b;
        this.d = d;
        this.e = e;
        this.c = i.a().c();
    }
    
    private void i(com.apm.insight.entity.a iterator) {
        final List<AttachUserData> a = i.b().a(this.a);
        final HashMap hashMap = new HashMap();
        JSONObject optJSONObject;
        if ((optJSONObject = iterator.h().optJSONObject("custom")) == null) {
            optJSONObject = new JSONObject();
            iterator.a("custom", optJSONObject);
        }
        final int n = 0;
        if (a != null) {
            for (int i = 0; i < a.size(); ++i) {
                try {
                    final AttachUserData attachUserData = a.get(i);
                    final long uptimeMillis = SystemClock.uptimeMillis();
                    com.apm.insight.entity.a.a(optJSONObject, attachUserData.getUserData(this.a));
                    final StringBuilder sb = new StringBuilder();
                    sb.append("custom_cost_");
                    sb.append(attachUserData.getClass().getName());
                    sb.append("_");
                    sb.append(hashMap.size());
                    hashMap.put(sb.toString(), SystemClock.uptimeMillis() - uptimeMillis);
                }
                finally {
                    final Throwable t;
                    com.apm.insight.entity.a.a(optJSONObject, t);
                }
            }
        }
        try {
            optJSONObject.put("fd_count", h.a());
        }
        finally {}
        final List<AttachUserData> b = i.b().b(this.a);
        if (b != null) {
            final JSONObject optJSONObject2 = iterator.h().optJSONObject("custom_long");
            int j = n;
            JSONObject jsonObject;
            if ((jsonObject = optJSONObject2) == null) {
                jsonObject = new JSONObject();
                iterator.a("custom_long", jsonObject);
                j = n;
            }
            while (j < b.size()) {
                try {
                    final AttachUserData attachUserData2 = b.get(j);
                    final long uptimeMillis2 = SystemClock.uptimeMillis();
                    com.apm.insight.entity.a.a(jsonObject, attachUserData2.getUserData(this.a));
                    final StringBuilder sb2 = new StringBuilder();
                    sb2.append("custom_cost_");
                    sb2.append(attachUserData2.getClass().getName());
                    sb2.append("_");
                    sb2.append(hashMap.size());
                    hashMap.put(sb2.toString(), SystemClock.uptimeMillis() - uptimeMillis2);
                }
                finally {
                    final Throwable t2;
                    com.apm.insight.entity.a.a(jsonObject, t2);
                }
                ++j;
            }
        }
        iterator = (com.apm.insight.entity.a)hashMap.entrySet().iterator();
        while (((Iterator)iterator).hasNext()) {
            final Map.Entry<String, V> entry = ((Iterator<Map.Entry<String, V>>)iterator).next();
            try {
                optJSONObject.put((String)entry.getKey(), (Object)entry.getValue());
            }
            finally {}
        }
    }
    
    public com.apm.insight.entity.a a(final int n, final com.apm.insight.entity.a a) {
        com.apm.insight.entity.a a2 = a;
        if (a == null) {
            a2 = new com.apm.insight.entity.a();
        }
        if (n != 0) {
            if (n != 1) {
                if (n != 2) {
                    if (n != 4) {
                        if (n == 5) {
                            this.d(a2);
                        }
                    }
                    else {
                        this.f(a2);
                    }
                }
                else {
                    this.e(a2);
                }
            }
            else {
                this.c(a2);
                this.i(a2);
            }
        }
        else {
            this.b(a2);
        }
        return a2;
    }
    
    public com.apm.insight.entity.a a(final com.apm.insight.entity.a a) {
        return a;
    }
    
    public com.apm.insight.entity.a a(@Nullable com.apm.insight.entity.a o, @Nullable final a a, final boolean b) {
        Object o2 = o;
        if (o == null) {
            o2 = new com.apm.insight.entity.a();
        }
        o = o2;
        int i = 0;
        Object o3 = o2;
        Object o4 = o;
        while (i < this.b()) {
            final long uptimeMillis = SystemClock.uptimeMillis();
            com.apm.insight.entity.a a2 = (com.apm.insight.entity.a)o4;
            if (a != null) {
                try {
                    a.a(i, (com.apm.insight.entity.a)o4);
                }
                finally {
                    final Throwable t;
                    a.a(t);
                    a2 = (com.apm.insight.entity.a)o4;
                }
            }
            try {
                o = this.a(i, a2);
            }
            finally {
                o = a2;
                if (a != null) {
                    final Throwable t2;
                    a.a(t2);
                    o = a2;
                }
            }
            o4 = o;
            Object o5 = o3;
            if (a != null) {
                try {
                    final int b2 = this.b();
                    boolean b3 = true;
                    if (i != b2 - 1) {
                        b3 = false;
                    }
                    o = a.a(i, (com.apm.insight.entity.a)o, b3);
                }
                finally {
                    final Throwable t3;
                    a.a(t3);
                }
                o4 = o;
                o5 = o3;
                if (b) {
                    if (i != 0) {
                        ((com.apm.insight.entity.a)o3).c(((com.apm.insight.entity.a)o).h());
                    }
                    else {
                        o3 = o;
                    }
                    o4 = new com.apm.insight.entity.a();
                    o5 = o3;
                }
            }
            final StringBuilder sb = new StringBuilder();
            sb.append("step_cost_");
            sb.append(i);
            ((com.apm.insight.entity.a)o5).b(sb.toString(), String.valueOf(SystemClock.uptimeMillis() - uptimeMillis));
            ++i;
            o3 = o5;
        }
        return this.a((com.apm.insight.entity.a)o3);
    }
    
    protected boolean a() {
        return false;
    }
    
    public int b() {
        return 6;
    }
    
    public com.apm.insight.entity.a b(final com.apm.insight.entity.a a) {
        a.a(i.p(), i.q());
        if (i.m()) {
            a.a("is_mp", 1);
        }
        while (true) {
            final Throwable t2;
            try {
                a.a(this.c.getPluginInfo());
                break Label_0102;
            }
            finally {
                final HashMap hashMap = new(java.util.HashMap.class)();
                final HashMap hashMap3;
                final HashMap hashMap2 = hashMap3 = hashMap;
                new HashMap();
                final StringBuilder sb = new(java.lang.StringBuilder.class)();
                final StringBuilder sb3;
                final StringBuilder sb2 = sb3 = sb;
                new StringBuilder();
                final StringBuilder sb4 = sb2;
                final String s = "Data fetch failed since source misstake:\n";
                sb4.append(s);
                final StringBuilder sb5 = sb2;
                final Throwable t = t2;
                final String s2 = v.a(t);
                sb5.append(s2);
                final HashMap hashMap4 = hashMap2;
                final StringBuilder sb6 = sb2;
                final String s3 = sb6.toString();
                final int n = 0;
                final Integer n2 = n;
                hashMap4.put(s3, n2);
                final com.apm.insight.entity.a a2 = a;
                final HashMap hashMap5 = hashMap2;
                a2.a(hashMap5);
            }
            try {
                final HashMap hashMap = new(java.util.HashMap.class)();
                final HashMap hashMap3;
                final HashMap hashMap2 = hashMap3 = hashMap;
                new HashMap();
                final StringBuilder sb = new(java.lang.StringBuilder.class)();
                final StringBuilder sb3;
                final StringBuilder sb2 = sb3 = sb;
                new StringBuilder();
                final StringBuilder sb4 = sb2;
                final String s = "Data fetch failed since source misstake:\n";
                sb4.append(s);
                final StringBuilder sb5 = sb2;
                final Throwable t = t2;
                final String s2 = v.a(t);
                sb5.append(s2);
                final HashMap hashMap4 = hashMap2;
                final StringBuilder sb6 = sb2;
                final String s3 = sb6.toString();
                final int n = 0;
                final Integer n2 = n;
                hashMap4.put(s3, n2);
                final com.apm.insight.entity.a a2 = a;
                final HashMap hashMap5 = hashMap2;
                a2.a(hashMap5);
                a.b(i.o());
                a.a("process_name", (Object)com.apm.insight.l.a.c(i.g()));
                return a;
            }
            finally {
                continue;
            }
            break;
        }
    }
    
    public com.apm.insight.entity.a c(final com.apm.insight.entity.a a) {
        if (!com.apm.insight.l.a.b(i.g())) {
            a.a("remote_process", 1);
        }
        a.a("pid", Process.myPid());
        a.a(i.j());
        if (this.c()) {
            final b d = this.d;
            if (d != null) {
                a.a(d);
            }
        }
        try {
            a.a(this.c.getPatchInfo());
        }
        finally {
            try {
                final StringBuilder sb = new StringBuilder();
                sb.append("Data fetch failed since source misstake:\n");
                final Throwable t;
                sb.append(v.a(t));
                a.a(Arrays.asList(sb.toString()));
            }
            finally {}
        }
        final String k = i.k();
        if (k != null) {
            a.a("business", (Object)k);
        }
        a.a("is_background", com.apm.insight.l.a.a(this.b) ^ true);
        return a;
    }
    
    protected boolean c() {
        return true;
    }
    
    public com.apm.insight.entity.a d(final com.apm.insight.entity.a a) {
        if (this.d()) {
            a.b(w.a(this.b));
        }
        return a;
    }
    
    protected boolean d() {
        return true;
    }
    
    public com.apm.insight.entity.a e(final com.apm.insight.entity.a a) {
        final d e = this.e;
        int a2;
        if (e == null) {
            a2 = 0;
        }
        else {
            a2 = e.a();
        }
        a.a("battery", a2);
        a.c(i.b().a());
        return a;
    }
    
    public com.apm.insight.entity.a f(final com.apm.insight.entity.a a) {
        if (this.a()) {
            this.h(a);
        }
        return a;
    }
    
    void g(final com.apm.insight.entity.a a) {
    }
    
    protected void h(final com.apm.insight.entity.a a) {
    }
    
    public interface a
    {
        com.apm.insight.entity.a a(final int p0, final com.apm.insight.entity.a p1);
        
        com.apm.insight.entity.a a(final int p0, final com.apm.insight.entity.a p1, final boolean p2);
        
        void a(final Throwable p0);
    }
}

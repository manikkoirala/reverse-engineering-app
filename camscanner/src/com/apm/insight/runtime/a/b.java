// 
// Decompiled by Procyon v0.6.0
// 

package com.apm.insight.runtime.a;

import java.util.Date;
import java.util.Iterator;
import java.util.Collection;
import org.json.JSONArray;
import android.os.SystemClock;
import android.os.Bundle;
import android.app.Activity;
import android.app.Application$ActivityLifecycleCallbacks;
import com.apm.insight.i;
import org.json.JSONException;
import org.json.JSONObject;
import java.util.ArrayList;
import androidx.annotation.NonNull;
import java.util.LinkedList;
import java.util.List;
import android.content.Context;
import android.app.Application;

public class b
{
    private static boolean a = true;
    private static boolean b = false;
    private static boolean c = false;
    private static int d = 1;
    private static boolean e = false;
    private static long f = -1L;
    private static volatile b z;
    private int A;
    private int B;
    private Application g;
    private Context h;
    private List<String> i;
    private List<Long> j;
    private List<String> k;
    private List<Long> l;
    private LinkedList<a> m;
    private String n;
    private long o;
    private String p;
    private long q;
    private String r;
    private long s;
    private String t;
    private long u;
    private String v;
    private long w;
    private boolean x;
    private long y;
    
    private b(@NonNull final Application application) {
        this.i = new ArrayList<String>();
        this.j = new ArrayList<Long>();
        this.k = new ArrayList<String>();
        this.l = new ArrayList<Long>();
        this.m = new LinkedList<a>();
        this.x = false;
        this.y = -1L;
        this.A = 50;
        this.h = (Context)application;
        this.g = application;
        try {
            this.m();
        }
        finally {}
    }
    
    private a a(final String s, final String s2, final long n) {
        a a;
        if (this.m.size() >= this.A) {
            final a e = this.m.poll();
            if ((a = e) != null) {
                this.m.add(e);
                a = e;
            }
        }
        else {
            a = null;
        }
        a e2;
        if ((e2 = a) == null) {
            e2 = new a(s, s2, n);
            this.m.add(e2);
        }
        return e2;
    }
    
    private JSONObject a(final String s, final long n) {
        final JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("name", (Object)s);
            jsonObject.put("time", n);
            return jsonObject;
        }
        catch (final JSONException ex) {
            return jsonObject;
        }
    }
    
    public static void a() {
        com.apm.insight.runtime.a.b.e = true;
    }
    
    private void a(final String a, final long c, final String b) {
        try {
            final a a2 = this.a(a, b, c);
            a2.b = b;
            a2.a = a;
            a2.c = c;
        }
        finally {}
    }
    
    public static int b() {
        final int d = com.apm.insight.runtime.a.b.d;
        int n = 1;
        if (d == 1) {
            if (com.apm.insight.runtime.a.b.e) {
                n = 2;
            }
            return n;
        }
        return d;
    }
    
    public static long c() {
        return com.apm.insight.runtime.a.b.f;
    }
    
    public static b d() {
        if (com.apm.insight.runtime.a.b.z == null) {
            synchronized (b.class) {
                if (com.apm.insight.runtime.a.b.z == null) {
                    com.apm.insight.runtime.a.b.z = new b(i.h());
                }
            }
        }
        return com.apm.insight.runtime.a.b.z;
    }
    
    private void m() {
        if (this.g != null) {
            this.g.registerActivityLifecycleCallbacks((Application$ActivityLifecycleCallbacks)new Application$ActivityLifecycleCallbacks(this) {
                final b a;
                
                public void onActivityCreated(final Activity activity, final Bundle bundle) {
                    this.a.n = activity.getClass().getName();
                    this.a.o = System.currentTimeMillis();
                    com.apm.insight.runtime.a.b.b = (bundle != null);
                    com.apm.insight.runtime.a.b.c = true;
                    this.a.i.add(this.a.n);
                    this.a.j.add(this.a.o);
                    final b a = this.a;
                    a.a(a.n, this.a.o, "onCreate");
                }
                
                public void onActivityDestroyed(final Activity activity) {
                    final String name = activity.getClass().getName();
                    final int index = this.a.i.indexOf(name);
                    if (index > -1 && index < this.a.i.size()) {
                        this.a.i.remove(index);
                        this.a.j.remove(index);
                    }
                    this.a.k.add(name);
                    final long currentTimeMillis = System.currentTimeMillis();
                    this.a.l.add(currentTimeMillis);
                    this.a.a(name, currentTimeMillis, "onDestroy");
                }
                
                public void onActivityPaused(final Activity activity) {
                    this.a.t = activity.getClass().getName();
                    this.a.u = System.currentTimeMillis();
                    this.a.B--;
                    Label_0094: {
                        if (this.a.B != 0) {
                            if (this.a.B >= 0) {
                                break Label_0094;
                            }
                            this.a.B = 0;
                        }
                        this.a.x = false;
                        com.apm.insight.runtime.a.b.c = false;
                        this.a.y = SystemClock.uptimeMillis();
                    }
                    final b a = this.a;
                    a.a(a.t, this.a.u, "onPause");
                }
                
                public void onActivityResumed(final Activity activity) {
                    this.a.r = activity.getClass().getName();
                    this.a.s = System.currentTimeMillis();
                    this.a.B++;
                    if (!this.a.x) {
                        if (com.apm.insight.runtime.a.b.a) {
                            com.apm.insight.runtime.a.b.a = false;
                            com.apm.insight.runtime.a.b.d = 1;
                            com.apm.insight.runtime.a.b.f = this.a.s;
                        }
                        if (!this.a.r.equals(this.a.t)) {
                            return;
                        }
                        if (com.apm.insight.runtime.a.b.c && !com.apm.insight.runtime.a.b.b) {
                            com.apm.insight.runtime.a.b.d = 4;
                            com.apm.insight.runtime.a.b.f = this.a.s;
                            return;
                        }
                        if (!com.apm.insight.runtime.a.b.c) {
                            com.apm.insight.runtime.a.b.d = 3;
                            com.apm.insight.runtime.a.b.f = this.a.s;
                            return;
                        }
                    }
                    this.a.x = true;
                    final b a = this.a;
                    a.a(a.r, this.a.s, "onResume");
                }
                
                public void onActivitySaveInstanceState(final Activity activity, final Bundle bundle) {
                }
                
                public void onActivityStarted(final Activity activity) {
                    this.a.p = activity.getClass().getName();
                    this.a.q = System.currentTimeMillis();
                    final b a = this.a;
                    a.a(a.p, this.a.q, "onStart");
                }
                
                public void onActivityStopped(final Activity activity) {
                    this.a.v = activity.getClass().getName();
                    this.a.w = System.currentTimeMillis();
                    final b a = this.a;
                    a.a(a.v, this.a.w, "onStop");
                }
            });
        }
    }
    
    private JSONArray n() {
        final JSONArray jsonArray = new JSONArray();
        final List<String> i = this.i;
        if (i == null) {
            return jsonArray;
        }
        if (i.isEmpty()) {
            return jsonArray;
        }
        int n = 0;
        while (true) {
            if (n >= this.i.size()) {
                return jsonArray;
            }
            try {
                jsonArray.put((Object)this.a(this.i.get(n), this.j.get(n)));
                ++n;
                continue;
            }
            finally {
                return jsonArray;
            }
            break;
        }
    }
    
    private JSONArray o() {
        final JSONArray jsonArray = new JSONArray();
        final List<String> k = this.k;
        if (k == null) {
            return jsonArray;
        }
        if (k.isEmpty()) {
            return jsonArray;
        }
        int n = 0;
        while (true) {
            if (n >= this.k.size()) {
                return jsonArray;
            }
            try {
                jsonArray.put((Object)this.a(this.k.get(n), this.l.get(n)));
                ++n;
                continue;
            }
            finally {
                return jsonArray;
            }
            break;
        }
    }
    
    public long e() {
        return SystemClock.uptimeMillis() - this.y;
    }
    
    public boolean f() {
        return this.x;
    }
    
    public JSONObject g() {
        final JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("last_create_activity", (Object)this.a(this.n, this.o));
            jsonObject.put("last_start_activity", (Object)this.a(this.p, this.q));
            jsonObject.put("last_resume_activity", (Object)this.a(this.r, this.s));
            jsonObject.put("last_pause_activity", (Object)this.a(this.t, this.u));
            jsonObject.put("last_stop_activity", (Object)this.a(this.v, this.w));
            jsonObject.put("alive_activities", (Object)this.n());
            jsonObject.put("finish_activities", (Object)this.o());
            return jsonObject;
        }
        catch (final JSONException ex) {
            return jsonObject;
        }
    }
    
    @NonNull
    public String h() {
        return String.valueOf(this.r);
    }
    
    public JSONArray i() {
        final JSONArray jsonArray = new JSONArray();
        final Iterator iterator = new ArrayList(this.m).iterator();
        while (iterator.hasNext()) {
            jsonArray.put((Object)((a)iterator.next()).toString());
        }
        return jsonArray;
    }
    
    private static class a
    {
        String a;
        String b;
        long c;
        
        a(final String a, final String b, final long c) {
            this.b = b;
            this.c = c;
            this.a = a;
        }
        
        @Override
        public String toString() {
            final StringBuilder sb = new StringBuilder();
            sb.append(com.apm.insight.l.b.a().format(new Date(this.c)));
            sb.append(" : ");
            sb.append(this.a);
            sb.append(' ');
            sb.append(this.b);
            return sb.toString();
        }
    }
}

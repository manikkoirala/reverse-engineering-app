// 
// Decompiled by Procyon v0.6.0
// 

package com.apm.insight.runtime;

import java.util.List;
import java.util.HashMap;
import java.util.Map;
import com.apm.insight.ICommonParams;
import android.content.Context;

public class i
{
    public static d a(final Context context) {
        return new d(context, new ICommonParams() {
            @Override
            public Map<String, Object> getCommonParams() {
                return new HashMap<String, Object>();
            }
            
            @Override
            public String getDeviceId() {
                return null;
            }
            
            @Override
            public List<String> getPatchInfo() {
                return null;
            }
            
            @Override
            public Map<String, Integer> getPluginInfo() {
                return null;
            }
            
            @Override
            public String getSessionId() {
                return null;
            }
            
            @Override
            public long getUserId() {
                return 0L;
            }
        });
    }
}

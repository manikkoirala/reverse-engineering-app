// 
// Decompiled by Procyon v0.6.0
// 

package com.apm.insight.runtime;

import com.apm.insight.l.r;
import java.util.Iterator;
import java.util.Map;
import org.json.JSONArray;
import com.apm.insight.c;
import java.io.IOException;
import com.apm.insight.l.l;
import com.apm.insight.l.i;
import java.util.Arrays;
import com.apm.insight.l.o;
import java.io.File;
import java.util.HashMap;
import android.content.Context;

public class h
{
    private Context a;
    private HashMap<String, Long> b;
    private int c;
    private int d;
    
    public h(final Context a) {
        this.b = null;
        this.c = 50;
        this.d = 100;
        this.a = a;
        this.b = this.c();
        this.b();
    }
    
    private void a(final File file) {
        final File g = o.g(this.a);
        file.renameTo(new File(g, String.valueOf(System.currentTimeMillis())));
        final String[] list = g.list();
        if (list == null) {
            return;
        }
        if (list.length > 5) {
            Arrays.sort(list);
            new File(g, list[0]).delete();
        }
    }
    
    private void b() {
        this.c = com.apm.insight.runtime.a.a(this.c, "custom_event_settings", "npth_simple_setting", "crash_limit_issue");
        this.d = com.apm.insight.runtime.a.a(this.d, "custom_event_settings", "npth_simple_setting", "crash_limit_all");
    }
    
    private HashMap<String, Long> c() {
        final File h = o.h(this.a);
        final HashMap hashMap = new HashMap();
        hashMap.put("time", System.currentTimeMillis());
        try {
            final JSONArray b = i.b(h.getAbsolutePath());
            if (l.a(b)) {
                return hashMap;
            }
            final Long decode = Long.decode(b.optString(0, (String)null));
            if (System.currentTimeMillis() - decode > 86400000L) {
                this.a(h);
                return hashMap;
            }
            hashMap.put("time", decode);
            for (int i = 1; i < b.length(); ++i) {
                final String[] split = b.optString(i, "").split(" ");
                if (split.length == 2) {
                    hashMap.put(split[0], Long.decode(split[1]));
                }
            }
            goto Label_0155;
        }
        catch (final IOException ex) {
            goto Label_0155;
        }
        finally {
            final Throwable t;
            com.apm.insight.c.a().a("NPTH_CATCH", t);
        }
    }
    
    public void a() {
        final HashMap<String, Long> b = this.b;
        final Long obj = b.remove("time");
        if (obj == null) {
            com.apm.insight.c.a().a("NPTH_CATCH", new RuntimeException("err times, no time"));
            return;
        }
        final StringBuilder sb = new StringBuilder();
        sb.append(obj);
        sb.append('\n');
        for (final Map.Entry<String, V> entry : b.entrySet()) {
            sb.append(entry.getKey());
            sb.append(' ');
            sb.append(entry.getValue());
            sb.append('\n');
        }
        try {
            i.a(o.h(this.a), sb.toString(), false);
        }
        catch (final IOException ex) {}
    }
    
    public boolean a(final String s) {
        String s2 = s;
        if (s == null) {
            s2 = "default";
        }
        final long longValue = r.a(this.b, "all", 1L);
        return r.a(this.b, s2, 1L) < this.c && longValue < this.d;
    }
}

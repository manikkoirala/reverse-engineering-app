// 
// Decompiled by Procyon v0.6.0
// 

package com.apm.insight.runtime;

import com.apm.insight.i;
import com.apm.insight.MonitorCrash;

public class m
{
    private static MonitorCrash a;
    private static int b = -1;
    private static int c;
    
    public static MonitorCrash a() {
        if (m.a == null) {
            (m.a = MonitorCrash.initSDK(i.g(), "239017", 1030835L, "1.3.8.nourl-alpha.15", "com.apm.insight")).config().setChannel("release");
        }
        return m.a;
    }
    
    public static void a(final Throwable t, final String s) {
        if (i.g() == null) {
            return;
        }
        if (!b()) {
            return;
        }
        a().reportCustomErr(s, "INNER", t);
    }
    
    private static boolean b() {
        if (m.b == -1) {
            m.b = 5;
        }
        final int c = m.c;
        if (c < m.b) {
            m.c = c + 1;
            return true;
        }
        return false;
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.apm.insight.runtime;

import java.util.Iterator;
import com.apm.insight.l.q;
import android.text.TextUtils;
import com.apm.insight.d;
import com.apm.insight.l.l;
import org.json.JSONArray;
import com.apm.insight.c;
import org.json.JSONException;
import java.io.File;
import com.apm.insight.l.o;
import com.apm.insight.i;
import androidx.annotation.Nullable;
import org.json.JSONObject;

public class n
{
    protected static JSONObject a;
    
    static {
        n.a = new JSONObject();
    }
    
    public static void a(@Nullable final JSONObject jsonObject) {
        if (jsonObject == null) {
            return;
        }
        try {
            final String a = com.apm.insight.runtime.a.a(jsonObject);
            final File file = new File(o.j(i.g()), "apminsight/configCrash/configNative");
            if (a != null) {
                com.apm.insight.l.i.a(file, b(n.a = new JSONObject(a)), false);
                goto Label_0080;
            }
            n.a = new JSONObject();
            goto Label_0080;
        }
        catch (final JSONException ex) {
            goto Label_0080;
        }
        finally {
            final Throwable t;
            c.a().a("NPTH_CATCH", t);
        }
    }
    
    public static boolean a(final String s, final f f) {
        if (n.a == null) {
            return false;
        }
        f f2;
        if ((f2 = f) == null) {
            f2 = new f();
        }
        final JSONObject optJSONObject = n.a.optJSONObject(s);
        return optJSONObject != null && !a(optJSONObject.optJSONArray("disable"), f2) && a(optJSONObject.optJSONArray("enable"), f2);
    }
    
    private static boolean a(final JSONArray obj, final f f) {
        if (l.a(obj)) {
            return false;
        }
        for (int i = 0; i < obj.length(); ++i) {
            final JSONObject optJSONObject = obj.optJSONObject(i);
            if (optJSONObject == null) {
                final d a = c.a();
                final StringBuilder sb = new StringBuilder();
                sb.append("err config: ");
                sb.append(obj);
                a.a("NPTH_CATCH", new IllegalArgumentException(sb.toString()));
            }
            else if (a(optJSONObject, f)) {
                return true;
            }
        }
        return false;
    }
    
    private static boolean a(final JSONObject jsonObject, final f f) {
        final Iterator keys = jsonObject.keys();
        boolean b = false;
        while (keys.hasNext()) {
            final String s = keys.next();
            if (TextUtils.isEmpty((CharSequence)s)) {
                continue;
            }
            StringBuilder sb = null;
            Label_0077: {
                if (s.startsWith("header_")) {
                    if (!a(jsonObject.optJSONObject(s), f.b(s.substring(7)))) {
                        sb = new StringBuilder();
                        break Label_0077;
                    }
                }
                else if (s.startsWith("java_")) {
                    if (!a(jsonObject.optJSONObject(s), f.a(s.substring(5)))) {
                        sb = new StringBuilder();
                        break Label_0077;
                    }
                }
                else {
                    final StringBuilder sb2 = new StringBuilder();
                    sb2.append("no rules match ");
                    sb2.append(s);
                    q.a((Object)sb2.toString());
                }
                b = true;
                continue;
            }
            sb.append("not match ");
            sb.append(s);
            q.a((Object)sb.toString());
            return false;
        }
        return b;
    }
    
    private static boolean a(final JSONObject jsonObject, final Object obj) {
        final JSONArray optJSONArray = jsonObject.optJSONArray("values");
        if (optJSONArray.length() == 0) {
            return false;
        }
        final String optString = jsonObject.optString("op");
        final String value = String.valueOf(obj);
        if (optString.equals("=")) {
            return value.equals(String.valueOf(optJSONArray.opt(0)));
        }
        if (optString.equals("in")) {
            for (int i = 0; i < optJSONArray.length(); ++i) {
                if (String.valueOf(optJSONArray.opt(i)).equals(value)) {
                    return true;
                }
            }
        }
        return false;
    }
    
    private static JSONArray b(final JSONArray obj, final f f) {
        final JSONArray jsonArray = new JSONArray();
        if (l.a(obj)) {
            return jsonArray;
        }
        for (int i = 0; i < obj.length(); ++i) {
            final JSONObject optJSONObject = obj.optJSONObject(i);
            if (optJSONObject == null) {
                final d a = c.a();
                final StringBuilder sb = new StringBuilder();
                sb.append("err config: ");
                sb.append(obj);
                a.a("NPTH_CATCH", new IllegalArgumentException(sb.toString()));
            }
            else if (a(optJSONObject, f)) {
                jsonArray.put((Object)optJSONObject);
            }
        }
        return jsonArray;
    }
    
    public static JSONObject b(final JSONObject jsonObject) {
        final Iterator keys = jsonObject.keys();
        final f f = new f();
        final JSONObject jsonObject2 = new JSONObject();
        while (keys.hasNext()) {
            final String str = keys.next();
            if ("configType".equals(str)) {
                continue;
            }
            final JSONObject optJSONObject = jsonObject.optJSONObject(str);
            if (optJSONObject == null) {
                final d a = c.a();
                final StringBuilder sb = new StringBuilder();
                sb.append("err config with key: ");
                sb.append(str);
                a.a("NPTH_CATCH", new IllegalArgumentException(sb.toString()));
            }
            else {
                StringBuilder sb2;
                String str2;
                if (a(optJSONObject.optJSONArray("disable"), f)) {
                    sb2 = new StringBuilder();
                    str2 = "match diable ";
                }
                else {
                    final JSONArray b = b(optJSONObject.optJSONArray("enable"), f);
                    if (!l.a(b)) {
                        try {
                            jsonObject2.put(str, (Object)new JSONObject().put("enable", (Object)b));
                        }
                        catch (final JSONException ex) {}
                        continue;
                    }
                    sb2 = new StringBuilder();
                    str2 = "not match ";
                }
                sb2.append(str2);
                sb2.append(str);
                q.a((Object)sb2.toString());
            }
        }
        return jsonObject2;
    }
}

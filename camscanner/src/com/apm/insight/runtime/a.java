// 
// Decompiled by Procyon v0.6.0
// 

package com.apm.insight.runtime;

import com.apm.insight.entity.b;
import com.apm.insight.k.k;
import com.apm.insight.l.q;
import org.json.JSONArray;
import com.apm.insight.i;
import androidx.annotation.Nullable;
import org.json.JSONObject;
import com.apm.insight.l.l;
import java.util.concurrent.ConcurrentLinkedQueue;

public class a
{
    private static ConcurrentLinkedQueue<Object> a;
    
    static {
        com.apm.insight.runtime.a.a = new ConcurrentLinkedQueue<Object>();
    }
    
    public static int a(final int n, final String... array) {
        return l.a(a(), n, array);
    }
    
    public static int a(final String... array) {
        return l.a(a(), -1, array);
    }
    
    @Nullable
    public static String a(@Nullable JSONObject optJSONObject) {
        if (optJSONObject == null) {
            return null;
        }
        optJSONObject = optJSONObject.optJSONObject("exception_modules");
        if (optJSONObject != null) {
            return optJSONObject.optString("npth");
        }
        return null;
    }
    
    @Nullable
    public static JSONObject a() {
        return e.c(i.a().e());
    }
    
    @Nullable
    public static JSONObject a(final JSONArray jsonArray, final String s) {
        if (jsonArray != null) {
            if (jsonArray.length() != 0) {
                for (int i = 0; i < jsonArray.length(); ++i) {
                    final JSONObject optJSONObject = jsonArray.optJSONObject(i).optJSONObject(s);
                    if (optJSONObject != null) {
                        return optJSONObject;
                    }
                }
            }
        }
        return null;
    }
    
    public static void a(final JSONArray obj, final boolean b) {
        if (obj == null) {
            return;
        }
        final StringBuilder sb = new StringBuilder();
        sb.append("fromnet ");
        sb.append(b);
        sb.append(" : ");
        sb.append(obj);
        q.a("apmconfig", sb.toString());
        if (b) {
            k.f();
        }
        int n = 0;
    Label_0157_Outer:
        while (true) {
            Label_0163: {
                if (n >= obj.length()) {
                    break Label_0163;
                }
                while (true) {
                    try {
                        final JSONObject optJSONObject = obj.optJSONObject(n);
                        final String str = optJSONObject.keys().next();
                        final JSONObject optJSONObject2 = optJSONObject.optJSONObject(str);
                        final StringBuilder sb2 = new StringBuilder();
                        sb2.append("update config ");
                        sb2.append(str);
                        sb2.append(" : ");
                        sb2.append(optJSONObject2);
                        q.a((Object)sb2.toString());
                        e.a(str, optJSONObject2);
                        if (b) {
                            k.a(str);
                        }
                        ++n;
                        continue Label_0157_Outer;
                        com.apm.insight.runtime.n.a(a(obj, String.valueOf(i.a().e())));
                        iftrue(Label_0188:)(!b);
                        k.a(false, obj);
                        Label_0188:;
                    }
                    finally {
                        continue;
                    }
                    break;
                }
            }
        }
    }
    
    public static boolean a(final Object o) {
        final String b = com.apm.insight.entity.b.b(o);
        return b != null && e.b(b);
    }
    
    public static boolean a(final Object o, final String s) {
        final String b = com.apm.insight.entity.b.b(o);
        if (b != null) {
            final e d = e.d(b);
            return d != null && d.a(s);
        }
        return false;
    }
    
    public static boolean a(final String s) {
        if (!e.b(s)) {
            com.apm.insight.k.a.b();
        }
        return e.f(s);
    }
    
    public static boolean b() {
        return o.e();
    }
    
    public static boolean b(final String s) {
        if (!e.b(s)) {
            com.apm.insight.k.a.b();
        }
        return e.h(s);
    }
    
    @Nullable
    public static JSONArray c() {
        return l.a(a(), "custom_event_settings", "npth_simple_setting", "max_utm_thread_ignore");
    }
    
    public static boolean c(final String s) {
        if (!e.b(s)) {
            com.apm.insight.k.a.b();
        }
        return e.g(s);
    }
    
    public static boolean d() {
        final int a = a("custom_event_settings", "npth_simple_setting", "disable_looper_monitor");
        boolean b = true;
        if (a != 1) {
            b = false;
        }
        return b;
    }
    
    public static boolean e() {
        final int a = a("custom_event_settings", "npth_simple_setting", "enable_all_thread_stack_native");
        boolean b = true;
        if (a != 1) {
            b = false;
        }
        return b;
    }
    
    public static boolean f() {
        final int a = a("custom_event_settings", "npth_simple_setting", "anr_with_traces_txt");
        boolean b = true;
        if (a != 1) {
            b = false;
        }
        return b;
    }
    
    public static boolean g() {
        final int a = a("custom_event_settings", "npth_simple_setting", "upload_crash_crash");
        boolean b = true;
        if (a != 1) {
            b = false;
        }
        return b;
    }
    
    public static boolean h() {
        final int a = a("custom_event_settings", "npth_simple_setting", "force_apm_crash");
        boolean b = true;
        if (a != 1) {
            b = false;
        }
        return b;
    }
    
    public static boolean i() {
        final int a = a("custom_event_settings", "npth_simple_setting", "enable_killed_anr");
        boolean b = true;
        if (a != 1) {
            b = false;
        }
        return b;
    }
    
    public static boolean j() {
        final int a = a("custom_event_settings", "npth_simple_setting", "enable_anr_all_process_trace");
        boolean b = true;
        if (a != 1) {
            b = false;
        }
        return b;
    }
}

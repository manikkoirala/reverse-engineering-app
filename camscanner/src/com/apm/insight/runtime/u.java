// 
// Decompiled by Procyon v0.6.0
// 

package com.apm.insight.runtime;

import android.os.SystemClock;
import androidx.annotation.Nullable;
import java.util.concurrent.ConcurrentLinkedQueue;
import android.os.Handler;
import java.util.Queue;
import android.os.HandlerThread;
import android.os.Message;

public class u
{
    static final b<d, Runnable> a;
    static final b<Message, Runnable> b;
    private final HandlerThread c;
    private final Queue<d> d;
    private final Queue<Message> e;
    private volatile Handler f;
    private final Object g;
    
    static {
        a = (b)new b<d, Runnable>() {};
        b = (b)new b<Message, Runnable>() {};
    }
    
    public u(final String s) {
        this.d = new ConcurrentLinkedQueue<d>();
        this.e = new ConcurrentLinkedQueue<Message>();
        this.g = new Object();
        this.c = new c(s);
    }
    
    private Message b(final Runnable runnable) {
        return Message.obtain(this.f, runnable);
    }
    
    @Nullable
    public Handler a() {
        return this.f;
    }
    
    public final boolean a(final Message message, final long n) {
        long n2 = n;
        if (n < 0L) {
            n2 = 0L;
        }
        return this.b(message, SystemClock.uptimeMillis() + n2);
    }
    
    public final boolean a(final Runnable runnable) {
        return this.a(this.b(runnable), 0L);
    }
    
    public final boolean a(final Runnable runnable, final long n) {
        return this.a(this.b(runnable), n);
    }
    
    public void b() {
        ((Thread)this.c).start();
    }
    
    public final boolean b(final Message message, final long n) {
        if (this.f == null) {
            synchronized (this.g) {
                if (this.f == null) {
                    this.d.add(new d(message, n));
                    return true;
                }
            }
        }
        try {
            return this.f.sendMessageAtTime(message, n);
        }
        finally {
            return true;
        }
    }
    
    public HandlerThread c() {
        return this.c;
    }
    
    class a implements Runnable
    {
        final u a;
        
        a(final u a) {
            this.a = a;
        }
        
        void a() {
            while (!this.a.d.isEmpty()) {
                final d d = this.a.d.poll();
                if (this.a.f != null) {
                    try {
                        this.a.f.sendMessageAtTime(d.a, d.b);
                    }
                    finally {}
                }
            }
        }
        
        void b() {
            while (!this.a.e.isEmpty()) {
                if (this.a.f != null) {
                    try {
                        this.a.f.sendMessageAtFrontOfQueue((Message)this.a.e.poll());
                    }
                    finally {}
                }
            }
        }
        
        @Override
        public void run() {
            this.b();
            this.a();
        }
    }
    
    public interface b<A, B>
    {
    }
    
    class c extends HandlerThread
    {
        volatile int a;
        volatile boolean b;
        final u c;
        
        c(final u c, final String s) {
            this.c = c;
            super(s);
            this.a = 0;
            this.b = false;
        }
        
        protected void onLooperPrepared() {
            // 
            // This method could not be decompiled.
            // 
            // Original Bytecode:
            // 
            //     1: invokespecial   android/os/HandlerThread.onLooperPrepared:()V
            //     4: aload_0        
            //     5: getfield        com/apm/insight/runtime/u$c.c:Lcom/apm/insight/runtime/u;
            //     8: invokestatic    com/apm/insight/runtime/u.a:(Lcom/apm/insight/runtime/u;)Ljava/lang/Object;
            //    11: astore_1       
            //    12: aload_1        
            //    13: monitorenter   
            //    14: aload_0        
            //    15: getfield        com/apm/insight/runtime/u$c.c:Lcom/apm/insight/runtime/u;
            //    18: astore_3       
            //    19: new             Landroid/os/Handler;
            //    22: astore_2       
            //    23: aload_2        
            //    24: invokespecial   android/os/Handler.<init>:()V
            //    27: aload_3        
            //    28: aload_2        
            //    29: invokestatic    com/apm/insight/runtime/u.a:(Lcom/apm/insight/runtime/u;Landroid/os/Handler;)Landroid/os/Handler;
            //    32: pop            
            //    33: aload_1        
            //    34: monitorexit    
            //    35: aload_0        
            //    36: getfield        com/apm/insight/runtime/u$c.c:Lcom/apm/insight/runtime/u;
            //    39: invokestatic    com/apm/insight/runtime/u.b:(Lcom/apm/insight/runtime/u;)Landroid/os/Handler;
            //    42: new             Lcom/apm/insight/runtime/u$a;
            //    45: dup            
            //    46: aload_0        
            //    47: getfield        com/apm/insight/runtime/u$c.c:Lcom/apm/insight/runtime/u;
            //    50: invokespecial   com/apm/insight/runtime/u$a.<init>:(Lcom/apm/insight/runtime/u;)V
            //    53: invokevirtual   android/os/Handler.post:(Ljava/lang/Runnable;)Z
            //    56: pop            
            //    57: invokestatic    android/os/Looper.loop:()V
            //    60: goto            57
            //    63: astore_1       
            //    64: invokestatic    com/apm/insight/i.g:()Landroid/content/Context;
            //    67: invokestatic    com/apm/insight/b/g.a:(Landroid/content/Context;)Lcom/apm/insight/b/g;
            //    70: invokevirtual   com/apm/insight/b/g.a:()Lcom/apm/insight/b/b;
            //    73: invokevirtual   com/apm/insight/b/b.c:()V
            //    76: aload_0        
            //    77: getfield        com/apm/insight/runtime/u$c.a:I
            //    80: iconst_5       
            //    81: if_icmpge       96
            //    84: invokestatic    com/apm/insight/c.a:()Lcom/apm/insight/d;
            //    87: ldc             "NPTH_CATCH"
            //    89: aload_1        
            //    90: invokevirtual   com/apm/insight/d.a:(Ljava/lang/String;Ljava/lang/Throwable;)V
            //    93: goto            127
            //    96: aload_0        
            //    97: getfield        com/apm/insight/runtime/u$c.b:Z
            //   100: ifne            127
            //   103: aload_0        
            //   104: iconst_1       
            //   105: putfield        com/apm/insight/runtime/u$c.b:Z
            //   108: invokestatic    com/apm/insight/c.a:()Lcom/apm/insight/d;
            //   111: astore_2       
            //   112: new             Ljava/lang/RuntimeException;
            //   115: astore_1       
            //   116: aload_1        
            //   117: invokespecial   java/lang/RuntimeException.<init>:()V
            //   120: aload_2        
            //   121: ldc             "NPTH_ERR_MAX"
            //   123: aload_1        
            //   124: invokevirtual   com/apm/insight/d.a:(Ljava/lang/String;Ljava/lang/Throwable;)V
            //   127: aload_0        
            //   128: aload_0        
            //   129: getfield        com/apm/insight/runtime/u$c.a:I
            //   132: iconst_1       
            //   133: iadd           
            //   134: putfield        com/apm/insight/runtime/u$c.a:I
            //   137: goto            57
            //   140: astore_2       
            //   141: aload_1        
            //   142: monitorexit    
            //   143: aload_2        
            //   144: athrow         
            //   145: astore_1       
            //   146: goto            57
            //    Exceptions:
            //  Try           Handler
            //  Start  End    Start  End    Type
            //  -----  -----  -----  -----  ----
            //  14     35     140    145    Any
            //  57     60     63     140    Any
            //  64     93     145    149    Any
            //  96     112    145    149    Any
            //  112    127    145    149    Any
            //  127    137    145    149    Any
            //  141    143    140    145    Any
            // 
            // The error that occurred was:
            // 
            // java.lang.IllegalStateException: Expression is linked from several locations: Label_0096:
            //     at com.strobel.decompiler.ast.Error.expressionLinkedFromMultipleLocations(Error.java:27)
            //     at com.strobel.decompiler.ast.AstOptimizer.mergeDisparateObjectInitializations(AstOptimizer.java:2604)
            //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:235)
            //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:42)
            //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:206)
            //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:93)
            //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethodBody(AstBuilder.java:868)
            //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethod(AstBuilder.java:761)
            //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addTypeMembers(AstBuilder.java:638)
            //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeCore(AstBuilder.java:605)
            //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeNoCache(AstBuilder.java:195)
            //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addTypeMembers(AstBuilder.java:662)
            //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeCore(AstBuilder.java:605)
            //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeNoCache(AstBuilder.java:195)
            //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createType(AstBuilder.java:162)
            //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addType(AstBuilder.java:137)
            //     at com.strobel.decompiler.languages.java.JavaLanguage.buildAst(JavaLanguage.java:71)
            //     at com.strobel.decompiler.languages.java.JavaLanguage.decompileType(JavaLanguage.java:59)
            //     at com.strobel.decompiler.DecompilerDriver.decompileType(DecompilerDriver.java:333)
            //     at com.strobel.decompiler.DecompilerDriver.decompileJar(DecompilerDriver.java:254)
            //     at com.strobel.decompiler.DecompilerDriver.main(DecompilerDriver.java:144)
            // 
            throw new IllegalStateException("An error occurred while decompiling this method.");
        }
    }
    
    static class d
    {
        Message a;
        long b;
        
        d(final Message a, final long b) {
            this.a = a;
            this.b = b;
        }
    }
}

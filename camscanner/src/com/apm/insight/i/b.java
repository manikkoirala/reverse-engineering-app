// 
// Decompiled by Procyon v0.6.0
// 

package com.apm.insight.i;

import java.io.IOException;
import com.apm.insight.l.r;
import org.json.JSONArray;
import org.json.JSONObject;
import com.apm.insight.b.g;
import com.apm.insight.runtime.k;
import com.apm.insight.nativecrash.NativeImpl;
import com.apm.insight.Npth;
import android.os.Process;
import android.os.SystemClock;
import com.apm.insight.l.v;
import com.apm.insight.CrashType;
import com.apm.insight.runtime.a.f;
import com.apm.insight.l.i;
import com.apm.insight.g.a;
import com.apm.insight.l.o;
import java.io.File;
import androidx.annotation.NonNull;
import android.content.Context;
import com.apm.insight.g.c;

public class b implements c
{
    private Context a;
    
    public b(@NonNull final Context a) {
        this.a = a;
    }
    
    private void b(long l, final Thread thread, final Throwable t, final String child, final File file, final String s, final boolean b) {
        synchronized (this) {
            final File file2 = new File(o.a(this.a), child);
            com.apm.insight.g.a.a().a(file2.getName());
            file2.mkdirs();
            i.f(file2);
            final com.apm.insight.entity.a a = f.a().a(CrashType.LAUNCH, null, new com.apm.insight.runtime.a.c.a(this, t, v.c(t), l, s, b, thread, child, file2) {
                long a = 0L;
                final Throwable b;
                final boolean c;
                final long d;
                final String e;
                final boolean f;
                final Thread g;
                final String h;
                final File i;
                final b j;
                
                @Override
                public com.apm.insight.entity.a a(final int n, final com.apm.insight.entity.a a) {
                    this.a = SystemClock.uptimeMillis();
                    String s5;
                    String s6;
                    if (n != 0) {
                        if (n == 1) {
                            a.a("timestamp", this.d);
                            a.a("main_process", com.apm.insight.l.a.b(this.j.a));
                            a.a("crash_type", CrashType.JAVA);
                            final Thread g = this.g;
                            String name;
                            if (g != null) {
                                name = g.getName();
                            }
                            else {
                                name = "";
                            }
                            a.a("crash_thread_name", (Object)name);
                            a.a("tid", Process.myTid());
                            final boolean hasCrashWhenJavaCrash = Npth.hasCrashWhenJavaCrash();
                            final String s = "true";
                            String s2;
                            if (hasCrashWhenJavaCrash) {
                                s2 = "true";
                            }
                            else {
                                s2 = "false";
                            }
                            a.a("crash_after_crash", s2);
                            String s3;
                            if (NativeImpl.d()) {
                                s3 = s;
                            }
                            else {
                                s3 = "false";
                            }
                            a.a("crash_after_native", s3);
                            a.a().a(this.g, this.b, true, a);
                            return a;
                        }
                        if (n != 2) {
                            Object o;
                            String s4;
                            if (n != 3) {
                                if (n != 4) {
                                    if (n != 5) {
                                        return a;
                                    }
                                    o = this.h;
                                    s4 = "crash_uuid";
                                }
                                else {
                                    if (!this.c) {
                                        com.apm.insight.l.a.a(this.j.a, a.h());
                                        return a;
                                    }
                                    return a;
                                }
                            }
                            else {
                                final JSONObject b = v.b(Thread.currentThread().getName());
                                if (b != null) {
                                    a.a("all_thread_stacks", b);
                                }
                                o = k.b(com.apm.insight.i.f());
                                s4 = "logcat";
                            }
                            a.a(s4, o);
                            return a;
                        }
                        if (this.c) {
                            com.apm.insight.l.a.a(this.j.a, a.h());
                        }
                        a.a("launch_did", (Object)a.a(this.j.a));
                        final JSONArray c = com.apm.insight.b.g.b().c();
                        final long uptimeMillis = SystemClock.uptimeMillis();
                        final JSONObject a2 = com.apm.insight.b.g.b().a(uptimeMillis).a();
                        final JSONArray a3 = com.apm.insight.b.k.a(100, uptimeMillis);
                        a.a("history_message", (Object)c);
                        a.a("current_message", a2);
                        a.a("pending_messages", (Object)a3);
                        a.a("disable_looper_monitor", String.valueOf(com.apm.insight.runtime.a.d()));
                        s5 = String.valueOf(com.apm.insight.c.b.a());
                        s6 = "npth_force_apm_crash";
                    }
                    else {
                        a.a("stack", (Object)v.a(this.b));
                        a.a("event_type", (Object)"start_crash");
                        a.a("isOOM", this.c);
                        a.a("crash_time", this.d);
                        a.a("launch_mode", com.apm.insight.runtime.a.b.b());
                        a.a("launch_time", com.apm.insight.runtime.a.b.c());
                        final String e = this.e;
                        if (e == null) {
                            return a;
                        }
                        a.a("crash_md5", (Object)e);
                        a.a("crash_md5", this.e);
                        final boolean f = this.f;
                        if (!f) {
                            return a;
                        }
                        s5 = String.valueOf(f);
                        s6 = "has_ignore";
                    }
                    a.a(s6, s5);
                    return a;
                }
                
                @Override
                public com.apm.insight.entity.a a(final int i, final com.apm.insight.entity.a a, final boolean b) {
                    if (r.a(r.b(i))) {
                        return a;
                    }
                    try {
                        final File j = this.i;
                        final StringBuilder sb = new StringBuilder();
                        sb.append(this.i.getName());
                        sb.append(".");
                        sb.append(i);
                        i.a(new File(j, sb.toString()), a.h(), false);
                    }
                    catch (final IOException ex) {
                        ex.printStackTrace();
                    }
                    return a;
                }
                
                @Override
                public void a(final Throwable t) {
                }
            }, true);
            l = System.currentTimeMillis() - l;
            try {
                a.a("crash_type", "normal");
                a.b("crash_cost", String.valueOf(l));
                a.a("crash_cost", String.valueOf(l / 1000L));
            }
            finally {
                final Throwable t2;
                com.apm.insight.c.a().a("NPTH_CATCH", t2);
            }
            if (r.a(4)) {
                return;
            }
            if (!Npth.isStopUpload() && !r.a(2048)) {
                return;
            }
        }
    }
    
    @Override
    public void a(final long n, final Thread thread, final Throwable t, final String s, final File file, final String s2, final boolean b) {
        this.b(n, thread, t, s, file, s2, b);
    }
    
    @Override
    public boolean a(final Throwable t) {
        return true;
    }
}

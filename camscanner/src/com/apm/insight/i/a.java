// 
// Decompiled by Procyon v0.6.0
// 

package com.apm.insight.i;

import android.text.TextUtils;
import android.annotation.SuppressLint;
import android.content.Context;
import java.util.UUID;

public class a
{
    private static volatile UUID a;
    private static String b = "";
    
    @SuppressLint({ "MissingPermission", "HardwareIds" })
    private a(final Context p0) {
        // 
        // This method could not be decompiled.
        // 
        // Original Bytecode:
        // 
        //     1: invokespecial   java/lang/Object.<init>:()V
        //     4: getstatic       com/apm/insight/i/a.a:Ljava/util/UUID;
        //     7: ifnonnull       110
        //    10: ldc             Lcom/apm/insight/i/a;.class
        //    12: monitorenter   
        //    13: getstatic       com/apm/insight/i/a.a:Ljava/util/UUID;
        //    16: ifnonnull       98
        //    19: invokestatic    com/apm/insight/runtime/s.a:()Lcom/apm/insight/runtime/s;
        //    22: astore_3       
        //    23: aconst_null    
        //    24: astore_2       
        //    25: aload_3        
        //    26: aconst_null    
        //    27: invokevirtual   com/apm/insight/runtime/s.a:(Ljava/lang/String;)Ljava/lang/String;
        //    30: astore_3       
        //    31: aload_3        
        //    32: ifnull          45
        //    35: aload_3        
        //    36: invokestatic    java/util/UUID.fromString:(Ljava/lang/String;)Ljava/util/UUID;
        //    39: putstatic       com/apm/insight/i/a.a:Ljava/util/UUID;
        //    42: goto            98
        //    45: aload_1        
        //    46: invokevirtual   android/content/Context.getContentResolver:()Landroid/content/ContentResolver;
        //    49: ldc             "android_id"
        //    51: invokestatic    android/provider/Settings$Secure.getString:(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;
        //    54: astore_1       
        //    55: goto            61
        //    58: astore_1       
        //    59: aload_2        
        //    60: astore_1       
        //    61: aload_1        
        //    62: ifnull          78
        //    65: aload_1        
        //    66: ldc             "utf8"
        //    68: invokevirtual   java/lang/String.getBytes:(Ljava/lang/String;)[B
        //    71: invokestatic    java/util/UUID.nameUUIDFromBytes:([B)Ljava/util/UUID;
        //    74: astore_1       
        //    75: goto            82
        //    78: invokestatic    java/util/UUID.randomUUID:()Ljava/util/UUID;
        //    81: astore_1       
        //    82: aload_1        
        //    83: putstatic       com/apm/insight/i/a.a:Ljava/util/UUID;
        //    86: invokestatic    com/apm/insight/runtime/s.a:()Lcom/apm/insight/runtime/s;
        //    89: getstatic       com/apm/insight/i/a.a:Ljava/util/UUID;
        //    92: invokevirtual   java/util/UUID.toString:()Ljava/lang/String;
        //    95: invokevirtual   com/apm/insight/runtime/s.c:(Ljava/lang/String;)V
        //    98: ldc             Lcom/apm/insight/i/a;.class
        //   100: monitorexit    
        //   101: goto            110
        //   104: astore_1       
        //   105: ldc             Lcom/apm/insight/i/a;.class
        //   107: monitorexit    
        //   108: aload_1        
        //   109: athrow         
        //   110: return         
        //   111: astore_1       
        //   112: goto            86
        //   115: astore_1       
        //   116: goto            98
        //    Exceptions:
        //  Try           Handler
        //  Start  End    Start  End    Type
        //  -----  -----  -----  -----  ----
        //  13     23     104    110    Any
        //  25     31     104    110    Any
        //  35     42     104    110    Any
        //  45     55     58     61     Any
        //  65     75     111    115    Any
        //  78     82     111    115    Any
        //  82     86     111    115    Any
        //  86     98     115    119    Any
        //  98     101    104    110    Any
        //  105    108    104    110    Any
        // 
        // The error that occurred was:
        // 
        // java.lang.IndexOutOfBoundsException: Index 60 out of bounds for length 60
        //     at java.base/jdk.internal.util.Preconditions.outOfBounds(Preconditions.java:64)
        //     at java.base/jdk.internal.util.Preconditions.outOfBoundsCheckIndex(Preconditions.java:70)
        //     at java.base/jdk.internal.util.Preconditions.checkIndex(Preconditions.java:266)
        //     at java.base/java.util.Objects.checkIndex(Objects.java:361)
        //     at java.base/java.util.ArrayList.get(ArrayList.java:427)
        //     at com.strobel.decompiler.ast.AstBuilder.convertToAst(AstBuilder.java:3362)
        //     at com.strobel.decompiler.ast.AstBuilder.build(AstBuilder.java:112)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:203)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:93)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethodBody(AstBuilder.java:868)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createConstructor(AstBuilder.java:799)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addTypeMembers(AstBuilder.java:635)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeCore(AstBuilder.java:605)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeNoCache(AstBuilder.java:195)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createType(AstBuilder.java:162)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addType(AstBuilder.java:137)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.buildAst(JavaLanguage.java:71)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.decompileType(JavaLanguage.java:59)
        //     at com.strobel.decompiler.DecompilerDriver.decompileType(DecompilerDriver.java:333)
        //     at com.strobel.decompiler.DecompilerDriver.decompileJar(DecompilerDriver.java:254)
        //     at com.strobel.decompiler.DecompilerDriver.main(DecompilerDriver.java:144)
        // 
        throw new IllegalStateException("An error occurred while decompiling this method.");
    }
    
    public static String a(final Context context) {
        synchronized (a.class) {
            if (TextUtils.isEmpty((CharSequence)com.apm.insight.i.a.b)) {
                final UUID a = new a(context).a();
                if (a != null) {
                    com.apm.insight.i.a.b = a.toString();
                }
            }
            return com.apm.insight.i.a.b;
        }
    }
    
    public UUID a() {
        return com.apm.insight.i.a.a;
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.apm.insight.k;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.io.OutputStream;
import java.util.zip.GZIPOutputStream;
import org.json.JSONException;
import org.json.JSONObject;
import java.util.zip.Deflater;
import java.io.IOException;
import java.io.Closeable;
import com.apm.insight.l.k;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.util.List;
import java.util.Iterator;
import java.util.Map;
import java.io.File;
import com.apm.insight.l.q;
import android.text.TextUtils;
import java.net.URL;
import com.apm.insight.i;
import com.apm.insight.Npth;

public class e
{
    public static boolean a = false;
    private static h b;
    
    public static l a(final long n, final String s, byte[] array, final a a, String s2, final boolean b) {
        if (Npth.isStopUpload()) {
            return new l(201);
        }
        if (s == null) {
            return new l(201);
        }
        byte[] array2;
        if ((array2 = array) == null) {
            array2 = new byte[0];
        }
        final int length = array2.length;
        String s3;
        if (e.a.b == a && length > 128) {
            array = b(array2);
            s3 = "gzip";
        }
        else if (e.a.c == a && length > 128) {
            array = a(array2);
            s3 = "deflate";
        }
        else {
            s3 = null;
            array = array2;
        }
        if (array == null) {
            return new l(202);
        }
        if (b) {
            final byte[] a2 = i.i().getEncryptImpl().a(array);
            byte[] array3 = array;
            String string = s;
            if (a2 != null) {
                String string2 = null;
                Label_0255: {
                    String str;
                    StringBuilder sb2;
                    if (TextUtils.isEmpty((CharSequence)new URL(s).getQuery())) {
                        s2 = "?";
                        string2 = s;
                        if (s.endsWith("?")) {
                            break Label_0255;
                        }
                        final StringBuilder sb = new StringBuilder();
                        str = s2;
                        sb2 = sb;
                    }
                    else {
                        final String s4 = "&";
                        string2 = s;
                        if (s.endsWith("&")) {
                            break Label_0255;
                        }
                        sb2 = new StringBuilder();
                        str = s4;
                    }
                    sb2.append(s);
                    sb2.append(str);
                    string2 = sb2.toString();
                }
                final StringBuilder sb3 = new StringBuilder();
                sb3.append(string2);
                sb3.append("tt_data=a");
                string = sb3.toString();
                s2 = "application/octet-stream;tt-data=a";
                array3 = a2;
            }
            return a(string, array3, s2, s3, "POST", true, true);
        }
        return a(s, array, s2, s3, "POST", true, false);
    }
    
    public static l a(final String s, final String s2) {
        return a(s, s2, b());
    }
    
    public static l a(final String s, final String s2, final boolean b) {
        try {
            if (!TextUtils.isEmpty((CharSequence)s2) && !TextUtils.isEmpty((CharSequence)s)) {
                return a(2097152L, s, s2.getBytes(), e.a.b, "application/json; charset=utf-8", b);
            }
            return new l(201);
        }
        finally {
            final Throwable t;
            q.b(t);
            return new l(207, t);
        }
    }
    
    public static l a(final String s, final String s2, final File... array) {
        return b(s, s2, array);
    }
    
    private static l a(final String s, final byte[] array, final String s2, final String s3, final String s4, final boolean b, final boolean b2) {
        return b(s, array, s2, s3, s4, b, b2);
    }
    
    private static String a(final String str, final Map map) {
        String string = str;
        if (TextUtils.isDigitsOnly((CharSequence)str)) {
            return string;
        }
        string = str;
        if (map == null) {
            return string;
        }
        if (map.isEmpty()) {
            string = str;
            return string;
        }
        final StringBuilder sb = new StringBuilder(str);
        if (!str.contains("?")) {
            sb.append("?");
        }
        try {
            for (final Map.Entry<Object, V> entry : map.entrySet()) {
                if (entry.getValue() != null) {
                    if (!sb.toString().endsWith("?")) {
                        sb.append("&");
                    }
                    sb.append(d(entry.getKey().toString(), "UTF-8"));
                    sb.append("=");
                    sb.append(d(entry.getValue().toString(), "UTF-8"));
                }
            }
            string = sb.toString();
            return string;
        }
        catch (final Exception ex) {
            string = str;
            return string;
        }
    }
    
    public static void a(final h b) {
        e.b = b;
    }
    
    public static boolean a() {
        return true;
    }
    
    public static boolean a(final String p0, final String p1, final String p2, final String p3, final List<String> p4) {
        // 
        // This method could not be decompiled.
        // 
        // Original Bytecode:
        // 
        //     3: ifeq            8
        //     6: iconst_0       
        //     7: ireturn        
        //     8: new             Lcom/apm/insight/k/j;
        //    11: astore          6
        //    13: aload           6
        //    15: aload_0        
        //    16: ldc             "UTF-8"
        //    18: iconst_0       
        //    19: invokespecial   com/apm/insight/k/j.<init>:(Ljava/lang/String;Ljava/lang/String;Z)V
        //    22: aload           6
        //    24: ldc             "aid"
        //    26: aload_1        
        //    27: invokevirtual   com/apm/insight/k/j.a:(Ljava/lang/String;Ljava/lang/String;)V
        //    30: aload           6
        //    32: ldc             "device_id"
        //    34: aload_2        
        //    35: invokevirtual   com/apm/insight/k/j.a:(Ljava/lang/String;Ljava/lang/String;)V
        //    38: aload           6
        //    40: ldc             "os"
        //    42: ldc             "Android"
        //    44: invokevirtual   com/apm/insight/k/j.a:(Ljava/lang/String;Ljava/lang/String;)V
        //    47: aload           6
        //    49: ldc             "process_name"
        //    51: aload_3        
        //    52: invokevirtual   com/apm/insight/k/j.a:(Ljava/lang/String;Ljava/lang/String;)V
        //    55: aload           4
        //    57: invokeinterface java/util/List.iterator:()Ljava/util/Iterator;
        //    62: astore_0       
        //    63: aload_0        
        //    64: invokeinterface java/util/Iterator.hasNext:()Z
        //    69: ifeq            142
        //    72: aload_0        
        //    73: invokeinterface java/util/Iterator.next:()Ljava/lang/Object;
        //    78: checkcast       Ljava/lang/String;
        //    81: astore_2       
        //    82: new             Ljava/io/File;
        //    85: astore_1       
        //    86: aload_1        
        //    87: aload_2        
        //    88: invokespecial   java/io/File.<init>:(Ljava/lang/String;)V
        //    91: aload_1        
        //    92: invokevirtual   java/io/File.exists:()Z
        //    95: ifeq            63
        //    98: new             Ljava/util/HashMap;
        //   101: astore_2       
        //   102: aload_2        
        //   103: invokespecial   java/util/HashMap.<init>:()V
        //   106: aload_2        
        //   107: ldc             "logtype"
        //   109: ldc             "alog"
        //   111: invokeinterface java/util/Map.put:(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
        //   116: pop            
        //   117: aload_2        
        //   118: ldc             "scene"
        //   120: ldc             "Crash"
        //   122: invokeinterface java/util/Map.put:(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
        //   127: pop            
        //   128: aload           6
        //   130: aload_1        
        //   131: invokevirtual   java/io/File.getName:()Ljava/lang/String;
        //   134: aload_1        
        //   135: aload_2        
        //   136: invokevirtual   com/apm/insight/k/j.a:(Ljava/lang/String;Ljava/io/File;Ljava/util/Map;)V
        //   139: goto            63
        //   142: aload           6
        //   144: invokevirtual   com/apm/insight/k/j.a:()Ljava/lang/String;
        //   147: astore_1       
        //   148: new             Lorg/json/JSONObject;
        //   151: astore_0       
        //   152: aload_0        
        //   153: aload_1        
        //   154: invokespecial   org/json/JSONObject.<init>:(Ljava/lang/String;)V
        //   157: aload_0        
        //   158: ldc             "errno"
        //   160: iconst_m1      
        //   161: invokevirtual   org/json/JSONObject.optInt:(Ljava/lang/String;I)I
        //   164: istore          5
        //   166: iload           5
        //   168: sipush          200
        //   171: if_icmpne       176
        //   174: iconst_1       
        //   175: ireturn        
        //   176: iconst_0       
        //   177: ireturn        
        //   178: astore_0       
        //   179: aload_0        
        //   180: invokevirtual   java/lang/Throwable.printStackTrace:()V
        //   183: iconst_0       
        //   184: ireturn        
        //   185: astore_0       
        //   186: goto            176
        //    Signature:
        //  (Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/List<Ljava/lang/String;>;)Z
        //    Exceptions:
        //  Try           Handler
        //  Start  End    Start  End    Type                    
        //  -----  -----  -----  -----  ------------------------
        //  8      63     178    185    Ljava/io/IOException;
        //  63     139    178    185    Ljava/io/IOException;
        //  142    148    178    185    Ljava/io/IOException;
        //  148    166    185    189    Lorg/json/JSONException;
        //  148    166    178    185    Ljava/io/IOException;
        // 
        // The error that occurred was:
        // 
        // java.lang.IllegalStateException: Expression is linked from several locations: Label_0176:
        //     at com.strobel.decompiler.ast.Error.expressionLinkedFromMultipleLocations(Error.java:27)
        //     at com.strobel.decompiler.ast.AstOptimizer.mergeDisparateObjectInitializations(AstOptimizer.java:2604)
        //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:235)
        //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:42)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:206)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:93)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethodBody(AstBuilder.java:868)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethod(AstBuilder.java:761)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addTypeMembers(AstBuilder.java:638)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeCore(AstBuilder.java:605)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeNoCache(AstBuilder.java:195)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createType(AstBuilder.java:162)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addType(AstBuilder.java:137)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.buildAst(JavaLanguage.java:71)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.decompileType(JavaLanguage.java:59)
        //     at com.strobel.decompiler.DecompilerDriver.decompileType(DecompilerDriver.java:333)
        //     at com.strobel.decompiler.DecompilerDriver.decompileJar(DecompilerDriver.java:254)
        //     at com.strobel.decompiler.DecompilerDriver.main(DecompilerDriver.java:144)
        // 
        throw new IllegalStateException("An error occurred while decompiling this method.");
    }
    
    private static byte[] a(final InputStream inputStream) {
        final ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        final byte[] array = new byte[8192];
        while (true) {
            final int read = inputStream.read(array);
            if (-1 == read) {
                break;
            }
            byteArrayOutputStream.write(array, 0, read);
        }
        inputStream.close();
        try {
            return byteArrayOutputStream.toByteArray();
        }
        finally {
            k.a(byteArrayOutputStream);
        }
    }
    
    public static byte[] a(final String s, final Map<String, String> map, final byte[] array) {
        try {
            return a(2097152L, a(s, map), array, e.a.b, "application/json; charset=utf-8", false).b();
        }
        catch (final IOException ex) {
            ex.printStackTrace();
            return null;
        }
    }
    
    private static byte[] a(byte[] output) {
        final ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream(8192);
        final Deflater deflater = new Deflater();
        deflater.setInput(output);
        deflater.finish();
        output = new byte[8192];
        while (!deflater.finished()) {
            byteArrayOutputStream.write(output, 0, deflater.deflate(output));
        }
        deflater.end();
        return byteArrayOutputStream.toByteArray();
    }
    
    public static l b(final String s, final String s2) {
        return a(s, s2, a());
    }
    
    public static l b(String a, final String s, final File... array) {
        if (Npth.isStopUpload()) {
            return new l(201);
        }
        try {
            final j j = new j(c(a, "have_dump=true&encrypt=true"), "UTF-8", true);
            j.a("json", s, true);
            j.a("file", array);
            a = j.a();
            try {
                return new l(0, new JSONObject(a));
            }
            catch (final JSONException ex) {
                return new l(0, (Throwable)ex);
            }
        }
        catch (final IOException ex2) {
            ex2.printStackTrace();
            return new l(207);
        }
    }
    
    private static l b(final String p0, final byte[] p1, final String p2, final String p3, final String p4, final boolean p5, final boolean p6) {
        // 
        // This method could not be decompiled.
        // 
        // Original Bytecode:
        // 
        //     1: astore          10
        //     3: aconst_null    
        //     4: astore          9
        //     6: getstatic       com/apm/insight/k/e.b:Lcom/apm/insight/k/h;
        //     9: astore          11
        //    11: aload_0        
        //    12: astore          8
        //    14: aload           11
        //    16: ifnull          30
        //    19: aload           11
        //    21: aload_0        
        //    22: aload_1        
        //    23: invokeinterface com/apm/insight/k/h.a:(Ljava/lang/String;[B)Ljava/lang/String;
        //    28: astore          8
        //    30: new             Ljava/net/URL;
        //    33: astore_0       
        //    34: aload_0        
        //    35: aload           8
        //    37: invokespecial   java/net/URL.<init>:(Ljava/lang/String;)V
        //    40: aload_0        
        //    41: invokevirtual   java/net/URL.openConnection:()Ljava/net/URLConnection;
        //    44: checkcast       Ljava/net/HttpURLConnection;
        //    47: astore_0       
        //    48: iload           5
        //    50: ifeq            61
        //    53: aload_0        
        //    54: iconst_1       
        //    55: invokevirtual   java/net/URLConnection.setDoOutput:(Z)V
        //    58: goto            66
        //    61: aload_0        
        //    62: iconst_0       
        //    63: invokevirtual   java/net/URLConnection.setDoOutput:(Z)V
        //    66: aload_2        
        //    67: ifnull          78
        //    70: aload_0        
        //    71: ldc_w           "Content-Type"
        //    74: aload_2        
        //    75: invokevirtual   java/net/URLConnection.setRequestProperty:(Ljava/lang/String;Ljava/lang/String;)V
        //    78: aload_3        
        //    79: ifnull          90
        //    82: aload_0        
        //    83: ldc_w           "Content-Encoding"
        //    86: aload_3        
        //    87: invokevirtual   java/net/URLConnection.setRequestProperty:(Ljava/lang/String;Ljava/lang/String;)V
        //    90: aload_0        
        //    91: ldc_w           "Accept-Encoding"
        //    94: ldc             "gzip"
        //    96: invokevirtual   java/net/URLConnection.setRequestProperty:(Ljava/lang/String;Ljava/lang/String;)V
        //    99: aload           4
        //   101: ifnull          312
        //   104: aload_0        
        //   105: aload           4
        //   107: invokevirtual   java/net/HttpURLConnection.setRequestMethod:(Ljava/lang/String;)V
        //   110: aload_1        
        //   111: ifnull          166
        //   114: aload_1        
        //   115: arraylength    
        //   116: istore          7
        //   118: iload           7
        //   120: ifle            166
        //   123: new             Ljava/io/DataOutputStream;
        //   126: astore_3       
        //   127: aload_3        
        //   128: aload_0        
        //   129: invokevirtual   java/net/URLConnection.getOutputStream:()Ljava/io/OutputStream;
        //   132: invokespecial   java/io/DataOutputStream.<init>:(Ljava/io/OutputStream;)V
        //   135: aload_3        
        //   136: aload_1        
        //   137: invokevirtual   java/io/OutputStream.write:([B)V
        //   140: aload_3        
        //   141: invokevirtual   java/io/DataOutputStream.flush:()V
        //   144: aload_3        
        //   145: invokestatic    com/apm/insight/l/k.a:(Ljava/io/Closeable;)V
        //   148: goto            166
        //   151: astore_2       
        //   152: aload_3        
        //   153: astore_1       
        //   154: goto            160
        //   157: astore_2       
        //   158: aconst_null    
        //   159: astore_1       
        //   160: aload_1        
        //   161: invokestatic    com/apm/insight/l/k.a:(Ljava/io/Closeable;)V
        //   164: aload_2        
        //   165: athrow         
        //   166: aload_0        
        //   167: invokevirtual   java/net/HttpURLConnection.getResponseCode:()I
        //   170: istore          7
        //   172: iload           7
        //   174: sipush          200
        //   177: if_icmpne       264
        //   180: aload_0        
        //   181: invokevirtual   java/net/URLConnection.getInputStream:()Ljava/io/InputStream;
        //   184: astore_3       
        //   185: ldc             "gzip"
        //   187: aload_0        
        //   188: invokevirtual   java/net/URLConnection.getContentEncoding:()Ljava/lang/String;
        //   191: invokevirtual   java/lang/String.equalsIgnoreCase:(Ljava/lang/String;)Z
        //   194: istore          5
        //   196: iload           5
        //   198: ifeq            236
        //   201: new             Ljava/util/zip/GZIPInputStream;
        //   204: astore_2       
        //   205: aload_2        
        //   206: aload_3        
        //   207: invokespecial   java/util/zip/GZIPInputStream.<init>:(Ljava/io/InputStream;)V
        //   210: aload_2        
        //   211: invokestatic    com/apm/insight/k/e.a:(Ljava/io/InputStream;)[B
        //   214: astore_1       
        //   215: aload_2        
        //   216: invokestatic    com/apm/insight/l/k.a:(Ljava/io/Closeable;)V
        //   219: goto            241
        //   222: astore_1       
        //   223: goto            230
        //   226: astore_1       
        //   227: aload           9
        //   229: astore_2       
        //   230: aload_2        
        //   231: invokestatic    com/apm/insight/l/k.a:(Ljava/io/Closeable;)V
        //   234: aload_1        
        //   235: athrow         
        //   236: aload_3        
        //   237: invokestatic    com/apm/insight/k/e.a:(Ljava/io/InputStream;)[B
        //   240: astore_1       
        //   241: aload_1        
        //   242: invokestatic    com/apm/insight/k/e.c:([B)Lcom/apm/insight/k/l;
        //   245: astore_1       
        //   246: aload_0        
        //   247: invokevirtual   java/net/HttpURLConnection.disconnect:()V
        //   250: aload_3        
        //   251: invokestatic    com/apm/insight/l/k.a:(Ljava/io/Closeable;)V
        //   254: aload_1        
        //   255: areturn        
        //   256: astore_2       
        //   257: aload_0        
        //   258: astore_1       
        //   259: aload_3        
        //   260: astore_0       
        //   261: goto            341
        //   264: new             Ljava/lang/StringBuilder;
        //   267: astore_1       
        //   268: aload_1        
        //   269: invokespecial   java/lang/StringBuilder.<init>:()V
        //   272: aload_1        
        //   273: ldc_w           "http response code "
        //   276: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   279: pop            
        //   280: aload_1        
        //   281: iload           7
        //   283: invokevirtual   java/lang/StringBuilder.append:(I)Ljava/lang/StringBuilder;
        //   286: pop            
        //   287: new             Lcom/apm/insight/k/l;
        //   290: dup            
        //   291: sipush          206
        //   294: aload_1        
        //   295: invokevirtual   java/lang/StringBuilder.toString:()Ljava/lang/String;
        //   298: invokespecial   com/apm/insight/k/l.<init>:(ILjava/lang/String;)V
        //   301: astore_1       
        //   302: aload_0        
        //   303: invokevirtual   java/net/HttpURLConnection.disconnect:()V
        //   306: aconst_null    
        //   307: invokestatic    com/apm/insight/l/k.a:(Ljava/io/Closeable;)V
        //   310: aload_1        
        //   311: areturn        
        //   312: new             Ljava/lang/IllegalArgumentException;
        //   315: astore_1       
        //   316: aload_1        
        //   317: ldc_w           "request method is not null"
        //   320: invokespecial   java/lang/IllegalArgumentException.<init>:(Ljava/lang/String;)V
        //   323: aload_1        
        //   324: athrow         
        //   325: astore_2       
        //   326: aconst_null    
        //   327: astore_3       
        //   328: aload_0        
        //   329: astore_1       
        //   330: aload_3        
        //   331: astore_0       
        //   332: goto            341
        //   335: astore_2       
        //   336: aconst_null    
        //   337: astore_0       
        //   338: aload           10
        //   340: astore_1       
        //   341: aload_2        
        //   342: invokestatic    com/apm/insight/l/q.a:(Ljava/lang/Throwable;)V
        //   345: new             Lcom/apm/insight/k/l;
        //   348: dup            
        //   349: sipush          207
        //   352: aload_2        
        //   353: invokespecial   com/apm/insight/k/l.<init>:(ILjava/lang/Throwable;)V
        //   356: astore_2       
        //   357: aload_1        
        //   358: ifnull          365
        //   361: aload_1        
        //   362: invokevirtual   java/net/HttpURLConnection.disconnect:()V
        //   365: aload_0        
        //   366: invokestatic    com/apm/insight/l/k.a:(Ljava/io/Closeable;)V
        //   369: aload_2        
        //   370: areturn        
        //   371: astore_2       
        //   372: aload_1        
        //   373: ifnull          380
        //   376: aload_1        
        //   377: invokevirtual   java/net/HttpURLConnection.disconnect:()V
        //   380: aload_0        
        //   381: invokestatic    com/apm/insight/l/k.a:(Ljava/io/Closeable;)V
        //   384: aload_2        
        //   385: athrow         
        //   386: astore          8
        //   388: aload_0        
        //   389: astore          8
        //   391: goto            30
        //   394: astore_0       
        //   395: goto            250
        //   398: astore_0       
        //   399: goto            306
        //   402: astore_1       
        //   403: goto            365
        //   406: astore_1       
        //   407: goto            380
        //    Exceptions:
        //  Try           Handler
        //  Start  End    Start  End    Type                 
        //  -----  -----  -----  -----  ---------------------
        //  6      11     335    341    Any
        //  19     30     386    394    Any
        //  30     48     335    341    Any
        //  53     58     325    335    Any
        //  61     66     325    335    Any
        //  70     78     325    335    Any
        //  82     90     325    335    Any
        //  90     99     325    335    Any
        //  104    110    325    335    Any
        //  114    118    325    335    Any
        //  123    135    157    160    Any
        //  135    144    151    157    Any
        //  144    148    325    335    Any
        //  160    166    325    335    Any
        //  166    172    325    335    Any
        //  180    185    325    335    Any
        //  185    196    256    264    Any
        //  201    210    226    230    Any
        //  210    215    222    226    Any
        //  215    219    256    264    Any
        //  230    236    256    264    Any
        //  236    241    256    264    Any
        //  241    246    256    264    Any
        //  246    250    394    398    Ljava/lang/Exception;
        //  264    302    325    335    Any
        //  302    306    398    402    Ljava/lang/Exception;
        //  312    325    325    335    Any
        //  341    357    371    386    Any
        //  361    365    402    406    Ljava/lang/Exception;
        //  376    380    406    410    Ljava/lang/Exception;
        // 
        // The error that occurred was:
        // 
        // java.lang.IndexOutOfBoundsException: Index 222 out of bounds for length 222
        //     at java.base/jdk.internal.util.Preconditions.outOfBounds(Preconditions.java:64)
        //     at java.base/jdk.internal.util.Preconditions.outOfBoundsCheckIndex(Preconditions.java:70)
        //     at java.base/jdk.internal.util.Preconditions.checkIndex(Preconditions.java:266)
        //     at java.base/java.util.Objects.checkIndex(Objects.java:361)
        //     at java.base/java.util.ArrayList.get(ArrayList.java:427)
        //     at com.strobel.decompiler.ast.AstBuilder.convertToAst(AstBuilder.java:3362)
        //     at com.strobel.decompiler.ast.AstBuilder.build(AstBuilder.java:112)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:203)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:93)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethodBody(AstBuilder.java:868)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethod(AstBuilder.java:761)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addTypeMembers(AstBuilder.java:638)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeCore(AstBuilder.java:605)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeNoCache(AstBuilder.java:195)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createType(AstBuilder.java:162)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addType(AstBuilder.java:137)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.buildAst(JavaLanguage.java:71)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.decompileType(JavaLanguage.java:59)
        //     at com.strobel.decompiler.DecompilerDriver.decompileType(DecompilerDriver.java:333)
        //     at com.strobel.decompiler.DecompilerDriver.decompileJar(DecompilerDriver.java:254)
        //     at com.strobel.decompiler.DecompilerDriver.main(DecompilerDriver.java:144)
        // 
        throw new IllegalStateException("An error occurred while decompiling this method.");
    }
    
    public static boolean b() {
        return true;
    }
    
    private static byte[] b(final byte[] b) {
        final ByteArrayOutputStream out = new ByteArrayOutputStream(8192);
        final GZIPOutputStream gzipOutputStream = new GZIPOutputStream(out);
        try {
            gzipOutputStream.write(b);
            gzipOutputStream.close();
            return out.toByteArray();
        }
        finally {
            try {
                final Throwable t;
                q.b(t);
                return null;
            }
            finally {
                gzipOutputStream.close();
            }
        }
    }
    
    private static l c(final byte[] array) {
        return new l(204, array);
    }
    
    public static String c() {
        return i.i().getJavaCrashUploadUrl();
    }
    
    private static String c(String str, final String str2) {
        String string = str;
        try {
            string = str;
            final URL url = new URL(str);
            string = str;
            String string2 = null;
            Label_0119: {
                StringBuilder sb;
                if (TextUtils.isEmpty((CharSequence)url.getQuery())) {
                    string2 = str;
                    string = str;
                    if (str.endsWith("?")) {
                        break Label_0119;
                    }
                    string = str;
                    sb = new(java.lang.StringBuilder.class)();
                    string = str;
                    new StringBuilder();
                    string = str;
                    sb.append(str);
                    string = str;
                    sb.append("?");
                }
                else {
                    string2 = str;
                    string = str;
                    if (str.endsWith("&")) {
                        break Label_0119;
                    }
                    string = str;
                    sb = new(java.lang.StringBuilder.class)();
                    string = str;
                    new StringBuilder();
                    string = str;
                    sb.append(str);
                    string = str;
                    sb.append("&");
                }
                string = str;
                string2 = sb.toString();
            }
            string = string2;
            string = string2;
            final StringBuilder sb2 = new StringBuilder();
            string = string2;
            sb2.append(string2);
            string = string2;
            sb2.append(str2);
            string = string2;
            str = (string = sb2.toString());
            return string;
        }
        finally {
            return string;
        }
    }
    
    public static String d() {
        return i.i().getAlogUploadUrl();
    }
    
    private static String d(String encode, String enc) {
        if (enc == null) {
            enc = "UTF-8";
        }
        try {
            encode = URLEncoder.encode(encode, enc);
            return encode;
        }
        catch (final UnsupportedEncodingException cause) {
            throw new IllegalArgumentException(cause);
        }
    }
    
    public static String e() {
        return i.i().getLaunchCrashUploadUrl();
    }
    
    public static String f() {
        return i.i().getExceptionUploadUrl();
    }
    
    public static String g() {
        return i.i().getNativeCrashUploadUrl();
    }
    
    public enum a
    {
        a(0), 
        b(1), 
        c(2);
        
        private static final a[] e;
        final int d;
        
        private a(final int d) {
            this.d = d;
        }
    }
    
    public enum b
    {
        a(0), 
        b(1), 
        c(2), 
        d(3), 
        e(4), 
        f(5), 
        g(6), 
        h(7), 
        i(8), 
        j(9), 
        k(10);
        
        private static final b[] m;
        final int l;
        
        private b(final int l) {
            this.l = l;
        }
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.apm.insight.k;

import com.apm.insight.Npth;
import com.apm.insight.l.p;
import java.util.Arrays;
import java.util.Collections;
import android.text.TextUtils;
import java.util.Iterator;
import com.apm.insight.l.r;
import com.apm.insight.runtime.h;
import com.apm.insight.l.o;
import androidx.annotation.Nullable;
import org.json.JSONArray;
import com.apm.insight.entity.d;
import org.json.JSONObject;
import com.apm.insight.l.w;
import com.apm.insight.runtime.q;
import com.apm.insight.nativecrash.c;
import com.apm.insight.nativecrash.NativeImpl;
import com.apm.insight.l.l;
import com.apm.insight.runtime.k;
import com.apm.insight.entity.a;
import com.apm.insight.entity.Header;
import com.apm.insight.l.i;
import com.apm.insight.entity.e;
import com.apm.insight.CrashType;
import java.util.ArrayList;
import java.util.HashMap;
import android.content.Context;
import java.io.File;
import java.util.List;

public final class b
{
    private static volatile b d;
    List<File> a;
    List<File> b;
    private Context c;
    private int e;
    private b f;
    private HashMap<String, b> g;
    private volatile boolean h;
    private Runnable i;
    private Runnable j;
    
    private b(final Context c) {
        this.a = new ArrayList<File>();
        this.b = new ArrayList<File>();
        this.e = -1;
        this.h = false;
        this.i = new Runnable() {
            final b a;
            
            @Override
            public void run() {
                this.a.g();
            }
        };
        this.j = new Runnable() {
            final b a;
            
            @Override
            public void run() {
                this.a.e();
            }
        };
        this.c = c;
    }
    
    @Nullable
    private e a(final File parent, CrashType f, String s, long abs, long long1) {
        e e = null;
        Label_0713: {
            try {
                Label_0691: {
                    if (parent.isFile()) {
                        break Label_0691;
                    }
                    Object o = CrashType.LAUNCH;
                    final boolean b = f == o;
                    if (f == null) {
                        try {
                            final String name = parent.getName();
                            try {
                                return com.apm.insight.l.i.d(new File(parent, name).getAbsolutePath());
                            }
                            finally {}
                        }
                        finally {}
                        break Label_0713;
                    }
                    o = com.apm.insight.l.i.a(parent, (CrashType)f);
                    try {
                        final JSONObject b2 = ((e)o).b();
                        if (((e)o).b() != null) {
                            if (f == CrashType.ANR) {
                                return (e)o;
                            }
                            b2.put("crash_time", abs);
                            b2.put("app_start_time", long1);
                            final JSONObject optJSONObject = b2.optJSONObject("header");
                            if (optJSONObject == null) {
                                f = Header.a(this.c, abs).f();
                            }
                            else {
                                f = optJSONObject;
                                if (b) {
                                    b2.remove("header");
                                    f = optJSONObject;
                                }
                            }
                            String optString;
                            if ((optString = ((JSONObject)f).optString("sdk_version_name", (String)null)) == null) {
                                optString = "1.3.8.nourl-alpha.15";
                            }
                            com.apm.insight.entity.a.a(b2, "filters", "sdk_version", optString);
                            if (com.apm.insight.l.i.a(b2.optJSONArray("logcat"))) {
                                b2.put("logcat", (Object)k.b(s));
                            }
                            com.apm.insight.entity.a.a(b2, "filters", "has_dump", "true");
                            com.apm.insight.entity.a.a(b2, "filters", "has_logcat", String.valueOf(l.a(b2, "logcat") ^ true));
                            com.apm.insight.entity.a.a(b2, "filters", "memory_leak", String.valueOf(com.apm.insight.entity.a.b(s)));
                            com.apm.insight.entity.a.a(b2, "filters", "fd_leak", String.valueOf(com.apm.insight.entity.a.c(s)));
                            com.apm.insight.entity.a.a(b2, "filters", "threads_leak", String.valueOf(com.apm.insight.entity.a.d(s)));
                            com.apm.insight.entity.a.a(b2, "filters", "is_64_devices", String.valueOf(Header.a()));
                            com.apm.insight.entity.a.a(b2, "filters", "is_64_runtime", String.valueOf(NativeImpl.e()));
                            com.apm.insight.entity.a.a(b2, "filters", "is_x86_devices", String.valueOf(Header.b()));
                            com.apm.insight.entity.a.a(b2, "filters", "has_meminfo_file", String.valueOf(com.apm.insight.entity.a.a(s)));
                            com.apm.insight.entity.a.a(b2, "filters", "is_root", String.valueOf(com.apm.insight.nativecrash.c.m()));
                            b2.put("launch_did", (Object)com.apm.insight.i.a.a(this.c));
                            b2.put("crash_uuid", (Object)parent.getName());
                            b2.put("jiffy", q.a.a());
                            try {
                                long1 = Long.parseLong(com.apm.insight.runtime.b.a(abs, s));
                                abs = Math.abs(long1 - abs);
                                if (abs < 60000L) {
                                    s = "< 60s";
                                }
                                else {
                                    s = "> 60s";
                                }
                                com.apm.insight.entity.a.a(b2, "filters", "lastAliveTime", s);
                                b2.put("lastAliveTime", (Object)String.valueOf(long1));
                            }
                            finally {
                                b2.put("lastAliveTime", (Object)"unknown");
                                com.apm.insight.entity.a.a(b2, "filters", "lastAliveTime", "unknown");
                            }
                            b2.put("has_dump", (Object)"true");
                            if (b2.opt("storage") == null) {
                                com.apm.insight.entity.a.a(b2, w.a(com.apm.insight.i.g()));
                            }
                            if (Header.b((JSONObject)f)) {
                                com.apm.insight.entity.a.a(b2, "filters", "unauthentic_version", "unauthentic_version");
                            }
                            com.apm.insight.entity.d.b(b2);
                            ((e)o).b().put("upload_scene", (Object)"launch_scan");
                            if (b) {
                                final JSONObject jsonObject = new JSONObject();
                                b2.put("event_type", (Object)"start_crash");
                                b2.put("stack", b2.remove("data"));
                                jsonObject.put("data", (Object)new JSONArray().put((Object)b2));
                                jsonObject.put("header", f);
                                ((e)o).a(jsonObject);
                                return (e)o;
                            }
                            b2.put("isJava", 1);
                            return (e)o;
                        }
                        else {
                            try {
                                com.apm.insight.l.i.a(parent);
                            }
                            finally {}
                        }
                    }
                    finally {}
                }
            }
            finally {
                e = null;
            }
        }
        com.apm.insight.l.i.a(parent);
        final Throwable t;
        com.apm.insight.c.a().a("NPTH_CATCH", t);
        Object o = e;
        return (e)o;
    }
    
    public static b a() {
        if (b.d == null) {
            synchronized (b.class) {
                if (b.d == null) {
                    b.d = new b(com.apm.insight.i.g());
                }
            }
        }
        return b.d;
    }
    
    private JSONObject a(final c c) {
        final JSONObject d = c.d();
        if (d != null) {
            final JSONObject j = d;
            if (d.length() != 0) {
                return j;
            }
        }
        if (com.apm.insight.i.d()) {
            c.l();
        }
        if (!c.c()) {
            c.k();
            return null;
        }
        if (!c.f()) {
            c.k();
            return null;
        }
        if (c.g()) {
            c.k();
            return null;
        }
        c.e();
        return c.j();
    }
    
    private void a(final b b) {
        com.apm.insight.l.i.a(o.a(this.c, b.a));
    }
    
    private void a(final b b, final boolean b2, @Nullable final h h) {
        if (b.b.isEmpty()) {
            return;
        }
        if (b.e == null) {
            b.e = b.d;
        }
        for (final a a : b.b) {
            try {
                final File a2 = a.a;
                final CrashType d = a.d;
                final String a3 = b.a;
                final long b3 = a.b;
                final long c = a.c;
                try {
                    final e a4 = this.a(a2, d, a3, b3, c);
                    if (a4 != null) {
                        final JSONObject b4 = a4.b();
                        if (b4 != null) {
                            final JSONObject optJSONObject = b4.optJSONObject("header");
                            if (optJSONObject != null) {
                                if (d == null && (new File(a2, a2.getName()).exists() || a2.getName().split("_").length < 5)) {
                                    if (!com.apm.insight.k.e.b(a4.a(), b4.toString()).a()) {
                                        continue;
                                    }
                                }
                                else {
                                    final File a5 = com.apm.insight.entity.b.a(a2);
                                    if (a5.exists()) {
                                        try {
                                            final JSONArray jsonArray = new JSONArray(com.apm.insight.l.i.c(a5));
                                            JSONObject optJSONObject2;
                                            if (d == CrashType.LAUNCH) {
                                                optJSONObject2 = ((JSONArray)b4.opt("data")).optJSONObject(0);
                                            }
                                            else {
                                                optJSONObject2 = b4;
                                            }
                                            Label_0440: {
                                                if (b2 || b.e == a) {
                                                    if (!a.e.contains("ignore")) {
                                                        if (h != null && !h.a(optJSONObject2.optString("crash_md5", "default"))) {
                                                            com.apm.insight.l.i.a(a.a);
                                                            continue;
                                                        }
                                                        break Label_0440;
                                                    }
                                                }
                                                try {
                                                    com.apm.insight.entity.a.a(optJSONObject2, "filters", "aid", String.valueOf(optJSONObject.opt("aid")));
                                                    try {
                                                        com.apm.insight.entity.a.a(optJSONObject2, "filters", "has_ignore", String.valueOf(a.e.contains("ignore")));
                                                        optJSONObject.put("aid", 2010);
                                                    }
                                                    finally {}
                                                }
                                                finally {}
                                                final Throwable t;
                                                com.apm.insight.c.a().a("NPTH_CATCH", t);
                                            }
                                            com.apm.insight.entity.a.a(optJSONObject2, "filters", "start_uuid", b.a);
                                            com.apm.insight.entity.a.a(optJSONObject2, "filters", "leak_threads_count", String.valueOf(b.g));
                                            com.apm.insight.entity.a.a(optJSONObject2, "filters", "crash_thread_name", optJSONObject2.optString("crash_thread_name", "unknown"));
                                            r.a(b4);
                                            try {
                                                com.apm.insight.entity.b.a(b4, jsonArray, (com.apm.insight.entity.b.a)new com.apm.insight.entity.b.a(this, a4, a2, b) {
                                                    final e a;
                                                    final File b;
                                                    final b c;
                                                    final b d;
                                                    
                                                    @Override
                                                    public void a(final JSONObject jsonObject) {
                                                        com.apm.insight.k.e.a(this.a.a(), jsonObject.toString(), new File(this.b, "logZip"), o.a(this.d.c, this.c.a));
                                                    }
                                                });
                                                if (!com.apm.insight.l.i.a(a2)) {
                                                    com.apm.insight.e.a.a().a(com.apm.insight.e.a.a.a(a2.getAbsolutePath()));
                                                }
                                                com.apm.insight.k.c.a(d, b4);
                                            }
                                            finally {}
                                        }
                                        finally {}
                                    }
                                }
                            }
                        }
                    }
                    com.apm.insight.l.i.a(a2);
                }
                finally {}
            }
            finally {}
            final Throwable t2;
            com.apm.insight.c.a().a("NPTH_CATCH", t2);
            com.apm.insight.l.i.a(a.a);
        }
    }
    
    private void a(final File file, final b b) {
    }
    
    private void a(final HashMap<String, b> p0) {
        // 
        // This method could not be decompiled.
        // 
        // Original Bytecode:
        // 
        //     1: getfield        com/apm/insight/k/b.c:Landroid/content/Context;
        //     4: invokestatic    com/apm/insight/l/o.f:(Landroid/content/Context;)Ljava/io/File;
        //     7: invokevirtual   java/io/File.listFiles:()[Ljava/io/File;
        //    10: astore          6
        //    12: aload           6
        //    14: ifnull          198
        //    17: aload           6
        //    19: arraylength    
        //    20: ifne            26
        //    23: goto            198
        //    26: iconst_0       
        //    27: istore_2       
        //    28: iload_2        
        //    29: aload           6
        //    31: arraylength    
        //    32: if_icmpge       198
        //    35: iload_2        
        //    36: iconst_5       
        //    37: if_icmpge       198
        //    40: aload           6
        //    42: iload_2        
        //    43: aaload         
        //    44: astore          7
        //    46: aload           7
        //    48: invokevirtual   java/io/File.isDirectory:()Z
        //    51: ifne            57
        //    54: goto            164
        //    57: aload           7
        //    59: invokevirtual   java/io/File.getName:()Ljava/lang/String;
        //    62: ldc_w           "G"
        //    65: invokevirtual   java/lang/String.endsWith:(Ljava/lang/String;)Z
        //    68: ifeq            164
        //    71: aload           7
        //    73: invokevirtual   java/io/File.getName:()Ljava/lang/String;
        //    76: astore          8
        //    78: aload_1        
        //    79: aload           8
        //    81: invokevirtual   java/util/HashMap.get:(Ljava/lang/Object;)Ljava/lang/Object;
        //    84: checkcast       Lcom/apm/insight/k/b$b;
        //    87: astore          5
        //    89: aload           5
        //    91: astore          4
        //    93: aload           5
        //    95: ifnonnull       119
        //    98: new             Lcom/apm/insight/k/b$b;
        //   101: astore          4
        //   103: aload           4
        //   105: aload           8
        //   107: invokespecial   com/apm/insight/k/b$b.<init>:(Ljava/lang/String;)V
        //   110: aload_1        
        //   111: aload           8
        //   113: aload           4
        //   115: invokevirtual   java/util/HashMap.put:(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
        //   118: pop            
        //   119: aload           7
        //   121: invokestatic    com/apm/insight/l/o.l:(Ljava/io/File;)Ljava/io/File;
        //   124: aload           7
        //   126: invokestatic    com/apm/insight/l/o.m:(Ljava/io/File;)Ljava/io/File;
        //   129: invokestatic    com/apm/insight/nativecrash/d.a:(Ljava/io/File;Ljava/io/File;)Lorg/json/JSONArray;
        //   132: astore          5
        //   134: aload           5
        //   136: invokevirtual   org/json/JSONArray.length:()I
        //   139: istore_3       
        //   140: aload           4
        //   142: iload_3        
        //   143: putfield        com/apm/insight/k/b$b.g:I
        //   146: iload_3        
        //   147: ifle            192
        //   150: aload           7
        //   152: invokestatic    com/apm/insight/l/o.n:(Ljava/io/File;)Ljava/io/File;
        //   155: aload           5
        //   157: iconst_0       
        //   158: invokestatic    com/apm/insight/l/i.a:(Ljava/io/File;Lorg/json/JSONArray;Z)V
        //   161: goto            192
        //   164: aload           7
        //   166: invokestatic    com/apm/insight/l/i.a:(Ljava/io/File;)Z
        //   169: pop            
        //   170: goto            192
        //   173: astore          4
        //   175: invokestatic    com/apm/insight/c.a:()Lcom/apm/insight/d;
        //   178: ldc_w           "NPTH_CATCH"
        //   181: aload           4
        //   183: invokevirtual   com/apm/insight/d.a:(Ljava/lang/String;Ljava/lang/Throwable;)V
        //   186: aload           7
        //   188: invokestatic    com/apm/insight/l/i.a:(Ljava/io/File;)Z
        //   191: pop            
        //   192: iinc            2, 1
        //   195: goto            28
        //   198: return         
        //   199: astore          4
        //   201: goto            192
        //    Signature:
        //  (Ljava/util/HashMap<Ljava/lang/String;Lcom/apm/insight/k/b$b;>;)V
        //    Exceptions:
        //  Try           Handler
        //  Start  End    Start  End    Type
        //  -----  -----  -----  -----  ----
        //  46     54     173    192    Any
        //  57     89     173    192    Any
        //  98     119    173    192    Any
        //  119    146    173    192    Any
        //  150    161    199    204    Any
        //  164    170    173    192    Any
        // 
        // The error that occurred was:
        // 
        // java.lang.IllegalStateException: Expression is linked from several locations: Label_0164:
        //     at com.strobel.decompiler.ast.Error.expressionLinkedFromMultipleLocations(Error.java:27)
        //     at com.strobel.decompiler.ast.AstOptimizer.mergeDisparateObjectInitializations(AstOptimizer.java:2604)
        //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:235)
        //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:42)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:206)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:93)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethodBody(AstBuilder.java:868)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethod(AstBuilder.java:761)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addTypeMembers(AstBuilder.java:638)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeCore(AstBuilder.java:605)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeNoCache(AstBuilder.java:195)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createType(AstBuilder.java:162)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addType(AstBuilder.java:137)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.buildAst(JavaLanguage.java:71)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.decompileType(JavaLanguage.java:59)
        //     at com.strobel.decompiler.DecompilerDriver.decompileType(DecompilerDriver.java:333)
        //     at com.strobel.decompiler.DecompilerDriver.decompileJar(DecompilerDriver.java:254)
        //     at com.strobel.decompiler.DecompilerDriver.main(DecompilerDriver.java:144)
        // 
        throw new IllegalStateException("An error occurred while decompiling this method.");
    }
    
    private void a(final HashMap<String, b> hashMap, b value) {
        final File[] listFiles = o.d(this.c).listFiles();
        if (listFiles != null) {
            if (listFiles.length != 0) {
                for (int n = 0; n < listFiles.length && n < 5; ++n) {
                    final File file = listFiles[n];
                    try {
                        if (file.isDirectory()) {
                            if (file.getName().endsWith("G")) {
                                final String name = file.getName();
                                if ((value = hashMap.get(name)) == null) {
                                    value = new b(name);
                                    hashMap.put(name, (b)value);
                                }
                                ((b)value).c.add(new a(file, CrashType.NATIVE));
                                continue;
                            }
                        }
                        com.apm.insight.l.i.a(file);
                    }
                    finally {
                        final Throwable t;
                        com.apm.insight.c.a().a("NPTH_CATCH", t);
                        com.apm.insight.l.i.a(file);
                    }
                }
            }
        }
    }
    
    private void a(final HashMap<String, b> hashMap, b b, File d, final String str) {
        if (str.endsWith("G")) {
            final String[] split = str.split("_");
            final int length = split.length;
            final b b2 = null;
            if (length < 5) {
                b.b.add(new a(d, null));
                return;
            }
            int n = 0;
            try {
                final long long1 = Long.parseLong(split[0]);
                final long long2 = Long.parseLong(split[4]);
                final String s = split[2];
                b = (b)split[1];
                ((String)b).hashCode();
                Label_0189: {
                    switch (b) {
                        case "java": {
                            n = 2;
                            break Label_0189;
                        }
                        case "anr": {
                            n = 1;
                            break Label_0189;
                        }
                        case "launch": {
                            break Label_0189;
                        }
                        default:
                            break;
                    }
                    n = -1;
                }
                switch (n) {
                    default: {
                        b = b2;
                        break;
                    }
                    case 2: {
                        b = (b)CrashType.JAVA;
                        break;
                    }
                    case 1: {
                        b = (b)CrashType.ANR;
                        break;
                    }
                    case 0: {
                        b = (b)CrashType.LAUNCH;
                        break;
                    }
                }
                b value;
                if ((value = hashMap.get(s)) == null) {
                    value = new b(s);
                    hashMap.put(s, value);
                }
                d = (File)new a(d, long1, (CrashType)b);
                ((a)d).c = long2;
                final a d2 = value.d;
                if ((d2 == null || d2.b > ((a)d).b) && b != null && b != CrashType.ANR && !str.contains("ignore")) {
                    value.d = (a)d;
                }
                value.b.add((a)d);
                return;
            }
            finally {
                b.b.add(new a(d, null));
                final com.apm.insight.d a = com.apm.insight.c.a();
                final StringBuilder sb = new StringBuilder();
                sb.append("err format crashTime:");
                sb.append(str);
                a.a("NPTH_CATCH", new RuntimeException(sb.toString()));
                return;
            }
        }
        com.apm.insight.l.i.a(d);
    }
    
    private boolean a(final File file) {
        final String[] list = file.list();
        if (list == null) {
            return false;
        }
        for (final String s : list) {
            if (!TextUtils.isEmpty((CharSequence)s) && s.endsWith("")) {
                return true;
            }
        }
        return false;
    }
    
    private void b(final b p0, final boolean p1, @Nullable final h p2) {
        // 
        // This method could not be decompiled.
        // 
        // Original Bytecode:
        // 
        //     1: getfield        com/apm/insight/k/b$b.c:Ljava/util/List;
        //     4: invokeinterface java/util/List.size:()I
        //     9: iconst_1       
        //    10: if_icmple       16
        //    13: goto            37
        //    16: aload_1        
        //    17: getfield        com/apm/insight/k/b$b.c:Ljava/util/List;
        //    20: invokeinterface java/util/List.isEmpty:()Z
        //    25: ifeq            37
        //    28: aload_1        
        //    29: aload_1        
        //    30: getfield        com/apm/insight/k/b$b.d:Lcom/apm/insight/k/b$a;
        //    33: putfield        com/apm/insight/k/b$b.e:Lcom/apm/insight/k/b$a;
        //    36: return         
        //    37: aload_0        
        //    38: getfield        com/apm/insight/k/b.c:Landroid/content/Context;
        //    41: invokestatic    com/apm/insight/l/p.b:(Landroid/content/Context;)Z
        //    44: istore          5
        //    46: aload_1        
        //    47: aload_1        
        //    48: getfield        com/apm/insight/k/b$b.d:Lcom/apm/insight/k/b$a;
        //    51: putfield        com/apm/insight/k/b$b.e:Lcom/apm/insight/k/b$a;
        //    54: new             Lcom/apm/insight/nativecrash/c;
        //    57: dup            
        //    58: aload_0        
        //    59: getfield        com/apm/insight/k/b.c:Landroid/content/Context;
        //    62: invokespecial   com/apm/insight/nativecrash/c.<init>:(Landroid/content/Context;)V
        //    65: astore          10
        //    67: aload_1        
        //    68: getfield        com/apm/insight/k/b$b.c:Ljava/util/List;
        //    71: invokeinterface java/util/List.iterator:()Ljava/util/Iterator;
        //    76: astore          11
        //    78: aload           11
        //    80: invokeinterface java/util/Iterator.hasNext:()Z
        //    85: ifeq            556
        //    88: aload           11
        //    90: invokeinterface java/util/Iterator.next:()Ljava/lang/Object;
        //    95: checkcast       Lcom/apm/insight/k/b$a;
        //    98: astore          14
        //   100: aload           14
        //   102: getfield        com/apm/insight/k/b$a.a:Ljava/io/File;
        //   105: astore          12
        //   107: aload           10
        //   109: aload           12
        //   111: invokevirtual   com/apm/insight/nativecrash/c.a:(Ljava/io/File;)V
        //   114: aload_0        
        //   115: aload           10
        //   117: invokespecial   com/apm/insight/k/b.a:(Lcom/apm/insight/nativecrash/c;)Lorg/json/JSONObject;
        //   120: astore          9
        //   122: aload           9
        //   124: ifnull          520
        //   127: aload           9
        //   129: invokevirtual   org/json/JSONObject.length:()I
        //   132: ifne            138
        //   135: goto            520
        //   138: aload           9
        //   140: invokevirtual   org/json/JSONObject.length:()I
        //   143: istore          4
        //   145: iload           4
        //   147: ifeq            78
        //   150: iload_2        
        //   151: ifne            339
        //   154: aload           9
        //   156: ldc             "crash_time"
        //   158: invokevirtual   org/json/JSONObject.optLong:(Ljava/lang/String;)J
        //   161: lstore          7
        //   163: aload_1        
        //   164: getfield        com/apm/insight/k/b$b.e:Lcom/apm/insight/k/b$a;
        //   167: astore          13
        //   169: aload           13
        //   171: ifnonnull       209
        //   174: aload_1        
        //   175: aload           14
        //   177: putfield        com/apm/insight/k/b$b.e:Lcom/apm/insight/k/b$a;
        //   180: aload_1        
        //   181: iconst_1       
        //   182: putfield        com/apm/insight/k/b$b.f:Z
        //   185: aload_3        
        //   186: ifnull          206
        //   189: aload_3        
        //   190: ldc_w           "default"
        //   193: invokevirtual   com/apm/insight/runtime/h.a:(Ljava/lang/String;)Z
        //   196: istore          6
        //   198: iload           6
        //   200: ifne            206
        //   203: goto            520
        //   206: goto            360
        //   209: aload_1        
        //   210: getfield        com/apm/insight/k/b$b.f:Z
        //   213: istore          6
        //   215: iload           6
        //   217: ifne            283
        //   220: lload           7
        //   222: aload           13
        //   224: getfield        com/apm/insight/k/b$a.b:J
        //   227: lcmp           
        //   228: ifge            283
        //   231: aload_1        
        //   232: aload           14
        //   234: putfield        com/apm/insight/k/b$b.e:Lcom/apm/insight/k/b$a;
        //   237: aload_3        
        //   238: ifnull          254
        //   241: aload_3        
        //   242: ldc_w           "default"
        //   245: invokevirtual   com/apm/insight/runtime/h.a:(Ljava/lang/String;)Z
        //   248: ifne            254
        //   251: goto            203
        //   254: aload_0        
        //   255: aload           12
        //   257: invokespecial   com/apm/insight/k/b.a:(Ljava/io/File;)Z
        //   260: ifne            270
        //   263: aload_0        
        //   264: aload           12
        //   266: aload_1        
        //   267: invokespecial   com/apm/insight/k/b.a:(Ljava/io/File;Lcom/apm/insight/k/b$b;)V
        //   270: aload_1        
        //   271: iconst_1       
        //   272: putfield        com/apm/insight/k/b$b.f:Z
        //   275: goto            360
        //   278: astore          9
        //   280: goto            336
        //   283: aload           9
        //   285: ldc             "filters"
        //   287: ldc_w           "aid"
        //   290: aload           9
        //   292: ldc             "header"
        //   294: invokevirtual   org/json/JSONObject.optJSONObject:(Ljava/lang/String;)Lorg/json/JSONObject;
        //   297: ldc_w           "aid"
        //   300: invokevirtual   org/json/JSONObject.opt:(Ljava/lang/String;)Ljava/lang/Object;
        //   303: invokestatic    java/lang/String.valueOf:(Ljava/lang/Object;)Ljava/lang/String;
        //   306: invokestatic    com/apm/insight/entity/a.a:(Lorg/json/JSONObject;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
        //   309: aload           9
        //   311: ldc             "header"
        //   313: invokevirtual   org/json/JSONObject.optJSONObject:(Ljava/lang/String;)Lorg/json/JSONObject;
        //   316: ldc_w           "aid"
        //   319: sipush          2010
        //   322: invokevirtual   org/json/JSONObject.put:(Ljava/lang/String;I)Lorg/json/JSONObject;
        //   325: pop            
        //   326: goto            360
        //   329: astore          9
        //   331: goto            336
        //   334: astore          9
        //   336: goto            536
        //   339: aload_3        
        //   340: ifnull          360
        //   343: aload_3        
        //   344: ldc_w           "default"
        //   347: invokevirtual   com/apm/insight/runtime/h.a:(Ljava/lang/String;)Z
        //   350: istore          6
        //   352: iload           6
        //   354: ifne            360
        //   357: goto            520
        //   360: aload           9
        //   362: ldc             "filters"
        //   364: ldc_w           "start_uuid"
        //   367: aload_1        
        //   368: getfield        com/apm/insight/k/b$b.a:Ljava/lang/String;
        //   371: invokestatic    com/apm/insight/entity/a.a:(Lorg/json/JSONObject;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
        //   374: aload           9
        //   376: ldc             "filters"
        //   378: ldc_w           "crash_thread_name"
        //   381: aload           9
        //   383: ldc_w           "crash_thread_name"
        //   386: ldc_w           "unknown"
        //   389: invokevirtual   org/json/JSONObject.optString:(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
        //   392: invokestatic    com/apm/insight/entity/a.a:(Lorg/json/JSONObject;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
        //   395: iload           5
        //   397: ifeq            509
        //   400: new             Lcom/apm/insight/k/c$a;
        //   403: astore          13
        //   405: aload           13
        //   407: aload           9
        //   409: getstatic       com/apm/insight/CrashType.NATIVE:Lcom/apm/insight/CrashType;
        //   412: invokespecial   com/apm/insight/k/c$a.<init>:(Lorg/json/JSONObject;Lcom/apm/insight/CrashType;)V
        //   415: invokestatic    com/apm/insight/runtime/s.a:()Lcom/apm/insight/runtime/s;
        //   418: astore          14
        //   420: aload           13
        //   422: invokevirtual   com/apm/insight/k/c$a.b:()J
        //   425: ldc2_w          -1
        //   428: lcmp           
        //   429: ifne            440
        //   432: invokestatic    java/lang/System.currentTimeMillis:()J
        //   435: lstore          7
        //   437: goto            447
        //   440: aload           13
        //   442: invokevirtual   com/apm/insight/k/c$a.b:()J
        //   445: lstore          7
        //   447: aload           14
        //   449: lload           7
        //   451: invokevirtual   com/apm/insight/runtime/s.b:(J)Lorg/json/JSONArray;
        //   454: astore          14
        //   456: aload           13
        //   458: invokevirtual   com/apm/insight/k/c$a.c:()Ljava/lang/String;
        //   461: aload           13
        //   463: invokevirtual   com/apm/insight/k/c$a.a:()Ljava/lang/String;
        //   466: aload           14
        //   468: invokestatic    com/apm/insight/entity/b.a:(Ljava/lang/String;Ljava/lang/String;Lorg/json/JSONArray;)Lorg/json/JSONArray;
        //   471: astore          14
        //   473: new             Lcom/apm/insight/k/b$4;
        //   476: astore          13
        //   478: aload           13
        //   480: aload_0        
        //   481: aload           12
        //   483: aload_1        
        //   484: invokespecial   com/apm/insight/k/b$4.<init>:(Lcom/apm/insight/k/b;Ljava/io/File;Lcom/apm/insight/k/b$b;)V
        //   487: aload           9
        //   489: aload           14
        //   491: aload           13
        //   493: invokestatic    com/apm/insight/entity/b.a:(Lorg/json/JSONObject;Lorg/json/JSONArray;Lcom/apm/insight/entity/b$a;)V
        //   496: aload           10
        //   498: invokevirtual   com/apm/insight/nativecrash/c.k:()Z
        //   501: ifne            509
        //   504: aload           10
        //   506: invokevirtual   com/apm/insight/nativecrash/c.h:()V
        //   509: getstatic       com/apm/insight/CrashType.NATIVE:Lcom/apm/insight/CrashType;
        //   512: aload           9
        //   514: invokestatic    com/apm/insight/k/c.a:(Lcom/apm/insight/CrashType;Lorg/json/JSONObject;)V
        //   517: goto            553
        //   520: aload           10
        //   522: invokevirtual   com/apm/insight/nativecrash/c.k:()Z
        //   525: pop            
        //   526: goto            553
        //   529: astore          9
        //   531: goto            536
        //   534: astore          9
        //   536: invokestatic    com/apm/insight/c.a:()Lcom/apm/insight/d;
        //   539: ldc_w           "NPTH_CATCH"
        //   542: aload           9
        //   544: invokevirtual   com/apm/insight/d.a:(Ljava/lang/String;Ljava/lang/Throwable;)V
        //   547: aload           12
        //   549: invokestatic    com/apm/insight/l/i.a:(Ljava/io/File;)Z
        //   552: pop            
        //   553: goto            78
        //   556: return         
        //    Exceptions:
        //  Try           Handler
        //  Start  End    Start  End    Type
        //  -----  -----  -----  -----  ----
        //  107    122    534    536    Any
        //  127    135    534    536    Any
        //  138    145    534    536    Any
        //  154    163    534    536    Any
        //  163    169    334    336    Any
        //  174    180    334    336    Any
        //  180    185    534    536    Any
        //  189    198    334    336    Any
        //  209    215    329    334    Any
        //  220    237    278    283    Any
        //  241    251    278    283    Any
        //  254    270    278    283    Any
        //  270    275    529    534    Any
        //  283    326    529    534    Any
        //  343    352    529    534    Any
        //  360    395    529    534    Any
        //  400    437    529    534    Any
        //  440    447    529    534    Any
        //  447    509    529    534    Any
        //  509    517    529    534    Any
        //  520    526    529    534    Any
        // 
        // The error that occurred was:
        // 
        // java.lang.IllegalStateException: Expression is linked from several locations: Label_0270:
        //     at com.strobel.decompiler.ast.Error.expressionLinkedFromMultipleLocations(Error.java:27)
        //     at com.strobel.decompiler.ast.AstOptimizer.mergeDisparateObjectInitializations(AstOptimizer.java:2604)
        //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:235)
        //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:42)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:206)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:93)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethodBody(AstBuilder.java:868)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethod(AstBuilder.java:761)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addTypeMembers(AstBuilder.java:638)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeCore(AstBuilder.java:605)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeNoCache(AstBuilder.java:195)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createType(AstBuilder.java:162)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addType(AstBuilder.java:137)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.buildAst(JavaLanguage.java:71)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.decompileType(JavaLanguage.java:59)
        //     at com.strobel.decompiler.DecompilerDriver.decompileType(DecompilerDriver.java:333)
        //     at com.strobel.decompiler.DecompilerDriver.decompileJar(DecompilerDriver.java:254)
        //     at com.strobel.decompiler.DecompilerDriver.main(DecompilerDriver.java:144)
        // 
        throw new IllegalStateException("An error occurred while decompiling this method.");
    }
    
    private void b(final HashMap<String, b> hashMap, final b b) {
        final File[] listFiles = o.a(this.c).listFiles();
        if (listFiles == null) {
            return;
        }
        Arrays.sort(listFiles, Collections.reverseOrder());
        for (int i = 0; i < listFiles.length; ++i) {
            final File file = listFiles[i];
            try {
                if (!com.apm.insight.e.a.a().a(file.getAbsolutePath())) {
                    if (com.apm.insight.l.i.g(file)) {
                        continue;
                    }
                    if (com.apm.insight.g.a.a().b(file.getName())) {
                        continue;
                    }
                    if (!file.isFile()) {
                        this.a(hashMap, b, file, file.getName());
                        continue;
                    }
                }
                com.apm.insight.l.i.a(file);
            }
            finally {
                final Throwable t;
                com.apm.insight.c.a().a("NPTH_CATCH", t);
            }
        }
    }
    
    private void c(final HashMap<String, b> hashMap, final b b) {
        com.apm.insight.l.i.a(o.b(this.c));
    }
    
    private void d() {
        if (this.f != null) {
            return;
        }
        this.f = new b("old_uuid");
        this.a(this.g = new HashMap<String, b>());
        this.b(this.g, this.f);
        this.c(this.g, this.f);
        this.a(this.g, this.f);
        this.b(this.f, true, null);
        this.a(this.f, true, null);
        this.f = null;
        if (this.g.isEmpty()) {
            this.f();
            return;
        }
        this.g();
    }
    
    private void e() {
        if (!this.h) {
            if (this.g != null) {
                if (!p.b(this.c)) {
                    this.f();
                }
                final boolean h = this.h();
                final h h2 = new h(this.c);
                final Iterator<b> iterator = this.g.values().iterator();
                while (iterator.hasNext()) {
                    this.b(iterator.next(), h, h2);
                }
                final Iterator<b> iterator2 = this.g.values().iterator();
                while (iterator2.hasNext()) {
                    this.a(iterator2.next(), h, h2);
                }
                final Iterator<b> iterator3 = this.g.values().iterator();
                while (iterator3.hasNext()) {
                    this.a(iterator3.next());
                }
                h2.a();
                com.apm.insight.runtime.b.a();
                this.f();
            }
        }
    }
    
    private void f() {
        this.h = true;
        this.g = null;
        NativeImpl.i();
    }
    
    private void g() {
        if (this.h) {
            return;
        }
        if (p.b(this.c) && (System.currentTimeMillis() - com.apm.insight.i.j() > 5000L || !com.apm.insight.i.i().isApmExists() || Npth.hasCrash())) {
            this.e();
        }
        else {
            com.apm.insight.runtime.p.b().a(this.i, 5000L);
        }
    }
    
    private boolean h() {
        final int e = this.e;
        boolean b = false;
        Label_0040: {
            if (e == -1) {
                if (com.apm.insight.runtime.a.b()) {
                    if (com.apm.insight.runtime.a.g()) {
                        this.e = 1;
                        break Label_0040;
                    }
                }
                this.e = 0;
            }
        }
        if (this.e == 1) {
            b = true;
        }
        return b;
    }
    
    private void i() {
        final File[] listFiles = o.i(this.c).listFiles();
        if (listFiles == null) {
            return;
        }
        for (int n = 0; n < listFiles.length && n < 5; ++n) {
            final File file = listFiles[n];
            if (file.getName().endsWith(".atmp")) {
                com.apm.insight.a.a.a().a(file.getAbsolutePath());
            }
            else {
                try {
                    final e e = com.apm.insight.l.i.e(file.getAbsolutePath());
                    if (e != null) {
                        if (e.b() != null) {
                            e.b().put("upload_scene", (Object)"launch_scan");
                        }
                        if (com.apm.insight.k.e.a(com.apm.insight.k.e.d(), e.e(), e.d(), e.f(), e.g())) {
                            com.apm.insight.l.i.a(file);
                            com.apm.insight.l.i.a(e.c());
                        }
                    }
                    else {
                        com.apm.insight.l.i.a(file);
                    }
                }
                finally {
                    final Throwable t;
                    com.apm.insight.c.a().a("NPTH_CATCH", t);
                }
            }
        }
    }
    
    public void a(final boolean b) {
        if (Npth.isStopUpload()) {
            return;
        }
        if (b) {
            this.d();
            this.i();
            com.apm.insight.c.a.a();
        }
    }
    
    public void b() {
        try {
            if (!this.h) {
                if (com.apm.insight.l.a.b(com.apm.insight.i.g())) {
                    com.apm.insight.runtime.p.b().a(this.j);
                }
            }
        }
        finally {}
    }
    
    public boolean c() {
        return this.h;
    }
    
    static class a
    {
        File a;
        long b;
        long c;
        @Nullable
        CrashType d;
        String e;
        
        a(final File a, final long b, @Nullable final CrashType d) {
            this.c = -1L;
            this.a = a;
            this.b = b;
            this.d = d;
            this.e = a.getName();
        }
        
        a(final File a, @Nullable final CrashType d) {
            this.b = -1L;
            this.c = -1L;
            this.a = a;
            this.d = d;
            this.e = a.getName();
        }
    }
    
    static class b
    {
        String a;
        List<a> b;
        List<a> c;
        a d;
        a e;
        boolean f;
        int g;
        
        b(final String a) {
            this.b = new ArrayList<a>();
            this.c = new ArrayList<a>();
            this.f = false;
            this.g = 0;
            this.a = a;
        }
    }
}

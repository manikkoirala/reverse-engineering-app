// 
// Decompiled by Procyon v0.6.0
// 

package com.apm.insight.k;

import androidx.annotation.Nullable;
import org.json.JSONArray;
import org.json.JSONObject;
import com.apm.insight.CrashType;
import java.util.concurrent.ConcurrentLinkedQueue;

public abstract class c
{
    private static ConcurrentLinkedQueue<c> a;
    
    static {
        c.a = new ConcurrentLinkedQueue<c>();
    }
    
    static void a(final CrashType crashType, final JSONObject jsonObject) {
        final ConcurrentLinkedQueue<c> a = c.a;
        if (a != null) {
            if (!a.isEmpty()) {
                final a a2 = new a(jsonObject, crashType);
                while (!c.a.isEmpty()) {
                    final c c = com.apm.insight.k.c.a.poll();
                    if (c != null) {
                        c.a(crashType, a2);
                    }
                }
                c.a = null;
            }
        }
    }
    
    public abstract void a(final CrashType p0, final a p1);
    
    public static class a
    {
        private JSONObject a;
        private JSONObject b;
        private CrashType c;
        
        a(final JSONObject a, final CrashType c) {
            this.c = c;
            if (c == CrashType.LAUNCH) {
                this.a = ((JSONArray)a.opt("data")).optJSONObject(0);
            }
            else {
                this.a = a;
            }
            this.b = a.optJSONObject("header");
        }
        
        @Nullable
        public String a() {
            return this.a.optString("crash_thread_name", (String)null);
        }
        
        public long b() {
            return this.a.optInt("app_start_time", -1);
        }
        
        @Nullable
        public String c() {
            final int n = c$1.a[this.c.ordinal()];
            if (n == 1) {
                return this.a.optString("data", (String)null);
            }
            if (n == 2) {
                return this.a.optString("stack", (String)null);
            }
            if (n != 3) {
                return null;
            }
            return this.a.optString("data", (String)null);
        }
    }
}

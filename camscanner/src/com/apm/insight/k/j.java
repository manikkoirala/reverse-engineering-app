// 
// Decompiled by Procyon v0.6.0
// 

package com.apm.insight.k;

import java.io.FilterOutputStream;
import java.io.OutputStream;
import com.apm.insight.i;
import java.io.FileInputStream;
import java.util.Map;
import java.io.File;
import java.util.Iterator;
import java.io.IOException;
import java.io.Reader;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.net.URL;
import java.net.HttpURLConnection;

public class j
{
    private final String a;
    private HttpURLConnection b;
    private String c;
    private boolean d;
    private f e;
    private m f;
    
    public j(final String spec, String string, final boolean d) {
        this.c = string;
        this.d = d;
        final StringBuilder sb = new StringBuilder();
        sb.append("AAA");
        sb.append(System.currentTimeMillis());
        sb.append("AAA");
        string = sb.toString();
        this.a = string;
        (this.b = (HttpURLConnection)new URL(spec).openConnection()).setUseCaches(false);
        this.b.setDoOutput(true);
        this.b.setDoInput(true);
        this.b.setRequestMethod("POST");
        final HttpURLConnection b = this.b;
        final StringBuilder sb2 = new StringBuilder();
        sb2.append("multipart/form-data; boundary=");
        sb2.append(string);
        b.setRequestProperty("Content-Type", sb2.toString());
        if (d) {
            this.b.setRequestProperty("Content-Encoding", "gzip");
            this.f = new m(this.b.getOutputStream());
        }
        else {
            this.e = new f(this.b.getOutputStream());
        }
    }
    
    public String a() {
        final ArrayList list = new ArrayList();
        final StringBuilder sb = new StringBuilder();
        sb.append("\r\n--");
        sb.append(this.a);
        sb.append("--");
        sb.append("\r\n");
        final byte[] bytes = sb.toString().getBytes();
        if (this.d) {
            this.f.write(bytes);
            this.f.b();
            this.f.a();
        }
        else {
            this.e.write(bytes);
            this.e.flush();
            this.e.a();
        }
        final int responseCode = this.b.getResponseCode();
        if (responseCode == 200) {
            final BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(this.b.getInputStream()));
            while (true) {
                final String line = bufferedReader.readLine();
                if (line == null) {
                    break;
                }
                list.add(line);
            }
            bufferedReader.close();
            this.b.disconnect();
            final StringBuilder sb2 = new StringBuilder();
            final Iterator iterator = list.iterator();
            while (iterator.hasNext()) {
                sb2.append((String)iterator.next());
            }
            return sb2.toString();
        }
        final StringBuilder sb3 = new StringBuilder();
        sb3.append("Server returned non-OK status: ");
        sb3.append(responseCode);
        throw new IOException(sb3.toString());
    }
    
    public void a(final String str, final File file, final Map<String, String> map) {
        final String name = file.getName();
        final StringBuilder sb = new StringBuilder();
        sb.append("--");
        sb.append(this.a);
        sb.append("\r\n");
        sb.append("Content-Disposition: form-data; name=\"");
        sb.append(str);
        sb.append("\"; filename=\"");
        sb.append(name);
        sb.append("\"");
        for (final Map.Entry<String, V> entry : map.entrySet()) {
            sb.append("; ");
            sb.append(entry.getKey());
            sb.append("=\"");
            sb.append((String)entry.getValue());
            sb.append("\"");
        }
        sb.append("\r\n");
        sb.append("Content-Transfer-Encoding: binary");
        sb.append("\r\n");
        sb.append("\r\n");
        if (this.d) {
            this.f.write(sb.toString().getBytes());
        }
        else {
            this.e.write(sb.toString().getBytes());
        }
        final FileInputStream fileInputStream = new FileInputStream(file);
        final byte[] b = new byte[8192];
        while (true) {
            final int read = fileInputStream.read(b);
            if (read == -1) {
                break;
            }
            if (this.d) {
                this.f.write(b, 0, read);
            }
            else {
                this.e.write(b, 0, read);
            }
        }
        fileInputStream.close();
        if (this.d) {
            this.f.write("\r\n".getBytes());
        }
        else {
            this.e.write("\r\n".getBytes());
            this.e.flush();
        }
    }
    
    public void a(final String s, final String s2) {
        this.a(s, s2, false);
    }
    
    public void a(final String str, final String s, final boolean b) {
        final StringBuilder sb = new StringBuilder();
        sb.append("--");
        sb.append(this.a);
        sb.append("\r\n");
        sb.append("Content-Disposition: form-data; name=\"");
        sb.append(str);
        sb.append("\"");
        sb.append("\r\n");
        sb.append("Content-Type: text/plain; charset=");
        sb.append(this.c);
        sb.append("\r\n");
        sb.append("\r\n");
        try {
            if (this.d) {
                this.f.write(sb.toString().getBytes());
            }
            else {
                this.e.write(sb.toString().getBytes());
            }
        }
        catch (final IOException ex) {}
        byte[] array2;
        final byte[] array = array2 = s.getBytes();
        if (b) {
            array2 = i.i().getEncryptImpl().a(array);
        }
        try {
            if (this.d) {
                this.f.write(array2);
                this.f.write("\r\n".getBytes());
            }
            else {
                this.e.write(array2);
                this.e.write("\r\n".getBytes());
            }
        }
        catch (final IOException ex2) {}
    }
    
    public void a(final String s, final File... array) {
        final StringBuilder sb = new StringBuilder();
        sb.append("--");
        sb.append(this.a);
        sb.append("\r\n");
        sb.append("Content-Disposition: form-data; name=\"");
        sb.append(s);
        sb.append("\"; filename=\"");
        sb.append(s);
        sb.append("\"");
        sb.append("\r\n");
        sb.append("Content-Transfer-Encoding: binary");
        sb.append("\r\n");
        sb.append("\r\n");
        if (this.d) {
            this.f.write(sb.toString().getBytes());
        }
        else {
            this.e.write(sb.toString().getBytes());
        }
        FilterOutputStream filterOutputStream;
        if (this.d) {
            filterOutputStream = this.f;
        }
        else {
            filterOutputStream = this.e;
        }
        com.apm.insight.l.i.a(filterOutputStream, array);
        if (this.d) {
            this.f.write("\r\n".getBytes());
        }
        else {
            this.e.write("\r\n".getBytes());
            this.e.flush();
        }
    }
}

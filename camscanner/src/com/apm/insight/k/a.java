// 
// Decompiled by Procyon v0.6.0
// 

package com.apm.insight.k;

import org.json.JSONArray;
import org.json.JSONObject;
import androidx.annotation.Nullable;
import com.apm.insight.l.q;
import java.util.Map;
import com.apm.insight.entity.b;
import com.apm.insight.runtime.u;
import com.apm.insight.l.p;
import com.apm.insight.i;

public class a
{
    private static Runnable a;
    private static int b;
    
    static {
        com.apm.insight.k.a.a = new Runnable() {
            @Override
            public void run() {
                if (p.b(i.g())) {
                    i();
                }
                if (com.apm.insight.k.a.b > 0) {
                    u u;
                    Runnable runnable;
                    long n;
                    if (com.apm.insight.l.a.b(i.g())) {
                        u = com.apm.insight.runtime.p.b();
                        runnable = com.apm.insight.k.a.a;
                        n = 15000L;
                    }
                    else {
                        u = com.apm.insight.runtime.p.b();
                        runnable = com.apm.insight.k.a.a;
                        n = 60000L;
                    }
                    u.a(runnable, n);
                }
            }
        };
        com.apm.insight.k.a.b = 0;
    }
    
    public static void a() {
        com.apm.insight.k.a.b = 40;
        com.apm.insight.runtime.p.b().a(com.apm.insight.k.a.a);
    }
    
    public static void b() {
        if (!k.b()) {
            k.c();
        }
        if (p.b(i.g()) && k.a(false)) {
            i();
        }
    }
    
    public static boolean c() {
        return false;
    }
    
    public static void d() {
    }
    
    @Nullable
    private static byte[] h() {
        try {
            return e.a(i.i().getConfigUrl(), null, com.apm.insight.entity.b.a().toString().getBytes());
        }
        finally {
            final Throwable t;
            q.a(t);
            return null;
        }
    }
    
    private static void i() {
        synchronized (a.class) {
            final int b = com.apm.insight.k.a.b;
            if (b > 0) {
                com.apm.insight.k.a.b = b - 1;
            }
            q.a((Object)"try fetchApmConfig");
            if (com.apm.insight.l.a.b(i.g())) {
                final Throwable obj;
                Label_0081: {
                    try {
                        final byte[] h = h();
                        if (h != null) {
                            new JSONObject(new String(h)).optJSONArray("data");
                            break Label_0081;
                        }
                    }
                    finally {
                        q.a("npth", obj);
                    }
                    obj = null;
                }
                final StringBuilder sb = new StringBuilder();
                sb.append("after fetchApmConfig net ");
                sb.append(obj);
                q.a((Object)sb.toString());
                if (obj == null) {
                    com.apm.insight.k.a.b -= 10;
                    return;
                }
                com.apm.insight.runtime.a.a((JSONArray)obj, true);
            }
            else {
                k.c();
                if (!k.a()) {
                    return;
                }
            }
            com.apm.insight.k.a.b = 0;
        }
    }
}

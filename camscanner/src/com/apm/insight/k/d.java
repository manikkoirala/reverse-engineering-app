// 
// Decompiled by Procyon v0.6.0
// 

package com.apm.insight.k;

import org.json.JSONException;
import com.apm.insight.runtime.p;
import com.apm.insight.h.b;
import java.util.Iterator;
import java.util.HashMap;
import java.util.Map;
import com.apm.insight.runtime.a;
import com.apm.insight.l.r;
import com.apm.insight.Npth;
import com.apm.insight.CrashType;
import com.apm.insight.l.q;
import java.io.File;
import com.apm.insight.l.o;
import org.json.JSONObject;
import com.apm.insight.i;
import androidx.annotation.NonNull;
import android.content.Context;
import android.annotation.SuppressLint;

public class d
{
    @SuppressLint({ "StaticFieldLeak" })
    private static volatile d a;
    private volatile Context b;
    
    private d(@NonNull final Context b) {
        this.b = b;
    }
    
    public static d a() {
        if (d.a == null) {
            d.a = new d(i.g());
        }
        return d.a;
    }
    
    public void a(final JSONObject jsonObject) {
        if (jsonObject != null) {
            if (jsonObject.length() > 0) {
                try {
                    final String f = e.f();
                    final File file = new File(o.a(this.b), o.c());
                    com.apm.insight.l.i.a(file, file.getName(), f, jsonObject, e.b());
                    if (e.a(f, jsonObject.toString()).a()) {
                        com.apm.insight.l.i.a(file);
                    }
                }
                finally {
                    final Throwable t;
                    q.b(t);
                }
            }
        }
    }
    
    public void a(final JSONObject jsonObject, final long n, final boolean b) {
        if (jsonObject == null) {
            return;
        }
        if (jsonObject.length() <= 0) {
            return;
        }
        try {
            final String c = e.c();
            final File a = o.a(this.b);
            final CrashType anr = CrashType.ANR;
            int n2 = 0;
            final File file = new File(a, i.a(n, anr, false, false));
            com.apm.insight.l.i.a(file, file.getName(), c, jsonObject, e.b());
            if (b) {
                if (!Npth.isStopUpload()) {
                    jsonObject.put("upload_scene", (Object)"direct");
                    jsonObject.put("crash_uuid", (Object)file.getName());
                    r.a(jsonObject);
                    File[] array2;
                    if (com.apm.insight.runtime.a.j()) {
                        final HashMap<String, com.apm.insight.runtime.r.a> a2 = com.apm.insight.runtime.r.a(n, "anr_trace");
                        final File[] array = new File[a2.size() + 2];
                        final Iterator iterator = a2.entrySet().iterator();
                        while (true) {
                            array2 = array;
                            if (!iterator.hasNext()) {
                                break;
                            }
                            final Map.Entry<String, V> entry = (Map.Entry<String, V>)iterator.next();
                            if (entry.getKey().equals(com.apm.insight.l.a.c(this.b))) {
                                continue;
                            }
                            array[n2] = o.a(this.b, ((com.apm.insight.runtime.r.a)entry.getValue()).b);
                            ++n2;
                        }
                    }
                    else {
                        array2 = new File[2];
                    }
                    array2[array2.length - 1] = o.a(this.b, i.f());
                    array2[array2.length - 2] = com.apm.insight.runtime.r.a(n);
                    if (e.a(c, jsonObject.toString(), array2).a()) {
                        com.apm.insight.l.i.a(file);
                        if (!Npth.hasCrash()) {
                            com.apm.insight.l.i.a(o.e(i.g()));
                        }
                    }
                }
            }
        }
        finally {}
    }
    
    public boolean a(final long n, final JSONObject jsonObject) {
        boolean b = false;
        if (jsonObject != null) {
            if (jsonObject.length() > 0) {
                try {
                    final String c = e.c();
                    final File file = new File(o.a(this.b), o.a(i.e()));
                    com.apm.insight.l.i.a(file, file.getName(), c, jsonObject, e.a());
                    jsonObject.put("upload_scene", (Object)"direct");
                    r.a(jsonObject);
                    if (e.b(c, jsonObject.toString()).a()) {
                        com.apm.insight.l.i.a(file);
                        b = true;
                    }
                    return b;
                }
                finally {
                    final Throwable t;
                    q.b(t);
                }
            }
        }
        return false;
    }
    
    public boolean a(final JSONObject jsonObject, final File file, final File file2) {
        final boolean b = false;
        boolean b2;
        try {
            final String g = e.g();
            r.a(jsonObject);
            e.a(g, jsonObject.toString(), file, file2, com.apm.insight.runtime.r.a(System.currentTimeMillis()), new File(com.apm.insight.h.b.a())).a();
        }
        finally {
            final Throwable t;
            q.b(t);
            b2 = b;
        }
        return b2;
    }
    
    public void b(final JSONObject jsonObject) {
        if (jsonObject != null) {
            if (jsonObject.length() != 0) {
                p.b().a(new Runnable(this, jsonObject) {
                    final JSONObject a;
                    final d b;
                    
                    @Override
                    public void run() {
                        final String c = e.c();
                        try {
                            this.a.put("upload_scene", (Object)"direct");
                        }
                        catch (final JSONException ex) {
                            ((Throwable)ex).printStackTrace();
                        }
                        e.b(c, this.a.toString());
                    }
                });
            }
        }
    }
}

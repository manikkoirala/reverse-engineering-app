// 
// Decompiled by Procyon v0.6.0
// 

package com.apm.insight.k;

import java.util.List;
import com.apm.insight.entity.b;
import com.apm.insight.runtime.a.f;
import java.util.LinkedList;
import java.util.Iterator;
import java.util.Map;
import android.os.Handler;
import android.text.TextUtils;
import org.json.JSONException;
import com.apm.insight.runtime.a;
import com.apm.insight.l.q;
import android.os.Looper;
import androidx.annotation.Nullable;
import com.apm.insight.h;
import androidx.annotation.NonNull;
import com.apm.insight.runtime.p;
import com.apm.insight.runtime.o;
import com.apm.insight.Npth;
import com.apm.insight.runtime.u;
import java.util.HashMap;
import com.apm.insight.entity.c;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.ConcurrentHashMap;

public final class g
{
    private static final ConcurrentHashMap<Object, ConcurrentLinkedQueue<c>> a;
    private static final HashMap<Object, HashMap<String, ConcurrentLinkedQueue<c>>> b;
    private static volatile g c;
    private final u d;
    private volatile boolean e;
    private Runnable f;
    
    static {
        a = new ConcurrentHashMap<Object, ConcurrentLinkedQueue<c>>();
        b = new HashMap<Object, HashMap<String, ConcurrentLinkedQueue<c>>>();
    }
    
    private g() {
        this.e = false;
        this.f = new Runnable() {
            final g a;
            
            @Override
            public void run() {
                if (Npth.isStopUpload()) {
                    return;
                }
                if (!g.b.isEmpty() && o.e()) {
                    f();
                }
                this.a.c();
                this.a.d.a(this.a.f, 30000L);
            }
        };
        this.d = p.b();
    }
    
    public static g a() {
        if (g.c == null) {
            synchronized (g.class) {
                if (g.c == null) {
                    g.c = new g();
                }
            }
        }
        return g.c;
    }
    
    public static void a(@NonNull final c c) {
        a(h.a(), c);
    }
    
    public static void a(@Nullable final Object o, @NonNull final c c) {
        final Handler a = p.b().a();
        if (a == null || a.getLooper() != Looper.myLooper()) {
            p.b().a(new Runnable(o, c) {
                final Object a;
                final c b;
                
                @Override
                public void run() {
                    g.a(this.a, this.b);
                }
            });
            return;
        }
        Object a2;
        if ((a2 = o) == null) {
            a2 = h.a();
        }
        if (!o.e()) {
            q.a("EventUploadQueue", "enqueue before init.");
            c(a2, c);
            return;
        }
        if (!com.apm.insight.runtime.a.a(a2)) {
            com.apm.insight.k.a.b();
        }
        f();
        String string;
        try {
            string = c.h().getString("log_type");
        }
        catch (final JSONException ex) {
            ((Throwable)ex).printStackTrace();
            string = null;
        }
        if (!TextUtils.isEmpty((CharSequence)string) && com.apm.insight.runtime.a.a(a2, string)) {
            final StringBuilder sb = new StringBuilder();
            sb.append("logType ");
            sb.append(string);
            sb.append(" enqueued");
            q.a("EventUploadQueue", sb.toString());
            b(a2, c);
        }
        else {
            final StringBuilder sb2 = new StringBuilder();
            sb2.append("logType ");
            sb2.append(string);
            sb2.append(" not sampled");
            q.a("EventUploadQueue", sb2.toString());
        }
    }
    
    private static void b(Object o, final c e) {
        synchronized (o) {
            final ConcurrentHashMap<Object, ConcurrentLinkedQueue<c>> a = g.a;
            ConcurrentLinkedQueue value;
            if ((value = a.get(o)) == null) {
                value = new ConcurrentLinkedQueue();
                a.put(o, value);
            }
            monitorexit(o);
            value.add(e);
            final int size = a.size();
            final boolean b = size >= 30;
            o = new StringBuilder();
            ((StringBuilder)o).append("[enqueue] size=");
            ((StringBuilder)o).append(size);
            q.b(((StringBuilder)o).toString());
            if (b) {
                g();
            }
        }
    }
    
    private static void c(Object o, final c e) {
        try {
            final String string = e.h().getString("log_type");
            final HashMap<Object, HashMap<String, ConcurrentLinkedQueue<c>>> b = g.b;
            synchronized (b) {
                HashMap value;
                if ((value = b.get(string)) == null) {
                    value = new HashMap();
                    b.put(o, value);
                }
                if ((o = value.get(string)) == null) {
                    o = new ConcurrentLinkedQueue();
                    value.put(string, o);
                }
                monitorexit(b);
                ((ConcurrentLinkedQueue<c>)o).add(e);
                if (((ConcurrentLinkedQueue)o).size() > 100) {
                    ((ConcurrentLinkedQueue<Object>)o).poll();
                }
            }
        }
        catch (final JSONException ex) {
            ((Throwable)ex).printStackTrace();
        }
    }
    
    private static void f() {
        Object b = g.b;
        synchronized (b) {
            final HashMap hashMap = new HashMap((Map<?, ?>)b);
            ((HashMap)b).clear();
            monitorexit(b);
            if (!com.apm.insight.runtime.a.b()) {
                q.a("EventUploadQueue", "ApmConfig not inited, clear cache.");
                return;
            }
            final Iterator iterator = hashMap.entrySet().iterator();
            while (iterator.hasNext()) {
                b = iterator.next();
                for (final Map.Entry<String, V> entry : ((HashMap)((Map.Entry<Object, ?>)b).getValue()).entrySet()) {
                    final String str = entry.getKey();
                    final ConcurrentLinkedQueue concurrentLinkedQueue = (ConcurrentLinkedQueue)entry.getValue();
                    Label_0190: {
                        if (concurrentLinkedQueue != null) {
                            if (!com.apm.insight.runtime.a.b() || com.apm.insight.runtime.a.a(((Map.Entry<Object, ?>)b).getKey(), str)) {
                                while (!concurrentLinkedQueue.isEmpty()) {
                                    try {
                                        final c c = concurrentLinkedQueue.poll();
                                        if (c != null) {
                                            b(((Map.Entry<Object, ?>)b).getKey(), c);
                                            continue;
                                        }
                                    }
                                    finally {
                                        break;
                                    }
                                    break Label_0190;
                                }
                                continue;
                            }
                        }
                    }
                    final StringBuilder sb = new StringBuilder();
                    sb.append("logType ");
                    sb.append(str);
                    sb.append(" not sampled");
                    q.a("EventUploadQueue", sb.toString());
                }
            }
        }
    }
    
    private static void g() {
        if (!o.e()) {
            return;
        }
        if (Npth.isStopUpload()) {
            return;
        }
        try {
            p.b().a(new Runnable() {
                @Override
                public void run() {
                    g.a().c();
                }
            });
        }
        finally {}
    }
    
    public void b() {
        if (g.a.isEmpty()) {
            this.d.a(this.f, 30000L);
        }
        else {
            this.d.a(this.f);
        }
    }
    
    public void c() {
        Object d = this.d;
        synchronized (d) {
            if (this.e) {
                return;
            }
            this.e = true;
            monitorexit(d);
            final LinkedList list = new LinkedList();
        Label_0045:
            while (true) {
                for (final Map.Entry<K, ConcurrentLinkedQueue<?>> entry : g.a.entrySet()) {
                    d = entry.getValue();
                    final K key = entry.getKey();
                    while (!((ConcurrentLinkedQueue)d).isEmpty()) {
                        int n = 0;
                    Label_0126_Outer:
                        while (true) {
                            while (true) {
                                if (n < 30) {
                                    try {
                                        if (!((ConcurrentLinkedQueue)d).isEmpty()) {
                                            list.add(((ConcurrentLinkedQueue)d).poll());
                                            ++n;
                                            continue Label_0126_Outer;
                                        }
                                        if (list.isEmpty()) {
                                            continue Label_0045;
                                        }
                                        final com.apm.insight.entity.a a = com.apm.insight.runtime.a.f.a().a(list, com.apm.insight.entity.b.a(key));
                                        if (a != null) {
                                            q.a((Object)"upload events");
                                            com.apm.insight.k.d.a().a(a.h());
                                        }
                                        list.clear();
                                    }
                                    finally {
                                        final Throwable t;
                                        q.b(t);
                                    }
                                    break;
                                }
                                continue;
                            }
                        }
                    }
                }
                break;
            }
            this.e = false;
        }
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.apm.insight.k;

import com.apm.insight.j.d;
import com.apm.insight.l.q;
import com.apm.insight.runtime.s;
import com.apm.insight.nativecrash.NativeImpl;
import com.apm.insight.b.g;
import com.apm.insight.l.a;
import com.apm.insight.runtime.p;
import android.content.Context;

public final class i implements Runnable
{
    private Context a;
    
    private i(final Context a) {
        this.a = a;
    }
    
    public static void a(final Context context) {
        a(context, 0);
    }
    
    public static void a(final Context context, final int n) {
        p.b().a(new i(context), n);
    }
    
    public static boolean a() {
        return b.a().c() || !a.b(com.apm.insight.i.g());
    }
    
    @Override
    public void run() {
        while (true) {
            try {
                g.a(this.a).a().f();
                try {
                    if (com.apm.insight.l.a.b(this.a)) {
                        b.a().a(com.apm.insight.l.p.b(this.a));
                    }
                    else {
                        NativeImpl.i();
                    }
                    s.a().a(com.apm.insight.i.a().b(), com.apm.insight.entity.b.b());
                    if (p.b().a() != null) {
                        return;
                    }
                }
                finally {
                    try {
                        final Throwable t;
                        q.b(t);
                    }
                    finally {
                        s.a().a(com.apm.insight.i.a().b(), com.apm.insight.entity.b.b());
                        if (p.b().a() != null) {
                            d.a(p.b().a(), this.a).a();
                        }
                    }
                }
            }
            finally {
                continue;
            }
            break;
        }
    }
}

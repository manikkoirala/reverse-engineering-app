// 
// Decompiled by Procyon v0.6.0
// 

package com.apm.insight.k;

import androidx.annotation.NonNull;
import com.apm.insight.runtime.p;
import com.apm.insight.runtime.a;
import com.apm.insight.l.o;
import java.util.Iterator;
import com.apm.insight.l.q;
import com.apm.insight.runtime.e;
import com.apm.insight.entity.b;
import com.apm.insight.l.i;
import org.json.JSONArray;
import java.util.HashMap;
import java.util.Map;
import java.io.File;

public class k
{
    private static File a;
    private static boolean b = false;
    private static boolean c = false;
    private static String d = "exception_modules";
    private static String e = "npth";
    private static Map<String, String> f;
    
    public static void a(final String s) {
        if (k.f == null) {
            k.f = new HashMap<String, String>();
        }
        k.f.put(s, String.valueOf(System.currentTimeMillis()));
    }
    
    public static void a(final boolean p0, final JSONArray p1) {
        // 
        // This method could not be decompiled.
        // 
        // Original Bytecode:
        // 
        //     3: astore_2       
        //     4: aload_2        
        //     5: invokestatic    com/apm/insight/i.g:()Landroid/content/Context;
        //     8: invokestatic    com/apm/insight/l/o.j:(Landroid/content/Context;)Ljava/lang/String;
        //    11: ldc             "apminsight/configCrash/configFile"
        //    13: invokespecial   java/io/File.<init>:(Ljava/lang/String;Ljava/lang/String;)V
        //    16: aload_2        
        //    17: aload_1        
        //    18: iconst_0       
        //    19: invokestatic    com/apm/insight/l/i.a:(Ljava/io/File;Lorg/json/JSONArray;Z)V
        //    22: invokestatic    com/apm/insight/k/k.g:()Ljava/io/File;
        //    25: getstatic       com/apm/insight/k/k.f:Ljava/util/Map;
        //    28: invokestatic    com/apm/insight/l/i.a:(Ljava/io/File;Ljava/util/Map;)V
        //    31: return         
        //    32: astore_1       
        //    33: goto            22
        //    36: astore_1       
        //    37: goto            31
        //    Exceptions:
        //  Try           Handler
        //  Start  End    Start  End    Type                 
        //  -----  -----  -----  -----  ---------------------
        //  0      22     32     36     Ljava/io/IOException;
        //  22     31     36     40     Any
        // 
        // The error that occurred was:
        // 
        // java.lang.IllegalStateException: Expression is linked from several locations: Label_0022:
        //     at com.strobel.decompiler.ast.Error.expressionLinkedFromMultipleLocations(Error.java:27)
        //     at com.strobel.decompiler.ast.AstOptimizer.mergeDisparateObjectInitializations(AstOptimizer.java:2604)
        //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:235)
        //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:42)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:206)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:93)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethodBody(AstBuilder.java:868)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethod(AstBuilder.java:761)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addTypeMembers(AstBuilder.java:638)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeCore(AstBuilder.java:605)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeNoCache(AstBuilder.java:195)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createType(AstBuilder.java:162)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addType(AstBuilder.java:137)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.buildAst(JavaLanguage.java:71)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.decompileType(JavaLanguage.java:59)
        //     at com.strobel.decompiler.DecompilerDriver.decompileType(DecompilerDriver.java:333)
        //     at com.strobel.decompiler.DecompilerDriver.decompileJar(DecompilerDriver.java:254)
        //     at com.strobel.decompiler.DecompilerDriver.main(DecompilerDriver.java:144)
        // 
        throw new IllegalStateException("An error occurred while decompiling this method.");
    }
    
    static boolean a() {
        return k.b;
    }
    
    static boolean a(boolean b) {
        final File g = g();
        try {
            Map<String, String> f;
            if ((f = k.f) == null) {
                f = i.e(g);
            }
            if ((k.f = f) == null) {
                k.f = new HashMap<String, String>();
            }
            if (f.size() < b.c()) {}
            final Iterator<String> iterator = b.d().iterator();
            while (iterator.hasNext() && k.f.containsKey(iterator.next())) {}
            final long currentTimeMillis = System.currentTimeMillis();
            final Iterator<Map.Entry<String, String>> iterator2 = k.f.entrySet().iterator();
            b = false;
            while (iterator2.hasNext()) {
                final Map.Entry<K, String> entry = (Map.Entry<K, String>)iterator2.next();
                try {
                    if (currentTimeMillis - Long.decode(entry.getValue()) <= com.apm.insight.runtime.e.e((String)entry.getKey())) {
                        continue;
                    }
                    b = true;
                }
                finally {
                    final Throwable t;
                    q.a(t);
                }
            }
            String s;
            if (b) {
                s = "config should be updated";
            }
            else {
                s = "config should not be updated";
            }
            q.a((Object)s);
            return b;
        }
        finally {
            return true;
        }
    }
    
    static boolean b() {
        return k.c;
    }
    
    static void c() {
        if (k.b) {
            return;
        }
        k.c = true;
        final File file = new File(o.j(com.apm.insight.i.g()), "apminsight/configCrash/configFile");
        if (!file.exists()) {
            return;
        }
        try {
            com.apm.insight.runtime.a.a(new JSONArray(i.c(file)), false);
            k.b = true;
        }
        finally {
            com.apm.insight.runtime.a.a(null, false);
        }
    }
    
    public static void d() {
        c();
        if (a(false)) {
            com.apm.insight.k.a.a();
        }
    }
    
    public static void e() {
        p.b().a(new Runnable() {
            @Override
            public void run() {
                if (k.a(false)) {
                    com.apm.insight.k.a.a();
                }
            }
        });
    }
    
    public static void f() {
        final Map<String, String> f = k.f;
        if (f != null) {
            f.clear();
        }
    }
    
    @NonNull
    private static File g() {
        if (k.a == null) {
            k.a = new File(o.j(com.apm.insight.i.g()), "apminsight/configCrash/configInvalid");
        }
        return k.a;
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.apm.insight.k;

import androidx.annotation.Nullable;
import org.json.JSONObject;

public class l
{
    private final int a;
    private String b;
    private JSONObject c;
    private byte[] d;
    
    public l(final int a) {
        this.a = a;
    }
    
    public l(final int a, final String b) {
        this.a = a;
        this.b = b;
    }
    
    public l(final int a, final Throwable t) {
        this.a = a;
        if (t != null) {
            this.b = t.getMessage();
        }
    }
    
    public l(final int a, final JSONObject c) {
        this.a = a;
        this.c = c;
    }
    
    public l(final int a, final byte[] d) {
        this.a = a;
        this.d = d;
    }
    
    public boolean a() {
        return this.a != 207;
    }
    
    @Nullable
    public byte[] b() {
        return this.d;
    }
}

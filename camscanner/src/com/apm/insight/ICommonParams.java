// 
// Decompiled by Procyon v0.6.0
// 

package com.apm.insight;

import java.util.List;
import java.util.Map;

public interface ICommonParams
{
    Map<String, Object> getCommonParams();
    
    String getDeviceId();
    
    List<String> getPatchInfo();
    
    Map<String, Integer> getPluginInfo();
    
    String getSessionId();
    
    long getUserId();
}

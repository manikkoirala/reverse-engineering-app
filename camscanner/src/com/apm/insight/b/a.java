// 
// Decompiled by Procyon v0.6.0
// 

package com.apm.insight.b;

import com.apm.insight.i;
import android.app.ActivityManager$ProcessErrorStateInfo;

public class a
{
    static String a(final ActivityManager$ProcessErrorStateInfo activityManager$ProcessErrorStateInfo) {
        if (!i.s()) {
            return "|------------- processErrorStateInfo--------------|\ndisable anr info\n\"-----------------------end----------------------------\"";
        }
        final StringBuilder sb = new StringBuilder();
        sb.append("|------------- processErrorStateInfo--------------|\n");
        final StringBuilder sb2 = new StringBuilder();
        sb2.append("condition: ");
        sb2.append(activityManager$ProcessErrorStateInfo.condition);
        sb2.append("\n");
        sb.append(sb2.toString());
        final StringBuilder sb3 = new StringBuilder();
        sb3.append("processName: ");
        sb3.append(activityManager$ProcessErrorStateInfo.processName);
        sb3.append("\n");
        sb.append(sb3.toString());
        final StringBuilder sb4 = new StringBuilder();
        sb4.append("pid: ");
        sb4.append(activityManager$ProcessErrorStateInfo.pid);
        sb4.append("\n");
        sb.append(sb4.toString());
        final StringBuilder sb5 = new StringBuilder();
        sb5.append("uid: ");
        sb5.append(activityManager$ProcessErrorStateInfo.uid);
        sb5.append("\n");
        sb.append(sb5.toString());
        final StringBuilder sb6 = new StringBuilder();
        sb6.append("tag: ");
        sb6.append(activityManager$ProcessErrorStateInfo.tag);
        sb6.append("\n");
        sb.append(sb6.toString());
        final StringBuilder sb7 = new StringBuilder();
        sb7.append("shortMsg : ");
        sb7.append(activityManager$ProcessErrorStateInfo.shortMsg);
        sb7.append("\n");
        sb.append(sb7.toString());
        final StringBuilder sb8 = new StringBuilder();
        sb8.append("longMsg : ");
        sb8.append(activityManager$ProcessErrorStateInfo.longMsg);
        sb8.append("\n");
        sb.append(sb8.toString());
        sb.append("-----------------------end----------------------------");
        return sb.toString();
    }
    
    static boolean a(final ActivityManager$ProcessErrorStateInfo activityManager$ProcessErrorStateInfo, final ActivityManager$ProcessErrorStateInfo activityManager$ProcessErrorStateInfo2) {
        return String.valueOf(activityManager$ProcessErrorStateInfo.condition).equals(String.valueOf(activityManager$ProcessErrorStateInfo2.condition)) && String.valueOf(activityManager$ProcessErrorStateInfo.processName).equals(String.valueOf(activityManager$ProcessErrorStateInfo2.processName)) && String.valueOf(activityManager$ProcessErrorStateInfo.pid).equals(String.valueOf(activityManager$ProcessErrorStateInfo2.pid)) && String.valueOf(activityManager$ProcessErrorStateInfo.uid).equals(String.valueOf(activityManager$ProcessErrorStateInfo2.uid)) && String.valueOf(activityManager$ProcessErrorStateInfo.tag).equals(String.valueOf(activityManager$ProcessErrorStateInfo2.tag)) && String.valueOf(activityManager$ProcessErrorStateInfo.shortMsg).equals(String.valueOf(activityManager$ProcessErrorStateInfo2.shortMsg)) && String.valueOf(activityManager$ProcessErrorStateInfo.longMsg).equals(String.valueOf(activityManager$ProcessErrorStateInfo2.longMsg));
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.apm.insight.b;

import android.text.TextUtils;
import androidx.annotation.Nullable;
import com.apm.insight.f;
import com.apm.insight.c;
import com.apm.insight.l.v;
import android.os.Looper;
import org.json.JSONObject;
import android.os.Process;
import com.apm.insight.l.a;
import android.os.SystemClock;
import com.apm.insight.l.r;
import android.content.Context;
import android.app.ActivityManager$ProcessErrorStateInfo;
import android.os.FileObserver;

public class d
{
    private static String a;
    private static long b = -1L;
    private static boolean c = false;
    private static FileObserver d;
    private static ActivityManager$ProcessErrorStateInfo e;
    
    static String a(final Context context, final int n) {
        if (r.a(256)) {
            com.apm.insight.b.d.c = false;
            return "TEST_ANR_INFO";
        }
        if (SystemClock.uptimeMillis() - com.apm.insight.b.d.b < 5000L) {
            return null;
        }
        try {
            final ActivityManager$ProcessErrorStateInfo a = com.apm.insight.l.a.a(context, n);
            if (a != null && Process.myPid() == a.pid) {
                final ActivityManager$ProcessErrorStateInfo e = com.apm.insight.b.d.e;
                if (e != null && com.apm.insight.b.a.a(e, a)) {
                    return null;
                }
                com.apm.insight.b.d.e = a;
                com.apm.insight.b.d.a = null;
                com.apm.insight.b.d.b = SystemClock.uptimeMillis();
                com.apm.insight.b.d.c = false;
                return com.apm.insight.b.a.a(a);
            }
        }
        finally {}
        final String a2 = com.apm.insight.b.d.a;
        if (a2 != null) {
            com.apm.insight.b.d.c = true;
            com.apm.insight.b.d.a = null;
            com.apm.insight.b.d.b = SystemClock.uptimeMillis();
            return a2;
        }
        return null;
    }
    
    public static JSONObject a(final boolean b) {
        try {
            final StackTraceElement[] stackTrace = Looper.getMainLooper().getThread().getStackTrace();
            final JSONObject jsonObject = new JSONObject();
            jsonObject.put("thread_number", 1);
            jsonObject.put("mainStackFromTrace", (Object)v.a(stackTrace));
            return jsonObject;
        }
        finally {
            final Throwable t;
            com.apm.insight.c.a().a("NPTH_CATCH", t);
            return null;
        }
    }
    
    public static void a(final String s, final f f) {
        final FileObserver d = com.apm.insight.b.d.d;
        if (d != null) {
            d.stopWatching();
        }
        (com.apm.insight.b.d.d = new FileObserver(s, 136, f, s) {
            final f a;
            final String b;
            
            public void onEvent(final int n, @Nullable final String s) {
                if (TextUtils.isEmpty((CharSequence)s)) {
                    return;
                }
                try {
                    com.apm.insight.b.d.a = this.a.a(this.b, s);
                }
                finally {
                    final Throwable t;
                    com.apm.insight.c.a().a("NPTH_CATCH", t);
                }
            }
        }).startWatching();
    }
    
    public static boolean a() {
        return com.apm.insight.b.d.c;
    }
    
    public static void b() {
        com.apm.insight.b.d.e = null;
    }
}

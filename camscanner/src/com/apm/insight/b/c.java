// 
// Decompiled by Procyon v0.6.0
// 

package com.apm.insight.b;

import com.apm.insight.runtime.p;
import android.os.SystemClock;

public class c
{
    private static long b;
    private final b a;
    private boolean c;
    private final Runnable d;
    
    c(final b a) {
        this.c = false;
        final Runnable d = new Runnable() {
            final c a;
            
            @Override
            public void run() {
                if (this.a.c) {
                    return;
                }
                this.a.a.d();
                com.apm.insight.b.c.b = SystemClock.uptimeMillis();
                f.a();
                p.b().a(this.a.d, 500L);
                com.apm.insight.runtime.b.a(com.apm.insight.b.c.b);
            }
        };
        this.d = d;
        this.a = a;
        p.b().a(d, 5000L);
    }
    
    public static boolean c() {
        return SystemClock.uptimeMillis() - c.b <= 15000L;
    }
    
    public void a() {
        if (this.c) {
            return;
        }
        p.b().a(this.d, 5000L);
    }
    
    public void b() {
        this.c = true;
    }
}

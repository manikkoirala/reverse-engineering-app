// 
// Decompiled by Procyon v0.6.0
// 

package com.apm.insight.b;

import android.os.SystemClock;
import androidx.annotation.Nullable;
import androidx.annotation.NonNull;
import com.apm.insight.CrashType;
import com.apm.insight.ICrashCallback;
import java.io.IOException;
import com.apm.insight.nativecrash.NativeImpl;
import com.apm.insight.runtime.r;
import com.apm.insight.l.i;
import java.util.Date;
import com.apm.insight.l.o;
import com.apm.insight.Npth;
import java.util.LinkedList;
import com.apm.insight.runtime.a;
import android.text.TextUtils;
import java.util.Iterator;
import java.util.Map;
import java.util.HashMap;
import org.json.JSONException;
import com.apm.insight.l.l;
import org.json.JSONArray;
import org.json.JSONObject;
import android.content.Context;
import java.io.File;
import java.util.regex.Pattern;
import java.util.List;

public class b
{
    static volatile boolean a = true;
    private static volatile boolean f;
    private long A;
    private final Runnable B;
    private int C;
    private List<Pattern> D;
    private File E;
    Pattern b;
    private c c;
    private final Context d;
    private volatile boolean e;
    private long g;
    private File h;
    private boolean i;
    private JSONObject j;
    private JSONObject k;
    private String l;
    private String m;
    private String n;
    private String o;
    private JSONArray p;
    private JSONObject q;
    private int r;
    private long s;
    private JSONArray t;
    private JSONArray u;
    private JSONObject v;
    private boolean w;
    private final Object x;
    private volatile boolean y;
    private long z;
    
    public b(final Context d) {
        this.g = -1L;
        this.h = null;
        this.i = true;
        this.l = "unknown";
        this.m = "unknown";
        this.n = "unknown";
        this.o = "npth_inner_default";
        this.r = 0;
        this.s = -1L;
        this.x = new Object();
        this.z = -1L;
        this.A = 0L;
        this.B = new Runnable() {
            final b a;
            
            @Override
            public void run() {
                try {
                    this.a.a(200, 25);
                }
                finally {
                    final Throwable t;
                    com.apm.insight.c.a().a("NPTH_CATCH", t);
                }
            }
        };
        this.C = 0;
        this.D = null;
        this.b = null;
        this.E = null;
        this.d = d;
    }
    
    private static String a(final float n) {
        if (n <= 0.0f) {
            return "0%";
        }
        if (n <= 0.1f) {
            return "0% - 10%";
        }
        if (n <= 0.3f) {
            return "10% - 30%";
        }
        if (n <= 0.6f) {
            return "30% - 60%";
        }
        if (n <= 0.9f) {
            return "60% - 90%";
        }
        return "90% - 100%";
    }
    
    private static String a(final float n, final float n2) {
        if (n2 > 0.0f) {
            return a(n / n2);
        }
        String s;
        if (n > 0.0f) {
            s = "100%";
        }
        else {
            s = "0%";
        }
        return s;
    }
    
    private JSONObject a(final String s, final JSONArray jsonArray) {
        final JSONObject jsonObject = new JSONObject();
        final JSONArray a = com.apm.insight.l.l.a(256, 128, jsonArray);
        if (a.length() != jsonArray.length()) {
            ++this.r;
        }
        try {
            jsonObject.put("thread_name", (Object)s);
            jsonObject.put("thread_stack", (Object)a);
            return jsonObject;
        }
        catch (final JSONException ex) {
            return null;
        }
    }
    
    private void a(final String p0, final JSONObject p1) {
        // 
        // This method could not be decompiled.
        // 
        // Original Bytecode:
        // 
        //     3: pop2           
        //     4: aload_1        
        //     5: ldc             "\n"
        //     7: invokevirtual   java/lang/String.split:(Ljava/lang/String;)[Ljava/lang/String;
        //    10: astore          21
        //    12: iconst_3       
        //    13: newarray        F
        //    15: astore          26
        //    17: new             Ljava/util/HashMap;
        //    20: dup            
        //    21: invokespecial   java/util/HashMap.<init>:()V
        //    24: astore          25
        //    26: new             Ljava/util/HashMap;
        //    29: dup            
        //    30: invokespecial   java/util/HashMap.<init>:()V
        //    33: astore          24
        //    35: new             Ljava/util/HashMap;
        //    38: dup            
        //    39: invokespecial   java/util/HashMap.<init>:()V
        //    42: astore_1       
        //    43: new             Ljava/util/HashMap;
        //    46: dup            
        //    47: invokespecial   java/util/HashMap.<init>:()V
        //    50: astore          17
        //    52: new             Ljava/util/HashMap;
        //    55: dup            
        //    56: invokespecial   java/util/HashMap.<init>:()V
        //    59: astore          16
        //    61: aload           21
        //    63: arraylength    
        //    64: istore          7
        //    66: ldc             "unknown"
        //    68: astore          18
        //    70: ldc             "unknown"
        //    72: astore          11
        //    74: ldc             "unknown"
        //    76: astore          12
        //    78: iconst_0       
        //    79: istore          6
        //    81: iconst_0       
        //    82: istore          5
        //    84: iconst_0       
        //    85: istore          9
        //    87: iload           6
        //    89: iload           7
        //    91: if_icmpge       1669
        //    94: aload           21
        //    96: iload           6
        //    98: aaload         
        //    99: astore          19
        //   101: aload           19
        //   103: invokestatic    android/text/TextUtils.isEmpty:(Ljava/lang/CharSequence;)Z
        //   106: ifeq            127
        //   109: aload           11
        //   111: astore          14
        //   113: aload           12
        //   115: astore          15
        //   117: iload           9
        //   119: istore          10
        //   121: aload_1        
        //   122: astore          13
        //   124: goto            1648
        //   127: iload           5
        //   129: ifeq            1567
        //   132: iload           5
        //   134: iconst_1       
        //   135: if_icmpeq       1188
        //   138: iload           5
        //   140: iconst_2       
        //   141: if_icmpeq       1181
        //   144: iload           5
        //   146: iconst_3       
        //   147: if_icmpeq       164
        //   150: aload           11
        //   152: astore          19
        //   154: aload           12
        //   156: astore          20
        //   158: aload_1        
        //   159: astore          14
        //   161: goto            1512
        //   164: aload           19
        //   166: ldc             "\\s"
        //   168: invokevirtual   java/lang/String.split:(Ljava/lang/String;)[Ljava/lang/String;
        //   171: astore          23
        //   173: aload           23
        //   175: arraylength    
        //   176: iconst_2       
        //   177: if_icmpge       194
        //   180: aload_1        
        //   181: astore          14
        //   183: aload           11
        //   185: astore          19
        //   187: aload           12
        //   189: astore          20
        //   191: goto            1512
        //   194: ldc             "CPU"
        //   196: aload           23
        //   198: iconst_0       
        //   199: aaload         
        //   200: invokevirtual   java/lang/String.equalsIgnoreCase:(Ljava/lang/String;)Z
        //   203: ifeq            300
        //   206: ldc             "usage"
        //   208: aload           23
        //   210: iconst_1       
        //   211: aaload         
        //   212: invokevirtual   java/lang/String.equalsIgnoreCase:(Ljava/lang/String;)Z
        //   215: ifeq            300
        //   218: aload           19
        //   220: ldc             "ago"
        //   222: invokevirtual   java/lang/String.contains:(Ljava/lang/CharSequence;)Z
        //   225: ifeq            231
        //   228: iconst_1       
        //   229: istore          9
        //   231: iload           9
        //   233: istore          10
        //   235: aload           25
        //   237: invokevirtual   java/util/HashMap.isEmpty:()Z
        //   240: ifeq            343
        //   243: iload           9
        //   245: istore          10
        //   247: aload           24
        //   249: invokevirtual   java/util/HashMap.isEmpty:()Z
        //   252: ifeq            343
        //   255: iload           9
        //   257: istore          10
        //   259: aload_1        
        //   260: invokevirtual   java/util/HashMap.isEmpty:()Z
        //   263: ifeq            343
        //   266: iload           9
        //   268: istore          10
        //   270: aload           16
        //   272: invokevirtual   java/util/HashMap.isEmpty:()Z
        //   275: ifeq            343
        //   278: aload           17
        //   280: invokevirtual   java/util/HashMap.isEmpty:()Z
        //   283: ifne            293
        //   286: iload           9
        //   288: istore          10
        //   290: goto            343
        //   293: iload           5
        //   295: istore          4
        //   297: goto            1620
        //   300: aload           25
        //   302: invokevirtual   java/util/HashMap.isEmpty:()Z
        //   305: ifne            353
        //   308: aload           24
        //   310: invokevirtual   java/util/HashMap.isEmpty:()Z
        //   313: ifne            353
        //   316: aload_1        
        //   317: invokevirtual   java/util/HashMap.isEmpty:()Z
        //   320: ifne            353
        //   323: aload           16
        //   325: invokevirtual   java/util/HashMap.isEmpty:()Z
        //   328: ifne            353
        //   331: aload           17
        //   333: invokevirtual   java/util/HashMap.isEmpty:()Z
        //   336: ifne            353
        //   339: iload           9
        //   341: istore          10
        //   343: iconst_4       
        //   344: istore          4
        //   346: iload           10
        //   348: istore          9
        //   350: goto            1620
        //   353: aload           25
        //   355: invokevirtual   java/util/HashMap.isEmpty:()Z
        //   358: ifeq            394
        //   361: aload           23
        //   363: iconst_1       
        //   364: aaload         
        //   365: ldc             "TOTAL:"
        //   367: invokevirtual   java/lang/String.equalsIgnoreCase:(Ljava/lang/String;)Z
        //   370: ifeq            394
        //   373: ldc             ""
        //   375: astore          14
        //   377: aload           25
        //   379: astore          13
        //   381: aload_1        
        //   382: astore          15
        //   384: aload           13
        //   386: astore_1       
        //   387: aload           14
        //   389: astore          13
        //   391: goto            614
        //   394: aload           19
        //   396: aload_0        
        //   397: getfield        com/apm/insight/b/b.d:Landroid/content/Context;
        //   400: invokevirtual   android/content/Context.getPackageName:()Ljava/lang/String;
        //   403: invokevirtual   java/lang/String.contains:(Ljava/lang/CharSequence;)Z
        //   406: ifeq            521
        //   409: ldc             ""
        //   411: astore          13
        //   413: iconst_0       
        //   414: istore          4
        //   416: iload           4
        //   418: aload           23
        //   420: arraylength    
        //   421: if_icmpge       512
        //   424: aload           23
        //   426: iload           4
        //   428: aaload         
        //   429: aload_0        
        //   430: getfield        com/apm/insight/b/b.d:Landroid/content/Context;
        //   433: invokevirtual   android/content/Context.getPackageName:()Ljava/lang/String;
        //   436: invokevirtual   java/lang/String.contains:(Ljava/lang/CharSequence;)Z
        //   439: ifeq            506
        //   442: new             Ljava/lang/StringBuilder;
        //   445: dup            
        //   446: invokespecial   java/lang/StringBuilder.<init>:()V
        //   449: astore          14
        //   451: aload           23
        //   453: iload           4
        //   455: aaload         
        //   456: astore          13
        //   458: aload           14
        //   460: aload           13
        //   462: aload           13
        //   464: bipush          47
        //   466: invokevirtual   java/lang/String.indexOf:(I)I
        //   469: iconst_1       
        //   470: iadd           
        //   471: aload           23
        //   473: iload           4
        //   475: aaload         
        //   476: invokevirtual   java/lang/String.length:()I
        //   479: iconst_1       
        //   480: isub           
        //   481: invokevirtual   java/lang/String.substring:(II)Ljava/lang/String;
        //   484: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   487: pop            
        //   488: aload           14
        //   490: bipush          95
        //   492: invokevirtual   java/lang/StringBuilder.append:(C)Ljava/lang/StringBuilder;
        //   495: pop            
        //   496: aload           14
        //   498: invokevirtual   java/lang/StringBuilder.toString:()Ljava/lang/String;
        //   501: astore          13
        //   503: goto            506
        //   506: iinc            4, 1
        //   509: goto            416
        //   512: aload_1        
        //   513: astore          15
        //   515: aload           15
        //   517: astore_1       
        //   518: goto            614
        //   521: aload_1        
        //   522: astore          15
        //   524: aload           24
        //   526: invokevirtual   java/util/HashMap.isEmpty:()Z
        //   529: ifeq            552
        //   532: aload           19
        //   534: ldc             "system_server:"
        //   536: invokevirtual   java/lang/String.contains:(Ljava/lang/CharSequence;)Z
        //   539: ifeq            552
        //   542: ldc             ""
        //   544: astore          13
        //   546: aload           24
        //   548: astore_1       
        //   549: goto            614
        //   552: aload           16
        //   554: invokevirtual   java/util/HashMap.isEmpty:()Z
        //   557: ifeq            580
        //   560: aload           19
        //   562: ldc             "kswapd"
        //   564: invokevirtual   java/lang/String.contains:(Ljava/lang/CharSequence;)Z
        //   567: ifeq            580
        //   570: ldc             ""
        //   572: astore          13
        //   574: aload           16
        //   576: astore_1       
        //   577: goto            614
        //   580: aload           17
        //   582: invokevirtual   java/util/HashMap.isEmpty:()Z
        //   585: ifeq            608
        //   588: aload           19
        //   590: ldc             "dex2oat"
        //   592: invokevirtual   java/lang/String.contains:(Ljava/lang/CharSequence;)Z
        //   595: ifeq            608
        //   598: ldc             ""
        //   600: astore          13
        //   602: aload           17
        //   604: astore_1       
        //   605: goto            614
        //   608: ldc             ""
        //   610: astore          13
        //   612: aconst_null    
        //   613: astore_1       
        //   614: aload           15
        //   616: astore          14
        //   618: aload_1        
        //   619: ifnull          183
        //   622: iconst_0       
        //   623: istore          4
        //   625: aload           23
        //   627: iload           4
        //   629: aaload         
        //   630: astore          14
        //   632: ldc             "%"
        //   634: astore          22
        //   636: iload           4
        //   638: istore          8
        //   640: aload           14
        //   642: ldc             "%"
        //   644: invokevirtual   java/lang/String.contains:(Ljava/lang/CharSequence;)Z
        //   647: ifne            668
        //   650: iload           4
        //   652: iconst_1       
        //   653: iadd           
        //   654: istore          8
        //   656: iload           8
        //   658: istore          4
        //   660: iload           8
        //   662: aload           23
        //   664: arraylength    
        //   665: if_icmplt       625
        //   668: aload           23
        //   670: iload           8
        //   672: aaload         
        //   673: ldc             "%"
        //   675: ldc             ""
        //   677: invokevirtual   java/lang/String.replace:(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;
        //   680: invokestatic    java/lang/Float.valueOf:(Ljava/lang/String;)Ljava/lang/Float;
        //   683: invokevirtual   java/lang/Float.floatValue:()F
        //   686: fstore_3       
        //   687: new             Ljava/lang/StringBuilder;
        //   690: astore          14
        //   692: aload           14
        //   694: invokespecial   java/lang/StringBuilder.<init>:()V
        //   697: aload           14
        //   699: aload           13
        //   701: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   704: pop            
        //   705: aload           14
        //   707: ldc             "total"
        //   709: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   712: pop            
        //   713: aload           14
        //   715: invokevirtual   java/lang/StringBuilder.toString:()Ljava/lang/String;
        //   718: astore          14
        //   720: aload_1        
        //   721: aload           25
        //   723: if_acmpne       729
        //   726: goto            736
        //   729: fload_3        
        //   730: invokestatic    com/apm/insight/l/d.e:()I
        //   733: i2f            
        //   734: fdiv           
        //   735: fstore_3       
        //   736: aload_1        
        //   737: aload           14
        //   739: fload_3        
        //   740: invokestatic    java/lang/Float.valueOf:(F)Ljava/lang/Float;
        //   743: invokevirtual   java/util/HashMap.put:(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
        //   746: pop            
        //   747: goto            793
        //   750: astore          14
        //   752: new             Ljava/lang/StringBuilder;
        //   755: dup            
        //   756: invokespecial   java/lang/StringBuilder.<init>:()V
        //   759: astore          14
        //   761: aload           14
        //   763: aload           13
        //   765: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   768: pop            
        //   769: aload           14
        //   771: ldc             "total"
        //   773: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   776: pop            
        //   777: aload_1        
        //   778: aload           14
        //   780: invokevirtual   java/lang/StringBuilder.toString:()Ljava/lang/String;
        //   783: ldc_w           -1.0
        //   786: invokestatic    java/lang/Float.valueOf:(F)Ljava/lang/Float;
        //   789: invokevirtual   java/util/HashMap.put:(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
        //   792: pop            
        //   793: iinc            8, 3
        //   796: iconst_0       
        //   797: istore          4
        //   799: aload           11
        //   801: astore          19
        //   803: aload           12
        //   805: astore          20
        //   807: aload           15
        //   809: astore          14
        //   811: iload           8
        //   813: aload           23
        //   815: arraylength    
        //   816: if_icmpge       1512
        //   819: ldc_w           "softirq"
        //   822: astore          14
        //   824: iload           4
        //   826: ifeq            874
        //   829: iload           4
        //   831: iconst_1       
        //   832: if_icmpeq       871
        //   835: iload           4
        //   837: iconst_2       
        //   838: if_icmpeq       868
        //   841: iload           4
        //   843: iconst_3       
        //   844: if_icmpeq       865
        //   847: iload           4
        //   849: iconst_4       
        //   850: if_icmpeq       862
        //   853: iload           4
        //   855: iconst_5       
        //   856: if_icmpeq       994
        //   859: goto            1015
        //   862: goto            974
        //   865: goto            949
        //   868: goto            924
        //   871: goto            899
        //   874: ldc_w           "user"
        //   877: aload           23
        //   879: iload           8
        //   881: aaload         
        //   882: invokevirtual   java/lang/String.equalsIgnoreCase:(Ljava/lang/String;)Z
        //   885: ifeq            899
        //   888: ldc_w           "user"
        //   891: astore          14
        //   893: iconst_1       
        //   894: istore          4
        //   896: goto            1018
        //   899: ldc_w           "kernel"
        //   902: aload           23
        //   904: iload           8
        //   906: aaload         
        //   907: invokevirtual   java/lang/String.equalsIgnoreCase:(Ljava/lang/String;)Z
        //   910: ifeq            924
        //   913: ldc_w           "kernel"
        //   916: astore          14
        //   918: iconst_2       
        //   919: istore          4
        //   921: goto            1018
        //   924: ldc_w           "iowait"
        //   927: aload           23
        //   929: iload           8
        //   931: aaload         
        //   932: invokevirtual   java/lang/String.equalsIgnoreCase:(Ljava/lang/String;)Z
        //   935: ifeq            949
        //   938: ldc_w           "iowait"
        //   941: astore          14
        //   943: iconst_3       
        //   944: istore          4
        //   946: goto            1018
        //   949: ldc_w           "irq"
        //   952: aload           23
        //   954: iload           8
        //   956: aaload         
        //   957: invokevirtual   java/lang/String.equalsIgnoreCase:(Ljava/lang/String;)Z
        //   960: ifeq            974
        //   963: ldc_w           "irq"
        //   966: astore          14
        //   968: iconst_4       
        //   969: istore          4
        //   971: goto            1018
        //   974: ldc_w           "softirq"
        //   977: aload           23
        //   979: iload           8
        //   981: aaload         
        //   982: invokevirtual   java/lang/String.equalsIgnoreCase:(Ljava/lang/String;)Z
        //   985: ifeq            994
        //   988: iconst_5       
        //   989: istore          4
        //   991: goto            1018
        //   994: ldc_w           "softirq"
        //   997: aload           23
        //   999: iload           8
        //  1001: aaload         
        //  1002: invokevirtual   java/lang/String.equalsIgnoreCase:(Ljava/lang/String;)Z
        //  1005: ifeq            1015
        //  1008: bipush          6
        //  1010: istore          4
        //  1012: goto            1018
        //  1015: aconst_null    
        //  1016: astore          14
        //  1018: aload           14
        //  1020: ifnull          1153
        //  1023: aload           23
        //  1025: iload           8
        //  1027: iconst_1       
        //  1028: isub           
        //  1029: aaload         
        //  1030: aload           22
        //  1032: ldc             ""
        //  1034: invokevirtual   java/lang/String.replace:(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;
        //  1037: invokestatic    java/lang/Float.valueOf:(Ljava/lang/String;)Ljava/lang/Float;
        //  1040: invokevirtual   java/lang/Float.floatValue:()F
        //  1043: fstore_3       
        //  1044: new             Ljava/lang/StringBuilder;
        //  1047: astore          19
        //  1049: aload           19
        //  1051: invokespecial   java/lang/StringBuilder.<init>:()V
        //  1054: aload           19
        //  1056: aload           13
        //  1058: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //  1061: pop            
        //  1062: aload           19
        //  1064: aload           14
        //  1066: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //  1069: pop            
        //  1070: aload           19
        //  1072: invokevirtual   java/lang/StringBuilder.toString:()Ljava/lang/String;
        //  1075: astore          19
        //  1077: aload_1        
        //  1078: aload           25
        //  1080: if_acmpne       1086
        //  1083: goto            1093
        //  1086: fload_3        
        //  1087: invokestatic    com/apm/insight/l/d.e:()I
        //  1090: i2f            
        //  1091: fdiv           
        //  1092: fstore_3       
        //  1093: aload_1        
        //  1094: aload           19
        //  1096: fload_3        
        //  1097: invokestatic    java/lang/Float.valueOf:(F)Ljava/lang/Float;
        //  1100: invokevirtual   java/util/HashMap.put:(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
        //  1103: pop            
        //  1104: goto            1153
        //  1107: astore          19
        //  1109: new             Ljava/lang/StringBuilder;
        //  1112: dup            
        //  1113: invokespecial   java/lang/StringBuilder.<init>:()V
        //  1116: astore          19
        //  1118: aload           19
        //  1120: aload           13
        //  1122: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //  1125: pop            
        //  1126: aload           19
        //  1128: aload           14
        //  1130: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //  1133: pop            
        //  1134: aload_1        
        //  1135: aload           19
        //  1137: invokevirtual   java/lang/StringBuilder.toString:()Ljava/lang/String;
        //  1140: ldc_w           -1.0
        //  1143: invokestatic    java/lang/Float.valueOf:(F)Ljava/lang/Float;
        //  1146: invokevirtual   java/util/HashMap.put:(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
        //  1149: pop            
        //  1150: goto            1153
        //  1153: iload           4
        //  1155: bipush          6
        //  1157: if_icmplt       1175
        //  1160: aload           11
        //  1162: astore          19
        //  1164: aload           12
        //  1166: astore          20
        //  1168: aload           15
        //  1170: astore          14
        //  1172: goto            1512
        //  1175: iinc            8, 3
        //  1178: goto            799
        //  1181: aload           12
        //  1183: astore          13
        //  1185: goto            1412
        //  1188: aload_1        
        //  1189: astore          15
        //  1191: aload           19
        //  1193: invokevirtual   java/lang/String.trim:()Ljava/lang/String;
        //  1196: astore          19
        //  1198: aload           19
        //  1200: invokevirtual   java/lang/String.toLowerCase:()Ljava/lang/String;
        //  1203: astore          13
        //  1205: aload           13
        //  1207: ldc_w           "shortmsg"
        //  1210: invokevirtual   java/lang/String.startsWith:(Ljava/lang/String;)Z
        //  1213: ifeq            1235
        //  1216: aload           19
        //  1218: aload           19
        //  1220: bipush          58
        //  1222: invokevirtual   java/lang/String.indexOf:(I)I
        //  1225: invokevirtual   java/lang/String.substring:(I)Ljava/lang/String;
        //  1228: pop            
        //  1229: iconst_0       
        //  1230: istore          4
        //  1232: goto            1262
        //  1235: aload           13
        //  1237: ldc_w           "reason:"
        //  1240: invokevirtual   java/lang/String.startsWith:(Ljava/lang/String;)Z
        //  1243: ifeq            1530
        //  1246: aload           19
        //  1248: aload           19
        //  1250: bipush          58
        //  1252: invokevirtual   java/lang/String.indexOf:(I)I
        //  1255: invokevirtual   java/lang/String.substring:(I)Ljava/lang/String;
        //  1258: pop            
        //  1259: iconst_1       
        //  1260: istore          4
        //  1262: aload           13
        //  1264: ldc_w           "input dispatch"
        //  1267: invokevirtual   java/lang/String.contains:(Ljava/lang/CharSequence;)Z
        //  1270: ifeq            1285
        //  1273: ldc_w           "Input dispatching timed out"
        //  1276: astore          12
        //  1278: aload           11
        //  1280: astore          14
        //  1282: goto            1386
        //  1285: aload           13
        //  1287: ldc_w           "broadcast of intent"
        //  1290: invokevirtual   java/lang/String.contains:(Ljava/lang/CharSequence;)Z
        //  1293: ifeq            1304
        //  1296: ldc_w           "Broadcast of Intent"
        //  1299: astore          12
        //  1301: goto            1278
        //  1304: aload           13
        //  1306: ldc_w           "executing service"
        //  1309: invokevirtual   java/lang/String.contains:(Ljava/lang/CharSequence;)Z
        //  1312: ifeq            1359
        //  1315: aload           11
        //  1317: astore          14
        //  1319: ldc_w           "null"
        //  1322: aload           11
        //  1324: invokevirtual   java/lang/String.equalsIgnoreCase:(Ljava/lang/String;)Z
        //  1327: ifeq            1351
        //  1330: aload           19
        //  1332: aload           19
        //  1334: ldc_w           "service "
        //  1337: invokevirtual   java/lang/String.indexOf:(Ljava/lang/String;)I
        //  1340: bipush          8
        //  1342: iadd           
        //  1343: invokevirtual   java/lang/String.substring:(I)Ljava/lang/String;
        //  1346: invokevirtual   java/lang/String.trim:()Ljava/lang/String;
        //  1349: astore          14
        //  1351: ldc_w           "executing service"
        //  1354: astore          12
        //  1356: goto            1386
        //  1359: aload           13
        //  1361: ldc_w           "service.startforeground"
        //  1364: invokevirtual   java/lang/String.contains:(Ljava/lang/CharSequence;)Z
        //  1367: ifeq            1378
        //  1370: ldc_w           "not call Service.startForeground"
        //  1373: astore          12
        //  1375: goto            1278
        //  1378: aload           18
        //  1380: astore          12
        //  1382: aload           11
        //  1384: astore          14
        //  1386: aload           14
        //  1388: astore          11
        //  1390: aload           12
        //  1392: astore          13
        //  1394: iload           4
        //  1396: ifeq            1412
        //  1399: iconst_2       
        //  1400: istore          4
        //  1402: aload           14
        //  1404: astore          11
        //  1406: aload           15
        //  1408: astore_1       
        //  1409: goto            1620
        //  1412: aload           19
        //  1414: invokevirtual   java/lang/String.trim:()Ljava/lang/String;
        //  1417: astore          12
        //  1419: aload           11
        //  1421: astore          19
        //  1423: aload           13
        //  1425: astore          20
        //  1427: aload_1        
        //  1428: astore          14
        //  1430: aload           12
        //  1432: ldc_w           "Load:"
        //  1435: invokevirtual   java/lang/String.startsWith:(Ljava/lang/String;)Z
        //  1438: ifeq            1512
        //  1441: aload           12
        //  1443: ldc_w           "Load:"
        //  1446: ldc             ""
        //  1448: invokevirtual   java/lang/String.replace:(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;
        //  1451: invokevirtual   java/lang/String.trim:()Ljava/lang/String;
        //  1454: ldc_w           "/"
        //  1457: invokevirtual   java/lang/String.split:(Ljava/lang/String;)[Ljava/lang/String;
        //  1460: astore          12
        //  1462: iconst_3       
        //  1463: aload           12
        //  1465: arraylength    
        //  1466: if_icmpne       1502
        //  1469: iconst_0       
        //  1470: istore          4
        //  1472: iload           4
        //  1474: aload           12
        //  1476: arraylength    
        //  1477: if_icmpge       1502
        //  1480: aload           26
        //  1482: iload           4
        //  1484: aload           12
        //  1486: iload           4
        //  1488: aaload         
        //  1489: invokestatic    java/lang/Float.valueOf:(Ljava/lang/String;)Ljava/lang/Float;
        //  1492: invokevirtual   java/lang/Float.floatValue:()F
        //  1495: fastore        
        //  1496: iinc            4, 1
        //  1499: goto            1472
        //  1502: iconst_3       
        //  1503: istore          4
        //  1505: aload           13
        //  1507: astore          12
        //  1509: goto            1620
        //  1512: aload           14
        //  1514: astore_1       
        //  1515: iload           5
        //  1517: istore          4
        //  1519: aload           19
        //  1521: astore          11
        //  1523: aload           20
        //  1525: astore          12
        //  1527: goto            1620
        //  1530: aload           11
        //  1532: astore          19
        //  1534: aload           12
        //  1536: astore          20
        //  1538: aload           15
        //  1540: astore_1       
        //  1541: aload           13
        //  1543: ldc_w           "appfreeze"
        //  1546: invokevirtual   java/lang/String.contains:(Ljava/lang/CharSequence;)Z
        //  1549: ifeq            1515
        //  1552: ldc_w           "AppFreeze"
        //  1555: astore          12
        //  1557: bipush          10
        //  1559: istore          4
        //  1561: aload           15
        //  1563: astore_1       
        //  1564: goto            1620
        //  1567: aload_1        
        //  1568: astore          13
        //  1570: aload           19
        //  1572: invokevirtual   java/lang/String.trim:()Ljava/lang/String;
        //  1575: astore          14
        //  1577: aload           11
        //  1579: astore          19
        //  1581: aload           12
        //  1583: astore          20
        //  1585: aload           13
        //  1587: astore_1       
        //  1588: aload           14
        //  1590: ldc_w           "tag:"
        //  1593: invokevirtual   java/lang/String.startsWith:(Ljava/lang/String;)Z
        //  1596: ifeq            1515
        //  1599: aload           14
        //  1601: ldc_w           "tag:"
        //  1604: ldc             ""
        //  1606: invokevirtual   java/lang/String.replace:(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;
        //  1609: invokevirtual   java/lang/String.trim:()Ljava/lang/String;
        //  1612: astore          11
        //  1614: iconst_1       
        //  1615: istore          4
        //  1617: aload           13
        //  1619: astore_1       
        //  1620: iload           4
        //  1622: istore          5
        //  1624: aload           11
        //  1626: astore          14
        //  1628: aload           12
        //  1630: astore          15
        //  1632: iload           9
        //  1634: istore          10
        //  1636: aload_1        
        //  1637: astore          13
        //  1639: iload           4
        //  1641: iconst_4       
        //  1642: if_icmplt       1648
        //  1645: goto            1669
        //  1648: iinc            6, 1
        //  1651: aload           13
        //  1653: astore_1       
        //  1654: aload           14
        //  1656: astore          11
        //  1658: aload           15
        //  1660: astore          12
        //  1662: iload           10
        //  1664: istore          9
        //  1666: goto            87
        //  1669: aload_2        
        //  1670: ldc_w           "anr_tag"
        //  1673: aload           11
        //  1675: invokevirtual   org/json/JSONObject.put:(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
        //  1678: pop            
        //  1679: aload_2        
        //  1680: ldc_w           "anr_has_ago"
        //  1683: iload           9
        //  1685: invokestatic    java/lang/String.valueOf:(Z)Ljava/lang/String;
        //  1688: invokevirtual   org/json/JSONObject.put:(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
        //  1691: pop            
        //  1692: aload_2        
        //  1693: ldc_w           "anr_reason"
        //  1696: aload           12
        //  1698: invokevirtual   org/json/JSONObject.put:(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
        //  1701: pop            
        //  1702: aload_1        
        //  1703: aload_2        
        //  1704: ldc_w           "app"
        //  1707: invokestatic    com/apm/insight/b/b.a:(Ljava/util/HashMap;Lorg/json/JSONObject;Ljava/lang/String;)V
        //  1710: aload           25
        //  1712: aload_2        
        //  1713: ldc             "total"
        //  1715: invokestatic    com/apm/insight/b/b.a:(Ljava/util/HashMap;Lorg/json/JSONObject;Ljava/lang/String;)V
        //  1718: aload           24
        //  1720: invokevirtual   java/util/HashMap.isEmpty:()Z
        //  1723: ifeq            1740
        //  1726: aload_2        
        //  1727: ldc_w           "npth_anr_systemserver_total"
        //  1730: ldc_w           "not found"
        //  1733: invokevirtual   org/json/JSONObject.put:(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
        //  1736: pop            
        //  1737: goto            1759
        //  1740: aload_2        
        //  1741: ldc_w           "npth_anr_systemserver_total"
        //  1744: aload           24
        //  1746: invokestatic    com/apm/insight/l/r.a:(Ljava/util/Map;)Ljava/lang/Float;
        //  1749: invokevirtual   java/lang/Float.floatValue:()F
        //  1752: invokestatic    com/apm/insight/b/b.b:(F)Ljava/lang/String;
        //  1755: invokevirtual   org/json/JSONObject.put:(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
        //  1758: pop            
        //  1759: aload           16
        //  1761: invokevirtual   java/util/HashMap.isEmpty:()Z
        //  1764: ifeq            1781
        //  1767: aload_2        
        //  1768: ldc_w           "npth_anr_kswapd_total"
        //  1771: ldc_w           "not found"
        //  1774: invokevirtual   org/json/JSONObject.put:(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
        //  1777: pop            
        //  1778: goto            1800
        //  1781: aload_2        
        //  1782: ldc_w           "npth_anr_kswapd_total"
        //  1785: aload           16
        //  1787: invokestatic    com/apm/insight/l/r.a:(Ljava/util/Map;)Ljava/lang/Float;
        //  1790: invokevirtual   java/lang/Float.floatValue:()F
        //  1793: invokestatic    com/apm/insight/b/b.b:(F)Ljava/lang/String;
        //  1796: invokevirtual   org/json/JSONObject.put:(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
        //  1799: pop            
        //  1800: aload           17
        //  1802: invokevirtual   java/util/HashMap.isEmpty:()Z
        //  1805: ifeq            1822
        //  1808: aload_2        
        //  1809: ldc_w           "npth_anr_dex2oat_total"
        //  1812: ldc_w           "not found"
        //  1815: invokevirtual   org/json/JSONObject.put:(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
        //  1818: pop            
        //  1819: goto            1841
        //  1822: aload_2        
        //  1823: ldc_w           "npth_anr_dex2oat_total"
        //  1826: aload           17
        //  1828: invokestatic    com/apm/insight/l/r.a:(Ljava/util/Map;)Ljava/lang/Float;
        //  1831: invokevirtual   java/lang/Float.floatValue:()F
        //  1834: invokestatic    com/apm/insight/b/b.b:(F)Ljava/lang/String;
        //  1837: invokevirtual   org/json/JSONObject.put:(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
        //  1840: pop            
        //  1841: return         
        //  1842: astore          14
        //  1844: goto            752
        //  1847: astore          14
        //  1849: goto            752
        //  1852: astore          19
        //  1854: goto            1109
        //  1857: astore          19
        //  1859: goto            1109
        //    Exceptions:
        //  Try           Handler
        //  Start  End    Start  End    Type
        //  -----  -----  -----  -----  ----
        //  668    687    750    752    Any
        //  687    720    1842   1847   Any
        //  729    736    1847   1852   Any
        //  736    747    1847   1852   Any
        //  1023   1044   1107   1109   Any
        //  1044   1077   1852   1857   Any
        //  1086   1093   1857   1862   Any
        //  1093   1104   1857   1862   Any
        // 
        // The error that occurred was:
        // 
        // java.lang.IndexOutOfBoundsException: Index 857 out of bounds for length 857
        //     at java.base/jdk.internal.util.Preconditions.outOfBounds(Preconditions.java:64)
        //     at java.base/jdk.internal.util.Preconditions.outOfBoundsCheckIndex(Preconditions.java:70)
        //     at java.base/jdk.internal.util.Preconditions.checkIndex(Preconditions.java:266)
        //     at java.base/java.util.Objects.checkIndex(Objects.java:361)
        //     at java.base/java.util.ArrayList.get(ArrayList.java:427)
        //     at com.strobel.decompiler.ast.AstBuilder.convertToAst(AstBuilder.java:3362)
        //     at com.strobel.decompiler.ast.AstBuilder.build(AstBuilder.java:112)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:203)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:93)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethodBody(AstBuilder.java:868)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethod(AstBuilder.java:761)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addTypeMembers(AstBuilder.java:638)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeCore(AstBuilder.java:605)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeNoCache(AstBuilder.java:195)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createType(AstBuilder.java:162)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addType(AstBuilder.java:137)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.buildAst(JavaLanguage.java:71)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.decompileType(JavaLanguage.java:59)
        //     at com.strobel.decompiler.DecompilerDriver.decompileType(DecompilerDriver.java:333)
        //     at com.strobel.decompiler.DecompilerDriver.decompileJar(DecompilerDriver.java:254)
        //     at com.strobel.decompiler.DecompilerDriver.main(DecompilerDriver.java:144)
        // 
        throw new IllegalStateException("An error occurred while decompiling this method.");
    }
    
    private static void a(final HashMap<String, Float> hashMap, final JSONObject jsonObject, String str) {
        final StringBuilder sb = new StringBuilder();
        sb.append("npth_anr_");
        sb.append(str);
        str = sb.toString();
        String s;
        if (hashMap.isEmpty()) {
            final StringBuilder sb2 = new StringBuilder();
            sb2.append(str);
            sb2.append("_total");
            s = sb2.toString();
            str = "not found";
        }
        else {
            final Iterator<Map.Entry<String, Float>> iterator = hashMap.entrySet().iterator();
            float n = 0.0f;
            float n2 = 0.0f;
            float n3 = 0.0f;
            float n4 = 0.0f;
            float n5 = 0.0f;
            while (iterator.hasNext()) {
                final Map.Entry<String, V> entry = (Map.Entry<String, V>)iterator.next();
                final String s2 = entry.getKey();
                if (s2.endsWith("user")) {
                    n3 += (float)entry.getValue();
                }
                else if (s2.endsWith("kernel")) {
                    n2 += (float)entry.getValue();
                }
                else if (s2.endsWith("iowait")) {
                    n += (float)entry.getValue();
                }
                else if (s2.endsWith("irq")) {
                    n4 += (float)entry.getValue();
                }
                else {
                    if (!s2.endsWith("softirq")) {
                        continue;
                    }
                    n5 += (float)entry.getValue();
                }
            }
            final float n6 = n3 + n2 + n + n4 + n5;
            final StringBuilder sb3 = new StringBuilder();
            sb3.append(str);
            sb3.append("_total");
            jsonObject.put(sb3.toString(), (Object)b(n6));
            final StringBuilder sb4 = new StringBuilder();
            sb4.append(str);
            sb4.append("_kernel_user_ratio");
            jsonObject.put(sb4.toString(), (Object)a(n2, n6));
            final StringBuilder sb5 = new StringBuilder();
            sb5.append(str);
            sb5.append("_iowait_user_ratio");
            s = sb5.toString();
            str = a(n, n6);
        }
        jsonObject.put(s, (Object)str);
    }
    
    private void a(final JSONArray jsonArray) {
        if (jsonArray == null) {
            return;
        }
        this.j = null;
        this.q = null;
        this.r = 0;
        final JSONArray jsonArray2 = new JSONArray();
        final JSONArray p = new JSONArray();
        JSONArray jsonArray3 = new JSONArray();
        this.l = "unknown";
        this.m = "unknown";
        this.n = "unknown";
        final int[] array2;
        final int[] array = array2 = new int[3];
        array2[0] = 0;
        array2[2] = (array2[1] = 0);
        String s = null;
        int i = 0;
        int n = 0;
        while (i < jsonArray.length()) {
            final String optString = jsonArray.optString(i);
            Label_0838: {
                if (TextUtils.isEmpty((CharSequence)optString)) {
                    if (jsonArray3.length() > 0 && !TextUtils.isEmpty((CharSequence)s)) {
                        if (this.j == null && "main".equals(s)) {
                            this.j = this.c(jsonArray3);
                        }
                        else {
                            jsonArray2.put((Object)this.a(s, jsonArray3));
                        }
                        String n2;
                        try {
                            if (!"main".equals(s)) {
                                s.substring(0, s.indexOf(40)).trim();
                            }
                        }
                        finally {
                            n2 = s;
                        }
                        if (!this.a(n2)) {
                            final Throwable t;
                            Label_0259: {
                                try {
                                    this.b(jsonArray3);
                                    break Label_0259;
                                }
                                catch (final IllegalArgumentException ex) {
                                    com.apm.insight.c.a().a("NPTH_CATCH", ex);
                                }
                                t = null;
                            }
                            if (t != null) {
                                final int n3 = t[0];
                                if (n3 > array[0]) {
                                    array[0] = n3;
                                    this.l = n2;
                                }
                                final int n4 = t[1];
                                if (n4 > array[1]) {
                                    array[1] = n4;
                                    this.m = n2;
                                }
                                final int n5 = t[2];
                                if (n5 > array[2]) {
                                    array[2] = n5;
                                    this.n = n2;
                                }
                            }
                        }
                    }
                    JSONArray jsonArray4 = jsonArray3;
                    if (jsonArray3.length() > 0) {
                        jsonArray4 = new JSONArray();
                    }
                    s = null;
                    jsonArray3 = jsonArray4;
                }
                else {
                    Label_0830: {
                        if (n != 0) {
                            if (n != 1) {
                                break Label_0838;
                            }
                            JSONArray jsonArray5;
                            String s4;
                            if (optString.contains(" prio=")) {
                                if (jsonArray3.length() > 0 && !TextUtils.isEmpty((CharSequence)s)) {
                                    if (this.j == null && "main".equals(s)) {
                                        this.j = this.c(jsonArray3);
                                    }
                                    else {
                                        jsonArray2.put((Object)this.a(s, jsonArray3));
                                    }
                                    String n6;
                                    try {
                                        if (!"main".equals(s)) {
                                            s.substring(0, s.indexOf(40)).trim();
                                        }
                                    }
                                    finally {
                                        n6 = s;
                                    }
                                    if (!this.a(n6)) {
                                        final Throwable t2;
                                        Label_0546: {
                                            try {
                                                this.b(jsonArray3);
                                                break Label_0546;
                                            }
                                            catch (final IllegalArgumentException ex2) {
                                                com.apm.insight.c.a().a("NPTH_CATCH", ex2);
                                            }
                                            t2 = null;
                                        }
                                        if (t2 != null) {
                                            final int n7 = t2[0];
                                            if (n7 > array[0]) {
                                                array[0] = n7;
                                                this.l = n6;
                                            }
                                            final int n8 = t2[1];
                                            if (n8 > array[1]) {
                                                array[1] = n8;
                                                this.m = n6;
                                            }
                                            final int n9 = t2[2];
                                            if (n9 > array[2]) {
                                                array[2] = n9;
                                                this.n = n6;
                                            }
                                        }
                                    }
                                }
                                String s3;
                                final String s2 = s3 = optString.substring(1, optString.indexOf(34, 1));
                                if (!"main".equals(s2)) {
                                    final StringBuilder sb = new StringBuilder();
                                    sb.append(s2);
                                    sb.append("  (");
                                    sb.append(optString.substring(optString.indexOf(34, 2) + 1));
                                    sb.append(" )");
                                    s3 = sb.toString();
                                }
                                jsonArray5 = jsonArray3;
                                if (jsonArray3.length() > 0) {
                                    jsonArray5 = new JSONArray();
                                }
                                s4 = s3;
                            }
                            else {
                                s4 = s;
                                jsonArray5 = jsonArray3;
                                if (TextUtils.isEmpty((CharSequence)s)) {
                                    break Label_0830;
                                }
                            }
                            jsonArray5.put((Object)optString);
                            s = s4;
                            jsonArray3 = jsonArray5;
                            break Label_0838;
                        }
                        else if (optString.startsWith("DALVIK THREADS") || optString.startsWith("suspend") || optString.startsWith("\"")) {
                            n = 1;
                        }
                    }
                    p.put((Object)optString);
                }
            }
            ++i;
        }
        if (jsonArray2.length() > 0) {
            this.p = p;
            try {
                (this.q = new JSONObject()).put("thread_all_count", jsonArray2.length());
                this.q.put("thread_stacks", (Object)jsonArray2);
            }
            catch (final JSONException ex3) {
                ((Throwable)ex3).printStackTrace();
            }
        }
    }
    
    private boolean a(final long n) {
        if (this.y) {
            this.y = false;
            this.b(n);
        }
        return false;
    }
    
    private boolean a(final String input) {
    Label_0037_Outer:
        while (true) {
            if (this.D != null) {
                break Label_0150;
            }
            Object d = com.apm.insight.runtime.a.c();
            Label_0069: {
                if (d == null) {
                    break Label_0069;
                }
                this.D = new LinkedList<Pattern>();
                this.o = ((JSONArray)d).optString(0);
                int n = 1;
            Label_0063_Outer:
                while (true) {
                    if (n >= ((JSONArray)d).length()) {
                        break Label_0069;
                    }
                    while (true) {
                        try {
                            this.D.add(Pattern.compile(((JSONArray)d).optString(n)));
                            ++n;
                            continue Label_0063_Outer;
                            d = this.D.iterator();
                        Block_6_Outer:
                            while (true) {
                                while (true) {
                                    Label_0160: {
                                        break Label_0160;
                                        d = new LinkedList<Pattern>();
                                        (this.D = (List<Pattern>)d).add(Pattern.compile("^main$"));
                                        this.D.add(Pattern.compile("^default_npth_thread$"));
                                        this.D.add(Pattern.compile("^RenderThread$"));
                                        this.D.add(Pattern.compile("^Jit thread pool worker thread.*$"));
                                        continue Label_0037_Outer;
                                        iftrue(Label_0160:)(!((Iterator<Pattern>)d).next().matcher(input).matches());
                                        return true;
                                        Label_0190: {
                                            return false;
                                        }
                                    }
                                    iftrue(Label_0190:)(!((Iterator)d).hasNext());
                                    continue;
                                }
                                iftrue(Label_0150:)(this.D != null);
                                continue Block_6_Outer;
                            }
                        }
                        finally {
                            continue;
                        }
                        break;
                    }
                    break;
                }
            }
            break;
        }
    }
    
    private static String b(final float n) {
        return a(n / 100.0f);
    }
    
    private void b(final long n) {
        Label_0338: {
            if (this.A == this.z) {
                break Label_0338;
            }
            try {
                this.s = System.currentTimeMillis();
                this.u = com.apm.insight.b.g.b().c();
                this.t = com.apm.insight.b.k.a(100, n);
                this.k = com.apm.insight.b.g.b().a(n).a();
                final JSONObject v = new JSONObject();
                this.v = v;
                com.apm.insight.l.a.a(this.d, v);
                this.w = this.g();
                this.i = (Npth.hasCrash() ^ true);
                try {
                    this.g = this.s;
                    final String b = com.apm.insight.l.o.b();
                    final File parent = new File(com.apm.insight.l.o.f(this.d), b);
                    final StringBuilder sb = new StringBuilder();
                    sb.append("trace_");
                    sb.append(com.apm.insight.l.a.c(this.d).replace(':', '_'));
                    sb.append(".txt");
                    final File file = new File(parent, sb.toString());
                    file.getParentFile().mkdirs();
                    final StringBuilder sb2 = new StringBuilder();
                    sb2.append(com.apm.insight.l.b.a().format(new Date(System.currentTimeMillis())));
                    sb2.append("\n");
                    com.apm.insight.l.i.a(file, sb2.toString(), false);
                    com.apm.insight.runtime.r.a("anr_trace", b);
                    NativeImpl.i(file.getAbsolutePath());
                    try {
                        this.a(this.p = com.apm.insight.l.i.b(file.getAbsolutePath()));
                    }
                    catch (final IOException ex) {}
                    finally {
                        final Throwable t;
                        com.apm.insight.c.a().a("NPTH_CATCH", t);
                    }
                }
                finally {}
                try {
                    this.g = this.s;
                    final String b2 = com.apm.insight.l.o.b();
                    final File parent2 = new File(com.apm.insight.l.o.f(this.d), b2);
                    final StringBuilder sb3 = new StringBuilder();
                    sb3.append("trace");
                    sb3.append(com.apm.insight.l.a.c(this.d).replace(':', '_'));
                    sb3.append(".txt");
                    final File file2 = new File(parent2, sb3.toString());
                    file2.getParentFile().mkdirs();
                    final StringBuilder sb4 = new StringBuilder();
                    sb4.append(com.apm.insight.l.b.a().format(new Date(System.currentTimeMillis())));
                    sb4.append("\n");
                    com.apm.insight.l.i.a(file2, sb4.toString(), false);
                    com.apm.insight.runtime.r.a("anr_trace", b2);
                    NativeImpl.i(file2.getAbsolutePath());
                    try {
                        this.a(this.p = com.apm.insight.l.i.b(file2.getAbsolutePath()));
                    }
                    catch (final IOException ex2) {}
                    finally {
                        final Throwable t2;
                        com.apm.insight.c.a().a("NPTH_CATCH", t2);
                    }
                }
                finally {}
            }
            finally {}
        }
    }
    
    private static void b(final String s, final JSONArray jsonArray) {
        for (final ICrashCallback crashCallback : com.apm.insight.runtime.o.a().e()) {
            try {
                if (crashCallback instanceof com.apm.insight.b) {
                    ((com.apm.insight.b)crashCallback).a(CrashType.ANR, s, null, jsonArray);
                }
                else {
                    crashCallback.onCrash(CrashType.ANR, s, null);
                }
            }
            finally {
                final Throwable t;
                com.apm.insight.c.a().a("NPTH_CATCH", t);
            }
        }
    }
    
    @Nullable
    private int[] b(@NonNull final JSONArray jsonArray) {
        for (int i = 0; i < jsonArray.length(); ++i) {
            final String optString = jsonArray.optString(i);
            int index;
            if (optString != null && !optString.isEmpty()) {
                index = optString.indexOf("utm=");
            }
            else {
                index = -1;
            }
            if (index > 0) {
                if (this.b == null) {
                    this.b = Pattern.compile("[^0-9]+");
                }
                final String[] split = this.b.split(optString.substring(index));
                if (split == null || split.length < 2) {
                    break;
                }
                try {
                    final int intValue = Integer.decode(split[1]);
                    final int intValue2 = Integer.decode(split[2]);
                    return new int[] { intValue, intValue2, intValue + intValue2 };
                }
                finally {
                    final StringBuilder sb = new StringBuilder();
                    sb.append("Err stack line: ");
                    sb.append(optString);
                    throw new IllegalArgumentException(sb.toString());
                }
            }
        }
        return null;
    }
    
    private String c(long n) {
        n -= com.apm.insight.i.j();
        if (n < 30000L) {
            return "0 - 30s";
        }
        if (n < 60000L) {
            return "30s - 1min";
        }
        if (n < 120000L) {
            return "1min - 2min";
        }
        if (n < 300000L) {
            return "2min - 5min";
        }
        if (n < 600000L) {
            return "5min - 10min";
        }
        if (n < 1800000L) {
            return "10min - 30min";
        }
        if (n < 3600000L) {
            return "30min - 1h";
        }
        return "1h - ";
    }
    
    private JSONObject c(@NonNull final JSONArray jsonArray) {
        final JSONObject jsonObject = new JSONObject();
        final JSONArray a = com.apm.insight.l.l.a(256, 128, jsonArray);
        if (a.length() != jsonArray.length()) {
            ++this.r;
        }
        try {
            jsonObject.put("thread_number", 1);
            final StringBuilder sb = new StringBuilder();
            for (int i = 0; i < a.length(); ++i) {
                sb.append(a.getString(i));
                sb.append('\n');
            }
            jsonObject.put("mainStackFromTrace", (Object)sb.toString());
            return jsonObject;
        }
        catch (final JSONException ex) {
            return null;
        }
    }
    
    private boolean g() {
        boolean b2;
        final boolean b = b2 = (((com.apm.insight.l.a.a(this.d) ? 1 : 0) ^ 0x1) != 0x0);
        if (b) {
            b2 = b;
            if (com.apm.insight.runtime.a.b.d().e() <= 2000L) {
                b2 = false;
            }
        }
        return b2;
    }
    
    private File h() {
        if (this.E == null) {
            final File filesDir = this.d.getFilesDir();
            final StringBuilder sb = new StringBuilder();
            sb.append("has_anr_signal_");
            sb.append(com.apm.insight.l.a.c(this.d).replaceAll(":", "_"));
            this.E = new File(filesDir, sb.toString());
        }
        return this.E;
    }
    
    private boolean i() {
        return com.apm.insight.runtime.a.i();
    }
    
    public void a() {
        if (this.e) {
            return;
        }
        this.c = new c(this);
        this.g = com.apm.insight.i.j();
        this.e = true;
    }
    
    boolean a(final int p0, final int p1) {
        // 
        // This method could not be decompiled.
        // 
        // Original Bytecode:
        // 
        //     5: invokestatic    android/os/SystemClock.uptimeMillis:()J
        //     8: lstore          9
        //    10: aload_0        
        //    11: lload           9
        //    13: invokespecial   com/apm/insight/b/b.a:(J)Z
        //    16: istore          5
        //    18: aload_0        
        //    19: getfield        com/apm/insight/b/b.d:Landroid/content/Context;
        //    22: iconst_1       
        //    23: invokestatic    com/apm/insight/b/d.a:(Landroid/content/Context;I)Ljava/lang/String;
        //    26: astore          24
        //    28: invokestatic    java/lang/System.currentTimeMillis:()J
        //    31: lstore          7
        //    33: ldc_w           "normal"
        //    36: astore          21
        //    38: aload           24
        //    40: invokestatic    android/text/TextUtils.isEmpty:(Ljava/lang/CharSequence;)Z
        //    43: istore_3       
        //    44: iconst_0       
        //    45: istore          4
        //    47: iload_3        
        //    48: ifeq            61
        //    51: iload           5
        //    53: ifeq            61
        //    56: iconst_1       
        //    57: istore_3       
        //    58: goto            63
        //    61: iconst_0       
        //    62: istore_3       
        //    63: ldc             "unknown"
        //    65: astore          20
        //    67: ldc             "unknown"
        //    69: astore          19
        //    71: ldc             "unknown"
        //    73: astore          22
        //    75: iload           5
        //    77: ifne            115
        //    80: aload           24
        //    82: invokestatic    android/text/TextUtils.isEmpty:(Ljava/lang/CharSequence;)Z
        //    85: ifne            91
        //    88: goto            115
        //    91: aconst_null    
        //    92: astore          23
        //    94: aconst_null    
        //    95: astore          13
        //    97: aconst_null    
        //    98: astore          12
        //   100: aconst_null    
        //   101: astore          15
        //   103: aconst_null    
        //   104: astore          11
        //   106: aconst_null    
        //   107: astore          14
        //   109: iconst_0       
        //   110: istore          5
        //   112: goto            331
        //   115: aload_0        
        //   116: getfield        com/apm/insight/b/b.x:Ljava/lang/Object;
        //   119: astore          11
        //   121: aload           11
        //   123: monitorenter   
        //   124: iload_3        
        //   125: iconst_1       
        //   126: ixor           
        //   127: istore          4
        //   129: aload           11
        //   131: monitorexit    
        //   132: aload_0        
        //   133: getfield        com/apm/insight/b/b.j:Lorg/json/JSONObject;
        //   136: ifnull          178
        //   139: invokestatic    java/lang/System.currentTimeMillis:()J
        //   142: aload_0        
        //   143: getfield        com/apm/insight/b/b.g:J
        //   146: lsub           
        //   147: ldc2_w          20000
        //   150: lcmp           
        //   151: ifgt            178
        //   154: iload_3        
        //   155: ifeq            166
        //   158: ldc_w           "trace_only"
        //   161: astore          11
        //   163: goto            171
        //   166: ldc_w           "trace_last"
        //   169: astore          11
        //   171: aload           11
        //   173: astore          13
        //   175: goto            205
        //   178: aload           21
        //   180: astore          13
        //   182: aload_0        
        //   183: getfield        com/apm/insight/b/b.y:Z
        //   186: ifeq            199
        //   189: aload_0        
        //   190: iconst_0       
        //   191: putfield        com/apm/insight/b/b.y:Z
        //   194: ldc_w           "trace_after"
        //   197: astore          13
        //   199: aload_0        
        //   200: lload           9
        //   202: invokespecial   com/apm/insight/b/b.b:(J)V
        //   205: aload_0        
        //   206: getfield        com/apm/insight/b/b.j:Lorg/json/JSONObject;
        //   209: astore          15
        //   211: aload_0        
        //   212: getfield        com/apm/insight/b/b.l:Ljava/lang/String;
        //   215: astore          20
        //   217: aload_0        
        //   218: getfield        com/apm/insight/b/b.m:Ljava/lang/String;
        //   221: astore          19
        //   223: aload_0        
        //   224: getfield        com/apm/insight/b/b.n:Ljava/lang/String;
        //   227: astore          22
        //   229: aload_0        
        //   230: getfield        com/apm/insight/b/b.p:Lorg/json/JSONArray;
        //   233: astore          23
        //   235: aload_0        
        //   236: getfield        com/apm/insight/b/b.u:Lorg/json/JSONArray;
        //   239: astore          14
        //   241: aload_0        
        //   242: getfield        com/apm/insight/b/b.t:Lorg/json/JSONArray;
        //   245: astore          12
        //   247: aload_0        
        //   248: getfield        com/apm/insight/b/b.v:Lorg/json/JSONObject;
        //   251: astore          16
        //   253: aload_0        
        //   254: getfield        com/apm/insight/b/b.k:Lorg/json/JSONObject;
        //   257: astore          11
        //   259: aload_0        
        //   260: getfield        com/apm/insight/b/b.w:Z
        //   263: istore          5
        //   265: aload_0        
        //   266: getfield        com/apm/insight/b/b.s:J
        //   269: lstore          7
        //   271: iload_3        
        //   272: ifne            323
        //   275: aload_0        
        //   276: aconst_null    
        //   277: putfield        com/apm/insight/b/b.j:Lorg/json/JSONObject;
        //   280: aload_0        
        //   281: aconst_null    
        //   282: putfield        com/apm/insight/b/b.p:Lorg/json/JSONArray;
        //   285: aload_0        
        //   286: aconst_null    
        //   287: putfield        com/apm/insight/b/b.t:Lorg/json/JSONArray;
        //   290: aload_0        
        //   291: aconst_null    
        //   292: putfield        com/apm/insight/b/b.k:Lorg/json/JSONObject;
        //   295: aload_0        
        //   296: aconst_null    
        //   297: putfield        com/apm/insight/b/b.u:Lorg/json/JSONArray;
        //   300: aload_0        
        //   301: ldc             "unknown"
        //   303: putfield        com/apm/insight/b/b.l:Ljava/lang/String;
        //   306: aload_0        
        //   307: ldc             "unknown"
        //   309: putfield        com/apm/insight/b/b.m:Ljava/lang/String;
        //   312: aload_0        
        //   313: ldc             "unknown"
        //   315: putfield        com/apm/insight/b/b.n:Ljava/lang/String;
        //   318: aload_0        
        //   319: iconst_0       
        //   320: putfield        com/apm/insight/b/b.r:I
        //   323: aload           13
        //   325: astore          21
        //   327: aload           16
        //   329: astore          13
        //   331: iload_3        
        //   332: ifne            477
        //   335: aload           24
        //   337: invokestatic    android/text/TextUtils.isEmpty:(Ljava/lang/CharSequence;)Z
        //   340: ifeq            477
        //   343: aload_0        
        //   344: getfield        com/apm/insight/b/b.j:Lorg/json/JSONObject;
        //   347: ifnull          437
        //   350: invokestatic    java/lang/System.currentTimeMillis:()J
        //   353: aload_0        
        //   354: getfield        com/apm/insight/b/b.g:J
        //   357: lsub           
        //   358: ldc2_w          20000
        //   361: lcmp           
        //   362: ifle            437
        //   365: aload_0        
        //   366: aconst_null    
        //   367: putfield        com/apm/insight/b/b.j:Lorg/json/JSONObject;
        //   370: aload_0        
        //   371: aconst_null    
        //   372: putfield        com/apm/insight/b/b.p:Lorg/json/JSONArray;
        //   375: aload_0        
        //   376: aconst_null    
        //   377: putfield        com/apm/insight/b/b.t:Lorg/json/JSONArray;
        //   380: aload_0        
        //   381: aconst_null    
        //   382: putfield        com/apm/insight/b/b.k:Lorg/json/JSONObject;
        //   385: aload_0        
        //   386: aconst_null    
        //   387: putfield        com/apm/insight/b/b.u:Lorg/json/JSONArray;
        //   390: aload_0        
        //   391: ldc             "unknown"
        //   393: putfield        com/apm/insight/b/b.l:Ljava/lang/String;
        //   396: aload_0        
        //   397: ldc             "unknown"
        //   399: putfield        com/apm/insight/b/b.m:Ljava/lang/String;
        //   402: aload_0        
        //   403: ldc             "unknown"
        //   405: putfield        com/apm/insight/b/b.n:Ljava/lang/String;
        //   408: aload_0        
        //   409: iconst_0       
        //   410: putfield        com/apm/insight/b/b.r:I
        //   413: aload_0        
        //   414: getfield        com/apm/insight/b/b.h:Ljava/io/File;
        //   417: astore          11
        //   419: aload           11
        //   421: ifnull          430
        //   424: aload           11
        //   426: invokestatic    com/apm/insight/l/i.a:(Ljava/io/File;)Z
        //   429: pop            
        //   430: aload_0        
        //   431: aconst_null    
        //   432: putfield        com/apm/insight/b/b.h:Ljava/io/File;
        //   435: iconst_0       
        //   436: ireturn        
        //   437: aload_0        
        //   438: getfield        com/apm/insight/b/b.j:Lorg/json/JSONObject;
        //   441: ifnull          475
        //   444: invokestatic    java/lang/System.currentTimeMillis:()J
        //   447: aload_0        
        //   448: getfield        com/apm/insight/b/b.g:J
        //   451: lsub           
        //   452: ldc2_w          2000
        //   455: lcmp           
        //   456: ifle            475
        //   459: invokestatic    com/apm/insight/nativecrash/NativeImpl.g:()Z
        //   462: ifeq            473
        //   465: aload_0        
        //   466: invokespecial   com/apm/insight/b/b.h:()Ljava/io/File;
        //   469: invokestatic    com/apm/insight/l/i.a:(Ljava/io/File;)Z
        //   472: pop            
        //   473: iconst_0       
        //   474: ireturn        
        //   475: iconst_0       
        //   476: ireturn        
        //   477: aload           15
        //   479: ifnonnull       659
        //   482: aload           12
        //   484: ifnonnull       611
        //   487: invokestatic    com/apm/insight/b/g.b:()Lcom/apm/insight/b/h;
        //   490: invokevirtual   com/apm/insight/b/h.c:()Lorg/json/JSONArray;
        //   493: astore          16
        //   495: aload           11
        //   497: astore          18
        //   499: bipush          100
        //   501: lload           9
        //   503: invokestatic    com/apm/insight/b/k.a:(IJ)Lorg/json/JSONArray;
        //   506: astore          17
        //   508: aload           17
        //   510: astore          12
        //   512: aload           11
        //   514: astore          18
        //   516: invokestatic    com/apm/insight/b/g.b:()Lcom/apm/insight/b/h;
        //   519: lload           9
        //   521: invokevirtual   com/apm/insight/b/h.a:(J)Lcom/apm/insight/b/h$e;
        //   524: invokevirtual   com/apm/insight/b/h$e.a:()Lorg/json/JSONObject;
        //   527: astore          14
        //   529: aload           17
        //   531: astore          12
        //   533: aload           14
        //   535: astore          18
        //   537: new             Lorg/json/JSONObject;
        //   540: astore          11
        //   542: aload           17
        //   544: astore          12
        //   546: aload           14
        //   548: astore          18
        //   550: aload           11
        //   552: invokespecial   org/json/JSONObject.<init>:()V
        //   555: aload_0        
        //   556: getfield        com/apm/insight/b/b.d:Landroid/content/Context;
        //   559: aload           11
        //   561: invokestatic    com/apm/insight/l/a.a:(Landroid/content/Context;Lorg/json/JSONObject;)V
        //   564: aload           11
        //   566: astore          12
        //   568: aload           17
        //   570: astore          11
        //   572: aload           14
        //   574: astore          17
        //   576: aload           16
        //   578: astore          14
        //   580: goto            623
        //   583: astore          12
        //   585: aload           11
        //   587: astore          12
        //   589: aload           17
        //   591: astore          11
        //   593: aload           14
        //   595: astore          17
        //   597: goto            648
        //   600: goto            608
        //   603: astore          16
        //   605: goto            600
        //   608: goto            659
        //   611: aload           11
        //   613: astore          17
        //   615: aload           12
        //   617: astore          11
        //   619: aload           13
        //   621: astore          12
        //   623: getstatic       com/apm/insight/b/b.a:Z
        //   626: invokestatic    com/apm/insight/b/d.a:(Z)Lorg/json/JSONObject;
        //   629: astore          13
        //   631: aload           13
        //   633: astore          15
        //   635: aload           14
        //   637: astore          16
        //   639: goto            648
        //   642: astore          13
        //   644: aload           14
        //   646: astore          16
        //   648: aload           15
        //   650: astore          13
        //   652: aload           17
        //   654: astore          14
        //   656: goto            683
        //   659: aload           11
        //   661: astore          17
        //   663: aload           14
        //   665: astore          16
        //   667: aload           12
        //   669: astore          11
        //   671: aload           13
        //   673: astore          12
        //   675: aload           17
        //   677: astore          14
        //   679: aload           15
        //   681: astore          13
        //   683: aload           13
        //   685: ifnull          2021
        //   688: aload           13
        //   690: invokevirtual   org/json/JSONObject.length:()I
        //   693: ifle            2021
        //   696: aload           13
        //   698: ldc_w           "pid"
        //   701: invokestatic    android/os/Process.myPid:()I
        //   704: invokevirtual   org/json/JSONObject.put:(Ljava/lang/String;I)Lorg/json/JSONObject;
        //   707: pop            
        //   708: aload           13
        //   710: ldc_w           "package"
        //   713: aload_0        
        //   714: getfield        com/apm/insight/b/b.d:Landroid/content/Context;
        //   717: invokevirtual   android/content/Context.getPackageName:()Ljava/lang/String;
        //   720: invokevirtual   org/json/JSONObject.put:(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
        //   723: pop            
        //   724: aload           13
        //   726: ldc_w           "is_remote_process"
        //   729: iconst_0       
        //   730: invokevirtual   org/json/JSONObject.put:(Ljava/lang/String;I)Lorg/json/JSONObject;
        //   733: pop            
        //   734: aload           13
        //   736: ldc_w           "is_new_stack"
        //   739: bipush          10
        //   741: invokevirtual   org/json/JSONObject.put:(Ljava/lang/String;I)Lorg/json/JSONObject;
        //   744: pop            
        //   745: new             Lcom/apm/insight/entity/a;
        //   748: astore          15
        //   750: new             Lorg/json/JSONObject;
        //   753: astore          17
        //   755: aload           17
        //   757: invokespecial   org/json/JSONObject.<init>:()V
        //   760: aload           15
        //   762: aload           17
        //   764: invokespecial   com/apm/insight/entity/a.<init>:(Lorg/json/JSONObject;)V
        //   767: aload           15
        //   769: ldc_w           "data"
        //   772: aload           13
        //   774: invokevirtual   org/json/JSONObject.toString:()Ljava/lang/String;
        //   777: invokevirtual   com/apm/insight/entity/a.a:(Ljava/lang/String;Ljava/lang/Object;)V
        //   780: aload           15
        //   782: ldc_w           "is_anr"
        //   785: iconst_1       
        //   786: invokestatic    java/lang/Integer.valueOf:(I)Ljava/lang/Integer;
        //   789: invokevirtual   com/apm/insight/entity/a.a:(Ljava/lang/String;Ljava/lang/Object;)V
        //   792: aload           15
        //   794: ldc_w           "anrType"
        //   797: aload           21
        //   799: invokevirtual   com/apm/insight/entity/a.a:(Ljava/lang/String;Ljava/lang/Object;)V
        //   802: aload           15
        //   804: ldc_w           "history_message"
        //   807: aload           16
        //   809: invokevirtual   com/apm/insight/entity/a.a:(Ljava/lang/String;Ljava/lang/Object;)V
        //   812: aload           15
        //   814: ldc_w           "current_message"
        //   817: aload           14
        //   819: invokevirtual   com/apm/insight/entity/a.a:(Ljava/lang/String;Ljava/lang/Object;)V
        //   822: aload           15
        //   824: ldc_w           "pending_messages"
        //   827: aload           11
        //   829: invokevirtual   com/apm/insight/entity/a.a:(Ljava/lang/String;Ljava/lang/Object;)V
        //   832: aload           15
        //   834: ldc_w           "anr_time"
        //   837: invokestatic    java/lang/System.currentTimeMillis:()J
        //   840: invokestatic    java/lang/Long.valueOf:(J)Ljava/lang/Long;
        //   843: invokevirtual   com/apm/insight/entity/a.a:(Ljava/lang/String;Ljava/lang/Object;)V
        //   846: aload           15
        //   848: ldc_w           "crash_time"
        //   851: lload           7
        //   853: invokestatic    java/lang/Long.valueOf:(J)Ljava/lang/Long;
        //   856: invokevirtual   com/apm/insight/entity/a.a:(Ljava/lang/String;Ljava/lang/Object;)V
        //   859: invokestatic    com/apm/insight/c/b.b:()Z
        //   862: pop            
        //   863: aload           15
        //   865: aload           12
        //   867: invokevirtual   com/apm/insight/entity/a.c:(Lorg/json/JSONObject;)V
        //   870: iload_3        
        //   871: ifeq            882
        //   874: ldc_w           "Resons for no ANR_INFO:\n1. User click close button too quickly as soon as the ANR dialog appear.\n2. User close the app since can not stand the carton.\n3. Some OS force stop the process group without any hint dialog.\n\nThe ANR will be upload by the follow ways only:\n1. Receive the ANR signal(SIGQUIT).\n2. The app is forground or was forground last 2s.\n3. Happens in main process.\n4. Process was killed exactly."
        //   877: astore          11
        //   879: goto            886
        //   882: aload           24
        //   884: astore          11
        //   886: aload           15
        //   888: ldc_w           "anr_info"
        //   891: aload           11
        //   893: invokevirtual   com/apm/insight/entity/a.a:(Ljava/lang/String;Ljava/lang/Object;)V
        //   896: aload           23
        //   898: ifnull          911
        //   901: aload           15
        //   903: ldc_w           "dump_trace"
        //   906: aload           23
        //   908: invokevirtual   com/apm/insight/entity/a.a:(Ljava/lang/String;Ljava/lang/Object;)V
        //   911: iload_3        
        //   912: ifne            943
        //   915: aload_0        
        //   916: getfield        com/apm/insight/b/b.q:Lorg/json/JSONObject;
        //   919: astore          11
        //   921: aload           11
        //   923: ifnull          934
        //   926: aload           11
        //   928: invokevirtual   org/json/JSONObject.length:()I
        //   931: ifne            943
        //   934: aconst_null    
        //   935: invokestatic    com/apm/insight/l/v.b:(Ljava/lang/String;)Lorg/json/JSONObject;
        //   938: astore          11
        //   940: goto            949
        //   943: aload_0        
        //   944: getfield        com/apm/insight/b/b.q:Lorg/json/JSONObject;
        //   947: astore          11
        //   949: aload           15
        //   951: ldc_w           "all_thread_stacks"
        //   954: aload           11
        //   956: invokevirtual   com/apm/insight/entity/a.a:(Ljava/lang/String;Ljava/lang/Object;)V
        //   959: invokestatic    com/apm/insight/runtime/a/f.a:()Lcom/apm/insight/runtime/a/f;
        //   962: astore          12
        //   964: getstatic       com/apm/insight/CrashType.ANR:Lcom/apm/insight/CrashType;
        //   967: astore          11
        //   969: aload           12
        //   971: aload           11
        //   973: aload           15
        //   975: invokevirtual   com/apm/insight/runtime/a/f.a:(Lcom/apm/insight/CrashType;Lcom/apm/insight/entity/a;)Lcom/apm/insight/entity/a;
        //   978: astore          15
        //   980: aload           15
        //   982: ldc_w           "is_background"
        //   985: iload           5
        //   987: invokestatic    java/lang/Boolean.valueOf:(Z)Ljava/lang/Boolean;
        //   990: invokevirtual   com/apm/insight/entity/a.a:(Ljava/lang/String;Ljava/lang/Object;)V
        //   993: aload           15
        //   995: ldc_w           "logcat"
        //   998: invokestatic    com/apm/insight/i.f:()Ljava/lang/String;
        //  1001: invokestatic    com/apm/insight/runtime/k.b:(Ljava/lang/String;)Lorg/json/JSONArray;
        //  1004: invokevirtual   com/apm/insight/entity/a.a:(Ljava/lang/String;Ljava/lang/Object;)V
        //  1007: aload           15
        //  1009: ldc_w           "has_dump"
        //  1012: ldc_w           "true"
        //  1015: invokevirtual   com/apm/insight/entity/a.a:(Ljava/lang/String;Ljava/lang/Object;)V
        //  1018: aload           15
        //  1020: ldc_w           "crash_uuid"
        //  1023: lload           7
        //  1025: aload           11
        //  1027: iconst_0       
        //  1028: iconst_0       
        //  1029: invokestatic    com/apm/insight/i.a:(JLcom/apm/insight/CrashType;ZZ)Ljava/lang/String;
        //  1032: invokevirtual   com/apm/insight/entity/a.a:(Ljava/lang/String;Ljava/lang/Object;)V
        //  1035: aload           15
        //  1037: ldc_w           "jiffy"
        //  1040: invokestatic    com/apm/insight/runtime/q$a.a:()J
        //  1043: invokestatic    java/lang/Long.valueOf:(J)Ljava/lang/Long;
        //  1046: invokevirtual   com/apm/insight/entity/a.a:(Ljava/lang/String;Ljava/lang/Object;)V
        //  1049: aload           15
        //  1051: invokevirtual   com/apm/insight/entity/a.h:()Lorg/json/JSONObject;
        //  1054: ldc_w           "filters"
        //  1057: invokevirtual   org/json/JSONObject.optJSONObject:(Ljava/lang/String;)Lorg/json/JSONObject;
        //  1060: astore          12
        //  1062: aload           15
        //  1064: invokevirtual   com/apm/insight/entity/a.h:()Lorg/json/JSONObject;
        //  1067: invokestatic    com/apm/insight/entity/d.b:(Lorg/json/JSONObject;)V
        //  1070: aload           12
        //  1072: ifnonnull       1112
        //  1075: new             Lorg/json/JSONObject;
        //  1078: astore          11
        //  1080: aload           11
        //  1082: invokespecial   org/json/JSONObject.<init>:()V
        //  1085: aload           11
        //  1087: astore          12
        //  1089: aload           15
        //  1091: ldc_w           "filters"
        //  1094: aload           11
        //  1096: invokevirtual   com/apm/insight/entity/a.a:(Ljava/lang/String;Ljava/lang/Object;)V
        //  1099: goto            1116
        //  1102: astore          11
        //  1104: goto            1771
        //  1107: astore          11
        //  1109: goto            1771
        //  1112: aload           12
        //  1114: astore          11
        //  1116: aload           11
        //  1118: astore          12
        //  1120: aload           11
        //  1122: ldc_w           "anrType"
        //  1125: aload           21
        //  1127: invokevirtual   org/json/JSONObject.put:(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
        //  1130: pop            
        //  1131: aload           11
        //  1133: astore          12
        //  1135: aload           11
        //  1137: ldc_w           "max_utm_thread"
        //  1140: aload           20
        //  1142: invokevirtual   org/json/JSONObject.put:(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
        //  1145: pop            
        //  1146: aload           11
        //  1148: astore          12
        //  1150: aload           11
        //  1152: ldc_w           "max_stm_thread"
        //  1155: aload           19
        //  1157: invokevirtual   org/json/JSONObject.put:(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
        //  1160: pop            
        //  1161: aload           11
        //  1163: astore          12
        //  1165: aload           11
        //  1167: ldc_w           "max_utm_stm_thread"
        //  1170: aload           22
        //  1172: invokevirtual   org/json/JSONObject.put:(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
        //  1175: pop            
        //  1176: aload           11
        //  1178: astore          12
        //  1180: aload           11
        //  1182: ldc_w           "max_utm_thread_version"
        //  1185: aload_0        
        //  1186: getfield        com/apm/insight/b/b.o:Ljava/lang/String;
        //  1189: invokevirtual   org/json/JSONObject.put:(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
        //  1192: pop            
        //  1193: aload           11
        //  1195: astore          12
        //  1197: aload           11
        //  1199: ldc_w           "crash_length"
        //  1202: aload_0        
        //  1203: lload           7
        //  1205: invokespecial   com/apm/insight/b/b.c:(J)Ljava/lang/String;
        //  1208: invokevirtual   org/json/JSONObject.put:(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
        //  1211: pop            
        //  1212: aload           11
        //  1214: astore          12
        //  1216: aload           11
        //  1218: ldc_w           "disable_looper_monitor"
        //  1221: invokestatic    com/apm/insight/runtime/a.d:()Z
        //  1224: invokestatic    java/lang/String.valueOf:(Z)Ljava/lang/String;
        //  1227: invokevirtual   org/json/JSONObject.put:(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
        //  1230: pop            
        //  1231: aload           11
        //  1233: astore          12
        //  1235: aload           11
        //  1237: ldc_w           "npth_force_apm_crash"
        //  1240: invokestatic    com/apm/insight/c/b.b:()Z
        //  1243: invokestatic    java/lang/String.valueOf:(Z)Ljava/lang/String;
        //  1246: invokevirtual   org/json/JSONObject.put:(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
        //  1249: pop            
        //  1250: aload           11
        //  1252: astore          12
        //  1254: aload           11
        //  1256: ldc_w           "sdk_version"
        //  1259: ldc_w           "1.3.8.nourl-alpha.15"
        //  1262: invokevirtual   org/json/JSONObject.put:(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
        //  1265: pop            
        //  1266: aload           11
        //  1268: astore          12
        //  1270: aload           11
        //  1272: ldc_w           "has_logcat"
        //  1275: aload           15
        //  1277: invokevirtual   com/apm/insight/entity/a.a:()Z
        //  1280: invokestatic    java/lang/String.valueOf:(Z)Ljava/lang/String;
        //  1283: invokevirtual   org/json/JSONObject.put:(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
        //  1286: pop            
        //  1287: aload           11
        //  1289: astore          12
        //  1291: aload           11
        //  1293: ldc_w           "memory_leak"
        //  1296: aload           15
        //  1298: invokevirtual   com/apm/insight/entity/a.f:()Z
        //  1301: invokestatic    java/lang/String.valueOf:(Z)Ljava/lang/String;
        //  1304: invokevirtual   org/json/JSONObject.put:(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
        //  1307: pop            
        //  1308: aload           11
        //  1310: astore          12
        //  1312: aload           11
        //  1314: ldc_w           "fd_leak"
        //  1317: aload           15
        //  1319: invokevirtual   com/apm/insight/entity/a.d:()Z
        //  1322: invokestatic    java/lang/String.valueOf:(Z)Ljava/lang/String;
        //  1325: invokevirtual   org/json/JSONObject.put:(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
        //  1328: pop            
        //  1329: aload           11
        //  1331: astore          12
        //  1333: aload           11
        //  1335: ldc_w           "threads_leak"
        //  1338: aload           15
        //  1340: invokevirtual   com/apm/insight/entity/a.e:()Z
        //  1343: invokestatic    java/lang/String.valueOf:(Z)Ljava/lang/String;
        //  1346: invokevirtual   org/json/JSONObject.put:(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
        //  1349: pop            
        //  1350: aload           11
        //  1352: astore          12
        //  1354: aload           11
        //  1356: ldc_w           "is_64_devices"
        //  1359: invokestatic    com/apm/insight/entity/Header.a:()Z
        //  1362: invokestatic    java/lang/String.valueOf:(Z)Ljava/lang/String;
        //  1365: invokevirtual   org/json/JSONObject.put:(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
        //  1368: pop            
        //  1369: aload           11
        //  1371: astore          12
        //  1373: aload           11
        //  1375: ldc_w           "is_64_runtime"
        //  1378: invokestatic    com/apm/insight/nativecrash/NativeImpl.e:()Z
        //  1381: invokestatic    java/lang/String.valueOf:(Z)Ljava/lang/String;
        //  1384: invokevirtual   org/json/JSONObject.put:(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
        //  1387: pop            
        //  1388: aload           11
        //  1390: astore          12
        //  1392: aload           11
        //  1394: ldc_w           "is_x86_devices"
        //  1397: invokestatic    com/apm/insight/entity/Header.b:()Z
        //  1400: invokestatic    java/lang/String.valueOf:(Z)Ljava/lang/String;
        //  1403: invokevirtual   org/json/JSONObject.put:(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
        //  1406: pop            
        //  1407: aload           11
        //  1409: astore          12
        //  1411: aload           11
        //  1413: ldc_w           "has_meminfo_file"
        //  1416: aload           15
        //  1418: invokevirtual   com/apm/insight/entity/a.g:()Z
        //  1421: invokestatic    java/lang/String.valueOf:(Z)Ljava/lang/String;
        //  1424: invokevirtual   org/json/JSONObject.put:(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
        //  1427: pop            
        //  1428: aload           11
        //  1430: astore          12
        //  1432: invokestatic    com/apm/insight/nativecrash/c.m:()Z
        //  1435: istore          5
        //  1437: iload           5
        //  1439: ifeq            1450
        //  1442: ldc_w           "true"
        //  1445: astore          14
        //  1447: goto            1455
        //  1450: ldc_w           "false"
        //  1453: astore          14
        //  1455: aload           11
        //  1457: astore          12
        //  1459: aload           11
        //  1461: ldc_w           "is_root"
        //  1464: aload           14
        //  1466: invokevirtual   org/json/JSONObject.put:(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
        //  1469: pop            
        //  1470: aload           11
        //  1472: astore          12
        //  1474: aload           11
        //  1476: ldc_w           "anr_normal_trace"
        //  1479: aload_0        
        //  1480: getfield        com/apm/insight/b/b.y:Z
        //  1483: iconst_1       
        //  1484: ixor           
        //  1485: invokestatic    java/lang/String.valueOf:(Z)Ljava/lang/String;
        //  1488: invokevirtual   org/json/JSONObject.put:(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
        //  1491: pop            
        //  1492: aload           11
        //  1494: astore          12
        //  1496: aload           11
        //  1498: ldc_w           "anr_no_run"
        //  1501: iload           6
        //  1503: invokestatic    java/lang/String.valueOf:(Z)Ljava/lang/String;
        //  1506: invokevirtual   org/json/JSONObject.put:(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
        //  1509: pop            
        //  1510: aload           11
        //  1512: astore          12
        //  1514: invokestatic    com/apm/insight/Npth.hasCrash:()Z
        //  1517: istore          5
        //  1519: iload           5
        //  1521: ifeq            1532
        //  1524: ldc_w           "true"
        //  1527: astore          14
        //  1529: goto            1537
        //  1532: ldc_w           "false"
        //  1535: astore          14
        //  1537: aload           11
        //  1539: astore          12
        //  1541: aload           11
        //  1543: ldc_w           "crash_after_crash"
        //  1546: aload           14
        //  1548: invokevirtual   org/json/JSONObject.put:(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
        //  1551: pop            
        //  1552: aload           11
        //  1554: astore          12
        //  1556: aload           11
        //  1558: ldc_w           "from_file"
        //  1561: invokestatic    com/apm/insight/b/d.a:()Z
        //  1564: invokestatic    java/lang/String.valueOf:(Z)Ljava/lang/String;
        //  1567: invokevirtual   org/json/JSONObject.put:(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
        //  1570: pop            
        //  1571: aload           11
        //  1573: astore          12
        //  1575: aload           11
        //  1577: ldc_w           "has_dump"
        //  1580: ldc_w           "true"
        //  1583: invokevirtual   org/json/JSONObject.put:(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
        //  1586: pop            
        //  1587: aload           11
        //  1589: astore          12
        //  1591: aload           11
        //  1593: ldc_w           "from_kill"
        //  1596: iload_3        
        //  1597: invokestatic    java/lang/String.valueOf:(Z)Ljava/lang/String;
        //  1600: invokevirtual   org/json/JSONObject.put:(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
        //  1603: pop            
        //  1604: aload           11
        //  1606: astore          12
        //  1608: aload           11
        //  1610: ldc_w           "last_resume_activity"
        //  1613: invokestatic    com/apm/insight/runtime/a/b.d:()Lcom/apm/insight/runtime/a/b;
        //  1616: invokevirtual   com/apm/insight/runtime/a/b.h:()Ljava/lang/String;
        //  1619: invokevirtual   org/json/JSONObject.put:(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
        //  1622: pop            
        //  1623: aload           11
        //  1625: astore          12
        //  1627: aload_0        
        //  1628: getfield        com/apm/insight/b/b.r:I
        //  1631: istore_1       
        //  1632: iload_1        
        //  1633: ifle            1653
        //  1636: aload           11
        //  1638: astore          12
        //  1640: aload           11
        //  1642: ldc_w           "may_have_stack_overflow"
        //  1645: iload_1        
        //  1646: invokestatic    java/lang/String.valueOf:(I)Ljava/lang/String;
        //  1649: invokevirtual   org/json/JSONObject.put:(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
        //  1652: pop            
        //  1653: iload_3        
        //  1654: ifne            1696
        //  1657: aload_0        
        //  1658: aload           24
        //  1660: aload           11
        //  1662: invokespecial   com/apm/insight/b/b.a:(Ljava/lang/String;Lorg/json/JSONObject;)V
        //  1665: aload           11
        //  1667: astore          12
        //  1669: goto            1771
        //  1672: astore          14
        //  1674: aload           11
        //  1676: astore          12
        //  1678: invokestatic    com/apm/insight/c.a:()Lcom/apm/insight/d;
        //  1681: ldc_w           "NPTH_CATCH"
        //  1684: aload           14
        //  1686: invokevirtual   com/apm/insight/d.a:(Ljava/lang/String;Ljava/lang/Throwable;)V
        //  1689: aload           11
        //  1691: astore          12
        //  1693: goto            1771
        //  1696: aload           11
        //  1698: astore          12
        //  1700: aload_0        
        //  1701: invokespecial   com/apm/insight/b/b.i:()Z
        //  1704: istore          5
        //  1706: aload           11
        //  1708: astore          12
        //  1710: iload           5
        //  1712: ifne            1771
        //  1715: aload           11
        //  1717: astore          12
        //  1719: aload           11
        //  1721: ldc_w           "aid"
        //  1724: aload           15
        //  1726: invokevirtual   com/apm/insight/entity/a.i:()Lcom/apm/insight/entity/Header;
        //  1729: invokevirtual   com/apm/insight/entity/Header.f:()Lorg/json/JSONObject;
        //  1732: ldc_w           "aid"
        //  1735: invokevirtual   org/json/JSONObject.opt:(Ljava/lang/String;)Ljava/lang/Object;
        //  1738: invokestatic    java/lang/String.valueOf:(Ljava/lang/Object;)Ljava/lang/String;
        //  1741: invokevirtual   org/json/JSONObject.put:(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
        //  1744: pop            
        //  1745: aload           11
        //  1747: astore          12
        //  1749: aload           15
        //  1751: invokevirtual   com/apm/insight/entity/a.i:()Lcom/apm/insight/entity/Header;
        //  1754: invokevirtual   com/apm/insight/entity/Header.f:()Lorg/json/JSONObject;
        //  1757: ldc_w           "aid"
        //  1760: sipush          2010
        //  1763: invokevirtual   org/json/JSONObject.put:(Ljava/lang/String;I)Lorg/json/JSONObject;
        //  1766: pop            
        //  1767: aload           11
        //  1769: astore          12
        //  1771: iload_3        
        //  1772: ifeq            1837
        //  1775: invokestatic    com/apm/insight/k/e.c:()Ljava/lang/String;
        //  1778: astore          12
        //  1780: new             Ljava/io/File;
        //  1783: astore          11
        //  1785: aload           11
        //  1787: aload_0        
        //  1788: getfield        com/apm/insight/b/b.d:Landroid/content/Context;
        //  1791: invokestatic    com/apm/insight/l/o.a:(Landroid/content/Context;)Ljava/io/File;
        //  1794: lload           7
        //  1796: getstatic       com/apm/insight/CrashType.ANR:Lcom/apm/insight/CrashType;
        //  1799: iconst_0       
        //  1800: iconst_0       
        //  1801: invokestatic    com/apm/insight/i.a:(JLcom/apm/insight/CrashType;ZZ)Ljava/lang/String;
        //  1804: invokespecial   java/io/File.<init>:(Ljava/io/File;Ljava/lang/String;)V
        //  1807: aload_0        
        //  1808: aload           11
        //  1810: putfield        com/apm/insight/b/b.h:Ljava/io/File;
        //  1813: aload           11
        //  1815: aload           11
        //  1817: invokevirtual   java/io/File.getName:()Ljava/lang/String;
        //  1820: aload           12
        //  1822: aload           15
        //  1824: invokevirtual   com/apm/insight/entity/a.h:()Lorg/json/JSONObject;
        //  1827: invokestatic    com/apm/insight/k/e.b:()Z
        //  1830: invokestatic    com/apm/insight/l/i.a:(Ljava/io/File;Ljava/lang/String;Ljava/lang/String;Lorg/json/JSONObject;Z)Ljava/lang/String;
        //  1833: pop            
        //  1834: goto            2021
        //  1837: aload_0        
        //  1838: getfield        com/apm/insight/b/b.h:Ljava/io/File;
        //  1841: astore          11
        //  1843: aload           11
        //  1845: ifnull          1859
        //  1848: aload           11
        //  1850: invokestatic    com/apm/insight/l/i.a:(Ljava/io/File;)Z
        //  1853: pop            
        //  1854: aload_0        
        //  1855: aconst_null    
        //  1856: putfield        com/apm/insight/b/b.h:Ljava/io/File;
        //  1859: invokestatic    com/apm/insight/a/a.a:()Lcom/apm/insight/a/a;
        //  1862: getstatic       com/apm/insight/CrashType.ANR:Lcom/apm/insight/CrashType;
        //  1865: lload           7
        //  1867: invokestatic    com/apm/insight/i.e:()Ljava/lang/String;
        //  1870: invokevirtual   com/apm/insight/a/a.a:(Lcom/apm/insight/CrashType;JLjava/lang/String;)V
        //  1873: invokestatic    com/apm/insight/l/f.f:()Ljava/io/File;
        //  1876: invokevirtual   java/io/File.length:()J
        //  1879: ldc2_w          1024
        //  1882: lcmp           
        //  1883: ifle            1898
        //  1886: aload           15
        //  1888: ldc_w           "has_system_traces"
        //  1891: ldc_w           "true"
        //  1894: invokevirtual   com/apm/insight/entity/a.a:(Ljava/lang/String;Ljava/lang/String;)Lcom/apm/insight/entity/a;
        //  1897: pop            
        //  1898: invokestatic    com/apm/insight/i.f:()Ljava/lang/String;
        //  1901: invokestatic    com/apm/insight/l/o.e:(Ljava/lang/String;)Ljava/io/File;
        //  1904: invokestatic    com/apm/insight/i.f:()Ljava/lang/String;
        //  1907: invokestatic    com/apm/insight/l/o.f:(Ljava/lang/String;)Ljava/io/File;
        //  1910: invokestatic    com/apm/insight/nativecrash/d.a:(Ljava/io/File;Ljava/io/File;)Lorg/json/JSONArray;
        //  1913: astore          11
        //  1915: aload           12
        //  1917: ldc_w           "leak_threads_count"
        //  1920: aload           11
        //  1922: invokevirtual   org/json/JSONArray.length:()I
        //  1925: invokestatic    java/lang/String.valueOf:(I)Ljava/lang/String;
        //  1928: invokevirtual   org/json/JSONObject.put:(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
        //  1931: pop            
        //  1932: aload           11
        //  1934: invokevirtual   org/json/JSONArray.length:()I
        //  1937: ifle            1952
        //  1940: invokestatic    com/apm/insight/i.f:()Ljava/lang/String;
        //  1943: invokestatic    com/apm/insight/l/o.g:(Ljava/lang/String;)Ljava/io/File;
        //  1946: aload           11
        //  1948: iconst_0       
        //  1949: invokestatic    com/apm/insight/l/i.a:(Ljava/io/File;Lorg/json/JSONArray;Z)V
        //  1952: aload           13
        //  1954: ldc_w           "mainStackFromTrace"
        //  1957: invokevirtual   org/json/JSONObject.optString:(Ljava/lang/String;)Ljava/lang/String;
        //  1960: astore          12
        //  1962: aload           12
        //  1964: invokestatic    com/apm/insight/entity/b.a:(Ljava/lang/String;)Lorg/json/JSONArray;
        //  1967: astore          11
        //  1969: aload           15
        //  1971: invokevirtual   com/apm/insight/entity/a.h:()Lorg/json/JSONObject;
        //  1974: astore          14
        //  1976: new             Lcom/apm/insight/b/b$2;
        //  1979: astore          13
        //  1981: aload           13
        //  1983: aload_0        
        //  1984: lload           7
        //  1986: invokespecial   com/apm/insight/b/b$2.<init>:(Lcom/apm/insight/b/b;J)V
        //  1989: aload           14
        //  1991: aload           11
        //  1993: aload           13
        //  1995: invokestatic    com/apm/insight/entity/b.a:(Lorg/json/JSONObject;Lorg/json/JSONArray;Lcom/apm/insight/entity/b$a;)V
        //  1998: aload           12
        //  2000: aload           11
        //  2002: invokestatic    com/apm/insight/b/b.b:(Ljava/lang/String;Lorg/json/JSONArray;)V
        //  2005: goto            2021
        //  2008: astore          11
        //  2010: invokestatic    com/apm/insight/c.a:()Lcom/apm/insight/d;
        //  2013: ldc_w           "NPTH_CATCH"
        //  2016: aload           11
        //  2018: invokevirtual   com/apm/insight/d.a:(Ljava/lang/String;Ljava/lang/Throwable;)V
        //  2021: iload           4
        //  2023: ireturn        
        //  2024: astore          12
        //  2026: aload           11
        //  2028: monitorexit    
        //  2029: aload           12
        //  2031: athrow         
        //  2032: astore          11
        //  2034: aload           18
        //  2036: astore          11
        //  2038: aload           16
        //  2040: astore          14
        //  2042: goto            600
        //  2045: astore          11
        //  2047: goto            1898
        //  2050: astore          11
        //  2052: goto            1952
        //    Exceptions:
        //  Try           Handler
        //  Start  End    Start  End    Type
        //  -----  -----  -----  -----  ----
        //  129    132    2024   2032   Any
        //  487    495    603    608    Any
        //  499    508    2032   2045   Any
        //  516    529    2032   2045   Any
        //  537    542    2032   2045   Any
        //  550    555    2032   2045   Any
        //  555    564    583    600    Any
        //  623    631    642    648    Any
        //  696    708    2008   2021   Any
        //  708    767    2008   2021   Any
        //  767    780    2008   2021   Any
        //  780    832    2008   2021   Any
        //  832    846    2008   2021   Any
        //  846    870    2008   2021   Any
        //  886    896    2008   2021   Any
        //  901    911    2008   2021   Any
        //  915    921    2008   2021   Any
        //  926    934    2008   2021   Any
        //  934    940    2008   2021   Any
        //  943    949    2008   2021   Any
        //  949    980    2008   2021   Any
        //  980    993    2008   2021   Any
        //  993    1018   2008   2021   Any
        //  1018   1035   2008   2021   Any
        //  1035   1070   2008   2021   Any
        //  1075   1085   1107   1112   Any
        //  1089   1099   1102   1107   Any
        //  1120   1131   1102   1107   Any
        //  1135   1146   1102   1107   Any
        //  1150   1161   1102   1107   Any
        //  1165   1176   1102   1107   Any
        //  1180   1193   1102   1107   Any
        //  1197   1212   1102   1107   Any
        //  1216   1231   1102   1107   Any
        //  1235   1250   1102   1107   Any
        //  1254   1266   1102   1107   Any
        //  1270   1287   1102   1107   Any
        //  1291   1308   1102   1107   Any
        //  1312   1329   1102   1107   Any
        //  1333   1350   1102   1107   Any
        //  1354   1369   1102   1107   Any
        //  1373   1388   1102   1107   Any
        //  1392   1407   1102   1107   Any
        //  1411   1428   1102   1107   Any
        //  1432   1437   1102   1107   Any
        //  1459   1470   1102   1107   Any
        //  1474   1492   1102   1107   Any
        //  1496   1510   1102   1107   Any
        //  1514   1519   1102   1107   Any
        //  1541   1552   1102   1107   Any
        //  1556   1571   1102   1107   Any
        //  1575   1587   1102   1107   Any
        //  1591   1604   1102   1107   Any
        //  1608   1623   1102   1107   Any
        //  1627   1632   1102   1107   Any
        //  1640   1653   1102   1107   Any
        //  1657   1665   1672   1696   Any
        //  1678   1689   1102   1107   Any
        //  1700   1706   1102   1107   Any
        //  1719   1745   1102   1107   Any
        //  1749   1767   1102   1107   Any
        //  1775   1834   2008   2021   Any
        //  1837   1843   2008   2021   Any
        //  1848   1859   2008   2021   Any
        //  1859   1873   2008   2021   Any
        //  1873   1898   2045   2050   Any
        //  1898   1915   2050   2055   Any
        //  1915   1952   2050   2055   Any
        //  1952   2005   2008   2021   Any
        //  2026   2029   2024   2032   Any
        // 
        // The error that occurred was:
        // 
        // java.lang.IndexOutOfBoundsException: Index 925 out of bounds for length 925
        //     at java.base/jdk.internal.util.Preconditions.outOfBounds(Preconditions.java:64)
        //     at java.base/jdk.internal.util.Preconditions.outOfBoundsCheckIndex(Preconditions.java:70)
        //     at java.base/jdk.internal.util.Preconditions.checkIndex(Preconditions.java:266)
        //     at java.base/java.util.Objects.checkIndex(Objects.java:361)
        //     at java.base/java.util.ArrayList.get(ArrayList.java:427)
        //     at com.strobel.decompiler.ast.AstBuilder.convertToAst(AstBuilder.java:3362)
        //     at com.strobel.decompiler.ast.AstBuilder.build(AstBuilder.java:112)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:203)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:93)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethodBody(AstBuilder.java:868)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethod(AstBuilder.java:761)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addTypeMembers(AstBuilder.java:638)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeCore(AstBuilder.java:605)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeNoCache(AstBuilder.java:195)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createType(AstBuilder.java:162)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addType(AstBuilder.java:137)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.buildAst(JavaLanguage.java:71)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.decompileType(JavaLanguage.java:59)
        //     at com.strobel.decompiler.DecompilerDriver.decompileType(DecompilerDriver.java:333)
        //     at com.strobel.decompiler.DecompilerDriver.decompileJar(DecompilerDriver.java:254)
        //     at com.strobel.decompiler.DecompilerDriver.main(DecompilerDriver.java:144)
        // 
        throw new IllegalStateException("An error occurred while decompiling this method.");
    }
    
    public void b() {
        if (!this.e) {
            return;
        }
        this.e = false;
        final c c = this.c;
        if (c != null) {
            c.b();
        }
        this.c = null;
    }
    
    public void c() {
        final c c = this.c;
        if (c != null) {
            c.a();
        }
    }
    
    public void d() {
        if (com.apm.insight.b.b.f) {
            return;
        }
        synchronized (this.x) {
            if (com.apm.insight.b.b.f) {
                return;
            }
            monitorexit(this.x);
            this.B.run();
        }
    }
    
    public void e() {
        if (NativeImpl.g()) {
            try {
                com.apm.insight.l.i.a(this.h(), String.valueOf(this.C + 1), false);
            }
            finally {
                final Throwable t;
                com.apm.insight.c.a().a("NPTH_CATCH", t);
            }
        }
        this.z = SystemClock.uptimeMillis();
        this.y = true;
    }
    
    public void f() {
        final File h = this.h();
        try {
            final int intValue = Integer.decode(com.apm.insight.l.i.c(h.getAbsolutePath()));
            this.C = intValue;
            if (intValue >= 2) {
                NativeImpl.a(false);
            }
            else {
                NativeImpl.a(true);
            }
        }
        catch (final IOException h) {
            NativeImpl.a(true);
        }
        finally {
            com.apm.insight.l.i.a(h);
        }
    }
}

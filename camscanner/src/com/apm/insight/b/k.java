// 
// Decompiled by Procyon v0.6.0
// 

package com.apm.insight.b;

import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONArray;
import android.os.Build$VERSION;
import android.os.Looper;
import android.os.Message;
import java.lang.reflect.Field;
import android.os.MessageQueue;

public class k
{
    private static MessageQueue a;
    private static Field b;
    private static Field c;
    
    protected static Message a(Message message) {
        final Field c = k.c;
        if (c == null) {
            try {
                (k.c = Class.forName("android.os.Message").getDeclaredField("next")).setAccessible(true);
                message = (Message)k.c.get(message);
                return message;
            }
            catch (final Exception ex) {
                return null;
            }
        }
        try {
            message = (Message)c.get(message);
            return message;
        }
        catch (final Exception ex2) {
            return null;
        }
    }
    
    public static Message a(final MessageQueue messageQueue) {
        final Field b = k.b;
        if (b == null) {
            try {
                (k.b = Class.forName("android.os.MessageQueue").getDeclaredField("mMessages")).setAccessible(true);
                return (Message)k.b.get(messageQueue);
            }
            catch (final Exception ex) {
                return null;
            }
        }
        try {
            return (Message)b.get(messageQueue);
        }
        catch (final Exception ex2) {
            return null;
        }
    }
    
    public static MessageQueue a() {
        if (k.a == null && Looper.getMainLooper() != null) {
            final Looper mainLooper = Looper.getMainLooper();
            MessageQueue a;
            if (mainLooper == Looper.myLooper()) {
                a = Looper.myQueue();
            }
            else {
                if (Build$VERSION.SDK_INT < 23) {
                    try {
                        final Field declaredField = mainLooper.getClass().getDeclaredField("mQueue");
                        declaredField.setAccessible(true);
                        k.a = (MessageQueue)declaredField.get(mainLooper);
                    }
                    finally {
                        final Throwable t;
                        t.printStackTrace();
                    }
                    return k.a;
                }
                a = \u3007080.\u3007080(mainLooper);
            }
            k.a = a;
        }
        return k.a;
    }
    
    public static JSONArray a(final int p0, final long p1) {
        // 
        // This method could not be decompiled.
        // 
        // Original Bytecode:
        // 
        //     3: astore          7
        //     5: new             Lorg/json/JSONArray;
        //     8: dup            
        //     9: invokespecial   org/json/JSONArray.<init>:()V
        //    12: astore          6
        //    14: aload           7
        //    16: ifnonnull       22
        //    19: aload           6
        //    21: areturn        
        //    22: aload           7
        //    24: monitorenter   
        //    25: aload           7
        //    27: invokestatic    com/apm/insight/b/k.a:(Landroid/os/MessageQueue;)Landroid/os/Message;
        //    30: astore          5
        //    32: aload           5
        //    34: ifnonnull       43
        //    37: aload           7
        //    39: monitorexit    
        //    40: aload           6
        //    42: areturn        
        //    43: iconst_0       
        //    44: istore          4
        //    46: iconst_0       
        //    47: istore_3       
        //    48: aload           5
        //    50: ifnull          100
        //    53: iload           4
        //    55: iload_0        
        //    56: if_icmpge       100
        //    59: iinc            4, 1
        //    62: iinc            3, 1
        //    65: aload           5
        //    67: lload_1        
        //    68: invokestatic    com/apm/insight/b/k.a:(Landroid/os/Message;J)Lorg/json/JSONObject;
        //    71: astore          8
        //    73: aload           8
        //    75: ldc             "id"
        //    77: iload_3        
        //    78: invokevirtual   org/json/JSONObject.put:(Ljava/lang/String;I)Lorg/json/JSONObject;
        //    81: pop            
        //    82: aload           6
        //    84: aload           8
        //    86: invokevirtual   org/json/JSONArray.put:(Ljava/lang/Object;)Lorg/json/JSONArray;
        //    89: pop            
        //    90: aload           5
        //    92: invokestatic    com/apm/insight/b/k.a:(Landroid/os/Message;)Landroid/os/Message;
        //    95: astore          5
        //    97: goto            48
        //   100: aload           7
        //   102: monitorexit    
        //   103: aload           6
        //   105: areturn        
        //   106: astore          5
        //   108: aload           7
        //   110: monitorexit    
        //   111: aload           5
        //   113: athrow         
        //   114: astore          5
        //   116: invokestatic    com/apm/insight/c.a:()Lcom/apm/insight/d;
        //   119: ldc             "NPTH_CATCH"
        //   121: aload           5
        //   123: invokevirtual   com/apm/insight/d.a:(Ljava/lang/String;Ljava/lang/Throwable;)V
        //   126: aload           6
        //   128: areturn        
        //   129: astore          9
        //   131: goto            82
        //    Exceptions:
        //  Try           Handler
        //  Start  End    Start  End    Type                    
        //  -----  -----  -----  -----  ------------------------
        //  22     25     114    129    Any
        //  25     32     106    114    Any
        //  37     40     106    114    Any
        //  65     73     106    114    Any
        //  73     82     129    134    Lorg/json/JSONException;
        //  73     82     106    114    Any
        //  82     97     106    114    Any
        //  100    103    106    114    Any
        //  108    111    106    114    Any
        //  111    114    114    129    Any
        // 
        // The error that occurred was:
        // 
        // java.lang.IllegalStateException: Expression is linked from several locations: Label_0082:
        //     at com.strobel.decompiler.ast.Error.expressionLinkedFromMultipleLocations(Error.java:27)
        //     at com.strobel.decompiler.ast.AstOptimizer.mergeDisparateObjectInitializations(AstOptimizer.java:2604)
        //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:235)
        //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:42)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:206)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:93)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethodBody(AstBuilder.java:868)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethod(AstBuilder.java:761)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addTypeMembers(AstBuilder.java:638)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeCore(AstBuilder.java:605)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeNoCache(AstBuilder.java:195)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createType(AstBuilder.java:162)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addType(AstBuilder.java:137)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.buildAst(JavaLanguage.java:71)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.decompileType(JavaLanguage.java:59)
        //     at com.strobel.decompiler.DecompilerDriver.decompileType(DecompilerDriver.java:333)
        //     at com.strobel.decompiler.DecompilerDriver.decompileJar(DecompilerDriver.java:254)
        //     at com.strobel.decompiler.DecompilerDriver.main(DecompilerDriver.java:144)
        // 
        throw new IllegalStateException("An error occurred while decompiling this method.");
    }
    
    private static JSONObject a(final Message message, final long n) {
        final JSONObject jsonObject = new JSONObject();
        if (message == null) {
            return jsonObject;
        }
        try {
            jsonObject.put("when", message.getWhen() - n);
            if (message.getCallback() != null) {
                jsonObject.put("callback", (Object)String.valueOf(message.getCallback()));
            }
            jsonObject.put("what", message.what);
            if (message.getTarget() != null) {
                jsonObject.put("target", (Object)String.valueOf(message.getTarget()));
            }
            else {
                jsonObject.put("barrier", message.arg1);
            }
            jsonObject.put("arg1", message.arg1);
            jsonObject.put("arg2", message.arg2);
            final Object obj = message.obj;
            if (obj != null) {
                jsonObject.put("obj", obj);
            }
        }
        catch (final JSONException ex) {
            ((Throwable)ex).printStackTrace();
        }
        return jsonObject;
    }
}

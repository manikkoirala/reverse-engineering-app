// 
// Decompiled by Procyon v0.6.0
// 

package com.apm.insight.b;

import android.os.SystemClock;
import android.text.TextUtils;
import java.util.concurrent.CopyOnWriteArrayList;
import android.util.Printer;

public class i
{
    private static volatile boolean a;
    private static Printer b;
    private static final CopyOnWriteArrayList<e> c;
    private static e d;
    private static volatile a e;
    private static volatile boolean f;
    private static long g;
    private static int h;
    
    static {
        c = new CopyOnWriteArrayList<e>();
        i.f = false;
        i.g = 0L;
        i.h = 0;
    }
    
    public static void a() {
        if (i.a) {
            return;
        }
        i.a = true;
        i.b = (Printer)new Printer() {
            public void println(final String s) {
                if (TextUtils.isEmpty((CharSequence)s)) {
                    return;
                }
                boolean b = false;
                if (s.charAt(0) == '>') {
                    b = true;
                }
                else if (s.charAt(0) != '<') {
                    return;
                }
                i.a(b, s);
            }
        };
        j.a();
        j.a(i.b);
    }
    
    public static void a(final e e) {
        final CopyOnWriteArrayList<e> c = i.c;
        synchronized (c) {
            c.add(e);
        }
    }
    
    public static void a(final boolean b, final String s) {
        final long nanoTime = System.nanoTime();
        com.apm.insight.b.e.a = nanoTime / 1000000L;
        com.apm.insight.b.e.b = SystemClock.currentThreadTimeMillis();
        if (b) {
            final e d = i.d;
            if (d != null && d.a()) {
                i.d.a(s);
            }
        }
        final CopyOnWriteArrayList<e> c = i.c;
        for (int i = 0; i < c.size(); ++i) {
            final e e = c.get(i);
            if (e != null && e.a()) {
                final boolean c2 = e.c;
                if (b) {
                    if (!c2) {
                        e.a(s);
                    }
                }
                else if (c2) {
                    e.b(s);
                }
            }
            else if (!b && e.c) {
                e.b("");
            }
        }
        if (!b) {
            final e d2 = i.d;
            if (d2 != null && d2.a()) {
                i.d.b("");
            }
        }
        if (i.f) {
            i.g += System.nanoTime() - nanoTime;
            final int h = i.h;
            i.h = h + 1;
            if (h >= 1000) {
                if (i.e != null) {
                    i.e.a(i.g);
                }
                i.h = 0;
                i.g = 0L;
                i.f = false;
            }
        }
    }
    
    public interface a
    {
        void a(final long p0);
    }
}

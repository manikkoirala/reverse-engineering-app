// 
// Decompiled by Procyon v0.6.0
// 

package com.apm.insight.b;

import org.json.JSONException;
import org.json.JSONObject;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import org.json.JSONArray;
import android.os.Process;
import android.text.TextUtils;
import com.apm.insight.runtime.g;
import android.os.Looper;
import android.os.SystemClock;
import com.apm.insight.runtime.u;

public class h
{
    public static boolean b = false;
    private static int t = 2;
    c a;
    private int c;
    private volatile int d;
    private int e;
    private int f;
    private f g;
    private b h;
    private long i;
    private long j;
    private int k;
    private long l;
    private String m;
    private String n;
    private com.apm.insight.b.e o;
    private volatile boolean p;
    private boolean q;
    private final u r;
    private volatile boolean s;
    private Runnable u;
    
    public h(final int n) {
        this(n, false);
    }
    
    public h(final int n, final boolean b) {
        this.c = 0;
        this.d = 0;
        this.e = 100;
        this.f = 200;
        this.i = -1L;
        this.j = -1L;
        this.k = -1;
        this.l = -1L;
        this.p = false;
        this.q = false;
        this.s = false;
        this.u = new Runnable() {
            final h a;
            private long b = 0L;
            private long c;
            private int d = -1;
            private int e = 0;
            private int f = 0;
            
            @Override
            public void run() {
                final long uptimeMillis = SystemClock.uptimeMillis();
                final a a = this.a.h.a();
                if (this.d == this.a.d) {
                    ++this.e;
                }
                else {
                    this.e = 0;
                    this.f = 0;
                    this.c = uptimeMillis;
                }
                this.d = this.a.d;
                final int e = this.e;
                if (e > 0 && e - this.f >= com.apm.insight.b.h.t && this.b != 0L && uptimeMillis - this.c > 700L && this.a.s) {
                    a.f = Looper.getMainLooper().getThread().getStackTrace();
                    this.f = this.e;
                }
                a.d = this.a.s;
                a.c = uptimeMillis - this.b - 300L;
                a.a = uptimeMillis;
                final long uptimeMillis2 = SystemClock.uptimeMillis();
                this.b = uptimeMillis2;
                a.b = uptimeMillis2 - uptimeMillis;
                a.e = this.a.d;
                this.a.r.a(this.a.u, 300L);
                this.a.h.a(a);
            }
        };
        this.a = (c)new c(this) {
            final h a;
        };
        if (!b && !com.apm.insight.b.h.b) {
            this.r = null;
        }
        else {
            final u r = new u("looper_monitor");
            (this.r = r).b();
            this.h = new b(300);
            r.a(this.u, 300L);
        }
    }
    
    private static long a(final int n) {
        final long n2 = 0L;
        if (n < 0) {
            return 0L;
        }
        try {
            return g.a(n);
        }
        finally {
            return n2;
        }
    }
    
    public static String a(String str) {
        if (TextUtils.isEmpty((CharSequence)str)) {
            return "unknown message";
        }
        try {
            Object split = str.split(":");
            String str2;
            if (split.length == 2) {
                str2 = split[1];
            }
            else {
                str2 = "";
            }
            while (true) {
                if (str.contains("{") && str.contains("}")) {
                    String str3 = (String)(split = str.split("\\{")[0]);
                    try {
                        final String str4 = str.split("\\}")[1];
                        split = str3;
                        split = str3;
                        final StringBuilder sb = new StringBuilder();
                        split = str3;
                        sb.append(str3);
                        split = str3;
                        sb.append(str4);
                        split = str3;
                        String string = sb.toString();
                        while (true) {
                            str = string;
                            split = str3;
                            if (string.contains("@")) {
                                split = str3;
                                final String[] split2 = string.split("@");
                                str = string;
                                split = str3;
                                if (split2.length > 1) {
                                    str = split2[0];
                                }
                            }
                            String replace = str;
                            split = str3;
                            if (str.contains("(")) {
                                replace = str;
                                split = str3;
                                if (str.contains(")")) {
                                    replace = str;
                                    split = str3;
                                    if (!str.endsWith(" null")) {
                                        split = str3;
                                        final String[] split3 = str.split("\\(");
                                        split = str3;
                                        if (split3.length > 1) {
                                            str = split3[1];
                                        }
                                        split = str3;
                                        replace = str.replace(")", "");
                                    }
                                }
                            }
                            str = replace;
                            split = str3;
                            if (replace.startsWith(" ")) {
                                split = str3;
                                str = replace.replace(" ", "");
                            }
                            split = str3;
                            split = str3;
                            final StringBuilder sb2 = new StringBuilder();
                            split = str3;
                            sb2.append(str);
                            split = str3;
                            sb2.append(str2);
                            split = str3;
                            str = sb2.toString();
                            return str;
                            str3 = str;
                            string = str;
                            continue;
                        }
                    }
                    finally {
                        str = (String)split;
                    }
                    return str;
                }
                continue;
            }
        }
        finally {
            return str;
        }
    }
    
    private void a(final int n, final long n2, final String s) {
        this.a(n, n2, s, true);
    }
    
    private void a(final int n, final long n2, final String h, final boolean b) {
        this.q = true;
        final e a = this.g.a(n);
        a.f = n2 - this.i;
        if (b) {
            final long currentThreadTimeMillis = SystemClock.currentThreadTimeMillis();
            a.g = currentThreadTimeMillis - this.l;
            this.l = currentThreadTimeMillis;
        }
        else {
            a.g = -1L;
        }
        a.e = this.c;
        a.h = h;
        a.i = this.m;
        a.a = this.i;
        a.b = n2;
        a.c = this.j;
        this.g.a(a);
        this.c = 0;
        this.i = n2;
    }
    
    private void a(final boolean b, final long j) {
        final int d = this.d + 1;
        this.d = d;
        this.d = (d & 0xFFFF);
        this.q = false;
        if (this.i < 0L) {
            this.i = j;
        }
        if (this.j < 0L) {
            this.j = j;
        }
        if (this.k < 0) {
            this.k = Process.myTid();
            this.l = SystemClock.currentThreadTimeMillis();
        }
        final long i = this.i;
        final long n = this.f;
        if (j - i > n) {
            final long k = this.j;
            if (j - k > n) {
                if (b) {
                    if (this.c == 0) {
                        this.a(1, j, "no message running");
                    }
                    else {
                        this.a(9, k, this.m);
                        this.a(1, j, "no message running", false);
                    }
                }
                else {
                    if (this.c != 0) {
                        this.a(9, k, this.m, false);
                    }
                    this.a(8, j, this.n, true);
                }
            }
            else {
                this.a(9, j, this.n);
            }
        }
        this.j = j;
    }
    
    private void e() {
        this.e = 100;
        this.f = 300;
    }
    
    public e a(final long n) {
        final e e = new e();
        e.h = this.n;
        e.i = this.m;
        e.f = n - this.j;
        e.g = a(this.k) - this.l;
        e.e = this.c;
        return e;
    }
    
    public void a() {
        if (this.p) {
            return;
        }
        this.p = true;
        this.e();
        this.g = new f(this.e);
        this.o = new com.apm.insight.b.e(this) {
            final h d;
            
            @Override
            public void a(final String s) {
                this.d.s = true;
                this.d.n = s;
                super.a(s);
                this.d.a(true, e.a);
            }
            
            @Override
            public boolean a() {
                return true;
            }
            
            @Override
            public void b(final String s) {
                super.b(s);
                this.d.c++;
                this.d.a(false, e.a);
                final h d = this.d;
                d.m = d.n;
                this.d.n = "no message running";
                this.d.s = false;
            }
        };
        com.apm.insight.b.i.a();
        com.apm.insight.b.i.a(this.o);
        com.apm.insight.b.k.a(com.apm.insight.b.k.a());
    }
    
    void b() {
        this.a();
    }
    
    public JSONArray c() {
        final JSONArray jsonArray = new JSONArray();
        try {
            final List<e> a = this.g.a();
            if (a == null) {
                return jsonArray;
            }
            final Iterator<e> iterator = a.iterator();
            int n = 0;
            while (iterator.hasNext()) {
                final e e = iterator.next();
                if (e == null) {
                    continue;
                }
                ++n;
                jsonArray.put((Object)e.a().put("id", n));
            }
            return jsonArray;
        }
        finally {
            return jsonArray;
        }
    }
    
    private static class a
    {
        long a;
        long b;
        long c;
        boolean d;
        int e;
        StackTraceElement[] f;
        
        void a() {
            this.a = -1L;
            this.b = -1L;
            this.c = -1L;
            this.e = -1;
            this.f = null;
        }
    }
    
    private static class b
    {
        final int a;
        a b;
        final List<a> c;
        private int d;
        
        public b(final int n) {
            this.d = 0;
            this.a = n;
            this.c = new ArrayList<a>(n);
        }
        
        a a() {
            a b = this.b;
            if (b != null) {
                this.b = null;
            }
            else {
                b = new a();
            }
            return b;
        }
        
        void a(a b) {
            final int size = this.c.size();
            final int a = this.a;
            int size2;
            if (size < a) {
                this.c.add(b);
                size2 = this.c.size();
            }
            else {
                final int d = this.d % a;
                this.d = d;
                b = this.c.set(d, b);
                b.a();
                this.b = b;
                size2 = this.d + 1;
            }
            this.d = size2;
        }
    }
    
    public interface c
    {
    }
    
    public static class d
    {
        long a;
        long b;
        long c;
        long d;
        long e;
    }
    
    public static class e
    {
        public long a;
        long b;
        long c;
        int d;
        int e;
        long f;
        long g;
        String h;
        public String i;
        String j;
        d k;
        
        private void a(final JSONObject jsonObject) {
            jsonObject.put("block_uuid", (Object)this.j);
            jsonObject.put("sblock_uuid", (Object)this.j);
            jsonObject.put("belong_frame", this.k != null);
            final d k = this.k;
            if (k != null) {
                jsonObject.put("vsyncDelayTime", this.c - k.a / 1000000L);
                jsonObject.put("doFrameTime", this.k.b / 1000000L - this.c);
                final d i = this.k;
                jsonObject.put("inputHandlingTime", i.c / 1000000L - i.b / 1000000L);
                final d j = this.k;
                jsonObject.put("animationsTime", j.d / 1000000L - j.c / 1000000L);
                final d l = this.k;
                jsonObject.put("performTraversalsTime", l.e / 1000000L - l.d / 1000000L);
                jsonObject.put("drawTime", this.b - this.k.e / 1000000L);
            }
        }
        
        public JSONObject a() {
            final JSONObject jsonObject = new JSONObject();
            try {
                jsonObject.put("msg", (Object)com.apm.insight.b.h.a(this.h));
                jsonObject.put("cpuDuration", this.g);
                jsonObject.put("duration", this.f);
                jsonObject.put("type", this.d);
                jsonObject.put("count", this.e);
                jsonObject.put("messageCount", this.e);
                jsonObject.put("lastDuration", this.b - this.c);
                jsonObject.put("start", this.a);
                jsonObject.put("end", this.b);
                this.a(jsonObject);
            }
            catch (final JSONException ex) {
                ((Throwable)ex).printStackTrace();
            }
            return jsonObject;
        }
        
        void b() {
            this.d = -1;
            this.e = -1;
            this.f = -1L;
            this.h = null;
            this.j = null;
            this.k = null;
            this.i = null;
        }
    }
    
    private static class f
    {
        int a;
        int b;
        e c;
        List<e> d;
        
        f(final int a) {
            this.d = new ArrayList<e>();
            this.a = a;
        }
        
        e a(final int n) {
            e c = this.c;
            if (c != null) {
                c.d = n;
                this.c = null;
            }
            else {
                c = new e();
                c.d = n;
            }
            return c;
        }
        
        List<e> a() {
            final ArrayList list = new ArrayList();
            final int size = this.d.size();
            final int a = this.a;
            int i = 0;
            final int n = 0;
            if (size == a) {
                int b = this.b;
                int j;
                while (true) {
                    j = n;
                    if (b >= this.d.size()) {
                        break;
                    }
                    list.add(this.d.get(b));
                    ++b;
                }
                while (j < this.b - 1) {
                    list.add(this.d.get(j));
                    ++j;
                }
            }
            else {
                while (i < this.d.size()) {
                    list.add(this.d.get(i));
                    ++i;
                }
            }
            return list;
        }
        
        void a(e c) {
            final int size = this.d.size();
            final int a = this.a;
            int size2;
            if (size < a) {
                this.d.add(c);
                size2 = this.d.size();
            }
            else {
                final int b = this.b % a;
                this.b = b;
                c = this.d.set(b, c);
                c.b();
                this.c = c;
                size2 = this.b + 1;
            }
            this.b = size2;
        }
    }
}

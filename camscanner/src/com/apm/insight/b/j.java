// 
// Decompiled by Procyon v0.6.0
// 

package com.apm.insight.b;

import java.util.Iterator;
import android.text.TextUtils;
import java.util.ArrayList;
import java.util.List;
import java.lang.reflect.Field;
import android.os.Looper;
import com.apm.insight.i;
import android.util.Printer;

public final class j
{
    private static int a = 5;
    private static b b;
    private static a c;
    private static boolean d;
    private static Printer e;
    
    public static void a() {
        if (j.d) {
            return;
        }
        j.d = true;
        j.b = new b();
        final Printer d = d();
        if ((j.e = d) != null) {
            j.b.a.add(d);
        }
        if (i.r()) {
            Looper.getMainLooper().setMessageLogging((Printer)j.b);
        }
    }
    
    public static void a(final Printer printer) {
        if (printer != null && !j.b.c.contains(printer)) {
            j.b.c.add(printer);
            j.b.e = true;
        }
    }
    
    private static Printer d() {
        try {
            final Field declaredField = Class.forName("android.os.Looper").getDeclaredField("mLogging");
            declaredField.setAccessible(true);
            return (Printer)declaredField.get(Looper.getMainLooper());
        }
        catch (final Exception ex) {
            return null;
        }
    }
    
    public interface a
    {
        void a(final long p0);
    }
    
    static class b implements Printer
    {
        List<Printer> a;
        List<Printer> b;
        List<Printer> c;
        boolean d;
        boolean e;
        
        b() {
            this.a = new ArrayList<Printer>();
            this.b = new ArrayList<Printer>();
            this.c = new ArrayList<Printer>();
            this.d = false;
            this.e = false;
        }
        
        public void println(final String s) {
            if (TextUtils.isEmpty((CharSequence)s)) {
                return;
            }
            long currentTimeMillis;
            if (j.c != null) {
                currentTimeMillis = System.currentTimeMillis();
            }
            else {
                currentTimeMillis = 0L;
            }
            if (s.charAt(0) == '>' && this.e) {
                for (final Printer printer : this.c) {
                    if (!this.a.contains(printer)) {
                        this.a.add(printer);
                    }
                }
                this.c.clear();
                this.e = false;
            }
            this.a.size();
            j.a;
            for (final Printer printer2 : this.a) {
                if (printer2 != null) {
                    printer2.println(s);
                }
            }
            if (s.charAt(0) == '<' && this.d) {
                for (final Printer printer3 : this.b) {
                    this.a.remove(printer3);
                    this.c.remove(printer3);
                }
                this.b.clear();
                this.d = false;
            }
            if (j.c != null && currentTimeMillis > 0L) {
                j.c.a(System.currentTimeMillis() - currentTimeMillis);
            }
        }
    }
}

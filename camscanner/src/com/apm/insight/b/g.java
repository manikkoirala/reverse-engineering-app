// 
// Decompiled by Procyon v0.6.0
// 

package com.apm.insight.b;

import androidx.annotation.NonNull;
import android.content.Context;

public class g
{
    private static volatile g a;
    private static h c;
    private final b b;
    
    private g(@NonNull final Context context) {
        this.b = new b(context);
        (g.c = new h(0)).b();
    }
    
    public static g a(final Context context) {
        if (g.a == null) {
            synchronized (g.class) {
                if (g.a == null) {
                    g.a = new g(context);
                }
            }
        }
        return g.a;
    }
    
    public static h b() {
        return g.c;
    }
    
    public b a() {
        return this.b;
    }
    
    public void c() {
        this.b.a();
    }
    
    public void d() {
        this.b.b();
    }
}

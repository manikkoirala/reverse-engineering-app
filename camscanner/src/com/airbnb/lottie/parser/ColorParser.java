// 
// Decompiled by Procyon v0.6.0
// 

package com.airbnb.lottie.parser;

import android.graphics.Color;
import java.io.IOException;
import com.airbnb.lottie.parser.moshi.JsonReader;

public class ColorParser implements ValueParser<Integer>
{
    public static final ColorParser \u3007080;
    
    static {
        \u3007080 = new ColorParser();
    }
    
    private ColorParser() {
    }
    
    public Integer \u3007o00\u3007\u3007Oo(final JsonReader jsonReader, final float n) throws IOException {
        final boolean b = jsonReader.O\u30078O8\u3007008() == JsonReader.Token.BEGIN_ARRAY;
        if (b) {
            jsonReader.\u3007\u3007888();
        }
        final double \u3007o\u3007 = jsonReader.\u3007O\u3007();
        final double \u3007o\u30072 = jsonReader.\u3007O\u3007();
        final double \u3007o\u30073 = jsonReader.\u3007O\u3007();
        final double \u3007o\u30074 = jsonReader.\u3007O\u3007();
        if (b) {
            jsonReader.\u30078o8o\u3007();
        }
        double n2 = \u3007o\u3007;
        double n3 = \u3007o\u30072;
        double n4 = \u3007o\u30073;
        double n5 = \u3007o\u30074;
        if (\u3007o\u3007 <= 1.0) {
            n2 = \u3007o\u3007;
            n3 = \u3007o\u30072;
            n4 = \u3007o\u30073;
            n5 = \u3007o\u30074;
            if (\u3007o\u30072 <= 1.0) {
                n2 = \u3007o\u3007;
                n3 = \u3007o\u30072;
                n4 = \u3007o\u30073;
                n5 = \u3007o\u30074;
                if (\u3007o\u30073 <= 1.0) {
                    final double n6 = \u3007o\u3007 * 255.0;
                    final double n7 = \u3007o\u30072 * 255.0;
                    final double n8 = \u3007o\u30073 * 255.0;
                    n2 = n6;
                    n3 = n7;
                    n4 = n8;
                    n5 = \u3007o\u30074;
                    if (\u3007o\u30074 <= 1.0) {
                        n5 = \u3007o\u30074 * 255.0;
                        n4 = n8;
                        n3 = n7;
                        n2 = n6;
                    }
                }
            }
        }
        return Color.argb((int)n5, (int)n2, (int)n3, (int)n4);
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.airbnb.lottie.parser;

import android.graphics.PointF;
import androidx.core.view.animation.PathInterpolatorCompat;
import com.airbnb.lottie.utils.Utils;
import com.airbnb.lottie.utils.MiscUtils;
import com.airbnb.lottie.LottieComposition;
import androidx.annotation.Nullable;
import java.io.IOException;
import com.airbnb.lottie.value.Keyframe;
import android.view.animation.LinearInterpolator;
import com.airbnb.lottie.parser.moshi.JsonReader;
import java.lang.ref.WeakReference;
import androidx.collection.SparseArrayCompat;
import android.view.animation.Interpolator;

class KeyframeParser
{
    private static final Interpolator \u3007080;
    private static SparseArrayCompat<WeakReference<Interpolator>> \u3007o00\u3007\u3007Oo;
    static JsonReader.Options \u3007o\u3007;
    
    static {
        \u3007080 = (Interpolator)new LinearInterpolator();
        KeyframeParser.\u3007o\u3007 = JsonReader.Options.\u3007080("t", "s", "e", "o", "i", "h", "to", "ti");
    }
    
    private static <T> Keyframe<T> O8(final JsonReader jsonReader, final float n, final ValueParser<T> valueParser) throws IOException {
        return new Keyframe<T>(valueParser.\u3007080(jsonReader, n));
    }
    
    private static SparseArrayCompat<WeakReference<Interpolator>> Oo08() {
        if (KeyframeParser.\u3007o00\u3007\u3007Oo == null) {
            KeyframeParser.\u3007o00\u3007\u3007Oo = new SparseArrayCompat<WeakReference<Interpolator>>();
        }
        return KeyframeParser.\u3007o00\u3007\u3007Oo;
    }
    
    private static void o\u30070(final int n, final WeakReference<Interpolator> weakReference) {
        synchronized (KeyframeParser.class) {
            KeyframeParser.\u3007o00\u3007\u3007Oo.put(n, weakReference);
        }
    }
    
    @Nullable
    private static WeakReference<Interpolator> \u3007080(final int n) {
        synchronized (KeyframeParser.class) {
            return Oo08().get(n);
        }
    }
    
    static <T> Keyframe<T> \u3007o00\u3007\u3007Oo(final JsonReader jsonReader, final LottieComposition lottieComposition, final float n, final ValueParser<T> valueParser, final boolean b) throws IOException {
        if (b) {
            return \u3007o\u3007(lottieComposition, jsonReader, n, valueParser);
        }
        return O8(jsonReader, n, valueParser);
    }
    
    private static <T> Keyframe<T> \u3007o\u3007(LottieComposition lottieComposition, JsonReader referent, float y, final ValueParser<T> valueParser) throws IOException {
        ((JsonReader)referent).oO80();
        final Object o = null;
        PointF oo08 = null;
        PointF oo9 = null;
        Object \u300781;
        Object \u3007080 = \u300781 = oo9;
        Object oo11;
        Object oo10 = oo11 = \u300781;
        boolean b = false;
        float n = 0.0f;
        while (((JsonReader)referent).OO0o\u3007\u3007()) {
            switch (((JsonReader)referent).OOO\u3007O0(KeyframeParser.\u3007o\u3007)) {
                default: {
                    ((JsonReader)referent).\u300700\u30078();
                    continue;
                }
                case 7: {
                    oo10 = JsonUtils.Oo08((JsonReader)referent, y);
                    continue;
                }
                case 6: {
                    oo11 = JsonUtils.Oo08((JsonReader)referent, y);
                    continue;
                }
                case 5: {
                    b = (((JsonReader)referent).\u3007O00() == 1);
                    continue;
                }
                case 4: {
                    oo9 = JsonUtils.Oo08((JsonReader)referent, y);
                    continue;
                }
                case 3: {
                    oo08 = JsonUtils.Oo08((JsonReader)referent, y);
                    continue;
                }
                case 2: {
                    \u3007080 = valueParser.\u3007080((JsonReader)referent, y);
                    continue;
                }
                case 1: {
                    \u300781 = valueParser.\u3007080((JsonReader)referent, y);
                    continue;
                }
                case 0: {
                    n = (float)((JsonReader)referent).\u3007O\u3007();
                    continue;
                }
            }
        }
        ((JsonReader)referent).\u3007O8o08O();
        Label_0518: {
            if (b) {
                referent = KeyframeParser.\u3007080;
                final Object o2 = \u300781;
                break Label_0518;
            }
            Label_0511: {
                if (oo08 == null || oo9 == null) {
                    break Label_0511;
                }
                final float x = oo08.x;
                final float n2 = -y;
                oo08.x = MiscUtils.\u3007o00\u3007\u3007Oo(x, n2, y);
                oo08.y = MiscUtils.\u3007o00\u3007\u3007Oo(oo08.y, -100.0f, 100.0f);
                oo9.x = MiscUtils.\u3007o00\u3007\u3007Oo(oo9.x, n2, y);
                final float \u3007o00\u3007\u3007Oo = MiscUtils.\u3007o00\u3007\u3007Oo(oo9.y, -100.0f, 100.0f);
                oo9.y = \u3007o00\u3007\u3007Oo;
                final int \u300780\u3007808\u3007O = Utils.\u300780\u3007808\u3007O(oo08.x, oo08.y, oo9.x, \u3007o00\u3007\u3007Oo);
                final WeakReference<Interpolator> \u300782 = \u3007080(\u300780\u3007808\u3007O);
                referent = o;
                if (\u300782 != null) {
                    referent = \u300782.get();
                }
                while (true) {
                    Object o3;
                    if (\u300782 != null && (o3 = referent) != null) {
                        break Label_0506;
                    }
                    oo08.x /= y;
                    oo08.y /= y;
                    final float x2 = oo9.x / y;
                    oo9.x = x2;
                    y = oo9.y / y;
                    oo9.y = y;
                    try {
                        referent = PathInterpolatorCompat.create(oo08.x, oo08.y, x2, y);
                    }
                    catch (final IllegalArgumentException ex) {
                        if (ex.getMessage().equals("The Path cannot loop back on itself.")) {
                            referent = PathInterpolatorCompat.create(Math.min(oo08.x, 1.0f), oo08.y, Math.max(oo9.x, 0.0f), oo9.y);
                        }
                        else {
                            referent = new LinearInterpolator();
                        }
                    }
                    try {
                        o\u30070(\u300780\u3007808\u3007O, new WeakReference<Interpolator>((Interpolator)referent));
                        o3 = referent;
                        referent = o3;
                        while (true) {
                            final Object o2 = \u3007080;
                            lottieComposition = (LottieComposition)new Keyframe(lottieComposition, \u300781, o2, (Interpolator)referent, n, null);
                            ((Keyframe)lottieComposition).OO0o\u3007\u3007 = (PointF)oo11;
                            ((Keyframe)lottieComposition).Oooo8o0\u3007 = (PointF)oo10;
                            return (Keyframe<T>)lottieComposition;
                            referent = KeyframeParser.\u3007080;
                            continue;
                        }
                    }
                    catch (final ArrayIndexOutOfBoundsException ex2) {
                        o3 = referent;
                        continue;
                    }
                    break;
                }
            }
        }
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.airbnb.lottie.parser;

import java.io.IOException;
import com.airbnb.lottie.model.animatable.AnimatableIntegerValue;
import com.airbnb.lottie.model.animatable.AnimatableColorValue;
import com.airbnb.lottie.model.animatable.AnimatableFloatValue;
import java.util.List;
import java.util.ArrayList;
import com.airbnb.lottie.model.content.ShapeStroke;
import com.airbnb.lottie.LottieComposition;
import com.airbnb.lottie.parser.moshi.JsonReader;

class ShapeStrokeParser
{
    private static JsonReader.Options \u3007080;
    private static final JsonReader.Options \u3007o00\u3007\u3007Oo;
    
    static {
        ShapeStrokeParser.\u3007080 = JsonReader.Options.\u3007080("nm", "c", "w", "o", "lc", "lj", "ml", "hd", "d");
        \u3007o00\u3007\u3007Oo = JsonReader.Options.\u3007080("n", "v");
    }
    
    static ShapeStroke \u3007080(final JsonReader jsonReader, final LottieComposition lottieComposition) throws IOException {
        final ArrayList list = new ArrayList();
        String o800o8O = null;
        AnimatableFloatValue animatableFloatValue = null;
        AnimatableColorValue \u3007o\u3007 = null;
        AnimatableIntegerValue oo80 = null;
        AnimatableFloatValue oo81 = null;
        Enum<ShapeStroke.LineCapType> enum1 = null;
        Enum<ShapeStroke.LineJoinType> enum2 = null;
        float n = 0.0f;
        boolean \u3007\u3007808\u3007 = false;
        while (jsonReader.OO0o\u3007\u3007()) {
            switch (jsonReader.OOO\u3007O0(ShapeStrokeParser.\u3007080)) {
                default: {
                    jsonReader.\u300700\u30078();
                    continue;
                }
                case 8: {
                    jsonReader.\u3007\u3007888();
                    AnimatableFloatValue animatableFloatValue2 = animatableFloatValue;
                    while (jsonReader.OO0o\u3007\u3007()) {
                        jsonReader.oO80();
                        String o800o8O2 = null;
                        AnimatableFloatValue oo82 = null;
                        while (jsonReader.OO0o\u3007\u3007()) {
                            final int ooo\u3007O0 = jsonReader.OOO\u3007O0(ShapeStrokeParser.\u3007o00\u3007\u3007Oo);
                            if (ooo\u3007O0 != 0) {
                                if (ooo\u3007O0 != 1) {
                                    jsonReader.O8\u3007o();
                                    jsonReader.\u300700\u30078();
                                }
                                else {
                                    oo82 = AnimatableValueParser.Oo08(jsonReader, lottieComposition);
                                }
                            }
                            else {
                                o800o8O2 = jsonReader.o800o8O();
                            }
                        }
                        jsonReader.\u3007O8o08O();
                        o800o8O2.hashCode();
                        final int hashCode = o800o8O2.hashCode();
                        int n2 = -1;
                        switch (hashCode) {
                            case 111: {
                                if (!o800o8O2.equals("o")) {
                                    break;
                                }
                                n2 = 2;
                                break;
                            }
                            case 103: {
                                if (!o800o8O2.equals("g")) {
                                    break;
                                }
                                n2 = 1;
                                break;
                            }
                            case 100: {
                                if (!o800o8O2.equals("d")) {
                                    break;
                                }
                                n2 = 0;
                                break;
                            }
                        }
                        switch (n2) {
                            default: {
                                continue;
                            }
                            case 2: {
                                animatableFloatValue2 = oo82;
                                continue;
                            }
                            case 0:
                            case 1: {
                                lottieComposition.OoO8(true);
                                list.add(oo82);
                                continue;
                            }
                        }
                    }
                    jsonReader.\u30078o8o\u3007();
                    animatableFloatValue = animatableFloatValue2;
                    if (list.size() == 1) {
                        list.add(list.get(0));
                        animatableFloatValue = animatableFloatValue2;
                        continue;
                    }
                    continue;
                }
                case 7: {
                    \u3007\u3007808\u3007 = jsonReader.\u3007\u3007808\u3007();
                    continue;
                }
                case 6: {
                    n = (float)jsonReader.\u3007O\u3007();
                    continue;
                }
                case 5: {
                    enum2 = ShapeStroke.LineJoinType.values()[jsonReader.\u3007O00() - 1];
                    continue;
                }
                case 4: {
                    enum1 = ShapeStroke.LineCapType.values()[jsonReader.\u3007O00() - 1];
                    continue;
                }
                case 3: {
                    oo80 = AnimatableValueParser.oO80(jsonReader, lottieComposition);
                    continue;
                }
                case 2: {
                    oo81 = AnimatableValueParser.Oo08(jsonReader, lottieComposition);
                    continue;
                }
                case 1: {
                    \u3007o\u3007 = AnimatableValueParser.\u3007o\u3007(jsonReader, lottieComposition);
                    continue;
                }
                case 0: {
                    o800o8O = jsonReader.o800o8O();
                    continue;
                }
            }
        }
        return new ShapeStroke(o800o8O, animatableFloatValue, list, \u3007o\u3007, oo80, oo81, (ShapeStroke.LineCapType)enum1, (ShapeStroke.LineJoinType)enum2, n, \u3007\u3007808\u3007);
    }
}

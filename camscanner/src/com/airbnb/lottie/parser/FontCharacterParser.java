// 
// Decompiled by Procyon v0.6.0
// 

package com.airbnb.lottie.parser;

import java.io.IOException;
import com.airbnb.lottie.model.content.ShapeGroup;
import java.util.List;
import com.airbnb.lottie.model.content.ContentModel;
import java.util.ArrayList;
import com.airbnb.lottie.model.FontCharacter;
import com.airbnb.lottie.LottieComposition;
import com.airbnb.lottie.parser.moshi.JsonReader;

class FontCharacterParser
{
    private static final JsonReader.Options \u3007080;
    private static final JsonReader.Options \u3007o00\u3007\u3007Oo;
    
    static {
        \u3007080 = JsonReader.Options.\u3007080("ch", "size", "w", "style", "fFamily", "data");
        \u3007o00\u3007\u3007Oo = JsonReader.Options.\u3007080("shapes");
    }
    
    static FontCharacter \u3007080(final JsonReader jsonReader, final LottieComposition lottieComposition) throws IOException {
        final ArrayList list = new ArrayList();
        jsonReader.oO80();
        double \u3007o\u3007 = 0.0;
        String o800o8O = null;
        String o800o8O2 = null;
        double \u3007o\u30072 = 0.0;
        char char1 = '\0';
        while (jsonReader.OO0o\u3007\u3007()) {
            final int ooo\u3007O0 = jsonReader.OOO\u3007O0(FontCharacterParser.\u3007080);
            if (ooo\u3007O0 != 0) {
                if (ooo\u3007O0 != 1) {
                    if (ooo\u3007O0 != 2) {
                        if (ooo\u3007O0 != 3) {
                            if (ooo\u3007O0 != 4) {
                                if (ooo\u3007O0 != 5) {
                                    jsonReader.O8\u3007o();
                                    jsonReader.\u300700\u30078();
                                }
                                else {
                                    jsonReader.oO80();
                                    while (jsonReader.OO0o\u3007\u3007()) {
                                        if (jsonReader.OOO\u3007O0(FontCharacterParser.\u3007o00\u3007\u3007Oo) != 0) {
                                            jsonReader.O8\u3007o();
                                            jsonReader.\u300700\u30078();
                                        }
                                        else {
                                            jsonReader.\u3007\u3007888();
                                            while (jsonReader.OO0o\u3007\u3007()) {
                                                list.add(ContentModelParser.\u3007080(jsonReader, lottieComposition));
                                            }
                                            jsonReader.\u30078o8o\u3007();
                                        }
                                    }
                                    jsonReader.\u3007O8o08O();
                                }
                            }
                            else {
                                o800o8O2 = jsonReader.o800o8O();
                            }
                        }
                        else {
                            o800o8O = jsonReader.o800o8O();
                        }
                    }
                    else {
                        \u3007o\u3007 = jsonReader.\u3007O\u3007();
                    }
                }
                else {
                    \u3007o\u30072 = jsonReader.\u3007O\u3007();
                }
            }
            else {
                char1 = jsonReader.o800o8O().charAt(0);
            }
        }
        jsonReader.\u3007O8o08O();
        return new FontCharacter(list, char1, \u3007o\u30072, \u3007o\u3007, o800o8O, o800o8O2);
    }
}

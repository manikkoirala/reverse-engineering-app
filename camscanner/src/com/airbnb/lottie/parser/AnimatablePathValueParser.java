// 
// Decompiled by Procyon v0.6.0
// 

package com.airbnb.lottie.parser;

import com.airbnb.lottie.model.animatable.AnimatableFloatValue;
import com.airbnb.lottie.model.animatable.AnimatableSplitDimensionPathValue;
import com.airbnb.lottie.model.animatable.AnimatableValue;
import java.io.IOException;
import android.graphics.PointF;
import com.airbnb.lottie.utils.Utils;
import com.airbnb.lottie.value.Keyframe;
import java.util.List;
import java.util.ArrayList;
import com.airbnb.lottie.model.animatable.AnimatablePathValue;
import com.airbnb.lottie.LottieComposition;
import com.airbnb.lottie.parser.moshi.JsonReader;

public class AnimatablePathValueParser
{
    private static JsonReader.Options \u3007080;
    
    static {
        AnimatablePathValueParser.\u3007080 = JsonReader.Options.\u3007080("k", "x", "y");
    }
    
    public static AnimatablePathValue \u3007080(final JsonReader jsonReader, final LottieComposition lottieComposition) throws IOException {
        final ArrayList list = new ArrayList();
        if (jsonReader.O\u30078O8\u3007008() == JsonReader.Token.BEGIN_ARRAY) {
            jsonReader.\u3007\u3007888();
            while (jsonReader.OO0o\u3007\u3007()) {
                list.add(PathKeyframeParser.\u3007080(jsonReader, lottieComposition));
            }
            jsonReader.\u30078o8o\u3007();
            KeyframesParser.\u3007o00\u3007\u3007Oo((List<? extends Keyframe<Object>>)list);
        }
        else {
            list.add(new Keyframe(JsonUtils.Oo08(jsonReader, Utils.Oo08())));
        }
        return new AnimatablePathValue(list);
    }
    
    static AnimatableValue<PointF, PointF> \u3007o00\u3007\u3007Oo(final JsonReader jsonReader, final LottieComposition lottieComposition) throws IOException {
        jsonReader.oO80();
        AnimatableValue<PointF, PointF> \u3007080 = null;
        AnimatableFloatValue oo08 = null;
        AnimatableFloatValue oo9 = null;
        boolean b = false;
        while (jsonReader.O\u30078O8\u3007008() != JsonReader.Token.END_OBJECT) {
            final int ooo\u3007O0 = jsonReader.OOO\u3007O0(AnimatablePathValueParser.\u3007080);
            if (ooo\u3007O0 != 0) {
                if (ooo\u3007O0 != 1) {
                    if (ooo\u3007O0 != 2) {
                        jsonReader.O8\u3007o();
                        jsonReader.\u300700\u30078();
                        continue;
                    }
                    if (jsonReader.O\u30078O8\u3007008() != JsonReader.Token.STRING) {
                        oo9 = AnimatableValueParser.Oo08(jsonReader, lottieComposition);
                        continue;
                    }
                    jsonReader.\u300700\u30078();
                }
                else {
                    if (jsonReader.O\u30078O8\u3007008() != JsonReader.Token.STRING) {
                        oo08 = AnimatableValueParser.Oo08(jsonReader, lottieComposition);
                        continue;
                    }
                    jsonReader.\u300700\u30078();
                }
                b = true;
            }
            else {
                \u3007080 = \u3007080(jsonReader, lottieComposition);
            }
        }
        jsonReader.\u3007O8o08O();
        if (b) {
            lottieComposition.\u3007080("Lottie doesn't support expressions.");
        }
        if (\u3007080 != null) {
            return \u3007080;
        }
        return new AnimatableSplitDimensionPathValue(oo08, oo9);
    }
}

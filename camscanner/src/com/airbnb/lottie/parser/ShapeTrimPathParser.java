// 
// Decompiled by Procyon v0.6.0
// 

package com.airbnb.lottie.parser;

import java.io.IOException;
import com.airbnb.lottie.model.animatable.AnimatableFloatValue;
import com.airbnb.lottie.model.content.ShapeTrimPath;
import com.airbnb.lottie.LottieComposition;
import com.airbnb.lottie.parser.moshi.JsonReader;

class ShapeTrimPathParser
{
    private static JsonReader.Options \u3007080;
    
    static {
        ShapeTrimPathParser.\u3007080 = JsonReader.Options.\u3007080("s", "e", "o", "nm", "m", "hd");
    }
    
    static ShapeTrimPath \u3007080(final JsonReader jsonReader, final LottieComposition lottieComposition) throws IOException {
        String o800o8O = null;
        AnimatableFloatValue o\u30070;
        Object forId = o\u30070 = null;
        AnimatableFloatValue o\u30073;
        AnimatableFloatValue o\u30072 = o\u30073 = o\u30070;
        boolean \u3007\u3007808\u3007 = false;
        while (jsonReader.OO0o\u3007\u3007()) {
            final int ooo\u3007O0 = jsonReader.OOO\u3007O0(ShapeTrimPathParser.\u3007080);
            if (ooo\u3007O0 != 0) {
                if (ooo\u3007O0 != 1) {
                    if (ooo\u3007O0 != 2) {
                        if (ooo\u3007O0 != 3) {
                            if (ooo\u3007O0 != 4) {
                                if (ooo\u3007O0 != 5) {
                                    jsonReader.\u300700\u30078();
                                }
                                else {
                                    \u3007\u3007808\u3007 = jsonReader.\u3007\u3007808\u3007();
                                }
                            }
                            else {
                                forId = ShapeTrimPath.Type.forId(jsonReader.\u3007O00());
                            }
                        }
                        else {
                            o800o8O = jsonReader.o800o8O();
                        }
                    }
                    else {
                        o\u30073 = AnimatableValueParser.o\u30070(jsonReader, lottieComposition, false);
                    }
                }
                else {
                    o\u30072 = AnimatableValueParser.o\u30070(jsonReader, lottieComposition, false);
                }
            }
            else {
                o\u30070 = AnimatableValueParser.o\u30070(jsonReader, lottieComposition, false);
            }
        }
        return new ShapeTrimPath(o800o8O, (ShapeTrimPath.Type)forId, o\u30070, o\u30072, o\u30073, \u3007\u3007808\u3007);
    }
}

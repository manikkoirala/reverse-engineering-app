// 
// Decompiled by Procyon v0.6.0
// 

package com.airbnb.lottie.parser;

import java.io.IOException;
import com.airbnb.lottie.model.animatable.AnimatableTransform;
import com.airbnb.lottie.model.animatable.AnimatableFloatValue;
import com.airbnb.lottie.model.content.Repeater;
import com.airbnb.lottie.LottieComposition;
import com.airbnb.lottie.parser.moshi.JsonReader;

class RepeaterParser
{
    private static JsonReader.Options \u3007080;
    
    static {
        RepeaterParser.\u3007080 = JsonReader.Options.\u3007080("nm", "c", "o", "tr", "hd");
    }
    
    static Repeater \u3007080(final JsonReader jsonReader, final LottieComposition lottieComposition) throws IOException {
        String o800o8O = null;
        Object o\u30070 = null;
        Object \u3007\u3007888;
        AnimatableFloatValue o\u30072 = (AnimatableFloatValue)(\u3007\u3007888 = o\u30070);
        boolean \u3007\u3007808\u3007 = false;
        while (jsonReader.OO0o\u3007\u3007()) {
            final int ooo\u3007O0 = jsonReader.OOO\u3007O0(RepeaterParser.\u3007080);
            if (ooo\u3007O0 != 0) {
                if (ooo\u3007O0 != 1) {
                    if (ooo\u3007O0 != 2) {
                        if (ooo\u3007O0 != 3) {
                            if (ooo\u3007O0 != 4) {
                                jsonReader.\u300700\u30078();
                            }
                            else {
                                \u3007\u3007808\u3007 = jsonReader.\u3007\u3007808\u3007();
                            }
                        }
                        else {
                            \u3007\u3007888 = AnimatableTransformParser.\u3007\u3007888(jsonReader, lottieComposition);
                        }
                    }
                    else {
                        o\u30072 = AnimatableValueParser.o\u30070(jsonReader, lottieComposition, false);
                    }
                }
                else {
                    o\u30070 = AnimatableValueParser.o\u30070(jsonReader, lottieComposition, false);
                }
            }
            else {
                o800o8O = jsonReader.o800o8O();
            }
        }
        return new Repeater(o800o8O, (AnimatableFloatValue)o\u30070, o\u30072, (AnimatableTransform)\u3007\u3007888, \u3007\u3007808\u3007);
    }
}

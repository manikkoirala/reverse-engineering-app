// 
// Decompiled by Procyon v0.6.0
// 

package com.airbnb.lottie.parser;

import java.io.IOException;
import com.airbnb.lottie.model.animatable.AnimatableFloatValue;
import com.airbnb.lottie.model.animatable.AnimatablePointValue;
import com.airbnb.lottie.model.animatable.AnimatableIntegerValue;
import com.airbnb.lottie.model.animatable.AnimatableGradientColorValue;
import com.airbnb.lottie.model.content.GradientType;
import android.graphics.Path$FillType;
import com.airbnb.lottie.model.content.GradientFill;
import com.airbnb.lottie.LottieComposition;
import com.airbnb.lottie.parser.moshi.JsonReader;

class GradientFillParser
{
    private static final JsonReader.Options \u3007080;
    private static final JsonReader.Options \u3007o00\u3007\u3007Oo;
    
    static {
        \u3007080 = JsonReader.Options.\u3007080("nm", "g", "o", "t", "s", "e", "r", "hd");
        \u3007o00\u3007\u3007Oo = JsonReader.Options.\u3007080("p", "k");
    }
    
    static GradientFill \u3007080(final JsonReader jsonReader, final LottieComposition lottieComposition) throws IOException {
        Path$FillType winding = Path$FillType.WINDING;
        String o800o8O = null;
        AnimatablePointValue animatablePointValue = null;
        Object oo80;
        final GradientType gradientType = (GradientType)(oo80 = animatablePointValue);
        Object \u300780\u3007808\u3007O2;
        Object \u300780\u3007808\u3007O = \u300780\u3007808\u3007O2 = oo80;
        boolean \u3007\u3007808\u3007 = false;
        Object \u3007\u3007888 = gradientType;
        while (jsonReader.OO0o\u3007\u3007()) {
            switch (jsonReader.OOO\u3007O0(GradientFillParser.\u3007080)) {
                default: {
                    jsonReader.O8\u3007o();
                    jsonReader.\u300700\u30078();
                    continue;
                }
                case 7: {
                    \u3007\u3007808\u3007 = jsonReader.\u3007\u3007808\u3007();
                    continue;
                }
                case 6: {
                    Path$FillType path$FillType;
                    if (jsonReader.\u3007O00() == 1) {
                        path$FillType = Path$FillType.WINDING;
                    }
                    else {
                        path$FillType = Path$FillType.EVEN_ODD;
                    }
                    winding = path$FillType;
                    continue;
                }
                case 5: {
                    \u300780\u3007808\u3007O2 = AnimatableValueParser.\u300780\u3007808\u3007O(jsonReader, lottieComposition);
                    continue;
                }
                case 4: {
                    \u300780\u3007808\u3007O = AnimatableValueParser.\u300780\u3007808\u3007O(jsonReader, lottieComposition);
                    continue;
                }
                case 3: {
                    GradientType gradientType2;
                    if (jsonReader.\u3007O00() == 1) {
                        gradientType2 = GradientType.LINEAR;
                    }
                    else {
                        gradientType2 = GradientType.RADIAL;
                    }
                    animatablePointValue = (AnimatablePointValue)gradientType2;
                    continue;
                }
                case 2: {
                    oo80 = AnimatableValueParser.oO80(jsonReader, lottieComposition);
                    continue;
                }
                case 1: {
                    jsonReader.oO80();
                    int \u3007o00 = -1;
                    while (jsonReader.OO0o\u3007\u3007()) {
                        final int ooo\u3007O0 = jsonReader.OOO\u3007O0(GradientFillParser.\u3007o00\u3007\u3007Oo);
                        if (ooo\u3007O0 != 0) {
                            if (ooo\u3007O0 != 1) {
                                jsonReader.O8\u3007o();
                                jsonReader.\u300700\u30078();
                            }
                            else {
                                \u3007\u3007888 = AnimatableValueParser.\u3007\u3007888(jsonReader, lottieComposition, \u3007o00);
                            }
                        }
                        else {
                            \u3007o00 = jsonReader.\u3007O00();
                        }
                    }
                    jsonReader.\u3007O8o08O();
                    continue;
                }
                case 0: {
                    o800o8O = jsonReader.o800o8O();
                    continue;
                }
            }
        }
        return new GradientFill(o800o8O, (GradientType)animatablePointValue, winding, (AnimatableGradientColorValue)\u3007\u3007888, (AnimatableIntegerValue)oo80, (AnimatablePointValue)\u300780\u3007808\u3007O, (AnimatablePointValue)\u300780\u3007808\u3007O2, null, null, \u3007\u3007808\u3007);
    }
}

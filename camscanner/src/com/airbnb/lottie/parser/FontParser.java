// 
// Decompiled by Procyon v0.6.0
// 

package com.airbnb.lottie.parser;

import java.io.IOException;
import com.airbnb.lottie.model.Font;
import com.airbnb.lottie.parser.moshi.JsonReader;

class FontParser
{
    private static final JsonReader.Options \u3007080;
    
    static {
        \u3007080 = JsonReader.Options.\u3007080("fFamily", "fName", "fStyle", "ascent");
    }
    
    static Font \u3007080(final JsonReader jsonReader) throws IOException {
        jsonReader.oO80();
        String o800o8O = null;
        String o800o8O2 = null;
        String o800o8O3 = null;
        float n = 0.0f;
        while (jsonReader.OO0o\u3007\u3007()) {
            final int ooo\u3007O0 = jsonReader.OOO\u3007O0(FontParser.\u3007080);
            if (ooo\u3007O0 != 0) {
                if (ooo\u3007O0 != 1) {
                    if (ooo\u3007O0 != 2) {
                        if (ooo\u3007O0 != 3) {
                            jsonReader.O8\u3007o();
                            jsonReader.\u300700\u30078();
                        }
                        else {
                            n = (float)jsonReader.\u3007O\u3007();
                        }
                    }
                    else {
                        o800o8O3 = jsonReader.o800o8O();
                    }
                }
                else {
                    o800o8O2 = jsonReader.o800o8O();
                }
            }
            else {
                o800o8O = jsonReader.o800o8O();
            }
        }
        jsonReader.\u3007O8o08O();
        return new Font(o800o8O, o800o8O2, o800o8O3, n);
    }
}

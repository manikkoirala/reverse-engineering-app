// 
// Decompiled by Procyon v0.6.0
// 

package com.airbnb.lottie.parser;

import java.io.IOException;
import com.airbnb.lottie.model.animatable.AnimatableShapeValue;
import com.airbnb.lottie.model.content.ShapePath;
import com.airbnb.lottie.LottieComposition;
import com.airbnb.lottie.parser.moshi.JsonReader;

class ShapePathParser
{
    static JsonReader.Options \u3007080;
    
    static {
        ShapePathParser.\u3007080 = JsonReader.Options.\u3007080("nm", "ind", "ks", "hd");
    }
    
    static ShapePath \u3007080(final JsonReader jsonReader, final LottieComposition lottieComposition) throws IOException {
        String o800o8O = null;
        AnimatableShapeValue \u30078o8o\u3007 = null;
        int \u3007o00 = 0;
        boolean \u3007\u3007808\u3007 = false;
        while (jsonReader.OO0o\u3007\u3007()) {
            final int ooo\u3007O0 = jsonReader.OOO\u3007O0(ShapePathParser.\u3007080);
            if (ooo\u3007O0 != 0) {
                if (ooo\u3007O0 != 1) {
                    if (ooo\u3007O0 != 2) {
                        if (ooo\u3007O0 != 3) {
                            jsonReader.\u300700\u30078();
                        }
                        else {
                            \u3007\u3007808\u3007 = jsonReader.\u3007\u3007808\u3007();
                        }
                    }
                    else {
                        \u30078o8o\u3007 = AnimatableValueParser.\u30078o8o\u3007(jsonReader, lottieComposition);
                    }
                }
                else {
                    \u3007o00 = jsonReader.\u3007O00();
                }
            }
            else {
                o800o8O = jsonReader.o800o8O();
            }
        }
        return new ShapePath(o800o8O, \u3007o00, \u30078o8o\u3007, \u3007\u3007808\u3007);
    }
}

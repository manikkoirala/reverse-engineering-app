// 
// Decompiled by Procyon v0.6.0
// 

package com.airbnb.lottie.parser;

import java.io.IOException;
import android.graphics.PointF;
import com.airbnb.lottie.model.animatable.AnimatableValue;
import com.airbnb.lottie.model.animatable.AnimatablePointValue;
import com.airbnb.lottie.model.content.CircleShape;
import com.airbnb.lottie.LottieComposition;
import com.airbnb.lottie.parser.moshi.JsonReader;

class CircleShapeParser
{
    private static JsonReader.Options \u3007080;
    
    static {
        CircleShapeParser.\u3007080 = JsonReader.Options.\u3007080("nm", "p", "s", "hd", "d");
    }
    
    static CircleShape \u3007080(final JsonReader jsonReader, final LottieComposition lottieComposition, int ooo\u3007O0) throws IOException {
        boolean b = ooo\u3007O0 == 3;
        String o800o8O = null;
        AnimatablePointValue \u300780\u3007808\u3007O;
        AnimatableValue<PointF, PointF> \u3007o00\u3007\u3007Oo = \u300780\u3007808\u3007O = null;
        boolean \u3007\u3007808\u3007 = false;
        while (jsonReader.OO0o\u3007\u3007()) {
            ooo\u3007O0 = jsonReader.OOO\u3007O0(CircleShapeParser.\u3007080);
            if (ooo\u3007O0 != 0) {
                if (ooo\u3007O0 != 1) {
                    if (ooo\u3007O0 != 2) {
                        if (ooo\u3007O0 != 3) {
                            if (ooo\u3007O0 != 4) {
                                jsonReader.O8\u3007o();
                                jsonReader.\u300700\u30078();
                            }
                            else {
                                b = (jsonReader.\u3007O00() == 3);
                            }
                        }
                        else {
                            \u3007\u3007808\u3007 = jsonReader.\u3007\u3007808\u3007();
                        }
                    }
                    else {
                        \u300780\u3007808\u3007O = AnimatableValueParser.\u300780\u3007808\u3007O(jsonReader, lottieComposition);
                    }
                }
                else {
                    \u3007o00\u3007\u3007Oo = AnimatablePathValueParser.\u3007o00\u3007\u3007Oo(jsonReader, lottieComposition);
                }
            }
            else {
                o800o8O = jsonReader.o800o8O();
            }
        }
        return new CircleShape(o800o8O, \u3007o00\u3007\u3007Oo, \u300780\u3007808\u3007O, b, \u3007\u3007808\u3007);
    }
}

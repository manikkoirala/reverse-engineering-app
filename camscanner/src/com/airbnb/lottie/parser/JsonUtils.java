// 
// Decompiled by Procyon v0.6.0
// 

package com.airbnb.lottie.parser;

import java.util.ArrayList;
import java.util.List;
import android.graphics.PointF;
import androidx.annotation.ColorInt;
import java.io.IOException;
import android.graphics.Color;
import com.airbnb.lottie.parser.moshi.JsonReader;

class JsonUtils
{
    private static final JsonReader.Options \u3007080;
    
    static {
        \u3007080 = JsonReader.Options.\u3007080("x", "y");
    }
    
    @ColorInt
    static int O8(final JsonReader jsonReader) throws IOException {
        jsonReader.\u3007\u3007888();
        final int n = (int)(jsonReader.\u3007O\u3007() * 255.0);
        final int n2 = (int)(jsonReader.\u3007O\u3007() * 255.0);
        final int n3 = (int)(jsonReader.\u3007O\u3007() * 255.0);
        while (jsonReader.OO0o\u3007\u3007()) {
            jsonReader.\u300700\u30078();
        }
        jsonReader.\u30078o8o\u3007();
        return Color.argb(255, n, n2, n3);
    }
    
    static PointF Oo08(final JsonReader jsonReader, final float n) throws IOException {
        final int n2 = JsonUtils$1.\u3007080[jsonReader.O\u30078O8\u3007008().ordinal()];
        if (n2 == 1) {
            return \u3007o00\u3007\u3007Oo(jsonReader, n);
        }
        if (n2 == 2) {
            return \u3007080(jsonReader, n);
        }
        if (n2 == 3) {
            return \u3007o\u3007(jsonReader, n);
        }
        final StringBuilder sb = new StringBuilder();
        sb.append("Unknown point starts with ");
        sb.append(jsonReader.O\u30078O8\u3007008());
        throw new IllegalArgumentException(sb.toString());
    }
    
    static List<PointF> o\u30070(final JsonReader jsonReader, final float n) throws IOException {
        final ArrayList list = new ArrayList();
        jsonReader.\u3007\u3007888();
        while (jsonReader.O\u30078O8\u3007008() == JsonReader.Token.BEGIN_ARRAY) {
            jsonReader.\u3007\u3007888();
            list.add(Oo08(jsonReader, n));
            jsonReader.\u30078o8o\u3007();
        }
        jsonReader.\u30078o8o\u3007();
        return list;
    }
    
    private static PointF \u3007080(final JsonReader jsonReader, final float n) throws IOException {
        jsonReader.\u3007\u3007888();
        final float n2 = (float)jsonReader.\u3007O\u3007();
        final float n3 = (float)jsonReader.\u3007O\u3007();
        while (jsonReader.O\u30078O8\u3007008() != JsonReader.Token.END_ARRAY) {
            jsonReader.\u300700\u30078();
        }
        jsonReader.\u30078o8o\u3007();
        return new PointF(n2 * n, n3 * n);
    }
    
    private static PointF \u3007o00\u3007\u3007Oo(final JsonReader jsonReader, final float n) throws IOException {
        final float n2 = (float)jsonReader.\u3007O\u3007();
        final float n3 = (float)jsonReader.\u3007O\u3007();
        while (jsonReader.OO0o\u3007\u3007()) {
            jsonReader.\u300700\u30078();
        }
        return new PointF(n2 * n, n3 * n);
    }
    
    private static PointF \u3007o\u3007(final JsonReader jsonReader, final float n) throws IOException {
        jsonReader.oO80();
        float \u3007\u3007888 = 0.0f;
        float \u3007\u3007889 = 0.0f;
        while (jsonReader.OO0o\u3007\u3007()) {
            final int ooo\u3007O0 = jsonReader.OOO\u3007O0(JsonUtils.\u3007080);
            if (ooo\u3007O0 != 0) {
                if (ooo\u3007O0 != 1) {
                    jsonReader.O8\u3007o();
                    jsonReader.\u300700\u30078();
                }
                else {
                    \u3007\u3007889 = \u3007\u3007888(jsonReader);
                }
            }
            else {
                \u3007\u3007888 = \u3007\u3007888(jsonReader);
            }
        }
        jsonReader.\u3007O8o08O();
        return new PointF(\u3007\u3007888 * n, \u3007\u3007889 * n);
    }
    
    static float \u3007\u3007888(final JsonReader jsonReader) throws IOException {
        final JsonReader.Token o\u30078O8\u3007008 = jsonReader.O\u30078O8\u3007008();
        final int n = JsonUtils$1.\u3007080[o\u30078O8\u3007008.ordinal()];
        if (n == 1) {
            return (float)jsonReader.\u3007O\u3007();
        }
        if (n == 2) {
            jsonReader.\u3007\u3007888();
            final float n2 = (float)jsonReader.\u3007O\u3007();
            while (jsonReader.OO0o\u3007\u3007()) {
                jsonReader.\u300700\u30078();
            }
            jsonReader.\u30078o8o\u3007();
            return n2;
        }
        final StringBuilder sb = new StringBuilder();
        sb.append("Unknown value for token of type ");
        sb.append(o\u30078O8\u3007008);
        throw new IllegalArgumentException(sb.toString());
    }
}

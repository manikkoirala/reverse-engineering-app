// 
// Decompiled by Procyon v0.6.0
// 

package com.airbnb.lottie.parser;

import java.io.IOException;
import com.airbnb.lottie.model.animatable.AnimatableIntegerValue;
import android.view.animation.Interpolator;
import com.airbnb.lottie.model.animatable.AnimatableTransform;
import com.airbnb.lottie.LottieComposition;
import com.airbnb.lottie.model.animatable.AnimatableSplitDimensionPathValue;
import com.airbnb.lottie.model.animatable.AnimatableValue;
import android.graphics.PointF;
import com.airbnb.lottie.model.animatable.AnimatablePathValue;
import com.airbnb.lottie.model.animatable.AnimatableFloatValue;
import com.airbnb.lottie.value.Keyframe;
import com.airbnb.lottie.value.ScaleXY;
import com.airbnb.lottie.model.animatable.AnimatableScaleValue;
import com.airbnb.lottie.parser.moshi.JsonReader;

public class AnimatableTransformParser
{
    private static JsonReader.Options \u3007080;
    private static JsonReader.Options \u3007o00\u3007\u3007Oo;
    
    static {
        AnimatableTransformParser.\u3007080 = JsonReader.Options.\u3007080("a", "p", "s", "rz", "r", "o", "so", "eo", "sk", "sa");
        AnimatableTransformParser.\u3007o00\u3007\u3007Oo = JsonReader.Options.\u3007080("k");
    }
    
    private static boolean O8(final AnimatableScaleValue animatableScaleValue) {
        if (animatableScaleValue != null) {
            final boolean \u3007o\u3007 = animatableScaleValue.\u3007o\u3007();
            boolean b = false;
            if (!\u3007o\u3007) {
                return b;
            }
            b = b;
            if (!((ScaleXY)animatableScaleValue.\u3007o00\u3007\u3007Oo().get(0).\u3007o00\u3007\u3007Oo).\u3007080(1.0f, 1.0f)) {
                return b;
            }
        }
        return true;
    }
    
    private static boolean Oo08(final AnimatableFloatValue animatableFloatValue) {
        if (animatableFloatValue != null) {
            final boolean \u3007o\u3007 = animatableFloatValue.\u3007o\u3007();
            boolean b = false;
            if (!\u3007o\u3007) {
                return b;
            }
            b = b;
            if ((float)animatableFloatValue.\u3007o00\u3007\u3007Oo().get(0).\u3007o00\u3007\u3007Oo != 0.0f) {
                return b;
            }
        }
        return true;
    }
    
    private static boolean o\u30070(final AnimatableFloatValue animatableFloatValue) {
        if (animatableFloatValue != null) {
            final boolean \u3007o\u3007 = animatableFloatValue.\u3007o\u3007();
            boolean b = false;
            if (!\u3007o\u3007) {
                return b;
            }
            b = b;
            if ((float)animatableFloatValue.\u3007o00\u3007\u3007Oo().get(0).\u3007o00\u3007\u3007Oo != 0.0f) {
                return b;
            }
        }
        return true;
    }
    
    private static boolean \u3007080(final AnimatablePathValue animatablePathValue) {
        if (animatablePathValue != null) {
            final boolean \u3007o\u3007 = animatablePathValue.\u3007o\u3007();
            boolean b = false;
            if (!\u3007o\u3007) {
                return b;
            }
            b = b;
            if (!((PointF)animatablePathValue.\u3007o00\u3007\u3007Oo().get(0).\u3007o00\u3007\u3007Oo).equals(0.0f, 0.0f)) {
                return b;
            }
        }
        return true;
    }
    
    private static boolean \u3007o00\u3007\u3007Oo(final AnimatableValue<PointF, PointF> animatableValue) {
        if (animatableValue != null) {
            final boolean b = animatableValue instanceof AnimatableSplitDimensionPathValue;
            boolean b3;
            final boolean b2 = b3 = false;
            if (b) {
                return b3;
            }
            b3 = b2;
            if (!animatableValue.\u3007o\u3007()) {
                return b3;
            }
            b3 = b2;
            if (!((PointF)((Keyframe)animatableValue.\u3007o00\u3007\u3007Oo().get(0)).\u3007o00\u3007\u3007Oo).equals(0.0f, 0.0f)) {
                return b3;
            }
        }
        return true;
    }
    
    private static boolean \u3007o\u3007(final AnimatableFloatValue animatableFloatValue) {
        if (animatableFloatValue != null) {
            final boolean \u3007o\u3007 = animatableFloatValue.\u3007o\u3007();
            boolean b = false;
            if (!\u3007o\u3007) {
                return b;
            }
            b = b;
            if ((float)animatableFloatValue.\u3007o00\u3007\u3007Oo().get(0).\u3007o00\u3007\u3007Oo != 0.0f) {
                return b;
            }
        }
        return true;
    }
    
    public static AnimatableTransform \u3007\u3007888(final JsonReader jsonReader, final LottieComposition lottieComposition) throws IOException {
        final boolean b = jsonReader.O\u30078O8\u3007008() == JsonReader.Token.BEGIN_OBJECT;
        if (b) {
            jsonReader.oO80();
        }
        AnimatableFloatValue o\u30070 = null;
        AnimatablePathValue \u3007080 = null;
        AnimatableValue<PointF, PointF> \u3007o00\u3007\u3007Oo = null;
        AnimatableScaleValue oo0o\u3007\u3007\u3007\u30070 = null;
        AnimatableFloatValue o\u30072 = null;
        AnimatableFloatValue o\u30073 = null;
        AnimatableIntegerValue oo80 = null;
        AnimatableFloatValue o\u30074 = null;
        AnimatableFloatValue o\u30075 = null;
        while (jsonReader.OO0o\u3007\u3007()) {
            switch (jsonReader.OOO\u3007O0(AnimatableTransformParser.\u3007080)) {
                default: {
                    jsonReader.O8\u3007o();
                    jsonReader.\u300700\u30078();
                    continue;
                }
                case 9: {
                    o\u30073 = AnimatableValueParser.o\u30070(jsonReader, lottieComposition, false);
                    continue;
                }
                case 8: {
                    o\u30072 = AnimatableValueParser.o\u30070(jsonReader, lottieComposition, false);
                    continue;
                }
                case 7: {
                    o\u30075 = AnimatableValueParser.o\u30070(jsonReader, lottieComposition, false);
                    continue;
                }
                case 6: {
                    o\u30074 = AnimatableValueParser.o\u30070(jsonReader, lottieComposition, false);
                    continue;
                }
                case 5: {
                    oo80 = AnimatableValueParser.oO80(jsonReader, lottieComposition);
                    continue;
                }
                case 3: {
                    lottieComposition.\u3007080("Lottie doesn't support 3D layers.");
                }
                case 4: {
                    o\u30070 = AnimatableValueParser.o\u30070(jsonReader, lottieComposition, false);
                    if (o\u30070.\u3007o00\u3007\u3007Oo().isEmpty()) {
                        o\u30070.\u3007o00\u3007\u3007Oo().add(new Keyframe(lottieComposition, 0.0f, 0.0f, null, 0.0f, lottieComposition.o\u30070()));
                    }
                    else {
                        if (((Keyframe)o\u30070.\u3007o00\u3007\u3007Oo().get(0)).\u3007o00\u3007\u3007Oo != null) {
                            continue;
                        }
                        o\u30070.\u3007o00\u3007\u3007Oo().set(0, new Keyframe(lottieComposition, 0.0f, 0.0f, null, 0.0f, lottieComposition.o\u30070()));
                    }
                    continue;
                }
                case 2: {
                    oo0o\u3007\u3007\u3007\u30070 = AnimatableValueParser.OO0o\u3007\u3007\u3007\u30070(jsonReader, lottieComposition);
                    continue;
                }
                case 1: {
                    \u3007o00\u3007\u3007Oo = AnimatablePathValueParser.\u3007o00\u3007\u3007Oo(jsonReader, lottieComposition);
                    continue;
                }
                case 0: {
                    jsonReader.oO80();
                    while (jsonReader.OO0o\u3007\u3007()) {
                        if (jsonReader.OOO\u3007O0(AnimatableTransformParser.\u3007o00\u3007\u3007Oo) != 0) {
                            jsonReader.O8\u3007o();
                            jsonReader.\u300700\u30078();
                        }
                        else {
                            \u3007080 = AnimatablePathValueParser.\u3007080(jsonReader, lottieComposition);
                        }
                    }
                    jsonReader.\u3007O8o08O();
                    continue;
                }
            }
        }
        if (b) {
            jsonReader.\u3007O8o08O();
        }
        AnimatablePathValue animatablePathValue;
        if (\u3007080(\u3007080)) {
            animatablePathValue = null;
        }
        else {
            animatablePathValue = \u3007080;
        }
        AnimatableValue<PointF, PointF> animatableValue;
        if (\u3007o00\u3007\u3007Oo(\u3007o00\u3007\u3007Oo)) {
            animatableValue = null;
        }
        else {
            animatableValue = \u3007o00\u3007\u3007Oo;
        }
        AnimatableFloatValue animatableFloatValue;
        if (\u3007o\u3007(o\u30070)) {
            animatableFloatValue = null;
        }
        else {
            animatableFloatValue = o\u30070;
        }
        AnimatableScaleValue animatableScaleValue = oo0o\u3007\u3007\u3007\u30070;
        if (O8(oo0o\u3007\u3007\u3007\u30070)) {
            animatableScaleValue = null;
        }
        if (o\u30070(o\u30072)) {
            o\u30072 = null;
        }
        if (Oo08(o\u30073)) {
            o\u30073 = null;
        }
        return new AnimatableTransform(animatablePathValue, animatableValue, animatableScaleValue, animatableFloatValue, oo80, o\u30074, o\u30075, o\u30072, o\u30073);
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.airbnb.lottie.parser;

import java.io.IOException;
import com.airbnb.lottie.model.animatable.AnimatableFloatValue;
import android.graphics.PointF;
import com.airbnb.lottie.model.animatable.AnimatableValue;
import com.airbnb.lottie.model.content.PolystarShape;
import com.airbnb.lottie.LottieComposition;
import com.airbnb.lottie.parser.moshi.JsonReader;

class PolystarShapeParser
{
    private static final JsonReader.Options \u3007080;
    
    static {
        \u3007080 = JsonReader.Options.\u3007080("nm", "sy", "pt", "p", "r", "or", "os", "ir", "is", "hd");
    }
    
    static PolystarShape \u3007080(final JsonReader jsonReader, final LottieComposition lottieComposition) throws IOException {
        String o800o8O = null;
        AnimatableFloatValue o\u30070;
        Object forValue = o\u30070 = null;
        AnimatableFloatValue o\u30072;
        Object \u3007o00\u3007\u3007Oo = o\u30072 = o\u30070;
        AnimatableFloatValue oo9;
        AnimatableFloatValue oo08 = oo9 = o\u30072;
        AnimatableFloatValue o\u30074;
        AnimatableFloatValue o\u30073 = o\u30074 = oo9;
        boolean \u3007\u3007808\u3007 = false;
        while (jsonReader.OO0o\u3007\u3007()) {
            switch (jsonReader.OOO\u3007O0(PolystarShapeParser.\u3007080)) {
                default: {
                    jsonReader.O8\u3007o();
                    jsonReader.\u300700\u30078();
                    continue;
                }
                case 9: {
                    \u3007\u3007808\u3007 = jsonReader.\u3007\u3007808\u3007();
                    continue;
                }
                case 8: {
                    o\u30073 = AnimatableValueParser.o\u30070(jsonReader, lottieComposition, false);
                    continue;
                }
                case 7: {
                    oo08 = AnimatableValueParser.Oo08(jsonReader, lottieComposition);
                    continue;
                }
                case 6: {
                    o\u30074 = AnimatableValueParser.o\u30070(jsonReader, lottieComposition, false);
                    continue;
                }
                case 5: {
                    oo9 = AnimatableValueParser.Oo08(jsonReader, lottieComposition);
                    continue;
                }
                case 4: {
                    o\u30072 = AnimatableValueParser.o\u30070(jsonReader, lottieComposition, false);
                    continue;
                }
                case 3: {
                    \u3007o00\u3007\u3007Oo = AnimatablePathValueParser.\u3007o00\u3007\u3007Oo(jsonReader, lottieComposition);
                    continue;
                }
                case 2: {
                    o\u30070 = AnimatableValueParser.o\u30070(jsonReader, lottieComposition, false);
                    continue;
                }
                case 1: {
                    forValue = PolystarShape.Type.forValue(jsonReader.\u3007O00());
                    continue;
                }
                case 0: {
                    o800o8O = jsonReader.o800o8O();
                    continue;
                }
            }
        }
        return new PolystarShape(o800o8O, (PolystarShape.Type)forValue, o\u30070, (AnimatableValue<PointF, PointF>)\u3007o00\u3007\u3007Oo, o\u30072, oo08, oo9, o\u30073, o\u30074, \u3007\u3007808\u3007);
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.airbnb.lottie.parser;

import com.airbnb.lottie.animation.keyframe.PathKeyframe;
import java.io.IOException;
import java.util.ArrayList;
import com.airbnb.lottie.value.Keyframe;
import java.util.List;
import com.airbnb.lottie.LottieComposition;
import com.airbnb.lottie.parser.moshi.JsonReader;

class KeyframesParser
{
    static JsonReader.Options \u3007080;
    
    static {
        KeyframesParser.\u3007080 = JsonReader.Options.\u3007080("k");
    }
    
    static <T> List<Keyframe<T>> \u3007080(final JsonReader jsonReader, final LottieComposition lottieComposition, final float n, final ValueParser<T> valueParser) throws IOException {
        final ArrayList list = new ArrayList();
        if (jsonReader.O\u30078O8\u3007008() == JsonReader.Token.STRING) {
            lottieComposition.\u3007080("Lottie doesn't support expressions.");
            return list;
        }
        jsonReader.oO80();
        while (jsonReader.OO0o\u3007\u3007()) {
            if (jsonReader.OOO\u3007O0(KeyframesParser.\u3007080) != 0) {
                jsonReader.\u300700\u30078();
            }
            else if (jsonReader.O\u30078O8\u3007008() == JsonReader.Token.BEGIN_ARRAY) {
                jsonReader.\u3007\u3007888();
                if (jsonReader.O\u30078O8\u3007008() == JsonReader.Token.NUMBER) {
                    list.add(KeyframeParser.\u3007o00\u3007\u3007Oo(jsonReader, lottieComposition, n, valueParser, false));
                }
                else {
                    while (jsonReader.OO0o\u3007\u3007()) {
                        list.add(KeyframeParser.\u3007o00\u3007\u3007Oo(jsonReader, lottieComposition, n, valueParser, true));
                    }
                }
                jsonReader.\u30078o8o\u3007();
            }
            else {
                list.add(KeyframeParser.\u3007o00\u3007\u3007Oo(jsonReader, lottieComposition, n, valueParser, false));
            }
        }
        jsonReader.\u3007O8o08O();
        \u3007o00\u3007\u3007Oo((List<? extends Keyframe<Object>>)list);
        return list;
    }
    
    public static <T> void \u3007o00\u3007\u3007Oo(final List<? extends Keyframe<T>> list) {
        final int size = list.size();
        int n = 0;
        int n2;
        while (true) {
            n2 = size - 1;
            if (n >= n2) {
                break;
            }
            final Keyframe keyframe = list.get(n);
            final int n3 = n + 1;
            final Keyframe keyframe2 = list.get(n3);
            keyframe.o\u30070 = keyframe2.Oo08;
            n = n3;
            if (keyframe.\u3007o\u3007 != null) {
                continue;
            }
            final T \u3007o00\u3007\u3007Oo = keyframe2.\u3007o00\u3007\u3007Oo;
            n = n3;
            if (\u3007o00\u3007\u3007Oo == null) {
                continue;
            }
            keyframe.\u3007o\u3007 = \u3007o00\u3007\u3007Oo;
            n = n3;
            if (!(keyframe instanceof PathKeyframe)) {
                continue;
            }
            ((PathKeyframe)keyframe).\u300780\u3007808\u3007O();
            n = n3;
        }
        final Keyframe keyframe3 = list.get(n2);
        if ((keyframe3.\u3007o00\u3007\u3007Oo == null || keyframe3.\u3007o\u3007 == null) && list.size() > 1) {
            list.remove(keyframe3);
        }
    }
}

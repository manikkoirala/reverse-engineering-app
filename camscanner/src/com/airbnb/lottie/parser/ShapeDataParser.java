// 
// Decompiled by Procyon v0.6.0
// 

package com.airbnb.lottie.parser;

import java.util.List;
import com.airbnb.lottie.model.CubicCurveData;
import com.airbnb.lottie.utils.MiscUtils;
import java.util.ArrayList;
import java.util.Collections;
import android.graphics.PointF;
import java.io.IOException;
import com.airbnb.lottie.parser.moshi.JsonReader;
import com.airbnb.lottie.model.content.ShapeData;

public class ShapeDataParser implements ValueParser<ShapeData>
{
    public static final ShapeDataParser \u3007080;
    private static final JsonReader.Options \u3007o00\u3007\u3007Oo;
    
    static {
        \u3007080 = new ShapeDataParser();
        \u3007o00\u3007\u3007Oo = JsonReader.Options.\u3007080("c", "v", "i", "o");
    }
    
    private ShapeDataParser() {
    }
    
    public ShapeData \u3007o00\u3007\u3007Oo(final JsonReader jsonReader, final float n) throws IOException {
        if (jsonReader.O\u30078O8\u3007008() == JsonReader.Token.BEGIN_ARRAY) {
            jsonReader.\u3007\u3007888();
        }
        jsonReader.oO80();
        List<PointF> o\u30070 = null;
        List<PointF> o\u30072 = null;
        List<PointF> o\u30073 = null;
        boolean \u3007\u3007808\u3007 = false;
        while (jsonReader.OO0o\u3007\u3007()) {
            final int ooo\u3007O0 = jsonReader.OOO\u3007O0(ShapeDataParser.\u3007o00\u3007\u3007Oo);
            if (ooo\u3007O0 != 0) {
                if (ooo\u3007O0 != 1) {
                    if (ooo\u3007O0 != 2) {
                        if (ooo\u3007O0 != 3) {
                            jsonReader.O8\u3007o();
                            jsonReader.\u300700\u30078();
                        }
                        else {
                            o\u30073 = JsonUtils.o\u30070(jsonReader, n);
                        }
                    }
                    else {
                        o\u30072 = JsonUtils.o\u30070(jsonReader, n);
                    }
                }
                else {
                    o\u30070 = JsonUtils.o\u30070(jsonReader, n);
                }
            }
            else {
                \u3007\u3007808\u3007 = jsonReader.\u3007\u3007808\u3007();
            }
        }
        jsonReader.\u3007O8o08O();
        if (jsonReader.O\u30078O8\u3007008() == JsonReader.Token.END_ARRAY) {
            jsonReader.\u30078o8o\u3007();
        }
        if (o\u30070 == null || o\u30072 == null || o\u30073 == null) {
            throw new IllegalArgumentException("Shape data was missing information.");
        }
        if (o\u30070.isEmpty()) {
            return new ShapeData(new PointF(), false, Collections.emptyList());
        }
        final int size = o\u30070.size();
        final PointF pointF = o\u30070.get(0);
        final ArrayList list = new ArrayList(size);
        for (int i = 1; i < size; ++i) {
            final PointF pointF2 = o\u30070.get(i);
            final int n2 = i - 1;
            list.add((Object)new CubicCurveData(MiscUtils.\u3007080(o\u30070.get(n2), o\u30073.get(n2)), MiscUtils.\u3007080(pointF2, o\u30072.get(i)), pointF2));
        }
        if (\u3007\u3007808\u3007) {
            final PointF pointF3 = o\u30070.get(0);
            final int n3 = size - 1;
            list.add((Object)new CubicCurveData(MiscUtils.\u3007080(o\u30070.get(n3), o\u30073.get(n3)), MiscUtils.\u3007080(pointF3, o\u30072.get(0)), pointF3));
        }
        return new ShapeData(pointF, \u3007\u3007808\u3007, (List<CubicCurveData>)list);
    }
}

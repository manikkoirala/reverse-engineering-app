// 
// Decompiled by Procyon v0.6.0
// 

package com.airbnb.lottie.parser;

import java.io.IOException;
import com.airbnb.lottie.model.animatable.AnimatableIntegerValue;
import com.airbnb.lottie.model.animatable.AnimatableShapeValue;
import com.airbnb.lottie.utils.Logger;
import com.airbnb.lottie.model.content.Mask;
import com.airbnb.lottie.LottieComposition;
import com.airbnb.lottie.parser.moshi.JsonReader;

class MaskParser
{
    static Mask \u3007080(final JsonReader jsonReader, final LottieComposition lottieComposition) throws IOException {
        jsonReader.oO80();
        Mask.MaskMode maskMode = null;
        AnimatableShapeValue \u30078o8o\u3007 = null;
        AnimatableIntegerValue oo80 = null;
        boolean \u3007\u3007808\u3007 = false;
        while (jsonReader.OO0o\u3007\u3007()) {
            final String \u3007\u30078O0\u30078 = jsonReader.\u3007\u30078O0\u30078();
            \u3007\u30078O0\u30078.hashCode();
            final int hashCode = \u3007\u30078O0\u30078.hashCode();
            final int n = 3;
            int n2 = 0;
            Label_0162: {
                switch (hashCode) {
                    case 3357091: {
                        if (!\u3007\u30078O0\u30078.equals("mode")) {
                            break;
                        }
                        n2 = 3;
                        break Label_0162;
                    }
                    case 104433: {
                        if (!\u3007\u30078O0\u30078.equals("inv")) {
                            break;
                        }
                        n2 = 2;
                        break Label_0162;
                    }
                    case 3588: {
                        if (!\u3007\u30078O0\u30078.equals("pt")) {
                            break;
                        }
                        n2 = 1;
                        break Label_0162;
                    }
                    case 111: {
                        if (!\u3007\u30078O0\u30078.equals("o")) {
                            break;
                        }
                        n2 = 0;
                        break Label_0162;
                    }
                }
                n2 = -1;
            }
            switch (n2) {
                default: {
                    jsonReader.\u300700\u30078();
                    continue;
                }
                case 3: {
                    final String o800o8O = jsonReader.o800o8O();
                    o800o8O.hashCode();
                    int n3 = 0;
                    Label_0331: {
                        switch (o800o8O.hashCode()) {
                            case 115: {
                                n3 = n;
                                if (!o800o8O.equals("s")) {
                                    break;
                                }
                                break Label_0331;
                            }
                            case 110: {
                                if (!o800o8O.equals("n")) {
                                    break;
                                }
                                n3 = 2;
                                break Label_0331;
                            }
                            case 105: {
                                if (!o800o8O.equals("i")) {
                                    break;
                                }
                                n3 = 1;
                                break Label_0331;
                            }
                            case 97: {
                                if (!o800o8O.equals("a")) {
                                    break;
                                }
                                n3 = 0;
                                break Label_0331;
                            }
                        }
                        n3 = -1;
                    }
                    switch (n3) {
                        default: {
                            final StringBuilder sb = new StringBuilder();
                            sb.append("Unknown mask mode ");
                            sb.append(\u3007\u30078O0\u30078);
                            sb.append(". Defaulting to Add.");
                            Logger.\u3007o\u3007(sb.toString());
                            maskMode = Mask.MaskMode.MASK_MODE_ADD;
                            continue;
                        }
                        case 3: {
                            maskMode = Mask.MaskMode.MASK_MODE_SUBTRACT;
                            continue;
                        }
                        case 2: {
                            maskMode = Mask.MaskMode.MASK_MODE_NONE;
                            continue;
                        }
                        case 1: {
                            lottieComposition.\u3007080("Animation contains intersect masks. They are not supported but will be treated like add masks.");
                            maskMode = Mask.MaskMode.MASK_MODE_INTERSECT;
                            continue;
                        }
                        case 0: {
                            maskMode = Mask.MaskMode.MASK_MODE_ADD;
                            continue;
                        }
                    }
                    break;
                }
                case 2: {
                    \u3007\u3007808\u3007 = jsonReader.\u3007\u3007808\u3007();
                    continue;
                }
                case 1: {
                    \u30078o8o\u3007 = AnimatableValueParser.\u30078o8o\u3007(jsonReader, lottieComposition);
                    continue;
                }
                case 0: {
                    oo80 = AnimatableValueParser.oO80(jsonReader, lottieComposition);
                    continue;
                }
            }
        }
        jsonReader.\u3007O8o08O();
        return new Mask(maskMode, \u30078o8o\u3007, oo80, \u3007\u3007808\u3007);
    }
}

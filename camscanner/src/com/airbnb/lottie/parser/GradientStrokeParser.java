// 
// Decompiled by Procyon v0.6.0
// 

package com.airbnb.lottie.parser;

import java.io.IOException;
import com.airbnb.lottie.model.animatable.AnimatablePointValue;
import com.airbnb.lottie.model.animatable.AnimatableIntegerValue;
import com.airbnb.lottie.model.animatable.AnimatableGradientColorValue;
import com.airbnb.lottie.model.animatable.AnimatableFloatValue;
import java.util.List;
import com.airbnb.lottie.model.content.GradientType;
import com.airbnb.lottie.model.content.ShapeStroke;
import java.util.ArrayList;
import com.airbnb.lottie.model.content.GradientStroke;
import com.airbnb.lottie.LottieComposition;
import com.airbnb.lottie.parser.moshi.JsonReader;

class GradientStrokeParser
{
    private static JsonReader.Options \u3007080;
    private static final JsonReader.Options \u3007o00\u3007\u3007Oo;
    private static final JsonReader.Options \u3007o\u3007;
    
    static {
        GradientStrokeParser.\u3007080 = JsonReader.Options.\u3007080("nm", "g", "o", "t", "s", "e", "w", "lc", "lj", "ml", "hd", "d");
        \u3007o00\u3007\u3007Oo = JsonReader.Options.\u3007080("p", "k");
        \u3007o\u3007 = JsonReader.Options.\u3007080("n", "v");
    }
    
    static GradientStroke \u3007080(final JsonReader jsonReader, final LottieComposition lottieComposition) throws IOException {
        final ArrayList list = new ArrayList();
        String o800o8O = null;
        GradientType gradientType = null;
        AnimatableGradientColorValue animatableGradientColorValue = null;
        AnimatableIntegerValue oo80 = null;
        AnimatablePointValue \u300780\u3007808\u3007O = null;
        AnimatablePointValue \u300780\u3007808\u3007O2 = null;
        AnimatableFloatValue oo81 = null;
        ShapeStroke.LineCapType lineCapType = null;
        ShapeStroke.LineJoinType lineJoinType = null;
        float n = 0.0f;
        AnimatableFloatValue animatableFloatValue = null;
        boolean \u3007\u3007808\u3007 = false;
        while (jsonReader.OO0o\u3007\u3007()) {
            AnimatableGradientColorValue \u3007\u3007888 = null;
            switch (jsonReader.OOO\u3007O0(GradientStrokeParser.\u3007080)) {
                default: {
                    jsonReader.O8\u3007o();
                    jsonReader.\u300700\u30078();
                    continue;
                }
                case 11: {
                    jsonReader.\u3007\u3007888();
                    AnimatableFloatValue animatableFloatValue2 = animatableFloatValue;
                    while (jsonReader.OO0o\u3007\u3007()) {
                        jsonReader.oO80();
                        String o800o8O2 = null;
                        AnimatableFloatValue oo82 = null;
                        while (jsonReader.OO0o\u3007\u3007()) {
                            final int ooo\u3007O0 = jsonReader.OOO\u3007O0(GradientStrokeParser.\u3007o\u3007);
                            if (ooo\u3007O0 != 0) {
                                if (ooo\u3007O0 != 1) {
                                    jsonReader.O8\u3007o();
                                    jsonReader.\u300700\u30078();
                                }
                                else {
                                    oo82 = AnimatableValueParser.Oo08(jsonReader, lottieComposition);
                                }
                            }
                            else {
                                o800o8O2 = jsonReader.o800o8O();
                            }
                        }
                        jsonReader.\u3007O8o08O();
                        if (o800o8O2.equals("o")) {
                            animatableFloatValue2 = oo82;
                        }
                        else {
                            if (!o800o8O2.equals("d") && !o800o8O2.equals("g")) {
                                continue;
                            }
                            lottieComposition.OoO8(true);
                            list.add(oo82);
                        }
                    }
                    jsonReader.\u30078o8o\u3007();
                    if (list.size() == 1) {
                        list.add(list.get(0));
                    }
                    animatableFloatValue = animatableFloatValue2;
                    continue;
                }
                case 10: {
                    \u3007\u3007808\u3007 = jsonReader.\u3007\u3007808\u3007();
                    continue;
                }
                case 9: {
                    n = (float)jsonReader.\u3007O\u3007();
                    \u3007\u3007888 = animatableGradientColorValue;
                    break;
                }
                case 8: {
                    lineJoinType = ShapeStroke.LineJoinType.values()[jsonReader.\u3007O00() - 1];
                    \u3007\u3007888 = animatableGradientColorValue;
                    break;
                }
                case 7: {
                    lineCapType = ShapeStroke.LineCapType.values()[jsonReader.\u3007O00() - 1];
                    \u3007\u3007888 = animatableGradientColorValue;
                    break;
                }
                case 6: {
                    oo81 = AnimatableValueParser.Oo08(jsonReader, lottieComposition);
                    continue;
                }
                case 5: {
                    \u300780\u3007808\u3007O2 = AnimatableValueParser.\u300780\u3007808\u3007O(jsonReader, lottieComposition);
                    continue;
                }
                case 4: {
                    \u300780\u3007808\u3007O = AnimatableValueParser.\u300780\u3007808\u3007O(jsonReader, lottieComposition);
                    continue;
                }
                case 3: {
                    GradientType gradientType2;
                    if (jsonReader.\u3007O00() == 1) {
                        gradientType2 = GradientType.LINEAR;
                    }
                    else {
                        gradientType2 = GradientType.RADIAL;
                    }
                    gradientType = gradientType2;
                    \u3007\u3007888 = animatableGradientColorValue;
                    break;
                }
                case 2: {
                    oo80 = AnimatableValueParser.oO80(jsonReader, lottieComposition);
                    continue;
                }
                case 1: {
                    jsonReader.oO80();
                    int \u3007o00 = -1;
                    \u3007\u3007888 = animatableGradientColorValue;
                    while (jsonReader.OO0o\u3007\u3007()) {
                        final int ooo\u3007O2 = jsonReader.OOO\u3007O0(GradientStrokeParser.\u3007o00\u3007\u3007Oo);
                        if (ooo\u3007O2 != 0) {
                            if (ooo\u3007O2 != 1) {
                                jsonReader.O8\u3007o();
                                jsonReader.\u300700\u30078();
                            }
                            else {
                                \u3007\u3007888 = AnimatableValueParser.\u3007\u3007888(jsonReader, lottieComposition, \u3007o00);
                            }
                        }
                        else {
                            \u3007o00 = jsonReader.\u3007O00();
                        }
                    }
                    jsonReader.\u3007O8o08O();
                    break;
                }
                case 0: {
                    o800o8O = jsonReader.o800o8O();
                    continue;
                }
            }
            animatableGradientColorValue = \u3007\u3007888;
        }
        return new GradientStroke(o800o8O, gradientType, animatableGradientColorValue, oo80, \u300780\u3007808\u3007O, \u300780\u3007808\u3007O2, oo81, lineCapType, lineJoinType, n, list, animatableFloatValue, \u3007\u3007808\u3007);
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.airbnb.lottie.parser;

import java.io.IOException;
import com.airbnb.lottie.parser.moshi.JsonReader;
import android.graphics.PointF;

public class PathParser implements ValueParser<PointF>
{
    public static final PathParser \u3007080;
    
    static {
        \u3007080 = new PathParser();
    }
    
    private PathParser() {
    }
    
    public PointF \u3007o00\u3007\u3007Oo(final JsonReader jsonReader, final float n) throws IOException {
        return JsonUtils.Oo08(jsonReader, n);
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.airbnb.lottie.parser;

import java.io.IOException;
import com.airbnb.lottie.parser.moshi.JsonReader;
import com.airbnb.lottie.value.ScaleXY;

public class ScaleXYParser implements ValueParser<ScaleXY>
{
    public static final ScaleXYParser \u3007080;
    
    static {
        \u3007080 = new ScaleXYParser();
    }
    
    private ScaleXYParser() {
    }
    
    public ScaleXY \u3007o00\u3007\u3007Oo(final JsonReader jsonReader, final float n) throws IOException {
        final boolean b = jsonReader.O\u30078O8\u3007008() == JsonReader.Token.BEGIN_ARRAY;
        if (b) {
            jsonReader.\u3007\u3007888();
        }
        final float n2 = (float)jsonReader.\u3007O\u3007();
        final float n3 = (float)jsonReader.\u3007O\u3007();
        while (jsonReader.OO0o\u3007\u3007()) {
            jsonReader.\u300700\u30078();
        }
        if (b) {
            jsonReader.\u30078o8o\u3007();
        }
        return new ScaleXY(n2 / 100.0f * n, n3 / 100.0f * n);
    }
}

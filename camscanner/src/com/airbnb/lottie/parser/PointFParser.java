// 
// Decompiled by Procyon v0.6.0
// 

package com.airbnb.lottie.parser;

import java.io.IOException;
import com.airbnb.lottie.parser.moshi.JsonReader;
import android.graphics.PointF;

public class PointFParser implements ValueParser<PointF>
{
    public static final PointFParser \u3007080;
    
    static {
        \u3007080 = new PointFParser();
    }
    
    private PointFParser() {
    }
    
    public PointF \u3007o00\u3007\u3007Oo(final JsonReader jsonReader, final float n) throws IOException {
        final JsonReader.Token o\u30078O8\u3007008 = jsonReader.O\u30078O8\u3007008();
        if (o\u30078O8\u3007008 == JsonReader.Token.BEGIN_ARRAY) {
            return JsonUtils.Oo08(jsonReader, n);
        }
        if (o\u30078O8\u3007008 == JsonReader.Token.BEGIN_OBJECT) {
            return JsonUtils.Oo08(jsonReader, n);
        }
        if (o\u30078O8\u3007008 == JsonReader.Token.NUMBER) {
            final PointF pointF = new PointF((float)jsonReader.\u3007O\u3007() * n, (float)jsonReader.\u3007O\u3007() * n);
            while (jsonReader.OO0o\u3007\u3007()) {
                jsonReader.\u300700\u30078();
            }
            return pointF;
        }
        final StringBuilder sb = new StringBuilder();
        sb.append("Cannot convert json to point. Next token is ");
        sb.append(o\u30078O8\u3007008);
        throw new IllegalArgumentException(sb.toString());
    }
}

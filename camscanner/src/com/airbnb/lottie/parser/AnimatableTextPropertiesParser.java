// 
// Decompiled by Procyon v0.6.0
// 

package com.airbnb.lottie.parser;

import java.io.IOException;
import com.airbnb.lottie.model.animatable.AnimatableFloatValue;
import com.airbnb.lottie.model.animatable.AnimatableColorValue;
import com.airbnb.lottie.model.animatable.AnimatableTextProperties;
import com.airbnb.lottie.LottieComposition;
import com.airbnb.lottie.parser.moshi.JsonReader;

public class AnimatableTextPropertiesParser
{
    private static JsonReader.Options \u3007080;
    private static JsonReader.Options \u3007o00\u3007\u3007Oo;
    
    static {
        AnimatableTextPropertiesParser.\u3007080 = JsonReader.Options.\u3007080("a");
        AnimatableTextPropertiesParser.\u3007o00\u3007\u3007Oo = JsonReader.Options.\u3007080("fc", "sc", "sw", "t");
    }
    
    public static AnimatableTextProperties \u3007080(final JsonReader jsonReader, final LottieComposition lottieComposition) throws IOException {
        jsonReader.oO80();
        AnimatableTextProperties \u3007o00\u3007\u3007Oo = null;
        while (jsonReader.OO0o\u3007\u3007()) {
            if (jsonReader.OOO\u3007O0(AnimatableTextPropertiesParser.\u3007080) != 0) {
                jsonReader.O8\u3007o();
                jsonReader.\u300700\u30078();
            }
            else {
                \u3007o00\u3007\u3007Oo = \u3007o00\u3007\u3007Oo(jsonReader, lottieComposition);
            }
        }
        jsonReader.\u3007O8o08O();
        if (\u3007o00\u3007\u3007Oo == null) {
            return new AnimatableTextProperties(null, null, null, null);
        }
        return \u3007o00\u3007\u3007Oo;
    }
    
    private static AnimatableTextProperties \u3007o00\u3007\u3007Oo(final JsonReader jsonReader, final LottieComposition lottieComposition) throws IOException {
        jsonReader.oO80();
        AnimatableColorValue \u3007o\u3007 = null;
        AnimatableColorValue \u3007o\u30072 = null;
        AnimatableFloatValue oo9;
        AnimatableFloatValue oo08 = oo9 = null;
        while (jsonReader.OO0o\u3007\u3007()) {
            final int ooo\u3007O0 = jsonReader.OOO\u3007O0(AnimatableTextPropertiesParser.\u3007o00\u3007\u3007Oo);
            if (ooo\u3007O0 != 0) {
                if (ooo\u3007O0 != 1) {
                    if (ooo\u3007O0 != 2) {
                        if (ooo\u3007O0 != 3) {
                            jsonReader.O8\u3007o();
                            jsonReader.\u300700\u30078();
                        }
                        else {
                            oo9 = AnimatableValueParser.Oo08(jsonReader, lottieComposition);
                        }
                    }
                    else {
                        oo08 = AnimatableValueParser.Oo08(jsonReader, lottieComposition);
                    }
                }
                else {
                    \u3007o\u30072 = AnimatableValueParser.\u3007o\u3007(jsonReader, lottieComposition);
                }
            }
            else {
                \u3007o\u3007 = AnimatableValueParser.\u3007o\u3007(jsonReader, lottieComposition);
            }
        }
        jsonReader.\u3007O8o08O();
        return new AnimatableTextProperties(\u3007o\u3007, \u3007o\u30072, oo08, oo9);
    }
}

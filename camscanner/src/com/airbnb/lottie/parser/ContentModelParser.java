// 
// Decompiled by Procyon v0.6.0
// 

package com.airbnb.lottie.parser;

import androidx.annotation.Nullable;
import java.io.IOException;
import com.airbnb.lottie.model.content.MergePaths;
import com.airbnb.lottie.utils.Logger;
import com.airbnb.lottie.model.content.ContentModel;
import com.airbnb.lottie.LottieComposition;
import com.airbnb.lottie.parser.moshi.JsonReader;

class ContentModelParser
{
    private static JsonReader.Options \u3007080;
    
    static {
        ContentModelParser.\u3007080 = JsonReader.Options.\u3007080("ty", "d");
    }
    
    @Nullable
    static ContentModel \u3007080(final JsonReader jsonReader, final LottieComposition lottieComposition) throws IOException {
        jsonReader.oO80();
        int n = 2;
        int \u3007o00 = 2;
        ContentModel contentModel;
        String o800o8O;
        while (true) {
            final boolean oo0o\u3007\u3007 = jsonReader.OO0o\u3007\u3007();
            contentModel = null;
            if (!oo0o\u3007\u3007) {
                o800o8O = null;
                break;
            }
            final int ooo\u3007O0 = jsonReader.OOO\u3007O0(ContentModelParser.\u3007080);
            if (ooo\u3007O0 == 0) {
                o800o8O = jsonReader.o800o8O();
                break;
            }
            if (ooo\u3007O0 != 1) {
                jsonReader.O8\u3007o();
                jsonReader.\u300700\u30078();
            }
            else {
                \u3007o00 = jsonReader.\u3007O00();
            }
        }
        if (o800o8O == null) {
            return null;
        }
        Label_0438: {
            switch (o800o8O) {
                case "tr": {
                    n = 12;
                    break Label_0438;
                }
                case "tm": {
                    n = 11;
                    break Label_0438;
                }
                case "st": {
                    n = 10;
                    break Label_0438;
                }
                case "sr": {
                    n = 9;
                    break Label_0438;
                }
                case "sh": {
                    n = 8;
                    break Label_0438;
                }
                case "rp": {
                    n = 7;
                    break Label_0438;
                }
                case "rc": {
                    n = 6;
                    break Label_0438;
                }
                case "mm": {
                    n = 5;
                    break Label_0438;
                }
                case "gs": {
                    n = 4;
                    break Label_0438;
                }
                case "gr": {
                    n = 3;
                    break Label_0438;
                }
                case "gf": {
                    break Label_0438;
                }
                case "fl": {
                    n = 1;
                    break Label_0438;
                }
                case "el": {
                    n = 0;
                    break Label_0438;
                }
                default:
                    break;
            }
            n = -1;
        }
        ContentModel contentModel2 = null;
        switch (n) {
            default: {
                final StringBuilder sb = new StringBuilder();
                sb.append("Unknown shape type ");
                sb.append(o800o8O);
                Logger.\u3007o\u3007(sb.toString());
                contentModel2 = contentModel;
                break;
            }
            case 12: {
                contentModel2 = AnimatableTransformParser.\u3007\u3007888(jsonReader, lottieComposition);
                break;
            }
            case 11: {
                contentModel2 = ShapeTrimPathParser.\u3007080(jsonReader, lottieComposition);
                break;
            }
            case 10: {
                contentModel2 = ShapeStrokeParser.\u3007080(jsonReader, lottieComposition);
                break;
            }
            case 9: {
                contentModel2 = PolystarShapeParser.\u3007080(jsonReader, lottieComposition);
                break;
            }
            case 8: {
                contentModel2 = ShapePathParser.\u3007080(jsonReader, lottieComposition);
                break;
            }
            case 7: {
                contentModel2 = RepeaterParser.\u3007080(jsonReader, lottieComposition);
                break;
            }
            case 6: {
                contentModel2 = RectangleShapeParser.\u3007080(jsonReader, lottieComposition);
                break;
            }
            case 5: {
                final MergePaths \u3007080 = MergePathsParser.\u3007080(jsonReader);
                lottieComposition.\u3007080("Animation contains merge paths. Merge paths are only supported on KitKat+ and must be manually enabled by calling enableMergePathsForKitKatAndAbove().");
                contentModel2 = \u3007080;
                break;
            }
            case 4: {
                contentModel2 = GradientStrokeParser.\u3007080(jsonReader, lottieComposition);
                break;
            }
            case 3: {
                contentModel2 = ShapeGroupParser.\u3007080(jsonReader, lottieComposition);
                break;
            }
            case 2: {
                contentModel2 = GradientFillParser.\u3007080(jsonReader, lottieComposition);
                break;
            }
            case 1: {
                contentModel2 = ShapeFillParser.\u3007080(jsonReader, lottieComposition);
                break;
            }
            case 0: {
                contentModel2 = CircleShapeParser.\u3007080(jsonReader, lottieComposition, \u3007o00);
                break;
            }
        }
        while (jsonReader.OO0o\u3007\u3007()) {
            jsonReader.\u300700\u30078();
        }
        jsonReader.\u3007O8o08O();
        return contentModel2;
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.airbnb.lottie.parser;

import java.io.IOException;
import android.graphics.PointF;
import com.airbnb.lottie.utils.Utils;
import com.airbnb.lottie.animation.keyframe.PathKeyframe;
import com.airbnb.lottie.LottieComposition;
import com.airbnb.lottie.parser.moshi.JsonReader;

class PathKeyframeParser
{
    static PathKeyframe \u3007080(final JsonReader jsonReader, final LottieComposition lottieComposition) throws IOException {
        return new PathKeyframe(lottieComposition, KeyframeParser.\u3007o00\u3007\u3007Oo(jsonReader, lottieComposition, Utils.Oo08(), (ValueParser<PointF>)PathParser.\u3007080, jsonReader.O\u30078O8\u3007008() == JsonReader.Token.BEGIN_OBJECT));
    }
}

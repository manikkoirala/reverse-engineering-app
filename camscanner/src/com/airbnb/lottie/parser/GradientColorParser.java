// 
// Decompiled by Procyon v0.6.0
// 

package com.airbnb.lottie.parser;

import java.io.IOException;
import java.util.ArrayList;
import com.airbnb.lottie.parser.moshi.JsonReader;
import androidx.annotation.IntRange;
import com.airbnb.lottie.utils.MiscUtils;
import android.graphics.Color;
import java.util.List;
import com.airbnb.lottie.model.content.GradientColor;

public class GradientColorParser implements ValueParser<GradientColor>
{
    private int \u3007080;
    
    public GradientColorParser(final int \u3007080) {
        this.\u3007080 = \u3007080;
    }
    
    private void \u3007o00\u3007\u3007Oo(final GradientColor gradientColor, final List<Float> list) {
        int n = this.\u3007080 * 4;
        if (list.size() <= n) {
            return;
        }
        final int n2 = (list.size() - n) / 2;
        final double[] array = new double[n2];
        final double[] array2 = new double[n2];
        final int n3 = 0;
        int n4 = 0;
        int i;
        while (true) {
            i = n3;
            if (n >= list.size()) {
                break;
            }
            if (n % 2 == 0) {
                array[n4] = list.get(n);
            }
            else {
                array2[n4] = list.get(n);
                ++n4;
            }
            ++n;
        }
        while (i < gradientColor.\u3007o\u3007()) {
            final int n5 = gradientColor.\u3007080()[i];
            gradientColor.\u3007080()[i] = Color.argb(this.\u3007o\u3007(gradientColor.\u3007o00\u3007\u3007Oo()[i], array, array2), Color.red(n5), Color.green(n5), Color.blue(n5));
            ++i;
        }
    }
    
    @IntRange(from = 0L, to = 255L)
    private int \u3007o\u3007(double \u300780\u3007808\u3007O, final double[] array, final double[] array2) {
        for (int i = 1; i < array.length; ++i) {
            final int n = i - 1;
            final double n2 = array[n];
            final double n3 = array[i];
            if (n3 >= \u300780\u3007808\u3007O) {
                \u300780\u3007808\u3007O = (\u300780\u3007808\u3007O - n2) / (n3 - n2);
                \u300780\u3007808\u3007O = MiscUtils.\u300780\u3007808\u3007O(array2[n], array2[i], \u300780\u3007808\u3007O);
                return (int)(\u300780\u3007808\u3007O * 255.0);
            }
        }
        \u300780\u3007808\u3007O = array2[array2.length - 1];
        return (int)(\u300780\u3007808\u3007O * 255.0);
    }
    
    public GradientColor O8(final JsonReader jsonReader, final float n) throws IOException {
        final ArrayList list = new ArrayList();
        final JsonReader.Token o\u30078O8\u3007008 = jsonReader.O\u30078O8\u3007008();
        final JsonReader.Token begin_ARRAY = JsonReader.Token.BEGIN_ARRAY;
        final int n2 = 0;
        final boolean b = o\u30078O8\u3007008 == begin_ARRAY;
        if (b) {
            jsonReader.\u3007\u3007888();
        }
        while (jsonReader.OO0o\u3007\u3007()) {
            list.add((float)jsonReader.\u3007O\u3007());
        }
        if (b) {
            jsonReader.\u30078o8o\u3007();
        }
        if (this.\u3007080 == -1) {
            this.\u3007080 = list.size() / 4;
        }
        final int \u3007080 = this.\u3007080;
        final float[] array = new float[\u3007080];
        final int[] array2 = new int[\u3007080];
        final int n3 = 0;
        int n4 = 0;
        int i = n2;
        int n5 = n3;
        while (i < this.\u3007080 * 4) {
            final int n6 = i / 4;
            final double n7 = (float)list.get(i);
            final int n8 = i % 4;
            if (n8 != 0) {
                if (n8 != 1) {
                    if (n8 != 2) {
                        if (n8 == 3) {
                            array2[n6] = Color.argb(255, n5, n4, (int)(n7 * 255.0));
                        }
                    }
                    else {
                        n4 = (int)(n7 * 255.0);
                    }
                }
                else {
                    n5 = (int)(n7 * 255.0);
                }
            }
            else {
                array[n6] = (float)n7;
            }
            ++i;
        }
        final GradientColor gradientColor = new GradientColor(array, array2);
        this.\u3007o00\u3007\u3007Oo(gradientColor, list);
        return gradientColor;
    }
}

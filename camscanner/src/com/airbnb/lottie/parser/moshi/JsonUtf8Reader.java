// 
// Decompiled by Procyon v0.6.0
// 

package com.airbnb.lottie.parser.moshi;

import okio.Source;
import java.io.EOFException;
import java.io.IOException;
import androidx.annotation.Nullable;
import okio.BufferedSource;
import okio.Buffer;
import okio.ByteString;

final class JsonUtf8Reader extends JsonReader
{
    private static final ByteString O0O;
    private static final ByteString o8oOOo;
    private static final ByteString ooo0\u3007\u3007O;
    private static final ByteString \u3007O\u3007\u3007O8;
    private static final ByteString \u3007\u300708O;
    private long OO\u300700\u30078oO;
    private int o8\u3007OO0\u30070o;
    private int oOo0;
    private final Buffer oOo\u30078o008;
    private final BufferedSource \u30070O;
    @Nullable
    private String \u30078\u3007oO\u3007\u30078o;
    
    static {
        ooo0\u3007\u3007O = ByteString.encodeUtf8("'\\");
        \u3007\u300708O = ByteString.encodeUtf8("\"\\");
        O0O = ByteString.encodeUtf8("{}[]:, \n\t\r\f/\\;#=");
        o8oOOo = ByteString.encodeUtf8("\n\r");
        \u3007O\u3007\u3007O8 = ByteString.encodeUtf8("*/");
    }
    
    JsonUtf8Reader(final BufferedSource \u30070O) {
        this.oOo0 = 0;
        if (\u30070O != null) {
            this.\u30070O = \u30070O;
            this.oOo\u30078o008 = \u30070O.getBuffer();
            this.o\u3007\u30070\u3007(6);
            return;
        }
        throw new NullPointerException("source == null");
    }
    
    private int O000() throws IOException {
        long oo\u300700\u30078oO = 0L;
        int o8\u3007OO0\u30070o = 0;
        int n = 0;
        int n2 = 1;
        int n3 = 0;
        while (true) {
            final BufferedSource \u30070O = this.\u30070O;
            final int n4 = o8\u3007OO0\u30070o + 1;
            if (!\u30070O.request((long)n4)) {
                break;
            }
            final byte byte1 = this.oOo\u30078o008.getByte((long)o8\u3007OO0\u30070o);
            long n5 = 0L;
            int n6 = 0;
            int n7 = 0;
            int n8 = 0;
            Label_0494: {
                if (byte1 != 43) {
                    if (byte1 != 69 && byte1 != 101) {
                        if (byte1 != 45) {
                            if (byte1 != 46) {
                                if (byte1 >= 48 && byte1 <= 57) {
                                    if (n == 1 || n == 0) {
                                        n5 = -(byte1 - 48);
                                        n6 = 2;
                                        n7 = n2;
                                        n8 = n3;
                                        break Label_0494;
                                    }
                                    if (n == 2) {
                                        if (oo\u300700\u30078oO == 0L) {
                                            return 0;
                                        }
                                        n5 = 10L * oo\u300700\u30078oO - (byte1 - 48);
                                        final long n9 = lcmp(oo\u300700\u30078oO, -922337203685477580L);
                                        n7 = (n2 & ((n9 > 0 || (n9 == 0 && n5 < oo\u300700\u30078oO)) ? 1 : 0));
                                        n6 = n;
                                        n8 = n3;
                                        break Label_0494;
                                    }
                                    else {
                                        if (n == 3) {
                                            n6 = 4;
                                            n7 = n2;
                                            n5 = oo\u300700\u30078oO;
                                            n8 = n3;
                                            break Label_0494;
                                        }
                                        if (n != 5) {
                                            n6 = n;
                                            n7 = n2;
                                            n5 = oo\u300700\u30078oO;
                                            n8 = n3;
                                            if (n != 6) {
                                                break Label_0494;
                                            }
                                        }
                                        n6 = 7;
                                        n7 = n2;
                                        n5 = oo\u300700\u30078oO;
                                        n8 = n3;
                                        break Label_0494;
                                    }
                                }
                                else {
                                    if (!this.O\u3007O\u3007oO(byte1)) {
                                        break;
                                    }
                                    return 0;
                                }
                            }
                            else {
                                if (n == 2) {
                                    n6 = 3;
                                    n7 = n2;
                                    n5 = oo\u300700\u30078oO;
                                    n8 = n3;
                                    break Label_0494;
                                }
                                return 0;
                            }
                        }
                        else {
                            if (n == 0) {
                                n6 = 1;
                                n8 = 1;
                                n7 = n2;
                                n5 = oo\u300700\u30078oO;
                                break Label_0494;
                            }
                            if (n != 5) {
                                return 0;
                            }
                        }
                    }
                    else {
                        if (n != 2 && n != 4) {
                            return 0;
                        }
                        n6 = 5;
                        n7 = n2;
                        n5 = oo\u300700\u30078oO;
                        n8 = n3;
                        break Label_0494;
                    }
                }
                else if (n != 5) {
                    return 0;
                }
                n6 = 6;
                n8 = n3;
                n5 = oo\u300700\u30078oO;
                n7 = n2;
            }
            o8\u3007OO0\u30070o = n4;
            n = n6;
            n2 = n7;
            oo\u300700\u30078oO = n5;
            n3 = n8;
        }
        if (n == 2 && n2 != 0 && (oo\u300700\u30078oO != Long.MIN_VALUE || n3 != 0) && (oo\u300700\u30078oO != 0L || n3 == 0)) {
            if (n3 == 0) {
                oo\u300700\u30078oO = -oo\u300700\u30078oO;
            }
            this.OO\u300700\u30078oO = oo\u300700\u30078oO;
            this.oOo\u30078o008.skip((long)o8\u3007OO0\u30070o);
            return this.oOo0 = 16;
        }
        if (n != 2 && n != 4 && n != 7) {
            return 0;
        }
        this.o8\u3007OO0\u30070o = o8\u3007OO0\u30070o;
        return this.oOo0 = 17;
    }
    
    private int O08000() throws IOException {
        final int[] \u3007oOo8\u30070 = super.\u3007OOo8\u30070;
        final int o0 = super.o0;
        final int n = \u3007oOo8\u30070[o0 - 1];
        if (n == 1) {
            \u3007oOo8\u30070[o0 - 1] = 2;
        }
        else if (n == 2) {
            final int o8oO\u3007 = this.o8oO\u3007(true);
            this.oOo\u30078o008.readByte();
            if (o8oO\u3007 != 44) {
                if (o8oO\u3007 != 59) {
                    if (o8oO\u3007 == 93) {
                        return this.oOo0 = 4;
                    }
                    throw this.oO("Unterminated array");
                }
                else {
                    this.\u30078();
                }
            }
        }
        else if (n != 3 && n != 5) {
            if (n == 4) {
                \u3007oOo8\u30070[o0 - 1] = 5;
                final int o8oO\u30072 = this.o8oO\u3007(true);
                this.oOo\u30078o008.readByte();
                if (o8oO\u30072 != 58) {
                    if (o8oO\u30072 != 61) {
                        throw this.oO("Expected ':'");
                    }
                    this.\u30078();
                    if (this.\u30070O.request(1L) && this.oOo\u30078o008.getByte(0L) == 62) {
                        this.oOo\u30078o008.readByte();
                    }
                }
            }
            else if (n == 6) {
                \u3007oOo8\u30070[o0 - 1] = 7;
            }
            else if (n == 7) {
                if (this.o8oO\u3007(false) == -1) {
                    return this.oOo0 = 18;
                }
                this.\u30078();
            }
            else if (n == 8) {
                throw new IllegalStateException("JsonReader is closed");
            }
        }
        else {
            \u3007oOo8\u30070[o0 - 1] = 4;
            if (n == 5) {
                final int o8oO\u30073 = this.o8oO\u3007(true);
                this.oOo\u30078o008.readByte();
                if (o8oO\u30073 != 44) {
                    if (o8oO\u30073 != 59) {
                        if (o8oO\u30073 == 125) {
                            return this.oOo0 = 2;
                        }
                        throw this.oO("Unterminated object");
                    }
                    else {
                        this.\u30078();
                    }
                }
            }
            final int o8oO\u30074 = this.o8oO\u3007(true);
            if (o8oO\u30074 == 34) {
                this.oOo\u30078o008.readByte();
                return this.oOo0 = 13;
            }
            if (o8oO\u30074 == 39) {
                this.oOo\u30078o008.readByte();
                this.\u30078();
                return this.oOo0 = 12;
            }
            if (o8oO\u30074 != 125) {
                this.\u30078();
                if (this.O\u3007O\u3007oO((char)o8oO\u30074)) {
                    return this.oOo0 = 14;
                }
                throw this.oO("Expected name");
            }
            else {
                if (n != 5) {
                    this.oOo\u30078o008.readByte();
                    return this.oOo0 = 2;
                }
                throw this.oO("Expected name");
            }
        }
        final int o8oO\u30075 = this.o8oO\u3007(true);
        if (o8oO\u30075 == 34) {
            this.oOo\u30078o008.readByte();
            return this.oOo0 = 9;
        }
        if (o8oO\u30075 == 39) {
            this.\u30078();
            this.oOo\u30078o008.readByte();
            return this.oOo0 = 8;
        }
        if (o8oO\u30075 != 44 && o8oO\u30075 != 59) {
            if (o8oO\u30075 == 91) {
                this.oOo\u30078o008.readByte();
                return this.oOo0 = 3;
            }
            if (o8oO\u30075 != 93) {
                if (o8oO\u30075 == 123) {
                    this.oOo\u30078o008.readByte();
                    return this.oOo0 = 1;
                }
                final int oo00OOO = this.oO00OOO();
                if (oo00OOO != 0) {
                    return oo00OOO;
                }
                final int o2 = this.O000();
                if (o2 != 0) {
                    return o2;
                }
                if (this.O\u3007O\u3007oO(this.oOo\u30078o008.getByte(0L))) {
                    this.\u30078();
                    return this.oOo0 = 10;
                }
                throw this.oO("Expected value");
            }
            else if (n == 1) {
                this.oOo\u30078o008.readByte();
                return this.oOo0 = 4;
            }
        }
        if (n != 1 && n != 2) {
            throw this.oO("Unexpected value");
        }
        this.\u30078();
        return this.oOo0 = 7;
    }
    
    private void OOO() throws IOException {
        final long indexOfElement = this.\u30070O.indexOfElement(JsonUtf8Reader.o8oOOo);
        final Buffer oOo\u30078o008 = this.oOo\u30078o008;
        long size;
        if (indexOfElement != -1L) {
            size = indexOfElement + 1L;
        }
        else {
            size = oOo\u30078o008.size();
        }
        oOo\u30078o008.skip(size);
    }
    
    private void Ooo(final ByteString byteString) throws IOException {
        while (true) {
            final long indexOfElement = this.\u30070O.indexOfElement(byteString);
            if (indexOfElement == -1L) {
                throw this.oO("Unterminated string");
            }
            if (this.oOo\u30078o008.getByte(indexOfElement) != 92) {
                this.oOo\u30078o008.skip(indexOfElement + 1L);
                return;
            }
            this.oOo\u30078o008.skip(indexOfElement + 1L);
            this.\u300780();
        }
    }
    
    private boolean O\u3007O\u3007oO(final int n) throws IOException {
        if (n != 9 && n != 10 && n != 12 && n != 13 && n != 32) {
            if (n != 35) {
                if (n == 44) {
                    return false;
                }
                if (n != 47 && n != 61) {
                    if (n == 123 || n == 125 || n == 58) {
                        return false;
                    }
                    if (n != 59) {
                        switch (n) {
                            default: {
                                return true;
                            }
                            case 92: {
                                break;
                            }
                            case 91:
                            case 93: {
                                return false;
                            }
                        }
                    }
                }
            }
            this.\u30078();
        }
        return false;
    }
    
    private int o8oO\u3007(final boolean b) throws IOException {
        while (true) {
            int n = 0;
            while (true) {
                final BufferedSource \u30070O = this.\u30070O;
                final int n2 = n + 1;
                if (\u30070O.request((long)n2)) {
                    final byte byte1 = this.oOo\u30078o008.getByte((long)n);
                    if (byte1 != 10 && byte1 != 32 && byte1 != 13 && byte1 != 9) {
                        this.oOo\u30078o008.skip((long)(n2 - 1));
                        if (byte1 == 47) {
                            if (!this.\u30070O.request(2L)) {
                                return byte1;
                            }
                            this.\u30078();
                            final byte byte2 = this.oOo\u30078o008.getByte(1L);
                            if (byte2 != 42) {
                                if (byte2 != 47) {
                                    return byte1;
                                }
                                this.oOo\u30078o008.readByte();
                                this.oOo\u30078o008.readByte();
                                this.OOO();
                                break;
                            }
                            else {
                                this.oOo\u30078o008.readByte();
                                this.oOo\u30078o008.readByte();
                                if (this.\u3007O\u300780o08O()) {
                                    break;
                                }
                                throw this.oO("Unterminated comment");
                            }
                        }
                        else {
                            if (byte1 == 35) {
                                this.\u30078();
                                this.OOO();
                                break;
                            }
                            return byte1;
                        }
                    }
                    else {
                        n = n2;
                    }
                }
                else {
                    if (!b) {
                        return -1;
                    }
                    throw new EOFException("End of input");
                }
            }
        }
    }
    
    private int oO00OOO() throws IOException {
        final byte byte1 = this.oOo\u30078o008.getByte(0L);
        String s;
        String s2;
        int oOo0;
        if (byte1 != 116 && byte1 != 84) {
            if (byte1 != 102 && byte1 != 70) {
                if (byte1 != 110 && byte1 != 78) {
                    return 0;
                }
                s = "null";
                s2 = "NULL";
                oOo0 = 7;
            }
            else {
                s = "false";
                s2 = "FALSE";
                oOo0 = 6;
            }
        }
        else {
            s = "true";
            s2 = "TRUE";
            oOo0 = 5;
        }
        final int length = s.length();
        int n;
        for (int i = 1; i < length; i = n) {
            final BufferedSource \u30070O = this.\u30070O;
            n = i + 1;
            if (!\u30070O.request((long)n)) {
                return 0;
            }
            final byte byte2 = this.oOo\u30078o008.getByte((long)i);
            if (byte2 != s.charAt(i) && byte2 != s2.charAt(i)) {
                return 0;
            }
        }
        if (this.\u30070O.request((long)(length + 1)) && this.O\u3007O\u3007oO(this.oOo\u30078o008.getByte((long)length))) {
            return 0;
        }
        this.oOo\u30078o008.skip((long)length);
        return this.oOo0 = oOo0;
    }
    
    private void ooo\u30078oO() throws IOException {
        long n = this.\u30070O.indexOfElement(JsonUtf8Reader.O0O);
        final Buffer oOo\u30078o008 = this.oOo\u30078o008;
        if (n == -1L) {
            n = oOo\u30078o008.size();
        }
        oOo\u30078o008.skip(n);
    }
    
    private String o\u30078oOO88(final ByteString byteString) throws IOException {
        StringBuilder sb = null;
        while (true) {
            final long indexOfElement = this.\u30070O.indexOfElement(byteString);
            if (indexOfElement == -1L) {
                throw this.oO("Unterminated string");
            }
            if (this.oOo\u30078o008.getByte(indexOfElement) == 92) {
                StringBuilder sb2;
                if ((sb2 = sb) == null) {
                    sb2 = new StringBuilder();
                }
                sb2.append(this.oOo\u30078o008.readUtf8(indexOfElement));
                this.oOo\u30078o008.readByte();
                sb2.append(this.\u300780());
                sb = sb2;
            }
            else {
                if (sb == null) {
                    final String utf8 = this.oOo\u30078o008.readUtf8(indexOfElement);
                    this.oOo\u30078o008.readByte();
                    return utf8;
                }
                sb.append(this.oOo\u30078o008.readUtf8(indexOfElement));
                this.oOo\u30078o008.readByte();
                return sb.toString();
            }
        }
    }
    
    private String o\u3007O() throws IOException {
        final long indexOfElement = this.\u30070O.indexOfElement(JsonUtf8Reader.O0O);
        String s;
        if (indexOfElement != -1L) {
            s = this.oOo\u30078o008.readUtf8(indexOfElement);
        }
        else {
            s = this.oOo\u30078o008.readUtf8();
        }
        return s;
    }
    
    private void \u30078() throws IOException {
        if (super.o\u300700O) {
            return;
        }
        throw this.oO("Use JsonReader.setLenient(true) to accept malformed JSON");
    }
    
    private char \u300780() throws IOException {
        if (!this.\u30070O.request(1L)) {
            throw this.oO("Unterminated escape sequence");
        }
        final byte byte1 = this.oOo\u30078o008.readByte();
        if (byte1 == 10 || byte1 == 34 || byte1 == 39 || byte1 == 47 || byte1 == 92) {
            return (char)byte1;
        }
        if (byte1 == 98) {
            return '\b';
        }
        if (byte1 == 102) {
            return '\f';
        }
        if (byte1 == 110) {
            return '\n';
        }
        if (byte1 == 114) {
            return '\r';
        }
        if (byte1 == 116) {
            return '\t';
        }
        if (byte1 != 117) {
            if (super.o\u300700O) {
                return (char)byte1;
            }
            final StringBuilder sb = new StringBuilder();
            sb.append("Invalid escape sequence: \\");
            sb.append((char)byte1);
            throw this.oO(sb.toString());
        }
        else {
            if (this.\u30070O.request(4L)) {
                int i = 0;
                char c = '\0';
                while (i < 4) {
                    int byte2 = this.oOo\u30078o008.getByte((long)i);
                    final char c2 = (char)(c << 4);
                    if (byte2 >= '0' && byte2 <= '9') {
                        byte2 -= 48;
                    }
                    else {
                        if (byte2 >= 'a' && byte2 <= 'f') {
                            byte2 -= 97;
                        }
                        else {
                            if (byte2 < 'A' || byte2 > 'F') {
                                final StringBuilder sb2 = new StringBuilder();
                                sb2.append("\\u");
                                sb2.append(this.oOo\u30078o008.readUtf8(4L));
                                throw this.oO(sb2.toString());
                            }
                            byte2 -= 65;
                        }
                        byte2 += '\n';
                    }
                    c = (char)(c2 + byte2);
                    ++i;
                }
                this.oOo\u30078o008.skip(4L);
                return c;
            }
            final StringBuilder sb3 = new StringBuilder();
            sb3.append("Unterminated escape sequence at path ");
            sb3.append(this.getPath());
            throw new EOFException(sb3.toString());
        }
    }
    
    private int \u30078\u30070\u3007o\u3007O(final String s, final Options options) {
        for (int length = options.\u3007080.length, i = 0; i < length; ++i) {
            if (s.equals(options.\u3007080[i])) {
                this.oOo0 = 0;
                super.OO[super.o0 - 1] = s;
                return i;
            }
        }
        return -1;
    }
    
    private boolean \u3007O\u300780o08O() throws IOException {
        final BufferedSource \u30070O = this.\u30070O;
        final ByteString \u3007o\u3007\u3007O8 = JsonUtf8Reader.\u3007O\u3007\u3007O8;
        final long index = \u30070O.indexOf(\u3007o\u3007\u3007O8);
        final boolean b = index != -1L;
        final Buffer oOo\u30078o008 = this.oOo\u30078o008;
        long size;
        if (b) {
            size = index + \u3007o\u3007\u3007O8.size();
        }
        else {
            size = oOo\u30078o008.size();
        }
        oOo\u30078o008.skip(size);
        return b;
    }
    
    @Override
    public void O8\u3007o() throws IOException {
        if (!super.O8o08O8O) {
            int n;
            if ((n = this.oOo0) == 0) {
                n = this.O08000();
            }
            if (n == 14) {
                this.ooo\u30078oO();
            }
            else if (n == 13) {
                this.Ooo(JsonUtf8Reader.\u3007\u300708O);
            }
            else if (n == 12) {
                this.Ooo(JsonUtf8Reader.ooo0\u3007\u3007O);
            }
            else if (n != 15) {
                final StringBuilder sb = new StringBuilder();
                sb.append("Expected a name but was ");
                sb.append(this.O\u30078O8\u3007008());
                sb.append(" at path ");
                sb.append(this.getPath());
                throw new JsonDataException(sb.toString());
            }
            this.oOo0 = 0;
            super.OO[super.o0 - 1] = "null";
            return;
        }
        final StringBuilder sb2 = new StringBuilder();
        sb2.append("Cannot skip unexpected ");
        sb2.append(this.O\u30078O8\u3007008());
        sb2.append(" at ");
        sb2.append(this.getPath());
        throw new JsonDataException(sb2.toString());
    }
    
    @Override
    public boolean OO0o\u3007\u3007() throws IOException {
        int n;
        if ((n = this.oOo0) == 0) {
            n = this.O08000();
        }
        return n != 2 && n != 4 && n != 18;
    }
    
    @Override
    public int OOO\u3007O0(final Options options) throws IOException {
        int n;
        if ((n = this.oOo0) == 0) {
            n = this.O08000();
        }
        if (n < 12 || n > 15) {
            return -1;
        }
        if (n == 15) {
            return this.\u30078\u30070\u3007o\u3007O(this.\u30078\u3007oO\u3007\u30078o, options);
        }
        final int select = this.\u30070O.select(options.\u3007o00\u3007\u3007Oo);
        if (select != -1) {
            this.oOo0 = 0;
            super.OO[super.o0 - 1] = options.\u3007080[select];
            return select;
        }
        final String s = super.OO[super.o0 - 1];
        final String \u3007\u30078O0\u30078 = this.\u3007\u30078O0\u30078();
        final int \u30078\u30070\u3007o\u3007O = this.\u30078\u30070\u3007o\u3007O(\u3007\u30078O0\u30078, options);
        if (\u30078\u30070\u3007o\u3007O == -1) {
            this.oOo0 = 15;
            this.\u30078\u3007oO\u3007\u30078o = \u3007\u30078O0\u30078;
            super.OO[super.o0 - 1] = s;
        }
        return \u30078\u30070\u3007o\u3007O;
    }
    
    @Override
    public Token O\u30078O8\u3007008() throws IOException {
        int n;
        if ((n = this.oOo0) == 0) {
            n = this.O08000();
        }
        switch (n) {
            default: {
                throw new AssertionError();
            }
            case 18: {
                return Token.END_DOCUMENT;
            }
            case 16:
            case 17: {
                return Token.NUMBER;
            }
            case 12:
            case 13:
            case 14:
            case 15: {
                return Token.NAME;
            }
            case 8:
            case 9:
            case 10:
            case 11: {
                return Token.STRING;
            }
            case 7: {
                return Token.NULL;
            }
            case 5:
            case 6: {
                return Token.BOOLEAN;
            }
            case 4: {
                return Token.END_ARRAY;
            }
            case 3: {
                return Token.BEGIN_ARRAY;
            }
            case 2: {
                return Token.END_OBJECT;
            }
            case 1: {
                return Token.BEGIN_OBJECT;
            }
        }
    }
    
    @Override
    public void close() throws IOException {
        this.oOo0 = 0;
        super.\u3007OOo8\u30070[0] = 8;
        super.o0 = 1;
        this.oOo\u30078o008.clear();
        ((Source)this.\u30070O).close();
    }
    
    @Override
    public String o800o8O() throws IOException {
        int n;
        if ((n = this.oOo0) == 0) {
            n = this.O08000();
        }
        String s;
        if (n == 10) {
            s = this.o\u3007O();
        }
        else if (n == 9) {
            s = this.o\u30078oOO88(JsonUtf8Reader.\u3007\u300708O);
        }
        else if (n == 8) {
            s = this.o\u30078oOO88(JsonUtf8Reader.ooo0\u3007\u3007O);
        }
        else if (n == 11) {
            s = this.\u30078\u3007oO\u3007\u30078o;
            this.\u30078\u3007oO\u3007\u30078o = null;
        }
        else if (n == 16) {
            s = Long.toString(this.OO\u300700\u30078oO);
        }
        else {
            if (n != 17) {
                final StringBuilder sb = new StringBuilder();
                sb.append("Expected a string but was ");
                sb.append(this.O\u30078O8\u3007008());
                sb.append(" at path ");
                sb.append(this.getPath());
                throw new JsonDataException(sb.toString());
            }
            s = this.oOo\u30078o008.readUtf8((long)this.o8\u3007OO0\u30070o);
        }
        this.oOo0 = 0;
        final int[] \u300708O\u300700\u3007o = super.\u300708O\u300700\u3007o;
        final int n2 = super.o0 - 1;
        ++\u300708O\u300700\u3007o[n2];
        return s;
    }
    
    @Override
    public void oO80() throws IOException {
        int n;
        if ((n = this.oOo0) == 0) {
            n = this.O08000();
        }
        if (n == 1) {
            this.o\u3007\u30070\u3007(3);
            this.oOo0 = 0;
            return;
        }
        final StringBuilder sb = new StringBuilder();
        sb.append("Expected BEGIN_OBJECT but was ");
        sb.append(this.O\u30078O8\u3007008());
        sb.append(" at path ");
        sb.append(this.getPath());
        throw new JsonDataException(sb.toString());
    }
    
    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append("JsonReader(");
        sb.append(this.\u30070O);
        sb.append(")");
        return sb.toString();
    }
    
    @Override
    public void \u300700\u30078() throws IOException {
        if (!super.O8o08O8O) {
            int n = 0;
            int i = 0;
            do {
                int n2;
                if ((n2 = this.oOo0) == 0) {
                    n2 = this.O08000();
                }
                Label_0396: {
                    if (n2 == 3) {
                        this.o\u3007\u30070\u3007(1);
                    }
                    else if (n2 == 1) {
                        this.o\u3007\u30070\u3007(3);
                    }
                    else if (n2 == 4) {
                        i = n - 1;
                        if (i >= 0) {
                            --super.o0;
                            break Label_0396;
                        }
                        final StringBuilder sb = new StringBuilder();
                        sb.append("Expected a value but was ");
                        sb.append(this.O\u30078O8\u3007008());
                        sb.append(" at path ");
                        sb.append(this.getPath());
                        throw new JsonDataException(sb.toString());
                    }
                    else if (n2 == 2) {
                        i = n - 1;
                        if (i >= 0) {
                            --super.o0;
                            break Label_0396;
                        }
                        final StringBuilder sb2 = new StringBuilder();
                        sb2.append("Expected a value but was ");
                        sb2.append(this.O\u30078O8\u3007008());
                        sb2.append(" at path ");
                        sb2.append(this.getPath());
                        throw new JsonDataException(sb2.toString());
                    }
                    else {
                        if (n2 == 14 || n2 == 10) {
                            this.ooo\u30078oO();
                            i = n;
                            break Label_0396;
                        }
                        if (n2 == 9 || n2 == 13) {
                            this.Ooo(JsonUtf8Reader.\u3007\u300708O);
                            i = n;
                            break Label_0396;
                        }
                        if (n2 == 8 || n2 == 12) {
                            this.Ooo(JsonUtf8Reader.ooo0\u3007\u3007O);
                            i = n;
                            break Label_0396;
                        }
                        if (n2 == 17) {
                            this.oOo\u30078o008.skip((long)this.o8\u3007OO0\u30070o);
                            i = n;
                            break Label_0396;
                        }
                        if (n2 != 18) {
                            i = n;
                            break Label_0396;
                        }
                        final StringBuilder sb3 = new StringBuilder();
                        sb3.append("Expected a value but was ");
                        sb3.append(this.O\u30078O8\u3007008());
                        sb3.append(" at path ");
                        sb3.append(this.getPath());
                        throw new JsonDataException(sb3.toString());
                    }
                    i = n + 1;
                }
                this.oOo0 = 0;
                n = i;
            } while (i != 0);
            final int[] \u300708O\u300700\u3007o = super.\u300708O\u300700\u3007o;
            final int o0 = super.o0;
            final int n3 = o0 - 1;
            ++\u300708O\u300700\u3007o[n3];
            super.OO[o0 - 1] = "null";
            return;
        }
        final StringBuilder sb4 = new StringBuilder();
        sb4.append("Cannot skip unexpected ");
        sb4.append(this.O\u30078O8\u3007008());
        sb4.append(" at ");
        sb4.append(this.getPath());
        throw new JsonDataException(sb4.toString());
    }
    
    @Override
    public void \u30078o8o\u3007() throws IOException {
        int n;
        if ((n = this.oOo0) == 0) {
            n = this.O08000();
        }
        if (n == 4) {
            int o0 = super.o0 - 1;
            super.o0 = o0;
            final int[] \u300708O\u300700\u3007o = super.\u300708O\u300700\u3007o;
            --o0;
            ++\u300708O\u300700\u3007o[o0];
            this.oOo0 = 0;
            return;
        }
        final StringBuilder sb = new StringBuilder();
        sb.append("Expected END_ARRAY but was ");
        sb.append(this.O\u30078O8\u3007008());
        sb.append(" at path ");
        sb.append(this.getPath());
        throw new JsonDataException(sb.toString());
    }
    
    @Override
    public int \u3007O00() throws IOException {
        int n;
        if ((n = this.oOo0) == 0) {
            n = this.O08000();
        }
        if (n == 16) {
            final long oo\u300700\u30078oO = this.OO\u300700\u30078oO;
            final int n2 = (int)oo\u300700\u30078oO;
            if (oo\u300700\u30078oO == n2) {
                this.oOo0 = 0;
                final int[] \u300708O\u300700\u3007o = super.\u300708O\u300700\u3007o;
                final int n3 = super.o0 - 1;
                ++\u300708O\u300700\u3007o[n3];
                return n2;
            }
            final StringBuilder sb = new StringBuilder();
            sb.append("Expected an int but was ");
            sb.append(this.OO\u300700\u30078oO);
            sb.append(" at path ");
            sb.append(this.getPath());
            throw new JsonDataException(sb.toString());
        }
        else {
            while (true) {
                if (n == 17) {
                    this.\u30078\u3007oO\u3007\u30078o = this.oOo\u30078o008.readUtf8((long)this.o8\u3007OO0\u30070o);
                    break Label_0316;
                }
                String s;
                if (n != 9 && n != 8) {
                    if (n == 11) {
                        break Label_0316;
                    }
                    final StringBuilder sb2 = new StringBuilder();
                    sb2.append("Expected an int but was ");
                    sb2.append(this.O\u30078O8\u3007008());
                    sb2.append(" at path ");
                    sb2.append(this.getPath());
                    throw new JsonDataException(sb2.toString());
                }
                else if (n == 9) {
                    s = this.o\u30078oOO88(JsonUtf8Reader.\u3007\u300708O);
                }
                else {
                    s = this.o\u30078oOO88(JsonUtf8Reader.ooo0\u3007\u3007O);
                }
                this.\u30078\u3007oO\u3007\u30078o = s;
                try {
                    final int int1 = Integer.parseInt(s);
                    this.oOo0 = 0;
                    final int[] \u300708O\u300700\u3007o2 = super.\u300708O\u300700\u3007o;
                    final int n4 = super.o0 - 1;
                    ++\u300708O\u300700\u3007o2[n4];
                    return int1;
                    this.oOo0 = 11;
                    try {
                        final double double1 = Double.parseDouble(this.\u30078\u3007oO\u3007\u30078o);
                        final int n5 = (int)double1;
                        if (n5 == double1) {
                            this.\u30078\u3007oO\u3007\u30078o = null;
                            this.oOo0 = 0;
                            final int[] \u300708O\u300700\u3007o3 = super.\u300708O\u300700\u3007o;
                            final int n6 = super.o0 - 1;
                            ++\u300708O\u300700\u3007o3[n6];
                            return n5;
                        }
                        final StringBuilder sb3 = new StringBuilder();
                        sb3.append("Expected an int but was ");
                        sb3.append(this.\u30078\u3007oO\u3007\u30078o);
                        sb3.append(" at path ");
                        sb3.append(this.getPath());
                        throw new JsonDataException(sb3.toString());
                    }
                    catch (final NumberFormatException ex) {
                        final StringBuilder sb4 = new StringBuilder();
                        sb4.append("Expected an int but was ");
                        sb4.append(this.\u30078\u3007oO\u3007\u30078o);
                        sb4.append(" at path ");
                        sb4.append(this.getPath());
                        throw new JsonDataException(sb4.toString());
                    }
                }
                catch (final NumberFormatException ex2) {
                    continue;
                }
                break;
            }
        }
    }
    
    @Override
    public void \u3007O8o08O() throws IOException {
        int n;
        if ((n = this.oOo0) == 0) {
            n = this.O08000();
        }
        if (n == 2) {
            int o0 = super.o0 - 1;
            super.o0 = o0;
            super.OO[o0] = null;
            final int[] \u300708O\u300700\u3007o = super.\u300708O\u300700\u3007o;
            --o0;
            ++\u300708O\u300700\u3007o[o0];
            this.oOo0 = 0;
            return;
        }
        final StringBuilder sb = new StringBuilder();
        sb.append("Expected END_OBJECT but was ");
        sb.append(this.O\u30078O8\u3007008());
        sb.append(" at path ");
        sb.append(this.getPath());
        throw new JsonDataException(sb.toString());
    }
    
    @Override
    public double \u3007O\u3007() throws IOException {
        int n;
        if ((n = this.oOo0) == 0) {
            n = this.O08000();
        }
        if (n == 16) {
            this.oOo0 = 0;
            final int[] \u300708O\u300700\u3007o = super.\u300708O\u300700\u3007o;
            final int n2 = super.o0 - 1;
            ++\u300708O\u300700\u3007o[n2];
            return (double)this.OO\u300700\u30078oO;
        }
        Label_0339: {
            if (n == 17) {
                this.\u30078\u3007oO\u3007\u30078o = this.oOo\u30078o008.readUtf8((long)this.o8\u3007OO0\u30070o);
            }
            else if (n == 9) {
                this.\u30078\u3007oO\u3007\u30078o = this.o\u30078oOO88(JsonUtf8Reader.\u3007\u300708O);
            }
            else if (n == 8) {
                this.\u30078\u3007oO\u3007\u30078o = this.o\u30078oOO88(JsonUtf8Reader.ooo0\u3007\u3007O);
            }
            else if (n == 10) {
                this.\u30078\u3007oO\u3007\u30078o = this.o\u3007O();
            }
            else if (n != 11) {
                break Label_0339;
            }
            this.oOo0 = 11;
            try {
                final double double1 = Double.parseDouble(this.\u30078\u3007oO\u3007\u30078o);
                if (!super.o\u300700O && (Double.isNaN(double1) || Double.isInfinite(double1))) {
                    final StringBuilder sb = new StringBuilder();
                    sb.append("JSON forbids NaN and infinities: ");
                    sb.append(double1);
                    sb.append(" at path ");
                    sb.append(this.getPath());
                    throw new JsonEncodingException(sb.toString());
                }
                this.\u30078\u3007oO\u3007\u30078o = null;
                this.oOo0 = 0;
                final int[] \u300708O\u300700\u3007o2 = super.\u300708O\u300700\u3007o;
                final int n3 = super.o0 - 1;
                ++\u300708O\u300700\u3007o2[n3];
                return double1;
            }
            catch (final NumberFormatException ex) {
                final StringBuilder sb2 = new StringBuilder();
                sb2.append("Expected a double but was ");
                sb2.append(this.\u30078\u3007oO\u3007\u30078o);
                sb2.append(" at path ");
                sb2.append(this.getPath());
                throw new JsonDataException(sb2.toString());
            }
        }
        final StringBuilder sb3 = new StringBuilder();
        sb3.append("Expected a double but was ");
        sb3.append(this.O\u30078O8\u3007008());
        sb3.append(" at path ");
        sb3.append(this.getPath());
        throw new JsonDataException(sb3.toString());
    }
    
    @Override
    public boolean \u3007\u3007808\u3007() throws IOException {
        int n;
        if ((n = this.oOo0) == 0) {
            n = this.O08000();
        }
        if (n == 5) {
            this.oOo0 = 0;
            final int[] \u300708O\u300700\u3007o = super.\u300708O\u300700\u3007o;
            final int n2 = super.o0 - 1;
            ++\u300708O\u300700\u3007o[n2];
            return true;
        }
        if (n == 6) {
            this.oOo0 = 0;
            final int[] \u300708O\u300700\u3007o2 = super.\u300708O\u300700\u3007o;
            final int n3 = super.o0 - 1;
            ++\u300708O\u300700\u3007o2[n3];
            return false;
        }
        final StringBuilder sb = new StringBuilder();
        sb.append("Expected a boolean but was ");
        sb.append(this.O\u30078O8\u3007008());
        sb.append(" at path ");
        sb.append(this.getPath());
        throw new JsonDataException(sb.toString());
    }
    
    @Override
    public void \u3007\u3007888() throws IOException {
        int n;
        if ((n = this.oOo0) == 0) {
            n = this.O08000();
        }
        if (n == 3) {
            this.o\u3007\u30070\u3007(1);
            super.\u300708O\u300700\u3007o[super.o0 - 1] = 0;
            this.oOo0 = 0;
            return;
        }
        final StringBuilder sb = new StringBuilder();
        sb.append("Expected BEGIN_ARRAY but was ");
        sb.append(this.O\u30078O8\u3007008());
        sb.append(" at path ");
        sb.append(this.getPath());
        throw new JsonDataException(sb.toString());
    }
    
    @Override
    public String \u3007\u30078O0\u30078() throws IOException {
        int n;
        if ((n = this.oOo0) == 0) {
            n = this.O08000();
        }
        String s;
        if (n == 14) {
            s = this.o\u3007O();
        }
        else if (n == 13) {
            s = this.o\u30078oOO88(JsonUtf8Reader.\u3007\u300708O);
        }
        else if (n == 12) {
            s = this.o\u30078oOO88(JsonUtf8Reader.ooo0\u3007\u3007O);
        }
        else {
            if (n != 15) {
                final StringBuilder sb = new StringBuilder();
                sb.append("Expected a name but was ");
                sb.append(this.O\u30078O8\u3007008());
                sb.append(" at path ");
                sb.append(this.getPath());
                throw new JsonDataException(sb.toString());
            }
            s = this.\u30078\u3007oO\u3007\u30078o;
        }
        this.oOo0 = 0;
        return super.OO[super.o0 - 1] = s;
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.airbnb.lottie.parser.moshi;

final class JsonScope
{
    static String \u3007080(final int n, final int[] array, final String[] array2, final int[] array3) {
        final StringBuilder sb = new StringBuilder();
        sb.append('$');
        for (int i = 0; i < n; ++i) {
            final int n2 = array[i];
            if (n2 != 1 && n2 != 2) {
                if (n2 == 3 || n2 == 4 || n2 == 5) {
                    sb.append('.');
                    final String str = array2[i];
                    if (str != null) {
                        sb.append(str);
                    }
                }
            }
            else {
                sb.append('[');
                sb.append(array3[i]);
                sb.append(']');
            }
        }
        return sb.toString();
    }
}

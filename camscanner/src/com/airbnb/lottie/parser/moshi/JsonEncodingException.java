// 
// Decompiled by Procyon v0.6.0
// 

package com.airbnb.lottie.parser.moshi;

import androidx.annotation.Nullable;
import java.io.IOException;

final class JsonEncodingException extends IOException
{
    JsonEncodingException(@Nullable final String message) {
        super(message);
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.airbnb.lottie.parser.moshi;

import androidx.annotation.Nullable;

final class JsonDataException extends RuntimeException
{
    JsonDataException(@Nullable final String message) {
        super(message);
    }
}

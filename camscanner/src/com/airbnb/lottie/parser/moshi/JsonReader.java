// 
// Decompiled by Procyon v0.6.0
// 

package com.airbnb.lottie.parser.moshi;

import okio.Buffer;
import okio.ByteString;
import okio.Options;
import java.util.Arrays;
import okio.BufferedSource;
import java.io.IOException;
import okio.BufferedSink;
import java.io.Closeable;

public abstract class JsonReader implements Closeable
{
    private static final String[] \u3007080OO8\u30070;
    boolean O8o08O8O;
    String[] OO;
    int o0;
    boolean o\u300700O;
    int[] \u300708O\u300700\u3007o;
    int[] \u3007OOo8\u30070;
    
    static {
        \u3007080OO8\u30070 = new String[128];
        for (int i = 0; i <= 31; ++i) {
            JsonReader.\u3007080OO8\u30070[i] = String.format("\\u%04x", i);
        }
        final String[] \u3007080OO8\u3007 = JsonReader.\u3007080OO8\u30070;
        \u3007080OO8\u3007[34] = "\\\"";
        \u3007080OO8\u3007[92] = "\\\\";
        \u3007080OO8\u3007[9] = "\\t";
        \u3007080OO8\u3007[8] = "\\b";
        \u3007080OO8\u3007[10] = "\\n";
        \u3007080OO8\u3007[13] = "\\r";
        \u3007080OO8\u3007[12] = "\\f";
    }
    
    JsonReader() {
        this.\u3007OOo8\u30070 = new int[32];
        this.OO = new String[32];
        this.\u300708O\u300700\u3007o = new int[32];
    }
    
    public static JsonReader \u3007oo\u3007(final BufferedSource bufferedSource) {
        return new JsonUtf8Reader(bufferedSource);
    }
    
    private static void \u3007\u3007\u30070\u3007\u30070(final BufferedSink bufferedSink, final String s) throws IOException {
        final String[] \u3007080OO8\u30070 = JsonReader.\u3007080OO8\u30070;
        bufferedSink.writeByte(34);
        final int length = s.length();
        int i = 0;
        int n = 0;
        while (i < length) {
            final char char1 = s.charAt(i);
            int n2 = 0;
            Label_0129: {
                String s2;
                if (char1 < '\u0080') {
                    if ((s2 = \u3007080OO8\u30070[char1]) == null) {
                        n2 = n;
                        break Label_0129;
                    }
                }
                else if (char1 == '\u2028') {
                    s2 = "\\u2028";
                }
                else {
                    n2 = n;
                    if (char1 != '\u2029') {
                        break Label_0129;
                    }
                    s2 = "\\u2029";
                }
                if (n < i) {
                    bufferedSink.writeUtf8(s, n, i);
                }
                bufferedSink.writeUtf8(s2);
                n2 = i + 1;
            }
            ++i;
            n = n2;
        }
        if (n < length) {
            bufferedSink.writeUtf8(s, n, length);
        }
        bufferedSink.writeByte(34);
    }
    
    public abstract void O8\u3007o() throws IOException;
    
    public abstract boolean OO0o\u3007\u3007() throws IOException;
    
    public abstract int OOO\u3007O0(final Options p0) throws IOException;
    
    public abstract Token O\u30078O8\u3007008() throws IOException;
    
    public final String getPath() {
        return JsonScope.\u3007080(this.o0, this.\u3007OOo8\u30070, this.OO, this.\u300708O\u300700\u3007o);
    }
    
    public abstract String o800o8O() throws IOException;
    
    final JsonEncodingException oO(final String str) throws JsonEncodingException {
        final StringBuilder sb = new StringBuilder();
        sb.append(str);
        sb.append(" at path ");
        sb.append(this.getPath());
        throw new JsonEncodingException(sb.toString());
    }
    
    public abstract void oO80() throws IOException;
    
    final void o\u3007\u30070\u3007(final int n) {
        final int o0 = this.o0;
        final int[] \u3007oOo8\u30070 = this.\u3007OOo8\u30070;
        if (o0 == \u3007oOo8\u30070.length) {
            if (o0 == 256) {
                final StringBuilder sb = new StringBuilder();
                sb.append("Nesting too deep at ");
                sb.append(this.getPath());
                throw new JsonDataException(sb.toString());
            }
            this.\u3007OOo8\u30070 = Arrays.copyOf(\u3007oOo8\u30070, \u3007oOo8\u30070.length * 2);
            final String[] oo = this.OO;
            this.OO = Arrays.copyOf(oo, oo.length * 2);
            final int[] \u300708O\u300700\u3007o = this.\u300708O\u300700\u3007o;
            this.\u300708O\u300700\u3007o = Arrays.copyOf(\u300708O\u300700\u3007o, \u300708O\u300700\u3007o.length * 2);
        }
        this.\u3007OOo8\u30070[this.o0++] = n;
    }
    
    public abstract void \u300700\u30078() throws IOException;
    
    public abstract void \u30078o8o\u3007() throws IOException;
    
    public abstract int \u3007O00() throws IOException;
    
    public abstract void \u3007O8o08O() throws IOException;
    
    public abstract double \u3007O\u3007() throws IOException;
    
    public abstract boolean \u3007\u3007808\u3007() throws IOException;
    
    public abstract void \u3007\u3007888() throws IOException;
    
    public abstract String \u3007\u30078O0\u30078() throws IOException;
    
    public static final class Options
    {
        final String[] \u3007080;
        final okio.Options \u3007o00\u3007\u3007Oo;
        
        private Options(final String[] \u3007080, final okio.Options \u3007o00\u3007\u3007Oo) {
            this.\u3007080 = \u3007080;
            this.\u3007o00\u3007\u3007Oo = \u3007o00\u3007\u3007Oo;
        }
        
        public static Options \u3007080(final String... array) {
            try {
                final ByteString[] array2 = new ByteString[array.length];
                final Buffer buffer = new Buffer();
                for (int i = 0; i < array.length; ++i) {
                    \u3007\u3007\u30070\u3007\u30070((BufferedSink)buffer, array[i]);
                    buffer.readByte();
                    array2[i] = buffer.readByteString();
                }
                return new Options(array.clone(), okio.Options.of(array2));
            }
            catch (final IOException detailMessage) {
                throw new AssertionError((Object)detailMessage);
            }
        }
    }
    
    public enum Token
    {
        private static final Token[] $VALUES;
        
        BEGIN_ARRAY, 
        BEGIN_OBJECT, 
        BOOLEAN, 
        END_ARRAY, 
        END_DOCUMENT, 
        END_OBJECT, 
        NAME, 
        NULL, 
        NUMBER, 
        STRING;
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.airbnb.lottie.parser;

import java.io.IOException;
import com.airbnb.lottie.model.content.MergePaths;
import com.airbnb.lottie.parser.moshi.JsonReader;

class MergePathsParser
{
    private static final JsonReader.Options \u3007080;
    
    static {
        \u3007080 = JsonReader.Options.\u3007080("nm", "mm", "hd");
    }
    
    static MergePaths \u3007080(final JsonReader jsonReader) throws IOException {
        String o800o8O = null;
        MergePaths.MergePathsMode forId = null;
        boolean \u3007\u3007808\u3007 = false;
        while (jsonReader.OO0o\u3007\u3007()) {
            final int ooo\u3007O0 = jsonReader.OOO\u3007O0(MergePathsParser.\u3007080);
            if (ooo\u3007O0 != 0) {
                if (ooo\u3007O0 != 1) {
                    if (ooo\u3007O0 != 2) {
                        jsonReader.O8\u3007o();
                        jsonReader.\u300700\u30078();
                    }
                    else {
                        \u3007\u3007808\u3007 = jsonReader.\u3007\u3007808\u3007();
                    }
                }
                else {
                    forId = MergePaths.MergePathsMode.forId(jsonReader.\u3007O00());
                }
            }
            else {
                o800o8O = jsonReader.o800o8O();
            }
        }
        return new MergePaths(o800o8O, forId, \u3007\u3007808\u3007);
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.airbnb.lottie.parser;

import java.io.IOException;
import com.airbnb.lottie.model.animatable.AnimatableIntegerValue;
import com.airbnb.lottie.model.animatable.AnimatableColorValue;
import android.graphics.Path$FillType;
import com.airbnb.lottie.model.content.ShapeFill;
import com.airbnb.lottie.LottieComposition;
import com.airbnb.lottie.parser.moshi.JsonReader;

class ShapeFillParser
{
    private static final JsonReader.Options \u3007080;
    
    static {
        \u3007080 = JsonReader.Options.\u3007080("nm", "c", "o", "fillEnabled", "r", "hd");
    }
    
    static ShapeFill \u3007080(final JsonReader jsonReader, final LottieComposition lottieComposition) throws IOException {
        String o800o8O = null;
        AnimatableIntegerValue oo80;
        BaseAnimatableValue<Integer, Integer> \u3007o\u3007 = oo80 = null;
        int \u3007o00 = 1;
        boolean \u3007\u3007808\u3007 = false;
        boolean \u3007\u3007808\u30072 = false;
        while (jsonReader.OO0o\u3007\u3007()) {
            final int ooo\u3007O0 = jsonReader.OOO\u3007O0(ShapeFillParser.\u3007080);
            if (ooo\u3007O0 != 0) {
                if (ooo\u3007O0 != 1) {
                    if (ooo\u3007O0 != 2) {
                        if (ooo\u3007O0 != 3) {
                            if (ooo\u3007O0 != 4) {
                                if (ooo\u3007O0 != 5) {
                                    jsonReader.O8\u3007o();
                                    jsonReader.\u300700\u30078();
                                }
                                else {
                                    \u3007\u3007808\u30072 = jsonReader.\u3007\u3007808\u3007();
                                }
                            }
                            else {
                                \u3007o00 = jsonReader.\u3007O00();
                            }
                        }
                        else {
                            \u3007\u3007808\u3007 = jsonReader.\u3007\u3007808\u3007();
                        }
                    }
                    else {
                        oo80 = AnimatableValueParser.oO80(jsonReader, lottieComposition);
                    }
                }
                else {
                    \u3007o\u3007 = AnimatableValueParser.\u3007o\u3007(jsonReader, lottieComposition);
                }
            }
            else {
                o800o8O = jsonReader.o800o8O();
            }
        }
        Path$FillType path$FillType;
        if (\u3007o00 == 1) {
            path$FillType = Path$FillType.WINDING;
        }
        else {
            path$FillType = Path$FillType.EVEN_ODD;
        }
        return new ShapeFill(o800o8O, \u3007\u3007808\u3007, path$FillType, (AnimatableColorValue)\u3007o\u3007, oo80, \u3007\u3007808\u30072);
    }
}

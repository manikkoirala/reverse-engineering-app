// 
// Decompiled by Procyon v0.6.0
// 

package com.airbnb.lottie.parser;

import java.io.IOException;
import com.airbnb.lottie.model.animatable.AnimatableFloatValue;
import com.airbnb.lottie.model.animatable.AnimatablePointValue;
import android.graphics.PointF;
import com.airbnb.lottie.model.animatable.AnimatableValue;
import com.airbnb.lottie.model.content.RectangleShape;
import com.airbnb.lottie.LottieComposition;
import com.airbnb.lottie.parser.moshi.JsonReader;

class RectangleShapeParser
{
    private static JsonReader.Options \u3007080;
    
    static {
        RectangleShapeParser.\u3007080 = JsonReader.Options.\u3007080("nm", "p", "s", "r", "hd");
    }
    
    static RectangleShape \u3007080(final JsonReader jsonReader, final LottieComposition lottieComposition) throws IOException {
        String o800o8O = null;
        Object \u3007o00\u3007\u3007Oo = null;
        Object oo08;
        AnimatableValue<PointF, PointF> \u300780\u3007808\u3007O = (AnimatableValue<PointF, PointF>)(oo08 = \u3007o00\u3007\u3007Oo);
        boolean \u3007\u3007808\u3007 = false;
        while (jsonReader.OO0o\u3007\u3007()) {
            final int ooo\u3007O0 = jsonReader.OOO\u3007O0(RectangleShapeParser.\u3007080);
            if (ooo\u3007O0 != 0) {
                if (ooo\u3007O0 != 1) {
                    if (ooo\u3007O0 != 2) {
                        if (ooo\u3007O0 != 3) {
                            if (ooo\u3007O0 != 4) {
                                jsonReader.\u300700\u30078();
                            }
                            else {
                                \u3007\u3007808\u3007 = jsonReader.\u3007\u3007808\u3007();
                            }
                        }
                        else {
                            oo08 = AnimatableValueParser.Oo08(jsonReader, lottieComposition);
                        }
                    }
                    else {
                        \u300780\u3007808\u3007O = AnimatableValueParser.\u300780\u3007808\u3007O(jsonReader, lottieComposition);
                    }
                }
                else {
                    \u3007o00\u3007\u3007Oo = AnimatablePathValueParser.\u3007o00\u3007\u3007Oo(jsonReader, lottieComposition);
                }
            }
            else {
                o800o8O = jsonReader.o800o8O();
            }
        }
        return new RectangleShape(o800o8O, (AnimatableValue<PointF, PointF>)\u3007o00\u3007\u3007Oo, (AnimatablePointValue)\u300780\u3007808\u3007O, (AnimatableFloatValue)oo08, \u3007\u3007808\u3007);
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.airbnb.lottie.parser;

import java.io.IOException;
import com.airbnb.lottie.parser.moshi.JsonReader;

public class FloatParser implements ValueParser<Float>
{
    public static final FloatParser \u3007080;
    
    static {
        \u3007080 = new FloatParser();
    }
    
    private FloatParser() {
    }
    
    public Float \u3007o00\u3007\u3007Oo(final JsonReader jsonReader, final float n) throws IOException {
        return JsonUtils.\u3007\u3007888(jsonReader) * n;
    }
}

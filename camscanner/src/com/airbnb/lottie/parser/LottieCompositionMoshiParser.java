// 
// Decompiled by Procyon v0.6.0
// 

package com.airbnb.lottie.parser;

import android.graphics.Rect;
import com.airbnb.lottie.LottieImageAsset;
import com.airbnb.lottie.model.FontCharacter;
import androidx.collection.SparseArrayCompat;
import java.util.HashMap;
import java.util.ArrayList;
import com.airbnb.lottie.utils.Utils;
import com.airbnb.lottie.model.Marker;
import com.airbnb.lottie.utils.Logger;
import androidx.collection.LongSparseArray;
import com.airbnb.lottie.model.layer.Layer;
import java.util.List;
import com.airbnb.lottie.LottieComposition;
import java.io.IOException;
import com.airbnb.lottie.model.Font;
import java.util.Map;
import com.airbnb.lottie.parser.moshi.JsonReader;

public class LottieCompositionMoshiParser
{
    private static final JsonReader.Options O8;
    private static final JsonReader.Options \u3007080;
    static JsonReader.Options \u3007o00\u3007\u3007Oo;
    private static final JsonReader.Options \u3007o\u3007;
    
    static {
        \u3007080 = JsonReader.Options.\u3007080("w", "h", "ip", "op", "fr", "v", "layers", "assets", "fonts", "chars", "markers");
        LottieCompositionMoshiParser.\u3007o00\u3007\u3007Oo = JsonReader.Options.\u3007080("id", "layers", "w", "h", "p", "u");
        \u3007o\u3007 = JsonReader.Options.\u3007080("list");
        O8 = JsonReader.Options.\u3007080("cm", "tm", "dr");
    }
    
    private static void O8(final JsonReader jsonReader, final Map<String, Font> map) throws IOException {
        jsonReader.oO80();
        while (jsonReader.OO0o\u3007\u3007()) {
            if (jsonReader.OOO\u3007O0(LottieCompositionMoshiParser.\u3007o\u3007) != 0) {
                jsonReader.O8\u3007o();
                jsonReader.\u300700\u30078();
            }
            else {
                jsonReader.\u3007\u3007888();
                while (jsonReader.OO0o\u3007\u3007()) {
                    final Font \u3007080 = FontParser.\u3007080(jsonReader);
                    map.put(\u3007080.\u3007o00\u3007\u3007Oo(), \u3007080);
                }
                jsonReader.\u30078o8o\u3007();
            }
        }
        jsonReader.\u3007O8o08O();
    }
    
    private static void Oo08(final JsonReader jsonReader, final LottieComposition lottieComposition, final List<Layer> list, final LongSparseArray<Layer> longSparseArray) throws IOException {
        jsonReader.\u3007\u3007888();
        int n = 0;
        while (jsonReader.OO0o\u3007\u3007()) {
            final Layer \u3007o00\u3007\u3007Oo = LayerParser.\u3007o00\u3007\u3007Oo(jsonReader, lottieComposition);
            int i = n;
            if (\u3007o00\u3007\u3007Oo.O8() == Layer.LayerType.IMAGE) {
                i = n + 1;
            }
            list.add(\u3007o00\u3007\u3007Oo);
            longSparseArray.put(\u3007o00\u3007\u3007Oo.\u3007o00\u3007\u3007Oo(), \u3007o00\u3007\u3007Oo);
            if ((n = i) > 4) {
                final StringBuilder sb = new StringBuilder();
                sb.append("You have ");
                sb.append(i);
                sb.append(" images. Lottie should primarily be used with shapes. If you are using Adobe Illustrator, convert the Illustrator layers to shape layers.");
                Logger.\u3007o\u3007(sb.toString());
                n = i;
            }
        }
        jsonReader.\u30078o8o\u3007();
    }
    
    private static void o\u30070(final JsonReader jsonReader, final LottieComposition lottieComposition, final List<Marker> list) throws IOException {
        jsonReader.\u3007\u3007888();
        while (jsonReader.OO0o\u3007\u3007()) {
            jsonReader.oO80();
            float n = 0.0f;
            String o800o8O = null;
            float n2 = 0.0f;
            while (jsonReader.OO0o\u3007\u3007()) {
                final int ooo\u3007O0 = jsonReader.OOO\u3007O0(LottieCompositionMoshiParser.O8);
                if (ooo\u3007O0 != 0) {
                    if (ooo\u3007O0 != 1) {
                        if (ooo\u3007O0 != 2) {
                            jsonReader.O8\u3007o();
                            jsonReader.\u300700\u30078();
                        }
                        else {
                            n2 = (float)jsonReader.\u3007O\u3007();
                        }
                    }
                    else {
                        n = (float)jsonReader.\u3007O\u3007();
                    }
                }
                else {
                    o800o8O = jsonReader.o800o8O();
                }
            }
            jsonReader.\u3007O8o08O();
            list.add(new Marker(o800o8O, n, n2));
        }
        jsonReader.\u30078o8o\u3007();
    }
    
    public static LottieComposition \u3007080(final JsonReader jsonReader) throws IOException {
        final float oo08 = Utils.Oo08();
        final LongSparseArray longSparseArray = new LongSparseArray();
        final ArrayList list = new ArrayList();
        final HashMap hashMap = new HashMap();
        final HashMap hashMap2 = new HashMap();
        final HashMap hashMap3 = new HashMap();
        final ArrayList list2 = new ArrayList();
        final SparseArrayCompat sparseArrayCompat = new SparseArrayCompat();
        final LottieComposition lottieComposition = new LottieComposition();
        jsonReader.oO80();
        int \u3007o00 = 0;
        float n = 0.0f;
        float n2 = 0.0f;
        float n3 = 0.0f;
        int \u3007o2 = 0;
        while (jsonReader.OO0o\u3007\u3007()) {
            switch (jsonReader.OOO\u3007O0(LottieCompositionMoshiParser.\u3007080)) {
                default: {
                    jsonReader.O8\u3007o();
                    jsonReader.\u300700\u30078();
                    continue;
                }
                case 10: {
                    o\u30070(jsonReader, lottieComposition, list2);
                    continue;
                }
                case 9: {
                    \u3007o\u3007(jsonReader, lottieComposition, sparseArrayCompat);
                    continue;
                }
                case 8: {
                    O8(jsonReader, hashMap3);
                    continue;
                }
                case 7: {
                    \u3007o00\u3007\u3007Oo(jsonReader, lottieComposition, hashMap, hashMap2);
                    continue;
                }
                case 6: {
                    Oo08(jsonReader, lottieComposition, list, longSparseArray);
                    continue;
                }
                case 5: {
                    final String[] split = jsonReader.o800o8O().split("\\.");
                    if (!Utils.OO0o\u3007\u3007\u3007\u30070(Integer.parseInt(split[0]), Integer.parseInt(split[1]), Integer.parseInt(split[2]), 4, 4, 0)) {
                        lottieComposition.\u3007080("Lottie only supports bodymovin >= 4.4.0");
                        continue;
                    }
                    continue;
                }
                case 4: {
                    n3 = (float)jsonReader.\u3007O\u3007();
                    continue;
                }
                case 3: {
                    n2 = (float)jsonReader.\u3007O\u3007() - 0.01f;
                    continue;
                }
                case 2: {
                    n = (float)jsonReader.\u3007O\u3007();
                    continue;
                }
                case 1: {
                    \u3007o2 = jsonReader.\u3007O00();
                    continue;
                }
                case 0: {
                    \u3007o00 = jsonReader.\u3007O00();
                    continue;
                }
            }
        }
        lottieComposition.\u3007\u30078O0\u30078(new Rect(0, 0, (int)(\u3007o00 * oo08), (int)(\u3007o2 * oo08)), n, n2, n3, list, longSparseArray, hashMap, hashMap2, sparseArrayCompat, hashMap3, list2);
        return lottieComposition;
    }
    
    private static void \u3007o00\u3007\u3007Oo(final JsonReader jsonReader, final LottieComposition lottieComposition, final Map<String, List<Layer>> map, final Map<String, LottieImageAsset> map2) throws IOException {
        jsonReader.\u3007\u3007888();
        while (jsonReader.OO0o\u3007\u3007()) {
            final ArrayList list = new ArrayList();
            final LongSparseArray<Layer> longSparseArray = new LongSparseArray<Layer>();
            jsonReader.oO80();
            String o800o8O = null;
            String o800o8O3;
            String o800o8O2 = o800o8O3 = null;
            int \u3007o00 = 0;
            int \u3007o2 = 0;
            while (jsonReader.OO0o\u3007\u3007()) {
                final int ooo\u3007O0 = jsonReader.OOO\u3007O0(LottieCompositionMoshiParser.\u3007o00\u3007\u3007Oo);
                if (ooo\u3007O0 != 0) {
                    if (ooo\u3007O0 != 1) {
                        if (ooo\u3007O0 != 2) {
                            if (ooo\u3007O0 != 3) {
                                if (ooo\u3007O0 != 4) {
                                    if (ooo\u3007O0 != 5) {
                                        jsonReader.O8\u3007o();
                                        jsonReader.\u300700\u30078();
                                    }
                                    else {
                                        o800o8O3 = jsonReader.o800o8O();
                                    }
                                }
                                else {
                                    o800o8O2 = jsonReader.o800o8O();
                                }
                            }
                            else {
                                \u3007o2 = jsonReader.\u3007O00();
                            }
                        }
                        else {
                            \u3007o00 = jsonReader.\u3007O00();
                        }
                    }
                    else {
                        jsonReader.\u3007\u3007888();
                        while (jsonReader.OO0o\u3007\u3007()) {
                            final Layer \u3007o00\u3007\u3007Oo = LayerParser.\u3007o00\u3007\u3007Oo(jsonReader, lottieComposition);
                            longSparseArray.put(\u3007o00\u3007\u3007Oo.\u3007o00\u3007\u3007Oo(), \u3007o00\u3007\u3007Oo);
                            list.add(\u3007o00\u3007\u3007Oo);
                        }
                        jsonReader.\u30078o8o\u3007();
                    }
                }
                else {
                    o800o8O = jsonReader.o800o8O();
                }
            }
            jsonReader.\u3007O8o08O();
            if (o800o8O2 != null) {
                final LottieImageAsset lottieImageAsset = new LottieImageAsset(\u3007o00, \u3007o2, o800o8O, o800o8O2, o800o8O3);
                map2.put(lottieImageAsset.O8(), lottieImageAsset);
            }
            else {
                map.put(o800o8O, list);
            }
        }
        jsonReader.\u30078o8o\u3007();
    }
    
    private static void \u3007o\u3007(final JsonReader jsonReader, final LottieComposition lottieComposition, final SparseArrayCompat<FontCharacter> sparseArrayCompat) throws IOException {
        jsonReader.\u3007\u3007888();
        while (jsonReader.OO0o\u3007\u3007()) {
            final FontCharacter \u3007080 = FontCharacterParser.\u3007080(jsonReader, lottieComposition);
            sparseArrayCompat.put(\u3007080.hashCode(), \u3007080);
        }
        jsonReader.\u30078o8o\u3007();
    }
}

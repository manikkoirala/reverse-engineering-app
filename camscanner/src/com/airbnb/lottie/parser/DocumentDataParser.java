// 
// Decompiled by Procyon v0.6.0
// 

package com.airbnb.lottie.parser;

import java.io.IOException;
import com.airbnb.lottie.parser.moshi.JsonReader;
import com.airbnb.lottie.model.DocumentData;

public class DocumentDataParser implements ValueParser<DocumentData>
{
    public static final DocumentDataParser \u3007080;
    private static final JsonReader.Options \u3007o00\u3007\u3007Oo;
    
    static {
        \u3007080 = new DocumentDataParser();
        \u3007o00\u3007\u3007Oo = JsonReader.Options.\u3007080("t", "f", "s", "j", "tr", "lh", "ls", "fc", "sc", "sw", "of");
    }
    
    private DocumentDataParser() {
    }
    
    public DocumentData \u3007o00\u3007\u3007Oo(final JsonReader jsonReader, float n) throws IOException {
        Enum<DocumentData.Justification> enum1 = DocumentData.Justification.CENTER;
        jsonReader.oO80();
        String o800o8O = null;
        String o800o8O2 = null;
        float n2 = 0.0f;
        int \u3007o00 = 0;
        float n3 = 0.0f;
        float n4 = 0.0f;
        int o8 = 0;
        int o9 = 0;
        n = 0.0f;
        boolean \u3007\u3007808\u3007 = true;
        while (jsonReader.OO0o\u3007\u3007()) {
            switch (jsonReader.OOO\u3007O0(DocumentDataParser.\u3007o00\u3007\u3007Oo)) {
                default: {
                    jsonReader.O8\u3007o();
                    jsonReader.\u300700\u30078();
                    continue;
                }
                case 10: {
                    \u3007\u3007808\u3007 = jsonReader.\u3007\u3007808\u3007();
                    continue;
                }
                case 9: {
                    n = (float)jsonReader.\u3007O\u3007();
                    continue;
                }
                case 8: {
                    o9 = JsonUtils.O8(jsonReader);
                    continue;
                }
                case 7: {
                    o8 = JsonUtils.O8(jsonReader);
                    continue;
                }
                case 6: {
                    n4 = (float)jsonReader.\u3007O\u3007();
                    continue;
                }
                case 5: {
                    n3 = (float)jsonReader.\u3007O\u3007();
                    continue;
                }
                case 4: {
                    \u3007o00 = jsonReader.\u3007O00();
                    continue;
                }
                case 3: {
                    final int \u3007o2 = jsonReader.\u3007O00();
                    final DocumentData.Justification justification = (DocumentData.Justification)(enum1 = DocumentData.Justification.CENTER);
                    if (\u3007o2 > justification.ordinal()) {
                        continue;
                    }
                    if (\u3007o2 < 0) {
                        enum1 = justification;
                        continue;
                    }
                    enum1 = DocumentData.Justification.values()[\u3007o2];
                    continue;
                }
                case 2: {
                    n2 = (float)jsonReader.\u3007O\u3007();
                    continue;
                }
                case 1: {
                    o800o8O2 = jsonReader.o800o8O();
                    continue;
                }
                case 0: {
                    o800o8O = jsonReader.o800o8O();
                    continue;
                }
            }
        }
        jsonReader.\u3007O8o08O();
        return new DocumentData(o800o8O, o800o8O2, n2, (DocumentData.Justification)enum1, \u3007o00, n3, n4, o8, o9, n, \u3007\u3007808\u3007);
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.airbnb.lottie.parser;

import java.io.IOException;
import com.airbnb.lottie.parser.moshi.JsonReader;

interface ValueParser<V>
{
    V \u3007080(final JsonReader p0, final float p1) throws IOException;
}

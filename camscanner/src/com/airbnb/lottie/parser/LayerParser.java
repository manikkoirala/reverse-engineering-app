// 
// Decompiled by Procyon v0.6.0
// 

package com.airbnb.lottie.parser;

import java.io.IOException;
import com.airbnb.lottie.model.content.Mask;
import com.airbnb.lottie.model.content.ContentModel;
import java.util.List;
import android.view.animation.Interpolator;
import com.airbnb.lottie.value.Keyframe;
import android.graphics.Color;
import com.airbnb.lottie.utils.Utils;
import java.util.ArrayList;
import android.graphics.Rect;
import com.airbnb.lottie.model.animatable.AnimatableFloatValue;
import com.airbnb.lottie.model.animatable.AnimatableTextProperties;
import com.airbnb.lottie.model.animatable.AnimatableTextFrame;
import com.airbnb.lottie.model.animatable.AnimatableTransform;
import java.util.Collections;
import com.airbnb.lottie.model.layer.Layer;
import com.airbnb.lottie.LottieComposition;
import com.airbnb.lottie.parser.moshi.JsonReader;

public class LayerParser
{
    private static final JsonReader.Options \u3007080;
    private static final JsonReader.Options \u3007o00\u3007\u3007Oo;
    private static final JsonReader.Options \u3007o\u3007;
    
    static {
        \u3007080 = JsonReader.Options.\u3007080("nm", "ind", "refId", "ty", "parent", "sw", "sh", "sc", "ks", "tt", "masksProperties", "shapes", "t", "ef", "sr", "st", "w", "h", "ip", "op", "tm", "cl", "hd");
        \u3007o00\u3007\u3007Oo = JsonReader.Options.\u3007080("d", "a");
        \u3007o\u3007 = JsonReader.Options.\u3007080("nm");
    }
    
    public static Layer \u3007080(final LottieComposition lottieComposition) {
        final Rect \u3007o00\u3007\u3007Oo = lottieComposition.\u3007o00\u3007\u3007Oo();
        return new Layer(Collections.emptyList(), lottieComposition, "__container", -1L, Layer.LayerType.PRE_COMP, -1L, null, Collections.emptyList(), new AnimatableTransform(), 0, 0, 0, 0.0f, 0.0f, \u3007o00\u3007\u3007Oo.width(), \u3007o00\u3007\u3007Oo.height(), null, null, Collections.emptyList(), Layer.MatteType.NONE, null, false);
    }
    
    public static Layer \u3007o00\u3007\u3007Oo(final JsonReader jsonReader, final LottieComposition lottieComposition) throws IOException {
        Enum<Layer.MatteType> none = Layer.MatteType.NONE;
        final ArrayList list = new ArrayList();
        final ArrayList list2 = new ArrayList();
        jsonReader.oO80();
        final Float value = 1.0f;
        final Float value2 = 0.0f;
        final String s = "UNSET";
        final String s2 = null;
        final AnimatableFloatValue animatableFloatValue2;
        final AnimatableFloatValue animatableFloatValue = animatableFloatValue2 = null;
        AnimatableFloatValue o\u30070;
        final AnimatableFloatValue animatableFloatValue3 = o\u30070 = animatableFloatValue2;
        long n = 0L;
        long n2 = -1L;
        float n3 = 0.0f;
        float n4 = 0.0f;
        int n5 = 0;
        int n6 = 0;
        int color = 0;
        float n7 = 1.0f;
        float n8 = 0.0f;
        int n9 = 0;
        int n10 = 0;
        boolean \u3007\u3007808\u3007 = false;
        final AnimatableFloatValue animatableFloatValue4;
        Object o800o8O = animatableFloatValue4 = o\u30070;
        Object \u3007080 = animatableFloatValue3;
        Object o8 = animatableFloatValue2;
        Object \u3007\u3007888 = animatableFloatValue;
        String o800o8O2 = s2;
        Enum<Layer.LayerType> unknown = (Enum<Layer.LayerType>)animatableFloatValue4;
        String o800o8O3 = s;
        while (jsonReader.OO0o\u3007\u3007()) {
            switch (jsonReader.OOO\u3007O0(LayerParser.\u3007080)) {
                default: {
                    jsonReader.O8\u3007o();
                    jsonReader.\u300700\u30078();
                    continue;
                }
                case 22: {
                    \u3007\u3007808\u3007 = jsonReader.\u3007\u3007808\u3007();
                    continue;
                }
                case 21: {
                    o800o8O = jsonReader.o800o8O();
                    continue;
                }
                case 20: {
                    o\u30070 = AnimatableValueParser.o\u30070(jsonReader, lottieComposition, false);
                    continue;
                }
                case 19: {
                    n4 = (float)jsonReader.\u3007O\u3007();
                    continue;
                }
                case 18: {
                    n3 = (float)jsonReader.\u3007O\u3007();
                    continue;
                }
                case 17: {
                    n10 = (int)(jsonReader.\u3007O00() * Utils.Oo08());
                    continue;
                }
                case 16: {
                    n9 = (int)(jsonReader.\u3007O00() * Utils.Oo08());
                    continue;
                }
                case 15: {
                    n8 = (float)jsonReader.\u3007O\u3007();
                    continue;
                }
                case 14: {
                    n7 = (float)jsonReader.\u3007O\u3007();
                    continue;
                }
                case 13: {
                    jsonReader.\u3007\u3007888();
                    final ArrayList obj = new ArrayList();
                    while (jsonReader.OO0o\u3007\u3007()) {
                        jsonReader.oO80();
                        while (jsonReader.OO0o\u3007\u3007()) {
                            if (jsonReader.OOO\u3007O0(LayerParser.\u3007o\u3007) != 0) {
                                jsonReader.O8\u3007o();
                                jsonReader.\u300700\u30078();
                            }
                            else {
                                obj.add(jsonReader.o800o8O());
                            }
                        }
                        jsonReader.\u3007O8o08O();
                    }
                    jsonReader.\u30078o8o\u3007();
                    final StringBuilder sb = new StringBuilder();
                    sb.append("Lottie doesn't support layer effects. If you are using them for  fills, strokes, trim paths etc. then try adding them directly as contents  in your shape. Found: ");
                    sb.append(obj);
                    lottieComposition.\u3007080(sb.toString());
                    continue;
                }
                case 12: {
                    jsonReader.oO80();
                    while (jsonReader.OO0o\u3007\u3007()) {
                        final int ooo\u3007O0 = jsonReader.OOO\u3007O0(LayerParser.\u3007o00\u3007\u3007Oo);
                        if (ooo\u3007O0 != 0) {
                            if (ooo\u3007O0 != 1) {
                                jsonReader.O8\u3007o();
                                jsonReader.\u300700\u30078();
                            }
                            else {
                                jsonReader.\u3007\u3007888();
                                if (jsonReader.OO0o\u3007\u3007()) {
                                    \u3007080 = AnimatableTextPropertiesParser.\u3007080(jsonReader, lottieComposition);
                                }
                                while (jsonReader.OO0o\u3007\u3007()) {
                                    jsonReader.\u300700\u30078();
                                }
                                jsonReader.\u30078o8o\u3007();
                            }
                        }
                        else {
                            o8 = AnimatableValueParser.O8(jsonReader, lottieComposition);
                        }
                    }
                    jsonReader.\u3007O8o08O();
                    continue;
                }
                case 11: {
                    jsonReader.\u3007\u3007888();
                    while (jsonReader.OO0o\u3007\u3007()) {
                        final ContentModel \u300781 = ContentModelParser.\u3007080(jsonReader, lottieComposition);
                        if (\u300781 != null) {
                            list2.add(\u300781);
                        }
                    }
                    jsonReader.\u30078o8o\u3007();
                    continue;
                }
                case 10: {
                    jsonReader.\u3007\u3007888();
                    while (jsonReader.OO0o\u3007\u3007()) {
                        list.add(MaskParser.\u3007080(jsonReader, lottieComposition));
                    }
                    lottieComposition.\u3007O00(list.size());
                    jsonReader.\u30078o8o\u3007();
                    continue;
                }
                case 9: {
                    none = Layer.MatteType.values()[jsonReader.\u3007O00()];
                    lottieComposition.\u3007O00(1);
                    continue;
                }
                case 8: {
                    \u3007\u3007888 = AnimatableTransformParser.\u3007\u3007888(jsonReader, lottieComposition);
                    continue;
                }
                case 7: {
                    color = Color.parseColor(jsonReader.o800o8O());
                    continue;
                }
                case 6: {
                    n6 = (int)(jsonReader.\u3007O00() * Utils.Oo08());
                    continue;
                }
                case 5: {
                    n5 = (int)(jsonReader.\u3007O00() * Utils.Oo08());
                    continue;
                }
                case 4: {
                    n2 = jsonReader.\u3007O00();
                    continue;
                }
                case 3: {
                    final int \u3007o00 = jsonReader.\u3007O00();
                    if (\u3007o00 < (unknown = Layer.LayerType.UNKNOWN).ordinal()) {
                        unknown = Layer.LayerType.values()[\u3007o00];
                        continue;
                    }
                    continue;
                }
                case 2: {
                    o800o8O2 = jsonReader.o800o8O();
                    continue;
                }
                case 1: {
                    n = jsonReader.\u3007O00();
                    continue;
                }
                case 0: {
                    o800o8O3 = jsonReader.o800o8O();
                    continue;
                }
            }
        }
        jsonReader.\u3007O8o08O();
        final float f = n3 / n7;
        float o\u30072 = n4 / n7;
        final ArrayList list3 = new ArrayList();
        if (f > 0.0f) {
            list3.add(new Keyframe(lottieComposition, value2, value2, null, 0.0f, f));
        }
        if (o\u30072 <= 0.0f) {
            o\u30072 = lottieComposition.o\u30070();
        }
        list3.add(new Keyframe(lottieComposition, value, value, null, f, o\u30072));
        list3.add(new Keyframe(lottieComposition, value2, value2, null, o\u30072, Float.MAX_VALUE));
        if (o800o8O3.endsWith(".ai") || "ai".equals(o800o8O)) {
            lottieComposition.\u3007080("Convert your Illustrator layers to shape layers.");
        }
        return new Layer(list2, lottieComposition, o800o8O3, n, (Layer.LayerType)unknown, n2, o800o8O2, list, (AnimatableTransform)\u3007\u3007888, n5, n6, color, n7, n8, n9, n10, (AnimatableTextFrame)o8, (AnimatableTextProperties)\u3007080, list3, (Layer.MatteType)none, o\u30070, \u3007\u3007808\u3007);
    }
}

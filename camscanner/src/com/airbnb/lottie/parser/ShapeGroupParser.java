// 
// Decompiled by Procyon v0.6.0
// 

package com.airbnb.lottie.parser;

import java.io.IOException;
import com.airbnb.lottie.model.content.ContentModel;
import java.util.List;
import java.util.ArrayList;
import com.airbnb.lottie.model.content.ShapeGroup;
import com.airbnb.lottie.LottieComposition;
import com.airbnb.lottie.parser.moshi.JsonReader;

class ShapeGroupParser
{
    private static JsonReader.Options \u3007080;
    
    static {
        ShapeGroupParser.\u3007080 = JsonReader.Options.\u3007080("nm", "hd", "it");
    }
    
    static ShapeGroup \u3007080(final JsonReader jsonReader, final LottieComposition lottieComposition) throws IOException {
        final ArrayList list = new ArrayList();
        String o800o8O = null;
        boolean \u3007\u3007808\u3007 = false;
        while (jsonReader.OO0o\u3007\u3007()) {
            final int ooo\u3007O0 = jsonReader.OOO\u3007O0(ShapeGroupParser.\u3007080);
            if (ooo\u3007O0 != 0) {
                if (ooo\u3007O0 != 1) {
                    if (ooo\u3007O0 != 2) {
                        jsonReader.\u300700\u30078();
                    }
                    else {
                        jsonReader.\u3007\u3007888();
                        while (jsonReader.OO0o\u3007\u3007()) {
                            final ContentModel \u3007080 = ContentModelParser.\u3007080(jsonReader, lottieComposition);
                            if (\u3007080 != null) {
                                list.add(\u3007080);
                            }
                        }
                        jsonReader.\u30078o8o\u3007();
                    }
                }
                else {
                    \u3007\u3007808\u3007 = jsonReader.\u3007\u3007808\u3007();
                }
            }
            else {
                o800o8O = jsonReader.o800o8O();
            }
        }
        return new ShapeGroup(o800o8O, list, \u3007\u3007808\u3007);
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.airbnb.lottie.parser;

import java.io.IOException;
import com.airbnb.lottie.parser.moshi.JsonReader;

public class IntegerParser implements ValueParser<Integer>
{
    public static final IntegerParser \u3007080;
    
    static {
        \u3007080 = new IntegerParser();
    }
    
    private IntegerParser() {
    }
    
    public Integer \u3007o00\u3007\u3007Oo(final JsonReader jsonReader, final float n) throws IOException {
        return Math.round(JsonUtils.\u3007\u3007888(jsonReader) * n);
    }
}

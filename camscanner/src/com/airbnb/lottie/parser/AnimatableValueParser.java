// 
// Decompiled by Procyon v0.6.0
// 

package com.airbnb.lottie.parser;

import com.airbnb.lottie.model.content.GradientColor;
import com.airbnb.lottie.model.animatable.AnimatableGradientColorValue;
import com.airbnb.lottie.model.animatable.AnimatableColorValue;
import com.airbnb.lottie.model.content.ShapeData;
import com.airbnb.lottie.model.animatable.AnimatableShapeValue;
import android.graphics.PointF;
import com.airbnb.lottie.model.animatable.AnimatablePointValue;
import androidx.annotation.Nullable;
import com.airbnb.lottie.value.Keyframe;
import java.util.List;
import com.airbnb.lottie.utils.Utils;
import com.airbnb.lottie.model.animatable.AnimatableIntegerValue;
import com.airbnb.lottie.model.animatable.AnimatableFloatValue;
import com.airbnb.lottie.value.ScaleXY;
import com.airbnb.lottie.model.animatable.AnimatableScaleValue;
import java.io.IOException;
import com.airbnb.lottie.model.DocumentData;
import com.airbnb.lottie.model.animatable.AnimatableTextFrame;
import com.airbnb.lottie.LottieComposition;
import com.airbnb.lottie.parser.moshi.JsonReader;

public class AnimatableValueParser
{
    static AnimatableTextFrame O8(final JsonReader jsonReader, final LottieComposition lottieComposition) throws IOException {
        return new AnimatableTextFrame(\u3007o00\u3007\u3007Oo(jsonReader, lottieComposition, (ValueParser<DocumentData>)DocumentDataParser.\u3007080));
    }
    
    static AnimatableScaleValue OO0o\u3007\u3007\u3007\u30070(final JsonReader jsonReader, final LottieComposition lottieComposition) throws IOException {
        return new AnimatableScaleValue(\u3007o00\u3007\u3007Oo(jsonReader, lottieComposition, (ValueParser<ScaleXY>)ScaleXYParser.\u3007080));
    }
    
    public static AnimatableFloatValue Oo08(final JsonReader jsonReader, final LottieComposition lottieComposition) throws IOException {
        return o\u30070(jsonReader, lottieComposition, true);
    }
    
    static AnimatableIntegerValue oO80(final JsonReader jsonReader, final LottieComposition lottieComposition) throws IOException {
        return new AnimatableIntegerValue(\u3007o00\u3007\u3007Oo(jsonReader, lottieComposition, (ValueParser<Integer>)IntegerParser.\u3007080));
    }
    
    public static AnimatableFloatValue o\u30070(final JsonReader jsonReader, final LottieComposition lottieComposition, final boolean b) throws IOException {
        float oo08;
        if (b) {
            oo08 = Utils.Oo08();
        }
        else {
            oo08 = 1.0f;
        }
        return new AnimatableFloatValue(\u3007080(jsonReader, oo08, lottieComposition, (ValueParser<Float>)FloatParser.\u3007080));
    }
    
    @Nullable
    private static <T> List<Keyframe<T>> \u3007080(final JsonReader jsonReader, final float n, final LottieComposition lottieComposition, final ValueParser<T> valueParser) throws IOException {
        return KeyframesParser.\u3007080(jsonReader, lottieComposition, n, valueParser);
    }
    
    static AnimatablePointValue \u300780\u3007808\u3007O(final JsonReader jsonReader, final LottieComposition lottieComposition) throws IOException {
        return new AnimatablePointValue(\u3007080(jsonReader, Utils.Oo08(), lottieComposition, (ValueParser<PointF>)PointFParser.\u3007080));
    }
    
    static AnimatableShapeValue \u30078o8o\u3007(final JsonReader jsonReader, final LottieComposition lottieComposition) throws IOException {
        return new AnimatableShapeValue(\u3007080(jsonReader, Utils.Oo08(), lottieComposition, (ValueParser<ShapeData>)ShapeDataParser.\u3007080));
    }
    
    @Nullable
    private static <T> List<Keyframe<T>> \u3007o00\u3007\u3007Oo(final JsonReader jsonReader, final LottieComposition lottieComposition, final ValueParser<T> valueParser) throws IOException {
        return KeyframesParser.\u3007080(jsonReader, lottieComposition, 1.0f, valueParser);
    }
    
    static AnimatableColorValue \u3007o\u3007(final JsonReader jsonReader, final LottieComposition lottieComposition) throws IOException {
        return new AnimatableColorValue(\u3007o00\u3007\u3007Oo(jsonReader, lottieComposition, (ValueParser<Integer>)ColorParser.\u3007080));
    }
    
    static AnimatableGradientColorValue \u3007\u3007888(final JsonReader jsonReader, final LottieComposition lottieComposition, final int n) throws IOException {
        return new AnimatableGradientColorValue(\u3007o00\u3007\u3007Oo(jsonReader, lottieComposition, (ValueParser<GradientColor>)new GradientColorParser(n)));
    }
}

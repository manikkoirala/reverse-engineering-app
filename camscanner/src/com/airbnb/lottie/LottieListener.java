// 
// Decompiled by Procyon v0.6.0
// 

package com.airbnb.lottie;

public interface LottieListener<T>
{
    void onResult(final T p0);
}

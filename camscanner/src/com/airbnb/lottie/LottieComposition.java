// 
// Decompiled by Procyon v0.6.0
// 

package com.airbnb.lottie;

import com.airbnb.lottie.utils.Logger;
import java.util.Iterator;
import androidx.annotation.Nullable;
import androidx.annotation.RestrictTo;
import com.airbnb.lottie.model.FontCharacter;
import androidx.collection.SparseArrayCompat;
import java.util.HashSet;
import com.airbnb.lottie.model.Marker;
import java.util.List;
import com.airbnb.lottie.model.layer.Layer;
import androidx.collection.LongSparseArray;
import com.airbnb.lottie.model.Font;
import android.graphics.Rect;
import java.util.Map;

public class LottieComposition
{
    private Map<String, LottieImageAsset> O8;
    private float OO0o\u3007\u3007;
    private Rect OO0o\u3007\u3007\u3007\u30070;
    private Map<String, Font> Oo08;
    private boolean Oooo8o0\u3007;
    private LongSparseArray<Layer> oO80;
    private List<Marker> o\u30070;
    private final PerformanceTracker \u3007080;
    private List<Layer> \u300780\u3007808\u3007O;
    private float \u30078o8o\u3007;
    private float \u3007O8o08O;
    private final HashSet<String> \u3007o00\u3007\u3007Oo;
    private Map<String, List<Layer>> \u3007o\u3007;
    private int \u3007\u3007808\u3007;
    private SparseArrayCompat<FontCharacter> \u3007\u3007888;
    
    public LottieComposition() {
        this.\u3007080 = new PerformanceTracker();
        this.\u3007o00\u3007\u3007Oo = new HashSet<String>();
        this.\u3007\u3007808\u3007 = 0;
    }
    
    public float O8() {
        return (float)(long)(this.Oo08() / this.OO0o\u3007\u3007 * 1000.0f);
    }
    
    public PerformanceTracker OO0o\u3007\u3007() {
        return this.\u3007080;
    }
    
    public List<Layer> OO0o\u3007\u3007\u3007\u30070() {
        return this.\u300780\u3007808\u3007O;
    }
    
    public float Oo08() {
        return this.\u3007O8o08O - this.\u30078o8o\u3007;
    }
    
    @RestrictTo({ RestrictTo.Scope.LIBRARY })
    public void OoO8(final boolean oooo8o0\u3007) {
        this.Oooo8o0\u3007 = oooo8o0\u3007;
    }
    
    @Nullable
    @RestrictTo({ RestrictTo.Scope.LIBRARY })
    public List<Layer> Oooo8o0\u3007(final String s) {
        return this.\u3007o\u3007.get(s);
    }
    
    public void o800o8O(final boolean b) {
        this.\u3007080.\u3007o00\u3007\u3007Oo(b);
    }
    
    public float oO80() {
        return this.OO0o\u3007\u3007;
    }
    
    @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP })
    public float o\u30070() {
        return this.\u3007O8o08O;
    }
    
    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("LottieComposition:\n");
        final Iterator<Layer> iterator = this.\u300780\u3007808\u3007O.iterator();
        while (iterator.hasNext()) {
            sb.append(iterator.next().oo88o8O("\t"));
        }
        return sb.toString();
    }
    
    @RestrictTo({ RestrictTo.Scope.LIBRARY })
    public void \u3007080(final String e) {
        Logger.\u3007o\u3007(e);
        this.\u3007o00\u3007\u3007Oo.add(e);
    }
    
    @RestrictTo({ RestrictTo.Scope.LIBRARY })
    public Layer \u30070\u3007O0088o(final long n) {
        return this.oO80.get(n);
    }
    
    public Map<String, LottieImageAsset> \u300780\u3007808\u3007O() {
        return this.O8;
    }
    
    @Nullable
    public Marker \u30078o8o\u3007(final String s) {
        this.o\u30070.size();
        for (int i = 0; i < this.o\u30070.size(); ++i) {
            final Marker marker = this.o\u30070.get(i);
            if (marker.\u3007080(s)) {
                return marker;
            }
        }
        return null;
    }
    
    @RestrictTo({ RestrictTo.Scope.LIBRARY })
    public void \u3007O00(final int n) {
        this.\u3007\u3007808\u3007 += n;
    }
    
    @RestrictTo({ RestrictTo.Scope.LIBRARY })
    public int \u3007O8o08O() {
        return this.\u3007\u3007808\u3007;
    }
    
    @RestrictTo({ RestrictTo.Scope.LIBRARY })
    public boolean \u3007O\u3007() {
        return this.Oooo8o0\u3007;
    }
    
    public Rect \u3007o00\u3007\u3007Oo() {
        return this.OO0o\u3007\u3007\u3007\u30070;
    }
    
    public SparseArrayCompat<FontCharacter> \u3007o\u3007() {
        return this.\u3007\u3007888;
    }
    
    @RestrictTo({ RestrictTo.Scope.LIBRARY_GROUP })
    public float \u3007\u3007808\u3007() {
        return this.\u30078o8o\u3007;
    }
    
    public Map<String, Font> \u3007\u3007888() {
        return this.Oo08;
    }
    
    @RestrictTo({ RestrictTo.Scope.LIBRARY })
    public void \u3007\u30078O0\u30078(final Rect oo0o\u3007\u3007\u3007\u30070, final float \u30078o8o\u3007, final float \u3007o8o08O, final float oo0o\u3007\u3007, final List<Layer> \u300780\u3007808\u3007O, final LongSparseArray<Layer> oo80, final Map<String, List<Layer>> \u3007o\u3007, final Map<String, LottieImageAsset> o8, final SparseArrayCompat<FontCharacter> \u3007\u3007888, final Map<String, Font> oo81, final List<Marker> o\u30070) {
        this.OO0o\u3007\u3007\u3007\u30070 = oo0o\u3007\u3007\u3007\u30070;
        this.\u30078o8o\u3007 = \u30078o8o\u3007;
        this.\u3007O8o08O = \u3007o8o08O;
        this.OO0o\u3007\u3007 = oo0o\u3007\u3007;
        this.\u300780\u3007808\u3007O = \u300780\u3007808\u3007O;
        this.oO80 = oo80;
        this.\u3007o\u3007 = \u3007o\u3007;
        this.O8 = o8;
        this.\u3007\u3007888 = \u3007\u3007888;
        this.Oo08 = oo81;
        this.o\u30070 = o\u30070;
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.airbnb.lottie;

import java.util.concurrent.ExecutionException;
import java.util.concurrent.FutureTask;
import com.airbnb.lottie.utils.Logger;
import java.util.Iterator;
import java.util.Collection;
import java.util.ArrayList;
import android.os.Looper;
import java.util.LinkedHashSet;
import androidx.annotation.RestrictTo;
import java.util.concurrent.Callable;
import java.util.concurrent.Executors;
import android.os.Handler;
import java.util.Set;
import androidx.annotation.Nullable;
import java.util.concurrent.Executor;

public class LottieTask<T>
{
    public static Executor Oo08;
    @Nullable
    private volatile LottieResult<T> O8;
    private final Set<LottieListener<T>> \u3007080;
    private final Set<LottieListener<Throwable>> \u3007o00\u3007\u3007Oo;
    private final Handler \u3007o\u3007;
    
    static {
        LottieTask.Oo08 = Executors.newCachedThreadPool();
    }
    
    @RestrictTo({ RestrictTo.Scope.LIBRARY })
    public LottieTask(final Callable<LottieResult<T>> callable) {
        this(callable, false);
    }
    
    @RestrictTo({ RestrictTo.Scope.LIBRARY })
    LottieTask(final Callable<LottieResult<T>> callable, final boolean b) {
        this.\u3007080 = new LinkedHashSet<LottieListener<T>>(1);
        this.\u3007o00\u3007\u3007Oo = new LinkedHashSet<LottieListener<Throwable>>(1);
        this.\u3007o\u3007 = new Handler(Looper.getMainLooper());
        this.O8 = null;
        if (b) {
            try {
                this.setResult(callable.call());
            }
            finally {
                final Throwable t;
                this.setResult(new LottieResult<T>(t));
            }
        }
        else {
            LottieTask.Oo08.execute(new LottieFutureTask(callable));
        }
    }
    
    private void oO80() {
        this.\u3007o\u3007.post((Runnable)new Runnable(this) {
            final LottieTask o0;
            
            @Override
            public void run() {
                if (this.o0.O8 == null) {
                    return;
                }
                final LottieResult \u3007080 = this.o0.O8;
                if (\u3007080.\u3007o00\u3007\u3007Oo() != null) {
                    this.o0.\u300780\u3007808\u3007O(\u3007080.\u3007o00\u3007\u3007Oo());
                }
                else {
                    this.o0.\u3007\u3007888(\u3007080.\u3007080());
                }
            }
        });
    }
    
    private void setResult(@Nullable final LottieResult<T> o8) {
        if (this.O8 == null) {
            this.O8 = o8;
            this.oO80();
            return;
        }
        throw new IllegalStateException("A task may only be set once.");
    }
    
    private void \u300780\u3007808\u3007O(final T t) {
        synchronized (this) {
            final Iterator iterator = new ArrayList(this.\u3007080).iterator();
            while (iterator.hasNext()) {
                ((LottieListener<T>)iterator.next()).onResult(t);
            }
        }
    }
    
    private void \u3007\u3007888(final Throwable t) {
        synchronized (this) {
            final ArrayList list = new ArrayList(this.\u3007o00\u3007\u3007Oo);
            if (list.isEmpty()) {
                Logger.O8("Lottie encountered an error but no failure listener was added:", t);
                return;
            }
            final Iterator iterator = list.iterator();
            while (iterator.hasNext()) {
                ((LottieListener<Throwable>)iterator.next()).onResult(t);
            }
        }
    }
    
    public LottieTask<T> OO0o\u3007\u3007\u3007\u30070(final LottieListener<Throwable> lottieListener) {
        synchronized (this) {
            this.\u3007o00\u3007\u3007Oo.remove(lottieListener);
            return this;
        }
    }
    
    public LottieTask<T> Oo08(final LottieListener<Throwable> lottieListener) {
        synchronized (this) {
            if (this.O8 != null && this.O8.\u3007080() != null) {
                lottieListener.onResult(this.O8.\u3007080());
            }
            this.\u3007o00\u3007\u3007Oo.add(lottieListener);
            return this;
        }
    }
    
    public LottieTask<T> o\u30070(final LottieListener<T> lottieListener) {
        synchronized (this) {
            if (this.O8 != null && this.O8.\u3007o00\u3007\u3007Oo() != null) {
                lottieListener.onResult(this.O8.\u3007o00\u3007\u3007Oo());
            }
            this.\u3007080.add(lottieListener);
            return this;
        }
    }
    
    public LottieTask<T> \u30078o8o\u3007(final LottieListener<T> lottieListener) {
        synchronized (this) {
            this.\u3007080.remove(lottieListener);
            return this;
        }
    }
    
    private class LottieFutureTask extends FutureTask<LottieResult<T>>
    {
        final LottieTask o0;
        
        LottieFutureTask(final LottieTask o0, final Callable<LottieResult<T>> callable) {
            this.o0 = o0;
            super(callable);
        }
        
        @Override
        protected void done() {
            if (this.isCancelled()) {
                return;
            }
            try {
                this.o0.setResult(((FutureTask<LottieResult>)this).get());
                return;
            }
            catch (final ExecutionException ex) {}
            catch (final InterruptedException ex2) {}
            final ExecutionException ex;
            this.o0.setResult(new LottieResult(ex));
        }
    }
}

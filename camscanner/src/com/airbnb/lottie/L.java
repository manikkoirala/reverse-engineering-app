// 
// Decompiled by Procyon v0.6.0
// 

package com.airbnb.lottie;

import androidx.core.os.TraceCompat;
import androidx.annotation.RestrictTo;

@RestrictTo({ RestrictTo.Scope.LIBRARY })
public class L
{
    private static long[] O8;
    private static int Oo08 = 0;
    private static int o\u30070 = 0;
    public static boolean \u3007080 = false;
    private static boolean \u3007o00\u3007\u3007Oo = false;
    private static String[] \u3007o\u3007;
    
    public static void \u3007080(final String s) {
        if (!L.\u3007o00\u3007\u3007Oo) {
            return;
        }
        final int oo08 = L.Oo08;
        if (oo08 == 20) {
            ++L.o\u30070;
            return;
        }
        L.\u3007o\u3007[oo08] = s;
        L.O8[oo08] = System.nanoTime();
        TraceCompat.beginSection(s);
        ++L.Oo08;
    }
    
    public static float \u3007o00\u3007\u3007Oo(final String str) {
        final int o\u30070 = L.o\u30070;
        if (o\u30070 > 0) {
            L.o\u30070 = o\u30070 - 1;
            return 0.0f;
        }
        if (!L.\u3007o00\u3007\u3007Oo) {
            return 0.0f;
        }
        final int oo08 = L.Oo08 - 1;
        if ((L.Oo08 = oo08) == -1) {
            throw new IllegalStateException("Can't end trace section. There are none.");
        }
        if (str.equals(L.\u3007o\u3007[oo08])) {
            TraceCompat.endSection();
            return (System.nanoTime() - L.O8[L.Oo08]) / 1000000.0f;
        }
        final StringBuilder sb = new StringBuilder();
        sb.append("Unbalanced trace call ");
        sb.append(str);
        sb.append(". Expected ");
        sb.append(L.\u3007o\u3007[L.Oo08]);
        sb.append(".");
        throw new IllegalStateException(sb.toString());
    }
}

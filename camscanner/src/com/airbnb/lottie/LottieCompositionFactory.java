// 
// Decompiled by Procyon v0.6.0
// 

package com.airbnb.lottie;

import android.content.res.Resources$NotFoundException;
import com.airbnb.lottie.network.NetworkFetcher;
import com.airbnb.lottie.parser.LottieCompositionMoshiParser;
import java.io.Closeable;
import java.util.Iterator;
import java.util.zip.ZipEntry;
import com.airbnb.lottie.model.LottieCompositionCache;
import com.airbnb.lottie.utils.Utils;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import okio.Okio;
import java.io.IOException;
import java.util.zip.ZipInputStream;
import java.io.InputStream;
import androidx.annotation.WorkerThread;
import com.airbnb.lottie.parser.moshi.JsonReader;
import java.util.concurrent.Callable;
import java.lang.ref.WeakReference;
import androidx.annotation.Nullable;
import androidx.annotation.RawRes;
import android.content.Context;
import java.util.HashMap;
import java.util.Map;

public class LottieCompositionFactory
{
    private static final Map<String, LottieTask<LottieComposition>> \u3007080;
    
    static {
        \u3007080 = new HashMap<String, LottieTask<LottieComposition>>();
    }
    
    public static LottieTask<LottieComposition> O8(final Context context, final String str) {
        final StringBuilder sb = new StringBuilder();
        sb.append("asset_");
        sb.append(str);
        return Oo08(context, str, sb.toString());
    }
    
    public static LottieTask<LottieComposition> OO0o\u3007\u3007(final Context referent, @RawRes final int n, @Nullable final String s) {
        return \u3007o00\u3007\u3007Oo(s, new Callable<LottieResult<LottieComposition>>(new WeakReference((T)referent), referent.getApplicationContext(), n) {
            final int OO;
            final WeakReference o0;
            final Context \u3007OOo8\u30070;
            
            @Override
            public LottieResult<LottieComposition> call() {
                Context \u3007oOo8\u30070 = (Context)this.o0.get();
                if (\u3007oOo8\u30070 == null) {
                    \u3007oOo8\u30070 = this.\u3007OOo8\u30070;
                }
                return LottieCompositionFactory.Oooo8o0\u3007(\u3007oOo8\u30070, this.OO);
            }
        });
    }
    
    @WorkerThread
    public static LottieResult<LottieComposition> OO0o\u3007\u3007\u3007\u30070(final JsonReader jsonReader, @Nullable final String s) {
        return \u30078o8o\u3007(jsonReader, s, true);
    }
    
    public static LottieTask<LottieComposition> Oo08(final Context context, final String s, @Nullable final String s2) {
        return \u3007o00\u3007\u3007Oo(s2, new Callable<LottieResult<LottieComposition>>(context.getApplicationContext(), s, s2) {
            final String OO;
            final Context o0;
            final String \u3007OOo8\u30070;
            
            @Override
            public LottieResult<LottieComposition> call() {
                return LottieCompositionFactory.o\u30070(this.o0, this.\u3007OOo8\u30070, this.OO);
            }
        });
    }
    
    private static boolean OoO8(final Context context) {
        return (context.getResources().getConfiguration().uiMode & 0x30) == 0x20;
    }
    
    @WorkerThread
    public static LottieResult<LottieComposition> Oooo8o0\u3007(final Context context, @RawRes final int n) {
        return \u3007\u3007808\u3007(context, n, o800o8O(context, n));
    }
    
    private static String o800o8O(final Context context, @RawRes final int i) {
        final StringBuilder sb = new StringBuilder();
        sb.append("rawRes");
        String str;
        if (OoO8(context)) {
            str = "_night_";
        }
        else {
            str = "_day_";
        }
        sb.append(str);
        sb.append(i);
        return sb.toString();
    }
    
    @WorkerThread
    public static LottieResult<LottieComposition> oO80(final InputStream inputStream, @Nullable final String s) {
        return \u300780\u3007808\u3007O(inputStream, s, true);
    }
    
    @WorkerThread
    public static LottieResult<LottieComposition> o\u30070(final Context context, final String s, @Nullable final String s2) {
        try {
            if (s.endsWith(".zip")) {
                return \u3007\u30078O0\u30078(new ZipInputStream(context.getAssets().open(s)), s2);
            }
            return oO80(context.getAssets().open(s), s2);
        }
        catch (final IOException ex) {
            return new LottieResult<LottieComposition>(ex);
        }
    }
    
    @WorkerThread
    private static LottieResult<LottieComposition> \u30070\u3007O0088o(final ZipInputStream zipInputStream, @Nullable final String s) {
        final HashMap hashMap = new HashMap();
        try {
            ZipEntry zipEntry = zipInputStream.getNextEntry();
            LottieComposition lottieComposition = null;
            while (zipEntry != null) {
                final String name = zipEntry.getName();
                if (name.contains("__MACOSX")) {
                    zipInputStream.closeEntry();
                }
                else if (zipEntry.getName().contains(".json")) {
                    lottieComposition = \u30078o8o\u3007(JsonReader.\u3007oo\u3007(Okio.buffer(Okio.source((InputStream)zipInputStream))), null, false).\u3007o00\u3007\u3007Oo();
                }
                else if (!name.contains(".png") && !name.contains(".webp")) {
                    zipInputStream.closeEntry();
                }
                else {
                    final String[] split = name.split("/");
                    hashMap.put(split[split.length - 1], BitmapFactory.decodeStream((InputStream)zipInputStream));
                }
                zipEntry = zipInputStream.getNextEntry();
            }
            if (lottieComposition == null) {
                return new LottieResult<LottieComposition>(new IllegalArgumentException("Unable to parse composition"));
            }
            for (final Map.Entry<String, V> entry : hashMap.entrySet()) {
                final LottieImageAsset \u3007o\u3007 = \u3007o\u3007(lottieComposition, entry.getKey());
                if (\u3007o\u3007 != null) {
                    \u3007o\u3007.o\u30070(Utils.\u3007O8o08O((Bitmap)entry.getValue(), \u3007o\u3007.Oo08(), \u3007o\u3007.\u3007o\u3007()));
                }
            }
            for (final Map.Entry<K, LottieImageAsset> entry2 : lottieComposition.\u300780\u3007808\u3007O().entrySet()) {
                if (entry2.getValue().\u3007080() == null) {
                    final StringBuilder sb = new StringBuilder();
                    sb.append("There is no image for ");
                    sb.append(entry2.getValue().\u3007o00\u3007\u3007Oo());
                    return new LottieResult<LottieComposition>(new IllegalStateException(sb.toString()));
                }
            }
            if (s != null) {
                LottieCompositionCache.\u3007o00\u3007\u3007Oo().\u3007o\u3007(s, lottieComposition);
            }
            return new LottieResult<LottieComposition>(lottieComposition);
        }
        catch (final IOException ex) {
            return new LottieResult<LottieComposition>(ex);
        }
    }
    
    @WorkerThread
    private static LottieResult<LottieComposition> \u300780\u3007808\u3007O(final InputStream inputStream, @Nullable final String s, final boolean b) {
        try {
            return OO0o\u3007\u3007\u3007\u30070(JsonReader.\u3007oo\u3007(Okio.buffer(Okio.source(inputStream))), s);
        }
        finally {
            if (b) {
                Utils.\u3007o\u3007(inputStream);
            }
        }
    }
    
    private static LottieResult<LottieComposition> \u30078o8o\u3007(final JsonReader jsonReader, @Nullable final String s, final boolean b) {
        try {
            try {
                final LottieComposition \u3007080 = LottieCompositionMoshiParser.\u3007080(jsonReader);
                if (s != null) {
                    LottieCompositionCache.\u3007o00\u3007\u3007Oo().\u3007o\u3007(s, \u3007080);
                }
                final LottieResult lottieResult = new LottieResult<LottieComposition>(\u3007080);
                if (b) {
                    Utils.\u3007o\u3007(jsonReader);
                }
                return (LottieResult<LottieComposition>)lottieResult;
            }
            finally {
                if (b) {
                    Utils.\u3007o\u3007(jsonReader);
                }
                Utils.\u3007o\u3007(jsonReader);
                return;
            }
        }
        catch (final Exception ex) {}
    }
    
    public static LottieTask<LottieComposition> \u3007O00(final Context context, final String s, @Nullable final String s2) {
        return \u3007o00\u3007\u3007Oo(s2, new Callable<LottieResult<LottieComposition>>(context, s, s2) {
            final String OO;
            final Context o0;
            final String \u3007OOo8\u30070;
            
            @Override
            public LottieResult<LottieComposition> call() {
                return NetworkFetcher.Oo08(this.o0, this.\u3007OOo8\u30070, this.OO);
            }
        });
    }
    
    public static LottieTask<LottieComposition> \u3007O8o08O(final Context context, @RawRes final int n) {
        return OO0o\u3007\u3007(context, n, o800o8O(context, n));
    }
    
    public static LottieTask<LottieComposition> \u3007O\u3007(final Context context, final String str) {
        final StringBuilder sb = new StringBuilder();
        sb.append("url_");
        sb.append(str);
        return \u3007O00(context, str, sb.toString());
    }
    
    private static LottieTask<LottieComposition> \u3007o00\u3007\u3007Oo(@Nullable final String s, final Callable<LottieResult<LottieComposition>> callable) {
        LottieComposition \u3007080;
        if (s == null) {
            \u3007080 = null;
        }
        else {
            \u3007080 = LottieCompositionCache.\u3007o00\u3007\u3007Oo().\u3007080(s);
        }
        if (\u3007080 != null) {
            return new LottieTask<LottieComposition>((Callable<LottieResult<Object>>)new Callable<LottieResult<LottieComposition>>(\u3007080) {
                final LottieComposition o0;
                
                @Override
                public LottieResult<LottieComposition> call() {
                    return new LottieResult<LottieComposition>(this.o0);
                }
            });
        }
        if (s != null) {
            final Map<String, LottieTask<LottieComposition>> \u300781 = LottieCompositionFactory.\u3007080;
            if (\u300781.containsKey(s)) {
                return \u300781.get(s);
            }
        }
        final LottieTask lottieTask = new LottieTask(callable);
        if (s != null) {
            lottieTask.o\u30070(new LottieListener<LottieComposition>(s) {
                final String \u3007080;
                
                public void \u3007080(final LottieComposition lottieComposition) {
                    LottieCompositionFactory.\u3007080.remove(this.\u3007080);
                }
            });
            lottieTask.Oo08(new LottieListener<Throwable>(s) {
                final String \u3007080;
                
                public void \u3007080(final Throwable t) {
                    LottieCompositionFactory.\u3007080.remove(this.\u3007080);
                }
            });
            LottieCompositionFactory.\u3007080.put(s, lottieTask);
        }
        return lottieTask;
    }
    
    @Nullable
    private static LottieImageAsset \u3007o\u3007(final LottieComposition lottieComposition, final String anObject) {
        for (final LottieImageAsset lottieImageAsset : lottieComposition.\u300780\u3007808\u3007O().values()) {
            if (lottieImageAsset.\u3007o00\u3007\u3007Oo().equals(anObject)) {
                return lottieImageAsset;
            }
        }
        return null;
    }
    
    @WorkerThread
    public static LottieResult<LottieComposition> \u3007\u3007808\u3007(final Context context, @RawRes final int n, @Nullable final String s) {
        try {
            return oO80(context.getResources().openRawResource(n), s);
        }
        catch (final Resources$NotFoundException ex) {
            return new LottieResult<LottieComposition>((Throwable)ex);
        }
    }
    
    public static LottieTask<LottieComposition> \u3007\u3007888(final InputStream inputStream, @Nullable final String s) {
        return \u3007o00\u3007\u3007Oo(s, new Callable<LottieResult<LottieComposition>>(inputStream, s) {
            final InputStream o0;
            final String \u3007OOo8\u30070;
            
            @Override
            public LottieResult<LottieComposition> call() {
                return LottieCompositionFactory.oO80(this.o0, this.\u3007OOo8\u30070);
            }
        });
    }
    
    @WorkerThread
    public static LottieResult<LottieComposition> \u3007\u30078O0\u30078(final ZipInputStream zipInputStream, @Nullable final String s) {
        try {
            return \u30070\u3007O0088o(zipInputStream, s);
        }
        finally {
            Utils.\u3007o\u3007(zipInputStream);
        }
    }
}

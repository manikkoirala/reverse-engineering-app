// 
// Decompiled by Procyon v0.6.0
// 

package com.airbnb.lottie;

public interface LottieLogger
{
    void debug(final String p0);
    
    void error(final String p0, final Throwable p1);
    
    void \u3007080(final String p0);
    
    void \u3007o00\u3007\u3007Oo(final String p0, final Throwable p1);
}

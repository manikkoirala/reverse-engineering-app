// 
// Decompiled by Procyon v0.6.0
// 

package com.airbnb.lottie.manager;

import com.airbnb.lottie.utils.Logger;
import android.view.View;
import java.util.HashMap;
import androidx.annotation.Nullable;
import com.airbnb.lottie.FontAssetDelegate;
import android.graphics.drawable.Drawable$Callback;
import android.graphics.Typeface;
import java.util.Map;
import com.airbnb.lottie.model.MutablePair;
import android.content.res.AssetManager;

public class FontAssetManager
{
    private final AssetManager O8;
    private String Oo08;
    private final MutablePair<String> \u3007080;
    private final Map<MutablePair<String>, Typeface> \u3007o00\u3007\u3007Oo;
    private final Map<String, Typeface> \u3007o\u3007;
    
    public FontAssetManager(final Drawable$Callback drawable$Callback, @Nullable final FontAssetDelegate fontAssetDelegate) {
        this.\u3007080 = new MutablePair<String>();
        this.\u3007o00\u3007\u3007Oo = new HashMap<MutablePair<String>, Typeface>();
        this.\u3007o\u3007 = new HashMap<String, Typeface>();
        this.Oo08 = ".ttf";
        if (!(drawable$Callback instanceof View)) {
            Logger.\u3007o\u3007("LottieDrawable must be inside of a view for images to work.");
            this.O8 = null;
            return;
        }
        this.O8 = ((View)drawable$Callback).getContext().getAssets();
    }
    
    private Typeface O8(final Typeface typeface, final String s) {
        final boolean contains = s.contains("Italic");
        final boolean contains2 = s.contains("Bold");
        int n;
        if (contains && contains2) {
            n = 3;
        }
        else if (contains) {
            n = 2;
        }
        else if (contains2) {
            n = 1;
        }
        else {
            n = 0;
        }
        if (typeface.getStyle() == n) {
            return typeface;
        }
        return Typeface.create(typeface, n);
    }
    
    private Typeface \u3007080(final String str) {
        final Typeface typeface = this.\u3007o\u3007.get(str);
        if (typeface != null) {
            return typeface;
        }
        final StringBuilder sb = new StringBuilder();
        sb.append("fonts/");
        sb.append(str);
        sb.append(this.Oo08);
        final Typeface fromAsset = Typeface.createFromAsset(this.O8, sb.toString());
        this.\u3007o\u3007.put(str, fromAsset);
        return fromAsset;
    }
    
    public Typeface \u3007o00\u3007\u3007Oo(final String s, final String s2) {
        this.\u3007080.\u3007o00\u3007\u3007Oo(s, s2);
        final Typeface typeface = this.\u3007o00\u3007\u3007Oo.get(this.\u3007080);
        if (typeface != null) {
            return typeface;
        }
        final Typeface o8 = this.O8(this.\u3007080(s), s2);
        this.\u3007o00\u3007\u3007Oo.put(this.\u3007080, o8);
        return o8;
    }
    
    public void \u3007o\u3007(@Nullable final FontAssetDelegate fontAssetDelegate) {
    }
}

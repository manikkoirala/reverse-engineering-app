// 
// Decompiled by Procyon v0.6.0
// 

package com.airbnb.lottie.manager;

import android.content.res.AssetManager;
import java.io.IOException;
import com.airbnb.lottie.utils.Utils;
import android.graphics.Rect;
import android.graphics.BitmapFactory;
import android.util.Base64;
import android.graphics.BitmapFactory$Options;
import androidx.annotation.Nullable;
import android.graphics.Bitmap;
import java.util.HashMap;
import com.airbnb.lottie.utils.Logger;
import android.view.View;
import android.text.TextUtils;
import com.airbnb.lottie.ImageAssetDelegate;
import android.graphics.drawable.Drawable$Callback;
import com.airbnb.lottie.LottieImageAsset;
import java.util.Map;
import android.content.Context;

public class ImageAssetManager
{
    private static final Object O8;
    private final Context \u3007080;
    private String \u3007o00\u3007\u3007Oo;
    private final Map<String, LottieImageAsset> \u3007o\u3007;
    
    static {
        O8 = new Object();
    }
    
    public ImageAssetManager(final Drawable$Callback drawable$Callback, String \u3007o00\u3007\u3007Oo, final ImageAssetDelegate imageAssetDelegate, final Map<String, LottieImageAsset> \u3007o\u3007) {
        this.\u3007o00\u3007\u3007Oo = \u3007o00\u3007\u3007Oo;
        if (!TextUtils.isEmpty((CharSequence)\u3007o00\u3007\u3007Oo)) {
            \u3007o00\u3007\u3007Oo = this.\u3007o00\u3007\u3007Oo;
            if (\u3007o00\u3007\u3007Oo.charAt(\u3007o00\u3007\u3007Oo.length() - 1) != '/') {
                final StringBuilder sb = new StringBuilder();
                sb.append(this.\u3007o00\u3007\u3007Oo);
                sb.append('/');
                this.\u3007o00\u3007\u3007Oo = sb.toString();
            }
        }
        if (!(drawable$Callback instanceof View)) {
            Logger.\u3007o\u3007("LottieDrawable must be inside of a view for images to work.");
            this.\u3007o\u3007 = new HashMap<String, LottieImageAsset>();
            this.\u3007080 = null;
            return;
        }
        this.\u3007080 = ((View)drawable$Callback).getContext();
        this.\u3007o\u3007 = \u3007o\u3007;
        this.O8(imageAssetDelegate);
    }
    
    private Bitmap \u3007o\u3007(final String s, @Nullable final Bitmap bitmap) {
        synchronized (ImageAssetManager.O8) {
            this.\u3007o\u3007.get(s).o\u30070(bitmap);
            return bitmap;
        }
    }
    
    public void O8(@Nullable final ImageAssetDelegate imageAssetDelegate) {
    }
    
    @Nullable
    public Bitmap \u3007080(final String s) {
        final LottieImageAsset lottieImageAsset = this.\u3007o\u3007.get(s);
        if (lottieImageAsset == null) {
            return null;
        }
        final Bitmap \u3007080 = lottieImageAsset.\u3007080();
        if (\u3007080 != null) {
            return \u3007080;
        }
        final String \u3007o00\u3007\u3007Oo = lottieImageAsset.\u3007o00\u3007\u3007Oo();
        final BitmapFactory$Options bitmapFactory$Options = new BitmapFactory$Options();
        bitmapFactory$Options.inScaled = true;
        bitmapFactory$Options.inDensity = 160;
        if (\u3007o00\u3007\u3007Oo.startsWith("data:") && \u3007o00\u3007\u3007Oo.indexOf("base64,") > 0) {
            try {
                final byte[] decode = Base64.decode(\u3007o00\u3007\u3007Oo.substring(\u3007o00\u3007\u3007Oo.indexOf(44) + 1), 0);
                return this.\u3007o\u3007(s, BitmapFactory.decodeByteArray(decode, 0, decode.length, bitmapFactory$Options));
            }
            catch (final IllegalArgumentException ex) {
                Logger.O8("data URL did not have correct base64 format.", ex);
                return null;
            }
        }
        try {
            if (!TextUtils.isEmpty((CharSequence)this.\u3007o00\u3007\u3007Oo)) {
                final AssetManager assets = this.\u3007080.getAssets();
                final StringBuilder sb = new StringBuilder();
                sb.append(this.\u3007o00\u3007\u3007Oo);
                sb.append(\u3007o00\u3007\u3007Oo);
                return this.\u3007o\u3007(s, Utils.\u3007O8o08O(BitmapFactory.decodeStream(assets.open(sb.toString()), (Rect)null, bitmapFactory$Options), lottieImageAsset.Oo08(), lottieImageAsset.\u3007o\u3007()));
            }
            throw new IllegalStateException("You must set an images folder before loading an image. Set it with LottieComposition#setImagesFolder or LottieDrawable#setImagesFolder");
        }
        catch (final IOException ex2) {
            Logger.O8("Unable to open asset.", ex2);
            return null;
        }
    }
    
    public boolean \u3007o00\u3007\u3007Oo(final Context obj) {
        return (obj == null && this.\u3007080 == null) || this.\u3007080.equals(obj);
    }
}

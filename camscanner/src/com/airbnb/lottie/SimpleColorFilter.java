// 
// Decompiled by Procyon v0.6.0
// 

package com.airbnb.lottie;

import android.graphics.PorterDuff$Mode;
import androidx.annotation.ColorInt;
import android.graphics.PorterDuffColorFilter;

public class SimpleColorFilter extends PorterDuffColorFilter
{
    public SimpleColorFilter(@ColorInt final int n) {
        super(n, PorterDuff$Mode.SRC_ATOP);
    }
}

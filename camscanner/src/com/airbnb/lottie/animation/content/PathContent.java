// 
// Decompiled by Procyon v0.6.0
// 

package com.airbnb.lottie.animation.content;

import android.graphics.Path;

interface PathContent extends Content
{
    Path getPath();
}

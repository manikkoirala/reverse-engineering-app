// 
// Decompiled by Procyon v0.6.0
// 

package com.airbnb.lottie.animation.content;

import android.graphics.Path$FillType;
import com.airbnb.lottie.model.content.ShapeTrimPath;
import java.util.List;
import com.airbnb.lottie.model.content.ShapeData;
import com.airbnb.lottie.model.content.ShapePath;
import com.airbnb.lottie.model.layer.BaseLayer;
import android.graphics.Path;
import com.airbnb.lottie.LottieDrawable;
import com.airbnb.lottie.animation.keyframe.BaseKeyframeAnimation;

public class ShapeContent implements PathContent, AnimationListener
{
    private final LottieDrawable O8;
    private final BaseKeyframeAnimation<?, Path> Oo08;
    private boolean o\u30070;
    private final Path \u3007080;
    private final String \u3007o00\u3007\u3007Oo;
    private final boolean \u3007o\u3007;
    private CompoundTrimPathContent \u3007\u3007888;
    
    public ShapeContent(final LottieDrawable o8, final BaseLayer baseLayer, final ShapePath shapePath) {
        this.\u3007080 = new Path();
        this.\u3007\u3007888 = new CompoundTrimPathContent();
        this.\u3007o00\u3007\u3007Oo = shapePath.\u3007o00\u3007\u3007Oo();
        this.\u3007o\u3007 = shapePath.O8();
        this.O8 = o8;
        final BaseKeyframeAnimation<ShapeData, Path> \u3007080 = shapePath.\u3007o\u3007().\u3007080();
        baseLayer.\u300780\u3007808\u3007O(this.Oo08 = \u3007080);
        \u3007080.\u3007080((BaseKeyframeAnimation.AnimationListener)this);
    }
    
    private void \u3007o00\u3007\u3007Oo() {
        this.o\u30070 = false;
        this.O8.invalidateSelf();
    }
    
    @Override
    public void O8() {
        this.\u3007o00\u3007\u3007Oo();
    }
    
    @Override
    public void Oo08(final List<Content> list, final List<Content> list2) {
        for (int i = 0; i < list.size(); ++i) {
            final Content content = list.get(i);
            if (content instanceof TrimPathContent) {
                final TrimPathContent trimPathContent = (TrimPathContent)content;
                if (trimPathContent.getType() == ShapeTrimPath.Type.SIMULTANEOUSLY) {
                    this.\u3007\u3007888.\u3007080(trimPathContent);
                    trimPathContent.\u3007o00\u3007\u3007Oo(this);
                }
            }
        }
    }
    
    @Override
    public Path getPath() {
        if (this.o\u30070) {
            return this.\u3007080;
        }
        this.\u3007080.reset();
        if (this.\u3007o\u3007) {
            this.o\u30070 = true;
            return this.\u3007080;
        }
        this.\u3007080.set((Path)this.Oo08.oO80());
        this.\u3007080.setFillType(Path$FillType.EVEN_ODD);
        this.\u3007\u3007888.\u3007o00\u3007\u3007Oo(this.\u3007080);
        this.o\u30070 = true;
        return this.\u3007080;
    }
}

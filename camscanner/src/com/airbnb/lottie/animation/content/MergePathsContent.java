// 
// Decompiled by Procyon v0.6.0
// 

package com.airbnb.lottie.animation.content;

import java.util.ListIterator;
import android.graphics.Path$Op;
import java.util.ArrayList;
import android.graphics.Path;
import com.airbnb.lottie.model.content.MergePaths;
import java.util.List;
import android.annotation.TargetApi;

@TargetApi(19)
public class MergePathsContent implements PathContent, GreedyContent
{
    private final String O8;
    private final List<PathContent> Oo08;
    private final MergePaths o\u30070;
    private final Path \u3007080;
    private final Path \u3007o00\u3007\u3007Oo;
    private final Path \u3007o\u3007;
    
    public MergePathsContent(final MergePaths o\u30070) {
        this.\u3007080 = new Path();
        this.\u3007o00\u3007\u3007Oo = new Path();
        this.\u3007o\u3007 = new Path();
        this.Oo08 = new ArrayList<PathContent>();
        this.O8 = o\u30070.\u3007o\u3007();
        this.o\u30070 = o\u30070;
    }
    
    private void O8() {
        for (int i = 0; i < this.Oo08.size(); ++i) {
            this.\u3007o\u3007.addPath(this.Oo08.get(i).getPath());
        }
    }
    
    @TargetApi(19)
    private void o\u30070(final Path$Op path$Op) {
        this.\u3007o00\u3007\u3007Oo.reset();
        this.\u3007080.reset();
        for (int i = this.Oo08.size() - 1; i >= 1; --i) {
            final PathContent pathContent = this.Oo08.get(i);
            if (pathContent instanceof ContentGroup) {
                final ContentGroup contentGroup = (ContentGroup)pathContent;
                final List<PathContent> \u300780\u3007808\u3007O = contentGroup.\u300780\u3007808\u3007O();
                for (int j = \u300780\u3007808\u3007O.size() - 1; j >= 0; --j) {
                    final Path path = \u300780\u3007808\u3007O.get(j).getPath();
                    path.transform(contentGroup.OO0o\u3007\u3007\u3007\u30070());
                    this.\u3007o00\u3007\u3007Oo.addPath(path);
                }
            }
            else {
                this.\u3007o00\u3007\u3007Oo.addPath(pathContent.getPath());
            }
        }
        final List<PathContent> oo08 = this.Oo08;
        int k = 0;
        final PathContent pathContent2 = oo08.get(0);
        if (pathContent2 instanceof ContentGroup) {
            final ContentGroup contentGroup2 = (ContentGroup)pathContent2;
            for (List<PathContent> \u300780\u3007808\u3007O2 = contentGroup2.\u300780\u3007808\u3007O(); k < \u300780\u3007808\u3007O2.size(); ++k) {
                final Path path2 = \u300780\u3007808\u3007O2.get(k).getPath();
                path2.transform(contentGroup2.OO0o\u3007\u3007\u3007\u30070());
                this.\u3007080.addPath(path2);
            }
        }
        else {
            this.\u3007080.set(pathContent2.getPath());
        }
        this.\u3007o\u3007.op(this.\u3007080, this.\u3007o00\u3007\u3007Oo, path$Op);
    }
    
    @Override
    public void Oo08(final List<Content> list, final List<Content> list2) {
        for (int i = 0; i < this.Oo08.size(); ++i) {
            this.Oo08.get(i).Oo08(list, list2);
        }
    }
    
    @Override
    public Path getPath() {
        this.\u3007o\u3007.reset();
        if (this.o\u30070.O8()) {
            return this.\u3007o\u3007;
        }
        final int n = MergePathsContent$1.\u3007080[this.o\u30070.\u3007o00\u3007\u3007Oo().ordinal()];
        if (n != 1) {
            if (n != 2) {
                if (n != 3) {
                    if (n != 4) {
                        if (n == 5) {
                            this.o\u30070(Path$Op.XOR);
                        }
                    }
                    else {
                        this.o\u30070(Path$Op.INTERSECT);
                    }
                }
                else {
                    this.o\u30070(Path$Op.REVERSE_DIFFERENCE);
                }
            }
            else {
                this.o\u30070(Path$Op.UNION);
            }
        }
        else {
            this.O8();
        }
        return this.\u3007o\u3007;
    }
    
    @Override
    public void \u3007o00\u3007\u3007Oo(final ListIterator<Content> listIterator) {
        while (listIterator.hasPrevious() && listIterator.previous() != this) {}
        while (listIterator.hasPrevious()) {
            final Content content = listIterator.previous();
            if (content instanceof PathContent) {
                this.Oo08.add((PathContent)content);
                listIterator.remove();
            }
        }
    }
}

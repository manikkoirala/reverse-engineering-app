// 
// Decompiled by Procyon v0.6.0
// 

package com.airbnb.lottie.animation.content;

import com.airbnb.lottie.utils.MiscUtils;
import com.airbnb.lottie.model.KeyPath;
import com.airbnb.lottie.LottieProperty;
import androidx.annotation.Nullable;
import com.airbnb.lottie.value.LottieValueCallback;
import com.airbnb.lottie.model.content.ShapeTrimPath;
import java.util.List;
import com.airbnb.lottie.model.layer.BaseLayer;
import com.airbnb.lottie.LottieDrawable;
import android.graphics.Path;
import com.airbnb.lottie.model.content.CircleShape;
import android.graphics.PointF;
import com.airbnb.lottie.animation.keyframe.BaseKeyframeAnimation;

public class EllipseContent implements PathContent, AnimationListener, KeyPathElementContent
{
    private final BaseKeyframeAnimation<?, PointF> O8;
    private final BaseKeyframeAnimation<?, PointF> Oo08;
    private boolean oO80;
    private final CircleShape o\u30070;
    private final Path \u3007080;
    private final String \u3007o00\u3007\u3007Oo;
    private final LottieDrawable \u3007o\u3007;
    private CompoundTrimPathContent \u3007\u3007888;
    
    public EllipseContent(final LottieDrawable \u3007o\u3007, final BaseLayer baseLayer, final CircleShape o\u30070) {
        this.\u3007080 = new Path();
        this.\u3007\u3007888 = new CompoundTrimPathContent();
        this.\u3007o00\u3007\u3007Oo = o\u30070.\u3007o00\u3007\u3007Oo();
        this.\u3007o\u3007 = \u3007o\u3007;
        final BaseKeyframeAnimation<PointF, PointF> \u3007080 = o\u30070.O8().\u3007080();
        this.O8 = \u3007080;
        final BaseKeyframeAnimation<PointF, PointF> \u300781 = o\u30070.\u3007o\u3007().\u3007080();
        this.Oo08 = \u300781;
        this.o\u30070 = o\u30070;
        baseLayer.\u300780\u3007808\u3007O(\u3007080);
        baseLayer.\u300780\u3007808\u3007O(\u300781);
        \u3007080.\u3007080((BaseKeyframeAnimation.AnimationListener)this);
        \u300781.\u3007080((BaseKeyframeAnimation.AnimationListener)this);
    }
    
    private void \u3007o00\u3007\u3007Oo() {
        this.oO80 = false;
        this.\u3007o\u3007.invalidateSelf();
    }
    
    @Override
    public void O8() {
        this.\u3007o00\u3007\u3007Oo();
    }
    
    @Override
    public void Oo08(final List<Content> list, final List<Content> list2) {
        for (int i = 0; i < list.size(); ++i) {
            final Content content = list.get(i);
            if (content instanceof TrimPathContent) {
                final TrimPathContent trimPathContent = (TrimPathContent)content;
                if (trimPathContent.getType() == ShapeTrimPath.Type.SIMULTANEOUSLY) {
                    this.\u3007\u3007888.\u3007080(trimPathContent);
                    trimPathContent.\u3007o00\u3007\u3007Oo(this);
                }
            }
        }
    }
    
    @Override
    public String getName() {
        return this.\u3007o00\u3007\u3007Oo;
    }
    
    @Override
    public Path getPath() {
        if (this.oO80) {
            return this.\u3007080;
        }
        this.\u3007080.reset();
        if (this.o\u30070.Oo08()) {
            this.oO80 = true;
            return this.\u3007080;
        }
        final PointF pointF = this.O8.oO80();
        final float n = pointF.x / 2.0f;
        final float n2 = pointF.y / 2.0f;
        final float n3 = n * 0.55228f;
        final float n4 = 0.55228f * n2;
        this.\u3007080.reset();
        if (this.o\u30070.o\u30070()) {
            final Path \u3007080 = this.\u3007080;
            final float n5 = -n2;
            \u3007080.moveTo(0.0f, n5);
            final Path \u300781 = this.\u3007080;
            final float n6 = 0.0f - n3;
            final float n7 = -n;
            final float n8 = 0.0f - n4;
            \u300781.cubicTo(n6, n5, n7, n8, n7, 0.0f);
            final Path \u300782 = this.\u3007080;
            final float n9 = n4 + 0.0f;
            \u300782.cubicTo(n7, n9, n6, n2, 0.0f, n2);
            final Path \u300783 = this.\u3007080;
            final float n10 = n3 + 0.0f;
            \u300783.cubicTo(n10, n2, n, n9, n, 0.0f);
            this.\u3007080.cubicTo(n, n8, n10, n5, 0.0f, n5);
        }
        else {
            final Path \u300784 = this.\u3007080;
            final float n11 = -n2;
            \u300784.moveTo(0.0f, n11);
            final Path \u300785 = this.\u3007080;
            final float n12 = n3 + 0.0f;
            final float n13 = 0.0f - n4;
            \u300785.cubicTo(n12, n11, n, n13, n, 0.0f);
            final Path \u300786 = this.\u3007080;
            final float n14 = n4 + 0.0f;
            \u300786.cubicTo(n, n14, n12, n2, 0.0f, n2);
            final Path \u300787 = this.\u3007080;
            final float n15 = 0.0f - n3;
            final float n16 = -n;
            \u300787.cubicTo(n15, n2, n16, n14, n16, 0.0f);
            this.\u3007080.cubicTo(n16, n13, n15, n11, 0.0f, n11);
        }
        final PointF pointF2 = this.Oo08.oO80();
        this.\u3007080.offset(pointF2.x, pointF2.y);
        this.\u3007080.close();
        this.\u3007\u3007888.\u3007o00\u3007\u3007Oo(this.\u3007080);
        this.oO80 = true;
        return this.\u3007080;
    }
    
    @Override
    public <T> void o\u30070(final T t, @Nullable final LottieValueCallback<T> lottieValueCallback) {
        if (t == LottieProperty.\u3007\u3007888) {
            this.O8.OO0o\u3007\u3007((LottieValueCallback<PointF>)lottieValueCallback);
        }
        else if (t == LottieProperty.OO0o\u3007\u3007\u3007\u30070) {
            this.Oo08.OO0o\u3007\u3007((LottieValueCallback<PointF>)lottieValueCallback);
        }
    }
    
    @Override
    public void \u3007\u3007888(final KeyPath keyPath, final int n, final List<KeyPath> list, final KeyPath keyPath2) {
        MiscUtils.\u3007O8o08O(keyPath, n, list, keyPath2, this);
    }
}

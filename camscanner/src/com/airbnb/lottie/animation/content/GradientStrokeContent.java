// 
// Decompiled by Procyon v0.6.0
// 

package com.airbnb.lottie.animation.content;

import android.graphics.Shader;
import android.graphics.Matrix;
import android.graphics.Canvas;
import com.airbnb.lottie.LottieProperty;
import com.airbnb.lottie.value.LottieValueCallback;
import android.graphics.Shader$TileMode;
import com.airbnb.lottie.model.content.GradientStroke;
import com.airbnb.lottie.model.layer.BaseLayer;
import com.airbnb.lottie.LottieDrawable;
import android.graphics.RadialGradient;
import com.airbnb.lottie.model.content.GradientColor;
import android.graphics.LinearGradient;
import androidx.collection.LongSparseArray;
import android.graphics.RectF;
import androidx.annotation.Nullable;
import com.airbnb.lottie.animation.keyframe.ValueCallbackKeyframeAnimation;
import android.graphics.PointF;
import com.airbnb.lottie.animation.keyframe.BaseKeyframeAnimation;
import com.airbnb.lottie.model.content.GradientType;

public class GradientStrokeContent extends BaseStrokeContent
{
    private final GradientType OoO8;
    private final int o800o8O;
    private final BaseKeyframeAnimation<PointF, PointF> oo88o8O;
    @Nullable
    private ValueCallbackKeyframeAnimation o\u3007O8\u3007\u3007o;
    private final RectF \u30070\u3007O0088o;
    private final LongSparseArray<LinearGradient> \u3007O00;
    private final BaseKeyframeAnimation<GradientColor, GradientColor> \u3007O888o0o;
    private final boolean \u3007O\u3007;
    private final BaseKeyframeAnimation<PointF, PointF> \u3007oo\u3007;
    private final String \u3007\u3007808\u3007;
    private final LongSparseArray<RadialGradient> \u3007\u30078O0\u30078;
    
    public GradientStrokeContent(final LottieDrawable lottieDrawable, final BaseLayer baseLayer, final GradientStroke gradientStroke) {
        super(lottieDrawable, baseLayer, gradientStroke.\u3007o00\u3007\u3007Oo().toPaintCap(), gradientStroke.\u3007\u3007888().toPaintJoin(), gradientStroke.\u300780\u3007808\u3007O(), gradientStroke.\u30078o8o\u3007(), gradientStroke.OO0o\u3007\u3007(), gradientStroke.oO80(), gradientStroke.\u3007o\u3007());
        this.\u3007O00 = new LongSparseArray<LinearGradient>();
        this.\u3007\u30078O0\u30078 = new LongSparseArray<RadialGradient>();
        this.\u30070\u3007O0088o = new RectF();
        this.\u3007\u3007808\u3007 = gradientStroke.OO0o\u3007\u3007\u3007\u30070();
        this.OoO8 = gradientStroke.o\u30070();
        this.\u3007O\u3007 = gradientStroke.Oooo8o0\u3007();
        this.o800o8O = (int)(lottieDrawable.\u3007\u3007808\u3007().O8() / 32.0f);
        final BaseKeyframeAnimation<GradientColor, GradientColor> \u3007080 = gradientStroke.Oo08().\u3007080();
        (this.\u3007O888o0o = \u3007080).\u3007080((BaseKeyframeAnimation.AnimationListener)this);
        baseLayer.\u300780\u3007808\u3007O(\u3007080);
        final BaseKeyframeAnimation<PointF, PointF> \u300781 = gradientStroke.\u3007O8o08O().\u3007080();
        (this.oo88o8O = \u300781).\u3007080((BaseKeyframeAnimation.AnimationListener)this);
        baseLayer.\u300780\u3007808\u3007O(\u300781);
        final BaseKeyframeAnimation<PointF, PointF> \u300782 = gradientStroke.O8().\u3007080();
        (this.\u3007oo\u3007 = \u300782).\u3007080((BaseKeyframeAnimation.AnimationListener)this);
        baseLayer.\u300780\u3007808\u3007O(\u300782);
    }
    
    private int OO0o\u3007\u3007\u3007\u30070() {
        final int round = Math.round(this.oo88o8O.o\u30070() * this.o800o8O);
        final int round2 = Math.round(this.\u3007oo\u3007.o\u30070() * this.o800o8O);
        final int round3 = Math.round(this.\u3007O888o0o.o\u30070() * this.o800o8O);
        int n;
        if (round != 0) {
            n = 527 * round;
        }
        else {
            n = 17;
        }
        int n2 = n;
        if (round2 != 0) {
            n2 = n * 31 * round2;
        }
        int n3 = n2;
        if (round3 != 0) {
            n3 = n2 * 31 * round3;
        }
        return n3;
    }
    
    private int[] \u300780\u3007808\u3007O(int[] array) {
        final ValueCallbackKeyframeAnimation o\u3007O8\u3007\u3007o = this.o\u3007O8\u3007\u3007o;
        int[] array2 = array;
        if (o\u3007O8\u3007\u3007o != null) {
            final Integer[] array3 = o\u3007O8\u3007\u3007o.oO80();
            final int length = array.length;
            final int length2 = array3.length;
            final int n = 0;
            int n2 = 0;
            if (length == length2) {
                while (true) {
                    array2 = array;
                    if (n2 >= array.length) {
                        break;
                    }
                    array[n2] = array3[n2];
                    ++n2;
                }
            }
            else {
                array = new int[array3.length];
                int n3 = n;
                while (true) {
                    array2 = array;
                    if (n3 >= array3.length) {
                        break;
                    }
                    array[n3] = array3[n3];
                    ++n3;
                }
            }
        }
        return array2;
    }
    
    private LinearGradient \u30078o8o\u3007() {
        final int oo0o\u3007\u3007\u3007\u30070 = this.OO0o\u3007\u3007\u3007\u30070();
        final LongSparseArray<LinearGradient> \u3007o00 = this.\u3007O00;
        final long n = oo0o\u3007\u3007\u3007\u30070;
        final LinearGradient linearGradient = \u3007o00.get(n);
        if (linearGradient != null) {
            return linearGradient;
        }
        final PointF pointF = this.oo88o8O.oO80();
        final PointF pointF2 = this.\u3007oo\u3007.oO80();
        final GradientColor gradientColor = this.\u3007O888o0o.oO80();
        final LinearGradient linearGradient2 = new LinearGradient(pointF.x, pointF.y, pointF2.x, pointF2.y, this.\u300780\u3007808\u3007O(gradientColor.\u3007080()), gradientColor.\u3007o00\u3007\u3007Oo(), Shader$TileMode.CLAMP);
        this.\u3007O00.put(n, linearGradient2);
        return linearGradient2;
    }
    
    private RadialGradient \u3007O8o08O() {
        final int oo0o\u3007\u3007\u3007\u30070 = this.OO0o\u3007\u3007\u3007\u30070();
        final LongSparseArray<RadialGradient> \u3007\u30078O0\u30078 = this.\u3007\u30078O0\u30078;
        final long n = oo0o\u3007\u3007\u3007\u30070;
        final RadialGradient radialGradient = \u3007\u30078O0\u30078.get(n);
        if (radialGradient != null) {
            return radialGradient;
        }
        final PointF pointF = this.oo88o8O.oO80();
        final PointF pointF2 = this.\u3007oo\u3007.oO80();
        final GradientColor gradientColor = this.\u3007O888o0o.oO80();
        final int[] \u300780\u3007808\u3007O = this.\u300780\u3007808\u3007O(gradientColor.\u3007080());
        final float[] \u3007o00\u3007\u3007Oo = gradientColor.\u3007o00\u3007\u3007Oo();
        final float x = pointF.x;
        final float y = pointF.y;
        final RadialGradient radialGradient2 = new RadialGradient(x, y, (float)Math.hypot(pointF2.x - x, pointF2.y - y), \u300780\u3007808\u3007O, \u3007o00\u3007\u3007Oo, Shader$TileMode.CLAMP);
        this.\u3007\u30078O0\u30078.put(n, radialGradient2);
        return radialGradient2;
    }
    
    @Override
    public String getName() {
        return this.\u3007\u3007808\u3007;
    }
    
    @Override
    public <T> void o\u30070(final T t, @Nullable final LottieValueCallback<T> lottieValueCallback) {
        super.o\u30070(t, lottieValueCallback);
        if (t == LottieProperty.\u30070000OOO) {
            final ValueCallbackKeyframeAnimation o\u3007O8\u3007\u3007o = this.o\u3007O8\u3007\u3007o;
            if (o\u3007O8\u3007\u3007o != null) {
                super.o\u30070.\u3007oOO8O8(o\u3007O8\u3007\u3007o);
            }
            if (lottieValueCallback == null) {
                this.o\u3007O8\u3007\u3007o = null;
            }
            else {
                (this.o\u3007O8\u3007\u3007o = new ValueCallbackKeyframeAnimation((LottieValueCallback<A>)lottieValueCallback)).\u3007080((BaseKeyframeAnimation.AnimationListener)this);
                super.o\u30070.\u300780\u3007808\u3007O(this.o\u3007O8\u3007\u3007o);
            }
        }
    }
    
    @Override
    public void \u3007o\u3007(final Canvas canvas, final Matrix localMatrix, final int n) {
        if (this.\u3007O\u3007) {
            return;
        }
        this.\u3007080(this.\u30070\u3007O0088o, localMatrix, false);
        Object shader;
        if (this.OoO8 == GradientType.LINEAR) {
            shader = this.\u30078o8o\u3007();
        }
        else {
            shader = this.\u3007O8o08O();
        }
        ((Shader)shader).setLocalMatrix(localMatrix);
        super.\u300780\u3007808\u3007O.setShader((Shader)shader);
        super.\u3007o\u3007(canvas, localMatrix, n);
    }
}

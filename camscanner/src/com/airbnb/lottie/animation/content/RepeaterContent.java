// 
// Decompiled by Procyon v0.6.0
// 

package com.airbnb.lottie.animation.content;

import com.airbnb.lottie.model.KeyPath;
import com.airbnb.lottie.utils.MiscUtils;
import android.graphics.Canvas;
import com.airbnb.lottie.model.animatable.AnimatableTransform;
import java.util.Collections;
import java.util.ArrayList;
import java.util.ListIterator;
import android.graphics.RectF;
import com.airbnb.lottie.LottieProperty;
import androidx.annotation.Nullable;
import com.airbnb.lottie.value.LottieValueCallback;
import java.util.List;
import com.airbnb.lottie.model.content.Repeater;
import com.airbnb.lottie.LottieDrawable;
import android.graphics.Path;
import com.airbnb.lottie.animation.keyframe.TransformKeyframeAnimation;
import android.graphics.Matrix;
import com.airbnb.lottie.model.layer.BaseLayer;
import com.airbnb.lottie.animation.keyframe.BaseKeyframeAnimation;

public class RepeaterContent implements DrawingContent, PathContent, GreedyContent, AnimationListener, KeyPathElementContent
{
    private final BaseLayer O8;
    private ContentGroup OO0o\u3007\u3007\u3007\u30070;
    private final String Oo08;
    private final BaseKeyframeAnimation<Float, Float> oO80;
    private final boolean o\u30070;
    private final Matrix \u3007080;
    private final TransformKeyframeAnimation \u300780\u3007808\u3007O;
    private final Path \u3007o00\u3007\u3007Oo;
    private final LottieDrawable \u3007o\u3007;
    private final BaseKeyframeAnimation<Float, Float> \u3007\u3007888;
    
    public RepeaterContent(final LottieDrawable \u3007o\u3007, final BaseLayer o8, final Repeater repeater) {
        this.\u3007080 = new Matrix();
        this.\u3007o00\u3007\u3007Oo = new Path();
        this.\u3007o\u3007 = \u3007o\u3007;
        this.O8 = o8;
        this.Oo08 = repeater.\u3007o\u3007();
        this.o\u30070 = repeater.o\u30070();
        final BaseKeyframeAnimation<Float, Float> \u3007080 = repeater.\u3007o00\u3007\u3007Oo().\u3007080();
        o8.\u300780\u3007808\u3007O(this.\u3007\u3007888 = \u3007080);
        \u3007080.\u3007080((BaseKeyframeAnimation.AnimationListener)this);
        final BaseKeyframeAnimation<Float, Float> \u300781 = repeater.O8().\u3007080();
        o8.\u300780\u3007808\u3007O(this.oO80 = \u300781);
        \u300781.\u3007080((BaseKeyframeAnimation.AnimationListener)this);
        final TransformKeyframeAnimation \u3007o00\u3007\u3007Oo = repeater.Oo08().\u3007o00\u3007\u3007Oo();
        (this.\u300780\u3007808\u3007O = \u3007o00\u3007\u3007Oo).\u3007080(o8);
        \u3007o00\u3007\u3007Oo.\u3007o00\u3007\u3007Oo(this);
    }
    
    @Override
    public void O8() {
        this.\u3007o\u3007.invalidateSelf();
    }
    
    @Override
    public void Oo08(final List<Content> list, final List<Content> list2) {
        this.OO0o\u3007\u3007\u3007\u30070.Oo08(list, list2);
    }
    
    @Override
    public String getName() {
        return this.Oo08;
    }
    
    @Override
    public Path getPath() {
        final Path path = this.OO0o\u3007\u3007\u3007\u30070.getPath();
        this.\u3007o00\u3007\u3007Oo.reset();
        final float floatValue = this.\u3007\u3007888.oO80();
        final float floatValue2 = this.oO80.oO80();
        for (int i = (int)floatValue - 1; i >= 0; --i) {
            this.\u3007080.set(this.\u300780\u3007808\u3007O.\u3007\u3007888(i + floatValue2));
            this.\u3007o00\u3007\u3007Oo.addPath(path, this.\u3007080);
        }
        return this.\u3007o00\u3007\u3007Oo;
    }
    
    @Override
    public <T> void o\u30070(final T t, @Nullable final LottieValueCallback<T> lottieValueCallback) {
        if (this.\u300780\u3007808\u3007O.\u3007o\u3007(t, lottieValueCallback)) {
            return;
        }
        if (t == LottieProperty.\u3007O00) {
            this.\u3007\u3007888.OO0o\u3007\u3007((LottieValueCallback<Float>)lottieValueCallback);
        }
        else if (t == LottieProperty.\u3007\u30078O0\u30078) {
            this.oO80.OO0o\u3007\u3007((LottieValueCallback<Float>)lottieValueCallback);
        }
    }
    
    @Override
    public void \u3007080(final RectF rectF, final Matrix matrix, final boolean b) {
        this.OO0o\u3007\u3007\u3007\u30070.\u3007080(rectF, matrix, b);
    }
    
    @Override
    public void \u3007o00\u3007\u3007Oo(final ListIterator<Content> listIterator) {
        if (this.OO0o\u3007\u3007\u3007\u30070 != null) {
            return;
        }
        while (listIterator.hasPrevious() && listIterator.previous() != this) {}
        final ArrayList list = new ArrayList();
        while (listIterator.hasPrevious()) {
            list.add(listIterator.previous());
            listIterator.remove();
        }
        Collections.reverse(list);
        this.OO0o\u3007\u3007\u3007\u30070 = new ContentGroup(this.\u3007o\u3007, this.O8, "Repeater", this.o\u30070, list, null);
    }
    
    @Override
    public void \u3007o\u3007(final Canvas canvas, final Matrix matrix, final int n) {
        final float floatValue = this.\u3007\u3007888.oO80();
        final float floatValue2 = this.oO80.oO80();
        final float n2 = this.\u300780\u3007808\u3007O.\u300780\u3007808\u3007O().oO80() / 100.0f;
        final float n3 = this.\u300780\u3007808\u3007O.Oo08().oO80() / 100.0f;
        for (int i = (int)floatValue - 1; i >= 0; --i) {
            this.\u3007080.set(matrix);
            final Matrix \u3007080 = this.\u3007080;
            final TransformKeyframeAnimation \u300780\u3007808\u3007O = this.\u300780\u3007808\u3007O;
            final float n4 = (float)i;
            \u3007080.preConcat(\u300780\u3007808\u3007O.\u3007\u3007888(n4 + floatValue2));
            this.OO0o\u3007\u3007\u3007\u30070.\u3007o\u3007(canvas, this.\u3007080, (int)(n * MiscUtils.OO0o\u3007\u3007\u3007\u30070(n2, n3, n4 / floatValue)));
        }
    }
    
    @Override
    public void \u3007\u3007888(final KeyPath keyPath, final int n, final List<KeyPath> list, final KeyPath keyPath2) {
        MiscUtils.\u3007O8o08O(keyPath, n, list, keyPath2, this);
    }
}

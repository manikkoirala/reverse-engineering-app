// 
// Decompiled by Procyon v0.6.0
// 

package com.airbnb.lottie.animation.content;

import com.airbnb.lottie.model.KeyPath;
import com.airbnb.lottie.utils.MiscUtils;
import com.airbnb.lottie.animation.keyframe.IntegerKeyframeAnimation;
import com.airbnb.lottie.animation.keyframe.FloatKeyframeAnimation;
import androidx.annotation.CallSuper;
import com.airbnb.lottie.animation.keyframe.ValueCallbackKeyframeAnimation;
import com.airbnb.lottie.LottieProperty;
import com.airbnb.lottie.value.LottieValueCallback;
import com.airbnb.lottie.model.content.ShapeTrimPath;
import android.graphics.PathEffect;
import android.graphics.DashPathEffect;
import com.airbnb.lottie.utils.Utils;
import com.airbnb.lottie.L;
import android.graphics.Matrix;
import android.graphics.Canvas;
import android.graphics.Paint$Style;
import com.airbnb.lottie.animation.LPaint;
import java.util.ArrayList;
import com.airbnb.lottie.model.animatable.AnimatableFloatValue;
import com.airbnb.lottie.model.animatable.AnimatableIntegerValue;
import android.graphics.Paint$Join;
import android.graphics.Paint$Cap;
import android.graphics.Path;
import java.util.List;
import android.graphics.Paint;
import android.graphics.PathMeasure;
import com.airbnb.lottie.model.layer.BaseLayer;
import android.graphics.ColorFilter;
import com.airbnb.lottie.LottieDrawable;
import androidx.annotation.Nullable;
import android.graphics.RectF;
import com.airbnb.lottie.animation.keyframe.BaseKeyframeAnimation;

public abstract class BaseStrokeContent implements AnimationListener, KeyPathElementContent, DrawingContent
{
    private final RectF O8;
    @Nullable
    private final BaseKeyframeAnimation<?, Float> OO0o\u3007\u3007;
    private final BaseKeyframeAnimation<?, Float> OO0o\u3007\u3007\u3007\u30070;
    private final LottieDrawable Oo08;
    @Nullable
    private BaseKeyframeAnimation<ColorFilter, ColorFilter> Oooo8o0\u3007;
    private final float[] oO80;
    protected final BaseLayer o\u30070;
    private final PathMeasure \u3007080;
    final Paint \u300780\u3007808\u3007O;
    private final BaseKeyframeAnimation<?, Integer> \u30078o8o\u3007;
    private final List<BaseKeyframeAnimation<?, Float>> \u3007O8o08O;
    private final Path \u3007o00\u3007\u3007Oo;
    private final Path \u3007o\u3007;
    private final List<PathGroup> \u3007\u3007888;
    
    BaseStrokeContent(final LottieDrawable oo08, final BaseLayer o\u30070, final Paint$Cap strokeCap, final Paint$Join strokeJoin, final float strokeMiter, final AnimatableIntegerValue animatableIntegerValue, final AnimatableFloatValue animatableFloatValue, final List<AnimatableFloatValue> list, final AnimatableFloatValue animatableFloatValue2) {
        this.\u3007080 = new PathMeasure();
        this.\u3007o00\u3007\u3007Oo = new Path();
        this.\u3007o\u3007 = new Path();
        this.O8 = new RectF();
        this.\u3007\u3007888 = new ArrayList<PathGroup>();
        final LPaint \u300780\u3007808\u3007O = new LPaint(1);
        this.\u300780\u3007808\u3007O = \u300780\u3007808\u3007O;
        this.Oo08 = oo08;
        this.o\u30070 = o\u30070;
        \u300780\u3007808\u3007O.setStyle(Paint$Style.STROKE);
        \u300780\u3007808\u3007O.setStrokeCap(strokeCap);
        \u300780\u3007808\u3007O.setStrokeJoin(strokeJoin);
        \u300780\u3007808\u3007O.setStrokeMiter(strokeMiter);
        this.\u30078o8o\u3007 = animatableIntegerValue.\u3007080();
        this.OO0o\u3007\u3007\u3007\u30070 = animatableFloatValue.\u3007080();
        if (animatableFloatValue2 == null) {
            this.OO0o\u3007\u3007 = null;
        }
        else {
            this.OO0o\u3007\u3007 = animatableFloatValue2.\u3007080();
        }
        this.\u3007O8o08O = new ArrayList<BaseKeyframeAnimation<?, Float>>(list.size());
        this.oO80 = new float[list.size()];
        final int n = 0;
        for (int i = 0; i < list.size(); ++i) {
            this.\u3007O8o08O.add(((AnimatableFloatValue)list.get(i)).\u3007080());
        }
        o\u30070.\u300780\u3007808\u3007O(this.\u30078o8o\u3007);
        o\u30070.\u300780\u3007808\u3007O(this.OO0o\u3007\u3007\u3007\u30070);
        for (int j = 0; j < this.\u3007O8o08O.size(); ++j) {
            o\u30070.\u300780\u3007808\u3007O(this.\u3007O8o08O.get(j));
        }
        final BaseKeyframeAnimation<?, Float> oo0o\u3007\u3007 = this.OO0o\u3007\u3007;
        if (oo0o\u3007\u3007 != null) {
            o\u30070.\u300780\u3007808\u3007O(oo0o\u3007\u3007);
        }
        this.\u30078o8o\u3007.\u3007080((BaseKeyframeAnimation.AnimationListener)this);
        this.OO0o\u3007\u3007\u3007\u30070.\u3007080((BaseKeyframeAnimation.AnimationListener)this);
        for (int k = n; k < list.size(); ++k) {
            this.\u3007O8o08O.get(k).\u3007080((BaseKeyframeAnimation.AnimationListener)this);
        }
        final BaseKeyframeAnimation<?, Float> oo0o\u3007\u30072 = this.OO0o\u3007\u3007;
        if (oo0o\u3007\u30072 != null) {
            oo0o\u3007\u30072.\u3007080((BaseKeyframeAnimation.AnimationListener)this);
        }
    }
    
    private void oO80(final Canvas canvas, final PathGroup pathGroup, final Matrix matrix) {
        L.\u3007080("StrokeContent#applyTrimPath");
        if (pathGroup.\u3007o00\u3007\u3007Oo == null) {
            L.\u3007o00\u3007\u3007Oo("StrokeContent#applyTrimPath");
            return;
        }
        this.\u3007o00\u3007\u3007Oo.reset();
        for (int i = pathGroup.\u3007080.size() - 1; i >= 0; --i) {
            this.\u3007o00\u3007\u3007Oo.addPath(((PathContent)pathGroup.\u3007080.get(i)).getPath(), matrix);
        }
        this.\u3007080.setPath(this.\u3007o00\u3007\u3007Oo, false);
        float length = this.\u3007080.getLength();
        while (this.\u3007080.nextContour()) {
            length += this.\u3007080.getLength();
        }
        final float n = pathGroup.\u3007o00\u3007\u3007Oo.\u3007\u3007888().oO80() * length / 360.0f;
        final float n2 = pathGroup.\u3007o00\u3007\u3007Oo.oO80().oO80() * length / 100.0f + n;
        final float n3 = pathGroup.\u3007o00\u3007\u3007Oo.o\u30070().oO80() * length / 100.0f + n;
        int j = pathGroup.\u3007080.size() - 1;
        float n4 = 0.0f;
        while (j >= 0) {
            this.\u3007o\u3007.set(((PathContent)pathGroup.\u3007080.get(j)).getPath());
            this.\u3007o\u3007.transform(matrix);
            this.\u3007080.setPath(this.\u3007o\u3007, false);
            final float length2 = this.\u3007080.getLength();
            float n5 = 1.0f;
            Label_0502: {
                if (n3 > length) {
                    final float n6 = n3 - length;
                    if (n6 < n4 + length2 && n4 < n6) {
                        float n7;
                        if (n2 > length) {
                            n7 = (n2 - length) / length2;
                        }
                        else {
                            n7 = 0.0f;
                        }
                        Utils.\u3007080(this.\u3007o\u3007, n7, Math.min(n6 / length2, 1.0f), 0.0f);
                        canvas.drawPath(this.\u3007o\u3007, this.\u300780\u3007808\u3007O);
                        break Label_0502;
                    }
                }
                final float n8 = n4 + length2;
                if (n8 >= n2) {
                    if (n4 <= n3) {
                        if (n8 <= n3 && n2 < n4) {
                            canvas.drawPath(this.\u3007o\u3007, this.\u300780\u3007808\u3007O);
                        }
                        else {
                            float n9;
                            if (n2 < n4) {
                                n9 = 0.0f;
                            }
                            else {
                                n9 = (n2 - n4) / length2;
                            }
                            if (n3 <= n8) {
                                n5 = (n3 - n4) / length2;
                            }
                            Utils.\u3007080(this.\u3007o\u3007, n9, n5, 0.0f);
                            canvas.drawPath(this.\u3007o\u3007, this.\u300780\u3007808\u3007O);
                        }
                    }
                }
            }
            n4 += length2;
            --j;
        }
        L.\u3007o00\u3007\u3007Oo("StrokeContent#applyTrimPath");
    }
    
    private void \u3007o00\u3007\u3007Oo(final Matrix matrix) {
        L.\u3007080("StrokeContent#applyDashPattern");
        if (this.\u3007O8o08O.isEmpty()) {
            L.\u3007o00\u3007\u3007Oo("StrokeContent#applyDashPattern");
            return;
        }
        final float \u3007\u3007888 = Utils.\u3007\u3007888(matrix);
        for (int i = 0; i < this.\u3007O8o08O.size(); ++i) {
            this.oO80[i] = (float)this.\u3007O8o08O.get(i).oO80();
            if (i % 2 == 0) {
                final float[] oo80 = this.oO80;
                if (oo80[i] < 1.0f) {
                    oo80[i] = 1.0f;
                }
            }
            else {
                final float[] oo81 = this.oO80;
                if (oo81[i] < 0.1f) {
                    oo81[i] = 0.1f;
                }
            }
            final float[] oo82 = this.oO80;
            oo82[i] *= \u3007\u3007888;
        }
        final BaseKeyframeAnimation<?, Float> oo0o\u3007\u3007 = this.OO0o\u3007\u3007;
        float n;
        if (oo0o\u3007\u3007 == null) {
            n = 0.0f;
        }
        else {
            n = \u3007\u3007888 * oo0o\u3007\u3007.oO80();
        }
        this.\u300780\u3007808\u3007O.setPathEffect((PathEffect)new DashPathEffect(this.oO80, n));
        L.\u3007o00\u3007\u3007Oo("StrokeContent#applyDashPattern");
    }
    
    @Override
    public void O8() {
        this.Oo08.invalidateSelf();
    }
    
    @Override
    public void Oo08(final List<Content> list, final List<Content> list2) {
        int i = list.size() - 1;
        TrimPathContent trimPathContent = null;
        while (i >= 0) {
            final Content content = list.get(i);
            TrimPathContent trimPathContent2 = trimPathContent;
            if (content instanceof TrimPathContent) {
                final TrimPathContent trimPathContent3 = (TrimPathContent)content;
                trimPathContent2 = trimPathContent;
                if (trimPathContent3.getType() == ShapeTrimPath.Type.INDIVIDUALLY) {
                    trimPathContent2 = trimPathContent3;
                }
            }
            --i;
            trimPathContent = trimPathContent2;
        }
        if (trimPathContent != null) {
            trimPathContent.\u3007o00\u3007\u3007Oo(this);
        }
        int j = list2.size() - 1;
        PathGroup pathGroup = null;
        while (j >= 0) {
            final Content content2 = list2.get(j);
            PathGroup pathGroup2 = null;
            Label_0222: {
                if (content2 instanceof TrimPathContent) {
                    final TrimPathContent trimPathContent4 = (TrimPathContent)content2;
                    if (trimPathContent4.getType() == ShapeTrimPath.Type.INDIVIDUALLY) {
                        if (pathGroup != null) {
                            this.\u3007\u3007888.add(pathGroup);
                        }
                        pathGroup2 = new PathGroup(trimPathContent4);
                        trimPathContent4.\u3007o00\u3007\u3007Oo(this);
                        break Label_0222;
                    }
                }
                pathGroup2 = pathGroup;
                if (content2 instanceof PathContent) {
                    if ((pathGroup2 = pathGroup) == null) {
                        pathGroup2 = new PathGroup(trimPathContent);
                    }
                    pathGroup2.\u3007080.add(content2);
                }
            }
            --j;
            pathGroup = pathGroup2;
        }
        if (pathGroup != null) {
            this.\u3007\u3007888.add(pathGroup);
        }
    }
    
    @CallSuper
    @Override
    public <T> void o\u30070(final T t, @Nullable final LottieValueCallback<T> lottieValueCallback) {
        if (t == LottieProperty.O8) {
            this.\u30078o8o\u3007.OO0o\u3007\u3007((LottieValueCallback<Integer>)lottieValueCallback);
        }
        else if (t == LottieProperty.\u3007\u3007808\u3007) {
            this.OO0o\u3007\u3007\u3007\u30070.OO0o\u3007\u3007((LottieValueCallback<Float>)lottieValueCallback);
        }
        else if (t == LottieProperty.\u3007oOO8O8) {
            final BaseKeyframeAnimation<ColorFilter, ColorFilter> oooo8o0\u3007 = this.Oooo8o0\u3007;
            if (oooo8o0\u3007 != null) {
                this.o\u30070.\u3007oOO8O8(oooo8o0\u3007);
            }
            if (lottieValueCallback == null) {
                this.Oooo8o0\u3007 = null;
            }
            else {
                (this.Oooo8o0\u3007 = new ValueCallbackKeyframeAnimation<ColorFilter, ColorFilter>((LottieValueCallback<ColorFilter>)lottieValueCallback)).\u3007080((BaseKeyframeAnimation.AnimationListener)this);
                this.o\u30070.\u300780\u3007808\u3007O(this.Oooo8o0\u3007);
            }
        }
    }
    
    @Override
    public void \u3007080(final RectF rectF, final Matrix matrix, final boolean b) {
        L.\u3007080("StrokeContent#getBounds");
        this.\u3007o00\u3007\u3007Oo.reset();
        for (int i = 0; i < this.\u3007\u3007888.size(); ++i) {
            final PathGroup pathGroup = this.\u3007\u3007888.get(i);
            for (int j = 0; j < pathGroup.\u3007080.size(); ++j) {
                this.\u3007o00\u3007\u3007Oo.addPath(((PathContent)pathGroup.\u3007080.get(j)).getPath(), matrix);
            }
        }
        this.\u3007o00\u3007\u3007Oo.computeBounds(this.O8, false);
        final float \u3007\u3007808\u3007 = ((FloatKeyframeAnimation)this.OO0o\u3007\u3007\u3007\u30070).\u3007\u3007808\u3007();
        final RectF o8 = this.O8;
        final float left = o8.left;
        final float n = \u3007\u3007808\u3007 / 2.0f;
        o8.set(left - n, o8.top - n, o8.right + n, o8.bottom + n);
        rectF.set(this.O8);
        rectF.set(rectF.left - 1.0f, rectF.top - 1.0f, rectF.right + 1.0f, rectF.bottom + 1.0f);
        L.\u3007o00\u3007\u3007Oo("StrokeContent#getBounds");
    }
    
    @Override
    public void \u3007o\u3007(final Canvas canvas, final Matrix matrix, int i) {
        L.\u3007080("StrokeContent#draw");
        if (Utils.oO80(matrix)) {
            L.\u3007o00\u3007\u3007Oo("StrokeContent#draw");
            return;
        }
        i = (int)(i / 255.0f * ((IntegerKeyframeAnimation)this.\u30078o8o\u3007).\u3007\u3007808\u3007() / 100.0f * 255.0f);
        final Paint \u300780\u3007808\u3007O = this.\u300780\u3007808\u3007O;
        final int n = 0;
        \u300780\u3007808\u3007O.setAlpha(MiscUtils.\u3007o\u3007(i, 0, 255));
        this.\u300780\u3007808\u3007O.setStrokeWidth(((FloatKeyframeAnimation)this.OO0o\u3007\u3007\u3007\u30070).\u3007\u3007808\u3007() * Utils.\u3007\u3007888(matrix));
        if (this.\u300780\u3007808\u3007O.getStrokeWidth() <= 0.0f) {
            L.\u3007o00\u3007\u3007Oo("StrokeContent#draw");
            return;
        }
        this.\u3007o00\u3007\u3007Oo(matrix);
        final BaseKeyframeAnimation<ColorFilter, ColorFilter> oooo8o0\u3007 = this.Oooo8o0\u3007;
        i = n;
        if (oooo8o0\u3007 != null) {
            this.\u300780\u3007808\u3007O.setColorFilter((ColorFilter)oooo8o0\u3007.oO80());
            i = n;
        }
        while (i < this.\u3007\u3007888.size()) {
            final PathGroup pathGroup = this.\u3007\u3007888.get(i);
            if (pathGroup.\u3007o00\u3007\u3007Oo != null) {
                this.oO80(canvas, pathGroup, matrix);
            }
            else {
                L.\u3007080("StrokeContent#buildPath");
                this.\u3007o00\u3007\u3007Oo.reset();
                for (int j = pathGroup.\u3007080.size() - 1; j >= 0; --j) {
                    this.\u3007o00\u3007\u3007Oo.addPath(((PathContent)pathGroup.\u3007080.get(j)).getPath(), matrix);
                }
                L.\u3007o00\u3007\u3007Oo("StrokeContent#buildPath");
                L.\u3007080("StrokeContent#drawPath");
                canvas.drawPath(this.\u3007o00\u3007\u3007Oo, this.\u300780\u3007808\u3007O);
                L.\u3007o00\u3007\u3007Oo("StrokeContent#drawPath");
            }
            ++i;
        }
        L.\u3007o00\u3007\u3007Oo("StrokeContent#draw");
    }
    
    @Override
    public void \u3007\u3007888(final KeyPath keyPath, final int n, final List<KeyPath> list, final KeyPath keyPath2) {
        MiscUtils.\u3007O8o08O(keyPath, n, list, keyPath2, this);
    }
    
    private static final class PathGroup
    {
        private final List<PathContent> \u3007080;
        @Nullable
        private final TrimPathContent \u3007o00\u3007\u3007Oo;
        
        private PathGroup(@Nullable final TrimPathContent \u3007o00\u3007\u3007Oo) {
            this.\u3007080 = new ArrayList<PathContent>();
            this.\u3007o00\u3007\u3007Oo = \u3007o00\u3007\u3007Oo;
        }
    }
}

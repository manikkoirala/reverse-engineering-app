// 
// Decompiled by Procyon v0.6.0
// 

package com.airbnb.lottie.animation.content;

import com.airbnb.lottie.utils.MiscUtils;
import com.airbnb.lottie.model.KeyPath;
import com.airbnb.lottie.LottieProperty;
import androidx.annotation.Nullable;
import com.airbnb.lottie.value.LottieValueCallback;
import com.airbnb.lottie.animation.keyframe.FloatKeyframeAnimation;
import com.airbnb.lottie.model.content.ShapeTrimPath;
import java.util.List;
import com.airbnb.lottie.model.content.RectangleShape;
import com.airbnb.lottie.model.layer.BaseLayer;
import android.graphics.RectF;
import android.graphics.Path;
import android.graphics.PointF;
import com.airbnb.lottie.LottieDrawable;
import com.airbnb.lottie.animation.keyframe.BaseKeyframeAnimation;

public class RectangleContent implements AnimationListener, KeyPathElementContent, PathContent
{
    private final boolean O8;
    private boolean OO0o\u3007\u3007\u3007\u30070;
    private final LottieDrawable Oo08;
    private final BaseKeyframeAnimation<?, Float> oO80;
    private final BaseKeyframeAnimation<?, PointF> o\u30070;
    private final Path \u3007080;
    private CompoundTrimPathContent \u300780\u3007808\u3007O;
    private final RectF \u3007o00\u3007\u3007Oo;
    private final String \u3007o\u3007;
    private final BaseKeyframeAnimation<?, PointF> \u3007\u3007888;
    
    public RectangleContent(final LottieDrawable oo08, final BaseLayer baseLayer, final RectangleShape rectangleShape) {
        this.\u3007080 = new Path();
        this.\u3007o00\u3007\u3007Oo = new RectF();
        this.\u300780\u3007808\u3007O = new CompoundTrimPathContent();
        this.\u3007o\u3007 = rectangleShape.\u3007o\u3007();
        this.O8 = rectangleShape.o\u30070();
        this.Oo08 = oo08;
        final BaseKeyframeAnimation<PointF, PointF> \u3007080 = rectangleShape.O8().\u3007080();
        this.o\u30070 = \u3007080;
        final BaseKeyframeAnimation<PointF, PointF> \u300781 = rectangleShape.Oo08().\u3007080();
        this.\u3007\u3007888 = \u300781;
        final BaseKeyframeAnimation<Float, Float> \u300782 = rectangleShape.\u3007o00\u3007\u3007Oo().\u3007080();
        this.oO80 = \u300782;
        baseLayer.\u300780\u3007808\u3007O(\u3007080);
        baseLayer.\u300780\u3007808\u3007O(\u300781);
        baseLayer.\u300780\u3007808\u3007O(\u300782);
        \u3007080.\u3007080((BaseKeyframeAnimation.AnimationListener)this);
        \u300781.\u3007080((BaseKeyframeAnimation.AnimationListener)this);
        \u300782.\u3007080((BaseKeyframeAnimation.AnimationListener)this);
    }
    
    private void \u3007o00\u3007\u3007Oo() {
        this.OO0o\u3007\u3007\u3007\u30070 = false;
        this.Oo08.invalidateSelf();
    }
    
    @Override
    public void O8() {
        this.\u3007o00\u3007\u3007Oo();
    }
    
    @Override
    public void Oo08(final List<Content> list, final List<Content> list2) {
        for (int i = 0; i < list.size(); ++i) {
            final Content content = list.get(i);
            if (content instanceof TrimPathContent) {
                final TrimPathContent trimPathContent = (TrimPathContent)content;
                if (trimPathContent.getType() == ShapeTrimPath.Type.SIMULTANEOUSLY) {
                    this.\u300780\u3007808\u3007O.\u3007080(trimPathContent);
                    trimPathContent.\u3007o00\u3007\u3007Oo(this);
                }
            }
        }
    }
    
    @Override
    public String getName() {
        return this.\u3007o\u3007;
    }
    
    @Override
    public Path getPath() {
        if (this.OO0o\u3007\u3007\u3007\u30070) {
            return this.\u3007080;
        }
        this.\u3007080.reset();
        if (this.O8) {
            this.OO0o\u3007\u3007\u3007\u30070 = true;
            return this.\u3007080;
        }
        final PointF pointF = this.\u3007\u3007888.oO80();
        final float a = pointF.x / 2.0f;
        final float b = pointF.y / 2.0f;
        final BaseKeyframeAnimation<?, Float> oo80 = this.oO80;
        float \u3007\u3007808\u3007;
        if (oo80 == null) {
            \u3007\u3007808\u3007 = 0.0f;
        }
        else {
            \u3007\u3007808\u3007 = ((FloatKeyframeAnimation)oo80).\u3007\u3007808\u3007();
        }
        final float min = Math.min(a, b);
        float n = \u3007\u3007808\u3007;
        if (\u3007\u3007808\u3007 > min) {
            n = min;
        }
        final PointF pointF2 = this.o\u30070.oO80();
        this.\u3007080.moveTo(pointF2.x + a, pointF2.y - b + n);
        this.\u3007080.lineTo(pointF2.x + a, pointF2.y + b - n);
        final float n2 = fcmpl(n, 0.0f);
        if (n2 > 0) {
            final RectF \u3007o00\u3007\u3007Oo = this.\u3007o00\u3007\u3007Oo;
            final float x = pointF2.x;
            final float n3 = n * 2.0f;
            final float y = pointF2.y;
            \u3007o00\u3007\u3007Oo.set(x + a - n3, y + b - n3, x + a, y + b);
            this.\u3007080.arcTo(this.\u3007o00\u3007\u3007Oo, 0.0f, 90.0f, false);
        }
        this.\u3007080.lineTo(pointF2.x - a + n, pointF2.y + b);
        if (n2 > 0) {
            final RectF \u3007o00\u3007\u3007Oo2 = this.\u3007o00\u3007\u3007Oo;
            final float x2 = pointF2.x;
            final float y2 = pointF2.y;
            final float n4 = n * 2.0f;
            \u3007o00\u3007\u3007Oo2.set(x2 - a, y2 + b - n4, x2 - a + n4, y2 + b);
            this.\u3007080.arcTo(this.\u3007o00\u3007\u3007Oo, 90.0f, 90.0f, false);
        }
        this.\u3007080.lineTo(pointF2.x - a, pointF2.y - b + n);
        if (n2 > 0) {
            final RectF \u3007o00\u3007\u3007Oo3 = this.\u3007o00\u3007\u3007Oo;
            final float x3 = pointF2.x;
            final float y3 = pointF2.y;
            final float n5 = n * 2.0f;
            \u3007o00\u3007\u3007Oo3.set(x3 - a, y3 - b, x3 - a + n5, y3 - b + n5);
            this.\u3007080.arcTo(this.\u3007o00\u3007\u3007Oo, 180.0f, 90.0f, false);
        }
        this.\u3007080.lineTo(pointF2.x + a - n, pointF2.y - b);
        if (n2 > 0) {
            final RectF \u3007o00\u3007\u3007Oo4 = this.\u3007o00\u3007\u3007Oo;
            final float x4 = pointF2.x;
            final float n6 = n * 2.0f;
            final float y4 = pointF2.y;
            \u3007o00\u3007\u3007Oo4.set(x4 + a - n6, y4 - b, x4 + a, y4 - b + n6);
            this.\u3007080.arcTo(this.\u3007o00\u3007\u3007Oo, 270.0f, 90.0f, false);
        }
        this.\u3007080.close();
        this.\u300780\u3007808\u3007O.\u3007o00\u3007\u3007Oo(this.\u3007080);
        this.OO0o\u3007\u3007\u3007\u30070 = true;
        return this.\u3007080;
    }
    
    @Override
    public <T> void o\u30070(final T t, @Nullable final LottieValueCallback<T> lottieValueCallback) {
        if (t == LottieProperty.oO80) {
            this.\u3007\u3007888.OO0o\u3007\u3007((LottieValueCallback<PointF>)lottieValueCallback);
        }
        else if (t == LottieProperty.OO0o\u3007\u3007\u3007\u30070) {
            this.o\u30070.OO0o\u3007\u3007((LottieValueCallback<PointF>)lottieValueCallback);
        }
        else if (t == LottieProperty.\u300780\u3007808\u3007O) {
            this.oO80.OO0o\u3007\u3007((LottieValueCallback<Float>)lottieValueCallback);
        }
    }
    
    @Override
    public void \u3007\u3007888(final KeyPath keyPath, final int n, final List<KeyPath> list, final KeyPath keyPath2) {
        MiscUtils.\u3007O8o08O(keyPath, n, list, keyPath2, this);
    }
}

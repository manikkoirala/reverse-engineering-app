// 
// Decompiled by Procyon v0.6.0
// 

package com.airbnb.lottie.animation.content;

import com.airbnb.lottie.utils.Utils;
import android.graphics.Path;
import java.util.ArrayList;
import java.util.List;

public class CompoundTrimPathContent
{
    private List<TrimPathContent> \u3007080;
    
    public CompoundTrimPathContent() {
        this.\u3007080 = new ArrayList<TrimPathContent>();
    }
    
    void \u3007080(final TrimPathContent trimPathContent) {
        this.\u3007080.add(trimPathContent);
    }
    
    public void \u3007o00\u3007\u3007Oo(final Path path) {
        for (int i = this.\u3007080.size() - 1; i >= 0; --i) {
            Utils.\u3007o00\u3007\u3007Oo(path, this.\u3007080.get(i));
        }
    }
}

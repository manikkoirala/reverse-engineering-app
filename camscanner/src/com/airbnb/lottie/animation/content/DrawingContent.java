// 
// Decompiled by Procyon v0.6.0
// 

package com.airbnb.lottie.animation.content;

import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.RectF;

public interface DrawingContent extends Content
{
    void \u3007080(final RectF p0, final Matrix p1, final boolean p2);
    
    void \u3007o\u3007(final Canvas p0, final Matrix p1, final int p2);
}

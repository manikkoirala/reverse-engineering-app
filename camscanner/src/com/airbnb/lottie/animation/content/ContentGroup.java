// 
// Decompiled by Procyon v0.6.0
// 

package com.airbnb.lottie.animation.content;

import com.airbnb.lottie.model.KeyPath;
import com.airbnb.lottie.utils.Utils;
import android.graphics.Canvas;
import com.airbnb.lottie.value.LottieValueCallback;
import java.util.Collection;
import com.airbnb.lottie.model.content.ContentModel;
import java.util.ArrayList;
import com.airbnb.lottie.animation.LPaint;
import com.airbnb.lottie.model.animatable.AnimatableTransform;
import com.airbnb.lottie.model.content.ShapeGroup;
import com.airbnb.lottie.model.layer.BaseLayer;
import android.graphics.Matrix;
import com.airbnb.lottie.animation.keyframe.TransformKeyframeAnimation;
import com.airbnb.lottie.LottieDrawable;
import android.graphics.Paint;
import android.graphics.RectF;
import androidx.annotation.Nullable;
import java.util.List;
import android.graphics.Path;
import com.airbnb.lottie.model.KeyPathElement;
import com.airbnb.lottie.animation.keyframe.BaseKeyframeAnimation;

public class ContentGroup implements DrawingContent, PathContent, AnimationListener, KeyPathElement
{
    private final Path O8;
    @Nullable
    private List<PathContent> OO0o\u3007\u3007\u3007\u30070;
    private final RectF Oo08;
    private final List<Content> oO80;
    private final String o\u30070;
    private Paint \u3007080;
    private final LottieDrawable \u300780\u3007808\u3007O;
    @Nullable
    private TransformKeyframeAnimation \u30078o8o\u3007;
    private RectF \u3007o00\u3007\u3007Oo;
    private final Matrix \u3007o\u3007;
    private final boolean \u3007\u3007888;
    
    public ContentGroup(final LottieDrawable lottieDrawable, final BaseLayer baseLayer, final ShapeGroup shapeGroup) {
        this(lottieDrawable, baseLayer, shapeGroup.\u3007o\u3007(), shapeGroup.O8(), \u3007o00\u3007\u3007Oo(lottieDrawable, baseLayer, shapeGroup.\u3007o00\u3007\u3007Oo()), oO80(shapeGroup.\u3007o00\u3007\u3007Oo()));
    }
    
    ContentGroup(final LottieDrawable \u300780\u3007808\u3007O, final BaseLayer baseLayer, final String o\u30070, final boolean \u3007\u3007888, final List<Content> oo80, @Nullable final AnimatableTransform animatableTransform) {
        this.\u3007080 = new LPaint();
        this.\u3007o00\u3007\u3007Oo = new RectF();
        this.\u3007o\u3007 = new Matrix();
        this.O8 = new Path();
        this.Oo08 = new RectF();
        this.o\u30070 = o\u30070;
        this.\u300780\u3007808\u3007O = \u300780\u3007808\u3007O;
        this.\u3007\u3007888 = \u3007\u3007888;
        this.oO80 = oo80;
        if (animatableTransform != null) {
            (this.\u30078o8o\u3007 = animatableTransform.\u3007o00\u3007\u3007Oo()).\u3007080(baseLayer);
            this.\u30078o8o\u3007.\u3007o00\u3007\u3007Oo(this);
        }
        final ArrayList list = new ArrayList();
        for (int i = oo80.size() - 1; i >= 0; --i) {
            final Content content = oo80.get(i);
            if (content instanceof GreedyContent) {
                list.add(content);
            }
        }
        for (int j = list.size() - 1; j >= 0; --j) {
            ((GreedyContent)list.get(j)).\u3007o00\u3007\u3007Oo(oo80.listIterator(oo80.size()));
        }
    }
    
    @Nullable
    static AnimatableTransform oO80(final List<ContentModel> list) {
        for (int i = 0; i < list.size(); ++i) {
            final ContentModel contentModel = list.get(i);
            if (contentModel instanceof AnimatableTransform) {
                return (AnimatableTransform)contentModel;
            }
        }
        return null;
    }
    
    private boolean \u30078o8o\u3007() {
        int i = 0;
        int n = 0;
        while (i < this.oO80.size()) {
            int n2 = n;
            if (this.oO80.get(i) instanceof DrawingContent) {
                n2 = ++n;
                if (n >= 2) {
                    return true;
                }
            }
            ++i;
            n = n2;
        }
        return false;
    }
    
    private static List<Content> \u3007o00\u3007\u3007Oo(final LottieDrawable lottieDrawable, final BaseLayer baseLayer, final List<ContentModel> list) {
        final ArrayList list2 = new ArrayList(list.size());
        for (int i = 0; i < list.size(); ++i) {
            final Content \u3007080 = list.get(i).\u3007080(lottieDrawable, baseLayer);
            if (\u3007080 != null) {
                list2.add(\u3007080);
            }
        }
        return list2;
    }
    
    @Override
    public void O8() {
        this.\u300780\u3007808\u3007O.invalidateSelf();
    }
    
    Matrix OO0o\u3007\u3007\u3007\u30070() {
        final TransformKeyframeAnimation \u30078o8o\u3007 = this.\u30078o8o\u3007;
        if (\u30078o8o\u3007 != null) {
            return \u30078o8o\u3007.o\u30070();
        }
        this.\u3007o\u3007.reset();
        return this.\u3007o\u3007;
    }
    
    @Override
    public void Oo08(final List<Content> list, final List<Content> list2) {
        final ArrayList list3 = new ArrayList(list.size() + this.oO80.size());
        list3.addAll(list);
        for (int i = this.oO80.size() - 1; i >= 0; --i) {
            final Content content = this.oO80.get(i);
            content.Oo08(list3, this.oO80.subList(0, i));
            list3.add(content);
        }
    }
    
    @Override
    public String getName() {
        return this.o\u30070;
    }
    
    @Override
    public Path getPath() {
        this.\u3007o\u3007.reset();
        final TransformKeyframeAnimation \u30078o8o\u3007 = this.\u30078o8o\u3007;
        if (\u30078o8o\u3007 != null) {
            this.\u3007o\u3007.set(\u30078o8o\u3007.o\u30070());
        }
        this.O8.reset();
        if (this.\u3007\u3007888) {
            return this.O8;
        }
        for (int i = this.oO80.size() - 1; i >= 0; --i) {
            final Content content = this.oO80.get(i);
            if (content instanceof PathContent) {
                this.O8.addPath(((PathContent)content).getPath(), this.\u3007o\u3007);
            }
        }
        return this.O8;
    }
    
    @Override
    public <T> void o\u30070(final T t, @Nullable final LottieValueCallback<T> lottieValueCallback) {
        final TransformKeyframeAnimation \u30078o8o\u3007 = this.\u30078o8o\u3007;
        if (\u30078o8o\u3007 != null) {
            \u30078o8o\u3007.\u3007o\u3007(t, lottieValueCallback);
        }
    }
    
    @Override
    public void \u3007080(final RectF rectF, final Matrix matrix, final boolean b) {
        this.\u3007o\u3007.set(matrix);
        final TransformKeyframeAnimation \u30078o8o\u3007 = this.\u30078o8o\u3007;
        if (\u30078o8o\u3007 != null) {
            this.\u3007o\u3007.preConcat(\u30078o8o\u3007.o\u30070());
        }
        this.Oo08.set(0.0f, 0.0f, 0.0f, 0.0f);
        for (int i = this.oO80.size() - 1; i >= 0; --i) {
            final Content content = this.oO80.get(i);
            if (content instanceof DrawingContent) {
                ((DrawingContent)content).\u3007080(this.Oo08, this.\u3007o\u3007, b);
                rectF.union(this.Oo08);
            }
        }
    }
    
    List<PathContent> \u300780\u3007808\u3007O() {
        if (this.OO0o\u3007\u3007\u3007\u30070 == null) {
            this.OO0o\u3007\u3007\u3007\u30070 = new ArrayList<PathContent>();
            for (int i = 0; i < this.oO80.size(); ++i) {
                final Content content = this.oO80.get(i);
                if (content instanceof PathContent) {
                    this.OO0o\u3007\u3007\u3007\u30070.add((PathContent)content);
                }
            }
        }
        return this.OO0o\u3007\u3007\u3007\u30070;
    }
    
    @Override
    public void \u3007o\u3007(final Canvas canvas, final Matrix matrix, int n) {
        if (this.\u3007\u3007888) {
            return;
        }
        this.\u3007o\u3007.set(matrix);
        final TransformKeyframeAnimation \u30078o8o\u3007 = this.\u30078o8o\u3007;
        int alpha = n;
        if (\u30078o8o\u3007 != null) {
            this.\u3007o\u3007.preConcat(\u30078o8o\u3007.o\u30070());
            int intValue;
            if (this.\u30078o8o\u3007.oO80() == null) {
                intValue = 100;
            }
            else {
                intValue = this.\u30078o8o\u3007.oO80().oO80();
            }
            alpha = (int)(intValue / 100.0f * n / 255.0f * 255.0f);
        }
        if (this.\u300780\u3007808\u3007O.oo\u3007() && this.\u30078o8o\u3007() && alpha != 255) {
            n = 1;
        }
        else {
            n = 0;
        }
        if (n != 0) {
            this.\u3007o00\u3007\u3007Oo.set(0.0f, 0.0f, 0.0f, 0.0f);
            this.\u3007080(this.\u3007o00\u3007\u3007Oo, this.\u3007o\u3007, true);
            this.\u3007080.setAlpha(alpha);
            Utils.OO0o\u3007\u3007(canvas, this.\u3007o00\u3007\u3007Oo, this.\u3007080);
        }
        if (n != 0) {
            alpha = 255;
        }
        for (int i = this.oO80.size() - 1; i >= 0; --i) {
            final Content value = this.oO80.get(i);
            if (value instanceof DrawingContent) {
                ((DrawingContent)value).\u3007o\u3007(canvas, this.\u3007o\u3007, alpha);
            }
        }
        if (n != 0) {
            canvas.restore();
        }
    }
    
    @Override
    public void \u3007\u3007888(final KeyPath keyPath, final int n, final List<KeyPath> list, KeyPath keyPath2) {
        if (!keyPath.\u3007\u3007888(this.getName(), n)) {
            return;
        }
        KeyPath \u3007080 = keyPath2;
        if (!"__container".equals(this.getName())) {
            keyPath2 = (\u3007080 = keyPath2.\u3007080(this.getName()));
            if (keyPath.\u3007o\u3007(this.getName(), n)) {
                list.add(keyPath2.\u300780\u3007808\u3007O(this));
                \u3007080 = keyPath2;
            }
        }
        if (keyPath.oO80(this.getName(), n)) {
            final int oo08 = keyPath.Oo08(this.getName(), n);
            for (int i = 0; i < this.oO80.size(); ++i) {
                final Content content = this.oO80.get(i);
                if (content instanceof KeyPathElement) {
                    ((KeyPathElement)content).\u3007\u3007888(keyPath, n + oo08, list, \u3007080);
                }
            }
        }
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.airbnb.lottie.animation.content;

import com.airbnb.lottie.model.KeyPath;
import com.airbnb.lottie.utils.MiscUtils;
import android.graphics.Shader;
import com.airbnb.lottie.L;
import android.graphics.Canvas;
import android.graphics.Matrix;
import com.airbnb.lottie.LottieProperty;
import com.airbnb.lottie.value.LottieValueCallback;
import android.graphics.Shader$TileMode;
import java.util.ArrayList;
import com.airbnb.lottie.animation.LPaint;
import com.airbnb.lottie.model.content.GradientFill;
import android.graphics.Paint;
import android.graphics.ColorFilter;
import com.airbnb.lottie.model.layer.BaseLayer;
import androidx.annotation.Nullable;
import com.airbnb.lottie.animation.keyframe.ValueCallbackKeyframeAnimation;
import com.airbnb.lottie.LottieDrawable;
import com.airbnb.lottie.model.content.GradientColor;
import java.util.List;
import androidx.annotation.NonNull;
import android.graphics.Path;
import android.graphics.RectF;
import android.graphics.RadialGradient;
import com.airbnb.lottie.model.content.GradientType;
import android.graphics.PointF;
import android.graphics.LinearGradient;
import androidx.collection.LongSparseArray;
import com.airbnb.lottie.animation.keyframe.BaseKeyframeAnimation;

public class GradientFillContent implements DrawingContent, AnimationListener, KeyPathElementContent
{
    private final LongSparseArray<LinearGradient> O8;
    private final BaseKeyframeAnimation<PointF, PointF> OO0o\u3007\u3007;
    private final GradientType OO0o\u3007\u3007\u3007\u30070;
    private final LongSparseArray<RadialGradient> Oo08;
    private final BaseKeyframeAnimation<PointF, PointF> Oooo8o0\u3007;
    private final RectF oO80;
    private final Path o\u30070;
    @NonNull
    private final String \u3007080;
    private final List<PathContent> \u300780\u3007808\u3007O;
    private final BaseKeyframeAnimation<GradientColor, GradientColor> \u30078o8o\u3007;
    private final LottieDrawable \u3007O00;
    private final BaseKeyframeAnimation<Integer, Integer> \u3007O8o08O;
    @Nullable
    private ValueCallbackKeyframeAnimation \u3007O\u3007;
    private final boolean \u3007o00\u3007\u3007Oo;
    private final BaseLayer \u3007o\u3007;
    @Nullable
    private BaseKeyframeAnimation<ColorFilter, ColorFilter> \u3007\u3007808\u3007;
    private final Paint \u3007\u3007888;
    private final int \u3007\u30078O0\u30078;
    
    public GradientFillContent(final LottieDrawable \u3007o00, final BaseLayer \u3007o\u3007, final GradientFill gradientFill) {
        this.O8 = new LongSparseArray<LinearGradient>();
        this.Oo08 = new LongSparseArray<RadialGradient>();
        final Path o\u30070 = new Path();
        this.o\u30070 = o\u30070;
        this.\u3007\u3007888 = new LPaint(1);
        this.oO80 = new RectF();
        this.\u300780\u3007808\u3007O = new ArrayList<PathContent>();
        this.\u3007o\u3007 = \u3007o\u3007;
        this.\u3007080 = gradientFill.o\u30070();
        this.\u3007o00\u3007\u3007Oo = gradientFill.\u300780\u3007808\u3007O();
        this.\u3007O00 = \u3007o00;
        this.OO0o\u3007\u3007\u3007\u30070 = gradientFill.Oo08();
        o\u30070.setFillType(gradientFill.\u3007o\u3007());
        this.\u3007\u30078O0\u30078 = (int)(\u3007o00.\u3007\u3007808\u3007().O8() / 32.0f);
        final BaseKeyframeAnimation<GradientColor, GradientColor> \u3007080 = gradientFill.O8().\u3007080();
        (this.\u30078o8o\u3007 = \u3007080).\u3007080((BaseKeyframeAnimation.AnimationListener)this);
        \u3007o\u3007.\u300780\u3007808\u3007O(\u3007080);
        final BaseKeyframeAnimation<Integer, Integer> \u300781 = gradientFill.\u3007\u3007888().\u3007080();
        (this.\u3007O8o08O = \u300781).\u3007080((BaseKeyframeAnimation.AnimationListener)this);
        \u3007o\u3007.\u300780\u3007808\u3007O(\u300781);
        final BaseKeyframeAnimation<PointF, PointF> \u300782 = gradientFill.oO80().\u3007080();
        (this.OO0o\u3007\u3007 = \u300782).\u3007080((BaseKeyframeAnimation.AnimationListener)this);
        \u3007o\u3007.\u300780\u3007808\u3007O(\u300782);
        final BaseKeyframeAnimation<PointF, PointF> \u300783 = gradientFill.\u3007o00\u3007\u3007Oo().\u3007080();
        (this.Oooo8o0\u3007 = \u300783).\u3007080((BaseKeyframeAnimation.AnimationListener)this);
        \u3007o\u3007.\u300780\u3007808\u3007O(\u300783);
    }
    
    private RadialGradient OO0o\u3007\u3007\u3007\u30070() {
        final int oo80 = this.oO80();
        final LongSparseArray<RadialGradient> oo81 = this.Oo08;
        final long n = oo80;
        final RadialGradient radialGradient = oo81.get(n);
        if (radialGradient != null) {
            return radialGradient;
        }
        final PointF pointF = this.OO0o\u3007\u3007.oO80();
        final PointF pointF2 = this.Oooo8o0\u3007.oO80();
        final GradientColor gradientColor = this.\u30078o8o\u3007.oO80();
        final int[] \u3007o00\u3007\u3007Oo = this.\u3007o00\u3007\u3007Oo(gradientColor.\u3007080());
        final float[] \u3007o00\u3007\u3007Oo2 = gradientColor.\u3007o00\u3007\u3007Oo();
        final float x = pointF.x;
        final float y = pointF.y;
        float n2 = (float)Math.hypot(pointF2.x - x, pointF2.y - y);
        if (n2 <= 0.0f) {
            n2 = 0.001f;
        }
        final RadialGradient radialGradient2 = new RadialGradient(x, y, n2, \u3007o00\u3007\u3007Oo, \u3007o00\u3007\u3007Oo2, Shader$TileMode.CLAMP);
        this.Oo08.put(n, radialGradient2);
        return radialGradient2;
    }
    
    private int oO80() {
        final int round = Math.round(this.OO0o\u3007\u3007.o\u30070() * this.\u3007\u30078O0\u30078);
        final int round2 = Math.round(this.Oooo8o0\u3007.o\u30070() * this.\u3007\u30078O0\u30078);
        final int round3 = Math.round(this.\u30078o8o\u3007.o\u30070() * this.\u3007\u30078O0\u30078);
        int n;
        if (round != 0) {
            n = 527 * round;
        }
        else {
            n = 17;
        }
        int n2 = n;
        if (round2 != 0) {
            n2 = n * 31 * round2;
        }
        int n3 = n2;
        if (round3 != 0) {
            n3 = n2 * 31 * round3;
        }
        return n3;
    }
    
    private LinearGradient \u300780\u3007808\u3007O() {
        final int oo80 = this.oO80();
        final LongSparseArray<LinearGradient> o8 = this.O8;
        final long n = oo80;
        final LinearGradient linearGradient = o8.get(n);
        if (linearGradient != null) {
            return linearGradient;
        }
        final PointF pointF = this.OO0o\u3007\u3007.oO80();
        final PointF pointF2 = this.Oooo8o0\u3007.oO80();
        final GradientColor gradientColor = this.\u30078o8o\u3007.oO80();
        final LinearGradient linearGradient2 = new LinearGradient(pointF.x, pointF.y, pointF2.x, pointF2.y, this.\u3007o00\u3007\u3007Oo(gradientColor.\u3007080()), gradientColor.\u3007o00\u3007\u3007Oo(), Shader$TileMode.CLAMP);
        this.O8.put(n, linearGradient2);
        return linearGradient2;
    }
    
    private int[] \u3007o00\u3007\u3007Oo(int[] array) {
        final ValueCallbackKeyframeAnimation \u3007o\u3007 = this.\u3007O\u3007;
        int[] array2 = array;
        if (\u3007o\u3007 != null) {
            final Integer[] array3 = \u3007o\u3007.oO80();
            final int length = array.length;
            final int length2 = array3.length;
            final int n = 0;
            int n2 = 0;
            if (length == length2) {
                while (true) {
                    array2 = array;
                    if (n2 >= array.length) {
                        break;
                    }
                    array[n2] = array3[n2];
                    ++n2;
                }
            }
            else {
                array = new int[array3.length];
                int n3 = n;
                while (true) {
                    array2 = array;
                    if (n3 >= array3.length) {
                        break;
                    }
                    array[n3] = array3[n3];
                    ++n3;
                }
            }
        }
        return array2;
    }
    
    @Override
    public void O8() {
        this.\u3007O00.invalidateSelf();
    }
    
    @Override
    public void Oo08(final List<Content> list, final List<Content> list2) {
        for (int i = 0; i < list2.size(); ++i) {
            final Content content = list2.get(i);
            if (content instanceof PathContent) {
                this.\u300780\u3007808\u3007O.add((PathContent)content);
            }
        }
    }
    
    @Override
    public String getName() {
        return this.\u3007080;
    }
    
    @Override
    public <T> void o\u30070(final T t, @Nullable final LottieValueCallback<T> lottieValueCallback) {
        if (t == LottieProperty.O8) {
            this.\u3007O8o08O.OO0o\u3007\u3007((LottieValueCallback<Integer>)lottieValueCallback);
        }
        else if (t == LottieProperty.\u3007oOO8O8) {
            final BaseKeyframeAnimation<ColorFilter, ColorFilter> \u3007\u3007808\u3007 = this.\u3007\u3007808\u3007;
            if (\u3007\u3007808\u3007 != null) {
                this.\u3007o\u3007.\u3007oOO8O8(\u3007\u3007808\u3007);
            }
            if (lottieValueCallback == null) {
                this.\u3007\u3007808\u3007 = null;
            }
            else {
                (this.\u3007\u3007808\u3007 = new ValueCallbackKeyframeAnimation<ColorFilter, ColorFilter>((LottieValueCallback<ColorFilter>)lottieValueCallback)).\u3007080((BaseKeyframeAnimation.AnimationListener)this);
                this.\u3007o\u3007.\u300780\u3007808\u3007O(this.\u3007\u3007808\u3007);
            }
        }
        else if (t == LottieProperty.\u30070000OOO) {
            final ValueCallbackKeyframeAnimation \u3007o\u3007 = this.\u3007O\u3007;
            if (\u3007o\u3007 != null) {
                this.\u3007o\u3007.\u3007oOO8O8(\u3007o\u3007);
            }
            if (lottieValueCallback == null) {
                this.\u3007O\u3007 = null;
            }
            else {
                (this.\u3007O\u3007 = new ValueCallbackKeyframeAnimation((LottieValueCallback<A>)lottieValueCallback)).\u3007080((BaseKeyframeAnimation.AnimationListener)this);
                this.\u3007o\u3007.\u300780\u3007808\u3007O(this.\u3007O\u3007);
            }
        }
    }
    
    @Override
    public void \u3007080(final RectF rectF, final Matrix matrix, final boolean b) {
        this.o\u30070.reset();
        for (int i = 0; i < this.\u300780\u3007808\u3007O.size(); ++i) {
            this.o\u30070.addPath(this.\u300780\u3007808\u3007O.get(i).getPath(), matrix);
        }
        this.o\u30070.computeBounds(rectF, false);
        rectF.set(rectF.left - 1.0f, rectF.top - 1.0f, rectF.right + 1.0f, rectF.bottom + 1.0f);
    }
    
    @Override
    public void \u3007o\u3007(final Canvas canvas, final Matrix localMatrix, int n) {
        if (this.\u3007o00\u3007\u3007Oo) {
            return;
        }
        L.\u3007080("GradientFillContent#draw");
        this.o\u30070.reset();
        for (int i = 0; i < this.\u300780\u3007808\u3007O.size(); ++i) {
            this.o\u30070.addPath(this.\u300780\u3007808\u3007O.get(i).getPath(), localMatrix);
        }
        this.o\u30070.computeBounds(this.oO80, false);
        Object shader;
        if (this.OO0o\u3007\u3007\u3007\u30070 == GradientType.LINEAR) {
            shader = this.\u300780\u3007808\u3007O();
        }
        else {
            shader = this.OO0o\u3007\u3007\u3007\u30070();
        }
        ((Shader)shader).setLocalMatrix(localMatrix);
        this.\u3007\u3007888.setShader((Shader)shader);
        final BaseKeyframeAnimation<ColorFilter, ColorFilter> \u3007\u3007808\u3007 = this.\u3007\u3007808\u3007;
        if (\u3007\u3007808\u3007 != null) {
            this.\u3007\u3007888.setColorFilter((ColorFilter)\u3007\u3007808\u3007.oO80());
        }
        n = (int)(n / 255.0f * this.\u3007O8o08O.oO80() / 100.0f * 255.0f);
        this.\u3007\u3007888.setAlpha(MiscUtils.\u3007o\u3007(n, 0, 255));
        canvas.drawPath(this.o\u30070, this.\u3007\u3007888);
        L.\u3007o00\u3007\u3007Oo("GradientFillContent#draw");
    }
    
    @Override
    public void \u3007\u3007888(final KeyPath keyPath, final int n, final List<KeyPath> list, final KeyPath keyPath2) {
        MiscUtils.\u3007O8o08O(keyPath, n, list, keyPath2, this);
    }
}

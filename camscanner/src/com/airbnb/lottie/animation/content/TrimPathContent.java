// 
// Decompiled by Procyon v0.6.0
// 

package com.airbnb.lottie.animation.content;

import java.util.ArrayList;
import com.airbnb.lottie.model.layer.BaseLayer;
import java.util.List;
import com.airbnb.lottie.model.content.ShapeTrimPath;
import com.airbnb.lottie.animation.keyframe.BaseKeyframeAnimation;

public class TrimPathContent implements Content, AnimationListener
{
    private final ShapeTrimPath.Type O8;
    private final BaseKeyframeAnimation<?, Float> Oo08;
    private final BaseKeyframeAnimation<?, Float> o\u30070;
    private final String \u3007080;
    private final boolean \u3007o00\u3007\u3007Oo;
    private final List<AnimationListener> \u3007o\u3007;
    private final BaseKeyframeAnimation<?, Float> \u3007\u3007888;
    
    public TrimPathContent(final BaseLayer baseLayer, final ShapeTrimPath shapeTrimPath) {
        this.\u3007o\u3007 = new ArrayList<AnimationListener>();
        this.\u3007080 = shapeTrimPath.\u3007o\u3007();
        this.\u3007o00\u3007\u3007Oo = shapeTrimPath.o\u30070();
        this.O8 = shapeTrimPath.getType();
        final BaseKeyframeAnimation<Float, Float> \u3007080 = shapeTrimPath.Oo08().\u3007080();
        this.Oo08 = \u3007080;
        final BaseKeyframeAnimation<Float, Float> \u300781 = shapeTrimPath.\u3007o00\u3007\u3007Oo().\u3007080();
        this.o\u30070 = \u300781;
        final BaseKeyframeAnimation<Float, Float> \u300782 = shapeTrimPath.O8().\u3007080();
        this.\u3007\u3007888 = \u300782;
        baseLayer.\u300780\u3007808\u3007O(\u3007080);
        baseLayer.\u300780\u3007808\u3007O(\u300781);
        baseLayer.\u300780\u3007808\u3007O(\u300782);
        \u3007080.\u3007080((BaseKeyframeAnimation.AnimationListener)this);
        \u300781.\u3007080((BaseKeyframeAnimation.AnimationListener)this);
        \u300782.\u3007080((BaseKeyframeAnimation.AnimationListener)this);
    }
    
    @Override
    public void O8() {
        for (int i = 0; i < this.\u3007o\u3007.size(); ++i) {
            this.\u3007o\u3007.get(i).O8();
        }
    }
    
    @Override
    public void Oo08(final List<Content> list, final List<Content> list2) {
    }
    
    ShapeTrimPath.Type getType() {
        return this.O8;
    }
    
    public BaseKeyframeAnimation<?, Float> oO80() {
        return this.Oo08;
    }
    
    public BaseKeyframeAnimation<?, Float> o\u30070() {
        return this.o\u30070;
    }
    
    public boolean \u300780\u3007808\u3007O() {
        return this.\u3007o00\u3007\u3007Oo;
    }
    
    void \u3007o00\u3007\u3007Oo(final AnimationListener animationListener) {
        this.\u3007o\u3007.add(animationListener);
    }
    
    public BaseKeyframeAnimation<?, Float> \u3007\u3007888() {
        return this.\u3007\u3007888;
    }
}

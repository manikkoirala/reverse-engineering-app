// 
// Decompiled by Procyon v0.6.0
// 

package com.airbnb.lottie.animation.content;

import com.airbnb.lottie.utils.MiscUtils;
import com.airbnb.lottie.model.KeyPath;
import com.airbnb.lottie.LottieProperty;
import com.airbnb.lottie.value.LottieValueCallback;
import com.airbnb.lottie.model.content.ShapeTrimPath;
import java.util.List;
import com.airbnb.lottie.model.layer.BaseLayer;
import android.graphics.PointF;
import com.airbnb.lottie.LottieDrawable;
import androidx.annotation.Nullable;
import android.graphics.Path;
import com.airbnb.lottie.model.content.PolystarShape;
import com.airbnb.lottie.animation.keyframe.BaseKeyframeAnimation;

public class PolystarContent implements PathContent, AnimationListener, KeyPathElementContent
{
    private final PolystarShape.Type O8;
    private CompoundTrimPathContent OO0o\u3007\u3007;
    private final BaseKeyframeAnimation<?, Float> OO0o\u3007\u3007\u3007\u30070;
    private final boolean Oo08;
    private boolean Oooo8o0\u3007;
    private final BaseKeyframeAnimation<?, Float> oO80;
    private final BaseKeyframeAnimation<?, Float> o\u30070;
    private final Path \u3007080;
    @Nullable
    private final BaseKeyframeAnimation<?, Float> \u300780\u3007808\u3007O;
    @Nullable
    private final BaseKeyframeAnimation<?, Float> \u30078o8o\u3007;
    private final BaseKeyframeAnimation<?, Float> \u3007O8o08O;
    private final String \u3007o00\u3007\u3007Oo;
    private final LottieDrawable \u3007o\u3007;
    private final BaseKeyframeAnimation<?, PointF> \u3007\u3007888;
    
    public PolystarContent(final LottieDrawable \u3007o\u3007, final BaseLayer baseLayer, final PolystarShape polystarShape) {
        this.\u3007080 = new Path();
        this.OO0o\u3007\u3007 = new CompoundTrimPathContent();
        this.\u3007o\u3007 = \u3007o\u3007;
        this.\u3007o00\u3007\u3007Oo = polystarShape.O8();
        final PolystarShape.Type type = polystarShape.getType();
        this.O8 = type;
        this.Oo08 = polystarShape.OO0o\u3007\u3007\u3007\u30070();
        final BaseKeyframeAnimation<Float, Float> \u3007080 = polystarShape.\u3007\u3007888().\u3007080();
        this.o\u30070 = \u3007080;
        final BaseKeyframeAnimation<PointF, PointF> \u300781 = polystarShape.oO80().\u3007080();
        this.\u3007\u3007888 = \u300781;
        final BaseKeyframeAnimation<Float, Float> \u300782 = polystarShape.\u300780\u3007808\u3007O().\u3007080();
        this.oO80 = \u300782;
        final BaseKeyframeAnimation<Float, Float> \u300783 = polystarShape.Oo08().\u3007080();
        this.OO0o\u3007\u3007\u3007\u30070 = \u300783;
        final BaseKeyframeAnimation<Float, Float> \u300784 = polystarShape.o\u30070().\u3007080();
        this.\u3007O8o08O = \u300784;
        final PolystarShape.Type star = PolystarShape.Type.STAR;
        if (type == star) {
            this.\u300780\u3007808\u3007O = polystarShape.\u3007o00\u3007\u3007Oo().\u3007080();
            this.\u30078o8o\u3007 = polystarShape.\u3007o\u3007().\u3007080();
        }
        else {
            this.\u300780\u3007808\u3007O = null;
            this.\u30078o8o\u3007 = null;
        }
        baseLayer.\u300780\u3007808\u3007O(\u3007080);
        baseLayer.\u300780\u3007808\u3007O(\u300781);
        baseLayer.\u300780\u3007808\u3007O(\u300782);
        baseLayer.\u300780\u3007808\u3007O(\u300783);
        baseLayer.\u300780\u3007808\u3007O(\u300784);
        if (type == star) {
            baseLayer.\u300780\u3007808\u3007O(this.\u300780\u3007808\u3007O);
            baseLayer.\u300780\u3007808\u3007O(this.\u30078o8o\u3007);
        }
        \u3007080.\u3007080((BaseKeyframeAnimation.AnimationListener)this);
        \u300781.\u3007080((BaseKeyframeAnimation.AnimationListener)this);
        \u300782.\u3007080((BaseKeyframeAnimation.AnimationListener)this);
        \u300783.\u3007080((BaseKeyframeAnimation.AnimationListener)this);
        \u300784.\u3007080((BaseKeyframeAnimation.AnimationListener)this);
        if (type == star) {
            this.\u300780\u3007808\u3007O.\u3007080((BaseKeyframeAnimation.AnimationListener)this);
            this.\u30078o8o\u3007.\u3007080((BaseKeyframeAnimation.AnimationListener)this);
        }
    }
    
    private void oO80() {
        final float floatValue = this.o\u30070.oO80();
        final BaseKeyframeAnimation<?, Float> oo80 = this.oO80;
        double n;
        if (oo80 == null) {
            n = 0.0;
        }
        else {
            n = oo80.oO80();
        }
        final double radians = Math.toRadians(n - 90.0);
        final double a = floatValue;
        final float n2 = (float)(6.283185307179586 / a);
        final float n3 = n2 / 2.0f;
        final float n4 = floatValue - (int)floatValue;
        final float n5 = fcmpl(n4, 0.0f);
        double n6 = radians;
        if (n5 != 0) {
            n6 = radians + (1.0f - n4) * n3;
        }
        final float floatValue2 = this.OO0o\u3007\u3007\u3007\u30070.oO80();
        final float floatValue3 = this.\u300780\u3007808\u3007O.oO80();
        final BaseKeyframeAnimation<?, Float> \u30078o8o\u3007 = this.\u30078o8o\u3007;
        float n7;
        if (\u30078o8o\u3007 != null) {
            n7 = \u30078o8o\u3007.oO80() / 100.0f;
        }
        else {
            n7 = 0.0f;
        }
        final BaseKeyframeAnimation<?, Float> \u3007o8o08O = this.\u3007O8o08O;
        float n8;
        if (\u3007o8o08O != null) {
            n8 = \u3007o8o08O.oO80() / 100.0f;
        }
        else {
            n8 = 0.0f;
        }
        float n9;
        float n11;
        float n12;
        double n13;
        if (n5 != 0) {
            n9 = (floatValue2 - floatValue3) * n4 + floatValue3;
            final double n10 = n9;
            n11 = (float)(n10 * Math.cos(n6));
            n12 = (float)(n10 * Math.sin(n6));
            this.\u3007080.moveTo(n11, n12);
            n13 = n6 + n2 * n4 / 2.0f;
        }
        else {
            final double n14 = floatValue2;
            n11 = (float)(Math.cos(n6) * n14);
            n12 = (float)(n14 * Math.sin(n6));
            this.\u3007080.moveTo(n11, n12);
            n13 = n6 + n3;
            n9 = 0.0f;
        }
        final double n15 = Math.ceil(a) * 2.0;
        int n16 = 0;
        int n17 = 0;
        float n18 = n12;
        float n19 = n11;
        final float n20 = n2;
        while (true) {
            final double n21 = n16;
            if (n21 >= n15) {
                break;
            }
            float n22;
            if (n17 != 0) {
                n22 = floatValue2;
            }
            else {
                n22 = floatValue3;
            }
            final float n23 = fcmpl(n9, 0.0f);
            float n24;
            if (n23 != 0 && n21 == n15 - 2.0) {
                n24 = n20 * n4 / 2.0f;
            }
            else {
                n24 = n3;
            }
            if (n23 != 0 && n21 == n15 - 1.0) {
                n22 = n9;
            }
            final double n25 = n22;
            final float n26 = (float)(n25 * Math.cos(n13));
            final float n27 = (float)(n25 * Math.sin(n13));
            if (n7 == 0.0f && n8 == 0.0f) {
                this.\u3007080.lineTo(n26, n27);
            }
            else {
                final double n28 = (float)(Math.atan2(n18, n19) - 1.5707963267948966);
                final float n29 = (float)Math.cos(n28);
                final float n30 = (float)Math.sin(n28);
                final double n31 = (float)(Math.atan2(n27, n26) - 1.5707963267948966);
                final float n32 = (float)Math.cos(n31);
                final float n33 = (float)Math.sin(n31);
                float n34;
                if (n17 != 0) {
                    n34 = n7;
                }
                else {
                    n34 = n8;
                }
                float n35;
                if (n17 != 0) {
                    n35 = n8;
                }
                else {
                    n35 = n7;
                }
                float n36;
                if (n17 != 0) {
                    n36 = floatValue3;
                }
                else {
                    n36 = floatValue2;
                }
                float n37;
                if (n17 != 0) {
                    n37 = floatValue2;
                }
                else {
                    n37 = floatValue3;
                }
                final float n38 = n36 * n34 * 0.47829f;
                final float n39 = n29 * n38;
                final float n40 = n38 * n30;
                final float n41 = n37 * n35 * 0.47829f;
                final float n42 = n32 * n41;
                final float n43 = n41 * n33;
                float n44 = n39;
                float n45 = n42;
                float n46 = n40;
                float n47 = n43;
                if (n5 != 0) {
                    if (n16 == 0) {
                        n44 = n39 * n4;
                        n46 = n40 * n4;
                        n45 = n42;
                        n47 = n43;
                    }
                    else {
                        n44 = n39;
                        n45 = n42;
                        n46 = n40;
                        n47 = n43;
                        if (n21 == n15 - 1.0) {
                            n45 = n42 * n4;
                            n47 = n43 * n4;
                            n46 = n40;
                            n44 = n39;
                        }
                    }
                }
                this.\u3007080.cubicTo(n19 - n44, n18 - n46, n26 + n45, n27 + n47, n26, n27);
            }
            n13 += n24;
            n17 ^= 0x1;
            ++n16;
            n19 = n26;
            n18 = n27;
        }
        final PointF pointF = this.\u3007\u3007888.oO80();
        this.\u3007080.offset(pointF.x, pointF.y);
        this.\u3007080.close();
    }
    
    private void \u300780\u3007808\u3007O() {
        this.Oooo8o0\u3007 = false;
        this.\u3007o\u3007.invalidateSelf();
    }
    
    private void \u3007o00\u3007\u3007Oo() {
        final int n = (int)Math.floor(this.o\u30070.oO80());
        final BaseKeyframeAnimation<?, Float> oo80 = this.oO80;
        double n2;
        if (oo80 == null) {
            n2 = 0.0;
        }
        else {
            n2 = oo80.oO80();
        }
        final double radians = Math.toRadians(n2 - 90.0);
        final double a = n;
        final float n3 = (float)(6.283185307179586 / a);
        final float n4 = this.\u3007O8o08O.oO80() / 100.0f;
        final float floatValue = this.OO0o\u3007\u3007\u3007\u30070.oO80();
        final double n5 = floatValue;
        float n6 = (float)(Math.cos(radians) * n5);
        float n7 = (float)(Math.sin(radians) * n5);
        this.\u3007080.moveTo(n6, n7);
        final double n8 = n3;
        double n9 = radians + n8;
        final double ceil = Math.ceil(a);
        float n11;
        float n12;
        for (int n10 = 0; n10 < ceil; ++n10, n7 = n12, n6 = n11) {
            n11 = (float)(Math.cos(n9) * n5);
            n12 = (float)(n5 * Math.sin(n9));
            if (n4 != 0.0f) {
                final double n13 = (float)(Math.atan2(n7, n6) - 1.5707963267948966);
                final float n14 = (float)Math.cos(n13);
                final float n15 = (float)Math.sin(n13);
                final double n16 = (float)(Math.atan2(n12, n11) - 1.5707963267948966);
                final float n17 = (float)Math.cos(n16);
                final float n18 = (float)Math.sin(n16);
                final float n19 = floatValue * n4 * 0.25f;
                this.\u3007080.cubicTo(n6 - n14 * n19, n7 - n15 * n19, n11 + n17 * n19, n12 + n19 * n18, n11, n12);
            }
            else {
                this.\u3007080.lineTo(n11, n12);
            }
            n9 += n8;
        }
        final PointF pointF = this.\u3007\u3007888.oO80();
        this.\u3007080.offset(pointF.x, pointF.y);
        this.\u3007080.close();
    }
    
    @Override
    public void O8() {
        this.\u300780\u3007808\u3007O();
    }
    
    @Override
    public void Oo08(final List<Content> list, final List<Content> list2) {
        for (int i = 0; i < list.size(); ++i) {
            final Content content = list.get(i);
            if (content instanceof TrimPathContent) {
                final TrimPathContent trimPathContent = (TrimPathContent)content;
                if (trimPathContent.getType() == ShapeTrimPath.Type.SIMULTANEOUSLY) {
                    this.OO0o\u3007\u3007.\u3007080(trimPathContent);
                    trimPathContent.\u3007o00\u3007\u3007Oo(this);
                }
            }
        }
    }
    
    @Override
    public String getName() {
        return this.\u3007o00\u3007\u3007Oo;
    }
    
    @Override
    public Path getPath() {
        if (this.Oooo8o0\u3007) {
            return this.\u3007080;
        }
        this.\u3007080.reset();
        if (this.Oo08) {
            this.Oooo8o0\u3007 = true;
            return this.\u3007080;
        }
        final int n = PolystarContent$1.\u3007080[this.O8.ordinal()];
        if (n != 1) {
            if (n == 2) {
                this.\u3007o00\u3007\u3007Oo();
            }
        }
        else {
            this.oO80();
        }
        this.\u3007080.close();
        this.OO0o\u3007\u3007.\u3007o00\u3007\u3007Oo(this.\u3007080);
        this.Oooo8o0\u3007 = true;
        return this.\u3007080;
    }
    
    @Override
    public <T> void o\u30070(final T t, @Nullable final LottieValueCallback<T> lottieValueCallback) {
        if (t == LottieProperty.\u30070\u3007O0088o) {
            this.o\u30070.OO0o\u3007\u3007((LottieValueCallback<Float>)lottieValueCallback);
        }
        else if (t == LottieProperty.OoO8) {
            this.oO80.OO0o\u3007\u3007((LottieValueCallback<Float>)lottieValueCallback);
        }
        else if (t == LottieProperty.OO0o\u3007\u3007\u3007\u30070) {
            this.\u3007\u3007888.OO0o\u3007\u3007((LottieValueCallback<PointF>)lottieValueCallback);
        }
        else {
            if (t == LottieProperty.o800o8O) {
                final BaseKeyframeAnimation<?, Float> \u300780\u3007808\u3007O = this.\u300780\u3007808\u3007O;
                if (\u300780\u3007808\u3007O != null) {
                    \u300780\u3007808\u3007O.OO0o\u3007\u3007((LottieValueCallback<Float>)lottieValueCallback);
                    return;
                }
            }
            if (t == LottieProperty.\u3007O888o0o) {
                this.OO0o\u3007\u3007\u3007\u30070.OO0o\u3007\u3007((LottieValueCallback<Float>)lottieValueCallback);
            }
            else {
                if (t == LottieProperty.oo88o8O) {
                    final BaseKeyframeAnimation<?, Float> \u30078o8o\u3007 = this.\u30078o8o\u3007;
                    if (\u30078o8o\u3007 != null) {
                        \u30078o8o\u3007.OO0o\u3007\u3007((LottieValueCallback<Float>)lottieValueCallback);
                        return;
                    }
                }
                if (t == LottieProperty.\u3007oo\u3007) {
                    this.\u3007O8o08O.OO0o\u3007\u3007((LottieValueCallback<Float>)lottieValueCallback);
                }
            }
        }
    }
    
    @Override
    public void \u3007\u3007888(final KeyPath keyPath, final int n, final List<KeyPath> list, final KeyPath keyPath2) {
        MiscUtils.\u3007O8o08O(keyPath, n, list, keyPath2, this);
    }
}

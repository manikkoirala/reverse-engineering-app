// 
// Decompiled by Procyon v0.6.0
// 

package com.airbnb.lottie.animation.content;

import com.airbnb.lottie.animation.keyframe.ColorKeyframeAnimation;
import android.graphics.Matrix;
import android.graphics.Canvas;
import com.airbnb.lottie.animation.keyframe.ValueCallbackKeyframeAnimation;
import com.airbnb.lottie.LottieProperty;
import com.airbnb.lottie.value.LottieValueCallback;
import com.airbnb.lottie.model.content.ShapeStroke;
import com.airbnb.lottie.LottieDrawable;
import com.airbnb.lottie.model.layer.BaseLayer;
import androidx.annotation.Nullable;
import android.graphics.ColorFilter;
import com.airbnb.lottie.animation.keyframe.BaseKeyframeAnimation;

public class StrokeContent extends BaseStrokeContent
{
    @Nullable
    private BaseKeyframeAnimation<ColorFilter, ColorFilter> \u30070\u3007O0088o;
    private final boolean \u3007O00;
    private final String \u3007O\u3007;
    private final BaseLayer \u3007\u3007808\u3007;
    private final BaseKeyframeAnimation<Integer, Integer> \u3007\u30078O0\u30078;
    
    public StrokeContent(final LottieDrawable lottieDrawable, final BaseLayer \u3007\u3007808\u3007, final ShapeStroke shapeStroke) {
        super(lottieDrawable, \u3007\u3007808\u3007, shapeStroke.\u3007o00\u3007\u3007Oo().toPaintCap(), shapeStroke.Oo08().toPaintJoin(), shapeStroke.\u3007\u3007888(), shapeStroke.\u300780\u3007808\u3007O(), shapeStroke.OO0o\u3007\u3007\u3007\u30070(), shapeStroke.o\u30070(), shapeStroke.O8());
        this.\u3007\u3007808\u3007 = \u3007\u3007808\u3007;
        this.\u3007O\u3007 = shapeStroke.oO80();
        this.\u3007O00 = shapeStroke.\u30078o8o\u3007();
        final BaseKeyframeAnimation<Integer, Integer> \u3007080 = shapeStroke.\u3007o\u3007().\u3007080();
        (this.\u3007\u30078O0\u30078 = \u3007080).\u3007080((BaseKeyframeAnimation.AnimationListener)this);
        \u3007\u3007808\u3007.\u300780\u3007808\u3007O(\u3007080);
    }
    
    @Override
    public String getName() {
        return this.\u3007O\u3007;
    }
    
    @Override
    public <T> void o\u30070(final T t, @Nullable final LottieValueCallback<T> lottieValueCallback) {
        super.o\u30070(t, lottieValueCallback);
        if (t == LottieProperty.\u3007o00\u3007\u3007Oo) {
            this.\u3007\u30078O0\u30078.OO0o\u3007\u3007((LottieValueCallback<Integer>)lottieValueCallback);
        }
        else if (t == LottieProperty.\u3007oOO8O8) {
            final BaseKeyframeAnimation<ColorFilter, ColorFilter> \u30070\u3007O0088o = this.\u30070\u3007O0088o;
            if (\u30070\u3007O0088o != null) {
                this.\u3007\u3007808\u3007.\u3007oOO8O8(\u30070\u3007O0088o);
            }
            if (lottieValueCallback == null) {
                this.\u30070\u3007O0088o = null;
            }
            else {
                (this.\u30070\u3007O0088o = new ValueCallbackKeyframeAnimation<ColorFilter, ColorFilter>((LottieValueCallback<ColorFilter>)lottieValueCallback)).\u3007080((BaseKeyframeAnimation.AnimationListener)this);
                this.\u3007\u3007808\u3007.\u300780\u3007808\u3007O(this.\u3007\u30078O0\u30078);
            }
        }
    }
    
    @Override
    public void \u3007o\u3007(final Canvas canvas, final Matrix matrix, final int n) {
        if (this.\u3007O00) {
            return;
        }
        super.\u300780\u3007808\u3007O.setColor(((ColorKeyframeAnimation)this.\u3007\u30078O0\u30078).\u3007\u3007808\u3007());
        final BaseKeyframeAnimation<ColorFilter, ColorFilter> \u30070\u3007O0088o = this.\u30070\u3007O0088o;
        if (\u30070\u3007O0088o != null) {
            super.\u300780\u3007808\u3007O.setColorFilter((ColorFilter)\u30070\u3007O0088o.oO80());
        }
        super.\u3007o\u3007(canvas, matrix, n);
    }
}

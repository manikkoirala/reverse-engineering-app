// 
// Decompiled by Procyon v0.6.0
// 

package com.airbnb.lottie.animation.content;

import com.airbnb.lottie.model.KeyPath;
import com.airbnb.lottie.utils.MiscUtils;
import com.airbnb.lottie.animation.keyframe.ColorKeyframeAnimation;
import com.airbnb.lottie.L;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.RectF;
import com.airbnb.lottie.animation.keyframe.ValueCallbackKeyframeAnimation;
import com.airbnb.lottie.LottieProperty;
import com.airbnb.lottie.value.LottieValueCallback;
import java.util.ArrayList;
import com.airbnb.lottie.animation.LPaint;
import com.airbnb.lottie.model.content.ShapeFill;
import com.airbnb.lottie.model.layer.BaseLayer;
import android.graphics.Paint;
import androidx.annotation.Nullable;
import android.graphics.ColorFilter;
import android.graphics.Path;
import java.util.List;
import com.airbnb.lottie.LottieDrawable;
import com.airbnb.lottie.animation.keyframe.BaseKeyframeAnimation;

public class FillContent implements DrawingContent, AnimationListener, KeyPathElementContent
{
    private final String O8;
    private final LottieDrawable OO0o\u3007\u3007\u3007\u30070;
    private final boolean Oo08;
    private final BaseKeyframeAnimation<Integer, Integer> oO80;
    private final List<PathContent> o\u30070;
    private final Path \u3007080;
    @Nullable
    private BaseKeyframeAnimation<ColorFilter, ColorFilter> \u300780\u3007808\u3007O;
    private final Paint \u3007o00\u3007\u3007Oo;
    private final BaseLayer \u3007o\u3007;
    private final BaseKeyframeAnimation<Integer, Integer> \u3007\u3007888;
    
    public FillContent(final LottieDrawable oo0o\u3007\u3007\u3007\u30070, final BaseLayer \u3007o\u3007, final ShapeFill shapeFill) {
        final Path \u3007080 = new Path();
        this.\u3007080 = \u3007080;
        this.\u3007o00\u3007\u3007Oo = new LPaint(1);
        this.o\u30070 = new ArrayList<PathContent>();
        this.\u3007o\u3007 = \u3007o\u3007;
        this.O8 = shapeFill.O8();
        this.Oo08 = shapeFill.o\u30070();
        this.OO0o\u3007\u3007\u3007\u30070 = oo0o\u3007\u3007\u3007\u30070;
        if (shapeFill.\u3007o00\u3007\u3007Oo() != null && shapeFill.Oo08() != null) {
            \u3007080.setFillType(shapeFill.\u3007o\u3007());
            final BaseKeyframeAnimation<Integer, Integer> \u300781 = shapeFill.\u3007o00\u3007\u3007Oo().\u3007080();
            (this.\u3007\u3007888 = \u300781).\u3007080((BaseKeyframeAnimation.AnimationListener)this);
            \u3007o\u3007.\u300780\u3007808\u3007O(\u300781);
            final BaseKeyframeAnimation<Integer, Integer> \u300782 = shapeFill.Oo08().\u3007080();
            (this.oO80 = \u300782).\u3007080((BaseKeyframeAnimation.AnimationListener)this);
            \u3007o\u3007.\u300780\u3007808\u3007O(\u300782);
            return;
        }
        this.\u3007\u3007888 = null;
        this.oO80 = null;
    }
    
    @Override
    public void O8() {
        this.OO0o\u3007\u3007\u3007\u30070.invalidateSelf();
    }
    
    @Override
    public void Oo08(final List<Content> list, final List<Content> list2) {
        for (int i = 0; i < list2.size(); ++i) {
            final Content content = list2.get(i);
            if (content instanceof PathContent) {
                this.o\u30070.add((PathContent)content);
            }
        }
    }
    
    @Override
    public String getName() {
        return this.O8;
    }
    
    @Override
    public <T> void o\u30070(final T t, @Nullable final LottieValueCallback<T> lottieValueCallback) {
        if (t == LottieProperty.\u3007080) {
            this.\u3007\u3007888.OO0o\u3007\u3007((LottieValueCallback<Integer>)lottieValueCallback);
        }
        else if (t == LottieProperty.O8) {
            this.oO80.OO0o\u3007\u3007((LottieValueCallback<Integer>)lottieValueCallback);
        }
        else if (t == LottieProperty.\u3007oOO8O8) {
            final BaseKeyframeAnimation<ColorFilter, ColorFilter> \u300780\u3007808\u3007O = this.\u300780\u3007808\u3007O;
            if (\u300780\u3007808\u3007O != null) {
                this.\u3007o\u3007.\u3007oOO8O8(\u300780\u3007808\u3007O);
            }
            if (lottieValueCallback == null) {
                this.\u300780\u3007808\u3007O = null;
            }
            else {
                (this.\u300780\u3007808\u3007O = new ValueCallbackKeyframeAnimation<ColorFilter, ColorFilter>((LottieValueCallback<ColorFilter>)lottieValueCallback)).\u3007080((BaseKeyframeAnimation.AnimationListener)this);
                this.\u3007o\u3007.\u300780\u3007808\u3007O(this.\u300780\u3007808\u3007O);
            }
        }
    }
    
    @Override
    public void \u3007080(final RectF rectF, final Matrix matrix, final boolean b) {
        this.\u3007080.reset();
        for (int i = 0; i < this.o\u30070.size(); ++i) {
            this.\u3007080.addPath(this.o\u30070.get(i).getPath(), matrix);
        }
        this.\u3007080.computeBounds(rectF, false);
        rectF.set(rectF.left - 1.0f, rectF.top - 1.0f, rectF.right + 1.0f, rectF.bottom + 1.0f);
    }
    
    @Override
    public void \u3007o\u3007(final Canvas canvas, final Matrix matrix, int i) {
        if (this.Oo08) {
            return;
        }
        L.\u3007080("FillContent#draw");
        this.\u3007o00\u3007\u3007Oo.setColor(((ColorKeyframeAnimation)this.\u3007\u3007888).\u3007\u3007808\u3007());
        final int n = (int)(i / 255.0f * this.oO80.oO80() / 100.0f * 255.0f);
        final Paint \u3007o00\u3007\u3007Oo = this.\u3007o00\u3007\u3007Oo;
        i = 0;
        \u3007o00\u3007\u3007Oo.setAlpha(MiscUtils.\u3007o\u3007(n, 0, 255));
        final BaseKeyframeAnimation<ColorFilter, ColorFilter> \u300780\u3007808\u3007O = this.\u300780\u3007808\u3007O;
        if (\u300780\u3007808\u3007O != null) {
            this.\u3007o00\u3007\u3007Oo.setColorFilter((ColorFilter)\u300780\u3007808\u3007O.oO80());
        }
        this.\u3007080.reset();
        while (i < this.o\u30070.size()) {
            this.\u3007080.addPath(this.o\u30070.get(i).getPath(), matrix);
            ++i;
        }
        canvas.drawPath(this.\u3007080, this.\u3007o00\u3007\u3007Oo);
        L.\u3007o00\u3007\u3007Oo("FillContent#draw");
    }
    
    @Override
    public void \u3007\u3007888(final KeyPath keyPath, final int n, final List<KeyPath> list, final KeyPath keyPath2) {
        MiscUtils.\u3007O8o08O(keyPath, n, list, keyPath2, this);
    }
}

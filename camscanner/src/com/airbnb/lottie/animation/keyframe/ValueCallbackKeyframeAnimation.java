// 
// Decompiled by Procyon v0.6.0
// 

package com.airbnb.lottie.animation.keyframe;

import com.airbnb.lottie.value.Keyframe;
import java.util.List;
import java.util.Collections;
import androidx.annotation.Nullable;
import com.airbnb.lottie.value.LottieValueCallback;
import com.airbnb.lottie.value.LottieFrameInfo;

public class ValueCallbackKeyframeAnimation<K, A> extends BaseKeyframeAnimation<K, A>
{
    private final A OO0o\u3007\u3007\u3007\u30070;
    private final LottieFrameInfo<A> \u300780\u3007808\u3007O;
    
    public ValueCallbackKeyframeAnimation(final LottieValueCallback<A> lottieValueCallback) {
        this((LottieValueCallback<Object>)lottieValueCallback, null);
    }
    
    public ValueCallbackKeyframeAnimation(final LottieValueCallback<A> lottieValueCallback, @Nullable final A oo0o\u3007\u3007\u3007\u30070) {
        super(Collections.emptyList());
        this.\u300780\u3007808\u3007O = new LottieFrameInfo<A>();
        this.OO0o\u3007\u3007(lottieValueCallback);
        this.OO0o\u3007\u3007\u3007\u30070 = oo0o\u3007\u3007\u3007\u30070;
    }
    
    @Override
    public void OO0o\u3007\u3007\u3007\u30070() {
        if (super.Oo08 != null) {
            super.OO0o\u3007\u3007\u3007\u30070();
        }
    }
    
    @Override
    public A oO80() {
        final LottieValueCallback<A> oo08 = super.Oo08;
        final A oo0o\u3007\u3007\u3007\u30070 = this.OO0o\u3007\u3007\u3007\u30070;
        return oo08.\u3007o00\u3007\u3007Oo(0.0f, 0.0f, oo0o\u3007\u3007\u3007\u30070, oo0o\u3007\u3007\u3007\u30070, this.o\u30070(), this.o\u30070(), this.o\u30070());
    }
    
    @Override
    A \u300780\u3007808\u3007O(final Keyframe<K> keyframe, final float n) {
        return this.oO80();
    }
    
    @Override
    public void \u3007O8o08O(final float o8) {
        super.O8 = o8;
    }
    
    @Override
    float \u3007o\u3007() {
        return 1.0f;
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.airbnb.lottie.animation.keyframe;

import com.airbnb.lottie.value.LottieValueCallback;
import android.graphics.Path;
import com.airbnb.lottie.value.Keyframe;
import java.util.List;
import android.graphics.PathMeasure;
import android.graphics.PointF;

public class PathKeyframeAnimation extends KeyframeAnimation<PointF>
{
    private final float[] OO0o\u3007\u3007\u3007\u30070;
    private final PointF \u300780\u3007808\u3007O;
    private PathKeyframe \u30078o8o\u3007;
    private PathMeasure \u3007O8o08O;
    
    public PathKeyframeAnimation(final List<? extends Keyframe<PointF>> list) {
        super(list);
        this.\u300780\u3007808\u3007O = new PointF();
        this.OO0o\u3007\u3007\u3007\u30070 = new float[2];
        this.\u3007O8o08O = new PathMeasure();
    }
    
    public PointF \u3007\u3007808\u3007(final Keyframe<PointF> keyframe, final float n) {
        final PathKeyframe \u30078o8o\u3007 = (PathKeyframe)keyframe;
        final Path oo0o\u3007\u3007\u3007\u30070 = \u30078o8o\u3007.OO0o\u3007\u3007\u3007\u30070();
        if (oo0o\u3007\u3007\u3007\u30070 == null) {
            return keyframe.\u3007o00\u3007\u3007Oo;
        }
        final LottieValueCallback<A> oo08 = super.Oo08;
        if (oo08 != null) {
            final PointF pointF = (PointF)oo08.\u3007o00\u3007\u3007Oo(\u30078o8o\u3007.Oo08, \u30078o8o\u3007.o\u30070, (A)\u30078o8o\u3007.\u3007o00\u3007\u3007Oo, (A)\u30078o8o\u3007.\u3007o\u3007, this.Oo08(), n, this.o\u30070());
            if (pointF != null) {
                return pointF;
            }
        }
        if (this.\u30078o8o\u3007 != \u30078o8o\u3007) {
            this.\u3007O8o08O.setPath(oo0o\u3007\u3007\u3007\u30070, false);
            this.\u30078o8o\u3007 = \u30078o8o\u3007;
        }
        final PathMeasure \u3007o8o08O = this.\u3007O8o08O;
        \u3007o8o08O.getPosTan(n * \u3007o8o08O.getLength(), this.OO0o\u3007\u3007\u3007\u30070, (float[])null);
        final PointF \u300780\u3007808\u3007O = this.\u300780\u3007808\u3007O;
        final float[] oo0o\u3007\u3007\u3007\u30072 = this.OO0o\u3007\u3007\u3007\u30070;
        \u300780\u3007808\u3007O.set(oo0o\u3007\u3007\u3007\u30072[0], oo0o\u3007\u3007\u3007\u30072[1]);
        return this.\u300780\u3007808\u3007O;
    }
}

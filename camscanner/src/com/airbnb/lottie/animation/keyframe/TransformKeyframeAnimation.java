// 
// Decompiled by Procyon v0.6.0
// 

package com.airbnb.lottie.animation.keyframe;

import java.util.Collections;
import com.airbnb.lottie.value.Keyframe;
import com.airbnb.lottie.LottieProperty;
import com.airbnb.lottie.value.LottieValueCallback;
import com.airbnb.lottie.model.layer.BaseLayer;
import com.airbnb.lottie.model.animatable.AnimatableTransform;
import android.graphics.PointF;
import com.airbnb.lottie.value.ScaleXY;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import android.graphics.Matrix;

public class TransformKeyframeAnimation
{
    private final Matrix O8;
    @Nullable
    private BaseKeyframeAnimation<?, Float> OO0o\u3007\u3007;
    @NonNull
    private BaseKeyframeAnimation<Integer, Integer> OO0o\u3007\u3007\u3007\u30070;
    private final float[] Oo08;
    @Nullable
    private BaseKeyframeAnimation<?, Float> Oooo8o0\u3007;
    @NonNull
    private BaseKeyframeAnimation<ScaleXY, ScaleXY> oO80;
    @NonNull
    private BaseKeyframeAnimation<PointF, PointF> o\u30070;
    private final Matrix \u3007080;
    @NonNull
    private BaseKeyframeAnimation<Float, Float> \u300780\u3007808\u3007O;
    @Nullable
    private FloatKeyframeAnimation \u30078o8o\u3007;
    @Nullable
    private FloatKeyframeAnimation \u3007O8o08O;
    private final Matrix \u3007o00\u3007\u3007Oo;
    private final Matrix \u3007o\u3007;
    @NonNull
    private BaseKeyframeAnimation<?, PointF> \u3007\u3007888;
    
    public TransformKeyframeAnimation(final AnimatableTransform animatableTransform) {
        this.\u3007080 = new Matrix();
        BaseKeyframeAnimation<PointF, PointF> \u3007080;
        if (animatableTransform.\u3007o\u3007() == null) {
            \u3007080 = null;
        }
        else {
            \u3007080 = animatableTransform.\u3007o\u3007().\u3007080();
        }
        this.o\u30070 = \u3007080;
        BaseKeyframeAnimation<?, PointF> \u300781;
        if (animatableTransform.o\u30070() == null) {
            \u300781 = null;
        }
        else {
            \u300781 = animatableTransform.o\u30070().\u3007080();
        }
        this.\u3007\u3007888 = \u300781;
        BaseKeyframeAnimation<ScaleXY, ScaleXY> \u300782;
        if (animatableTransform.oO80() == null) {
            \u300782 = null;
        }
        else {
            \u300782 = animatableTransform.oO80().\u3007080();
        }
        this.oO80 = \u300782;
        BaseKeyframeAnimation<Float, Float> \u300783;
        if (animatableTransform.\u3007\u3007888() == null) {
            \u300783 = null;
        }
        else {
            \u300783 = animatableTransform.\u3007\u3007888().\u3007080();
        }
        this.\u300780\u3007808\u3007O = \u300783;
        FloatKeyframeAnimation \u30078o8o\u3007;
        if (animatableTransform.\u300780\u3007808\u3007O() == null) {
            \u30078o8o\u3007 = null;
        }
        else {
            \u30078o8o\u3007 = (FloatKeyframeAnimation)animatableTransform.\u300780\u3007808\u3007O().\u3007080();
        }
        this.\u30078o8o\u3007 = \u30078o8o\u3007;
        if (\u30078o8o\u3007 != null) {
            this.\u3007o00\u3007\u3007Oo = new Matrix();
            this.\u3007o\u3007 = new Matrix();
            this.O8 = new Matrix();
            this.Oo08 = new float[9];
        }
        else {
            this.\u3007o00\u3007\u3007Oo = null;
            this.\u3007o\u3007 = null;
            this.O8 = null;
            this.Oo08 = null;
        }
        FloatKeyframeAnimation \u3007o8o08O;
        if (animatableTransform.OO0o\u3007\u3007\u3007\u30070() == null) {
            \u3007o8o08O = null;
        }
        else {
            \u3007o8o08O = (FloatKeyframeAnimation)animatableTransform.OO0o\u3007\u3007\u3007\u30070().\u3007080();
        }
        this.\u3007O8o08O = \u3007o8o08O;
        if (animatableTransform.Oo08() != null) {
            this.OO0o\u3007\u3007\u3007\u30070 = animatableTransform.Oo08().\u3007080();
        }
        if (animatableTransform.\u30078o8o\u3007() != null) {
            this.OO0o\u3007\u3007 = animatableTransform.\u30078o8o\u3007().\u3007080();
        }
        else {
            this.OO0o\u3007\u3007 = null;
        }
        if (animatableTransform.O8() != null) {
            this.Oooo8o0\u3007 = animatableTransform.O8().\u3007080();
        }
        else {
            this.Oooo8o0\u3007 = null;
        }
    }
    
    private void O8() {
        for (int i = 0; i < 9; ++i) {
            this.Oo08[i] = 0.0f;
        }
    }
    
    public void OO0o\u3007\u3007\u3007\u30070(final float n) {
        final BaseKeyframeAnimation<Integer, Integer> oo0o\u3007\u3007\u3007\u30070 = this.OO0o\u3007\u3007\u3007\u30070;
        if (oo0o\u3007\u3007\u3007\u30070 != null) {
            oo0o\u3007\u3007\u3007\u30070.\u3007O8o08O(n);
        }
        final BaseKeyframeAnimation<?, Float> oo0o\u3007\u3007 = this.OO0o\u3007\u3007;
        if (oo0o\u3007\u3007 != null) {
            oo0o\u3007\u3007.\u3007O8o08O(n);
        }
        final BaseKeyframeAnimation<?, Float> oooo8o0\u3007 = this.Oooo8o0\u3007;
        if (oooo8o0\u3007 != null) {
            oooo8o0\u3007.\u3007O8o08O(n);
        }
        final BaseKeyframeAnimation<PointF, PointF> o\u30070 = this.o\u30070;
        if (o\u30070 != null) {
            o\u30070.\u3007O8o08O(n);
        }
        final BaseKeyframeAnimation<?, PointF> \u3007\u3007888 = this.\u3007\u3007888;
        if (\u3007\u3007888 != null) {
            \u3007\u3007888.\u3007O8o08O(n);
        }
        final BaseKeyframeAnimation<ScaleXY, ScaleXY> oo80 = this.oO80;
        if (oo80 != null) {
            oo80.\u3007O8o08O(n);
        }
        final BaseKeyframeAnimation<Float, Float> \u300780\u3007808\u3007O = this.\u300780\u3007808\u3007O;
        if (\u300780\u3007808\u3007O != null) {
            \u300780\u3007808\u3007O.\u3007O8o08O(n);
        }
        final FloatKeyframeAnimation \u30078o8o\u3007 = this.\u30078o8o\u3007;
        if (\u30078o8o\u3007 != null) {
            \u30078o8o\u3007.\u3007O8o08O(n);
        }
        final FloatKeyframeAnimation \u3007o8o08O = this.\u3007O8o08O;
        if (\u3007o8o08O != null) {
            \u3007o8o08O.\u3007O8o08O(n);
        }
    }
    
    @Nullable
    public BaseKeyframeAnimation<?, Float> Oo08() {
        return this.Oooo8o0\u3007;
    }
    
    @Nullable
    public BaseKeyframeAnimation<?, Integer> oO80() {
        return this.OO0o\u3007\u3007\u3007\u30070;
    }
    
    public Matrix o\u30070() {
        this.\u3007080.reset();
        final BaseKeyframeAnimation<?, PointF> \u3007\u3007888 = this.\u3007\u3007888;
        if (\u3007\u3007888 != null) {
            final PointF pointF = \u3007\u3007888.oO80();
            final float x = pointF.x;
            if (x != 0.0f || pointF.y != 0.0f) {
                this.\u3007080.preTranslate(x, pointF.y);
            }
        }
        final BaseKeyframeAnimation<Float, Float> \u300780\u3007808\u3007O = this.\u300780\u3007808\u3007O;
        if (\u300780\u3007808\u3007O != null) {
            float n;
            if (\u300780\u3007808\u3007O instanceof ValueCallbackKeyframeAnimation) {
                n = \u300780\u3007808\u3007O.oO80();
            }
            else {
                n = ((FloatKeyframeAnimation)\u300780\u3007808\u3007O).\u3007\u3007808\u3007();
            }
            if (n != 0.0f) {
                this.\u3007080.preRotate(n);
            }
        }
        if (this.\u30078o8o\u3007 != null) {
            final FloatKeyframeAnimation \u3007o8o08O = this.\u3007O8o08O;
            float n2;
            if (\u3007o8o08O == null) {
                n2 = 0.0f;
            }
            else {
                n2 = (float)Math.cos(Math.toRadians(-\u3007o8o08O.\u3007\u3007808\u3007() + 90.0f));
            }
            final FloatKeyframeAnimation \u3007o8o08O2 = this.\u3007O8o08O;
            float n3;
            if (\u3007o8o08O2 == null) {
                n3 = 1.0f;
            }
            else {
                n3 = (float)Math.sin(Math.toRadians(-\u3007o8o08O2.\u3007\u3007808\u3007() + 90.0f));
            }
            final float n4 = (float)Math.tan(Math.toRadians(this.\u30078o8o\u3007.\u3007\u3007808\u3007()));
            this.O8();
            final float[] oo08 = this.Oo08;
            oo08[0] = n2;
            oo08[1] = n3;
            final float n5 = -n3;
            oo08[3] = n5;
            oo08[4] = n2;
            oo08[8] = 1.0f;
            this.\u3007o00\u3007\u3007Oo.setValues(oo08);
            this.O8();
            final float[] oo9 = this.Oo08;
            oo9[0] = 1.0f;
            oo9[3] = n4;
            oo9[8] = (oo9[4] = 1.0f);
            this.\u3007o\u3007.setValues(oo9);
            this.O8();
            final float[] oo10 = this.Oo08;
            oo10[0] = n2;
            oo10[1] = n5;
            oo10[3] = n3;
            oo10[4] = n2;
            oo10[8] = 1.0f;
            this.O8.setValues(oo10);
            this.\u3007o\u3007.preConcat(this.\u3007o00\u3007\u3007Oo);
            this.O8.preConcat(this.\u3007o\u3007);
            this.\u3007080.preConcat(this.O8);
        }
        final BaseKeyframeAnimation<ScaleXY, ScaleXY> oo11 = this.oO80;
        if (oo11 != null) {
            final ScaleXY scaleXY = oo11.oO80();
            if (scaleXY.\u3007o00\u3007\u3007Oo() != 1.0f || scaleXY.\u3007o\u3007() != 1.0f) {
                this.\u3007080.preScale(scaleXY.\u3007o00\u3007\u3007Oo(), scaleXY.\u3007o\u3007());
            }
        }
        final BaseKeyframeAnimation<PointF, PointF> o\u30070 = this.o\u30070;
        if (o\u30070 != null) {
            final PointF pointF2 = o\u30070.oO80();
            final float x2 = pointF2.x;
            if (x2 != 0.0f || pointF2.y != 0.0f) {
                this.\u3007080.preTranslate(-x2, -pointF2.y);
            }
        }
        return this.\u3007080;
    }
    
    public void \u3007080(final BaseLayer baseLayer) {
        baseLayer.\u300780\u3007808\u3007O(this.OO0o\u3007\u3007\u3007\u30070);
        baseLayer.\u300780\u3007808\u3007O(this.OO0o\u3007\u3007);
        baseLayer.\u300780\u3007808\u3007O(this.Oooo8o0\u3007);
        baseLayer.\u300780\u3007808\u3007O(this.o\u30070);
        baseLayer.\u300780\u3007808\u3007O(this.\u3007\u3007888);
        baseLayer.\u300780\u3007808\u3007O(this.oO80);
        baseLayer.\u300780\u3007808\u3007O(this.\u300780\u3007808\u3007O);
        baseLayer.\u300780\u3007808\u3007O(this.\u30078o8o\u3007);
        baseLayer.\u300780\u3007808\u3007O(this.\u3007O8o08O);
    }
    
    @Nullable
    public BaseKeyframeAnimation<?, Float> \u300780\u3007808\u3007O() {
        return this.OO0o\u3007\u3007;
    }
    
    public void \u3007o00\u3007\u3007Oo(final BaseKeyframeAnimation.AnimationListener animationListener) {
        final BaseKeyframeAnimation<Integer, Integer> oo0o\u3007\u3007\u3007\u30070 = this.OO0o\u3007\u3007\u3007\u30070;
        if (oo0o\u3007\u3007\u3007\u30070 != null) {
            oo0o\u3007\u3007\u3007\u30070.\u3007080(animationListener);
        }
        final BaseKeyframeAnimation<?, Float> oo0o\u3007\u3007 = this.OO0o\u3007\u3007;
        if (oo0o\u3007\u3007 != null) {
            oo0o\u3007\u3007.\u3007080(animationListener);
        }
        final BaseKeyframeAnimation<?, Float> oooo8o0\u3007 = this.Oooo8o0\u3007;
        if (oooo8o0\u3007 != null) {
            oooo8o0\u3007.\u3007080(animationListener);
        }
        final BaseKeyframeAnimation<PointF, PointF> o\u30070 = this.o\u30070;
        if (o\u30070 != null) {
            o\u30070.\u3007080(animationListener);
        }
        final BaseKeyframeAnimation<?, PointF> \u3007\u3007888 = this.\u3007\u3007888;
        if (\u3007\u3007888 != null) {
            \u3007\u3007888.\u3007080(animationListener);
        }
        final BaseKeyframeAnimation<ScaleXY, ScaleXY> oo80 = this.oO80;
        if (oo80 != null) {
            oo80.\u3007080(animationListener);
        }
        final BaseKeyframeAnimation<Float, Float> \u300780\u3007808\u3007O = this.\u300780\u3007808\u3007O;
        if (\u300780\u3007808\u3007O != null) {
            \u300780\u3007808\u3007O.\u3007080(animationListener);
        }
        final FloatKeyframeAnimation \u30078o8o\u3007 = this.\u30078o8o\u3007;
        if (\u30078o8o\u3007 != null) {
            \u30078o8o\u3007.\u3007080(animationListener);
        }
        final FloatKeyframeAnimation \u3007o8o08O = this.\u3007O8o08O;
        if (\u3007o8o08O != null) {
            \u3007o8o08O.\u3007080(animationListener);
        }
    }
    
    public <T> boolean \u3007o\u3007(final T t, @Nullable final LottieValueCallback<T> lottieValueCallback) {
        if (t == LottieProperty.Oo08) {
            final BaseKeyframeAnimation<PointF, PointF> o\u30070 = this.o\u30070;
            if (o\u30070 == null) {
                this.o\u30070 = new ValueCallbackKeyframeAnimation<PointF, PointF>((LottieValueCallback<PointF>)lottieValueCallback, new PointF());
            }
            else {
                o\u30070.OO0o\u3007\u3007((LottieValueCallback<PointF>)lottieValueCallback);
            }
        }
        else if (t == LottieProperty.o\u30070) {
            final BaseKeyframeAnimation<?, PointF> \u3007\u3007888 = this.\u3007\u3007888;
            if (\u3007\u3007888 == null) {
                this.\u3007\u3007888 = new ValueCallbackKeyframeAnimation<Object, PointF>((LottieValueCallback<Object>)lottieValueCallback, new PointF());
            }
            else {
                \u3007\u3007888.OO0o\u3007\u3007((LottieValueCallback<PointF>)lottieValueCallback);
            }
        }
        else if (t == LottieProperty.\u30078o8o\u3007) {
            final BaseKeyframeAnimation<ScaleXY, ScaleXY> oo80 = this.oO80;
            if (oo80 == null) {
                this.oO80 = new ValueCallbackKeyframeAnimation<ScaleXY, ScaleXY>((LottieValueCallback<ScaleXY>)lottieValueCallback, new ScaleXY());
            }
            else {
                oo80.OO0o\u3007\u3007((LottieValueCallback<ScaleXY>)lottieValueCallback);
            }
        }
        else if (t == LottieProperty.\u3007O8o08O) {
            final BaseKeyframeAnimation<Float, Float> \u300780\u3007808\u3007O = this.\u300780\u3007808\u3007O;
            if (\u300780\u3007808\u3007O == null) {
                this.\u300780\u3007808\u3007O = new ValueCallbackKeyframeAnimation<Float, Float>((LottieValueCallback<Float>)lottieValueCallback, 0.0f);
            }
            else {
                \u300780\u3007808\u3007O.OO0o\u3007\u3007((LottieValueCallback<Float>)lottieValueCallback);
            }
        }
        else {
            if (t != LottieProperty.\u3007o\u3007) {
                if (t == LottieProperty.o\u3007O8\u3007\u3007o) {
                    final BaseKeyframeAnimation<?, Float> oo0o\u3007\u3007 = this.OO0o\u3007\u3007;
                    if (oo0o\u3007\u3007 != null) {
                        if (oo0o\u3007\u3007 == null) {
                            this.OO0o\u3007\u3007 = new ValueCallbackKeyframeAnimation<Object, Float>((LottieValueCallback<Float>)lottieValueCallback, Float.valueOf(100));
                            return true;
                        }
                        oo0o\u3007\u3007.OO0o\u3007\u3007((LottieValueCallback<Float>)lottieValueCallback);
                        return true;
                    }
                }
                if (t == LottieProperty.\u300700) {
                    final BaseKeyframeAnimation<?, Float> oooo8o0\u3007 = this.Oooo8o0\u3007;
                    if (oooo8o0\u3007 != null) {
                        if (oooo8o0\u3007 == null) {
                            this.Oooo8o0\u3007 = new ValueCallbackKeyframeAnimation<Object, Float>((LottieValueCallback<Float>)lottieValueCallback, Float.valueOf(100));
                            return true;
                        }
                        oooo8o0\u3007.OO0o\u3007\u3007((LottieValueCallback<Float>)lottieValueCallback);
                        return true;
                    }
                }
                if (t == LottieProperty.OO0o\u3007\u3007) {
                    final FloatKeyframeAnimation \u30078o8o\u3007 = this.\u30078o8o\u3007;
                    if (\u30078o8o\u3007 != null) {
                        if (\u30078o8o\u3007 == null) {
                            this.\u30078o8o\u3007 = new FloatKeyframeAnimation(Collections.singletonList(new Keyframe<Float>(0.0f)));
                        }
                        ((BaseKeyframeAnimation<K, T>)this.\u30078o8o\u3007).OO0o\u3007\u3007((LottieValueCallback<T>)lottieValueCallback);
                        return true;
                    }
                }
                if (t == LottieProperty.Oooo8o0\u3007) {
                    final FloatKeyframeAnimation \u3007o8o08O = this.\u3007O8o08O;
                    if (\u3007o8o08O != null) {
                        if (\u3007o8o08O == null) {
                            this.\u3007O8o08O = new FloatKeyframeAnimation(Collections.singletonList(new Keyframe<Float>(0.0f)));
                        }
                        ((BaseKeyframeAnimation<K, T>)this.\u3007O8o08O).OO0o\u3007\u3007((LottieValueCallback<T>)lottieValueCallback);
                        return true;
                    }
                }
                return false;
            }
            final BaseKeyframeAnimation<Integer, Integer> oo0o\u3007\u3007\u3007\u30070 = this.OO0o\u3007\u3007\u3007\u30070;
            if (oo0o\u3007\u3007\u3007\u30070 == null) {
                this.OO0o\u3007\u3007\u3007\u30070 = new ValueCallbackKeyframeAnimation<Integer, Integer>((LottieValueCallback<Integer>)lottieValueCallback, 100);
            }
            else {
                oo0o\u3007\u3007\u3007\u30070.OO0o\u3007\u3007((LottieValueCallback<Integer>)lottieValueCallback);
            }
        }
        return true;
    }
    
    public Matrix \u3007\u3007888(final float n) {
        final BaseKeyframeAnimation<?, PointF> \u3007\u3007888 = this.\u3007\u3007888;
        final PointF pointF = null;
        PointF pointF2;
        if (\u3007\u3007888 == null) {
            pointF2 = null;
        }
        else {
            pointF2 = \u3007\u3007888.oO80();
        }
        final BaseKeyframeAnimation<ScaleXY, ScaleXY> oo80 = this.oO80;
        ScaleXY scaleXY;
        if (oo80 == null) {
            scaleXY = null;
        }
        else {
            scaleXY = oo80.oO80();
        }
        this.\u3007080.reset();
        if (pointF2 != null) {
            this.\u3007080.preTranslate(pointF2.x * n, pointF2.y * n);
        }
        if (scaleXY != null) {
            final Matrix \u3007080 = this.\u3007080;
            final double a = scaleXY.\u3007o00\u3007\u3007Oo();
            final double n2 = n;
            \u3007080.preScale((float)Math.pow(a, n2), (float)Math.pow(scaleXY.\u3007o\u3007(), n2));
        }
        final BaseKeyframeAnimation<Float, Float> \u300780\u3007808\u3007O = this.\u300780\u3007808\u3007O;
        if (\u300780\u3007808\u3007O != null) {
            final float floatValue = \u300780\u3007808\u3007O.oO80();
            final BaseKeyframeAnimation<PointF, PointF> o\u30070 = this.o\u30070;
            PointF pointF3;
            if (o\u30070 == null) {
                pointF3 = pointF;
            }
            else {
                pointF3 = o\u30070.oO80();
            }
            final Matrix \u300781 = this.\u3007080;
            float y = 0.0f;
            float x;
            if (pointF3 == null) {
                x = 0.0f;
            }
            else {
                x = pointF3.x;
            }
            if (pointF3 != null) {
                y = pointF3.y;
            }
            \u300781.preRotate(floatValue * n, x, y);
        }
        return this.\u3007080;
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.airbnb.lottie.animation.keyframe;

import android.animation.TimeInterpolator;
import androidx.annotation.NonNull;
import com.airbnb.lottie.L;
import androidx.annotation.FloatRange;
import java.util.ArrayList;
import com.airbnb.lottie.value.Keyframe;
import java.util.List;
import androidx.annotation.Nullable;
import com.airbnb.lottie.value.LottieValueCallback;

public abstract class BaseKeyframeAnimation<K, A>
{
    protected float O8;
    @Nullable
    protected LottieValueCallback<A> Oo08;
    private float oO80;
    @Nullable
    private A o\u30070;
    final List<AnimationListener> \u3007080;
    private boolean \u3007o00\u3007\u3007Oo;
    private final KeyframesWrapper<K> \u3007o\u3007;
    private float \u3007\u3007888;
    
    BaseKeyframeAnimation(final List<? extends Keyframe<K>> list) {
        this.\u3007080 = new ArrayList<AnimationListener>(1);
        this.\u3007o00\u3007\u3007Oo = false;
        this.O8 = 0.0f;
        this.o\u30070 = null;
        this.\u3007\u3007888 = -1.0f;
        this.oO80 = -1.0f;
        this.\u3007o\u3007 = Oooo8o0\u3007(list);
    }
    
    private static <T> KeyframesWrapper<T> Oooo8o0\u3007(final List<? extends Keyframe<T>> list) {
        if (list.isEmpty()) {
            return (KeyframesWrapper<T>)new EmptyKeyframeWrapper();
        }
        if (list.size() != 0) {
            return (KeyframesWrapper<T>)new SingleKeyframeWrapper((List<? extends Keyframe<Object>>)list);
        }
        return (KeyframesWrapper<T>)new KeyframesWrapperImpl((List<? extends Keyframe<Object>>)list);
    }
    
    @FloatRange(from = 0.0, to = 1.0)
    private float \u3007\u3007888() {
        if (this.\u3007\u3007888 == -1.0f) {
            this.\u3007\u3007888 = this.\u3007o\u3007.\u3007o00\u3007\u3007Oo();
        }
        return this.\u3007\u3007888;
    }
    
    protected float O8() {
        final Keyframe<K> \u3007o00\u3007\u3007Oo = this.\u3007o00\u3007\u3007Oo();
        if (\u3007o00\u3007\u3007Oo.oO80()) {
            return 0.0f;
        }
        return ((TimeInterpolator)\u3007o00\u3007\u3007Oo.O8).getInterpolation(this.Oo08());
    }
    
    public void OO0o\u3007\u3007(@Nullable final LottieValueCallback<A> oo08) {
        final LottieValueCallback<A> oo9 = this.Oo08;
        if (oo9 != null) {
            oo9.\u3007o\u3007(null);
        }
        if ((this.Oo08 = oo08) != null) {
            oo08.\u3007o\u3007(this);
        }
    }
    
    public void OO0o\u3007\u3007\u3007\u30070() {
        for (int i = 0; i < this.\u3007080.size(); ++i) {
            this.\u3007080.get(i).O8();
        }
    }
    
    float Oo08() {
        if (this.\u3007o00\u3007\u3007Oo) {
            return 0.0f;
        }
        final Keyframe<K> \u3007o00\u3007\u3007Oo = this.\u3007o00\u3007\u3007Oo();
        if (\u3007o00\u3007\u3007Oo.oO80()) {
            return 0.0f;
        }
        return (this.O8 - \u3007o00\u3007\u3007Oo.Oo08()) / (\u3007o00\u3007\u3007Oo.\u3007o00\u3007\u3007Oo() - \u3007o00\u3007\u3007Oo.Oo08());
    }
    
    public A oO80() {
        final float o8 = this.O8();
        if (this.Oo08 == null && this.\u3007o\u3007.\u3007o\u3007(o8)) {
            return this.o\u30070;
        }
        return this.o\u30070 = this.\u300780\u3007808\u3007O(this.\u3007o00\u3007\u3007Oo(), o8);
    }
    
    public float o\u30070() {
        return this.O8;
    }
    
    public void \u3007080(final AnimationListener animationListener) {
        this.\u3007080.add(animationListener);
    }
    
    abstract A \u300780\u3007808\u3007O(final Keyframe<K> p0, final float p1);
    
    public void \u30078o8o\u3007() {
        this.\u3007o00\u3007\u3007Oo = true;
    }
    
    public void \u3007O8o08O(@FloatRange(from = 0.0, to = 1.0) final float n) {
        if (this.\u3007o\u3007.isEmpty()) {
            return;
        }
        float o8;
        if (n < this.\u3007\u3007888()) {
            o8 = this.\u3007\u3007888();
        }
        else {
            o8 = n;
            if (n > this.\u3007o\u3007()) {
                o8 = this.\u3007o\u3007();
            }
        }
        if (o8 == this.O8) {
            return;
        }
        this.O8 = o8;
        if (this.\u3007o\u3007.O8(o8)) {
            this.OO0o\u3007\u3007\u3007\u30070();
        }
    }
    
    protected Keyframe<K> \u3007o00\u3007\u3007Oo() {
        L.\u3007080("BaseKeyframeAnimation#getCurrentKeyframe");
        final Keyframe<K> \u3007080 = this.\u3007o\u3007.\u3007080();
        L.\u3007o00\u3007\u3007Oo("BaseKeyframeAnimation#getCurrentKeyframe");
        return \u3007080;
    }
    
    @FloatRange(from = 0.0, to = 1.0)
    float \u3007o\u3007() {
        if (this.oO80 == -1.0f) {
            this.oO80 = this.\u3007o\u3007.Oo08();
        }
        return this.oO80;
    }
    
    public interface AnimationListener
    {
        void O8();
    }
    
    private static final class EmptyKeyframeWrapper<T> implements KeyframesWrapper<T>
    {
        @Override
        public boolean O8(final float n) {
            return false;
        }
        
        @Override
        public float Oo08() {
            return 1.0f;
        }
        
        @Override
        public boolean isEmpty() {
            return true;
        }
        
        @Override
        public Keyframe<T> \u3007080() {
            throw new IllegalStateException("not implemented");
        }
        
        @Override
        public float \u3007o00\u3007\u3007Oo() {
            return 0.0f;
        }
        
        @Override
        public boolean \u3007o\u3007(final float n) {
            throw new IllegalStateException("not implemented");
        }
    }
    
    private interface KeyframesWrapper<T>
    {
        boolean O8(final float p0);
        
        @FloatRange(from = 0.0, to = 1.0)
        float Oo08();
        
        boolean isEmpty();
        
        Keyframe<T> \u3007080();
        
        @FloatRange(from = 0.0, to = 1.0)
        float \u3007o00\u3007\u3007Oo();
        
        boolean \u3007o\u3007(final float p0);
    }
    
    private static final class KeyframesWrapperImpl<T> implements KeyframesWrapper<T>
    {
        private float O8;
        private final List<? extends Keyframe<T>> \u3007080;
        @NonNull
        private Keyframe<T> \u3007o00\u3007\u3007Oo;
        private Keyframe<T> \u3007o\u3007;
        
        KeyframesWrapperImpl(final List<? extends Keyframe<T>> \u3007080) {
            this.\u3007o\u3007 = null;
            this.O8 = -1.0f;
            this.\u3007080 = \u3007080;
            this.\u3007o00\u3007\u3007Oo = this.o\u30070(0.0f);
        }
        
        private Keyframe<T> o\u30070(final float n) {
            final List<? extends Keyframe<T>> \u3007080 = this.\u3007080;
            final Keyframe keyframe = \u3007080.get(\u3007080.size() - 1);
            if (n >= keyframe.Oo08()) {
                return keyframe;
            }
            for (int i = this.\u3007080.size() - 2; i >= 1; --i) {
                final Keyframe keyframe2 = (Keyframe)this.\u3007080.get(i);
                if (this.\u3007o00\u3007\u3007Oo != keyframe2) {
                    if (keyframe2.\u3007080(n)) {
                        return keyframe2;
                    }
                }
            }
            return (Keyframe)this.\u3007080.get(0);
        }
        
        @Override
        public boolean O8(final float n) {
            if (this.\u3007o00\u3007\u3007Oo.\u3007080(n)) {
                return this.\u3007o00\u3007\u3007Oo.oO80() ^ true;
            }
            this.\u3007o00\u3007\u3007Oo = this.o\u30070(n);
            return true;
        }
        
        @Override
        public float Oo08() {
            final List<? extends Keyframe<T>> \u3007080 = this.\u3007080;
            return ((Keyframe)\u3007080.get(\u3007080.size() - 1)).\u3007o00\u3007\u3007Oo();
        }
        
        @Override
        public boolean isEmpty() {
            return false;
        }
        
        @NonNull
        @Override
        public Keyframe<T> \u3007080() {
            return this.\u3007o00\u3007\u3007Oo;
        }
        
        @Override
        public float \u3007o00\u3007\u3007Oo() {
            return ((Keyframe)this.\u3007080.get(0)).Oo08();
        }
        
        @Override
        public boolean \u3007o\u3007(final float o8) {
            final Keyframe<T> \u3007o\u3007 = this.\u3007o\u3007;
            final Keyframe<T> \u3007o00\u3007\u3007Oo = this.\u3007o00\u3007\u3007Oo;
            if (\u3007o\u3007 == \u3007o00\u3007\u3007Oo && this.O8 == o8) {
                return true;
            }
            this.\u3007o\u3007 = \u3007o00\u3007\u3007Oo;
            this.O8 = o8;
            return false;
        }
    }
    
    private static final class SingleKeyframeWrapper<T> implements KeyframesWrapper<T>
    {
        @NonNull
        private final Keyframe<T> \u3007080;
        private float \u3007o00\u3007\u3007Oo;
        
        SingleKeyframeWrapper(final List<? extends Keyframe<T>> list) {
            this.\u3007o00\u3007\u3007Oo = -1.0f;
            this.\u3007080 = (Keyframe)list.get(0);
        }
        
        @Override
        public boolean O8(final float n) {
            return this.\u3007080.oO80() ^ true;
        }
        
        @Override
        public float Oo08() {
            return this.\u3007080.\u3007o00\u3007\u3007Oo();
        }
        
        @Override
        public boolean isEmpty() {
            return false;
        }
        
        @Override
        public Keyframe<T> \u3007080() {
            return this.\u3007080;
        }
        
        @Override
        public float \u3007o00\u3007\u3007Oo() {
            return this.\u3007080.Oo08();
        }
        
        @Override
        public boolean \u3007o\u3007(final float \u3007o00\u3007\u3007Oo) {
            if (this.\u3007o00\u3007\u3007Oo == \u3007o00\u3007\u3007Oo) {
                return true;
            }
            this.\u3007o00\u3007\u3007Oo = \u3007o00\u3007\u3007Oo;
            return false;
        }
    }
}

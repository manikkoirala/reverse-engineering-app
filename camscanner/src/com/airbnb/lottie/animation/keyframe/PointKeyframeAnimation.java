// 
// Decompiled by Procyon v0.6.0
// 

package com.airbnb.lottie.animation.keyframe;

import com.airbnb.lottie.value.LottieValueCallback;
import com.airbnb.lottie.value.Keyframe;
import java.util.List;
import android.graphics.PointF;

public class PointKeyframeAnimation extends KeyframeAnimation<PointF>
{
    private final PointF \u300780\u3007808\u3007O;
    
    public PointKeyframeAnimation(final List<Keyframe<PointF>> list) {
        super(list);
        this.\u300780\u3007808\u3007O = new PointF();
    }
    
    public PointF \u3007\u3007808\u3007(final Keyframe<PointF> keyframe, final float n) {
        final PointF \u3007o00\u3007\u3007Oo = keyframe.\u3007o00\u3007\u3007Oo;
        if (\u3007o00\u3007\u3007Oo != null) {
            final PointF \u3007o\u3007 = keyframe.\u3007o\u3007;
            if (\u3007o\u3007 != null) {
                final PointF pointF = \u3007o00\u3007\u3007Oo;
                final PointF pointF2 = \u3007o\u3007;
                final LottieValueCallback<A> oo08 = super.Oo08;
                if (oo08 != null) {
                    final PointF pointF3 = (PointF)oo08.\u3007o00\u3007\u3007Oo(keyframe.Oo08, keyframe.o\u30070, (A)pointF, (A)pointF2, n, this.Oo08(), this.o\u30070());
                    if (pointF3 != null) {
                        return pointF3;
                    }
                }
                final PointF \u300780\u3007808\u3007O = this.\u300780\u3007808\u3007O;
                final float x = pointF.x;
                final float x2 = pointF2.x;
                final float y = pointF.y;
                \u300780\u3007808\u3007O.set(x + (x2 - x) * n, y + n * (pointF2.y - y));
                return this.\u300780\u3007808\u3007O;
            }
        }
        throw new IllegalStateException("Missing values for keyframe.");
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.airbnb.lottie.animation.keyframe;

import com.airbnb.lottie.value.LottieValueCallback;
import com.airbnb.lottie.utils.GammaEvaluator;
import com.airbnb.lottie.utils.MiscUtils;
import com.airbnb.lottie.value.Keyframe;
import java.util.List;

public class ColorKeyframeAnimation extends KeyframeAnimation<Integer>
{
    public ColorKeyframeAnimation(final List<Keyframe<Integer>> list) {
        super(list);
    }
    
    Integer \u3007O00(final Keyframe<Integer> keyframe, final float n) {
        return this.\u3007O\u3007(keyframe, n);
    }
    
    public int \u3007O\u3007(final Keyframe<Integer> keyframe, final float n) {
        final Integer \u3007o00\u3007\u3007Oo = keyframe.\u3007o00\u3007\u3007Oo;
        if (\u3007o00\u3007\u3007Oo != null && keyframe.\u3007o\u3007 != null) {
            final int intValue = \u3007o00\u3007\u3007Oo;
            final int intValue2 = keyframe.\u3007o\u3007;
            final LottieValueCallback<A> oo08 = super.Oo08;
            if (oo08 != null) {
                final Integer n2 = (Integer)oo08.\u3007o00\u3007\u3007Oo(keyframe.Oo08, keyframe.o\u30070, (A)intValue, (A)intValue2, n, this.Oo08(), this.o\u30070());
                if (n2 != null) {
                    return n2;
                }
            }
            return GammaEvaluator.\u3007o\u3007(MiscUtils.\u3007o00\u3007\u3007Oo(n, 0.0f, 1.0f), intValue, intValue2);
        }
        throw new IllegalStateException("Missing values for keyframe.");
    }
    
    public int \u3007\u3007808\u3007() {
        return this.\u3007O\u3007(((BaseKeyframeAnimation<Integer, A>)this).\u3007o00\u3007\u3007Oo(), this.O8());
    }
}

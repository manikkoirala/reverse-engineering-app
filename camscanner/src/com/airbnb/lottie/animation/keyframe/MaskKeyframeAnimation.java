// 
// Decompiled by Procyon v0.6.0
// 

package com.airbnb.lottie.animation.keyframe;

import java.util.ArrayList;
import com.airbnb.lottie.model.content.Mask;
import android.graphics.Path;
import com.airbnb.lottie.model.content.ShapeData;
import java.util.List;

public class MaskKeyframeAnimation
{
    private final List<BaseKeyframeAnimation<ShapeData, Path>> \u3007080;
    private final List<BaseKeyframeAnimation<Integer, Integer>> \u3007o00\u3007\u3007Oo;
    private final List<Mask> \u3007o\u3007;
    
    public MaskKeyframeAnimation(final List<Mask> \u3007o\u3007) {
        this.\u3007o\u3007 = \u3007o\u3007;
        this.\u3007080 = new ArrayList<BaseKeyframeAnimation<ShapeData, Path>>(\u3007o\u3007.size());
        this.\u3007o00\u3007\u3007Oo = new ArrayList<BaseKeyframeAnimation<Integer, Integer>>(\u3007o\u3007.size());
        for (int i = 0; i < \u3007o\u3007.size(); ++i) {
            this.\u3007080.add(((Mask)\u3007o\u3007.get(i)).\u3007o00\u3007\u3007Oo().\u3007080());
            this.\u3007o00\u3007\u3007Oo.add(((Mask)\u3007o\u3007.get(i)).\u3007o\u3007().\u3007080());
        }
    }
    
    public List<BaseKeyframeAnimation<ShapeData, Path>> \u3007080() {
        return this.\u3007080;
    }
    
    public List<Mask> \u3007o00\u3007\u3007Oo() {
        return this.\u3007o\u3007;
    }
    
    public List<BaseKeyframeAnimation<Integer, Integer>> \u3007o\u3007() {
        return this.\u3007o00\u3007\u3007Oo;
    }
}

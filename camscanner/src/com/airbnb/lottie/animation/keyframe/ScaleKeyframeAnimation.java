// 
// Decompiled by Procyon v0.6.0
// 

package com.airbnb.lottie.animation.keyframe;

import com.airbnb.lottie.value.LottieValueCallback;
import com.airbnb.lottie.utils.MiscUtils;
import com.airbnb.lottie.value.Keyframe;
import java.util.List;
import com.airbnb.lottie.value.ScaleXY;

public class ScaleKeyframeAnimation extends KeyframeAnimation<ScaleXY>
{
    private final ScaleXY \u300780\u3007808\u3007O;
    
    public ScaleKeyframeAnimation(final List<Keyframe<ScaleXY>> list) {
        super(list);
        this.\u300780\u3007808\u3007O = new ScaleXY();
    }
    
    public ScaleXY \u3007\u3007808\u3007(final Keyframe<ScaleXY> keyframe, final float n) {
        final ScaleXY \u3007o00\u3007\u3007Oo = keyframe.\u3007o00\u3007\u3007Oo;
        if (\u3007o00\u3007\u3007Oo != null) {
            final ScaleXY \u3007o\u3007 = keyframe.\u3007o\u3007;
            if (\u3007o\u3007 != null) {
                final ScaleXY scaleXY = \u3007o00\u3007\u3007Oo;
                final ScaleXY scaleXY2 = \u3007o\u3007;
                final LottieValueCallback<A> oo08 = super.Oo08;
                if (oo08 != null) {
                    final ScaleXY scaleXY3 = (ScaleXY)oo08.\u3007o00\u3007\u3007Oo(keyframe.Oo08, keyframe.o\u30070, (A)scaleXY, (A)scaleXY2, n, this.Oo08(), this.o\u30070());
                    if (scaleXY3 != null) {
                        return scaleXY3;
                    }
                }
                this.\u300780\u3007808\u3007O.O8(MiscUtils.OO0o\u3007\u3007\u3007\u30070(scaleXY.\u3007o00\u3007\u3007Oo(), scaleXY2.\u3007o00\u3007\u3007Oo(), n), MiscUtils.OO0o\u3007\u3007\u3007\u30070(scaleXY.\u3007o\u3007(), scaleXY2.\u3007o\u3007(), n));
                return this.\u300780\u3007808\u3007O;
            }
        }
        throw new IllegalStateException("Missing values for keyframe.");
    }
}

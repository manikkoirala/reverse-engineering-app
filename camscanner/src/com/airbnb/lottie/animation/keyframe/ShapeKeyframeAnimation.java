// 
// Decompiled by Procyon v0.6.0
// 

package com.airbnb.lottie.animation.keyframe;

import com.airbnb.lottie.utils.MiscUtils;
import com.airbnb.lottie.value.Keyframe;
import java.util.List;
import android.graphics.Path;
import com.airbnb.lottie.model.content.ShapeData;

public class ShapeKeyframeAnimation extends BaseKeyframeAnimation<ShapeData, Path>
{
    private final Path OO0o\u3007\u3007\u3007\u30070;
    private final ShapeData \u300780\u3007808\u3007O;
    
    public ShapeKeyframeAnimation(final List<Keyframe<ShapeData>> list) {
        super(list);
        this.\u300780\u3007808\u3007O = new ShapeData();
        this.OO0o\u3007\u3007\u3007\u30070 = new Path();
    }
    
    public Path \u3007\u3007808\u3007(final Keyframe<ShapeData> keyframe, final float n) {
        this.\u300780\u3007808\u3007O.\u3007o\u3007(keyframe.\u3007o00\u3007\u3007Oo, keyframe.\u3007o\u3007, n);
        MiscUtils.oO80(this.\u300780\u3007808\u3007O, this.OO0o\u3007\u3007\u3007\u30070);
        return this.OO0o\u3007\u3007\u3007\u30070;
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.airbnb.lottie.animation.keyframe;

import com.airbnb.lottie.value.Keyframe;
import java.util.List;
import java.util.Collections;
import android.graphics.PointF;

public class SplitDimensionPathKeyframeAnimation extends BaseKeyframeAnimation<PointF, PointF>
{
    private final BaseKeyframeAnimation<Float, Float> OO0o\u3007\u3007\u3007\u30070;
    private final PointF \u300780\u3007808\u3007O;
    private final BaseKeyframeAnimation<Float, Float> \u30078o8o\u3007;
    
    public SplitDimensionPathKeyframeAnimation(final BaseKeyframeAnimation<Float, Float> oo0o\u3007\u3007\u3007\u30070, final BaseKeyframeAnimation<Float, Float> \u30078o8o\u3007) {
        super(Collections.emptyList());
        this.\u300780\u3007808\u3007O = new PointF();
        this.OO0o\u3007\u3007\u3007\u30070 = oo0o\u3007\u3007\u3007\u30070;
        this.\u30078o8o\u3007 = \u30078o8o\u3007;
        this.\u3007O8o08O(this.o\u30070());
    }
    
    @Override
    public void \u3007O8o08O(final float n) {
        this.OO0o\u3007\u3007\u3007\u30070.\u3007O8o08O(n);
        this.\u30078o8o\u3007.\u3007O8o08O(n);
        this.\u300780\u3007808\u3007O.set((float)this.OO0o\u3007\u3007\u3007\u30070.oO80(), (float)this.\u30078o8o\u3007.oO80());
        for (int i = 0; i < super.\u3007080.size(); ++i) {
            ((AnimationListener)super.\u3007080.get(i)).O8();
        }
    }
    
    PointF \u3007O\u3007(final Keyframe<PointF> keyframe, final float n) {
        return this.\u300780\u3007808\u3007O;
    }
    
    public PointF \u3007\u3007808\u3007() {
        return this.\u3007O\u3007(null, 0.0f);
    }
}

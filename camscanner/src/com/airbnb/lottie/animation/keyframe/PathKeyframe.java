// 
// Decompiled by Procyon v0.6.0
// 

package com.airbnb.lottie.animation.keyframe;

import com.airbnb.lottie.utils.Utils;
import com.airbnb.lottie.LottieComposition;
import androidx.annotation.Nullable;
import android.graphics.Path;
import android.graphics.PointF;
import com.airbnb.lottie.value.Keyframe;

public class PathKeyframe extends Keyframe<PointF>
{
    private final Keyframe<PointF> \u3007O\u3007;
    @Nullable
    private Path \u3007\u3007808\u3007;
    
    public PathKeyframe(final LottieComposition lottieComposition, final Keyframe<PointF> \u3007o\u3007) {
        super(lottieComposition, \u3007o\u3007.\u3007o00\u3007\u3007Oo, \u3007o\u3007.\u3007o\u3007, \u3007o\u3007.O8, \u3007o\u3007.Oo08, \u3007o\u3007.o\u30070);
        this.\u3007O\u3007 = \u3007o\u3007;
        this.\u300780\u3007808\u3007O();
    }
    
    @Nullable
    Path OO0o\u3007\u3007\u3007\u30070() {
        return this.\u3007\u3007808\u3007;
    }
    
    public void \u300780\u3007808\u3007O() {
        final T \u3007o\u3007 = (T)super.\u3007o\u3007;
        boolean b = false;
        Label_0049: {
            if (\u3007o\u3007 != null) {
                final T \u3007o00\u3007\u3007Oo = (T)super.\u3007o00\u3007\u3007Oo;
                if (\u3007o00\u3007\u3007Oo != null && ((PointF)\u3007o00\u3007\u3007Oo).equals(((PointF)\u3007o\u3007).x, ((PointF)\u3007o\u3007).y)) {
                    b = true;
                    break Label_0049;
                }
            }
            b = false;
        }
        final T \u3007o\u30072 = (T)super.\u3007o\u3007;
        if (\u3007o\u30072 != null && !b) {
            final PointF pointF = (PointF)super.\u3007o00\u3007\u3007Oo;
            final PointF pointF2 = (PointF)\u3007o\u30072;
            final Keyframe<PointF> \u3007o\u30073 = this.\u3007O\u3007;
            this.\u3007\u3007808\u3007 = Utils.O8(pointF, pointF2, \u3007o\u30073.OO0o\u3007\u3007, \u3007o\u30073.Oooo8o0\u3007);
        }
    }
}

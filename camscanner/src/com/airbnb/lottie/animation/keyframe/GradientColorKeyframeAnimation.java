// 
// Decompiled by Procyon v0.6.0
// 

package com.airbnb.lottie.animation.keyframe;

import com.airbnb.lottie.value.Keyframe;
import java.util.List;
import com.airbnb.lottie.model.content.GradientColor;

public class GradientColorKeyframeAnimation extends KeyframeAnimation<GradientColor>
{
    private final GradientColor \u300780\u3007808\u3007O;
    
    public GradientColorKeyframeAnimation(final List<Keyframe<GradientColor>> list) {
        super(list);
        int \u3007o\u3007 = 0;
        final GradientColor gradientColor = (GradientColor)list.get(0).\u3007o00\u3007\u3007Oo;
        if (gradientColor != null) {
            \u3007o\u3007 = gradientColor.\u3007o\u3007();
        }
        this.\u300780\u3007808\u3007O = new GradientColor(new float[\u3007o\u3007], new int[\u3007o\u3007]);
    }
    
    GradientColor \u3007\u3007808\u3007(final Keyframe<GradientColor> keyframe, final float n) {
        this.\u300780\u3007808\u3007O.O8(keyframe.\u3007o00\u3007\u3007Oo, keyframe.\u3007o\u3007, n);
        return this.\u300780\u3007808\u3007O;
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.airbnb.lottie.animation.keyframe;

import com.airbnb.lottie.value.LottieValueCallback;
import com.airbnb.lottie.utils.MiscUtils;
import com.airbnb.lottie.value.Keyframe;
import java.util.List;

public class IntegerKeyframeAnimation extends KeyframeAnimation<Integer>
{
    public IntegerKeyframeAnimation(final List<Keyframe<Integer>> list) {
        super(list);
    }
    
    Integer \u3007O00(final Keyframe<Integer> keyframe, final float n) {
        return this.\u3007O\u3007(keyframe, n);
    }
    
    int \u3007O\u3007(final Keyframe<Integer> keyframe, final float n) {
        if (keyframe.\u3007o00\u3007\u3007Oo != null && keyframe.\u3007o\u3007 != null) {
            final LottieValueCallback<A> oo08 = super.Oo08;
            if (oo08 != null) {
                final Integer n2 = (Integer)oo08.\u3007o00\u3007\u3007Oo(keyframe.Oo08, keyframe.o\u30070, (A)keyframe.\u3007o00\u3007\u3007Oo, (A)keyframe.\u3007o\u3007, n, this.Oo08(), this.o\u30070());
                if (n2 != null) {
                    return n2;
                }
            }
            return MiscUtils.\u30078o8o\u3007(keyframe.\u3007\u3007888(), keyframe.O8(), n);
        }
        throw new IllegalStateException("Missing values for keyframe.");
    }
    
    public int \u3007\u3007808\u3007() {
        return this.\u3007O\u3007(((BaseKeyframeAnimation<Integer, A>)this).\u3007o00\u3007\u3007Oo(), this.O8());
    }
}

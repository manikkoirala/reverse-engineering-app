// 
// Decompiled by Procyon v0.6.0
// 

package com.airbnb.lottie.value;

import androidx.annotation.RestrictTo;
import androidx.annotation.Nullable;
import com.airbnb.lottie.animation.keyframe.BaseKeyframeAnimation;

public class LottieValueCallback<T>
{
    private final LottieFrameInfo<T> \u3007080;
    @Nullable
    private BaseKeyframeAnimation<?, ?> \u3007o00\u3007\u3007Oo;
    @Nullable
    protected T \u3007o\u3007;
    
    public LottieValueCallback(@Nullable final T \u3007o\u3007) {
        this.\u3007080 = new LottieFrameInfo<T>();
        this.\u3007o\u3007 = \u3007o\u3007;
    }
    
    @Nullable
    public T \u3007080(final LottieFrameInfo<T> lottieFrameInfo) {
        return this.\u3007o\u3007;
    }
    
    @Nullable
    @RestrictTo({ RestrictTo.Scope.LIBRARY })
    public final T \u3007o00\u3007\u3007Oo(final float n, final float n2, final T t, final T t2, final float n3, final float n4, final float n5) {
        return this.\u3007080(this.\u3007080.\u3007080(n, n2, t, t2, n3, n4, n5));
    }
    
    @RestrictTo({ RestrictTo.Scope.LIBRARY })
    public final void \u3007o\u3007(@Nullable final BaseKeyframeAnimation<?, ?> \u3007o00\u3007\u3007Oo) {
        this.\u3007o00\u3007\u3007Oo = \u3007o00\u3007\u3007Oo;
    }
}

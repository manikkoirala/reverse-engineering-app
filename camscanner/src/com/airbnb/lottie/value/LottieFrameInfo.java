// 
// Decompiled by Procyon v0.6.0
// 

package com.airbnb.lottie.value;

import androidx.annotation.RestrictTo;

public class LottieFrameInfo<T>
{
    private T O8;
    private float Oo08;
    private float o\u30070;
    private float \u3007080;
    private float \u3007o00\u3007\u3007Oo;
    private T \u3007o\u3007;
    private float \u3007\u3007888;
    
    @RestrictTo({ RestrictTo.Scope.LIBRARY })
    public LottieFrameInfo<T> \u3007080(final float \u3007080, final float \u3007o00\u3007\u3007Oo, final T \u3007o\u3007, final T o8, final float oo08, final float o\u30070, final float \u3007\u3007888) {
        this.\u3007080 = \u3007080;
        this.\u3007o00\u3007\u3007Oo = \u3007o00\u3007\u3007Oo;
        this.\u3007o\u3007 = \u3007o\u3007;
        this.O8 = o8;
        this.Oo08 = oo08;
        this.o\u30070 = o\u30070;
        this.\u3007\u3007888 = \u3007\u3007888;
        return this;
    }
}

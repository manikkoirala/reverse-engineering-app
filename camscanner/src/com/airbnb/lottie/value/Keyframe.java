// 
// Decompiled by Procyon v0.6.0
// 

package com.airbnb.lottie.value;

import androidx.annotation.FloatRange;
import com.airbnb.lottie.LottieComposition;
import android.graphics.PointF;
import androidx.annotation.Nullable;
import android.view.animation.Interpolator;

public class Keyframe<T>
{
    @Nullable
    public final Interpolator O8;
    public PointF OO0o\u3007\u3007;
    private int OO0o\u3007\u3007\u3007\u30070;
    public final float Oo08;
    public PointF Oooo8o0\u3007;
    private float oO80;
    @Nullable
    public Float o\u30070;
    @Nullable
    private final LottieComposition \u3007080;
    private int \u300780\u3007808\u3007O;
    private float \u30078o8o\u3007;
    private float \u3007O8o08O;
    @Nullable
    public final T \u3007o00\u3007\u3007Oo;
    @Nullable
    public T \u3007o\u3007;
    private float \u3007\u3007888;
    
    public Keyframe(final LottieComposition \u3007080, @Nullable final T \u3007o00\u3007\u3007Oo, @Nullable final T \u3007o\u3007, @Nullable final Interpolator o8, final float oo08, @Nullable final Float o\u30070) {
        this.\u3007\u3007888 = -3987645.8f;
        this.oO80 = -3987645.8f;
        this.\u300780\u3007808\u3007O = 784923401;
        this.OO0o\u3007\u3007\u3007\u30070 = 784923401;
        this.\u30078o8o\u3007 = Float.MIN_VALUE;
        this.\u3007O8o08O = Float.MIN_VALUE;
        this.OO0o\u3007\u3007 = null;
        this.Oooo8o0\u3007 = null;
        this.\u3007080 = \u3007080;
        this.\u3007o00\u3007\u3007Oo = \u3007o00\u3007\u3007Oo;
        this.\u3007o\u3007 = \u3007o\u3007;
        this.O8 = o8;
        this.Oo08 = oo08;
        this.o\u30070 = o\u30070;
    }
    
    public Keyframe(final T t) {
        this.\u3007\u3007888 = -3987645.8f;
        this.oO80 = -3987645.8f;
        this.\u300780\u3007808\u3007O = 784923401;
        this.OO0o\u3007\u3007\u3007\u30070 = 784923401;
        this.\u30078o8o\u3007 = Float.MIN_VALUE;
        this.\u3007O8o08O = Float.MIN_VALUE;
        this.OO0o\u3007\u3007 = null;
        this.Oooo8o0\u3007 = null;
        this.\u3007080 = null;
        this.\u3007o00\u3007\u3007Oo = t;
        this.\u3007o\u3007 = t;
        this.O8 = null;
        this.Oo08 = Float.MIN_VALUE;
        this.o\u30070 = Float.MAX_VALUE;
    }
    
    public int O8() {
        if (this.OO0o\u3007\u3007\u3007\u30070 == 784923401) {
            this.OO0o\u3007\u3007\u3007\u30070 = (int)this.\u3007o\u3007;
        }
        return this.OO0o\u3007\u3007\u3007\u30070;
    }
    
    public float Oo08() {
        final LottieComposition \u3007080 = this.\u3007080;
        if (\u3007080 == null) {
            return 0.0f;
        }
        if (this.\u30078o8o\u3007 == Float.MIN_VALUE) {
            this.\u30078o8o\u3007 = (this.Oo08 - \u3007080.\u3007\u3007808\u3007()) / this.\u3007080.Oo08();
        }
        return this.\u30078o8o\u3007;
    }
    
    public boolean oO80() {
        return this.O8 == null;
    }
    
    public float o\u30070() {
        if (this.\u3007\u3007888 == -3987645.8f) {
            this.\u3007\u3007888 = (float)this.\u3007o00\u3007\u3007Oo;
        }
        return this.\u3007\u3007888;
    }
    
    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append("Keyframe{startValue=");
        sb.append(this.\u3007o00\u3007\u3007Oo);
        sb.append(", endValue=");
        sb.append(this.\u3007o\u3007);
        sb.append(", startFrame=");
        sb.append(this.Oo08);
        sb.append(", endFrame=");
        sb.append(this.o\u30070);
        sb.append(", interpolator=");
        sb.append(this.O8);
        sb.append('}');
        return sb.toString();
    }
    
    public boolean \u3007080(@FloatRange(from = 0.0, to = 1.0) final float n) {
        return n >= this.Oo08() && n < this.\u3007o00\u3007\u3007Oo();
    }
    
    public float \u3007o00\u3007\u3007Oo() {
        if (this.\u3007080 == null) {
            return 1.0f;
        }
        if (this.\u3007O8o08O == Float.MIN_VALUE) {
            if (this.o\u30070 == null) {
                this.\u3007O8o08O = 1.0f;
            }
            else {
                this.\u3007O8o08O = this.Oo08() + (this.o\u30070 - this.Oo08) / this.\u3007080.Oo08();
            }
        }
        return this.\u3007O8o08O;
    }
    
    public float \u3007o\u3007() {
        if (this.oO80 == -3987645.8f) {
            this.oO80 = (float)this.\u3007o\u3007;
        }
        return this.oO80;
    }
    
    public int \u3007\u3007888() {
        if (this.\u300780\u3007808\u3007O == 784923401) {
            this.\u300780\u3007808\u3007O = (int)this.\u3007o00\u3007\u3007Oo;
        }
        return this.\u300780\u3007808\u3007O;
    }
}

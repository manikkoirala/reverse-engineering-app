// 
// Decompiled by Procyon v0.6.0
// 

package com.airbnb.lottie.value;

public class ScaleXY
{
    private float \u3007080;
    private float \u3007o00\u3007\u3007Oo;
    
    public ScaleXY() {
        this(1.0f, 1.0f);
    }
    
    public ScaleXY(final float \u3007080, final float \u3007o00\u3007\u3007Oo) {
        this.\u3007080 = \u3007080;
        this.\u3007o00\u3007\u3007Oo = \u3007o00\u3007\u3007Oo;
    }
    
    public void O8(final float \u3007080, final float \u3007o00\u3007\u3007Oo) {
        this.\u3007080 = \u3007080;
        this.\u3007o00\u3007\u3007Oo = \u3007o00\u3007\u3007Oo;
    }
    
    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append(this.\u3007o00\u3007\u3007Oo());
        sb.append("x");
        sb.append(this.\u3007o\u3007());
        return sb.toString();
    }
    
    public boolean \u3007080(final float n, final float n2) {
        return this.\u3007080 == n && this.\u3007o00\u3007\u3007Oo == n2;
    }
    
    public float \u3007o00\u3007\u3007Oo() {
        return this.\u3007080;
    }
    
    public float \u3007o\u3007() {
        return this.\u3007o00\u3007\u3007Oo;
    }
}

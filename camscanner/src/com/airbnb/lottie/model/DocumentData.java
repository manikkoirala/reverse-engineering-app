// 
// Decompiled by Procyon v0.6.0
// 

package com.airbnb.lottie.model;

import androidx.annotation.ColorInt;
import androidx.annotation.RestrictTo;

@RestrictTo({ RestrictTo.Scope.LIBRARY })
public class DocumentData
{
    public final Justification O8;
    public final float OO0o\u3007\u3007\u3007\u30070;
    public final int Oo08;
    @ColorInt
    public final int oO80;
    public final float o\u30070;
    public final String \u3007080;
    @ColorInt
    public final int \u300780\u3007808\u3007O;
    public final boolean \u30078o8o\u3007;
    public final String \u3007o00\u3007\u3007Oo;
    public final float \u3007o\u3007;
    public final float \u3007\u3007888;
    
    public DocumentData(final String \u3007080, final String \u3007o00\u3007\u3007Oo, final float \u3007o\u3007, final Justification o8, final int oo08, final float o\u30070, final float \u3007\u3007888, @ColorInt final int oo9, @ColorInt final int \u300780\u3007808\u3007O, final float oo0o\u3007\u3007\u3007\u30070, final boolean \u30078o8o\u3007) {
        this.\u3007080 = \u3007080;
        this.\u3007o00\u3007\u3007Oo = \u3007o00\u3007\u3007Oo;
        this.\u3007o\u3007 = \u3007o\u3007;
        this.O8 = o8;
        this.Oo08 = oo08;
        this.o\u30070 = o\u30070;
        this.\u3007\u3007888 = \u3007\u3007888;
        this.oO80 = oo9;
        this.\u300780\u3007808\u3007O = \u300780\u3007808\u3007O;
        this.OO0o\u3007\u3007\u3007\u30070 = oo0o\u3007\u3007\u3007\u30070;
        this.\u30078o8o\u3007 = \u30078o8o\u3007;
    }
    
    @Override
    public int hashCode() {
        final int n = (int)((this.\u3007080.hashCode() * 31 + this.\u3007o00\u3007\u3007Oo.hashCode()) * 31 + this.\u3007o\u3007);
        final int ordinal = this.O8.ordinal();
        final int oo08 = this.Oo08;
        final long n2 = Float.floatToRawIntBits(this.o\u30070);
        return (((n * 31 + ordinal) * 31 + oo08) * 31 + (int)(n2 ^ n2 >>> 32)) * 31 + this.oO80;
    }
    
    public enum Justification
    {
        private static final Justification[] $VALUES;
        
        CENTER, 
        LEFT_ALIGN, 
        RIGHT_ALIGN;
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.airbnb.lottie.model;

import androidx.annotation.Nullable;
import androidx.annotation.VisibleForTesting;
import com.airbnb.lottie.LottieComposition;
import androidx.collection.LruCache;
import androidx.annotation.RestrictTo;

@RestrictTo({ RestrictTo.Scope.LIBRARY })
public class LottieCompositionCache
{
    private static final LottieCompositionCache \u3007o00\u3007\u3007Oo;
    private final LruCache<String, LottieComposition> \u3007080;
    
    static {
        \u3007o00\u3007\u3007Oo = new LottieCompositionCache();
    }
    
    @VisibleForTesting
    LottieCompositionCache() {
        this.\u3007080 = new LruCache<String, LottieComposition>(20);
    }
    
    public static LottieCompositionCache \u3007o00\u3007\u3007Oo() {
        return LottieCompositionCache.\u3007o00\u3007\u3007Oo;
    }
    
    @Nullable
    public LottieComposition \u3007080(@Nullable final String s) {
        if (s == null) {
            return null;
        }
        return this.\u3007080.get(s);
    }
    
    public void \u3007o\u3007(@Nullable final String s, final LottieComposition lottieComposition) {
        if (s == null) {
            return;
        }
        this.\u3007080.put(s, lottieComposition);
    }
}

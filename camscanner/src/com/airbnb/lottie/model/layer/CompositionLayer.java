// 
// Decompiled by Procyon v0.6.0
// 

package com.airbnb.lottie.model.layer;

import com.airbnb.lottie.model.KeyPath;
import com.airbnb.lottie.animation.keyframe.ValueCallbackKeyframeAnimation;
import com.airbnb.lottie.LottieProperty;
import com.airbnb.lottie.value.LottieValueCallback;
import androidx.annotation.FloatRange;
import com.airbnb.lottie.utils.Utils;
import com.airbnb.lottie.L;
import android.graphics.Matrix;
import android.graphics.Canvas;
import com.airbnb.lottie.model.animatable.AnimatableFloatValue;
import androidx.collection.LongSparseArray;
import java.util.ArrayList;
import com.airbnb.lottie.LottieComposition;
import com.airbnb.lottie.LottieDrawable;
import androidx.annotation.Nullable;
import com.airbnb.lottie.animation.keyframe.BaseKeyframeAnimation;
import java.util.List;
import android.graphics.RectF;
import android.graphics.Paint;

public class CompositionLayer extends BaseLayer
{
    private Paint O8ooOoo\u3007;
    private final RectF O\u30078O8\u3007008;
    private final List<BaseLayer> o\u3007O8\u3007\u3007o;
    private final RectF \u300700;
    @Nullable
    private BaseKeyframeAnimation<Float, Float> \u3007oo\u3007;
    
    public CompositionLayer(final LottieDrawable lottieDrawable, final Layer layer, final List<Layer> list, final LottieComposition lottieComposition) {
        super(lottieDrawable, layer);
        this.o\u3007O8\u3007\u3007o = new ArrayList<BaseLayer>();
        this.\u300700 = new RectF();
        this.O\u30078O8\u3007008 = new RectF();
        this.O8ooOoo\u3007 = new Paint();
        final AnimatableFloatValue \u30070\u3007O0088o = layer.\u30070\u3007O0088o();
        if (\u30070\u3007O0088o != null) {
            this.\u300780\u3007808\u3007O(this.\u3007oo\u3007 = \u30070\u3007O0088o.\u3007080());
            this.\u3007oo\u3007.\u3007080((BaseKeyframeAnimation.AnimationListener)this);
        }
        else {
            this.\u3007oo\u3007 = null;
        }
        final LongSparseArray<BaseLayer> longSparseArray = new LongSparseArray<BaseLayer>(lottieComposition.OO0o\u3007\u3007\u3007\u30070().size());
        int n = list.size() - 1;
        BaseLayer baseLayer = null;
        int i;
        while (true) {
            i = 0;
            if (n < 0) {
                break;
            }
            final Layer layer2 = list.get(n);
            final BaseLayer o800o8O = BaseLayer.o800o8O(layer2, lottieDrawable, lottieComposition);
            if (o800o8O != null) {
                longSparseArray.put(o800o8O.\u3007O888o0o().\u3007o00\u3007\u3007Oo(), o800o8O);
                if (baseLayer != null) {
                    baseLayer.o\u3007\u30070\u3007(o800o8O);
                    baseLayer = null;
                }
                else {
                    this.o\u3007O8\u3007\u3007o.add(0, o800o8O);
                    final int n2 = CompositionLayer$1.\u3007080[layer2.o\u30070().ordinal()];
                    if (n2 == 1 || n2 == 2) {
                        baseLayer = o800o8O;
                    }
                }
            }
            --n;
        }
        while (i < longSparseArray.size()) {
            final BaseLayer baseLayer2 = longSparseArray.get(longSparseArray.keyAt(i));
            if (baseLayer2 != null) {
                final BaseLayer baseLayer3 = longSparseArray.get(baseLayer2.\u3007O888o0o().oO80());
                if (baseLayer3 != null) {
                    baseLayer2.OOO\u3007O0(baseLayer3);
                }
            }
            ++i;
        }
    }
    
    @Override
    void OoO8(final Canvas canvas, final Matrix matrix, int alpha) {
        L.\u3007080("CompositionLayer#draw");
        this.O\u30078O8\u3007008.set(0.0f, 0.0f, (float)super.\u3007\u3007808\u3007.OO0o\u3007\u3007\u3007\u30070(), (float)super.\u3007\u3007808\u3007.\u300780\u3007808\u3007O());
        matrix.mapRect(this.O\u30078O8\u3007008);
        final boolean b = super.Oooo8o0\u3007.oo\u3007() && this.o\u3007O8\u3007\u3007o.size() > 1 && alpha != 255;
        if (b) {
            this.O8ooOoo\u3007.setAlpha(alpha);
            Utils.OO0o\u3007\u3007(canvas, this.O\u30078O8\u3007008, this.O8ooOoo\u3007);
        }
        else {
            canvas.save();
        }
        if (b) {
            alpha = 255;
        }
        for (int i = this.o\u3007O8\u3007\u3007o.size() - 1; i >= 0; --i) {
            if (this.O\u30078O8\u3007008.isEmpty() || canvas.clipRect(this.O\u30078O8\u3007008)) {
                this.o\u3007O8\u3007\u3007o.get(i).\u3007o\u3007(canvas, matrix, alpha);
            }
        }
        canvas.restore();
        L.\u3007o00\u3007\u3007Oo("CompositionLayer#draw");
    }
    
    public void oo\u3007(@FloatRange(from = 0.0, to = 1.0) float \u3007\u3007808\u3007) {
        super.oo\u3007(\u3007\u3007808\u3007);
        float n = \u3007\u3007808\u3007;
        if (this.\u3007oo\u3007 != null) {
            final float oo08 = super.Oooo8o0\u3007.\u3007\u3007808\u3007().Oo08();
            \u3007\u3007808\u3007 = super.\u3007\u3007808\u3007.\u3007080().\u3007\u3007808\u3007();
            n = (this.\u3007oo\u3007.oO80() * super.\u3007\u3007808\u3007.\u3007080().oO80() - \u3007\u3007808\u3007) / (oo08 + 0.01f);
        }
        \u3007\u3007808\u3007 = n;
        if (this.\u3007oo\u3007 == null) {
            \u3007\u3007808\u3007 = n - super.\u3007\u3007808\u3007.\u3007O\u3007();
        }
        float n2 = \u3007\u3007808\u3007;
        if (super.\u3007\u3007808\u3007.OoO8() != 0.0f) {
            n2 = \u3007\u3007808\u3007 / super.\u3007\u3007808\u3007.OoO8();
        }
        for (int i = this.o\u3007O8\u3007\u3007o.size() - 1; i >= 0; --i) {
            this.o\u3007O8\u3007\u3007o.get(i).oo\u3007(n2);
        }
    }
    
    @Override
    public <T> void o\u30070(final T t, @Nullable final LottieValueCallback<T> lottieValueCallback) {
        super.o\u30070(t, lottieValueCallback);
        if (t == LottieProperty.O\u30078O8\u3007008) {
            if (lottieValueCallback == null) {
                final BaseKeyframeAnimation<Float, Float> \u3007oo\u3007 = this.\u3007oo\u3007;
                if (\u3007oo\u3007 != null) {
                    \u3007oo\u3007.OO0o\u3007\u3007(null);
                }
            }
            else {
                (this.\u3007oo\u3007 = new ValueCallbackKeyframeAnimation<Float, Float>((LottieValueCallback<Float>)lottieValueCallback)).\u3007080((BaseKeyframeAnimation.AnimationListener)this);
                this.\u300780\u3007808\u3007O(this.\u3007oo\u3007);
            }
        }
    }
    
    protected void \u30070000OOO(final KeyPath keyPath, final int n, final List<KeyPath> list, final KeyPath keyPath2) {
        for (int i = 0; i < this.o\u3007O8\u3007\u3007o.size(); ++i) {
            this.o\u3007O8\u3007\u3007o.get(i).\u3007\u3007888(keyPath, n, list, keyPath2);
        }
    }
    
    @Override
    public void \u3007080(final RectF rectF, final Matrix matrix, final boolean b) {
        super.\u3007080(rectF, matrix, b);
        for (int i = this.o\u3007O8\u3007\u3007o.size() - 1; i >= 0; --i) {
            this.\u300700.set(0.0f, 0.0f, 0.0f, 0.0f);
            this.o\u3007O8\u3007\u3007o.get(i).\u3007080(this.\u300700, super.OO0o\u3007\u3007, true);
            rectF.union(this.\u300700);
        }
    }
}

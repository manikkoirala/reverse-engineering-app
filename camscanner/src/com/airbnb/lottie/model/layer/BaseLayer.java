// 
// Decompiled by Procyon v0.6.0
// 

package com.airbnb.lottie.model.layer;

import com.airbnb.lottie.model.KeyPath;
import androidx.annotation.CallSuper;
import com.airbnb.lottie.value.LottieValueCallback;
import androidx.annotation.FloatRange;
import com.airbnb.lottie.animation.content.Content;
import java.util.Collections;
import android.os.Build$VERSION;
import com.airbnb.lottie.L;
import com.airbnb.lottie.utils.Logger;
import com.airbnb.lottie.LottieComposition;
import com.airbnb.lottie.utils.Utils;
import com.airbnb.lottie.model.content.Mask;
import android.graphics.Canvas;
import com.airbnb.lottie.model.content.ShapeData;
import java.util.Iterator;
import android.graphics.Xfermode;
import android.graphics.PorterDuffXfermode;
import java.util.ArrayList;
import android.graphics.PorterDuff$Mode;
import com.airbnb.lottie.animation.LPaint;
import com.airbnb.lottie.animation.keyframe.MaskKeyframeAnimation;
import com.airbnb.lottie.animation.keyframe.TransformKeyframeAnimation;
import com.airbnb.lottie.animation.keyframe.FloatKeyframeAnimation;
import androidx.annotation.Nullable;
import android.graphics.Path;
import com.airbnb.lottie.LottieDrawable;
import java.util.List;
import android.graphics.RectF;
import android.graphics.Matrix;
import android.graphics.Paint;
import com.airbnb.lottie.model.KeyPathElement;
import com.airbnb.lottie.animation.keyframe.BaseKeyframeAnimation;
import com.airbnb.lottie.animation.content.DrawingContent;

public abstract class BaseLayer implements DrawingContent, AnimationListener, KeyPathElement
{
    private final Paint O8;
    final Matrix OO0o\u3007\u3007;
    private final RectF OO0o\u3007\u3007\u3007\u30070;
    private final Paint Oo08;
    private List<BaseLayer> OoO8;
    final LottieDrawable Oooo8o0\u3007;
    private final List<BaseKeyframeAnimation<?, ?>> o800o8O;
    private final RectF oO80;
    private boolean oo88o8O;
    private final Paint o\u30070;
    private final Path \u3007080;
    @Nullable
    private BaseLayer \u30070\u3007O0088o;
    private final RectF \u300780\u3007808\u3007O;
    private final RectF \u30078o8o\u3007;
    @Nullable
    private FloatKeyframeAnimation \u3007O00;
    final TransformKeyframeAnimation \u3007O888o0o;
    private final String \u3007O8o08O;
    @Nullable
    private MaskKeyframeAnimation \u3007O\u3007;
    private final Matrix \u3007o00\u3007\u3007Oo;
    private final Paint \u3007o\u3007;
    final Layer \u3007\u3007808\u3007;
    private final Paint \u3007\u3007888;
    @Nullable
    private BaseLayer \u3007\u30078O0\u30078;
    
    BaseLayer(final LottieDrawable oooo8o0\u3007, final Layer \u3007\u3007808\u3007) {
        this.\u3007080 = new Path();
        this.\u3007o00\u3007\u3007Oo = new Matrix();
        this.\u3007o\u3007 = new LPaint(1);
        this.O8 = new LPaint(1, PorterDuff$Mode.DST_IN);
        this.Oo08 = new LPaint(1, PorterDuff$Mode.DST_OUT);
        final LPaint o\u30070 = new LPaint(1);
        this.o\u30070 = o\u30070;
        this.\u3007\u3007888 = new LPaint(PorterDuff$Mode.CLEAR);
        this.oO80 = new RectF();
        this.\u300780\u3007808\u3007O = new RectF();
        this.OO0o\u3007\u3007\u3007\u30070 = new RectF();
        this.\u30078o8o\u3007 = new RectF();
        this.OO0o\u3007\u3007 = new Matrix();
        this.o800o8O = new ArrayList<BaseKeyframeAnimation<?, ?>>();
        this.oo88o8O = true;
        this.Oooo8o0\u3007 = oooo8o0\u3007;
        this.\u3007\u3007808\u3007 = \u3007\u3007808\u3007;
        final StringBuilder sb = new StringBuilder();
        sb.append(\u3007\u3007808\u3007.\u3007\u3007888());
        sb.append("#draw");
        this.\u3007O8o08O = sb.toString();
        if (\u3007\u3007808\u3007.o\u30070() == Layer.MatteType.INVERT) {
            o\u30070.setXfermode((Xfermode)new PorterDuffXfermode(PorterDuff$Mode.DST_OUT));
        }
        else {
            o\u30070.setXfermode((Xfermode)new PorterDuffXfermode(PorterDuff$Mode.DST_IN));
        }
        (this.\u3007O888o0o = \u3007\u3007808\u3007.o800o8O().\u3007o00\u3007\u3007Oo()).\u3007o00\u3007\u3007Oo(this);
        if (\u3007\u3007808\u3007.Oo08() != null && !\u3007\u3007808\u3007.Oo08().isEmpty()) {
            final MaskKeyframeAnimation \u3007o\u3007 = new MaskKeyframeAnimation(\u3007\u3007808\u3007.Oo08());
            this.\u3007O\u3007 = \u3007o\u3007;
            final Iterator<BaseKeyframeAnimation<ShapeData, Path>> iterator = \u3007o\u3007.\u3007080().iterator();
            while (iterator.hasNext()) {
                iterator.next().\u3007080((BaseKeyframeAnimation.AnimationListener)this);
            }
            for (final BaseKeyframeAnimation baseKeyframeAnimation : this.\u3007O\u3007.\u3007o\u3007()) {
                this.\u300780\u3007808\u3007O(baseKeyframeAnimation);
                baseKeyframeAnimation.\u3007080((BaseKeyframeAnimation.AnimationListener)this);
            }
        }
        this.\u300700\u30078();
    }
    
    private void O8ooOoo\u3007(final float n) {
        this.Oooo8o0\u3007.\u3007\u3007808\u3007().OO0o\u3007\u3007().\u3007080(this.\u3007\u3007808\u3007.\u3007\u3007888(), n);
    }
    
    private void O8\u3007o(final boolean oo88o8O) {
        if (oo88o8O != this.oo88o8O) {
            this.oo88o8O = oo88o8O;
            this.O\u30078O8\u3007008();
        }
    }
    
    private void OO0o\u3007\u3007(final Canvas canvas, final Matrix matrix, final Mask mask, final BaseKeyframeAnimation<ShapeData, Path> baseKeyframeAnimation, final BaseKeyframeAnimation<Integer, Integer> baseKeyframeAnimation2) {
        Utils.OO0o\u3007\u3007(canvas, this.oO80, this.O8);
        canvas.drawRect(this.oO80, this.\u3007o\u3007);
        this.Oo08.setAlpha((int)(baseKeyframeAnimation2.oO80() * 2.55f));
        this.\u3007080.set((Path)baseKeyframeAnimation.oO80());
        this.\u3007080.transform(matrix);
        canvas.drawPath(this.\u3007080, this.Oo08);
        canvas.restore();
    }
    
    private void OO0o\u3007\u3007\u3007\u30070(final Canvas canvas, final Matrix matrix, final Mask mask, final BaseKeyframeAnimation<ShapeData, Path> baseKeyframeAnimation, final BaseKeyframeAnimation<Integer, Integer> baseKeyframeAnimation2) {
        this.\u3007080.set((Path)baseKeyframeAnimation.oO80());
        this.\u3007080.transform(matrix);
        this.\u3007o\u3007.setAlpha((int)(baseKeyframeAnimation2.oO80() * 2.55f));
        canvas.drawPath(this.\u3007080, this.\u3007o\u3007);
    }
    
    private void Oooo8o0\u3007(final Canvas canvas, final Matrix matrix, final Mask mask, final BaseKeyframeAnimation<ShapeData, Path> baseKeyframeAnimation, final BaseKeyframeAnimation<Integer, Integer> baseKeyframeAnimation2) {
        Utils.OO0o\u3007\u3007(canvas, this.oO80, this.Oo08);
        canvas.drawRect(this.oO80, this.\u3007o\u3007);
        this.Oo08.setAlpha((int)(baseKeyframeAnimation2.oO80() * 2.55f));
        this.\u3007080.set((Path)baseKeyframeAnimation.oO80());
        this.\u3007080.transform(matrix);
        canvas.drawPath(this.\u3007080, this.Oo08);
        canvas.restore();
    }
    
    private void O\u30078O8\u3007008() {
        this.Oooo8o0\u3007.invalidateSelf();
    }
    
    @Nullable
    static BaseLayer o800o8O(final Layer layer, final LottieDrawable lottieDrawable, final LottieComposition lottieComposition) {
        switch (BaseLayer$2.\u3007080[layer.O8().ordinal()]) {
            default: {
                final StringBuilder sb = new StringBuilder();
                sb.append("Unknown layer type ");
                sb.append(layer.O8());
                Logger.\u3007o\u3007(sb.toString());
                return null;
            }
            case 6: {
                return new TextLayer(lottieDrawable, layer);
            }
            case 5: {
                return new NullLayer(lottieDrawable, layer);
            }
            case 4: {
                return new ImageLayer(lottieDrawable, layer);
            }
            case 3: {
                return new SolidLayer(lottieDrawable, layer);
            }
            case 2: {
                return new CompositionLayer(lottieDrawable, layer, lottieComposition.Oooo8o0\u3007(layer.\u30078o8o\u3007()), lottieComposition);
            }
            case 1: {
                return new ShapeLayer(lottieDrawable, layer);
            }
        }
    }
    
    private void o\u3007O8\u3007\u3007o(final RectF rectF, final Matrix matrix) {
        this.\u300780\u3007808\u3007O.set(0.0f, 0.0f, 0.0f, 0.0f);
        if (!this.oo88o8O()) {
            return;
        }
        for (int size = this.\u3007O\u3007.\u3007o00\u3007\u3007Oo().size(), i = 0; i < size; ++i) {
            final Mask mask = this.\u3007O\u3007.\u3007o00\u3007\u3007Oo().get(i);
            this.\u3007080.set((Path)this.\u3007O\u3007.\u3007080().get(i).oO80());
            this.\u3007080.transform(matrix);
            final int n = BaseLayer$2.\u3007o00\u3007\u3007Oo[mask.\u3007080().ordinal()];
            if (n == 1 || n == 2) {
                return;
            }
            if (n == 3 || n == 4) {
                if (mask.O8()) {
                    return;
                }
            }
            this.\u3007080.computeBounds(this.\u30078o8o\u3007, false);
            if (i == 0) {
                this.\u300780\u3007808\u3007O.set(this.\u30078o8o\u3007);
            }
            else {
                final RectF \u300780\u3007808\u3007O = this.\u300780\u3007808\u3007O;
                \u300780\u3007808\u3007O.set(Math.min(\u300780\u3007808\u3007O.left, this.\u30078o8o\u3007.left), Math.min(this.\u300780\u3007808\u3007O.top, this.\u30078o8o\u3007.top), Math.max(this.\u300780\u3007808\u3007O.right, this.\u30078o8o\u3007.right), Math.max(this.\u300780\u3007808\u3007O.bottom, this.\u30078o8o\u3007.bottom));
            }
        }
        if (!rectF.intersect(this.\u300780\u3007808\u3007O)) {
            rectF.set(0.0f, 0.0f, 0.0f, 0.0f);
        }
    }
    
    private void \u300700(final RectF rectF, final Matrix matrix) {
        if (!this.\u3007oo\u3007()) {
            return;
        }
        if (this.\u3007\u3007808\u3007.o\u30070() == Layer.MatteType.INVERT) {
            return;
        }
        this.OO0o\u3007\u3007\u3007\u30070.set(0.0f, 0.0f, 0.0f, 0.0f);
        this.\u3007\u30078O0\u30078.\u3007080(this.OO0o\u3007\u3007\u3007\u30070, matrix, true);
        if (!rectF.intersect(this.OO0o\u3007\u3007\u3007\u30070)) {
            rectF.set(0.0f, 0.0f, 0.0f, 0.0f);
        }
    }
    
    private void \u300700\u30078() {
        final boolean empty = this.\u3007\u3007808\u3007.\u3007o\u3007().isEmpty();
        boolean b = true;
        if (!empty) {
            (this.\u3007O00 = new FloatKeyframeAnimation(this.\u3007\u3007808\u3007.\u3007o\u3007())).\u30078o8o\u3007();
            this.\u3007O00.\u3007080((BaseKeyframeAnimation.AnimationListener)new AnimationListener(this) {
                final BaseLayer \u3007080;
                
                @Override
                public void O8() {
                    final BaseLayer \u3007080 = this.\u3007080;
                    \u3007080.O8\u3007o(\u3007080.\u3007O00.\u3007\u3007808\u3007() == 1.0f);
                }
            });
            if (((BaseKeyframeAnimation<K, Float>)this.\u3007O00).oO80() != 1.0f) {
                b = false;
            }
            this.O8\u3007o(b);
            this.\u300780\u3007808\u3007O(this.\u3007O00);
        }
        else {
            this.O8\u3007o(true);
        }
    }
    
    private void \u30070\u3007O0088o(final Canvas canvas) {
        L.\u3007080("Layer#clearLayer");
        final RectF oo80 = this.oO80;
        canvas.drawRect(oo80.left - 1.0f, oo80.top - 1.0f, oo80.right + 1.0f, oo80.bottom + 1.0f, this.\u3007\u3007888);
        L.\u3007o00\u3007\u3007Oo("Layer#clearLayer");
    }
    
    private void \u30078o8o\u3007(final Canvas canvas, final Matrix matrix, final Mask mask, final BaseKeyframeAnimation<ShapeData, Path> baseKeyframeAnimation, final BaseKeyframeAnimation<Integer, Integer> baseKeyframeAnimation2) {
        Utils.OO0o\u3007\u3007(canvas, this.oO80, this.O8);
        this.\u3007080.set((Path)baseKeyframeAnimation.oO80());
        this.\u3007080.transform(matrix);
        this.\u3007o\u3007.setAlpha((int)(baseKeyframeAnimation2.oO80() * 2.55f));
        canvas.drawPath(this.\u3007080, this.\u3007o\u3007);
        canvas.restore();
    }
    
    private boolean \u3007O00() {
        if (this.\u3007O\u3007.\u3007080().isEmpty()) {
            return false;
        }
        for (int i = 0; i < this.\u3007O\u3007.\u3007o00\u3007\u3007Oo().size(); ++i) {
            if (this.\u3007O\u3007.\u3007o00\u3007\u3007Oo().get(i).\u3007080() != Mask.MaskMode.MASK_MODE_NONE) {
                return false;
            }
        }
        return true;
    }
    
    private void \u3007O8o08O(final Canvas canvas, final Matrix matrix, final Mask mask, final BaseKeyframeAnimation<ShapeData, Path> baseKeyframeAnimation, final BaseKeyframeAnimation<Integer, Integer> baseKeyframeAnimation2) {
        Utils.OO0o\u3007\u3007(canvas, this.oO80, this.\u3007o\u3007);
        canvas.drawRect(this.oO80, this.\u3007o\u3007);
        this.\u3007080.set((Path)baseKeyframeAnimation.oO80());
        this.\u3007080.transform(matrix);
        this.\u3007o\u3007.setAlpha((int)(baseKeyframeAnimation2.oO80() * 2.55f));
        canvas.drawPath(this.\u3007080, this.Oo08);
        canvas.restore();
    }
    
    private void \u3007O\u3007(final Canvas canvas, final Matrix matrix, final Mask mask, final BaseKeyframeAnimation<ShapeData, Path> baseKeyframeAnimation, final BaseKeyframeAnimation<Integer, Integer> baseKeyframeAnimation2) {
        this.\u3007080.set((Path)baseKeyframeAnimation.oO80());
        this.\u3007080.transform(matrix);
        canvas.drawPath(this.\u3007080, this.Oo08);
    }
    
    private void \u3007\u3007808\u3007(final Canvas canvas, final Matrix matrix) {
        L.\u3007080("Layer#saveLayer");
        Utils.Oooo8o0\u3007(canvas, this.oO80, this.O8, 19);
        if (Build$VERSION.SDK_INT < 28) {
            this.\u30070\u3007O0088o(canvas);
        }
        L.\u3007o00\u3007\u3007Oo("Layer#saveLayer");
        for (int i = 0; i < this.\u3007O\u3007.\u3007o00\u3007\u3007Oo().size(); ++i) {
            final Mask mask = this.\u3007O\u3007.\u3007o00\u3007\u3007Oo().get(i);
            final BaseKeyframeAnimation baseKeyframeAnimation = this.\u3007O\u3007.\u3007080().get(i);
            final BaseKeyframeAnimation baseKeyframeAnimation2 = this.\u3007O\u3007.\u3007o\u3007().get(i);
            final int n = BaseLayer$2.\u3007o00\u3007\u3007Oo[mask.\u3007080().ordinal()];
            if (n != 1) {
                if (n != 2) {
                    if (n != 3) {
                        if (n == 4) {
                            if (mask.O8()) {
                                this.\u3007O8o08O(canvas, matrix, mask, baseKeyframeAnimation, baseKeyframeAnimation2);
                            }
                            else {
                                this.OO0o\u3007\u3007\u3007\u30070(canvas, matrix, mask, baseKeyframeAnimation, baseKeyframeAnimation2);
                            }
                        }
                    }
                    else if (mask.O8()) {
                        this.OO0o\u3007\u3007(canvas, matrix, mask, baseKeyframeAnimation, baseKeyframeAnimation2);
                    }
                    else {
                        this.\u30078o8o\u3007(canvas, matrix, mask, baseKeyframeAnimation, baseKeyframeAnimation2);
                    }
                }
                else {
                    if (i == 0) {
                        this.\u3007o\u3007.setColor(-16777216);
                        this.\u3007o\u3007.setAlpha(255);
                        canvas.drawRect(this.oO80, this.\u3007o\u3007);
                    }
                    if (mask.O8()) {
                        this.Oooo8o0\u3007(canvas, matrix, mask, baseKeyframeAnimation, baseKeyframeAnimation2);
                    }
                    else {
                        this.\u3007O\u3007(canvas, matrix, mask, baseKeyframeAnimation, baseKeyframeAnimation2);
                    }
                }
            }
            else if (this.\u3007O00()) {
                this.\u3007o\u3007.setAlpha(255);
                canvas.drawRect(this.oO80, this.\u3007o\u3007);
            }
        }
        L.\u3007080("Layer#restoreLayer");
        canvas.restore();
        L.\u3007o00\u3007\u3007Oo("Layer#restoreLayer");
    }
    
    private void \u3007\u30078O0\u30078() {
        if (this.OoO8 != null) {
            return;
        }
        if (this.\u30070\u3007O0088o == null) {
            this.OoO8 = Collections.emptyList();
            return;
        }
        this.OoO8 = new ArrayList<BaseLayer>();
        for (BaseLayer baseLayer = this.\u30070\u3007O0088o; baseLayer != null; baseLayer = baseLayer.\u30070\u3007O0088o) {
            this.OoO8.add(baseLayer);
        }
    }
    
    @Override
    public void O8() {
        this.O\u30078O8\u3007008();
    }
    
    void OOO\u3007O0(@Nullable final BaseLayer \u30070\u3007O0088o) {
        this.\u30070\u3007O0088o = \u30070\u3007O0088o;
    }
    
    @Override
    public void Oo08(final List<Content> list, final List<Content> list2) {
    }
    
    abstract void OoO8(final Canvas p0, final Matrix p1, final int p2);
    
    @Override
    public String getName() {
        return this.\u3007\u3007808\u3007.\u3007\u3007888();
    }
    
    boolean oo88o8O() {
        final MaskKeyframeAnimation \u3007o\u3007 = this.\u3007O\u3007;
        return \u3007o\u3007 != null && !\u3007o\u3007.\u3007080().isEmpty();
    }
    
    void oo\u3007(@FloatRange(from = 0.0, to = 1.0) float ooO8) {
        this.\u3007O888o0o.OO0o\u3007\u3007\u3007\u30070(ooO8);
        final MaskKeyframeAnimation \u3007o\u3007 = this.\u3007O\u3007;
        final int n = 0;
        if (\u3007o\u3007 != null) {
            for (int i = 0; i < this.\u3007O\u3007.\u3007080().size(); ++i) {
                this.\u3007O\u3007.\u3007080().get(i).\u3007O8o08O(ooO8);
            }
        }
        float n2 = ooO8;
        if (this.\u3007\u3007808\u3007.OoO8() != 0.0f) {
            n2 = ooO8 / this.\u3007\u3007808\u3007.OoO8();
        }
        final FloatKeyframeAnimation \u3007o00 = this.\u3007O00;
        if (\u3007o00 != null) {
            \u3007o00.\u3007O8o08O(n2 / this.\u3007\u3007808\u3007.OoO8());
        }
        final BaseLayer \u3007\u30078O0\u30078 = this.\u3007\u30078O0\u30078;
        int j = n;
        if (\u3007\u30078O0\u30078 != null) {
            ooO8 = \u3007\u30078O0\u30078.\u3007\u3007808\u3007.OoO8();
            this.\u3007\u30078O0\u30078.oo\u3007(ooO8 * n2);
            j = n;
        }
        while (j < this.o800o8O.size()) {
            this.o800o8O.get(j).\u3007O8o08O(n2);
            ++j;
        }
    }
    
    @CallSuper
    @Override
    public <T> void o\u30070(final T t, @Nullable final LottieValueCallback<T> lottieValueCallback) {
        this.\u3007O888o0o.\u3007o\u3007(t, lottieValueCallback);
    }
    
    void o\u3007\u30070\u3007(@Nullable final BaseLayer \u3007\u30078O0\u30078) {
        this.\u3007\u30078O0\u30078 = \u3007\u30078O0\u30078;
    }
    
    void \u30070000OOO(final KeyPath keyPath, final int n, final List<KeyPath> list, final KeyPath keyPath2) {
    }
    
    @CallSuper
    @Override
    public void \u3007080(final RectF rectF, final Matrix matrix, final boolean b) {
        this.oO80.set(0.0f, 0.0f, 0.0f, 0.0f);
        this.\u3007\u30078O0\u30078();
        this.OO0o\u3007\u3007.set(matrix);
        if (b) {
            final List<BaseLayer> ooO8 = this.OoO8;
            if (ooO8 != null) {
                for (int i = ooO8.size() - 1; i >= 0; --i) {
                    this.OO0o\u3007\u3007.preConcat(this.OoO8.get(i).\u3007O888o0o.o\u30070());
                }
            }
            else {
                final BaseLayer \u30070\u3007O0088o = this.\u30070\u3007O0088o;
                if (\u30070\u3007O0088o != null) {
                    this.OO0o\u3007\u3007.preConcat(\u30070\u3007O0088o.\u3007O888o0o.o\u30070());
                }
            }
        }
        this.OO0o\u3007\u3007.preConcat(this.\u3007O888o0o.o\u30070());
    }
    
    public void \u300780\u3007808\u3007O(@Nullable final BaseKeyframeAnimation<?, ?> baseKeyframeAnimation) {
        if (baseKeyframeAnimation == null) {
            return;
        }
        this.o800o8O.add(baseKeyframeAnimation);
    }
    
    Layer \u3007O888o0o() {
        return this.\u3007\u3007808\u3007;
    }
    
    public void \u3007oOO8O8(final BaseKeyframeAnimation<?, ?> baseKeyframeAnimation) {
        this.o800o8O.remove(baseKeyframeAnimation);
    }
    
    boolean \u3007oo\u3007() {
        return this.\u3007\u30078O0\u30078 != null;
    }
    
    @Override
    public void \u3007o\u3007(final Canvas canvas, final Matrix matrix, int n) {
        L.\u3007080(this.\u3007O8o08O);
        if (!this.oo88o8O || this.\u3007\u3007808\u3007.\u3007O888o0o()) {
            L.\u3007o00\u3007\u3007Oo(this.\u3007O8o08O);
            return;
        }
        this.\u3007\u30078O0\u30078();
        L.\u3007080("Layer#parentMatrix");
        this.\u3007o00\u3007\u3007Oo.reset();
        this.\u3007o00\u3007\u3007Oo.set(matrix);
        for (int i = this.OoO8.size() - 1; i >= 0; --i) {
            this.\u3007o00\u3007\u3007Oo.preConcat(this.OoO8.get(i).\u3007O888o0o.o\u30070());
        }
        L.\u3007o00\u3007\u3007Oo("Layer#parentMatrix");
        int intValue;
        if (this.\u3007O888o0o.oO80() == null) {
            intValue = 100;
        }
        else {
            intValue = this.\u3007O888o0o.oO80().oO80();
        }
        n = (int)(n / 255.0f * intValue / 100.0f * 255.0f);
        if (!this.\u3007oo\u3007() && !this.oo88o8O()) {
            this.\u3007o00\u3007\u3007Oo.preConcat(this.\u3007O888o0o.o\u30070());
            L.\u3007080("Layer#drawLayer");
            this.OoO8(canvas, this.\u3007o00\u3007\u3007Oo, n);
            L.\u3007o00\u3007\u3007Oo("Layer#drawLayer");
            this.O8ooOoo\u3007(L.\u3007o00\u3007\u3007Oo(this.\u3007O8o08O));
            return;
        }
        L.\u3007080("Layer#computeBounds");
        this.\u3007080(this.oO80, this.\u3007o00\u3007\u3007Oo, false);
        this.\u300700(this.oO80, matrix);
        this.\u3007o00\u3007\u3007Oo.preConcat(this.\u3007O888o0o.o\u30070());
        this.o\u3007O8\u3007\u3007o(this.oO80, this.\u3007o00\u3007\u3007Oo);
        if (!this.oO80.intersect(0.0f, 0.0f, (float)canvas.getWidth(), (float)canvas.getHeight())) {
            this.oO80.set(0.0f, 0.0f, 0.0f, 0.0f);
        }
        L.\u3007o00\u3007\u3007Oo("Layer#computeBounds");
        if (!this.oO80.isEmpty()) {
            L.\u3007080("Layer#saveLayer");
            this.\u3007o\u3007.setAlpha(255);
            Utils.OO0o\u3007\u3007(canvas, this.oO80, this.\u3007o\u3007);
            L.\u3007o00\u3007\u3007Oo("Layer#saveLayer");
            this.\u30070\u3007O0088o(canvas);
            L.\u3007080("Layer#drawLayer");
            this.OoO8(canvas, this.\u3007o00\u3007\u3007Oo, n);
            L.\u3007o00\u3007\u3007Oo("Layer#drawLayer");
            if (this.oo88o8O()) {
                this.\u3007\u3007808\u3007(canvas, this.\u3007o00\u3007\u3007Oo);
            }
            if (this.\u3007oo\u3007()) {
                L.\u3007080("Layer#drawMatte");
                L.\u3007080("Layer#saveLayer");
                Utils.Oooo8o0\u3007(canvas, this.oO80, this.o\u30070, 19);
                L.\u3007o00\u3007\u3007Oo("Layer#saveLayer");
                this.\u30070\u3007O0088o(canvas);
                this.\u3007\u30078O0\u30078.\u3007o\u3007(canvas, matrix, n);
                L.\u3007080("Layer#restoreLayer");
                canvas.restore();
                L.\u3007o00\u3007\u3007Oo("Layer#restoreLayer");
                L.\u3007o00\u3007\u3007Oo("Layer#drawMatte");
            }
            L.\u3007080("Layer#restoreLayer");
            canvas.restore();
            L.\u3007o00\u3007\u3007Oo("Layer#restoreLayer");
        }
        this.O8ooOoo\u3007(L.\u3007o00\u3007\u3007Oo(this.\u3007O8o08O));
    }
    
    @Override
    public void \u3007\u3007888(final KeyPath keyPath, final int n, final List<KeyPath> list, KeyPath keyPath2) {
        if (!keyPath.\u3007\u3007888(this.getName(), n)) {
            return;
        }
        KeyPath \u3007080 = keyPath2;
        if (!"__container".equals(this.getName())) {
            keyPath2 = (\u3007080 = keyPath2.\u3007080(this.getName()));
            if (keyPath.\u3007o\u3007(this.getName(), n)) {
                list.add(keyPath2.\u300780\u3007808\u3007O(this));
                \u3007080 = keyPath2;
            }
        }
        if (keyPath.oO80(this.getName(), n)) {
            this.\u30070000OOO(keyPath, n + keyPath.Oo08(this.getName(), n), list, \u3007080);
        }
    }
}

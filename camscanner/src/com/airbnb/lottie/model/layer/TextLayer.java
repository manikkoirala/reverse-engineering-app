// 
// Decompiled by Procyon v0.6.0
// 

package com.airbnb.lottie.model.layer;

import com.airbnb.lottie.animation.keyframe.ValueCallbackKeyframeAnimation;
import com.airbnb.lottie.LottieProperty;
import com.airbnb.lottie.value.LottieValueCallback;
import java.util.Arrays;
import com.airbnb.lottie.model.content.ShapeGroup;
import java.util.ArrayList;
import android.graphics.Typeface;
import android.graphics.Path;
import android.graphics.Canvas;
import com.airbnb.lottie.model.DocumentData;
import com.airbnb.lottie.utils.Utils;
import com.airbnb.lottie.model.Font;
import com.airbnb.lottie.model.animatable.AnimatableFloatValue;
import com.airbnb.lottie.model.animatable.AnimatableColorValue;
import com.airbnb.lottie.model.animatable.AnimatableTextProperties;
import java.util.HashMap;
import android.graphics.Paint$Style;
import com.airbnb.lottie.animation.content.ContentGroup;
import java.util.List;
import com.airbnb.lottie.model.FontCharacter;
import java.util.Map;
import androidx.collection.LongSparseArray;
import android.graphics.Matrix;
import com.airbnb.lottie.animation.keyframe.TextKeyframeAnimation;
import android.graphics.RectF;
import com.airbnb.lottie.LottieComposition;
import com.airbnb.lottie.LottieDrawable;
import androidx.annotation.Nullable;
import com.airbnb.lottie.animation.keyframe.BaseKeyframeAnimation;
import android.graphics.Paint;

public class TextLayer extends BaseLayer
{
    private final Paint O8ooOoo\u3007;
    @Nullable
    private BaseKeyframeAnimation<Integer, Integer> O8\u3007o;
    private final LottieDrawable OOO\u3007O0;
    @Nullable
    private BaseKeyframeAnimation<Float, Float> Oo8Oo00oo;
    private final Paint O\u30078O8\u3007008;
    @Nullable
    private BaseKeyframeAnimation<Integer, Integer> o0ooO;
    @Nullable
    private BaseKeyframeAnimation<Float, Float> o8;
    private final LottieComposition oo\u3007;
    @Nullable
    private BaseKeyframeAnimation<Float, Float> o\u30070OOo\u30070;
    @Nullable
    private BaseKeyframeAnimation<Float, Float> o\u30078;
    private final RectF o\u3007O8\u3007\u3007o;
    private final TextKeyframeAnimation o\u3007\u30070\u3007;
    private final Matrix \u300700;
    private final LongSparseArray<String> \u30070000OOO;
    @Nullable
    private BaseKeyframeAnimation<Integer, Integer> \u300700\u30078;
    @Nullable
    private BaseKeyframeAnimation<Integer, Integer> \u3007o;
    private final Map<FontCharacter, List<ContentGroup>> \u3007oOO8O8;
    private final StringBuilder \u3007oo\u3007;
    @Nullable
    private BaseKeyframeAnimation<Float, Float> \u3007\u30070o;
    @Nullable
    private BaseKeyframeAnimation<Float, Float> \u3007\u3007\u30070\u3007\u30070;
    
    TextLayer(final LottieDrawable ooo\u3007O0, final Layer layer) {
        super(ooo\u3007O0, layer);
        this.\u3007oo\u3007 = new StringBuilder(2);
        this.o\u3007O8\u3007\u3007o = new RectF();
        this.\u300700 = new Matrix();
        this.O\u30078O8\u3007008 = new Paint(1) {
            final TextLayer \u3007080;
            
            {
                this.setStyle(Paint$Style.FILL);
            }
        };
        this.O8ooOoo\u3007 = new Paint(1) {
            final TextLayer \u3007080;
            
            {
                this.setStyle(Paint$Style.STROKE);
            }
        };
        this.\u3007oOO8O8 = new HashMap<FontCharacter, List<ContentGroup>>();
        this.\u30070000OOO = new LongSparseArray<String>();
        this.OOO\u3007O0 = ooo\u3007O0;
        this.oo\u3007 = layer.\u3007080();
        final TextKeyframeAnimation o8 = layer.\u3007O00().O8();
        (this.o\u3007\u30070\u3007 = o8).\u3007080((BaseKeyframeAnimation.AnimationListener)this);
        this.\u300780\u3007808\u3007O(o8);
        final AnimatableTextProperties \u3007\u30078O0\u30078 = layer.\u3007\u30078O0\u30078();
        if (\u3007\u30078O0\u30078 != null) {
            final AnimatableColorValue \u3007080 = \u3007\u30078O0\u30078.\u3007080;
            if (\u3007080 != null) {
                (this.O8\u3007o = \u3007080.\u3007080()).\u3007080((BaseKeyframeAnimation.AnimationListener)this);
                this.\u300780\u3007808\u3007O(this.O8\u3007o);
            }
        }
        if (\u3007\u30078O0\u30078 != null) {
            final AnimatableColorValue \u3007o00\u3007\u3007Oo = \u3007\u30078O0\u30078.\u3007o00\u3007\u3007Oo;
            if (\u3007o00\u3007\u3007Oo != null) {
                (this.\u3007o = \u3007o00\u3007\u3007Oo.\u3007080()).\u3007080((BaseKeyframeAnimation.AnimationListener)this);
                this.\u300780\u3007808\u3007O(this.\u3007o);
            }
        }
        if (\u3007\u30078O0\u30078 != null) {
            final AnimatableFloatValue \u3007o\u3007 = \u3007\u30078O0\u30078.\u3007o\u3007;
            if (\u3007o\u3007 != null) {
                (this.o\u30078 = \u3007o\u3007.\u3007080()).\u3007080((BaseKeyframeAnimation.AnimationListener)this);
                this.\u300780\u3007808\u3007O(this.o\u30078);
            }
        }
        if (\u3007\u30078O0\u30078 != null) {
            final AnimatableFloatValue o9 = \u3007\u30078O0\u30078.O8;
            if (o9 != null) {
                (this.Oo8Oo00oo = o9.\u3007080()).\u3007080((BaseKeyframeAnimation.AnimationListener)this);
                this.\u300780\u3007808\u3007O(this.Oo8Oo00oo);
            }
        }
    }
    
    private float O08000(final String s, final Font font, final float n, final float n2) {
        float n3 = 0.0f;
        for (int i = 0; i < s.length(); ++i) {
            final FontCharacter fontCharacter = this.oo\u3007.\u3007o\u3007().get(FontCharacter.\u3007o\u3007(s.charAt(i), font.\u3007080(), font.\u3007o\u3007()));
            if (fontCharacter != null) {
                n3 += (float)(fontCharacter.\u3007o00\u3007\u3007Oo() * n * Utils.Oo08() * n2);
            }
        }
        return n3;
    }
    
    private void Oo8Oo00oo(final String s, final DocumentData documentData, final Canvas canvas) {
        if (documentData.\u30078o8o\u3007) {
            this.o\u30078(s, this.O\u30078O8\u3007008, canvas);
            this.o\u30078(s, this.O8ooOoo\u3007, canvas);
        }
        else {
            this.o\u30078(s, this.O8ooOoo\u3007, canvas);
            this.o\u30078(s, this.O\u30078O8\u3007008, canvas);
        }
    }
    
    private boolean O\u3007O\u3007oO(final int codePoint) {
        return Character.getType(codePoint) == 16 || Character.getType(codePoint) == 27 || Character.getType(codePoint) == 6 || Character.getType(codePoint) == 28 || Character.getType(codePoint) == 19;
    }
    
    private String o0ooO(String string, int i) {
        int codePoint;
        int j;
        int codePoint2;
        for (codePoint = string.codePointAt(i), j = Character.charCount(codePoint) + i; j < string.length(); j += Character.charCount(codePoint2), codePoint = codePoint * 31 + codePoint2) {
            codePoint2 = string.codePointAt(j);
            if (!this.O\u3007O\u3007oO(codePoint2)) {
                break;
            }
        }
        final LongSparseArray<String> \u30070000OOO = this.\u30070000OOO;
        final long n = codePoint;
        if (\u30070000OOO.containsKey(n)) {
            return this.\u30070000OOO.get(n);
        }
        this.\u3007oo\u3007.setLength(0);
        while (i < j) {
            final int codePoint3 = string.codePointAt(i);
            this.\u3007oo\u3007.appendCodePoint(codePoint3);
            i += Character.charCount(codePoint3);
        }
        string = this.\u3007oo\u3007.toString();
        this.\u30070000OOO.put(n, string);
        return string;
    }
    
    private void o8(final FontCharacter fontCharacter, final Matrix matrix, final float n, final DocumentData documentData, final Canvas canvas) {
        final List<ContentGroup> \u30078 = this.\u30078(fontCharacter);
        for (int i = 0; i < \u30078.size(); ++i) {
            final Path path = \u30078.get(i).getPath();
            path.computeBounds(this.o\u3007O8\u3007\u3007o, false);
            this.\u300700.set(matrix);
            this.\u300700.preTranslate(0.0f, -documentData.\u3007\u3007888 * Utils.Oo08());
            this.\u300700.preScale(n, n);
            path.transform(this.\u300700);
            if (documentData.\u30078o8o\u3007) {
                this.o\u30070OOo\u30070(path, this.O\u30078O8\u3007008, canvas);
                this.o\u30070OOo\u30070(path, this.O8ooOoo\u3007, canvas);
            }
            else {
                this.o\u30070OOo\u30070(path, this.O8ooOoo\u3007, canvas);
                this.o\u30070OOo\u30070(path, this.O\u30078O8\u3007008, canvas);
            }
        }
    }
    
    private void oO(final DocumentData documentData, final Font font, final Matrix matrix, final Canvas canvas) {
        final float \u3007\u3007888 = Utils.\u3007\u3007888(matrix);
        final Typeface o\u3007\u30070\u3007 = this.OOO\u3007O0.o\u3007\u30070\u3007(font.\u3007080(), font.\u3007o\u3007());
        if (o\u3007\u30070\u3007 == null) {
            return;
        }
        final String \u3007080 = documentData.\u3007080;
        this.OOO\u3007O0.\u30070000OOO();
        this.O\u30078O8\u3007008.setTypeface(o\u3007\u30070\u3007);
        final BaseKeyframeAnimation<Float, Float> \u3007\u30070o = this.\u3007\u30070o;
        float n;
        if (\u3007\u30070o != null) {
            n = \u3007\u30070o.oO80();
        }
        else {
            final BaseKeyframeAnimation<Float, Float> o\u30070OOo\u30070 = this.o\u30070OOo\u30070;
            if (o\u30070OOo\u30070 != null) {
                n = o\u30070OOo\u30070.oO80();
            }
            else {
                n = documentData.\u3007o\u3007;
            }
        }
        this.O\u30078O8\u3007008.setTextSize(n * Utils.Oo08());
        this.O8ooOoo\u3007.setTypeface(this.O\u30078O8\u3007008.getTypeface());
        this.O8ooOoo\u3007.setTextSize(this.O\u30078O8\u3007008.getTextSize());
        final float n2 = documentData.o\u30070 * Utils.Oo08();
        final List<String> \u30078\u30070\u3007o\u3007O = this.\u30078\u30070\u3007o\u3007O(\u3007080);
        for (int size = \u30078\u30070\u3007o\u3007O.size(), i = 0; i < size; ++i) {
            final String s = \u30078\u30070\u3007o\u3007O.get(i);
            this.\u3007o(documentData.O8, canvas, this.O8ooOoo\u3007.measureText(s));
            canvas.translate(0.0f, i * n2 - (size - 1) * n2 / 2.0f);
            this.\u3007\u3007\u30070\u3007\u30070(s, documentData, canvas, \u3007\u3007888);
            canvas.setMatrix(matrix);
        }
    }
    
    private void o\u30070OOo\u30070(final Path path, final Paint paint, final Canvas canvas) {
        if (paint.getColor() == 0) {
            return;
        }
        if (paint.getStyle() == Paint$Style.STROKE && paint.getStrokeWidth() == 0.0f) {
            return;
        }
        canvas.drawPath(path, paint);
    }
    
    private void o\u30078(final String s, final Paint paint, final Canvas canvas) {
        if (paint.getColor() == 0) {
            return;
        }
        if (paint.getStyle() == Paint$Style.STROKE && paint.getStrokeWidth() == 0.0f) {
            return;
        }
        canvas.drawText(s, 0, s.length(), 0.0f, 0.0f, paint);
    }
    
    private void \u300708O8o\u30070(final DocumentData documentData, final Matrix matrix, final Font font, final Canvas canvas) {
        final BaseKeyframeAnimation<Float, Float> \u3007\u30070o = this.\u3007\u30070o;
        float n;
        if (\u3007\u30070o != null) {
            n = \u3007\u30070o.oO80();
        }
        else {
            final BaseKeyframeAnimation<Float, Float> o\u30070OOo\u30070 = this.o\u30070OOo\u30070;
            if (o\u30070OOo\u30070 != null) {
                n = o\u30070OOo\u30070.oO80();
            }
            else {
                n = documentData.\u3007o\u3007;
            }
        }
        final float n2 = n / 100.0f;
        final float \u3007\u3007888 = Utils.\u3007\u3007888(matrix);
        final String \u3007080 = documentData.\u3007080;
        final float n3 = documentData.o\u30070 * Utils.Oo08();
        final List<String> \u30078\u30070\u3007o\u3007O = this.\u30078\u30070\u3007o\u3007O(\u3007080);
        for (int size = \u30078\u30070\u3007o\u3007O.size(), i = 0; i < size; ++i) {
            final String s = \u30078\u30070\u3007o\u3007O.get(i);
            final float o08000 = this.O08000(s, font, n2, \u3007\u3007888);
            canvas.save();
            this.\u3007o(documentData.O8, canvas, o08000);
            canvas.translate(0.0f, i * n3 - (size - 1) * n3 / 2.0f);
            this.\u3007\u30070o(s, documentData, matrix, font, canvas, \u3007\u3007888, n2);
            canvas.restore();
        }
    }
    
    private List<ContentGroup> \u30078(final FontCharacter fontCharacter) {
        if (this.\u3007oOO8O8.containsKey(fontCharacter)) {
            return this.\u3007oOO8O8.get(fontCharacter);
        }
        final List<ShapeGroup> \u3007080 = fontCharacter.\u3007080();
        final int size = \u3007080.size();
        final ArrayList list = new ArrayList(size);
        for (int i = 0; i < size; ++i) {
            list.add((Object)new ContentGroup(this.OOO\u3007O0, this, (ShapeGroup)\u3007080.get(i)));
        }
        this.\u3007oOO8O8.put(fontCharacter, (ArrayList)list);
        return (List<ContentGroup>)list;
    }
    
    private List<String> \u30078\u30070\u3007o\u3007O(final String s) {
        return Arrays.asList(s.replaceAll("\r\n", "\r").replaceAll("\n", "\r").split("\r"));
    }
    
    private void \u3007o(final DocumentData.Justification justification, final Canvas canvas, final float n) {
        final int n2 = TextLayer$3.\u3007080[justification.ordinal()];
        if (n2 != 2) {
            if (n2 == 3) {
                canvas.translate(-n / 2.0f, 0.0f);
            }
        }
        else {
            canvas.translate(-n, 0.0f);
        }
    }
    
    private void \u3007\u30070o(final String s, final DocumentData documentData, final Matrix matrix, final Font font, final Canvas canvas, final float n, final float n2) {
        for (int i = 0; i < s.length(); ++i) {
            final FontCharacter fontCharacter = this.oo\u3007.\u3007o\u3007().get(FontCharacter.\u3007o\u3007(s.charAt(i), font.\u3007080(), font.\u3007o\u3007()));
            if (fontCharacter != null) {
                this.o8(fontCharacter, matrix, n2, documentData, canvas);
                final float n3 = (float)fontCharacter.\u3007o00\u3007\u3007Oo();
                final float oo08 = Utils.Oo08();
                final float n4 = documentData.Oo08 / 10.0f;
                final BaseKeyframeAnimation<Float, Float> \u3007\u3007\u30070\u3007\u30070 = this.\u3007\u3007\u30070\u3007\u30070;
                float n6 = 0.0f;
                Label_0159: {
                    float n5;
                    if (\u3007\u3007\u30070\u3007\u30070 != null) {
                        n5 = \u3007\u3007\u30070\u3007\u30070.oO80();
                    }
                    else {
                        final BaseKeyframeAnimation<Float, Float> oo8Oo00oo = this.Oo8Oo00oo;
                        n6 = n4;
                        if (oo8Oo00oo == null) {
                            break Label_0159;
                        }
                        n5 = oo8Oo00oo.oO80();
                    }
                    n6 = n4 + n5;
                }
                canvas.translate(n3 * n2 * oo08 * n + n6 * n, 0.0f);
            }
        }
    }
    
    private void \u3007\u3007\u30070\u3007\u30070(final String s, final DocumentData documentData, final Canvas canvas, final float n) {
        int i = 0;
        while (i < s.length()) {
            final String o0ooO = this.o0ooO(s, i);
            i += o0ooO.length();
            this.Oo8Oo00oo(o0ooO, documentData, canvas);
            final float measureText = this.O\u30078O8\u3007008.measureText(o0ooO, 0, 1);
            final float n2 = documentData.Oo08 / 10.0f;
            final BaseKeyframeAnimation<Float, Float> \u3007\u3007\u30070\u3007\u30070 = this.\u3007\u3007\u30070\u3007\u30070;
            float n4 = 0.0f;
            Label_0128: {
                float n3;
                if (\u3007\u3007\u30070\u3007\u30070 != null) {
                    n3 = \u3007\u3007\u30070\u3007\u30070.oO80();
                }
                else {
                    final BaseKeyframeAnimation<Float, Float> oo8Oo00oo = this.Oo8Oo00oo;
                    n4 = n2;
                    if (oo8Oo00oo == null) {
                        break Label_0128;
                    }
                    n3 = oo8Oo00oo.oO80();
                }
                n4 = n2 + n3;
            }
            canvas.translate(measureText + n4 * n, 0.0f);
        }
    }
    
    @Override
    void OoO8(final Canvas canvas, final Matrix matrix, int intValue) {
        canvas.save();
        if (!this.OOO\u3007O0.o88\u3007OO08\u3007()) {
            canvas.setMatrix(matrix);
        }
        final DocumentData documentData = ((BaseKeyframeAnimation<K, DocumentData>)this.o\u3007\u30070\u3007).oO80();
        final Font font = this.oo\u3007.\u3007\u3007888().get(documentData.\u3007o00\u3007\u3007Oo);
        if (font == null) {
            canvas.restore();
            return;
        }
        final BaseKeyframeAnimation<Integer, Integer> \u300700\u30078 = this.\u300700\u30078;
        if (\u300700\u30078 != null) {
            this.O\u30078O8\u3007008.setColor((int)\u300700\u30078.oO80());
        }
        else {
            final BaseKeyframeAnimation<Integer, Integer> o8\u3007o = this.O8\u3007o;
            if (o8\u3007o != null) {
                this.O\u30078O8\u3007008.setColor((int)o8\u3007o.oO80());
            }
            else {
                this.O\u30078O8\u3007008.setColor(documentData.oO80);
            }
        }
        final BaseKeyframeAnimation<Integer, Integer> o0ooO = this.o0ooO;
        if (o0ooO != null) {
            this.O8ooOoo\u3007.setColor((int)o0ooO.oO80());
        }
        else {
            final BaseKeyframeAnimation<Integer, Integer> \u3007o = this.\u3007o;
            if (\u3007o != null) {
                this.O8ooOoo\u3007.setColor((int)\u3007o.oO80());
            }
            else {
                this.O8ooOoo\u3007.setColor(documentData.\u300780\u3007808\u3007O);
            }
        }
        if (super.\u3007O888o0o.oO80() == null) {
            intValue = 100;
        }
        else {
            intValue = super.\u3007O888o0o.oO80().oO80();
        }
        intValue = intValue * 255 / 100;
        this.O\u30078O8\u3007008.setAlpha(intValue);
        this.O8ooOoo\u3007.setAlpha(intValue);
        final BaseKeyframeAnimation<Float, Float> o8 = this.o8;
        if (o8 != null) {
            this.O8ooOoo\u3007.setStrokeWidth((float)o8.oO80());
        }
        else {
            final BaseKeyframeAnimation<Float, Float> o\u30078 = this.o\u30078;
            if (o\u30078 != null) {
                this.O8ooOoo\u3007.setStrokeWidth((float)o\u30078.oO80());
            }
            else {
                this.O8ooOoo\u3007.setStrokeWidth(documentData.OO0o\u3007\u3007\u3007\u30070 * Utils.Oo08() * Utils.\u3007\u3007888(matrix));
            }
        }
        if (this.OOO\u3007O0.o88\u3007OO08\u3007()) {
            this.\u300708O8o\u30070(documentData, matrix, font, canvas);
        }
        else {
            this.oO(documentData, font, matrix, canvas);
        }
        canvas.restore();
    }
    
    @Override
    public <T> void o\u30070(final T t, @Nullable final LottieValueCallback<T> lottieValueCallback) {
        super.o\u30070(t, lottieValueCallback);
        if (t == LottieProperty.\u3007080) {
            final BaseKeyframeAnimation<Integer, Integer> \u300700\u30078 = this.\u300700\u30078;
            if (\u300700\u30078 != null) {
                this.\u3007oOO8O8(\u300700\u30078);
            }
            if (lottieValueCallback == null) {
                this.\u300700\u30078 = null;
            }
            else {
                (this.\u300700\u30078 = new ValueCallbackKeyframeAnimation<Integer, Integer>((LottieValueCallback<Integer>)lottieValueCallback)).\u3007080((BaseKeyframeAnimation.AnimationListener)this);
                this.\u300780\u3007808\u3007O(this.\u300700\u30078);
            }
        }
        else if (t == LottieProperty.\u3007o00\u3007\u3007Oo) {
            final BaseKeyframeAnimation<Integer, Integer> o0ooO = this.o0ooO;
            if (o0ooO != null) {
                this.\u3007oOO8O8(o0ooO);
            }
            if (lottieValueCallback == null) {
                this.o0ooO = null;
            }
            else {
                (this.o0ooO = new ValueCallbackKeyframeAnimation<Integer, Integer>((LottieValueCallback<Integer>)lottieValueCallback)).\u3007080((BaseKeyframeAnimation.AnimationListener)this);
                this.\u300780\u3007808\u3007O(this.o0ooO);
            }
        }
        else if (t == LottieProperty.\u3007\u3007808\u3007) {
            final BaseKeyframeAnimation<Float, Float> o8 = this.o8;
            if (o8 != null) {
                this.\u3007oOO8O8(o8);
            }
            if (lottieValueCallback == null) {
                this.o8 = null;
            }
            else {
                (this.o8 = new ValueCallbackKeyframeAnimation<Float, Float>((LottieValueCallback<Float>)lottieValueCallback)).\u3007080((BaseKeyframeAnimation.AnimationListener)this);
                this.\u300780\u3007808\u3007O(this.o8);
            }
        }
        else if (t == LottieProperty.\u3007O\u3007) {
            final BaseKeyframeAnimation<Float, Float> \u3007\u3007\u30070\u3007\u30070 = this.\u3007\u3007\u30070\u3007\u30070;
            if (\u3007\u3007\u30070\u3007\u30070 != null) {
                this.\u3007oOO8O8(\u3007\u3007\u30070\u3007\u30070);
            }
            if (lottieValueCallback == null) {
                this.\u3007\u3007\u30070\u3007\u30070 = null;
            }
            else {
                (this.\u3007\u3007\u30070\u3007\u30070 = new ValueCallbackKeyframeAnimation<Float, Float>((LottieValueCallback<Float>)lottieValueCallback)).\u3007080((BaseKeyframeAnimation.AnimationListener)this);
                this.\u300780\u3007808\u3007O(this.\u3007\u3007\u30070\u3007\u30070);
            }
        }
        else if (t == LottieProperty.O8ooOoo\u3007) {
            final BaseKeyframeAnimation<Float, Float> \u3007\u30070o = this.\u3007\u30070o;
            if (\u3007\u30070o != null) {
                this.\u3007oOO8O8(\u3007\u30070o);
            }
            if (lottieValueCallback == null) {
                this.\u3007\u30070o = null;
            }
            else {
                (this.\u3007\u30070o = new ValueCallbackKeyframeAnimation<Float, Float>((LottieValueCallback<Float>)lottieValueCallback)).\u3007080((BaseKeyframeAnimation.AnimationListener)this);
                this.\u300780\u3007808\u3007O(this.\u3007\u30070o);
            }
        }
    }
    
    @Override
    public void \u3007080(final RectF rectF, final Matrix matrix, final boolean b) {
        super.\u3007080(rectF, matrix, b);
        rectF.set(0.0f, 0.0f, (float)this.oo\u3007.\u3007o00\u3007\u3007Oo().width(), (float)this.oo\u3007.\u3007o00\u3007\u3007Oo().height());
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.airbnb.lottie.model.layer;

import android.graphics.RectF;
import java.util.List;
import com.airbnb.lottie.model.KeyPath;
import android.graphics.Matrix;
import androidx.annotation.NonNull;
import android.graphics.Canvas;
import java.util.Collections;
import com.airbnb.lottie.model.content.ShapeGroup;
import com.airbnb.lottie.LottieDrawable;
import com.airbnb.lottie.animation.content.ContentGroup;

public class ShapeLayer extends BaseLayer
{
    private final ContentGroup \u3007oo\u3007;
    
    ShapeLayer(final LottieDrawable lottieDrawable, final Layer layer) {
        super(lottieDrawable, layer);
        (this.\u3007oo\u3007 = new ContentGroup(lottieDrawable, this, new ShapeGroup("__container", layer.\u3007O8o08O(), false))).Oo08(Collections.emptyList(), Collections.emptyList());
    }
    
    @Override
    void OoO8(@NonNull final Canvas canvas, final Matrix matrix, final int n) {
        this.\u3007oo\u3007.\u3007o\u3007(canvas, matrix, n);
    }
    
    protected void \u30070000OOO(final KeyPath keyPath, final int n, final List<KeyPath> list, final KeyPath keyPath2) {
        this.\u3007oo\u3007.\u3007\u3007888(keyPath, n, list, keyPath2);
    }
    
    @Override
    public void \u3007080(final RectF rectF, final Matrix matrix, final boolean b) {
        super.\u3007080(rectF, matrix, b);
        this.\u3007oo\u3007.\u3007080(rectF, super.OO0o\u3007\u3007, b);
    }
}

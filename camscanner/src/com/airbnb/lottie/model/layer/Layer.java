// 
// Decompiled by Procyon v0.6.0
// 

package com.airbnb.lottie.model.layer;

import java.util.Iterator;
import java.util.Locale;
import com.airbnb.lottie.model.animatable.AnimatableTextProperties;
import com.airbnb.lottie.LottieComposition;
import com.airbnb.lottie.model.animatable.AnimatableTextFrame;
import com.airbnb.lottie.model.animatable.AnimatableTransform;
import androidx.annotation.Nullable;
import com.airbnb.lottie.model.animatable.AnimatableFloatValue;
import com.airbnb.lottie.model.content.ContentModel;
import com.airbnb.lottie.model.content.Mask;
import com.airbnb.lottie.value.Keyframe;
import java.util.List;

public class Layer
{
    private final long O8;
    private final float OO0o\u3007\u3007;
    private final int OO0o\u3007\u3007\u3007\u30070;
    private final LayerType Oo08;
    private final List<Keyframe<Float>> OoO8;
    private final float Oooo8o0\u3007;
    private final MatteType o800o8O;
    private final List<Mask> oO80;
    private final long o\u30070;
    private final List<ContentModel> \u3007080;
    @Nullable
    private final AnimatableFloatValue \u30070\u3007O0088o;
    private final AnimatableTransform \u300780\u3007808\u3007O;
    private final int \u30078o8o\u3007;
    @Nullable
    private final AnimatableTextFrame \u3007O00;
    private final boolean \u3007O888o0o;
    private final int \u3007O8o08O;
    private final int \u3007O\u3007;
    private final LottieComposition \u3007o00\u3007\u3007Oo;
    private final String \u3007o\u3007;
    private final int \u3007\u3007808\u3007;
    @Nullable
    private final String \u3007\u3007888;
    @Nullable
    private final AnimatableTextProperties \u3007\u30078O0\u30078;
    
    public Layer(final List<ContentModel> \u3007080, final LottieComposition \u3007o00\u3007\u3007Oo, final String \u3007o\u3007, final long o8, final LayerType oo08, final long o\u30070, @Nullable final String \u3007\u3007888, final List<Mask> oo9, final AnimatableTransform \u300780\u3007808\u3007O, final int oo0o\u3007\u3007\u3007\u30070, final int \u30078o8o\u3007, final int \u3007o8o08O, final float oo0o\u3007\u3007, final float oooo8o0\u3007, final int \u3007\u3007808\u3007, final int \u3007o\u30072, @Nullable final AnimatableTextFrame \u3007o00, @Nullable final AnimatableTextProperties \u3007\u30078O0\u30078, final List<Keyframe<Float>> ooO8, final MatteType o800o8O, @Nullable final AnimatableFloatValue \u30070\u3007O0088o, final boolean \u3007o888o0o) {
        this.\u3007080 = \u3007080;
        this.\u3007o00\u3007\u3007Oo = \u3007o00\u3007\u3007Oo;
        this.\u3007o\u3007 = \u3007o\u3007;
        this.O8 = o8;
        this.Oo08 = oo08;
        this.o\u30070 = o\u30070;
        this.\u3007\u3007888 = \u3007\u3007888;
        this.oO80 = oo9;
        this.\u300780\u3007808\u3007O = \u300780\u3007808\u3007O;
        this.OO0o\u3007\u3007\u3007\u30070 = oo0o\u3007\u3007\u3007\u30070;
        this.\u30078o8o\u3007 = \u30078o8o\u3007;
        this.\u3007O8o08O = \u3007o8o08O;
        this.OO0o\u3007\u3007 = oo0o\u3007\u3007;
        this.Oooo8o0\u3007 = oooo8o0\u3007;
        this.\u3007\u3007808\u3007 = \u3007\u3007808\u3007;
        this.\u3007O\u3007 = \u3007o\u30072;
        this.\u3007O00 = \u3007o00;
        this.\u3007\u30078O0\u30078 = \u3007\u30078O0\u30078;
        this.OoO8 = ooO8;
        this.o800o8O = o800o8O;
        this.\u30070\u3007O0088o = \u30070\u3007O0088o;
        this.\u3007O888o0o = \u3007o888o0o;
    }
    
    public LayerType O8() {
        return this.Oo08;
    }
    
    int OO0o\u3007\u3007() {
        return this.\u3007O8o08O;
    }
    
    int OO0o\u3007\u3007\u3007\u30070() {
        return this.\u3007\u3007808\u3007;
    }
    
    List<Mask> Oo08() {
        return this.oO80;
    }
    
    float OoO8() {
        return this.OO0o\u3007\u3007;
    }
    
    int Oooo8o0\u3007() {
        return this.\u30078o8o\u3007;
    }
    
    AnimatableTransform o800o8O() {
        return this.\u300780\u3007808\u3007O;
    }
    
    long oO80() {
        return this.o\u30070;
    }
    
    public String oo88o8O(final String s) {
        final StringBuilder sb = new StringBuilder();
        sb.append(s);
        sb.append(this.\u3007\u3007888());
        sb.append("\n");
        final Layer \u30070\u3007O0088o = this.\u3007o00\u3007\u3007Oo.\u30070\u3007O0088o(this.oO80());
        if (\u30070\u3007O0088o != null) {
            sb.append("\t\tParents: ");
            sb.append(\u30070\u3007O0088o.\u3007\u3007888());
            for (Layer layer = this.\u3007o00\u3007\u3007Oo.\u30070\u3007O0088o(\u30070\u3007O0088o.oO80()); layer != null; layer = this.\u3007o00\u3007\u3007Oo.\u30070\u3007O0088o(layer.oO80())) {
                sb.append("->");
                sb.append(layer.\u3007\u3007888());
            }
            sb.append(s);
            sb.append("\n");
        }
        if (!this.Oo08().isEmpty()) {
            sb.append(s);
            sb.append("\tMasks: ");
            sb.append(this.Oo08().size());
            sb.append("\n");
        }
        if (this.\u3007\u3007808\u3007() != 0 && this.Oooo8o0\u3007() != 0) {
            sb.append(s);
            sb.append("\tBackground: ");
            sb.append(String.format(Locale.US, "%dx%d %X\n", this.\u3007\u3007808\u3007(), this.Oooo8o0\u3007(), this.OO0o\u3007\u3007()));
        }
        if (!this.\u3007080.isEmpty()) {
            sb.append(s);
            sb.append("\tShapes:\n");
            for (final ContentModel next : this.\u3007080) {
                sb.append(s);
                sb.append("\t\t");
                sb.append(next);
                sb.append("\n");
            }
        }
        return sb.toString();
    }
    
    MatteType o\u30070() {
        return this.o800o8O;
    }
    
    @Override
    public String toString() {
        return this.oo88o8O("");
    }
    
    LottieComposition \u3007080() {
        return this.\u3007o00\u3007\u3007Oo;
    }
    
    @Nullable
    AnimatableFloatValue \u30070\u3007O0088o() {
        return this.\u30070\u3007O0088o;
    }
    
    int \u300780\u3007808\u3007O() {
        return this.\u3007O\u3007;
    }
    
    @Nullable
    String \u30078o8o\u3007() {
        return this.\u3007\u3007888;
    }
    
    @Nullable
    AnimatableTextFrame \u3007O00() {
        return this.\u3007O00;
    }
    
    public boolean \u3007O888o0o() {
        return this.\u3007O888o0o;
    }
    
    List<ContentModel> \u3007O8o08O() {
        return this.\u3007080;
    }
    
    float \u3007O\u3007() {
        return this.Oooo8o0\u3007 / this.\u3007o00\u3007\u3007Oo.Oo08();
    }
    
    public long \u3007o00\u3007\u3007Oo() {
        return this.O8;
    }
    
    List<Keyframe<Float>> \u3007o\u3007() {
        return this.OoO8;
    }
    
    int \u3007\u3007808\u3007() {
        return this.OO0o\u3007\u3007\u3007\u30070;
    }
    
    String \u3007\u3007888() {
        return this.\u3007o\u3007;
    }
    
    @Nullable
    AnimatableTextProperties \u3007\u30078O0\u30078() {
        return this.\u3007\u30078O0\u30078;
    }
    
    public enum LayerType
    {
        private static final LayerType[] $VALUES;
        
        IMAGE, 
        NULL, 
        PRE_COMP, 
        SHAPE, 
        SOLID, 
        TEXT, 
        UNKNOWN;
    }
    
    public enum MatteType
    {
        private static final MatteType[] $VALUES;
        
        ADD, 
        INVERT, 
        NONE, 
        UNKNOWN;
    }
}

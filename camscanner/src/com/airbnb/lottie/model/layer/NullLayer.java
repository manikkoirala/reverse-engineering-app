// 
// Decompiled by Procyon v0.6.0
// 

package com.airbnb.lottie.model.layer;

import android.graphics.RectF;
import android.graphics.Matrix;
import android.graphics.Canvas;
import com.airbnb.lottie.LottieDrawable;

public class NullLayer extends BaseLayer
{
    NullLayer(final LottieDrawable lottieDrawable, final Layer layer) {
        super(lottieDrawable, layer);
    }
    
    @Override
    void OoO8(final Canvas canvas, final Matrix matrix, final int n) {
    }
    
    @Override
    public void \u3007080(final RectF rectF, final Matrix matrix, final boolean b) {
        super.\u3007080(rectF, matrix, b);
        rectF.set(0.0f, 0.0f, 0.0f, 0.0f);
    }
}

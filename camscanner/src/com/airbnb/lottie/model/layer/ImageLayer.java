// 
// Decompiled by Procyon v0.6.0
// 

package com.airbnb.lottie.model.layer;

import android.graphics.RectF;
import com.airbnb.lottie.animation.keyframe.ValueCallbackKeyframeAnimation;
import com.airbnb.lottie.LottieProperty;
import com.airbnb.lottie.value.LottieValueCallback;
import com.airbnb.lottie.utils.Utils;
import android.graphics.Matrix;
import androidx.annotation.NonNull;
import android.graphics.Canvas;
import android.graphics.Bitmap;
import com.airbnb.lottie.animation.LPaint;
import com.airbnb.lottie.LottieDrawable;
import android.graphics.Paint;
import android.graphics.Rect;
import androidx.annotation.Nullable;
import android.graphics.ColorFilter;
import com.airbnb.lottie.animation.keyframe.BaseKeyframeAnimation;

public class ImageLayer extends BaseLayer
{
    @Nullable
    private BaseKeyframeAnimation<ColorFilter, ColorFilter> O\u30078O8\u3007008;
    private final Rect o\u3007O8\u3007\u3007o;
    private final Rect \u300700;
    private final Paint \u3007oo\u3007;
    
    ImageLayer(final LottieDrawable lottieDrawable, final Layer layer) {
        super(lottieDrawable, layer);
        this.\u3007oo\u3007 = new LPaint(3);
        this.o\u3007O8\u3007\u3007o = new Rect();
        this.\u300700 = new Rect();
    }
    
    @Nullable
    private Bitmap \u3007o() {
        return super.Oooo8o0\u3007.\u3007\u30078O0\u30078(super.\u3007\u3007808\u3007.\u30078o8o\u3007());
    }
    
    public void OoO8(@NonNull final Canvas canvas, final Matrix matrix, final int alpha) {
        final Bitmap \u3007o = this.\u3007o();
        if (\u3007o != null) {
            if (!\u3007o.isRecycled()) {
                final float oo08 = Utils.Oo08();
                this.\u3007oo\u3007.setAlpha(alpha);
                final BaseKeyframeAnimation<ColorFilter, ColorFilter> o\u30078O8\u3007008 = this.O\u30078O8\u3007008;
                if (o\u30078O8\u3007008 != null) {
                    this.\u3007oo\u3007.setColorFilter((ColorFilter)o\u30078O8\u3007008.oO80());
                }
                canvas.save();
                canvas.concat(matrix);
                this.o\u3007O8\u3007\u3007o.set(0, 0, \u3007o.getWidth(), \u3007o.getHeight());
                this.\u300700.set(0, 0, (int)(\u3007o.getWidth() * oo08), (int)(\u3007o.getHeight() * oo08));
                canvas.drawBitmap(\u3007o, this.o\u3007O8\u3007\u3007o, this.\u300700, this.\u3007oo\u3007);
                canvas.restore();
            }
        }
    }
    
    @Override
    public <T> void o\u30070(final T t, @Nullable final LottieValueCallback<T> lottieValueCallback) {
        super.o\u30070(t, lottieValueCallback);
        if (t == LottieProperty.\u3007oOO8O8) {
            if (lottieValueCallback == null) {
                this.O\u30078O8\u3007008 = null;
            }
            else {
                this.O\u30078O8\u3007008 = new ValueCallbackKeyframeAnimation<ColorFilter, ColorFilter>((LottieValueCallback<ColorFilter>)lottieValueCallback);
            }
        }
    }
    
    @Override
    public void \u3007080(final RectF rectF, final Matrix matrix, final boolean b) {
        super.\u3007080(rectF, matrix, b);
        final Bitmap \u3007o = this.\u3007o();
        if (\u3007o != null) {
            rectF.set(0.0f, 0.0f, \u3007o.getWidth() * Utils.Oo08(), \u3007o.getHeight() * Utils.Oo08());
            super.OO0o\u3007\u3007.mapRect(rectF);
        }
    }
}

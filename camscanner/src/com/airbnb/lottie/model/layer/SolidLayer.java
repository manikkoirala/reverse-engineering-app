// 
// Decompiled by Procyon v0.6.0
// 

package com.airbnb.lottie.model.layer;

import com.airbnb.lottie.animation.keyframe.ValueCallbackKeyframeAnimation;
import com.airbnb.lottie.LottieProperty;
import com.airbnb.lottie.value.LottieValueCallback;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Canvas;
import android.graphics.Paint$Style;
import com.airbnb.lottie.animation.LPaint;
import com.airbnb.lottie.LottieDrawable;
import android.graphics.RectF;
import androidx.annotation.Nullable;
import android.graphics.ColorFilter;
import com.airbnb.lottie.animation.keyframe.BaseKeyframeAnimation;
import android.graphics.Paint;
import android.graphics.Path;

public class SolidLayer extends BaseLayer
{
    private final Layer O8ooOoo\u3007;
    private final Path O\u30078O8\u3007008;
    private final Paint o\u3007O8\u3007\u3007o;
    private final float[] \u300700;
    @Nullable
    private BaseKeyframeAnimation<ColorFilter, ColorFilter> \u3007oOO8O8;
    private final RectF \u3007oo\u3007;
    
    SolidLayer(final LottieDrawable lottieDrawable, final Layer o8ooOoo\u3007) {
        super(lottieDrawable, o8ooOoo\u3007);
        this.\u3007oo\u3007 = new RectF();
        final LPaint o\u3007O8\u3007\u3007o = new LPaint();
        this.o\u3007O8\u3007\u3007o = o\u3007O8\u3007\u3007o;
        this.\u300700 = new float[8];
        this.O\u30078O8\u3007008 = new Path();
        this.O8ooOoo\u3007 = o8ooOoo\u3007;
        o\u3007O8\u3007\u3007o.setAlpha(0);
        o\u3007O8\u3007\u3007o.setStyle(Paint$Style.FILL);
        o\u3007O8\u3007\u3007o.setColor(o8ooOoo\u3007.OO0o\u3007\u3007());
    }
    
    public void OoO8(final Canvas canvas, final Matrix matrix, int alpha) {
        final int alpha2 = Color.alpha(this.O8ooOoo\u3007.OO0o\u3007\u3007());
        if (alpha2 == 0) {
            return;
        }
        int intValue;
        if (super.\u3007O888o0o.oO80() == null) {
            intValue = 100;
        }
        else {
            intValue = super.\u3007O888o0o.oO80().oO80();
        }
        alpha = (int)(alpha / 255.0f * (alpha2 / 255.0f * intValue / 100.0f) * 255.0f);
        this.o\u3007O8\u3007\u3007o.setAlpha(alpha);
        final BaseKeyframeAnimation<ColorFilter, ColorFilter> \u3007oOO8O8 = this.\u3007oOO8O8;
        if (\u3007oOO8O8 != null) {
            this.o\u3007O8\u3007\u3007o.setColorFilter((ColorFilter)\u3007oOO8O8.oO80());
        }
        if (alpha > 0) {
            final float[] \u300700 = this.\u300700;
            \u300700[1] = (\u300700[0] = 0.0f);
            \u300700[2] = (float)this.O8ooOoo\u3007.\u3007\u3007808\u3007();
            final float[] \u30072 = this.\u300700;
            \u30072[3] = 0.0f;
            \u30072[4] = (float)this.O8ooOoo\u3007.\u3007\u3007808\u3007();
            this.\u300700[5] = (float)this.O8ooOoo\u3007.Oooo8o0\u3007();
            final float[] \u30073 = this.\u300700;
            \u30073[6] = 0.0f;
            \u30073[7] = (float)this.O8ooOoo\u3007.Oooo8o0\u3007();
            matrix.mapPoints(this.\u300700);
            this.O\u30078O8\u3007008.reset();
            final Path o\u30078O8\u3007008 = this.O\u30078O8\u3007008;
            final float[] \u30074 = this.\u300700;
            o\u30078O8\u3007008.moveTo(\u30074[0], \u30074[1]);
            final Path o\u30078O8\u30079 = this.O\u30078O8\u3007008;
            final float[] \u30075 = this.\u300700;
            o\u30078O8\u30079.lineTo(\u30075[2], \u30075[3]);
            final Path o\u30078O8\u300710 = this.O\u30078O8\u3007008;
            final float[] \u30076 = this.\u300700;
            o\u30078O8\u300710.lineTo(\u30076[4], \u30076[5]);
            final Path o\u30078O8\u300711 = this.O\u30078O8\u3007008;
            final float[] \u30077 = this.\u300700;
            o\u30078O8\u300711.lineTo(\u30077[6], \u30077[7]);
            final Path o\u30078O8\u300712 = this.O\u30078O8\u3007008;
            final float[] \u30078 = this.\u300700;
            o\u30078O8\u300712.lineTo(\u30078[0], \u30078[1]);
            this.O\u30078O8\u3007008.close();
            canvas.drawPath(this.O\u30078O8\u3007008, this.o\u3007O8\u3007\u3007o);
        }
    }
    
    @Override
    public <T> void o\u30070(final T t, @Nullable final LottieValueCallback<T> lottieValueCallback) {
        super.o\u30070(t, lottieValueCallback);
        if (t == LottieProperty.\u3007oOO8O8) {
            if (lottieValueCallback == null) {
                this.\u3007oOO8O8 = null;
            }
            else {
                this.\u3007oOO8O8 = new ValueCallbackKeyframeAnimation<ColorFilter, ColorFilter>((LottieValueCallback<ColorFilter>)lottieValueCallback);
            }
        }
    }
    
    @Override
    public void \u3007080(final RectF rectF, final Matrix matrix, final boolean b) {
        super.\u3007080(rectF, matrix, b);
        this.\u3007oo\u3007.set(0.0f, 0.0f, (float)this.O8ooOoo\u3007.\u3007\u3007808\u3007(), (float)this.O8ooOoo\u3007.Oooo8o0\u3007());
        super.OO0o\u3007\u3007.mapRect(this.\u3007oo\u3007);
        rectF.set(this.\u3007oo\u3007);
    }
}

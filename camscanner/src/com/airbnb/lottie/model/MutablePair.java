// 
// Decompiled by Procyon v0.6.0
// 

package com.airbnb.lottie.model;

import androidx.core.util.Pair;
import androidx.annotation.Nullable;
import androidx.annotation.RestrictTo;

@RestrictTo({ RestrictTo.Scope.LIBRARY })
public class MutablePair<T>
{
    @Nullable
    T \u3007080;
    @Nullable
    T \u3007o00\u3007\u3007Oo;
    
    private static boolean \u3007080(final Object o, final Object obj) {
        return o == obj || (o != null && o.equals(obj));
    }
    
    @Override
    public boolean equals(final Object o) {
        final boolean b = o instanceof Pair;
        final boolean b2 = false;
        if (!b) {
            return false;
        }
        final Pair pair = (Pair)o;
        boolean b3 = b2;
        if (\u3007080(pair.first, this.\u3007080)) {
            b3 = b2;
            if (\u3007080(pair.second, this.\u3007o00\u3007\u3007Oo)) {
                b3 = true;
            }
        }
        return b3;
    }
    
    @Override
    public int hashCode() {
        final T \u3007080 = this.\u3007080;
        int hashCode = 0;
        int hashCode2;
        if (\u3007080 == null) {
            hashCode2 = 0;
        }
        else {
            hashCode2 = \u3007080.hashCode();
        }
        final T \u3007o00\u3007\u3007Oo = this.\u3007o00\u3007\u3007Oo;
        if (\u3007o00\u3007\u3007Oo != null) {
            hashCode = \u3007o00\u3007\u3007Oo.hashCode();
        }
        return hashCode2 ^ hashCode;
    }
    
    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append("Pair{");
        sb.append(String.valueOf(this.\u3007080));
        sb.append(" ");
        sb.append(String.valueOf(this.\u3007o00\u3007\u3007Oo));
        sb.append("}");
        return sb.toString();
    }
    
    public void \u3007o00\u3007\u3007Oo(final T \u3007080, final T \u3007o00\u3007\u3007Oo) {
        this.\u3007080 = \u3007080;
        this.\u3007o00\u3007\u3007Oo = \u3007o00\u3007\u3007Oo;
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.airbnb.lottie.model;

import androidx.annotation.RestrictTo;

@RestrictTo({ RestrictTo.Scope.LIBRARY })
public class Font
{
    private final float O8;
    private final String \u3007080;
    private final String \u3007o00\u3007\u3007Oo;
    private final String \u3007o\u3007;
    
    public Font(final String \u3007080, final String \u3007o00\u3007\u3007Oo, final String \u3007o\u3007, final float o8) {
        this.\u3007080 = \u3007080;
        this.\u3007o00\u3007\u3007Oo = \u3007o00\u3007\u3007Oo;
        this.\u3007o\u3007 = \u3007o\u3007;
        this.O8 = o8;
    }
    
    public String \u3007080() {
        return this.\u3007080;
    }
    
    public String \u3007o00\u3007\u3007Oo() {
        return this.\u3007o00\u3007\u3007Oo;
    }
    
    public String \u3007o\u3007() {
        return this.\u3007o\u3007;
    }
}

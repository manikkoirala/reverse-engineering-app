// 
// Decompiled by Procyon v0.6.0
// 

package com.airbnb.lottie.model;

import androidx.annotation.CheckResult;
import androidx.annotation.RestrictTo;
import java.util.Arrays;
import java.util.Collection;
import java.util.ArrayList;
import androidx.annotation.Nullable;
import java.util.List;

public class KeyPath
{
    private final List<String> \u3007080;
    @Nullable
    private KeyPathElement \u3007o00\u3007\u3007Oo;
    
    private KeyPath(final KeyPath keyPath) {
        this.\u3007080 = new ArrayList<String>(keyPath.\u3007080);
        this.\u3007o00\u3007\u3007Oo = keyPath.\u3007o00\u3007\u3007Oo;
    }
    
    public KeyPath(final String... a) {
        this.\u3007080 = Arrays.asList(a);
    }
    
    private boolean o\u30070(final String anObject) {
        return "__container".equals(anObject);
    }
    
    private boolean \u3007o00\u3007\u3007Oo() {
        final List<String> \u3007080 = this.\u3007080;
        return ((String)\u3007080.get(\u3007080.size() - 1)).equals("**");
    }
    
    @Nullable
    @RestrictTo({ RestrictTo.Scope.LIBRARY })
    public KeyPathElement O8() {
        return this.\u3007o00\u3007\u3007Oo;
    }
    
    @RestrictTo({ RestrictTo.Scope.LIBRARY })
    public int Oo08(final String anObject, final int n) {
        if (this.o\u30070(anObject)) {
            return 0;
        }
        if (!this.\u3007080.get(n).equals("**")) {
            return 1;
        }
        if (n == this.\u3007080.size() - 1) {
            return 0;
        }
        if (this.\u3007080.get(n + 1).equals(anObject)) {
            return 2;
        }
        return 0;
    }
    
    @RestrictTo({ RestrictTo.Scope.LIBRARY })
    public boolean oO80(final String anObject, final int n) {
        final boolean equals = "__container".equals(anObject);
        final boolean b = true;
        if (equals) {
            return true;
        }
        boolean b2 = b;
        if (n >= this.\u3007080.size() - 1) {
            b2 = (this.\u3007080.get(n).equals("**") && b);
        }
        return b2;
    }
    
    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append("KeyPath{keys=");
        sb.append(this.\u3007080);
        sb.append(",resolved=");
        sb.append(this.\u3007o00\u3007\u3007Oo != null);
        sb.append('}');
        return sb.toString();
    }
    
    @CheckResult
    @RestrictTo({ RestrictTo.Scope.LIBRARY })
    public KeyPath \u3007080(final String s) {
        final KeyPath keyPath = new KeyPath(this);
        keyPath.\u3007080.add(s);
        return keyPath;
    }
    
    @RestrictTo({ RestrictTo.Scope.LIBRARY })
    public KeyPath \u300780\u3007808\u3007O(final KeyPathElement \u3007o00\u3007\u3007Oo) {
        final KeyPath keyPath = new KeyPath(this);
        keyPath.\u3007o00\u3007\u3007Oo = \u3007o00\u3007\u3007Oo;
        return keyPath;
    }
    
    @RestrictTo({ RestrictTo.Scope.LIBRARY })
    public boolean \u3007o\u3007(final String anObject, int n) {
        final int size = this.\u3007080.size();
        final boolean b = false;
        final boolean b2 = false;
        if (n >= size) {
            return false;
        }
        final boolean b3 = n == this.\u3007080.size() - 1;
        final String s = this.\u3007080.get(n);
        if (!s.equals("**")) {
            final boolean b4 = s.equals(anObject) || s.equals("*");
            if (!b3) {
                boolean b5 = b2;
                if (n != this.\u3007080.size() - 2) {
                    return b5;
                }
                b5 = b2;
                if (!this.\u3007o00\u3007\u3007Oo()) {
                    return b5;
                }
            }
            boolean b5 = b2;
            if (b4) {
                b5 = true;
            }
            return b5;
        }
        if (!b3 && this.\u3007080.get(n + 1).equals(anObject)) {
            if (n != this.\u3007080.size() - 2) {
                boolean b6 = b;
                if (n != this.\u3007080.size() - 3) {
                    return b6;
                }
                b6 = b;
                if (!this.\u3007o00\u3007\u3007Oo()) {
                    return b6;
                }
            }
            return true;
        }
        return b3 || (++n >= this.\u3007080.size() - 1 && this.\u3007080.get(n).equals(anObject));
    }
    
    @RestrictTo({ RestrictTo.Scope.LIBRARY })
    public boolean \u3007\u3007888(final String anObject, final int n) {
        return this.o\u30070(anObject) || (n < this.\u3007080.size() && (this.\u3007080.get(n).equals(anObject) || this.\u3007080.get(n).equals("**") || this.\u3007080.get(n).equals("*")));
    }
}

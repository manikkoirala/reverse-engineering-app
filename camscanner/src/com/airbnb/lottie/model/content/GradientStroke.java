// 
// Decompiled by Procyon v0.6.0
// 

package com.airbnb.lottie.model.content;

import com.airbnb.lottie.animation.content.GradientStrokeContent;
import com.airbnb.lottie.animation.content.Content;
import com.airbnb.lottie.model.layer.BaseLayer;
import com.airbnb.lottie.LottieDrawable;
import com.airbnb.lottie.model.animatable.AnimatableGradientColorValue;
import androidx.annotation.Nullable;
import com.airbnb.lottie.model.animatable.AnimatableFloatValue;
import java.util.List;
import com.airbnb.lottie.model.animatable.AnimatablePointValue;
import com.airbnb.lottie.model.animatable.AnimatableIntegerValue;

public class GradientStroke implements ContentModel
{
    private final AnimatableIntegerValue O8;
    private final boolean OO0o\u3007\u3007;
    private final float OO0o\u3007\u3007\u3007\u30070;
    private final AnimatablePointValue Oo08;
    private final ShapeStroke.LineCapType oO80;
    private final AnimatablePointValue o\u30070;
    private final String \u3007080;
    private final ShapeStroke.LineJoinType \u300780\u3007808\u3007O;
    private final List<AnimatableFloatValue> \u30078o8o\u3007;
    @Nullable
    private final AnimatableFloatValue \u3007O8o08O;
    private final GradientType \u3007o00\u3007\u3007Oo;
    private final AnimatableGradientColorValue \u3007o\u3007;
    private final AnimatableFloatValue \u3007\u3007888;
    
    public GradientStroke(final String \u3007080, final GradientType \u3007o00\u3007\u3007Oo, final AnimatableGradientColorValue \u3007o\u3007, final AnimatableIntegerValue o8, final AnimatablePointValue oo08, final AnimatablePointValue o\u30070, final AnimatableFloatValue \u3007\u3007888, final ShapeStroke.LineCapType oo9, final ShapeStroke.LineJoinType \u300780\u3007808\u3007O, final float oo0o\u3007\u3007\u3007\u30070, final List<AnimatableFloatValue> \u30078o8o\u3007, @Nullable final AnimatableFloatValue \u3007o8o08O, final boolean oo0o\u3007\u3007) {
        this.\u3007080 = \u3007080;
        this.\u3007o00\u3007\u3007Oo = \u3007o00\u3007\u3007Oo;
        this.\u3007o\u3007 = \u3007o\u3007;
        this.O8 = o8;
        this.Oo08 = oo08;
        this.o\u30070 = o\u30070;
        this.\u3007\u3007888 = \u3007\u3007888;
        this.oO80 = oo9;
        this.\u300780\u3007808\u3007O = \u300780\u3007808\u3007O;
        this.OO0o\u3007\u3007\u3007\u30070 = oo0o\u3007\u3007\u3007\u30070;
        this.\u30078o8o\u3007 = \u30078o8o\u3007;
        this.\u3007O8o08O = \u3007o8o08O;
        this.OO0o\u3007\u3007 = oo0o\u3007\u3007;
    }
    
    public AnimatablePointValue O8() {
        return this.o\u30070;
    }
    
    public AnimatableFloatValue OO0o\u3007\u3007() {
        return this.\u3007\u3007888;
    }
    
    public String OO0o\u3007\u3007\u3007\u30070() {
        return this.\u3007080;
    }
    
    public AnimatableGradientColorValue Oo08() {
        return this.\u3007o\u3007;
    }
    
    public boolean Oooo8o0\u3007() {
        return this.OO0o\u3007\u3007;
    }
    
    public List<AnimatableFloatValue> oO80() {
        return this.\u30078o8o\u3007;
    }
    
    public GradientType o\u30070() {
        return this.\u3007o00\u3007\u3007Oo;
    }
    
    @Override
    public Content \u3007080(final LottieDrawable lottieDrawable, final BaseLayer baseLayer) {
        return new GradientStrokeContent(lottieDrawable, baseLayer, this);
    }
    
    public float \u300780\u3007808\u3007O() {
        return this.OO0o\u3007\u3007\u3007\u30070;
    }
    
    public AnimatableIntegerValue \u30078o8o\u3007() {
        return this.O8;
    }
    
    public AnimatablePointValue \u3007O8o08O() {
        return this.Oo08;
    }
    
    public ShapeStroke.LineCapType \u3007o00\u3007\u3007Oo() {
        return this.oO80;
    }
    
    @Nullable
    public AnimatableFloatValue \u3007o\u3007() {
        return this.\u3007O8o08O;
    }
    
    public ShapeStroke.LineJoinType \u3007\u3007888() {
        return this.\u300780\u3007808\u3007O;
    }
}

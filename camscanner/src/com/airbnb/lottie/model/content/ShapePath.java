// 
// Decompiled by Procyon v0.6.0
// 

package com.airbnb.lottie.model.content;

import com.airbnb.lottie.animation.content.ShapeContent;
import com.airbnb.lottie.animation.content.Content;
import com.airbnb.lottie.model.layer.BaseLayer;
import com.airbnb.lottie.LottieDrawable;
import com.airbnb.lottie.model.animatable.AnimatableShapeValue;

public class ShapePath implements ContentModel
{
    private final boolean O8;
    private final String \u3007080;
    private final int \u3007o00\u3007\u3007Oo;
    private final AnimatableShapeValue \u3007o\u3007;
    
    public ShapePath(final String \u3007080, final int \u3007o00\u3007\u3007Oo, final AnimatableShapeValue \u3007o\u3007, final boolean o8) {
        this.\u3007080 = \u3007080;
        this.\u3007o00\u3007\u3007Oo = \u3007o00\u3007\u3007Oo;
        this.\u3007o\u3007 = \u3007o\u3007;
        this.O8 = o8;
    }
    
    public boolean O8() {
        return this.O8;
    }
    
    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append("ShapePath{name=");
        sb.append(this.\u3007080);
        sb.append(", index=");
        sb.append(this.\u3007o00\u3007\u3007Oo);
        sb.append('}');
        return sb.toString();
    }
    
    @Override
    public Content \u3007080(final LottieDrawable lottieDrawable, final BaseLayer baseLayer) {
        return new ShapeContent(lottieDrawable, baseLayer, this);
    }
    
    public String \u3007o00\u3007\u3007Oo() {
        return this.\u3007080;
    }
    
    public AnimatableShapeValue \u3007o\u3007() {
        return this.\u3007o\u3007;
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.airbnb.lottie.model.content;

import androidx.annotation.Nullable;
import com.airbnb.lottie.animation.content.Content;
import com.airbnb.lottie.model.layer.BaseLayer;
import com.airbnb.lottie.LottieDrawable;

public interface ContentModel
{
    @Nullable
    Content \u3007080(final LottieDrawable p0, final BaseLayer p1);
}

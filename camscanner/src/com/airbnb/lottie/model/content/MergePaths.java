// 
// Decompiled by Procyon v0.6.0
// 

package com.airbnb.lottie.model.content;

import androidx.annotation.Nullable;
import com.airbnb.lottie.animation.content.MergePathsContent;
import com.airbnb.lottie.utils.Logger;
import com.airbnb.lottie.animation.content.Content;
import com.airbnb.lottie.model.layer.BaseLayer;
import com.airbnb.lottie.LottieDrawable;

public class MergePaths implements ContentModel
{
    private final String \u3007080;
    private final MergePathsMode \u3007o00\u3007\u3007Oo;
    private final boolean \u3007o\u3007;
    
    public MergePaths(final String \u3007080, final MergePathsMode \u3007o00\u3007\u3007Oo, final boolean \u3007o\u3007) {
        this.\u3007080 = \u3007080;
        this.\u3007o00\u3007\u3007Oo = \u3007o00\u3007\u3007Oo;
        this.\u3007o\u3007 = \u3007o\u3007;
    }
    
    public boolean O8() {
        return this.\u3007o\u3007;
    }
    
    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append("MergePaths{mode=");
        sb.append(this.\u3007o00\u3007\u3007Oo);
        sb.append('}');
        return sb.toString();
    }
    
    @Nullable
    @Override
    public Content \u3007080(final LottieDrawable lottieDrawable, final BaseLayer baseLayer) {
        if (!lottieDrawable.OO0o\u3007\u3007()) {
            Logger.\u3007o\u3007("Animation contains merge paths but they are disabled.");
            return null;
        }
        return new MergePathsContent(this);
    }
    
    public MergePathsMode \u3007o00\u3007\u3007Oo() {
        return this.\u3007o00\u3007\u3007Oo;
    }
    
    public String \u3007o\u3007() {
        return this.\u3007080;
    }
    
    public enum MergePathsMode
    {
        private static final MergePathsMode[] $VALUES;
        
        ADD, 
        EXCLUDE_INTERSECTIONS, 
        INTERSECT, 
        MERGE, 
        SUBTRACT;
        
        public static MergePathsMode forId(final int n) {
            if (n == 1) {
                return MergePathsMode.MERGE;
            }
            if (n == 2) {
                return MergePathsMode.ADD;
            }
            if (n == 3) {
                return MergePathsMode.SUBTRACT;
            }
            if (n == 4) {
                return MergePathsMode.INTERSECT;
            }
            if (n != 5) {
                return MergePathsMode.MERGE;
            }
            return MergePathsMode.EXCLUDE_INTERSECTIONS;
        }
    }
}

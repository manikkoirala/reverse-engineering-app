// 
// Decompiled by Procyon v0.6.0
// 

package com.airbnb.lottie.model.content;

import com.airbnb.lottie.model.animatable.AnimatableIntegerValue;
import com.airbnb.lottie.model.animatable.AnimatableShapeValue;

public class Mask
{
    private final boolean O8;
    private final MaskMode \u3007080;
    private final AnimatableShapeValue \u3007o00\u3007\u3007Oo;
    private final AnimatableIntegerValue \u3007o\u3007;
    
    public Mask(final MaskMode \u3007080, final AnimatableShapeValue \u3007o00\u3007\u3007Oo, final AnimatableIntegerValue \u3007o\u3007, final boolean o8) {
        this.\u3007080 = \u3007080;
        this.\u3007o00\u3007\u3007Oo = \u3007o00\u3007\u3007Oo;
        this.\u3007o\u3007 = \u3007o\u3007;
        this.O8 = o8;
    }
    
    public boolean O8() {
        return this.O8;
    }
    
    public MaskMode \u3007080() {
        return this.\u3007080;
    }
    
    public AnimatableShapeValue \u3007o00\u3007\u3007Oo() {
        return this.\u3007o00\u3007\u3007Oo;
    }
    
    public AnimatableIntegerValue \u3007o\u3007() {
        return this.\u3007o\u3007;
    }
    
    public enum MaskMode
    {
        private static final MaskMode[] $VALUES;
        
        MASK_MODE_ADD, 
        MASK_MODE_INTERSECT, 
        MASK_MODE_NONE, 
        MASK_MODE_SUBTRACT;
    }
}

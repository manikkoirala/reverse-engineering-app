// 
// Decompiled by Procyon v0.6.0
// 

package com.airbnb.lottie.model.content;

import androidx.annotation.Nullable;
import com.airbnb.lottie.animation.content.RepeaterContent;
import com.airbnb.lottie.animation.content.Content;
import com.airbnb.lottie.model.layer.BaseLayer;
import com.airbnb.lottie.LottieDrawable;
import com.airbnb.lottie.model.animatable.AnimatableFloatValue;
import com.airbnb.lottie.model.animatable.AnimatableTransform;

public class Repeater implements ContentModel
{
    private final AnimatableTransform O8;
    private final boolean Oo08;
    private final String \u3007080;
    private final AnimatableFloatValue \u3007o00\u3007\u3007Oo;
    private final AnimatableFloatValue \u3007o\u3007;
    
    public Repeater(final String \u3007080, final AnimatableFloatValue \u3007o00\u3007\u3007Oo, final AnimatableFloatValue \u3007o\u3007, final AnimatableTransform o8, final boolean oo08) {
        this.\u3007080 = \u3007080;
        this.\u3007o00\u3007\u3007Oo = \u3007o00\u3007\u3007Oo;
        this.\u3007o\u3007 = \u3007o\u3007;
        this.O8 = o8;
        this.Oo08 = oo08;
    }
    
    public AnimatableFloatValue O8() {
        return this.\u3007o\u3007;
    }
    
    public AnimatableTransform Oo08() {
        return this.O8;
    }
    
    public boolean o\u30070() {
        return this.Oo08;
    }
    
    @Nullable
    @Override
    public Content \u3007080(final LottieDrawable lottieDrawable, final BaseLayer baseLayer) {
        return new RepeaterContent(lottieDrawable, baseLayer, this);
    }
    
    public AnimatableFloatValue \u3007o00\u3007\u3007Oo() {
        return this.\u3007o00\u3007\u3007Oo;
    }
    
    public String \u3007o\u3007() {
        return this.\u3007080;
    }
}

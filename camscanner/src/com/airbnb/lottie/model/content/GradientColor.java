// 
// Decompiled by Procyon v0.6.0
// 

package com.airbnb.lottie.model.content;

import com.airbnb.lottie.utils.GammaEvaluator;
import com.airbnb.lottie.utils.MiscUtils;

public class GradientColor
{
    private final float[] \u3007080;
    private final int[] \u3007o00\u3007\u3007Oo;
    
    public GradientColor(final float[] \u3007080, final int[] \u3007o00\u3007\u3007Oo) {
        this.\u3007080 = \u3007080;
        this.\u3007o00\u3007\u3007Oo = \u3007o00\u3007\u3007Oo;
    }
    
    public void O8(final GradientColor gradientColor, final GradientColor gradientColor2, final float n) {
        if (gradientColor.\u3007o00\u3007\u3007Oo.length == gradientColor2.\u3007o00\u3007\u3007Oo.length) {
            for (int i = 0; i < gradientColor.\u3007o00\u3007\u3007Oo.length; ++i) {
                this.\u3007080[i] = MiscUtils.OO0o\u3007\u3007\u3007\u30070(gradientColor.\u3007080[i], gradientColor2.\u3007080[i], n);
                this.\u3007o00\u3007\u3007Oo[i] = GammaEvaluator.\u3007o\u3007(n, gradientColor.\u3007o00\u3007\u3007Oo[i], gradientColor2.\u3007o00\u3007\u3007Oo[i]);
            }
            return;
        }
        final StringBuilder sb = new StringBuilder();
        sb.append("Cannot interpolate between gradients. Lengths vary (");
        sb.append(gradientColor.\u3007o00\u3007\u3007Oo.length);
        sb.append(" vs ");
        sb.append(gradientColor2.\u3007o00\u3007\u3007Oo.length);
        sb.append(")");
        throw new IllegalArgumentException(sb.toString());
    }
    
    public int[] \u3007080() {
        return this.\u3007o00\u3007\u3007Oo;
    }
    
    public float[] \u3007o00\u3007\u3007Oo() {
        return this.\u3007080;
    }
    
    public int \u3007o\u3007() {
        return this.\u3007o00\u3007\u3007Oo.length;
    }
}

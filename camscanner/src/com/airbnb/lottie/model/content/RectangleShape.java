// 
// Decompiled by Procyon v0.6.0
// 

package com.airbnb.lottie.model.content;

import com.airbnb.lottie.animation.content.RectangleContent;
import com.airbnb.lottie.animation.content.Content;
import com.airbnb.lottie.model.layer.BaseLayer;
import com.airbnb.lottie.LottieDrawable;
import com.airbnb.lottie.model.animatable.AnimatablePointValue;
import android.graphics.PointF;
import com.airbnb.lottie.model.animatable.AnimatableValue;
import com.airbnb.lottie.model.animatable.AnimatableFloatValue;

public class RectangleShape implements ContentModel
{
    private final AnimatableFloatValue O8;
    private final boolean Oo08;
    private final String \u3007080;
    private final AnimatableValue<PointF, PointF> \u3007o00\u3007\u3007Oo;
    private final AnimatablePointValue \u3007o\u3007;
    
    public RectangleShape(final String \u3007080, final AnimatableValue<PointF, PointF> \u3007o00\u3007\u3007Oo, final AnimatablePointValue \u3007o\u3007, final AnimatableFloatValue o8, final boolean oo08) {
        this.\u3007080 = \u3007080;
        this.\u3007o00\u3007\u3007Oo = \u3007o00\u3007\u3007Oo;
        this.\u3007o\u3007 = \u3007o\u3007;
        this.O8 = o8;
        this.Oo08 = oo08;
    }
    
    public AnimatableValue<PointF, PointF> O8() {
        return this.\u3007o00\u3007\u3007Oo;
    }
    
    public AnimatablePointValue Oo08() {
        return this.\u3007o\u3007;
    }
    
    public boolean o\u30070() {
        return this.Oo08;
    }
    
    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append("RectangleShape{position=");
        sb.append(this.\u3007o00\u3007\u3007Oo);
        sb.append(", size=");
        sb.append(this.\u3007o\u3007);
        sb.append('}');
        return sb.toString();
    }
    
    @Override
    public Content \u3007080(final LottieDrawable lottieDrawable, final BaseLayer baseLayer) {
        return new RectangleContent(lottieDrawable, baseLayer, this);
    }
    
    public AnimatableFloatValue \u3007o00\u3007\u3007Oo() {
        return this.O8;
    }
    
    public String \u3007o\u3007() {
        return this.\u3007080;
    }
}

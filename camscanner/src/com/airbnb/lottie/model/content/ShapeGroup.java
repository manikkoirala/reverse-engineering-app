// 
// Decompiled by Procyon v0.6.0
// 

package com.airbnb.lottie.model.content;

import com.airbnb.lottie.animation.content.ContentGroup;
import com.airbnb.lottie.animation.content.Content;
import com.airbnb.lottie.model.layer.BaseLayer;
import com.airbnb.lottie.LottieDrawable;
import java.util.Arrays;
import java.util.List;

public class ShapeGroup implements ContentModel
{
    private final String \u3007080;
    private final List<ContentModel> \u3007o00\u3007\u3007Oo;
    private final boolean \u3007o\u3007;
    
    public ShapeGroup(final String \u3007080, final List<ContentModel> \u3007o00\u3007\u3007Oo, final boolean \u3007o\u3007) {
        this.\u3007080 = \u3007080;
        this.\u3007o00\u3007\u3007Oo = \u3007o00\u3007\u3007Oo;
        this.\u3007o\u3007 = \u3007o\u3007;
    }
    
    public boolean O8() {
        return this.\u3007o\u3007;
    }
    
    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append("ShapeGroup{name='");
        sb.append(this.\u3007080);
        sb.append("' Shapes: ");
        sb.append(Arrays.toString(this.\u3007o00\u3007\u3007Oo.toArray()));
        sb.append('}');
        return sb.toString();
    }
    
    @Override
    public Content \u3007080(final LottieDrawable lottieDrawable, final BaseLayer baseLayer) {
        return new ContentGroup(lottieDrawable, baseLayer, this);
    }
    
    public List<ContentModel> \u3007o00\u3007\u3007Oo() {
        return this.\u3007o00\u3007\u3007Oo;
    }
    
    public String \u3007o\u3007() {
        return this.\u3007080;
    }
}

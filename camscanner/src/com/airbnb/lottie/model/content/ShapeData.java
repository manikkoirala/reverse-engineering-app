// 
// Decompiled by Procyon v0.6.0
// 

package com.airbnb.lottie.model.content;

import com.airbnb.lottie.utils.MiscUtils;
import com.airbnb.lottie.utils.Logger;
import androidx.annotation.FloatRange;
import java.util.Collection;
import java.util.ArrayList;
import android.graphics.PointF;
import com.airbnb.lottie.model.CubicCurveData;
import java.util.List;

public class ShapeData
{
    private final List<CubicCurveData> \u3007080;
    private PointF \u3007o00\u3007\u3007Oo;
    private boolean \u3007o\u3007;
    
    public ShapeData() {
        this.\u3007080 = new ArrayList<CubicCurveData>();
    }
    
    public ShapeData(final PointF \u3007o00\u3007\u3007Oo, final boolean \u3007o\u3007, final List<CubicCurveData> c) {
        this.\u3007o00\u3007\u3007Oo = \u3007o00\u3007\u3007Oo;
        this.\u3007o\u3007 = \u3007o\u3007;
        this.\u3007080 = new ArrayList<CubicCurveData>(c);
    }
    
    private void Oo08(final float n, final float n2) {
        if (this.\u3007o00\u3007\u3007Oo == null) {
            this.\u3007o00\u3007\u3007Oo = new PointF();
        }
        this.\u3007o00\u3007\u3007Oo.set(n, n2);
    }
    
    public boolean O8() {
        return this.\u3007o\u3007;
    }
    
    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append("ShapeData{numCurves=");
        sb.append(this.\u3007080.size());
        sb.append("closed=");
        sb.append(this.\u3007o\u3007);
        sb.append('}');
        return sb.toString();
    }
    
    public List<CubicCurveData> \u3007080() {
        return this.\u3007080;
    }
    
    public PointF \u3007o00\u3007\u3007Oo() {
        return this.\u3007o00\u3007\u3007Oo;
    }
    
    public void \u3007o\u3007(final ShapeData shapeData, final ShapeData shapeData2, @FloatRange(from = 0.0, to = 1.0) final float n) {
        if (this.\u3007o00\u3007\u3007Oo == null) {
            this.\u3007o00\u3007\u3007Oo = new PointF();
        }
        this.\u3007o\u3007 = (shapeData.O8() || shapeData2.O8());
        if (shapeData.\u3007080().size() != shapeData2.\u3007080().size()) {
            final StringBuilder sb = new StringBuilder();
            sb.append("Curves must have the same number of control points. Shape 1: ");
            sb.append(shapeData.\u3007080().size());
            sb.append("\tShape 2: ");
            sb.append(shapeData2.\u3007080().size());
            Logger.\u3007o\u3007(sb.toString());
        }
        final int min = Math.min(shapeData.\u3007080().size(), shapeData2.\u3007080().size());
        if (this.\u3007080.size() < min) {
            for (int i = this.\u3007080.size(); i < min; ++i) {
                this.\u3007080.add(new CubicCurveData());
            }
        }
        else if (this.\u3007080.size() > min) {
            for (int j = this.\u3007080.size() - 1; j >= min; --j) {
                final List<CubicCurveData> \u3007080 = this.\u3007080;
                \u3007080.remove(\u3007080.size() - 1);
            }
        }
        final PointF \u3007o00\u3007\u3007Oo = shapeData.\u3007o00\u3007\u3007Oo();
        final PointF \u3007o00\u3007\u3007Oo2 = shapeData2.\u3007o00\u3007\u3007Oo();
        this.Oo08(MiscUtils.OO0o\u3007\u3007\u3007\u30070(\u3007o00\u3007\u3007Oo.x, \u3007o00\u3007\u3007Oo2.x, n), MiscUtils.OO0o\u3007\u3007\u3007\u30070(\u3007o00\u3007\u3007Oo.y, \u3007o00\u3007\u3007Oo2.y, n));
        for (int k = this.\u3007080.size() - 1; k >= 0; --k) {
            final CubicCurveData cubicCurveData = shapeData.\u3007080().get(k);
            final CubicCurveData cubicCurveData2 = shapeData2.\u3007080().get(k);
            final PointF \u300781 = cubicCurveData.\u3007080();
            final PointF \u3007o00\u3007\u3007Oo3 = cubicCurveData.\u3007o00\u3007\u3007Oo();
            final PointF \u3007o\u3007 = cubicCurveData.\u3007o\u3007();
            final PointF \u300782 = cubicCurveData2.\u3007080();
            final PointF \u3007o00\u3007\u3007Oo4 = cubicCurveData2.\u3007o00\u3007\u3007Oo();
            final PointF \u3007o\u30072 = cubicCurveData2.\u3007o\u3007();
            this.\u3007080.get(k).O8(MiscUtils.OO0o\u3007\u3007\u3007\u30070(\u300781.x, \u300782.x, n), MiscUtils.OO0o\u3007\u3007\u3007\u30070(\u300781.y, \u300782.y, n));
            this.\u3007080.get(k).Oo08(MiscUtils.OO0o\u3007\u3007\u3007\u30070(\u3007o00\u3007\u3007Oo3.x, \u3007o00\u3007\u3007Oo4.x, n), MiscUtils.OO0o\u3007\u3007\u3007\u30070(\u3007o00\u3007\u3007Oo3.y, \u3007o00\u3007\u3007Oo4.y, n));
            this.\u3007080.get(k).o\u30070(MiscUtils.OO0o\u3007\u3007\u3007\u30070(\u3007o\u3007.x, \u3007o\u30072.x, n), MiscUtils.OO0o\u3007\u3007\u3007\u30070(\u3007o\u3007.y, \u3007o\u30072.y, n));
        }
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.airbnb.lottie.model.content;

import com.airbnb.lottie.animation.content.TrimPathContent;
import com.airbnb.lottie.animation.content.Content;
import com.airbnb.lottie.model.layer.BaseLayer;
import com.airbnb.lottie.LottieDrawable;
import com.airbnb.lottie.model.animatable.AnimatableFloatValue;

public class ShapeTrimPath implements ContentModel
{
    private final AnimatableFloatValue O8;
    private final AnimatableFloatValue Oo08;
    private final boolean o\u30070;
    private final String \u3007080;
    private final Type \u3007o00\u3007\u3007Oo;
    private final AnimatableFloatValue \u3007o\u3007;
    
    public ShapeTrimPath(final String \u3007080, final Type \u3007o00\u3007\u3007Oo, final AnimatableFloatValue \u3007o\u3007, final AnimatableFloatValue o8, final AnimatableFloatValue oo08, final boolean o\u30070) {
        this.\u3007080 = \u3007080;
        this.\u3007o00\u3007\u3007Oo = \u3007o00\u3007\u3007Oo;
        this.\u3007o\u3007 = \u3007o\u3007;
        this.O8 = o8;
        this.Oo08 = oo08;
        this.o\u30070 = o\u30070;
    }
    
    public AnimatableFloatValue O8() {
        return this.Oo08;
    }
    
    public AnimatableFloatValue Oo08() {
        return this.\u3007o\u3007;
    }
    
    public Type getType() {
        return this.\u3007o00\u3007\u3007Oo;
    }
    
    public boolean o\u30070() {
        return this.o\u30070;
    }
    
    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append("Trim Path: {start: ");
        sb.append(this.\u3007o\u3007);
        sb.append(", end: ");
        sb.append(this.O8);
        sb.append(", offset: ");
        sb.append(this.Oo08);
        sb.append("}");
        return sb.toString();
    }
    
    @Override
    public Content \u3007080(final LottieDrawable lottieDrawable, final BaseLayer baseLayer) {
        return new TrimPathContent(baseLayer, this);
    }
    
    public AnimatableFloatValue \u3007o00\u3007\u3007Oo() {
        return this.O8;
    }
    
    public String \u3007o\u3007() {
        return this.\u3007080;
    }
    
    public enum Type
    {
        private static final Type[] $VALUES;
        
        INDIVIDUALLY, 
        SIMULTANEOUSLY;
        
        public static Type forId(final int i) {
            if (i == 1) {
                return Type.SIMULTANEOUSLY;
            }
            if (i == 2) {
                return Type.INDIVIDUALLY;
            }
            final StringBuilder sb = new StringBuilder();
            sb.append("Unknown trim path type ");
            sb.append(i);
            throw new IllegalArgumentException(sb.toString());
        }
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.airbnb.lottie.model.content;

import com.airbnb.lottie.animation.content.GradientFillContent;
import com.airbnb.lottie.animation.content.Content;
import com.airbnb.lottie.model.layer.BaseLayer;
import com.airbnb.lottie.LottieDrawable;
import com.airbnb.lottie.model.animatable.AnimatableGradientColorValue;
import android.graphics.Path$FillType;
import androidx.annotation.Nullable;
import com.airbnb.lottie.model.animatable.AnimatableFloatValue;
import com.airbnb.lottie.model.animatable.AnimatablePointValue;
import com.airbnb.lottie.model.animatable.AnimatableIntegerValue;

public class GradientFill implements ContentModel
{
    private final AnimatableIntegerValue O8;
    private final boolean OO0o\u3007\u3007\u3007\u30070;
    private final AnimatablePointValue Oo08;
    @Nullable
    private final AnimatableFloatValue oO80;
    private final AnimatablePointValue o\u30070;
    private final GradientType \u3007080;
    @Nullable
    private final AnimatableFloatValue \u300780\u3007808\u3007O;
    private final Path$FillType \u3007o00\u3007\u3007Oo;
    private final AnimatableGradientColorValue \u3007o\u3007;
    private final String \u3007\u3007888;
    
    public GradientFill(final String \u3007\u3007888, final GradientType \u3007080, final Path$FillType \u3007o00\u3007\u3007Oo, final AnimatableGradientColorValue \u3007o\u3007, final AnimatableIntegerValue o8, final AnimatablePointValue oo08, final AnimatablePointValue o\u30070, final AnimatableFloatValue oo9, final AnimatableFloatValue \u300780\u3007808\u3007O, final boolean oo0o\u3007\u3007\u3007\u30070) {
        this.\u3007080 = \u3007080;
        this.\u3007o00\u3007\u3007Oo = \u3007o00\u3007\u3007Oo;
        this.\u3007o\u3007 = \u3007o\u3007;
        this.O8 = o8;
        this.Oo08 = oo08;
        this.o\u30070 = o\u30070;
        this.\u3007\u3007888 = \u3007\u3007888;
        this.oO80 = oo9;
        this.\u300780\u3007808\u3007O = \u300780\u3007808\u3007O;
        this.OO0o\u3007\u3007\u3007\u30070 = oo0o\u3007\u3007\u3007\u30070;
    }
    
    public AnimatableGradientColorValue O8() {
        return this.\u3007o\u3007;
    }
    
    public GradientType Oo08() {
        return this.\u3007080;
    }
    
    public AnimatablePointValue oO80() {
        return this.Oo08;
    }
    
    public String o\u30070() {
        return this.\u3007\u3007888;
    }
    
    @Override
    public Content \u3007080(final LottieDrawable lottieDrawable, final BaseLayer baseLayer) {
        return new GradientFillContent(lottieDrawable, baseLayer, this);
    }
    
    public boolean \u300780\u3007808\u3007O() {
        return this.OO0o\u3007\u3007\u3007\u30070;
    }
    
    public AnimatablePointValue \u3007o00\u3007\u3007Oo() {
        return this.o\u30070;
    }
    
    public Path$FillType \u3007o\u3007() {
        return this.\u3007o00\u3007\u3007Oo;
    }
    
    public AnimatableIntegerValue \u3007\u3007888() {
        return this.O8;
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.airbnb.lottie.model.content;

import android.graphics.Paint$Join;
import android.graphics.Paint$Cap;
import com.airbnb.lottie.animation.content.StrokeContent;
import com.airbnb.lottie.animation.content.Content;
import com.airbnb.lottie.model.layer.BaseLayer;
import com.airbnb.lottie.LottieDrawable;
import java.util.List;
import androidx.annotation.Nullable;
import com.airbnb.lottie.model.animatable.AnimatableFloatValue;
import com.airbnb.lottie.model.animatable.AnimatableIntegerValue;
import com.airbnb.lottie.model.animatable.AnimatableColorValue;

public class ShapeStroke implements ContentModel
{
    private final AnimatableColorValue O8;
    private final boolean OO0o\u3007\u3007\u3007\u30070;
    private final AnimatableIntegerValue Oo08;
    private final LineJoinType oO80;
    private final AnimatableFloatValue o\u30070;
    private final String \u3007080;
    private final float \u300780\u3007808\u3007O;
    @Nullable
    private final AnimatableFloatValue \u3007o00\u3007\u3007Oo;
    private final List<AnimatableFloatValue> \u3007o\u3007;
    private final LineCapType \u3007\u3007888;
    
    public ShapeStroke(final String \u3007080, @Nullable final AnimatableFloatValue \u3007o00\u3007\u3007Oo, final List<AnimatableFloatValue> \u3007o\u3007, final AnimatableColorValue o8, final AnimatableIntegerValue oo08, final AnimatableFloatValue o\u30070, final LineCapType \u3007\u3007888, final LineJoinType oo9, final float \u300780\u3007808\u3007O, final boolean oo0o\u3007\u3007\u3007\u30070) {
        this.\u3007080 = \u3007080;
        this.\u3007o00\u3007\u3007Oo = \u3007o00\u3007\u3007Oo;
        this.\u3007o\u3007 = \u3007o\u3007;
        this.O8 = o8;
        this.Oo08 = oo08;
        this.o\u30070 = o\u30070;
        this.\u3007\u3007888 = \u3007\u3007888;
        this.oO80 = oo9;
        this.\u300780\u3007808\u3007O = \u300780\u3007808\u3007O;
        this.OO0o\u3007\u3007\u3007\u30070 = oo0o\u3007\u3007\u3007\u30070;
    }
    
    public AnimatableFloatValue O8() {
        return this.\u3007o00\u3007\u3007Oo;
    }
    
    public AnimatableFloatValue OO0o\u3007\u3007\u3007\u30070() {
        return this.o\u30070;
    }
    
    public LineJoinType Oo08() {
        return this.oO80;
    }
    
    public String oO80() {
        return this.\u3007080;
    }
    
    public List<AnimatableFloatValue> o\u30070() {
        return this.\u3007o\u3007;
    }
    
    @Override
    public Content \u3007080(final LottieDrawable lottieDrawable, final BaseLayer baseLayer) {
        return new StrokeContent(lottieDrawable, baseLayer, this);
    }
    
    public AnimatableIntegerValue \u300780\u3007808\u3007O() {
        return this.Oo08;
    }
    
    public boolean \u30078o8o\u3007() {
        return this.OO0o\u3007\u3007\u3007\u30070;
    }
    
    public LineCapType \u3007o00\u3007\u3007Oo() {
        return this.\u3007\u3007888;
    }
    
    public AnimatableColorValue \u3007o\u3007() {
        return this.O8;
    }
    
    public float \u3007\u3007888() {
        return this.\u300780\u3007808\u3007O;
    }
    
    public enum LineCapType
    {
        private static final LineCapType[] $VALUES;
        
        BUTT, 
        ROUND, 
        UNKNOWN;
        
        public Paint$Cap toPaintCap() {
            final int n = ShapeStroke$1.\u3007080[this.ordinal()];
            if (n == 1) {
                return Paint$Cap.BUTT;
            }
            if (n != 2) {
                return Paint$Cap.SQUARE;
            }
            return Paint$Cap.ROUND;
        }
    }
    
    public enum LineJoinType
    {
        private static final LineJoinType[] $VALUES;
        
        BEVEL, 
        MITER, 
        ROUND;
        
        public Paint$Join toPaintJoin() {
            final int n = ShapeStroke$1.\u3007o00\u3007\u3007Oo[this.ordinal()];
            if (n == 1) {
                return Paint$Join.BEVEL;
            }
            if (n == 2) {
                return Paint$Join.MITER;
            }
            if (n != 3) {
                return null;
            }
            return Paint$Join.ROUND;
        }
    }
}

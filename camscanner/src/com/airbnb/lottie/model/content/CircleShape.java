// 
// Decompiled by Procyon v0.6.0
// 

package com.airbnb.lottie.model.content;

import com.airbnb.lottie.animation.content.EllipseContent;
import com.airbnb.lottie.animation.content.Content;
import com.airbnb.lottie.model.layer.BaseLayer;
import com.airbnb.lottie.LottieDrawable;
import com.airbnb.lottie.model.animatable.AnimatablePointValue;
import android.graphics.PointF;
import com.airbnb.lottie.model.animatable.AnimatableValue;

public class CircleShape implements ContentModel
{
    private final boolean O8;
    private final boolean Oo08;
    private final String \u3007080;
    private final AnimatableValue<PointF, PointF> \u3007o00\u3007\u3007Oo;
    private final AnimatablePointValue \u3007o\u3007;
    
    public CircleShape(final String \u3007080, final AnimatableValue<PointF, PointF> \u3007o00\u3007\u3007Oo, final AnimatablePointValue \u3007o\u3007, final boolean o8, final boolean oo08) {
        this.\u3007080 = \u3007080;
        this.\u3007o00\u3007\u3007Oo = \u3007o00\u3007\u3007Oo;
        this.\u3007o\u3007 = \u3007o\u3007;
        this.O8 = o8;
        this.Oo08 = oo08;
    }
    
    public AnimatablePointValue O8() {
        return this.\u3007o\u3007;
    }
    
    public boolean Oo08() {
        return this.Oo08;
    }
    
    public boolean o\u30070() {
        return this.O8;
    }
    
    @Override
    public Content \u3007080(final LottieDrawable lottieDrawable, final BaseLayer baseLayer) {
        return new EllipseContent(lottieDrawable, baseLayer, this);
    }
    
    public String \u3007o00\u3007\u3007Oo() {
        return this.\u3007080;
    }
    
    public AnimatableValue<PointF, PointF> \u3007o\u3007() {
        return this.\u3007o00\u3007\u3007Oo;
    }
}

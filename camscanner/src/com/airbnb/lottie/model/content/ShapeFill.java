// 
// Decompiled by Procyon v0.6.0
// 

package com.airbnb.lottie.model.content;

import com.airbnb.lottie.animation.content.FillContent;
import com.airbnb.lottie.animation.content.Content;
import com.airbnb.lottie.model.layer.BaseLayer;
import com.airbnb.lottie.LottieDrawable;
import android.graphics.Path$FillType;
import com.airbnb.lottie.model.animatable.AnimatableIntegerValue;
import androidx.annotation.Nullable;
import com.airbnb.lottie.model.animatable.AnimatableColorValue;

public class ShapeFill implements ContentModel
{
    @Nullable
    private final AnimatableColorValue O8;
    @Nullable
    private final AnimatableIntegerValue Oo08;
    private final boolean o\u30070;
    private final boolean \u3007080;
    private final Path$FillType \u3007o00\u3007\u3007Oo;
    private final String \u3007o\u3007;
    
    public ShapeFill(final String \u3007o\u3007, final boolean \u3007080, final Path$FillType \u3007o00\u3007\u3007Oo, @Nullable final AnimatableColorValue o8, @Nullable final AnimatableIntegerValue oo08, final boolean o\u30070) {
        this.\u3007o\u3007 = \u3007o\u3007;
        this.\u3007080 = \u3007080;
        this.\u3007o00\u3007\u3007Oo = \u3007o00\u3007\u3007Oo;
        this.O8 = o8;
        this.Oo08 = oo08;
        this.o\u30070 = o\u30070;
    }
    
    public String O8() {
        return this.\u3007o\u3007;
    }
    
    @Nullable
    public AnimatableIntegerValue Oo08() {
        return this.Oo08;
    }
    
    public boolean o\u30070() {
        return this.o\u30070;
    }
    
    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append("ShapeFill{color=, fillEnabled=");
        sb.append(this.\u3007080);
        sb.append('}');
        return sb.toString();
    }
    
    @Override
    public Content \u3007080(final LottieDrawable lottieDrawable, final BaseLayer baseLayer) {
        return new FillContent(lottieDrawable, baseLayer, this);
    }
    
    @Nullable
    public AnimatableColorValue \u3007o00\u3007\u3007Oo() {
        return this.O8;
    }
    
    public Path$FillType \u3007o\u3007() {
        return this.\u3007o00\u3007\u3007Oo;
    }
}

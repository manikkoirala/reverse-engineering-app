// 
// Decompiled by Procyon v0.6.0
// 

package com.airbnb.lottie.model.content;

import com.airbnb.lottie.animation.content.PolystarContent;
import com.airbnb.lottie.animation.content.Content;
import com.airbnb.lottie.model.layer.BaseLayer;
import com.airbnb.lottie.LottieDrawable;
import com.airbnb.lottie.model.animatable.AnimatableFloatValue;
import android.graphics.PointF;
import com.airbnb.lottie.model.animatable.AnimatableValue;

public class PolystarShape implements ContentModel
{
    private final AnimatableValue<PointF, PointF> O8;
    private final boolean OO0o\u3007\u3007\u3007\u30070;
    private final AnimatableFloatValue Oo08;
    private final AnimatableFloatValue oO80;
    private final AnimatableFloatValue o\u30070;
    private final String \u3007080;
    private final AnimatableFloatValue \u300780\u3007808\u3007O;
    private final Type \u3007o00\u3007\u3007Oo;
    private final AnimatableFloatValue \u3007o\u3007;
    private final AnimatableFloatValue \u3007\u3007888;
    
    public PolystarShape(final String \u3007080, final Type \u3007o00\u3007\u3007Oo, final AnimatableFloatValue \u3007o\u3007, final AnimatableValue<PointF, PointF> o8, final AnimatableFloatValue oo08, final AnimatableFloatValue o\u30070, final AnimatableFloatValue \u3007\u3007888, final AnimatableFloatValue oo9, final AnimatableFloatValue \u300780\u3007808\u3007O, final boolean oo0o\u3007\u3007\u3007\u30070) {
        this.\u3007080 = \u3007080;
        this.\u3007o00\u3007\u3007Oo = \u3007o00\u3007\u3007Oo;
        this.\u3007o\u3007 = \u3007o\u3007;
        this.O8 = o8;
        this.Oo08 = oo08;
        this.o\u30070 = o\u30070;
        this.\u3007\u3007888 = \u3007\u3007888;
        this.oO80 = oo9;
        this.\u300780\u3007808\u3007O = \u300780\u3007808\u3007O;
        this.OO0o\u3007\u3007\u3007\u30070 = oo0o\u3007\u3007\u3007\u30070;
    }
    
    public String O8() {
        return this.\u3007080;
    }
    
    public boolean OO0o\u3007\u3007\u3007\u30070() {
        return this.OO0o\u3007\u3007\u3007\u30070;
    }
    
    public AnimatableFloatValue Oo08() {
        return this.\u3007\u3007888;
    }
    
    public Type getType() {
        return this.\u3007o00\u3007\u3007Oo;
    }
    
    public AnimatableValue<PointF, PointF> oO80() {
        return this.O8;
    }
    
    public AnimatableFloatValue o\u30070() {
        return this.\u300780\u3007808\u3007O;
    }
    
    @Override
    public Content \u3007080(final LottieDrawable lottieDrawable, final BaseLayer baseLayer) {
        return new PolystarContent(lottieDrawable, baseLayer, this);
    }
    
    public AnimatableFloatValue \u300780\u3007808\u3007O() {
        return this.Oo08;
    }
    
    public AnimatableFloatValue \u3007o00\u3007\u3007Oo() {
        return this.o\u30070;
    }
    
    public AnimatableFloatValue \u3007o\u3007() {
        return this.oO80;
    }
    
    public AnimatableFloatValue \u3007\u3007888() {
        return this.\u3007o\u3007;
    }
    
    public enum Type
    {
        private static final Type[] $VALUES;
        
        POLYGON(2), 
        STAR(1);
        
        private final int value;
        
        private Type(final int value) {
            this.value = value;
        }
        
        public static Type forValue(final int n) {
            for (final Type type : values()) {
                if (type.value == n) {
                    return type;
                }
            }
            return null;
        }
    }
}

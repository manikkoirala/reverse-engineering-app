// 
// Decompiled by Procyon v0.6.0
// 

package com.airbnb.lottie.model;

public class Marker
{
    private static String O8 = "\r";
    private final String \u3007080;
    public final float \u3007o00\u3007\u3007Oo;
    public final float \u3007o\u3007;
    
    public Marker(final String \u3007080, final float \u3007o00\u3007\u3007Oo, final float \u3007o\u3007) {
        this.\u3007080 = \u3007080;
        this.\u3007o\u3007 = \u3007o\u3007;
        this.\u3007o00\u3007\u3007Oo = \u3007o00\u3007\u3007Oo;
    }
    
    public boolean \u3007080(final String s) {
        if (this.\u3007080.equalsIgnoreCase(s)) {
            return true;
        }
        if (this.\u3007080.endsWith(Marker.O8)) {
            final String \u3007080 = this.\u3007080;
            if (\u3007080.substring(0, \u3007080.length() - 1).equalsIgnoreCase(s)) {
                return true;
            }
        }
        return false;
    }
}

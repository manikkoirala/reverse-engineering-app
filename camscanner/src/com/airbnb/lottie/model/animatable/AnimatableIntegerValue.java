// 
// Decompiled by Procyon v0.6.0
// 

package com.airbnb.lottie.model.animatable;

import com.airbnb.lottie.animation.keyframe.IntegerKeyframeAnimation;
import com.airbnb.lottie.animation.keyframe.BaseKeyframeAnimation;
import com.airbnb.lottie.value.Keyframe;
import java.util.List;

public class AnimatableIntegerValue extends BaseAnimatableValue<Integer, Integer>
{
    public AnimatableIntegerValue(final List<Keyframe<Integer>> list) {
        super(list);
    }
    
    @Override
    public BaseKeyframeAnimation<Integer, Integer> \u3007080() {
        return (BaseKeyframeAnimation<Integer, Integer>)new IntegerKeyframeAnimation(super.\u3007080);
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.airbnb.lottie.model.animatable;

import com.airbnb.lottie.value.Keyframe;
import java.util.List;
import com.airbnb.lottie.animation.keyframe.BaseKeyframeAnimation;

public interface AnimatableValue<K, A>
{
    BaseKeyframeAnimation<K, A> \u3007080();
    
    List<Keyframe<K>> \u3007o00\u3007\u3007Oo();
    
    boolean \u3007o\u3007();
}

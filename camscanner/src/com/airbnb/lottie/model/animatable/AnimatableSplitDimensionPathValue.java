// 
// Decompiled by Procyon v0.6.0
// 

package com.airbnb.lottie.model.animatable;

import com.airbnb.lottie.value.Keyframe;
import java.util.List;
import com.airbnb.lottie.animation.keyframe.SplitDimensionPathKeyframeAnimation;
import com.airbnb.lottie.animation.keyframe.BaseKeyframeAnimation;
import android.graphics.PointF;

public class AnimatableSplitDimensionPathValue implements AnimatableValue<PointF, PointF>
{
    private final AnimatableFloatValue \u3007080;
    private final AnimatableFloatValue \u3007o00\u3007\u3007Oo;
    
    public AnimatableSplitDimensionPathValue(final AnimatableFloatValue \u3007080, final AnimatableFloatValue \u3007o00\u3007\u3007Oo) {
        this.\u3007080 = \u3007080;
        this.\u3007o00\u3007\u3007Oo = \u3007o00\u3007\u3007Oo;
    }
    
    @Override
    public BaseKeyframeAnimation<PointF, PointF> \u3007080() {
        return new SplitDimensionPathKeyframeAnimation(this.\u3007080.\u3007080(), this.\u3007o00\u3007\u3007Oo.\u3007080());
    }
    
    @Override
    public List<Keyframe<PointF>> \u3007o00\u3007\u3007Oo() {
        throw new UnsupportedOperationException("Cannot call getKeyframes on AnimatableSplitDimensionPathValue.");
    }
    
    @Override
    public boolean \u3007o\u3007() {
        return this.\u3007080.\u3007o\u3007() && this.\u3007o00\u3007\u3007Oo.\u3007o\u3007();
    }
}

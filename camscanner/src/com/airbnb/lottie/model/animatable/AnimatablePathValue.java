// 
// Decompiled by Procyon v0.6.0
// 

package com.airbnb.lottie.model.animatable;

import com.airbnb.lottie.animation.keyframe.PathKeyframeAnimation;
import com.airbnb.lottie.animation.keyframe.PointKeyframeAnimation;
import com.airbnb.lottie.animation.keyframe.BaseKeyframeAnimation;
import com.airbnb.lottie.value.Keyframe;
import java.util.List;
import android.graphics.PointF;

public class AnimatablePathValue implements AnimatableValue<PointF, PointF>
{
    private final List<Keyframe<PointF>> \u3007080;
    
    public AnimatablePathValue(final List<Keyframe<PointF>> \u3007080) {
        this.\u3007080 = \u3007080;
    }
    
    @Override
    public BaseKeyframeAnimation<PointF, PointF> \u3007080() {
        if (this.\u3007080.get(0).oO80()) {
            return (BaseKeyframeAnimation<PointF, PointF>)new PointKeyframeAnimation(this.\u3007080);
        }
        return (BaseKeyframeAnimation<PointF, PointF>)new PathKeyframeAnimation(this.\u3007080);
    }
    
    @Override
    public List<Keyframe<PointF>> \u3007o00\u3007\u3007Oo() {
        return this.\u3007080;
    }
    
    @Override
    public boolean \u3007o\u3007() {
        final int size = this.\u3007080.size();
        boolean b = false;
        if (size == 1) {
            b = b;
            if (this.\u3007080.get(0).oO80()) {
                b = true;
            }
        }
        return b;
    }
}

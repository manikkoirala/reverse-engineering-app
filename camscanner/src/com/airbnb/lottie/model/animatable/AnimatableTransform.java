// 
// Decompiled by Procyon v0.6.0
// 

package com.airbnb.lottie.model.animatable;

import com.airbnb.lottie.animation.keyframe.TransformKeyframeAnimation;
import com.airbnb.lottie.animation.content.Content;
import com.airbnb.lottie.model.layer.BaseLayer;
import com.airbnb.lottie.LottieDrawable;
import android.graphics.PointF;
import androidx.annotation.Nullable;
import com.airbnb.lottie.model.content.ContentModel;

public class AnimatableTransform implements ContentModel
{
    @Nullable
    private final AnimatableFloatValue O8;
    @Nullable
    private final AnimatableIntegerValue Oo08;
    @Nullable
    private final AnimatableFloatValue oO80;
    @Nullable
    private final AnimatableFloatValue o\u30070;
    @Nullable
    private final AnimatablePathValue \u3007080;
    @Nullable
    private final AnimatableFloatValue \u300780\u3007808\u3007O;
    @Nullable
    private final AnimatableValue<PointF, PointF> \u3007o00\u3007\u3007Oo;
    @Nullable
    private final AnimatableScaleValue \u3007o\u3007;
    @Nullable
    private final AnimatableFloatValue \u3007\u3007888;
    
    public AnimatableTransform() {
        this(null, null, null, null, null, null, null, null, null);
    }
    
    public AnimatableTransform(@Nullable final AnimatablePathValue \u3007080, @Nullable final AnimatableValue<PointF, PointF> \u3007o00\u3007\u3007Oo, @Nullable final AnimatableScaleValue \u3007o\u3007, @Nullable final AnimatableFloatValue o8, @Nullable final AnimatableIntegerValue oo08, @Nullable final AnimatableFloatValue oo9, @Nullable final AnimatableFloatValue \u300780\u3007808\u3007O, @Nullable final AnimatableFloatValue o\u30070, @Nullable final AnimatableFloatValue \u3007\u3007888) {
        this.\u3007080 = \u3007080;
        this.\u3007o00\u3007\u3007Oo = \u3007o00\u3007\u3007Oo;
        this.\u3007o\u3007 = \u3007o\u3007;
        this.O8 = o8;
        this.Oo08 = oo08;
        this.oO80 = oo9;
        this.\u300780\u3007808\u3007O = \u300780\u3007808\u3007O;
        this.o\u30070 = o\u30070;
        this.\u3007\u3007888 = \u3007\u3007888;
    }
    
    @Nullable
    public AnimatableFloatValue O8() {
        return this.\u300780\u3007808\u3007O;
    }
    
    @Nullable
    public AnimatableFloatValue OO0o\u3007\u3007\u3007\u30070() {
        return this.\u3007\u3007888;
    }
    
    @Nullable
    public AnimatableIntegerValue Oo08() {
        return this.Oo08;
    }
    
    @Nullable
    public AnimatableScaleValue oO80() {
        return this.\u3007o\u3007;
    }
    
    @Nullable
    public AnimatableValue<PointF, PointF> o\u30070() {
        return this.\u3007o00\u3007\u3007Oo;
    }
    
    @Nullable
    @Override
    public Content \u3007080(final LottieDrawable lottieDrawable, final BaseLayer baseLayer) {
        return null;
    }
    
    @Nullable
    public AnimatableFloatValue \u300780\u3007808\u3007O() {
        return this.o\u30070;
    }
    
    @Nullable
    public AnimatableFloatValue \u30078o8o\u3007() {
        return this.oO80;
    }
    
    public TransformKeyframeAnimation \u3007o00\u3007\u3007Oo() {
        return new TransformKeyframeAnimation(this);
    }
    
    @Nullable
    public AnimatablePathValue \u3007o\u3007() {
        return this.\u3007080;
    }
    
    @Nullable
    public AnimatableFloatValue \u3007\u3007888() {
        return this.O8;
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.airbnb.lottie.model.animatable;

import androidx.annotation.Nullable;

public class AnimatableTextProperties
{
    @Nullable
    public final AnimatableFloatValue O8;
    @Nullable
    public final AnimatableColorValue \u3007080;
    @Nullable
    public final AnimatableColorValue \u3007o00\u3007\u3007Oo;
    @Nullable
    public final AnimatableFloatValue \u3007o\u3007;
    
    public AnimatableTextProperties(@Nullable final AnimatableColorValue \u3007080, @Nullable final AnimatableColorValue \u3007o00\u3007\u3007Oo, @Nullable final AnimatableFloatValue \u3007o\u3007, @Nullable final AnimatableFloatValue o8) {
        this.\u3007080 = \u3007080;
        this.\u3007o00\u3007\u3007Oo = \u3007o00\u3007\u3007Oo;
        this.\u3007o\u3007 = \u3007o\u3007;
        this.O8 = o8;
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.airbnb.lottie.model.animatable;

import java.util.Arrays;
import java.util.Collections;
import com.airbnb.lottie.value.Keyframe;
import java.util.List;

abstract class BaseAnimatableValue<V, O> implements AnimatableValue<V, O>
{
    final List<Keyframe<V>> \u3007080;
    
    BaseAnimatableValue(final V v) {
        this(Collections.singletonList(new Keyframe<V>(v)));
    }
    
    BaseAnimatableValue(final List<Keyframe<V>> \u3007080) {
        this.\u3007080 = \u3007080;
    }
    
    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        if (!this.\u3007080.isEmpty()) {
            sb.append("values=");
            sb.append(Arrays.toString(this.\u3007080.toArray()));
        }
        return sb.toString();
    }
    
    @Override
    public List<Keyframe<V>> \u3007o00\u3007\u3007Oo() {
        return this.\u3007080;
    }
    
    @Override
    public boolean \u3007o\u3007() {
        final boolean empty = this.\u3007080.isEmpty();
        boolean b = true;
        if (!empty) {
            b = (this.\u3007080.size() == 1 && this.\u3007080.get(0).oO80() && b);
        }
        return b;
    }
}

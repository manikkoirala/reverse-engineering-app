// 
// Decompiled by Procyon v0.6.0
// 

package com.airbnb.lottie.model;

import com.airbnb.lottie.model.content.ShapeGroup;
import java.util.List;
import androidx.annotation.RestrictTo;

@RestrictTo({ RestrictTo.Scope.LIBRARY })
public class FontCharacter
{
    private final double O8;
    private final String Oo08;
    private final String o\u30070;
    private final List<ShapeGroup> \u3007080;
    private final char \u3007o00\u3007\u3007Oo;
    private final double \u3007o\u3007;
    
    public FontCharacter(final List<ShapeGroup> \u3007080, final char \u3007o00\u3007\u3007Oo, final double \u3007o\u3007, final double o8, final String oo08, final String o\u30070) {
        this.\u3007080 = \u3007080;
        this.\u3007o00\u3007\u3007Oo = \u3007o00\u3007\u3007Oo;
        this.\u3007o\u3007 = \u3007o\u3007;
        this.O8 = o8;
        this.Oo08 = oo08;
        this.o\u30070 = o\u30070;
    }
    
    public static int \u3007o\u3007(final char c, final String s, final String s2) {
        return (('\0' + c) * 31 + s.hashCode()) * 31 + s2.hashCode();
    }
    
    @Override
    public int hashCode() {
        return \u3007o\u3007(this.\u3007o00\u3007\u3007Oo, this.o\u30070, this.Oo08);
    }
    
    public List<ShapeGroup> \u3007080() {
        return this.\u3007080;
    }
    
    public double \u3007o00\u3007\u3007Oo() {
        return this.O8;
    }
}

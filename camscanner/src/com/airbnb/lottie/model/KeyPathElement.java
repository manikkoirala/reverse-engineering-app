// 
// Decompiled by Procyon v0.6.0
// 

package com.airbnb.lottie.model;

import java.util.List;
import androidx.annotation.Nullable;
import com.airbnb.lottie.value.LottieValueCallback;
import androidx.annotation.RestrictTo;

@RestrictTo({ RestrictTo.Scope.LIBRARY })
public interface KeyPathElement
{
     <T> void o\u30070(final T p0, @Nullable final LottieValueCallback<T> p1);
    
    void \u3007\u3007888(final KeyPath p0, final int p1, final List<KeyPath> p2, final KeyPath p3);
}

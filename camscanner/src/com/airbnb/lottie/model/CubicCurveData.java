// 
// Decompiled by Procyon v0.6.0
// 

package com.airbnb.lottie.model;

import android.graphics.PointF;
import androidx.annotation.RestrictTo;

@RestrictTo({ RestrictTo.Scope.LIBRARY })
public class CubicCurveData
{
    private final PointF \u3007080;
    private final PointF \u3007o00\u3007\u3007Oo;
    private final PointF \u3007o\u3007;
    
    public CubicCurveData() {
        this.\u3007080 = new PointF();
        this.\u3007o00\u3007\u3007Oo = new PointF();
        this.\u3007o\u3007 = new PointF();
    }
    
    public CubicCurveData(final PointF \u3007080, final PointF \u3007o00\u3007\u3007Oo, final PointF \u3007o\u3007) {
        this.\u3007080 = \u3007080;
        this.\u3007o00\u3007\u3007Oo = \u3007o00\u3007\u3007Oo;
        this.\u3007o\u3007 = \u3007o\u3007;
    }
    
    public void O8(final float n, final float n2) {
        this.\u3007080.set(n, n2);
    }
    
    public void Oo08(final float n, final float n2) {
        this.\u3007o00\u3007\u3007Oo.set(n, n2);
    }
    
    public void o\u30070(final float n, final float n2) {
        this.\u3007o\u3007.set(n, n2);
    }
    
    public PointF \u3007080() {
        return this.\u3007080;
    }
    
    public PointF \u3007o00\u3007\u3007Oo() {
        return this.\u3007o00\u3007\u3007Oo;
    }
    
    public PointF \u3007o\u3007() {
        return this.\u3007o\u3007;
    }
}

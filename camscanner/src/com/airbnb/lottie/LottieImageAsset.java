// 
// Decompiled by Procyon v0.6.0
// 

package com.airbnb.lottie;

import androidx.annotation.RestrictTo;
import androidx.annotation.Nullable;
import android.graphics.Bitmap;

public class LottieImageAsset
{
    private final String O8;
    private final String Oo08;
    @Nullable
    private Bitmap o\u30070;
    private final int \u3007080;
    private final int \u3007o00\u3007\u3007Oo;
    private final String \u3007o\u3007;
    
    @RestrictTo({ RestrictTo.Scope.LIBRARY })
    public LottieImageAsset(final int \u3007080, final int \u3007o00\u3007\u3007Oo, final String \u3007o\u3007, final String o8, final String oo08) {
        this.\u3007080 = \u3007080;
        this.\u3007o00\u3007\u3007Oo = \u3007o00\u3007\u3007Oo;
        this.\u3007o\u3007 = \u3007o\u3007;
        this.O8 = o8;
        this.Oo08 = oo08;
    }
    
    public String O8() {
        return this.\u3007o\u3007;
    }
    
    public int Oo08() {
        return this.\u3007080;
    }
    
    public void o\u30070(@Nullable final Bitmap o\u30070) {
        this.o\u30070 = o\u30070;
    }
    
    @Nullable
    public Bitmap \u3007080() {
        return this.o\u30070;
    }
    
    public String \u3007o00\u3007\u3007Oo() {
        return this.O8;
    }
    
    public int \u3007o\u3007() {
        return this.\u3007o00\u3007\u3007Oo;
    }
}

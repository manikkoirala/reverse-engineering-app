// 
// Decompiled by Procyon v0.6.0
// 

package com.airbnb.lottie;

import android.view.AbsSavedState;
import android.os.Parcel;
import android.os.Parcelable$Creator;
import android.view.View$BaseSavedState;
import androidx.annotation.MainThread;
import android.widget.ImageView$ScaleType;
import android.graphics.Bitmap;
import java.util.Iterator;
import android.graphics.drawable.Drawable$Callback;
import androidx.core.view.ViewCompat;
import android.text.TextUtils;
import android.os.Parcelable;
import android.view.View;
import androidx.annotation.NonNull;
import android.graphics.drawable.Drawable;
import androidx.annotation.FloatRange;
import java.io.InputStream;
import java.io.ByteArrayInputStream;
import android.animation.ValueAnimator$AnimatorUpdateListener;
import android.animation.Animator$AnimatorListener;
import android.graphics.Paint;
import android.os.Build$VERSION;
import android.content.res.TypedArray;
import android.graphics.ColorFilter;
import com.airbnb.lottie.value.LottieValueCallback;
import com.airbnb.lottie.model.KeyPath;
import android.util.AttributeSet;
import java.util.HashSet;
import android.content.Context;
import com.airbnb.lottie.utils.Logger;
import com.airbnb.lottie.utils.Utils;
import java.util.Set;
import androidx.annotation.RawRes;
import androidx.annotation.DrawableRes;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatImageView;

public class LottieAnimationView extends AppCompatImageView
{
    private static final LottieListener<Throwable> \u3007o0O;
    private int O0O;
    private boolean O8o08O8O;
    @Nullable
    private LottieListener<Throwable> OO;
    private boolean OO\u300700\u30078oO;
    private final LottieListener<LottieComposition> o0;
    @Nullable
    private LottieTask<LottieComposition> o8oOOo;
    private boolean o8\u3007OO0\u30070o;
    private boolean oOo0;
    private boolean oOo\u30078o008;
    private RenderMode ooo0\u3007\u3007O;
    private final LottieDrawable o\u300700O;
    private String \u3007080OO8\u30070;
    @DrawableRes
    private int \u300708O\u300700\u3007o;
    @RawRes
    private int \u30070O;
    private boolean \u30078\u3007oO\u3007\u30078o;
    private final LottieListener<Throwable> \u3007OOo8\u30070;
    @Nullable
    private LottieComposition \u3007O\u3007\u3007O8;
    private Set<LottieOnCompositionLoadedListener> \u3007\u300708O;
    
    static {
        \u3007o0O = new LottieListener<Throwable>() {
            public void \u3007080(final Throwable cause) {
                if (Utils.\u30078o8o\u3007(cause)) {
                    Logger.O8("Unable to load composition.", cause);
                    return;
                }
                throw new IllegalStateException("Unable to parse composition", cause);
            }
        };
    }
    
    public LottieAnimationView(final Context context) {
        super(context);
        this.o0 = new LottieListener<LottieComposition>() {
            final LottieAnimationView \u3007080;
            
            public void \u3007080(final LottieComposition composition) {
                this.\u3007080.setComposition(composition);
            }
        };
        this.\u3007OOo8\u30070 = new LottieListener<Throwable>() {
            final LottieAnimationView \u3007080;
            
            public void \u3007080(final Throwable t) {
                if (this.\u3007080.\u300708O\u300700\u3007o != 0) {
                    final LottieAnimationView \u3007080 = this.\u3007080;
                    \u3007080.setImageResource(\u3007080.\u300708O\u300700\u3007o);
                }
                LottieListener lottieListener;
                if (this.\u3007080.OO == null) {
                    lottieListener = LottieAnimationView.\u3007o0O;
                }
                else {
                    lottieListener = this.\u3007080.OO;
                }
                lottieListener.onResult(t);
            }
        };
        this.\u300708O\u300700\u3007o = 0;
        this.o\u300700O = new LottieDrawable();
        this.oOo\u30078o008 = false;
        this.oOo0 = false;
        this.OO\u300700\u30078oO = false;
        this.o8\u3007OO0\u30070o = false;
        this.\u30078\u3007oO\u3007\u30078o = true;
        this.ooo0\u3007\u3007O = RenderMode.AUTOMATIC;
        this.\u3007\u300708O = new HashSet<LottieOnCompositionLoadedListener>();
        this.O0O = 0;
        this.OO0o\u3007\u3007(null);
    }
    
    public LottieAnimationView(final Context context, final AttributeSet set) {
        super(context, set);
        this.o0 = new LottieListener<LottieComposition>() {
            final LottieAnimationView \u3007080;
            
            public void \u3007080(final LottieComposition composition) {
                this.\u3007080.setComposition(composition);
            }
        };
        this.\u3007OOo8\u30070 = new LottieListener<Throwable>() {
            final LottieAnimationView \u3007080;
            
            public void \u3007080(final Throwable t) {
                if (this.\u3007080.\u300708O\u300700\u3007o != 0) {
                    final LottieAnimationView \u3007080 = this.\u3007080;
                    \u3007080.setImageResource(\u3007080.\u300708O\u300700\u3007o);
                }
                LottieListener lottieListener;
                if (this.\u3007080.OO == null) {
                    lottieListener = LottieAnimationView.\u3007o0O;
                }
                else {
                    lottieListener = this.\u3007080.OO;
                }
                lottieListener.onResult(t);
            }
        };
        this.\u300708O\u300700\u3007o = 0;
        this.o\u300700O = new LottieDrawable();
        this.oOo\u30078o008 = false;
        this.oOo0 = false;
        this.OO\u300700\u30078oO = false;
        this.o8\u3007OO0\u30070o = false;
        this.\u30078\u3007oO\u3007\u30078o = true;
        this.ooo0\u3007\u3007O = RenderMode.AUTOMATIC;
        this.\u3007\u300708O = new HashSet<LottieOnCompositionLoadedListener>();
        this.O0O = 0;
        this.OO0o\u3007\u3007(set);
    }
    
    public LottieAnimationView(final Context context, final AttributeSet set, final int n) {
        super(context, set, n);
        this.o0 = new LottieListener<LottieComposition>() {
            final LottieAnimationView \u3007080;
            
            public void \u3007080(final LottieComposition composition) {
                this.\u3007080.setComposition(composition);
            }
        };
        this.\u3007OOo8\u30070 = new LottieListener<Throwable>() {
            final LottieAnimationView \u3007080;
            
            public void \u3007080(final Throwable t) {
                if (this.\u3007080.\u300708O\u300700\u3007o != 0) {
                    final LottieAnimationView \u3007080 = this.\u3007080;
                    \u3007080.setImageResource(\u3007080.\u300708O\u300700\u3007o);
                }
                LottieListener lottieListener;
                if (this.\u3007080.OO == null) {
                    lottieListener = LottieAnimationView.\u3007o0O;
                }
                else {
                    lottieListener = this.\u3007080.OO;
                }
                lottieListener.onResult(t);
            }
        };
        this.\u300708O\u300700\u3007o = 0;
        this.o\u300700O = new LottieDrawable();
        this.oOo\u30078o008 = false;
        this.oOo0 = false;
        this.OO\u300700\u30078oO = false;
        this.o8\u3007OO0\u30070o = false;
        this.\u30078\u3007oO\u3007\u30078o = true;
        this.ooo0\u3007\u3007O = RenderMode.AUTOMATIC;
        this.\u3007\u300708O = new HashSet<LottieOnCompositionLoadedListener>();
        this.O0O = 0;
        this.OO0o\u3007\u3007(set);
    }
    
    private void OO0o\u3007\u3007(@Nullable final AttributeSet set) {
        final TypedArray obtainStyledAttributes = ((View)this).getContext().obtainStyledAttributes(set, R.styleable.LottieAnimationView);
        final boolean inEditMode = ((View)this).isInEditMode();
        boolean b = false;
        if (!inEditMode) {
            this.\u30078\u3007oO\u3007\u30078o = obtainStyledAttributes.getBoolean(R.styleable.LottieAnimationView_lottie_cacheComposition, true);
            final int lottieAnimationView_lottie_rawRes = R.styleable.LottieAnimationView_lottie_rawRes;
            final boolean hasValue = obtainStyledAttributes.hasValue(lottieAnimationView_lottie_rawRes);
            final int lottieAnimationView_lottie_fileName = R.styleable.LottieAnimationView_lottie_fileName;
            final boolean hasValue2 = obtainStyledAttributes.hasValue(lottieAnimationView_lottie_fileName);
            final int lottieAnimationView_lottie_url = R.styleable.LottieAnimationView_lottie_url;
            final boolean hasValue3 = obtainStyledAttributes.hasValue(lottieAnimationView_lottie_url);
            if (hasValue && hasValue2) {
                throw new IllegalArgumentException("lottie_rawRes and lottie_fileName cannot be used at the same time. Please use only one at once.");
            }
            if (hasValue) {
                final int resourceId = obtainStyledAttributes.getResourceId(lottieAnimationView_lottie_rawRes, 0);
                if (resourceId != 0) {
                    this.setAnimation(resourceId);
                }
            }
            else if (hasValue2) {
                final String string = obtainStyledAttributes.getString(lottieAnimationView_lottie_fileName);
                if (string != null) {
                    this.setAnimation(string);
                }
            }
            else if (hasValue3) {
                final String string2 = obtainStyledAttributes.getString(lottieAnimationView_lottie_url);
                if (string2 != null) {
                    this.setAnimationFromUrl(string2);
                }
            }
            this.setFallbackResource(obtainStyledAttributes.getResourceId(R.styleable.LottieAnimationView_lottie_fallbackRes, 0));
        }
        if (obtainStyledAttributes.getBoolean(R.styleable.LottieAnimationView_lottie_autoPlay, false)) {
            this.OO\u300700\u30078oO = true;
            this.o8\u3007OO0\u30070o = true;
        }
        if (obtainStyledAttributes.getBoolean(R.styleable.LottieAnimationView_lottie_loop, false)) {
            this.o\u300700O.\u300780(-1);
        }
        final int lottieAnimationView_lottie_repeatMode = R.styleable.LottieAnimationView_lottie_repeatMode;
        if (obtainStyledAttributes.hasValue(lottieAnimationView_lottie_repeatMode)) {
            this.setRepeatMode(obtainStyledAttributes.getInt(lottieAnimationView_lottie_repeatMode, 1));
        }
        final int lottieAnimationView_lottie_repeatCount = R.styleable.LottieAnimationView_lottie_repeatCount;
        if (obtainStyledAttributes.hasValue(lottieAnimationView_lottie_repeatCount)) {
            this.setRepeatCount(obtainStyledAttributes.getInt(lottieAnimationView_lottie_repeatCount, -1));
        }
        final int lottieAnimationView_lottie_speed = R.styleable.LottieAnimationView_lottie_speed;
        if (obtainStyledAttributes.hasValue(lottieAnimationView_lottie_speed)) {
            this.setSpeed(obtainStyledAttributes.getFloat(lottieAnimationView_lottie_speed, 1.0f));
        }
        this.setImageAssetsFolder(obtainStyledAttributes.getString(R.styleable.LottieAnimationView_lottie_imageAssetsFolder));
        this.setProgress(obtainStyledAttributes.getFloat(R.styleable.LottieAnimationView_lottie_progress, 0.0f));
        this.OO0o\u3007\u3007\u3007\u30070(obtainStyledAttributes.getBoolean(R.styleable.LottieAnimationView_lottie_enableMergePathsForKitKatAndAbove, false));
        final int lottieAnimationView_lottie_colorFilter = R.styleable.LottieAnimationView_lottie_colorFilter;
        if (obtainStyledAttributes.hasValue(lottieAnimationView_lottie_colorFilter)) {
            this.o\u30070(new KeyPath(new String[] { "**" }), LottieProperty.\u3007oOO8O8, new LottieValueCallback<ColorFilter>(new SimpleColorFilter(obtainStyledAttributes.getColor(lottieAnimationView_lottie_colorFilter, 0))));
        }
        final int lottieAnimationView_lottie_scale = R.styleable.LottieAnimationView_lottie_scale;
        if (obtainStyledAttributes.hasValue(lottieAnimationView_lottie_scale)) {
            this.o\u300700O.OOO(obtainStyledAttributes.getFloat(lottieAnimationView_lottie_scale, 1.0f));
        }
        final int lottieAnimationView_lottie_renderMode = R.styleable.LottieAnimationView_lottie_renderMode;
        if (obtainStyledAttributes.hasValue(lottieAnimationView_lottie_renderMode)) {
            final RenderMode automatic = RenderMode.AUTOMATIC;
            int n;
            if ((n = obtainStyledAttributes.getInt(lottieAnimationView_lottie_renderMode, automatic.ordinal())) >= RenderMode.values().length) {
                n = automatic.ordinal();
            }
            this.setRenderMode(RenderMode.values()[n]);
        }
        if (this.getScaleType() != null) {
            this.o\u300700O.ooo\u30078oO(this.getScaleType());
        }
        obtainStyledAttributes.recycle();
        final LottieDrawable o\u300700O = this.o\u300700O;
        if (Utils.o\u30070(((View)this).getContext()) != 0.0f) {
            b = true;
        }
        o\u300700O.OO8oO0o\u3007(b);
        this.\u30078o8o\u3007();
        this.O8o08O8O = true;
    }
    
    private void oO80() {
        final LottieTask<LottieComposition> o8oOOo = this.o8oOOo;
        if (o8oOOo != null) {
            o8oOOo.\u30078o8o\u3007(this.o0);
            this.o8oOOo.OO0o\u3007\u3007\u3007\u30070(this.\u3007OOo8\u30070);
        }
    }
    
    private void setCompositionTask(final LottieTask<LottieComposition> lottieTask) {
        this.\u300780\u3007808\u3007O();
        this.oO80();
        this.o8oOOo = lottieTask.o\u30070(this.o0).Oo08(this.\u3007OOo8\u30070);
    }
    
    private void \u300780\u3007808\u3007O() {
        this.\u3007O\u3007\u3007O8 = null;
        this.o\u300700O.oO80();
    }
    
    private void \u30078o8o\u3007() {
        final int n = LottieAnimationView$5.\u3007080[this.ooo0\u3007\u3007O.ordinal()];
        int n3;
        final int n2 = n3 = 2;
        Label_0099: {
            if (n != 1) {
                if (n != 2 && n == 3) {
                    final LottieComposition \u3007o\u3007\u3007O8 = this.\u3007O\u3007\u3007O8;
                    boolean b = false;
                    if (\u3007o\u3007\u3007O8 == null || !\u3007o\u3007\u3007O8.\u3007O\u3007() || Build$VERSION.SDK_INT >= 28) {
                        final LottieComposition \u3007o\u3007\u3007O9 = this.\u3007O\u3007\u3007O8;
                        if (\u3007o\u3007\u3007O9 == null || \u3007o\u3007\u3007O9.\u3007O8o08O() <= 4) {
                            b = true;
                        }
                    }
                    if (b) {
                        n3 = n2;
                        break Label_0099;
                    }
                }
                n3 = 1;
            }
        }
        if (n3 != ((View)this).getLayerType()) {
            ((View)this).setLayerType(n3, (Paint)null);
        }
    }
    
    public void O8(final Animator$AnimatorListener animator$AnimatorListener) {
        this.o\u300700O.\u3007o\u3007(animator$AnimatorListener);
    }
    
    public void OO0o\u3007\u3007\u3007\u30070(final boolean b) {
        this.o\u300700O.\u3007O8o08O(b);
    }
    
    public void Oo08(final ValueAnimator$AnimatorUpdateListener valueAnimator$AnimatorUpdateListener) {
        this.o\u300700O.O8(valueAnimator$AnimatorUpdateListener);
    }
    
    public void OoO8(final String s, @Nullable final String s2) {
        this.\u30070\u3007O0088o(new ByteArrayInputStream(s.getBytes()), s2);
    }
    
    public boolean Oooo8o0\u3007() {
        return this.o\u300700O.OOO\u3007O0();
    }
    
    public void buildDrawingCache(final boolean b) {
        L.\u3007080("buildDrawingCache");
        ++this.O0O;
        super.buildDrawingCache(b);
        if (this.O0O == 1 && ((View)this).getWidth() > 0 && ((View)this).getHeight() > 0 && ((View)this).getLayerType() == 1 && ((View)this).getDrawingCache(b) == null) {
            this.setRenderMode(RenderMode.HARDWARE);
        }
        --this.O0O;
        L.\u3007o00\u3007\u3007Oo("buildDrawingCache");
    }
    
    @Nullable
    public LottieComposition getComposition() {
        return this.\u3007O\u3007\u3007O8;
    }
    
    public long getDuration() {
        final LottieComposition \u3007o\u3007\u3007O8 = this.\u3007O\u3007\u3007O8;
        long n;
        if (\u3007o\u3007\u3007O8 != null) {
            n = (long)\u3007o\u3007\u3007O8.O8();
        }
        else {
            n = 0L;
        }
        return n;
    }
    
    public int getFrame() {
        return this.o\u300700O.\u3007O00();
    }
    
    @Nullable
    public String getImageAssetsFolder() {
        return this.o\u300700O.OoO8();
    }
    
    public float getMaxFrame() {
        return this.o\u300700O.o800o8O();
    }
    
    public float getMinFrame() {
        return this.o\u300700O.oo88o8O();
    }
    
    @Nullable
    public PerformanceTracker getPerformanceTracker() {
        return this.o\u300700O.\u3007oo\u3007();
    }
    
    @FloatRange(from = 0.0, to = 1.0)
    public float getProgress() {
        return this.o\u300700O.o\u3007O8\u3007\u3007o();
    }
    
    public int getRepeatCount() {
        return this.o\u300700O.\u300700();
    }
    
    public int getRepeatMode() {
        return this.o\u300700O.O\u30078O8\u3007008();
    }
    
    public float getScale() {
        return this.o\u300700O.O8ooOoo\u3007();
    }
    
    public float getSpeed() {
        return this.o\u300700O.\u3007oOO8O8();
    }
    
    public void invalidateDrawable(@NonNull final Drawable drawable) {
        final Drawable drawable2 = this.getDrawable();
        final LottieDrawable o\u300700O = this.o\u300700O;
        if (drawable2 == o\u300700O) {
            super.invalidateDrawable((Drawable)o\u300700O);
        }
        else {
            super.invalidateDrawable(drawable);
        }
    }
    
    protected void onAttachedToWindow() {
        super.onAttachedToWindow();
        if (this.o8\u3007OO0\u30070o || this.OO\u300700\u30078oO) {
            this.\u3007O\u3007();
            this.o8\u3007OO0\u30070o = false;
            this.OO\u300700\u30078oO = false;
        }
        if (Build$VERSION.SDK_INT < 23) {
            this.onVisibilityChanged((View)this, ((View)this).getVisibility());
        }
    }
    
    protected void onDetachedFromWindow() {
        if (this.Oooo8o0\u3007()) {
            this.\u3007\u3007888();
            this.OO\u300700\u30078oO = true;
        }
        super.onDetachedFromWindow();
    }
    
    protected void onRestoreInstanceState(final Parcelable parcelable) {
        if (!(parcelable instanceof SavedState)) {
            super.onRestoreInstanceState(parcelable);
            return;
        }
        final SavedState savedState = (SavedState)parcelable;
        super.onRestoreInstanceState(((AbsSavedState)savedState).getSuperState());
        final String o0 = savedState.o0;
        this.\u3007080OO8\u30070 = o0;
        if (!TextUtils.isEmpty((CharSequence)o0)) {
            this.setAnimation(this.\u3007080OO8\u30070);
        }
        final int \u3007oOo8\u30070 = savedState.\u3007OOo8\u30070;
        if ((this.\u30070O = \u3007oOo8\u30070) != 0) {
            this.setAnimation(\u3007oOo8\u30070);
        }
        this.setProgress(savedState.OO);
        if (savedState.\u300708O\u300700\u3007o) {
            this.\u3007O\u3007();
        }
        this.o\u300700O.\u300708O8o\u30070(savedState.o\u300700O);
        this.setRepeatMode(savedState.O8o08O8O);
        this.setRepeatCount(savedState.\u3007080OO8\u30070);
    }
    
    protected Parcelable onSaveInstanceState() {
        final SavedState savedState = new SavedState(super.onSaveInstanceState());
        savedState.o0 = this.\u3007080OO8\u30070;
        savedState.\u3007OOo8\u30070 = this.\u30070O;
        savedState.OO = this.o\u300700O.o\u3007O8\u3007\u3007o();
        savedState.\u300708O\u300700\u3007o = (this.o\u300700O.OOO\u3007O0() || (!ViewCompat.isAttachedToWindow((View)this) && this.OO\u300700\u30078oO));
        savedState.o\u300700O = this.o\u300700O.OoO8();
        savedState.O8o08O8O = this.o\u300700O.O\u30078O8\u3007008();
        savedState.\u3007080OO8\u30070 = this.o\u300700O.\u300700();
        return (Parcelable)savedState;
    }
    
    protected void onVisibilityChanged(@NonNull final View view, final int n) {
        if (!this.O8o08O8O) {
            return;
        }
        if (((View)this).isShown()) {
            if (this.oOo0) {
                this.\u3007\u30078O0\u30078();
            }
            else if (this.oOo\u30078o008) {
                this.\u3007O\u3007();
            }
            this.oOo0 = false;
            this.oOo\u30078o008 = false;
        }
        else if (this.Oooo8o0\u3007()) {
            this.\u3007\u3007808\u3007();
            this.oOo0 = true;
        }
    }
    
    public <T> void o\u30070(final KeyPath keyPath, final T t, final LottieValueCallback<T> lottieValueCallback) {
        this.o\u300700O.Oo08(keyPath, t, lottieValueCallback);
    }
    
    public void setAnimation(@RawRes final int \u30070O) {
        this.\u30070O = \u30070O;
        this.\u3007080OO8\u30070 = null;
        LottieTask<LottieComposition> compositionTask;
        if (this.\u30078\u3007oO\u3007\u30078o) {
            compositionTask = LottieCompositionFactory.\u3007O8o08O(((View)this).getContext(), \u30070O);
        }
        else {
            compositionTask = LottieCompositionFactory.OO0o\u3007\u3007(((View)this).getContext(), \u30070O, null);
        }
        this.setCompositionTask(compositionTask);
    }
    
    public void setAnimation(final String \u3007080OO8\u30070) {
        this.\u3007080OO8\u30070 = \u3007080OO8\u30070;
        this.\u30070O = 0;
        LottieTask<LottieComposition> compositionTask;
        if (this.\u30078\u3007oO\u3007\u30078o) {
            compositionTask = LottieCompositionFactory.O8(((View)this).getContext(), \u3007080OO8\u30070);
        }
        else {
            compositionTask = LottieCompositionFactory.Oo08(((View)this).getContext(), \u3007080OO8\u30070, null);
        }
        this.setCompositionTask(compositionTask);
    }
    
    @Deprecated
    public void setAnimationFromJson(final String s) {
        this.OoO8(s, null);
    }
    
    public void setAnimationFromUrl(final String s) {
        LottieTask<LottieComposition> compositionTask;
        if (this.\u30078\u3007oO\u3007\u30078o) {
            compositionTask = LottieCompositionFactory.\u3007O\u3007(((View)this).getContext(), s);
        }
        else {
            compositionTask = LottieCompositionFactory.\u3007O00(((View)this).getContext(), s, null);
        }
        this.setCompositionTask(compositionTask);
    }
    
    public void setApplyingOpacityToLayersEnabled(final boolean b) {
        this.o\u300700O.o8(b);
    }
    
    public void setCacheComposition(final boolean \u30078\u3007oO\u3007\u30078o) {
        this.\u30078\u3007oO\u3007\u30078o = \u30078\u3007oO\u3007\u30078o;
    }
    
    public void setComposition(@NonNull final LottieComposition lottieComposition) {
        if (L.\u3007080) {
            final StringBuilder sb = new StringBuilder();
            sb.append("Set Composition \n");
            sb.append(lottieComposition);
        }
        this.o\u300700O.setCallback((Drawable$Callback)this);
        this.\u3007O\u3007\u3007O8 = lottieComposition;
        final boolean oo8Oo00oo = this.o\u300700O.Oo8Oo00oo(lottieComposition);
        this.\u30078o8o\u3007();
        if (this.getDrawable() == this.o\u300700O && !oo8Oo00oo) {
            return;
        }
        this.onVisibilityChanged((View)this, ((View)this).getVisibility());
        ((View)this).requestLayout();
        final Iterator<LottieOnCompositionLoadedListener> iterator = this.\u3007\u300708O.iterator();
        while (iterator.hasNext()) {
            iterator.next().\u3007080(lottieComposition);
        }
    }
    
    public void setFailureListener(@Nullable final LottieListener<Throwable> oo) {
        this.OO = oo;
    }
    
    public void setFallbackResource(@DrawableRes final int \u300708O\u300700\u3007o) {
        this.\u300708O\u300700\u3007o = \u300708O\u300700\u3007o;
    }
    
    public void setFontAssetDelegate(final FontAssetDelegate fontAssetDelegate) {
        this.o\u300700O.\u3007\u3007\u30070\u3007\u30070(fontAssetDelegate);
    }
    
    public void setFrame(final int n) {
        this.o\u300700O.o\u30070OOo\u30070(n);
    }
    
    public void setImageAssetDelegate(final ImageAssetDelegate imageAssetDelegate) {
        this.o\u300700O.\u3007\u30070o(imageAssetDelegate);
    }
    
    public void setImageAssetsFolder(final String s) {
        this.o\u300700O.\u300708O8o\u30070(s);
    }
    
    @Override
    public void setImageBitmap(final Bitmap imageBitmap) {
        this.oO80();
        super.setImageBitmap(imageBitmap);
    }
    
    @Override
    public void setImageDrawable(final Drawable imageDrawable) {
        this.oO80();
        super.setImageDrawable(imageDrawable);
    }
    
    @Override
    public void setImageResource(final int imageResource) {
        this.oO80();
        super.setImageResource(imageResource);
    }
    
    public void setMaxFrame(final int n) {
        this.o\u300700O.oO(n);
    }
    
    public void setMaxFrame(final String s) {
        this.o\u300700O.\u30078(s);
    }
    
    public void setMaxProgress(@FloatRange(from = 0.0, to = 1.0) final float n) {
        this.o\u300700O.O08000(n);
    }
    
    public void setMinAndMaxFrame(final String s) {
        this.o\u300700O.O\u3007O\u3007oO(s);
    }
    
    public void setMinFrame(final int n) {
        this.o\u300700O.o8oO\u3007(n);
    }
    
    public void setMinFrame(final String s) {
        this.o\u300700O.o\u30078oOO88(s);
    }
    
    public void setMinProgress(final float n) {
        this.o\u300700O.o\u3007O(n);
    }
    
    public void setPerformanceTrackingEnabled(final boolean b) {
        this.o\u300700O.oO00OOO(b);
    }
    
    public void setProgress(@FloatRange(from = 0.0, to = 1.0) final float n) {
        this.o\u300700O.O000(n);
    }
    
    public void setRenderMode(final RenderMode ooo0\u3007\u3007O) {
        this.ooo0\u3007\u3007O = ooo0\u3007\u3007O;
        this.\u30078o8o\u3007();
    }
    
    public void setRepeatCount(final int n) {
        this.o\u300700O.\u300780(n);
    }
    
    public void setRepeatMode(final int n) {
        this.o\u300700O.Ooo(n);
    }
    
    public void setSafeMode(final boolean b) {
        this.o\u300700O.\u3007O\u300780o08O(b);
    }
    
    public void setScale(final float n) {
        this.o\u300700O.OOO(n);
        if (this.getDrawable() == this.o\u300700O) {
            this.setImageDrawable(null);
            this.setImageDrawable(this.o\u300700O);
        }
    }
    
    public void setScaleType(final ImageView$ScaleType scaleType) {
        super.setScaleType(scaleType);
        final LottieDrawable o\u300700O = this.o\u300700O;
        if (o\u300700O != null) {
            o\u300700O.ooo\u30078oO(scaleType);
        }
    }
    
    public void setSpeed(final float n) {
        this.o\u300700O.O0o\u3007\u3007Oo(n);
    }
    
    public void setTextDelegate(final TextDelegate textDelegate) {
        this.o\u300700O.o0O0(textDelegate);
    }
    
    public void \u30070\u3007O0088o(final InputStream inputStream, @Nullable final String s) {
        this.setCompositionTask(LottieCompositionFactory.\u3007\u3007888(inputStream, s));
    }
    
    public void \u3007O00() {
        this.o\u300700O.\u3007o();
    }
    
    @MainThread
    public void \u3007O\u3007() {
        if (((View)this).isShown()) {
            this.o\u300700O.\u300700\u30078();
            this.\u30078o8o\u3007();
        }
        else {
            this.oOo\u30078o008 = true;
        }
    }
    
    @MainThread
    public void \u3007\u3007808\u3007() {
        this.o8\u3007OO0\u30070o = false;
        this.OO\u300700\u30078oO = false;
        this.oOo0 = false;
        this.oOo\u30078o008 = false;
        this.o\u300700O.O8\u3007o();
        this.\u30078o8o\u3007();
    }
    
    @MainThread
    public void \u3007\u3007888() {
        this.OO\u300700\u30078oO = false;
        this.oOo0 = false;
        this.oOo\u30078o008 = false;
        this.o\u300700O.\u3007\u3007888();
        this.\u30078o8o\u3007();
    }
    
    @MainThread
    public void \u3007\u30078O0\u30078() {
        if (((View)this).isShown()) {
            this.o\u300700O.o\u30078();
            this.\u30078o8o\u3007();
        }
        else {
            this.oOo\u30078o008 = false;
            this.oOo0 = true;
        }
    }
    
    private static class SavedState extends View$BaseSavedState
    {
        public static final Parcelable$Creator<SavedState> CREATOR;
        int O8o08O8O;
        float OO;
        String o0;
        String o\u300700O;
        int \u3007080OO8\u30070;
        boolean \u300708O\u300700\u3007o;
        int \u3007OOo8\u30070;
        
        static {
            CREATOR = (Parcelable$Creator)new Parcelable$Creator<SavedState>() {
                public SavedState \u3007080(final Parcel parcel) {
                    return new SavedState(parcel);
                }
                
                public SavedState[] \u3007o00\u3007\u3007Oo(final int n) {
                    return new SavedState[n];
                }
            };
        }
        
        private SavedState(final Parcel parcel) {
            super(parcel);
            this.o0 = parcel.readString();
            this.OO = parcel.readFloat();
            final int int1 = parcel.readInt();
            boolean \u300708O\u300700\u3007o = true;
            if (int1 != 1) {
                \u300708O\u300700\u3007o = false;
            }
            this.\u300708O\u300700\u3007o = \u300708O\u300700\u3007o;
            this.o\u300700O = parcel.readString();
            this.O8o08O8O = parcel.readInt();
            this.\u3007080OO8\u30070 = parcel.readInt();
        }
        
        SavedState(final Parcelable parcelable) {
            super(parcelable);
        }
        
        public void writeToParcel(final Parcel parcel, final int n) {
            super.writeToParcel(parcel, n);
            parcel.writeString(this.o0);
            parcel.writeFloat(this.OO);
            parcel.writeInt((int)(this.\u300708O\u300700\u3007o ? 1 : 0));
            parcel.writeString(this.o\u300700O);
            parcel.writeInt(this.O8o08O8O);
            parcel.writeInt(this.\u3007080OO8\u30070);
        }
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.airbnb.lottie;

import android.graphics.ColorFilter;
import com.airbnb.lottie.value.ScaleXY;
import android.graphics.PointF;

public interface LottieProperty
{
    public static final Integer O8 = 4;
    public static final Float O8ooOoo\u3007 = 14.0f;
    public static final Float OO0o\u3007\u3007 = n;
    public static final PointF OO0o\u3007\u3007\u3007\u30070 = new PointF();
    public static final PointF Oo08 = new PointF();
    public static final Float OoO8 = 7.0f;
    public static final Float Oooo8o0\u3007 = n;
    public static final Float O\u30078O8\u3007008 = 13.0f;
    public static final Float o800o8O = 8.0f;
    public static final PointF oO80 = new PointF();
    public static final Float oo88o8O = 10.0f;
    public static final PointF o\u30070 = new PointF();
    public static final Float o\u3007O8\u3007\u3007o = 12.0f;
    public static final Float \u300700 = 12.1f;
    public static final Integer[] \u30070000OOO = new Integer[0];
    public static final Integer \u3007080 = 1;
    public static final Float \u30070\u3007O0088o = 6.0f;
    public static final Float \u300780\u3007808\u3007O;
    public static final ScaleXY \u30078o8o\u3007 = new ScaleXY();
    public static final Float \u3007O00 = 4.0f;
    public static final Float \u3007O888o0o = 9.0f;
    public static final Float \u3007O8o08O = 1.0f;
    public static final Float \u3007O\u3007 = 3.0f;
    public static final Integer \u3007o00\u3007\u3007Oo = 2;
    public static final ColorFilter \u3007oOO8O8 = new ColorFilter();
    public static final Float \u3007oo\u3007 = 11.0f;
    public static final Integer \u3007o\u3007 = 3;
    public static final Float \u3007\u3007808\u3007 = 2.0f;
    public static final PointF \u3007\u3007888 = new PointF();
    public static final Float \u3007\u30078O0\u30078 = 5.0f;
    
    default static {
        final Float n = \u300780\u3007808\u3007O = 0.0f;
    }
}

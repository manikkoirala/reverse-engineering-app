// 
// Decompiled by Procyon v0.6.0
// 

package com.airbnb.lottie.utils;

import com.airbnb.lottie.LottieLogger;

public class Logger
{
    private static LottieLogger \u3007080;
    
    static {
        Logger.\u3007080 = new LogcatLogger();
    }
    
    public static void O8(final String s, final Throwable t) {
        Logger.\u3007080.\u3007o00\u3007\u3007Oo(s, t);
    }
    
    public static void \u3007080(final String s) {
        Logger.\u3007080.debug(s);
    }
    
    public static void \u3007o00\u3007\u3007Oo(final String s, final Throwable t) {
        Logger.\u3007080.error(s, t);
    }
    
    public static void \u3007o\u3007(final String s) {
        Logger.\u3007080.\u3007080(s);
    }
}

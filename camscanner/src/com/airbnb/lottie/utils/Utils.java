// 
// Decompiled by Procyon v0.6.0
// 

package com.airbnb.lottie.utils;

import java.io.Closeable;
import com.airbnb.lottie.animation.keyframe.FloatKeyframeAnimation;
import androidx.annotation.Nullable;
import com.airbnb.lottie.animation.content.TrimPathContent;
import android.graphics.Bitmap;
import java.net.UnknownServiceException;
import java.net.UnknownHostException;
import javax.net.ssl.SSLException;
import java.net.ProtocolException;
import java.io.InterruptedIOException;
import java.nio.channels.ClosedChannelException;
import java.net.SocketException;
import android.provider.Settings$Global;
import android.content.Context;
import android.graphics.Matrix;
import android.os.Build$VERSION;
import com.airbnb.lottie.L;
import android.content.res.Resources;
import android.graphics.Paint;
import android.graphics.RectF;
import android.graphics.Canvas;
import android.graphics.PointF;
import android.graphics.Path;
import android.graphics.PathMeasure;

public final class Utils
{
    private static final float[] O8;
    private static final float Oo08;
    private static float o\u30070;
    private static final PathMeasure \u3007080;
    private static final Path \u3007o00\u3007\u3007Oo;
    private static final Path \u3007o\u3007;
    
    static {
        \u3007080 = new PathMeasure();
        \u3007o00\u3007\u3007Oo = new Path();
        \u3007o\u3007 = new Path();
        O8 = new float[4];
        Oo08 = (float)(Math.sqrt(2.0) / 2.0);
        Utils.o\u30070 = -1.0f;
    }
    
    public static Path O8(final PointF pointF, final PointF pointF2, final PointF pointF3, final PointF pointF4) {
        final Path path = new Path();
        path.moveTo(pointF.x, pointF.y);
        if (pointF3 != null && pointF4 != null && (pointF3.length() != 0.0f || pointF4.length() != 0.0f)) {
            final float x = pointF.x;
            final float x2 = pointF3.x;
            final float y = pointF.y;
            final float y2 = pointF3.y;
            final float x3 = pointF2.x;
            final float x4 = pointF4.x;
            final float y3 = pointF2.y;
            path.cubicTo(x2 + x, y + y2, x3 + x4, y3 + pointF4.y, x3, y3);
        }
        else {
            path.lineTo(pointF2.x, pointF2.y);
        }
        return path;
    }
    
    public static void OO0o\u3007\u3007(final Canvas canvas, final RectF rectF, final Paint paint) {
        Oooo8o0\u3007(canvas, rectF, paint, 31);
    }
    
    public static boolean OO0o\u3007\u3007\u3007\u30070(final int n, final int n2, final int n3, final int n4, final int n5, final int n6) {
        boolean b = false;
        if (n < n4) {
            return false;
        }
        if (n > n4) {
            return true;
        }
        if (n2 < n5) {
            return false;
        }
        if (n2 > n5) {
            return true;
        }
        if (n3 >= n6) {
            b = true;
        }
        return b;
    }
    
    public static float Oo08() {
        if (Utils.o\u30070 == -1.0f) {
            Utils.o\u30070 = Resources.getSystem().getDisplayMetrics().density;
        }
        return Utils.o\u30070;
    }
    
    public static void Oooo8o0\u3007(final Canvas canvas, final RectF rectF, final Paint paint, final int n) {
        L.\u3007080("Utils#saveLayer");
        if (Build$VERSION.SDK_INT < 23) {
            canvas.saveLayer(rectF, paint, n);
        }
        else {
            canvas.saveLayer(rectF, paint);
        }
        L.\u3007o00\u3007\u3007Oo("Utils#saveLayer");
    }
    
    public static boolean oO80(final Matrix matrix) {
        final float[] o8 = Utils.O8;
        o8[1] = (o8[0] = 0.0f);
        o8[2] = 37394.73f;
        o8[3] = 39575.234f;
        matrix.mapPoints(o8);
        return o8[0] == o8[2] || o8[1] == o8[3];
    }
    
    public static float o\u30070(final Context context) {
        return Settings$Global.getFloat(context.getContentResolver(), "animator_duration_scale", 1.0f);
    }
    
    public static void \u3007080(final Path path, float min, float n, float n2) {
        L.\u3007080("applyTrimPathIfNeeded");
        final PathMeasure \u3007080 = Utils.\u3007080;
        \u3007080.setPath(path, false);
        final float length = \u3007080.getLength();
        if (min == 1.0f && n == 0.0f) {
            L.\u3007o00\u3007\u3007Oo("applyTrimPathIfNeeded");
            return;
        }
        if (length < 1.0f || Math.abs(n - min - 1.0f) < 0.01) {
            L.\u3007o00\u3007\u3007Oo("applyTrimPathIfNeeded");
            return;
        }
        final float n3 = min * length;
        n *= length;
        min = Math.min(n3, n);
        final float max = Math.max(n3, n);
        n2 *= length;
        n = min + n2;
        final float n4 = max + n2;
        n2 = n;
        min = n4;
        if (n >= length) {
            n2 = n;
            min = n4;
            if (n4 >= length) {
                n2 = (float)MiscUtils.o\u30070(n, length);
                min = (float)MiscUtils.o\u30070(n4, length);
            }
        }
        n = n2;
        if (n2 < 0.0f) {
            n = (float)MiscUtils.o\u30070(n2, length);
        }
        n2 = min;
        if (min < 0.0f) {
            n2 = (float)MiscUtils.o\u30070(min, length);
        }
        final float n5 = fcmpl(n, n2);
        if (n5 == 0) {
            path.reset();
            L.\u3007o00\u3007\u3007Oo("applyTrimPathIfNeeded");
            return;
        }
        min = n;
        if (n5 >= 0) {
            min = n - length;
        }
        final Path \u3007o00\u3007\u3007Oo = Utils.\u3007o00\u3007\u3007Oo;
        \u3007o00\u3007\u3007Oo.reset();
        \u3007080.getSegment(min, n2, \u3007o00\u3007\u3007Oo, true);
        if (n2 > length) {
            final Path \u3007o\u3007 = Utils.\u3007o\u3007;
            \u3007o\u3007.reset();
            \u3007080.getSegment(0.0f, n2 % length, \u3007o\u3007, true);
            \u3007o00\u3007\u3007Oo.addPath(\u3007o\u3007);
        }
        else if (min < 0.0f) {
            final Path \u3007o\u30072 = Utils.\u3007o\u3007;
            \u3007o\u30072.reset();
            \u3007080.getSegment(min + length, length, \u3007o\u30072, true);
            \u3007o00\u3007\u3007Oo.addPath(\u3007o\u30072);
        }
        path.set(\u3007o00\u3007\u3007Oo);
        L.\u3007o00\u3007\u3007Oo("applyTrimPathIfNeeded");
    }
    
    public static int \u300780\u3007808\u3007O(final float n, final float n2, final float n3, final float n4) {
        int n5;
        if (n != 0.0f) {
            n5 = (int)(527 * n);
        }
        else {
            n5 = 17;
        }
        int n6 = n5;
        if (n2 != 0.0f) {
            n6 = (int)(n5 * 31 * n2);
        }
        int n7 = n6;
        if (n3 != 0.0f) {
            n7 = (int)(n6 * 31 * n3);
        }
        int n8 = n7;
        if (n4 != 0.0f) {
            n8 = (int)(n7 * 31 * n4);
        }
        return n8;
    }
    
    public static boolean \u30078o8o\u3007(final Throwable t) {
        return t instanceof SocketException || t instanceof ClosedChannelException || t instanceof InterruptedIOException || t instanceof ProtocolException || t instanceof SSLException || t instanceof UnknownHostException || t instanceof UnknownServiceException;
    }
    
    public static Bitmap \u3007O8o08O(final Bitmap bitmap, final int n, final int n2) {
        if (bitmap.getWidth() == n && bitmap.getHeight() == n2) {
            return bitmap;
        }
        final Bitmap scaledBitmap = Bitmap.createScaledBitmap(bitmap, n, n2, true);
        bitmap.recycle();
        return scaledBitmap;
    }
    
    public static void \u3007o00\u3007\u3007Oo(final Path path, @Nullable final TrimPathContent trimPathContent) {
        if (trimPathContent != null) {
            if (!trimPathContent.\u300780\u3007808\u3007O()) {
                \u3007080(path, ((FloatKeyframeAnimation)trimPathContent.oO80()).\u3007\u3007808\u3007() / 100.0f, ((FloatKeyframeAnimation)trimPathContent.o\u30070()).\u3007\u3007808\u3007() / 100.0f, ((FloatKeyframeAnimation)trimPathContent.\u3007\u3007888()).\u3007\u3007808\u3007() / 360.0f);
            }
        }
    }
    
    public static void \u3007o\u3007(final Closeable closeable) {
        if (closeable == null) {
            goto Label_0016;
        }
        try {
            closeable.close();
            goto Label_0016;
        }
        catch (final RuntimeException ex) {
            throw ex;
        }
        catch (final Exception ex2) {
            goto Label_0016;
        }
    }
    
    public static float \u3007\u3007888(final Matrix matrix) {
        final float[] o8 = Utils.O8;
        o8[1] = (o8[0] = 0.0f);
        o8[3] = (o8[2] = Utils.Oo08);
        matrix.mapPoints(o8);
        return (float)Math.hypot(o8[2] - o8[0], o8[3] - o8[1]);
    }
}

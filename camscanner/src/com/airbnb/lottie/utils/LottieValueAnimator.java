// 
// Decompiled by Procyon v0.6.0
// 

package com.airbnb.lottie.utils;

import android.view.Choreographer;
import androidx.annotation.FloatRange;
import com.airbnb.lottie.L;
import androidx.annotation.MainThread;
import androidx.annotation.Nullable;
import com.airbnb.lottie.LottieComposition;
import androidx.annotation.VisibleForTesting;
import android.view.Choreographer$FrameCallback;

public class LottieValueAnimator extends BaseLottieAnimator implements Choreographer$FrameCallback
{
    private float O8o08O8O;
    private float OO;
    @VisibleForTesting
    protected boolean OO\u300700\u30078oO;
    @Nullable
    private LottieComposition oOo0;
    private float oOo\u30078o008;
    private long o\u300700O;
    private int \u3007080OO8\u30070;
    private boolean \u300708O\u300700\u3007o;
    private float \u30070O;
    
    public LottieValueAnimator() {
        this.OO = 1.0f;
        this.\u300708O\u300700\u3007o = false;
        this.o\u300700O = 0L;
        this.O8o08O8O = 0.0f;
        this.\u3007080OO8\u30070 = 0;
        this.\u30070O = -2.14748365E9f;
        this.oOo\u30078o008 = 2.14748365E9f;
        this.OO\u300700\u30078oO = false;
    }
    
    private void o\u3007\u30070\u3007() {
        if (this.oOo0 == null) {
            return;
        }
        final float o8o08O8O = this.O8o08O8O;
        if (o8o08O8O >= this.\u30070O && o8o08O8O <= this.oOo\u30078o008) {
            return;
        }
        throw new IllegalStateException(String.format("Frame must be [%f,%f]. It is %f", this.\u30070O, this.oOo\u30078o008, this.O8o08O8O));
    }
    
    private float \u30078o8o\u3007() {
        final LottieComposition oOo0 = this.oOo0;
        if (oOo0 == null) {
            return Float.MAX_VALUE;
        }
        return 1.0E9f / oOo0.oO80() / Math.abs(this.OO);
    }
    
    private boolean \u3007\u3007808\u3007() {
        return this.Oooo8o0\u3007() < 0.0f;
    }
    
    public void O8ooOoo\u3007(final float f, final float f2) {
        if (f <= f2) {
            final LottieComposition oOo0 = this.oOo0;
            float \u3007\u3007808\u3007;
            if (oOo0 == null) {
                \u3007\u3007808\u3007 = -3.4028235E38f;
            }
            else {
                \u3007\u3007808\u3007 = oOo0.\u3007\u3007808\u3007();
            }
            final LottieComposition oOo2 = this.oOo0;
            float o\u30070;
            if (oOo2 == null) {
                o\u30070 = Float.MAX_VALUE;
            }
            else {
                o\u30070 = oOo2.o\u30070();
            }
            this.\u30070O = MiscUtils.\u3007o00\u3007\u3007Oo(f, \u3007\u3007808\u3007, o\u30070);
            this.oOo\u30078o008 = MiscUtils.\u3007o00\u3007\u3007Oo(f2, \u3007\u3007808\u3007, o\u30070);
            this.\u300700((float)(int)MiscUtils.\u3007o00\u3007\u3007Oo(this.O8o08O8O, f, f2));
            return;
        }
        throw new IllegalArgumentException(String.format("minFrame (%s) must be <= maxFrame (%s)", f, f2));
    }
    
    public float OO0o\u3007\u3007() {
        final LottieComposition oOo0 = this.oOo0;
        if (oOo0 == null) {
            return 0.0f;
        }
        float n;
        if ((n = this.\u30070O) == -2.14748365E9f) {
            n = oOo0.\u3007\u3007808\u3007();
        }
        return n;
    }
    
    public float OO0o\u3007\u3007\u3007\u30070() {
        return this.O8o08O8O;
    }
    
    @MainThread
    protected void OoO8() {
        this.\u3007O888o0o(true);
    }
    
    public float Oooo8o0\u3007() {
        return this.OO;
    }
    
    public void O\u30078O8\u3007008(final float n) {
        this.O8ooOoo\u3007(this.\u30070O, n);
    }
    
    @MainThread
    public void cancel() {
        this.\u3007080();
        this.OoO8();
    }
    
    public void doFrame(final long n) {
        this.\u30070\u3007O0088o();
        if (this.oOo0 != null) {
            if (this.isRunning()) {
                L.\u3007080("LottieValueAnimator#doFrame");
                final long o\u300700O = this.o\u300700O;
                long n2 = 0L;
                if (o\u300700O != 0L) {
                    n2 = n - o\u300700O;
                }
                final float n3 = n2 / this.\u30078o8o\u3007();
                final float o8o08O8O = this.O8o08O8O;
                float n4 = n3;
                if (this.\u3007\u3007808\u3007()) {
                    n4 = -n3;
                }
                final float o8o08O8O2 = o8o08O8O + n4;
                this.O8o08O8O = o8o08O8O2;
                final boolean o8 = MiscUtils.O8(o8o08O8O2, this.OO0o\u3007\u3007(), this.\u3007O8o08O());
                this.O8o08O8O = MiscUtils.\u3007o00\u3007\u3007Oo(this.O8o08O8O, this.OO0o\u3007\u3007(), this.\u3007O8o08O());
                this.o\u300700O = n;
                this.o\u30070();
                if (o8 ^ true) {
                    if (this.getRepeatCount() != -1 && this.\u3007080OO8\u30070 >= this.getRepeatCount()) {
                        float o8o08O8O3;
                        if (this.OO < 0.0f) {
                            o8o08O8O3 = this.OO0o\u3007\u3007();
                        }
                        else {
                            o8o08O8O3 = this.\u3007O8o08O();
                        }
                        this.O8o08O8O = o8o08O8O3;
                        this.OoO8();
                        this.\u3007o00\u3007\u3007Oo(this.\u3007\u3007808\u3007());
                    }
                    else {
                        this.\u3007o\u3007();
                        ++this.\u3007080OO8\u30070;
                        if (this.getRepeatMode() == 2) {
                            this.\u300708O\u300700\u3007o ^= true;
                            this.\u3007oo\u3007();
                        }
                        else {
                            float o8o08O8O4;
                            if (this.\u3007\u3007808\u3007()) {
                                o8o08O8O4 = this.\u3007O8o08O();
                            }
                            else {
                                o8o08O8O4 = this.OO0o\u3007\u3007();
                            }
                            this.O8o08O8O = o8o08O8O4;
                        }
                        this.o\u300700O = n;
                    }
                }
                this.o\u3007\u30070\u3007();
                L.\u3007o00\u3007\u3007Oo("LottieValueAnimator#doFrame");
            }
        }
    }
    
    @FloatRange(from = 0.0, to = 1.0)
    public float getAnimatedFraction() {
        if (this.oOo0 == null) {
            return 0.0f;
        }
        float n;
        float n2;
        float n3;
        if (this.\u3007\u3007808\u3007()) {
            n = this.\u3007O8o08O() - this.O8o08O8O;
            n2 = this.\u3007O8o08O();
            n3 = this.OO0o\u3007\u3007();
        }
        else {
            n = this.O8o08O8O - this.OO0o\u3007\u3007();
            n2 = this.\u3007O8o08O();
            n3 = this.OO0o\u3007\u3007();
        }
        return n / (n2 - n3);
    }
    
    public Object getAnimatedValue() {
        return this.\u300780\u3007808\u3007O();
    }
    
    public long getDuration() {
        final LottieComposition oOo0 = this.oOo0;
        long n;
        if (oOo0 == null) {
            n = 0L;
        }
        else {
            n = (long)oOo0.O8();
        }
        return n;
    }
    
    public boolean isRunning() {
        return this.OO\u300700\u30078oO;
    }
    
    @MainThread
    public void oO80() {
        this.OoO8();
        this.\u3007o00\u3007\u3007Oo(this.\u3007\u3007808\u3007());
    }
    
    @MainThread
    public void oo88o8O() {
        this.OO\u300700\u30078oO = true;
        this.\u30070\u3007O0088o();
        this.o\u300700O = 0L;
        if (this.\u3007\u3007808\u3007() && this.OO0o\u3007\u3007\u3007\u30070() == this.OO0o\u3007\u3007()) {
            this.O8o08O8O = this.\u3007O8o08O();
        }
        else if (!this.\u3007\u3007808\u3007() && this.OO0o\u3007\u3007\u3007\u30070() == this.\u3007O8o08O()) {
            this.O8o08O8O = this.OO0o\u3007\u3007();
        }
    }
    
    public void o\u3007O8\u3007\u3007o(final LottieComposition oOo0) {
        final boolean b = this.oOo0 == null;
        this.oOo0 = oOo0;
        if (b) {
            this.O8ooOoo\u3007((float)(int)Math.max(this.\u30070O, oOo0.\u3007\u3007808\u3007()), (float)(int)Math.min(this.oOo\u30078o008, oOo0.o\u30070()));
        }
        else {
            this.O8ooOoo\u3007((float)(int)oOo0.\u3007\u3007808\u3007(), (float)(int)oOo0.o\u30070());
        }
        final float o8o08O8O = this.O8o08O8O;
        this.O8o08O8O = 0.0f;
        this.\u300700((float)(int)o8o08O8O);
        this.o\u30070();
    }
    
    public void setRepeatMode(final int repeatMode) {
        super.setRepeatMode(repeatMode);
        if (repeatMode != 2 && this.\u300708O\u300700\u3007o) {
            this.\u300708O\u300700\u3007o = false;
            this.\u3007oo\u3007();
        }
    }
    
    public void \u300700(final float n) {
        if (this.O8o08O8O == n) {
            return;
        }
        this.O8o08O8O = MiscUtils.\u3007o00\u3007\u3007Oo(n, this.OO0o\u3007\u3007(), this.\u3007O8o08O());
        this.o\u300700O = 0L;
        this.o\u30070();
    }
    
    public void \u30070000OOO(final float oo) {
        this.OO = oo;
    }
    
    protected void \u30070\u3007O0088o() {
        if (this.isRunning()) {
            this.\u3007O888o0o(false);
            Choreographer.getInstance().postFrameCallback((Choreographer$FrameCallback)this);
        }
    }
    
    @FloatRange(from = 0.0, to = 1.0)
    public float \u300780\u3007808\u3007O() {
        final LottieComposition oOo0 = this.oOo0;
        if (oOo0 == null) {
            return 0.0f;
        }
        return (this.O8o08O8O - oOo0.\u3007\u3007808\u3007()) / (this.oOo0.o\u30070() - this.oOo0.\u3007\u3007808\u3007());
    }
    
    @MainThread
    protected void \u3007O888o0o(final boolean b) {
        Choreographer.getInstance().removeFrameCallback((Choreographer$FrameCallback)this);
        if (b) {
            this.OO\u300700\u30078oO = false;
        }
    }
    
    public float \u3007O8o08O() {
        final LottieComposition oOo0 = this.oOo0;
        if (oOo0 == null) {
            return 0.0f;
        }
        float n;
        if ((n = this.oOo\u30078o008) == 2.14748365E9f) {
            n = oOo0.o\u30070();
        }
        return n;
    }
    
    @MainThread
    public void \u3007O\u3007() {
        this.OoO8();
    }
    
    public void \u3007oOO8O8(final int n) {
        this.O8ooOoo\u3007((float)n, (float)(int)this.oOo\u30078o008);
    }
    
    public void \u3007oo\u3007() {
        this.\u30070000OOO(-this.Oooo8o0\u3007());
    }
    
    public void \u3007\u3007888() {
        this.oOo0 = null;
        this.\u30070O = -2.14748365E9f;
        this.oOo\u30078o008 = 2.14748365E9f;
    }
    
    @MainThread
    public void \u3007\u30078O0\u30078() {
        this.OO\u300700\u30078oO = true;
        this.O8(this.\u3007\u3007808\u3007());
        float n;
        if (this.\u3007\u3007808\u3007()) {
            n = this.\u3007O8o08O();
        }
        else {
            n = this.OO0o\u3007\u3007();
        }
        this.\u300700((float)(int)n);
        this.o\u300700O = 0L;
        this.\u3007080OO8\u30070 = 0;
        this.\u30070\u3007O0088o();
    }
}

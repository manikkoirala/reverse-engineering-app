// 
// Decompiled by Procyon v0.6.0
// 

package com.airbnb.lottie.utils;

public class GammaEvaluator
{
    private static float \u3007080(float n) {
        if (n <= 0.04045f) {
            n /= 12.92f;
        }
        else {
            n = (float)Math.pow((n + 0.055f) / 1.055f, 2.4000000953674316);
        }
        return n;
    }
    
    private static float \u3007o00\u3007\u3007Oo(float n) {
        if (n <= 0.0031308f) {
            n *= 12.92f;
        }
        else {
            n = (float)(Math.pow(n, 0.4166666567325592) * 1.0549999475479126 - 0.054999999701976776);
        }
        return n;
    }
    
    public static int \u3007o\u3007(final float n, int round, final int n2) {
        if (round == n2) {
            return round;
        }
        final float n3 = (round >> 24 & 0xFF) / 255.0f;
        final float n4 = (round >> 16 & 0xFF) / 255.0f;
        final float n5 = (round >> 8 & 0xFF) / 255.0f;
        final float n6 = (round & 0xFF) / 255.0f;
        final float n7 = (n2 >> 24 & 0xFF) / 255.0f;
        final float n8 = (n2 >> 16 & 0xFF) / 255.0f;
        final float n9 = (n2 >> 8 & 0xFF) / 255.0f;
        final float n10 = (n2 & 0xFF) / 255.0f;
        final float \u3007080 = \u3007080(n4);
        final float \u300781 = \u3007080(n5);
        final float \u300782 = \u3007080(n6);
        final float \u300783 = \u3007080(n8);
        final float \u300784 = \u3007080(n9);
        final float \u300785 = \u3007080(n10);
        final float \u3007o00\u3007\u3007Oo = \u3007o00\u3007\u3007Oo(\u3007080 + (\u300783 - \u3007080) * n);
        final float \u3007o00\u3007\u3007Oo2 = \u3007o00\u3007\u3007Oo(\u300781 + (\u300784 - \u300781) * n);
        final float \u3007o00\u3007\u3007Oo3 = \u3007o00\u3007\u3007Oo(\u300782 + n * (\u300785 - \u300782));
        round = Math.round((n3 + (n7 - n3) * n) * 255.0f);
        return Math.round(\u3007o00\u3007\u3007Oo * 255.0f) << 16 | round << 24 | Math.round(\u3007o00\u3007\u3007Oo2 * 255.0f) << 8 | Math.round(\u3007o00\u3007\u3007Oo3 * 255.0f);
    }
}

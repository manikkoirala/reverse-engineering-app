// 
// Decompiled by Procyon v0.6.0
// 

package com.airbnb.lottie.utils;

import com.airbnb.lottie.L;
import java.util.HashSet;
import java.util.Set;
import com.airbnb.lottie.LottieLogger;

public class LogcatLogger implements LottieLogger
{
    private static final Set<String> \u3007080;
    
    static {
        \u3007080 = new HashSet<String>();
    }
    
    @Override
    public void debug(final String s) {
        this.\u3007o\u3007(s, null);
    }
    
    @Override
    public void error(final String s, final Throwable t) {
        final boolean \u3007080 = L.\u3007080;
    }
    
    @Override
    public void \u3007080(final String s) {
        this.\u3007o00\u3007\u3007Oo(s, null);
    }
    
    @Override
    public void \u3007o00\u3007\u3007Oo(final String s, final Throwable t) {
        final Set<String> \u3007080 = LogcatLogger.\u3007080;
        if (\u3007080.contains(s)) {
            return;
        }
        \u3007080.add(s);
    }
    
    public void \u3007o\u3007(final String s, final Throwable t) {
        final boolean \u3007080 = L.\u3007080;
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.airbnb.lottie.utils;

import o\u30070.Oo08;
import android.animation.TimeInterpolator;
import java.util.Iterator;
import android.animation.Animator;
import o\u30070.O8;
import android.os.Build$VERSION;
import java.util.concurrent.CopyOnWriteArraySet;
import android.animation.Animator$AnimatorListener;
import android.animation.ValueAnimator$AnimatorUpdateListener;
import java.util.Set;
import android.animation.ValueAnimator;

public abstract class BaseLottieAnimator extends ValueAnimator
{
    private final Set<ValueAnimator$AnimatorUpdateListener> o0;
    private final Set<Animator$AnimatorListener> \u3007OOo8\u30070;
    
    public BaseLottieAnimator() {
        this.o0 = new CopyOnWriteArraySet<ValueAnimator$AnimatorUpdateListener>();
        this.\u3007OOo8\u30070 = new CopyOnWriteArraySet<Animator$AnimatorListener>();
    }
    
    void O8(final boolean b) {
        for (final Animator$AnimatorListener animator$AnimatorListener : this.\u3007OOo8\u30070) {
            if (Build$VERSION.SDK_INT >= 26) {
                O8.\u3007080(animator$AnimatorListener, (Animator)this, b);
            }
            else {
                animator$AnimatorListener.onAnimationStart((Animator)this);
            }
        }
    }
    
    public void addListener(final Animator$AnimatorListener animator$AnimatorListener) {
        this.\u3007OOo8\u30070.add(animator$AnimatorListener);
    }
    
    public void addUpdateListener(final ValueAnimator$AnimatorUpdateListener valueAnimator$AnimatorUpdateListener) {
        this.o0.add(valueAnimator$AnimatorUpdateListener);
    }
    
    public long getStartDelay() {
        throw new UnsupportedOperationException("LottieAnimator does not support getStartDelay.");
    }
    
    void o\u30070() {
        final Iterator<ValueAnimator$AnimatorUpdateListener> iterator = this.o0.iterator();
        while (iterator.hasNext()) {
            iterator.next().onAnimationUpdate((ValueAnimator)this);
        }
    }
    
    public void removeAllListeners() {
        this.\u3007OOo8\u30070.clear();
    }
    
    public void removeAllUpdateListeners() {
        this.o0.clear();
    }
    
    public void removeListener(final Animator$AnimatorListener animator$AnimatorListener) {
        this.\u3007OOo8\u30070.remove(animator$AnimatorListener);
    }
    
    public void removeUpdateListener(final ValueAnimator$AnimatorUpdateListener valueAnimator$AnimatorUpdateListener) {
        this.o0.remove(valueAnimator$AnimatorUpdateListener);
    }
    
    public ValueAnimator setDuration(final long n) {
        throw new UnsupportedOperationException("LottieAnimator does not support setDuration.");
    }
    
    public void setInterpolator(final TimeInterpolator timeInterpolator) {
        throw new UnsupportedOperationException("LottieAnimator does not support setInterpolator.");
    }
    
    public void setStartDelay(final long n) {
        throw new UnsupportedOperationException("LottieAnimator does not support setStartDelay.");
    }
    
    void \u3007080() {
        final Iterator<Animator$AnimatorListener> iterator = this.\u3007OOo8\u30070.iterator();
        while (iterator.hasNext()) {
            iterator.next().onAnimationCancel((Animator)this);
        }
    }
    
    void \u3007o00\u3007\u3007Oo(final boolean b) {
        for (final Animator$AnimatorListener animator$AnimatorListener : this.\u3007OOo8\u30070) {
            if (Build$VERSION.SDK_INT >= 26) {
                Oo08.\u3007080(animator$AnimatorListener, (Animator)this, b);
            }
            else {
                animator$AnimatorListener.onAnimationEnd((Animator)this);
            }
        }
    }
    
    void \u3007o\u3007() {
        final Iterator<Animator$AnimatorListener> iterator = this.\u3007OOo8\u30070.iterator();
        while (iterator.hasNext()) {
            iterator.next().onAnimationRepeat((Animator)this);
        }
    }
}

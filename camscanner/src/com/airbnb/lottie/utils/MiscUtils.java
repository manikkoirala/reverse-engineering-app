// 
// Decompiled by Procyon v0.6.0
// 

package com.airbnb.lottie.utils;

import com.airbnb.lottie.model.KeyPathElement;
import com.airbnb.lottie.animation.content.KeyPathElementContent;
import java.util.List;
import com.airbnb.lottie.model.KeyPath;
import com.airbnb.lottie.model.CubicCurveData;
import android.graphics.Path;
import com.airbnb.lottie.model.content.ShapeData;
import androidx.annotation.FloatRange;
import android.graphics.PointF;

public class MiscUtils
{
    private static PointF \u3007080;
    
    static {
        MiscUtils.\u3007080 = new PointF();
    }
    
    public static boolean O8(final float n, final float n2, final float n3) {
        return n >= n2 && n <= n3;
    }
    
    public static float OO0o\u3007\u3007\u3007\u30070(final float n, final float n2, @FloatRange(from = 0.0, to = 1.0) final float n3) {
        return n + n3 * (n2 - n);
    }
    
    private static int Oo08(final int n, final int n2) {
        final int n3 = n / n2;
        final boolean b = (n ^ n2) >= 0;
        int n4 = n3;
        if (!b) {
            n4 = n3;
            if (n % n2 != 0) {
                n4 = n3 - 1;
            }
        }
        return n4;
    }
    
    public static void oO80(final ShapeData shapeData, final Path path) {
        path.reset();
        final PointF \u3007o00\u3007\u3007Oo = shapeData.\u3007o00\u3007\u3007Oo();
        path.moveTo(\u3007o00\u3007\u3007Oo.x, \u3007o00\u3007\u3007Oo.y);
        MiscUtils.\u3007080.set(\u3007o00\u3007\u3007Oo.x, \u3007o00\u3007\u3007Oo.y);
        for (int i = 0; i < shapeData.\u3007080().size(); ++i) {
            final CubicCurveData cubicCurveData = shapeData.\u3007080().get(i);
            final PointF \u3007080 = cubicCurveData.\u3007080();
            final PointF \u3007o00\u3007\u3007Oo2 = cubicCurveData.\u3007o00\u3007\u3007Oo();
            final PointF \u3007o\u3007 = cubicCurveData.\u3007o\u3007();
            if (\u3007080.equals((Object)MiscUtils.\u3007080) && \u3007o00\u3007\u3007Oo2.equals((Object)\u3007o\u3007)) {
                path.lineTo(\u3007o\u3007.x, \u3007o\u3007.y);
            }
            else {
                path.cubicTo(\u3007080.x, \u3007080.y, \u3007o00\u3007\u3007Oo2.x, \u3007o00\u3007\u3007Oo2.y, \u3007o\u3007.x, \u3007o\u3007.y);
            }
            MiscUtils.\u3007080.set(\u3007o\u3007.x, \u3007o\u3007.y);
        }
        if (shapeData.O8()) {
            path.close();
        }
    }
    
    static int o\u30070(final float n, final float n2) {
        return \u3007\u3007888((int)n, (int)n2);
    }
    
    public static PointF \u3007080(final PointF pointF, final PointF pointF2) {
        return new PointF(pointF.x + pointF2.x, pointF.y + pointF2.y);
    }
    
    public static double \u300780\u3007808\u3007O(final double n, final double n2, @FloatRange(from = 0.0, to = 1.0) final double n3) {
        return n + n3 * (n2 - n);
    }
    
    public static int \u30078o8o\u3007(final int n, final int n2, @FloatRange(from = 0.0, to = 1.0) final float n3) {
        return (int)(n + n3 * (n2 - n));
    }
    
    public static void \u3007O8o08O(final KeyPath keyPath, final int n, final List<KeyPath> list, final KeyPath keyPath2, final KeyPathElementContent keyPathElementContent) {
        if (keyPath.\u3007o\u3007(keyPathElementContent.getName(), n)) {
            list.add(keyPath2.\u3007080(keyPathElementContent.getName()).\u300780\u3007808\u3007O(keyPathElementContent));
        }
    }
    
    public static float \u3007o00\u3007\u3007Oo(final float b, final float a, final float a2) {
        return Math.max(a, Math.min(a2, b));
    }
    
    public static int \u3007o\u3007(final int b, final int a, final int a2) {
        return Math.max(a, Math.min(a2, b));
    }
    
    private static int \u3007\u3007888(final int n, final int n2) {
        return n - n2 * Oo08(n, n2);
    }
}

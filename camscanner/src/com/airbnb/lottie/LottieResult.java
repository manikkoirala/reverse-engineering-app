// 
// Decompiled by Procyon v0.6.0
// 

package com.airbnb.lottie;

import java.util.Arrays;
import androidx.annotation.Nullable;

public final class LottieResult<V>
{
    @Nullable
    private final V \u3007080;
    @Nullable
    private final Throwable \u3007o00\u3007\u3007Oo;
    
    public LottieResult(final V \u3007080) {
        this.\u3007080 = \u3007080;
        this.\u3007o00\u3007\u3007Oo = null;
    }
    
    public LottieResult(final Throwable \u3007o00\u3007\u3007Oo) {
        this.\u3007o00\u3007\u3007Oo = \u3007o00\u3007\u3007Oo;
        this.\u3007080 = null;
    }
    
    @Override
    public boolean equals(final Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof LottieResult)) {
            return false;
        }
        final LottieResult lottieResult = (LottieResult)o;
        return (this.\u3007o00\u3007\u3007Oo() != null && this.\u3007o00\u3007\u3007Oo().equals(lottieResult.\u3007o00\u3007\u3007Oo())) || (this.\u3007080() != null && lottieResult.\u3007080() != null && this.\u3007080().toString().equals(this.\u3007080().toString()));
    }
    
    @Override
    public int hashCode() {
        return Arrays.hashCode(new Object[] { this.\u3007o00\u3007\u3007Oo(), this.\u3007080() });
    }
    
    @Nullable
    public Throwable \u3007080() {
        return this.\u3007o00\u3007\u3007Oo;
    }
    
    @Nullable
    public V \u3007o00\u3007\u3007Oo() {
        return this.\u3007080;
    }
}

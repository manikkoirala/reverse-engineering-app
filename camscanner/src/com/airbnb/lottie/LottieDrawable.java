// 
// Decompiled by Procyon v0.6.0
// 

package com.airbnb.lottie;

import android.graphics.Bitmap;
import android.animation.Animator$AnimatorListener;
import android.graphics.ColorFilter;
import androidx.annotation.IntRange;
import android.graphics.Typeface;
import java.util.Collections;
import com.airbnb.lottie.utils.Logger;
import com.airbnb.lottie.model.Marker;
import androidx.annotation.MainThread;
import java.util.Iterator;
import android.widget.ImageView;
import java.util.Collection;
import java.util.List;
import com.airbnb.lottie.model.KeyPathElement;
import com.airbnb.lottie.value.LottieValueCallback;
import com.airbnb.lottie.model.KeyPath;
import com.airbnb.lottie.utils.MiscUtils;
import androidx.annotation.FloatRange;
import androidx.annotation.NonNull;
import com.airbnb.lottie.parser.LayerParser;
import android.view.View;
import android.content.Context;
import android.graphics.Rect;
import android.graphics.Canvas;
import android.animation.ValueAnimator;
import java.util.HashSet;
import com.airbnb.lottie.model.layer.CompositionLayer;
import com.airbnb.lottie.manager.FontAssetManager;
import java.util.ArrayList;
import java.util.Set;
import android.animation.ValueAnimator$AnimatorUpdateListener;
import android.widget.ImageView$ScaleType;
import android.graphics.Matrix;
import androidx.annotation.Nullable;
import com.airbnb.lottie.manager.ImageAssetManager;
import com.airbnb.lottie.utils.LottieValueAnimator;
import android.graphics.drawable.Animatable;
import android.graphics.drawable.Drawable$Callback;
import android.graphics.drawable.Drawable;

public class LottieDrawable extends Drawable implements Drawable$Callback, Animatable
{
    private int O0O;
    private boolean O88O;
    private boolean O8o08O8O;
    private final LottieValueAnimator OO;
    @Nullable
    private ImageAssetManager OO\u300700\u30078oO;
    private final Matrix o0;
    private boolean o8oOOo;
    @Nullable
    private String o8\u3007OO0\u30070o;
    @Nullable
    private ImageView$ScaleType oOo0;
    private final ValueAnimator$AnimatorUpdateListener oOo\u30078o008;
    private boolean ooo0\u3007\u3007O;
    private boolean o\u300700O;
    private final Set<Object> \u3007080OO8\u30070;
    private float \u300708O\u300700\u3007o;
    private final ArrayList<LazyCompositionTask> \u30070O;
    @Nullable
    private FontAssetManager \u30078\u3007oO\u3007\u30078o;
    private LottieComposition \u3007OOo8\u30070;
    private boolean \u3007O\u3007\u3007O8;
    private boolean \u3007o0O;
    @Nullable
    private CompositionLayer \u3007\u300708O;
    
    public LottieDrawable() {
        this.o0 = new Matrix();
        final LottieValueAnimator oo = new LottieValueAnimator();
        this.OO = oo;
        this.\u300708O\u300700\u3007o = 1.0f;
        this.o\u300700O = true;
        this.O8o08O8O = false;
        this.\u3007080OO8\u30070 = new HashSet<Object>();
        this.\u30070O = new ArrayList<LazyCompositionTask>();
        final ValueAnimator$AnimatorUpdateListener oOo\u30078o008 = (ValueAnimator$AnimatorUpdateListener)new ValueAnimator$AnimatorUpdateListener() {
            final LottieDrawable \u3007080;
            
            public void onAnimationUpdate(final ValueAnimator valueAnimator) {
                if (this.\u3007080.\u3007\u300708O != null) {
                    this.\u3007080.\u3007\u300708O.oo\u3007(this.\u3007080.OO.\u300780\u3007808\u3007O());
                }
            }
        };
        this.oOo\u30078o008 = (ValueAnimator$AnimatorUpdateListener)oOo\u30078o008;
        this.O0O = 255;
        this.\u3007o0O = true;
        this.O88O = false;
        oo.addUpdateListener((ValueAnimator$AnimatorUpdateListener)oOo\u30078o008);
    }
    
    private void OO0o\u3007\u3007\u3007\u30070(final Canvas canvas) {
        if (this.\u3007\u300708O == null) {
            return;
        }
        final Rect bounds = this.getBounds();
        float a = bounds.width() / (float)this.\u3007OOo8\u30070.\u3007o00\u3007\u3007Oo().width();
        float b = bounds.height() / (float)this.\u3007OOo8\u30070.\u3007o00\u3007\u3007Oo().height();
        final boolean \u3007o0O = this.\u3007o0O;
        final int n = -1;
        float n2 = a;
        float n3 = b;
        int save = n;
        if (\u3007o0O) {
            final float min = Math.min(a, b);
            float n4;
            if (min < 1.0f) {
                n4 = 1.0f / min;
                a /= n4;
                b /= n4;
            }
            else {
                n4 = 1.0f;
            }
            n2 = a;
            n3 = b;
            save = n;
            if (n4 > 1.0f) {
                save = canvas.save();
                final float n5 = bounds.width() / 2.0f;
                final float n6 = bounds.height() / 2.0f;
                final float n7 = n5 * min;
                final float n8 = min * n6;
                canvas.translate(n5 - n7, n6 - n8);
                canvas.scale(n4, n4, n7, n8);
                n3 = b;
                n2 = a;
            }
        }
        this.o0.reset();
        this.o0.preScale(n2, n3);
        this.\u3007\u300708O.\u3007o\u3007(canvas, this.o0, this.O0O);
        if (save > 0) {
            canvas.restoreToCount(save);
        }
    }
    
    @Nullable
    private Context getContext() {
        final Drawable$Callback callback = this.getCallback();
        if (callback == null) {
            return null;
        }
        if (callback instanceof View) {
            return ((View)callback).getContext();
        }
        return null;
    }
    
    private void o\u30070() {
        this.\u3007\u300708O = new CompositionLayer(this, LayerParser.\u3007080(this.\u3007OOo8\u30070), this.\u3007OOo8\u30070.OO0o\u3007\u3007\u3007\u30070(), this.\u3007OOo8\u30070);
    }
    
    private void \u30070() {
        if (this.\u3007OOo8\u30070 == null) {
            return;
        }
        final float o8ooOoo\u3007 = this.O8ooOoo\u3007();
        this.setBounds(0, 0, (int)(this.\u3007OOo8\u30070.\u3007o00\u3007\u3007Oo().width() * o8ooOoo\u3007), (int)(this.\u3007OOo8\u30070.\u3007o00\u3007\u3007Oo().height() * o8ooOoo\u3007));
    }
    
    private ImageAssetManager \u30070\u3007O0088o() {
        if (this.getCallback() == null) {
            return null;
        }
        final ImageAssetManager oo\u300700\u30078oO = this.OO\u300700\u30078oO;
        if (oo\u300700\u30078oO != null && !oo\u300700\u30078oO.\u3007o00\u3007\u3007Oo(this.getContext())) {
            this.OO\u300700\u30078oO = null;
        }
        if (this.OO\u300700\u30078oO == null) {
            this.OO\u300700\u30078oO = new ImageAssetManager(this.getCallback(), this.o8\u3007OO0\u30070o, null, this.\u3007OOo8\u30070.\u300780\u3007808\u3007O());
        }
        return this.OO\u300700\u30078oO;
    }
    
    private void \u300780\u3007808\u3007O(@NonNull final Canvas canvas) {
        if (ImageView$ScaleType.FIT_XY == this.oOo0) {
            this.OO0o\u3007\u3007\u3007\u30070(canvas);
        }
        else {
            this.\u30078o8o\u3007(canvas);
        }
    }
    
    private void \u30078o8o\u3007(final Canvas canvas) {
        if (this.\u3007\u300708O == null) {
            return;
        }
        final float \u300708O\u300700\u3007o = this.\u300708O\u300700\u3007o;
        float \u3007o888o0o = this.\u3007O888o0o(canvas);
        float n;
        if (\u300708O\u300700\u3007o > \u3007o888o0o) {
            n = this.\u300708O\u300700\u3007o / \u3007o888o0o;
        }
        else {
            \u3007o888o0o = \u300708O\u300700\u3007o;
            n = 1.0f;
        }
        int save;
        if (n > 1.0f) {
            save = canvas.save();
            final float n2 = this.\u3007OOo8\u30070.\u3007o00\u3007\u3007Oo().width() / 2.0f;
            final float n3 = this.\u3007OOo8\u30070.\u3007o00\u3007\u3007Oo().height() / 2.0f;
            final float n4 = n2 * \u3007o888o0o;
            final float n5 = n3 * \u3007o888o0o;
            canvas.translate(this.O8ooOoo\u3007() * n2 - n4, this.O8ooOoo\u3007() * n3 - n5);
            canvas.scale(n, n, n4, n5);
        }
        else {
            save = -1;
        }
        this.o0.reset();
        this.o0.preScale(\u3007o888o0o, \u3007o888o0o);
        this.\u3007\u300708O.\u3007o\u3007(canvas, this.o0, this.O0O);
        if (save > 0) {
            canvas.restoreToCount(save);
        }
    }
    
    private float \u3007O888o0o(@NonNull final Canvas canvas) {
        return Math.min(canvas.getWidth() / (float)this.\u3007OOo8\u30070.\u3007o00\u3007\u3007Oo().width(), canvas.getHeight() / (float)this.\u3007OOo8\u30070.\u3007o00\u3007\u3007Oo().height());
    }
    
    private FontAssetManager \u3007O\u3007() {
        if (this.getCallback() == null) {
            return null;
        }
        if (this.\u30078\u3007oO\u3007\u30078o == null) {
            this.\u30078\u3007oO\u3007\u30078o = new FontAssetManager(this.getCallback(), null);
        }
        return this.\u30078\u3007oO\u3007\u30078o;
    }
    
    public void O000(@FloatRange(from = 0.0, to = 1.0) final float n) {
        if (this.\u3007OOo8\u30070 == null) {
            this.\u30070O.add((LazyCompositionTask)new LazyCompositionTask(this, n) {
                final float \u3007080;
                final LottieDrawable \u3007o00\u3007\u3007Oo;
                
                @Override
                public void \u3007080(final LottieComposition lottieComposition) {
                    this.\u3007o00\u3007\u3007Oo.O000(this.\u3007080);
                }
            });
            return;
        }
        L.\u3007080("Drawable#setProgress");
        this.OO.\u300700(MiscUtils.OO0o\u3007\u3007\u3007\u30070(this.\u3007OOo8\u30070.\u3007\u3007808\u3007(), this.\u3007OOo8\u30070.o\u30070(), n));
        L.\u3007o00\u3007\u3007Oo("Drawable#setProgress");
    }
    
    public void O08000(@FloatRange(from = 0.0, to = 1.0) final float n) {
        final LottieComposition \u3007oOo8\u30070 = this.\u3007OOo8\u30070;
        if (\u3007oOo8\u30070 == null) {
            this.\u30070O.add((LazyCompositionTask)new LazyCompositionTask(this, n) {
                final float \u3007080;
                final LottieDrawable \u3007o00\u3007\u3007Oo;
                
                @Override
                public void \u3007080(final LottieComposition lottieComposition) {
                    this.\u3007o00\u3007\u3007Oo.O08000(this.\u3007080);
                }
            });
            return;
        }
        this.oO((int)MiscUtils.OO0o\u3007\u3007\u3007\u30070(\u3007oOo8\u30070.\u3007\u3007808\u3007(), this.\u3007OOo8\u30070.o\u30070(), n));
    }
    
    public void O0o\u3007\u3007Oo(final float n) {
        this.OO.\u30070000OOO(n);
    }
    
    public void O8(final ValueAnimator$AnimatorUpdateListener valueAnimator$AnimatorUpdateListener) {
        this.OO.addUpdateListener(valueAnimator$AnimatorUpdateListener);
    }
    
    public float O8ooOoo\u3007() {
        return this.\u300708O\u300700\u3007o;
    }
    
    public void O8\u3007o() {
        this.\u30070O.clear();
        this.OO.\u3007O\u3007();
    }
    
    public boolean OO0o\u3007\u3007() {
        return this.ooo0\u3007\u3007O;
    }
    
    void OO8oO0o\u3007(final Boolean b) {
        this.o\u300700O = b;
    }
    
    public void OOO(final float \u300708O\u300700\u3007o) {
        this.\u300708O\u300700\u3007o = \u300708O\u300700\u3007o;
        this.\u30070();
    }
    
    public boolean OOO\u3007O0() {
        final LottieValueAnimator oo = this.OO;
        return oo != null && oo.isRunning();
    }
    
    public <T> void Oo08(final KeyPath keyPath, final T t, final LottieValueCallback<T> lottieValueCallback) {
        if (this.\u3007\u300708O == null) {
            this.\u30070O.add((LazyCompositionTask)new LazyCompositionTask(this, keyPath, t, lottieValueCallback) {
                final LottieDrawable O8;
                final KeyPath \u3007080;
                final Object \u3007o00\u3007\u3007Oo;
                final LottieValueCallback \u3007o\u3007;
                
                @Override
                public void \u3007080(final LottieComposition lottieComposition) {
                    this.O8.Oo08(this.\u3007080, this.\u3007o00\u3007\u3007Oo, this.\u3007o\u3007);
                }
            });
            return;
        }
        final KeyPathElement o8 = keyPath.O8();
        boolean b = true;
        if (o8 != null) {
            keyPath.O8().o\u30070(t, lottieValueCallback);
        }
        else {
            final List<KeyPath> o0ooO = this.o0ooO(keyPath);
            for (int i = 0; i < o0ooO.size(); ++i) {
                ((KeyPath)o0ooO.get(i)).O8().o\u30070(t, lottieValueCallback);
            }
            b = (true ^ o0ooO.isEmpty());
        }
        if (b) {
            this.invalidateSelf();
            if (t == LottieProperty.O\u30078O8\u3007008) {
                this.O000(this.o\u3007O8\u3007\u3007o());
            }
        }
    }
    
    public boolean Oo8Oo00oo(final LottieComposition \u3007oOo8\u30070) {
        if (this.\u3007OOo8\u30070 == \u3007oOo8\u30070) {
            return false;
        }
        this.O88O = false;
        this.oO80();
        this.\u3007OOo8\u30070 = \u3007oOo8\u30070;
        this.o\u30070();
        this.OO.o\u3007O8\u3007\u3007o(\u3007oOo8\u30070);
        this.O000(this.OO.getAnimatedFraction());
        this.OOO(this.\u300708O\u300700\u3007o);
        this.\u30070();
        final Iterator iterator = new ArrayList(this.\u30070O).iterator();
        while (iterator.hasNext()) {
            ((LazyCompositionTask)iterator.next()).\u3007080(\u3007oOo8\u30070);
            iterator.remove();
        }
        this.\u30070O.clear();
        \u3007oOo8\u30070.o800o8O(this.o8oOOo);
        final Drawable$Callback callback = this.getCallback();
        if (callback instanceof ImageView) {
            final ImageView imageView = (ImageView)callback;
            imageView.setImageDrawable((Drawable)null);
            imageView.setImageDrawable((Drawable)this);
        }
        return true;
    }
    
    @Nullable
    public String OoO8() {
        return this.o8\u3007OO0\u30070o;
    }
    
    public void Ooo(final int repeatMode) {
        this.OO.setRepeatMode(repeatMode);
    }
    
    @MainThread
    public void Oooo8o0\u3007() {
        this.\u30070O.clear();
        this.OO.oO80();
    }
    
    public int O\u30078O8\u3007008() {
        return this.OO.getRepeatMode();
    }
    
    public void O\u3007O\u3007oO(final String str) {
        final LottieComposition \u3007oOo8\u30070 = this.\u3007OOo8\u30070;
        if (\u3007oOo8\u30070 == null) {
            this.\u30070O.add((LazyCompositionTask)new LazyCompositionTask(this, str) {
                final String \u3007080;
                final LottieDrawable \u3007o00\u3007\u3007Oo;
                
                @Override
                public void \u3007080(final LottieComposition lottieComposition) {
                    this.\u3007o00\u3007\u3007Oo.O\u3007O\u3007oO(this.\u3007080);
                }
            });
            return;
        }
        final Marker \u30078o8o\u3007 = \u3007oOo8\u30070.\u30078o8o\u3007(str);
        if (\u30078o8o\u3007 != null) {
            final int n = (int)\u30078o8o\u3007.\u3007o00\u3007\u3007Oo;
            this.\u30078\u30070\u3007o\u3007O(n, (int)\u30078o8o\u3007.\u3007o\u3007 + n);
            return;
        }
        final StringBuilder sb = new StringBuilder();
        sb.append("Cannot find marker with name ");
        sb.append(str);
        sb.append(".");
        throw new IllegalArgumentException(sb.toString());
    }
    
    public void draw(@NonNull final Canvas canvas) {
        this.O88O = false;
        L.\u3007080("Drawable#draw");
        if (this.O8o08O8O) {
            try {
                this.\u300780\u3007808\u3007O(canvas);
            }
            finally {
                final Throwable t;
                Logger.\u3007o00\u3007\u3007Oo("Lottie crashed in draw!", t);
            }
        }
        else {
            this.\u300780\u3007808\u3007O(canvas);
        }
        L.\u3007o00\u3007\u3007Oo("Drawable#draw");
    }
    
    public int getAlpha() {
        return this.O0O;
    }
    
    public int getIntrinsicHeight() {
        final LottieComposition \u3007oOo8\u30070 = this.\u3007OOo8\u30070;
        int n;
        if (\u3007oOo8\u30070 == null) {
            n = -1;
        }
        else {
            n = (int)(\u3007oOo8\u30070.\u3007o00\u3007\u3007Oo().height() * this.O8ooOoo\u3007());
        }
        return n;
    }
    
    public int getIntrinsicWidth() {
        final LottieComposition \u3007oOo8\u30070 = this.\u3007OOo8\u30070;
        int n;
        if (\u3007oOo8\u30070 == null) {
            n = -1;
        }
        else {
            n = (int)(\u3007oOo8\u30070.\u3007o00\u3007\u3007Oo().width() * this.O8ooOoo\u3007());
        }
        return n;
    }
    
    public int getOpacity() {
        return -3;
    }
    
    public void invalidateDrawable(@NonNull final Drawable drawable) {
        final Drawable$Callback callback = this.getCallback();
        if (callback == null) {
            return;
        }
        callback.invalidateDrawable((Drawable)this);
    }
    
    public void invalidateSelf() {
        if (this.O88O) {
            return;
        }
        this.O88O = true;
        final Drawable$Callback callback = this.getCallback();
        if (callback != null) {
            callback.invalidateDrawable((Drawable)this);
        }
    }
    
    public boolean isRunning() {
        return this.OOO\u3007O0();
    }
    
    public void o0O0(final TextDelegate textDelegate) {
    }
    
    public List<KeyPath> o0ooO(final KeyPath keyPath) {
        if (this.\u3007\u300708O == null) {
            Logger.\u3007o\u3007("Cannot resolve KeyPath. Composition is not set yet.");
            return Collections.emptyList();
        }
        final ArrayList list = new ArrayList();
        this.\u3007\u300708O.\u3007\u3007888(keyPath, 0, list, new KeyPath(new String[0]));
        return list;
    }
    
    public void o8(final boolean \u3007o\u3007\u3007O8) {
        this.\u3007O\u3007\u3007O8 = \u3007o\u3007\u3007O8;
    }
    
    public float o800o8O() {
        return this.OO.\u3007O8o08O();
    }
    
    public boolean o88\u3007OO08\u3007() {
        return this.\u3007OOo8\u30070.\u3007o\u3007().size() > 0;
    }
    
    public void o8oO\u3007(final int n) {
        if (this.\u3007OOo8\u30070 == null) {
            this.\u30070O.add((LazyCompositionTask)new LazyCompositionTask(this, n) {
                final int \u3007080;
                final LottieDrawable \u3007o00\u3007\u3007Oo;
                
                @Override
                public void \u3007080(final LottieComposition lottieComposition) {
                    this.\u3007o00\u3007\u3007Oo.o8oO\u3007(this.\u3007080);
                }
            });
            return;
        }
        this.OO.\u3007oOO8O8(n);
    }
    
    public void oO(final int n) {
        if (this.\u3007OOo8\u30070 == null) {
            this.\u30070O.add((LazyCompositionTask)new LazyCompositionTask(this, n) {
                final int \u3007080;
                final LottieDrawable \u3007o00\u3007\u3007Oo;
                
                @Override
                public void \u3007080(final LottieComposition lottieComposition) {
                    this.\u3007o00\u3007\u3007Oo.oO(this.\u3007080);
                }
            });
            return;
        }
        this.OO.O\u30078O8\u3007008(n + 0.99f);
    }
    
    public void oO00OOO(final boolean o8oOOo) {
        this.o8oOOo = o8oOOo;
        final LottieComposition \u3007oOo8\u30070 = this.\u3007OOo8\u30070;
        if (\u3007oOo8\u30070 != null) {
            \u3007oOo8\u30070.o800o8O(o8oOOo);
        }
    }
    
    public void oO80() {
        if (this.OO.isRunning()) {
            this.OO.cancel();
        }
        this.\u3007OOo8\u30070 = null;
        this.\u3007\u300708O = null;
        this.OO\u300700\u30078oO = null;
        this.OO.\u3007\u3007888();
        this.invalidateSelf();
    }
    
    public float oo88o8O() {
        return this.OO.OO0o\u3007\u3007();
    }
    
    void ooo\u30078oO(final ImageView$ScaleType oOo0) {
        this.oOo0 = oOo0;
    }
    
    public boolean oo\u3007() {
        return this.\u3007O\u3007\u3007O8;
    }
    
    public void o\u30070OOo\u30070(final int n) {
        if (this.\u3007OOo8\u30070 == null) {
            this.\u30070O.add((LazyCompositionTask)new LazyCompositionTask(this, n) {
                final int \u3007080;
                final LottieDrawable \u3007o00\u3007\u3007Oo;
                
                @Override
                public void \u3007080(final LottieComposition lottieComposition) {
                    this.\u3007o00\u3007\u3007Oo.o\u30070OOo\u30070(this.\u3007080);
                }
            });
            return;
        }
        this.OO.\u300700((float)n);
    }
    
    @MainThread
    public void o\u30078() {
        if (this.\u3007\u300708O == null) {
            this.\u30070O.add((LazyCompositionTask)new LazyCompositionTask(this) {
                final LottieDrawable \u3007080;
                
                @Override
                public void \u3007080(final LottieComposition lottieComposition) {
                    this.\u3007080.o\u30078();
                }
            });
            return;
        }
        if (this.o\u300700O || this.\u300700() == 0) {
            this.OO.oo88o8O();
        }
        if (!this.o\u300700O) {
            float n;
            if (this.\u3007oOO8O8() < 0.0f) {
                n = this.oo88o8O();
            }
            else {
                n = this.o800o8O();
            }
            this.o\u30070OOo\u30070((int)n);
            this.OO.oO80();
        }
    }
    
    public void o\u30078oOO88(final String str) {
        final LottieComposition \u3007oOo8\u30070 = this.\u3007OOo8\u30070;
        if (\u3007oOo8\u30070 == null) {
            this.\u30070O.add((LazyCompositionTask)new LazyCompositionTask(this, str) {
                final String \u3007080;
                final LottieDrawable \u3007o00\u3007\u3007Oo;
                
                @Override
                public void \u3007080(final LottieComposition lottieComposition) {
                    this.\u3007o00\u3007\u3007Oo.o\u30078oOO88(this.\u3007080);
                }
            });
            return;
        }
        final Marker \u30078o8o\u3007 = \u3007oOo8\u30070.\u30078o8o\u3007(str);
        if (\u30078o8o\u3007 != null) {
            this.o8oO\u3007((int)\u30078o8o\u3007.\u3007o00\u3007\u3007Oo);
            return;
        }
        final StringBuilder sb = new StringBuilder();
        sb.append("Cannot find marker with name ");
        sb.append(str);
        sb.append(".");
        throw new IllegalArgumentException(sb.toString());
    }
    
    public void o\u3007O(final float n) {
        final LottieComposition \u3007oOo8\u30070 = this.\u3007OOo8\u30070;
        if (\u3007oOo8\u30070 == null) {
            this.\u30070O.add((LazyCompositionTask)new LazyCompositionTask(this, n) {
                final float \u3007080;
                final LottieDrawable \u3007o00\u3007\u3007Oo;
                
                @Override
                public void \u3007080(final LottieComposition lottieComposition) {
                    this.\u3007o00\u3007\u3007Oo.o\u3007O(this.\u3007080);
                }
            });
            return;
        }
        this.o8oO\u3007((int)MiscUtils.OO0o\u3007\u3007\u3007\u30070(\u3007oOo8\u30070.\u3007\u3007808\u3007(), this.\u3007OOo8\u30070.o\u30070(), n));
    }
    
    @FloatRange(from = 0.0, to = 1.0)
    public float o\u3007O8\u3007\u3007o() {
        return this.OO.\u300780\u3007808\u3007O();
    }
    
    @Nullable
    public Typeface o\u3007\u30070\u3007(final String s, final String s2) {
        final FontAssetManager \u3007o\u3007 = this.\u3007O\u3007();
        if (\u3007o\u3007 != null) {
            return \u3007o\u3007.\u3007o00\u3007\u3007Oo(s, s2);
        }
        return null;
    }
    
    public void scheduleDrawable(@NonNull final Drawable drawable, @NonNull final Runnable runnable, final long n) {
        final Drawable$Callback callback = this.getCallback();
        if (callback == null) {
            return;
        }
        callback.scheduleDrawable((Drawable)this, runnable, n);
    }
    
    public void setAlpha(@IntRange(from = 0L, to = 255L) final int o0O) {
        this.O0O = o0O;
        this.invalidateSelf();
    }
    
    public void setColorFilter(@Nullable final ColorFilter colorFilter) {
        Logger.\u3007o\u3007("Use addColorFilter instead.");
    }
    
    @MainThread
    public void start() {
        this.\u300700\u30078();
    }
    
    @MainThread
    public void stop() {
        this.Oooo8o0\u3007();
    }
    
    public void unscheduleDrawable(@NonNull final Drawable drawable, @NonNull final Runnable runnable) {
        final Drawable$Callback callback = this.getCallback();
        if (callback == null) {
            return;
        }
        callback.unscheduleDrawable((Drawable)this, runnable);
    }
    
    public int \u300700() {
        return this.OO.getRepeatCount();
    }
    
    @Nullable
    public TextDelegate \u30070000OOO() {
        return null;
    }
    
    @MainThread
    public void \u300700\u30078() {
        if (this.\u3007\u300708O == null) {
            this.\u30070O.add((LazyCompositionTask)new LazyCompositionTask(this) {
                final LottieDrawable \u3007080;
                
                @Override
                public void \u3007080(final LottieComposition lottieComposition) {
                    this.\u3007080.\u300700\u30078();
                }
            });
            return;
        }
        if (this.o\u300700O || this.\u300700() == 0) {
            this.OO.\u3007\u30078O0\u30078();
        }
        if (!this.o\u300700O) {
            float n;
            if (this.\u3007oOO8O8() < 0.0f) {
                n = this.oo88o8O();
            }
            else {
                n = this.o800o8O();
            }
            this.o\u30070OOo\u30070((int)n);
            this.OO.oO80();
        }
    }
    
    public void \u300708O8o\u30070(@Nullable final String o8\u3007OO0\u30070o) {
        this.o8\u3007OO0\u30070o = o8\u3007OO0\u30070o;
    }
    
    public void \u30078(final String str) {
        final LottieComposition \u3007oOo8\u30070 = this.\u3007OOo8\u30070;
        if (\u3007oOo8\u30070 == null) {
            this.\u30070O.add((LazyCompositionTask)new LazyCompositionTask(this, str) {
                final String \u3007080;
                final LottieDrawable \u3007o00\u3007\u3007Oo;
                
                @Override
                public void \u3007080(final LottieComposition lottieComposition) {
                    this.\u3007o00\u3007\u3007Oo.\u30078(this.\u3007080);
                }
            });
            return;
        }
        final Marker \u30078o8o\u3007 = \u3007oOo8\u30070.\u30078o8o\u3007(str);
        if (\u30078o8o\u3007 != null) {
            this.oO((int)(\u30078o8o\u3007.\u3007o00\u3007\u3007Oo + \u30078o8o\u3007.\u3007o\u3007));
            return;
        }
        final StringBuilder sb = new StringBuilder();
        sb.append("Cannot find marker with name ");
        sb.append(str);
        sb.append(".");
        throw new IllegalArgumentException(sb.toString());
    }
    
    public void \u300780(final int repeatCount) {
        this.OO.setRepeatCount(repeatCount);
    }
    
    public void \u30078\u30070\u3007o\u3007O(final int n, final int n2) {
        if (this.\u3007OOo8\u30070 == null) {
            this.\u30070O.add((LazyCompositionTask)new LazyCompositionTask(this, n, n2) {
                final int \u3007080;
                final int \u3007o00\u3007\u3007Oo;
                final LottieDrawable \u3007o\u3007;
                
                @Override
                public void \u3007080(final LottieComposition lottieComposition) {
                    this.\u3007o\u3007.\u30078\u30070\u3007o\u3007O(this.\u3007080, this.\u3007o00\u3007\u3007Oo);
                }
            });
            return;
        }
        this.OO.O8ooOoo\u3007((float)n, n2 + 0.99f);
    }
    
    public int \u3007O00() {
        return (int)this.OO.OO0o\u3007\u3007\u3007\u30070();
    }
    
    public void \u3007O8o08O(final boolean ooo0\u3007\u3007O) {
        if (this.ooo0\u3007\u3007O == ooo0\u3007\u3007O) {
            return;
        }
        this.ooo0\u3007\u3007O = ooo0\u3007\u3007O;
        if (this.\u3007OOo8\u30070 != null) {
            this.o\u30070();
        }
    }
    
    public void \u3007O\u300780o08O(final boolean o8o08O8O) {
        this.O8o08O8O = o8o08O8O;
    }
    
    public void \u3007o() {
        this.OO.removeAllUpdateListeners();
        this.OO.addUpdateListener(this.oOo\u30078o008);
    }
    
    public float \u3007oOO8O8() {
        return this.OO.Oooo8o0\u3007();
    }
    
    @Nullable
    public PerformanceTracker \u3007oo\u3007() {
        final LottieComposition \u3007oOo8\u30070 = this.\u3007OOo8\u30070;
        if (\u3007oOo8\u30070 != null) {
            return \u3007oOo8\u30070.OO0o\u3007\u3007();
        }
        return null;
    }
    
    public void \u3007o\u3007(final Animator$AnimatorListener animator$AnimatorListener) {
        this.OO.addListener(animator$AnimatorListener);
    }
    
    public void \u3007\u30070o(final ImageAssetDelegate imageAssetDelegate) {
        final ImageAssetManager oo\u300700\u30078oO = this.OO\u300700\u30078oO;
        if (oo\u300700\u30078oO != null) {
            oo\u300700\u30078oO.O8(imageAssetDelegate);
        }
    }
    
    public LottieComposition \u3007\u3007808\u3007() {
        return this.\u3007OOo8\u30070;
    }
    
    public void \u3007\u3007888() {
        this.\u30070O.clear();
        this.OO.cancel();
    }
    
    @Nullable
    public Bitmap \u3007\u30078O0\u30078(final String s) {
        final ImageAssetManager \u30070\u3007O0088o = this.\u30070\u3007O0088o();
        if (\u30070\u3007O0088o != null) {
            return \u30070\u3007O0088o.\u3007080(s);
        }
        return null;
    }
    
    public void \u3007\u3007\u30070\u3007\u30070(final FontAssetDelegate fontAssetDelegate) {
        final FontAssetManager \u30078\u3007oO\u3007\u30078o = this.\u30078\u3007oO\u3007\u30078o;
        if (\u30078\u3007oO\u3007\u30078o != null) {
            \u30078\u3007oO\u3007\u30078o.\u3007o\u3007(fontAssetDelegate);
        }
    }
    
    private interface LazyCompositionTask
    {
        void \u3007080(final LottieComposition p0);
    }
}

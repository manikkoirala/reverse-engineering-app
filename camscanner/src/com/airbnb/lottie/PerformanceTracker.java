// 
// Decompiled by Procyon v0.6.0
// 

package com.airbnb.lottie;

import java.util.Iterator;
import java.util.HashMap;
import androidx.collection.ArraySet;
import com.airbnb.lottie.utils.MeanCalculator;
import java.util.Map;
import java.util.Set;
import androidx.core.util.Pair;
import java.util.Comparator;

public class PerformanceTracker
{
    private final Comparator<Pair<String, Float>> O8;
    private boolean \u3007080;
    private final Set<FrameListener> \u3007o00\u3007\u3007Oo;
    private final Map<String, MeanCalculator> \u3007o\u3007;
    
    public PerformanceTracker() {
        this.\u3007080 = false;
        this.\u3007o00\u3007\u3007Oo = new ArraySet<FrameListener>();
        this.\u3007o\u3007 = new HashMap<String, MeanCalculator>();
        this.O8 = new Comparator<Pair<String, Float>>() {
            final PerformanceTracker o0;
            
            public int \u3007080(final Pair<String, Float> pair, final Pair<String, Float> pair2) {
                final float floatValue = pair.second;
                final float floatValue2 = pair2.second;
                if (floatValue2 > floatValue) {
                    return 1;
                }
                if (floatValue > floatValue2) {
                    return -1;
                }
                return 0;
            }
        };
    }
    
    public void \u3007080(final String s, final float n) {
        if (!this.\u3007080) {
            return;
        }
        MeanCalculator meanCalculator;
        if ((meanCalculator = this.\u3007o\u3007.get(s)) == null) {
            meanCalculator = new MeanCalculator();
            this.\u3007o\u3007.put(s, meanCalculator);
        }
        meanCalculator.\u3007080(n);
        if (s.equals("__container")) {
            final Iterator<FrameListener> iterator = this.\u3007o00\u3007\u3007Oo.iterator();
            while (iterator.hasNext()) {
                iterator.next().\u3007080(n);
            }
        }
    }
    
    void \u3007o00\u3007\u3007Oo(final boolean \u3007080) {
        this.\u3007080 = \u3007080;
    }
    
    public interface FrameListener
    {
        void \u3007080(final float p0);
    }
}

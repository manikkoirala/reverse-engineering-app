// 
// Decompiled by Procyon v0.6.0
// 

package com.airbnb.lottie.network;

import java.io.File;
import java.io.FileInputStream;
import java.net.URL;
import com.airbnb.lottie.utils.Logger;
import androidx.annotation.WorkerThread;
import androidx.core.util.Pair;
import com.airbnb.lottie.LottieCompositionFactory;
import java.util.zip.ZipInputStream;
import java.io.InputStream;
import java.io.IOException;
import java.net.HttpURLConnection;
import com.airbnb.lottie.LottieComposition;
import com.airbnb.lottie.LottieResult;
import androidx.annotation.Nullable;
import android.content.Context;

public class NetworkFetcher
{
    private final Context \u3007080;
    private final String \u3007o00\u3007\u3007Oo;
    @Nullable
    private final NetworkCache \u3007o\u3007;
    
    private NetworkFetcher(Context applicationContext, final String \u3007o00\u3007\u3007Oo, @Nullable final String s) {
        applicationContext = applicationContext.getApplicationContext();
        this.\u3007080 = applicationContext;
        this.\u3007o00\u3007\u3007Oo = \u3007o00\u3007\u3007Oo;
        if (s == null) {
            this.\u3007o\u3007 = null;
        }
        else {
            this.\u3007o\u3007 = new NetworkCache(applicationContext);
        }
    }
    
    public static LottieResult<LottieComposition> Oo08(final Context context, final String s, @Nullable final String s2) {
        return new NetworkFetcher(context, s, s2).O8();
    }
    
    private String o\u30070(final HttpURLConnection p0) throws IOException {
        // 
        // This method could not be decompiled.
        // 
        // Original Bytecode:
        // 
        //     1: invokevirtual   java/net/HttpURLConnection.getResponseCode:()I
        //     4: pop            
        //     5: new             Ljava/io/BufferedReader;
        //     8: dup            
        //     9: new             Ljava/io/InputStreamReader;
        //    12: dup            
        //    13: aload_1        
        //    14: invokevirtual   java/net/HttpURLConnection.getErrorStream:()Ljava/io/InputStream;
        //    17: invokespecial   java/io/InputStreamReader.<init>:(Ljava/io/InputStream;)V
        //    20: invokespecial   java/io/BufferedReader.<init>:(Ljava/io/Reader;)V
        //    23: astore_1       
        //    24: new             Ljava/lang/StringBuilder;
        //    27: dup            
        //    28: invokespecial   java/lang/StringBuilder.<init>:()V
        //    31: astore_2       
        //    32: aload_1        
        //    33: invokevirtual   java/io/BufferedReader.readLine:()Ljava/lang/String;
        //    36: astore_3       
        //    37: aload_3        
        //    38: ifnull          57
        //    41: aload_2        
        //    42: aload_3        
        //    43: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //    46: pop            
        //    47: aload_2        
        //    48: bipush          10
        //    50: invokevirtual   java/lang/StringBuilder.append:(C)Ljava/lang/StringBuilder;
        //    53: pop            
        //    54: goto            32
        //    57: aload_1        
        //    58: invokevirtual   java/io/BufferedReader.close:()V
        //    61: aload_2        
        //    62: invokevirtual   java/lang/StringBuilder.toString:()Ljava/lang/String;
        //    65: areturn        
        //    66: astore_2       
        //    67: goto            73
        //    70: astore_2       
        //    71: aload_2        
        //    72: athrow         
        //    73: aload_1        
        //    74: invokevirtual   java/io/BufferedReader.close:()V
        //    77: aload_2        
        //    78: athrow         
        //    79: astore_1       
        //    80: goto            61
        //    83: astore_1       
        //    84: goto            77
        //    Exceptions:
        //  throws java.io.IOException
        //    Exceptions:
        //  Try           Handler
        //  Start  End    Start  End    Type                 
        //  -----  -----  -----  -----  ---------------------
        //  32     37     70     73     Ljava/lang/Exception;
        //  32     37     66     79     Any
        //  41     54     70     73     Ljava/lang/Exception;
        //  41     54     66     79     Any
        //  57     61     79     83     Ljava/lang/Exception;
        //  71     73     66     79     Any
        //  73     77     83     87     Ljava/lang/Exception;
        // 
        // The error that occurred was:
        // 
        // java.lang.IndexOutOfBoundsException: Index 48 out of bounds for length 48
        //     at java.base/jdk.internal.util.Preconditions.outOfBounds(Preconditions.java:64)
        //     at java.base/jdk.internal.util.Preconditions.outOfBoundsCheckIndex(Preconditions.java:70)
        //     at java.base/jdk.internal.util.Preconditions.checkIndex(Preconditions.java:266)
        //     at java.base/java.util.Objects.checkIndex(Objects.java:361)
        //     at java.base/java.util.ArrayList.get(ArrayList.java:427)
        //     at com.strobel.decompiler.ast.AstBuilder.convertToAst(AstBuilder.java:3362)
        //     at com.strobel.decompiler.ast.AstBuilder.build(AstBuilder.java:112)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:203)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:93)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethodBody(AstBuilder.java:868)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethod(AstBuilder.java:761)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addTypeMembers(AstBuilder.java:638)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeCore(AstBuilder.java:605)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeNoCache(AstBuilder.java:195)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createType(AstBuilder.java:162)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addType(AstBuilder.java:137)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.buildAst(JavaLanguage.java:71)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.decompileType(JavaLanguage.java:59)
        //     at com.strobel.decompiler.DecompilerDriver.decompileType(DecompilerDriver.java:333)
        //     at com.strobel.decompiler.DecompilerDriver.decompileJar(DecompilerDriver.java:254)
        //     at com.strobel.decompiler.DecompilerDriver.main(DecompilerDriver.java:144)
        // 
        throw new IllegalStateException("An error occurred while decompiling this method.");
    }
    
    @Nullable
    @WorkerThread
    private LottieComposition \u3007080() {
        final NetworkCache \u3007o\u3007 = this.\u3007o\u3007;
        if (\u3007o\u3007 == null) {
            return null;
        }
        final Pair<FileExtension, InputStream> \u3007080 = \u3007o\u3007.\u3007080(this.\u3007o00\u3007\u3007Oo);
        if (\u3007080 == null) {
            return null;
        }
        final FileExtension fileExtension = \u3007080.first;
        final InputStream in = \u3007080.second;
        LottieResult<LottieComposition> lottieResult;
        if (fileExtension == FileExtension.ZIP) {
            lottieResult = LottieCompositionFactory.\u3007\u30078O0\u30078(new ZipInputStream(in), this.\u3007o00\u3007\u3007Oo);
        }
        else {
            lottieResult = LottieCompositionFactory.oO80(in, this.\u3007o00\u3007\u3007Oo);
        }
        if (lottieResult.\u3007o00\u3007\u3007Oo() != null) {
            return lottieResult.\u3007o00\u3007\u3007Oo();
        }
        return null;
    }
    
    @WorkerThread
    private LottieResult<LottieComposition> \u3007o00\u3007\u3007Oo() {
        try {
            return this.\u3007o\u3007();
        }
        catch (final IOException ex) {
            return new LottieResult<LottieComposition>(ex);
        }
    }
    
    @WorkerThread
    private LottieResult<LottieComposition> \u3007o\u3007() throws IOException {
        final StringBuilder sb = new StringBuilder();
        sb.append("Fetching ");
        sb.append(this.\u3007o00\u3007\u3007Oo);
        Logger.\u3007080(sb.toString());
        final HttpURLConnection httpURLConnection = (HttpURLConnection)new URL(this.\u3007o00\u3007\u3007Oo).openConnection();
        httpURLConnection.setRequestMethod("GET");
        try {
            try {
                httpURLConnection.connect();
                if (httpURLConnection.getErrorStream() == null && httpURLConnection.getResponseCode() == 200) {
                    final LottieResult<LottieComposition> \u3007\u3007888 = this.\u3007\u3007888(httpURLConnection);
                    final StringBuilder sb2 = new StringBuilder();
                    sb2.append("Completed fetch from network. Success: ");
                    sb2.append(\u3007\u3007888.\u3007o00\u3007\u3007Oo() != null);
                    Logger.\u3007080(sb2.toString());
                    httpURLConnection.disconnect();
                    return \u3007\u3007888;
                }
                final String o\u30070 = this.o\u30070(httpURLConnection);
                final StringBuilder sb3 = new StringBuilder();
                sb3.append("Unable to fetch ");
                sb3.append(this.\u3007o00\u3007\u3007Oo);
                sb3.append(". Failed with ");
                sb3.append(httpURLConnection.getResponseCode());
                sb3.append("\n");
                sb3.append(o\u30070);
                final LottieResult lottieResult = new LottieResult<LottieComposition>(new IllegalArgumentException(sb3.toString()));
                httpURLConnection.disconnect();
                return (LottieResult<LottieComposition>)lottieResult;
            }
            finally {}
        }
        catch (final Exception ex) {
            final LottieResult lottieResult2 = new LottieResult(ex);
            httpURLConnection.disconnect();
            return lottieResult2;
        }
        httpURLConnection.disconnect();
    }
    
    @Nullable
    private LottieResult<LottieComposition> \u3007\u3007888(final HttpURLConnection httpURLConnection) throws IOException {
        String contentType;
        if ((contentType = httpURLConnection.getContentType()) == null) {
            contentType = "application/json";
        }
        FileExtension fileExtension;
        LottieResult<LottieComposition> lottieResult;
        if (contentType.contains("application/zip")) {
            Logger.\u3007080("Handling zip response.");
            fileExtension = FileExtension.ZIP;
            final NetworkCache \u3007o\u3007 = this.\u3007o\u3007;
            if (\u3007o\u3007 == null) {
                lottieResult = LottieCompositionFactory.\u3007\u30078O0\u30078(new ZipInputStream(httpURLConnection.getInputStream()), null);
            }
            else {
                lottieResult = LottieCompositionFactory.\u3007\u30078O0\u30078(new ZipInputStream(new FileInputStream(\u3007o\u3007.o\u30070(this.\u3007o00\u3007\u3007Oo, httpURLConnection.getInputStream(), fileExtension))), this.\u3007o00\u3007\u3007Oo);
            }
        }
        else {
            Logger.\u3007080("Received json response.");
            fileExtension = FileExtension.JSON;
            final NetworkCache \u3007o\u30072 = this.\u3007o\u3007;
            if (\u3007o\u30072 == null) {
                lottieResult = LottieCompositionFactory.oO80(httpURLConnection.getInputStream(), null);
            }
            else {
                lottieResult = LottieCompositionFactory.oO80(new FileInputStream(new File(\u3007o\u30072.o\u30070(this.\u3007o00\u3007\u3007Oo, httpURLConnection.getInputStream(), fileExtension).getAbsolutePath())), this.\u3007o00\u3007\u3007Oo);
            }
        }
        if (this.\u3007o\u3007 != null && lottieResult.\u3007o00\u3007\u3007Oo() != null) {
            this.\u3007o\u3007.Oo08(this.\u3007o00\u3007\u3007Oo, fileExtension);
        }
        return lottieResult;
    }
    
    @WorkerThread
    public LottieResult<LottieComposition> O8() {
        final LottieComposition \u3007080 = this.\u3007080();
        if (\u3007080 != null) {
            return new LottieResult<LottieComposition>(\u3007080);
        }
        final StringBuilder sb = new StringBuilder();
        sb.append("Animation for ");
        sb.append(this.\u3007o00\u3007\u3007Oo);
        sb.append(" not found in cache. Fetching from network.");
        Logger.\u3007080(sb.toString());
        return this.\u3007o00\u3007\u3007Oo();
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.airbnb.lottie.network;

import com.airbnb.lottie.utils.Logger;

public enum FileExtension
{
    private static final FileExtension[] $VALUES;
    
    JSON(".json"), 
    ZIP(".zip");
    
    public final String extension;
    
    private FileExtension(final String extension) {
        this.extension = extension;
    }
    
    public static FileExtension forFile(final String str) {
        for (final FileExtension fileExtension : values()) {
            if (str.endsWith(fileExtension.extension)) {
                return fileExtension;
            }
        }
        final StringBuilder sb = new StringBuilder();
        sb.append("Unable to find correct extension for ");
        sb.append(str);
        Logger.\u3007o\u3007(sb.toString());
        return FileExtension.JSON;
    }
    
    public String tempExtension() {
        final StringBuilder sb = new StringBuilder();
        sb.append(".temp");
        sb.append(this.extension);
        return sb.toString();
    }
    
    @Override
    public String toString() {
        return this.extension;
    }
}

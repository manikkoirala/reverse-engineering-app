// 
// Decompiled by Procyon v0.6.0
// 

package com.airbnb.lottie.network;

import java.io.OutputStream;
import androidx.annotation.WorkerThread;
import java.io.FileInputStream;
import androidx.core.util.Pair;
import java.io.IOException;
import java.io.FileOutputStream;
import java.io.InputStream;
import com.airbnb.lottie.utils.Logger;
import androidx.annotation.Nullable;
import java.io.FileNotFoundException;
import java.io.File;
import android.content.Context;

public class NetworkCache
{
    private final Context \u3007080;
    
    public NetworkCache(final Context context) {
        this.\u3007080 = context.getApplicationContext();
    }
    
    private File O8() {
        final File file = new File(this.\u3007080.getCacheDir(), "lottie_network_cache");
        if (file.isFile()) {
            file.delete();
        }
        if (!file.exists()) {
            file.mkdirs();
        }
        return file;
    }
    
    private static String \u3007o00\u3007\u3007Oo(String str, final FileExtension fileExtension, final boolean b) {
        final StringBuilder sb = new StringBuilder();
        sb.append("lottie_cache_");
        sb.append(str.replaceAll("\\W+", ""));
        if (b) {
            str = fileExtension.tempExtension();
        }
        else {
            str = fileExtension.extension;
        }
        sb.append(str);
        return sb.toString();
    }
    
    @Nullable
    private File \u3007o\u3007(final String s) throws FileNotFoundException {
        final File file = new File(this.O8(), \u3007o00\u3007\u3007Oo(s, FileExtension.JSON, false));
        if (file.exists()) {
            return file;
        }
        final File file2 = new File(this.O8(), \u3007o00\u3007\u3007Oo(s, FileExtension.ZIP, false));
        if (file2.exists()) {
            return file2;
        }
        return null;
    }
    
    void Oo08(String \u3007o00\u3007\u3007Oo, final FileExtension fileExtension) {
        \u3007o00\u3007\u3007Oo = \u3007o00\u3007\u3007Oo(\u3007o00\u3007\u3007Oo, fileExtension, true);
        final File file = new File(this.O8(), \u3007o00\u3007\u3007Oo);
        final File file2 = new File(file.getAbsolutePath().replace(".temp", ""));
        final boolean renameTo = file.renameTo(file2);
        final StringBuilder sb = new StringBuilder();
        sb.append("Copying temp file to real file (");
        sb.append(file2);
        sb.append(")");
        Logger.\u3007080(sb.toString());
        if (!renameTo) {
            final StringBuilder sb2 = new StringBuilder();
            sb2.append("Unable to rename cache file ");
            sb2.append(file.getAbsolutePath());
            sb2.append(" to ");
            sb2.append(file2.getAbsolutePath());
            sb2.append(".");
            Logger.\u3007o\u3007(sb2.toString());
        }
    }
    
    File o\u30070(String \u3007o00\u3007\u3007Oo, final InputStream inputStream, final FileExtension fileExtension) throws IOException {
        \u3007o00\u3007\u3007Oo = \u3007o00\u3007\u3007Oo((String)\u3007o00\u3007\u3007Oo, fileExtension, true);
        final File file = new File(this.O8(), (String)\u3007o00\u3007\u3007Oo);
        try {
            \u3007o00\u3007\u3007Oo = new FileOutputStream(file);
            try {
                final byte[] array = new byte[1024];
                while (true) {
                    final int read = inputStream.read(array);
                    if (read == -1) {
                        break;
                    }
                    ((OutputStream)\u3007o00\u3007\u3007Oo).write(array, 0, read);
                }
                ((OutputStream)\u3007o00\u3007\u3007Oo).flush();
                return file;
            }
            finally {
                ((OutputStream)\u3007o00\u3007\u3007Oo).close();
            }
        }
        finally {
            inputStream.close();
        }
    }
    
    @Nullable
    @WorkerThread
    Pair<FileExtension, InputStream> \u3007080(final String str) {
        try {
            final File \u3007o\u3007 = this.\u3007o\u3007(str);
            if (\u3007o\u3007 == null) {
                return null;
            }
            final FileInputStream fileInputStream = new FileInputStream(\u3007o\u3007);
            FileExtension fileExtension;
            if (\u3007o\u3007.getAbsolutePath().endsWith(".zip")) {
                fileExtension = FileExtension.ZIP;
            }
            else {
                fileExtension = FileExtension.JSON;
            }
            final StringBuilder sb = new StringBuilder();
            sb.append("Cache hit for ");
            sb.append(str);
            sb.append(" at ");
            sb.append(\u3007o\u3007.getAbsolutePath());
            Logger.\u3007080(sb.toString());
            return new Pair<FileExtension, InputStream>(fileExtension, fileInputStream);
        }
        catch (final FileNotFoundException ex) {
            return null;
        }
    }
}

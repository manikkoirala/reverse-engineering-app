// 
// Decompiled by Procyon v0.6.0
// 

package com.android.installreferrer.commons;

import android.util.Log;

public final class InstallReferrerCommons
{
    public static void logVerbose(final String s, final String s2) {
        Log.isLoggable(s, 2);
    }
    
    public static void logWarn(final String s, final String s2) {
        Log.isLoggable(s, 5);
    }
}

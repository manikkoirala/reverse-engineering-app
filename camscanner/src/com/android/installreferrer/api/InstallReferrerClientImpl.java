// 
// Decompiled by Procyon v0.6.0
// 

package com.android.installreferrer.api;

import android.os.BaseBundle;
import com.google.android.finsky.externalreferrer.IGetInstallReferrerService$Stub;
import android.os.IBinder;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Retention;
import android.content.pm.ServiceInfo;
import java.util.List;
import android.content.pm.ResolveInfo;
import android.content.ComponentName;
import android.content.Intent;
import android.os.RemoteException;
import android.os.Bundle;
import com.android.installreferrer.commons.InstallReferrerCommons;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager$NameNotFoundException;
import com.google.android.finsky.externalreferrer.IGetInstallReferrerService;
import android.content.Context;
import android.content.ServiceConnection;

class InstallReferrerClientImpl extends InstallReferrerClient
{
    private ServiceConnection O8;
    private int \u3007080;
    private final Context \u3007o00\u3007\u3007Oo;
    private IGetInstallReferrerService \u3007o\u3007;
    
    public InstallReferrerClientImpl(final Context context) {
        this.\u3007080 = 0;
        this.\u3007o00\u3007\u3007Oo = context.getApplicationContext();
    }
    
    private boolean \u3007o\u3007() {
        final PackageManager packageManager = this.\u3007o00\u3007\u3007Oo.getPackageManager();
        try {
            if (packageManager.getPackageInfo("com.android.vending", 128).versionCode >= 80837300) {
                return true;
            }
            return false;
        }
        catch (final PackageManager$NameNotFoundException ex) {
            return false;
        }
    }
    
    @Override
    public void endConnection() {
        this.\u3007080 = 3;
        if (this.O8 != null) {
            InstallReferrerCommons.logVerbose("InstallReferrerClient", "Unbinding from service.");
            this.\u3007o00\u3007\u3007Oo.unbindService(this.O8);
            this.O8 = null;
        }
        this.\u3007o\u3007 = null;
    }
    
    @Override
    public ReferrerDetails getInstallReferrer() throws RemoteException {
        if (this.isReady()) {
            final Bundle bundle = new Bundle();
            ((BaseBundle)bundle).putString("package_name", this.\u3007o00\u3007\u3007Oo.getPackageName());
            try {
                return new ReferrerDetails(this.\u3007o\u3007.\u3007o00\u3007\u3007Oo(bundle));
            }
            catch (final RemoteException ex) {
                InstallReferrerCommons.logWarn("InstallReferrerClient", "RemoteException getting install referrer information");
                this.\u3007080 = 0;
                throw ex;
            }
        }
        throw new IllegalStateException("Service not connected. Please start a connection before using the service.");
    }
    
    @Override
    public boolean isReady() {
        return this.\u3007080 == 2 && this.\u3007o\u3007 != null && this.O8 != null;
    }
    
    @Override
    public void startConnection(final InstallReferrerStateListener installReferrerStateListener) {
        if (this.isReady()) {
            InstallReferrerCommons.logVerbose("InstallReferrerClient", "Service connection is valid. No need to re-initialize.");
            installReferrerStateListener.onInstallReferrerSetupFinished(0);
            return;
        }
        final int \u3007080 = this.\u3007080;
        if (\u3007080 == 1) {
            InstallReferrerCommons.logWarn("InstallReferrerClient", "Client is already in the process of connecting to the service.");
            installReferrerStateListener.onInstallReferrerSetupFinished(3);
            return;
        }
        if (\u3007080 == 3) {
            InstallReferrerCommons.logWarn("InstallReferrerClient", "Client was already closed and can't be reused. Please create another instance.");
            installReferrerStateListener.onInstallReferrerSetupFinished(3);
            return;
        }
        InstallReferrerCommons.logVerbose("InstallReferrerClient", "Starting install referrer service setup.");
        final Intent intent = new Intent("com.google.android.finsky.BIND_GET_INSTALL_REFERRER_SERVICE");
        intent.setComponent(new ComponentName("com.android.vending", "com.google.android.finsky.externalreferrer.GetInstallReferrerService"));
        final List queryIntentServices = this.\u3007o00\u3007\u3007Oo.getPackageManager().queryIntentServices(intent, 0);
        if (queryIntentServices != null && !queryIntentServices.isEmpty()) {
            final ServiceInfo serviceInfo = queryIntentServices.get(0).serviceInfo;
            if (serviceInfo != null) {
                final String packageName = serviceInfo.packageName;
                final String name = serviceInfo.name;
                if ("com.android.vending".equals(packageName) && name != null && this.\u3007o\u3007()) {
                    final Intent intent2 = new Intent(intent);
                    final InstallReferrerServiceConnection o8 = new InstallReferrerServiceConnection(installReferrerStateListener);
                    this.O8 = (ServiceConnection)o8;
                    try {
                        if (this.\u3007o00\u3007\u3007Oo.bindService(intent2, (ServiceConnection)o8, 1)) {
                            InstallReferrerCommons.logVerbose("InstallReferrerClient", "Service was bonded successfully.");
                            return;
                        }
                        InstallReferrerCommons.logWarn("InstallReferrerClient", "Connection to service is blocked.");
                        this.\u3007080 = 0;
                        installReferrerStateListener.onInstallReferrerSetupFinished(1);
                        return;
                    }
                    catch (final SecurityException ex) {
                        InstallReferrerCommons.logWarn("InstallReferrerClient", "No permission to connect to service.");
                        this.\u3007080 = 0;
                        installReferrerStateListener.onInstallReferrerSetupFinished(4);
                        return;
                    }
                }
                InstallReferrerCommons.logWarn("InstallReferrerClient", "Play Store missing or incompatible. Version 8.3.73 or later required.");
                this.\u3007080 = 0;
                installReferrerStateListener.onInstallReferrerSetupFinished(2);
                return;
            }
        }
        this.\u3007080 = 0;
        InstallReferrerCommons.logVerbose("InstallReferrerClient", "Install Referrer service unavailable on device.");
        installReferrerStateListener.onInstallReferrerSetupFinished(2);
    }
    
    @Retention(RetentionPolicy.SOURCE)
    public @interface ClientState {
        public static final int CLOSED = 3;
        public static final int CONNECTED = 2;
        public static final int CONNECTING = 1;
        public static final int DISCONNECTED = 0;
    }
    
    private final class InstallReferrerServiceConnection implements ServiceConnection
    {
        private final InstallReferrerStateListener o0;
        final InstallReferrerClientImpl \u3007OOo8\u30070;
        
        private InstallReferrerServiceConnection(final InstallReferrerClientImpl \u3007oOo8\u30070, final InstallReferrerStateListener o0) {
            this.\u3007OOo8\u30070 = \u3007oOo8\u30070;
            if (o0 != null) {
                this.o0 = o0;
                return;
            }
            throw new RuntimeException("Please specify a listener to know when setup is done.");
        }
        
        public void onServiceConnected(final ComponentName componentName, final IBinder binder) {
            InstallReferrerCommons.logVerbose("InstallReferrerClient", "Install Referrer service connected.");
            this.\u3007OOo8\u30070.\u3007o\u3007 = IGetInstallReferrerService$Stub.\u300700\u30078(binder);
            this.\u3007OOo8\u30070.\u3007080 = 2;
            this.o0.onInstallReferrerSetupFinished(0);
        }
        
        public void onServiceDisconnected(final ComponentName componentName) {
            InstallReferrerCommons.logWarn("InstallReferrerClient", "Install Referrer service disconnected.");
            this.\u3007OOo8\u30070.\u3007o\u3007 = null;
            this.\u3007OOo8\u30070.\u3007080 = 0;
            this.o0.onInstallReferrerServiceDisconnected();
        }
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.android.dx.stock;

import java.util.Collection;
import java.util.Comparator;
import java.io.IOException;
import java.util.Arrays;
import java.lang.reflect.UndeclaredThrowableException;
import java.lang.reflect.InvocationTargetException;
import com.android.dx.Comparison;
import com.android.dx.Label;
import com.android.dx.FieldId;
import com.android.dx.DexMaker;
import com.android.dx.Local;
import com.android.dx.Code;
import java.util.HashSet;
import java.lang.reflect.Field;
import java.lang.reflect.Constructor;
import java.lang.reflect.Modifier;
import java.util.Set;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.io.File;
import com.android.dx.MethodId;
import com.android.dx.TypeId;
import java.util.Map;

public final class ProxyBuilder<T>
{
    private static final Map<TypeId<?>, MethodId<?, ?>> OO0o\u3007\u3007;
    private static final Map<Class<?>, MethodId<?, ?>> Oooo8o0\u3007;
    private static final Map<ProxiedClass<?>, Class<?>> \u30078o8o\u3007;
    private static final Map<Class<?>, Class<?>> \u3007O8o08O;
    private File O8;
    private boolean OO0o\u3007\u3007\u3007\u30070;
    private Class<?>[] Oo08;
    private Method[] oO80;
    private Object[] o\u30070;
    private final Class<T> \u3007080;
    private boolean \u300780\u3007808\u3007O;
    private ClassLoader \u3007o00\u3007\u3007Oo;
    private InvocationHandler \u3007o\u3007;
    private List<Class<?>> \u3007\u3007888;
    
    static {
        \u30078o8o\u3007 = Collections.synchronizedMap(new HashMap<ProxiedClass<?>, Class<?>>());
        final HashMap \u3007o8o08O = new HashMap();
        (\u3007O8o08O = \u3007o8o08O).put(Boolean.TYPE, Boolean.class);
        \u3007o8o08O.put(Integer.TYPE, Integer.class);
        \u3007o8o08O.put(Byte.TYPE, Byte.class);
        \u3007o8o08O.put(Long.TYPE, Long.class);
        \u3007o8o08O.put(Short.TYPE, Short.class);
        \u3007o8o08O.put(Float.TYPE, Float.class);
        \u3007o8o08O.put(Double.TYPE, Double.class);
        \u3007o8o08O.put(Character.TYPE, Character.class);
        OO0o\u3007\u3007 = new HashMap<TypeId<?>, MethodId<?, ?>>();
        for (final Map.Entry<Class<T>, V> entry : \u3007o8o08O.entrySet()) {
            final TypeId<Object> \u3007080 = TypeId.\u3007080((Class<Object>)entry.getKey());
            final TypeId<R> \u300781 = TypeId.\u3007080((Class<R>)entry.getValue());
            ProxyBuilder.OO0o\u3007\u3007.put(\u3007080, \u300781.Oo08((TypeId<Object>)\u300781, "valueOf", \u3007080));
        }
        final HashMap oooo8o0\u3007 = new HashMap();
        oooo8o0\u3007.put(Boolean.TYPE, TypeId.\u3007080(Boolean.class).Oo08(TypeId.O8, "booleanValue", (TypeId<?>[])new TypeId[0]));
        oooo8o0\u3007.put(Integer.TYPE, TypeId.\u3007080(Integer.class).Oo08(TypeId.\u300780\u3007808\u3007O, "intValue", (TypeId<?>[])new TypeId[0]));
        oooo8o0\u3007.put(Byte.TYPE, TypeId.\u3007080(Byte.class).Oo08(TypeId.Oo08, "byteValue", (TypeId<?>[])new TypeId[0]));
        oooo8o0\u3007.put(Long.TYPE, TypeId.\u3007080(Long.class).Oo08(TypeId.OO0o\u3007\u3007\u3007\u30070, "longValue", (TypeId<?>[])new TypeId[0]));
        oooo8o0\u3007.put(Short.TYPE, TypeId.\u3007080(Short.class).Oo08(TypeId.\u30078o8o\u3007, "shortValue", (TypeId<?>[])new TypeId[0]));
        oooo8o0\u3007.put(Float.TYPE, TypeId.\u3007080(Float.class).Oo08(TypeId.oO80, "floatValue", (TypeId<?>[])new TypeId[0]));
        oooo8o0\u3007.put(Double.TYPE, TypeId.\u3007080(Double.class).Oo08(TypeId.\u3007\u3007888, "doubleValue", (TypeId<?>[])new TypeId[0]));
        oooo8o0\u3007.put(Character.TYPE, TypeId.\u3007080(Character.class).Oo08(TypeId.o\u30070, "charValue", (TypeId<?>[])new TypeId[0]));
        Oooo8o0\u3007 = oooo8o0\u3007;
    }
    
    private ProxyBuilder(final Class<T> \u3007080) {
        this.\u3007o00\u3007\u3007Oo = ProxyBuilder.class.getClassLoader();
        this.Oo08 = new Class[0];
        this.o\u30070 = new Object[0];
        this.\u3007\u3007888 = new ArrayList<Class<?>>();
        this.\u3007080 = \u3007080;
    }
    
    private static void O8(final boolean b, final String s) {
        if (b) {
            return;
        }
        throw new IllegalArgumentException(s);
    }
    
    private void OO0o\u3007\u3007(final Set<MethodSetEntry> set, final Set<MethodSetEntry> set2, final Class<?> clazz) {
        final Method[] declaredMethods = clazz.getDeclaredMethods();
        final int length = declaredMethods.length;
        final int n = 0;
        for (final Method method : declaredMethods) {
            Label_0190: {
                if ((method.getModifiers() & 0x10) != 0x0) {
                    final MethodSetEntry methodSetEntry = new MethodSetEntry(method);
                    set2.add(methodSetEntry);
                    set.remove(methodSetEntry);
                }
                else if ((method.getModifiers() & 0x8) == 0x0) {
                    if (!Modifier.isPublic(method.getModifiers()) && !Modifier.isProtected(method.getModifiers())) {
                        if (!this.\u300780\u3007808\u3007O) {
                            break Label_0190;
                        }
                        if (Modifier.isPrivate(method.getModifiers())) {
                            break Label_0190;
                        }
                    }
                    if (!method.getName().equals("finalize") || method.getParameterTypes().length != 0) {
                        final MethodSetEntry methodSetEntry2 = new MethodSetEntry(method);
                        if (!set2.contains(methodSetEntry2)) {
                            set.add(methodSetEntry2);
                        }
                    }
                }
            }
        }
        if (clazz.isInterface()) {
            final Class[] interfaces = clazz.getInterfaces();
            for (int length2 = interfaces.length, j = n; j < length2; ++j) {
                this.OO0o\u3007\u3007(set, set2, interfaces[j]);
            }
        }
    }
    
    private static <T> Constructor<T>[] OO0o\u3007\u3007\u3007\u30070(final Class<T> clazz) {
        return clazz.getDeclaredConstructors();
    }
    
    private static TypeId<?>[] Oo08(final Class<?>[] array) {
        final TypeId[] array2 = new TypeId[array.length];
        for (int i = 0; i < array.length; ++i) {
            array2[i] = TypeId.\u3007080(array[i]);
        }
        return array2;
    }
    
    public static void OoO8(final Object obj, final InvocationHandler value) {
        try {
            final Field declaredField = obj.getClass().getDeclaredField("$__handler");
            declaredField.setAccessible(true);
            declaredField.set(obj, value);
        }
        catch (final IllegalAccessException detailMessage) {
            throw new AssertionError((Object)detailMessage);
        }
        catch (final NoSuchFieldException cause) {
            throw new IllegalArgumentException("Not a valid proxy instance", cause);
        }
    }
    
    private Method[] Oooo8o0\u3007() {
        final HashSet set = new HashSet();
        final HashSet set2 = new HashSet();
        for (Class<? super T> clazz = this.\u3007080; clazz != null; clazz = clazz.getSuperclass()) {
            this.OO0o\u3007\u3007(set, set2, clazz);
        }
        Class<T> clazz2 = this.\u3007080;
        int n;
        while (true) {
            n = 0;
            int i = 0;
            if (clazz2 == null) {
                break;
            }
            for (Class[] interfaces = clazz2.getInterfaces(); i < interfaces.length; ++i) {
                this.OO0o\u3007\u3007(set, set2, interfaces[i]);
            }
            clazz2 = clazz2.getSuperclass();
        }
        final Iterator<Class<?>> iterator = this.\u3007\u3007888.iterator();
        while (iterator.hasNext()) {
            this.OO0o\u3007\u3007(set, set2, iterator.next());
        }
        final Method[] array = new Method[set.size()];
        final Iterator iterator2 = set.iterator();
        int n2 = n;
        while (iterator2.hasNext()) {
            array[n2] = ((MethodSetEntry)iterator2.next()).O8;
            ++n2;
        }
        return array;
    }
    
    private static void o800o8O(final Class<?> clazz, final Method[] value) {
        try {
            final Field declaredField = clazz.getDeclaredField("$__methodArray");
            declaredField.setAccessible(true);
            declaredField.set(null, value);
        }
        catch (final IllegalAccessException detailMessage) {
            throw new AssertionError((Object)detailMessage);
        }
        catch (final NoSuchFieldException detailMessage2) {
            throw new AssertionError((Object)detailMessage2);
        }
    }
    
    private static void oO80(final Code code, final Class obj, final Local local, final Local local2, final Local local3) {
        if (ProxyBuilder.Oooo8o0\u3007.containsKey(obj)) {
            code.o\u30070(local3, local);
            code.o800o8O(\u3007\u3007808\u3007(obj), local2, local3, (Local<?>[])new Local[0]);
            code.o\u3007\u30070\u3007(local2);
        }
        else if (Void.TYPE.equals(obj)) {
            code.OOO\u3007O0();
        }
        else {
            code.o\u30070(local2, local);
            code.o\u3007\u30070\u3007(local2);
        }
    }
    
    private static void oo88o8O(final Code code, final Method obj, final Local<String> local, final Local<AbstractMethodError> local2) {
        final MethodId<AbstractMethodError, Void> \u3007o\u3007 = TypeId.\u3007080(AbstractMethodError.class).\u3007o\u3007(TypeId.Oooo8o0\u3007);
        final StringBuilder sb = new StringBuilder();
        sb.append("'");
        sb.append(obj);
        sb.append("' cannot be called");
        code.oo88o8O(local, sb.toString());
        code.O8ooOoo\u3007((Local<Object>)local2, (MethodId<Object, Void>)\u3007o\u3007, local);
        code.\u300700\u30078(local2);
    }
    
    public static <T> ProxyBuilder<T> o\u30070(final Class<T> clazz) {
        return new ProxyBuilder<T>(clazz);
    }
    
    private static Local<?> \u3007080(final Code code, final Local<?> local, final Local<Object> local2) {
        final MethodId methodId = ProxyBuilder.OO0o\u3007\u3007.get(local.getType());
        if (methodId == null) {
            return local;
        }
        code.\u30070\u3007O0088o((MethodId<?, Object>)methodId, local2, local);
        return local2;
    }
    
    private Class<? extends T> \u30070\u3007O0088o(final ClassLoader classLoader, final String name) throws ClassNotFoundException {
        return (Class<? extends T>)classLoader.loadClass(name);
    }
    
    private static <T, G extends T> void \u300780\u3007808\u3007O(final DexMaker dexMaker, final TypeId<G> typeId, final TypeId<T> typeId2, final Class<T> clazz) {
        final TypeId<InvocationHandler> \u3007080 = TypeId.\u3007080(InvocationHandler.class);
        final TypeId<Method[]> \u300781 = TypeId.\u3007080(Method[].class);
        dexMaker.\u3007o00\u3007\u3007Oo(typeId.O8((TypeId<Object>)\u3007080, "$__handler"), 2, null);
        dexMaker.\u3007o00\u3007\u3007Oo(typeId.O8((TypeId<Object>)\u300781, "$__methodArray"), 10, null);
        for (final Constructor<T> constructor : OO0o\u3007\u3007\u3007\u30070(clazz)) {
            if (constructor.getModifiers() != 16) {
                final TypeId<?>[] oo08 = Oo08(constructor.getParameterTypes());
                final Code \u300782 = dexMaker.\u3007080(typeId.\u3007o\u3007(oo08), 1);
                final Local<G> oo0o\u3007\u3007 = \u300782.OO0o\u3007\u3007(typeId);
                final int length2 = oo08.length;
                final Local[] array = new Local[length2];
                for (int j = 0; j < length2; ++j) {
                    array[j] = \u300782.\u3007O8o08O(j, oo08[j]);
                }
                \u300782.\u3007O00(typeId2.\u3007o\u3007(oo08), null, (Local<? extends T>)oo0o\u3007\u3007, (Local<?>[])array);
                \u300782.OOO\u3007O0();
            }
        }
    }
    
    private TypeId<?>[] \u30078o8o\u3007() {
        final TypeId[] array = new TypeId[this.\u3007\u3007888.size()];
        final Iterator<Class<?>> iterator = this.\u3007\u3007888.iterator();
        int n = 0;
        while (iterator.hasNext()) {
            array[n] = TypeId.\u3007080(iterator.next());
            ++n;
        }
        return array;
    }
    
    private static void \u3007O00(final MethodId methodId, final Code code, final Local local, final Local[] array, final Local local2) {
        code.OoO8((MethodId<Object, Object>)methodId, local2, local, (Local<?>[])array);
    }
    
    private static String \u3007O888o0o(final Method method) {
        final String name = method.getReturnType().getName();
        final StringBuilder sb = new StringBuilder();
        sb.append("super$");
        sb.append(method.getName());
        sb.append("$");
        sb.append(name.replace('.', '_').replace('[', '_').replace(';', '_'));
        return sb.toString();
    }
    
    private static <T> String \u3007O8o08O(final Class<T> clazz, final List<Class<?>> list) {
        final String hexString = Integer.toHexString(list.hashCode());
        final StringBuilder sb = new StringBuilder();
        sb.append(clazz.getName().replace(".", "/"));
        sb.append("_");
        sb.append(hexString);
        sb.append("_Proxy");
        return sb.toString();
    }
    
    private static MethodId<?, ?> \u3007\u3007808\u3007(final Class<?> clazz) {
        return ProxyBuilder.Oooo8o0\u3007.get(clazz);
    }
    
    private static <T, G extends T> void \u3007\u3007888(final DexMaker dexMaker, final TypeId<G> typeId, final Method[] array, final TypeId<T> typeId2) {
        final TypeId<InvocationHandler> \u3007080 = TypeId.\u3007080(InvocationHandler.class);
        final TypeId<Method[]> \u300781 = TypeId.\u3007080(Method[].class);
        final FieldId<G, T> o8 = typeId.O8((TypeId<T>)\u3007080, "$__handler");
        final FieldId<G, T> o9 = typeId.O8((TypeId<T>)\u300781, "$__methodArray");
        final TypeId<Method> \u300782 = TypeId.\u3007080(Method.class);
        final TypeId<Object[]> \u300783 = TypeId.\u3007080(Object[].class);
        final TypeId<Object> oo0o\u3007\u3007 = TypeId.OO0o\u3007\u3007;
        final MethodId<InvocationHandler, R> oo08 = \u3007080.Oo08((TypeId<R>)oo0o\u3007\u3007, "invoke", oo0o\u3007\u3007, \u300782, \u300783);
        for (int i = 0; i < array.length; ++i) {
            final Method method = array[i];
            final String name = method.getName();
            final Class<?>[] parameterTypes = method.getParameterTypes();
            final int length = parameterTypes.length;
            final TypeId[] array2 = new TypeId[length];
            for (int j = 0; j < length; ++j) {
                array2[j] = TypeId.\u3007080(parameterTypes[j]);
            }
            final Class<?> returnType = method.getReturnType();
            final TypeId<R> \u300784 = TypeId.\u3007080(returnType);
            final MethodId<G, T> oo9 = typeId.Oo08((TypeId<T>)\u300784, name, (TypeId<?>[])array2);
            final TypeId<AbstractMethodError> \u300785 = TypeId.\u3007080(AbstractMethodError.class);
            final Code \u300786 = dexMaker.\u3007080(oo9, 1);
            final Local<G> oo0o\u3007\u30072 = \u300786.OO0o\u3007\u3007(typeId);
            final Local<V> \u3007oOO8O8 = \u300786.\u3007oOO8O8((TypeId<V>)\u3007080);
            final TypeId<Object> oo0o\u3007\u30073 = TypeId.OO0o\u3007\u3007;
            final Local<Object> \u3007oOO8O9 = \u300786.\u3007oOO8O8(oo0o\u3007\u30073);
            final TypeId<Integer> \u300780\u3007808\u3007O = TypeId.\u300780\u3007808\u3007O;
            final Local<T> \u3007oOO8O10 = \u300786.\u3007oOO8O8((TypeId<T>)\u300780\u3007808\u3007O);
            final Local<T> \u3007oOO8O11 = \u300786.\u3007oOO8O8((TypeId<T>)\u300783);
            final Local<T> \u3007oOO8O12 = \u300786.\u3007oOO8O8((TypeId<T>)\u300780\u3007808\u3007O);
            final Local<Object> \u3007oOO8O13 = \u300786.\u3007oOO8O8(oo0o\u3007\u30073);
            final Local<R> \u3007oOO8O14 = \u300786.\u3007oOO8O8(\u300784);
            final Local<V> \u3007oOO8O15 = \u300786.\u3007oOO8O8((TypeId<V>)\u300781);
            final Local<Object> \u3007oOO8O16 = \u300786.\u3007oOO8O8((TypeId<Object>)\u300782);
            final Local<T> \u3007oOO8O17 = \u300786.\u3007oOO8O8((TypeId<T>)\u300780\u3007808\u3007O);
            final Class clazz = ProxyBuilder.\u3007O8o08O.get(returnType);
            Local<Object> \u3007oOO8O18;
            if (clazz != null) {
                \u3007oOO8O18 = \u300786.\u3007oOO8O8((TypeId<Object>)TypeId.\u3007080((Class<T>)clazz));
            }
            else {
                \u3007oOO8O18 = null;
            }
            final Local<V> \u3007oOO8O19 = \u300786.\u3007oOO8O8((TypeId<V>)\u3007080);
            Local[] array3;
            Object \u3007oOO8O20;
            Object oo10;
            Local<String> \u3007oOO8O21;
            Local<AbstractMethodError> \u3007oOO8O22;
            if ((method.getModifiers() & 0x400) == 0x0) {
                array3 = new Local[parameterTypes.length];
                \u3007oOO8O20 = \u300786.\u3007oOO8O8(\u300784);
                oo10 = typeId2.Oo08((TypeId<T>)\u300784, name, (TypeId<?>[])array2);
                \u3007oOO8O21 = null;
                \u3007oOO8O22 = null;
            }
            else {
                \u3007oOO8O21 = \u300786.\u3007oOO8O8(TypeId.Oooo8o0\u3007);
                \u3007oOO8O22 = \u300786.\u3007oOO8O8(\u300785);
                \u3007oOO8O20 = null;
                array3 = null;
                oo10 = null;
            }
            \u300786.oo88o8O((Local<Integer>)\u3007oOO8O17, i);
            \u300786.oo\u3007((FieldId<?, ? extends V>)o9, \u3007oOO8O15);
            \u300786.O8(\u3007oOO8O16, \u3007oOO8O15, (Local<Integer>)\u3007oOO8O17);
            \u300786.oo88o8O((Local<Integer>)\u3007oOO8O12, length);
            \u300786.O\u30078O8\u3007008((Local<Object>)\u3007oOO8O11, (Local<Integer>)\u3007oOO8O12);
            \u300786.Oooo8o0\u3007((FieldId<G, ? extends V>)o8, \u3007oOO8O8, oo0o\u3007\u30072);
            \u300786.oo88o8O(\u3007oOO8O19, (V)null);
            final Label label = new Label();
            \u300786.\u300780\u3007808\u3007O(Comparison.EQ, label, \u3007oOO8O19, \u3007oOO8O8);
            for (int n = length, k = 0; k < n; ++k) {
                \u300786.oo88o8O((Local<Integer>)\u3007oOO8O10, k);
                \u300786.Oo08(\u3007oOO8O11, (Local<Integer>)\u3007oOO8O10, \u3007080(\u300786, \u300786.\u3007O8o08O(k, (TypeId<?>)array2[k]), \u3007oOO8O13));
            }
            \u300786.\u3007\u30078O0\u30078((MethodId<Object, Object>)oo08, \u3007oOO8O9, \u3007oOO8O8, oo0o\u3007\u30072, \u3007oOO8O16, \u3007oOO8O11);
            oO80(\u300786, returnType, \u3007oOO8O9, \u3007oOO8O14, \u3007oOO8O18);
            \u300786.o\u3007O8\u3007\u3007o(label);
            if ((method.getModifiers() & 0x400) == 0x0) {
                for (int l = 0; l < array3.length; ++l) {
                    array3[l] = \u300786.\u3007O8o08O(l, (TypeId<Object>)array2[l]);
                }
                if (Void.TYPE.equals(returnType)) {
                    \u300786.OoO8((MethodId<T, T>)oo10, null, (Local<? extends T>)oo0o\u3007\u30072, (Local<?>[])array3);
                    \u300786.OOO\u3007O0();
                }
                else {
                    \u3007O00((MethodId)oo10, \u300786, oo0o\u3007\u30072, array3, (Local)\u3007oOO8O20);
                    \u300786.o\u3007\u30070\u3007((Local<?>)\u3007oOO8O20);
                }
            }
            else {
                oo88o8O(\u300786, method, \u3007oOO8O21, \u3007oOO8O22);
            }
            final Code \u300787 = dexMaker.\u3007080(typeId.Oo08((TypeId<T>)\u300784, \u3007O888o0o(method), (TypeId<?>[])array2), 1);
            if ((method.getModifiers() & 0x400) == 0x0) {
                final Local<G> oo0o\u3007\u30074 = \u300787.OO0o\u3007\u3007(typeId);
                final int length2 = parameterTypes.length;
                final Local[] array4 = new Local[length2];
                for (int n2 = 0; n2 < length2; ++n2) {
                    array4[n2] = \u300787.\u3007O8o08O(n2, (TypeId<Object>)array2[n2]);
                }
                if (Void.TYPE.equals(returnType)) {
                    \u300787.OoO8((MethodId<T, T>)oo10, null, (Local<? extends T>)oo0o\u3007\u30074, (Local<?>[])array4);
                    \u300787.OOO\u3007O0();
                }
                else {
                    final Local<R> \u3007oOO8O23 = \u300787.\u3007oOO8O8(\u300784);
                    \u3007O00((MethodId)oo10, \u300787, oo0o\u3007\u30074, array4, \u3007oOO8O23);
                    \u300787.o\u3007\u30070\u3007(\u3007oOO8O23);
                }
            }
            else {
                oo88o8O(\u300787, method, \u300787.\u3007oOO8O8(TypeId.Oooo8o0\u3007), \u300787.\u3007oOO8O8(\u300785));
            }
        }
    }
    
    private static RuntimeException \u3007\u30078O0\u30078(final InvocationTargetException ex) {
        final Throwable cause = ex.getCause();
        if (cause instanceof Error) {
            throw (Error)cause;
        }
        if (cause instanceof RuntimeException) {
            throw (RuntimeException)cause;
        }
        throw new UndeclaredThrowableException(cause);
    }
    
    public ProxyBuilder<T> \u3007O\u3007(final InvocationHandler \u3007o\u3007) {
        this.\u3007o\u3007 = \u3007o\u3007;
        return this;
    }
    
    public T \u3007o00\u3007\u3007Oo() throws IOException {
        final InvocationHandler \u3007o\u3007 = this.\u3007o\u3007;
        final boolean b = true;
        O8(\u3007o\u3007 != null, "handler == null");
        O8(this.Oo08.length == this.o\u30070.length && b, "constructorArgValues.length != constructorArgTypes.length");
        final Class<? extends T> \u3007o\u30072 = this.\u3007o\u3007();
        try {
            final Constructor<? extends T> constructor = \u3007o\u30072.getConstructor(this.Oo08);
            try {
                final T instance = (T)constructor.newInstance(this.o\u30070);
                OoO8(instance, this.\u3007o\u3007);
                return instance;
            }
            catch (final InvocationTargetException ex) {
                throw \u3007\u30078O0\u30078(ex);
            }
            catch (final IllegalAccessException detailMessage) {
                throw new AssertionError((Object)detailMessage);
            }
            catch (final InstantiationException detailMessage2) {
                throw new AssertionError((Object)detailMessage2);
            }
        }
        catch (final NoSuchMethodException ex2) {
            final StringBuilder sb = new StringBuilder();
            sb.append("No constructor for ");
            sb.append(this.\u3007080.getName());
            sb.append(" with parameter types ");
            sb.append(Arrays.toString(this.Oo08));
            throw new IllegalArgumentException(sb.toString());
        }
    }
    
    public Class<? extends T> \u3007o\u3007() throws IOException {
        ClassLoader classLoader;
        if (this.\u300780\u3007808\u3007O) {
            classLoader = this.\u3007080.getClassLoader();
        }
        else {
            classLoader = this.\u3007o00\u3007\u3007Oo;
        }
        final ProxiedClass proxiedClass = new ProxiedClass((Class)this.\u3007080, (List)this.\u3007\u3007888, classLoader, this.\u300780\u3007808\u3007O);
        final Map<ProxiedClass<?>, Class<?>> \u30078o8o\u3007 = ProxyBuilder.\u30078o8o\u3007;
        final Class clazz = \u30078o8o\u3007.get(proxiedClass);
        if (clazz != null) {
            return clazz;
        }
        final DexMaker dexMaker = new DexMaker();
        final String \u3007o8o08O = \u3007O8o08O(this.\u3007080, this.\u3007\u3007888);
        final StringBuilder sb = new StringBuilder();
        sb.append("L");
        sb.append(\u3007o8o08O);
        sb.append(";");
        final TypeId<Object> \u3007o00\u3007\u3007Oo = (TypeId<Object>)TypeId.\u3007o00\u3007\u3007Oo(sb.toString());
        final TypeId<T> \u3007080 = TypeId.\u3007080(this.\u3007080);
        \u300780\u3007808\u3007O(dexMaker, (TypeId<G>)\u3007o00\u3007\u3007Oo, \u3007080, this.\u3007080);
        Method[] a;
        if ((a = this.oO80) == null) {
            a = this.Oooo8o0\u3007();
        }
        Arrays.sort(a, new Comparator<Method>(this) {
            final ProxyBuilder o0;
            
            public int \u3007080(final Method method, final Method method2) {
                final StringBuilder sb = new StringBuilder();
                sb.append(method.getDeclaringClass());
                sb.append(method.getName());
                sb.append(Arrays.toString(method.getParameterTypes()));
                sb.append(method.getReturnType());
                final String string = sb.toString();
                final StringBuilder sb2 = new StringBuilder();
                sb2.append(method2.getDeclaringClass());
                sb2.append(method2.getName());
                sb2.append(Arrays.toString(method2.getParameterTypes()));
                sb2.append(method2.getReturnType());
                return string.compareTo(sb2.toString());
            }
        });
        \u3007\u3007888(dexMaker, (TypeId<G>)\u3007o00\u3007\u3007Oo, a, \u3007080);
        final StringBuilder sb2 = new StringBuilder();
        sb2.append(\u3007o8o08O);
        sb2.append(".generated");
        dexMaker.\u3007o\u3007(\u3007o00\u3007\u3007Oo, sb2.toString(), 1, \u3007080, this.\u30078o8o\u3007());
        if (this.\u300780\u3007808\u3007O) {
            dexMaker.OO0o\u3007\u3007\u3007\u30070(classLoader);
        }
        if (this.OO0o\u3007\u3007\u3007\u30070) {
            dexMaker.\u300780\u3007808\u3007O();
        }
        ClassLoader classLoader2;
        if (this.\u300780\u3007808\u3007O) {
            classLoader2 = dexMaker.Oo08(null, this.O8);
        }
        else {
            classLoader2 = dexMaker.Oo08(this.\u3007o00\u3007\u3007Oo, this.O8);
        }
        try {
            final Class<? extends T> \u30070\u3007O0088o = this.\u30070\u3007O0088o(classLoader2, \u3007o8o08O);
            o800o8O(\u30070\u3007O0088o, a);
            \u30078o8o\u3007.put(proxiedClass, \u30070\u3007O0088o);
            return \u30070\u3007O0088o;
        }
        catch (final ClassNotFoundException detailMessage) {
            throw new AssertionError((Object)detailMessage);
        }
        catch (final IllegalAccessError cause) {
            final StringBuilder sb3 = new StringBuilder();
            sb3.append("cannot proxy inaccessible class ");
            sb3.append(this.\u3007080);
            throw new UnsupportedOperationException(sb3.toString(), cause);
        }
    }
    
    public static class MethodSetEntry
    {
        public final Method O8;
        public final String \u3007080;
        public final Class<?>[] \u3007o00\u3007\u3007Oo;
        public final Class<?> \u3007o\u3007;
        
        public MethodSetEntry(final Method o8) {
            this.O8 = o8;
            this.\u3007080 = o8.getName();
            this.\u3007o00\u3007\u3007Oo = o8.getParameterTypes();
            this.\u3007o\u3007 = o8.getReturnType();
        }
        
        @Override
        public boolean equals(final Object o) {
            final boolean b = o instanceof MethodSetEntry;
            boolean b3;
            final boolean b2 = b3 = false;
            if (b) {
                final MethodSetEntry methodSetEntry = (MethodSetEntry)o;
                b3 = b2;
                if (this.\u3007080.equals(methodSetEntry.\u3007080)) {
                    b3 = b2;
                    if (this.\u3007o\u3007.equals(methodSetEntry.\u3007o\u3007)) {
                        b3 = b2;
                        if (Arrays.equals(this.\u3007o00\u3007\u3007Oo, methodSetEntry.\u3007o00\u3007\u3007Oo)) {
                            b3 = true;
                        }
                    }
                }
            }
            return b3;
        }
        
        @Override
        public int hashCode() {
            final int n = 527 + this.\u3007080.hashCode() + 17;
            final int n2 = n + (n * 31 + this.\u3007o\u3007.hashCode());
            return n2 + (n2 * 31 + Arrays.hashCode(this.\u3007o00\u3007\u3007Oo));
        }
    }
    
    private static class ProxiedClass<U>
    {
        final boolean O8;
        final Class<U> \u3007080;
        final List<Class<?>> \u3007o00\u3007\u3007Oo;
        final ClassLoader \u3007o\u3007;
        
        private ProxiedClass(final Class<U> \u3007080, final List<Class<?>> c, final ClassLoader \u3007o\u3007, final boolean o8) {
            this.\u3007080 = \u3007080;
            this.\u3007o00\u3007\u3007Oo = new ArrayList<Class<?>>(c);
            this.\u3007o\u3007 = \u3007o\u3007;
            this.O8 = o8;
        }
        
        @Override
        public boolean equals(final Object o) {
            boolean b = true;
            if (this == o) {
                return true;
            }
            if (o != null && this.getClass() == o.getClass()) {
                final ProxiedClass proxiedClass = (ProxiedClass)o;
                if (this.\u3007080 != proxiedClass.\u3007080 || !this.\u3007o00\u3007\u3007Oo.equals(proxiedClass.\u3007o00\u3007\u3007Oo) || this.\u3007o\u3007 != proxiedClass.\u3007o\u3007 || this.O8 != proxiedClass.O8) {
                    b = false;
                }
                return b;
            }
            return false;
        }
        
        @Override
        public int hashCode() {
            return this.\u3007080.hashCode() + this.\u3007o00\u3007\u3007Oo.hashCode() + this.\u3007o\u3007.hashCode() + (this.O8 ? 1 : 0);
        }
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.android.dx;

public enum BinaryOp
{
    private static final BinaryOp[] $VALUES;
    
    ADD {
    }, 
    AND {
    }, 
    DIVIDE {
    }, 
    MULTIPLY {
    }, 
    OR {
    }, 
    REMAINDER {
    }, 
    SHIFT_LEFT {
    }, 
    SHIFT_RIGHT {
    }, 
    SUBTRACT {
    }, 
    UNSIGNED_SHIFT_RIGHT {
    }, 
    XOR {
    };
}

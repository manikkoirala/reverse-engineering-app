// 
// Decompiled by Procyon v0.6.0
// 

package com.android.dx.dex.file;

import com.android.dx.rop.cst.Constant;
import java.util.Collection;
import java.util.Iterator;
import com.android.dx.rop.cst.CstMethodHandle;
import java.util.TreeMap;

public final class MethodHandlesSection extends UniformItemSection
{
    private final TreeMap<CstMethodHandle, MethodHandleItem> o\u30070;
    
    public MethodHandlesSection(final DexFile dexFile) {
        super("method_handles", dexFile, 8);
        this.o\u30070 = new TreeMap<CstMethodHandle, MethodHandleItem>();
    }
    
    public void OoO8(final CstMethodHandle cstMethodHandle) {
        if (cstMethodHandle != null) {
            this.\u3007O8o08O();
            if (this.o\u30070.get(cstMethodHandle) == null) {
                this.o\u30070.put(cstMethodHandle, new MethodHandleItem(cstMethodHandle));
            }
            return;
        }
        throw new NullPointerException("methodHandle == null");
    }
    
    int \u30070\u3007O0088o(final CstMethodHandle key) {
        return this.o\u30070.get(key).o\u30070();
    }
    
    @Override
    protected void \u3007O00() {
        final Iterator<MethodHandleItem> iterator = this.o\u30070.values().iterator();
        int n = 0;
        while (iterator.hasNext()) {
            iterator.next().\u300780\u3007808\u3007O(n);
            ++n;
        }
    }
    
    @Override
    public Collection<? extends Item> \u3007\u3007888() {
        return this.o\u30070.values();
    }
    
    public IndexedItem \u3007\u30078O0\u30078(final Constant constant) {
        if (constant == null) {
            throw new NullPointerException("cst == null");
        }
        this.\u30078o8o\u3007();
        final IndexedItem indexedItem = this.o\u30070.get(constant);
        if (indexedItem != null) {
            return indexedItem;
        }
        throw new IllegalArgumentException("not found");
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.android.dx.dex.file;

import com.android.dx.rop.cst.CstNat;
import com.android.dx.util.Hex;
import com.android.dx.util.AnnotatedOutput;
import com.android.dx.rop.cst.CstMemberRef;

public abstract class MemberIdItem extends IdItem
{
    private final CstMemberRef OO;
    
    public MemberIdItem(final CstMemberRef oo) {
        super(oo.o\u30070());
        this.OO = oo;
    }
    
    @Override
    public int O8() {
        return 8;
    }
    
    protected abstract String OO0o\u3007\u3007();
    
    @Override
    public final void Oo08(final DexFile dexFile, final AnnotatedOutput annotatedOutput) {
        final TypeIdsSection o800o8O = dexFile.o800o8O();
        final StringIdsSection ooO8 = dexFile.OoO8();
        final CstNat \u3007\u3007888 = this.OO.\u3007\u3007888();
        final int \u30070\u3007O0088o = o800o8O.\u30070\u3007O0088o(this.OO0o\u3007\u3007\u3007\u30070());
        final int \u30070\u3007O0088o2 = ooO8.\u30070\u3007O0088o(\u3007\u3007888.oO80());
        final int \u3007o8o08O = this.\u3007O8o08O(dexFile);
        if (annotatedOutput.\u3007o\u3007()) {
            final StringBuilder sb = new StringBuilder();
            sb.append(this.oO80());
            sb.append(' ');
            sb.append(this.OO.toHuman());
            annotatedOutput.oO80(0, sb.toString());
            final StringBuilder sb2 = new StringBuilder();
            sb2.append("  class_idx: ");
            sb2.append(Hex.Oo08(\u30070\u3007O0088o));
            annotatedOutput.oO80(2, sb2.toString());
            final StringBuilder sb3 = new StringBuilder();
            sb3.append(this.OO0o\u3007\u3007());
            sb3.append(':');
            annotatedOutput.oO80(2, String.format("  %-10s %s", sb3.toString(), Hex.Oo08(\u3007o8o08O)));
            final StringBuilder sb4 = new StringBuilder();
            sb4.append("  name_idx:  ");
            sb4.append(Hex.oO80(\u30070\u3007O0088o2));
            annotatedOutput.oO80(4, sb4.toString());
        }
        annotatedOutput.writeShort(\u30070\u3007O0088o);
        annotatedOutput.writeShort(\u3007o8o08O);
        annotatedOutput.writeInt(\u30070\u3007O0088o2);
    }
    
    @Override
    public void \u3007080(final DexFile dexFile) {
        super.\u3007080(dexFile);
        dexFile.OoO8().o800o8O(this.\u30078o8o\u3007().\u3007\u3007888().oO80());
    }
    
    public final CstMemberRef \u30078o8o\u3007() {
        return this.OO;
    }
    
    protected abstract int \u3007O8o08O(final DexFile p0);
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.android.dx.dex.file;

public abstract class IndexedItem extends Item
{
    private int o0;
    
    public IndexedItem() {
        this.o0 = -1;
    }
    
    public final String oO80() {
        final StringBuilder sb = new StringBuilder();
        sb.append('[');
        sb.append(Integer.toHexString(this.o0));
        sb.append(']');
        return sb.toString();
    }
    
    public final int o\u30070() {
        final int o0 = this.o0;
        if (o0 >= 0) {
            return o0;
        }
        throw new RuntimeException("index not yet set");
    }
    
    public final void \u300780\u3007808\u3007O(final int o0) {
        if (this.o0 == -1) {
            this.o0 = o0;
            return;
        }
        throw new RuntimeException("index already set");
    }
    
    public final boolean \u3007\u3007888() {
        return this.o0 >= 0;
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.android.dx.dex.file;

import com.android.dx.util.Hex;
import com.android.dx.util.AnnotatedOutput;
import com.android.dx.rop.type.Type;
import com.android.dx.rop.type.StdTypeList;
import com.android.dx.rop.type.TypeList;
import com.android.dx.rop.type.Prototype;
import com.android.dx.rop.cst.CstString;

public final class ProtoIdItem extends IndexedItem
{
    private final CstString OO;
    private TypeListItem \u300708O\u300700\u3007o;
    private final Prototype \u3007OOo8\u30070;
    
    public ProtoIdItem(final Prototype \u3007oOo8\u30070) {
        if (\u3007oOo8\u30070 != null) {
            this.\u3007OOo8\u30070 = \u3007oOo8\u30070;
            this.OO = OO0o\u3007\u3007\u3007\u30070(\u3007oOo8\u30070);
            final StdTypeList oo08 = \u3007oOo8\u30070.Oo08();
            TypeListItem \u300708O\u300700\u3007o;
            if (oo08.size() == 0) {
                \u300708O\u300700\u3007o = null;
            }
            else {
                \u300708O\u300700\u3007o = new TypeListItem(oo08);
            }
            this.\u300708O\u300700\u3007o = \u300708O\u300700\u3007o;
            return;
        }
        throw new NullPointerException("prototype == null");
    }
    
    private static CstString OO0o\u3007\u3007\u3007\u30070(final Prototype prototype) {
        final StdTypeList oo08 = prototype.Oo08();
        final int size = oo08.size();
        final StringBuilder sb = new StringBuilder(size + 1);
        sb.append(\u30078o8o\u3007(prototype.o\u30070()));
        for (int i = 0; i < size; ++i) {
            sb.append(\u30078o8o\u3007(oo08.getType(i)));
        }
        return new CstString(sb.toString());
    }
    
    private static char \u30078o8o\u3007(final Type type) {
        char char1;
        if ((char1 = type.oO80().charAt(0)) == '[') {
            char1 = 'L';
        }
        return char1;
    }
    
    @Override
    public int O8() {
        return 12;
    }
    
    @Override
    public void Oo08(final DexFile dexFile, final AnnotatedOutput annotatedOutput) {
        final int \u30070\u3007O0088o = dexFile.OoO8().\u30070\u3007O0088o(this.OO);
        final int ooO8 = dexFile.o800o8O().OoO8(this.\u3007OOo8\u30070.o\u30070());
        final int \u300780\u3007808\u3007O = OffsettedItem.\u300780\u3007808\u3007O(this.\u300708O\u300700\u3007o);
        if (annotatedOutput.\u3007o\u3007()) {
            final StringBuilder sb = new StringBuilder();
            sb.append(this.\u3007OOo8\u30070.o\u30070().toHuman());
            sb.append(" proto(");
            final StdTypeList oo08 = this.\u3007OOo8\u30070.Oo08();
            for (int size = oo08.size(), i = 0; i < size; ++i) {
                if (i != 0) {
                    sb.append(", ");
                }
                sb.append(oo08.getType(i).toHuman());
            }
            sb.append(")");
            final StringBuilder sb2 = new StringBuilder();
            sb2.append(this.oO80());
            sb2.append(' ');
            sb2.append(sb.toString());
            annotatedOutput.oO80(0, sb2.toString());
            final StringBuilder sb3 = new StringBuilder();
            sb3.append("  shorty_idx:      ");
            sb3.append(Hex.oO80(\u30070\u3007O0088o));
            sb3.append(" // ");
            sb3.append(this.OO.OO0o\u3007\u3007\u3007\u30070());
            annotatedOutput.oO80(4, sb3.toString());
            final StringBuilder sb4 = new StringBuilder();
            sb4.append("  return_type_idx: ");
            sb4.append(Hex.oO80(ooO8));
            sb4.append(" // ");
            sb4.append(this.\u3007OOo8\u30070.o\u30070().toHuman());
            annotatedOutput.oO80(4, sb4.toString());
            final StringBuilder sb5 = new StringBuilder();
            sb5.append("  parameters_off:  ");
            sb5.append(Hex.oO80(\u300780\u3007808\u3007O));
            annotatedOutput.oO80(4, sb5.toString());
        }
        annotatedOutput.writeInt(\u30070\u3007O0088o);
        annotatedOutput.writeInt(ooO8);
        annotatedOutput.writeInt(\u300780\u3007808\u3007O);
    }
    
    @Override
    public void \u3007080(final DexFile dexFile) {
        final StringIdsSection ooO8 = dexFile.OoO8();
        final TypeIdsSection o800o8O = dexFile.o800o8O();
        final MixedItemSection \u3007o888o0o = dexFile.\u3007O888o0o();
        o800o8O.\u3007O888o0o(this.\u3007OOo8\u30070.o\u30070());
        ooO8.o800o8O(this.OO);
        final TypeListItem \u300708O\u300700\u3007o = this.\u300708O\u300700\u3007o;
        if (\u300708O\u300700\u3007o != null) {
            this.\u300708O\u300700\u3007o = \u3007o888o0o.\u3007\u30078O0\u30078(\u300708O\u300700\u3007o);
        }
    }
    
    @Override
    public ItemType \u3007o00\u3007\u3007Oo() {
        return ItemType.TYPE_PROTO_ID_ITEM;
    }
}

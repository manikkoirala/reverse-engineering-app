// 
// Decompiled by Procyon v0.6.0
// 

package com.android.dx.dex.file;

import com.android.dx.rop.type.StdTypeList;
import com.android.dx.util.Hex;
import java.util.Iterator;
import com.android.dx.rop.type.Type;
import com.android.dx.rop.cst.Constant;
import com.android.dx.dex.code.DalvInsnList;
import com.android.dex.util.ExceptionWithContext;
import com.android.dx.util.AnnotatedOutput;
import com.android.dx.rop.cst.CstMethodRef;
import com.android.dx.rop.type.TypeList;
import com.android.dx.dex.code.DalvCode;

public final class CodeItem extends OffsettedItem
{
    private final DalvCode O8o08O8O;
    private DebugInfoItem oOo0;
    private final TypeList oOo\u30078o008;
    private final CstMethodRef o\u300700O;
    private CatchStructs \u3007080OO8\u30070;
    private final boolean \u30070O;
    
    public CodeItem(final CstMethodRef o\u300700O, final DalvCode o8o08O8O, final boolean \u30070O, final TypeList oOo\u30078o008) {
        super(4, -1);
        if (o\u300700O == null) {
            throw new NullPointerException("ref == null");
        }
        if (o8o08O8O == null) {
            throw new NullPointerException("code == null");
        }
        if (oOo\u30078o008 != null) {
            this.o\u300700O = o\u300700O;
            this.O8o08O8O = o8o08O8O;
            this.\u30070O = \u30070O;
            this.oOo\u30078o008 = oOo\u30078o008;
            this.\u3007080OO8\u30070 = null;
            this.oOo0 = null;
            return;
        }
        throw new NullPointerException("throwsList == null");
    }
    
    private void OoO8(final DexFile dexFile, final AnnotatedOutput annotatedOutput) {
        final DalvInsnList o\u30070 = this.O8o08O8O.o\u30070();
        try {
            o\u30070.o800o8O(annotatedOutput);
        }
        catch (final RuntimeException ex) {
            final StringBuilder sb = new StringBuilder();
            sb.append("...while writing instructions for ");
            sb.append(this.o\u300700O.toHuman());
            throw ExceptionWithContext.withContext(ex, sb.toString());
        }
    }
    
    private int \u30070\u3007O0088o() {
        return this.O8o08O8O.o\u30070().\u3007\u30078O0\u30078();
    }
    
    private int \u3007O00() {
        return this.o\u300700O.oO80(this.\u30070O);
    }
    
    private int \u3007\u30078O0\u30078() {
        return this.O8o08O8O.o\u30070().\u3007O00();
    }
    
    @Override
    protected void OO0o\u3007\u3007(final Section section, int o\u30070) {
        final DexFile oo08 = section.Oo08();
        this.O8o08O8O.\u3007080((DalvCode.AssignIndicesCallback)new DalvCode.AssignIndicesCallback(this, oo08) {
            final DexFile \u3007080;
            final CodeItem \u3007o00\u3007\u3007Oo;
            
            @Override
            public int \u3007080(final Constant constant) {
                final IndexedItem o8 = this.\u3007080.O8(constant);
                if (o8 == null) {
                    return -1;
                }
                return o8.o\u30070();
            }
        });
        final CatchStructs \u3007080OO8\u30070 = this.\u3007080OO8\u30070;
        if (\u3007080OO8\u30070 != null) {
            \u3007080OO8\u30070.\u3007o\u3007(oo08);
            o\u30070 = this.\u3007080OO8\u30070.o\u30070();
        }
        else {
            o\u30070 = 0;
        }
        int \u3007\u3007808\u3007;
        final int n = \u3007\u3007808\u3007 = this.O8o08O8O.o\u30070().\u3007\u3007808\u3007();
        if ((n & 0x1) != 0x0) {
            \u3007\u3007808\u3007 = n + 1;
        }
        this.Oooo8o0\u3007(\u3007\u3007808\u3007 * 2 + 16 + o\u30070);
    }
    
    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append("CodeItem{");
        sb.append(this.\u3007\u3007808\u3007());
        sb.append("}");
        return sb.toString();
    }
    
    @Override
    public void \u3007080(final DexFile dexFile) {
        final MixedItemSection oo08 = dexFile.Oo08();
        final TypeIdsSection o800o8O = dexFile.o800o8O();
        if (this.O8o08O8O.\u30078o8o\u3007() || this.O8o08O8O.OO0o\u3007\u3007\u3007\u30070()) {
            oo08.\u3007O00(this.oOo0 = new DebugInfoItem(this.O8o08O8O, this.\u30070O, this.o\u300700O));
        }
        if (this.O8o08O8O.\u300780\u3007808\u3007O()) {
            final Iterator<Type> iterator = this.O8o08O8O.\u3007o\u3007().iterator();
            while (iterator.hasNext()) {
                o800o8O.\u3007O888o0o(iterator.next());
            }
            this.\u3007080OO8\u30070 = new CatchStructs(this.O8o08O8O);
        }
        final Iterator<Constant> iterator2 = this.O8o08O8O.Oo08().iterator();
        while (iterator2.hasNext()) {
            dexFile.\u3007oo\u3007(iterator2.next());
        }
    }
    
    @Override
    protected void \u3007O\u3007(final DexFile dexFile, final AnnotatedOutput annotatedOutput) {
        final boolean \u3007o\u3007 = annotatedOutput.\u3007o\u3007();
        final int \u30070\u3007O0088o = this.\u30070\u3007O0088o();
        final int \u3007\u30078O0\u30078 = this.\u3007\u30078O0\u30078();
        final int \u3007o00 = this.\u3007O00();
        final int \u3007\u3007808\u3007 = this.O8o08O8O.o\u30070().\u3007\u3007808\u3007();
        final boolean b = (\u3007\u3007808\u3007 & 0x1) != 0x0;
        final CatchStructs \u3007080OO8\u30070 = this.\u3007080OO8\u30070;
        int oo08;
        if (\u3007080OO8\u30070 == null) {
            oo08 = 0;
        }
        else {
            oo08 = \u3007080OO8\u30070.Oo08();
        }
        final DebugInfoItem oOo0 = this.oOo0;
        int oo9;
        if (oOo0 == null) {
            oo9 = 0;
        }
        else {
            oo9 = oOo0.oO80();
        }
        if (\u3007o\u3007) {
            final StringBuilder sb = new StringBuilder();
            sb.append(this.\u30078o8o\u3007());
            sb.append(' ');
            sb.append(this.o\u300700O.toHuman());
            annotatedOutput.oO80(0, sb.toString());
            final StringBuilder sb2 = new StringBuilder();
            sb2.append("  registers_size: ");
            sb2.append(Hex.Oo08(\u30070\u3007O0088o));
            annotatedOutput.oO80(2, sb2.toString());
            final StringBuilder sb3 = new StringBuilder();
            sb3.append("  ins_size:       ");
            sb3.append(Hex.Oo08(\u3007o00));
            annotatedOutput.oO80(2, sb3.toString());
            final StringBuilder sb4 = new StringBuilder();
            sb4.append("  outs_size:      ");
            sb4.append(Hex.Oo08(\u3007\u30078O0\u30078));
            annotatedOutput.oO80(2, sb4.toString());
            final StringBuilder sb5 = new StringBuilder();
            sb5.append("  tries_size:     ");
            sb5.append(Hex.Oo08(oo08));
            annotatedOutput.oO80(2, sb5.toString());
            final StringBuilder sb6 = new StringBuilder();
            sb6.append("  debug_off:      ");
            sb6.append(Hex.oO80(oo9));
            annotatedOutput.oO80(4, sb6.toString());
            final StringBuilder sb7 = new StringBuilder();
            sb7.append("  insns_size:     ");
            sb7.append(Hex.oO80(\u3007\u3007808\u3007));
            annotatedOutput.oO80(4, sb7.toString());
            if (this.oOo\u30078o008.size() != 0) {
                final StringBuilder sb8 = new StringBuilder();
                sb8.append("  throws ");
                sb8.append(StdTypeList.\u3007oo\u3007(this.oOo\u30078o008));
                annotatedOutput.oO80(0, sb8.toString());
            }
        }
        annotatedOutput.writeShort(\u30070\u3007O0088o);
        annotatedOutput.writeShort(\u3007o00);
        annotatedOutput.writeShort(\u3007\u30078O0\u30078);
        annotatedOutput.writeShort(oo08);
        annotatedOutput.writeInt(oo9);
        annotatedOutput.writeInt(\u3007\u3007808\u3007);
        this.OoO8(dexFile, annotatedOutput);
        if (this.\u3007080OO8\u30070 != null) {
            if (b) {
                if (\u3007o\u3007) {
                    annotatedOutput.oO80(2, "  padding: 0");
                }
                annotatedOutput.writeShort(0);
            }
            this.\u3007080OO8\u30070.\u3007\u3007888(dexFile, annotatedOutput);
        }
        if (\u3007o\u3007 && this.oOo0 != null) {
            annotatedOutput.oO80(0, "  debug info");
            this.oOo0.\u3007O00(dexFile, annotatedOutput, "    ");
        }
    }
    
    @Override
    public ItemType \u3007o00\u3007\u3007Oo() {
        return ItemType.TYPE_CODE_ITEM;
    }
    
    @Override
    public String \u3007\u3007808\u3007() {
        return this.o\u300700O.toHuman();
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.android.dx.dex.file;

import com.android.dx.rop.code.AccessFlags;
import com.android.dex.Leb128;
import com.android.dx.util.AnnotatedOutput;
import com.android.dx.util.Hex;
import com.android.dx.rop.cst.Constant;
import com.android.dx.rop.cst.CstFieldRef;

public final class EncodedField extends EncodedMember implements Comparable<EncodedField>
{
    private final CstFieldRef \u3007OOo8\u30070;
    
    public EncodedField(final CstFieldRef \u3007oOo8\u30070, final int n) {
        super(n);
        if (\u3007oOo8\u30070 != null) {
            this.\u3007OOo8\u30070 = \u3007oOo8\u30070;
            return;
        }
        throw new NullPointerException("field == null");
    }
    
    public void Oo08(final DexFile dexFile) {
        dexFile.OO0o\u3007\u3007\u3007\u30070().o800o8O(this.\u3007OOo8\u30070);
    }
    
    @Override
    public boolean equals(final Object o) {
        final boolean b = o instanceof EncodedField;
        boolean b2 = false;
        if (!b) {
            return false;
        }
        if (this.o\u30070((EncodedField)o) == 0) {
            b2 = true;
        }
        return b2;
    }
    
    @Override
    public int hashCode() {
        return this.\u3007OOo8\u30070.hashCode();
    }
    
    public int o\u30070(final EncodedField encodedField) {
        return this.\u3007OOo8\u30070.\u3007o\u3007(encodedField.\u3007OOo8\u30070);
    }
    
    @Override
    public String toHuman() {
        return this.\u3007OOo8\u30070.toHuman();
    }
    
    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder(100);
        sb.append(EncodedField.class.getName());
        sb.append('{');
        sb.append(Hex.Oo08(this.O8()));
        sb.append(' ');
        sb.append(this.\u3007OOo8\u30070);
        sb.append('}');
        return sb.toString();
    }
    
    @Override
    public int \u3007o\u3007(final DexFile dexFile, final AnnotatedOutput annotatedOutput, int n, int i) {
        final int ooO8 = dexFile.OO0o\u3007\u3007\u3007\u30070().OoO8(this.\u3007OOo8\u30070);
        n = ooO8 - n;
        final int o8 = this.O8();
        if (annotatedOutput.\u3007o\u3007()) {
            annotatedOutput.oO80(0, String.format("  [%x] %s", i, this.\u3007OOo8\u30070.toHuman()));
            i = Leb128.\u3007080(n);
            final StringBuilder sb = new StringBuilder();
            sb.append("    field_idx:    ");
            sb.append(Hex.oO80(ooO8));
            annotatedOutput.oO80(i, sb.toString());
            i = Leb128.\u3007080(o8);
            final StringBuilder sb2 = new StringBuilder();
            sb2.append("    access_flags: ");
            sb2.append(AccessFlags.\u3007o00\u3007\u3007Oo(o8));
            annotatedOutput.oO80(i, sb2.toString());
        }
        annotatedOutput.\u3007080(n);
        annotatedOutput.\u3007080(o8);
        return ooO8;
    }
    
    public CstFieldRef \u3007\u3007888() {
        return this.\u3007OOo8\u30070;
    }
}

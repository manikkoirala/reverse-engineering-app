// 
// Decompiled by Procyon v0.6.0
// 

package com.android.dx.dex.file;

import java.util.Iterator;
import com.android.dx.util.ByteArrayAnnotatedOutput;
import com.android.dx.rop.cst.Zeroes;
import com.android.dx.rop.cst.CstLiteralBits;
import java.util.List;
import java.util.Collections;
import com.android.dx.util.AnnotatedOutput;
import com.android.dx.rop.cst.Constant;
import java.util.HashMap;
import com.android.dx.rop.cst.CstType;
import com.android.dx.rop.cst.CstArray;
import java.util.ArrayList;

public final class ClassDataItem extends OffsettedItem
{
    private final ArrayList<EncodedField> O8o08O8O;
    private CstArray OO\u300700\u30078oO;
    private byte[] o8\u3007OO0\u30070o;
    private final ArrayList<EncodedMethod> oOo0;
    private final ArrayList<EncodedMethod> oOo\u30078o008;
    private final CstType o\u300700O;
    private final HashMap<EncodedField, Constant> \u3007080OO8\u30070;
    private final ArrayList<EncodedField> \u30070O;
    
    public ClassDataItem(final CstType o\u300700O) {
        super(1, -1);
        if (o\u300700O != null) {
            this.o\u300700O = o\u300700O;
            this.O8o08O8O = new ArrayList<EncodedField>(20);
            this.\u3007080OO8\u30070 = new HashMap<EncodedField, Constant>(40);
            this.\u30070O = new ArrayList<EncodedField>(20);
            this.oOo\u30078o008 = new ArrayList<EncodedMethod>(20);
            this.oOo0 = new ArrayList<EncodedMethod>(20);
            this.OO\u300700\u30078oO = null;
            return;
        }
        throw new NullPointerException("thisClass == null");
    }
    
    private static void o800o8O(final DexFile dexFile, final AnnotatedOutput annotatedOutput, final String str, final ArrayList<? extends EncodedMember> list) {
        final int size = list.size();
        if (size == 0) {
            return;
        }
        final boolean \u3007o\u3007 = annotatedOutput.\u3007o\u3007();
        int i = 0;
        if (\u3007o\u3007) {
            final StringBuilder sb = new StringBuilder();
            sb.append("  ");
            sb.append(str);
            sb.append(":");
            annotatedOutput.oO80(0, sb.toString());
        }
        int \u3007o\u30072 = 0;
        while (i < size) {
            \u3007o\u30072 = list.get(i).\u3007o\u3007(dexFile, annotatedOutput, \u3007o\u30072, i);
            ++i;
        }
    }
    
    private static void oo88o8O(final DexFile dexFile, final AnnotatedOutput annotatedOutput, final String str, final int i) {
        if (annotatedOutput.\u3007o\u3007()) {
            final StringBuilder sb = new StringBuilder();
            sb.append(str);
            sb.append("_size:");
            annotatedOutput.o\u30070(String.format("  %-21s %08x", sb.toString(), i));
        }
        annotatedOutput.\u3007080(i);
    }
    
    private CstArray \u300700() {
        Collections.sort(this.O8o08O8O);
        int i;
        for (i = this.O8o08O8O.size(); i > 0; --i) {
            final Constant constant = this.\u3007080OO8\u30070.get(this.O8o08O8O.get(i - 1));
            if (constant instanceof CstLiteralBits) {
                if (((CstLiteralBits)constant).oO80() != 0L) {
                    break;
                }
            }
            else if (constant != null) {
                break;
            }
        }
        if (i == 0) {
            return null;
        }
        final CstArray.List list = new CstArray.List(i);
        for (int j = 0; j < i; ++j) {
            final EncodedField key = this.O8o08O8O.get(j);
            Constant \u3007080;
            if ((\u3007080 = this.\u3007080OO8\u30070.get(key)) == null) {
                \u3007080 = Zeroes.\u3007080(key.\u3007\u3007888().getType());
            }
            list.\u3007O00(j, \u3007080);
        }
        list.Oo08();
        return new CstArray(list);
    }
    
    private void \u3007O888o0o(final DexFile dexFile, final AnnotatedOutput annotatedOutput) {
        final boolean \u3007o\u3007 = annotatedOutput.\u3007o\u3007();
        if (\u3007o\u3007) {
            final StringBuilder sb = new StringBuilder();
            sb.append(this.\u30078o8o\u3007());
            sb.append(" class data for ");
            sb.append(this.o\u300700O.toHuman());
            annotatedOutput.oO80(0, sb.toString());
        }
        oo88o8O(dexFile, annotatedOutput, "static_fields", this.O8o08O8O.size());
        oo88o8O(dexFile, annotatedOutput, "instance_fields", this.\u30070O.size());
        oo88o8O(dexFile, annotatedOutput, "direct_methods", this.oOo\u30078o008.size());
        oo88o8O(dexFile, annotatedOutput, "virtual_methods", this.oOo0.size());
        o800o8O(dexFile, annotatedOutput, "static_fields", this.O8o08O8O);
        o800o8O(dexFile, annotatedOutput, "instance_fields", this.\u30070O);
        o800o8O(dexFile, annotatedOutput, "direct_methods", this.oOo\u30078o008);
        o800o8O(dexFile, annotatedOutput, "virtual_methods", this.oOo0);
        if (\u3007o\u3007) {
            annotatedOutput.\u3007o00\u3007\u3007Oo();
        }
    }
    
    @Override
    protected void OO0o\u3007\u3007(final Section section, final int n) {
        final ByteArrayAnnotatedOutput byteArrayAnnotatedOutput = new ByteArrayAnnotatedOutput();
        this.\u3007O888o0o(section.Oo08(), byteArrayAnnotatedOutput);
        final byte[] \u3007o00 = byteArrayAnnotatedOutput.\u3007O00();
        this.o8\u3007OO0\u30070o = \u3007o00;
        this.Oooo8o0\u3007(\u3007o00.length);
    }
    
    public void OoO8(final EncodedMethod e) {
        if (e != null) {
            this.oOo0.add(e);
            return;
        }
        throw new NullPointerException("method == null");
    }
    
    public boolean o\u3007O8\u3007\u3007o() {
        return this.O8o08O8O.isEmpty() && this.\u30070O.isEmpty() && this.oOo\u30078o008.isEmpty() && this.oOo0.isEmpty();
    }
    
    @Override
    public void \u3007080(final DexFile dexFile) {
        if (!this.O8o08O8O.isEmpty()) {
            this.\u3007oo\u3007();
            final Iterator<EncodedField> iterator = this.O8o08O8O.iterator();
            while (iterator.hasNext()) {
                iterator.next().Oo08(dexFile);
            }
        }
        if (!this.\u30070O.isEmpty()) {
            Collections.sort(this.\u30070O);
            final Iterator<EncodedField> iterator2 = this.\u30070O.iterator();
            while (iterator2.hasNext()) {
                iterator2.next().Oo08(dexFile);
            }
        }
        if (!this.oOo\u30078o008.isEmpty()) {
            Collections.sort(this.oOo\u30078o008);
            final Iterator<EncodedMethod> iterator3 = this.oOo\u30078o008.iterator();
            while (iterator3.hasNext()) {
                iterator3.next().Oo08(dexFile);
            }
        }
        if (!this.oOo0.isEmpty()) {
            Collections.sort(this.oOo0);
            final Iterator<EncodedMethod> iterator4 = this.oOo0.iterator();
            while (iterator4.hasNext()) {
                iterator4.next().Oo08(dexFile);
            }
        }
    }
    
    public void \u30070\u3007O0088o(final EncodedField encodedField, final Constant value) {
        if (encodedField == null) {
            throw new NullPointerException("field == null");
        }
        if (this.OO\u300700\u30078oO == null) {
            this.O8o08O8O.add(encodedField);
            this.\u3007080OO8\u30070.put(encodedField, value);
            return;
        }
        throw new UnsupportedOperationException("static fields already sorted");
    }
    
    public void \u3007O00(final EncodedMethod e) {
        if (e != null) {
            this.oOo\u30078o008.add(e);
            return;
        }
        throw new NullPointerException("method == null");
    }
    
    public void \u3007O\u3007(final DexFile dexFile, final AnnotatedOutput annotatedOutput) {
        if (annotatedOutput.\u3007o\u3007()) {
            this.\u3007O888o0o(dexFile, annotatedOutput);
        }
        else {
            annotatedOutput.write(this.o8\u3007OO0\u30070o);
        }
    }
    
    @Override
    public ItemType \u3007o00\u3007\u3007Oo() {
        return ItemType.TYPE_CLASS_DATA_ITEM;
    }
    
    public CstArray \u3007oo\u3007() {
        if (this.OO\u300700\u30078oO == null && this.O8o08O8O.size() != 0) {
            this.OO\u300700\u30078oO = this.\u300700();
        }
        return this.OO\u300700\u30078oO;
    }
    
    @Override
    public String \u3007\u3007808\u3007() {
        return this.toString();
    }
    
    public void \u3007\u30078O0\u30078(final EncodedField e) {
        if (e != null) {
            this.\u30070O.add(e);
            return;
        }
        throw new NullPointerException("field == null");
    }
}

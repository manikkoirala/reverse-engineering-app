// 
// Decompiled by Procyon v0.6.0
// 

package com.android.dx.dex.file;

import java.util.Collection;
import com.android.dx.util.AnnotatedOutput;

public abstract class Section
{
    private int O8;
    private boolean Oo08;
    private final String \u3007080;
    private final DexFile \u3007o00\u3007\u3007Oo;
    private final int \u3007o\u3007;
    
    public Section(final String \u3007080, final DexFile \u3007o00\u3007\u3007Oo, final int \u3007o\u3007) {
        if (\u3007o00\u3007\u3007Oo != null) {
            OO0o\u3007\u3007(\u3007o\u3007);
            this.\u3007080 = \u3007080;
            this.\u3007o00\u3007\u3007Oo = \u3007o00\u3007\u3007Oo;
            this.\u3007o\u3007 = \u3007o\u3007;
            this.O8 = -1;
            this.Oo08 = false;
            return;
        }
        throw new NullPointerException("file == null");
    }
    
    public static void OO0o\u3007\u3007(final int n) {
        if (n > 0 && (n & n - 1) == 0x0) {
            return;
        }
        throw new IllegalArgumentException("invalid alignment");
    }
    
    public final int O8() {
        return this.\u3007o\u3007;
    }
    
    public final int OO0o\u3007\u3007\u3007\u30070(int o8) {
        if (o8 < 0) {
            throw new IllegalArgumentException("fileOffset < 0");
        }
        if (this.O8 < 0) {
            final int n = this.\u3007o\u3007 - 1;
            o8 = (o8 + n & ~n);
            return this.O8 = o8;
        }
        throw new RuntimeException("fileOffset already set");
    }
    
    public final DexFile Oo08() {
        return this.\u3007o00\u3007\u3007Oo;
    }
    
    public abstract int Oooo8o0\u3007();
    
    public final void oO80() {
        this.\u3007O8o08O();
        this.\u300780\u3007808\u3007O();
        this.Oo08 = true;
    }
    
    public final int o\u30070() {
        final int o8 = this.O8;
        if (o8 >= 0) {
            return o8;
        }
        throw new RuntimeException("fileOffset not set");
    }
    
    protected final void \u3007080(final AnnotatedOutput annotatedOutput) {
        annotatedOutput.Oo08(this.\u3007o\u3007);
    }
    
    protected abstract void \u300780\u3007808\u3007O();
    
    protected final void \u30078o8o\u3007() {
        if (this.Oo08) {
            return;
        }
        throw new RuntimeException("not prepared");
    }
    
    protected final void \u3007O8o08O() {
        if (!this.Oo08) {
            return;
        }
        throw new RuntimeException("already prepared");
    }
    
    protected abstract void \u3007O\u3007(final AnnotatedOutput p0);
    
    public abstract int \u3007o00\u3007\u3007Oo(final Item p0);
    
    public final int \u3007o\u3007(final int n) {
        if (n < 0) {
            throw new IllegalArgumentException("relative < 0");
        }
        final int o8 = this.O8;
        if (o8 >= 0) {
            return o8 + n;
        }
        throw new RuntimeException("fileOffset not yet set");
    }
    
    public final void \u3007\u3007808\u3007(final AnnotatedOutput annotatedOutput) {
        this.\u30078o8o\u3007();
        this.\u3007080(annotatedOutput);
        final int cursor = annotatedOutput.getCursor();
        final int o8 = this.O8;
        if (o8 < 0) {
            this.O8 = cursor;
        }
        else if (o8 != cursor) {
            final StringBuilder sb = new StringBuilder();
            sb.append("alignment mismatch: for ");
            sb.append(this);
            sb.append(", at ");
            sb.append(cursor);
            sb.append(", but expected ");
            sb.append(this.O8);
            throw new RuntimeException(sb.toString());
        }
        if (annotatedOutput.\u3007o\u3007()) {
            if (this.\u3007080 != null) {
                final StringBuilder sb2 = new StringBuilder();
                sb2.append("\n");
                sb2.append(this.\u3007080);
                sb2.append(":");
                annotatedOutput.oO80(0, sb2.toString());
            }
            else if (cursor != 0) {
                annotatedOutput.oO80(0, "\n");
            }
        }
        this.\u3007O\u3007(annotatedOutput);
    }
    
    public abstract Collection<? extends Item> \u3007\u3007888();
}

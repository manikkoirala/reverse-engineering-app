// 
// Decompiled by Procyon v0.6.0
// 

package com.android.dx.dex.file;

import com.android.dx.rop.cst.Constant;
import com.android.dx.util.AnnotatedOutput;
import com.android.dx.util.ByteArrayAnnotatedOutput;
import com.android.dx.rop.cst.CstArray;

public final class EncodedArrayItem extends OffsettedItem
{
    private byte[] O8o08O8O;
    private final CstArray o\u300700O;
    
    public EncodedArrayItem(final CstArray o\u300700O) {
        super(1, -1);
        if (o\u300700O != null) {
            this.o\u300700O = o\u300700O;
            this.O8o08O8O = null;
            return;
        }
        throw new NullPointerException("array == null");
    }
    
    @Override
    protected void OO0o\u3007\u3007(final Section section, final int n) {
        final ByteArrayAnnotatedOutput byteArrayAnnotatedOutput = new ByteArrayAnnotatedOutput();
        new ValueEncoder(section.Oo08(), byteArrayAnnotatedOutput).o\u30070(this.o\u300700O, false);
        final byte[] \u3007o00 = byteArrayAnnotatedOutput.\u3007O00();
        this.O8o08O8O = \u3007o00;
        this.Oooo8o0\u3007(\u3007o00.length);
    }
    
    @Override
    public int hashCode() {
        return this.o\u300700O.hashCode();
    }
    
    @Override
    public void \u3007080(final DexFile dexFile) {
        ValueEncoder.\u3007o00\u3007\u3007Oo(dexFile, this.o\u300700O);
    }
    
    @Override
    protected void \u3007O\u3007(final DexFile dexFile, final AnnotatedOutput annotatedOutput) {
        if (annotatedOutput.\u3007o\u3007()) {
            final StringBuilder sb = new StringBuilder();
            sb.append(this.\u30078o8o\u3007());
            sb.append(" encoded array");
            annotatedOutput.oO80(0, sb.toString());
            new ValueEncoder(dexFile, annotatedOutput).o\u30070(this.o\u300700O, true);
        }
        else {
            annotatedOutput.write(this.O8o08O8O);
        }
    }
    
    @Override
    public ItemType \u3007o00\u3007\u3007Oo() {
        return ItemType.TYPE_ENCODED_ARRAY_ITEM;
    }
    
    @Override
    public String \u3007\u3007808\u3007() {
        return this.o\u300700O.toHuman();
    }
    
    @Override
    protected int \u3007\u3007888(final OffsettedItem offsettedItem) {
        return this.o\u300700O.\u3007o\u3007(((EncodedArrayItem)offsettedItem).o\u300700O);
    }
}

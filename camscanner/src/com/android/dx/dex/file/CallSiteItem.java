// 
// Decompiled by Procyon v0.6.0
// 

package com.android.dx.dex.file;

import com.android.dx.rop.cst.Constant;
import com.android.dx.rop.cst.CstArray;
import com.android.dx.util.AnnotatedOutput;
import com.android.dx.util.ByteArrayAnnotatedOutput;
import com.android.dx.rop.cst.CstCallSite;

public final class CallSiteItem extends OffsettedItem
{
    private byte[] o\u300700O;
    
    public CallSiteItem(final CstCallSite cstCallSite) {
        super(1, \u3007O00(cstCallSite));
    }
    
    private static int \u3007O00(final CstCallSite cstCallSite) {
        return -1;
    }
    
    @Override
    protected void OO0o\u3007\u3007(final Section section, final int n) {
        final ByteArrayAnnotatedOutput byteArrayAnnotatedOutput = new ByteArrayAnnotatedOutput();
        new ValueEncoder(section.Oo08(), byteArrayAnnotatedOutput).o\u30070(null, true);
        final byte[] \u3007o00 = byteArrayAnnotatedOutput.\u3007O00();
        this.o\u300700O = \u3007o00;
        this.Oooo8o0\u3007(\u3007o00.length);
    }
    
    @Override
    public String toString() {
        throw null;
    }
    
    @Override
    public void \u3007080(final DexFile dexFile) {
        ValueEncoder.\u3007o00\u3007\u3007Oo(dexFile, null);
    }
    
    @Override
    protected void \u3007O\u3007(final DexFile dexFile, final AnnotatedOutput annotatedOutput) {
        if (annotatedOutput.\u3007o\u3007()) {
            final StringBuilder sb = new StringBuilder();
            sb.append(this.\u30078o8o\u3007());
            sb.append(" call site");
            annotatedOutput.oO80(0, sb.toString());
            new ValueEncoder(dexFile, annotatedOutput).o\u30070(null, true);
        }
        else {
            annotatedOutput.write(this.o\u300700O);
        }
    }
    
    @Override
    public ItemType \u3007o00\u3007\u3007Oo() {
        return ItemType.TYPE_ENCODED_ARRAY_ITEM;
    }
    
    @Override
    public String \u3007\u3007808\u3007() {
        throw null;
    }
}

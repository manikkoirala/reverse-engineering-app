// 
// Decompiled by Procyon v0.6.0
// 

package com.android.dx.dex.file;

import com.android.dx.rop.cst.CstMemberRef;
import com.android.dx.rop.cst.CstFieldRef;

public final class FieldIdItem extends MemberIdItem
{
    public FieldIdItem(final CstFieldRef cstFieldRef) {
        super(cstFieldRef);
    }
    
    @Override
    protected String OO0o\u3007\u3007() {
        return "type_idx";
    }
    
    public CstFieldRef Oooo8o0\u3007() {
        return (CstFieldRef)this.\u30078o8o\u3007();
    }
    
    @Override
    public void \u3007080(final DexFile dexFile) {
        super.\u3007080(dexFile);
        dexFile.o800o8O().\u3007O888o0o(this.Oooo8o0\u3007().getType());
    }
    
    @Override
    protected int \u3007O8o08O(final DexFile dexFile) {
        return dexFile.o800o8O().OoO8(this.Oooo8o0\u3007().getType());
    }
    
    @Override
    public ItemType \u3007o00\u3007\u3007Oo() {
        return ItemType.TYPE_FIELD_ID_ITEM;
    }
}

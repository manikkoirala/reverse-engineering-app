// 
// Decompiled by Procyon v0.6.0
// 

package com.android.dx.dex.file;

import com.android.dx.rop.cst.Constant;
import com.android.dx.util.ByteArray;
import com.android.dx.util.Hex;
import com.android.dx.util.AnnotatedOutput;
import com.android.dex.Leb128;
import com.android.dx.rop.cst.CstString;

public final class StringDataItem extends OffsettedItem
{
    private final CstString o\u300700O;
    
    public StringDataItem(final CstString o\u300700O) {
        super(1, \u3007O00(o\u300700O));
        this.o\u300700O = o\u300700O;
    }
    
    private static int \u3007O00(final CstString cstString) {
        return Leb128.\u3007080(cstString.\u3007\u3007888()) + cstString.oO80() + 1;
    }
    
    @Override
    public void \u3007080(final DexFile dexFile) {
    }
    
    public void \u3007O\u3007(final DexFile dexFile, final AnnotatedOutput annotatedOutput) {
        final ByteArray o\u30070 = this.o\u300700O.o\u30070();
        final int \u3007\u3007888 = this.o\u300700O.\u3007\u3007888();
        if (annotatedOutput.\u3007o\u3007()) {
            final int \u3007080 = Leb128.\u3007080(\u3007\u3007888);
            final StringBuilder sb = new StringBuilder();
            sb.append("utf16_size: ");
            sb.append(Hex.oO80(\u3007\u3007888));
            annotatedOutput.oO80(\u3007080, sb.toString());
            annotatedOutput.oO80(o\u30070.\u3007o00\u3007\u3007Oo() + 1, this.o\u300700O.OO0o\u3007\u3007\u3007\u30070());
        }
        annotatedOutput.\u3007080(\u3007\u3007888);
        annotatedOutput.\u30078o8o\u3007(o\u30070);
        annotatedOutput.writeByte(0);
    }
    
    @Override
    public ItemType \u3007o00\u3007\u3007Oo() {
        return ItemType.TYPE_STRING_DATA_ITEM;
    }
    
    @Override
    public String \u3007\u3007808\u3007() {
        return this.o\u300700O.OO0o\u3007\u3007\u3007\u30070();
    }
    
    @Override
    protected int \u3007\u3007888(final OffsettedItem offsettedItem) {
        return this.o\u300700O.\u3007o\u3007(((StringDataItem)offsettedItem).o\u300700O);
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.android.dx.dex.file;

import java.util.Collection;
import com.android.dx.util.Hex;
import com.android.dx.util.AnnotatedOutput;
import com.android.dx.rop.cst.Constant;
import com.android.dx.rop.cst.CstFieldRef;
import java.util.TreeMap;

public final class FieldIdsSection extends MemberIdsSection
{
    private final TreeMap<CstFieldRef, FieldIdItem> o\u30070;
    
    public FieldIdsSection(final DexFile dexFile) {
        super("field_ids", dexFile);
        this.o\u30070 = new TreeMap<CstFieldRef, FieldIdItem>();
    }
    
    public int OoO8(final CstFieldRef key) {
        if (key == null) {
            throw new NullPointerException("ref == null");
        }
        this.\u30078o8o\u3007();
        final FieldIdItem fieldIdItem = this.o\u30070.get(key);
        if (fieldIdItem != null) {
            return fieldIdItem.o\u30070();
        }
        throw new IllegalArgumentException("not found");
    }
    
    public FieldIdItem o800o8O(final CstFieldRef cstFieldRef) {
        monitorenter(this);
        if (cstFieldRef != null) {
            Label_0067: {
                try {
                    this.\u3007O8o08O();
                    FieldIdItem value;
                    if ((value = this.o\u30070.get(cstFieldRef)) == null) {
                        value = new FieldIdItem(cstFieldRef);
                        this.o\u30070.put(cstFieldRef, value);
                    }
                    monitorexit(this);
                    return value;
                }
                finally {
                    break Label_0067;
                }
                throw new NullPointerException("field == null");
            }
            monitorexit(this);
        }
        throw new NullPointerException("field == null");
    }
    
    public IndexedItem \u30070\u3007O0088o(final Constant constant) {
        if (constant == null) {
            throw new NullPointerException("cst == null");
        }
        this.\u30078o8o\u3007();
        final IndexedItem indexedItem = this.o\u30070.get(constant);
        if (indexedItem != null) {
            return indexedItem;
        }
        throw new IllegalArgumentException("not found");
    }
    
    public void \u3007O888o0o(final AnnotatedOutput annotatedOutput) {
        this.\u30078o8o\u3007();
        final int size = this.o\u30070.size();
        int o\u30070;
        if (size == 0) {
            o\u30070 = 0;
        }
        else {
            o\u30070 = this.o\u30070();
        }
        if (annotatedOutput.\u3007o\u3007()) {
            final StringBuilder sb = new StringBuilder();
            sb.append("field_ids_size:  ");
            sb.append(Hex.oO80(size));
            annotatedOutput.oO80(4, sb.toString());
            final StringBuilder sb2 = new StringBuilder();
            sb2.append("field_ids_off:   ");
            sb2.append(Hex.oO80(o\u30070));
            annotatedOutput.oO80(4, sb2.toString());
        }
        annotatedOutput.writeInt(size);
        annotatedOutput.writeInt(o\u30070);
    }
    
    @Override
    public Collection<? extends Item> \u3007\u3007888() {
        return this.o\u30070.values();
    }
}

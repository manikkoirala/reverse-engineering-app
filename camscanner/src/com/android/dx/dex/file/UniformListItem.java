// 
// Decompiled by Procyon v0.6.0
// 

package com.android.dx.dex.file;

import com.android.dx.util.Hex;
import com.android.dx.util.AnnotatedOutput;
import java.util.Iterator;
import java.util.List;

public final class UniformListItem<T extends OffsettedItem> extends OffsettedItem
{
    private final List<T> O8o08O8O;
    private final ItemType o\u300700O;
    
    public UniformListItem(final ItemType o\u300700O, final List<T> o8o08O8O) {
        super(\u3007O00(o8o08O8O), OoO8(o8o08O8O));
        if (o\u300700O != null) {
            this.O8o08O8O = o8o08O8O;
            this.o\u300700O = o\u300700O;
            return;
        }
        throw new NullPointerException("itemType == null");
    }
    
    private static int OoO8(final List<? extends OffsettedItem> list) {
        return list.size() * ((OffsettedItem)list.get(0)).O8() + \u3007O00(list);
    }
    
    private int \u30070\u3007O0088o() {
        return this.OO0o\u3007\u3007\u3007\u30070();
    }
    
    private static int \u3007O00(final List<? extends OffsettedItem> list) {
        try {
            return Math.max(4, ((OffsettedItem)list.get(0)).OO0o\u3007\u3007\u3007\u30070());
        }
        catch (final NullPointerException ex) {
            throw new NullPointerException("items == null");
        }
        catch (final IndexOutOfBoundsException ex2) {
            throw new IllegalArgumentException("items.size() == 0");
        }
    }
    
    @Override
    protected void OO0o\u3007\u3007(final Section section, int n) {
        n += this.\u30070\u3007O0088o();
        final Iterator<T> iterator = this.O8o08O8O.iterator();
        int n2 = 1;
        int n3 = -1;
        int oo0o\u3007\u3007\u3007\u30070 = -1;
        while (iterator.hasNext()) {
            final OffsettedItem offsettedItem = iterator.next();
            final int o8 = offsettedItem.O8();
            if (n2 != 0) {
                oo0o\u3007\u3007\u3007\u30070 = offsettedItem.OO0o\u3007\u3007\u3007\u30070();
                n3 = o8;
                n2 = 0;
            }
            else {
                if (o8 != n3) {
                    throw new UnsupportedOperationException("item size mismatch");
                }
                if (offsettedItem.OO0o\u3007\u3007\u3007\u30070() != oo0o\u3007\u3007\u3007\u30070) {
                    throw new UnsupportedOperationException("item alignment mismatch");
                }
            }
            n = offsettedItem.\u3007O8o08O(section, n) + o8;
        }
    }
    
    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder(100);
        sb.append(UniformListItem.class.getName());
        sb.append(this.O8o08O8O);
        return sb.toString();
    }
    
    @Override
    public void \u3007080(final DexFile dexFile) {
        final Iterator<T> iterator = this.O8o08O8O.iterator();
        while (iterator.hasNext()) {
            iterator.next().\u3007080(dexFile);
        }
    }
    
    @Override
    protected void \u3007O\u3007(final DexFile dexFile, final AnnotatedOutput annotatedOutput) {
        final int size = this.O8o08O8O.size();
        if (annotatedOutput.\u3007o\u3007()) {
            final StringBuilder sb = new StringBuilder();
            sb.append(this.\u30078o8o\u3007());
            sb.append(" ");
            sb.append(this.\u3007o\u3007());
            annotatedOutput.oO80(0, sb.toString());
            final StringBuilder sb2 = new StringBuilder();
            sb2.append("  size: ");
            sb2.append(Hex.oO80(size));
            annotatedOutput.oO80(4, sb2.toString());
        }
        annotatedOutput.writeInt(size);
        final Iterator<T> iterator = this.O8o08O8O.iterator();
        while (iterator.hasNext()) {
            iterator.next().Oo08(dexFile, annotatedOutput);
        }
    }
    
    @Override
    public ItemType \u3007o00\u3007\u3007Oo() {
        return this.o\u300700O;
    }
    
    @Override
    public final String \u3007\u3007808\u3007() {
        final StringBuilder sb = new StringBuilder(100);
        sb.append("{");
        final Iterator<T> iterator = this.O8o08O8O.iterator();
        int n = 1;
        while (iterator.hasNext()) {
            final OffsettedItem offsettedItem = iterator.next();
            if (n != 0) {
                n = 0;
            }
            else {
                sb.append(", ");
            }
            sb.append(offsettedItem.\u3007\u3007808\u3007());
        }
        sb.append("}");
        return sb.toString();
    }
    
    public final List<T> \u3007\u30078O0\u30078() {
        return this.O8o08O8O;
    }
}

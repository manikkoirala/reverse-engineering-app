// 
// Decompiled by Procyon v0.6.0
// 

package com.android.dx.dex.file;

import com.android.dx.rop.cst.Constant;
import java.util.Collection;
import com.android.dx.util.Hex;
import com.android.dx.util.AnnotatedOutput;
import java.util.Iterator;
import com.android.dx.rop.cst.CstString;
import java.util.TreeMap;

public final class StringIdsSection extends UniformItemSection
{
    private final TreeMap<CstString, StringIdItem> o\u30070;
    
    public StringIdsSection(final DexFile dexFile) {
        super("string_ids", dexFile, 4);
        this.o\u30070 = new TreeMap<CstString, StringIdItem>();
    }
    
    public StringIdItem OoO8(final StringIdItem value) {
        monitorenter(this);
        if (value != null) {
            Label_0065: {
                try {
                    this.\u3007O8o08O();
                    final CstString oo0o\u3007\u3007\u3007\u30070 = value.OO0o\u3007\u3007\u3007\u30070();
                    final StringIdItem stringIdItem = this.o\u30070.get(oo0o\u3007\u3007\u3007\u30070);
                    if (stringIdItem != null) {
                        monitorexit(this);
                        return stringIdItem;
                    }
                    this.o\u30070.put(oo0o\u3007\u3007\u3007\u30070, value);
                    monitorexit(this);
                    return value;
                }
                finally {
                    break Label_0065;
                }
                throw new NullPointerException("string == null");
            }
            monitorexit(this);
        }
        throw new NullPointerException("string == null");
    }
    
    public StringIdItem o800o8O(final CstString cstString) {
        return this.OoO8(new StringIdItem(cstString));
    }
    
    public int \u30070\u3007O0088o(final CstString key) {
        if (key == null) {
            throw new NullPointerException("string == null");
        }
        this.\u30078o8o\u3007();
        final StringIdItem stringIdItem = this.o\u30070.get(key);
        if (stringIdItem != null) {
            return stringIdItem.o\u30070();
        }
        throw new IllegalArgumentException("not found");
    }
    
    @Override
    protected void \u3007O00() {
        final Iterator<StringIdItem> iterator = this.o\u30070.values().iterator();
        int n = 0;
        while (iterator.hasNext()) {
            iterator.next().\u300780\u3007808\u3007O(n);
            ++n;
        }
    }
    
    public void \u3007O888o0o(final AnnotatedOutput annotatedOutput) {
        this.\u30078o8o\u3007();
        final int size = this.o\u30070.size();
        int o\u30070;
        if (size == 0) {
            o\u30070 = 0;
        }
        else {
            o\u30070 = this.o\u30070();
        }
        if (annotatedOutput.\u3007o\u3007()) {
            final StringBuilder sb = new StringBuilder();
            sb.append("string_ids_size: ");
            sb.append(Hex.oO80(size));
            annotatedOutput.oO80(4, sb.toString());
            final StringBuilder sb2 = new StringBuilder();
            sb2.append("string_ids_off:  ");
            sb2.append(Hex.oO80(o\u30070));
            annotatedOutput.oO80(4, sb2.toString());
        }
        annotatedOutput.writeInt(size);
        annotatedOutput.writeInt(o\u30070);
    }
    
    @Override
    public Collection<? extends Item> \u3007\u3007888() {
        return this.o\u30070.values();
    }
    
    public IndexedItem \u3007\u30078O0\u30078(final Constant constant) {
        if (constant == null) {
            throw new NullPointerException("cst == null");
        }
        this.\u30078o8o\u3007();
        final IndexedItem indexedItem = this.o\u30070.get(constant);
        if (indexedItem != null) {
            return indexedItem;
        }
        throw new IllegalArgumentException("not found");
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.android.dx.dex.file;

import com.android.dx.rop.cst.Constant;
import com.android.dx.rop.cst.CstArray;
import com.android.dx.rop.type.StdTypeList;
import com.android.dx.rop.code.AccessFlags;
import com.android.dx.util.Hex;
import com.android.dx.util.AnnotatedOutput;
import com.android.dx.rop.type.TypeList;
import com.android.dx.rop.cst.CstType;
import com.android.dx.rop.cst.CstString;

public final class ClassDefItem extends IndexedItem
{
    private final CstString O8o08O8O;
    private final int OO;
    private AnnotationsDirectoryItem oOo\u30078o008;
    private TypeListItem o\u300700O;
    private final ClassDataItem \u3007080OO8\u30070;
    private final CstType \u300708O\u300700\u3007o;
    private EncodedArrayItem \u30070O;
    private final CstType \u3007OOo8\u30070;
    
    public ClassDefItem(final CstType \u3007oOo8\u30070, final int oo, final CstType \u300708O\u300700\u3007o, final TypeList list, final CstString o8o08O8O) {
        if (\u3007oOo8\u30070 == null) {
            throw new NullPointerException("thisClass == null");
        }
        if (list != null) {
            this.\u3007OOo8\u30070 = \u3007oOo8\u30070;
            this.OO = oo;
            this.\u300708O\u300700\u3007o = \u300708O\u300700\u3007o;
            TypeListItem o\u300700O;
            if (list.size() == 0) {
                o\u300700O = null;
            }
            else {
                o\u300700O = new TypeListItem(list);
            }
            this.o\u300700O = o\u300700O;
            this.O8o08O8O = o8o08O8O;
            this.\u3007080OO8\u30070 = new ClassDataItem(\u3007oOo8\u30070);
            this.\u30070O = null;
            this.oOo\u30078o008 = new AnnotationsDirectoryItem();
            return;
        }
        throw new NullPointerException("interfaces == null");
    }
    
    @Override
    public int O8() {
        return 32;
    }
    
    public void OO0o\u3007\u3007(final EncodedMethod encodedMethod) {
        this.\u3007080OO8\u30070.OoO8(encodedMethod);
    }
    
    public void OO0o\u3007\u3007\u3007\u30070(final EncodedMethod encodedMethod) {
        this.\u3007080OO8\u30070.\u3007O00(encodedMethod);
    }
    
    @Override
    public void Oo08(final DexFile dexFile, final AnnotatedOutput annotatedOutput) {
        final boolean \u3007o\u3007 = annotatedOutput.\u3007o\u3007();
        final TypeIdsSection o800o8O = dexFile.o800o8O();
        final int \u30070\u3007O0088o = o800o8O.\u30070\u3007O0088o(this.\u3007OOo8\u30070);
        final CstType \u300708O\u300700\u3007o = this.\u300708O\u300700\u3007o;
        int \u30070\u3007O0088o2 = -1;
        int \u30070\u3007O0088o3;
        if (\u300708O\u300700\u3007o == null) {
            \u30070\u3007O0088o3 = -1;
        }
        else {
            \u30070\u3007O0088o3 = o800o8O.\u30070\u3007O0088o(\u300708O\u300700\u3007o);
        }
        final int \u300780\u3007808\u3007O = OffsettedItem.\u300780\u3007808\u3007O(this.o\u300700O);
        int oo80;
        if (this.oOo\u30078o008.\u3007O00()) {
            oo80 = 0;
        }
        else {
            oo80 = this.oOo\u30078o008.oO80();
        }
        if (this.O8o08O8O != null) {
            \u30070\u3007O0088o2 = dexFile.OoO8().\u30070\u3007O0088o(this.O8o08O8O);
        }
        int oo81;
        if (this.\u3007080OO8\u30070.o\u3007O8\u3007\u3007o()) {
            oo81 = 0;
        }
        else {
            oo81 = this.\u3007080OO8\u30070.oO80();
        }
        final int \u300780\u3007808\u3007O2 = OffsettedItem.\u300780\u3007808\u3007O(this.\u30070O);
        if (\u3007o\u3007) {
            final StringBuilder sb = new StringBuilder();
            sb.append(this.oO80());
            sb.append(' ');
            sb.append(this.\u3007OOo8\u30070.toHuman());
            annotatedOutput.oO80(0, sb.toString());
            final StringBuilder sb2 = new StringBuilder();
            sb2.append("  class_idx:           ");
            sb2.append(Hex.oO80(\u30070\u3007O0088o));
            annotatedOutput.oO80(4, sb2.toString());
            final StringBuilder sb3 = new StringBuilder();
            sb3.append("  access_flags:        ");
            sb3.append(AccessFlags.\u3007080(this.OO));
            annotatedOutput.oO80(4, sb3.toString());
            final StringBuilder sb4 = new StringBuilder();
            sb4.append("  superclass_idx:      ");
            sb4.append(Hex.oO80(\u30070\u3007O0088o3));
            sb4.append(" // ");
            final CstType \u300708O\u300700\u3007o2 = this.\u300708O\u300700\u3007o;
            final String s = "<none>";
            String human;
            if (\u300708O\u300700\u3007o2 == null) {
                human = "<none>";
            }
            else {
                human = \u300708O\u300700\u3007o2.toHuman();
            }
            sb4.append(human);
            annotatedOutput.oO80(4, sb4.toString());
            final StringBuilder sb5 = new StringBuilder();
            sb5.append("  interfaces_off:      ");
            sb5.append(Hex.oO80(\u300780\u3007808\u3007O));
            annotatedOutput.oO80(4, sb5.toString());
            if (\u300780\u3007808\u3007O != 0) {
                final TypeList \u3007o00 = this.o\u300700O.\u3007O00();
                for (int size = \u3007o00.size(), i = 0; i < size; ++i) {
                    final StringBuilder sb6 = new StringBuilder();
                    sb6.append("    ");
                    sb6.append(\u3007o00.getType(i).toHuman());
                    annotatedOutput.oO80(0, sb6.toString());
                }
            }
            final StringBuilder sb7 = new StringBuilder();
            sb7.append("  source_file_idx:     ");
            sb7.append(Hex.oO80(\u30070\u3007O0088o2));
            sb7.append(" // ");
            final CstString o8o08O8O = this.O8o08O8O;
            String human2;
            if (o8o08O8O == null) {
                human2 = s;
            }
            else {
                human2 = o8o08O8O.toHuman();
            }
            sb7.append(human2);
            annotatedOutput.oO80(4, sb7.toString());
            final StringBuilder sb8 = new StringBuilder();
            sb8.append("  annotations_off:     ");
            sb8.append(Hex.oO80(oo80));
            annotatedOutput.oO80(4, sb8.toString());
            final StringBuilder sb9 = new StringBuilder();
            sb9.append("  class_data_off:      ");
            sb9.append(Hex.oO80(oo81));
            annotatedOutput.oO80(4, sb9.toString());
            final StringBuilder sb10 = new StringBuilder();
            sb10.append("  static_values_off:   ");
            sb10.append(Hex.oO80(\u300780\u3007808\u3007O2));
            annotatedOutput.oO80(4, sb10.toString());
        }
        annotatedOutput.writeInt(\u30070\u3007O0088o);
        annotatedOutput.writeInt(this.OO);
        annotatedOutput.writeInt(\u30070\u3007O0088o3);
        annotatedOutput.writeInt(\u300780\u3007808\u3007O);
        annotatedOutput.writeInt(\u30070\u3007O0088o2);
        annotatedOutput.writeInt(oo80);
        annotatedOutput.writeInt(oo81);
        annotatedOutput.writeInt(\u300780\u3007808\u3007O2);
    }
    
    public TypeList Oooo8o0\u3007() {
        final TypeListItem o\u300700O = this.o\u300700O;
        if (o\u300700O == null) {
            return StdTypeList.OO;
        }
        return o\u300700O.\u3007O00();
    }
    
    @Override
    public void \u3007080(final DexFile dexFile) {
        final TypeIdsSection o800o8O = dexFile.o800o8O();
        final MixedItemSection oo08 = dexFile.Oo08();
        final MixedItemSection oo88o8O = dexFile.oo88o8O();
        final MixedItemSection \u3007o888o0o = dexFile.\u3007O888o0o();
        final StringIdsSection ooO8 = dexFile.OoO8();
        o800o8O.o800o8O(this.\u3007OOo8\u30070);
        if (!this.\u3007080OO8\u30070.o\u3007O8\u3007\u3007o()) {
            dexFile.\u3007\u3007888().\u3007O00(this.\u3007080OO8\u30070);
            final CstArray \u3007oo\u3007 = this.\u3007080OO8\u30070.\u3007oo\u3007();
            if (\u3007oo\u3007 != null) {
                this.\u30070O = oo08.\u3007\u30078O0\u30078(new EncodedArrayItem(\u3007oo\u3007));
            }
        }
        final CstType \u300708O\u300700\u3007o = this.\u300708O\u300700\u3007o;
        if (\u300708O\u300700\u3007o != null) {
            o800o8O.o800o8O(\u300708O\u300700\u3007o);
        }
        final TypeListItem o\u300700O = this.o\u300700O;
        if (o\u300700O != null) {
            this.o\u300700O = \u3007o888o0o.\u3007\u30078O0\u30078(o\u300700O);
        }
        final CstString o8o08O8O = this.O8o08O8O;
        if (o8o08O8O != null) {
            ooO8.o800o8O(o8o08O8O);
        }
        if (!this.oOo\u30078o008.\u3007O00()) {
            if (this.oOo\u30078o008.\u3007\u30078O0\u30078()) {
                this.oOo\u30078o008 = oo88o8O.\u3007\u30078O0\u30078(this.oOo\u30078o008);
            }
            else {
                oo88o8O.\u3007O00(this.oOo\u30078o008);
            }
        }
    }
    
    public void \u30078o8o\u3007(final EncodedField encodedField) {
        this.\u3007080OO8\u30070.\u3007\u30078O0\u30078(encodedField);
    }
    
    public void \u3007O8o08O(final EncodedField encodedField, final Constant constant) {
        this.\u3007080OO8\u30070.\u30070\u3007O0088o(encodedField, constant);
    }
    
    public CstType \u3007O\u3007() {
        return this.\u3007OOo8\u30070;
    }
    
    @Override
    public ItemType \u3007o00\u3007\u3007Oo() {
        return ItemType.TYPE_CLASS_DEF_ITEM;
    }
    
    public CstType \u3007\u3007808\u3007() {
        return this.\u300708O\u300700\u3007o;
    }
}

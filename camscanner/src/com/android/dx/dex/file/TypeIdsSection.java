// 
// Decompiled by Procyon v0.6.0
// 

package com.android.dx.dex.file;

import com.android.dx.rop.cst.Constant;
import java.util.Collection;
import java.util.Iterator;
import com.android.dex.DexIndexOverflowException;
import com.android.dx.util.Hex;
import com.android.dx.util.AnnotatedOutput;
import com.android.dx.rop.cst.CstType;
import com.android.dx.rop.type.Type;
import java.util.TreeMap;

public final class TypeIdsSection extends UniformItemSection
{
    private final TreeMap<Type, TypeIdItem> o\u30070;
    
    public TypeIdsSection(final DexFile dexFile) {
        super("type_ids", dexFile, 4);
        this.o\u30070 = new TreeMap<Type, TypeIdItem>();
    }
    
    public int OoO8(final Type type) {
        if (type == null) {
            throw new NullPointerException("type == null");
        }
        this.\u30078o8o\u3007();
        final TypeIdItem typeIdItem = this.o\u30070.get(type);
        if (typeIdItem != null) {
            return typeIdItem.o\u30070();
        }
        final StringBuilder sb = new StringBuilder();
        sb.append("not found: ");
        sb.append(type);
        throw new IllegalArgumentException(sb.toString());
    }
    
    public TypeIdItem o800o8O(final CstType cstType) {
        monitorenter(this);
        if (cstType != null) {
            Label_0075: {
                try {
                    this.\u3007O8o08O();
                    final Type o\u30070 = cstType.o\u30070();
                    TypeIdItem value;
                    if ((value = this.o\u30070.get(o\u30070)) == null) {
                        value = new TypeIdItem(cstType);
                        this.o\u30070.put(o\u30070, value);
                    }
                    monitorexit(this);
                    return value;
                }
                finally {
                    break Label_0075;
                }
                throw new NullPointerException("type == null");
            }
            monitorexit(this);
        }
        throw new NullPointerException("type == null");
    }
    
    public void oo88o8O(final AnnotatedOutput annotatedOutput) {
        this.\u30078o8o\u3007();
        final int size = this.o\u30070.size();
        int o\u30070;
        if (size == 0) {
            o\u30070 = 0;
        }
        else {
            o\u30070 = this.o\u30070();
        }
        if (size <= 65536) {
            if (annotatedOutput.\u3007o\u3007()) {
                final StringBuilder sb = new StringBuilder();
                sb.append("type_ids_size:   ");
                sb.append(Hex.oO80(size));
                annotatedOutput.oO80(4, sb.toString());
                final StringBuilder sb2 = new StringBuilder();
                sb2.append("type_ids_off:    ");
                sb2.append(Hex.oO80(o\u30070));
                annotatedOutput.oO80(4, sb2.toString());
            }
            annotatedOutput.writeInt(size);
            annotatedOutput.writeInt(o\u30070);
            return;
        }
        throw new DexIndexOverflowException(String.format("Too many type identifiers to fit in one dex file: %1$d; max is %2$d.%nYou may try using multi-dex. If multi-dex is enabled then the list of classes for the main dex list is too large.", this.\u3007\u3007888().size(), 65536));
    }
    
    public int \u30070\u3007O0088o(final CstType cstType) {
        if (cstType != null) {
            return this.OoO8(cstType.o\u30070());
        }
        throw new NullPointerException("type == null");
    }
    
    @Override
    protected void \u3007O00() {
        final Iterator<? extends Item> iterator = this.\u3007\u3007888().iterator();
        int n = 0;
        while (iterator.hasNext()) {
            ((TypeIdItem)iterator.next()).\u300780\u3007808\u3007O(n);
            ++n;
        }
    }
    
    public TypeIdItem \u3007O888o0o(final Type type) {
        monitorenter(this);
        if (type != null) {
            Label_0076: {
                try {
                    this.\u3007O8o08O();
                    TypeIdItem value;
                    if ((value = this.o\u30070.get(type)) == null) {
                        value = new TypeIdItem(new CstType(type));
                        this.o\u30070.put(type, value);
                    }
                    monitorexit(this);
                    return value;
                }
                finally {
                    break Label_0076;
                }
                throw new NullPointerException("type == null");
            }
            monitorexit(this);
        }
        throw new NullPointerException("type == null");
    }
    
    @Override
    public Collection<? extends Item> \u3007\u3007888() {
        return this.o\u30070.values();
    }
    
    public IndexedItem \u3007\u30078O0\u30078(final Constant obj) {
        if (obj == null) {
            throw new NullPointerException("cst == null");
        }
        this.\u30078o8o\u3007();
        final IndexedItem indexedItem = this.o\u30070.get(((CstType)obj).o\u30070());
        if (indexedItem != null) {
            return indexedItem;
        }
        final StringBuilder sb = new StringBuilder();
        sb.append("not found: ");
        sb.append(obj);
        throw new IllegalArgumentException(sb.toString());
    }
}

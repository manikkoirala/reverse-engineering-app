// 
// Decompiled by Procyon v0.6.0
// 

package com.android.dx.dex.file;

import com.android.dex.DexIndexOverflowException;
import java.util.Iterator;
import java.util.Map;
import java.util.Formatter;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.TreeMap;

public abstract class MemberIdsSection extends UniformItemSection
{
    public MemberIdsSection(final String s, final DexFile dexFile) {
        super(s, dexFile, 4);
    }
    
    private String \u3007\u30078O0\u30078() {
        final TreeMap treeMap = new TreeMap();
        final Iterator<? extends Item> iterator = this.\u3007\u3007888().iterator();
        while (iterator.hasNext()) {
            final String oo80 = ((MemberIdItem)iterator.next()).OO0o\u3007\u3007\u3007\u30070().oO80();
            AtomicInteger atomicInteger;
            if ((atomicInteger = (AtomicInteger)treeMap.get(oo80)) == null) {
                atomicInteger = new AtomicInteger();
                treeMap.put(oo80, atomicInteger);
            }
            atomicInteger.incrementAndGet();
        }
        final Formatter formatter = new Formatter();
        try {
            String s;
            if (this instanceof MethodIdsSection) {
                s = "method";
            }
            else {
                s = "field";
            }
            formatter.format("Too many %1$s references to fit in one dex file: %2$d; max is %3$d.%nYou may try using multi-dex. If multi-dex is enabled then the list of classes for the main dex list is too large.%nReferences by package:", s, this.\u3007\u3007888().size(), 65536);
            for (final Map.Entry<K, AtomicInteger> entry : treeMap.entrySet()) {
                formatter.format("%n%6d %s", entry.getValue().get(), entry.getKey());
            }
            return formatter.toString();
        }
        finally {
            formatter.close();
        }
    }
    
    @Override
    protected void \u3007O00() {
        if (this.\u3007\u3007888().size() <= 65536) {
            final Iterator<? extends Item> iterator = this.\u3007\u3007888().iterator();
            int n = 0;
            while (iterator.hasNext()) {
                ((MemberIdItem)iterator.next()).\u300780\u3007808\u3007O(n);
                ++n;
            }
            return;
        }
        throw new DexIndexOverflowException(this.\u3007\u30078O0\u30078());
    }
}

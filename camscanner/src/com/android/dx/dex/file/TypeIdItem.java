// 
// Decompiled by Procyon v0.6.0
// 

package com.android.dx.dex.file;

import com.android.dx.rop.cst.CstString;
import com.android.dx.util.Hex;
import com.android.dx.util.AnnotatedOutput;
import com.android.dx.rop.cst.CstType;

public final class TypeIdItem extends IdItem
{
    public TypeIdItem(final CstType cstType) {
        super(cstType);
    }
    
    @Override
    public int O8() {
        return 4;
    }
    
    @Override
    public void Oo08(final DexFile dexFile, final AnnotatedOutput annotatedOutput) {
        final CstString \u3007\u3007888 = this.OO0o\u3007\u3007\u3007\u30070().\u3007\u3007888();
        final int \u30070\u3007O0088o = dexFile.OoO8().\u30070\u3007O0088o(\u3007\u3007888);
        if (annotatedOutput.\u3007o\u3007()) {
            final StringBuilder sb = new StringBuilder();
            sb.append(this.oO80());
            sb.append(' ');
            sb.append(\u3007\u3007888.toHuman());
            annotatedOutput.oO80(0, sb.toString());
            final StringBuilder sb2 = new StringBuilder();
            sb2.append("  descriptor_idx: ");
            sb2.append(Hex.oO80(\u30070\u3007O0088o));
            annotatedOutput.oO80(4, sb2.toString());
        }
        annotatedOutput.writeInt(\u30070\u3007O0088o);
    }
    
    @Override
    public void \u3007080(final DexFile dexFile) {
        dexFile.OoO8().o800o8O(this.OO0o\u3007\u3007\u3007\u30070().\u3007\u3007888());
    }
    
    @Override
    public ItemType \u3007o00\u3007\u3007Oo() {
        return ItemType.TYPE_TYPE_ID_ITEM;
    }
}

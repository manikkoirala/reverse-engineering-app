// 
// Decompiled by Procyon v0.6.0
// 

package com.android.dx.dex.file;

import java.util.Collection;
import java.util.Collections;
import java.util.List;

public final class HeaderSection extends UniformItemSection
{
    private final List<HeaderItem> o\u30070;
    
    public HeaderSection(final DexFile dexFile) {
        super(null, dexFile, 4);
        final HeaderItem o = new HeaderItem();
        o.\u300780\u3007808\u3007O(0);
        this.o\u30070 = Collections.singletonList(o);
    }
    
    @Override
    protected void \u3007O00() {
    }
    
    @Override
    public Collection<? extends Item> \u3007\u3007888() {
        return this.o\u30070;
    }
}

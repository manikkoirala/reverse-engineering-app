// 
// Decompiled by Procyon v0.6.0
// 

package com.android.dx.dex.file;

import com.android.dx.rop.cst.CstBaseMethodRef;
import com.android.dex.util.ByteOutput;
import com.android.dex.EncodedValueCodec;
import com.android.dx.rop.cst.CstLiteralBits;
import java.util.Collection;
import com.android.dx.util.Hex;
import java.util.Iterator;
import com.android.dx.rop.annotation.NameValuePair;
import com.android.dx.rop.annotation.Annotation;
import com.android.dx.rop.cst.CstBoolean;
import com.android.dx.rop.cst.CstKnownNull;
import com.android.dx.rop.cst.CstAnnotation;
import com.android.dx.rop.cst.CstArray;
import com.android.dx.rop.cst.CstEnumRef;
import com.android.dx.rop.cst.CstMethodRef;
import com.android.dx.rop.cst.CstFieldRef;
import com.android.dx.rop.cst.CstType;
import com.android.dx.rop.cst.CstString;
import com.android.dx.rop.cst.CstMethodHandle;
import com.android.dx.rop.cst.CstProtoRef;
import com.android.dx.rop.cst.CstDouble;
import com.android.dx.rop.cst.CstFloat;
import com.android.dx.rop.cst.CstLong;
import com.android.dx.rop.cst.CstInteger;
import com.android.dx.rop.cst.CstChar;
import com.android.dx.rop.cst.CstShort;
import com.android.dx.rop.cst.CstByte;
import com.android.dx.rop.cst.Constant;
import com.android.dx.util.AnnotatedOutput;

public final class ValueEncoder
{
    private final DexFile \u3007080;
    private final AnnotatedOutput \u3007o00\u3007\u3007Oo;
    
    public ValueEncoder(final DexFile \u3007080, final AnnotatedOutput \u3007o00\u3007\u3007Oo) {
        if (\u3007080 == null) {
            throw new NullPointerException("file == null");
        }
        if (\u3007o00\u3007\u3007Oo != null) {
            this.\u3007080 = \u3007080;
            this.\u3007o00\u3007\u3007Oo = \u3007o00\u3007\u3007Oo;
            return;
        }
        throw new NullPointerException("out == null");
    }
    
    private static int O8(final Constant constant) {
        if (constant instanceof CstByte) {
            return 0;
        }
        if (constant instanceof CstShort) {
            return 2;
        }
        if (constant instanceof CstChar) {
            return 3;
        }
        if (constant instanceof CstInteger) {
            return 4;
        }
        if (constant instanceof CstLong) {
            return 6;
        }
        if (constant instanceof CstFloat) {
            return 16;
        }
        if (constant instanceof CstDouble) {
            return 17;
        }
        if (constant instanceof CstProtoRef) {
            return 21;
        }
        if (constant instanceof CstMethodHandle) {
            return 22;
        }
        if (constant instanceof CstString) {
            return 23;
        }
        if (constant instanceof CstType) {
            return 24;
        }
        if (constant instanceof CstFieldRef) {
            return 25;
        }
        if (constant instanceof CstMethodRef) {
            return 26;
        }
        if (constant instanceof CstEnumRef) {
            return 27;
        }
        if (constant instanceof CstArray) {
            return 28;
        }
        if (constant instanceof CstAnnotation) {
            return 29;
        }
        if (constant instanceof CstKnownNull) {
            return 30;
        }
        if (constant instanceof CstBoolean) {
            return 31;
        }
        throw new RuntimeException("Shouldn't happen");
    }
    
    public static void \u3007080(final DexFile dexFile, final Annotation annotation) {
        final TypeIdsSection o800o8O = dexFile.o800o8O();
        final StringIdsSection ooO8 = dexFile.OoO8();
        o800o8O.o800o8O(annotation.getType());
        for (final NameValuePair nameValuePair : annotation.\u300780\u3007808\u3007O()) {
            ooO8.o800o8O(nameValuePair.\u3007o00\u3007\u3007Oo());
            \u3007o00\u3007\u3007Oo(dexFile, nameValuePair.\u3007o\u3007());
        }
    }
    
    public static void \u3007o00\u3007\u3007Oo(final DexFile dexFile, final Constant constant) {
        if (constant instanceof CstAnnotation) {
            \u3007080(dexFile, ((CstAnnotation)constant).o\u30070());
        }
        else if (constant instanceof CstArray) {
            final CstArray.List o\u30070 = ((CstArray)constant).o\u30070();
            for (int size = o\u30070.size(), i = 0; i < size; ++i) {
                \u3007o00\u3007\u3007Oo(dexFile, o\u30070.\u3007O\u3007(i));
            }
        }
        else {
            dexFile.\u3007oo\u3007(constant);
        }
    }
    
    public static String \u3007o\u3007(final Constant constant) {
        if (O8(constant) == 30) {
            return "null";
        }
        final StringBuilder sb = new StringBuilder();
        sb.append(constant.Oo08());
        sb.append(' ');
        sb.append(constant.toHuman());
        return sb.toString();
    }
    
    public void Oo08(final Annotation annotation, final boolean b) {
        final boolean b2 = b && this.\u3007o00\u3007\u3007Oo.\u3007o\u3007();
        final StringIdsSection ooO8 = this.\u3007080.OoO8();
        final TypeIdsSection o800o8O = this.\u3007080.o800o8O();
        final CstType type = annotation.getType();
        final int \u30070\u3007O0088o = o800o8O.\u30070\u3007O0088o(type);
        if (b2) {
            final AnnotatedOutput \u3007o00\u3007\u3007Oo = this.\u3007o00\u3007\u3007Oo;
            final StringBuilder sb = new StringBuilder();
            sb.append("  type_idx: ");
            sb.append(Hex.oO80(\u30070\u3007O0088o));
            sb.append(" // ");
            sb.append(type.toHuman());
            \u3007o00\u3007\u3007Oo.o\u30070(sb.toString());
        }
        this.\u3007o00\u3007\u3007Oo.\u3007080(o800o8O.\u30070\u3007O0088o(annotation.getType()));
        final Collection<NameValuePair> \u300780\u3007808\u3007O = annotation.\u300780\u3007808\u3007O();
        final int size = \u300780\u3007808\u3007O.size();
        if (b2) {
            final AnnotatedOutput \u3007o00\u3007\u3007Oo2 = this.\u3007o00\u3007\u3007Oo;
            final StringBuilder sb2 = new StringBuilder();
            sb2.append("  size: ");
            sb2.append(Hex.oO80(size));
            \u3007o00\u3007\u3007Oo2.o\u30070(sb2.toString());
        }
        this.\u3007o00\u3007\u3007Oo.\u3007080(size);
        final Iterator iterator = \u300780\u3007808\u3007O.iterator();
        int i = 0;
        while (iterator.hasNext()) {
            final NameValuePair nameValuePair = (NameValuePair)iterator.next();
            final CstString \u3007o00\u3007\u3007Oo3 = nameValuePair.\u3007o00\u3007\u3007Oo();
            final int \u30070\u3007O0088o2 = ooO8.\u30070\u3007O0088o(\u3007o00\u3007\u3007Oo3);
            final Constant \u3007o\u3007 = nameValuePair.\u3007o\u3007();
            int n = i;
            if (b2) {
                final AnnotatedOutput \u3007o00\u3007\u3007Oo4 = this.\u3007o00\u3007\u3007Oo;
                final StringBuilder sb3 = new StringBuilder();
                sb3.append("  elements[");
                sb3.append(i);
                sb3.append("]:");
                \u3007o00\u3007\u3007Oo4.oO80(0, sb3.toString());
                n = i + 1;
                final AnnotatedOutput \u3007o00\u3007\u3007Oo5 = this.\u3007o00\u3007\u3007Oo;
                final StringBuilder sb4 = new StringBuilder();
                sb4.append("    name_idx: ");
                sb4.append(Hex.oO80(\u30070\u3007O0088o2));
                sb4.append(" // ");
                sb4.append(\u3007o00\u3007\u3007Oo3.toHuman());
                \u3007o00\u3007\u3007Oo5.o\u30070(sb4.toString());
            }
            this.\u3007o00\u3007\u3007Oo.\u3007080(\u30070\u3007O0088o2);
            if (b2) {
                final AnnotatedOutput \u3007o00\u3007\u3007Oo6 = this.\u3007o00\u3007\u3007Oo;
                final StringBuilder sb5 = new StringBuilder();
                sb5.append("    value: ");
                sb5.append(\u3007o\u3007(\u3007o\u3007));
                \u3007o00\u3007\u3007Oo6.o\u30070(sb5.toString());
            }
            this.\u3007\u3007888(\u3007o\u3007);
            i = n;
        }
        if (b2) {
            this.\u3007o00\u3007\u3007Oo.\u3007o00\u3007\u3007Oo();
        }
    }
    
    public void o\u30070(final CstArray cstArray, final boolean b) {
        int i = 0;
        final boolean b2 = b && this.\u3007o00\u3007\u3007Oo.\u3007o\u3007();
        final CstArray.List o\u30070 = cstArray.o\u30070();
        final int size = o\u30070.size();
        if (b2) {
            final AnnotatedOutput \u3007o00\u3007\u3007Oo = this.\u3007o00\u3007\u3007Oo;
            final StringBuilder sb = new StringBuilder();
            sb.append("  size: ");
            sb.append(Hex.oO80(size));
            \u3007o00\u3007\u3007Oo.o\u30070(sb.toString());
        }
        this.\u3007o00\u3007\u3007Oo.\u3007080(size);
        while (i < size) {
            final Constant \u3007o\u3007 = o\u30070.\u3007O\u3007(i);
            if (b2) {
                final AnnotatedOutput \u3007o00\u3007\u3007Oo2 = this.\u3007o00\u3007\u3007Oo;
                final StringBuilder sb2 = new StringBuilder();
                sb2.append("  [");
                sb2.append(Integer.toHexString(i));
                sb2.append("] ");
                sb2.append(\u3007o\u3007(\u3007o\u3007));
                \u3007o00\u3007\u3007Oo2.o\u30070(sb2.toString());
            }
            this.\u3007\u3007888(\u3007o\u3007);
            ++i;
        }
        if (b2) {
            this.\u3007o00\u3007\u3007Oo.\u3007o00\u3007\u3007Oo();
        }
    }
    
    public void \u3007\u3007888(final Constant constant) {
        final int o8 = O8(constant);
        if (o8 != 0 && o8 != 6 && o8 != 2) {
            if (o8 == 3) {
                EncodedValueCodec.\u3007o\u3007(this.\u3007o00\u3007\u3007Oo, o8, ((CstLiteralBits)constant).oO80());
                return;
            }
            if (o8 != 4) {
                if (o8 == 16) {
                    EncodedValueCodec.\u3007080(this.\u3007o00\u3007\u3007Oo, o8, ((CstFloat)constant).oO80() << 32);
                    return;
                }
                if (o8 == 17) {
                    EncodedValueCodec.\u3007080(this.\u3007o00\u3007\u3007Oo, o8, ((CstDouble)constant).oO80());
                    return;
                }
                switch (o8) {
                    default: {
                        throw new RuntimeException("Shouldn't happen");
                    }
                    case 31: {
                        this.\u3007o00\u3007\u3007Oo.writeByte(((CstBoolean)constant).\u3007\u3007888() << 5 | o8);
                        return;
                    }
                    case 30: {
                        this.\u3007o00\u3007\u3007Oo.writeByte(o8);
                        return;
                    }
                    case 29: {
                        this.\u3007o00\u3007\u3007Oo.writeByte(o8);
                        this.Oo08(((CstAnnotation)constant).o\u30070(), false);
                        return;
                    }
                    case 28: {
                        this.\u3007o00\u3007\u3007Oo.writeByte(o8);
                        this.o\u30070((CstArray)constant, false);
                        return;
                    }
                    case 27: {
                        EncodedValueCodec.\u3007o\u3007(this.\u3007o00\u3007\u3007Oo, o8, this.\u3007080.OO0o\u3007\u3007\u3007\u30070().OoO8(((CstEnumRef)constant).oO80()));
                        return;
                    }
                    case 26: {
                        EncodedValueCodec.\u3007o\u3007(this.\u3007o00\u3007\u3007Oo, o8, this.\u3007080.\u3007O\u3007().OoO8((CstBaseMethodRef)constant));
                        return;
                    }
                    case 25: {
                        EncodedValueCodec.\u3007o\u3007(this.\u3007o00\u3007\u3007Oo, o8, this.\u3007080.OO0o\u3007\u3007\u3007\u30070().OoO8((CstFieldRef)constant));
                        return;
                    }
                    case 24: {
                        EncodedValueCodec.\u3007o\u3007(this.\u3007o00\u3007\u3007Oo, o8, this.\u3007080.o800o8O().\u30070\u3007O0088o((CstType)constant));
                        return;
                    }
                    case 23: {
                        EncodedValueCodec.\u3007o\u3007(this.\u3007o00\u3007\u3007Oo, o8, this.\u3007080.OoO8().\u30070\u3007O0088o((CstString)constant));
                        return;
                    }
                    case 22: {
                        EncodedValueCodec.\u3007o\u3007(this.\u3007o00\u3007\u3007Oo, o8, this.\u3007080.\u3007\u3007808\u3007().\u30070\u3007O0088o((CstMethodHandle)constant));
                        return;
                    }
                    case 21: {
                        EncodedValueCodec.\u3007o\u3007(this.\u3007o00\u3007\u3007Oo, o8, this.\u3007080.\u3007O00().\u30070\u3007O0088o(((CstProtoRef)constant).o\u30070()));
                        return;
                    }
                }
            }
        }
        EncodedValueCodec.\u3007o00\u3007\u3007Oo(this.\u3007o00\u3007\u3007Oo, o8, ((CstLiteralBits)constant).oO80());
    }
}

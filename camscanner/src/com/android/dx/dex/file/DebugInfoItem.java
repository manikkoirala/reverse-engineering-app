// 
// Decompiled by Procyon v0.6.0
// 

package com.android.dx.dex.file;

import com.android.dex.util.ExceptionWithContext;
import com.android.dx.dex.code.DalvInsnList;
import com.android.dx.dex.code.LocalList;
import com.android.dx.dex.code.PositionList;
import com.android.dx.util.AnnotatedOutput;
import java.io.PrintWriter;
import com.android.dx.rop.cst.CstMethodRef;
import com.android.dx.dex.code.DalvCode;

public class DebugInfoItem extends OffsettedItem
{
    private byte[] O8o08O8O;
    private final DalvCode o\u300700O;
    private final boolean \u3007080OO8\u30070;
    private final CstMethodRef \u30070O;
    
    public DebugInfoItem(final DalvCode o\u300700O, final boolean \u3007080OO8\u30070, final CstMethodRef \u30070O) {
        super(1, -1);
        if (o\u300700O != null) {
            this.o\u300700O = o\u300700O;
            this.\u3007080OO8\u30070 = \u3007080OO8\u30070;
            this.\u30070O = \u30070O;
            return;
        }
        throw new NullPointerException("code == null");
    }
    
    private byte[] \u30070\u3007O0088o(final DexFile dexFile, final String s, final PrintWriter printWriter, final AnnotatedOutput annotatedOutput, final boolean b) {
        final PositionList oo80 = this.o\u300700O.oO80();
        final LocalList \u3007\u3007888 = this.o\u300700O.\u3007\u3007888();
        final DalvInsnList o\u30070 = this.o\u300700O.o\u30070();
        final DebugInfoEncoder debugInfoEncoder = new DebugInfoEncoder(oo80, \u3007\u3007888, dexFile, o\u30070.\u3007\u3007808\u3007(), o\u30070.\u3007\u30078O0\u30078(), this.\u3007080OO8\u30070, this.\u30070O);
        byte[] array;
        if (printWriter == null && annotatedOutput == null) {
            array = debugInfoEncoder.O8();
        }
        else {
            array = debugInfoEncoder.o\u30070(s, printWriter, annotatedOutput, b);
        }
        return array;
    }
    
    private byte[] \u3007\u30078O0\u30078(final DexFile dexFile, final String s, final PrintWriter printWriter, final AnnotatedOutput annotatedOutput, final boolean b) {
        return this.\u30070\u3007O0088o(dexFile, s, printWriter, annotatedOutput, b);
    }
    
    @Override
    protected void OO0o\u3007\u3007(final Section section, final int n) {
        try {
            final byte[] \u3007\u30078O0\u30078 = this.\u3007\u30078O0\u30078(section.Oo08(), null, null, null, false);
            this.O8o08O8O = \u3007\u30078O0\u30078;
            this.Oooo8o0\u3007(\u3007\u30078O0\u30078.length);
        }
        catch (final RuntimeException ex) {
            final StringBuilder sb = new StringBuilder();
            sb.append("...while placing debug info for ");
            sb.append(this.\u30070O.toHuman());
            throw ExceptionWithContext.withContext(ex, sb.toString());
        }
    }
    
    @Override
    public void \u3007080(final DexFile dexFile) {
    }
    
    public void \u3007O00(final DexFile dexFile, final AnnotatedOutput annotatedOutput, final String s) {
        this.\u3007\u30078O0\u30078(dexFile, s, null, annotatedOutput, false);
    }
    
    @Override
    protected void \u3007O\u3007(final DexFile dexFile, final AnnotatedOutput annotatedOutput) {
        if (annotatedOutput.\u3007o\u3007()) {
            final StringBuilder sb = new StringBuilder();
            sb.append(this.\u30078o8o\u3007());
            sb.append(" debug info");
            annotatedOutput.o\u30070(sb.toString());
            this.\u3007\u30078O0\u30078(dexFile, null, null, annotatedOutput, true);
        }
        annotatedOutput.write(this.O8o08O8O);
    }
    
    @Override
    public ItemType \u3007o00\u3007\u3007Oo() {
        return ItemType.TYPE_DEBUG_INFO_ITEM;
    }
    
    @Override
    public String \u3007\u3007808\u3007() {
        throw new RuntimeException("unsupported");
    }
}

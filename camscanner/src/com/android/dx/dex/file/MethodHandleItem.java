// 
// Decompiled by Procyon v0.6.0
// 

package com.android.dx.dex.file;

import com.android.dx.util.Hex;
import com.android.dx.util.AnnotatedOutput;
import com.android.dx.rop.cst.CstMemberRef;
import com.android.dx.rop.cst.Constant;
import com.android.dx.rop.cst.CstBaseMethodRef;
import com.android.dx.rop.cst.CstInterfaceMethodRef;
import com.android.dx.rop.cst.CstFieldRef;
import com.android.dx.rop.cst.CstMethodHandle;

public final class MethodHandleItem extends IndexedItem
{
    private final CstMethodHandle OO;
    private final int \u3007OOo8\u30070;
    
    public MethodHandleItem(final CstMethodHandle oo) {
        this.\u3007OOo8\u30070 = 8;
        this.OO = oo;
    }
    
    private int OO0o\u3007\u3007\u3007\u30070(final DexFile dexFile) {
        final Constant oo80 = this.OO.oO80();
        if (this.OO.\u300780\u3007808\u3007O()) {
            return dexFile.OO0o\u3007\u3007\u3007\u30070().OoO8((CstFieldRef)oo80);
        }
        if (this.OO.\u30078o8o\u3007()) {
            CstMemberRef \u3007o8o08O = (CstMemberRef)oo80;
            if (oo80 instanceof CstInterfaceMethodRef) {
                \u3007o8o08O = ((CstInterfaceMethodRef)oo80).\u3007O8o08O();
            }
            return dexFile.\u3007O\u3007().OoO8((CstBaseMethodRef)\u3007o8o08O);
        }
        throw new IllegalStateException("Unhandled invocation type");
    }
    
    @Override
    public int O8() {
        return 8;
    }
    
    @Override
    public void Oo08(final DexFile dexFile, final AnnotatedOutput annotatedOutput) {
        final int oo0o\u3007\u3007\u3007\u30070 = this.OO0o\u3007\u3007\u3007\u30070(dexFile);
        final int o\u30070 = this.OO.o\u30070();
        if (annotatedOutput.\u3007o\u3007()) {
            final StringBuilder sb = new StringBuilder();
            sb.append(this.oO80());
            sb.append(' ');
            sb.append(this.OO.toString());
            annotatedOutput.oO80(0, sb.toString());
            final StringBuilder sb2 = new StringBuilder();
            sb2.append(" // ");
            sb2.append(CstMethodHandle.\u3007\u3007888(o\u30070));
            final String string = sb2.toString();
            final StringBuilder sb3 = new StringBuilder();
            sb3.append("type:     ");
            sb3.append(Hex.Oo08(o\u30070));
            sb3.append(string);
            annotatedOutput.oO80(2, sb3.toString());
            final StringBuilder sb4 = new StringBuilder();
            sb4.append("reserved: ");
            sb4.append(Hex.Oo08(0));
            annotatedOutput.oO80(2, sb4.toString());
            final StringBuilder sb5 = new StringBuilder();
            sb5.append(" // ");
            sb5.append(this.OO.oO80().toString());
            final String string2 = sb5.toString();
            if (this.OO.\u300780\u3007808\u3007O()) {
                final StringBuilder sb6 = new StringBuilder();
                sb6.append("fieldId:  ");
                sb6.append(Hex.Oo08(oo0o\u3007\u3007\u3007\u30070));
                sb6.append(string2);
                annotatedOutput.oO80(2, sb6.toString());
            }
            else {
                final StringBuilder sb7 = new StringBuilder();
                sb7.append("methodId: ");
                sb7.append(Hex.Oo08(oo0o\u3007\u3007\u3007\u30070));
                sb7.append(string2);
                annotatedOutput.oO80(2, sb7.toString());
            }
            final StringBuilder sb8 = new StringBuilder();
            sb8.append("reserved: ");
            sb8.append(Hex.Oo08(0));
            annotatedOutput.oO80(2, sb8.toString());
        }
        annotatedOutput.writeShort(o\u30070);
        annotatedOutput.writeShort(0);
        annotatedOutput.writeShort(this.OO0o\u3007\u3007\u3007\u30070(dexFile));
        annotatedOutput.writeShort(0);
    }
    
    @Override
    public void \u3007080(final DexFile dexFile) {
        dexFile.\u3007\u3007808\u3007().OoO8(this.OO);
    }
    
    @Override
    public ItemType \u3007o00\u3007\u3007Oo() {
        return ItemType.TYPE_METHOD_HANDLE_ITEM;
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.android.dx.dex.file;

import com.android.dx.rop.cst.CstCallSite;
import com.android.dx.rop.cst.Constant;
import com.android.dx.util.Hex;
import com.android.dx.util.AnnotatedOutput;
import com.android.dx.rop.cst.CstCallSiteRef;

public final class CallSiteIdItem extends IndexedItem implements Comparable
{
    CallSiteItem OO;
    final CstCallSiteRef \u3007OOo8\u30070;
    
    @Override
    public int O8() {
        return 4;
    }
    
    @Override
    public void Oo08(final DexFile dexFile, final AnnotatedOutput annotatedOutput) {
        final int oo80 = this.OO.oO80();
        if (annotatedOutput.\u3007o\u3007()) {
            final StringBuilder sb = new StringBuilder();
            sb.append(this.oO80());
            sb.append(' ');
            sb.append(this.\u3007OOo8\u30070.toString());
            annotatedOutput.oO80(0, sb.toString());
            final StringBuilder sb2 = new StringBuilder();
            sb2.append("call_site_off: ");
            sb2.append(Hex.oO80(oo80));
            annotatedOutput.oO80(4, sb2.toString());
        }
        annotatedOutput.writeInt(oo80);
    }
    
    @Override
    public int compareTo(final Object o) {
        return this.\u3007OOo8\u30070.\u3007o\u3007(((CallSiteIdItem)o).\u3007OOo8\u30070);
    }
    
    @Override
    public void \u3007080(final DexFile dexFile) {
        this.\u3007OOo8\u30070.o\u30070();
        final CallSiteIdsSection o\u30070 = dexFile.o\u30070();
        CallSiteItem ooO8;
        if ((ooO8 = o\u30070.OoO8(null)) == null) {
            final MixedItemSection oo08 = dexFile.Oo08();
            ooO8 = new CallSiteItem(null);
            oo08.\u3007O00(ooO8);
            o\u30070.\u3007\u30078O0\u30078(null, ooO8);
        }
        this.OO = ooO8;
    }
    
    @Override
    public ItemType \u3007o00\u3007\u3007Oo() {
        return ItemType.TYPE_CALL_SITE_ID_ITEM;
    }
}

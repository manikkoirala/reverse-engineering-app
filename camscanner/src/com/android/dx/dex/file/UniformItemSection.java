// 
// Decompiled by Procyon v0.6.0
// 

package com.android.dx.dex.file;

import com.android.dx.util.AnnotatedOutput;
import java.util.Iterator;
import java.util.Collection;

public abstract class UniformItemSection extends Section
{
    public UniformItemSection(final String s, final DexFile dexFile, final int n) {
        super(s, dexFile, n);
    }
    
    @Override
    public final int Oooo8o0\u3007() {
        final Collection<? extends Item> \u3007\u3007888 = this.\u3007\u3007888();
        final int size = \u3007\u3007888.size();
        if (size == 0) {
            return 0;
        }
        return size * \u3007\u3007888.iterator().next().O8();
    }
    
    @Override
    protected final void \u300780\u3007808\u3007O() {
        final DexFile oo08 = this.Oo08();
        this.\u3007O00();
        final Iterator<? extends Item> iterator = this.\u3007\u3007888().iterator();
        while (iterator.hasNext()) {
            ((Item)iterator.next()).\u3007080(oo08);
        }
    }
    
    protected abstract void \u3007O00();
    
    @Override
    protected final void \u3007O\u3007(final AnnotatedOutput annotatedOutput) {
        final DexFile oo08 = this.Oo08();
        final int o8 = this.O8();
        final Iterator<? extends Item> iterator = this.\u3007\u3007888().iterator();
        while (iterator.hasNext()) {
            ((Item)iterator.next()).Oo08(oo08, annotatedOutput);
            annotatedOutput.Oo08(o8);
        }
    }
    
    @Override
    public final int \u3007o00\u3007\u3007Oo(final Item item) {
        final IndexedItem indexedItem = (IndexedItem)item;
        return this.\u3007o\u3007(indexedItem.o\u30070() * indexedItem.O8());
    }
}

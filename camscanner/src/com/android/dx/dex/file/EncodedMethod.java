// 
// Decompiled by Procyon v0.6.0
// 

package com.android.dx.dex.file;

import com.android.dx.rop.code.AccessFlags;
import com.android.dex.Leb128;
import com.android.dx.util.AnnotatedOutput;
import com.android.dx.util.Hex;
import com.android.dx.rop.cst.Constant;
import com.android.dx.rop.cst.CstBaseMethodRef;
import com.android.dx.rop.type.TypeList;
import com.android.dx.dex.code.DalvCode;
import com.android.dx.rop.cst.CstMethodRef;

public final class EncodedMethod extends EncodedMember implements Comparable<EncodedMethod>
{
    private final CodeItem OO;
    private final CstMethodRef \u3007OOo8\u30070;
    
    public EncodedMethod(final CstMethodRef \u3007oOo8\u30070, final int n, final DalvCode dalvCode, final TypeList list) {
        super(n);
        if (\u3007oOo8\u30070 != null) {
            this.\u3007OOo8\u30070 = \u3007oOo8\u30070;
            if (dalvCode == null) {
                this.OO = null;
            }
            else {
                this.OO = new CodeItem(\u3007oOo8\u30070, dalvCode, (n & 0x8) != 0x0, list);
            }
            return;
        }
        throw new NullPointerException("method == null");
    }
    
    public void Oo08(final DexFile dexFile) {
        final MethodIdsSection \u3007o\u3007 = dexFile.\u3007O\u3007();
        final MixedItemSection oo88o8O = dexFile.oo88o8O();
        \u3007o\u3007.o800o8O(this.\u3007OOo8\u30070);
        final CodeItem oo = this.OO;
        if (oo != null) {
            oo88o8O.\u3007O00(oo);
        }
    }
    
    @Override
    public boolean equals(final Object o) {
        final boolean b = o instanceof EncodedMethod;
        boolean b2 = false;
        if (!b) {
            return false;
        }
        if (this.o\u30070((EncodedMethod)o) == 0) {
            b2 = true;
        }
        return b2;
    }
    
    public int o\u30070(final EncodedMethod encodedMethod) {
        return this.\u3007OOo8\u30070.\u3007o\u3007(encodedMethod.\u3007OOo8\u30070);
    }
    
    @Override
    public final String toHuman() {
        return this.\u3007OOo8\u30070.toHuman();
    }
    
    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder(100);
        sb.append(EncodedMethod.class.getName());
        sb.append('{');
        sb.append(Hex.Oo08(this.O8()));
        sb.append(' ');
        sb.append(this.\u3007OOo8\u30070);
        if (this.OO != null) {
            sb.append(' ');
            sb.append(this.OO);
        }
        sb.append('}');
        return sb.toString();
    }
    
    @Override
    public int \u3007o\u3007(final DexFile dexFile, final AnnotatedOutput annotatedOutput, int n, final int i) {
        final int ooO8 = dexFile.\u3007O\u3007().OoO8(this.\u3007OOo8\u30070);
        final int n2 = ooO8 - n;
        final int o8 = this.O8();
        final int \u300780\u3007808\u3007O = OffsettedItem.\u300780\u3007808\u3007O(this.OO);
        if (\u300780\u3007808\u3007O != 0) {
            n = 1;
        }
        else {
            n = 0;
        }
        if (n == (((o8 & 0x500) == 0x0) ? 1 : 0)) {
            if (annotatedOutput.\u3007o\u3007()) {
                annotatedOutput.oO80(0, String.format("  [%x] %s", i, this.\u3007OOo8\u30070.toHuman()));
                n = Leb128.\u3007080(n2);
                final StringBuilder sb = new StringBuilder();
                sb.append("    method_idx:   ");
                sb.append(Hex.oO80(ooO8));
                annotatedOutput.oO80(n, sb.toString());
                n = Leb128.\u3007080(o8);
                final StringBuilder sb2 = new StringBuilder();
                sb2.append("    access_flags: ");
                sb2.append(AccessFlags.O8(o8));
                annotatedOutput.oO80(n, sb2.toString());
                n = Leb128.\u3007080(\u300780\u3007808\u3007O);
                final StringBuilder sb3 = new StringBuilder();
                sb3.append("    code_off:     ");
                sb3.append(Hex.oO80(\u300780\u3007808\u3007O));
                annotatedOutput.oO80(n, sb3.toString());
            }
            annotatedOutput.\u3007080(n2);
            annotatedOutput.\u3007080(o8);
            annotatedOutput.\u3007080(\u300780\u3007808\u3007O);
            return ooO8;
        }
        throw new UnsupportedOperationException("code vs. access_flags mismatch");
    }
}

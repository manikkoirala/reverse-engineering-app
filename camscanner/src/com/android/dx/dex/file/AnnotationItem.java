// 
// Decompiled by Procyon v0.6.0
// 

package com.android.dx.dex.file;

import com.android.dx.rop.cst.Constant;
import com.android.dx.rop.cst.CstString;
import java.util.Iterator;
import com.android.dx.rop.annotation.NameValuePair;
import com.android.dx.rop.annotation.AnnotationVisibility;
import com.android.dx.util.AnnotatedOutput;
import com.android.dx.util.ByteArrayAnnotatedOutput;
import java.util.Comparator;
import java.util.Arrays;
import com.android.dx.rop.annotation.Annotation;

public final class AnnotationItem extends OffsettedItem
{
    private static final TypeIdSorter \u30070O;
    private TypeIdItem O8o08O8O;
    private final Annotation o\u300700O;
    private byte[] \u3007080OO8\u30070;
    
    static {
        \u30070O = new TypeIdSorter();
    }
    
    public static void \u30070\u3007O0088o(final AnnotationItem[] a) {
        Arrays.sort(a, AnnotationItem.\u30070O);
    }
    
    @Override
    protected void OO0o\u3007\u3007(final Section section, final int n) {
        final ByteArrayAnnotatedOutput byteArrayAnnotatedOutput = new ByteArrayAnnotatedOutput();
        new ValueEncoder(section.Oo08(), byteArrayAnnotatedOutput).Oo08(this.o\u300700O, false);
        final byte[] \u3007o00 = byteArrayAnnotatedOutput.\u3007O00();
        this.\u3007080OO8\u30070 = \u3007o00;
        this.Oooo8o0\u3007(\u3007o00.length + 1);
    }
    
    @Override
    public int hashCode() {
        return this.o\u300700O.hashCode();
    }
    
    @Override
    public void \u3007080(final DexFile dexFile) {
        this.O8o08O8O = dexFile.o800o8O().o800o8O(this.o\u300700O.getType());
        ValueEncoder.\u3007080(dexFile, this.o\u300700O);
    }
    
    @Override
    protected void \u3007O\u3007(final DexFile dexFile, final AnnotatedOutput annotatedOutput) {
        final boolean \u3007o\u3007 = annotatedOutput.\u3007o\u3007();
        final AnnotationVisibility oo0o\u3007\u3007\u3007\u30070 = this.o\u300700O.OO0o\u3007\u3007\u3007\u30070();
        if (\u3007o\u3007) {
            final StringBuilder sb = new StringBuilder();
            sb.append(this.\u30078o8o\u3007());
            sb.append(" annotation");
            annotatedOutput.oO80(0, sb.toString());
            final StringBuilder sb2 = new StringBuilder();
            sb2.append("  visibility: VISBILITY_");
            sb2.append(oo0o\u3007\u3007\u3007\u30070);
            annotatedOutput.oO80(1, sb2.toString());
        }
        final int n = AnnotationItem$1.\u3007080[oo0o\u3007\u3007\u3007\u30070.ordinal()];
        if (n != 1) {
            if (n != 2) {
                if (n != 3) {
                    throw new RuntimeException("shouldn't happen");
                }
                annotatedOutput.writeByte(2);
            }
            else {
                annotatedOutput.writeByte(1);
            }
        }
        else {
            annotatedOutput.writeByte(0);
        }
        if (\u3007o\u3007) {
            new ValueEncoder(dexFile, annotatedOutput).Oo08(this.o\u300700O, true);
        }
        else {
            annotatedOutput.write(this.\u3007080OO8\u30070);
        }
    }
    
    @Override
    public ItemType \u3007o00\u3007\u3007Oo() {
        return ItemType.TYPE_ANNOTATION_ITEM;
    }
    
    @Override
    public String \u3007\u3007808\u3007() {
        return this.o\u300700O.toHuman();
    }
    
    @Override
    protected int \u3007\u3007888(final OffsettedItem offsettedItem) {
        return this.o\u300700O.oO80(((AnnotationItem)offsettedItem).o\u300700O);
    }
    
    public void \u3007\u30078O0\u30078(final AnnotatedOutput annotatedOutput, final String str) {
        final StringBuilder sb = new StringBuilder();
        sb.append(str);
        sb.append("visibility: ");
        sb.append(this.o\u300700O.OO0o\u3007\u3007\u3007\u30070().toHuman());
        annotatedOutput.oO80(0, sb.toString());
        final StringBuilder sb2 = new StringBuilder();
        sb2.append(str);
        sb2.append("type: ");
        sb2.append(this.o\u300700O.getType().toHuman());
        annotatedOutput.oO80(0, sb2.toString());
        for (final NameValuePair nameValuePair : this.o\u300700O.\u300780\u3007808\u3007O()) {
            final CstString \u3007o00\u3007\u3007Oo = nameValuePair.\u3007o00\u3007\u3007Oo();
            final Constant \u3007o\u3007 = nameValuePair.\u3007o\u3007();
            final StringBuilder sb3 = new StringBuilder();
            sb3.append(str);
            sb3.append(\u3007o00\u3007\u3007Oo.toHuman());
            sb3.append(": ");
            sb3.append(ValueEncoder.\u3007o\u3007(\u3007o\u3007));
            annotatedOutput.oO80(0, sb3.toString());
        }
    }
    
    private static class TypeIdSorter implements Comparator<AnnotationItem>
    {
        public int \u3007080(final AnnotationItem annotationItem, final AnnotationItem annotationItem2) {
            final int o\u30070 = annotationItem.O8o08O8O.o\u30070();
            final int o\u30072 = annotationItem2.O8o08O8O.o\u30070();
            if (o\u30070 < o\u30072) {
                return -1;
            }
            if (o\u30070 > o\u30072) {
                return 1;
            }
            return 0;
        }
    }
}

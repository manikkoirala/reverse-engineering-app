// 
// Decompiled by Procyon v0.6.0
// 

package com.android.dx.dex.file;

import com.android.dx.util.AnnotatedOutput;

public abstract class Item
{
    public abstract int O8();
    
    public abstract void Oo08(final DexFile p0, final AnnotatedOutput p1);
    
    public abstract void \u3007080(final DexFile p0);
    
    public abstract ItemType \u3007o00\u3007\u3007Oo();
    
    public final String \u3007o\u3007() {
        return this.\u3007o00\u3007\u3007Oo().toHuman();
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.android.dx.dex.file;

import com.android.dx.util.Hex;
import com.android.dx.util.AnnotatedOutput;

public final class AnnotationSetRefItem extends OffsettedItem
{
    private AnnotationSetItem o\u300700O;
    
    @Override
    public void \u3007080(final DexFile dexFile) {
        this.o\u300700O = dexFile.oo88o8O().\u3007\u30078O0\u30078(this.o\u300700O);
    }
    
    @Override
    protected void \u3007O\u3007(final DexFile dexFile, final AnnotatedOutput annotatedOutput) {
        final int oo80 = this.o\u300700O.oO80();
        if (annotatedOutput.\u3007o\u3007()) {
            final StringBuilder sb = new StringBuilder();
            sb.append("  annotations_off: ");
            sb.append(Hex.oO80(oo80));
            annotatedOutput.oO80(4, sb.toString());
        }
        annotatedOutput.writeInt(oo80);
    }
    
    @Override
    public ItemType \u3007o00\u3007\u3007Oo() {
        return ItemType.TYPE_ANNOTATION_SET_REF_ITEM;
    }
    
    @Override
    public String \u3007\u3007808\u3007() {
        return this.o\u300700O.\u3007\u3007808\u3007();
    }
}

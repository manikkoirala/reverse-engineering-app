// 
// Decompiled by Procyon v0.6.0
// 

package com.android.dx.dex.file;

import com.android.dx.util.AnnotatedOutput;
import com.android.dx.util.ToHuman;

public abstract class EncodedMember implements ToHuman
{
    private final int o0;
    
    public EncodedMember(final int o0) {
        this.o0 = o0;
    }
    
    public final int O8() {
        return this.o0;
    }
    
    public abstract int \u3007o\u3007(final DexFile p0, final AnnotatedOutput p1, final int p2, final int p3);
}

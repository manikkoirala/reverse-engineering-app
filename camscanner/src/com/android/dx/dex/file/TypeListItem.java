// 
// Decompiled by Procyon v0.6.0
// 

package com.android.dx.dex.file;

import com.android.dx.rop.type.Type;
import com.android.dx.util.Hex;
import com.android.dx.util.AnnotatedOutput;
import com.android.dx.rop.type.StdTypeList;
import com.android.dx.rop.type.TypeList;

public final class TypeListItem extends OffsettedItem
{
    private final TypeList o\u300700O;
    
    public TypeListItem(final TypeList o\u300700O) {
        super(4, o\u300700O.size() * 2 + 4);
        this.o\u300700O = o\u300700O;
    }
    
    @Override
    public int hashCode() {
        return StdTypeList.\u3007\u30078O0\u30078(this.o\u300700O);
    }
    
    @Override
    public void \u3007080(final DexFile dexFile) {
        final TypeIdsSection o800o8O = dexFile.o800o8O();
        for (int size = this.o\u300700O.size(), i = 0; i < size; ++i) {
            o800o8O.\u3007O888o0o(this.o\u300700O.getType(i));
        }
    }
    
    public TypeList \u3007O00() {
        return this.o\u300700O;
    }
    
    @Override
    protected void \u3007O\u3007(final DexFile dexFile, final AnnotatedOutput annotatedOutput) {
        final TypeIdsSection o800o8O = dexFile.o800o8O();
        final int size = this.o\u300700O.size();
        final boolean \u3007o\u3007 = annotatedOutput.\u3007o\u3007();
        final int n = 0;
        if (\u3007o\u3007) {
            final StringBuilder sb = new StringBuilder();
            sb.append(this.\u30078o8o\u3007());
            sb.append(" type_list");
            annotatedOutput.oO80(0, sb.toString());
            final StringBuilder sb2 = new StringBuilder();
            sb2.append("  size: ");
            sb2.append(Hex.oO80(size));
            annotatedOutput.oO80(4, sb2.toString());
            for (int i = 0; i < size; ++i) {
                final Type type = this.o\u300700O.getType(i);
                final int ooO8 = o800o8O.OoO8(type);
                final StringBuilder sb3 = new StringBuilder();
                sb3.append("  ");
                sb3.append(Hex.Oo08(ooO8));
                sb3.append(" // ");
                sb3.append(type.toHuman());
                annotatedOutput.oO80(2, sb3.toString());
            }
        }
        annotatedOutput.writeInt(size);
        for (int j = n; j < size; ++j) {
            annotatedOutput.writeShort(o800o8O.OoO8(this.o\u300700O.getType(j)));
        }
    }
    
    @Override
    public ItemType \u3007o00\u3007\u3007Oo() {
        return ItemType.TYPE_TYPE_LIST;
    }
    
    @Override
    public String \u3007\u3007808\u3007() {
        throw new RuntimeException("unsupported");
    }
    
    @Override
    protected int \u3007\u3007888(final OffsettedItem offsettedItem) {
        return StdTypeList.\u3007\u3007808\u3007(this.o\u300700O, ((TypeListItem)offsettedItem).o\u300700O);
    }
}

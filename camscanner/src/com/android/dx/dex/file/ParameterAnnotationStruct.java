// 
// Decompiled by Procyon v0.6.0
// 

package com.android.dx.dex.file;

import java.util.Iterator;
import com.android.dx.util.Hex;
import com.android.dx.rop.cst.CstBaseMethodRef;
import com.android.dx.util.AnnotatedOutput;
import com.android.dx.rop.cst.Constant;
import com.android.dx.rop.cst.CstMethodRef;
import com.android.dx.util.ToHuman;

public final class ParameterAnnotationStruct implements ToHuman, Comparable<ParameterAnnotationStruct>
{
    private final CstMethodRef o0;
    private final UniformListItem<AnnotationSetRefItem> \u3007OOo8\u30070;
    
    public int O8(final ParameterAnnotationStruct parameterAnnotationStruct) {
        return this.o0.\u3007o\u3007(parameterAnnotationStruct.o0);
    }
    
    public void Oo08(final DexFile dexFile, final AnnotatedOutput annotatedOutput) {
        final int ooO8 = dexFile.\u3007O\u3007().OoO8(this.o0);
        final int oo80 = this.\u3007OOo8\u30070.oO80();
        if (annotatedOutput.\u3007o\u3007()) {
            final StringBuilder sb = new StringBuilder();
            sb.append("    ");
            sb.append(this.o0.toHuman());
            annotatedOutput.oO80(0, sb.toString());
            final StringBuilder sb2 = new StringBuilder();
            sb2.append("      method_idx:      ");
            sb2.append(Hex.oO80(ooO8));
            annotatedOutput.oO80(4, sb2.toString());
            final StringBuilder sb3 = new StringBuilder();
            sb3.append("      annotations_off: ");
            sb3.append(Hex.oO80(oo80));
            annotatedOutput.oO80(4, sb3.toString());
        }
        annotatedOutput.writeInt(ooO8);
        annotatedOutput.writeInt(oo80);
    }
    
    @Override
    public boolean equals(final Object o) {
        return o instanceof ParameterAnnotationStruct && this.o0.equals(((ParameterAnnotationStruct)o).o0);
    }
    
    @Override
    public int hashCode() {
        return this.o0.hashCode();
    }
    
    @Override
    public String toHuman() {
        final StringBuilder sb = new StringBuilder();
        sb.append(this.o0.toHuman());
        sb.append(": ");
        final Iterator<AnnotationSetRefItem> iterator = this.\u3007OOo8\u30070.\u3007\u30078O0\u30078().iterator();
        int n = 1;
        while (iterator.hasNext()) {
            final AnnotationSetRefItem annotationSetRefItem = iterator.next();
            if (n != 0) {
                n = 0;
            }
            else {
                sb.append(", ");
            }
            sb.append(annotationSetRefItem.\u3007\u3007808\u3007());
        }
        return sb.toString();
    }
    
    public void \u3007o\u3007(final DexFile dexFile) {
        final MethodIdsSection \u3007o\u3007 = dexFile.\u3007O\u3007();
        final MixedItemSection oo88o8O = dexFile.oo88o8O();
        \u3007o\u3007.o800o8O(this.o0);
        oo88o8O.\u3007O00(this.\u3007OOo8\u30070);
    }
}

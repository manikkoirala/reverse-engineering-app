// 
// Decompiled by Procyon v0.6.0
// 

package com.android.dx.dex.file;

import com.android.dx.util.Hex;
import com.android.dx.util.AnnotatedOutput;
import java.util.Iterator;
import java.util.List;
import java.util.ArrayList;

public final class MapItem extends OffsettedItem
{
    private final Section O8o08O8O;
    private final int oOo\u30078o008;
    private final ItemType o\u300700O;
    private final Item \u3007080OO8\u30070;
    private final Item \u30070O;
    
    private MapItem(final ItemType o\u300700O, final Section o8o08O8O, final Item \u3007080OO8\u30070, final Item \u30070O, final int oOo\u30078o008) {
        super(4, 12);
        if (o\u300700O == null) {
            throw new NullPointerException("type == null");
        }
        if (o8o08O8O == null) {
            throw new NullPointerException("section == null");
        }
        if (\u3007080OO8\u30070 == null) {
            throw new NullPointerException("firstItem == null");
        }
        if (\u30070O == null) {
            throw new NullPointerException("lastItem == null");
        }
        if (oOo\u30078o008 > 0) {
            this.o\u300700O = o\u300700O;
            this.O8o08O8O = o8o08O8O;
            this.\u3007080OO8\u30070 = \u3007080OO8\u30070;
            this.\u30070O = \u30070O;
            this.oOo\u30078o008 = oOo\u30078o008;
            return;
        }
        throw new IllegalArgumentException("itemCount <= 0");
    }
    
    private MapItem(final Section o8o08O8O) {
        super(4, 12);
        if (o8o08O8O != null) {
            this.o\u300700O = ItemType.TYPE_MAP_LIST;
            this.O8o08O8O = o8o08O8O;
            this.\u3007080OO8\u30070 = null;
            this.\u30070O = null;
            this.oOo\u30078o008 = 1;
            return;
        }
        throw new NullPointerException("section == null");
    }
    
    public static void \u3007O00(final Section[] array, final MixedItemSection mixedItemSection) {
        if (array == null) {
            throw new NullPointerException("sections == null");
        }
        if (mixedItemSection.\u3007\u3007888().size() == 0) {
            final ArrayList list = new ArrayList(50);
            for (final Section section : array) {
                final Iterator<? extends Item> iterator = section.\u3007\u3007888().iterator();
                ItemType itemType = null;
                Item item2;
                Item item = item2 = null;
                int n = 0;
                while (iterator.hasNext()) {
                    final Item item3 = (Item)iterator.next();
                    final ItemType \u3007o00\u3007\u3007Oo = item3.\u3007o00\u3007\u3007Oo();
                    ItemType itemType2 = itemType;
                    Item item4 = item;
                    int n2 = n;
                    if (\u3007o00\u3007\u3007Oo != itemType) {
                        if (n != 0) {
                            list.add(new MapItem(itemType, section, item, item2, n));
                        }
                        item4 = item3;
                        itemType2 = \u3007o00\u3007\u3007Oo;
                        n2 = 0;
                    }
                    n = n2 + 1;
                    itemType = itemType2;
                    item = item4;
                    item2 = item3;
                }
                if (n != 0) {
                    list.add(new MapItem(itemType, section, item, item2, n));
                }
                else if (section == mixedItemSection) {
                    list.add(new MapItem(mixedItemSection));
                }
            }
            mixedItemSection.\u3007O00(new UniformListItem<Object>(ItemType.TYPE_MAP_LIST, list));
            return;
        }
        throw new IllegalArgumentException("mapSection.items().size() != 0");
    }
    
    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder(100);
        sb.append(MapItem.class.getName());
        sb.append('{');
        sb.append(this.O8o08O8O.toString());
        sb.append(' ');
        sb.append(this.o\u300700O.toHuman());
        sb.append('}');
        return sb.toString();
    }
    
    @Override
    public void \u3007080(final DexFile dexFile) {
    }
    
    @Override
    protected void \u3007O\u3007(final DexFile dexFile, final AnnotatedOutput annotatedOutput) {
        final int mapValue = this.o\u300700O.getMapValue();
        final Item \u3007080OO8\u30070 = this.\u3007080OO8\u30070;
        int n;
        if (\u3007080OO8\u30070 == null) {
            n = this.O8o08O8O.o\u30070();
        }
        else {
            n = this.O8o08O8O.\u3007o00\u3007\u3007Oo(\u3007080OO8\u30070);
        }
        if (annotatedOutput.\u3007o\u3007()) {
            final StringBuilder sb = new StringBuilder();
            sb.append(this.\u30078o8o\u3007());
            sb.append(' ');
            sb.append(this.o\u300700O.getTypeName());
            sb.append(" map");
            annotatedOutput.oO80(0, sb.toString());
            final StringBuilder sb2 = new StringBuilder();
            sb2.append("  type:   ");
            sb2.append(Hex.Oo08(mapValue));
            sb2.append(" // ");
            sb2.append(this.o\u300700O.toString());
            annotatedOutput.oO80(2, sb2.toString());
            annotatedOutput.oO80(2, "  unused: 0");
            final StringBuilder sb3 = new StringBuilder();
            sb3.append("  size:   ");
            sb3.append(Hex.oO80(this.oOo\u30078o008));
            annotatedOutput.oO80(4, sb3.toString());
            final StringBuilder sb4 = new StringBuilder();
            sb4.append("  offset: ");
            sb4.append(Hex.oO80(n));
            annotatedOutput.oO80(4, sb4.toString());
        }
        annotatedOutput.writeShort(mapValue);
        annotatedOutput.writeShort(0);
        annotatedOutput.writeInt(this.oOo\u30078o008);
        annotatedOutput.writeInt(n);
    }
    
    @Override
    public ItemType \u3007o00\u3007\u3007Oo() {
        return ItemType.TYPE_MAP_ITEM;
    }
    
    @Override
    public final String \u3007\u3007808\u3007() {
        return this.toString();
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.android.dx.dex.file;

import com.android.dx.util.Hex;
import com.android.dx.util.AnnotatedOutput;

public final class AnnotationSetItem extends OffsettedItem
{
    private final AnnotationItem[] o\u300700O;
    
    @Override
    protected void OO0o\u3007\u3007(final Section section, final int n) {
        AnnotationItem.\u30070\u3007O0088o(this.o\u300700O);
    }
    
    @Override
    public int hashCode() {
        throw null;
    }
    
    @Override
    public void \u3007080(final DexFile dexFile) {
        final MixedItemSection oo08 = dexFile.Oo08();
        for (int length = this.o\u300700O.length, i = 0; i < length; ++i) {
            final AnnotationItem[] o\u300700O = this.o\u300700O;
            o\u300700O[i] = oo08.\u3007\u30078O0\u30078(o\u300700O[i]);
        }
    }
    
    @Override
    protected void \u3007O\u3007(final DexFile dexFile, final AnnotatedOutput annotatedOutput) {
        final boolean \u3007o\u3007 = annotatedOutput.\u3007o\u3007();
        final int length = this.o\u300700O.length;
        int i = 0;
        if (\u3007o\u3007) {
            final StringBuilder sb = new StringBuilder();
            sb.append(this.\u30078o8o\u3007());
            sb.append(" annotation set");
            annotatedOutput.oO80(0, sb.toString());
            final StringBuilder sb2 = new StringBuilder();
            sb2.append("  size: ");
            sb2.append(Hex.oO80(length));
            annotatedOutput.oO80(4, sb2.toString());
        }
        annotatedOutput.writeInt(length);
        while (i < length) {
            final int oo80 = this.o\u300700O[i].oO80();
            if (\u3007o\u3007) {
                final StringBuilder sb3 = new StringBuilder();
                sb3.append("  entries[");
                sb3.append(Integer.toHexString(i));
                sb3.append("]: ");
                sb3.append(Hex.oO80(oo80));
                annotatedOutput.oO80(4, sb3.toString());
                this.o\u300700O[i].\u3007\u30078O0\u30078(annotatedOutput, "    ");
            }
            annotatedOutput.writeInt(oo80);
            ++i;
        }
    }
    
    @Override
    public ItemType \u3007o00\u3007\u3007Oo() {
        return ItemType.TYPE_ANNOTATION_SET_ITEM;
    }
    
    @Override
    public String \u3007\u3007808\u3007() {
        throw null;
    }
    
    @Override
    protected int \u3007\u3007888(final OffsettedItem offsettedItem) {
        offsettedItem.getClass();
        throw null;
    }
}

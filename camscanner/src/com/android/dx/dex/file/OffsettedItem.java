// 
// Decompiled by Procyon v0.6.0
// 

package com.android.dx.dex.file;

import com.android.dex.util.ExceptionWithContext;
import com.android.dx.util.AnnotatedOutput;

public abstract class OffsettedItem extends Item implements Comparable<OffsettedItem>
{
    private Section OO;
    private final int o0;
    private int \u300708O\u300700\u3007o;
    private int \u3007OOo8\u30070;
    
    public OffsettedItem(final int o0, final int \u3007oOo8\u30070) {
        Section.OO0o\u3007\u3007(o0);
        if (\u3007oOo8\u30070 >= -1) {
            this.o0 = o0;
            this.\u3007OOo8\u30070 = \u3007oOo8\u30070;
            this.OO = null;
            this.\u300708O\u300700\u3007o = -1;
            return;
        }
        throw new IllegalArgumentException("writeSize < -1");
    }
    
    public static int \u300780\u3007808\u3007O(final OffsettedItem offsettedItem) {
        if (offsettedItem == null) {
            return 0;
        }
        return offsettedItem.oO80();
    }
    
    @Override
    public final int O8() {
        final int \u3007oOo8\u30070 = this.\u3007OOo8\u30070;
        if (\u3007oOo8\u30070 >= 0) {
            return \u3007oOo8\u30070;
        }
        throw new UnsupportedOperationException("writeSize is unknown");
    }
    
    protected void OO0o\u3007\u3007(final Section section, final int n) {
    }
    
    public final int OO0o\u3007\u3007\u3007\u30070() {
        return this.o0;
    }
    
    @Override
    public final void Oo08(final DexFile dexFile, final AnnotatedOutput annotatedOutput) {
        annotatedOutput.Oo08(this.o0);
        try {
            if (this.\u3007OOo8\u30070 >= 0) {
                annotatedOutput.O8(this.oO80());
                this.\u3007O\u3007(dexFile, annotatedOutput);
                return;
            }
            throw new UnsupportedOperationException("writeSize is unknown");
        }
        catch (final RuntimeException ex) {
            final StringBuilder sb = new StringBuilder();
            sb.append("...while writing ");
            sb.append(this);
            throw ExceptionWithContext.withContext(ex, sb.toString());
        }
    }
    
    public final void Oooo8o0\u3007(final int \u3007oOo8\u30070) {
        if (\u3007oOo8\u30070 < 0) {
            throw new IllegalArgumentException("writeSize < 0");
        }
        if (this.\u3007OOo8\u30070 < 0) {
            this.\u3007OOo8\u30070 = \u3007oOo8\u30070;
            return;
        }
        throw new UnsupportedOperationException("writeSize already set");
    }
    
    @Override
    public final boolean equals(final Object o) {
        boolean b = true;
        if (this == o) {
            return true;
        }
        final OffsettedItem offsettedItem = (OffsettedItem)o;
        if (this.\u3007o00\u3007\u3007Oo() != offsettedItem.\u3007o00\u3007\u3007Oo()) {
            return false;
        }
        if (this.\u3007\u3007888(offsettedItem) != 0) {
            b = false;
        }
        return b;
    }
    
    public final int oO80() {
        final int \u300708O\u300700\u3007o = this.\u300708O\u300700\u3007o;
        if (\u300708O\u300700\u3007o >= 0) {
            return this.OO.\u3007o\u3007(\u300708O\u300700\u3007o);
        }
        throw new RuntimeException("offset not yet known");
    }
    
    public final int o\u30070(final OffsettedItem offsettedItem) {
        if (this == offsettedItem) {
            return 0;
        }
        final ItemType \u3007o00\u3007\u3007Oo = this.\u3007o00\u3007\u3007Oo();
        final ItemType \u3007o00\u3007\u3007Oo2 = offsettedItem.\u3007o00\u3007\u3007Oo();
        if (\u3007o00\u3007\u3007Oo != \u3007o00\u3007\u3007Oo2) {
            return \u3007o00\u3007\u3007Oo.compareTo(\u3007o00\u3007\u3007Oo2);
        }
        return this.\u3007\u3007888(offsettedItem);
    }
    
    public final String \u30078o8o\u3007() {
        final StringBuilder sb = new StringBuilder();
        sb.append('[');
        sb.append(Integer.toHexString(this.oO80()));
        sb.append(']');
        return sb.toString();
    }
    
    public final int \u3007O8o08O(final Section oo, int \u300708O\u300700\u3007o) {
        if (oo == null) {
            throw new NullPointerException("addedTo == null");
        }
        if (\u300708O\u300700\u3007o < 0) {
            throw new IllegalArgumentException("offset < 0");
        }
        if (this.OO == null) {
            final int n = this.o0 - 1;
            \u300708O\u300700\u3007o = (\u300708O\u300700\u3007o + n & ~n);
            this.OO0o\u3007\u3007(this.OO = oo, this.\u300708O\u300700\u3007o = \u300708O\u300700\u3007o);
            return \u300708O\u300700\u3007o;
        }
        throw new RuntimeException("already written");
    }
    
    protected abstract void \u3007O\u3007(final DexFile p0, final AnnotatedOutput p1);
    
    public abstract String \u3007\u3007808\u3007();
    
    protected int \u3007\u3007888(final OffsettedItem offsettedItem) {
        throw new UnsupportedOperationException("unsupported");
    }
}

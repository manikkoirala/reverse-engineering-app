// 
// Decompiled by Procyon v0.6.0
// 

package com.android.dx.dex.file;

import com.android.dx.rop.cst.CstProtoRef;
import com.android.dx.rop.cst.Constant;
import java.util.Collection;
import java.util.Iterator;
import com.android.dx.util.Hex;
import com.android.dx.util.AnnotatedOutput;
import com.android.dx.rop.type.Prototype;
import java.util.TreeMap;

public final class ProtoIdsSection extends UniformItemSection
{
    private final TreeMap<Prototype, ProtoIdItem> o\u30070;
    
    public ProtoIdsSection(final DexFile dexFile) {
        super("proto_ids", dexFile, 4);
        this.o\u30070 = new TreeMap<Prototype, ProtoIdItem>();
    }
    
    public ProtoIdItem OoO8(final Prototype prototype) {
        monitorenter(this);
        if (prototype != null) {
            Label_0067: {
                try {
                    this.\u3007O8o08O();
                    ProtoIdItem value;
                    if ((value = this.o\u30070.get(prototype)) == null) {
                        value = new ProtoIdItem(prototype);
                        this.o\u30070.put(prototype, value);
                    }
                    monitorexit(this);
                    return value;
                }
                finally {
                    break Label_0067;
                }
                throw new NullPointerException("prototype == null");
            }
            monitorexit(this);
        }
        throw new NullPointerException("prototype == null");
    }
    
    public void o800o8O(final AnnotatedOutput annotatedOutput) {
        this.\u30078o8o\u3007();
        final int size = this.o\u30070.size();
        int o\u30070;
        if (size == 0) {
            o\u30070 = 0;
        }
        else {
            o\u30070 = this.o\u30070();
        }
        if (size <= 65536) {
            if (annotatedOutput.\u3007o\u3007()) {
                final StringBuilder sb = new StringBuilder();
                sb.append("proto_ids_size:  ");
                sb.append(Hex.oO80(size));
                annotatedOutput.oO80(4, sb.toString());
                final StringBuilder sb2 = new StringBuilder();
                sb2.append("proto_ids_off:   ");
                sb2.append(Hex.oO80(o\u30070));
                annotatedOutput.oO80(4, sb2.toString());
            }
            annotatedOutput.writeInt(size);
            annotatedOutput.writeInt(o\u30070);
            return;
        }
        throw new UnsupportedOperationException("too many proto ids");
    }
    
    public int \u30070\u3007O0088o(final Prototype key) {
        if (key == null) {
            throw new NullPointerException("prototype == null");
        }
        this.\u30078o8o\u3007();
        final ProtoIdItem protoIdItem = this.o\u30070.get(key);
        if (protoIdItem != null) {
            return protoIdItem.o\u30070();
        }
        throw new IllegalArgumentException("not found");
    }
    
    @Override
    protected void \u3007O00() {
        final Iterator<? extends Item> iterator = this.\u3007\u3007888().iterator();
        int n = 0;
        while (iterator.hasNext()) {
            ((ProtoIdItem)iterator.next()).\u300780\u3007808\u3007O(n);
            ++n;
        }
    }
    
    @Override
    public Collection<? extends Item> \u3007\u3007888() {
        return this.o\u30070.values();
    }
    
    public IndexedItem \u3007\u30078O0\u30078(final Constant constant) {
        if (constant == null) {
            throw new NullPointerException("cst == null");
        }
        if (!(constant instanceof CstProtoRef)) {
            throw new IllegalArgumentException("cst not instance of CstProtoRef");
        }
        this.\u30078o8o\u3007();
        final IndexedItem indexedItem = this.o\u30070.get(((CstProtoRef)constant).o\u30070());
        if (indexedItem != null) {
            return indexedItem;
        }
        throw new IllegalArgumentException("not found");
    }
}

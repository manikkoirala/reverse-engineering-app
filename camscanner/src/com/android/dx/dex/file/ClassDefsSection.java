// 
// Decompiled by Procyon v0.6.0
// 

package com.android.dx.dex.file;

import java.util.Collection;
import java.util.Iterator;
import com.android.dx.util.Hex;
import com.android.dx.util.AnnotatedOutput;
import com.android.dx.rop.type.TypeList;
import com.android.dx.rop.cst.CstType;
import java.util.ArrayList;
import com.android.dx.rop.type.Type;
import java.util.TreeMap;

public final class ClassDefsSection extends UniformItemSection
{
    private final TreeMap<Type, ClassDefItem> o\u30070;
    private ArrayList<ClassDefItem> \u3007\u3007888;
    
    public ClassDefsSection(final DexFile dexFile) {
        super("class_defs", dexFile, 4);
        this.o\u30070 = new TreeMap<Type, ClassDefItem>();
        this.\u3007\u3007888 = null;
    }
    
    private int \u30070\u3007O0088o(final Type type, int i, int n) {
        final ClassDefItem e = this.o\u30070.get(type);
        if (e == null || e.\u3007\u3007888()) {
            return i;
        }
        if (n >= 0) {
            final int n2 = n - 1;
            final CstType \u3007\u3007808\u3007 = e.\u3007\u3007808\u3007();
            n = i;
            if (\u3007\u3007808\u3007 != null) {
                n = this.\u30070\u3007O0088o(\u3007\u3007808\u3007.o\u30070(), i, n2);
            }
            final TypeList oooo8o0\u3007 = e.Oooo8o0\u3007();
            int size;
            for (size = oooo8o0\u3007.size(), i = 0; i < size; ++i) {
                n = this.\u30070\u3007O0088o(oooo8o0\u3007.getType(i), n, n2);
            }
            e.\u300780\u3007808\u3007O(n);
            this.\u3007\u3007888.add(e);
            return n + 1;
        }
        final StringBuilder sb = new StringBuilder();
        sb.append("class circularity with ");
        sb.append(type);
        throw new RuntimeException(sb.toString());
    }
    
    public void OoO8(final AnnotatedOutput annotatedOutput) {
        this.\u30078o8o\u3007();
        final int size = this.o\u30070.size();
        int o\u30070;
        if (size == 0) {
            o\u30070 = 0;
        }
        else {
            o\u30070 = this.o\u30070();
        }
        if (annotatedOutput.\u3007o\u3007()) {
            final StringBuilder sb = new StringBuilder();
            sb.append("class_defs_size: ");
            sb.append(Hex.oO80(size));
            annotatedOutput.oO80(4, sb.toString());
            final StringBuilder sb2 = new StringBuilder();
            sb2.append("class_defs_off:  ");
            sb2.append(Hex.oO80(o\u30070));
            annotatedOutput.oO80(4, sb2.toString());
        }
        annotatedOutput.writeInt(size);
        annotatedOutput.writeInt(o\u30070);
    }
    
    @Override
    protected void \u3007O00() {
        final int size = this.o\u30070.size();
        this.\u3007\u3007888 = new ArrayList<ClassDefItem>(size);
        final Iterator<Type> iterator = this.o\u30070.keySet().iterator();
        int \u30070\u3007O0088o = 0;
        while (iterator.hasNext()) {
            \u30070\u3007O0088o = this.\u30070\u3007O0088o(iterator.next(), \u30070\u3007O0088o, size - \u30070\u3007O0088o);
        }
    }
    
    @Override
    public Collection<? extends Item> \u3007\u3007888() {
        final ArrayList<ClassDefItem> \u3007\u3007888 = this.\u3007\u3007888;
        if (\u3007\u3007888 != null) {
            return \u3007\u3007888;
        }
        return this.o\u30070.values();
    }
    
    public void \u3007\u30078O0\u30078(final ClassDefItem value) {
        try {
            final Type o\u30070 = value.\u3007O\u3007().o\u30070();
            this.\u3007O8o08O();
            if (this.o\u30070.get(o\u30070) == null) {
                this.o\u30070.put(o\u30070, value);
                return;
            }
            final StringBuilder sb = new StringBuilder();
            sb.append("already added: ");
            sb.append(o\u30070);
            throw new IllegalArgumentException(sb.toString());
        }
        catch (final NullPointerException ex) {
            throw new NullPointerException("clazz == null");
        }
    }
}

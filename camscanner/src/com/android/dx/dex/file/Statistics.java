// 
// Decompiled by Procyon v0.6.0
// 

package com.android.dx.dex.file;

import java.util.TreeMap;
import com.android.dx.util.AnnotatedOutput;
import java.util.Iterator;
import java.util.HashMap;

public final class Statistics
{
    private final HashMap<String, Data> \u3007080;
    
    public Statistics() {
        this.\u3007080 = new HashMap<String, Data>(50);
    }
    
    public void \u3007080(final Item item) {
        final String \u3007o\u3007 = item.\u3007o\u3007();
        final Data data = this.\u3007080.get(\u3007o\u3007);
        if (data == null) {
            this.\u3007080.put(\u3007o\u3007, new Data(item, \u3007o\u3007));
        }
        else {
            data.\u3007o00\u3007\u3007Oo(item);
        }
    }
    
    public void \u3007o00\u3007\u3007Oo(final Section section) {
        final Iterator<? extends Item> iterator = section.\u3007\u3007888().iterator();
        while (iterator.hasNext()) {
            this.\u3007080((Item)iterator.next());
        }
    }
    
    public final void \u3007o\u3007(final AnnotatedOutput annotatedOutput) {
        if (this.\u3007080.size() == 0) {
            return;
        }
        annotatedOutput.oO80(0, "\nstatistics:\n");
        final TreeMap treeMap = new TreeMap();
        for (final Data value : this.\u3007080.values()) {
            treeMap.put(value.\u3007080, value);
        }
        final Iterator iterator2 = treeMap.values().iterator();
        while (iterator2.hasNext()) {
            ((Data)iterator2.next()).O8(annotatedOutput);
        }
    }
    
    private static class Data
    {
        private int O8;
        private int Oo08;
        private final String \u3007080;
        private int \u3007o00\u3007\u3007Oo;
        private int \u3007o\u3007;
        
        public Data(final Item item, final String \u3007080) {
            final int o8 = item.O8();
            this.\u3007080 = \u3007080;
            this.\u3007o00\u3007\u3007Oo = 1;
            this.\u3007o\u3007 = o8;
            this.O8 = o8;
            this.Oo08 = o8;
        }
        
        public void O8(final AnnotatedOutput annotatedOutput) {
            annotatedOutput.o\u30070(this.\u3007o\u3007());
        }
        
        public void \u3007o00\u3007\u3007Oo(final Item item) {
            final int o8 = item.O8();
            ++this.\u3007o00\u3007\u3007Oo;
            this.\u3007o\u3007 += o8;
            if (o8 > this.O8) {
                this.O8 = o8;
            }
            if (o8 < this.Oo08) {
                this.Oo08 = o8;
            }
        }
        
        public String \u3007o\u3007() {
            final StringBuilder sb = new StringBuilder();
            final StringBuilder sb2 = new StringBuilder();
            sb2.append("  ");
            sb2.append(this.\u3007080);
            sb2.append(": ");
            sb2.append(this.\u3007o00\u3007\u3007Oo);
            sb2.append(" item");
            String str;
            if (this.\u3007o00\u3007\u3007Oo == 1) {
                str = "";
            }
            else {
                str = "s";
            }
            sb2.append(str);
            sb2.append("; ");
            sb2.append(this.\u3007o\u3007);
            sb2.append(" bytes total\n");
            sb.append(sb2.toString());
            if (this.Oo08 == this.O8) {
                final StringBuilder sb3 = new StringBuilder();
                sb3.append("    ");
                sb3.append(this.Oo08);
                sb3.append(" bytes/item\n");
                sb.append(sb3.toString());
            }
            else {
                final int i = this.\u3007o\u3007 / this.\u3007o00\u3007\u3007Oo;
                final StringBuilder sb4 = new StringBuilder();
                sb4.append("    ");
                sb4.append(this.Oo08);
                sb4.append("..");
                sb4.append(this.O8);
                sb4.append(" bytes/item; average ");
                sb4.append(i);
                sb4.append("\n");
                sb.append(sb4.toString());
            }
            return sb.toString();
        }
    }
}

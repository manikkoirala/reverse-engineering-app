// 
// Decompiled by Procyon v0.6.0
// 

package com.android.dx.dex.file;

import com.android.dx.rop.cst.CstType;

public abstract class IdItem extends IndexedItem
{
    private final CstType \u3007OOo8\u30070;
    
    public IdItem(final CstType \u3007oOo8\u30070) {
        if (\u3007oOo8\u30070 != null) {
            this.\u3007OOo8\u30070 = \u3007oOo8\u30070;
            return;
        }
        throw new NullPointerException("type == null");
    }
    
    public final CstType OO0o\u3007\u3007\u3007\u30070() {
        return this.\u3007OOo8\u30070;
    }
    
    @Override
    public void \u3007080(final DexFile dexFile) {
        dexFile.o800o8O().o800o8O(this.\u3007OOo8\u30070);
    }
}

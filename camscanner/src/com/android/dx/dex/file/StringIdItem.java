// 
// Decompiled by Procyon v0.6.0
// 

package com.android.dx.dex.file;

import com.android.dx.rop.cst.Constant;
import com.android.dx.util.Hex;
import com.android.dx.util.AnnotatedOutput;
import com.android.dx.rop.cst.CstString;

public final class StringIdItem extends IndexedItem implements Comparable
{
    private StringDataItem OO;
    private final CstString \u3007OOo8\u30070;
    
    public StringIdItem(final CstString \u3007oOo8\u30070) {
        if (\u3007oOo8\u30070 != null) {
            this.\u3007OOo8\u30070 = \u3007oOo8\u30070;
            this.OO = null;
            return;
        }
        throw new NullPointerException("value == null");
    }
    
    @Override
    public int O8() {
        return 4;
    }
    
    public CstString OO0o\u3007\u3007\u3007\u30070() {
        return this.\u3007OOo8\u30070;
    }
    
    @Override
    public void Oo08(final DexFile dexFile, final AnnotatedOutput annotatedOutput) {
        final int oo80 = this.OO.oO80();
        if (annotatedOutput.\u3007o\u3007()) {
            final StringBuilder sb = new StringBuilder();
            sb.append(this.oO80());
            sb.append(' ');
            sb.append(this.\u3007OOo8\u30070.\u30078o8o\u3007(100));
            annotatedOutput.oO80(0, sb.toString());
            final StringBuilder sb2 = new StringBuilder();
            sb2.append("  string_data_off: ");
            sb2.append(Hex.oO80(oo80));
            annotatedOutput.oO80(4, sb2.toString());
        }
        annotatedOutput.writeInt(oo80);
    }
    
    @Override
    public int compareTo(final Object o) {
        return this.\u3007OOo8\u30070.\u3007o\u3007(((StringIdItem)o).\u3007OOo8\u30070);
    }
    
    @Override
    public boolean equals(final Object o) {
        return o instanceof StringIdItem && this.\u3007OOo8\u30070.equals(((StringIdItem)o).\u3007OOo8\u30070);
    }
    
    @Override
    public int hashCode() {
        return this.\u3007OOo8\u30070.hashCode();
    }
    
    @Override
    public void \u3007080(final DexFile dexFile) {
        if (this.OO == null) {
            dexFile.\u30070\u3007O0088o().\u3007O00(this.OO = new StringDataItem(this.\u3007OOo8\u30070));
        }
    }
    
    @Override
    public ItemType \u3007o00\u3007\u3007Oo() {
        return ItemType.TYPE_STRING_ID_ITEM;
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.android.dx.dex.file;

import java.util.Collection;
import java.util.Iterator;
import com.android.dx.rop.cst.Constant;
import com.android.dx.rop.cst.CstCallSite;
import com.android.dx.rop.cst.CstCallSiteRef;
import java.util.TreeMap;

public final class CallSiteIdsSection extends UniformItemSection
{
    private final TreeMap<CstCallSiteRef, CallSiteIdItem> o\u30070;
    private final TreeMap<CstCallSite, CallSiteItem> \u3007\u3007888;
    
    public CallSiteIdsSection(final DexFile dexFile) {
        super("call_site_ids", dexFile, 4);
        this.o\u30070 = new TreeMap<CstCallSiteRef, CallSiteIdItem>();
        this.\u3007\u3007888 = new TreeMap<CstCallSite, CallSiteItem>();
    }
    
    CallSiteItem OoO8(final CstCallSite cstCallSite) {
        throw new NullPointerException("callSite == null");
    }
    
    public IndexedItem \u30070\u3007O0088o(final Constant constant) {
        if (constant == null) {
            throw new NullPointerException("cst == null");
        }
        this.\u30078o8o\u3007();
        final IndexedItem indexedItem = this.o\u30070.get(constant);
        if (indexedItem != null) {
            return indexedItem;
        }
        throw new IllegalArgumentException("not found");
    }
    
    @Override
    protected void \u3007O00() {
        final Iterator<CallSiteIdItem> iterator = this.o\u30070.values().iterator();
        int n = 0;
        while (iterator.hasNext()) {
            iterator.next().\u300780\u3007808\u3007O(n);
            ++n;
        }
    }
    
    @Override
    public Collection<? extends Item> \u3007\u3007888() {
        return this.o\u30070.values();
    }
    
    void \u3007\u30078O0\u30078(final CstCallSite cstCallSite, final CallSiteItem callSiteItem) {
        throw new NullPointerException("callSite == null");
    }
}

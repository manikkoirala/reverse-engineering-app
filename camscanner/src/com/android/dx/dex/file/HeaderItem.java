// 
// Decompiled by Procyon v0.6.0
// 

package com.android.dx.dex.file;

import com.android.dx.util.Hex;
import com.android.dx.rop.cst.CstString;
import com.android.dx.util.AnnotatedOutput;

public final class HeaderItem extends IndexedItem
{
    @Override
    public int O8() {
        return 112;
    }
    
    @Override
    public void Oo08(final DexFile dexFile, final AnnotatedOutput annotatedOutput) {
        final int o\u30070 = dexFile.Oooo8o0\u3007().o\u30070();
        final Section \u3007o8o08O = dexFile.\u3007O8o08O();
        final Section oo0o\u3007\u3007 = dexFile.OO0o\u3007\u3007();
        final int o\u30072 = \u3007o8o08O.o\u30070();
        final int n = oo0o\u3007\u3007.o\u30070() + oo0o\u3007\u3007.Oooo8o0\u3007() - o\u30072;
        final String \u3007o00\u3007\u3007Oo = dexFile.\u300780\u3007808\u3007O().\u3007o00\u3007\u3007Oo();
        if (annotatedOutput.\u3007o\u3007()) {
            final StringBuilder sb = new StringBuilder();
            sb.append("magic: ");
            sb.append(new CstString(\u3007o00\u3007\u3007Oo).OO0o\u3007\u3007\u3007\u30070());
            annotatedOutput.oO80(8, sb.toString());
            annotatedOutput.oO80(4, "checksum");
            annotatedOutput.oO80(20, "signature");
            final StringBuilder sb2 = new StringBuilder();
            sb2.append("file_size:       ");
            sb2.append(Hex.oO80(dexFile.\u30078o8o\u3007()));
            annotatedOutput.oO80(4, sb2.toString());
            final StringBuilder sb3 = new StringBuilder();
            sb3.append("header_size:     ");
            sb3.append(Hex.oO80(112));
            annotatedOutput.oO80(4, sb3.toString());
            final StringBuilder sb4 = new StringBuilder();
            sb4.append("endian_tag:      ");
            sb4.append(Hex.oO80(305419896));
            annotatedOutput.oO80(4, sb4.toString());
            annotatedOutput.oO80(4, "link_size:       0");
            annotatedOutput.oO80(4, "link_off:        0");
            final StringBuilder sb5 = new StringBuilder();
            sb5.append("map_off:         ");
            sb5.append(Hex.oO80(o\u30070));
            annotatedOutput.oO80(4, sb5.toString());
        }
        for (int i = 0; i < 8; ++i) {
            annotatedOutput.writeByte(\u3007o00\u3007\u3007Oo.charAt(i));
        }
        annotatedOutput.\u300780\u3007808\u3007O(24);
        annotatedOutput.writeInt(dexFile.\u30078o8o\u3007());
        annotatedOutput.writeInt(112);
        annotatedOutput.writeInt(305419896);
        annotatedOutput.\u300780\u3007808\u3007O(8);
        annotatedOutput.writeInt(o\u30070);
        dexFile.OoO8().\u3007O888o0o(annotatedOutput);
        dexFile.o800o8O().oo88o8O(annotatedOutput);
        dexFile.\u3007O00().o800o8O(annotatedOutput);
        dexFile.OO0o\u3007\u3007\u3007\u30070().\u3007O888o0o(annotatedOutput);
        dexFile.\u3007O\u3007().\u3007O888o0o(annotatedOutput);
        dexFile.oO80().OoO8(annotatedOutput);
        if (annotatedOutput.\u3007o\u3007()) {
            final StringBuilder sb6 = new StringBuilder();
            sb6.append("data_size:       ");
            sb6.append(Hex.oO80(n));
            annotatedOutput.oO80(4, sb6.toString());
            final StringBuilder sb7 = new StringBuilder();
            sb7.append("data_off:        ");
            sb7.append(Hex.oO80(o\u30072));
            annotatedOutput.oO80(4, sb7.toString());
        }
        annotatedOutput.writeInt(n);
        annotatedOutput.writeInt(o\u30072);
    }
    
    @Override
    public void \u3007080(final DexFile dexFile) {
    }
    
    @Override
    public ItemType \u3007o00\u3007\u3007Oo() {
        return ItemType.TYPE_HEADER_ITEM;
    }
}

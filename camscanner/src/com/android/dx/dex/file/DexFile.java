// 
// Decompiled by Procyon v0.6.0
// 

package com.android.dx.dex.file;

import java.io.IOException;
import java.io.Writer;
import com.android.dx.rop.cst.CstCallSiteRef;
import com.android.dx.rop.cst.CstMethodHandle;
import com.android.dx.rop.cst.CstProtoRef;
import com.android.dx.rop.cst.CstEnumRef;
import com.android.dx.rop.cst.CstFieldRef;
import com.android.dx.rop.cst.CstBaseMethodRef;
import com.android.dx.rop.cst.CstType;
import com.android.dx.rop.cst.CstString;
import com.android.dx.rop.cst.Constant;
import java.security.NoSuchAlgorithmException;
import java.security.DigestException;
import java.security.MessageDigest;
import java.util.zip.Adler32;
import com.android.dx.util.AnnotatedOutput;
import com.android.dex.util.ExceptionWithContext;
import com.android.dx.util.ByteArrayAnnotatedOutput;
import com.android.dx.dex.DexOptions;

public final class DexFile
{
    private final MixedItemSection O8;
    private final CallSiteIdsSection OO0o\u3007\u3007;
    private final MethodIdsSection OO0o\u3007\u3007\u3007\u30070;
    private final MixedItemSection Oo08;
    private final MethodHandlesSection Oooo8o0\u3007;
    private final ProtoIdsSection oO80;
    private final StringIdsSection o\u30070;
    private final DexOptions \u3007080;
    private int \u30070\u3007O0088o;
    private final FieldIdsSection \u300780\u3007808\u3007O;
    private final ClassDefsSection \u30078o8o\u3007;
    private final Section[] \u3007O00;
    private final MixedItemSection \u3007O8o08O;
    private final HeaderSection \u3007O\u3007;
    private final MixedItemSection \u3007o00\u3007\u3007Oo;
    private final MixedItemSection \u3007o\u3007;
    private final MixedItemSection \u3007\u3007808\u3007;
    private final TypeIdsSection \u3007\u3007888;
    private int \u3007\u30078O0\u30078;
    
    public DexFile(final DexOptions \u3007080) {
        this.\u3007080 = \u3007080;
        final HeaderSection \u3007o\u3007 = new HeaderSection(this);
        this.\u3007O\u3007 = \u3007o\u3007;
        final MixedItemSection.SortType o0 = MixedItemSection.SortType.o0;
        final MixedItemSection \u3007o\u30072 = new MixedItemSection(null, this, 4, o0);
        this.\u3007o\u3007 = \u3007o\u30072;
        final MixedItemSection.SortType \u3007oOo8\u30070 = MixedItemSection.SortType.\u3007OOo8\u30070;
        final MixedItemSection \u3007o00\u3007\u3007Oo = new MixedItemSection("word_data", this, 4, \u3007oOo8\u30070);
        this.\u3007o00\u3007\u3007Oo = \u3007o00\u3007\u3007Oo;
        final MixedItemSection oo08 = new MixedItemSection("string_data", this, 1, MixedItemSection.SortType.OO);
        this.Oo08 = oo08;
        final MixedItemSection \u3007o8o08O = new MixedItemSection(null, this, 1, o0);
        this.\u3007O8o08O = \u3007o8o08O;
        final MixedItemSection \u3007\u3007808\u3007 = new MixedItemSection("byte_data", this, 1, \u3007oOo8\u30070);
        this.\u3007\u3007808\u3007 = \u3007\u3007808\u3007;
        final StringIdsSection o\u30070 = new StringIdsSection(this);
        this.o\u30070 = o\u30070;
        final TypeIdsSection \u3007\u3007888 = new TypeIdsSection(this);
        this.\u3007\u3007888 = \u3007\u3007888;
        final ProtoIdsSection oo9 = new ProtoIdsSection(this);
        this.oO80 = oo9;
        final FieldIdsSection \u300780\u3007808\u3007O = new FieldIdsSection(this);
        this.\u300780\u3007808\u3007O = \u300780\u3007808\u3007O;
        final MethodIdsSection oo0o\u3007\u3007\u3007\u30070 = new MethodIdsSection(this);
        this.OO0o\u3007\u3007\u3007\u30070 = oo0o\u3007\u3007\u3007\u30070;
        final ClassDefsSection \u30078o8o\u3007 = new ClassDefsSection(this);
        this.\u30078o8o\u3007 = \u30078o8o\u3007;
        final MixedItemSection o2 = new MixedItemSection("map", this, 4, o0);
        this.O8 = o2;
        if (\u3007080.\u3007080(26)) {
            final CallSiteIdsSection oo0o\u3007\u3007 = new CallSiteIdsSection(this);
            this.OO0o\u3007\u3007 = oo0o\u3007\u3007;
            final MethodHandlesSection oooo8o0\u3007 = new MethodHandlesSection(this);
            this.Oooo8o0\u3007 = oooo8o0\u3007;
            this.\u3007O00 = new Section[] { \u3007o\u3007, o\u30070, \u3007\u3007888, oo9, \u300780\u3007808\u3007O, oo0o\u3007\u3007\u3007\u30070, \u30078o8o\u3007, oo0o\u3007\u3007, oooo8o0\u3007, \u3007o00\u3007\u3007Oo, \u3007o\u30072, oo08, \u3007\u3007808\u3007, \u3007o8o08O, o2 };
        }
        else {
            this.OO0o\u3007\u3007 = null;
            this.Oooo8o0\u3007 = null;
            this.\u3007O00 = new Section[] { \u3007o\u3007, o\u30070, \u3007\u3007888, oo9, \u300780\u3007808\u3007O, oo0o\u3007\u3007\u3007\u30070, \u30078o8o\u3007, \u3007o00\u3007\u3007Oo, \u3007o\u30072, oo08, \u3007\u3007808\u3007, \u3007o8o08O, o2 };
        }
        this.\u3007\u30078O0\u30078 = -1;
        this.\u30070\u3007O0088o = 79;
    }
    
    private ByteArrayAnnotatedOutput \u300700(final boolean b, final boolean b2, final Storage storage) {
        this.\u30078o8o\u3007.oO80();
        this.\u3007O8o08O.oO80();
        this.\u3007o00\u3007\u3007Oo.oO80();
        if (this.\u3007080.\u3007080(26)) {
            this.OO0o\u3007\u3007.oO80();
        }
        this.\u3007\u3007808\u3007.oO80();
        if (this.\u3007080.\u3007080(26)) {
            this.Oooo8o0\u3007.oO80();
        }
        this.OO0o\u3007\u3007\u3007\u30070.oO80();
        this.\u300780\u3007808\u3007O.oO80();
        this.oO80.oO80();
        this.\u3007o\u3007.oO80();
        this.\u3007\u3007888.oO80();
        this.o\u30070.oO80();
        this.Oo08.oO80();
        this.\u3007O\u3007.oO80();
        final int length = this.\u3007O00.length;
        final int n = 0;
        int i = 0;
        int \u3007\u30078O0\u30078 = 0;
        while (i < length) {
            final Section section = this.\u3007O00[i];
            Label_0260: {
                if ((section == this.OO0o\u3007\u3007 || section == this.Oooo8o0\u3007) && section.\u3007\u3007888().isEmpty()) {
                    break Label_0260;
                }
                final int oo0o\u3007\u3007\u3007\u30070 = section.OO0o\u3007\u3007\u3007\u30070(\u3007\u30078O0\u30078);
                if (oo0o\u3007\u3007\u3007\u30070 < \u3007\u30078O0\u30078) {
                    break Label_0260;
                }
                try {
                    final MixedItemSection o8 = this.O8;
                    if (section == o8) {
                        MapItem.\u3007O00(this.\u3007O00, o8);
                        this.O8.oO80();
                    }
                    if (section instanceof MixedItemSection) {
                        ((MixedItemSection)section).\u30070\u3007O0088o();
                    }
                    \u3007\u30078O0\u30078 = section.Oooo8o0\u3007() + oo0o\u3007\u3007\u3007\u30070;
                    ++i;
                    continue;
                }
                catch (final RuntimeException ex) {
                    final StringBuilder sb = new StringBuilder();
                    sb.append("...while writing section ");
                    sb.append(i);
                    throw ExceptionWithContext.withContext(ex, sb.toString());
                }
            }
            final StringBuilder sb2 = new StringBuilder();
            sb2.append("bogus placement for section ");
            sb2.append(i);
            throw new RuntimeException(sb2.toString());
        }
        this.\u3007\u30078O0\u30078 = \u3007\u30078O0\u30078;
        final byte[] array = new byte[\u3007\u30078O0\u30078];
        final ByteArrayAnnotatedOutput byteArrayAnnotatedOutput = new ByteArrayAnnotatedOutput(array);
        int j = n;
        if (b) {
            byteArrayAnnotatedOutput.\u3007O8o08O(this.\u30070\u3007O0088o, b2);
            j = n;
        }
        while (j < length) {
            try {
                final Section section2 = this.\u3007O00[j];
                if ((section2 != this.OO0o\u3007\u3007 && section2 != this.Oooo8o0\u3007) || !section2.\u3007\u3007888().isEmpty()) {
                    final int n2 = section2.o\u30070() - byteArrayAnnotatedOutput.getCursor();
                    if (n2 < 0) {
                        final StringBuilder sb3 = new StringBuilder();
                        sb3.append("excess write of ");
                        sb3.append(-n2);
                        throw new ExceptionWithContext(sb3.toString());
                    }
                    byteArrayAnnotatedOutput.\u300780\u3007808\u3007O(n2);
                    section2.\u3007\u3007808\u3007(byteArrayAnnotatedOutput);
                }
                ++j;
                continue;
            }
            catch (final RuntimeException ex2) {
                ExceptionWithContext exceptionWithContext;
                if (ex2 instanceof ExceptionWithContext) {
                    exceptionWithContext = (ExceptionWithContext)ex2;
                }
                else {
                    exceptionWithContext = new ExceptionWithContext(ex2);
                }
                final StringBuilder sb4 = new StringBuilder();
                sb4.append("...while writing section ");
                sb4.append(j);
                exceptionWithContext.addContext(sb4.toString());
                throw exceptionWithContext;
            }
            break;
        }
        if (byteArrayAnnotatedOutput.getCursor() == this.\u3007\u30078O0\u30078) {
            \u3007o\u3007(array, byteArrayAnnotatedOutput.getCursor());
            \u3007o00\u3007\u3007Oo(array, byteArrayAnnotatedOutput.getCursor());
            if (b) {
                this.\u3007o00\u3007\u3007Oo.OoO8(byteArrayAnnotatedOutput, ItemType.TYPE_CODE_ITEM, "\nmethod code index:\n\n");
                this.\u3007\u30078O0\u30078().\u3007o\u3007(byteArrayAnnotatedOutput);
                byteArrayAnnotatedOutput.Oooo8o0\u3007();
            }
            return byteArrayAnnotatedOutput;
        }
        throw new RuntimeException("foreshortened write");
    }
    
    private static void \u3007o00\u3007\u3007Oo(final byte[] b, int n) {
        final Adler32 adler32 = new Adler32();
        adler32.update(b, 12, n - 12);
        n = (int)adler32.getValue();
        b[8] = (byte)n;
        b[9] = (byte)(n >> 8);
        b[10] = (byte)(n >> 16);
        b[11] = (byte)(n >> 24);
    }
    
    private static void \u3007o\u3007(final byte[] array, int digest) {
        try {
            final MessageDigest instance = MessageDigest.getInstance("SHA-1");
            instance.update(array, 32, digest - 32);
            try {
                digest = instance.digest(array, 12, 20);
                if (digest == 20) {
                    return;
                }
                final StringBuilder sb = new StringBuilder();
                sb.append("unexpected digest write: ");
                sb.append(digest);
                sb.append(" bytes");
                throw new RuntimeException(sb.toString());
            }
            catch (final DigestException cause) {
                throw new RuntimeException(cause);
            }
        }
        catch (final NoSuchAlgorithmException cause2) {
            throw new RuntimeException(cause2);
        }
    }
    
    IndexedItem O8(final Constant constant) {
        if (constant instanceof CstString) {
            return this.o\u30070.\u3007\u30078O0\u30078(constant);
        }
        if (constant instanceof CstType) {
            return this.\u3007\u3007888.\u3007\u30078O0\u30078(constant);
        }
        if (constant instanceof CstBaseMethodRef) {
            return this.OO0o\u3007\u3007\u3007\u30070.\u30070\u3007O0088o(constant);
        }
        if (constant instanceof CstFieldRef) {
            return this.\u300780\u3007808\u3007O.\u30070\u3007O0088o(constant);
        }
        if (constant instanceof CstEnumRef) {
            return this.\u300780\u3007808\u3007O.o800o8O(((CstEnumRef)constant).oO80());
        }
        if (constant instanceof CstProtoRef) {
            return this.oO80.\u3007\u30078O0\u30078(constant);
        }
        if (constant instanceof CstMethodHandle) {
            return this.Oooo8o0\u3007.\u3007\u30078O0\u30078(constant);
        }
        if (constant instanceof CstCallSiteRef) {
            return this.OO0o\u3007\u3007.\u30070\u3007O0088o(constant);
        }
        return null;
    }
    
    Section OO0o\u3007\u3007() {
        return this.O8;
    }
    
    public FieldIdsSection OO0o\u3007\u3007\u3007\u30070() {
        return this.\u300780\u3007808\u3007O;
    }
    
    MixedItemSection Oo08() {
        return this.\u3007\u3007808\u3007;
    }
    
    StringIdsSection OoO8() {
        return this.o\u30070;
    }
    
    MixedItemSection Oooo8o0\u3007() {
        return this.O8;
    }
    
    public TypeIdsSection o800o8O() {
        return this.\u3007\u3007888;
    }
    
    public ClassDefsSection oO80() {
        return this.\u30078o8o\u3007;
    }
    
    MixedItemSection oo88o8O() {
        return this.\u3007o00\u3007\u3007Oo;
    }
    
    public CallSiteIdsSection o\u30070() {
        return this.OO0o\u3007\u3007;
    }
    
    public byte[] o\u3007O8\u3007\u3007o(final Writer writer, final boolean b) throws IOException {
        final boolean b2 = writer != null;
        final ByteArrayAnnotatedOutput \u300700 = this.\u300700(b2, b, null);
        if (b2) {
            \u300700.\u30070\u3007O0088o(writer);
        }
        return \u300700.\u3007\u3007808\u3007();
    }
    
    public void \u3007080(final ClassDefItem classDefItem) {
        this.\u30078o8o\u3007.\u3007\u30078O0\u30078(classDefItem);
    }
    
    MixedItemSection \u30070\u3007O0088o() {
        return this.Oo08;
    }
    
    public DexOptions \u300780\u3007808\u3007O() {
        return this.\u3007080;
    }
    
    public int \u30078o8o\u3007() {
        final int \u3007\u30078O0\u30078 = this.\u3007\u30078O0\u30078;
        if (\u3007\u30078O0\u30078 >= 0) {
            return \u3007\u30078O0\u30078;
        }
        throw new RuntimeException("file size not yet known");
    }
    
    ProtoIdsSection \u3007O00() {
        return this.oO80;
    }
    
    MixedItemSection \u3007O888o0o() {
        return this.\u3007o\u3007;
    }
    
    Section \u3007O8o08O() {
        return this.\u3007o00\u3007\u3007Oo;
    }
    
    public MethodIdsSection \u3007O\u3007() {
        return this.OO0o\u3007\u3007\u3007\u30070;
    }
    
    void \u3007oo\u3007(final Constant constant) {
        if (constant != null) {
            if (constant instanceof CstString) {
                this.o\u30070.o800o8O((CstString)constant);
            }
            else if (constant instanceof CstType) {
                this.\u3007\u3007888.o800o8O((CstType)constant);
            }
            else if (constant instanceof CstBaseMethodRef) {
                this.OO0o\u3007\u3007\u3007\u30070.o800o8O((CstBaseMethodRef)constant);
            }
            else if (constant instanceof CstFieldRef) {
                this.\u300780\u3007808\u3007O.o800o8O((CstFieldRef)constant);
            }
            else if (constant instanceof CstEnumRef) {
                this.\u300780\u3007808\u3007O.o800o8O(((CstEnumRef)constant).oO80());
            }
            else if (constant instanceof CstProtoRef) {
                this.oO80.OoO8(((CstProtoRef)constant).o\u30070());
            }
            else if (constant instanceof CstMethodHandle) {
                this.Oooo8o0\u3007.OoO8((CstMethodHandle)constant);
            }
            return;
        }
        throw new NullPointerException("cst == null");
    }
    
    public MethodHandlesSection \u3007\u3007808\u3007() {
        return this.Oooo8o0\u3007;
    }
    
    MixedItemSection \u3007\u3007888() {
        return this.\u3007O8o08O;
    }
    
    public Statistics \u3007\u30078O0\u30078() {
        final Statistics statistics = new Statistics();
        final Section[] \u3007o00 = this.\u3007O00;
        for (int length = \u3007o00.length, i = 0; i < length; ++i) {
            statistics.\u3007o00\u3007\u3007Oo(\u3007o00[i]);
        }
        return statistics;
    }
    
    public static final class Storage
    {
    }
}

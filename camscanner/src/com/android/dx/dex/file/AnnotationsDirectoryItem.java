// 
// Decompiled by Procyon v0.6.0
// 

package com.android.dx.dex.file;

import java.util.List;
import java.util.Collections;
import com.android.dx.util.Hex;
import com.android.dx.util.AnnotatedOutput;
import java.util.Iterator;
import java.util.ArrayList;

public final class AnnotationsDirectoryItem extends OffsettedItem
{
    private ArrayList<FieldAnnotationStruct> O8o08O8O;
    private AnnotationSetItem o\u300700O;
    private ArrayList<MethodAnnotationStruct> \u3007080OO8\u30070;
    private ArrayList<ParameterAnnotationStruct> \u30070O;
    
    public AnnotationsDirectoryItem() {
        super(4, -1);
        this.o\u300700O = null;
        this.O8o08O8O = null;
        this.\u3007080OO8\u30070 = null;
        this.\u30070O = null;
    }
    
    private static int \u30070\u3007O0088o(final ArrayList<?> list) {
        if (list == null) {
            return 0;
        }
        return list.size();
    }
    
    @Override
    protected void OO0o\u3007\u3007(final Section section, final int n) {
        this.Oooo8o0\u3007((\u30070\u3007O0088o(this.O8o08O8O) + \u30070\u3007O0088o(this.\u3007080OO8\u30070) + \u30070\u3007O0088o(this.\u30070O)) * 8 + 16);
    }
    
    @Override
    public int hashCode() {
        final AnnotationSetItem o\u300700O = this.o\u300700O;
        if (o\u300700O == null) {
            return 0;
        }
        return o\u300700O.hashCode();
    }
    
    @Override
    public void \u3007080(final DexFile dexFile) {
        final MixedItemSection oo88o8O = dexFile.oo88o8O();
        final AnnotationSetItem o\u300700O = this.o\u300700O;
        if (o\u300700O != null) {
            this.o\u300700O = oo88o8O.\u3007\u30078O0\u30078(o\u300700O);
        }
        final ArrayList<FieldAnnotationStruct> o8o08O8O = this.O8o08O8O;
        if (o8o08O8O != null) {
            final Iterator<FieldAnnotationStruct> iterator = o8o08O8O.iterator();
            while (iterator.hasNext()) {
                iterator.next().\u3007o\u3007(dexFile);
            }
        }
        final ArrayList<MethodAnnotationStruct> \u3007080OO8\u30070 = this.\u3007080OO8\u30070;
        if (\u3007080OO8\u30070 != null) {
            final Iterator<MethodAnnotationStruct> iterator2 = \u3007080OO8\u30070.iterator();
            while (iterator2.hasNext()) {
                iterator2.next().\u3007o\u3007(dexFile);
            }
        }
        final ArrayList<ParameterAnnotationStruct> \u30070O = this.\u30070O;
        if (\u30070O != null) {
            final Iterator<ParameterAnnotationStruct> iterator3 = \u30070O.iterator();
            while (iterator3.hasNext()) {
                iterator3.next().\u3007o\u3007(dexFile);
            }
        }
    }
    
    public boolean \u3007O00() {
        return this.o\u300700O == null && this.O8o08O8O == null && this.\u3007080OO8\u30070 == null && this.\u30070O == null;
    }
    
    @Override
    protected void \u3007O\u3007(final DexFile dexFile, final AnnotatedOutput annotatedOutput) {
        final boolean \u3007o\u3007 = annotatedOutput.\u3007o\u3007();
        final int \u300780\u3007808\u3007O = OffsettedItem.\u300780\u3007808\u3007O(this.o\u300700O);
        final int \u30070\u3007O0088o = \u30070\u3007O0088o(this.O8o08O8O);
        final int \u30070\u3007O0088o2 = \u30070\u3007O0088o(this.\u3007080OO8\u30070);
        final int \u30070\u3007O0088o3 = \u30070\u3007O0088o(this.\u30070O);
        if (\u3007o\u3007) {
            final StringBuilder sb = new StringBuilder();
            sb.append(this.\u30078o8o\u3007());
            sb.append(" annotations directory");
            annotatedOutput.oO80(0, sb.toString());
            final StringBuilder sb2 = new StringBuilder();
            sb2.append("  class_annotations_off: ");
            sb2.append(Hex.oO80(\u300780\u3007808\u3007O));
            annotatedOutput.oO80(4, sb2.toString());
            final StringBuilder sb3 = new StringBuilder();
            sb3.append("  fields_size:           ");
            sb3.append(Hex.oO80(\u30070\u3007O0088o));
            annotatedOutput.oO80(4, sb3.toString());
            final StringBuilder sb4 = new StringBuilder();
            sb4.append("  methods_size:          ");
            sb4.append(Hex.oO80(\u30070\u3007O0088o2));
            annotatedOutput.oO80(4, sb4.toString());
            final StringBuilder sb5 = new StringBuilder();
            sb5.append("  parameters_size:       ");
            sb5.append(Hex.oO80(\u30070\u3007O0088o3));
            annotatedOutput.oO80(4, sb5.toString());
        }
        annotatedOutput.writeInt(\u300780\u3007808\u3007O);
        annotatedOutput.writeInt(\u30070\u3007O0088o);
        annotatedOutput.writeInt(\u30070\u3007O0088o2);
        annotatedOutput.writeInt(\u30070\u3007O0088o3);
        if (\u30070\u3007O0088o != 0) {
            Collections.sort(this.O8o08O8O);
            if (\u3007o\u3007) {
                annotatedOutput.oO80(0, "  fields:");
            }
            final Iterator<FieldAnnotationStruct> iterator = this.O8o08O8O.iterator();
            while (iterator.hasNext()) {
                iterator.next().Oo08(dexFile, annotatedOutput);
            }
        }
        if (\u30070\u3007O0088o2 != 0) {
            Collections.sort(this.\u3007080OO8\u30070);
            if (\u3007o\u3007) {
                annotatedOutput.oO80(0, "  methods:");
            }
            final Iterator<MethodAnnotationStruct> iterator2 = this.\u3007080OO8\u30070.iterator();
            while (iterator2.hasNext()) {
                iterator2.next().Oo08(dexFile, annotatedOutput);
            }
        }
        if (\u30070\u3007O0088o3 != 0) {
            Collections.sort(this.\u30070O);
            if (\u3007o\u3007) {
                annotatedOutput.oO80(0, "  parameters:");
            }
            final Iterator<ParameterAnnotationStruct> iterator3 = this.\u30070O.iterator();
            while (iterator3.hasNext()) {
                iterator3.next().Oo08(dexFile, annotatedOutput);
            }
        }
    }
    
    @Override
    public ItemType \u3007o00\u3007\u3007Oo() {
        return ItemType.TYPE_ANNOTATIONS_DIRECTORY_ITEM;
    }
    
    @Override
    public String \u3007\u3007808\u3007() {
        throw new RuntimeException("unsupported");
    }
    
    public int \u3007\u3007888(final OffsettedItem offsettedItem) {
        if (this.\u3007\u30078O0\u30078()) {
            return this.o\u300700O.o\u30070(((AnnotationsDirectoryItem)offsettedItem).o\u300700O);
        }
        throw new UnsupportedOperationException("uninternable instance");
    }
    
    public boolean \u3007\u30078O0\u30078() {
        return this.o\u300700O != null && this.O8o08O8O == null && this.\u3007080OO8\u30070 == null && this.\u30070O == null;
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.android.dx.dex.file;

import com.android.dex.util.ExceptionWithContext;
import java.util.List;
import java.util.Collections;
import java.util.Comparator;
import java.util.BitSet;
import com.android.dx.rop.cst.CstType;
import com.android.dx.rop.type.Type;
import java.util.Iterator;
import com.android.dx.rop.type.StdTypeList;
import com.android.dx.rop.cst.CstString;
import java.util.ArrayList;
import java.io.IOException;
import com.android.dx.rop.cst.CstMethodRef;
import com.android.dx.rop.type.Prototype;
import com.android.dx.util.ByteArrayAnnotatedOutput;
import com.android.dx.dex.code.LocalList;
import java.io.PrintWriter;
import com.android.dx.util.AnnotatedOutput;
import com.android.dx.dex.code.PositionList;

public final class DebugInfoEncoder
{
    private final DexFile O8;
    private String OO0o\u3007\u3007;
    private int OO0o\u3007\u3007\u3007\u30070;
    private final int Oo08;
    private boolean Oooo8o0\u3007;
    private final boolean oO80;
    private final int o\u30070;
    private final PositionList \u3007080;
    private int \u300780\u3007808\u3007O;
    private AnnotatedOutput \u30078o8o\u3007;
    private PrintWriter \u3007O8o08O;
    private final LocalList \u3007o00\u3007\u3007Oo;
    private final ByteArrayAnnotatedOutput \u3007o\u3007;
    private final LocalList.Entry[] \u3007\u3007808\u3007;
    private final Prototype \u3007\u3007888;
    
    public DebugInfoEncoder(final PositionList \u3007080, final LocalList \u3007o00\u3007\u3007Oo, final DexFile o8, final int oo08, final int o\u30070, final boolean oo9, final CstMethodRef cstMethodRef) {
        this.\u300780\u3007808\u3007O = 0;
        this.OO0o\u3007\u3007\u3007\u30070 = 1;
        this.\u3007080 = \u3007080;
        this.\u3007o00\u3007\u3007Oo = \u3007o00\u3007\u3007Oo;
        this.O8 = o8;
        this.\u3007\u3007888 = cstMethodRef.\u300780\u3007808\u3007O();
        this.oO80 = oo9;
        this.Oo08 = oo08;
        this.o\u30070 = o\u30070;
        this.\u3007o\u3007 = new ByteArrayAnnotatedOutput();
        this.\u3007\u3007808\u3007 = new LocalList.Entry[o\u30070];
    }
    
    private void OO0o\u3007\u3007(final LocalList.Entry entry) throws IOException {
        if (entry.\u3007\u3007888() != null) {
            this.Oooo8o0\u3007(entry);
            return;
        }
        final int cursor = this.\u3007o\u3007.getCursor();
        this.\u3007o\u3007.writeByte(3);
        this.OoO8(entry.Oo08());
        this.\u3007\u30078O0\u30078(entry.O8());
        this.\u30070\u3007O0088o(entry.getType());
        if (this.\u30078o8o\u3007 != null || this.\u3007O8o08O != null) {
            this.\u3007080(this.\u3007o\u3007.getCursor() - cursor, String.format("%04x: +local %s", this.\u300780\u3007808\u3007O, this.o800o8O(entry)));
        }
    }
    
    private void OO0o\u3007\u3007\u3007\u30070(final ArrayList<PositionList.Entry> list, final ArrayList<LocalList.Entry> list2) throws IOException {
        final AnnotatedOutput \u30078o8o\u3007 = this.\u30078o8o\u3007;
        final int n = 0;
        final boolean b = \u30078o8o\u3007 != null || this.\u3007O8o08O != null;
        final int cursor = this.\u3007o\u3007.getCursor();
        if (list.size() > 0) {
            this.OO0o\u3007\u3007\u3007\u30070 = ((PositionList.Entry)list.get(0)).\u3007o00\u3007\u3007Oo().\u3007080();
        }
        this.\u3007o\u3007.\u3007080(this.OO0o\u3007\u3007\u3007\u30070);
        if (b) {
            final int cursor2 = this.\u3007o\u3007.getCursor();
            final StringBuilder sb = new StringBuilder();
            sb.append("line_start: ");
            sb.append(this.OO0o\u3007\u3007\u3007\u30070);
            this.\u3007080(cursor2 - cursor, sb.toString());
        }
        final int oo88o8O = this.oo88o8O();
        final StdTypeList oo08 = this.\u3007\u3007888.Oo08();
        final int size = oo08.size();
        int i = oo88o8O;
        if (!this.oO80) {
            for (final LocalList.Entry entry : list2) {
                if (oo88o8O == entry.Oo08()) {
                    this.\u3007\u3007808\u3007[oo88o8O] = entry;
                    break;
                }
            }
            i = oo88o8O + 1;
        }
        final int cursor3 = this.\u3007o\u3007.getCursor();
        this.\u3007o\u3007.\u3007080(size);
        if (b) {
            this.\u3007080(this.\u3007o\u3007.getCursor() - cursor3, String.format("parameters_size: %04x", size));
        }
        int j = 0;
    Label_0267:
        while (j < size) {
            final Type \u3007o\u3007 = oo08.\u3007O\u3007(j);
            final int cursor4 = this.\u3007o\u3007.getCursor();
            while (true) {
                for (final LocalList.Entry entry2 : list2) {
                    if (i == entry2.Oo08()) {
                        if (entry2.\u3007\u3007888() != null) {
                            this.\u3007\u30078O0\u30078(null);
                        }
                        else {
                            this.\u3007\u30078O0\u30078(entry2.O8());
                        }
                        this.\u3007\u3007808\u3007[i] = entry2;
                        if (entry2 == null) {
                            this.\u3007\u30078O0\u30078(null);
                        }
                        if (b) {
                            String human;
                            if (entry2 != null && entry2.\u3007\u3007888() == null) {
                                human = entry2.O8().toHuman();
                            }
                            else {
                                human = "<unnamed>";
                            }
                            final int cursor5 = this.\u3007o\u3007.getCursor();
                            final StringBuilder sb2 = new StringBuilder();
                            sb2.append("parameter ");
                            sb2.append(human);
                            sb2.append(" ");
                            sb2.append("v");
                            sb2.append(i);
                            this.\u3007080(cursor5 - cursor4, sb2.toString());
                        }
                        i += \u3007o\u3007.Oo08();
                        ++j;
                        continue Label_0267;
                    }
                }
                LocalList.Entry entry2 = null;
                continue;
            }
        }
        final LocalList.Entry[] \u3007\u3007808\u3007 = this.\u3007\u3007808\u3007;
        for (int length = \u3007\u3007808\u3007.length, k = n; k < length; ++k) {
            final LocalList.Entry entry3 = \u3007\u3007808\u3007[k];
            if (entry3 != null) {
                if (entry3.\u3007\u3007888() != null) {
                    this.Oooo8o0\u3007(entry3);
                }
            }
        }
    }
    
    private byte[] Oo08() throws IOException {
        final ArrayList<PositionList.Entry> \u3007o00\u3007\u3007Oo = this.\u3007o00\u3007\u3007Oo();
        this.OO0o\u3007\u3007\u3007\u30070(\u3007o00\u3007\u3007Oo, this.\u3007O888o0o());
        this.\u3007o\u3007.writeByte(7);
        final AnnotatedOutput \u30078o8o\u3007 = this.\u30078o8o\u3007;
        int n = 0;
        if (\u30078o8o\u3007 != null || this.\u3007O8o08O != null) {
            this.\u3007080(1, String.format("%04x: prologue end", this.\u300780\u3007808\u3007O));
        }
        final int size = \u3007o00\u3007\u3007Oo.size();
        final int size2 = this.\u3007o00\u3007\u3007Oo.size();
        int n2 = 0;
        while (true) {
            final int \u3007\u3007808\u3007 = this.\u3007\u3007808\u3007(n);
            final int \u3007o00 = this.\u3007O00(n2, \u3007o00\u3007\u3007Oo);
            int \u3007o00\u3007\u3007Oo2;
            if (\u3007\u3007808\u3007 < size2) {
                \u3007o00\u3007\u3007Oo2 = this.\u3007o00\u3007\u3007Oo.\u3007\u3007808\u3007(\u3007\u3007808\u3007).\u3007o00\u3007\u3007Oo();
            }
            else {
                \u3007o00\u3007\u3007Oo2 = Integer.MAX_VALUE;
            }
            int \u3007080;
            if (\u3007o00 < size) {
                \u3007080 = ((PositionList.Entry)\u3007o00\u3007\u3007Oo.get(\u3007o00)).\u3007080();
            }
            else {
                \u3007080 = Integer.MAX_VALUE;
            }
            final int min = Math.min(\u3007080, \u3007o00\u3007\u3007Oo2);
            if (min == Integer.MAX_VALUE) {
                break;
            }
            if (min == this.Oo08 && \u3007o00\u3007\u3007Oo2 == Integer.MAX_VALUE && \u3007080 == Integer.MAX_VALUE) {
                break;
            }
            if (min == \u3007080) {
                this.\u3007O\u3007((PositionList.Entry)\u3007o00\u3007\u3007Oo.get(\u3007o00));
                n2 = \u3007o00 + 1;
                n = \u3007\u3007808\u3007;
            }
            else {
                this.oO80(min - this.\u300780\u3007808\u3007O);
                n = \u3007\u3007808\u3007;
                n2 = \u3007o00;
            }
        }
        this.\u300780\u3007808\u3007O();
        return this.\u3007o\u3007.\u3007O00();
    }
    
    private void OoO8(final int i) throws IOException {
        if (i >= 0) {
            this.\u3007o\u3007.\u3007080(i);
            return;
        }
        final StringBuilder sb = new StringBuilder();
        sb.append("Signed value where unsigned required: ");
        sb.append(i);
        throw new RuntimeException(sb.toString());
    }
    
    private void Oooo8o0\u3007(final LocalList.Entry entry) throws IOException {
        final int cursor = this.\u3007o\u3007.getCursor();
        this.\u3007o\u3007.writeByte(4);
        this.OoO8(entry.Oo08());
        this.\u3007\u30078O0\u30078(entry.O8());
        this.\u30070\u3007O0088o(entry.getType());
        this.\u3007\u30078O0\u30078(entry.\u3007\u3007888());
        if (this.\u30078o8o\u3007 != null || this.\u3007O8o08O != null) {
            this.\u3007080(this.\u3007o\u3007.getCursor() - cursor, String.format("%04x: +localx %s", this.\u300780\u3007808\u3007O, this.o800o8O(entry)));
        }
    }
    
    private String o800o8O(final LocalList.Entry entry) {
        final StringBuilder sb = new StringBuilder();
        sb.append("v");
        sb.append(entry.Oo08());
        sb.append(' ');
        final CstString o8 = entry.O8();
        if (o8 == null) {
            sb.append("null");
        }
        else {
            sb.append(o8.toHuman());
        }
        sb.append(' ');
        final CstType type = entry.getType();
        if (type == null) {
            sb.append("null");
        }
        else {
            sb.append(type.toHuman());
        }
        final CstString \u3007\u3007888 = entry.\u3007\u3007888();
        if (\u3007\u3007888 != null) {
            sb.append(' ');
            sb.append(\u3007\u3007888.toHuman());
        }
        return sb.toString();
    }
    
    private void oO80(final int n) throws IOException {
        final int cursor = this.\u3007o\u3007.getCursor();
        this.\u3007o\u3007.writeByte(1);
        this.\u3007o\u3007.\u3007080(n);
        this.\u300780\u3007808\u3007O += n;
        if (this.\u30078o8o\u3007 != null || this.\u3007O8o08O != null) {
            this.\u3007080(this.\u3007o\u3007.getCursor() - cursor, String.format("%04x: advance pc", this.\u300780\u3007808\u3007O));
        }
    }
    
    private int oo88o8O() {
        return this.o\u30070 - this.\u3007\u3007888.Oo08().\u3007O00() - ((this.oO80 ^ true) ? 1 : 0);
    }
    
    private void \u3007080(int n, final String str) {
        String string = str;
        if (this.OO0o\u3007\u3007 != null) {
            final StringBuilder sb = new StringBuilder();
            sb.append(this.OO0o\u3007\u3007);
            sb.append(str);
            string = sb.toString();
        }
        final AnnotatedOutput \u30078o8o\u3007 = this.\u30078o8o\u3007;
        if (\u30078o8o\u3007 != null) {
            if (!this.Oooo8o0\u3007) {
                n = 0;
            }
            \u30078o8o\u3007.oO80(n, string);
        }
        final PrintWriter \u3007o8o08O = this.\u3007O8o08O;
        if (\u3007o8o08O != null) {
            \u3007o8o08O.println(string);
        }
    }
    
    private void \u30070\u3007O0088o(final CstType cstType) throws IOException {
        if (cstType != null) {
            final DexFile o8 = this.O8;
            if (o8 != null) {
                this.\u3007o\u3007.\u3007080(o8.o800o8O().\u30070\u3007O0088o(cstType) + 1);
                return;
            }
        }
        this.\u3007o\u3007.\u3007080(0);
    }
    
    private void \u300780\u3007808\u3007O() {
        this.\u3007o\u3007.writeByte(0);
        if (this.\u30078o8o\u3007 != null || this.\u3007O8o08O != null) {
            this.\u3007080(1, "end sequence");
        }
    }
    
    private void \u30078o8o\u3007(final LocalList.Entry entry) throws IOException {
        final int cursor = this.\u3007o\u3007.getCursor();
        this.\u3007o\u3007.writeByte(5);
        this.\u3007o\u3007.\u3007080(entry.Oo08());
        if (this.\u30078o8o\u3007 != null || this.\u3007O8o08O != null) {
            this.\u3007080(this.\u3007o\u3007.getCursor() - cursor, String.format("%04x: -local %s", this.\u300780\u3007808\u3007O, this.o800o8O(entry)));
        }
    }
    
    private int \u3007O00(int n, final ArrayList<PositionList.Entry> list) throws IOException {
        while (n < list.size() && list.get(n).\u3007080() == this.\u300780\u3007808\u3007O) {
            this.\u3007O\u3007(list.get(n));
            ++n;
        }
        return n;
    }
    
    private ArrayList<LocalList.Entry> \u3007O888o0o() {
        final ArrayList list = new ArrayList(this.\u3007\u3007888.Oo08().size());
        final int oo88o8O = this.oo88o8O();
        final BitSet set = new BitSet(this.o\u30070 - oo88o8O);
        for (int size = this.\u3007o00\u3007\u3007Oo.size(), i = 0; i < size; ++i) {
            final LocalList.Entry \u3007\u3007808\u3007 = this.\u3007o00\u3007\u3007Oo.\u3007\u3007808\u3007(i);
            final int oo08 = \u3007\u3007808\u3007.Oo08();
            if (oo08 >= oo88o8O) {
                final int n = oo08 - oo88o8O;
                if (!set.get(n)) {
                    set.set(n);
                    list.add(\u3007\u3007808\u3007);
                }
            }
        }
        Collections.sort((List<Object>)list, (Comparator<? super Object>)new Comparator<LocalList.Entry>(this) {
            final DebugInfoEncoder o0;
            
            @Override
            public boolean equals(final Object o) {
                return o == this;
            }
            
            public int \u3007080(final LocalList.Entry entry, final LocalList.Entry entry2) {
                return entry.Oo08() - entry2.Oo08();
            }
        });
        return list;
    }
    
    private void \u3007O8o08O(final LocalList.Entry entry) throws IOException {
        final int cursor = this.\u3007o\u3007.getCursor();
        this.\u3007o\u3007.writeByte(6);
        this.OoO8(entry.Oo08());
        if (this.\u30078o8o\u3007 != null || this.\u3007O8o08O != null) {
            this.\u3007080(this.\u3007o\u3007.getCursor() - cursor, String.format("%04x: +local restart %s", this.\u300780\u3007808\u3007O, this.o800o8O(entry)));
        }
    }
    
    private void \u3007O\u3007(final PositionList.Entry entry) throws IOException {
        final int \u3007080 = entry.\u3007o00\u3007\u3007Oo().\u3007080();
        final int \u300781 = entry.\u3007080();
        final int n = \u3007080 - this.OO0o\u3007\u3007\u3007\u30070;
        final int n2 = \u300781 - this.\u300780\u3007808\u3007O;
        if (n2 >= 0) {
            int n3;
            if (n < -4 || (n3 = n) > 10) {
                this.\u3007\u3007888(n);
                n3 = 0;
            }
            final int \u3007o\u3007 = \u3007o\u3007(n3, n2);
            int n4 = n3;
            int n5 = \u3007o\u3007;
            int n6 = n2;
            if ((\u3007o\u3007 & 0xFFFFFF00) > 0) {
                this.oO80(n2);
                n5 = \u3007o\u3007(n3, 0);
                if ((n5 & 0xFFFFFF00) > 0) {
                    this.\u3007\u3007888(n3);
                    n5 = \u3007o\u3007(0, 0);
                    n6 = 0;
                    n4 = 0;
                }
                else {
                    n6 = 0;
                    n4 = n3;
                }
            }
            this.\u3007o\u3007.writeByte(n5);
            this.OO0o\u3007\u3007\u3007\u30070 += n4;
            final int n7 = this.\u300780\u3007808\u3007O + n6;
            this.\u300780\u3007808\u3007O = n7;
            if (this.\u30078o8o\u3007 != null || this.\u3007O8o08O != null) {
                this.\u3007080(1, String.format("%04x: line %d", n7, this.OO0o\u3007\u3007\u3007\u30070));
            }
            return;
        }
        throw new RuntimeException("Position entries must be in ascending address order");
    }
    
    private ArrayList<PositionList.Entry> \u3007o00\u3007\u3007Oo() {
        final PositionList \u3007080 = this.\u3007080;
        int i = 0;
        int size;
        if (\u3007080 == null) {
            size = 0;
        }
        else {
            size = \u3007080.size();
        }
        final ArrayList list = new ArrayList<Object>(size);
        while (i < size) {
            list.add(this.\u3007080.\u3007\u3007808\u3007(i));
            ++i;
        }
        Collections.sort((List<Object>)list, (Comparator<? super Object>)new Comparator<PositionList.Entry>(this) {
            final DebugInfoEncoder o0;
            
            @Override
            public boolean equals(final Object o) {
                return o == this;
            }
            
            public int \u3007080(final PositionList.Entry entry, final PositionList.Entry entry2) {
                return entry.\u3007080() - entry2.\u3007080();
            }
        });
        return (ArrayList<PositionList.Entry>)list;
    }
    
    private static int \u3007o\u3007(final int n, final int n2) {
        if (n >= -4 && n <= 10) {
            return n + 4 + n2 * 15 + 10;
        }
        throw new RuntimeException("Parameter out of range");
    }
    
    private int \u3007\u3007808\u3007(int n) throws IOException {
        while (n < this.\u3007o00\u3007\u3007Oo.size() && this.\u3007o00\u3007\u3007Oo.\u3007\u3007808\u3007(n).\u3007o00\u3007\u3007Oo() == this.\u300780\u3007808\u3007O) {
            final LocalList.Entry \u3007\u3007808\u3007 = this.\u3007o00\u3007\u3007Oo.\u3007\u3007808\u3007(n);
            final int oo08 = \u3007\u3007808\u3007.Oo08();
            final LocalList.Entry[] \u3007\u3007808\u30072 = this.\u3007\u3007808\u3007;
            final LocalList.Entry entry = \u3007\u3007808\u30072[oo08];
            if (\u3007\u3007808\u3007 != entry) {
                \u3007\u3007808\u30072[oo08] = \u3007\u3007808\u3007;
                if (\u3007\u3007808\u3007.oO80()) {
                    if (entry != null && \u3007\u3007808\u3007.\u300780\u3007808\u3007O(entry)) {
                        if (entry.oO80()) {
                            throw new RuntimeException("shouldn't happen");
                        }
                        this.\u3007O8o08O(\u3007\u3007808\u3007);
                    }
                    else {
                        this.OO0o\u3007\u3007(\u3007\u3007808\u3007);
                    }
                }
                else if (\u3007\u3007808\u3007.\u3007o\u3007() != LocalList.Disposition.END_REPLACED) {
                    this.\u30078o8o\u3007(\u3007\u3007808\u3007);
                }
            }
            ++n;
        }
        return n;
    }
    
    private void \u3007\u3007888(final int n) throws IOException {
        final int cursor = this.\u3007o\u3007.getCursor();
        this.\u3007o\u3007.writeByte(2);
        this.\u3007o\u3007.OoO8(n);
        this.OO0o\u3007\u3007\u3007\u30070 += n;
        if (this.\u30078o8o\u3007 != null || this.\u3007O8o08O != null) {
            this.\u3007080(this.\u3007o\u3007.getCursor() - cursor, String.format("line = %d", this.OO0o\u3007\u3007\u3007\u30070));
        }
    }
    
    private void \u3007\u30078O0\u30078(final CstString cstString) throws IOException {
        if (cstString != null) {
            final DexFile o8 = this.O8;
            if (o8 != null) {
                this.\u3007o\u3007.\u3007080(o8.OoO8().\u30070\u3007O0088o(cstString) + 1);
                return;
            }
        }
        this.\u3007o\u3007.\u3007080(0);
    }
    
    public byte[] O8() {
        try {
            return this.Oo08();
        }
        catch (final IOException ex) {
            throw ExceptionWithContext.withContext(ex, "...while encoding debug info");
        }
    }
    
    public byte[] o\u30070(final String oo0o\u3007\u3007, final PrintWriter \u3007o8o08O, final AnnotatedOutput \u30078o8o\u3007, final boolean oooo8o0\u3007) {
        this.OO0o\u3007\u3007 = oo0o\u3007\u3007;
        this.\u3007O8o08O = \u3007o8o08O;
        this.\u30078o8o\u3007 = \u30078o8o\u3007;
        this.Oooo8o0\u3007 = oooo8o0\u3007;
        return this.O8();
    }
}

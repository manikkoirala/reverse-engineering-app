// 
// Decompiled by Procyon v0.6.0
// 

package com.android.dx.dex.file;

import com.android.dx.util.ByteArrayAnnotatedOutput;
import java.util.Iterator;
import java.util.Map;
import com.android.dx.util.Hex;
import com.android.dx.util.AnnotatedOutput;
import java.io.PrintWriter;
import com.android.dx.dex.code.CatchTable;
import com.android.dx.dex.code.DalvCode;
import com.android.dx.dex.code.CatchHandlerList;
import java.util.TreeMap;

public final class CatchStructs
{
    private int O8;
    private TreeMap<CatchHandlerList, Integer> Oo08;
    private final DalvCode \u3007080;
    private CatchTable \u3007o00\u3007\u3007Oo;
    private byte[] \u3007o\u3007;
    
    public CatchStructs(final DalvCode \u3007080) {
        this.\u3007080 = \u3007080;
        this.\u3007o00\u3007\u3007Oo = null;
        this.\u3007o\u3007 = null;
        this.O8 = 0;
        this.Oo08 = null;
    }
    
    private void O8() {
        if (this.\u3007o00\u3007\u3007Oo == null) {
            this.\u3007o00\u3007\u3007Oo = this.\u3007080.O8();
        }
    }
    
    private static void \u3007080(final CatchHandlerList list, final int n, final int n2, final String s, final PrintWriter printWriter, final AnnotatedOutput annotatedOutput) {
        final StringBuilder sb = new StringBuilder();
        sb.append(Hex.Oo08(n));
        sb.append(": ");
        final String \u30070\u3007O0088o = list.\u30070\u3007O0088o(s, sb.toString());
        if (printWriter != null) {
            printWriter.println(\u30070\u3007O0088o);
        }
        annotatedOutput.oO80(n2, \u30070\u3007O0088o);
    }
    
    private void \u3007o00\u3007\u3007Oo(final String s, final PrintWriter printWriter, final AnnotatedOutput annotatedOutput) {
        this.O8();
        final int n = 0;
        final boolean b = annotatedOutput != null;
        int n2;
        if (b) {
            n2 = 6;
        }
        else {
            n2 = 0;
        }
        int n3;
        if (b) {
            n3 = 2;
        }
        else {
            n3 = 0;
        }
        final int size = this.\u3007o00\u3007\u3007Oo.size();
        final StringBuilder sb = new StringBuilder();
        sb.append(s);
        sb.append("  ");
        final String string = sb.toString();
        if (b) {
            final StringBuilder sb2 = new StringBuilder();
            sb2.append(s);
            sb2.append("tries:");
            annotatedOutput.oO80(0, sb2.toString());
        }
        else {
            final StringBuilder sb3 = new StringBuilder();
            sb3.append(s);
            sb3.append("tries:");
            printWriter.println(sb3.toString());
        }
        for (int i = 0; i < size; ++i) {
            final CatchTable.Entry \u3007o\u3007 = this.\u3007o00\u3007\u3007Oo.\u3007O\u3007(i);
            final CatchHandlerList \u3007o\u30072 = \u3007o\u3007.\u3007o\u3007();
            final StringBuilder sb4 = new StringBuilder();
            sb4.append(string);
            sb4.append("try ");
            sb4.append(Hex.o\u30070(\u3007o\u3007.O8()));
            sb4.append("..");
            sb4.append(Hex.o\u30070(\u3007o\u3007.\u3007o00\u3007\u3007Oo()));
            final String string2 = sb4.toString();
            final String \u30070\u3007O0088o = \u3007o\u30072.\u30070\u3007O0088o(string, "");
            if (b) {
                annotatedOutput.oO80(n2, string2);
                annotatedOutput.oO80(n3, \u30070\u3007O0088o);
            }
            else {
                printWriter.println(string2);
                printWriter.println(\u30070\u3007O0088o);
            }
        }
        if (!b) {
            return;
        }
        final StringBuilder sb5 = new StringBuilder();
        sb5.append(s);
        sb5.append("handlers:");
        annotatedOutput.oO80(0, sb5.toString());
        final int o8 = this.O8;
        final StringBuilder sb6 = new StringBuilder();
        sb6.append(string);
        sb6.append("size: ");
        sb6.append(Hex.Oo08(this.Oo08.size()));
        annotatedOutput.oO80(o8, sb6.toString());
        final Iterator<Map.Entry<CatchHandlerList, Integer>> iterator = this.Oo08.entrySet().iterator();
        CatchHandlerList list = null;
        int n4 = n;
        while (iterator.hasNext()) {
            final Map.Entry<CatchHandlerList, V> entry = (Map.Entry<CatchHandlerList, V>)iterator.next();
            final CatchHandlerList list2 = entry.getKey();
            final int intValue = (int)entry.getValue();
            if (list != null) {
                \u3007080(list, n4, intValue - n4, string, printWriter, annotatedOutput);
            }
            list = list2;
            n4 = intValue;
        }
        \u3007080(list, n4, this.\u3007o\u3007.length - n4, string, printWriter, annotatedOutput);
    }
    
    public int Oo08() {
        this.O8();
        return this.\u3007o00\u3007\u3007Oo.size();
    }
    
    public int o\u30070() {
        return this.Oo08() * 8 + this.\u3007o\u3007.length;
    }
    
    public void \u3007o\u3007(final DexFile dexFile) {
        this.O8();
        final TypeIdsSection o800o8O = dexFile.o800o8O();
        final int size = this.\u3007o00\u3007\u3007Oo.size();
        this.Oo08 = new TreeMap<CatchHandlerList, Integer>();
        for (int i = 0; i < size; ++i) {
            this.Oo08.put(this.\u3007o00\u3007\u3007Oo.\u3007O\u3007(i).\u3007o\u3007(), null);
        }
        if (this.Oo08.size() <= 65535) {
            final ByteArrayAnnotatedOutput byteArrayAnnotatedOutput = new ByteArrayAnnotatedOutput();
            this.O8 = byteArrayAnnotatedOutput.\u3007080(this.Oo08.size());
            for (final Map.Entry<CatchHandlerList, V> entry : this.Oo08.entrySet()) {
                final CatchHandlerList list = entry.getKey();
                int size2 = list.size();
                final boolean \u3007\u3007808\u3007 = list.\u3007\u3007808\u3007();
                entry.setValue((V)byteArrayAnnotatedOutput.getCursor());
                if (\u3007\u3007808\u3007) {
                    byteArrayAnnotatedOutput.OoO8(-(size2 - 1));
                    --size2;
                }
                else {
                    byteArrayAnnotatedOutput.OoO8(size2);
                }
                for (int j = 0; j < size2; ++j) {
                    final CatchHandlerList.Entry \u3007o00 = list.\u3007O00(j);
                    byteArrayAnnotatedOutput.\u3007080(o800o8O.\u30070\u3007O0088o(\u3007o00.\u3007o00\u3007\u3007Oo()));
                    byteArrayAnnotatedOutput.\u3007080(\u3007o00.\u3007o\u3007());
                }
                if (\u3007\u3007808\u3007) {
                    byteArrayAnnotatedOutput.\u3007080(list.\u3007O00(size2).\u3007o\u3007());
                }
            }
            this.\u3007o\u3007 = byteArrayAnnotatedOutput.\u3007O00();
            return;
        }
        throw new UnsupportedOperationException("too many catch handlers");
    }
    
    public void \u3007\u3007888(final DexFile dexFile, final AnnotatedOutput annotatedOutput) {
        this.O8();
        if (annotatedOutput.\u3007o\u3007()) {
            this.\u3007o00\u3007\u3007Oo("  ", null, annotatedOutput);
        }
        for (int size = this.\u3007o00\u3007\u3007Oo.size(), i = 0; i < size; ++i) {
            final CatchTable.Entry \u3007o\u3007 = this.\u3007o00\u3007\u3007Oo.\u3007O\u3007(i);
            final int o8 = \u3007o\u3007.O8();
            final int \u3007o00\u3007\u3007Oo = \u3007o\u3007.\u3007o00\u3007\u3007Oo();
            final int n = \u3007o00\u3007\u3007Oo - o8;
            if (n >= 65536) {
                final StringBuilder sb = new StringBuilder();
                sb.append("bogus exception range: ");
                sb.append(Hex.oO80(o8));
                sb.append("..");
                sb.append(Hex.oO80(\u3007o00\u3007\u3007Oo));
                throw new UnsupportedOperationException(sb.toString());
            }
            annotatedOutput.writeInt(o8);
            annotatedOutput.writeShort(n);
            annotatedOutput.writeShort(this.Oo08.get(\u3007o\u3007.\u3007o\u3007()));
        }
        annotatedOutput.write(this.\u3007o\u3007);
    }
}

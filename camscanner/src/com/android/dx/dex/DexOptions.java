// 
// Decompiled by Procyon v0.6.0
// 

package com.android.dx.dex;

import com.android.dex.DexFormat;
import java.io.PrintStream;

public final class DexOptions
{
    public boolean O8;
    public final PrintStream Oo08;
    public boolean \u3007080;
    public int \u3007o00\u3007\u3007Oo;
    public boolean \u3007o\u3007;
    
    public DexOptions() {
        this.\u3007080 = true;
        this.\u3007o00\u3007\u3007Oo = 13;
        this.\u3007o\u3007 = false;
        this.O8 = false;
        this.Oo08 = System.err;
    }
    
    public boolean \u3007080(final int n) {
        return this.\u3007o00\u3007\u3007Oo >= n;
    }
    
    public String \u3007o00\u3007\u3007Oo() {
        return DexFormat.\u3007080(this.\u3007o00\u3007\u3007Oo);
    }
}

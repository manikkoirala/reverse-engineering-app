// 
// Decompiled by Procyon v0.6.0
// 

package com.android.dx.dex.code;

import com.android.dx.rop.code.RegisterSpecList;
import com.android.dx.rop.code.SourcePosition;

public final class CodeAddress extends ZeroSizeInsn
{
    private final boolean Oo08;
    
    public CodeAddress(final SourcePosition sourcePosition) {
        this(sourcePosition, false);
    }
    
    public CodeAddress(final SourcePosition sourcePosition, final boolean oo08) {
        super(sourcePosition);
        this.Oo08 = oo08;
    }
    
    public boolean O\u30078O8\u3007008() {
        return this.Oo08;
    }
    
    @Override
    public final DalvInsn o\u3007O8\u3007\u3007o(final RegisterSpecList list) {
        return new CodeAddress(this.OO0o\u3007\u3007());
    }
    
    @Override
    protected String \u3007080() {
        return null;
    }
    
    @Override
    protected String \u30070\u3007O0088o(final boolean b) {
        return "code-address";
    }
}

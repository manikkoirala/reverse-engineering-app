// 
// Decompiled by Procyon v0.6.0
// 

package com.android.dx.dex.code;

import com.android.dx.rop.code.SourcePosition;
import com.android.dx.rop.code.BasicBlock;
import com.android.dx.rop.code.BasicBlockList;
import com.android.dx.rop.code.RopMethod;

public final class BlockAddresses
{
    private final CodeAddress[] \u3007080;
    private final CodeAddress[] \u3007o00\u3007\u3007Oo;
    private final CodeAddress[] \u3007o\u3007;
    
    public BlockAddresses(final RopMethod ropMethod) {
        final int \u3007o\u3007 = ropMethod.\u3007o00\u3007\u3007Oo().\u3007O\u3007();
        this.\u3007080 = new CodeAddress[\u3007o\u3007];
        this.\u3007o00\u3007\u3007Oo = new CodeAddress[\u3007o\u3007];
        this.\u3007o\u3007 = new CodeAddress[\u3007o\u3007];
        this.Oo08(ropMethod);
    }
    
    private void Oo08(final RopMethod ropMethod) {
        final BasicBlockList \u3007o00\u3007\u3007Oo = ropMethod.\u3007o00\u3007\u3007Oo();
        for (int size = \u3007o00\u3007\u3007Oo.size(), i = 0; i < size; ++i) {
            final BasicBlock o800o8O = \u3007o00\u3007\u3007Oo.o800o8O(i);
            final int label = o800o8O.getLabel();
            this.\u3007080[label] = new CodeAddress(o800o8O.\u3007o00\u3007\u3007Oo().\u3007O\u3007(0).oO80());
            final SourcePosition oo80 = o800o8O.\u3007o\u3007().oO80();
            this.\u3007o00\u3007\u3007Oo[label] = new CodeAddress(oo80);
            this.\u3007o\u3007[label] = new CodeAddress(oo80);
        }
    }
    
    public CodeAddress O8(final BasicBlock basicBlock) {
        return this.\u3007080[basicBlock.getLabel()];
    }
    
    public CodeAddress \u3007080(final BasicBlock basicBlock) {
        return this.\u3007o\u3007[basicBlock.getLabel()];
    }
    
    public CodeAddress \u3007o00\u3007\u3007Oo(final BasicBlock basicBlock) {
        return this.\u3007o00\u3007\u3007Oo[basicBlock.getLabel()];
    }
    
    public CodeAddress \u3007o\u3007(final int n) {
        return this.\u3007080[n];
    }
}

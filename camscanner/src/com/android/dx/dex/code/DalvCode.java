// 
// Decompiled by Procyon v0.6.0
// 

package com.android.dx.dex.code;

import com.android.dx.rop.type.Type;
import com.android.dx.rop.cst.Constant;
import java.util.HashSet;

public final class DalvCode
{
    private CatchTable O8;
    private PositionList Oo08;
    private LocalList o\u30070;
    private final int \u3007080;
    private OutputFinisher \u3007o00\u3007\u3007Oo;
    private CatchBuilder \u3007o\u3007;
    private DalvInsnList \u3007\u3007888;
    
    public DalvCode(final int \u3007080, final OutputFinisher \u3007o00\u3007\u3007Oo, final CatchBuilder \u3007o\u3007) {
        if (\u3007o00\u3007\u3007Oo == null) {
            throw new NullPointerException("unprocessedInsns == null");
        }
        if (\u3007o\u3007 != null) {
            this.\u3007080 = \u3007080;
            this.\u3007o00\u3007\u3007Oo = \u3007o00\u3007\u3007Oo;
            this.\u3007o\u3007 = \u3007o\u3007;
            this.O8 = null;
            this.Oo08 = null;
            this.o\u30070 = null;
            this.\u3007\u3007888 = null;
            return;
        }
        throw new NullPointerException("unprocessedCatches == null");
    }
    
    private void \u3007o00\u3007\u3007Oo() {
        if (this.\u3007\u3007888 != null) {
            return;
        }
        final DalvInsnList \u3007\u3007808\u3007 = this.\u3007o00\u3007\u3007Oo.\u3007\u3007808\u3007();
        this.\u3007\u3007888 = \u3007\u3007808\u3007;
        this.Oo08 = PositionList.\u3007O\u3007(\u3007\u3007808\u3007, this.\u3007080);
        this.o\u30070 = LocalList.\u3007O\u3007(this.\u3007\u3007888);
        this.O8 = this.\u3007o\u3007.build();
        this.\u3007o00\u3007\u3007Oo = null;
        this.\u3007o\u3007 = null;
    }
    
    public CatchTable O8() {
        this.\u3007o00\u3007\u3007Oo();
        return this.O8;
    }
    
    public boolean OO0o\u3007\u3007\u3007\u30070() {
        return this.\u3007o00\u3007\u3007Oo.\u3007\u30078O0\u30078();
    }
    
    public HashSet<Constant> Oo08() {
        return this.\u3007o00\u3007\u3007Oo.\u3007O00();
    }
    
    public PositionList oO80() {
        this.\u3007o00\u3007\u3007Oo();
        return this.Oo08;
    }
    
    public DalvInsnList o\u30070() {
        this.\u3007o00\u3007\u3007Oo();
        return this.\u3007\u3007888;
    }
    
    public void \u3007080(final AssignIndicesCallback assignIndicesCallback) {
        this.\u3007o00\u3007\u3007Oo.OO0o\u3007\u3007\u3007\u30070(assignIndicesCallback);
    }
    
    public boolean \u300780\u3007808\u3007O() {
        return this.\u3007o\u3007.\u3007080();
    }
    
    public boolean \u30078o8o\u3007() {
        final int \u3007080 = this.\u3007080;
        boolean b = true;
        if (\u3007080 == 1 || !this.\u3007o00\u3007\u3007Oo.\u30070\u3007O0088o()) {
            b = false;
        }
        return b;
    }
    
    public HashSet<Type> \u3007o\u3007() {
        return this.\u3007o\u3007.\u3007o00\u3007\u3007Oo();
    }
    
    public LocalList \u3007\u3007888() {
        this.\u3007o00\u3007\u3007Oo();
        return this.o\u30070;
    }
    
    public interface AssignIndicesCallback
    {
        int \u3007080(final Constant p0);
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.android.dx.dex.code;

import com.android.dx.io.OpcodeInfo;
import com.android.dx.io.Opcodes;

public final class Dop
{
    private final InsnFormat O8;
    private final boolean Oo08;
    private final int \u3007080;
    private final int \u3007o00\u3007\u3007Oo;
    private final int \u3007o\u3007;
    
    public Dop(final int \u3007080, final int \u3007o00\u3007\u3007Oo, final int \u3007o\u3007, final InsnFormat o8, final boolean oo08) {
        if (!Opcodes.\u3007080(\u3007080)) {
            throw new IllegalArgumentException("bogus opcode");
        }
        if (!Opcodes.\u3007080(\u3007o00\u3007\u3007Oo)) {
            throw new IllegalArgumentException("bogus family");
        }
        if (!Opcodes.\u3007080(\u3007o\u3007)) {
            throw new IllegalArgumentException("bogus nextOpcode");
        }
        if (o8 != null) {
            this.\u3007080 = \u3007080;
            this.\u3007o00\u3007\u3007Oo = \u3007o00\u3007\u3007Oo;
            this.\u3007o\u3007 = \u3007o\u3007;
            this.O8 = o8;
            this.Oo08 = oo08;
            return;
        }
        throw new NullPointerException("format == null");
    }
    
    public int O8() {
        return this.\u3007o\u3007;
    }
    
    public int Oo08() {
        return this.\u3007080;
    }
    
    public Dop o\u30070() {
        switch (this.\u3007080) {
            default: {
                final StringBuilder sb = new StringBuilder();
                sb.append("bogus opcode: ");
                sb.append(this);
                throw new IllegalArgumentException(sb.toString());
            }
            case 61: {
                return Dops.\u30070;
            }
            case 60: {
                return Dops.o88\u3007OO08\u3007;
            }
            case 59: {
                return Dops.OO8oO0o\u3007;
            }
            case 58: {
                return Dops.o0O0;
            }
            case 57: {
                return Dops.ooo\u30078oO;
            }
            case 56: {
                return Dops.O0o\u3007\u3007Oo;
            }
            case 55: {
                return Dops.\u3007O\u300780o08O;
            }
            case 54: {
                return Dops.OOO;
            }
            case 53: {
                return Dops.\u300780;
            }
            case 52: {
                return Dops.Ooo;
            }
            case 51: {
                return Dops.oO00OOO;
            }
            case 50: {
                return Dops.O000;
            }
        }
    }
    
    @Override
    public String toString() {
        return this.\u3007o\u3007();
    }
    
    public int \u3007080() {
        return this.\u3007o00\u3007\u3007Oo;
    }
    
    public InsnFormat \u3007o00\u3007\u3007Oo() {
        return this.O8;
    }
    
    public String \u3007o\u3007() {
        return OpcodeInfo.\u3007o\u3007(this.\u3007080);
    }
    
    public boolean \u3007\u3007888() {
        return this.Oo08;
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.android.dx.dex.code;

import com.android.dx.dex.DexOptions;
import java.util.ArrayList;

public final class OutputCollector
{
    private final OutputFinisher \u3007080;
    private ArrayList<DalvInsn> \u3007o00\u3007\u3007Oo;
    
    public OutputCollector(final DexOptions dexOptions, final int n, final int initialCapacity, final int n2, final int n3) {
        this.\u3007080 = new OutputFinisher(dexOptions, n, n2, n3);
        this.\u3007o00\u3007\u3007Oo = new ArrayList<DalvInsn>(initialCapacity);
    }
    
    private void \u3007o00\u3007\u3007Oo() {
        for (int size = this.\u3007o00\u3007\u3007Oo.size(), i = 0; i < size; ++i) {
            this.\u3007080.\u3007080(this.\u3007o00\u3007\u3007Oo.get(i));
        }
        this.\u3007o00\u3007\u3007Oo = null;
    }
    
    public void O8(final int n, final CodeAddress codeAddress) {
        this.\u3007080.\u300700(n, codeAddress);
    }
    
    public void \u3007080(final DalvInsn dalvInsn) {
        this.\u3007080.\u3007080(dalvInsn);
    }
    
    public OutputFinisher \u3007o\u3007() {
        if (this.\u3007o00\u3007\u3007Oo != null) {
            this.\u3007o00\u3007\u3007Oo();
            return this.\u3007080;
        }
        throw new UnsupportedOperationException("already processed");
    }
}

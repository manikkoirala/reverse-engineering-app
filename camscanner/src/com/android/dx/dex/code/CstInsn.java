// 
// Decompiled by Procyon v0.6.0
// 

package com.android.dx.dex.code;

import com.android.dx.util.Hex;
import com.android.dx.rop.cst.CstString;
import com.android.dx.rop.code.RegisterSpecList;
import com.android.dx.rop.code.SourcePosition;
import com.android.dx.rop.cst.Constant;

public final class CstInsn extends FixedSizeInsn
{
    private final Constant Oo08;
    private int o\u30070;
    private int \u3007\u3007888;
    
    public CstInsn(final Dop dop, final SourcePosition sourcePosition, final RegisterSpecList list, final Constant oo08) {
        super(dop, sourcePosition, list);
        if (oo08 != null) {
            this.Oo08 = oo08;
            this.o\u30070 = -1;
            this.\u3007\u3007888 = -1;
            return;
        }
        throw new NullPointerException("constant == null");
    }
    
    @Override
    public String O8() {
        final Constant oo08 = this.Oo08;
        if (oo08 instanceof CstString) {
            return ((CstString)oo08).OO0o\u3007\u3007\u3007\u30070();
        }
        return oo08.toHuman();
    }
    
    public int O8ooOoo\u3007() {
        final int o\u30070 = this.o\u30070;
        if (o\u30070 >= 0) {
            return o\u30070;
        }
        final StringBuilder sb = new StringBuilder();
        sb.append("index not yet set for ");
        sb.append(this.Oo08);
        throw new IllegalStateException(sb.toString());
    }
    
    public Constant O\u30078O8\u3007008() {
        return this.Oo08;
    }
    
    @Override
    public DalvInsn oo88o8O(final Dop dop) {
        final CstInsn cstInsn = new CstInsn(dop, this.OO0o\u3007\u3007(), this.Oooo8o0\u3007(), this.Oo08);
        final int o\u30070 = this.o\u30070;
        if (o\u30070 >= 0) {
            cstInsn.o\u3007\u30070\u3007(o\u30070);
        }
        final int \u3007\u3007888 = this.\u3007\u3007888;
        if (\u3007\u3007888 >= 0) {
            cstInsn.\u30070000OOO(\u3007\u3007888);
        }
        return cstInsn;
    }
    
    @Override
    public DalvInsn o\u3007O8\u3007\u3007o(final RegisterSpecList list) {
        final CstInsn cstInsn = new CstInsn(this.\u3007O8o08O(), this.OO0o\u3007\u3007(), list, this.Oo08);
        final int o\u30070 = this.o\u30070;
        if (o\u30070 >= 0) {
            cstInsn.o\u3007\u30070\u3007(o\u30070);
        }
        final int \u3007\u3007888 = this.\u3007\u3007888;
        if (\u3007\u3007888 >= 0) {
            cstInsn.\u30070000OOO(\u3007\u3007888);
        }
        return cstInsn;
    }
    
    public void o\u3007\u30070\u3007(final int o\u30070) {
        if (o\u30070 < 0) {
            throw new IllegalArgumentException("index < 0");
        }
        if (this.o\u30070 < 0) {
            this.o\u30070 = o\u30070;
            return;
        }
        throw new IllegalStateException("index already set");
    }
    
    public void \u30070000OOO(final int \u3007\u3007888) {
        if (\u3007\u3007888 < 0) {
            throw new IllegalArgumentException("index < 0");
        }
        if (this.\u3007\u3007888 < 0) {
            this.\u3007\u3007888 = \u3007\u3007888;
            return;
        }
        throw new IllegalStateException("class index already set");
    }
    
    @Override
    protected String \u3007080() {
        return this.Oo08.toHuman();
    }
    
    public boolean \u3007oOO8O8() {
        return this.o\u30070 >= 0;
    }
    
    @Override
    public String \u3007o\u3007() {
        if (!this.\u3007oOO8O8()) {
            return "";
        }
        final StringBuilder sb = new StringBuilder(20);
        sb.append(this.O\u30078O8\u3007008().Oo08());
        sb.append('@');
        final int o\u30070 = this.o\u30070;
        if (o\u30070 < 65536) {
            sb.append(Hex.Oo08(o\u30070));
        }
        else {
            sb.append(Hex.oO80(o\u30070));
        }
        return sb.toString();
    }
}

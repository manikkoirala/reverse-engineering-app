// 
// Decompiled by Procyon v0.6.0
// 

package com.android.dx.dex.code;

import com.android.dx.util.AnnotatedOutput;
import com.android.dx.rop.code.RegisterSpecList;
import com.android.dx.rop.code.SourcePosition;

public abstract class ZeroSizeInsn extends DalvInsn
{
    public ZeroSizeInsn(final SourcePosition sourcePosition) {
        super(Dops.\u3007o00\u3007\u3007Oo, sourcePosition, RegisterSpecList.OO);
    }
    
    @Override
    public final DalvInsn oo88o8O(final Dop dop) {
        throw new RuntimeException("unsupported");
    }
    
    @Override
    public final void \u300700(final AnnotatedOutput annotatedOutput) {
    }
    
    @Override
    public final int \u3007o00\u3007\u3007Oo() {
        return 0;
    }
    
    @Override
    public DalvInsn \u3007oo\u3007(final int n) {
        return this.o\u3007O8\u3007\u3007o(this.Oooo8o0\u3007().\u300700(n));
    }
}

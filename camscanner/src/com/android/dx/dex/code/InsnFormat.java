// 
// Decompiled by Procyon v0.6.0
// 

package com.android.dx.dex.code;

import java.util.BitSet;
import com.android.dx.rop.code.RegisterSpec;
import com.android.dx.rop.code.RegisterSpecList;
import com.android.dx.rop.cst.CstKnownNull;
import com.android.dx.util.Hex;
import com.android.dx.rop.cst.CstLiteral64;
import com.android.dx.rop.cst.CstLiteralBits;
import com.android.dx.util.AnnotatedOutput;

public abstract class InsnFormat
{
    protected static void O8ooOoo\u3007(final AnnotatedOutput annotatedOutput, final short n, final short n2) {
        annotatedOutput.writeShort(n);
        annotatedOutput.writeShort(n2);
    }
    
    protected static String OO0o\u3007\u3007(final CstLiteralBits cstLiteralBits, final int n) {
        final StringBuilder sb = new StringBuilder(20);
        sb.append("#");
        long oo80;
        if (cstLiteralBits instanceof CstLiteral64) {
            oo80 = ((CstLiteral64)cstLiteralBits).oO80();
        }
        else {
            oo80 = cstLiteralBits.\u3007\u3007888();
        }
        if (n != 4) {
            if (n != 8) {
                if (n != 16) {
                    if (n != 32) {
                        if (n != 64) {
                            throw new RuntimeException("shouldn't happen");
                        }
                        sb.append(Hex.\u300780\u3007808\u3007O(oo80));
                    }
                    else {
                        sb.append(Hex.oO80((int)oo80));
                    }
                }
                else {
                    sb.append(Hex.Oo08((int)oo80));
                }
            }
            else {
                sb.append(Hex.O8((int)oo80));
            }
        }
        else {
            sb.append(Hex.OO0o\u3007\u3007\u3007\u30070((int)oo80));
        }
        return sb.toString();
    }
    
    protected static short Oo08(final int n, final int n2) {
        if ((n & 0xFF) != n) {
            throw new IllegalArgumentException("low out of range 0..255");
        }
        if ((n2 & 0xFF) == n2) {
            return (short)(n | n2 << 8);
        }
        throw new IllegalArgumentException("high out of range 0..255");
    }
    
    protected static boolean OoO8(final int n) {
        return n >= -8 && n <= 7;
    }
    
    protected static String Oooo8o0\u3007(final CstLiteralBits cstLiteralBits) {
        final StringBuilder sb = new StringBuilder(100);
        sb.append('#');
        if (cstLiteralBits instanceof CstKnownNull) {
            sb.append("null");
        }
        else {
            sb.append(cstLiteralBits.Oo08());
            sb.append(' ');
            sb.append(cstLiteralBits.toHuman());
        }
        return sb.toString();
    }
    
    protected static void O\u30078O8\u3007008(final AnnotatedOutput annotatedOutput, final short n, final long n2) {
        o\u3007\u30070\u3007(annotatedOutput, n, (short)n2, (short)(n2 >> 16), (short)(n2 >> 32), (short)(n2 >> 48));
    }
    
    protected static boolean o800o8O(final int n) {
        return (short)n == n;
    }
    
    protected static boolean oo88o8O(final int n) {
        return n == (n & 0xF);
    }
    
    protected static short o\u30070(final int n, final int n2, final int n3, final int n4) {
        if ((n & 0xF) != n) {
            throw new IllegalArgumentException("n0 out of range 0..15");
        }
        if ((n2 & 0xF) != n2) {
            throw new IllegalArgumentException("n1 out of range 0..15");
        }
        if ((n3 & 0xF) != n3) {
            throw new IllegalArgumentException("n2 out of range 0..15");
        }
        if ((n4 & 0xF) == n4) {
            return (short)(n | n2 << 4 | n3 << 8 | n4 << 12);
        }
        throw new IllegalArgumentException("n3 out of range 0..15");
    }
    
    protected static void o\u3007O8\u3007\u3007o(final AnnotatedOutput annotatedOutput, final short n) {
        annotatedOutput.writeShort(n);
    }
    
    protected static void o\u3007\u30070\u3007(final AnnotatedOutput annotatedOutput, final short n, final short n2, final short n3, final short n4, final short n5) {
        annotatedOutput.writeShort(n);
        annotatedOutput.writeShort(n2);
        annotatedOutput.writeShort(n3);
        annotatedOutput.writeShort(n4);
        annotatedOutput.writeShort(n5);
    }
    
    protected static void \u300700(final AnnotatedOutput annotatedOutput, final short n, final int n2) {
        \u3007oOO8O8(annotatedOutput, n, (short)n2, (short)(n2 >> 16));
    }
    
    protected static void \u30070000OOO(final AnnotatedOutput annotatedOutput, final short n, final short n2, final short n3, final short n4) {
        annotatedOutput.writeShort(n);
        annotatedOutput.writeShort(n2);
        annotatedOutput.writeShort(n3);
        annotatedOutput.writeShort(n4);
    }
    
    protected static String \u3007080(final DalvInsn dalvInsn) {
        final int \u3007oOO8O8 = ((TargetInsn)dalvInsn).\u3007oOO8O8();
        String s;
        if (\u3007oOO8O8 == (short)\u3007oOO8O8) {
            s = Hex.\u3007o00\u3007\u3007Oo(\u3007oOO8O8);
        }
        else {
            s = Hex.\u3007o\u3007(\u3007oOO8O8);
        }
        return s;
    }
    
    protected static boolean \u30070\u3007O0088o(final int n) {
        return (byte)n == n;
    }
    
    protected static boolean \u30078o8o\u3007(final RegisterSpecList list) {
        final int size = list.size();
        if (size < 2) {
            return true;
        }
        int \u30078o8o\u3007 = list.\u3007O00(0).\u30078o8o\u3007();
        for (int i = 0; i < size; ++i) {
            final RegisterSpec \u3007o00 = list.\u3007O00(i);
            if (\u3007o00.\u30078o8o\u3007() != \u30078o8o\u3007) {
                return false;
            }
            \u30078o8o\u3007 += \u3007o00.oO80();
        }
        return true;
    }
    
    protected static String \u3007O00(final RegisterSpecList list) {
        final int size = list.size();
        final StringBuilder sb = new StringBuilder(size * 5 + 2);
        sb.append('{');
        for (int i = 0; i < size; ++i) {
            if (i != 0) {
                sb.append(", ");
            }
            sb.append(list.\u3007O00(i).\u30070\u3007O0088o());
        }
        sb.append('}');
        return sb.toString();
    }
    
    protected static boolean \u3007O888o0o(final int n) {
        return n == (n & 0xFF);
    }
    
    protected static short \u3007O\u3007(final DalvInsn dalvInsn, final int n) {
        if ((n & 0xFF) != n) {
            throw new IllegalArgumentException("arg out of range 0..255");
        }
        final int oo08 = dalvInsn.\u3007O8o08O().Oo08();
        if ((oo08 & 0xFF) == oo08) {
            return (short)(oo08 | n << 8);
        }
        throw new IllegalArgumentException("opcode out of range 0..255");
    }
    
    protected static void \u3007oOO8O8(final AnnotatedOutput annotatedOutput, final short n, final short n2, final short n3) {
        annotatedOutput.writeShort(n);
        annotatedOutput.writeShort(n2);
        annotatedOutput.writeShort(n3);
    }
    
    protected static boolean \u3007oo\u3007(final int n) {
        return n == (0xFFFF & n);
    }
    
    protected static String \u3007o\u3007(final DalvInsn dalvInsn) {
        final int o8ooOoo\u3007 = ((TargetInsn)dalvInsn).O8ooOoo\u3007();
        String s;
        if (o8ooOoo\u3007 == (char)o8ooOoo\u3007) {
            s = Hex.Oo08(o8ooOoo\u3007);
        }
        else {
            s = Hex.oO80(o8ooOoo\u3007);
        }
        return s;
    }
    
    protected static int \u3007\u3007808\u3007(final int n, final int n2) {
        if ((n & 0xF) != n) {
            throw new IllegalArgumentException("low out of range 0..15");
        }
        if ((n2 & 0xF) == n2) {
            return n | n2 << 4;
        }
        throw new IllegalArgumentException("high out of range 0..15");
    }
    
    protected static String \u3007\u30078O0\u30078(final RegisterSpecList list) {
        final int size = list.size();
        final StringBuilder sb = new StringBuilder(30);
        sb.append("{");
        if (size != 0) {
            if (size != 1) {
                RegisterSpec registerSpec2;
                final RegisterSpec registerSpec = registerSpec2 = list.\u3007O00(size - 1);
                if (registerSpec.oO80() == 2) {
                    registerSpec2 = registerSpec.\u3007O888o0o(1);
                }
                sb.append(list.\u3007O00(0).\u30070\u3007O0088o());
                sb.append("..");
                sb.append(registerSpec2.\u30070\u3007O0088o());
            }
            else {
                sb.append(list.\u3007O00(0).\u30070\u3007O0088o());
            }
        }
        sb.append("}");
        return sb.toString();
    }
    
    public abstract int O8();
    
    public abstract boolean OO0o\u3007\u3007\u3007\u30070(final DalvInsn p0);
    
    public abstract void OOO\u3007O0(final AnnotatedOutput p0, final DalvInsn p1);
    
    public abstract String oO80(final DalvInsn p0);
    
    public abstract String \u300780\u3007808\u3007O(final DalvInsn p0, final boolean p1);
    
    public final String \u3007O8o08O(final DalvInsn dalvInsn, final boolean b) {
        final String \u3007o\u3007 = dalvInsn.\u3007O8o08O().\u3007o\u3007();
        final String oo80 = this.oO80(dalvInsn);
        final String \u300780\u3007808\u3007O = this.\u300780\u3007808\u3007O(dalvInsn, b);
        final StringBuilder sb = new StringBuilder(100);
        sb.append(\u3007o\u3007);
        if (oo80.length() != 0) {
            sb.append(' ');
            sb.append(oo80);
        }
        if (\u300780\u3007808\u3007O.length() != 0) {
            sb.append(" // ");
            sb.append(\u300780\u3007808\u3007O);
        }
        return sb.toString();
    }
    
    public boolean \u3007o00\u3007\u3007Oo(final TargetInsn targetInsn) {
        return false;
    }
    
    public BitSet \u3007\u3007888(final DalvInsn dalvInsn) {
        return new BitSet();
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.android.dx.dex.code;

import com.android.dx.util.FixedSizeList;

public final class CatchTable extends FixedSizeList implements Comparable<CatchTable>
{
    public static final CatchTable OO;
    
    static {
        OO = new CatchTable(0);
    }
    
    public CatchTable(final int n) {
        super(n);
    }
    
    public void \u3007O00(final int n, final Entry entry) {
        this.OO0o\u3007\u3007\u3007\u30070(n, entry);
    }
    
    public Entry \u3007O\u3007(final int n) {
        return (Entry)this.oO80(n);
    }
    
    public int \u3007\u3007808\u3007(final CatchTable catchTable) {
        if (this == catchTable) {
            return 0;
        }
        final int size = this.size();
        final int size2 = catchTable.size();
        for (int min = Math.min(size, size2), i = 0; i < min; ++i) {
            final int \u3007080 = this.\u3007O\u3007(i).\u3007080(catchTable.\u3007O\u3007(i));
            if (\u3007080 != 0) {
                return \u3007080;
            }
        }
        if (size < size2) {
            return -1;
        }
        if (size > size2) {
            return 1;
        }
        return 0;
    }
    
    public static class Entry implements Comparable<Entry>
    {
        private final CatchHandlerList OO;
        private final int o0;
        private final int \u3007OOo8\u30070;
        
        public Entry(final int o0, final int \u3007oOo8\u30070, final CatchHandlerList oo) {
            if (o0 < 0) {
                throw new IllegalArgumentException("start < 0");
            }
            if (\u3007oOo8\u30070 <= o0) {
                throw new IllegalArgumentException("end <= start");
            }
            if (!oo.O8()) {
                this.o0 = o0;
                this.\u3007OOo8\u30070 = \u3007oOo8\u30070;
                this.OO = oo;
                return;
            }
            throw new IllegalArgumentException("handlers.isMutable()");
        }
        
        public int O8() {
            return this.o0;
        }
        
        @Override
        public boolean equals(final Object o) {
            final boolean b = o instanceof Entry;
            boolean b2 = false;
            if (b) {
                b2 = b2;
                if (this.\u3007080((Entry)o) == 0) {
                    b2 = true;
                }
            }
            return b2;
        }
        
        @Override
        public int hashCode() {
            return (this.o0 * 31 + this.\u3007OOo8\u30070) * 31 + this.OO.hashCode();
        }
        
        public int \u3007080(final Entry entry) {
            final int o0 = this.o0;
            final int o2 = entry.o0;
            if (o0 < o2) {
                return -1;
            }
            if (o0 > o2) {
                return 1;
            }
            final int \u3007oOo8\u30070 = this.\u3007OOo8\u30070;
            final int \u3007oOo8\u30072 = entry.\u3007OOo8\u30070;
            if (\u3007oOo8\u30070 < \u3007oOo8\u30072) {
                return -1;
            }
            if (\u3007oOo8\u30070 > \u3007oOo8\u30072) {
                return 1;
            }
            return this.OO.\u3007O\u3007(entry.OO);
        }
        
        public int \u3007o00\u3007\u3007Oo() {
            return this.\u3007OOo8\u30070;
        }
        
        public CatchHandlerList \u3007o\u3007() {
            return this.OO;
        }
    }
}

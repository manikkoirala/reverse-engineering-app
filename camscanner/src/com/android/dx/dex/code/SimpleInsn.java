// 
// Decompiled by Procyon v0.6.0
// 

package com.android.dx.dex.code;

import com.android.dx.rop.code.RegisterSpecList;
import com.android.dx.rop.code.SourcePosition;

public final class SimpleInsn extends FixedSizeInsn
{
    public SimpleInsn(final Dop dop, final SourcePosition sourcePosition, final RegisterSpecList list) {
        super(dop, sourcePosition, list);
    }
    
    @Override
    public DalvInsn oo88o8O(final Dop dop) {
        return new SimpleInsn(dop, this.OO0o\u3007\u3007(), this.Oooo8o0\u3007());
    }
    
    @Override
    public DalvInsn o\u3007O8\u3007\u3007o(final RegisterSpecList list) {
        return new SimpleInsn(this.\u3007O8o08O(), this.OO0o\u3007\u3007(), list);
    }
    
    @Override
    protected String \u3007080() {
        return null;
    }
}

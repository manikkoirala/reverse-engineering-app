// 
// Decompiled by Procyon v0.6.0
// 

package com.android.dx.dex.code;

import com.android.dx.rop.code.PlainInsn;
import com.android.dx.rop.cst.Constant;
import com.android.dx.rop.code.ThrowingCstInsn;
import com.android.dx.rop.code.Rop;
import com.android.dx.rop.type.TypeBearer;
import com.android.dx.rop.code.SourcePosition;
import com.android.dx.rop.code.ThrowingInsn;
import com.android.dx.rop.cst.CstInteger;
import com.android.dx.rop.code.PlainCstInsn;
import com.android.dx.rop.code.RegisterSpecList;
import com.android.dx.rop.code.RegisterSpec;
import com.android.dx.rop.code.Insn;
import com.android.dx.rop.code.BasicBlock;
import com.android.dx.util.IntList;
import com.android.dx.util.Bits;
import com.android.dx.rop.code.BasicBlockList;
import com.android.dx.rop.code.LocalVariableInfo;
import com.android.dx.rop.code.RopMethod;
import com.android.dx.dex.DexOptions;

public final class RopTranslator
{
    private final BlockAddresses O8;
    private final boolean OO0o\u3007\u3007\u3007\u30070;
    private final OutputCollector Oo08;
    private int[] oO80;
    private final TranslationVisitor o\u30070;
    private final DexOptions \u3007080;
    private final int \u300780\u3007808\u3007O;
    private final RopMethod \u3007o00\u3007\u3007Oo;
    private final int \u3007o\u3007;
    private final int \u3007\u3007888;
    
    private RopTranslator(final RopMethod \u3007o00\u3007\u3007Oo, int n, final LocalVariableInfo localVariableInfo, final int \u300780\u3007808\u3007O, final DexOptions \u3007080) {
        this.\u3007080 = \u3007080;
        this.\u3007o00\u3007\u3007Oo = \u3007o00\u3007\u3007Oo;
        this.\u3007o\u3007 = n;
        this.O8 = new BlockAddresses(\u3007o00\u3007\u3007Oo);
        this.\u300780\u3007808\u3007O = \u300780\u3007808\u3007O;
        this.oO80 = null;
        final boolean oo80 = oO80(\u3007o00\u3007\u3007Oo, \u300780\u3007808\u3007O);
        this.OO0o\u3007\u3007\u3007\u30070 = oo80;
        final BasicBlockList \u3007o00\u3007\u3007Oo2 = \u3007o00\u3007\u3007Oo.\u3007o00\u3007\u3007Oo();
        final int n2 = \u3007o00\u3007\u3007Oo2.size() * 3;
        final int \u3007o888o0o = \u3007o00\u3007\u3007Oo2.\u3007O888o0o();
        final int oo88o8O = \u3007o00\u3007\u3007Oo2.oo88o8O();
        if (oo80) {
            n = 0;
        }
        else {
            n = \u300780\u3007808\u3007O;
        }
        n += oo88o8O;
        this.\u3007\u3007888 = n;
        final OutputCollector oo81 = new OutputCollector(\u3007080, n2 + \u3007o888o0o, n2, n, \u300780\u3007808\u3007O);
        this.Oo08 = oo81;
        this.o\u30070 = new TranslationVisitor(oo81);
    }
    
    private void OO0o\u3007\u3007() {
        final BasicBlockList \u3007o00\u3007\u3007Oo = this.\u3007o00\u3007\u3007Oo.\u3007o00\u3007\u3007Oo();
        final int size = \u3007o00\u3007\u3007Oo.size();
        final int \u3007o\u3007 = \u3007o00\u3007\u3007Oo.\u3007O\u3007();
        final int[] oo08 = Bits.Oo08(\u3007o\u3007);
        final int[] oo9 = Bits.Oo08(\u3007o\u3007);
        for (int i = 0; i < size; ++i) {
            Bits.o\u30070(oo08, \u3007o00\u3007\u3007Oo.o800o8O(i).getLabel());
        }
        final int[] oo10 = new int[size];
        int j = this.\u3007o00\u3007\u3007Oo.\u3007o\u3007();
        int n = 0;
        while (j != -1) {
            int label = 0;
            int n3 = 0;
        Label_0182:
            while (true) {
                final IntList o8 = this.\u3007o00\u3007\u3007Oo.O8(j);
                final int size2 = o8.size();
                int n2 = 0;
                while (true) {
                    label = j;
                    n3 = n;
                    if (n2 >= size2) {
                        break Label_0182;
                    }
                    final int \u30078o8o\u3007 = o8.\u30078o8o\u3007(n2);
                    if (Bits.O8(oo9, \u30078o8o\u3007)) {
                        label = j;
                        n3 = n;
                        break Label_0182;
                    }
                    if (Bits.O8(oo08, \u30078o8o\u3007)) {
                        if (\u3007o00\u3007\u3007Oo.\u3007oo\u3007(\u30078o8o\u3007).O8() == j) {
                            Bits.o\u30070(oo9, \u30078o8o\u3007);
                            j = \u30078o8o\u3007;
                            break;
                        }
                    }
                    ++n2;
                }
            }
            while (true) {
                n = n3;
                if (label == -1) {
                    break;
                }
                Bits.\u3007080(oo08, label);
                Bits.\u3007080(oo9, label);
                oo10[n3] = label;
                n = n3 + 1;
                final BasicBlock \u3007oo\u3007 = \u3007o00\u3007\u3007Oo.\u3007oo\u3007(label);
                final BasicBlock o\u3007O8\u3007\u3007o = \u3007o00\u3007\u3007Oo.o\u3007O8\u3007\u3007o(\u3007oo\u3007);
                if (o\u3007O8\u3007\u3007o == null) {
                    break;
                }
                label = o\u3007O8\u3007\u3007o.getLabel();
                final int o9 = \u3007oo\u3007.O8();
                if (Bits.O8(oo08, label)) {
                    n3 = n;
                }
                else if (o9 != label && o9 >= 0 && Bits.O8(oo08, o9)) {
                    label = o9;
                    n3 = n;
                }
                else {
                    final IntList o\u30070 = \u3007oo\u3007.o\u30070();
                    for (int size3 = o\u30070.size(), k = 0; k < size3; ++k) {
                        final int \u30078o8o\u30072 = o\u30070.\u30078o8o\u3007(k);
                        if (Bits.O8(oo08, \u30078o8o\u30072)) {
                            label = \u30078o8o\u30072;
                            n3 = n;
                            continue Label_0182;
                        }
                    }
                    label = -1;
                    n3 = n;
                }
            }
            j = Bits.\u3007o\u3007(oo08, 0);
        }
        if (n == size) {
            this.oO80 = oo10;
            return;
        }
        throw new RuntimeException("shouldn't happen");
    }
    
    private static RegisterSpecList OO0o\u3007\u3007\u3007\u30070(final Insn insn, final RegisterSpec registerSpec) {
        RegisterSpecList list2;
        final RegisterSpecList list = list2 = insn.OO0o\u3007\u3007\u3007\u30070();
        if (insn.\u3007\u3007888().o\u30070()) {
            list2 = list;
            if (list.size() == 2) {
                list2 = list;
                if (registerSpec.\u30078o8o\u3007() == list.\u3007O00(1).\u30078o8o\u3007()) {
                    list2 = RegisterSpecList.OoO8(list.\u3007O00(1), list.\u3007O00(0));
                }
            }
        }
        if (registerSpec == null) {
            return list2;
        }
        return list2.o\u3007O8\u3007\u3007o(registerSpec);
    }
    
    public static DalvCode Oooo8o0\u3007(final RopMethod ropMethod, final int n, final LocalVariableInfo localVariableInfo, final int n2, final DexOptions dexOptions) {
        return new RopTranslator(ropMethod, n, localVariableInfo, n2, dexOptions).\u3007\u3007808\u3007();
    }
    
    private static boolean oO80(final RopMethod ropMethod, final int n) {
        final boolean[] array = { true };
        ropMethod.\u3007o00\u3007\u3007Oo().OoO8(new Insn.BaseVisitor(array, ropMethod.\u3007o00\u3007\u3007Oo().oo88o8O(), n) {
            final boolean[] \u3007080;
            final int \u3007o00\u3007\u3007Oo;
            final int \u3007o\u3007;
            
            @Override
            public void \u3007080(final PlainCstInsn plainCstInsn) {
                if (plainCstInsn.\u3007\u3007888().O8() == 3) {
                    final int \u300780\u3007808\u3007O = ((CstInteger)plainCstInsn.OO0o\u3007\u3007()).\u300780\u3007808\u3007O();
                    final boolean[] \u3007080 = this.\u3007080;
                    \u3007080[0] = (\u3007080[0] && this.\u3007o00\u3007\u3007Oo - this.\u3007o\u3007 + \u300780\u3007808\u3007O == plainCstInsn.\u300780\u3007808\u3007O().\u30078o8o\u3007());
                }
            }
        });
        return array[0];
    }
    
    private static RegisterSpecList \u300780\u3007808\u3007O(final Insn insn) {
        return OO0o\u3007\u3007\u3007\u30070(insn, insn.\u300780\u3007808\u3007O());
    }
    
    private void \u30078o8o\u3007(final BasicBlock basicBlock, final int n) {
        this.Oo08.\u3007080(this.O8.O8(basicBlock));
        this.o\u30070.\u3007\u3007888(basicBlock, this.O8.\u3007o00\u3007\u3007Oo(basicBlock));
        basicBlock.\u3007o00\u3007\u3007Oo().\u3007\u3007808\u3007(this.o\u30070);
        this.Oo08.\u3007080(this.O8.\u3007080(basicBlock));
        final int o8 = basicBlock.O8();
        final Insn \u3007o\u3007 = basicBlock.\u3007o\u3007();
        if (o8 >= 0 && o8 != n) {
            if (\u3007o\u3007.\u3007\u3007888().\u3007o00\u3007\u3007Oo() == 4 && basicBlock.Oo08() == n) {
                this.Oo08.O8(1, this.O8.\u3007o\u3007(o8));
            }
            else {
                this.Oo08.\u3007080(new TargetInsn(Dops.\u3007\u30070o, \u3007o\u3007.oO80(), RegisterSpecList.OO, this.O8.\u3007o\u3007(o8)));
            }
        }
    }
    
    private void \u3007O8o08O() {
        final BasicBlockList \u3007o00\u3007\u3007Oo = this.\u3007o00\u3007\u3007Oo.\u3007o00\u3007\u3007Oo();
        final int[] oo80 = this.oO80;
        int n;
        for (int length = oo80.length, i = 0; i < length; i = n) {
            n = i + 1;
            int n2;
            if (n == oo80.length) {
                n2 = -1;
            }
            else {
                n2 = oo80[n];
            }
            this.\u30078o8o\u3007(\u3007o00\u3007\u3007Oo.\u3007oo\u3007(oo80[i]), n2);
        }
    }
    
    private DalvCode \u3007\u3007808\u3007() {
        this.OO0o\u3007\u3007();
        this.\u3007O8o08O();
        return new DalvCode(this.\u3007o\u3007, this.Oo08.\u3007o\u3007(), new StdCatchBuilder(this.\u3007o00\u3007\u3007Oo, this.oO80, this.O8));
    }
    
    private class TranslationVisitor implements Visitor
    {
        final RopTranslator O8;
        private final OutputCollector \u3007080;
        private BasicBlock \u3007o00\u3007\u3007Oo;
        private CodeAddress \u3007o\u3007;
        
        public TranslationVisitor(final RopTranslator o8, final OutputCollector \u3007080) {
            this.O8 = o8;
            this.\u3007080 = \u3007080;
        }
        
        private RegisterSpec o\u30070() {
            final int o8 = this.\u3007o00\u3007\u3007Oo.O8();
            if (o8 < 0) {
                return null;
            }
            final Insn \u3007o\u3007 = this.O8.\u3007o00\u3007\u3007Oo.\u3007o00\u3007\u3007Oo().\u3007oo\u3007(o8).\u3007o00\u3007\u3007Oo().\u3007O\u3007(0);
            if (\u3007o\u3007.\u3007\u3007888().O8() != 56) {
                return null;
            }
            return \u3007o\u3007.\u300780\u3007808\u3007O();
        }
        
        @Override
        public void O8(final ThrowingInsn obj) {
            final SourcePosition oo80 = obj.oO80();
            final Dop \u3007080 = RopToDop.\u3007080(obj);
            if (obj.\u3007\u3007888().\u3007o00\u3007\u3007Oo() != 6) {
                throw new RuntimeException("shouldn't happen");
            }
            final RegisterSpec o\u30070 = this.o\u30070();
            if (\u3007080.\u3007\u3007888() == (o\u30070 != null)) {
                this.Oo08(this.\u3007o\u3007);
                this.Oo08(new SimpleInsn(\u3007080, oo80, OO0o\u3007\u3007\u3007\u30070(obj, o\u30070)));
                return;
            }
            final StringBuilder sb = new StringBuilder();
            sb.append("Insn with result/move-result-pseudo mismatch");
            sb.append(obj);
            throw new RuntimeException(sb.toString());
        }
        
        protected void Oo08(final DalvInsn dalvInsn) {
            this.\u3007080.\u3007080(dalvInsn);
        }
        
        @Override
        public void \u3007080(final PlainCstInsn plainCstInsn) {
            final SourcePosition oo80 = plainCstInsn.oO80();
            final Dop \u3007080 = RopToDop.\u3007080(plainCstInsn);
            final Rop \u3007\u3007888 = plainCstInsn.\u3007\u3007888();
            final int o8 = \u3007\u3007888.O8();
            if (\u3007\u3007888.\u3007o00\u3007\u3007Oo() == 1) {
                if (o8 == 3) {
                    if (!this.O8.OO0o\u3007\u3007\u3007\u30070) {
                        final RegisterSpec \u300780\u3007808\u3007O = plainCstInsn.\u300780\u3007808\u3007O();
                        this.Oo08(new SimpleInsn(\u3007080, oo80, RegisterSpecList.OoO8(\u300780\u3007808\u3007O, RegisterSpec.\u3007O\u3007(this.O8.\u3007\u3007888 - this.O8.\u300780\u3007808\u3007O + ((CstInteger)plainCstInsn.OO0o\u3007\u3007()).\u300780\u3007808\u3007O(), \u300780\u3007808\u3007O.getType()))));
                    }
                }
                else {
                    this.Oo08(new CstInsn(\u3007080, oo80, \u300780\u3007808\u3007O(plainCstInsn), plainCstInsn.OO0o\u3007\u3007()));
                }
                return;
            }
            throw new RuntimeException("shouldn't happen");
        }
        
        @Override
        public void \u3007o00\u3007\u3007Oo(final ThrowingCstInsn obj) {
            final SourcePosition oo80 = obj.oO80();
            final Dop \u3007080 = RopToDop.\u3007080(obj);
            final Rop \u3007\u3007888 = obj.\u3007\u3007888();
            final Constant oo0o\u3007\u3007 = obj.OO0o\u3007\u3007();
            if (\u3007\u3007888.\u3007o00\u3007\u3007Oo() == 6) {
                this.Oo08(this.\u3007o\u3007);
                if (\u3007\u3007888.Oo08()) {
                    this.Oo08(new CstInsn(\u3007080, oo80, obj.OO0o\u3007\u3007\u3007\u30070(), oo0o\u3007\u3007));
                }
                else {
                    final RegisterSpec o\u30070 = this.o\u30070();
                    final RegisterSpecList \u3007\u3007889 = OO0o\u3007\u3007\u3007\u30070(obj, o\u30070);
                    final boolean \u3007\u3007890 = \u3007080.\u3007\u3007888();
                    int n = false ? 1 : 0;
                    final boolean b = \u3007\u3007890 || \u3007\u3007888.O8() == 43;
                    if (o\u30070 != null) {
                        n = (true ? 1 : 0);
                    }
                    if ((b ? 1 : 0) != n) {
                        final StringBuilder sb = new StringBuilder();
                        sb.append("Insn with result/move-result-pseudo mismatch ");
                        sb.append(obj);
                        throw new RuntimeException(sb.toString());
                    }
                    FixedSizeInsn fixedSizeInsn;
                    if (\u3007\u3007888.O8() == 41 && \u3007080.Oo08() != 35) {
                        fixedSizeInsn = new SimpleInsn(\u3007080, oo80, \u3007\u3007889);
                    }
                    else {
                        fixedSizeInsn = new CstInsn(\u3007080, oo80, \u3007\u3007889, oo0o\u3007\u3007);
                    }
                    this.Oo08(fixedSizeInsn);
                }
                return;
            }
            final StringBuilder sb2 = new StringBuilder();
            sb2.append("Expected BRANCH_THROW got ");
            sb2.append(\u3007\u3007888.\u3007o00\u3007\u3007Oo());
            throw new RuntimeException(sb2.toString());
        }
        
        @Override
        public void \u3007o\u3007(final PlainInsn plainInsn) {
            final Rop \u3007\u3007888 = plainInsn.\u3007\u3007888();
            if (\u3007\u3007888.O8() == 54) {
                return;
            }
            if (\u3007\u3007888.O8() == 56) {
                return;
            }
            final SourcePosition oo80 = plainInsn.oO80();
            final Dop \u3007080 = RopToDop.\u3007080(plainInsn);
            final int \u3007o00\u3007\u3007Oo = \u3007\u3007888.\u3007o00\u3007\u3007Oo();
            FixedSizeInsn fixedSizeInsn = null;
            Label_0141: {
                if (\u3007o00\u3007\u3007Oo != 1 && \u3007o00\u3007\u3007Oo != 2) {
                    if (\u3007o00\u3007\u3007Oo == 3) {
                        return;
                    }
                    if (\u3007o00\u3007\u3007Oo == 4) {
                        fixedSizeInsn = new TargetInsn(\u3007080, oo80, \u300780\u3007808\u3007O(plainInsn), this.O8.O8.\u3007o\u3007(this.\u3007o00\u3007\u3007Oo.o\u30070().\u30078o8o\u3007(1)));
                        break Label_0141;
                    }
                    if (\u3007o00\u3007\u3007Oo != 6) {
                        throw new RuntimeException("shouldn't happen");
                    }
                }
                fixedSizeInsn = new SimpleInsn(\u3007080, oo80, \u300780\u3007808\u3007O(plainInsn));
            }
            this.Oo08(fixedSizeInsn);
        }
        
        public void \u3007\u3007888(final BasicBlock \u3007o00\u3007\u3007Oo, final CodeAddress \u3007o\u3007) {
            this.\u3007o00\u3007\u3007Oo = \u3007o00\u3007\u3007Oo;
            this.\u3007o\u3007 = \u3007o\u3007;
        }
    }
}

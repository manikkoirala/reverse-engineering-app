// 
// Decompiled by Procyon v0.6.0
// 

package com.android.dx.dex.code.form;

import java.util.BitSet;
import com.android.dx.util.AnnotatedOutput;
import com.android.dx.rop.code.RegisterSpecList;
import com.android.dx.dex.code.TargetInsn;
import com.android.dx.dex.code.DalvInsn;
import com.android.dx.dex.code.InsnFormat;

public final class Form31t extends InsnFormat
{
    public static final InsnFormat \u3007080;
    
    static {
        \u3007080 = new Form31t();
    }
    
    private Form31t() {
    }
    
    @Override
    public int O8() {
        return 3;
    }
    
    @Override
    public boolean OO0o\u3007\u3007\u3007\u30070(final DalvInsn dalvInsn) {
        final RegisterSpecList oooo8o0\u3007 = dalvInsn.Oooo8o0\u3007();
        return dalvInsn instanceof TargetInsn && oooo8o0\u3007.size() == 1 && InsnFormat.\u3007O888o0o(oooo8o0\u3007.\u3007O00(0).\u30078o8o\u3007());
    }
    
    @Override
    public void OOO\u3007O0(final AnnotatedOutput annotatedOutput, final DalvInsn dalvInsn) {
        InsnFormat.\u300700(annotatedOutput, InsnFormat.\u3007O\u3007(dalvInsn, dalvInsn.Oooo8o0\u3007().\u3007O00(0).\u30078o8o\u3007()), ((TargetInsn)dalvInsn).\u3007oOO8O8());
    }
    
    @Override
    public String oO80(final DalvInsn dalvInsn) {
        final RegisterSpecList oooo8o0\u3007 = dalvInsn.Oooo8o0\u3007();
        final StringBuilder sb = new StringBuilder();
        sb.append(oooo8o0\u3007.\u3007O00(0).\u30070\u3007O0088o());
        sb.append(", ");
        sb.append(InsnFormat.\u3007o\u3007(dalvInsn));
        return sb.toString();
    }
    
    @Override
    public String \u300780\u3007808\u3007O(final DalvInsn dalvInsn, final boolean b) {
        return InsnFormat.\u3007080(dalvInsn);
    }
    
    @Override
    public boolean \u3007o00\u3007\u3007Oo(final TargetInsn targetInsn) {
        return true;
    }
    
    @Override
    public BitSet \u3007\u3007888(final DalvInsn dalvInsn) {
        final RegisterSpecList oooo8o0\u3007 = dalvInsn.Oooo8o0\u3007();
        final BitSet set = new BitSet(1);
        set.set(0, InsnFormat.\u3007O888o0o(oooo8o0\u3007.\u3007O00(0).\u30078o8o\u3007()));
        return set;
    }
}

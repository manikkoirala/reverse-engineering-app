// 
// Decompiled by Procyon v0.6.0
// 

package com.android.dx.dex.code.form;

import java.util.BitSet;
import com.android.dx.util.AnnotatedOutput;
import com.android.dx.rop.cst.Constant;
import com.android.dx.rop.code.RegisterSpecList;
import com.android.dx.rop.cst.CstLiteralBits;
import com.android.dx.dex.code.CstInsn;
import com.android.dx.dex.code.DalvInsn;
import com.android.dx.dex.code.InsnFormat;

public final class Form21h extends InsnFormat
{
    public static final InsnFormat \u3007080;
    
    static {
        \u3007080 = new Form21h();
    }
    
    private Form21h() {
    }
    
    @Override
    public int O8() {
        return 2;
    }
    
    @Override
    public boolean OO0o\u3007\u3007\u3007\u30070(final DalvInsn dalvInsn) {
        final RegisterSpecList oooo8o0\u3007 = dalvInsn.Oooo8o0\u3007();
        final boolean b = dalvInsn instanceof CstInsn;
        final boolean b2 = false;
        final boolean b3 = false;
        boolean b4 = b2;
        if (b) {
            b4 = b2;
            if (oooo8o0\u3007.size() == 1) {
                if (!InsnFormat.\u3007O888o0o(oooo8o0\u3007.\u3007O00(0).\u30078o8o\u3007())) {
                    b4 = b2;
                }
                else {
                    final Constant o\u30078O8\u3007008 = ((CstInsn)dalvInsn).O\u30078O8\u3007008();
                    if (!(o\u30078O8\u3007008 instanceof CstLiteralBits)) {
                        return false;
                    }
                    final CstLiteralBits cstLiteralBits = (CstLiteralBits)o\u30078O8\u3007008;
                    if (oooo8o0\u3007.\u3007O00(0).oO80() == 1) {
                        boolean b5 = b3;
                        if ((cstLiteralBits.\u3007\u3007888() & 0xFFFF) == 0x0) {
                            b5 = true;
                        }
                        return b5;
                    }
                    b4 = b2;
                    if ((cstLiteralBits.oO80() & 0xFFFFFFFFFFFFL) == 0x0L) {
                        b4 = true;
                    }
                }
            }
        }
        return b4;
    }
    
    @Override
    public void OOO\u3007O0(final AnnotatedOutput annotatedOutput, final DalvInsn dalvInsn) {
        final RegisterSpecList oooo8o0\u3007 = dalvInsn.Oooo8o0\u3007();
        final CstLiteralBits cstLiteralBits = (CstLiteralBits)((CstInsn)dalvInsn).O\u30078O8\u3007008();
        int n;
        if (oooo8o0\u3007.\u3007O00(0).oO80() == 1) {
            n = cstLiteralBits.\u3007\u3007888() >>> 16;
        }
        else {
            n = (int)(cstLiteralBits.oO80() >>> 48);
        }
        InsnFormat.O8ooOoo\u3007(annotatedOutput, InsnFormat.\u3007O\u3007(dalvInsn, oooo8o0\u3007.\u3007O00(0).\u30078o8o\u3007()), (short)n);
    }
    
    @Override
    public String oO80(final DalvInsn dalvInsn) {
        final RegisterSpecList oooo8o0\u3007 = dalvInsn.Oooo8o0\u3007();
        final CstLiteralBits cstLiteralBits = (CstLiteralBits)((CstInsn)dalvInsn).O\u30078O8\u3007008();
        final StringBuilder sb = new StringBuilder();
        sb.append(oooo8o0\u3007.\u3007O00(0).\u30070\u3007O0088o());
        sb.append(", ");
        sb.append(InsnFormat.Oooo8o0\u3007(cstLiteralBits));
        return sb.toString();
    }
    
    @Override
    public String \u300780\u3007808\u3007O(final DalvInsn dalvInsn, final boolean b) {
        final RegisterSpecList oooo8o0\u3007 = dalvInsn.Oooo8o0\u3007();
        final CstLiteralBits cstLiteralBits = (CstLiteralBits)((CstInsn)dalvInsn).O\u30078O8\u3007008();
        int n;
        if (oooo8o0\u3007.\u3007O00(0).oO80() == 1) {
            n = 32;
        }
        else {
            n = 64;
        }
        return InsnFormat.OO0o\u3007\u3007(cstLiteralBits, n);
    }
    
    @Override
    public BitSet \u3007\u3007888(final DalvInsn dalvInsn) {
        final RegisterSpecList oooo8o0\u3007 = dalvInsn.Oooo8o0\u3007();
        final BitSet set = new BitSet(1);
        set.set(0, InsnFormat.\u3007O888o0o(oooo8o0\u3007.\u3007O00(0).\u30078o8o\u3007()));
        return set;
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.android.dx.dex.code.form;

import com.android.dx.util.AnnotatedOutput;
import com.android.dx.dex.code.SimpleInsn;
import com.android.dx.dex.code.DalvInsn;
import com.android.dx.dex.code.InsnFormat;

public final class Form10x extends InsnFormat
{
    public static final InsnFormat \u3007080;
    
    static {
        \u3007080 = new Form10x();
    }
    
    private Form10x() {
    }
    
    @Override
    public int O8() {
        return 1;
    }
    
    @Override
    public boolean OO0o\u3007\u3007\u3007\u30070(final DalvInsn dalvInsn) {
        return dalvInsn instanceof SimpleInsn && dalvInsn.Oooo8o0\u3007().size() == 0;
    }
    
    @Override
    public void OOO\u3007O0(final AnnotatedOutput annotatedOutput, final DalvInsn dalvInsn) {
        InsnFormat.o\u3007O8\u3007\u3007o(annotatedOutput, InsnFormat.\u3007O\u3007(dalvInsn, 0));
    }
    
    @Override
    public String oO80(final DalvInsn dalvInsn) {
        return "";
    }
    
    @Override
    public String \u300780\u3007808\u3007O(final DalvInsn dalvInsn, final boolean b) {
        return "";
    }
}

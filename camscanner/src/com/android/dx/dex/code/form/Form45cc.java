// 
// Decompiled by Procyon v0.6.0
// 

package com.android.dx.dex.code.form;

import java.util.BitSet;
import com.android.dx.util.AnnotatedOutput;
import com.android.dx.rop.cst.CstProtoRef;
import com.android.dx.rop.cst.CstMethodRef;
import com.android.dx.dex.code.MultiCstInsn;
import com.android.dx.dex.code.DalvInsn;
import com.android.dx.rop.type.TypeBearer;
import com.android.dx.rop.type.Type;
import com.android.dx.rop.code.RegisterSpec;
import com.android.dx.rop.code.RegisterSpecList;
import com.android.dx.dex.code.InsnFormat;

public final class Form45cc extends InsnFormat
{
    public static final InsnFormat \u3007080;
    
    static {
        \u3007080 = new Form45cc();
    }
    
    private Form45cc() {
    }
    
    private static int O8\u3007o(final RegisterSpecList list) {
        final int size = list.size();
        final int n = -1;
        if (size > 5) {
            return -1;
        }
        int i = 0;
        int n2 = 0;
        while (i < size) {
            final RegisterSpec \u3007o00 = list.\u3007O00(i);
            n2 += \u3007o00.oO80();
            if (!InsnFormat.oo88o8O(\u3007o00.\u30078o8o\u3007() + \u3007o00.oO80() - 1)) {
                return -1;
            }
            ++i;
        }
        int n3 = n;
        if (n2 <= 5) {
            n3 = n2;
        }
        return n3;
    }
    
    private static RegisterSpecList oo\u3007(final RegisterSpecList list) {
        final int o8\u3007o = O8\u3007o(list);
        final int size = list.size();
        if (o8\u3007o == size) {
            return list;
        }
        final RegisterSpecList list2 = new RegisterSpecList(o8\u3007o);
        int i = 0;
        int n = 0;
        while (i < size) {
            final RegisterSpec \u3007o00 = list.\u3007O00(i);
            list2.\u3007O888o0o(n, \u3007o00);
            if (\u3007o00.oO80() == 2) {
                list2.\u3007O888o0o(n + 1, RegisterSpec.\u3007O\u3007(\u3007o00.\u30078o8o\u3007() + 1, Type.o8oOOo));
                n += 2;
            }
            else {
                ++n;
            }
            ++i;
        }
        list2.Oo08();
        return list2;
    }
    
    @Override
    public int O8() {
        return 4;
    }
    
    @Override
    public boolean OO0o\u3007\u3007\u3007\u30070(final DalvInsn dalvInsn) {
        final boolean b = dalvInsn instanceof MultiCstInsn;
        final boolean b2 = false;
        if (!b) {
            return false;
        }
        final MultiCstInsn multiCstInsn = (MultiCstInsn)dalvInsn;
        if (multiCstInsn.\u3007oOO8O8() != 2) {
            return false;
        }
        final int o8ooOoo\u3007 = multiCstInsn.O8ooOoo\u3007(0);
        final int o8ooOoo\u30072 = multiCstInsn.O8ooOoo\u3007(1);
        boolean b3 = b2;
        if (InsnFormat.\u3007oo\u3007(o8ooOoo\u3007)) {
            if (!InsnFormat.\u3007oo\u3007(o8ooOoo\u30072)) {
                b3 = b2;
            }
            else {
                if (!(multiCstInsn.O\u30078O8\u3007008(0) instanceof CstMethodRef)) {
                    return false;
                }
                if (!(multiCstInsn.O\u30078O8\u3007008(1) instanceof CstProtoRef)) {
                    return false;
                }
                b3 = b2;
                if (O8\u3007o(multiCstInsn.Oooo8o0\u3007()) >= 0) {
                    b3 = true;
                }
            }
        }
        return b3;
    }
    
    @Override
    public void OOO\u3007O0(final AnnotatedOutput annotatedOutput, final DalvInsn dalvInsn) {
        final MultiCstInsn multiCstInsn = (MultiCstInsn)dalvInsn;
        int \u30078o8o\u3007 = 0;
        final short n = (short)multiCstInsn.O8ooOoo\u3007(0);
        final short n2 = (short)multiCstInsn.O8ooOoo\u3007(1);
        final RegisterSpecList oo\u3007 = oo\u3007(dalvInsn.Oooo8o0\u3007());
        final int size = oo\u3007.size();
        int \u30078o8o\u30072;
        if (size > 0) {
            \u30078o8o\u30072 = oo\u3007.\u3007O00(0).\u30078o8o\u3007();
        }
        else {
            \u30078o8o\u30072 = 0;
        }
        int \u30078o8o\u30073;
        if (size > 1) {
            \u30078o8o\u30073 = oo\u3007.\u3007O00(1).\u30078o8o\u3007();
        }
        else {
            \u30078o8o\u30073 = 0;
        }
        int \u30078o8o\u30074;
        if (size > 2) {
            \u30078o8o\u30074 = oo\u3007.\u3007O00(2).\u30078o8o\u3007();
        }
        else {
            \u30078o8o\u30074 = 0;
        }
        int \u30078o8o\u30075;
        if (size > 3) {
            \u30078o8o\u30075 = oo\u3007.\u3007O00(3).\u30078o8o\u3007();
        }
        else {
            \u30078o8o\u30075 = 0;
        }
        if (size > 4) {
            \u30078o8o\u3007 = oo\u3007.\u3007O00(4).\u30078o8o\u3007();
        }
        InsnFormat.\u30070000OOO(annotatedOutput, InsnFormat.\u3007O\u3007(dalvInsn, InsnFormat.\u3007\u3007808\u3007(\u30078o8o\u3007, size)), n, InsnFormat.o\u30070(\u30078o8o\u30072, \u30078o8o\u30073, \u30078o8o\u30074, \u30078o8o\u30075), n2);
    }
    
    @Override
    public String oO80(final DalvInsn dalvInsn) {
        final RegisterSpecList oo\u3007 = oo\u3007(dalvInsn.Oooo8o0\u3007());
        final StringBuilder sb = new StringBuilder();
        sb.append(InsnFormat.\u3007O00(oo\u3007));
        sb.append(", ");
        sb.append(dalvInsn.O8());
        return sb.toString();
    }
    
    @Override
    public String \u300780\u3007808\u3007O(final DalvInsn dalvInsn, final boolean b) {
        if (b) {
            return dalvInsn.\u3007o\u3007();
        }
        return "";
    }
    
    @Override
    public BitSet \u3007\u3007888(final DalvInsn dalvInsn) {
        final RegisterSpecList oooo8o0\u3007 = dalvInsn.Oooo8o0\u3007();
        final int size = oooo8o0\u3007.size();
        final BitSet set = new BitSet(size);
        for (int i = 0; i < size; ++i) {
            final RegisterSpec \u3007o00 = oooo8o0\u3007.\u3007O00(i);
            set.set(i, InsnFormat.oo88o8O(\u3007o00.\u30078o8o\u3007() + \u3007o00.oO80() - 1));
        }
        return set;
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.android.dx.dex.code.form;

import java.util.BitSet;
import com.android.dx.util.AnnotatedOutput;
import com.android.dx.rop.code.RegisterSpecList;
import com.android.dx.dex.code.TargetInsn;
import com.android.dx.dex.code.DalvInsn;
import com.android.dx.dex.code.InsnFormat;

public final class Form22t extends InsnFormat
{
    public static final InsnFormat \u3007080;
    
    static {
        \u3007080 = new Form22t();
    }
    
    private Form22t() {
    }
    
    @Override
    public int O8() {
        return 2;
    }
    
    @Override
    public boolean OO0o\u3007\u3007\u3007\u30070(final DalvInsn dalvInsn) {
        final RegisterSpecList oooo8o0\u3007 = dalvInsn.Oooo8o0\u3007();
        if (dalvInsn instanceof TargetInsn && oooo8o0\u3007.size() == 2 && InsnFormat.oo88o8O(oooo8o0\u3007.\u3007O00(0).\u30078o8o\u3007())) {
            boolean \u3007o00\u3007\u3007Oo = true;
            if (InsnFormat.oo88o8O(oooo8o0\u3007.\u3007O00(1).\u30078o8o\u3007())) {
                final TargetInsn targetInsn = (TargetInsn)dalvInsn;
                if (targetInsn.\u30070000OOO()) {
                    \u3007o00\u3007\u3007Oo = this.\u3007o00\u3007\u3007Oo(targetInsn);
                }
                return \u3007o00\u3007\u3007Oo;
            }
        }
        return false;
    }
    
    @Override
    public void OOO\u3007O0(final AnnotatedOutput annotatedOutput, final DalvInsn dalvInsn) {
        final RegisterSpecList oooo8o0\u3007 = dalvInsn.Oooo8o0\u3007();
        InsnFormat.O8ooOoo\u3007(annotatedOutput, InsnFormat.\u3007O\u3007(dalvInsn, InsnFormat.\u3007\u3007808\u3007(oooo8o0\u3007.\u3007O00(0).\u30078o8o\u3007(), oooo8o0\u3007.\u3007O00(1).\u30078o8o\u3007())), (short)((TargetInsn)dalvInsn).\u3007oOO8O8());
    }
    
    @Override
    public String oO80(final DalvInsn dalvInsn) {
        final RegisterSpecList oooo8o0\u3007 = dalvInsn.Oooo8o0\u3007();
        final StringBuilder sb = new StringBuilder();
        sb.append(oooo8o0\u3007.\u3007O00(0).\u30070\u3007O0088o());
        sb.append(", ");
        sb.append(oooo8o0\u3007.\u3007O00(1).\u30070\u3007O0088o());
        sb.append(", ");
        sb.append(InsnFormat.\u3007o\u3007(dalvInsn));
        return sb.toString();
    }
    
    @Override
    public String \u300780\u3007808\u3007O(final DalvInsn dalvInsn, final boolean b) {
        return InsnFormat.\u3007080(dalvInsn);
    }
    
    @Override
    public boolean \u3007o00\u3007\u3007Oo(final TargetInsn targetInsn) {
        final int \u3007oOO8O8 = targetInsn.\u3007oOO8O8();
        return \u3007oOO8O8 != 0 && InsnFormat.o800o8O(\u3007oOO8O8);
    }
    
    @Override
    public BitSet \u3007\u3007888(final DalvInsn dalvInsn) {
        final RegisterSpecList oooo8o0\u3007 = dalvInsn.Oooo8o0\u3007();
        final BitSet set = new BitSet(2);
        set.set(0, InsnFormat.oo88o8O(oooo8o0\u3007.\u3007O00(0).\u30078o8o\u3007()));
        set.set(1, InsnFormat.oo88o8O(oooo8o0\u3007.\u3007O00(1).\u30078o8o\u3007()));
        return set;
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.android.dx.dex.code.form;

import com.android.dx.util.AnnotatedOutput;
import com.android.dx.rop.code.RegisterSpecList;
import com.android.dx.rop.cst.Constant;
import com.android.dx.rop.cst.CstCallSiteRef;
import com.android.dx.rop.cst.CstType;
import com.android.dx.rop.cst.CstMethodRef;
import com.android.dx.dex.code.CstInsn;
import com.android.dx.dex.code.DalvInsn;
import com.android.dx.dex.code.InsnFormat;

public final class Form3rc extends InsnFormat
{
    public static final InsnFormat \u3007080;
    
    static {
        \u3007080 = new Form3rc();
    }
    
    private Form3rc() {
    }
    
    @Override
    public int O8() {
        return 3;
    }
    
    @Override
    public boolean OO0o\u3007\u3007\u3007\u30070(final DalvInsn dalvInsn) {
        final boolean b = dalvInsn instanceof CstInsn;
        final boolean b2 = false;
        if (!b) {
            return false;
        }
        final CstInsn cstInsn = (CstInsn)dalvInsn;
        final int o8ooOoo\u3007 = cstInsn.O8ooOoo\u3007();
        final Constant o\u30078O8\u3007008 = cstInsn.O\u30078O8\u3007008();
        if (!InsnFormat.\u3007oo\u3007(o8ooOoo\u3007)) {
            return false;
        }
        if (!(o\u30078O8\u3007008 instanceof CstMethodRef) && !(o\u30078O8\u3007008 instanceof CstType) && !(o\u30078O8\u3007008 instanceof CstCallSiteRef)) {
            return false;
        }
        final RegisterSpecList oooo8o0\u3007 = cstInsn.Oooo8o0\u3007();
        oooo8o0\u3007.size();
        if (oooo8o0\u3007.size() != 0) {
            boolean b3 = b2;
            if (!InsnFormat.\u30078o8o\u3007(oooo8o0\u3007)) {
                return b3;
            }
            b3 = b2;
            if (!InsnFormat.\u3007oo\u3007(oooo8o0\u3007.\u3007O00(0).\u30078o8o\u3007())) {
                return b3;
            }
            b3 = b2;
            if (!InsnFormat.\u3007O888o0o(oooo8o0\u3007.\u3007\u30078O0\u30078())) {
                return b3;
            }
        }
        return true;
    }
    
    @Override
    public void OOO\u3007O0(final AnnotatedOutput annotatedOutput, final DalvInsn dalvInsn) {
        final RegisterSpecList oooo8o0\u3007 = dalvInsn.Oooo8o0\u3007();
        final int o8ooOoo\u3007 = ((CstInsn)dalvInsn).O8ooOoo\u3007();
        final int size = oooo8o0\u3007.size();
        int \u30078o8o\u3007 = 0;
        if (size != 0) {
            \u30078o8o\u3007 = oooo8o0\u3007.\u3007O00(0).\u30078o8o\u3007();
        }
        InsnFormat.\u3007oOO8O8(annotatedOutput, InsnFormat.\u3007O\u3007(dalvInsn, oooo8o0\u3007.\u3007\u30078O0\u30078()), (short)o8ooOoo\u3007, (short)\u30078o8o\u3007);
    }
    
    @Override
    public String oO80(final DalvInsn dalvInsn) {
        final StringBuilder sb = new StringBuilder();
        sb.append(InsnFormat.\u3007\u30078O0\u30078(dalvInsn.Oooo8o0\u3007()));
        sb.append(", ");
        sb.append(dalvInsn.O8());
        return sb.toString();
    }
    
    @Override
    public String \u300780\u3007808\u3007O(final DalvInsn dalvInsn, final boolean b) {
        if (b) {
            return dalvInsn.\u3007o\u3007();
        }
        return "";
    }
}

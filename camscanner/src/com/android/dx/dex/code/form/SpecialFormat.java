// 
// Decompiled by Procyon v0.6.0
// 

package com.android.dx.dex.code.form;

import com.android.dx.util.AnnotatedOutput;
import com.android.dx.dex.code.DalvInsn;
import com.android.dx.dex.code.InsnFormat;

public final class SpecialFormat extends InsnFormat
{
    public static final InsnFormat \u3007080;
    
    static {
        \u3007080 = new SpecialFormat();
    }
    
    private SpecialFormat() {
    }
    
    @Override
    public int O8() {
        throw new RuntimeException("unsupported");
    }
    
    @Override
    public boolean OO0o\u3007\u3007\u3007\u30070(final DalvInsn dalvInsn) {
        return true;
    }
    
    @Override
    public void OOO\u3007O0(final AnnotatedOutput annotatedOutput, final DalvInsn dalvInsn) {
        throw new RuntimeException("unsupported");
    }
    
    @Override
    public String oO80(final DalvInsn dalvInsn) {
        throw new RuntimeException("unsupported");
    }
    
    @Override
    public String \u300780\u3007808\u3007O(final DalvInsn dalvInsn, final boolean b) {
        throw new RuntimeException("unsupported");
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.android.dx.dex.code.form;

import java.util.BitSet;
import com.android.dx.util.AnnotatedOutput;
import com.android.dx.rop.cst.Constant;
import com.android.dx.rop.code.RegisterSpecList;
import com.android.dx.rop.cst.CstLiteralBits;
import com.android.dx.dex.code.CstInsn;
import com.android.dx.dex.code.DalvInsn;
import com.android.dx.dex.code.InsnFormat;

public final class Form11n extends InsnFormat
{
    public static final InsnFormat \u3007080;
    
    static {
        \u3007080 = new Form11n();
    }
    
    private Form11n() {
    }
    
    @Override
    public int O8() {
        return 1;
    }
    
    @Override
    public boolean OO0o\u3007\u3007\u3007\u30070(final DalvInsn dalvInsn) {
        final RegisterSpecList oooo8o0\u3007 = dalvInsn.Oooo8o0\u3007();
        final boolean b = dalvInsn instanceof CstInsn;
        boolean b3;
        final boolean b2 = b3 = false;
        if (b) {
            b3 = b2;
            if (oooo8o0\u3007.size() == 1) {
                if (!InsnFormat.oo88o8O(oooo8o0\u3007.\u3007O00(0).\u30078o8o\u3007())) {
                    b3 = b2;
                }
                else {
                    final Constant o\u30078O8\u3007008 = ((CstInsn)dalvInsn).O\u30078O8\u3007008();
                    if (!(o\u30078O8\u3007008 instanceof CstLiteralBits)) {
                        return false;
                    }
                    final CstLiteralBits cstLiteralBits = (CstLiteralBits)o\u30078O8\u3007008;
                    b3 = b2;
                    if (cstLiteralBits.o\u30070()) {
                        b3 = b2;
                        if (InsnFormat.OoO8(cstLiteralBits.\u3007\u3007888())) {
                            b3 = true;
                        }
                    }
                }
            }
        }
        return b3;
    }
    
    @Override
    public void OOO\u3007O0(final AnnotatedOutput annotatedOutput, final DalvInsn dalvInsn) {
        InsnFormat.o\u3007O8\u3007\u3007o(annotatedOutput, InsnFormat.\u3007O\u3007(dalvInsn, InsnFormat.\u3007\u3007808\u3007(dalvInsn.Oooo8o0\u3007().\u3007O00(0).\u30078o8o\u3007(), ((CstLiteralBits)((CstInsn)dalvInsn).O\u30078O8\u3007008()).\u3007\u3007888() & 0xF)));
    }
    
    @Override
    public String oO80(final DalvInsn dalvInsn) {
        final RegisterSpecList oooo8o0\u3007 = dalvInsn.Oooo8o0\u3007();
        final CstLiteralBits cstLiteralBits = (CstLiteralBits)((CstInsn)dalvInsn).O\u30078O8\u3007008();
        final StringBuilder sb = new StringBuilder();
        sb.append(oooo8o0\u3007.\u3007O00(0).\u30070\u3007O0088o());
        sb.append(", ");
        sb.append(InsnFormat.Oooo8o0\u3007(cstLiteralBits));
        return sb.toString();
    }
    
    @Override
    public String \u300780\u3007808\u3007O(final DalvInsn dalvInsn, final boolean b) {
        return InsnFormat.OO0o\u3007\u3007((CstLiteralBits)((CstInsn)dalvInsn).O\u30078O8\u3007008(), 4);
    }
    
    @Override
    public BitSet \u3007\u3007888(final DalvInsn dalvInsn) {
        final RegisterSpecList oooo8o0\u3007 = dalvInsn.Oooo8o0\u3007();
        final BitSet set = new BitSet(1);
        set.set(0, InsnFormat.oo88o8O(oooo8o0\u3007.\u3007O00(0).\u30078o8o\u3007()));
        return set;
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.android.dx.dex.code.form;

import com.android.dx.util.AnnotatedOutput;
import com.android.dx.rop.code.RegisterSpecList;
import com.android.dx.rop.cst.CstProtoRef;
import com.android.dx.rop.cst.CstMethodRef;
import com.android.dx.dex.code.MultiCstInsn;
import com.android.dx.dex.code.DalvInsn;
import com.android.dx.dex.code.InsnFormat;

public final class Form4rcc extends InsnFormat
{
    public static final InsnFormat \u3007080;
    
    static {
        \u3007080 = new Form4rcc();
    }
    
    private Form4rcc() {
    }
    
    @Override
    public int O8() {
        return 4;
    }
    
    @Override
    public boolean OO0o\u3007\u3007\u3007\u30070(final DalvInsn dalvInsn) {
        final boolean b = dalvInsn instanceof MultiCstInsn;
        final boolean b2 = false;
        if (!b) {
            return false;
        }
        final MultiCstInsn multiCstInsn = (MultiCstInsn)dalvInsn;
        final int o8ooOoo\u3007 = multiCstInsn.O8ooOoo\u3007(0);
        final int o8ooOoo\u30072 = multiCstInsn.O8ooOoo\u3007(1);
        boolean b3 = b2;
        if (InsnFormat.\u3007oo\u3007(o8ooOoo\u3007)) {
            if (!InsnFormat.\u3007oo\u3007(o8ooOoo\u30072)) {
                b3 = b2;
            }
            else {
                if (!(multiCstInsn.O\u30078O8\u3007008(0) instanceof CstMethodRef)) {
                    return false;
                }
                if (!(multiCstInsn.O\u30078O8\u3007008(1) instanceof CstProtoRef)) {
                    return false;
                }
                final RegisterSpecList oooo8o0\u3007 = multiCstInsn.Oooo8o0\u3007();
                final int size = oooo8o0\u3007.size();
                if (size == 0) {
                    return true;
                }
                b3 = b2;
                if (InsnFormat.\u3007O888o0o(oooo8o0\u3007.\u3007\u30078O0\u30078())) {
                    b3 = b2;
                    if (InsnFormat.\u3007oo\u3007(size)) {
                        b3 = b2;
                        if (InsnFormat.\u3007oo\u3007(oooo8o0\u3007.\u3007O00(0).\u30078o8o\u3007())) {
                            b3 = b2;
                            if (InsnFormat.\u30078o8o\u3007(oooo8o0\u3007)) {
                                b3 = true;
                            }
                        }
                    }
                }
            }
        }
        return b3;
    }
    
    @Override
    public void OOO\u3007O0(final AnnotatedOutput annotatedOutput, final DalvInsn dalvInsn) {
        final MultiCstInsn multiCstInsn = (MultiCstInsn)dalvInsn;
        short n = 0;
        final short n2 = (short)multiCstInsn.O8ooOoo\u3007(0);
        final short n3 = (short)multiCstInsn.O8ooOoo\u3007(1);
        final RegisterSpecList oooo8o0\u3007 = dalvInsn.Oooo8o0\u3007();
        if (oooo8o0\u3007.size() > 0) {
            n = (short)oooo8o0\u3007.\u3007O00(0).\u30078o8o\u3007();
        }
        InsnFormat.\u30070000OOO(annotatedOutput, InsnFormat.\u3007O\u3007(dalvInsn, oooo8o0\u3007.\u3007\u30078O0\u30078()), n2, n, n3);
    }
    
    @Override
    public String oO80(final DalvInsn dalvInsn) {
        final StringBuilder sb = new StringBuilder();
        sb.append(InsnFormat.\u3007\u30078O0\u30078(dalvInsn.Oooo8o0\u3007()));
        sb.append(", ");
        sb.append(dalvInsn.O8());
        return sb.toString();
    }
    
    @Override
    public String \u300780\u3007808\u3007O(final DalvInsn dalvInsn, final boolean b) {
        if (b) {
            return dalvInsn.\u3007o\u3007();
        }
        return "";
    }
}

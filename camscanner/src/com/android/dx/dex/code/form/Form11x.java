// 
// Decompiled by Procyon v0.6.0
// 

package com.android.dx.dex.code.form;

import java.util.BitSet;
import com.android.dx.util.AnnotatedOutput;
import com.android.dx.rop.code.RegisterSpecList;
import com.android.dx.dex.code.SimpleInsn;
import com.android.dx.dex.code.DalvInsn;
import com.android.dx.dex.code.InsnFormat;

public final class Form11x extends InsnFormat
{
    public static final InsnFormat \u3007080;
    
    static {
        \u3007080 = new Form11x();
    }
    
    private Form11x() {
    }
    
    @Override
    public int O8() {
        return 1;
    }
    
    @Override
    public boolean OO0o\u3007\u3007\u3007\u30070(final DalvInsn dalvInsn) {
        final RegisterSpecList oooo8o0\u3007 = dalvInsn.Oooo8o0\u3007();
        final boolean b = dalvInsn instanceof SimpleInsn;
        boolean b3;
        final boolean b2 = b3 = false;
        if (b) {
            b3 = b2;
            if (oooo8o0\u3007.size() == 1) {
                b3 = b2;
                if (InsnFormat.\u3007O888o0o(oooo8o0\u3007.\u3007O00(0).\u30078o8o\u3007())) {
                    b3 = true;
                }
            }
        }
        return b3;
    }
    
    @Override
    public void OOO\u3007O0(final AnnotatedOutput annotatedOutput, final DalvInsn dalvInsn) {
        InsnFormat.o\u3007O8\u3007\u3007o(annotatedOutput, InsnFormat.\u3007O\u3007(dalvInsn, dalvInsn.Oooo8o0\u3007().\u3007O00(0).\u30078o8o\u3007()));
    }
    
    @Override
    public String oO80(final DalvInsn dalvInsn) {
        return dalvInsn.Oooo8o0\u3007().\u3007O00(0).\u30070\u3007O0088o();
    }
    
    @Override
    public String \u300780\u3007808\u3007O(final DalvInsn dalvInsn, final boolean b) {
        return "";
    }
    
    @Override
    public BitSet \u3007\u3007888(final DalvInsn dalvInsn) {
        final RegisterSpecList oooo8o0\u3007 = dalvInsn.Oooo8o0\u3007();
        final BitSet set = new BitSet(1);
        set.set(0, InsnFormat.\u3007O888o0o(oooo8o0\u3007.\u3007O00(0).\u30078o8o\u3007()));
        return set;
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.android.dx.dex.code.form;

import java.util.BitSet;
import com.android.dx.util.AnnotatedOutput;
import com.android.dx.rop.code.RegisterSpec;
import com.android.dx.rop.code.RegisterSpecList;
import com.android.dx.dex.code.SimpleInsn;
import com.android.dx.dex.code.DalvInsn;
import com.android.dx.dex.code.InsnFormat;

public final class Form12x extends InsnFormat
{
    public static final InsnFormat \u3007080;
    
    static {
        \u3007080 = new Form12x();
    }
    
    private Form12x() {
    }
    
    @Override
    public int O8() {
        return 1;
    }
    
    @Override
    public boolean OO0o\u3007\u3007\u3007\u30070(final DalvInsn dalvInsn) {
        final boolean b = dalvInsn instanceof SimpleInsn;
        final boolean b2 = false;
        if (!b) {
            return false;
        }
        final RegisterSpecList oooo8o0\u3007 = dalvInsn.Oooo8o0\u3007();
        final int size = oooo8o0\u3007.size();
        RegisterSpec registerSpec;
        RegisterSpec \u3007o2;
        if (size != 2) {
            if (size != 3) {
                return false;
            }
            final RegisterSpec \u3007o00 = oooo8o0\u3007.\u3007O00(1);
            registerSpec = oooo8o0\u3007.\u3007O00(2);
            \u3007o2 = \u3007o00;
            if (\u3007o00.\u30078o8o\u3007() != oooo8o0\u3007.\u3007O00(0).\u30078o8o\u3007()) {
                return false;
            }
        }
        else {
            \u3007o2 = oooo8o0\u3007.\u3007O00(0);
            registerSpec = oooo8o0\u3007.\u3007O00(1);
        }
        boolean b3 = b2;
        if (InsnFormat.oo88o8O(\u3007o2.\u30078o8o\u3007())) {
            b3 = b2;
            if (InsnFormat.oo88o8O(registerSpec.\u30078o8o\u3007())) {
                b3 = true;
            }
        }
        return b3;
    }
    
    @Override
    public void OOO\u3007O0(final AnnotatedOutput annotatedOutput, final DalvInsn dalvInsn) {
        final RegisterSpecList oooo8o0\u3007 = dalvInsn.Oooo8o0\u3007();
        final int size = oooo8o0\u3007.size();
        InsnFormat.o\u3007O8\u3007\u3007o(annotatedOutput, InsnFormat.\u3007O\u3007(dalvInsn, InsnFormat.\u3007\u3007808\u3007(oooo8o0\u3007.\u3007O00(size - 2).\u30078o8o\u3007(), oooo8o0\u3007.\u3007O00(size - 1).\u30078o8o\u3007())));
    }
    
    @Override
    public String oO80(final DalvInsn dalvInsn) {
        final RegisterSpecList oooo8o0\u3007 = dalvInsn.Oooo8o0\u3007();
        final int size = oooo8o0\u3007.size();
        final StringBuilder sb = new StringBuilder();
        sb.append(oooo8o0\u3007.\u3007O00(size - 2).\u30070\u3007O0088o());
        sb.append(", ");
        sb.append(oooo8o0\u3007.\u3007O00(size - 1).\u30070\u3007O0088o());
        return sb.toString();
    }
    
    @Override
    public String \u300780\u3007808\u3007O(final DalvInsn dalvInsn, final boolean b) {
        return "";
    }
    
    @Override
    public BitSet \u3007\u3007888(final DalvInsn dalvInsn) {
        final RegisterSpecList oooo8o0\u3007 = dalvInsn.Oooo8o0\u3007();
        final BitSet set = new BitSet(2);
        final int \u30078o8o\u3007 = oooo8o0\u3007.\u3007O00(0).\u30078o8o\u3007();
        final int \u30078o8o\u30072 = oooo8o0\u3007.\u3007O00(1).\u30078o8o\u3007();
        final int size = oooo8o0\u3007.size();
        if (size != 2) {
            if (size != 3) {
                throw new AssertionError();
            }
            if (\u30078o8o\u3007 != \u30078o8o\u30072) {
                set.set(0, false);
                set.set(1, false);
            }
            else {
                final boolean oo88o8O = InsnFormat.oo88o8O(\u30078o8o\u30072);
                set.set(0, oo88o8O);
                set.set(1, oo88o8O);
            }
            set.set(2, InsnFormat.oo88o8O(oooo8o0\u3007.\u3007O00(2).\u30078o8o\u3007()));
        }
        else {
            set.set(0, InsnFormat.oo88o8O(\u30078o8o\u3007));
            set.set(1, InsnFormat.oo88o8O(\u30078o8o\u30072));
        }
        return set;
    }
}

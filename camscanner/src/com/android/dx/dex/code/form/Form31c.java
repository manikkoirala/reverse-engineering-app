// 
// Decompiled by Procyon v0.6.0
// 

package com.android.dx.dex.code.form;

import java.util.BitSet;
import com.android.dx.util.AnnotatedOutput;
import com.android.dx.rop.cst.Constant;
import com.android.dx.rop.code.RegisterSpec;
import com.android.dx.rop.code.RegisterSpecList;
import com.android.dx.rop.cst.CstString;
import com.android.dx.rop.cst.CstFieldRef;
import com.android.dx.rop.cst.CstType;
import com.android.dx.dex.code.CstInsn;
import com.android.dx.dex.code.DalvInsn;
import com.android.dx.dex.code.InsnFormat;

public final class Form31c extends InsnFormat
{
    public static final InsnFormat \u3007080;
    
    static {
        \u3007080 = new Form31c();
    }
    
    private Form31c() {
    }
    
    @Override
    public int O8() {
        return 3;
    }
    
    @Override
    public boolean OO0o\u3007\u3007\u3007\u30070(final DalvInsn dalvInsn) {
        final boolean b = dalvInsn instanceof CstInsn;
        boolean b2 = false;
        if (!b) {
            return false;
        }
        final RegisterSpecList oooo8o0\u3007 = dalvInsn.Oooo8o0\u3007();
        final int size = oooo8o0\u3007.size();
        RegisterSpec registerSpec;
        if (size != 1) {
            if (size != 2) {
                return false;
            }
            if ((registerSpec = oooo8o0\u3007.\u3007O00(0)).\u30078o8o\u3007() != oooo8o0\u3007.\u3007O00(1).\u30078o8o\u3007()) {
                return false;
            }
        }
        else {
            registerSpec = oooo8o0\u3007.\u3007O00(0);
        }
        if (!InsnFormat.\u3007O888o0o(registerSpec.\u30078o8o\u3007())) {
            return false;
        }
        final Constant o\u30078O8\u3007008 = ((CstInsn)dalvInsn).O\u30078O8\u3007008();
        if (o\u30078O8\u3007008 instanceof CstType || o\u30078O8\u3007008 instanceof CstFieldRef || o\u30078O8\u3007008 instanceof CstString) {
            b2 = true;
        }
        return b2;
    }
    
    @Override
    public void OOO\u3007O0(final AnnotatedOutput annotatedOutput, final DalvInsn dalvInsn) {
        InsnFormat.\u300700(annotatedOutput, InsnFormat.\u3007O\u3007(dalvInsn, dalvInsn.Oooo8o0\u3007().\u3007O00(0).\u30078o8o\u3007()), ((CstInsn)dalvInsn).O8ooOoo\u3007());
    }
    
    @Override
    public String oO80(final DalvInsn dalvInsn) {
        final RegisterSpecList oooo8o0\u3007 = dalvInsn.Oooo8o0\u3007();
        final StringBuilder sb = new StringBuilder();
        sb.append(oooo8o0\u3007.\u3007O00(0).\u30070\u3007O0088o());
        sb.append(", ");
        sb.append(dalvInsn.O8());
        return sb.toString();
    }
    
    @Override
    public String \u300780\u3007808\u3007O(final DalvInsn dalvInsn, final boolean b) {
        if (b) {
            return dalvInsn.\u3007o\u3007();
        }
        return "";
    }
    
    @Override
    public BitSet \u3007\u3007888(final DalvInsn dalvInsn) {
        final RegisterSpecList oooo8o0\u3007 = dalvInsn.Oooo8o0\u3007();
        final int size = oooo8o0\u3007.size();
        final BitSet set = new BitSet(size);
        final boolean \u3007o888o0o = InsnFormat.\u3007O888o0o(oooo8o0\u3007.\u3007O00(0).\u30078o8o\u3007());
        if (size == 1) {
            set.set(0, \u3007o888o0o);
        }
        else if (oooo8o0\u3007.\u3007O00(0).\u30078o8o\u3007() == oooo8o0\u3007.\u3007O00(1).\u30078o8o\u3007()) {
            set.set(0, \u3007o888o0o);
            set.set(1, \u3007o888o0o);
        }
        return set;
    }
}

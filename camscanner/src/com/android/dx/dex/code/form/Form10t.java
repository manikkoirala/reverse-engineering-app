// 
// Decompiled by Procyon v0.6.0
// 

package com.android.dx.dex.code.form;

import com.android.dx.util.AnnotatedOutput;
import com.android.dx.dex.code.TargetInsn;
import com.android.dx.dex.code.DalvInsn;
import com.android.dx.dex.code.InsnFormat;

public final class Form10t extends InsnFormat
{
    public static final InsnFormat \u3007080;
    
    static {
        \u3007080 = new Form10t();
    }
    
    private Form10t() {
    }
    
    @Override
    public int O8() {
        return 1;
    }
    
    @Override
    public boolean OO0o\u3007\u3007\u3007\u30070(final DalvInsn dalvInsn) {
        if (dalvInsn instanceof TargetInsn && dalvInsn.Oooo8o0\u3007().size() == 0) {
            final TargetInsn targetInsn = (TargetInsn)dalvInsn;
            return !targetInsn.\u30070000OOO() || this.\u3007o00\u3007\u3007Oo(targetInsn);
        }
        return false;
    }
    
    @Override
    public void OOO\u3007O0(final AnnotatedOutput annotatedOutput, final DalvInsn dalvInsn) {
        InsnFormat.o\u3007O8\u3007\u3007o(annotatedOutput, InsnFormat.\u3007O\u3007(dalvInsn, ((TargetInsn)dalvInsn).\u3007oOO8O8() & 0xFF));
    }
    
    @Override
    public String oO80(final DalvInsn dalvInsn) {
        return InsnFormat.\u3007o\u3007(dalvInsn);
    }
    
    @Override
    public String \u300780\u3007808\u3007O(final DalvInsn dalvInsn, final boolean b) {
        return InsnFormat.\u3007080(dalvInsn);
    }
    
    @Override
    public boolean \u3007o00\u3007\u3007Oo(final TargetInsn targetInsn) {
        final int \u3007oOO8O8 = targetInsn.\u3007oOO8O8();
        return \u3007oOO8O8 != 0 && InsnFormat.\u30070\u3007O0088o(\u3007oOO8O8);
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.android.dx.dex.code;

import com.android.dx.rop.code.RegisterSpecList;
import com.android.dx.rop.code.SourcePosition;

public final class TargetInsn extends FixedSizeInsn
{
    private CodeAddress Oo08;
    
    public TargetInsn(final Dop dop, final SourcePosition sourcePosition, final RegisterSpecList list, final CodeAddress oo08) {
        super(dop, sourcePosition, list);
        if (oo08 != null) {
            this.Oo08 = oo08;
            return;
        }
        throw new NullPointerException("target == null");
    }
    
    public int O8ooOoo\u3007() {
        return this.Oo08.oO80();
    }
    
    public CodeAddress O\u30078O8\u3007008() {
        return this.Oo08;
    }
    
    @Override
    public DalvInsn oo88o8O(final Dop dop) {
        return new TargetInsn(dop, this.OO0o\u3007\u3007(), this.Oooo8o0\u3007(), this.Oo08);
    }
    
    @Override
    public DalvInsn o\u3007O8\u3007\u3007o(final RegisterSpecList list) {
        return new TargetInsn(this.\u3007O8o08O(), this.OO0o\u3007\u3007(), list, this.Oo08);
    }
    
    public TargetInsn o\u3007\u30070\u3007(final CodeAddress codeAddress) {
        return new TargetInsn(this.\u3007O8o08O().o\u30070(), this.OO0o\u3007\u3007(), this.Oooo8o0\u3007(), codeAddress);
    }
    
    public boolean \u30070000OOO() {
        return this.\u3007\u3007808\u3007() && this.Oo08.\u3007\u3007808\u3007();
    }
    
    @Override
    protected String \u3007080() {
        final CodeAddress oo08 = this.Oo08;
        if (oo08 == null) {
            return "????";
        }
        return oo08.\u3007O00();
    }
    
    public int \u3007oOO8O8() {
        return this.Oo08.oO80() - this.oO80();
    }
}

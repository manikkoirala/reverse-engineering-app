// 
// Decompiled by Procyon v0.6.0
// 

package com.android.dx.dex.code;

import com.android.dx.ssa.RegisterMapper;
import com.android.dx.rop.code.RegisterSpecList;
import com.android.dx.rop.code.SourcePosition;
import com.android.dx.rop.code.RegisterSpec;

public final class LocalStart extends ZeroSizeInsn
{
    private final RegisterSpec Oo08;
    
    public LocalStart(final SourcePosition sourcePosition, final RegisterSpec oo08) {
        super(sourcePosition);
        if (oo08 != null) {
            this.Oo08 = oo08;
            return;
        }
        throw new NullPointerException("local == null");
    }
    
    public static String O8ooOoo\u3007(final RegisterSpec registerSpec) {
        final StringBuilder sb = new StringBuilder();
        sb.append(registerSpec.\u30070\u3007O0088o());
        sb.append(' ');
        registerSpec.\u300780\u3007808\u3007O();
        throw null;
    }
    
    public RegisterSpec O\u30078O8\u3007008() {
        return this.Oo08;
    }
    
    @Override
    public DalvInsn o\u3007O8\u3007\u3007o(final RegisterSpecList list) {
        return new LocalStart(this.OO0o\u3007\u3007(), this.Oo08);
    }
    
    @Override
    protected String \u3007080() {
        return this.Oo08.toString();
    }
    
    @Override
    protected String \u30070\u3007O0088o(final boolean b) {
        final StringBuilder sb = new StringBuilder();
        sb.append("local-start ");
        sb.append(O8ooOoo\u3007(this.Oo08));
        return sb.toString();
    }
    
    @Override
    public DalvInsn \u3007O888o0o(final RegisterMapper registerMapper) {
        return new LocalStart(this.OO0o\u3007\u3007(), registerMapper.\u3007o00\u3007\u3007Oo(this.Oo08));
    }
    
    @Override
    public DalvInsn \u3007oo\u3007(final int n) {
        return new LocalStart(this.OO0o\u3007\u3007(), this.Oo08.\u3007O888o0o(n));
    }
}

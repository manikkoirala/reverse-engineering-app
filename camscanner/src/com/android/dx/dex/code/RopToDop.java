// 
// Decompiled by Procyon v0.6.0
// 

package com.android.dx.dex.code;

import com.android.dx.rop.cst.Constant;
import com.android.dx.rop.code.RegisterSpec;
import com.android.dx.rop.cst.CstProtoRef;
import com.android.dx.rop.cst.CstMethodHandle;
import com.android.dx.rop.cst.CstString;
import com.android.dx.rop.cst.CstType;
import com.android.dx.rop.code.ThrowingCstInsn;
import com.android.dx.rop.cst.CstFieldRef;
import com.android.dx.rop.code.Insn;
import com.android.dx.rop.code.Rops;
import com.android.dx.rop.code.Rop;
import java.util.HashMap;

public final class RopToDop
{
    private static final HashMap<Rop, Dop> \u3007080;
    
    static {
        final HashMap \u300781 = new HashMap(400);
        (\u3007080 = \u300781).put(Rops.\u3007080, Dops.\u3007o\u3007);
        final Rop \u3007o00\u3007\u3007Oo = Rops.\u3007o00\u3007\u3007Oo;
        final Dop o8 = Dops.O8;
        \u300781.put(\u3007o00\u3007\u3007Oo, o8);
        final Rop \u3007o\u3007 = Rops.\u3007o\u3007;
        final Dop \u3007\u3007888 = Dops.\u3007\u3007888;
        \u300781.put(\u3007o\u3007, \u3007\u3007888);
        \u300781.put(Rops.O8, o8);
        \u300781.put(Rops.Oo08, \u3007\u3007888);
        final Rop o\u30070 = Rops.o\u30070;
        final Dop oo0o\u3007\u3007\u3007\u30070 = Dops.OO0o\u3007\u3007\u3007\u30070;
        \u300781.put(o\u30070, oo0o\u3007\u3007\u3007\u30070);
        \u300781.put(Rops.oO80, o8);
        \u300781.put(Rops.\u300780\u3007808\u3007O, \u3007\u3007888);
        \u300781.put(Rops.OO0o\u3007\u3007\u3007\u30070, o8);
        \u300781.put(Rops.\u30078o8o\u3007, \u3007\u3007888);
        \u300781.put(Rops.\u3007O8o08O, oo0o\u3007\u3007\u3007\u30070);
        final Rop oo0o\u3007\u3007 = Rops.OO0o\u3007\u3007;
        final Dop o800o8O = Dops.o800o8O;
        \u300781.put(oo0o\u3007\u3007, o800o8O);
        final Rop oooo8o0\u3007 = Rops.Oooo8o0\u3007;
        final Dop o\u3007O8\u3007\u3007o = Dops.o\u3007O8\u3007\u3007o;
        \u300781.put(oooo8o0\u3007, o\u3007O8\u3007\u3007o);
        \u300781.put(Rops.\u3007\u3007808\u3007, o800o8O);
        \u300781.put(Rops.\u3007O\u3007, o\u3007O8\u3007\u3007o);
        \u300781.put(Rops.\u3007\u30078O0\u30078, o800o8O);
        \u300781.put(Rops.\u30070\u3007O0088o, Dops.\u3007\u30070o);
        final Rop ooO8 = Rops.OoO8;
        final Dop ooo\u30078oO = Dops.ooo\u30078oO;
        \u300781.put(ooO8, ooo\u30078oO);
        final Rop o800o8O2 = Rops.o800o8O;
        final Dop o0o\u3007\u3007Oo = Dops.O0o\u3007\u3007Oo;
        \u300781.put(o800o8O2, o0o\u3007\u3007Oo);
        \u300781.put(Rops.\u3007O888o0o, Dops.OO8oO0o\u3007);
        \u300781.put(Rops.oo88o8O, Dops.o0O0);
        \u300781.put(Rops.\u3007oo\u3007, Dops.o88\u3007OO08\u3007);
        \u300781.put(Rops.o\u3007O8\u3007\u3007o, Dops.\u30070);
        \u300781.put(Rops.\u300700, ooo\u30078oO);
        \u300781.put(Rops.O\u30078O8\u3007008, o0o\u3007\u3007Oo);
        final Rop o8ooOoo\u3007 = Rops.O8ooOoo\u3007;
        final Dop oo00OOO = Dops.oO00OOO;
        \u300781.put(o8ooOoo\u3007, oo00OOO);
        final Rop \u3007oOO8O8 = Rops.\u3007oOO8O8;
        final Dop o9 = Dops.O000;
        \u300781.put(\u3007oOO8O8, o9);
        \u300781.put(Rops.\u30070000OOO, Dops.\u300780);
        \u300781.put(Rops.o\u3007\u30070\u3007, Dops.Ooo);
        \u300781.put(Rops.OOO\u3007O0, Dops.OOO);
        \u300781.put(Rops.oo\u3007, Dops.\u3007O\u300780o08O);
        \u300781.put(Rops.O8\u3007o, oo00OOO);
        \u300781.put(Rops.\u300700\u30078, o9);
        \u300781.put(Rops.\u3007o, Dops.O08000);
        \u300781.put(Rops.o0ooO, Dops.O8OO08o);
        \u300781.put(Rops.o\u30078, Dops.O0o8\u3007O);
        \u300781.put(Rops.o8, Dops.Oo0O080);
        \u300781.put(Rops.Oo8Oo00oo, Dops.\u3007\u30078);
        \u300781.put(Rops.\u3007\u3007\u30070\u3007\u30070, Dops.\u30078\u3007o\u30078);
        \u300781.put(Rops.o\u30070OOo\u30070, Dops.\u3007oOo\u3007);
        \u300781.put(Rops.\u3007\u30070o, Dops.O0O\u3007OOo);
        \u300781.put(Rops.\u300708O8o\u30070, Dops.oo08OO\u30070);
        \u300781.put(Rops.oO, Dops.OO\u3007);
        \u300781.put(Rops.\u30078, Dops.o88o0O);
        \u300781.put(Rops.O08000, Dops.oO80OOO\u3007);
        \u300781.put(Rops.\u30078\u30070\u3007o\u3007O, Dops.\u3007\u30070O8ooO);
        \u300781.put(Rops.O\u3007O\u3007oO, Dops.o8\u3007);
        \u300781.put(Rops.o8oO\u3007, Dops.\u30070O00oO);
        \u300781.put(Rops.o\u30078oOO88, Dops.\u3007\u30070o\u3007o8);
        \u300781.put(Rops.o\u3007O, Dops.O0\u3007oO\u3007o);
        \u300781.put(Rops.oO00OOO, Dops.\u3007\u3007O00\u30078);
        \u300781.put(Rops.O000, Dops.O08O0\u3007O);
        \u300781.put(Rops.\u300780, Dops.oO0\u3007\u3007o8\u3007);
        \u300781.put(Rops.Ooo, Dops.OOo);
        \u300781.put(Rops.\u3007O\u300780o08O, Dops.\u3007o\u3007Oo0);
        \u300781.put(Rops.OOO, Dops.OOoo);
        \u300781.put(Rops.ooo\u30078oO, Dops.Oo08OO8oO);
        \u300781.put(Rops.O0o\u3007\u3007Oo, Dops.\u3007Oo\u3007o8);
        \u300781.put(Rops.OO8oO0o\u3007, Dops.O8O\u30078oo08);
        \u300781.put(Rops.o0O0, Dops.\u300708O8o8);
        \u300781.put(Rops.\u30070, Dops.\u300708\u30070\u3007o\u30078);
        \u300781.put(Rops.o88\u3007OO08\u3007, Dops.o\u3007OOo000);
        \u300781.put(Rops.O8O\u3007, Dops.o\u3007);
        \u300781.put(Rops.O0O8OO088, Dops.O0o\u3007);
        \u300781.put(Rops.\u3007o0O0O8, Dops.o0oO);
        \u300781.put(Rops.\u3007\u3007o8, Dops.OO88\u3007OOO);
        \u300781.put(Rops.Oo\u3007O, Dops.\u3007\u3007o0o);
        \u300781.put(Rops.o8O\u3007, Dops.oO0\u3007\u3007O8o);
        \u300781.put(Rops.O0, Dops.\u3007o\u3007o);
        \u300781.put(Rops.ooOO, Dops.OO88o);
        \u300781.put(Rops.\u3007O, Dops.O00O);
        \u300781.put(Rops.OOO8o\u3007\u3007, Dops.OO0\u3007\u30078);
        \u300781.put(Rops.\u30070O\u3007Oo, Dops.o08o\u30070);
        \u300781.put(Rops.o\u3007o, Dops.Oo8);
        \u300781.put(Rops.Oo\u3007o, Dops.\u3007OOo8\u30070);
        \u300781.put(Rops.OOo0O, Dops.OO);
        \u300781.put(Rops.O0OO8\u30070, Dops.\u300708O\u300700\u3007o);
        \u300781.put(Rops.\u3007000\u3007\u300708, Dops.o\u300700O);
        \u300781.put(Rops.O8O\u300788oO0, Dops.O8o08O8O);
        \u300781.put(Rops.O00, Dops.\u3007080OO8\u30070);
        \u300781.put(Rops.O0o, Dops.\u30070O);
        \u300781.put(Rops.o0O\u30078o0O, Dops.oOo\u30078o008);
        \u300781.put(Rops.O0oO008, Dops.oOo0);
        \u300781.put(Rops.\u3007\u300700OO, Dops.o\u3007O);
        \u300781.put(Rops.\u30070OO8, Dops.\u30078\u30070\u3007o\u3007O);
        \u300781.put(Rops.oOo, Dops.o8oO\u3007);
        \u300781.put(Rops.o0, Dops.O\u3007O\u3007oO);
        \u300781.put(Rops.O0oo0o0\u3007, Dops.o\u30078oOO88);
        \u300781.put(Rops.\u3007o\u30078, Dops.\u3007\u30070o8O\u3007\u3007);
        \u300781.put(Rops.ooo\u3007\u3007O\u3007, Dops.\u3007oo);
        \u300781.put(Rops.OO\u30070008O8, Dops.Oo0oOo\u30070);
        \u300781.put(Rops.\u30078o, Dops.O\u3007Oo);
        \u300781.put(Rops.O\u30070\u3007o808\u3007, Dops.\u30078o8O\u3007O);
        \u300781.put(Rops.\u3007o\u3007Oo0, Dops.O8oOo80);
        \u300781.put(Rops.O00O, Dops.OoO\u3007);
        \u300781.put(Rops.OOoo, Dops.O\u30078oOo8O);
        \u300781.put(Rops.OO0\u3007\u30078, Dops.o\u3007\u30070\u300788);
        \u300781.put(Rops.Oo08OO8oO, Dops.\u3007o8OO0);
        \u300781.put(Rops.\u3007Oo\u3007o8, Dops.\u30078o\u3007\u30078080);
        \u300781.put(Rops.O\u3007Oo, Dops.o8o\u3007\u30070O);
        \u300781.put(Rops.OoO\u3007, Dops.ooo8o\u3007o\u3007);
        \u300781.put(Rops.\u3007o8OO0, Dops.oO8o);
        \u300781.put(Rops.\u3007\u30070o8O\u3007\u3007, Dops.\u3007\u30070\u30070o8);
        \u300781.put(Rops.O\u30078oOo8O, Dops.\u3007O00);
        final Rop \u30078o\u3007\u30078080 = Rops.\u30078o\u3007\u30078080;
        final Dop \u3007\u30078O0\u30078 = Dops.\u3007\u30078O0\u30078;
        \u300781.put(\u30078o\u3007\u30078080, \u3007\u30078O0\u30078);
        final Rop \u3007oo = Rops.\u3007oo;
        final Dop \u30070\u3007O0088o = Dops.\u30070\u3007O0088o;
        \u300781.put(\u3007oo, \u30070\u3007O0088o);
        \u300781.put(Rops.\u30078o8O\u3007O, \u3007\u30078O0\u30078);
        \u300781.put(Rops.o8o\u3007\u30070O, \u30070\u3007O0088o);
        \u300781.put(Rops.Oo0oOo\u30070, Dops.OoO8);
        \u300781.put(Rops.O8oOo80, Dops.\u3007o);
        \u300781.put(Rops.o\u3007\u30070\u300788, Dops.o\u30070OOo\u30070);
        \u300781.put(Rops.ooo8o\u3007o\u3007, Dops.OOO\u3007O0);
        \u300781.put(Rops.oO8o, Dops.oo\u3007);
        final Rop \u3007\u30070\u30070o8 = Rops.\u3007\u30070\u30070o8;
        final Dop o8O\u3007 = Dops.O8O\u3007;
        \u300781.put(\u3007\u30070\u30070o8, o8O\u3007);
        final Rop \u3007008\u3007oo = Rops.\u3007008\u3007oo;
        final Dop o0O8OO088 = Dops.O0O8OO088;
        \u300781.put(\u3007008\u3007oo, o0O8OO088);
        \u300781.put(Rops.O\u3007\u3007, o8O\u3007);
        \u300781.put(Rops.oo0O\u30070\u3007\u3007\u3007, o0O8OO088);
        \u300781.put(Rops.oO8008O, Dops.\u3007o0O0O8);
        \u300781.put(Rops.O0\u3007oo, Dops.\u3007\u3007o8);
        \u300781.put(Rops.\u3007OO8Oo0\u3007, Dops.Oo\u3007O);
        \u300781.put(Rops.o08oOO, Dops.o8O\u3007);
        \u300781.put(Rops.o88O\u30078, Dops.O0);
        final Rop o88O8 = Rops.o88O8;
        final Dop ooOO = Dops.ooOO;
        \u300781.put(o88O8, ooOO);
        final Rop \u3007\u300700O\u30070o = Rops.\u3007\u300700O\u30070o;
        final Dop \u3007o = Dops.\u3007O;
        \u300781.put(\u3007\u300700O\u30070o, \u3007o);
        \u300781.put(Rops.O8888, ooOO);
        \u300781.put(Rops.\u30078O0O808\u3007, \u3007o);
        \u300781.put(Rops.OOo88OOo, Dops.OOO8o\u3007\u3007);
        \u300781.put(Rops.o08\u3007\u30070O, Dops.\u30070O\u3007Oo);
        \u300781.put(Rops.O\u3007oO\u3007oo8o, Dops.\u300700O0O0);
        \u300781.put(Rops.O88\u3007\u3007o0O, Dops.Ooo8\u3007\u3007);
        \u300781.put(Rops.o08O, Dops.\u3007000O0);
        \u300781.put(Rops.O88o\u3007, Dops.o0ooO);
        \u300781.put(Rops.o\u30070o\u3007\u3007, Dops.O8\u3007o);
        \u300781.put(Rops.\u3007\u3007\u30070880, Dops.\u300700\u30078);
        final Rop \u3007o8oO = Rops.\u3007o8oO;
        final Dop o\u30072 = Dops.O\u30070;
        \u300781.put(\u3007o8oO, o\u30072);
        \u300781.put(Rops.\u3007oO8O0\u3007\u3007O, Dops.o\u3007o);
        \u300781.put(Rops.OO8\u3007, o\u30072);
        \u300781.put(Rops.O8OO08o, Dops.oo);
        final Rop \u300708\u30070\u3007o\u30078 = Rops.\u300708\u30070\u3007o\u30078;
        final Dop oo\u3007O8o\u30078 = Dops.Oo\u3007O8o\u30078;
        \u300781.put(\u300708\u30070\u3007o\u30078, oo\u3007O8o\u30078);
        \u300781.put(Rops.o\u3007, Dops.O0\u3007OO8);
        \u300781.put(Rops.o0oO, oo\u3007O8o\u30078);
        \u300781.put(Rops.\u3007\u3007o0o, Dops.\u3007000\u3007\u300708);
        final Rop o08O0\u3007O = Rops.O08O0\u3007O;
        final Dop oOo0O = Dops.OOo0O;
        \u300781.put(o08O0\u3007O, oOo0O);
        \u300781.put(Rops.\u300708O8o8, Dops.O880oOO08);
        \u300781.put(Rops.o\u3007OOo000, oOo0O);
        \u300781.put(Rops.O0o\u3007, Dops.O\u3007Oooo\u3007\u3007);
        final Rop oo80OOO\u3007 = Rops.oO80OOO\u3007;
        final Dop o0o = Dops.O0o;
        \u300781.put(oo80OOO\u3007, o0o);
        \u300781.put(Rops.\u3007\u30070o\u3007o8, Dops.o8O0);
        \u300781.put(Rops.oO0\u3007\u3007o8\u3007, o0o);
        \u300781.put(Rops.\u3007\u30078, Dops.O0o\u3007O0\u3007);
    }
    
    public static Dop \u3007080(final Insn insn) {
        final Rop \u3007\u3007888 = insn.\u3007\u3007888();
        final Dop dop = RopToDop.\u3007080.get(\u3007\u3007888);
        if (dop != null) {
            return dop;
        }
        final int o8 = \u3007\u3007888.O8();
        if (o8 == 4) {
            return Dops.\u3007O\u3007;
        }
        if (o8 != 5) {
            if (o8 == 41) {
                return Dops.o\u30078;
            }
            if (o8 == 42) {
                return Dops.o8;
            }
            if (o8 != 55) {
                Label_0399: {
                    switch (o8) {
                        default: {
                            switch (o8) {
                                default: {
                                    break Label_0399;
                                }
                                case 59: {
                                    return Dops.\u30078\u3007oO\u3007\u30078o;
                                }
                                case 58: {
                                    return Dops.OO\u300700\u30078oO;
                                }
                                case 57: {
                                    return Dops.\u3007\u3007\u30070\u3007\u30070;
                                }
                            }
                            break;
                        }
                        case 53: {
                            return Dops.O0oo0o0\u3007;
                        }
                        case 52: {
                            return Dops.oOo;
                        }
                        case 51: {
                            return Dops.\u30070OO8;
                        }
                        case 50: {
                            return Dops.\u3007\u300700OO;
                        }
                        case 49: {
                            return Dops.o0;
                        }
                        case 48: {
                            final int \u3007o00\u3007\u3007Oo = ((CstFieldRef)((ThrowingCstInsn)insn).OO0o\u3007\u3007()).\u3007o00\u3007\u3007Oo();
                            if (\u3007o00\u3007\u3007Oo == 1) {
                                return Dops.o0O\u30078o0O;
                            }
                            if (\u3007o00\u3007\u3007Oo == 2) {
                                return Dops.Oo0oO\u3007O\u3007O;
                            }
                            if (\u3007o00\u3007\u3007Oo == 3) {
                                return Dops.O0oO008;
                            }
                            if (\u3007o00\u3007\u3007Oo == 6) {
                                return Dops.o8O0;
                            }
                            if (\u3007o00\u3007\u3007Oo == 8) {
                                return Dops.o\u30078\u3007;
                            }
                            break;
                        }
                        case 47: {
                            final int \u3007o00\u3007\u3007Oo2 = ((CstFieldRef)((ThrowingCstInsn)insn).OO0o\u3007\u3007()).\u3007o00\u3007\u3007Oo();
                            if (\u3007o00\u3007\u3007Oo2 == 1) {
                                return Dops.O\u3007OO;
                            }
                            if (\u3007o00\u3007\u3007Oo2 == 2) {
                                return Dops.O\u300708;
                            }
                            if (\u3007o00\u3007\u3007Oo2 == 3) {
                                return Dops.O0OO8\u30070;
                            }
                            if (\u3007o00\u3007\u3007Oo2 == 6) {
                                return Dops.O880oOO08;
                            }
                            if (\u3007o00\u3007\u3007Oo2 == 8) {
                                return Dops.ooO\u300700O;
                            }
                            break;
                        }
                        case 46: {
                            final int \u3007o00\u3007\u3007Oo3 = ((CstFieldRef)((ThrowingCstInsn)insn).OO0o\u3007\u3007()).\u3007o00\u3007\u3007Oo();
                            if (\u3007o00\u3007\u3007Oo3 == 1) {
                                return Dops.oO\u3007;
                            }
                            if (\u3007o00\u3007\u3007Oo3 == 2) {
                                return Dops.O8O\u300788oO0;
                            }
                            if (\u3007o00\u3007\u3007Oo3 == 3) {
                                return Dops.o80ooO;
                            }
                            if (\u3007o00\u3007\u3007Oo3 == 6) {
                                return Dops.O0\u3007OO8;
                            }
                            if (\u3007o00\u3007\u3007Oo3 == 8) {
                                return Dops.O00;
                            }
                            break;
                        }
                        case 45: {
                            final int \u3007o00\u3007\u3007Oo4 = ((CstFieldRef)((ThrowingCstInsn)insn).OO0o\u3007\u3007()).\u3007o00\u3007\u3007Oo();
                            if (\u3007o00\u3007\u3007Oo4 == 1) {
                                return Dops.Oo;
                            }
                            if (\u3007o00\u3007\u3007Oo4 == 2) {
                                return Dops.Oo\u3007o;
                            }
                            if (\u3007o00\u3007\u3007Oo4 == 3) {
                                return Dops.ooo0\u3007O88O;
                            }
                            if (\u3007o00\u3007\u3007Oo4 == 6) {
                                return Dops.o\u3007o;
                            }
                            if (\u3007o00\u3007\u3007Oo4 == 8) {
                                return Dops.OOo8o\u3007O;
                            }
                            break;
                        }
                    }
                }
                final StringBuilder sb = new StringBuilder();
                sb.append("unknown rop: ");
                sb.append(\u3007\u3007888);
                throw new RuntimeException(sb.toString());
            }
            final RegisterSpec \u300780\u3007808\u3007O = insn.\u300780\u3007808\u3007O();
            if (\u300780\u3007808\u3007O == null) {
                return Dops.\u3007o\u3007;
            }
            switch (\u300780\u3007808\u3007O.\u3007o00\u3007\u3007Oo()) {
                default: {
                    throw new RuntimeException("Unexpected basic type");
                }
                case 9: {
                    return Dops.\u3007\u3007808\u3007;
                }
                case 4:
                case 7: {
                    return Dops.Oooo8o0\u3007;
                }
                case 1:
                case 2:
                case 3:
                case 5:
                case 6:
                case 8: {
                    return Dops.OO0o\u3007\u3007;
                }
            }
        }
        else {
            final Constant oo0o\u3007\u3007 = ((ThrowingCstInsn)insn).OO0o\u3007\u3007();
            if (oo0o\u3007\u3007 instanceof CstType) {
                return Dops.o\u3007\u30070\u3007;
            }
            if (oo0o\u3007\u3007 instanceof CstString) {
                return Dops.\u3007oOO8O8;
            }
            if (oo0o\u3007\u3007 instanceof CstMethodHandle) {
                return Dops.\u3007\u300708O;
            }
            if (oo0o\u3007\u3007 instanceof CstProtoRef) {
                return Dops.O0O;
            }
            throw new RuntimeException("Unexpected constant type");
        }
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.android.dx.dex.code;

import com.android.dx.util.Hex;
import com.android.dx.rop.code.RegisterSpecList;
import com.android.dx.rop.code.SourcePosition;
import com.android.dx.rop.cst.Constant;

public final class MultiCstInsn extends FixedSizeInsn
{
    private final Constant[] Oo08;
    private final int[] o\u30070;
    private int \u3007\u3007888;
    
    private MultiCstInsn(final Dop dop, final SourcePosition sourcePosition, final RegisterSpecList list, final Constant[] oo08, final int[] o\u30070, final int \u3007\u3007888) {
        super(dop, sourcePosition, list);
        this.Oo08 = oo08;
        this.o\u30070 = o\u30070;
        this.\u3007\u3007888 = \u3007\u3007888;
    }
    
    @Override
    public String O8() {
        return this.\u3007080();
    }
    
    public int O8ooOoo\u3007(final int i) {
        if (this.o\u3007\u30070\u3007(i)) {
            return this.o\u30070[i];
        }
        final StringBuilder sb = new StringBuilder();
        sb.append("index not yet set for constant ");
        sb.append(i);
        sb.append(" value = ");
        sb.append(this.Oo08[i]);
        throw new IllegalStateException(sb.toString());
    }
    
    public void OOO\u3007O0(final int \u3007\u3007888) {
        if (\u3007\u3007888 < 0) {
            throw new IllegalArgumentException("index < 0");
        }
        if (!this.\u30070000OOO()) {
            this.\u3007\u3007888 = \u3007\u3007888;
            return;
        }
        throw new IllegalStateException("class index already set");
    }
    
    public Constant O\u30078O8\u3007008(final int n) {
        return this.Oo08[n];
    }
    
    @Override
    public DalvInsn oo88o8O(final Dop dop) {
        return new MultiCstInsn(dop, this.OO0o\u3007\u3007(), this.Oooo8o0\u3007(), this.Oo08, this.o\u30070, this.\u3007\u3007888);
    }
    
    public void oo\u3007(final int n, final int n2) {
        if (n2 < 0) {
            throw new IllegalArgumentException("index < 0");
        }
        if (!this.o\u3007\u30070\u3007(n)) {
            this.o\u30070[n] = n2;
            return;
        }
        throw new IllegalStateException("index already set");
    }
    
    @Override
    public DalvInsn o\u3007O8\u3007\u3007o(final RegisterSpecList list) {
        return new MultiCstInsn(this.\u3007O8o08O(), this.OO0o\u3007\u3007(), list, this.Oo08, this.o\u30070, this.\u3007\u3007888);
    }
    
    public boolean o\u3007\u30070\u3007(final int n) {
        return this.o\u30070[n] != -1;
    }
    
    public boolean \u30070000OOO() {
        return this.\u3007\u3007888 != -1;
    }
    
    @Override
    protected String \u3007080() {
        final StringBuilder sb = new StringBuilder();
        for (int i = 0; i < this.Oo08.length; ++i) {
            if (sb.length() > 0) {
                sb.append(", ");
            }
            sb.append(this.Oo08[i].toHuman());
        }
        return sb.toString();
    }
    
    public int \u3007oOO8O8() {
        return this.Oo08.length;
    }
    
    @Override
    public String \u3007o\u3007() {
        final StringBuilder sb = new StringBuilder();
        for (int i = 0; i < this.Oo08.length; ++i) {
            if (!this.o\u3007\u30070\u3007(i)) {
                return "";
            }
            if (i > 0) {
                sb.append(", ");
            }
            sb.append(this.O\u30078O8\u3007008(i).Oo08());
            sb.append('@');
            final int o8ooOoo\u3007 = this.O8ooOoo\u3007(i);
            if (o8ooOoo\u3007 < 65536) {
                sb.append(Hex.Oo08(o8ooOoo\u3007));
            }
            else {
                sb.append(Hex.oO80(o8ooOoo\u3007));
            }
        }
        return sb.toString();
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.android.dx.dex.code;

import com.android.dx.util.TwoColumnOutput;
import com.android.dx.ssa.RegisterMapper;
import com.android.dx.util.Hex;
import com.android.dx.util.AnnotatedOutput;
import java.util.BitSet;
import com.android.dx.rop.code.RegisterSpec;
import com.android.dx.rop.code.SourcePosition;
import com.android.dx.rop.code.RegisterSpecList;

public abstract class DalvInsn
{
    private final RegisterSpecList O8;
    private int \u3007080;
    private final Dop \u3007o00\u3007\u3007Oo;
    private final SourcePosition \u3007o\u3007;
    
    public DalvInsn(final Dop \u3007o00\u3007\u3007Oo, final SourcePosition \u3007o\u3007, final RegisterSpecList o8) {
        if (\u3007o00\u3007\u3007Oo == null) {
            throw new NullPointerException("opcode == null");
        }
        if (\u3007o\u3007 == null) {
            throw new NullPointerException("position == null");
        }
        if (o8 != null) {
            this.\u3007080 = -1;
            this.\u3007o00\u3007\u3007Oo = \u3007o00\u3007\u3007Oo;
            this.\u3007o\u3007 = \u3007o\u3007;
            this.O8 = o8;
            return;
        }
        throw new NullPointerException("registers == null");
    }
    
    public static SimpleInsn OoO8(final SourcePosition sourcePosition, final RegisterSpec registerSpec, final RegisterSpec registerSpec2) {
        final int oo80 = registerSpec.oO80();
        boolean b = true;
        if (oo80 != 1) {
            b = false;
        }
        final boolean \u3007\u3007808\u3007 = registerSpec.getType().\u3007\u3007808\u3007();
        final int \u30078o8o\u3007 = registerSpec.\u30078o8o\u3007();
        Dop dop;
        if ((registerSpec2.\u30078o8o\u3007() | \u30078o8o\u3007) < 16) {
            if (\u3007\u3007808\u3007) {
                dop = Dops.OO0o\u3007\u3007\u3007\u30070;
            }
            else if (b) {
                dop = Dops.O8;
            }
            else {
                dop = Dops.\u3007\u3007888;
            }
        }
        else if (\u30078o8o\u3007 < 256) {
            if (\u3007\u3007808\u3007) {
                dop = Dops.\u30078o8o\u3007;
            }
            else if (b) {
                dop = Dops.Oo08;
            }
            else {
                dop = Dops.oO80;
            }
        }
        else if (\u3007\u3007808\u3007) {
            dop = Dops.\u3007O8o08O;
        }
        else if (b) {
            dop = Dops.o\u30070;
        }
        else {
            dop = Dops.\u300780\u3007808\u3007O;
        }
        return new SimpleInsn(dop, sourcePosition, RegisterSpecList.OoO8(registerSpec, registerSpec2));
    }
    
    public String O8() {
        throw new UnsupportedOperationException("Not supported.");
    }
    
    public final SourcePosition OO0o\u3007\u3007() {
        return this.\u3007o\u3007;
    }
    
    public final int OO0o\u3007\u3007\u3007\u30070(final BitSet set) {
        int i = this.\u3007O\u3007() ? 1 : 0;
        final int size = this.O8.size();
        int a = 0;
        int oo80;
        if (i != 0 && !set.get(0)) {
            oo80 = this.O8.\u3007O00(0).oO80();
        }
        else {
            oo80 = 0;
        }
        while (i < size) {
            int n = a;
            if (!set.get(i)) {
                n = a + this.O8.\u3007O00(i).oO80();
            }
            ++i;
            a = n;
        }
        return Math.max(a, oo80);
    }
    
    public DalvInsn Oo08(final BitSet set) {
        final RegisterSpecList o8 = this.O8;
        final boolean value = set.get(0);
        if (this.\u3007O\u3007()) {
            set.set(0);
        }
        final RegisterSpecList oo88o8O = o8.oo88o8O(set);
        if (this.\u3007O\u3007()) {
            set.set(0, value);
        }
        if (oo88o8O.size() == 0) {
            return null;
        }
        return new HighRegisterPrefix(this.\u3007o\u3007, oo88o8O);
    }
    
    public final RegisterSpecList Oooo8o0\u3007() {
        return this.O8;
    }
    
    public final void o800o8O(final int \u3007080) {
        if (\u3007080 >= 0) {
            this.\u3007080 = \u3007080;
            return;
        }
        throw new IllegalArgumentException("address < 0");
    }
    
    public final int oO80() {
        final int \u3007080 = this.\u3007080;
        if (\u3007080 >= 0) {
            return \u3007080;
        }
        throw new RuntimeException("address not yet known");
    }
    
    public abstract DalvInsn oo88o8O(final Dop p0);
    
    public DalvInsn o\u30070(final BitSet set) {
        if (this.\u3007O\u3007() && !set.get(0)) {
            final RegisterSpec \u3007o00 = this.O8.\u3007O00(0);
            return OoO8(this.\u3007o\u3007, \u3007o00, \u3007o00.oo88o8O(0));
        }
        return null;
    }
    
    public abstract DalvInsn o\u3007O8\u3007\u3007o(final RegisterSpecList p0);
    
    @Override
    public final String toString() {
        final StringBuilder sb = new StringBuilder(100);
        sb.append(this.\u3007O00());
        sb.append(' ');
        sb.append(this.\u3007o\u3007);
        sb.append(": ");
        sb.append(this.\u3007o00\u3007\u3007Oo.\u3007o\u3007());
        boolean b;
        if (this.O8.size() != 0) {
            sb.append(this.O8.\u3007O8o08O(" ", ", ", null));
            b = true;
        }
        else {
            b = false;
        }
        final String \u3007080 = this.\u3007080();
        if (\u3007080 != null) {
            if (b) {
                sb.append(',');
            }
            sb.append(' ');
            sb.append(\u3007080);
        }
        return sb.toString();
    }
    
    public abstract void \u300700(final AnnotatedOutput p0);
    
    protected abstract String \u3007080();
    
    protected abstract String \u30070\u3007O0088o(final boolean p0);
    
    public DalvInsn \u300780\u3007808\u3007O() {
        return this.o\u3007O8\u3007\u3007o(this.O8.\u3007oo\u3007(0, this.\u3007O\u3007(), null));
    }
    
    public final int \u30078o8o\u3007() {
        return this.oO80() + this.\u3007o00\u3007\u3007Oo();
    }
    
    public final String \u3007O00() {
        final int \u3007080 = this.\u3007080;
        if (\u3007080 != -1) {
            return String.format("%04x", \u3007080);
        }
        return Hex.oO80(System.identityHashCode(this));
    }
    
    public DalvInsn \u3007O888o0o(final RegisterMapper registerMapper) {
        return this.o\u3007O8\u3007\u3007o(registerMapper.\u3007o\u3007(this.Oooo8o0\u3007()));
    }
    
    public final Dop \u3007O8o08O() {
        return this.\u3007o00\u3007\u3007Oo;
    }
    
    public final boolean \u3007O\u3007() {
        return this.\u3007o00\u3007\u3007Oo.\u3007\u3007888();
    }
    
    public abstract int \u3007o00\u3007\u3007Oo();
    
    public abstract DalvInsn \u3007oo\u3007(final int p0);
    
    public String \u3007o\u3007() {
        throw new UnsupportedOperationException("Not supported.");
    }
    
    public final boolean \u3007\u3007808\u3007() {
        return this.\u3007080 >= 0;
    }
    
    public DalvInsn \u3007\u3007888(final BitSet set) {
        return this.o\u3007O8\u3007\u3007o(this.O8.\u3007oo\u3007(0, this.\u3007O\u3007(), set));
    }
    
    public final String \u3007\u30078O0\u30078(String string, int length, final boolean b) {
        final String \u30070\u3007O0088o = this.\u30070\u3007O0088o(b);
        if (\u30070\u3007O0088o == null) {
            return null;
        }
        final StringBuilder sb = new StringBuilder();
        sb.append(string);
        sb.append(this.\u3007O00());
        sb.append(": ");
        string = sb.toString();
        final int length2 = string.length();
        if (length == 0) {
            length = \u30070\u3007O0088o.length();
        }
        else {
            length -= length2;
        }
        return TwoColumnOutput.oO80(string, length2, "", \u30070\u3007O0088o, length);
    }
}

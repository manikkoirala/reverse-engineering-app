// 
// Decompiled by Procyon v0.6.0
// 

package com.android.dx.dex.code;

import com.android.dx.util.AnnotatedOutput;
import com.android.dx.rop.type.TypeBearer;
import com.android.dx.rop.code.RegisterSpec;
import com.android.dx.rop.code.RegisterSpecList;
import com.android.dx.rop.code.SourcePosition;

public final class HighRegisterPrefix extends VariableSizeInsn
{
    private SimpleInsn[] Oo08;
    
    public HighRegisterPrefix(final SourcePosition sourcePosition, final RegisterSpecList list) {
        super(sourcePosition, list);
        if (list.size() != 0) {
            this.Oo08 = null;
            return;
        }
        throw new IllegalArgumentException("registers.size() == 0");
    }
    
    private static SimpleInsn O8ooOoo\u3007(final RegisterSpec registerSpec, final int n) {
        return DalvInsn.OoO8(SourcePosition.O8, RegisterSpec.\u3007O\u3007(n, registerSpec.getType()), registerSpec);
    }
    
    private void O\u30078O8\u3007008() {
        if (this.Oo08 != null) {
            return;
        }
        final RegisterSpecList oooo8o0\u3007 = this.Oooo8o0\u3007();
        final int size = oooo8o0\u3007.size();
        this.Oo08 = new SimpleInsn[size];
        int i = 0;
        int n = 0;
        while (i < size) {
            final RegisterSpec \u3007o00 = oooo8o0\u3007.\u3007O00(i);
            this.Oo08[i] = O8ooOoo\u3007(\u3007o00, n);
            n += \u3007o00.oO80();
            ++i;
        }
    }
    
    @Override
    public DalvInsn o\u3007O8\u3007\u3007o(final RegisterSpecList list) {
        return new HighRegisterPrefix(this.OO0o\u3007\u3007(), list);
    }
    
    @Override
    public void \u300700(final AnnotatedOutput annotatedOutput) {
        this.O\u30078O8\u3007008();
        final SimpleInsn[] oo08 = this.Oo08;
        for (int length = oo08.length, i = 0; i < length; ++i) {
            oo08[i].\u300700(annotatedOutput);
        }
    }
    
    @Override
    protected String \u3007080() {
        return null;
    }
    
    @Override
    protected String \u30070\u3007O0088o(final boolean b) {
        final RegisterSpecList oooo8o0\u3007 = this.Oooo8o0\u3007();
        final int size = oooo8o0\u3007.size();
        final StringBuilder sb = new StringBuilder(100);
        int i = 0;
        int n = 0;
        while (i < size) {
            final RegisterSpec \u3007o00 = oooo8o0\u3007.\u3007O00(i);
            final SimpleInsn o8ooOoo\u3007 = O8ooOoo\u3007(\u3007o00, n);
            if (i != 0) {
                sb.append('\n');
            }
            sb.append(o8ooOoo\u3007.\u30070\u3007O0088o(b));
            n += \u3007o00.oO80();
            ++i;
        }
        return sb.toString();
    }
    
    @Override
    public int \u3007o00\u3007\u3007Oo() {
        this.O\u30078O8\u3007008();
        final SimpleInsn[] oo08 = this.Oo08;
        final int length = oo08.length;
        int i = 0;
        int n = 0;
        while (i < length) {
            n += oo08[i].\u3007o00\u3007\u3007Oo();
            ++i;
        }
        return n;
    }
}

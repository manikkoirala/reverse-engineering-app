// 
// Decompiled by Procyon v0.6.0
// 

package com.android.dx.dex.code;

import com.android.dx.rop.cst.Constant;
import com.android.dx.rop.cst.CstProtoRef;
import com.android.dx.rop.cst.CstCallSiteRef;
import com.android.dx.rop.cst.CstBaseMethodRef;
import com.android.dex.util.ExceptionWithContext;
import com.android.dx.util.AnnotatedOutput;
import java.util.ArrayList;
import com.android.dx.util.FixedSizeList;

public final class DalvInsnList extends FixedSizeList
{
    private final int OO;
    
    public DalvInsnList(final int n, final int oo) {
        super(n);
        this.OO = oo;
    }
    
    public static DalvInsnList \u30070\u3007O0088o(final ArrayList<DalvInsn> list, int i) {
        final int size = list.size();
        final DalvInsnList list2 = new DalvInsnList(size, i);
        for (i = 0; i < size; ++i) {
            list2.OoO8(i, list.get(i));
        }
        list2.Oo08();
        return list2;
    }
    
    public void OoO8(final int n, final DalvInsn dalvInsn) {
        this.OO0o\u3007\u3007\u3007\u30070(n, dalvInsn);
    }
    
    public void o800o8O(final AnnotatedOutput annotatedOutput) {
        final int cursor = annotatedOutput.getCursor();
        final int size = this.size();
        final boolean \u3007o\u3007 = annotatedOutput.\u3007o\u3007();
        int i;
        final int n = i = 0;
        if (\u3007o\u3007) {
            final boolean oo0o\u3007\u3007\u3007\u30070 = annotatedOutput.OO0o\u3007\u3007\u3007\u30070();
            int n2 = 0;
            while (true) {
                i = n;
                if (n2 >= size) {
                    break;
                }
                final DalvInsn dalvInsn = (DalvInsn)this.oO80(n2);
                final int n3 = dalvInsn.\u3007o00\u3007\u3007Oo() * 2;
                String \u3007\u30078O0\u30078;
                if (n3 == 0 && !oo0o\u3007\u3007\u3007\u30070) {
                    \u3007\u30078O0\u30078 = null;
                }
                else {
                    \u3007\u30078O0\u30078 = dalvInsn.\u3007\u30078O0\u30078("  ", annotatedOutput.\u3007\u3007888(), true);
                }
                if (\u3007\u30078O0\u30078 != null) {
                    annotatedOutput.oO80(n3, \u3007\u30078O0\u30078);
                }
                else if (n3 != 0) {
                    annotatedOutput.oO80(n3, "");
                }
                ++n2;
            }
        }
        while (i < size) {
            final DalvInsn obj = (DalvInsn)this.oO80(i);
            try {
                obj.\u300700(annotatedOutput);
                ++i;
                continue;
            }
            catch (final RuntimeException ex) {
                final StringBuilder sb = new StringBuilder();
                sb.append("...while writing ");
                sb.append(obj);
                throw ExceptionWithContext.withContext(ex, sb.toString());
            }
            break;
        }
        final int j = (annotatedOutput.getCursor() - cursor) / 2;
        if (j == this.\u3007\u3007808\u3007()) {
            return;
        }
        final StringBuilder sb2 = new StringBuilder();
        sb2.append("write length mismatch; expected ");
        sb2.append(this.\u3007\u3007808\u3007());
        sb2.append(" but actually wrote ");
        sb2.append(j);
        throw new RuntimeException(sb2.toString());
    }
    
    public int \u3007O00() {
        final int size = this.size();
        int i = 0;
        int n = 0;
        while (i < size) {
            final DalvInsn dalvInsn = (DalvInsn)this.oO80(i);
            final boolean b = dalvInsn instanceof CstInsn;
            boolean b2 = true;
            int n3 = 0;
            Label_0200: {
                int n2;
                if (b) {
                    final Constant o\u30078O8\u3007008 = ((CstInsn)dalvInsn).O\u30078O8\u3007008();
                    if (o\u30078O8\u3007008 instanceof CstBaseMethodRef) {
                        final CstBaseMethodRef cstBaseMethodRef = (CstBaseMethodRef)o\u30078O8\u3007008;
                        if (dalvInsn.\u3007O8o08O().\u3007080() != 113) {
                            b2 = false;
                        }
                        n2 = cstBaseMethodRef.oO80(b2);
                    }
                    else if (o\u30078O8\u3007008 instanceof CstCallSiteRef) {
                        n2 = ((CstCallSiteRef)o\u30078O8\u3007008).\u3007\u3007888().Oo08().\u3007O00();
                    }
                    else {
                        n2 = 0;
                    }
                }
                else {
                    n3 = n;
                    if (!(dalvInsn instanceof MultiCstInsn)) {
                        break Label_0200;
                    }
                    if (dalvInsn.\u3007O8o08O().\u3007080() != 250) {
                        throw new RuntimeException("Expecting invoke-polymorphic");
                    }
                    n2 = ((CstProtoRef)((MultiCstInsn)dalvInsn).O\u30078O8\u3007008(1)).o\u30070().Oo08().\u3007O00() + 1;
                }
                n3 = n;
                if (n2 > n) {
                    n3 = n2;
                }
            }
            ++i;
            n = n3;
        }
        return n;
    }
    
    public DalvInsn \u3007O\u3007(final int n) {
        return (DalvInsn)this.oO80(n);
    }
    
    public int \u3007\u3007808\u3007() {
        final int size = this.size();
        if (size == 0) {
            return 0;
        }
        return this.\u3007O\u3007(size - 1).\u30078o8o\u3007();
    }
    
    public int \u3007\u30078O0\u30078() {
        return this.OO;
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.android.dx.dex.code;

import com.android.dx.rop.cst.Constant;
import com.android.dx.rop.cst.CstType;
import com.android.dx.util.Hex;
import com.android.dx.util.FixedSizeList;

public final class CatchHandlerList extends FixedSizeList implements Comparable<CatchHandlerList>
{
    public static final CatchHandlerList OO;
    
    static {
        OO = new CatchHandlerList(0);
    }
    
    public CatchHandlerList(final int n) {
        super(n);
    }
    
    @Override
    public String toHuman() {
        return this.\u30070\u3007O0088o("", "");
    }
    
    public String \u30070\u3007O0088o(final String s, final String str) {
        final StringBuilder sb = new StringBuilder(100);
        final int size = this.size();
        sb.append(s);
        sb.append(str);
        sb.append("catch ");
        for (int i = 0; i < size; ++i) {
            final Entry \u3007o00 = this.\u3007O00(i);
            if (i != 0) {
                sb.append(",\n");
                sb.append(s);
                sb.append("  ");
            }
            if (i == size - 1 && this.\u3007\u3007808\u3007()) {
                sb.append("<any>");
            }
            else {
                sb.append(\u3007o00.\u3007o00\u3007\u3007Oo().toHuman());
            }
            sb.append(" -> ");
            sb.append(Hex.o\u30070(\u3007o00.\u3007o\u3007()));
        }
        return sb.toString();
    }
    
    public Entry \u3007O00(final int n) {
        return (Entry)this.oO80(n);
    }
    
    public int \u3007O\u3007(final CatchHandlerList list) {
        if (this == list) {
            return 0;
        }
        final int size = this.size();
        final int size2 = list.size();
        for (int min = Math.min(size, size2), i = 0; i < min; ++i) {
            final int \u3007080 = this.\u3007O00(i).\u3007080(list.\u3007O00(i));
            if (\u3007080 != 0) {
                return \u3007080;
            }
        }
        if (size < size2) {
            return -1;
        }
        if (size > size2) {
            return 1;
        }
        return 0;
    }
    
    public boolean \u3007\u3007808\u3007() {
        final int size = this.size();
        return size != 0 && this.\u3007O00(size - 1).\u3007o00\u3007\u3007Oo().equals(CstType.\u300708O\u300700\u3007o);
    }
    
    public void \u3007\u30078O0\u30078(final int n, final CstType cstType, final int n2) {
        this.OO0o\u3007\u3007\u3007\u30070(n, new Entry(cstType, n2));
    }
    
    public static class Entry implements Comparable<Entry>
    {
        private final CstType o0;
        private final int \u3007OOo8\u30070;
        
        public Entry(final CstType o0, final int \u3007oOo8\u30070) {
            if (\u3007oOo8\u30070 < 0) {
                throw new IllegalArgumentException("handler < 0");
            }
            if (o0 != null) {
                this.\u3007OOo8\u30070 = \u3007oOo8\u30070;
                this.o0 = o0;
                return;
            }
            throw new NullPointerException("exceptionType == null");
        }
        
        @Override
        public boolean equals(final Object o) {
            final boolean b = o instanceof Entry;
            boolean b2 = false;
            if (b) {
                b2 = b2;
                if (this.\u3007080((Entry)o) == 0) {
                    b2 = true;
                }
            }
            return b2;
        }
        
        @Override
        public int hashCode() {
            return this.\u3007OOo8\u30070 * 31 + this.o0.hashCode();
        }
        
        public int \u3007080(final Entry entry) {
            final int \u3007oOo8\u30070 = this.\u3007OOo8\u30070;
            final int \u3007oOo8\u30072 = entry.\u3007OOo8\u30070;
            if (\u3007oOo8\u30070 < \u3007oOo8\u30072) {
                return -1;
            }
            if (\u3007oOo8\u30070 > \u3007oOo8\u30072) {
                return 1;
            }
            return this.o0.\u3007o\u3007(entry.o0);
        }
        
        public CstType \u3007o00\u3007\u3007Oo() {
            return this.o0;
        }
        
        public int \u3007o\u3007() {
            return this.\u3007OOo8\u30070;
        }
    }
}

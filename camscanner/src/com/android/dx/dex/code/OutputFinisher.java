// 
// Decompiled by Procyon v0.6.0
// 

package com.android.dx.dex.code;

import java.util.BitSet;
import java.util.HashSet;
import com.android.dx.rop.cst.Constant;
import com.android.dx.rop.cst.CstMemberRef;
import com.android.dx.rop.code.RegisterSpecList;
import java.util.Iterator;
import com.android.dx.rop.code.RegisterSpec;
import com.android.dx.rop.code.RegisterSpecSet;
import com.android.dex.DexException;
import com.android.dx.ssa.RegisterMapper;
import com.android.dx.ssa.BasicRegisterMapper;
import java.util.ArrayList;
import com.android.dx.dex.DexOptions;

public final class OutputFinisher
{
    private boolean O8;
    private boolean Oo08;
    private final int oO80;
    private int o\u30070;
    private final DexOptions \u3007080;
    private final int \u3007o00\u3007\u3007Oo;
    private ArrayList<DalvInsn> \u3007o\u3007;
    private int \u3007\u3007888;
    
    public OutputFinisher(final DexOptions \u3007080, final int initialCapacity, final int \u3007o00\u3007\u3007Oo, final int oo80) {
        this.\u3007080 = \u3007080;
        this.\u3007o00\u3007\u3007Oo = \u3007o00\u3007\u3007Oo;
        this.\u3007o\u3007 = new ArrayList<DalvInsn>(initialCapacity);
        this.o\u30070 = -1;
        this.O8 = false;
        this.Oo08 = false;
        this.oO80 = oo80;
    }
    
    private void O8(final int n) {
        this.O8ooOoo\u3007(n);
        this.\u3007\u3007888 += n;
    }
    
    private void O8ooOoo\u3007(final int n) {
        final int size = this.\u3007o\u3007.size();
        final int n2 = this.\u3007o00\u3007\u3007Oo + this.o\u30070 + this.\u3007\u3007888;
        final int oo80 = this.oO80;
        final BasicRegisterMapper basicRegisterMapper = new BasicRegisterMapper(n2);
        final int n3 = 0;
        int n4 = 0;
        int i;
        while (true) {
            i = n3;
            if (n4 >= n2) {
                break;
            }
            if (n4 >= n2 - oo80) {
                basicRegisterMapper.Oo08(n4, n4 + n, 1);
            }
            else {
                basicRegisterMapper.Oo08(n4, n4, 1);
            }
            ++n4;
        }
        while (i < size) {
            final DalvInsn dalvInsn = this.\u3007o\u3007.get(i);
            if (!(dalvInsn instanceof CodeAddress)) {
                this.\u3007o\u3007.set(i, dalvInsn.\u3007O888o0o(basicRegisterMapper));
            }
            ++i;
        }
    }
    
    private Dop OO0o\u3007\u3007(final DalvInsn obj) {
        final Dop oooo8o0\u3007 = this.Oooo8o0\u3007(obj.\u300780\u3007808\u3007O(), obj.\u3007O8o08O());
        if (oooo8o0\u3007 != null) {
            return oooo8o0\u3007;
        }
        final StringBuilder sb = new StringBuilder();
        sb.append("No expanded opcode for ");
        sb.append(obj);
        throw new DexException(sb.toString());
    }
    
    private void Oo08(final int n) {
        this.O\u30078O8\u3007008(n);
        this.o\u30070 += n;
    }
    
    private static boolean OoO8(final DalvInsn dalvInsn) {
        if (dalvInsn instanceof LocalSnapshot) {
            final RegisterSpecSet o\u30078O8\u3007008 = ((LocalSnapshot)dalvInsn).O\u30078O8\u3007008();
            for (int size = o\u30078O8\u3007008.size(), i = 0; i < size; ++i) {
                if (o800o8O(o\u30078O8\u3007008.\u300780\u3007808\u3007O(i))) {
                    return true;
                }
            }
        }
        else if (dalvInsn instanceof LocalStart && o800o8O(((LocalStart)dalvInsn).O\u30078O8\u3007008())) {
            return true;
        }
        return false;
    }
    
    private Dop Oooo8o0\u3007(final DalvInsn dalvInsn, Dop \u3007o00\u3007\u3007Oo) {
        while (\u3007o00\u3007\u3007Oo != null) {
            if (\u3007o00\u3007\u3007Oo.\u3007o00\u3007\u3007Oo().OO0o\u3007\u3007\u3007\u30070(dalvInsn)) {
                if (!this.\u3007080.\u3007o\u3007) {
                    break;
                }
                if (\u3007o00\u3007\u3007Oo.Oo08() != 26) {
                    break;
                }
            }
            \u3007o00\u3007\u3007Oo = Dops.\u3007o00\u3007\u3007Oo(\u3007o00\u3007\u3007Oo, this.\u3007080);
        }
        return \u3007o00\u3007\u3007Oo;
    }
    
    private void O\u30078O8\u3007008(final int n) {
        for (int size = this.\u3007o\u3007.size(), i = 0; i < size; ++i) {
            final DalvInsn dalvInsn = this.\u3007o\u3007.get(i);
            if (!(dalvInsn instanceof CodeAddress)) {
                this.\u3007o\u3007.set(i, dalvInsn.\u3007oo\u3007(n));
            }
        }
    }
    
    private static boolean o800o8O(final RegisterSpec registerSpec) {
        if (registerSpec == null) {
            return false;
        }
        registerSpec.\u300780\u3007808\u3007O();
        throw null;
    }
    
    private void oO80() {
        do {
            this.\u3007\u3007888();
        } while (this.\u3007O\u3007());
    }
    
    private void oo88o8O(final Dop[] array) {
        if (this.o\u30070 == 0) {
            for (int size = this.\u3007o\u3007.size(), i = 0; i < size; ++i) {
                final DalvInsn dalvInsn = this.\u3007o\u3007.get(i);
                final Dop \u3007o8o08O = dalvInsn.\u3007O8o08O();
                final Dop dop = array[i];
                if (\u3007o8o08O != dop) {
                    this.\u3007o\u3007.set(i, dalvInsn.oo88o8O(dop));
                }
            }
        }
        else {
            this.\u3007o\u3007 = this.\u3007oo\u3007(array);
        }
    }
    
    private void o\u30070(final Dop[] array) {
        do {
            final int \u3007o00\u3007\u3007Oo = this.\u3007o00\u3007\u3007Oo;
            final int o\u30070 = this.o\u30070;
            final int \u3007\u3007888 = this.\u3007\u3007888;
            final int oo80 = this.oO80;
            final Iterator<DalvInsn> iterator = this.\u3007o\u3007.iterator();
            int n = 0;
            int n2 = 0;
            int n3 = 0;
            int n4 = 0;
            while (iterator.hasNext()) {
                final RegisterSpecList oooo8o0\u3007 = iterator.next().Oooo8o0\u3007();
                int n5 = 0;
                int n6 = n4;
                int n7 = n3;
                int n8 = n2;
                int n9 = n;
                while (true) {
                    n = n9;
                    n2 = n8;
                    n3 = n7;
                    n4 = n6;
                    if (n5 >= oooo8o0\u3007.size()) {
                        break;
                    }
                    final RegisterSpec \u3007o00 = oooo8o0\u3007.\u3007O00(n5);
                    int n10 = n9;
                    int n11 = n8;
                    int n12 = n7;
                    int n13 = n6;
                    if (\u3007o00.Oooo8o0\u3007()) {
                        final boolean b = \u3007o00.\u30078o8o\u3007() >= \u3007o00\u3007\u3007Oo + o\u30070 + \u3007\u3007888 - oo80;
                        if (\u3007o00.\u3007\u3007808\u3007()) {
                            if (b) {
                                n11 = n8 + 1;
                                n10 = n9;
                                n12 = n7;
                                n13 = n6;
                            }
                            else {
                                n13 = n6 + 1;
                                n10 = n9;
                                n11 = n8;
                                n12 = n7;
                            }
                        }
                        else if (b) {
                            n10 = n9 + 1;
                            n11 = n8;
                            n12 = n7;
                            n13 = n6;
                        }
                        else {
                            n12 = n7 + 1;
                            n13 = n6;
                            n11 = n8;
                            n10 = n9;
                        }
                    }
                    ++n5;
                    n9 = n10;
                    n8 = n11;
                    n7 = n12;
                    n6 = n13;
                }
            }
            if (n > n2 && n3 > n4) {
                this.Oo08(1);
            }
            else if (n > n2) {
                this.O8(1);
            }
            else {
                if (n3 <= n4) {
                    break;
                }
                this.Oo08(1);
                if (this.oO80 == 0 || n2 <= n) {
                    continue;
                }
                this.O8(1);
            }
        } while (this.o\u3007O8\u3007\u3007o(array));
    }
    
    private boolean o\u3007O8\u3007\u3007o(final Dop[] array) {
        int o\u30070;
        if ((o\u30070 = this.o\u30070) < 0) {
            o\u30070 = 0;
        }
        boolean b = false;
        while (true) {
            final int \u3007o8o08O = this.\u3007O8o08O(array);
            if (o\u30070 >= \u3007o8o08O) {
                break;
            }
            for (int size = this.\u3007o\u3007.size(), i = 0; i < size; ++i) {
                final DalvInsn dalvInsn = this.\u3007o\u3007.get(i);
                if (!(dalvInsn instanceof CodeAddress)) {
                    this.\u3007o\u3007.set(i, dalvInsn.\u3007oo\u3007(\u3007o8o08O - o\u30070));
                }
            }
            b = true;
            o\u30070 = \u3007o8o08O;
        }
        this.o\u30070 = o\u30070;
        return b;
    }
    
    private static void \u300780\u3007808\u3007O(final CstInsn cstInsn, final DalvCode.AssignIndicesCallback assignIndicesCallback) {
        final Constant o\u30078O8\u3007008 = cstInsn.O\u30078O8\u3007008();
        final int \u3007080 = assignIndicesCallback.\u3007080(o\u30078O8\u3007008);
        if (\u3007080 >= 0) {
            cstInsn.o\u3007\u30070\u3007(\u3007080);
        }
        if (o\u30078O8\u3007008 instanceof CstMemberRef) {
            final int \u300781 = assignIndicesCallback.\u3007080(((CstMemberRef)o\u30078O8\u3007008).o\u30070());
            if (\u300781 >= 0) {
                cstInsn.\u30070000OOO(\u300781);
            }
        }
    }
    
    private static void \u30078o8o\u3007(final MultiCstInsn multiCstInsn, final DalvCode.AssignIndicesCallback assignIndicesCallback) {
        for (int i = 0; i < multiCstInsn.\u3007oOO8O8(); ++i) {
            final Constant o\u30078O8\u3007008 = multiCstInsn.O\u30078O8\u3007008(i);
            multiCstInsn.oo\u3007(i, assignIndicesCallback.\u3007080(o\u30078O8\u3007008));
            if (o\u30078O8\u3007008 instanceof CstMemberRef) {
                multiCstInsn.OOO\u3007O0(assignIndicesCallback.\u3007080(((CstMemberRef)o\u30078O8\u3007008).o\u30070()));
            }
        }
    }
    
    private Dop[] \u3007O888o0o() {
        final int size = this.\u3007o\u3007.size();
        final Dop[] array = new Dop[size];
        for (int i = 0; i < size; ++i) {
            array[i] = this.\u3007o\u3007.get(i).\u3007O8o08O();
        }
        return array;
    }
    
    private int \u3007O8o08O(final Dop[] array) {
        final int size = this.\u3007o\u3007.size();
        int o\u30070 = this.o\u30070;
        for (int i = 0; i < size; ++i) {
            final DalvInsn dalvInsn = this.\u3007o\u3007.get(i);
            final Dop dop = array[i];
            final Dop oooo8o0\u3007 = this.Oooo8o0\u3007(dalvInsn, dop);
            int n;
            if (oooo8o0\u3007 == null) {
                final int oo0o\u3007\u3007\u3007\u30070 = dalvInsn.OO0o\u3007\u3007\u3007\u30070(this.OO0o\u3007\u3007(dalvInsn).\u3007o00\u3007\u3007Oo().\u3007\u3007888(dalvInsn));
                if (oo0o\u3007\u3007\u3007\u30070 > (n = o\u30070)) {
                    n = oo0o\u3007\u3007\u3007\u30070;
                }
            }
            else {
                n = o\u30070;
                if (dop == oooo8o0\u3007) {
                    continue;
                }
            }
            array[i] = oooo8o0\u3007;
            o\u30070 = n;
        }
        return o\u30070;
    }
    
    private boolean \u3007O\u3007() {
        int size = this.\u3007o\u3007.size();
        int i = 0;
        boolean b = false;
        while (i < size) {
            final DalvInsn dalvInsn = this.\u3007o\u3007.get(i);
            Label_0207: {
                if (!(dalvInsn instanceof TargetInsn)) {
                    break Label_0207;
                }
                final Dop \u3007o8o08O = dalvInsn.\u3007O8o08O();
                final TargetInsn targetInsn = (TargetInsn)dalvInsn;
                if (\u3007o8o08O.\u3007o00\u3007\u3007Oo().\u3007o00\u3007\u3007Oo(targetInsn)) {
                    break Label_0207;
                }
                Label_0204: {
                    if (\u3007o8o08O.\u3007080() == 40) {
                        final Dop oooo8o0\u3007 = this.Oooo8o0\u3007(dalvInsn, \u3007o8o08O);
                        if (oooo8o0\u3007 != null) {
                            this.\u3007o\u3007.set(i, dalvInsn.oo88o8O(oooo8o0\u3007));
                            break Label_0204;
                        }
                        throw new UnsupportedOperationException("method too long");
                    }
                    try {
                        final ArrayList<DalvInsn> \u3007o\u3007 = this.\u3007o\u3007;
                        final int index = i + 1;
                        final CodeAddress codeAddress = \u3007o\u3007.get(index);
                        this.\u3007o\u3007.set(i, new TargetInsn(Dops.\u3007\u30070o, targetInsn.OO0o\u3007\u3007(), RegisterSpecList.OO, targetInsn.O\u30078O8\u3007008()));
                        this.\u3007o\u3007.add(i, targetInsn.o\u3007\u30070\u3007(codeAddress));
                        ++size;
                        i = index;
                        b = true;
                        ++i;
                        continue;
                    }
                    catch (final ClassCastException ex) {
                        throw new IllegalStateException("unpaired TargetInsn");
                    }
                    catch (final IndexOutOfBoundsException ex2) {
                        throw new IllegalStateException("unpaired TargetInsn (dangling)");
                    }
                }
            }
            break;
        }
        return b;
    }
    
    private static void \u3007o00\u3007\u3007Oo(final HashSet<Constant> set, final DalvInsn dalvInsn) {
        if (dalvInsn instanceof CstInsn) {
            set.add(((CstInsn)dalvInsn).O\u30078O8\u3007008());
        }
        else {
            final boolean b = dalvInsn instanceof MultiCstInsn;
            int i = 0;
            final int n = 0;
            if (b) {
                final MultiCstInsn multiCstInsn = (MultiCstInsn)dalvInsn;
                for (int j = n; j < multiCstInsn.\u3007oOO8O8(); ++j) {
                    set.add(multiCstInsn.O\u30078O8\u3007008(j));
                }
            }
            else if (dalvInsn instanceof LocalSnapshot) {
                for (RegisterSpecSet o\u30078O8\u3007008 = ((LocalSnapshot)dalvInsn).O\u30078O8\u3007008(); i < o\u30078O8\u3007008.size(); ++i) {
                    \u3007o\u3007(set, o\u30078O8\u3007008.\u300780\u3007808\u3007O(i));
                }
            }
            else if (dalvInsn instanceof LocalStart) {
                \u3007o\u3007(set, ((LocalStart)dalvInsn).O\u30078O8\u3007008());
            }
        }
    }
    
    private void \u3007oOO8O8(final DalvInsn dalvInsn) {
        if (!this.O8 && dalvInsn.OO0o\u3007\u3007().\u3007080() >= 0) {
            this.O8 = true;
        }
        if (!this.Oo08 && OoO8(dalvInsn)) {
            this.Oo08 = true;
        }
    }
    
    private ArrayList<DalvInsn> \u3007oo\u3007(final Dop[] array) {
        final int size = this.\u3007o\u3007.size();
        final ArrayList list = new ArrayList<DalvInsn>(size * 2);
        final ArrayList list2 = new ArrayList();
        for (int i = 0; i < size; ++i) {
            DalvInsn \u3007\u3007888 = this.\u3007o\u3007.get(i);
            final Dop \u3007o8o08O = \u3007\u3007888.\u3007O8o08O();
            Dop oo0o\u3007\u3007 = array[i];
            DalvInsn oo08;
            DalvInsn o\u30070;
            if (oo0o\u3007\u3007 != null) {
                oo08 = null;
                o\u30070 = null;
            }
            else {
                oo0o\u3007\u3007 = this.OO0o\u3007\u3007(\u3007\u3007888);
                final BitSet \u3007\u3007889 = oo0o\u3007\u3007.\u3007o00\u3007\u3007Oo().\u3007\u3007888(\u3007\u3007888);
                oo08 = \u3007\u3007888.Oo08(\u3007\u3007889);
                o\u30070 = \u3007\u3007888.o\u30070(\u3007\u3007889);
                \u3007\u3007888 = \u3007\u3007888.\u3007\u3007888(\u3007\u3007889);
            }
            if (\u3007\u3007888 instanceof CodeAddress) {
                final CodeAddress e = (CodeAddress)\u3007\u3007888;
                if (e.O\u30078O8\u3007008()) {
                    list2.add(e);
                    continue;
                }
            }
            if (oo08 != null) {
                list.add(oo08);
            }
            if (!(\u3007\u3007888 instanceof ZeroSizeInsn) && list2.size() > 0) {
                final Iterator iterator = list2.iterator();
                while (iterator.hasNext()) {
                    list.add((DalvInsn)iterator.next());
                }
                list2.clear();
            }
            DalvInsn oo88o8O = \u3007\u3007888;
            if (oo0o\u3007\u3007 != \u3007o8o08O) {
                oo88o8O = \u3007\u3007888.oo88o8O(oo0o\u3007\u3007);
            }
            list.add(oo88o8O);
            if (o\u30070 != null) {
                list.add(o\u30070);
            }
        }
        return (ArrayList<DalvInsn>)list;
    }
    
    private static void \u3007o\u3007(final HashSet<Constant> set, final RegisterSpec registerSpec) {
        if (registerSpec == null) {
            return;
        }
        registerSpec.\u300780\u3007808\u3007O();
        throw null;
    }
    
    private void \u3007\u3007888() {
        final int size = this.\u3007o\u3007.size();
        int i = 0;
        int n = 0;
        while (i < size) {
            final DalvInsn dalvInsn = this.\u3007o\u3007.get(i);
            dalvInsn.o800o8O(n);
            n += dalvInsn.\u3007o00\u3007\u3007Oo();
            ++i;
        }
    }
    
    public void OO0o\u3007\u3007\u3007\u30070(final DalvCode.AssignIndicesCallback assignIndicesCallback) {
        for (final DalvInsn dalvInsn : this.\u3007o\u3007) {
            if (dalvInsn instanceof CstInsn) {
                \u300780\u3007808\u3007O((CstInsn)dalvInsn, assignIndicesCallback);
            }
            else {
                if (!(dalvInsn instanceof MultiCstInsn)) {
                    continue;
                }
                \u30078o8o\u3007((MultiCstInsn)dalvInsn, assignIndicesCallback);
            }
        }
    }
    
    public void insert(final int index, final DalvInsn element) {
        this.\u3007o\u3007.add(index, element);
        this.\u3007oOO8O8(element);
    }
    
    public void \u300700(int n, final CodeAddress codeAddress) {
        n = this.\u3007o\u3007.size() - n - 1;
        try {
            this.\u3007o\u3007.set(n, this.\u3007o\u3007.get(n).o\u3007\u30070\u3007(codeAddress));
        }
        catch (final ClassCastException ex) {
            throw new IllegalArgumentException("non-reversible instruction");
        }
        catch (final IndexOutOfBoundsException ex2) {
            throw new IllegalArgumentException("too few instructions");
        }
    }
    
    public void \u3007080(final DalvInsn e) {
        this.\u3007o\u3007.add(e);
        this.\u3007oOO8O8(e);
    }
    
    public boolean \u30070\u3007O0088o() {
        return this.O8;
    }
    
    public HashSet<Constant> \u3007O00() {
        final HashSet set = new HashSet(20);
        final Iterator<DalvInsn> iterator = this.\u3007o\u3007.iterator();
        while (iterator.hasNext()) {
            \u3007o00\u3007\u3007Oo(set, iterator.next());
        }
        return set;
    }
    
    public DalvInsnList \u3007\u3007808\u3007() {
        if (this.o\u30070 < 0) {
            final Dop[] \u3007o888o0o = this.\u3007O888o0o();
            this.o\u3007O8\u3007\u3007o(\u3007o888o0o);
            if (this.\u3007080.\u3007080) {
                this.o\u30070(\u3007o888o0o);
            }
            this.oo88o8O(\u3007o888o0o);
            this.oO80();
            return DalvInsnList.\u30070\u3007O0088o(this.\u3007o\u3007, this.o\u30070 + this.\u3007o00\u3007\u3007Oo + this.\u3007\u3007888);
        }
        throw new UnsupportedOperationException("already processed");
    }
    
    public boolean \u3007\u30078O0\u30078() {
        return this.Oo08;
    }
}

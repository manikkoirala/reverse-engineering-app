// 
// Decompiled by Procyon v0.6.0
// 

package com.android.dx.dex.code;

import java.util.HashSet;
import com.android.dx.rop.code.BasicBlockList;
import java.util.ArrayList;
import com.android.dx.rop.type.TypeList;
import com.android.dx.util.IntList;
import com.android.dx.rop.cst.CstType;
import com.android.dx.rop.type.Type;
import com.android.dx.rop.code.BasicBlock;
import com.android.dx.rop.code.RopMethod;

public final class StdCatchBuilder implements CatchBuilder
{
    private final RopMethod \u3007080;
    private final int[] \u3007o00\u3007\u3007Oo;
    private final BlockAddresses \u3007o\u3007;
    
    public StdCatchBuilder(final RopMethod \u3007080, final int[] \u3007o00\u3007\u3007Oo, final BlockAddresses \u3007o\u3007) {
        if (\u3007080 == null) {
            throw new NullPointerException("method == null");
        }
        if (\u3007o00\u3007\u3007Oo == null) {
            throw new NullPointerException("order == null");
        }
        if (\u3007o\u3007 != null) {
            this.\u3007080 = \u3007080;
            this.\u3007o00\u3007\u3007Oo = \u3007o00\u3007\u3007Oo;
            this.\u3007o\u3007 = \u3007o\u3007;
            return;
        }
        throw new NullPointerException("addresses == null");
    }
    
    private static CatchHandlerList O8(final BasicBlock basicBlock, final BlockAddresses blockAddresses) {
        final IntList o\u30070 = basicBlock.o\u30070();
        final int size = o\u30070.size();
        final int o8 = basicBlock.O8();
        final TypeList oo08 = basicBlock.\u3007o\u3007().Oo08();
        final int size2 = oo08.size();
        if (size2 == 0) {
            return CatchHandlerList.OO;
        }
        if ((o8 != -1 || size == size2) && (o8 == -1 || (size == size2 + 1 && o8 == o\u30070.\u30078o8o\u3007(size2)))) {
            final int n = 0;
            int n2 = 0;
            int n3;
            while (true) {
                n3 = size2;
                if (n2 >= size2) {
                    break;
                }
                if (oo08.getType(n2).equals(Type.\u3007\u3007o\u3007)) {
                    n3 = n2 + 1;
                    break;
                }
                ++n2;
            }
            final CatchHandlerList list = new CatchHandlerList(n3);
            for (int i = n; i < n3; ++i) {
                list.\u3007\u30078O0\u30078(i, new CstType(oo08.getType(i)), blockAddresses.\u3007o\u3007(o\u30070.\u30078o8o\u3007(i)).oO80());
            }
            list.Oo08();
            return list;
        }
        throw new RuntimeException("shouldn't happen: weird successors list");
    }
    
    private static CatchTable.Entry Oo08(final BasicBlock basicBlock, final BasicBlock basicBlock2, final CatchHandlerList list, final BlockAddresses blockAddresses) {
        return new CatchTable.Entry(blockAddresses.\u3007o00\u3007\u3007Oo(basicBlock).oO80(), blockAddresses.\u3007080(basicBlock2).oO80(), list);
    }
    
    private static boolean o\u30070(final BasicBlock basicBlock, final BasicBlock basicBlock2, final BlockAddresses blockAddresses) {
        if (basicBlock == null) {
            throw new NullPointerException("start == null");
        }
        if (basicBlock2 != null) {
            return blockAddresses.\u3007080(basicBlock2).oO80() - blockAddresses.\u3007o00\u3007\u3007Oo(basicBlock).oO80() <= 65535;
        }
        throw new NullPointerException("end == null");
    }
    
    public static CatchTable \u3007o\u3007(final RopMethod ropMethod, final int[] array, final BlockAddresses blockAddresses) {
        final int length = array.length;
        final BasicBlockList \u3007o00\u3007\u3007Oo = ropMethod.\u3007o00\u3007\u3007Oo();
        final ArrayList list = new ArrayList<CatchTable.Entry>(length);
        CatchHandlerList oo = CatchHandlerList.OO;
        BasicBlock basicBlock = null;
        final int n = 0;
        BasicBlock basicBlock2 = null;
        for (int i = 0; i < length; ++i) {
            final BasicBlock \u3007oo\u3007 = \u3007o00\u3007\u3007Oo.\u3007oo\u3007(array[i]);
            if (\u3007oo\u3007.\u3007080()) {
                final CatchHandlerList o8 = O8(\u3007oo\u3007, blockAddresses);
                if (oo.size() != 0) {
                    if (oo.equals(o8) && o\u30070(basicBlock, \u3007oo\u3007, blockAddresses)) {
                        basicBlock2 = \u3007oo\u3007;
                        continue;
                    }
                    if (oo.size() != 0) {
                        list.add(Oo08(basicBlock, basicBlock2, oo, blockAddresses));
                    }
                }
                basicBlock = (basicBlock2 = \u3007oo\u3007);
                oo = o8;
            }
        }
        if (oo.size() != 0) {
            list.add(Oo08(basicBlock, basicBlock2, oo, blockAddresses));
        }
        final int size = list.size();
        if (size == 0) {
            return CatchTable.OO;
        }
        final CatchTable catchTable = new CatchTable(size);
        for (int j = n; j < size; ++j) {
            catchTable.\u3007O00(j, list.get(j));
        }
        catchTable.Oo08();
        return catchTable;
    }
    
    @Override
    public CatchTable build() {
        return \u3007o\u3007(this.\u3007080, this.\u3007o00\u3007\u3007Oo, this.\u3007o\u3007);
    }
    
    @Override
    public boolean \u3007080() {
        final BasicBlockList \u3007o00\u3007\u3007Oo = this.\u3007080.\u3007o00\u3007\u3007Oo();
        for (int size = \u3007o00\u3007\u3007Oo.size(), i = 0; i < size; ++i) {
            if (\u3007o00\u3007\u3007Oo.o800o8O(i).\u3007o\u3007().Oo08().size() != 0) {
                return true;
            }
        }
        return false;
    }
    
    @Override
    public HashSet<Type> \u3007o00\u3007\u3007Oo() {
        final HashSet set = new HashSet(20);
        final BasicBlockList \u3007o00\u3007\u3007Oo = this.\u3007080.\u3007o00\u3007\u3007Oo();
        for (int size = \u3007o00\u3007\u3007Oo.size(), i = 0; i < size; ++i) {
            final TypeList oo08 = \u3007o00\u3007\u3007Oo.o800o8O(i).\u3007o\u3007().Oo08();
            for (int size2 = oo08.size(), j = 0; j < size2; ++j) {
                set.add(oo08.getType(j));
            }
        }
        return set;
    }
}

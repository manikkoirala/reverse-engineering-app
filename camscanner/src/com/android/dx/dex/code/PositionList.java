// 
// Decompiled by Procyon v0.6.0
// 

package com.android.dx.dex.code;

import com.android.dx.rop.code.SourcePosition;
import com.android.dx.util.FixedSizeList;

public final class PositionList extends FixedSizeList
{
    public static final PositionList OO;
    
    static {
        OO = new PositionList(0);
    }
    
    public PositionList(final int n) {
        super(n);
    }
    
    public static PositionList \u3007O\u3007(final DalvInsnList list, int i) {
        if (i == 1) {
            return PositionList.OO;
        }
        if (i != 2 && i != 3) {
            throw new IllegalArgumentException("bogus howMuch");
        }
        final SourcePosition o8 = SourcePosition.O8;
        final int size = list.size();
        final Entry[] array = new Entry[size];
        final int n = 0;
        SourcePosition sourcePosition = o8;
        int j = 0;
        int n2 = 0;
        int n3 = 0;
        while (j < size) {
            final DalvInsn \u3007o\u3007 = list.\u3007O\u3007(j);
            int n4;
            int n5;
            SourcePosition sourcePosition2;
            if (\u3007o\u3007 instanceof CodeAddress) {
                n4 = 1;
                n5 = n2;
                sourcePosition2 = sourcePosition;
            }
            else {
                final SourcePosition oo0o\u3007\u3007 = \u3007o\u3007.OO0o\u3007\u3007();
                n5 = n2;
                sourcePosition2 = sourcePosition;
                n4 = n3;
                if (!oo0o\u3007\u3007.equals(o8)) {
                    if (oo0o\u3007\u3007.\u3007o00\u3007\u3007Oo(sourcePosition)) {
                        n5 = n2;
                        sourcePosition2 = sourcePosition;
                        n4 = n3;
                    }
                    else if (i == 3 && n3 == 0) {
                        n5 = n2;
                        sourcePosition2 = sourcePosition;
                        n4 = n3;
                    }
                    else {
                        array[n2] = new Entry(\u3007o\u3007.oO80(), oo0o\u3007\u3007);
                        n5 = n2 + 1;
                        sourcePosition2 = oo0o\u3007\u3007;
                        n4 = 0;
                    }
                }
            }
            ++j;
            n2 = n5;
            sourcePosition = sourcePosition2;
            n3 = n4;
        }
        final PositionList list2 = new PositionList(n2);
        for (i = n; i < n2; ++i) {
            list2.\u3007O00(i, array[i]);
        }
        list2.Oo08();
        return list2;
    }
    
    public void \u3007O00(final int n, final Entry entry) {
        this.OO0o\u3007\u3007\u3007\u30070(n, entry);
    }
    
    public Entry \u3007\u3007808\u3007(final int n) {
        return (Entry)this.oO80(n);
    }
    
    public static class Entry
    {
        private final int \u3007080;
        private final SourcePosition \u3007o00\u3007\u3007Oo;
        
        public Entry(final int \u3007080, final SourcePosition \u3007o00\u3007\u3007Oo) {
            if (\u3007080 < 0) {
                throw new IllegalArgumentException("address < 0");
            }
            if (\u3007o00\u3007\u3007Oo != null) {
                this.\u3007080 = \u3007080;
                this.\u3007o00\u3007\u3007Oo = \u3007o00\u3007\u3007Oo;
                return;
            }
            throw new NullPointerException("position == null");
        }
        
        public int \u3007080() {
            return this.\u3007080;
        }
        
        public SourcePosition \u3007o00\u3007\u3007Oo() {
            return this.\u3007o00\u3007\u3007Oo;
        }
    }
}

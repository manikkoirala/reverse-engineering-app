// 
// Decompiled by Procyon v0.6.0
// 

package com.android.dx.dex.code;

import com.android.dx.rop.code.RegisterSpecList;
import com.android.dx.rop.code.SourcePosition;

public abstract class VariableSizeInsn extends DalvInsn
{
    public VariableSizeInsn(final SourcePosition sourcePosition, final RegisterSpecList list) {
        super(Dops.\u3007o00\u3007\u3007Oo, sourcePosition, list);
    }
    
    @Override
    public final DalvInsn oo88o8O(final Dop dop) {
        throw new RuntimeException("unsupported");
    }
    
    @Override
    public final DalvInsn \u3007oo\u3007(final int n) {
        return this.o\u3007O8\u3007\u3007o(this.Oooo8o0\u3007().\u300700(n));
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.android.dx.dex.code;

import java.util.Iterator;
import com.android.dx.rop.type.TypeBearer;
import com.android.dx.rop.type.Type;
import java.util.Arrays;
import com.android.dx.rop.code.RegisterSpecSet;
import java.util.ArrayList;
import com.android.dx.rop.cst.CstString;
import com.android.dx.rop.cst.CstType;
import com.android.dx.rop.code.RegisterSpec;
import com.android.dx.util.FixedSizeList;

public final class LocalList extends FixedSizeList
{
    public static final LocalList OO;
    
    static {
        OO = new LocalList(0);
    }
    
    public LocalList(final int n) {
        super(n);
    }
    
    public static LocalList \u3007O\u3007(final DalvInsnList list) {
        final int size = list.size();
        final MakeState makeState = new MakeState(size);
        for (int i = 0; i < size; ++i) {
            final DalvInsn \u3007o\u3007 = list.\u3007O\u3007(i);
            if (\u3007o\u3007 instanceof LocalSnapshot) {
                makeState.\u300780\u3007808\u3007O(\u3007o\u3007.oO80(), ((LocalSnapshot)\u3007o\u3007).O\u30078O8\u3007008());
            }
            else if (\u3007o\u3007 instanceof LocalStart) {
                makeState.OO0o\u3007\u3007\u3007\u30070(\u3007o\u3007.oO80(), ((LocalStart)\u3007o\u3007).O\u30078O8\u3007008());
            }
        }
        return makeState.oO80();
    }
    
    public void \u3007O00(final int n, final Entry entry) {
        this.OO0o\u3007\u3007\u3007\u30070(n, entry);
    }
    
    public Entry \u3007\u3007808\u3007(final int n) {
        return (Entry)this.oO80(n);
    }
    
    public enum Disposition
    {
        private static final Disposition[] $VALUES;
        
        END_CLOBBERED_BY_NEXT, 
        END_CLOBBERED_BY_PREV, 
        END_MOVED, 
        END_REPLACED, 
        END_SIMPLY, 
        START;
    }
    
    public static class Entry implements Comparable<Entry>
    {
        private final RegisterSpec OO;
        private final int o0;
        private final CstType \u300708O\u300700\u3007o;
        private final Disposition \u3007OOo8\u30070;
        
        public Entry(final int n, final Disposition disposition, final RegisterSpec registerSpec) {
            if (n >= 0) {
                if (disposition != null) {
                    try {
                        registerSpec.\u300780\u3007808\u3007O();
                        throw new NullPointerException("spec.getLocalItem() == null");
                    }
                    catch (final NullPointerException ex) {
                        throw new NullPointerException("spec == null");
                    }
                }
                throw new NullPointerException("disposition == null");
            }
            throw new IllegalArgumentException("address < 0");
        }
        
        public CstString O8() {
            this.OO.\u300780\u3007808\u3007O();
            throw null;
        }
        
        public boolean OO0o\u3007\u3007\u3007\u30070(final RegisterSpec registerSpec) {
            return this.OO.\u3007\u3007888(registerSpec);
        }
        
        public int Oo08() {
            return this.OO.\u30078o8o\u3007();
        }
        
        @Override
        public boolean equals(final Object o) {
            final boolean b = o instanceof Entry;
            boolean b2 = false;
            if (!b) {
                return false;
            }
            if (this.\u3007080((Entry)o) == 0) {
                b2 = true;
            }
            return b2;
        }
        
        public CstType getType() {
            return this.\u300708O\u300700\u3007o;
        }
        
        public boolean oO80() {
            return this.\u3007OOo8\u30070 == Disposition.START;
        }
        
        public RegisterSpec o\u30070() {
            return this.OO;
        }
        
        @Override
        public String toString() {
            final StringBuilder sb = new StringBuilder();
            sb.append(Integer.toHexString(this.o0));
            sb.append(" ");
            sb.append(this.\u3007OOo8\u30070);
            sb.append(" ");
            sb.append(this.OO);
            return sb.toString();
        }
        
        public int \u3007080(final Entry entry) {
            final int o0 = this.o0;
            final int o2 = entry.o0;
            int n = -1;
            if (o0 < o2) {
                return -1;
            }
            if (o0 > o2) {
                return 1;
            }
            final boolean oo80 = this.oO80();
            if (oo80 != entry.oO80()) {
                if (oo80) {
                    n = 1;
                }
                return n;
            }
            return this.OO.Oo08(entry.OO);
        }
        
        public boolean \u300780\u3007808\u3007O(final Entry entry) {
            return this.OO0o\u3007\u3007\u3007\u30070(entry.OO);
        }
        
        public Entry \u30078o8o\u3007(final Disposition disposition) {
            if (disposition == this.\u3007OOo8\u30070) {
                return this;
            }
            return new Entry(this.o0, disposition, this.OO);
        }
        
        public int \u3007o00\u3007\u3007Oo() {
            return this.o0;
        }
        
        public Disposition \u3007o\u3007() {
            return this.\u3007OOo8\u30070;
        }
        
        public CstString \u3007\u3007888() {
            this.OO.\u300780\u3007808\u3007O();
            throw null;
        }
    }
    
    public static class MakeState
    {
        private int[] O8;
        private final int Oo08;
        private final ArrayList<Entry> \u3007080;
        private int \u3007o00\u3007\u3007Oo;
        private RegisterSpecSet \u3007o\u3007;
        
        public MakeState(final int initialCapacity) {
            this.\u3007080 = new ArrayList<Entry>(initialCapacity);
            this.\u3007o00\u3007\u3007Oo = 0;
            this.\u3007o\u3007 = null;
            this.O8 = null;
            this.Oo08 = 0;
        }
        
        private boolean O8(final int n, final RegisterSpec registerSpec) {
            int n2 = this.\u3007080.size() - 1;
            int n3;
            while (true) {
                n3 = 0;
                if (n2 < 0) {
                    break;
                }
                final Entry entry = this.\u3007080.get(n2);
                if (entry != null) {
                    if (entry.\u3007o00\u3007\u3007Oo() != n) {
                        return false;
                    }
                    if (entry.OO0o\u3007\u3007\u3007\u30070(registerSpec)) {
                        break;
                    }
                }
                --n2;
            }
            this.\u3007o\u3007.OO0o\u3007\u3007(registerSpec);
            final ArrayList<Entry> \u3007080 = this.\u3007080;
            final Entry entry2 = null;
            \u3007080.set(n2, null);
            ++this.\u3007o00\u3007\u3007Oo;
            final int \u30078o8o\u3007 = registerSpec.\u30078o8o\u3007();
            Entry entry3 = entry2;
            int n4;
            int n5;
            while (true) {
                n4 = n2 - 1;
                n5 = n3;
                if (n4 < 0) {
                    break;
                }
                final Entry entry4 = this.\u3007080.get(n4);
                if (entry4 == null) {
                    n2 = n4;
                    entry3 = entry4;
                }
                else {
                    n2 = n4;
                    entry3 = entry4;
                    if (entry4.o\u30070().\u30078o8o\u3007() == \u30078o8o\u3007) {
                        n5 = 1;
                        entry3 = entry4;
                        break;
                    }
                    continue;
                }
            }
            if (n5 != 0) {
                this.O8[\u30078o8o\u3007] = n4;
                if (entry3.\u3007o00\u3007\u3007Oo() == n) {
                    this.\u3007080.set(n4, entry3.\u30078o8o\u3007(Disposition.END_SIMPLY));
                }
            }
            return true;
        }
        
        private void \u3007080(int n, final int n2) {
            final int[] o8 = this.O8;
            final boolean b = o8 == null;
            final int oo08 = this.Oo08;
            if (n == oo08 && !b) {
                return;
            }
            if (n >= oo08) {
                if (b || n2 >= o8.length) {
                    n = n2 + 1;
                    final RegisterSpecSet \u3007o\u3007 = new RegisterSpecSet(n);
                    final int[] array = new int[n];
                    Arrays.fill(array, -1);
                    if (!b) {
                        \u3007o\u3007.\u3007O8o08O(this.\u3007o\u3007);
                        final int[] o9 = this.O8;
                        System.arraycopy(o9, 0, array, 0, o9.length);
                    }
                    this.\u3007o\u3007 = \u3007o\u3007;
                    this.O8 = array;
                }
                return;
            }
            throw new RuntimeException("shouldn't happen");
        }
        
        private void \u3007o00\u3007\u3007Oo(final int n, final Disposition disposition, final RegisterSpec registerSpec) {
            final int \u30078o8o\u3007 = registerSpec.\u30078o8o\u3007();
            this.\u3007080.add(new Entry(n, disposition, registerSpec));
            if (disposition == Disposition.START) {
                this.\u3007o\u3007.\u30078o8o\u3007(registerSpec);
                this.O8[\u30078o8o\u3007] = -1;
            }
            else {
                this.\u3007o\u3007.OO0o\u3007\u3007(registerSpec);
                this.O8[\u30078o8o\u3007] = this.\u3007080.size() - 1;
            }
        }
        
        private void \u3007o\u3007(final int n, final Disposition disposition, final RegisterSpec registerSpec) {
            if (disposition != Disposition.START) {
                final int n2 = this.O8[registerSpec.\u30078o8o\u3007()];
                if (n2 >= 0) {
                    final Entry entry = this.\u3007080.get(n2);
                    if (entry.\u3007o00\u3007\u3007Oo() == n && entry.o\u30070().equals(registerSpec)) {
                        this.\u3007080.set(n2, entry.\u30078o8o\u3007(disposition));
                        this.\u3007o\u3007.OO0o\u3007\u3007(registerSpec);
                        return;
                    }
                }
                this.o\u30070(n, registerSpec, disposition);
                return;
            }
            throw new RuntimeException("shouldn't happen");
        }
        
        private static RegisterSpec \u3007\u3007888(final RegisterSpec registerSpec) {
            RegisterSpec \u3007oo\u3007 = registerSpec;
            if (registerSpec != null) {
                \u3007oo\u3007 = registerSpec;
                if (registerSpec.getType() == Type.\u3007O\u3007\u3007O8) {
                    \u3007oo\u3007 = registerSpec.\u3007oo\u3007(Type.\u3007\u3007o\u3007);
                }
            }
            return \u3007oo\u3007;
        }
        
        public void OO0o\u3007\u3007\u3007\u30070(final int n, RegisterSpec \u3007\u3007888) {
            final int \u30078o8o\u3007 = \u3007\u3007888.\u30078o8o\u3007();
            \u3007\u3007888 = \u3007\u3007888(\u3007\u3007888);
            this.\u3007080(n, \u30078o8o\u3007);
            final RegisterSpec \u300780\u3007808\u3007O = this.\u3007o\u3007.\u300780\u3007808\u3007O(\u30078o8o\u3007);
            if (\u3007\u3007888.\u3007\u3007888(\u300780\u3007808\u3007O)) {
                return;
            }
            final RegisterSpec oo80 = this.\u3007o\u3007.oO80(\u3007\u3007888);
            if (oo80 != null) {
                this.\u3007o\u3007(n, Disposition.END_MOVED, oo80);
            }
            final int index = this.O8[\u30078o8o\u3007];
            if (\u300780\u3007808\u3007O != null) {
                this.\u3007o00\u3007\u3007Oo(n, Disposition.END_REPLACED, \u300780\u3007808\u3007O);
            }
            else if (index >= 0) {
                final Entry entry = this.\u3007080.get(index);
                if (entry.\u3007o00\u3007\u3007Oo() == n) {
                    if (entry.OO0o\u3007\u3007\u3007\u30070(\u3007\u3007888)) {
                        this.\u3007080.set(index, null);
                        ++this.\u3007o00\u3007\u3007Oo;
                        this.\u3007o\u3007.\u30078o8o\u3007(\u3007\u3007888);
                        this.O8[\u30078o8o\u3007] = -1;
                        return;
                    }
                    this.\u3007080.set(index, entry.\u30078o8o\u3007(Disposition.END_REPLACED));
                }
            }
            if (\u30078o8o\u3007 > 0) {
                final RegisterSpec \u300780\u3007808\u3007O2 = this.\u3007o\u3007.\u300780\u3007808\u3007O(\u30078o8o\u3007 - 1);
                if (\u300780\u3007808\u3007O2 != null && \u300780\u3007808\u3007O2.Oooo8o0\u3007()) {
                    this.\u3007o\u3007(n, Disposition.END_CLOBBERED_BY_NEXT, \u300780\u3007808\u3007O2);
                }
            }
            if (\u3007\u3007888.Oooo8o0\u3007()) {
                final RegisterSpec \u300780\u3007808\u3007O3 = this.\u3007o\u3007.\u300780\u3007808\u3007O(\u30078o8o\u3007 + 1);
                if (\u300780\u3007808\u3007O3 != null) {
                    this.\u3007o\u3007(n, Disposition.END_CLOBBERED_BY_PREV, \u300780\u3007808\u3007O3);
                }
            }
            this.\u3007o00\u3007\u3007Oo(n, Disposition.START, \u3007\u3007888);
        }
        
        public void Oo08(final int n, final RegisterSpec registerSpec) {
            this.o\u30070(n, registerSpec, Disposition.END_SIMPLY);
        }
        
        public LocalList oO80() {
            final int n = 0;
            this.\u3007080(Integer.MAX_VALUE, 0);
            final int size = this.\u3007080.size();
            final int n2 = size - this.\u3007o00\u3007\u3007Oo;
            if (n2 == 0) {
                return LocalList.OO;
            }
            final Entry[] array = new Entry[n2];
            if (size == n2) {
                this.\u3007080.toArray(array);
            }
            else {
                final Iterator<Entry> iterator = this.\u3007080.iterator();
                int n3 = 0;
                while (iterator.hasNext()) {
                    final Entry entry = iterator.next();
                    if (entry != null) {
                        array[n3] = entry;
                        ++n3;
                    }
                }
            }
            Arrays.sort(array);
            final LocalList list = new LocalList(n2);
            for (int i = n; i < n2; ++i) {
                list.\u3007O00(i, array[i]);
            }
            list.Oo08();
            return list;
        }
        
        public void o\u30070(final int n, RegisterSpec \u3007\u3007888, final Disposition disposition) {
            final int \u30078o8o\u3007 = \u3007\u3007888.\u30078o8o\u3007();
            \u3007\u3007888 = \u3007\u3007888(\u3007\u3007888);
            this.\u3007080(n, \u30078o8o\u3007);
            if (this.O8[\u30078o8o\u3007] >= 0) {
                return;
            }
            if (this.O8(n, \u3007\u3007888)) {
                return;
            }
            this.\u3007o00\u3007\u3007Oo(n, disposition, \u3007\u3007888);
        }
        
        public void \u300780\u3007808\u3007O(final int n, final RegisterSpecSet set) {
            final int oo0o\u3007\u3007\u3007\u30070 = set.OO0o\u3007\u3007\u3007\u30070();
            this.\u3007080(n, oo0o\u3007\u3007\u3007\u30070 - 1);
            for (int i = 0; i < oo0o\u3007\u3007\u3007\u30070; ++i) {
                final RegisterSpec \u300780\u3007808\u3007O = this.\u3007o\u3007.\u300780\u3007808\u3007O(i);
                final RegisterSpec \u3007\u3007888 = \u3007\u3007888(set.\u300780\u3007808\u3007O(i));
                if (\u300780\u3007808\u3007O == null) {
                    if (\u3007\u3007888 != null) {
                        this.OO0o\u3007\u3007\u3007\u30070(n, \u3007\u3007888);
                    }
                }
                else if (\u3007\u3007888 == null) {
                    this.Oo08(n, \u300780\u3007808\u3007O);
                }
                else if (!\u3007\u3007888.\u3007\u3007888(\u300780\u3007808\u3007O)) {
                    this.Oo08(n, \u300780\u3007808\u3007O);
                    this.OO0o\u3007\u3007\u3007\u30070(n, \u3007\u3007888);
                }
            }
        }
    }
}

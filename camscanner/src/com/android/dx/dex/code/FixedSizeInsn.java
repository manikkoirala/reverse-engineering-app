// 
// Decompiled by Procyon v0.6.0
// 

package com.android.dx.dex.code;

import com.android.dx.util.AnnotatedOutput;
import com.android.dx.rop.code.RegisterSpecList;
import com.android.dx.rop.code.SourcePosition;

public abstract class FixedSizeInsn extends DalvInsn
{
    public FixedSizeInsn(final Dop dop, final SourcePosition sourcePosition, final RegisterSpecList list) {
        super(dop, sourcePosition, list);
    }
    
    @Override
    public final void \u300700(final AnnotatedOutput annotatedOutput) {
        this.\u3007O8o08O().\u3007o00\u3007\u3007Oo().OOO\u3007O0(annotatedOutput, this);
    }
    
    @Override
    protected final String \u30070\u3007O0088o(final boolean b) {
        return this.\u3007O8o08O().\u3007o00\u3007\u3007Oo().\u3007O8o08O(this, b);
    }
    
    @Override
    public final int \u3007o00\u3007\u3007Oo() {
        return this.\u3007O8o08O().\u3007o00\u3007\u3007Oo().O8();
    }
    
    @Override
    public final DalvInsn \u3007oo\u3007(final int n) {
        return this.o\u3007O8\u3007\u3007o(this.Oooo8o0\u3007().\u300700(n));
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.android.dx.dex.code;

import com.android.dx.ssa.RegisterMapper;
import com.android.dx.rop.code.RegisterSpec;
import com.android.dx.rop.code.RegisterSpecList;
import com.android.dx.rop.code.SourcePosition;
import com.android.dx.rop.code.RegisterSpecSet;

public final class LocalSnapshot extends ZeroSizeInsn
{
    private final RegisterSpecSet Oo08;
    
    public LocalSnapshot(final SourcePosition sourcePosition, final RegisterSpecSet oo08) {
        super(sourcePosition);
        if (oo08 != null) {
            this.Oo08 = oo08;
            return;
        }
        throw new NullPointerException("locals == null");
    }
    
    public RegisterSpecSet O\u30078O8\u3007008() {
        return this.Oo08;
    }
    
    @Override
    public DalvInsn o\u3007O8\u3007\u3007o(final RegisterSpecList list) {
        return new LocalSnapshot(this.OO0o\u3007\u3007(), this.Oo08);
    }
    
    @Override
    protected String \u3007080() {
        return this.Oo08.toString();
    }
    
    @Override
    protected String \u30070\u3007O0088o(final boolean b) {
        final int size = this.Oo08.size();
        final int oo0o\u3007\u3007\u3007\u30070 = this.Oo08.OO0o\u3007\u3007\u3007\u30070();
        final StringBuilder sb = new StringBuilder(size * 40 + 100);
        sb.append("local-snapshot");
        for (int i = 0; i < oo0o\u3007\u3007\u3007\u30070; ++i) {
            final RegisterSpec \u300780\u3007808\u3007O = this.Oo08.\u300780\u3007808\u3007O(i);
            if (\u300780\u3007808\u3007O != null) {
                sb.append("\n  ");
                sb.append(LocalStart.O8ooOoo\u3007(\u300780\u3007808\u3007O));
            }
        }
        return sb.toString();
    }
    
    @Override
    public DalvInsn \u3007O888o0o(final RegisterMapper registerMapper) {
        return new LocalSnapshot(this.OO0o\u3007\u3007(), registerMapper.O8(this.Oo08));
    }
    
    @Override
    public DalvInsn \u3007oo\u3007(final int n) {
        return new LocalSnapshot(this.OO0o\u3007\u3007(), this.Oo08.Oooo8o0\u3007(n));
    }
}

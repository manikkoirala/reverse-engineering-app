// 
// Decompiled by Procyon v0.6.0
// 

package com.android.dx;

import java.util.Iterator;
import com.android.dx.util.IntList;
import com.android.dx.rop.code.InsnList;
import com.android.dx.rop.code.BasicBlock;
import java.util.Collections;
import java.util.ArrayList;
import com.android.dx.rop.code.Insn;
import java.util.List;

public final class Label
{
    List<Label> O8;
    Label Oo08;
    Label o\u30070;
    final List<Insn> \u3007080;
    Code \u3007o00\u3007\u3007Oo;
    boolean \u3007o\u3007;
    int \u3007\u3007888;
    
    public Label() {
        this.\u3007080 = new ArrayList<Insn>();
        this.\u3007o\u3007 = false;
        this.O8 = Collections.emptyList();
        this.\u3007\u3007888 = -1;
    }
    
    void \u3007080() {
        for (int i = 0; i < this.O8.size(); ++i) {
            while (this.O8.get(i).\u3007o00\u3007\u3007Oo()) {
                final List<Label> o8 = this.O8;
                o8.set(i, o8.get(i).Oo08);
            }
        }
        while (true) {
            final Label oo08 = this.Oo08;
            if (oo08 == null || !oo08.\u3007o00\u3007\u3007Oo()) {
                break;
            }
            this.Oo08 = this.Oo08.Oo08;
        }
        while (true) {
            final Label o\u30070 = this.o\u30070;
            if (o\u30070 == null || !o\u30070.\u3007o00\u3007\u3007Oo()) {
                break;
            }
            this.o\u30070 = this.o\u30070.Oo08;
        }
    }
    
    boolean \u3007o00\u3007\u3007Oo() {
        return this.\u3007080.isEmpty();
    }
    
    BasicBlock \u3007o\u3007() {
        final InsnList list = new InsnList(this.\u3007080.size());
        for (int i = 0; i < this.\u3007080.size(); ++i) {
            list.\u3007\u30078O0\u30078(i, this.\u3007080.get(i));
        }
        list.Oo08();
        final IntList list2 = new IntList();
        final Iterator<Label> iterator = this.O8.iterator();
        while (iterator.hasNext()) {
            list2.oO80(iterator.next().\u3007\u3007888);
        }
        final Label oo08 = this.Oo08;
        int \u3007\u3007888;
        if (oo08 != null) {
            \u3007\u3007888 = oo08.\u3007\u3007888;
            list2.oO80(\u3007\u3007888);
        }
        else {
            \u3007\u3007888 = -1;
        }
        final Label o\u30070 = this.o\u30070;
        if (o\u30070 != null) {
            list2.oO80(o\u30070.\u3007\u3007888);
        }
        list2.Oo08();
        return new BasicBlock(this.\u3007\u3007888, list, list2, \u3007\u3007888);
    }
}

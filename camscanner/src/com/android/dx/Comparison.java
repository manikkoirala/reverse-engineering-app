// 
// Decompiled by Procyon v0.6.0
// 

package com.android.dx;

import com.android.dx.rop.code.Rops;
import com.android.dx.rop.code.Rop;
import com.android.dx.rop.type.TypeList;

public enum Comparison
{
    private static final Comparison[] $VALUES;
    
    EQ {
        @Override
        Rop \u3007080(final TypeList list) {
            return Rops.\u3007\u3007888(list);
        }
    }, 
    GE {
        @Override
        Rop \u3007080(final TypeList list) {
            return Rops.oO80(list);
        }
    }, 
    GT {
        @Override
        Rop \u3007080(final TypeList list) {
            return Rops.\u300780\u3007808\u3007O(list);
        }
    }, 
    LE {
        @Override
        Rop \u3007080(final TypeList list) {
            return Rops.OO0o\u3007\u3007\u3007\u30070(list);
        }
    }, 
    LT {
        @Override
        Rop \u3007080(final TypeList list) {
            return Rops.\u30078o8o\u3007(list);
        }
    }, 
    NE {
        @Override
        Rop \u3007080(final TypeList list) {
            return Rops.\u3007O8o08O(list);
        }
    };
    
    abstract Rop \u3007080(final TypeList p0);
}

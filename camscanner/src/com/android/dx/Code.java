// 
// Decompiled by Procyon v0.6.0
// 

package com.android.dx;

import com.android.dx.rop.cst.CstInteger;
import com.android.dx.rop.code.BasicBlockList;
import com.android.dx.rop.code.RegisterSpec;
import com.android.dx.rop.code.ThrowingInsn;
import java.util.Iterator;
import com.android.dx.rop.code.PlainCstInsn;
import java.util.Collections;
import java.util.Collection;
import com.android.dx.rop.cst.Constant;
import com.android.dx.rop.type.TypeList;
import com.android.dx.rop.code.ThrowingCstInsn;
import com.android.dx.rop.type.Type;
import com.android.dx.rop.code.Rop;
import com.android.dx.rop.code.Insn;
import com.android.dx.rop.code.PlainInsn;
import com.android.dx.rop.type.TypeBearer;
import com.android.dx.rop.code.Rops;
import com.android.dx.rop.code.RegisterSpecList;
import java.util.ArrayList;
import com.android.dx.rop.type.StdTypeList;
import com.android.dx.rop.code.SourcePosition;
import java.util.List;

public final class Code
{
    private boolean O8;
    private final List<Label> OO0o\u3007\u3007\u3007\u30070;
    private final Local<?> Oo08;
    private SourcePosition oO80;
    private final List<Local<?>> o\u30070;
    private final MethodId<?, ?> \u3007080;
    private final List<TypeId<?>> \u300780\u3007808\u3007O;
    private StdTypeList \u30078o8o\u3007;
    private final List<Label> \u3007o00\u3007\u3007Oo;
    private Label \u3007o\u3007;
    private final List<Local<?>> \u3007\u3007888;
    
    Code(final DexMaker.MethodDeclaration methodDeclaration) {
        this.\u3007o00\u3007\u3007Oo = new ArrayList<Label>();
        final ArrayList o\u30070 = new ArrayList();
        this.o\u30070 = o\u30070;
        this.\u3007\u3007888 = new ArrayList<Local<?>>();
        this.oO80 = SourcePosition.O8;
        this.\u300780\u3007808\u3007O = new ArrayList<TypeId<?>>();
        this.OO0o\u3007\u3007\u3007\u30070 = new ArrayList<Label>();
        this.\u30078o8o\u3007 = StdTypeList.OO;
        final MethodId<?, ?> \u3007080 = methodDeclaration.\u3007080;
        this.\u3007080 = \u3007080;
        if (methodDeclaration.\u3007o\u3007()) {
            this.Oo08 = null;
        }
        else {
            o\u30070.add(this.Oo08 = Local.\u3007080(this, \u3007080.\u3007080));
        }
        final TypeId<?>[] \u300781 = \u3007080.O8.\u3007080;
        for (int length = \u300781.length, i = 0; i < length; ++i) {
            this.o\u30070.add(Local.\u3007080(this, \u300781[i]));
        }
        this.\u3007o\u3007(this.\u3007o\u3007 = new Label());
        this.\u3007o\u3007.\u3007o\u3007 = true;
    }
    
    private void O8\u3007o(final Label o\u30070, final List<Label> o8) {
        final Label label = new Label();
        this.\u3007o\u3007(label);
        final Label \u3007o\u3007 = this.\u3007o\u3007;
        \u3007o\u3007.Oo08 = label;
        \u3007o\u3007.o\u30070 = o\u30070;
        \u3007o\u3007.O8 = o8;
        this.\u3007o\u3007 = label;
        label.\u3007o\u3007 = true;
    }
    
    private static RegisterSpecList OO0o\u3007\u3007\u3007\u30070(final Local<?> local, final Local<?>[] array) {
        final int n = 0;
        int n2;
        if (local != null) {
            n2 = 1;
        }
        else {
            n2 = 0;
        }
        final RegisterSpecList list = new RegisterSpecList(array.length + n2);
        int i = n;
        if (local != null) {
            list.\u3007O888o0o(0, local.O8());
            i = n;
        }
        while (i < array.length) {
            list.\u3007O888o0o(i + n2, array[i].O8());
            ++i;
        }
        return list;
    }
    
    private <T> Local<T> oO80(final Local<?> local, final TypeId<T> obj) {
        if (local.\u3007o00\u3007\u3007Oo.equals(obj)) {
            return (Local<T>)local;
        }
        final StringBuilder sb = new StringBuilder();
        sb.append("requested ");
        sb.append(obj);
        sb.append(" but was ");
        sb.append(local.\u3007o00\u3007\u3007Oo);
        throw new IllegalArgumentException(sb.toString());
    }
    
    private void \u300700(final Local<?> local, final boolean b) {
        Rop rop;
        if (b) {
            rop = Rops.OoO8(local.\u3007o00\u3007\u3007Oo.\u3007o00\u3007\u3007Oo);
        }
        else {
            rop = Rops.\u30070\u3007O0088o(local.\u3007o00\u3007\u3007Oo.\u3007o00\u3007\u3007Oo);
        }
        this.\u3007080(new PlainInsn(rop, this.oO80, local.O8(), RegisterSpecList.OO));
    }
    
    private void \u3007080(final Insn insn) {
        this.\u3007o00\u3007\u3007Oo(insn, null);
    }
    
    private Rop \u30078o8o\u3007(final Type type, final Type type2) {
        if (type.\u3007o00\u3007\u3007Oo() == 6) {
            final int \u3007o00\u3007\u3007Oo = type2.\u3007o00\u3007\u3007Oo();
            if (\u3007o00\u3007\u3007Oo == 2) {
                return Rops.OoO\u3007;
            }
            if (\u3007o00\u3007\u3007Oo == 3) {
                return Rops.\u3007o8OO0;
            }
            if (\u3007o00\u3007\u3007Oo == 8) {
                return Rops.\u3007\u30070o8O\u3007\u3007;
            }
        }
        return Rops.O8(type2, type);
    }
    
    private <D, R> void \u3007O\u3007(final Rop rop, final MethodId<D, R> methodId, final Local<? super R> local, final Local<? extends D> local2, final Local<?>... array) {
        this.\u3007080(new ThrowingCstInsn(rop, this.oO80, OO0o\u3007\u3007\u3007\u30070(local2, array), this.\u30078o8o\u3007, methodId.o\u30070));
        if (local != null) {
            this.\u300700(local, false);
        }
    }
    
    private void \u3007o00\u3007\u3007Oo(final Insn insn, final Label label) {
        final Label \u3007o\u3007 = this.\u3007o\u3007;
        if (\u3007o\u3007 == null || !\u3007o\u3007.\u3007o\u3007) {
            throw new IllegalStateException("no current label");
        }
        \u3007o\u3007.\u3007080.add(insn);
        final int \u3007o00\u3007\u3007Oo = insn.\u3007\u3007888().\u3007o00\u3007\u3007Oo();
        if (\u3007o00\u3007\u3007Oo != 1) {
            if (\u3007o00\u3007\u3007Oo != 2) {
                if (\u3007o00\u3007\u3007Oo != 3) {
                    if (\u3007o00\u3007\u3007Oo != 4) {
                        if (\u3007o00\u3007\u3007Oo != 6) {
                            throw new IllegalArgumentException();
                        }
                        if (label != null) {
                            final StringBuilder sb = new StringBuilder();
                            sb.append("unexpected branch: ");
                            sb.append(label);
                            throw new IllegalArgumentException(sb.toString());
                        }
                        this.O8\u3007o(null, new ArrayList<Label>(this.OO0o\u3007\u3007\u3007\u30070));
                    }
                    else {
                        if (label == null) {
                            throw new IllegalArgumentException("branch == null");
                        }
                        this.O8\u3007o(label, Collections.emptyList());
                    }
                }
                else {
                    if (label == null) {
                        throw new IllegalArgumentException("branch == null");
                    }
                    this.\u3007o\u3007.Oo08 = label;
                    this.\u3007o\u3007 = null;
                }
            }
            else {
                if (label != null) {
                    final StringBuilder sb2 = new StringBuilder();
                    sb2.append("unexpected branch: ");
                    sb2.append(label);
                    throw new IllegalArgumentException(sb2.toString());
                }
                this.\u3007o\u3007 = null;
            }
            return;
        }
        if (label == null) {
            return;
        }
        final StringBuilder sb3 = new StringBuilder();
        sb3.append("unexpected branch: ");
        sb3.append(label);
        throw new IllegalArgumentException(sb3.toString());
    }
    
    private void \u3007oo\u3007(final Local local, final Object o) {
        Rop rop;
        if (o == null) {
            rop = Rops.\u3007\u30078O0\u30078;
        }
        else {
            rop = Rops.\u3007o\u3007(local.\u3007o00\u3007\u3007Oo.\u3007o00\u3007\u3007Oo);
        }
        if (rop.\u3007o00\u3007\u3007Oo() == 1) {
            this.\u3007080(new PlainCstInsn(rop, this.oO80, local.O8(), RegisterSpecList.OO, Constants.\u3007080(o)));
        }
        else {
            this.\u3007080(new ThrowingCstInsn(rop, this.oO80, RegisterSpecList.OO, this.\u30078o8o\u3007, Constants.\u3007080(o)));
            this.\u300700(local, true);
        }
    }
    
    private void \u3007o\u3007(final Label label) {
        final Code \u3007o00\u3007\u3007Oo = label.\u3007o00\u3007\u3007Oo;
        if (\u3007o00\u3007\u3007Oo == this) {
            return;
        }
        if (\u3007o00\u3007\u3007Oo == null) {
            label.\u3007o00\u3007\u3007Oo = this;
            this.\u3007o00\u3007\u3007Oo.add(label);
            return;
        }
        throw new IllegalArgumentException("Cannot adopt label; it belongs to another Code");
    }
    
    private void \u3007\u3007888() {
        final Iterator<Label> iterator = this.\u3007o00\u3007\u3007Oo.iterator();
        int \u3007\u3007888 = 0;
        while (iterator.hasNext()) {
            final Label label = iterator.next();
            if (label.\u3007o00\u3007\u3007Oo()) {
                iterator.remove();
            }
            else {
                label.\u3007080();
                label.\u3007\u3007888 = \u3007\u3007888;
                ++\u3007\u3007888;
            }
        }
    }
    
    public void O8(final Local<?> local, final Local<?> local2, final Local<Integer> local3) {
        this.\u3007080(new ThrowingInsn(Rops.\u3007080(local.\u3007o00\u3007\u3007Oo.\u3007o00\u3007\u3007Oo), this.oO80, RegisterSpecList.OoO8(local2.O8(), local3.O8()), this.\u30078o8o\u3007));
        this.\u300700(local, true);
    }
    
    public <T> void O8ooOoo\u3007(final Local<T> local, final MethodId<T, Void> methodId, final Local<?>... array) {
        if (local != null) {
            this.\u3007080(new ThrowingCstInsn(Rops.O88o\u3007, this.oO80, RegisterSpecList.OO, this.\u30078o8o\u3007, methodId.\u3007080.\u3007o\u3007));
            this.\u300700(local, true);
            this.\u3007O00((MethodId<Object, Object>)methodId, null, local, array);
            return;
        }
        throw new IllegalArgumentException();
    }
    
    public <T> Local<T> OO0o\u3007\u3007(final TypeId<T> typeId) {
        final Local<?> oo08 = this.Oo08;
        if (oo08 != null) {
            return this.oO80(oo08, typeId);
        }
        throw new IllegalStateException("static methods cannot access 'this'");
    }
    
    public void OOO\u3007O0() {
        if (this.\u3007080.\u3007o00\u3007\u3007Oo.equals(TypeId.\u3007O8o08O)) {
            this.\u3007080(new PlainInsn(Rops.O\u30078oOo8O, this.oO80, null, RegisterSpecList.OO));
            return;
        }
        final StringBuilder sb = new StringBuilder();
        sb.append("declared ");
        sb.append(this.\u3007080.\u3007o00\u3007\u3007Oo);
        sb.append(" but returned void");
        throw new IllegalArgumentException(sb.toString());
    }
    
    public void Oo08(final Local<?> local, final Local<Integer> local2, final Local<?> local3) {
        this.\u3007080(new ThrowingInsn(Rops.\u3007o00\u3007\u3007Oo(local3.\u3007o00\u3007\u3007Oo.\u3007o00\u3007\u3007Oo), this.oO80, RegisterSpecList.o800o8O(local3.O8(), local.O8(), local2.O8()), this.\u30078o8o\u3007));
    }
    
    public <D, R> void OoO8(final MethodId<D, R> methodId, final Local<? super R> local, final Local<? extends D> local2, final Local<?>... array) {
        this.\u3007O\u3007(Rops.\u3007O\u3007(methodId.O8(true)), methodId, local, local2, array);
    }
    
    public <D, V> void Oooo8o0\u3007(final FieldId<D, ? extends V> fieldId, final Local<V> local, final Local<D> local2) {
        this.\u3007080(new ThrowingCstInsn(Rops.Oo08(local.\u3007o00\u3007\u3007Oo.\u3007o00\u3007\u3007Oo), this.oO80, RegisterSpecList.\u30070\u3007O0088o(local2.O8()), this.\u30078o8o\u3007, fieldId.Oo08));
        this.\u300700(local, true);
    }
    
    public <T> void O\u30078O8\u3007008(final Local<T> local, final Local<Integer> local2) {
        this.\u3007080(new ThrowingCstInsn(Rops.o800o8O(local.\u3007o00\u3007\u3007Oo.\u3007o00\u3007\u3007Oo), this.oO80, RegisterSpecList.\u30070\u3007O0088o(local2.O8()), this.\u30078o8o\u3007, local.\u3007o00\u3007\u3007Oo.\u3007o\u3007));
        this.\u300700(local, true);
    }
    
    public <D, R> void o800o8O(final MethodId<D, R> methodId, final Local<? super R> local, final Local<? extends D> local2, final Local<?>... array) {
        this.\u3007O\u3007(Rops.\u3007O00(methodId.O8(true)), methodId, local, local2, array);
    }
    
    public <T> void oo88o8O(final Local<T> local, final T t) {
        this.\u3007oo\u3007(local, t);
    }
    
    public <V> void oo\u3007(final FieldId<?, ? extends V> fieldId, final Local<V> local) {
        this.\u3007080(new ThrowingCstInsn(Rops.o\u30070(local.\u3007o00\u3007\u3007Oo.\u3007o00\u3007\u3007Oo), this.oO80, RegisterSpecList.OO, this.\u30078o8o\u3007, fieldId.Oo08));
        this.\u300700(local, true);
    }
    
    public void o\u30070(final Local<?> local, final Local<?> local2) {
        if (local2.getType().\u3007o00\u3007\u3007Oo.\u3007\u3007808\u3007()) {
            this.\u3007080(new ThrowingCstInsn(Rops.o\u30070o\u3007\u3007, this.oO80, RegisterSpecList.\u30070\u3007O0088o(local2.O8()), this.\u30078o8o\u3007, local.\u3007o00\u3007\u3007Oo.\u3007o\u3007));
            this.\u300700(local, true);
        }
        else {
            this.\u3007080(new PlainInsn(this.\u30078o8o\u3007(local2.\u3007o00\u3007\u3007Oo.\u3007o00\u3007\u3007Oo, local.\u3007o00\u3007\u3007Oo.\u3007o00\u3007\u3007Oo), this.oO80, local.O8(), local2.O8()));
        }
    }
    
    public void o\u3007O8\u3007\u3007o(final Label \u3007o\u3007) {
        this.\u3007o\u3007(\u3007o\u3007);
        if (!\u3007o\u3007.\u3007o\u3007) {
            \u3007o\u3007.\u3007o\u3007 = true;
            if (this.\u3007o\u3007 != null) {
                this.\u3007O888o0o(\u3007o\u3007);
            }
            this.\u3007o\u3007 = \u3007o\u3007;
            return;
        }
        throw new IllegalStateException("already marked");
    }
    
    public void o\u3007\u30070\u3007(final Local<?> local) {
        if (local.\u3007o00\u3007\u3007Oo.equals(this.\u3007080.\u3007o00\u3007\u3007Oo)) {
            this.\u3007080(new PlainInsn(Rops.\u3007O888o0o(local.\u3007o00\u3007\u3007Oo.\u3007o00\u3007\u3007Oo), this.oO80, null, RegisterSpecList.\u30070\u3007O0088o(local.O8())));
            return;
        }
        final StringBuilder sb = new StringBuilder();
        sb.append("declared ");
        sb.append(this.\u3007080.\u3007o00\u3007\u3007Oo);
        sb.append(" but returned ");
        sb.append(local.\u3007o00\u3007\u3007Oo);
        throw new IllegalArgumentException(sb.toString());
    }
    
    int \u30070000OOO() {
        final Iterator<Local<?>> iterator = this.o\u30070.iterator();
        int n = 0;
        while (iterator.hasNext()) {
            n += iterator.next().\u3007o\u3007();
        }
        return n;
    }
    
    public void \u300700\u30078(final Local<? extends Throwable> local) {
        this.\u3007080(new ThrowingInsn(Rops.o\u3007\u30070\u300788, this.oO80, RegisterSpecList.\u30070\u3007O0088o(local.O8()), this.\u30078o8o\u3007));
    }
    
    public <R> void \u30070\u3007O0088o(final MethodId<?, R> methodId, final Local<? super R> local, final Local<?>... array) {
        this.\u3007O\u3007(Rops.\u3007\u3007808\u3007(methodId.O8(true)), (MethodId<Object, Object>)methodId, (Local<? super Object>)local, null, array);
    }
    
    public <T> void \u300780\u3007808\u3007O(final Comparison comparison, final Label label, final Local<T> local, final Local<T> local2) {
        this.\u3007o\u3007(label);
        this.\u3007o00\u3007\u3007Oo(new PlainInsn(comparison.\u3007080(StdTypeList.OoO8(local.\u3007o00\u3007\u3007Oo.\u3007o00\u3007\u3007Oo, local2.\u3007o00\u3007\u3007Oo.\u3007o00\u3007\u3007Oo)), this.oO80, null, RegisterSpecList.OoO8(local.O8(), local2.O8())), label);
    }
    
    public <D, R> void \u3007O00(final MethodId<D, R> methodId, final Local<? super R> local, final Local<? extends D> local2, final Local<?>... array) {
        this.\u3007O\u3007(Rops.OO0o\u3007\u3007(methodId.O8(true)), methodId, local, local2, array);
    }
    
    public void \u3007O888o0o(final Label label) {
        this.\u3007o\u3007(label);
        this.\u3007o00\u3007\u3007Oo(new PlainInsn(Rops.\u30070\u3007O0088o, this.oO80, null, RegisterSpecList.OO), label);
    }
    
    public <T> Local<T> \u3007O8o08O(final int n, final TypeId<T> typeId) {
        int n2 = n;
        if (this.Oo08 != null) {
            n2 = n + 1;
        }
        return this.oO80(this.o\u30070.get(n2), typeId);
    }
    
    BasicBlockList \u3007o() {
        if (!this.O8) {
            this.\u3007\u3007808\u3007();
        }
        this.\u3007\u3007888();
        final BasicBlockList list = new BasicBlockList(this.\u3007o00\u3007\u3007Oo.size());
        for (int i = 0; i < this.\u3007o00\u3007\u3007Oo.size(); ++i) {
            list.\u300700(i, this.\u3007o00\u3007\u3007Oo.get(i).\u3007o\u3007());
        }
        return list;
    }
    
    public <T> Local<T> \u3007oOO8O8(final TypeId<T> typeId) {
        if (!this.O8) {
            final Local<T> \u3007080 = Local.\u3007080(this, typeId);
            this.\u3007\u3007888.add(\u3007080);
            return \u3007080;
        }
        throw new IllegalStateException("Cannot allocate locals after adding instructions");
    }
    
    void \u3007\u3007808\u3007() {
        if (!this.O8) {
            this.O8 = true;
            final Iterator<Local<?>> iterator = this.\u3007\u3007888.iterator();
            int n = 0;
            while (iterator.hasNext()) {
                n += iterator.next().\u3007o00\u3007\u3007Oo(n);
            }
            final ArrayList list = new ArrayList();
            final Iterator<Local<?>> iterator2 = this.o\u30070.iterator();
            int n2 = n;
            while (iterator2.hasNext()) {
                final Local local = iterator2.next();
                final CstInteger oo0o\u3007\u3007\u3007\u30070 = CstInteger.OO0o\u3007\u3007\u3007\u30070(n2 - n);
                n2 += local.\u3007o00\u3007\u3007Oo(n2);
                list.add(new PlainCstInsn(Rops.\u3007\u30078O0\u30078(local.\u3007o00\u3007\u3007Oo.\u3007o00\u3007\u3007Oo), this.oO80, local.O8(), RegisterSpecList.OO, oo0o\u3007\u3007\u3007\u30070));
            }
            this.\u3007o00\u3007\u3007Oo.get(0).\u3007080.addAll(0, list);
            return;
        }
        throw new AssertionError();
    }
    
    public <D, R> void \u3007\u30078O0\u30078(final MethodId<D, R> methodId, final Local<? super R> local, final Local<? extends D> local2, final Local<?>... array) {
        this.\u3007O\u3007(Rops.Oooo8o0\u3007(methodId.O8(true)), methodId, local, local2, array);
    }
}

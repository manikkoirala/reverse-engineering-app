// 
// Decompiled by Procyon v0.6.0
// 

package com.android.dx.util;

public class MutabilityControl
{
    private boolean o0;
    
    public MutabilityControl(final boolean o0) {
        this.o0 = o0;
    }
    
    public final boolean O8() {
        return this.o0;
    }
    
    public void Oo08() {
        this.o0 = false;
    }
    
    public final void o\u30070() {
        if (this.o0) {
            return;
        }
        throw new MutabilityException("immutable instance");
    }
    
    public final boolean \u3007o\u3007() {
        return this.o0 ^ true;
    }
    
    public final void \u3007\u3007888() {
        if (!this.o0) {
            return;
        }
        throw new MutabilityException("mutable instance");
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.android.dx.util;

public final class Bits
{
    public static boolean O8(final int[] array, final int n) {
        boolean b = true;
        if ((array[n >> 5] & 1 << (n & 0x1F)) == 0x0) {
            b = false;
        }
        return b;
    }
    
    public static int[] Oo08(final int n) {
        return new int[n + 31 >> 5];
    }
    
    public static void o\u30070(final int[] array, final int n) {
        final int n2 = n >> 5;
        array[n2] |= 1 << (n & 0x1F);
    }
    
    public static void \u3007080(final int[] array, final int n) {
        final int n2 = n >> 5;
        array[n2] &= ~(1 << (n & 0x1F));
    }
    
    public static int \u3007o00\u3007\u3007Oo(int numberOfTrailingZeros, int n) {
        n = (numberOfTrailingZeros = Integer.numberOfTrailingZeros(numberOfTrailingZeros & ~((1 << n) - 1)));
        if (n == 32) {
            numberOfTrailingZeros = -1;
        }
        return numberOfTrailingZeros;
    }
    
    public static int \u3007o\u3007(final int[] array, int i) {
        final int length = array.length;
        int n = i & 0x1F;
        int n2;
        int \u3007o00\u3007\u3007Oo;
        for (i >>= 5; i < length; ++i, n = 0) {
            n2 = array[i];
            if (n2 != 0) {
                \u3007o00\u3007\u3007Oo = \u3007o00\u3007\u3007Oo(n2, n);
                if (\u3007o00\u3007\u3007Oo >= 0) {
                    return (i << 5) + \u3007o00\u3007\u3007Oo;
                }
            }
        }
        return -1;
    }
}

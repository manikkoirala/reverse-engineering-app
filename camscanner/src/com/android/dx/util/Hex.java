// 
// Decompiled by Procyon v0.6.0
// 

package com.android.dx.util;

public final class Hex
{
    public static String O8(int n) {
        final char[] value = new char[2];
        for (int i = 0; i < 2; ++i) {
            value[1 - i] = Character.forDigit(n & 0xF, 16);
            n >>= 4;
        }
        return new String(value);
    }
    
    public static String OO0o\u3007\u3007\u3007\u30070(final int n) {
        return new String(new char[] { Character.forDigit(n & 0xF, 16) });
    }
    
    public static String Oo08(int i) {
        final char[] value = new char[4];
        final int n = 0;
        int n2 = i;
        for (i = n; i < 4; ++i) {
            value[3 - i] = Character.forDigit(n2 & 0xF, 16);
            n2 >>= 4;
        }
        return new String(value);
    }
    
    public static String oO80(int n) {
        final char[] value = new char[8];
        for (int i = 0; i < 8; ++i) {
            value[7 - i] = Character.forDigit(n & 0xF, 16);
            n >>= 4;
        }
        return new String(value);
    }
    
    public static String o\u30070(final int n) {
        if (n == (char)n) {
            return Oo08(n);
        }
        return oO80(n);
    }
    
    public static String \u3007080(final byte[] array, int i, int j, int n, final int n2, final int n3) {
        final int k = i + j;
        if ((i | j | k) < 0 || k > array.length) {
            final StringBuilder sb = new StringBuilder();
            sb.append("arr.length ");
            sb.append(array.length);
            sb.append("; ");
            sb.append(i);
            sb.append("..!");
            sb.append(k);
            throw new IndexOutOfBoundsException(sb.toString());
        }
        if (n < 0) {
            throw new IllegalArgumentException("outOffset < 0");
        }
        if (j == 0) {
            return "";
        }
        final StringBuilder sb2 = new StringBuilder(j * 4 + 6);
        int n4 = 0;
        while (j > 0) {
            if (n4 == 0) {
                String str;
                if (n3 != 2) {
                    if (n3 != 4) {
                        if (n3 != 6) {
                            str = oO80(n);
                        }
                        else {
                            str = \u3007\u3007888(n);
                        }
                    }
                    else {
                        str = Oo08(n);
                    }
                }
                else {
                    str = O8(n);
                }
                sb2.append(str);
                sb2.append(": ");
            }
            else if ((n4 & 0x1) == 0x0) {
                sb2.append(' ');
            }
            sb2.append(O8(array[i]));
            ++n;
            final int n5 = i + 1;
            i = ++n4;
            if (n4 == n2) {
                sb2.append('\n');
                i = 0;
            }
            --j;
            n4 = i;
            i = n5;
        }
        if (n4 != 0) {
            sb2.append('\n');
        }
        return sb2.toString();
    }
    
    public static String \u300780\u3007808\u3007O(long n) {
        final char[] value = new char[16];
        for (int i = 0; i < 16; ++i) {
            value[15 - i] = Character.forDigit((int)n & 0xF, 16);
            n >>= 4;
        }
        return new String(value);
    }
    
    public static String \u3007o00\u3007\u3007Oo(int n) {
        final char[] value = new char[5];
        int i = 0;
        if (n < 0) {
            value[0] = '-';
            n = -n;
        }
        else {
            value[0] = '+';
        }
        while (i < 4) {
            value[4 - i] = Character.forDigit(n & 0xF, 16);
            n >>= 4;
            ++i;
        }
        return new String(value);
    }
    
    public static String \u3007o\u3007(int n) {
        final char[] value = new char[9];
        int i = 0;
        if (n < 0) {
            value[0] = '-';
            n = -n;
        }
        else {
            value[0] = '+';
        }
        while (i < 8) {
            value[8 - i] = Character.forDigit(n & 0xF, 16);
            n >>= 4;
            ++i;
        }
        return new String(value);
    }
    
    public static String \u3007\u3007888(int i) {
        final char[] value = new char[6];
        final int n = 0;
        int n2 = i;
        for (i = n; i < 6; ++i) {
            value[5 - i] = Character.forDigit(n2 & 0xF, 16);
            n2 >>= 4;
        }
        return new String(value);
    }
}

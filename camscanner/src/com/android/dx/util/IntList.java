// 
// Decompiled by Procyon v0.6.0
// 

package com.android.dx.util;

import java.util.Arrays;

public final class IntList extends MutabilityControl
{
    public static final IntList o\u300700O;
    private int OO;
    private boolean \u300708O\u300700\u3007o;
    private int[] \u3007OOo8\u30070;
    
    static {
        (o\u300700O = new IntList(0)).Oo08();
    }
    
    public IntList() {
        this(4);
    }
    
    public IntList(final int n) {
        super(true);
        try {
            this.\u3007OOo8\u30070 = new int[n];
            this.OO = 0;
            this.\u300708O\u300700\u3007o = true;
        }
        catch (final NegativeArraySizeException ex) {
            throw new IllegalArgumentException("size < 0");
        }
    }
    
    private void \u3007O8o08O() {
        final int oo = this.OO;
        final int[] \u3007oOo8\u30070 = this.\u3007OOo8\u30070;
        if (oo == \u3007oOo8\u30070.length) {
            final int[] \u3007oOo8\u30072 = new int[oo * 3 / 2 + 10];
            System.arraycopy(\u3007oOo8\u30070, 0, \u3007oOo8\u30072, 0, oo);
            this.\u3007OOo8\u30070 = \u3007oOo8\u30072;
        }
    }
    
    public int OO0o\u3007\u3007(int \u300780\u3007808\u3007O) {
        \u300780\u3007808\u3007O = this.\u300780\u3007808\u3007O(\u300780\u3007808\u3007O);
        if (\u300780\u3007808\u3007O < 0) {
            \u300780\u3007808\u3007O = -1;
        }
        return \u300780\u3007808\u3007O;
    }
    
    public boolean OO0o\u3007\u3007\u3007\u30070(final int n) {
        return this.OO0o\u3007\u3007(n) >= 0;
    }
    
    public void Oooo8o0\u3007(final int n, final int n2) {
        this.o\u30070();
        if (n < this.OO) {
            try {
                this.\u3007OOo8\u30070[n] = n2;
                this.\u300708O\u300700\u3007o = false;
            }
            catch (final ArrayIndexOutOfBoundsException ex) {
                if (n < 0) {
                    throw new IllegalArgumentException("n < 0");
                }
            }
            return;
        }
        throw new IndexOutOfBoundsException("n >= size()");
    }
    
    @Override
    public boolean equals(final Object o) {
        if (o == this) {
            return true;
        }
        if (!(o instanceof IntList)) {
            return false;
        }
        final IntList list = (IntList)o;
        if (this.\u300708O\u300700\u3007o != list.\u300708O\u300700\u3007o) {
            return false;
        }
        if (this.OO != list.OO) {
            return false;
        }
        for (int i = 0; i < this.OO; ++i) {
            if (this.\u3007OOo8\u30070[i] != list.\u3007OOo8\u30070[i]) {
                return false;
            }
        }
        return true;
    }
    
    @Override
    public int hashCode() {
        int i = 0;
        int n = 0;
        while (i < this.OO) {
            n = n * 31 + this.\u3007OOo8\u30070[i];
            ++i;
        }
        return n;
    }
    
    public void insert(final int n, final int n2) {
        if (n <= this.OO) {
            this.\u3007O8o08O();
            final int[] \u3007oOo8\u30070 = this.\u3007OOo8\u30070;
            final int n3 = n + 1;
            System.arraycopy(\u3007oOo8\u30070, n, \u3007oOo8\u30070, n3, this.OO - n);
            final int[] \u3007oOo8\u30072 = this.\u3007OOo8\u30070;
            \u3007oOo8\u30072[n] = n2;
            int oo = this.OO;
            final boolean b = true;
            ++oo;
            this.OO = oo;
            boolean \u300708O\u300700\u3007o = false;
            Label_0117: {
                if (this.\u300708O\u300700\u3007o && (n == 0 || n2 > \u3007oOo8\u30072[n - 1])) {
                    \u300708O\u300700\u3007o = b;
                    if (n == oo - 1) {
                        break Label_0117;
                    }
                    if (n2 < \u3007oOo8\u30072[n3]) {
                        \u300708O\u300700\u3007o = b;
                        break Label_0117;
                    }
                }
                \u300708O\u300700\u3007o = false;
            }
            this.\u300708O\u300700\u3007o = \u300708O\u300700\u3007o;
            return;
        }
        throw new IndexOutOfBoundsException("n > size()");
    }
    
    public void oO80(final int n) {
        this.o\u30070();
        this.\u3007O8o08O();
        final int[] \u3007oOo8\u30070 = this.\u3007OOo8\u30070;
        final int oo = this.OO;
        final int oo2 = oo + 1;
        this.OO = oo2;
        \u3007oOo8\u30070[oo] = n;
        if (this.\u300708O\u300700\u3007o) {
            boolean \u300708O\u300700\u3007o = true;
            if (oo2 > 1) {
                if (n < \u3007oOo8\u30070[oo2 - 2]) {
                    \u300708O\u300700\u3007o = false;
                }
                this.\u300708O\u300700\u3007o = \u300708O\u300700\u3007o;
            }
        }
    }
    
    public int size() {
        return this.OO;
    }
    
    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder(this.OO * 5 + 10);
        sb.append('{');
        for (int i = 0; i < this.OO; ++i) {
            if (i != 0) {
                sb.append(", ");
            }
            sb.append(this.\u3007OOo8\u30070[i]);
        }
        sb.append('}');
        return sb.toString();
    }
    
    public int \u300780\u3007808\u3007O(final int n) {
        final int oo = this.OO;
        if (!this.\u300708O\u300700\u3007o) {
            for (int i = 0; i < oo; ++i) {
                if (this.\u3007OOo8\u30070[i] == n) {
                    return i;
                }
            }
            return -oo;
        }
        int n2 = -1;
        int j = oo;
        while (j > n2 + 1) {
            final int n3 = (j - n2 >> 1) + n2;
            if (n <= this.\u3007OOo8\u30070[n3]) {
                j = n3;
            }
            else {
                n2 = n3;
            }
        }
        if (j != oo) {
            if (n != this.\u3007OOo8\u30070[j]) {
                j = -j - 1;
            }
            return j;
        }
        return -oo - 1;
    }
    
    public int \u30078o8o\u3007(int n) {
        if (n < this.OO) {
            try {
                n = this.\u3007OOo8\u30070[n];
                return n;
            }
            catch (final ArrayIndexOutOfBoundsException ex) {
                throw new IndexOutOfBoundsException("n < 0");
            }
        }
        throw new IndexOutOfBoundsException("n >= size()");
    }
    
    public void \u3007O\u3007() {
        this.o\u30070();
        if (!this.\u300708O\u300700\u3007o) {
            Arrays.sort(this.\u3007OOo8\u30070, 0, this.OO);
            this.\u300708O\u300700\u3007o = true;
        }
    }
    
    public void \u3007\u3007808\u3007(final int oo) {
        if (oo < 0) {
            throw new IllegalArgumentException("newSize < 0");
        }
        if (oo <= this.OO) {
            this.o\u30070();
            this.OO = oo;
            return;
        }
        throw new IllegalArgumentException("newSize > size");
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.android.dx.util;

import java.util.Arrays;

public class FixedSizeList extends MutabilityControl implements ToHuman
{
    private Object[] \u3007OOo8\u30070;
    
    public FixedSizeList(final int n) {
        super(n != 0);
        try {
            this.\u3007OOo8\u30070 = new Object[n];
        }
        catch (final NegativeArraySizeException ex) {
            throw new IllegalArgumentException("size < 0");
        }
    }
    
    private String Oooo8o0\u3007(final String str, final String str2, final String str3, final boolean b) {
        final int length = this.\u3007OOo8\u30070.length;
        final StringBuilder sb = new StringBuilder(length * 10 + 10);
        if (str != null) {
            sb.append(str);
        }
        for (int i = 0; i < length; ++i) {
            if (i != 0 && str2 != null) {
                sb.append(str2);
            }
            if (b) {
                sb.append(((ToHuman)this.\u3007OOo8\u30070[i]).toHuman());
            }
            else {
                sb.append(this.\u3007OOo8\u30070[i]);
            }
        }
        if (str3 != null) {
            sb.append(str3);
        }
        return sb.toString();
    }
    
    private Object \u30078o8o\u3007(final int n) {
        if (n < 0) {
            throw new IndexOutOfBoundsException("n < 0");
        }
        throw new IndexOutOfBoundsException("n >= size()");
    }
    
    public String OO0o\u3007\u3007(final String s, final String s2, final String s3) {
        return this.Oooo8o0\u3007(s, s2, s3, false);
    }
    
    protected final void OO0o\u3007\u3007\u3007\u30070(final int n, final Object o) {
        this.o\u30070();
        try {
            this.\u3007OOo8\u30070[n] = o;
        }
        catch (final ArrayIndexOutOfBoundsException ex) {
            this.\u30078o8o\u3007(n);
        }
    }
    
    @Override
    public boolean equals(final Object o) {
        return this == o || (o != null && this.getClass() == o.getClass() && Arrays.equals(this.\u3007OOo8\u30070, ((FixedSizeList)o).\u3007OOo8\u30070));
    }
    
    @Override
    public int hashCode() {
        return Arrays.hashCode(this.\u3007OOo8\u30070);
    }
    
    protected final Object oO80(final int i) {
        try {
            final Object o = this.\u3007OOo8\u30070[i];
            if (o != null) {
                return o;
            }
            final StringBuilder sb = new StringBuilder();
            sb.append("unset: ");
            sb.append(i);
            throw new NullPointerException(sb.toString());
        }
        catch (final ArrayIndexOutOfBoundsException ex) {
            return this.\u30078o8o\u3007(i);
        }
    }
    
    public final int size() {
        return this.\u3007OOo8\u30070.length;
    }
    
    @Override
    public String toHuman() {
        final String name = this.getClass().getName();
        final StringBuilder sb = new StringBuilder();
        sb.append(name.substring(name.lastIndexOf(46) + 1));
        sb.append('{');
        return this.Oooo8o0\u3007(sb.toString(), ", ", "}", true);
    }
    
    @Override
    public String toString() {
        final String name = this.getClass().getName();
        final StringBuilder sb = new StringBuilder();
        sb.append(name.substring(name.lastIndexOf(46) + 1));
        sb.append('{');
        return this.Oooo8o0\u3007(sb.toString(), ", ", "}", false);
    }
    
    protected final Object \u300780\u3007808\u3007O(final int n) {
        return this.\u3007OOo8\u30070[n];
    }
    
    public String \u3007O8o08O(final String s, final String s2, final String s3) {
        return this.Oooo8o0\u3007(s, s2, s3, true);
    }
}

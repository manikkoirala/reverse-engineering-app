// 
// Decompiled by Procyon v0.6.0
// 

package com.android.dx.util;

public final class ByteArray
{
    private final byte[] \u3007080;
    private final int \u3007o00\u3007\u3007Oo;
    private final int \u3007o\u3007;
    
    public ByteArray(final byte[] array) {
        this(array, 0, array.length);
    }
    
    public ByteArray(final byte[] \u3007080, final int \u3007o00\u3007\u3007Oo, final int n) {
        if (\u3007080 == null) {
            throw new NullPointerException("bytes == null");
        }
        if (\u3007o00\u3007\u3007Oo < 0) {
            throw new IllegalArgumentException("start < 0");
        }
        if (n < \u3007o00\u3007\u3007Oo) {
            throw new IllegalArgumentException("end < start");
        }
        if (n <= \u3007080.length) {
            this.\u3007080 = \u3007080;
            this.\u3007o00\u3007\u3007Oo = \u3007o00\u3007\u3007Oo;
            this.\u3007o\u3007 = n - \u3007o00\u3007\u3007Oo;
            return;
        }
        throw new IllegalArgumentException("end > bytes.length");
    }
    
    public void \u3007080(final byte[] array, final int n) {
        final int length = array.length;
        final int \u3007o\u3007 = this.\u3007o\u3007;
        if (length - n >= \u3007o\u3007) {
            System.arraycopy(this.\u3007080, this.\u3007o00\u3007\u3007Oo, array, n, \u3007o\u3007);
            return;
        }
        throw new IndexOutOfBoundsException("(out.length - offset) < size()");
    }
    
    public int \u3007o00\u3007\u3007Oo() {
        return this.\u3007o\u3007;
    }
}

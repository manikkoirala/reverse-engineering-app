// 
// Decompiled by Procyon v0.6.0
// 

package com.android.dx.util;

public class LabeledList extends FixedSizeList
{
    private final IntList OO;
    
    public LabeledList(final int n) {
        super(n);
        this.OO = new IntList(n);
    }
    
    private void \u3007\u3007808\u3007(final int n, final int n2) {
        for (int size = this.OO.size(), i = 0; i <= n - size; ++i) {
            this.OO.oO80(-1);
        }
        this.OO.Oooo8o0\u3007(n, n2);
    }
    
    private void \u3007\u30078O0\u30078(final int n) {
        this.OO.Oooo8o0\u3007(n, -1);
    }
    
    protected void \u30070\u3007O0088o(final int n, final LabeledItem labeledItem) {
        final LabeledItem labeledItem2 = (LabeledItem)this.\u300780\u3007808\u3007O(n);
        this.OO0o\u3007\u3007\u3007\u30070(n, labeledItem);
        if (labeledItem2 != null) {
            this.\u3007\u30078O0\u30078(labeledItem2.getLabel());
        }
        if (labeledItem != null) {
            this.\u3007\u3007808\u3007(labeledItem.getLabel(), n);
        }
    }
    
    public final int \u3007O00(final int n) {
        if (n >= this.OO.size()) {
            return -1;
        }
        return this.OO.\u30078o8o\u3007(n);
    }
    
    public final int \u3007O\u3007() {
        int n;
        for (n = this.OO.size() - 1; n >= 0 && this.OO.\u30078o8o\u3007(n) < 0; --n) {}
        ++n;
        this.OO.\u3007\u3007808\u3007(n);
        return n;
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.android.dx.util;

import java.io.IOException;
import java.io.Writer;
import com.android.dex.Leb128;
import java.util.Arrays;
import com.android.dex.util.ExceptionWithContext;
import java.util.ArrayList;
import com.android.dex.util.ByteOutput;

public final class ByteArrayAnnotatedOutput implements AnnotatedOutput, ByteOutput
{
    private boolean O8;
    private ArrayList<Annotation> Oo08;
    private int o\u30070;
    private final boolean \u3007080;
    private byte[] \u3007o00\u3007\u3007Oo;
    private int \u3007o\u3007;
    private int \u3007\u3007888;
    
    public ByteArrayAnnotatedOutput() {
        this(1000);
    }
    
    public ByteArrayAnnotatedOutput(final int n) {
        this(new byte[n], true);
    }
    
    public ByteArrayAnnotatedOutput(final byte[] array) {
        this(array, false);
    }
    
    private ByteArrayAnnotatedOutput(final byte[] \u3007o00\u3007\u3007Oo, final boolean \u3007080) {
        if (\u3007o00\u3007\u3007Oo != null) {
            this.\u3007080 = \u3007080;
            this.\u3007o00\u3007\u3007Oo = \u3007o00\u3007\u3007Oo;
            this.\u3007o\u3007 = 0;
            this.O8 = false;
            this.Oo08 = null;
            this.o\u30070 = 0;
            this.\u3007\u3007888 = 0;
            return;
        }
        throw new NullPointerException("data == null");
    }
    
    private void OO0o\u3007\u3007(final int n) {
        final byte[] \u3007o00\u3007\u3007Oo = this.\u3007o00\u3007\u3007Oo;
        if (\u3007o00\u3007\u3007Oo.length < n) {
            final byte[] \u3007o00\u3007\u3007Oo2 = new byte[n * 2 + 1000];
            System.arraycopy(\u3007o00\u3007\u3007Oo, 0, \u3007o00\u3007\u3007Oo2, 0, this.\u3007o\u3007);
            this.\u3007o00\u3007\u3007Oo = \u3007o00\u3007\u3007Oo2;
        }
    }
    
    private static void \u3007O\u3007() {
        throw new IndexOutOfBoundsException("attempt to write past the end");
    }
    
    @Override
    public void O8(final int i) {
        if (this.\u3007o\u3007 == i) {
            return;
        }
        final StringBuilder sb = new StringBuilder();
        sb.append("expected cursor ");
        sb.append(i);
        sb.append("; actual value: ");
        sb.append(this.\u3007o\u3007);
        throw new ExceptionWithContext(sb.toString());
    }
    
    @Override
    public boolean OO0o\u3007\u3007\u3007\u30070() {
        return this.O8;
    }
    
    @Override
    public void Oo08(int n) {
        final int n2 = n - 1;
        if (n >= 0 && (n & n2) == 0x0) {
            n = (this.\u3007o\u3007 + n2 & ~n2);
            if (this.\u3007080) {
                this.OO0o\u3007\u3007(n);
            }
            else if (n > this.\u3007o00\u3007\u3007Oo.length) {
                \u3007O\u3007();
                return;
            }
            Arrays.fill(this.\u3007o00\u3007\u3007Oo, this.\u3007o\u3007, n, (byte)0);
            this.\u3007o\u3007 = n;
            return;
        }
        throw new IllegalArgumentException("bogus alignment");
    }
    
    public int OoO8(final int n) {
        if (this.\u3007080) {
            this.OO0o\u3007\u3007(this.\u3007o\u3007 + 5);
        }
        final int \u3007o\u3007 = this.\u3007o\u3007;
        Leb128.\u3007o00\u3007\u3007Oo(this, n);
        return this.\u3007o\u3007 - \u3007o\u3007;
    }
    
    public void Oooo8o0\u3007() {
        this.\u3007o00\u3007\u3007Oo();
        final ArrayList<Annotation> oo08 = this.Oo08;
        if (oo08 != null) {
            int i = oo08.size();
            while (i > 0) {
                final ArrayList<Annotation> oo9 = this.Oo08;
                final int n = i - 1;
                final Annotation annotation = oo9.get(n);
                if (annotation.\u3007o00\u3007\u3007Oo() > this.\u3007o\u3007) {
                    this.Oo08.remove(n);
                    --i;
                }
                else {
                    final int \u3007080 = annotation.\u3007080();
                    final int \u3007o\u3007 = this.\u3007o\u3007;
                    if (\u3007080 > \u3007o\u3007) {
                        annotation.O8(\u3007o\u3007);
                        break;
                    }
                    break;
                }
            }
        }
    }
    
    @Override
    public int getCursor() {
        return this.\u3007o\u3007;
    }
    
    @Override
    public void oO80(final int n, final String s) {
        if (this.Oo08 == null) {
            return;
        }
        this.\u3007o00\u3007\u3007Oo();
        final int size = this.Oo08.size();
        int \u3007080;
        if (size == 0) {
            \u3007080 = 0;
        }
        else {
            \u3007080 = this.Oo08.get(size - 1).\u3007080();
        }
        final int \u3007o\u3007 = this.\u3007o\u3007;
        int n2 = \u3007080;
        if (\u3007080 <= \u3007o\u3007) {
            n2 = \u3007o\u3007;
        }
        this.Oo08.add(new Annotation(n2, n + n2, s));
    }
    
    @Override
    public void o\u30070(final String s) {
        if (this.Oo08 == null) {
            return;
        }
        this.\u3007o00\u3007\u3007Oo();
        this.Oo08.add(new Annotation(this.\u3007o\u3007, s));
    }
    
    @Override
    public void write(final byte[] array) {
        this.\u3007\u30078O0\u30078(array, 0, array.length);
    }
    
    @Override
    public void writeByte(final int n) {
        final int \u3007o\u3007 = this.\u3007o\u3007;
        final int \u3007o\u30072 = \u3007o\u3007 + 1;
        if (this.\u3007080) {
            this.OO0o\u3007\u3007(\u3007o\u30072);
        }
        else if (\u3007o\u30072 > this.\u3007o00\u3007\u3007Oo.length) {
            \u3007O\u3007();
            return;
        }
        this.\u3007o00\u3007\u3007Oo[\u3007o\u3007] = (byte)n;
        this.\u3007o\u3007 = \u3007o\u30072;
    }
    
    @Override
    public void writeInt(final int n) {
        final int \u3007o\u3007 = this.\u3007o\u3007;
        final int \u3007o\u30072 = \u3007o\u3007 + 4;
        if (this.\u3007080) {
            this.OO0o\u3007\u3007(\u3007o\u30072);
        }
        else if (\u3007o\u30072 > this.\u3007o00\u3007\u3007Oo.length) {
            \u3007O\u3007();
            return;
        }
        final byte[] \u3007o00\u3007\u3007Oo = this.\u3007o00\u3007\u3007Oo;
        \u3007o00\u3007\u3007Oo[\u3007o\u3007] = (byte)n;
        \u3007o00\u3007\u3007Oo[\u3007o\u3007 + 1] = (byte)(n >> 8);
        \u3007o00\u3007\u3007Oo[\u3007o\u3007 + 2] = (byte)(n >> 16);
        \u3007o00\u3007\u3007Oo[\u3007o\u3007 + 3] = (byte)(n >> 24);
        this.\u3007o\u3007 = \u3007o\u30072;
    }
    
    @Override
    public void writeShort(final int n) {
        final int \u3007o\u3007 = this.\u3007o\u3007;
        final int \u3007o\u30072 = \u3007o\u3007 + 2;
        if (this.\u3007080) {
            this.OO0o\u3007\u3007(\u3007o\u30072);
        }
        else if (\u3007o\u30072 > this.\u3007o00\u3007\u3007Oo.length) {
            \u3007O\u3007();
            return;
        }
        final byte[] \u3007o00\u3007\u3007Oo = this.\u3007o00\u3007\u3007Oo;
        \u3007o00\u3007\u3007Oo[\u3007o\u3007] = (byte)n;
        \u3007o00\u3007\u3007Oo[\u3007o\u3007 + 1] = (byte)(n >> 8);
        this.\u3007o\u3007 = \u3007o\u30072;
    }
    
    @Override
    public int \u3007080(final int n) {
        if (this.\u3007080) {
            this.OO0o\u3007\u3007(this.\u3007o\u3007 + 5);
        }
        final int \u3007o\u3007 = this.\u3007o\u3007;
        Leb128.\u3007o\u3007(this, n);
        return this.\u3007o\u3007 - \u3007o\u3007;
    }
    
    public void \u30070\u3007O0088o(final Writer writer) throws IOException {
        final int \u3007\u3007888 = this.\u3007\u3007888();
        final TwoColumnOutput twoColumnOutput = new TwoColumnOutput(writer, this.o\u30070 - \u3007\u3007888 - 1, \u3007\u3007888, "|");
        final Writer oo08 = twoColumnOutput.Oo08();
        final Writer o\u30070 = twoColumnOutput.o\u30070();
        final int size = this.Oo08.size();
        int index = 0;
        int n = 0;
        int \u3007o\u3007;
        while (true) {
            \u3007o\u3007 = this.\u3007o\u3007;
            if (n >= \u3007o\u3007 || index >= size) {
                break;
            }
            final Annotation annotation = this.Oo08.get(index);
            final int \u3007o00\u3007\u3007Oo = annotation.\u3007o00\u3007\u3007Oo();
            String \u3007o\u30072;
            int n2;
            int n3;
            int n4;
            if (n < \u3007o00\u3007\u3007Oo) {
                \u3007o\u30072 = "";
                n2 = n;
                n3 = index;
                n4 = \u3007o00\u3007\u3007Oo;
            }
            else {
                final int \u3007080 = annotation.\u3007080();
                \u3007o\u30072 = annotation.\u3007o\u3007();
                n3 = index + 1;
                n4 = \u3007080;
                n2 = \u3007o00\u3007\u3007Oo;
            }
            oo08.write(Hex.\u3007080(this.\u3007o00\u3007\u3007Oo, n2, n4 - n2, n2, this.\u3007\u3007888, 6));
            o\u30070.write(\u3007o\u30072);
            twoColumnOutput.\u3007o00\u3007\u3007Oo();
            final int n5 = n4;
            index = n3;
            n = n5;
        }
        int i = index;
        if (n < \u3007o\u3007) {
            oo08.write(Hex.\u3007080(this.\u3007o00\u3007\u3007Oo, n, \u3007o\u3007 - n, n, this.\u3007\u3007888, 6));
            i = index;
        }
        while (i < size) {
            o\u30070.write(this.Oo08.get(i).\u3007o\u3007());
            ++i;
        }
        twoColumnOutput.\u3007o00\u3007\u3007Oo();
    }
    
    @Override
    public void \u300780\u3007808\u3007O(int n) {
        if (n >= 0) {
            n += this.\u3007o\u3007;
            if (this.\u3007080) {
                this.OO0o\u3007\u3007(n);
            }
            else if (n > this.\u3007o00\u3007\u3007Oo.length) {
                \u3007O\u3007();
                return;
            }
            Arrays.fill(this.\u3007o00\u3007\u3007Oo, this.\u3007o\u3007, n, (byte)0);
            this.\u3007o\u3007 = n;
            return;
        }
        throw new IllegalArgumentException("count < 0");
    }
    
    @Override
    public void \u30078o8o\u3007(final ByteArray byteArray) {
        final int \u3007o00\u3007\u3007Oo = byteArray.\u3007o00\u3007\u3007Oo();
        final int \u3007o\u3007 = this.\u3007o\u3007;
        final int \u3007o\u30072 = \u3007o00\u3007\u3007Oo + \u3007o\u3007;
        if (this.\u3007080) {
            this.OO0o\u3007\u3007(\u3007o\u30072);
        }
        else if (\u3007o\u30072 > this.\u3007o00\u3007\u3007Oo.length) {
            \u3007O\u3007();
            return;
        }
        byteArray.\u3007080(this.\u3007o00\u3007\u3007Oo, \u3007o\u3007);
        this.\u3007o\u3007 = \u3007o\u30072;
    }
    
    public byte[] \u3007O00() {
        final int \u3007o\u3007 = this.\u3007o\u3007;
        final byte[] array = new byte[\u3007o\u3007];
        System.arraycopy(this.\u3007o00\u3007\u3007Oo, 0, array, 0, \u3007o\u3007);
        return array;
    }
    
    public void \u3007O8o08O(final int o\u30070, final boolean o8) {
        if (this.Oo08 != null || this.\u3007o\u3007 != 0) {
            throw new RuntimeException("cannot enable annotations");
        }
        if (o\u30070 >= 40) {
            final int n = (o\u30070 - 7) / 15 + 1 & 0xFFFFFFFE;
            int \u3007\u3007888;
            if (n < 6) {
                \u3007\u3007888 = 6;
            }
            else if ((\u3007\u3007888 = n) > 10) {
                \u3007\u3007888 = 10;
            }
            this.Oo08 = new ArrayList<Annotation>(1000);
            this.o\u30070 = o\u30070;
            this.\u3007\u3007888 = \u3007\u3007888;
            this.O8 = o8;
            return;
        }
        throw new IllegalArgumentException("annotationWidth < 40");
    }
    
    @Override
    public void \u3007o00\u3007\u3007Oo() {
        final ArrayList<Annotation> oo08 = this.Oo08;
        if (oo08 == null) {
            return;
        }
        final int size = oo08.size();
        if (size != 0) {
            this.Oo08.get(size - 1).Oo08(this.\u3007o\u3007);
        }
    }
    
    @Override
    public boolean \u3007o\u3007() {
        return this.Oo08 != null;
    }
    
    public byte[] \u3007\u3007808\u3007() {
        return this.\u3007o00\u3007\u3007Oo;
    }
    
    @Override
    public int \u3007\u3007888() {
        final int \u3007\u3007888 = this.\u3007\u3007888;
        return this.o\u30070 - (\u3007\u3007888 * 2 + 8 + \u3007\u3007888 / 2);
    }
    
    public void \u3007\u30078O0\u30078(final byte[] array, final int i, final int n) {
        final int \u3007o\u3007 = this.\u3007o\u3007;
        final int n2 = \u3007o\u3007 + n;
        if ((i | n | n2) >= 0 && i + n <= array.length) {
            if (this.\u3007080) {
                this.OO0o\u3007\u3007(n2);
            }
            else if (n2 > this.\u3007o00\u3007\u3007Oo.length) {
                \u3007O\u3007();
                return;
            }
            System.arraycopy(array, i, this.\u3007o00\u3007\u3007Oo, \u3007o\u3007, n);
            this.\u3007o\u3007 = n2;
            return;
        }
        final StringBuilder sb = new StringBuilder();
        sb.append("bytes.length ");
        sb.append(array.length);
        sb.append("; ");
        sb.append(i);
        sb.append("..!");
        sb.append(n2);
        throw new IndexOutOfBoundsException(sb.toString());
    }
    
    private static class Annotation
    {
        private final int \u3007080;
        private int \u3007o00\u3007\u3007Oo;
        private final String \u3007o\u3007;
        
        public Annotation(final int \u3007080, final int \u3007o00\u3007\u3007Oo, final String \u3007o\u3007) {
            this.\u3007080 = \u3007080;
            this.\u3007o00\u3007\u3007Oo = \u3007o00\u3007\u3007Oo;
            this.\u3007o\u3007 = \u3007o\u3007;
        }
        
        public Annotation(final int n, final String s) {
            this(n, Integer.MAX_VALUE, s);
        }
        
        public void O8(final int \u3007o00\u3007\u3007Oo) {
            this.\u3007o00\u3007\u3007Oo = \u3007o00\u3007\u3007Oo;
        }
        
        public void Oo08(final int \u3007o00\u3007\u3007Oo) {
            if (this.\u3007o00\u3007\u3007Oo == Integer.MAX_VALUE) {
                this.\u3007o00\u3007\u3007Oo = \u3007o00\u3007\u3007Oo;
            }
        }
        
        public int \u3007080() {
            return this.\u3007o00\u3007\u3007Oo;
        }
        
        public int \u3007o00\u3007\u3007Oo() {
            return this.\u3007080;
        }
        
        public String \u3007o\u3007() {
            return this.\u3007o\u3007;
        }
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.android.dx.util;

import java.io.IOException;
import java.io.StringWriter;
import java.io.Writer;

public final class TwoColumnOutput
{
    private final StringBuffer O8;
    private final IndentingWriter Oo08;
    private final IndentingWriter o\u30070;
    private final Writer \u3007080;
    private final int \u3007o00\u3007\u3007Oo;
    private final StringBuffer \u3007o\u3007;
    
    public TwoColumnOutput(final Writer \u3007080, final int \u3007o00\u3007\u3007Oo, final int n, final String s) {
        if (\u3007080 == null) {
            throw new NullPointerException("out == null");
        }
        if (\u3007o00\u3007\u3007Oo < 1) {
            throw new IllegalArgumentException("leftWidth < 1");
        }
        if (n < 1) {
            throw new IllegalArgumentException("rightWidth < 1");
        }
        if (s != null) {
            final StringWriter stringWriter = new StringWriter(1000);
            final StringWriter stringWriter2 = new StringWriter(1000);
            this.\u3007080 = \u3007080;
            this.\u3007o00\u3007\u3007Oo = \u3007o00\u3007\u3007Oo;
            this.\u3007o\u3007 = stringWriter.getBuffer();
            this.O8 = stringWriter2.getBuffer();
            this.Oo08 = new IndentingWriter(stringWriter, \u3007o00\u3007\u3007Oo);
            this.o\u30070 = new IndentingWriter(stringWriter2, n, s);
            return;
        }
        throw new NullPointerException("spacer == null");
    }
    
    private void O8() throws IOException {
        \u3007080(this.O8, this.o\u30070);
        while (this.O8.length() != 0) {
            this.Oo08.write(10);
            this.\u3007\u3007888();
        }
    }
    
    public static String oO80(final String str, final int n, final String s, final String str2, final int n2) {
        final StringWriter stringWriter = new StringWriter((str.length() + str2.length()) * 3);
        final TwoColumnOutput twoColumnOutput = new TwoColumnOutput(stringWriter, n, n2, s);
        try {
            twoColumnOutput.Oo08().write(str);
            twoColumnOutput.o\u30070().write(str2);
            twoColumnOutput.\u3007o00\u3007\u3007Oo();
            return stringWriter.toString();
        }
        catch (final IOException cause) {
            throw new RuntimeException("shouldn't happen", cause);
        }
    }
    
    private static void \u3007080(final StringBuffer sb, final Writer writer) throws IOException {
        final int length = sb.length();
        if (length != 0 && sb.charAt(length - 1) != '\n') {
            writer.write(10);
        }
    }
    
    private static void \u300780\u3007808\u3007O(final Writer writer, int i) throws IOException {
        while (i > 0) {
            writer.write(32);
            --i;
        }
    }
    
    private void \u3007o\u3007() throws IOException {
        \u3007080(this.\u3007o\u3007, this.Oo08);
        while (this.\u3007o\u3007.length() != 0) {
            this.o\u30070.write(10);
            this.\u3007\u3007888();
        }
    }
    
    private void \u3007\u3007888() throws IOException {
        while (true) {
            final int index = this.\u3007o\u3007.indexOf("\n");
            if (index < 0) {
                return;
            }
            final int index2 = this.O8.indexOf("\n");
            if (index2 < 0) {
                return;
            }
            if (index != 0) {
                this.\u3007080.write(this.\u3007o\u3007.substring(0, index));
            }
            if (index2 != 0) {
                \u300780\u3007808\u3007O(this.\u3007080, this.\u3007o00\u3007\u3007Oo - index);
                this.\u3007080.write(this.O8.substring(0, index2));
            }
            this.\u3007080.write(10);
            this.\u3007o\u3007.delete(0, index + 1);
            this.O8.delete(0, index2 + 1);
        }
    }
    
    public Writer Oo08() {
        return this.Oo08;
    }
    
    public Writer o\u30070() {
        return this.o\u30070;
    }
    
    public void \u3007o00\u3007\u3007Oo() {
        try {
            \u3007080(this.\u3007o\u3007, this.Oo08);
            \u3007080(this.O8, this.o\u30070);
            this.\u3007\u3007888();
            this.\u3007o\u3007();
            this.O8();
        }
        catch (final IOException cause) {
            throw new RuntimeException(cause);
        }
    }
}

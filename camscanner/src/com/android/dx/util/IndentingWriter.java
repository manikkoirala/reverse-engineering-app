// 
// Decompiled by Procyon v0.6.0
// 

package com.android.dx.util;

import java.io.IOException;
import java.io.Writer;
import java.io.FilterWriter;

public final class IndentingWriter extends FilterWriter
{
    private int O8o08O8O;
    private final int OO;
    private final String o0;
    private boolean o\u300700O;
    private int \u300708O\u300700\u3007o;
    private final int \u3007OOo8\u30070;
    
    public IndentingWriter(final Writer writer, final int n) {
        this(writer, n, "");
    }
    
    public IndentingWriter(final Writer out, final int n, final String s) {
        super(out);
        if (out == null) {
            throw new NullPointerException("out == null");
        }
        if (n < 0) {
            throw new IllegalArgumentException("width < 0");
        }
        if (s != null) {
            int \u3007oOo8\u30070;
            if (n != 0) {
                \u3007oOo8\u30070 = n;
            }
            else {
                \u3007oOo8\u30070 = Integer.MAX_VALUE;
            }
            this.\u3007OOo8\u30070 = \u3007oOo8\u30070;
            this.OO = n >> 1;
            String o0 = s;
            if (s.length() == 0) {
                o0 = null;
            }
            this.o0 = o0;
            this.Oo08();
            return;
        }
        throw new NullPointerException("prefix == null");
    }
    
    private void Oo08() {
        this.\u300708O\u300700\u3007o = 0;
        this.o\u300700O = (this.OO != 0);
        this.O8o08O8O = 0;
    }
    
    @Override
    public void write(final int c) throws IOException {
        synchronized (super.lock) {
            final boolean o\u300700O = this.o\u300700O;
            int n = 0;
            if (o\u300700O) {
                if (c == 32) {
                    final int o8o08O8O = this.O8o08O8O + 1;
                    this.O8o08O8O = o8o08O8O;
                    final int oo = this.OO;
                    if (o8o08O8O >= oo) {
                        this.O8o08O8O = oo;
                        this.o\u300700O = false;
                    }
                }
                else {
                    this.o\u300700O = false;
                }
            }
            if (this.\u300708O\u300700\u3007o == this.\u3007OOo8\u30070 && c != 10) {
                super.out.write(10);
                this.\u300708O\u300700\u3007o = 0;
            }
            if (this.\u300708O\u300700\u3007o == 0) {
                final String o0 = this.o0;
                if (o0 != null) {
                    super.out.write(o0);
                }
                if (!this.o\u300700O) {
                    int o8o08O8O2;
                    while (true) {
                        o8o08O8O2 = this.O8o08O8O;
                        if (n >= o8o08O8O2) {
                            break;
                        }
                        super.out.write(32);
                        ++n;
                    }
                    this.\u300708O\u300700\u3007o = o8o08O8O2;
                }
            }
            super.out.write(c);
            if (c == 10) {
                this.Oo08();
            }
            else {
                ++this.\u300708O\u300700\u3007o;
            }
        }
    }
    
    @Override
    public void write(final String s, int index, int n) throws IOException {
        final Object lock = super.lock;
        monitorenter(lock);
        while (true) {
            if (n <= 0) {
                return;
            }
            try {
                this.write(s.charAt(index));
                ++index;
                --n;
                continue;
            }
            finally {
                monitorexit(lock);
            }
        }
    }
    
    @Override
    public void write(final char[] array, int n, int n2) throws IOException {
        final Object lock = super.lock;
        monitorenter(lock);
        while (true) {
            if (n2 <= 0) {
                return;
            }
            try {
                this.write(array[n]);
                ++n;
                --n2;
                continue;
            }
            finally {
                monitorexit(lock);
            }
        }
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.android.dx;

import java.util.zip.ZipOutputStream;
import com.android.dx.rop.cst.CstType;
import com.android.dx.rop.cst.Constant;
import com.android.dx.rop.cst.CstString;
import com.android.dx.dex.file.ClassDefItem;
import com.android.dx.dex.code.DalvCode;
import com.android.dx.rop.type.StdTypeList;
import com.android.dx.rop.code.LocalVariableInfo;
import com.android.dx.dex.code.RopTranslator;
import com.android.dx.rop.code.RopMethod;
import com.android.dx.dex.file.EncodedMethod;
import com.android.dx.dex.file.EncodedField;
import java.util.zip.ZipEntry;
import java.util.jar.JarEntry;
import java.util.jar.JarOutputStream;
import java.io.OutputStream;
import java.io.BufferedOutputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.Writer;
import com.android.dx.dex.DexOptions;
import java.util.Iterator;
import java.util.Set;
import java.util.Arrays;
import java.io.PrintStream;
import java.lang.reflect.InvocationTargetException;
import java.io.File;
import java.util.LinkedHashMap;
import com.android.dx.dex.file.DexFile;
import java.util.Map;

public final class DexMaker
{
    private static boolean Oo08;
    private static boolean o\u30070;
    private boolean O8;
    private final Map<TypeId<?>, TypeDeclaration> \u3007080;
    private ClassLoader \u3007o00\u3007\u3007Oo;
    private DexFile \u3007o\u3007;
    
    public DexMaker() {
        this.\u3007080 = new LinkedHashMap<TypeId<?>, TypeDeclaration>();
    }
    
    private ClassLoader o\u30070(final File file, final File file2, ClassLoader obj) {
        try {
            final ClassLoader \u3007o00\u3007\u3007Oo = this.\u3007o00\u3007\u3007Oo;
            final boolean b = \u3007o00\u3007\u3007Oo != null;
            if (obj == null) {
                if (\u3007o00\u3007\u3007Oo != null) {
                    obj = \u3007o00\u3007\u3007Oo;
                }
                else {
                    obj = null;
                }
            }
            final Class<?> forName = Class.forName("dalvik.system.BaseDexClassLoader");
            boolean b2 = b;
            if (b) {
                b2 = b;
                if (!forName.isAssignableFrom(obj.getClass())) {
                    if (!obj.getClass().getName().equals("java.lang.BootClassLoader") && !DexMaker.o\u30070) {
                        final PrintStream err = System.err;
                        final StringBuilder sb = new StringBuilder();
                        sb.append("Cannot share classloader as shared classloader '");
                        sb.append(obj);
                        sb.append("' is not a subclass of '");
                        sb.append(forName);
                        sb.append("'");
                        err.println(sb.toString());
                        DexMaker.o\u30070 = true;
                    }
                    b2 = false;
                }
            }
            if (this.O8) {
                Label_0228: {
                    if (!b2) {
                        break Label_0228;
                    }
                    try {
                        obj.getClass().getMethod("addDexPath", String.class, Boolean.TYPE).invoke(obj, file.getPath(), Boolean.TRUE);
                        return obj;
                        return forName.getConstructor(String.class, File.class, String.class, ClassLoader.class, Boolean.TYPE).newInstance(file.getPath(), file2.getAbsoluteFile(), null, obj, Boolean.TRUE);
                    }
                    catch (final InvocationTargetException ex) {
                        if (!(ex.getCause() instanceof SecurityException)) {
                            throw ex;
                        }
                        if (!DexMaker.Oo08) {
                            final PrintStream err2 = System.err;
                            final StringBuilder sb2 = new StringBuilder();
                            sb2.append("Cannot allow to call blacklisted super methods. This might break spying on system classes.");
                            sb2.append(ex.getCause());
                            err2.println(sb2.toString());
                            DexMaker.Oo08 = true;
                        }
                    }
                }
            }
            if (b2) {
                obj.getClass().getMethod("addDexPath", String.class).invoke(obj, file.getPath());
                return obj;
            }
            return (ClassLoader)Class.forName("dalvik.system.DexClassLoader").getConstructor(String.class, String.class, String.class, ClassLoader.class).newInstance(file.getPath(), file2.getAbsolutePath(), null, obj);
        }
        catch (final IllegalAccessException ex2) {
            throw new AssertionError();
        }
        catch (final NoSuchMethodException ex3) {
            throw new AssertionError();
        }
        catch (final InstantiationException ex4) {
            throw new AssertionError();
        }
        catch (final InvocationTargetException ex5) {
            throw new RuntimeException(ex5.getCause());
        }
        catch (final ClassNotFoundException cause) {
            throw new UnsupportedOperationException("load() requires a Dalvik VM", cause);
        }
    }
    
    private String \u3007\u3007888() {
        final Set<TypeId<?>> keySet = this.\u3007080.keySet();
        final Iterator<TypeId<?>> iterator = keySet.iterator();
        final int size = keySet.size();
        final int[] a = new int[size];
        final int n = 0;
        int n2 = 0;
        while (iterator.hasNext()) {
            final TypeDeclaration oo80 = this.oO80(iterator.next());
            final Set keySet2 = oo80.\u300780\u3007808\u3007O.keySet();
            if (oo80.O8 != null) {
                a[n2] = (oo80.O8.hashCode() * 31 + oo80.o\u30070.hashCode()) * 31 + keySet2.hashCode();
                ++n2;
            }
        }
        Arrays.sort(a);
        final int n3 = 1;
        int i = n;
        int j = n3;
        while (i < size) {
            j = j * 31 + a[i];
            ++i;
        }
        final StringBuilder sb = new StringBuilder();
        sb.append("Generated_");
        sb.append(j);
        sb.append(".jar");
        return sb.toString();
    }
    
    public byte[] O8() {
        if (this.\u3007o\u3007 == null) {
            final DexOptions dexOptions = new DexOptions();
            dexOptions.\u3007o00\u3007\u3007Oo = 13;
            this.\u3007o\u3007 = new DexFile(dexOptions);
        }
        final Iterator<TypeDeclaration> iterator = this.\u3007080.values().iterator();
        while (iterator.hasNext()) {
            this.\u3007o\u3007.\u3007080(iterator.next().\u30078o8o\u3007());
        }
        try {
            return this.\u3007o\u3007.o\u3007O8\u3007\u3007o(null, false);
        }
        catch (final IOException cause) {
            throw new RuntimeException(cause);
        }
    }
    
    public void OO0o\u3007\u3007\u3007\u30070(final ClassLoader \u3007o00\u3007\u3007Oo) {
        this.\u3007o00\u3007\u3007Oo = \u3007o00\u3007\u3007Oo;
    }
    
    public ClassLoader Oo08(final ClassLoader classLoader, File file) throws IOException {
        File oo08 = file;
        if (file == null) {
            final String property = System.getProperty("dexmaker.dexcache");
            if (property != null) {
                oo08 = new File(property);
            }
            else {
                oo08 = new AppDataDirGuesser().Oo08();
                if (oo08 == null) {
                    throw new IllegalArgumentException("dexcache == null (and no default could be found; consider setting the 'dexmaker.dexcache' system property)");
                }
            }
        }
        final File file2 = new File(oo08, this.\u3007\u3007888());
        if (file2.exists()) {
            if (!file2.canWrite()) {
                return this.o\u30070(file2, oo08, classLoader);
            }
            file2.delete();
        }
        final byte[] o8 = this.O8();
        file = (File)new JarOutputStream(new BufferedOutputStream(new FileOutputStream(file2)));
        file2.setReadOnly();
        try {
            final JarEntry ze = new JarEntry("classes.dex");
            ze.setSize(o8.length);
            ((JarOutputStream)file).putNextEntry(ze);
            try {
                ((OutputStream)file).write(o8);
                ((ZipOutputStream)file).closeEntry();
                ((OutputStream)file).close();
                return this.o\u30070(file2, oo08, classLoader);
            }
            finally {
                ((ZipOutputStream)file).closeEntry();
            }
        }
        finally {
            ((OutputStream)file).close();
        }
    }
    
    TypeDeclaration oO80(final TypeId<?> typeId) {
        TypeDeclaration typeDeclaration;
        if ((typeDeclaration = this.\u3007080.get(typeId)) == null) {
            typeDeclaration = new TypeDeclaration(typeId);
            this.\u3007080.put(typeId, typeDeclaration);
        }
        return typeDeclaration;
    }
    
    public Code \u3007080(final MethodId<?, ?> obj, int i) {
        final TypeDeclaration oo80 = this.oO80(obj.\u3007080);
        if (oo80.\u300780\u3007808\u3007O.containsKey(obj)) {
            final StringBuilder sb = new StringBuilder();
            sb.append("already declared: ");
            sb.append(obj);
            throw new IllegalStateException(sb.toString());
        }
        if ((i & 0xFFFFEA80) == 0x0) {
            int n = i;
            if ((i & 0x20) != 0x0) {
                n = ((i & 0xFFFFFFDF) | 0x20000);
            }
            Label_0072: {
                if (!obj.\u3007o00\u3007\u3007Oo()) {
                    i = n;
                    if (!obj.\u3007o\u3007()) {
                        break Label_0072;
                    }
                }
                i = (n | 0x10000);
            }
            final MethodDeclaration methodDeclaration = new MethodDeclaration(obj, i);
            oo80.\u300780\u3007808\u3007O.put(obj, methodDeclaration);
            return methodDeclaration.\u3007o\u3007;
        }
        final StringBuilder sb2 = new StringBuilder();
        sb2.append("Unexpected flag: ");
        sb2.append(Integer.toHexString(i));
        throw new IllegalArgumentException(sb2.toString());
    }
    
    public void \u300780\u3007808\u3007O() {
        this.O8 = true;
    }
    
    public void \u3007o00\u3007\u3007Oo(final FieldId<?, ?> obj, final int i, final Object o) {
        final TypeDeclaration oo80 = this.oO80(obj.\u3007080);
        if (oo80.oO80.containsKey(obj)) {
            final StringBuilder sb = new StringBuilder();
            sb.append("already declared: ");
            sb.append(obj);
            throw new IllegalStateException(sb.toString());
        }
        if ((i & 0xFFFFEF20) != 0x0) {
            final StringBuilder sb2 = new StringBuilder();
            sb2.append("Unexpected flag: ");
            sb2.append(Integer.toHexString(i));
            throw new IllegalArgumentException(sb2.toString());
        }
        if ((i & 0x8) == 0x0 && o != null) {
            throw new IllegalArgumentException("staticValue is non-null, but field is not static");
        }
        oo80.oO80.put(obj, new FieldDeclaration(obj, i, o));
    }
    
    public void \u3007o\u3007(final TypeId<?> obj, final String s, final int i, final TypeId<?> typeId, final TypeId<?>... array) {
        final TypeDeclaration oo80 = this.oO80(obj);
        if ((i & 0xFFFFEBEE) != 0x0) {
            final StringBuilder sb = new StringBuilder();
            sb.append("Unexpected flag: ");
            sb.append(Integer.toHexString(i));
            throw new IllegalArgumentException(sb.toString());
        }
        if (!oo80.\u3007o00\u3007\u3007Oo) {
            oo80.\u3007o00\u3007\u3007Oo = true;
            oo80.\u3007o\u3007 = i;
            oo80.O8 = typeId;
            oo80.Oo08 = s;
            oo80.o\u30070 = new TypeList(array);
            return;
        }
        final StringBuilder sb2 = new StringBuilder();
        sb2.append("already declared: ");
        sb2.append(obj);
        throw new IllegalStateException(sb2.toString());
    }
    
    static class FieldDeclaration
    {
        final FieldId<?, ?> \u3007080;
        private final int \u3007o00\u3007\u3007Oo;
        private final Object \u3007o\u3007;
        
        FieldDeclaration(final FieldId<?, ?> \u3007080, final int \u3007o00\u3007\u3007Oo, final Object \u3007o\u3007) {
            if ((\u3007o00\u3007\u3007Oo & 0x8) == 0x0 && \u3007o\u3007 != null) {
                throw new IllegalArgumentException("instance fields may not have a value");
            }
            this.\u3007080 = \u3007080;
            this.\u3007o00\u3007\u3007Oo = \u3007o00\u3007\u3007Oo;
            this.\u3007o\u3007 = \u3007o\u3007;
        }
        
        public boolean \u3007o00\u3007\u3007Oo() {
            return (this.\u3007o00\u3007\u3007Oo & 0x8) != 0x0;
        }
        
        EncodedField \u3007o\u3007() {
            return new EncodedField(this.\u3007080.Oo08, this.\u3007o00\u3007\u3007Oo);
        }
    }
    
    static class MethodDeclaration
    {
        final MethodId<?, ?> \u3007080;
        private final int \u3007o00\u3007\u3007Oo;
        private final Code \u3007o\u3007;
        
        public MethodDeclaration(final MethodId<?, ?> \u3007080, final int \u3007o00\u3007\u3007Oo) {
            this.\u3007080 = \u3007080;
            this.\u3007o00\u3007\u3007Oo = \u3007o00\u3007\u3007Oo;
            this.\u3007o\u3007 = new Code(this);
        }
        
        EncodedMethod O8(final DexOptions dexOptions) {
            final int \u3007o00\u3007\u3007Oo = this.\u3007o00\u3007\u3007Oo;
            if ((\u3007o00\u3007\u3007Oo & 0x400) == 0x0 && (\u3007o00\u3007\u3007Oo & 0x100) == 0x0) {
                return new EncodedMethod(this.\u3007080.o\u30070, this.\u3007o00\u3007\u3007Oo, RopTranslator.Oooo8o0\u3007(new RopMethod(this.\u3007o\u3007.\u3007o(), 0), 1, null, this.\u3007o\u3007.\u30070000OOO(), dexOptions), StdTypeList.OO);
            }
            return new EncodedMethod(this.\u3007080.o\u30070, \u3007o00\u3007\u3007Oo, null, StdTypeList.OO);
        }
        
        boolean \u3007o00\u3007\u3007Oo() {
            return (this.\u3007o00\u3007\u3007Oo & 0x1000A) != 0x0;
        }
        
        boolean \u3007o\u3007() {
            return (this.\u3007o00\u3007\u3007Oo & 0x8) != 0x0;
        }
    }
    
    static class TypeDeclaration
    {
        private TypeId<?> O8;
        private String Oo08;
        private final Map<FieldId, FieldDeclaration> oO80;
        private TypeList o\u30070;
        private final TypeId<?> \u3007080;
        private final Map<MethodId, MethodDeclaration> \u300780\u3007808\u3007O;
        private boolean \u3007o00\u3007\u3007Oo;
        private int \u3007o\u3007;
        private ClassDefItem \u3007\u3007888;
        
        TypeDeclaration(final TypeId<?> \u3007080) {
            this.oO80 = new LinkedHashMap<FieldId, FieldDeclaration>();
            this.\u300780\u3007808\u3007O = new LinkedHashMap<MethodId, MethodDeclaration>();
            this.\u3007080 = \u3007080;
        }
        
        ClassDefItem \u30078o8o\u3007() {
            if (this.\u3007o00\u3007\u3007Oo) {
                final DexOptions dexOptions = new DexOptions();
                dexOptions.\u3007o00\u3007\u3007Oo = 13;
                final CstType \u3007o\u3007 = this.\u3007080.\u3007o\u3007;
                if (this.\u3007\u3007888 == null) {
                    this.\u3007\u3007888 = new ClassDefItem(\u3007o\u3007, this.\u3007o\u3007, this.O8.\u3007o\u3007, this.o\u30070.\u3007o00\u3007\u3007Oo, new CstString(this.Oo08));
                    for (final MethodDeclaration methodDeclaration : this.\u300780\u3007808\u3007O.values()) {
                        final EncodedMethod o8 = methodDeclaration.O8(dexOptions);
                        if (methodDeclaration.\u3007o00\u3007\u3007Oo()) {
                            this.\u3007\u3007888.OO0o\u3007\u3007\u3007\u30070(o8);
                        }
                        else {
                            this.\u3007\u3007888.OO0o\u3007\u3007(o8);
                        }
                    }
                    for (final FieldDeclaration fieldDeclaration : this.oO80.values()) {
                        final EncodedField \u3007o\u30072 = fieldDeclaration.\u3007o\u3007();
                        if (fieldDeclaration.\u3007o00\u3007\u3007Oo()) {
                            this.\u3007\u3007888.\u3007O8o08O(\u3007o\u30072, Constants.\u3007080(fieldDeclaration.\u3007o\u3007));
                        }
                        else {
                            this.\u3007\u3007888.\u30078o8o\u3007(\u3007o\u30072);
                        }
                    }
                }
                return this.\u3007\u3007888;
            }
            final StringBuilder sb = new StringBuilder();
            sb.append("Undeclared type ");
            sb.append(this.\u3007080);
            sb.append(" declares members: ");
            sb.append(this.oO80.keySet());
            sb.append(" ");
            sb.append(this.\u300780\u3007808\u3007O.keySet());
            throw new IllegalStateException(sb.toString());
        }
    }
}

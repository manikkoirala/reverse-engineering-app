// 
// Decompiled by Procyon v0.6.0
// 

package com.android.dx;

import com.android.dx.rop.cst.CstString;
import com.android.dx.rop.cst.CstFieldRef;
import com.android.dx.rop.cst.CstNat;

public final class FieldId<D, V>
{
    final CstNat O8;
    final CstFieldRef Oo08;
    final TypeId<D> \u3007080;
    final TypeId<V> \u3007o00\u3007\u3007Oo;
    final String \u3007o\u3007;
    
    FieldId(final TypeId<D> \u3007080, final TypeId<V> \u3007o00\u3007\u3007Oo, final String \u3007o\u3007) {
        if (\u3007080 != null && \u3007o00\u3007\u3007Oo != null && \u3007o\u3007 != null) {
            this.\u3007080 = \u3007080;
            this.\u3007o00\u3007\u3007Oo = \u3007o00\u3007\u3007Oo;
            this.\u3007o\u3007 = \u3007o\u3007;
            final CstNat o8 = new CstNat(new CstString(\u3007o\u3007), new CstString(\u3007o00\u3007\u3007Oo.\u3007080));
            this.O8 = o8;
            this.Oo08 = new CstFieldRef(\u3007080.\u3007o\u3007, o8);
            return;
        }
        throw null;
    }
    
    @Override
    public boolean equals(final Object o) {
        if (o instanceof FieldId) {
            final FieldId fieldId = (FieldId)o;
            if (fieldId.\u3007080.equals(this.\u3007080) && fieldId.\u3007o\u3007.equals(this.\u3007o\u3007)) {
                return true;
            }
        }
        return false;
    }
    
    public TypeId<V> getType() {
        return this.\u3007o00\u3007\u3007Oo;
    }
    
    @Override
    public int hashCode() {
        return this.\u3007080.hashCode() + this.\u3007o\u3007.hashCode() * 37;
    }
    
    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append(this.\u3007080);
        sb.append(".");
        sb.append(this.\u3007o\u3007);
        return sb.toString();
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.android.dx;

import com.android.dx.rop.type.TypeBearer;
import com.android.dx.rop.code.RegisterSpec;

public final class Local<T>
{
    private RegisterSpec O8;
    private final Code \u3007080;
    final TypeId<T> \u3007o00\u3007\u3007Oo;
    private int \u3007o\u3007;
    
    private Local(final Code \u3007080, final TypeId<T> \u3007o00\u3007\u3007Oo) {
        this.\u3007o\u3007 = -1;
        this.\u3007080 = \u3007080;
        this.\u3007o00\u3007\u3007Oo = \u3007o00\u3007\u3007Oo;
    }
    
    static <T> Local<T> \u3007080(final Code code, final TypeId<T> typeId) {
        return new Local<T>(code, typeId);
    }
    
    RegisterSpec O8() {
        if (this.O8 == null) {
            this.\u3007080.\u3007\u3007808\u3007();
            if (this.O8 == null) {
                throw new AssertionError();
            }
        }
        return this.O8;
    }
    
    public TypeId getType() {
        return this.\u3007o00\u3007\u3007Oo;
    }
    
    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append("v");
        sb.append(this.\u3007o\u3007);
        sb.append("(");
        sb.append(this.\u3007o00\u3007\u3007Oo);
        sb.append(")");
        return sb.toString();
    }
    
    int \u3007o00\u3007\u3007Oo(final int \u3007o\u3007) {
        this.\u3007o\u3007 = \u3007o\u3007;
        this.O8 = RegisterSpec.\u3007O\u3007(\u3007o\u3007, this.\u3007o00\u3007\u3007Oo.\u3007o00\u3007\u3007Oo);
        return this.\u3007o\u3007();
    }
    
    int \u3007o\u3007() {
        return this.\u3007o00\u3007\u3007Oo.\u3007o00\u3007\u3007Oo.Oo08();
    }
}

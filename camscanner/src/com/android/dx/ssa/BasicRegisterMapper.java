// 
// Decompiled by Procyon v0.6.0
// 

package com.android.dx.ssa;

import com.android.dx.rop.code.RegisterSpec;
import com.android.dx.util.IntList;

public class BasicRegisterMapper extends RegisterMapper
{
    private final IntList \u3007080;
    private int \u3007o00\u3007\u3007Oo;
    
    public BasicRegisterMapper(final int n) {
        this.\u3007080 = new IntList(n);
    }
    
    public void Oo08(int \u3007o00\u3007\u3007Oo, int \u3007o00\u3007\u3007Oo2, final int n) {
        if (\u3007o00\u3007\u3007Oo >= this.\u3007080.size()) {
            for (int i = \u3007o00\u3007\u3007Oo - this.\u3007080.size(); i >= 0; --i) {
                this.\u3007080.oO80(-1);
            }
        }
        this.\u3007080.Oooo8o0\u3007(\u3007o00\u3007\u3007Oo, \u3007o00\u3007\u3007Oo2);
        \u3007o00\u3007\u3007Oo = this.\u3007o00\u3007\u3007Oo;
        \u3007o00\u3007\u3007Oo2 += n;
        if (\u3007o00\u3007\u3007Oo < \u3007o00\u3007\u3007Oo2) {
            this.\u3007o00\u3007\u3007Oo = \u3007o00\u3007\u3007Oo2;
        }
    }
    
    @Override
    public int \u3007080() {
        return this.\u3007o00\u3007\u3007Oo;
    }
    
    @Override
    public RegisterSpec \u3007o00\u3007\u3007Oo(final RegisterSpec registerSpec) {
        if (registerSpec == null) {
            return null;
        }
        int \u30078o8o\u3007;
        try {
            \u30078o8o\u3007 = this.\u3007080.\u30078o8o\u3007(registerSpec.\u30078o8o\u3007());
        }
        catch (final IndexOutOfBoundsException ex) {
            \u30078o8o\u3007 = -1;
        }
        if (\u30078o8o\u3007 >= 0) {
            return registerSpec.oo88o8O(\u30078o8o\u3007);
        }
        throw new RuntimeException("no mapping specified for register");
    }
}

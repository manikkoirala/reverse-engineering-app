// 
// Decompiled by Procyon v0.6.0
// 

package com.android.dx.ssa;

import com.android.dx.rop.code.RegisterSpecList;
import com.android.dx.rop.code.RegisterSpec;
import com.android.dx.rop.code.RegisterSpecSet;

public abstract class RegisterMapper
{
    public final RegisterSpecSet O8(RegisterSpecSet set) {
        final int oo0o\u3007\u3007\u3007\u30070 = set.OO0o\u3007\u3007\u3007\u30070();
        final RegisterSpecSet set2 = new RegisterSpecSet(this.\u3007080());
        for (int i = 0; i < oo0o\u3007\u3007\u3007\u30070; ++i) {
            final RegisterSpec \u300780\u3007808\u3007O = set.\u300780\u3007808\u3007O(i);
            if (\u300780\u3007808\u3007O != null) {
                set2.\u30078o8o\u3007(this.\u3007o00\u3007\u3007Oo(\u300780\u3007808\u3007O));
            }
        }
        set2.Oo08();
        if (!set2.equals(set)) {
            set = set2;
        }
        return set;
    }
    
    public abstract int \u3007080();
    
    public abstract RegisterSpec \u3007o00\u3007\u3007Oo(final RegisterSpec p0);
    
    public final RegisterSpecList \u3007o\u3007(RegisterSpecList list) {
        final int size = list.size();
        final RegisterSpecList list2 = new RegisterSpecList(size);
        for (int i = 0; i < size; ++i) {
            list2.\u3007O888o0o(i, this.\u3007o00\u3007\u3007Oo(list.\u3007O00(i)));
        }
        list2.Oo08();
        if (!list2.equals(list)) {
            list = list2;
        }
        return list;
    }
}

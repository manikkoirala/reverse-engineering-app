// 
// Decompiled by Procyon v0.6.0
// 

package com.android.dx.ssa;

public enum Optimizer$OptionalStep
{
    private static final Optimizer$OptionalStep[] $VALUES;
    
    CONST_COLLECTOR, 
    ESCAPE_ANALYSIS, 
    LITERAL_UPGRADE, 
    MOVE_PARAM_COMBINER, 
    SCCP;
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.android.dx.rop.code;

import com.android.dx.rop.type.Type;
import com.android.dx.rop.cst.Constant;
import com.android.dx.rop.cst.CstString;
import java.util.concurrent.ConcurrentHashMap;
import com.android.dx.util.ToHuman;
import com.android.dx.rop.type.TypeBearer;

public final class RegisterSpec implements TypeBearer, ToHuman, Comparable<RegisterSpec>
{
    private static final ConcurrentHashMap<Object, RegisterSpec> OO;
    private static final ThreadLocal<ForComparison> \u300708O\u300700\u3007o;
    private final int o0;
    private final TypeBearer \u3007OOo8\u30070;
    
    static {
        OO = new ConcurrentHashMap<Object, RegisterSpec>(10000, 0.75f);
        \u300708O\u300700\u3007o = new ThreadLocal<ForComparison>() {
            protected ForComparison \u3007080() {
                return new ForComparison();
            }
        };
    }
    
    private RegisterSpec(final int o0, final TypeBearer \u3007oOo8\u30070, final LocalItem localItem) {
        if (o0 < 0) {
            throw new IllegalArgumentException("reg < 0");
        }
        if (\u3007oOo8\u30070 != null) {
            this.o0 = o0;
            this.\u3007OOo8\u30070 = \u3007oOo8\u30070;
            return;
        }
        throw new NullPointerException("type == null");
    }
    
    private static RegisterSpec OO0o\u3007\u3007(final int n, final TypeBearer typeBearer, final LocalItem localItem) {
        final ForComparison key = RegisterSpec.\u300708O\u300700\u3007o.get();
        key.O8(n, typeBearer, localItem);
        final ConcurrentHashMap<Object, RegisterSpec> oo = RegisterSpec.OO;
        RegisterSpec oo2;
        if ((oo2 = oo.get(key)) == null) {
            oo2 = key.Oo08();
            final RegisterSpec registerSpec = oo.putIfAbsent(oo2, oo2);
            if (registerSpec != null) {
                return registerSpec;
            }
        }
        return oo2;
    }
    
    public static String OoO8(final int i) {
        final StringBuilder sb = new StringBuilder();
        sb.append("v");
        sb.append(i);
        return sb.toString();
    }
    
    private String o800o8O(final boolean b) {
        final StringBuilder sb = new StringBuilder(40);
        sb.append(this.\u30070\u3007O0088o());
        sb.append(":");
        final Type type = this.\u3007OOo8\u30070.getType();
        sb.append(type);
        if (type != this.\u3007OOo8\u30070) {
            sb.append("=");
            if (b) {
                final TypeBearer \u3007oOo8\u30070 = this.\u3007OOo8\u30070;
                if (\u3007oOo8\u30070 instanceof CstString) {
                    sb.append(((CstString)\u3007oOo8\u30070).OO0o\u3007\u3007\u3007\u30070());
                    return sb.toString();
                }
            }
            if (b) {
                final TypeBearer \u3007oOo8\u30072 = this.\u3007OOo8\u30070;
                if (\u3007oOo8\u30072 instanceof Constant) {
                    sb.append(\u3007oOo8\u30072.toHuman());
                    return sb.toString();
                }
            }
            sb.append(this.\u3007OOo8\u30070);
        }
        return sb.toString();
    }
    
    private boolean o\u30070(final int n, final TypeBearer obj, final LocalItem localItem) {
        return this.o0 == n && this.\u3007OOo8\u30070.equals(obj);
    }
    
    public static RegisterSpec \u3007O00(final int n, final TypeBearer typeBearer, final LocalItem localItem) {
        return OO0o\u3007\u3007(n, typeBearer, localItem);
    }
    
    private static int \u3007O8o08O(final int n, final TypeBearer typeBearer, final LocalItem localItem) {
        return (0 * 31 + typeBearer.hashCode()) * 31 + n;
    }
    
    public static RegisterSpec \u3007O\u3007(final int n, final TypeBearer typeBearer) {
        return OO0o\u3007\u3007(n, typeBearer, null);
    }
    
    public int OO0o\u3007\u3007\u3007\u30070() {
        return this.o0 + this.oO80();
    }
    
    public int Oo08(final RegisterSpec registerSpec) {
        final int o0 = this.o0;
        final int o2 = registerSpec.o0;
        if (o0 < o2) {
            return -1;
        }
        if (o0 > o2) {
            return 1;
        }
        if (this == registerSpec) {
            return 0;
        }
        final int \u3007o\u3007 = this.\u3007OOo8\u30070.getType().\u3007o\u3007(registerSpec.\u3007OOo8\u30070.getType());
        if (\u3007o\u3007 != 0) {
            return \u3007o\u3007;
        }
        return 0;
    }
    
    public boolean Oooo8o0\u3007() {
        return this.\u3007OOo8\u30070.getType().OO0o\u3007\u3007();
    }
    
    @Override
    public boolean equals(final Object o) {
        if (this == o) {
            return true;
        }
        if (o instanceof RegisterSpec) {
            final RegisterSpec registerSpec = (RegisterSpec)o;
            return this.o\u30070(registerSpec.o0, registerSpec.\u3007OOo8\u30070, null);
        }
        if (o instanceof ForComparison) {
            final ForComparison forComparison = (ForComparison)o;
            final int \u3007080 = forComparison.\u3007080;
            final TypeBearer \u3007o00\u3007\u3007Oo = forComparison.\u3007o00\u3007\u3007Oo;
            ForComparison.\u3007o\u3007(forComparison);
            return this.o\u30070(\u3007080, \u3007o00\u3007\u3007Oo, null);
        }
        return false;
    }
    
    @Override
    public Type getType() {
        return this.\u3007OOo8\u30070.getType();
    }
    
    @Override
    public int hashCode() {
        return \u3007O8o08O(this.o0, this.\u3007OOo8\u30070, null);
    }
    
    public int oO80() {
        return this.\u3007OOo8\u30070.getType().Oo08();
    }
    
    public RegisterSpec oo88o8O(final int n) {
        if (this.o0 == n) {
            return this;
        }
        return \u3007O00(n, this.\u3007OOo8\u30070, null);
    }
    
    @Override
    public String toHuman() {
        return this.o800o8O(true);
    }
    
    @Override
    public String toString() {
        return this.o800o8O(false);
    }
    
    @Override
    public final int \u3007080() {
        return this.\u3007OOo8\u30070.\u3007080();
    }
    
    public String \u30070\u3007O0088o() {
        return OoO8(this.o0);
    }
    
    public LocalItem \u300780\u3007808\u3007O() {
        return null;
    }
    
    public int \u30078o8o\u3007() {
        return this.o0;
    }
    
    public RegisterSpec \u3007O888o0o(final int n) {
        if (n == 0) {
            return this;
        }
        return this.oo88o8O(this.o0 + n);
    }
    
    @Override
    public final int \u3007o00\u3007\u3007Oo() {
        return this.\u3007OOo8\u30070.\u3007o00\u3007\u3007Oo();
    }
    
    public RegisterSpec \u3007oo\u3007(final TypeBearer typeBearer) {
        return \u3007O00(this.o0, typeBearer, null);
    }
    
    public boolean \u3007\u3007808\u3007() {
        final int \u30078o8o\u3007 = this.\u30078o8o\u3007();
        boolean b = true;
        if ((\u30078o8o\u3007 & 0x1) != 0x0) {
            b = false;
        }
        return b;
    }
    
    public boolean \u3007\u3007888(final RegisterSpec registerSpec) {
        final boolean \u3007\u30078O0\u30078 = this.\u3007\u30078O0\u30078(registerSpec);
        boolean b = false;
        if (!\u3007\u30078O0\u30078) {
            return false;
        }
        if (this.o0 == registerSpec.o0) {
            b = true;
        }
        return b;
    }
    
    public boolean \u3007\u30078O0\u30078(final RegisterSpec registerSpec) {
        boolean b = false;
        if (registerSpec == null) {
            return false;
        }
        if (this.\u3007OOo8\u30070.getType().equals(registerSpec.\u3007OOo8\u30070.getType())) {
            b = true;
        }
        return b;
    }
    
    private static class ForComparison
    {
        private int \u3007080;
        private TypeBearer \u3007o00\u3007\u3007Oo;
        
        static /* synthetic */ LocalItem \u3007o\u3007(final ForComparison forComparison) {
            forComparison.getClass();
            return null;
        }
        
        public void O8(final int \u3007080, final TypeBearer \u3007o00\u3007\u3007Oo, final LocalItem localItem) {
            this.\u3007080 = \u3007080;
            this.\u3007o00\u3007\u3007Oo = \u3007o00\u3007\u3007Oo;
        }
        
        public RegisterSpec Oo08() {
            return new RegisterSpec(this.\u3007080, this.\u3007o00\u3007\u3007Oo, null, null);
        }
        
        @Override
        public boolean equals(final Object o) {
            return o instanceof RegisterSpec && ((RegisterSpec)o).o\u30070(this.\u3007080, this.\u3007o00\u3007\u3007Oo, null);
        }
        
        @Override
        public int hashCode() {
            return \u3007O8o08O(this.\u3007080, this.\u3007o00\u3007\u3007Oo, null);
        }
    }
}

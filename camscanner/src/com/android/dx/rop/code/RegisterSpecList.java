// 
// Decompiled by Procyon v0.6.0
// 

package com.android.dx.rop.code;

import java.util.BitSet;
import com.android.dx.rop.type.Type;
import com.android.dx.rop.type.TypeList;
import com.android.dx.util.FixedSizeList;

public final class RegisterSpecList extends FixedSizeList implements TypeList
{
    public static final RegisterSpecList OO;
    
    static {
        OO = new RegisterSpecList(0);
    }
    
    public RegisterSpecList(final int n) {
        super(n);
    }
    
    public static RegisterSpecList OoO8(final RegisterSpec registerSpec, final RegisterSpec registerSpec2) {
        final RegisterSpecList list = new RegisterSpecList(2);
        list.\u3007O888o0o(0, registerSpec);
        list.\u3007O888o0o(1, registerSpec2);
        return list;
    }
    
    public static RegisterSpecList o800o8O(final RegisterSpec registerSpec, final RegisterSpec registerSpec2, final RegisterSpec registerSpec3) {
        final RegisterSpecList list = new RegisterSpecList(3);
        list.\u3007O888o0o(0, registerSpec);
        list.\u3007O888o0o(1, registerSpec2);
        list.\u3007O888o0o(2, registerSpec3);
        return list;
    }
    
    public static RegisterSpecList \u30070\u3007O0088o(final RegisterSpec registerSpec) {
        final RegisterSpecList list = new RegisterSpecList(1);
        list.\u3007O888o0o(0, registerSpec);
        return list;
    }
    
    @Override
    public Type getType(final int n) {
        return this.\u3007O00(n).getType().getType();
    }
    
    public RegisterSpecList oo88o8O(final BitSet set) {
        final int n = this.size() - set.cardinality();
        if (n == 0) {
            return RegisterSpecList.OO;
        }
        final RegisterSpecList list = new RegisterSpecList(n);
        int i = 0;
        int n2 = 0;
        while (i < this.size()) {
            int n3 = n2;
            if (!set.get(i)) {
                list.OO0o\u3007\u3007\u3007\u30070(n2, this.oO80(i));
                n3 = n2 + 1;
            }
            ++i;
            n2 = n3;
        }
        if (this.\u3007o\u3007()) {
            list.Oo08();
        }
        return list;
    }
    
    public RegisterSpecList o\u3007O8\u3007\u3007o(final RegisterSpec registerSpec) {
        final int size = this.size();
        final RegisterSpecList list = new RegisterSpecList(size + 1);
        int n;
        for (int i = 0; i < size; i = n) {
            n = i + 1;
            list.OO0o\u3007\u3007\u3007\u30070(n, this.oO80(i));
        }
        list.OO0o\u3007\u3007\u3007\u30070(0, registerSpec);
        if (this.\u3007o\u3007()) {
            list.Oo08();
        }
        return list;
    }
    
    public RegisterSpecList \u300700(final int n) {
        final int size = this.size();
        if (size == 0) {
            return this;
        }
        final RegisterSpecList list = new RegisterSpecList(size);
        for (int i = 0; i < size; ++i) {
            final RegisterSpec registerSpec = (RegisterSpec)this.oO80(i);
            if (registerSpec != null) {
                list.OO0o\u3007\u3007\u3007\u30070(i, registerSpec.\u3007O888o0o(n));
            }
        }
        if (this.\u3007o\u3007()) {
            list.Oo08();
        }
        return list;
    }
    
    public RegisterSpec \u3007O00(final int n) {
        return (RegisterSpec)this.oO80(n);
    }
    
    public void \u3007O888o0o(final int n, final RegisterSpec registerSpec) {
        this.OO0o\u3007\u3007\u3007\u30070(n, registerSpec);
    }
    
    public RegisterSpecList \u3007oo\u3007(int i, final boolean b, final BitSet set) {
        final int size = this.size();
        if (size == 0) {
            return this;
        }
        final Expander expander = new Expander(this, set, i, b);
        for (i = 0; i < size; ++i) {
            expander.\u3007o\u3007(i);
        }
        return expander.Oo08();
    }
    
    public int \u3007\u30078O0\u30078() {
        final int size = this.size();
        int i = 0;
        int n = 0;
        while (i < size) {
            n += this.getType(i).Oo08();
            ++i;
        }
        return n;
    }
    
    private static class Expander
    {
        private final RegisterSpecList O8;
        private boolean Oo08;
        private final BitSet \u3007080;
        private final RegisterSpecList \u3007o00\u3007\u3007Oo;
        private int \u3007o\u3007;
        
        private Expander(final RegisterSpecList \u3007o00\u3007\u3007Oo, final BitSet \u3007080, final int \u3007o\u3007, final boolean oo08) {
            this.\u3007o00\u3007\u3007Oo = \u3007o00\u3007\u3007Oo;
            this.\u3007080 = \u3007080;
            this.\u3007o\u3007 = \u3007o\u3007;
            this.O8 = new RegisterSpecList(\u3007o00\u3007\u3007Oo.size());
            this.Oo08 = oo08;
        }
        
        private void O8(final int bitIndex, RegisterSpec registerSpec) {
            final BitSet \u3007080 = this.\u3007080;
            boolean b = true;
            if (\u3007080 != null) {
                if (\u3007080.get(bitIndex)) {
                    b = false;
                }
            }
            RegisterSpec oo88o8O = registerSpec;
            if (b) {
                registerSpec = (oo88o8O = registerSpec.oo88o8O(this.\u3007o\u3007));
                if (!this.Oo08) {
                    this.\u3007o\u3007 += registerSpec.oO80();
                    oo88o8O = registerSpec;
                }
            }
            this.Oo08 = false;
            this.O8.OO0o\u3007\u3007\u3007\u30070(bitIndex, oo88o8O);
        }
        
        private RegisterSpecList Oo08() {
            if (this.\u3007o00\u3007\u3007Oo.\u3007o\u3007()) {
                this.O8.Oo08();
            }
            return this.O8;
        }
        
        private void \u3007o\u3007(final int n) {
            this.O8(n, (RegisterSpec)this.\u3007o00\u3007\u3007Oo.oO80(n));
        }
    }
}

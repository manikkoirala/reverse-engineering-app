// 
// Decompiled by Procyon v0.6.0
// 

package com.android.dx.rop.code;

import com.android.dx.util.Hex;
import com.android.dx.rop.type.StdTypeList;
import com.android.dx.rop.type.Type;
import com.android.dx.rop.type.TypeList;

public final class Rop
{
    private final TypeList O8;
    private final int Oo08;
    private final boolean o\u30070;
    private final int \u3007080;
    private final Type \u3007o00\u3007\u3007Oo;
    private final TypeList \u3007o\u3007;
    private final String \u3007\u3007888;
    
    public Rop(final int n, final Type type, final TypeList list, final int n2, final String s) {
        this(n, type, list, StdTypeList.OO, n2, false, s);
    }
    
    public Rop(final int \u3007080, final Type \u3007o00\u3007\u3007Oo, final TypeList \u3007o\u3007, final TypeList o8, final int n, final boolean o\u30070, final String \u3007\u3007888) {
        if (\u3007o00\u3007\u3007Oo == null) {
            throw new NullPointerException("result == null");
        }
        if (\u3007o\u3007 == null) {
            throw new NullPointerException("sources == null");
        }
        if (o8 == null) {
            throw new NullPointerException("exceptions == null");
        }
        if (n < 1 || n > 6) {
            final StringBuilder sb = new StringBuilder();
            sb.append("invalid branchingness: ");
            sb.append(n);
            throw new IllegalArgumentException(sb.toString());
        }
        if (o8.size() != 0 && n != 6) {
            throw new IllegalArgumentException("exceptions / branchingness mismatch");
        }
        this.\u3007080 = \u3007080;
        this.\u3007o00\u3007\u3007Oo = \u3007o00\u3007\u3007Oo;
        this.\u3007o\u3007 = \u3007o\u3007;
        this.O8 = o8;
        this.Oo08 = n;
        this.o\u30070 = o\u30070;
        this.\u3007\u3007888 = \u3007\u3007888;
    }
    
    public Rop(final int n, final Type type, final TypeList list, final TypeList list2, final String s) {
        this(n, type, list, list2, 6, false, s);
    }
    
    public Rop(final int n, final Type type, final TypeList list, final String s) {
        this(n, type, list, StdTypeList.OO, 1, false, s);
    }
    
    public Rop(final int n, final TypeList list, final TypeList list2) {
        this(n, Type.o8oOOo, list, list2, 6, true, null);
    }
    
    public int O8() {
        return this.\u3007080;
    }
    
    public boolean Oo08() {
        return this.o\u30070;
    }
    
    @Override
    public boolean equals(final Object o) {
        boolean b = true;
        if (this == o) {
            return true;
        }
        if (!(o instanceof Rop)) {
            return false;
        }
        final Rop rop = (Rop)o;
        if (this.\u3007080 != rop.\u3007080 || this.Oo08 != rop.Oo08 || this.\u3007o00\u3007\u3007Oo != rop.\u3007o00\u3007\u3007Oo || !this.\u3007o\u3007.equals(rop.\u3007o\u3007) || !this.O8.equals(rop.O8)) {
            b = false;
        }
        return b;
    }
    
    @Override
    public int hashCode() {
        return (((this.\u3007080 * 31 + this.Oo08) * 31 + this.\u3007o00\u3007\u3007Oo.hashCode()) * 31 + this.\u3007o\u3007.hashCode()) * 31 + this.O8.hashCode();
    }
    
    public boolean o\u30070() {
        final int \u3007080 = this.\u3007080;
        if (\u3007080 != 14 && \u3007080 != 16) {
            switch (\u3007080) {
                default: {
                    return false;
                }
                case 20:
                case 21:
                case 22: {
                    break;
                }
            }
        }
        return true;
    }
    
    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder(40);
        sb.append("Rop{");
        sb.append(RegOps.\u3007080(this.\u3007080));
        if (this.\u3007o00\u3007\u3007Oo != Type.o8oOOo) {
            sb.append(" ");
            sb.append(this.\u3007o00\u3007\u3007Oo);
        }
        else {
            sb.append(" .");
        }
        sb.append(" <-");
        final int size = this.\u3007o\u3007.size();
        final int n = 0;
        if (size == 0) {
            sb.append(" .");
        }
        else {
            for (int i = 0; i < size; ++i) {
                sb.append(' ');
                sb.append(this.\u3007o\u3007.getType(i));
            }
        }
        if (this.o\u30070) {
            sb.append(" call");
        }
        final int size2 = this.O8.size();
        if (size2 != 0) {
            sb.append(" throws");
            for (int j = n; j < size2; ++j) {
                sb.append(' ');
                if (this.O8.getType(j) == Type.\u300700O0) {
                    sb.append("<any>");
                }
                else {
                    sb.append(this.O8.getType(j));
                }
            }
        }
        else {
            final int oo08 = this.Oo08;
            if (oo08 != 1) {
                if (oo08 != 2) {
                    if (oo08 != 3) {
                        if (oo08 != 4) {
                            if (oo08 != 5) {
                                final StringBuilder sb2 = new StringBuilder();
                                sb2.append(" ");
                                sb2.append(Hex.O8(this.Oo08));
                                sb.append(sb2.toString());
                            }
                            else {
                                sb.append(" switches");
                            }
                        }
                        else {
                            sb.append(" ifs");
                        }
                    }
                    else {
                        sb.append(" gotos");
                    }
                }
                else {
                    sb.append(" returns");
                }
            }
            else {
                sb.append(" flows");
            }
        }
        sb.append('}');
        return sb.toString();
    }
    
    public final boolean \u3007080() {
        return this.O8.size() != 0;
    }
    
    public int \u3007o00\u3007\u3007Oo() {
        return this.Oo08;
    }
    
    public String \u3007o\u3007() {
        final String \u3007\u3007888 = this.\u3007\u3007888;
        if (\u3007\u3007888 != null) {
            return \u3007\u3007888;
        }
        return this.toString();
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.android.dx.rop.code;

import com.android.dx.util.Hex;
import com.android.dx.util.LabeledItem;
import com.android.dx.util.IntList;
import com.android.dx.util.LabeledList;

public final class BasicBlockList extends LabeledList
{
    private int \u300708O\u300700\u3007o;
    
    public BasicBlockList(final int n) {
        super(n);
        this.\u300708O\u300700\u3007o = -1;
    }
    
    public void OoO8(final Insn.Visitor visitor) {
        for (int size = this.size(), i = 0; i < size; ++i) {
            this.o800o8O(i).\u3007o00\u3007\u3007Oo().\u3007\u3007808\u3007(visitor);
        }
    }
    
    public BasicBlock o800o8O(final int n) {
        return (BasicBlock)this.oO80(n);
    }
    
    public int oo88o8O() {
        if (this.\u300708O\u300700\u3007o == -1) {
            final RegCountVisitor regCountVisitor = new RegCountVisitor();
            this.OoO8(regCountVisitor);
            this.\u300708O\u300700\u3007o = regCountVisitor.Oo08();
        }
        return this.\u300708O\u300700\u3007o;
    }
    
    public BasicBlock o\u3007O8\u3007\u3007o(final BasicBlock basicBlock) {
        final int o8 = basicBlock.O8();
        final IntList o\u30070 = basicBlock.o\u30070();
        final int size = o\u30070.size();
        if (size == 0) {
            return null;
        }
        if (size == 1) {
            return this.\u3007oo\u3007(o\u30070.\u30078o8o\u3007(0));
        }
        if (o8 != -1) {
            return this.\u3007oo\u3007(o8);
        }
        return this.\u3007oo\u3007(o\u30070.\u30078o8o\u3007(0));
    }
    
    public void \u300700(final int n, final BasicBlock basicBlock) {
        super.\u30070\u3007O0088o(n, basicBlock);
        this.\u300708O\u300700\u3007o = -1;
    }
    
    public int \u3007O888o0o() {
        final int size = this.size();
        int i = 0;
        int n = 0;
        while (i < size) {
            final BasicBlock basicBlock = (BasicBlock)this.\u300780\u3007808\u3007O(i);
            int n2 = n;
            if (basicBlock != null) {
                n2 = n + basicBlock.\u3007o00\u3007\u3007Oo().size();
            }
            ++i;
            n = n2;
        }
        return n;
    }
    
    public BasicBlock \u3007oo\u3007(final int n) {
        final int \u3007o00 = this.\u3007O00(n);
        if (\u3007o00 >= 0) {
            return this.o800o8O(\u3007o00);
        }
        final StringBuilder sb = new StringBuilder();
        sb.append("no such label: ");
        sb.append(Hex.Oo08(n));
        throw new IllegalArgumentException(sb.toString());
    }
    
    private static class RegCountVisitor implements Visitor
    {
        private int \u3007080;
        
        public RegCountVisitor() {
            this.\u3007080 = 0;
        }
        
        private void o\u30070(final RegisterSpec registerSpec) {
            final int oo0o\u3007\u3007\u3007\u30070 = registerSpec.OO0o\u3007\u3007\u3007\u30070();
            if (oo0o\u3007\u3007\u3007\u30070 > this.\u3007080) {
                this.\u3007080 = oo0o\u3007\u3007\u3007\u30070;
            }
        }
        
        private void \u3007\u3007888(final Insn insn) {
            final RegisterSpec \u300780\u3007808\u3007O = insn.\u300780\u3007808\u3007O();
            if (\u300780\u3007808\u3007O != null) {
                this.o\u30070(\u300780\u3007808\u3007O);
            }
            final RegisterSpecList oo0o\u3007\u3007\u3007\u30070 = insn.OO0o\u3007\u3007\u3007\u30070();
            for (int size = oo0o\u3007\u3007\u3007\u30070.size(), i = 0; i < size; ++i) {
                this.o\u30070(oo0o\u3007\u3007\u3007\u30070.\u3007O00(i));
            }
        }
        
        @Override
        public void O8(final ThrowingInsn throwingInsn) {
            this.\u3007\u3007888(throwingInsn);
        }
        
        public int Oo08() {
            return this.\u3007080;
        }
        
        @Override
        public void \u3007080(final PlainCstInsn plainCstInsn) {
            this.\u3007\u3007888(plainCstInsn);
        }
        
        @Override
        public void \u3007o00\u3007\u3007Oo(final ThrowingCstInsn throwingCstInsn) {
            this.\u3007\u3007888(throwingCstInsn);
        }
        
        @Override
        public void \u3007o\u3007(final PlainInsn plainInsn) {
            this.\u3007\u3007888(plainInsn);
        }
    }
}

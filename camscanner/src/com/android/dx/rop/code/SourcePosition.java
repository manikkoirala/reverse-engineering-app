// 
// Decompiled by Procyon v0.6.0
// 

package com.android.dx.rop.code;

import com.android.dx.util.Hex;
import com.android.dx.rop.cst.CstString;

public final class SourcePosition
{
    public static final SourcePosition O8;
    private final CstString \u3007080;
    private final int \u3007o00\u3007\u3007Oo;
    private final int \u3007o\u3007;
    
    static {
        O8 = new SourcePosition(null, -1, -1);
    }
    
    public SourcePosition(final CstString \u3007080, final int \u3007o00\u3007\u3007Oo, final int \u3007o\u3007) {
        if (\u3007o00\u3007\u3007Oo < -1) {
            throw new IllegalArgumentException("address < -1");
        }
        if (\u3007o\u3007 >= -1) {
            this.\u3007080 = \u3007080;
            this.\u3007o00\u3007\u3007Oo = \u3007o00\u3007\u3007Oo;
            this.\u3007o\u3007 = \u3007o\u3007;
            return;
        }
        throw new IllegalArgumentException("line < -1");
    }
    
    @Override
    public boolean equals(final Object o) {
        final boolean b = o instanceof SourcePosition;
        final boolean b2 = false;
        if (!b) {
            return false;
        }
        if (this == o) {
            return true;
        }
        final SourcePosition sourcePosition = (SourcePosition)o;
        boolean b3 = b2;
        if (this.\u3007o00\u3007\u3007Oo == sourcePosition.\u3007o00\u3007\u3007Oo) {
            b3 = b2;
            if (this.\u3007o\u3007(sourcePosition)) {
                b3 = true;
            }
        }
        return b3;
    }
    
    @Override
    public int hashCode() {
        return this.\u3007080.hashCode() + this.\u3007o00\u3007\u3007Oo + this.\u3007o\u3007;
    }
    
    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder(50);
        final CstString \u3007080 = this.\u3007080;
        if (\u3007080 != null) {
            sb.append(\u3007080.toHuman());
            sb.append(":");
        }
        final int \u3007o\u3007 = this.\u3007o\u3007;
        if (\u3007o\u3007 >= 0) {
            sb.append(\u3007o\u3007);
        }
        sb.append('@');
        final int \u3007o00\u3007\u3007Oo = this.\u3007o00\u3007\u3007Oo;
        if (\u3007o00\u3007\u3007Oo < 0) {
            sb.append("????");
        }
        else {
            sb.append(Hex.Oo08(\u3007o00\u3007\u3007Oo));
        }
        return sb.toString();
    }
    
    public int \u3007080() {
        return this.\u3007o\u3007;
    }
    
    public boolean \u3007o00\u3007\u3007Oo(final SourcePosition sourcePosition) {
        return this.\u3007o\u3007 == sourcePosition.\u3007o\u3007;
    }
    
    public boolean \u3007o\u3007(final SourcePosition sourcePosition) {
        if (this.\u3007o\u3007 == sourcePosition.\u3007o\u3007) {
            final CstString \u3007080 = this.\u3007080;
            final CstString \u300781 = sourcePosition.\u3007080;
            if (\u3007080 == \u300781 || (\u3007080 != null && \u3007080.equals(\u300781))) {
                return true;
            }
        }
        return false;
    }
}

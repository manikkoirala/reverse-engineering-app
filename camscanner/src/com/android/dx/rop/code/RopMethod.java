// 
// Decompiled by Procyon v0.6.0
// 

package com.android.dx.rop.code;

import com.android.dx.util.Hex;
import com.android.dx.util.IntList;

public final class RopMethod
{
    private IntList O8;
    private final BasicBlockList \u3007080;
    private final int \u3007o00\u3007\u3007Oo;
    private IntList[] \u3007o\u3007;
    
    public RopMethod(final BasicBlockList \u3007080, final int \u3007o00\u3007\u3007Oo) {
        if (\u3007080 == null) {
            throw new NullPointerException("blocks == null");
        }
        if (\u3007o00\u3007\u3007Oo >= 0) {
            this.\u3007080 = \u3007080;
            this.\u3007o00\u3007\u3007Oo = \u3007o00\u3007\u3007Oo;
            this.\u3007o\u3007 = null;
            this.O8 = null;
            return;
        }
        throw new IllegalArgumentException("firstLabel < 0");
    }
    
    private void \u3007080() {
        final int \u3007o\u3007 = this.\u3007080.\u3007O\u3007();
        final IntList[] \u3007o\u30072 = new IntList[\u3007o\u3007];
        final IntList o8 = new IntList(10);
        final int size = this.\u3007080.size();
        final int n = 0;
        int n2 = 0;
        int i;
        while (true) {
            i = n;
            if (n2 >= size) {
                break;
            }
            final BasicBlock o800o8O = this.\u3007080.o800o8O(n2);
            final int label = o800o8O.getLabel();
            final IntList o\u30070 = o800o8O.o\u30070();
            final int size2 = o\u30070.size();
            if (size2 == 0) {
                o8.oO80(label);
            }
            else {
                for (int j = 0; j < size2; ++j) {
                    final int \u30078o8o\u3007 = o\u30070.\u30078o8o\u3007(j);
                    IntList list;
                    if ((list = \u3007o\u30072[\u30078o8o\u3007]) == null) {
                        list = new IntList(10);
                        \u3007o\u30072[\u30078o8o\u3007] = list;
                    }
                    list.oO80(label);
                }
            }
            ++n2;
        }
        while (i < \u3007o\u3007) {
            final IntList list2 = \u3007o\u30072[i];
            if (list2 != null) {
                list2.\u3007O\u3007();
                list2.Oo08();
            }
            ++i;
        }
        o8.\u3007O\u3007();
        o8.Oo08();
        final int \u3007o00\u3007\u3007Oo = this.\u3007o00\u3007\u3007Oo;
        if (\u3007o\u30072[\u3007o00\u3007\u3007Oo] == null) {
            \u3007o\u30072[\u3007o00\u3007\u3007Oo] = IntList.o\u300700O;
        }
        this.\u3007o\u3007 = \u3007o\u30072;
        this.O8 = o8;
    }
    
    public IntList O8(final int n) {
        if (this.O8 == null) {
            this.\u3007080();
        }
        final IntList list = this.\u3007o\u3007[n];
        if (list != null) {
            return list;
        }
        final StringBuilder sb = new StringBuilder();
        sb.append("no such block: ");
        sb.append(Hex.Oo08(n));
        throw new RuntimeException(sb.toString());
    }
    
    public BasicBlockList \u3007o00\u3007\u3007Oo() {
        return this.\u3007080;
    }
    
    public int \u3007o\u3007() {
        return this.\u3007o00\u3007\u3007Oo;
    }
}

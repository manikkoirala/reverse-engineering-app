// 
// Decompiled by Procyon v0.6.0
// 

package com.android.dx.rop.code;

import com.android.dx.rop.cst.Constant;

public abstract class CstInsn extends Insn
{
    private final Constant o\u300700O;
    
    public CstInsn(final Rop rop, final SourcePosition sourcePosition, final RegisterSpec registerSpec, final RegisterSpecList list, final Constant o\u300700O) {
        super(rop, sourcePosition, registerSpec, list);
        if (o\u300700O != null) {
            this.o\u300700O = o\u300700O;
            return;
        }
        throw new NullPointerException("cst == null");
    }
    
    public Constant OO0o\u3007\u3007() {
        return this.o\u300700O;
    }
    
    @Override
    public String o\u30070() {
        return this.o\u300700O.toHuman();
    }
}

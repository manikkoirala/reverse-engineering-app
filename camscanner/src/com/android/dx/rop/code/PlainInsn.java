// 
// Decompiled by Procyon v0.6.0
// 

package com.android.dx.rop.code;

import com.android.dx.rop.type.StdTypeList;
import com.android.dx.rop.type.TypeList;

public final class PlainInsn extends Insn
{
    public PlainInsn(final Rop rop, final SourcePosition sourcePosition, final RegisterSpec registerSpec, final RegisterSpec registerSpec2) {
        this(rop, sourcePosition, registerSpec, RegisterSpecList.\u30070\u3007O0088o(registerSpec2));
    }
    
    public PlainInsn(final Rop rop, final SourcePosition sourcePosition, final RegisterSpec registerSpec, final RegisterSpecList list) {
        super(rop, sourcePosition, registerSpec, list);
        final int \u3007o00\u3007\u3007Oo = rop.\u3007o00\u3007\u3007Oo();
        if (\u3007o00\u3007\u3007Oo == 5 || \u3007o00\u3007\u3007Oo == 6) {
            final StringBuilder sb = new StringBuilder();
            sb.append("opcode with invalid branchingness: ");
            sb.append(rop.\u3007o00\u3007\u3007Oo());
            throw new IllegalArgumentException(sb.toString());
        }
        if (registerSpec != null && rop.\u3007o00\u3007\u3007Oo() != 1) {
            throw new IllegalArgumentException("can't mix branchingness with result");
        }
    }
    
    @Override
    public TypeList Oo08() {
        return StdTypeList.OO;
    }
    
    @Override
    public void \u3007o\u3007(final Visitor visitor) {
        visitor.\u3007o\u3007(this);
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.android.dx.rop.code;

import com.android.dx.rop.cst.CstString;
import com.android.dx.rop.cst.Constant;
import com.android.dx.rop.type.TypeList;

public final class ThrowingCstInsn extends CstInsn
{
    private final TypeList O8o08O8O;
    
    public ThrowingCstInsn(final Rop rop, final SourcePosition sourcePosition, final RegisterSpecList list, final TypeList o8o08O8O, final Constant constant) {
        super(rop, sourcePosition, null, list, constant);
        if (rop.\u3007o00\u3007\u3007Oo() != 6) {
            final StringBuilder sb = new StringBuilder();
            sb.append("opcode with invalid branchingness: ");
            sb.append(rop.\u3007o00\u3007\u3007Oo());
            throw new IllegalArgumentException(sb.toString());
        }
        if (o8o08O8O != null) {
            this.O8o08O8O = o8o08O8O;
            return;
        }
        throw new NullPointerException("catches == null");
    }
    
    @Override
    public TypeList Oo08() {
        return this.O8o08O8O;
    }
    
    @Override
    public String o\u30070() {
        final Constant oo0o\u3007\u3007 = this.OO0o\u3007\u3007();
        String str = oo0o\u3007\u3007.toHuman();
        if (oo0o\u3007\u3007 instanceof CstString) {
            str = ((CstString)oo0o\u3007\u3007).OO0o\u3007\u3007\u3007\u30070();
        }
        final StringBuilder sb = new StringBuilder();
        sb.append(str);
        sb.append(" ");
        sb.append(ThrowingInsn.OO0o\u3007\u3007(this.O8o08O8O));
        return sb.toString();
    }
    
    @Override
    public void \u3007o\u3007(final Visitor visitor) {
        visitor.\u3007o00\u3007\u3007Oo(this);
    }
}

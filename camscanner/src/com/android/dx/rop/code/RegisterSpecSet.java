// 
// Decompiled by Procyon v0.6.0
// 

package com.android.dx.rop.code;

import com.android.dx.util.MutabilityControl;

public final class RegisterSpecSet extends MutabilityControl
{
    public static final RegisterSpecSet \u300708O\u300700\u3007o;
    private int OO;
    private final RegisterSpec[] \u3007OOo8\u30070;
    
    static {
        \u300708O\u300700\u3007o = new RegisterSpecSet(0);
    }
    
    public RegisterSpecSet(final int n) {
        super(n != 0);
        this.\u3007OOo8\u30070 = new RegisterSpec[n];
        this.OO = 0;
    }
    
    public void OO0o\u3007\u3007(final RegisterSpec registerSpec) {
        try {
            this.\u3007OOo8\u30070[registerSpec.\u30078o8o\u3007()] = null;
            this.OO = -1;
        }
        catch (final ArrayIndexOutOfBoundsException ex) {
            throw new IllegalArgumentException("bogus reg");
        }
    }
    
    public int OO0o\u3007\u3007\u3007\u30070() {
        return this.\u3007OOo8\u30070.length;
    }
    
    public RegisterSpecSet Oooo8o0\u3007(final int n) {
        final int length = this.\u3007OOo8\u30070.length;
        final RegisterSpecSet set = new RegisterSpecSet(length + n);
        for (int i = 0; i < length; ++i) {
            final RegisterSpec registerSpec = this.\u3007OOo8\u30070[i];
            if (registerSpec != null) {
                set.\u30078o8o\u3007(registerSpec.\u3007O888o0o(n));
            }
        }
        set.OO = this.OO;
        if (this.\u3007o\u3007()) {
            set.Oo08();
        }
        return set;
    }
    
    @Override
    public boolean equals(final Object o) {
        if (!(o instanceof RegisterSpecSet)) {
            return false;
        }
        final RegisterSpecSet set = (RegisterSpecSet)o;
        final RegisterSpec[] \u3007oOo8\u30070 = set.\u3007OOo8\u30070;
        final int length = this.\u3007OOo8\u30070.length;
        if (length == \u3007oOo8\u30070.length && this.size() == set.size()) {
            for (int i = 0; i < length; ++i) {
                final RegisterSpec registerSpec = this.\u3007OOo8\u30070[i];
                final RegisterSpec registerSpec2 = \u3007oOo8\u30070[i];
                if (registerSpec != registerSpec2) {
                    if (registerSpec == null || !registerSpec.equals(registerSpec2)) {
                        return false;
                    }
                }
            }
            return true;
        }
        return false;
    }
    
    @Override
    public int hashCode() {
        final int length = this.\u3007OOo8\u30070.length;
        int i = 0;
        int n = 0;
        while (i < length) {
            final RegisterSpec registerSpec = this.\u3007OOo8\u30070[i];
            int hashCode;
            if (registerSpec == null) {
                hashCode = 0;
            }
            else {
                hashCode = registerSpec.hashCode();
            }
            n = n * 31 + hashCode;
            ++i;
        }
        return n;
    }
    
    public RegisterSpec oO80(final RegisterSpec registerSpec) {
        for (int length = this.\u3007OOo8\u30070.length, i = 0; i < length; ++i) {
            final RegisterSpec registerSpec2 = this.\u3007OOo8\u30070[i];
            if (registerSpec2 != null) {
                if (registerSpec.\u3007\u30078O0\u30078(registerSpec2)) {
                    return registerSpec2;
                }
            }
        }
        return null;
    }
    
    public int size() {
        int oo;
        if ((oo = this.OO) < 0) {
            final int length = this.\u3007OOo8\u30070.length;
            oo = 0;
            int n;
            for (int i = 0; i < length; ++i, oo = n) {
                n = oo;
                if (this.\u3007OOo8\u30070[i] != null) {
                    n = oo + 1;
                }
            }
            this.OO = oo;
        }
        return oo;
    }
    
    @Override
    public String toString() {
        final int length = this.\u3007OOo8\u30070.length;
        final StringBuilder sb = new StringBuilder(length * 25);
        sb.append('{');
        int i = 0;
        int n = 0;
        while (i < length) {
            final RegisterSpec obj = this.\u3007OOo8\u30070[i];
            int n2 = n;
            if (obj != null) {
                if (n != 0) {
                    sb.append(", ");
                }
                else {
                    n = 1;
                }
                sb.append(obj);
                n2 = n;
            }
            ++i;
            n = n2;
        }
        sb.append('}');
        return sb.toString();
    }
    
    public RegisterSpec \u300780\u3007808\u3007O(final int n) {
        try {
            return this.\u3007OOo8\u30070[n];
        }
        catch (final ArrayIndexOutOfBoundsException ex) {
            throw new IllegalArgumentException("bogus reg");
        }
    }
    
    public void \u30078o8o\u3007(final RegisterSpec registerSpec) {
        this.o\u30070();
        if (registerSpec != null) {
            this.OO = -1;
            try {
                final int \u30078o8o\u3007 = registerSpec.\u30078o8o\u3007();
                final RegisterSpec[] \u3007oOo8\u30070 = this.\u3007OOo8\u30070;
                \u3007oOo8\u30070[\u30078o8o\u3007] = registerSpec;
                if (\u30078o8o\u3007 > 0) {
                    final int n = \u30078o8o\u3007 - 1;
                    final RegisterSpec registerSpec2 = \u3007oOo8\u30070[n];
                    if (registerSpec2 != null && registerSpec2.oO80() == 2) {
                        this.\u3007OOo8\u30070[n] = null;
                    }
                }
                if (registerSpec.oO80() == 2) {
                    this.\u3007OOo8\u30070[\u30078o8o\u3007 + 1] = null;
                }
                return;
            }
            catch (final ArrayIndexOutOfBoundsException ex) {
                throw new IllegalArgumentException("spec.getReg() out of range");
            }
        }
        throw new NullPointerException("spec == null");
    }
    
    public void \u3007O8o08O(final RegisterSpecSet set) {
        for (int oo0o\u3007\u3007\u3007\u30070 = set.OO0o\u3007\u3007\u3007\u30070(), i = 0; i < oo0o\u3007\u3007\u3007\u30070; ++i) {
            final RegisterSpec \u300780\u3007808\u3007O = set.\u300780\u3007808\u3007O(i);
            if (\u300780\u3007808\u3007O != null) {
                this.\u30078o8o\u3007(\u300780\u3007808\u3007O);
            }
        }
    }
}

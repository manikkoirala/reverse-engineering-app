// 
// Decompiled by Procyon v0.6.0
// 

package com.android.dx.rop.code;

import com.android.dx.rop.type.TypeList;

public final class ThrowingInsn extends Insn
{
    private final TypeList o\u300700O;
    
    public ThrowingInsn(final Rop rop, final SourcePosition sourcePosition, final RegisterSpecList list, final TypeList o\u300700O) {
        super(rop, sourcePosition, null, list);
        if (rop.\u3007o00\u3007\u3007Oo() != 6) {
            final StringBuilder sb = new StringBuilder();
            sb.append("opcode with invalid branchingness: ");
            sb.append(rop.\u3007o00\u3007\u3007Oo());
            throw new IllegalArgumentException(sb.toString());
        }
        if (o\u300700O != null) {
            this.o\u300700O = o\u300700O;
            return;
        }
        throw new NullPointerException("catches == null");
    }
    
    public static String OO0o\u3007\u3007(final TypeList list) {
        final StringBuilder sb = new StringBuilder(100);
        sb.append("catch");
        for (int size = list.size(), i = 0; i < size; ++i) {
            sb.append(" ");
            sb.append(list.getType(i).toHuman());
        }
        return sb.toString();
    }
    
    @Override
    public TypeList Oo08() {
        return this.o\u300700O;
    }
    
    @Override
    public String o\u30070() {
        return OO0o\u3007\u3007(this.o\u300700O);
    }
    
    @Override
    public void \u3007o\u3007(final Visitor visitor) {
        visitor.O8(this);
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.android.dx.rop.code;

import com.android.dx.rop.type.Prototype;
import com.android.dx.rop.type.TypeBearer;
import com.android.dx.rop.type.TypeList;
import com.android.dx.rop.type.StdTypeList;
import com.android.dx.rop.type.Type;

public final class Rops
{
    public static final Rop O0;
    public static final Rop O00;
    public static final Rop O000;
    public static final Rop O00O;
    public static final Rop O08000;
    public static final Rop O08O0\u3007O;
    public static final Rop O0O8OO088;
    public static final Rop O0OO8\u30070;
    public static final Rop O0O\u3007OOo;
    public static final Rop O0o;
    public static final Rop O0o8\u3007O;
    public static final Rop O0oO008;
    public static final Rop O0oo0o0\u3007;
    public static final Rop O0o\u3007;
    public static final Rop O0o\u3007O0\u3007;
    public static final Rop O0o\u3007\u3007Oo;
    public static final Rop O0\u3007OO8;
    public static final Rop O0\u3007oO\u3007o;
    public static final Rop O0\u3007oo;
    public static final Rop O8;
    public static final Rop O80\u3007O\u3007080;
    public static final Rop O880oOO08;
    public static final Rop O8888;
    public static final Rop O88o\u3007;
    public static final Rop O88\u3007\u3007o0O;
    public static final Rop O8OO08o;
    public static final Rop O8O\u3007;
    public static final Rop O8O\u300788oO0;
    public static final Rop O8O\u30078oo08;
    public static final Rop O8oOo80;
    public static final Rop O8ooOoo\u3007;
    public static final Rop O8\u3007o;
    public static final Rop OO0o\u3007\u3007;
    public static final Rop OO0o\u3007\u3007\u3007\u30070;
    public static final Rop OO0\u3007\u30078;
    public static final Rop OO88o;
    public static final Rop OO88\u3007OOO;
    public static final Rop OO8oO0o\u3007;
    public static final Rop OO8\u3007;
    public static final Rop OOO;
    public static final Rop OOO8o\u3007\u3007;
    public static final Rop OOO\u3007O0;
    public static final Rop OOo;
    public static final Rop OOo0O;
    public static final Rop OOo88OOo;
    public static final Rop OOo8o\u3007O;
    public static final Rop OOoo;
    public static final Rop OO\u3007;
    public static final Rop OO\u30070008O8;
    public static final Rop Oo;
    public static final Rop Oo08;
    public static final Rop Oo08OO8oO;
    public static final Rop Oo0O080;
    public static final Rop Oo0oOo\u30070;
    public static final Rop Oo0oO\u3007O\u3007O;
    public static final Rop Oo8Oo00oo;
    public static final Rop OoO8;
    public static final Rop OoOOo8;
    public static final Rop OoO\u3007;
    public static final Rop Ooo;
    public static final Rop Ooo8;
    public static final Rop Ooo8\u3007\u3007;
    public static final Rop Oooo8o0\u3007;
    public static final Rop Oo\u3007;
    public static final Rop Oo\u3007O;
    public static final Rop Oo\u3007O8o\u30078;
    public static final Rop Oo\u3007o;
    public static final Rop O\u3007;
    public static final Rop O\u30070;
    public static final Rop O\u300708;
    public static final Rop O\u30070\u3007o808\u3007;
    public static final Rop O\u30078O8\u3007008;
    public static final Rop O\u30078oOo8O;
    public static final Rop O\u3007OO;
    public static final Rop O\u3007Oo;
    public static final Rop O\u3007Oooo\u3007\u3007;
    public static final Rop O\u3007O\u3007oO;
    public static final Rop O\u3007oO\u3007oo8o;
    public static final Rop O\u3007\u3007;
    public static final Rop o0;
    public static final Rop o08O;
    public static final Rop o08oOO;
    public static final Rop o08\u3007\u30070O;
    public static final Rop o0O0;
    public static final Rop o0O\u30078o0O;
    public static final Rop o0oO;
    public static final Rop o0ooO;
    public static final Rop o8;
    public static final Rop o800o8O;
    public static final Rop o80ooO;
    public static final Rop o88O8;
    public static final Rop o88O\u30078;
    public static final Rop o88o0O;
    public static final Rop o88\u3007OO08\u3007;
    public static final Rop o8O0;
    public static final Rop o8O\u3007;
    public static final Rop o8oO\u3007;
    public static final Rop o8o\u3007\u30070O;
    public static final Rop o8\u3007;
    public static final Rop oO;
    public static final Rop oO0;
    public static final Rop oO00OOO;
    public static final Rop oO0\u3007\u3007O8o;
    public static final Rop oO0\u3007\u3007o8\u3007;
    public static final Rop oO80;
    public static final Rop oO8008O;
    public static final Rop oO80OOO\u3007;
    public static final Rop oO8o;
    public static final Rop oOo;
    public static final Rop oO\u3007;
    public static final Rop oo;
    public static final Rop oo08OO\u30070;
    public static final Rop oo0O\u30070\u3007\u3007\u3007;
    public static final Rop oo88o8O;
    public static final Rop ooOO;
    public static final Rop ooO\u300700O;
    public static final Rop ooo0\u3007O88O;
    public static final Rop ooo8o\u3007o\u3007;
    public static final Rop ooo\u30078oO;
    public static final Rop ooo\u3007\u3007O\u3007;
    public static final Rop oo\u3007;
    public static final Rop o\u3007;
    public static final Rop o\u30070;
    public static final Rop o\u300700O0O\u3007o;
    public static final Rop o\u30070OOo\u30070;
    public static final Rop o\u30070o\u3007\u3007;
    public static final Rop o\u30070\u3007;
    public static final Rop o\u30078;
    public static final Rop o\u30078oOO88;
    public static final Rop o\u30078\u3007;
    public static final Rop o\u3007O;
    public static final Rop o\u3007O8\u3007\u3007o;
    public static final Rop o\u3007OOo000;
    public static final Rop o\u3007o;
    public static final Rop o\u3007\u30070\u3007;
    public static final Rop o\u3007\u30070\u300788;
    public static final Rop \u30070;
    public static final Rop \u300700;
    public static final Rop \u30070000OOO;
    public static final Rop \u3007000O0;
    public static final Rop \u3007000\u3007\u300708;
    public static final Rop \u3007008\u3007o0\u3007\u3007;
    public static final Rop \u3007008\u3007oo;
    public static final Rop \u300700O0O0;
    public static final Rop \u300700\u30078;
    public static final Rop \u3007080;
    public static final Rop \u3007080O0;
    public static final Rop \u300708O8o8;
    public static final Rop \u300708O8o\u30070;
    public static final Rop \u300708\u30070\u3007o\u30078;
    public static final Rop \u30070O00oO;
    public static final Rop \u30070OO8;
    public static final Rop \u30070O\u3007Oo;
    public static final Rop \u30070\u3007O0088o;
    public static final Rop \u30078;
    public static final Rop \u300780;
    public static final Rop \u300780\u3007808\u3007O;
    public static final Rop \u30078O0O808\u3007;
    public static final Rop \u30078o;
    public static final Rop \u30078o8O\u3007O;
    public static final Rop \u30078o8o\u3007;
    public static final Rop \u30078o\u3007\u30078080;
    public static final Rop \u30078\u30070\u3007o\u3007O;
    public static final Rop \u30078\u3007o\u30078;
    public static final Rop \u3007O;
    public static final Rop \u3007O00;
    public static final Rop \u3007O80\u3007oOo;
    public static final Rop \u3007O888o0o;
    public static final Rop \u3007O8o08O;
    public static final Rop \u3007O8\u3007OO\u3007;
    public static final Rop \u3007OO0;
    public static final Rop \u3007OO8Oo0\u3007;
    public static final Rop \u3007Oo;
    public static final Rop \u3007Oo\u3007o8;
    public static final Rop \u3007O\u3007;
    public static final Rop \u3007O\u300780o08O;
    public static final Rop \u3007o;
    public static final Rop \u3007o00\u3007\u3007Oo;
    public static final Rop \u3007o0O0O8;
    public static final Rop \u3007o8OO0;
    public static final Rop \u3007o8oO;
    public static final Rop \u3007oO8O0\u3007\u3007O;
    public static final Rop \u3007oOO8O8;
    public static final Rop \u3007oOo\u3007;
    public static final Rop \u3007oo;
    public static final Rop \u3007oo\u3007;
    public static final Rop \u3007o\u3007;
    public static final Rop \u3007o\u30078;
    public static final Rop \u3007o\u3007Oo0;
    public static final Rop \u3007o\u3007o;
    public static final Rop \u3007\u300700OO;
    public static final Rop \u3007\u300700O\u30070o;
    public static final Rop \u3007\u30070O8ooO;
    public static final Rop \u3007\u30070o;
    public static final Rop \u3007\u30070o8O\u3007\u3007;
    public static final Rop \u3007\u30070o\u3007o8;
    public static final Rop \u3007\u30070\u30070o8;
    public static final Rop \u3007\u30078;
    public static final Rop \u3007\u3007808\u3007;
    public static final Rop \u3007\u3007888;
    public static final Rop \u3007\u30078O0\u30078;
    public static final Rop \u3007\u3007O00\u30078;
    public static final Rop \u3007\u3007o0o;
    public static final Rop \u3007\u3007o8;
    public static final Rop \u3007\u3007\u3007;
    public static final Rop \u3007\u3007\u30070880;
    public static final Rop \u3007\u3007\u30070\u3007\u30070;
    
    static {
        final Type o8oOOo = Type.o8oOOo;
        final StdTypeList oo2 = StdTypeList.OO;
        \u3007080 = new Rop(1, o8oOOo, oo2, "nop");
        final Type ooo0\u3007\u3007O = Type.ooo0\u3007\u3007O;
        final StdTypeList \u300708O\u300700\u3007o = StdTypeList.\u300708O\u300700\u3007o;
        \u3007o00\u3007\u3007Oo = new Rop(2, ooo0\u3007\u3007O, \u300708O\u300700\u3007o, "move-int");
        final Type \u3007\u300708O = Type.\u3007\u300708O;
        final StdTypeList o\u300700O = StdTypeList.o\u300700O;
        \u3007o\u3007 = new Rop(2, \u3007\u300708O, o\u300700O, "move-long");
        final Type \u30078\u3007oO\u3007\u30078o = Type.\u30078\u3007oO\u3007\u30078o;
        final StdTypeList o8o08O8O = StdTypeList.O8o08O8O;
        O8 = new Rop(2, \u30078\u3007oO\u3007\u30078o, o8o08O8O, "move-float");
        final Type o8\u3007OO0\u30070o = Type.o8\u3007OO0\u30070o;
        final StdTypeList \u3007080OO8\u30070 = StdTypeList.\u3007080OO8\u30070;
        Oo08 = new Rop(2, o8\u3007OO0\u30070o, \u3007080OO8\u30070, "move-double");
        final Type \u3007\u3007o\u3007 = Type.\u3007\u3007o\u3007;
        final StdTypeList \u30070O = StdTypeList.\u30070O;
        o\u30070 = new Rop(2, \u3007\u3007o\u3007, \u30070O, "move-object");
        \u3007\u3007888 = new Rop(2, Type.\u3007o0O, StdTypeList.oOo\u30078o008, "move-return-address");
        oO80 = new Rop(3, ooo0\u3007\u3007O, oo2, "move-param-int");
        \u300780\u3007808\u3007O = new Rop(3, \u3007\u300708O, oo2, "move-param-long");
        OO0o\u3007\u3007\u3007\u30070 = new Rop(3, \u30078\u3007oO\u3007\u30078o, oo2, "move-param-float");
        \u30078o8o\u3007 = new Rop(3, o8\u3007OO0\u30070o, oo2, "move-param-double");
        \u3007O8o08O = new Rop(3, \u3007\u3007o\u3007, oo2, "move-param-object");
        OO0o\u3007\u3007 = new Rop(5, ooo0\u3007\u3007O, oo2, "const-int");
        Oooo8o0\u3007 = new Rop(5, \u3007\u300708O, oo2, "const-long");
        \u3007\u3007808\u3007 = new Rop(5, \u30078\u3007oO\u3007\u30078o, oo2, "const-float");
        \u3007O\u3007 = new Rop(5, o8\u3007OO0\u30070o, oo2, "const-double");
        final StdTypeList \u300780\u3007808\u3007O2 = Exceptions.\u300780\u3007808\u3007O;
        \u3007O00 = new Rop(5, \u3007\u3007o\u3007, oo2, \u300780\u3007808\u3007O2, "const-object");
        \u3007\u30078O0\u30078 = new Rop(5, \u3007\u3007o\u3007, oo2, "const-object-nothrow");
        \u30070\u3007O0088o = new Rop(6, o8oOOo, oo2, 3, "goto");
        OoO8 = new Rop(7, o8oOOo, \u300708O\u300700\u3007o, 4, "if-eqz-int");
        o800o8O = new Rop(8, o8oOOo, \u300708O\u300700\u3007o, 4, "if-nez-int");
        \u3007O888o0o = new Rop(9, o8oOOo, \u300708O\u300700\u3007o, 4, "if-ltz-int");
        oo88o8O = new Rop(10, o8oOOo, \u300708O\u300700\u3007o, 4, "if-gez-int");
        \u3007oo\u3007 = new Rop(11, o8oOOo, \u300708O\u300700\u3007o, 4, "if-lez-int");
        o\u3007O8\u3007\u3007o = new Rop(12, o8oOOo, \u300708O\u300700\u3007o, 4, "if-gtz-int");
        \u300700 = new Rop(7, o8oOOo, \u30070O, 4, "if-eqz-object");
        O\u30078O8\u3007008 = new Rop(8, o8oOOo, \u30070O, 4, "if-nez-object");
        final StdTypeList oo\u300700\u30078oO = StdTypeList.OO\u300700\u30078oO;
        O8ooOoo\u3007 = new Rop(7, o8oOOo, oo\u300700\u30078oO, 4, "if-eq-int");
        \u3007oOO8O8 = new Rop(8, o8oOOo, oo\u300700\u30078oO, 4, "if-ne-int");
        \u30070000OOO = new Rop(9, o8oOOo, oo\u300700\u30078oO, 4, "if-lt-int");
        o\u3007\u30070\u3007 = new Rop(10, o8oOOo, oo\u300700\u30078oO, 4, "if-ge-int");
        OOO\u3007O0 = new Rop(11, o8oOOo, oo\u300700\u30078oO, 4, "if-le-int");
        oo\u3007 = new Rop(12, o8oOOo, oo\u300700\u30078oO, 4, "if-gt-int");
        final StdTypeList \u3007\u300708O2 = StdTypeList.\u3007\u300708O;
        O8\u3007o = new Rop(7, o8oOOo, \u3007\u300708O2, 4, "if-eq-object");
        \u300700\u30078 = new Rop(8, o8oOOo, \u3007\u300708O2, 4, "if-ne-object");
        \u3007o = new Rop(13, o8oOOo, \u300708O\u300700\u3007o, 5, "switch");
        o0ooO = new Rop(14, ooo0\u3007\u3007O, oo\u300700\u30078oO, "add-int");
        final StdTypeList o8\u3007OO0\u30070o2 = StdTypeList.o8\u3007OO0\u30070o;
        o\u30078 = new Rop(14, \u3007\u300708O, o8\u3007OO0\u30070o2, "add-long");
        final StdTypeList \u30078\u3007oO\u3007\u30078o2 = StdTypeList.\u30078\u3007oO\u3007\u30078o;
        o8 = new Rop(14, \u30078\u3007oO\u3007\u30078o, \u30078\u3007oO\u3007\u30078o2, "add-float");
        final StdTypeList ooo0\u3007\u3007O2 = StdTypeList.ooo0\u3007\u3007O;
        Oo8Oo00oo = new Rop(14, o8\u3007OO0\u30070o, ooo0\u3007\u3007O2, 1, "add-double");
        \u3007\u3007\u30070\u3007\u30070 = new Rop(15, ooo0\u3007\u3007O, oo\u300700\u30078oO, "sub-int");
        o\u30070OOo\u30070 = new Rop(15, \u3007\u300708O, o8\u3007OO0\u30070o2, "sub-long");
        \u3007\u30070o = new Rop(15, \u30078\u3007oO\u3007\u30078o, \u30078\u3007oO\u3007\u30078o2, "sub-float");
        \u300708O8o\u30070 = new Rop(15, o8\u3007OO0\u30070o, ooo0\u3007\u3007O2, 1, "sub-double");
        oO = new Rop(16, ooo0\u3007\u3007O, oo\u300700\u30078oO, "mul-int");
        \u30078 = new Rop(16, \u3007\u300708O, o8\u3007OO0\u30070o2, "mul-long");
        O08000 = new Rop(16, \u30078\u3007oO\u3007\u30078o, \u30078\u3007oO\u3007\u30078o2, "mul-float");
        \u30078\u30070\u3007o\u3007O = new Rop(16, o8\u3007OO0\u30070o, ooo0\u3007\u3007O2, 1, "mul-double");
        final StdTypeList oo0o\u3007\u3007\u3007\u30070 = Exceptions.OO0o\u3007\u3007\u3007\u30070;
        O\u3007O\u3007oO = new Rop(17, ooo0\u3007\u3007O, oo\u300700\u30078oO, oo0o\u3007\u3007\u3007\u30070, "div-int");
        o8oO\u3007 = new Rop(17, \u3007\u300708O, o8\u3007OO0\u30070o2, oo0o\u3007\u3007\u3007\u30070, "div-long");
        o\u30078oOO88 = new Rop(17, \u30078\u3007oO\u3007\u30078o, \u30078\u3007oO\u3007\u30078o2, "div-float");
        o\u3007O = new Rop(17, o8\u3007OO0\u30070o, ooo0\u3007\u3007O2, "div-double");
        oO00OOO = new Rop(18, ooo0\u3007\u3007O, oo\u300700\u30078oO, oo0o\u3007\u3007\u3007\u30070, "rem-int");
        O000 = new Rop(18, \u3007\u300708O, o8\u3007OO0\u30070o2, oo0o\u3007\u3007\u3007\u30070, "rem-long");
        \u300780 = new Rop(18, \u30078\u3007oO\u3007\u30078o, \u30078\u3007oO\u3007\u30078o2, "rem-float");
        Ooo = new Rop(18, o8\u3007OO0\u30070o, ooo0\u3007\u3007O2, "rem-double");
        \u3007O\u300780o08O = new Rop(19, ooo0\u3007\u3007O, \u300708O\u300700\u3007o, "neg-int");
        OOO = new Rop(19, \u3007\u300708O, o\u300700O, "neg-long");
        ooo\u30078oO = new Rop(19, \u30078\u3007oO\u3007\u30078o, o8o08O8O, "neg-float");
        O0o\u3007\u3007Oo = new Rop(19, o8\u3007OO0\u30070o, \u3007080OO8\u30070, "neg-double");
        OO8oO0o\u3007 = new Rop(20, ooo0\u3007\u3007O, oo\u300700\u30078oO, "and-int");
        o0O0 = new Rop(20, \u3007\u300708O, o8\u3007OO0\u30070o2, "and-long");
        \u30070 = new Rop(21, ooo0\u3007\u3007O, oo\u300700\u30078oO, "or-int");
        o88\u3007OO08\u3007 = new Rop(21, \u3007\u300708O, o8\u3007OO0\u30070o2, "or-long");
        O8O\u3007 = new Rop(22, ooo0\u3007\u3007O, oo\u300700\u30078oO, "xor-int");
        O0O8OO088 = new Rop(22, \u3007\u300708O, o8\u3007OO0\u30070o2, "xor-long");
        \u3007o0O0O8 = new Rop(23, ooo0\u3007\u3007O, oo\u300700\u30078oO, "shl-int");
        final StdTypeList o88O9 = StdTypeList.O88O;
        \u3007\u3007o8 = new Rop(23, \u3007\u300708O, o88O9, "shl-long");
        Oo\u3007O = new Rop(24, ooo0\u3007\u3007O, oo\u300700\u30078oO, "shr-int");
        o8O\u3007 = new Rop(24, \u3007\u300708O, o88O9, "shr-long");
        O0 = new Rop(25, ooo0\u3007\u3007O, oo\u300700\u30078oO, "ushr-int");
        ooOO = new Rop(25, \u3007\u300708O, o88O9, "ushr-long");
        \u3007O = new Rop(26, ooo0\u3007\u3007O, \u300708O\u300700\u3007o, "not-int");
        OOO8o\u3007\u3007 = new Rop(26, \u3007\u300708O, o\u300700O, "not-long");
        \u30070O\u3007Oo = new Rop(14, ooo0\u3007\u3007O, \u300708O\u300700\u3007o, "add-const-int");
        \u300700O0O0 = new Rop(14, \u3007\u300708O, o\u300700O, "add-const-long");
        Ooo8\u3007\u3007 = new Rop(14, \u30078\u3007oO\u3007\u30078o, o8o08O8O, "add-const-float");
        \u3007000O0 = new Rop(14, o8\u3007OO0\u30070o, \u3007080OO8\u30070, "add-const-double");
        o\u3007o = new Rop(15, ooo0\u3007\u3007O, \u300708O\u300700\u3007o, "sub-const-int");
        O\u30070 = new Rop(15, \u3007\u300708O, o\u300700O, "sub-const-long");
        oo = new Rop(15, \u30078\u3007oO\u3007\u30078o, o8o08O8O, "sub-const-float");
        Oo = new Rop(15, o8\u3007OO0\u30070o, \u3007080OO8\u30070, "sub-const-double");
        Oo\u3007o = new Rop(16, ooo0\u3007\u3007O, \u300708O\u300700\u3007o, "mul-const-int");
        ooo0\u3007O88O = new Rop(16, \u3007\u300708O, o\u300700O, "mul-const-long");
        OOo8o\u3007O = new Rop(16, \u30078\u3007oO\u3007\u30078o, o8o08O8O, "mul-const-float");
        O880oOO08 = new Rop(16, o8\u3007OO0\u30070o, \u3007080OO8\u30070, "mul-const-double");
        OOo0O = new Rop(17, ooo0\u3007\u3007O, \u300708O\u300700\u3007o, oo0o\u3007\u3007\u3007\u30070, "div-const-int");
        O\u3007Oooo\u3007\u3007 = new Rop(17, \u3007\u300708O, o\u300700O, oo0o\u3007\u3007\u3007\u30070, "div-const-long");
        O\u3007OO = new Rop(17, \u30078\u3007oO\u3007\u30078o, o8o08O8O, "div-const-float");
        O\u300708 = new Rop(17, o8\u3007OO0\u30070o, \u3007080OO8\u30070, "div-const-double");
        O0OO8\u30070 = new Rop(18, ooo0\u3007\u3007O, \u300708O\u300700\u3007o, oo0o\u3007\u3007\u3007\u30070, "rem-const-int");
        ooO\u300700O = new Rop(18, \u3007\u300708O, o\u300700O, oo0o\u3007\u3007\u3007\u30070, "rem-const-long");
        O0\u3007OO8 = new Rop(18, \u30078\u3007oO\u3007\u30078o, o8o08O8O, "rem-const-float");
        Oo\u3007O8o\u30078 = new Rop(18, o8\u3007OO0\u30070o, \u3007080OO8\u30070, "rem-const-double");
        \u3007000\u3007\u300708 = new Rop(20, ooo0\u3007\u3007O, \u300708O\u300700\u3007o, "and-const-int");
        oO\u3007 = new Rop(20, \u3007\u300708O, o\u300700O, "and-const-long");
        O8O\u300788oO0 = new Rop(21, ooo0\u3007\u3007O, \u300708O\u300700\u3007o, "or-const-int");
        o80ooO = new Rop(21, \u3007\u300708O, o\u300700O, "or-const-long");
        O00 = new Rop(22, ooo0\u3007\u3007O, \u300708O\u300700\u3007o, "xor-const-int");
        o8O0 = new Rop(22, \u3007\u300708O, o\u300700O, "xor-const-long");
        O0o = new Rop(23, ooo0\u3007\u3007O, \u300708O\u300700\u3007o, "shl-const-int");
        O0o\u3007O0\u3007 = new Rop(23, \u3007\u300708O, \u300708O\u300700\u3007o, "shl-const-long");
        o0O\u30078o0O = new Rop(24, ooo0\u3007\u3007O, \u300708O\u300700\u3007o, "shr-const-int");
        Oo0oO\u3007O\u3007O = new Rop(24, \u3007\u300708O, \u300708O\u300700\u3007o, "shr-const-long");
        O0oO008 = new Rop(25, ooo0\u3007\u3007O, \u300708O\u300700\u3007o, "ushr-const-int");
        o\u30078\u3007 = new Rop(25, \u3007\u300708O, \u300708O\u300700\u3007o, "ushr-const-long");
        \u3007\u300700OO = new Rop(27, ooo0\u3007\u3007O, o8\u3007OO0\u30070o2, "cmpl-long");
        \u30070OO8 = new Rop(27, ooo0\u3007\u3007O, \u30078\u3007oO\u3007\u30078o2, "cmpl-float");
        oOo = new Rop(27, ooo0\u3007\u3007O, ooo0\u3007\u3007O2, "cmpl-double");
        o0 = new Rop(28, ooo0\u3007\u3007O, \u30078\u3007oO\u3007\u30078o2, "cmpg-float");
        O0oo0o0\u3007 = new Rop(28, ooo0\u3007\u3007O, ooo0\u3007\u3007O2, "cmpg-double");
        \u3007o\u30078 = new Rop(29, ooo0\u3007\u3007O, o\u300700O, "conv-l2i");
        ooo\u3007\u3007O\u3007 = new Rop(29, ooo0\u3007\u3007O, o8o08O8O, "conv-f2i");
        OO\u30070008O8 = new Rop(29, ooo0\u3007\u3007O, \u3007080OO8\u30070, "conv-d2i");
        \u30078o = new Rop(29, \u3007\u300708O, \u300708O\u300700\u3007o, "conv-i2l");
        O\u30070\u3007o808\u3007 = new Rop(29, \u3007\u300708O, o8o08O8O, "conv-f2l");
        \u3007o\u3007Oo0 = new Rop(29, \u3007\u300708O, \u3007080OO8\u30070, "conv-d2l");
        O00O = new Rop(29, \u30078\u3007oO\u3007\u30078o, \u300708O\u300700\u3007o, "conv-i2f");
        OOoo = new Rop(29, \u30078\u3007oO\u3007\u30078o, o\u300700O, "conv-l2f");
        OO0\u3007\u30078 = new Rop(29, \u30078\u3007oO\u3007\u30078o, \u3007080OO8\u30070, "conv-d2f");
        Oo08OO8oO = new Rop(29, o8\u3007OO0\u30070o, \u300708O\u300700\u3007o, "conv-i2d");
        \u3007Oo\u3007o8 = new Rop(29, o8\u3007OO0\u30070o, o\u300700O, "conv-l2d");
        O\u3007Oo = new Rop(29, o8\u3007OO0\u30070o, o8o08O8O, "conv-f2d");
        OoO\u3007 = new Rop(30, ooo0\u3007\u3007O, \u300708O\u300700\u3007o, "to-byte");
        \u3007o8OO0 = new Rop(31, ooo0\u3007\u3007O, \u300708O\u300700\u3007o, "to-char");
        \u3007\u30070o8O\u3007\u3007 = new Rop(32, ooo0\u3007\u3007O, \u300708O\u300700\u3007o, "to-short");
        O\u30078oOo8O = new Rop(33, o8oOOo, oo2, 2, "return-void");
        \u30078o\u3007\u30078080 = new Rop(33, o8oOOo, \u300708O\u300700\u3007o, 2, "return-int");
        \u3007oo = new Rop(33, o8oOOo, o\u300700O, 2, "return-long");
        \u30078o8O\u3007O = new Rop(33, o8oOOo, o8o08O8O, 2, "return-float");
        o8o\u3007\u30070O = new Rop(33, o8oOOo, \u3007080OO8\u30070, 2, "return-double");
        Oo0oOo\u30070 = new Rop(33, o8oOOo, \u30070O, 2, "return-object");
        final StdTypeList oo0o\u3007\u3007 = Exceptions.OO0o\u3007\u3007;
        O8oOo80 = new Rop(34, ooo0\u3007\u3007O, \u30070O, oo0o\u3007\u3007, "array-length");
        final StdTypeList oOo2 = StdTypeList.oOo0;
        o\u3007\u30070\u300788 = new Rop(35, o8oOOo, oOo2, oOo2, "throw");
        ooo8o\u3007o\u3007 = new Rop(36, o8oOOo, \u30070O, oo0o\u3007\u3007, "monitor-enter");
        oO8o = new Rop(37, o8oOOo, \u30070O, Exceptions.\u3007O\u3007, "monitor-exit");
        final StdTypeList ooo\u3007\u3007 = StdTypeList.oOO\u3007\u3007;
        final StdTypeList oooo8o0\u3007 = Exceptions.Oooo8o0\u3007;
        \u3007\u30070\u30070o8 = new Rop(38, ooo0\u3007\u3007O, ooo\u3007\u3007, oooo8o0\u3007, "aget-int");
        \u3007008\u3007oo = new Rop(38, \u3007\u300708O, StdTypeList.o8o, oooo8o0\u3007, "aget-long");
        O\u3007\u3007 = new Rop(38, \u30078\u3007oO\u3007\u30078o, StdTypeList.oo8ooo8O, oooo8o0\u3007, "aget-float");
        oo0O\u30070\u3007\u3007\u3007 = new Rop(38, o8\u3007OO0\u30070o, StdTypeList.o\u3007oO, oooo8o0\u3007, "aget-double");
        oO8008O = new Rop(38, \u3007\u3007o\u3007, StdTypeList.\u300708\u3007o0O, oooo8o0\u3007, "aget-object");
        O0\u3007oo = new Rop(38, ooo0\u3007\u3007O, StdTypeList.\u3007\u3007o\u3007, oooo8o0\u3007, "aget-boolean");
        \u3007OO8Oo0\u3007 = new Rop(38, ooo0\u3007\u3007O, StdTypeList.Oo80, oooo8o0\u3007, "aget-byte");
        o08oOO = new Rop(38, ooo0\u3007\u3007O, StdTypeList.O\u3007o88o08\u3007, oooo8o0\u3007, "aget-char");
        o88O\u30078 = new Rop(38, ooo0\u3007\u3007O, StdTypeList.\u300700O0, oooo8o0\u3007, "aget-short");
        o88O8 = new Rop(39, o8oOOo, StdTypeList.O\u300708oOOO0, oooo8o0\u3007, "aput-int");
        \u3007\u300700O\u30070o = new Rop(39, o8oOOo, StdTypeList.o8\u3007OO, oooo8o0\u3007, "aput-long");
        O8888 = new Rop(39, o8oOOo, StdTypeList.Ooo08, oooo8o0\u3007, "aput-float");
        \u30078O0O808\u3007 = new Rop(39, o8oOOo, StdTypeList.\u3007OO8ooO8\u3007, oooo8o0\u3007, "aput-double");
        final StdTypeList ooO = StdTypeList.ooO;
        final StdTypeList \u3007\u3007808\u30072 = Exceptions.\u3007\u3007808\u3007;
        OOo88OOo = new Rop(39, o8oOOo, ooO, \u3007\u3007808\u30072, "aput-object");
        o08\u3007\u30070O = new Rop(39, o8oOOo, StdTypeList.\u3007OO\u300700\u30070O, \u3007\u3007808\u30072, "aput-boolean");
        O\u3007oO\u3007oo8o = new Rop(39, o8oOOo, StdTypeList.Oo0\u3007Ooo, \u3007\u3007808\u30072, "aput-byte");
        O88\u3007\u3007o0O = new Rop(39, o8oOOo, StdTypeList.\u3007\u3007\u30070o\u3007\u30070, \u3007\u3007808\u30072, "aput-char");
        o08O = new Rop(39, o8oOOo, StdTypeList.oO\u30078O8oOo, \u3007\u3007808\u30072, "aput-short");
        O88o\u3007 = new Rop(40, \u3007\u3007o\u3007, oo2, \u300780\u3007808\u3007O2, "new-instance");
        final Type oOoo80oO = Type.oOoo80oO;
        final StdTypeList \u3007o8o08O = Exceptions.\u3007O8o08O;
        OoOOo8 = new Rop(41, oOoo80oO, \u300708O\u300700\u3007o, \u3007o8o08O, "new-array-int");
        \u3007080O0 = new Rop(41, Type.Oo0O0o8, \u300708O\u300700\u3007o, \u3007o8o08O, "new-array-long");
        \u3007008\u3007o0\u3007\u3007 = new Rop(41, Type.oOO0880O, \u300708O\u300700\u3007o, \u3007o8o08O, "new-array-float");
        Ooo8 = new Rop(41, Type.oO00\u3007o, \u300708O\u300700\u3007o, \u3007o8o08O, "new-array-double");
        Oo\u3007 = new Rop(41, Type.\u30070O\u3007O00O, \u300708O\u300700\u3007o, \u3007o8o08O, "new-array-boolean");
        O80\u3007O\u3007080 = new Rop(41, Type.o0OoOOo0, \u300708O\u300700\u3007o, \u3007o8o08O, "new-array-byte");
        O\u3007 = new Rop(41, Type.o\u3007o\u3007Oo88, \u300708O\u300700\u3007o, \u3007o8o08O, "new-array-char");
        \u3007\u3007\u3007 = new Rop(41, Type.\u3007800OO\u30070O, \u300708O\u300700\u3007o, \u3007o8o08O, "new-array-short");
        o\u30070o\u3007\u3007 = new Rop(43, o8oOOo, \u30070O, Exceptions.\u30078o8o\u3007, "check-cast");
        \u3007\u3007\u30070880 = new Rop(44, ooo0\u3007\u3007O, \u30070O, \u300780\u3007808\u3007O2, "instance-of");
        \u3007O8\u3007OO\u3007 = new Rop(45, ooo0\u3007\u3007O, \u30070O, oo0o\u3007\u3007, "get-field-int");
        \u3007o8oO = new Rop(45, \u3007\u300708O, \u30070O, oo0o\u3007\u3007, "get-field-long");
        \u3007oO8O0\u3007\u3007O = new Rop(45, \u30078\u3007oO\u3007\u30078o, \u30070O, oo0o\u3007\u3007, "get-field-float");
        OO8\u3007 = new Rop(45, o8\u3007OO0\u30070o, \u30070O, oo0o\u3007\u3007, "get-field-double");
        O8OO08o = new Rop(45, \u3007\u3007o\u3007, \u30070O, oo0o\u3007\u3007, "get-field-object");
        \u30078\u3007o\u30078 = new Rop(45, ooo0\u3007\u3007O, \u30070O, oo0o\u3007\u3007, "get-field-boolean");
        OO\u3007 = new Rop(45, ooo0\u3007\u3007O, \u30070O, oo0o\u3007\u3007, "get-field-byte");
        o8\u3007 = new Rop(45, ooo0\u3007\u3007O, \u30070O, oo0o\u3007\u3007, "get-field-char");
        \u3007\u3007O00\u30078 = new Rop(45, ooo0\u3007\u3007O, \u30070O, oo0o\u3007\u3007, "get-field-short");
        O8O\u30078oo08 = new Rop(46, ooo0\u3007\u3007O, oo2, \u300780\u3007808\u3007O2, "get-static-int");
        \u300708\u30070\u3007o\u30078 = new Rop(46, \u3007\u300708O, oo2, \u300780\u3007808\u3007O2, "get-static-long");
        o\u3007 = new Rop(46, \u30078\u3007oO\u3007\u30078o, oo2, \u300780\u3007808\u3007O2, "get-static-float");
        o0oO = new Rop(46, o8\u3007OO0\u30070o, oo2, \u300780\u3007808\u3007O2, "get-static-double");
        \u3007\u3007o0o = new Rop(46, \u3007\u3007o\u3007, oo2, \u300780\u3007808\u3007O2, "get-static-object");
        \u3007o\u3007o = new Rop(46, ooo0\u3007\u3007O, oo2, \u300780\u3007808\u3007O2, "get-field-boolean");
        O0o8\u3007O = new Rop(46, ooo0\u3007\u3007O, oo2, \u300780\u3007808\u3007O2, "get-field-byte");
        \u3007oOo\u3007 = new Rop(46, ooo0\u3007\u3007O, oo2, \u300780\u3007808\u3007O2, "get-field-char");
        o88o0O = new Rop(46, ooo0\u3007\u3007O, oo2, \u300780\u3007808\u3007O2, "get-field-short");
        final StdTypeList o0O = StdTypeList.O0O;
        \u30070O00oO = new Rop(47, o8oOOo, o0O, oo0o\u3007\u3007, "put-field-int");
        O08O0\u3007O = new Rop(47, o8oOOo, StdTypeList.o8oOOo, oo0o\u3007\u3007, "put-field-long");
        \u300708O8o8 = new Rop(47, o8oOOo, StdTypeList.\u3007O\u3007\u3007O8, oo0o\u3007\u3007, "put-field-float");
        o\u3007OOo000 = new Rop(47, o8oOOo, StdTypeList.\u3007o0O, oo0o\u3007\u3007, "put-field-double");
        O0o\u3007 = new Rop(47, o8oOOo, \u3007\u300708O2, oo0o\u3007\u3007, "put-field-object");
        OO88\u3007OOO = new Rop(47, o8oOOo, o0O, oo0o\u3007\u3007, "put-field-boolean");
        oO0\u3007\u3007O8o = new Rop(47, o8oOOo, o0O, oo0o\u3007\u3007, "put-field-byte");
        OO88o = new Rop(47, o8oOOo, o0O, oo0o\u3007\u3007, "put-field-char");
        Oo0O080 = new Rop(47, o8oOOo, o0O, oo0o\u3007\u3007, "put-field-short");
        O0O\u3007OOo = new Rop(48, o8oOOo, \u300708O\u300700\u3007o, \u300780\u3007808\u3007O2, "put-static-int");
        oO80OOO\u3007 = new Rop(48, o8oOOo, o\u300700O, \u300780\u3007808\u3007O2, "put-static-long");
        \u3007\u30070o\u3007o8 = new Rop(48, o8oOOo, o8o08O8O, \u300780\u3007808\u3007O2, "put-static-float");
        oO0\u3007\u3007o8\u3007 = new Rop(48, o8oOOo, \u3007080OO8\u30070, \u300780\u3007808\u3007O2, "put-static-double");
        \u3007\u30078 = new Rop(48, o8oOOo, \u30070O, \u300780\u3007808\u3007O2, "put-static-object");
        oo08OO\u30070 = new Rop(48, o8oOOo, \u300708O\u300700\u3007o, \u300780\u3007808\u3007O2, "put-static-boolean");
        \u3007\u30070O8ooO = new Rop(48, o8oOOo, \u300708O\u300700\u3007o, \u300780\u3007808\u3007O2, "put-static-byte");
        O0\u3007oO\u3007o = new Rop(48, o8oOOo, \u300708O\u300700\u3007o, \u300780\u3007808\u3007O2, "put-static-char");
        OOo = new Rop(48, o8oOOo, \u300708O\u300700\u3007o, \u300780\u3007808\u3007O2, "put-static-short");
        \u3007Oo = new Rop(54, o8oOOo, \u300708O\u300700\u3007o, "mark-local-int");
        o\u300700O0O\u3007o = new Rop(54, o8oOOo, o\u300700O, "mark-local-long");
        \u3007O80\u3007oOo = new Rop(54, o8oOOo, o8o08O8O, "mark-local-float");
        \u3007OO0 = new Rop(54, o8oOOo, \u3007080OO8\u30070, "mark-local-double");
        oO0 = new Rop(54, o8oOOo, \u30070O, "mark-local-object");
        o\u30070\u3007 = new Rop(57, o8oOOo, oo2, "fill-array-data");
    }
    
    public static Rop O8(final TypeBearer typeBearer, final TypeBearer typeBearer2) {
        final int \u3007080 = typeBearer.\u3007080();
        final int \u300781 = typeBearer2.\u3007080();
        if (\u300781 != 4) {
            if (\u300781 != 5) {
                if (\u300781 != 6) {
                    if (\u300781 != 7) {
                        return o\u3007O8\u3007\u3007o(StdTypeList.OoO8(typeBearer.getType(), typeBearer2.getType()));
                    }
                }
                else {
                    if (\u3007080 == 4) {
                        return Rops.Oo08OO8oO;
                    }
                    if (\u3007080 == 5) {
                        return Rops.O00O;
                    }
                    if (\u3007080 == 7) {
                        return Rops.\u30078o;
                    }
                }
                if (\u3007080 == 4) {
                    return Rops.\u3007Oo\u3007o8;
                }
                if (\u3007080 == 5) {
                    return Rops.OOoo;
                }
                if (\u3007080 == 6) {
                    return Rops.\u3007o\u30078;
                }
            }
            if (\u3007080 == 4) {
                return Rops.O\u3007Oo;
            }
            if (\u3007080 == 6) {
                return Rops.ooo\u3007\u3007O\u3007;
            }
            if (\u3007080 == 7) {
                return Rops.O\u30070\u3007o808\u3007;
            }
        }
        if (\u3007080 == 5) {
            return Rops.OO0\u3007\u30078;
        }
        if (\u3007080 == 6) {
            return Rops.OO\u30070008O8;
        }
        if (\u3007080 == 7) {
            return Rops.\u3007o\u3007Oo0;
        }
        return o\u3007O8\u3007\u3007o(StdTypeList.OoO8(typeBearer.getType(), typeBearer2.getType()));
    }
    
    public static Rop OO0o\u3007\u3007(final Prototype prototype) {
        return new Rop(52, prototype.O8(), StdTypeList.oOo0);
    }
    
    public static Rop OO0o\u3007\u3007\u3007\u30070(final TypeList list) {
        return oo88o8O(list, Rops.\u3007oo\u3007, null, Rops.OOO\u3007O0, null);
    }
    
    public static Rop Oo08(final TypeBearer typeBearer) {
        switch (typeBearer.\u3007o00\u3007\u3007Oo()) {
            default: {
                return \u3007oo\u3007(typeBearer);
            }
            case 9: {
                return Rops.O8OO08o;
            }
            case 8: {
                return Rops.\u3007\u3007O00\u30078;
            }
            case 7: {
                return Rops.\u3007o8oO;
            }
            case 6: {
                return Rops.\u3007O8\u3007OO\u3007;
            }
            case 5: {
                return Rops.\u3007oO8O0\u3007\u3007O;
            }
            case 4: {
                return Rops.OO8\u3007;
            }
            case 3: {
                return Rops.o8\u3007;
            }
            case 2: {
                return Rops.OO\u3007;
            }
            case 1: {
                return Rops.\u30078\u3007o\u30078;
            }
        }
    }
    
    public static Rop OoO8(final TypeBearer typeBearer) {
        return new Rop(56, typeBearer.getType(), StdTypeList.OO, null);
    }
    
    public static Rop Oooo8o0\u3007(final Prototype prototype) {
        return new Rop(53, prototype.O8(), StdTypeList.oOo0);
    }
    
    public static Rop o800o8O(final TypeBearer typeBearer) {
        final Type type = typeBearer.getType();
        switch (type.\u3007\u3007888().\u3007o00\u3007\u3007Oo()) {
            default: {
                return \u3007oo\u3007(type);
            }
            case 9: {
                return new Rop(41, type, StdTypeList.\u300708O\u300700\u3007o, Exceptions.\u3007O8o08O, "new-array-object");
            }
            case 8: {
                return Rops.\u3007\u3007\u3007;
            }
            case 7: {
                return Rops.\u3007080O0;
            }
            case 6: {
                return Rops.OoOOo8;
            }
            case 5: {
                return Rops.\u3007008\u3007o0\u3007\u3007;
            }
            case 4: {
                return Rops.Ooo8;
            }
            case 3: {
                return Rops.O\u3007;
            }
            case 2: {
                return Rops.O80\u3007O\u3007080;
            }
            case 1: {
                return Rops.Oo\u3007;
            }
        }
    }
    
    public static Rop oO80(final TypeList list) {
        return oo88o8O(list, Rops.oo88o8O, null, Rops.o\u3007\u30070\u3007, null);
    }
    
    private static Rop oo88o8O(final TypeList list, final Rop rop, final Rop rop2, final Rop rop3, final Rop rop4) {
        final int size = list.size();
        if (size != 1) {
            if (size == 2) {
                final int \u3007080 = list.getType(0).\u3007080();
                if (\u3007080 == list.getType(1).\u3007080()) {
                    if (\u3007080 == 6) {
                        return rop3;
                    }
                    if (\u3007080 == 9) {
                        if (rop4 != null) {
                            return rop4;
                        }
                    }
                }
            }
        }
        else {
            final int \u300781 = list.getType(0).\u3007080();
            if (\u300781 == 6) {
                return rop;
            }
            if (\u300781 == 9) {
                if (rop2 != null) {
                    return rop2;
                }
            }
        }
        return o\u3007O8\u3007\u3007o(list);
    }
    
    public static Rop o\u30070(final TypeBearer typeBearer) {
        switch (typeBearer.\u3007o00\u3007\u3007Oo()) {
            default: {
                return \u3007oo\u3007(typeBearer);
            }
            case 9: {
                return Rops.\u3007\u3007o0o;
            }
            case 8: {
                return Rops.o88o0O;
            }
            case 7: {
                return Rops.\u300708\u30070\u3007o\u30078;
            }
            case 6: {
                return Rops.O8O\u30078oo08;
            }
            case 5: {
                return Rops.o\u3007;
            }
            case 4: {
                return Rops.o0oO;
            }
            case 3: {
                return Rops.\u3007oOo\u3007;
            }
            case 2: {
                return Rops.O0o8\u3007O;
            }
            case 1: {
                return Rops.\u3007o\u3007o;
            }
        }
    }
    
    private static Rop o\u3007O8\u3007\u3007o(final TypeList obj) {
        final StringBuilder sb = new StringBuilder();
        sb.append("bad types: ");
        sb.append(obj);
        throw new IllegalArgumentException(sb.toString());
    }
    
    public static Rop \u3007080(final TypeBearer typeBearer) {
        switch (typeBearer.\u3007o00\u3007\u3007Oo()) {
            default: {
                return \u3007oo\u3007(typeBearer);
            }
            case 9: {
                return Rops.oO8008O;
            }
            case 8: {
                return Rops.o88O\u30078;
            }
            case 7: {
                return Rops.\u3007008\u3007oo;
            }
            case 6: {
                return Rops.\u3007\u30070\u30070o8;
            }
            case 5: {
                return Rops.O\u3007\u3007;
            }
            case 4: {
                return Rops.oo0O\u30070\u3007\u3007\u3007;
            }
            case 3: {
                return Rops.o08oOO;
            }
            case 2: {
                return Rops.\u3007OO8Oo0\u3007;
            }
            case 1: {
                return Rops.O0\u3007oo;
            }
        }
    }
    
    public static Rop \u30070\u3007O0088o(final TypeBearer typeBearer) {
        return new Rop(55, typeBearer.getType(), StdTypeList.OO, null);
    }
    
    public static Rop \u300780\u3007808\u3007O(final TypeList list) {
        return oo88o8O(list, Rops.o\u3007O8\u3007\u3007o, null, Rops.oo\u3007, null);
    }
    
    public static Rop \u30078o8o\u3007(final TypeList list) {
        return oo88o8O(list, Rops.\u3007O888o0o, null, Rops.\u30070000OOO, null);
    }
    
    public static Rop \u3007O00(final Prototype prototype) {
        return new Rop(50, prototype.O8(), StdTypeList.oOo0);
    }
    
    public static Rop \u3007O888o0o(final TypeBearer typeBearer) {
        final int \u3007080 = typeBearer.\u3007080();
        if (\u3007080 == 0) {
            return Rops.O\u30078oOo8O;
        }
        if (\u3007080 == 9) {
            return Rops.Oo0oOo\u30070;
        }
        if (\u3007080 == 4) {
            return Rops.o8o\u3007\u30070O;
        }
        if (\u3007080 == 5) {
            return Rops.\u30078o8O\u3007O;
        }
        if (\u3007080 == 6) {
            return Rops.\u30078o\u3007\u30078080;
        }
        if (\u3007080 != 7) {
            return \u3007oo\u3007(typeBearer);
        }
        return Rops.\u3007oo;
    }
    
    public static Rop \u3007O8o08O(final TypeList list) {
        return oo88o8O(list, Rops.o800o8O, Rops.O\u30078O8\u3007008, Rops.\u3007oOO8O8, Rops.\u300700\u30078);
    }
    
    public static Rop \u3007O\u3007(final Prototype prototype) {
        return new Rop(51, prototype.O8(), StdTypeList.oOo0);
    }
    
    public static Rop \u3007o00\u3007\u3007Oo(final TypeBearer typeBearer) {
        switch (typeBearer.\u3007o00\u3007\u3007Oo()) {
            default: {
                return \u3007oo\u3007(typeBearer);
            }
            case 9: {
                return Rops.OOo88OOo;
            }
            case 8: {
                return Rops.o08O;
            }
            case 7: {
                return Rops.\u3007\u300700O\u30070o;
            }
            case 6: {
                return Rops.o88O8;
            }
            case 5: {
                return Rops.O8888;
            }
            case 4: {
                return Rops.\u30078O0O808\u3007;
            }
            case 3: {
                return Rops.O88\u3007\u3007o0O;
            }
            case 2: {
                return Rops.O\u3007oO\u3007oo8o;
            }
            case 1: {
                return Rops.o08\u3007\u30070O;
            }
        }
    }
    
    private static Rop \u3007oo\u3007(final TypeBearer obj) {
        final StringBuilder sb = new StringBuilder();
        sb.append("bad type: ");
        sb.append(obj);
        throw new IllegalArgumentException(sb.toString());
    }
    
    public static Rop \u3007o\u3007(final TypeBearer typeBearer) {
        if (typeBearer.getType() == Type.\u3007O\u3007\u3007O8) {
            return Rops.\u3007\u30078O0\u30078;
        }
        final int \u3007080 = typeBearer.\u3007080();
        if (\u3007080 == 4) {
            return Rops.\u3007O\u3007;
        }
        if (\u3007080 == 5) {
            return Rops.\u3007\u3007808\u3007;
        }
        if (\u3007080 == 6) {
            return Rops.OO0o\u3007\u3007;
        }
        if (\u3007080 == 7) {
            return Rops.Oooo8o0\u3007;
        }
        if (\u3007080 != 9) {
            return \u3007oo\u3007(typeBearer);
        }
        return Rops.\u3007O00;
    }
    
    public static Rop \u3007\u3007808\u3007(final Prototype prototype) {
        return new Rop(49, prototype.O8(), StdTypeList.oOo0);
    }
    
    public static Rop \u3007\u3007888(final TypeList list) {
        return oo88o8O(list, Rops.OoO8, Rops.\u300700, Rops.O8ooOoo\u3007, Rops.O8\u3007o);
    }
    
    public static Rop \u3007\u30078O0\u30078(final TypeBearer typeBearer) {
        final int \u3007080 = typeBearer.\u3007080();
        if (\u3007080 == 4) {
            return Rops.\u30078o8o\u3007;
        }
        if (\u3007080 == 5) {
            return Rops.OO0o\u3007\u3007\u3007\u30070;
        }
        if (\u3007080 == 6) {
            return Rops.oO80;
        }
        if (\u3007080 == 7) {
            return Rops.\u300780\u3007808\u3007O;
        }
        if (\u3007080 != 9) {
            return \u3007oo\u3007(typeBearer);
        }
        return Rops.\u3007O8o08O;
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.android.dx.rop.code;

import com.android.dx.util.FixedSizeList;

public final class InsnList extends FixedSizeList
{
    public InsnList(final int n) {
        super(n);
    }
    
    public Insn \u3007O00() {
        return this.\u3007O\u3007(this.size() - 1);
    }
    
    public Insn \u3007O\u3007(final int n) {
        return (Insn)this.oO80(n);
    }
    
    public void \u3007\u3007808\u3007(final Insn.Visitor visitor) {
        for (int size = this.size(), i = 0; i < size; ++i) {
            this.\u3007O\u3007(i).\u3007o\u3007(visitor);
        }
    }
    
    public void \u3007\u30078O0\u30078(final int n, final Insn insn) {
        this.OO0o\u3007\u3007\u3007\u30070(n, insn);
    }
}

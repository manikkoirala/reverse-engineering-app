// 
// Decompiled by Procyon v0.6.0
// 

package com.android.dx.rop.code;

import com.android.dx.rop.type.StdTypeList;
import com.android.dx.rop.type.Type;

public final class Exceptions
{
    public static final Type O8;
    public static final StdTypeList OO0o\u3007\u3007;
    public static final StdTypeList OO0o\u3007\u3007\u3007\u30070;
    public static final Type Oo08;
    public static final StdTypeList Oooo8o0\u3007;
    public static final Type oO80;
    public static final Type o\u30070;
    public static final Type \u3007080;
    public static final StdTypeList \u300780\u3007808\u3007O;
    public static final StdTypeList \u30078o8o\u3007;
    public static final StdTypeList \u3007O8o08O;
    public static final StdTypeList \u3007O\u3007;
    public static final Type \u3007o00\u3007\u3007Oo;
    public static final Type \u3007o\u3007;
    public static final StdTypeList \u3007\u3007808\u3007;
    public static final Type \u3007\u3007888;
    
    static {
        final Type type = \u3007080 = Type.OO0o\u3007\u3007\u3007\u30070("Ljava/lang/ArithmeticException;");
        final Type type2 = \u3007o00\u3007\u3007Oo = Type.OO0o\u3007\u3007\u3007\u30070("Ljava/lang/ArrayIndexOutOfBoundsException;");
        final Type type3 = \u3007o\u3007 = Type.OO0o\u3007\u3007\u3007\u30070("Ljava/lang/ArrayStoreException;");
        final Type type4 = O8 = Type.OO0o\u3007\u3007\u3007\u30070("Ljava/lang/ClassCastException;");
        final Type type5 = Oo08 = Type.OO0o\u3007\u3007\u3007\u30070("Ljava/lang/Error;");
        final Type type6 = o\u30070 = Type.OO0o\u3007\u3007\u3007\u30070("Ljava/lang/IllegalMonitorStateException;");
        final Type type7 = \u3007\u3007888 = Type.OO0o\u3007\u3007\u3007\u30070("Ljava/lang/NegativeArraySizeException;");
        final Type type8 = oO80 = Type.OO0o\u3007\u3007\u3007\u30070("Ljava/lang/NullPointerException;");
        \u300780\u3007808\u3007O = StdTypeList.\u30070\u3007O0088o(type5);
        OO0o\u3007\u3007\u3007\u30070 = StdTypeList.OoO8(type5, type);
        \u30078o8o\u3007 = StdTypeList.OoO8(type5, type4);
        \u3007O8o08O = StdTypeList.OoO8(type5, type7);
        OO0o\u3007\u3007 = StdTypeList.OoO8(type5, type8);
        Oooo8o0\u3007 = StdTypeList.o800o8O(type5, type8, type2);
        \u3007\u3007808\u3007 = StdTypeList.\u3007O888o0o(type5, type8, type2, type3);
        \u3007O\u3007 = StdTypeList.o800o8O(type5, type8, type6);
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.android.dx.rop.code;

import com.android.dx.rop.type.StdTypeList;
import com.android.dx.rop.type.TypeList;
import com.android.dx.rop.cst.Constant;

public final class PlainCstInsn extends CstInsn
{
    public PlainCstInsn(final Rop rop, final SourcePosition sourcePosition, final RegisterSpec registerSpec, final RegisterSpecList list, final Constant constant) {
        super(rop, sourcePosition, registerSpec, list, constant);
        if (rop.\u3007o00\u3007\u3007Oo() == 1) {
            return;
        }
        final StringBuilder sb = new StringBuilder();
        sb.append("opcode with invalid branchingness: ");
        sb.append(rop.\u3007o00\u3007\u3007Oo());
        throw new IllegalArgumentException(sb.toString());
    }
    
    @Override
    public TypeList Oo08() {
        return StdTypeList.OO;
    }
    
    @Override
    public void \u3007o\u3007(final Visitor visitor) {
        visitor.\u3007080(this);
    }
}

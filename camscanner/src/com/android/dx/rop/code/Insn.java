// 
// Decompiled by Procyon v0.6.0
// 

package com.android.dx.rop.code;

import com.android.dx.rop.type.TypeList;
import com.android.dx.util.ToHuman;

public abstract class Insn implements ToHuman
{
    private final RegisterSpec OO;
    private final Rop o0;
    private final RegisterSpecList \u300708O\u300700\u3007o;
    private final SourcePosition \u3007OOo8\u30070;
    
    public Insn(final Rop o0, final SourcePosition \u3007oOo8\u30070, final RegisterSpec oo, final RegisterSpecList \u300708O\u300700\u3007o) {
        if (o0 == null) {
            throw new NullPointerException("opcode == null");
        }
        if (\u3007oOo8\u30070 == null) {
            throw new NullPointerException("position == null");
        }
        if (\u300708O\u300700\u3007o != null) {
            this.o0 = o0;
            this.\u3007OOo8\u30070 = \u3007oOo8\u30070;
            this.OO = oo;
            this.\u300708O\u300700\u3007o = \u300708O\u300700\u3007o;
            return;
        }
        throw new NullPointerException("sources == null");
    }
    
    public final boolean O8() {
        return this.o0.\u3007080();
    }
    
    public final RegisterSpecList OO0o\u3007\u3007\u3007\u30070() {
        return this.\u300708O\u300700\u3007o;
    }
    
    public abstract TypeList Oo08();
    
    @Override
    public final boolean equals(final Object o) {
        return this == o;
    }
    
    @Override
    public final int hashCode() {
        return System.identityHashCode(this);
    }
    
    public final SourcePosition oO80() {
        return this.\u3007OOo8\u30070;
    }
    
    public String o\u30070() {
        return null;
    }
    
    @Override
    public String toHuman() {
        return this.\u30078o8o\u3007(this.o\u30070());
    }
    
    @Override
    public String toString() {
        return this.\u3007O8o08O(this.o\u30070());
    }
    
    public final RegisterSpec \u300780\u3007808\u3007O() {
        return this.OO;
    }
    
    protected final String \u30078o8o\u3007(final String str) {
        final StringBuilder sb = new StringBuilder(80);
        sb.append(this.\u3007OOo8\u30070);
        sb.append(": ");
        sb.append(this.o0.\u3007o\u3007());
        if (str != null) {
            sb.append("(");
            sb.append(str);
            sb.append(")");
        }
        if (this.OO == null) {
            sb.append(" .");
        }
        else {
            sb.append(" ");
            sb.append(this.OO.toHuman());
        }
        sb.append(" <-");
        final int size = this.\u300708O\u300700\u3007o.size();
        if (size == 0) {
            sb.append(" .");
        }
        else {
            for (int i = 0; i < size; ++i) {
                sb.append(" ");
                sb.append(this.\u300708O\u300700\u3007o.\u3007O00(i).toHuman());
            }
        }
        return sb.toString();
    }
    
    protected final String \u3007O8o08O(final String str) {
        final StringBuilder sb = new StringBuilder(80);
        sb.append("Insn{");
        sb.append(this.\u3007OOo8\u30070);
        sb.append(' ');
        sb.append(this.o0);
        if (str != null) {
            sb.append(' ');
            sb.append(str);
        }
        sb.append(" :: ");
        final RegisterSpec oo = this.OO;
        if (oo != null) {
            sb.append(oo);
            sb.append(" <- ");
        }
        sb.append(this.\u300708O\u300700\u3007o);
        sb.append('}');
        return sb.toString();
    }
    
    public abstract void \u3007o\u3007(final Visitor p0);
    
    public final Rop \u3007\u3007888() {
        return this.o0;
    }
    
    public static class BaseVisitor implements Visitor
    {
        @Override
        public void O8(final ThrowingInsn throwingInsn) {
        }
        
        @Override
        public void \u3007o00\u3007\u3007Oo(final ThrowingCstInsn throwingCstInsn) {
        }
        
        @Override
        public void \u3007o\u3007(final PlainInsn plainInsn) {
        }
    }
    
    public interface Visitor
    {
        void O8(final ThrowingInsn p0);
        
        void \u3007080(final PlainCstInsn p0);
        
        void \u3007o00\u3007\u3007Oo(final ThrowingCstInsn p0);
        
        void \u3007o\u3007(final PlainInsn p0);
    }
}

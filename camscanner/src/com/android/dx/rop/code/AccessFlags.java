// 
// Decompiled by Procyon v0.6.0
// 

package com.android.dx.rop.code;

import com.android.dx.util.Hex;

public final class AccessFlags
{
    public static String O8(final int n) {
        return \u3007o\u3007(n, 204287, 3);
    }
    
    public static String \u3007080(final int n) {
        return \u3007o\u3007(n, 30257, 1);
    }
    
    public static String \u3007o00\u3007\u3007Oo(final int n) {
        return \u3007o\u3007(n, 20703, 2);
    }
    
    private static String \u3007o\u3007(int n, final int n2, final int n3) {
        final StringBuilder sb = new StringBuilder(80);
        final int n4 = ~n2 & n;
        n &= n2;
        if ((n & 0x1) != 0x0) {
            sb.append("|public");
        }
        if ((n & 0x2) != 0x0) {
            sb.append("|private");
        }
        if ((n & 0x4) != 0x0) {
            sb.append("|protected");
        }
        if ((n & 0x8) != 0x0) {
            sb.append("|static");
        }
        if ((n & 0x10) != 0x0) {
            sb.append("|final");
        }
        if ((n & 0x20) != 0x0) {
            if (n3 == 1) {
                sb.append("|super");
            }
            else {
                sb.append("|synchronized");
            }
        }
        if ((n & 0x40) != 0x0) {
            if (n3 == 3) {
                sb.append("|bridge");
            }
            else {
                sb.append("|volatile");
            }
        }
        if ((n & 0x80) != 0x0) {
            if (n3 == 3) {
                sb.append("|varargs");
            }
            else {
                sb.append("|transient");
            }
        }
        if ((n & 0x100) != 0x0) {
            sb.append("|native");
        }
        if ((n & 0x200) != 0x0) {
            sb.append("|interface");
        }
        if ((n & 0x400) != 0x0) {
            sb.append("|abstract");
        }
        if ((n & 0x800) != 0x0) {
            sb.append("|strictfp");
        }
        if ((n & 0x1000) != 0x0) {
            sb.append("|synthetic");
        }
        if ((n & 0x2000) != 0x0) {
            sb.append("|annotation");
        }
        if ((n & 0x4000) != 0x0) {
            sb.append("|enum");
        }
        if ((0x10000 & n) != 0x0) {
            sb.append("|constructor");
        }
        if ((n & 0x20000) != 0x0) {
            sb.append("|declared_synchronized");
        }
        if (n4 != 0 || sb.length() == 0) {
            sb.append('|');
            sb.append(Hex.Oo08(n4));
        }
        return sb.substring(1);
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.android.dx.rop.code;

import com.android.dx.util.Hex;
import com.android.dx.util.IntList;
import com.android.dx.util.LabeledItem;

public final class BasicBlock implements LabeledItem
{
    private final int O8;
    private final int \u3007080;
    private final InsnList \u3007o00\u3007\u3007Oo;
    private final IntList \u3007o\u3007;
    
    public BasicBlock(final int \u3007080, final InsnList \u3007o00\u3007\u3007Oo, final IntList list, final int n) {
        if (\u3007080 >= 0) {
            try {
                \u3007o00\u3007\u3007Oo.\u3007\u3007888();
                final int size = \u3007o00\u3007\u3007Oo.size();
                if (size != 0) {
                    for (int i = size - 2; i >= 0; --i) {
                        if (\u3007o00\u3007\u3007Oo.\u3007O\u3007(i).\u3007\u3007888().\u3007o00\u3007\u3007Oo() != 1) {
                            final StringBuilder sb = new StringBuilder();
                            sb.append("insns[");
                            sb.append(i);
                            sb.append("] is a branch or can throw");
                            throw new IllegalArgumentException(sb.toString());
                        }
                    }
                    if (\u3007o00\u3007\u3007Oo.\u3007O\u3007(size - 1).\u3007\u3007888().\u3007o00\u3007\u3007Oo() != 1) {
                        try {
                            list.\u3007\u3007888();
                            if (n < -1) {
                                throw new IllegalArgumentException("primarySuccessor < -1");
                            }
                            if (n >= 0 && !list.OO0o\u3007\u3007\u3007\u30070(n)) {
                                final StringBuilder sb2 = new StringBuilder();
                                sb2.append("primarySuccessor ");
                                sb2.append(n);
                                sb2.append(" not in successors ");
                                sb2.append(list);
                                throw new IllegalArgumentException(sb2.toString());
                            }
                            this.\u3007080 = \u3007080;
                            this.\u3007o00\u3007\u3007Oo = \u3007o00\u3007\u3007Oo;
                            this.\u3007o\u3007 = list;
                            this.O8 = n;
                            return;
                        }
                        catch (final NullPointerException ex) {
                            throw new NullPointerException("successors == null");
                        }
                    }
                    throw new IllegalArgumentException("insns does not end with a branch or throwing instruction");
                }
                throw new IllegalArgumentException("insns.size() == 0");
            }
            catch (final NullPointerException ex2) {
                throw new NullPointerException("insns == null");
            }
        }
        throw new IllegalArgumentException("label < 0");
    }
    
    public int O8() {
        return this.O8;
    }
    
    public int Oo08() {
        if (this.\u3007o\u3007.size() == 2) {
            int n;
            if ((n = this.\u3007o\u3007.\u30078o8o\u3007(0)) == this.O8) {
                n = this.\u3007o\u3007.\u30078o8o\u3007(1);
            }
            return n;
        }
        throw new UnsupportedOperationException("block doesn't have exactly two successors");
    }
    
    @Override
    public boolean equals(final Object o) {
        return this == o;
    }
    
    @Override
    public int getLabel() {
        return this.\u3007080;
    }
    
    @Override
    public int hashCode() {
        return System.identityHashCode(this);
    }
    
    public IntList o\u30070() {
        return this.\u3007o\u3007;
    }
    
    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append('{');
        sb.append(Hex.Oo08(this.\u3007080));
        sb.append('}');
        return sb.toString();
    }
    
    public boolean \u3007080() {
        return this.\u3007o00\u3007\u3007Oo.\u3007O00().O8();
    }
    
    public InsnList \u3007o00\u3007\u3007Oo() {
        return this.\u3007o00\u3007\u3007Oo;
    }
    
    public Insn \u3007o\u3007() {
        return this.\u3007o00\u3007\u3007Oo.\u3007O00();
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.android.dx.rop.annotation;

import com.android.dx.util.ToHuman;

public enum AnnotationVisibility implements ToHuman
{
    private static final AnnotationVisibility[] $VALUES;
    
    BUILD("build"), 
    EMBEDDED("embedded"), 
    RUNTIME("runtime"), 
    SYSTEM("system");
    
    private final String human;
    
    private AnnotationVisibility(final String human) {
        this.human = human;
    }
    
    @Override
    public String toHuman() {
        return this.human;
    }
}

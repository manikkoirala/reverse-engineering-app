// 
// Decompiled by Procyon v0.6.0
// 

package com.android.dx.rop.annotation;

import java.util.Collections;
import java.util.Collection;
import java.util.Iterator;
import com.android.dx.rop.cst.Constant;
import com.android.dx.rop.cst.CstType;
import com.android.dx.rop.cst.CstString;
import java.util.TreeMap;
import com.android.dx.util.ToHuman;
import com.android.dx.util.MutabilityControl;

public final class Annotation extends MutabilityControl implements Comparable<Annotation>, ToHuman
{
    private final AnnotationVisibility OO;
    private final TreeMap<CstString, NameValuePair> \u300708O\u300700\u3007o;
    private final CstType \u3007OOo8\u30070;
    
    public AnnotationVisibility OO0o\u3007\u3007\u3007\u30070() {
        return this.OO;
    }
    
    @Override
    public boolean equals(final Object o) {
        if (!(o instanceof Annotation)) {
            return false;
        }
        final Annotation annotation = (Annotation)o;
        return this.\u3007OOo8\u30070.equals(annotation.\u3007OOo8\u30070) && this.OO == annotation.OO && this.\u300708O\u300700\u3007o.equals(annotation.\u300708O\u300700\u3007o);
    }
    
    public CstType getType() {
        return this.\u3007OOo8\u30070;
    }
    
    @Override
    public int hashCode() {
        return (this.\u3007OOo8\u30070.hashCode() * 31 + this.\u300708O\u300700\u3007o.hashCode()) * 31 + this.OO.hashCode();
    }
    
    public int oO80(final Annotation annotation) {
        final int \u3007o\u3007 = this.\u3007OOo8\u30070.\u3007o\u3007(annotation.\u3007OOo8\u30070);
        if (\u3007o\u3007 != 0) {
            return \u3007o\u3007;
        }
        final int compareTo = this.OO.compareTo(annotation.OO);
        if (compareTo != 0) {
            return compareTo;
        }
        final Iterator<NameValuePair> iterator = this.\u300708O\u300700\u3007o.values().iterator();
        final Iterator<NameValuePair> iterator2 = annotation.\u300708O\u300700\u3007o.values().iterator();
        while (iterator.hasNext() && iterator2.hasNext()) {
            final int \u3007080 = iterator.next().\u3007080(iterator2.next());
            if (\u3007080 != 0) {
                return \u3007080;
            }
        }
        if (iterator.hasNext()) {
            return 1;
        }
        if (iterator2.hasNext()) {
            return -1;
        }
        return 0;
    }
    
    @Override
    public String toHuman() {
        final StringBuilder sb = new StringBuilder();
        sb.append(this.OO.toHuman());
        sb.append("-annotation ");
        sb.append(this.\u3007OOo8\u30070.toHuman());
        sb.append(" {");
        final Iterator<NameValuePair> iterator = this.\u300708O\u300700\u3007o.values().iterator();
        int n = 1;
        while (iterator.hasNext()) {
            final NameValuePair nameValuePair = iterator.next();
            if (n != 0) {
                n = 0;
            }
            else {
                sb.append(", ");
            }
            sb.append(nameValuePair.\u3007o00\u3007\u3007Oo().toHuman());
            sb.append(": ");
            sb.append(nameValuePair.\u3007o\u3007().toHuman());
        }
        sb.append("}");
        return sb.toString();
    }
    
    @Override
    public String toString() {
        return this.toHuman();
    }
    
    public Collection<NameValuePair> \u300780\u3007808\u3007O() {
        return Collections.unmodifiableCollection((Collection<? extends NameValuePair>)this.\u300708O\u300700\u3007o.values());
    }
}

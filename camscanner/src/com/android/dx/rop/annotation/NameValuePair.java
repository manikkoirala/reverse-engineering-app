// 
// Decompiled by Procyon v0.6.0
// 

package com.android.dx.rop.annotation;

import com.android.dx.rop.cst.Constant;
import com.android.dx.rop.cst.CstString;

public final class NameValuePair implements Comparable<NameValuePair>
{
    private final CstString o0;
    private final Constant \u3007OOo8\u30070;
    
    @Override
    public boolean equals(final Object o) {
        final boolean b = o instanceof NameValuePair;
        final boolean b2 = false;
        if (!b) {
            return false;
        }
        final NameValuePair nameValuePair = (NameValuePair)o;
        boolean b3 = b2;
        if (this.o0.equals(nameValuePair.o0)) {
            b3 = b2;
            if (this.\u3007OOo8\u30070.equals(nameValuePair.\u3007OOo8\u30070)) {
                b3 = true;
            }
        }
        return b3;
    }
    
    @Override
    public int hashCode() {
        return this.o0.hashCode() * 31 + this.\u3007OOo8\u30070.hashCode();
    }
    
    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append(this.o0.toHuman());
        sb.append(":");
        sb.append(this.\u3007OOo8\u30070);
        return sb.toString();
    }
    
    public int \u3007080(final NameValuePair nameValuePair) {
        final int \u3007o\u3007 = this.o0.\u3007o\u3007(nameValuePair.o0);
        if (\u3007o\u3007 != 0) {
            return \u3007o\u3007;
        }
        return this.\u3007OOo8\u30070.\u3007o\u3007(nameValuePair.\u3007OOo8\u30070);
    }
    
    public CstString \u3007o00\u3007\u3007Oo() {
        return this.o0;
    }
    
    public Constant \u3007o\u3007() {
        return this.\u3007OOo8\u30070;
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.android.dx.rop.type;

public interface TypeList
{
    Type getType(final int p0);
    
    int size();
}

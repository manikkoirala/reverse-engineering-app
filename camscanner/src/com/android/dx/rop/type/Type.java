// 
// Decompiled by Procyon v0.6.0
// 

package com.android.dx.rop.type;

import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

public final class Type implements TypeBearer, Comparable<Type>
{
    public static final Type O0O;
    public static final Type O88O;
    public static final Type OO\u300700\u30078oO;
    public static final Type OO\u3007OOo;
    public static final Type Oo0O0o8;
    public static final Type Oo0\u3007Ooo;
    public static final Type Oo80;
    public static final Type Ooo08;
    public static final Type O\u300708oOOO0;
    public static final Type O\u3007o88o08\u3007;
    public static final Type o0OoOOo0;
    public static final Type o8o;
    public static final Type o8oOOo;
    public static final Type o8\u3007OO;
    public static final Type o8\u3007OO0\u30070o;
    public static final Type oO00\u3007o;
    public static final Type oOO0880O;
    public static final Type oOO\u3007\u3007;
    public static final Type oOo0;
    public static final Type oOoo80oO;
    public static final Type oOo\u30078o008;
    public static final Type oO\u30078O8oOo;
    public static final Type oo8ooo8O;
    public static final Type ooO;
    public static final Type ooo0\u3007\u3007O;
    public static final Type o\u3007oO;
    public static final Type o\u3007o\u3007Oo88;
    public static final Type \u300700O0;
    public static final Type \u300708\u3007o0O;
    private static final ConcurrentMap<String, Type> \u30070O;
    public static final Type \u30070O\u3007O00O;
    public static final Type \u3007800OO\u30070O;
    public static final Type \u30078\u3007oO\u3007\u30078o;
    public static final Type \u3007OO8ooO8\u3007;
    public static final Type \u3007OO\u300700\u30070O;
    public static final Type \u3007O\u3007\u3007O8;
    public static final Type \u3007o0O;
    public static final Type \u3007\u300708O;
    public static final Type \u3007\u3007o\u3007;
    public static final Type \u3007\u3007\u30070o\u3007\u30070;
    private Type O8o08O8O;
    private final int OO;
    private final String o0;
    private Type o\u300700O;
    private Type \u3007080OO8\u30070;
    private String \u300708O\u300700\u3007o;
    private final int \u3007OOo8\u30070;
    
    static {
        \u30070O = new ConcurrentHashMap<String, Type>(10000, 0.75f);
        final Type type = oOo\u30078o008 = new Type("Z", 1);
        final Type type2 = oOo0 = new Type("B", 2);
        final Type type3 = OO\u300700\u30078oO = new Type("C", 3);
        final Type type4 = o8\u3007OO0\u30070o = new Type("D", 4);
        final Type type5 = \u30078\u3007oO\u3007\u30078o = new Type("F", 5);
        final Type type6 = ooo0\u3007\u3007O = new Type("I", 6);
        final Type type7 = \u3007\u300708O = new Type("J", 7);
        final Type type8 = O0O = new Type("S", 8);
        o8oOOo = new Type("V", 0);
        \u3007O\u3007\u3007O8 = new Type("<null>", 9);
        \u3007o0O = new Type("<addr>", 10);
        O88O = new Type("Ljava/lang/annotation/Annotation;", 9);
        oOO\u3007\u3007 = new Type("Ljava/lang/Class;", 9);
        o8o = new Type("Ljava/lang/Cloneable;", 9);
        oo8ooo8O = new Type("Ljava/lang/invoke/MethodHandle;", 9);
        o\u3007oO = new Type("Ljava/lang/invoke/MethodType;", 9);
        \u300708\u3007o0O = new Type("Ljava/lang/invoke/VarHandle;", 9);
        final Type type9 = \u3007\u3007o\u3007 = new Type("Ljava/lang/Object;", 9);
        Oo80 = new Type("Ljava/io/Serializable;", 9);
        O\u3007o88o08\u3007 = new Type("Ljava/lang/String;", 9);
        \u300700O0 = new Type("Ljava/lang/Throwable;", 9);
        O\u300708oOOO0 = new Type("Ljava/lang/Boolean;", 9);
        o8\u3007OO = new Type("Ljava/lang/Byte;", 9);
        Ooo08 = new Type("Ljava/lang/Character;", 9);
        \u3007OO8ooO8\u3007 = new Type("Ljava/lang/Double;", 9);
        ooO = new Type("Ljava/lang/Float;", 9);
        \u3007OO\u300700\u30070O = new Type("Ljava/lang/Integer;", 9);
        Oo0\u3007Ooo = new Type("Ljava/lang/Long;", 9);
        \u3007\u3007\u30070o\u3007\u30070 = new Type("Ljava/lang/Short;", 9);
        oO\u30078O8oOo = new Type("Ljava/lang/Void;", 9);
        final StringBuilder sb = new StringBuilder();
        sb.append("[");
        sb.append(type.o0);
        \u30070O\u3007O00O = new Type(sb.toString(), 9);
        final StringBuilder sb2 = new StringBuilder();
        sb2.append("[");
        sb2.append(type2.o0);
        o0OoOOo0 = new Type(sb2.toString(), 9);
        final StringBuilder sb3 = new StringBuilder();
        sb3.append("[");
        sb3.append(type3.o0);
        o\u3007o\u3007Oo88 = new Type(sb3.toString(), 9);
        final StringBuilder sb4 = new StringBuilder();
        sb4.append("[");
        sb4.append(type4.o0);
        oO00\u3007o = new Type(sb4.toString(), 9);
        final StringBuilder sb5 = new StringBuilder();
        sb5.append("[");
        sb5.append(type5.o0);
        oOO0880O = new Type(sb5.toString(), 9);
        final StringBuilder sb6 = new StringBuilder();
        sb6.append("[");
        sb6.append(type6.o0);
        oOoo80oO = new Type(sb6.toString(), 9);
        final StringBuilder sb7 = new StringBuilder();
        sb7.append("[");
        sb7.append(type7.o0);
        Oo0O0o8 = new Type(sb7.toString(), 9);
        final StringBuilder sb8 = new StringBuilder();
        sb8.append("[");
        sb8.append(type9.o0);
        OO\u3007OOo = new Type(sb8.toString(), 9);
        final StringBuilder sb9 = new StringBuilder();
        sb9.append("[");
        sb9.append(type8.o0);
        \u3007800OO\u30070O = new Type(sb9.toString(), 9);
        \u300780\u3007808\u3007O();
    }
    
    private Type(final String s, final int n) {
        this(s, n, -1);
    }
    
    private Type(final String o0, final int \u3007oOo8\u30070, final int oo) {
        if (o0 == null) {
            throw new NullPointerException("descriptor == null");
        }
        if (\u3007oOo8\u30070 < 0 || \u3007oOo8\u30070 >= 11) {
            throw new IllegalArgumentException("bad basicType");
        }
        if (oo >= -1) {
            this.o0 = o0;
            this.\u3007OOo8\u30070 = \u3007oOo8\u30070;
            this.OO = oo;
            this.o\u300700O = null;
            this.O8o08O8O = null;
            this.\u3007080OO8\u30070 = null;
            return;
        }
        throw new IllegalArgumentException("newAt < -1");
    }
    
    public static Type OO0o\u3007\u3007\u3007\u30070(final String str) {
        final Type type = Type.\u30070O.get(str);
        if (type != null) {
            return type;
        }
        try {
            final char char1 = str.charAt(0);
            if (char1 == '[') {
                return OO0o\u3007\u3007\u3007\u30070(str.substring(1)).O8();
            }
            final int length = str.length();
            if (char1 == 'L') {
                final int index = length - 1;
                if (str.charAt(index) == ';') {
                    int i = 1;
                    while (i < index) {
                        final char char2 = str.charAt(i);
                        Label_0189: {
                            if (char2 != '(' && char2 != ')' && char2 != '.') {
                                if (char2 != '/') {
                                    if (char2 == ';' || char2 == '[') {
                                        break Label_0189;
                                    }
                                }
                                else if (i == 1 || i == index || str.charAt(i - 1) == '/') {
                                    final StringBuilder sb = new StringBuilder();
                                    sb.append("bad descriptor: ");
                                    sb.append(str);
                                    throw new IllegalArgumentException(sb.toString());
                                }
                                ++i;
                                continue;
                            }
                        }
                        final StringBuilder sb2 = new StringBuilder();
                        sb2.append("bad descriptor: ");
                        sb2.append(str);
                        throw new IllegalArgumentException(sb2.toString());
                    }
                    return \u3007O\u3007(new Type(str, 9));
                }
            }
            final StringBuilder sb3 = new StringBuilder();
            sb3.append("bad descriptor: ");
            sb3.append(str);
            throw new IllegalArgumentException(sb3.toString());
        }
        catch (final NullPointerException ex) {
            throw new NullPointerException("descriptor == null");
        }
        catch (final IndexOutOfBoundsException ex2) {
            throw new IllegalArgumentException("descriptor is empty");
        }
    }
    
    private static void \u300780\u3007808\u3007O() {
        \u3007O\u3007(Type.oOo\u30078o008);
        \u3007O\u3007(Type.oOo0);
        \u3007O\u3007(Type.OO\u300700\u30078oO);
        \u3007O\u3007(Type.o8\u3007OO0\u30070o);
        \u3007O\u3007(Type.\u30078\u3007oO\u3007\u30078o);
        \u3007O\u3007(Type.ooo0\u3007\u3007O);
        \u3007O\u3007(Type.\u3007\u300708O);
        \u3007O\u3007(Type.O0O);
        \u3007O\u3007(Type.O88O);
        \u3007O\u3007(Type.oOO\u3007\u3007);
        \u3007O\u3007(Type.o8o);
        \u3007O\u3007(Type.oo8ooo8O);
        \u3007O\u3007(Type.\u300708\u3007o0O);
        \u3007O\u3007(Type.\u3007\u3007o\u3007);
        \u3007O\u3007(Type.Oo80);
        \u3007O\u3007(Type.O\u3007o88o08\u3007);
        \u3007O\u3007(Type.\u300700O0);
        \u3007O\u3007(Type.O\u300708oOOO0);
        \u3007O\u3007(Type.o8\u3007OO);
        \u3007O\u3007(Type.Ooo08);
        \u3007O\u3007(Type.\u3007OO8ooO8\u3007);
        \u3007O\u3007(Type.ooO);
        \u3007O\u3007(Type.\u3007OO\u300700\u30070O);
        \u3007O\u3007(Type.Oo0\u3007Ooo);
        \u3007O\u3007(Type.\u3007\u3007\u30070o\u3007\u30070);
        \u3007O\u3007(Type.oO\u30078O8oOo);
        \u3007O\u3007(Type.\u30070O\u3007O00O);
        \u3007O\u3007(Type.o0OoOOo0);
        \u3007O\u3007(Type.o\u3007o\u3007Oo88);
        \u3007O\u3007(Type.oO00\u3007o);
        \u3007O\u3007(Type.oOO0880O);
        \u3007O\u3007(Type.oOoo80oO);
        \u3007O\u3007(Type.Oo0O0o8);
        \u3007O\u3007(Type.OO\u3007OOo);
        \u3007O\u3007(Type.\u3007800OO\u30070O);
    }
    
    public static Type \u30078o8o\u3007(final String s) {
        try {
            if (s.equals("V")) {
                return Type.o8oOOo;
            }
            return OO0o\u3007\u3007\u3007\u30070(s);
        }
        catch (final NullPointerException ex) {
            throw new NullPointerException("descriptor == null");
        }
    }
    
    private static Type \u3007O\u3007(Type type) {
        final Type type2 = Type.\u30070O.putIfAbsent(type.oO80(), type);
        if (type2 != null) {
            type = type2;
        }
        return type;
    }
    
    public Type O8() {
        if (this.o\u300700O == null) {
            final StringBuilder sb = new StringBuilder();
            sb.append('[');
            sb.append(this.o0);
            this.o\u300700O = \u3007O\u3007(new Type(sb.toString(), 9));
        }
        return this.o\u300700O;
    }
    
    public boolean OO0o\u3007\u3007() {
        final int \u3007oOo8\u30070 = this.\u3007OOo8\u30070;
        return \u3007oOo8\u30070 == 4 || \u3007oOo8\u30070 == 7;
    }
    
    public int Oo08() {
        final int \u3007oOo8\u30070 = this.\u3007OOo8\u30070;
        if (\u3007oOo8\u30070 != 4 && \u3007oOo8\u30070 != 7) {
            return 1;
        }
        return 2;
    }
    
    public boolean Oooo8o0\u3007() {
        final int \u3007oOo8\u30070 = this.\u3007OOo8\u30070;
        return \u3007oOo8\u30070 == 1 || \u3007oOo8\u30070 == 2 || \u3007oOo8\u30070 == 3 || \u3007oOo8\u30070 == 6 || \u3007oOo8\u30070 == 8;
    }
    
    @Override
    public boolean equals(final Object o) {
        return this == o || (o instanceof Type && this.o0.equals(((Type)o).o0));
    }
    
    @Override
    public Type getType() {
        return this;
    }
    
    @Override
    public int hashCode() {
        return this.o0.hashCode();
    }
    
    public String oO80() {
        return this.o0;
    }
    
    public String o\u30070() {
        if (this.\u300708O\u300700\u3007o == null) {
            if (!this.\u3007\u3007808\u3007()) {
                final StringBuilder sb = new StringBuilder();
                sb.append("not an object type: ");
                sb.append(this.o0);
                throw new IllegalArgumentException(sb.toString());
            }
            if (this.o0.charAt(0) == '[') {
                this.\u300708O\u300700\u3007o = this.o0;
            }
            else {
                final String o0 = this.o0;
                this.\u300708O\u300700\u3007o = o0.substring(1, o0.length() - 1);
            }
        }
        return this.\u300708O\u300700\u3007o;
    }
    
    @Override
    public String toHuman() {
        switch (this.\u3007OOo8\u30070) {
            default: {
                return this.o0;
            }
            case 9: {
                if (this.\u3007O8o08O()) {
                    final StringBuilder sb = new StringBuilder();
                    sb.append(this.\u3007\u3007888().toHuman());
                    sb.append("[]");
                    return sb.toString();
                }
                return this.o\u30070().replace("/", ".");
            }
            case 8: {
                return "short";
            }
            case 7: {
                return "long";
            }
            case 6: {
                return "int";
            }
            case 5: {
                return "float";
            }
            case 4: {
                return "double";
            }
            case 3: {
                return "char";
            }
            case 2: {
                return "byte";
            }
            case 1: {
                return "boolean";
            }
            case 0: {
                return "void";
            }
        }
    }
    
    @Override
    public String toString() {
        return this.o0;
    }
    
    @Override
    public int \u3007080() {
        final int \u3007oOo8\u30070 = this.\u3007OOo8\u30070;
        if (\u3007oOo8\u30070 != 1 && \u3007oOo8\u30070 != 2 && \u3007oOo8\u30070 != 3 && \u3007oOo8\u30070 != 6 && \u3007oOo8\u30070 != 8) {
            return \u3007oOo8\u30070;
        }
        return 6;
    }
    
    public boolean \u3007O8o08O() {
        final String o0 = this.o0;
        boolean b = false;
        if (o0.charAt(0) == '[') {
            b = true;
        }
        return b;
    }
    
    @Override
    public int \u3007o00\u3007\u3007Oo() {
        return this.\u3007OOo8\u30070;
    }
    
    public int \u3007o\u3007(final Type type) {
        return this.o0.compareTo(type.o0);
    }
    
    public boolean \u3007\u3007808\u3007() {
        return this.\u3007OOo8\u30070 == 9;
    }
    
    public Type \u3007\u3007888() {
        if (this.O8o08O8O == null) {
            if (this.o0.charAt(0) != '[') {
                final StringBuilder sb = new StringBuilder();
                sb.append("not an array type: ");
                sb.append(this.o0);
                throw new IllegalArgumentException(sb.toString());
            }
            this.O8o08O8O = OO0o\u3007\u3007\u3007\u30070(this.o0.substring(1));
        }
        return this.O8o08O8O;
    }
}

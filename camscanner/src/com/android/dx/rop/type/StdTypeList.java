// 
// Decompiled by Procyon v0.6.0
// 

package com.android.dx.rop.type;

import com.android.dx.util.FixedSizeList;

public final class StdTypeList extends FixedSizeList implements TypeList
{
    public static final StdTypeList O0O;
    public static final StdTypeList O88O;
    public static final StdTypeList O8o08O8O;
    public static final StdTypeList OO;
    public static final StdTypeList OO\u300700\u30078oO;
    public static final StdTypeList Oo0\u3007Ooo;
    public static final StdTypeList Oo80;
    public static final StdTypeList Ooo08;
    public static final StdTypeList O\u300708oOOO0;
    public static final StdTypeList O\u3007o88o08\u3007;
    public static final StdTypeList o8o;
    public static final StdTypeList o8oOOo;
    public static final StdTypeList o8\u3007OO;
    public static final StdTypeList o8\u3007OO0\u30070o;
    public static final StdTypeList oOO\u3007\u3007;
    public static final StdTypeList oOo0;
    public static final StdTypeList oOo\u30078o008;
    public static final StdTypeList oO\u30078O8oOo;
    public static final StdTypeList oo8ooo8O;
    public static final StdTypeList ooO;
    public static final StdTypeList ooo0\u3007\u3007O;
    public static final StdTypeList o\u300700O;
    public static final StdTypeList o\u3007oO;
    public static final StdTypeList \u300700O0;
    public static final StdTypeList \u3007080OO8\u30070;
    public static final StdTypeList \u300708O\u300700\u3007o;
    public static final StdTypeList \u300708\u3007o0O;
    public static final StdTypeList \u30070O;
    public static final StdTypeList \u30078\u3007oO\u3007\u30078o;
    public static final StdTypeList \u3007OO8ooO8\u3007;
    public static final StdTypeList \u3007OO\u300700\u30070O;
    public static final StdTypeList \u3007O\u3007\u3007O8;
    public static final StdTypeList \u3007o0O;
    public static final StdTypeList \u3007\u300708O;
    public static final StdTypeList \u3007\u3007o\u3007;
    public static final StdTypeList \u3007\u3007\u30070o\u3007\u30070;
    
    static {
        OO = new StdTypeList(0);
        final Type ooo0\u3007\u3007O2 = Type.ooo0\u3007\u3007O;
        \u300708O\u300700\u3007o = \u30070\u3007O0088o(ooo0\u3007\u3007O2);
        final Type \u3007\u300708O2 = Type.\u3007\u300708O;
        o\u300700O = \u30070\u3007O0088o(\u3007\u300708O2);
        final Type \u30078\u3007oO\u3007\u30078o2 = Type.\u30078\u3007oO\u3007\u30078o;
        O8o08O8O = \u30070\u3007O0088o(\u30078\u3007oO\u3007\u30078o2);
        final Type o8\u3007OO0\u30070o2 = Type.o8\u3007OO0\u30070o;
        \u3007080OO8\u30070 = \u30070\u3007O0088o(o8\u3007OO0\u30070o2);
        final Type \u3007\u3007o\u30072 = Type.\u3007\u3007o\u3007;
        \u30070O = \u30070\u3007O0088o(\u3007\u3007o\u30072);
        oOo\u30078o008 = \u30070\u3007O0088o(Type.\u3007o0O);
        oOo0 = \u30070\u3007O0088o(Type.\u300700O0);
        OO\u300700\u30078oO = OoO8(ooo0\u3007\u3007O2, ooo0\u3007\u3007O2);
        o8\u3007OO0\u30070o = OoO8(\u3007\u300708O2, \u3007\u300708O2);
        \u30078\u3007oO\u3007\u30078o = OoO8(\u30078\u3007oO\u3007\u30078o2, \u30078\u3007oO\u3007\u30078o2);
        ooo0\u3007\u3007O = OoO8(o8\u3007OO0\u30070o2, o8\u3007OO0\u30070o2);
        \u3007\u300708O = OoO8(\u3007\u3007o\u30072, \u3007\u3007o\u30072);
        O0O = OoO8(ooo0\u3007\u3007O2, \u3007\u3007o\u30072);
        o8oOOo = OoO8(\u3007\u300708O2, \u3007\u3007o\u30072);
        \u3007O\u3007\u3007O8 = OoO8(\u30078\u3007oO\u3007\u30078o2, \u3007\u3007o\u30072);
        \u3007o0O = OoO8(o8\u3007OO0\u30070o2, \u3007\u3007o\u30072);
        O88O = OoO8(\u3007\u300708O2, ooo0\u3007\u3007O2);
        final Type oOoo80oO = Type.oOoo80oO;
        oOO\u3007\u3007 = OoO8(oOoo80oO, ooo0\u3007\u3007O2);
        final Type oo0O0o8 = Type.Oo0O0o8;
        o8o = OoO8(oo0O0o8, ooo0\u3007\u3007O2);
        final Type ooo0880O = Type.oOO0880O;
        oo8ooo8O = OoO8(ooo0880O, ooo0\u3007\u3007O2);
        final Type oo00\u3007o = Type.oO00\u3007o;
        o\u3007oO = OoO8(oo00\u3007o, ooo0\u3007\u3007O2);
        final Type oo\u3007OOo = Type.OO\u3007OOo;
        \u300708\u3007o0O = OoO8(oo\u3007OOo, ooo0\u3007\u3007O2);
        final Type \u30070O\u3007O00O = Type.\u30070O\u3007O00O;
        \u3007\u3007o\u3007 = OoO8(\u30070O\u3007O00O, ooo0\u3007\u3007O2);
        final Type o0OoOOo0 = Type.o0OoOOo0;
        Oo80 = OoO8(o0OoOOo0, ooo0\u3007\u3007O2);
        final Type o\u3007o\u3007Oo88 = Type.o\u3007o\u3007Oo88;
        O\u3007o88o08\u3007 = OoO8(o\u3007o\u3007Oo88, ooo0\u3007\u3007O2);
        final Type \u3007800OO\u30070O = Type.\u3007800OO\u30070O;
        \u300700O0 = OoO8(\u3007800OO\u30070O, ooo0\u3007\u3007O2);
        O\u300708oOOO0 = o800o8O(ooo0\u3007\u3007O2, oOoo80oO, ooo0\u3007\u3007O2);
        o8\u3007OO = o800o8O(\u3007\u300708O2, oo0O0o8, ooo0\u3007\u3007O2);
        Ooo08 = o800o8O(\u30078\u3007oO\u3007\u30078o2, ooo0880O, ooo0\u3007\u3007O2);
        \u3007OO8ooO8\u3007 = o800o8O(o8\u3007OO0\u30070o2, oo00\u3007o, ooo0\u3007\u3007O2);
        ooO = o800o8O(\u3007\u3007o\u30072, oo\u3007OOo, ooo0\u3007\u3007O2);
        \u3007OO\u300700\u30070O = o800o8O(ooo0\u3007\u3007O2, \u30070O\u3007O00O, ooo0\u3007\u3007O2);
        Oo0\u3007Ooo = o800o8O(ooo0\u3007\u3007O2, o0OoOOo0, ooo0\u3007\u3007O2);
        \u3007\u3007\u30070o\u3007\u30070 = o800o8O(ooo0\u3007\u3007O2, o\u3007o\u3007Oo88, ooo0\u3007\u3007O2);
        oO\u30078O8oOo = o800o8O(ooo0\u3007\u3007O2, \u3007800OO\u30070O, ooo0\u3007\u3007O2);
    }
    
    public StdTypeList(final int n) {
        super(n);
    }
    
    public static StdTypeList OoO8(final Type type, final Type type2) {
        final StdTypeList list = new StdTypeList(2);
        list.oo88o8O(0, type);
        list.oo88o8O(1, type2);
        return list;
    }
    
    public static StdTypeList o800o8O(final Type type, final Type type2, final Type type3) {
        final StdTypeList list = new StdTypeList(3);
        list.oo88o8O(0, type);
        list.oo88o8O(1, type2);
        list.oo88o8O(2, type3);
        return list;
    }
    
    public static StdTypeList \u30070\u3007O0088o(final Type type) {
        final StdTypeList list = new StdTypeList(1);
        list.oo88o8O(0, type);
        return list;
    }
    
    public static StdTypeList \u3007O888o0o(final Type type, final Type type2, final Type type3, final Type type4) {
        final StdTypeList list = new StdTypeList(4);
        list.oo88o8O(0, type);
        list.oo88o8O(1, type2);
        list.oo88o8O(2, type3);
        list.oo88o8O(3, type4);
        return list;
    }
    
    public static String \u3007oo\u3007(final TypeList list) {
        final int size = list.size();
        if (size == 0) {
            return "<empty>";
        }
        final StringBuilder sb = new StringBuilder(100);
        for (int i = 0; i < size; ++i) {
            if (i != 0) {
                sb.append(", ");
            }
            sb.append(list.getType(i).toHuman());
        }
        return sb.toString();
    }
    
    public static int \u3007\u3007808\u3007(final TypeList list, final TypeList list2) {
        final int size = list.size();
        final int size2 = list2.size();
        for (int min = Math.min(size, size2), i = 0; i < min; ++i) {
            final int \u3007o\u3007 = list.getType(i).\u3007o\u3007(list2.getType(i));
            if (\u3007o\u3007 != 0) {
                return \u3007o\u3007;
            }
        }
        if (size == size2) {
            return 0;
        }
        if (size < size2) {
            return -1;
        }
        return 1;
    }
    
    public static int \u3007\u30078O0\u30078(final TypeList list) {
        final int size = list.size();
        int i = 0;
        int n = 0;
        while (i < size) {
            n = n * 31 + list.getType(i).hashCode();
            ++i;
        }
        return n;
    }
    
    @Override
    public Type getType(final int n) {
        return this.\u3007O\u3007(n);
    }
    
    public void oo88o8O(final int n, final Type type) {
        this.OO0o\u3007\u3007\u3007\u30070(n, type);
    }
    
    public StdTypeList o\u3007O8\u3007\u3007o(final Type type) {
        final int size = this.size();
        final StdTypeList list = new StdTypeList(size + 1);
        int i = 0;
        list.OO0o\u3007\u3007\u3007\u30070(0, type);
        while (i < size) {
            final int n = i + 1;
            list.OO0o\u3007\u3007\u3007\u30070(n, this.\u300780\u3007808\u3007O(i));
            i = n;
        }
        return list;
    }
    
    public int \u3007O00() {
        final int size = this.size();
        int i = 0;
        int n = 0;
        while (i < size) {
            n += this.\u3007O\u3007(i).Oo08();
            ++i;
        }
        return n;
    }
    
    public Type \u3007O\u3007(final int n) {
        return (Type)this.oO80(n);
    }
}

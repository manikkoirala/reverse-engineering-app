// 
// Decompiled by Procyon v0.6.0
// 

package com.android.dx.rop.type;

import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

public final class Prototype implements Comparable<Prototype>
{
    private static final ConcurrentMap<String, Prototype> o\u300700O;
    private final StdTypeList OO;
    private final String o0;
    private StdTypeList \u300708O\u300700\u3007o;
    private final Type \u3007OOo8\u30070;
    
    static {
        o\u300700O = new ConcurrentHashMap<String, Prototype>(10000, 0.75f);
    }
    
    private Prototype(final String o0, final Type \u3007oOo8\u30070, final StdTypeList oo) {
        if (o0 == null) {
            throw new NullPointerException("descriptor == null");
        }
        if (\u3007oOo8\u30070 == null) {
            throw new NullPointerException("returnType == null");
        }
        if (oo != null) {
            this.o0 = o0;
            this.\u3007OOo8\u30070 = \u3007oOo8\u30070;
            this.OO = oo;
            this.\u300708O\u300700\u3007o = null;
            return;
        }
        throw new NullPointerException("parameterTypes == null");
    }
    
    private static Type[] oO80(final String s) {
        final int length = s.length();
        final int n = 0;
        if (s.charAt(0) != '(') {
            throw new IllegalArgumentException("bad descriptor");
        }
        int index = 1;
        int n2 = 0;
        int n3;
        while (true) {
            n3 = n;
            if (index >= length) {
                break;
            }
            final char char1 = s.charAt(index);
            if (char1 == ')') {
                n3 = index;
                break;
            }
            int n4 = n2;
            if (char1 >= 'A') {
                n4 = n2;
                if (char1 <= 'Z') {
                    n4 = n2 + 1;
                }
            }
            ++index;
            n2 = n4;
        }
        if (n3 == 0 || n3 == length - 1) {
            throw new IllegalArgumentException("bad descriptor");
        }
        if (s.indexOf(41, n3 + 1) == -1) {
            return new Type[n2];
        }
        throw new IllegalArgumentException("bad descriptor");
    }
    
    private static Prototype \u300780\u3007808\u3007O(Prototype prototype) {
        final Prototype prototype2 = Prototype.o\u300700O.putIfAbsent(prototype.\u3007o\u3007(), prototype);
        if (prototype2 != null) {
            prototype = prototype2;
        }
        return prototype;
    }
    
    public static Prototype \u3007o00\u3007\u3007Oo(final String s) {
        final Prototype prototype = Prototype.o\u300700O.get(s);
        if (prototype != null) {
            return prototype;
        }
        final Type[] oo80 = oO80(s);
        final int n = 0;
        int n2 = 1;
        int n3 = 0;
        while (true) {
            char c = s.charAt(n2);
            if (c == ')') {
                final Type \u30078o8o\u3007 = Type.\u30078o8o\u3007(s.substring(n2 + 1));
                final StdTypeList list = new StdTypeList(n3);
                for (int i = n; i < n3; ++i) {
                    list.oo88o8O(i, oo80[i]);
                }
                return new Prototype(s, \u30078o8o\u3007, list);
            }
            int n4;
            for (n4 = n2; c == '['; c = s.charAt(n4)) {
                ++n4;
            }
            int index;
            if (c == 'L') {
                index = s.indexOf(59, n4);
                if (index == -1) {
                    throw new IllegalArgumentException("bad descriptor");
                }
                ++index;
            }
            else {
                index = n4 + 1;
            }
            oo80[n3] = Type.OO0o\u3007\u3007\u3007\u30070(s.substring(n2, index));
            ++n3;
            n2 = index;
        }
    }
    
    public static Prototype \u3007\u3007888(final String s) {
        if (s == null) {
            throw new NullPointerException("descriptor == null");
        }
        final Prototype prototype = Prototype.o\u300700O.get(s);
        if (prototype != null) {
            return prototype;
        }
        return \u300780\u3007808\u3007O(\u3007o00\u3007\u3007Oo(s));
    }
    
    public StdTypeList O8() {
        if (this.\u300708O\u300700\u3007o == null) {
            final int size = this.OO.size();
            final StdTypeList list = new StdTypeList(size);
            int i = 0;
            boolean b = false;
            while (i < size) {
                Type type;
                if ((type = this.OO.\u3007O\u3007(i)).Oooo8o0\u3007()) {
                    type = Type.ooo0\u3007\u3007O;
                    b = true;
                }
                list.oo88o8O(i, type);
                ++i;
            }
            StdTypeList oo;
            if (b) {
                oo = list;
            }
            else {
                oo = this.OO;
            }
            this.\u300708O\u300700\u3007o = oo;
        }
        return this.\u300708O\u300700\u3007o;
    }
    
    public Prototype OO0o\u3007\u3007\u3007\u30070(final Type type) {
        final StringBuilder sb = new StringBuilder();
        sb.append("(");
        sb.append(type.oO80());
        sb.append(this.o0.substring(1));
        final String string = sb.toString();
        final StdTypeList o\u3007O8\u3007\u3007o = this.OO.o\u3007O8\u3007\u3007o(type);
        o\u3007O8\u3007\u3007o.Oo08();
        return \u300780\u3007808\u3007O(new Prototype(string, this.\u3007OOo8\u30070, o\u3007O8\u3007\u3007o));
    }
    
    public StdTypeList Oo08() {
        return this.OO;
    }
    
    @Override
    public boolean equals(final Object o) {
        return this == o || (o instanceof Prototype && this.o0.equals(((Prototype)o).o0));
    }
    
    @Override
    public int hashCode() {
        return this.o0.hashCode();
    }
    
    public Type o\u30070() {
        return this.\u3007OOo8\u30070;
    }
    
    @Override
    public String toString() {
        return this.o0;
    }
    
    public int \u3007080(final Prototype prototype) {
        if (this == prototype) {
            return 0;
        }
        final int \u3007o\u3007 = this.\u3007OOo8\u30070.\u3007o\u3007(prototype.\u3007OOo8\u30070);
        if (\u3007o\u3007 != 0) {
            return \u3007o\u3007;
        }
        final int size = this.OO.size();
        final int size2 = prototype.OO.size();
        for (int min = Math.min(size, size2), i = 0; i < min; ++i) {
            final int \u3007o\u30072 = this.OO.\u3007O\u3007(i).\u3007o\u3007(prototype.OO.\u3007O\u3007(i));
            if (\u3007o\u30072 != 0) {
                return \u3007o\u30072;
            }
        }
        if (size < size2) {
            return -1;
        }
        if (size > size2) {
            return 1;
        }
        return 0;
    }
    
    public String \u3007o\u3007() {
        return this.o0;
    }
}

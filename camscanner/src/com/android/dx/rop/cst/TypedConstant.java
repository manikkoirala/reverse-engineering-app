// 
// Decompiled by Procyon v0.6.0
// 

package com.android.dx.rop.cst;

import com.android.dx.rop.type.Type;
import com.android.dx.rop.type.TypeBearer;

public abstract class TypedConstant extends Constant implements TypeBearer
{
    @Override
    public abstract /* synthetic */ Type getType();
    
    @Override
    public final int \u3007080() {
        return this.getType().\u3007080();
    }
    
    @Override
    public final int \u3007o00\u3007\u3007Oo() {
        return this.getType().\u3007o00\u3007\u3007Oo();
    }
}

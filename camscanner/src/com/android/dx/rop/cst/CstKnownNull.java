// 
// Decompiled by Procyon v0.6.0
// 

package com.android.dx.rop.cst;

import com.android.dx.rop.type.Type;

public final class CstKnownNull extends CstLiteralBits
{
    public static final CstKnownNull o0;
    
    static {
        o0 = new CstKnownNull();
    }
    
    private CstKnownNull() {
    }
    
    @Override
    protected int O8(final Constant constant) {
        return 0;
    }
    
    @Override
    public String Oo08() {
        return "known-null";
    }
    
    @Override
    public boolean equals(final Object o) {
        return o instanceof CstKnownNull;
    }
    
    @Override
    public Type getType() {
        return Type.\u3007O\u3007\u3007O8;
    }
    
    @Override
    public int hashCode() {
        return 1147565434;
    }
    
    @Override
    public long oO80() {
        return 0L;
    }
    
    @Override
    public boolean o\u30070() {
        return true;
    }
    
    @Override
    public String toHuman() {
        return "null";
    }
    
    @Override
    public String toString() {
        return "known-null";
    }
    
    @Override
    public int \u3007\u3007888() {
        return 0;
    }
}

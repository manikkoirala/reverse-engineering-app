// 
// Decompiled by Procyon v0.6.0
// 

package com.android.dx.rop.cst;

import java.util.concurrent.ConcurrentHashMap;
import com.android.dx.rop.type.Type;
import java.util.concurrent.ConcurrentMap;

public final class CstType extends TypedConstant
{
    public static final CstType O0O;
    public static final CstType O88O;
    public static final CstType O8o08O8O;
    private static final ConcurrentMap<Type, CstType> OO;
    public static final CstType OO\u300700\u30078oO;
    public static final CstType o8o;
    public static final CstType o8oOOo;
    public static final CstType o8\u3007OO0\u30070o;
    public static final CstType oOO\u3007\u3007;
    public static final CstType oOo0;
    public static final CstType oOo\u30078o008;
    public static final CstType oo8ooo8O;
    public static final CstType ooo0\u3007\u3007O;
    public static final CstType o\u300700O;
    public static final CstType \u3007080OO8\u30070;
    public static final CstType \u300708O\u300700\u3007o;
    public static final CstType \u30070O;
    public static final CstType \u30078\u3007oO\u3007\u30078o;
    public static final CstType \u3007O\u3007\u3007O8;
    public static final CstType \u3007o0O;
    public static final CstType \u3007\u300708O;
    private final Type o0;
    private CstString \u3007OOo8\u30070;
    
    static {
        OO = new ConcurrentHashMap<Type, CstType>(1000, 0.75f);
        \u300708O\u300700\u3007o = new CstType(Type.\u3007\u3007o\u3007);
        o\u300700O = new CstType(Type.O\u300708oOOO0);
        O8o08O8O = new CstType(Type.o8\u3007OO);
        \u3007080OO8\u30070 = new CstType(Type.Ooo08);
        \u30070O = new CstType(Type.\u3007OO8ooO8\u3007);
        oOo\u30078o008 = new CstType(Type.ooO);
        oOo0 = new CstType(Type.Oo0\u3007Ooo);
        OO\u300700\u30078oO = new CstType(Type.\u3007OO\u300700\u30070O);
        o8\u3007OO0\u30070o = new CstType(Type.\u3007\u3007\u30070o\u3007\u30070);
        \u30078\u3007oO\u3007\u30078o = new CstType(Type.oO\u30078O8oOo);
        ooo0\u3007\u3007O = new CstType(Type.\u30070O\u3007O00O);
        \u3007\u300708O = new CstType(Type.o0OoOOo0);
        O0O = new CstType(Type.o\u3007o\u3007Oo88);
        o8oOOo = new CstType(Type.oO00\u3007o);
        \u3007O\u3007\u3007O8 = new CstType(Type.oOO0880O);
        \u3007o0O = new CstType(Type.Oo0O0o8);
        O88O = new CstType(Type.oOoo80oO);
        oOO\u3007\u3007 = new CstType(Type.\u3007800OO\u30070O);
        o8o = new CstType(Type.oo8ooo8O);
        oo8ooo8O = new CstType(Type.\u300708\u3007o0O);
        \u300780\u3007808\u3007O();
    }
    
    public CstType(final Type o0) {
        if (o0 == null) {
            throw new NullPointerException("type == null");
        }
        if (o0 != Type.\u3007O\u3007\u3007O8) {
            this.o0 = o0;
            this.\u3007OOo8\u30070 = null;
            return;
        }
        throw new UnsupportedOperationException("KNOWN_NULL is not representable");
    }
    
    public static CstType OO0o\u3007\u3007\u3007\u30070(final Type type) {
        final CstType cstType = new CstType(type);
        final CstType cstType2 = CstType.OO.putIfAbsent(type, cstType);
        CstType cstType3 = cstType;
        if (cstType2 != null) {
            cstType3 = cstType2;
        }
        return cstType3;
    }
    
    private static void \u300780\u3007808\u3007O() {
        \u30078o8o\u3007(CstType.\u300708O\u300700\u3007o);
        \u30078o8o\u3007(CstType.o\u300700O);
        \u30078o8o\u3007(CstType.O8o08O8O);
        \u30078o8o\u3007(CstType.\u3007080OO8\u30070);
        \u30078o8o\u3007(CstType.\u30070O);
        \u30078o8o\u3007(CstType.oOo\u30078o008);
        \u30078o8o\u3007(CstType.oOo0);
        \u30078o8o\u3007(CstType.OO\u300700\u30078oO);
        \u30078o8o\u3007(CstType.o8\u3007OO0\u30070o);
        \u30078o8o\u3007(CstType.\u30078\u3007oO\u3007\u30078o);
        \u30078o8o\u3007(CstType.ooo0\u3007\u3007O);
        \u30078o8o\u3007(CstType.\u3007\u300708O);
        \u30078o8o\u3007(CstType.O0O);
        \u30078o8o\u3007(CstType.o8oOOo);
        \u30078o8o\u3007(CstType.\u3007O\u3007\u3007O8);
        \u30078o8o\u3007(CstType.\u3007o0O);
        \u30078o8o\u3007(CstType.O88O);
        \u30078o8o\u3007(CstType.oOO\u3007\u3007);
        \u30078o8o\u3007(CstType.o8o);
    }
    
    private static void \u30078o8o\u3007(final CstType obj) {
        if (CstType.OO.putIfAbsent(obj.o\u30070(), obj) == null) {
            return;
        }
        final StringBuilder sb = new StringBuilder();
        sb.append("Attempted re-init of ");
        sb.append(obj);
        throw new IllegalStateException(sb.toString());
    }
    
    @Override
    protected int O8(final Constant constant) {
        return this.o0.oO80().compareTo(((CstType)constant).o0.oO80());
    }
    
    @Override
    public String Oo08() {
        return "type";
    }
    
    @Override
    public boolean equals(final Object o) {
        final boolean b = o instanceof CstType;
        boolean b2 = false;
        if (!b) {
            return false;
        }
        if (this.o0 == ((CstType)o).o0) {
            b2 = true;
        }
        return b2;
    }
    
    @Override
    public Type getType() {
        return Type.oOO\u3007\u3007;
    }
    
    @Override
    public int hashCode() {
        return this.o0.hashCode();
    }
    
    public String oO80() {
        final String string = this.\u3007\u3007888().getString();
        final int lastIndex = string.lastIndexOf(47);
        final int lastIndex2 = string.lastIndexOf(91);
        if (lastIndex == -1) {
            return "default";
        }
        return string.substring(lastIndex2 + 2, lastIndex).replace('/', '.');
    }
    
    public Type o\u30070() {
        return this.o0;
    }
    
    @Override
    public String toHuman() {
        return this.o0.toHuman();
    }
    
    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append("type{");
        sb.append(this.toHuman());
        sb.append('}');
        return sb.toString();
    }
    
    public CstString \u3007\u3007888() {
        if (this.\u3007OOo8\u30070 == null) {
            this.\u3007OOo8\u30070 = new CstString(this.o0.oO80());
        }
        return this.\u3007OOo8\u30070;
    }
}

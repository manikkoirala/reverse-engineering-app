// 
// Decompiled by Procyon v0.6.0
// 

package com.android.dx.rop.cst;

import com.android.dx.util.ToHuman;

public abstract class Constant implements ToHuman, Comparable<Constant>
{
    protected abstract int O8(final Constant p0);
    
    public abstract String Oo08();
    
    public final int \u3007o\u3007(final Constant constant) {
        final Class<? extends Constant> class1 = this.getClass();
        final Class<? extends Constant> class2 = constant.getClass();
        if (class1 != class2) {
            return class1.getName().compareTo(class2.getName());
        }
        return this.O8(constant);
    }
}

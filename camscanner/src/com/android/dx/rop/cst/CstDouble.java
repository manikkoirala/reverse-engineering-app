// 
// Decompiled by Procyon v0.6.0
// 

package com.android.dx.rop.cst;

import com.android.dx.util.Hex;
import com.android.dx.rop.type.Type;

public final class CstDouble extends CstLiteral64
{
    public static final CstDouble OO;
    public static final CstDouble \u3007OOo8\u30070;
    
    static {
        \u3007OOo8\u30070 = new CstDouble(Double.doubleToLongBits(0.0));
        OO = new CstDouble(Double.doubleToLongBits(1.0));
    }
    
    private CstDouble(final long n) {
        super(n);
    }
    
    public static CstDouble \u300780\u3007808\u3007O(final long n) {
        return new CstDouble(n);
    }
    
    @Override
    public String Oo08() {
        return "double";
    }
    
    @Override
    public Type getType() {
        return Type.o8\u3007OO0\u30070o;
    }
    
    @Override
    public String toHuman() {
        return Double.toString(Double.longBitsToDouble(this.oO80()));
    }
    
    @Override
    public String toString() {
        final long oo80 = this.oO80();
        final StringBuilder sb = new StringBuilder();
        sb.append("double{0x");
        sb.append(Hex.\u300780\u3007808\u3007O(oo80));
        sb.append(" / ");
        sb.append(Double.longBitsToDouble(oo80));
        sb.append('}');
        return sb.toString();
    }
}

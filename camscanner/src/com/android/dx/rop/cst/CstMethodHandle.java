// 
// Decompiled by Procyon v0.6.0
// 

package com.android.dx.rop.cst;

import com.android.dx.rop.type.Type;

public final class CstMethodHandle extends TypedConstant
{
    private static final String[] OO;
    private final int o0;
    private final Constant \u3007OOo8\u30070;
    
    static {
        OO = new String[] { "static-put", "static-get", "instance-put", "instance-get", "invoke-static", "invoke-instance", "invoke-constructor", "invoke-direct", "invoke-interface" };
    }
    
    public static boolean OO0o\u3007\u3007\u3007\u30070(final int n) {
        return n == 0 || n == 1 || n == 2 || n == 3;
    }
    
    public static boolean \u3007O8o08O(final int n) {
        switch (n) {
            default: {
                return false;
            }
            case 4:
            case 5:
            case 6:
            case 7:
            case 8: {
                return true;
            }
        }
    }
    
    public static String \u3007\u3007888(final int n) {
        return CstMethodHandle.OO[n];
    }
    
    @Override
    protected int O8(final Constant constant) {
        final CstMethodHandle cstMethodHandle = (CstMethodHandle)constant;
        if (this.o\u30070() == cstMethodHandle.o\u30070()) {
            return this.oO80().\u3007o\u3007(cstMethodHandle.oO80());
        }
        return Integer.compare(this.o\u30070(), cstMethodHandle.o\u30070());
    }
    
    @Override
    public String Oo08() {
        return "method handle";
    }
    
    @Override
    public Type getType() {
        return Type.oo8ooo8O;
    }
    
    public Constant oO80() {
        return this.\u3007OOo8\u30070;
    }
    
    public int o\u30070() {
        return this.o0;
    }
    
    @Override
    public String toHuman() {
        final StringBuilder sb = new StringBuilder();
        sb.append(\u3007\u3007888(this.o0));
        sb.append(",");
        sb.append(this.\u3007OOo8\u30070.toString());
        return sb.toString();
    }
    
    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append("method-handle{");
        sb.append(this.toHuman());
        sb.append("}");
        return sb.toString();
    }
    
    public boolean \u300780\u3007808\u3007O() {
        return OO0o\u3007\u3007\u3007\u30070(this.o0);
    }
    
    public boolean \u30078o8o\u3007() {
        return \u3007O8o08O(this.o0);
    }
}

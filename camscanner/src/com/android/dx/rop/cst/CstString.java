// 
// Decompiled by Procyon v0.6.0
// 

package com.android.dx.rop.cst;

import com.android.dx.rop.type.Type;
import com.android.dx.util.ByteArray;

public final class CstString extends TypedConstant
{
    public static final CstString OO;
    private final String o0;
    private final ByteArray \u3007OOo8\u30070;
    
    static {
        OO = new CstString("");
    }
    
    public CstString(final String s) {
        if (s != null) {
            this.o0 = s.intern();
            this.\u3007OOo8\u30070 = new ByteArray(\u300780\u3007808\u3007O(s));
            return;
        }
        throw new NullPointerException("string == null");
    }
    
    public static byte[] \u300780\u3007808\u3007O(final String s) {
        final int length = s.length();
        final byte[] array = new byte[length * 3];
        int i = 0;
        int n = 0;
        while (i < length) {
            final char char1 = s.charAt(i);
            if (char1 != '\0' && char1 < '\u0080') {
                array[n] = (byte)char1;
                ++n;
            }
            else if (char1 < '\u0800') {
                array[n] = (byte)((char1 >> 6 & 0x1F) | 0xC0);
                array[n + 1] = (byte)((char1 & '?') | 0x80);
                n += 2;
            }
            else {
                array[n] = (byte)((char1 >> 12 & 0xF) | 0xE0);
                array[n + 1] = (byte)((char1 >> 6 & 0x3F) | 0x80);
                array[n + 2] = (byte)((char1 & '?') | 0x80);
                n += 3;
            }
            ++i;
        }
        final byte[] array2 = new byte[n];
        System.arraycopy(array, 0, array2, 0, n);
        return array2;
    }
    
    @Override
    protected int O8(final Constant constant) {
        return this.o0.compareTo(((CstString)constant).o0);
    }
    
    public String OO0o\u3007\u3007\u3007\u30070() {
        final StringBuilder sb = new StringBuilder();
        sb.append('\"');
        sb.append(this.toHuman());
        sb.append('\"');
        return sb.toString();
    }
    
    @Override
    public String Oo08() {
        return "utf8";
    }
    
    @Override
    public boolean equals(final Object o) {
        return o instanceof CstString && this.o0.equals(((CstString)o).o0);
    }
    
    public String getString() {
        return this.o0;
    }
    
    @Override
    public Type getType() {
        return Type.O\u3007o88o08\u3007;
    }
    
    @Override
    public int hashCode() {
        return this.o0.hashCode();
    }
    
    public int oO80() {
        return this.\u3007OOo8\u30070.\u3007o00\u3007\u3007Oo();
    }
    
    public ByteArray o\u30070() {
        return this.\u3007OOo8\u30070;
    }
    
    @Override
    public String toHuman() {
        final int length = this.o0.length();
        final StringBuilder sb = new StringBuilder(length * 3 / 2);
        for (int i = 0; i < length; ++i) {
            final char char1 = this.o0.charAt(i);
            if (char1 >= ' ' && char1 < '\u007f') {
                if (char1 == '\'' || char1 == '\"' || char1 == '\\') {
                    sb.append('\\');
                }
                sb.append(char1);
            }
            else if (char1 <= '\u007f') {
                if (char1 != '\t') {
                    if (char1 != '\n') {
                        if (char1 != '\r') {
                            char char2;
                            if (i < length - 1) {
                                char2 = this.o0.charAt(i + 1);
                            }
                            else {
                                char2 = '\0';
                            }
                            final boolean b = char2 >= '0' && char2 <= '7';
                            sb.append('\\');
                            int j = 6;
                            int n = b ? 1 : 0;
                            while (j >= 0) {
                                final char c = (char)((char1 >> j & 0x7) + 48);
                                int n2;
                                if (c != '0' || (n2 = n) != 0) {
                                    sb.append(c);
                                    n2 = 1;
                                }
                                j -= 3;
                                n = n2;
                            }
                            if (n == 0) {
                                sb.append('0');
                            }
                        }
                        else {
                            sb.append("\\r");
                        }
                    }
                    else {
                        sb.append("\\n");
                    }
                }
                else {
                    sb.append("\\t");
                }
            }
            else {
                sb.append("\\u");
                sb.append(Character.forDigit(char1 >> 12, 16));
                sb.append(Character.forDigit(char1 >> 8 & 0xF, 16));
                sb.append(Character.forDigit(char1 >> 4 & 0xF, 16));
                sb.append(Character.forDigit(char1 & '\u000f', 16));
            }
        }
        return sb.toString();
    }
    
    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append("string{\"");
        sb.append(this.toHuman());
        sb.append("\"}");
        return sb.toString();
    }
    
    public String \u30078o8o\u3007(final int n) {
        String str = this.toHuman();
        String str2;
        if (str.length() <= n - 2) {
            str2 = "";
        }
        else {
            str = str.substring(0, n - 5);
            str2 = "...";
        }
        final StringBuilder sb = new StringBuilder();
        sb.append('\"');
        sb.append(str);
        sb.append(str2);
        sb.append('\"');
        return sb.toString();
    }
    
    public int \u3007\u3007888() {
        return this.o0.length();
    }
}

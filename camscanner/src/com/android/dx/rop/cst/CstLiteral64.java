// 
// Decompiled by Procyon v0.6.0
// 

package com.android.dx.rop.cst;

import com.android.dx.rop.type.Type;

public abstract class CstLiteral64 extends CstLiteralBits
{
    private final long o0;
    
    CstLiteral64(final long o0) {
        this.o0 = o0;
    }
    
    @Override
    protected int O8(final Constant constant) {
        final long o0 = ((CstLiteral64)constant).o0;
        final long o2 = this.o0;
        if (o2 < o0) {
            return -1;
        }
        if (o2 > o0) {
            return 1;
        }
        return 0;
    }
    
    @Override
    public final boolean equals(final Object o) {
        return o != null && this.getClass() == o.getClass() && this.o0 == ((CstLiteral64)o).o0;
    }
    
    @Override
    public final int hashCode() {
        final long o0 = this.o0;
        return (int)o0 ^ (int)(o0 >> 32);
    }
    
    @Override
    public final long oO80() {
        return this.o0;
    }
    
    @Override
    public final boolean o\u30070() {
        final long o0 = this.o0;
        return (int)o0 == o0;
    }
    
    @Override
    public final int \u3007\u3007888() {
        return (int)this.o0;
    }
}

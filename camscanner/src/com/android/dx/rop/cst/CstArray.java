// 
// Decompiled by Procyon v0.6.0
// 

package com.android.dx.rop.cst;

import com.android.dx.util.FixedSizeList;

public class CstArray extends Constant
{
    private final List o0;
    
    public CstArray(final List o0) {
        if (o0 != null) {
            o0.\u3007\u3007888();
            this.o0 = o0;
            return;
        }
        throw new NullPointerException("list == null");
    }
    
    @Override
    protected int O8(final Constant constant) {
        return this.o0.\u3007\u3007808\u3007(((CstArray)constant).o0);
    }
    
    @Override
    public String Oo08() {
        return "array";
    }
    
    @Override
    public boolean equals(final Object o) {
        return o instanceof CstArray && this.o0.equals(((CstArray)o).o0);
    }
    
    @Override
    public int hashCode() {
        return this.o0.hashCode();
    }
    
    public List o\u30070() {
        return this.o0;
    }
    
    @Override
    public String toHuman() {
        return this.o0.\u3007O8o08O("{", ", ", "}");
    }
    
    @Override
    public String toString() {
        return this.o0.OO0o\u3007\u3007("array{", ", ", "}");
    }
    
    public static final class List extends FixedSizeList implements Comparable<List>
    {
        public List(final int n) {
            super(n);
        }
        
        public void \u3007O00(final int n, final Constant constant) {
            this.OO0o\u3007\u3007\u3007\u30070(n, constant);
        }
        
        public Constant \u3007O\u3007(final int n) {
            return (Constant)this.oO80(n);
        }
        
        public int \u3007\u3007808\u3007(final List list) {
            final int size = this.size();
            final int size2 = list.size();
            int n;
            if (size < size2) {
                n = size;
            }
            else {
                n = size2;
            }
            for (int i = 0; i < n; ++i) {
                final int \u3007o\u3007 = ((Constant)this.oO80(i)).\u3007o\u3007((Constant)list.oO80(i));
                if (\u3007o\u3007 != 0) {
                    return \u3007o\u3007;
                }
            }
            if (size < size2) {
                return -1;
            }
            if (size > size2) {
                return 1;
            }
            return 0;
        }
    }
}

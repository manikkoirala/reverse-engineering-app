// 
// Decompiled by Procyon v0.6.0
// 

package com.android.dx.rop.cst;

import com.android.dx.rop.type.Type;
import com.android.dx.rop.type.Prototype;

public abstract class CstBaseMethodRef extends CstMemberRef
{
    private final Prototype OO;
    private Prototype \u300708O\u300700\u3007o;
    
    CstBaseMethodRef(final CstType cstType, final CstNat cstNat) {
        super(cstType, cstNat);
        final String string = this.\u3007\u3007888().o\u30070().getString();
        if (this.\u30078o8o\u3007()) {
            this.OO = Prototype.\u3007o00\u3007\u3007Oo(string);
        }
        else {
            this.OO = Prototype.\u3007\u3007888(string);
        }
        this.\u300708O\u300700\u3007o = null;
    }
    
    @Override
    protected final int O8(final Constant constant) {
        final int o8 = super.O8(constant);
        if (o8 != 0) {
            return o8;
        }
        return this.OO.\u3007080(((CstBaseMethodRef)constant).OO);
    }
    
    public final Prototype OO0o\u3007\u3007\u3007\u30070(final boolean b) {
        if (b) {
            return this.OO;
        }
        if (this.\u300708O\u300700\u3007o == null) {
            this.\u300708O\u300700\u3007o = this.OO.OO0o\u3007\u3007\u3007\u30070(this.o\u30070().o\u30070());
        }
        return this.\u300708O\u300700\u3007o;
    }
    
    @Override
    public final Type getType() {
        return this.OO.o\u30070();
    }
    
    public final int oO80(final boolean b) {
        return this.OO0o\u3007\u3007\u3007\u30070(b).Oo08().\u3007O00();
    }
    
    public final Prototype \u300780\u3007808\u3007O() {
        return this.OO;
    }
    
    public final boolean \u30078o8o\u3007() {
        final CstType o\u30070 = this.o\u30070();
        if (o\u30070.equals(CstType.o8o)) {
            final String string = this.\u3007\u3007888().oO80().getString();
            string.hashCode();
            if (string.equals("invoke") || string.equals("invokeExact")) {
                return true;
            }
        }
        else if (o\u30070.equals(CstType.oo8ooo8O)) {
            final String string2 = this.\u3007\u3007888().oO80().getString();
            string2.hashCode();
            final int hashCode = string2.hashCode();
            int n = -1;
            switch (hashCode) {
                case 2013994287: {
                    if (!string2.equals("weakCompareAndSetRelease")) {
                        break;
                    }
                    n = 30;
                    break;
                }
                case 2002508693: {
                    if (!string2.equals("getAndSetAcquire")) {
                        break;
                    }
                    n = 29;
                    break;
                }
                case 1483964149: {
                    if (!string2.equals("compareAndExchange")) {
                        break;
                    }
                    n = 28;
                    break;
                }
                case 1352153939: {
                    if (!string2.equals("getAndBitwiseOr")) {
                        break;
                    }
                    n = 27;
                    break;
                }
                case 1245632875: {
                    if (!string2.equals("getAndBitwiseXorAcquire")) {
                        break;
                    }
                    n = 26;
                    break;
                }
                case 937077366: {
                    if (!string2.equals("getAndAddAcquire")) {
                        break;
                    }
                    n = 25;
                    break;
                }
                case 748071969: {
                    if (!string2.equals("compareAndExchangeAcquire")) {
                        break;
                    }
                    n = 24;
                    break;
                }
                case 685319959: {
                    if (!string2.equals("getOpaque")) {
                        break;
                    }
                    n = 23;
                    break;
                }
                case 470702883: {
                    if (!string2.equals("setOpaque")) {
                        break;
                    }
                    n = 22;
                    break;
                }
                case 353422447: {
                    if (!string2.equals("getAndBitwiseAndAcquire")) {
                        break;
                    }
                    n = 21;
                    break;
                }
                case 282724865: {
                    if (!string2.equals("getAndSet")) {
                        break;
                    }
                    n = 20;
                    break;
                }
                case 282707520: {
                    if (!string2.equals("getAndAdd")) {
                        break;
                    }
                    n = 19;
                    break;
                }
                case 189872914: {
                    if (!string2.equals("getVolatile")) {
                        break;
                    }
                    n = 18;
                    break;
                }
                case 101293086: {
                    if (!string2.equals("setVolatile")) {
                        break;
                    }
                    n = 17;
                    break;
                }
                case 93645315: {
                    if (!string2.equals("getAndBitwiseOrAcquire")) {
                        break;
                    }
                    n = 16;
                    break;
                }
                case 113762: {
                    if (!string2.equals("set")) {
                        break;
                    }
                    n = 15;
                    break;
                }
                case 102230: {
                    if (!string2.equals("get")) {
                        break;
                    }
                    n = 14;
                    break;
                }
                case -37641530: {
                    if (!string2.equals("getAndSetRelease")) {
                        break;
                    }
                    n = 13;
                    break;
                }
                case -127361888: {
                    if (!string2.equals("getAcquire")) {
                        break;
                    }
                    n = 12;
                    break;
                }
                case -230706875: {
                    if (!string2.equals("setRelease")) {
                        break;
                    }
                    n = 11;
                    break;
                }
                case -240822786: {
                    if (!string2.equals("weakCompareAndSetAcquire")) {
                        break;
                    }
                    n = 10;
                    break;
                }
                case -567150350: {
                    if (!string2.equals("weakCompareAndSetPlain")) {
                        break;
                    }
                    n = 9;
                    break;
                }
                case -794517348: {
                    if (!string2.equals("getAndBitwiseXorRelease")) {
                        break;
                    }
                    n = 8;
                    break;
                }
                case -1032892181: {
                    if (!string2.equals("getAndBitwiseXor")) {
                        break;
                    }
                    n = 7;
                    break;
                }
                case -1032914329: {
                    if (!string2.equals("getAndBitwiseAnd")) {
                        break;
                    }
                    n = 6;
                    break;
                }
                case -1103072857: {
                    if (!string2.equals("getAndAddRelease")) {
                        break;
                    }
                    n = 5;
                    break;
                }
                case -1117944904: {
                    if (!string2.equals("weakCompareAndSet")) {
                        break;
                    }
                    n = 4;
                    break;
                }
                case -1292078254: {
                    if (!string2.equals("compareAndExchangeRelease")) {
                        break;
                    }
                    n = 3;
                    break;
                }
                case -1671098288: {
                    if (!string2.equals("compareAndSet")) {
                        break;
                    }
                    n = 2;
                    break;
                }
                case -1686727776: {
                    if (!string2.equals("getAndBitwiseAndRelease")) {
                        break;
                    }
                    n = 1;
                    break;
                }
                case -1946504908: {
                    if (!string2.equals("getAndBitwiseOrRelease")) {
                        break;
                    }
                    n = 0;
                    break;
                }
            }
            switch (n) {
                case 0:
                case 1:
                case 2:
                case 3:
                case 4:
                case 5:
                case 6:
                case 7:
                case 8:
                case 9:
                case 10:
                case 11:
                case 12:
                case 13:
                case 14:
                case 15:
                case 16:
                case 17:
                case 18:
                case 19:
                case 20:
                case 21:
                case 22:
                case 23:
                case 24:
                case 25:
                case 26:
                case 27:
                case 28:
                case 29:
                case 30: {
                    return true;
                }
            }
        }
        return false;
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.android.dx.rop.cst;

import com.android.dx.rop.type.Type;

public abstract class CstLiteral32 extends CstLiteralBits
{
    private final int o0;
    
    CstLiteral32(final int o0) {
        this.o0 = o0;
    }
    
    @Override
    protected int O8(final Constant constant) {
        final int o0 = ((CstLiteral32)constant).o0;
        final int o2 = this.o0;
        if (o2 < o0) {
            return -1;
        }
        if (o2 > o0) {
            return 1;
        }
        return 0;
    }
    
    @Override
    public final boolean equals(final Object o) {
        return o != null && this.getClass() == o.getClass() && this.o0 == ((CstLiteral32)o).o0;
    }
    
    @Override
    public final int hashCode() {
        return this.o0;
    }
    
    @Override
    public final long oO80() {
        return this.o0;
    }
    
    @Override
    public final boolean o\u30070() {
        return true;
    }
    
    @Override
    public final int \u3007\u3007888() {
        return this.o0;
    }
}

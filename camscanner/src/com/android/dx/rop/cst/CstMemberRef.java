// 
// Decompiled by Procyon v0.6.0
// 

package com.android.dx.rop.cst;

import com.android.dx.rop.type.Type;

public abstract class CstMemberRef extends TypedConstant
{
    private final CstType o0;
    private final CstNat \u3007OOo8\u30070;
    
    CstMemberRef(final CstType o0, final CstNat \u3007oOo8\u30070) {
        if (o0 == null) {
            throw new NullPointerException("definingClass == null");
        }
        if (\u3007oOo8\u30070 != null) {
            this.o0 = o0;
            this.\u3007OOo8\u30070 = \u3007oOo8\u30070;
            return;
        }
        throw new NullPointerException("nat == null");
    }
    
    @Override
    protected int O8(final Constant constant) {
        final CstMemberRef cstMemberRef = (CstMemberRef)constant;
        final int \u3007o\u3007 = this.o0.\u3007o\u3007(cstMemberRef.o0);
        if (\u3007o\u3007 != 0) {
            return \u3007o\u3007;
        }
        return this.\u3007OOo8\u30070.oO80().\u3007o\u3007(cstMemberRef.\u3007OOo8\u30070.oO80());
    }
    
    @Override
    public final boolean equals(final Object o) {
        boolean b2;
        final boolean b = b2 = false;
        if (o != null) {
            if (this.getClass() != o.getClass()) {
                b2 = b;
            }
            else {
                final CstMemberRef cstMemberRef = (CstMemberRef)o;
                b2 = b;
                if (this.o0.equals(cstMemberRef.o0)) {
                    b2 = b;
                    if (this.\u3007OOo8\u30070.equals(cstMemberRef.\u3007OOo8\u30070)) {
                        b2 = true;
                    }
                }
            }
        }
        return b2;
    }
    
    @Override
    public final int hashCode() {
        return this.o0.hashCode() * 31 ^ this.\u3007OOo8\u30070.hashCode();
    }
    
    public final CstType o\u30070() {
        return this.o0;
    }
    
    @Override
    public final String toHuman() {
        final StringBuilder sb = new StringBuilder();
        sb.append(this.o0.toHuman());
        sb.append('.');
        sb.append(this.\u3007OOo8\u30070.toHuman());
        return sb.toString();
    }
    
    @Override
    public final String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append(this.Oo08());
        sb.append('{');
        sb.append(this.toHuman());
        sb.append('}');
        return sb.toString();
    }
    
    public final CstNat \u3007\u3007888() {
        return this.\u3007OOo8\u30070;
    }
}

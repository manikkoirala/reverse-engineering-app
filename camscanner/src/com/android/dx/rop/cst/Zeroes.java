// 
// Decompiled by Procyon v0.6.0
// 

package com.android.dx.rop.cst;

import com.android.dx.rop.type.Type;

public final class Zeroes
{
    public static Constant \u3007080(final Type type) {
        switch (type.\u3007o00\u3007\u3007Oo()) {
            default: {
                final StringBuilder sb = new StringBuilder();
                sb.append("no zero for type: ");
                sb.append(type.toHuman());
                throw new UnsupportedOperationException(sb.toString());
            }
            case 9: {
                return CstKnownNull.o0;
            }
            case 8: {
                return CstShort.\u3007OOo8\u30070;
            }
            case 7: {
                return CstLong.\u3007OOo8\u30070;
            }
            case 6: {
                return CstInteger.\u300708O\u300700\u3007o;
            }
            case 5: {
                return CstFloat.\u3007OOo8\u30070;
            }
            case 4: {
                return CstDouble.\u3007OOo8\u30070;
            }
            case 3: {
                return CstChar.\u3007OOo8\u30070;
            }
            case 2: {
                return CstByte.\u3007OOo8\u30070;
            }
            case 1: {
                return CstBoolean.\u3007OOo8\u30070;
            }
        }
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.android.dx.rop.cst;

import com.android.dx.util.Hex;
import com.android.dx.rop.type.Type;

public final class CstByte extends CstLiteral32
{
    public static final CstByte \u3007OOo8\u30070;
    
    static {
        \u3007OOo8\u30070 = \u300780\u3007808\u3007O((byte)0);
    }
    
    private CstByte(final byte b) {
        super(b);
    }
    
    public static CstByte \u300780\u3007808\u3007O(final byte b) {
        return new CstByte(b);
    }
    
    @Override
    public String Oo08() {
        return "byte";
    }
    
    @Override
    public Type getType() {
        return Type.oOo0;
    }
    
    @Override
    public String toHuman() {
        return Integer.toString(this.\u3007\u3007888());
    }
    
    @Override
    public String toString() {
        final int \u3007\u3007888 = this.\u3007\u3007888();
        final StringBuilder sb = new StringBuilder();
        sb.append("byte{0x");
        sb.append(Hex.O8(\u3007\u3007888));
        sb.append(" / ");
        sb.append(\u3007\u3007888);
        sb.append('}');
        return sb.toString();
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.android.dx.rop.cst;

import com.android.dx.rop.type.Type;

public final class CstFieldRef extends CstMemberRef
{
    public CstFieldRef(final CstType cstType, final CstNat cstNat) {
        super(cstType, cstNat);
    }
    
    @Override
    protected int O8(final Constant constant) {
        final int o8 = super.O8(constant);
        if (o8 != 0) {
            return o8;
        }
        return this.\u3007\u3007888().o\u30070().\u3007o\u3007(((CstFieldRef)constant).\u3007\u3007888().o\u30070());
    }
    
    @Override
    public String Oo08() {
        return "field";
    }
    
    @Override
    public Type getType() {
        return this.\u3007\u3007888().\u3007\u3007888();
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.android.dx.rop.cst;

import com.android.dx.util.Hex;
import com.android.dx.rop.type.Type;

public final class CstFloat extends CstLiteral32
{
    public static final CstFloat OO;
    public static final CstFloat \u300708O\u300700\u3007o;
    public static final CstFloat \u3007OOo8\u30070;
    
    static {
        \u3007OOo8\u30070 = \u300780\u3007808\u3007O(Float.floatToIntBits(0.0f));
        OO = \u300780\u3007808\u3007O(Float.floatToIntBits(1.0f));
        \u300708O\u300700\u3007o = \u300780\u3007808\u3007O(Float.floatToIntBits(2.0f));
    }
    
    private CstFloat(final int n) {
        super(n);
    }
    
    public static CstFloat \u300780\u3007808\u3007O(final int n) {
        return new CstFloat(n);
    }
    
    @Override
    public String Oo08() {
        return "float";
    }
    
    @Override
    public Type getType() {
        return Type.\u30078\u3007oO\u3007\u30078o;
    }
    
    @Override
    public String toHuman() {
        return Float.toString(Float.intBitsToFloat(this.\u3007\u3007888()));
    }
    
    @Override
    public String toString() {
        final int \u3007\u3007888 = this.\u3007\u3007888();
        final StringBuilder sb = new StringBuilder();
        sb.append("float{0x");
        sb.append(Hex.oO80(\u3007\u3007888));
        sb.append(" / ");
        sb.append(Float.intBitsToFloat(\u3007\u3007888));
        sb.append('}');
        return sb.toString();
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.android.dx.rop.cst;

import com.android.dx.rop.type.Type;

public final class CstBoolean extends CstLiteral32
{
    public static final CstBoolean OO;
    public static final CstBoolean \u3007OOo8\u30070;
    
    static {
        \u3007OOo8\u30070 = new CstBoolean(false);
        OO = new CstBoolean(true);
    }
    
    private CstBoolean(final boolean b) {
        super(b ? 1 : 0);
    }
    
    public static CstBoolean OO0o\u3007\u3007\u3007\u30070(final boolean b) {
        CstBoolean cstBoolean;
        if (b) {
            cstBoolean = CstBoolean.OO;
        }
        else {
            cstBoolean = CstBoolean.\u3007OOo8\u30070;
        }
        return cstBoolean;
    }
    
    @Override
    public String Oo08() {
        return "boolean";
    }
    
    @Override
    public Type getType() {
        return Type.oOo\u30078o008;
    }
    
    @Override
    public String toHuman() {
        String s;
        if (this.\u300780\u3007808\u3007O()) {
            s = "true";
        }
        else {
            s = "false";
        }
        return s;
    }
    
    @Override
    public String toString() {
        String s;
        if (this.\u300780\u3007808\u3007O()) {
            s = "boolean{true}";
        }
        else {
            s = "boolean{false}";
        }
        return s;
    }
    
    public boolean \u300780\u3007808\u3007O() {
        return this.\u3007\u3007888() != 0;
    }
}

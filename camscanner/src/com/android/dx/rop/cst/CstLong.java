// 
// Decompiled by Procyon v0.6.0
// 

package com.android.dx.rop.cst;

import com.android.dx.util.Hex;
import com.android.dx.rop.type.Type;

public final class CstLong extends CstLiteral64
{
    public static final CstLong OO;
    public static final CstLong \u3007OOo8\u30070;
    
    static {
        \u3007OOo8\u30070 = \u300780\u3007808\u3007O(0L);
        OO = \u300780\u3007808\u3007O(1L);
    }
    
    private CstLong(final long n) {
        super(n);
    }
    
    public static CstLong \u300780\u3007808\u3007O(final long n) {
        return new CstLong(n);
    }
    
    @Override
    public String Oo08() {
        return "long";
    }
    
    @Override
    public Type getType() {
        return Type.\u3007\u300708O;
    }
    
    @Override
    public String toHuman() {
        return Long.toString(this.oO80());
    }
    
    @Override
    public String toString() {
        final long oo80 = this.oO80();
        final StringBuilder sb = new StringBuilder();
        sb.append("long{0x");
        sb.append(Hex.\u300780\u3007808\u3007O(oo80));
        sb.append(" / ");
        sb.append(oo80);
        sb.append('}');
        return sb.toString();
    }
}

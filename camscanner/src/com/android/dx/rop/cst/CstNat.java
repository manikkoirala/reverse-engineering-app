// 
// Decompiled by Procyon v0.6.0
// 

package com.android.dx.rop.cst;

import com.android.dx.rop.type.Type;

public final class CstNat extends Constant
{
    public static final CstNat OO;
    private final CstString o0;
    private final CstString \u3007OOo8\u30070;
    
    static {
        OO = new CstNat(new CstString("TYPE"), new CstString("Ljava/lang/Class;"));
    }
    
    public CstNat(final CstString o0, final CstString \u3007oOo8\u30070) {
        if (o0 == null) {
            throw new NullPointerException("name == null");
        }
        if (\u3007oOo8\u30070 != null) {
            this.o0 = o0;
            this.\u3007OOo8\u30070 = \u3007oOo8\u30070;
            return;
        }
        throw new NullPointerException("descriptor == null");
    }
    
    @Override
    protected int O8(final Constant constant) {
        final CstNat cstNat = (CstNat)constant;
        final int \u3007o\u3007 = this.o0.\u3007o\u3007(cstNat.o0);
        if (\u3007o\u3007 != 0) {
            return \u3007o\u3007;
        }
        return this.\u3007OOo8\u30070.\u3007o\u3007(cstNat.\u3007OOo8\u30070);
    }
    
    @Override
    public String Oo08() {
        return "nat";
    }
    
    @Override
    public boolean equals(final Object o) {
        final boolean b = o instanceof CstNat;
        final boolean b2 = false;
        if (!b) {
            return false;
        }
        final CstNat cstNat = (CstNat)o;
        boolean b3 = b2;
        if (this.o0.equals(cstNat.o0)) {
            b3 = b2;
            if (this.\u3007OOo8\u30070.equals(cstNat.\u3007OOo8\u30070)) {
                b3 = true;
            }
        }
        return b3;
    }
    
    @Override
    public int hashCode() {
        return this.o0.hashCode() * 31 ^ this.\u3007OOo8\u30070.hashCode();
    }
    
    public CstString oO80() {
        return this.o0;
    }
    
    public CstString o\u30070() {
        return this.\u3007OOo8\u30070;
    }
    
    @Override
    public String toHuman() {
        final StringBuilder sb = new StringBuilder();
        sb.append(this.o0.toHuman());
        sb.append(':');
        sb.append(this.\u3007OOo8\u30070.toHuman());
        return sb.toString();
    }
    
    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append("nat{");
        sb.append(this.toHuman());
        sb.append('}');
        return sb.toString();
    }
    
    public Type \u3007\u3007888() {
        return Type.OO0o\u3007\u3007\u3007\u30070(this.\u3007OOo8\u30070.getString());
    }
}

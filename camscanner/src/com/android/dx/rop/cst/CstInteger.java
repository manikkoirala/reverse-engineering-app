// 
// Decompiled by Procyon v0.6.0
// 

package com.android.dx.rop.cst;

import com.android.dx.util.Hex;
import com.android.dx.rop.type.Type;

public final class CstInteger extends CstLiteral32
{
    public static final CstInteger O8o08O8O;
    public static final CstInteger OO;
    public static final CstInteger oOo\u30078o008;
    public static final CstInteger o\u300700O;
    public static final CstInteger \u3007080OO8\u30070;
    public static final CstInteger \u300708O\u300700\u3007o;
    public static final CstInteger \u30070O;
    private static final CstInteger[] \u3007OOo8\u30070;
    
    static {
        \u3007OOo8\u30070 = new CstInteger[511];
        OO = OO0o\u3007\u3007\u3007\u30070(-1);
        \u300708O\u300700\u3007o = OO0o\u3007\u3007\u3007\u30070(0);
        o\u300700O = OO0o\u3007\u3007\u3007\u30070(1);
        O8o08O8O = OO0o\u3007\u3007\u3007\u30070(2);
        \u3007080OO8\u30070 = OO0o\u3007\u3007\u3007\u30070(3);
        \u30070O = OO0o\u3007\u3007\u3007\u30070(4);
        oOo\u30078o008 = OO0o\u3007\u3007\u3007\u30070(5);
    }
    
    private CstInteger(final int n) {
        super(n);
    }
    
    public static CstInteger OO0o\u3007\u3007\u3007\u30070(final int n) {
        final CstInteger[] \u3007oOo8\u30070 = CstInteger.\u3007OOo8\u30070;
        final int n2 = (Integer.MAX_VALUE & n) % \u3007oOo8\u30070.length;
        final CstInteger cstInteger = \u3007oOo8\u30070[n2];
        if (cstInteger != null && cstInteger.\u300780\u3007808\u3007O() == n) {
            return cstInteger;
        }
        return \u3007oOo8\u30070[n2] = new CstInteger(n);
    }
    
    @Override
    public String Oo08() {
        return "int";
    }
    
    @Override
    public Type getType() {
        return Type.ooo0\u3007\u3007O;
    }
    
    @Override
    public String toHuman() {
        return Integer.toString(this.\u3007\u3007888());
    }
    
    @Override
    public String toString() {
        final int \u3007\u3007888 = this.\u3007\u3007888();
        final StringBuilder sb = new StringBuilder();
        sb.append("int{0x");
        sb.append(Hex.oO80(\u3007\u3007888));
        sb.append(" / ");
        sb.append(\u3007\u3007888);
        sb.append('}');
        return sb.toString();
    }
    
    public int \u300780\u3007808\u3007O() {
        return this.\u3007\u3007888();
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.android.dx.rop.cst;

import com.android.dx.util.Hex;
import com.android.dx.rop.type.Type;

public final class CstShort extends CstLiteral32
{
    public static final CstShort \u3007OOo8\u30070;
    
    static {
        \u3007OOo8\u30070 = \u300780\u3007808\u3007O((short)0);
    }
    
    private CstShort(final short n) {
        super(n);
    }
    
    public static CstShort \u300780\u3007808\u3007O(final short n) {
        return new CstShort(n);
    }
    
    @Override
    public String Oo08() {
        return "short";
    }
    
    @Override
    public Type getType() {
        return Type.O0O;
    }
    
    @Override
    public String toHuman() {
        return Integer.toString(this.\u3007\u3007888());
    }
    
    @Override
    public String toString() {
        final int \u3007\u3007888 = this.\u3007\u3007888();
        final StringBuilder sb = new StringBuilder();
        sb.append("short{0x");
        sb.append(Hex.Oo08(\u3007\u3007888));
        sb.append(" / ");
        sb.append(\u3007\u3007888);
        sb.append('}');
        return sb.toString();
    }
}

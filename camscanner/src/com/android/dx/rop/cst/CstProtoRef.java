// 
// Decompiled by Procyon v0.6.0
// 

package com.android.dx.rop.cst;

import com.android.dx.rop.type.Type;
import com.android.dx.rop.type.Prototype;

public final class CstProtoRef extends TypedConstant
{
    private final Prototype o0;
    
    @Override
    protected int O8(final Constant constant) {
        return this.o0.\u3007080(((CstProtoRef)constant).o\u30070());
    }
    
    @Override
    public String Oo08() {
        return "proto";
    }
    
    @Override
    public boolean equals(final Object o) {
        return o instanceof CstProtoRef && this.o\u30070().equals(((CstProtoRef)o).o\u30070());
    }
    
    @Override
    public Type getType() {
        return Type.o\u3007oO;
    }
    
    @Override
    public int hashCode() {
        return this.o0.hashCode();
    }
    
    public Prototype o\u30070() {
        return this.o0;
    }
    
    @Override
    public String toHuman() {
        return this.o0.\u3007o\u3007();
    }
    
    @Override
    public final String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append(this.Oo08());
        sb.append("{");
        sb.append(this.toHuman());
        sb.append('}');
        return sb.toString();
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.android.dx.rop.cst;

import com.android.dx.rop.annotation.Annotation;

public final class CstAnnotation extends Constant
{
    private final Annotation o0;
    
    @Override
    protected int O8(final Constant constant) {
        return this.o0.oO80(((CstAnnotation)constant).o0);
    }
    
    @Override
    public String Oo08() {
        return "annotation";
    }
    
    @Override
    public boolean equals(final Object o) {
        return o instanceof CstAnnotation && this.o0.equals(((CstAnnotation)o).o0);
    }
    
    @Override
    public int hashCode() {
        return this.o0.hashCode();
    }
    
    public Annotation o\u30070() {
        return this.o0;
    }
    
    @Override
    public String toHuman() {
        return this.o0.toString();
    }
    
    @Override
    public String toString() {
        return this.o0.toString();
    }
}

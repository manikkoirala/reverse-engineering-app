// 
// Decompiled by Procyon v0.6.0
// 

package com.android.dx.rop.cst;

import com.android.dx.util.Hex;
import com.android.dx.rop.type.Type;

public final class CstChar extends CstLiteral32
{
    public static final CstChar \u3007OOo8\u30070;
    
    static {
        \u3007OOo8\u30070 = \u300780\u3007808\u3007O('\0');
    }
    
    private CstChar(final char c) {
        super(c);
    }
    
    public static CstChar \u300780\u3007808\u3007O(final char c) {
        return new CstChar(c);
    }
    
    @Override
    public String Oo08() {
        return "char";
    }
    
    @Override
    public Type getType() {
        return Type.OO\u300700\u30078oO;
    }
    
    @Override
    public String toHuman() {
        return Integer.toString(this.\u3007\u3007888());
    }
    
    @Override
    public String toString() {
        final int \u3007\u3007888 = this.\u3007\u3007888();
        final StringBuilder sb = new StringBuilder();
        sb.append("char{0x");
        sb.append(Hex.Oo08(\u3007\u3007888));
        sb.append(" / ");
        sb.append(\u3007\u3007888);
        sb.append('}');
        return sb.toString();
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.android.dx;

import com.android.dx.rop.cst.CstType;
import com.android.dx.rop.cst.CstString;
import com.android.dx.rop.cst.CstShort;
import com.android.dx.rop.cst.CstLong;
import com.android.dx.rop.cst.CstInteger;
import com.android.dx.rop.cst.CstFloat;
import com.android.dx.rop.cst.CstDouble;
import com.android.dx.rop.cst.CstChar;
import com.android.dx.rop.cst.CstByte;
import com.android.dx.rop.cst.CstBoolean;
import com.android.dx.rop.cst.CstKnownNull;
import com.android.dx.rop.cst.TypedConstant;

final class Constants
{
    static TypedConstant \u3007080(final Object obj) {
        if (obj == null) {
            return CstKnownNull.o0;
        }
        if (obj instanceof Boolean) {
            return CstBoolean.OO0o\u3007\u3007\u3007\u30070((boolean)obj);
        }
        if (obj instanceof Byte) {
            return CstByte.\u300780\u3007808\u3007O((byte)obj);
        }
        if (obj instanceof Character) {
            return CstChar.\u300780\u3007808\u3007O((char)obj);
        }
        if (obj instanceof Double) {
            return CstDouble.\u300780\u3007808\u3007O(Double.doubleToLongBits((double)obj));
        }
        if (obj instanceof Float) {
            return CstFloat.\u300780\u3007808\u3007O(Float.floatToIntBits((float)obj));
        }
        if (obj instanceof Integer) {
            return CstInteger.OO0o\u3007\u3007\u3007\u30070((int)obj);
        }
        if (obj instanceof Long) {
            return CstLong.\u300780\u3007808\u3007O((long)obj);
        }
        if (obj instanceof Short) {
            return CstShort.\u300780\u3007808\u3007O((short)obj);
        }
        if (obj instanceof String) {
            return new CstString((String)obj);
        }
        if (obj instanceof Class) {
            return new CstType(TypeId.\u3007080((Class<Object>)obj).\u3007o00\u3007\u3007Oo);
        }
        if (obj instanceof TypeId) {
            return new CstType(((TypeId)obj).\u3007o00\u3007\u3007Oo);
        }
        final StringBuilder sb = new StringBuilder();
        sb.append("Not a constant: ");
        sb.append(obj);
        throw new UnsupportedOperationException(sb.toString());
    }
}

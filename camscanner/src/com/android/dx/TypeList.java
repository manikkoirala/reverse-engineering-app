// 
// Decompiled by Procyon v0.6.0
// 

package com.android.dx;

import java.util.Arrays;
import com.android.dx.rop.type.StdTypeList;

final class TypeList
{
    final TypeId<?>[] \u3007080;
    final StdTypeList \u3007o00\u3007\u3007Oo;
    
    TypeList(final TypeId<?>[] array) {
        this.\u3007080 = array.clone();
        this.\u3007o00\u3007\u3007Oo = new StdTypeList(array.length);
        for (int i = 0; i < array.length; ++i) {
            this.\u3007o00\u3007\u3007Oo.oo88o8O(i, array[i].\u3007o00\u3007\u3007Oo);
        }
    }
    
    @Override
    public boolean equals(final Object o) {
        return o instanceof TypeList && Arrays.equals(((TypeList)o).\u3007080, this.\u3007080);
    }
    
    @Override
    public int hashCode() {
        return Arrays.hashCode(this.\u3007080);
    }
    
    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        for (int i = 0; i < this.\u3007080.length; ++i) {
            if (i > 0) {
                sb.append(", ");
            }
            sb.append(this.\u3007080[i]);
        }
        return sb.toString();
    }
}

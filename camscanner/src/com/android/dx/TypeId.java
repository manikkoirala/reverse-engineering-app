// 
// Decompiled by Procyon v0.6.0
// 

package com.android.dx;

import java.util.HashMap;
import com.android.dx.rop.cst.CstType;
import com.android.dx.rop.type.Type;
import java.util.Map;

public final class TypeId<T>
{
    public static final TypeId<Boolean> O8;
    public static final TypeId<Object> OO0o\u3007\u3007;
    public static final TypeId<Long> OO0o\u3007\u3007\u3007\u30070;
    public static final TypeId<Byte> Oo08;
    public static final TypeId<String> Oooo8o0\u3007;
    public static final TypeId<Float> oO80;
    public static final TypeId<Character> o\u30070;
    public static final TypeId<Integer> \u300780\u3007808\u3007O;
    public static final TypeId<Short> \u30078o8o\u3007;
    public static final TypeId<Void> \u3007O8o08O;
    private static final Map<Class<?>, TypeId<?>> \u3007\u3007808\u3007;
    public static final TypeId<Double> \u3007\u3007888;
    final String \u3007080;
    final Type \u3007o00\u3007\u3007Oo;
    final CstType \u3007o\u3007;
    
    static {
        final TypeId<Boolean> typeId = O8 = new TypeId<Boolean>(Type.oOo\u30078o008);
        final TypeId<Byte> typeId2 = Oo08 = new TypeId<Byte>(Type.oOo0);
        final TypeId<Character> typeId3 = o\u30070 = new TypeId<Character>(Type.OO\u300700\u30078oO);
        final TypeId<Double> typeId4 = \u3007\u3007888 = new TypeId<Double>(Type.o8\u3007OO0\u30070o);
        final TypeId<Float> typeId5 = oO80 = new TypeId<Float>(Type.\u30078\u3007oO\u3007\u30078o);
        final TypeId<Integer> typeId6 = \u300780\u3007808\u3007O = new TypeId<Integer>(Type.ooo0\u3007\u3007O);
        final TypeId<Long> typeId7 = OO0o\u3007\u3007\u3007\u30070 = new TypeId<Long>(Type.\u3007\u300708O);
        final TypeId<Short> typeId8 = \u30078o8o\u3007 = new TypeId<Short>(Type.O0O);
        final TypeId<Void> typeId9 = \u3007O8o08O = new TypeId<Void>(Type.o8oOOo);
        OO0o\u3007\u3007 = new TypeId<Object>(Type.\u3007\u3007o\u3007);
        Oooo8o0\u3007 = new TypeId<String>(Type.O\u3007o88o08\u3007);
        final HashMap \u3007\u3007808\u30072 = new HashMap();
        (\u3007\u3007808\u3007 = \u3007\u3007808\u30072).put(Boolean.TYPE, typeId);
        \u3007\u3007808\u30072.put(Byte.TYPE, typeId2);
        \u3007\u3007808\u30072.put(Character.TYPE, typeId3);
        \u3007\u3007808\u30072.put(Double.TYPE, typeId4);
        \u3007\u3007808\u30072.put(Float.TYPE, typeId5);
        \u3007\u3007808\u30072.put(Integer.TYPE, typeId6);
        \u3007\u3007808\u30072.put(Long.TYPE, typeId7);
        \u3007\u3007808\u30072.put(Short.TYPE, typeId8);
        \u3007\u3007808\u30072.put(Void.TYPE, typeId9);
    }
    
    TypeId(final Type type) {
        this(type.oO80(), type);
    }
    
    TypeId(final String \u3007080, final Type \u3007o00\u3007\u3007Oo) {
        if (\u3007080 != null && \u3007o00\u3007\u3007Oo != null) {
            this.\u3007080 = \u3007080;
            this.\u3007o00\u3007\u3007Oo = \u3007o00\u3007\u3007Oo;
            this.\u3007o\u3007 = CstType.OO0o\u3007\u3007\u3007\u30070(\u3007o00\u3007\u3007Oo);
            return;
        }
        throw null;
    }
    
    public static <T> TypeId<T> \u3007080(final Class<T> clazz) {
        if (clazz.isPrimitive()) {
            return (TypeId)TypeId.\u3007\u3007808\u3007.get(clazz);
        }
        final String replace = clazz.getName().replace('.', '/');
        String string;
        if (clazz.isArray()) {
            string = replace;
        }
        else {
            final StringBuilder sb = new StringBuilder();
            sb.append('L');
            sb.append(replace);
            sb.append(';');
            string = sb.toString();
        }
        return (TypeId<T>)\u3007o00\u3007\u3007Oo(string);
    }
    
    public static <T> TypeId<T> \u3007o00\u3007\u3007Oo(final String s) {
        return new TypeId<T>(s, Type.\u30078o8o\u3007(s));
    }
    
    public <V> FieldId<T, V> O8(final TypeId<V> typeId, final String s) {
        return new FieldId<T, V>(this, typeId, s);
    }
    
    public <R> MethodId<T, R> Oo08(final TypeId<R> typeId, final String s, final TypeId<?>... array) {
        return new MethodId<T, R>(this, typeId, s, new TypeList(array));
    }
    
    @Override
    public boolean equals(final Object o) {
        return o instanceof TypeId && ((TypeId)o).\u3007080.equals(this.\u3007080);
    }
    
    @Override
    public int hashCode() {
        return this.\u3007080.hashCode();
    }
    
    @Override
    public String toString() {
        return this.\u3007080;
    }
    
    public MethodId<T, Void> \u3007o\u3007(final TypeId<?>... array) {
        return new MethodId<T, Void>(this, TypeId.\u3007O8o08O, "<init>", new TypeList(array));
    }
}

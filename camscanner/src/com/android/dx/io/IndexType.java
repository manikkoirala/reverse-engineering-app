// 
// Decompiled by Procyon v0.6.0
// 

package com.android.dx.io;

public enum IndexType
{
    private static final IndexType[] $VALUES;
    
    CALL_SITE_REF, 
    FIELD_OFFSET, 
    FIELD_REF, 
    INLINE_METHOD, 
    METHOD_AND_PROTO_REF, 
    METHOD_HANDLE_REF, 
    METHOD_REF, 
    NONE, 
    PROTO_REF, 
    STRING_REF, 
    TYPE_REF, 
    UNKNOWN, 
    VARIES, 
    VTABLE_OFFSET;
}

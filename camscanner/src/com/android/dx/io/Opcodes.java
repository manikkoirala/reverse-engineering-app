// 
// Decompiled by Procyon v0.6.0
// 

package com.android.dx.io;

public final class Opcodes
{
    public static boolean \u3007080(final int n) {
        boolean b = false;
        if (n < -1) {
            return false;
        }
        if (n == -1) {
            return true;
        }
        final int n2 = n & 0xFF;
        if (n2 != 0 && n2 != 255) {
            if ((n & 0xFF00) == 0x0) {
                b = true;
            }
            return b;
        }
        return true;
    }
}

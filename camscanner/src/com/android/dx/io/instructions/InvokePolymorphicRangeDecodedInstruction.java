// 
// Decompiled by Procyon v0.6.0
// 

package com.android.dx.io.instructions;

import com.android.dx.io.IndexType;

public class InvokePolymorphicRangeDecodedInstruction extends DecodedInstruction
{
    private final int oO80;
    private final int \u300780\u3007808\u3007O;
    private final int \u3007\u3007888;
    
    public InvokePolymorphicRangeDecodedInstruction(final InstructionCodec instructionCodec, final int n, final int n2, final IndexType indexType, final int \u3007\u3007888, final int oo80, final int n3) {
        super(instructionCodec, n, n2, indexType, 0, 0L);
        if (n3 == (short)n3) {
            this.\u3007\u3007888 = \u3007\u3007888;
            this.oO80 = oo80;
            this.\u300780\u3007808\u3007O = n3;
            return;
        }
        final StringBuilder sb = new StringBuilder();
        sb.append("protoIndex doesn't fit in a short: ");
        sb.append(n3);
        throw new IllegalArgumentException(sb.toString());
    }
    
    @Override
    public int Oo08() {
        return this.\u3007\u3007888;
    }
    
    @Override
    public int \u30070\u3007O0088o() {
        return this.oO80;
    }
    
    @Override
    public short \u3007\u30078O0\u30078() {
        return (short)this.\u300780\u3007808\u3007O;
    }
}

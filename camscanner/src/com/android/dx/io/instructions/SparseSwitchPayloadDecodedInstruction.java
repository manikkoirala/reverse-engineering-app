// 
// Decompiled by Procyon v0.6.0
// 

package com.android.dx.io.instructions;

import com.android.dx.io.IndexType;

public final class SparseSwitchPayloadDecodedInstruction extends DecodedInstruction
{
    private final int[] oO80;
    private final int[] \u3007\u3007888;
    
    public SparseSwitchPayloadDecodedInstruction(final InstructionCodec instructionCodec, final int n, final int[] \u3007\u3007888, final int[] oo80) {
        super(instructionCodec, n, 0, null, 0, 0L);
        if (\u3007\u3007888.length == oo80.length) {
            this.\u3007\u3007888 = \u3007\u3007888;
            this.oO80 = oo80;
            return;
        }
        throw new IllegalArgumentException("keys/targets length mismatch");
    }
    
    public int[] oo88o8O() {
        return this.\u3007\u3007888;
    }
    
    @Override
    public int \u30070\u3007O0088o() {
        return 0;
    }
    
    public int[] \u3007oo\u3007() {
        return this.oO80;
    }
}

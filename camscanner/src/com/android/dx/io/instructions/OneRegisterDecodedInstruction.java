// 
// Decompiled by Procyon v0.6.0
// 

package com.android.dx.io.instructions;

import com.android.dx.io.IndexType;

public final class OneRegisterDecodedInstruction extends DecodedInstruction
{
    private final int \u3007\u3007888;
    
    public OneRegisterDecodedInstruction(final InstructionCodec instructionCodec, final int n, final int n2, final IndexType indexType, final int n3, final long n4, final int \u3007\u3007888) {
        super(instructionCodec, n, n2, indexType, n3, n4);
        this.\u3007\u3007888 = \u3007\u3007888;
    }
    
    @Override
    public int \u3007080() {
        return this.\u3007\u3007888;
    }
    
    @Override
    public int \u30070\u3007O0088o() {
        return 1;
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.android.dx.io.instructions;

import com.android.dex.DexException;
import com.android.dx.util.Hex;
import com.android.dx.io.Opcodes;
import com.android.dx.io.IndexType;

public abstract class DecodedInstruction
{
    private final IndexType O8;
    private final int Oo08;
    private final long o\u30070;
    private final InstructionCodec \u3007080;
    private final int \u3007o00\u3007\u3007Oo;
    private final int \u3007o\u3007;
    
    public DecodedInstruction(final InstructionCodec \u3007080, final int \u3007o00\u3007\u3007Oo, final int \u3007o\u3007, final IndexType o8, final int oo08, final long o\u30070) {
        if (\u3007080 == null) {
            throw new NullPointerException("format == null");
        }
        if (Opcodes.\u3007080(\u3007o00\u3007\u3007Oo)) {
            this.\u3007080 = \u3007080;
            this.\u3007o00\u3007\u3007Oo = \u3007o00\u3007\u3007Oo;
            this.\u3007o\u3007 = \u3007o\u3007;
            this.O8 = o8;
            this.Oo08 = oo08;
            this.o\u30070 = o\u30070;
            return;
        }
        throw new IllegalArgumentException("invalid opcode");
    }
    
    public final short O8() {
        final int \u3007o\u3007 = this.\u3007o\u3007();
        if ((0xFFFF0000 & \u3007o\u3007) == 0x0) {
            return (short)\u3007o\u3007;
        }
        final StringBuilder sb = new StringBuilder();
        sb.append("Register B out of range: ");
        sb.append(Hex.\u300780\u3007808\u3007O(\u3007o\u3007));
        throw new DexException(sb.toString());
    }
    
    public final int OO0o\u3007\u3007() {
        final long o\u30070 = this.o\u30070;
        if (o\u30070 == (int)o\u30070) {
            return (int)o\u30070;
        }
        final StringBuilder sb = new StringBuilder();
        sb.append("Literal out of range: ");
        sb.append(Hex.\u300780\u3007808\u3007O(this.o\u30070));
        throw new DexException(sb.toString());
    }
    
    public final short OO0o\u3007\u3007\u3007\u30070() {
        return (short)this.\u3007o\u3007;
    }
    
    public int Oo08() {
        return 0;
    }
    
    public final int OoO8(final int n) {
        return this.Oo08 - n;
    }
    
    public final int Oooo8o0\u3007() {
        final long o\u30070 = this.o\u30070;
        if (o\u30070 >= -8L && o\u30070 <= 7L) {
            return (int)o\u30070 & 0xF;
        }
        final StringBuilder sb = new StringBuilder();
        sb.append("Literal out of range: ");
        sb.append(Hex.\u300780\u3007808\u3007O(this.o\u30070));
        throw new DexException(sb.toString());
    }
    
    public final int o800o8O(int ooO8) {
        ooO8 = this.OoO8(ooO8);
        if (ooO8 == (byte)ooO8) {
            return ooO8 & 0xFF;
        }
        final StringBuilder sb = new StringBuilder();
        sb.append("Target out of range: ");
        sb.append(Hex.\u3007o\u3007(ooO8));
        throw new DexException(sb.toString());
    }
    
    public int oO80() {
        return 0;
    }
    
    public final short o\u30070() {
        final int oo08 = this.Oo08();
        if ((0xFFFF0000 & oo08) == 0x0) {
            return (short)oo08;
        }
        final StringBuilder sb = new StringBuilder();
        sb.append("Register C out of range: ");
        sb.append(Hex.\u300780\u3007808\u3007O(oo08));
        throw new DexException(sb.toString());
    }
    
    public int \u3007080() {
        return 0;
    }
    
    public abstract int \u30070\u3007O0088o();
    
    public final int \u300780\u3007808\u3007O() {
        return this.\u3007o\u3007;
    }
    
    public final long \u30078o8o\u3007() {
        return this.o\u30070;
    }
    
    public final short \u3007O00() {
        return (short)this.\u3007o00\u3007\u3007Oo;
    }
    
    public final short \u3007O888o0o(int ooO8) {
        ooO8 = this.OoO8(ooO8);
        final short n = (short)ooO8;
        if (ooO8 == n) {
            return n;
        }
        final StringBuilder sb = new StringBuilder();
        sb.append("Target out of range: ");
        sb.append(Hex.\u3007o\u3007(ooO8));
        throw new DexException(sb.toString());
    }
    
    public final int \u3007O8o08O() {
        final long o\u30070 = this.o\u30070;
        if (o\u30070 == (byte)o\u30070) {
            return (int)o\u30070 & 0xFF;
        }
        final StringBuilder sb = new StringBuilder();
        sb.append("Literal out of range: ");
        sb.append(Hex.\u300780\u3007808\u3007O(this.o\u30070));
        throw new DexException(sb.toString());
    }
    
    public final int \u3007O\u3007() {
        return this.\u3007o00\u3007\u3007Oo;
    }
    
    public final short \u3007o00\u3007\u3007Oo() {
        final int \u3007080 = this.\u3007080();
        if ((0xFFFF0000 & \u3007080) == 0x0) {
            return (short)\u3007080;
        }
        final StringBuilder sb = new StringBuilder();
        sb.append("Register A out of range: ");
        sb.append(Hex.\u300780\u3007808\u3007O(\u3007080));
        throw new DexException(sb.toString());
    }
    
    public int \u3007o\u3007() {
        return 0;
    }
    
    public final short \u3007\u3007808\u3007() {
        final long o\u30070 = this.o\u30070;
        if (o\u30070 == (short)o\u30070) {
            return (short)o\u30070;
        }
        final StringBuilder sb = new StringBuilder();
        sb.append("Literal out of range: ");
        sb.append(Hex.\u300780\u3007808\u3007O(this.o\u30070));
        throw new DexException(sb.toString());
    }
    
    public int \u3007\u3007888() {
        return 0;
    }
    
    public short \u3007\u30078O0\u30078() {
        throw new IllegalStateException(this.getClass().toString());
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.android.dx.io.instructions;

import com.android.dx.io.IndexType;

public class InvokePolymorphicDecodedInstruction extends DecodedInstruction
{
    private final int[] oO80;
    private final int \u3007\u3007888;
    
    public InvokePolymorphicDecodedInstruction(final InstructionCodec instructionCodec, final int n, final int n2, final IndexType indexType, final int n3, final int[] oo80) {
        super(instructionCodec, n, n2, indexType, 0, 0L);
        if (n3 == (short)n3) {
            this.\u3007\u3007888 = n3;
            this.oO80 = oo80;
            return;
        }
        final StringBuilder sb = new StringBuilder();
        sb.append("protoIndex doesn't fit in a short: ");
        sb.append(n3);
        throw new IllegalArgumentException(sb.toString());
    }
    
    @Override
    public int Oo08() {
        final int[] oo80 = this.oO80;
        final int length = oo80.length;
        int n = 0;
        if (length > 0) {
            n = oo80[0];
        }
        return n;
    }
    
    @Override
    public int oO80() {
        final int[] oo80 = this.oO80;
        int n;
        if (oo80.length > 2) {
            n = oo80[2];
        }
        else {
            n = 0;
        }
        return n;
    }
    
    public int oo88o8O() {
        final int[] oo80 = this.oO80;
        int n;
        if (oo80.length > 3) {
            n = oo80[3];
        }
        else {
            n = 0;
        }
        return n;
    }
    
    @Override
    public int \u30070\u3007O0088o() {
        return this.oO80.length;
    }
    
    public int \u3007oo\u3007() {
        final int[] oo80 = this.oO80;
        int n;
        if (oo80.length > 4) {
            n = oo80[4];
        }
        else {
            n = 0;
        }
        return n;
    }
    
    @Override
    public int \u3007\u3007888() {
        final int[] oo80 = this.oO80;
        int n;
        if (oo80.length > 1) {
            n = oo80[1];
        }
        else {
            n = 0;
        }
        return n;
    }
    
    @Override
    public short \u3007\u30078O0\u30078() {
        return (short)this.\u3007\u3007888;
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.android.dx.io.instructions;

import com.android.dex.DexException;
import com.android.dx.util.Hex;
import java.util.Arrays;
import com.android.dx.io.OpcodeInfo;
import java.io.EOFException;
import com.android.dx.io.IndexType;

public enum InstructionCodec
{
    private static final InstructionCodec[] $VALUES;
    
    FORMAT_00X {
        @Override
        public DecodedInstruction decode(final int n, final CodeInput codeInput) throws EOFException {
            return new ZeroRegisterDecodedInstruction(this, n, 0, null, 0, 0L);
        }
        
        @Override
        public void encode(final DecodedInstruction decodedInstruction, final CodeOutput codeOutput) {
            codeOutput.oO80(decodedInstruction.\u3007O00());
        }
    }, 
    FORMAT_10T {
        @Override
        public DecodedInstruction decode(final int n, final CodeInput codeInput) throws EOFException {
            return new ZeroRegisterDecodedInstruction(this, byte0(n), 0, null, codeInput.\u3007\u3007888() - 1 + (byte)byte1(n), 0L);
        }
        
        @Override
        public void encode(final DecodedInstruction decodedInstruction, final CodeOutput codeOutput) {
            codeOutput.oO80(codeUnit(decodedInstruction.\u3007O\u3007(), decodedInstruction.o800o8O(codeOutput.\u3007\u3007888())));
        }
    }, 
    FORMAT_10X {
        @Override
        public DecodedInstruction decode(final int n, final CodeInput codeInput) throws EOFException {
            return new ZeroRegisterDecodedInstruction(this, byte0(n), 0, null, 0, byte1(n));
        }
        
        @Override
        public void encode(final DecodedInstruction decodedInstruction, final CodeOutput codeOutput) {
            codeOutput.oO80(decodedInstruction.\u3007O00());
        }
    }, 
    FORMAT_11N {
        @Override
        public DecodedInstruction decode(final int n, final CodeInput codeInput) throws EOFException {
            return new OneRegisterDecodedInstruction(this, byte0(n), 0, null, 0, nibble3(n) << 28 >> 28, nibble2(n));
        }
        
        @Override
        public void encode(final DecodedInstruction decodedInstruction, final CodeOutput codeOutput) {
            codeOutput.oO80(codeUnit(decodedInstruction.\u3007O00(), makeByte(decodedInstruction.\u3007080(), decodedInstruction.Oooo8o0\u3007())));
        }
    }, 
    FORMAT_11X {
        @Override
        public DecodedInstruction decode(final int n, final CodeInput codeInput) throws EOFException {
            return new OneRegisterDecodedInstruction(this, byte0(n), 0, null, 0, 0L, byte1(n));
        }
        
        @Override
        public void encode(final DecodedInstruction decodedInstruction, final CodeOutput codeOutput) {
            codeOutput.oO80(codeUnit(decodedInstruction.\u3007O\u3007(), decodedInstruction.\u3007080()));
        }
    }, 
    FORMAT_12X {
        @Override
        public DecodedInstruction decode(final int n, final CodeInput codeInput) throws EOFException {
            return new TwoRegisterDecodedInstruction(this, byte0(n), 0, null, 0, 0L, nibble2(n), nibble3(n));
        }
        
        @Override
        public void encode(final DecodedInstruction decodedInstruction, final CodeOutput codeOutput) {
            codeOutput.oO80(codeUnit(decodedInstruction.\u3007O00(), makeByte(decodedInstruction.\u3007080(), decodedInstruction.\u3007o\u3007())));
        }
    }, 
    FORMAT_20BC {
        @Override
        public DecodedInstruction decode(int \u3007o8o08O, final CodeInput codeInput) throws EOFException {
            final int \u3007080 = byte0(\u3007o8o08O);
            \u3007o8o08O = byte1(\u3007o8o08O);
            return new ZeroRegisterDecodedInstruction(this, \u3007080, codeInput.read(), IndexType.VARIES, 0, \u3007o8o08O);
        }
        
        @Override
        public void encode(final DecodedInstruction decodedInstruction, final CodeOutput codeOutput) {
            codeOutput.\u3007080(codeUnit(decodedInstruction.\u3007O\u3007(), decodedInstruction.\u3007O8o08O()), decodedInstruction.OO0o\u3007\u3007\u3007\u30070());
        }
    }, 
    FORMAT_20T {
        @Override
        public DecodedInstruction decode(int \u3007o8o08O, final CodeInput codeInput) throws EOFException {
            final int \u3007\u3007888 = codeInput.\u3007\u3007888();
            final int \u3007080 = byte0(\u3007o8o08O);
            \u3007o8o08O = byte1(\u3007o8o08O);
            return new ZeroRegisterDecodedInstruction(this, \u3007080, 0, null, \u3007\u3007888 - 1 + (short)codeInput.read(), \u3007o8o08O);
        }
        
        @Override
        public void encode(final DecodedInstruction decodedInstruction, final CodeOutput codeOutput) {
            codeOutput.\u3007080(decodedInstruction.\u3007O00(), decodedInstruction.\u3007O888o0o(codeOutput.\u3007\u3007888()));
        }
    }, 
    FORMAT_21C {
        @Override
        public DecodedInstruction decode(int \u3007o8o08O, final CodeInput codeInput) throws EOFException {
            final int \u3007080 = byte0(\u3007o8o08O);
            \u3007o8o08O = byte1(\u3007o8o08O);
            return new OneRegisterDecodedInstruction(this, \u3007080, codeInput.read(), OpcodeInfo.\u3007o00\u3007\u3007Oo(\u3007080), 0, 0L, \u3007o8o08O);
        }
        
        @Override
        public void encode(final DecodedInstruction decodedInstruction, final CodeOutput codeOutput) {
            codeOutput.\u3007080(codeUnit(decodedInstruction.\u3007O\u3007(), decodedInstruction.\u3007080()), decodedInstruction.OO0o\u3007\u3007\u3007\u30070());
        }
    }, 
    FORMAT_21H {
        @Override
        public DecodedInstruction decode(int n, final CodeInput codeInput) throws EOFException {
            final int \u3007080 = byte0(n);
            final int \u3007o8o08O = byte1(n);
            final long n2 = (short)codeInput.read();
            if (\u3007080 == 21) {
                n = 16;
            }
            else {
                n = 48;
            }
            return new OneRegisterDecodedInstruction(this, \u3007080, 0, null, 0, n2 << n, \u3007o8o08O);
        }
        
        @Override
        public void encode(final DecodedInstruction decodedInstruction, final CodeOutput codeOutput) {
            final int \u3007o\u3007 = decodedInstruction.\u3007O\u3007();
            int n;
            if (\u3007o\u3007 == 21) {
                n = 16;
            }
            else {
                n = 48;
            }
            codeOutput.\u3007080(codeUnit(\u3007o\u3007, decodedInstruction.\u3007080()), (short)(decodedInstruction.\u30078o8o\u3007() >> n));
        }
    }, 
    FORMAT_21S {
        @Override
        public DecodedInstruction decode(int \u3007o8o08O, final CodeInput codeInput) throws EOFException {
            final int \u3007080 = byte0(\u3007o8o08O);
            \u3007o8o08O = byte1(\u3007o8o08O);
            return new OneRegisterDecodedInstruction(this, \u3007080, 0, null, 0, (short)codeInput.read(), \u3007o8o08O);
        }
        
        @Override
        public void encode(final DecodedInstruction decodedInstruction, final CodeOutput codeOutput) {
            codeOutput.\u3007080(codeUnit(decodedInstruction.\u3007O\u3007(), decodedInstruction.\u3007080()), decodedInstruction.\u3007\u3007808\u3007());
        }
    }, 
    FORMAT_21T {
        @Override
        public DecodedInstruction decode(int \u3007o8o08O, final CodeInput codeInput) throws EOFException {
            final int \u3007\u3007888 = codeInput.\u3007\u3007888();
            final int \u3007080 = byte0(\u3007o8o08O);
            \u3007o8o08O = byte1(\u3007o8o08O);
            return new OneRegisterDecodedInstruction(this, \u3007080, 0, null, \u3007\u3007888 - 1 + (short)codeInput.read(), 0L, \u3007o8o08O);
        }
        
        @Override
        public void encode(final DecodedInstruction decodedInstruction, final CodeOutput codeOutput) {
            codeOutput.\u3007080(codeUnit(decodedInstruction.\u3007O\u3007(), decodedInstruction.\u3007080()), decodedInstruction.\u3007O888o0o(codeOutput.\u3007\u3007888()));
        }
    }, 
    FORMAT_22B {
        @Override
        public DecodedInstruction decode(int read, final CodeInput codeInput) throws EOFException {
            final int \u3007080 = byte0(read);
            final int \u3007o8o08O = byte1(read);
            read = codeInput.read();
            return new TwoRegisterDecodedInstruction(this, \u3007080, 0, null, 0, (byte)byte1(read), \u3007o8o08O, byte0(read));
        }
        
        @Override
        public void encode(final DecodedInstruction decodedInstruction, final CodeOutput codeOutput) {
            codeOutput.\u3007080(codeUnit(decodedInstruction.\u3007O\u3007(), decodedInstruction.\u3007080()), codeUnit(decodedInstruction.\u3007o\u3007(), decodedInstruction.\u3007O8o08O()));
        }
    }, 
    FORMAT_22C {
        @Override
        public DecodedInstruction decode(int \u3007\u3007808\u3007, final CodeInput codeInput) throws EOFException {
            final int \u3007080 = byte0(\u3007\u3007808\u3007);
            final int oooo8o0\u3007 = nibble2(\u3007\u3007808\u3007);
            \u3007\u3007808\u3007 = nibble3(\u3007\u3007808\u3007);
            return new TwoRegisterDecodedInstruction(this, \u3007080, codeInput.read(), OpcodeInfo.\u3007o00\u3007\u3007Oo(\u3007080), 0, 0L, oooo8o0\u3007, \u3007\u3007808\u3007);
        }
        
        @Override
        public void encode(final DecodedInstruction decodedInstruction, final CodeOutput codeOutput) {
            codeOutput.\u3007080(codeUnit(decodedInstruction.\u3007O\u3007(), makeByte(decodedInstruction.\u3007080(), decodedInstruction.\u3007o\u3007())), decodedInstruction.OO0o\u3007\u3007\u3007\u30070());
        }
    }, 
    FORMAT_22CS {
        @Override
        public DecodedInstruction decode(int \u3007\u3007808\u3007, final CodeInput codeInput) throws EOFException {
            final int \u3007080 = byte0(\u3007\u3007808\u3007);
            final int oooo8o0\u3007 = nibble2(\u3007\u3007808\u3007);
            \u3007\u3007808\u3007 = nibble3(\u3007\u3007808\u3007);
            return new TwoRegisterDecodedInstruction(this, \u3007080, codeInput.read(), IndexType.FIELD_OFFSET, 0, 0L, oooo8o0\u3007, \u3007\u3007808\u3007);
        }
        
        @Override
        public void encode(final DecodedInstruction decodedInstruction, final CodeOutput codeOutput) {
            codeOutput.\u3007080(codeUnit(decodedInstruction.\u3007O\u3007(), makeByte(decodedInstruction.\u3007080(), decodedInstruction.\u3007o\u3007())), decodedInstruction.OO0o\u3007\u3007\u3007\u30070());
        }
    }, 
    FORMAT_22S {
        @Override
        public DecodedInstruction decode(int \u3007\u3007808\u3007, final CodeInput codeInput) throws EOFException {
            final int \u3007080 = byte0(\u3007\u3007808\u3007);
            final int oooo8o0\u3007 = nibble2(\u3007\u3007808\u3007);
            \u3007\u3007808\u3007 = nibble3(\u3007\u3007808\u3007);
            return new TwoRegisterDecodedInstruction(this, \u3007080, 0, null, 0, (short)codeInput.read(), oooo8o0\u3007, \u3007\u3007808\u3007);
        }
        
        @Override
        public void encode(final DecodedInstruction decodedInstruction, final CodeOutput codeOutput) {
            codeOutput.\u3007080(codeUnit(decodedInstruction.\u3007O\u3007(), makeByte(decodedInstruction.\u3007080(), decodedInstruction.\u3007o\u3007())), decodedInstruction.\u3007\u3007808\u3007());
        }
    }, 
    FORMAT_22T {
        @Override
        public DecodedInstruction decode(int \u3007\u3007808\u3007, final CodeInput codeInput) throws EOFException {
            final int \u3007\u3007888 = codeInput.\u3007\u3007888();
            final int \u3007080 = byte0(\u3007\u3007808\u3007);
            final int oooo8o0\u3007 = nibble2(\u3007\u3007808\u3007);
            \u3007\u3007808\u3007 = nibble3(\u3007\u3007808\u3007);
            return new TwoRegisterDecodedInstruction(this, \u3007080, 0, null, \u3007\u3007888 - 1 + (short)codeInput.read(), 0L, oooo8o0\u3007, \u3007\u3007808\u3007);
        }
        
        @Override
        public void encode(final DecodedInstruction decodedInstruction, final CodeOutput codeOutput) {
            codeOutput.\u3007080(codeUnit(decodedInstruction.\u3007O\u3007(), makeByte(decodedInstruction.\u3007080(), decodedInstruction.\u3007o\u3007())), decodedInstruction.\u3007O888o0o(codeOutput.\u3007\u3007888()));
        }
    }, 
    FORMAT_22X {
        @Override
        public DecodedInstruction decode(final int n, final CodeInput codeInput) throws EOFException {
            return new TwoRegisterDecodedInstruction(this, byte0(n), 0, null, 0, 0L, byte1(n), codeInput.read());
        }
        
        @Override
        public void encode(final DecodedInstruction decodedInstruction, final CodeOutput codeOutput) {
            codeOutput.\u3007080(codeUnit(decodedInstruction.\u3007O\u3007(), decodedInstruction.\u3007080()), decodedInstruction.O8());
        }
    }, 
    FORMAT_23X {
        @Override
        public DecodedInstruction decode(int read, final CodeInput codeInput) throws EOFException {
            final int \u3007080 = byte0(read);
            final int \u3007o8o08O = byte1(read);
            read = codeInput.read();
            return new ThreeRegisterDecodedInstruction(this, \u3007080, 0, null, 0, 0L, \u3007o8o08O, byte0(read), byte1(read));
        }
        
        @Override
        public void encode(final DecodedInstruction decodedInstruction, final CodeOutput codeOutput) {
            codeOutput.\u3007080(codeUnit(decodedInstruction.\u3007O\u3007(), decodedInstruction.\u3007080()), codeUnit(decodedInstruction.\u3007o\u3007(), decodedInstruction.Oo08()));
        }
    }, 
    FORMAT_30T {
        @Override
        public DecodedInstruction decode(int \u3007o8o08O, final CodeInput codeInput) throws EOFException {
            final int \u3007\u3007888 = codeInput.\u3007\u3007888();
            final int \u3007080 = byte0(\u3007o8o08O);
            \u3007o8o08O = byte1(\u3007o8o08O);
            return new ZeroRegisterDecodedInstruction(this, \u3007080, 0, null, \u3007\u3007888 - 1 + codeInput.readInt(), \u3007o8o08O);
        }
        
        @Override
        public void encode(final DecodedInstruction decodedInstruction, final CodeOutput codeOutput) {
            final int ooO8 = decodedInstruction.OoO8(codeOutput.\u3007\u3007888());
            codeOutput.\u30078o8o\u3007(decodedInstruction.\u3007O00(), unit0(ooO8), unit1(ooO8));
        }
    }, 
    FORMAT_31C {
        @Override
        public DecodedInstruction decode(int \u3007o8o08O, final CodeInput codeInput) throws EOFException {
            final int \u3007080 = byte0(\u3007o8o08O);
            \u3007o8o08O = byte1(\u3007o8o08O);
            return new OneRegisterDecodedInstruction(this, \u3007080, codeInput.readInt(), OpcodeInfo.\u3007o00\u3007\u3007Oo(\u3007080), 0, 0L, \u3007o8o08O);
        }
        
        @Override
        public void encode(final DecodedInstruction decodedInstruction, final CodeOutput codeOutput) {
            final int \u300780\u3007808\u3007O = decodedInstruction.\u300780\u3007808\u3007O();
            codeOutput.\u30078o8o\u3007(codeUnit(decodedInstruction.\u3007O\u3007(), decodedInstruction.\u3007080()), unit0(\u300780\u3007808\u3007O), unit1(\u300780\u3007808\u3007O));
        }
    }, 
    FORMAT_31I {
        @Override
        public DecodedInstruction decode(int \u3007o8o08O, final CodeInput codeInput) throws EOFException {
            final int \u3007080 = byte0(\u3007o8o08O);
            \u3007o8o08O = byte1(\u3007o8o08O);
            return new OneRegisterDecodedInstruction(this, \u3007080, 0, null, 0, codeInput.readInt(), \u3007o8o08O);
        }
        
        @Override
        public void encode(final DecodedInstruction decodedInstruction, final CodeOutput codeOutput) {
            final int oo0o\u3007\u3007 = decodedInstruction.OO0o\u3007\u3007();
            codeOutput.\u30078o8o\u3007(codeUnit(decodedInstruction.\u3007O\u3007(), decodedInstruction.\u3007080()), unit0(oo0o\u3007\u3007), unit1(oo0o\u3007\u3007));
        }
    }, 
    FORMAT_31T {
        @Override
        public DecodedInstruction decode(int n, final CodeInput codeInput) throws EOFException {
            final int n2 = codeInput.\u3007\u3007888() - 1;
            final int \u3007080 = byte0(n);
            final int \u3007o8o08O = byte1(n);
            n = n2 + codeInput.readInt();
            if (\u3007080 == 43 || \u3007080 == 44) {
                codeInput.\u3007o00\u3007\u3007Oo(n, n2);
            }
            return new OneRegisterDecodedInstruction(this, \u3007080, 0, null, n, 0L, \u3007o8o08O);
        }
        
        @Override
        public void encode(final DecodedInstruction decodedInstruction, final CodeOutput codeOutput) {
            final int ooO8 = decodedInstruction.OoO8(codeOutput.\u3007\u3007888());
            codeOutput.\u30078o8o\u3007(codeUnit(decodedInstruction.\u3007O\u3007(), decodedInstruction.\u3007080()), unit0(ooO8), unit1(ooO8));
        }
    }, 
    FORMAT_32X {
        @Override
        public DecodedInstruction decode(int read, final CodeInput codeInput) throws EOFException {
            final int \u3007080 = byte0(read);
            final int \u3007o8o08O = byte1(read);
            final int read2 = codeInput.read();
            read = codeInput.read();
            return new TwoRegisterDecodedInstruction(this, \u3007080, 0, null, 0, \u3007o8o08O, read2, read);
        }
        
        @Override
        public void encode(final DecodedInstruction decodedInstruction, final CodeOutput codeOutput) {
            codeOutput.\u30078o8o\u3007(decodedInstruction.\u3007O00(), decodedInstruction.\u3007o00\u3007\u3007Oo(), decodedInstruction.O8());
        }
    }, 
    FORMAT_35C {
        @Override
        public DecodedInstruction decode(final int n, final CodeInput codeInput) throws EOFException {
            return decodeRegisterList(this, n, codeInput);
        }
        
        @Override
        public void encode(final DecodedInstruction decodedInstruction, final CodeOutput codeOutput) {
            encodeRegisterList(decodedInstruction, codeOutput);
        }
    }, 
    FORMAT_35MI {
        @Override
        public DecodedInstruction decode(final int n, final CodeInput codeInput) throws EOFException {
            return decodeRegisterList(this, n, codeInput);
        }
        
        @Override
        public void encode(final DecodedInstruction decodedInstruction, final CodeOutput codeOutput) {
            encodeRegisterList(decodedInstruction, codeOutput);
        }
    }, 
    FORMAT_35MS {
        @Override
        public DecodedInstruction decode(final int n, final CodeInput codeInput) throws EOFException {
            return decodeRegisterList(this, n, codeInput);
        }
        
        @Override
        public void encode(final DecodedInstruction decodedInstruction, final CodeOutput codeOutput) {
            encodeRegisterList(decodedInstruction, codeOutput);
        }
    }, 
    FORMAT_3RC {
        @Override
        public DecodedInstruction decode(final int n, final CodeInput codeInput) throws EOFException {
            return decodeRegisterRange(this, n, codeInput);
        }
        
        @Override
        public void encode(final DecodedInstruction decodedInstruction, final CodeOutput codeOutput) {
            encodeRegisterRange(decodedInstruction, codeOutput);
        }
    }, 
    FORMAT_3RMI {
        @Override
        public DecodedInstruction decode(final int n, final CodeInput codeInput) throws EOFException {
            return decodeRegisterRange(this, n, codeInput);
        }
        
        @Override
        public void encode(final DecodedInstruction decodedInstruction, final CodeOutput codeOutput) {
            encodeRegisterRange(decodedInstruction, codeOutput);
        }
    }, 
    FORMAT_3RMS {
        @Override
        public DecodedInstruction decode(final int n, final CodeInput codeInput) throws EOFException {
            return decodeRegisterRange(this, n, codeInput);
        }
        
        @Override
        public void encode(final DecodedInstruction decodedInstruction, final CodeOutput codeOutput) {
            encodeRegisterRange(decodedInstruction, codeOutput);
        }
    }, 
    FORMAT_45CC {
        @Override
        public DecodedInstruction decode(int \u300780\u3007808\u3007O, final CodeInput codeInput) throws EOFException {
            final int \u3007080 = byte0(\u300780\u3007808\u3007O);
            if (\u3007080 != 250) {
                throw new UnsupportedOperationException(String.valueOf(\u3007080));
            }
            final int oooo8o0\u3007 = nibble2(\u300780\u3007808\u3007O);
            final int \u3007\u3007808\u3007 = nibble3(\u300780\u3007808\u3007O);
            final int read = codeInput.read();
            final int read2 = codeInput.read();
            \u300780\u3007808\u3007O = nibble0(read2);
            final int oo0o\u3007\u3007\u3007\u30070 = nibble1(read2);
            final int oooo8o0\u30072 = nibble2(read2);
            final int \u3007\u3007808\u30072 = nibble3(read2);
            final int read3 = codeInput.read();
            final IndexType \u3007o00\u3007\u3007Oo = OpcodeInfo.\u3007o00\u3007\u3007Oo(\u3007080);
            if (\u3007\u3007808\u3007 >= 1 && \u3007\u3007808\u3007 <= 5) {
                return new InvokePolymorphicDecodedInstruction(this, \u3007080, read, \u3007o00\u3007\u3007Oo, read3, Arrays.copyOfRange(new int[] { \u300780\u3007808\u3007O, oo0o\u3007\u3007\u3007\u30070, oooo8o0\u30072, \u3007\u3007808\u30072, oooo8o0\u3007 }, 0, \u3007\u3007808\u3007));
            }
            final StringBuilder sb = new StringBuilder();
            sb.append("bogus registerCount: ");
            sb.append(Hex.OO0o\u3007\u3007\u3007\u30070(\u3007\u3007808\u3007));
            throw new DexException(sb.toString());
        }
        
        @Override
        public void encode(final DecodedInstruction decodedInstruction, final CodeOutput codeOutput) {
            final InvokePolymorphicDecodedInstruction invokePolymorphicDecodedInstruction = (InvokePolymorphicDecodedInstruction)decodedInstruction;
            codeOutput.o\u30070(codeUnit(invokePolymorphicDecodedInstruction.\u3007O\u3007(), makeByte(invokePolymorphicDecodedInstruction.\u3007oo\u3007(), invokePolymorphicDecodedInstruction.\u30070\u3007O0088o())), invokePolymorphicDecodedInstruction.OO0o\u3007\u3007\u3007\u30070(), codeUnit(invokePolymorphicDecodedInstruction.Oo08(), invokePolymorphicDecodedInstruction.\u3007\u3007888(), invokePolymorphicDecodedInstruction.oO80(), invokePolymorphicDecodedInstruction.oo88o8O()), invokePolymorphicDecodedInstruction.\u3007\u30078O0\u30078());
        }
    }, 
    FORMAT_4RCC {
        @Override
        public DecodedInstruction decode(int read, final CodeInput codeInput) throws EOFException {
            final int \u3007080 = byte0(read);
            if (\u3007080 == 251) {
                final int \u3007o8o08O = byte1(read);
                final int read2 = codeInput.read();
                read = codeInput.read();
                return new InvokePolymorphicRangeDecodedInstruction(this, \u3007080, read2, OpcodeInfo.\u3007o00\u3007\u3007Oo(\u3007080), read, \u3007o8o08O, codeInput.read());
            }
            throw new UnsupportedOperationException(String.valueOf(\u3007080));
        }
        
        @Override
        public void encode(final DecodedInstruction decodedInstruction, final CodeOutput codeOutput) {
            codeOutput.o\u30070(codeUnit(decodedInstruction.\u3007O\u3007(), decodedInstruction.\u30070\u3007O0088o()), decodedInstruction.OO0o\u3007\u3007\u3007\u30070(), decodedInstruction.o\u30070(), decodedInstruction.\u3007\u30078O0\u30078());
        }
    }, 
    FORMAT_51L {
        @Override
        public DecodedInstruction decode(int \u3007o8o08O, final CodeInput codeInput) throws EOFException {
            final int \u3007080 = byte0(\u3007o8o08O);
            \u3007o8o08O = byte1(\u3007o8o08O);
            return new OneRegisterDecodedInstruction(this, \u3007080, 0, null, 0, codeInput.readLong(), \u3007o8o08O);
        }
        
        @Override
        public void encode(final DecodedInstruction decodedInstruction, final CodeOutput codeOutput) {
            final long \u30078o8o\u3007 = decodedInstruction.\u30078o8o\u3007();
            codeOutput.\u300780\u3007808\u3007O(codeUnit(decodedInstruction.\u3007O\u3007(), decodedInstruction.\u3007080()), unit0(\u30078o8o\u3007), unit1(\u30078o8o\u3007), unit2(\u30078o8o\u3007), unit3(\u30078o8o\u3007));
        }
    }, 
    FORMAT_FILL_ARRAY_DATA_PAYLOAD {
        @Override
        public DecodedInstruction decode(final int n, final CodeInput codeInput) throws EOFException {
            final int read = codeInput.read();
            final int int1 = codeInput.readInt();
            int i = 0;
            final int n2 = 0;
            int j = 0;
            final int n3 = 0;
            if (read == 1) {
                final byte[] array = new byte[int1];
                int read2 = 0;
                for (int n4 = 1; j < int1; ++j, n4 ^= 0x1) {
                    if (n4 != 0) {
                        read2 = codeInput.read();
                    }
                    array[j] = (byte)(read2 & 0xFF);
                    read2 >>= 8;
                }
                return new FillArrayDataPayloadDecodedInstruction(this, n, array);
            }
            if (read == 2) {
                final short[] array2 = new short[int1];
                for (int k = n2; k < int1; ++k) {
                    array2[k] = (short)codeInput.read();
                }
                return new FillArrayDataPayloadDecodedInstruction(this, n, array2);
            }
            if (read == 4) {
                final int[] array3 = new int[int1];
                while (i < int1) {
                    array3[i] = codeInput.readInt();
                    ++i;
                }
                return new FillArrayDataPayloadDecodedInstruction(this, n, array3);
            }
            if (read == 8) {
                final long[] array4 = new long[int1];
                for (int l = n3; l < int1; ++l) {
                    array4[l] = codeInput.readLong();
                }
                return new FillArrayDataPayloadDecodedInstruction(this, n, array4);
            }
            final StringBuilder sb = new StringBuilder();
            sb.append("bogus element_width: ");
            sb.append(Hex.Oo08(read));
            throw new DexException(sb.toString());
        }
        
        @Override
        public void encode(final DecodedInstruction decodedInstruction, final CodeOutput codeOutput) {
            final FillArrayDataPayloadDecodedInstruction fillArrayDataPayloadDecodedInstruction = (FillArrayDataPayloadDecodedInstruction)decodedInstruction;
            final short \u3007oo\u3007 = fillArrayDataPayloadDecodedInstruction.\u3007oo\u3007();
            final Object oo88o8O = fillArrayDataPayloadDecodedInstruction.oo88o8O();
            codeOutput.oO80(fillArrayDataPayloadDecodedInstruction.\u3007O00());
            codeOutput.oO80(\u3007oo\u3007);
            codeOutput.writeInt(fillArrayDataPayloadDecodedInstruction.o\u3007O8\u3007\u3007o());
            if (\u3007oo\u3007 != 1) {
                if (\u3007oo\u3007 != 2) {
                    if (\u3007oo\u3007 != 4) {
                        if (\u3007oo\u3007 != 8) {
                            final StringBuilder sb = new StringBuilder();
                            sb.append("bogus element_width: ");
                            sb.append(Hex.Oo08(\u3007oo\u3007));
                            throw new DexException(sb.toString());
                        }
                        codeOutput.O8((long[])oo88o8O);
                    }
                    else {
                        codeOutput.Oo08((int[])oo88o8O);
                    }
                }
                else {
                    codeOutput.\u3007o\u3007((short[])oo88o8O);
                }
            }
            else {
                codeOutput.write((byte[])oo88o8O);
            }
        }
    }, 
    FORMAT_PACKED_SWITCH_PAYLOAD {
        @Override
        public DecodedInstruction decode(final int n, final CodeInput codeInput) throws EOFException {
            final int oo0o\u3007\u3007\u3007\u30070 = codeInput.OO0o\u3007\u3007\u3007\u30070();
            final int read = codeInput.read();
            final int int1 = codeInput.readInt();
            final int[] array = new int[read];
            for (int i = 0; i < read; ++i) {
                array[i] = codeInput.readInt() + (oo0o\u3007\u3007\u3007\u30070 - 1);
            }
            return new PackedSwitchPayloadDecodedInstruction(this, n, int1, array);
        }
        
        @Override
        public void encode(final DecodedInstruction decodedInstruction, final CodeOutput codeOutput) {
            final PackedSwitchPayloadDecodedInstruction packedSwitchPayloadDecodedInstruction = (PackedSwitchPayloadDecodedInstruction)decodedInstruction;
            final int[] \u3007oo\u3007 = packedSwitchPayloadDecodedInstruction.\u3007oo\u3007();
            final int oo0o\u3007\u3007\u3007\u30070 = codeOutput.OO0o\u3007\u3007\u3007\u30070();
            codeOutput.oO80(packedSwitchPayloadDecodedInstruction.\u3007O00());
            codeOutput.oO80(asUnsignedUnit(\u3007oo\u3007.length));
            codeOutput.writeInt(packedSwitchPayloadDecodedInstruction.oo88o8O());
            for (int length = \u3007oo\u3007.length, i = 0; i < length; ++i) {
                codeOutput.writeInt(\u3007oo\u3007[i] - oo0o\u3007\u3007\u3007\u30070);
            }
        }
    }, 
    FORMAT_SPARSE_SWITCH_PAYLOAD {
        @Override
        public DecodedInstruction decode(final int n, final CodeInput codeInput) throws EOFException {
            final int oo0o\u3007\u3007\u3007\u30070 = codeInput.OO0o\u3007\u3007\u3007\u30070();
            final int read = codeInput.read();
            final int[] array = new int[read];
            final int[] array2 = new int[read];
            final int n2 = 0;
            int n3 = 0;
            int i;
            while (true) {
                i = n2;
                if (n3 >= read) {
                    break;
                }
                array[n3] = codeInput.readInt();
                ++n3;
            }
            while (i < read) {
                array2[i] = codeInput.readInt() + (oo0o\u3007\u3007\u3007\u30070 - 1);
                ++i;
            }
            return new SparseSwitchPayloadDecodedInstruction(this, n, array, array2);
        }
        
        @Override
        public void encode(final DecodedInstruction decodedInstruction, final CodeOutput codeOutput) {
            final SparseSwitchPayloadDecodedInstruction sparseSwitchPayloadDecodedInstruction = (SparseSwitchPayloadDecodedInstruction)decodedInstruction;
            final int[] oo88o8O = sparseSwitchPayloadDecodedInstruction.oo88o8O();
            final int[] \u3007oo\u3007 = sparseSwitchPayloadDecodedInstruction.\u3007oo\u3007();
            final int oo0o\u3007\u3007\u3007\u30070 = codeOutput.OO0o\u3007\u3007\u3007\u30070();
            codeOutput.oO80(sparseSwitchPayloadDecodedInstruction.\u3007O00());
            codeOutput.oO80(asUnsignedUnit(\u3007oo\u3007.length));
            final int length = oo88o8O.length;
            final int n = 0;
            for (int i = 0; i < length; ++i) {
                codeOutput.writeInt(oo88o8O[i]);
            }
            for (int length2 = \u3007oo\u3007.length, j = n; j < length2; ++j) {
                codeOutput.writeInt(\u3007oo\u3007[j] - oo0o\u3007\u3007\u3007\u30070);
            }
        }
    };
    
    private static short asUnsignedUnit(final int n) {
        if ((0xFFFF0000 & n) == 0x0) {
            return (short)n;
        }
        throw new IllegalArgumentException("bogus unsigned code unit");
    }
    
    private static int byte0(final int n) {
        return n & 0xFF;
    }
    
    private static int byte1(final int n) {
        return n >> 8 & 0xFF;
    }
    
    private static int byte2(final int n) {
        return n >> 16 & 0xFF;
    }
    
    private static int byte3(final int n) {
        return n >>> 24;
    }
    
    private static short codeUnit(final int n, final int n2) {
        if ((n & 0xFFFFFF00) != 0x0) {
            throw new IllegalArgumentException("bogus lowByte");
        }
        if ((n2 & 0xFFFFFF00) == 0x0) {
            return (short)(n | n2 << 8);
        }
        throw new IllegalArgumentException("bogus highByte");
    }
    
    private static short codeUnit(final int n, final int n2, final int n3, final int n4) {
        if ((n & 0xFFFFFFF0) != 0x0) {
            throw new IllegalArgumentException("bogus nibble0");
        }
        if ((n2 & 0xFFFFFFF0) != 0x0) {
            throw new IllegalArgumentException("bogus nibble1");
        }
        if ((n3 & 0xFFFFFFF0) != 0x0) {
            throw new IllegalArgumentException("bogus nibble2");
        }
        if ((n4 & 0xFFFFFFF0) == 0x0) {
            return (short)(n | n2 << 4 | n3 << 8 | n4 << 12);
        }
        throw new IllegalArgumentException("bogus nibble3");
    }
    
    private static DecodedInstruction decodeRegisterList(final InstructionCodec instructionCodec, int nibble1, final CodeInput codeInput) throws EOFException {
        final int byte0 = byte0(nibble1);
        final int nibble2 = nibble2(nibble1);
        final int nibble3 = nibble3(nibble1);
        final int read = codeInput.read();
        final int read2 = codeInput.read();
        final int nibble4 = nibble0(read2);
        nibble1 = nibble1(read2);
        final int nibble5 = nibble2(read2);
        final int nibble6 = nibble3(read2);
        final IndexType \u3007o00\u3007\u3007Oo = OpcodeInfo.\u3007o00\u3007\u3007Oo(byte0);
        if (nibble3 == 0) {
            return new ZeroRegisterDecodedInstruction(instructionCodec, byte0, read, \u3007o00\u3007\u3007Oo, 0, 0L);
        }
        if (nibble3 == 1) {
            return new OneRegisterDecodedInstruction(instructionCodec, byte0, read, \u3007o00\u3007\u3007Oo, 0, 0L, nibble4);
        }
        if (nibble3 == 2) {
            return new TwoRegisterDecodedInstruction(instructionCodec, byte0, read, \u3007o00\u3007\u3007Oo, 0, 0L, nibble4, nibble1);
        }
        if (nibble3 == 3) {
            return new ThreeRegisterDecodedInstruction(instructionCodec, byte0, read, \u3007o00\u3007\u3007Oo, 0, 0L, nibble4, nibble1, nibble5);
        }
        if (nibble3 == 4) {
            return new FourRegisterDecodedInstruction(instructionCodec, byte0, read, \u3007o00\u3007\u3007Oo, 0, 0L, nibble4, nibble1, nibble5, nibble6);
        }
        if (nibble3 == 5) {
            return new FiveRegisterDecodedInstruction(instructionCodec, byte0, read, \u3007o00\u3007\u3007Oo, 0, 0L, nibble4, nibble1, nibble5, nibble6, nibble2);
        }
        final StringBuilder sb = new StringBuilder();
        sb.append("bogus registerCount: ");
        sb.append(Hex.OO0o\u3007\u3007\u3007\u30070(nibble3));
        throw new DexException(sb.toString());
    }
    
    private static DecodedInstruction decodeRegisterRange(final InstructionCodec instructionCodec, int read, final CodeInput codeInput) throws EOFException {
        final int byte0 = byte0(read);
        final int byte2 = byte1(read);
        read = codeInput.read();
        return new RegisterRangeDecodedInstruction(instructionCodec, byte0, read, OpcodeInfo.\u3007o00\u3007\u3007Oo(byte0), 0, 0L, codeInput.read(), byte2);
    }
    
    private static void encodeRegisterList(final DecodedInstruction decodedInstruction, final CodeOutput codeOutput) {
        codeOutput.\u30078o8o\u3007(codeUnit(decodedInstruction.\u3007O\u3007(), makeByte(decodedInstruction.oO80(), decodedInstruction.\u30070\u3007O0088o())), decodedInstruction.OO0o\u3007\u3007\u3007\u30070(), codeUnit(decodedInstruction.\u3007080(), decodedInstruction.\u3007o\u3007(), decodedInstruction.Oo08(), decodedInstruction.\u3007\u3007888()));
    }
    
    private static void encodeRegisterRange(final DecodedInstruction decodedInstruction, final CodeOutput codeOutput) {
        codeOutput.\u30078o8o\u3007(codeUnit(decodedInstruction.\u3007O\u3007(), decodedInstruction.\u30070\u3007O0088o()), decodedInstruction.OO0o\u3007\u3007\u3007\u30070(), decodedInstruction.\u3007o00\u3007\u3007Oo());
    }
    
    private static int makeByte(final int n, final int n2) {
        if ((n & 0xFFFFFFF0) != 0x0) {
            throw new IllegalArgumentException("bogus lowNibble");
        }
        if ((n2 & 0xFFFFFFF0) == 0x0) {
            return n | n2 << 4;
        }
        throw new IllegalArgumentException("bogus highNibble");
    }
    
    private static int nibble0(final int n) {
        return n & 0xF;
    }
    
    private static int nibble1(final int n) {
        return n >> 4 & 0xF;
    }
    
    private static int nibble2(final int n) {
        return n >> 8 & 0xF;
    }
    
    private static int nibble3(final int n) {
        return n >> 12 & 0xF;
    }
    
    private static short unit0(final int n) {
        return (short)n;
    }
    
    private static short unit0(final long n) {
        return (short)n;
    }
    
    private static short unit1(final int n) {
        return (short)(n >> 16);
    }
    
    private static short unit1(final long n) {
        return (short)(n >> 16);
    }
    
    private static short unit2(final long n) {
        return (short)(n >> 32);
    }
    
    private static short unit3(final long n) {
        return (short)(n >> 48);
    }
    
    public abstract DecodedInstruction decode(final int p0, final CodeInput p1) throws EOFException;
    
    public abstract void encode(final DecodedInstruction p0, final CodeOutput p1);
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.android.dx.io.instructions;

import java.io.EOFException;

public interface CodeInput extends CodeCursor
{
    int read() throws EOFException;
    
    int readInt() throws EOFException;
    
    long readLong() throws EOFException;
}

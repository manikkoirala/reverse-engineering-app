// 
// Decompiled by Procyon v0.6.0
// 

package com.android.dx.io.instructions;

import com.android.dx.io.IndexType;

public final class ZeroRegisterDecodedInstruction extends DecodedInstruction
{
    public ZeroRegisterDecodedInstruction(final InstructionCodec instructionCodec, final int n, final int n2, final IndexType indexType, final int n3, final long n4) {
        super(instructionCodec, n, n2, indexType, n3, n4);
    }
    
    @Override
    public int \u30070\u3007O0088o() {
        return 0;
    }
}

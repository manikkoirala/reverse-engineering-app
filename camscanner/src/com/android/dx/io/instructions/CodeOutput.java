// 
// Decompiled by Procyon v0.6.0
// 

package com.android.dx.io.instructions;

public interface CodeOutput extends CodeCursor
{
    void O8(final long[] p0);
    
    void Oo08(final int[] p0);
    
    void oO80(final short p0);
    
    void o\u30070(final short p0, final short p1, final short p2, final short p3);
    
    void write(final byte[] p0);
    
    void writeInt(final int p0);
    
    void \u3007080(final short p0, final short p1);
    
    void \u300780\u3007808\u3007O(final short p0, final short p1, final short p2, final short p3, final short p4);
    
    void \u30078o8o\u3007(final short p0, final short p1, final short p2);
    
    void \u3007o\u3007(final short[] p0);
}

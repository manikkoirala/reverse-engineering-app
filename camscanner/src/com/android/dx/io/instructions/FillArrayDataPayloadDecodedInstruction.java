// 
// Decompiled by Procyon v0.6.0
// 

package com.android.dx.io.instructions;

import com.android.dx.io.IndexType;

public final class FillArrayDataPayloadDecodedInstruction extends DecodedInstruction
{
    private final int oO80;
    private final int \u300780\u3007808\u3007O;
    private final Object \u3007\u3007888;
    
    private FillArrayDataPayloadDecodedInstruction(final InstructionCodec instructionCodec, final int n, final Object \u3007\u3007888, final int oo80, final int \u300780\u3007808\u3007O) {
        super(instructionCodec, n, 0, null, 0, 0L);
        this.\u3007\u3007888 = \u3007\u3007888;
        this.oO80 = oo80;
        this.\u300780\u3007808\u3007O = \u300780\u3007808\u3007O;
    }
    
    public FillArrayDataPayloadDecodedInstruction(final InstructionCodec instructionCodec, final int n, final byte[] array) {
        this(instructionCodec, n, array, array.length, 1);
    }
    
    public FillArrayDataPayloadDecodedInstruction(final InstructionCodec instructionCodec, final int n, final int[] array) {
        this(instructionCodec, n, array, array.length, 4);
    }
    
    public FillArrayDataPayloadDecodedInstruction(final InstructionCodec instructionCodec, final int n, final long[] array) {
        this(instructionCodec, n, array, array.length, 8);
    }
    
    public FillArrayDataPayloadDecodedInstruction(final InstructionCodec instructionCodec, final int n, final short[] array) {
        this(instructionCodec, n, array, array.length, 2);
    }
    
    public Object oo88o8O() {
        return this.\u3007\u3007888;
    }
    
    public int o\u3007O8\u3007\u3007o() {
        return this.oO80;
    }
    
    @Override
    public int \u30070\u3007O0088o() {
        return 0;
    }
    
    public short \u3007oo\u3007() {
        return (short)this.\u300780\u3007808\u3007O;
    }
}

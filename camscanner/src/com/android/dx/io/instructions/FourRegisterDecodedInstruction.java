// 
// Decompiled by Procyon v0.6.0
// 

package com.android.dx.io.instructions;

import com.android.dx.io.IndexType;

public final class FourRegisterDecodedInstruction extends DecodedInstruction
{
    private final int OO0o\u3007\u3007\u3007\u30070;
    private final int oO80;
    private final int \u300780\u3007808\u3007O;
    private final int \u3007\u3007888;
    
    public FourRegisterDecodedInstruction(final InstructionCodec instructionCodec, final int n, final int n2, final IndexType indexType, final int n3, final long n4, final int \u3007\u3007888, final int oo80, final int \u300780\u3007808\u3007O, final int oo0o\u3007\u3007\u3007\u30070) {
        super(instructionCodec, n, n2, indexType, n3, n4);
        this.\u3007\u3007888 = \u3007\u3007888;
        this.oO80 = oo80;
        this.\u300780\u3007808\u3007O = \u300780\u3007808\u3007O;
        this.OO0o\u3007\u3007\u3007\u30070 = oo0o\u3007\u3007\u3007\u30070;
    }
    
    @Override
    public int Oo08() {
        return this.\u300780\u3007808\u3007O;
    }
    
    @Override
    public int \u3007080() {
        return this.\u3007\u3007888;
    }
    
    @Override
    public int \u30070\u3007O0088o() {
        return 4;
    }
    
    @Override
    public int \u3007o\u3007() {
        return this.oO80;
    }
    
    @Override
    public int \u3007\u3007888() {
        return this.OO0o\u3007\u3007\u3007\u30070;
    }
}

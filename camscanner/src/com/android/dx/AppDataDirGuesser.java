// 
// Decompiled by Procyon v0.6.0
// 

package com.android.dx;

import java.util.ArrayList;
import java.lang.reflect.Field;
import java.io.File;

class AppDataDirGuesser
{
    private File O8(final String pathname) {
        File file = new File(pathname);
        if (!this.\u300780\u3007808\u3007O(file)) {
            file = null;
        }
        return file;
    }
    
    static String[] OO0o\u3007\u3007(final String s) {
        String s2 = s;
        if (s.startsWith("dexPath=")) {
            final int index = s.indexOf(44);
            if (index == -1) {
                s2 = s.substring(8);
            }
            else {
                s2 = s.substring(8, index);
            }
        }
        return s2.split(":");
    }
    
    static String OO0o\u3007\u3007\u3007\u30070(final String s) {
        if (s.contains("DexPathList")) {
            return \u3007O8o08O(s);
        }
        return \u30078o8o\u3007(s);
    }
    
    private static String \u30078o8o\u3007(String s) {
        final int lastIndex = s.lastIndexOf(91);
        if (lastIndex != -1) {
            s = s.substring(lastIndex + 1);
        }
        final int index = s.indexOf(93);
        if (index != -1) {
            s = s.substring(0, index);
        }
        return s;
    }
    
    private static String \u3007O8o08O(final String s) {
        final int beginIndex = s.indexOf("DexPathList") + 11;
        String string = s;
        if (s.length() > beginIndex + 4) {
            final String substring = s.substring(beginIndex);
            final int index = substring.indexOf(93);
            final int n = 0;
            string = s;
            if (substring.charAt(0) == '[') {
                string = s;
                if (substring.charAt(1) == '[') {
                    string = s;
                    if (index >= 0) {
                        final String[] split = substring.substring(2, index).split(",");
                        for (int i = 0; i < split.length; ++i) {
                            final int index2 = split[i].indexOf(34);
                            final int lastIndex = split[i].lastIndexOf(34);
                            if (index2 > 0 && index2 < lastIndex) {
                                split[i] = split[i].substring(index2 + 1, lastIndex);
                            }
                        }
                        final StringBuilder sb = new StringBuilder();
                        for (int length = split.length, j = n; j < length; ++j) {
                            final String str = split[j];
                            if (sb.length() > 0) {
                                sb.append(':');
                            }
                            sb.append(str);
                        }
                        string = sb.toString();
                    }
                }
            }
        }
        return string;
    }
    
    private String \u3007o00\u3007\u3007Oo(final ClassLoader obj, final Class<?> clazz) {
        try {
            final Field declaredField = clazz.getDeclaredField("path");
            declaredField.setAccessible(true);
            return (String)declaredField.get(obj);
        }
        catch (final NoSuchFieldException | IllegalAccessException | ClassCastException ex) {
            return OO0o\u3007\u3007\u3007\u30070(obj.toString());
        }
    }
    
    private ClassLoader \u3007\u3007888() {
        return AppDataDirGuesser.class.getClassLoader();
    }
    
    public File Oo08() {
        try {
            final ClassLoader \u3007\u3007888 = this.\u3007\u3007888();
            final Class<?> forName = Class.forName("dalvik.system.PathClassLoader");
            forName.cast(\u3007\u3007888);
            final File[] o\u30070 = this.o\u30070(this.\u3007o00\u3007\u3007Oo(\u3007\u3007888, forName));
            if (o\u30070.length > 0) {
                return o\u30070[0];
            }
            return null;
        }
        catch (final ClassCastException | ClassNotFoundException ex) {
            return null;
        }
    }
    
    File oO80(final String s) {
        final Integer \u3007o\u3007 = this.\u3007o\u3007();
        if (\u3007o\u3007 == null) {
            return null;
        }
        return this.O8(String.format("/data/user/%d/%s", \u3007o\u3007 / 100000, s));
    }
    
    File[] o\u30070(String s) {
        final ArrayList list = new ArrayList();
        final String[] oo0o\u3007\u3007 = OO0o\u3007\u3007(s);
        for (int length = oo0o\u3007\u3007.length, i = 0; i < length; ++i) {
            s = oo0o\u3007\u3007[i];
            if (s.startsWith("/data/app/")) {
                final int lastIndex = s.lastIndexOf(".apk");
                if (lastIndex == s.length() - 4) {
                    final int lastIndex2 = s.lastIndexOf("/", lastIndex);
                    if (lastIndex2 != 9) {
                        final int lastIndex3 = s.lastIndexOf("/", lastIndex2 - 1);
                        if (lastIndex3 != -1) {
                            final int index = s.indexOf("-", lastIndex3);
                            if (index != -1) {
                                final String substring = s.substring(lastIndex3 + 1, index);
                                final StringBuilder sb = new StringBuilder();
                                sb.append("/data/data/");
                                sb.append(substring);
                                File parent;
                                if ((parent = this.O8(sb.toString())) == null) {
                                    parent = this.oO80(substring);
                                }
                                if (parent != null) {
                                    final File file = new File(parent, "cache");
                                    if ((this.\u3007080(file) || file.mkdir()) && this.\u300780\u3007808\u3007O(file)) {
                                        list.add(file);
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        return (File[])list.toArray(new File[list.size()]);
    }
    
    boolean \u3007080(final File file) {
        return file.exists();
    }
    
    boolean \u300780\u3007808\u3007O(final File file) {
        return file.isDirectory() && file.canWrite();
    }
    
    Integer \u3007o\u3007() {
        try {
            return (Integer)Class.forName("android.os.Process").getMethod("myUid", (Class<?>[])new Class[0]).invoke(null, new Object[0]);
        }
        catch (final Exception ex) {
            return null;
        }
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.android.dx;

import com.android.dx.rop.type.Prototype;
import com.android.dx.rop.cst.CstString;
import com.android.dx.rop.cst.CstMethodRef;
import com.android.dx.rop.cst.CstNat;

public final class MethodId<D, R>
{
    final TypeList O8;
    final CstNat Oo08;
    final CstMethodRef o\u30070;
    final TypeId<D> \u3007080;
    final TypeId<R> \u3007o00\u3007\u3007Oo;
    final String \u3007o\u3007;
    
    MethodId(final TypeId<D> \u3007080, final TypeId<R> \u3007o00\u3007\u3007Oo, final String \u3007o\u3007, final TypeList o8) {
        if (\u3007080 != null && \u3007o00\u3007\u3007Oo != null && \u3007o\u3007 != null && o8 != null) {
            this.\u3007080 = \u3007080;
            this.\u3007o00\u3007\u3007Oo = \u3007o00\u3007\u3007Oo;
            this.\u3007o\u3007 = \u3007o\u3007;
            this.O8 = o8;
            final CstNat oo08 = new CstNat(new CstString(\u3007o\u3007), new CstString(this.\u3007080(false)));
            this.Oo08 = oo08;
            this.o\u30070 = new CstMethodRef(\u3007080.\u3007o\u3007, oo08);
            return;
        }
        throw null;
    }
    
    Prototype O8(final boolean b) {
        return Prototype.\u3007\u3007888(this.\u3007080(b));
    }
    
    @Override
    public boolean equals(final Object o) {
        if (o instanceof MethodId) {
            final MethodId methodId = (MethodId)o;
            if (methodId.\u3007080.equals(this.\u3007080) && methodId.\u3007o\u3007.equals(this.\u3007o\u3007) && methodId.O8.equals(this.O8) && methodId.\u3007o00\u3007\u3007Oo.equals(this.\u3007o00\u3007\u3007Oo)) {
                return true;
            }
        }
        return false;
    }
    
    @Override
    public int hashCode() {
        return (((527 + this.\u3007080.hashCode()) * 31 + this.\u3007o\u3007.hashCode()) * 31 + this.O8.hashCode()) * 31 + this.\u3007o00\u3007\u3007Oo.hashCode();
    }
    
    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append(this.\u3007080);
        sb.append(".");
        sb.append(this.\u3007o\u3007);
        sb.append("(");
        sb.append(this.O8);
        sb.append(")");
        return sb.toString();
    }
    
    String \u3007080(final boolean b) {
        final StringBuilder sb = new StringBuilder();
        sb.append("(");
        if (b) {
            sb.append(this.\u3007080.\u3007080);
        }
        final TypeId<?>[] \u3007080 = this.O8.\u3007080;
        for (int length = \u3007080.length, i = 0; i < length; ++i) {
            sb.append(\u3007080[i].\u3007080);
        }
        sb.append(")");
        sb.append(this.\u3007o00\u3007\u3007Oo.\u3007080);
        return sb.toString();
    }
    
    public boolean \u3007o00\u3007\u3007Oo() {
        return this.\u3007o\u3007.equals("<init>");
    }
    
    public boolean \u3007o\u3007() {
        return this.\u3007o\u3007.equals("<clinit>");
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.android.dex.util;

import java.io.PrintWriter;
import java.io.PrintStream;

public class ExceptionWithContext extends RuntimeException
{
    private StringBuffer context;
    
    public ExceptionWithContext(final String s) {
        this(s, null);
    }
    
    public ExceptionWithContext(String s, final Throwable cause) {
        if (s == null) {
            if (cause != null) {
                s = cause.getMessage();
            }
            else {
                s = null;
            }
        }
        super(s, cause);
        if (cause instanceof ExceptionWithContext) {
            s = ((ExceptionWithContext)cause).context.toString();
            (this.context = new StringBuffer(s.length() + 200)).append(s);
        }
        else {
            this.context = new StringBuffer(200);
        }
    }
    
    public ExceptionWithContext(final Throwable t) {
        this(null, t);
    }
    
    public static ExceptionWithContext withContext(final Throwable t, final String s) {
        ExceptionWithContext exceptionWithContext;
        if (t instanceof ExceptionWithContext) {
            exceptionWithContext = (ExceptionWithContext)t;
        }
        else {
            exceptionWithContext = new ExceptionWithContext(t);
        }
        exceptionWithContext.addContext(s);
        return exceptionWithContext;
    }
    
    public void addContext(final String str) {
        if (str != null) {
            this.context.append(str);
            if (!str.endsWith("\n")) {
                this.context.append('\n');
            }
            return;
        }
        throw new NullPointerException("str == null");
    }
    
    public String getContext() {
        return this.context.toString();
    }
    
    public void printContext(final PrintStream printStream) {
        printStream.println(this.getMessage());
    }
    
    public void printContext(final PrintWriter printWriter) {
        printWriter.println(this.getMessage());
        printWriter.print(this.context);
    }
    
    @Override
    public void printStackTrace(final PrintStream s) {
        super.printStackTrace(s);
        s.println(this.context);
    }
    
    @Override
    public void printStackTrace(final PrintWriter s) {
        super.printStackTrace(s);
        s.println(this.context);
    }
}

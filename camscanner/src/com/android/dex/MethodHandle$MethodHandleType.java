// 
// Decompiled by Procyon v0.6.0
// 

package com.android.dex;

public enum MethodHandle$MethodHandleType
{
    private static final MethodHandle$MethodHandleType[] $VALUES;
    
    METHOD_HANDLE_TYPE_INSTANCE_GET(3), 
    METHOD_HANDLE_TYPE_INSTANCE_PUT(2), 
    METHOD_HANDLE_TYPE_INVOKE_CONSTRUCTOR(7), 
    METHOD_HANDLE_TYPE_INVOKE_DIRECT(6), 
    METHOD_HANDLE_TYPE_INVOKE_INSTANCE(5), 
    METHOD_HANDLE_TYPE_INVOKE_INTERFACE(8), 
    METHOD_HANDLE_TYPE_INVOKE_STATIC(4), 
    METHOD_HANDLE_TYPE_STATIC_GET(1), 
    METHOD_HANDLE_TYPE_STATIC_PUT(0);
    
    private final int value;
    
    private MethodHandle$MethodHandleType(final int value) {
        this.value = value;
    }
    
    public boolean isField() {
        final int n = MethodHandle$1.\u3007080[this.ordinal()];
        return n == 1 || n == 2 || n == 3 || n == 4;
    }
}

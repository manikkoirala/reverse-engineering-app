// 
// Decompiled by Procyon v0.6.0
// 

package com.android.dex;

import com.android.dex.util.ByteOutput;

public final class EncodedValueCodec
{
    public static void \u3007080(final ByteOutput byteOutput, int i, long j) {
        int n;
        if ((n = 64 - Long.numberOfTrailingZeros(j)) == 0) {
            n = 1;
        }
        final int n2 = n + 7 >> 3;
        j >>= 64 - n2 * 8;
        byteOutput.writeByte(i | n2 - 1 << 5);
        for (i = n2; i > 0; --i) {
            byteOutput.writeByte((byte)j);
            j >>= 8;
        }
    }
    
    public static void \u3007o00\u3007\u3007Oo(final ByteOutput byteOutput, int i, long n) {
        final int n2 = 65 - Long.numberOfLeadingZeros(n >> 63 ^ n) + 7 >> 3;
        byteOutput.writeByte(i | n2 - 1 << 5);
        for (i = n2; i > 0; --i) {
            byteOutput.writeByte((byte)n);
            n >>= 8;
        }
    }
    
    public static void \u3007o\u3007(final ByteOutput byteOutput, int i, long j) {
        int n;
        if ((n = 64 - Long.numberOfLeadingZeros(j)) == 0) {
            n = 1;
        }
        final int n2 = n + 7 >> 3;
        byteOutput.writeByte(i | n2 - 1 << 5);
        for (i = n2; i > 0; --i) {
            byteOutput.writeByte((byte)j);
            j >>= 8;
        }
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.android.dex;

import com.android.dex.util.ByteOutput;

public final class Leb128
{
    public static int \u3007080(int i) {
        int n;
        for (i >>= 7, n = 0; i != 0; i >>= 7, ++n) {}
        return n + 1;
    }
    
    public static void \u3007o00\u3007\u3007Oo(final ByteOutput byteOutput, int n) {
        int n2 = n >> 7;
        int n3;
        if ((Integer.MIN_VALUE & n) == 0x0) {
            n3 = 0;
        }
        else {
            n3 = -1;
        }
        int n4 = 1;
        while (true) {
            final int n5 = n;
            final int n6 = n2;
            if (n4 == 0) {
                break;
            }
            if (n6 == n3 && (n6 & 0x1) == (n5 >> 6 & 0x1)) {
                n = 0;
            }
            else {
                n = 1;
            }
            int n7;
            if (n != 0) {
                n7 = 128;
            }
            else {
                n7 = 0;
            }
            byteOutput.writeByte((byte)((n5 & 0x7F) | n7));
            n2 = n6 >> 7;
            n4 = n;
            n = n6;
        }
    }
    
    public static void \u3007o\u3007(final ByteOutput byteOutput, int n) {
        while (true) {
            final int n2 = n >>> 7;
            if (n2 == 0) {
                break;
            }
            byteOutput.writeByte((byte)((n & 0x7F) | 0x80));
            n = n2;
        }
        byteOutput.writeByte((byte)(n & 0x7F));
    }
}

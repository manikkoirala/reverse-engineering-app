// 
// Decompiled by Procyon v0.6.0
// 

package com.android.dex;

import com.android.dex.util.ExceptionWithContext;

public class DexException extends ExceptionWithContext
{
    public DexException(final String s) {
        super(s);
    }
    
    public DexException(final Throwable t) {
        super(t);
    }
}

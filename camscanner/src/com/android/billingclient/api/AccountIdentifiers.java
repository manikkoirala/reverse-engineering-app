// 
// Decompiled by Procyon v0.6.0
// 

package com.android.billingclient.api;

import androidx.annotation.Nullable;

public final class AccountIdentifiers
{
    @Nullable
    private final String \u3007080;
    @Nullable
    private final String \u3007o00\u3007\u3007Oo;
    
    AccountIdentifiers(@Nullable final String \u3007080, @Nullable final String \u3007o00\u3007\u3007Oo) {
        this.\u3007080 = \u3007080;
        this.\u3007o00\u3007\u3007Oo = \u3007o00\u3007\u3007Oo;
    }
    
    @Nullable
    public String \u3007080() {
        return this.\u3007080;
    }
    
    @Nullable
    public String \u3007o00\u3007\u3007Oo() {
        return this.\u3007o00\u3007\u3007Oo;
    }
}

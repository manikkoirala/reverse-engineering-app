// 
// Decompiled by Procyon v0.6.0
// 

package com.android.billingclient.api;

import androidx.annotation.NonNull;

public final class ConsumeParams
{
    private String \u3007080;
    
    @NonNull
    public static Builder \u3007o00\u3007\u3007Oo() {
        return new Builder(null);
    }
    
    @NonNull
    public String \u3007080() {
        return this.\u3007080;
    }
    
    public static final class Builder
    {
        private String \u3007080;
        
        @NonNull
        public ConsumeParams \u3007080() {
            final String \u3007080 = this.\u3007080;
            if (\u3007080 != null) {
                final ConsumeParams consumeParams = new ConsumeParams(null);
                ConsumeParams.\u3007o\u3007(consumeParams, \u3007080);
                return consumeParams;
            }
            throw new IllegalArgumentException("Purchase token must be set");
        }
        
        @NonNull
        public Builder \u3007o00\u3007\u3007Oo(@NonNull final String \u3007080) {
            this.\u3007080 = \u3007080;
            return this;
        }
    }
}

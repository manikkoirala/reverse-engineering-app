// 
// Decompiled by Procyon v0.6.0
// 

package com.android.billingclient.api;

import android.text.TextUtils;

public final class zzbt
{
    private String \u3007080;
    
    public final zzbt \u3007080(final String \u3007080) {
        this.\u3007080 = \u3007080;
        return this;
    }
    
    public final zzbv \u3007o00\u3007\u3007Oo() {
        if (!TextUtils.isEmpty((CharSequence)this.\u3007080)) {
            return new zzbv(this.\u3007080, null, null, 0, null);
        }
        throw new IllegalArgumentException("SKU must be set.");
    }
}

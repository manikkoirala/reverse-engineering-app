// 
// Decompiled by Procyon v0.6.0
// 

package com.android.billingclient.api;

import androidx.annotation.NonNull;

public final class QueryPurchasesParams
{
    private final String \u3007080 = builder.\u3007080;
    
    @NonNull
    public static Builder \u3007080() {
        return new Builder(null);
    }
    
    @NonNull
    public final String \u3007o00\u3007\u3007Oo() {
        return this.\u3007080;
    }
    
    public static class Builder
    {
        private String \u3007080;
        
        @NonNull
        public QueryPurchasesParams \u3007080() {
            if (this.\u3007080 != null) {
                return new QueryPurchasesParams(this, null);
            }
            throw new IllegalArgumentException("Product type must be set");
        }
        
        @NonNull
        public Builder \u3007o00\u3007\u3007Oo(@NonNull final String \u3007080) {
            this.\u3007080 = \u3007080;
            return this;
        }
    }
}

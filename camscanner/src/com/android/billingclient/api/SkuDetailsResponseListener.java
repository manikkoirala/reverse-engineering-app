// 
// Decompiled by Procyon v0.6.0
// 

package com.android.billingclient.api;

import androidx.annotation.Nullable;
import java.util.List;
import androidx.annotation.NonNull;

@Deprecated
public interface SkuDetailsResponseListener
{
    void \u3007080(@NonNull final BillingResult p0, @Nullable final List<SkuDetails> p1);
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.android.billingclient.api;

import androidx.annotation.Nullable;
import org.json.JSONException;
import android.text.TextUtils;
import androidx.annotation.NonNull;
import org.json.JSONObject;

@Deprecated
public class SkuDetails
{
    private final String \u3007080;
    private final JSONObject \u3007o00\u3007\u3007Oo;
    
    public SkuDetails(@NonNull final String \u3007080) throws JSONException {
        this.\u3007080 = \u3007080;
        final JSONObject \u3007o00\u3007\u3007Oo = new JSONObject(\u3007080);
        this.\u3007o00\u3007\u3007Oo = \u3007o00\u3007\u3007Oo;
        if (TextUtils.isEmpty((CharSequence)\u3007o00\u3007\u3007Oo.optString("productId"))) {
            throw new IllegalArgumentException("SKU cannot be empty.");
        }
        if (!TextUtils.isEmpty((CharSequence)\u3007o00\u3007\u3007Oo.optString("type"))) {
            return;
        }
        throw new IllegalArgumentException("SkuType cannot be empty.");
    }
    
    public long O8() {
        return this.\u3007o00\u3007\u3007Oo.optLong("introductoryPriceAmountMicros");
    }
    
    @NonNull
    public String OO0o\u3007\u3007() {
        return this.\u3007o00\u3007\u3007Oo.optString("subscriptionPeriod");
    }
    
    public long OO0o\u3007\u3007\u3007\u30070() {
        return this.\u3007o00\u3007\u3007Oo.optLong("price_amount_micros");
    }
    
    public int Oo08() {
        return this.\u3007o00\u3007\u3007Oo.optInt("introductoryPriceCycles");
    }
    
    final String OoO8() {
        return this.\u3007o00\u3007\u3007Oo.optString("skuDetailsToken");
    }
    
    @NonNull
    public String Oooo8o0\u3007() {
        return this.\u3007o00\u3007\u3007Oo.optString("title");
    }
    
    @Override
    public boolean equals(@Nullable final Object o) {
        return this == o || (o instanceof SkuDetails && TextUtils.equals((CharSequence)this.\u3007080, (CharSequence)((SkuDetails)o).\u3007080));
    }
    
    @NonNull
    public String getType() {
        return this.\u3007o00\u3007\u3007Oo.optString("type");
    }
    
    @Override
    public int hashCode() {
        return this.\u3007080.hashCode();
    }
    
    public long oO80() {
        if (this.\u3007o00\u3007\u3007Oo.has("original_price_micros")) {
            return this.\u3007o00\u3007\u3007Oo.optLong("original_price_micros");
        }
        return this.OO0o\u3007\u3007\u3007\u30070();
    }
    
    @NonNull
    public String o\u30070() {
        return this.\u3007o00\u3007\u3007Oo.optString("introductoryPricePeriod");
    }
    
    @NonNull
    @Override
    public String toString() {
        return "SkuDetails: ".concat(String.valueOf(this.\u3007080));
    }
    
    @NonNull
    public String \u3007080() {
        return this.\u3007o00\u3007\u3007Oo.optString("description");
    }
    
    @NonNull
    public String \u30070\u3007O0088o() {
        return this.\u3007o00\u3007\u3007Oo.optString("serializedDocid");
    }
    
    @NonNull
    public String \u300780\u3007808\u3007O() {
        return this.\u3007o00\u3007\u3007Oo.optString("price");
    }
    
    @NonNull
    public String \u30078o8o\u3007() {
        return this.\u3007o00\u3007\u3007Oo.optString("price_currency_code");
    }
    
    @NonNull
    public String \u3007O00() {
        String s;
        if ((s = this.\u3007o00\u3007\u3007Oo.optString("offerIdToken")).isEmpty()) {
            s = this.\u3007o00\u3007\u3007Oo.optString("offer_id_token");
        }
        return s;
    }
    
    @NonNull
    public String \u3007O8o08O() {
        return this.\u3007o00\u3007\u3007Oo.optString("productId");
    }
    
    @NonNull
    public String \u3007O\u3007() {
        return this.\u3007o00\u3007\u3007Oo.optString("offer_id");
    }
    
    @NonNull
    public String \u3007o00\u3007\u3007Oo() {
        return this.\u3007o00\u3007\u3007Oo.optString("freeTrialPeriod");
    }
    
    @NonNull
    public String \u3007o\u3007() {
        return this.\u3007o00\u3007\u3007Oo.optString("introductoryPrice");
    }
    
    public int \u3007\u3007808\u3007() {
        return this.\u3007o00\u3007\u3007Oo.optInt("offer_type");
    }
    
    @NonNull
    public String \u3007\u3007888() {
        if (this.\u3007o00\u3007\u3007Oo.has("original_price")) {
            return this.\u3007o00\u3007\u3007Oo.optString("original_price");
        }
        return this.\u300780\u3007808\u3007O();
    }
    
    @NonNull
    public final String \u3007\u30078O0\u30078() {
        return this.\u3007o00\u3007\u3007Oo.optString("packageName");
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.android.billingclient.api;

import java.util.Iterator;
import java.util.Collection;
import java.util.List;
import androidx.annotation.NonNull;
import com.google.android.gms.internal.play_billing.zzu;

public final class QueryProductDetailsParams
{
    private final zzu \u3007080 = builder.\u3007080;
    
    @NonNull
    public static Builder \u3007080() {
        return new Builder(null);
    }
    
    public final zzu \u3007o00\u3007\u3007Oo() {
        return this.\u3007080;
    }
    
    @NonNull
    public final String \u3007o\u3007() {
        return ((List<Product>)this.\u3007080).get(0).\u3007o\u3007();
    }
    
    public static class Builder
    {
        private zzu \u3007080;
        
        @NonNull
        public QueryProductDetailsParams \u3007080() {
            return new QueryProductDetailsParams(this, null);
        }
        
        @NonNull
        public Builder \u3007o00\u3007\u3007Oo(@NonNull final List<Product> list) {
            if (list == null || list.isEmpty()) {
                throw new IllegalArgumentException("Product list cannot be empty.");
            }
            final Iterator iterator = list.iterator();
            boolean b = false;
            boolean b2 = false;
            while (iterator.hasNext()) {
                final Product product = (Product)iterator.next();
                b |= product.\u3007o\u3007().equals("inapp");
                b2 |= product.\u3007o\u3007().equals("subs");
            }
            if (b && b2) {
                throw new IllegalArgumentException("All products should be of the same product type.");
            }
            this.\u3007080 = zzu.zzk((Collection)list);
            return this;
        }
    }
    
    public static class Product
    {
        private final String \u3007080 = builder.\u3007080;
        private final String \u3007o00\u3007\u3007Oo = builder.\u3007o00\u3007\u3007Oo;
        
        @NonNull
        public static Builder \u3007080() {
            return new Builder(null);
        }
        
        @NonNull
        public final String \u3007o00\u3007\u3007Oo() {
            return this.\u3007080;
        }
        
        @NonNull
        public final String \u3007o\u3007() {
            return this.\u3007o00\u3007\u3007Oo;
        }
        
        public static class Builder
        {
            private String \u3007080;
            private String \u3007o00\u3007\u3007Oo;
            
            @NonNull
            public Product \u3007080() {
                if (this.\u3007080 == null) {
                    throw new IllegalArgumentException("Product id must be provided.");
                }
                if (this.\u3007o00\u3007\u3007Oo != null) {
                    return new Product(this, null);
                }
                throw new IllegalArgumentException("Product type must be provided.");
            }
            
            @NonNull
            public Builder \u3007o00\u3007\u3007Oo(@NonNull final String \u3007080) {
                this.\u3007080 = \u3007080;
                return this;
            }
            
            @NonNull
            public Builder \u3007o\u3007(@NonNull final String \u3007o00\u3007\u3007Oo) {
                this.\u3007o00\u3007\u3007Oo = \u3007o00\u3007\u3007Oo;
                return this;
            }
        }
    }
}

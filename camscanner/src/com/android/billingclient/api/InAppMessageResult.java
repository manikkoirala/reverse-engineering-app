// 
// Decompiled by Procyon v0.6.0
// 

package com.android.billingclient.api;

import androidx.annotation.Nullable;

public final class InAppMessageResult
{
    private final int \u3007080;
    @Nullable
    private final String \u3007o00\u3007\u3007Oo;
    
    public InAppMessageResult(final int \u3007080, @Nullable final String \u3007o00\u3007\u3007Oo) {
        this.\u3007080 = \u3007080;
        this.\u3007o00\u3007\u3007Oo = \u3007o00\u3007\u3007Oo;
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.android.billingclient.api;

import java.util.concurrent.Executors;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.ThreadFactory;

final class zzal implements ThreadFactory
{
    private final ThreadFactory \u3007080;
    private final AtomicInteger \u3007o00\u3007\u3007Oo;
    
    zzal(final BillingClientImpl billingClientImpl) {
        this.\u3007080 = Executors.defaultThreadFactory();
        this.\u3007o00\u3007\u3007Oo = new AtomicInteger(1);
    }
    
    @Override
    public final Thread newThread(final Runnable runnable) {
        final Thread thread = this.\u3007080.newThread(runnable);
        final int andIncrement = this.\u3007o00\u3007\u3007Oo.getAndIncrement();
        final StringBuilder sb = new StringBuilder();
        sb.append("PlayBillingLibrary-");
        sb.append(andIncrement);
        thread.setName(sb.toString());
        return thread;
    }
}

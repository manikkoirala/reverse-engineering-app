// 
// Decompiled by Procyon v0.6.0
// 

package com.android.billingclient.api;

import android.os.BaseBundle;
import java.util.concurrent.TimeoutException;
import java.util.concurrent.CancellationException;
import android.os.Parcelable;
import android.app.PendingIntent;
import java.util.concurrent.TimeUnit;
import android.content.pm.PackageManager$NameNotFoundException;
import android.app.Activity;
import android.content.pm.ServiceInfo;
import android.content.ServiceConnection;
import android.content.ComponentName;
import android.content.pm.ResolveInfo;
import android.content.Intent;
import java.util.Iterator;
import java.util.Collection;
import com.google.android.gms.internal.play_billing.zzu;
import android.annotation.SuppressLint;
import com.android.billingclient.ktx.BuildConfig;
import android.os.Bundle;
import org.json.JSONException;
import java.util.List;
import android.text.TextUtils;
import java.util.ArrayList;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.Executors;
import com.google.android.gms.internal.play_billing.zzb;
import java.util.concurrent.Future;
import java.util.concurrent.Callable;
import androidx.annotation.AnyThread;
import android.os.Looper;
import androidx.annotation.Nullable;
import android.os.Handler;
import java.util.concurrent.ExecutorService;
import com.google.android.gms.internal.play_billing.zze;
import android.content.Context;

class BillingClientImpl extends BillingClient
{
    private volatile zzo O8;
    private boolean OO0o\u3007\u3007;
    private int OO0o\u3007\u3007\u3007\u30070;
    private Context Oo08;
    private boolean OoO8;
    private boolean Oooo8o0\u3007;
    private boolean o800o8O;
    private boolean oO80;
    private volatile zze o\u30070;
    private volatile int \u3007080;
    private boolean \u30070\u3007O0088o;
    private boolean \u300780\u3007808\u3007O;
    private boolean \u30078o8o\u3007;
    private boolean \u3007O00;
    private ExecutorService \u3007O888o0o;
    private boolean \u3007O8o08O;
    private boolean \u3007O\u3007;
    private final String \u3007o00\u3007\u3007Oo;
    private final Handler \u3007o\u3007;
    private boolean \u3007\u3007808\u3007;
    private volatile zzap \u3007\u3007888;
    private boolean \u3007\u30078O0\u30078;
    
    @AnyThread
    private BillingClientImpl(final Context context, final boolean b, final PurchasesUpdatedListener purchasesUpdatedListener, final String \u3007o00\u3007\u3007Oo, final String s, @Nullable final zzc zzc) {
        this.\u3007080 = 0;
        this.\u3007o\u3007 = new Handler(Looper.getMainLooper());
        this.OO0o\u3007\u3007\u3007\u30070 = 0;
        this.\u3007o00\u3007\u3007Oo = \u3007o00\u3007\u3007Oo;
        this.OO0o\u3007\u3007(context, purchasesUpdatedListener, b, null);
    }
    
    @AnyThread
    BillingClientImpl(@Nullable final String s, final boolean b, final Context context, final PurchasesUpdatedListener purchasesUpdatedListener, @Nullable final zzc zzc) {
        this(context, b, purchasesUpdatedListener, \u3007O888o0o(), null, null);
    }
    
    @AnyThread
    BillingClientImpl(@Nullable final String s, final boolean ooO8, final Context context, final zzbe zzbe) {
        this.\u3007080 = 0;
        this.\u3007o\u3007 = new Handler(Looper.getMainLooper());
        this.OO0o\u3007\u3007\u3007\u30070 = 0;
        this.\u3007o00\u3007\u3007Oo = \u3007O888o0o();
        final Context applicationContext = context.getApplicationContext();
        this.Oo08 = applicationContext;
        this.O8 = new zzo(applicationContext, null);
        this.OoO8 = ooO8;
    }
    
    private void OO0o\u3007\u3007(Context applicationContext, final PurchasesUpdatedListener purchasesUpdatedListener, final boolean ooO8, @Nullable final zzc zzc) {
        applicationContext = applicationContext.getApplicationContext();
        this.Oo08 = applicationContext;
        this.O8 = new zzo(applicationContext, purchasesUpdatedListener, zzc);
        this.OoO8 = ooO8;
        this.o800o8O = (zzc != null);
    }
    
    private final BillingResult OoO8(final BillingResult billingResult) {
        if (Thread.interrupted()) {
            return billingResult;
        }
        this.\u3007o\u3007.post((Runnable)new zzag(this, billingResult));
        return billingResult;
    }
    
    private final BillingResult o800o8O() {
        BillingResult billingResult;
        if (this.\u3007080 != 0 && this.\u3007080 != 3) {
            billingResult = zzbb.OO0o\u3007\u3007\u3007\u30070;
        }
        else {
            billingResult = zzbb.OO0o\u3007\u3007;
        }
        return billingResult;
    }
    
    @Nullable
    private final Future oo88o8O(final Callable callable, long n, @Nullable final Runnable runnable, final Handler handler) {
        n *= (long)0.95;
        if (this.\u3007O888o0o == null) {
            this.\u3007O888o0o = Executors.newFixedThreadPool(zzb.zza, new zzal(this));
        }
        try {
            final Future<Object> submit = this.\u3007O888o0o.submit((Callable<Object>)callable);
            handler.postDelayed((Runnable)new zzaf(submit, runnable), n);
            return submit;
        }
        catch (final Exception ex) {
            zzb.zzp("BillingClient", "Async task throws exception!", (Throwable)ex);
            return null;
        }
    }
    
    private final Handler \u30070\u3007O0088o() {
        Handler \u3007o\u3007;
        if (Looper.myLooper() == null) {
            \u3007o\u3007 = this.\u3007o\u3007;
        }
        else {
            \u3007o\u3007 = new Handler(Looper.myLooper());
        }
        return \u3007o\u3007;
    }
    
    @SuppressLint({ "PrivateApi" })
    private static String \u3007O888o0o() {
        try {
            return (String)BuildConfig.class.getField("VERSION_NAME").get(null);
        }
        catch (final Exception ex) {
            return "5.0.0";
        }
    }
    
    private final void \u3007oo\u3007(final String s, final PurchasesResponseListener purchasesResponseListener) {
        if (!this.o\u30070()) {
            purchasesResponseListener.\u3007080(zzbb.OO0o\u3007\u3007, (List<Purchase>)zzu.zzl());
            return;
        }
        if (TextUtils.isEmpty((CharSequence)s)) {
            zzb.zzo("BillingClient", "Please provide a valid product type.");
            purchasesResponseListener.\u3007080(zzbb.\u3007\u3007888, (List<Purchase>)zzu.zzl());
            return;
        }
        if (this.oo88o8O(new zzai(this, s, purchasesResponseListener), 30000L, new zzad(purchasesResponseListener), this.\u30070\u3007O0088o()) == null) {
            purchasesResponseListener.\u3007080(this.o800o8O(), (List<Purchase>)zzu.zzl());
        }
    }
    
    @Override
    public final int O8() {
        return this.\u3007080;
    }
    
    @Override
    public void OO0o\u3007\u3007\u3007\u30070(final QueryPurchasesParams queryPurchasesParams, final PurchasesResponseListener purchasesResponseListener) {
        this.\u3007oo\u3007(queryPurchasesParams.\u3007o00\u3007\u3007Oo(), purchasesResponseListener);
    }
    
    @Override
    public final BillingResult Oo08(final String str) {
        if (!this.o\u30070()) {
            return zzbb.OO0o\u3007\u3007;
        }
        int n = 0;
        Label_0239: {
            switch (str.hashCode()) {
                case 1987365622: {
                    if (str.equals("subscriptions")) {
                        n = 0;
                        break Label_0239;
                    }
                    break;
                }
                case 207616302: {
                    if (str.equals("priceChangeConfirmation")) {
                        n = 2;
                        break Label_0239;
                    }
                    break;
                }
                case 101286: {
                    if (str.equals("fff")) {
                        n = 8;
                        break Label_0239;
                    }
                    break;
                }
                case 100293: {
                    if (str.equals("eee")) {
                        n = 7;
                        break Label_0239;
                    }
                    break;
                }
                case 99300: {
                    if (str.equals("ddd")) {
                        n = 5;
                        break Label_0239;
                    }
                    break;
                }
                case 98307: {
                    if (str.equals("ccc")) {
                        n = 6;
                        break Label_0239;
                    }
                    break;
                }
                case 97314: {
                    if (str.equals("bbb")) {
                        n = 3;
                        break Label_0239;
                    }
                    break;
                }
                case 96321: {
                    if (str.equals("aaa")) {
                        n = 4;
                        break Label_0239;
                    }
                    break;
                }
                case -422092961: {
                    if (str.equals("subscriptionsUpdate")) {
                        n = 1;
                        break Label_0239;
                    }
                    break;
                }
            }
            n = -1;
        }
        switch (n) {
            default: {
                zzb.zzo("BillingClient", "Unsupported feature: ".concat(str));
                return zzbb.o\u3007O8\u3007\u3007o;
            }
            case 8: {
                BillingResult billingResult;
                if (this.\u30070\u3007O0088o) {
                    billingResult = zzbb.\u3007O8o08O;
                }
                else {
                    billingResult = zzbb.\u3007O888o0o;
                }
                return billingResult;
            }
            case 6:
            case 7: {
                BillingResult billingResult2;
                if (this.\u3007\u30078O0\u30078) {
                    billingResult2 = zzbb.\u3007O8o08O;
                }
                else {
                    billingResult2 = zzbb.OoO8;
                }
                return billingResult2;
            }
            case 5: {
                BillingResult billingResult3;
                if (this.\u3007O\u3007) {
                    billingResult3 = zzbb.\u3007O8o08O;
                }
                else {
                    billingResult3 = zzbb.o800o8O;
                }
                return billingResult3;
            }
            case 4: {
                BillingResult billingResult4;
                if (this.\u3007O00) {
                    billingResult4 = zzbb.\u3007O8o08O;
                }
                else {
                    billingResult4 = zzbb.\u30070\u3007O0088o;
                }
                return billingResult4;
            }
            case 3: {
                BillingResult billingResult5;
                if (this.\u3007\u3007808\u3007) {
                    billingResult5 = zzbb.\u3007O8o08O;
                }
                else {
                    billingResult5 = zzbb.oo88o8O;
                }
                return billingResult5;
            }
            case 2: {
                BillingResult billingResult6;
                if (this.\u3007O8o08O) {
                    billingResult6 = zzbb.\u3007O8o08O;
                }
                else {
                    billingResult6 = zzbb.\u3007\u30078O0\u30078;
                }
                return billingResult6;
            }
            case 1: {
                BillingResult billingResult7;
                if (this.\u300780\u3007808\u3007O) {
                    billingResult7 = zzbb.\u3007O8o08O;
                }
                else {
                    billingResult7 = zzbb.\u3007O\u3007;
                }
                return billingResult7;
            }
            case 0: {
                BillingResult billingResult8;
                if (this.oO80) {
                    billingResult8 = zzbb.\u3007O8o08O;
                }
                else {
                    billingResult8 = zzbb.\u3007\u3007808\u3007;
                }
                return billingResult8;
            }
        }
    }
    
    @Override
    public final boolean o\u30070() {
        return this.\u3007080 == 2 && this.o\u30070 != null && this.\u3007\u3007888 != null;
    }
    
    @Override
    public final void \u3007080(final AcknowledgePurchaseParams acknowledgePurchaseParams, final AcknowledgePurchaseResponseListener acknowledgePurchaseResponseListener) {
        if (!this.o\u30070()) {
            acknowledgePurchaseResponseListener.\u3007080(zzbb.OO0o\u3007\u3007);
            return;
        }
        if (TextUtils.isEmpty((CharSequence)acknowledgePurchaseParams.\u3007080())) {
            zzb.zzo("BillingClient", "Please provide a valid purchase token.");
            acknowledgePurchaseResponseListener.\u3007080(zzbb.\u300780\u3007808\u3007O);
            return;
        }
        if (!this.OO0o\u3007\u3007) {
            acknowledgePurchaseResponseListener.\u3007080(zzbb.\u3007o00\u3007\u3007Oo);
            return;
        }
        if (this.oo88o8O(new zzy(this, acknowledgePurchaseParams, acknowledgePurchaseResponseListener), 30000L, new zzz(acknowledgePurchaseResponseListener), this.\u30070\u3007O0088o()) == null) {
            acknowledgePurchaseResponseListener.\u3007080(this.o800o8O());
        }
    }
    
    @Override
    public void \u300780\u3007808\u3007O(final QueryProductDetailsParams queryProductDetailsParams, final ProductDetailsResponseListener productDetailsResponseListener) {
        if (!this.o\u30070()) {
            productDetailsResponseListener.\u3007080(zzbb.OO0o\u3007\u3007, new ArrayList<ProductDetails>());
            return;
        }
        if (!this.\u30070\u3007O0088o) {
            zzb.zzo("BillingClient", "Querying product details is not supported.");
            productDetailsResponseListener.\u3007080(zzbb.\u3007O888o0o, new ArrayList<ProductDetails>());
            return;
        }
        if (this.oo88o8O(new zzs(this, queryProductDetailsParams, productDetailsResponseListener), 30000L, new zzt(productDetailsResponseListener), this.\u30070\u3007O0088o()) == null) {
            productDetailsResponseListener.\u3007080(this.o800o8O(), new ArrayList<ProductDetails>());
        }
    }
    
    @Override
    public final void \u30078o8o\u3007(final SkuDetailsParams skuDetailsParams, final SkuDetailsResponseListener skuDetailsResponseListener) {
        if (!this.o\u30070()) {
            skuDetailsResponseListener.\u3007080(zzbb.OO0o\u3007\u3007, null);
            return;
        }
        final String \u3007080 = skuDetailsParams.\u3007080();
        final List<String> \u3007o00\u3007\u3007Oo = skuDetailsParams.\u3007o00\u3007\u3007Oo();
        if (TextUtils.isEmpty((CharSequence)\u3007080)) {
            zzb.zzo("BillingClient", "Please fix the input params. SKU type can't be empty.");
            skuDetailsResponseListener.\u3007080(zzbb.o\u30070, null);
            return;
        }
        if (\u3007o00\u3007\u3007Oo != null) {
            final ArrayList list = new ArrayList();
            for (final String s : \u3007o00\u3007\u3007Oo) {
                final zzbt zzbt = new zzbt(null);
                zzbt.\u3007080(s);
                list.add(zzbt.\u3007o00\u3007\u3007Oo());
            }
            if (this.oo88o8O(new zzq(this, \u3007080, list, null, skuDetailsResponseListener), 30000L, new zzaa(skuDetailsResponseListener), this.\u30070\u3007O0088o()) == null) {
                skuDetailsResponseListener.\u3007080(this.o800o8O(), null);
            }
            return;
        }
        zzb.zzo("BillingClient", "Please fix the input params. The list of SKUs can't be empty - set SKU list or SkuWithOffer list.");
        skuDetailsResponseListener.\u3007080(zzbb.Oo08, null);
    }
    
    @Override
    public final void \u3007O8o08O(final BillingClientStateListener billingClientStateListener) {
        if (this.o\u30070()) {
            zzb.zzn("BillingClient", "Service connection is valid. No need to re-initialize.");
            billingClientStateListener.\u3007080(zzbb.\u3007O8o08O);
            return;
        }
        if (this.\u3007080 == 1) {
            zzb.zzo("BillingClient", "Client is already in the process of connecting to billing service.");
            billingClientStateListener.\u3007080(zzbb.O8);
            return;
        }
        if (this.\u3007080 == 3) {
            zzb.zzo("BillingClient", "Client was already closed and can't be reused. Please create another instance.");
            billingClientStateListener.\u3007080(zzbb.OO0o\u3007\u3007);
            return;
        }
        this.\u3007080 = 1;
        this.O8.Oo08();
        zzb.zzn("BillingClient", "Starting in-app billing setup.");
        this.\u3007\u3007888 = new zzap(this, billingClientStateListener, null);
        final Intent intent = new Intent("com.android.vending.billing.InAppBillingService.BIND");
        intent.setPackage("com.android.vending");
        final List queryIntentServices = this.Oo08.getPackageManager().queryIntentServices(intent, 0);
        if (queryIntentServices != null && !queryIntentServices.isEmpty()) {
            final ServiceInfo serviceInfo = queryIntentServices.get(0).serviceInfo;
            if (serviceInfo != null) {
                final String packageName = serviceInfo.packageName;
                final String name = serviceInfo.name;
                if ("com.android.vending".equals(packageName) && name != null) {
                    final ComponentName component = new ComponentName(packageName, name);
                    final Intent intent2 = new Intent(intent);
                    intent2.setComponent(component);
                    intent2.putExtra("playBillingLibraryVersion", this.\u3007o00\u3007\u3007Oo);
                    if (this.Oo08.bindService(intent2, (ServiceConnection)this.\u3007\u3007888, 1)) {
                        zzb.zzn("BillingClient", "Service was bonded successfully.");
                        return;
                    }
                    zzb.zzo("BillingClient", "Connection to Billing service is blocked.");
                }
                else {
                    zzb.zzo("BillingClient", "The device doesn't have valid Play Store.");
                }
            }
        }
        this.\u3007080 = 0;
        zzb.zzn("BillingClient", "Billing service unavailable on device.");
        billingClientStateListener.\u3007080(zzbb.\u3007o\u3007);
    }
    
    @Override
    public final void \u3007o00\u3007\u3007Oo(final ConsumeParams consumeParams, final ConsumeResponseListener consumeResponseListener) {
        if (!this.o\u30070()) {
            consumeResponseListener.\u3007080(zzbb.OO0o\u3007\u3007, consumeParams.\u3007080());
            return;
        }
        if (this.oo88o8O(new com.android.billingclient.api.zzu(this, consumeParams, consumeResponseListener), 30000L, new zzv(consumeResponseListener, consumeParams), this.\u30070\u3007O0088o()) == null) {
            consumeResponseListener.\u3007080(this.o800o8O(), consumeParams.\u3007080());
        }
    }
    
    @Override
    public final void \u3007o\u3007() {
        try {
            try {
                this.O8.O8();
                if (this.\u3007\u3007888 != null) {
                    this.\u3007\u3007888.\u3007o\u3007();
                }
                if (this.\u3007\u3007888 != null && this.o\u30070 != null) {
                    zzb.zzn("BillingClient", "Unbinding from service.");
                    this.Oo08.unbindService((ServiceConnection)this.\u3007\u3007888);
                    this.\u3007\u3007888 = null;
                }
                this.o\u30070 = null;
                final ExecutorService \u3007o888o0o = this.\u3007O888o0o;
                if (\u3007o888o0o != null) {
                    \u3007o888o0o.shutdownNow();
                    this.\u3007O888o0o = null;
                }
                this.\u3007080 = 3;
                return;
            }
            finally {}
        }
        catch (final Exception ex) {
            zzb.zzp("BillingClient", "There was an exception while ending connection!", (Throwable)ex);
            this.\u3007080 = 3;
            return;
        }
        this.\u3007080 = 3;
    }
    
    @Override
    public final BillingResult \u3007\u3007888(Activity ex, BillingFlowParams billingFlowParams) {
        if (!this.o\u30070()) {
            final BillingResult oo0o\u3007\u3007 = zzbb.OO0o\u3007\u3007;
            this.OoO8(oo0o\u3007\u3007);
            return oo0o\u3007\u3007;
        }
        final ArrayList o\u30070 = billingFlowParams.o\u30070();
        final List \u3007\u3007888 = billingFlowParams.\u3007\u3007888();
        final SkuDetails skuDetails = (SkuDetails)com.google.android.gms.internal.play_billing.zzz.zza((Iterable)o\u30070, (Object)null);
        final BillingFlowParams.ProductDetailsParams productDetailsParams = (BillingFlowParams.ProductDetailsParams)com.google.android.gms.internal.play_billing.zzz.zza((Iterable)\u3007\u3007888, (Object)null);
        String s;
        String s2;
        if (skuDetails != null) {
            s = skuDetails.\u3007O8o08O();
            s2 = skuDetails.getType();
        }
        else {
            s = productDetailsParams.\u3007o00\u3007\u3007Oo().O8();
            s2 = productDetailsParams.\u3007o00\u3007\u3007Oo().Oo08();
        }
        final boolean equals = s2.equals("subs");
        Object zzk = "BillingClient";
        if (equals && !this.oO80) {
            zzb.zzo("BillingClient", "Current client doesn't support subscriptions.");
            final BillingResult \u3007\u3007808\u3007 = zzbb.\u3007\u3007808\u3007;
            this.OoO8(\u3007\u3007808\u3007);
            return \u3007\u3007808\u3007;
        }
        if (billingFlowParams.\u3007O\u3007() && !this.\u30078o8o\u3007) {
            zzb.zzo("BillingClient", "Current client doesn't support extra params for buy intent.");
            final BillingResult oo80 = zzbb.oO80;
            this.OoO8(oo80);
            return oo80;
        }
        if (o\u30070.size() > 1 && !this.\u3007\u30078O0\u30078) {
            zzb.zzo("BillingClient", "Current client doesn't support multi-item purchases.");
            final BillingResult ooO8 = zzbb.OoO8;
            this.OoO8(ooO8);
            return ooO8;
        }
        if (!\u3007\u3007888.isEmpty() && !this.\u30070\u3007O0088o) {
            zzb.zzo("BillingClient", "Current client doesn't support purchases with ProductDetails.");
            final BillingResult \u3007o888o0o = zzbb.\u3007O888o0o;
            this.OoO8(\u3007o888o0o);
            return \u3007o888o0o;
        }
        Label_1343: {
            if (!this.\u30078o8o\u3007) {
                break Label_1343;
            }
            final Bundle zzf = zzb.zzf(billingFlowParams, this.OO0o\u3007\u3007, this.OoO8, this.o800o8O, this.\u3007o00\u3007\u3007Oo);
            BillingFlowParams.ProductDetailsParams productDetailsParams2;
            SkuDetails skuDetails3;
            if (!o\u30070.isEmpty()) {
                final ArrayList<String> list = new ArrayList<String>();
                final ArrayList<String> list2 = new ArrayList<String>();
                final ArrayList<String> list3 = new ArrayList<String>();
                final ArrayList<Integer> list4 = new ArrayList<Integer>();
                final ArrayList<String> list5 = new ArrayList<String>();
                final Iterator iterator = o\u30070.iterator();
                boolean b = false;
                boolean b2 = false;
                boolean b3 = false;
                boolean b4 = false;
                while (iterator.hasNext()) {
                    final SkuDetails skuDetails2 = (SkuDetails)iterator.next();
                    if (!skuDetails2.OoO8().isEmpty()) {
                        list.add(skuDetails2.OoO8());
                    }
                    final String \u3007o00 = skuDetails2.\u3007O00();
                    final String \u3007o\u3007 = skuDetails2.\u3007O\u3007();
                    final int \u3007\u3007808\u30072 = skuDetails2.\u3007\u3007808\u3007();
                    final String \u30070\u3007O0088o = skuDetails2.\u30070\u3007O0088o();
                    list2.add(\u3007o00);
                    b |= (TextUtils.isEmpty((CharSequence)\u3007o00) ^ true);
                    list3.add(\u3007o\u3007);
                    final boolean b5 = b2 | (TextUtils.isEmpty((CharSequence)\u3007o\u3007) ^ true);
                    list4.add(\u3007\u3007808\u30072);
                    b3 |= (\u3007\u3007808\u30072 != 0);
                    b4 |= (TextUtils.isEmpty((CharSequence)\u30070\u3007O0088o) ^ true);
                    list5.add(\u30070\u3007O0088o);
                    b2 = b5;
                }
                final Object o = zzk;
                if (!list.isEmpty()) {
                    zzf.putStringArrayList("skuDetailsTokens", (ArrayList)list);
                }
                if (b) {
                    zzf.putStringArrayList("SKU_OFFER_ID_TOKEN_LIST", (ArrayList)list2);
                }
                if (b2) {
                    zzf.putStringArrayList("SKU_OFFER_ID_LIST", (ArrayList)list3);
                }
                if (b3) {
                    zzf.putIntegerArrayList("SKU_OFFER_TYPE_LIST", (ArrayList)list4);
                }
                if (b4) {
                    zzf.putStringArrayList("SKU_SERIALIZED_DOCID_LIST", (ArrayList)list5);
                }
                zzk = o;
                productDetailsParams2 = productDetailsParams;
                skuDetails3 = skuDetails;
                if (o\u30070.size() > 1) {
                    final ArrayList list6 = new ArrayList<String>(o\u30070.size() - 1);
                    final ArrayList list7 = new ArrayList<String>(o\u30070.size() - 1);
                    for (int i = 1; i < o\u30070.size(); ++i) {
                        list6.add(((SkuDetails)o\u30070.get(i)).\u3007O8o08O());
                        list7.add(((SkuDetails)o\u30070.get(i)).getType());
                    }
                    zzf.putStringArrayList("additionalSkus", list6);
                    zzf.putStringArrayList("additionalSkuTypes", list7);
                    zzk = o;
                    productDetailsParams2 = productDetailsParams;
                    skuDetails3 = skuDetails;
                }
            }
            else {
                final String s3 = "BillingClient";
                final ArrayList list8 = new ArrayList<String>(\u3007\u3007888.size() - 1);
                final ArrayList list9 = new ArrayList<String>(\u3007\u3007888.size() - 1);
                final ArrayList<String> list10 = new ArrayList<String>();
                final ArrayList<String> list11 = new ArrayList<String>();
                for (int j = 0; j < \u3007\u3007888.size(); ++j) {
                    final BillingFlowParams.ProductDetailsParams productDetailsParams3 = \u3007\u3007888.get(j);
                    final ProductDetails \u3007o00\u3007\u3007Oo = productDetailsParams3.\u3007o00\u3007\u3007Oo();
                    if (!\u3007o00\u3007\u3007Oo.\u300780\u3007808\u3007O().isEmpty()) {
                        list10.add(\u3007o00\u3007\u3007Oo.\u300780\u3007808\u3007O());
                    }
                    list11.add(productDetailsParams3.\u3007o\u3007());
                    if (j > 0) {
                        list8.add(((BillingFlowParams.ProductDetailsParams)\u3007\u3007888.get(j)).\u3007o00\u3007\u3007Oo().O8());
                        list9.add(((BillingFlowParams.ProductDetailsParams)\u3007\u3007888.get(j)).\u3007o00\u3007\u3007Oo().Oo08());
                    }
                }
                zzf.putStringArrayList("SKU_OFFER_ID_TOKEN_LIST", (ArrayList)list11);
                if (!list10.isEmpty()) {
                    zzf.putStringArrayList("skuDetailsTokens", (ArrayList)list10);
                }
                zzk = s3;
                productDetailsParams2 = productDetailsParams;
                skuDetails3 = skuDetails;
                if (!list8.isEmpty()) {
                    zzf.putStringArrayList("additionalSkus", list8);
                    zzf.putStringArrayList("additionalSkuTypes", list9);
                    skuDetails3 = skuDetails;
                    productDetailsParams2 = productDetailsParams;
                    zzk = s3;
                }
            }
            if (((BaseBundle)zzf).containsKey("SKU_OFFER_ID_TOKEN_LIST") && !this.\u3007O\u3007) {
                final BillingResult o800o8O = zzbb.o800o8O;
                this.OoO8(o800o8O);
                return o800o8O;
            }
            int zzb = 0;
            Label_1135: {
                if (skuDetails3 != null && !TextUtils.isEmpty((CharSequence)skuDetails3.\u3007\u30078O0\u30078())) {
                    ((BaseBundle)zzf).putString("skuPackageName", skuDetails3.\u3007\u30078O0\u30078());
                }
                else {
                    if (productDetailsParams2 == null || TextUtils.isEmpty((CharSequence)productDetailsParams2.\u3007o00\u3007\u3007Oo().oO80())) {
                        zzb = 0;
                        break Label_1135;
                    }
                    ((BaseBundle)zzf).putString("skuPackageName", productDetailsParams2.\u3007o00\u3007\u3007Oo().oO80());
                }
                zzb = 1;
            }
            if (!TextUtils.isEmpty((CharSequence)null)) {
                ((BaseBundle)zzf).putString("accountName", (String)null);
            }
            final Intent intent = ((Activity)ex).getIntent();
            String stringExtra;
            String versionName = null;
            Bundle bundle;
            String s4;
            String s5;
            Future future;
            Bundle bundle2;
            Block_47_Outer:Label_1240_Outer:
            while (true) {
                if (intent == null) {
                    com.google.android.gms.internal.play_billing.zzb.zzo((String)zzk, "Activity's intent is null.");
                    break Label_1251;
                }
                if (TextUtils.isEmpty((CharSequence)intent.getStringExtra("PROXY_PACKAGE"))) {
                    break Label_1251;
                }
                stringExtra = intent.getStringExtra("PROXY_PACKAGE");
                ((BaseBundle)zzf).putString("proxyPackage", stringExtra);
                try {
                    versionName = this.Oo08.getPackageManager().getPackageInfo(stringExtra, 0).versionName;
                    bundle = zzf;
                    s4 = "proxyPackageVersion";
                    s5 = versionName;
                    ((BaseBundle)bundle).putString(s4, s5);
                    break Label_1251;
                }
                catch (final PackageManager$NameNotFoundException ex2) {}
                while (true) {
                    try {
                        bundle = zzf;
                        s4 = "proxyPackageVersion";
                        s5 = versionName;
                        ((BaseBundle)bundle).putString(s4, s5);
                        if (this.\u30070\u3007O0088o && !\u3007\u3007888.isEmpty()) {
                            zzb = 17;
                        }
                        else if (this.\u3007O00 && zzb != 0) {
                            zzb = 15;
                        }
                        else if (this.OO0o\u3007\u3007) {
                            zzb = 9;
                        }
                        else {
                            zzb = 6;
                        }
                        future = this.oo88o8O(new zzab(this, zzb, s, s2, billingFlowParams, zzf), 5000L, null, this.\u3007o\u3007);
                        billingFlowParams = (BillingFlowParams)zzk;
                        while (true) {
                            try {
                                bundle2 = future.get(5000L, TimeUnit.MILLISECONDS);
                                zzb = com.google.android.gms.internal.play_billing.zzb.zzb(bundle2, (String)billingFlowParams);
                                zzk = com.google.android.gms.internal.play_billing.zzb.zzk(bundle2, (String)billingFlowParams);
                                if (zzb != 0) {
                                    ex = (Exception)new StringBuilder();
                                    ((StringBuilder)ex).append("Unable to buy item, Error response code: ");
                                    ((StringBuilder)ex).append(zzb);
                                    com.google.android.gms.internal.play_billing.zzb.zzo((String)billingFlowParams, ((StringBuilder)ex).toString());
                                    ex = (Exception)BillingResult.\u3007o\u3007();
                                    ((BillingResult.Builder)ex).\u3007o\u3007(zzb);
                                    ((BillingResult.Builder)ex).\u3007o00\u3007\u3007Oo((String)zzk);
                                    ex = (Exception)((BillingResult.Builder)ex).\u3007080();
                                    this.OoO8((BillingResult)ex);
                                    return (BillingResult)ex;
                                }
                                zzk = new Intent((Context)ex, (Class)ProxyBillingActivity.class);
                                ((Intent)zzk).putExtra("BUY_INTENT", (Parcelable)bundle2.getParcelable("BUY_INTENT"));
                                ((Activity)ex).startActivity((Intent)zzk);
                                return zzbb.\u3007O8o08O;
                            }
                            catch (Exception ex) {
                                com.google.android.gms.internal.play_billing.zzb.zzp((String)billingFlowParams, "Exception while launching billing flow. Try to reconnect", (Throwable)ex);
                                ex = (Exception)zzbb.OO0o\u3007\u3007;
                                this.OoO8((BillingResult)ex);
                                return (BillingResult)ex;
                            }
                            catch (final CancellationException ex) {}
                            catch (final TimeoutException ex3) {}
                            com.google.android.gms.internal.play_billing.zzb.zzp((String)billingFlowParams, "Time out while launching billing flow. Try to reconnect", (Throwable)ex);
                            ex = (Exception)zzbb.Oooo8o0\u3007;
                            this.OoO8((BillingResult)ex);
                            return (BillingResult)ex;
                            ((BaseBundle)zzf).putString("proxyPackageVersion", "package not found");
                            continue Block_47_Outer;
                            billingFlowParams = (BillingFlowParams)"BillingClient";
                            future = this.oo88o8O(new zzac(this, s, s2), 5000L, null, this.\u3007o\u3007);
                            continue Label_1240_Outer;
                        }
                    }
                    catch (final PackageManager$NameNotFoundException ex4) {
                        continue;
                    }
                    break;
                }
                break;
            }
        }
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.android.billingclient.api;

import androidx.annotation.Nullable;
import java.util.List;
import androidx.annotation.NonNull;

public interface PurchasesUpdatedListener
{
    void \u3007o\u3007(@NonNull final BillingResult p0, @Nullable final List<Purchase> p1);
}

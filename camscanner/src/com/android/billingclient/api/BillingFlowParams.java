// 
// Decompiled by Procyon v0.6.0
// 

package com.android.billingclient.api;

import android.text.TextUtils;
import com.google.android.gms.internal.play_billing.zzm;
import java.util.Iterator;
import java.util.List;
import java.util.Collection;
import androidx.annotation.Nullable;
import androidx.annotation.NonNull;
import java.util.ArrayList;
import com.google.android.gms.internal.play_billing.zzu;

public class BillingFlowParams
{
    private SubscriptionUpdateParams O8;
    private zzu Oo08;
    private ArrayList o\u30070;
    private boolean \u3007080;
    private String \u3007o00\u3007\u3007Oo;
    private String \u3007o\u3007;
    private boolean \u3007\u3007888;
    
    @NonNull
    public static Builder \u3007080() {
        return new Builder(null);
    }
    
    @Nullable
    public final String O8() {
        return this.\u3007o\u3007;
    }
    
    @Nullable
    public final String Oo08() {
        return this.O8.O8();
    }
    
    @NonNull
    public final ArrayList o\u30070() {
        final ArrayList list = new ArrayList();
        list.addAll(this.o\u30070);
        return list;
    }
    
    final boolean \u3007O\u3007() {
        return this.\u3007o00\u3007\u3007Oo != null || this.\u3007o\u3007 != null || this.O8.\u3007o00\u3007\u3007Oo() != 0 || this.\u3007080 || this.\u3007\u3007888;
    }
    
    public final int \u3007o00\u3007\u3007Oo() {
        return this.O8.\u3007o00\u3007\u3007Oo();
    }
    
    @Nullable
    public final String \u3007o\u3007() {
        return this.\u3007o00\u3007\u3007Oo;
    }
    
    public final boolean \u3007\u3007808\u3007() {
        return this.\u3007\u3007888;
    }
    
    @NonNull
    public final List \u3007\u3007888() {
        return (List)this.Oo08;
    }
    
    public static class Builder
    {
        private ArrayList O8;
        private boolean Oo08;
        private SubscriptionUpdateParams.Builder o\u30070;
        private String \u3007080;
        private String \u3007o00\u3007\u3007Oo;
        private List \u3007o\u3007;
        
        @NonNull
        public Builder O8(@NonNull final List<ProductDetailsParams> c) {
            this.\u3007o\u3007 = new ArrayList(c);
            return this;
        }
        
        @Deprecated
        @NonNull
        public Builder Oo08(@NonNull final SkuDetails e) {
            final ArrayList o8 = new ArrayList();
            o8.add(e);
            this.O8 = o8;
            return this;
        }
        
        @NonNull
        public Builder o\u30070(@NonNull final SubscriptionUpdateParams subscriptionUpdateParams) {
            this.o\u30070 = SubscriptionUpdateParams.\u3007o\u3007(subscriptionUpdateParams);
            return this;
        }
        
        @NonNull
        public BillingFlowParams \u3007080() {
            final ArrayList o8 = this.O8;
            final boolean b = true;
            final boolean b2 = o8 != null && !o8.isEmpty();
            final List \u3007o\u3007 = this.\u3007o\u3007;
            final boolean b3 = \u3007o\u3007 != null && !\u3007o\u3007.isEmpty();
            if (!b2 && !b3) {
                throw new IllegalArgumentException("Details of the products must be provided.");
            }
            if (b2 && b3) {
                throw new IllegalArgumentException("Set SkuDetails or ProductDetailsParams, not both.");
            }
            if (b2) {
                if (this.O8.contains(null)) {
                    throw new IllegalArgumentException("SKU cannot be null.");
                }
                if (this.O8.size() > 1) {
                    final SkuDetails skuDetails = this.O8.get(0);
                    final String type = skuDetails.getType();
                    final ArrayList o9 = this.O8;
                    for (int size = o9.size(), i = 0; i < size; ++i) {
                        final SkuDetails skuDetails2 = (SkuDetails)o9.get(i);
                        if (!type.equals("play_pass_subs") && !skuDetails2.getType().equals("play_pass_subs") && !type.equals(skuDetails2.getType())) {
                            throw new IllegalArgumentException("SKUs should have the same type.");
                        }
                    }
                    final String \u3007\u30078O0\u30078 = skuDetails.\u3007\u30078O0\u30078();
                    final ArrayList o10 = this.O8;
                    for (int size2 = o10.size(), j = 0; j < size2; ++j) {
                        final SkuDetails skuDetails3 = (SkuDetails)o10.get(j);
                        if (!type.equals("play_pass_subs") && !skuDetails3.getType().equals("play_pass_subs") && !\u3007\u30078O0\u30078.equals(skuDetails3.\u3007\u30078O0\u30078())) {
                            throw new IllegalArgumentException("All SKUs must have the same package name.");
                        }
                    }
                }
            }
            else {
                final ProductDetailsParams productDetailsParams = this.\u3007o\u3007.get(0);
                for (int k = 0; k < this.\u3007o\u3007.size(); ++k) {
                    final ProductDetailsParams productDetailsParams2 = this.\u3007o\u3007.get(k);
                    if (productDetailsParams2 == null) {
                        throw new IllegalArgumentException("ProductDetailsParams cannot be null.");
                    }
                    if (k != 0 && !productDetailsParams2.\u3007o00\u3007\u3007Oo().Oo08().equals(productDetailsParams.\u3007o00\u3007\u3007Oo().Oo08()) && !productDetailsParams2.\u3007o00\u3007\u3007Oo().Oo08().equals("play_pass_subs")) {
                        throw new IllegalArgumentException("All products should have same ProductType.");
                    }
                }
                final String oo80 = productDetailsParams.\u3007o00\u3007\u3007Oo().oO80();
                for (final ProductDetailsParams productDetailsParams3 : this.\u3007o\u3007) {
                    if (!productDetailsParams.\u3007o00\u3007\u3007Oo().Oo08().equals("play_pass_subs") && !productDetailsParams3.\u3007o00\u3007\u3007Oo().Oo08().equals("play_pass_subs")) {
                        if (oo80.equals(productDetailsParams3.\u3007o00\u3007\u3007Oo().oO80())) {
                            continue;
                        }
                        throw new IllegalArgumentException("All products must have the same package name.");
                    }
                }
            }
            final BillingFlowParams billingFlowParams = new BillingFlowParams(null);
            boolean b4 = false;
            Label_0646: {
                if (b2) {
                    b4 = b;
                    if (!this.O8.get(0).\u3007\u30078O0\u30078().isEmpty()) {
                        break Label_0646;
                    }
                }
                b4 = (b3 && !((ProductDetailsParams)this.\u3007o\u3007.get(0)).\u3007o00\u3007\u3007Oo().oO80().isEmpty() && b);
            }
            BillingFlowParams.oO80(billingFlowParams, b4);
            BillingFlowParams.OO0o\u3007\u3007\u3007\u30070(billingFlowParams, this.\u3007080);
            BillingFlowParams.\u30078o8o\u3007(billingFlowParams, this.\u3007o00\u3007\u3007Oo);
            BillingFlowParams.Oooo8o0\u3007(billingFlowParams, this.o\u30070.\u3007080());
            final ArrayList o11 = this.O8;
            ArrayList list;
            if (o11 != null) {
                list = new ArrayList(o11);
            }
            else {
                list = new ArrayList();
            }
            BillingFlowParams.OO0o\u3007\u3007(billingFlowParams, (ArrayList)list);
            BillingFlowParams.\u300780\u3007808\u3007O(billingFlowParams, this.Oo08);
            final List \u3007o\u30072 = this.\u3007o\u3007;
            zzu zzu;
            if (\u3007o\u30072 != null) {
                zzu = com.google.android.gms.internal.play_billing.zzu.zzk((Collection)\u3007o\u30072);
            }
            else {
                zzu = com.google.android.gms.internal.play_billing.zzu.zzl();
            }
            BillingFlowParams.\u3007O8o08O(billingFlowParams, zzu);
            return billingFlowParams;
        }
        
        @NonNull
        public Builder \u3007o00\u3007\u3007Oo(@NonNull final String \u3007080) {
            this.\u3007080 = \u3007080;
            return this;
        }
        
        @NonNull
        public Builder \u3007o\u3007(@NonNull final String \u3007o00\u3007\u3007Oo) {
            this.\u3007o00\u3007\u3007Oo = \u3007o00\u3007\u3007Oo;
            return this;
        }
    }
    
    public static final class ProductDetailsParams
    {
        private final ProductDetails \u3007080 = builder.\u3007080;
        private final String \u3007o00\u3007\u3007Oo = builder.\u3007o00\u3007\u3007Oo;
        
        @NonNull
        public static Builder \u3007080() {
            return new Builder(null);
        }
        
        @NonNull
        public final ProductDetails \u3007o00\u3007\u3007Oo() {
            return this.\u3007080;
        }
        
        @NonNull
        public final String \u3007o\u3007() {
            return this.\u3007o00\u3007\u3007Oo;
        }
        
        public static class Builder
        {
            private ProductDetails \u3007080;
            private String \u3007o00\u3007\u3007Oo;
            
            @NonNull
            public ProductDetailsParams \u3007080() {
                zzm.zzc((Object)this.\u3007080, (Object)"ProductDetails is required for constructing ProductDetailsParams.");
                zzm.zzc((Object)this.\u3007o00\u3007\u3007Oo, (Object)"offerToken is required for constructing ProductDetailsParams.");
                return new ProductDetailsParams(this, null);
            }
            
            @NonNull
            public Builder \u3007o00\u3007\u3007Oo(@NonNull final String \u3007o00\u3007\u3007Oo) {
                this.\u3007o00\u3007\u3007Oo = \u3007o00\u3007\u3007Oo;
                return this;
            }
            
            @NonNull
            public Builder \u3007o\u3007(@NonNull final ProductDetails \u3007080) {
                this.\u3007080 = \u3007080;
                if (\u3007080.\u3007o\u3007() != null) {
                    \u3007080.\u3007o\u3007().getClass();
                    this.\u3007o00\u3007\u3007Oo = \u3007080.\u3007o\u3007().O8();
                }
                return this;
            }
        }
    }
    
    public static class SubscriptionUpdateParams
    {
        private String \u3007080;
        private int \u3007o00\u3007\u3007Oo = 0;
        
        @NonNull
        public static Builder \u3007080() {
            return new Builder(null);
        }
        
        final String O8() {
            return this.\u3007080;
        }
        
        final int \u3007o00\u3007\u3007Oo() {
            return this.\u3007o00\u3007\u3007Oo;
        }
        
        public static class Builder
        {
            private String \u3007080;
            private boolean \u3007o00\u3007\u3007Oo;
            private int \u3007o\u3007 = 0;
            
            @NonNull
            public Builder O8(final int \u3007o\u3007) {
                this.\u3007o\u3007 = \u3007o\u3007;
                return this;
            }
            
            @Deprecated
            @NonNull
            public Builder Oo08(final int \u3007o\u3007) {
                this.\u3007o\u3007 = \u3007o\u3007;
                return this;
            }
            
            @NonNull
            public SubscriptionUpdateParams \u3007080() {
                final boolean b = !TextUtils.isEmpty((CharSequence)this.\u3007080) || !TextUtils.isEmpty((CharSequence)null);
                final boolean b2 = true ^ TextUtils.isEmpty((CharSequence)null);
                if (b && b2) {
                    throw new IllegalArgumentException("Please provide Old SKU purchase information(token/id) or original external transaction id, not both.");
                }
                if (!this.\u3007o00\u3007\u3007Oo && !b && !b2) {
                    throw new IllegalArgumentException("Old SKU purchase information(token/id) or original external transaction id must be provided.");
                }
                final SubscriptionUpdateParams subscriptionUpdateParams = new SubscriptionUpdateParams(null);
                SubscriptionUpdateParams.Oo08(subscriptionUpdateParams, this.\u3007080);
                SubscriptionUpdateParams.o\u30070(subscriptionUpdateParams, this.\u3007o\u3007);
                return subscriptionUpdateParams;
            }
            
            @NonNull
            public Builder \u3007o00\u3007\u3007Oo(@NonNull final String \u3007080) {
                this.\u3007080 = \u3007080;
                return this;
            }
            
            @Deprecated
            @NonNull
            public Builder \u3007o\u3007(@NonNull final String \u3007080) {
                this.\u3007080 = \u3007080;
                return this;
            }
        }
    }
}

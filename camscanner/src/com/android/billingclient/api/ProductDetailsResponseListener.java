// 
// Decompiled by Procyon v0.6.0
// 

package com.android.billingclient.api;

import java.util.List;
import androidx.annotation.NonNull;

public interface ProductDetailsResponseListener
{
    void \u3007080(@NonNull final BillingResult p0, @NonNull final List<ProductDetails> p1);
}

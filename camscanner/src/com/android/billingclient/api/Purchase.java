// 
// Decompiled by Procyon v0.6.0
// 

package com.android.billingclient.api;

import android.text.TextUtils;
import androidx.annotation.Nullable;
import java.util.List;
import org.json.JSONArray;
import java.util.ArrayList;
import org.json.JSONException;
import androidx.annotation.NonNull;
import org.json.JSONObject;

public class Purchase
{
    private final String \u3007080;
    private final String \u3007o00\u3007\u3007Oo;
    private final JSONObject \u3007o\u3007;
    
    public Purchase(@NonNull final String \u3007080, @NonNull final String \u3007o00\u3007\u3007Oo) throws JSONException {
        this.\u3007080 = \u3007080;
        this.\u3007o00\u3007\u3007Oo = \u3007o00\u3007\u3007Oo;
        this.\u3007o\u3007 = new JSONObject(\u3007080);
    }
    
    private final ArrayList \u300780\u3007808\u3007O() {
        final ArrayList list = new ArrayList();
        if (this.\u3007o\u3007.has("productIds")) {
            final JSONArray optJSONArray = this.\u3007o\u3007.optJSONArray("productIds");
            if (optJSONArray != null) {
                for (int i = 0; i < optJSONArray.length(); ++i) {
                    list.add(optJSONArray.optString(i));
                }
            }
        }
        else if (this.\u3007o\u3007.has("productId")) {
            list.add(this.\u3007o\u3007.optString("productId"));
        }
        return list;
    }
    
    @NonNull
    public List<String> O8() {
        return this.\u300780\u3007808\u3007O();
    }
    
    public int Oo08() {
        if (this.\u3007o\u3007.optInt("purchaseState", 1) != 4) {
            return 1;
        }
        return 2;
    }
    
    @Override
    public boolean equals(@Nullable final Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Purchase)) {
            return false;
        }
        final Purchase purchase = (Purchase)o;
        return TextUtils.equals((CharSequence)this.\u3007080, (CharSequence)purchase.\u3007o\u3007()) && TextUtils.equals((CharSequence)this.\u3007o00\u3007\u3007Oo, (CharSequence)purchase.\u3007\u3007888());
    }
    
    @Override
    public int hashCode() {
        return this.\u3007080.hashCode();
    }
    
    public boolean oO80() {
        return this.\u3007o\u3007.optBoolean("acknowledged", true);
    }
    
    @NonNull
    public String o\u30070() {
        final JSONObject \u3007o\u3007 = this.\u3007o\u3007;
        return \u3007o\u3007.optString("token", \u3007o\u3007.optString("purchaseToken"));
    }
    
    @NonNull
    @Override
    public String toString() {
        return "Purchase. Json: ".concat(String.valueOf(this.\u3007080));
    }
    
    @Nullable
    public AccountIdentifiers \u3007080() {
        final String optString = this.\u3007o\u3007.optString("obfuscatedAccountId");
        final String optString2 = this.\u3007o\u3007.optString("obfuscatedProfileId");
        if (optString == null && optString2 == null) {
            return null;
        }
        return new AccountIdentifiers(optString, optString2);
    }
    
    @NonNull
    public String \u3007o00\u3007\u3007Oo() {
        return this.\u3007o\u3007.optString("orderId");
    }
    
    @NonNull
    public String \u3007o\u3007() {
        return this.\u3007080;
    }
    
    @NonNull
    public String \u3007\u3007888() {
        return this.\u3007o00\u3007\u3007Oo;
    }
}

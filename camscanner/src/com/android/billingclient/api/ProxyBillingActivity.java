// 
// Decompiled by Procyon v0.6.0
// 

package com.android.billingclient.api;

import android.os.BaseBundle;
import android.content.Context;
import android.os.Parcelable;
import androidx.annotation.NonNull;
import android.content.IntentSender$SendIntentException;
import android.app.PendingIntent;
import android.os.Bundle;
import com.google.android.gms.internal.play_billing.zzb;
import android.content.Intent;
import androidx.annotation.Nullable;
import android.os.ResultReceiver;
import com.google.android.apps.common.proguard.UsedByReflection;
import android.app.Activity;

@UsedByReflection("PlatformActivityProxy")
public class ProxyBillingActivity extends Activity
{
    public static final int \u300708O\u300700\u3007o = 0;
    private boolean OO;
    @Nullable
    private ResultReceiver o0;
    @Nullable
    private ResultReceiver \u3007OOo8\u30070;
    
    private Intent \u3007080(final String s) {
        final Intent intent = new Intent("com.android.vending.billing.ALTERNATIVE_BILLING");
        intent.setPackage(((Context)this).getApplicationContext().getPackageName());
        intent.putExtra("ALTERNATIVE_BILLING_USER_CHOICE_DATA", s);
        return intent;
    }
    
    private Intent \u3007o00\u3007\u3007Oo() {
        final Intent intent = new Intent("com.android.vending.billing.PURCHASES_UPDATED");
        intent.setPackage(((Context)this).getApplicationContext().getPackageName());
        return intent;
    }
    
    protected void onActivityResult(int zza, final int n, @Nullable Intent intent) {
        super.onActivityResult(zza, n, intent);
        final Bundle bundle = null;
        final Bundle bundle2 = null;
        if (zza == 100) {
            final int \u3007o00\u3007\u3007Oo = zzb.zzi(intent, "ProxyBillingActivity").\u3007o00\u3007\u3007Oo();
            Label_0105: {
                if ((zza = n) == -1) {
                    if (\u3007o00\u3007\u3007Oo == 0) {
                        zza = 0;
                        break Label_0105;
                    }
                    zza = -1;
                }
                final StringBuilder sb = new StringBuilder();
                sb.append("Activity finished with resultCode ");
                sb.append(zza);
                sb.append(" and billing's responseCode: ");
                sb.append(\u3007o00\u3007\u3007Oo);
                zzb.zzo("ProxyBillingActivity", sb.toString());
                zza = \u3007o00\u3007\u3007Oo;
            }
            final ResultReceiver o0 = this.o0;
            if (o0 != null) {
                Bundle extras;
                if (intent == null) {
                    extras = bundle2;
                }
                else {
                    extras = intent.getExtras();
                }
                o0.send(zza, extras);
            }
            else {
                if (intent != null) {
                    if (intent.getExtras() != null) {
                        final String string = ((BaseBundle)intent.getExtras()).getString("ALTERNATIVE_BILLING_USER_CHOICE_DATA");
                        if (string != null) {
                            intent = this.\u3007080(string);
                        }
                        else {
                            final Intent \u3007o00\u3007\u3007Oo2 = this.\u3007o00\u3007\u3007Oo();
                            \u3007o00\u3007\u3007Oo2.putExtras(intent.getExtras());
                            intent = \u3007o00\u3007\u3007Oo2;
                        }
                    }
                    else {
                        intent = this.\u3007o00\u3007\u3007Oo();
                        zzb.zzo("ProxyBillingActivity", "Got null bundle!");
                        intent.putExtra("RESPONSE_CODE", 6);
                        intent.putExtra("DEBUG_MESSAGE", "An internal error occurred.");
                    }
                }
                else {
                    intent = this.\u3007o00\u3007\u3007Oo();
                }
                ((Context)this).sendBroadcast(intent);
            }
        }
        else if (zza == 101) {
            zza = zzb.zza(intent, "ProxyBillingActivity");
            final ResultReceiver \u3007oOo8\u30070 = this.\u3007OOo8\u30070;
            if (\u3007oOo8\u30070 != null) {
                Bundle extras2;
                if (intent == null) {
                    extras2 = bundle;
                }
                else {
                    extras2 = intent.getExtras();
                }
                \u3007oOo8\u30070.send(zza, extras2);
            }
        }
        else {
            final StringBuilder sb2 = new StringBuilder();
            sb2.append("Got onActivityResult with wrong requestCode: ");
            sb2.append(zza);
            sb2.append("; skipping...");
            zzb.zzo("ProxyBillingActivity", sb2.toString());
        }
        this.OO = false;
        this.finish();
    }
    
    protected void onCreate(@Nullable final Bundle bundle) {
        super.onCreate(bundle);
        if (bundle == null) {
            zzb.zzn("ProxyBillingActivity", "Launching Play Store billing flow");
            Label_0143: {
                PendingIntent pendingIntent;
                if (this.getIntent().hasExtra("BUY_INTENT")) {
                    pendingIntent = (PendingIntent)this.getIntent().getParcelableExtra("BUY_INTENT");
                }
                else if (this.getIntent().hasExtra("SUBS_MANAGEMENT_INTENT")) {
                    pendingIntent = (PendingIntent)this.getIntent().getParcelableExtra("SUBS_MANAGEMENT_INTENT");
                    this.o0 = (ResultReceiver)this.getIntent().getParcelableExtra("result_receiver");
                }
                else {
                    if (this.getIntent().hasExtra("IN_APP_MESSAGE_INTENT")) {
                        pendingIntent = (PendingIntent)this.getIntent().getParcelableExtra("IN_APP_MESSAGE_INTENT");
                        this.\u3007OOo8\u30070 = (ResultReceiver)this.getIntent().getParcelableExtra("in_app_message_result_receiver");
                        final int n = 101;
                        break Label_0143;
                    }
                    pendingIntent = null;
                }
                final int n = 100;
                try {
                    this.OO = true;
                    this.startIntentSenderForResult(pendingIntent.getIntentSender(), n, new Intent(), 0, 0, 0);
                    return;
                }
                catch (final IntentSender$SendIntentException ex) {
                    zzb.zzp("ProxyBillingActivity", "Got exception while trying to start a purchase flow.", (Throwable)ex);
                    final ResultReceiver o0 = this.o0;
                    if (o0 != null) {
                        o0.send(6, (Bundle)null);
                    }
                    else {
                        final ResultReceiver \u3007oOo8\u30070 = this.\u3007OOo8\u30070;
                        if (\u3007oOo8\u30070 != null) {
                            \u3007oOo8\u30070.send(0, (Bundle)null);
                        }
                        else {
                            final Intent \u3007o00\u3007\u3007Oo = this.\u3007o00\u3007\u3007Oo();
                            \u3007o00\u3007\u3007Oo.putExtra("RESPONSE_CODE", 6);
                            \u3007o00\u3007\u3007Oo.putExtra("DEBUG_MESSAGE", "An internal error occurred.");
                            ((Context)this).sendBroadcast(\u3007o00\u3007\u3007Oo);
                        }
                    }
                    this.OO = false;
                    this.finish();
                    return;
                }
            }
        }
        zzb.zzn("ProxyBillingActivity", "Launching Play Store billing flow from savedInstanceState");
        this.OO = bundle.getBoolean("send_cancelled_broadcast_if_finished", false);
        if (((BaseBundle)bundle).containsKey("result_receiver")) {
            this.o0 = (ResultReceiver)bundle.getParcelable("result_receiver");
            return;
        }
        if (((BaseBundle)bundle).containsKey("in_app_message_result_receiver")) {
            this.\u3007OOo8\u30070 = (ResultReceiver)bundle.getParcelable("in_app_message_result_receiver");
        }
    }
    
    protected void onDestroy() {
        super.onDestroy();
        if (!this.isFinishing()) {
            return;
        }
        if (!this.OO) {
            return;
        }
        final Intent \u3007o00\u3007\u3007Oo = this.\u3007o00\u3007\u3007Oo();
        \u3007o00\u3007\u3007Oo.putExtra("RESPONSE_CODE", 1);
        \u3007o00\u3007\u3007Oo.putExtra("DEBUG_MESSAGE", "Billing dialog closed.");
        ((Context)this).sendBroadcast(\u3007o00\u3007\u3007Oo);
    }
    
    protected void onSaveInstanceState(@NonNull final Bundle bundle) {
        final ResultReceiver o0 = this.o0;
        if (o0 != null) {
            bundle.putParcelable("result_receiver", (Parcelable)o0);
        }
        final ResultReceiver \u3007oOo8\u30070 = this.\u3007OOo8\u30070;
        if (\u3007oOo8\u30070 != null) {
            bundle.putParcelable("in_app_message_result_receiver", (Parcelable)\u3007oOo8\u30070);
        }
        bundle.putBoolean("send_cancelled_broadcast_if_finished", this.OO);
    }
}

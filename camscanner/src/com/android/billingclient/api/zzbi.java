// 
// Decompiled by Procyon v0.6.0
// 

package com.android.billingclient.api;

import android.os.BaseBundle;
import java.util.ArrayList;
import com.google.android.gms.internal.play_billing.zzb;
import android.os.Bundle;

final class zzbi
{
    static BillingResult \u3007080(final Bundle bundle, final String s, final String s2) {
        final BillingResult oo0o\u3007\u3007\u3007\u30070 = zzbb.OO0o\u3007\u3007\u3007\u30070;
        if (bundle == null) {
            zzb.zzo("BillingClient", String.format("%s got null owned items list", s2));
            return oo0o\u3007\u3007\u3007\u30070;
        }
        final int zzb = com.google.android.gms.internal.play_billing.zzb.zzb(bundle, "BillingClient");
        final String zzk = com.google.android.gms.internal.play_billing.zzb.zzk(bundle, "BillingClient");
        final BillingResult.Builder \u3007o\u3007 = BillingResult.\u3007o\u3007();
        \u3007o\u3007.\u3007o\u3007(zzb);
        \u3007o\u3007.\u3007o00\u3007\u3007Oo(zzk);
        final BillingResult \u3007080 = \u3007o\u3007.\u3007080();
        if (zzb != 0) {
            com.google.android.gms.internal.play_billing.zzb.zzo("BillingClient", String.format("%s failed. Response code: %s", s2, zzb));
            return \u3007080;
        }
        if (!((BaseBundle)bundle).containsKey("INAPP_PURCHASE_ITEM_LIST") || !((BaseBundle)bundle).containsKey("INAPP_PURCHASE_DATA_LIST") || !((BaseBundle)bundle).containsKey("INAPP_DATA_SIGNATURE_LIST")) {
            com.google.android.gms.internal.play_billing.zzb.zzo("BillingClient", String.format("Bundle returned from %s doesn't contain required fields.", s2));
            return oo0o\u3007\u3007\u3007\u30070;
        }
        final ArrayList stringArrayList = bundle.getStringArrayList("INAPP_PURCHASE_ITEM_LIST");
        final ArrayList stringArrayList2 = bundle.getStringArrayList("INAPP_PURCHASE_DATA_LIST");
        final ArrayList stringArrayList3 = bundle.getStringArrayList("INAPP_DATA_SIGNATURE_LIST");
        if (stringArrayList == null) {
            com.google.android.gms.internal.play_billing.zzb.zzo("BillingClient", String.format("Bundle returned from %s contains null SKUs list.", s2));
            return oo0o\u3007\u3007\u3007\u30070;
        }
        if (stringArrayList2 == null) {
            com.google.android.gms.internal.play_billing.zzb.zzo("BillingClient", String.format("Bundle returned from %s contains null purchases list.", s2));
            return oo0o\u3007\u3007\u3007\u30070;
        }
        if (stringArrayList3 == null) {
            com.google.android.gms.internal.play_billing.zzb.zzo("BillingClient", String.format("Bundle returned from %s contains null signatures list.", s2));
            return oo0o\u3007\u3007\u3007\u30070;
        }
        return zzbb.\u3007O8o08O;
    }
}

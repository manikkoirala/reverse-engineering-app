// 
// Decompiled by Procyon v0.6.0
// 

package com.android.billingclient.api;

import android.content.IntentFilter;
import com.google.android.gms.internal.play_billing.zzu;
import java.util.List;
import android.content.Intent;
import com.google.android.gms.internal.play_billing.zzb;
import android.content.Context;
import android.content.BroadcastReceiver;

final class zzn extends BroadcastReceiver
{
    private final PurchasesUpdatedListener \u3007080 = null;
    private boolean \u3007o00\u3007\u3007Oo;
    final zzo \u3007o\u3007;
    
    public final void O8(final Context context) {
        if (this.\u3007o00\u3007\u3007Oo) {
            context.unregisterReceiver((BroadcastReceiver)zzo.\u3007080(this.\u3007o\u3007));
            this.\u3007o00\u3007\u3007Oo = false;
            return;
        }
        zzb.zzo("BillingBroadcastManager", "Receiver is not registered.");
    }
    
    public final void onReceive(final Context context, final Intent intent) {
        final BillingResult zzi = zzb.zzi(intent, "BillingBroadcastManager");
        final String action = intent.getAction();
        if (action.equals("com.android.vending.billing.PURCHASES_UPDATED")) {
            this.\u3007080.\u3007o\u3007(zzi, zzb.zzm(intent.getExtras()));
            return;
        }
        if (action.equals("com.android.vending.billing.ALTERNATIVE_BILLING")) {
            intent.getExtras();
            if (zzi.\u3007o00\u3007\u3007Oo() != 0) {
                this.\u3007080.\u3007o\u3007(zzi, (List<Purchase>)zzu.zzl());
                return;
            }
            zzb.zzo("BillingBroadcastManager", "AlternativeBillingListener is null.");
            this.\u3007080.\u3007o\u3007(zzbb.OO0o\u3007\u3007\u3007\u30070, (List<Purchase>)zzu.zzl());
        }
    }
    
    public final void \u3007o\u3007(final Context context, final IntentFilter intentFilter) {
        if (!this.\u3007o00\u3007\u3007Oo) {
            context.registerReceiver((BroadcastReceiver)zzo.\u3007080(this.\u3007o\u3007), intentFilter);
            this.\u3007o00\u3007\u3007Oo = true;
        }
    }
}

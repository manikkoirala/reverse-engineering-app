// 
// Decompiled by Procyon v0.6.0
// 

package com.android.billingclient.api;

import com.google.android.gms.internal.play_billing.zzb;
import androidx.annotation.NonNull;

public final class BillingResult
{
    private int \u3007080;
    private String \u3007o00\u3007\u3007Oo;
    
    @NonNull
    public static Builder \u3007o\u3007() {
        return new Builder(null);
    }
    
    @NonNull
    @Override
    public String toString() {
        final String zzl = zzb.zzl(this.\u3007080);
        final String \u3007o00\u3007\u3007Oo = this.\u3007o00\u3007\u3007Oo;
        final StringBuilder sb = new StringBuilder();
        sb.append("Response Code: ");
        sb.append(zzl);
        sb.append(", Debug Message: ");
        sb.append(\u3007o00\u3007\u3007Oo);
        return sb.toString();
    }
    
    @NonNull
    public String \u3007080() {
        return this.\u3007o00\u3007\u3007Oo;
    }
    
    public int \u3007o00\u3007\u3007Oo() {
        return this.\u3007080;
    }
    
    public static class Builder
    {
        private int \u3007080;
        private String \u3007o00\u3007\u3007Oo = "";
        
        @NonNull
        public BillingResult \u3007080() {
            final BillingResult billingResult = new BillingResult();
            BillingResult.Oo08(billingResult, this.\u3007080);
            BillingResult.O8(billingResult, this.\u3007o00\u3007\u3007Oo);
            return billingResult;
        }
        
        @NonNull
        public Builder \u3007o00\u3007\u3007Oo(@NonNull final String \u3007o00\u3007\u3007Oo) {
            this.\u3007o00\u3007\u3007Oo = \u3007o00\u3007\u3007Oo;
            return this;
        }
        
        @NonNull
        public Builder \u3007o\u3007(final int \u3007080) {
            this.\u3007080 = \u3007080;
            return this;
        }
    }
}

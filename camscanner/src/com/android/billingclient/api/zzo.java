// 
// Decompiled by Procyon v0.6.0
// 

package com.android.billingclient.api;

import androidx.annotation.Nullable;
import android.content.IntentFilter;
import android.content.Context;

final class zzo
{
    private final Context \u3007080;
    private final zzn \u3007o00\u3007\u3007Oo;
    
    zzo(final Context \u3007080, final PurchasesUpdatedListener purchasesUpdatedListener, final zzc zzc) {
        this.\u3007080 = \u3007080;
        this.\u3007o00\u3007\u3007Oo = new zzn(this, purchasesUpdatedListener, zzc, null);
    }
    
    zzo(final Context \u3007080, final zzbe zzbe) {
        this.\u3007080 = \u3007080;
        this.\u3007o00\u3007\u3007Oo = new zzn(this, null, null);
    }
    
    final void O8() {
        this.\u3007o00\u3007\u3007Oo.O8(this.\u3007080);
    }
    
    final void Oo08() {
        final IntentFilter intentFilter = new IntentFilter("com.android.vending.billing.PURCHASES_UPDATED");
        intentFilter.addAction("com.android.vending.billing.ALTERNATIVE_BILLING");
        this.\u3007o00\u3007\u3007Oo.\u3007o\u3007(this.\u3007080, intentFilter);
    }
    
    @Nullable
    final zzbe \u3007o00\u3007\u3007Oo() {
        zzn.\u3007080(this.\u3007o00\u3007\u3007Oo);
        return null;
    }
    
    @Nullable
    final PurchasesUpdatedListener \u3007o\u3007() {
        return zzn.\u3007o00\u3007\u3007Oo(this.\u3007o00\u3007\u3007Oo);
    }
}

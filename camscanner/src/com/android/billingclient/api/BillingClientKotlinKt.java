// 
// Decompiled by Procyon v0.6.0
// 

package com.android.billingclient.api;

import kotlinx.coroutines.Deferred;
import kotlinx.coroutines.CompletableDeferred;
import kotlinx.coroutines.Job;
import kotlinx.coroutines.CompletableDeferredKt;
import kotlin.coroutines.Continuation;
import androidx.annotation.RecentlyNonNull;
import kotlin.Metadata;

@Metadata
public final class BillingClientKotlinKt
{
    @RecentlyNonNull
    public static final Object O8(@RecentlyNonNull final BillingClient billingClient, @RecentlyNonNull final QueryPurchasesParams queryPurchasesParams, @RecentlyNonNull final Continuation<? super PurchasesResult> continuation) {
        final CompletableDeferred \u3007o00\u3007\u3007Oo = CompletableDeferredKt.\u3007o00\u3007\u3007Oo((Job)null, 1, (Object)null);
        billingClient.OO0o\u3007\u3007\u3007\u30070(queryPurchasesParams, (PurchasesResponseListener)new BillingClientKotlinKt$queryPurchasesAsync.BillingClientKotlinKt$queryPurchasesAsync$4(\u3007o00\u3007\u3007Oo));
        return ((Deferred)\u3007o00\u3007\u3007Oo).\u30078o8o\u3007((Continuation)continuation);
    }
    
    @RecentlyNonNull
    public static final Object Oo08(@RecentlyNonNull final BillingClient billingClient, @RecentlyNonNull final SkuDetailsParams skuDetailsParams, @RecentlyNonNull final Continuation<? super SkuDetailsResult> continuation) {
        final CompletableDeferred \u3007o00\u3007\u3007Oo = CompletableDeferredKt.\u3007o00\u3007\u3007Oo((Job)null, 1, (Object)null);
        billingClient.\u30078o8o\u3007(skuDetailsParams, (SkuDetailsResponseListener)new BillingClientKotlinKt$querySkuDetails.BillingClientKotlinKt$querySkuDetails$2(\u3007o00\u3007\u3007Oo));
        return ((Deferred)\u3007o00\u3007\u3007Oo).\u30078o8o\u3007((Continuation)continuation);
    }
    
    @RecentlyNonNull
    public static final Object \u3007080(@RecentlyNonNull final BillingClient billingClient, @RecentlyNonNull final AcknowledgePurchaseParams acknowledgePurchaseParams, @RecentlyNonNull final Continuation<? super BillingResult> continuation) {
        final CompletableDeferred \u3007o00\u3007\u3007Oo = CompletableDeferredKt.\u3007o00\u3007\u3007Oo((Job)null, 1, (Object)null);
        billingClient.\u3007080(acknowledgePurchaseParams, (AcknowledgePurchaseResponseListener)new BillingClientKotlinKt$acknowledgePurchase.BillingClientKotlinKt$acknowledgePurchase$2(\u3007o00\u3007\u3007Oo));
        return ((Deferred)\u3007o00\u3007\u3007Oo).\u30078o8o\u3007((Continuation)continuation);
    }
    
    @RecentlyNonNull
    public static final Object \u3007o00\u3007\u3007Oo(@RecentlyNonNull final BillingClient billingClient, @RecentlyNonNull final ConsumeParams consumeParams, @RecentlyNonNull final Continuation<? super ConsumeResult> continuation) {
        final CompletableDeferred \u3007o00\u3007\u3007Oo = CompletableDeferredKt.\u3007o00\u3007\u3007Oo((Job)null, 1, (Object)null);
        billingClient.\u3007o00\u3007\u3007Oo(consumeParams, (ConsumeResponseListener)new BillingClientKotlinKt$consumePurchase.BillingClientKotlinKt$consumePurchase$2(\u3007o00\u3007\u3007Oo));
        return ((Deferred)\u3007o00\u3007\u3007Oo).\u30078o8o\u3007((Continuation)continuation);
    }
    
    @RecentlyNonNull
    public static final Object \u3007o\u3007(@RecentlyNonNull final BillingClient billingClient, @RecentlyNonNull final QueryProductDetailsParams queryProductDetailsParams, @RecentlyNonNull final Continuation<? super ProductDetailsResult> continuation) {
        final CompletableDeferred \u3007o00\u3007\u3007Oo = CompletableDeferredKt.\u3007o00\u3007\u3007Oo((Job)null, 1, (Object)null);
        billingClient.\u300780\u3007808\u3007O(queryProductDetailsParams, (ProductDetailsResponseListener)new BillingClientKotlinKt$queryProductDetails.BillingClientKotlinKt$queryProductDetails$2(\u3007o00\u3007\u3007Oo));
        return ((Deferred)\u3007o00\u3007\u3007Oo).\u30078o8o\u3007((Continuation)continuation);
    }
}

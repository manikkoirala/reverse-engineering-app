// 
// Decompiled by Procyon v0.6.0
// 

package com.android.billingclient.api;

import androidx.annotation.UiThread;
import android.app.Activity;
import androidx.annotation.AnyThread;
import androidx.annotation.NonNull;
import android.content.Context;

public abstract class BillingClient
{
    @AnyThread
    @NonNull
    public static Builder oO80(@NonNull final Context context) {
        return new Builder(context, null);
    }
    
    @AnyThread
    public abstract int O8();
    
    @AnyThread
    public abstract void OO0o\u3007\u3007\u3007\u30070(@NonNull final QueryPurchasesParams p0, @NonNull final PurchasesResponseListener p1);
    
    @AnyThread
    @NonNull
    public abstract BillingResult Oo08(@NonNull final String p0);
    
    @AnyThread
    public abstract boolean o\u30070();
    
    @AnyThread
    public abstract void \u3007080(@NonNull final AcknowledgePurchaseParams p0, @NonNull final AcknowledgePurchaseResponseListener p1);
    
    @AnyThread
    public abstract void \u300780\u3007808\u3007O(@NonNull final QueryProductDetailsParams p0, @NonNull final ProductDetailsResponseListener p1);
    
    @Deprecated
    @AnyThread
    public abstract void \u30078o8o\u3007(@NonNull final SkuDetailsParams p0, @NonNull final SkuDetailsResponseListener p1);
    
    @AnyThread
    public abstract void \u3007O8o08O(@NonNull final BillingClientStateListener p0);
    
    @AnyThread
    public abstract void \u3007o00\u3007\u3007Oo(@NonNull final ConsumeParams p0, @NonNull final ConsumeResponseListener p1);
    
    @AnyThread
    public abstract void \u3007o\u3007();
    
    @NonNull
    @UiThread
    public abstract BillingResult \u3007\u3007888(@NonNull final Activity p0, @NonNull final BillingFlowParams p1);
    
    @AnyThread
    public static final class Builder
    {
        private volatile boolean \u3007080;
        private final Context \u3007o00\u3007\u3007Oo = \u3007o00\u3007\u3007Oo;
        private volatile PurchasesUpdatedListener \u3007o\u3007;
        
        @NonNull
        public BillingClient \u3007080() {
            if (this.\u3007o00\u3007\u3007Oo == null) {
                throw new IllegalArgumentException("Please provide a valid Context.");
            }
            if (this.\u3007o\u3007 == null) {
                throw new IllegalArgumentException("Please provide a valid listener for purchases updates.");
            }
            if (!this.\u3007080) {
                throw new IllegalArgumentException("Support for pending purchases must be enabled. Enable this by calling 'enablePendingPurchases()' on BillingClientBuilder.");
            }
            if (this.\u3007o\u3007 != null) {
                return new BillingClientImpl(null, this.\u3007080, this.\u3007o00\u3007\u3007Oo, this.\u3007o\u3007, null);
            }
            return new BillingClientImpl(null, this.\u3007080, this.\u3007o00\u3007\u3007Oo, null);
        }
        
        @NonNull
        public Builder \u3007o00\u3007\u3007Oo() {
            this.\u3007080 = true;
            return this;
        }
        
        @NonNull
        public Builder \u3007o\u3007(@NonNull final PurchasesUpdatedListener \u3007o\u3007) {
            this.\u3007o\u3007 = \u3007o\u3007;
            return this;
        }
    }
}

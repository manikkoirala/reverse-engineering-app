// 
// Decompiled by Procyon v0.6.0
// 

package com.android.billingclient.api;

import java.util.Collection;
import java.util.ArrayList;
import androidx.annotation.NonNull;
import java.util.List;

@Deprecated
public class SkuDetailsParams
{
    private String \u3007080;
    private List \u3007o00\u3007\u3007Oo;
    
    @NonNull
    public static Builder \u3007o\u3007() {
        return new Builder(null);
    }
    
    @NonNull
    public String \u3007080() {
        return this.\u3007080;
    }
    
    @NonNull
    public List<String> \u3007o00\u3007\u3007Oo() {
        return this.\u3007o00\u3007\u3007Oo;
    }
    
    public static class Builder
    {
        private String \u3007080;
        private List \u3007o00\u3007\u3007Oo;
        
        @NonNull
        public SkuDetailsParams \u3007080() {
            final String \u3007080 = this.\u3007080;
            if (\u3007080 == null) {
                throw new IllegalArgumentException("SKU type must be set");
            }
            if (this.\u3007o00\u3007\u3007Oo != null) {
                final SkuDetailsParams skuDetailsParams = new SkuDetailsParams();
                SkuDetailsParams.O8(skuDetailsParams, \u3007080);
                SkuDetailsParams.Oo08(skuDetailsParams, this.\u3007o00\u3007\u3007Oo);
                return skuDetailsParams;
            }
            throw new IllegalArgumentException("SKU list or SkuWithOffer list must be set");
        }
        
        @NonNull
        public Builder \u3007o00\u3007\u3007Oo(@NonNull final List<String> c) {
            this.\u3007o00\u3007\u3007Oo = new ArrayList(c);
            return this;
        }
        
        @NonNull
        public Builder \u3007o\u3007(@NonNull final String \u3007080) {
            this.\u3007080 = \u3007080;
            return this;
        }
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.android.billingclient.api;

import androidx.annotation.Nullable;
import java.util.List;

public final class zzbh
{
    @Nullable
    private final List \u3007080;
    private final BillingResult \u3007o00\u3007\u3007Oo;
    
    public zzbh(final BillingResult \u3007o00\u3007\u3007Oo, @Nullable final List \u3007080) {
        this.\u3007080 = \u3007080;
        this.\u3007o00\u3007\u3007Oo = \u3007o00\u3007\u3007Oo;
    }
    
    public final BillingResult \u3007080() {
        return this.\u3007o00\u3007\u3007Oo;
    }
    
    @Nullable
    public final List \u3007o00\u3007\u3007Oo() {
        return this.\u3007080;
    }
}

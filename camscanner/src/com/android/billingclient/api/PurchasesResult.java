// 
// Decompiled by Procyon v0.6.0
// 

package com.android.billingclient.api;

import kotlin.jvm.internal.Intrinsics;
import androidx.annotation.RecentlyNonNull;
import java.util.List;
import org.jetbrains.annotations.NotNull;
import kotlin.Metadata;

@Metadata
public final class PurchasesResult
{
    @NotNull
    private final BillingResult \u3007080;
    @NotNull
    private final List \u3007o00\u3007\u3007Oo;
    
    public PurchasesResult(@RecentlyNonNull final BillingResult \u3007080, @RecentlyNonNull final List<? extends Purchase> \u3007o00\u3007\u3007Oo) {
        Intrinsics.checkNotNullParameter((Object)\u3007080, "billingResult");
        Intrinsics.checkNotNullParameter((Object)\u3007o00\u3007\u3007Oo, "purchasesList");
        this.\u3007080 = \u3007080;
        this.\u3007o00\u3007\u3007Oo = \u3007o00\u3007\u3007Oo;
    }
    
    @Override
    public boolean equals(@RecentlyNonNull final Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof PurchasesResult)) {
            return false;
        }
        final PurchasesResult purchasesResult = (PurchasesResult)o;
        return Intrinsics.\u3007o\u3007((Object)this.\u3007080, (Object)purchasesResult.\u3007080) && Intrinsics.\u3007o\u3007((Object)this.\u3007o00\u3007\u3007Oo, (Object)purchasesResult.\u3007o00\u3007\u3007Oo);
    }
    
    @Override
    public int hashCode() {
        return this.\u3007080.hashCode() * 31 + this.\u3007o00\u3007\u3007Oo.hashCode();
    }
    
    @NotNull
    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append("PurchasesResult(billingResult=");
        sb.append(this.\u3007080);
        sb.append(", purchasesList=");
        sb.append(this.\u3007o00\u3007\u3007Oo);
        sb.append(')');
        return sb.toString();
    }
    
    @NotNull
    public final BillingResult \u3007080() {
        return this.\u3007080;
    }
    
    @NotNull
    public final List<Purchase> \u3007o00\u3007\u3007Oo() {
        return this.\u3007o00\u3007\u3007Oo;
    }
}

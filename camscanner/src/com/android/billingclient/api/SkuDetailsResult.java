// 
// Decompiled by Procyon v0.6.0
// 

package com.android.billingclient.api;

import kotlin.jvm.internal.Intrinsics;
import androidx.annotation.RecentlyNonNull;
import java.util.List;
import org.jetbrains.annotations.NotNull;
import kotlin.Metadata;

@Metadata
public final class SkuDetailsResult
{
    @NotNull
    private final BillingResult \u3007080;
    private final List \u3007o00\u3007\u3007Oo;
    
    public SkuDetailsResult(@RecentlyNonNull final BillingResult \u3007080, @RecentlyNonNull final List<? extends SkuDetails> \u3007o00\u3007\u3007Oo) {
        Intrinsics.checkNotNullParameter((Object)\u3007080, "billingResult");
        this.\u3007080 = \u3007080;
        this.\u3007o00\u3007\u3007Oo = \u3007o00\u3007\u3007Oo;
    }
    
    @Override
    public boolean equals(@RecentlyNonNull final Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof SkuDetailsResult)) {
            return false;
        }
        final SkuDetailsResult skuDetailsResult = (SkuDetailsResult)o;
        return Intrinsics.\u3007o\u3007((Object)this.\u3007080, (Object)skuDetailsResult.\u3007080) && Intrinsics.\u3007o\u3007((Object)this.\u3007o00\u3007\u3007Oo, (Object)skuDetailsResult.\u3007o00\u3007\u3007Oo);
    }
    
    @Override
    public int hashCode() {
        final int hashCode = this.\u3007080.hashCode();
        final List \u3007o00\u3007\u3007Oo = this.\u3007o00\u3007\u3007Oo;
        int hashCode2;
        if (\u3007o00\u3007\u3007Oo == null) {
            hashCode2 = 0;
        }
        else {
            hashCode2 = \u3007o00\u3007\u3007Oo.hashCode();
        }
        return hashCode * 31 + hashCode2;
    }
    
    @NotNull
    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append("SkuDetailsResult(billingResult=");
        sb.append(this.\u3007080);
        sb.append(", skuDetailsList=");
        sb.append(this.\u3007o00\u3007\u3007Oo);
        sb.append(')');
        return sb.toString();
    }
    
    @NotNull
    public final BillingResult \u3007080() {
        return this.\u3007080;
    }
    
    @RecentlyNonNull
    public final List<SkuDetails> \u3007o00\u3007\u3007Oo() {
        return this.\u3007o00\u3007\u3007Oo;
    }
}

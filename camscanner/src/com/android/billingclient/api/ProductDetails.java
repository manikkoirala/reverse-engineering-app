// 
// Decompiled by Procyon v0.6.0
// 

package com.android.billingclient.api;

import androidx.annotation.NonNull;
import org.json.JSONException;
import org.json.JSONArray;
import java.util.ArrayList;
import android.text.TextUtils;
import org.json.JSONObject;
import androidx.annotation.Nullable;
import java.util.List;

public final class ProductDetails
{
    private final String O8;
    private final String Oo08;
    private final String oO80;
    private final String o\u30070;
    private final String \u3007080;
    @Nullable
    private final List \u300780\u3007808\u3007O;
    private final JSONObject \u3007o00\u3007\u3007Oo;
    private final String \u3007o\u3007;
    private final String \u3007\u3007888;
    
    ProductDetails(final String \u3007080) throws JSONException {
        this.\u3007080 = \u3007080;
        final JSONObject \u3007o00\u3007\u3007Oo = new JSONObject(\u3007080);
        this.\u3007o00\u3007\u3007Oo = \u3007o00\u3007\u3007Oo;
        final String optString = \u3007o00\u3007\u3007Oo.optString("productId");
        this.\u3007o\u3007 = optString;
        final String optString2 = \u3007o00\u3007\u3007Oo.optString("type");
        this.O8 = optString2;
        if (TextUtils.isEmpty((CharSequence)optString)) {
            throw new IllegalArgumentException("Product id cannot be empty.");
        }
        if (TextUtils.isEmpty((CharSequence)optString2)) {
            throw new IllegalArgumentException("Product type cannot be empty.");
        }
        this.Oo08 = \u3007o00\u3007\u3007Oo.optString("title");
        this.o\u30070 = \u3007o00\u3007\u3007Oo.optString("name");
        this.\u3007\u3007888 = \u3007o00\u3007\u3007Oo.optString("description");
        this.oO80 = \u3007o00\u3007\u3007Oo.optString("skuDetailsToken");
        if (!optString2.equals("inapp")) {
            final ArrayList \u300780\u3007808\u3007O = new ArrayList();
            final JSONArray optJSONArray = \u3007o00\u3007\u3007Oo.optJSONArray("subscriptionOfferDetails");
            if (optJSONArray != null) {
                for (int i = 0; i < optJSONArray.length(); ++i) {
                    \u300780\u3007808\u3007O.add(new SubscriptionOfferDetails(optJSONArray.getJSONObject(i)));
                }
            }
            this.\u300780\u3007808\u3007O = \u300780\u3007808\u3007O;
            return;
        }
        this.\u300780\u3007808\u3007O = null;
    }
    
    @NonNull
    public String O8() {
        return this.\u3007o\u3007;
    }
    
    @NonNull
    public String Oo08() {
        return this.O8;
    }
    
    @Override
    public final boolean equals(@Nullable final Object o) {
        return this == o || (o instanceof ProductDetails && TextUtils.equals((CharSequence)this.\u3007080, (CharSequence)((ProductDetails)o).\u3007080));
    }
    
    @Override
    public final int hashCode() {
        return this.\u3007080.hashCode();
    }
    
    @NonNull
    public final String oO80() {
        return this.\u3007o00\u3007\u3007Oo.optString("packageName");
    }
    
    @Nullable
    public List<SubscriptionOfferDetails> o\u30070() {
        return this.\u300780\u3007808\u3007O;
    }
    
    @NonNull
    @Override
    public final String toString() {
        final String \u3007080 = this.\u3007080;
        final String string = this.\u3007o00\u3007\u3007Oo.toString();
        final String \u3007o\u3007 = this.\u3007o\u3007;
        final String o8 = this.O8;
        final String oo08 = this.Oo08;
        final String oo9 = this.oO80;
        final String value = String.valueOf(this.\u300780\u3007808\u3007O);
        final StringBuilder sb = new StringBuilder();
        sb.append("ProductDetails{jsonString='");
        sb.append(\u3007080);
        sb.append("', parsedJson=");
        sb.append(string);
        sb.append(", productId='");
        sb.append(\u3007o\u3007);
        sb.append("', productType='");
        sb.append(o8);
        sb.append("', title='");
        sb.append(oo08);
        sb.append("', productDetailsToken='");
        sb.append(oo9);
        sb.append("', subscriptionOfferDetails=");
        sb.append(value);
        sb.append("}");
        return sb.toString();
    }
    
    @NonNull
    public String \u3007080() {
        return this.\u3007\u3007888;
    }
    
    final String \u300780\u3007808\u3007O() {
        return this.oO80;
    }
    
    @NonNull
    public String \u3007o00\u3007\u3007Oo() {
        return this.o\u30070;
    }
    
    @Nullable
    public OneTimePurchaseOfferDetails \u3007o\u3007() {
        final JSONObject optJSONObject = this.\u3007o00\u3007\u3007Oo.optJSONObject("oneTimePurchaseOfferDetails");
        if (optJSONObject != null) {
            return new OneTimePurchaseOfferDetails(optJSONObject);
        }
        return null;
    }
    
    @NonNull
    public String \u3007\u3007888() {
        return this.Oo08;
    }
    
    public static final class OneTimePurchaseOfferDetails
    {
        private final String O8;
        private final String Oo08;
        private final String \u3007080;
        private final long \u3007o00\u3007\u3007Oo;
        private final String \u3007o\u3007;
        
        OneTimePurchaseOfferDetails(final JSONObject jsonObject) {
            this.\u3007080 = jsonObject.optString("formattedPrice");
            this.\u3007o00\u3007\u3007Oo = jsonObject.optLong("priceAmountMicros");
            this.\u3007o\u3007 = jsonObject.optString("priceCurrencyCode");
            this.O8 = jsonObject.optString("offerIdToken");
            this.Oo08 = jsonObject.optString("offerId");
            jsonObject.optInt("offerType");
        }
        
        @NonNull
        public final String O8() {
            return this.O8;
        }
        
        @NonNull
        public String \u3007080() {
            return this.\u3007080;
        }
        
        public long \u3007o00\u3007\u3007Oo() {
            return this.\u3007o00\u3007\u3007Oo;
        }
        
        @NonNull
        public String \u3007o\u3007() {
            return this.\u3007o\u3007;
        }
    }
    
    public static final class PricingPhase
    {
        private final String O8;
        private final int Oo08;
        private final int o\u30070;
        private final String \u3007080;
        private final long \u3007o00\u3007\u3007Oo;
        private final String \u3007o\u3007;
        
        PricingPhase(final JSONObject jsonObject) {
            this.O8 = jsonObject.optString("billingPeriod");
            this.\u3007o\u3007 = jsonObject.optString("priceCurrencyCode");
            this.\u3007080 = jsonObject.optString("formattedPrice");
            this.\u3007o00\u3007\u3007Oo = jsonObject.optLong("priceAmountMicros");
            this.o\u30070 = jsonObject.optInt("recurrenceMode");
            this.Oo08 = jsonObject.optInt("billingCycleCount");
        }
        
        public long O8() {
            return this.\u3007o00\u3007\u3007Oo;
        }
        
        @NonNull
        public String Oo08() {
            return this.\u3007o\u3007;
        }
        
        public int o\u30070() {
            return this.o\u30070;
        }
        
        public int \u3007080() {
            return this.Oo08;
        }
        
        @NonNull
        public String \u3007o00\u3007\u3007Oo() {
            return this.O8;
        }
        
        @NonNull
        public String \u3007o\u3007() {
            return this.\u3007080;
        }
    }
    
    public static class PricingPhases
    {
        private final List<PricingPhase> \u3007080;
        
        PricingPhases(final JSONArray jsonArray) {
            final ArrayList \u3007080 = new ArrayList();
            if (jsonArray != null) {
                for (int i = 0; i < jsonArray.length(); ++i) {
                    final JSONObject optJSONObject = jsonArray.optJSONObject(i);
                    if (optJSONObject != null) {
                        \u3007080.add(new PricingPhase(optJSONObject));
                    }
                }
            }
            this.\u3007080 = \u3007080;
        }
        
        @NonNull
        public List<PricingPhase> \u3007080() {
            return this.\u3007080;
        }
    }
    
    public static final class SubscriptionOfferDetails
    {
        @Nullable
        private final zzbg O8;
        private final String \u3007080;
        private final PricingPhases \u3007o00\u3007\u3007Oo;
        private final List<String> \u3007o\u3007;
        
        SubscriptionOfferDetails(final JSONObject jsonObject) throws JSONException {
            this.\u3007080 = jsonObject.getString("offerIdToken");
            this.\u3007o00\u3007\u3007Oo = new PricingPhases(jsonObject.getJSONArray("pricingPhases"));
            final JSONObject optJSONObject = jsonObject.optJSONObject("installmentPlanDetails");
            zzbg o8;
            if (optJSONObject == null) {
                o8 = null;
            }
            else {
                o8 = new zzbg(optJSONObject);
            }
            this.O8 = o8;
            final ArrayList \u3007o\u3007 = new ArrayList();
            final JSONArray optJSONArray = jsonObject.optJSONArray("offerTags");
            if (optJSONArray != null) {
                for (int i = 0; i < optJSONArray.length(); ++i) {
                    \u3007o\u3007.add(optJSONArray.getString(i));
                }
            }
            this.\u3007o\u3007 = \u3007o\u3007;
        }
        
        @NonNull
        public String \u3007080() {
            return this.\u3007080;
        }
        
        @NonNull
        public PricingPhases \u3007o00\u3007\u3007Oo() {
            return this.\u3007o00\u3007\u3007Oo;
        }
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.android.billingclient.api;

import com.google.android.gms.internal.play_billing.zzb;
import androidx.annotation.Nullable;
import android.os.Bundle;
import android.os.ResultReceiver;

final class zzak extends ResultReceiver
{
    public final void onReceiveResult(final int n, @Nullable final Bundle bundle) {
        zzb.zzj(bundle, "BillingClient");
        throw null;
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.android.billingclient.api;

import android.os.BaseBundle;
import android.os.Bundle;
import android.text.TextUtils;
import com.google.android.gms.internal.play_billing.zze;
import java.util.concurrent.Callable;
import com.google.android.gms.internal.play_billing.zzd;
import com.google.android.gms.internal.play_billing.zzb;
import android.os.IBinder;
import android.content.ComponentName;
import android.content.ServiceConnection;

final class zzap implements ServiceConnection
{
    private BillingClientStateListener OO = oo;
    private final Object o0 = new Object();
    final BillingClientImpl \u300708O\u300700\u3007o;
    private boolean \u3007OOo8\u30070 = false;
    
    private final void O8(final BillingResult billingResult) {
        synchronized (this.o0) {
            final BillingClientStateListener oo = this.OO;
            if (oo != null) {
                oo.\u3007080(billingResult);
            }
        }
    }
    
    public final void onServiceConnected(final ComponentName componentName, final IBinder binder) {
        zzb.zzn("BillingClient", "Billing service connected.");
        BillingClientImpl.\u3007\u3007808\u3007(this.\u300708O\u300700\u3007o, zzd.zzo(binder));
        final BillingClientImpl \u300708O\u300700\u3007o = this.\u300708O\u300700\u3007o;
        if (BillingClientImpl.o0ooO(\u300708O\u300700\u3007o, (Callable)new zzam(this), 30000L, (Runnable)new zzan(this), BillingClientImpl.\u3007oOO8O8(\u300708O\u300700\u3007o)) == null) {
            this.O8(BillingClientImpl.\u30070000OOO(this.\u300708O\u300700\u3007o));
        }
    }
    
    public final void onServiceDisconnected(final ComponentName componentName) {
        zzb.zzo("BillingClient", "Billing service disconnected.");
        BillingClientImpl.\u3007\u3007808\u3007(this.\u300708O\u300700\u3007o, (zze)null);
        BillingClientImpl.o\u30078(this.\u300708O\u300700\u3007o, 0);
        synchronized (this.o0) {
            final BillingClientStateListener oo = this.OO;
            if (oo != null) {
                oo.\u3007o00\u3007\u3007Oo();
            }
        }
    }
    
    final void \u3007o\u3007() {
        synchronized (this.o0) {
            this.OO = null;
            this.\u3007OOo8\u30070 = true;
        }
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.app.hubert.guide.lifecycle;

public abstract class FragmentLifecycleAdapter implements FragmentLifecycle
{
    @Override
    public void onDestroy() {
    }
    
    @Override
    public void onStart() {
    }
    
    @Override
    public void onStop() {
    }
}

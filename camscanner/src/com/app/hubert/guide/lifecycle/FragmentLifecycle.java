// 
// Decompiled by Procyon v0.6.0
// 

package com.app.hubert.guide.lifecycle;

public interface FragmentLifecycle
{
    void onDestroy();
    
    void onDestroyView();
    
    void onStart();
    
    void onStop();
}

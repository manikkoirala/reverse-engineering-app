// 
// Decompiled by Procyon v0.6.0
// 

package com.app.hubert.guide.lifecycle;

import com.app.hubert.guide.util.LogUtil;
import android.app.Fragment;

public class ListenerFragment extends Fragment
{
    FragmentLifecycle o0;
    
    public void onDestroy() {
        super.onDestroy();
        LogUtil.\u3007080("onDestroy: ");
        this.o0.onDestroy();
    }
    
    public void onDestroyView() {
        super.onDestroyView();
        this.o0.onDestroyView();
    }
    
    public void onStart() {
        super.onStart();
        LogUtil.\u3007080("onStart: ");
        this.o0.onStart();
    }
    
    public void onStop() {
        super.onStop();
        this.o0.onStop();
    }
    
    public void \u3007080(final FragmentLifecycle o0) {
        this.o0 = o0;
    }
}

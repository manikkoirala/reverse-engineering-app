// 
// Decompiled by Procyon v0.6.0
// 

package com.app.hubert.guide.lifecycle;

import com.app.hubert.guide.util.LogUtil;
import androidx.fragment.app.Fragment;

public class V4ListenerFragment extends Fragment
{
    FragmentLifecycle o0;
    
    @Override
    public void onDestroy() {
        super.onDestroy();
        LogUtil.\u3007080("onDestroy: ");
        this.o0.onDestroy();
    }
    
    @Override
    public void onDestroyView() {
        super.onDestroyView();
        this.o0.onDestroyView();
    }
    
    @Override
    public void onStart() {
        super.onStart();
        LogUtil.\u3007080("onStart: ");
        this.o0.onStart();
    }
    
    @Override
    public void onStop() {
        super.onStop();
        this.o0.onStop();
    }
    
    public void \u300780O8o8O\u3007(final FragmentLifecycle o0) {
        this.o0 = o0;
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.app.hubert.guide.listener;

import android.view.animation.Animation;
import android.view.animation.Animation$AnimationListener;

public abstract class AnimationListenerAdapter implements Animation$AnimationListener
{
    public void onAnimationRepeat(final Animation animation) {
    }
    
    public void onAnimationStart(final Animation animation) {
    }
}

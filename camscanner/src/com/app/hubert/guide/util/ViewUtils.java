// 
// Decompiled by Procyon v0.6.0
// 

package com.app.hubert.guide.util;

import android.content.Context;
import androidx.viewpager.widget.ViewPager;
import android.app.Activity;
import android.graphics.Rect;
import android.view.View;

public class ViewUtils
{
    public static Rect \u3007080(final View view, final View view2) {
        if (view2 == null || view == null) {
            throw new IllegalArgumentException("parent and child can not be null .");
        }
        final Context context = view2.getContext();
        View decorView;
        if (context instanceof Activity) {
            decorView = ((Activity)context).getWindow().getDecorView();
        }
        else {
            decorView = null;
        }
        final Rect rect = new Rect();
        final Rect rect2 = new Rect();
        if (view2 == view) {
            view2.getHitRect(rect);
            return rect;
        }
        View view3 = view2;
        while (view3 != decorView && view3 != view) {
            view3.getHitRect(rect2);
            if (!view3.getClass().equals("NoSaveStateFrameLayout")) {
                rect.left += rect2.left;
                rect.top += rect2.top;
            }
            final View view4 = (View)view3.getParent();
            if (view4 == null) {
                throw new IllegalArgumentException("the view is not showing in the window!");
            }
            view3 = view4;
            if (view4.getParent() == null) {
                continue;
            }
            view3 = view4;
            if (!(view4.getParent() instanceof ViewPager)) {
                continue;
            }
            view3 = (View)view4.getParent();
        }
        rect.right = rect.left + view2.getMeasuredWidth();
        rect.bottom = rect.top + view2.getMeasuredHeight();
        return rect;
    }
}

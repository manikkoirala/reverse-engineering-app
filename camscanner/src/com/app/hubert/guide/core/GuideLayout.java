// 
// Decompiled by Procyon v0.6.0
// 

package com.app.hubert.guide.core;

import android.view.MotionEvent;
import android.view.animation.Animation$AnimationListener;
import android.view.animation.Animation;
import com.app.hubert.guide.listener.AnimationListenerAdapter;
import com.app.hubert.guide.model.RelativeGuide;
import android.view.ViewGroup$LayoutParams;
import android.widget.RelativeLayout$LayoutParams;
import android.view.LayoutInflater;
import android.view.View$OnClickListener;
import com.app.hubert.guide.model.HighlightOptions;
import android.view.ViewConfiguration;
import android.graphics.MaskFilter;
import android.graphics.BlurMaskFilter;
import android.graphics.BlurMaskFilter$Blur;
import android.graphics.Xfermode;
import android.graphics.PorterDuffXfermode;
import android.graphics.PorterDuff$Mode;
import android.graphics.RectF;
import java.util.Iterator;
import java.util.List;
import android.view.View;
import android.view.ViewGroup;
import com.app.hubert.guide.model.HighLight;
import android.graphics.Canvas;
import android.content.Context;
import android.graphics.Paint;
import com.app.hubert.guide.model.GuidePage;
import android.widget.FrameLayout;

public class GuideLayout extends FrameLayout
{
    private float O8o08O8O;
    public GuidePage OO;
    private Controller o0;
    private float o\u300700O;
    private int \u3007080OO8\u30070;
    private OnGuideLayoutDismissListener \u300708O\u300700\u3007o;
    private Paint \u3007OOo8\u30070;
    
    public GuideLayout(final Context context, final GuidePage guidePage, final Controller o0) {
        super(context);
        this.Oo08();
        this.setGuidePage(guidePage);
        this.o0 = o0;
    }
    
    private void O8(final Canvas canvas) {
        final List<HighLight> oo0o\u3007\u3007\u3007\u30070 = this.OO.OO0o\u3007\u3007\u3007\u30070();
        if (oo0o\u3007\u3007\u3007\u30070 != null) {
            for (final HighLight highLight : oo0o\u3007\u3007\u3007\u30070) {
                final RectF \u3007080 = highLight.\u3007080((View)((View)this).getParent());
                final int n = GuideLayout$4.\u3007080[highLight.getShape().ordinal()];
                if (n != 1) {
                    if (n != 2) {
                        if (n != 3) {
                            canvas.drawRect(\u3007080, this.\u3007OOo8\u30070);
                        }
                        else {
                            canvas.drawRoundRect(\u3007080, (float)highLight.\u3007o00\u3007\u3007Oo(), (float)highLight.\u3007o00\u3007\u3007Oo(), this.\u3007OOo8\u30070);
                        }
                    }
                    else {
                        canvas.drawOval(\u3007080, this.\u3007OOo8\u30070);
                    }
                }
                else {
                    canvas.drawCircle(\u3007080.centerX(), \u3007080.centerY(), highLight.getRadius(), this.\u3007OOo8\u30070);
                }
                this.\u3007\u3007888(canvas, highLight, \u3007080);
            }
        }
    }
    
    private void Oo08() {
        (this.\u3007OOo8\u30070 = new Paint()).setAntiAlias(true);
        this.\u3007OOo8\u30070.setXfermode((Xfermode)new PorterDuffXfermode(PorterDuff$Mode.CLEAR));
        this.\u3007OOo8\u30070.setMaskFilter((MaskFilter)new BlurMaskFilter(10.0f, BlurMaskFilter$Blur.INNER));
        ((View)this).setLayerType(1, (Paint)null);
        ((View)this).setWillNotDraw(false);
        this.\u3007080OO8\u30070 = ViewConfiguration.get(((View)this).getContext()).getScaledTouchSlop();
    }
    
    private void o\u30070(final HighLight highLight) {
        final HighlightOptions options = highLight.getOptions();
        if (options != null) {
            final View$OnClickListener \u3007080 = options.\u3007080;
            if (\u3007080 != null) {
                \u3007080.onClick((View)this);
            }
        }
    }
    
    private void setGuidePage(final GuidePage oo) {
        this.OO = oo;
        ((View)this).setOnClickListener((View$OnClickListener)new View$OnClickListener(this) {
            final GuideLayout o0;
            
            public void onClick(final View view) {
                if (this.o0.OO.Oooo8o0\u3007()) {
                    this.o0.oO80();
                }
            }
        });
    }
    
    private void \u3007o00\u3007\u3007Oo(final GuidePage guidePage) {
        ((ViewGroup)this).removeAllViews();
        final int \u30078o8o\u3007 = guidePage.\u30078o8o\u3007();
        if (\u30078o8o\u3007 != 0) {
            final LayoutInflater from = LayoutInflater.from(((View)this).getContext());
            int i = 0;
            final View inflate = from.inflate(\u30078o8o\u3007, (ViewGroup)this, false);
            final RelativeLayout$LayoutParams relativeLayout$LayoutParams = new RelativeLayout$LayoutParams(-1, -1);
            final int[] \u3007\u3007888 = guidePage.\u3007\u3007888();
            if (\u3007\u3007888 != null && \u3007\u3007888.length > 0) {
                while (i < \u3007\u3007888.length) {
                    final int j = \u3007\u3007888[i];
                    final View viewById = inflate.findViewById(j);
                    if (viewById != null) {
                        viewById.setOnClickListener((View$OnClickListener)new View$OnClickListener(this) {
                            final GuideLayout o0;
                            
                            public void onClick(final View view) {
                                this.o0.oO80();
                            }
                        });
                    }
                    else {
                        final StringBuilder sb = new StringBuilder();
                        sb.append("can't find the view by id : ");
                        sb.append(j);
                        sb.append(" which used to remove guide page");
                    }
                    ++i;
                }
            }
            guidePage.\u3007O8o08O();
            ((ViewGroup)this).addView(inflate, (ViewGroup$LayoutParams)relativeLayout$LayoutParams);
        }
        final List<RelativeGuide> oo0o\u3007\u3007 = guidePage.OO0o\u3007\u3007();
        if (oo0o\u3007\u3007.size() > 0) {
            final Iterator<RelativeGuide> iterator = oo0o\u3007\u3007.iterator();
            while (iterator.hasNext()) {
                ((ViewGroup)this).addView(iterator.next().\u3007080((ViewGroup)((View)this).getParent()));
            }
        }
    }
    
    private void \u3007o\u3007() {
        if (((View)this).getParent() != null) {
            ((ViewGroup)((View)this).getParent()).removeView((View)this);
            final OnGuideLayoutDismissListener \u300708O\u300700\u3007o = this.\u300708O\u300700\u3007o;
            if (\u300708O\u300700\u3007o != null) {
                \u300708O\u300700\u3007o.\u3007080(this);
            }
        }
    }
    
    private void \u3007\u3007888(final Canvas canvas, final HighLight highLight, final RectF rectF) {
        highLight.getOptions();
    }
    
    public void oO80() {
        final Animation \u300780\u3007808\u3007O = this.OO.\u300780\u3007808\u3007O();
        if (\u300780\u3007808\u3007O != null) {
            \u300780\u3007808\u3007O.setAnimationListener((Animation$AnimationListener)new AnimationListenerAdapter(this) {
                final GuideLayout \u3007080;
                
                public void onAnimationEnd(final Animation animation) {
                    this.\u3007080.\u3007o\u3007();
                }
            });
            ((View)this).startAnimation(\u300780\u3007808\u3007O);
        }
        else {
            this.\u3007o\u3007();
        }
    }
    
    protected void onAttachedToWindow() {
        super.onAttachedToWindow();
        this.\u3007o00\u3007\u3007Oo(this.OO);
        final Animation oo80 = this.OO.oO80();
        if (oo80 != null) {
            ((View)this).startAnimation(oo80);
        }
    }
    
    protected void onDraw(final Canvas canvas) {
        super.onDraw(canvas);
        int o\u30070;
        if ((o\u30070 = this.OO.o\u30070()) == 0) {
            o\u30070 = -1308622848;
        }
        canvas.drawColor(o\u30070);
        this.O8(canvas);
    }
    
    public boolean onTouchEvent(final MotionEvent motionEvent) {
        final int action = motionEvent.getAction();
        if (action != 0) {
            if (action == 1 || action == 3) {
                final float x = motionEvent.getX();
                final float y = motionEvent.getY();
                if (Math.abs(x - this.o\u300700O) < this.\u3007080OO8\u30070 && Math.abs(y - this.O8o08O8O) < this.\u3007080OO8\u30070) {
                    for (final HighLight highLight : this.OO.OO0o\u3007\u3007\u3007\u30070()) {
                        if (highLight.\u3007080((View)((View)this).getParent()).contains(x, y)) {
                            this.o\u30070(highLight);
                            return true;
                        }
                    }
                    this.performClick();
                }
            }
        }
        else {
            this.o\u300700O = motionEvent.getX();
            this.O8o08O8O = motionEvent.getY();
        }
        return super.onTouchEvent(motionEvent);
    }
    
    public boolean performClick() {
        return super.performClick();
    }
    
    public void setOnGuideLayoutDismissListener(final OnGuideLayoutDismissListener \u300708O\u300700\u3007o) {
        this.\u300708O\u300700\u3007o = \u300708O\u300700\u3007o;
    }
    
    public interface OnGuideLayoutDismissListener
    {
        void \u3007080(final GuideLayout p0);
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.app.hubert.guide.core;

import android.text.TextUtils;
import java.util.ArrayList;
import android.app.Fragment;
import com.app.hubert.guide.model.GuidePage;
import java.util.List;
import android.app.Activity;
import android.view.View;
import com.app.hubert.guide.listener.OnGuideChangedListener;

public class Builder
{
    String O8;
    boolean Oo08;
    OnGuideChangedListener oO80;
    View o\u30070;
    Activity \u3007080;
    List<GuidePage> \u300780\u3007808\u3007O;
    Fragment \u3007o00\u3007\u3007Oo;
    androidx.fragment.app.Fragment \u3007o\u3007;
    int \u3007\u3007888;
    
    public Builder(final Activity \u3007080) {
        this.\u3007\u3007888 = 1;
        this.\u300780\u3007808\u3007O = new ArrayList<GuidePage>();
        this.\u3007080 = \u3007080;
    }
    
    private void O8() {
        if (TextUtils.isEmpty((CharSequence)this.O8)) {
            throw new IllegalArgumentException("the param 'label' is missing, please call setLabel()");
        }
        if (this.\u3007080 == null && (this.\u3007o00\u3007\u3007Oo != null || this.\u3007o\u3007 != null)) {
            throw new IllegalStateException("activity is null, please make sure that fragment is showing when call NewbieGuide");
        }
    }
    
    public Builder Oo08(final String o8) {
        this.O8 = o8;
        return this;
    }
    
    public Builder o\u30070(final OnGuideChangedListener oo80) {
        this.oO80 = oo80;
        return this;
    }
    
    public Builder \u3007080(final GuidePage guidePage) {
        this.\u300780\u3007808\u3007O.add(guidePage);
        return this;
    }
    
    public Builder \u3007o00\u3007\u3007Oo(final boolean oo08) {
        this.Oo08 = oo08;
        return this;
    }
    
    public Builder \u3007o\u3007(final View o\u30070) {
        this.o\u30070 = o\u30070;
        return this;
    }
    
    public Controller \u3007\u3007888() {
        this.O8();
        final Controller controller = new Controller(this);
        controller.OO0o\u3007\u3007();
        return controller;
    }
}

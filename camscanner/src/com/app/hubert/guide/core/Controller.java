// 
// Decompiled by Procyon v0.6.0
// 

package com.app.hubert.guide.core;

import android.app.FragmentManager;
import com.app.hubert.guide.lifecycle.V4ListenerFragment;
import com.app.hubert.guide.lifecycle.FragmentLifecycle;
import com.app.hubert.guide.util.LogUtil;
import com.app.hubert.guide.lifecycle.FragmentLifecycleAdapter;
import com.app.hubert.guide.lifecycle.ListenerFragment;
import android.widget.FrameLayout$LayoutParams;
import java.lang.reflect.Field;
import android.view.ViewGroup$LayoutParams;
import android.view.View;
import android.view.ViewGroup;
import android.content.Context;
import android.app.Fragment;
import android.content.SharedPreferences;
import android.widget.FrameLayout;
import android.app.Activity;
import com.app.hubert.guide.model.GuidePage;
import java.util.List;
import com.app.hubert.guide.listener.OnGuideChangedListener;

public class Controller
{
    private OnGuideChangedListener O8;
    private int OO0o\u3007\u3007;
    private GuideLayout OO0o\u3007\u3007\u3007\u30070;
    private String Oo08;
    private List<GuidePage> oO80;
    private boolean o\u30070;
    private Activity \u3007080;
    private int \u300780\u3007808\u3007O;
    private FrameLayout \u30078o8o\u3007;
    private SharedPreferences \u3007O8o08O;
    private Fragment \u3007o00\u3007\u3007Oo;
    private androidx.fragment.app.Fragment \u3007o\u3007;
    private int \u3007\u3007888;
    
    public Controller(final Builder builder) {
        this.OO0o\u3007\u3007 = -1;
        final Activity \u3007080 = builder.\u3007080;
        this.\u3007080 = \u3007080;
        this.\u3007o00\u3007\u3007Oo = builder.\u3007o00\u3007\u3007Oo;
        this.\u3007o\u3007 = builder.\u3007o\u3007;
        this.O8 = builder.oO80;
        this.Oo08 = builder.O8;
        this.o\u30070 = builder.Oo08;
        this.oO80 = builder.\u300780\u3007808\u3007O;
        this.\u3007\u3007888 = builder.\u3007\u3007888;
        View view;
        if ((view = builder.o\u30070) == null) {
            view = \u3007080.findViewById(16908290);
        }
        if (view instanceof FrameLayout) {
            this.\u30078o8o\u3007 = (FrameLayout)view;
        }
        else {
            final FrameLayout \u30078o8o\u3007 = new FrameLayout((Context)this.\u3007080);
            final ViewGroup viewGroup = (ViewGroup)view.getParent();
            this.OO0o\u3007\u3007 = viewGroup.indexOfChild(view);
            viewGroup.removeView(view);
            final int oo0o\u3007\u3007 = this.OO0o\u3007\u3007;
            if (oo0o\u3007\u3007 >= 0) {
                viewGroup.addView((View)\u30078o8o\u3007, oo0o\u3007\u3007, view.getLayoutParams());
            }
            else {
                viewGroup.addView((View)\u30078o8o\u3007, view.getLayoutParams());
            }
            ((ViewGroup)\u30078o8o\u3007).addView(view, new ViewGroup$LayoutParams(-1, -1));
            this.\u30078o8o\u3007 = \u30078o8o\u3007;
        }
        this.\u3007O8o08O = ((Context)this.\u3007080).getSharedPreferences("NewbieGuide", 0);
    }
    
    private void OO0o\u3007\u3007\u3007\u30070(final Fragment obj) {
        try {
            final Field declaredField = Fragment.class.getDeclaredField("mChildFragmentManager");
            declaredField.setAccessible(true);
            declaredField.set(obj, null);
        }
        catch (final IllegalAccessException cause) {
            throw new RuntimeException(cause);
        }
        catch (final NoSuchFieldException cause2) {
            throw new RuntimeException(cause2);
        }
    }
    
    private void Oooo8o0\u3007() {
        final GuideLayout oo0o\u3007\u3007\u3007\u30070 = new GuideLayout((Context)this.\u3007080, this.oO80.get(this.\u300780\u3007808\u3007O), this);
        oo0o\u3007\u3007\u3007\u30070.setOnGuideLayoutDismissListener((GuideLayout.OnGuideLayoutDismissListener)new GuideLayout.OnGuideLayoutDismissListener(this) {
            final Controller \u3007080;
            
            @Override
            public void \u3007080(final GuideLayout guideLayout) {
                this.\u3007080.\u3007\u3007808\u3007();
            }
        });
        ((ViewGroup)this.\u30078o8o\u3007).addView((View)oo0o\u3007\u3007\u3007\u30070, (ViewGroup$LayoutParams)new FrameLayout$LayoutParams(-1, -1));
        this.OO0o\u3007\u3007\u3007\u30070 = oo0o\u3007\u3007\u3007\u30070;
    }
    
    private void \u300780\u3007808\u3007O() {
        final Fragment \u3007o00\u3007\u3007Oo = this.\u3007o00\u3007\u3007Oo;
        if (\u3007o00\u3007\u3007Oo != null) {
            this.OO0o\u3007\u3007\u3007\u30070(\u3007o00\u3007\u3007Oo);
            final FragmentManager childFragmentManager = this.\u3007o00\u3007\u3007Oo.getChildFragmentManager();
            ListenerFragment listenerFragment;
            if ((listenerFragment = (ListenerFragment)childFragmentManager.findFragmentByTag("listener_fragment")) == null) {
                listenerFragment = new ListenerFragment();
                childFragmentManager.beginTransaction().add((Fragment)listenerFragment, "listener_fragment").commitAllowingStateLoss();
            }
            listenerFragment.\u3007080(new FragmentLifecycleAdapter(this) {
                final Controller \u3007080;
                
                @Override
                public void onDestroyView() {
                    LogUtil.\u3007o\u3007("ListenerFragment.onDestroyView");
                    this.\u3007080.\u30078o8o\u3007();
                }
            });
        }
        final androidx.fragment.app.Fragment \u3007o\u3007 = this.\u3007o\u3007;
        if (\u3007o\u3007 != null) {
            final androidx.fragment.app.FragmentManager childFragmentManager2 = \u3007o\u3007.getChildFragmentManager();
            V4ListenerFragment v4ListenerFragment;
            if ((v4ListenerFragment = (V4ListenerFragment)childFragmentManager2.findFragmentByTag("listener_fragment")) == null) {
                v4ListenerFragment = new V4ListenerFragment();
                childFragmentManager2.beginTransaction().add(v4ListenerFragment, "listener_fragment").commitAllowingStateLoss();
            }
            v4ListenerFragment.\u300780O8o8O\u3007(new FragmentLifecycleAdapter(this) {
                final Controller \u3007080;
                
                @Override
                public void onDestroyView() {
                    LogUtil.\u3007o\u3007("v4ListenerFragment.onDestroyView");
                    this.\u3007080.\u30078o8o\u3007();
                }
            });
        }
    }
    
    private void \u3007O8o08O() {
        final Fragment \u3007o00\u3007\u3007Oo = this.\u3007o00\u3007\u3007Oo;
        if (\u3007o00\u3007\u3007Oo != null) {
            final FragmentManager childFragmentManager = \u3007o00\u3007\u3007Oo.getChildFragmentManager();
            final ListenerFragment listenerFragment = (ListenerFragment)childFragmentManager.findFragmentByTag("listener_fragment");
            if (listenerFragment != null) {
                childFragmentManager.beginTransaction().remove((Fragment)listenerFragment).commitAllowingStateLoss();
            }
        }
        final androidx.fragment.app.Fragment \u3007o\u3007 = this.\u3007o\u3007;
        if (\u3007o\u3007 != null) {
            final androidx.fragment.app.FragmentManager childFragmentManager2 = \u3007o\u3007.getChildFragmentManager();
            final V4ListenerFragment v4ListenerFragment = (V4ListenerFragment)childFragmentManager2.findFragmentByTag("listener_fragment");
            if (v4ListenerFragment != null) {
                childFragmentManager2.beginTransaction().remove(v4ListenerFragment).commitAllowingStateLoss();
            }
        }
    }
    
    private void \u3007\u3007808\u3007() {
        if (this.\u300780\u3007808\u3007O < this.oO80.size() - 1) {
            ++this.\u300780\u3007808\u3007O;
            this.Oooo8o0\u3007();
        }
        else {
            final OnGuideChangedListener o8 = this.O8;
            if (o8 != null) {
                o8.\u3007o00\u3007\u3007Oo(this);
            }
            this.\u3007O8o08O();
        }
    }
    
    public void OO0o\u3007\u3007() {
        final int int1 = this.\u3007O8o08O.getInt(this.Oo08, 0);
        if (!this.o\u30070 && int1 >= this.\u3007\u3007888) {
            return;
        }
        ((View)this.\u30078o8o\u3007).post((Runnable)new Runnable(this, int1) {
            final int o0;
            final Controller \u3007OOo8\u30070;
            
            @Override
            public void run() {
                if (this.\u3007OOo8\u30070.oO80 != null && this.\u3007OOo8\u30070.oO80.size() != 0) {
                    this.\u3007OOo8\u30070.\u300780\u3007808\u3007O = 0;
                    this.\u3007OOo8\u30070.Oooo8o0\u3007();
                    if (this.\u3007OOo8\u30070.O8 != null) {
                        this.\u3007OOo8\u30070.O8.\u3007080(this.\u3007OOo8\u30070);
                    }
                    this.\u3007OOo8\u30070.\u300780\u3007808\u3007O();
                    this.\u3007OOo8\u30070.\u3007O8o08O.edit().putInt(this.\u3007OOo8\u30070.Oo08, this.o0 + 1).apply();
                    return;
                }
                throw new IllegalStateException("there is no guide to show!! Please add at least one Page.");
            }
        });
    }
    
    public void \u30078o8o\u3007() {
        final GuideLayout oo0o\u3007\u3007\u3007\u30070 = this.OO0o\u3007\u3007\u3007\u30070;
        if (oo0o\u3007\u3007\u3007\u30070 != null && ((View)oo0o\u3007\u3007\u3007\u30070).getParent() != null) {
            final ViewGroup viewGroup = (ViewGroup)((View)this.OO0o\u3007\u3007\u3007\u30070).getParent();
            viewGroup.removeView((View)this.OO0o\u3007\u3007\u3007\u30070);
            if (!(viewGroup instanceof FrameLayout)) {
                final ViewGroup viewGroup2 = (ViewGroup)((View)viewGroup).getParent();
                final View child = viewGroup.getChildAt(0);
                viewGroup.removeAllViews();
                if (child != null) {
                    final int oo0o\u3007\u3007 = this.OO0o\u3007\u3007;
                    if (oo0o\u3007\u3007 > 0) {
                        viewGroup2.addView(child, oo0o\u3007\u3007, ((View)viewGroup).getLayoutParams());
                    }
                    else {
                        viewGroup2.addView(child, ((View)viewGroup).getLayoutParams());
                    }
                }
            }
        }
        final OnGuideChangedListener o8 = this.O8;
        if (o8 != null) {
            o8.\u3007o00\u3007\u3007Oo(this);
        }
    }
}

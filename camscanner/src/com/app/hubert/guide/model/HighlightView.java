// 
// Decompiled by Procyon v0.6.0
// 

package com.app.hubert.guide.model;

import android.graphics.Rect;
import com.app.hubert.guide.util.LogUtil;
import com.app.hubert.guide.util.ViewUtils;
import android.view.View;
import android.graphics.RectF;

public class HighlightView implements HighLight
{
    private int O8;
    private HighlightOptions Oo08;
    private RectF o\u30070;
    private View \u3007080;
    private Shape \u3007o00\u3007\u3007Oo;
    private int \u3007o\u3007;
    
    public HighlightView(final View \u3007080, final Shape \u3007o00\u3007\u3007Oo, final int \u3007o\u3007, final int o8) {
        this.\u3007080 = \u3007080;
        this.\u3007o00\u3007\u3007Oo = \u3007o00\u3007\u3007Oo;
        this.\u3007o\u3007 = \u3007o\u3007;
        this.O8 = o8;
    }
    
    @Override
    public HighlightOptions getOptions() {
        return this.Oo08;
    }
    
    @Override
    public float getRadius() {
        final View \u3007080 = this.\u3007080;
        if (\u3007080 != null) {
            return (float)(Math.max(\u3007080.getWidth() / 2, this.\u3007080.getHeight() / 2) + this.O8);
        }
        throw new IllegalArgumentException("the highlight view is null!");
    }
    
    @Override
    public Shape getShape() {
        return this.\u3007o00\u3007\u3007Oo;
    }
    
    @Override
    public RectF \u3007080(final View view) {
        if (this.\u3007080 != null) {
            if (this.o\u30070 == null) {
                this.o\u30070 = new RectF();
                final Rect \u3007080 = ViewUtils.\u3007080(view, this.\u3007080);
                final RectF o\u30070 = this.o\u30070;
                final int left = \u3007080.left;
                final int o8 = this.O8;
                o\u30070.left = (float)(left - o8);
                o\u30070.top = (float)(\u3007080.top - o8);
                o\u30070.right = (float)(\u3007080.right + o8);
                o\u30070.bottom = (float)(\u3007080.bottom + o8);
                final StringBuilder sb = new StringBuilder();
                sb.append(this.\u3007080.getClass().getSimpleName());
                sb.append("'s location:");
                sb.append(this.o\u30070);
                LogUtil.\u3007o\u3007(sb.toString());
            }
            return this.o\u30070;
        }
        throw new IllegalArgumentException("the highlight view is null!");
    }
    
    @Override
    public int \u3007o00\u3007\u3007Oo() {
        return this.\u3007o\u3007;
    }
    
    public void \u3007o\u3007(final HighlightOptions oo08) {
        this.Oo08 = oo08;
    }
}

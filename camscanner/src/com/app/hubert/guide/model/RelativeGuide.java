// 
// Decompiled by Procyon v0.6.0
// 

package com.app.hubert.guide.model;

import android.view.ViewGroup$LayoutParams;
import com.app.hubert.guide.util.LogUtil;
import android.widget.FrameLayout$LayoutParams;
import android.view.LayoutInflater;
import android.graphics.RectF;
import android.view.View;
import android.view.ViewGroup;
import androidx.annotation.LayoutRes;

public class RelativeGuide
{
    public int O8;
    public HighLight \u3007080;
    @LayoutRes
    public int \u3007o00\u3007\u3007Oo;
    public int \u3007o\u3007;
    
    public RelativeGuide(@LayoutRes final int \u3007o00\u3007\u3007Oo, final int o8, final int \u3007o\u3007) {
        this.\u3007o00\u3007\u3007Oo = \u3007o00\u3007\u3007Oo;
        this.O8 = o8;
        this.\u3007o\u3007 = \u3007o\u3007;
    }
    
    private MarginInfo \u3007o00\u3007\u3007Oo(final int n, final ViewGroup viewGroup, final View view) {
        final MarginInfo marginInfo = new MarginInfo();
        final RectF \u3007080 = this.\u3007080.\u3007080((View)viewGroup);
        if (n != 3) {
            if (n != 5) {
                if (n != 48) {
                    if (n == 80) {
                        marginInfo.\u3007o00\u3007\u3007Oo = (int)(\u3007080.bottom + this.\u3007o\u3007);
                        marginInfo.\u3007080 = (int)\u3007080.left;
                    }
                }
                else {
                    marginInfo.Oo08 = 80;
                    marginInfo.O8 = (int)(((View)viewGroup).getHeight() - \u3007080.top + this.\u3007o\u3007);
                    marginInfo.\u3007080 = (int)\u3007080.left;
                }
            }
            else {
                marginInfo.\u3007080 = (int)(\u3007080.right + this.\u3007o\u3007);
                marginInfo.\u3007o00\u3007\u3007Oo = (int)\u3007080.top;
            }
        }
        else {
            marginInfo.Oo08 = 5;
            marginInfo.\u3007o\u3007 = (int)(((View)viewGroup).getWidth() - \u3007080.left + this.\u3007o\u3007);
            marginInfo.\u3007o00\u3007\u3007Oo = (int)\u3007080.top;
        }
        return marginInfo;
    }
    
    protected void O8(final View view) {
    }
    
    public final View \u3007080(final ViewGroup viewGroup) {
        final View inflate = LayoutInflater.from(((View)viewGroup).getContext()).inflate(this.\u3007o00\u3007\u3007Oo, viewGroup, false);
        this.O8(inflate);
        final FrameLayout$LayoutParams layoutParams = (FrameLayout$LayoutParams)inflate.getLayoutParams();
        final MarginInfo \u3007o00\u3007\u3007Oo = this.\u3007o00\u3007\u3007Oo(this.O8, viewGroup, inflate);
        LogUtil.\u3007o00\u3007\u3007Oo(\u3007o00\u3007\u3007Oo.toString());
        this.\u3007o\u3007(\u3007o00\u3007\u3007Oo, viewGroup, inflate);
        layoutParams.gravity = \u3007o00\u3007\u3007Oo.Oo08;
        layoutParams.leftMargin += \u3007o00\u3007\u3007Oo.\u3007080;
        layoutParams.topMargin += \u3007o00\u3007\u3007Oo.\u3007o00\u3007\u3007Oo;
        layoutParams.rightMargin += \u3007o00\u3007\u3007Oo.\u3007o\u3007;
        layoutParams.bottomMargin += \u3007o00\u3007\u3007Oo.O8;
        inflate.setLayoutParams((ViewGroup$LayoutParams)layoutParams);
        return inflate;
    }
    
    protected void \u3007o\u3007(final MarginInfo marginInfo, final ViewGroup viewGroup, final View view) {
    }
    
    public static class MarginInfo
    {
        public int O8;
        public int Oo08;
        public int \u3007080;
        public int \u3007o00\u3007\u3007Oo;
        public int \u3007o\u3007;
        
        @Override
        public String toString() {
            final StringBuilder sb = new StringBuilder();
            sb.append("MarginInfo{leftMargin=");
            sb.append(this.\u3007080);
            sb.append(", topMargin=");
            sb.append(this.\u3007o00\u3007\u3007Oo);
            sb.append(", rightMargin=");
            sb.append(this.\u3007o\u3007);
            sb.append(", bottomMargin=");
            sb.append(this.O8);
            sb.append(", gravity=");
            sb.append(this.Oo08);
            sb.append('}');
            return sb.toString();
        }
    }
}

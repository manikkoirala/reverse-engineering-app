// 
// Decompiled by Procyon v0.6.0
// 

package com.app.hubert.guide.model;

import android.view.View$OnClickListener;

public class HighlightOptions
{
    public View$OnClickListener \u3007080;
    public RelativeGuide \u3007o00\u3007\u3007Oo;
    
    public static class Builder
    {
        private HighlightOptions \u3007080;
        
        public Builder() {
            this.\u3007080 = new HighlightOptions();
        }
        
        public HighlightOptions \u3007080() {
            return this.\u3007080;
        }
        
        public Builder \u3007o00\u3007\u3007Oo(final View$OnClickListener \u3007080) {
            this.\u3007080.\u3007080 = \u3007080;
            return this;
        }
        
        public Builder \u3007o\u3007(final RelativeGuide \u3007o00\u3007\u3007Oo) {
            this.\u3007080.\u3007o00\u3007\u3007Oo = \u3007o00\u3007\u3007Oo;
            return this;
        }
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.app.hubert.guide.model;

import android.view.View;
import androidx.annotation.NonNull;
import android.graphics.RectF;

public class HighlightRectF implements HighLight
{
    private HighlightOptions O8;
    private RectF \u3007080;
    private Shape \u3007o00\u3007\u3007Oo;
    private int \u3007o\u3007;
    
    public HighlightRectF(@NonNull final RectF \u3007080, @NonNull final Shape \u3007o00\u3007\u3007Oo, final int \u3007o\u3007) {
        this.\u3007080 = \u3007080;
        this.\u3007o00\u3007\u3007Oo = \u3007o00\u3007\u3007Oo;
        this.\u3007o\u3007 = \u3007o\u3007;
    }
    
    @Override
    public HighlightOptions getOptions() {
        return this.O8;
    }
    
    @Override
    public float getRadius() {
        return Math.min(this.\u3007080.width() / 2.0f, this.\u3007080.height() / 2.0f);
    }
    
    @Override
    public Shape getShape() {
        return this.\u3007o00\u3007\u3007Oo;
    }
    
    @Override
    public RectF \u3007080(final View view) {
        return this.\u3007080;
    }
    
    @Override
    public int \u3007o00\u3007\u3007Oo() {
        return this.\u3007o\u3007;
    }
    
    public void \u3007o\u3007(final HighlightOptions o8) {
        this.O8 = o8;
    }
}

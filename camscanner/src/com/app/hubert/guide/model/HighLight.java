// 
// Decompiled by Procyon v0.6.0
// 

package com.app.hubert.guide.model;

import android.graphics.RectF;
import android.view.View;
import androidx.annotation.Nullable;

public interface HighLight
{
    @Nullable
    HighlightOptions getOptions();
    
    float getRadius();
    
    Shape getShape();
    
    RectF \u3007080(final View p0);
    
    int \u3007o00\u3007\u3007Oo();
    
    public enum Shape
    {
        private static final Shape[] $VALUES;
        
        CIRCLE, 
        OVAL, 
        RECTANGLE, 
        ROUND_RECTANGLE;
    }
}

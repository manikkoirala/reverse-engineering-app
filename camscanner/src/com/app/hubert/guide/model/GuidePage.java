// 
// Decompiled by Procyon v0.6.0
// 

package com.app.hubert.guide.model;

import androidx.annotation.Nullable;
import androidx.annotation.ColorInt;
import com.app.hubert.guide.listener.OnLayoutInflatedListener;
import android.graphics.RectF;
import java.util.Iterator;
import android.view.View;
import java.util.ArrayList;
import java.util.List;
import android.view.animation.Animation;

public class GuidePage
{
    private int O8;
    private int[] Oo08;
    private Animation o\u30070;
    private List<HighLight> \u3007080;
    private boolean \u3007o00\u3007\u3007Oo;
    private int \u3007o\u3007;
    private Animation \u3007\u3007888;
    
    public GuidePage() {
        this.\u3007080 = new ArrayList<HighLight>();
        this.\u3007o00\u3007\u3007Oo = true;
    }
    
    public static GuidePage \u3007\u3007808\u3007() {
        return new GuidePage();
    }
    
    public GuidePage O8(final View view, final HighLight.Shape shape, final int n, final int n2, final HighlightOptions highlightOptions) {
        final HighlightView \u3007080 = new HighlightView(view, shape, n, n2);
        if (highlightOptions != null) {
            final RelativeGuide \u3007o00\u3007\u3007Oo = highlightOptions.\u3007o00\u3007\u3007Oo;
            if (\u3007o00\u3007\u3007Oo != null) {
                \u3007o00\u3007\u3007Oo.\u3007080 = \u3007080;
            }
        }
        \u3007080.\u3007o\u3007(highlightOptions);
        this.\u3007080.add(\u3007080);
        return this;
    }
    
    public List<RelativeGuide> OO0o\u3007\u3007() {
        final ArrayList list = new ArrayList();
        final Iterator<HighLight> iterator = this.\u3007080.iterator();
        while (iterator.hasNext()) {
            final HighlightOptions options = iterator.next().getOptions();
            if (options != null) {
                final RelativeGuide \u3007o00\u3007\u3007Oo = options.\u3007o00\u3007\u3007Oo;
                if (\u3007o00\u3007\u3007Oo == null) {
                    continue;
                }
                list.add(\u3007o00\u3007\u3007Oo);
            }
        }
        return list;
    }
    
    public List<HighLight> OO0o\u3007\u3007\u3007\u30070() {
        return this.\u3007080;
    }
    
    public GuidePage Oo08(final View view, final HighLight.Shape shape, final HighlightOptions highlightOptions) {
        return this.O8(view, shape, 0, 0, highlightOptions);
    }
    
    public boolean Oooo8o0\u3007() {
        return this.\u3007o00\u3007\u3007Oo;
    }
    
    public Animation oO80() {
        return this.o\u30070;
    }
    
    public int o\u30070() {
        return this.\u3007o\u3007;
    }
    
    public GuidePage \u3007080(final RectF rectF, final HighLight.Shape shape, final int n, final RelativeGuide relativeGuide) {
        final HighlightRectF \u3007080 = new HighlightRectF(rectF, shape, n);
        if (relativeGuide != null) {
            ((HighlightRectF)(relativeGuide.\u3007080 = \u3007080)).\u3007o\u3007(new HighlightOptions.Builder().\u3007o\u3007(relativeGuide).\u3007080());
        }
        this.\u3007080.add(\u3007080);
        return this;
    }
    
    public Animation \u300780\u3007808\u3007O() {
        return this.\u3007\u3007888;
    }
    
    public int \u30078o8o\u3007() {
        return this.O8;
    }
    
    public GuidePage \u3007O00(final boolean \u3007o00\u3007\u3007Oo) {
        this.\u3007o00\u3007\u3007Oo = \u3007o00\u3007\u3007Oo;
        return this;
    }
    
    public OnLayoutInflatedListener \u3007O8o08O() {
        return null;
    }
    
    public GuidePage \u3007O\u3007(@ColorInt final int \u3007o\u3007) {
        this.\u3007o\u3007 = \u3007o\u3007;
        return this;
    }
    
    public GuidePage \u3007o00\u3007\u3007Oo(final View view, final HighLight.Shape shape, final int n, final int n2, @Nullable final RelativeGuide relativeGuide) {
        final HighlightView \u3007080 = new HighlightView(view, shape, n, n2);
        if (relativeGuide != null) {
            ((HighlightView)(relativeGuide.\u3007080 = \u3007080)).\u3007o\u3007(new HighlightOptions.Builder().\u3007o\u3007(relativeGuide).\u3007080());
        }
        this.\u3007080.add(\u3007080);
        return this;
    }
    
    public GuidePage \u3007o\u3007(final View view, final HighLight.Shape shape, final RelativeGuide relativeGuide) {
        return this.\u3007o00\u3007\u3007Oo(view, shape, 0, 0, relativeGuide);
    }
    
    public int[] \u3007\u3007888() {
        return this.Oo08;
    }
}

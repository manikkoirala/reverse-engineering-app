// 
// Decompiled by Procyon v0.6.0
// 

package com.applovin.adview;

import androidx.lifecycle.Lifecycle;
import android.view.ViewGroup;
import com.applovin.sdk.AppLovinAd;
import androidx.annotation.Nullable;
import androidx.annotation.NonNull;
import com.applovin.sdk.AppLovinAdVideoPlaybackListener;
import com.applovin.sdk.AppLovinAdLoadListener;
import com.applovin.sdk.AppLovinAdDisplayListener;
import com.applovin.sdk.AppLovinAdClickListener;

public interface AppLovinInterstitialAdDialog
{
    void setAdClickListener(final AppLovinAdClickListener p0);
    
    void setAdDisplayListener(final AppLovinAdDisplayListener p0);
    
    void setAdLoadListener(final AppLovinAdLoadListener p0);
    
    void setAdVideoPlaybackListener(final AppLovinAdVideoPlaybackListener p0);
    
    void setExtraInfo(@NonNull final String p0, @Nullable final Object p1);
    
    void show();
    
    void showAndRender(final AppLovinAd p0);
    
    void showAndRender(final AppLovinAd p0, final ViewGroup p1, final Lifecycle p2);
}

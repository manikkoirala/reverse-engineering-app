// 
// Decompiled by Procyon v0.6.0
// 

package com.applovin.adview;

import com.applovin.impl.adview.q;
import android.content.Context;
import com.applovin.sdk.AppLovinSdk;

public class AppLovinInterstitialAd
{
    public static AppLovinInterstitialAdDialog create(final AppLovinSdk appLovinSdk, final Context context) {
        if (appLovinSdk == null) {
            throw new IllegalArgumentException("No sdk specified");
        }
        if (context != null) {
            return (AppLovinInterstitialAdDialog)new q(appLovinSdk, context);
        }
        throw new IllegalArgumentException("No context specified");
    }
    
    @Override
    public String toString() {
        return "AppLovinInterstitialAd{}";
    }
}

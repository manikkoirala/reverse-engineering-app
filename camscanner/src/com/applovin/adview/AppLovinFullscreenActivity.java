// 
// Decompiled by Procyon v0.6.0
// 

package com.applovin.adview;

import androidx.annotation.RequiresApi;
import com.applovin.impl.adview.activity.b.e;
import android.view.KeyEvent;
import android.view.View;
import android.os.Process;
import com.applovin.impl.sdk.utils.StringUtils;
import android.content.ServiceConnection;
import android.content.Intent;
import com.applovin.impl.adview.activity.FullscreenAdService;
import com.applovin.impl.adview.activity.b.a$a;
import com.applovin.impl.sdk.c.b;
import com.applovin.sdk.AppLovinSdk;
import android.content.Context;
import com.applovin.sdk.AppLovinSdkSettings;
import android.text.TextUtils;
import com.applovin.impl.sdk.y;
import androidx.annotation.Nullable;
import android.os.Bundle;
import androidx.annotation.NonNull;
import android.content.res.Configuration;
import android.window.OnBackInvokedCallback;
import com.applovin.impl.sdk.utils.h;
import com.applovin.impl.sdk.utils.w;
import java.util.concurrent.atomic.AtomicBoolean;
import com.applovin.impl.adview.activity.b.a;
import com.applovin.impl.sdk.o;
import android.annotation.SuppressLint;
import com.applovin.impl.adview.q;
import com.applovin.impl.adview.l;
import android.app.Activity;

public class AppLovinFullscreenActivity extends Activity implements l
{
    @SuppressLint({ "StaticFieldLeak" })
    public static q parentInterstitialWrapper;
    private o a;
    private com.applovin.impl.adview.activity.b.a b;
    private final AtomicBoolean c;
    private com.applovin.impl.adview.activity.a d;
    private a e;
    private boolean f;
    
    public AppLovinFullscreenActivity() {
        this.c = new AtomicBoolean(true);
    }
    
    private void a() {
        final com.applovin.impl.adview.activity.b.a b = this.b;
        if (b != null) {
            b.l();
        }
        if (w.d(((Context)this).getApplicationContext())) {
            super.onBackPressed();
        }
    }
    
    public void dismiss() {
        if (h.l() && this.e != null) {
            this.getOnBackInvokedDispatcher().unregisterOnBackInvokedCallback((OnBackInvokedCallback)this.e);
            this.e = null;
        }
        final com.applovin.impl.adview.activity.b.a b = this.b;
        if (b != null) {
            b.h();
        }
        else {
            this.finish();
        }
    }
    
    public void onBackPressed() {
        this.a();
    }
    
    public void onConfigurationChanged(@NonNull final Configuration configuration) {
        super.onConfigurationChanged(configuration);
        final com.applovin.impl.adview.activity.b.a b = this.b;
        if (b != null) {
            b.a(configuration);
        }
    }
    
    protected void onCreate(@Nullable final Bundle bundle) {
        super.onCreate(bundle);
        if (bundle != null && AppLovinFullscreenActivity.parentInterstitialWrapper == null) {
            if (bundle.getBoolean("com.applovin.dismiss_on_restore", false)) {
                y.h("AppLovinFullscreenActivity", "Dismissing ad. Activity was destroyed while in background.");
                this.dismiss();
                return;
            }
            y.f("AppLovinFullscreenActivity", "Activity was destroyed while in background.");
        }
        try {
            this.requestWindowFeature(1);
        }
        finally {
            final Throwable t;
            y.c("AppLovinFullscreenActivity", "Failed to request window feature", t);
        }
        this.getWindow().setFlags(1024, 1024);
        this.getWindow().addFlags(16777216);
        this.getWindow().addFlags(128);
        final View viewById = this.findViewById(16908290);
        viewById.setBackgroundColor(-16777216);
        final String stringExtra = this.getIntent().getStringExtra("com.applovin.interstitial.sdk_key");
        if (TextUtils.isEmpty((CharSequence)stringExtra)) {
            final q parentInterstitialWrapper = AppLovinFullscreenActivity.parentInterstitialWrapper;
            if (parentInterstitialWrapper != null && parentInterstitialWrapper.f() != null) {
                q.a(AppLovinFullscreenActivity.parentInterstitialWrapper.f(), AppLovinFullscreenActivity.parentInterstitialWrapper.c(), "Empty SDK key", (Throwable)null, this);
            }
            this.finish();
            return;
        }
        final o a = AppLovinSdk.getInstance(stringExtra, new AppLovinSdkSettings((Context)this), (Context)this).a();
        this.a = a;
        this.f = (boolean)a.a(com.applovin.impl.sdk.c.b.db);
        if (this.a.a(com.applovin.impl.sdk.c.b.dc)) {
            viewById.setFitsSystemWindows(true);
        }
        com.applovin.impl.sdk.utils.b.a(this.f, (Activity)this);
        if (h.l() && (boolean)this.a.a(com.applovin.impl.sdk.c.b.gu)) {
            this.e = new a(new Runnable(this) {
                final AppLovinFullscreenActivity a;
                
                @Override
                public void run() {
                    this.a.a();
                }
            });
            this.getOnBackInvokedDispatcher().registerOnBackInvokedCallback(0, (OnBackInvokedCallback)this.e);
        }
        final q parentInterstitialWrapper2 = AppLovinFullscreenActivity.parentInterstitialWrapper;
        if (parentInterstitialWrapper2 != null) {
            com.applovin.impl.adview.activity.b.a.a(parentInterstitialWrapper2.f(), AppLovinFullscreenActivity.parentInterstitialWrapper.e(), AppLovinFullscreenActivity.parentInterstitialWrapper.c(), AppLovinFullscreenActivity.parentInterstitialWrapper.d(), AppLovinFullscreenActivity.parentInterstitialWrapper.b(), this.a, (Activity)this, (a$a)new a$a(this) {
                final AppLovinFullscreenActivity a;
                
                public void a(final com.applovin.impl.adview.activity.b.a a) {
                    this.a.b = a;
                    a.d();
                }
                
                public void a(final String s, final Throwable t) {
                    q.a(AppLovinFullscreenActivity.parentInterstitialWrapper.f(), AppLovinFullscreenActivity.parentInterstitialWrapper.c(), s, t, this.a);
                }
            });
            return;
        }
        ((Context)this).bindService(new Intent((Context)this, (Class)FullscreenAdService.class), (ServiceConnection)(this.d = new com.applovin.impl.adview.activity.a(this, this.a)), 1);
        if (!h.h()) {
            return;
        }
        final String s = this.a.ay().getExtraParameters().get("disable_set_data_dir_suffix");
        if (StringUtils.isValidString(s) && Boolean.parseBoolean(s)) {
            return;
        }
        try {
            \u3007080.\u3007080(String.valueOf(Process.myPid()));
        }
        finally {}
    }
    
    protected void onDestroy() {
        AppLovinFullscreenActivity.parentInterstitialWrapper = null;
        final com.applovin.impl.adview.activity.a d = this.d;
        if (d != null) {
            try {
                ((Context)this).unbindService((ServiceConnection)d);
            }
            finally {}
        }
        final com.applovin.impl.adview.activity.b.a b = this.b;
        if (b != null) {
            if (!b.i()) {
                this.b.h();
            }
            this.b.k();
        }
        super.onDestroy();
    }
    
    public boolean onKeyDown(final int n, final KeyEvent keyEvent) {
        final com.applovin.impl.adview.activity.b.a b = this.b;
        if (b != null) {
            b.a(n, keyEvent);
        }
        return super.onKeyDown(n, keyEvent);
    }
    
    protected void onPause() {
        super.onPause();
        final com.applovin.impl.adview.activity.b.a b = this.b;
        if (b != null) {
            b.g();
        }
    }
    
    protected void onResume() {
        try {
            super.onResume();
            if (!this.c.get()) {
                final com.applovin.impl.adview.activity.b.a b = this.b;
                if (b != null) {
                    b.f();
                }
            }
        }
        catch (final IllegalArgumentException ex) {
            this.a.F();
            if (y.a()) {
                this.a.F().b("AppLovinFullscreenActivity", "Error was encountered in onResume().", (Throwable)ex);
            }
            this.a.ag().a("AppLovinFullscreenActivity", "onResume", (Throwable)ex);
            this.dismiss();
        }
    }
    
    protected void onSaveInstanceState(@NonNull final Bundle bundle) {
        super.onSaveInstanceState(bundle);
        final o a = this.a;
        if (a != null) {
            bundle.putBoolean("com.applovin.dismiss_on_restore", (boolean)a.a(com.applovin.impl.sdk.c.b.gp));
        }
    }
    
    protected void onStop() {
        super.onStop();
        final com.applovin.impl.adview.activity.b.a b = this.b;
        if (b != null) {
            b.j();
        }
    }
    
    public void onWindowFocusChanged(final boolean b) {
        if (this.b != null) {
            if (!this.c.getAndSet(false) || this.b instanceof e) {
                this.b.c(b);
            }
            if (b) {
                com.applovin.impl.sdk.utils.b.a(this.f, (Activity)this);
            }
        }
        super.onWindowFocusChanged(b);
    }
    
    public void setPresenter(final com.applovin.impl.adview.activity.b.a b) {
        this.b = b;
    }
    
    @RequiresApi(api = 33)
    private static class a implements OnBackInvokedCallback
    {
        private final Runnable a;
        
        protected a(final Runnable a) {
            this.a = a;
        }
        
        public void onBackInvoked() {
            this.a.run();
        }
    }
}

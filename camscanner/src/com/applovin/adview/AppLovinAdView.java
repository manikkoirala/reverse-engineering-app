// 
// Decompiled by Procyon v0.6.0
// 

package com.applovin.adview;

import android.view.ViewGroup;
import androidx.annotation.Nullable;
import androidx.annotation.NonNull;
import com.applovin.sdk.AppLovinAdLoadListener;
import com.applovin.sdk.AppLovinAdDisplayListener;
import com.applovin.sdk.AppLovinAdClickListener;
import com.applovin.sdk.AppLovinAd;
import com.applovin.impl.sdk.y;
import android.util.DisplayMetrics;
import android.view.View;
import android.graphics.Color;
import android.widget.TextView;
import android.util.TypedValue;
import com.applovin.sdk.AppLovinSdk;
import com.applovin.sdk.AppLovinAdSize;
import android.util.AttributeSet;
import android.content.Context;
import com.applovin.impl.adview.b;
import android.widget.RelativeLayout;

public class AppLovinAdView extends RelativeLayout
{
    public static final String NAMESPACE = "http://schemas.applovin.com/android/1.0";
    private b a;
    
    public AppLovinAdView(final Context context, final AttributeSet set) {
        this(context, set, 0);
    }
    
    public AppLovinAdView(final Context context, final AttributeSet set, final int n) {
        super(context, set, n);
        this.a(null, null, null, context, set);
    }
    
    public AppLovinAdView(final AppLovinAdSize appLovinAdSize, final Context context) {
        this(appLovinAdSize, null, context);
    }
    
    public AppLovinAdView(final AppLovinAdSize appLovinAdSize, final String s, final Context context) {
        super(context);
        this.a(appLovinAdSize, s, null, context, null);
    }
    
    public AppLovinAdView(final AppLovinSdk appLovinSdk, final AppLovinAdSize appLovinAdSize, final Context context) {
        this(appLovinSdk, appLovinAdSize, null, context);
    }
    
    public AppLovinAdView(final AppLovinSdk appLovinSdk, final AppLovinAdSize appLovinAdSize, final String s, final Context context) {
        super(context.getApplicationContext());
        this.a(appLovinAdSize, s, appLovinSdk, context, null);
    }
    
    private void a(final AttributeSet set, final Context context) {
        final DisplayMetrics displayMetrics = context.getResources().getDisplayMetrics();
        final int widthPixels = displayMetrics.widthPixels;
        final int n = (int)TypedValue.applyDimension(1, 50.0f, displayMetrics);
        final TextView textView = new TextView(context);
        ((View)textView).setBackgroundColor(Color.rgb(220, 220, 220));
        textView.setTextColor(-16777216);
        textView.setText((CharSequence)"AppLovin Ad");
        textView.setGravity(17);
        ((ViewGroup)this).addView((View)textView, widthPixels, n);
    }
    
    private void a(final AppLovinAdSize appLovinAdSize, final String s, final AppLovinSdk appLovinSdk, final Context context, final AttributeSet set) {
        if (!((View)this).isInEditMode()) {
            (this.a = new b()).a(this, context, appLovinAdSize, s, appLovinSdk, set);
        }
        else {
            this.a(set, context);
        }
    }
    
    public void destroy() {
        final b a = this.a;
        if (a != null) {
            a.f();
        }
    }
    
    public b getController() {
        return this.a;
    }
    
    public AppLovinAdSize getSize() {
        final b a = this.a;
        if (a != null) {
            return a.b();
        }
        return null;
    }
    
    public String getZoneId() {
        final b a = this.a;
        if (a != null) {
            return a.c();
        }
        return null;
    }
    
    public void loadNextAd() {
        final b a = this.a;
        if (a != null) {
            a.a();
        }
        else {
            y.g("AppLovinSdk", "Unable to load next ad: AppLovinAdView is not initialized.");
        }
    }
    
    protected void onAttachedToWindow() {
        super.onAttachedToWindow();
        final b a = this.a;
        if (a != null) {
            a.i();
        }
    }
    
    protected void onDetachedFromWindow() {
        final b a = this.a;
        if (a != null) {
            a.j();
        }
        super.onDetachedFromWindow();
    }
    
    public void pause() {
        final b a = this.a;
        if (a != null) {
            a.d();
        }
    }
    
    public void renderAd(final AppLovinAd appLovinAd) {
        final b a = this.a;
        if (a != null) {
            a.a(appLovinAd);
        }
    }
    
    public void resume() {
        final b a = this.a;
        if (a != null) {
            a.e();
        }
    }
    
    public void setAdClickListener(final AppLovinAdClickListener appLovinAdClickListener) {
        final b a = this.a;
        if (a != null) {
            a.a(appLovinAdClickListener);
        }
    }
    
    public void setAdDisplayListener(final AppLovinAdDisplayListener appLovinAdDisplayListener) {
        final b a = this.a;
        if (a != null) {
            a.a(appLovinAdDisplayListener);
        }
    }
    
    public void setAdLoadListener(final AppLovinAdLoadListener appLovinAdLoadListener) {
        final b a = this.a;
        if (a != null) {
            a.a(appLovinAdLoadListener);
        }
    }
    
    public void setAdViewEventListener(final AppLovinAdViewEventListener appLovinAdViewEventListener) {
        final b a = this.a;
        if (a != null) {
            a.a(appLovinAdViewEventListener);
        }
    }
    
    public void setExtraInfo(@NonNull final String s, @Nullable final Object o) {
        if (s != null) {
            final b a = this.a;
            if (a != null) {
                a.a(s, o);
            }
            return;
        }
        throw new IllegalArgumentException("No key specified");
    }
    
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append("AppLovinAdView{zoneId='");
        sb.append(this.getZoneId());
        sb.append("\", size=");
        sb.append(this.getSize());
        sb.append('}');
        return sb.toString();
    }
}

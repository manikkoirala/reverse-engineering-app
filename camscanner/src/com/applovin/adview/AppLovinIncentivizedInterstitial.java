// 
// Decompiled by Procyon v0.6.0
// 

package com.applovin.adview;

import androidx.lifecycle.Lifecycle;
import android.view.ViewGroup;
import com.applovin.sdk.AppLovinAd;
import com.applovin.sdk.AppLovinAdClickListener;
import com.applovin.sdk.AppLovinAdDisplayListener;
import com.applovin.sdk.AppLovinAdVideoPlaybackListener;
import com.applovin.sdk.AppLovinAdRewardListener;
import androidx.annotation.Nullable;
import androidx.annotation.NonNull;
import com.applovin.impl.sdk.y;
import com.applovin.sdk.AppLovinAdLoadListener;
import com.applovin.sdk.AppLovinSdk;
import android.content.Context;
import com.applovin.impl.sdk.b.a;

public class AppLovinIncentivizedInterstitial
{
    private final a a;
    
    public AppLovinIncentivizedInterstitial(final Context context) {
        this(AppLovinSdk.getInstance(context));
    }
    
    public AppLovinIncentivizedInterstitial(final AppLovinSdk appLovinSdk) {
        this(null, appLovinSdk);
    }
    
    public AppLovinIncentivizedInterstitial(final String s, final AppLovinSdk appLovinSdk) {
        if (appLovinSdk != null) {
            this.a = this.createIncentivizedAdController(s, appLovinSdk);
            return;
        }
        throw new IllegalArgumentException("No sdk specified");
    }
    
    public static AppLovinIncentivizedInterstitial create(final Context context) {
        return create(AppLovinSdk.getInstance(context));
    }
    
    public static AppLovinIncentivizedInterstitial create(final AppLovinSdk appLovinSdk) {
        return create(null, appLovinSdk);
    }
    
    public static AppLovinIncentivizedInterstitial create(final String s, final AppLovinSdk appLovinSdk) {
        return new AppLovinIncentivizedInterstitial(s, appLovinSdk);
    }
    
    protected a createIncentivizedAdController(final String s, final AppLovinSdk appLovinSdk) {
        return new a(s, appLovinSdk);
    }
    
    public String getZoneId() {
        return this.a.b();
    }
    
    public boolean isAdReadyToDisplay() {
        return this.a.a();
    }
    
    public void preload(final AppLovinAdLoadListener appLovinAdLoadListener) {
        if (appLovinAdLoadListener == null) {
            y.g("AppLovinIncentivizedInterstitial", "AppLovinAdLoadListener was null when preloading incentivized interstitials; using a listener is highly recommended.");
        }
        this.a.a(appLovinAdLoadListener);
    }
    
    public void setExtraInfo(@NonNull final String s, @Nullable final Object o) {
        if (s != null) {
            this.a.a(s, o);
            return;
        }
        throw new IllegalArgumentException("No key specified");
    }
    
    public void show(final Context context) {
        this.show(context, null, null);
    }
    
    public void show(final Context context, final AppLovinAdRewardListener appLovinAdRewardListener) {
        this.show(context, appLovinAdRewardListener, null);
    }
    
    public void show(final Context context, final AppLovinAdRewardListener appLovinAdRewardListener, final AppLovinAdVideoPlaybackListener appLovinAdVideoPlaybackListener) {
        this.show(context, appLovinAdRewardListener, appLovinAdVideoPlaybackListener, null);
    }
    
    public void show(final Context context, final AppLovinAdRewardListener appLovinAdRewardListener, final AppLovinAdVideoPlaybackListener appLovinAdVideoPlaybackListener, final AppLovinAdDisplayListener appLovinAdDisplayListener) {
        this.show(context, appLovinAdRewardListener, appLovinAdVideoPlaybackListener, appLovinAdDisplayListener, null);
    }
    
    public void show(final Context context, final AppLovinAdRewardListener appLovinAdRewardListener, final AppLovinAdVideoPlaybackListener appLovinAdVideoPlaybackListener, final AppLovinAdDisplayListener appLovinAdDisplayListener, final AppLovinAdClickListener appLovinAdClickListener) {
        this.show(null, context, appLovinAdRewardListener, appLovinAdVideoPlaybackListener, appLovinAdDisplayListener, appLovinAdClickListener);
    }
    
    public void show(final AppLovinAd appLovinAd, final Context context, final AppLovinAdRewardListener appLovinAdRewardListener, final AppLovinAdVideoPlaybackListener appLovinAdVideoPlaybackListener, final AppLovinAdDisplayListener appLovinAdDisplayListener, final AppLovinAdClickListener appLovinAdClickListener) {
        this.a.a(appLovinAd, context, (String)null, appLovinAdRewardListener, appLovinAdVideoPlaybackListener, appLovinAdDisplayListener, appLovinAdClickListener);
    }
    
    public void show(final AppLovinAd appLovinAd, final ViewGroup viewGroup, final Lifecycle lifecycle, final Context context, final AppLovinAdRewardListener appLovinAdRewardListener, final AppLovinAdVideoPlaybackListener appLovinAdVideoPlaybackListener, final AppLovinAdDisplayListener appLovinAdDisplayListener, final AppLovinAdClickListener appLovinAdClickListener) {
        this.a.a(appLovinAd, viewGroup, lifecycle, context, appLovinAdRewardListener, appLovinAdVideoPlaybackListener, appLovinAdDisplayListener, appLovinAdClickListener);
    }
    
    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append("AppLovinIncentivizedInterstitial{zoneId='");
        sb.append(this.getZoneId());
        sb.append("', isAdReadyToDisplay=");
        sb.append(this.isAdReadyToDisplay());
        sb.append('}');
        return sb.toString();
    }
}

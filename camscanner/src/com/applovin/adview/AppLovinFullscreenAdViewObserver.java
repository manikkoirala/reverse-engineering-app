// 
// Decompiled by Procyon v0.6.0
// 

package com.applovin.adview;

import androidx.lifecycle.OnLifecycleEvent;
import androidx.lifecycle.Lifecycle;
import com.applovin.impl.adview.q;
import com.applovin.impl.adview.activity.b.a;
import java.util.concurrent.atomic.AtomicBoolean;
import com.applovin.impl.sdk.o;
import androidx.lifecycle.LifecycleObserver;

public class AppLovinFullscreenAdViewObserver implements LifecycleObserver
{
    private final o a;
    private final AtomicBoolean b;
    private a c;
    private q d;
    
    public AppLovinFullscreenAdViewObserver(final Lifecycle lifecycle, final q d, final o a) {
        this.b = new AtomicBoolean(true);
        this.d = d;
        this.a = a;
        lifecycle.addObserver(this);
    }
    
    @OnLifecycleEvent(Lifecycle.Event.ON_DESTROY)
    public void onDestroy() {
        final q d = this.d;
        if (d != null) {
            d.a();
            this.d = null;
        }
        final a c = this.c;
        if (c != null) {
            c.h();
            this.c.k();
            this.c = null;
        }
    }
    
    @OnLifecycleEvent(Lifecycle.Event.ON_PAUSE)
    public void onPause() {
        final a c = this.c;
        if (c != null) {
            c.g();
            this.c.e();
        }
    }
    
    @OnLifecycleEvent(Lifecycle.Event.ON_RESUME)
    public void onResume() {
        if (this.b.getAndSet(false)) {
            return;
        }
        final a c = this.c;
        if (c != null) {
            c.f();
            this.c.a(0L);
        }
    }
    
    @OnLifecycleEvent(Lifecycle.Event.ON_STOP)
    public void onStop() {
        final a c = this.c;
        if (c != null) {
            c.j();
        }
    }
    
    public void setPresenter(final a c) {
        this.c = c;
    }
}

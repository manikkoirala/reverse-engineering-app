// 
// Decompiled by Procyon v0.6.0
// 

package com.applovin.exoplayer2;

import android.os.Build$VERSION;
import java.util.HashSet;

public final class t
{
    @Deprecated
    public static final String a;
    private static final HashSet<String> b;
    private static String c;
    
    static {
        final StringBuilder sb = new StringBuilder();
        sb.append("ExoPlayerLib/2.15.1 (Linux; Android ");
        sb.append(Build$VERSION.RELEASE);
        sb.append(") ");
        sb.append("ExoPlayerLib/2.15.1");
        a = sb.toString();
        b = new HashSet<String>();
        t.c = "goog.exo.core";
    }
    
    public static String a() {
        synchronized (t.class) {
            return t.c;
        }
    }
    
    public static void a(final String s) {
        synchronized (t.class) {
            if (t.b.add(s)) {
                final StringBuilder sb = new StringBuilder();
                sb.append(t.c);
                sb.append(", ");
                sb.append(s);
                t.c = sb.toString();
            }
        }
    }
}

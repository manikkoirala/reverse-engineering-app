// 
// Decompiled by Procyon v0.6.0
// 

package com.applovin.exoplayer2;

import android.os.BaseBundle;
import com.applovin.exoplayer2.common.base.Objects;
import androidx.annotation.Nullable;
import com.applovin.exoplayer2.l.a;
import android.os.Bundle;

public final class az extends aq
{
    public static final a<az> a;
    private final boolean c;
    private final boolean d;
    
    static {
        a = new \u3007O888o0o();
    }
    
    public az() {
        this.c = false;
        this.d = false;
    }
    
    public az(final boolean d) {
        this.c = true;
        this.d = d;
    }
    
    private static az a(final Bundle bundle) {
        com.applovin.exoplayer2.l.a.a(((BaseBundle)bundle).getInt(a(0), -1) == 3);
        az az;
        if (bundle.getBoolean(a(1), false)) {
            az = new az(bundle.getBoolean(a(2), false));
        }
        else {
            az = new az();
        }
        return az;
    }
    
    private static String a(final int i) {
        return Integer.toString(i, 36);
    }
    
    @Override
    public boolean equals(@Nullable final Object o) {
        final boolean b = o instanceof az;
        final boolean b2 = false;
        if (!b) {
            return false;
        }
        final az az = (az)o;
        boolean b3 = b2;
        if (this.d == az.d) {
            b3 = b2;
            if (this.c == az.c) {
                b3 = true;
            }
        }
        return b3;
    }
    
    @Override
    public int hashCode() {
        return Objects.hashCode(this.c, this.d);
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.applovin.exoplayer2;

import android.os.BaseBundle;
import android.os.Bundle;

public abstract class aq implements g
{
    public static final a<aq> b;
    
    static {
        b = new Oooo8o0\u3007();
    }
    
    aq() {
    }
    
    private static aq a(final Bundle bundle) {
        final int int1 = ((BaseBundle)bundle).getInt(a(0), -1);
        if (int1 == 0) {
            return x.a.fromBundle(bundle);
        }
        if (int1 == 1) {
            return aj.a.fromBundle(bundle);
        }
        if (int1 == 2) {
            return ax.a.fromBundle(bundle);
        }
        if (int1 == 3) {
            return az.a.fromBundle(bundle);
        }
        final StringBuilder sb = new StringBuilder();
        sb.append("Encountered unknown rating type: ");
        sb.append(int1);
        throw new IllegalArgumentException(sb.toString());
    }
    
    private static String a(final int i) {
        return Integer.toString(i, 36);
    }
}

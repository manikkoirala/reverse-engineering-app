// 
// Decompiled by Procyon v0.6.0
// 

package com.applovin.exoplayer2;

import com.applovin.exoplayer2.l.ai;
import androidx.annotation.Nullable;
import androidx.annotation.CheckResult;
import android.os.Bundle;
import com.applovin.exoplayer2.l.a;
import androidx.annotation.FloatRange;

public final class am implements g
{
    public static final am a;
    public static final a<am> d;
    public final float b;
    public final float c;
    private final int e;
    
    static {
        a = new am(1.0f);
        d = new \u300780\u3007808\u3007O();
    }
    
    public am(final float n) {
        this(n, 1.0f);
    }
    
    public am(@FloatRange(from = 0.0, fromInclusive = false) final float b, @FloatRange(from = 0.0, fromInclusive = false) final float c) {
        final boolean b2 = true;
        com.applovin.exoplayer2.l.a.a(b > 0.0f);
        com.applovin.exoplayer2.l.a.a(c > 0.0f && b2);
        this.b = b;
        this.c = c;
        this.e = Math.round(b * 1000.0f);
    }
    
    private static String a(final int i) {
        return Integer.toString(i, 36);
    }
    
    public long a(final long n) {
        return n * this.e;
    }
    
    @CheckResult
    public am a(@FloatRange(from = 0.0, fromInclusive = false) final float n) {
        return new am(n, this.c);
    }
    
    @Override
    public boolean equals(@Nullable final Object o) {
        boolean b = true;
        if (this == o) {
            return true;
        }
        if (o != null && am.class == o.getClass()) {
            final am am = (am)o;
            if (this.b != am.b || this.c != am.c) {
                b = false;
            }
            return b;
        }
        return false;
    }
    
    @Override
    public int hashCode() {
        return (527 + Float.floatToRawIntBits(this.b)) * 31 + Float.floatToRawIntBits(this.c);
    }
    
    @Override
    public String toString() {
        return ai.a("PlaybackParameters(speed=%.2f, pitch=%.2f)", new Object[] { this.b, this.c });
    }
}

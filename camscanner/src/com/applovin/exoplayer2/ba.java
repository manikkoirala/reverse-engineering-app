// 
// Decompiled by Procyon v0.6.0
// 

package com.applovin.exoplayer2;

import android.os.BaseBundle;
import android.net.Uri;
import com.applovin.exoplayer2.l.ai;
import com.applovin.exoplayer2.l.a;
import android.util.Pair;
import androidx.annotation.Nullable;
import android.os.IBinder;
import com.applovin.exoplayer2.common.a.s;
import com.applovin.exoplayer2.l.b;
import android.os.Bundle;

public abstract class ba implements g
{
    public static final ba a;
    public static final g.a<ba> b;
    
    static {
        a = new ba() {
            @Override
            public a a(final int n, final a a, final boolean b) {
                throw new IndexOutOfBoundsException();
            }
            
            @Override
            public c a(final int n, final c c, final long n2) {
                throw new IndexOutOfBoundsException();
            }
            
            @Override
            public Object a(final int n) {
                throw new IndexOutOfBoundsException();
            }
            
            @Override
            public int b() {
                return 0;
            }
            
            @Override
            public int c() {
                return 0;
            }
            
            @Override
            public int c(final Object o) {
                return -1;
            }
        };
        b = new oo88o8O();
    }
    
    protected ba() {
    }
    
    private static ba a(final Bundle bundle) {
        final s<c> a = a(c.s, com.applovin.exoplayer2.l.b.a(bundle, b(0)));
        final s<a> a2 = a(ba.a.g, com.applovin.exoplayer2.l.b.a(bundle, b(1)));
        int[] array;
        if ((array = ((BaseBundle)bundle).getIntArray(b(2))) == null) {
            array = c(a.size());
        }
        return new b(a, a2, array);
    }
    
    private static <T extends g> s<T> a(final g.a<T> a, @Nullable final IBinder binder) {
        if (binder == null) {
            return s.g();
        }
        final s.a<T> a2 = new s.a<T>();
        final s<Bundle> a3 = f.a(binder);
        for (int i = 0; i < a3.size(); ++i) {
            a2.b(a.fromBundle((Bundle)a3.get(i)));
        }
        return a2.a();
    }
    
    private static String b(final int i) {
        return Integer.toString(i, 36);
    }
    
    private static int[] c(final int n) {
        final int[] array = new int[n];
        for (int i = 0; i < n; ++i) {
            array[i] = i;
        }
        return array;
    }
    
    public int a(int b, final int n, final boolean b2) {
        if (n == 0) {
            if (b == this.a(b2)) {
                b = -1;
            }
            else {
                ++b;
            }
            return b;
        }
        if (n == 1) {
            return b;
        }
        if (n == 2) {
            if (b == this.a(b2)) {
                b = this.b(b2);
            }
            else {
                ++b;
            }
            return b;
        }
        throw new IllegalStateException();
    }
    
    public final int a(int a, final a a2, final c c, final int n, final boolean b) {
        final int c2 = this.a(a, a2).c;
        if (this.a(c2, c).q != a) {
            return a + 1;
        }
        a = this.a(c2, n, b);
        if (a == -1) {
            return -1;
        }
        return this.a(a, c).p;
    }
    
    public int a(final boolean b) {
        int n;
        if (this.d()) {
            n = -1;
        }
        else {
            n = this.b() - 1;
        }
        return n;
    }
    
    public final Pair<Object, Long> a(final c c, final a a, final int n, final long n2) {
        return (Pair<Object, Long>)a.b((Object)this.a(c, a, n, n2, 0L));
    }
    
    @Nullable
    public final Pair<Object, Long> a(final c c, final a a, int p5, long n, long b) {
        a.a(p5, 0, this.b());
        this.a(p5, c, b);
        b = n;
        if (n == -9223372036854775807L) {
            n = (b = c.b());
            if (n == -9223372036854775807L) {
                return null;
            }
        }
        p5 = c.p;
        this.a(p5, a);
        while (p5 < c.q && a.e != b) {
            final int n2 = p5 + 1;
            if (this.a(n2, a).e > b) {
                break;
            }
            p5 = n2;
        }
        this.a(p5, a, true);
        b -= a.e;
        final long d = a.d;
        n = b;
        if (d != -9223372036854775807L) {
            n = Math.min(b, d - 1L);
        }
        n = Math.max(0L, n);
        return (Pair<Object, Long>)Pair.create(a.b(a.b), (Object)n);
    }
    
    public final a a(final int n, final a a) {
        return this.a(n, a, false);
    }
    
    public abstract a a(final int p0, final a p1, final boolean p2);
    
    public a a(final Object o, final a a) {
        return this.a(this.c(o), a, true);
    }
    
    public final c a(final int n, final c c) {
        return this.a(n, c, 0L);
    }
    
    public abstract c a(final int p0, final c p1, final long p2);
    
    public abstract Object a(final int p0);
    
    public abstract int b();
    
    public int b(int a, final int n, final boolean b) {
        if (n == 0) {
            if (a == this.b(b)) {
                a = -1;
            }
            else {
                --a;
            }
            return a;
        }
        if (n == 1) {
            return a;
        }
        if (n == 2) {
            if (a == this.b(b)) {
                a = this.a(b);
            }
            else {
                --a;
            }
            return a;
        }
        throw new IllegalStateException();
    }
    
    public int b(final boolean b) {
        int n;
        if (this.d()) {
            n = -1;
        }
        else {
            n = 0;
        }
        return n;
    }
    
    public final boolean b(final int n, final a a, final c c, final int n2, final boolean b) {
        return this.a(n, a, c, n2, b) == -1;
    }
    
    public abstract int c();
    
    public abstract int c(final Object p0);
    
    public final boolean d() {
        return this.b() == 0;
    }
    
    @Override
    public boolean equals(@Nullable final Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof ba)) {
            return false;
        }
        final ba ba = (ba)o;
        if (ba.b() == this.b() && ba.c() == this.c()) {
            final c c = new c();
            final a a = new a();
            final c c2 = new c();
            final a a2 = new a();
            for (int i = 0; i < this.b(); ++i) {
                if (!this.a(i, c).equals(ba.a(i, c2))) {
                    return false;
                }
            }
            for (int j = 0; j < this.c(); ++j) {
                if (!this.a(j, a, true).equals(ba.a(j, a2, true))) {
                    return false;
                }
            }
            return true;
        }
        return false;
    }
    
    @Override
    public int hashCode() {
        final c c = new c();
        final a a = new a();
        int n = 217 + this.b();
        final int n2 = 0;
        for (int i = 0; i < this.b(); ++i) {
            n = n * 31 + this.a(i, c).hashCode();
        }
        int n3 = n * 31 + this.c();
        for (int j = n2; j < this.c(); ++j) {
            n3 = n3 * 31 + this.a(j, a, true).hashCode();
        }
        return n3;
    }
    
    public static final class a implements g
    {
        public static final g.a<a> g;
        @Nullable
        public Object a;
        @Nullable
        public Object b;
        public int c;
        public long d;
        public long e;
        public boolean f;
        private com.applovin.exoplayer2.h.a.a h;
        
        static {
            g = new \u3007oo\u3007();
        }
        
        public a() {
            this.h = com.applovin.exoplayer2.h.a.a.a;
        }
        
        private static a a(Bundle bundle) {
            final int int1 = ((BaseBundle)bundle).getInt(g(0), 0);
            final long long1 = ((BaseBundle)bundle).getLong(g(1), -9223372036854775807L);
            final long long2 = ((BaseBundle)bundle).getLong(g(2), 0L);
            final boolean boolean1 = bundle.getBoolean(g(3));
            bundle = bundle.getBundle(g(4));
            com.applovin.exoplayer2.h.a.a a;
            if (bundle != null) {
                a = com.applovin.exoplayer2.h.a.a.g.fromBundle(bundle);
            }
            else {
                a = com.applovin.exoplayer2.h.a.a.a;
            }
            final a a2 = new a();
            a2.a(null, null, int1, long1, long2, a, boolean1);
            return a2;
        }
        
        private static String g(final int i) {
            return Integer.toString(i, 36);
        }
        
        public int a(final int n, final int n2) {
            return this.h.a(n).a(n2);
        }
        
        public int a(final long n) {
            return this.h.a(n, this.d);
        }
        
        public long a() {
            return this.d;
        }
        
        public long a(final int n) {
            return this.h.a(n).a;
        }
        
        public a a(@Nullable final Object o, @Nullable final Object o2, final int n, final long n2, final long n3) {
            return this.a(o, o2, n, n2, n3, com.applovin.exoplayer2.h.a.a.a, false);
        }
        
        public a a(@Nullable final Object a, @Nullable final Object b, final int c, final long d, final long e, final com.applovin.exoplayer2.h.a.a h, final boolean f) {
            this.a = a;
            this.b = b;
            this.c = c;
            this.d = d;
            this.e = e;
            this.h = h;
            this.f = f;
            return this;
        }
        
        public int b(final int n) {
            return this.h.a(n).a();
        }
        
        public int b(final long n) {
            return this.h.b(n, this.d);
        }
        
        public long b() {
            return com.applovin.exoplayer2.h.a(this.e);
        }
        
        public long b(final int n, final int n2) {
            final com.applovin.exoplayer2.h.a.a.a a = this.h.a(n);
            long n3;
            if (a.b != -1) {
                n3 = a.e[n2];
            }
            else {
                n3 = -9223372036854775807L;
            }
            return n3;
        }
        
        public long c() {
            return this.e;
        }
        
        public boolean c(final int n) {
            return this.h.a(n).c() ^ true;
        }
        
        public int d() {
            return this.h.c;
        }
        
        public int d(final int n) {
            return this.h.a(n).b;
        }
        
        public int e() {
            return this.h.f;
        }
        
        public boolean e(final int n) {
            return this.h.a(n).g;
        }
        
        @Override
        public boolean equals(@Nullable final Object o) {
            boolean b = true;
            if (this == o) {
                return true;
            }
            if (o != null && a.class.equals(o.getClass())) {
                final a a = (a)o;
                if (!ai.a(this.a, a.a) || !ai.a(this.b, a.b) || this.c != a.c || this.d != a.d || this.e != a.e || this.f != a.f || !ai.a((Object)this.h, (Object)a.h)) {
                    b = false;
                }
                return b;
            }
            return false;
        }
        
        public long f() {
            return this.h.d;
        }
        
        public long f(final int n) {
            return this.h.a(n).f;
        }
        
        @Override
        public int hashCode() {
            final Object a = this.a;
            int hashCode = 0;
            int hashCode2;
            if (a == null) {
                hashCode2 = 0;
            }
            else {
                hashCode2 = a.hashCode();
            }
            final Object b = this.b;
            if (b != null) {
                hashCode = b.hashCode();
            }
            final int c = this.c;
            final long d = this.d;
            final int n = (int)(d ^ d >>> 32);
            final long e = this.e;
            return ((((((217 + hashCode2) * 31 + hashCode) * 31 + c) * 31 + n) * 31 + (int)(e ^ e >>> 32)) * 31 + (this.f ? 1 : 0)) * 31 + this.h.hashCode();
        }
    }
    
    public static final class b extends ba
    {
        private final s<c> c;
        private final s<a> d;
        private final int[] e;
        private final int[] f;
        
        public b(final s<c> c, final s<a> d, final int[] e) {
            final int size = c.size();
            final int length = e.length;
            int i = 0;
            com.applovin.exoplayer2.l.a.a(size == length);
            this.c = c;
            this.d = d;
            this.e = e;
            this.f = new int[e.length];
            while (i < e.length) {
                this.f[e[i]] = i;
                ++i;
            }
        }
        
        @Override
        public int a(int b, final int n, final boolean b2) {
            if (n == 1) {
                return b;
            }
            if (b == this.a(b2)) {
                if (n == 2) {
                    b = this.b(b2);
                }
                else {
                    b = -1;
                }
                return b;
            }
            if (b2) {
                b = this.e[this.f[b] + 1];
            }
            else {
                ++b;
            }
            return b;
        }
        
        @Override
        public int a(final boolean b) {
            if (this.d()) {
                return -1;
            }
            int n;
            if (b) {
                n = this.e[this.b() - 1];
            }
            else {
                n = this.b() - 1;
            }
            return n;
        }
        
        @Override
        public a a(final int n, final a a, final boolean b) {
            final a a2 = this.d.get(n);
            a.a(a2.a, a2.b, a2.c, a2.d, a2.e, a2.h, a2.f);
            return a;
        }
        
        @Override
        public c a(final int n, final c c, final long n2) {
            final c c2 = this.c.get(n);
            c.a(c2.b, c2.d, c2.e, c2.f, c2.g, c2.h, c2.i, c2.j, c2.l, c2.n, c2.o, c2.p, c2.q, c2.r);
            c.m = c2.m;
            return c;
        }
        
        @Override
        public Object a(final int n) {
            throw new UnsupportedOperationException();
        }
        
        @Override
        public int b() {
            return this.c.size();
        }
        
        @Override
        public int b(int a, final int n, final boolean b) {
            if (n == 1) {
                return a;
            }
            if (a == this.b(b)) {
                if (n == 2) {
                    a = this.a(b);
                }
                else {
                    a = -1;
                }
                return a;
            }
            if (b) {
                a = this.e[this.f[a] - 1];
            }
            else {
                --a;
            }
            return a;
        }
        
        @Override
        public int b(final boolean b) {
            if (this.d()) {
                return -1;
            }
            int n = 0;
            if (b) {
                n = this.e[0];
            }
            return n;
        }
        
        @Override
        public int c() {
            return this.d.size();
        }
        
        @Override
        public int c(final Object o) {
            throw new UnsupportedOperationException();
        }
    }
    
    public static final class c implements g
    {
        public static final Object a;
        public static final g.a<c> s;
        private static final Object t;
        private static final ab u;
        public Object b;
        @Deprecated
        @Nullable
        public Object c;
        public ab d;
        @Nullable
        public Object e;
        public long f;
        public long g;
        public long h;
        public boolean i;
        public boolean j;
        @Deprecated
        public boolean k;
        @Nullable
        public ab.e l;
        public boolean m;
        public long n;
        public long o;
        public int p;
        public int q;
        public long r;
        
        static {
            a = new Object();
            t = new Object();
            u = new ab.b().a("com.applovin.exoplayer2.Timeline").a(Uri.EMPTY).a();
            s = new o\u3007O8\u3007\u3007o();
        }
        
        public c() {
            this.b = c.a;
            this.d = c.u;
        }
        
        private static c a(final Bundle bundle) {
            final Bundle bundle2 = bundle.getBundle(a(1));
            ab.e e = null;
            ab ab;
            if (bundle2 != null) {
                ab = com.applovin.exoplayer2.ab.g.fromBundle(bundle2);
            }
            else {
                ab = null;
            }
            final long long1 = ((BaseBundle)bundle).getLong(a(2), -9223372036854775807L);
            final long long2 = ((BaseBundle)bundle).getLong(a(3), -9223372036854775807L);
            final long long3 = ((BaseBundle)bundle).getLong(a(4), -9223372036854775807L);
            final boolean boolean1 = bundle.getBoolean(a(5), false);
            final boolean boolean2 = bundle.getBoolean(a(6), false);
            final Bundle bundle3 = bundle.getBundle(a(7));
            if (bundle3 != null) {
                e = (ab.e)com.applovin.exoplayer2.ab.e.g.fromBundle(bundle3);
            }
            final boolean boolean3 = bundle.getBoolean(a(8), false);
            final long long4 = ((BaseBundle)bundle).getLong(a(9), 0L);
            final long long5 = ((BaseBundle)bundle).getLong(a(10), -9223372036854775807L);
            final int int1 = ((BaseBundle)bundle).getInt(a(11), 0);
            final int int2 = ((BaseBundle)bundle).getInt(a(12), 0);
            final long long6 = ((BaseBundle)bundle).getLong(a(13), 0L);
            final c c = new c();
            c.a(ba.c.t, ab, null, long1, long2, long3, boolean1, boolean2, e, long4, long5, int1, int2, long6);
            c.m = boolean3;
            return c;
        }
        
        private static String a(final int i) {
            return Integer.toString(i, 36);
        }
        
        public long a() {
            return com.applovin.exoplayer2.h.a(this.n);
        }
        
        public c a(Object h, @Nullable final ab ab, @Nullable final Object e, final long f, final long g, final long h2, final boolean i, final boolean j, @Nullable final ab.e l, final long n, final long o, final int p14, final int q, final long r) {
            this.b = h;
            ab u;
            if (ab != null) {
                u = ab;
            }
            else {
                u = ba.c.u;
            }
            this.d = u;
            Label_0046: {
                if (ab != null) {
                    final ab.f c = ab.c;
                    if (c != null) {
                        h = c.h;
                        break Label_0046;
                    }
                }
                h = null;
            }
            this.c = h;
            this.e = e;
            this.f = f;
            this.g = g;
            this.h = h2;
            this.i = i;
            this.j = j;
            this.k = (l != null);
            this.l = l;
            this.n = n;
            this.o = o;
            this.p = p14;
            this.q = q;
            this.r = r;
            this.m = false;
            return this;
        }
        
        public long b() {
            return this.n;
        }
        
        public long c() {
            return com.applovin.exoplayer2.h.a(this.o);
        }
        
        public long d() {
            return ai.c(this.h);
        }
        
        public boolean e() {
            final boolean k = this.k;
            final ab.e l = this.l;
            final boolean b = true;
            com.applovin.exoplayer2.l.a.b(k == (l != null));
            return this.l != null && b;
        }
        
        @Override
        public boolean equals(@Nullable final Object o) {
            boolean b = true;
            if (this == o) {
                return true;
            }
            if (o != null && c.class.equals(o.getClass())) {
                final c c = (c)o;
                if (!ai.a(this.b, c.b) || !ai.a((Object)this.d, (Object)c.d) || !ai.a(this.e, c.e) || !ai.a((Object)this.l, (Object)c.l) || this.f != c.f || this.g != c.g || this.h != c.h || this.i != c.i || this.j != c.j || this.m != c.m || this.n != c.n || this.o != c.o || this.p != c.p || this.q != c.q || this.r != c.r) {
                    b = false;
                }
                return b;
            }
            return false;
        }
        
        @Override
        public int hashCode() {
            final int hashCode = this.b.hashCode();
            final int hashCode2 = this.d.hashCode();
            final Object e = this.e;
            int hashCode3 = 0;
            int hashCode4;
            if (e == null) {
                hashCode4 = 0;
            }
            else {
                hashCode4 = e.hashCode();
            }
            final ab.e l = this.l;
            if (l != null) {
                hashCode3 = l.hashCode();
            }
            final long f = this.f;
            final int n = (int)(f ^ f >>> 32);
            final long g = this.g;
            final int n2 = (int)(g ^ g >>> 32);
            final long h = this.h;
            final int n3 = (int)(h ^ h >>> 32);
            final int i = this.i ? 1 : 0;
            final int j = this.j ? 1 : 0;
            final int m = this.m ? 1 : 0;
            final long n4 = this.n;
            final int n5 = (int)(n4 ^ n4 >>> 32);
            final long o = this.o;
            final int n6 = (int)(o ^ o >>> 32);
            final int p = this.p;
            final int q = this.q;
            final long r = this.r;
            return ((((((((((((((217 + hashCode) * 31 + hashCode2) * 31 + hashCode4) * 31 + hashCode3) * 31 + n) * 31 + n2) * 31 + n3) * 31 + i) * 31 + j) * 31 + m) * 31 + n5) * 31 + n6) * 31 + p) * 31 + q) * 31 + (int)(r ^ r >>> 32);
        }
    }
}

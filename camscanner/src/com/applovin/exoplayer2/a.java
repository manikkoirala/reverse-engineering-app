// 
// Decompiled by Procyon v0.6.0
// 

package com.applovin.exoplayer2;

import android.util.Pair;
import com.applovin.exoplayer2.h.z;

public abstract class a extends ba
{
    private final int c;
    private final z d;
    private final boolean e;
    
    public a(final boolean e, final z d) {
        this.e = e;
        this.d = d;
        this.c = d.a();
    }
    
    private int a(int a, final boolean b) {
        if (b) {
            a = this.d.a(a);
        }
        else if (a < this.c - 1) {
            ++a;
        }
        else {
            a = -1;
        }
        return a;
    }
    
    public static Object a(final Object o) {
        return ((Pair)o).first;
    }
    
    public static Object a(final Object o, final Object o2) {
        return Pair.create(o, o2);
    }
    
    private int b(int b, final boolean b2) {
        if (b2) {
            b = this.d.b(b);
        }
        else if (b > 0) {
            --b;
        }
        else {
            b = -1;
        }
        return b;
    }
    
    public static Object b(final Object o) {
        return ((Pair)o).second;
    }
    
    @Override
    public int a(int n, int n2, boolean b) {
        final boolean e = this.e;
        final int n3 = 0;
        int n4 = n2;
        if (e) {
            if ((n4 = n2) == 1) {
                n4 = 2;
            }
            b = false;
        }
        final int c = this.c(n);
        final int f = this.f(c);
        final ba d = this.d(c);
        if (n4 == 2) {
            n2 = n3;
        }
        else {
            n2 = n4;
        }
        n = d.a(n - f, n2, b);
        if (n != -1) {
            return f + n;
        }
        for (n = this.a(c, b); n != -1 && this.d(n).d(); n = this.a(n, b)) {}
        if (n != -1) {
            return this.f(n) + this.d(n).b(b);
        }
        if (n4 == 2) {
            return this.b(b);
        }
        return -1;
    }
    
    @Override
    public int a(boolean b) {
        int n = this.c;
        if (n == 0) {
            return -1;
        }
        if (this.e) {
            b = false;
        }
        if (b) {
            n = this.d.b();
        }
        else {
            --n;
        }
        while (this.d(n).d()) {
            if ((n = this.b(n, b)) == -1) {
                return -1;
            }
        }
        return this.f(n) + this.d(n).a(b);
    }
    
    @Override
    public final ba.a a(final int n, final ba.a a, final boolean b) {
        final int b2 = this.b(n);
        final int f = this.f(b2);
        this.d(b2).a(n - this.e(b2), a, b);
        a.c += f;
        if (b) {
            a.b = a(this.g(b2), a.b(a.b));
        }
        return a;
    }
    
    @Override
    public final ba.a a(final Object b, final ba.a a) {
        final Object a2 = a(b);
        final Object b2 = b(b);
        final int d = this.d(a2);
        final int f = this.f(d);
        this.d(d).a(b2, a);
        a.c += f;
        a.b = b;
        return a;
    }
    
    @Override
    public final c a(final int n, final c c, final long n2) {
        final int c2 = this.c(n);
        final int f = this.f(c2);
        final int e = this.e(c2);
        this.d(c2).a(n - f, c, n2);
        Object b = this.g(c2);
        if (!ba.c.a.equals(c.b)) {
            b = a(b, c.b);
        }
        c.b = b;
        c.p += e;
        c.q += e;
        return c;
    }
    
    @Override
    public final Object a(final int n) {
        final int b = this.b(n);
        return a(this.g(b), this.d(b).a(n - this.e(b)));
    }
    
    protected abstract int b(final int p0);
    
    @Override
    public int b(int n, int n2, boolean b) {
        final boolean e = this.e;
        final int n3 = 0;
        int n4 = n2;
        if (e) {
            if ((n4 = n2) == 1) {
                n4 = 2;
            }
            b = false;
        }
        final int c = this.c(n);
        final int f = this.f(c);
        final ba d = this.d(c);
        if (n4 == 2) {
            n2 = n3;
        }
        else {
            n2 = n4;
        }
        n = d.b(n - f, n2, b);
        if (n != -1) {
            return f + n;
        }
        for (n = this.b(c, b); n != -1 && this.d(n).d(); n = this.b(n, b)) {}
        if (n != -1) {
            return this.f(n) + this.d(n).a(b);
        }
        if (n4 == 2) {
            return this.a(b);
        }
        return -1;
    }
    
    @Override
    public int b(boolean b) {
        if (this.c == 0) {
            return -1;
        }
        final boolean e = this.e;
        int n = 0;
        if (e) {
            b = false;
        }
        if (b) {
            n = this.d.c();
        }
        while (this.d(n).d()) {
            if ((n = this.a(n, b)) == -1) {
                return -1;
            }
        }
        return this.f(n) + this.d(n).b(b);
    }
    
    protected abstract int c(final int p0);
    
    @Override
    public final int c(Object b) {
        final boolean b2 = b instanceof Pair;
        int n = -1;
        if (!b2) {
            return -1;
        }
        final Object a = a(b);
        b = b(b);
        final int d = this.d(a);
        if (d == -1) {
            return -1;
        }
        final int c = this.d(d).c(b);
        if (c != -1) {
            n = this.e(d) + c;
        }
        return n;
    }
    
    protected abstract int d(final Object p0);
    
    protected abstract ba d(final int p0);
    
    protected abstract int e(final int p0);
    
    protected abstract int f(final int p0);
    
    protected abstract Object g(final int p0);
}

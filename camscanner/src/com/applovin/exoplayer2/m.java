// 
// Decompiled by Procyon v0.6.0
// 

package com.applovin.exoplayer2;

import com.applovin.exoplayer2.l.a;
import com.applovin.exoplayer2.l.d;
import androidx.annotation.Nullable;
import com.applovin.exoplayer2.l.ac;
import com.applovin.exoplayer2.l.s;

final class m implements s
{
    private final ac a;
    private final a b;
    @Nullable
    private ar c;
    @Nullable
    private s d;
    private boolean e;
    private boolean f;
    
    public m(final a b, final d d) {
        this.b = b;
        this.a = new ac(d);
        this.e = true;
    }
    
    private void b(final boolean b) {
        if (this.c(b)) {
            this.e = true;
            if (this.f) {
                this.a.a();
            }
            return;
        }
        final s s = (s)com.applovin.exoplayer2.l.a.b((Object)this.d);
        final long c_ = s.c_();
        if (this.e) {
            if (c_ < this.a.c_()) {
                this.a.b();
                return;
            }
            this.e = false;
            if (this.f) {
                this.a.a();
            }
        }
        this.a.a(c_);
        final am d = s.d();
        if (!d.equals(this.a.d())) {
            this.a.a(d);
            this.b.a(d);
        }
    }
    
    private boolean c(final boolean b) {
        final ar c = this.c;
        if (c != null && !c.A()) {
            if (!this.c.z()) {
                if (b) {
                    return true;
                }
                if (this.c.g()) {
                    return true;
                }
            }
            return false;
        }
        return true;
    }
    
    public long a(final boolean b) {
        this.b(b);
        return this.c_();
    }
    
    public void a() {
        this.f = true;
        this.a.a();
    }
    
    public void a(final long n) {
        this.a.a(n);
    }
    
    public void a(final am am) {
        final s d = this.d;
        am d2 = am;
        if (d != null) {
            d.a(am);
            d2 = this.d.d();
        }
        this.a.a(d2);
    }
    
    public void a(final ar c) throws p {
        final s c2 = c.c();
        if (c2 != null) {
            final s d = this.d;
            if (c2 != d) {
                if (d != null) {
                    throw p.a(new IllegalStateException("Multiple renderer media clocks enabled."));
                }
                this.d = c2;
                this.c = c;
                c2.a(this.a.d());
            }
        }
    }
    
    public void b() {
        this.f = false;
        this.a.b();
    }
    
    public void b(final ar ar) {
        if (ar == this.c) {
            this.d = null;
            this.c = null;
            this.e = true;
        }
    }
    
    public long c_() {
        long n;
        if (this.e) {
            n = this.a.c_();
        }
        else {
            n = ((s)com.applovin.exoplayer2.l.a.b((Object)this.d)).c_();
        }
        return n;
    }
    
    public am d() {
        final s d = this.d;
        am am;
        if (d != null) {
            am = d.d();
        }
        else {
            am = this.a.d();
        }
        return am;
    }
    
    public interface a
    {
        void a(final am p0);
    }
}

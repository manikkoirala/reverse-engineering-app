// 
// Decompiled by Procyon v0.6.0
// 

package com.applovin.exoplayer2;

import androidx.annotation.RequiresApi;
import com.applovin.exoplayer2.l.ai;
import android.content.Context;
import java.util.UUID;

public final class h
{
    public static final UUID a;
    public static final UUID b;
    public static final UUID c;
    public static final UUID d;
    public static final UUID e;
    
    static {
        a = new UUID(0L, 0L);
        b = new UUID(1186680826959645954L, -5988876978535335093L);
        c = new UUID(-2129748144642739255L, 8654423357094679310L);
        d = new UUID(-1301668207276963122L, -6645017420763422227L);
        e = new UUID(-7348484286925749626L, -6083546864340672619L);
    }
    
    @Deprecated
    @RequiresApi(21)
    public static int a(final Context context) {
        return ai.a(context);
    }
    
    @Deprecated
    public static long a(final long n) {
        return ai.a(n);
    }
    
    @Deprecated
    public static String a(final int n) {
        return ai.i(n);
    }
    
    @Deprecated
    public static int b(final int n) {
        return ai.h(n);
    }
    
    @Deprecated
    public static long b(final long n) {
        return ai.b(n);
    }
}

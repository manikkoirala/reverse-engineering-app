// 
// Decompiled by Procyon v0.6.0
// 

package com.applovin.exoplayer2;

import android.os.BaseBundle;
import com.applovin.exoplayer2.common.base.Objects;
import androidx.annotation.Nullable;
import android.os.Bundle;
import androidx.annotation.FloatRange;
import com.applovin.exoplayer2.l.a;
import androidx.annotation.IntRange;

public final class ax extends aq
{
    public static final a<ax> a;
    @IntRange(from = 1L)
    private final int c;
    private final float d;
    
    static {
        a = new \u3007\u30078O0\u30078();
    }
    
    public ax(@IntRange(from = 1L) final int c) {
        com.applovin.exoplayer2.l.a.a(c > 0, (Object)"maxStars must be a positive integer");
        this.c = c;
        this.d = -1.0f;
    }
    
    public ax(@IntRange(from = 1L) final int c, @FloatRange(from = 0.0) final float d) {
        final boolean b = true;
        com.applovin.exoplayer2.l.a.a(c > 0, (Object)"maxStars must be a positive integer");
        com.applovin.exoplayer2.l.a.a(d >= 0.0f && d <= c && b, (Object)"starRating is out of range [0, maxStars]");
        this.c = c;
        this.d = d;
    }
    
    private static ax a(final Bundle bundle) {
        boolean b = false;
        if (((BaseBundle)bundle).getInt(a(0), -1) == 2) {
            b = true;
        }
        com.applovin.exoplayer2.l.a.a(b);
        final int int1 = ((BaseBundle)bundle).getInt(a(1), 5);
        final float float1 = bundle.getFloat(a(2), -1.0f);
        ax ax;
        if (float1 == -1.0f) {
            ax = new ax(int1);
        }
        else {
            ax = new ax(int1, float1);
        }
        return ax;
    }
    
    private static String a(final int i) {
        return Integer.toString(i, 36);
    }
    
    @Override
    public boolean equals(@Nullable final Object o) {
        final boolean b = o instanceof ax;
        final boolean b2 = false;
        if (!b) {
            return false;
        }
        final ax ax = (ax)o;
        boolean b3 = b2;
        if (this.c == ax.c) {
            b3 = b2;
            if (this.d == ax.d) {
                b3 = true;
            }
        }
        return b3;
    }
    
    @Override
    public int hashCode() {
        return Objects.hashCode(this.c, this.d);
    }
}

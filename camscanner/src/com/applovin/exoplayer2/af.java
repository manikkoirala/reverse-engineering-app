// 
// Decompiled by Procyon v0.6.0
// 

package com.applovin.exoplayer2;

import com.applovin.exoplayer2.h.n;
import com.applovin.exoplayer2.j.k;
import com.applovin.exoplayer2.k.b;
import com.applovin.exoplayer2.j.j;
import java.util.List;
import com.applovin.exoplayer2.common.a.s;
import com.applovin.exoplayer2.h.p;
import android.util.Pair;
import android.os.Handler;
import androidx.annotation.Nullable;
import com.applovin.exoplayer2.a.a;

final class af
{
    private final ba.a a;
    private final ba.c b;
    @Nullable
    private final a c;
    private final Handler d;
    private long e;
    private int f;
    private boolean g;
    @Nullable
    private ad h;
    @Nullable
    private ad i;
    @Nullable
    private ad j;
    private int k;
    @Nullable
    private Object l;
    private long m;
    
    public af(@Nullable final a c, final Handler d) {
        this.c = c;
        this.d = d;
        this.a = new ba.a();
        this.b = new ba.c();
    }
    
    private long a(final ba ba, final Object o) {
        final int c = ba.a(o, this.a).c;
        final Object l = this.l;
        if (l != null) {
            final int c2 = ba.c(l);
            if (c2 != -1 && ba.a(c2, this.a).c == c) {
                return this.m;
            }
        }
        for (ad ad = this.h; ad != null; ad = ad.g()) {
            if (ad.b.equals(o)) {
                return ad.f.a.d;
            }
        }
        for (ad ad2 = this.h; ad2 != null; ad2 = ad2.g()) {
            final int c3 = ba.c(ad2.b);
            if (c3 != -1 && ba.a(c3, this.a).c == c) {
                return ad2.f.a.d;
            }
        }
        final long e = this.e;
        this.e = 1L + e;
        if (this.h == null) {
            this.l = o;
            this.m = e;
        }
        return e;
    }
    
    private long a(final ba ba, final Object o, final int n) {
        ba.a(o, this.a);
        final long a = this.a.a(n);
        if (a == Long.MIN_VALUE) {
            return this.a.d;
        }
        return a + this.a.f(n);
    }
    
    @Nullable
    private ae a(final al al) {
        return this.a(al.a, al.b, al.c, al.s);
    }
    
    @Nullable
    private ae a(final ba ba, ad g, long b) {
        final ae f = g.f;
        final long n = g.a() + f.e - b;
        if (f.g) {
            final int c = ba.c(f.a.a);
            final ba.a a = this.a;
            final ba.c b2 = this.b;
            final int f2 = this.f;
            final boolean g2 = this.g;
            b = 0L;
            final int a2 = ba.a(c, a, b2, f2, g2);
            if (a2 == -1) {
                return null;
            }
            final int c2 = ba.a(a2, this.a, true).c;
            final Object b3 = this.a.b;
            long d = f.a.d;
            long longValue;
            Object o;
            if (ba.a(c2, this.b).p == a2) {
                final Pair<Object, Long> a3 = ba.a(this.b, this.a, c2, -9223372036854775807L, Math.max(0L, n));
                if (a3 == null) {
                    return null;
                }
                final Object first = a3.first;
                longValue = (long)a3.second;
                g = g.g();
                if (g != null && g.b.equals(first)) {
                    b = g.f.a.d;
                }
                else {
                    b = this.e;
                    this.e = 1L + b;
                }
                final long n2 = -9223372036854775807L;
                o = first;
                d = b;
                b = n2;
            }
            else {
                longValue = 0L;
                o = b3;
            }
            return this.a(ba, a(ba, o, longValue, d, this.a), b, longValue);
        }
        else {
            final p.a a4 = f.a;
            ba.a(a4.a, this.a);
            if (a4.a()) {
                final int b4 = a4.b;
                final int d2 = this.a.d(b4);
                if (d2 == -1) {
                    return null;
                }
                final int a5 = this.a.a(b4, a4.c);
                if (a5 < d2) {
                    return this.a(ba, a4.a, b4, a5, f.c, a4.d);
                }
                if ((b = f.c) == -9223372036854775807L) {
                    final ba.c b5 = this.b;
                    final ba.a a6 = this.a;
                    final Pair<Object, Long> a7 = ba.a(b5, a6, a6.c, -9223372036854775807L, Math.max(0L, n));
                    if (a7 == null) {
                        return null;
                    }
                    b = (long)a7.second;
                }
                return this.a(ba, a4.a, Math.max(this.a(ba, a4.a, a4.b), b), f.c, a4.d);
            }
            else {
                final int b6 = this.a.b(a4.e);
                if (b6 == this.a.d(a4.e)) {
                    b = this.a(ba, a4.a, a4.e);
                    return this.a(ba, a4.a, b, f.e, a4.d);
                }
                return this.a(ba, a4.a, a4.e, b6, f.e, a4.d);
            }
        }
    }
    
    @Nullable
    private ae a(final ba ba, final p.a a, final long n, final long n2) {
        ba.a(a.a, this.a);
        if (a.a()) {
            return this.a(ba, a.a, a.b, a.c, n, a.d);
        }
        return this.a(ba, a.a, n2, n, a.d);
    }
    
    private ae a(final ba ba, final Object o, final int n, final int n2, final long n3, long n4) {
        final p.a a = new p.a(o, n, n2, n4);
        final long b = ba.a(a.a, this.a).b(a.b, a.c);
        if (n2 == this.a.b(n)) {
            n4 = this.a.f();
        }
        else {
            n4 = 0L;
        }
        final boolean e = this.a.e(a.b);
        if (b != -9223372036854775807L && n4 >= b) {
            n4 = Math.max(0L, b - 1L);
        }
        return new ae(a, n4, n3, -9223372036854775807L, b, e, false, false, false);
    }
    
    private ae a(final ba ba, final Object o, long a, final long n, long d) {
        final long n2 = a;
        ba.a(o, this.a);
        final int b = this.a.b(n2);
        final p.a a2 = new p.a(o, d, b);
        final boolean a3 = this.a(a2);
        final boolean a4 = this.a(ba, a2);
        final boolean a5 = this.a(ba, a2, a3);
        final boolean b2 = b != -1 && this.a.e(b);
        if (b != -1) {
            a = this.a.a(b);
        }
        else {
            a = -9223372036854775807L;
        }
        if (a != -9223372036854775807L && a != Long.MIN_VALUE) {
            d = a;
        }
        else {
            d = this.a.d;
        }
        long max = n2;
        if (d != -9223372036854775807L) {
            max = n2;
            if (n2 >= d) {
                max = Math.max(0L, d - 1L);
            }
        }
        return new ae(a2, max, n, a, d, b2, a3, a4, a5);
    }
    
    private static p.a a(final ba ba, final Object o, final long n, final long n2, final ba.a a) {
        ba.a(o, a);
        final int a2 = a.a(n);
        if (a2 == -1) {
            return new p.a(o, n2, a.b(n));
        }
        return new p.a(o, a2, a.b(a2), n2);
    }
    
    private boolean a(final long n, final long n2) {
        return n == -9223372036854775807L || n == n2;
    }
    
    private boolean a(final ae ae, final ae ae2) {
        return ae.b == ae2.b && ae.a.equals(ae2.a);
    }
    
    private boolean a(final ba ba) {
        ad ad = this.h;
        if (ad == null) {
            return true;
        }
        int n = ba.c(ad.b);
        while (true) {
            n = ba.a(n, this.a, this.b, this.f, this.g);
            while (ad.g() != null && !ad.f.g) {
                ad = ad.g();
            }
            final ad g = ad.g();
            if (n == -1) {
                break;
            }
            if (g == null) {
                break;
            }
            if (ba.c(g.b) != n) {
                break;
            }
            ad = g;
        }
        final boolean a = this.a(ad);
        ad.f = this.a(ba, ad.f);
        return a ^ true;
    }
    
    private boolean a(final ba ba, final p.a a) {
        final boolean a2 = this.a(a);
        boolean b = false;
        if (!a2) {
            return false;
        }
        if (ba.a(ba.a(a.a, this.a).c, this.b).q == ba.c(a.a)) {
            b = true;
        }
        return b;
    }
    
    private boolean a(final ba ba, final p.a a, final boolean b) {
        final int c = ba.c(a.a);
        return !ba.a(ba.a(c, this.a).c, this.b).j && ba.b(c, this.a, this.b, this.f, this.g) && b;
    }
    
    private boolean a(final p.a a) {
        return !a.a() && a.e == -1;
    }
    
    private void h() {
        if (this.c != null) {
            final s.a<Object> i = s.i();
            for (ad ad = this.h; ad != null; ad = ad.g()) {
                i.b(ad.f.a);
            }
            final ad j = this.i;
            p.a a;
            if (j == null) {
                a = null;
            }
            else {
                a = j.f.a;
            }
            this.d.post((Runnable)new Oo08(this, (s.a)i, a));
        }
    }
    
    public ad a(final as[] array, final j j, final b b, final ah ah, final ae ae, final k k) {
        final ad i = this.j;
        long c = 0L;
        Label_0071: {
            if (i == null) {
                if (ae.a.a()) {
                    c = ae.c;
                    if (c != -9223372036854775807L) {
                        break Label_0071;
                    }
                }
                c = 0L;
            }
            else {
                c = i.a() + this.j.f.e - ae.b;
            }
        }
        final ad l = new ad(array, c, j, b, ah, ae, k);
        final ad m = this.j;
        if (m != null) {
            m.a(l);
        }
        else {
            this.h = l;
            this.i = l;
        }
        this.l = null;
        this.j = l;
        ++this.k;
        this.h();
        return l;
    }
    
    @Nullable
    public ae a(final long n, final al al) {
        final ad j = this.j;
        ae ae;
        if (j == null) {
            ae = this.a(al);
        }
        else {
            ae = this.a(al.a, j, n);
        }
        return ae;
    }
    
    public ae a(final ba ba, final ae ae) {
        final p.a a = ae.a;
        final boolean a2 = this.a(a);
        final boolean a3 = this.a(ba, a);
        final boolean a4 = this.a(ba, a, a2);
        ba.a(ae.a.a, this.a);
        long a5 = 0L;
        Label_0090: {
            if (!a.a()) {
                final int e = a.e;
                if (e != -1) {
                    a5 = this.a.a(e);
                    break Label_0090;
                }
            }
            a5 = -9223372036854775807L;
        }
        long n;
        if (a.a()) {
            n = this.a.b(a.b, a.c);
        }
        else if (a5 != -9223372036854775807L && a5 != Long.MIN_VALUE) {
            n = a5;
        }
        else {
            n = this.a.a();
        }
        boolean e2;
        if (a.a()) {
            e2 = this.a.e(a.b);
        }
        else {
            final int e3 = a.e;
            e2 = (e3 != -1 && this.a.e(e3));
        }
        return new ae(a, ae.b, ae.c, a5, n, e2, a2, a3, a4);
    }
    
    public p.a a(final ba ba, final Object o, final long n) {
        return a(ba, o, n, this.a(ba, o), this.a);
    }
    
    public void a(final long n) {
        final ad j = this.j;
        if (j != null) {
            j.d(n);
        }
    }
    
    public boolean a() {
        final ad j = this.j;
        return j == null || (!j.f.i && j.c() && this.j.f.e != -9223372036854775807L && this.k < 100);
    }
    
    public boolean a(ad g) {
        final boolean b = false;
        com.applovin.exoplayer2.l.a.b(g != null);
        if (g.equals(this.j)) {
            return false;
        }
        this.j = g;
        boolean b2 = b;
        while (g.g() != null) {
            g = g.g();
            if (g == this.i) {
                this.i = this.h;
                b2 = true;
            }
            g.f();
            --this.k;
        }
        this.j.a((ad)null);
        this.h();
        return b2;
    }
    
    public boolean a(final ba ba, final int f) {
        this.f = f;
        return this.a(ba);
    }
    
    public boolean a(final ba ba, long n, final long n2) {
        ad h = this.h;
        ad ad = null;
        while (true) {
            boolean b = true;
            if (h == null) {
                return true;
            }
            final ae f = h.f;
            ae a;
            if (ad == null) {
                a = this.a(ba, f);
            }
            else {
                final ae a2 = this.a(ba, ad, n);
                if (a2 == null) {
                    return this.a(ad) ^ true;
                }
                if (!this.a(f, a2)) {
                    return this.a(ad) ^ true;
                }
                a = a2;
            }
            h.f = a.b(f.c);
            if (!this.a(f.e, a.e)) {
                h.j();
                n = a.e;
                if (n == -9223372036854775807L) {
                    n = Long.MAX_VALUE;
                }
                else {
                    n = h.a(n);
                }
                final boolean b2 = h == this.i && !h.f.f && (n2 == Long.MIN_VALUE || n2 >= n);
                if (this.a(h) || b2) {
                    b = false;
                }
                return b;
            }
            final ad g = h.g();
            ad = h;
            h = g;
        }
    }
    
    public boolean a(final ba ba, final boolean g) {
        this.g = g;
        return this.a(ba);
    }
    
    public boolean a(final n n) {
        final ad j = this.j;
        return j != null && j.a == n;
    }
    
    @Nullable
    public ad b() {
        return this.j;
    }
    
    @Nullable
    public ad c() {
        return this.h;
    }
    
    @Nullable
    public ad d() {
        return this.i;
    }
    
    public ad e() {
        final ad i = this.i;
        com.applovin.exoplayer2.l.a.b(i != null && i.g() != null);
        this.i = this.i.g();
        this.h();
        return this.i;
    }
    
    @Nullable
    public ad f() {
        final ad h = this.h;
        if (h == null) {
            return null;
        }
        if (h == this.i) {
            this.i = h.g();
        }
        this.h.f();
        if (--this.k == 0) {
            this.j = null;
            final ad h2 = this.h;
            this.l = h2.b;
            this.m = h2.f.a.d;
        }
        this.h = this.h.g();
        this.h();
        return this.h;
    }
    
    public void g() {
        if (this.k == 0) {
            return;
        }
        ad g = (ad)com.applovin.exoplayer2.l.a.a((Object)this.h);
        this.l = g.b;
        this.m = g.f.a.d;
        while (g != null) {
            g.f();
            g = g.g();
        }
        this.h = null;
        this.j = null;
        this.i = null;
        this.k = 0;
        this.h();
    }
}

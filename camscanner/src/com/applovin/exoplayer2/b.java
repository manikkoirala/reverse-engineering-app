// 
// Decompiled by Procyon v0.6.0
// 

package com.applovin.exoplayer2;

import android.content.Intent;
import android.content.BroadcastReceiver;
import android.content.IntentFilter;
import android.os.Handler;
import android.content.Context;

final class b
{
    private final Context a;
    private final a b;
    private boolean c;
    
    public b(final Context context, final Handler handler, final b b) {
        this.a = context.getApplicationContext();
        this.b = new a(handler, b);
    }
    
    public void a(final boolean b) {
        if (b && !this.c) {
            this.a.registerReceiver((BroadcastReceiver)this.b, new IntentFilter("android.media.AUDIO_BECOMING_NOISY"));
            this.c = true;
        }
        else if (!b && this.c) {
            this.a.unregisterReceiver((BroadcastReceiver)this.b);
            this.c = false;
        }
    }
    
    private final class a extends BroadcastReceiver implements Runnable
    {
        final b a;
        private final b b;
        private final Handler c;
        
        public a(final b a, final Handler c, final b b) {
            this.a = a;
            this.c = c;
            this.b = b;
        }
        
        public void onReceive(final Context context, final Intent intent) {
            if ("android.media.AUDIO_BECOMING_NOISY".equals(intent.getAction())) {
                this.c.post((Runnable)this);
            }
        }
        
        public void run() {
            if (this.a.c) {
                this.b.a();
            }
        }
    }
    
    public interface b
    {
        void a();
    }
}

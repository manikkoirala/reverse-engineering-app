// 
// Decompiled by Procyon v0.6.0
// 

package com.applovin.exoplayer2;

import androidx.annotation.CheckResult;
import com.applovin.exoplayer2.common.a.s;
import com.applovin.exoplayer2.g.a;
import java.util.List;
import com.applovin.exoplayer2.j.k;
import com.applovin.exoplayer2.h.ad;
import androidx.annotation.Nullable;
import com.applovin.exoplayer2.h.p;

final class al
{
    private static final p.a t;
    public final ba a;
    public final p.a b;
    public final long c;
    public final long d;
    public final int e;
    @Nullable
    public final com.applovin.exoplayer2.p f;
    public final boolean g;
    public final ad h;
    public final k i;
    public final List<a> j;
    public final p.a k;
    public final boolean l;
    public final int m;
    public final am n;
    public final boolean o;
    public final boolean p;
    public volatile long q;
    public volatile long r;
    public volatile long s;
    
    static {
        t = new p.a(new Object());
    }
    
    public al(final ba a, final p.a b, final long c, final long d, final int e, @Nullable final com.applovin.exoplayer2.p f, final boolean g, final ad h, final k i, final List<a> j, final p.a k, final boolean l, final int m, final am n, final long q, final long r, final long s, final boolean o, final boolean p19) {
        this.a = a;
        this.b = b;
        this.c = c;
        this.d = d;
        this.e = e;
        this.f = f;
        this.g = g;
        this.h = h;
        this.i = i;
        this.j = j;
        this.k = k;
        this.l = l;
        this.m = m;
        this.n = n;
        this.q = q;
        this.r = r;
        this.s = s;
        this.o = o;
        this.p = p19;
    }
    
    public static al a(final k k) {
        final ba a = ba.a;
        final p.a t = al.t;
        return new al(a, t, -9223372036854775807L, 0L, 1, null, false, ad.a, k, (List<a>)s.g(), t, false, 0, am.a, 0L, 0L, 0L, false, false);
    }
    
    public static p.a a() {
        return al.t;
    }
    
    @CheckResult
    public al a(final int n) {
        return new al(this.a, this.b, this.c, this.d, n, this.f, this.g, this.h, this.i, this.j, this.k, this.l, this.m, this.n, this.q, this.r, this.s, this.o, this.p);
    }
    
    @CheckResult
    public al a(final am am) {
        return new al(this.a, this.b, this.c, this.d, this.e, this.f, this.g, this.h, this.i, this.j, this.k, this.l, this.m, am, this.q, this.r, this.s, this.o, this.p);
    }
    
    @CheckResult
    public al a(final ba ba) {
        return new al(ba, this.b, this.c, this.d, this.e, this.f, this.g, this.h, this.i, this.j, this.k, this.l, this.m, this.n, this.q, this.r, this.s, this.o, this.p);
    }
    
    @CheckResult
    public al a(final p.a a) {
        return new al(this.a, this.b, this.c, this.d, this.e, this.f, this.g, this.h, this.i, this.j, a, this.l, this.m, this.n, this.q, this.r, this.s, this.o, this.p);
    }
    
    @CheckResult
    public al a(final p.a a, final long n, final long n2, final long n3, final long n4, final ad ad, final k k, final List<a> list) {
        return new al(this.a, a, n2, n3, this.e, this.f, this.g, ad, k, list, this.k, this.l, this.m, this.n, this.q, n4, n, this.o, this.p);
    }
    
    @CheckResult
    public al a(@Nullable final com.applovin.exoplayer2.p p) {
        return new al(this.a, this.b, this.c, this.d, this.e, p, this.g, this.h, this.i, this.j, this.k, this.l, this.m, this.n, this.q, this.r, this.s, this.o, this.p);
    }
    
    @CheckResult
    public al a(final boolean b) {
        return new al(this.a, this.b, this.c, this.d, this.e, this.f, b, this.h, this.i, this.j, this.k, this.l, this.m, this.n, this.q, this.r, this.s, this.o, this.p);
    }
    
    @CheckResult
    public al a(final boolean b, final int n) {
        return new al(this.a, this.b, this.c, this.d, this.e, this.f, this.g, this.h, this.i, this.j, this.k, b, n, this.n, this.q, this.r, this.s, this.o, this.p);
    }
    
    @CheckResult
    public al b(final boolean b) {
        return new al(this.a, this.b, this.c, this.d, this.e, this.f, this.g, this.h, this.i, this.j, this.k, this.l, this.m, this.n, this.q, this.r, this.s, b, this.p);
    }
    
    @CheckResult
    public al c(final boolean b) {
        return new al(this.a, this.b, this.c, this.d, this.e, this.f, this.g, this.h, this.i, this.j, this.k, this.l, this.m, this.n, this.q, this.r, this.s, this.o, b);
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.applovin.exoplayer2;

import com.applovin.exoplayer2.l.a;
import com.applovin.exoplayer2.h.g;
import com.applovin.exoplayer2.l.q;
import com.applovin.exoplayer2.h.d;
import com.applovin.exoplayer2.h.p;
import com.applovin.exoplayer2.k.b;
import com.applovin.exoplayer2.j.k;
import androidx.annotation.Nullable;
import com.applovin.exoplayer2.j.j;
import com.applovin.exoplayer2.h.x;
import com.applovin.exoplayer2.h.n;

final class ad
{
    public final n a;
    public final Object b;
    public final x[] c;
    public boolean d;
    public boolean e;
    public ae f;
    public boolean g;
    private final boolean[] h;
    private final as[] i;
    private final j j;
    private final ah k;
    @Nullable
    private ad l;
    private com.applovin.exoplayer2.h.ad m;
    private k n;
    private long o;
    
    public ad(final as[] i, final long o, final j j, final b b, final ah k, final ae f, final k n) {
        this.i = i;
        this.o = o;
        this.j = j;
        this.k = k;
        final p.a a = f.a;
        this.b = a.a;
        this.f = f;
        this.m = com.applovin.exoplayer2.h.ad.a;
        this.n = n;
        this.c = new x[i.length];
        this.h = new boolean[i.length];
        this.a = a(a, k, b, f.b, f.d);
    }
    
    private static n a(final p.a a, final ah ah, final b b, final long n, final long n2) {
        n a2 = ah.a(a, b, n);
        if (n2 != -9223372036854775807L) {
            a2 = new d(a2, true, 0L, n2);
        }
        return a2;
    }
    
    private static void a(final ah ah, final n n) {
        try {
            if (n instanceof d) {
                ah.a(((d)n).a);
            }
            else {
                ah.a(n);
            }
        }
        catch (final RuntimeException ex) {
            q.c("MediaPeriodHolder", "Period release failed.", (Throwable)ex);
        }
    }
    
    private void a(final x[] array) {
        int n = 0;
        while (true) {
            final as[] i = this.i;
            if (n >= i.length) {
                break;
            }
            if (i[n].a() == -2) {
                array[n] = null;
            }
            ++n;
        }
    }
    
    private void b(final x[] array) {
        int n = 0;
        while (true) {
            final as[] i = this.i;
            if (n >= i.length) {
                break;
            }
            if (i[n].a() == -2 && this.n.a(n)) {
                array[n] = new g();
            }
            ++n;
        }
    }
    
    private void k() {
        if (!this.m()) {
            return;
        }
        int n = 0;
        while (true) {
            final k n2 = this.n;
            if (n >= n2.a) {
                break;
            }
            final boolean a = n2.a(n);
            final com.applovin.exoplayer2.j.d d = this.n.c[n];
            if (a && d != null) {
                d.a();
            }
            ++n;
        }
    }
    
    private void l() {
        if (!this.m()) {
            return;
        }
        int n = 0;
        while (true) {
            final k n2 = this.n;
            if (n >= n2.a) {
                break;
            }
            final boolean a = n2.a(n);
            final com.applovin.exoplayer2.j.d d = this.n.c[n];
            if (a && d != null) {
                d.b();
            }
            ++n;
        }
    }
    
    private boolean m() {
        return this.l == null;
    }
    
    public long a() {
        return this.o;
    }
    
    public long a(final long n) {
        return n + this.a();
    }
    
    public long a(final k k, final long n, final boolean b) {
        return this.a(k, n, b, new boolean[this.i.length]);
    }
    
    public long a(final k n, long a, final boolean b, final boolean[] array) {
        int n2 = 0;
        while (true) {
            final int a2 = n.a;
            boolean b2 = true;
            if (n2 >= a2) {
                break;
            }
            final boolean[] h = this.h;
            if (b || !n.a(this.n, n2)) {
                b2 = false;
            }
            h[n2] = b2;
            ++n2;
        }
        this.a(this.c);
        this.l();
        this.n = n;
        this.k();
        a = this.a.a(n.c, this.h, this.c, array, a);
        this.b(this.c);
        this.e = false;
        int n3 = 0;
        while (true) {
            final x[] c = this.c;
            if (n3 >= c.length) {
                break;
            }
            if (c[n3] != null) {
                a.b(n.a(n3));
                if (this.i[n3].a() != -2) {
                    this.e = true;
                }
            }
            else {
                a.b(n.c[n3] == null);
            }
            ++n3;
        }
        return a;
    }
    
    public void a(final float n, final ba ba) throws com.applovin.exoplayer2.p {
        this.d = true;
        this.m = this.a.b();
        final k b = this.b(n, ba);
        final ae f = this.f;
        final long b2 = f.b;
        final long e = f.e;
        long max = b2;
        if (e != -9223372036854775807L) {
            max = b2;
            if (b2 >= e) {
                max = Math.max(0L, e - 1L);
            }
        }
        final long a = this.a(b, max, false);
        final long o = this.o;
        final ae f2 = this.f;
        this.o = o + (f2.b - a);
        this.f = f2.a(a);
    }
    
    public void a(@Nullable final ad l) {
        if (l == this.l) {
            return;
        }
        this.l();
        this.l = l;
        this.k();
    }
    
    public long b() {
        return this.f.b + this.o;
    }
    
    public long b(final long n) {
        return n - this.a();
    }
    
    public k b(final float n, final ba ba) throws com.applovin.exoplayer2.p {
        final k a = this.j.a(this.i, this.h(), this.f.a, ba);
        for (final com.applovin.exoplayer2.j.d d : a.c) {
            if (d != null) {
                d.a(n);
            }
        }
        return a;
    }
    
    public void c(final long o) {
        this.o = o;
    }
    
    public boolean c() {
        return this.d && (!this.e || this.a.d() == Long.MIN_VALUE);
    }
    
    public long d() {
        if (!this.d) {
            return this.f.b;
        }
        long d;
        if (this.e) {
            d = this.a.d();
        }
        else {
            d = Long.MIN_VALUE;
        }
        long e = d;
        if (d == Long.MIN_VALUE) {
            e = this.f.e;
        }
        return e;
    }
    
    public void d(final long n) {
        com.applovin.exoplayer2.l.a.b(this.m());
        if (this.d) {
            this.a.a(this.b(n));
        }
    }
    
    public long e() {
        long e;
        if (!this.d) {
            e = 0L;
        }
        else {
            e = this.a.e();
        }
        return e;
    }
    
    public void e(long b) {
        com.applovin.exoplayer2.l.a.b(this.m());
        b = this.b(b);
        this.a.c(b);
    }
    
    public void f() {
        this.l();
        a(this.k, this.a);
    }
    
    @Nullable
    public ad g() {
        return this.l;
    }
    
    public com.applovin.exoplayer2.h.ad h() {
        return this.m;
    }
    
    public k i() {
        return this.n;
    }
    
    public void j() {
        final n a = this.a;
        if (a instanceof d) {
            long d;
            if ((d = this.f.d) == -9223372036854775807L) {
                d = Long.MIN_VALUE;
            }
            ((d)a).a(0L, d);
        }
    }
}

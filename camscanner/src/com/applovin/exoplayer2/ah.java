// 
// Decompiled by Procyon v0.6.0
// 

package com.applovin.exoplayer2;

import com.applovin.exoplayer2.d.OO0o\u3007\u3007\u3007\u30070;
import java.io.IOException;
import com.applovin.exoplayer2.h.m;
import com.applovin.exoplayer2.h.j;
import java.util.Collection;
import com.applovin.exoplayer2.h.k;
import com.applovin.exoplayer2.k.b;
import java.util.Iterator;
import com.applovin.exoplayer2.h.l;
import com.applovin.exoplayer2.l.ai;
import com.applovin.exoplayer2.h.p;
import java.util.HashSet;
import java.util.ArrayList;
import android.os.Handler;
import com.applovin.exoplayer2.a.a;
import androidx.annotation.Nullable;
import com.applovin.exoplayer2.k.aa;
import com.applovin.exoplayer2.h.z;
import java.util.Set;
import java.util.HashMap;
import com.applovin.exoplayer2.d.g;
import com.applovin.exoplayer2.h.q;
import java.util.Map;
import com.applovin.exoplayer2.h.n;
import java.util.IdentityHashMap;
import java.util.List;

final class ah
{
    private final List<c> a;
    private final IdentityHashMap<n, c> b;
    private final Map<Object, c> c;
    private final d d;
    private final q.a e;
    private final g.a f;
    private final HashMap<c, b> g;
    private final Set<c> h;
    private z i;
    private boolean j;
    @Nullable
    private aa k;
    
    public ah(final d d, @Nullable final com.applovin.exoplayer2.a.a a, final Handler handler) {
        this.d = d;
        this.i = new z.a(0);
        this.b = new IdentityHashMap<n, c>();
        this.c = new HashMap<Object, c>();
        this.a = new ArrayList<c>();
        final q.a e = new q.a();
        this.e = e;
        final g.a f = new g.a();
        this.f = f;
        this.g = new HashMap<c, b>();
        this.h = new HashSet<c>();
        if (a != null) {
            e.a(handler, a);
            f.a(handler, a);
        }
    }
    
    private static Object a(final c c, final Object o) {
        return com.applovin.exoplayer2.a.a(c.b, o);
    }
    
    private static Object a(final Object o) {
        return com.applovin.exoplayer2.a.a(o);
    }
    
    private void a(final int n, int i) {
        --i;
        while (i >= n) {
            final c c = this.a.remove(i);
            this.c.remove(c.b);
            this.b(i, -c.a.f().b());
            c.e = true;
            if (this.j) {
                this.d(c);
            }
            --i;
        }
    }
    
    private void a(final c key) {
        this.h.add(key);
        final b b = this.g.get(key);
        if (b != null) {
            b.a.a(b.b);
        }
    }
    
    private static int b(final c c, final int n) {
        return n + c.d;
    }
    
    @Nullable
    private static p.a b(final c c, final p.a a) {
        for (int i = 0; i < c.c.size(); ++i) {
            if (c.c.get(i).d == a.d) {
                return a.b(a(c, a.a));
            }
        }
        return null;
    }
    
    private static Object b(final Object o) {
        return com.applovin.exoplayer2.a.b(o);
    }
    
    private void b(int i, final int n) {
        while (i < this.a.size()) {
            final c c = this.a.get(i);
            c.d += n;
            ++i;
        }
    }
    
    private void b(final c key) {
        final b b = this.g.get(key);
        if (b != null) {
            b.a.b(b.b);
        }
    }
    
    private void c(final c key) {
        final l a = key.a;
        final o\u30070 o\u30070 = new o\u30070(this);
        final a a2 = new a(key);
        this.g.put(key, new b(a, o\u30070, a2));
        a.a(ai.b(), (q)a2);
        a.a(ai.b(), (g)a2);
        a.a((p.b)o\u30070, this.k);
    }
    
    private void d(final c key) {
        if (key.e && key.c.isEmpty()) {
            final b b = (b)com.applovin.exoplayer2.l.a.b((Object)this.g.remove(key));
            b.a.c(b.b);
            b.a.a((q)b.c);
            b.a.a((g)b.c);
            this.h.remove(key);
        }
    }
    
    private void e() {
        final Iterator<c> iterator = this.h.iterator();
        while (iterator.hasNext()) {
            final c c = iterator.next();
            if (c.c.isEmpty()) {
                this.b(c);
                iterator.remove();
            }
        }
    }
    
    public ba a(int i, int d, final int b, final z j) {
        com.applovin.exoplayer2.l.a.a(i >= 0 && i <= d && d <= this.b() && b >= 0);
        this.i = j;
        if (i != d && i != b) {
            final int min = Math.min(i, b);
            final int max = Math.max(d - i + b - 1, d - 1);
            final int d2 = this.a.get(min).d;
            ai.a((List)this.a, i, d, b);
            i = min;
            d = d2;
            while (i <= max) {
                final c c = this.a.get(i);
                c.d = d;
                d += c.a.f().b();
                ++i;
            }
            return this.d();
        }
        return this.d();
    }
    
    public ba a(final int n, final int n2, final z i) {
        com.applovin.exoplayer2.l.a.a(n >= 0 && n <= n2 && n2 <= this.b());
        this.i = i;
        this.a(n, n2);
        return this.d();
    }
    
    public ba a(final int n, final List<c> list, final z i) {
        if (!list.isEmpty()) {
            this.i = i;
            for (int j = n; j < list.size() + n; ++j) {
                final c c = list.get(j - n);
                if (j > 0) {
                    final c c2 = this.a.get(j - 1);
                    c.a(c2.d + c2.a.f().b());
                }
                else {
                    c.a(0);
                }
                this.b(j, c.a.f().b());
                this.a.add(j, c);
                this.c.put(c.b, c);
                if (this.j) {
                    this.c(c);
                    if (this.b.isEmpty()) {
                        this.h.add(c);
                    }
                    else {
                        this.b(c);
                    }
                }
            }
        }
        return this.d();
    }
    
    public ba a(final z z) {
        final int b = this.b();
        z a = z;
        if (z.a() != b) {
            a = z.d().a(0, b);
        }
        this.i = a;
        return this.d();
    }
    
    public ba a(final List<c> list, final z z) {
        this.a(0, this.a.size());
        return this.a(this.a.size(), list, z);
    }
    
    public n a(p.a b, final com.applovin.exoplayer2.k.b b2, final long n) {
        final Object a = a(b.a);
        b = b.b(b(b.a));
        final c value = (c)com.applovin.exoplayer2.l.a.b((Object)this.c.get(a));
        this.a(value);
        value.c.add(b);
        final k a2 = value.a.a(b, b2, n);
        this.b.put(a2, value);
        this.e();
        return a2;
    }
    
    public void a(final n key) {
        final c c = (c)com.applovin.exoplayer2.l.a.b((Object)this.b.remove(key));
        c.a.a(key);
        c.c.remove(((k)key).a);
        if (!this.b.isEmpty()) {
            this.e();
        }
        this.d(c);
    }
    
    public void a(@Nullable final aa k) {
        com.applovin.exoplayer2.l.a.b(this.j ^ true);
        this.k = k;
        for (int i = 0; i < this.a.size(); ++i) {
            final c c = this.a.get(i);
            this.c(c);
            this.h.add(c);
        }
        this.j = true;
    }
    
    public boolean a() {
        return this.j;
    }
    
    public int b() {
        return this.a.size();
    }
    
    public void c() {
        for (final b b : this.g.values()) {
            try {
                b.a.c(b.b);
            }
            catch (final RuntimeException ex) {
                com.applovin.exoplayer2.l.q.c("MediaSourceList", "Failed to release child source.", (Throwable)ex);
            }
            b.a.a((q)b.c);
            b.a.a((g)b.c);
        }
        this.g.clear();
        this.h.clear();
        this.j = false;
    }
    
    public ba d() {
        if (this.a.isEmpty()) {
            return ba.a;
        }
        int i = 0;
        int d = 0;
        while (i < this.a.size()) {
            final c c = this.a.get(i);
            c.d = d;
            d += c.a.f().b();
            ++i;
        }
        return new ap(this.a, this.i);
    }
    
    private final class a implements g, q
    {
        final ah a;
        private final c b;
        private q.a c;
        private g.a d;
        
        public a(final ah a, final c b) {
            this.a = a;
            this.c = a.e;
            this.d = a.f;
            this.b = b;
        }
        
        private boolean f(int a, @Nullable p.a a2) {
            if (a2 != null) {
                if ((a2 = b(this.b, a2)) == null) {
                    return false;
                }
            }
            else {
                a2 = null;
            }
            a = b(this.b, a);
            final q.a c = this.c;
            if (c.a != a || !ai.a((Object)c.b, (Object)a2)) {
                this.c = this.a.e.a(a, a2, 0L);
            }
            final g.a d = this.d;
            if (d.a != a || !ai.a((Object)d.b, (Object)a2)) {
                this.d = this.a.f.a(a, a2);
            }
            return true;
        }
        
        @Override
        public void a(final int n, @Nullable final p.a a) {
            if (this.f(n, a)) {
                this.d.a();
            }
        }
        
        @Override
        public void a(final int n, @Nullable final p.a a, final int n2) {
            if (this.f(n, a)) {
                this.d.a(n2);
            }
        }
        
        @Override
        public void a(final int n, @Nullable final p.a a, final j j, final m m) {
            if (this.f(n, a)) {
                this.c.a(j, m);
            }
        }
        
        @Override
        public void a(final int n, @Nullable final p.a a, final j j, final m m, final IOException ex, final boolean b) {
            if (this.f(n, a)) {
                this.c.a(j, m, ex, b);
            }
        }
        
        @Override
        public void a(final int n, @Nullable final p.a a, final m m) {
            if (this.f(n, a)) {
                this.c.a(m);
            }
        }
        
        @Override
        public void a(final int n, @Nullable final p.a a, final Exception ex) {
            if (this.f(n, a)) {
                this.d.a(ex);
            }
        }
        
        @Override
        public void b(final int n, @Nullable final p.a a) {
            if (this.f(n, a)) {
                this.d.b();
            }
        }
        
        @Override
        public void b(final int n, @Nullable final p.a a, final j j, final m m) {
            if (this.f(n, a)) {
                this.c.b(j, m);
            }
        }
        
        @Override
        public void c(final int n, @Nullable final p.a a) {
            if (this.f(n, a)) {
                this.d.c();
            }
        }
        
        @Override
        public void c(final int n, @Nullable final p.a a, final j j, final m m) {
            if (this.f(n, a)) {
                this.c.c(j, m);
            }
        }
        
        @Override
        public void d(final int n, @Nullable final p.a a) {
            if (this.f(n, a)) {
                this.d.d();
            }
        }
    }
    
    private static final class b
    {
        public final p a;
        public final p.b b;
        public final a c;
        
        public b(final p a, final p.b b, final a c) {
            this.a = a;
            this.b = b;
            this.c = c;
        }
    }
    
    static final class c implements ag
    {
        public final l a;
        public final Object b;
        public final List<p.a> c;
        public int d;
        public boolean e;
        
        public c(final p p2, final boolean b) {
            this.a = new l(p2, b);
            this.c = new ArrayList<p.a>();
            this.b = new Object();
        }
        
        @Override
        public Object a() {
            return this.b;
        }
        
        public void a(final int d) {
            this.d = d;
            this.e = false;
            this.c.clear();
        }
        
        @Override
        public ba b() {
            return this.a.f();
        }
    }
    
    public interface d
    {
        void e();
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.applovin.exoplayer2;

import com.applovin.exoplayer2.l.ai;
import java.util.Arrays;
import java.util.List;
import java.util.Iterator;
import com.applovin.exoplayer2.h.z;
import java.util.Collection;
import java.util.HashMap;

final class ap extends a
{
    private final int c;
    private final int d;
    private final int[] e;
    private final int[] f;
    private final ba[] g;
    private final Object[] h;
    private final HashMap<Object, Integer> i;
    
    public ap(final Collection<? extends ag> collection, final z z) {
        int c = 0;
        super(false, z);
        final int size = collection.size();
        this.e = new int[size];
        this.f = new int[size];
        this.g = new ba[size];
        this.h = new Object[size];
        this.i = new HashMap<Object, Integer>();
        final Iterator iterator = collection.iterator();
        int d = 0;
        int i = 0;
        while (iterator.hasNext()) {
            final ag ag = (ag)iterator.next();
            this.g[i] = ag.b();
            this.f[i] = c;
            this.e[i] = d;
            c += this.g[i].b();
            d += this.g[i].c();
            this.h[i] = ag.a();
            this.i.put(this.h[i], i);
            ++i;
        }
        this.c = c;
        this.d = d;
    }
    
    List<ba> a() {
        return Arrays.asList(this.g);
    }
    
    @Override
    public int b() {
        return this.c;
    }
    
    @Override
    protected int b(final int n) {
        return ai.a(this.e, n + 1, false, false);
    }
    
    @Override
    public int c() {
        return this.d;
    }
    
    @Override
    protected int c(final int n) {
        return ai.a(this.f, n + 1, false, false);
    }
    
    @Override
    protected int d(final Object key) {
        final Integer n = this.i.get(key);
        int intValue;
        if (n == null) {
            intValue = -1;
        }
        else {
            intValue = n;
        }
        return intValue;
    }
    
    @Override
    protected ba d(final int n) {
        return this.g[n];
    }
    
    @Override
    protected int e(final int n) {
        return this.e[n];
    }
    
    @Override
    protected int f(final int n) {
        return this.f[n];
    }
    
    @Override
    protected Object g(final int n) {
        return this.h[n];
    }
}

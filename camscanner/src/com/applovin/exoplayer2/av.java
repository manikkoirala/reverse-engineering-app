// 
// Decompiled by Procyon v0.6.0
// 

package com.applovin.exoplayer2;

import androidx.annotation.Nullable;
import com.applovin.exoplayer2.l.ai;
import com.applovin.exoplayer2.l.a;

public final class av
{
    public static final av a;
    public static final av b;
    public static final av c;
    public static final av d;
    public static final av e;
    public final long f;
    public final long g;
    
    static {
        final av e2 = a = new av(0L, 0L);
        b = new av(Long.MAX_VALUE, Long.MAX_VALUE);
        c = new av(Long.MAX_VALUE, 0L);
        d = new av(0L, Long.MAX_VALUE);
        e = e2;
    }
    
    public av(final long f, final long g) {
        final boolean b = true;
        com.applovin.exoplayer2.l.a.a(f >= 0L);
        com.applovin.exoplayer2.l.a.a(g >= 0L && b);
        this.f = f;
        this.g = g;
    }
    
    public long a(final long n, final long n2, final long n3) {
        final long f = this.f;
        if (f == 0L && this.g == 0L) {
            return n;
        }
        final long c = ai.c(n, f, Long.MIN_VALUE);
        final long b = ai.b(n, this.g, Long.MAX_VALUE);
        boolean b2 = true;
        final boolean b3 = c <= n2 && n2 <= b;
        if (c > n3 || n3 > b) {
            b2 = false;
        }
        if (b3 && b2) {
            if (Math.abs(n2 - n) <= Math.abs(n3 - n)) {
                return n2;
            }
            return n3;
        }
        else {
            if (b3) {
                return n2;
            }
            if (b2) {
                return n3;
            }
            return c;
        }
    }
    
    @Override
    public boolean equals(@Nullable final Object o) {
        boolean b = true;
        if (this == o) {
            return true;
        }
        if (o != null && av.class == o.getClass()) {
            final av av = (av)o;
            if (this.f != av.f || this.g != av.g) {
                b = false;
            }
            return b;
        }
        return false;
    }
    
    @Override
    public int hashCode() {
        return (int)this.f * 31 + (int)this.g;
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.applovin.exoplayer2;

import com.applovin.exoplayer2.j.g;
import com.applovin.exoplayer2.d.f;
import com.applovin.exoplayer2.h.b;
import android.os.Message;
import com.applovin.exoplayer2.h.y;
import android.os.SystemClock;
import java.util.concurrent.atomic.AtomicBoolean;
import com.applovin.exoplayer2.l.q;
import java.io.IOException;
import java.util.Collection;
import com.applovin.exoplayer2.common.base.Supplier;
import com.applovin.exoplayer2.l.ai;
import java.util.Collections;
import androidx.annotation.CheckResult;
import java.util.List;
import android.util.Pair;
import com.applovin.exoplayer2.h.x;
import android.os.Handler;
import com.applovin.exoplayer2.common.a.aq;
import com.applovin.exoplayer2.a.a;
import java.util.ArrayList;
import android.os.Looper;
import android.os.HandlerThread;
import com.applovin.exoplayer2.l.o;
import com.applovin.exoplayer2.k.d;
import com.applovin.exoplayer2.j.k;
import com.applovin.exoplayer2.j.j;
import java.util.Set;
import androidx.annotation.Nullable;
import com.applovin.exoplayer2.j.j$a;
import com.applovin.exoplayer2.h.n;
import android.os.Handler$Callback;

final class s implements Handler$Callback, ah.d, ao.a, n.a, j$a, m.a
{
    private boolean A;
    private boolean B;
    private boolean C;
    private boolean D;
    private int E;
    private boolean F;
    private boolean G;
    private boolean H;
    private boolean I;
    private int J;
    @Nullable
    private g K;
    private long L;
    private int M;
    private boolean N;
    @Nullable
    private p O;
    private long P;
    private final ar[] a;
    private final Set<ar> b;
    private final as[] c;
    private final j d;
    private final k e;
    private final aa f;
    private final com.applovin.exoplayer2.k.d g;
    private final o h;
    private final HandlerThread i;
    private final Looper j;
    private final ba.c k;
    private final ba.a l;
    private final long m;
    private final boolean n;
    private final m o;
    private final ArrayList<c> p;
    private final com.applovin.exoplayer2.l.d q;
    private final e r;
    private final af s;
    private final ah t;
    private final z u;
    private final long v;
    private av w;
    private al x;
    private d y;
    private boolean z;
    
    public s(final ar[] a, final j d, final k e, final aa f, final com.applovin.exoplayer2.k.d g, int i, final boolean f2, @Nullable final com.applovin.exoplayer2.a.a a2, final av w, final z u, final long n, final boolean a3, final Looper looper, final com.applovin.exoplayer2.l.d q, final e r) {
        this.r = r;
        this.a = a;
        this.d = d;
        this.e = e;
        this.f = f;
        this.g = g;
        this.E = i;
        this.F = f2;
        this.w = w;
        this.u = u;
        this.v = n;
        this.P = n;
        this.A = a3;
        this.q = q;
        this.m = f.e();
        this.n = f.f();
        final al a4 = al.a(e);
        this.x = a4;
        this.y = new d(a4);
        this.c = new as[a.length];
        for (i = 0; i < a.length; ++i) {
            a[i].a(i);
            this.c[i] = a[i].b();
        }
        this.o = new m((m.a)this, q);
        this.p = new ArrayList<c>();
        this.b = aq.b();
        this.k = new ba.c();
        this.l = new ba.a();
        d.a((j$a)this, g);
        this.N = true;
        final Handler handler = new Handler(looper);
        this.s = new af(a2, handler);
        this.t = new ah((ah.d)this, a2, handler);
        final HandlerThread j = new HandlerThread("ExoPlayer:Playback", -16);
        ((Thread)(this.i = j)).start();
        final Looper looper2 = j.getLooper();
        this.j = looper2;
        this.h = q.a(looper2, (Handler$Callback)this);
    }
    
    private void A() {
        final ad c = this.s.c();
        this.B = (c != null && c.f.h && this.A);
    }
    
    private boolean B() {
        final boolean j = this.J();
        final boolean b = false;
        if (!j) {
            return false;
        }
        if (this.B) {
            return false;
        }
        final ad c = this.s.c();
        if (c == null) {
            return false;
        }
        final ad g = c.g();
        boolean b2 = b;
        if (g != null) {
            b2 = b;
            if (this.L >= g.b()) {
                b2 = b;
                if (g.g) {
                    b2 = true;
                }
            }
        }
        return b2;
    }
    
    private boolean C() {
        final ad d = this.s.d();
        if (!d.d) {
            return false;
        }
        int n = 0;
        while (true) {
            final ar[] a = this.a;
            if (n >= a.length) {
                return true;
            }
            final ar ar = a[n];
            final x x = d.c[n];
            if (ar.f() != x || (x != null && !ar.g() && !this.a(ar, d))) {
                return false;
            }
            ++n;
        }
    }
    
    private void D() {
        final boolean e = this.E();
        this.D = e;
        if (e) {
            this.s.b().e(this.L);
        }
        this.G();
    }
    
    private boolean E() {
        if (!this.F()) {
            return false;
        }
        final ad b = this.s.b();
        final long d = this.d(b.e());
        long b2;
        if (b == this.s.c()) {
            b2 = b.b(this.L);
        }
        else {
            b2 = b.b(this.L) - b.f.b;
        }
        return this.f.a(b2, d, this.o.d().b);
    }
    
    private boolean F() {
        final ad b = this.s.b();
        return b != null && b.e() != Long.MIN_VALUE;
    }
    
    private void G() {
        final ad b = this.s.b();
        final boolean b2 = this.D || (b != null && b.a.f());
        final al x = this.x;
        if (b2 != x.g) {
            this.x = x.a(b2);
        }
    }
    
    private void H() throws p {
        this.a(new boolean[this.a.length]);
    }
    
    private long I() {
        return this.d(this.x.q);
    }
    
    private boolean J() {
        final al x = this.x;
        return x.l && x.m == 0;
    }
    
    private long a(final ba ba, final Object o, final long n) {
        ba.a(ba.a(o, this.l).c, this.k);
        final ba.c k = this.k;
        if (k.g != -9223372036854775807L && k.e()) {
            final ba.c i = this.k;
            if (i.j) {
                return com.applovin.exoplayer2.h.b(i.d() - this.k.g) - (n + this.l.c());
            }
        }
        return -9223372036854775807L;
    }
    
    private long a(final com.applovin.exoplayer2.h.p.a a, final long n, final boolean b) throws p {
        return this.a(a, n, this.s.c() != this.s.d(), b);
    }
    
    private long a(final com.applovin.exoplayer2.h.p.a a, long n, final boolean b, final boolean b2) throws p {
        this.j();
        this.C = false;
        if (b2 || this.x.e == 3) {
            this.b(2);
        }
        ad ad2;
        ad ad;
        for (ad = (ad2 = this.s.c()); ad2 != null && !a.equals(ad2.f.a); ad2 = ad2.g()) {}
        if (b || ad != ad2 || (ad2 != null && ad2.a(n) < 0L)) {
            final ar[] a2 = this.a;
            for (int length = a2.length, i = 0; i < length; ++i) {
                this.b(a2[i]);
            }
            if (ad2 != null) {
                while (this.s.c() != ad2) {
                    this.s.f();
                }
                this.s.a(ad2);
                ad2.c(0L);
                this.H();
            }
        }
        if (ad2 != null) {
            this.s.a(ad2);
            long b3;
            if (!ad2.d) {
                ad2.f = ad2.f.a(n);
                b3 = n;
            }
            else {
                b3 = n;
                if (ad2.e) {
                    b3 = ad2.a.b(n);
                    ad2.a.a(b3 - this.m, this.n);
                }
            }
            this.b(b3);
            this.D();
            n = b3;
        }
        else {
            this.s.g();
            this.b(n);
        }
        this.h(false);
        this.h.c(2);
        return n;
    }
    
    private Pair<com.applovin.exoplayer2.h.p.a, Long> a(final ba ba) {
        final boolean d = ba.d();
        final long n = 0L;
        if (d) {
            return (Pair<com.applovin.exoplayer2.h.p.a, Long>)Pair.create((Object)al.a(), (Object)0L);
        }
        final Pair<Object, Long> a = ba.a(this.k, this.l, ba.b(this.F), -9223372036854775807L);
        final com.applovin.exoplayer2.h.p.a a2 = this.s.a(ba, a.first, 0L);
        long l = (long)a.second;
        if (a2.a()) {
            ba.a(a2.a, this.l);
            l = n;
            if (a2.c == this.l.b(a2.b)) {
                l = this.l.f();
            }
        }
        return (Pair<com.applovin.exoplayer2.h.p.a, Long>)Pair.create((Object)a2, (Object)l);
    }
    
    @Nullable
    private static Pair<Object, Long> a(final ba ba, final g g, final boolean b, final int n, final boolean b2, final ba.c c, final ba.a a) {
        ba a2 = g.a;
        if (ba.d()) {
            return null;
        }
        if (a2.d()) {
            a2 = ba;
        }
        try {
            final Pair<Object, Long> a3 = a2.a(c, a, g.b, g.c);
            if (ba.equals(a2)) {
                return a3;
            }
            if (ba.c(a3.first) != -1) {
                Pair<Object, Long> a4 = a3;
                if (a2.a(a3.first, a).f) {
                    a4 = a3;
                    if (a2.a(a.c, c).p == a2.c(a3.first)) {
                        a4 = ba.a(c, a, ba.a(a3.first, a).c, g.c);
                    }
                }
                return a4;
            }
            if (b) {
                final Object a5 = a(c, a, n, b2, a3.first, a2, ba);
                if (a5 != null) {
                    return ba.a(c, a, ba.a(a5, a).c, -9223372036854775807L);
                }
            }
            return null;
        }
        catch (final IndexOutOfBoundsException ex) {
            return null;
        }
    }
    
    @CheckResult
    private al a(final com.applovin.exoplayer2.h.p.a a, final long n, final long n2, final long n3, final boolean b, final int n4) {
        this.N = (this.N || n != this.x.s || !a.equals(this.x.b));
        this.A();
        final al x = this.x;
        com.applovin.exoplayer2.h.ad ad = x.h;
        k k = x.i;
        Object o = x.j;
        if (this.t.a()) {
            final ad c = this.s.c();
            if (c == null) {
                ad = com.applovin.exoplayer2.h.ad.a;
            }
            else {
                ad = c.h();
            }
            if (c == null) {
                k = this.e;
            }
            else {
                k = c.i();
            }
            o = this.a(k.c);
            if (c != null) {
                final ae f = c.f;
                if (f.c != n2) {
                    c.f = f.b(n2);
                }
            }
        }
        else if (!a.equals(this.x.b)) {
            ad = com.applovin.exoplayer2.h.ad.a;
            k = this.e;
            o = com.applovin.exoplayer2.common.a.s.g();
        }
        if (b) {
            this.y.b(n4);
        }
        return this.x.a(a, n, n2, n3, this.I(), ad, k, (List<com.applovin.exoplayer2.g.a>)o);
    }
    
    private com.applovin.exoplayer2.common.a.s<com.applovin.exoplayer2.g.a> a(final com.applovin.exoplayer2.j.d[] array) {
        final com.applovin.exoplayer2.common.a.s.a<com.applovin.exoplayer2.g.a> a = new com.applovin.exoplayer2.common.a.s.a<com.applovin.exoplayer2.g.a>();
        final int length = array.length;
        int i = 0;
        int n = 0;
        while (i < length) {
            final com.applovin.exoplayer2.j.d d = array[i];
            int n2 = n;
            if (d != null) {
                final com.applovin.exoplayer2.g.a j = ((com.applovin.exoplayer2.j.g)d).a(0).j;
                if (j == null) {
                    a.b(new com.applovin.exoplayer2.g.a(new com.applovin.exoplayer2.g.a.a[0]));
                    n2 = n;
                }
                else {
                    a.b(j);
                    n2 = 1;
                }
            }
            ++i;
            n = n2;
        }
        List<E> list;
        if (n != 0) {
            list = (List<E>)a.a();
        }
        else {
            list = (List<E>)com.applovin.exoplayer2.common.a.s.g();
        }
        return (com.applovin.exoplayer2.common.a.s<com.applovin.exoplayer2.g.a>)list;
    }
    
    private static f a(final ba ba, final al al, @Nullable final g g, final af af, int n, final boolean b, final ba.c c, final ba.a a) {
        if (ba.d()) {
            return new f(al.a(), 0L, -9223372036854775807L, false, true, false);
        }
        final com.applovin.exoplayer2.h.p.a b2 = al.b;
        Object obj = b2.a;
        final boolean a2 = a(al, a);
        long n2;
        if (!al.b.a() && !a2) {
            n2 = al.s;
        }
        else {
            n2 = al.c;
        }
        final int n3 = 0;
        long n4 = 0L;
        boolean b5 = false;
        boolean b7 = false;
        boolean b8 = false;
        Label_0489: {
            if (g != null) {
                final Pair<Object, Long> a3 = a(ba, g, true, n, b, c, a);
                boolean b3;
                boolean b4;
                if (a3 == null) {
                    n = ba.b(b);
                    n4 = n2;
                    b3 = false;
                    b4 = false;
                    b5 = true;
                }
                else {
                    if (g.c == -9223372036854775807L) {
                        n = ba.a(a3.first, a).c;
                        n4 = n2;
                        b3 = false;
                    }
                    else {
                        obj = a3.first;
                        n4 = (long)a3.second;
                        b3 = true;
                        n = -1;
                    }
                    b4 = (al.e == 4);
                    b5 = false;
                }
                final boolean b6 = b3;
                b7 = b4;
                b8 = b6;
            }
            else {
                Label_0250: {
                    if (al.a.d()) {
                        n = ba.b(b);
                    }
                    else {
                        if (ba.c(obj) == -1) {
                            final Object a4 = a(c, a, n, b, obj, al.a, ba);
                            boolean b9;
                            if (a4 == null) {
                                n = ba.b(b);
                                b9 = true;
                            }
                            else {
                                n = ba.a(a4, a).c;
                                b9 = false;
                            }
                            b5 = b9;
                            break Label_0250;
                        }
                        if (n2 == -9223372036854775807L) {
                            n = ba.a(obj, a).c;
                        }
                        else {
                            if (a2) {
                                al.a.a(b2.a, a);
                                if (al.a.a(a.c, c).p == al.a.c(b2.a)) {
                                    final Pair<Object, Long> a5 = ba.a(c, a, ba.a(obj, a).c, n2 + a.c());
                                    obj = a5.first;
                                    n4 = (long)a5.second;
                                }
                                else {
                                    n4 = n2;
                                }
                                n = -1;
                                b7 = false;
                                b5 = false;
                                b8 = true;
                                break Label_0489;
                            }
                            n = -1;
                        }
                    }
                    b5 = false;
                }
                b7 = false;
                n4 = n2;
                b8 = false;
            }
        }
        long n5;
        if (n != -1) {
            final Pair<Object, Long> a6 = ba.a(c, a, n, -9223372036854775807L);
            obj = a6.first;
            n4 = (long)a6.second;
            n5 = -9223372036854775807L;
        }
        else {
            n5 = n4;
        }
        com.applovin.exoplayer2.h.p.a a7 = af.a(ba, obj, n4);
        Label_0591: {
            if (a7.e != -1) {
                n = b2.e;
                if (n == -1 || a7.b < n) {
                    n = 0;
                    break Label_0591;
                }
            }
            n = 1;
        }
        final boolean equals = b2.a.equals(obj);
        if (equals && !b2.a() && !a7.a() && n != 0) {
            n = 1;
        }
        else {
            n = 0;
        }
        ba.a(obj, a);
        int n6 = n3;
        Label_0727: {
            if (equals) {
                n6 = n3;
                if (!a2) {
                    n6 = n3;
                    if (n2 == n5) {
                        if (!a7.a() || !a.e(a7.b)) {
                            n6 = n3;
                            if (!b2.a()) {
                                break Label_0727;
                            }
                            n6 = n3;
                            if (!a.e(b2.b)) {
                                break Label_0727;
                            }
                        }
                        n6 = 1;
                    }
                }
            }
        }
        if (n != 0 || n6 != 0) {
            a7 = b2;
        }
        long n7 = n4;
        if (a7.a()) {
            if (a7.equals(b2)) {
                n7 = al.s;
            }
            else {
                ba.a(a7.a, a);
                if (a7.c == a.b(a7.b)) {
                    n7 = a.f();
                }
                else {
                    n7 = 0L;
                }
            }
        }
        return new f(a7, n7, n5, b7, b5, b8);
    }
    
    @Nullable
    static Object a(final ba.c c, final ba.a a, final int n, final boolean b, final Object o, final ba ba, final ba ba2) {
        int n2;
        int c2;
        int n3;
        int c3;
        for (n2 = ba.c(o), c2 = ba.c(), n3 = 0, c3 = -1; n3 < c2 && c3 == -1; c3 = ba2.c(ba.a(n2)), ++n3) {
            n2 = ba.a(n2, a, c, n, b);
            if (n2 == -1) {
                break;
            }
        }
        Object a2;
        if (c3 == -1) {
            a2 = null;
        }
        else {
            a2 = ba2.a(c3);
        }
        return a2;
    }
    
    private void a(final float n) {
        for (ad ad = this.s.c(); ad != null; ad = ad.g()) {
            for (final com.applovin.exoplayer2.j.d d : ad.i().c) {
                if (d != null) {
                    d.a(n);
                }
            }
        }
    }
    
    private void a(final int n, final boolean b) throws p {
        final ar ar = this.a[n];
        if (c(ar)) {
            return;
        }
        final ad d = this.s.d();
        final boolean b2 = d == this.s.c();
        final k i = d.i();
        final at at = i.b[n];
        final v[] a = a(i.c[n]);
        final boolean b3 = this.J() && this.x.e == 3;
        final boolean b4 = !b && b3;
        ++this.J;
        this.b.add(ar);
        ar.a(at, a, d.c[n], this.L, b4, b2, d.b(), d.a());
        ((ao.b)ar).a(11, new ar.a(this) {
            final s a;
            
            @Override
            public void a() {
                this.a.h.c(2);
            }
            
            @Override
            public void a(final long n) {
                if (n >= 2000L) {
                    this.a.H = true;
                }
            }
        });
        this.o.a(ar);
        if (b3) {
            ar.e();
        }
    }
    
    private void a(final long n, final long n2) {
        this.h.d(2);
        this.h.a(2, n + n2);
    }
    
    private void a(final am am, final float n, final boolean b, final boolean b2) throws p {
        if (b) {
            if (b2) {
                this.y.a(1);
            }
            this.x = this.x.a(am);
        }
        this.a(am.b);
        for (final ar ar : this.a) {
            if (ar != null) {
                ar.a(n, am.b);
            }
        }
    }
    
    private void a(final am am, final boolean b) throws p {
        this.a(am, am.b, true, b);
    }
    
    private void a(final ar ar) throws p {
        if (ar.d_() == 2) {
            ar.l();
        }
    }
    
    private void a(final ar ar, final long n) {
        ar.i();
        if (ar instanceof com.applovin.exoplayer2.i.m) {
            ((com.applovin.exoplayer2.i.m)ar).c(n);
        }
    }
    
    private void a(final av w) {
        this.w = w;
    }
    
    private void a(final ba ba, final ba ba2) {
        if (ba.d() && ba2.d()) {
            return;
        }
        for (int i = this.p.size() - 1; i >= 0; --i) {
            if (!a(this.p.get(i), ba, ba2, this.E, this.F, this.k, this.l)) {
                this.p.get(i).a.a(false);
                this.p.remove(i);
            }
        }
        Collections.sort(this.p);
    }
    
    private void a(final ba ba, final com.applovin.exoplayer2.h.p.a a, final ba ba2, final com.applovin.exoplayer2.h.p.a a2, final long n) {
        if (!ba.d() && this.a(ba, a)) {
            ba.a(ba.a(a.a, this.l).c, this.k);
            this.u.a((ab.e)ai.a((Object)this.k.l));
            if (n != -9223372036854775807L) {
                this.u.a(this.a(ba, a.a, n));
            }
            else {
                final Object b = this.k.b;
                Object b2;
                if (!ba2.d()) {
                    b2 = ba2.a(ba2.a(a2.a, this.l).c, this.k).b;
                }
                else {
                    b2 = null;
                }
                if (!ai.a(b2, b)) {
                    this.u.a(-9223372036854775807L);
                }
            }
            return;
        }
        final float b3 = this.o.d().b;
        final am n2 = this.x.n;
        if (b3 != n2.b) {
            this.o.a(n2);
        }
    }
    
    private static void a(final ba ba, final c c, final ba.c c2, final ba.a a) {
        final int q = ba.a(ba.a(c.d, a).c, c2).q;
        final Object b = ba.a(q, a, true).b;
        final long d = a.d;
        long n;
        if (d != -9223372036854775807L) {
            n = d - 1L;
        }
        else {
            n = Long.MAX_VALUE;
        }
        c.a(q, n, b);
    }
    
    private void a(final ba ba, boolean n) throws p {
        Object o = a(ba, this.x, this.K, this.s, this.E, this.F, this.k, this.l);
        final com.applovin.exoplayer2.h.p.a a = ((f)o).a;
        final long c = ((f)o).c;
        final boolean d = ((f)o).d;
        long n2 = ((f)o).b;
        final boolean b = !this.x.b.equals(a) || n2 != this.x.s;
        int n3 = 3;
        final long n4 = -9223372036854775807L;
        try {
            if (((f)o).e) {
                if (this.x.e != 1) {
                    this.b(4);
                }
                this.a(false, false, false, true);
            }
            Label_0191: {
                if (b) {
                    break Label_0191;
                }
                final af s = this.s;
                final long l = this.L;
                final long t = this.t();
                long a2 = n2;
                try {
                    if (!s.a(ba, l, t)) {
                        this.f(false);
                        a2 = n2;
                    }
                    ad ad = null;
                    Label_0211: {
                        while (true) {
                            while (true) {
                                while (true) {
                                    while (true) {
                                        final al x = this.x;
                                        final ba a3 = x.a;
                                        final com.applovin.exoplayer2.h.p.a b2 = x.b;
                                        if (((f)o).f) {
                                            n2 = a2;
                                        }
                                        else {
                                            n2 = -9223372036854775807L;
                                        }
                                        this.a(ba, a, a3, b2, n2);
                                        if (b || c != this.x.c) {
                                            o = this.x;
                                            final Object a4 = ((al)o).b.a;
                                            o = ((al)o).a;
                                            if (b && n != 0 && !((ba)o).d() && !((ba)o).a(a4, this.l).f) {
                                                n = 1;
                                            }
                                            else {
                                                n = 0;
                                            }
                                            n2 = this.x.d;
                                            if (ba.c(a4) == -1) {
                                                n3 = 4;
                                            }
                                            this.x = this.a(a, a2, c, n2, (boolean)(n != 0), n3);
                                        }
                                        this.A();
                                        this.a(ba, this.x.a);
                                        this.x = this.x.a(ba);
                                        if (!ba.d()) {
                                            this.K = null;
                                        }
                                        this.h(false);
                                        return;
                                        ad = ad.g();
                                        break Label_0211;
                                        ad = this.s.c();
                                        break Label_0211;
                                        Label_0265: {
                                            a2 = this.a(a, n2, d);
                                        }
                                        continue;
                                    }
                                    ad.f = this.s.a(ba, ad.f);
                                    ad.j();
                                    continue;
                                }
                                a2 = n2;
                                iftrue(Label_0277:)(ba.d());
                                continue;
                            }
                            iftrue(Label_0255:)(!ad.f.a.equals(a));
                            continue;
                        }
                    }
                    iftrue(Label_0265:)(ad == null);
                }
                finally {}
            }
        }
        finally {}
        final al x2 = this.x;
        final ba a5 = x2.a;
        final com.applovin.exoplayer2.h.p.a b3 = x2.b;
        long n5 = n4;
        if (((f)o).f) {
            n5 = n2;
        }
        this.a(ba, a, a5, b3, n5);
        if (b || c != this.x.c) {
            final al x3 = this.x;
            final Object a6 = x3.b.a;
            final ba a7 = x3.a;
            final boolean b4 = b && n != 0 && !a7.d() && !a7.a(a6, this.l).f;
            final long d2 = this.x.d;
            if (ba.c(a6) == -1) {
                n3 = 4;
            }
            this.x = this.a(a, n2, c, d2, b4, n3);
        }
        this.A();
        this.a(ba, this.x.a);
        this.x = this.x.a(ba);
        if (!ba.d()) {
            this.K = null;
        }
        this.h(false);
    }
    
    private void a(final Supplier<Boolean> supplier, final long n) {
        synchronized (this) {
            final long a = this.q.a();
            boolean b = false;
            for (long n2 = n; !supplier.get() && n2 > 0L; n2 = a + n - this.q.a()) {
                try {
                    this.q.c();
                    this.wait(n2);
                }
                catch (final InterruptedException ex) {
                    b = true;
                }
            }
            if (b) {
                Thread.currentThread().interrupt();
            }
        }
    }
    
    private void a(final com.applovin.exoplayer2.h.ad ad, final k k) {
        this.f.a(this.a, ad, k.c);
    }
    
    private void a(final com.applovin.exoplayer2.h.z z) throws p {
        this.y.a(1);
        this.a(this.t.a(z), false);
    }
    
    private void a(final a a) throws p {
        this.y.a(1);
        if (a.c != -1) {
            this.K = new g(new ap(a.a, a.b), a.c, a.d);
        }
        this.a(this.t.a(a.a, a.b), false);
    }
    
    private void a(final a a, final int n) throws p {
        this.y.a(1);
        final ah t = this.t;
        int b = n;
        if (n == -1) {
            b = t.b();
        }
        this.a(t.a(b, a.a, a.b), false);
    }
    
    private void a(final b b) throws p {
        this.y.a(1);
        this.a(this.t.a(b.a, b.b, b.c, b.d), false);
    }
    
    private void a(final g k) throws p {
        final d y = this.y;
        int n = 1;
        y.a(1);
        final Pair<Object, Long> a = a(this.x.a, k, true, this.E, this.F, this.k, this.l);
        com.applovin.exoplayer2.h.p.a a3 = null;
        int n3 = 0;
        long n5 = 0L;
        long n6 = 0L;
        Label_0278: {
            long n2;
            long n4;
            if (a == null) {
                final Pair<com.applovin.exoplayer2.h.p.a, Long> a2 = this.a(this.x.a);
                a3 = (com.applovin.exoplayer2.h.p.a)a2.first;
                n2 = (long)a2.second;
                n3 = ((this.x.a.d() ^ true) ? 1 : 0);
                n4 = -9223372036854775807L;
            }
            else {
                final Object first = a.first;
                n2 = (long)a.second;
                if (k.c == -9223372036854775807L) {
                    n4 = -9223372036854775807L;
                }
                else {
                    n4 = n2;
                }
                a3 = this.s.a(this.x.a, first, n2);
                if (a3.a()) {
                    this.x.a.a(a3.a, this.l);
                    long f;
                    if (this.l.b(a3.b) == a3.c) {
                        f = this.l.f();
                    }
                    else {
                        f = 0L;
                    }
                    n5 = n4;
                    n3 = 1;
                    n6 = f;
                    break Label_0278;
                }
                if (k.c == -9223372036854775807L) {
                    n3 = 1;
                }
                else {
                    n3 = 0;
                }
            }
            n5 = n4;
            n6 = n2;
            try {
                Label_0555: {
                    long a5 = 0L;
                    Label_0522: {
                        if (this.x.a.d()) {
                            this.K = k;
                        }
                        else if (a == null) {
                            if (this.x.e != 1) {
                                this.b(4);
                            }
                            this.a(false, true, false, true);
                        }
                        else {
                            long n7 = 0L;
                            Label_0477: {
                                if (a3.equals(this.x.b)) {
                                    final ad c = this.s.c();
                                    long a4;
                                    if (c != null && c.d && n6 != 0L) {
                                        a4 = c.a.a(n6, this.w);
                                    }
                                    else {
                                        a4 = n6;
                                    }
                                    n7 = a4;
                                    if (com.applovin.exoplayer2.h.a(a4) == com.applovin.exoplayer2.h.a(this.x.s)) {
                                        final al x = this.x;
                                        final int e = x.e;
                                        if (e != 2) {
                                            n7 = a4;
                                            if (e != 3) {
                                                break Label_0477;
                                            }
                                        }
                                        final long s = x.s;
                                        this.x = this.a(a3, s, n5, s, (boolean)(n3 != 0), 2);
                                        return;
                                    }
                                }
                                else {
                                    n7 = n6;
                                }
                            }
                            a5 = this.a(a3, n7, this.x.e == 4);
                            if (n6 != a5) {
                                break Label_0522;
                            }
                            n = 0;
                            break Label_0522;
                        }
                        a5 = n6;
                        break Label_0555;
                    }
                    n3 |= n;
                    try {
                        final al x2 = this.x;
                        final ba a6 = x2.a;
                        this.a(a6, a3, a6, x2.b, n5);
                        this.x = this.a(a3, a5, n5, a5, (boolean)(n3 != 0), 2);
                        return;
                    }
                    finally {
                        n6 = a5;
                    }
                }
            }
            finally {}
        }
        this.x = this.a(a3, n6, n5, n6, (boolean)(n3 != 0), 2);
    }
    
    private void a(final IOException ex, final int n) {
        final p a = com.applovin.exoplayer2.p.a(ex, n);
        final ad c = this.s.c();
        p a2 = a;
        if (c != null) {
            a2 = a.a(c.f.a);
        }
        com.applovin.exoplayer2.l.q.c("ExoPlayerImplInternal", "Playback error", (Throwable)a2);
        this.a(false, false);
        this.x = this.x.a(a2);
    }
    
    private void a(final boolean b, int e, final boolean b2, final int n) throws p {
        this.y.a(b2 ? 1 : 0);
        this.y.c(n);
        this.x = this.x.a(b, e);
        this.C = false;
        this.b(b);
        if (!this.J()) {
            this.j();
            this.l();
        }
        else {
            e = this.x.e;
            if (e == 3) {
                this.i();
                this.h.c(2);
            }
            else if (e == 2) {
                this.h.c(2);
            }
        }
    }
    
    private void a(final boolean g, @Nullable final AtomicBoolean atomicBoolean) {
        if (this.G != g && !(this.G = g)) {
            for (final ar ar : this.a) {
                if (!c(ar) && this.b.remove(ar)) {
                    ar.n();
                }
            }
        }
        if (atomicBoolean != null) {
            synchronized (this) {
                atomicBoolean.set(true);
                this.notifyAll();
            }
        }
    }
    
    private void a(final boolean b, final boolean b2) {
        this.a(b || !this.G, false, true, false);
        this.y.a(b2 ? 1 : 0);
        this.f.b();
        this.b(1);
    }
    
    private void a(final boolean b, final boolean b2, final boolean b3, final boolean b4) {
        this.h.d(2);
        final p p4 = null;
        this.O = null;
        this.C = false;
        this.o.b();
        this.L = 0L;
        for (final ar ar : this.a) {
            Label_0090: {
                try {
                    this.b(ar);
                    break Label_0090;
                }
                catch (final RuntimeException ar) {}
                catch (final p p5) {}
                com.applovin.exoplayer2.l.q.c("ExoPlayerImplInternal", "Disable failed.", (Throwable)ar);
            }
        }
        if (b) {
            for (final ar ar2 : this.a) {
                if (this.b.remove(ar2)) {
                    try {
                        ar2.n();
                    }
                    catch (final RuntimeException ex) {
                        com.applovin.exoplayer2.l.q.c("ExoPlayerImplInternal", "Reset failed.", (Throwable)ex);
                    }
                }
            }
        }
        this.J = 0;
        final al x = this.x;
        com.applovin.exoplayer2.h.p.a b5 = x.b;
        long s = x.s;
        long n;
        if (!this.x.b.a() && !a(this.x, this.l)) {
            n = this.x.s;
        }
        else {
            n = this.x.c;
        }
        long longValue = 0L;
        boolean b6 = false;
        Label_0347: {
            if (b2) {
                this.K = null;
                final Pair<com.applovin.exoplayer2.h.p.a, Long> a3 = this.a(this.x.a);
                final com.applovin.exoplayer2.h.p.a a4 = (com.applovin.exoplayer2.h.p.a)a3.first;
                longValue = (long)a3.second;
                final boolean equals = a4.equals(this.x.b);
                final long n2 = -9223372036854775807L;
                b5 = a4;
                s = longValue;
                n = n2;
                if (!equals) {
                    b6 = true;
                    b5 = a4;
                    n = n2;
                    break Label_0347;
                }
            }
            b6 = false;
            longValue = s;
        }
        this.s.g();
        this.D = false;
        final al x2 = this.x;
        final ba a5 = x2.a;
        final int e = x2.e;
        p f;
        if (b4) {
            f = p4;
        }
        else {
            f = x2.f;
        }
        com.applovin.exoplayer2.h.ad ad;
        if (b6) {
            ad = com.applovin.exoplayer2.h.ad.a;
        }
        else {
            ad = x2.h;
        }
        k k;
        if (b6) {
            k = this.e;
        }
        else {
            k = x2.i;
        }
        Object o;
        if (b6) {
            o = com.applovin.exoplayer2.common.a.s.g();
        }
        else {
            o = x2.j;
        }
        final al x3 = this.x;
        this.x = new al(a5, b5, n, longValue, e, f, false, ad, k, (List<com.applovin.exoplayer2.g.a>)o, b5, x3.l, x3.m, x3.n, longValue, 0L, longValue, this.I, false);
        if (b3) {
            this.t.c();
        }
    }
    
    private void a(final boolean[] array) throws p {
        final ad d = this.s.d();
        final k i = d.i();
        final int n = 0;
        int n2 = 0;
        int j;
        while (true) {
            j = n;
            if (n2 >= this.a.length) {
                break;
            }
            if (!i.a(n2) && this.b.remove(this.a[n2])) {
                this.a[n2].n();
            }
            ++n2;
        }
        while (j < this.a.length) {
            if (i.a(j)) {
                this.a(j, array[j]);
            }
            ++j;
        }
        d.g = true;
    }
    
    private static boolean a(final al al, final ba.a a) {
        final com.applovin.exoplayer2.h.p.a b = al.b;
        final ba a2 = al.a;
        return a2.d() || a2.a(b.a, a).f;
    }
    
    private boolean a(final ar ar, final ad ad) {
        final ad g = ad.g();
        return ad.f.f && g.d && (ar instanceof com.applovin.exoplayer2.i.m || ar.h() >= g.b());
    }
    
    private boolean a(final ba ba, final com.applovin.exoplayer2.h.p.a a) {
        final boolean a2 = a.a();
        boolean b2;
        final boolean b = b2 = false;
        if (!a2) {
            if (ba.d()) {
                b2 = b;
            }
            else {
                ba.a(ba.a(a.a, this.l).c, this.k);
                b2 = b;
                if (this.k.e()) {
                    final ba.c k = this.k;
                    b2 = b;
                    if (k.j) {
                        b2 = b;
                        if (k.g != -9223372036854775807L) {
                            b2 = true;
                        }
                    }
                }
            }
        }
        return b2;
    }
    
    private static boolean a(final c c, final ba ba, final ba ba2, int c2, final boolean b, final ba.c c3, final ba.a a) {
        final Object d = c.d;
        if (d == null) {
            long b2;
            if (c.a.f() == Long.MIN_VALUE) {
                b2 = -9223372036854775807L;
            }
            else {
                b2 = h.b(c.a.f());
            }
            final Pair<Object, Long> a2 = a(ba, new g(c.a.a(), c.a.g(), b2), false, c2, b, c3, a);
            if (a2 == null) {
                return false;
            }
            c.a(ba.c(a2.first), (long)a2.second, a2.first);
            if (c.a.f() == Long.MIN_VALUE) {
                a(ba, c, c3, a);
            }
            return true;
        }
        else {
            c2 = ba.c(d);
            if (c2 == -1) {
                return false;
            }
            if (c.a.f() == Long.MIN_VALUE) {
                a(ba, c, c3, a);
                return true;
            }
            c.b = c2;
            ba2.a(c.d, a);
            if (a.f && ba2.a(a.c, c3).p == ba2.c(c.d)) {
                final Pair<Object, Long> a3 = ba.a(c3, a, ba.a(c.d, a).c, c.c + a.c());
                c.a(ba.c(a3.first), (long)a3.second, a3.first);
            }
            return true;
        }
    }
    
    private static v[] a(final com.applovin.exoplayer2.j.d d) {
        int i = 0;
        int e;
        if (d != null) {
            e = ((com.applovin.exoplayer2.j.g)d).e();
        }
        else {
            e = 0;
        }
        final v[] array = new v[e];
        while (i < e) {
            array[i] = ((com.applovin.exoplayer2.j.g)d).a(i);
            ++i;
        }
        return array;
    }
    
    private void b(final int n) {
        final al x = this.x;
        if (x.e != n) {
            this.x = x.a(n);
        }
    }
    
    private void b(final int n, final int n2, final com.applovin.exoplayer2.h.z z) throws p {
        this.y.a(1);
        this.a(this.t.a(n, n2, z), false);
    }
    
    private void b(long a) throws p {
        final ad c = this.s.c();
        if (c != null) {
            a = c.a(a);
        }
        this.L = a;
        this.o.a(a);
        for (final ar ar : this.a) {
            if (c(ar)) {
                ar.a(this.L);
            }
        }
        this.r();
    }
    
    private void b(final am am) throws p {
        this.o.a(am);
        this.a(this.o.d(), true);
    }
    
    private void b(final ao ao) throws p {
        if (ao.f() == -9223372036854775807L) {
            this.c(ao);
        }
        else if (this.x.a.d()) {
            this.p.add(new c(ao));
        }
        else {
            final c e = new c(ao);
            final ba a = this.x.a;
            if (a(e, a, a, this.E, this.F, this.k, this.l)) {
                this.p.add(e);
                Collections.sort(this.p);
            }
            else {
                ao.a(false);
            }
        }
    }
    
    private void b(final ar ar) throws p {
        if (!c(ar)) {
            return;
        }
        this.o.b(ar);
        this.a(ar);
        ar.m();
        --this.J;
    }
    
    private void b(final boolean b) {
        for (ad ad = this.s.c(); ad != null; ad = ad.g()) {
            for (final com.applovin.exoplayer2.j.d d : ad.i().c) {
                if (d != null) {
                    d.a(b);
                }
            }
        }
    }
    
    private boolean b(final long n, final long n2) {
        if (this.I && this.H) {
            return false;
        }
        this.a(n, n2);
        return true;
    }
    
    private void c(final int e) throws p {
        this.E = e;
        if (!this.s.a(this.x.a, e)) {
            this.f(true);
        }
        this.h(false);
    }
    
    private void c(final long n) {
        for (final ar ar : this.a) {
            if (ar.f() != null) {
                this.a(ar, n);
            }
        }
    }
    
    private void c(long n, final long n2) throws p {
        if (!this.p.isEmpty()) {
            if (!this.x.b.a()) {
                long n3 = n;
                if (this.N) {
                    n3 = n - 1L;
                    this.N = false;
                }
                final al x = this.x;
                final int c = x.a.c(x.b.a);
                int min;
                final int n4 = min = Math.min(this.M, this.p.size());
                long n5 = n3;
            Label_0254_Outer:
                while (true) {
                    Label_0125: {
                        if (n4 <= 0) {
                            break Label_0125;
                        }
                        c c2 = this.p.get(n4 - 1);
                        min = n4;
                        n = n3;
                        while (c2 != null) {
                            final int b = c2.b;
                            if (b <= c && (b != c || c2.c <= n)) {
                                break;
                            }
                            final int n6 = --min;
                            n5 = n;
                            if (n6 <= 0) {
                                break Label_0125;
                            }
                            c2 = this.p.get(n6 - 1);
                            min = n6;
                        }
                        int n7 = min;
                        long n8 = n;
                        while (true) {
                            Label_0244: {
                                if (min >= this.p.size()) {
                                    break Label_0244;
                                }
                                c c3 = this.p.get(min);
                                int n9;
                                c c4;
                                while (true) {
                                    n9 = min;
                                    c4 = c3;
                                    if (c3 == null) {
                                        break;
                                    }
                                    n9 = min;
                                    c4 = c3;
                                    if (c3.d == null) {
                                        break;
                                    }
                                    final int b2 = c3.b;
                                    if (b2 >= c) {
                                        n9 = min;
                                        c4 = c3;
                                        if (b2 != c) {
                                            break;
                                        }
                                        n9 = min;
                                        c4 = c3;
                                        if (c3.c > n) {
                                            break;
                                        }
                                    }
                                    n7 = ++min;
                                    n8 = n;
                                    if (min >= this.p.size()) {
                                        break Label_0244;
                                    }
                                    c3 = this.p.get(min);
                                }
                                while (c4 != null && c4.d != null && c4.b == c) {
                                    final long c5 = c4.c;
                                    if (c5 > n && c5 <= n2) {
                                        try {
                                            this.c(c4.a);
                                            if (!c4.a.h() && !c4.a.j()) {
                                                ++n9;
                                            }
                                            else {
                                                this.p.remove(n9);
                                            }
                                            if (n9 < this.p.size()) {
                                                c4 = this.p.get(n9);
                                                continue Label_0254_Outer;
                                            }
                                            c4 = null;
                                            continue Label_0254_Outer;
                                        }
                                        finally {
                                            if (c4.a.h() || c4.a.j()) {
                                                this.p.remove(n9);
                                            }
                                        }
                                        break;
                                    }
                                    break;
                                }
                                this.M = n9;
                                return;
                            }
                            c c3 = null;
                            n = n8;
                            min = n7;
                            continue;
                        }
                    }
                    c c2 = null;
                    n = n5;
                    continue;
                }
            }
        }
    }
    
    private void c(final ao ao) throws p {
        if (ao.e() == this.j) {
            this.e(ao);
            final int e = this.x.e;
            if (e == 3 || e == 2) {
                this.h.c(2);
            }
        }
        else {
            this.h.a(15, (Object)ao).a();
        }
    }
    
    private void c(final n n) throws p {
        if (!this.s.a(n)) {
            return;
        }
        final ad b = this.s.b();
        b.a(this.o.d().b, this.x.a);
        this.a(b.h(), b.i());
        if (b == this.s.c()) {
            this.b(b.f.b);
            this.H();
            final al x = this.x;
            final com.applovin.exoplayer2.h.p.a b2 = x.b;
            final long b3 = b.f.b;
            this.x = this.a(b2, b3, x.c, b3, false, 5);
        }
        this.D();
    }
    
    private void c(final boolean a) throws p {
        this.A = a;
        this.A();
        if (this.B && this.s.d() != this.s.c()) {
            this.f(true);
            this.h(false);
        }
    }
    
    private static boolean c(final ar ar) {
        return ar.d_() != 0;
    }
    
    private long d(final long n) {
        final ad b = this.s.b();
        if (b == null) {
            return 0L;
        }
        return Math.max(0L, n - b.b(this.L));
    }
    
    private void d(final ao ao) {
        final Looper e = ao.e();
        if (!e.getThread().isAlive()) {
            com.applovin.exoplayer2.l.q.c("TAG", "Trying to send message on a dead thread.");
            ao.a(false);
            return;
        }
        this.q.a(e, (Handler$Callback)null).a((Runnable)new \u30070(this, ao));
    }
    
    private void d(final n n) {
        if (!this.s.a(n)) {
            return;
        }
        this.s.a(this.L);
        this.D();
    }
    
    private void d(final boolean i) {
        if (i == this.I) {
            return;
        }
        this.I = i;
        final al x = this.x;
        final int e = x.e;
        if (!i && e != 4 && e != 1) {
            this.h.c(2);
        }
        else {
            this.x = x.b(i);
        }
    }
    
    private void e(final ao ao) throws p {
        if (ao.j()) {
            return;
        }
        try {
            ao.b().a(ao.c(), ao.d());
        }
        finally {
            ao.a(true);
        }
    }
    
    private void e(final boolean f) throws p {
        this.F = f;
        if (!this.s.a(this.x.a, f)) {
            this.f(true);
        }
        this.h(false);
    }
    
    private void f() {
        this.y.a(this.x);
        if (this.y.g) {
            this.r.onPlaybackInfoUpdate(this.y);
            this.y = new d(this.x);
        }
    }
    
    private void f(final boolean b) throws p {
        final com.applovin.exoplayer2.h.p.a a = this.s.c().f.a;
        final long a2 = this.a(a, this.x.s, true, false);
        if (a2 != this.x.s) {
            final al x = this.x;
            this.x = this.a(a, a2, x.c, x.d, b, 5);
        }
    }
    
    private void g() {
        this.y.a(1);
        this.a(false, false, false, true);
        this.f.a();
        int n;
        if (this.x.a.d()) {
            n = 4;
        }
        else {
            n = 2;
        }
        this.b(n);
        this.t.a(this.g.a());
        this.h.c(2);
    }
    
    private boolean g(final boolean b) {
        if (this.J == 0) {
            return this.s();
        }
        final boolean b2 = false;
        if (!b) {
            return false;
        }
        final al x = this.x;
        if (!x.g) {
            return true;
        }
        long b3;
        if (this.a(x.a, this.s.c().f.a)) {
            b3 = this.u.b();
        }
        else {
            b3 = -9223372036854775807L;
        }
        final ad b4 = this.s.b();
        final boolean b5 = b4.c() && b4.f.i;
        final boolean b6 = b4.f.a.a() && !b4.d;
        if (!b5 && !b6) {
            final boolean b7 = b2;
            if (!this.f.a(this.I(), this.o.d().b, this.C, b3)) {
                return b7;
            }
        }
        return true;
    }
    
    private void h() throws p {
        this.a(this.t.d(), true);
    }
    
    private void h(final boolean b) {
        final ad b2 = this.s.b();
        com.applovin.exoplayer2.h.p.a a;
        if (b2 == null) {
            a = this.x.b;
        }
        else {
            a = b2.f.a;
        }
        final boolean b3 = this.x.k.equals(a) ^ true;
        if (b3) {
            this.x = this.x.a(a);
        }
        final al x = this.x;
        long q;
        if (b2 == null) {
            q = x.s;
        }
        else {
            q = b2.d();
        }
        x.q = q;
        this.x.r = this.I();
        if ((b3 || b) && b2 != null && b2.d) {
            this.a(b2.h(), b2.i());
        }
    }
    
    private void i() throws p {
        int i = 0;
        this.C = false;
        this.o.a();
        for (ar[] a = this.a; i < a.length; ++i) {
            final ar ar = a[i];
            if (c(ar)) {
                ar.e();
            }
        }
    }
    
    private void j() throws p {
        this.o.b();
        for (final ar ar : this.a) {
            if (c(ar)) {
                this.a(ar);
            }
        }
    }
    
    private void k() throws p {
        this.f(true);
    }
    
    private void l() throws p {
        final ad c = this.s.c();
        if (c == null) {
            return;
        }
        long c2;
        if (c.d) {
            c2 = c.a.c();
        }
        else {
            c2 = -9223372036854775807L;
        }
        if (c2 != -9223372036854775807L) {
            this.b(c2);
            if (c2 != this.x.s) {
                final al x = this.x;
                this.x = this.a(x.b, c2, x.c, c2, true, 5);
            }
        }
        else {
            final long a = this.o.a(c != this.s.d());
            this.L = a;
            final long b = c.b(a);
            this.c(this.x.s, b);
            this.x.s = b;
        }
        this.x.q = this.s.b().d();
        this.x.r = this.I();
        final al x2 = this.x;
        if (x2.l && x2.e == 3 && this.a(x2.a, x2.b) && this.x.n.b == 1.0f) {
            final float a2 = this.u.a(this.o(), this.I());
            if (this.o.d().b != a2) {
                this.o.a(this.x.n.a(a2));
                this.a(this.x.n, this.o.d().b, false, false);
            }
        }
    }
    
    private void m() {
        for (ad ad = this.s.c(); ad != null; ad = ad.g()) {
            for (final com.applovin.exoplayer2.j.d d : ad.i().c) {
                if (d != null) {
                    d.h();
                }
            }
        }
    }
    
    private void n() throws p, IOException {
        final long b = this.q.b();
        this.u();
        final int e = this.x.e;
        if (e == 1 || e == 4) {
            this.h.d(2);
            return;
        }
        final ad c = this.s.c();
        if (c == null) {
            this.a(b, 10L);
            return;
        }
        com.applovin.exoplayer2.l.ah.a("doSomeWork");
        this.l();
        int n4;
        int n5;
        if (c.d) {
            final long elapsedRealtime = SystemClock.elapsedRealtime();
            c.a.a(this.x.s - this.m, this.n);
            int n = 0;
            int n2 = 1;
            int n3 = 1;
            while (true) {
                final ar[] a = this.a;
                n4 = n2;
                n5 = n3;
                if (n >= a.length) {
                    break;
                }
                final ar ar = a[n];
                int n6;
                boolean b2;
                if (!c(ar)) {
                    n6 = n2;
                    b2 = (n3 != 0);
                }
                else {
                    ar.a(this.L, elapsedRealtime * 1000L);
                    int n7;
                    if (n2 != 0 && ar.A()) {
                        n7 = 1;
                    }
                    else {
                        n7 = 0;
                    }
                    final boolean b3 = c.c[n] != ar.f();
                    final boolean b4 = !b3 && ar.g();
                    final boolean b5 = b3 || b4 || ar.z() || ar.A();
                    final boolean b6 = n3 && b5;
                    n6 = n7;
                    b2 = b6;
                    if (!b5) {
                        ar.k();
                        b2 = b6;
                        n6 = n7;
                    }
                }
                ++n;
                n2 = n6;
                n3 = (b2 ? 1 : 0);
            }
        }
        else {
            c.a.e_();
            n4 = 1;
            n5 = 1;
        }
        final long e2 = c.f.e;
        final boolean b7 = n4 != 0 && c.d && (e2 == -9223372036854775807L || e2 <= this.x.s);
        if (b7 && this.B) {
            this.a(this.B = false, this.x.m, false, 5);
        }
        Label_0576: {
            if (b7 && c.f.i) {
                this.b(4);
                this.j();
            }
            else if (this.x.e == 2 && this.g((boolean)(n5 != 0))) {
                this.b(3);
                this.O = null;
                if (this.J()) {
                    this.i();
                }
            }
            else if (this.x.e == 3) {
                if (this.J == 0) {
                    if (this.s()) {
                        break Label_0576;
                    }
                }
                else if (n5 != 0) {
                    break Label_0576;
                }
                this.C = this.J();
                this.b(2);
                if (this.C) {
                    this.m();
                    this.u.a();
                }
                this.j();
            }
        }
        if (this.x.e == 2) {
            int n8 = 0;
            while (true) {
                final ar[] a2 = this.a;
                if (n8 >= a2.length) {
                    break;
                }
                if (c(a2[n8]) && this.a[n8].f() == c.c[n8]) {
                    this.a[n8].k();
                }
                ++n8;
            }
            final al x = this.x;
            if (!x.g && x.r < 500000L) {
                if (this.F()) {
                    throw new IllegalStateException("Playback stuck buffering and not loading");
                }
            }
        }
        final boolean i = this.I;
        final al x2 = this.x;
        if (i != x2.o) {
            this.x = x2.b(i);
        }
        boolean b8 = false;
        Label_0814: {
            if (!this.J() || this.x.e != 3) {
                final int e3 = this.x.e;
                if (e3 != 2) {
                    if (this.J != 0 && e3 != 4) {
                        this.a(b, 1000L);
                    }
                    else {
                        this.h.d(2);
                    }
                    b8 = false;
                    break Label_0814;
                }
            }
            b8 = (this.b(b, 10L) ^ true);
        }
        final al x3 = this.x;
        if (x3.p != b8) {
            this.x = x3.c(b8);
        }
        this.H = false;
        com.applovin.exoplayer2.l.ah.a();
    }
    
    private long o() {
        final al x = this.x;
        return this.a(x.a, x.b.a, x.s);
    }
    
    private void p() {
        this.a(true, false, true, false);
        this.f.c();
        this.b(1);
        this.i.quit();
        synchronized (this) {
            this.z = true;
            this.notifyAll();
        }
    }
    
    private void q() throws p {
        final float b = this.o.d().b;
        ad ad = this.s.c();
        final ad d = this.s.d();
        int n = 1;
        while (ad != null && ad.d) {
            final k b2 = ad.b(b, this.x.a);
            if (!b2.a(ad.i())) {
                if (n != 0) {
                    final ad c = this.s.c();
                    final boolean a = this.s.a(c);
                    final boolean[] array = new boolean[this.a.length];
                    final long a2 = c.a(b2, this.x.s, a, array);
                    final al x = this.x;
                    final boolean b3 = x.e != 4 && a2 != x.s;
                    final al x2 = this.x;
                    this.x = this.a(x2.b, a2, x2.c, x2.d, b3, 5);
                    if (b3) {
                        this.b(a2);
                    }
                    final boolean[] array2 = new boolean[this.a.length];
                    int n2 = 0;
                    while (true) {
                        final ar[] a3 = this.a;
                        if (n2 >= a3.length) {
                            break;
                        }
                        final ar ar = a3[n2];
                        final boolean c2 = c(ar);
                        array2[n2] = c2;
                        final x x3 = c.c[n2];
                        if (c2) {
                            if (x3 != ar.f()) {
                                this.b(ar);
                            }
                            else if (array[n2]) {
                                ar.a(this.L);
                            }
                        }
                        ++n2;
                    }
                    this.a(array2);
                }
                else {
                    this.s.a(ad);
                    if (ad.d) {
                        ad.a(b2, Math.max(ad.f.b, ad.b(this.L)), false);
                    }
                }
                this.h(true);
                if (this.x.e != 4) {
                    this.D();
                    this.l();
                    this.h.c(2);
                }
                return;
            }
            if (ad == d) {
                n = 0;
            }
            ad = ad.g();
        }
    }
    
    private void r() {
        for (ad ad = this.s.c(); ad != null; ad = ad.g()) {
            for (final com.applovin.exoplayer2.j.d d : ad.i().c) {
                if (d != null) {
                    d.g();
                }
            }
        }
    }
    
    private boolean s() {
        final ad c = this.s.c();
        final long e = c.f.e;
        return c.d && (e == -9223372036854775807L || this.x.s < e || !this.J());
    }
    
    private long t() {
        final ad d = this.s.d();
        if (d == null) {
            return 0L;
        }
        long a = d.a();
        if (!d.d) {
            return a;
        }
        int n = 0;
        while (true) {
            final ar[] a2 = this.a;
            if (n >= a2.length) {
                return a;
            }
            long max = a;
            if (c(a2[n])) {
                if (this.a[n].f() != d.c[n]) {
                    max = a;
                }
                else {
                    final long h = this.a[n].h();
                    if (h == Long.MIN_VALUE) {
                        return Long.MIN_VALUE;
                    }
                    max = Math.max(h, a);
                }
            }
            ++n;
            a = max;
        }
    }
    
    private void u() throws p, IOException {
        if (!this.x.a.d()) {
            if (this.t.a()) {
                this.v();
                this.w();
                this.x();
                this.z();
            }
        }
    }
    
    private void v() throws p {
        this.s.a(this.L);
        if (this.s.a()) {
            final ae a = this.s.a(this.L, this.x);
            if (a != null) {
                final ad a2 = this.s.a(this.c, this.d, this.f.d(), this.t, a, this.e);
                a2.a.a((n.a)this, a.b);
                if (this.s.c() == a2) {
                    this.b(a2.b());
                }
                this.h(false);
            }
        }
        if (this.D) {
            this.D = this.F();
            this.G();
        }
        else {
            this.D();
        }
    }
    
    private void w() {
        final ad d = this.s.d();
        if (d == null) {
            return;
        }
        final ad g = d.g();
        final int n = 0;
        if (g == null || this.B) {
            int n2 = n;
            if (!d.f.i) {
                if (!this.B) {
                    return;
                }
                n2 = n;
            }
            while (true) {
                final ar[] a = this.a;
                if (n2 >= a.length) {
                    break;
                }
                final ar ar = a[n2];
                final x x = d.c[n2];
                if (x != null && ar.f() == x && ar.g()) {
                    final long e = d.f.e;
                    long n3;
                    if (e != -9223372036854775807L && e != Long.MIN_VALUE) {
                        n3 = d.a() + d.f.e;
                    }
                    else {
                        n3 = -9223372036854775807L;
                    }
                    this.a(ar, n3);
                }
                ++n2;
            }
            return;
        }
        if (!this.C()) {
            return;
        }
        if (!d.g().d && this.L < d.g().b()) {
            return;
        }
        final k i = d.i();
        final ad e2 = this.s.e();
        final k j = e2.i();
        if (e2.d && e2.a.c() != -9223372036854775807L) {
            this.c(e2.b());
            return;
        }
        for (int k = 0; k < this.a.length; ++k) {
            final boolean a2 = i.a(k);
            final boolean a3 = j.a(k);
            if (a2 && !this.a[k].j()) {
                final boolean b = this.c[k].a() == -2;
                final at at = i.b[k];
                final at at2 = j.b[k];
                if (!a3 || !at2.equals(at) || b) {
                    this.a(this.a[k], e2.b());
                }
            }
        }
    }
    
    private void x() throws p {
        final ad d = this.s.d();
        if (d != null && this.s.c() != d) {
            if (!d.g) {
                if (this.y()) {
                    this.H();
                }
            }
        }
    }
    
    private boolean y() throws p {
        final ad d = this.s.d();
        final k i = d.i();
        int n = 0;
        boolean b = false;
        while (true) {
            final ar[] a = this.a;
            if (n >= a.length) {
                break;
            }
            final ar ar = a[n];
            if (c(ar)) {
                final boolean b2 = ar.f() != d.c[n];
                if (!i.a(n) || b2) {
                    if (!ar.j()) {
                        ar.a(a(i.c[n]), d.c[n], d.b(), d.a());
                    }
                    else if (ar.A()) {
                        this.b(ar);
                    }
                    else {
                        b = true;
                    }
                }
            }
            ++n;
        }
        return b ^ true;
    }
    
    private void z() throws p {
        int n = 0;
        while (this.B()) {
            if (n != 0) {
                this.f();
            }
            final ad c = this.s.c();
            final ad f = this.s.f();
            final ae f2 = f.f;
            final com.applovin.exoplayer2.h.p.a a = f2.a;
            final long b = f2.b;
            final al a2 = this.a(a, b, f2.c, b, true, 0);
            this.x = a2;
            final ba a3 = a2.a;
            this.a(a3, f.f.a, a3, c.f.a, -9223372036854775807L);
            this.A();
            this.l();
            n = 1;
        }
    }
    
    public void a() {
        this.h.b(0).a();
    }
    
    public void a(final int n) {
        this.h.a(11, n, 0).a();
    }
    
    public void a(final int n, final int n2, final com.applovin.exoplayer2.h.z z) {
        this.h.a(20, n, n2, (Object)z).a();
    }
    
    public void a(final long p) {
        this.P = p;
    }
    
    public void a(final am am) {
        this.h.a(16, (Object)am).a();
    }
    
    public void a(final ao ao) {
        synchronized (this) {
            if (!this.z && ((Thread)this.i).isAlive()) {
                this.h.a(14, (Object)ao).a();
                return;
            }
            com.applovin.exoplayer2.l.q.c("ExoPlayerImplInternal", "Ignoring messages sent after release.");
            ao.a(false);
        }
    }
    
    public void a(final ba ba, final int n, final long n2) {
        this.h.a(3, (Object)new g(ba, n, n2)).a();
    }
    
    public void a(final n n) {
        this.h.a(8, (Object)n).a();
    }
    
    public void a(final List<ah.c> list, final int n, final long n2, final com.applovin.exoplayer2.h.z z) {
        this.h.a(17, (Object)new a((List)list, z, n, n2)).a();
    }
    
    public void a(final boolean b) {
        this.h.a(12, (int)(b ? 1 : 0), 0).a();
    }
    
    public void a(final boolean b, final int n) {
        this.h.a(1, (int)(b ? 1 : 0), n).a();
    }
    
    public void b() {
        this.h.b(6).a();
    }
    
    public void b(final n n) {
        this.h.a(9, (Object)n).a();
    }
    
    public boolean c() {
        synchronized (this) {
            if (!this.z && ((Thread)this.i).isAlive()) {
                this.h.c(7);
                this.a(new o88\u3007OO08\u3007(this), this.v);
                return this.z;
            }
            return true;
        }
    }
    
    public Looper d() {
        return this.j;
    }
    
    public void e() {
        this.h.c(22);
    }
    
    public boolean handleMessage(final Message message) {
        int n = 1000;
        try {
            switch (message.what) {
                default: {
                    return false;
                }
                case 25: {
                    this.k();
                    break;
                }
                case 24: {
                    this.d(message.arg1 == 1);
                    break;
                }
                case 23: {
                    this.c(message.arg1 != 0);
                    break;
                }
                case 22: {
                    this.h();
                    break;
                }
                case 21: {
                    this.a((com.applovin.exoplayer2.h.z)message.obj);
                    break;
                }
                case 20: {
                    this.b(message.arg1, message.arg2, (com.applovin.exoplayer2.h.z)message.obj);
                    break;
                }
                case 19: {
                    this.a((b)message.obj);
                    break;
                }
                case 18: {
                    this.a((a)message.obj, message.arg1);
                    break;
                }
                case 17: {
                    this.a((a)message.obj);
                    break;
                }
                case 16: {
                    this.a((am)message.obj, false);
                    break;
                }
                case 15: {
                    this.d((ao)message.obj);
                    break;
                }
                case 14: {
                    this.b((ao)message.obj);
                    break;
                }
                case 13: {
                    this.a(message.arg1 != 0, (AtomicBoolean)message.obj);
                    break;
                }
                case 12: {
                    this.e(message.arg1 != 0);
                    break;
                }
                case 11: {
                    this.c(message.arg1);
                    break;
                }
                case 10: {
                    this.q();
                    break;
                }
                case 9: {
                    this.d((n)message.obj);
                    break;
                }
                case 8: {
                    this.c((n)message.obj);
                    break;
                }
                case 7: {
                    this.p();
                    return true;
                }
                case 6: {
                    this.a(false, true);
                    break;
                }
                case 5: {
                    this.a((av)message.obj);
                    break;
                }
                case 4: {
                    this.b((am)message.obj);
                    break;
                }
                case 3: {
                    this.a((g)message.obj);
                    break;
                }
                case 2: {
                    this.n();
                    break;
                }
                case 1: {
                    this.a(message.arg1 != 0, message.arg2, true, 1);
                    break;
                }
                case 0: {
                    this.g();
                    break;
                }
            }
        }
        catch (final RuntimeException ex) {
            if (ex instanceof IllegalStateException || ex instanceof IllegalArgumentException) {
                n = 1004;
            }
            final p a = com.applovin.exoplayer2.p.a(ex, n);
            com.applovin.exoplayer2.l.q.c("ExoPlayerImplInternal", "Playback error", (Throwable)a);
            this.a(true, false);
            this.x = this.x.a(a);
        }
        catch (final IOException ex2) {
            this.a(ex2, 2000);
        }
        catch (final com.applovin.exoplayer2.h.b b) {
            this.a(b, 1002);
        }
        catch (final com.applovin.exoplayer2.k.j j) {
            this.a((IOException)j, j.a);
        }
        catch (final com.applovin.exoplayer2.ai ai) {
            final int b2 = ai.b;
            if (b2 == 1) {
                if (ai.a) {
                    n = 3001;
                }
                else {
                    n = 3003;
                }
            }
            else if (b2 == 4) {
                if (ai.a) {
                    n = 3002;
                }
                else {
                    n = 3004;
                }
            }
            this.a(ai, n);
        }
        catch (final com.applovin.exoplayer2.d.f.a a2) {
            this.a(a2, a2.a);
        }
        catch (final p p) {
            p a3 = p;
            if (p.a == 1) {
                final ad d = this.s.d();
                a3 = p;
                if (d != null) {
                    a3 = p.a(d.f.a);
                }
            }
            if (a3.g && this.O == null) {
                com.applovin.exoplayer2.l.q.b("ExoPlayerImplInternal", "Recoverable renderer error", (Throwable)a3);
                this.O = a3;
                final o h = this.h;
                h.a(h.a(25, (Object)a3));
            }
            else {
                final p o = this.O;
                p o2 = a3;
                if (o != null) {
                    o.addSuppressed(a3);
                    o2 = this.O;
                }
                com.applovin.exoplayer2.l.q.c("ExoPlayerImplInternal", "Playback error", (Throwable)o2);
                this.a(true, false);
                this.x = this.x.a(o2);
            }
        }
        this.f();
        return true;
    }
    
    private static final class a
    {
        private final List<ah.c> a;
        private final com.applovin.exoplayer2.h.z b;
        private final int c;
        private final long d;
        
        private a(final List<ah.c> a, final com.applovin.exoplayer2.h.z b, final int c, final long d) {
            this.a = a;
            this.b = b;
            this.c = c;
            this.d = d;
        }
    }
    
    private static class b
    {
        public final int a;
        public final int b;
        public final int c;
        public final com.applovin.exoplayer2.h.z d;
    }
    
    private static final class c implements Comparable<c>
    {
        public final ao a;
        public int b;
        public long c;
        @Nullable
        public Object d;
        
        public c(final ao a) {
            this.a = a;
        }
        
        public int a(final c c) {
            final Object d = this.d;
            final int n = 1;
            if (d == null != (c.d == null)) {
                int n2 = n;
                if (d != null) {
                    n2 = -1;
                }
                return n2;
            }
            if (d == null) {
                return 0;
            }
            final int n3 = this.b - c.b;
            if (n3 != 0) {
                return n3;
            }
            return ai.a(this.c, c.c);
        }
        
        public void a(final int b, final long c, final Object d) {
            this.b = b;
            this.c = c;
            this.d = d;
        }
    }
    
    public static final class d
    {
        public al a;
        public int b;
        public boolean c;
        public int d;
        public boolean e;
        public int f;
        private boolean g;
        
        public d(final al a) {
            this.a = a;
        }
        
        public void a(final int n) {
            this.g |= (n > 0);
            this.b += n;
        }
        
        public void a(final al a) {
            this.g |= (this.a != a);
            this.a = a;
        }
        
        public void b(final int d) {
            final boolean c = this.c;
            boolean b = true;
            if (c && this.d != 5) {
                if (d != 5) {
                    b = false;
                }
                com.applovin.exoplayer2.l.a.a(b);
                return;
            }
            this.g = true;
            this.c = true;
            this.d = d;
        }
        
        public void c(final int f) {
            this.g = true;
            this.e = true;
            this.f = f;
        }
    }
    
    public interface e
    {
        void onPlaybackInfoUpdate(final d p0);
    }
    
    private static final class f
    {
        public final com.applovin.exoplayer2.h.p.a a;
        public final long b;
        public final long c;
        public final boolean d;
        public final boolean e;
        public final boolean f;
        
        public f(final com.applovin.exoplayer2.h.p.a a, final long b, final long c, final boolean d, final boolean e, final boolean f) {
            this.a = a;
            this.b = b;
            this.c = c;
            this.d = d;
            this.e = e;
            this.f = f;
        }
    }
    
    private static final class g
    {
        public final ba a;
        public final int b;
        public final long c;
        
        public g(final ba a, final int b, final long c) {
            this.a = a;
            this.b = b;
            this.c = c;
        }
    }
}

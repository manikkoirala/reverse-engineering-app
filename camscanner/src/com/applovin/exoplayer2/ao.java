// 
// Decompiled by Procyon v0.6.0
// 

package com.applovin.exoplayer2;

import java.util.concurrent.TimeoutException;
import com.applovin.exoplayer2.l.a;
import android.os.Looper;
import androidx.annotation.Nullable;
import com.applovin.exoplayer2.l.d;

public final class ao
{
    private final b a;
    private final a b;
    private final d c;
    private final ba d;
    private int e;
    @Nullable
    private Object f;
    private Looper g;
    private int h;
    private long i;
    private boolean j;
    private boolean k;
    private boolean l;
    private boolean m;
    private boolean n;
    
    public ao(final a b, final b a, final ba d, final int h, final d c, final Looper g) {
        this.b = b;
        this.a = a;
        this.d = d;
        this.g = g;
        this.c = c;
        this.h = h;
        this.i = -9223372036854775807L;
        this.j = true;
    }
    
    public ao a(final int e) {
        com.applovin.exoplayer2.l.a.b(this.k ^ true);
        this.e = e;
        return this;
    }
    
    public ao a(@Nullable final Object f) {
        com.applovin.exoplayer2.l.a.b(this.k ^ true);
        this.f = f;
        return this;
    }
    
    public ba a() {
        return this.d;
    }
    
    public void a(final boolean b) {
        synchronized (this) {
            this.l |= b;
            this.m = true;
            this.notifyAll();
        }
    }
    
    public boolean a(final long n) throws InterruptedException, TimeoutException {
        synchronized (this) {
            com.applovin.exoplayer2.l.a.b(this.k);
            com.applovin.exoplayer2.l.a.b(this.g.getThread() != Thread.currentThread());
            final long a = this.c.a();
            long n2 = n;
            boolean m;
            while (true) {
                m = this.m;
                if (m || n2 <= 0L) {
                    break;
                }
                this.c.c();
                this.wait(n2);
                n2 = a + n - this.c.a();
            }
            if (m) {
                return this.l;
            }
            throw new TimeoutException("Message delivery timed out.");
        }
    }
    
    public b b() {
        return this.a;
    }
    
    public int c() {
        return this.e;
    }
    
    @Nullable
    public Object d() {
        return this.f;
    }
    
    public Looper e() {
        return this.g;
    }
    
    public long f() {
        return this.i;
    }
    
    public int g() {
        return this.h;
    }
    
    public boolean h() {
        return this.j;
    }
    
    public ao i() {
        com.applovin.exoplayer2.l.a.b(this.k ^ true);
        if (this.i == -9223372036854775807L) {
            com.applovin.exoplayer2.l.a.a(this.j);
        }
        this.k = true;
        this.b.a(this);
        return this;
    }
    
    public boolean j() {
        synchronized (this) {
            return this.n;
        }
    }
    
    public interface a
    {
        void a(final ao p0);
    }
    
    public interface b
    {
        void a(final int p0, @Nullable final Object p1) throws p;
    }
}

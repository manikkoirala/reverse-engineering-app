// 
// Decompiled by Procyon v0.6.0
// 

package com.applovin.exoplayer2.e;

import java.util.Arrays;
import com.applovin.exoplayer2.v;
import com.applovin.exoplayer2.l.y;
import androidx.annotation.Nullable;
import java.io.IOException;
import com.applovin.exoplayer2.k.g;

public interface x
{
    int a(final g p0, final int p1, final boolean p2) throws IOException;
    
    int a(final g p0, final int p1, final boolean p2, final int p3) throws IOException;
    
    void a(final long p0, final int p1, final int p2, final int p3, @Nullable final a p4);
    
    void a(final y p0, final int p1);
    
    void a(final y p0, final int p1, final int p2);
    
    void a(final v p0);
    
    public static final class a
    {
        public final int a;
        public final byte[] b;
        public final int c;
        public final int d;
        
        public a(final int a, final byte[] b, final int c, final int d) {
            this.a = a;
            this.b = b;
            this.c = c;
            this.d = d;
        }
        
        @Override
        public boolean equals(@Nullable final Object o) {
            boolean b = true;
            if (this == o) {
                return true;
            }
            if (o != null && a.class == o.getClass()) {
                final a a = (a)o;
                if (this.a != a.a || this.c != a.c || this.d != a.d || !Arrays.equals(this.b, a.b)) {
                    b = false;
                }
                return b;
            }
            return false;
        }
        
        @Override
        public int hashCode() {
            return ((this.a * 31 + Arrays.hashCode(this.b)) * 31 + this.c) * 31 + this.d;
        }
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.applovin.exoplayer2.e;

import com.applovin.exoplayer2.l.a;

public final class y
{
    private final byte[] a;
    private final int b;
    private int c;
    private int d;
    
    public y(final byte[] a) {
        this.a = a;
        this.b = a.length;
    }
    
    private void c() {
        final int c = this.c;
        boolean b2 = false;
        Label_0038: {
            if (c >= 0) {
                final int b = this.b;
                if (c < b || (c == b && this.d == 0)) {
                    b2 = true;
                    break Label_0038;
                }
            }
            b2 = false;
        }
        com.applovin.exoplayer2.l.a.b(b2);
    }
    
    public int a(final int a) {
        final int c = this.c;
        int i = Math.min(a, 8 - this.d);
        final byte[] a2 = this.a;
        int n = c + 1;
        int n2 = (a2[c] & 0xFF) >> this.d & 255 >> 8 - i;
        while (i < a) {
            n2 |= (this.a[n] & 0xFF) << i;
            i += 8;
            ++n;
        }
        this.b(a);
        return n2 & -1 >>> 32 - a;
    }
    
    public boolean a() {
        final boolean b = ((this.a[this.c] & 0xFF) >> this.d & 0x1) == 0x1;
        this.b(1);
        return b;
    }
    
    public int b() {
        return this.c * 8 + this.d;
    }
    
    public void b(int d) {
        final int n = d / 8;
        final int c = this.c + n;
        this.c = c;
        d = this.d + (d - n * 8);
        this.d = d;
        if (d > 7) {
            this.c = c + 1;
            this.d = d - 8;
        }
        this.c();
    }
}

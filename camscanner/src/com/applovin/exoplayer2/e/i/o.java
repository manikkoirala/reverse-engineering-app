// 
// Decompiled by Procyon v0.6.0
// 

package com.applovin.exoplayer2.e.i;

import com.applovin.exoplayer2.l.q;
import com.applovin.exoplayer2.l.a;
import com.applovin.exoplayer2.v;
import com.applovin.exoplayer2.e.x;
import com.applovin.exoplayer2.l.y;

public final class o implements j
{
    private final y a;
    private x b;
    private boolean c;
    private long d;
    private int e;
    private int f;
    
    public o() {
        this.a = new y(10);
        this.d = -9223372036854775807L;
    }
    
    @Override
    public void a() {
        this.c = false;
        this.d = -9223372036854775807L;
    }
    
    @Override
    public void a(final long d, final int n) {
        if ((n & 0x4) == 0x0) {
            return;
        }
        this.c = true;
        if (d != -9223372036854775807L) {
            this.d = d;
        }
        this.e = 0;
        this.f = 0;
    }
    
    @Override
    public void a(final com.applovin.exoplayer2.e.j j, final ad.d d) {
        d.a();
        (this.b = j.a(d.b(), 5)).a(new v.a().a(d.c()).f("application/id3").a());
    }
    
    @Override
    public void a(final y y) {
        com.applovin.exoplayer2.l.a.a((Object)this.b);
        if (!this.c) {
            return;
        }
        final int a = y.a();
        final int f = this.f;
        if (f < 10) {
            final int min = Math.min(a, 10 - f);
            System.arraycopy(y.d(), y.c(), this.a.d(), this.f, min);
            if (this.f + min == 10) {
                this.a.d(0);
                if (73 != this.a.h() || 68 != this.a.h() || 51 != this.a.h()) {
                    q.c("Id3Reader", "Discarding invalid ID3 tag");
                    this.c = false;
                    return;
                }
                this.a.e(3);
                this.e = this.a.v() + 10;
            }
        }
        final int min2 = Math.min(a, this.e - this.f);
        this.b.a(y, min2);
        this.f += min2;
    }
    
    @Override
    public void b() {
        com.applovin.exoplayer2.l.a.a((Object)this.b);
        if (this.c) {
            final int e = this.e;
            if (e != 0) {
                if (this.f == e) {
                    final long d = this.d;
                    if (d != -9223372036854775807L) {
                        this.b.a(d, 1, e, 0, null);
                    }
                    this.c = false;
                }
            }
        }
    }
}

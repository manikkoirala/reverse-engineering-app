// 
// Decompiled by Procyon v0.6.0
// 

package com.applovin.exoplayer2.e.i;

import com.applovin.exoplayer2.l.y;
import com.applovin.exoplayer2.e.j;
import com.applovin.exoplayer2.l.ai;
import com.applovin.exoplayer2.l.a;
import com.applovin.exoplayer2.l.ag;
import com.applovin.exoplayer2.v;

public final class s implements x
{
    private v a;
    private ag b;
    private com.applovin.exoplayer2.e.x c;
    
    public s(final String s) {
        this.a = new v.a().f(s).a();
    }
    
    private void a() {
        com.applovin.exoplayer2.l.a.a((Object)this.b);
        ai.a((Object)this.c);
    }
    
    @Override
    public void a(final ag b, final j j, final ad.d d) {
        this.b = b;
        d.a();
        (this.c = j.a(d.b(), 5)).a(this.a);
    }
    
    @Override
    public void a(final y y) {
        this.a();
        final long b = this.b.b();
        final long c = this.b.c();
        if (b != -9223372036854775807L) {
            if (c != -9223372036854775807L) {
                final v a = this.a;
                if (c != a.p) {
                    final v a2 = a.a().a(c).a();
                    this.a = a2;
                    this.c.a(a2);
                }
                final int a3 = y.a();
                this.c.a(y, a3);
                this.c.a(b, 1, a3, 0, null);
            }
        }
    }
}

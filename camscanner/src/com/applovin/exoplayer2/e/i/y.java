// 
// Decompiled by Procyon v0.6.0
// 

package com.applovin.exoplayer2.e.i;

import com.applovin.exoplayer2.l.ai;
import com.applovin.exoplayer2.e.j;
import com.applovin.exoplayer2.l.ag;

public final class y implements ad
{
    private final x a;
    private final com.applovin.exoplayer2.l.y b;
    private int c;
    private int d;
    private boolean e;
    private boolean f;
    
    public y(final x a) {
        this.a = a;
        this.b = new com.applovin.exoplayer2.l.y(32);
    }
    
    @Override
    public void a() {
        this.f = true;
    }
    
    @Override
    public void a(final ag ag, final j j, final d d) {
        this.a.a(ag, j, d);
        this.f = true;
    }
    
    @Override
    public void a(final com.applovin.exoplayer2.l.y y, int n) {
        if ((n & 0x1) != 0x0) {
            n = 1;
        }
        else {
            n = 0;
        }
        int n2;
        if (n != 0) {
            n2 = y.c() + y.h();
        }
        else {
            n2 = -1;
        }
        if (this.f) {
            if (n == 0) {
                return;
            }
            this.f = false;
            y.d(n2);
            this.d = 0;
        }
        while (y.a() > 0) {
            n = this.d;
            if (n < 3) {
                if (n == 0) {
                    n = y.h();
                    y.d(y.c() - 1);
                    if (n == 255) {
                        this.f = true;
                        return;
                    }
                }
                n = Math.min(y.a(), 3 - this.d);
                y.a(this.b.d(), this.d, n);
                n += this.d;
                if ((this.d = n) != 3) {
                    continue;
                }
                this.b.d(0);
                this.b.c(3);
                this.b.e(1);
                final int h = this.b.h();
                n = this.b.h();
                this.e = ((h & 0x80) != 0x0);
                this.c = ((h & 0xF) << 8 | n) + 3;
                n = this.b.e();
                final int c = this.c;
                if (n >= c) {
                    continue;
                }
                n = Math.min(4098, Math.max(c, this.b.e() * 2));
                this.b.b(n);
            }
            else {
                n = Math.min(y.a(), this.c - this.d);
                y.a(this.b.d(), this.d, n);
                n += this.d;
                this.d = n;
                final int c2 = this.c;
                if (n != c2) {
                    continue;
                }
                if (this.e) {
                    if (ai.a(this.b.d(), 0, this.c, -1) != 0) {
                        this.f = true;
                        return;
                    }
                    this.b.c(this.c - 4);
                }
                else {
                    this.b.c(c2);
                }
                this.b.d(0);
                this.a.a(this.b);
                this.d = 0;
            }
        }
    }
}

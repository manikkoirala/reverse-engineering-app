// 
// Decompiled by Procyon v0.6.0
// 

package com.applovin.exoplayer2.e.i;

import java.util.Arrays;
import com.applovin.exoplayer2.l.a;

final class r
{
    public byte[] a;
    public int b;
    private final int c;
    private boolean d;
    private boolean e;
    
    public r(final int c, final int n) {
        this.c = c;
        (this.a = new byte[n + 3])[2] = 1;
    }
    
    public void a() {
        this.d = false;
        this.e = false;
    }
    
    public void a(final int n) {
        final boolean d = this.d;
        boolean d2 = true;
        com.applovin.exoplayer2.l.a.b(d ^ true);
        if (n != this.c) {
            d2 = false;
        }
        this.d = d2;
        if (d2) {
            this.b = 3;
            this.e = false;
        }
    }
    
    public void a(final byte[] array, final int n, int length) {
        if (!this.d) {
            return;
        }
        final int n2 = length - n;
        final byte[] a = this.a;
        length = a.length;
        final int b = this.b;
        if (length < b + n2) {
            this.a = Arrays.copyOf(a, (b + n2) * 2);
        }
        System.arraycopy(array, n, this.a, this.b, n2);
        this.b += n2;
    }
    
    public boolean b() {
        return this.e;
    }
    
    public boolean b(final int n) {
        if (!this.d) {
            return false;
        }
        this.b -= n;
        this.d = false;
        return this.e = true;
    }
}

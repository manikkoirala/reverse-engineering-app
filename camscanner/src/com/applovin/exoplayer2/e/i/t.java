// 
// Decompiled by Procyon v0.6.0
// 

package com.applovin.exoplayer2.e.i;

import com.applovin.exoplayer2.ai;
import com.applovin.exoplayer2.l.a;
import com.applovin.exoplayer2.l.q;
import androidx.annotation.Nullable;
import com.applovin.exoplayer2.l.y;
import com.applovin.exoplayer2.l.ag;
import com.applovin.exoplayer2.l.x;

public final class t implements ad
{
    private final j a;
    private final x b;
    private int c;
    private int d;
    private ag e;
    private boolean f;
    private boolean g;
    private boolean h;
    private int i;
    private int j;
    private boolean k;
    private long l;
    
    public t(final j a) {
        this.a = a;
        this.b = new x(new byte[10]);
        this.c = 0;
    }
    
    private void a(final int c) {
        this.c = c;
        this.d = 0;
    }
    
    private boolean a(final y y, @Nullable final byte[] array, final int n) {
        final int min = Math.min(y.a(), n - this.d);
        boolean b = true;
        if (min <= 0) {
            return true;
        }
        if (array == null) {
            y.e(min);
        }
        else {
            y.a(array, this.d, min);
        }
        final int d = this.d + min;
        this.d = d;
        if (d != n) {
            b = false;
        }
        return b;
    }
    
    private boolean b() {
        this.b.a(0);
        final int c = this.b.c(24);
        if (c != 1) {
            final StringBuilder sb = new StringBuilder();
            sb.append("Unexpected start code prefix: ");
            sb.append(c);
            q.c("PesReader", sb.toString());
            this.j = -1;
            return false;
        }
        this.b.b(8);
        final int c2 = this.b.c(16);
        this.b.b(5);
        this.k = this.b.e();
        this.b.b(2);
        this.f = this.b.e();
        this.g = this.b.e();
        this.b.b(6);
        final int c3 = this.b.c(8);
        this.i = c3;
        if (c2 == 0) {
            this.j = -1;
        }
        else if ((this.j = c2 + 6 - 9 - c3) < 0) {
            final StringBuilder sb2 = new StringBuilder();
            sb2.append("Found negative packet payload size: ");
            sb2.append(this.j);
            q.c("PesReader", sb2.toString());
            this.j = -1;
        }
        return true;
    }
    
    private void c() {
        this.b.a(0);
        this.l = -9223372036854775807L;
        if (this.f) {
            this.b.b(4);
            final long n = this.b.c(3);
            this.b.b(1);
            final long n2 = this.b.c(15) << 15;
            this.b.b(1);
            final long n3 = this.b.c(15);
            this.b.b(1);
            if (!this.h && this.g) {
                this.b.b(4);
                final long n4 = this.b.c(3);
                this.b.b(1);
                final long n5 = this.b.c(15) << 15;
                this.b.b(1);
                final long n6 = this.b.c(15);
                this.b.b(1);
                this.e.b(n4 << 30 | n5 | n6);
                this.h = true;
            }
            this.l = this.e.b(n << 30 | n2 | n3);
        }
    }
    
    @Override
    public final void a() {
        this.c = 0;
        this.d = 0;
        this.h = false;
        this.a.a();
    }
    
    @Override
    public void a(final ag e, final com.applovin.exoplayer2.e.j j, final d d) {
        this.e = e;
        this.a.a(j, d);
    }
    
    @Override
    public final void a(final y y, int j) throws ai {
        com.applovin.exoplayer2.l.a.a((Object)this.e);
        int n = j;
        if ((j & 0x1) != 0x0) {
            final int c = this.c;
            if (c != 0 && c != 1) {
                if (c != 2) {
                    if (c != 3) {
                        throw new IllegalStateException();
                    }
                    if (this.j != -1) {
                        final StringBuilder sb = new StringBuilder();
                        sb.append("Unexpected start indicator: expected ");
                        sb.append(this.j);
                        sb.append(" more bytes");
                        q.c("PesReader", sb.toString());
                    }
                    this.a.b();
                }
                else {
                    q.c("PesReader", "Unexpected start indicator reading extended header");
                }
            }
            this.a(1);
            n = j;
        }
        while (y.a() > 0) {
            final int c2 = this.c;
            if (c2 != 0) {
                j = 0;
                final int n2 = 0;
                int n3 = 0;
                if (c2 != 1) {
                    if (c2 != 2) {
                        if (c2 != 3) {
                            throw new IllegalStateException();
                        }
                        final int a = y.a();
                        j = this.j;
                        if (j != -1) {
                            n3 = a - j;
                        }
                        j = a;
                        if (n3 > 0) {
                            j = a - n3;
                            y.c(y.c() + j);
                        }
                        this.a.a(y);
                        final int i = this.j;
                        if (i == -1) {
                            continue;
                        }
                        j = i - j;
                        if ((this.j = j) != 0) {
                            continue;
                        }
                        this.a.b();
                        this.a(1);
                    }
                    else {
                        if (!this.a(y, this.b.a, Math.min(10, this.i)) || !this.a(y, null, this.i)) {
                            continue;
                        }
                        this.c();
                        if (this.k) {
                            j = 4;
                        }
                        n |= j;
                        this.a.a(this.l, n);
                        this.a(3);
                    }
                }
                else {
                    if (!this.a(y, this.b.a, 9)) {
                        continue;
                    }
                    j = n2;
                    if (this.b()) {
                        j = 2;
                    }
                    this.a(j);
                }
            }
            else {
                y.e(y.a());
            }
        }
    }
}

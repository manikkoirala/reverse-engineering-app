// 
// Decompiled by Procyon v0.6.0
// 

package com.applovin.exoplayer2.e.i;

import com.applovin.exoplayer2.l.a;
import com.applovin.exoplayer2.e.j;
import com.applovin.exoplayer2.e.b;
import com.applovin.exoplayer2.l.y;
import com.applovin.exoplayer2.e.x;
import com.applovin.exoplayer2.v;
import java.util.List;

final class af
{
    private final List<v> a;
    private final x[] b;
    
    public af(final List<v> a) {
        this.a = a;
        this.b = new x[a.size()];
    }
    
    public void a(final long n, final y y) {
        if (y.a() < 9) {
            return;
        }
        final int q = y.q();
        final int q2 = y.q();
        final int h = y.h();
        if (q == 434 && q2 == 1195456820 && h == 3) {
            com.applovin.exoplayer2.e.b.b(n, y, this.b);
        }
    }
    
    public void a(final j j, final ad.d d) {
        for (int i = 0; i < this.b.length; ++i) {
            d.a();
            final x a = j.a(d.b(), 3);
            final v v = this.a.get(i);
            final String l = v.l;
            final boolean b = "application/cea-608".equals(l) || "application/cea-708".equals(l);
            final StringBuilder sb = new StringBuilder();
            sb.append("Invalid closed caption mime type provided: ");
            sb.append(l);
            com.applovin.exoplayer2.l.a.a(b, (Object)sb.toString());
            a.a(new v.a().a(d.c()).f(l).b(v.d).c(v.c).p(v.D).a(v.n).a());
            this.b[i] = a;
        }
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.applovin.exoplayer2.e.i;

import androidx.annotation.Nullable;
import android.util.SparseArray;
import com.applovin.exoplayer2.l.ai;
import com.applovin.exoplayer2.l.a;
import com.applovin.exoplayer2.l.v$a;
import com.applovin.exoplayer2.l.v$b;
import java.util.List;
import com.applovin.exoplayer2.l.e;
import com.applovin.exoplayer2.l.v;
import java.util.Arrays;
import java.util.ArrayList;
import com.applovin.exoplayer2.l.y;
import com.applovin.exoplayer2.e.x;

public final class m implements j
{
    private final z a;
    private final boolean b;
    private final boolean c;
    private final r d;
    private final r e;
    private final r f;
    private long g;
    private final boolean[] h;
    private String i;
    private x j;
    private a k;
    private boolean l;
    private long m;
    private boolean n;
    private final y o;
    
    public m(final z a, final boolean b, final boolean c) {
        this.a = a;
        this.b = b;
        this.c = c;
        this.h = new boolean[3];
        this.d = new r(7, 128);
        this.e = new r(8, 128);
        this.f = new r(6, 128);
        this.m = -9223372036854775807L;
        this.o = new y();
    }
    
    private void a(final long n, final int n2, int a, final long n3) {
        if (!this.l || this.k.a()) {
            this.d.b(a);
            this.e.b(a);
            if (!this.l) {
                if (this.d.b() && this.e.b()) {
                    final ArrayList list = new ArrayList();
                    final r d = this.d;
                    list.add(Arrays.copyOf(d.a, d.b));
                    final r e = this.e;
                    list.add(Arrays.copyOf(e.a, e.b));
                    final r d2 = this.d;
                    final v$b a2 = v.a(d2.a, 3, d2.b);
                    final r e2 = this.e;
                    final v$a b = v.b(e2.a, 3, e2.b);
                    this.j.a(new com.applovin.exoplayer2.v.a().a(this.i).f("video/avc").d(com.applovin.exoplayer2.l.e.a(a2.a, a2.b, a2.c)).g(a2.e).h(a2.f).b(a2.g).a(list).a());
                    this.l = true;
                    this.k.a(a2);
                    this.k.a(b);
                    this.d.a();
                    this.e.a();
                }
            }
            else if (this.d.b()) {
                final r d3 = this.d;
                this.k.a(v.a(d3.a, 3, d3.b));
                this.d.a();
            }
            else if (this.e.b()) {
                final r e3 = this.e;
                this.k.a(v.b(e3.a, 3, e3.b));
                this.e.a();
            }
        }
        if (this.f.b(a)) {
            final r f = this.f;
            a = v.a(f.a, f.b);
            this.o.a(this.f.a, a);
            this.o.d(4);
            this.a.a(n3, this.o);
        }
        if (this.k.a(n, n2, this.l, this.n)) {
            this.n = false;
        }
    }
    
    private void a(final long n, final int n2, final long n3) {
        if (!this.l || this.k.a()) {
            this.d.a(n2);
            this.e.a(n2);
        }
        this.f.a(n2);
        this.k.a(n, n2, n3);
    }
    
    private void a(final byte[] array, final int n, final int n2) {
        if (!this.l || this.k.a()) {
            this.d.a(array, n, n2);
            this.e.a(array, n, n2);
        }
        this.f.a(array, n, n2);
        this.k.a(array, n, n2);
    }
    
    private void c() {
        com.applovin.exoplayer2.l.a.a((Object)this.j);
        ai.a((Object)this.k);
    }
    
    @Override
    public void a() {
        this.g = 0L;
        this.n = false;
        this.m = -9223372036854775807L;
        v.a(this.h);
        this.d.a();
        this.e.a();
        this.f.a();
        final a k = this.k;
        if (k != null) {
            k.b();
        }
    }
    
    @Override
    public void a(final long m, int n) {
        if (m != -9223372036854775807L) {
            this.m = m;
        }
        final boolean n2 = this.n;
        if ((n & 0x2) != 0x0) {
            n = 1;
        }
        else {
            n = 0;
        }
        this.n = (((n2 ? 1 : 0) | n) != 0x0);
    }
    
    @Override
    public void a(final com.applovin.exoplayer2.e.j j, final ad.d d) {
        d.a();
        this.i = d.c();
        final x a = j.a(d.b(), 2);
        this.j = a;
        this.k = new a(a, this.b, this.c);
        this.a.a(j, d);
    }
    
    @Override
    public void a(final y y) {
        this.c();
        int c = y.c();
        final int b = y.b();
        final byte[] d = y.d();
        this.g += y.a();
        this.j.a(y, y.a());
        while (true) {
            final int a = v.a(d, c, b, this.h);
            if (a == b) {
                break;
            }
            final int b2 = v.b(d, a);
            final int n = a - c;
            if (n > 0) {
                this.a(d, c, a);
            }
            final int n2 = b - a;
            final long n3 = this.g - n2;
            int n4;
            if (n < 0) {
                n4 = -n;
            }
            else {
                n4 = 0;
            }
            this.a(n3, n2, n4, this.m);
            this.a(n3, b2, this.m);
            c = a + 3;
        }
        this.a(d, c, b);
    }
    
    @Override
    public void b() {
    }
    
    private static final class a
    {
        private final x a;
        private final boolean b;
        private final boolean c;
        private final SparseArray<v$b> d;
        private final SparseArray<v$a> e;
        private final com.applovin.exoplayer2.l.z f;
        private byte[] g;
        private int h;
        private int i;
        private long j;
        private boolean k;
        private long l;
        private m.a.a m;
        private m.a.a n;
        private boolean o;
        private long p;
        private long q;
        private boolean r;
        
        public a(final x a, final boolean b, final boolean c) {
            this.a = a;
            this.b = b;
            this.c = c;
            this.d = (SparseArray<v$b>)new SparseArray();
            this.e = (SparseArray<v$a>)new SparseArray();
            this.m = new m.a.a();
            this.n = new m.a.a();
            final byte[] g = new byte[128];
            this.g = g;
            this.f = new com.applovin.exoplayer2.l.z(g, 0, 0);
            this.b();
        }
        
        private void a(final int n) {
            final long q = this.q;
            if (q == -9223372036854775807L) {
                return;
            }
            this.a.a(q, this.r ? 1 : 0, (int)(this.j - this.p), n, null);
        }
        
        public void a(final long j, final int i, final long l) {
            this.i = i;
            this.l = l;
            this.j = j;
            if ((this.b && i == 1) || (this.c && (i == 5 || i == 1 || i == 2))) {
                final m.a.a m = this.m;
                this.m = this.n;
                (this.n = m).a();
                this.h = 0;
                this.k = true;
            }
        }
        
        public void a(final v$a v$a) {
            this.e.append(v$a.a, (Object)v$a);
        }
        
        public void a(final v$b v$b) {
            this.d.append(v$b.d, (Object)v$b);
        }
        
        public void a(final byte[] array, int h, int e) {
            if (!this.k) {
                return;
            }
            e -= h;
            final byte[] g = this.g;
            final int length = g.length;
            final int h2 = this.h;
            if (length < h2 + e) {
                this.g = Arrays.copyOf(g, (h2 + e) * 2);
            }
            System.arraycopy(array, h, this.g, this.h, e);
            h = this.h + e;
            this.h = h;
            this.f.a(this.g, 0, h);
            if (!this.f.b(8)) {
                return;
            }
            this.f.a();
            final int c = this.f.c(2);
            this.f.a(5);
            if (!this.f.c()) {
                return;
            }
            this.f.d();
            if (!this.f.c()) {
                return;
            }
            final int d = this.f.d();
            if (!this.c) {
                this.k = false;
                this.n.a(d);
                return;
            }
            if (!this.f.c()) {
                return;
            }
            final int d2 = this.f.d();
            if (this.e.indexOfKey(d2) < 0) {
                this.k = false;
                return;
            }
            final v$a v$a = (v$a)this.e.get(d2);
            final v$b v$b = (v$b)this.d.get(v$a.b);
            if (v$b.h) {
                if (!this.f.b(2)) {
                    return;
                }
                this.f.a(2);
            }
            if (!this.f.b(v$b.j)) {
                return;
            }
            final int c2 = this.f.c(v$b.j);
            boolean b = false;
            boolean b2 = false;
            boolean b3 = false;
            Label_0386: {
                if (!v$b.i) {
                    if (!this.f.b(1)) {
                        return;
                    }
                    b = this.f.b();
                    if (b) {
                        if (!this.f.b(1)) {
                            return;
                        }
                        b2 = this.f.b();
                        b3 = true;
                        break Label_0386;
                    }
                }
                else {
                    b = false;
                }
                b3 = false;
                b2 = false;
            }
            final boolean b4 = this.i == 5;
            int d3;
            if (b4) {
                if (!this.f.c()) {
                    return;
                }
                d3 = this.f.d();
            }
            else {
                d3 = 0;
            }
            h = v$b.k;
            int e2 = 0;
            int e3 = 0;
            Label_0597: {
                Label_0594: {
                    Label_0591: {
                        if (h == 0) {
                            if (!this.f.b(v$b.l)) {
                                return;
                            }
                            h = this.f.c(v$b.l);
                            if (v$a.c && !b) {
                                if (!this.f.c()) {
                                    return;
                                }
                                e = this.f.e();
                                break Label_0591;
                            }
                        }
                        else if (h == 1 && !v$b.m) {
                            if (!this.f.c()) {
                                return;
                            }
                            e2 = this.f.e();
                            if (!v$a.c || b) {
                                h = 0;
                                e = 0;
                                break Label_0594;
                            }
                            if (!this.f.c()) {
                                return;
                            }
                            e3 = this.f.e();
                            h = 0;
                            e = 0;
                            break Label_0597;
                        }
                        else {
                            h = 0;
                        }
                        e = 0;
                    }
                    e2 = 0;
                }
                e3 = 0;
            }
            this.n.a(v$b, c, d, c2, d2, b, b3, b2, b4, d3, h, e, e2, e3);
            this.k = false;
        }
        
        public boolean a() {
            return this.c;
        }
        
        public boolean a(final long n, int n2, final boolean b, boolean b2) {
            final int i = this.i;
            final int n3 = 0;
            if (i == 9 || (this.c && this.n.a(this.m))) {
                if (b && this.o) {
                    this.a(n2 + (int)(n - this.j));
                }
                this.p = this.j;
                this.q = this.l;
                this.r = false;
                this.o = true;
            }
            if (this.b) {
                b2 = this.n.b();
            }
            final boolean r = this.r;
            final int j = this.i;
            if (j != 5) {
                n2 = n3;
                if (!b2) {
                    return this.r = (((r ? 1 : 0) | n2) != 0x0);
                }
                n2 = n3;
                if (j != 1) {
                    return this.r = (((r ? 1 : 0) | n2) != 0x0);
                }
            }
            n2 = 1;
            return this.r = (((r ? 1 : 0) | n2) != 0x0);
        }
        
        public void b() {
            this.k = false;
            this.o = false;
            this.n.a();
        }
        
        private static final class a
        {
            private boolean a;
            private boolean b;
            @Nullable
            private v$b c;
            private int d;
            private int e;
            private int f;
            private int g;
            private boolean h;
            private boolean i;
            private boolean j;
            private boolean k;
            private int l;
            private int m;
            private int n;
            private int o;
            private int p;
            
            private boolean a(final a a) {
                final boolean a2 = this.a;
                final boolean b = false;
                if (!a2) {
                    return false;
                }
                if (!a.a) {
                    return true;
                }
                final v$b v$b = (v$b)a.a((Object)this.c);
                final v$b v$b2 = (v$b)a.a((Object)a.c);
                if (this.f == a.f && this.g == a.g && this.h == a.h && (!this.i || !a.i || this.j == a.j)) {
                    final int d = this.d;
                    final int d2 = a.d;
                    if (d == d2 || (d != 0 && d2 != 0)) {
                        final int k = v$b.k;
                        if ((k != 0 || v$b2.k != 0 || (this.m == a.m && this.n == a.n)) && (k != 1 || v$b2.k != 1 || (this.o == a.o && this.p == a.p))) {
                            final boolean i = this.k;
                            if (i == a.k) {
                                boolean b2 = b;
                                if (!i) {
                                    return b2;
                                }
                                b2 = b;
                                if (this.l == a.l) {
                                    return b2;
                                }
                            }
                        }
                    }
                }
                return true;
            }
            
            public void a() {
                this.b = false;
                this.a = false;
            }
            
            public void a(final int e) {
                this.e = e;
                this.b = true;
            }
            
            public void a(final v$b c, final int d, final int e, final int f, final int g, final boolean h, final boolean i, final boolean j, final boolean k, final int l, final int m, final int n, final int o, final int p14) {
                this.c = c;
                this.d = d;
                this.e = e;
                this.f = f;
                this.g = g;
                this.h = h;
                this.i = i;
                this.j = j;
                this.k = k;
                this.l = l;
                this.m = m;
                this.n = n;
                this.o = o;
                this.p = p14;
                this.a = true;
                this.b = true;
            }
            
            public boolean b() {
                if (this.b) {
                    final int e = this.e;
                    if (e == 7 || e == 2) {
                        return true;
                    }
                }
                return false;
            }
        }
    }
}

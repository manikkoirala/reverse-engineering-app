// 
// Decompiled by Procyon v0.6.0
// 

package com.applovin.exoplayer2.e.i;

import com.applovin.exoplayer2.l.y;

public final class ae
{
    public static int a(final byte[] array, int n, final int n2) {
        while (n < n2 && array[n] != 71) {
            ++n;
        }
        return n;
    }
    
    public static long a(final y y, int q, int n) {
        y.d(q);
        if (y.a() < 5) {
            return -9223372036854775807L;
        }
        q = y.q();
        if ((0x800000 & q) != 0x0) {
            return -9223372036854775807L;
        }
        if ((0x1FFF00 & q) >> 8 != n) {
            return -9223372036854775807L;
        }
        n = 1;
        if ((q & 0x20) != 0x0) {
            q = 1;
        }
        else {
            q = 0;
        }
        if (q == 0) {
            return -9223372036854775807L;
        }
        if (y.h() >= 7 && y.a() >= 7) {
            if ((y.h() & 0x10) == 0x10) {
                q = n;
            }
            else {
                q = 0;
            }
            if (q != 0) {
                final byte[] array = new byte[6];
                y.a(array, 0, 6);
                return a(array);
            }
        }
        return -9223372036854775807L;
    }
    
    private static long a(final byte[] array) {
        return ((long)array[0] & 0xFFL) << 25 | ((long)array[1] & 0xFFL) << 17 | ((long)array[2] & 0xFFL) << 9 | ((long)array[3] & 0xFFL) << 1 | (0xFFL & (long)array[4]) >> 7;
    }
    
    public static boolean a(final byte[] array, final int n, final int n2, final int n3) {
        int i = -4;
        int n4 = 0;
        while (i <= 4) {
            final int n5 = i * 188 + n3;
            if (n5 >= n && n5 < n2 && array[n5] == 71) {
                if (++n4 == 5) {
                    return true;
                }
            }
            else {
                n4 = 0;
            }
            ++i;
        }
        return false;
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.applovin.exoplayer2.e.i;

import com.applovin.exoplayer2.e.g;
import com.applovin.exoplayer2.ai;
import java.util.Collections;
import com.applovin.exoplayer2.v;
import com.applovin.exoplayer2.b.a;
import com.applovin.exoplayer2.l.q;
import java.util.Arrays;
import androidx.annotation.Nullable;
import com.applovin.exoplayer2.l.y;
import com.applovin.exoplayer2.l.x;

public final class f implements j
{
    private static final byte[] a;
    private final boolean b;
    private final x c;
    private final y d;
    @Nullable
    private final String e;
    private String f;
    private com.applovin.exoplayer2.e.x g;
    private com.applovin.exoplayer2.e.x h;
    private int i;
    private int j;
    private int k;
    private boolean l;
    private boolean m;
    private int n;
    private int o;
    private int p;
    private boolean q;
    private long r;
    private int s;
    private long t;
    private com.applovin.exoplayer2.e.x u;
    private long v;
    
    static {
        a = new byte[] { 73, 68, 51 };
    }
    
    public f(final boolean b) {
        this(b, null);
    }
    
    public f(final boolean b, @Nullable final String e) {
        this.c = new x(new byte[7]);
        this.d = new y(Arrays.copyOf(com.applovin.exoplayer2.e.i.f.a, 10));
        this.e();
        this.n = -1;
        this.o = -1;
        this.r = -9223372036854775807L;
        this.t = -9223372036854775807L;
        this.b = b;
        this.e = e;
    }
    
    private void a(final com.applovin.exoplayer2.e.x u, final long v, final int j, final int s) {
        this.i = 4;
        this.j = j;
        this.u = u;
        this.v = v;
        this.s = s;
    }
    
    private boolean a(final byte b, final byte b2) {
        return a((b & 0xFF) << 8 | (b2 & 0xFF));
    }
    
    public static boolean a(final int n) {
        return (n & 0xFFF6) == 0xFFF0;
    }
    
    private boolean a(final y y, int n) {
        y.d(n + 1);
        final byte[] a = this.c.a;
        boolean b = true;
        final boolean b2 = true;
        if (!this.b(y, a, 1)) {
            return false;
        }
        this.c.a(4);
        final int c = this.c.c(1);
        final int n2 = this.n;
        if (n2 != -1 && c != n2) {
            return false;
        }
        if (this.o != -1) {
            if (!this.b(y, this.c.a, 1)) {
                return true;
            }
            this.c.a(2);
            if (this.c.c(4) != this.o) {
                return false;
            }
            y.d(n + 2);
        }
        if (!this.b(y, this.c.a, 4)) {
            return true;
        }
        this.c.a(14);
        final int c2 = this.c.c(13);
        if (c2 < 7) {
            return false;
        }
        final byte[] d = y.d();
        final int b3 = y.b();
        n += c2;
        if (n >= b3) {
            return true;
        }
        final byte b4 = d[n];
        if (b4 == -1) {
            return ++n == b3 || (this.a((byte)(-1), d[n]) && (d[n] & 0x8) >> 3 == c && b2);
        }
        if (b4 != 73) {
            return false;
        }
        final int n3 = n + 1;
        if (n3 == b3) {
            return true;
        }
        if (d[n3] != 68) {
            return false;
        }
        n += 2;
        if (n == b3) {
            return true;
        }
        if (d[n] != 51) {
            b = false;
        }
        return b;
    }
    
    private boolean a(final y y, final byte[] array, final int n) {
        final int min = Math.min(y.a(), n - this.j);
        y.a(array, this.j, min);
        final int j = this.j + min;
        this.j = j;
        return j == n;
    }
    
    private void b(final y y) {
        final byte[] d = y.d();
        int i = y.c();
        while (i < y.b()) {
            final int n = i + 1;
            final int n2 = d[i] & 0xFF;
            if (this.k == 512 && this.a((byte)(-1), (byte)n2) && (this.m || this.a(y, n - 2))) {
                this.p = (n2 & 0x8) >> 3;
                boolean l = true;
                if ((n2 & 0x1) != 0x0) {
                    l = false;
                }
                this.l = l;
                if (!this.m) {
                    this.h();
                }
                else {
                    this.g();
                }
                y.d(n);
                return;
            }
            final int k = this.k;
            final int n3 = n2 | k;
            if (n3 != 329) {
                if (n3 != 511) {
                    if (n3 != 836) {
                        if (n3 == 1075) {
                            this.f();
                            y.d(n);
                            return;
                        }
                        i = n;
                        if (k == 256) {
                            continue;
                        }
                        this.k = 256;
                        i = n - 1;
                    }
                    else {
                        this.k = 1024;
                        i = n;
                    }
                }
                else {
                    this.k = 512;
                    i = n;
                }
            }
            else {
                this.k = 768;
                i = n;
            }
        }
        y.d(i);
    }
    
    private boolean b(final y y, final byte[] array, final int n) {
        if (y.a() < n) {
            return false;
        }
        y.a(array, 0, n);
        return true;
    }
    
    private void c(final y y) {
        if (y.a() == 0) {
            return;
        }
        this.c.a[0] = y.d()[y.c()];
        this.c.a(2);
        final int c = this.c.c(4);
        final int o = this.o;
        if (o != -1 && c != o) {
            this.d();
            return;
        }
        if (!this.m) {
            this.m = true;
            this.n = this.p;
            this.o = c;
        }
        this.g();
    }
    
    private void d() {
        this.m = false;
        this.e();
    }
    
    private void d(final y y) {
        final int min = Math.min(y.a(), this.s - this.j);
        this.u.a(y, min);
        final int j = this.j + min;
        this.j = j;
        final int s = this.s;
        if (j == s) {
            final long t = this.t;
            if (t != -9223372036854775807L) {
                this.u.a(t, 1, s, 0, null);
                this.t += this.v;
            }
            this.e();
        }
    }
    
    private void e() {
        this.i = 0;
        this.j = 0;
        this.k = 256;
    }
    
    private void f() {
        this.i = 2;
        this.j = com.applovin.exoplayer2.e.i.f.a.length;
        this.s = 0;
        this.d.d(0);
    }
    
    private void g() {
        this.i = 3;
        this.j = 0;
    }
    
    private void h() {
        this.i = 1;
        this.j = 0;
    }
    
    private void i() {
        this.h.a(this.d, 10);
        this.d.d(6);
        this.a(this.h, 0L, 10, this.d.v() + 10);
    }
    
    private void j() throws ai {
        this.c.a(0);
        if (!this.q) {
            final int i = this.c.c(2) + 1;
            int n;
            if ((n = i) != 2) {
                final StringBuilder sb = new StringBuilder();
                sb.append("Detected audio object type: ");
                sb.append(i);
                sb.append(", but assuming AAC LC.");
                com.applovin.exoplayer2.l.q.c("AdtsReader", sb.toString());
                n = 2;
            }
            this.c.b(5);
            final byte[] a = com.applovin.exoplayer2.b.a.a(n, this.o, this.c.c(3));
            final a.a a2 = com.applovin.exoplayer2.b.a.a(a);
            final v a3 = new v.a().a(this.f).f("audio/mp4a-latm").d(a2.c).k(a2.b).l(a2.a).a(Collections.singletonList(a)).c(this.e).a();
            this.r = 1024000000L / a3.z;
            this.g.a(a3);
            this.q = true;
        }
        else {
            this.c.b(10);
        }
        this.c.b(4);
        int n2 = this.c.c(13) - 2 - 5;
        if (this.l) {
            n2 -= 2;
        }
        this.a(this.g, this.r, 0, n2);
    }
    
    private void k() {
        com.applovin.exoplayer2.l.a.b((Object)this.g);
        com.applovin.exoplayer2.l.ai.a((Object)this.u);
        com.applovin.exoplayer2.l.ai.a((Object)this.h);
    }
    
    @Override
    public void a() {
        this.t = -9223372036854775807L;
        this.d();
    }
    
    @Override
    public void a(final long t, final int n) {
        if (t != -9223372036854775807L) {
            this.t = t;
        }
    }
    
    @Override
    public void a(final com.applovin.exoplayer2.e.j j, final ad.d d) {
        d.a();
        this.f = d.c();
        final com.applovin.exoplayer2.e.x a = j.a(d.b(), 1);
        this.g = a;
        this.u = a;
        if (this.b) {
            d.a();
            (this.h = j.a(d.b(), 5)).a(new v.a().a(d.c()).f("application/id3").a());
        }
        else {
            this.h = new g();
        }
    }
    
    @Override
    public void a(final y y) throws ai {
        this.k();
        while (y.a() > 0) {
            final int i = this.i;
            if (i != 0) {
                if (i != 1) {
                    if (i != 2) {
                        if (i != 3) {
                            if (i != 4) {
                                throw new IllegalStateException();
                            }
                            this.d(y);
                        }
                        else {
                            int n;
                            if (this.l) {
                                n = 7;
                            }
                            else {
                                n = 5;
                            }
                            if (!this.a(y, this.c.a, n)) {
                                continue;
                            }
                            this.j();
                        }
                    }
                    else {
                        if (!this.a(y, this.d.d(), 10)) {
                            continue;
                        }
                        this.i();
                    }
                }
                else {
                    this.c(y);
                }
            }
            else {
                this.b(y);
            }
        }
    }
    
    @Override
    public void b() {
    }
    
    public long c() {
        return this.r;
    }
}

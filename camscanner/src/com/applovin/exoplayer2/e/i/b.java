// 
// Decompiled by Procyon v0.6.0
// 

package com.applovin.exoplayer2.e.i;

import com.applovin.exoplayer2.l.a;
import com.applovin.exoplayer2.l.ai;
import com.applovin.exoplayer2.v;
import androidx.annotation.Nullable;
import com.applovin.exoplayer2.l.y;
import com.applovin.exoplayer2.l.x;

public final class b implements j
{
    private final x a;
    private final y b;
    @Nullable
    private final String c;
    private String d;
    private com.applovin.exoplayer2.e.x e;
    private int f;
    private int g;
    private boolean h;
    private long i;
    private v j;
    private int k;
    private long l;
    
    public b() {
        this(null);
    }
    
    public b(@Nullable final String c) {
        final x a = new x(new byte[128]);
        this.a = a;
        this.b = new y(a.a);
        this.f = 0;
        this.l = -9223372036854775807L;
        this.c = c;
    }
    
    private boolean a(final y y, final byte[] array, final int n) {
        final int min = Math.min(y.a(), n - this.g);
        y.a(array, this.g, min);
        final int g = this.g + min;
        this.g = g;
        return g == n;
    }
    
    private boolean b(final y y) {
        while (true) {
            final int a = y.a();
            boolean h = false;
            final boolean b = false;
            if (a <= 0) {
                return false;
            }
            if (!this.h) {
                boolean h2 = b;
                if (y.h() == 11) {
                    h2 = true;
                }
                this.h = h2;
            }
            else {
                final int h3 = y.h();
                if (h3 == 119) {
                    this.h = false;
                    return true;
                }
                if (h3 == 11) {
                    h = true;
                }
                this.h = h;
            }
        }
    }
    
    private void c() {
        this.a.a(0);
        final com.applovin.exoplayer2.b.b.a a = com.applovin.exoplayer2.b.b.a(this.a);
        final v j = this.j;
        if (j == null || a.d != j.y || a.c != j.z || !ai.a((Object)a.a, (Object)j.l)) {
            final v a2 = new v.a().a(this.d).f(a.a).k(a.d).l(a.c).c(this.c).a();
            this.j = a2;
            this.e.a(a2);
        }
        this.k = a.e;
        this.i = a.f * 1000000L / this.j.z;
    }
    
    @Override
    public void a() {
        this.f = 0;
        this.g = 0;
        this.h = false;
        this.l = -9223372036854775807L;
    }
    
    @Override
    public void a(final long l, final int n) {
        if (l != -9223372036854775807L) {
            this.l = l;
        }
    }
    
    @Override
    public void a(final com.applovin.exoplayer2.e.j j, final ad.d d) {
        d.a();
        this.d = d.c();
        this.e = j.a(d.b(), 1);
    }
    
    @Override
    public void a(final y y) {
        com.applovin.exoplayer2.l.a.a((Object)this.e);
        while (y.a() > 0) {
            final int f = this.f;
            if (f != 0) {
                if (f != 1) {
                    if (f != 2) {
                        continue;
                    }
                    final int min = Math.min(y.a(), this.k - this.g);
                    this.e.a(y, min);
                    final int g = this.g + min;
                    this.g = g;
                    final int k = this.k;
                    if (g != k) {
                        continue;
                    }
                    final long l = this.l;
                    if (l != -9223372036854775807L) {
                        this.e.a(l, 1, k, 0, null);
                        this.l += this.i;
                    }
                    this.f = 0;
                }
                else {
                    if (!this.a(y, this.b.d(), 128)) {
                        continue;
                    }
                    this.c();
                    this.b.d(0);
                    this.e.a(this.b, 128);
                    this.f = 2;
                }
            }
            else {
                if (!this.b(y)) {
                    continue;
                }
                this.f = 1;
                this.b.d()[0] = 11;
                this.b.d()[1] = 119;
                this.g = 2;
            }
        }
    }
    
    @Override
    public void b() {
    }
}

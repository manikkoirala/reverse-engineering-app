// 
// Decompiled by Procyon v0.6.0
// 

package com.applovin.exoplayer2.e.i;

import com.applovin.exoplayer2.l.ai;
import java.io.IOException;
import com.applovin.exoplayer2.e.i;
import com.applovin.exoplayer2.l.y;
import com.applovin.exoplayer2.l.ag;
import com.applovin.exoplayer2.e.a;

final class u extends com.applovin.exoplayer2.e.a
{
    public u(final ag ag, final long n, final long n2) {
        super((d)new b(), (f)new a(ag), n, 0L, n + 1L, 0L, n2, 188L, 1000);
    }
    
    private static int b(final byte[] array, final int n) {
        return (array[n + 3] & 0xFF) | ((array[n] & 0xFF) << 24 | (array[n + 1] & 0xFF) << 16 | (array[n + 2] & 0xFF) << 8);
    }
    
    private static final class a implements f
    {
        private final ag a;
        private final y b;
        
        private a(final ag a) {
            this.a = a;
            this.b = new y();
        }
        
        private e a(final y y, final long n, final long n2) {
            int c = -1;
            long n3 = -9223372036854775807L;
            int n4 = -1;
            while (y.a() >= 4) {
                if (b(y.d(), y.c()) != 442) {
                    y.e(1);
                }
                else {
                    y.e(4);
                    final long a = v.a(y);
                    int c2 = n4;
                    long b = n3;
                    if (a != -9223372036854775807L) {
                        b = this.a.b(a);
                        if (b > n) {
                            if (n3 == -9223372036854775807L) {
                                return e.a(b, n2);
                            }
                            return e.a(n2 + n4);
                        }
                        else {
                            if (100000L + b > n) {
                                return e.a(n2 + y.c());
                            }
                            c2 = y.c();
                        }
                    }
                    a(y);
                    c = y.c();
                    n4 = c2;
                    n3 = b;
                }
            }
            if (n3 != -9223372036854775807L) {
                return e.b(n3, n2 + c);
            }
            return e.a;
        }
        
        private static void a(final y y) {
            final int b = y.b();
            if (y.a() < 10) {
                y.d(b);
                return;
            }
            y.e(9);
            final int n = y.h() & 0x7;
            if (y.a() < n) {
                y.d(b);
                return;
            }
            y.e(n);
            if (y.a() < 4) {
                y.d(b);
                return;
            }
            if (b(y.d(), y.c()) == 443) {
                y.e(4);
                final int i = y.i();
                if (y.a() < i) {
                    y.d(b);
                    return;
                }
                y.e(i);
            }
            while (y.a() >= 4) {
                final int a = b(y.d(), y.c());
                if (a == 442) {
                    break;
                }
                if (a == 441) {
                    break;
                }
                if (a >>> 8 != 1) {
                    break;
                }
                y.e(4);
                if (y.a() < 2) {
                    y.d(b);
                    return;
                }
                y.d(Math.min(y.b(), y.c() + y.i()));
            }
        }
        
        @Override
        public e a(final i i, final long n) throws IOException {
            final long c = i.c();
            final int n2 = (int)Math.min(20000L, i.d() - c);
            this.b.a(n2);
            i.d(this.b.d(), 0, n2);
            return this.a(this.b, n, c);
        }
        
        @Override
        public void a() {
            this.b.a(ai.f);
        }
    }
}

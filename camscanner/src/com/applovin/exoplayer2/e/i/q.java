// 
// Decompiled by Procyon v0.6.0
// 

package com.applovin.exoplayer2.e.i;

import com.applovin.exoplayer2.l.a;
import com.applovin.exoplayer2.v;
import com.applovin.exoplayer2.e.x;
import androidx.annotation.Nullable;
import com.applovin.exoplayer2.b.r;
import com.applovin.exoplayer2.l.y;

public final class q implements j
{
    private final y a;
    private final r.a b;
    @Nullable
    private final String c;
    private x d;
    private String e;
    private int f;
    private int g;
    private boolean h;
    private boolean i;
    private long j;
    private int k;
    private long l;
    
    public q() {
        this(null);
    }
    
    public q(@Nullable final String c) {
        this.f = 0;
        final y a = new y(4);
        this.a = a;
        a.d()[0] = -1;
        this.b = new r.a();
        this.l = -9223372036854775807L;
        this.c = c;
    }
    
    private void b(final y y) {
        final byte[] d = y.d();
        int i;
        int b;
        for (i = y.c(), b = y.b(); i < b; ++i) {
            final byte b2 = d[i];
            final boolean j = (b2 & 0xFF) == 0xFF;
            final boolean b3 = this.i && (b2 & 0xE0) == 0xE0;
            this.i = j;
            if (b3) {
                y.d(i + 1);
                this.i = false;
                this.a.d()[1] = d[i];
                this.g = 2;
                this.f = 1;
                return;
            }
        }
        y.d(b);
    }
    
    private void c(final y y) {
        final int min = Math.min(y.a(), 4 - this.g);
        y.a(this.a.d(), this.g, min);
        final int g = this.g + min;
        this.g = g;
        if (g < 4) {
            return;
        }
        this.a.d(0);
        if (!this.b.a(this.a.q())) {
            this.g = 0;
            this.f = 1;
            return;
        }
        final r.a b = this.b;
        this.k = b.c;
        if (!this.h) {
            this.j = b.g * 1000000L / b.d;
            this.d.a(new v.a().a(this.e).f(this.b.b).f(4096).k(this.b.e).l(this.b.d).c(this.c).a());
            this.h = true;
        }
        this.a.d(0);
        this.d.a(this.a, 4);
        this.f = 2;
    }
    
    private void d(final y y) {
        final int min = Math.min(y.a(), this.k - this.g);
        this.d.a(y, min);
        final int g = this.g + min;
        this.g = g;
        final int k = this.k;
        if (g < k) {
            return;
        }
        final long l = this.l;
        if (l != -9223372036854775807L) {
            this.d.a(l, 1, k, 0, null);
            this.l += this.j;
        }
        this.g = 0;
        this.f = 0;
    }
    
    @Override
    public void a() {
        this.f = 0;
        this.g = 0;
        this.i = false;
        this.l = -9223372036854775807L;
    }
    
    @Override
    public void a(final long l, final int n) {
        if (l != -9223372036854775807L) {
            this.l = l;
        }
    }
    
    @Override
    public void a(final com.applovin.exoplayer2.e.j j, final ad.d d) {
        d.a();
        this.e = d.c();
        this.d = j.a(d.b(), 1);
    }
    
    @Override
    public void a(final y y) {
        com.applovin.exoplayer2.l.a.a((Object)this.d);
        while (y.a() > 0) {
            final int f = this.f;
            if (f != 0) {
                if (f != 1) {
                    if (f != 2) {
                        throw new IllegalStateException();
                    }
                    this.d(y);
                }
                else {
                    this.c(y);
                }
            }
            else {
                this.b(y);
            }
        }
    }
    
    @Override
    public void b() {
    }
}

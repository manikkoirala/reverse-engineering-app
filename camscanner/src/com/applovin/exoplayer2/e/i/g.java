// 
// Decompiled by Procyon v0.6.0
// 

package com.applovin.exoplayer2.e.i;

import androidx.annotation.Nullable;
import android.util.SparseArray;
import com.applovin.exoplayer2.l.e;
import java.util.ArrayList;
import com.applovin.exoplayer2.l.y;
import com.applovin.exoplayer2.common.a.s;
import com.applovin.exoplayer2.v;
import java.util.List;

public final class g implements c
{
    private final int a;
    private final List<v> b;
    
    public g() {
        this(0);
    }
    
    public g(final int n) {
        this(n, (List<v>)s.g());
    }
    
    public g(final int a, final List<v> b) {
        this.a = a;
        this.b = b;
    }
    
    private z a(final b b) {
        return new z(this.c(b));
    }
    
    private boolean a(final int n) {
        return (n & this.a) != 0x0;
    }
    
    private af b(final b b) {
        return new af(this.c(b));
    }
    
    private List<v> c(final b b) {
        if (this.a(32)) {
            return this.b;
        }
        final y y = new y(b.d);
        List<v> b2 = this.b;
        while (y.a() > 0) {
            final int h = y.h();
            final int h2 = y.h();
            final int c = y.c();
            if (h == 134) {
                final ArrayList list = new ArrayList();
                final int h3 = y.h();
                int n = 0;
                while (true) {
                    b2 = list;
                    if (n >= (h3 & 0x1F)) {
                        break;
                    }
                    final String f = y.f(3);
                    final int h4 = y.h();
                    boolean b3 = true;
                    final boolean b4 = (h4 & 0x80) != 0x0;
                    int n2;
                    String s;
                    if (b4) {
                        n2 = (h4 & 0x3F);
                        s = "application/cea-708";
                    }
                    else {
                        s = "application/cea-608";
                        n2 = 1;
                    }
                    final byte b5 = (byte)y.h();
                    y.e(1);
                    List a;
                    if (b4) {
                        if ((b5 & 0x40) == 0x0) {
                            b3 = false;
                        }
                        a = e.a(b3);
                    }
                    else {
                        a = null;
                    }
                    list.add(new v.a().f(s).c(f).p(n2).a(a).a());
                    ++n;
                }
            }
            y.d(c + h2);
        }
        return b2;
    }
    
    @Override
    public SparseArray<ad> a() {
        return (SparseArray<ad>)new SparseArray();
    }
    
    @Nullable
    @Override
    public ad a(final int n, final b b) {
        if (n == 2) {
            return new t(new k(this.b(b)));
        }
        if (n == 3 || n == 4) {
            return new t(new q(b.b));
        }
        if (n == 21) {
            return new t(new o());
        }
        final ad ad = null;
        final ad ad2 = null;
        final ad ad3 = null;
        final ad ad4 = null;
        if (n == 27) {
            ad ad5;
            if (this.a(4)) {
                ad5 = ad3;
            }
            else {
                ad5 = new t(new m(this.a(b), this.a(1), this.a(8)));
            }
            return ad5;
        }
        if (n == 36) {
            return new t(new n(this.a(b)));
        }
        if (n != 89) {
            if (n != 138) {
                if (n == 172) {
                    return new t(new com.applovin.exoplayer2.e.i.d(b.b));
                }
                if (n != 257) {
                    if (n != 129) {
                        if (n != 130) {
                            if (n == 134) {
                                ad ad6;
                                if (this.a(16)) {
                                    ad6 = ad2;
                                }
                                else {
                                    ad6 = new com.applovin.exoplayer2.e.i.y(new com.applovin.exoplayer2.e.i.s("application/x-scte35"));
                                }
                                return ad6;
                            }
                            if (n != 135) {
                                switch (n) {
                                    default: {
                                        return null;
                                    }
                                    case 17: {
                                        ad ad7;
                                        if (this.a(2)) {
                                            ad7 = ad4;
                                        }
                                        else {
                                            ad7 = new t(new p(b.b));
                                        }
                                        return ad7;
                                    }
                                    case 16: {
                                        return new t(new l(this.b(b)));
                                    }
                                    case 15: {
                                        ad ad8;
                                        if (this.a(2)) {
                                            ad8 = ad;
                                        }
                                        else {
                                            ad8 = new t(new f(false, b.b));
                                        }
                                        return ad8;
                                    }
                                }
                            }
                        }
                        else {
                            if (!this.a(64)) {
                                return null;
                            }
                            return new t(new h(b.b));
                        }
                    }
                    return new t(new com.applovin.exoplayer2.e.i.b(b.b));
                }
                return new com.applovin.exoplayer2.e.i.y(new com.applovin.exoplayer2.e.i.s("application/vnd.dvb.ait"));
            }
            return new t(new h(b.b));
        }
        return new t(new i(b.c));
    }
}

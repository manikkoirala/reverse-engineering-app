// 
// Decompiled by Procyon v0.6.0
// 

package com.applovin.exoplayer2.e.i;

import com.applovin.exoplayer2.b.a;
import java.util.Collections;
import com.applovin.exoplayer2.ai;
import com.applovin.exoplayer2.v;
import com.applovin.exoplayer2.l.x;
import com.applovin.exoplayer2.l.y;
import androidx.annotation.Nullable;

public final class p implements j
{
    @Nullable
    private final String a;
    private final y b;
    private final x c;
    private com.applovin.exoplayer2.e.x d;
    private String e;
    private v f;
    private int g;
    private int h;
    private int i;
    private int j;
    private long k;
    private boolean l;
    private int m;
    private int n;
    private int o;
    private boolean p;
    private long q;
    private int r;
    private long s;
    private int t;
    @Nullable
    private String u;
    
    public p(@Nullable final String a) {
        this.a = a;
        final y b = new y(1024);
        this.b = b;
        this.c = new x(b.d());
        this.k = -9223372036854775807L;
    }
    
    private void a(final int n) {
        this.b.a(n);
        this.c.a(this.b.d());
    }
    
    private void a(final x x) throws ai {
        if (!x.e()) {
            this.l = true;
            this.b(x);
        }
        else if (!this.l) {
            return;
        }
        if (this.m != 0) {
            throw ai.b(null, null);
        }
        if (this.n == 0) {
            this.a(x, this.e(x));
            if (this.p) {
                x.b((int)this.q);
            }
            return;
        }
        throw ai.b(null, null);
    }
    
    private void a(final x x, final int n) {
        final int b = x.b();
        if ((b & 0x7) == 0x0) {
            this.b.d(b >> 3);
        }
        else {
            x.a(this.b.d(), 0, n * 8);
            this.b.d(0);
        }
        this.d.a(this.b, n);
        final long k = this.k;
        if (k != -9223372036854775807L) {
            this.d.a(k, 1, n, 0, null);
            this.k += this.s;
        }
    }
    
    private void b(final x x) throws ai {
        final int c = x.c(1);
        int c2;
        if (c == 1) {
            c2 = x.c(1);
        }
        else {
            c2 = 0;
        }
        this.m = c2;
        if (c2 != 0) {
            throw ai.b(null, null);
        }
        if (c == 1) {
            f(x);
        }
        if (!x.e()) {
            throw ai.b(null, null);
        }
        this.n = x.c(6);
        final int c3 = x.c(4);
        final int c4 = x.c(3);
        if (c3 == 0 && c4 == 0) {
            if (c == 0) {
                final int b = x.b();
                final int d = this.d(x);
                x.a(b);
                final byte[] o = new byte[(d + 7) / 8];
                x.a(o, 0, d);
                final v a = new v.a().a(this.e).f("audio/mp4a-latm").d(this.u).k(this.t).l(this.r).a(Collections.singletonList(o)).c(this.a).a();
                if (!a.equals(this.f)) {
                    this.f = a;
                    this.s = 1024000000L / a.z;
                    this.d.a(a);
                }
            }
            else {
                x.b((int)f(x) - this.d(x));
            }
            this.c(x);
            final boolean e = x.e();
            this.p = e;
            this.q = 0L;
            if (e) {
                if (c == 1) {
                    this.q = f(x);
                }
                else {
                    boolean e2;
                    do {
                        e2 = x.e();
                        this.q = (this.q << 8) + x.c(8);
                    } while (e2);
                }
            }
            if (x.e()) {
                x.b(8);
            }
            return;
        }
        throw ai.b(null, null);
    }
    
    private void c(final x x) {
        final int c = x.c(3);
        this.o = c;
        if (c != 0) {
            if (c != 1) {
                if (c != 3 && c != 4 && c != 5) {
                    if (c != 6 && c != 7) {
                        throw new IllegalStateException();
                    }
                    x.b(1);
                }
                else {
                    x.b(6);
                }
            }
            else {
                x.b(9);
            }
        }
        else {
            x.b(8);
        }
    }
    
    private int d(final x x) throws ai {
        final int a = x.a();
        final a.a a2 = com.applovin.exoplayer2.b.a.a(x, true);
        this.u = a2.c;
        this.r = a2.a;
        this.t = a2.b;
        return a - x.a();
    }
    
    private int e(final x x) throws ai {
        if (this.o == 0) {
            int n = 0;
            int i;
            do {
                i = x.c(8);
                n += i;
            } while (i == 255);
            return n;
        }
        throw ai.b(null, null);
    }
    
    private static long f(final x x) {
        return x.c((x.c(2) + 1) * 8);
    }
    
    @Override
    public void a() {
        this.g = 0;
        this.k = -9223372036854775807L;
        this.l = false;
    }
    
    @Override
    public void a(final long k, final int n) {
        if (k != -9223372036854775807L) {
            this.k = k;
        }
    }
    
    @Override
    public void a(final com.applovin.exoplayer2.e.j j, final ad.d d) {
        d.a();
        this.d = j.a(d.b(), 1);
        this.e = d.c();
    }
    
    @Override
    public void a(final y y) throws ai {
        com.applovin.exoplayer2.l.a.a((Object)this.d);
        while (y.a() > 0) {
            final int g = this.g;
            if (g != 0) {
                if (g != 1) {
                    if (g != 2) {
                        if (g != 3) {
                            throw new IllegalStateException();
                        }
                        final int min = Math.min(y.a(), this.i - this.h);
                        y.a(this.c.a, this.h, min);
                        if ((this.h += min) != this.i) {
                            continue;
                        }
                        this.c.a(0);
                        this.a(this.c);
                        this.g = 0;
                    }
                    else {
                        if ((this.i = ((this.j & 0xFFFFFF1F) << 8 | y.h())) > this.b.d().length) {
                            this.a(this.i);
                        }
                        this.h = 0;
                        this.g = 3;
                    }
                }
                else {
                    final int h = y.h();
                    if ((h & 0xE0) == 0xE0) {
                        this.j = h;
                        this.g = 2;
                    }
                    else {
                        if (h == 86) {
                            continue;
                        }
                        this.g = 0;
                    }
                }
            }
            else {
                if (y.h() != 86) {
                    continue;
                }
                this.g = 1;
            }
        }
    }
    
    @Override
    public void b() {
    }
}

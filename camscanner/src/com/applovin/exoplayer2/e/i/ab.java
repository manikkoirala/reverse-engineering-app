// 
// Decompiled by Procyon v0.6.0
// 

package com.applovin.exoplayer2.e.i;

import com.applovin.exoplayer2.l.q;
import java.io.IOException;
import com.applovin.exoplayer2.e.u;
import com.applovin.exoplayer2.l.ai;
import com.applovin.exoplayer2.e.i;
import com.applovin.exoplayer2.l.y;
import com.applovin.exoplayer2.l.ag;

final class ab
{
    private final int a;
    private final ag b;
    private final y c;
    private boolean d;
    private boolean e;
    private boolean f;
    private long g;
    private long h;
    private long i;
    
    ab(final int a) {
        this.a = a;
        this.b = new ag(0L);
        this.g = -9223372036854775807L;
        this.h = -9223372036854775807L;
        this.i = -9223372036854775807L;
        this.c = new y();
    }
    
    private int a(final i i) {
        this.c.a(ai.f);
        this.d = true;
        i.a();
        return 0;
    }
    
    private long a(final y y, final int n) {
        for (int i = y.c(); i < y.b(); ++i) {
            if (y.d()[i] == 71) {
                final long a = ae.a(y, i, n);
                if (a != -9223372036854775807L) {
                    return a;
                }
            }
        }
        return -9223372036854775807L;
    }
    
    private int b(final i i, final u u, final int n) throws IOException {
        final int n2 = (int)Math.min(this.a, i.d());
        final long c = i.c();
        final long a = 0;
        if (c != a) {
            u.a = a;
            return 1;
        }
        this.c.a(n2);
        i.a();
        i.d(this.c.d(), 0, n2);
        this.g = this.a(this.c, n);
        this.e = true;
        return 0;
    }
    
    private long b(final y y, final int n) {
        final int c = y.c();
        final int b = y.b();
        for (int i = b - 188; i >= c; --i) {
            if (ae.a(y.d(), c, b, i)) {
                final long a = ae.a(y, i, n);
                if (a != -9223372036854775807L) {
                    return a;
                }
            }
        }
        return -9223372036854775807L;
    }
    
    private int c(final i i, final u u, final int n) throws IOException {
        final long d = i.d();
        final int n2 = (int)Math.min(this.a, d);
        final long a = d - n2;
        if (i.c() != a) {
            u.a = a;
            return 1;
        }
        this.c.a(n2);
        i.a();
        i.d(this.c.d(), 0, n2);
        this.h = this.b(this.c, n);
        this.f = true;
        return 0;
    }
    
    public int a(final i i, final u u, final int n) throws IOException {
        if (n <= 0) {
            return this.a(i);
        }
        if (!this.f) {
            return this.c(i, u, n);
        }
        if (this.h == -9223372036854775807L) {
            return this.a(i);
        }
        if (!this.e) {
            return this.b(i, u, n);
        }
        final long g = this.g;
        if (g == -9223372036854775807L) {
            return this.a(i);
        }
        final long j = this.b.b(this.h) - this.b.b(g);
        this.i = j;
        if (j < 0L) {
            final StringBuilder sb = new StringBuilder();
            sb.append("Invalid duration: ");
            sb.append(this.i);
            sb.append(". Using TIME_UNSET instead.");
            q.c("TsDurationReader", sb.toString());
            this.i = -9223372036854775807L;
        }
        return this.a(i);
    }
    
    public boolean a() {
        return this.d;
    }
    
    public long b() {
        return this.i;
    }
    
    public ag c() {
        return this.b;
    }
}

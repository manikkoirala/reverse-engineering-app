// 
// Decompiled by Procyon v0.6.0
// 

package com.applovin.exoplayer2.e.i;

import com.applovin.exoplayer2.l.ai;
import com.applovin.exoplayer2.l.a;
import java.util.Collections;
import java.util.Arrays;
import com.applovin.exoplayer2.v;
import android.util.Pair;
import com.applovin.exoplayer2.l.y;
import androidx.annotation.Nullable;
import com.applovin.exoplayer2.e.x;

public final class k implements j
{
    private static final double[] c;
    private String a;
    private x b;
    @Nullable
    private final af d;
    @Nullable
    private final y e;
    @Nullable
    private final r f;
    private final boolean[] g;
    private final a h;
    private long i;
    private boolean j;
    private boolean k;
    private long l;
    private long m;
    private long n;
    private long o;
    private boolean p;
    private boolean q;
    
    static {
        c = new double[] { 23.976023976023978, 24.0, 25.0, 29.97002997002997, 30.0, 50.0, 59.94005994005994, 60.0 };
    }
    
    public k() {
        this(null);
    }
    
    k(@Nullable final af d) {
        this.d = d;
        this.g = new boolean[4];
        this.h = new a(128);
        if (d != null) {
            this.f = new r(178, 128);
            this.e = new y();
        }
        else {
            this.f = null;
            this.e = null;
        }
        this.m = -9223372036854775807L;
        this.o = -9223372036854775807L;
    }
    
    private static Pair<v, Long> a(final a a, final String s) {
        final byte[] copy = Arrays.copyOf(a.c, a.a);
        final byte b = copy[4];
        final int n = copy[5] & 0xFF;
        final byte b2 = copy[6];
        final int n2 = (b & 0xFF) << 4 | n >> 4;
        final int n3 = (n & 0xF) << 8 | (b2 & 0xFF);
        final int n4 = (copy[7] & 0xF0) >> 4;
        float n5 = 0.0f;
        Label_0162: {
            float n6;
            int n7;
            if (n4 != 2) {
                if (n4 != 3) {
                    if (n4 != 4) {
                        n5 = 1.0f;
                        break Label_0162;
                    }
                    n6 = (float)(n3 * 121);
                    n7 = n2 * 100;
                }
                else {
                    n6 = (float)(n3 * 16);
                    n7 = n2 * 9;
                }
            }
            else {
                n6 = (float)(n3 * 4);
                n7 = n2 * 3;
            }
            n5 = n6 / n7;
        }
        final v a2 = new v.a().a(s).f("video/mpeg2").g(n2).h(n3).b(n5).a(Collections.singletonList(copy)).a();
        final int n8 = (copy[7] & 0xF) - 1;
        if (n8 >= 0) {
            final double[] c = k.c;
            if (n8 < c.length) {
                final double n9 = c[n8];
                final byte b3 = copy[a.b + 9];
                final int n10 = (b3 & 0x60) >> 5;
                final int n11 = b3 & 0x1F;
                double n12 = n9;
                if (n10 != n11) {
                    n12 = n9 * ((n10 + 1.0) / (n11 + 1));
                }
                final long n13 = (long)(1000000.0 / n12);
                return (Pair<v, Long>)Pair.create((Object)a2, (Object)n13);
            }
        }
        final long n13 = 0L;
        return (Pair<v, Long>)Pair.create((Object)a2, (Object)n13);
    }
    
    @Override
    public void a() {
        com.applovin.exoplayer2.l.v.a(this.g);
        this.h.a();
        final r f = this.f;
        if (f != null) {
            f.a();
        }
        this.i = 0L;
        this.j = false;
        this.m = -9223372036854775807L;
        this.o = -9223372036854775807L;
    }
    
    @Override
    public void a(final long m, final int n) {
        this.m = m;
    }
    
    @Override
    public void a(final com.applovin.exoplayer2.e.j j, final ad.d d) {
        d.a();
        this.a = d.c();
        this.b = j.a(d.b(), 2);
        final af d2 = this.d;
        if (d2 != null) {
            d2.a(j, d);
        }
    }
    
    @Override
    public void a(final y y) {
        com.applovin.exoplayer2.l.a.a((Object)this.b);
        int c = y.c();
        final int b = y.b();
        final byte[] d = y.d();
        this.i += y.a();
        this.b.a(y, y.a());
        while (true) {
            final int a = com.applovin.exoplayer2.l.v.a(d, c, b, this.g);
            if (a == b) {
                break;
            }
            final byte[] d2 = y.d();
            final int n = a + 3;
            final int n2 = d2[n] & 0xFF;
            final int n3 = a - c;
            final boolean k = this.k;
            boolean q = false;
            if (!k) {
                if (n3 > 0) {
                    this.h.a(d, c, a);
                }
                int n4;
                if (n3 < 0) {
                    n4 = -n3;
                }
                else {
                    n4 = 0;
                }
                if (this.h.a(n2, n4)) {
                    final Pair<v, Long> a2 = a(this.h, (String)com.applovin.exoplayer2.l.a.b((Object)this.a));
                    this.b.a((v)a2.first);
                    this.l = (long)a2.second;
                    this.k = true;
                }
            }
            final r f = this.f;
            if (f != null) {
                int n5;
                if (n3 > 0) {
                    f.a(d, c, a);
                    n5 = 0;
                }
                else {
                    n5 = -n3;
                }
                if (this.f.b(n5)) {
                    final r f2 = this.f;
                    ((y)ai.a((Object)this.e)).a(this.f.a, com.applovin.exoplayer2.l.v.a(f2.a, f2.b));
                    ((af)ai.a((Object)this.d)).a(this.o, this.e);
                }
                if (n2 == 178 && y.d()[a + 2] == 1) {
                    this.f.a(n2);
                }
            }
            if (n2 != 0 && n2 != 179) {
                if (n2 == 184) {
                    this.p = true;
                }
            }
            else {
                final int n6 = b - a;
                if (this.q && this.k) {
                    final long o = this.o;
                    if (o != -9223372036854775807L) {
                        this.b.a(o, this.p ? 1 : 0, (int)(this.i - this.n) - n6, n6, null);
                    }
                }
                if (!this.j || this.q) {
                    this.n = this.i - n6;
                    long m = this.m;
                    if (m == -9223372036854775807L) {
                        final long o2 = this.o;
                        if (o2 != -9223372036854775807L) {
                            m = o2 + this.l;
                        }
                        else {
                            m = -9223372036854775807L;
                        }
                    }
                    this.o = m;
                    this.p = false;
                    this.m = -9223372036854775807L;
                    this.j = true;
                }
                if (n2 == 0) {
                    q = true;
                }
                this.q = q;
            }
            c = n;
        }
        if (!this.k) {
            this.h.a(d, c, b);
        }
        final r f3 = this.f;
        if (f3 != null) {
            f3.a(d, c, b);
        }
    }
    
    @Override
    public void b() {
    }
    
    private static final class a
    {
        private static final byte[] d;
        public int a;
        public int b;
        public byte[] c;
        private boolean e;
        
        static {
            d = new byte[] { 0, 0, 1 };
        }
        
        public a(final int n) {
            this.c = new byte[n];
        }
        
        public void a() {
            this.e = false;
            this.a = 0;
            this.b = 0;
        }
        
        public void a(final byte[] array, final int n, int n2) {
            if (!this.e) {
                return;
            }
            n2 -= n;
            final byte[] c = this.c;
            final int length = c.length;
            final int a = this.a;
            if (length < a + n2) {
                this.c = Arrays.copyOf(c, (a + n2) * 2);
            }
            System.arraycopy(array, n, this.c, this.a, n2);
            this.a += n2;
        }
        
        public boolean a(final int n, int n2) {
            if (this.e) {
                n2 = this.a - n2;
                this.a = n2;
                if (this.b != 0 || n != 181) {
                    this.e = false;
                    return true;
                }
                this.b = n2;
            }
            else if (n == 179) {
                this.e = true;
            }
            final byte[] d = com.applovin.exoplayer2.e.i.k.a.d;
            this.a(d, 0, d.length);
            return false;
        }
    }
}

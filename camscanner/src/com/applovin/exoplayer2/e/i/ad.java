// 
// Decompiled by Procyon v0.6.0
// 

package com.applovin.exoplayer2.e.i;

import android.util.SparseArray;
import java.util.Collections;
import java.util.List;
import androidx.annotation.Nullable;
import com.applovin.exoplayer2.ai;
import com.applovin.exoplayer2.l.y;
import com.applovin.exoplayer2.e.j;
import com.applovin.exoplayer2.l.ag;

public interface ad
{
    void a();
    
    void a(final ag p0, final j p1, final d p2);
    
    void a(final y p0, final int p1) throws ai;
    
    public static final class a
    {
        public final String a;
        public final int b;
        public final byte[] c;
        
        public a(final String a, final int b, final byte[] c) {
            this.a = a;
            this.b = b;
            this.c = c;
        }
    }
    
    public static final class b
    {
        public final int a;
        @Nullable
        public final String b;
        public final List<a> c;
        public final byte[] d;
        
        public b(final int a, @Nullable final String b, @Nullable final List<a> list, final byte[] d) {
            this.a = a;
            this.b = b;
            List<Object> c;
            if (list == null) {
                c = (List<Object>)Collections.emptyList();
            }
            else {
                c = (List<Object>)Collections.unmodifiableList((List<? extends a>)list);
            }
            this.c = (List<a>)c;
            this.d = d;
        }
    }
    
    public interface c
    {
        SparseArray<ad> a();
        
        @Nullable
        ad a(final int p0, final b p1);
    }
    
    public static final class d
    {
        private final String a;
        private final int b;
        private final int c;
        private int d;
        private String e;
        
        public d(final int n, final int n2) {
            this(Integer.MIN_VALUE, n, n2);
        }
        
        public d(final int i, final int b, final int c) {
            String string;
            if (i != Integer.MIN_VALUE) {
                final StringBuilder sb = new StringBuilder();
                sb.append(i);
                sb.append("/");
                string = sb.toString();
            }
            else {
                string = "";
            }
            this.a = string;
            this.b = b;
            this.c = c;
            this.d = Integer.MIN_VALUE;
            this.e = "";
        }
        
        private void d() {
            if (this.d != Integer.MIN_VALUE) {
                return;
            }
            throw new IllegalStateException("generateNewId() must be called before retrieving ids.");
        }
        
        public void a() {
            final int d = this.d;
            int b;
            if (d == Integer.MIN_VALUE) {
                b = this.b;
            }
            else {
                b = d + this.c;
            }
            this.d = b;
            final StringBuilder sb = new StringBuilder();
            sb.append(this.a);
            sb.append(this.d);
            this.e = sb.toString();
        }
        
        public int b() {
            this.d();
            return this.d;
        }
        
        public String c() {
            this.d();
            return this.e;
        }
    }
}

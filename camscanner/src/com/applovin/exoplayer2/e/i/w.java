// 
// Decompiled by Procyon v0.6.0
// 

package com.applovin.exoplayer2.e.i;

import com.applovin.exoplayer2.ai;
import com.applovin.exoplayer2.l.x;
import java.io.IOException;
import com.applovin.exoplayer2.l.a;
import com.applovin.exoplayer2.e.i;
import com.applovin.exoplayer2.e.j;
import androidx.annotation.Nullable;
import com.applovin.exoplayer2.l.y;
import android.util.SparseArray;
import com.applovin.exoplayer2.l.ag;
import com.applovin.exoplayer2.e.l;
import com.applovin.exoplayer2.e.h;

public final class w implements h
{
    public static final l a;
    private final ag b;
    private final SparseArray<a> c;
    private final y d;
    private final v e;
    private boolean f;
    private boolean g;
    private boolean h;
    private long i;
    @Nullable
    private u j;
    private j k;
    private boolean l;
    
    static {
        a = new Oo08();
    }
    
    public w() {
        this(new ag(0L));
    }
    
    public w(final ag b) {
        this.b = b;
        this.d = new y(4096);
        this.c = (SparseArray<a>)new SparseArray();
        this.e = new v();
    }
    
    private void a(final long n) {
        if (!this.l) {
            this.l = true;
            if (this.e.c() != -9223372036854775807L) {
                final u j = new u(this.e.b(), this.e.c(), n);
                this.j = j;
                this.k.a(j.a());
            }
            else {
                this.k.a(new com.applovin.exoplayer2.e.v.b(this.e.c()));
            }
        }
    }
    
    @Override
    public int a(final i i, final com.applovin.exoplayer2.e.u u) throws IOException {
        com.applovin.exoplayer2.l.a.a((Object)this.k);
        final long d = i.d();
        final long n = lcmp(d, -1L);
        if (n != 0 && !this.e.a()) {
            return this.e.a(i, u);
        }
        this.a(d);
        final u j = this.j;
        if (j != null && j.b()) {
            return this.j.a(i, u);
        }
        i.a();
        long n2;
        if (n != 0) {
            n2 = d - i.b();
        }
        else {
            n2 = -1L;
        }
        if (n2 != -1L && n2 < 4L) {
            return -1;
        }
        if (!i.b(this.d.d(), 0, 4, true)) {
            return -1;
        }
        this.d.d(0);
        final int q = this.d.q();
        if (q == 441) {
            return -1;
        }
        if (q == 442) {
            i.d(this.d.d(), 0, 10);
            this.d.d(9);
            i.b((this.d.h() & 0x7) + 14);
            return 0;
        }
        if (q == 443) {
            i.d(this.d.d(), 0, 2);
            this.d.d(0);
            i.b(this.d.i() + 6);
            return 0;
        }
        if ((q & 0xFFFFFF00) >> 8 != 1) {
            i.b(1);
            return 0;
        }
        final int n3 = q & 0xFF;
        a a2;
        final a a = a2 = (a)this.c.get(n3);
        if (!this.f) {
            a a3;
            if ((a3 = a) == null) {
                com.applovin.exoplayer2.e.i.j k;
                if (n3 == 189) {
                    k = new b();
                    this.g = true;
                    this.i = i.c();
                }
                else if ((n3 & 0xE0) == 0xC0) {
                    k = new q();
                    this.g = true;
                    this.i = i.c();
                }
                else if ((n3 & 0xF0) == 0xE0) {
                    k = new k();
                    this.h = true;
                    this.i = i.c();
                }
                else {
                    k = null;
                }
                a3 = a;
                if (k != null) {
                    k.a(this.k, new ad.d(n3, 256));
                    a3 = new a(k, this.b);
                    this.c.put(n3, (Object)a3);
                }
            }
            long n4;
            if (this.g && this.h) {
                n4 = this.i + 8192L;
            }
            else {
                n4 = 1048576L;
            }
            a2 = a3;
            if (i.c() > n4) {
                this.f = true;
                this.k.a();
                a2 = a3;
            }
        }
        i.d(this.d.d(), 0, 2);
        this.d.d(0);
        final int n5 = this.d.i() + 6;
        if (a2 == null) {
            i.b(n5);
        }
        else {
            this.d.a(n5);
            i.b(this.d.d(), 0, n5);
            this.d.d(6);
            a2.a(this.d);
            final y d2 = this.d;
            d2.c(d2.e());
        }
        return 0;
    }
    
    @Override
    public void a(long n, final long n2) {
        n = this.b.c();
        final int n3 = 1;
        final int n4 = 0;
        int n5;
        if ((n5 = ((n == -9223372036854775807L) ? 1 : 0)) == 0) {
            n = this.b.a();
            int n6;
            if (n != -9223372036854775807L && n != 0L && n != n2) {
                n6 = n3;
            }
            else {
                n6 = 0;
            }
            n5 = n6;
        }
        if (n5 != 0) {
            this.b.a(n2);
        }
        final u j = this.j;
        int i = n4;
        if (j != null) {
            j.a(n2);
            i = n4;
        }
        while (i < this.c.size()) {
            ((a)this.c.valueAt(i)).a();
            ++i;
        }
    }
    
    @Override
    public void a(final j k) {
        this.k = k;
    }
    
    @Override
    public boolean a(final i i) throws IOException {
        final byte[] array = new byte[14];
        boolean b = false;
        i.d(array, 0, 14);
        if (0x1BA != ((array[0] & 0xFF) << 24 | (array[1] & 0xFF) << 16 | (array[2] & 0xFF) << 8 | (array[3] & 0xFF))) {
            return false;
        }
        if ((array[4] & 0xC4) != 0x44) {
            return false;
        }
        if ((array[6] & 0x4) != 0x4) {
            return false;
        }
        if ((array[8] & 0x4) != 0x4) {
            return false;
        }
        if ((array[9] & 0x1) != 0x1) {
            return false;
        }
        if ((array[12] & 0x3) != 0x3) {
            return false;
        }
        i.c(array[13] & 0x7);
        i.d(array, 0, 3);
        if (0x1 == ((array[0] & 0xFF) << 16 | (array[1] & 0xFF) << 8 | (array[2] & 0xFF))) {
            b = true;
        }
        return b;
    }
    
    @Override
    public void c() {
    }
    
    private static final class a
    {
        private final com.applovin.exoplayer2.e.i.j a;
        private final ag b;
        private final x c;
        private boolean d;
        private boolean e;
        private boolean f;
        private int g;
        private long h;
        
        public a(final com.applovin.exoplayer2.e.i.j a, final ag b) {
            this.a = a;
            this.b = b;
            this.c = new x(new byte[64]);
        }
        
        private void b() {
            this.c.b(8);
            this.d = this.c.e();
            this.e = this.c.e();
            this.c.b(6);
            this.g = this.c.c(8);
        }
        
        private void c() {
            this.h = 0L;
            if (this.d) {
                this.c.b(4);
                final long n = this.c.c(3);
                this.c.b(1);
                final long n2 = this.c.c(15) << 15;
                this.c.b(1);
                final long n3 = this.c.c(15);
                this.c.b(1);
                if (!this.f && this.e) {
                    this.c.b(4);
                    final long n4 = this.c.c(3);
                    this.c.b(1);
                    final long n5 = this.c.c(15) << 15;
                    this.c.b(1);
                    final long n6 = this.c.c(15);
                    this.c.b(1);
                    this.b.b(n4 << 30 | n5 | n6);
                    this.f = true;
                }
                this.h = this.b.b(n << 30 | n2 | n3);
            }
        }
        
        public void a() {
            this.f = false;
            this.a.a();
        }
        
        public void a(final y y) throws ai {
            y.a(this.c.a, 0, 3);
            this.c.a(0);
            this.b();
            y.a(this.c.a, 0, this.g);
            this.c.a(0);
            this.c();
            this.a.a(this.h, 4);
            this.a.a(y);
            this.a.b();
        }
    }
}

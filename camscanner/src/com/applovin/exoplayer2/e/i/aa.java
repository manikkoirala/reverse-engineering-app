// 
// Decompiled by Procyon v0.6.0
// 

package com.applovin.exoplayer2.e.i;

import com.applovin.exoplayer2.l.ai;
import java.io.IOException;
import com.applovin.exoplayer2.e.i;
import com.applovin.exoplayer2.l.y;
import com.applovin.exoplayer2.l.ag;
import com.applovin.exoplayer2.e.a;

final class aa extends com.applovin.exoplayer2.e.a
{
    public aa(final ag ag, final long n, final long n2, final int n3, final int n4) {
        super((d)new b(), (f)new a(n3, ag, n4), n, 0L, n + 1L, 0L, n2, 188L, 940);
    }
    
    private static final class a implements f
    {
        private final ag a;
        private final y b;
        private final int c;
        private final int d;
        
        public a(final int c, final ag a, final int d) {
            this.c = c;
            this.a = a;
            this.d = d;
            this.b = new y();
        }
        
        private e a(final y y, final long n, final long n2) {
            final int b = y.b();
            long n3 = -1L;
            long n4 = -1L;
            long n5 = -9223372036854775807L;
            while (y.a() >= 188) {
                final int a = ae.a(y.d(), y.c(), b);
                final int n6 = a + 188;
                if (n6 > b) {
                    break;
                }
                final long a2 = ae.a(y, a, this.c);
                long n7 = n4;
                long b2 = n5;
                if (a2 != -9223372036854775807L) {
                    b2 = this.a.b(a2);
                    if (b2 > n) {
                        if (n5 == -9223372036854775807L) {
                            return e.a(b2, n2);
                        }
                        return e.a(n2 + n4);
                    }
                    else {
                        if (100000L + b2 > n) {
                            return e.a(n2 + a);
                        }
                        n7 = a;
                    }
                }
                y.d(n6);
                n3 = n6;
                n4 = n7;
                n5 = b2;
            }
            if (n5 != -9223372036854775807L) {
                return e.b(n5, n2 + n3);
            }
            return e.a;
        }
        
        @Override
        public e a(final i i, final long n) throws IOException {
            final long c = i.c();
            final int n2 = (int)Math.min(this.d, i.d() - c);
            this.b.a(n2);
            i.d(this.b.d(), 0, n2);
            return this.a(this.b, n, c);
        }
        
        @Override
        public void a() {
            this.b.a(ai.f);
        }
    }
}

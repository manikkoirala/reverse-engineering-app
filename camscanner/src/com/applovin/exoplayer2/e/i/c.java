// 
// Decompiled by Procyon v0.6.0
// 

package com.applovin.exoplayer2.e.i;

import com.applovin.exoplayer2.e.v;
import com.applovin.exoplayer2.e.j;
import java.io.IOException;
import com.applovin.exoplayer2.e.u;
import com.applovin.exoplayer2.e.i;
import com.applovin.exoplayer2.l.y;
import com.applovin.exoplayer2.e.l;
import com.applovin.exoplayer2.e.h;

public final class c implements h
{
    public static final l a;
    private final d b;
    private final y c;
    private boolean d;
    
    static {
        a = new \u3007o\u3007();
    }
    
    public c() {
        this.b = new d();
        this.c = new y(16384);
    }
    
    @Override
    public int a(final i i, final u u) throws IOException {
        final int a = i.a(this.c.d(), 0, 16384);
        if (a == -1) {
            return -1;
        }
        this.c.d(0);
        this.c.c(a);
        if (!this.d) {
            this.b.a(0L, 4);
            this.d = true;
        }
        this.b.a(this.c);
        return 0;
    }
    
    @Override
    public void a(final long n, final long n2) {
        this.d = false;
        this.b.a();
    }
    
    @Override
    public void a(final j j) {
        this.b.a(j, new ad.d(0, 1));
        j.a();
        j.a(new v.b(-9223372036854775807L));
    }
    
    @Override
    public boolean a(final i i) throws IOException {
        final y y = new y(10);
        int n = 0;
        while (true) {
            i.d(y.d(), 0, 10);
            y.d(0);
            if (y.m() != 4801587) {
                break;
            }
            y.e(3);
            final int v = y.v();
            n += v + 10;
            i.c(v);
        }
        i.a();
        i.c(n);
        int n2 = n;
        while (true) {
            int n3 = 0;
            while (true) {
                i.d(y.d(), 0, 7);
                y.d(0);
                final int j = y.i();
                if (j != 44096 && j != 44097) {
                    i.a();
                    if (++n2 - n >= 8192) {
                        return false;
                    }
                    i.c(n2);
                    break;
                }
                else {
                    if (++n3 >= 4) {
                        return true;
                    }
                    final int a = com.applovin.exoplayer2.b.c.a(y.d(), j);
                    if (a == -1) {
                        return false;
                    }
                    i.c(a - 7);
                }
            }
        }
    }
    
    @Override
    public void c() {
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.applovin.exoplayer2.e.i;

import com.applovin.exoplayer2.l.ai;
import com.applovin.exoplayer2.l.a;
import java.util.Collections;
import com.applovin.exoplayer2.l.e;
import com.applovin.exoplayer2.l.q;
import com.applovin.exoplayer2.v;
import androidx.annotation.Nullable;
import com.applovin.exoplayer2.l.y;
import com.applovin.exoplayer2.e.x;

public final class n implements j
{
    private final z a;
    private String b;
    private x c;
    private a d;
    private boolean e;
    private final boolean[] f;
    private final r g;
    private final r h;
    private final r i;
    private final r j;
    private final r k;
    private long l;
    private long m;
    private final y n;
    
    public n(final z a) {
        this.a = a;
        this.f = new boolean[3];
        this.g = new r(32, 128);
        this.h = new r(33, 128);
        this.i = new r(34, 128);
        this.j = new r(39, 128);
        this.k = new r(40, 128);
        this.m = -9223372036854775807L;
        this.n = new y();
    }
    
    private static v a(@Nullable final String s, final r r, final r r2, final r r3) {
        final int b = r.b;
        final byte[] o = new byte[r2.b + b + r3.b];
        System.arraycopy(r.a, 0, o, 0, b);
        System.arraycopy(r2.a, 0, o, r.b, r2.b);
        System.arraycopy(r3.a, 0, o, r.b + r2.b, r3.b);
        final com.applovin.exoplayer2.l.z z = new com.applovin.exoplayer2.l.z(r2.a, 0, r2.b);
        z.a(44);
        final int c = z.c(3);
        z.a();
        z.a(88);
        z.a(8);
        int i = 0;
        int n = 0;
        while (i < c) {
            int n2 = n;
            if (z.b()) {
                n2 = n + 89;
            }
            n = n2;
            if (z.b()) {
                n = n2 + 8;
            }
            ++i;
        }
        z.a(n);
        if (c > 0) {
            z.a((8 - c) * 2);
        }
        z.d();
        final int d = z.d();
        if (d == 3) {
            z.a();
        }
        final int d2 = z.d();
        final int d3 = z.d();
        int n3 = d2;
        int n4 = d3;
        if (z.b()) {
            final int d4 = z.d();
            final int d5 = z.d();
            final int d6 = z.d();
            final int d7 = z.d();
            int n5;
            if (d != 1 && d != 2) {
                n5 = 1;
            }
            else {
                n5 = 2;
            }
            int n6;
            if (d == 1) {
                n6 = 2;
            }
            else {
                n6 = 1;
            }
            n3 = d2 - n5 * (d4 + d5);
            n4 = d3 - n6 * (d6 + d7);
        }
        z.d();
        z.d();
        final int d8 = z.d();
        int j;
        if (z.b()) {
            j = 0;
        }
        else {
            j = c;
        }
        while (j <= c) {
            z.d();
            z.d();
            z.d();
            ++j;
        }
        z.d();
        z.d();
        z.d();
        z.d();
        z.d();
        z.d();
        if (z.b() && z.b()) {
            a(z);
        }
        z.a(2);
        if (z.b()) {
            z.a(8);
            z.d();
            z.d();
            z.a();
        }
        b(z);
        if (z.b()) {
            for (int k = 0; k < z.d(); ++k) {
                z.a(d8 + 4 + 1);
            }
        }
        z.a(2);
        final boolean b2 = z.b();
        float n8;
        final float n7 = n8 = 1.0f;
        int n9 = n4;
        if (b2) {
            float n10 = n7;
            if (z.b()) {
                final int c2 = z.c(8);
                if (c2 == 255) {
                    final int c3 = z.c(16);
                    final int c4 = z.c(16);
                    n10 = n7;
                    if (c3 != 0) {
                        n10 = n7;
                        if (c4 != 0) {
                            n10 = c3 / (float)c4;
                        }
                    }
                }
                else {
                    final float[] b3 = com.applovin.exoplayer2.l.v.b;
                    if (c2 < b3.length) {
                        n10 = b3[c2];
                    }
                    else {
                        final StringBuilder sb = new StringBuilder();
                        sb.append("Unexpected aspect_ratio_idc value: ");
                        sb.append(c2);
                        q.c("H265Reader", sb.toString());
                        n10 = n7;
                    }
                }
            }
            if (z.b()) {
                z.a();
            }
            if (z.b()) {
                z.a(4);
                if (z.b()) {
                    z.a(24);
                }
            }
            if (z.b()) {
                z.d();
                z.d();
            }
            z.a();
            n8 = n10;
            n9 = n4;
            if (z.b()) {
                n9 = n4 * 2;
                n8 = n10;
            }
        }
        z.a(r2.a, 0, r2.b);
        z.a(24);
        return new v.a().a(s).f("video/hevc").d(e.a(z)).g(n3).h(n9).b(n8).a(Collections.singletonList(o)).a();
    }
    
    private void a(final long n, final int n2, final int n3, final long n4) {
        this.d.a(n, n2, n3, n4, this.e);
        if (!this.e) {
            this.g.a(n3);
            this.h.a(n3);
            this.i.a(n3);
        }
        this.j.a(n3);
        this.k.a(n3);
    }
    
    private static void a(final com.applovin.exoplayer2.l.z z) {
        for (int i = 0; i < 4; ++i) {
            int n2;
            for (int j = 0; j < 6; j += n2) {
                final boolean b = z.b();
                final int n = 1;
                if (!b) {
                    z.d();
                }
                else {
                    final int min = Math.min(64, 1 << (i << 1) + 4);
                    if (i > 1) {
                        z.e();
                    }
                    for (int k = 0; k < min; ++k) {
                        z.e();
                    }
                }
                n2 = n;
                if (i == 3) {
                    n2 = 3;
                }
            }
        }
    }
    
    private void a(final byte[] array, final int n, final int n2) {
        this.d.a(array, n, n2);
        if (!this.e) {
            this.g.a(array, n, n2);
            this.h.a(array, n, n2);
            this.i.a(array, n, n2);
        }
        this.j.a(array, n, n2);
        this.k.a(array, n, n2);
    }
    
    private void b(final long n, int n2, final int n3, final long n4) {
        this.d.a(n, n2, this.e);
        if (!this.e) {
            this.g.b(n3);
            this.h.b(n3);
            this.i.b(n3);
            if (this.g.b() && this.h.b() && this.i.b()) {
                this.c.a(a(this.b, this.g, this.h, this.i));
                this.e = true;
            }
        }
        if (this.j.b(n3)) {
            final r j = this.j;
            n2 = com.applovin.exoplayer2.l.v.a(j.a, j.b);
            this.n.a(this.j.a, n2);
            this.n.e(5);
            this.a.a(n4, this.n);
        }
        if (this.k.b(n3)) {
            final r k = this.k;
            n2 = com.applovin.exoplayer2.l.v.a(k.a, k.b);
            this.n.a(this.k.a, n2);
            this.n.e(5);
            this.a.a(n4, this.n);
        }
    }
    
    private static void b(final com.applovin.exoplayer2.l.z z) {
        final int d = z.d();
        int i = 0;
        boolean b = false;
        int n = 0;
        while (i < d) {
            if (i != 0) {
                b = z.b();
            }
            int n3;
            if (b) {
                z.a();
                z.d();
                int n2 = 0;
                while (true) {
                    n3 = n;
                    if (n2 > n) {
                        break;
                    }
                    if (z.b()) {
                        z.a();
                    }
                    ++n2;
                }
            }
            else {
                final int d2 = z.d();
                final int d3 = z.d();
                for (int j = 0; j < d2; ++j) {
                    z.d();
                    z.a();
                }
                for (int k = 0; k < d3; ++k) {
                    z.d();
                    z.a();
                }
                n3 = d2 + d3;
            }
            ++i;
            n = n3;
        }
    }
    
    private void c() {
        com.applovin.exoplayer2.l.a.a((Object)this.c);
        ai.a((Object)this.d);
    }
    
    @Override
    public void a() {
        this.l = 0L;
        this.m = -9223372036854775807L;
        com.applovin.exoplayer2.l.v.a(this.f);
        this.g.a();
        this.h.a();
        this.i.a();
        this.j.a();
        this.k.a();
        final a d = this.d;
        if (d != null) {
            d.a();
        }
    }
    
    @Override
    public void a(final long m, final int n) {
        if (m != -9223372036854775807L) {
            this.m = m;
        }
    }
    
    @Override
    public void a(final com.applovin.exoplayer2.e.j j, final ad.d d) {
        d.a();
        this.b = d.c();
        final x a = j.a(d.b(), 2);
        this.c = a;
        this.d = new a(a);
        this.a.a(j, d);
    }
    
    @Override
    public void a(final y y) {
        this.c();
        while (y.a() > 0) {
            int i = y.c();
            final int b = y.b();
            final byte[] d = y.d();
            this.l += y.a();
            this.c.a(y, y.a());
            while (i < b) {
                final int a = com.applovin.exoplayer2.l.v.a(d, i, b, this.f);
                if (a == b) {
                    this.a(d, i, b);
                    return;
                }
                final int c = com.applovin.exoplayer2.l.v.c(d, a);
                final int n = a - i;
                if (n > 0) {
                    this.a(d, i, a);
                }
                final int n2 = b - a;
                final long n3 = this.l - n2;
                int n4;
                if (n < 0) {
                    n4 = -n;
                }
                else {
                    n4 = 0;
                }
                this.b(n3, n2, n4, this.m);
                this.a(n3, n2, c, this.m);
                i = a + 3;
            }
        }
    }
    
    @Override
    public void b() {
    }
    
    private static final class a
    {
        private final x a;
        private long b;
        private boolean c;
        private int d;
        private long e;
        private boolean f;
        private boolean g;
        private boolean h;
        private boolean i;
        private boolean j;
        private long k;
        private long l;
        private boolean m;
        
        public a(final x a) {
            this.a = a;
        }
        
        private void a(final int n) {
            final long l = this.l;
            if (l == -9223372036854775807L) {
                return;
            }
            this.a.a(l, this.m ? 1 : 0, (int)(this.b - this.k), n, null);
        }
        
        private static boolean b(final int n) {
            return (32 <= n && n <= 35) || n == 39;
        }
        
        private static boolean c(final int n) {
            return n < 32 || n == 40;
        }
        
        public void a() {
            this.f = false;
            this.g = false;
            this.h = false;
            this.i = false;
            this.j = false;
        }
        
        public void a(final long b, final int n, final int n2, final long e, final boolean b2) {
            final boolean b3 = false;
            this.g = false;
            this.h = false;
            this.e = e;
            this.d = 0;
            this.b = b;
            if (!c(n2)) {
                if (this.i && !this.j) {
                    if (b2) {
                        this.a(n);
                    }
                    this.i = false;
                }
                if (b(n2)) {
                    this.h = (this.j ^ true);
                    this.j = true;
                }
            }
            boolean f = false;
            Label_0137: {
                if (!(this.c = (n2 >= 16 && n2 <= 21))) {
                    f = b3;
                    if (n2 > 9) {
                        break Label_0137;
                    }
                }
                f = true;
            }
            this.f = f;
        }
        
        public void a(final long n, final int n2, final boolean b) {
            if (this.j && this.g) {
                this.m = this.c;
                this.j = false;
            }
            else if (this.h || this.g) {
                if (b && this.i) {
                    this.a(n2 + (int)(n - this.b));
                }
                this.k = this.b;
                this.l = this.e;
                this.m = this.c;
                this.i = true;
            }
        }
        
        public void a(final byte[] array, final int n, final int n2) {
            if (this.f) {
                final int d = this.d;
                final int n3 = n + 2 - d;
                if (n3 < n2) {
                    this.g = ((array[n3] & 0x80) != 0x0);
                    this.f = false;
                }
                else {
                    this.d = d + (n2 - n);
                }
            }
        }
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.applovin.exoplayer2.e.i;

import java.util.Collections;
import com.applovin.exoplayer2.v;
import com.applovin.exoplayer2.l.y;
import com.applovin.exoplayer2.e.x;
import java.util.List;

public final class i implements j
{
    private final List<ad.a> a;
    private final x[] b;
    private boolean c;
    private int d;
    private int e;
    private long f;
    
    public i(final List<ad.a> a) {
        this.a = a;
        this.b = new x[a.size()];
        this.f = -9223372036854775807L;
    }
    
    private boolean a(final y y, final int n) {
        if (y.a() == 0) {
            return false;
        }
        if (y.h() != n) {
            this.c = false;
        }
        --this.d;
        return this.c;
    }
    
    @Override
    public void a() {
        this.c = false;
        this.f = -9223372036854775807L;
    }
    
    @Override
    public void a(final long f, final int n) {
        if ((n & 0x4) == 0x0) {
            return;
        }
        this.c = true;
        if (f != -9223372036854775807L) {
            this.f = f;
        }
        this.e = 0;
        this.d = 2;
    }
    
    @Override
    public void a(final com.applovin.exoplayer2.e.j j, final ad.d d) {
        for (int i = 0; i < this.b.length; ++i) {
            final ad.a a = this.a.get(i);
            d.a();
            final x a2 = j.a(d.b(), 3);
            a2.a(new v.a().a(d.c()).f("application/dvbsubs").a(Collections.singletonList(a.c)).c(a.a).a());
            this.b[i] = a2;
        }
    }
    
    @Override
    public void a(final y y) {
        if (this.c) {
            if (this.d == 2 && !this.a(y, 32)) {
                return;
            }
            final int d = this.d;
            int i = 0;
            if (d == 1 && !this.a(y, 0)) {
                return;
            }
            final int c = y.c();
            final int a = y.a();
            for (x[] b = this.b; i < b.length; ++i) {
                final x x = b[i];
                y.d(c);
                x.a(y, a);
            }
            this.e += a;
        }
    }
    
    @Override
    public void b() {
        if (this.c) {
            if (this.f != -9223372036854775807L) {
                final x[] b = this.b;
                for (int length = b.length, i = 0; i < length; ++i) {
                    b[i].a(this.f, 1, this.e, 0, null);
                }
            }
            this.c = false;
        }
    }
}

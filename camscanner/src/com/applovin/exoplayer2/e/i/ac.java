// 
// Decompiled by Procyon v0.6.0
// 

package com.applovin.exoplayer2.e.i;

import java.util.Arrays;
import com.applovin.exoplayer2.e.u;
import java.io.IOException;
import com.applovin.exoplayer2.e.i;
import com.applovin.exoplayer2.e.v;
import com.applovin.exoplayer2.ai;
import java.util.Collections;
import java.util.ArrayList;
import com.applovin.exoplayer2.l.a;
import androidx.annotation.Nullable;
import com.applovin.exoplayer2.e.j;
import android.util.SparseBooleanArray;
import android.util.SparseArray;
import android.util.SparseIntArray;
import com.applovin.exoplayer2.l.y;
import com.applovin.exoplayer2.l.ag;
import java.util.List;
import com.applovin.exoplayer2.e.l;
import com.applovin.exoplayer2.e.h;

public final class ac implements h
{
    public static final l a;
    private final int b;
    private final int c;
    private final List<ag> d;
    private final y e;
    private final SparseIntArray f;
    private final ad.c g;
    private final SparseArray<ad> h;
    private final SparseBooleanArray i;
    private final SparseBooleanArray j;
    private final ab k;
    private aa l;
    private j m;
    private int n;
    private boolean o;
    private boolean p;
    private boolean q;
    @Nullable
    private ad r;
    private int s;
    private int t;
    
    static {
        a = new \u3007o00\u3007\u3007Oo();
    }
    
    public ac() {
        this(0);
    }
    
    public ac(final int n) {
        this(1, n, 112800);
    }
    
    public ac(final int n, final int n2, final int n3) {
        this(n, new ag(0L), new g(n2), n3);
    }
    
    public ac(final int b, final ag o, final ad.c c, final int c2) {
        this.g = (ad.c)com.applovin.exoplayer2.l.a.b((Object)c);
        this.c = c2;
        this.b = b;
        if (b != 1 && b != 2) {
            (this.d = new ArrayList<ag>()).add(o);
        }
        else {
            this.d = Collections.singletonList(o);
        }
        this.e = new y(new byte[9400], 0);
        this.i = new SparseBooleanArray();
        this.j = new SparseBooleanArray();
        this.h = (SparseArray<ad>)new SparseArray();
        this.f = new SparseIntArray();
        this.k = new ab(c2);
        this.m = com.applovin.exoplayer2.e.j.a;
        this.t = -1;
        this.b();
    }
    
    private int a() throws ai {
        final int c = this.e.c();
        final int b = this.e.b();
        final int a = ae.a(this.e.d(), c, b);
        this.e.d(a);
        final int n = a + 188;
        if (n > b) {
            final int s = this.s + (a - c);
            this.s = s;
            if (this.b == 2) {
                if (s > 376) {
                    throw ai.b("Cannot find sync byte. Most likely not a Transport Stream.", null);
                }
            }
        }
        else {
            this.s = 0;
        }
        return n;
    }
    
    private void a(final long n) {
        if (!this.p) {
            this.p = true;
            if (this.k.b() != -9223372036854775807L) {
                final aa l = new aa(this.k.c(), this.k.b(), n, this.t, this.c);
                this.l = l;
                this.m.a(l.a());
            }
            else {
                this.m.a(new v.b(this.k.b()));
            }
        }
    }
    
    private boolean a(final int n) {
        if (this.b != 2 && !this.o) {
            final SparseBooleanArray j = this.j;
            final boolean b = false;
            if (j.get(n, false)) {
                return b;
            }
        }
        return true;
    }
    
    private void b() {
        this.i.clear();
        this.h.clear();
        final SparseArray<ad> a = this.g.a();
        for (int size = a.size(), i = 0; i < size; ++i) {
            this.h.put(a.keyAt(i), (Object)a.valueAt(i));
        }
        this.h.put(0, (Object)new com.applovin.exoplayer2.e.i.y(new a()));
        this.r = null;
    }
    
    private boolean b(final i i) throws IOException {
        final byte[] d = this.e.d();
        if (9400 - this.e.c() < 188) {
            final int a = this.e.a();
            if (a > 0) {
                System.arraycopy(d, this.e.c(), d, 0, a);
            }
            this.e.a(d, a);
        }
        while (this.e.a() < 188) {
            final int b = this.e.b();
            final int a2 = i.a(d, b, 9400 - b);
            if (a2 == -1) {
                return false;
            }
            this.e.c(b + a2);
        }
        return true;
    }
    
    @Override
    public int a(final i i, final u u) throws IOException {
        final long d = i.d();
        if (this.o) {
            if (d != -1L && this.b != 2 && !this.k.a()) {
                return this.k.a(i, u, this.t);
            }
            this.a(d);
            if (this.q) {
                this.q = false;
                this.a(0L, 0L);
                if (i.c() != 0L) {
                    u.a = 0L;
                    return 1;
                }
            }
            final aa l = this.l;
            if (l != null && l.b()) {
                return this.l.a(i, u);
            }
        }
        if (!this.b(i)) {
            return -1;
        }
        final int a = this.a();
        final int b = this.e.b();
        if (a > b) {
            return 0;
        }
        final int q = this.e.q();
        if ((0x800000 & q) != 0x0) {
            this.e.d(a);
            return 0;
        }
        final int n = ((0x400000 & q) != 0x0 | false) ? 1 : 0;
        final int n2 = (0x1FFF00 & q) >> 8;
        final boolean b2 = (q & 0x20) != 0x0;
        ad ad;
        if ((q & 0x10) != 0x0) {
            ad = (ad)this.h.get(n2);
        }
        else {
            ad = null;
        }
        if (ad == null) {
            this.e.d(a);
            return 0;
        }
        if (this.b != 2) {
            final int n3 = q & 0xF;
            final int value = this.f.get(n2, n3 - 1);
            this.f.put(n2, n3);
            if (value == n3) {
                this.e.d(a);
                return 0;
            }
            if (n3 != (value + 1 & 0xF)) {
                ad.a();
            }
        }
        int n4 = n;
        if (b2) {
            final int h = this.e.h();
            int n5;
            if ((this.e.h() & 0x40) != 0x0) {
                n5 = 2;
            }
            else {
                n5 = 0;
            }
            n4 = (n | n5);
            this.e.e(h - 1);
        }
        final boolean o = this.o;
        if (this.a(n2)) {
            this.e.c(a);
            ad.a(this.e, n4);
            this.e.c(b);
        }
        if (this.b != 2 && !o && this.o && d != -1L) {
            this.q = true;
        }
        this.e.d(a);
        return 0;
    }
    
    @Override
    public void a(long a, final long n) {
        a.b(this.b != 2);
        for (int size = this.d.size(), i = 0; i < size; ++i) {
            final ag ag = this.d.get(i);
            boolean b;
            if (!(b = (ag.c() == -9223372036854775807L))) {
                a = ag.a();
                b = (a != -9223372036854775807L && a != 0L && a != n);
            }
            if (b) {
                ag.a(n);
            }
        }
        if (n != 0L) {
            final aa l = this.l;
            if (l != null) {
                l.a(n);
            }
        }
        this.e.a(0);
        this.f.clear();
        for (int j = 0; j < this.h.size(); ++j) {
            ((ad)this.h.valueAt(j)).a();
        }
        this.s = 0;
    }
    
    @Override
    public void a(final j m) {
        this.m = m;
    }
    
    @Override
    public boolean a(final i i) throws IOException {
        final byte[] d = this.e.d();
        i.d(d, 0, 940);
        int j = 0;
    Label_0023:
        while (j < 188) {
            int k = 0;
            while (true) {
                while (k < 5) {
                    if (d[k * 188 + j] != 71) {
                        final boolean b = false;
                        if (b) {
                            i.b(j);
                            return true;
                        }
                        ++j;
                        continue Label_0023;
                    }
                    else {
                        ++k;
                    }
                }
                final boolean b = true;
                continue;
            }
        }
        return false;
    }
    
    @Override
    public void c() {
    }
    
    private class a implements x
    {
        final ac a;
        private final com.applovin.exoplayer2.l.x b;
        
        public a(final ac a) {
            this.a = a;
            this.b = new com.applovin.exoplayer2.l.x(new byte[4]);
        }
        
        @Override
        public void a(final ag ag, final j j, final ad.d d) {
        }
        
        @Override
        public void a(final y y) {
            if (y.h() != 0) {
                return;
            }
            if ((y.h() & 0x80) == 0x0) {
                return;
            }
            y.e(6);
            for (int n = y.a() / 4, i = 0; i < n; ++i) {
                y.a(this.b, 4);
                final int c = this.b.c(16);
                this.b.b(3);
                if (c == 0) {
                    this.b.b(13);
                }
                else {
                    final int c2 = this.b.c(13);
                    if (this.a.h.get(c2) == null) {
                        this.a.h.put(c2, (Object)new com.applovin.exoplayer2.e.i.y(this.a.new b(c2)));
                        this.a.n++;
                    }
                }
            }
            if (this.a.b != 2) {
                this.a.h.remove(0);
            }
        }
    }
    
    private class b implements x
    {
        final ac a;
        private final com.applovin.exoplayer2.l.x b;
        private final SparseArray<ad> c;
        private final SparseIntArray d;
        private final int e;
        
        public b(final ac a, final int e) {
            this.a = a;
            this.b = new com.applovin.exoplayer2.l.x(new byte[5]);
            this.c = (SparseArray<ad>)new SparseArray();
            this.d = new SparseIntArray();
            this.e = e;
        }
        
        private ad.b a(final y y, int h) {
            final int c = y.c();
            final int to = h + c;
            h = -1;
            String s = null;
            List<ad.a> list = null;
            while (y.c() < to) {
                final int h2 = y.h();
                final int n = y.c() + y.h();
                if (n > to) {
                    break;
                }
                String trim = null;
                List<ad.a> list2 = null;
                Label_0379: {
                    Label_0178: {
                        Label_0156: {
                            if (h2 == 5) {
                                final long o = y.o();
                                if (o == 1094921523L) {
                                    break Label_0156;
                                }
                                if (o == 1161904947L) {
                                    break Label_0178;
                                }
                                if (o != 1094921524L) {
                                    trim = s;
                                    list2 = list;
                                    if (o == 1212503619L) {
                                        h = 36;
                                        trim = s;
                                        list2 = list;
                                    }
                                    break Label_0379;
                                }
                            }
                            else {
                                if (h2 == 106) {
                                    break Label_0156;
                                }
                                if (h2 == 122) {
                                    break Label_0178;
                                }
                                if (h2 == 127) {
                                    trim = s;
                                    list2 = list;
                                    if (y.h() != 21) {
                                        break Label_0379;
                                    }
                                }
                                else {
                                    if (h2 == 123) {
                                        h = 138;
                                        trim = s;
                                        list2 = list;
                                        break Label_0379;
                                    }
                                    if (h2 == 10) {
                                        trim = y.f(3).trim();
                                        list2 = list;
                                        break Label_0379;
                                    }
                                    if (h2 == 89) {
                                        list2 = new ArrayList<ad.a>();
                                        while (y.c() < n) {
                                            final String trim2 = y.f(3).trim();
                                            h = y.h();
                                            final byte[] array = new byte[4];
                                            y.a(array, 0, 4);
                                            list2.add(new ad.a(trim2, h, array));
                                        }
                                        h = 89;
                                        trim = s;
                                        break Label_0379;
                                    }
                                    trim = s;
                                    list2 = list;
                                    if (h2 == 111) {
                                        h = 257;
                                        list2 = list;
                                        trim = s;
                                    }
                                    break Label_0379;
                                }
                            }
                            h = 172;
                            trim = s;
                            list2 = list;
                            break Label_0379;
                        }
                        h = 129;
                        trim = s;
                        list2 = list;
                        break Label_0379;
                    }
                    h = 135;
                    trim = s;
                    list2 = list;
                }
                y.e(n - y.c());
                s = trim;
                list = list2;
            }
            y.d(to);
            return new ad.b(h, s, list, Arrays.copyOfRange(y.d(), c, to));
        }
        
        @Override
        public void a(final ag ag, final j j, final ad.d d) {
        }
        
        @Override
        public void a(final y y) {
            if (y.h() != 2) {
                return;
            }
            ag ag;
            if (this.a.b != 1 && this.a.b != 2 && this.a.n != 1) {
                ag = new ag(this.a.d.get(0).a());
                this.a.d.add(ag);
            }
            else {
                ag = this.a.d.get(0);
            }
            if ((y.h() & 0x80) == 0x0) {
                return;
            }
            y.e(1);
            final int i = y.i();
            y.e(3);
            y.a(this.b, 2);
            this.b.b(3);
            this.a.t = this.b.c(13);
            y.a(this.b, 2);
            this.b.b(4);
            y.e(this.b.c(12));
            if (this.a.b == 2 && this.a.r == null) {
                final ad.b b = new ad.b(21, null, null, com.applovin.exoplayer2.l.ai.f);
                final ac a = this.a;
                a.r = a.g.a(21, b);
                if (this.a.r != null) {
                    this.a.r.a(ag, this.a.m, new ad.d(i, 21, 8192));
                }
            }
            this.c.clear();
            this.d.clear();
            int n;
            for (int j = y.a(); j > 0; j = n) {
                y.a(this.b, 5);
                final int c = this.b.c(8);
                this.b.b(3);
                final int c2 = this.b.c(13);
                this.b.b(4);
                final int c3 = this.b.c(12);
                final ad.b a2 = this.a(y, c3);
                int a3;
                if (c == 6 || (a3 = c) == 5) {
                    a3 = a2.a;
                }
                n = j - (c3 + 5);
                int n2;
                if (this.a.b == 2) {
                    n2 = a3;
                }
                else {
                    n2 = c2;
                }
                if (!this.a.i.get(n2)) {
                    ad ad;
                    if (this.a.b == 2 && a3 == 21) {
                        ad = this.a.r;
                    }
                    else {
                        ad = this.a.g.a(a3, a2);
                    }
                    if (this.a.b != 2 || c2 < this.d.get(n2, 8192)) {
                        this.d.put(n2, c2);
                        this.c.put(n2, (Object)ad);
                    }
                }
            }
            for (int size = this.d.size(), k = 0; k < size; ++k) {
                final int key = this.d.keyAt(k);
                final int value = this.d.valueAt(k);
                this.a.i.put(key, true);
                this.a.j.put(value, true);
                final ad ad2 = (ad)this.c.valueAt(k);
                if (ad2 != null) {
                    if (ad2 != this.a.r) {
                        ad2.a(ag, this.a.m, new ad.d(i, key, 8192));
                    }
                    this.a.h.put(value, (Object)ad2);
                }
            }
            if (this.a.b == 2) {
                if (!this.a.o) {
                    this.a.m.a();
                    this.a.n = 0;
                    this.a.o = true;
                }
            }
            else {
                this.a.h.remove(this.e);
                final ac a4 = this.a;
                int n3;
                if (a4.b == 1) {
                    n3 = 0;
                }
                else {
                    n3 = this.a.n - 1;
                }
                a4.n = n3;
                if (this.a.n == 0) {
                    this.a.m.a();
                    this.a.o = true;
                }
            }
        }
    }
}

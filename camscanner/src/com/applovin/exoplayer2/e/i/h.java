// 
// Decompiled by Procyon v0.6.0
// 

package com.applovin.exoplayer2.e.i;

import com.applovin.exoplayer2.l.a;
import com.applovin.exoplayer2.d.e;
import com.applovin.exoplayer2.b.o;
import com.applovin.exoplayer2.v;
import com.applovin.exoplayer2.e.x;
import androidx.annotation.Nullable;
import com.applovin.exoplayer2.l.y;

public final class h implements j
{
    private final y a;
    @Nullable
    private final String b;
    private String c;
    private x d;
    private int e;
    private int f;
    private int g;
    private long h;
    private v i;
    private int j;
    private long k;
    
    public h(@Nullable final String b) {
        this.a = new y(new byte[18]);
        this.e = 0;
        this.k = -9223372036854775807L;
        this.b = b;
    }
    
    private boolean a(final y y, final byte[] array, final int n) {
        final int min = Math.min(y.a(), n - this.f);
        y.a(array, this.f, min);
        final int f = this.f + min;
        this.f = f;
        return f == n;
    }
    
    private boolean b(final y y) {
        while (y.a() > 0) {
            final int g = this.g << 8;
            this.g = g;
            final int g2 = g | y.h();
            this.g = g2;
            if (o.a(g2)) {
                final byte[] d = this.a.d();
                final int g3 = this.g;
                d[0] = (byte)(g3 >> 24 & 0xFF);
                d[1] = (byte)(g3 >> 16 & 0xFF);
                d[2] = (byte)(g3 >> 8 & 0xFF);
                d[3] = (byte)(g3 & 0xFF);
                this.f = 4;
                this.g = 0;
                return true;
            }
        }
        return false;
    }
    
    private void c() {
        final byte[] d = this.a.d();
        if (this.i == null) {
            final v a = o.a(d, this.c, this.b, null);
            this.i = a;
            this.d.a(a);
        }
        this.j = o.b(d);
        this.h = (int)(o.a(d) * 1000000L / this.i.z);
    }
    
    @Override
    public void a() {
        this.e = 0;
        this.f = 0;
        this.g = 0;
        this.k = -9223372036854775807L;
    }
    
    @Override
    public void a(final long k, final int n) {
        if (k != -9223372036854775807L) {
            this.k = k;
        }
    }
    
    @Override
    public void a(final com.applovin.exoplayer2.e.j j, final ad.d d) {
        d.a();
        this.c = d.c();
        this.d = j.a(d.b(), 1);
    }
    
    @Override
    public void a(final y y) {
        com.applovin.exoplayer2.l.a.a((Object)this.d);
        while (y.a() > 0) {
            final int e = this.e;
            if (e != 0) {
                if (e != 1) {
                    if (e != 2) {
                        throw new IllegalStateException();
                    }
                    final int min = Math.min(y.a(), this.j - this.f);
                    this.d.a(y, min);
                    final int f = this.f + min;
                    this.f = f;
                    final int j = this.j;
                    if (f != j) {
                        continue;
                    }
                    final long k = this.k;
                    if (k != -9223372036854775807L) {
                        this.d.a(k, 1, j, 0, null);
                        this.k += this.h;
                    }
                    this.e = 0;
                }
                else {
                    if (!this.a(y, this.a.d(), 18)) {
                        continue;
                    }
                    this.c();
                    this.a.d(0);
                    this.d.a(this.a, 18);
                    this.e = 2;
                }
            }
            else {
                if (!this.b(y)) {
                    continue;
                }
                this.e = 1;
            }
        }
    }
    
    @Override
    public void b() {
    }
}

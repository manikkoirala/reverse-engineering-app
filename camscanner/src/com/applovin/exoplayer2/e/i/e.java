// 
// Decompiled by Procyon v0.6.0
// 

package com.applovin.exoplayer2.e.i;

import com.applovin.exoplayer2.l.a;
import com.applovin.exoplayer2.e.u;
import java.io.EOFException;
import com.applovin.exoplayer2.ai;
import com.applovin.exoplayer2.e.d;
import java.io.IOException;
import com.applovin.exoplayer2.e.i;
import com.applovin.exoplayer2.e.v;
import com.applovin.exoplayer2.e.j;
import com.applovin.exoplayer2.l.x;
import com.applovin.exoplayer2.l.y;
import com.applovin.exoplayer2.e.l;
import com.applovin.exoplayer2.e.h;

public final class e implements h
{
    public static final l a;
    private final int b;
    private final f c;
    private final y d;
    private final y e;
    private final x f;
    private j g;
    private long h;
    private long i;
    private int j;
    private boolean k;
    private boolean l;
    private boolean m;
    
    static {
        a = new O8();
    }
    
    public e() {
        this(0);
    }
    
    public e(final int n) {
        int b = n;
        if ((n & 0x2) != 0x0) {
            b = (n | 0x1);
        }
        this.b = b;
        this.c = new f(true);
        this.d = new y(2048);
        this.j = -1;
        this.i = -1L;
        final y e = new y(10);
        this.e = e;
        this.f = new x(e.d());
    }
    
    private static int a(final int n, final long n2) {
        return (int)(n * 8 * 1000000L / n2);
    }
    
    private void a(final long n, final boolean b) {
        if (this.m) {
            return;
        }
        final int b2 = this.b;
        final boolean b3 = false;
        final boolean b4 = (b2 & 0x1) != 0x0 && this.j > 0;
        if (b4 && this.c.c() == -9223372036854775807L && !b) {
            return;
        }
        if (b4 && this.c.c() != -9223372036854775807L) {
            final j g = this.g;
            boolean b5 = b3;
            if ((this.b & 0x2) != 0x0) {
                b5 = true;
            }
            g.a(this.b(n, b5));
        }
        else {
            this.g.a(new v.b(-9223372036854775807L));
        }
        this.m = true;
    }
    
    private int b(final i i) throws IOException {
        int n = 0;
        while (true) {
            i.d(this.e.d(), 0, 10);
            this.e.d(0);
            if (this.e.m() != 4801587) {
                break;
            }
            this.e.e(3);
            final int v = this.e.v();
            n += v + 10;
            i.c(v);
        }
        i.a();
        i.c(n);
        if (this.i == -1L) {
            this.i = n;
        }
        return n;
    }
    
    private v b(final long n, final boolean b) {
        return new d(n, this.i, a(this.j, this.c.c()), this.j, b);
    }
    
    private void c(final i i) throws IOException {
        if (this.k) {
            return;
        }
        this.j = -1;
        i.a();
        final long c = i.c();
        long n = 0L;
        if (c == 0L) {
            this.b(i);
        }
        final int n2 = 0;
        int n3 = 0;
        int n6;
        while (true) {
            int n4 = n3;
            long n5 = n;
            n6 = n3;
            long n7 = n;
            try {
                if (i.b(this.e.d(), 0, 2, true)) {
                    n4 = n3;
                    n5 = n;
                    this.e.d(0);
                    n4 = n3;
                    n5 = n;
                    if (!com.applovin.exoplayer2.e.i.f.a(this.e.i())) {
                        n6 = n2;
                        break;
                    }
                    n4 = n3;
                    n5 = n;
                    if (!i.b(this.e.d(), 0, 4, true)) {
                        n6 = n3;
                        n7 = n;
                    }
                    else {
                        n4 = n3;
                        n5 = n;
                        this.f.a(14);
                        n4 = n3;
                        n5 = n;
                        final int c2 = this.f.c(13);
                        if (c2 <= 6) {
                            n4 = n3;
                            n5 = n;
                            this.k = true;
                            n4 = n3;
                            n5 = n;
                            throw ai.b("Malformed ADTS stream", null);
                        }
                        n7 = n + c2;
                        n6 = n3 + 1;
                        if (n6 != 1000) {
                            n3 = n6;
                            n = n7;
                            n4 = n6;
                            n5 = n7;
                            if (i.b(c2 - 6, true)) {
                                continue;
                            }
                        }
                    }
                }
            }
            catch (final EOFException ex) {
                n7 = n5;
                n6 = n4;
            }
            n = n7;
            break;
        }
        i.a();
        if (n6 > 0) {
            this.j = (int)(n / n6);
        }
        else {
            this.j = -1;
        }
        this.k = true;
    }
    
    @Override
    public int a(final i i, final u u) throws IOException {
        com.applovin.exoplayer2.l.a.a((Object)this.g);
        final long d = i.d();
        final int b = this.b;
        if ((b & 0x2) != 0x0 || ((b & 0x1) != 0x0 && d != -1L)) {
            this.c(i);
        }
        final int a = i.a(this.d.d(), 0, 2048);
        final boolean b2 = a == -1;
        this.a(d, b2);
        if (b2) {
            return -1;
        }
        this.d.d(0);
        this.d.c(a);
        if (!this.l) {
            this.c.a(this.h, 4);
            this.l = true;
        }
        this.c.a(this.d);
        return 0;
    }
    
    @Override
    public void a(final long n, final long h) {
        this.l = false;
        this.c.a();
        this.h = h;
    }
    
    @Override
    public void a(final j g) {
        this.g = g;
        this.c.a(g, new ad.d(0, 1));
        g.a();
    }
    
    @Override
    public boolean a(final i i) throws IOException {
        int b;
        final int n = b = this.b(i);
        int n2 = 0;
        int n3 = 0;
        int n4 = 0;
        do {
            i.d(this.e.d(), 0, 2);
            this.e.d(0);
            int n6 = 0;
            Label_0184: {
                int n5;
                if (!com.applovin.exoplayer2.e.i.f.a(this.e.i())) {
                    n5 = b + 1;
                    i.a();
                    i.c(n5);
                }
                else {
                    if (++n2 >= 4 && n3 > 188) {
                        return true;
                    }
                    i.d(this.e.d(), 0, 4);
                    this.f.a(14);
                    final int c = this.f.c(13);
                    if (c > 6) {
                        i.c(c - 6);
                        n3 += c;
                        n4 = b;
                        n6 = n2;
                        break Label_0184;
                    }
                    n5 = b + 1;
                    i.a();
                    i.c(n5);
                }
                n6 = 0;
                final int n7 = 0;
                n4 = n5;
                n3 = n7;
            }
            n2 = n6;
            b = n4;
        } while (n4 - n < 8192);
        return false;
    }
    
    @Override
    public void c() {
    }
}

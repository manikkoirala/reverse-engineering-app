// 
// Decompiled by Procyon v0.6.0
// 

package com.applovin.exoplayer2.e.i;

import com.applovin.exoplayer2.l.a;
import com.applovin.exoplayer2.b.c;
import com.applovin.exoplayer2.v;
import androidx.annotation.Nullable;
import com.applovin.exoplayer2.l.y;
import com.applovin.exoplayer2.l.x;

public final class d implements j
{
    private final x a;
    private final y b;
    @Nullable
    private final String c;
    private String d;
    private com.applovin.exoplayer2.e.x e;
    private int f;
    private int g;
    private boolean h;
    private boolean i;
    private long j;
    private v k;
    private int l;
    private long m;
    
    public d() {
        this(null);
    }
    
    public d(@Nullable final String c) {
        final x a = new x(new byte[16]);
        this.a = a;
        this.b = new y(a.a);
        this.f = 0;
        this.g = 0;
        this.h = false;
        this.i = false;
        this.m = -9223372036854775807L;
        this.c = c;
    }
    
    private boolean a(final y y, final byte[] array, final int n) {
        final int min = Math.min(y.a(), n - this.g);
        y.a(array, this.g, min);
        final int g = this.g + min;
        this.g = g;
        return g == n;
    }
    
    private boolean b(final y y) {
        while (true) {
            final int a = y.a();
            final boolean b = false;
            boolean h = false;
            if (a <= 0) {
                return false;
            }
            if (!this.h) {
                if (y.h() == 172) {
                    h = true;
                }
                this.h = h;
            }
            else {
                final int h2 = y.h();
                this.h = (h2 == 172);
                if (h2 == 64 || h2 == 65) {
                    boolean i = b;
                    if (h2 == 65) {
                        i = true;
                    }
                    this.i = i;
                    return true;
                }
                continue;
            }
        }
    }
    
    private void c() {
        this.a.a(0);
        final c.a a = com.applovin.exoplayer2.b.c.a(this.a);
        final v k = this.k;
        if (k == null || a.c != k.y || a.b != k.z || !"audio/ac4".equals(k.l)) {
            final v a2 = new v.a().a(this.d).f("audio/ac4").k(a.c).l(a.b).c(this.c).a();
            this.k = a2;
            this.e.a(a2);
        }
        this.l = a.d;
        this.j = a.e * 1000000L / this.k.z;
    }
    
    @Override
    public void a() {
        this.f = 0;
        this.g = 0;
        this.h = false;
        this.i = false;
        this.m = -9223372036854775807L;
    }
    
    @Override
    public void a(final long m, final int n) {
        if (m != -9223372036854775807L) {
            this.m = m;
        }
    }
    
    @Override
    public void a(final com.applovin.exoplayer2.e.j j, final ad.d d) {
        d.a();
        this.d = d.c();
        this.e = j.a(d.b(), 1);
    }
    
    @Override
    public void a(final y y) {
        com.applovin.exoplayer2.l.a.a((Object)this.e);
        while (y.a() > 0) {
            final int f = this.f;
            if (f != 0) {
                if (f != 1) {
                    if (f != 2) {
                        continue;
                    }
                    final int min = Math.min(y.a(), this.l - this.g);
                    this.e.a(y, min);
                    final int g = this.g + min;
                    this.g = g;
                    final int l = this.l;
                    if (g != l) {
                        continue;
                    }
                    final long m = this.m;
                    if (m != -9223372036854775807L) {
                        this.e.a(m, 1, l, 0, null);
                        this.m += this.j;
                    }
                    this.f = 0;
                }
                else {
                    if (!this.a(y, this.b.d(), 16)) {
                        continue;
                    }
                    this.c();
                    this.b.d(0);
                    this.e.a(this.b, 16);
                    this.f = 2;
                }
            }
            else {
                if (!this.b(y)) {
                    continue;
                }
                this.f = 1;
                this.b.d()[0] = -84;
                final byte[] d = this.b.d();
                int n;
                if (this.i) {
                    n = 65;
                }
                else {
                    n = 64;
                }
                d[1] = (byte)n;
                this.g = 2;
            }
        }
    }
    
    @Override
    public void b() {
    }
}

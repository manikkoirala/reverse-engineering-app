// 
// Decompiled by Procyon v0.6.0
// 

package com.applovin.exoplayer2.e.i;

import com.applovin.exoplayer2.l.ai;
import com.applovin.exoplayer2.l.a;
import java.util.Collections;
import com.applovin.exoplayer2.l.q;
import java.util.Arrays;
import com.applovin.exoplayer2.v;
import com.applovin.exoplayer2.e.x;
import com.applovin.exoplayer2.l.y;
import androidx.annotation.Nullable;

public final class l implements j
{
    private static final float[] a;
    @Nullable
    private final af b;
    @Nullable
    private final y c;
    private final boolean[] d;
    private final a e;
    @Nullable
    private final r f;
    private b g;
    private long h;
    private String i;
    private x j;
    private boolean k;
    private long l;
    
    static {
        a = new float[] { 1.0f, 1.0f, 1.0909091f, 0.90909094f, 1.4545455f, 1.2121212f, 1.0f };
    }
    
    public l() {
        this(null);
    }
    
    l(@Nullable final af b) {
        this.b = b;
        this.d = new boolean[4];
        this.e = new a(128);
        this.l = -9223372036854775807L;
        if (b != null) {
            this.f = new r(178, 128);
            this.c = new y();
        }
        else {
            this.f = null;
            this.c = null;
        }
    }
    
    private static v a(final a a, int i, final String s) {
        final byte[] copy = Arrays.copyOf(a.c, a.a);
        final com.applovin.exoplayer2.l.x x = new com.applovin.exoplayer2.l.x(copy);
        x.e(i);
        x.e(4);
        x.d();
        x.b(8);
        if (x.e()) {
            x.b(4);
            x.b(3);
        }
        i = x.c(4);
        float n = 1.0f;
        if (i == 15) {
            final int c = x.c(8);
            i = x.c(8);
            if (i == 0) {
                q.c("H263Reader", "Invalid aspect ratio");
            }
            else {
                n = c / (float)i;
            }
        }
        else {
            final float[] a2 = l.a;
            if (i < a2.length) {
                n = a2[i];
            }
            else {
                q.c("H263Reader", "Invalid aspect ratio");
            }
        }
        if (x.e()) {
            x.b(2);
            x.b(1);
            if (x.e()) {
                x.b(15);
                x.d();
                x.b(15);
                x.d();
                x.b(15);
                x.d();
                x.b(3);
                x.b(11);
                x.d();
                x.b(15);
                x.d();
            }
        }
        if (x.c(2) != 0) {
            q.c("H263Reader", "Unhandled video object layer shape");
        }
        x.d();
        i = x.c(16);
        x.d();
        if (x.e()) {
            if (i == 0) {
                q.c("H263Reader", "Invalid vop_increment_time_resolution");
            }
            else {
                --i;
                int n2 = 0;
                while (i > 0) {
                    ++n2;
                    i >>= 1;
                }
                x.b(n2);
            }
        }
        x.d();
        i = x.c(13);
        x.d();
        final int c2 = x.c(13);
        x.d();
        x.d();
        return new v.a().a(s).f("video/mp4v-es").g(i).h(c2).b(n).a(Collections.singletonList(copy)).a();
    }
    
    @Override
    public void a() {
        com.applovin.exoplayer2.l.v.a(this.d);
        this.e.a();
        final b g = this.g;
        if (g != null) {
            g.a();
        }
        final r f = this.f;
        if (f != null) {
            f.a();
        }
        this.h = 0L;
        this.l = -9223372036854775807L;
    }
    
    @Override
    public void a(final long l, final int n) {
        if (l != -9223372036854775807L) {
            this.l = l;
        }
    }
    
    @Override
    public void a(final com.applovin.exoplayer2.e.j j, final ad.d d) {
        d.a();
        this.i = d.c();
        final x a = j.a(d.b(), 2);
        this.j = a;
        this.g = new b(a);
        final af b = this.b;
        if (b != null) {
            b.a(j, d);
        }
    }
    
    @Override
    public void a(final y y) {
        com.applovin.exoplayer2.l.a.a((Object)this.g);
        com.applovin.exoplayer2.l.a.a((Object)this.j);
        int c = y.c();
        final int b = y.b();
        final byte[] d = y.d();
        this.h += y.a();
        this.j.a(y, y.a());
        while (true) {
            final int a = com.applovin.exoplayer2.l.v.a(d, c, b, this.d);
            if (a == b) {
                break;
            }
            final byte[] d2 = y.d();
            final int n = a + 3;
            final int n2 = d2[n] & 0xFF;
            final int n3 = a - c;
            final boolean k = this.k;
            final int n4 = 0;
            if (!k) {
                if (n3 > 0) {
                    this.e.a(d, c, a);
                }
                int n5;
                if (n3 < 0) {
                    n5 = -n3;
                }
                else {
                    n5 = 0;
                }
                if (this.e.a(n2, n5)) {
                    final x j = this.j;
                    final a e = this.e;
                    j.a(a(e, e.b, (String)com.applovin.exoplayer2.l.a.b((Object)this.i)));
                    this.k = true;
                }
            }
            this.g.a(d, c, a);
            final r f = this.f;
            if (f != null) {
                int n6;
                if (n3 > 0) {
                    f.a(d, c, a);
                    n6 = n4;
                }
                else {
                    n6 = -n3;
                }
                if (this.f.b(n6)) {
                    final r f2 = this.f;
                    ((y)ai.a((Object)this.c)).a(this.f.a, com.applovin.exoplayer2.l.v.a(f2.a, f2.b));
                    ((af)ai.a((Object)this.b)).a(this.l, this.c);
                }
                if (n2 == 178 && y.d()[a + 2] == 1) {
                    this.f.a(n2);
                }
            }
            final int n7 = b - a;
            this.g.a(this.h - n7, n7, this.k);
            this.g.a(n2, this.l);
            c = n;
        }
        if (!this.k) {
            this.e.a(d, c, b);
        }
        this.g.a(d, c, b);
        final r f3 = this.f;
        if (f3 != null) {
            f3.a(d, c, b);
        }
    }
    
    @Override
    public void b() {
    }
    
    private static final class a
    {
        private static final byte[] d;
        public int a;
        public int b;
        public byte[] c;
        private boolean e;
        private int f;
        
        static {
            d = new byte[] { 0, 0, 1 };
        }
        
        public a(final int n) {
            this.c = new byte[n];
        }
        
        public void a() {
            this.e = false;
            this.a = 0;
            this.f = 0;
        }
        
        public void a(final byte[] array, final int n, int n2) {
            if (!this.e) {
                return;
            }
            n2 -= n;
            final byte[] c = this.c;
            final int length = c.length;
            final int a = this.a;
            if (length < a + n2) {
                this.c = Arrays.copyOf(c, (a + n2) * 2);
            }
            System.arraycopy(array, n, this.c, this.a, n2);
            this.a += n2;
        }
        
        public boolean a(final int n, final int n2) {
            final int f = this.f;
            if (f != 0) {
                if (f != 1) {
                    if (f != 2) {
                        if (f != 3) {
                            if (f != 4) {
                                throw new IllegalStateException();
                            }
                            if (n == 179 || n == 181) {
                                this.a -= n2;
                                this.e = false;
                                return true;
                            }
                        }
                        else if ((n & 0xF0) != 0x20) {
                            q.c("H263Reader", "Unexpected start code value");
                            this.a();
                        }
                        else {
                            this.b = this.a;
                            this.f = 4;
                        }
                    }
                    else if (n > 31) {
                        q.c("H263Reader", "Unexpected start code value");
                        this.a();
                    }
                    else {
                        this.f = 3;
                    }
                }
                else if (n != 181) {
                    q.c("H263Reader", "Unexpected start code value");
                    this.a();
                }
                else {
                    this.f = 2;
                }
            }
            else if (n == 176) {
                this.f = 1;
                this.e = true;
            }
            final byte[] d = com.applovin.exoplayer2.e.i.l.a.d;
            this.a(d, 0, d.length);
            return false;
        }
    }
    
    private static final class b
    {
        private final x a;
        private boolean b;
        private boolean c;
        private boolean d;
        private int e;
        private int f;
        private long g;
        private long h;
        
        public b(final x a) {
            this.a = a;
        }
        
        public void a() {
            this.b = false;
            this.c = false;
            this.d = false;
            this.e = -1;
        }
        
        public void a(final int e, final long h) {
            this.e = e;
            this.d = false;
            final boolean b = true;
            this.b = (e == 182 || e == 179);
            this.c = (e == 182 && b);
            this.f = 0;
            this.h = h;
        }
        
        public void a(final long g, final int n, final boolean b) {
            if (this.e == 182 && b && this.b) {
                final long h = this.h;
                if (h != -9223372036854775807L) {
                    this.a.a(h, this.d ? 1 : 0, (int)(g - this.g), n, null);
                }
            }
            if (this.e != 179) {
                this.g = g;
            }
        }
        
        public void a(final byte[] array, final int n, final int n2) {
            if (this.c) {
                final int f = this.f;
                final int n3 = n + 1 - f;
                if (n3 < n2) {
                    this.d = ((array[n3] & 0xC0) >> 6 == 0);
                    this.c = false;
                }
                else {
                    this.f = f + (n2 - n);
                }
            }
        }
    }
}

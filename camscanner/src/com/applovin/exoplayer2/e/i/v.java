// 
// Decompiled by Procyon v0.6.0
// 

package com.applovin.exoplayer2.e.i;

import com.applovin.exoplayer2.l.q;
import java.io.IOException;
import com.applovin.exoplayer2.e.u;
import com.applovin.exoplayer2.l.ai;
import com.applovin.exoplayer2.e.i;
import com.applovin.exoplayer2.l.y;
import com.applovin.exoplayer2.l.ag;

final class v
{
    private final ag a;
    private final y b;
    private boolean c;
    private boolean d;
    private boolean e;
    private long f;
    private long g;
    private long h;
    
    v() {
        this.a = new ag(0L);
        this.f = -9223372036854775807L;
        this.g = -9223372036854775807L;
        this.h = -9223372036854775807L;
        this.b = new y();
    }
    
    private int a(final i i) {
        this.b.a(ai.f);
        this.c = true;
        i.a();
        return 0;
    }
    
    private int a(final byte[] array, final int n) {
        return (array[n + 3] & 0xFF) | ((array[n] & 0xFF) << 24 | (array[n + 1] & 0xFF) << 16 | (array[n + 2] & 0xFF) << 8);
    }
    
    public static long a(final y y) {
        final int c = y.c();
        if (y.a() < 9) {
            return -9223372036854775807L;
        }
        final byte[] array = new byte[9];
        y.a(array, 0, 9);
        y.d(c);
        if (!a(array)) {
            return -9223372036854775807L;
        }
        return b(array);
    }
    
    private static boolean a(final byte[] array) {
        boolean b = false;
        if ((array[0] & 0xC4) != 0x44) {
            return false;
        }
        if ((array[2] & 0x4) != 0x4) {
            return false;
        }
        if ((array[4] & 0x4) != 0x4) {
            return false;
        }
        if ((array[5] & 0x1) != 0x1) {
            return false;
        }
        if ((array[8] & 0x3) == 0x3) {
            b = true;
        }
        return b;
    }
    
    private int b(final i i, final u u) throws IOException {
        final int n = (int)Math.min(20000L, i.d());
        final long c = i.c();
        final long a = 0;
        if (c != a) {
            u.a = a;
            return 1;
        }
        this.b.a(n);
        i.a();
        i.d(this.b.d(), 0, n);
        this.f = this.b(this.b);
        this.d = true;
        return 0;
    }
    
    private long b(final y y) {
        for (int i = y.c(); i < y.b() - 3; ++i) {
            if (this.a(y.d(), i) == 442) {
                y.d(i + 4);
                final long a = a(y);
                if (a != -9223372036854775807L) {
                    return a;
                }
            }
        }
        return -9223372036854775807L;
    }
    
    private static long b(final byte[] array) {
        final byte b = array[0];
        final long n = b;
        final long n2 = b;
        final long n3 = array[1];
        final byte b2 = array[2];
        return (n & 0x38L) >> 3 << 30 | (n2 & 0x3L) << 28 | (n3 & 0xFFL) << 20 | ((long)b2 & 0xF8L) >> 3 << 15 | ((long)b2 & 0x3L) << 13 | ((long)array[3] & 0xFFL) << 5 | ((long)array[4] & 0xF8L) >> 3;
    }
    
    private int c(final i i, final u u) throws IOException {
        final long d = i.d();
        final int n = (int)Math.min(20000L, d);
        final long a = d - n;
        if (i.c() != a) {
            u.a = a;
            return 1;
        }
        this.b.a(n);
        i.a();
        i.d(this.b.d(), 0, n);
        this.g = this.c(this.b);
        this.e = true;
        return 0;
    }
    
    private long c(final y y) {
        for (int c = y.c(), i = y.b() - 4; i >= c; --i) {
            if (this.a(y.d(), i) == 442) {
                y.d(i + 4);
                final long a = a(y);
                if (a != -9223372036854775807L) {
                    return a;
                }
            }
        }
        return -9223372036854775807L;
    }
    
    public int a(final i i, final u u) throws IOException {
        if (!this.e) {
            return this.c(i, u);
        }
        if (this.g == -9223372036854775807L) {
            return this.a(i);
        }
        if (!this.d) {
            return this.b(i, u);
        }
        final long f = this.f;
        if (f == -9223372036854775807L) {
            return this.a(i);
        }
        final long h = this.a.b(this.g) - this.a.b(f);
        this.h = h;
        if (h < 0L) {
            final StringBuilder sb = new StringBuilder();
            sb.append("Invalid duration: ");
            sb.append(this.h);
            sb.append(". Using TIME_UNSET instead.");
            q.c("PsDurationReader", sb.toString());
            this.h = -9223372036854775807L;
        }
        return this.a(i);
    }
    
    public boolean a() {
        return this.c;
    }
    
    public ag b() {
        return this.a;
    }
    
    public long c() {
        return this.h;
    }
}

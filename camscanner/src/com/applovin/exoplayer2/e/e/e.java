// 
// Decompiled by Procyon v0.6.0
// 

package com.applovin.exoplayer2.e.e;

import java.io.IOException;
import com.applovin.exoplayer2.e.i;
import com.applovin.exoplayer2.l.y;

final class e
{
    private final y a;
    private int b;
    
    public e() {
        this.a = new y(8);
    }
    
    private long b(final i i) throws IOException {
        final byte[] d = this.a.d();
        final int n = 0;
        i.d(d, 0, 1);
        final int n2 = this.a.d()[0] & 0xFF;
        if (n2 == 0) {
            return Long.MIN_VALUE;
        }
        int n3;
        int n4;
        for (n3 = 128, n4 = 0; (n2 & n3) == 0x0; n3 >>= 1, ++n4) {}
        final int n5 = n2 & ~n3;
        i.d(this.a.d(), 1, n4);
        int j;
        int n6;
        byte[] d2;
        for (j = n, n6 = n5; j < n4; ++j, n6 = (d2[j] & 0xFF) + (n6 << 8)) {
            d2 = this.a.d();
        }
        this.b += n4 + 1;
        return n6;
    }
    
    public boolean a(final i i) throws IOException {
        final long d = i.d();
        final long n = 1024L;
        final long n2 = lcmp(d, -1L);
        long n3 = n;
        if (n2 != 0) {
            if (d > 1024L) {
                n3 = n;
            }
            else {
                n3 = d;
            }
        }
        final int n4 = (int)n3;
        final byte[] d2 = this.a.d();
        final boolean b = false;
        i.d(d2, 0, 4);
        long o = this.a.o();
        this.b = 4;
        while (o != 440786851L) {
            if (++this.b == n4) {
                return false;
            }
            i.d(this.a.d(), 0, 1);
            o = ((o << 8 & 0xFFFFFFFFFFFFFF00L) | (long)(this.a.d()[0] & 0xFF));
        }
        final long b2 = this.b(i);
        final long n5 = this.b;
        boolean b3 = b;
        if (b2 != Long.MIN_VALUE) {
            if (n2 != 0 && n5 + b2 >= d) {
                b3 = b;
            }
            else {
                while (true) {
                    final int b4 = this.b;
                    final long n6 = b4;
                    final long n7 = n5 + b2;
                    if (n6 < n7) {
                        if (this.b(i) == Long.MIN_VALUE) {
                            return false;
                        }
                        final long b5 = this.b(i);
                        final long n8 = lcmp(b5, 0L);
                        if (n8 < 0 || b5 > 2147483647L) {
                            return false;
                        }
                        if (n8 == 0) {
                            continue;
                        }
                        final int n9 = (int)b5;
                        i.c(n9);
                        this.b += n9;
                    }
                    else {
                        b3 = b;
                        if (b4 == n7) {
                            b3 = true;
                            break;
                        }
                        break;
                    }
                }
            }
        }
        return b3;
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.applovin.exoplayer2.e.e;

import com.applovin.exoplayer2.ai;
import java.io.IOException;
import com.applovin.exoplayer2.e.i;
import java.util.ArrayDeque;

final class a implements c
{
    private final byte[] a;
    private final ArrayDeque<a> b;
    private final f c;
    private b d;
    private int e;
    private int f;
    private long g;
    
    public a() {
        this.a = new byte[8];
        this.b = new ArrayDeque<a>();
        this.c = new f();
    }
    
    private long a(final i i, final int n) throws IOException {
        final byte[] a = this.a;
        int j = 0;
        i.b(a, 0, n);
        long n2 = 0L;
        while (j < n) {
            n2 = (n2 << 8 | (long)(this.a[j] & 0xFF));
            ++j;
        }
        return n2;
    }
    
    private double b(final i i, final int n) throws IOException {
        final long a = this.a(i, n);
        double longBitsToDouble;
        if (n == 4) {
            longBitsToDouble = Float.intBitsToFloat((int)a);
        }
        else {
            longBitsToDouble = Double.longBitsToDouble(a);
        }
        return longBitsToDouble;
    }
    
    private long b(final i i) throws IOException {
        i.a();
        int a;
        int n;
        while (true) {
            i.d(this.a, 0, 4);
            a = com.applovin.exoplayer2.e.e.f.a(this.a[0]);
            if (a != -1 && a <= 4) {
                n = (int)com.applovin.exoplayer2.e.e.f.a(this.a, a, false);
                if (this.d.b(n)) {
                    break;
                }
            }
            i.b(1);
        }
        i.b(a);
        return n;
    }
    
    private static String c(final i i, int length) throws IOException {
        if (length == 0) {
            return "";
        }
        final byte[] bytes = new byte[length];
        i.b(bytes, 0, length);
        while (length > 0 && bytes[length - 1] == 0) {
            --length;
        }
        return new String(bytes, 0, length);
    }
    
    @Override
    public void a() {
        this.e = 0;
        this.b.clear();
        this.c.a();
    }
    
    @Override
    public void a(final b d) {
        this.d = d;
    }
    
    @Override
    public boolean a(final i i) throws IOException {
        com.applovin.exoplayer2.l.a.a((Object)this.d);
        while (true) {
            final a a = this.b.peek();
            if (a != null && i.c() >= a.b) {
                this.d.c(this.b.pop().a);
                return true;
            }
            if (this.e == 0) {
                long n;
                if ((n = this.c.a(i, true, false, 4)) == -2L) {
                    n = this.b(i);
                }
                if (n == -1L) {
                    return false;
                }
                this.f = (int)n;
                this.e = 1;
            }
            if (this.e == 1) {
                this.g = this.c.a(i, false, true, 8);
                this.e = 2;
            }
            final int a2 = this.d.a(this.f);
            if (a2 != 0) {
                if (a2 == 1) {
                    final long c = i.c();
                    this.b.push(new a(this.f, this.g + c));
                    this.d.a(this.f, c, this.g);
                    this.e = 0;
                    return true;
                }
                if (a2 != 2) {
                    if (a2 != 3) {
                        if (a2 == 4) {
                            this.d.a(this.f, (int)this.g, i);
                            this.e = 0;
                            return true;
                        }
                        if (a2 != 5) {
                            final StringBuilder sb = new StringBuilder();
                            sb.append("Invalid element type ");
                            sb.append(a2);
                            throw ai.b(sb.toString(), null);
                        }
                        final long g = this.g;
                        if (g != 4L && g != 8L) {
                            final StringBuilder sb2 = new StringBuilder();
                            sb2.append("Invalid float size: ");
                            sb2.append(this.g);
                            throw ai.b(sb2.toString(), null);
                        }
                        this.d.a(this.f, this.b(i, (int)g));
                        this.e = 0;
                        return true;
                    }
                    else {
                        final long g2 = this.g;
                        if (g2 <= 2147483647L) {
                            this.d.a(this.f, c(i, (int)g2));
                            this.e = 0;
                            return true;
                        }
                        final StringBuilder sb3 = new StringBuilder();
                        sb3.append("String element size: ");
                        sb3.append(this.g);
                        throw ai.b(sb3.toString(), null);
                    }
                }
                else {
                    final long g3 = this.g;
                    if (g3 <= 8L) {
                        this.d.a(this.f, this.a(i, (int)g3));
                        this.e = 0;
                        return true;
                    }
                    final StringBuilder sb4 = new StringBuilder();
                    sb4.append("Invalid integer size: ");
                    sb4.append(this.g);
                    throw ai.b(sb4.toString(), null);
                }
            }
            else {
                i.b((int)this.g);
                this.e = 0;
            }
        }
    }
    
    private static final class a
    {
        private final int a;
        private final long b;
        
        private a(final int a, final long b) {
            this.a = a;
            this.b = b;
        }
    }
}

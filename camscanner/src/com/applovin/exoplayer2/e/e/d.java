// 
// Decompiled by Procyon v0.6.0
// 

package com.applovin.exoplayer2.e.e;

import com.applovin.exoplayer2.common.a.s;
import java.nio.ByteOrder;
import java.util.ArrayList;
import java.util.List;
import android.util.Pair;
import androidx.annotation.CallSuper;
import java.util.Locale;
import com.applovin.exoplayer2.e.u;
import java.util.Arrays;
import com.applovin.exoplayer2.l.q;
import com.applovin.exoplayer2.k.g;
import java.io.IOException;
import com.applovin.exoplayer2.e.x;
import com.applovin.exoplayer2.e.i;
import com.applovin.exoplayer2.l.v;
import java.util.Collections;
import java.util.HashMap;
import com.applovin.exoplayer2.l.ai;
import java.nio.ByteBuffer;
import com.applovin.exoplayer2.l.y;
import android.util.SparseArray;
import com.applovin.exoplayer2.e.j;
import com.applovin.exoplayer2.l.r;
import androidx.annotation.Nullable;
import java.util.Map;
import java.util.UUID;
import com.applovin.exoplayer2.e.l;
import com.applovin.exoplayer2.e.h;

public class d implements h
{
    public static final l a;
    private static final byte[] b;
    private static final byte[] c;
    private static final byte[] d;
    private static final UUID e;
    private static final Map<String, Integer> f;
    @Nullable
    private b A;
    private boolean B;
    private int C;
    private long D;
    private boolean E;
    private long F;
    private long G;
    private long H;
    @Nullable
    private r I;
    @Nullable
    private r J;
    private boolean K;
    private boolean L;
    private int M;
    private long N;
    private long O;
    private int P;
    private int Q;
    private int[] R;
    private int S;
    private int T;
    private int U;
    private int V;
    private boolean W;
    private int X;
    private int Y;
    private int Z;
    private boolean aa;
    private boolean ab;
    private boolean ac;
    private int ad;
    private byte ae;
    private boolean af;
    private j ag;
    private final com.applovin.exoplayer2.e.e.c g;
    private final f h;
    private final SparseArray<b> i;
    private final boolean j;
    private final y k;
    private final y l;
    private final y m;
    private final y n;
    private final y o;
    private final y p;
    private final y q;
    private final y r;
    private final y s;
    private final y t;
    private ByteBuffer u;
    private long v;
    private long w;
    private long x;
    private long y;
    private long z;
    
    static {
        a = new \u3007080();
        b = new byte[] { 49, 10, 48, 48, 58, 48, 48, 58, 48, 48, 44, 48, 48, 48, 32, 45, 45, 62, 32, 48, 48, 58, 48, 48, 58, 48, 48, 44, 48, 48, 48, 10 };
        c = ai.c("Format: Start, End, ReadOrder, Layer, Style, Name, MarginL, MarginR, MarginV, Effect, Text");
        d = new byte[] { 68, 105, 97, 108, 111, 103, 117, 101, 58, 32, 48, 58, 48, 48, 58, 48, 48, 58, 48, 48, 44, 48, 58, 48, 48, 58, 48, 48, 58, 48, 48, 44 };
        e = new UUID(72057594037932032L, -9223371306706625679L);
        final HashMap m = new HashMap();
        m.put("htc_video_rotA-000", 0);
        m.put("htc_video_rotA-090", 90);
        m.put("htc_video_rotA-180", 180);
        m.put("htc_video_rotA-270", 270);
        f = Collections.unmodifiableMap((Map<?, ?>)m);
    }
    
    public d() {
        this(0);
    }
    
    public d(final int n) {
        this(new com.applovin.exoplayer2.e.e.a(), n);
    }
    
    d(final com.applovin.exoplayer2.e.e.c g, final int n) {
        this.w = -1L;
        this.x = -9223372036854775807L;
        this.y = -9223372036854775807L;
        this.z = -9223372036854775807L;
        this.F = -1L;
        this.G = -1L;
        this.H = -9223372036854775807L;
        (this.g = g).a(new a());
        this.j = ((n & 0x1) == 0x0);
        this.h = new f();
        this.i = (SparseArray<b>)new SparseArray();
        this.m = new y(4);
        this.n = new y(ByteBuffer.allocate(4).putInt(-1).array());
        this.o = new y(4);
        this.k = new y(com.applovin.exoplayer2.l.v.a);
        this.l = new y(4);
        this.p = new y();
        this.q = new y();
        this.r = new y(8);
        this.s = new y();
        this.t = new y();
        this.R = new int[1];
    }
    
    private int a(final i i, final b b, int n) throws IOException {
        if ("S_TEXT/UTF8".equals(b.b)) {
            this.a(i, com.applovin.exoplayer2.e.e.d.b, n);
            return this.e();
        }
        if ("S_TEXT/ASS".equals(b.b)) {
            this.a(i, com.applovin.exoplayer2.e.e.d.d, n);
            return this.e();
        }
        final x v = b.V;
        final boolean aa = this.aa;
        boolean b2 = true;
        if (!aa) {
            if (b.g) {
                this.U &= 0xBFFFFFFF;
                final boolean ab = this.ab;
                int n2 = 128;
                if (!ab) {
                    i.b(this.m.d(), 0, 1);
                    ++this.X;
                    if ((this.m.d()[0] & 0x80) == 0x80) {
                        throw com.applovin.exoplayer2.ai.b("Extension bit is set in signal byte", null);
                    }
                    this.ae = this.m.d()[0];
                    this.ab = true;
                }
                final byte ae = this.ae;
                if ((ae & 0x1) == 0x1) {
                    final boolean b3 = (ae & 0x2) == 0x2;
                    this.U |= 0x40000000;
                    if (!this.af) {
                        i.b(this.r.d(), 0, 8);
                        this.X += 8;
                        this.af = true;
                        final byte[] d = this.m.d();
                        if (!b3) {
                            n2 = 0;
                        }
                        d[0] = (byte)(n2 | 0x8);
                        this.m.d(0);
                        v.a(this.m, 1, 1);
                        ++this.Y;
                        this.r.d(0);
                        v.a(this.r, 8, 1);
                        this.Y += 8;
                    }
                    if (b3) {
                        if (!this.ac) {
                            i.b(this.m.d(), 0, 1);
                            ++this.X;
                            this.m.d(0);
                            this.ad = this.m.h();
                            this.ac = true;
                        }
                        final int n3 = this.ad * 4;
                        this.m.a(n3);
                        i.b(this.m.d(), 0, n3);
                        this.X += n3;
                        final short n4 = (short)(this.ad / 2 + 1);
                        final int capacity = n4 * 6 + 2;
                        final ByteBuffer u = this.u;
                        if (u == null || u.capacity() < capacity) {
                            this.u = ByteBuffer.allocate(capacity);
                        }
                        this.u.position(0);
                        this.u.putShort(n4);
                        int n5 = 0;
                        int n6 = 0;
                        int ad;
                        while (true) {
                            ad = this.ad;
                            if (n5 >= ad) {
                                break;
                            }
                            final int w = this.m.w();
                            if (n5 % 2 == 0) {
                                this.u.putShort((short)(w - n6));
                            }
                            else {
                                this.u.putInt(w - n6);
                            }
                            ++n5;
                            n6 = w;
                        }
                        final int n7 = n - this.X - n6;
                        if (ad % 2 == 1) {
                            this.u.putInt(n7);
                        }
                        else {
                            this.u.putShort((short)n7);
                            this.u.putInt(0);
                        }
                        this.s.a(this.u.array(), capacity);
                        v.a(this.s, capacity, 1);
                        this.Y += capacity;
                    }
                }
            }
            else {
                final byte[] h = b.h;
                if (h != null) {
                    this.p.a(h, h.length);
                }
            }
            if (b.f > 0) {
                this.U |= 0x10000000;
                this.t.a(0);
                this.m.a(4);
                this.m.d()[0] = (byte)(n >> 24 & 0xFF);
                this.m.d()[1] = (byte)(n >> 16 & 0xFF);
                this.m.d()[2] = (byte)(n >> 8 & 0xFF);
                this.m.d()[3] = (byte)(n & 0xFF);
                v.a(this.m, 4, 2);
                this.Y += 4;
            }
            this.aa = true;
        }
        n += this.p.b();
        if (!"V_MPEG4/ISO/AVC".equals(b.b) && !"V_MPEGH/ISO/HEVC".equals(b.b)) {
            if (b.S != null) {
                if (this.p.b() != 0) {
                    b2 = false;
                }
                com.applovin.exoplayer2.l.a.b(b2);
                b.S.a(i);
            }
            while (true) {
                final int x = this.X;
                if (x >= n) {
                    break;
                }
                final int a = this.a(i, v, n - x);
                this.X += a;
                this.Y += a;
            }
        }
        else {
            final byte[] d2 = this.l.d();
            d2[0] = 0;
            d2[2] = (d2[1] = 0);
            final int w2 = b.W;
            while (this.X < n) {
                final int z = this.Z;
                if (z == 0) {
                    this.a(i, d2, 4 - w2, w2);
                    this.X += w2;
                    this.l.d(0);
                    this.Z = this.l.w();
                    this.k.d(0);
                    v.a(this.k, 4);
                    this.Y += 4;
                }
                else {
                    final int a2 = this.a(i, v, z);
                    this.X += a2;
                    this.Y += a2;
                    this.Z -= a2;
                }
            }
        }
        if ("A_VORBIS".equals(b.b)) {
            this.n.d(0);
            v.a(this.n, 4);
            this.Y += 4;
        }
        return this.e();
    }
    
    private int a(final i i, final x x, int a) throws IOException {
        final int a2 = this.p.a();
        if (a2 > 0) {
            a = Math.min(a, a2);
            x.a(this.p, a);
        }
        else {
            a = x.a((g)i, a, false);
        }
        return a;
    }
    
    private long a(final long n) throws com.applovin.exoplayer2.ai {
        final long x = this.x;
        if (x != -9223372036854775807L) {
            return ai.d(n, x, 1000L);
        }
        throw com.applovin.exoplayer2.ai.b("Can't scale timecode prior to timecodeScale being set.", null);
    }
    
    private com.applovin.exoplayer2.e.v a(@Nullable final r r, @Nullable final r r2) {
        if (this.w != -1L && this.z != -9223372036854775807L && r != null && r.a() != 0 && r2 != null && r2.a() == r.a()) {
            final int a = r.a();
            final int[] original = new int[a];
            final long[] original2 = new long[a];
            final long[] original3 = new long[a];
            final long[] original4 = new long[a];
            final int n = 0;
            int n2 = 0;
            int n3;
            while (true) {
                n3 = n;
                if (n2 >= a) {
                    break;
                }
                original4[n2] = r.a(n2);
                original2[n2] = this.w + r2.a(n2);
                ++n2;
            }
            int n4;
            while (true) {
                n4 = a - 1;
                if (n3 >= n4) {
                    break;
                }
                final int n5 = n3 + 1;
                original[n3] = (int)(original2[n5] - original2[n3]);
                original3[n3] = original4[n5] - original4[n3];
                n3 = n5;
            }
            original[n4] = (int)(this.w + this.v - original2[n4]);
            final long lng = this.z - original4[n4];
            original3[n4] = lng;
            int[] copy = original;
            long[] copy2 = original2;
            long[] copy3 = original3;
            long[] copy4 = original4;
            if (lng <= 0L) {
                final StringBuilder sb = new StringBuilder();
                sb.append("Discarding last cue point with unexpected duration: ");
                sb.append(lng);
                com.applovin.exoplayer2.l.q.c("MatroskaExtractor", sb.toString());
                copy = Arrays.copyOf(original, n4);
                copy2 = Arrays.copyOf(original2, n4);
                copy3 = Arrays.copyOf(original3, n4);
                copy4 = Arrays.copyOf(original4, n4);
            }
            return new com.applovin.exoplayer2.e.c(copy, copy2, copy3, copy4);
        }
        return new com.applovin.exoplayer2.e.v.b(this.z);
    }
    
    private void a(final b b, final long n, final int n2, int b2, final int n3) {
        final c s = b.S;
        if (s != null) {
            s.a(b, n, n2, b2, n3);
        }
        else {
            int n4 = 0;
            Label_0218: {
                if (!"S_TEXT/UTF8".equals(b.b)) {
                    n4 = b2;
                    if (!"S_TEXT/ASS".equals(b.b)) {
                        break Label_0218;
                    }
                }
                if (this.Q > 1) {
                    com.applovin.exoplayer2.l.q.c("MatroskaExtractor", "Skipping subtitle sample in laced block.");
                    n4 = b2;
                }
                else {
                    final long o = this.O;
                    if (o == -9223372036854775807L) {
                        com.applovin.exoplayer2.l.q.c("MatroskaExtractor", "Skipping subtitle sample with no duration.");
                        n4 = b2;
                    }
                    else {
                        a(b.b, o, this.q.d());
                        for (int i = this.q.c(); i < this.q.b(); ++i) {
                            if (this.q.d()[i] == 0) {
                                this.q.c(i);
                                break;
                            }
                        }
                        final x v = b.V;
                        final y q = this.q;
                        v.a(q, q.b());
                        n4 = b2 + this.q.b();
                    }
                }
            }
            int n5 = n2;
            b2 = n4;
            if ((0x10000000 & n2) != 0x0) {
                if (this.Q > 1) {
                    n5 = (n2 & 0xEFFFFFFF);
                    b2 = n4;
                }
                else {
                    b2 = this.t.b();
                    b.V.a(this.t, b2, 2);
                    b2 += n4;
                    n5 = n2;
                }
            }
            b.V.a(n, n5, b2, n3, b.i);
        }
        this.L = true;
    }
    
    private void a(final i i, final int b) throws IOException {
        if (this.m.b() >= b) {
            return;
        }
        if (this.m.e() < b) {
            final y m = this.m;
            m.b(Math.max(m.e() * 2, b));
        }
        i.b(this.m.d(), this.m.b(), b - this.m.b());
        this.m.c(b);
    }
    
    private void a(final i i, final byte[] original, final int n) throws IOException {
        final int n2 = original.length + n;
        if (this.q.e() < n2) {
            this.q.a(Arrays.copyOf(original, n2 + n));
        }
        else {
            System.arraycopy(original, 0, this.q.d(), 0, original.length);
        }
        i.b(this.q.d(), original.length, n);
        this.q.d(0);
        this.q.c(n2);
    }
    
    private void a(final i i, final byte[] array, final int n, final int a) throws IOException {
        final int min = Math.min(a, this.p.a());
        i.b(array, n + min, a - min);
        if (min > 0) {
            this.p.a(array, n, min);
        }
    }
    
    private static void a(final String s, final long n, final byte[] array) {
        s.hashCode();
        byte[] array2;
        int n2;
        if (!s.equals("S_TEXT/ASS")) {
            if (!s.equals("S_TEXT/UTF8")) {
                throw new IllegalArgumentException();
            }
            array2 = a(n, "%02d:%02d:%02d,%03d", 1000L);
            n2 = 19;
        }
        else {
            array2 = a(n, "%01d:%02d:%02d:%02d", 10000L);
            n2 = 21;
        }
        System.arraycopy(array2, 0, array, n2, array2.length);
    }
    
    private boolean a(final u u, long g) {
        if (this.E) {
            this.G = g;
            u.a = this.F;
            this.E = false;
            return true;
        }
        if (this.B) {
            g = this.G;
            if (g != -1L) {
                u.a = g;
                this.G = -1L;
                return true;
            }
        }
        return false;
    }
    
    private static boolean a(final String s) {
        s.hashCode();
        final int hashCode = s.hashCode();
        int n = -1;
        switch (hashCode) {
            case 1951062397: {
                if (!s.equals("A_OPUS")) {
                    break;
                }
                n = 31;
                break;
            }
            case 1950789798: {
                if (!s.equals("A_FLAC")) {
                    break;
                }
                n = 30;
                break;
            }
            case 1950749482: {
                if (!s.equals("A_EAC3")) {
                    break;
                }
                n = 29;
                break;
            }
            case 1809237540: {
                if (!s.equals("V_MPEG2")) {
                    break;
                }
                n = 28;
                break;
            }
            case 1422270023: {
                if (!s.equals("S_TEXT/UTF8")) {
                    break;
                }
                n = 27;
                break;
            }
            case 855502857: {
                if (!s.equals("V_MPEGH/ISO/HEVC")) {
                    break;
                }
                n = 26;
                break;
            }
            case 738597099: {
                if (!s.equals("S_TEXT/ASS")) {
                    break;
                }
                n = 25;
                break;
            }
            case 725957860: {
                if (!s.equals("A_PCM/INT/LIT")) {
                    break;
                }
                n = 24;
                break;
            }
            case 725948237: {
                if (!s.equals("A_PCM/INT/BIG")) {
                    break;
                }
                n = 23;
                break;
            }
            case 635596514: {
                if (!s.equals("A_PCM/FLOAT/IEEE")) {
                    break;
                }
                n = 22;
                break;
            }
            case 542569478: {
                if (!s.equals("A_DTS/EXPRESS")) {
                    break;
                }
                n = 21;
                break;
            }
            case 444813526: {
                if (!s.equals("V_THEORA")) {
                    break;
                }
                n = 20;
                break;
            }
            case 99146302: {
                if (!s.equals("S_HDMV/PGS")) {
                    break;
                }
                n = 19;
                break;
            }
            case 82338134: {
                if (!s.equals("V_VP9")) {
                    break;
                }
                n = 18;
                break;
            }
            case 82338133: {
                if (!s.equals("V_VP8")) {
                    break;
                }
                n = 17;
                break;
            }
            case 82318131: {
                if (!s.equals("V_AV1")) {
                    break;
                }
                n = 16;
                break;
            }
            case 62927045: {
                if (!s.equals("A_DTS")) {
                    break;
                }
                n = 15;
                break;
            }
            case 62923603: {
                if (!s.equals("A_AC3")) {
                    break;
                }
                n = 14;
                break;
            }
            case 62923557: {
                if (!s.equals("A_AAC")) {
                    break;
                }
                n = 13;
                break;
            }
            case -356037306: {
                if (!s.equals("A_DTS/LOSSLESS")) {
                    break;
                }
                n = 12;
                break;
            }
            case -425012669: {
                if (!s.equals("S_VOBSUB")) {
                    break;
                }
                n = 11;
                break;
            }
            case -538363109: {
                if (!s.equals("V_MPEG4/ISO/AVC")) {
                    break;
                }
                n = 10;
                break;
            }
            case -538363189: {
                if (!s.equals("V_MPEG4/ISO/ASP")) {
                    break;
                }
                n = 9;
                break;
            }
            case -933872740: {
                if (!s.equals("S_DVBSUB")) {
                    break;
                }
                n = 8;
                break;
            }
            case -1373388978: {
                if (!s.equals("V_MS/VFW/FOURCC")) {
                    break;
                }
                n = 7;
                break;
            }
            case -1482641357: {
                if (!s.equals("A_MPEG/L3")) {
                    break;
                }
                n = 6;
                break;
            }
            case -1482641358: {
                if (!s.equals("A_MPEG/L2")) {
                    break;
                }
                n = 5;
                break;
            }
            case -1730367663: {
                if (!s.equals("A_VORBIS")) {
                    break;
                }
                n = 4;
                break;
            }
            case -1784763192: {
                if (!s.equals("A_TRUEHD")) {
                    break;
                }
                n = 3;
                break;
            }
            case -1985379776: {
                if (!s.equals("A_MS/ACM")) {
                    break;
                }
                n = 2;
                break;
            }
            case -2095575984: {
                if (!s.equals("V_MPEG4/ISO/SP")) {
                    break;
                }
                n = 1;
                break;
            }
            case -2095576542: {
                if (!s.equals("V_MPEG4/ISO/AP")) {
                    break;
                }
                n = 0;
                break;
            }
        }
        switch (n) {
            default: {
                return false;
            }
            case 0:
            case 1:
            case 2:
            case 3:
            case 4:
            case 5:
            case 6:
            case 7:
            case 8:
            case 9:
            case 10:
            case 11:
            case 12:
            case 13:
            case 14:
            case 15:
            case 16:
            case 17:
            case 18:
            case 19:
            case 20:
            case 21:
            case 22:
            case 23:
            case 24:
            case 25:
            case 26:
            case 27:
            case 28:
            case 29:
            case 30:
            case 31: {
                return true;
            }
        }
    }
    
    private static byte[] a(long n, final String format, final long n2) {
        com.applovin.exoplayer2.l.a.a(n != -9223372036854775807L);
        final int i = (int)(n / 3600000000L);
        n -= i * 3600 * 1000000L;
        final int j = (int)(n / 60000000L);
        n -= j * 60 * 1000000L;
        final int k = (int)(n / 1000000L);
        return ai.c(String.format(Locale.US, format, i, j, k, (int)((n - k * 1000000L) / n2)));
    }
    
    private static int[] a(@Nullable final int[] array, final int b) {
        if (array == null) {
            return new int[b];
        }
        if (array.length >= b) {
            return array;
        }
        return new int[Math.max(array.length * 2, b)];
    }
    
    private void d(final int i) throws com.applovin.exoplayer2.ai {
        if (this.A != null) {
            return;
        }
        final StringBuilder sb = new StringBuilder();
        sb.append("Element ");
        sb.append(i);
        sb.append(" must be in a TrackEntry");
        throw com.applovin.exoplayer2.ai.b(sb.toString(), null);
    }
    
    private int e() {
        final int y = this.Y;
        this.f();
        return y;
    }
    
    private void e(final int i) throws com.applovin.exoplayer2.ai {
        if (this.I != null && this.J != null) {
            return;
        }
        final StringBuilder sb = new StringBuilder();
        sb.append("Element ");
        sb.append(i);
        sb.append(" must be in a Cues");
        throw com.applovin.exoplayer2.ai.b(sb.toString(), null);
    }
    
    private b f(final int n) throws com.applovin.exoplayer2.ai {
        this.d(n);
        return this.A;
    }
    
    private void f() {
        this.X = 0;
        this.Y = 0;
        this.Z = 0;
        this.aa = false;
        this.ab = false;
        this.ac = false;
        this.ad = 0;
        this.ae = 0;
        this.af = false;
        this.p.a(0);
    }
    
    private void g() {
        com.applovin.exoplayer2.l.a.a((Object)this.ag);
    }
    
    @CallSuper
    protected int a(final int n) {
        switch (n) {
            default: {
                return 0;
            }
            case 181:
            case 17545:
            case 21969:
            case 21970:
            case 21971:
            case 21972:
            case 21973:
            case 21974:
            case 21975:
            case 21976:
            case 21977:
            case 21978:
            case 30323:
            case 30324:
            case 30325: {
                return 5;
            }
            case 161:
            case 163:
            case 165:
            case 16877:
            case 16981:
            case 18402:
            case 21419:
            case 25506:
            case 30322: {
                return 4;
            }
            case 160:
            case 166:
            case 174:
            case 183:
            case 187:
            case 224:
            case 225:
            case 16868:
            case 18407:
            case 19899:
            case 20532:
            case 20533:
            case 21936:
            case 21968:
            case 25152:
            case 28032:
            case 30113:
            case 30320:
            case 290298740:
            case 357149030:
            case 374648427:
            case 408125543:
            case 440786851:
            case 475249515:
            case 524531317: {
                return 1;
            }
            case 134:
            case 17026:
            case 21358:
            case 2274716: {
                return 3;
            }
            case 131:
            case 136:
            case 155:
            case 159:
            case 176:
            case 179:
            case 186:
            case 215:
            case 231:
            case 238:
            case 241:
            case 251:
            case 16871:
            case 16980:
            case 17029:
            case 17143:
            case 18401:
            case 18408:
            case 20529:
            case 20530:
            case 21420:
            case 21432:
            case 21680:
            case 21682:
            case 21690:
            case 21930:
            case 21945:
            case 21946:
            case 21947:
            case 21948:
            case 21949:
            case 21998:
            case 22186:
            case 22203:
            case 25188:
            case 30321:
            case 2352003:
            case 2807729: {
                return 2;
            }
        }
    }
    
    @Override
    public final int a(final i i, final u u) throws IOException {
        int j = 0;
        this.L = false;
        boolean a = true;
        while (a && !this.L) {
            final boolean b = a = this.g.a(i);
            if (b) {
                a = b;
                if (this.a(u, i.c())) {
                    return 1;
                }
                continue;
            }
        }
        if (!a) {
            while (j < this.i.size()) {
                final b b2 = (b)this.i.valueAt(j);
                b2.d();
                b2.a();
                ++j;
            }
            return -1;
        }
        return 0;
    }
    
    @CallSuper
    protected void a(final int n, final double n2) throws com.applovin.exoplayer2.ai {
        Label_0287: {
            if (n != 181) {
                if (n != 17545) {
                    switch (n) {
                        default: {
                            switch (n) {
                                default: {
                                    break Label_0287;
                                }
                                case 30325: {
                                    this.f(n).t = (float)n2;
                                    break Label_0287;
                                }
                                case 30324: {
                                    this.f(n).s = (float)n2;
                                    break Label_0287;
                                }
                                case 30323: {
                                    this.f(n).r = (float)n2;
                                    break Label_0287;
                                }
                            }
                            break;
                        }
                        case 21978: {
                            this.f(n).L = (float)n2;
                            break;
                        }
                        case 21977: {
                            this.f(n).K = (float)n2;
                            break;
                        }
                        case 21976: {
                            this.f(n).J = (float)n2;
                            break;
                        }
                        case 21975: {
                            this.f(n).I = (float)n2;
                            break;
                        }
                        case 21974: {
                            this.f(n).H = (float)n2;
                            break;
                        }
                        case 21973: {
                            this.f(n).G = (float)n2;
                            break;
                        }
                        case 21972: {
                            this.f(n).F = (float)n2;
                            break;
                        }
                        case 21971: {
                            this.f(n).E = (float)n2;
                            break;
                        }
                        case 21970: {
                            this.f(n).D = (float)n2;
                            break;
                        }
                        case 21969: {
                            this.f(n).C = (float)n2;
                            break;
                        }
                    }
                }
                else {
                    this.y = (long)n2;
                }
            }
            else {
                this.f(n).P = (int)n2;
            }
        }
    }
    
    @CallSuper
    protected void a(int i, int u, final i j) throws IOException {
        if (i != 161 && i != 163) {
            if (i != 165) {
                if (i != 16877) {
                    if (i != 16981) {
                        if (i != 18402) {
                            if (i != 21419) {
                                if (i != 25506) {
                                    if (i != 30322) {
                                        final StringBuilder sb = new StringBuilder();
                                        sb.append("Unexpected id: ");
                                        sb.append(i);
                                        throw com.applovin.exoplayer2.ai.b(sb.toString(), null);
                                    }
                                    this.d(i);
                                    j.b(this.A.u = new byte[u], 0, u);
                                }
                                else {
                                    this.d(i);
                                    j.b(this.A.j = new byte[u], 0, u);
                                }
                            }
                            else {
                                Arrays.fill(this.o.d(), (byte)0);
                                j.b(this.o.d(), 4 - u, u);
                                this.o.d(0);
                                this.C = (int)this.o.o();
                            }
                        }
                        else {
                            final byte[] array = new byte[u];
                            j.b(array, 0, u);
                            this.f(i).i = new x.a(1, array, 0, 0);
                        }
                    }
                    else {
                        this.d(i);
                        j.b(this.A.h = new byte[u], 0, u);
                    }
                }
                else {
                    this.a(this.f(i), j, u);
                }
            }
            else {
                if (this.M != 2) {
                    return;
                }
                this.a((b)this.i.get(this.S), this.V, j, u);
            }
        }
        else {
            if (this.M == 0) {
                this.S = (int)this.h.a(j, false, true, 8);
                this.T = this.h.b();
                this.O = -9223372036854775807L;
                this.M = 1;
                this.m.a(0);
            }
            final b b = (b)this.i.get(this.S);
            if (b == null) {
                j.b(u - this.T);
                this.M = 0;
                return;
            }
            b.d();
            if (this.M == 1) {
                this.a(j, 3);
                final int k = (this.m.d()[2] & 0x6) >> 1;
                if (k == 0) {
                    this.Q = 1;
                    (this.R = a(this.R, 1))[0] = u - this.T - 3;
                }
                else {
                    int n = 4;
                    this.a(j, 4);
                    final int q = (this.m.d()[3] & 0xFF) + 1;
                    this.Q = q;
                    final int[] a = a(this.R, q);
                    this.R = a;
                    if (k == 2) {
                        final int t = this.T;
                        final int q2 = this.Q;
                        Arrays.fill(a, 0, q2, (u - t - 4) / q2);
                    }
                    else if (k == 1) {
                        int n2 = 0;
                        int n3 = 0;
                        int q3;
                        while (true) {
                            q3 = this.Q;
                            if (n2 >= q3 - 1) {
                                break;
                            }
                            this.R[n2] = 0;
                            int n4 = n;
                            int l;
                            int n5;
                            do {
                                n = n4 + 1;
                                this.a(j, n);
                                l = (this.m.d()[n - 1] & 0xFF);
                                final int[] r = this.R;
                                n5 = r[n2] + l;
                                r[n2] = n5;
                                n4 = n;
                            } while (l == 255);
                            n3 += n5;
                            ++n2;
                        }
                        this.R[q3 - 1] = u - this.T - n - n3;
                    }
                    else {
                        if (k != 3) {
                            final StringBuilder sb2 = new StringBuilder();
                            sb2.append("Unexpected lacing value: ");
                            sb2.append(k);
                            throw com.applovin.exoplayer2.ai.b(sb2.toString(), null);
                        }
                        int n6 = 0;
                        int n7 = 0;
                    Label_0752:
                        while (true) {
                            final int q4 = this.Q;
                            if (n6 >= q4 - 1) {
                                this.R[q4 - 1] = u - this.T - n - n7;
                                break;
                            }
                            this.R[n6] = 0;
                            ++n;
                            this.a(j, n);
                            final byte[] d = this.m.d();
                            int n8 = n - 1;
                            if (d[n8] != 0) {
                                int n9 = 0;
                                while (true) {
                                    while (n9 < 8) {
                                        final int n10 = 1 << 7 - n9;
                                        if ((this.m.d()[n8] & n10) != 0x0) {
                                            n += n9;
                                            this.a(j, n);
                                            long n11 = ~n10 & (this.m.d()[n8] & 0xFF);
                                            ++n8;
                                            while (n8 < n) {
                                                n11 = (n11 << 8 | (long)(this.m.d()[n8] & 0xFF));
                                                ++n8;
                                            }
                                            long n12 = n11;
                                            if (n6 > 0) {
                                                n12 = n11 - ((1L << n9 * 7 + 6) - 1L);
                                            }
                                            if (n12 >= -2147483648L && n12 <= 2147483647L) {
                                                int n13 = (int)n12;
                                                final int[] r2 = this.R;
                                                if (n6 != 0) {
                                                    n13 += r2[n6 - 1];
                                                }
                                                r2[n6] = n13;
                                                n7 += n13;
                                                ++n6;
                                                continue Label_0752;
                                            }
                                            throw com.applovin.exoplayer2.ai.b("EBML lacing sample size out of range.", null);
                                        }
                                        else {
                                            ++n9;
                                        }
                                    }
                                    long n12 = 0L;
                                    continue;
                                }
                            }
                            throw com.applovin.exoplayer2.ai.b("No valid varint length mask found", null);
                        }
                    }
                }
                final byte b2 = this.m.d()[0];
                u = this.m.d()[1];
                this.N = this.H + this.a((long)(b2 << 8 | (u & 0xFF)));
                if (b.d != 2 && (i != 163 || (this.m.d()[2] & 0x80) != 0x80)) {
                    u = 0;
                }
                else {
                    u = 1;
                }
                this.U = u;
                this.M = 2;
                this.P = 0;
            }
            if (i == 163) {
                while (true) {
                    i = this.P;
                    if (i >= this.Q) {
                        break;
                    }
                    i = this.a(j, b, this.R[i]);
                    this.a(b, this.P * b.e / 1000 + this.N, this.U, i, 0);
                    ++this.P;
                }
                this.M = 0;
            }
            else {
                while (true) {
                    i = this.P;
                    if (i >= this.Q) {
                        break;
                    }
                    final int[] r3 = this.R;
                    r3[i] = this.a(j, b, r3[i]);
                    ++this.P;
                }
            }
        }
    }
    
    @CallSuper
    protected void a(int n, final long n2) throws com.applovin.exoplayer2.ai {
        Label_1287: {
            if (n != 20529) {
                if (n != 20530) {
                    boolean u = false;
                    final boolean b = false;
                    switch (n) {
                        default: {
                            switch (n) {
                                default: {
                                    break Label_1287;
                                }
                                case 21949: {
                                    this.f(n).B = (int)n2;
                                    break Label_1287;
                                }
                                case 21948: {
                                    this.f(n).A = (int)n2;
                                    break Label_1287;
                                }
                                case 21947: {
                                    this.d(n);
                                    this.A.w = true;
                                    n = com.applovin.exoplayer2.m.b.a((int)n2);
                                    if (n != -1) {
                                        this.A.x = n;
                                        break Label_1287;
                                    }
                                    break Label_1287;
                                }
                                case 21946: {
                                    this.d(n);
                                    n = com.applovin.exoplayer2.m.b.b((int)n2);
                                    if (n != -1) {
                                        this.A.y = n;
                                        break Label_1287;
                                    }
                                    break Label_1287;
                                }
                                case 21945: {
                                    this.d(n);
                                    n = (int)n2;
                                    if (n == 1) {
                                        this.A.z = 2;
                                        break Label_1287;
                                    }
                                    if (n != 2) {
                                        break Label_1287;
                                    }
                                    this.A.z = 1;
                                    break Label_1287;
                                }
                            }
                            break;
                        }
                        case 2807729: {
                            this.x = n2;
                            break;
                        }
                        case 2352003: {
                            this.f(n).e = (int)n2;
                            break;
                        }
                        case 30321: {
                            this.d(n);
                            n = (int)n2;
                            if (n == 0) {
                                this.A.q = 0;
                                break;
                            }
                            if (n == 1) {
                                this.A.q = 1;
                                break;
                            }
                            if (n == 2) {
                                this.A.q = 2;
                                break;
                            }
                            if (n != 3) {
                                break;
                            }
                            this.A.q = 3;
                            break;
                        }
                        case 25188: {
                            this.f(n).O = (int)n2;
                            break;
                        }
                        case 22203: {
                            this.f(n).R = n2;
                            break;
                        }
                        case 22186: {
                            this.f(n).Q = n2;
                            break;
                        }
                        case 21998: {
                            this.f(n).f = (int)n2;
                            break;
                        }
                        case 21930: {
                            final b f = this.f(n);
                            boolean t = b;
                            if (n2 == 1L) {
                                t = true;
                            }
                            f.T = t;
                            break;
                        }
                        case 21690: {
                            this.f(n).o = (int)n2;
                            break;
                        }
                        case 21682: {
                            this.f(n).p = (int)n2;
                            break;
                        }
                        case 21680: {
                            this.f(n).n = (int)n2;
                            break;
                        }
                        case 21432: {
                            final int n3 = (int)n2;
                            this.d(n);
                            if (n3 == 0) {
                                this.A.v = 0;
                                break;
                            }
                            if (n3 == 1) {
                                this.A.v = 2;
                                break;
                            }
                            if (n3 == 3) {
                                this.A.v = 1;
                                break;
                            }
                            if (n3 != 15) {
                                break;
                            }
                            this.A.v = 3;
                            break;
                        }
                        case 21420: {
                            this.D = n2 + this.w;
                            break;
                        }
                        case 18408: {
                            if (n2 == 1L) {
                                break;
                            }
                            final StringBuilder sb = new StringBuilder();
                            sb.append("AESSettingsCipherMode ");
                            sb.append(n2);
                            sb.append(" not supported");
                            throw com.applovin.exoplayer2.ai.b(sb.toString(), null);
                        }
                        case 18401: {
                            if (n2 == 5L) {
                                break;
                            }
                            final StringBuilder sb2 = new StringBuilder();
                            sb2.append("ContentEncAlgo ");
                            sb2.append(n2);
                            sb2.append(" not supported");
                            throw com.applovin.exoplayer2.ai.b(sb2.toString(), null);
                        }
                        case 17143: {
                            if (n2 == 1L) {
                                break;
                            }
                            final StringBuilder sb3 = new StringBuilder();
                            sb3.append("EBMLReadVersion ");
                            sb3.append(n2);
                            sb3.append(" not supported");
                            throw com.applovin.exoplayer2.ai.b(sb3.toString(), null);
                        }
                        case 17029: {
                            if (n2 >= 1L && n2 <= 2L) {
                                break;
                            }
                            final StringBuilder sb4 = new StringBuilder();
                            sb4.append("DocTypeReadVersion ");
                            sb4.append(n2);
                            sb4.append(" not supported");
                            throw com.applovin.exoplayer2.ai.b(sb4.toString(), null);
                        }
                        case 16980: {
                            if (n2 == 3L) {
                                break;
                            }
                            final StringBuilder sb5 = new StringBuilder();
                            sb5.append("ContentCompAlgo ");
                            sb5.append(n2);
                            sb5.append(" not supported");
                            throw com.applovin.exoplayer2.ai.b(sb5.toString(), null);
                        }
                        case 16871: {
                            this.f(n).X = (int)n2;
                            break;
                        }
                        case 251: {
                            this.W = true;
                            break;
                        }
                        case 241: {
                            if (!this.K) {
                                this.e(n);
                                this.J.a(n2);
                                this.K = true;
                                break;
                            }
                            break;
                        }
                        case 238: {
                            this.V = (int)n2;
                            break;
                        }
                        case 231: {
                            this.H = this.a(n2);
                            break;
                        }
                        case 215: {
                            this.f(n).c = (int)n2;
                            break;
                        }
                        case 186: {
                            this.f(n).m = (int)n2;
                            break;
                        }
                        case 179: {
                            this.e(n);
                            this.I.a(this.a(n2));
                            break;
                        }
                        case 176: {
                            this.f(n).l = (int)n2;
                            break;
                        }
                        case 159: {
                            this.f(n).N = (int)n2;
                            break;
                        }
                        case 155: {
                            this.O = this.a(n2);
                            break;
                        }
                        case 136: {
                            final b f2 = this.f(n);
                            if (n2 == 1L) {
                                u = true;
                            }
                            f2.U = u;
                            break;
                        }
                        case 131: {
                            this.f(n).d = (int)n2;
                            break;
                        }
                    }
                }
                else if (n2 != 1L) {
                    final StringBuilder sb6 = new StringBuilder();
                    sb6.append("ContentEncodingScope ");
                    sb6.append(n2);
                    sb6.append(" not supported");
                    throw com.applovin.exoplayer2.ai.b(sb6.toString(), null);
                }
            }
            else if (n2 != 0L) {
                final StringBuilder sb7 = new StringBuilder();
                sb7.append("ContentEncodingOrder ");
                sb7.append(n2);
                sb7.append(" not supported");
                throw com.applovin.exoplayer2.ai.b(sb7.toString(), null);
            }
        }
    }
    
    @CallSuper
    protected void a(final int n, final long w, final long v) throws com.applovin.exoplayer2.ai {
        this.g();
        if (n != 160) {
            if (n != 174) {
                if (n != 187) {
                    if (n != 19899) {
                        if (n != 20533) {
                            if (n != 21968) {
                                if (n != 408125543) {
                                    if (n != 475249515) {
                                        if (n == 524531317) {
                                            if (!this.B) {
                                                if (this.j && this.F != -1L) {
                                                    this.E = true;
                                                }
                                                else {
                                                    this.ag.a(new com.applovin.exoplayer2.e.v.b(this.z));
                                                    this.B = true;
                                                }
                                            }
                                        }
                                    }
                                    else {
                                        this.I = new r();
                                        this.J = new r();
                                    }
                                }
                                else {
                                    final long w2 = this.w;
                                    if (w2 != -1L && w2 != w) {
                                        throw com.applovin.exoplayer2.ai.b("Multiple Segment elements not supported", null);
                                    }
                                    this.w = w;
                                    this.v = v;
                                }
                            }
                            else {
                                this.f(n).w = true;
                            }
                        }
                        else {
                            this.f(n).g = true;
                        }
                    }
                    else {
                        this.C = -1;
                        this.D = -1L;
                    }
                }
                else {
                    this.K = false;
                }
            }
            else {
                this.A = new b();
            }
        }
        else {
            this.W = false;
        }
    }
    
    @CallSuper
    protected void a(final int n, final String b) throws com.applovin.exoplayer2.ai {
        if (n != 134) {
            if (n != 17026) {
                if (n != 21358) {
                    if (n == 2274716) {
                        this.f(n).Y = b;
                    }
                }
                else {
                    this.f(n).a = b;
                }
            }
            else if (!"webm".equals(b)) {
                if (!"matroska".equals(b)) {
                    final StringBuilder sb = new StringBuilder();
                    sb.append("DocType ");
                    sb.append(b);
                    sb.append(" not supported");
                    throw com.applovin.exoplayer2.ai.b(sb.toString(), null);
                }
            }
        }
        else {
            this.f(n).b = b;
        }
    }
    
    @CallSuper
    @Override
    public void a(final long n, final long n2) {
        this.H = -9223372036854775807L;
        int i = 0;
        this.M = 0;
        this.g.a();
        this.h.a();
        this.f();
        while (i < this.i.size()) {
            ((b)this.i.valueAt(i)).b();
            ++i;
        }
    }
    
    protected void a(final b b, final int n, final i i, final int n2) throws IOException {
        if (n == 4 && "V_VP9".equals(b.b)) {
            this.t.a(n2);
            i.b(this.t.d(), 0, n2);
        }
        else {
            i.b(n2);
        }
    }
    
    protected void a(final b b, final i i, final int n) throws IOException {
        if (b.X != 1685485123 && b.X != 1685480259) {
            i.b(n);
        }
        else {
            i.b(b.M = new byte[n], 0, n);
        }
    }
    
    @Override
    public final void a(final j ag) {
        this.ag = ag;
    }
    
    @Override
    public final boolean a(final i i) throws IOException {
        return new e().a(i);
    }
    
    @CallSuper
    protected boolean b(final int n) {
        return n == 357149030 || n == 524531317 || n == 475249515 || n == 374648427;
    }
    
    @Override
    public final void c() {
    }
    
    @CallSuper
    protected void c(int c) throws com.applovin.exoplayer2.ai {
        this.g();
        if (c != 160) {
            if (c != 174) {
                if (c == 19899) {
                    c = this.C;
                    if (c != -1) {
                        final long d = this.D;
                        if (d != -1L) {
                            if (c == 475249515) {
                                this.F = d;
                            }
                            return;
                        }
                    }
                    throw com.applovin.exoplayer2.ai.b("Mandatory element SeekID or SeekPosition not found", null);
                }
                if (c != 25152) {
                    if (c != 28032) {
                        if (c != 357149030) {
                            if (c != 374648427) {
                                if (c == 475249515) {
                                    if (!this.B) {
                                        this.ag.a(this.a(this.I, this.J));
                                        this.B = true;
                                    }
                                    this.I = null;
                                    this.J = null;
                                }
                            }
                            else {
                                if (this.i.size() == 0) {
                                    throw com.applovin.exoplayer2.ai.b("No valid tracks were found", null);
                                }
                                this.ag.a();
                            }
                        }
                        else {
                            if (this.x == -9223372036854775807L) {
                                this.x = 1000000L;
                            }
                            final long y = this.y;
                            if (y != -9223372036854775807L) {
                                this.z = this.a(y);
                            }
                        }
                    }
                    else {
                        this.d(c);
                        final b a = this.A;
                        if (a.g) {
                            if (a.h != null) {
                                throw com.applovin.exoplayer2.ai.b("Combining encryption and compression is not supported", null);
                            }
                        }
                    }
                }
                else {
                    this.d(c);
                    final b a2 = this.A;
                    if (a2.g) {
                        if (a2.i == null) {
                            throw com.applovin.exoplayer2.ai.b("Encrypted Track found but ContentEncKeyID was not found", null);
                        }
                        a2.k = new com.applovin.exoplayer2.d.e(new com.applovin.exoplayer2.d.e.a[] { new com.applovin.exoplayer2.d.e.a(com.applovin.exoplayer2.h.a, "video/webm", this.A.i.b) });
                    }
                }
            }
            else {
                final b b = (b)com.applovin.exoplayer2.l.a.a((Object)this.A);
                final String b2 = b.b;
                if (b2 == null) {
                    throw com.applovin.exoplayer2.ai.b("CodecId is missing in TrackEntry element", null);
                }
                if (a(b2)) {
                    b.a(this.ag, b.c);
                    this.i.put(b.c, (Object)b);
                }
                this.A = null;
            }
        }
        else {
            if (this.M != 2) {
                return;
            }
            int i = 0;
            c = 0;
            while (i < this.Q) {
                c += this.R[i];
                ++i;
            }
            final b b3 = (b)this.i.get(this.S);
            b3.d();
            for (int j = 0; j < this.Q; ++j) {
                final long n = this.N;
                final long n2 = b3.e * j / 1000;
                int u;
                final int n3 = u = this.U;
                if (j == 0) {
                    u = n3;
                    if (!this.W) {
                        u = (n3 | 0x1);
                    }
                }
                final int n4 = this.R[j];
                c -= n4;
                this.a(b3, n2 + n, u, n4, c);
            }
            this.M = 0;
        }
    }
    
    private final class a implements b
    {
        final d a;
        
        private a(final d a) {
            this.a = a;
        }
        
        @Override
        public int a(final int n) {
            return this.a.a(n);
        }
        
        @Override
        public void a(final int n, final double n2) throws com.applovin.exoplayer2.ai {
            this.a.a(n, n2);
        }
        
        @Override
        public void a(final int n, final int n2, final i i) throws IOException {
            this.a.a(n, n2, i);
        }
        
        @Override
        public void a(final int n, final long n2) throws com.applovin.exoplayer2.ai {
            this.a.a(n, n2);
        }
        
        @Override
        public void a(final int n, final long n2, final long n3) throws com.applovin.exoplayer2.ai {
            this.a.a(n, n2, n3);
        }
        
        @Override
        public void a(final int n, final String s) throws com.applovin.exoplayer2.ai {
            this.a.a(n, s);
        }
        
        @Override
        public boolean b(final int n) {
            return this.a.b(n);
        }
        
        @Override
        public void c(final int n) throws com.applovin.exoplayer2.ai {
            this.a.c(n);
        }
    }
    
    private static final class b
    {
        public int A;
        public int B;
        public float C;
        public float D;
        public float E;
        public float F;
        public float G;
        public float H;
        public float I;
        public float J;
        public float K;
        public float L;
        public byte[] M;
        public int N;
        public int O;
        public int P;
        public long Q;
        public long R;
        public c S;
        public boolean T;
        public boolean U;
        public x V;
        public int W;
        private int X;
        private String Y;
        public String a;
        public String b;
        public int c;
        public int d;
        public int e;
        public int f;
        public boolean g;
        public byte[] h;
        public x.a i;
        public byte[] j;
        public com.applovin.exoplayer2.d.e k;
        public int l;
        public int m;
        public int n;
        public int o;
        public int p;
        public int q;
        public float r;
        public float s;
        public float t;
        public byte[] u;
        public int v;
        public boolean w;
        public int x;
        public int y;
        public int z;
        
        private b() {
            this.l = -1;
            this.m = -1;
            this.n = -1;
            this.o = -1;
            this.p = 0;
            this.q = -1;
            this.r = 0.0f;
            this.s = 0.0f;
            this.t = 0.0f;
            this.u = null;
            this.v = -1;
            this.w = false;
            this.x = -1;
            this.y = -1;
            this.z = -1;
            this.A = 1000;
            this.B = 200;
            this.C = -1.0f;
            this.D = -1.0f;
            this.E = -1.0f;
            this.F = -1.0f;
            this.G = -1.0f;
            this.H = -1.0f;
            this.I = -1.0f;
            this.J = -1.0f;
            this.K = -1.0f;
            this.L = -1.0f;
            this.N = 1;
            this.O = -1;
            this.P = 8000;
            this.Q = 0L;
            this.R = 0L;
            this.U = true;
            this.Y = "eng";
        }
        
        private static Pair<String, List<byte[]>> a(final y y) throws com.applovin.exoplayer2.ai {
            try {
                y.e(16);
                final long p = y.p();
                if (p == 1482049860L) {
                    return (Pair<String, List<byte[]>>)new Pair((Object)"video/divx", (Object)null);
                }
                if (p == 859189832L) {
                    return (Pair<String, List<byte[]>>)new Pair((Object)"video/3gpp", (Object)null);
                }
                if (p == 826496599L) {
                    int i = y.c() + 20;
                    for (byte[] d = y.d(); i < d.length - 4; ++i) {
                        if (d[i] == 0 && d[i + 1] == 0 && d[i + 2] == 1 && d[i + 3] == 15) {
                            return (Pair<String, List<byte[]>>)new Pair((Object)"video/wvc1", (Object)Collections.singletonList(Arrays.copyOfRange(d, i, d.length)));
                        }
                    }
                    throw com.applovin.exoplayer2.ai.b("Failed to find FourCC VC1 initialization data", null);
                }
                q.c("MatroskaExtractor", "Unknown FourCC. Setting mimeType to video/x-unknown");
                return (Pair<String, List<byte[]>>)new Pair((Object)"video/x-unknown", (Object)null);
            }
            catch (final ArrayIndexOutOfBoundsException ex) {
                throw com.applovin.exoplayer2.ai.b("Error parsing FourCC private data", null);
            }
        }
        
        private static List<byte[]> a(final byte[] array) throws com.applovin.exoplayer2.ai {
            Label_0203: {
                if (array[0] != 2) {
                    break Label_0203;
                }
                int n = 1;
                int n2 = 0;
                byte b;
                while (true) {
                    b = array[n];
                    if ((b & 0xFF) != 0xFF) {
                        break;
                    }
                    n2 += 255;
                    ++n;
                }
                ++n;
                final int n3 = n2 + (b & 0xFF);
                int n4 = 0;
                byte b2;
                while (true) {
                    b2 = array[n];
                    if ((b2 & 0xFF) != 0xFF) {
                        break;
                    }
                    n4 += 255;
                    ++n;
                }
                ++n;
                Label_0196: {
                    if (array[n] != 1) {
                        break Label_0196;
                    }
                    try {
                        final byte[] array2 = new byte[n3];
                        System.arraycopy(array, n, array2, 0, n3);
                        final int n5 = n + n3;
                        if (array[n5] != 3) {
                            throw com.applovin.exoplayer2.ai.b("Error parsing vorbis codec private", null);
                        }
                        final int n6 = n5 + (n4 + (b2 & 0xFF));
                        if (array[n6] == 5) {
                            final byte[] array3 = new byte[array.length - n6];
                            System.arraycopy(array, n6, array3, 0, array.length - n6);
                            final ArrayList list = new ArrayList(2);
                            list.add(array2);
                            list.add(array3);
                            return list;
                        }
                        throw com.applovin.exoplayer2.ai.b("Error parsing vorbis codec private", null);
                        throw com.applovin.exoplayer2.ai.b("Error parsing vorbis codec private", null);
                        throw com.applovin.exoplayer2.ai.b("Error parsing vorbis codec private", null);
                    }
                    catch (final ArrayIndexOutOfBoundsException ex) {
                        throw com.applovin.exoplayer2.ai.b("Error parsing vorbis codec private", null);
                    }
                }
            }
        }
        
        private byte[] a(final String str) throws com.applovin.exoplayer2.ai {
            final byte[] j = this.j;
            if (j != null) {
                return j;
            }
            final StringBuilder sb = new StringBuilder();
            sb.append("Missing CodecPrivate for codec ");
            sb.append(str);
            throw com.applovin.exoplayer2.ai.b(sb.toString(), null);
        }
        
        private static boolean b(final y y) throws com.applovin.exoplayer2.ai {
            try {
                final int j = y.j();
                boolean b = true;
                if (j == 1) {
                    return true;
                }
                if (j == 65534) {
                    y.d(24);
                    if (y.s() != com.applovin.exoplayer2.e.e.d.e.getMostSignificantBits() || y.s() != com.applovin.exoplayer2.e.e.d.e.getLeastSignificantBits()) {
                        b = false;
                    }
                    return b;
                }
                return false;
            }
            catch (final ArrayIndexOutOfBoundsException ex) {
                throw com.applovin.exoplayer2.ai.b("Error parsing MS/ACM codec private", null);
            }
        }
        
        @Nullable
        private byte[] c() {
            if (this.C != -1.0f && this.D != -1.0f && this.E != -1.0f && this.F != -1.0f && this.G != -1.0f && this.H != -1.0f && this.I != -1.0f && this.J != -1.0f && this.K != -1.0f && this.L != -1.0f) {
                final byte[] array = new byte[25];
                final ByteBuffer order = ByteBuffer.wrap(array).order(ByteOrder.LITTLE_ENDIAN);
                order.put((byte)0);
                order.putShort((short)(this.C * 50000.0f + 0.5f));
                order.putShort((short)(this.D * 50000.0f + 0.5f));
                order.putShort((short)(this.E * 50000.0f + 0.5f));
                order.putShort((short)(this.F * 50000.0f + 0.5f));
                order.putShort((short)(this.G * 50000.0f + 0.5f));
                order.putShort((short)(this.H * 50000.0f + 0.5f));
                order.putShort((short)(this.I * 50000.0f + 0.5f));
                order.putShort((short)(this.J * 50000.0f + 0.5f));
                order.putShort((short)(this.K + 0.5f));
                order.putShort((short)(this.L + 0.5f));
                order.putShort((short)this.A);
                order.putShort((short)this.B);
                return array;
            }
            return null;
        }
        
        private void d() {
            com.applovin.exoplayer2.l.a.b((Object)this.V);
        }
        
        public void a() {
            final c s = this.S;
            if (s != null) {
                s.a(this);
            }
        }
        
        public void a(final j j, final int n) throws com.applovin.exoplayer2.ai {
            final String b = this.b;
            b.hashCode();
            final int hashCode = b.hashCode();
            final int n2 = 4;
            final int n3 = 3;
            final int n4 = 0;
            int n5 = 0;
            Label_0965: {
                switch (hashCode) {
                    case 1951062397: {
                        if (!b.equals("A_OPUS")) {
                            break;
                        }
                        n5 = 31;
                        break Label_0965;
                    }
                    case 1950789798: {
                        if (!b.equals("A_FLAC")) {
                            break;
                        }
                        n5 = 30;
                        break Label_0965;
                    }
                    case 1950749482: {
                        if (!b.equals("A_EAC3")) {
                            break;
                        }
                        n5 = 29;
                        break Label_0965;
                    }
                    case 1809237540: {
                        if (!b.equals("V_MPEG2")) {
                            break;
                        }
                        n5 = 28;
                        break Label_0965;
                    }
                    case 1422270023: {
                        if (!b.equals("S_TEXT/UTF8")) {
                            break;
                        }
                        n5 = 27;
                        break Label_0965;
                    }
                    case 855502857: {
                        if (!b.equals("V_MPEGH/ISO/HEVC")) {
                            break;
                        }
                        n5 = 26;
                        break Label_0965;
                    }
                    case 738597099: {
                        if (!b.equals("S_TEXT/ASS")) {
                            break;
                        }
                        n5 = 25;
                        break Label_0965;
                    }
                    case 725957860: {
                        if (!b.equals("A_PCM/INT/LIT")) {
                            break;
                        }
                        n5 = 24;
                        break Label_0965;
                    }
                    case 725948237: {
                        if (!b.equals("A_PCM/INT/BIG")) {
                            break;
                        }
                        n5 = 23;
                        break Label_0965;
                    }
                    case 635596514: {
                        if (!b.equals("A_PCM/FLOAT/IEEE")) {
                            break;
                        }
                        n5 = 22;
                        break Label_0965;
                    }
                    case 542569478: {
                        if (!b.equals("A_DTS/EXPRESS")) {
                            break;
                        }
                        n5 = 21;
                        break Label_0965;
                    }
                    case 444813526: {
                        if (!b.equals("V_THEORA")) {
                            break;
                        }
                        n5 = 20;
                        break Label_0965;
                    }
                    case 99146302: {
                        if (!b.equals("S_HDMV/PGS")) {
                            break;
                        }
                        n5 = 19;
                        break Label_0965;
                    }
                    case 82338134: {
                        if (!b.equals("V_VP9")) {
                            break;
                        }
                        n5 = 18;
                        break Label_0965;
                    }
                    case 82338133: {
                        if (!b.equals("V_VP8")) {
                            break;
                        }
                        n5 = 17;
                        break Label_0965;
                    }
                    case 82318131: {
                        if (!b.equals("V_AV1")) {
                            break;
                        }
                        n5 = 16;
                        break Label_0965;
                    }
                    case 62927045: {
                        if (!b.equals("A_DTS")) {
                            break;
                        }
                        n5 = 15;
                        break Label_0965;
                    }
                    case 62923603: {
                        if (!b.equals("A_AC3")) {
                            break;
                        }
                        n5 = 14;
                        break Label_0965;
                    }
                    case 62923557: {
                        if (!b.equals("A_AAC")) {
                            break;
                        }
                        n5 = 13;
                        break Label_0965;
                    }
                    case -356037306: {
                        if (!b.equals("A_DTS/LOSSLESS")) {
                            break;
                        }
                        n5 = 12;
                        break Label_0965;
                    }
                    case -425012669: {
                        if (!b.equals("S_VOBSUB")) {
                            break;
                        }
                        n5 = 11;
                        break Label_0965;
                    }
                    case -538363109: {
                        if (!b.equals("V_MPEG4/ISO/AVC")) {
                            break;
                        }
                        n5 = 10;
                        break Label_0965;
                    }
                    case -538363189: {
                        if (!b.equals("V_MPEG4/ISO/ASP")) {
                            break;
                        }
                        n5 = 9;
                        break Label_0965;
                    }
                    case -933872740: {
                        if (!b.equals("S_DVBSUB")) {
                            break;
                        }
                        n5 = 8;
                        break Label_0965;
                    }
                    case -1373388978: {
                        if (!b.equals("V_MS/VFW/FOURCC")) {
                            break;
                        }
                        n5 = 7;
                        break Label_0965;
                    }
                    case -1482641357: {
                        if (!b.equals("A_MPEG/L3")) {
                            break;
                        }
                        n5 = 6;
                        break Label_0965;
                    }
                    case -1482641358: {
                        if (!b.equals("A_MPEG/L2")) {
                            break;
                        }
                        n5 = 5;
                        break Label_0965;
                    }
                    case -1730367663: {
                        if (!b.equals("A_VORBIS")) {
                            break;
                        }
                        n5 = 4;
                        break Label_0965;
                    }
                    case -1784763192: {
                        if (!b.equals("A_TRUEHD")) {
                            break;
                        }
                        n5 = 3;
                        break Label_0965;
                    }
                    case -1985379776: {
                        if (!b.equals("A_MS/ACM")) {
                            break;
                        }
                        n5 = 2;
                        break Label_0965;
                    }
                    case -2095575984: {
                        if (!b.equals("V_MPEG4/ISO/SP")) {
                            break;
                        }
                        n5 = 1;
                        break Label_0965;
                    }
                    case -2095576542: {
                        if (!b.equals("V_MPEG4/ISO/AP")) {
                            break;
                        }
                        n5 = 0;
                        break Label_0965;
                    }
                }
                n5 = -1;
            }
            String s = "audio/raw";
            List<byte[]> list = null;
            int n6 = 0;
            int n7 = 0;
            String s4 = null;
        Label_2179:
            while (true) {
                Label_2139: {
                    Label_1976: {
                        Label_1936: {
                            List list3 = null;
                            String s3 = null;
                            Label_1823: {
                                Label_1661: {
                                    String c = null;
                                    Label_1654: {
                                        List<byte[]> list2 = null;
                                        Label_1647: {
                                            switch (n5) {
                                                default: {
                                                    throw com.applovin.exoplayer2.ai.b("Unrecognized codec identifier.", null);
                                                }
                                                case 31: {
                                                    list = new ArrayList<byte[]>(3);
                                                    list.add(this.a(this.b));
                                                    final ByteBuffer allocate = ByteBuffer.allocate(8);
                                                    final ByteOrder little_ENDIAN = ByteOrder.LITTLE_ENDIAN;
                                                    list.add(allocate.order(little_ENDIAN).putLong(this.Q).array());
                                                    list.add(ByteBuffer.allocate(8).order(little_ENDIAN).putLong(this.R).array());
                                                    s = "audio/opus";
                                                    n6 = 5760;
                                                    break Label_1976;
                                                }
                                                case 30: {
                                                    list2 = Collections.singletonList(this.a(this.b));
                                                    s = "audio/flac";
                                                    break Label_1647;
                                                }
                                                case 29: {
                                                    final String s2 = "audio/eac3";
                                                    break Label_1636;
                                                }
                                                case 28: {
                                                    final String s2 = "video/mpeg2";
                                                    break Label_1636;
                                                }
                                                case 27: {
                                                    final String s2 = "application/x-subrip";
                                                    break Label_1636;
                                                }
                                                case 26: {
                                                    final com.applovin.exoplayer2.m.f a = com.applovin.exoplayer2.m.f.a(new y(this.a(this.b)));
                                                    list3 = a.a;
                                                    this.W = a.b;
                                                    s3 = a.c;
                                                    s = "video/hevc";
                                                    break Label_1823;
                                                }
                                                case 25: {
                                                    list2 = com.applovin.exoplayer2.common.a.s.a(com.applovin.exoplayer2.e.e.d.c, this.a(this.b));
                                                    s = "text/x-ssa";
                                                    break Label_1647;
                                                }
                                                case 24: {
                                                    if ((n7 = ai.c(this.O)) == 0) {
                                                        final StringBuilder sb = new StringBuilder();
                                                        sb.append("Unsupported little endian PCM bit depth: ");
                                                        sb.append(this.O);
                                                        sb.append(". Setting mimeType to ");
                                                        sb.append("audio/x-unknown");
                                                        com.applovin.exoplayer2.l.q.c("MatroskaExtractor", sb.toString());
                                                        break Label_2139;
                                                    }
                                                    break;
                                                }
                                                case 23: {
                                                    final int o = this.O;
                                                    if (o == 8) {
                                                        list = null;
                                                        s4 = null;
                                                        n7 = 3;
                                                        break Label_1661;
                                                    }
                                                    if (o == 16) {
                                                        n7 = 268435456;
                                                        break;
                                                    }
                                                    final StringBuilder sb2 = new StringBuilder();
                                                    sb2.append("Unsupported big endian PCM bit depth: ");
                                                    sb2.append(this.O);
                                                    sb2.append(". Setting mimeType to ");
                                                    sb2.append("audio/x-unknown");
                                                    com.applovin.exoplayer2.l.q.c("MatroskaExtractor", sb2.toString());
                                                    break Label_2139;
                                                }
                                                case 22: {
                                                    if (this.O == 32) {
                                                        n7 = n2;
                                                        break;
                                                    }
                                                    final StringBuilder sb3 = new StringBuilder();
                                                    sb3.append("Unsupported floating point PCM bit depth: ");
                                                    sb3.append(this.O);
                                                    sb3.append(". Setting mimeType to ");
                                                    sb3.append("audio/x-unknown");
                                                    com.applovin.exoplayer2.l.q.c("MatroskaExtractor", sb3.toString());
                                                    break Label_2139;
                                                }
                                                case 20: {
                                                    final String s2 = "video/x-unknown";
                                                    break Label_1636;
                                                }
                                                case 19: {
                                                    final String s2 = "application/pgs";
                                                    break Label_1636;
                                                }
                                                case 18: {
                                                    final String s2 = "video/x-vnd.on2.vp9";
                                                    break Label_1636;
                                                }
                                                case 17: {
                                                    final String s2 = "video/x-vnd.on2.vp8";
                                                    break Label_1636;
                                                }
                                                case 16: {
                                                    final String s2 = "video/av01";
                                                    break Label_1636;
                                                }
                                                case 15:
                                                case 21: {
                                                    final String s2 = "audio/vnd.dts";
                                                    break Label_1636;
                                                }
                                                case 14: {
                                                    final String s2 = "audio/ac3";
                                                    break Label_1636;
                                                }
                                                case 13: {
                                                    list = Collections.singletonList(this.a(this.b));
                                                    final com.applovin.exoplayer2.b.a.a a2 = com.applovin.exoplayer2.b.a.a(this.j);
                                                    this.P = a2.a;
                                                    this.N = a2.b;
                                                    c = a2.c;
                                                    s = "audio/mp4a-latm";
                                                    break Label_1654;
                                                }
                                                case 12: {
                                                    final String s2 = "audio/vnd.dts.hd";
                                                    break Label_1636;
                                                }
                                                case 11: {
                                                    list2 = com.applovin.exoplayer2.common.a.s.a(this.a(this.b));
                                                    s = "application/vobsub";
                                                    break Label_1647;
                                                }
                                                case 10: {
                                                    final com.applovin.exoplayer2.m.a a3 = com.applovin.exoplayer2.m.a.a(new y(this.a(this.b)));
                                                    list3 = a3.a;
                                                    this.W = a3.b;
                                                    s3 = a3.f;
                                                    s = "video/avc";
                                                    break Label_1823;
                                                }
                                                case 8: {
                                                    final byte[] array = new byte[4];
                                                    System.arraycopy(this.a(this.b), 0, array, 0, 4);
                                                    list2 = com.applovin.exoplayer2.common.a.s.a(array);
                                                    s = "application/dvbsubs";
                                                    break Label_1647;
                                                }
                                                case 7: {
                                                    final Pair<String, List<byte[]>> a4 = a(new y(this.a(this.b)));
                                                    s = (String)a4.first;
                                                    list2 = (List)a4.second;
                                                    break Label_1647;
                                                }
                                                case 6: {
                                                    s = "audio/mpeg";
                                                    break Label_1936;
                                                }
                                                case 5: {
                                                    s = "audio/mpeg-L2";
                                                    break Label_1936;
                                                }
                                                case 4: {
                                                    list = a(this.a(this.b));
                                                    s = "audio/vorbis";
                                                    n6 = 8192;
                                                    break Label_1976;
                                                }
                                                case 3: {
                                                    this.S = new c();
                                                    final String s2 = "audio/true-hd";
                                                    break Label_1636;
                                                }
                                                case 2: {
                                                    if (!b(new y(this.a(this.b)))) {
                                                        final StringBuilder sb4 = new StringBuilder();
                                                        sb4.append("Non-PCM MS/ACM is unsupported. Setting mimeType to ");
                                                        sb4.append("audio/x-unknown");
                                                        com.applovin.exoplayer2.l.q.c("MatroskaExtractor", sb4.toString());
                                                        break Label_2139;
                                                    }
                                                    if ((n7 = ai.c(this.O)) == 0) {
                                                        final StringBuilder sb5 = new StringBuilder();
                                                        sb5.append("Unsupported PCM bit depth: ");
                                                        sb5.append(this.O);
                                                        sb5.append(". Setting mimeType to ");
                                                        sb5.append("audio/x-unknown");
                                                        com.applovin.exoplayer2.l.q.c("MatroskaExtractor", sb5.toString());
                                                        break Label_2139;
                                                    }
                                                    break;
                                                }
                                                case 0:
                                                case 1:
                                                case 9: {
                                                    final byte[] i = this.j;
                                                    if (i == null) {
                                                        list2 = null;
                                                    }
                                                    else {
                                                        list2 = Collections.singletonList(i);
                                                    }
                                                    s = "video/mp4v-es";
                                                    break Label_1647;
                                                }
                                            }
                                            list = null;
                                            s4 = null;
                                            break Label_1661;
                                            final List list4 = null;
                                            String s2 = null;
                                            s = s2;
                                            list2 = list4;
                                        }
                                        c = null;
                                        list = list2;
                                    }
                                    n7 = -1;
                                    s4 = c;
                                }
                                n6 = -1;
                                break Label_2179;
                            }
                            n7 = -1;
                            n6 = -1;
                            final String s5 = s3;
                            list = list3;
                            s4 = s5;
                            break Label_2179;
                        }
                        list = null;
                        s4 = null;
                        n7 = -1;
                        n6 = 4096;
                        break Label_2179;
                    }
                    s4 = null;
                    n7 = -1;
                    break Label_2179;
                }
                final String s2 = "audio/x-unknown";
                continue;
            }
            final byte[] m = this.M;
            String c2 = s4;
            String anObject = s;
            if (m != null) {
                final com.applovin.exoplayer2.m.c a5 = com.applovin.exoplayer2.m.c.a(new y(m));
                c2 = s4;
                anObject = s;
                if (a5 != null) {
                    c2 = a5.c;
                    anObject = "video/dolby-vision";
                }
            }
            final boolean u = this.U;
            int n8;
            if (this.T) {
                n8 = 2;
            }
            else {
                n8 = 0;
            }
            final com.applovin.exoplayer2.v.a a6 = new com.applovin.exoplayer2.v.a();
            int n9;
            if (com.applovin.exoplayer2.l.u.a(anObject)) {
                a6.k(this.N).l(this.P).m(n7);
                n9 = 1;
            }
            else if (com.applovin.exoplayer2.l.u.b(anObject)) {
                if (this.p == 0) {
                    int n10;
                    if ((n10 = this.n) == -1) {
                        n10 = this.l;
                    }
                    this.n = n10;
                    int o2;
                    if ((o2 = this.o) == -1) {
                        o2 = this.m;
                    }
                    this.o = o2;
                }
                final int n11 = -1;
                final int n12 = this.n;
                float n13 = 0.0f;
                Label_2429: {
                    if (n12 != -1) {
                        final int o3 = this.o;
                        if (o3 != -1) {
                            n13 = this.m * n12 / (float)(this.l * o3);
                            break Label_2429;
                        }
                    }
                    n13 = -1.0f;
                }
                com.applovin.exoplayer2.m.b b2;
                if (this.w) {
                    b2 = new com.applovin.exoplayer2.m.b(this.x, this.z, this.y, this.c());
                }
                else {
                    b2 = null;
                }
                int intValue = n11;
                if (this.a != null) {
                    intValue = n11;
                    if (com.applovin.exoplayer2.e.e.d.f.containsKey(this.a)) {
                        intValue = com.applovin.exoplayer2.e.e.d.f.get(this.a);
                    }
                }
                if (this.q == 0 && Float.compare(this.r, 0.0f) == 0 && Float.compare(this.s, 0.0f) == 0) {
                    if (Float.compare(this.t, 0.0f) == 0) {
                        intValue = n4;
                    }
                    else if (Float.compare(this.s, 90.0f) == 0) {
                        intValue = 90;
                    }
                    else if (Float.compare(this.s, -180.0f) != 0 && Float.compare(this.s, 180.0f) != 0) {
                        if (Float.compare(this.s, -90.0f) == 0) {
                            intValue = 270;
                        }
                    }
                    else {
                        intValue = 180;
                    }
                }
                a6.g(this.l).h(this.m).b(n13).i(intValue).a(this.u).j(this.v).a(b2);
                n9 = 2;
            }
            else {
                n9 = n3;
                if (!"application/x-subrip".equals(anObject)) {
                    n9 = n3;
                    if (!"text/x-ssa".equals(anObject)) {
                        n9 = n3;
                        if (!"application/vobsub".equals(anObject)) {
                            n9 = n3;
                            if (!"application/pgs".equals(anObject)) {
                                if (!"application/dvbsubs".equals(anObject)) {
                                    throw com.applovin.exoplayer2.ai.b("Unexpected MIME type.", null);
                                }
                                n9 = n3;
                            }
                        }
                    }
                }
            }
            if (this.a != null && !com.applovin.exoplayer2.e.e.d.f.containsKey(this.a)) {
                a6.b(this.a);
            }
            (this.V = j.a(this.c, n9)).a(a6.a(n).f(anObject).f(n6).c(this.Y).b(((u | false) ? 1 : 0) | n8).a(list).d(c2).a(this.k).a());
        }
        
        public void b() {
            final c s = this.S;
            if (s != null) {
                s.a();
            }
        }
    }
    
    private static final class c
    {
        private final byte[] a;
        private boolean b;
        private int c;
        private long d;
        private int e;
        private int f;
        private int g;
        
        public c() {
            this.a = new byte[10];
        }
        
        public void a() {
            this.b = false;
            this.c = 0;
        }
        
        public void a(final b b) {
            if (this.c > 0) {
                b.V.a(this.d, this.e, this.f, this.g, b.i);
                this.c = 0;
            }
        }
        
        public void a(final b b, final long d, final int e, final int n, final int g) {
            if (!this.b) {
                return;
            }
            final int c = this.c;
            final int c2 = c + 1;
            this.c = c2;
            if (c == 0) {
                this.d = d;
                this.e = e;
                this.f = 0;
            }
            this.f += n;
            this.g = g;
            if (c2 >= 16) {
                this.a(b);
            }
        }
        
        public void a(final i i) throws IOException {
            if (this.b) {
                return;
            }
            i.d(this.a, 0, 10);
            i.a();
            if (com.applovin.exoplayer2.b.b.b(this.a) == 0) {
                return;
            }
            this.b = true;
        }
    }
}

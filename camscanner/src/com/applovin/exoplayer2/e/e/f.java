// 
// Decompiled by Procyon v0.6.0
// 

package com.applovin.exoplayer2.e.e;

import java.io.IOException;
import com.applovin.exoplayer2.e.i;

final class f
{
    private static final long[] a;
    private final byte[] b;
    private int c;
    private int d;
    
    static {
        a = new long[] { 128L, 64L, 32L, 16L, 8L, 4L, 2L, 1L };
    }
    
    public f() {
        this.b = new byte[8];
    }
    
    public static int a(int n) {
        int n2 = 0;
        while (true) {
            final long[] a = f.a;
            if (n2 >= a.length) {
                n = -1;
                break;
            }
            if ((a[n2] & (long)n) != 0x0L) {
                n = n2 + 1;
                break;
            }
            ++n2;
        }
        return n;
    }
    
    public static long a(final byte[] array, final int n, final boolean b) {
        long n3;
        final long n2 = n3 = ((long)array[0] & 0xFFL);
        if (b) {
            n3 = (n2 & ~f.a[n - 1]);
        }
        for (int i = 1; i < n; ++i) {
            n3 = (n3 << 8 | ((long)array[i] & 0xFFL));
        }
        return n3;
    }
    
    public long a(final i i, final boolean b, final boolean b2, final int n) throws IOException {
        if (this.c == 0) {
            if (!i.a(this.b, 0, 1, b)) {
                return -1L;
            }
            if ((this.d = a(this.b[0] & 0xFF)) == -1) {
                throw new IllegalStateException("No valid varint length mask found");
            }
            this.c = 1;
        }
        final int d = this.d;
        if (d > n) {
            this.c = 0;
            return -2L;
        }
        if (d != 1) {
            i.b(this.b, 1, d - 1);
        }
        this.c = 0;
        return a(this.b, this.d, b2);
    }
    
    public void a() {
        this.c = 0;
        this.d = 0;
    }
    
    public int b() {
        return this.d;
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.applovin.exoplayer2.e;

import com.applovin.exoplayer2.l.ai;
import java.io.IOException;
import androidx.annotation.Nullable;

public abstract class a
{
    protected final a a;
    protected final f b;
    @Nullable
    protected c c;
    private final int d;
    
    protected a(final d d, final f b, final long n, final long n2, final long n3, final long n4, final long n5, final long n6, final int d2) {
        this.b = b;
        this.d = d2;
        this.a = new a(d, n, n2, n3, n4, n5, n6);
    }
    
    protected final int a(final i i, final long a, final u u) {
        if (a == i.c()) {
            return 0;
        }
        u.a = a;
        return 1;
    }
    
    public int a(final i i, final u u) throws IOException {
        while (true) {
            final c c = (c)com.applovin.exoplayer2.l.a.a((Object)this.c);
            final long b = c.a();
            final long c2 = c.b();
            final long d = c.e();
            if (c2 - b <= this.d) {
                this.a(false, b);
                return this.a(i, b, u);
            }
            if (!this.a(i, d)) {
                return this.a(i, d, u);
            }
            i.a();
            final e a = this.b.a(i, c.c());
            final int a2 = a.b;
            if (a2 == -3) {
                this.a(false, d);
                return this.a(i, d, u);
            }
            if (a2 != -2) {
                if (a2 != -1) {
                    if (a2 == 0) {
                        this.a(i, a.d);
                        this.a(true, a.d);
                        return this.a(i, a.d, u);
                    }
                    throw new IllegalStateException("Invalid case");
                }
                else {
                    c.b(a.c, a.d);
                }
            }
            else {
                c.a(a.c, a.d);
            }
        }
    }
    
    public final v a() {
        return this.a;
    }
    
    public final void a(final long n) {
        final c c = this.c;
        if (c != null && c.d() == n) {
            return;
        }
        this.c = this.b(n);
    }
    
    protected final void a(final boolean b, final long n) {
        this.c = null;
        this.b.a();
        this.b(b, n);
    }
    
    protected final boolean a(final i i, long n) throws IOException {
        n -= i.c();
        if (n >= 0L && n <= 262144L) {
            i.b((int)n);
            return true;
        }
        return false;
    }
    
    protected c b(final long n) {
        return new c(n, this.a.b(n), this.a.c, this.a.d, this.a.e, this.a.f, this.a.g);
    }
    
    protected void b(final boolean b, final long n) {
    }
    
    public final boolean b() {
        return this.c != null;
    }
    
    public static class a implements v
    {
        private final d a;
        private final long b;
        private final long c;
        private final long d;
        private final long e;
        private final long f;
        private final long g;
        
        public a(final d a, final long b, final long c, final long d, final long e, final long f, final long g) {
            this.a = a;
            this.b = b;
            this.c = c;
            this.d = d;
            this.e = e;
            this.f = f;
            this.g = g;
        }
        
        @Override
        public v.a a(final long n) {
            return new v.a(new w(n, com.applovin.exoplayer2.e.a.c.a(this.a.timeUsToTargetTime(n), this.c, this.d, this.e, this.f, this.g)));
        }
        
        @Override
        public boolean a() {
            return true;
        }
        
        @Override
        public long b() {
            return this.b;
        }
        
        public long b(final long n) {
            return this.a.timeUsToTargetTime(n);
        }
    }
    
    public static final class b implements d
    {
        @Override
        public long timeUsToTargetTime(final long n) {
            return n;
        }
    }
    
    protected static class c
    {
        private final long a;
        private final long b;
        private final long c;
        private long d;
        private long e;
        private long f;
        private long g;
        private long h;
        
        protected c(final long a, final long b, final long d, final long e, final long f, final long g, final long c) {
            this.a = a;
            this.b = b;
            this.d = d;
            this.e = e;
            this.f = f;
            this.g = g;
            this.c = c;
            this.h = a(b, d, e, f, g, c);
        }
        
        private long a() {
            return this.f;
        }
        
        protected static long a(long n, final long n2, final long n3, final long n4, final long n5, final long n6) {
            if (n4 + 1L < n5 && n2 + 1L < n3) {
                n = (long)((n - n2) * ((n5 - n4) / (float)(n3 - n2)));
                return ai.a(n + n4 - n6 - n / 20L, n4, n5 - 1L);
            }
            return n4;
        }
        
        private void a(final long d, final long f) {
            this.d = d;
            this.f = f;
            this.f();
        }
        
        private long b() {
            return this.g;
        }
        
        private void b(final long e, final long g) {
            this.e = e;
            this.g = g;
            this.f();
        }
        
        private long c() {
            return this.b;
        }
        
        private long d() {
            return this.a;
        }
        
        private long e() {
            return this.h;
        }
        
        private void f() {
            this.h = a(this.b, this.d, this.e, this.f, this.g, this.c);
        }
    }
    
    protected interface d
    {
        long timeUsToTargetTime(final long p0);
    }
    
    public static final class e
    {
        public static final e a;
        private final int b;
        private final long c;
        private final long d;
        
        static {
            a = new e(-3, -9223372036854775807L, -1L);
        }
        
        private e(final int b, final long c, final long d) {
            this.b = b;
            this.c = c;
            this.d = d;
        }
        
        public static e a(final long n) {
            return new e(0, -9223372036854775807L, n);
        }
        
        public static e a(final long n, final long n2) {
            return new e(-1, n, n2);
        }
        
        public static e b(final long n, final long n2) {
            return new e(-2, n, n2);
        }
    }
    
    protected interface f
    {
        e a(final i p0, final long p1) throws IOException;
        
        void a();
    }
}

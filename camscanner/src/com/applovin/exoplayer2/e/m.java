// 
// Decompiled by Procyon v0.6.0
// 

package com.applovin.exoplayer2.e;

import java.io.IOException;
import com.applovin.exoplayer2.ai;
import com.applovin.exoplayer2.l.y;

public final class m
{
    public static int a(final y y, final int n) {
        switch (n) {
            default: {
                return -1;
            }
            case 8:
            case 9:
            case 10:
            case 11:
            case 12:
            case 13:
            case 14:
            case 15: {
                return 256 << n - 8;
            }
            case 7: {
                return y.i() + 1;
            }
            case 6: {
                return y.h() + 1;
            }
            case 2:
            case 3:
            case 4:
            case 5: {
                return 576 << n - 2;
            }
            case 1: {
                return 192;
            }
        }
    }
    
    public static long a(final i i, final p p2) throws IOException {
        i.a();
        boolean b = true;
        i.c(1);
        final byte[] array = { 0 };
        i.d(array, 0, 1);
        if ((array[0] & 0x1) != 0x1) {
            b = false;
        }
        i.c(2);
        int n;
        if (b) {
            n = 7;
        }
        else {
            n = 6;
        }
        final y y = new y(n);
        y.c(k.a(i, y.d(), 0, n));
        i.a();
        final a a = new a();
        if (a(y, p2, b, a)) {
            return a.a;
        }
        throw ai.b(null, null);
    }
    
    private static boolean a(final int n, final p p2) {
        final boolean b = false;
        boolean b2 = false;
        if (n <= 7) {
            if (n == p2.g - 1) {
                b2 = true;
            }
            return b2;
        }
        boolean b3 = b;
        if (n <= 10) {
            b3 = b;
            if (p2.g == 2) {
                b3 = true;
            }
        }
        return b3;
    }
    
    public static boolean a(final i i, final p p4, final int n, final a a) throws IOException {
        final long b = i.b();
        final byte[] array = new byte[2];
        i.d(array, 0, 2);
        if (((array[0] & 0xFF) << 8 | (array[1] & 0xFF)) != n) {
            i.a();
            i.c((int)(b - i.c()));
            return false;
        }
        final y y = new y(16);
        System.arraycopy(array, 0, y.d(), 0, 2);
        y.c(k.a(i, y.d(), 2, 14));
        i.a();
        i.c((int)(b - i.c()));
        return a(y, p4, n, a);
    }
    
    private static boolean a(final y y, final p p3, int a) {
        a = a(y, a);
        return a != -1 && a <= p3.b;
    }
    
    public static boolean a(final y y, final p p4, int n, final a a) {
        final int c = y.c();
        final long o = y.o();
        final long n2 = o >>> 16;
        final long n3 = n;
        final boolean b = false;
        if (n2 != n3) {
            return false;
        }
        final boolean b2 = (n2 & 0x1L) == 0x1L;
        final int n4 = (int)(o >> 12 & 0xFL);
        final int n5 = (int)(o >> 8 & 0xFL);
        final int n6 = (int)(0xFL & o >> 4);
        final int n7 = (int)(o >> 1 & 0x7L);
        if ((o & 0x1L) == 0x1L) {
            n = 1;
        }
        else {
            n = 0;
        }
        boolean b3 = b;
        if (a(n6, p4)) {
            b3 = b;
            if (b(n7, p4)) {
                b3 = b;
                if (n == 0) {
                    b3 = b;
                    if (a(y, p4, b2, a)) {
                        b3 = b;
                        if (a(y, p4, n4)) {
                            b3 = b;
                            if (b(y, p4, n5)) {
                                b3 = b;
                                if (b(y, c)) {
                                    b3 = true;
                                }
                            }
                        }
                    }
                }
            }
        }
        return b3;
    }
    
    private static boolean a(final y y, final p p4, final boolean b, final a a) {
        try {
            long d = y.D();
            if (!b) {
                d *= p4.b;
            }
            a.a = d;
            return true;
        }
        catch (final NumberFormatException ex) {
            return false;
        }
    }
    
    private static boolean b(final int n, final p p2) {
        boolean b = true;
        if (n == 0) {
            return true;
        }
        if (n != p2.i) {
            b = false;
        }
        return b;
    }
    
    private static boolean b(final y y, final int n) {
        final int h = y.h();
        final int c = y.c();
        final byte[] d = y.d();
        boolean b = true;
        if (h != com.applovin.exoplayer2.l.ai.b(d, n, c - 1, 0)) {
            b = false;
        }
        return b;
    }
    
    private static boolean b(final y y, final p p3, final int n) {
        final int e = p3.e;
        final boolean b = true;
        boolean b2 = true;
        final boolean b3 = true;
        if (n == 0) {
            return true;
        }
        if (n <= 11) {
            return n == p3.f && b3;
        }
        if (n == 12) {
            return y.h() * 1000 == e && b;
        }
        if (n <= 14) {
            int i = y.i();
            if (n == 14) {
                i *= 10;
            }
            if (i != e) {
                b2 = false;
            }
            return b2;
        }
        return false;
    }
    
    public static final class a
    {
        public long a;
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.applovin.exoplayer2.e;

public class d implements v
{
    private final long a;
    private final long b;
    private final int c;
    private final long d;
    private final int e;
    private final long f;
    private final boolean g;
    
    public d(final long a, final long b, final int e, final int n, final boolean g) {
        this.a = a;
        this.b = b;
        int c = n;
        if (n == -1) {
            c = 1;
        }
        this.c = c;
        this.e = e;
        this.g = g;
        if (a == -1L) {
            this.d = -1L;
            this.f = -9223372036854775807L;
        }
        else {
            this.d = a - b;
            this.f = a(a, b, e);
        }
    }
    
    private static long a(final long n, final long n2, final int n3) {
        return Math.max(0L, n - n2) * 8L * 1000000L / n3;
    }
    
    private long c(long a) {
        a = a * this.e / 8000000L;
        final int c = this.c;
        final long a2 = a / c * c;
        final long d = this.d;
        a = a2;
        if (d != -1L) {
            a = Math.min(a2, d - c);
        }
        a = Math.max(a, 0L);
        return this.b + a;
    }
    
    @Override
    public a a(long n) {
        if (this.d == -1L && !this.g) {
            return new a(new w(0L, this.b));
        }
        final long c = this.c(n);
        final long b = this.b(c);
        final w w = new w(b, c);
        if (this.d != -1L && b < n) {
            final int c2 = this.c;
            if (c2 + c < this.a) {
                n = c + c2;
                return new a(w, new w(this.b(n), n));
            }
        }
        return new a(w);
    }
    
    @Override
    public boolean a() {
        return this.d != -1L || this.g;
    }
    
    @Override
    public long b() {
        return this.f;
    }
    
    public long b(final long n) {
        return a(n, this.b, this.e);
    }
}

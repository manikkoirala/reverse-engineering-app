// 
// Decompiled by Procyon v0.6.0
// 

package com.applovin.exoplayer2.e;

import java.util.HashMap;
import java.util.ArrayList;
import java.util.Map;
import android.net.Uri;
import com.applovin.exoplayer2.e.c.b;
import com.applovin.exoplayer2.e.f.d;
import com.applovin.exoplayer2.e.g.g;
import com.applovin.exoplayer2.e.g.e;
import com.applovin.exoplayer2.e.h.c;
import com.applovin.exoplayer2.e.i.w;
import com.applovin.exoplayer2.e.i.ac;
import com.applovin.exoplayer2.e.d.a;
import java.util.List;
import androidx.annotation.Nullable;
import java.lang.reflect.Constructor;

public final class f implements l
{
    private static final int[] b;
    @Nullable
    private static final Constructor<? extends h> c;
    private boolean d;
    private boolean e;
    private int f;
    private int g;
    private int h;
    private int i;
    private int j;
    private int k;
    private int l;
    private int m;
    private int n;
    private int o;
    
    static {
        b = new int[] { 5, 4, 12, 8, 3, 10, 9, 11, 6, 2, 0, 1, 7, 14 };
        try {
            if (Boolean.TRUE.equals(Class.forName("com.applovin.exoplayer2.ext.flac.FlacLibrary").getMethod("isAvailable", (Class<?>[])new Class[0]).invoke(null, new Object[0]))) {
                Class.forName("com.applovin.exoplayer2.ext.flac.FlacExtractor").asSubclass(h.class).getConstructor(Integer.TYPE);
                goto Label_0153;
            }
            goto Label_0153;
        }
        catch (final Exception cause) {
            throw new RuntimeException("Error instantiating FLAC extension", cause);
        }
        catch (final ClassNotFoundException ex) {
            goto Label_0153;
        }
    }
    
    public f() {
        this.m = 1;
        this.o = 112800;
    }
    
    private void a(int n, final List<h> list) {
        int n2 = 2;
        switch (n) {
            case 14: {
                list.add(new a());
                break;
            }
            case 12: {
                list.add(new com.applovin.exoplayer2.e.j.a());
                break;
            }
            case 11: {
                list.add(new ac(this.m, this.n, this.o));
                break;
            }
            case 10: {
                list.add(new w());
                break;
            }
            case 9: {
                list.add(new c());
                break;
            }
            case 8: {
                list.add(new e(this.k));
                list.add(new g(this.j));
                break;
            }
            case 7: {
                n = this.l;
                final boolean d = this.d;
                if (!this.e) {
                    n2 = 0;
                }
                list.add(new d(n2 | (n | (d ? 1 : 0))));
                break;
            }
            case 6: {
                list.add(new com.applovin.exoplayer2.e.e.d(this.i));
                break;
            }
            case 5: {
                list.add(new b());
                break;
            }
            case 4: {
                final Constructor<? extends h> c = com.applovin.exoplayer2.e.f.c;
                if (c != null) {
                    try {
                        list.add((a)c.newInstance(this.h));
                        break;
                    }
                    catch (final Exception cause) {
                        throw new IllegalStateException("Unexpected error creating FLAC extractor", cause);
                    }
                }
                list.add(new com.applovin.exoplayer2.e.b.b(this.h));
                break;
            }
            case 3: {
                n = this.g;
                final boolean d2 = this.d;
                if (!this.e) {
                    n2 = 0;
                }
                list.add(new com.applovin.exoplayer2.e.a.a(n2 | (n | (d2 ? 1 : 0))));
                break;
            }
            case 2: {
                n = this.f;
                final boolean d3 = this.d;
                if (!this.e) {
                    n2 = 0;
                }
                list.add(new com.applovin.exoplayer2.e.i.e(n2 | (n | (d3 ? 1 : 0))));
                break;
            }
            case 1: {
                list.add(new com.applovin.exoplayer2.e.i.c());
                break;
            }
            case 0: {
                list.add(new com.applovin.exoplayer2.e.i.a());
                break;
            }
        }
    }
    
    @Override
    public h[] a(final Uri uri, final Map<String, List<String>> map) {
        synchronized (this) {
            final ArrayList list = new ArrayList<h>(14);
            final int a = com.applovin.exoplayer2.l.l.a((Map)map);
            if (a != -1) {
                this.a(a, list);
            }
            final int a2 = com.applovin.exoplayer2.l.l.a(uri);
            if (a2 != -1 && a2 != a) {
                this.a(a2, list);
            }
            for (final int n : com.applovin.exoplayer2.e.f.b) {
                if (n != a && n != a2) {
                    this.a(n, list);
                }
            }
            return (h[])list.toArray(new h[list.size()]);
        }
    }
    
    @Override
    public h[] createExtractors() {
        synchronized (this) {
            return this.a(Uri.EMPTY, new HashMap<String, List<String>>());
        }
    }
}

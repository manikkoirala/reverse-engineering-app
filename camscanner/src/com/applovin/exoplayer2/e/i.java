// 
// Decompiled by Procyon v0.6.0
// 

package com.applovin.exoplayer2.e;

import java.io.IOException;
import com.applovin.exoplayer2.k.g;

public interface i extends g
{
    int a(final int p0) throws IOException;
    
    int a(final byte[] p0, final int p1, final int p2) throws IOException;
    
    void a();
    
    boolean a(final byte[] p0, final int p1, final int p2, final boolean p3) throws IOException;
    
    long b();
    
    void b(final int p0) throws IOException;
    
    void b(final byte[] p0, final int p1, final int p2) throws IOException;
    
    boolean b(final int p0, final boolean p1) throws IOException;
    
    boolean b(final byte[] p0, final int p1, final int p2, final boolean p3) throws IOException;
    
    int c(final byte[] p0, final int p1, final int p2) throws IOException;
    
    long c();
    
    void c(final int p0) throws IOException;
    
    long d();
    
    void d(final byte[] p0, final int p1, final int p2) throws IOException;
}

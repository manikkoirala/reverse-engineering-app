// 
// Decompiled by Procyon v0.6.0
// 

package com.applovin.exoplayer2.e;

import java.util.Arrays;
import com.applovin.exoplayer2.l.ai;
import java.io.IOException;
import java.io.EOFException;
import java.io.InterruptedIOException;
import com.applovin.exoplayer2.k.g;

public final class e implements i
{
    private final byte[] a;
    private final g b;
    private final long c;
    private long d;
    private byte[] e;
    private int f;
    private int g;
    
    public e(final g b, final long d, final long c) {
        this.b = b;
        this.d = d;
        this.c = c;
        this.e = new byte[65536];
        this.a = new byte[4096];
    }
    
    private int a(final byte[] array, int a, final int n, final int n2, final boolean b) throws IOException {
        if (Thread.interrupted()) {
            throw new InterruptedIOException();
        }
        a = this.b.a(array, a + n2, n - n2);
        if (a != -1) {
            return n2 + a;
        }
        if (n2 == 0 && b) {
            return -1;
        }
        throw new EOFException();
    }
    
    private void d(int a) {
        a += this.f;
        final byte[] e = this.e;
        if (a > e.length) {
            a = ai.a(e.length * 2, 65536 + a, a + 524288);
            this.e = Arrays.copyOf(this.e, a);
        }
    }
    
    private int e(int min) {
        min = Math.min(this.g, min);
        this.f(min);
        return min;
    }
    
    private int e(final byte[] array, final int n, int min) {
        final int g = this.g;
        if (g == 0) {
            return 0;
        }
        min = Math.min(g, min);
        System.arraycopy(this.e, 0, array, n, min);
        this.f(min);
        return min;
    }
    
    private void f(final int n) {
        final int g = this.g - n;
        this.g = g;
        this.f = 0;
        final byte[] e = this.e;
        byte[] e2;
        if (g < e.length - 524288) {
            e2 = new byte[65536 + g];
        }
        else {
            e2 = e;
        }
        System.arraycopy(e, n, e2, 0, g);
        this.e = e2;
    }
    
    private void g(final int n) {
        if (n != -1) {
            this.d += n;
        }
    }
    
    @Override
    public int a(final int a) throws IOException {
        int n;
        if ((n = this.e(a)) == 0) {
            final byte[] a2 = this.a;
            n = this.a(a2, 0, Math.min(a, a2.length), 0, true);
        }
        this.g(n);
        return n;
    }
    
    @Override
    public int a(final byte[] array, final int n, final int n2) throws IOException {
        int n3;
        if ((n3 = this.e(array, n, n2)) == 0) {
            n3 = this.a(array, n, n2, 0, true);
        }
        this.g(n3);
        return n3;
    }
    
    @Override
    public void a() {
        this.f = 0;
    }
    
    public boolean a(final int a, final boolean b) throws IOException {
        int n;
        for (n = this.e(a); n < a && n != -1; n = this.a(this.a, -n, Math.min(a, this.a.length + n), n, b)) {}
        this.g(n);
        return n != -1;
    }
    
    @Override
    public boolean a(final byte[] array, final int n, final int n2, final boolean b) throws IOException {
        int n3;
        for (n3 = this.e(array, n, n2); n3 < n2 && n3 != -1; n3 = this.a(array, n, n2, n3, b)) {}
        this.g(n3);
        return n3 != -1;
    }
    
    @Override
    public long b() {
        return this.d + this.f;
    }
    
    @Override
    public void b(final int n) throws IOException {
        this.a(n, false);
    }
    
    @Override
    public void b(final byte[] array, final int n, final int n2) throws IOException {
        this.a(array, n, n2, false);
    }
    
    @Override
    public boolean b(final int n, final boolean b) throws IOException {
        this.d(n);
        int i = this.g - this.f;
        while (i < n) {
            i = this.a(this.e, this.f, n, i, b);
            if (i == -1) {
                return false;
            }
            this.g = this.f + i;
        }
        this.f += n;
        return true;
    }
    
    @Override
    public boolean b(final byte[] array, final int n, final int n2, final boolean b) throws IOException {
        if (!this.b(n2, b)) {
            return false;
        }
        System.arraycopy(this.e, this.f - n2, array, n, n2);
        return true;
    }
    
    @Override
    public int c(final byte[] array, final int n, int a) throws IOException {
        this.d(a);
        final int g = this.g;
        final int f = this.f;
        final int b = g - f;
        if (b == 0) {
            a = this.a(this.e, f, a, 0, true);
            if (a == -1) {
                return -1;
            }
            this.g += a;
        }
        else {
            a = Math.min(a, b);
        }
        System.arraycopy(this.e, this.f, array, n, a);
        this.f += a;
        return a;
    }
    
    @Override
    public long c() {
        return this.d;
    }
    
    @Override
    public void c(final int n) throws IOException {
        this.b(n, false);
    }
    
    @Override
    public long d() {
        return this.c;
    }
    
    @Override
    public void d(final byte[] array, final int n, final int n2) throws IOException {
        this.b(array, n, n2, false);
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.applovin.exoplayer2.e.d;

import java.util.List;
import java.io.Reader;
import java.io.StringReader;
import org.xmlpull.v1.XmlPullParserFactory;
import androidx.annotation.Nullable;
import com.applovin.exoplayer2.ai;
import com.applovin.exoplayer2.l.q;
import java.io.IOException;
import org.xmlpull.v1.XmlPullParserException;
import com.applovin.exoplayer2.l.aj;
import com.applovin.exoplayer2.common.a.s;
import org.xmlpull.v1.XmlPullParser;

final class e
{
    private static final String[] a;
    private static final String[] b;
    private static final String[] c;
    
    static {
        a = new String[] { "Camera:MotionPhoto", "GCamera:MotionPhoto", "Camera:MicroVideo", "GCamera:MicroVideo" };
        b = new String[] { "Camera:MotionPhotoPresentationTimestampUs", "GCamera:MotionPhotoPresentationTimestampUs", "Camera:MicroVideoPresentationTimestampUs", "GCamera:MicroVideoPresentationTimestampUs" };
        c = new String[] { "Camera:MicroVideoOffset", "GCamera:MicroVideoOffset" };
    }
    
    private static s<b.a> a(final XmlPullParser xmlPullParser, String string, final String s) throws XmlPullParserException, IOException {
        final s.a<b.a> i = s.i();
        final StringBuilder sb = new StringBuilder();
        sb.append(string);
        sb.append(":Item");
        final String string2 = sb.toString();
        final StringBuilder sb2 = new StringBuilder();
        sb2.append(string);
        sb2.append(":Directory");
        string = sb2.toString();
        do {
            xmlPullParser.next();
            if (aj.b(xmlPullParser, string2)) {
                final StringBuilder sb3 = new StringBuilder();
                sb3.append(s);
                sb3.append(":Mime");
                final String string3 = sb3.toString();
                final StringBuilder sb4 = new StringBuilder();
                sb4.append(s);
                sb4.append(":Semantic");
                final String string4 = sb4.toString();
                final StringBuilder sb5 = new StringBuilder();
                sb5.append(s);
                sb5.append(":Length");
                final String string5 = sb5.toString();
                final StringBuilder sb6 = new StringBuilder();
                sb6.append(s);
                sb6.append(":Padding");
                final String string6 = sb6.toString();
                final String c = aj.c(xmlPullParser, string3);
                final String c2 = aj.c(xmlPullParser, string4);
                final String c3 = aj.c(xmlPullParser, string5);
                final String c4 = aj.c(xmlPullParser, string6);
                if (c == null || c2 == null) {
                    return s.g();
                }
                long long1;
                if (c3 != null) {
                    long1 = Long.parseLong(c3);
                }
                else {
                    long1 = 0L;
                }
                long long2;
                if (c4 != null) {
                    long2 = Long.parseLong(c4);
                }
                else {
                    long2 = 0L;
                }
                i.b(new b.a(c, c2, long1, long2));
            }
        } while (!aj.a(xmlPullParser, string));
        return i.a();
    }
    
    @Nullable
    public static b a(final String s) throws IOException {
        try {
            return b(s);
        }
        catch (final XmlPullParserException | ai | NumberFormatException ex) {
            q.c("MotionPhotoXmpParser", "Ignoring unexpected XMP metadata");
            return null;
        }
    }
    
    private static boolean a(final XmlPullParser xmlPullParser) {
        final String[] a = e.a;
        final int length = a.length;
        boolean b = false;
        for (int i = 0; i < length; ++i) {
            final String c = aj.c(xmlPullParser, a[i]);
            if (c != null) {
                if (Integer.parseInt(c) == 1) {
                    b = true;
                }
                return b;
            }
        }
        return false;
    }
    
    private static long b(final XmlPullParser xmlPullParser) {
        final String[] b = e.b;
        final int length = b.length;
        int n = 0;
        while (true) {
            final long n2 = -9223372036854775807L;
            if (n >= length) {
                return -9223372036854775807L;
            }
            final String c = aj.c(xmlPullParser, b[n]);
            if (c != null) {
                long long1 = Long.parseLong(c);
                if (long1 == -1L) {
                    long1 = n2;
                }
                return long1;
            }
            ++n;
        }
    }
    
    @Nullable
    private static b b(final String s) throws XmlPullParserException, IOException {
        final XmlPullParser pullParser = XmlPullParserFactory.newInstance().newPullParser();
        pullParser.setInput((Reader)new StringReader(s));
        pullParser.next();
        if (!aj.b(pullParser, "x:xmpmeta")) {
            throw ai.b("Couldn't find xmp metadata", null);
        }
        List<E> g = (List<E>)s.g();
        long n = -9223372036854775807L;
        long b;
        List<b.a> list;
        do {
            pullParser.next();
            if (aj.b(pullParser, "rdf:Description")) {
                if (!a(pullParser)) {
                    return null;
                }
                b = b(pullParser);
                list = c(pullParser);
            }
            else if (aj.b(pullParser, "Container:Directory")) {
                list = a(pullParser, "Container", "Item");
                b = n;
            }
            else {
                list = (List<b.a>)g;
                b = n;
                if (aj.b(pullParser, "GContainer:Directory")) {
                    list = a(pullParser, "GContainer", "GContainerItem");
                    b = n;
                }
            }
            g = (List<E>)list;
            n = b;
        } while (!aj.a(pullParser, "x:xmpmeta"));
        if (list.isEmpty()) {
            return null;
        }
        return new b(b, list);
    }
    
    private static s<b.a> c(final XmlPullParser xmlPullParser) {
        final String[] c = e.c;
        for (int length = c.length, i = 0; i < length; ++i) {
            final String c2 = aj.c(xmlPullParser, c[i]);
            if (c2 != null) {
                return s.a(new b.a("image/jpeg", "Primary", 0L, 0L), new b.a("video/mp4", "MotionPhoto", Long.parseLong(c2), 0L));
            }
        }
        return s.g();
    }
}

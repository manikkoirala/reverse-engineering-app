// 
// Decompiled by Procyon v0.6.0
// 

package com.applovin.exoplayer2.e.d;

import com.applovin.exoplayer2.l.a;
import com.applovin.exoplayer2.e.i;
import com.applovin.exoplayer2.e.q;

final class c extends q
{
    private final long a;
    
    public c(final i i, final long a) {
        super(i);
        com.applovin.exoplayer2.l.a.a(i.c() >= a);
        this.a = a;
    }
    
    @Override
    public long b() {
        return super.b() - this.a;
    }
    
    @Override
    public long c() {
        return super.c() - this.a;
    }
    
    @Override
    public long d() {
        return super.d() - this.a;
    }
}

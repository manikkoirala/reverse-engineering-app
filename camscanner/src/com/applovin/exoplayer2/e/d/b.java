// 
// Decompiled by Procyon v0.6.0
// 

package com.applovin.exoplayer2.e.d;

import androidx.annotation.Nullable;
import java.util.List;

final class b
{
    public final long a;
    public final List<a> b;
    
    public b(final long a, final List<a> b) {
        this.a = a;
        this.b = b;
    }
    
    @Nullable
    public com.applovin.exoplayer2.g.f.b a(long n) {
        if (this.b.size() < 2) {
            return null;
        }
        int i = this.b.size() - 1;
        final long n2 = n;
        long n3 = -1L;
        long n4 = -1L;
        long n5;
        n = (n5 = n4);
        boolean b = false;
        long n6 = n;
        n = n2;
        while (i >= 0) {
            final a a = this.b.get(i);
            b |= "video/mp4".equals(a.a);
            long n7;
            long n8;
            if (i == 0) {
                n7 = n - a.d;
                n8 = 0L;
            }
            else {
                n8 = n - a.c;
                n7 = n;
            }
            n = n8;
            if (b && n != n7) {
                n5 = n7 - n;
                n6 = n;
                b = false;
            }
            if (i == 0) {
                n3 = n;
                n4 = n7;
            }
            --i;
        }
        if (n6 != -1L && n5 != -1L && n3 != -1L && n4 != -1L) {
            return new com.applovin.exoplayer2.g.f.b(n3, n4, this.a, n6, n5);
        }
        return null;
    }
    
    public static final class a
    {
        public final String a;
        public final String b;
        public final long c;
        public final long d;
        
        public a(final String a, final String b, final long c, final long d) {
            this.a = a;
            this.b = b;
            this.c = c;
            this.d = d;
        }
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.applovin.exoplayer2.e.d;

import com.applovin.exoplayer2.e.w;
import com.applovin.exoplayer2.e.v;
import com.applovin.exoplayer2.e.x;
import com.applovin.exoplayer2.e.j;

public final class d implements j
{
    private final long b;
    private final j c;
    
    public d(final long b, final j c) {
        this.b = b;
        this.c = c;
    }
    
    @Override
    public x a(final int n, final int n2) {
        return this.c.a(n, n2);
    }
    
    @Override
    public void a() {
        this.c.a();
    }
    
    @Override
    public void a(final v v) {
        this.c.a(new v(this, v) {
            final v a;
            final d b;
            
            @Override
            public a a(final long n) {
                final a a = this.a.a(n);
                final w a2 = a.a;
                final w w = new w(a2.b, a2.c + this.b.b);
                final w b = a.b;
                return new a(w, new w(b.b, b.c + this.b.b));
            }
            
            @Override
            public boolean a() {
                return this.a.a();
            }
            
            @Override
            public long b() {
                return this.a.b();
            }
        });
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.applovin.exoplayer2.e.d;

import com.applovin.exoplayer2.e.u;
import com.applovin.exoplayer2.v;
import java.io.IOException;
import com.applovin.exoplayer2.e.g.g;
import com.applovin.exoplayer2.e.i;
import androidx.annotation.Nullable;
import com.applovin.exoplayer2.g.f.b;
import com.applovin.exoplayer2.e.j;
import com.applovin.exoplayer2.l.y;
import com.applovin.exoplayer2.e.h;

public final class a implements h
{
    private final y a;
    private j b;
    private int c;
    private int d;
    private int e;
    private long f;
    @Nullable
    private b g;
    private i h;
    private c i;
    @Nullable
    private g j;
    
    public a() {
        this.a = new y(6);
        this.f = -1L;
    }
    
    @Nullable
    private static b a(final String s, final long n) throws IOException {
        if (n == -1L) {
            return null;
        }
        final com.applovin.exoplayer2.e.d.b a = e.a(s);
        if (a == null) {
            return null;
        }
        return a.a(n);
    }
    
    private void a() {
        this.a((com.applovin.exoplayer2.g.a.a)com.applovin.exoplayer2.l.a.b((Object)this.g));
        this.c = 5;
    }
    
    private void a(final com.applovin.exoplayer2.g.a.a... array) {
        ((j)com.applovin.exoplayer2.l.a.b((Object)this.b)).a(1024, 4).a(new v.a().e("image/jpeg").a(new com.applovin.exoplayer2.g.a(array)).a());
    }
    
    private int b(final i i) throws IOException {
        this.a.a(2);
        i.d(this.a.d(), 0, 2);
        return this.a.i();
    }
    
    private void b() {
        this.a(new com.applovin.exoplayer2.g.a.a[0]);
        ((j)com.applovin.exoplayer2.l.a.b((Object)this.b)).a();
        this.b.a(new com.applovin.exoplayer2.e.v.b(-9223372036854775807L));
        this.c = 6;
    }
    
    private void c(final i i) throws IOException {
        this.a.a(2);
        i.d(this.a.d(), 0, 2);
        i.c(this.a.i() - 2);
    }
    
    private void d(final i i) throws IOException {
        this.a.a(2);
        i.b(this.a.d(), 0, 2);
        final int j = this.a.i();
        this.d = j;
        if (j == 65498) {
            if (this.f != -1L) {
                this.c = 4;
            }
            else {
                this.b();
            }
        }
        else if ((j < 65488 || j > 65497) && j != 65281) {
            this.c = 1;
        }
    }
    
    private void e(final i i) throws IOException {
        this.a.a(2);
        i.b(this.a.d(), 0, 2);
        this.e = this.a.i() - 2;
        this.c = 2;
    }
    
    private void f(final i i) throws IOException {
        if (this.d == 65505) {
            final y y = new y(this.e);
            i.b(y.d(), 0, this.e);
            if (this.g == null && "http://ns.adobe.com/xap/1.0/".equals(y.B())) {
                final String b = y.B();
                if (b != null) {
                    final b a = a(b, i.d());
                    if ((this.g = a) != null) {
                        this.f = a.d;
                    }
                }
            }
        }
        else {
            i.b(this.e);
        }
        this.c = 0;
    }
    
    private void g(final i i) throws IOException {
        if (!i.b(this.a.d(), 0, 1, true)) {
            this.b();
        }
        else {
            i.a();
            if (this.j == null) {
                this.j = new g();
            }
            final c j = new c(i, this.f);
            this.i = j;
            if (this.j.a(j)) {
                this.j.a(new d(this.f, (j)com.applovin.exoplayer2.l.a.b((Object)this.b)));
                this.a();
            }
            else {
                this.b();
            }
        }
    }
    
    @Override
    public int a(final i h, final u u) throws IOException {
        final int c = this.c;
        if (c == 0) {
            this.d(h);
            return 0;
        }
        if (c == 1) {
            this.e(h);
            return 0;
        }
        if (c == 2) {
            this.f(h);
            return 0;
        }
        if (c != 4) {
            if (c == 5) {
                if (this.i == null || h != this.h) {
                    this.h = h;
                    this.i = new c(h, this.f);
                }
                final int a = ((g)com.applovin.exoplayer2.l.a.b((Object)this.j)).a(this.i, u);
                if (a == 1) {
                    u.a += this.f;
                }
                return a;
            }
            if (c == 6) {
                return -1;
            }
            throw new IllegalStateException();
        }
        else {
            final long c2 = h.c();
            final long f = this.f;
            if (c2 != f) {
                u.a = f;
                return 1;
            }
            this.g(h);
            return 0;
        }
    }
    
    @Override
    public void a(final long n, final long n2) {
        if (n == 0L) {
            this.c = 0;
            this.j = null;
        }
        else if (this.c == 5) {
            ((g)com.applovin.exoplayer2.l.a.b((Object)this.j)).a(n, n2);
        }
    }
    
    @Override
    public void a(final j b) {
        this.b = b;
    }
    
    @Override
    public boolean a(final i i) throws IOException {
        final int b = this.b(i);
        final boolean b2 = false;
        if (b != 65496) {
            return false;
        }
        if ((this.d = this.b(i)) == 65504) {
            this.c(i);
            this.d = this.b(i);
        }
        if (this.d != 65505) {
            return false;
        }
        i.c(2);
        this.a.a(6);
        i.d(this.a.d(), 0, 6);
        boolean b3 = b2;
        if (this.a.o() == 1165519206L) {
            b3 = b2;
            if (this.a.i() == 0) {
                b3 = true;
            }
        }
        return b3;
    }
    
    @Override
    public void c() {
        final g j = this.j;
        if (j != null) {
            j.c();
        }
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.applovin.exoplayer2.e;

import java.io.EOFException;
import com.applovin.exoplayer2.ai;
import androidx.annotation.Nullable;
import java.io.IOException;

public final class k
{
    public static int a(final i i, final byte[] array, final int n, final int n2) throws IOException {
        int j;
        int c;
        for (j = 0; j < n2; j += c) {
            c = i.c(array, n + j, n2 - j);
            if (c == -1) {
                break;
            }
        }
        return j;
    }
    
    public static void a(final boolean b, @Nullable final String s) throws ai {
        if (b) {
            return;
        }
        throw ai.b(s, null);
    }
    
    public static boolean a(final i i, final int n) throws IOException {
        try {
            i.b(n);
            return true;
        }
        catch (final EOFException ex) {
            return false;
        }
    }
    
    public static boolean a(final i i, final byte[] array, final int n, final int n2, final boolean b) throws IOException {
        try {
            return i.b(array, n, n2, b);
        }
        catch (final EOFException ex) {
            if (b) {
                return false;
            }
            throw ex;
        }
    }
    
    public static boolean b(final i i, final byte[] array, final int n, final int n2) throws IOException {
        try {
            i.b(array, n, n2);
            return true;
        }
        catch (final EOFException ex) {
            return false;
        }
    }
}

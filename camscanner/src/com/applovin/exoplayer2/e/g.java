// 
// Decompiled by Procyon v0.6.0
// 

package com.applovin.exoplayer2.e;

import com.applovin.exoplayer2.v;
import com.applovin.exoplayer2.l.y;
import androidx.annotation.Nullable;
import java.io.IOException;
import java.io.EOFException;

public final class g implements x
{
    private final byte[] a;
    
    public g() {
        this.a = new byte[4096];
    }
    
    @Override
    public int a(final com.applovin.exoplayer2.k.g g, int b, final boolean b2, final int n) throws IOException {
        b = Math.min(this.a.length, b);
        b = g.a(this.a, 0, b);
        if (b != -1) {
            return b;
        }
        if (b2) {
            return -1;
        }
        throw new EOFException();
    }
    
    @Override
    public void a(final long n, final int n2, final int n3, final int n4, @Nullable final a a) {
    }
    
    @Override
    public void a(final y y, final int n, final int n2) {
        y.e(n);
    }
    
    @Override
    public void a(final v v) {
    }
}

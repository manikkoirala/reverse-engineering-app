// 
// Decompiled by Procyon v0.6.0
// 

package com.applovin.exoplayer2.e;

import java.util.Arrays;
import com.applovin.exoplayer2.l.ai;

public final class c implements v
{
    public final int a;
    public final int[] b;
    public final long[] c;
    public final long[] d;
    public final long[] e;
    private final long f;
    
    public c(final int[] b, final long[] c, final long[] d, final long[] e) {
        this.b = b;
        this.c = c;
        this.d = d;
        this.e = e;
        final int length = b.length;
        this.a = length;
        if (length > 0) {
            this.f = d[length - 1] + e[length - 1];
        }
        else {
            this.f = 0L;
        }
    }
    
    @Override
    public a a(final long n) {
        int b = this.b(n);
        final w w = new w(this.e[b], this.c[b]);
        if (w.b < n && b != this.a - 1) {
            final long[] e = this.e;
            ++b;
            return new a(w, new w(e[b], this.c[b]));
        }
        return new a(w);
    }
    
    @Override
    public boolean a() {
        return true;
    }
    
    public int b(final long n) {
        return ai.a(this.e, n, true, true);
    }
    
    @Override
    public long b() {
        return this.f;
    }
    
    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append("ChunkIndex(length=");
        sb.append(this.a);
        sb.append(", sizes=");
        sb.append(Arrays.toString(this.b));
        sb.append(", offsets=");
        sb.append(Arrays.toString(this.c));
        sb.append(", timeUs=");
        sb.append(Arrays.toString(this.e));
        sb.append(", durationsUs=");
        sb.append(Arrays.toString(this.d));
        sb.append(")");
        return sb.toString();
    }
}

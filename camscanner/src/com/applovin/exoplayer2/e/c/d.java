// 
// Decompiled by Procyon v0.6.0
// 

package com.applovin.exoplayer2.e.c;

import com.applovin.exoplayer2.ai;
import com.applovin.exoplayer2.l.y;
import com.applovin.exoplayer2.e.x;

abstract class d
{
    protected final x a;
    
    protected d(final x a) {
        this.a = a;
    }
    
    protected abstract boolean a(final y p0) throws ai;
    
    protected abstract boolean a(final y p0, final long p1) throws ai;
    
    public final boolean b(final y y, final long n) throws ai {
        return this.a(y) && this.a(y, n);
    }
    
    public static final class a extends ai
    {
        public a(final String s) {
            super(s, null, false, 1);
        }
    }
}

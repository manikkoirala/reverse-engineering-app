// 
// Decompiled by Procyon v0.6.0
// 

package com.applovin.exoplayer2.e.c;

import com.applovin.exoplayer2.ai;
import java.util.Collections;
import com.applovin.exoplayer2.v;
import com.applovin.exoplayer2.l.y;
import com.applovin.exoplayer2.e.x;

final class a extends d
{
    private static final int[] b;
    private boolean c;
    private boolean d;
    private int e;
    
    static {
        b = new int[] { 5512, 11025, 22050, 44100 };
    }
    
    public a(final x x) {
        super(x);
    }
    
    @Override
    protected boolean a(final y y) throws d.a {
        if (!this.c) {
            final int h = y.h();
            final int e = h >> 4 & 0xF;
            if ((this.e = e) == 2) {
                super.a.a(new v.a().f("audio/mpeg").k(1).l(a.b[h >> 2 & 0x3]).a());
                this.d = true;
            }
            else if (e != 7 && e != 8) {
                if (e != 10) {
                    final StringBuilder sb = new StringBuilder();
                    sb.append("Audio format not supported: ");
                    sb.append(this.e);
                    throw new d.a(sb.toString());
                }
            }
            else {
                String s;
                if (e == 7) {
                    s = "audio/g711-alaw";
                }
                else {
                    s = "audio/g711-mlaw";
                }
                super.a.a(new v.a().f(s).k(1).l(8000).a());
                this.d = true;
            }
            this.c = true;
        }
        else {
            y.e(1);
        }
        return true;
    }
    
    @Override
    protected boolean a(final y y, final long n) throws ai {
        if (this.e == 2) {
            final int a = y.a();
            super.a.a(y, a);
            super.a.a(n, 1, a, 0, null);
            return true;
        }
        final int h = y.h();
        if (h == 0 && !this.d) {
            final int a2 = y.a();
            final byte[] o = new byte[a2];
            y.a(o, 0, a2);
            final com.applovin.exoplayer2.b.a.a a3 = com.applovin.exoplayer2.b.a.a(o);
            super.a.a(new v.a().f("audio/mp4a-latm").d(a3.c).k(a3.b).l(a3.a).a(Collections.singletonList(o)).a());
            this.d = true;
            return false;
        }
        if (this.e == 10 && h != 1) {
            return false;
        }
        final int a4 = y.a();
        super.a.a(y, a4);
        super.a.a(n, 1, a4, 0, null);
        return true;
    }
}

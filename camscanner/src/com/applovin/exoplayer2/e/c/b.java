// 
// Decompiled by Procyon v0.6.0
// 

package com.applovin.exoplayer2.e.c;

import com.applovin.exoplayer2.e.u;
import com.applovin.exoplayer2.e.t;
import java.io.IOException;
import com.applovin.exoplayer2.e.i;
import com.applovin.exoplayer2.e.v;
import com.applovin.exoplayer2.e.j;
import com.applovin.exoplayer2.l.y;
import com.applovin.exoplayer2.e.l;
import com.applovin.exoplayer2.e.h;

public final class b implements h
{
    public static final l a;
    private final y b;
    private final y c;
    private final y d;
    private final y e;
    private final c f;
    private j g;
    private int h;
    private boolean i;
    private long j;
    private int k;
    private int l;
    private int m;
    private long n;
    private boolean o;
    private a p;
    private e q;
    
    static {
        a = new \u3007080();
    }
    
    public b() {
        this.b = new y(4);
        this.c = new y(9);
        this.d = new y(11);
        this.e = new y();
        this.f = new c();
        this.h = 1;
    }
    
    private void a() {
        if (!this.o) {
            this.g.a(new v.b(-9223372036854775807L));
            this.o = true;
        }
    }
    
    private long b() {
        long n;
        if (this.i) {
            n = this.j + this.n;
        }
        else if (this.f.a() == -9223372036854775807L) {
            n = 0L;
        }
        else {
            n = this.n;
        }
        return n;
    }
    
    private boolean b(final i i) throws IOException {
        final byte[] d = this.c.d();
        boolean b = false;
        if (!i.a(d, 0, 9, true)) {
            return false;
        }
        this.c.d(0);
        this.c.e(4);
        final int h = this.c.h();
        final boolean b2 = (h & 0x4) != 0x0;
        if ((h & 0x1) != 0x0) {
            b = true;
        }
        if (b2 && this.p == null) {
            this.p = new a(this.g.a(8, 1));
        }
        if (b && this.q == null) {
            this.q = new e(this.g.a(9, 2));
        }
        this.g.a();
        this.k = this.c.q() - 9 + 4;
        this.h = 2;
        return true;
    }
    
    private void c(final i i) throws IOException {
        i.b(this.k);
        this.k = 0;
        this.h = 3;
    }
    
    private boolean d(final i i) throws IOException {
        if (!i.a(this.d.d(), 0, 11, true)) {
            return false;
        }
        this.d.d(0);
        this.l = this.d.h();
        this.m = this.d.m();
        this.n = this.d.m();
        this.n = ((long)(this.d.h() << 24) | this.n) * 1000L;
        this.d.e(3);
        this.h = 4;
        return true;
    }
    
    private boolean e(final i i) throws IOException {
        final long b = this.b();
        final int l = this.l;
        int n = 0;
        boolean b2 = false;
        Label_0190: {
            if (l == 8 && this.p != null) {
                this.a();
                n = (this.p.b(this.f(i), b) ? 1 : 0);
            }
            else if (l == 9 && this.q != null) {
                this.a();
                n = (this.q.b(this.f(i), b) ? 1 : 0);
            }
            else {
                if (l != 18 || this.o) {
                    i.b(this.m);
                    n = 0;
                    b2 = false;
                    break Label_0190;
                }
                final boolean b3 = this.f.b(this.f(i), b);
                final long a = this.f.a();
                n = (b3 ? 1 : 0);
                if (a != -9223372036854775807L) {
                    this.g.a(new t(this.f.c(), this.f.b(), a));
                    this.o = true;
                    n = (b3 ? 1 : 0);
                }
            }
            b2 = true;
        }
        if (!this.i && n != 0) {
            this.i = true;
            long j;
            if (this.f.a() == -9223372036854775807L) {
                j = -this.n;
            }
            else {
                j = 0L;
            }
            this.j = j;
        }
        this.k = 4;
        this.h = 2;
        return b2;
    }
    
    private y f(final i i) throws IOException {
        if (this.m > this.e.e()) {
            final y e = this.e;
            e.a(new byte[Math.max(e.e() * 2, this.m)], 0);
        }
        else {
            this.e.d(0);
        }
        this.e.c(this.m);
        i.b(this.e.d(), 0, this.m);
        return this.e;
    }
    
    @Override
    public int a(final i i, final u u) throws IOException {
        com.applovin.exoplayer2.l.a.a((Object)this.g);
        while (true) {
            final int h = this.h;
            if (h != 1) {
                if (h != 2) {
                    if (h != 3) {
                        if (h != 4) {
                            throw new IllegalStateException();
                        }
                        if (this.e(i)) {
                            return 0;
                        }
                        continue;
                    }
                    else {
                        if (!this.d(i)) {
                            return -1;
                        }
                        continue;
                    }
                }
                else {
                    this.c(i);
                }
            }
            else {
                if (!this.b(i)) {
                    return -1;
                }
                continue;
            }
        }
    }
    
    @Override
    public void a(final long n, final long n2) {
        if (n == 0L) {
            this.h = 1;
            this.i = false;
        }
        else {
            this.h = 3;
        }
        this.k = 0;
    }
    
    @Override
    public void a(final j g) {
        this.g = g;
    }
    
    @Override
    public boolean a(final i i) throws IOException {
        final byte[] d = this.b.d();
        boolean b = false;
        i.d(d, 0, 3);
        this.b.d(0);
        if (this.b.m() != 4607062) {
            return false;
        }
        i.d(this.b.d(), 0, 2);
        this.b.d(0);
        if ((this.b.i() & 0xFA) != 0x0) {
            return false;
        }
        i.d(this.b.d(), 0, 4);
        this.b.d(0);
        final int q = this.b.q();
        i.a();
        i.c(q);
        i.d(this.b.d(), 0, 4);
        this.b.d(0);
        if (this.b.q() == 0) {
            b = true;
        }
        return b;
    }
    
    @Override
    public void c() {
    }
}

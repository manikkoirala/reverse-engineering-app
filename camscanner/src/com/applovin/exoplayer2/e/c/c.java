// 
// Decompiled by Procyon v0.6.0
// 

package com.applovin.exoplayer2.e.c;

import java.util.List;
import java.util.Map;
import java.util.Date;
import java.util.HashMap;
import java.util.ArrayList;
import androidx.annotation.Nullable;
import com.applovin.exoplayer2.l.y;
import com.applovin.exoplayer2.e.x;
import com.applovin.exoplayer2.e.g;

final class c extends d
{
    private long b;
    private long[] c;
    private long[] d;
    
    public c() {
        super(new g());
        this.b = -9223372036854775807L;
        this.c = new long[0];
        this.d = new long[0];
    }
    
    @Nullable
    private static Object a(final y y, final int n) {
        if (n == 0) {
            return d(y);
        }
        if (n == 1) {
            return c(y);
        }
        if (n == 2) {
            return e(y);
        }
        if (n == 3) {
            return g(y);
        }
        if (n == 8) {
            return h(y);
        }
        if (n == 10) {
            return f(y);
        }
        if (n != 11) {
            return null;
        }
        return i(y);
    }
    
    private static int b(final y y) {
        return y.h();
    }
    
    private static Boolean c(final y y) {
        final int h = y.h();
        boolean b = true;
        if (h != 1) {
            b = false;
        }
        return b;
    }
    
    private static Double d(final y y) {
        return Double.longBitsToDouble(y.s());
    }
    
    private static String e(final y y) {
        final int i = y.i();
        final int c = y.c();
        y.e(i);
        return new String(y.d(), c, i);
    }
    
    private static ArrayList<Object> f(final y y) {
        final int w = y.w();
        final ArrayList list = new ArrayList<Object>(w);
        for (int i = 0; i < w; ++i) {
            final Object a = a(y, b(y));
            if (a != null) {
                list.add(a);
            }
        }
        return (ArrayList<Object>)list;
    }
    
    private static HashMap<String, Object> g(final y y) {
        final HashMap hashMap = new HashMap();
        while (true) {
            final String e = e(y);
            final int b = b(y);
            if (b == 9) {
                break;
            }
            final Object a = a(y, b);
            if (a == null) {
                continue;
            }
            hashMap.put(e, a);
        }
        return hashMap;
    }
    
    private static HashMap<String, Object> h(final y y) {
        final int w = y.w();
        final HashMap hashMap = new HashMap<String, Object>(w);
        for (int i = 0; i < w; ++i) {
            final String e = e(y);
            final Object a = a(y, b(y));
            if (a != null) {
                hashMap.put(e, a);
            }
        }
        return (HashMap<String, Object>)hashMap;
    }
    
    private static Date i(final y y) {
        final Date date = new Date((long)(double)d(y));
        y.e(2);
        return date;
    }
    
    public long a() {
        return this.b;
    }
    
    @Override
    protected boolean a(final y y) {
        return true;
    }
    
    @Override
    protected boolean a(final y y, final long n) {
        if (b(y) != 2) {
            return false;
        }
        if (!"onMetaData".equals(e(y))) {
            return false;
        }
        if (b(y) != 8) {
            return false;
        }
        final HashMap<String, Object> h = h(y);
        final Object value = h.get("duration");
        if (value instanceof Double) {
            final double doubleValue = (double)value;
            if (doubleValue > 0.0) {
                this.b = (long)(doubleValue * 1000000.0);
            }
        }
        final Object value2 = h.get("keyframes");
        if (value2 instanceof Map) {
            final Map map = (Map)value2;
            final Object value3 = map.get("filepositions");
            final Object value4 = map.get("times");
            if (value3 instanceof List && value4 instanceof List) {
                final List list = (List)value3;
                final List list2 = (List)value4;
                final int size = list2.size();
                this.c = new long[size];
                this.d = new long[size];
                for (int i = 0; i < size; ++i) {
                    final Object value5 = list.get(i);
                    final Object value6 = list2.get(i);
                    if (!(value6 instanceof Double) || !(value5 instanceof Double)) {
                        this.c = new long[0];
                        this.d = new long[0];
                        break;
                    }
                    this.c[i] = (long)((double)value6 * 1000000.0);
                    this.d[i] = ((Double)value5).longValue();
                }
            }
        }
        return false;
    }
    
    public long[] b() {
        return this.c;
    }
    
    public long[] c() {
        return this.d;
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.applovin.exoplayer2.e.c;

import com.applovin.exoplayer2.ai;
import java.util.List;
import com.applovin.exoplayer2.m.a;
import com.applovin.exoplayer2.l.v;
import com.applovin.exoplayer2.e.x;
import com.applovin.exoplayer2.l.y;

final class e extends d
{
    private final y b;
    private final y c;
    private int d;
    private boolean e;
    private boolean f;
    private int g;
    
    public e(final x x) {
        super(x);
        this.b = new y(v.a);
        this.c = new y(4);
    }
    
    @Override
    protected boolean a(final y y) throws a {
        final int h = y.h();
        final int g = h >> 4 & 0xF;
        final int i = h & 0xF;
        if (i == 7) {
            this.g = g;
            return g != 5;
        }
        final StringBuilder sb = new StringBuilder();
        sb.append("Video format not supported: ");
        sb.append(i);
        throw new a(sb.toString());
    }
    
    @Override
    protected boolean a(final y y, final long n) throws ai {
        final int h = y.h();
        final long n2 = y.n();
        if (h == 0 && !this.e) {
            final y y2 = new y(new byte[y.a()]);
            y.a(y2.d(), 0, y.a());
            final com.applovin.exoplayer2.m.a a = com.applovin.exoplayer2.m.a.a(y2);
            this.d = a.b;
            super.a.a(new com.applovin.exoplayer2.v.a().f("video/avc").d(a.f).g(a.c).h(a.d).b(a.e).a(a.a).a());
            this.e = true;
            return false;
        }
        if (h != 1 || !this.e) {
            return false;
        }
        int n3;
        if (this.g == 1) {
            n3 = 1;
        }
        else {
            n3 = 0;
        }
        if (!this.f && n3 == 0) {
            return false;
        }
        final byte[] d = this.c.d();
        d[0] = 0;
        d[2] = (d[1] = 0);
        final int d2 = this.d;
        int n4 = 0;
        while (y.a() > 0) {
            y.a(this.c.d(), 4 - d2, this.d);
            this.c.d(0);
            final int w = this.c.w();
            this.b.d(0);
            super.a.a(this.b, 4);
            super.a.a(y, w);
            n4 = n4 + 4 + w;
        }
        super.a.a(n + n2 * 1000L, n3, n4, 0, null);
        return this.f = true;
    }
}

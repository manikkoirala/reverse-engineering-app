// 
// Decompiled by Procyon v0.6.0
// 

package com.applovin.exoplayer2.e;

import com.applovin.exoplayer2.l.q;
import com.applovin.exoplayer2.l.y;

public final class b
{
    private static int a(final y y) {
        int n = 0;
        while (y.a() != 0) {
            final int h = y.h();
            n += h;
            if (h != 255) {
                return n;
            }
        }
        return -1;
    }
    
    public static void a(final long n, final y y, final x[] array) {
        while (true) {
            final int a = y.a();
            final boolean b = true;
            if (a <= 1) {
                break;
            }
            final int a2 = a(y);
            final int a3 = a(y);
            final int n2 = y.c() + a3;
            int b2;
            if (a3 != -1 && a3 <= y.a()) {
                b2 = n2;
                if (a2 == 4) {
                    b2 = n2;
                    if (a3 >= 8) {
                        final int h = y.h();
                        final int i = y.i();
                        int q;
                        if (i == 49) {
                            q = y.q();
                        }
                        else {
                            q = 0;
                        }
                        final int h2 = y.h();
                        if (i == 47) {
                            y.e(1);
                        }
                        boolean b3 = h == 181 && (i == 49 || i == 47) && h2 == 3;
                        if (i == 49) {
                            b3 &= (q == 1195456820 && b);
                        }
                        b2 = n2;
                        if (b3) {
                            b(n, y, array);
                            b2 = n2;
                        }
                    }
                }
            }
            else {
                q.c("CeaUtil", "Skipping remainder of malformed SEI NAL unit.");
                b2 = y.b();
            }
            y.d(b2);
        }
    }
    
    public static void b(final long n, final y y, final x[] array) {
        final int h = y.h();
        final int n2 = 0;
        if ((h & 0x40) == 0x0) {
            return;
        }
        y.e(1);
        final int n3 = (h & 0x1F) * 3;
        final int c = y.c();
        for (int length = array.length, i = n2; i < length; ++i) {
            final x x = array[i];
            y.d(c);
            x.a(y, n3);
            if (n != -9223372036854775807L) {
                x.a(n, 1, n3, 0, null);
            }
        }
    }
}

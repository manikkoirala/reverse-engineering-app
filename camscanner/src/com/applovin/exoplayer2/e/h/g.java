// 
// Decompiled by Procyon v0.6.0
// 

package com.applovin.exoplayer2.e.h;

import com.applovin.exoplayer2.l.a;
import com.applovin.exoplayer2.b.s;
import com.applovin.exoplayer2.v;
import java.util.Arrays;
import com.applovin.exoplayer2.l.y;

final class g extends h
{
    private static final byte[] a;
    private boolean b;
    
    static {
        a = new byte[] { 79, 112, 117, 115, 72, 101, 97, 100 };
    }
    
    private long a(final byte[] array) {
        final int n = array[0] & 0xFF;
        final int n2 = n & 0x3;
        int n3;
        if (n2 != 0) {
            n3 = 2;
            if (n2 != 1) {
                n3 = n3;
                if (n2 != 2) {
                    n3 = (array[1] & 0x3F);
                }
            }
        }
        else {
            n3 = 1;
        }
        final int n4 = n >> 3;
        final int n5 = n4 & 0x3;
        int n6;
        if (n4 >= 16) {
            n6 = 2500 << n5;
        }
        else if (n4 >= 12) {
            n6 = 10000 << (n5 & 0x1);
        }
        else if (n5 == 3) {
            n6 = 60000;
        }
        else {
            n6 = 10000 << n5;
        }
        return n3 * (long)n6;
    }
    
    public static boolean a(final y y) {
        final int a = y.a();
        final byte[] a2 = g.a;
        if (a < a2.length) {
            return false;
        }
        final byte[] a3 = new byte[a2.length];
        y.a(a3, 0, a2.length);
        return Arrays.equals(a3, a2);
    }
    
    @Override
    protected void a(final boolean b) {
        super.a(b);
        if (b) {
            this.b = false;
        }
    }
    
    @Override
    protected boolean a(final y y, final long n, final a a) {
        final boolean b = this.b;
        boolean b2 = true;
        if (!b) {
            final byte[] copy = Arrays.copyOf(y.d(), y.b());
            a.a = new v.a().f("audio/opus").k(s.a(copy)).l(48000).a(s.b(copy)).a();
            return this.b = true;
        }
        a.b((Object)a.a);
        if (y.q() != 1332770163) {
            b2 = false;
        }
        y.d(0);
        return b2;
    }
    
    @Override
    protected long b(final y y) {
        return this.b(this.a(y.d()));
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.applovin.exoplayer2.e.h;

import java.io.IOException;
import java.util.List;
import com.applovin.exoplayer2.v;
import java.util.ArrayList;
import com.applovin.exoplayer2.l.a;
import com.applovin.exoplayer2.ai;
import java.util.Arrays;
import com.applovin.exoplayer2.l.y;
import androidx.annotation.VisibleForTesting;
import com.applovin.exoplayer2.e.z;
import androidx.annotation.Nullable;

final class i extends h
{
    @Nullable
    private a a;
    private int b;
    private boolean c;
    @Nullable
    private z.d d;
    @Nullable
    private z.b e;
    
    @VisibleForTesting
    static int a(final byte b, final int n, final int n2) {
        return b >> n2 & 255 >>> 8 - n;
    }
    
    private static int a(final byte b, final a a) {
        int n;
        if (!a.d[a(b, a.e, 1)].a) {
            n = a.a.g;
        }
        else {
            n = a.a.h;
        }
        return n;
    }
    
    @VisibleForTesting
    static void a(final y y, final long n) {
        if (y.e() < y.b() + 4) {
            y.a(Arrays.copyOf(y.d(), y.b() + 4));
        }
        else {
            y.c(y.b() + 4);
        }
        final byte[] d = y.d();
        d[y.b() - 4] = (byte)(n & 0xFFL);
        d[y.b() - 3] = (byte)(n >>> 8 & 0xFFL);
        d[y.b() - 2] = (byte)(n >>> 16 & 0xFFL);
        d[y.b() - 1] = (byte)(n >>> 24 & 0xFFL);
    }
    
    public static boolean a(final y y) {
        try {
            return z.a(1, y, true);
        }
        catch (final ai ai) {
            return false;
        }
    }
    
    @Override
    protected void a(final boolean b) {
        super.a(b);
        if (b) {
            this.a = null;
            this.d = null;
            this.e = null;
        }
        this.b = 0;
        this.c = false;
    }
    
    @Override
    protected boolean a(final y y, final long n, final h.a a) throws IOException {
        if (this.a != null) {
            a.b((Object)a.a);
            return false;
        }
        final a c = this.c(y);
        if ((this.a = c) == null) {
            return true;
        }
        final z.d a2 = c.a;
        final ArrayList list = new ArrayList();
        list.add(a2.j);
        list.add(c.c);
        a.a = new v.a().f("audio/vorbis").d(a2.e).e(a2.d).k(a2.b).l(a2.c).a(list).a();
        return true;
    }
    
    @Override
    protected long b(final y y) {
        final byte[] d = y.d();
        int n = 0;
        if ((d[0] & 0x1) == 0x1) {
            return -1L;
        }
        final int a = a(y.d()[0], (a)com.applovin.exoplayer2.l.a.a((Object)this.a));
        if (this.c) {
            n = (this.b + a) / 4;
        }
        final long n2 = n;
        a(y, n2);
        this.c = true;
        this.b = a;
        return n2;
    }
    
    @Nullable
    @VisibleForTesting
    a c(final y y) throws IOException {
        final z.d d = this.d;
        if (d == null) {
            this.d = z.a(y);
            return null;
        }
        final z.b e = this.e;
        if (e == null) {
            this.e = z.b(y);
            return null;
        }
        final byte[] array = new byte[y.b()];
        System.arraycopy(y.d(), 0, array, 0, y.b());
        final z.c[] a = z.a(y, d.b);
        return new a(d, e, array, a, z.a(a.length - 1));
    }
    
    @Override
    protected void c(final long n) {
        super.c(n);
        int g = 0;
        this.c = (n != 0L);
        final z.d d = this.d;
        if (d != null) {
            g = d.g;
        }
        this.b = g;
    }
    
    static final class a
    {
        public final z.d a;
        public final z.b b;
        public final byte[] c;
        public final z.c[] d;
        public final int e;
        
        public a(final z.d a, final z.b b, final byte[] c, final z.c[] d, final int e) {
            this.a = a;
            this.b = b;
            this.c = c;
            this.d = d;
            this.e = e;
        }
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.applovin.exoplayer2.e.h;

import com.applovin.exoplayer2.ai;
import com.applovin.exoplayer2.e.k;
import com.applovin.exoplayer2.l.a;
import java.io.IOException;
import com.applovin.exoplayer2.e.i;
import com.applovin.exoplayer2.l.y;

final class e
{
    public int a;
    public int b;
    public long c;
    public long d;
    public long e;
    public long f;
    public int g;
    public int h;
    public int i;
    public final int[] j;
    private final y k;
    
    e() {
        this.j = new int[255];
        this.k = new y(255);
    }
    
    public void a() {
        this.a = 0;
        this.b = 0;
        this.c = 0L;
        this.d = 0L;
        this.e = 0L;
        this.f = 0L;
        this.g = 0;
        this.h = 0;
        this.i = 0;
    }
    
    public boolean a(final i i) throws IOException {
        return this.a(i, -1L);
    }
    
    public boolean a(final i i, final long n) throws IOException {
        com.applovin.exoplayer2.l.a.a(i.c() == i.b());
        this.k.a(4);
        long n2;
        while (true) {
            n2 = lcmp(n, -1L);
            if ((n2 != 0 && i.c() + 4L >= n) || !com.applovin.exoplayer2.e.k.a(i, this.k.d(), 0, 4, true)) {
                break;
            }
            this.k.d(0);
            if (this.k.o() == 1332176723L) {
                i.a();
                return true;
            }
            i.b(1);
        }
        while ((n2 == 0 || i.c() < n) && i.a(1) != -1) {}
        return false;
    }
    
    public boolean a(final i i, final boolean b) throws IOException {
        this.a();
        this.k.a(27);
        final byte[] d = this.k.d();
        int j = 0;
        if (!com.applovin.exoplayer2.e.k.a(i, d, 0, 27, b) || this.k.o() != 1332176723L) {
            return false;
        }
        if ((this.a = this.k.h()) != 0) {
            if (b) {
                return false;
            }
            throw ai.a("unsupported bit stream revision");
        }
        else {
            this.b = this.k.h();
            this.c = this.k.t();
            this.d = this.k.p();
            this.e = this.k.p();
            this.f = this.k.p();
            final int h = this.k.h();
            this.g = h;
            this.h = h + 27;
            this.k.a(h);
            if (!com.applovin.exoplayer2.e.k.a(i, this.k.d(), 0, this.g, b)) {
                return false;
            }
            while (j < this.g) {
                this.j[j] = this.k.h();
                this.i += this.j[j];
                ++j;
            }
            return true;
        }
    }
}

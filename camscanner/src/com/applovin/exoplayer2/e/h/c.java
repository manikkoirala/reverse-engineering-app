// 
// Decompiled by Procyon v0.6.0
// 

package com.applovin.exoplayer2.e.h;

import com.applovin.exoplayer2.e.x;
import com.applovin.exoplayer2.ai;
import com.applovin.exoplayer2.l.a;
import com.applovin.exoplayer2.e.u;
import java.io.IOException;
import com.applovin.exoplayer2.e.i;
import com.applovin.exoplayer2.l.y;
import com.applovin.exoplayer2.e.j;
import com.applovin.exoplayer2.e.l;
import com.applovin.exoplayer2.e.h;

public class c implements h
{
    public static final l a;
    private j b;
    private com.applovin.exoplayer2.e.h.h c;
    private boolean d;
    
    static {
        a = new \u3007080();
    }
    
    private static y a(final y y) {
        y.d(0);
        return y;
    }
    
    private boolean b(final i i) throws IOException {
        final e e = new e();
        if (e.a(i, true)) {
            if ((e.b & 0x2) == 0x2) {
                final int min = Math.min(e.i, 8);
                final y y = new y(min);
                i.d(y.d(), 0, min);
                if (com.applovin.exoplayer2.e.h.b.a(a(y))) {
                    this.c = new b();
                }
                else if (i.a(a(y))) {
                    this.c = new com.applovin.exoplayer2.e.h.i();
                }
                else {
                    if (!g.a(a(y))) {
                        return false;
                    }
                    this.c = new g();
                }
                return true;
            }
        }
        return false;
    }
    
    @Override
    public int a(final i i, final u u) throws IOException {
        com.applovin.exoplayer2.l.a.a((Object)this.b);
        if (this.c == null) {
            if (!this.b(i)) {
                throw ai.b("Failed to determine bitstream type", null);
            }
            i.a();
        }
        if (!this.d) {
            final x a = this.b.a(0, 1);
            this.b.a();
            this.c.a(this.b, a);
            this.d = true;
        }
        return this.c.a(i, u);
    }
    
    @Override
    public void a(final long n, final long n2) {
        final com.applovin.exoplayer2.e.h.h c = this.c;
        if (c != null) {
            c.a(n, n2);
        }
    }
    
    @Override
    public void a(final j b) {
        this.b = b;
    }
    
    @Override
    public boolean a(final i i) throws IOException {
        try {
            return this.b(i);
        }
        catch (final ai ai) {
            return false;
        }
    }
    
    @Override
    public void c() {
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.applovin.exoplayer2.e.h;

import com.applovin.exoplayer2.e.w;
import com.applovin.exoplayer2.e.v;
import androidx.annotation.VisibleForTesting;
import java.io.EOFException;
import com.applovin.exoplayer2.e.k;
import androidx.annotation.Nullable;
import com.applovin.exoplayer2.l.ai;
import java.io.IOException;
import com.applovin.exoplayer2.e.i;

final class a implements f
{
    private final e a;
    private final long b;
    private final long c;
    private final h d;
    private int e;
    private long f;
    private long g;
    private long h;
    private long i;
    private long j;
    private long k;
    private long l;
    
    public a(final h d, final long b, final long c, final long n, final long f, final boolean b2) {
        com.applovin.exoplayer2.l.a.a(b >= 0L && c > b);
        this.d = d;
        this.b = b;
        this.c = c;
        if (n != c - b && !b2) {
            this.e = 0;
        }
        else {
            this.f = f;
            this.e = 4;
        }
        this.a = new e();
    }
    
    private long c(final i i) throws IOException {
        if (this.i == this.j) {
            return -1L;
        }
        final long c = i.c();
        if (!this.a.a(i, this.j)) {
            final long j = this.i;
            if (j != c) {
                return j;
            }
            throw new IOException("No ogg page can be found.");
        }
        else {
            this.a.a(i, false);
            i.a();
            final long h = this.h;
            final e a = this.a;
            final long c2 = a.c;
            final long n = h - c2;
            final int n2 = a.h + a.i;
            if (0L <= n && n < 72000L) {
                return -1L;
            }
            final long n3 = lcmp(n, 0L);
            if (n3 < 0) {
                this.j = c;
                this.l = c2;
            }
            else {
                this.i = i.c() + n2;
                this.k = this.a.c;
            }
            final long k = this.j;
            final long l = this.i;
            if (k - l < 100000L) {
                return this.j = l;
            }
            final long n4 = n2;
            long n5;
            if (n3 <= 0) {
                n5 = 2L;
            }
            else {
                n5 = 1L;
            }
            final long c3 = i.c();
            final long m = this.j;
            final long i2 = this.i;
            return ai.a(c3 - n4 * n5 + n * (m - i2) / (this.l - this.k), i2, m - 1L);
        }
    }
    
    private void d(final i i) throws IOException {
        while (true) {
            this.a.a(i);
            this.a.a(i, false);
            final e a = this.a;
            if (a.c > this.h) {
                break;
            }
            i.b(a.h + a.i);
            this.i = i.c();
            this.k = this.a.c;
        }
        i.a();
    }
    
    @Override
    public long a(final i i) throws IOException {
        final int e = this.e;
        if (e != 0) {
            if (e != 1) {
                if (e != 2) {
                    if (e != 3) {
                        if (e == 4) {
                            return -1L;
                        }
                        throw new IllegalStateException();
                    }
                }
                else {
                    final long c = this.c(i);
                    if (c != -1L) {
                        return c;
                    }
                    this.e = 3;
                }
                this.d(i);
                this.e = 4;
                return -(this.k + 2L);
            }
        }
        else {
            final long c2 = i.c();
            this.g = c2;
            this.e = 1;
            final long n = this.c - 65307L;
            if (n > c2) {
                return n;
            }
        }
        this.f = this.b(i);
        this.e = 4;
        return this.g;
    }
    
    @Nullable
    public a a() {
        final long f = this.f;
        a a = null;
        if (f != 0L) {
            a = new a();
        }
        return a;
    }
    
    @Override
    public void a(final long n) {
        this.h = ai.a(n, 0L, this.f - 1L);
        this.e = 2;
        this.i = this.b;
        this.j = this.c;
        this.k = 0L;
        this.l = this.f;
    }
    
    @VisibleForTesting
    long b(final i i) throws IOException {
        this.a.a();
        if (this.a.a(i)) {
            this.a.a(i, false);
            final e a = this.a;
            i.b(a.h + a.i);
            long n = this.a.c;
            while (true) {
                final e a2 = this.a;
                if ((a2.b & 0x4) == 0x4 || !a2.a(i) || i.c() >= this.c || !this.a.a(i, true)) {
                    break;
                }
                final e a3 = this.a;
                if (!com.applovin.exoplayer2.e.k.a(i, a3.h + a3.i)) {
                    break;
                }
                n = this.a.c;
            }
            return n;
        }
        throw new EOFException();
    }
    
    private final class a implements v
    {
        final com.applovin.exoplayer2.e.h.a a;
        
        private a(final com.applovin.exoplayer2.e.h.a a) {
            this.a = a;
        }
        
        @Override
        public v.a a(final long n) {
            return new v.a(new w(n, ai.a(this.a.b + this.a.d.b(n) * (this.a.c - this.a.b) / this.a.f - 30000L, this.a.b, this.a.c - 1L)));
        }
        
        @Override
        public boolean a() {
            return true;
        }
        
        @Override
        public long b() {
            return this.a.d.a(this.a.f);
        }
    }
}

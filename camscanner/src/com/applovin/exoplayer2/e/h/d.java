// 
// Decompiled by Procyon v0.6.0
// 

package com.applovin.exoplayer2.e.h;

import java.util.Arrays;
import java.io.IOException;
import com.applovin.exoplayer2.e.k;
import com.applovin.exoplayer2.l.a;
import com.applovin.exoplayer2.e.i;
import com.applovin.exoplayer2.l.y;

final class d
{
    private final e a;
    private final y b;
    private int c;
    private int d;
    private boolean e;
    
    d() {
        this.a = new e();
        this.b = new y(new byte[65025], 0);
        this.c = -1;
    }
    
    private int a(final int n) {
        int n2 = 0;
        this.d = 0;
        int i;
        int n3;
        do {
            final int d = this.d;
            final e a = this.a;
            n3 = n2;
            if (n + d >= a.g) {
                break;
            }
            final int[] j = a.j;
            this.d = d + 1;
            i = j[d + n];
            n3 = (n2 += i);
        } while (i == 255);
        return n3;
    }
    
    public void a() {
        this.a.a();
        this.b.a(0);
        this.c = -1;
        this.e = false;
    }
    
    public boolean a(final i i) throws IOException {
        com.applovin.exoplayer2.l.a.b(i != null);
        if (this.e) {
            this.e = false;
            this.b.a(0);
        }
        while (!this.e) {
            if (this.c < 0) {
                if (!this.a.a(i) || !this.a.a(i, true)) {
                    return false;
                }
                final e a = this.a;
                int h = a.h;
                int c;
                if ((a.b & 0x1) == 0x1 && this.b.b() == 0) {
                    h += this.a(0);
                    c = this.d + 0;
                }
                else {
                    c = 0;
                }
                if (!k.a(i, h)) {
                    return false;
                }
                this.c = c;
            }
            final int a2 = this.a(this.c);
            final int n = this.c + this.d;
            if (a2 > 0) {
                final y b = this.b;
                b.b(b.b() + a2);
                if (!k.b(i, this.b.d(), this.b.b(), a2)) {
                    return false;
                }
                final y b2 = this.b;
                b2.c(b2.b() + a2);
                this.e = (this.a.j[n - 1] != 255);
            }
            int c2;
            if ((c2 = n) == this.a.g) {
                c2 = -1;
            }
            this.c = c2;
        }
        return true;
    }
    
    public e b() {
        return this.a;
    }
    
    public y c() {
        return this.b;
    }
    
    public void d() {
        if (this.b.d().length == 65025) {
            return;
        }
        final y b = this.b;
        b.a(Arrays.copyOf(b.d(), Math.max(65025, this.b.b())), this.b.b());
    }
}

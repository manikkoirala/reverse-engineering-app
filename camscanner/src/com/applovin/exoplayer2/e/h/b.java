// 
// Decompiled by Procyon v0.6.0
// 

package com.applovin.exoplayer2.e.h;

import com.applovin.exoplayer2.e.o;
import com.applovin.exoplayer2.e.v;
import com.applovin.exoplayer2.l.ai;
import com.applovin.exoplayer2.e.i;
import com.applovin.exoplayer2.e.n;
import com.applovin.exoplayer2.g.a;
import java.util.Arrays;
import com.applovin.exoplayer2.e.m;
import com.applovin.exoplayer2.l.y;
import androidx.annotation.Nullable;
import com.applovin.exoplayer2.e.p;

final class b extends h
{
    @Nullable
    private p a;
    @Nullable
    private a b;
    
    public static boolean a(final y y) {
        return y.a() >= 5 && y.h() == 127 && y.o() == 1179402563L;
    }
    
    private static boolean a(final byte[] array) {
        boolean b = false;
        if (array[0] == -1) {
            b = true;
        }
        return b;
    }
    
    private int c(final y y) {
        final int n = (y.d()[2] & 0xFF) >> 4;
        if (n == 6 || n == 7) {
            y.e(4);
            y.D();
        }
        final int a = com.applovin.exoplayer2.e.m.a(y, n);
        y.d(0);
        return a;
    }
    
    @Override
    protected void a(final boolean b) {
        super.a(b);
        if (b) {
            this.a = null;
            this.b = null;
        }
    }
    
    @Override
    protected boolean a(final y y, final long n, final h.a a) {
        final byte[] d = y.d();
        final p a2 = this.a;
        if (a2 == null) {
            final p a3 = new p(d, 17);
            this.a = a3;
            a.a = a3.a(Arrays.copyOfRange(d, 9, y.b()), null);
            return true;
        }
        if ((d[0] & 0x7F) == 0x3) {
            final p.a a4 = n.a(y);
            final p a5 = a2.a(a4);
            this.a = a5;
            this.b = new a(a5, a4);
            return true;
        }
        if (a(d)) {
            final a b = this.b;
            if (b != null) {
                b.b(n);
                a.b = this.b;
            }
            com.applovin.exoplayer2.l.a.b((Object)a.a);
            return false;
        }
        return true;
    }
    
    @Override
    protected long b(final y y) {
        if (!a(y.d())) {
            return -1L;
        }
        return this.c(y);
    }
    
    private static final class a implements f
    {
        private p a;
        private p.a b;
        private long c;
        private long d;
        
        public a(final p a, final p.a b) {
            this.a = a;
            this.b = b;
            this.c = -1L;
            this.d = -1L;
        }
        
        @Override
        public long a(final i i) {
            final long d = this.d;
            if (d >= 0L) {
                final long n = -(d + 2L);
                this.d = -1L;
                return n;
            }
            return -1L;
        }
        
        @Override
        public void a(final long n) {
            final long[] a = this.b.a;
            this.d = a[ai.a(a, n, true, true)];
        }
        
        @Override
        public v b() {
            com.applovin.exoplayer2.l.a.b(this.c != -1L);
            return new o(this.a, this.c);
        }
        
        public void b(final long c) {
            this.c = c;
        }
    }
}

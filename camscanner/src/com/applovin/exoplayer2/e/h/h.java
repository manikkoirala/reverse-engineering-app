// 
// Decompiled by Procyon v0.6.0
// 

package com.applovin.exoplayer2.e.h;

import com.applovin.exoplayer2.l.y;
import com.applovin.exoplayer2.e.u;
import com.applovin.exoplayer2.v;
import java.io.IOException;
import com.applovin.exoplayer2.e.i;
import com.applovin.exoplayer2.l.ai;
import com.applovin.exoplayer2.l.a;
import com.applovin.exoplayer2.e.j;
import com.applovin.exoplayer2.e.x;

abstract class h
{
    private final d a;
    private x b;
    private j c;
    private f d;
    private long e;
    private long f;
    private long g;
    private int h;
    private int i;
    private a j;
    private long k;
    private boolean l;
    private boolean m;
    
    public h() {
        this.a = new d();
        this.j = new a();
    }
    
    private void a() {
        com.applovin.exoplayer2.l.a.a((Object)this.b);
        ai.a((Object)this.c);
    }
    
    private boolean a(final i i) throws IOException {
        while (this.a.a(i)) {
            this.k = i.c() - this.f;
            if (!this.a(this.a.c(), this.f, this.j)) {
                return true;
            }
            this.f = i.c();
        }
        this.h = 3;
        return false;
    }
    
    private int b(final i i) throws IOException {
        if (!this.a(i)) {
            return -1;
        }
        final v a = this.j.a;
        this.i = a.z;
        if (!this.m) {
            this.b.a(a);
            this.m = true;
        }
        final f b = this.j.b;
        if (b != null) {
            this.d = b;
        }
        else if (i.d() == -1L) {
            this.d = new b();
        }
        else {
            final e b2 = this.a.b();
            this.d = new com.applovin.exoplayer2.e.h.a(this, this.f, i.d(), b2.h + b2.i, b2.c, (b2.b & 0x4) != 0x0);
        }
        this.h = 2;
        this.a.d();
        return 0;
    }
    
    private int b(final i i, final u u) throws IOException {
        final long a = this.d.a(i);
        if (a >= 0L) {
            u.a = a;
            return 1;
        }
        if (a < -1L) {
            this.c(-(a + 2L));
        }
        if (!this.l) {
            this.c.a((com.applovin.exoplayer2.e.v)com.applovin.exoplayer2.l.a.a((Object)this.d.b()));
            this.l = true;
        }
        if (this.k <= 0L && !this.a.a(i)) {
            this.h = 3;
            return -1;
        }
        this.k = 0L;
        final y c = this.a.c();
        final long b = this.b(c);
        if (b >= 0L) {
            final long g = this.g;
            if (g + b >= this.e) {
                final long a2 = this.a(g);
                this.b.a(c, c.b());
                this.b.a(a2, 1, c.b(), 0, null);
                this.e = -1L;
            }
        }
        this.g += b;
        return 0;
    }
    
    final int a(final i i, final u u) throws IOException {
        this.a();
        final int h = this.h;
        if (h == 0) {
            return this.b(i);
        }
        if (h == 1) {
            i.b((int)this.f);
            this.h = 2;
            return 0;
        }
        if (h == 2) {
            ai.a((Object)this.d);
            return this.b(i, u);
        }
        if (h == 3) {
            return -1;
        }
        throw new IllegalStateException();
    }
    
    protected long a(final long n) {
        return n * 1000000L / this.i;
    }
    
    final void a(final long n, final long n2) {
        this.a.a();
        if (n == 0L) {
            this.a(this.l ^ true);
        }
        else if (this.h != 0) {
            this.e = this.b(n2);
            ((f)ai.a((Object)this.d)).a(this.e);
            this.h = 2;
        }
    }
    
    void a(final j c, final x b) {
        this.c = c;
        this.b = b;
        this.a(true);
    }
    
    protected void a(final boolean b) {
        if (b) {
            this.j = new a();
            this.f = 0L;
            this.h = 0;
        }
        else {
            this.h = 1;
        }
        this.e = -1L;
        this.g = 0L;
    }
    
    protected abstract boolean a(final y p0, final long p1, final a p2) throws IOException;
    
    protected long b(final long n) {
        return this.i * n / 1000000L;
    }
    
    protected abstract long b(final y p0);
    
    protected void c(final long g) {
        this.g = g;
    }
    
    static class a
    {
        v a;
        f b;
    }
    
    private static final class b implements f
    {
        @Override
        public long a(final i i) {
            return -1L;
        }
        
        @Override
        public void a(final long n) {
        }
        
        @Override
        public com.applovin.exoplayer2.e.v b() {
            return new com.applovin.exoplayer2.e.v.b(-9223372036854775807L);
        }
    }
}

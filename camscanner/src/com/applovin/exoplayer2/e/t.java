// 
// Decompiled by Procyon v0.6.0
// 

package com.applovin.exoplayer2.e;

import com.applovin.exoplayer2.l.ai;
import com.applovin.exoplayer2.l.a;

public final class t implements v
{
    private final long[] a;
    private final long[] b;
    private final long c;
    private final boolean d;
    
    public t(final long[] a, final long[] b, final long c) {
        com.applovin.exoplayer2.l.a.a(a.length == b.length);
        final int length = b.length;
        final boolean d = length > 0;
        this.d = d;
        if (d && b[0] > 0L) {
            final int n = length + 1;
            final long[] a2 = new long[n];
            this.a = a2;
            final long[] b2 = new long[n];
            this.b = b2;
            System.arraycopy(a, 0, a2, 1, length);
            System.arraycopy(b, 0, b2, 1, length);
        }
        else {
            this.a = a;
            this.b = b;
        }
        this.c = c;
    }
    
    @Override
    public a a(final long n) {
        if (!this.d) {
            return new a(w.a);
        }
        int a = ai.a(this.b, n, true, true);
        final w w = new w(this.b[a], this.a[a]);
        if (w.b != n && a != this.b.length - 1) {
            final long[] b = this.b;
            ++a;
            return new a(w, new w(b[a], this.a[a]));
        }
        return new a(w);
    }
    
    @Override
    public boolean a() {
        return this.d;
    }
    
    @Override
    public long b() {
        return this.c;
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.applovin.exoplayer2.e;

import com.applovin.exoplayer2.l.ai;
import com.applovin.exoplayer2.l.a;

public final class o implements v
{
    private final p a;
    private final long b;
    
    public o(final p a, final long b) {
        this.a = a;
        this.b = b;
    }
    
    private w a(final long n, final long n2) {
        return new w(n * 1000000L / this.a.e, this.b + n2);
    }
    
    @Override
    public a a(final long n) {
        com.applovin.exoplayer2.l.a.a((Object)this.a.k);
        final p a = this.a;
        final p.a k = a.k;
        final long[] a2 = k.a;
        final long[] b = k.b;
        int a3 = ai.a(a2, a.a(n), true, false);
        long n2 = 0L;
        long n3;
        if (a3 == -1) {
            n3 = 0L;
        }
        else {
            n3 = a2[a3];
        }
        if (a3 != -1) {
            n2 = b[a3];
        }
        final w a4 = this.a(n3, n2);
        if (a4.b != n && a3 != a2.length - 1) {
            ++a3;
            return new a(a4, this.a(a2[a3], b[a3]));
        }
        return new a(a4);
    }
    
    @Override
    public boolean a() {
        return true;
    }
    
    @Override
    public long b() {
        return this.a.a();
    }
}

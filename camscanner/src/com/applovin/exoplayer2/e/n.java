// 
// Decompiled by Procyon v0.6.0
// 

package com.applovin.exoplayer2.e;

import com.applovin.exoplayer2.common.base.Charsets;
import com.applovin.exoplayer2.ai;
import java.util.List;
import java.util.Collections;
import com.applovin.exoplayer2.l.x;
import androidx.annotation.Nullable;
import com.applovin.exoplayer2.g.e.g;
import com.applovin.exoplayer2.g.a;
import java.util.Arrays;
import java.io.IOException;
import com.applovin.exoplayer2.l.y;

public final class n
{
    private static p.a a(final i i, final int n) throws IOException {
        final y y = new y(n);
        i.b(y.d(), 0, n);
        return a(y);
    }
    
    public static p.a a(final y y) {
        y.e(1);
        final int m = y.m();
        final long n = y.c();
        final long n2 = m;
        final int n3 = m / 18;
        final long[] original = new long[n3];
        final long[] original2 = new long[n3];
        int n4 = 0;
        long[] copy;
        long[] copy2;
        while (true) {
            copy = original;
            copy2 = original2;
            if (n4 >= n3) {
                break;
            }
            final long s = y.s();
            if (s == -1L) {
                copy = Arrays.copyOf(original, n4);
                copy2 = Arrays.copyOf(original2, n4);
                break;
            }
            original[n4] = s;
            original2[n4] = y.s();
            y.e(2);
            ++n4;
        }
        y.e((int)(n + n2 - y.c()));
        return new p.a(copy, copy2);
    }
    
    @Nullable
    public static com.applovin.exoplayer2.g.a a(final i i, final boolean b) throws IOException {
        final com.applovin.exoplayer2.g.a a = null;
        Object a2;
        if (b) {
            a2 = null;
        }
        else {
            a2 = g.a;
        }
        final com.applovin.exoplayer2.g.a a3 = new s().a(i, (g.a)a2);
        com.applovin.exoplayer2.g.a a4 = a;
        if (a3 != null) {
            if (a3.a() == 0) {
                a4 = a;
            }
            else {
                a4 = a3;
            }
        }
        return a4;
    }
    
    public static boolean a(final i i) throws IOException {
        final y y = new y(4);
        final byte[] d = y.d();
        boolean b = false;
        i.d(d, 0, 4);
        if (y.o() == 1716281667L) {
            b = true;
        }
        return b;
    }
    
    public static boolean a(final i i, final a a) throws IOException {
        i.a();
        final x x = new x(new byte[4]);
        i.d(x.a, 0, 4);
        final boolean e = x.e();
        final int c = x.c(7);
        final int n = x.c(24) + 4;
        if (c == 0) {
            a.a = d(i);
        }
        else {
            final p a2 = a.a;
            if (a2 == null) {
                throw new IllegalArgumentException();
            }
            if (c == 3) {
                a.a = a2.a(a(i, n));
            }
            else if (c == 4) {
                a.a = a2.a(b(i, n));
            }
            else if (c == 6) {
                a.a = a2.b(Collections.singletonList(c(i, n)));
            }
            else {
                i.b(n);
            }
        }
        return e;
    }
    
    @Nullable
    public static com.applovin.exoplayer2.g.a b(final i i, final boolean b) throws IOException {
        i.a();
        final long b2 = i.b();
        final com.applovin.exoplayer2.g.a a = a(i, b);
        i.b((int)(i.b() - b2));
        return a;
    }
    
    private static List<String> b(final i i, final int n) throws IOException {
        final y y = new y(n);
        i.b(y.d(), 0, n);
        y.e(4);
        return Arrays.asList(z.a(y, false, false).b);
    }
    
    public static void b(final i i) throws IOException {
        final y y = new y(4);
        i.b(y.d(), 0, 4);
        if (y.o() == 1716281667L) {
            return;
        }
        throw ai.b("Failed to read FLAC stream marker.", null);
    }
    
    public static int c(final i i) throws IOException {
        i.a();
        final y y = new y(2);
        i.d(y.d(), 0, 2);
        final int j = y.i();
        if (j >> 2 == 16382) {
            i.a();
            return j;
        }
        i.a();
        throw ai.b("First frame does not start with sync code.", null);
    }
    
    private static com.applovin.exoplayer2.g.c.a c(final i i, int q) throws IOException {
        final y y = new y(q);
        i.b(y.d(), 0, q);
        y.e(4);
        final int q2 = y.q();
        final String a = y.a(y.q(), Charsets.US_ASCII);
        final String f = y.f(y.q());
        final int q3 = y.q();
        final int q4 = y.q();
        final int q5 = y.q();
        q = y.q();
        final int q6 = y.q();
        final byte[] array = new byte[q6];
        y.a(array, 0, q6);
        return new com.applovin.exoplayer2.g.c.a(q2, a, f, q3, q4, q5, q, array);
    }
    
    private static p d(final i i) throws IOException {
        final byte[] array = new byte[38];
        i.b(array, 0, 38);
        return new p(array, 4);
    }
    
    public static final class a
    {
        @Nullable
        public p a;
        
        public a(@Nullable final p a) {
            this.a = a;
        }
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.applovin.exoplayer2.e;

import com.applovin.exoplayer2.g.e.i;
import com.applovin.exoplayer2.g.e.e;
import com.applovin.exoplayer2.g.a;
import java.util.regex.Matcher;
import com.applovin.exoplayer2.l.ai;
import java.util.regex.Pattern;

public final class r
{
    private static final Pattern c;
    public int a;
    public int b;
    
    static {
        c = Pattern.compile("^ [0-9a-fA-F]{8} ([0-9a-fA-F]{8}) ([0-9a-fA-F]{8})");
    }
    
    public r() {
        this.a = -1;
        this.b = -1;
    }
    
    private boolean a(final String input) {
        final Matcher matcher = r.c.matcher(input);
        if (!matcher.find()) {
            return false;
        }
        try {
            final int int1 = Integer.parseInt((String)ai.a((Object)matcher.group(1)), 16);
            final int int2 = Integer.parseInt((String)ai.a((Object)matcher.group(2)), 16);
            if (int1 > 0 || int2 > 0) {
                this.a = int1;
                this.b = int2;
                return true;
            }
            return false;
        }
        catch (final NumberFormatException ex) {
            return false;
        }
    }
    
    public boolean a() {
        return this.a != -1 && this.b != -1;
    }
    
    public boolean a(int b) {
        final int a = b >> 12;
        b &= 0xFFF;
        if (a <= 0 && b <= 0) {
            return false;
        }
        this.a = a;
        this.b = b;
        return true;
    }
    
    public boolean a(final a a) {
        for (int i = 0; i < a.a(); ++i) {
            final a.a a2 = a.a(i);
            if (a2 instanceof e) {
                final e e = (e)a2;
                if ("iTunSMPB".equals(e.b) && this.a(e.c)) {
                    return true;
                }
            }
            else if (a2 instanceof i) {
                final i j = (i)a2;
                if ("com.apple.iTunes".equals(j.a) && "iTunSMPB".equals(j.b) && this.a(j.c)) {
                    return true;
                }
            }
        }
        return false;
    }
}

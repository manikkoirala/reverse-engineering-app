// 
// Decompiled by Procyon v0.6.0
// 

package com.applovin.exoplayer2.e;

import com.applovin.exoplayer2.l.q;
import java.util.Arrays;
import com.applovin.exoplayer2.ai;
import com.applovin.exoplayer2.l.y;

public final class z
{
    public static int a(int i) {
        int n = 0;
        while (i > 0) {
            ++n;
            i >>>= 1;
        }
        return n;
    }
    
    private static long a(final long n, final long n2) {
        return (long)Math.floor(Math.pow((double)n, 1.0 / n2));
    }
    
    public static b a(final y y, final boolean b, final boolean b2) throws ai {
        int n = 0;
        if (b) {
            a(3, y, false);
        }
        final String f = y.f((int)y.p());
        final int length = f.length();
        final long p3 = y.p();
        final String[] array = new String[(int)p3];
        int n2 = 11 + length + 4;
        while (n < p3) {
            final String f2 = y.f((int)y.p());
            array[n] = f2;
            n2 = n2 + 4 + f2.length();
            ++n;
        }
        if (b2 && (y.h() & 0x1) == 0x0) {
            throw ai.b("framing bit expected to be set", null);
        }
        return new b(f, array, n2 + 1);
    }
    
    public static d a(final y y) throws ai {
        boolean b = true;
        a(1, y, false);
        final int x = y.x();
        final int h = y.h();
        final int x2 = y.x();
        int r;
        if ((r = y.r()) <= 0) {
            r = -1;
        }
        int r2;
        if ((r2 = y.r()) <= 0) {
            r2 = -1;
        }
        int r3;
        if ((r3 = y.r()) <= 0) {
            r3 = -1;
        }
        final int h2 = y.h();
        final int n = (int)Math.pow(2.0, h2 & 0xF);
        final int n2 = (int)Math.pow(2.0, (h2 & 0xF0) >> 4);
        if ((y.h() & 0x1) <= 0) {
            b = false;
        }
        return new d(x, h, x2, r, r2, r3, n, n2, b, Arrays.copyOf(y.d(), y.b()));
    }
    
    private static void a(final int n, final com.applovin.exoplayer2.e.y y) throws ai {
        for (int a = y.a(6), i = 0; i < a + 1; ++i) {
            final int a2 = y.a(16);
            if (a2 != 0) {
                final StringBuilder sb = new StringBuilder();
                sb.append("mapping type other than 0 not supported: ");
                sb.append(a2);
                q.d("VorbisUtil", sb.toString());
            }
            else {
                int n2;
                if (y.a()) {
                    n2 = y.a(4) + 1;
                }
                else {
                    n2 = 1;
                }
                if (y.a()) {
                    for (int a3 = y.a(8), j = 0; j < a3 + 1; ++j) {
                        final int n3 = n - 1;
                        y.b(a(n3));
                        y.b(a(n3));
                    }
                }
                if (y.a(2) != 0) {
                    throw ai.b("to reserved bits must be zero after mapping coupling steps", null);
                }
                if (n2 > 1) {
                    for (int k = 0; k < n; ++k) {
                        y.b(4);
                    }
                }
                for (int l = 0; l < n2; ++l) {
                    y.b(8);
                    y.b(8);
                    y.b(8);
                }
            }
        }
    }
    
    public static boolean a(final int i, final y y, final boolean b) throws ai {
        if (y.a() < 7) {
            if (b) {
                return false;
            }
            final StringBuilder sb = new StringBuilder();
            sb.append("too short header: ");
            sb.append(y.a());
            throw ai.b(sb.toString(), null);
        }
        else if (y.h() != i) {
            if (b) {
                return false;
            }
            final StringBuilder sb2 = new StringBuilder();
            sb2.append("expected header type ");
            sb2.append(Integer.toHexString(i));
            throw ai.b(sb2.toString(), null);
        }
        else {
            if (y.h() == 118 && y.h() == 111 && y.h() == 114 && y.h() == 98 && y.h() == 105 && y.h() == 115) {
                return true;
            }
            if (b) {
                return false;
            }
            throw ai.b("expected characters 'vorbis'", null);
        }
    }
    
    private static c[] a(final com.applovin.exoplayer2.e.y y) {
        final int n = y.a(6) + 1;
        final c[] array = new c[n];
        for (int i = 0; i < n; ++i) {
            array[i] = new c(y.a(), y.a(16), y.a(16), y.a(8));
        }
        return array;
    }
    
    public static c[] a(final y y, final int n) throws ai {
        final int n2 = 0;
        a(5, y, false);
        final int h = y.h();
        final com.applovin.exoplayer2.e.y y2 = new com.applovin.exoplayer2.e.y(y.d());
        y2.b(y.c() * 8);
        for (int i = 0; i < h + 1; ++i) {
            d(y2);
        }
        for (int a = y2.a(6), j = n2; j < a + 1; ++j) {
            if (y2.a(16) != 0) {
                throw ai.b("placeholder of time domain transforms not zeroed out", null);
            }
        }
        c(y2);
        b(y2);
        a(n, y2);
        final c[] a2 = a(y2);
        if (y2.a()) {
            return a2;
        }
        throw ai.b("framing bit after modes not set as expected", null);
    }
    
    public static b b(final y y) throws ai {
        return a(y, true, true);
    }
    
    private static void b(final com.applovin.exoplayer2.e.y y) throws ai {
        for (int a = y.a(6), i = 0; i < a + 1; ++i) {
            if (y.a(16) > 2) {
                throw ai.b("residueType greater than 2 is not decodable", null);
            }
            y.b(24);
            y.b(24);
            y.b(24);
            final int n = y.a(6) + 1;
            y.b(8);
            final int[] array = new int[n];
            for (int j = 0; j < n; ++j) {
                final int a2 = y.a(3);
                int a3;
                if (y.a()) {
                    a3 = y.a(5);
                }
                else {
                    a3 = 0;
                }
                array[j] = a3 * 8 + a2;
            }
            for (int k = 0; k < n; ++k) {
                for (int l = 0; l < 8; ++l) {
                    if ((array[k] & 1 << l) != 0x0) {
                        y.b(8);
                    }
                }
            }
        }
    }
    
    private static void c(final com.applovin.exoplayer2.e.y y) throws ai {
        for (int a = y.a(6), i = 0; i < a + 1; ++i) {
            final int a2 = y.a(16);
            if (a2 != 0) {
                if (a2 != 1) {
                    final StringBuilder sb = new StringBuilder();
                    sb.append("floor type greater than 1 not decodable: ");
                    sb.append(a2);
                    throw ai.b(sb.toString(), null);
                }
                final int a3 = y.a(5);
                final int[] array = new int[a3];
                int n = -1;
                int n2;
                for (int j = 0; j < a3; ++j, n = n2) {
                    final int a4 = y.a(4);
                    if ((array[j] = a4) > (n2 = n)) {
                        n2 = a4;
                    }
                }
                final int[] array2 = new int[++n];
                for (int k = 0; k < n; ++k) {
                    array2[k] = y.a(3) + 1;
                    final int a5 = y.a(2);
                    if (a5 > 0) {
                        y.b(8);
                    }
                    for (int l = 0; l < 1 << a5; ++l) {
                        y.b(8);
                    }
                }
                y.b(2);
                final int a6 = y.a(4);
                int n3 = 0;
                int n4 = 0;
                int n5 = 0;
                while (n3 < a3) {
                    for (n4 += array2[array[n3]]; n5 < n4; ++n5) {
                        y.b(a6);
                    }
                    ++n3;
                }
            }
            else {
                y.b(8);
                y.b(16);
                y.b(16);
                y.b(6);
                y.b(8);
                for (int a7 = y.a(4), n6 = 0; n6 < a7 + 1; ++n6) {
                    y.b(8);
                }
            }
        }
    }
    
    private static a d(final com.applovin.exoplayer2.e.y y) throws ai {
        if (y.a(24) != 5653314) {
            final StringBuilder sb = new StringBuilder();
            sb.append("expected code book to start with [0x56, 0x43, 0x42] at ");
            sb.append(y.b());
            throw ai.b(sb.toString(), null);
        }
        final int a = y.a(16);
        final int a2 = y.a(24);
        final long[] array = new long[a2];
        final boolean a3 = y.a();
        long a4 = 0L;
        int i = 0;
        if (!a3) {
            final boolean a5 = y.a();
            while (i < a2) {
                if (a5) {
                    if (y.a()) {
                        array[i] = y.a(5) + 1;
                    }
                    else {
                        array[i] = 0L;
                    }
                }
                else {
                    array[i] = y.a(5) + 1;
                }
                ++i;
            }
        }
        else {
            int n = y.a(5) + 1;
            int j = 0;
            while (j < a2) {
                for (int a6 = y.a(a(a2 - j)), n2 = 0; n2 < a6 && j < a2; ++j, ++n2) {
                    array[j] = n;
                }
                ++n;
            }
        }
        final int a7 = y.a(4);
        if (a7 <= 2) {
            if (a7 == 1 || a7 == 2) {
                y.b(32);
                y.b(32);
                final int a8 = y.a(4);
                y.b(1);
                if (a7 == 1) {
                    if (a != 0) {
                        a4 = a(a2, a);
                    }
                }
                else {
                    a4 = a2 * (long)a;
                }
                y.b((int)(a4 * (a8 + 1)));
            }
            return new a(a, a2, array, a7, a3);
        }
        final StringBuilder sb2 = new StringBuilder();
        sb2.append("lookup type greater than 2 not decodable: ");
        sb2.append(a7);
        throw ai.b(sb2.toString(), null);
    }
    
    private static final class a
    {
        public final int a;
        public final int b;
        public final long[] c;
        public final int d;
        public final boolean e;
        
        public a(final int a, final int b, final long[] c, final int d, final boolean e) {
            this.a = a;
            this.b = b;
            this.c = c;
            this.d = d;
            this.e = e;
        }
    }
    
    public static final class b
    {
        public final String a;
        public final String[] b;
        public final int c;
        
        public b(final String a, final String[] b, final int c) {
            this.a = a;
            this.b = b;
            this.c = c;
        }
    }
    
    public static final class c
    {
        public final boolean a;
        public final int b;
        public final int c;
        public final int d;
        
        public c(final boolean a, final int b, final int c, final int d) {
            this.a = a;
            this.b = b;
            this.c = c;
            this.d = d;
        }
    }
    
    public static final class d
    {
        public final int a;
        public final int b;
        public final int c;
        public final int d;
        public final int e;
        public final int f;
        public final int g;
        public final int h;
        public final boolean i;
        public final byte[] j;
        
        public d(final int a, final int b, final int c, final int d, final int e, final int f, final int g, final int h, final boolean i, final byte[] j) {
            this.a = a;
            this.b = b;
            this.c = c;
            this.d = d;
            this.e = e;
            this.f = f;
            this.g = g;
            this.h = h;
            this.i = i;
            this.j = j;
        }
    }
}

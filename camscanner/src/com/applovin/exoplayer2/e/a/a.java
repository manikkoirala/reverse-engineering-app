// 
// Decompiled by Procyon v0.6.0
// 

package com.applovin.exoplayer2.e.a;

import com.applovin.exoplayer2.e.u;
import com.applovin.exoplayer2.k.g;
import java.io.EOFException;
import java.io.IOException;
import java.util.Arrays;
import com.applovin.exoplayer2.e.i;
import com.applovin.exoplayer2.e.d;
import com.applovin.exoplayer2.l.ai;
import oO80.\u3007O8o08O;
import com.applovin.exoplayer2.e.v;
import com.applovin.exoplayer2.e.x;
import com.applovin.exoplayer2.e.j;
import com.applovin.exoplayer2.e.l;
import com.applovin.exoplayer2.e.h;

public final class a implements h
{
    public static final l a;
    private static final int[] b;
    private static final int[] c;
    private static final byte[] d;
    private static final byte[] e;
    private static final int f;
    private final byte[] g;
    private final int h;
    private boolean i;
    private long j;
    private int k;
    private int l;
    private boolean m;
    private long n;
    private int o;
    private int p;
    private long q;
    private j r;
    private x s;
    private v t;
    private boolean u;
    
    static {
        a = new \u3007O8o08O();
        b = new int[] { 13, 14, 16, 18, 20, 21, 27, 32, 6, 7, 6, 6, 1, 1, 1, 1 };
        final int[] array;
        final int[] c2 = array = new int[16];
        array[0] = 18;
        array[1] = 24;
        array[2] = 33;
        array[3] = 37;
        array[4] = 41;
        array[5] = 47;
        array[6] = 51;
        array[7] = 59;
        array[8] = 61;
        array[9] = 6;
        array[11] = (array[10] = 1);
        array[13] = (array[12] = 1);
        array[15] = (array[14] = 1);
        c = c2;
        d = ai.c("#!AMR\n");
        e = ai.c("#!AMR-WB\n");
        f = c2[8];
    }
    
    public a() {
        this(0);
    }
    
    public a(final int n) {
        int h = n;
        if ((n & 0x2) != 0x0) {
            h = (n | 0x1);
        }
        this.h = h;
        this.g = new byte[1];
        this.o = -1;
    }
    
    private int a(int i) throws com.applovin.exoplayer2.ai {
        if (!this.b(i)) {
            final StringBuilder sb = new StringBuilder();
            sb.append("Illegal AMR ");
            String str;
            if (this.i) {
                str = "WB";
            }
            else {
                str = "NB";
            }
            sb.append(str);
            sb.append(" frame type ");
            sb.append(i);
            throw com.applovin.exoplayer2.ai.b(sb.toString(), null);
        }
        if (this.i) {
            i = com.applovin.exoplayer2.e.a.a.c[i];
        }
        else {
            i = com.applovin.exoplayer2.e.a.a.b[i];
        }
        return i;
    }
    
    private static int a(final int n, final long n2) {
        return (int)(n * 8 * 1000000L / n2);
    }
    
    private v a(final long n, final boolean b) {
        return new d(n, this.n, a(this.o, 20000L), this.o, b);
    }
    
    private void a() {
        if (!this.u) {
            this.u = true;
            final boolean i = this.i;
            String s;
            if (i) {
                s = "audio/amr-wb";
            }
            else {
                s = "audio/3gpp";
            }
            int n;
            if (i) {
                n = 16000;
            }
            else {
                n = 8000;
            }
            this.s.a(new com.applovin.exoplayer2.v.a().f(s).f(com.applovin.exoplayer2.e.a.a.f).k(1).l(n).a());
        }
    }
    
    private void a(final long n, final int n2) {
        if (this.m) {
            return;
        }
        final int h = this.h;
        if ((h & 0x1) != 0x0 && n != -1L) {
            final int o = this.o;
            if (o == -1 || o == this.k) {
                if (this.p >= 20 || n2 == -1) {
                    final v a = this.a(n, (h & 0x2) != 0x0);
                    this.t = a;
                    this.r.a(a);
                    this.m = true;
                }
                return;
            }
        }
        final v.b t = new v.b(-9223372036854775807L);
        this.t = t;
        this.r.a(t);
        this.m = true;
    }
    
    private static boolean a(final i i, final byte[] a2) throws IOException {
        i.a();
        final byte[] a3 = new byte[a2.length];
        i.d(a3, 0, a2.length);
        return Arrays.equals(a3, a2);
    }
    
    private void b() {
        com.applovin.exoplayer2.l.a.a((Object)this.s);
        ai.a((Object)this.r);
    }
    
    private boolean b(final int n) {
        return n >= 0 && n <= 15 && (this.c(n) || this.d(n));
    }
    
    private boolean b(final i i) throws IOException {
        final byte[] d = com.applovin.exoplayer2.e.a.a.d;
        if (a(i, d)) {
            this.i = false;
            i.b(d.length);
            return true;
        }
        final byte[] e = com.applovin.exoplayer2.e.a.a.e;
        if (a(i, e)) {
            this.i = true;
            i.b(e.length);
            return true;
        }
        return false;
    }
    
    private int c(final i i) throws IOException {
        if (this.l == 0) {
            try {
                final int d = this.d(i);
                this.k = d;
                this.l = d;
                if (this.o == -1) {
                    this.n = i.c();
                    this.o = this.k;
                }
                if (this.o == this.k) {
                    ++this.p;
                }
            }
            catch (final EOFException ex) {
                return -1;
            }
        }
        final int a = this.s.a((g)i, this.l, true);
        if (a == -1) {
            return -1;
        }
        if ((this.l -= a) > 0) {
            return 0;
        }
        this.s.a(this.q + this.j, 1, this.k, 0, null);
        this.j += 20000L;
        return 0;
    }
    
    private boolean c(final int n) {
        return this.i && (n < 10 || n > 13);
    }
    
    private int d(final i i) throws IOException {
        i.a();
        i.d(this.g, 0, 1);
        final byte j = this.g[0];
        if ((j & 0x83) <= 0) {
            return this.a(j >> 3 & 0xF);
        }
        final StringBuilder sb = new StringBuilder();
        sb.append("Invalid padding bits for frame header ");
        sb.append(j);
        throw com.applovin.exoplayer2.ai.b(sb.toString(), null);
    }
    
    private boolean d(final int n) {
        return !this.i && (n < 12 || n > 14);
    }
    
    @Override
    public int a(final i i, final u u) throws IOException {
        this.b();
        if (i.c() == 0L && !this.b(i)) {
            throw com.applovin.exoplayer2.ai.b("Could not find AMR header.", null);
        }
        this.a();
        final int c = this.c(i);
        this.a(i.d(), c);
        return c;
    }
    
    @Override
    public void a(final long n, final long n2) {
        this.j = 0L;
        this.k = 0;
        this.l = 0;
        if (n != 0L) {
            final v t = this.t;
            if (t instanceof d) {
                this.q = ((d)t).b(n);
                return;
            }
        }
        this.q = 0L;
    }
    
    @Override
    public void a(final j r) {
        this.r = r;
        this.s = r.a(0, 1);
        r.a();
    }
    
    @Override
    public boolean a(final i i) throws IOException {
        return this.b(i);
    }
    
    @Override
    public void c() {
    }
}

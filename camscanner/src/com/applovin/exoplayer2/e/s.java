// 
// Decompiled by Procyon v0.6.0
// 

package com.applovin.exoplayer2.e;

import java.io.IOException;
import java.io.EOFException;
import com.applovin.exoplayer2.g.a;
import androidx.annotation.Nullable;
import com.applovin.exoplayer2.g.e.g;
import com.applovin.exoplayer2.l.y;

public final class s
{
    private final y a;
    
    public s() {
        this.a = new y(10);
    }
    
    @Nullable
    public a a(final i i, @Nullable final g.a a) throws IOException {
        Object a2 = null;
        int n = 0;
        while (true) {
            try {
                while (true) {
                    i.d(this.a.d(), 0, 10);
                    this.a.d(0);
                    if (this.a.m() != 4801587) {
                        break;
                    }
                    this.a.e(3);
                    final int v = this.a.v();
                    final int n2 = v + 10;
                    if (a2 == null) {
                        a2 = new byte[n2];
                        System.arraycopy(this.a.d(), 0, a2, 0, 10);
                        i.d((byte[])a2, 10, v);
                        a2 = new g(a).a((byte[])a2, n2);
                    }
                    else {
                        i.c(v);
                    }
                    n += n2;
                }
                i.a();
                i.c(n);
                return (a)a2;
            }
            catch (final EOFException ex) {
                continue;
            }
            break;
        }
    }
}

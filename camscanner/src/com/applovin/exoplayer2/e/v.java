// 
// Decompiled by Procyon v0.6.0
// 

package com.applovin.exoplayer2.e;

import androidx.annotation.Nullable;
import com.applovin.exoplayer2.l.a;

public interface v
{
    a a(final long p0);
    
    boolean a();
    
    long b();
    
    public static final class a
    {
        public final w a;
        public final w b;
        
        public a(final w w) {
            this(w, w);
        }
        
        public a(final w w, final w w2) {
            this.a = (w)com.applovin.exoplayer2.l.a.b((Object)w);
            this.b = (w)com.applovin.exoplayer2.l.a.b((Object)w2);
        }
        
        @Override
        public boolean equals(@Nullable final Object o) {
            boolean b = true;
            if (this == o) {
                return true;
            }
            if (o != null && a.class == o.getClass()) {
                final a a = (a)o;
                if (!this.a.equals(a.a) || !this.b.equals(a.b)) {
                    b = false;
                }
                return b;
            }
            return false;
        }
        
        @Override
        public int hashCode() {
            return this.a.hashCode() * 31 + this.b.hashCode();
        }
        
        @Override
        public String toString() {
            final StringBuilder sb = new StringBuilder();
            sb.append("[");
            sb.append(this.a);
            String string;
            if (this.a.equals(this.b)) {
                string = "";
            }
            else {
                final StringBuilder sb2 = new StringBuilder();
                sb2.append(", ");
                sb2.append(this.b);
                string = sb2.toString();
            }
            sb.append(string);
            sb.append("]");
            return sb.toString();
        }
    }
    
    public static class b implements v
    {
        private final long a;
        private final a b;
        
        public b(final long n) {
            this(n, 0L);
        }
        
        public b(final long a, final long n) {
            this.a = a;
            w a2;
            if (n == 0L) {
                a2 = w.a;
            }
            else {
                a2 = new w(0L, n);
            }
            this.b = new a(a2);
        }
        
        @Override
        public a a(final long n) {
            return this.b;
        }
        
        @Override
        public boolean a() {
            return false;
        }
        
        @Override
        public long b() {
            return this.a;
        }
    }
}

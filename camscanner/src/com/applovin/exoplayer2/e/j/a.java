// 
// Decompiled by Procyon v0.6.0
// 

package com.applovin.exoplayer2.e.j;

import com.applovin.exoplayer2.k.g;
import com.applovin.exoplayer2.v;
import java.io.IOException;
import android.util.Pair;
import com.applovin.exoplayer2.b.y;
import com.applovin.exoplayer2.e.u;
import com.applovin.exoplayer2.e.i;
import com.applovin.exoplayer2.l.ai;
import com.applovin.exoplayer2.e.x;
import com.applovin.exoplayer2.e.j;
import com.applovin.exoplayer2.e.l;
import com.applovin.exoplayer2.e.h;

public final class a implements h
{
    public static final l a;
    private j b;
    private x c;
    private b d;
    private int e;
    private long f;
    
    static {
        a = new \u3007080();
    }
    
    public a() {
        this.e = -1;
        this.f = -1L;
    }
    
    private void a() {
        com.applovin.exoplayer2.l.a.a((Object)this.c);
        ai.a((Object)this.b);
    }
    
    @Override
    public int a(final i i, final u u) throws IOException {
        this.a();
        if (this.d == null) {
            final com.applovin.exoplayer2.e.j.b a = com.applovin.exoplayer2.e.j.c.a(i);
            if (a == null) {
                throw com.applovin.exoplayer2.ai.b("Unsupported or unrecognized wav header.", null);
            }
            final int a2 = a.a;
            if (a2 == 17) {
                this.d = (b)new a(this.b, this.c, a);
            }
            else if (a2 == 6) {
                this.d = (b)new c(this.b, this.c, a, "audio/g711-alaw", -1);
            }
            else if (a2 == 7) {
                this.d = (b)new c(this.b, this.c, a, "audio/g711-mlaw", -1);
            }
            else {
                final int a3 = y.a(a2, a.f);
                if (a3 == 0) {
                    final StringBuilder sb = new StringBuilder();
                    sb.append("Unsupported WAV format type: ");
                    sb.append(a.a);
                    throw com.applovin.exoplayer2.ai.a(sb.toString());
                }
                this.d = (b)new c(this.b, this.c, a, "audio/raw", a3);
            }
        }
        final int e = this.e;
        int n = -1;
        if (e == -1) {
            final Pair<Long, Long> b = com.applovin.exoplayer2.e.j.c.b(i);
            this.e = ((Long)b.first).intValue();
            final long longValue = (long)b.second;
            this.f = longValue;
            this.d.a(this.e, longValue);
        }
        else if (i.c() == 0L) {
            i.b(this.e);
        }
        com.applovin.exoplayer2.l.a.b(this.f != -1L);
        if (!this.d.a(i, this.f - i.c())) {
            n = 0;
        }
        return n;
    }
    
    @Override
    public void a(final long n, final long n2) {
        final b d = this.d;
        if (d != null) {
            d.a(n2);
        }
    }
    
    @Override
    public void a(final j b) {
        this.b = b;
        this.c = b.a(0, 1);
        b.a();
    }
    
    @Override
    public boolean a(final i i) throws IOException {
        return com.applovin.exoplayer2.e.j.c.a(i) != null;
    }
    
    @Override
    public void c() {
    }
    
    private static final class a implements b
    {
        private static final int[] a;
        private static final int[] b;
        private final j c;
        private final x d;
        private final com.applovin.exoplayer2.e.j.b e;
        private final int f;
        private final byte[] g;
        private final com.applovin.exoplayer2.l.y h;
        private final int i;
        private final v j;
        private int k;
        private long l;
        private int m;
        private long n;
        
        static {
            a = new int[] { -1, -1, -1, -1, 2, 4, 6, 8, -1, -1, -1, -1, 2, 4, 6, 8 };
            b = new int[] { 7, 8, 9, 10, 11, 12, 13, 14, 16, 17, 19, 21, 23, 25, 28, 31, 34, 37, 41, 45, 50, 55, 60, 66, 73, 80, 88, 97, 107, 118, 130, 143, 157, 173, 190, 209, 230, 253, 279, 307, 337, 371, 408, 449, 494, 544, 598, 658, 724, 796, 876, 963, 1060, 1166, 1282, 1411, 1552, 1707, 1878, 2066, 2272, 2499, 2749, 3024, 3327, 3660, 4026, 4428, 4871, 5358, 5894, 6484, 7132, 7845, 8630, 9493, 10442, 11487, 12635, 13899, 15289, 16818, 18500, 20350, 22385, 24623, 27086, 29794, 32767 };
        }
        
        public a(final j c, final x d, final com.applovin.exoplayer2.e.j.b e) throws com.applovin.exoplayer2.ai {
            this.c = c;
            this.d = d;
            this.e = e;
            final int max = Math.max(1, e.c / 10);
            this.i = max;
            final com.applovin.exoplayer2.l.y y = new com.applovin.exoplayer2.l.y(e.g);
            y.j();
            final int j = y.j();
            this.f = j;
            final int b = e.b;
            final int i = (e.e - b * 4) * 8 / (e.f * b) + 1;
            if (j == i) {
                final int a = ai.a(max, j);
                this.g = new byte[e.e * a];
                this.h = new com.applovin.exoplayer2.l.y(a * a(j, b));
                final int n = e.c * e.e * 8 / j;
                this.j = new v.a().f("audio/raw").d(n).e(n).f(a(max, b)).k(e.b).l(e.c).m(2).a();
                return;
            }
            final StringBuilder sb = new StringBuilder();
            sb.append("Expected frames per block: ");
            sb.append(i);
            sb.append("; got: ");
            sb.append(j);
            throw com.applovin.exoplayer2.ai.b(sb.toString(), null);
        }
        
        private static int a(final int n, final int n2) {
            return n * 2 * n2;
        }
        
        private void a(final int n) {
            final long l = this.l;
            final long d = ai.d(this.n, 1000000L, (long)this.e.c);
            final int c = this.c(n);
            this.d.a(l + d, 1, c, this.m - c, null);
            this.n += n;
            this.m -= c;
        }
        
        private void a(final byte[] array, int i, int a, final byte[] array2) {
            final com.applovin.exoplayer2.e.j.b e = this.e;
            final int e2 = e.e;
            final int b = e.b;
            final int n = i * e2 + a * 4;
            final int n2 = e2 / b;
            int a2 = (short)((array[n + 1] & 0xFF) << 8 | (array[n] & 0xFF));
            final int min = Math.min(array[n + 2] & 0xFF, 88);
            int n3 = a.b[min];
            int n4 = (i * this.f * b + a) * 2;
            array2[n4] = (byte)(a2 & 0xFF);
            array2[n4 + 1] = (byte)(a2 >> 8);
            i = 0;
            a = min;
            while (i < (n2 - 4) * 2) {
                final int n5 = array[i / 8 * b * 4 + (b * 4 + n) + i / 2 % 4] & 0xFF;
                int n6;
                if (i % 2 == 0) {
                    n6 = (n5 & 0xF);
                }
                else {
                    n6 = n5 >> 4;
                }
                int n7 = ((n6 & 0x7) * 2 + 1) * n3 >> 3;
                if ((n6 & 0x8) != 0x0) {
                    n7 = -n7;
                }
                a2 = ai.a(a2 + n7, -32768, 32767);
                n4 += b * 2;
                array2[n4] = (byte)(a2 & 0xFF);
                array2[n4 + 1] = (byte)(a2 >> 8);
                final int n8 = a.a[n6];
                final int[] b2 = a.b;
                a = ai.a(a + n8, 0, b2.length - 1);
                n3 = b2[a];
                ++i;
            }
        }
        
        private void a(final byte[] array, int c, final com.applovin.exoplayer2.l.y y) {
            for (int i = 0; i < c; ++i) {
                for (int j = 0; j < this.e.b; ++j) {
                    this.a(array, i, j, y.d());
                }
            }
            c = this.c(this.f * c);
            y.d(0);
            y.c(c);
        }
        
        private int b(final int n) {
            return n / (this.e.b * 2);
        }
        
        private int c(final int n) {
            return a(n, this.e.b);
        }
        
        @Override
        public void a(final int n, final long n2) {
            this.c.a(new d(this.e, this.f, n, n2));
            this.d.a(this.j);
        }
        
        @Override
        public void a(final long l) {
            this.k = 0;
            this.l = l;
            this.m = 0;
            this.n = 0L;
        }
        
        @Override
        public boolean a(final i i, final long b) throws IOException {
            final int n = ai.a(this.i - this.b(this.m), this.f) * this.e.e;
            while (true) {
                Label_0036: {
                    if (b == 0L) {
                        break Label_0036;
                    }
                    final boolean b2 = false;
                    while (!b2) {
                        final int k = this.k;
                        if (k >= n) {
                            break;
                        }
                        final int a = i.a(this.g, this.k, (int)Math.min(n - k, b));
                        if (a == -1) {
                            break Label_0036;
                        }
                        this.k += a;
                    }
                    final int n2 = this.k / this.e.e;
                    if (n2 > 0) {
                        this.a(this.g, n2, this.h);
                        this.k -= n2 * this.e.e;
                        final int b3 = this.h.b();
                        this.d.a(this.h, b3);
                        final int m = this.m + b3;
                        this.m = m;
                        final int b4 = this.b(m);
                        final int j = this.i;
                        if (b4 >= j) {
                            this.a(j);
                        }
                    }
                    if (b2) {
                        final int b5 = this.b(this.m);
                        if (b5 > 0) {
                            this.a(b5);
                        }
                    }
                    return b2;
                }
                final boolean b2 = true;
                continue;
            }
        }
    }
    
    private interface b
    {
        void a(final int p0, final long p1) throws com.applovin.exoplayer2.ai;
        
        void a(final long p0);
        
        boolean a(final i p0, final long p1) throws IOException;
    }
    
    private static final class c implements b
    {
        private final j a;
        private final x b;
        private final com.applovin.exoplayer2.e.j.b c;
        private final v d;
        private final int e;
        private long f;
        private int g;
        private long h;
        
        public c(final j a, final x b, final com.applovin.exoplayer2.e.j.b c, final String s, final int n) throws com.applovin.exoplayer2.ai {
            this.a = a;
            this.b = b;
            this.c = c;
            final int n2 = c.b * c.f / 8;
            if (c.e == n2) {
                final int c2 = c.c;
                final int n3 = c2 * n2 * 8;
                final int max = Math.max(n2, c2 * n2 / 10);
                this.e = max;
                this.d = new v.a().f(s).d(n3).e(n3).f(max).k(c.b).l(c.c).m(n).a();
                return;
            }
            final StringBuilder sb = new StringBuilder();
            sb.append("Expected block size: ");
            sb.append(n2);
            sb.append("; got: ");
            sb.append(c.e);
            throw com.applovin.exoplayer2.ai.b(sb.toString(), null);
        }
        
        @Override
        public void a(final int n, final long n2) {
            this.a.a(new d(this.c, 1, n, n2));
            this.b.a(this.d);
        }
        
        @Override
        public void a(final long f) {
            this.f = f;
            this.g = 0;
            this.h = 0L;
        }
        
        @Override
        public boolean a(final i i, long f) throws IOException {
            boolean b;
            long n;
            while (true) {
                b = true;
                n = lcmp(f, 0L);
                if (n <= 0) {
                    break;
                }
                final int g = this.g;
                final int e = this.e;
                if (g >= e) {
                    break;
                }
                final int a = this.b.a((g)i, (int)Math.min(e - g, f), true);
                if (a == -1) {
                    f = 0L;
                }
                else {
                    this.g += a;
                    f -= a;
                }
            }
            final com.applovin.exoplayer2.e.j.b c = this.c;
            final int e2 = c.e;
            final int n2 = this.g / e2;
            if (n2 > 0) {
                f = this.f;
                final long d = ai.d(this.h, 1000000L, (long)c.c);
                final int n3 = n2 * e2;
                final int g2 = this.g - n3;
                this.b.a(f + d, 1, n3, g2, null);
                this.h += n2;
                this.g = g2;
            }
            if (n > 0) {
                b = false;
            }
            return b;
        }
    }
}

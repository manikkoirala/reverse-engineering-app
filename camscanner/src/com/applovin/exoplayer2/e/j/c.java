// 
// Decompiled by Procyon v0.6.0
// 

package com.applovin.exoplayer2.e.j;

import android.util.Pair;
import androidx.annotation.Nullable;
import java.io.IOException;
import com.applovin.exoplayer2.l.ai;
import com.applovin.exoplayer2.l.q;
import com.applovin.exoplayer2.l.y;
import com.applovin.exoplayer2.l.a;
import com.applovin.exoplayer2.e.i;

final class c
{
    @Nullable
    public static b a(final i i) throws IOException {
        com.applovin.exoplayer2.l.a.b((Object)i);
        final y y = new y(16);
        if (a.a(i, y).a != 1380533830) {
            return null;
        }
        i.d(y.d(), 0, 4);
        y.d(0);
        final int q = y.q();
        if (q != 1463899717) {
            final StringBuilder sb = new StringBuilder();
            sb.append("Unsupported RIFF format: ");
            sb.append(q);
            com.applovin.exoplayer2.l.q.d("WavHeaderReader", sb.toString());
            return null;
        }
        a a;
        for (a = c.a.a(i, y); a.a != 1718449184; a = c.a.a(i, y)) {
            i.c((int)a.b);
        }
        com.applovin.exoplayer2.l.a.b(a.b >= 16L);
        i.d(y.d(), 0, 16);
        y.d(0);
        final int j = y.j();
        final int k = y.j();
        final int x = y.x();
        final int x2 = y.x();
        final int l = y.j();
        final int m = y.j();
        final int n = (int)a.b - 16;
        byte[] f;
        if (n > 0) {
            final byte[] array = new byte[n];
            i.d(array, 0, n);
            f = array;
        }
        else {
            f = ai.f;
        }
        return new b(j, k, x, x2, l, m, f);
    }
    
    public static Pair<Long, Long> b(final i i) throws IOException {
        com.applovin.exoplayer2.l.a.b((Object)i);
        i.a();
        final y y = new y(8);
        a a = c.a.a(i, y);
        while (true) {
            final int a2 = a.a;
            if (a2 == 1684108385) {
                i.b(8);
                final long c = i.c();
                final long lng = a.b + c;
                final long d = i.d();
                long l = lng;
                if (d != -1L) {
                    l = lng;
                    if (lng > d) {
                        final StringBuilder sb = new StringBuilder();
                        sb.append("Data exceeds input length: ");
                        sb.append(lng);
                        sb.append(", ");
                        sb.append(d);
                        q.c("WavHeaderReader", sb.toString());
                        l = d;
                    }
                }
                return (Pair<Long, Long>)Pair.create((Object)c, (Object)l);
            }
            if (a2 != 1380533830 && a2 != 1718449184) {
                final StringBuilder sb2 = new StringBuilder();
                sb2.append("Ignoring unknown WAV chunk: ");
                sb2.append(a.a);
                q.c("WavHeaderReader", sb2.toString());
            }
            long n = a.b + 8L;
            if (a.a == 1380533830) {
                n = 12L;
            }
            if (n > 2147483647L) {
                final StringBuilder sb3 = new StringBuilder();
                sb3.append("Chunk is too large (~2GB+) to skip; id: ");
                sb3.append(a.a);
                throw com.applovin.exoplayer2.ai.a(sb3.toString());
            }
            i.b((int)n);
            a = c.a.a(i, y);
        }
    }
    
    private static final class a
    {
        public final int a;
        public final long b;
        
        private a(final int a, final long b) {
            this.a = a;
            this.b = b;
        }
        
        public static a a(final i i, final y y) throws IOException {
            i.d(y.d(), 0, 8);
            y.d(0);
            return new a(y.q(), y.p());
        }
    }
}

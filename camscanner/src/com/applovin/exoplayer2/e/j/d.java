// 
// Decompiled by Procyon v0.6.0
// 

package com.applovin.exoplayer2.e.j;

import com.applovin.exoplayer2.e.w;
import com.applovin.exoplayer2.l.ai;
import com.applovin.exoplayer2.e.v;

final class d implements v
{
    private final com.applovin.exoplayer2.e.j.b a;
    private final int b;
    private final long c;
    private final long d;
    private final long e;
    
    public d(final com.applovin.exoplayer2.e.j.b a, final int b, long n, final long n2) {
        this.a = a;
        this.b = b;
        this.c = n;
        n = (n2 - n) / a.e;
        this.d = n;
        this.e = this.b(n);
    }
    
    private long b(final long n) {
        return ai.d(n * this.b, 1000000L, (long)this.a.c);
    }
    
    @Override
    public a a(long c) {
        final long a = ai.a(this.a.c * c / (this.b * 1000000L), 0L, this.d - 1L);
        final long c2 = this.c;
        final long n = this.a.e;
        final long b = this.b(a);
        final w w = new w(b, c2 + n * a);
        if (b < c && a != this.d - 1L) {
            final long n2 = a + 1L;
            c = this.c;
            return new a(w, new w(this.b(n2), c + this.a.e * n2));
        }
        return new a(w);
    }
    
    @Override
    public boolean a() {
        return true;
    }
    
    @Override
    public long b() {
        return this.e;
    }
}

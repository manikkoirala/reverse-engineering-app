// 
// Decompiled by Procyon v0.6.0
// 

package com.applovin.exoplayer2.e.g;

import com.applovin.exoplayer2.l.q;
import com.applovin.exoplayer2.l.a;
import com.applovin.exoplayer2.e.x;
import androidx.annotation.Nullable;

public final class l
{
    public final boolean a;
    @Nullable
    public final String b;
    public final x.a c;
    public final int d;
    @Nullable
    public final byte[] e;
    
    public l(final boolean a, @Nullable final String b, final int d, final byte[] array, final int n, final int n2, @Nullable final byte[] e) {
        boolean b2 = true;
        final boolean b3 = d == 0;
        if (e != null) {
            b2 = false;
        }
        com.applovin.exoplayer2.l.a.a(b2 ^ b3);
        this.a = a;
        this.b = b;
        this.d = d;
        this.e = e;
        this.c = new x.a(a(b), array, n, n2);
    }
    
    private static int a(@Nullable final String str) {
        if (str == null) {
            return 1;
        }
        final int hashCode = str.hashCode();
        int n = -1;
        switch (hashCode) {
            case 3049895: {
                if (!str.equals("cens")) {
                    break;
                }
                n = 3;
                break;
            }
            case 3049879: {
                if (!str.equals("cenc")) {
                    break;
                }
                n = 2;
                break;
            }
            case 3046671: {
                if (!str.equals("cbcs")) {
                    break;
                }
                n = 1;
                break;
            }
            case 3046605: {
                if (!str.equals("cbc1")) {
                    break;
                }
                n = 0;
                break;
            }
        }
        switch (n) {
            default: {
                final StringBuilder sb = new StringBuilder();
                sb.append("Unsupported protection scheme type '");
                sb.append(str);
                sb.append("'. Assuming AES-CTR crypto mode.");
                q.c("TrackEncryptionBox", sb.toString());
                return 1;
            }
            case 2:
            case 3: {
                return 1;
            }
            case 0:
            case 1: {
                return 2;
            }
        }
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.applovin.exoplayer2.e.g;

import com.applovin.exoplayer2.g.a;
import java.io.IOException;
import com.applovin.exoplayer2.e.u;
import com.applovin.exoplayer2.g.f.c;
import com.applovin.exoplayer2.l.y;
import com.applovin.exoplayer2.ai;
import java.util.ArrayList;
import java.util.List;
import com.applovin.exoplayer2.common.base.Splitter;

final class i
{
    private static final Splitter a;
    private static final Splitter b;
    private final List<a> c;
    private int d;
    private int e;
    
    static {
        a = Splitter.on(':');
        b = Splitter.on('*');
    }
    
    public i() {
        this.c = new ArrayList<a>();
        this.d = 0;
    }
    
    private static int a(final String s) throws ai {
        s.hashCode();
        final int hashCode = s.hashCode();
        int n = -1;
        switch (hashCode) {
            case 1760745220: {
                if (!s.equals("Super_SlowMotion_BGM")) {
                    break;
                }
                n = 4;
                break;
            }
            case -830665521: {
                if (!s.equals("Super_SlowMotion_Deflickering_On")) {
                    break;
                }
                n = 3;
                break;
            }
            case -1251387154: {
                if (!s.equals("Super_SlowMotion_Data")) {
                    break;
                }
                n = 2;
                break;
            }
            case -1332107749: {
                if (!s.equals("Super_SlowMotion_Edit_Data")) {
                    break;
                }
                n = 1;
                break;
            }
            case -1711564334: {
                if (!s.equals("SlowMotion_Data")) {
                    break;
                }
                n = 0;
                break;
            }
        }
        switch (n) {
            default: {
                throw ai.b("Invalid SEF name", null);
            }
            case 4: {
                return 2817;
            }
            case 3: {
                return 2820;
            }
            case 2: {
                return 2816;
            }
            case 1: {
                return 2819;
            }
            case 0: {
                return 2192;
            }
        }
    }
    
    private static c a(final y y, int i) throws ai {
        final ArrayList list = new ArrayList();
        final List<String> splitToList = i.b.splitToList(y.f(i));
        i = 0;
        while (i < splitToList.size()) {
            final List<String> splitToList2 = i.a.splitToList(splitToList.get(i));
            if (splitToList2.size() == 3) {
                try {
                    list.add(new c.a(Long.parseLong(splitToList2.get(0)), Long.parseLong(splitToList2.get(1)), 1 << Integer.parseInt(splitToList2.get(2)) - 1));
                    ++i;
                    continue;
                }
                catch (final NumberFormatException ex) {
                    throw ai.b(null, ex);
                }
            }
            throw ai.b(null, null);
        }
        return new c(list);
    }
    
    private void a(final com.applovin.exoplayer2.e.i i, final u u) throws IOException {
        final y y = new y(8);
        i.b(y.d(), 0, 8);
        this.e = y.r() + 8;
        if (y.q() != 1397048916) {
            u.a = 0L;
            return;
        }
        u.a = i.c() - (this.e - 12);
        this.d = 2;
    }
    
    private void a(final com.applovin.exoplayer2.e.i i, final List<com.applovin.exoplayer2.g.a.a> list) throws IOException {
        final long c = i.c();
        final int n = (int)(i.d() - i.c() - this.e);
        final y y = new y(n);
        final byte[] d = y.d();
        int j = 0;
        i.b(d, 0, n);
        while (j < this.c.size()) {
            final a a = this.c.get(j);
            y.d((int)(a.b - c));
            y.e(4);
            final int r = y.r();
            final int a2 = a(y.f(r));
            final int c2 = a.c;
            if (a2 != 2192) {
                if (a2 != 2816 && a2 != 2817 && a2 != 2819) {
                    if (a2 != 2820) {
                        throw new IllegalStateException();
                    }
                }
            }
            else {
                list.add(a(y, c2 - (r + 8)));
            }
            ++j;
        }
    }
    
    private void b(final com.applovin.exoplayer2.e.i i, final u u) throws IOException {
        final long d = i.d();
        final int n = this.e - 12 - 8;
        final y y = new y(n);
        i.b(y.d(), 0, n);
        for (int j = 0; j < n / 12; ++j) {
            y.e(2);
            final short l = y.l();
            if (l != 2192 && l != 2816 && l != 2817 && l != 2819 && l != 2820) {
                y.e(8);
            }
            else {
                this.c.add(new a(l, d - this.e - y.r(), y.r()));
            }
        }
        if (this.c.isEmpty()) {
            u.a = 0L;
            return;
        }
        this.d = 3;
        u.a = this.c.get(0).b;
    }
    
    public int a(final com.applovin.exoplayer2.e.i i, final u u, final List<com.applovin.exoplayer2.g.a.a> list) throws IOException {
        final int d = this.d;
        final long n = 0L;
        if (d != 0) {
            if (d != 1) {
                if (d != 2) {
                    if (d != 3) {
                        throw new IllegalStateException();
                    }
                    this.a(i, list);
                    u.a = 0L;
                }
                else {
                    this.b(i, u);
                }
            }
            else {
                this.a(i, u);
            }
        }
        else {
            final long d2 = i.d();
            long a = n;
            if (d2 != -1L) {
                if (d2 < 8L) {
                    a = n;
                }
                else {
                    a = d2 - 8L;
                }
            }
            u.a = a;
            this.d = 1;
        }
        return 1;
    }
    
    public void a() {
        this.c.clear();
        this.d = 0;
    }
    
    private static final class a
    {
        public final int a;
        public final long b;
        public final int c;
        
        public a(final int a, final long b, final int c) {
            this.a = a;
            this.b = b;
            this.c = c;
        }
    }
}

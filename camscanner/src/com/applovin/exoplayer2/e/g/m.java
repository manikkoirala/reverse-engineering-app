// 
// Decompiled by Procyon v0.6.0
// 

package com.applovin.exoplayer2.e.g;

import java.io.IOException;
import com.applovin.exoplayer2.e.i;
import com.applovin.exoplayer2.l.y;
import androidx.annotation.Nullable;

final class m
{
    public c a;
    public long b;
    public long c;
    public long d;
    public int e;
    public int f;
    public long[] g;
    public int[] h;
    public int[] i;
    public int[] j;
    public long[] k;
    public boolean[] l;
    public boolean m;
    public boolean[] n;
    @Nullable
    public l o;
    public final y p;
    public boolean q;
    public long r;
    public boolean s;
    
    public m() {
        this.g = new long[0];
        this.h = new int[0];
        this.i = new int[0];
        this.j = new int[0];
        this.k = new long[0];
        this.l = new boolean[0];
        this.n = new boolean[0];
        this.p = new y();
    }
    
    public void a() {
        this.e = 0;
        this.r = 0L;
        this.s = false;
        this.m = false;
        this.q = false;
        this.o = null;
    }
    
    public void a(final int n) {
        this.p.a(n);
        this.m = true;
        this.q = true;
    }
    
    public void a(int e, final int f) {
        this.e = e;
        this.f = f;
        if (this.h.length < e) {
            this.g = new long[e];
            this.h = new int[e];
        }
        if (this.i.length < f) {
            e = f * 125 / 100;
            this.i = new int[e];
            this.j = new int[e];
            this.k = new long[e];
            this.l = new boolean[e];
            this.n = new boolean[e];
        }
    }
    
    public void a(final i i) throws IOException {
        i.b(this.p.d(), 0, this.p.b());
        this.p.d(0);
        this.q = false;
    }
    
    public void a(final y y) {
        y.a(this.p.d(), 0, this.p.b());
        this.p.d(0);
        this.q = false;
    }
    
    public long b(final int n) {
        return this.k[n] + this.j[n];
    }
    
    public boolean c(final int n) {
        return this.m && this.n[n];
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.applovin.exoplayer2.e.g;

import androidx.annotation.Nullable;
import com.applovin.exoplayer2.v;

public final class k
{
    public final int a;
    public final int b;
    public final long c;
    public final long d;
    public final long e;
    public final v f;
    public final int g;
    @Nullable
    public final long[] h;
    @Nullable
    public final long[] i;
    public final int j;
    @Nullable
    private final l[] k;
    
    public k(final int a, final int b, final long c, final long d, final long e, final v f, final int g, @Nullable final l[] k, final int j, @Nullable final long[] h, @Nullable final long[] i) {
        this.a = a;
        this.b = b;
        this.c = c;
        this.d = d;
        this.e = e;
        this.f = f;
        this.g = g;
        this.k = k;
        this.j = j;
        this.h = h;
        this.i = i;
    }
    
    @Nullable
    public l a(final int n) {
        final l[] k = this.k;
        l l;
        if (k == null) {
            l = null;
        }
        else {
            l = k[n];
        }
        return l;
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.applovin.exoplayer2.e.g;

import com.applovin.exoplayer2.l.y;
import com.applovin.exoplayer2.l.q;
import java.nio.ByteBuffer;
import androidx.annotation.Nullable;
import java.util.UUID;

public final class h
{
    public static boolean a(final byte[] array) {
        return d(array) != null;
    }
    
    public static byte[] a(final UUID uuid, @Nullable final byte[] array) {
        return a(uuid, null, array);
    }
    
    public static byte[] a(UUID uuid, @Nullable final UUID[] array, @Nullable final byte[] src) {
        final int n = 0;
        int length;
        if (src != null) {
            length = src.length;
        }
        else {
            length = 0;
        }
        int capacity;
        final int n2 = capacity = length + 32;
        if (array != null) {
            capacity = n2 + (array.length * 16 + 4);
        }
        final ByteBuffer allocate = ByteBuffer.allocate(capacity);
        allocate.putInt(capacity);
        allocate.putInt(1886614376);
        int n3;
        if (array != null) {
            n3 = 16777216;
        }
        else {
            n3 = 0;
        }
        allocate.putInt(n3);
        allocate.putLong(uuid.getMostSignificantBits());
        allocate.putLong(uuid.getLeastSignificantBits());
        if (array != null) {
            allocate.putInt(array.length);
            for (int length2 = array.length, i = n; i < length2; ++i) {
                uuid = array[i];
                allocate.putLong(uuid.getMostSignificantBits());
                allocate.putLong(uuid.getLeastSignificantBits());
            }
        }
        if (src != null && src.length != 0) {
            allocate.putInt(src.length);
            allocate.put(src);
        }
        return allocate.array();
    }
    
    @Nullable
    public static byte[] a(final byte[] array, final UUID obj) {
        final a d = d(array);
        if (d == null) {
            return null;
        }
        if (!obj.equals(d.a)) {
            final StringBuilder sb = new StringBuilder();
            sb.append("UUID mismatch. Expected: ");
            sb.append(obj);
            sb.append(", got: ");
            sb.append(d.a);
            sb.append(".");
            q.c("PsshAtomUtil", sb.toString());
            return null;
        }
        return d.c;
    }
    
    @Nullable
    public static UUID b(final byte[] array) {
        final a d = d(array);
        if (d == null) {
            return null;
        }
        return d.a;
    }
    
    public static int c(final byte[] array) {
        final a d = d(array);
        if (d == null) {
            return -1;
        }
        return d.b;
    }
    
    @Nullable
    private static a d(final byte[] array) {
        final y y = new y(array);
        if (y.b() < 32) {
            return null;
        }
        y.d(0);
        if (y.q() != y.a() + 4) {
            return null;
        }
        if (y.q() != 1886614376) {
            return null;
        }
        final int a = com.applovin.exoplayer2.e.g.a.a(y.q());
        if (a > 1) {
            final StringBuilder sb = new StringBuilder();
            sb.append("Unsupported pssh version: ");
            sb.append(a);
            q.c("PsshAtomUtil", sb.toString());
            return null;
        }
        final UUID uuid = new UUID(y.s(), y.s());
        if (a == 1) {
            y.e(y.w() * 16);
        }
        final int w = y.w();
        if (w != y.a()) {
            return null;
        }
        final byte[] array2 = new byte[w];
        y.a(array2, 0, w);
        return new a(uuid, a, array2);
    }
    
    private static class a
    {
        private final UUID a;
        private final int b;
        private final byte[] c;
        
        public a(final UUID a, final int b, final byte[] c) {
            this.a = a;
            this.b = b;
            this.c = c;
        }
    }
}

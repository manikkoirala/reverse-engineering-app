// 
// Decompiled by Procyon v0.6.0
// 

package com.applovin.exoplayer2.e.g;

import com.applovin.exoplayer2.l.u;
import com.applovin.exoplayer2.m.c;
import com.applovin.exoplayer2.common.a.s;
import com.applovin.exoplayer2.common.base.Function;
import java.util.List;
import java.util.ArrayList;
import java.util.Arrays;
import com.applovin.exoplayer2.l.q;
import com.applovin.exoplayer2.e.r;
import com.applovin.exoplayer2.v;
import com.applovin.exoplayer2.d.e;
import androidx.annotation.Nullable;
import com.applovin.exoplayer2.e.k;
import com.applovin.exoplayer2.l.y;
import android.util.Pair;
import com.applovin.exoplayer2.l.ai;

final class b
{
    private static final byte[] a;
    
    static {
        a = ai.c("OpusHead");
    }
    
    private static int a(final int n) {
        if (n == 1936684398) {
            return 1;
        }
        if (n == 1986618469) {
            return 2;
        }
        if (n == 1952807028 || n == 1935832172 || n == 1937072756 || n == 1668047728) {
            return 3;
        }
        if (n == 1835365473) {
            return 5;
        }
        return -1;
    }
    
    public static Pair<com.applovin.exoplayer2.g.a, com.applovin.exoplayer2.g.a> a(final com.applovin.exoplayer2.e.g.a.b b) {
        final y b2 = b.b;
        b2.d(8);
        com.applovin.exoplayer2.g.a a = null;
        Object c = null;
        while (b2.a() >= 8) {
            final int c2 = b2.c();
            final int q = b2.q();
            final int q2 = b2.q();
            com.applovin.exoplayer2.g.a a2;
            if (q2 == 1835365473) {
                b2.d(c2);
                a2 = a(b2, c2 + q);
            }
            else {
                a2 = a;
                if (q2 == 1936553057) {
                    b2.d(c2);
                    c = c(b2, c2 + q);
                    a2 = a;
                }
            }
            b2.d(c2 + q);
            a = a2;
        }
        return (Pair<com.applovin.exoplayer2.g.a, com.applovin.exoplayer2.g.a>)Pair.create((Object)a, c);
    }
    
    @Nullable
    static Pair<Integer, l> a(final y y, final int n, final int n2) throws com.applovin.exoplayer2.ai {
        int n3 = n + 8;
        final boolean b = false;
        String s = null;
        Integer n4 = null;
        int n5 = -1;
        int n6 = 0;
        while (n3 - n < n2) {
            y.d(n3);
            final int q = y.q();
            final int q2 = y.q();
            Integer value;
            String f;
            if (q2 == 1718775137) {
                value = y.q();
                f = s;
            }
            else if (q2 == 1935894637) {
                y.e(4);
                f = y.f(4);
                value = n4;
            }
            else {
                f = s;
                value = n4;
                if (q2 == 1935894633) {
                    n5 = n3;
                    n6 = q;
                    value = n4;
                    f = s;
                }
            }
            n3 += q;
            s = f;
            n4 = value;
        }
        if (!"cenc".equals(s) && !"cbc1".equals(s) && !"cens".equals(s) && !"cbcs".equals(s)) {
            return null;
        }
        k.a(n4 != null, "frma atom is mandatory");
        k.a(n5 != -1, "schi atom is mandatory");
        final l a = a(y, n5, n6, s);
        boolean b2 = b;
        if (a != null) {
            b2 = true;
        }
        k.a(b2, "tenc atom is mandatory");
        return (Pair<Integer, l>)Pair.create((Object)n4, (Object)ai.a((Object)a));
    }
    
    private static c a(final y y, final int n, final int n2, final String s, @Nullable final com.applovin.exoplayer2.d.e e, final boolean b) throws com.applovin.exoplayer2.ai {
        y.d(12);
        final int q = y.q();
        final c c = new c(q);
        for (int i = 0; i < q; ++i) {
            final int c2 = y.c();
            final int q2 = y.q();
            k.a(q2 > 0, "childAtomSize must be positive");
            final int q3 = y.q();
            if (q3 != 1635148593 && q3 != 1635148595 && q3 != 1701733238 && q3 != 1831958048 && q3 != 1836070006 && q3 != 1752589105 && q3 != 1751479857 && q3 != 1932670515 && q3 != 1211250227 && q3 != 1987063864 && q3 != 1987063865 && q3 != 1635135537 && q3 != 1685479798 && q3 != 1685479729 && q3 != 1685481573 && q3 != 1685481521) {
                if (q3 != 1836069985 && q3 != 1701733217 && q3 != 1633889587 && q3 != 1700998451 && q3 != 1633889588 && q3 != 1685353315 && q3 != 1685353317 && q3 != 1685353320 && q3 != 1685353324 && q3 != 1685353336 && q3 != 1935764850 && q3 != 1935767394 && q3 != 1819304813 && q3 != 1936684916 && q3 != 1953984371 && q3 != 778924082 && q3 != 778924083 && q3 != 1835557169 && q3 != 1835560241 && q3 != 1634492771 && q3 != 1634492791 && q3 != 1970037111 && q3 != 1332770163 && q3 != 1716281667) {
                    if (q3 != 1414810956 && q3 != 1954034535 && q3 != 2004251764 && q3 != 1937010800 && q3 != 1664495672) {
                        if (q3 == 1835365492) {
                            a(y, q3, c2, n, c);
                        }
                        else if (q3 == 1667329389) {
                            c.b = new v.a().a(n).f("application/x-camera-motion").a();
                        }
                    }
                    else {
                        a(y, q3, c2, q2, n, s, c);
                    }
                }
                else {
                    a(y, q3, c2, q2, n, s, b, e, c, i);
                }
            }
            else {
                a(y, q3, c2, q2, n, n2, e, c, i);
            }
            y.d(c2 + q2);
        }
        return c;
    }
    
    @Nullable
    private static com.applovin.exoplayer2.e.g.k a(com.applovin.exoplayer2.e.g.a.a e, final com.applovin.exoplayer2.e.g.a.b b, long n, @Nullable final com.applovin.exoplayer2.d.e e2, final boolean b2, final boolean b3) throws com.applovin.exoplayer2.ai {
        final com.applovin.exoplayer2.e.g.a.a a = (com.applovin.exoplayer2.e.g.a.a)com.applovin.exoplayer2.l.a.b((Object)e.e(1835297121));
        final int a2 = a(d(((com.applovin.exoplayer2.e.g.a.b)com.applovin.exoplayer2.l.a.b((Object)a.d(1751411826))).b));
        final com.applovin.exoplayer2.e.g.k k = null;
        if (a2 == -1) {
            return null;
        }
        final f c = c(((com.applovin.exoplayer2.e.g.a.b)com.applovin.exoplayer2.l.a.b((Object)e.d(1953196132))).b);
        final long n2 = -9223372036854775807L;
        if (n == -9223372036854775807L) {
            n = c.b;
        }
        final long b4 = b(b.b);
        if (n == -9223372036854775807L) {
            n = n2;
        }
        else {
            n = ai.d(n, 1000000L, b4);
        }
        final com.applovin.exoplayer2.e.g.a.a a3 = (com.applovin.exoplayer2.e.g.a.a)com.applovin.exoplayer2.l.a.b((Object)((com.applovin.exoplayer2.e.g.a.a)com.applovin.exoplayer2.l.a.b((Object)a.e(1835626086))).e(1937007212));
        final Pair<Long, String> e3 = e(((com.applovin.exoplayer2.e.g.a.b)com.applovin.exoplayer2.l.a.b((Object)a.d(1835296868))).b);
        final c a4 = a(((com.applovin.exoplayer2.e.g.a.b)com.applovin.exoplayer2.l.a.b((Object)a3.d(1937011556))).b, c.a, c.c, (String)e3.second, e2, b3);
        long[] array = null;
        long[] array2 = null;
        Label_0262: {
            if (!b2) {
                e = e.e(1701082227);
                if (e != null) {
                    final Pair<long[], long[]> b5 = b(e);
                    if (b5 != null) {
                        array = (long[])b5.first;
                        array2 = (long[])b5.second;
                        break Label_0262;
                    }
                }
            }
            array = null;
            array2 = null;
        }
        com.applovin.exoplayer2.e.g.k i;
        if (a4.b == null) {
            i = k;
        }
        else {
            i = new com.applovin.exoplayer2.e.g.k(c.a, a2, (long)e3.first, b4, n, a4.b, a4.d, a4.a, a4.c, array, array2);
        }
        return i;
    }
    
    @Nullable
    private static l a(final y y, int a, int h, final String s) {
        int n = a + 8;
        while (true) {
            final byte[] array = null;
            if (n - a >= h) {
                return null;
            }
            y.d(n);
            final int q = y.q();
            if (y.q() == 1952804451) {
                a = a.a(y.q());
                y.e(1);
                if (a == 0) {
                    y.e(1);
                    h = 0;
                    a = 0;
                }
                else {
                    h = y.h();
                    a = (h & 0xF);
                    h = (h & 0xF0) >> 4;
                }
                final boolean b = y.h() == 1;
                final int h2 = y.h();
                final byte[] array2 = new byte[16];
                y.a(array2, 0, 16);
                byte[] array3 = array;
                if (b) {
                    array3 = array;
                    if (h2 == 0) {
                        final int h3 = y.h();
                        array3 = new byte[h3];
                        y.a(array3, 0, h3);
                    }
                }
                return new l(b, s, h2, array2, h, a, array3);
            }
            n += q;
        }
    }
    
    private static n a(final com.applovin.exoplayer2.e.g.k k, final com.applovin.exoplayer2.e.g.a.a a, final r r) throws com.applovin.exoplayer2.ai {
        final com.applovin.exoplayer2.e.g.a.b d = a.d(1937011578);
        b b;
        if (d != null) {
            b = new d(d, k.f);
        }
        else {
            final com.applovin.exoplayer2.e.g.a.b d2 = a.d(1937013298);
            if (d2 == null) {
                throw com.applovin.exoplayer2.ai.b("Track has no sample table size information", null);
            }
            b = new e(d2);
        }
        final int a2 = b.a();
        if (a2 == 0) {
            return new n(k, new long[0], new int[0], 0, new long[0], new int[0], 0L);
        }
        com.applovin.exoplayer2.e.g.a.b d3 = a.d(1937007471);
        boolean b2;
        if (d3 == null) {
            d3 = (com.applovin.exoplayer2.e.g.a.b)com.applovin.exoplayer2.l.a.b((Object)a.d(1668232756));
            b2 = true;
        }
        else {
            b2 = false;
        }
        final y b3 = d3.b;
        final y b4 = ((com.applovin.exoplayer2.e.g.a.b)com.applovin.exoplayer2.l.a.b((Object)a.d(1937011555))).b;
        final y b5 = ((com.applovin.exoplayer2.e.g.a.b)com.applovin.exoplayer2.l.a.b((Object)a.d(1937011827))).b;
        final com.applovin.exoplayer2.e.g.a.b d4 = a.d(1937011571);
        y b6;
        if (d4 != null) {
            b6 = d4.b;
        }
        else {
            b6 = null;
        }
        final com.applovin.exoplayer2.e.g.a.b d5 = a.d(1668576371);
        y b7;
        if (d5 != null) {
            b7 = d5.b;
        }
        else {
            b7 = null;
        }
        final a a3 = new a(b4, b3, b2);
        b5.d(12);
        int i = b5.w() - 1;
        final int w = b5.w();
        int w2 = b5.w();
        int w3;
        if (b7 != null) {
            b7.d(12);
            w3 = b7.w();
        }
        else {
            w3 = 0;
        }
        int w4 = 0;
        int n = 0;
        Label_0341: {
            if (b6 != null) {
                b6.d(12);
                w4 = b6.w();
                if (w4 > 0) {
                    n = b6.w() - 1;
                    break Label_0341;
                }
                b6 = null;
            }
            else {
                w4 = 0;
            }
            n = -1;
        }
        final int b8 = b.b();
        final String l = k.f.l;
        int[] original4 = null;
        long[] d7 = null;
        int c2 = 0;
        long f = 0L;
        int[] b11 = null;
        long[] a5 = null;
        int n14 = 0;
        Label_1291: {
            if (b8 == -1 || (!"audio/raw".equals(l) && !"audio/g711-mlaw".equals(l) && !"audio/g711-alaw".equals(l)) || i != 0 || w3 != 0 || w4 != 0) {
                final long[] original = new long[a2];
                final int[] original2 = new int[a2];
                final long[] original3 = new long[a2];
                original4 = new int[a2];
                int n2 = n;
                final int n3 = 0;
                int n4 = 0;
                final int n5 = 0;
                final int n6 = 0;
                int w5 = 0;
                long n7 = 0L;
                long d6 = 0L;
                final int n8 = w3;
                int n9 = w;
                int j = w4;
                int c = n6;
                int q = n5;
                int n10 = n8;
                while (true) {
                    int n15;
                    int n16;
                    int n17;
                    int n18;
                    int n20;
                    int n21;
                    int w6;
                    int q2;
                    long n23;
                    int n24;
                    int n25;
                    for (int n11 = a2, n12 = n3; n12 < n11; ++n12, d6 += n23, w2 = q2, n25 = n16, n2 = n20, j = n21, n4 = n18, n9 = w6, n10 = n15, q = n25, c = n24, w5 = n17) {
                        boolean a4 = true;
                        boolean b9;
                        while (true) {
                            b9 = a4;
                            if (c != 0) {
                                break;
                            }
                            a4 = a3.a();
                            if (!(b9 = a4)) {
                                break;
                            }
                            d6 = a3.d;
                            c = a3.c;
                        }
                        final int m = n9;
                        if (!b9) {
                            com.applovin.exoplayer2.l.q.c("AtomParsers", "Unexpected end of chunk data");
                            final long[] copy = Arrays.copyOf(original, n12);
                            final int[] copy2 = Arrays.copyOf(original2, n12);
                            final long[] copy3 = Arrays.copyOf(original3, n12);
                            original4 = Arrays.copyOf(original4, n12);
                            n11 = n12;
                            final int i2 = c;
                            final long[] array = copy;
                            d7 = copy3;
                            final long n13 = q;
                            boolean b10 = false;
                            Label_1081: {
                                if (b7 != null) {
                                    while (n10 > 0) {
                                        if (b7.w() != 0) {
                                            b10 = false;
                                            break Label_1081;
                                        }
                                        b7.q();
                                        --n10;
                                    }
                                }
                                b10 = true;
                            }
                            if (j != 0 || m != 0 || i2 != 0 || i != 0 || w5 != 0 || !b10) {
                                final StringBuilder sb = new StringBuilder();
                                sb.append("Inconsistent stbl box for track ");
                                sb.append(k.a);
                                sb.append(": remainingSynchronizationSamples ");
                                sb.append(j);
                                sb.append(", remainingSamplesAtTimestampDelta ");
                                sb.append(m);
                                sb.append(", remainingSamplesInChunk ");
                                sb.append(i2);
                                sb.append(", remainingTimestampDeltaChanges ");
                                sb.append(i);
                                sb.append(", remainingSamplesAtTimestampOffset ");
                                sb.append(w5);
                                String str;
                                if (!b10) {
                                    str = ", ctts invalid";
                                }
                                else {
                                    str = "";
                                }
                                sb.append(str);
                                com.applovin.exoplayer2.l.q.c("AtomParsers", sb.toString());
                            }
                            final long[] array2 = array;
                            c2 = n4;
                            f = n7 + n13;
                            b11 = copy2;
                            a5 = array2;
                            n14 = n11;
                            break Label_1291;
                        }
                        n15 = n10;
                        n16 = q;
                        n17 = w5;
                        if (b7 != null) {
                            while (w5 == 0 && n10 > 0) {
                                w5 = b7.w();
                                q = b7.q();
                                --n10;
                            }
                            n17 = w5 - 1;
                            n16 = q;
                            n15 = n10;
                        }
                        original[n12] = d6;
                        final int c3 = b.c();
                        if ((original2[n12] = c3) > (n18 = n4)) {
                            n18 = c3;
                        }
                        original3[n12] = n7 + n16;
                        int n19;
                        if (b6 == null) {
                            n19 = 1;
                        }
                        else {
                            n19 = 0;
                        }
                        original4[n12] = n19;
                        n20 = n2;
                        n21 = j;
                        if (n12 == n2) {
                            original4[n12] = 1;
                            final int n22 = j - 1;
                            n20 = n2;
                            if ((n21 = n22) > 0) {
                                n20 = ((y)com.applovin.exoplayer2.l.a.b((Object)b6)).w() - 1;
                                n21 = n22;
                            }
                        }
                        n7 += w2;
                        w6 = m - 1;
                        if (w6 == 0 && i > 0) {
                            w6 = b5.w();
                            q2 = b5.q();
                            --i;
                        }
                        else {
                            q2 = w2;
                        }
                        n23 = original2[n12];
                        n24 = c - 1;
                    }
                    final int m = n9;
                    final int i2 = c;
                    d7 = original3;
                    final int[] copy2 = original2;
                    final long[] array = original;
                    continue;
                }
            }
            final int a6 = a3.a;
            final long[] array3 = new long[a6];
            final int[] array4 = new int[a6];
            while (a3.a()) {
                final int b12 = a3.b;
                array3[b12] = a3.d;
                array4[b12] = a3.c;
            }
            final com.applovin.exoplayer2.e.g.d.a a7 = com.applovin.exoplayer2.e.g.d.a(b8, array3, array4, w2);
            a5 = a7.a;
            b11 = a7.b;
            c2 = a7.c;
            d7 = a7.d;
            original4 = a7.e;
            f = a7.f;
            n14 = a2;
        }
        final long d8 = ai.d(f, 1000000L, k.c);
        final long[] h = k.h;
        if (h == null) {
            ai.a(d7, 1000000L, k.c);
            return new n(k, a5, b11, c2, d7, original4, d8);
        }
        if (h.length == 1 && k.b == 1 && d7.length >= 2) {
            final long n26 = ((long[])com.applovin.exoplayer2.l.a.b((Object)k.i))[0];
            final long n27 = n26 + ai.d(k.h[0], k.c, k.d);
            if (a(d7, f, n26, n27)) {
                final long d9 = ai.d(n26 - d7[0], (long)k.f.z, k.c);
                final long d10 = ai.d(f - n27, (long)k.f.z, k.c);
                if ((d9 != 0L || d10 != 0L) && d9 <= 2147483647L && d10 <= 2147483647L) {
                    r.a = (int)d9;
                    r.b = (int)d10;
                    ai.a(d7, 1000000L, k.c);
                    return new n(k, a5, b11, c2, d7, original4, ai.d(k.h[0], 1000000L, k.d));
                }
            }
        }
        final long[] h2 = k.h;
        if (h2.length == 1 && h2[0] == 0L) {
            final long n28 = ((long[])com.applovin.exoplayer2.l.a.b((Object)k.i))[0];
            for (int n29 = 0; n29 < d7.length; ++n29) {
                d7[n29] = ai.d(d7[n29] - n28, 1000000L, k.c);
            }
            return new n(k, a5, b11, c2, d7, original4, ai.d(f - n28, 1000000L, k.c));
        }
        final boolean b13 = k.b == 1;
        final int[] array5 = new int[h2.length];
        final int[] array6 = new int[h2.length];
        final long[] array7 = (long[])com.applovin.exoplayer2.l.a.b((Object)k.i);
        int n30 = 0;
        int n31 = 0;
        int n32 = 0;
        int n33 = 0;
        final int[] array8 = b11;
        while (true) {
            final long[] h3 = k.h;
            if (n30 >= h3.length) {
                break;
            }
            final long n34 = array7[n30];
            int n37;
            int n38;
            if (n34 != -1L) {
                final long d11 = ai.d(h3[n30], k.c, k.d);
                array5[n30] = ai.a(d7, n34, true, true);
                array6[n30] = ai.b(d7, n34 + d11, b13, false);
                int n35;
                int n36;
                while (true) {
                    n35 = array5[n30];
                    n36 = array6[n30];
                    if (n35 >= n36 || (original4[n35] & 0x1) != 0x0) {
                        break;
                    }
                    array5[n30] = n35 + 1;
                }
                n32 += n36 - n35;
                n37 = (n31 | ((n33 != n35) ? 1 : 0));
                n38 = n36;
            }
            else {
                final int n39 = n31;
                n38 = n33;
                n37 = n39;
            }
            ++n30;
            final int n40 = n37;
            n33 = n38;
            n31 = n40;
        }
        final int n41 = 0;
        int n42 = 1;
        if (n32 == n14) {
            n42 = 0;
        }
        final int n43 = n31 | n42;
        long[] array9;
        if (n43 != 0) {
            array9 = new long[n32];
        }
        else {
            array9 = a5;
        }
        int[] array10;
        if (n43 != 0) {
            array10 = new int[n32];
        }
        else {
            array10 = array8;
        }
        if (n43 != 0) {
            c2 = 0;
        }
        int[] array11;
        if (n43 != 0) {
            array11 = new int[n32];
        }
        else {
            array11 = original4;
        }
        final long[] array12 = new long[n32];
        long n44 = 0L;
        int n45 = 0;
        final int[] array13 = array8;
        final int[] array14 = original4;
        final int[] array15 = array11;
        int n46 = n41;
        final int[] array16 = array6;
        while (n46 < k.h.length) {
            final long n47 = k.i[n46];
            int n48 = array5[n46];
            final int n49 = array16[n46];
            if (n43 != 0) {
                final int n50 = n49 - n48;
                System.arraycopy(a5, n48, array9, n45, n50);
                System.arraycopy(array13, n48, array10, n45, n50);
                System.arraycopy(array14, n48, array15, n45, n50);
            }
            while (n48 < n49) {
                array12[n45] = ai.d(n44, 1000000L, k.d) + ai.d(Math.max(0L, d7[n48] - n47), 1000000L, k.c);
                if (n43 != 0 && array10[n45] > c2) {
                    c2 = array13[n48];
                }
                ++n45;
                ++n48;
            }
            final long n51 = k.h[n46];
            ++n46;
            n44 += n51;
        }
        return new n(k, array9, array10, c2, array12, array15, ai.d(n44, 1000000L, k.d));
    }
    
    @Nullable
    public static com.applovin.exoplayer2.g.a a(final com.applovin.exoplayer2.e.g.a.a a) {
        final com.applovin.exoplayer2.e.g.a.b d = a.d(1751411826);
        final com.applovin.exoplayer2.e.g.a.b d2 = a.d(1801812339);
        final com.applovin.exoplayer2.e.g.a.b d3 = a.d(1768715124);
        com.applovin.exoplayer2.g.a a3;
        final com.applovin.exoplayer2.g.a a2 = a3 = null;
        if (d != null) {
            a3 = a2;
            if (d2 != null) {
                a3 = a2;
                if (d3 != null) {
                    if (d(d.b) != 1835299937) {
                        a3 = a2;
                    }
                    else {
                        final y b = d2.b;
                        b.d(12);
                        final int q = b.q();
                        final String[] array = new String[q];
                        for (int i = 0; i < q; ++i) {
                            final int q2 = b.q();
                            b.e(4);
                            array[i] = b.f(q2 - 8);
                        }
                        final y b2 = d3.b;
                        b2.d(8);
                        final ArrayList<com.applovin.exoplayer2.g.a.a> list = new ArrayList<com.applovin.exoplayer2.g.a.a>();
                        while (b2.a() > 8) {
                            final int c = b2.c();
                            final int q3 = b2.q();
                            final int j = b2.q() - 1;
                            if (j >= 0 && j < q) {
                                final com.applovin.exoplayer2.g.f.a a4 = com.applovin.exoplayer2.e.g.f.a(b2, c + q3, array[j]);
                                if (a4 != null) {
                                    list.add((com.applovin.exoplayer2.g.a.a)a4);
                                }
                            }
                            else {
                                final StringBuilder sb = new StringBuilder();
                                sb.append("Skipped metadata with unknown key index: ");
                                sb.append(j);
                                com.applovin.exoplayer2.l.q.c("AtomParsers", sb.toString());
                            }
                            b2.d(c + q3);
                        }
                        if (list.isEmpty()) {
                            a3 = a2;
                        }
                        else {
                            a3 = new com.applovin.exoplayer2.g.a(list);
                        }
                    }
                }
            }
        }
        return a3;
    }
    
    @Nullable
    private static com.applovin.exoplayer2.g.a a(final y y, final int n) {
        y.e(8);
        a(y);
        while (y.c() < n) {
            final int c = y.c();
            final int q = y.q();
            if (y.q() == 1768715124) {
                y.d(c);
                return b(y, c + q);
            }
            y.d(c + q);
        }
        return null;
    }
    
    public static List<n> a(final com.applovin.exoplayer2.e.g.a.a a, final r r, final long n, @Nullable final com.applovin.exoplayer2.d.e e, final boolean b, final boolean b2, final Function<com.applovin.exoplayer2.e.g.k, com.applovin.exoplayer2.e.g.k> function) throws com.applovin.exoplayer2.ai {
        final ArrayList list = new ArrayList();
        for (int i = 0; i < a.d.size(); ++i) {
            final com.applovin.exoplayer2.e.g.a.a a2 = a.d.get(i);
            if (a2.a == 1953653099) {
                final com.applovin.exoplayer2.e.g.k k = function.apply(a(a2, (com.applovin.exoplayer2.e.g.a.b)com.applovin.exoplayer2.l.a.b((Object)a.d(1836476516)), n, e, b, b2));
                if (k != null) {
                    list.add(a(k, (com.applovin.exoplayer2.e.g.a.a)com.applovin.exoplayer2.l.a.b((Object)((com.applovin.exoplayer2.e.g.a.a)com.applovin.exoplayer2.l.a.b((Object)((com.applovin.exoplayer2.e.g.a.a)com.applovin.exoplayer2.l.a.b((Object)a2.e(1835297121))).e(1835626086))).e(1937007212)), r));
                }
            }
        }
        return list;
    }
    
    public static void a(final y y) {
        final int c = y.c();
        y.e(4);
        int n = c;
        if (y.q() != 1751411826) {
            n = c + 4;
        }
        y.d(n);
    }
    
    private static void a(final y y, int n, final int n2, final int n3, final int n4, final int n5, @Nullable final com.applovin.exoplayer2.d.e e, final c c, int n6) throws com.applovin.exoplayer2.ai {
        y.d(n2 + 8 + 8);
        y.e(16);
        final int i = y.i();
        final int j = y.i();
        y.e(50);
        int c2 = y.c();
        com.applovin.exoplayer2.d.e a = e;
        int n7 = n;
        if (n == 1701733238) {
            final Pair<Integer, l> c3 = c(y, n2, n3);
            a = e;
            if (c3 != null) {
                n = (int)c3.first;
                if (e == null) {
                    a = null;
                }
                else {
                    a = e.a(((l)c3.second).b);
                }
                c.a[n6] = (l)c3.second;
            }
            y.d(c2);
            n7 = n;
        }
        final String s = "video/3gpp";
        String s2;
        if (n7 == 1831958048) {
            s2 = "video/mpeg";
        }
        else if (n7 == 1211250227) {
            s2 = "video/3gpp";
        }
        else {
            s2 = null;
        }
        float e2 = 1.0f;
        n6 = -1;
        String s3 = null;
        List<byte[]> list = null;
        byte[] array = null;
        b b = null;
        int n8 = 0;
        String s4 = s2;
        final com.applovin.exoplayer2.d.e e3 = a;
        while (c2 - n2 < n3) {
            y.d(c2);
            n = y.c();
            final int q = y.q();
            if (q == 0 && y.c() - n2 == n3) {
                break;
            }
            k.a(q > 0, "childAtomSize must be positive");
            final int q2 = y.q();
            String s5 = null;
            float d = 0.0f;
            s s6 = null;
            byte[] d2 = null;
            b b2 = null;
            int n9 = 0;
            Label_1324: {
                String s7 = null;
                Label_0450: {
                    if (q2 == 1635148611) {
                        k.a(s4 == null, null);
                        y.d(n + 8);
                        final com.applovin.exoplayer2.m.a a2 = com.applovin.exoplayer2.m.a.a(y);
                        list = a2.a;
                        c.c = a2.b;
                        if (n8 == 0) {
                            e2 = a2.e;
                        }
                        s3 = a2.f;
                        s5 = "video/avc";
                    }
                    else if (q2 == 1752589123) {
                        k.a(s4 == null, null);
                        y.d(n + 8);
                        final com.applovin.exoplayer2.m.f a3 = com.applovin.exoplayer2.m.f.a(y);
                        list = a3.a;
                        c.c = a3.b;
                        s3 = a3.c;
                        s5 = "video/hevc";
                    }
                    else if (q2 != 1685480259 && q2 != 1685485123) {
                        if (q2 == 1987076931) {
                            k.a(s4 == null, null);
                            if (n7 == 1987063864) {
                                s5 = "video/x-vnd.on2.vp8";
                            }
                            else {
                                s5 = "video/x-vnd.on2.vp9";
                            }
                        }
                        else {
                            if (q2 == 1635135811) {
                                k.a(s4 == null, null);
                                s5 = "video/av01";
                                d = e2;
                                n = n6;
                                s6 = (s)list;
                                d2 = array;
                                b2 = b;
                                n9 = n8;
                                break Label_1324;
                            }
                            if (q2 == 1681012275) {
                                k.a(s4 == null, null);
                                s5 = s;
                                d = e2;
                                n = n6;
                                s6 = (s)list;
                                d2 = array;
                                b2 = b;
                                n9 = n8;
                                break Label_1324;
                            }
                            if (q2 == 1702061171) {
                                k.a(s4 == null, null);
                                final Pair<String, byte[]> e4 = e(y, n);
                                s5 = (String)e4.first;
                                final byte[] array2 = (byte[])e4.second;
                                if (array2 != null) {
                                    list = com.applovin.exoplayer2.common.a.s.a(array2);
                                }
                                s7 = s3;
                                d = e2;
                                n = n6;
                                s6 = (s)list;
                                d2 = array;
                                break Label_0450;
                            }
                            if (q2 == 1885434736) {
                                d = d(y, n);
                                n9 = 1;
                                s5 = s4;
                                n = n6;
                                s6 = (s)list;
                                d2 = array;
                                b2 = b;
                                break Label_1324;
                            }
                            if (q2 == 1937126244) {
                                d2 = d(y, n, q);
                                s5 = s4;
                                s7 = s3;
                                d = e2;
                                n = n6;
                                s6 = (s)list;
                                break Label_0450;
                            }
                            if (q2 == 1936995172) {
                                final int h = y.h();
                                y.e(3);
                                s5 = s4;
                                s7 = s3;
                                d = e2;
                                n = n6;
                                s6 = (s)list;
                                d2 = array;
                                if (h != 0) {
                                    break Label_0450;
                                }
                                n = y.h();
                                if (n == 0) {
                                    n = 0;
                                    s5 = s4;
                                    s7 = s3;
                                    d = e2;
                                    s6 = (s)list;
                                    d2 = array;
                                    break Label_0450;
                                }
                                if (n == 1) {
                                    n = 1;
                                    s5 = s4;
                                    s7 = s3;
                                    d = e2;
                                    s6 = (s)list;
                                    d2 = array;
                                    break Label_0450;
                                }
                                if (n == 2) {
                                    n = 2;
                                    s5 = s4;
                                    s7 = s3;
                                    d = e2;
                                    s6 = (s)list;
                                    d2 = array;
                                    break Label_0450;
                                }
                                if (n != 3) {
                                    s5 = s4;
                                    s7 = s3;
                                    d = e2;
                                    n = n6;
                                    s6 = (s)list;
                                    d2 = array;
                                    break Label_0450;
                                }
                                n = 3;
                                s5 = s4;
                                s7 = s3;
                                d = e2;
                                s6 = (s)list;
                                d2 = array;
                                break Label_0450;
                            }
                            else {
                                s5 = s4;
                                s7 = s3;
                                d = e2;
                                n = n6;
                                s6 = (s)list;
                                d2 = array;
                                if (q2 != 1668246642) {
                                    break Label_0450;
                                }
                                final int q3 = y.q();
                                if (q3 == 1852009592) {
                                    n = 1;
                                }
                                else {
                                    n = 0;
                                }
                                if (n == 0 && q3 != 1852009571) {
                                    final StringBuilder sb = new StringBuilder();
                                    sb.append("Unsupported color type: ");
                                    sb.append(com.applovin.exoplayer2.e.g.a.c(q3));
                                    com.applovin.exoplayer2.l.q.c("AtomParsers", sb.toString());
                                    s5 = s4;
                                    s7 = s3;
                                    d = e2;
                                    n = n6;
                                    s6 = (s)list;
                                    d2 = array;
                                    break Label_0450;
                                }
                                final int k = y.i();
                                final int l = y.i();
                                y.e(2);
                                if (n != 0 && (y.h() & 0x80) != 0x0) {
                                    n = 1;
                                }
                                else {
                                    n = 0;
                                }
                                final int a4 = com.applovin.exoplayer2.m.b.a(k);
                                if (n != 0) {
                                    n = 1;
                                }
                                else {
                                    n = 2;
                                }
                                b2 = new b(a4, n, com.applovin.exoplayer2.m.b.b(l), (byte[])null);
                                s5 = s4;
                                d = e2;
                                n = n6;
                                s6 = (s)list;
                                d2 = array;
                                n9 = n8;
                                break Label_1324;
                            }
                        }
                    }
                    else {
                        final com.applovin.exoplayer2.m.c a5 = c.a(y);
                        s5 = s4;
                        d = e2;
                        n = n6;
                        s6 = (s)list;
                        d2 = array;
                        b2 = b;
                        n9 = n8;
                        if (a5 != null) {
                            s3 = a5.c;
                            s5 = "video/dolby-vision";
                            n9 = n8;
                            b2 = b;
                            d2 = array;
                            s6 = (s)list;
                            n = n6;
                            d = e2;
                        }
                        break Label_1324;
                    }
                    d2 = array;
                    s6 = (s)list;
                    n = n6;
                    d = e2;
                    s7 = s3;
                }
                s3 = s7;
                b2 = b;
                n9 = n8;
            }
            c2 += q;
            s4 = s5;
            e2 = d;
            n6 = n;
            list = s6;
            array = d2;
            b = b2;
            n8 = n9;
        }
        if (s4 == null) {
            return;
        }
        c.b = new v.a().a(n4).f(s4).d(s3).g(i).h(j).b(e2).i(n5).a(array).j(n6).a(list).a(e3).a(b).a();
    }
    
    private static void a(final y y, int n, final int n2, final int n3, final int n4, final String s, final c c) {
        y.d(n2 + 8 + 8);
        final String s2 = "application/ttml+xml";
        List<byte[]> a = null;
        long n5 = Long.MAX_VALUE;
        String s3;
        if (n == 1414810956) {
            s3 = s2;
        }
        else if (n == 1954034535) {
            n = n3 - 8 - 8;
            final byte[] array = new byte[n];
            y.a(array, 0, n);
            a = s.a(array);
            s3 = "application/x-quicktime-tx3g";
        }
        else if (n == 2004251764) {
            s3 = "application/x-mp4-vtt";
        }
        else if (n == 1937010800) {
            n5 = 0L;
            s3 = s2;
        }
        else {
            if (n != 1664495672) {
                throw new IllegalStateException();
            }
            c.d = 1;
            s3 = "application/x-mp4-cea-608";
        }
        c.b = new v.a().a(n4).f(s3).c(s).a(n5).a(a).a();
    }
    
    private static void a(final y y, int intValue, final int n, final int n2, final int i, final String s, final boolean b, @Nullable com.applovin.exoplayer2.d.e a, final c c, int n3) throws com.applovin.exoplayer2.ai {
        y.d(n + 8 + 8);
        int j;
        if (b) {
            j = y.i();
            y.e(6);
        }
        else {
            y.e(8);
            j = 0;
        }
        int n4;
        int n5;
        if (j != 0 && j != 1) {
            if (j != 2) {
                return;
            }
            y.e(16);
            n4 = (int)Math.round(y.A());
            n5 = y.w();
            y.e(20);
        }
        else {
            final int k = y.i();
            y.e(6);
            n4 = y.u();
            if (j == 1) {
                y.e(16);
            }
            n5 = k;
        }
        final int c2 = y.c();
        com.applovin.exoplayer2.d.e e;
        if (intValue == 1701733217) {
            final Pair<Integer, l> c3 = c(y, n, n2);
            if (c3 != null) {
                intValue = (int)c3.first;
                if (a == null) {
                    a = null;
                }
                else {
                    a = a.a(((l)c3.second).b);
                }
                c.a[n3] = (l)c3.second;
            }
            y.d(c2);
            e = a;
        }
        else {
            e = a;
        }
        String s2 = null;
        Label_0532: {
            if (intValue == 1633889587) {
                s2 = "audio/ac3";
            }
            else if (intValue == 1700998451) {
                s2 = "audio/eac3";
            }
            else if (intValue == 1633889588) {
                s2 = "audio/ac4";
            }
            else if (intValue == 1685353315) {
                s2 = "audio/vnd.dts";
            }
            else if (intValue != 1685353320 && intValue != 1685353324) {
                if (intValue == 1685353317) {
                    s2 = "audio/vnd.dts.hd;profile=lbr";
                }
                else if (intValue == 1685353336) {
                    s2 = "audio/vnd.dts.uhd;profile=p2";
                }
                else if (intValue == 1935764850) {
                    s2 = "audio/3gpp";
                }
                else if (intValue == 1935767394) {
                    s2 = "audio/amr-wb";
                }
                else {
                    s2 = "audio/raw";
                    if (intValue == 1819304813 || intValue == 1936684916) {
                        intValue = 2;
                        break Label_0532;
                    }
                    if (intValue == 1953984371) {
                        intValue = 268435456;
                        break Label_0532;
                    }
                    if (intValue != 778924082 && intValue != 778924083) {
                        if (intValue == 1835557169) {
                            s2 = "audio/mha1";
                        }
                        else if (intValue == 1835560241) {
                            s2 = "audio/mhm1";
                        }
                        else if (intValue == 1634492771) {
                            s2 = "audio/alac";
                        }
                        else if (intValue == 1634492791) {
                            s2 = "audio/g711-alaw";
                        }
                        else if (intValue == 1970037111) {
                            s2 = "audio/g711-mlaw";
                        }
                        else if (intValue == 1332770163) {
                            s2 = "audio/opus";
                        }
                        else {
                            if (intValue != 1716281667) {
                                intValue = -1;
                                s2 = null;
                                break Label_0532;
                            }
                            s2 = "audio/flac";
                        }
                    }
                    else {
                        s2 = "audio/mpeg";
                    }
                }
            }
            else {
                s2 = "audio/vnd.dts.hd";
            }
            intValue = -1;
        }
        String c4 = null;
        s s3 = null;
        String s4 = s2;
        n3 = c2;
        while (true) {
            boolean b2 = true;
            if (n3 - n >= n2) {
                break;
            }
            y.d(n3);
            final int q = y.q();
            if (q <= 0) {
                b2 = false;
            }
            k.a(b2, "childAtomSize must be positive");
            final int q2 = y.q();
            List<byte[]> list = null;
            int intValue2 = 0;
            int intValue3 = 0;
            String s5 = null;
            Label_1244: {
                Label_0639: {
                    if (q2 == 1835557187) {
                        final int n6 = q - 13;
                        final byte[] array = new byte[n6];
                        y.d(n3 + 13);
                        y.a(array, 0, n6);
                        list = s.a(array);
                    }
                    else if (q2 != 1702061171 && (!b || q2 != 2002876005)) {
                        Label_1079: {
                            if (q2 == 1684103987) {
                                y.d(n3 + 8);
                                c.b = com.applovin.exoplayer2.b.b.a(y, Integer.toString(i), s, e);
                            }
                            else if (q2 == 1684366131) {
                                y.d(n3 + 8);
                                c.b = com.applovin.exoplayer2.b.b.b(y, Integer.toString(i), s, e);
                            }
                            else if (q2 == 1684103988) {
                                y.d(n3 + 8);
                                c.b = com.applovin.exoplayer2.b.c.a(y, Integer.toString(i), s, e);
                            }
                            else if (q2 == 1684305011) {
                                c.b = new v.a().a(i).f(s4).k(n5).l(n4).a(e).c(s).a();
                            }
                            else {
                                if (q2 == 1682927731) {
                                    final int n7 = q - 8;
                                    final byte[] a2 = b.a;
                                    final byte[] copy = Arrays.copyOf(a2, a2.length + n7);
                                    y.d(n3 + 8);
                                    y.a(copy, a2.length, n7);
                                    list = com.applovin.exoplayer2.b.s.b(copy);
                                    break Label_0639;
                                }
                                if (q2 == 1684425825) {
                                    final int n8 = q - 12;
                                    final byte[] array2 = new byte[n8 + 4];
                                    array2[0] = 102;
                                    array2[1] = 76;
                                    array2[2] = 97;
                                    array2[3] = 67;
                                    y.d(n3 + 12);
                                    y.a(array2, 4, n8);
                                    list = s.a(array2);
                                    break Label_1079;
                                }
                                if (q2 == 1634492771) {
                                    final int n9 = q - 12;
                                    final byte[] array3 = new byte[n9];
                                    y.d(n3 + 12);
                                    y.a(array3, 0, n9);
                                    final Pair a3 = com.applovin.exoplayer2.l.e.a(array3);
                                    intValue2 = (int)a3.first;
                                    intValue3 = (int)a3.second;
                                    list = s.a(array3);
                                    s5 = c4;
                                    break Label_1244;
                                }
                                list = s3;
                                break Label_1079;
                            }
                            list = s3;
                        }
                    }
                    else {
                        int b3;
                        if (q2 == 1702061171) {
                            b3 = n3;
                        }
                        else {
                            b3 = b(y, n3, q);
                        }
                        intValue3 = n5;
                        intValue2 = n4;
                        s5 = c4;
                        list = s3;
                        if (b3 == -1) {
                            break Label_1244;
                        }
                        final Pair<String, byte[]> e2 = e(y, b3);
                        final String anObject = (String)e2.first;
                        final byte[] array4 = (byte[])e2.second;
                        intValue3 = n5;
                        intValue2 = n4;
                        s4 = anObject;
                        s5 = c4;
                        list = s3;
                        if (array4 != null) {
                            if ("audio/mp4a-latm".equals(anObject)) {
                                final com.applovin.exoplayer2.b.a.a a4 = com.applovin.exoplayer2.b.a.a(array4);
                                n4 = a4.a;
                                n5 = a4.b;
                                c4 = a4.c;
                            }
                            list = s.a(array4);
                            s5 = c4;
                            s4 = anObject;
                            intValue2 = n4;
                            intValue3 = n5;
                        }
                        break Label_1244;
                    }
                }
                intValue3 = n5;
                intValue2 = n4;
                s5 = c4;
            }
            n3 += q;
            n5 = intValue3;
            n4 = intValue2;
            c4 = s5;
            s3 = (s)list;
        }
        if (c.b == null && s4 != null) {
            c.b = new v.a().a(i).f(s4).d(c4).k(n5).l(n4).m(intValue).a(s3).a(e).c(s).a();
        }
    }
    
    private static void a(final y y, final int n, final int n2, final int n3, final c c) {
        y.d(n2 + 8 + 8);
        if (n == 1835365492) {
            y.B();
            final String b = y.B();
            if (b != null) {
                c.b = new v.a().a(n3).f(b).a();
            }
        }
    }
    
    private static boolean a(final long[] array, final long n, final long n2, final long n3) {
        final int length = array.length;
        boolean b = true;
        final int n4 = length - 1;
        final int a = ai.a(4, 0, n4);
        final int a2 = ai.a(array.length - 4, 0, n4);
        if (array[0] > n2 || n2 >= array[a] || array[a2] >= n3 || n3 > n) {
            b = false;
        }
        return b;
    }
    
    private static int b(final y y, final int n, final int n2) throws com.applovin.exoplayer2.ai {
        int q;
        for (int c = y.c(); c - n < n2; c += q) {
            y.d(c);
            q = y.q();
            k.a(q > 0, "childAtomSize must be positive");
            if (y.q() == 1702061171) {
                return c;
            }
        }
        return -1;
    }
    
    private static long b(final y y) {
        int n = 8;
        y.d(8);
        if (com.applovin.exoplayer2.e.g.a.a(y.q()) != 0) {
            n = 16;
        }
        y.e(n);
        return y.o();
    }
    
    @Nullable
    private static Pair<long[], long[]> b(final com.applovin.exoplayer2.e.g.a.a a) {
        final com.applovin.exoplayer2.e.g.a.b d = a.d(1701606260);
        if (d == null) {
            return null;
        }
        final y b = d.b;
        b.d(8);
        final int a2 = a.a(b.q());
        final int w = b.w();
        final long[] array = new long[w];
        final long[] array2 = new long[w];
        for (int i = 0; i < w; ++i) {
            long n;
            if (a2 == 1) {
                n = b.y();
            }
            else {
                n = b.o();
            }
            array[i] = n;
            long s;
            if (a2 == 1) {
                s = b.s();
            }
            else {
                s = b.q();
            }
            array2[i] = s;
            if (b.k() != 1) {
                throw new IllegalArgumentException("Unsupported media rate.");
            }
            b.e(2);
        }
        return (Pair<long[], long[]>)Pair.create((Object)array, (Object)array2);
    }
    
    @Nullable
    private static com.applovin.exoplayer2.g.a b(final y y, final int n) {
        y.e(8);
        final ArrayList list = new ArrayList();
        while (y.c() < n) {
            final com.applovin.exoplayer2.g.a.a a = com.applovin.exoplayer2.e.g.f.a(y);
            if (a != null) {
                list.add(a);
            }
        }
        com.applovin.exoplayer2.g.a a2;
        if (list.isEmpty()) {
            a2 = null;
        }
        else {
            a2 = new com.applovin.exoplayer2.g.a(list);
        }
        return a2;
    }
    
    @Nullable
    private static Pair<Integer, l> c(final y y, final int n, final int n2) throws com.applovin.exoplayer2.ai {
        int q;
        for (int c = y.c(); c - n < n2; c += q) {
            y.d(c);
            q = y.q();
            k.a(q > 0, "childAtomSize must be positive");
            if (y.q() == 1936289382) {
                final Pair<Integer, l> a = a(y, c, q);
                if (a != null) {
                    return a;
                }
            }
        }
        return null;
    }
    
    private static f c(final y y) {
        final int n = 8;
        y.d(8);
        final int a = com.applovin.exoplayer2.e.g.a.a(y.q());
        int n2;
        if (a == 0) {
            n2 = 8;
        }
        else {
            n2 = 16;
        }
        y.e(n2);
        final int q = y.q();
        y.e(4);
        final int c = y.c();
        int n3 = n;
        if (a == 0) {
            n3 = 4;
        }
        final int n4 = 0;
        while (true) {
            for (int i = 0; i < n3; ++i) {
                if (y.d()[c + i] != -1) {
                    final boolean b = false;
                    final long n5 = -9223372036854775807L;
                    long n6;
                    if (b) {
                        y.e(n3);
                        n6 = n5;
                    }
                    else {
                        if (a == 0) {
                            n6 = y.o();
                        }
                        else {
                            n6 = y.y();
                        }
                        if (n6 == 0L) {
                            n6 = n5;
                        }
                    }
                    y.e(16);
                    final int q2 = y.q();
                    final int q3 = y.q();
                    y.e(4);
                    final int q4 = y.q();
                    final int q5 = y.q();
                    int n7;
                    if (q2 == 0 && q3 == 65536 && q4 == -65536 && q5 == 0) {
                        n7 = 90;
                    }
                    else if (q2 == 0 && q3 == -65536 && q4 == 65536 && q5 == 0) {
                        n7 = 270;
                    }
                    else {
                        n7 = n4;
                        if (q2 == -65536) {
                            n7 = n4;
                            if (q3 == 0) {
                                n7 = n4;
                                if (q4 == 0) {
                                    n7 = n4;
                                    if (q5 == -65536) {
                                        n7 = 180;
                                    }
                                }
                            }
                        }
                    }
                    return new f(q, n6, n7);
                }
            }
            final boolean b = true;
            continue;
        }
    }
    
    @Nullable
    private static com.applovin.exoplayer2.g.a c(final y y, int h) {
        y.e(12);
        while (y.c() < h) {
            final int c = y.c();
            final int q = y.q();
            if (y.q() == 1935766900) {
                if (q < 14) {
                    return null;
                }
                y.e(5);
                h = y.h();
                if (h != 12 && h != 13) {
                    return null;
                }
                float n;
                if (h == 12) {
                    n = 240.0f;
                }
                else {
                    n = 120.0f;
                }
                y.e(1);
                return new com.applovin.exoplayer2.g.a(new com.applovin.exoplayer2.g.a.a[] { new com.applovin.exoplayer2.g.f.d(n, y.h()) });
            }
            else {
                y.d(c + q);
            }
        }
        return null;
    }
    
    private static float d(final y y, int w) {
        y.d(w + 8);
        w = y.w();
        return w / (float)y.w();
    }
    
    private static int d(final y y) {
        y.d(16);
        return y.q();
    }
    
    @Nullable
    private static byte[] d(final y y, final int n, final int n2) {
        int q;
        for (int from = n + 8; from - n < n2; from += q) {
            y.d(from);
            q = y.q();
            if (y.q() == 1886547818) {
                return Arrays.copyOfRange(y.d(), from, q + from);
            }
        }
        return null;
    }
    
    private static Pair<Long, String> e(final y y) {
        final int n = 8;
        y.d(8);
        final int a = com.applovin.exoplayer2.e.g.a.a(y.q());
        int n2;
        if (a == 0) {
            n2 = 8;
        }
        else {
            n2 = 16;
        }
        y.e(n2);
        final long o = y.o();
        int n3 = n;
        if (a == 0) {
            n3 = 4;
        }
        y.e(n3);
        final int i = y.i();
        final StringBuilder sb = new StringBuilder();
        sb.append("");
        sb.append((char)((i >> 10 & 0x1F) + 96));
        sb.append((char)((i >> 5 & 0x1F) + 96));
        sb.append((char)((i & 0x1F) + 96));
        return (Pair<Long, String>)Pair.create((Object)o, (Object)sb.toString());
    }
    
    private static Pair<String, byte[]> e(final y y, int n) {
        y.d(n + 8 + 4);
        y.e(1);
        f(y);
        y.e(2);
        n = y.h();
        if ((n & 0x80) != 0x0) {
            y.e(2);
        }
        if ((n & 0x40) != 0x0) {
            y.e(y.i());
        }
        if ((n & 0x20) != 0x0) {
            y.e(2);
        }
        y.e(1);
        f(y);
        final String a = u.a(y.h());
        if (!"audio/mpeg".equals(a) && !"audio/vnd.dts".equals(a) && !"audio/vnd.dts.hd".equals(a)) {
            y.e(12);
            y.e(1);
            n = f(y);
            final byte[] array = new byte[n];
            y.a(array, 0, n);
            return (Pair<String, byte[]>)Pair.create((Object)a, (Object)array);
        }
        return (Pair<String, byte[]>)Pair.create((Object)a, (Object)null);
    }
    
    private static int f(final y y) {
        int n;
        int n2;
        for (n = y.h(), n2 = (n & 0x7F); (n & 0x80) == 0x80; n = y.h(), n2 = (n2 << 7 | (n & 0x7F))) {}
        return n2;
    }
    
    private static final class a
    {
        public final int a;
        public int b;
        public int c;
        public long d;
        private final boolean e;
        private final y f;
        private final y g;
        private int h;
        private int i;
        
        public a(final y g, final y f, final boolean e) throws com.applovin.exoplayer2.ai {
            this.g = g;
            this.f = f;
            this.e = e;
            f.d(12);
            this.a = f.w();
            g.d(12);
            this.i = g.w();
            final int q = g.q();
            boolean b = true;
            if (q != 1) {
                b = false;
            }
            k.a(b, "first_chunk must be 1");
            this.b = -1;
        }
        
        public boolean a() {
            final int b = this.b + 1;
            this.b = b;
            if (b == this.a) {
                return false;
            }
            long d;
            if (this.e) {
                d = this.f.y();
            }
            else {
                d = this.f.o();
            }
            this.d = d;
            if (this.b == this.h) {
                this.c = this.g.w();
                this.g.e(4);
                int h;
                if (--this.i > 0) {
                    h = this.g.w() - 1;
                }
                else {
                    h = -1;
                }
                this.h = h;
            }
            return true;
        }
    }
    
    private interface b
    {
        int a();
        
        int b();
        
        int c();
    }
    
    private static final class c
    {
        public final l[] a;
        @Nullable
        public v b;
        public int c;
        public int d;
        
        public c(final int n) {
            this.a = new l[n];
            this.d = 0;
        }
    }
    
    static final class d implements b
    {
        private final int a;
        private final int b;
        private final y c;
        
        public d(final com.applovin.exoplayer2.e.g.a.b b, final v v) {
            final y b2 = b.b;
            (this.c = b2).d(12);
            int w;
            final int i = w = b2.w();
            Label_0118: {
                if ("audio/raw".equals(v.l)) {
                    final int c = ai.c(v.A, v.y);
                    if (i != 0) {
                        w = i;
                        if (i % c == 0) {
                            break Label_0118;
                        }
                    }
                    final StringBuilder sb = new StringBuilder();
                    sb.append("Audio sample size mismatch. stsd sample size: ");
                    sb.append(c);
                    sb.append(", stsz sample size: ");
                    sb.append(i);
                    q.c("AtomParsers", sb.toString());
                    w = c;
                }
            }
            int a;
            if ((a = w) == 0) {
                a = -1;
            }
            this.a = a;
            this.b = b2.w();
        }
        
        @Override
        public int a() {
            return this.b;
        }
        
        @Override
        public int b() {
            return this.a;
        }
        
        @Override
        public int c() {
            int n;
            if ((n = this.a) == -1) {
                n = this.c.w();
            }
            return n;
        }
    }
    
    static final class e implements b
    {
        private final y a;
        private final int b;
        private final int c;
        private int d;
        private int e;
        
        public e(final com.applovin.exoplayer2.e.g.a.b b) {
            final y b2 = b.b;
            (this.a = b2).d(12);
            this.c = (b2.w() & 0xFF);
            this.b = b2.w();
        }
        
        @Override
        public int a() {
            return this.b;
        }
        
        @Override
        public int b() {
            return -1;
        }
        
        @Override
        public int c() {
            final int c = this.c;
            if (c == 8) {
                return this.a.h();
            }
            if (c == 16) {
                return this.a.i();
            }
            if (this.d++ % 2 == 0) {
                return ((this.e = this.a.h()) & 0xF0) >> 4;
            }
            return this.e & 0xF;
        }
    }
    
    private static final class f
    {
        private final int a;
        private final long b;
        private final int c;
        
        public f(final int a, final long b, final int c) {
            this.a = a;
            this.b = b;
            this.c = c;
        }
    }
}

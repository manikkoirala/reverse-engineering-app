// 
// Decompiled by Procyon v0.6.0
// 

package com.applovin.exoplayer2.e.g;

import com.applovin.exoplayer2.l.y;
import java.util.Arrays;
import androidx.annotation.Nullable;
import java.util.ArrayList;
import java.util.List;

abstract class a
{
    public final int a;
    
    public a(final int a) {
        this.a = a;
    }
    
    public static int a(final int n) {
        return n >> 24 & 0xFF;
    }
    
    public static int b(final int n) {
        return n & 0xFFFFFF;
    }
    
    public static String c(final int n) {
        final StringBuilder sb = new StringBuilder();
        sb.append("");
        sb.append((char)(n >> 24 & 0xFF));
        sb.append((char)(n >> 16 & 0xFF));
        sb.append((char)(n >> 8 & 0xFF));
        sb.append((char)(n & 0xFF));
        return sb.toString();
    }
    
    @Override
    public String toString() {
        return c(this.a);
    }
    
    static final class a extends com.applovin.exoplayer2.e.g.a
    {
        public final long b;
        public final List<b> c;
        public final List<a> d;
        
        public a(final int n, final long b) {
            super(n);
            this.b = b;
            this.c = new ArrayList<b>();
            this.d = new ArrayList<a>();
        }
        
        public void a(final a a) {
            this.d.add(a);
        }
        
        public void a(final b b) {
            this.c.add(b);
        }
        
        @Nullable
        public b d(final int n) {
            for (int size = this.c.size(), i = 0; i < size; ++i) {
                final b b = this.c.get(i);
                if (b.a == n) {
                    return b;
                }
            }
            return null;
        }
        
        @Nullable
        public a e(final int n) {
            for (int size = this.d.size(), i = 0; i < size; ++i) {
                final a a = this.d.get(i);
                if (a.a == n) {
                    return a;
                }
            }
            return null;
        }
        
        @Override
        public String toString() {
            final StringBuilder sb = new StringBuilder();
            sb.append(com.applovin.exoplayer2.e.g.a.c(super.a));
            sb.append(" leaves: ");
            sb.append(Arrays.toString(this.c.toArray()));
            sb.append(" containers: ");
            sb.append(Arrays.toString(this.d.toArray()));
            return sb.toString();
        }
    }
    
    static final class b extends a
    {
        public final y b;
        
        public b(final int n, final y b) {
            super(n);
            this.b = b;
        }
    }
}

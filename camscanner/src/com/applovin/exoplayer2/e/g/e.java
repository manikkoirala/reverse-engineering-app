// 
// Decompiled by Procyon v0.6.0
// 

package com.applovin.exoplayer2.e.g;

import com.applovin.exoplayer2.e.u;
import com.applovin.exoplayer2.k.g;
import java.io.IOException;
import com.applovin.exoplayer2.e.i;
import com.applovin.exoplayer2.common.base.Function;
import com.applovin.exoplayer2.e.r;
import java.util.Arrays;
import java.util.UUID;
import com.applovin.exoplayer2.l.q;
import java.util.ArrayList;
import android.util.Pair;
import com.applovin.exoplayer2.ai;
import java.util.Collections;
import java.util.ArrayDeque;
import com.applovin.exoplayer2.g.b.c;
import com.applovin.exoplayer2.l.ag;
import com.applovin.exoplayer2.l.y;
import android.util.SparseArray;
import java.util.List;
import com.applovin.exoplayer2.e.x;
import com.applovin.exoplayer2.e.j;
import androidx.annotation.Nullable;
import com.applovin.exoplayer2.v;
import com.applovin.exoplayer2.e.l;
import com.applovin.exoplayer2.e.h;

public class e implements h
{
    public static final l a;
    private static final byte[] b;
    private static final v c;
    private long A;
    private long B;
    @Nullable
    private b C;
    private int D;
    private int E;
    private int F;
    private boolean G;
    private j H;
    private x[] I;
    private x[] J;
    private boolean K;
    private final int d;
    @Nullable
    private final k e;
    private final List<v> f;
    private final SparseArray<b> g;
    private final y h;
    private final y i;
    private final y j;
    private final byte[] k;
    private final y l;
    @Nullable
    private final ag m;
    private final c n;
    private final y o;
    private final ArrayDeque<com.applovin.exoplayer2.e.g.a.a> p;
    private final ArrayDeque<a> q;
    @Nullable
    private final x r;
    private int s;
    private int t;
    private long u;
    private int v;
    @Nullable
    private y w;
    private long x;
    private int y;
    private long z;
    
    static {
        a = new \u3007o00\u3007\u3007Oo();
        b = new byte[] { -94, 57, 79, 82, 90, -101, 79, 20, -94, 68, 108, 66, 124, 100, -115, -12 };
        c = new v.a().f("application/x-emsg").a();
    }
    
    public e() {
        this(0);
    }
    
    public e(final int n) {
        this(n, null);
    }
    
    public e(final int n, @Nullable final ag ag) {
        this(n, ag, null, Collections.emptyList());
    }
    
    public e(final int n, @Nullable final ag ag, @Nullable final k k, final List<v> list) {
        this(n, ag, k, list, null);
    }
    
    public e(final int d, @Nullable final ag m, @Nullable final k e, final List<v> list, @Nullable final x r) {
        this.d = d;
        this.m = m;
        this.e = e;
        this.f = Collections.unmodifiableList((List<? extends v>)list);
        this.r = r;
        this.n = new c();
        this.o = new y(16);
        this.h = new y(com.applovin.exoplayer2.l.v.a);
        this.i = new y(5);
        this.j = new y();
        final byte[] k = new byte[16];
        this.k = k;
        this.l = new y(k);
        this.p = new ArrayDeque<com.applovin.exoplayer2.e.g.a.a>();
        this.q = new ArrayDeque<a>();
        this.g = (SparseArray<b>)new SparseArray();
        this.A = -9223372036854775807L;
        this.z = -9223372036854775807L;
        this.B = -9223372036854775807L;
        this.H = com.applovin.exoplayer2.e.j.a;
        this.I = new x[0];
        this.J = new x[0];
    }
    
    private static int a(final int i) throws ai {
        if (i >= 0) {
            return i;
        }
        final StringBuilder sb = new StringBuilder();
        sb.append("Unexpected negative value: ");
        sb.append(i);
        throw ai.b(sb.toString(), null);
    }
    
    private static int a(final b b, int n, int n2, final y y, int i) throws ai {
        y.d(8);
        final int b2 = com.applovin.exoplayer2.e.g.a.b(y.q());
        final k a = b.d.a;
        final m b3 = b.b;
        final com.applovin.exoplayer2.e.g.c c = (com.applovin.exoplayer2.e.g.c)com.applovin.exoplayer2.l.ai.a((Object)b3.a);
        b3.h[n] = y.w();
        final long[] g = b3.g;
        final long c2 = b3.c;
        g[n] = c2;
        if ((b2 & 0x1) != 0x0) {
            g[n] = c2 + y.q();
        }
        final boolean b4 = (b2 & 0x4) != 0x0;
        int n3 = c.d;
        if (b4) {
            n3 = y.q();
        }
        final boolean b5 = (b2 & 0x100) != 0x0;
        final boolean b6 = (b2 & 0x200) != 0x0;
        final boolean b7 = (b2 & 0x400) != 0x0;
        final boolean b8 = (b2 & 0x800) != 0x0;
        final long[] h = a.h;
        long d;
        if (h != null && h.length == 1 && h[0] == 0L) {
            d = com.applovin.exoplayer2.l.ai.d(((long[])com.applovin.exoplayer2.l.ai.a((Object)a.i))[0], 1000000L, a.c);
        }
        else {
            d = 0L;
        }
        final int[] j = b3.i;
        final int[] k = b3.j;
        final long[] l = b3.k;
        final boolean[] m = b3.l;
        if (a.b == 2 && (n2 & 0x1) != 0x0) {
            n2 = 1;
        }
        else {
            n2 = 0;
        }
        final int n4 = i + b3.h[n];
        final long c3 = a.c;
        long r = b3.r;
        while (i < n4) {
            if (b5) {
                n = y.q();
            }
            else {
                n = c.b;
            }
            final int a2 = a(n);
            if (b6) {
                n = y.q();
            }
            else {
                n = c.c;
            }
            final int a3 = a(n);
            if (b7) {
                n = y.q();
            }
            else if (i == 0 && b4) {
                n = n3;
            }
            else {
                n = c.d;
            }
            if (b8) {
                k[i] = (int)(y.q() * 1000000L / c3);
            }
            else {
                k[i] = 0;
            }
            final long n5 = com.applovin.exoplayer2.l.ai.d(r, 1000000L, c3) - d;
            l[i] = n5;
            if (!b3.s) {
                l[i] = n5 + b.d.h;
            }
            j[i] = a3;
            m[i] = ((n >> 16 & 0x1) == 0x0 && (n2 == 0 || i == 0));
            r += a2;
            ++i;
        }
        b3.r = r;
        return n4;
    }
    
    private static Pair<Long, com.applovin.exoplayer2.e.c> a(final y y, long n) throws ai {
        y.d(8);
        final int a = com.applovin.exoplayer2.e.g.a.a(y.q());
        y.e(4);
        final long o = y.o();
        long n2;
        long n3;
        if (a == 0) {
            n2 = y.o();
            n3 = y.o();
        }
        else {
            n2 = y.y();
            n3 = y.y();
        }
        n += n3;
        final long d = com.applovin.exoplayer2.l.ai.d(n2, 1000000L, o);
        y.e(2);
        final int i = y.i();
        final int[] array = new int[i];
        final long[] array2 = new long[i];
        final long[] array3 = new long[i];
        final long[] array4 = new long[i];
        long n4 = n2;
        long d2 = d;
        for (int j = 0; j < i; ++j) {
            final int q = y.q();
            if ((q & Integer.MIN_VALUE) != 0x0) {
                throw ai.b("Unhandled indirect reference", null);
            }
            final long o2 = y.o();
            array[j] = (q & Integer.MAX_VALUE);
            array2[j] = n;
            array4[j] = d2;
            n4 += o2;
            d2 = com.applovin.exoplayer2.l.ai.d(n4, 1000000L, o);
            array3[j] = d2 - array4[j];
            y.e(4);
            n += array[j];
        }
        return (Pair<Long, com.applovin.exoplayer2.e.c>)Pair.create((Object)d, (Object)new com.applovin.exoplayer2.e.c(array, array2, array3, array4));
    }
    
    @Nullable
    private static com.applovin.exoplayer2.d.e a(final List<com.applovin.exoplayer2.e.g.a.b> list) {
        final int size = list.size();
        final com.applovin.exoplayer2.d.e e = null;
        int i = 0;
        List<com.applovin.exoplayer2.d.e.a> list2 = null;
        while (i < size) {
            final com.applovin.exoplayer2.e.g.a.b b = list.get(i);
            List<com.applovin.exoplayer2.d.e.a> list3 = list2;
            if (b.a == 1886614376) {
                if ((list3 = list2) == null) {
                    list3 = new ArrayList<com.applovin.exoplayer2.d.e.a>();
                }
                final byte[] d = b.b.d();
                final UUID b2 = com.applovin.exoplayer2.e.g.h.b(d);
                if (b2 == null) {
                    q.c("FragmentedMp4Extractor", "Skipped pssh atom (failed to extract uuid)");
                }
                else {
                    ((ArrayList<com.applovin.exoplayer2.d.e.a>)list3).add(new com.applovin.exoplayer2.d.e.a(b2, "video/mp4", d));
                }
            }
            ++i;
            list2 = list3;
        }
        com.applovin.exoplayer2.d.e e2;
        if (list2 == null) {
            e2 = e;
        }
        else {
            e2 = new com.applovin.exoplayer2.d.e(list2);
        }
        return e2;
    }
    
    private com.applovin.exoplayer2.e.g.c a(final SparseArray<com.applovin.exoplayer2.e.g.c> sparseArray, final int n) {
        if (sparseArray.size() == 1) {
            return (com.applovin.exoplayer2.e.g.c)sparseArray.valueAt(0);
        }
        return (com.applovin.exoplayer2.e.g.c)com.applovin.exoplayer2.l.a.b((Object)sparseArray.get(n));
    }
    
    @Nullable
    private static b a(final SparseArray<b> sparseArray) {
        final int size = sparseArray.size();
        b b = null;
        long n = Long.MAX_VALUE;
        b b3;
        long n2;
        for (int i = 0; i < size; ++i, b = b3, n = n2) {
            final b b2 = (b)sparseArray.valueAt(i);
            if (!b2.l) {
                b3 = b;
                n2 = n;
                if (b2.f == b2.d.b) {
                    continue;
                }
            }
            if (b2.l && b2.h == b2.b.e) {
                b3 = b;
                n2 = n;
            }
            else {
                final long c = b2.c();
                b3 = b;
                n2 = n;
                if (c < n) {
                    b3 = b2;
                    n2 = c;
                }
            }
        }
        return b;
    }
    
    @Nullable
    private static b a(final y y, final SparseArray<b> sparseArray, final boolean b) {
        y.d(8);
        final int b2 = com.applovin.exoplayer2.e.g.a.b(y.q());
        final int q = y.q();
        Object o;
        if (b) {
            o = sparseArray.valueAt(0);
        }
        else {
            o = sparseArray.get(q);
        }
        final b b3 = (b)o;
        if (b3 == null) {
            return null;
        }
        if ((b2 & 0x1) != 0x0) {
            final long y2 = y.y();
            final m b4 = b3.b;
            b4.c = y2;
            b4.d = y2;
        }
        final com.applovin.exoplayer2.e.g.c e = b3.e;
        int a;
        if ((b2 & 0x2) != 0x0) {
            a = y.q() - 1;
        }
        else {
            a = e.a;
        }
        int n;
        if ((b2 & 0x8) != 0x0) {
            n = y.q();
        }
        else {
            n = e.b;
        }
        int n2;
        if ((b2 & 0x10) != 0x0) {
            n2 = y.q();
        }
        else {
            n2 = e.c;
        }
        int n3;
        if ((b2 & 0x20) != 0x0) {
            n3 = y.q();
        }
        else {
            n3 = e.d;
        }
        b3.b.a = new com.applovin.exoplayer2.e.g.c(a, n, n2, n3);
        return b3;
    }
    
    private void a() {
        this.s = 0;
        this.v = 0;
    }
    
    private void a(final long n) throws ai {
        while (!this.p.isEmpty() && this.p.peek().b == n) {
            this.a(this.p.pop());
        }
        this.a();
    }
    
    private void a(final com.applovin.exoplayer2.e.g.a.a a) throws ai {
        final int a2 = a.a;
        if (a2 == 1836019574) {
            this.b(a);
        }
        else if (a2 == 1836019558) {
            this.c(a);
        }
        else if (!this.p.isEmpty()) {
            this.p.peek().a(a);
        }
    }
    
    private static void a(final com.applovin.exoplayer2.e.g.a.a a, final SparseArray<b> sparseArray, final boolean b, final int n, final byte[] array) throws ai {
        for (int size = a.d.size(), i = 0; i < size; ++i) {
            final com.applovin.exoplayer2.e.g.a.a a2 = a.d.get(i);
            if (a2.a == 1953653094) {
                b(a2, sparseArray, b, n, array);
            }
        }
    }
    
    private static void a(final com.applovin.exoplayer2.e.g.a.a a, final b b, final int n) throws ai {
        final List<com.applovin.exoplayer2.e.g.a.b> c = a.c;
        final int size = c.size();
        final int n2 = 0;
        int i = 0;
        int n3 = 0;
        int n4 = 0;
        while (i < size) {
            final com.applovin.exoplayer2.e.g.a.b b2 = c.get(i);
            int n5 = n3;
            int n6 = n4;
            if (b2.a == 1953658222) {
                final y b3 = b2.b;
                b3.d(12);
                final int w = b3.w();
                n5 = n3;
                n6 = n4;
                if (w > 0) {
                    n6 = n4 + w;
                    n5 = n3 + 1;
                }
            }
            ++i;
            n3 = n5;
            n4 = n6;
        }
        b.h = 0;
        b.g = 0;
        b.f = 0;
        b.b.a(n3, n4);
        int n7 = 0;
        int n8 = 0;
        int n9;
        int a2;
        for (int j = n2; j < size; ++j, n7 = n9, n8 = a2) {
            final com.applovin.exoplayer2.e.g.a.b b4 = c.get(j);
            n9 = n7;
            a2 = n8;
            if (b4.a == 1953658222) {
                a2 = a(b, n7, n, b4.b, n8);
                n9 = n7 + 1;
            }
        }
    }
    
    private static void a(final com.applovin.exoplayer2.e.g.a.a a, @Nullable final String s, final m m) throws ai {
        final byte[] array = null;
        y y = null;
        y y2 = null;
        y y3;
        y y4;
        for (int i = 0; i < a.c.size(); ++i, y = y3, y2 = y4) {
            final com.applovin.exoplayer2.e.g.a.b b = a.c.get(i);
            final y b2 = b.b;
            final int a2 = b.a;
            if (a2 == 1935828848) {
                b2.d(12);
                y3 = y;
                y4 = y2;
                if (b2.q() == 1936025959) {
                    y3 = b2;
                    y4 = y2;
                }
            }
            else {
                y3 = y;
                y4 = y2;
                if (a2 == 1936158820) {
                    b2.d(12);
                    y3 = y;
                    y4 = y2;
                    if (b2.q() == 1936025959) {
                        y4 = b2;
                        y3 = y;
                    }
                }
            }
        }
        if (y == null || y2 == null) {
            return;
        }
        y.d(8);
        final int a3 = a.a(y.q());
        y.e(4);
        if (a3 == 1) {
            y.e(4);
        }
        if (y.q() != 1) {
            throw ai.a("Entry count in sbgp != 1 (unsupported).");
        }
        y2.d(8);
        final int a4 = a.a(y2.q());
        y2.e(4);
        if (a4 == 1) {
            if (y2.o() == 0L) {
                throw ai.a("Variable length description in sgpd found (unsupported)");
            }
        }
        else if (a4 >= 2) {
            y2.e(4);
        }
        if (y2.o() != 1L) {
            throw ai.a("Entry count in sgpd != 1 (unsupported).");
        }
        y2.e(1);
        final int h = y2.h();
        final boolean b3 = y2.h() == 1;
        if (!b3) {
            return;
        }
        final int h2 = y2.h();
        final byte[] array2 = new byte[16];
        y2.a(array2, 0, 16);
        byte[] array3 = array;
        if (h2 == 0) {
            final int h3 = y2.h();
            array3 = new byte[h3];
            y2.a(array3, 0, h3);
        }
        m.m = true;
        m.o = new com.applovin.exoplayer2.e.g.l(b3, s, h2, array2, (h & 0xF0) >> 4, h & 0xF, array3);
    }
    
    private void a(final com.applovin.exoplayer2.e.g.a.b b, final long n) throws ai {
        if (!this.p.isEmpty()) {
            this.p.peek().a(b);
        }
        else {
            final int a = b.a;
            if (a == 1936286840) {
                final Pair<Long, com.applovin.exoplayer2.e.c> a2 = a(b.b, n);
                this.B = (long)a2.first;
                this.H.a((com.applovin.exoplayer2.e.v)a2.second);
                this.K = true;
            }
            else if (a == 1701671783) {
                this.a(b.b);
            }
        }
    }
    
    private static void a(final com.applovin.exoplayer2.e.g.l l, final y y, final m m) throws ai {
        final int d = l.d;
        y.d(8);
        final int b = com.applovin.exoplayer2.e.g.a.b(y.q());
        boolean val = true;
        if ((b & 0x1) == 0x1) {
            y.e(8);
        }
        final int h = y.h();
        final int w = y.w();
        if (w <= m.f) {
            int n4;
            if (h == 0) {
                final boolean[] n = m.n;
                int n2 = 0;
                int n3 = 0;
                while (true) {
                    n4 = n3;
                    if (n2 >= w) {
                        break;
                    }
                    final int h2 = y.h();
                    n3 += h2;
                    n[n2] = (h2 > d);
                    ++n2;
                }
            }
            else {
                if (h <= d) {
                    val = false;
                }
                n4 = h * w + 0;
                Arrays.fill(m.n, 0, w, val);
            }
            Arrays.fill(m.n, w, m.f, false);
            if (n4 > 0) {
                m.a(n4);
            }
            return;
        }
        final StringBuilder sb = new StringBuilder();
        sb.append("Saiz sample count ");
        sb.append(w);
        sb.append(" is greater than fragment sample count");
        sb.append(m.f);
        throw ai.b(sb.toString(), null);
    }
    
    private void a(final y y) {
        if (this.I.length == 0) {
            return;
        }
        y.d(8);
        final int a = com.applovin.exoplayer2.e.g.a.a(y.q());
        long d;
        long d2;
        long n;
        String s;
        String s2;
        long n2;
        if (a != 0) {
            if (a != 1) {
                final StringBuilder sb = new StringBuilder();
                sb.append("Skipping unsupported emsg version: ");
                sb.append(a);
                com.applovin.exoplayer2.l.q.c("FragmentedMp4Extractor", sb.toString());
                return;
            }
            final long o = y.o();
            d = com.applovin.exoplayer2.l.ai.d(y.y(), 1000000L, o);
            d2 = com.applovin.exoplayer2.l.ai.d(y.o(), 1000L, o);
            n = y.o();
            s = (String)com.applovin.exoplayer2.l.a.b((Object)y.B());
            s2 = (String)com.applovin.exoplayer2.l.a.b((Object)y.B());
            n2 = -9223372036854775807L;
        }
        else {
            s = (String)com.applovin.exoplayer2.l.a.b((Object)y.B());
            s2 = (String)com.applovin.exoplayer2.l.a.b((Object)y.B());
            final long o2 = y.o();
            final long d3 = com.applovin.exoplayer2.l.ai.d(y.o(), 1000000L, o2);
            final long b = this.B;
            if (b != -9223372036854775807L) {
                d = b + d3;
            }
            else {
                d = -9223372036854775807L;
            }
            final long d4 = com.applovin.exoplayer2.l.ai.d(y.o(), 1000L, o2);
            n = y.o();
            d2 = d4;
            n2 = d3;
        }
        final byte[] array = new byte[y.a()];
        final int a2 = y.a();
        final int n3 = 0;
        y.a(array, 0, a2);
        final y y2 = new y(this.n.a(new com.applovin.exoplayer2.g.b.a(s, s2, d2, n, array)));
        final int a3 = y2.a();
        for (final x x : this.I) {
            y2.d(0);
            x.a(y2, a3);
        }
        if (d == -9223372036854775807L) {
            this.q.addLast(new a(n2, a3));
            this.y += a3;
        }
        else {
            final ag m = this.m;
            long c = d;
            if (m != null) {
                c = m.c(d);
            }
            final x[] k = this.I;
            for (int length2 = k.length, l = n3; l < length2; ++l) {
                k[l].a(c, 1, a3, 0, null);
            }
        }
    }
    
    private static void a(final y y, int n, final m m) throws ai {
        y.d(n + 8);
        n = com.applovin.exoplayer2.e.g.a.b(y.q());
        if ((n & 0x1) != 0x0) {
            throw ai.a("Overriding TrackEncryptionBox parameters is unsupported.");
        }
        final boolean val = (n & 0x2) != 0x0;
        n = y.w();
        if (n == 0) {
            Arrays.fill(m.n, 0, m.f, false);
            return;
        }
        if (n == m.f) {
            Arrays.fill(m.n, 0, n, val);
            m.a(y.a());
            m.a(y);
            return;
        }
        final StringBuilder sb = new StringBuilder();
        sb.append("Senc sample count ");
        sb.append(n);
        sb.append(" is different from fragment sample count");
        sb.append(m.f);
        throw ai.b(sb.toString(), null);
    }
    
    private static void a(final y y, final m m) throws ai {
        y.d(8);
        final int q = y.q();
        if ((com.applovin.exoplayer2.e.g.a.b(q) & 0x1) == 0x1) {
            y.e(8);
        }
        final int w = y.w();
        if (w == 1) {
            final int a = com.applovin.exoplayer2.e.g.a.a(q);
            final long d = m.d;
            long n;
            if (a == 0) {
                n = y.o();
            }
            else {
                n = y.y();
            }
            m.d = d + n;
            return;
        }
        final StringBuilder sb = new StringBuilder();
        sb.append("Unexpected saio entry count: ");
        sb.append(w);
        throw ai.b(sb.toString(), null);
    }
    
    private static void a(final y y, final m m, final byte[] a) throws ai {
        y.d(8);
        y.a(a, 0, 16);
        if (!Arrays.equals(a, e.b)) {
            return;
        }
        a(y, 16, m);
    }
    
    private static Pair<Integer, com.applovin.exoplayer2.e.g.c> b(final y y) {
        y.d(12);
        return (Pair<Integer, com.applovin.exoplayer2.e.g.c>)Pair.create((Object)y.q(), (Object)new com.applovin.exoplayer2.e.g.c(y.q() - 1, y.q(), y.q(), y.q()));
    }
    
    private void b() {
        final x[] i = new x[2];
        this.I = i;
        final x r = this.r;
        final int n = 0;
        int n2;
        if (r != null) {
            i[0] = r;
            n2 = 1;
        }
        else {
            n2 = 0;
        }
        final int d = this.d;
        int n3 = 100;
        int n4 = n2;
        if ((d & 0x4) != 0x0) {
            i[n2] = this.H.a(100, 5);
            n3 = 101;
            n4 = n2 + 1;
        }
        final x[] j = (x[])com.applovin.exoplayer2.l.ai.a((Object[])this.I, n4);
        this.I = j;
        for (int length = j.length, k = 0; k < length; ++k) {
            j[k].a(com.applovin.exoplayer2.e.g.e.c);
        }
        this.J = new x[this.f.size()];
        for (int l = n; l < this.J.length; ++l, ++n3) {
            final x a = this.H.a(n3, 3);
            a.a(this.f.get(l));
            this.J[l] = a;
        }
    }
    
    private void b(final long n) {
        while (!this.q.isEmpty()) {
            final a a = this.q.removeFirst();
            this.y -= a.b;
            final long n2 = a.a + n;
            final ag m = this.m;
            long c = n2;
            if (m != null) {
                c = m.c(n2);
            }
            final x[] i = this.I;
            for (int length = i.length, j = 0; j < length; ++j) {
                i[j].a(c, 1, a.b, this.y, null);
            }
        }
    }
    
    private void b(final com.applovin.exoplayer2.e.g.a.a a) throws ai {
        final k e = this.e;
        final boolean b = true;
        final int n = 0;
        final int n2 = 0;
        com.applovin.exoplayer2.l.a.b(e == null, (Object)"Unexpected moov box.");
        final com.applovin.exoplayer2.d.e a2 = a(a.c);
        final com.applovin.exoplayer2.e.g.a.a a3 = (com.applovin.exoplayer2.e.g.a.a)com.applovin.exoplayer2.l.a.b((Object)a.e(1836475768));
        final SparseArray sparseArray = new SparseArray();
        final int size = a3.c.size();
        long c = -9223372036854775807L;
        for (int i = 0; i < size; ++i) {
            final com.applovin.exoplayer2.e.g.a.b b2 = a3.c.get(i);
            final int a4 = b2.a;
            if (a4 == 1953654136) {
                final Pair<Integer, com.applovin.exoplayer2.e.g.c> b3 = b(b2.b);
                sparseArray.put((int)b3.first, (Object)b3.second);
            }
            else if (a4 == 1835362404) {
                c = c(b2.b);
            }
        }
        final List<n> a5 = com.applovin.exoplayer2.e.g.b.a(a, new r(), c, a2, (this.d & 0x10) != 0x0, false, new \u3007080(this));
        final int size2 = a5.size();
        if (this.g.size() == 0) {
            for (int j = n2; j < size2; ++j) {
                final n n3 = a5.get(j);
                final k a6 = n3.a;
                this.g.put(a6.a, (Object)new b(this.H.a(j, a6.b), n3, this.a((SparseArray<com.applovin.exoplayer2.e.g.c>)sparseArray, a6.a)));
                this.A = Math.max(this.A, a6.e);
            }
            this.H.a();
        }
        else {
            com.applovin.exoplayer2.l.a.b(this.g.size() == size2 && b);
            for (int k = n; k < size2; ++k) {
                final n n4 = a5.get(k);
                final k a7 = n4.a;
                ((b)this.g.get(a7.a)).a(n4, this.a((SparseArray<com.applovin.exoplayer2.e.g.c>)sparseArray, a7.a));
            }
        }
    }
    
    private static void b(final com.applovin.exoplayer2.e.g.a.a a, final SparseArray<b> sparseArray, final boolean b, int i, final byte[] array) throws ai {
        final b a2 = a(((com.applovin.exoplayer2.e.g.a.b)com.applovin.exoplayer2.l.a.b((Object)a.d(1952868452))).b, sparseArray, b);
        if (a2 == null) {
            return;
        }
        final m b2 = a2.b;
        final long r = b2.r;
        final boolean s = b2.s;
        a2.a();
        a2.l = true;
        final com.applovin.exoplayer2.e.g.a.b d = a.d(1952867444);
        if (d != null && (i & 0x2) == 0x0) {
            b2.r = d(d.b);
            b2.s = true;
        }
        else {
            b2.r = r;
            b2.s = s;
        }
        a(a, a2, i);
        final com.applovin.exoplayer2.e.g.l a3 = a2.d.a.a(((com.applovin.exoplayer2.e.g.c)com.applovin.exoplayer2.l.a.b((Object)b2.a)).a);
        final com.applovin.exoplayer2.e.g.a.b d2 = a.d(1935763834);
        if (d2 != null) {
            a((com.applovin.exoplayer2.e.g.l)com.applovin.exoplayer2.l.a.b((Object)a3), d2.b, b2);
        }
        final com.applovin.exoplayer2.e.g.a.b d3 = a.d(1935763823);
        if (d3 != null) {
            a(d3.b, b2);
        }
        final com.applovin.exoplayer2.e.g.a.b d4 = a.d(1936027235);
        if (d4 != null) {
            b(d4.b, b2);
        }
        String b3;
        if (a3 != null) {
            b3 = a3.b;
        }
        else {
            b3 = null;
        }
        a(a, b3, b2);
        int size;
        com.applovin.exoplayer2.e.g.a.b b4;
        for (size = a.c.size(), i = 0; i < size; ++i) {
            b4 = a.c.get(i);
            if (b4.a == 1970628964) {
                a(b4.b, b2, array);
            }
        }
    }
    
    private static void b(final y y, final m m) throws ai {
        a(y, 0, m);
    }
    
    private static boolean b(final int n) {
        return n == 1751411826 || n == 1835296868 || n == 1836476516 || n == 1936286840 || n == 1937011556 || n == 1937011827 || n == 1668576371 || n == 1937011555 || n == 1937011578 || n == 1937013298 || n == 1937007471 || n == 1668232756 || n == 1937011571 || n == 1952867444 || n == 1952868452 || n == 1953196132 || n == 1953654136 || n == 1953658222 || n == 1886614376 || n == 1935763834 || n == 1935763823 || n == 1936027235 || n == 1970628964 || n == 1935828848 || n == 1936158820 || n == 1701606260 || n == 1835362404 || n == 1701671783;
    }
    
    private boolean b(final i i) throws IOException {
        if (this.v == 0) {
            if (!i.a(this.o.d(), 0, 8, true)) {
                return false;
            }
            this.v = 8;
            this.o.d(0);
            this.u = this.o.o();
            this.t = this.o.q();
        }
        final long u = this.u;
        if (u == 1L) {
            i.b(this.o.d(), 8, 8);
            this.v += 8;
            this.u = this.o.y();
        }
        else if (u == 0L) {
            long n2;
            final long n = n2 = i.d();
            if (n == -1L) {
                n2 = n;
                if (!this.p.isEmpty()) {
                    n2 = this.p.peek().b;
                }
            }
            if (n2 != -1L) {
                this.u = n2 - i.c() + this.v;
            }
        }
        if (this.u < this.v) {
            throw ai.a("Atom size less than header length (unsupported).");
        }
        final long c = i.c() - this.v;
        final int t = this.t;
        if ((t == 1836019558 || t == 1835295092) && !this.K) {
            this.H.a(new com.applovin.exoplayer2.e.v.b(this.A, c));
            this.K = true;
        }
        if (this.t == 1836019558) {
            for (int size = this.g.size(), j = 0; j < size; ++j) {
                final m b = ((b)this.g.valueAt(j)).b;
                b.b = c;
                b.d = c;
                b.c = c;
            }
        }
        final int t2 = this.t;
        if (t2 == 1835295092) {
            this.C = null;
            this.x = c + this.u;
            this.s = 2;
            return true;
        }
        if (c(t2)) {
            final long n3 = i.c() + this.u - 8L;
            this.p.push(new com.applovin.exoplayer2.e.g.a.a(this.t, n3));
            if (this.u == this.v) {
                this.a(n3);
            }
            else {
                this.a();
            }
        }
        else if (b(this.t)) {
            if (this.v != 8) {
                throw ai.a("Leaf atom defines extended atom size (unsupported).");
            }
            final long u2 = this.u;
            if (u2 > 2147483647L) {
                throw ai.a("Leaf atom with length > 2147483647 (unsupported).");
            }
            final y w = new y((int)u2);
            System.arraycopy(this.o.d(), 0, w.d(), 0, 8);
            this.w = w;
            this.s = 1;
        }
        else {
            if (this.u > 2147483647L) {
                throw ai.a("Skipping atom with length > 2147483647 (unsupported).");
            }
            this.w = null;
            this.s = 1;
        }
        return true;
    }
    
    private static long c(final y y) {
        y.d(8);
        long n;
        if (com.applovin.exoplayer2.e.g.a.a(y.q()) == 0) {
            n = y.o();
        }
        else {
            n = y.y();
        }
        return n;
    }
    
    private void c(final com.applovin.exoplayer2.e.g.a.a a) throws ai {
        final SparseArray<b> g = this.g;
        final k e = this.e;
        final int n = 0;
        a(a, g, e != null, this.d, this.k);
        final com.applovin.exoplayer2.d.e a2 = a(a.c);
        if (a2 != null) {
            for (int size = this.g.size(), i = 0; i < size; ++i) {
                ((b)this.g.valueAt(i)).a(a2);
            }
        }
        if (this.z != -9223372036854775807L) {
            for (int size2 = this.g.size(), j = n; j < size2; ++j) {
                ((b)this.g.valueAt(j)).a(this.z);
            }
            this.z = -9223372036854775807L;
        }
    }
    
    private void c(final i i) throws IOException {
        final int n = (int)this.u - this.v;
        final y w = this.w;
        if (w != null) {
            i.b(w.d(), 8, n);
            this.a(new com.applovin.exoplayer2.e.g.a.b(this.t, w), i.c());
        }
        else {
            i.b(n);
        }
        this.a(i.c());
    }
    
    private static boolean c(final int n) {
        return n == 1836019574 || n == 1953653099 || n == 1835297121 || n == 1835626086 || n == 1937007212 || n == 1836019558 || n == 1953653094 || n == 1836475768 || n == 1701082227;
    }
    
    private static long d(final y y) {
        y.d(8);
        long n;
        if (com.applovin.exoplayer2.e.g.a.a(y.q()) == 1) {
            n = y.y();
        }
        else {
            n = y.o();
        }
        return n;
    }
    
    private void d(final i i) throws IOException {
        final int size = this.g.size();
        long n = Long.MAX_VALUE;
        int j = 0;
        b b = null;
        while (j < size) {
            final m b2 = ((b)this.g.valueAt(j)).b;
            long n2 = n;
            b b3 = b;
            if (b2.q) {
                final long d = b2.d;
                n2 = n;
                b3 = b;
                if (d < n) {
                    b3 = (b)this.g.valueAt(j);
                    n2 = d;
                }
            }
            ++j;
            n = n2;
            b = b3;
        }
        if (b == null) {
            this.s = 3;
            return;
        }
        final int n3 = (int)(n - i.c());
        if (n3 >= 0) {
            i.b(n3);
            b.b.a(i);
            return;
        }
        throw ai.b("Offset to encryption data was negative.", null);
    }
    
    private boolean e(final i i) throws IOException {
        b c;
        if ((c = this.C) == null) {
            c = a(this.g);
            if (c == null) {
                final int n = (int)(this.x - i.c());
                if (n >= 0) {
                    i.b(n);
                    this.a();
                    return false;
                }
                throw ai.b("Offset to end of mdat was negative.", null);
            }
            else {
                int n2;
                if ((n2 = (int)(c.c() - i.c())) < 0) {
                    com.applovin.exoplayer2.l.q.c("FragmentedMp4Extractor", "Ignoring negative offset to sample data.");
                    n2 = 0;
                }
                i.b(n2);
                this.C = c;
            }
        }
        if (this.s == 3) {
            final int d = c.d();
            this.D = d;
            if (c.f < c.i) {
                i.b(d);
                c.g();
                if (!c.f()) {
                    this.C = null;
                }
                this.s = 3;
                return true;
            }
            if (c.d.a.g == 1) {
                this.D = d - 8;
                i.b(8);
            }
            if ("audio/ac4".equals(c.d.a.f.l)) {
                this.E = c.a(this.D, 7);
                com.applovin.exoplayer2.b.c.a(this.D, this.l);
                c.a.a(this.l, 7);
                this.E += 7;
            }
            else {
                this.E = c.a(this.D, 0);
            }
            this.D += this.E;
            this.s = 4;
            this.F = 0;
        }
        final k a = c.d.a;
        final x a2 = c.a;
        final long b = c.b();
        final ag m = this.m;
        long c2 = b;
        if (m != null) {
            c2 = m.c(b);
        }
        if (a.j != 0) {
            final byte[] d2 = this.i.d();
            d2[0] = 0;
            d2[2] = (d2[1] = 0);
            final int j = a.j;
            final int n3 = 4 - j;
            while (this.E < this.D) {
                final int f = this.F;
                if (f == 0) {
                    i.b(d2, n3, j + 1);
                    this.i.d(0);
                    final int q = this.i.q();
                    if (q < 1) {
                        throw ai.b("Invalid NAL length", null);
                    }
                    this.F = q - 1;
                    this.h.d(0);
                    a2.a(this.h, 4);
                    a2.a(this.i, 1);
                    this.G = (this.J.length > 0 && com.applovin.exoplayer2.l.v.a(a.f.l, d2[4]));
                    this.E += 5;
                    this.D += n3;
                }
                else {
                    int n4;
                    if (this.G) {
                        this.j.a(f);
                        i.b(this.j.d(), 0, this.F);
                        a2.a(this.j, this.F);
                        n4 = this.F;
                        final int a3 = com.applovin.exoplayer2.l.v.a(this.j.d(), this.j.b());
                        this.j.d((int)("video/hevc".equals(a.f.l) ? 1 : 0));
                        this.j.c(a3);
                        com.applovin.exoplayer2.e.b.a(c2, this.j, this.J);
                    }
                    else {
                        n4 = a2.a((g)i, f, false);
                    }
                    this.E += n4;
                    this.F -= n4;
                }
            }
        }
        else {
            while (true) {
                final int e = this.E;
                final int d3 = this.D;
                if (e >= d3) {
                    break;
                }
                this.E += a2.a((g)i, d3 - e, false);
            }
        }
        final int e2 = c.e();
        final com.applovin.exoplayer2.e.g.l h = c.h();
        Object c3;
        if (h != null) {
            c3 = h.c;
        }
        else {
            c3 = null;
        }
        a2.a(c2, e2, this.D, 0, (x.a)c3);
        this.b(c2);
        if (!c.f()) {
            this.C = null;
        }
        this.s = 3;
        return true;
    }
    
    @Override
    public int a(final i i, final u u) throws IOException {
        while (true) {
            final int s = this.s;
            if (s != 0) {
                if (s != 1) {
                    if (s != 2) {
                        if (this.e(i)) {
                            return 0;
                        }
                        continue;
                    }
                    else {
                        this.d(i);
                    }
                }
                else {
                    this.c(i);
                }
            }
            else {
                if (!this.b(i)) {
                    return -1;
                }
                continue;
            }
        }
    }
    
    @Nullable
    protected k a(@Nullable final k k) {
        return k;
    }
    
    @Override
    public void a(final long n, final long z) {
        for (int size = this.g.size(), i = 0; i < size; ++i) {
            ((b)this.g.valueAt(i)).a();
        }
        this.q.clear();
        this.y = 0;
        this.z = z;
        this.p.clear();
        this.a();
    }
    
    @Override
    public void a(final j h) {
        this.H = h;
        this.a();
        this.b();
        final k e = this.e;
        if (e != null) {
            this.g.put(0, (Object)new b(h.a(0, e.b), new n(this.e, new long[0], new int[0], 0, new long[0], new int[0], 0L), new com.applovin.exoplayer2.e.g.c(0, 0, 0, 0)));
            this.H.a();
        }
    }
    
    @Override
    public boolean a(final i i) throws IOException {
        return com.applovin.exoplayer2.e.g.j.a(i);
    }
    
    @Override
    public void c() {
    }
    
    private static final class a
    {
        public final long a;
        public final int b;
        
        public a(final long a, final int b) {
            this.a = a;
            this.b = b;
        }
    }
    
    private static final class b
    {
        public final x a;
        public final m b;
        public final y c;
        public n d;
        public com.applovin.exoplayer2.e.g.c e;
        public int f;
        public int g;
        public int h;
        public int i;
        private final y j;
        private final y k;
        private boolean l;
        
        public b(final x a, final n d, final com.applovin.exoplayer2.e.g.c e) {
            this.a = a;
            this.d = d;
            this.e = e;
            this.b = new m();
            this.c = new y();
            this.j = new y(1);
            this.k = new y();
            this.a(d, e);
        }
        
        public int a(int i, int n) {
            final com.applovin.exoplayer2.e.g.l h = this.h();
            if (h == null) {
                return 0;
            }
            int n2 = h.d;
            y y;
            if (n2 != 0) {
                y = this.b.p;
            }
            else {
                final byte[] array = (byte[])com.applovin.exoplayer2.l.ai.a((Object)h.e);
                this.k.a(array, array.length);
                y = this.k;
                n2 = array.length;
            }
            final boolean c = this.b.c(this.f);
            final boolean b = c || n != 0;
            final byte[] d = this.j.d();
            int n3;
            if (b) {
                n3 = 128;
            }
            else {
                n3 = 0;
            }
            d[0] = (byte)(n3 | n2);
            this.j.d(0);
            this.a.a(this.j, 1, 1);
            this.a.a(y, n2, 1);
            if (!b) {
                return n2 + 1;
            }
            if (!c) {
                this.c.a(8);
                final byte[] d2 = this.c.d();
                d2[0] = 0;
                d2[1] = 1;
                d2[2] = (byte)(n >> 8 & 0xFF);
                d2[3] = (byte)(n & 0xFF);
                d2[4] = (byte)(i >> 24 & 0xFF);
                d2[5] = (byte)(i >> 16 & 0xFF);
                d2[6] = (byte)(i >> 8 & 0xFF);
                d2[7] = (byte)(i & 0xFF);
                this.a.a(this.c, 8, 1);
                return n2 + 1 + 8;
            }
            final y p2 = this.b.p;
            i = p2.i();
            p2.e(-2);
            i = i * 6 + 2;
            y c2 = p2;
            if (n != 0) {
                this.c.a(i);
                final byte[] d3 = this.c.d();
                p2.a(d3, 0, i);
                n += ((d3[2] & 0xFF) << 8 | (d3[3] & 0xFF));
                d3[2] = (byte)(n >> 8 & 0xFF);
                d3[3] = (byte)(n & 0xFF);
                c2 = this.c;
            }
            this.a.a(c2, i, 1);
            return n2 + 1 + i;
        }
        
        public void a() {
            this.b.a();
            this.f = 0;
            this.h = 0;
            this.g = 0;
            this.i = 0;
            this.l = false;
        }
        
        public void a(final long n) {
            int f = this.f;
            while (true) {
                final m b = this.b;
                if (f >= b.f || b.b(f) >= n) {
                    break;
                }
                if (this.b.l[f]) {
                    this.i = f;
                }
                ++f;
            }
        }
        
        public void a(com.applovin.exoplayer2.d.e a) {
            final com.applovin.exoplayer2.e.g.l a2 = this.d.a.a(((com.applovin.exoplayer2.e.g.c)com.applovin.exoplayer2.l.ai.a((Object)this.b.a)).a);
            String b;
            if (a2 != null) {
                b = a2.b;
            }
            else {
                b = null;
            }
            a = a.a(b);
            this.a.a(this.d.a.f.a().a(a).a());
        }
        
        public void a(final n d, final com.applovin.exoplayer2.e.g.c e) {
            this.d = d;
            this.e = e;
            this.a.a(d.a.f);
            this.a();
        }
        
        public long b() {
            long b;
            if (!this.l) {
                b = this.d.f[this.f];
            }
            else {
                b = this.b.b(this.f);
            }
            return b;
        }
        
        public long c() {
            long n;
            if (!this.l) {
                n = this.d.c[this.f];
            }
            else {
                n = this.b.g[this.h];
            }
            return n;
        }
        
        public int d() {
            int n;
            if (!this.l) {
                n = this.d.d[this.f];
            }
            else {
                n = this.b.i[this.f];
            }
            return n;
        }
        
        public int e() {
            int n;
            if (!this.l) {
                n = this.d.g[this.f];
            }
            else if (this.b.l[this.f]) {
                n = 1;
            }
            else {
                n = 0;
            }
            int n2 = n;
            if (this.h() != null) {
                n2 = (n | 0x40000000);
            }
            return n2;
        }
        
        public boolean f() {
            ++this.f;
            if (!this.l) {
                return false;
            }
            final int g = this.g + 1;
            this.g = g;
            final int[] h = this.b.h;
            final int h2 = this.h;
            if (g == h[h2]) {
                this.h = h2 + 1;
                this.g = 0;
                return false;
            }
            return true;
        }
        
        public void g() {
            final com.applovin.exoplayer2.e.g.l h = this.h();
            if (h == null) {
                return;
            }
            final y p = this.b.p;
            final int d = h.d;
            if (d != 0) {
                p.e(d);
            }
            if (this.b.c(this.f)) {
                p.e(p.i() * 6);
            }
        }
        
        @Nullable
        public com.applovin.exoplayer2.e.g.l h() {
            final boolean l = this.l;
            final com.applovin.exoplayer2.e.g.l i = null;
            if (!l) {
                return null;
            }
            final int a = ((com.applovin.exoplayer2.e.g.c)com.applovin.exoplayer2.l.ai.a((Object)this.b.a)).a;
            com.applovin.exoplayer2.e.g.l j = this.b.o;
            if (j == null) {
                j = this.d.a.a(a);
            }
            com.applovin.exoplayer2.e.g.l k = i;
            if (j != null) {
                k = i;
                if (j.a) {
                    k = j;
                }
            }
            return k;
        }
    }
}

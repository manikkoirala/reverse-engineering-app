// 
// Decompiled by Procyon v0.6.0
// 

package com.applovin.exoplayer2.e.g;

import com.applovin.exoplayer2.l.y;
import java.io.IOException;
import com.applovin.exoplayer2.e.i;

final class j
{
    private static final int[] a;
    
    static {
        a = new int[] { 1769172845, 1769172786, 1769172787, 1769172788, 1769172789, 1769172790, 1769172793, 1635148593, 1752589105, 1751479857, 1635135537, 1836069937, 1836069938, 862401121, 862401122, 862417462, 862417718, 862414134, 862414646, 1295275552, 1295270176, 1714714144, 1801741417, 1295275600, 1903435808, 1297305174, 1684175153, 1769172332, 1885955686 };
    }
    
    private static boolean a(final int n, final boolean b) {
        if (n >>> 8 == 3368816) {
            return true;
        }
        if (n == 1751476579 && b) {
            return true;
        }
        final int[] a = j.a;
        for (int length = a.length, i = 0; i < length; ++i) {
            if (a[i] == n) {
                return true;
            }
        }
        return false;
    }
    
    public static boolean a(final i i) throws IOException {
        return a(i, true, false);
    }
    
    public static boolean a(final i i, final boolean b) throws IOException {
        return a(i, false, b);
    }
    
    private static boolean a(final i i, final boolean b, final boolean b2) throws IOException {
        final long d = i.d();
        final long n = 4096L;
        final long n2 = lcmp(d, -1L);
        long n3 = n;
        if (n2 != 0) {
            if (d > 4096L) {
                n3 = n;
            }
            else {
                n3 = d;
            }
        }
        int n4 = (int)n3;
        final y y = new y(64);
        int j = 0;
        int n5 = 0;
        while (true) {
            while (j < n4) {
                y.a(8);
                if (!i.b(y.d(), 0, 8, true)) {
                    break;
                }
                final long o = y.o();
                final int q = y.q();
                long s;
                int n6;
                if (o == 1L) {
                    i.d(y.d(), 8, 8);
                    y.c(16);
                    s = y.s();
                    n6 = 16;
                }
                else {
                    s = o;
                    if (o == 0L) {
                        final long d2 = i.d();
                        s = o;
                        if (d2 != -1L) {
                            s = d2 - i.b() + 8;
                        }
                    }
                    n6 = 8;
                }
                final long n7 = n6;
                if (s < n7) {
                    return false;
                }
                final int n8 = j + n6;
                if (q == 1836019574) {
                    int n10;
                    final int n9 = n10 = n4 + (int)s;
                    if (n2 != 0) {
                        n10 = n9;
                        if (n9 > d) {
                            n10 = (int)d;
                        }
                    }
                    n4 = n10;
                    j = n8;
                }
                else {
                    if (q == 1836019558 || q == 1836475768) {
                        final boolean b3 = true;
                        final boolean b4 = true;
                        return n5 != 0 && b == b3 && b4;
                    }
                    if (n8 + s - n7 >= n4) {
                        break;
                    }
                    final int n11 = (int)(s - n7);
                    final int n12 = n8 + n11;
                    int n14;
                    if (q == 1718909296) {
                        if (n11 < 8) {
                            return false;
                        }
                        y.a(n11);
                        i.d(y.d(), 0, n11);
                        for (int n13 = n11 / 4, k = 0; k < n13; ++k) {
                            if (k == 1) {
                                y.e(4);
                            }
                            else if (a(y.q(), b2)) {
                                n5 = 1;
                                break;
                            }
                        }
                        if (n5 == 0) {
                            return false;
                        }
                        n14 = n5;
                    }
                    else {
                        n14 = n5;
                        if (n11 != 0) {
                            i.c(n11);
                            n14 = n5;
                        }
                    }
                    j = n12;
                    n5 = n14;
                }
            }
            final boolean b3 = false;
            continue;
        }
    }
}

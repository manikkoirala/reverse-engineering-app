// 
// Decompiled by Procyon v0.6.0
// 

package com.applovin.exoplayer2.e.g;

import com.applovin.exoplayer2.v;
import com.applovin.exoplayer2.e.r;
import com.applovin.exoplayer2.g.e.i;
import androidx.annotation.Nullable;
import com.applovin.exoplayer2.g.e.h;
import com.applovin.exoplayer2.g.e.e;
import com.applovin.exoplayer2.g.e.l;
import com.applovin.exoplayer2.l.q;
import com.applovin.exoplayer2.g.a;
import com.applovin.exoplayer2.l.y;
import androidx.annotation.VisibleForTesting;

final class f
{
    @VisibleForTesting
    static final String[] a;
    
    static {
        a = new String[] { "Blues", "Classic Rock", "Country", "Dance", "Disco", "Funk", "Grunge", "Hip-Hop", "Jazz", "Metal", "New Age", "Oldies", "Other", "Pop", "R&B", "Rap", "Reggae", "Rock", "Techno", "Industrial", "Alternative", "Ska", "Death Metal", "Pranks", "Soundtrack", "Euro-Techno", "Ambient", "Trip-Hop", "Vocal", "Jazz+Funk", "Fusion", "Trance", "Classical", "Instrumental", "Acid", "House", "Game", "Sound Clip", "Gospel", "Noise", "AlternRock", "Bass", "Soul", "Punk", "Space", "Meditative", "Instrumental Pop", "Instrumental Rock", "Ethnic", "Gothic", "Darkwave", "Techno-Industrial", "Electronic", "Pop-Folk", "Eurodance", "Dream", "Southern Rock", "Comedy", "Cult", "Gangsta", "Top 40", "Christian Rap", "Pop/Funk", "Jungle", "Native American", "Cabaret", "New Wave", "Psychadelic", "Rave", "Showtunes", "Trailer", "Lo-Fi", "Tribal", "Acid Punk", "Acid Jazz", "Polka", "Retro", "Musical", "Rock & Roll", "Hard Rock", "Folk", "Folk-Rock", "National Folk", "Swing", "Fast Fusion", "Bebob", "Latin", "Revival", "Celtic", "Bluegrass", "Avantgarde", "Gothic Rock", "Progressive Rock", "Psychedelic Rock", "Symphonic Rock", "Slow Rock", "Big Band", "Chorus", "Easy Listening", "Acoustic", "Humour", "Speech", "Chanson", "Opera", "Chamber Music", "Sonata", "Symphony", "Booty Bass", "Primus", "Porn Groove", "Satire", "Slow Jam", "Club", "Tango", "Samba", "Folklore", "Ballad", "Power Ballad", "Rhythmic Soul", "Freestyle", "Duet", "Punk Rock", "Drum Solo", "A capella", "Euro-House", "Dance Hall", "Goa", "Drum & Bass", "Club-House", "Hardcore", "Terror", "Indie", "BritPop", "Afro-Punk", "Polsk Punk", "Beat", "Christian Gangsta Rap", "Heavy Metal", "Black Metal", "Crossover", "Contemporary Christian", "Christian Rock", "Merengue", "Salsa", "Thrash Metal", "Anime", "Jpop", "Synthpop", "Abstract", "Art Rock", "Baroque", "Bhangra", "Big beat", "Breakbeat", "Chillout", "Downtempo", "Dub", "EBM", "Eclectic", "Electro", "Electroclash", "Emo", "Experimental", "Garage", "Global", "IDM", "Illbient", "Industro-Goth", "Jam Band", "Krautrock", "Leftfield", "Lounge", "Math Rock", "New Romantic", "Nu-Breakz", "Post-Punk", "Post-Rock", "Psytrance", "Shoegaze", "Space Rock", "Trop Rock", "World Music", "Neoclassical", "Audiobook", "Audio theatre", "Neue Deutsche Welle", "Podcast", "Indie-Rock", "G-Funk", "Dubstep", "Garage Rock", "Psybient" };
    }
    
    @Nullable
    public static a.a a(final y y) {
        final int n = y.c() + y.q();
        final int q = y.q();
        int n2 = q >> 24 & 0xFF;
        Label_0788: {
            Label_0463: {
                if (n2 == 169 || n2 == 253) {
                    break Label_0463;
                }
                Label_0062: {
                    if (q != 1735291493) {
                        break Label_0062;
                    }
                    try {
                        final l b = b(y);
                        y.d(n);
                        return b;
                        Label_0087: {
                            iftrue(Label_0112:)(q != 1953655662);
                        }
                        Block_28: {
                            Block_27: {
                                l a;
                                l a2;
                                l b2;
                                e a3;
                                l a4;
                                l a5;
                                h a6;
                                l b3;
                                l a7;
                                h a8;
                                com.applovin.exoplayer2.g.e.a c;
                                l a9;
                                l a10;
                                l a11;
                                h a12;
                                Block_21_Outer:Block_18_Outer:Block_23_Outer:
                                while (true) {
                                Block_31:
                                    while (true) {
                                        while (true) {
                                        Block_16_Outer:
                                            while (true) {
                                            Block_32_Outer:
                                                while (true) {
                                                Block_22_Outer:
                                                    while (true) {
                                                        Block_24: {
                                                            Block_30: {
                                                            Block_19_Outer:
                                                                while (true) {
                                                                    while (true) {
                                                                        Block_29: {
                                                                            Block_17: {
                                                                                break Block_17;
                                                                                a = a(q, "TSOC", y);
                                                                                y.d(n);
                                                                                return a;
                                                                                Label_0391:
                                                                                iftrue(Label_0416:)(q != 1936683886);
                                                                                break Block_29;
                                                                                a2 = a(q, "TPE2", y);
                                                                                y.d(n);
                                                                                return a2;
                                                                                Label_0337:
                                                                                iftrue(Label_0364:)(q != 1920233063);
                                                                                break Block_27;
                                                                                b2 = b(q, "TPOS", y);
                                                                                y.d(n);
                                                                                return b2;
                                                                                a3 = a(q, y);
                                                                                y.d(n);
                                                                                return a3;
                                                                                Label_0364:
                                                                                iftrue(Label_0391:)(q != 1885823344);
                                                                                break Block_28;
                                                                                a4 = a(q, "TSOT", y);
                                                                                y.d(n);
                                                                                return a4;
                                                                                Label_0287:
                                                                                iftrue(Label_0312:)(q != 1936679265);
                                                                                a5 = a(q, "TSOP", y);
                                                                                y.d(n);
                                                                                return a5;
                                                                                a6 = a(q, "TCMP", y, true, true);
                                                                                y.d(n);
                                                                                return a6;
                                                                                Label_0416:
                                                                                iftrue(Label_0441:)(q != 1953919848);
                                                                                break Block_30;
                                                                            }
                                                                            b3 = b(q, "TRCK", y);
                                                                            y.d(n);
                                                                            return b3;
                                                                        }
                                                                        a7 = a(q, "TVSHOWSORT", y);
                                                                        y.d(n);
                                                                        return a7;
                                                                        a8 = a(q, "TBPM", y, true, false);
                                                                        y.d(n);
                                                                        return a8;
                                                                        while (true) {
                                                                            c = c(y);
                                                                            y.d(n);
                                                                            return c;
                                                                            Label_0166:
                                                                            iftrue(Label_0187:)(q != 1668249202);
                                                                            continue Block_23_Outer;
                                                                        }
                                                                        a9 = a(q, "TSO2", y);
                                                                        y.d(n);
                                                                        return a9;
                                                                        Label_0139:
                                                                        iftrue(Label_0166:)(q != 1668311404);
                                                                        continue Block_18_Outer;
                                                                    }
                                                                    Label_0212:
                                                                    iftrue(Label_0237:)(q != 1936682605);
                                                                    continue Block_19_Outer;
                                                                }
                                                                Label_0262:
                                                                iftrue(Label_0287:)(q != 1936679282);
                                                                break Block_24;
                                                            }
                                                            a10 = a(q, "TVSHOW", y);
                                                            y.d(n);
                                                            return a10;
                                                        }
                                                        a11 = a(q, "TSOA", y);
                                                        y.d(n);
                                                        return a11;
                                                        Label_0441:
                                                        iftrue(Label_0705:)(q != 757935405);
                                                        break Block_31;
                                                        n2 = (0xFFFFFF & q);
                                                        iftrue(Label_0496:)(n2 != 6516084);
                                                        continue Block_22_Outer;
                                                    }
                                                    iftrue(Label_0087:)(q != 1684632427);
                                                    continue Block_32_Outer;
                                                }
                                                Label_0187:
                                                iftrue(Label_0212:)(q != 1631670868);
                                                continue Block_16_Outer;
                                            }
                                            Label_0112:
                                            iftrue(Label_0139:)(q != 1953329263);
                                            continue Block_23_Outer;
                                        }
                                        Label_0237:
                                        iftrue(Label_0262:)(q != 1936679276);
                                        continue;
                                    }
                                    a12 = a(y, n);
                                    y.d(n);
                                    return a12;
                                    Label_0312:
                                    iftrue(Label_0337:)(q != 1936679791);
                                    continue Block_21_Outer;
                                }
                            }
                            final h a13 = a(q, "ITUNESADVISORY", y, false, false);
                            y.d(n);
                            return a13;
                        }
                        final h a14 = a(q, "ITUNESGAPLESS", y, false, true);
                        y.d(n);
                        return a14;
                    }
                    finally {
                        break Label_0788;
                    }
                }
            }
            Label_0496: {
                if (n2 == 7233901 || n2 == 7631467) {
                    final l a15 = a(q, "TIT2", y);
                    y.d(n);
                    return a15;
                }
            }
            if (n2 == 6516589 || n2 == 7828084) {
                final l a16 = a(q, "TCOM", y);
                y.d(n);
                return a16;
            }
            if (n2 == 6578553) {
                final l a17 = a(q, "TDRC", y);
                y.d(n);
                return a17;
            }
            if (n2 == 4280916) {
                final l a18 = a(q, "TPE1", y);
                y.d(n);
                return a18;
            }
            if (n2 == 7630703) {
                final l a19 = a(q, "TSSE", y);
                y.d(n);
                return a19;
            }
            if (n2 == 6384738) {
                final l a20 = a(q, "TALB", y);
                y.d(n);
                return a20;
            }
            if (n2 == 7108978) {
                final l a21 = a(q, "USLT", y);
                y.d(n);
                return a21;
            }
            if (n2 == 6776174) {
                final l a22 = a(q, "TCON", y);
                y.d(n);
                return a22;
            }
            if (n2 == 6779504) {
                final l a23 = a(q, "TIT1", y);
                y.d(n);
                return a23;
            }
            Label_0705:
            final StringBuilder sb = new StringBuilder();
            sb.append("Skipped unknown metadata entry: ");
            sb.append(com.applovin.exoplayer2.e.g.a.c(q));
            com.applovin.exoplayer2.l.q.a("MetadataUtil", sb.toString());
            y.d(n);
            return null;
        }
        y.d(n);
    }
    
    @Nullable
    private static e a(final int n, final y y) {
        final int q = y.q();
        if (y.q() == 1684108385) {
            y.e(8);
            final String g = y.g(q - 16);
            return new e("und", g, g);
        }
        final StringBuilder sb = new StringBuilder();
        sb.append("Failed to parse comment attribute: ");
        sb.append(com.applovin.exoplayer2.e.g.a.c(n));
        com.applovin.exoplayer2.l.q.c("MetadataUtil", sb.toString());
        return null;
    }
    
    @Nullable
    private static h a(final int n, final String s, final y y, final boolean b, final boolean b2) {
        int n2;
        final int b3 = n2 = d(y);
        if (b2) {
            n2 = Math.min(1, b3);
        }
        if (n2 >= 0) {
            h h;
            if (b) {
                h = new l(s, null, Integer.toString(n2));
            }
            else {
                h = new e("und", s, Integer.toString(n2));
            }
            return h;
        }
        final StringBuilder sb = new StringBuilder();
        sb.append("Failed to parse uint8 attribute: ");
        sb.append(com.applovin.exoplayer2.e.g.a.c(n));
        q.c("MetadataUtil", sb.toString());
        return null;
    }
    
    @Nullable
    private static h a(final y y, final int n) {
        String g = null;
        String g2 = null;
        int n2 = -1;
        int n3 = -1;
        while (y.c() < n) {
            final int c = y.c();
            final int q = y.q();
            final int q2 = y.q();
            y.e(4);
            if (q2 == 1835360622) {
                g = y.g(q - 12);
            }
            else if (q2 == 1851878757) {
                g2 = y.g(q - 12);
            }
            else {
                if (q2 == 1684108385) {
                    n2 = c;
                    n3 = q;
                }
                y.e(q - 12);
            }
        }
        if (g != null && g2 != null && n2 != -1) {
            y.d(n2);
            y.e(16);
            return new i(g, g2, y.g(n3 - 16));
        }
        return null;
    }
    
    @Nullable
    private static l a(final int n, final String s, final y y) {
        final int q = y.q();
        if (y.q() == 1684108385) {
            y.e(8);
            return new l(s, null, y.g(q - 16));
        }
        final StringBuilder sb = new StringBuilder();
        sb.append("Failed to parse text attribute: ");
        sb.append(com.applovin.exoplayer2.e.g.a.c(n));
        com.applovin.exoplayer2.l.q.c("MetadataUtil", sb.toString());
        return null;
    }
    
    @Nullable
    public static com.applovin.exoplayer2.g.f.a a(final y y, int q, final String s) {
        while (true) {
            final int c = y.c();
            if (c >= q) {
                return null;
            }
            int q2 = y.q();
            if (y.q() == 1684108385) {
                final int q3 = y.q();
                q = y.q();
                q2 -= 16;
                final byte[] array = new byte[q2];
                y.a(array, 0, q2);
                return new com.applovin.exoplayer2.g.f.a(s, array, q, q3);
            }
            y.d(c + q2);
        }
    }
    
    public static void a(final int n, final r r, final v.a a) {
        if (n == 1 && r.a()) {
            a.n(r.a).o(r.b);
        }
    }
    
    public static void a(int i, @Nullable a a, @Nullable final a a2, final v.a a3, final a... array) {
        final int n = 0;
        final a a4 = new a(new a.a[0]);
        Label_0106: {
            if (i == 1) {
                if (a != null) {
                    break Label_0106;
                }
            }
            else if (i == 2 && a2 != null) {
                a.a a5;
                com.applovin.exoplayer2.g.f.a a6;
                for (i = 0; i < a2.a(); ++i) {
                    a5 = a2.a(i);
                    if (a5 instanceof com.applovin.exoplayer2.g.f.a) {
                        a6 = (com.applovin.exoplayer2.g.f.a)a5;
                        if ("com.android.capture.fps".equals(a6.a)) {
                            a = new a(new a.a[] { a6 });
                            break Label_0106;
                        }
                    }
                }
            }
            a = a4;
        }
        int length;
        for (length = array.length, i = n; i < length; ++i) {
            a = a.a(array[i]);
        }
        if (a.a() > 0) {
            a3.a(a);
        }
    }
    
    @Nullable
    private static l b(int i, final String s, final y y) {
        final int q = y.q();
        if (y.q() == 1684108385 && q >= 22) {
            y.e(10);
            final int j = y.i();
            if (j > 0) {
                final StringBuilder sb = new StringBuilder();
                sb.append("");
                sb.append(j);
                final String string = sb.toString();
                i = y.i();
                String string2 = string;
                if (i > 0) {
                    final StringBuilder sb2 = new StringBuilder();
                    sb2.append(string);
                    sb2.append("/");
                    sb2.append(i);
                    string2 = sb2.toString();
                }
                return new l(s, null, string2);
            }
        }
        final StringBuilder sb3 = new StringBuilder();
        sb3.append("Failed to parse index/count attribute: ");
        sb3.append(com.applovin.exoplayer2.e.g.a.c(i));
        com.applovin.exoplayer2.l.q.c("MetadataUtil", sb3.toString());
        return null;
    }
    
    @Nullable
    private static l b(final y y) {
        final int d = d(y);
        String s = null;
        Label_0030: {
            if (d > 0) {
                final String[] a = f.a;
                if (d <= a.length) {
                    s = a[d - 1];
                    break Label_0030;
                }
            }
            s = null;
        }
        if (s != null) {
            return new l("TCON", null, s);
        }
        q.c("MetadataUtil", "Failed to parse standard genre code");
        return null;
    }
    
    @Nullable
    private static com.applovin.exoplayer2.g.e.a c(final y y) {
        int q = y.q();
        if (y.q() != 1684108385) {
            com.applovin.exoplayer2.l.q.c("MetadataUtil", "Failed to parse cover art attribute");
            return null;
        }
        final int b = com.applovin.exoplayer2.e.g.a.b(y.q());
        String s;
        if (b == 13) {
            s = "image/jpeg";
        }
        else if (b == 14) {
            s = "image/png";
        }
        else {
            s = null;
        }
        if (s == null) {
            final StringBuilder sb = new StringBuilder();
            sb.append("Unrecognized cover art flags: ");
            sb.append(b);
            com.applovin.exoplayer2.l.q.c("MetadataUtil", sb.toString());
            return null;
        }
        y.e(4);
        q -= 16;
        final byte[] array = new byte[q];
        y.a(array, 0, q);
        return new com.applovin.exoplayer2.g.e.a(s, null, 3, array);
    }
    
    private static int d(final y y) {
        y.e(4);
        if (y.q() == 1684108385) {
            y.e(8);
            return y.h();
        }
        q.c("MetadataUtil", "Failed to parse uint8 attribute value");
        return -1;
    }
}

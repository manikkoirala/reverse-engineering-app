// 
// Decompiled by Procyon v0.6.0
// 

package com.applovin.exoplayer2.e.g;

import com.applovin.exoplayer2.e.w;
import com.applovin.exoplayer2.e.x;
import com.applovin.exoplayer2.b.c;
import com.applovin.exoplayer2.e.u;
import java.io.IOException;
import com.applovin.exoplayer2.ai;
import android.util.Pair;
import com.applovin.exoplayer2.common.base.Function;
import com.applovin.exoplayer2.d.e;
import com.applovin.exoplayer2.e.r;
import java.util.ArrayList;
import com.applovin.exoplayer2.g.f.b;
import com.applovin.exoplayer2.e.j;
import androidx.annotation.Nullable;
import java.util.List;
import java.util.ArrayDeque;
import com.applovin.exoplayer2.l.y;
import com.applovin.exoplayer2.e.l;
import com.applovin.exoplayer2.e.v;
import com.applovin.exoplayer2.e.h;

public final class g implements h, v
{
    public static final l a;
    private final int b;
    private final y c;
    private final y d;
    private final y e;
    private final y f;
    private final ArrayDeque<com.applovin.exoplayer2.e.g.a.a> g;
    private final i h;
    private final List<com.applovin.exoplayer2.g.a.a> i;
    private int j;
    private int k;
    private long l;
    private int m;
    @Nullable
    private y n;
    private int o;
    private int p;
    private int q;
    private int r;
    private j s;
    private a[] t;
    private long[][] u;
    private int v;
    private long w;
    private int x;
    @Nullable
    private com.applovin.exoplayer2.g.f.b y;
    
    static {
        a = new \u3007o\u3007();
    }
    
    public g() {
        this(0);
    }
    
    public g(int n) {
        this.b = n;
        if ((n & 0x4) != 0x0) {
            n = 3;
        }
        else {
            n = 0;
        }
        this.j = n;
        this.h = new i();
        this.i = new ArrayList<com.applovin.exoplayer2.g.a.a>();
        this.f = new y(16);
        this.g = new ArrayDeque<com.applovin.exoplayer2.e.g.a.a>();
        this.c = new y(com.applovin.exoplayer2.l.v.a);
        this.d = new y(4);
        this.e = new y();
        this.o = -1;
    }
    
    private static int a(final int n) {
        if (n == 1751476579) {
            return 2;
        }
        if (n != 1903435808) {
            return 0;
        }
        return 1;
    }
    
    private static int a(final n n, final long n2) {
        int n3;
        if ((n3 = n.a(n2)) == -1) {
            n3 = n.b(n2);
        }
        return n3;
    }
    
    private static int a(final y y) {
        y.d(8);
        final int a = a(y.q());
        if (a != 0) {
            return a;
        }
        y.e(4);
        while (y.a() > 0) {
            final int a2 = a(y.q());
            if (a2 != 0) {
                return a2;
            }
        }
        return 0;
    }
    
    private static long a(final n n, final long n2, final long b) {
        final int a = a(n, n2);
        if (a == -1) {
            return b;
        }
        return Math.min(n.c[a], b);
    }
    
    private void a(final com.applovin.exoplayer2.e.g.a.a a) throws ai {
        final ArrayList list = new ArrayList();
        final boolean b = this.x == 1;
        final r r = new r();
        final com.applovin.exoplayer2.e.g.a.b d = a.d(1969517665);
        com.applovin.exoplayer2.g.a a3;
        com.applovin.exoplayer2.g.a a4;
        if (d != null) {
            final Pair<com.applovin.exoplayer2.g.a, com.applovin.exoplayer2.g.a> a2 = com.applovin.exoplayer2.e.g.b.a(d);
            a3 = (com.applovin.exoplayer2.g.a)a2.first;
            a4 = (com.applovin.exoplayer2.g.a)a2.second;
            if (a3 != null) {
                r.a(a3);
            }
        }
        else {
            a4 = null;
            a3 = null;
        }
        final com.applovin.exoplayer2.e.g.a.a e = a.e(1835365473);
        com.applovin.exoplayer2.g.a a5;
        if (e != null) {
            a5 = com.applovin.exoplayer2.e.g.b.a(e);
        }
        else {
            a5 = null;
        }
        final List<n> a6 = com.applovin.exoplayer2.e.g.b.a(a, r, -9223372036854775807L, null, (this.b & 0x1) != 0x0, b, new O8());
        final j j = (j)com.applovin.exoplayer2.l.a.b((Object)this.s);
        final int size = a6.size();
        int i = 0;
        int size2 = -1;
        long max = -9223372036854775807L;
        while (i < size) {
            final n n = a6.get(i);
            if (n.b != 0) {
                final k a7 = n.a;
                long b2 = a7.e;
                if (b2 == -9223372036854775807L) {
                    b2 = n.h;
                }
                max = Math.max(max, b2);
                final a a8 = new a(a7, n, j.a(i, a7.b));
                final int e2 = n.e;
                final com.applovin.exoplayer2.v.a a9 = a7.f.a();
                a9.f(e2 + 30);
                if (a7.b == 2 && b2 > 0L) {
                    final int b3 = n.b;
                    if (b3 > 1) {
                        a9.a(b3 / (b2 / 1000000.0f));
                    }
                }
                com.applovin.exoplayer2.e.g.f.a(a7.b, r, a9);
                final int b4 = a7.b;
                com.applovin.exoplayer2.g.a a10;
                if (this.i.isEmpty()) {
                    a10 = null;
                }
                else {
                    a10 = new com.applovin.exoplayer2.g.a((List<? extends com.applovin.exoplayer2.g.a.a>)this.i);
                }
                com.applovin.exoplayer2.e.g.f.a(b4, a3, a5, a9, a4, a10);
                a8.c.a(a9.a());
                final int b5 = a7.b;
                final int n2 = size2;
                if (b5 == 2) {
                    if ((size2 = n2) == -1) {
                        size2 = list.size();
                    }
                }
                else {
                    size2 = n2;
                }
                list.add(a8);
            }
            ++i;
        }
        this.v = size2;
        this.w = max;
        final a[] t = (a[])list.toArray(new a[0]);
        this.t = t;
        this.u = a(t);
        j.a();
        j.a(this);
    }
    
    private static long[][] a(final a[] array) {
        final long[][] array2 = new long[array.length][];
        final int[] array3 = new int[array.length];
        final long[] array4 = new long[array.length];
        final boolean[] array5 = new boolean[array.length];
        for (int i = 0; i < array.length; ++i) {
            array2[i] = new long[array[i].b.b];
            array4[i] = array[i].b.f[0];
        }
        long n = 0L;
        int j = 0;
        while (j < array.length) {
            long n2 = Long.MAX_VALUE;
            int n3 = -1;
            long n4;
            int n5;
            for (int k = 0; k < array.length; ++k, n2 = n4, n3 = n5) {
                n4 = n2;
                n5 = n3;
                if (!array5[k]) {
                    final long n6 = array4[k];
                    n4 = n2;
                    n5 = n3;
                    if (n6 <= n2) {
                        n5 = k;
                        n4 = n6;
                    }
                }
            }
            int n7 = array3[n3];
            final long[] array6 = array2[n3];
            array6[n7] = n;
            final n b = array[n3].b;
            n += b.d[n7];
            ++n7;
            if ((array3[n3] = n7) < array6.length) {
                array4[n3] = b.f[n7];
            }
            else {
                array5[n3] = true;
                ++j;
            }
        }
        return array2;
    }
    
    private void b(final long n) throws ai {
        while (!this.g.isEmpty() && this.g.peek().b == n) {
            final com.applovin.exoplayer2.e.g.a.a a = this.g.pop();
            if (a.a == 1836019574) {
                this.a(a);
                this.g.clear();
                this.j = 2;
            }
            else {
                if (this.g.isEmpty()) {
                    continue;
                }
                this.g.peek().a(a);
            }
        }
        if (this.j != 2) {
            this.d();
        }
    }
    
    private static boolean b(final int n) {
        return n == 1835296868 || n == 1836476516 || n == 1751411826 || n == 1937011556 || n == 1937011827 || n == 1937011571 || n == 1668576371 || n == 1701606260 || n == 1937011555 || n == 1937011578 || n == 1937013298 || n == 1937007471 || n == 1668232756 || n == 1953196132 || n == 1718909296 || n == 1969517665 || n == 1801812339 || n == 1768715124;
    }
    
    private boolean b(final com.applovin.exoplayer2.e.i i) throws IOException {
        if (this.m == 0) {
            if (!i.a(this.f.d(), 0, 8, true)) {
                this.e();
                return false;
            }
            this.m = 8;
            this.f.d(0);
            this.l = this.f.o();
            this.k = this.f.q();
        }
        final long l = this.l;
        if (l == 1L) {
            i.b(this.f.d(), 8, 8);
            this.m += 8;
            this.l = this.f.y();
        }
        else if (l == 0L) {
            long n2;
            final long n = n2 = i.d();
            if (n == -1L) {
                final com.applovin.exoplayer2.e.g.a.a a = this.g.peek();
                n2 = n;
                if (a != null) {
                    n2 = a.b;
                }
            }
            if (n2 != -1L) {
                this.l = n2 - i.c() + this.m;
            }
        }
        if (this.l >= this.m) {
            if (c(this.k)) {
                final long c = i.c();
                final long j = this.l;
                final int m = this.m;
                final long n3 = c + j - m;
                if (j != m && this.k == 1835365473) {
                    this.c(i);
                }
                this.g.push(new com.applovin.exoplayer2.e.g.a.a(this.k, n3));
                if (this.l == this.m) {
                    this.b(n3);
                }
                else {
                    this.d();
                }
            }
            else if (b(this.k)) {
                com.applovin.exoplayer2.l.a.b(this.m == 8);
                com.applovin.exoplayer2.l.a.b(this.l <= 2147483647L);
                final y n4 = new y((int)this.l);
                System.arraycopy(this.f.d(), 0, n4.d(), 0, 8);
                this.n = n4;
                this.j = 1;
            }
            else {
                this.e(i.c() - this.m);
                this.n = null;
                this.j = 1;
            }
            return true;
        }
        throw ai.a("Atom size less than header length (unsupported).");
    }
    
    private boolean b(final com.applovin.exoplayer2.e.i i, final u u) throws IOException {
        final long n = this.l - this.m;
        final long c = i.c();
        final y n2 = this.n;
        boolean b = true;
        boolean b2 = false;
        Label_0151: {
            if (n2 != null) {
                i.b(n2.d(), this.m, (int)n);
                if (this.k == 1718909296) {
                    this.x = a(n2);
                }
                else if (!this.g.isEmpty()) {
                    this.g.peek().a(new com.applovin.exoplayer2.e.g.a.b(this.k, n2));
                }
            }
            else {
                if (n >= 262144L) {
                    u.a = i.c() + n;
                    b2 = true;
                    break Label_0151;
                }
                i.b((int)n);
            }
            b2 = false;
        }
        this.b(c + n);
        if (!b2 || this.j == 2) {
            b = false;
        }
        return b;
    }
    
    private int c(final long n) {
        int n2 = -1;
        int n3 = -1;
        int i = 0;
        long n4 = Long.MAX_VALUE;
        int n5 = 1;
        long n6 = Long.MAX_VALUE;
        int n7 = 1;
        long n8 = Long.MAX_VALUE;
        while (i < ((a[])com.applovin.exoplayer2.l.ai.a((Object)this.t)).length) {
            final a a = this.t[i];
            final int d = a.d;
            final n b = a.b;
            long n9;
            if (d == b.b) {
                n9 = n4;
            }
            else {
                final long n10 = b.c[d];
                final long n11 = ((long[][])com.applovin.exoplayer2.l.ai.a((Object)this.u))[i][d];
                final long n12 = n10 - n;
                final boolean b2 = n12 < 0L || n12 >= 262144L;
                int n13 = 0;
                long n14 = 0L;
                int n15 = 0;
                long n16 = 0L;
                Label_0216: {
                    if (b2 || n7 == 0) {
                        n13 = n3;
                        n14 = n6;
                        n15 = n7;
                        n16 = n8;
                        if ((b2 ? 1 : 0) != n7) {
                            break Label_0216;
                        }
                        n13 = n3;
                        n14 = n6;
                        n15 = n7;
                        n16 = n8;
                        if (n12 >= n8) {
                            break Label_0216;
                        }
                    }
                    n15 = (b2 ? 1 : 0);
                    n16 = n12;
                    n13 = i;
                    n14 = n11;
                }
                n3 = n13;
                n9 = n4;
                n6 = n14;
                n7 = n15;
                n8 = n16;
                if (n11 < n4) {
                    n2 = i;
                    n8 = n16;
                    n7 = n15;
                    n6 = n14;
                    n5 = (b2 ? 1 : 0);
                    n9 = n11;
                    n3 = n13;
                }
            }
            ++i;
            n4 = n9;
        }
        if (n4 == Long.MAX_VALUE || n5 == 0 || n6 < n4 + 10485760L) {
            n2 = n3;
        }
        return n2;
    }
    
    private int c(final com.applovin.exoplayer2.e.i i, final u u) throws IOException {
        final int a = this.h.a(i, u, this.i);
        if (a == 1 && u.a == 0L) {
            this.d();
        }
        return a;
    }
    
    private void c(final com.applovin.exoplayer2.e.i i) throws IOException {
        this.e.a(8);
        i.d(this.e.d(), 0, 8);
        com.applovin.exoplayer2.e.g.b.a(this.e);
        i.b(this.e.c());
        i.a();
    }
    
    private static boolean c(final int n) {
        return n == 1836019574 || n == 1953653099 || n == 1835297121 || n == 1835626086 || n == 1937007212 || n == 1701082227 || n == 1835365473;
    }
    
    private int d(final com.applovin.exoplayer2.e.i i, final u u) throws IOException {
        final long c = i.c();
        if (this.o == -1 && (this.o = this.c(c)) == -1) {
            return -1;
        }
        final a a = ((a[])com.applovin.exoplayer2.l.ai.a((Object)this.t))[this.o];
        final x c2 = a.c;
        final int d = a.d;
        final n b = a.b;
        final long a2 = b.c[d];
        final int n = b.d[d];
        final long n2 = a2 - c + this.p;
        if (n2 >= 0L && n2 < 262144L) {
            long n3 = n2;
            int n4 = n;
            if (a.a.g == 1) {
                n3 = n2 + 8L;
                n4 = n - 8;
            }
            i.b((int)n3);
            final k a3 = a.a;
            int n6;
            if (a3.j != 0) {
                final byte[] d2 = this.d.d();
                d2[0] = 0;
                d2[2] = (d2[1] = 0);
                final int j = a.a.j;
                final int n5 = 4 - j;
                while (true) {
                    n6 = n4;
                    if (this.q >= n4) {
                        break;
                    }
                    final int r = this.r;
                    if (r == 0) {
                        i.b(d2, n5, j);
                        this.p += j;
                        this.d.d(0);
                        final int q = this.d.q();
                        if (q < 0) {
                            throw ai.b("Invalid NAL length", null);
                        }
                        this.r = q;
                        this.c.d(0);
                        c2.a(this.c, 4);
                        this.q += 4;
                        n4 += n5;
                    }
                    else {
                        final int a4 = c2.a((com.applovin.exoplayer2.k.g)i, r, false);
                        this.p += a4;
                        this.q += a4;
                        this.r -= a4;
                    }
                }
            }
            else {
                int n7 = n4;
                if ("audio/ac4".equals(a3.f.l)) {
                    if (this.q == 0) {
                        com.applovin.exoplayer2.b.c.a(n4, this.e);
                        c2.a(this.e, 7);
                        this.q += 7;
                    }
                    n7 = n4 + 7;
                }
                while (true) {
                    final int q2 = this.q;
                    if (q2 >= (n6 = n7)) {
                        break;
                    }
                    final int a5 = c2.a((com.applovin.exoplayer2.k.g)i, n7 - q2, false);
                    this.p += a5;
                    this.q += a5;
                    this.r -= a5;
                }
            }
            final n b2 = a.b;
            c2.a(b2.f[d], b2.g[d], n6, 0, null);
            ++a.d;
            this.o = -1;
            this.p = 0;
            this.q = 0;
            return this.r = 0;
        }
        u.a = a2;
        return 1;
    }
    
    private void d() {
        this.j = 0;
        this.m = 0;
    }
    
    private void d(final long n) {
        for (final a a : this.t) {
            final n b = a.b;
            int d;
            if ((d = b.a(n)) == -1) {
                d = b.b(n);
            }
            a.d = d;
        }
    }
    
    private void e() {
        if (this.x == 2 && (this.b & 0x2) != 0x0) {
            final j j = (j)com.applovin.exoplayer2.l.a.b((Object)this.s);
            final x a = j.a(0, 4);
            com.applovin.exoplayer2.g.a a2;
            if (this.y == null) {
                a2 = null;
            }
            else {
                a2 = new com.applovin.exoplayer2.g.a(new com.applovin.exoplayer2.g.a.a[] { this.y });
            }
            a.a(new com.applovin.exoplayer2.v.a().a(a2).a());
            j.a();
            j.a(new b(-9223372036854775807L));
        }
    }
    
    private void e(final long n) {
        if (this.k == 1836086884) {
            final int m = this.m;
            this.y = new com.applovin.exoplayer2.g.f.b(0L, n, -9223372036854775807L, n + m, this.l - m);
        }
    }
    
    @Override
    public int a(final com.applovin.exoplayer2.e.i i, final u u) throws IOException {
        while (true) {
            final int j = this.j;
            if (j != 0) {
                if (j != 1) {
                    if (j == 2) {
                        return this.d(i, u);
                    }
                    if (j == 3) {
                        return this.c(i, u);
                    }
                    throw new IllegalStateException();
                }
                else {
                    if (this.b(i, u)) {
                        return 1;
                    }
                    continue;
                }
            }
            else {
                if (!this.b(i)) {
                    return -1;
                }
                continue;
            }
        }
    }
    
    @Override
    public v.a a(long a) {
        if (((a[])com.applovin.exoplayer2.l.a.b((Object)this.t)).length == 0) {
            return new v.a(com.applovin.exoplayer2.e.w.a);
        }
        final int v = this.v;
        long n;
        long n3;
        long n4;
        if (v != -1) {
            final n b = this.t[v].b;
            final int a2 = a(b, a);
            if (a2 == -1) {
                return new v.a(com.applovin.exoplayer2.e.w.a);
            }
            n = b.f[a2];
            final long n2 = b.c[a2];
            Label_0157: {
                if (n < a && a2 < b.b - 1) {
                    final int b2 = b.b(a);
                    if (b2 != -1 && b2 != a2) {
                        a = b.f[b2];
                        n3 = b.c[b2];
                        break Label_0157;
                    }
                }
                n3 = -1L;
                a = -9223372036854775807L;
            }
            n4 = a;
            a = n2;
        }
        else {
            final long n5 = Long.MAX_VALUE;
            n3 = -1L;
            n4 = -9223372036854775807L;
            n = a;
            a = n5;
        }
        int n6 = 0;
        while (true) {
            final a[] t = this.t;
            if (n6 >= t.length) {
                break;
            }
            long n7 = n3;
            long a3 = a;
            if (n6 != this.v) {
                final n b3 = t[n6].b;
                a3 = a(b3, n, a);
                a = n3;
                if (n4 != -9223372036854775807L) {
                    a = a(b3, n4, n3);
                }
                n7 = a;
            }
            ++n6;
            n3 = n7;
            a = a3;
        }
        final w w = new w(n, a);
        if (n4 == -9223372036854775807L) {
            return new v.a(w);
        }
        return new v.a(w, new w(n4, n3));
    }
    
    @Override
    public void a(final long n, final long n2) {
        this.g.clear();
        this.m = 0;
        this.o = -1;
        this.p = 0;
        this.q = 0;
        this.r = 0;
        if (n == 0L) {
            if (this.j != 3) {
                this.d();
            }
            else {
                this.h.a();
                this.i.clear();
            }
        }
        else if (this.t != null) {
            this.d(n2);
        }
    }
    
    @Override
    public void a(final j s) {
        this.s = s;
    }
    
    @Override
    public boolean a() {
        return true;
    }
    
    @Override
    public boolean a(final com.applovin.exoplayer2.e.i i) throws IOException {
        return com.applovin.exoplayer2.e.g.j.a(i, (this.b & 0x2) != 0x0);
    }
    
    @Override
    public long b() {
        return this.w;
    }
    
    @Override
    public void c() {
    }
    
    private static final class a
    {
        public final k a;
        public final n b;
        public final x c;
        public int d;
        
        public a(final k a, final n b, final x c) {
            this.a = a;
            this.b = b;
            this.c = c;
        }
    }
}

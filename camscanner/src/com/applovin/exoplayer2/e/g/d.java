// 
// Decompiled by Procyon v0.6.0
// 

package com.applovin.exoplayer2.e.g;

import com.applovin.exoplayer2.l.ai;

final class d
{
    public static a a(final int n, final long[] array, final int[] array2, final long n2) {
        final int a = 8192 / n;
        final int length = array2.length;
        final int n3 = 0;
        int i = 0;
        int n4 = 0;
        while (i < length) {
            n4 += ai.a(array2[i], a);
            ++i;
        }
        final long[] array3 = new long[n4];
        final int[] array4 = new int[n4];
        final long[] array5 = new long[n4];
        final int[] array6 = new int[n4];
        final int n5 = 0;
        int n6 = 0;
        int max = 0;
        int j = n3;
        int n7 = n5;
        while (j < array2.length) {
            int k = array2[j];
            long n8 = array[j];
            while (k > 0) {
                final int min = Math.min(a, k);
                array3[n6] = n8;
                final int b = n * min;
                array4[n6] = b;
                max = Math.max(max, b);
                array5[n6] = n7 * n2;
                array6[n6] = 1;
                n8 += array4[n6];
                n7 += min;
                k -= min;
                ++n6;
            }
            ++j;
        }
        return new a(array3, array4, max, array5, array6, n2 * n7);
    }
    
    public static final class a
    {
        public final long[] a;
        public final int[] b;
        public final int c;
        public final long[] d;
        public final int[] e;
        public final long f;
        
        private a(final long[] a, final int[] b, final int c, final long[] d, final int[] e, final long f) {
            this.a = a;
            this.b = b;
            this.c = c;
            this.d = d;
            this.e = e;
            this.f = f;
        }
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.applovin.exoplayer2.e;

import com.applovin.exoplayer2.v;
import java.util.Collections;
import java.util.Collection;
import com.applovin.exoplayer2.g.c.b;
import com.applovin.exoplayer2.l.q;
import com.applovin.exoplayer2.l.ai;
import java.util.ArrayList;
import java.util.List;
import com.applovin.exoplayer2.l.x;
import com.applovin.exoplayer2.g.a;
import androidx.annotation.Nullable;

public final class p
{
    public final int a;
    public final int b;
    public final int c;
    public final int d;
    public final int e;
    public final int f;
    public final int g;
    public final int h;
    public final int i;
    public final long j;
    @Nullable
    public final a k;
    @Nullable
    private final com.applovin.exoplayer2.g.a l;
    
    private p(final int a, final int b, final int c, final int d, final int e, final int g, final int h, final long j, @Nullable final a k, @Nullable final com.applovin.exoplayer2.g.a l) {
        this.a = a;
        this.b = b;
        this.c = c;
        this.d = d;
        this.e = e;
        this.f = a(e);
        this.g = g;
        this.h = h;
        this.i = b(h);
        this.j = j;
        this.k = k;
        this.l = l;
    }
    
    public p(final byte[] array, int c) {
        final x x = new x(array);
        x.a(c * 8);
        this.a = x.c(16);
        this.b = x.c(16);
        this.c = x.c(24);
        this.d = x.c(24);
        c = x.c(20);
        this.e = c;
        this.f = a(c);
        this.g = x.c(3) + 1;
        c = x.c(5) + 1;
        this.h = c;
        this.i = b(c);
        this.j = x.d(36);
        this.k = null;
        this.l = null;
    }
    
    private static int a(final int n) {
        switch (n) {
            default: {
                return -1;
            }
            case 192000: {
                return 3;
            }
            case 176400: {
                return 2;
            }
            case 96000: {
                return 11;
            }
            case 88200: {
                return 1;
            }
            case 48000: {
                return 10;
            }
            case 44100: {
                return 9;
            }
            case 32000: {
                return 8;
            }
            case 24000: {
                return 7;
            }
            case 22050: {
                return 6;
            }
            case 16000: {
                return 5;
            }
            case 8000: {
                return 4;
            }
        }
    }
    
    @Nullable
    private static com.applovin.exoplayer2.g.a a(final List<String> list, final List<com.applovin.exoplayer2.g.c.a> c) {
        final boolean empty = list.isEmpty();
        final com.applovin.exoplayer2.g.a a = null;
        if (empty && c.isEmpty()) {
            return null;
        }
        final ArrayList list2 = new ArrayList();
        for (int i = 0; i < list.size(); ++i) {
            final String str = list.get(i);
            final String[] b = ai.b(str, "=");
            if (b.length != 2) {
                final StringBuilder sb = new StringBuilder();
                sb.append("Failed to parse Vorbis comment: ");
                sb.append(str);
                q.c("FlacStreamMetadata", sb.toString());
            }
            else {
                list2.add(new b(b[0], b[1]));
            }
        }
        list2.addAll(c);
        com.applovin.exoplayer2.g.a a2;
        if (list2.isEmpty()) {
            a2 = a;
        }
        else {
            a2 = new com.applovin.exoplayer2.g.a(list2);
        }
        return a2;
    }
    
    private static int b(final int n) {
        if (n == 8) {
            return 1;
        }
        if (n == 12) {
            return 2;
        }
        if (n == 16) {
            return 4;
        }
        if (n == 20) {
            return 5;
        }
        if (n != 24) {
            return -1;
        }
        return 6;
    }
    
    public long a() {
        final long j = this.j;
        long n;
        if (j == 0L) {
            n = -9223372036854775807L;
        }
        else {
            n = j * 1000000L / this.e;
        }
        return n;
    }
    
    public long a(final long n) {
        return ai.a(n * this.e / 1000000L, 0L, this.j - 1L);
    }
    
    public p a(@Nullable final a a) {
        return new p(this.a, this.b, this.c, this.d, this.e, this.g, this.h, this.j, a, this.l);
    }
    
    public p a(final List<String> list) {
        return new p(this.a, this.b, this.c, this.d, this.e, this.g, this.h, this.j, this.k, this.a(a(list, Collections.emptyList())));
    }
    
    @Nullable
    public com.applovin.exoplayer2.g.a a(@Nullable com.applovin.exoplayer2.g.a a) {
        final com.applovin.exoplayer2.g.a l = this.l;
        if (l != null) {
            a = l.a(a);
        }
        return a;
    }
    
    public v a(final byte[] o, @Nullable com.applovin.exoplayer2.g.a a) {
        o[4] = -128;
        int d = this.d;
        if (d <= 0) {
            d = -1;
        }
        a = this.a(a);
        return new v.a().f("audio/flac").f(d).k(this.g).l(this.e).a(Collections.singletonList(o)).a(a).a();
    }
    
    public long b() {
        final int d = this.d;
        long n;
        long n2;
        if (d > 0) {
            n = (d + (long)this.c) / 2L;
            n2 = 1L;
        }
        else {
            final int a = this.a;
            long n3;
            if (a == this.b && a > 0) {
                n3 = a;
            }
            else {
                n3 = 4096L;
            }
            n = n3 * this.g * this.h / 8L;
            n2 = 64L;
        }
        return n + n2;
    }
    
    public p b(final List<com.applovin.exoplayer2.g.c.a> list) {
        return new p(this.a, this.b, this.c, this.d, this.e, this.g, this.h, this.j, this.k, this.a(a(Collections.emptyList(), list)));
    }
    
    public static class a
    {
        public final long[] a;
        public final long[] b;
        
        public a(final long[] a, final long[] b) {
            this.a = a;
            this.b = b;
        }
    }
}

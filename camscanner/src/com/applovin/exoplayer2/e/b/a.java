// 
// Decompiled by Procyon v0.6.0
// 

package com.applovin.exoplayer2.e.b;

import java.io.IOException;
import com.applovin.exoplayer2.e.i;
import com.applovin.exoplayer2.e.m;
import java.util.Objects;
import com.applovin.exoplayer2.e.p;

final class a extends a
{
    public a(final p obj, final int n, final long n2, final long n3) {
        Objects.requireNonNull(obj);
        super((d)new \u3007080(obj), (f)new a(obj, n), obj.a(), 0L, obj.j, n2, n3, obj.b(), Math.max(6, obj.c));
    }
    
    private static final class a implements f
    {
        private final p a;
        private final int b;
        private final m.a c;
        
        private a(final p a, final int b) {
            this.a = a;
            this.b = b;
            this.c = new m.a();
        }
        
        private long a(final i i) throws IOException {
            while (i.b() < i.d() - 6L && !m.a(i, this.a, this.b, this.c)) {
                i.c(1);
            }
            if (i.b() >= i.d() - 6L) {
                i.c((int)(i.d() - i.b()));
                return this.a.j;
            }
            return this.c.a;
        }
        
        @Override
        public e a(final i i, final long n) throws IOException {
            final long c = i.c();
            final long a = this.a(i);
            final long b = i.b();
            i.c(Math.max(6, this.a.c));
            final long a2 = this.a(i);
            final long b2 = i.b();
            if (a <= n && a2 > n) {
                return e.a(b);
            }
            if (a2 <= n) {
                return e.b(a2, b2);
            }
            return e.a(a, c);
        }
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.applovin.exoplayer2.e.b;

import com.applovin.exoplayer2.e.n;
import com.applovin.exoplayer2.e.o;
import com.applovin.exoplayer2.e.v;
import java.io.IOException;
import com.applovin.exoplayer2.e.u;
import com.applovin.exoplayer2.e.i;
import com.applovin.exoplayer2.l.ai;
import com.applovin.exoplayer2.e.p;
import androidx.annotation.Nullable;
import com.applovin.exoplayer2.g.a;
import com.applovin.exoplayer2.e.x;
import com.applovin.exoplayer2.e.j;
import com.applovin.exoplayer2.e.m;
import com.applovin.exoplayer2.l.y;
import com.applovin.exoplayer2.e.l;
import com.applovin.exoplayer2.e.h;

public final class b implements h
{
    public static final l a;
    private final byte[] b;
    private final y c;
    private final boolean d;
    private final m.a e;
    private j f;
    private x g;
    private int h;
    @Nullable
    private a i;
    private p j;
    private int k;
    private int l;
    private com.applovin.exoplayer2.e.b.a m;
    private int n;
    private long o;
    
    static {
        a = new \u3007o00\u3007\u3007Oo();
    }
    
    public b() {
        this(0);
    }
    
    public b(final int n) {
        this.b = new byte[42];
        this.c = new y(new byte[32768], 0);
        boolean d = true;
        if ((n & 0x1) == 0x0) {
            d = false;
        }
        this.d = d;
        this.e = new m.a();
        this.h = 0;
    }
    
    private long a(final y y, final boolean b) {
        com.applovin.exoplayer2.l.a.b((Object)this.j);
        int i;
        for (i = y.c(); i <= y.b() - 16; ++i) {
            y.d(i);
            if (com.applovin.exoplayer2.e.m.a(y, this.j, this.l, this.e)) {
                y.d(i);
                return this.e.a;
            }
        }
        if (b) {
            while (i <= y.b() - this.k) {
                y.d(i);
                final int n = 0;
                int a;
                try {
                    a = (com.applovin.exoplayer2.e.m.a(y, this.j, this.l, this.e) ? 1 : 0);
                }
                catch (final IndexOutOfBoundsException ex) {
                    a = 0;
                }
                if (y.c() > y.b()) {
                    a = n;
                }
                if (a != 0) {
                    y.d(i);
                    return this.e.a;
                }
                ++i;
            }
            y.d(y.b());
        }
        else {
            y.d(i);
        }
        return -1L;
    }
    
    private void a() {
        ((x)ai.a((Object)this.g)).a(this.o * 1000000L / ((p)ai.a((Object)this.j)).e, 1, this.n, 0, null);
    }
    
    private int b(final i i, final u u) throws IOException {
        com.applovin.exoplayer2.l.a.b((Object)this.g);
        com.applovin.exoplayer2.l.a.b((Object)this.j);
        final com.applovin.exoplayer2.e.b.a m = this.m;
        if (m != null && m.b()) {
            return this.m.a(i, u);
        }
        if (this.o == -1L) {
            this.o = com.applovin.exoplayer2.e.m.a(i, this.j);
            return 0;
        }
        final int b = this.c.b();
        boolean b2;
        if (b < 32768) {
            final int a = i.a(this.c.d(), b, 32768 - b);
            b2 = (a == -1);
            if (!b2) {
                this.c.c(b + a);
            }
            else if (this.c.a() == 0) {
                this.a();
                return -1;
            }
        }
        else {
            b2 = false;
        }
        final int c = this.c.c();
        final int n = this.n;
        final int k = this.k;
        if (n < k) {
            final y c2 = this.c;
            c2.e(Math.min(k - n, c2.a()));
        }
        final long a2 = this.a(this.c, b2);
        final int n2 = this.c.c() - c;
        this.c.d(c);
        this.g.a(this.c, n2);
        this.n += n2;
        if (a2 != -1L) {
            this.a();
            this.n = 0;
            this.o = a2;
        }
        if (this.c.a() < 16) {
            final int a3 = this.c.a();
            System.arraycopy(this.c.d(), this.c.c(), this.c.d(), 0, a3);
            this.c.d(0);
            this.c.c(a3);
        }
        return 0;
    }
    
    private v b(final long n, final long n2) {
        com.applovin.exoplayer2.l.a.b((Object)this.j);
        final p j = this.j;
        if (j.k != null) {
            return new o(j, n);
        }
        if (n2 != -1L && j.j > 0L) {
            final com.applovin.exoplayer2.e.b.a m = new com.applovin.exoplayer2.e.b.a(j, this.l, n, n2);
            this.m = m;
            return m.a();
        }
        return new v.b(j.a());
    }
    
    private void b(final i i) throws IOException {
        this.i = com.applovin.exoplayer2.e.n.b(i, this.d ^ true);
        this.h = 1;
    }
    
    private void c(final i i) throws IOException {
        final byte[] b = this.b;
        i.d(b, 0, b.length);
        i.a();
        this.h = 2;
    }
    
    private void d(final i i) throws IOException {
        com.applovin.exoplayer2.e.n.b(i);
        this.h = 3;
    }
    
    private void e(final i i) throws IOException {
        final n.a a = new n.a(this.j);
        for (boolean a2 = false; !a2; a2 = com.applovin.exoplayer2.e.n.a(i, a), this.j = (p)ai.a((Object)a.a)) {}
        com.applovin.exoplayer2.l.a.b((Object)this.j);
        this.k = Math.max(this.j.c, 6);
        ((x)ai.a((Object)this.g)).a(this.j.a(this.b, this.i));
        this.h = 4;
    }
    
    private void f(final i i) throws IOException {
        this.l = com.applovin.exoplayer2.e.n.c(i);
        ((j)ai.a((Object)this.f)).a(this.b(i.c(), i.d()));
        this.h = 5;
    }
    
    @Override
    public int a(final i i, final u u) throws IOException {
        final int h = this.h;
        if (h == 0) {
            this.b(i);
            return 0;
        }
        if (h == 1) {
            this.c(i);
            return 0;
        }
        if (h == 2) {
            this.d(i);
            return 0;
        }
        if (h == 3) {
            this.e(i);
            return 0;
        }
        if (h == 4) {
            this.f(i);
            return 0;
        }
        if (h == 5) {
            return this.b(i, u);
        }
        throw new IllegalStateException();
    }
    
    @Override
    public void a(long o, final long n) {
        final long n2 = 0L;
        if (o == 0L) {
            this.h = 0;
        }
        else {
            final com.applovin.exoplayer2.e.b.a m = this.m;
            if (m != null) {
                m.a(n);
            }
        }
        if (n == 0L) {
            o = n2;
        }
        else {
            o = -1L;
        }
        this.o = o;
        this.n = 0;
        this.c.a(0);
    }
    
    @Override
    public void a(final j f) {
        this.f = f;
        this.g = f.a(0, 1);
        f.a();
    }
    
    @Override
    public boolean a(final i i) throws IOException {
        com.applovin.exoplayer2.e.n.a(i, false);
        return com.applovin.exoplayer2.e.n.a(i);
    }
    
    @Override
    public void c() {
    }
}

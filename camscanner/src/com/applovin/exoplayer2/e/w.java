// 
// Decompiled by Procyon v0.6.0
// 

package com.applovin.exoplayer2.e;

import androidx.annotation.Nullable;

public final class w
{
    public static final w a;
    public final long b;
    public final long c;
    
    static {
        a = new w(0L, 0L);
    }
    
    public w(final long b, final long c) {
        this.b = b;
        this.c = c;
    }
    
    @Override
    public boolean equals(@Nullable final Object o) {
        boolean b = true;
        if (this == o) {
            return true;
        }
        if (o != null && w.class == o.getClass()) {
            final w w = (w)o;
            if (this.b != w.b || this.c != w.c) {
                b = false;
            }
            return b;
        }
        return false;
    }
    
    @Override
    public int hashCode() {
        return (int)this.b * 31 + (int)this.c;
    }
    
    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append("[timeUs=");
        sb.append(this.b);
        sb.append(", position=");
        sb.append(this.c);
        sb.append("]");
        return sb.toString();
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.applovin.exoplayer2.e;

import java.io.IOException;

public class q implements i
{
    private final i a;
    
    public q(final i a) {
        this.a = a;
    }
    
    @Override
    public int a(final int n) throws IOException {
        return this.a.a(n);
    }
    
    @Override
    public int a(final byte[] array, final int n, final int n2) throws IOException {
        return this.a.a(array, n, n2);
    }
    
    @Override
    public void a() {
        this.a.a();
    }
    
    @Override
    public boolean a(final byte[] array, final int n, final int n2, final boolean b) throws IOException {
        return this.a.a(array, n, n2, b);
    }
    
    @Override
    public long b() {
        return this.a.b();
    }
    
    @Override
    public void b(final int n) throws IOException {
        this.a.b(n);
    }
    
    @Override
    public void b(final byte[] array, final int n, final int n2) throws IOException {
        this.a.b(array, n, n2);
    }
    
    @Override
    public boolean b(final int n, final boolean b) throws IOException {
        return this.a.b(n, b);
    }
    
    @Override
    public boolean b(final byte[] array, final int n, final int n2, final boolean b) throws IOException {
        return this.a.b(array, n, n2, b);
    }
    
    @Override
    public int c(final byte[] array, final int n, final int n2) throws IOException {
        return this.a.c(array, n, n2);
    }
    
    @Override
    public long c() {
        return this.a.c();
    }
    
    @Override
    public void c(final int n) throws IOException {
        this.a.c(n);
    }
    
    @Override
    public long d() {
        return this.a.d();
    }
    
    @Override
    public void d(final byte[] array, final int n, final int n2) throws IOException {
        this.a.d(array, n, n2);
    }
}

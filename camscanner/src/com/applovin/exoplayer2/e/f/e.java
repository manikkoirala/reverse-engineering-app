// 
// Decompiled by Procyon v0.6.0
// 

package com.applovin.exoplayer2.e.f;

import com.applovin.exoplayer2.e.v;

interface e extends v
{
    long c();
    
    long c(final long p0);
    
    public static class a extends b implements e
    {
        public a() {
            super(-9223372036854775807L);
        }
        
        @Override
        public long c() {
            return -1L;
        }
        
        @Override
        public long c(final long n) {
            return 0L;
        }
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.applovin.exoplayer2.e.f;

import com.applovin.exoplayer2.e.w;
import com.applovin.exoplayer2.e.v;
import androidx.annotation.Nullable;
import com.applovin.exoplayer2.l.q;
import com.applovin.exoplayer2.l.ai;
import com.applovin.exoplayer2.l.y;
import com.applovin.exoplayer2.b.r;

final class f implements e
{
    private final long[] a;
    private final long[] b;
    private final long c;
    private final long d;
    
    private f(final long[] a, final long[] b, final long c, final long d) {
        this.a = a;
        this.b = b;
        this.c = c;
        this.d = d;
    }
    
    @Nullable
    public static f a(final long lng, long n, final r.a a, final y y) {
        y.e(10);
        final int q = y.q();
        if (q <= 0) {
            return null;
        }
        final int d = a.d;
        final long n2 = q;
        int n3;
        if (d >= 32000) {
            n3 = 1152;
        }
        else {
            n3 = 576;
        }
        final long d2 = ai.d(n2, n3 * 1000000L, (long)d);
        final int i = y.i();
        final int j = y.i();
        final int k = y.i();
        y.e(2);
        final long b = n + a.c;
        final long[] array = new long[i];
        final long[] array2 = new long[i];
        for (int l = 0; l < i; ++l) {
            array[l] = l * d2 / i;
            array2[l] = Math.max(n, b);
            int n4;
            if (k != 1) {
                if (k != 2) {
                    if (k != 3) {
                        if (k != 4) {
                            return null;
                        }
                        n4 = y.w();
                    }
                    else {
                        n4 = y.m();
                    }
                }
                else {
                    n4 = y.i();
                }
            }
            else {
                n4 = y.h();
            }
            n += n4 * j;
        }
        if (lng != -1L && lng != n) {
            final StringBuilder sb = new StringBuilder();
            sb.append("VBRI data size mismatch: ");
            sb.append(lng);
            sb.append(", ");
            sb.append(n);
            com.applovin.exoplayer2.l.q.c("VbriSeeker", sb.toString());
        }
        return new f(array, array2, d2, n);
    }
    
    @Override
    public v.a a(final long n) {
        int a = ai.a(this.a, n, true, true);
        final w w = new w(this.a[a], this.b[a]);
        if (w.b < n && a != this.a.length - 1) {
            final long[] a2 = this.a;
            ++a;
            return new v.a(w, new w(a2[a], this.b[a]));
        }
        return new v.a(w);
    }
    
    @Override
    public boolean a() {
        return true;
    }
    
    @Override
    public long b() {
        return this.c;
    }
    
    @Override
    public long c() {
        return this.d;
    }
    
    @Override
    public long c(final long n) {
        return this.a[ai.a(this.b, n, true, true)];
    }
}

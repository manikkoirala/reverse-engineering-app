// 
// Decompiled by Procyon v0.6.0
// 

package com.applovin.exoplayer2.e.f;

import com.applovin.exoplayer2.e.w;
import com.applovin.exoplayer2.l.ai;
import com.applovin.exoplayer2.e.v;
import com.applovin.exoplayer2.l.r;

final class b implements e
{
    private final long a;
    private final r b;
    private final r c;
    private long d;
    
    public b(final long d, final long n, final long a) {
        this.d = d;
        this.a = a;
        final r b = new r();
        this.b = b;
        final r c = new r();
        this.c = c;
        b.a(0L);
        c.a(n);
    }
    
    @Override
    public v.a a(final long n) {
        int a = ai.a(this.b, n, true, true);
        final w w = new w(this.b.a(a), this.c.a(a));
        if (w.b != n && a != this.b.a() - 1) {
            final r b = this.b;
            ++a;
            return new v.a(w, new w(b.a(a), this.c.a(a)));
        }
        return new v.a(w);
    }
    
    public void a(final long n, final long n2) {
        if (this.b(n)) {
            return;
        }
        this.b.a(n);
        this.c.a(n2);
    }
    
    @Override
    public boolean a() {
        return true;
    }
    
    @Override
    public long b() {
        return this.d;
    }
    
    public boolean b(final long n) {
        final r b = this.b;
        final int a = b.a();
        boolean b2 = true;
        if (n - b.a(a - 1) >= 100000L) {
            b2 = false;
        }
        return b2;
    }
    
    @Override
    public long c() {
        return this.a;
    }
    
    @Override
    public long c(final long n) {
        return this.b.a(ai.a(this.c, n, true, true));
    }
    
    void d(final long d) {
        this.d = d;
    }
}

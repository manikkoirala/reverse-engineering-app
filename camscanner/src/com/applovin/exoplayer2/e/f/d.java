// 
// Decompiled by Procyon v0.6.0
// 

package com.applovin.exoplayer2.e.f;

import com.applovin.exoplayer2.e.u;
import com.applovin.exoplayer2.e.v;
import java.io.IOException;
import java.io.EOFException;
import com.applovin.exoplayer2.ai;
import com.applovin.exoplayer2.e.i;
import androidx.annotation.Nullable;
import com.applovin.exoplayer2.g.a;
import com.applovin.exoplayer2.e.j;
import com.applovin.exoplayer2.e.x;
import com.applovin.exoplayer2.e.s;
import com.applovin.exoplayer2.b.r;
import com.applovin.exoplayer2.l.y;
import com.applovin.exoplayer2.g.e.g;
import com.applovin.exoplayer2.e.l;
import com.applovin.exoplayer2.e.h;

public final class d implements h
{
    public static final l a;
    private static final g.a b;
    private final int c;
    private final long d;
    private final y e;
    private final r.a f;
    private final com.applovin.exoplayer2.e.r g;
    private final s h;
    private final x i;
    private j j;
    private x k;
    private x l;
    private int m;
    @Nullable
    private a n;
    private long o;
    private long p;
    private long q;
    private int r;
    private e s;
    private boolean t;
    private boolean u;
    private long v;
    
    static {
        a = new \u3007080();
        b = new \u3007o00\u3007\u3007Oo();
    }
    
    public d() {
        this(0);
    }
    
    public d(final int n) {
        this(n, -9223372036854775807L);
    }
    
    public d(final int n, final long d) {
        int c = n;
        if ((n & 0x2) != 0x0) {
            c = (n | 0x1);
        }
        this.c = c;
        this.d = d;
        this.e = new y(10);
        this.f = new r.a();
        this.g = new com.applovin.exoplayer2.e.r();
        this.o = -9223372036854775807L;
        this.h = new s();
        final com.applovin.exoplayer2.e.g g = new com.applovin.exoplayer2.e.g();
        this.i = g;
        this.l = g;
    }
    
    private static int a(final y y, int q) {
        if (y.b() >= q + 4) {
            y.d(q);
            q = y.q();
            if (q == 1483304551 || q == 1231971951) {
                return q;
            }
        }
        if (y.b() >= 40) {
            y.d(36);
            if (y.q() == 1447187017) {
                return 1447187017;
            }
        }
        return 0;
    }
    
    private long a(final long n) {
        return this.o + n * 1000000L / this.f.d;
    }
    
    private static long a(@Nullable final a a) {
        if (a != null) {
            for (int a2 = a.a(), i = 0; i < a2; ++i) {
                final a.a a3 = a.a(i);
                if (a3 instanceof com.applovin.exoplayer2.g.e.l) {
                    final com.applovin.exoplayer2.g.e.l l = (com.applovin.exoplayer2.g.e.l)a3;
                    if (l.f.equals("TLEN")) {
                        return com.applovin.exoplayer2.h.b(Long.parseLong(l.b));
                    }
                }
            }
        }
        return -9223372036854775807L;
    }
    
    @Nullable
    private static c a(@Nullable final a a, final long n) {
        if (a != null) {
            for (int a2 = a.a(), i = 0; i < a2; ++i) {
                final a.a a3 = a.a(i);
                if (a3 instanceof com.applovin.exoplayer2.g.e.j) {
                    return c.a(n, (com.applovin.exoplayer2.g.e.j)a3, a(a));
                }
            }
        }
        return null;
    }
    
    private static boolean a(final int n, final long n2) {
        return (n & 0xFFFE0C00) == (n2 & 0xFFFFFFFFFFFE0C00L);
    }
    
    private boolean a(final i i, final boolean b) throws IOException {
        int n;
        if (b) {
            n = 32768;
        }
        else {
            n = 131072;
        }
        i.a();
        int n2;
        if (i.c() == 0L) {
            g.a b2;
            if ((this.c & 0x8) == 0x0) {
                b2 = null;
            }
            else {
                b2 = com.applovin.exoplayer2.e.f.d.b;
            }
            final a a = this.h.a(i, b2);
            this.n = a;
            if (a != null) {
                this.g.a(a);
            }
            n2 = (int)i.b();
            if (!b) {
                i.b(n2);
            }
        }
        else {
            n2 = 0;
        }
        int m = 0;
        int n3 = 0;
        int n4 = 0;
        while (true) {
            while (!this.d(i)) {
                this.e.d(0);
                final int q = this.e.q();
                if (m == 0 || a(q, m)) {
                    final int a2 = com.applovin.exoplayer2.b.r.a(q);
                    if (a2 != -1) {
                        final int n5 = n3 + 1;
                        int n6;
                        if (n5 == 1) {
                            this.f.a(q);
                            n6 = q;
                        }
                        else {
                            n6 = m;
                            if (n5 == 4) {
                                if (b) {
                                    i.b(n2 + n4);
                                }
                                else {
                                    i.a();
                                }
                                this.m = m;
                                return true;
                            }
                        }
                        i.c(a2 - 4);
                        m = n6;
                        n3 = n5;
                        continue;
                    }
                }
                final int n7 = n4 + 1;
                if (n4 == n) {
                    if (b) {
                        return false;
                    }
                    throw ai.b("Searched too many bytes.", null);
                }
                else {
                    if (b) {
                        i.a();
                        i.c(n2 + n7);
                    }
                    else {
                        i.b(1);
                    }
                    n4 = n7;
                    m = 0;
                    n3 = 0;
                }
            }
            if (n3 > 0) {
                continue;
            }
            break;
        }
        throw new EOFException();
    }
    
    private int b(final i i) throws IOException {
        if (this.m == 0) {
            try {
                this.a(i, false);
            }
            catch (final EOFException ex) {
                return -1;
            }
        }
        if (this.s == null) {
            final e e = this.e(i);
            this.s = e;
            this.j.a(e);
            final x l = this.l;
            final com.applovin.exoplayer2.v.a o = new com.applovin.exoplayer2.v.a().f(this.f.b).f(4096).k(this.f.e).l(this.f.d).n(this.g.a).o(this.g.b);
            a n;
            if ((this.c & 0x8) != 0x0) {
                n = null;
            }
            else {
                n = this.n;
            }
            l.a(o.a(n).a());
            this.q = i.c();
        }
        else if (this.q != 0L) {
            final long c = i.c();
            final long q = this.q;
            if (c < q) {
                i.b((int)(q - c));
            }
        }
        return this.c(i);
    }
    
    private e b(final i i, final boolean b) throws IOException {
        i.d(this.e.d(), 0, 4);
        this.e.d(0);
        this.f.a(this.e.q());
        return new com.applovin.exoplayer2.e.f.a(i.d(), i.c(), this.f, b);
    }
    
    private void b() {
        com.applovin.exoplayer2.l.a.a((Object)this.k);
        com.applovin.exoplayer2.l.ai.a((Object)this.j);
    }
    
    private int c(final i i) throws IOException {
        if (this.r == 0) {
            i.a();
            if (this.d(i)) {
                return -1;
            }
            this.e.d(0);
            final int q = this.e.q();
            if (!a(q, this.m) || com.applovin.exoplayer2.b.r.a(q) == -1) {
                i.b(1);
                return this.m = 0;
            }
            this.f.a(q);
            if (this.o == -9223372036854775807L) {
                this.o = this.s.c(i.c());
                if (this.d != -9223372036854775807L) {
                    this.o += this.d - this.s.c(0L);
                }
            }
            final r.a f = this.f;
            this.r = f.c;
            final e s = this.s;
            if (s instanceof b) {
                final b b = (b)s;
                b.a(this.a(this.p + f.g), i.c() + this.f.c);
                if (this.u && b.b(this.v)) {
                    this.u = false;
                    this.l = this.k;
                }
            }
        }
        final int a = this.l.a((com.applovin.exoplayer2.k.g)i, this.r, true);
        if (a == -1) {
            return -1;
        }
        if ((this.r -= a) > 0) {
            return 0;
        }
        this.l.a(this.a(this.p), 1, this.f.c, 0, null);
        this.p += this.f.g;
        return this.r = 0;
    }
    
    private boolean d(final i i) throws IOException {
        final e s = this.s;
        if (s != null) {
            final long c = s.c();
            if (c != -1L && i.b() > c - 4L) {
                return true;
            }
        }
        try {
            return i.b(this.e.d(), 0, 4, true) ^ true;
        }
        catch (final EOFException ex) {
            return true;
        }
    }
    
    private e e(final i i) throws IOException {
        e f = this.f(i);
        final c a = a(this.n, i.c());
        if (this.t) {
            return new e.a();
        }
        if ((this.c & 0x4) != 0x0) {
            long n;
            long n2;
            if (a != null) {
                n = a.b();
                n2 = a.c();
            }
            else if (f != null) {
                n = f.b();
                n2 = f.c();
            }
            else {
                n = a(this.n);
                n2 = -1L;
            }
            f = new b(n, i.c(), n2);
        }
        else if (a != null) {
            f = a;
        }
        else if (f == null) {
            f = null;
        }
        boolean b = true;
        if (f != null) {
            e b2 = f;
            if (f.a()) {
                return b2;
            }
            b2 = f;
            if ((this.c & 0x1) == 0x0) {
                return b2;
            }
        }
        if ((this.c & 0x2) == 0x0) {
            b = false;
        }
        return this.b(i, b);
    }
    
    @Nullable
    private e f(final i i) throws IOException {
        final y y = new y(this.f.c);
        i.d(y.d(), 0, this.f.c);
        final r.a f = this.f;
        int n = 0;
        Label_0084: {
            if ((f.a & 0x1) != 0x0) {
                if (f.e != 1) {
                    n = 36;
                    break Label_0084;
                }
            }
            else if (f.e == 1) {
                n = 13;
                break Label_0084;
            }
            n = 21;
        }
        final int a = a(y, n);
        f a2;
        if (a != 1483304551 && a != 1231971951) {
            if (a == 1447187017) {
                a2 = com.applovin.exoplayer2.e.f.f.a(i.d(), i.c(), this.f, y);
                i.b(this.f.c);
            }
            else {
                i.a();
                a2 = null;
            }
        }
        else {
            final com.applovin.exoplayer2.e.f.g a3 = com.applovin.exoplayer2.e.f.g.a(i.d(), i.c(), this.f, y);
            if (a3 != null && !this.g.a()) {
                i.a();
                i.c(n + 141);
                i.d(this.e.d(), 0, 3);
                this.e.d(0);
                this.g.a(this.e.m());
            }
            i.b(this.f.c);
            if ((a2 = (f)a3) != null) {
                a2 = (f)a3;
                if (!a3.a()) {
                    a2 = (f)a3;
                    if (a == 1231971951) {
                        return this.b(i, false);
                    }
                }
            }
        }
        return a2;
    }
    
    @Override
    public int a(final i i, final u u) throws IOException {
        this.b();
        final int b = this.b(i);
        if (b == -1 && this.s instanceof b) {
            final long a = this.a(this.p);
            if (this.s.b() != a) {
                ((b)this.s).d(a);
                this.j.a(this.s);
            }
        }
        return b;
    }
    
    public void a() {
        this.t = true;
    }
    
    @Override
    public void a(final long n, final long v) {
        this.m = 0;
        this.o = -9223372036854775807L;
        this.p = 0L;
        this.r = 0;
        this.v = v;
        final e s = this.s;
        if (s instanceof b && !((b)s).b(v)) {
            this.u = true;
            this.l = this.i;
        }
    }
    
    @Override
    public void a(final j j) {
        this.j = j;
        final x a = j.a(0, 1);
        this.k = a;
        this.l = a;
        this.j.a();
    }
    
    @Override
    public boolean a(final i i) throws IOException {
        return this.a(i, true);
    }
    
    @Override
    public void c() {
    }
}

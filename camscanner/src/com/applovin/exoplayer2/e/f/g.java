// 
// Decompiled by Procyon v0.6.0
// 

package com.applovin.exoplayer2.e.f;

import com.applovin.exoplayer2.l.a;
import com.applovin.exoplayer2.e.w;
import com.applovin.exoplayer2.e.v;
import com.applovin.exoplayer2.l.q;
import com.applovin.exoplayer2.l.ai;
import com.applovin.exoplayer2.l.y;
import com.applovin.exoplayer2.b.r;
import androidx.annotation.Nullable;

final class g implements e
{
    private final long a;
    private final int b;
    private final long c;
    private final long d;
    private final long e;
    @Nullable
    private final long[] f;
    
    private g(final long n, final int n2, final long n3) {
        this(n, n2, n3, -1L, null);
    }
    
    private g(long n, final int b, long c, final long d, @Nullable final long[] f) {
        this.a = n;
        this.b = b;
        this.c = c;
        this.f = f;
        this.d = d;
        c = -1L;
        if (d == -1L) {
            n = c;
        }
        else {
            n += d;
        }
        this.e = n;
    }
    
    private long a(final int n) {
        return this.c * n / 100L;
    }
    
    @Nullable
    public static g a(final long lng, final long n, final r.a a, final y y) {
        final int g = a.g;
        final int d = a.d;
        final int q = y.q();
        if ((q & 0x1) == 0x1) {
            final int w = y.w();
            if (w != 0) {
                final long d2 = ai.d((long)w, g * 1000000L, (long)d);
                if ((q & 0x6) != 0x6) {
                    return new g(n, a.c, d2);
                }
                final long o = y.o();
                final long[] array = new long[100];
                for (int i = 0; i < 100; ++i) {
                    array[i] = y.h();
                }
                if (lng != -1L) {
                    final long lng2 = n + o;
                    if (lng != lng2) {
                        final StringBuilder sb = new StringBuilder();
                        sb.append("XING data size mismatch: ");
                        sb.append(lng);
                        sb.append(", ");
                        sb.append(lng2);
                        com.applovin.exoplayer2.l.q.c("XingSeeker", sb.toString());
                    }
                }
                return new g(n, a.c, d2, o, array);
            }
        }
        return null;
    }
    
    @Override
    public v.a a(long a) {
        if (!this.a()) {
            return new v.a(new w(0L, this.a + this.b));
        }
        a = ai.a(a, 0L, this.c);
        final double n = a * 100.0 / this.c;
        double n2 = 0.0;
        if (n > 0.0) {
            if (n >= 100.0) {
                n2 = 256.0;
            }
            else {
                final int n3 = (int)n;
                final long[] array = (long[])a.a((Object)this.f);
                final double n4 = (double)array[n3];
                double n5;
                if (n3 == 99) {
                    n5 = 256.0;
                }
                else {
                    n5 = (double)array[n3 + 1];
                }
                n2 = n4 + (n - n3) * (n5 - n4);
            }
        }
        return new v.a(new w(a, this.a + ai.a(Math.round(n2 / 256.0 * this.d), (long)this.b, this.d - 1L)));
    }
    
    @Override
    public boolean a() {
        return this.f != null;
    }
    
    @Override
    public long b() {
        return this.c;
    }
    
    @Override
    public long c() {
        return this.e;
    }
    
    @Override
    public long c(long n) {
        n -= this.a;
        if (this.a() && n > this.b) {
            final long[] array = (long[])com.applovin.exoplayer2.l.a.a((Object)this.f);
            final double n2 = n * 256.0 / this.d;
            final int a = ai.a(array, (long)n2, true, true);
            final long a2 = this.a(a);
            final long n3 = array[a];
            final int n4 = a + 1;
            final long a3 = this.a(n4);
            if (a == 99) {
                n = 256L;
            }
            else {
                n = array[n4];
            }
            double n5;
            if (n3 == n) {
                n5 = 0.0;
            }
            else {
                n5 = (n2 - n3) / (n - n3);
            }
            return a2 + Math.round(n5 * (a3 - a2));
        }
        return 0L;
    }
}

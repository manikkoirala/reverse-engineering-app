// 
// Decompiled by Procyon v0.6.0
// 

package com.applovin.exoplayer2.e.f;

import com.applovin.exoplayer2.e.w;
import com.applovin.exoplayer2.e.v;
import com.applovin.exoplayer2.g.e.j;
import com.applovin.exoplayer2.l.ai;
import android.util.Pair;
import com.applovin.exoplayer2.h;

final class c implements e
{
    private final long[] a;
    private final long[] b;
    private final long c;
    
    private c(final long[] a, final long[] b, long b2) {
        this.a = a;
        this.b = b;
        if (b2 == -9223372036854775807L) {
            b2 = h.b(b[b.length - 1]);
        }
        this.c = b2;
    }
    
    private static Pair<Long, Long> a(final long l, final long[] array, final long[] array2) {
        int a = ai.a(array, l, true, true);
        final long i = array[a];
        final long j = array2[a];
        if (++a == array.length) {
            return (Pair<Long, Long>)Pair.create((Object)i, (Object)j);
        }
        final long n = array[a];
        final long n2 = array2[a];
        double n3;
        if (n == i) {
            n3 = 0.0;
        }
        else {
            n3 = (l - (double)i) / (n - i);
        }
        return (Pair<Long, Long>)Pair.create((Object)l, (Object)((long)(n3 * (n2 - j)) + j));
    }
    
    public static c a(long n, final j j, final long n2) {
        final int length = j.d.length;
        final int n3 = length + 1;
        final long[] array = new long[n3];
        final long[] array2 = new long[n3];
        array[0] = n;
        long n4 = 0L;
        array2[0] = 0L;
        for (int i = 1; i <= length; ++i) {
            final int b = j.b;
            final int[] d = j.d;
            final int n5 = i - 1;
            n += b + d[n5];
            n4 += j.c + j.e[n5];
            array[i] = n;
            array2[i] = n4;
        }
        return new c(array, array2, n2);
    }
    
    @Override
    public v.a a(final long n) {
        final Pair<Long, Long> a = a(h.a(ai.a(n, 0L, this.c)), this.b, this.a);
        return new v.a(new w(h.b((long)a.first), (long)a.second));
    }
    
    @Override
    public boolean a() {
        return true;
    }
    
    @Override
    public long b() {
        return this.c;
    }
    
    @Override
    public long c() {
        return -1L;
    }
    
    @Override
    public long c(final long n) {
        return h.b((long)a(n, this.a, this.b).second);
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.applovin.exoplayer2.h;

import com.applovin.exoplayer2.k.g;
import java.util.List;
import java.util.Map;
import com.applovin.exoplayer2.k.aa;
import androidx.annotation.Nullable;
import android.net.Uri;
import com.applovin.exoplayer2.k.l;
import java.io.IOException;
import com.applovin.exoplayer2.l.y;
import com.applovin.exoplayer2.l.a;

final class i implements com.applovin.exoplayer2.k.i
{
    private final com.applovin.exoplayer2.k.i a;
    private final int b;
    private final a c;
    private final byte[] d;
    private int e;
    
    public i(final com.applovin.exoplayer2.k.i a, final int n, final a c) {
        com.applovin.exoplayer2.l.a.a(n > 0);
        this.a = a;
        this.b = n;
        this.c = c;
        this.d = new byte[1];
        this.e = n;
    }
    
    private boolean d() throws IOException {
        if (((g)this.a).a(this.d, 0, 1) == -1) {
            return false;
        }
        final int n = (this.d[0] & 0xFF) << 4;
        if (n == 0) {
            return true;
        }
        final byte[] array = new byte[n];
        int n2 = n;
        int n3 = 0;
        int n4;
        while (true) {
            n4 = n;
            if (n2 <= 0) {
                break;
            }
            final int a = ((g)this.a).a(array, n3, n2);
            if (a == -1) {
                return false;
            }
            n3 += a;
            n2 -= a;
        }
        while (n4 > 0 && array[n4 - 1] == 0) {
            --n4;
        }
        if (n4 > 0) {
            this.c.a(new y(array, n4));
        }
        return true;
    }
    
    public int a(final byte[] array, int a, final int b) throws IOException {
        if (this.e == 0) {
            if (!this.d()) {
                return -1;
            }
            this.e = this.b;
        }
        a = ((g)this.a).a(array, a, Math.min(this.e, b));
        if (a != -1) {
            this.e -= a;
        }
        return a;
    }
    
    public long a(final l l) {
        throw new UnsupportedOperationException();
    }
    
    @Nullable
    public Uri a() {
        return this.a.a();
    }
    
    public void a(final aa aa) {
        com.applovin.exoplayer2.l.a.b((Object)aa);
        this.a.a(aa);
    }
    
    public Map<String, List<String>> b() {
        return this.a.b();
    }
    
    public void c() {
        throw new UnsupportedOperationException();
    }
    
    public interface a
    {
        void a(final y p0);
    }
}

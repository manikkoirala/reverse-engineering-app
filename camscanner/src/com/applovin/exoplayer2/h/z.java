// 
// Decompiled by Procyon v0.6.0
// 

package com.applovin.exoplayer2.h;

import java.util.Arrays;
import java.util.Random;

public interface z
{
    int a();
    
    int a(final int p0);
    
    z a(final int p0, final int p1);
    
    int b();
    
    int b(final int p0);
    
    z b(final int p0, final int p1);
    
    int c();
    
    z d();
    
    public static class a implements z
    {
        private final Random a;
        private final int[] b;
        private final int[] c;
        
        public a(final int n) {
            this(n, new Random());
        }
        
        private a(final int n, final Random random) {
            this(a(n, random), random);
        }
        
        private a(final int[] b, final Random a) {
            this.b = b;
            this.a = a;
            this.c = new int[b.length];
            for (int i = 0; i < b.length; ++i) {
                this.c[b[i]] = i;
            }
        }
        
        private static int[] a(final int n, final Random random) {
            final int[] array = new int[n];
            int bound;
            for (int i = 0; i < n; i = bound) {
                bound = i + 1;
                final int nextInt = random.nextInt(bound);
                array[i] = array[nextInt];
                array[nextInt] = i;
            }
            return array;
        }
        
        @Override
        public int a() {
            return this.b.length;
        }
        
        @Override
        public int a(int n) {
            n = this.c[n] + 1;
            final int[] b = this.b;
            if (n < b.length) {
                n = b[n];
            }
            else {
                n = -1;
            }
            return n;
        }
        
        @Override
        public z a(final int n, final int n2) {
            final int[] a = new int[n2];
            final int[] array = new int[n2];
            int n3 = 0;
            int bound;
            for (int i = 0; i < n2; i = bound) {
                a[i] = this.a.nextInt(this.b.length + 1);
                final Random a2 = this.a;
                bound = i + 1;
                final int nextInt = a2.nextInt(bound);
                array[i] = array[nextInt];
                array[nextInt] = i + n;
            }
            Arrays.sort(a);
            final int[] array2 = new int[this.b.length + n2];
            int n4 = 0;
            int n5 = 0;
            while (true) {
                final int[] b = this.b;
                if (n3 >= b.length + n2) {
                    break;
                }
                if (n4 < n2 && n5 == a[n4]) {
                    array2[n3] = array[n4];
                    ++n4;
                }
                else {
                    final int n6 = b[n5];
                    if ((array2[n3] = n6) >= n) {
                        array2[n3] = n6 + n2;
                    }
                    ++n5;
                }
                ++n3;
            }
            return new a(array2, new Random(this.a.nextLong()));
        }
        
        @Override
        public int b() {
            final int[] b = this.b;
            int n;
            if (b.length > 0) {
                n = b[b.length - 1];
            }
            else {
                n = -1;
            }
            return n;
        }
        
        @Override
        public int b(int n) {
            int n2 = this.c[n];
            n = -1;
            if (--n2 >= 0) {
                n = this.b[n2];
            }
            return n;
        }
        
        @Override
        public z b(final int n, final int n2) {
            final int n3 = n2 - n;
            final int[] array = new int[this.b.length - n3];
            int n4 = 0;
            int n5 = 0;
            while (true) {
                final int[] b = this.b;
                if (n4 >= b.length) {
                    break;
                }
                final int n6 = b[n4];
                if (n6 >= n && n6 < n2) {
                    ++n5;
                }
                else {
                    int n7;
                    if ((n7 = n6) >= n) {
                        n7 = n6 - n3;
                    }
                    array[n4 - n5] = n7;
                }
                ++n4;
            }
            return new a(array, new Random(this.a.nextLong()));
        }
        
        @Override
        public int c() {
            final int[] b = this.b;
            int n;
            if (b.length > 0) {
                n = b[0];
            }
            else {
                n = -1;
            }
            return n;
        }
        
        @Override
        public z d() {
            return new a(0, new Random(this.a.nextLong()));
        }
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.applovin.exoplayer2.h;

import java.io.IOException;
import com.applovin.exoplayer2.l.a;
import com.applovin.exoplayer2.j.d;
import com.applovin.exoplayer2.l.ai;
import com.applovin.exoplayer2.av;
import androidx.annotation.Nullable;
import com.applovin.exoplayer2.k.b;

public final class k implements n, n.a
{
    public final p.a a;
    private final long b;
    private final b c;
    private p d;
    private n e;
    @Nullable
    private n.a f;
    @Nullable
    private a g;
    private boolean h;
    private long i;
    
    public k(final p.a a, final b c, final long b) {
        this.a = a;
        this.c = c;
        this.b = b;
        this.i = -9223372036854775807L;
    }
    
    private long e(long n) {
        final long i = this.i;
        if (i != -9223372036854775807L) {
            n = i;
        }
        return n;
    }
    
    @Override
    public long a(final long n, final av av) {
        return ((n)ai.a((Object)this.e)).a(n, av);
    }
    
    @Override
    public long a(final d[] array, final boolean[] array2, final x[] array3, final boolean[] array4, long n) {
        final long i = this.i;
        if (i != -9223372036854775807L && n == this.b) {
            this.i = -9223372036854775807L;
            n = i;
        }
        return ((n)ai.a((Object)this.e)).a(array, array2, array3, array4, n);
    }
    
    @Override
    public void a(final long n) {
        ((n)ai.a((Object)this.e)).a(n);
    }
    
    @Override
    public void a(final long n, final boolean b) {
        ((n)ai.a((Object)this.e)).a(n, b);
    }
    
    @Override
    public void a(final n.a f, final long n) {
        this.f = f;
        final n e = this.e;
        if (e != null) {
            e.a((n.a)this, this.e(this.b));
        }
    }
    
    @Override
    public void a(final n n) {
        ((n.a)ai.a((Object)this.f)).a((n)this);
        final a g = this.g;
        if (g != null) {
            g.a(this.a);
        }
    }
    
    public void a(final p.a a) {
        final long e = this.e(this.b);
        final n b = ((p)a.b((Object)this.d)).b(a, this.c, e);
        this.e = b;
        if (this.f != null) {
            b.a((n.a)this, e);
        }
    }
    
    public void a(final p d) {
        com.applovin.exoplayer2.l.a.b(this.d == null);
        this.d = d;
    }
    
    @Override
    public long b(final long n) {
        return ((n)ai.a((Object)this.e)).b(n);
    }
    
    @Override
    public ad b() {
        return ((n)ai.a((Object)this.e)).b();
    }
    
    public void b(final n n) {
        ((y.a<k>)ai.a((Object)this.f)).a(this);
    }
    
    @Override
    public long c() {
        return ((n)ai.a((Object)this.e)).c();
    }
    
    @Override
    public boolean c(final long n) {
        final n e = this.e;
        return e != null && e.c(n);
    }
    
    @Override
    public long d() {
        return ((n)ai.a((Object)this.e)).d();
    }
    
    public void d(final long i) {
        this.i = i;
    }
    
    @Override
    public long e() {
        return ((n)ai.a((Object)this.e)).e();
    }
    
    @Override
    public void e_() throws IOException {
        try {
            final n e = this.e;
            if (e != null) {
                e.e_();
            }
            else {
                final p d = this.d;
                if (d != null) {
                    d.e();
                }
            }
        }
        catch (final IOException ex) {
            final a g = this.g;
            if (g == null) {
                throw ex;
            }
            if (!this.h) {
                this.h = true;
                g.a(this.a, ex);
            }
        }
    }
    
    @Override
    public boolean f() {
        final n e = this.e;
        return e != null && e.f();
    }
    
    public long g() {
        return this.b;
    }
    
    public long h() {
        return this.i;
    }
    
    public void i() {
        if (this.e != null) {
            ((p)com.applovin.exoplayer2.l.a.b((Object)this.d)).a(this.e);
        }
    }
    
    public interface a
    {
        void a(final p.a p0);
        
        void a(final p.a p0, final IOException p1);
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.applovin.exoplayer2.h.a;

import android.os.BaseBundle;
import androidx.annotation.CheckResult;
import \u300780\u3007808\u3007O.\u3007o00\u3007\u3007Oo;
import android.net.Uri;
import java.util.Arrays;
import com.applovin.exoplayer2.l.ai;
import androidx.annotation.IntRange;
import java.util.ArrayList;
import android.os.Bundle;
import \u300780\u3007808\u3007O.\u3007080;
import androidx.annotation.Nullable;
import com.applovin.exoplayer2.g;

public final class a implements g
{
    public static final a a;
    public static final g.a<a> g;
    private static final a h;
    @Nullable
    public final Object b;
    public final int c;
    public final long d;
    public final long e;
    public final int f;
    private final a[] i;
    
    static {
        a = new a(null, new a[0], 0L, -9223372036854775807L, 0);
        h = new a(0L).b(0);
        g = new \u3007080();
    }
    
    private a(@Nullable final Object b, final a[] i, final long d, final long e, final int f) {
        this.b = b;
        this.d = d;
        this.e = e;
        this.c = i.length + f;
        this.i = i;
        this.f = f;
    }
    
    private static a a(final Bundle bundle) {
        final ArrayList parcelableArrayList = bundle.getParcelableArrayList(b(1));
        int i = 0;
        a[] array;
        if (parcelableArrayList == null) {
            array = new a[0];
        }
        else {
            array = new a[parcelableArrayList.size()];
            while (i < parcelableArrayList.size()) {
                array[i] = com.applovin.exoplayer2.h.a.a.a.h.fromBundle(parcelableArrayList.get(i));
                ++i;
            }
        }
        return new a(null, array, ((BaseBundle)bundle).getLong(b(2), 0L), ((BaseBundle)bundle).getLong(b(3), -9223372036854775807L), ((BaseBundle)bundle).getInt(b(4)));
    }
    
    private boolean a(final long n, final long n2, final int n3) {
        boolean b = false;
        final boolean b2 = false;
        if (n == Long.MIN_VALUE) {
            return false;
        }
        final long a = this.a(n3).a;
        if (a == Long.MIN_VALUE) {
            if (n2 != -9223372036854775807L) {
                final boolean b3 = b2;
                if (n >= n2) {
                    return b3;
                }
            }
            return true;
        }
        if (n < a) {
            b = true;
        }
        return b;
    }
    
    private static String b(final int i) {
        return Integer.toString(i, 36);
    }
    
    public int a(final long n, final long n2) {
        int n3;
        for (n3 = this.c - 1; n3 >= 0 && this.a(n, n2, n3); --n3) {}
        if (n3 < 0 || !this.a(n3).c()) {
            n3 = -1;
        }
        return n3;
    }
    
    public a a(@IntRange(from = 0L) final int n) {
        final int f = this.f;
        a h;
        if (n < f) {
            h = com.applovin.exoplayer2.h.a.a.h;
        }
        else {
            h = this.i[n - f];
        }
        return h;
    }
    
    public int b(final long n, final long n2) {
        int n4;
        final int n3 = n4 = -1;
        if (n != Long.MIN_VALUE) {
            if (n2 != -9223372036854775807L && n >= n2) {
                n4 = n3;
            }
            else {
                int f;
                for (f = this.f; f < this.c && ((this.a(f).a != Long.MIN_VALUE && this.a(f).a <= n) || !this.a(f).b()); ++f) {}
                n4 = n3;
                if (f < this.c) {
                    n4 = f;
                }
            }
        }
        return n4;
    }
    
    @Override
    public boolean equals(@Nullable final Object o) {
        boolean b = true;
        if (this == o) {
            return true;
        }
        if (o != null && a.class == o.getClass()) {
            final a a = (a)o;
            if (!ai.a(this.b, a.b) || this.c != a.c || this.d != a.d || this.e != a.e || this.f != a.f || !Arrays.equals(this.i, a.i)) {
                b = false;
            }
            return b;
        }
        return false;
    }
    
    @Override
    public int hashCode() {
        final int c = this.c;
        final Object b = this.b;
        int hashCode;
        if (b == null) {
            hashCode = 0;
        }
        else {
            hashCode = b.hashCode();
        }
        return ((((c * 31 + hashCode) * 31 + (int)this.d) * 31 + (int)this.e) * 31 + this.f) * 31 + Arrays.hashCode(this.i);
    }
    
    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append("AdPlaybackState(adsId=");
        sb.append(this.b);
        sb.append(", adResumePositionUs=");
        sb.append(this.d);
        sb.append(", adGroups=[");
        for (int i = 0; i < this.i.length; ++i) {
            sb.append("adGroup(timeUs=");
            sb.append(this.i[i].a);
            sb.append(", ads=[");
            for (int j = 0; j < this.i[i].d.length; ++j) {
                sb.append("ad(state=");
                final int n = this.i[i].d[j];
                if (n != 0) {
                    if (n != 1) {
                        if (n != 2) {
                            if (n != 3) {
                                if (n != 4) {
                                    sb.append('?');
                                }
                                else {
                                    sb.append('!');
                                }
                            }
                            else {
                                sb.append('P');
                            }
                        }
                        else {
                            sb.append('S');
                        }
                    }
                    else {
                        sb.append('R');
                    }
                }
                else {
                    sb.append('_');
                }
                sb.append(", durationUs=");
                sb.append(this.i[i].e[j]);
                sb.append(')');
                if (j < this.i[i].d.length - 1) {
                    sb.append(", ");
                }
            }
            sb.append("])");
            if (i < this.i.length - 1) {
                sb.append(", ");
            }
        }
        sb.append("])");
        return sb.toString();
    }
    
    public static final class a implements g
    {
        public static final g.a<a> h;
        public final long a;
        public final int b;
        public final Uri[] c;
        public final int[] d;
        public final long[] e;
        public final long f;
        public final boolean g;
        
        static {
            h = new \u3007o00\u3007\u3007Oo();
        }
        
        public a(final long n) {
            this(n, -1, new int[0], new Uri[0], new long[0], 0L, false);
        }
        
        private a(final long a, final int b, final int[] d, final Uri[] c, final long[] e, final long f, final boolean g) {
            com.applovin.exoplayer2.l.a.a(d.length == c.length);
            this.a = a;
            this.b = b;
            this.d = d;
            this.c = c;
            this.e = e;
            this.f = f;
            this.g = g;
        }
        
        private static a a(final Bundle bundle) {
            final long long1 = ((BaseBundle)bundle).getLong(c(0));
            final int int1 = ((BaseBundle)bundle).getInt(c(1), -1);
            final ArrayList parcelableArrayList = bundle.getParcelableArrayList(c(2));
            final int[] intArray = ((BaseBundle)bundle).getIntArray(c(3));
            long[] longArray = ((BaseBundle)bundle).getLongArray(c(4));
            final long long2 = ((BaseBundle)bundle).getLong(c(5));
            final boolean boolean1 = bundle.getBoolean(c(6));
            int[] array = intArray;
            if (intArray == null) {
                array = new int[0];
            }
            Uri[] array2;
            if (parcelableArrayList == null) {
                array2 = new Uri[0];
            }
            else {
                array2 = parcelableArrayList.toArray(new Uri[0]);
            }
            if (longArray == null) {
                longArray = new long[0];
            }
            return new a(long1, int1, array, array2, longArray, long2, boolean1);
        }
        
        @CheckResult
        private static int[] a(int[] copy, int max) {
            final int length = copy.length;
            max = Math.max(max, length);
            copy = Arrays.copyOf(copy, max);
            Arrays.fill(copy, length, max, 0);
            return copy;
        }
        
        @CheckResult
        private static long[] a(long[] copy, int max) {
            final int length = copy.length;
            max = Math.max(max, length);
            copy = Arrays.copyOf(copy, max);
            Arrays.fill(copy, length, max, -9223372036854775807L);
            return copy;
        }
        
        private static String c(final int i) {
            return Integer.toString(i, 36);
        }
        
        public int a() {
            return this.a(-1);
        }
        
        public int a(@IntRange(from = -1L) int n) {
            ++n;
            while (true) {
                final int[] d = this.d;
                if (n >= d.length || this.g) {
                    break;
                }
                final int n2 = d[n];
                if (n2 == 0) {
                    break;
                }
                if (n2 == 1) {
                    break;
                }
                ++n;
            }
            return n;
        }
        
        @CheckResult
        public a b(final int newLength) {
            return new a(this.a, newLength, a(this.d, newLength), Arrays.copyOf(this.c, newLength), a(this.e, newLength), this.f, this.g);
        }
        
        public boolean b() {
            return this.b == -1 || this.a() < this.b;
        }
        
        public boolean c() {
            if (this.b == -1) {
                return true;
            }
            for (int i = 0; i < this.b; ++i) {
                final int n = this.d[i];
                if (n == 0 || n == 1) {
                    return true;
                }
            }
            return false;
        }
        
        @Override
        public boolean equals(@Nullable final Object o) {
            boolean b = true;
            if (this == o) {
                return true;
            }
            if (o != null && a.class == o.getClass()) {
                final a a = (a)o;
                if (this.a != a.a || this.b != a.b || !Arrays.equals(this.c, a.c) || !Arrays.equals(this.d, a.d) || !Arrays.equals(this.e, a.e) || this.f != a.f || this.g != a.g) {
                    b = false;
                }
                return b;
            }
            return false;
        }
        
        @Override
        public int hashCode() {
            final int b = this.b;
            final long a = this.a;
            final int n = (int)(a ^ a >>> 32);
            final int hashCode = Arrays.hashCode(this.c);
            final int hashCode2 = Arrays.hashCode(this.d);
            final int hashCode3 = Arrays.hashCode(this.e);
            final long f = this.f;
            return (((((b * 31 + n) * 31 + hashCode) * 31 + hashCode2) * 31 + hashCode3) * 31 + (int)(f ^ f >>> 32)) * 31 + (this.g ? 1 : 0);
        }
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.applovin.exoplayer2.h;

import com.applovin.exoplayer2.k.aa;
import java.util.Iterator;
import android.os.Handler;
import com.applovin.exoplayer2.ba;
import androidx.annotation.Nullable;
import android.os.Looper;
import com.applovin.exoplayer2.d.g;
import java.util.HashSet;
import java.util.ArrayList;

public abstract class a implements p
{
    private final ArrayList<b> a;
    private final HashSet<b> b;
    private final q.a c;
    private final g.a d;
    @Nullable
    private Looper e;
    @Nullable
    private ba f;
    
    public a() {
        this.a = new ArrayList<b>(1);
        this.b = new HashSet<b>(1);
        this.c = new q.a();
        this.d = new g.a();
    }
    
    protected final g.a a(final int n, @Nullable final p.a a) {
        return this.d.a(n, a);
    }
    
    protected final q.a a(final int n, @Nullable final p.a a, final long n2) {
        return this.c.a(n, a, n2);
    }
    
    protected final q.a a(@Nullable final p.a a) {
        return this.c.a(0, a, 0L);
    }
    
    protected void a() {
    }
    
    @Override
    public final void a(final Handler handler, final g g) {
        com.applovin.exoplayer2.l.a.b((Object)handler);
        com.applovin.exoplayer2.l.a.b((Object)g);
        this.d.a(handler, g);
    }
    
    @Override
    public final void a(final Handler handler, final q q) {
        com.applovin.exoplayer2.l.a.b((Object)handler);
        com.applovin.exoplayer2.l.a.b((Object)q);
        this.c.a(handler, q);
    }
    
    protected final void a(final ba f) {
        this.f = f;
        final Iterator<b> iterator = this.a.iterator();
        while (iterator.hasNext()) {
            iterator.next().onSourceInfoRefreshed(this, f);
        }
    }
    
    @Override
    public final void a(final g g) {
        this.d.a(g);
    }
    
    @Override
    public final void a(final b e) {
        com.applovin.exoplayer2.l.a.b((Object)this.e);
        final boolean empty = this.b.isEmpty();
        this.b.add(e);
        if (empty) {
            this.a();
        }
    }
    
    @Override
    public final void a(final b b, @Nullable final aa aa) {
        final Looper myLooper = Looper.myLooper();
        final Looper e = this.e;
        com.applovin.exoplayer2.l.a.a(e == null || e == myLooper);
        final ba f = this.f;
        this.a.add(b);
        if (this.e == null) {
            this.e = myLooper;
            this.b.add(b);
            this.a(aa);
        }
        else if (f != null) {
            this.a(b);
            b.onSourceInfoRefreshed(this, f);
        }
    }
    
    @Override
    public final void a(final q q) {
        this.c.a(q);
    }
    
    protected abstract void a(@Nullable final aa p0);
    
    protected final g.a b(@Nullable final p.a a) {
        return this.d.a(0, a);
    }
    
    protected void b() {
    }
    
    @Override
    public final void b(final b o) {
        final boolean empty = this.b.isEmpty();
        this.b.remove(o);
        if ((empty ^ true) && this.b.isEmpty()) {
            this.b();
        }
    }
    
    protected abstract void c();
    
    @Override
    public final void c(final b o) {
        this.a.remove(o);
        if (this.a.isEmpty()) {
            this.e = null;
            this.f = null;
            this.b.clear();
            this.c();
        }
        else {
            this.b(o);
        }
    }
    
    protected final boolean d() {
        return this.b.isEmpty() ^ true;
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.applovin.exoplayer2.h;

import androidx.annotation.VisibleForTesting;
import com.applovin.exoplayer2.l.ai;
import com.applovin.exoplayer2.ab;
import android.util.Pair;
import com.applovin.exoplayer2.l.a;
import com.applovin.exoplayer2.k.aa;
import com.applovin.exoplayer2.k.b;
import androidx.annotation.Nullable;
import com.applovin.exoplayer2.ba;

public final class l extends e<Void>
{
    private final p a;
    private final boolean b;
    private final ba.c c;
    private final ba.a d;
    private a e;
    @Nullable
    private k f;
    private boolean g;
    private boolean h;
    private boolean i;
    
    public l(final p a, final boolean b) {
        this.a = a;
        this.b = (b && a.i());
        this.c = new ba.c();
        this.d = new ba.a();
        final ba h = a.h();
        if (h != null) {
            this.e = l.a.a(h, null, null);
            this.i = true;
        }
        else {
            this.e = l.a.a(a.g());
        }
    }
    
    private Object a(final Object o) {
        Object a = o;
        if (this.e.f != null) {
            a = o;
            if (o.equals(l.a.d)) {
                a = this.e.f;
            }
        }
        return a;
    }
    
    private void a(final long n) {
        final k f = this.f;
        final int c = this.e.c(f.a.a);
        if (c == -1) {
            return;
        }
        final long d = this.e.a(c, this.d).d;
        long max = n;
        if (d != -9223372036854775807L) {
            max = n;
            if (n >= d) {
                max = Math.max(0L, d - 1L);
            }
        }
        f.d(max);
    }
    
    private Object b(final Object obj) {
        Object d = obj;
        if (this.e.f != null) {
            d = obj;
            if (this.e.f.equals(obj)) {
                d = l.a.d;
            }
        }
        return d;
    }
    
    public k a(final p.a a, final com.applovin.exoplayer2.k.b b, final long n) {
        final k f = new k(a, b, n);
        f.a(this.a);
        if (this.h) {
            f.a(a.b(this.a(a.a)));
        }
        else {
            this.f = f;
            if (!this.g) {
                this.g = true;
                this.a(null, this.a);
            }
        }
        return f;
    }
    
    @Nullable
    @Override
    protected p.a a(final Void void1, final p.a a) {
        return a.b(this.b(a.a));
    }
    
    @Override
    public void a(final n n) {
        ((k)n).i();
        if (n == this.f) {
            this.f = null;
        }
    }
    
    public void a(@Nullable final aa aa) {
        super.a(aa);
        if (!this.b) {
            this.g = true;
            this.a(null, this.a);
        }
    }
    
    @Override
    protected void a(final Void void1, final p p3, final ba ba) {
        o b3 = null;
        Label_0292: {
            if (this.h) {
                this.e = this.e.a(ba);
                final k f = this.f;
                if (f != null) {
                    this.a(f.h());
                }
            }
            else if (ba.d()) {
                a e;
                if (this.i) {
                    e = this.e.a(ba);
                }
                else {
                    e = l.a.a(ba, com.applovin.exoplayer2.ba.c.a, l.a.d);
                }
                this.e = e;
            }
            else {
                ba.a(0, this.c);
                long b = this.c.b();
                final Object b2 = this.c.b;
                final k f2 = this.f;
                if (f2 != null) {
                    final long g = f2.g();
                    this.e.a(this.f.a.a, this.d);
                    final long n = this.d.c() + g;
                    if (n != this.e.a(0, this.c).b()) {
                        b = n;
                    }
                }
                final Pair<Object, Long> a = ba.a(this.c, this.d, 0, b);
                final Object first = a.first;
                final long longValue = (long)a.second;
                a e2;
                if (this.i) {
                    e2 = this.e.a(ba);
                }
                else {
                    e2 = l.a.a(ba, b2, first);
                }
                this.e = e2;
                final k f3 = this.f;
                if (f3 != null) {
                    this.a(longValue);
                    final p.a a2 = f3.a;
                    b3 = a2.b(this.a(a2.a));
                    break Label_0292;
                }
            }
            b3 = null;
        }
        this.i = true;
        this.h = true;
        this.a(this.e);
        if (b3 != null) {
            ((k)com.applovin.exoplayer2.l.a.b((Object)this.f)).a((p.a)b3);
        }
    }
    
    public void c() {
        this.h = false;
        this.g = false;
        super.c();
    }
    
    @Override
    public void e() {
    }
    
    public ba f() {
        return this.e;
    }
    
    @Override
    public ab g() {
        return this.a.g();
    }
    
    private static final class a extends h
    {
        public static final Object d;
        @Nullable
        private final Object e;
        @Nullable
        private final Object f;
        
        static {
            d = new Object();
        }
        
        private a(final ba ba, @Nullable final Object e, @Nullable final Object f) {
            super(ba);
            this.e = e;
            this.f = f;
        }
        
        public static a a(final ab ab) {
            return new a(new l.b(ab), c.a, a.d);
        }
        
        public static a a(final ba ba, @Nullable final Object o, @Nullable final Object o2) {
            return new a(ba, o, o2);
        }
        
        @Override
        public ba.a a(final int n, final ba.a a, final boolean b) {
            super.c.a(n, a, b);
            if (ai.a(a.b, this.f) && b) {
                a.b = l.a.d;
            }
            return a;
        }
        
        @Override
        public c a(final int n, final c c, final long n2) {
            super.c.a(n, c, n2);
            if (ai.a(c.b, this.e)) {
                c.b = ba.c.a;
            }
            return c;
        }
        
        public a a(final ba ba) {
            return new a(ba, this.e, this.f);
        }
        
        @Override
        public Object a(final int n) {
            Object o;
            if (ai.a(o = super.c.a(n), this.f)) {
                o = a.d;
            }
            return o;
        }
        
        @Override
        public int c(final Object obj) {
            final ba c = super.c;
            Object o = obj;
            if (a.d.equals(obj)) {
                final Object f = this.f;
                o = obj;
                if (f != null) {
                    o = f;
                }
            }
            return c.c(o);
        }
    }
    
    @VisibleForTesting
    public static final class b extends ba
    {
        private final ab c;
        
        public b(final ab c) {
            this.c = c;
        }
        
        @Override
        public ba.a a(final int n, final ba.a a, final boolean b) {
            Object d = null;
            Integer value;
            if (b) {
                value = 0;
            }
            else {
                value = null;
            }
            if (b) {
                d = l.a.d;
            }
            a.a(value, d, 0, -9223372036854775807L, 0L, com.applovin.exoplayer2.h.a.a.a, true);
            return a;
        }
        
        @Override
        public c a(final int n, final c c, final long n2) {
            c.a(ba.c.a, this.c, null, -9223372036854775807L, -9223372036854775807L, -9223372036854775807L, false, true, null, 0L, -9223372036854775807L, 0, 0, 0L);
            c.m = true;
            return c;
        }
        
        @Override
        public Object a(final int n) {
            return l.a.d;
        }
        
        @Override
        public int b() {
            return 1;
        }
        
        @Override
        public int c() {
            return 1;
        }
        
        @Override
        public int c(final Object o) {
            int n;
            if (o == l.a.d) {
                n = 0;
            }
            else {
                n = -1;
            }
            return n;
        }
    }
}

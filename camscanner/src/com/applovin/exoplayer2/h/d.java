// 
// Decompiled by Procyon v0.6.0
// 

package com.applovin.exoplayer2.h;

import com.applovin.exoplayer2.c.g;
import com.applovin.exoplayer2.w;
import java.io.IOException;
import com.applovin.exoplayer2.l.a;
import com.applovin.exoplayer2.l.ai;
import com.applovin.exoplayer2.av;
import com.applovin.exoplayer2.v;
import com.applovin.exoplayer2.l.u;
import androidx.annotation.Nullable;

public final class d implements n, n.a
{
    public final n a;
    long b;
    long c;
    @Nullable
    private n.a d;
    private a[] e;
    private long f;
    
    public d(final n a, final boolean b, final long b2, final long c) {
        this.a = a;
        this.e = new a[0];
        long f;
        if (b) {
            f = b2;
        }
        else {
            f = -9223372036854775807L;
        }
        this.f = f;
        this.b = b2;
        this.c = c;
    }
    
    private static boolean a(final long n, final com.applovin.exoplayer2.j.d[] array) {
        if (n != 0L) {
            for (final com.applovin.exoplayer2.j.d d : array) {
                if (d != null) {
                    final v f = d.f();
                    if (!u.a(f.l, f.i)) {
                        return true;
                    }
                }
            }
        }
        return false;
    }
    
    private av b(long a, final av av) {
        final long a2 = ai.a(av.f, 0L, a - this.b);
        final long g = av.g;
        final long c = this.c;
        if (c == Long.MIN_VALUE) {
            a = Long.MAX_VALUE;
        }
        else {
            a = c - a;
        }
        a = ai.a(g, 0L, a);
        if (a2 == av.f && a == av.g) {
            return av;
        }
        return new av(a2, a);
    }
    
    @Override
    public long a(final long n, av b) {
        final long b2 = this.b;
        if (n == b2) {
            return b2;
        }
        b = this.b(n, b);
        return this.a.a(n, b);
    }
    
    @Override
    public long a(final com.applovin.exoplayer2.j.d[] array, final boolean[] array2, final x[] array3, final boolean[] array4, long c) {
        this.e = new a[array3.length];
        final x[] array5 = new x[array3.length];
        final int n = 0;
        int n2 = 0;
        while (true) {
            final int length = array3.length;
            x a = null;
            if (n2 >= length) {
                break;
            }
            final a[] e = this.e;
            final a a2 = (a)array3[n2];
            if ((e[n2] = a2) != null) {
                a = a2.a;
            }
            array5[n2] = a;
            ++n2;
        }
        final long a3 = this.a.a(array, array2, array5, array4, c);
        long f = 0L;
        Label_0144: {
            if (this.g()) {
                final long b = this.b;
                if (c == b && a(b, array)) {
                    f = a3;
                    break Label_0144;
                }
            }
            f = -9223372036854775807L;
        }
        this.f = f;
        boolean b2 = false;
        Label_0203: {
            Label_0200: {
                if (a3 != c) {
                    if (a3 >= this.b) {
                        c = this.c;
                        if (c == Long.MIN_VALUE) {
                            break Label_0200;
                        }
                        if (a3 <= c) {
                            break Label_0200;
                        }
                    }
                    b2 = false;
                    break Label_0203;
                }
            }
            b2 = true;
        }
        com.applovin.exoplayer2.l.a.b(b2);
        for (int i = n; i < array3.length; ++i) {
            final x x = array5[i];
            if (x == null) {
                this.e[i] = null;
            }
            else {
                final a[] e2 = this.e;
                final a a4 = e2[i];
                if (a4 == null || a4.a != x) {
                    e2[i] = new a(x);
                }
            }
            array3[i] = this.e[i];
        }
        return a3;
    }
    
    @Override
    public void a(final long n) {
        this.a.a(n);
    }
    
    public void a(final long b, final long c) {
        this.b = b;
        this.c = c;
    }
    
    @Override
    public void a(final long n, final boolean b) {
        this.a.a(n, b);
    }
    
    @Override
    public void a(final n.a d, final long n) {
        this.d = d;
        this.a.a((n.a)this, n);
    }
    
    @Override
    public void a(final n n) {
        ((n.a)com.applovin.exoplayer2.l.a.b((Object)this.d)).a((n)this);
    }
    
    @Override
    public long b(long c) {
        this.f = -9223372036854775807L;
        final a[] e = this.e;
        final int length = e.length;
        final boolean b = false;
        for (final a a : e) {
            if (a != null) {
                a.a();
            }
        }
        final long b2 = this.a.b(c);
        boolean b3 = false;
        Label_0111: {
            if (b2 != c) {
                b3 = b;
                if (b2 < this.b) {
                    break Label_0111;
                }
                c = this.c;
                if (c != Long.MIN_VALUE) {
                    b3 = b;
                    if (b2 > c) {
                        break Label_0111;
                    }
                }
            }
            b3 = true;
        }
        com.applovin.exoplayer2.l.a.b(b3);
        return b2;
    }
    
    @Override
    public ad b() {
        return this.a.b();
    }
    
    public void b(final n n) {
        ((y.a<d>)com.applovin.exoplayer2.l.a.b((Object)this.d)).a(this);
    }
    
    @Override
    public long c() {
        if (this.g()) {
            long f = this.f;
            this.f = -9223372036854775807L;
            final long c = this.c();
            if (c != -9223372036854775807L) {
                f = c;
            }
            return f;
        }
        final long c2 = this.a.c();
        if (c2 == -9223372036854775807L) {
            return -9223372036854775807L;
        }
        final long b = this.b;
        final boolean b2 = true;
        com.applovin.exoplayer2.l.a.b(c2 >= b);
        final long c3 = this.c;
        boolean b3 = b2;
        if (c3 != Long.MIN_VALUE) {
            b3 = (c2 <= c3 && b2);
        }
        com.applovin.exoplayer2.l.a.b(b3);
        return c2;
    }
    
    @Override
    public boolean c(final long n) {
        return this.a.c(n);
    }
    
    @Override
    public long d() {
        final long d = this.a.d();
        if (d != Long.MIN_VALUE) {
            final long c = this.c;
            if (c == Long.MIN_VALUE || d < c) {
                return d;
            }
        }
        return Long.MIN_VALUE;
    }
    
    @Override
    public long e() {
        final long e = this.a.e();
        if (e != Long.MIN_VALUE) {
            final long c = this.c;
            if (c == Long.MIN_VALUE || e < c) {
                return e;
            }
        }
        return Long.MIN_VALUE;
    }
    
    @Override
    public void e_() throws IOException {
        this.a.e_();
    }
    
    @Override
    public boolean f() {
        return this.a.f();
    }
    
    boolean g() {
        return this.f != -9223372036854775807L;
    }
    
    private final class a implements x
    {
        public final x a;
        final d b;
        private boolean c;
        
        public a(final d b, final x a) {
            this.b = b;
            this.a = a;
        }
        
        @Override
        public int a(final long n) {
            if (this.b.g()) {
                return -3;
            }
            return this.a.a(n);
        }
        
        @Override
        public int a(final w w, final g g, int n) {
            if (this.b.g()) {
                return -3;
            }
            if (this.c) {
                g.a_(4);
                return -4;
            }
            n = this.a.a(w, g, n);
            if (n == -5) {
                final v v = (v)com.applovin.exoplayer2.l.a.b((Object)w.b);
                n = v.B;
                if (n != 0 || v.C != 0) {
                    final d b = this.b;
                    final long b2 = b.b;
                    int c = 0;
                    if (b2 != 0L) {
                        n = 0;
                    }
                    if (b.c == Long.MIN_VALUE) {
                        c = v.C;
                    }
                    w.b = v.a().n(n).o(c).a();
                }
                return -5;
            }
            final d b3 = this.b;
            final long c2 = b3.c;
            if (c2 != Long.MIN_VALUE && ((n == -4 && g.d >= c2) || (n == -3 && b3.d() == Long.MIN_VALUE && !g.c))) {
                g.a();
                g.a_(4);
                this.c = true;
                return -4;
            }
            return n;
        }
        
        public void a() {
            this.c = false;
        }
        
        @Override
        public boolean b() {
            return !this.b.g() && this.a.b();
        }
        
        @Override
        public void c() throws IOException {
            this.a.c();
        }
    }
}

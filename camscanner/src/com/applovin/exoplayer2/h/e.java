// 
// Decompiled by Procyon v0.6.0
// 

package com.applovin.exoplayer2.h;

import com.applovin.exoplayer2.d.OO0o\u3007\u3007\u3007\u30070;
import java.io.IOException;
import com.applovin.exoplayer2.d.g;
import com.applovin.exoplayer2.l.ai;
import androidx.annotation.CallSuper;
import java.util.Iterator;
import com.applovin.exoplayer2.ba;
import com.applovin.exoplayer2.k.aa;
import androidx.annotation.Nullable;
import android.os.Handler;
import java.util.HashMap;

public abstract class e<T> extends com.applovin.exoplayer2.h.a
{
    private final HashMap<T, b<T>> a;
    @Nullable
    private Handler b;
    @Nullable
    private aa c;
    
    protected e() {
        this.a = new HashMap<T, b<T>>();
    }
    
    protected int a(final T t, final int n) {
        return n;
    }
    
    protected long a(final T t, final long n) {
        return n;
    }
    
    @Nullable
    protected p.a a(final T t, final p.a a) {
        return a;
    }
    
    @CallSuper
    @Override
    protected void a() {
        for (final b b : this.a.values()) {
            b.a.a(b.b);
        }
    }
    
    @CallSuper
    @Override
    protected void a(@Nullable final aa c) {
        this.c = c;
        this.b = ai.a();
    }
    
    protected final void a(final T t, final p p2) {
        com.applovin.exoplayer2.l.a.a(this.a.containsKey(t) ^ true);
        final O8 o8 = new O8(this, t);
        final a a = new a(t);
        this.a.put(t, new b<T>(p2, o8, a));
        p2.a((Handler)com.applovin.exoplayer2.l.a.b((Object)this.b), (q)a);
        p2.a((Handler)com.applovin.exoplayer2.l.a.b((Object)this.b), (g)a);
        p2.a((p.b)o8, this.c);
        if (!this.d()) {
            p2.b((p.b)o8);
        }
    }
    
    protected abstract void a(final T p0, final p p1, final ba p2);
    
    @CallSuper
    @Override
    protected void b() {
        for (final b b : this.a.values()) {
            b.a.b(b.b);
        }
    }
    
    @CallSuper
    @Override
    protected void c() {
        for (final b b : this.a.values()) {
            b.a.c(b.b);
            b.a.a((q)b.c);
            b.a.a((g)b.c);
        }
        this.a.clear();
    }
    
    @CallSuper
    @Override
    public void e() throws IOException {
        final Iterator<b<T>> iterator = this.a.values().iterator();
        while (iterator.hasNext()) {
            iterator.next().a.e();
        }
    }
    
    private final class a implements g, q
    {
        final e a;
        private final T b;
        private q.a c;
        private g.a d;
        
        public a(final e a, final T b) {
            this.a = a;
            this.c = a.a((p.a)null);
            this.d = a.b((p.a)null);
            this.b = b;
        }
        
        private m a(final m m) {
            final long a = this.a.a(this.b, m.f);
            final long a2 = this.a.a(this.b, m.g);
            if (a == m.f && a2 == m.g) {
                return m;
            }
            return new m(m.a, m.b, m.c, m.d, m.e, a, a2);
        }
        
        private boolean f(int a, @Nullable p.a a2) {
            if (a2 != null) {
                if ((a2 = this.a.a(this.b, a2)) == null) {
                    return false;
                }
            }
            else {
                a2 = null;
            }
            a = this.a.a(this.b, a);
            final q.a c = this.c;
            if (c.a != a || !ai.a((Object)c.b, (Object)a2)) {
                this.c = this.a.a(a, a2, 0L);
            }
            final g.a d = this.d;
            if (d.a != a || !ai.a((Object)d.b, (Object)a2)) {
                this.d = this.a.a(a, a2);
            }
            return true;
        }
        
        @Override
        public void a(final int n, @Nullable final p.a a) {
            if (this.f(n, a)) {
                this.d.a();
            }
        }
        
        @Override
        public void a(final int n, @Nullable final p.a a, final int n2) {
            if (this.f(n, a)) {
                this.d.a(n2);
            }
        }
        
        @Override
        public void a(final int n, @Nullable final p.a a, final j j, final m m) {
            if (this.f(n, a)) {
                this.c.a(j, this.a(m));
            }
        }
        
        @Override
        public void a(final int n, @Nullable final p.a a, final j j, final m m, final IOException ex, final boolean b) {
            if (this.f(n, a)) {
                this.c.a(j, this.a(m), ex, b);
            }
        }
        
        @Override
        public void a(final int n, @Nullable final p.a a, final m m) {
            if (this.f(n, a)) {
                this.c.a(this.a(m));
            }
        }
        
        @Override
        public void a(final int n, @Nullable final p.a a, final Exception ex) {
            if (this.f(n, a)) {
                this.d.a(ex);
            }
        }
        
        @Override
        public void b(final int n, @Nullable final p.a a) {
            if (this.f(n, a)) {
                this.d.b();
            }
        }
        
        @Override
        public void b(final int n, @Nullable final p.a a, final j j, final m m) {
            if (this.f(n, a)) {
                this.c.b(j, this.a(m));
            }
        }
        
        @Override
        public void c(final int n, @Nullable final p.a a) {
            if (this.f(n, a)) {
                this.d.c();
            }
        }
        
        @Override
        public void c(final int n, @Nullable final p.a a, final j j, final m m) {
            if (this.f(n, a)) {
                this.c.c(j, this.a(m));
            }
        }
        
        @Override
        public void d(final int n, @Nullable final p.a a) {
            if (this.f(n, a)) {
                this.d.d();
            }
        }
    }
    
    private static final class b<T>
    {
        public final p a;
        public final p.b b;
        public final a c;
        
        public b(final p a, final p.b b, final a c) {
            this.a = a;
            this.b = b;
            this.c = c;
        }
    }
}

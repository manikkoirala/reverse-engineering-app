// 
// Decompiled by Procyon v0.6.0
// 

package com.applovin.exoplayer2.h;

import com.applovin.exoplayer2.ba;

public abstract class h extends ba
{
    protected final ba c;
    
    public h(final ba c) {
        this.c = c;
    }
    
    @Override
    public int a(final int n, final int n2, final boolean b) {
        return this.c.a(n, n2, b);
    }
    
    @Override
    public int a(final boolean b) {
        return this.c.a(b);
    }
    
    @Override
    public a a(final int n, final a a, final boolean b) {
        return this.c.a(n, a, b);
    }
    
    @Override
    public c a(final int n, final c c, final long n2) {
        return this.c.a(n, c, n2);
    }
    
    @Override
    public Object a(final int n) {
        return this.c.a(n);
    }
    
    @Override
    public int b() {
        return this.c.b();
    }
    
    @Override
    public int b(final int n, final int n2, final boolean b) {
        return this.c.b(n, n2, b);
    }
    
    @Override
    public int b(final boolean b) {
        return this.c.b(b);
    }
    
    @Override
    public int c() {
        return this.c.c();
    }
    
    @Override
    public int c(final Object o) {
        return this.c.c(o);
    }
}

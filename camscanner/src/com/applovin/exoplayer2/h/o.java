// 
// Decompiled by Procyon v0.6.0
// 

package com.applovin.exoplayer2.h;

import androidx.annotation.Nullable;

public class o
{
    public final Object a;
    public final int b;
    public final int c;
    public final long d;
    public final int e;
    
    protected o(final o o) {
        this.a = o.a;
        this.b = o.b;
        this.c = o.c;
        this.d = o.d;
        this.e = o.e;
    }
    
    public o(final Object o) {
        this(o, -1L);
    }
    
    public o(final Object o, final int n, final int n2, final long n3) {
        this(o, n, n2, n3, -1);
    }
    
    private o(final Object a, final int b, final int c, final long d, final int e) {
        this.a = a;
        this.b = b;
        this.c = c;
        this.d = d;
        this.e = e;
    }
    
    public o(final Object o, final long n) {
        this(o, -1, -1, n, -1);
    }
    
    public o(final Object o, final long n, final int n2) {
        this(o, -1, -1, n, n2);
    }
    
    public o a(final Object obj) {
        o o;
        if (this.a.equals(obj)) {
            o = this;
        }
        else {
            o = new o(obj, this.b, this.c, this.d, this.e);
        }
        return o;
    }
    
    public boolean a() {
        return this.b != -1;
    }
    
    @Override
    public boolean equals(@Nullable final Object o) {
        boolean b = true;
        if (this == o) {
            return true;
        }
        if (!(o instanceof o)) {
            return false;
        }
        final o o2 = (o)o;
        if (!this.a.equals(o2.a) || this.b != o2.b || this.c != o2.c || this.d != o2.d || this.e != o2.e) {
            b = false;
        }
        return b;
    }
    
    @Override
    public int hashCode() {
        return ((((527 + this.a.hashCode()) * 31 + this.b) * 31 + this.c) * 31 + (int)this.d) * 31 + this.e;
    }
}

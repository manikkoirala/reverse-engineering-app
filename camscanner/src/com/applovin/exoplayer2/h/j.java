// 
// Decompiled by Procyon v0.6.0
// 

package com.applovin.exoplayer2.h;

import java.util.Collections;
import java.util.List;
import java.util.Map;
import android.net.Uri;
import com.applovin.exoplayer2.k.l;
import java.util.concurrent.atomic.AtomicLong;

public final class j
{
    private static final AtomicLong h;
    public final long a;
    public final l b;
    public final Uri c;
    public final Map<String, List<String>> d;
    public final long e;
    public final long f;
    public final long g;
    
    static {
        h = new AtomicLong();
    }
    
    public j(final long n, final l l, final long n2) {
        this(n, l, l.a, Collections.emptyMap(), n2, 0L, 0L);
    }
    
    public j(final long a, final l b, final Uri c, final Map<String, List<String>> d, final long e, final long f, final long g) {
        this.a = a;
        this.b = b;
        this.c = c;
        this.d = d;
        this.e = e;
        this.f = f;
        this.g = g;
    }
    
    public static long a() {
        return j.h.getAndIncrement();
    }
}

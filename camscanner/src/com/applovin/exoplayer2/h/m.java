// 
// Decompiled by Procyon v0.6.0
// 

package com.applovin.exoplayer2.h;

import androidx.annotation.Nullable;
import com.applovin.exoplayer2.v;

public final class m
{
    public final int a;
    public final int b;
    @Nullable
    public final v c;
    public final int d;
    @Nullable
    public final Object e;
    public final long f;
    public final long g;
    
    public m(final int n) {
        this(n, -1, null, 0, null, -9223372036854775807L, -9223372036854775807L);
    }
    
    public m(final int a, final int b, @Nullable final v c, final int d, @Nullable final Object e, final long f, final long g) {
        this.a = a;
        this.b = b;
        this.c = c;
        this.d = d;
        this.e = e;
        this.f = f;
        this.g = g;
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.applovin.exoplayer2.h;

import com.applovin.exoplayer2.ba;
import com.applovin.exoplayer2.ab;
import java.io.IOException;
import com.applovin.exoplayer2.k.b;
import androidx.annotation.Nullable;
import com.applovin.exoplayer2.k.aa;
import com.applovin.exoplayer2.d.g;
import android.os.Handler;

public interface p
{
    void a(final Handler p0, final g p1);
    
    void a(final Handler p0, final q p1);
    
    void a(final g p0);
    
    void a(final n p0);
    
    void a(final b p0);
    
    void a(final b p0, @Nullable final aa p1);
    
    void a(final q p0);
    
    n b(final a p0, final com.applovin.exoplayer2.k.b p1, final long p2);
    
    void b(final b p0);
    
    void c(final b p0);
    
    void e() throws IOException;
    
    ab g();
    
    @Nullable
    ba h();
    
    boolean i();
    
    public static final class a extends o
    {
        public a(final o o) {
            super(o);
        }
        
        public a(final Object o) {
            super(o);
        }
        
        public a(final Object o, final int n, final int n2, final long n3) {
            super(o, n, n2, n3);
        }
        
        public a(final Object o, final long n, final int n2) {
            super(o, n, n2);
        }
        
        public a b(final Object o) {
            return new a(super.a(o));
        }
    }
    
    public interface b
    {
        void onSourceInfoRefreshed(final p p0, final ba p1);
    }
}

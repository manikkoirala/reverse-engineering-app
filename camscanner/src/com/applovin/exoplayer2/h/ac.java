// 
// Decompiled by Procyon v0.6.0
// 

package com.applovin.exoplayer2.h;

import java.util.Arrays;
import com.applovin.exoplayer2.l.q;
import androidx.annotation.Nullable;
import java.util.List;
import com.applovin.exoplayer2.l.c;
import com.applovin.exoplayer2.common.a.s;
import android.os.Bundle;
import com.applovin.exoplayer2.l.a;
import com.applovin.exoplayer2.v;
import com.applovin.exoplayer2.g;

public final class ac implements g
{
    public static final a<ac> b;
    public final int a;
    private final v[] c;
    private int d;
    
    static {
        b = new \u3007o00\u3007\u3007Oo();
    }
    
    public ac(final v... c) {
        com.applovin.exoplayer2.l.a.a(c.length > 0);
        this.c = c;
        this.a = c.length;
        this.a();
    }
    
    private static String a(@Nullable final String s) {
        if (s != null) {
            final String s2 = s;
            if (!s.equals("und")) {
                return s2;
            }
        }
        return "";
    }
    
    private void a() {
        final String a = a(this.c[0].c);
        final int c = c(this.c[0].e);
        int n = 1;
        while (true) {
            final v[] c2 = this.c;
            if (n >= c2.length) {
                return;
            }
            if (!a.equals(a(c2[n].c))) {
                final v[] c3 = this.c;
                a("languages", c3[0].c, c3[n].c, n);
                return;
            }
            if (c != c(this.c[n].e)) {
                a("role flags", Integer.toBinaryString(this.c[0].e), Integer.toBinaryString(this.c[n].e), n);
                return;
            }
            ++n;
        }
    }
    
    private static void a(final String str, @Nullable final String str2, @Nullable final String str3, final int i) {
        final StringBuilder sb = new StringBuilder();
        sb.append("Different ");
        sb.append(str);
        sb.append(" combined in one TrackGroup: '");
        sb.append(str2);
        sb.append("' (track 0) and '");
        sb.append(str3);
        sb.append("' (track ");
        sb.append(i);
        sb.append(")");
        q.c("TrackGroup", "", (Throwable)new IllegalStateException(sb.toString()));
    }
    
    private static String b(final int i) {
        return Integer.toString(i, 36);
    }
    
    private static int c(final int n) {
        return n | 0x4000;
    }
    
    public int a(final v v) {
        int n = 0;
        while (true) {
            final v[] c = this.c;
            if (n >= c.length) {
                return -1;
            }
            if (v == c[n]) {
                return n;
            }
            ++n;
        }
    }
    
    public v a(final int n) {
        return this.c[n];
    }
    
    @Override
    public boolean equals(@Nullable final Object o) {
        boolean b = true;
        if (this == o) {
            return true;
        }
        if (o != null && ac.class == o.getClass()) {
            final ac ac = (ac)o;
            if (this.a != ac.a || !Arrays.equals(this.c, ac.c)) {
                b = false;
            }
            return b;
        }
        return false;
    }
    
    @Override
    public int hashCode() {
        if (this.d == 0) {
            this.d = 527 + Arrays.hashCode(this.c);
        }
        return this.d;
    }
}

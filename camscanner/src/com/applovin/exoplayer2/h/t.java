// 
// Decompiled by Procyon v0.6.0
// 

package com.applovin.exoplayer2.h;

import java.io.InterruptedIOException;
import com.applovin.exoplayer2.l.y;
import com.applovin.exoplayer2.k.l$a;
import com.applovin.exoplayer2.k.l;
import com.applovin.exoplayer2.k.z;
import com.applovin.exoplayer2.k.v$a;
import java.util.List;
import com.applovin.exoplayer2.k.w$b;
import java.io.IOException;
import com.applovin.exoplayer2.j.d;
import com.applovin.exoplayer2.av;
import java.util.Collections;
import java.util.HashMap;
import com.applovin.exoplayer2.k.w$d;
import com.applovin.exoplayer2.l.a;
import com.applovin.exoplayer2.l.u;
import java.util.Arrays;
import com.applovin.exoplayer2.e.x;
import com.applovin.exoplayer2.l.ai;
import android.os.Handler;
import androidx.annotation.Nullable;
import com.applovin.exoplayer2.k.b;
import com.applovin.exoplayer2.d.g;
import com.applovin.exoplayer2.d.h;
import com.applovin.exoplayer2.k.i;
import android.net.Uri;
import com.applovin.exoplayer2.v;
import java.util.Map;
import com.applovin.exoplayer2.k.w$e;
import com.applovin.exoplayer2.k.w$a;
import com.applovin.exoplayer2.e.j;

final class t implements j, n, w.c, w$a<a>, w$e
{
    private static final Map<String, String> b;
    private static final v c;
    private e A;
    private com.applovin.exoplayer2.e.v B;
    private long C;
    private boolean D;
    private int E;
    private boolean F;
    private boolean G;
    private int H;
    private long I;
    private long J;
    private long K;
    private boolean L;
    private int M;
    private boolean N;
    private boolean O;
    private final Uri d;
    private final i e;
    private final h f;
    private final com.applovin.exoplayer2.k.v g;
    private final q.a h;
    private final g.a i;
    private final b j;
    private final com.applovin.exoplayer2.k.b k;
    @Nullable
    private final String l;
    private final long m;
    private final com.applovin.exoplayer2.k.w n;
    private final s o;
    private final com.applovin.exoplayer2.l.g p;
    private final Runnable q;
    private final Runnable r;
    private final Handler s;
    @Nullable
    private n.a t;
    @Nullable
    private com.applovin.exoplayer2.g.d.b u;
    private w[] v;
    private d[] w;
    private boolean x;
    private boolean y;
    private boolean z;
    
    static {
        b = t();
        c = new v.a().a("icy").f("application/x-icy").a();
    }
    
    public t(final Uri d, final i e, final s o, final h f, final g.a i, final com.applovin.exoplayer2.k.v g, final q.a h, final b j, final com.applovin.exoplayer2.k.b k, @Nullable final String l, final int n) {
        this.d = d;
        this.e = e;
        this.f = f;
        this.i = i;
        this.g = g;
        this.h = h;
        this.j = j;
        this.k = k;
        this.l = l;
        this.m = n;
        this.n = new com.applovin.exoplayer2.k.w("ProgressiveMediaPeriod");
        this.o = o;
        this.p = new com.applovin.exoplayer2.l.g();
        this.q = new OO0o\u3007\u3007(this);
        this.r = new Oooo8o0\u3007(this);
        this.s = ai.a();
        this.w = new d[0];
        this.v = new w[0];
        this.K = -9223372036854775807L;
        this.I = -1L;
        this.C = -9223372036854775807L;
        this.E = 1;
    }
    
    private x a(final d d) {
        final int length = this.v.length;
        for (int i = 0; i < length; ++i) {
            if (d.equals(this.w[i])) {
                return this.v[i];
            }
        }
        final w a = com.applovin.exoplayer2.h.w.a(this.k, this.s.getLooper(), this.f, this.i);
        a.a((w.c)this);
        final d[] w = this.w;
        final int n = length + 1;
        final d[] array = Arrays.copyOf(w, n);
        array[length] = d;
        this.w = (d[])ai.a((Object[])array);
        final w[] array2 = Arrays.copyOf(this.v, n);
        array2[length] = a;
        this.v = (w[])ai.a((Object[])array2);
        return a;
    }
    
    private void a(final a a) {
        if (this.I == -1L) {
            this.I = a.m;
        }
    }
    
    private boolean a(final a a, int i) {
        if (this.I == -1L) {
            final com.applovin.exoplayer2.e.v b = this.B;
            if (b == null || b.b() == -9223372036854775807L) {
                final boolean y = this.y;
                i = 0;
                if (y && !this.m()) {
                    this.L = true;
                    return false;
                }
                this.G = this.y;
                this.J = 0L;
                this.M = 0;
                for (w[] v = this.v; i < v.length; ++i) {
                    v[i].b();
                }
                a.a(0L, 0L);
                return true;
            }
        }
        this.M = i;
        return true;
    }
    
    private boolean a(final boolean[] array, final long n) {
        for (int length = this.v.length, i = 0; i < length; ++i) {
            if (!this.v[i].a(n, false) && (array[i] || !this.z)) {
                return false;
            }
        }
        return true;
    }
    
    private void b(final com.applovin.exoplayer2.e.v v) {
        com.applovin.exoplayer2.e.v b;
        if (this.u == null) {
            b = v;
        }
        else {
            b = new com.applovin.exoplayer2.e.v.b(-9223372036854775807L);
        }
        this.B = b;
        this.C = v.b();
        final long i = this.I;
        int e = 1;
        final boolean d = i == -1L && v.b() == -9223372036854775807L;
        this.D = d;
        if (d) {
            e = 7;
        }
        this.E = e;
        this.j.a(this.C, v.a(), this.D);
        if (!this.y) {
            this.n();
        }
    }
    
    private void c(final int n) {
        this.s();
        final e a = this.A;
        final boolean[] d = a.d;
        if (!d[n]) {
            final v a2 = a.a.a(n).a(0);
            this.h.a(com.applovin.exoplayer2.l.u.e(a2.l), a2, 0, null, this.J);
            d[n] = true;
        }
    }
    
    private void d(int i) {
        this.s();
        final boolean[] b = this.A.b;
        if (this.L && b[i]) {
            final w w = this.v[i];
            i = 0;
            if (!w.b(false)) {
                this.K = 0L;
                this.L = false;
                this.G = true;
                this.J = 0L;
                this.M = 0;
                for (w[] v = this.v; i < v.length; ++i) {
                    v[i].b();
                }
                ((y.a<t>)com.applovin.exoplayer2.l.a.b((Object)this.t)).a(this);
            }
        }
    }
    
    private boolean m() {
        return this.G || this.r();
    }
    
    private void n() {
        if (!this.O && !this.y && this.x) {
            if (this.B != null) {
                final w[] v = this.v;
                for (int length = v.length, i = 0; i < length; ++i) {
                    if (v[i].g() == null) {
                        return;
                    }
                }
                this.p.b();
                final int length2 = this.v.length;
                final ac[] array = new ac[length2];
                final boolean[] array2 = new boolean[length2];
                for (int j = 0; j < length2; ++j) {
                    final v v2 = (v)com.applovin.exoplayer2.l.a.b((Object)this.v[j].g());
                    final String l = v2.l;
                    final boolean a = com.applovin.exoplayer2.l.u.a(l);
                    final boolean b = a || com.applovin.exoplayer2.l.u.b(l);
                    array2[j] = b;
                    this.z |= b;
                    final com.applovin.exoplayer2.g.d.b u = this.u;
                    v a2 = v2;
                    if (u != null) {
                        v a3 = null;
                        Label_0266: {
                            if (!a) {
                                a3 = v2;
                                if (!this.w[j].b) {
                                    break Label_0266;
                                }
                            }
                            final com.applovin.exoplayer2.g.a k = v2.j;
                            com.applovin.exoplayer2.g.a a4;
                            if (k == null) {
                                a4 = new com.applovin.exoplayer2.g.a(new com.applovin.exoplayer2.g.a.a[] { u });
                            }
                            else {
                                a4 = k.a(u);
                            }
                            a3 = v2.a().a(a4).a();
                        }
                        a2 = a3;
                        if (a) {
                            a2 = a3;
                            if (a3.f == -1) {
                                a2 = a3;
                                if (a3.g == -1) {
                                    a2 = a3;
                                    if (u.a != -1) {
                                        a2 = a3.a().d(u.a).a();
                                    }
                                }
                            }
                        }
                    }
                    array[j] = new ac(new v[] { a2.a(this.f.a(a2)) });
                }
                this.A = new e(new ad(array), array2);
                this.y = true;
                ((n.a)com.applovin.exoplayer2.l.a.b((Object)this.t)).a((n)this);
            }
        }
    }
    
    private void o() {
        final a a = new a(this.d, this.e, this.o, this, this.p);
        if (this.y) {
            com.applovin.exoplayer2.l.a.b(this.r());
            final long c = this.C;
            if (c != -9223372036854775807L && this.K > c) {
                this.N = true;
                this.K = -9223372036854775807L;
                return;
            }
            a.a(((com.applovin.exoplayer2.e.v)com.applovin.exoplayer2.l.a.b((Object)this.B)).a(this.K).a.c, this.K);
            final w[] v = this.v;
            for (int length = v.length, i = 0; i < length; ++i) {
                v[i].a(this.K);
            }
            this.K = -9223372036854775807L;
        }
        this.M = this.p();
        this.h.a(new com.applovin.exoplayer2.h.j(a.b, a.l, this.n.a((w$d)a, (w$a)this, this.g.a(this.E))), 1, -1, null, 0, null, a.k, this.C);
    }
    
    private int p() {
        final w[] v = this.v;
        final int length = v.length;
        int i = 0;
        int n = 0;
        while (i < length) {
            n += v[i].c();
            ++i;
        }
        return n;
    }
    
    private long q() {
        final w[] v = this.v;
        final int length = v.length;
        long max = Long.MIN_VALUE;
        for (int i = 0; i < length; ++i) {
            max = Math.max(max, v[i].h());
        }
        return max;
    }
    
    private boolean r() {
        return this.K != -9223372036854775807L;
    }
    
    private void s() {
        com.applovin.exoplayer2.l.a.b(this.y);
        com.applovin.exoplayer2.l.a.b((Object)this.A);
        com.applovin.exoplayer2.l.a.b((Object)this.B);
    }
    
    private static Map<String, String> t() {
        final HashMap m = new HashMap();
        m.put("Icy-MetaData", "1");
        return (Map<String, String>)Collections.unmodifiableMap((Map<?, ?>)m);
    }
    
    int a(final int n, final long n2) {
        if (this.m()) {
            return 0;
        }
        this.c(n);
        final w w = this.v[n];
        final int b = w.b(n2, this.N);
        w.a(b);
        if (b == 0) {
            this.d(n);
        }
        return b;
    }
    
    int a(final int n, final com.applovin.exoplayer2.w w, final com.applovin.exoplayer2.c.g g, int a) {
        if (this.m()) {
            return -3;
        }
        this.c(n);
        a = this.v[n].a(w, g, a, this.N);
        if (a == -3) {
            this.d(n);
        }
        return a;
    }
    
    @Override
    public long a(final long n, final av av) {
        this.s();
        if (!this.B.a()) {
            return 0L;
        }
        final com.applovin.exoplayer2.e.v.a a = this.B.a(n);
        return av.a(n, a.a.b, a.b.b);
    }
    
    @Override
    public long a(final com.applovin.exoplayer2.j.d[] array, final boolean[] array2, final com.applovin.exoplayer2.h.x[] array3, final boolean[] array4, long b) {
        this.s();
        final e a = this.A;
        final ad a2 = a.a;
        final boolean[] c = a.c;
        final int h = this.H;
        final int n = 0;
        final int n2 = 0;
        final int n3 = 0;
        for (int i = 0; i < array.length; ++i) {
            final com.applovin.exoplayer2.h.x x = array3[i];
            if (x != null && (array[i] == null || !array2[i])) {
                final int a3 = ((c)x).b;
                com.applovin.exoplayer2.l.a.b(c[a3]);
                --this.H;
                c[a3] = false;
                array3[i] = null;
            }
        }
        final boolean b2 = this.F ? (h == 0) : (b != 0L);
        int j = 0;
        int n4 = b2 ? 1 : 0;
        while (j < array.length) {
            int n5 = n4;
            if (array3[j] == null) {
                final com.applovin.exoplayer2.j.d d = array[j];
                n5 = n4;
                if (d != null) {
                    com.applovin.exoplayer2.l.a.b(((com.applovin.exoplayer2.j.g)d).e() == 1);
                    com.applovin.exoplayer2.l.a.b(((com.applovin.exoplayer2.j.g)d).b(0) == 0);
                    final int a4 = a2.a(((com.applovin.exoplayer2.j.g)d).d());
                    com.applovin.exoplayer2.l.a.b(c[a4] ^ true);
                    ++this.H;
                    c[a4] = true;
                    array3[j] = new c(a4);
                    array4[j] = true;
                    if ((n5 = n4) == 0) {
                        final w w = this.v[a4];
                        if (!w.a(b, true) && w.f() != 0) {
                            n5 = 1;
                        }
                        else {
                            n5 = 0;
                        }
                    }
                }
            }
            ++j;
            n4 = n5;
        }
        long n6;
        if (this.H == 0) {
            this.L = false;
            this.G = false;
            if (this.n.c()) {
                final w[] v = this.v;
                for (int length = v.length, k = n3; k < length; ++k) {
                    v[k].k();
                }
                this.n.d();
                n6 = b;
            }
            else {
                final w[] v2 = this.v;
                final int length2 = v2.length;
                int n7 = n;
                while (true) {
                    n6 = b;
                    if (n7 >= length2) {
                        break;
                    }
                    v2[n7].b();
                    ++n7;
                }
            }
        }
        else {
            n6 = b;
            if (n4 != 0) {
                b = this.b(b);
                int n8 = n2;
                while (true) {
                    n6 = b;
                    if (n8 >= array3.length) {
                        break;
                    }
                    if (array3[n8] != null) {
                        array4[n8] = true;
                    }
                    ++n8;
                }
            }
        }
        this.F = true;
        return n6;
    }
    
    @Override
    public x a(final int n, final int n2) {
        return this.a(new d(n, false));
    }
    
    public w$b a(final a a, long a2, final long n, final IOException ex, int p5) {
        this.a(a);
        final z a3 = a.d;
        final com.applovin.exoplayer2.h.j j = new com.applovin.exoplayer2.h.j(a.b, a.l, a3.e(), a3.f(), a2, n, a3.d());
        a2 = this.g.a(new v$a(j, new m(1, -1, null, 0, null, com.applovin.exoplayer2.h.a(a.k), com.applovin.exoplayer2.h.a(this.C)), ex, p5));
        w$b w$b;
        if (a2 == -9223372036854775807L) {
            w$b = com.applovin.exoplayer2.k.w.d;
        }
        else {
            p5 = this.p();
            final boolean b = p5 > this.M;
            if (this.a(a, p5)) {
                w$b = com.applovin.exoplayer2.k.w.a(b, a2);
            }
            else {
                w$b = com.applovin.exoplayer2.k.w.c;
            }
        }
        final boolean b2 = w$b.a() ^ true;
        this.h.a(j, 1, -1, null, 0, null, a.k, this.C, ex, b2);
        if (b2) {
            this.g.a(a.b);
        }
        return w$b;
    }
    
    @Override
    public void a() {
        this.x = true;
        this.s.post(this.q);
    }
    
    @Override
    public void a(final long n) {
    }
    
    @Override
    public void a(final long n, final boolean b) {
        this.s();
        if (this.r()) {
            return;
        }
        final boolean[] c = this.A.c;
        for (int length = this.v.length, i = 0; i < length; ++i) {
            this.v[i].a(n, b, c[i]);
        }
    }
    
    @Override
    public void a(final com.applovin.exoplayer2.e.v v) {
        this.s.post((Runnable)new \u3007O8o08O(this, v));
    }
    
    @Override
    public void a(final n.a t, final long n) {
        this.t = t;
        this.p.a();
        this.o();
    }
    
    public void a(final a a, final long n, final long n2) {
        if (this.C == -9223372036854775807L) {
            final com.applovin.exoplayer2.e.v b = this.B;
            if (b != null) {
                final boolean a2 = b.a();
                final long q = this.q();
                long c;
                if (q == Long.MIN_VALUE) {
                    c = 0L;
                }
                else {
                    c = q + 10000L;
                }
                this.C = c;
                this.j.a(c, a2, this.D);
            }
        }
        final z a3 = a.d;
        final com.applovin.exoplayer2.h.j j = new com.applovin.exoplayer2.h.j(a.b, a.l, a3.e(), a3.f(), n, n2, a3.d());
        this.g.a(a.b);
        this.h.b(j, 1, -1, null, 0, null, a.k, this.C);
        this.a(a);
        this.N = true;
        ((y.a<t>)a.b((Object)this.t)).a(this);
    }
    
    public void a(final a a, final long n, final long n2, final boolean b) {
        final z a2 = a.d;
        final com.applovin.exoplayer2.h.j j = new com.applovin.exoplayer2.h.j(a.b, a.l, a2.e(), a2.f(), n, n2, a2.d());
        this.g.a(a.b);
        this.h.c(j, 1, -1, null, 0, null, a.k, this.C);
        if (!b) {
            this.a(a);
            final w[] v = this.v;
            for (int length = v.length, i = 0; i < length; ++i) {
                v[i].b();
            }
            if (this.H > 0) {
                ((y.a<t>)a.b((Object)this.t)).a(this);
            }
        }
    }
    
    @Override
    public void a(final v v) {
        this.s.post(this.q);
    }
    
    boolean a(final int n) {
        return !this.m() && this.v[n].b(this.N);
    }
    
    @Override
    public long b(long k) {
        this.s();
        final boolean[] b = this.A.b;
        if (!this.B.a()) {
            k = 0L;
        }
        int i = 0;
        final int n = 0;
        this.G = false;
        this.J = k;
        if (this.r()) {
            return this.K = k;
        }
        if (this.E != 7 && this.a(b, k)) {
            return k;
        }
        this.L = false;
        this.K = k;
        this.N = false;
        if (this.n.c()) {
            final w[] v = this.v;
            for (int length = v.length, j = n; j < length; ++j) {
                v[j].k();
            }
            this.n.d();
        }
        else {
            this.n.b();
            for (w[] v2 = this.v; i < v2.length; ++i) {
                v2[i].b();
            }
        }
        return k;
    }
    
    @Override
    public ad b() {
        this.s();
        return this.A.a;
    }
    
    void b(final int n) throws IOException {
        this.v[n].e();
        this.i();
    }
    
    @Override
    public long c() {
        if (this.G && (this.N || this.p() > this.M)) {
            this.G = false;
            return this.J;
        }
        return -9223372036854775807L;
    }
    
    @Override
    public boolean c(final long n) {
        if (!this.N && !this.n.a() && !this.L && (!this.y || this.H != 0)) {
            boolean a = this.p.a();
            if (!this.n.c()) {
                this.o();
                a = true;
            }
            return a;
        }
        return false;
    }
    
    @Override
    public long d() {
        this.s();
        final boolean[] b = this.A.b;
        if (this.N) {
            return Long.MIN_VALUE;
        }
        if (this.r()) {
            return this.K;
        }
        long n2;
        if (this.z) {
            final int length = this.v.length;
            int n = 0;
            long a = Long.MAX_VALUE;
            while (true) {
                n2 = a;
                if (n >= length) {
                    break;
                }
                long min = a;
                if (b[n]) {
                    min = a;
                    if (!this.v[n].j()) {
                        min = Math.min(a, this.v[n].h());
                    }
                }
                ++n;
                a = min;
            }
        }
        else {
            n2 = Long.MAX_VALUE;
        }
        long q = n2;
        if (n2 == Long.MAX_VALUE) {
            q = this.q();
        }
        long j = q;
        if (q == Long.MIN_VALUE) {
            j = this.J;
        }
        return j;
    }
    
    @Override
    public long e() {
        long d;
        if (this.H == 0) {
            d = Long.MIN_VALUE;
        }
        else {
            d = this.d();
        }
        return d;
    }
    
    @Override
    public void e_() throws IOException {
        this.i();
        if (this.N && !this.y) {
            throw com.applovin.exoplayer2.ai.b("Loading finished before preparation is complete.", null);
        }
    }
    
    @Override
    public boolean f() {
        return this.n.c() && this.p.e();
    }
    
    public void g() {
        if (this.y) {
            final w[] v = this.v;
            for (int length = v.length, i = 0; i < length; ++i) {
                v[i].d();
            }
        }
        this.n.a((w$e)this);
        this.s.removeCallbacksAndMessages((Object)null);
        this.t = null;
        this.O = true;
    }
    
    public void h() {
        final w[] v = this.v;
        for (int length = v.length, i = 0; i < length; ++i) {
            v[i].a();
        }
        this.o.a();
    }
    
    void i() throws IOException {
        this.n.a(this.g.a(this.E));
    }
    
    x j() {
        return this.a(new d(0, true));
    }
    
    final class a implements i.a, w$d
    {
        final t a;
        private final long b;
        private final Uri c;
        private final z d;
        private final s e;
        private final j f;
        private final com.applovin.exoplayer2.l.g g;
        private final com.applovin.exoplayer2.e.u h;
        private volatile boolean i;
        private boolean j;
        private long k;
        private l l;
        private long m;
        @Nullable
        private x n;
        private boolean o;
        
        public a(final t a, final Uri c, final com.applovin.exoplayer2.k.i i, final s e, final j f, final com.applovin.exoplayer2.l.g g) {
            this.a = a;
            this.c = c;
            this.d = new z(i);
            this.e = e;
            this.f = f;
            this.g = g;
            this.h = new com.applovin.exoplayer2.e.u();
            this.j = true;
            this.m = -1L;
            this.b = com.applovin.exoplayer2.h.j.a();
            this.l = this.a(0L);
        }
        
        private l a(final long n) {
            return new l$a().a(this.c).a(n).b(this.a.l).b(6).a(com.applovin.exoplayer2.h.t.b).a();
        }
        
        private void a(final long a, final long k) {
            this.h.a = a;
            this.k = k;
            this.j = true;
            this.o = false;
        }
        
        public void a() {
            this.i = true;
        }
        
        @Override
        public void a(final com.applovin.exoplayer2.l.y y) {
            long n;
            if (!this.o) {
                n = this.k;
            }
            else {
                n = Math.max(this.a.q(), this.k);
            }
            final int a = y.a();
            final x x = (x)com.applovin.exoplayer2.l.a.b((Object)this.n);
            x.a(y, a);
            x.a(n, 1, a, 0, null);
            this.o = true;
        }
        
        public void b() throws IOException {
            int i = 0;
            while (i == 0 && !this.i) {
                int a = i;
                try {
                    final long a2 = this.h.a;
                    a = i;
                    final l a3 = this.a(a2);
                    a = i;
                    this.l = a3;
                    a = i;
                    final long a4 = this.d.a(a3);
                    a = i;
                    this.m = a4;
                    if (a4 != -1L) {
                        a = i;
                        this.m = a4 + a2;
                    }
                    a = i;
                    this.a.u = com.applovin.exoplayer2.g.d.b.a(this.d.b());
                    a = i;
                    Object d;
                    final z z = (z)(d = this.d);
                    a = i;
                    if (this.a.u != null) {
                        d = z;
                        a = i;
                        if (this.a.u.f != -1) {
                            a = i;
                            d = new(com.applovin.exoplayer2.h.i.class)();
                            a = i;
                            new i((com.applovin.exoplayer2.k.i)this.d, this.a.u.f, (i.a)this);
                            a = i;
                            final x j = this.a.j();
                            a = i;
                            this.n = j;
                            a = i;
                            j.a(com.applovin.exoplayer2.h.t.c);
                        }
                    }
                    a = i;
                    final s e = this.e;
                    a = i;
                    final Uri c = this.c;
                    a = i;
                    final Map b = this.d.b();
                    a = i;
                    final long m = this.m;
                    a = i;
                    final j f = this.f;
                    final long n = a2;
                    a = i;
                    e.a((com.applovin.exoplayer2.k.g)d, c, b, a2, m, f);
                    a = i;
                    if (this.a.u != null) {
                        a = i;
                        this.e.b();
                    }
                    int n2 = i;
                    long c2 = n;
                    a = i;
                    if (this.j) {
                        a = i;
                        this.e.a(n, this.k);
                        a = i;
                        this.j = false;
                        c2 = n;
                        n2 = i;
                    }
                Label_0343:
                    while (true) {
                        final long n3 = c2;
                        i = n2;
                        while (i == 0) {
                            a = i;
                            if (!this.i) {
                                a = i;
                                try {
                                    this.g.c();
                                    a = i;
                                    n2 = (a = this.e.a(this.h));
                                    c2 = this.e.c();
                                    i = n2;
                                    a = n2;
                                    if (c2 > this.a.m + n3) {
                                        a = n2;
                                        this.g.b();
                                        a = n2;
                                        this.a.s.post(this.a.r);
                                        continue Label_0343;
                                    }
                                    continue;
                                }
                                catch (final InterruptedException ex) {
                                    a = i;
                                    a = i;
                                    final InterruptedIOException ex2 = new InterruptedIOException();
                                    a = i;
                                    throw ex2;
                                }
                                break;
                            }
                            break;
                        }
                        break;
                    }
                    if (i == 1) {
                        a = 0;
                    }
                    else {
                        a = i;
                        if (this.e.c() != -1L) {
                            this.h.a = this.e.c();
                            a = i;
                        }
                    }
                    ai.a((com.applovin.exoplayer2.k.i)this.d);
                    i = a;
                    continue;
                }
                finally {
                    if (a != 1 && this.e.c() != -1L) {
                        this.h.a = this.e.c();
                    }
                    ai.a((com.applovin.exoplayer2.k.i)this.d);
                }
                break;
            }
        }
    }
    
    interface b
    {
        void a(final long p0, final boolean p1, final boolean p2);
    }
    
    private final class c implements x
    {
        final t a;
        private final int b;
        
        public c(final t a, final int b) {
            this.a = a;
            this.b = b;
        }
        
        @Override
        public int a(final long n) {
            return this.a.a(this.b, n);
        }
        
        @Override
        public int a(final com.applovin.exoplayer2.w w, final com.applovin.exoplayer2.c.g g, final int n) {
            return this.a.a(this.b, w, g, n);
        }
        
        @Override
        public boolean b() {
            return this.a.a(this.b);
        }
        
        @Override
        public void c() throws IOException {
            this.a.b(this.b);
        }
    }
    
    private static final class d
    {
        public final int a;
        public final boolean b;
        
        public d(final int a, final boolean b) {
            this.a = a;
            this.b = b;
        }
        
        @Override
        public boolean equals(@Nullable final Object o) {
            boolean b = true;
            if (this == o) {
                return true;
            }
            if (o != null && d.class == o.getClass()) {
                final d d = (d)o;
                if (this.a != d.a || this.b != d.b) {
                    b = false;
                }
                return b;
            }
            return false;
        }
        
        @Override
        public int hashCode() {
            return this.a * 31 + (this.b ? 1 : 0);
        }
    }
    
    private static final class e
    {
        public final ad a;
        public final boolean[] b;
        public final boolean[] c;
        public final boolean[] d;
        
        public e(final ad a, final boolean[] b) {
            this.a = a;
            this.b = b;
            final int b2 = a.b;
            this.c = new boolean[b2];
            this.d = new boolean[b2];
        }
    }
}

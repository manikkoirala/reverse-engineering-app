// 
// Decompiled by Procyon v0.6.0
// 

package com.applovin.exoplayer2.h;

import androidx.annotation.Nullable;
import java.io.IOException;
import java.io.EOFException;
import com.applovin.exoplayer2.c.c;
import com.applovin.exoplayer2.l.ai;
import com.applovin.exoplayer2.e.x;
import java.util.Arrays;
import com.applovin.exoplayer2.k.a;
import com.applovin.exoplayer2.c.g;
import java.nio.ByteBuffer;
import com.applovin.exoplayer2.l.y;
import com.applovin.exoplayer2.k.b;

class v
{
    private final b a;
    private final int b;
    private final y c;
    private a d;
    private a e;
    private a f;
    private long g;
    
    public v(final b a) {
        this.a = a;
        final int c = a.c();
        this.b = c;
        this.c = new y(32);
        final a f = new a(0L, c);
        this.d = f;
        this.e = f;
        this.f = f;
    }
    
    private int a(final int a) {
        final a f = this.f;
        if (!f.c) {
            f.a(this.a.a(), new a(this.f.b, this.b));
        }
        return Math.min(a, (int)(this.f.b - this.g));
    }
    
    private static a a(a e, final long n) {
        while (n >= e.b) {
            e = e.e;
        }
        return e;
    }
    
    private static a a(a a, long n, final ByteBuffer byteBuffer, int i) {
        a = a(a, n);
        while (i > 0) {
            final int min = Math.min(i, (int)(a.b - n));
            byteBuffer.put(a.d.a, a.a(n), min);
            final int n2 = i - min;
            final long n3 = n += min;
            i = n2;
            if (n3 == a.b) {
                a = a.e;
                n = n3;
                i = n2;
            }
        }
        return a;
    }
    
    private static a a(a a, long n, final byte[] array, final int n2) {
        a = a(a, n);
        int n3;
        long n4;
        for (int i = n2; i > 0; i = n3, n = n4) {
            final int min = Math.min(i, (int)(a.b - n));
            System.arraycopy(a.d.a, a.a(n), array, n2 - i, min);
            n3 = i - min;
            n4 = n + min;
            i = n3;
            n = n4;
            if (n4 == a.b) {
                a = a.e;
            }
        }
        return a;
    }
    
    private static a a(a a, final g g, final w.a a2, final y y) {
        a b = a;
        if (g.g()) {
            b = b(a, g, a2, y);
        }
        if (g.e()) {
            y.a(4);
            a = a(b, a2.b, y.d(), 4);
            final int w = y.w();
            a2.b += 4L;
            a2.a -= 4;
            g.f(w);
            a = a(a, a2.b, g.b, w);
            a2.b += w;
            g.e(a2.a -= w);
            a = a(a, a2.b, g.e, a2.a);
        }
        else {
            g.f(a2.a);
            a = a(b, a2.b, g.b, a2.a);
        }
        return a;
    }
    
    private void a(a a) {
        if (!a.c) {
            return;
        }
        final a f = this.f;
        final int n = (f.c ? 1 : 0) + (int)(f.a - a.a) / this.b;
        final com.applovin.exoplayer2.k.a[] array = new com.applovin.exoplayer2.k.a[n];
        for (int i = 0; i < n; ++i) {
            array[i] = a.d;
            a = a.a();
        }
        this.a.a(array);
    }
    
    private static a b(a a, final g g, final w.a a2, final y y) {
        final long b = a2.b;
        y.a(1);
        a = a(a, b, y.d(), 1);
        final long n = b + 1L;
        final byte[] d = y.d();
        final int n2 = 0;
        final byte b2 = d[0];
        final boolean b3 = (b2 & 0x80) != 0x0;
        final int n3 = b2 & 0x7F;
        final c a3 = g.a;
        final byte[] a4 = a3.a;
        if (a4 == null) {
            a3.a = new byte[16];
        }
        else {
            Arrays.fill(a4, (byte)0);
        }
        a = a(a, n, a3.a, n3);
        long n4 = n + n3;
        int i;
        if (b3) {
            y.a(2);
            a = a(a, n4, y.d(), 2);
            n4 += 2L;
            i = y.i();
        }
        else {
            i = 1;
        }
        final int[] d2 = a3.d;
        int[] array = null;
        Label_0194: {
            if (d2 != null) {
                array = d2;
                if (d2.length >= i) {
                    break Label_0194;
                }
            }
            array = new int[i];
        }
        final int[] e = a3.e;
        int[] array2 = null;
        Label_0224: {
            if (e != null) {
                array2 = e;
                if (e.length >= i) {
                    break Label_0224;
                }
            }
            array2 = new int[i];
        }
        if (b3) {
            final int n5 = i * 6;
            y.a(n5);
            final a a5 = a(a, n4, y.d(), n5);
            final long n6 = n4 + n5;
            y.d(0);
            int n7 = n2;
            while (true) {
                n4 = n6;
                a = a5;
                if (n7 >= i) {
                    break;
                }
                array[n7] = y.i();
                array2[n7] = y.w();
                ++n7;
            }
        }
        else {
            array2[array[0] = 0] = a2.a - (int)(n4 - a2.b);
        }
        final x.a a6 = (x.a)ai.a((Object)a2.c);
        a3.a(i, array, array2, a6.b, a3.a, a6.a, a6.c, a6.d);
        final long b4 = a2.b;
        final int n8 = (int)(n4 - b4);
        a2.b = b4 + n8;
        a2.a -= n8;
        return a;
    }
    
    private void b(final int n) {
        final long g = this.g + n;
        this.g = g;
        final a f = this.f;
        if (g == f.b) {
            this.f = f.e;
        }
    }
    
    public int a(final com.applovin.exoplayer2.k.g g, int n, final boolean b) throws IOException {
        n = this.a(n);
        final a f = this.f;
        n = g.a(f.d.a, f.a(this.g), n);
        if (n != -1) {
            this.b(n);
            return n;
        }
        if (b) {
            return -1;
        }
        throw new EOFException();
    }
    
    public void a() {
        this.a(this.d);
        final a f = new a(0L, this.b);
        this.d = f;
        this.e = f;
        this.f = f;
        this.g = 0L;
        this.a.b();
    }
    
    public void a(final long n) {
        if (n == -1L) {
            return;
        }
        a d;
        while (true) {
            d = this.d;
            if (n < d.b) {
                break;
            }
            this.a.a(d.d);
            this.d = this.d.a();
        }
        if (this.e.a < d.a) {
            this.e = d;
        }
    }
    
    public void a(final g g, final w.a a) {
        this.e = a(this.e, g, a, this.c);
    }
    
    public void a(final y y, int i) {
        while (i > 0) {
            final int a = this.a(i);
            final a f = this.f;
            y.a(f.d.a, f.a(this.g), a);
            i -= a;
            this.b(a);
        }
    }
    
    public void b() {
        this.e = this.d;
    }
    
    public void b(final g g, final w.a a) {
        a(this.e, g, a, this.c);
    }
    
    public long c() {
        return this.g;
    }
    
    private static final class a
    {
        public final long a;
        public final long b;
        public boolean c;
        @Nullable
        public com.applovin.exoplayer2.k.a d;
        @Nullable
        public a e;
        
        public a(final long a, final int n) {
            this.a = a;
            this.b = a + n;
        }
        
        public int a(final long n) {
            return (int)(n - this.a) + this.d.b;
        }
        
        public a a() {
            this.d = null;
            final a e = this.e;
            this.e = null;
            return e;
        }
        
        public void a(final com.applovin.exoplayer2.k.a d, final a e) {
            this.d = d;
            this.e = e;
            this.c = true;
        }
    }
}

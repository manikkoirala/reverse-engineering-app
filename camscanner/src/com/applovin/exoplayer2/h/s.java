// 
// Decompiled by Procyon v0.6.0
// 

package com.applovin.exoplayer2.h;

import com.applovin.exoplayer2.e.j;
import java.util.List;
import java.util.Map;
import android.net.Uri;
import com.applovin.exoplayer2.k.g;
import java.io.IOException;
import com.applovin.exoplayer2.e.u;

public interface s
{
    int a(final u p0) throws IOException;
    
    void a();
    
    void a(final long p0, final long p1);
    
    void a(final g p0, final Uri p1, final Map<String, List<String>> p2, final long p3, final long p4, final j p5) throws IOException;
    
    void b();
    
    long c();
    
    public interface a
    {
        s createProgressiveMediaExtractor();
    }
}

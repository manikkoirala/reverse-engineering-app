// 
// Decompiled by Procyon v0.6.0
// 

package com.applovin.exoplayer2.h;

import com.applovin.exoplayer2.k.o$a;
import com.applovin.exoplayer2.e.l;
import android.content.Context;
import android.util.SparseArray;
import com.applovin.exoplayer2.k.i$a;

public final class f implements r
{
    private final i$a a;
    private final SparseArray<r> b;
    private final int[] c;
    private long d;
    private long e;
    private long f;
    private float g;
    private float h;
    
    public f(final Context context, final l l) {
        this((i$a)new o$a(context), l);
    }
    
    public f(final i$a a, final l l) {
        this.a = a;
        final SparseArray<r> a2 = a(a, l);
        this.b = a2;
        this.c = new int[a2.size()];
        for (int i = 0; i < this.b.size(); ++i) {
            this.c[i] = this.b.keyAt(i);
        }
        this.d = -9223372036854775807L;
        this.e = -9223372036854775807L;
        this.f = -9223372036854775807L;
        this.g = -3.4028235E38f;
        this.h = -3.4028235E38f;
    }
    
    private static SparseArray<r> a(final i$a p0, final l p1) {
        // 
        // This method could not be decompiled.
        // 
        // Original Bytecode:
        // 
        //     3: dup            
        //     4: invokespecial   android/util/SparseArray.<init>:()V
        //     7: astore_2       
        //     8: aload_2        
        //     9: iconst_0       
        //    10: ldc             "com.applovin.exoplayer2.source.dash.DashMediaSource$Factory"
        //    12: invokestatic    java/lang/Class.forName:(Ljava/lang/String;)Ljava/lang/Class;
        //    15: ldc             Lcom/applovin/exoplayer2/h/r;.class
        //    17: invokevirtual   java/lang/Class.asSubclass:(Ljava/lang/Class;)Ljava/lang/Class;
        //    20: iconst_1       
        //    21: anewarray       Ljava/lang/Class;
        //    24: dup            
        //    25: iconst_0       
        //    26: ldc             Lcom/applovin/exoplayer2/k/i$a;.class
        //    28: aastore        
        //    29: invokevirtual   java/lang/Class.getConstructor:([Ljava/lang/Class;)Ljava/lang/reflect/Constructor;
        //    32: iconst_1       
        //    33: anewarray       Ljava/lang/Object;
        //    36: dup            
        //    37: iconst_0       
        //    38: aload_0        
        //    39: aastore        
        //    40: invokevirtual   java/lang/reflect/Constructor.newInstance:([Ljava/lang/Object;)Ljava/lang/Object;
        //    43: checkcast       Lcom/applovin/exoplayer2/h/r;
        //    46: invokevirtual   android/util/SparseArray.put:(ILjava/lang/Object;)V
        //    49: aload_2        
        //    50: iconst_1       
        //    51: ldc             "com.applovin.exoplayer2.source.smoothstreaming.SsMediaSource$Factory"
        //    53: invokestatic    java/lang/Class.forName:(Ljava/lang/String;)Ljava/lang/Class;
        //    56: ldc             Lcom/applovin/exoplayer2/h/r;.class
        //    58: invokevirtual   java/lang/Class.asSubclass:(Ljava/lang/Class;)Ljava/lang/Class;
        //    61: iconst_1       
        //    62: anewarray       Ljava/lang/Class;
        //    65: dup            
        //    66: iconst_0       
        //    67: ldc             Lcom/applovin/exoplayer2/k/i$a;.class
        //    69: aastore        
        //    70: invokevirtual   java/lang/Class.getConstructor:([Ljava/lang/Class;)Ljava/lang/reflect/Constructor;
        //    73: iconst_1       
        //    74: anewarray       Ljava/lang/Object;
        //    77: dup            
        //    78: iconst_0       
        //    79: aload_0        
        //    80: aastore        
        //    81: invokevirtual   java/lang/reflect/Constructor.newInstance:([Ljava/lang/Object;)Ljava/lang/Object;
        //    84: checkcast       Lcom/applovin/exoplayer2/h/r;
        //    87: invokevirtual   android/util/SparseArray.put:(ILjava/lang/Object;)V
        //    90: aload_2        
        //    91: iconst_2       
        //    92: ldc             "com.applovin.exoplayer2.source.hls.HlsMediaSource$Factory"
        //    94: invokestatic    java/lang/Class.forName:(Ljava/lang/String;)Ljava/lang/Class;
        //    97: ldc             Lcom/applovin/exoplayer2/h/r;.class
        //    99: invokevirtual   java/lang/Class.asSubclass:(Ljava/lang/Class;)Ljava/lang/Class;
        //   102: iconst_1       
        //   103: anewarray       Ljava/lang/Class;
        //   106: dup            
        //   107: iconst_0       
        //   108: ldc             Lcom/applovin/exoplayer2/k/i$a;.class
        //   110: aastore        
        //   111: invokevirtual   java/lang/Class.getConstructor:([Ljava/lang/Class;)Ljava/lang/reflect/Constructor;
        //   114: iconst_1       
        //   115: anewarray       Ljava/lang/Object;
        //   118: dup            
        //   119: iconst_0       
        //   120: aload_0        
        //   121: aastore        
        //   122: invokevirtual   java/lang/reflect/Constructor.newInstance:([Ljava/lang/Object;)Ljava/lang/Object;
        //   125: checkcast       Lcom/applovin/exoplayer2/h/r;
        //   128: invokevirtual   android/util/SparseArray.put:(ILjava/lang/Object;)V
        //   131: aload_2        
        //   132: iconst_3       
        //   133: ldc             "com.applovin.exoplayer2.source.rtsp.RtspMediaSource$Factory"
        //   135: invokestatic    java/lang/Class.forName:(Ljava/lang/String;)Ljava/lang/Class;
        //   138: ldc             Lcom/applovin/exoplayer2/h/r;.class
        //   140: invokevirtual   java/lang/Class.asSubclass:(Ljava/lang/Class;)Ljava/lang/Class;
        //   143: iconst_0       
        //   144: anewarray       Ljava/lang/Class;
        //   147: invokevirtual   java/lang/Class.getConstructor:([Ljava/lang/Class;)Ljava/lang/reflect/Constructor;
        //   150: iconst_0       
        //   151: anewarray       Ljava/lang/Object;
        //   154: invokevirtual   java/lang/reflect/Constructor.newInstance:([Ljava/lang/Object;)Ljava/lang/Object;
        //   157: checkcast       Lcom/applovin/exoplayer2/h/r;
        //   160: invokevirtual   android/util/SparseArray.put:(ILjava/lang/Object;)V
        //   163: aload_2        
        //   164: iconst_4       
        //   165: new             Lcom/applovin/exoplayer2/h/u$a;
        //   168: dup            
        //   169: aload_0        
        //   170: aload_1        
        //   171: invokespecial   com/applovin/exoplayer2/h/u$a.<init>:(Lcom/applovin/exoplayer2/k/i$a;Lcom/applovin/exoplayer2/e/l;)V
        //   174: invokevirtual   android/util/SparseArray.put:(ILjava/lang/Object;)V
        //   177: aload_2        
        //   178: areturn        
        //   179: astore_3       
        //   180: goto            49
        //   183: astore_3       
        //   184: goto            90
        //   187: astore_3       
        //   188: goto            131
        //   191: astore_3       
        //   192: goto            163
        //    Signature:
        //  (Lcom/applovin/exoplayer2/k/i$a;Lcom/applovin/exoplayer2/e/l;)Landroid/util/SparseArray<Lcom/applovin/exoplayer2/h/r;>;
        //    Exceptions:
        //  Try           Handler
        //  Start  End    Start  End    Type                 
        //  -----  -----  -----  -----  ---------------------
        //  8      49     179    183    Ljava/lang/Exception;
        //  49     90     183    187    Ljava/lang/Exception;
        //  90     131    187    191    Ljava/lang/Exception;
        //  131    163    191    195    Ljava/lang/Exception;
        // 
        // The error that occurred was:
        // 
        // java.lang.IndexOutOfBoundsException: Index 102 out of bounds for length 102
        //     at java.base/jdk.internal.util.Preconditions.outOfBounds(Preconditions.java:64)
        //     at java.base/jdk.internal.util.Preconditions.outOfBoundsCheckIndex(Preconditions.java:70)
        //     at java.base/jdk.internal.util.Preconditions.checkIndex(Preconditions.java:266)
        //     at java.base/java.util.Objects.checkIndex(Objects.java:361)
        //     at java.base/java.util.ArrayList.get(ArrayList.java:427)
        //     at com.strobel.decompiler.ast.AstBuilder.convertToAst(AstBuilder.java:3362)
        //     at com.strobel.decompiler.ast.AstBuilder.build(AstBuilder.java:112)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:203)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:93)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethodBody(AstBuilder.java:868)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethod(AstBuilder.java:761)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addTypeMembers(AstBuilder.java:638)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeCore(AstBuilder.java:605)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeNoCache(AstBuilder.java:195)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createType(AstBuilder.java:162)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addType(AstBuilder.java:137)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.buildAst(JavaLanguage.java:71)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.decompileType(JavaLanguage.java:59)
        //     at com.strobel.decompiler.DecompilerDriver.decompileType(DecompilerDriver.java:333)
        //     at com.strobel.decompiler.DecompilerDriver.decompileJar(DecompilerDriver.java:254)
        //     at com.strobel.decompiler.DecompilerDriver.main(DecompilerDriver.java:144)
        // 
        throw new IllegalStateException("An error occurred while decompiling this method.");
    }
}

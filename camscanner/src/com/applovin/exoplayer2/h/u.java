// 
// Decompiled by Procyon v0.6.0
// 

package com.applovin.exoplayer2.h;

import com.applovin.exoplayer2.d.d;
import com.applovin.exoplayer2.e.l;
import com.applovin.exoplayer2.e.f;
import com.applovin.exoplayer2.k.i;
import com.applovin.exoplayer2.k.b;
import com.applovin.exoplayer2.ba;
import androidx.annotation.Nullable;
import com.applovin.exoplayer2.k.aa;
import com.applovin.exoplayer2.k.v;
import com.applovin.exoplayer2.d.h;
import com.applovin.exoplayer2.k.i$a;
import com.applovin.exoplayer2.ab;

public final class u extends com.applovin.exoplayer2.h.a implements t.b
{
    private final ab a;
    private final ab.f b;
    private final i$a c;
    private final s.a d;
    private final h e;
    private final v f;
    private final int g;
    private boolean h;
    private long i;
    private boolean j;
    private boolean k;
    @Nullable
    private aa l;
    
    private u(final ab a, final i$a c, final s.a d, final h e, final v f, final int g) {
        this.b = (ab.f)com.applovin.exoplayer2.l.a.b((Object)a.c);
        this.a = a;
        this.c = c;
        this.d = d;
        this.e = e;
        this.f = f;
        this.g = g;
        this.h = true;
        this.i = -9223372036854775807L;
    }
    
    private void f() {
        ba ba;
        final com.applovin.exoplayer2.h.aa aa = (com.applovin.exoplayer2.h.aa)(ba = new com.applovin.exoplayer2.h.aa(this.i, this.j, (boolean)(0 != 0), this.k, null, this.a));
        if (this.h) {
            ba = new com.applovin.exoplayer2.h.h(this, aa) {
                final u d;
                
                @Override
                public ba.a a(final int n, final ba.a a, final boolean b) {
                    super.a(n, a, b);
                    a.f = true;
                    return a;
                }
                
                @Override
                public ba.c a(final int n, final ba.c c, final long n2) {
                    super.a(n, c, n2);
                    c.m = true;
                    return c;
                }
            };
        }
        this.a(ba);
    }
    
    @Override
    public void a(final long n, final boolean j, final boolean k) {
        long i = n;
        if (n == -9223372036854775807L) {
            i = this.i;
        }
        if (!this.h && this.i == i && this.j == j && this.k == k) {
            return;
        }
        this.i = i;
        this.j = j;
        this.k = k;
        this.h = false;
        this.f();
    }
    
    @Override
    public void a(final n n) {
        ((t)n).g();
    }
    
    @Override
    protected void a(@Nullable final aa l) {
        this.l = l;
        this.e.a();
        this.f();
    }
    
    @Override
    public n b(final p.a a, final b b, final long n) {
        final i a2 = this.c.a();
        final aa l = this.l;
        if (l != null) {
            a2.a(l);
        }
        return new t(this.b.a, a2, this.d.createProgressiveMediaExtractor(), this.e, this.b(a), this.f, this.a(a), (t.b)this, b, this.b.f, this.g);
    }
    
    @Override
    protected void c() {
        this.e.b();
    }
    
    @Override
    public void e() {
    }
    
    @Override
    public ab g() {
        return this.a;
    }
    
    public static final class a implements r
    {
        private final i$a a;
        private s.a b;
        private com.applovin.exoplayer2.d.i c;
        private v d;
        private int e;
        @Nullable
        private String f;
        @Nullable
        private Object g;
        
        public a(final i$a i$a) {
            this(i$a, new f());
        }
        
        public a(final i$a i$a, final l l) {
            this(i$a, new \u3007\u3007808\u3007(l));
        }
        
        public a(final i$a a, final s.a b) {
            this.a = a;
            this.b = b;
            this.c = new com.applovin.exoplayer2.d.d();
            this.d = (v)new com.applovin.exoplayer2.k.r();
            this.e = 1048576;
        }
        
        public u a(final ab ab) {
            com.applovin.exoplayer2.l.a.b((Object)ab.c);
            final ab.f c = ab.c;
            final Object h = c.h;
            boolean b = true;
            final boolean b2 = h == null && this.g != null;
            if (c.f != null || this.f == null) {
                b = false;
            }
            ab ab2;
            if (b2 && b) {
                ab2 = ab.a().a(this.g).b(this.f).a();
            }
            else if (b2) {
                ab2 = ab.a().a(this.g).a();
            }
            else {
                ab2 = ab;
                if (b) {
                    ab2 = ab.a().b(this.f).a();
                }
            }
            return new u(ab2, this.a, this.b, this.c.a(ab2), this.d, this.e, null);
        }
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.applovin.exoplayer2.h;

import com.applovin.exoplayer2.l.a;
import com.applovin.exoplayer2.l.h;
import android.util.SparseArray;

final class ab<V>
{
    private int a;
    private final SparseArray<V> b;
    private final h<V> c;
    
    public ab() {
        this((h)new \u3007080());
    }
    
    public ab(final h<V> c) {
        this.b = (SparseArray<V>)new SparseArray();
        this.c = c;
        this.a = -1;
    }
    
    public V a() {
        final SparseArray<V> b = this.b;
        return (V)b.valueAt(b.size() - 1);
    }
    
    public V a(final int n) {
        if (this.a == -1) {
            this.a = 0;
        }
        while (true) {
            final int a = this.a;
            if (a <= 0 || n >= this.b.keyAt(a)) {
                break;
            }
            --this.a;
        }
        while (this.a < this.b.size() - 1 && n >= this.b.keyAt(this.a + 1)) {
            ++this.a;
        }
        return (V)this.b.valueAt(this.a);
    }
    
    public void a(final int n, final V v) {
        final int a = this.a;
        final boolean b = false;
        if (a == -1) {
            com.applovin.exoplayer2.l.a.b(this.b.size() == 0);
            this.a = 0;
        }
        if (this.b.size() > 0) {
            final SparseArray<V> b2 = this.b;
            final int key = b2.keyAt(b2.size() - 1);
            boolean b3 = b;
            if (n >= key) {
                b3 = true;
            }
            com.applovin.exoplayer2.l.a.a(b3);
            if (key == n) {
                final h<V> c = this.c;
                final SparseArray<V> b4 = this.b;
                c.accept(b4.valueAt(b4.size() - 1));
            }
        }
        this.b.append(n, (Object)v);
    }
    
    public void b() {
        for (int i = 0; i < this.b.size(); ++i) {
            this.c.accept(this.b.valueAt(i));
        }
        this.a = -1;
        this.b.clear();
    }
    
    public void b(final int n) {
        int n2;
        for (int i = 0; i < this.b.size() - 1; i = n2) {
            final SparseArray<V> b = this.b;
            n2 = i + 1;
            if (n < b.keyAt(n2)) {
                break;
            }
            this.c.accept(this.b.valueAt(i));
            this.b.removeAt(i);
            final int a = this.a;
            if (a > 0) {
                this.a = a - 1;
            }
        }
    }
    
    public void c(int min) {
        for (int n = this.b.size() - 1; n >= 0 && min < this.b.keyAt(n); --n) {
            this.c.accept(this.b.valueAt(n));
            this.b.removeAt(n);
        }
        if (this.b.size() > 0) {
            min = Math.min(this.a, this.b.size() - 1);
        }
        else {
            min = -1;
        }
        this.a = min;
    }
    
    public boolean c() {
        return this.b.size() == 0;
    }
}

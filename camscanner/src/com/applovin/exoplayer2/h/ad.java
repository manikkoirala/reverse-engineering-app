// 
// Decompiled by Procyon v0.6.0
// 

package com.applovin.exoplayer2.h;

import java.util.Arrays;
import androidx.annotation.Nullable;
import java.util.List;
import com.applovin.exoplayer2.l.c;
import com.applovin.exoplayer2.common.a.s;
import android.os.Bundle;
import com.applovin.exoplayer2.g;

public final class ad implements g
{
    public static final ad a;
    public static final a<ad> c;
    public final int b;
    private final ac[] d;
    private int e;
    
    static {
        a = new ad(new ac[0]);
        c = new \u3007o\u3007();
    }
    
    public ad(final ac... d) {
        this.d = d;
        this.b = d.length;
    }
    
    private static String b(final int i) {
        return Integer.toString(i, 36);
    }
    
    public int a(final ac ac) {
        for (int i = 0; i < this.b; ++i) {
            if (this.d[i] == ac) {
                return i;
            }
        }
        return -1;
    }
    
    public ac a(final int n) {
        return this.d[n];
    }
    
    public boolean a() {
        return this.b == 0;
    }
    
    @Override
    public boolean equals(@Nullable final Object o) {
        boolean b = true;
        if (this == o) {
            return true;
        }
        if (o != null && ad.class == o.getClass()) {
            final ad ad = (ad)o;
            if (this.b != ad.b || !Arrays.equals(this.d, ad.d)) {
                b = false;
            }
            return b;
        }
        return false;
    }
    
    @Override
    public int hashCode() {
        if (this.e == 0) {
            this.e = Arrays.hashCode(this.d);
        }
        return this.e;
    }
}

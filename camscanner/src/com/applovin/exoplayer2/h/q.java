// 
// Decompiled by Procyon v0.6.0
// 

package com.applovin.exoplayer2.h;

import java.util.Iterator;
import com.applovin.exoplayer2.l.ai;
import com.applovin.exoplayer2.l.a;
import android.os.Handler;
import com.applovin.exoplayer2.v;
import androidx.annotation.CheckResult;
import com.applovin.exoplayer2.h;
import java.util.concurrent.CopyOnWriteArrayList;
import java.io.IOException;
import androidx.annotation.Nullable;

public interface q
{
    void a(final int p0, @Nullable final p.a p1, final j p2, final m p3);
    
    void a(final int p0, @Nullable final p.a p1, final j p2, final m p3, final IOException p4, final boolean p5);
    
    void a(final int p0, @Nullable final p.a p1, final m p2);
    
    void b(final int p0, @Nullable final p.a p1, final j p2, final m p3);
    
    void c(final int p0, @Nullable final p.a p1, final j p2, final m p3);
    
    public static class a
    {
        public final int a;
        @Nullable
        public final p.a b;
        private final CopyOnWriteArrayList<q.a.a> c;
        private final long d;
        
        public a() {
            this(new CopyOnWriteArrayList<q.a.a>(), 0, null, 0L);
        }
        
        private a(final CopyOnWriteArrayList<q.a.a> c, final int a, @Nullable final p.a b, final long d) {
            this.c = c;
            this.a = a;
            this.b = b;
            this.d = d;
        }
        
        private long a(long n) {
            final long a = h.a(n);
            n = -9223372036854775807L;
            if (a != -9223372036854775807L) {
                n = this.d + a;
            }
            return n;
        }
        
        @CheckResult
        public q.a a(final int n, @Nullable final p.a a, final long n2) {
            return new q.a(this.c, n, a, n2);
        }
        
        public void a(final int n, @Nullable final v v, final int n2, @Nullable final Object o, final long n3) {
            this.a(new m(1, n, v, n2, o, this.a(n3), -9223372036854775807L));
        }
        
        public void a(final Handler handler, final q q) {
            com.applovin.exoplayer2.l.a.b((Object)handler);
            com.applovin.exoplayer2.l.a.b((Object)q);
            this.c.add(new q.a.a(handler, q));
        }
        
        public void a(final j j, final int n, final int n2, @Nullable final v v, final int n3, @Nullable final Object o, final long n4, final long n5) {
            this.a(j, new m(n, n2, v, n3, o, this.a(n4), this.a(n5)));
        }
        
        public void a(final j j, final int n, final int n2, @Nullable final v v, final int n3, @Nullable final Object o, final long n4, final long n5, final IOException ex, final boolean b) {
            this.a(j, new m(n, n2, v, n3, o, this.a(n4), this.a(n5)), ex, b);
        }
        
        public void a(final j j, final m m) {
            for (final q.a.a a : this.c) {
                ai.a(a.a, (Runnable)new oO80(this, a.b, j, m));
            }
        }
        
        public void a(final j j, final m m, final IOException ex, final boolean b) {
            for (final q.a.a a : this.c) {
                ai.a(a.a, (Runnable)new \u3007\u3007888(this, a.b, j, m, ex, b));
            }
        }
        
        public void a(final m m) {
            for (final q.a.a a : this.c) {
                ai.a(a.a, (Runnable)new OO0o\u3007\u3007\u3007\u30070(this, a.b, m));
            }
        }
        
        public void a(final q q) {
            for (final q.a.a o : this.c) {
                if (o.b == q) {
                    this.c.remove(o);
                }
            }
        }
        
        public void b(final j j, final int n, final int n2, @Nullable final v v, final int n3, @Nullable final Object o, final long n4, final long n5) {
            this.b(j, new m(n, n2, v, n3, o, this.a(n4), this.a(n5)));
        }
        
        public void b(final j j, final m m) {
            for (final q.a.a a : this.c) {
                ai.a(a.a, (Runnable)new \u30078o8o\u3007(this, a.b, j, m));
            }
        }
        
        public void c(final j j, final int n, final int n2, @Nullable final v v, final int n3, @Nullable final Object o, final long n4, final long n5) {
            this.c(j, new m(n, n2, v, n3, o, this.a(n4), this.a(n5)));
        }
        
        public void c(final j j, final m m) {
            for (final q.a.a a : this.c) {
                ai.a(a.a, (Runnable)new \u300780\u3007808\u3007O(this, a.b, j, m));
            }
        }
        
        private static final class a
        {
            public Handler a;
            public q b;
            
            public a(final Handler a, final q b) {
                this.a = a;
                this.b = b;
            }
        }
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.applovin.exoplayer2.h;

import com.applovin.exoplayer2.l.y;
import com.applovin.exoplayer2.l.q;
import androidx.annotation.CallSuper;
import java.io.IOException;
import com.applovin.exoplayer2.e.O8;
import androidx.annotation.GuardedBy;
import com.applovin.exoplayer2.l.u;
import com.applovin.exoplayer2.d.e;
import com.applovin.exoplayer2.l.ai;
import com.applovin.exoplayer2.l.a;
import com.applovin.exoplayer2.k.b;
import com.applovin.exoplayer2.d.f;
import android.os.Looper;
import com.applovin.exoplayer2.d.g;
import com.applovin.exoplayer2.d.h;
import androidx.annotation.Nullable;
import com.applovin.exoplayer2.v;
import com.applovin.exoplayer2.e.x;

public class w implements x
{
    private boolean A;
    @Nullable
    private v B;
    @Nullable
    private v C;
    private int D;
    private boolean E;
    private boolean F;
    private long G;
    private boolean H;
    private final com.applovin.exoplayer2.h.v a;
    private final a b;
    private final ab<b> c;
    @Nullable
    private final h d;
    @Nullable
    private final g.a e;
    @Nullable
    private final Looper f;
    @Nullable
    private c g;
    @Nullable
    private v h;
    @Nullable
    private f i;
    private int j;
    private int[] k;
    private long[] l;
    private int[] m;
    private int[] n;
    private long[] o;
    private x.a[] p;
    private int q;
    private int r;
    private int s;
    private int t;
    private long u;
    private long v;
    private long w;
    private boolean x;
    private boolean y;
    private boolean z;
    
    protected w(final com.applovin.exoplayer2.k.b b, @Nullable final Looper f, @Nullable final h d, @Nullable final g.a e) {
        this.f = f;
        this.d = d;
        this.e = e;
        this.a = new com.applovin.exoplayer2.h.v(b);
        this.b = new a();
        this.j = 1000;
        this.k = new int[1000];
        this.l = new long[1000];
        this.o = new long[1000];
        this.n = new int[1000];
        this.m = new int[1000];
        this.p = new x.a[1000];
        this.c = new ab<b>((com.applovin.exoplayer2.l.h<b>)new \u3007O00());
        this.u = Long.MIN_VALUE;
        this.v = Long.MIN_VALUE;
        this.w = Long.MIN_VALUE;
        this.z = true;
        this.y = true;
    }
    
    private int a(int n, final int n2, final long n3, final boolean b) {
        int n4 = -1;
        final int n5 = 0;
        int n6 = n;
        n = n5;
        int n7;
        while (true) {
            n7 = n4;
            if (n >= n2) {
                break;
            }
            final long n8 = this.o[n6];
            n7 = n4;
            if (n8 > n3) {
                break;
            }
            if (!b || (this.n[n6] & 0x1) != 0x0) {
                if (n8 == n3) {
                    n7 = n;
                    break;
                }
                n4 = n;
            }
            if (++n6 == this.j) {
                n6 = 0;
            }
            ++n;
        }
        return n7;
    }
    
    private int a(final com.applovin.exoplayer2.w w, final com.applovin.exoplayer2.c.g g, final boolean b, final boolean b2, final a a) {
        synchronized (this) {
            g.c = false;
            if (!this.o()) {
                if (b2 || this.x) {
                    g.a_(4);
                    return -4;
                }
                final v c = this.C;
                if (c != null && (b || c != this.h)) {
                    this.a((v)a.b((Object)c), w);
                    return -5;
                }
                return -3;
            }
            else {
                final v a2 = this.c.a(this.f()).a;
                if (b || a2 != this.h) {
                    this.a(a2, w);
                    return -5;
                }
                final int f = this.f(this.t);
                if (!this.c(f)) {
                    g.c = true;
                    return -3;
                }
                g.a_(this.n[f]);
                final long d = this.o[f];
                g.d = d;
                if (d < this.u) {
                    g.b(Integer.MIN_VALUE);
                }
                a.a = this.m[f];
                a.b = this.l[f];
                a.c = this.p[f];
                return -4;
            }
        }
    }
    
    public static w a(final com.applovin.exoplayer2.k.b b, final Looper looper, final h h, final g.a a) {
        return new w(b, (Looper)a.b((Object)looper), (h)a.b((Object)h), (g.a)a.b((Object)a));
    }
    
    private void a(final long b, int c, final long n, int j, @Nullable final x.a a) {
        synchronized (this) {
            final int q = this.q;
            if (q > 0) {
                final int f = this.f(q - 1);
                a.a(this.l[f] + this.m[f] <= n);
            }
            this.x = ((0x20000000 & c) != 0x0);
            this.w = Math.max(this.w, b);
            final int f2 = this.f(this.q);
            this.o[f2] = b;
            this.l[f2] = n;
            this.m[f2] = j;
            this.n[f2] = c;
            this.p[f2] = a;
            this.k[f2] = this.D;
            if (this.c.c() || !this.c.a().a.equals(this.C)) {
                final h d = this.d;
                h.a a2;
                if (d != null) {
                    a2 = d.a((Looper)a.b((Object)this.f), this.e, this.C);
                }
                else {
                    a2 = com.applovin.exoplayer2.d.h.a.b;
                }
                final ab<b> c2 = this.c;
                c = this.c();
                c2.a(c, new b((v)a.b((Object)this.C), a2));
            }
            c = this.q + 1;
            this.q = c;
            j = this.j;
            if (c == j) {
                c = j + 1000;
                final int[] k = new int[c];
                final long[] l = new long[c];
                final long[] o = new long[c];
                final int[] n2 = new int[c];
                final int[] m = new int[c];
                final x.a[] p5 = new x.a[c];
                final int s = this.s;
                j -= s;
                System.arraycopy(this.l, s, l, 0, j);
                System.arraycopy(this.o, this.s, o, 0, j);
                System.arraycopy(this.n, this.s, n2, 0, j);
                System.arraycopy(this.m, this.s, m, 0, j);
                System.arraycopy(this.p, this.s, p5, 0, j);
                System.arraycopy(this.k, this.s, k, 0, j);
                final int s2 = this.s;
                System.arraycopy(this.l, 0, l, j, s2);
                System.arraycopy(this.o, 0, o, j, s2);
                System.arraycopy(this.n, 0, n2, j, s2);
                System.arraycopy(this.m, 0, m, j, s2);
                System.arraycopy(this.p, 0, p5, j, s2);
                System.arraycopy(this.k, 0, k, j, s2);
                this.l = l;
                this.o = o;
                this.n = n2;
                this.m = m;
                this.p = p5;
                this.k = k;
                this.s = 0;
                this.j = c;
            }
        }
    }
    
    private void a(final v h, final com.applovin.exoplayer2.w w) {
        final v h2 = this.h;
        final boolean b = h2 == null;
        Object o;
        if (b) {
            o = null;
        }
        else {
            o = h2.o;
        }
        this.h = h;
        final e o2 = h.o;
        final h d = this.d;
        v a;
        if (d != null) {
            a = h.a(d.a(h));
        }
        else {
            a = h;
        }
        w.b = a;
        w.a = this.i;
        if (this.d == null) {
            return;
        }
        if (!b && ai.a(o, (Object)o2)) {
            return;
        }
        final f i = this.i;
        final f b2 = this.d.b((Looper)com.applovin.exoplayer2.l.a.b((Object)this.f), this.e, h);
        this.i = b2;
        w.a = b2;
        if (i != null) {
            i.b(this.e);
        }
    }
    
    private long b(int n) {
        final int n2 = this.c() - n;
        final boolean b = false;
        com.applovin.exoplayer2.l.a.a(n2 >= 0 && n2 <= this.q - this.t);
        final int q = this.q - n2;
        this.q = q;
        this.w = Math.max(this.v, this.e(q));
        boolean x = b;
        if (n2 == 0) {
            x = b;
            if (this.x) {
                x = true;
            }
        }
        this.x = x;
        this.c.c(n);
        n = this.q;
        if (n != 0) {
            n = this.f(n - 1);
            return this.l[n] + this.m[n];
        }
        return 0L;
    }
    
    private long b(long d, final boolean b, final boolean b2) {
        synchronized (this) {
            final int q = this.q;
            if (q != 0) {
                final long[] o = this.o;
                final int s = this.s;
                if (d >= o[s]) {
                    int n = q;
                    if (b2) {
                        final int t = this.t;
                        if (t != (n = q)) {
                            n = t + 1;
                        }
                    }
                    final int a = this.a(s, n, d, b);
                    if (a == -1) {
                        return -1L;
                    }
                    d = this.d(a);
                    return d;
                }
            }
            return -1L;
        }
    }
    
    private boolean b(final long n) {
        synchronized (this) {
            final int q = this.q;
            boolean b = true;
            if (q == 0) {
                if (n <= this.v) {
                    b = false;
                }
                return b;
            }
            if (this.i() >= n) {
                return false;
            }
            this.b(this.r + this.c(n));
            return true;
        }
    }
    
    private int c(final long n) {
        int q = this.q;
        int n2;
        for (int f = this.f(q - 1); q > this.t && this.o[f] >= n; f = this.j - 1, q = n2) {
            n2 = q - 1;
            final int n3 = f - 1;
            q = n2;
            if ((f = n3) == -1) {}
        }
        return q;
    }
    
    private boolean c(final int n) {
        final f i = this.i;
        return i == null || i.c() == 4 || ((this.n[n] & 0x40000000) == 0x0 && this.i.d());
    }
    
    private boolean c(v c) {
        synchronized (this) {
            this.z = false;
            if (ai.a((Object)c, (Object)this.C)) {
                return false;
            }
            if (!this.c.c() && this.c.a().a.equals(c)) {
                this.C = this.c.a().a;
            }
            else {
                this.C = c;
            }
            c = this.C;
            this.E = com.applovin.exoplayer2.l.u.a(c.l, c.i);
            this.F = false;
            return true;
        }
    }
    
    @GuardedBy("this")
    private long d(int t) {
        this.v = Math.max(this.v, this.e(t));
        this.q -= t;
        final int r = this.r + t;
        this.r = r;
        final int s = this.s + t;
        this.s = s;
        final int j = this.j;
        if (s >= j) {
            this.s = s - j;
        }
        t = this.t - t;
        if ((this.t = t) < 0) {
            this.t = 0;
        }
        this.c.b(r);
        if (this.q == 0) {
            if ((t = this.s) == 0) {
                t = this.j;
            }
            --t;
            return this.l[t] + this.m[t];
        }
        return this.l[this.s];
    }
    
    private long e(final int n) {
        long max = Long.MIN_VALUE;
        if (n == 0) {
            return Long.MIN_VALUE;
        }
        int f = this.f(n - 1);
        int n2 = 0;
        long n3;
        while (true) {
            n3 = max;
            if (n2 >= n) {
                break;
            }
            max = Math.max(max, this.o[f]);
            if ((this.n[f] & 0x1) != 0x0) {
                n3 = max;
                break;
            }
            if (--f == -1) {
                f = this.j - 1;
            }
            ++n2;
        }
        return n3;
    }
    
    private int f(int n) {
        n += this.s;
        final int j = this.j;
        if (n >= j) {
            n -= j;
        }
        return n;
    }
    
    private void l() {
        synchronized (this) {
            this.t = 0;
            this.a.b();
        }
    }
    
    private long m() {
        synchronized (this) {
            final int q = this.q;
            if (q == 0) {
                return -1L;
            }
            return this.d(q);
        }
    }
    
    private void n() {
        final f i = this.i;
        if (i != null) {
            i.b(this.e);
            this.i = null;
            this.h = null;
        }
    }
    
    private boolean o() {
        return this.t != this.q;
    }
    
    @Override
    public final int a(final com.applovin.exoplayer2.k.g g, final int n, final boolean b, final int n2) throws IOException {
        return this.a.a(g, n, b);
    }
    
    @CallSuper
    public int a(final com.applovin.exoplayer2.w w, final com.applovin.exoplayer2.c.g g, final int n, final boolean b) {
        boolean b2 = false;
        final int a = this.a(w, g, (n & 0x2) != 0x0, b, this.b);
        if (a == -4 && !g.c()) {
            if ((n & 0x1) != 0x0) {
                b2 = true;
            }
            if ((n & 0x4) == 0x0) {
                if (b2) {
                    this.a.b(g, this.b);
                }
                else {
                    this.a.a(g, this.b);
                }
            }
            if (!b2) {
                ++this.t;
            }
        }
        return a;
    }
    
    @CallSuper
    public void a() {
        this.a(true);
        this.n();
    }
    
    public final void a(final int n) {
        monitorenter(this);
        boolean b = false;
        Label_0030: {
            Label_0028: {
                if (n >= 0) {
                    Label_0047: {
                        try {
                            if (this.t + n <= this.q) {
                                b = true;
                                break Label_0030;
                            }
                        }
                        finally {
                            break Label_0047;
                        }
                        break Label_0028;
                    }
                    monitorexit(this);
                }
            }
            b = false;
        }
        com.applovin.exoplayer2.l.a.a(b);
        this.t += n;
        monitorexit(this);
    }
    
    public final void a(final long u) {
        this.u = u;
    }
    
    @Override
    public void a(long n, int n2, final int n3, final int n4, @Nullable final x.a a) {
        if (this.A) {
            this.a((v)a.a((Object)this.B));
        }
        final int n5 = n2 & 0x1;
        final boolean b = n5 != 0;
        if (this.y) {
            if (!b) {
                return;
            }
            this.y = false;
        }
        n += this.G;
        if (this.E) {
            if (n < this.u) {
                return;
            }
            if (n5 == 0) {
                if (!this.F) {
                    final StringBuilder sb = new StringBuilder();
                    sb.append("Overriding unexpected non-sync sample for format: ");
                    sb.append(this.C);
                    com.applovin.exoplayer2.l.q.c("SampleQueue", sb.toString());
                    this.F = true;
                }
                n2 |= 0x1;
            }
        }
        if (this.H) {
            if (!b || !this.b(n)) {
                return;
            }
            this.H = false;
        }
        this.a(n, n2, this.a.c() - n3 - n4, n3, a);
    }
    
    public final void a(final long n, final boolean b, final boolean b2) {
        this.a.a(this.b(n, b, b2));
    }
    
    public final void a(@Nullable final c g) {
        this.g = g;
    }
    
    @Override
    public final void a(final y y, final int n, final int n2) {
        this.a.a(y, n);
    }
    
    @Override
    public final void a(final v b) {
        final v b2 = this.b(b);
        this.A = false;
        this.B = b;
        final boolean c = this.c(b2);
        final c g = this.g;
        if (g != null && c) {
            g.a(b2);
        }
    }
    
    @CallSuper
    public void a(final boolean b) {
        this.a.a();
        this.q = 0;
        this.r = 0;
        this.s = 0;
        this.t = 0;
        this.y = true;
        this.u = Long.MIN_VALUE;
        this.v = Long.MIN_VALUE;
        this.w = Long.MIN_VALUE;
        this.x = false;
        this.c.b();
        if (b) {
            this.B = null;
            this.C = null;
            this.z = true;
        }
    }
    
    public final boolean a(final long u, final boolean b) {
        synchronized (this) {
            this.l();
            final int f = this.f(this.t);
            if (!this.o() || u < this.o[f] || (u > this.w && !b)) {
                return false;
            }
            final int a = this.a(f, this.q - this.t, u, true);
            if (a == -1) {
                return false;
            }
            this.u = u;
            this.t += a;
            return true;
        }
    }
    
    public final int b(final long n, final boolean b) {
        synchronized (this) {
            final int f = this.f(this.t);
            if (!this.o() || n < this.o[f]) {
                return 0;
            }
            if (n > this.w && b) {
                final int q = this.q;
                final int t = this.t;
                monitorexit(this);
                return q - t;
            }
            final int a = this.a(f, this.q - this.t, n, true);
            if (a == -1) {
                return 0;
            }
            return a;
        }
    }
    
    @CallSuper
    protected v b(final v v) {
        v a = v;
        if (this.G != 0L) {
            a = v;
            if (v.p != Long.MAX_VALUE) {
                a = v.a().a(v.p + this.G).a();
            }
        }
        return a;
    }
    
    public final void b() {
        this.a(false);
    }
    
    @CallSuper
    public boolean b(final boolean b) {
        synchronized (this) {
            final boolean o = this.o();
            final boolean b2 = true;
            if (!o) {
                boolean b3 = b2;
                if (!b) {
                    b3 = b2;
                    if (!this.x) {
                        final v c = this.C;
                        b3 = (c != null && c != this.h && b2);
                    }
                }
                return b3;
            }
            return this.c.a(this.f()).a != this.h || this.c(this.f(this.t));
        }
    }
    
    public final int c() {
        return this.r + this.q;
    }
    
    @CallSuper
    public void d() {
        this.k();
        this.n();
    }
    
    @CallSuper
    public void e() throws IOException {
        final f i = this.i;
        if (i != null && i.c() == 1) {
            throw (f.a)com.applovin.exoplayer2.l.a.b((Object)this.i.e());
        }
    }
    
    public final int f() {
        return this.r + this.t;
    }
    
    @Nullable
    public final v g() {
        synchronized (this) {
            v c;
            if (this.z) {
                c = null;
            }
            else {
                c = this.C;
            }
            return c;
        }
    }
    
    public final long h() {
        synchronized (this) {
            return this.w;
        }
    }
    
    public final long i() {
        synchronized (this) {
            return Math.max(this.v, this.e(this.t));
        }
    }
    
    public final boolean j() {
        synchronized (this) {
            return this.x;
        }
    }
    
    public final void k() {
        this.a.a(this.m());
    }
    
    static final class a
    {
        public int a;
        public long b;
        @Nullable
        public x.a c;
    }
    
    private static final class b
    {
        public final v a;
        public final h.a b;
        
        private b(final v a, final h.a b) {
            this.a = a;
            this.b = b;
        }
    }
    
    public interface c
    {
        void a(final v p0);
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.applovin.exoplayer2.h;

import com.applovin.exoplayer2.g;
import com.applovin.exoplayer2.l.a;
import android.net.Uri;
import androidx.annotation.Nullable;
import com.applovin.exoplayer2.ab;
import com.applovin.exoplayer2.ba;

public final class aa extends ba
{
    private static final Object c;
    private static final ab d;
    private final long e;
    private final long f;
    private final long g;
    private final long h;
    private final long i;
    private final long j;
    private final long k;
    private final boolean l;
    private final boolean m;
    private final boolean n;
    @Nullable
    private final Object o;
    @Nullable
    private final ab p;
    @Nullable
    private final ab.e q;
    
    static {
        c = new Object();
        d = new ab.b().a("SinglePeriodTimeline").a(Uri.EMPTY).a();
    }
    
    public aa(final long e, final long f, final long g, final long h, final long i, final long j, final long k, final boolean l, final boolean m, final boolean n, @Nullable final Object o, final ab ab, @Nullable final ab.e q) {
        this.e = e;
        this.f = f;
        this.g = g;
        this.h = h;
        this.i = i;
        this.j = j;
        this.k = k;
        this.l = l;
        this.m = m;
        this.n = n;
        this.o = o;
        this.p = (ab)com.applovin.exoplayer2.l.a.b((Object)ab);
        this.q = q;
    }
    
    public aa(final long n, final long n2, final long n3, final long n4, final boolean b, final boolean b2, final boolean b3, @Nullable final Object o, final ab ab) {
        g d;
        if (b3) {
            d = ab.d;
        }
        else {
            d = null;
        }
        this(-9223372036854775807L, -9223372036854775807L, -9223372036854775807L, n, n2, n3, n4, b, b2, false, o, ab, (ab.e)d);
    }
    
    public aa(final long n, final boolean b, final boolean b2, final boolean b3, @Nullable final Object o, final ab ab) {
        this(n, n, 0L, 0L, b, b2, b3, o, ab);
    }
    
    @Override
    public a a(final int n, final a a, final boolean b) {
        a.a(n, 0, 1);
        Object c;
        if (b) {
            c = aa.c;
        }
        else {
            c = null;
        }
        return a.a(null, c, 0, this.h, -this.j);
    }
    
    @Override
    public c a(final int n, final c c, long n2) {
        com.applovin.exoplayer2.l.a.a(n, 0, 1);
        final long k = this.k;
        final boolean m = this.m;
        long n3 = k;
        Label_0089: {
            if (m) {
                n3 = k;
                if (!this.n) {
                    n3 = k;
                    if (n2 != 0L) {
                        final long i = this.i;
                        if (i != -9223372036854775807L) {
                            n2 = (n3 = k + n2);
                            if (n2 <= i) {
                                break Label_0089;
                            }
                        }
                        n2 = -9223372036854775807L;
                        return c.a(ba.c.a, this.p, this.o, this.e, this.f, this.g, this.l, m, this.q, n2, this.i, 0, 0, this.j);
                    }
                }
            }
        }
        n2 = n3;
        return c.a(ba.c.a, this.p, this.o, this.e, this.f, this.g, this.l, m, this.q, n2, this.i, 0, 0, this.j);
    }
    
    @Override
    public Object a(final int n) {
        com.applovin.exoplayer2.l.a.a(n, 0, 1);
        return aa.c;
    }
    
    @Override
    public int b() {
        return 1;
    }
    
    @Override
    public int c() {
        return 1;
    }
    
    @Override
    public int c(final Object obj) {
        int n;
        if (aa.c.equals(obj)) {
            n = 0;
        }
        else {
            n = -1;
        }
        return n;
    }
}

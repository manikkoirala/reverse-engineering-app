// 
// Decompiled by Procyon v0.6.0
// 

package com.applovin.exoplayer2;

import android.os.BaseBundle;
import java.util.Arrays;
import java.util.ArrayList;
import com.applovin.exoplayer2.l.c;
import android.os.Bundle;
import java.util.Collections;
import com.applovin.exoplayer2.l.ai;
import com.applovin.exoplayer2.m.b;
import com.applovin.exoplayer2.d.e;
import java.util.List;
import com.applovin.exoplayer2.g.a;
import androidx.annotation.Nullable;

public final class v implements g
{
    public static final g.a<v> F;
    private static final v G;
    public final int A;
    public final int B;
    public final int C;
    public final int D;
    public final int E;
    private int H;
    @Nullable
    public final String a;
    @Nullable
    public final String b;
    @Nullable
    public final String c;
    public final int d;
    public final int e;
    public final int f;
    public final int g;
    public final int h;
    @Nullable
    public final String i;
    @Nullable
    public final com.applovin.exoplayer2.g.a j;
    @Nullable
    public final String k;
    @Nullable
    public final String l;
    public final int m;
    public final List<byte[]> n;
    @Nullable
    public final e o;
    public final long p;
    public final int q;
    public final int r;
    public final float s;
    public final int t;
    public final float u;
    @Nullable
    public final byte[] v;
    public final int w;
    @Nullable
    public final b x;
    public final int y;
    public final int z;
    
    static {
        G = new a().a();
        F = new O8O\u3007();
    }
    
    private v(final a a) {
        this.a = a.a;
        this.b = a.b;
        this.c = ai.b(a.c);
        this.d = a.d;
        this.e = a.e;
        int f = a.f;
        this.f = f;
        final int g = a.g;
        this.g = g;
        if (g != -1) {
            f = g;
        }
        this.h = f;
        this.i = a.h;
        this.j = a.i;
        this.k = a.j;
        this.l = a.k;
        this.m = a.l;
        List<Object> n;
        if (a.m == null) {
            n = (List<Object>)Collections.emptyList();
        }
        else {
            n = (List<Object>)a.m;
        }
        this.n = (List<byte[]>)n;
        final e n2 = a.n;
        this.o = n2;
        this.p = a.o;
        this.q = a.p;
        this.r = a.q;
        this.s = a.r;
        final int s = a.s;
        final int n3 = 0;
        int s2;
        if (s == -1) {
            s2 = 0;
        }
        else {
            s2 = a.s;
        }
        this.t = s2;
        float t;
        if (a.t == -1.0f) {
            t = 1.0f;
        }
        else {
            t = a.t;
        }
        this.u = t;
        this.v = a.u;
        this.w = a.v;
        this.x = a.w;
        this.y = a.x;
        this.z = a.y;
        this.A = a.z;
        int a2;
        if (a.A == -1) {
            a2 = 0;
        }
        else {
            a2 = a.A;
        }
        this.B = a2;
        int b;
        if (a.B == -1) {
            b = n3;
        }
        else {
            b = a.B;
        }
        this.C = b;
        this.D = a.C;
        if (a.D == 0 && n2 != null) {
            this.E = 1;
        }
        else {
            this.E = a.D;
        }
    }
    
    private static v a(final Bundle bundle) {
        final a a = new a();
        c.a(bundle);
        int n = 0;
        final String string = ((BaseBundle)bundle).getString(b(0));
        final v g = v.G;
        a.a(a(string, g.a)).b(a(((BaseBundle)bundle).getString(b(1)), g.b)).c(a(((BaseBundle)bundle).getString(b(2)), g.c)).b(((BaseBundle)bundle).getInt(b(3), g.d)).c(((BaseBundle)bundle).getInt(b(4), g.e)).d(((BaseBundle)bundle).getInt(b(5), g.f)).e(((BaseBundle)bundle).getInt(b(6), g.g)).d(a(((BaseBundle)bundle).getString(b(7)), g.i)).a(a(bundle.getParcelable(b(8)), g.j)).e(a(((BaseBundle)bundle).getString(b(9)), g.k)).f(a(((BaseBundle)bundle).getString(b(10)), g.l)).f(((BaseBundle)bundle).getInt(b(11), g.m));
        final ArrayList list = new ArrayList();
        while (true) {
            final byte[] byteArray = bundle.getByteArray(c(n));
            if (byteArray == null) {
                break;
            }
            list.add(byteArray);
            ++n;
        }
        final a a2 = a.a(list).a((e)bundle.getParcelable(b(13)));
        final String b = b(14);
        final v g2 = v.G;
        a2.a(((BaseBundle)bundle).getLong(b, g2.p)).g(((BaseBundle)bundle).getInt(b(15), g2.q)).h(((BaseBundle)bundle).getInt(b(16), g2.r)).a(bundle.getFloat(b(17), g2.s)).i(((BaseBundle)bundle).getInt(b(18), g2.t)).b(bundle.getFloat(b(19), g2.u)).a(bundle.getByteArray(b(20))).j(((BaseBundle)bundle).getInt(b(21), g2.w)).a((b)c.a(com.applovin.exoplayer2.m.b.e, bundle.getBundle(b(22)))).k(((BaseBundle)bundle).getInt(b(23), g2.y)).l(((BaseBundle)bundle).getInt(b(24), g2.z)).m(((BaseBundle)bundle).getInt(b(25), g2.A)).n(((BaseBundle)bundle).getInt(b(26), g2.B)).o(((BaseBundle)bundle).getInt(b(27), g2.C)).p(((BaseBundle)bundle).getInt(b(28), g2.D)).q(((BaseBundle)bundle).getInt(b(29), g2.E));
        return a.a();
    }
    
    @Nullable
    private static <T> T a(@Nullable T t, @Nullable final T t2) {
        if (t == null) {
            t = t2;
        }
        return t;
    }
    
    private static String b(final int i) {
        return Integer.toString(i, 36);
    }
    
    private static String c(final int i) {
        final StringBuilder sb = new StringBuilder();
        sb.append(b(12));
        sb.append("_");
        sb.append(Integer.toString(i, 36));
        return sb.toString();
    }
    
    public a a() {
        return new a(this);
    }
    
    public v a(final int n) {
        return this.a().q(n).a();
    }
    
    public boolean a(final v v) {
        if (this.n.size() != v.n.size()) {
            return false;
        }
        for (int i = 0; i < this.n.size(); ++i) {
            if (!Arrays.equals(this.n.get(i), v.n.get(i))) {
                return false;
            }
        }
        return true;
    }
    
    public int b() {
        final int q = this.q;
        int n = -1;
        if (q != -1) {
            final int r = this.r;
            if (r == -1) {
                n = n;
            }
            else {
                n = q * r;
            }
        }
        return n;
    }
    
    @Override
    public boolean equals(@Nullable final Object o) {
        boolean b = true;
        if (this == o) {
            return true;
        }
        if (o != null && v.class == o.getClass()) {
            final v v = (v)o;
            final int h = this.H;
            if (h != 0) {
                final int h2 = v.H;
                if (h2 != 0 && h != h2) {
                    return false;
                }
            }
            if (this.d != v.d || this.e != v.e || this.f != v.f || this.g != v.g || this.m != v.m || this.p != v.p || this.q != v.q || this.r != v.r || this.t != v.t || this.w != v.w || this.y != v.y || this.z != v.z || this.A != v.A || this.B != v.B || this.C != v.C || this.D != v.D || this.E != v.E || Float.compare(this.s, v.s) != 0 || Float.compare(this.u, v.u) != 0 || !ai.a((Object)this.a, (Object)v.a) || !ai.a((Object)this.b, (Object)v.b) || !ai.a((Object)this.i, (Object)v.i) || !ai.a((Object)this.k, (Object)v.k) || !ai.a((Object)this.l, (Object)v.l) || !ai.a((Object)this.c, (Object)v.c) || !Arrays.equals(this.v, v.v) || !ai.a((Object)this.j, (Object)v.j) || !ai.a((Object)this.x, (Object)v.x) || !ai.a((Object)this.o, (Object)v.o) || !this.a(v)) {
                b = false;
            }
            return b;
        }
        return false;
    }
    
    @Override
    public int hashCode() {
        if (this.H == 0) {
            final String a = this.a;
            int hashCode = 0;
            int hashCode2;
            if (a == null) {
                hashCode2 = 0;
            }
            else {
                hashCode2 = a.hashCode();
            }
            final String b = this.b;
            int hashCode3;
            if (b != null) {
                hashCode3 = b.hashCode();
            }
            else {
                hashCode3 = 0;
            }
            final String c = this.c;
            int hashCode4;
            if (c == null) {
                hashCode4 = 0;
            }
            else {
                hashCode4 = c.hashCode();
            }
            final int d = this.d;
            final int e = this.e;
            final int f = this.f;
            final int g = this.g;
            final String i = this.i;
            int hashCode5;
            if (i == null) {
                hashCode5 = 0;
            }
            else {
                hashCode5 = i.hashCode();
            }
            final com.applovin.exoplayer2.g.a j = this.j;
            int hashCode6;
            if (j == null) {
                hashCode6 = 0;
            }
            else {
                hashCode6 = j.hashCode();
            }
            final String k = this.k;
            int hashCode7;
            if (k == null) {
                hashCode7 = 0;
            }
            else {
                hashCode7 = k.hashCode();
            }
            final String l = this.l;
            if (l != null) {
                hashCode = l.hashCode();
            }
            this.H = (((((((((((((((((((((((((527 + hashCode2) * 31 + hashCode3) * 31 + hashCode4) * 31 + d) * 31 + e) * 31 + f) * 31 + g) * 31 + hashCode5) * 31 + hashCode6) * 31 + hashCode7) * 31 + hashCode) * 31 + this.m) * 31 + (int)this.p) * 31 + this.q) * 31 + this.r) * 31 + Float.floatToIntBits(this.s)) * 31 + this.t) * 31 + Float.floatToIntBits(this.u)) * 31 + this.w) * 31 + this.y) * 31 + this.z) * 31 + this.A) * 31 + this.B) * 31 + this.C) * 31 + this.D) * 31 + this.E;
        }
        return this.H;
    }
    
    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append("Format(");
        sb.append(this.a);
        sb.append(", ");
        sb.append(this.b);
        sb.append(", ");
        sb.append(this.k);
        sb.append(", ");
        sb.append(this.l);
        sb.append(", ");
        sb.append(this.i);
        sb.append(", ");
        sb.append(this.h);
        sb.append(", ");
        sb.append(this.c);
        sb.append(", [");
        sb.append(this.q);
        sb.append(", ");
        sb.append(this.r);
        sb.append(", ");
        sb.append(this.s);
        sb.append("], [");
        sb.append(this.y);
        sb.append(", ");
        sb.append(this.z);
        sb.append("])");
        return sb.toString();
    }
    
    public static final class a
    {
        private int A;
        private int B;
        private int C;
        private int D;
        @Nullable
        private String a;
        @Nullable
        private String b;
        @Nullable
        private String c;
        private int d;
        private int e;
        private int f;
        private int g;
        @Nullable
        private String h;
        @Nullable
        private com.applovin.exoplayer2.g.a i;
        @Nullable
        private String j;
        @Nullable
        private String k;
        private int l;
        @Nullable
        private List<byte[]> m;
        @Nullable
        private e n;
        private long o;
        private int p;
        private int q;
        private float r;
        private int s;
        private float t;
        @Nullable
        private byte[] u;
        private int v;
        @Nullable
        private b w;
        private int x;
        private int y;
        private int z;
        
        public a() {
            this.f = -1;
            this.g = -1;
            this.l = -1;
            this.o = Long.MAX_VALUE;
            this.p = -1;
            this.q = -1;
            this.r = -1.0f;
            this.t = 1.0f;
            this.v = -1;
            this.x = -1;
            this.y = -1;
            this.z = -1;
            this.C = -1;
            this.D = 0;
        }
        
        private a(final v v) {
            this.a = v.a;
            this.b = v.b;
            this.c = v.c;
            this.d = v.d;
            this.e = v.e;
            this.f = v.f;
            this.g = v.g;
            this.h = v.i;
            this.i = v.j;
            this.j = v.k;
            this.k = v.l;
            this.l = v.m;
            this.m = v.n;
            this.n = v.o;
            this.o = v.p;
            this.p = v.q;
            this.q = v.r;
            this.r = v.s;
            this.s = v.t;
            this.t = v.u;
            this.u = v.v;
            this.v = v.w;
            this.w = v.x;
            this.x = v.y;
            this.y = v.z;
            this.z = v.A;
            this.A = v.B;
            this.B = v.C;
            this.C = v.D;
            this.D = v.E;
        }
        
        public a a(final float r) {
            this.r = r;
            return this;
        }
        
        public a a(final int i) {
            this.a = Integer.toString(i);
            return this;
        }
        
        public a a(final long o) {
            this.o = o;
            return this;
        }
        
        public a a(@Nullable final e n) {
            this.n = n;
            return this;
        }
        
        public a a(@Nullable final com.applovin.exoplayer2.g.a i) {
            this.i = i;
            return this;
        }
        
        public a a(@Nullable final b w) {
            this.w = w;
            return this;
        }
        
        public a a(@Nullable final String a) {
            this.a = a;
            return this;
        }
        
        public a a(@Nullable final List<byte[]> m) {
            this.m = m;
            return this;
        }
        
        public a a(@Nullable final byte[] u) {
            this.u = u;
            return this;
        }
        
        public v a() {
            return new v(this, null);
        }
        
        public a b(final float t) {
            this.t = t;
            return this;
        }
        
        public a b(final int d) {
            this.d = d;
            return this;
        }
        
        public a b(@Nullable final String b) {
            this.b = b;
            return this;
        }
        
        public a c(final int e) {
            this.e = e;
            return this;
        }
        
        public a c(@Nullable final String c) {
            this.c = c;
            return this;
        }
        
        public a d(final int f) {
            this.f = f;
            return this;
        }
        
        public a d(@Nullable final String h) {
            this.h = h;
            return this;
        }
        
        public a e(final int g) {
            this.g = g;
            return this;
        }
        
        public a e(@Nullable final String j) {
            this.j = j;
            return this;
        }
        
        public a f(final int l) {
            this.l = l;
            return this;
        }
        
        public a f(@Nullable final String k) {
            this.k = k;
            return this;
        }
        
        public a g(final int p) {
            this.p = p;
            return this;
        }
        
        public a h(final int q) {
            this.q = q;
            return this;
        }
        
        public a i(final int s) {
            this.s = s;
            return this;
        }
        
        public a j(final int v) {
            this.v = v;
            return this;
        }
        
        public a k(final int x) {
            this.x = x;
            return this;
        }
        
        public a l(final int y) {
            this.y = y;
            return this;
        }
        
        public a m(final int z) {
            this.z = z;
            return this;
        }
        
        public a n(final int a) {
            this.A = a;
            return this;
        }
        
        public a o(final int b) {
            this.B = b;
            return this;
        }
        
        public a p(final int c) {
            this.C = c;
            return this;
        }
        
        public a q(final int d) {
            this.D = d;
            return this;
        }
    }
}

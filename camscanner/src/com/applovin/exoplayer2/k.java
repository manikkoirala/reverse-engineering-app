// 
// Decompiled by Procyon v0.6.0
// 

package com.applovin.exoplayer2;

import android.os.SystemClock;
import com.applovin.exoplayer2.l.ai;
import com.applovin.exoplayer2.common.b.d;

public final class k implements z
{
    private final float a;
    private final float b;
    private final long c;
    private final float d;
    private final long e;
    private final long f;
    private final float g;
    private long h;
    private long i;
    private long j;
    private long k;
    private long l;
    private long m;
    private float n;
    private float o;
    private float p;
    private long q;
    private long r;
    private long s;
    
    private k(final float n, final float n2, final long c, final float d, final long e, final long f, final float g) {
        this.a = n;
        this.b = n2;
        this.c = c;
        this.d = d;
        this.e = e;
        this.f = f;
        this.g = g;
        this.h = -9223372036854775807L;
        this.i = -9223372036854775807L;
        this.k = -9223372036854775807L;
        this.l = -9223372036854775807L;
        this.o = n;
        this.n = n2;
        this.p = 1.0f;
        this.q = -9223372036854775807L;
        this.j = -9223372036854775807L;
        this.m = -9223372036854775807L;
        this.r = -9223372036854775807L;
        this.s = -9223372036854775807L;
    }
    
    private static long a(final long n, final long n2, final float n3) {
        return (long)(n * n3 + (1.0f - n3) * n2);
    }
    
    private void b(long m) {
        final long n = this.r + this.s * 3L;
        if (this.m > n) {
            m = com.applovin.exoplayer2.h.b(this.c);
            final float p = this.p;
            final float n2 = (float)m;
            m = (long)((p - 1.0f) * n2);
            this.m = com.applovin.exoplayer2.common.b.d.a(n, this.j, this.m - (m + (long)((this.n - 1.0f) * n2)));
        }
        else {
            m = ai.a(m - (long)(Math.max(0.0f, this.p - 1.0f) / this.d), this.m, n);
            this.m = m;
            final long l = this.l;
            if (l != -9223372036854775807L && m > l) {
                this.m = l;
            }
        }
    }
    
    private void b(long abs, long r) {
        abs -= r;
        r = this.r;
        if (r == -9223372036854775807L) {
            this.r = abs;
            this.s = 0L;
        }
        else {
            r = Math.max(abs, a(r, abs, this.g));
            this.r = r;
            abs = Math.abs(abs - r);
            this.s = a(this.s, abs, this.g);
        }
    }
    
    private void c() {
        long h = this.h;
        long n2;
        if (h != -9223372036854775807L) {
            final long i = this.i;
            if (i != -9223372036854775807L) {
                h = i;
            }
            final long k = this.k;
            long n = h;
            if (k != -9223372036854775807L) {
                n = h;
                if (h < k) {
                    n = k;
                }
            }
            final long l = this.l;
            n2 = n;
            if (l != -9223372036854775807L) {
                n2 = n;
                if (n > l) {
                    n2 = l;
                }
            }
        }
        else {
            n2 = -9223372036854775807L;
        }
        if (this.j == n2) {
            return;
        }
        this.j = n2;
        this.m = n2;
        this.r = -9223372036854775807L;
        this.s = -9223372036854775807L;
        this.q = -9223372036854775807L;
    }
    
    @Override
    public float a(long a, final long n) {
        if (this.h == -9223372036854775807L) {
            return 1.0f;
        }
        this.b(a, n);
        if (this.q != -9223372036854775807L && SystemClock.elapsedRealtime() - this.q < this.c) {
            return this.p;
        }
        this.q = SystemClock.elapsedRealtime();
        this.b(a);
        a -= this.m;
        if (Math.abs(a) < this.e) {
            this.p = 1.0f;
        }
        else {
            this.p = ai.a(this.d * a + 1.0f, this.o, this.n);
        }
        return this.p;
    }
    
    @Override
    public void a() {
        final long m = this.m;
        if (m == -9223372036854775807L) {
            return;
        }
        final long i = m + this.f;
        this.m = i;
        final long l = this.l;
        if (l != -9223372036854775807L && i > l) {
            this.m = l;
        }
        this.q = -9223372036854775807L;
    }
    
    @Override
    public void a(final long i) {
        this.i = i;
        this.c();
    }
    
    @Override
    public void a(final ab.e e) {
        this.h = com.applovin.exoplayer2.h.b(e.b);
        this.k = com.applovin.exoplayer2.h.b(e.c);
        this.l = com.applovin.exoplayer2.h.b(e.d);
        float o = e.e;
        if (o == -3.4028235E38f) {
            o = this.a;
        }
        this.o = o;
        float n = e.f;
        if (n == -3.4028235E38f) {
            n = this.b;
        }
        this.n = n;
        this.c();
    }
    
    @Override
    public long b() {
        return this.m;
    }
    
    public static final class a
    {
        private float a;
        private float b;
        private long c;
        private float d;
        private long e;
        private long f;
        private float g;
        
        public a() {
            this.a = 0.97f;
            this.b = 1.03f;
            this.c = 1000L;
            this.d = 1.0E-7f;
            this.e = h.b(20L);
            this.f = h.b(500L);
            this.g = 0.999f;
        }
        
        public k a() {
            return new k(this.a, this.b, this.c, this.d, this.e, this.f, this.g, null);
        }
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.applovin.exoplayer2;

import android.os.BaseBundle;
import java.util.Arrays;
import com.applovin.exoplayer2.common.a.s;
import com.applovin.exoplayer2.common.a.u;
import java.util.UUID;
import java.util.Collections;
import java.util.List;
import com.applovin.exoplayer2.l.ai;
import com.applovin.exoplayer2.l.a;
import android.os.Bundle;
import android.net.Uri;
import androidx.annotation.Nullable;

public final class ab implements g
{
    public static final ab a;
    public static final g.a<ab> g;
    public final String b;
    @Nullable
    public final f c;
    public final e d;
    public final ac e;
    public final c f;
    
    static {
        a = new b().a();
        g = new \u3007080();
    }
    
    private ab(final String b, final c f, @Nullable final f c, final e d, final ac e) {
        this.b = b;
        this.c = c;
        this.d = d;
        this.e = e;
        this.f = f;
    }
    
    public static ab a(final Uri uri) {
        return new b().a(uri).a();
    }
    
    private static ab a(Bundle bundle) {
        final String s = (String)com.applovin.exoplayer2.l.a.b((Object)((BaseBundle)bundle).getString(a(0), ""));
        final Bundle bundle2 = bundle.getBundle(a(1));
        e a;
        if (bundle2 == null) {
            a = e.a;
        }
        else {
            a = e.g.fromBundle(bundle2);
        }
        final Bundle bundle3 = bundle.getBundle(a(2));
        ac a2;
        if (bundle3 == null) {
            a2 = ac.a;
        }
        else {
            a2 = ac.H.fromBundle(bundle3);
        }
        bundle = bundle.getBundle(a(3));
        c c;
        if (bundle == null) {
            c = new c(0L, Long.MIN_VALUE, false, false, false);
        }
        else {
            c = ab.c.f.fromBundle(bundle);
        }
        return new ab(s, c, null, a, a2);
    }
    
    private static String a(final int i) {
        return Integer.toString(i, 36);
    }
    
    public b a() {
        return new b(this);
    }
    
    @Override
    public boolean equals(@Nullable final Object o) {
        boolean b = true;
        if (this == o) {
            return true;
        }
        if (!(o instanceof ab)) {
            return false;
        }
        final ab ab = (ab)o;
        if (!ai.a((Object)this.b, (Object)ab.b) || !this.f.equals(ab.f) || !ai.a((Object)this.c, (Object)ab.c) || !ai.a((Object)this.d, (Object)ab.d) || !ai.a((Object)this.e, (Object)ab.e)) {
            b = false;
        }
        return b;
    }
    
    @Override
    public int hashCode() {
        final int hashCode = this.b.hashCode();
        final f c = this.c;
        int hashCode2;
        if (c != null) {
            hashCode2 = c.hashCode();
        }
        else {
            hashCode2 = 0;
        }
        return (((hashCode * 31 + hashCode2) * 31 + this.d.hashCode()) * 31 + this.f.hashCode()) * 31 + this.e.hashCode();
    }
    
    public static final class a
    {
        public final Uri a;
        @Nullable
        public final Object b;
        
        @Override
        public boolean equals(@Nullable final Object o) {
            boolean b = true;
            if (this == o) {
                return true;
            }
            if (!(o instanceof a)) {
                return false;
            }
            final a a = (a)o;
            if (!this.a.equals((Object)a.a) || !ai.a(this.b, a.b)) {
                b = false;
            }
            return b;
        }
        
        @Override
        public int hashCode() {
            final int hashCode = this.a.hashCode();
            final Object b = this.b;
            int hashCode2;
            if (b != null) {
                hashCode2 = b.hashCode();
            }
            else {
                hashCode2 = 0;
            }
            return hashCode * 31 + hashCode2;
        }
    }
    
    public static final class b
    {
        @Nullable
        private String a;
        @Nullable
        private Uri b;
        @Nullable
        private String c;
        private long d;
        private long e;
        private boolean f;
        private boolean g;
        private boolean h;
        private d.a i;
        private List<Object> j;
        @Nullable
        private String k;
        private List<Object> l;
        @Nullable
        private ab.a m;
        @Nullable
        private Object n;
        @Nullable
        private ac o;
        private e.a p;
        
        public b() {
            this.e = Long.MIN_VALUE;
            this.i = new d.a();
            this.j = Collections.emptyList();
            this.l = Collections.emptyList();
            this.p = new e.a();
        }
        
        private b(final ab ab) {
            this();
            final c f = ab.f;
            this.e = f.b;
            this.f = f.c;
            this.g = f.d;
            this.d = f.a;
            this.h = f.e;
            this.a = ab.b;
            this.o = ab.e;
            this.p = ab.d.a();
            final f c = ab.c;
            if (c != null) {
                this.k = c.f;
                this.c = c.b;
                this.b = c.a;
                this.j = c.e;
                this.l = c.g;
                this.n = c.h;
                final d c2 = c.c;
                Object b;
                if (c2 != null) {
                    b = c2.b();
                }
                else {
                    b = new d.a();
                }
                this.i = (d.a)b;
                this.m = c.d;
            }
        }
        
        public b a(@Nullable final Uri b) {
            this.b = b;
            return this;
        }
        
        public b a(@Nullable final Object n) {
            this.n = n;
            return this;
        }
        
        public b a(final String s) {
            this.a = (String)com.applovin.exoplayer2.l.a.b((Object)s);
            return this;
        }
        
        public ab a() {
            com.applovin.exoplayer2.l.a.b(this.i.b == null || this.i.a != null);
            final Uri b = this.b;
            Object a = null;
            f f;
            if (b != null) {
                final String c = this.c;
                if (this.i.a != null) {
                    a = this.i.a();
                }
                f = new f(b, c, (d)a, this.m, (List)this.j, this.k, (List)this.l, this.n);
            }
            else {
                f = null;
            }
            String a2 = this.a;
            if (a2 == null) {
                a2 = "";
            }
            final c c2 = new c(this.d, this.e, this.f, this.g, this.h);
            final e a3 = this.p.a();
            ac ac = this.o;
            if (ac == null) {
                ac = com.applovin.exoplayer2.ac.a;
            }
            return new ab(a2, c2, f, a3, ac, null);
        }
        
        public b b(@Nullable final String k) {
            this.k = k;
            return this;
        }
    }
    
    public static final class c implements g
    {
        public static final g.a<c> f;
        public final long a;
        public final long b;
        public final boolean c;
        public final boolean d;
        public final boolean e;
        
        static {
            f = new \u3007o00\u3007\u3007Oo();
        }
        
        private c(final long a, final long b, final boolean c, final boolean d, final boolean e) {
            this.a = a;
            this.b = b;
            this.c = c;
            this.d = d;
            this.e = e;
        }
        
        private static String a(final int i) {
            return Integer.toString(i, 36);
        }
        
        @Override
        public boolean equals(@Nullable final Object o) {
            boolean b = true;
            if (this == o) {
                return true;
            }
            if (!(o instanceof c)) {
                return false;
            }
            final c c = (c)o;
            if (this.a != c.a || this.b != c.b || this.c != c.c || this.d != c.d || this.e != c.e) {
                b = false;
            }
            return b;
        }
        
        @Override
        public int hashCode() {
            final long a = this.a;
            final int n = (int)(a ^ a >>> 32);
            final long b = this.b;
            return (((n * 31 + (int)(b ^ b >>> 32)) * 31 + (this.c ? 1 : 0)) * 31 + (this.d ? 1 : 0)) * 31 + (this.e ? 1 : 0);
        }
    }
    
    public static final class d
    {
        public final UUID a;
        @Nullable
        public final Uri b;
        public final u<String, String> c;
        public final boolean d;
        public final boolean e;
        public final boolean f;
        public final s<Integer> g;
        @Nullable
        private final byte[] h;
        
        private d(final a a) {
            com.applovin.exoplayer2.l.a.b(!a.f || a.b != null);
            this.a = (UUID)com.applovin.exoplayer2.l.a.b((Object)a.a);
            this.b = a.b;
            this.c = a.c;
            this.d = a.d;
            this.f = a.f;
            this.e = a.e;
            this.g = a.g;
            byte[] copy;
            if (a.h != null) {
                copy = Arrays.copyOf(a.h, a.h.length);
            }
            else {
                copy = null;
            }
            this.h = copy;
        }
        
        @Nullable
        public byte[] a() {
            final byte[] h = this.h;
            byte[] copy;
            if (h != null) {
                copy = Arrays.copyOf(h, h.length);
            }
            else {
                copy = null;
            }
            return copy;
        }
        
        public a b() {
            return new a(this);
        }
        
        @Override
        public boolean equals(@Nullable final Object o) {
            boolean b = true;
            if (this == o) {
                return true;
            }
            if (!(o instanceof d)) {
                return false;
            }
            final d d = (d)o;
            if (!this.a.equals(d.a) || !ai.a((Object)this.b, (Object)d.b) || !ai.a((Object)this.c, (Object)d.c) || this.d != d.d || this.f != d.f || this.e != d.e || !this.g.equals(d.g) || !Arrays.equals(this.h, d.h)) {
                b = false;
            }
            return b;
        }
        
        @Override
        public int hashCode() {
            final int hashCode = this.a.hashCode();
            final Uri b = this.b;
            int hashCode2;
            if (b != null) {
                hashCode2 = b.hashCode();
            }
            else {
                hashCode2 = 0;
            }
            return ((((((hashCode * 31 + hashCode2) * 31 + this.c.hashCode()) * 31 + (this.d ? 1 : 0)) * 31 + (this.f ? 1 : 0)) * 31 + (this.e ? 1 : 0)) * 31 + this.g.hashCode()) * 31 + Arrays.hashCode(this.h);
        }
        
        public static final class a
        {
            @Nullable
            private UUID a;
            @Nullable
            private Uri b;
            private u<String, String> c;
            private boolean d;
            private boolean e;
            private boolean f;
            private s<Integer> g;
            @Nullable
            private byte[] h;
            
            @Deprecated
            private a() {
                this.c = u.a();
                this.g = s.g();
            }
            
            private a(final d d) {
                this.a = d.a;
                this.b = d.b;
                this.c = d.c;
                this.d = d.d;
                this.e = d.e;
                this.f = d.f;
                this.g = d.g;
                this.h = d.h;
            }
            
            public d a() {
                return new d(this);
            }
        }
    }
    
    public static final class e implements g
    {
        public static final e a;
        public static final g.a<e> g;
        public final long b;
        public final long c;
        public final long d;
        public final float e;
        public final float f;
        
        static {
            a = new a().a();
            g = new \u3007o\u3007();
        }
        
        @Deprecated
        public e(final long b, final long c, final long d, final float e, final float f) {
            this.b = b;
            this.c = c;
            this.d = d;
            this.e = e;
            this.f = f;
        }
        
        private e(final a a) {
            this(a.a, a.b, a.c, a.d, a.e);
        }
        
        private static String a(final int i) {
            return Integer.toString(i, 36);
        }
        
        public a a() {
            return new a(this);
        }
        
        @Override
        public boolean equals(@Nullable final Object o) {
            boolean b = true;
            if (this == o) {
                return true;
            }
            if (!(o instanceof e)) {
                return false;
            }
            final e e = (e)o;
            if (this.b != e.b || this.c != e.c || this.d != e.d || this.e != e.e || this.f != e.f) {
                b = false;
            }
            return b;
        }
        
        @Override
        public int hashCode() {
            final long b = this.b;
            final int n = (int)(b ^ b >>> 32);
            final long c = this.c;
            final int n2 = (int)(c ^ c >>> 32);
            final long d = this.d;
            final int n3 = (int)(d ^ d >>> 32);
            final float e = this.e;
            int floatToIntBits = 0;
            int floatToIntBits2;
            if (e != 0.0f) {
                floatToIntBits2 = Float.floatToIntBits(e);
            }
            else {
                floatToIntBits2 = 0;
            }
            final float f = this.f;
            if (f != 0.0f) {
                floatToIntBits = Float.floatToIntBits(f);
            }
            return (((n * 31 + n2) * 31 + n3) * 31 + floatToIntBits2) * 31 + floatToIntBits;
        }
        
        public static final class a
        {
            private long a;
            private long b;
            private long c;
            private float d;
            private float e;
            
            public a() {
                this.a = -9223372036854775807L;
                this.b = -9223372036854775807L;
                this.c = -9223372036854775807L;
                this.d = -3.4028235E38f;
                this.e = -3.4028235E38f;
            }
            
            private a(final e e) {
                this.a = e.b;
                this.b = e.c;
                this.c = e.d;
                this.d = e.e;
                this.e = e.f;
            }
            
            public e a() {
                return new e(this);
            }
        }
    }
    
    public static final class f
    {
        public final Uri a;
        @Nullable
        public final String b;
        @Nullable
        public final d c;
        @Nullable
        public final ab.a d;
        public final List<Object> e;
        @Nullable
        public final String f;
        public final List<Object> g;
        @Nullable
        public final Object h;
        
        private f(final Uri a, @Nullable final String b, @Nullable final d c, @Nullable final ab.a d, final List<Object> e, @Nullable final String f, final List<Object> g, @Nullable final Object h) {
            this.a = a;
            this.b = b;
            this.c = c;
            this.d = d;
            this.e = e;
            this.f = f;
            this.g = g;
            this.h = h;
        }
        
        @Override
        public boolean equals(@Nullable final Object o) {
            boolean b = true;
            if (this == o) {
                return true;
            }
            if (!(o instanceof f)) {
                return false;
            }
            final f f = (f)o;
            if (!this.a.equals((Object)f.a) || !ai.a((Object)this.b, (Object)f.b) || !ai.a((Object)this.c, (Object)f.c) || !ai.a((Object)this.d, (Object)f.d) || !this.e.equals(f.e) || !ai.a((Object)this.f, (Object)f.f) || !this.g.equals(f.g) || !ai.a(this.h, f.h)) {
                b = false;
            }
            return b;
        }
        
        @Override
        public int hashCode() {
            final int hashCode = this.a.hashCode();
            final String b = this.b;
            int hashCode2 = 0;
            int hashCode3;
            if (b == null) {
                hashCode3 = 0;
            }
            else {
                hashCode3 = b.hashCode();
            }
            final d c = this.c;
            int hashCode4;
            if (c == null) {
                hashCode4 = 0;
            }
            else {
                hashCode4 = c.hashCode();
            }
            final ab.a d = this.d;
            int hashCode5;
            if (d == null) {
                hashCode5 = 0;
            }
            else {
                hashCode5 = d.hashCode();
            }
            final int hashCode6 = this.e.hashCode();
            final String f = this.f;
            int hashCode7;
            if (f == null) {
                hashCode7 = 0;
            }
            else {
                hashCode7 = f.hashCode();
            }
            final int hashCode8 = this.g.hashCode();
            final Object h = this.h;
            if (h != null) {
                hashCode2 = h.hashCode();
            }
            return ((((((hashCode * 31 + hashCode3) * 31 + hashCode4) * 31 + hashCode5) * 31 + hashCode6) * 31 + hashCode7) * 31 + hashCode8) * 31 + hashCode2;
        }
    }
}

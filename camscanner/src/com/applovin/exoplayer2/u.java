// 
// Decompiled by Procyon v0.6.0
// 

package com.applovin.exoplayer2;

public final class u extends RuntimeException
{
    public final int a;
    
    public u(final int a) {
        super(a(a));
        this.a = a;
    }
    
    private static String a(final int n) {
        if (n == 1) {
            return "Player release timed out.";
        }
        if (n == 2) {
            return "Setting foreground mode timed out.";
        }
        if (n != 3) {
            return "Undefined timeout.";
        }
        return "Detaching surface timed out.";
    }
}

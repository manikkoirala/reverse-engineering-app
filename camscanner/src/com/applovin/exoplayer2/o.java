// 
// Decompiled by Procyon v0.6.0
// 

package com.applovin.exoplayer2;

import android.os.BaseBundle;
import androidx.annotation.Nullable;
import android.os.Bundle;

public final class o implements g
{
    public static final o a;
    public static final a<o> e;
    public final int b;
    public final int c;
    public final int d;
    
    static {
        a = new o(0, 0, 0);
        e = new O8\u3007o();
    }
    
    public o(final int b, final int c, final int d) {
        this.b = b;
        this.c = c;
        this.d = d;
    }
    
    private static String a(final int i) {
        return Integer.toString(i, 36);
    }
    
    @Override
    public boolean equals(@Nullable final Object o) {
        boolean b = true;
        if (this == o) {
            return true;
        }
        if (!(o instanceof o)) {
            return false;
        }
        final o o2 = (o)o;
        if (this.b != o2.b || this.c != o2.c || this.d != o2.d) {
            b = false;
        }
        return b;
    }
    
    @Override
    public int hashCode() {
        return ((527 + this.b) * 31 + this.c) * 31 + this.d;
    }
}

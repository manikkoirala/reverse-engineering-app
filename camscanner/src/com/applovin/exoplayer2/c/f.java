// 
// Decompiled by Procyon v0.6.0
// 

package com.applovin.exoplayer2.c;

import androidx.annotation.Nullable;

public class f extends Exception
{
    public f(final String message) {
        super(message);
    }
    
    public f(final String message, @Nullable final Throwable cause) {
        super(message, cause);
    }
    
    public f(@Nullable final Throwable cause) {
        super(cause);
    }
}

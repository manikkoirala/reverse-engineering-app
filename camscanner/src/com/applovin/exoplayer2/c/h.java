// 
// Decompiled by Procyon v0.6.0
// 

package com.applovin.exoplayer2.c;

import androidx.annotation.Nullable;
import com.applovin.exoplayer2.l.a;
import com.applovin.exoplayer2.v;

public final class h
{
    public final String a;
    public final v b;
    public final v c;
    public final int d;
    public final int e;
    
    public h(final String s, final v v, final v v2, final int d, final int e) {
        com.applovin.exoplayer2.l.a.a(d == 0 || e == 0);
        this.a = com.applovin.exoplayer2.l.a.a(s);
        this.b = (v)com.applovin.exoplayer2.l.a.b((Object)v);
        this.c = (v)com.applovin.exoplayer2.l.a.b((Object)v2);
        this.d = d;
        this.e = e;
    }
    
    @Override
    public boolean equals(@Nullable final Object o) {
        boolean b = true;
        if (this == o) {
            return true;
        }
        if (o != null && h.class == o.getClass()) {
            final h h = (h)o;
            if (this.d != h.d || this.e != h.e || !this.a.equals(h.a) || !this.b.equals(h.b) || !this.c.equals(h.c)) {
                b = false;
            }
            return b;
        }
        return false;
    }
    
    @Override
    public int hashCode() {
        return ((((527 + this.d) * 31 + this.e) * 31 + this.a.hashCode()) * 31 + this.b.hashCode()) * 31 + this.c.hashCode();
    }
}

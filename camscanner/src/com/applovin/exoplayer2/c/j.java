// 
// Decompiled by Procyon v0.6.0
// 

package com.applovin.exoplayer2.c;

import androidx.annotation.CallSuper;
import com.applovin.exoplayer2.l.a;
import androidx.annotation.Nullable;
import java.util.ArrayDeque;

public abstract class j<I extends g, O extends i, E extends f> implements d<I, O, E>
{
    private final Thread a;
    private final Object b;
    private final ArrayDeque<I> c;
    private final ArrayDeque<O> d;
    private final I[] e;
    private final O[] f;
    private int g;
    private int h;
    @Nullable
    private I i;
    @Nullable
    private E j;
    private boolean k;
    private boolean l;
    private int m;
    
    protected j(final I[] e, final O[] f) {
        this.b = new Object();
        this.c = new ArrayDeque<I>();
        this.d = new ArrayDeque<O>();
        this.e = e;
        this.g = e.length;
        final int n = 0;
        for (int i = 0; i < this.g; ++i) {
            this.e[i] = this.g();
        }
        this.f = f;
        this.h = f.length;
        for (int j = n; j < this.h; ++j) {
            this.f[j] = this.h();
        }
        (this.a = new Thread(this, "ExoPlayer:SimpleDecoder") {
            final j a;
            
            @Override
            public void run() {
                this.a.k();
            }
        }).start();
    }
    
    private void b(final I n) {
        n.a();
        this.e[this.g++] = n;
    }
    
    private void b(final O o) {
        o.a();
        this.f[this.h++] = o;
    }
    
    private void i() throws E, f {
        final f j = this.j;
        if (j == null) {
            return;
        }
        throw j;
    }
    
    private void j() {
        if (this.m()) {
            this.b.notify();
        }
    }
    
    private void k() {
        try {
            while (this.l()) {}
        }
        catch (final InterruptedException cause) {
            throw new IllegalStateException(cause);
        }
    }
    
    private boolean l() throws InterruptedException {
        Object j = this.b;
        synchronized (j) {
            while (!this.l && !this.m()) {
                this.b.wait();
            }
            if (this.l) {
                return false;
            }
            final g g = this.c.removeFirst();
            final O[] f = this.f;
            final int h = this.h - 1;
            this.h = h;
            final i e = f[h];
            final boolean k = this.k;
            this.k = false;
            monitorexit(j);
            if (g.c()) {
                e.b(4);
            }
            else {
                if (g.b()) {
                    e.b(Integer.MIN_VALUE);
                }
                try {
                    j = this.a((I)g, (O)e, k);
                }
                catch (OutOfMemoryError j) {
                    j = this.a((Throwable)j);
                }
                catch (RuntimeException j) {
                    j = this.a((Throwable)j);
                }
                if (j != null) {
                    synchronized (this.b) {
                        this.j = (E)j;
                        return false;
                    }
                }
            }
            synchronized (this.b) {
                if (this.k) {
                    e.f();
                }
                else if (e.b()) {
                    ++this.m;
                    e.f();
                }
                else {
                    e.b = this.m;
                    this.m = 0;
                    this.d.addLast((O)e);
                }
                this.b((I)g);
                return true;
            }
        }
    }
    
    private boolean m() {
        return !this.c.isEmpty() && this.h > 0;
    }
    
    @Nullable
    protected abstract E a(final I p0, final O p1, final boolean p2);
    
    protected abstract E a(final Throwable p0);
    
    protected final void a(final int n) {
        final int g = this.g;
        final int length = this.e.length;
        int i = 0;
        com.applovin.exoplayer2.l.a.b(g == length);
        for (I[] e = this.e; i < e.length; ++i) {
            e[i].f(n);
        }
    }
    
    @Override
    public final void a(final I e) throws E, f {
        synchronized (this.b) {
            this.i();
            com.applovin.exoplayer2.l.a.a(e == this.i);
            this.c.addLast(e);
            this.j();
            this.i = null;
        }
    }
    
    @CallSuper
    protected void a(final O o) {
        synchronized (this.b) {
            this.b(o);
            this.j();
        }
    }
    
    @Override
    public final void c() {
        synchronized (this.b) {
            this.k = true;
            this.m = 0;
            final g i = this.i;
            if (i != null) {
                this.b((I)i);
                this.i = null;
            }
            while (!this.c.isEmpty()) {
                this.b(this.c.removeFirst());
            }
            while (!this.d.isEmpty()) {
                this.d.removeFirst().f();
            }
        }
    }
    
    @CallSuper
    @Override
    public void d() {
        synchronized (this.b) {
            this.l = true;
            this.b.notify();
            monitorexit(this.b);
            try {
                this.a.join();
            }
            catch (final InterruptedException ex) {
                Thread.currentThread().interrupt();
            }
        }
    }
    
    @Nullable
    public final I e() throws E, f {
        synchronized (this.b) {
            this.i();
            com.applovin.exoplayer2.l.a.b(this.i == null);
            int g = this.g;
            g i;
            if (g == 0) {
                i = null;
            }
            else {
                final I[] e = this.e;
                --g;
                this.g = g;
                i = e[g];
            }
            return this.i = (I)i;
        }
    }
    
    @Nullable
    public final O f() throws E, f {
        synchronized (this.b) {
            this.i();
            if (this.d.isEmpty()) {
                return null;
            }
            return this.d.removeFirst();
        }
    }
    
    protected abstract I g();
    
    protected abstract O h();
}

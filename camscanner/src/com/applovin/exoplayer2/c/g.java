// 
// Decompiled by Procyon v0.6.0
// 

package com.applovin.exoplayer2.c;

import androidx.annotation.Nullable;
import java.nio.ByteBuffer;

public class g extends com.applovin.exoplayer2.c.a
{
    public final c a;
    @Nullable
    public ByteBuffer b;
    public boolean c;
    public long d;
    @Nullable
    public ByteBuffer e;
    private final int f;
    private final int g;
    
    public g(final int n) {
        this(n, 0);
    }
    
    public g(final int f, final int g) {
        this.a = new c();
        this.f = f;
        this.g = g;
    }
    
    public static g f() {
        return new g(0);
    }
    
    private ByteBuffer g(final int n) {
        final int f = this.f;
        if (f == 1) {
            return ByteBuffer.allocate(n);
        }
        if (f == 2) {
            return ByteBuffer.allocateDirect(n);
        }
        final ByteBuffer b = this.b;
        int capacity;
        if (b == null) {
            capacity = 0;
        }
        else {
            capacity = b.capacity();
        }
        throw new a(capacity, n);
    }
    
    @Override
    public void a() {
        super.a();
        final ByteBuffer b = this.b;
        if (b != null) {
            b.clear();
        }
        final ByteBuffer e = this.e;
        if (e != null) {
            e.clear();
        }
        this.c = false;
    }
    
    public void e(final int capacity) {
        final ByteBuffer e = this.e;
        if (e != null && e.capacity() >= capacity) {
            this.e.clear();
        }
        else {
            this.e = ByteBuffer.allocate(capacity);
        }
    }
    
    public void f(int position) {
        final int n = position + this.g;
        final ByteBuffer b = this.b;
        if (b == null) {
            this.b = this.g(n);
            return;
        }
        final int capacity = b.capacity();
        position = b.position();
        final int n2 = n + position;
        if (capacity >= n2) {
            this.b = b;
            return;
        }
        final ByteBuffer g = this.g(n2);
        g.order(b.order());
        if (position > 0) {
            b.flip();
            g.put(b);
        }
        this.b = g;
    }
    
    public final boolean g() {
        return this.d(1073741824);
    }
    
    public final void h() {
        final ByteBuffer b = this.b;
        if (b != null) {
            b.flip();
        }
        final ByteBuffer e = this.e;
        if (e != null) {
            e.flip();
        }
    }
    
    public static final class a extends IllegalStateException
    {
        public final int a;
        public final int b;
        
        public a(final int n, final int n2) {
            final StringBuilder sb = new StringBuilder();
            sb.append("Buffer too small (");
            sb.append(n);
            sb.append(" < ");
            sb.append(n2);
            sb.append(")");
            super(sb.toString());
            this.a = n;
            this.b = n2;
        }
    }
}

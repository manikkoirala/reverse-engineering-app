// 
// Decompiled by Procyon v0.6.0
// 

package com.applovin.exoplayer2.c;

import android.media.MediaCodec$CryptoInfo$Pattern;
import androidx.annotation.RequiresApi;
import com.applovin.exoplayer2.l.a;
import com.applovin.exoplayer2.l.ai;
import android.media.MediaCodec$CryptoInfo;
import androidx.annotation.Nullable;

public final class c
{
    @Nullable
    public byte[] a;
    @Nullable
    public byte[] b;
    public int c;
    @Nullable
    public int[] d;
    @Nullable
    public int[] e;
    public int f;
    public int g;
    public int h;
    private final MediaCodec$CryptoInfo i;
    @Nullable
    private final a j;
    
    public c() {
        final MediaCodec$CryptoInfo i = new MediaCodec$CryptoInfo();
        this.i = i;
        final int a = ai.a;
        a j = null;
        if (a >= 24) {
            j = new a(i);
        }
        this.j = j;
    }
    
    public MediaCodec$CryptoInfo a() {
        return this.i;
    }
    
    public void a(final int n) {
        if (n == 0) {
            return;
        }
        if (this.d == null) {
            final int[] array = { 0 };
            this.d = array;
            this.i.numBytesOfClearData = array;
        }
        final int[] d = this.d;
        d[0] += n;
    }
    
    public void a(final int n, final int[] array, final int[] array2, final byte[] array3, final byte[] array4, final int n2, final int g, final int h) {
        this.f = n;
        this.d = array;
        this.e = array2;
        this.b = array3;
        this.a = array4;
        this.c = n2;
        this.g = g;
        this.h = h;
        final MediaCodec$CryptoInfo i = this.i;
        i.numSubSamples = n;
        i.numBytesOfClearData = array;
        i.numBytesOfEncryptedData = array2;
        i.key = array3;
        i.iv = array4;
        i.mode = n2;
        if (ai.a >= 24) {
            ((a)com.applovin.exoplayer2.l.a.b((Object)this.j)).a(g, h);
        }
    }
    
    @RequiresApi(24)
    private static final class a
    {
        private final MediaCodec$CryptoInfo a;
        private final MediaCodec$CryptoInfo$Pattern b;
        
        private a(final MediaCodec$CryptoInfo a) {
            this.a = a;
            this.b = new MediaCodec$CryptoInfo$Pattern(0, 0);
        }
        
        private void a(final int n, final int n2) {
            \u3007080.\u3007080(this.b, n, n2);
            \u3007o00\u3007\u3007Oo.\u3007080(this.a, this.b);
        }
    }
}

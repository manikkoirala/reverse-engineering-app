// 
// Decompiled by Procyon v0.6.0
// 

package com.applovin.exoplayer2.c;

public abstract class a
{
    private int a;
    
    public void a() {
        this.a = 0;
    }
    
    public final void a_(final int a) {
        this.a = a;
    }
    
    public final void b(final int n) {
        this.a |= n;
    }
    
    public final boolean b() {
        return this.d(Integer.MIN_VALUE);
    }
    
    public final void c(final int n) {
        this.a &= ~n;
    }
    
    public final boolean c() {
        return this.d(4);
    }
    
    public final boolean d() {
        return this.d(1);
    }
    
    protected final boolean d(final int n) {
        return (this.a & n) == n;
    }
    
    public final boolean e() {
        return this.d(268435456);
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.applovin.exoplayer2;

import com.applovin.exoplayer2.l.ai;
import androidx.annotation.Nullable;
import com.applovin.exoplayer2.l.a;
import com.applovin.exoplayer2.h.p;

final class ae
{
    public final p.a a;
    public final long b;
    public final long c;
    public final long d;
    public final long e;
    public final boolean f;
    public final boolean g;
    public final boolean h;
    public final boolean i;
    
    ae(final p.a a, final long b, final long c, final long d, final long e, final boolean f, final boolean g, final boolean h, final boolean i) {
        final boolean b2 = false;
        com.applovin.exoplayer2.l.a.a(!i || g);
        com.applovin.exoplayer2.l.a.a(!h || g);
        boolean b3 = false;
        Label_0096: {
            if (f) {
                b3 = b2;
                if (g) {
                    break Label_0096;
                }
                b3 = b2;
                if (h) {
                    break Label_0096;
                }
                b3 = b2;
                if (i) {
                    break Label_0096;
                }
            }
            b3 = true;
        }
        com.applovin.exoplayer2.l.a.a(b3);
        this.a = a;
        this.b = b;
        this.c = c;
        this.d = d;
        this.e = e;
        this.f = f;
        this.g = g;
        this.h = h;
        this.i = i;
    }
    
    public ae a(final long n) {
        ae ae;
        if (n == this.b) {
            ae = this;
        }
        else {
            ae = new ae(this.a, n, this.c, this.d, this.e, this.f, this.g, this.h, this.i);
        }
        return ae;
    }
    
    public ae b(final long n) {
        ae ae;
        if (n == this.c) {
            ae = this;
        }
        else {
            ae = new ae(this.a, this.b, n, this.d, this.e, this.f, this.g, this.h, this.i);
        }
        return ae;
    }
    
    @Override
    public boolean equals(@Nullable final Object o) {
        boolean b = true;
        if (this == o) {
            return true;
        }
        if (o != null && ae.class == o.getClass()) {
            final ae ae = (ae)o;
            if (this.b != ae.b || this.c != ae.c || this.d != ae.d || this.e != ae.e || this.f != ae.f || this.g != ae.g || this.h != ae.h || this.i != ae.i || !ai.a((Object)this.a, (Object)ae.a)) {
                b = false;
            }
            return b;
        }
        return false;
    }
    
    @Override
    public int hashCode() {
        return ((((((((527 + this.a.hashCode()) * 31 + (int)this.b) * 31 + (int)this.c) * 31 + (int)this.d) * 31 + (int)this.e) * 31 + (this.f ? 1 : 0)) * 31 + (this.g ? 1 : 0)) * 31 + (this.h ? 1 : 0)) * 31 + (this.i ? 1 : 0);
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.applovin.exoplayer2;

import android.os.BaseBundle;
import com.applovin.exoplayer2.common.base.Objects;
import androidx.annotation.Nullable;
import com.applovin.exoplayer2.l.a;
import android.os.Bundle;

public final class x extends aq
{
    public static final a<x> a;
    private final boolean c;
    private final boolean d;
    
    static {
        a = new O0O8OO088();
    }
    
    public x() {
        this.c = false;
        this.d = false;
    }
    
    public x(final boolean d) {
        this.c = true;
        this.d = d;
    }
    
    private static x a(final Bundle bundle) {
        com.applovin.exoplayer2.l.a.a(((BaseBundle)bundle).getInt(a(0), -1) == 0);
        x x;
        if (bundle.getBoolean(a(1), false)) {
            x = new x(bundle.getBoolean(a(2), false));
        }
        else {
            x = new x();
        }
        return x;
    }
    
    private static String a(final int i) {
        return Integer.toString(i, 36);
    }
    
    @Override
    public boolean equals(@Nullable final Object o) {
        final boolean b = o instanceof x;
        final boolean b2 = false;
        if (!b) {
            return false;
        }
        final x x = (x)o;
        boolean b3 = b2;
        if (this.d == x.d) {
            b3 = b2;
            if (this.c == x.c) {
                b3 = true;
            }
        }
        return b3;
    }
    
    @Override
    public int hashCode() {
        return Objects.hashCode(this.c, this.d);
    }
}

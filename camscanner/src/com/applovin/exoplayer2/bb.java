// 
// Decompiled by Procyon v0.6.0
// 

package com.applovin.exoplayer2;

import com.applovin.exoplayer2.l.q;
import android.annotation.SuppressLint;
import android.content.Context;
import android.os.PowerManager$WakeLock;
import androidx.annotation.Nullable;
import android.os.PowerManager;

final class bb
{
    @Nullable
    private final PowerManager a;
    @Nullable
    private PowerManager$WakeLock b;
    private boolean c;
    private boolean d;
    
    public bb(final Context context) {
        this.a = (PowerManager)context.getApplicationContext().getSystemService("power");
    }
    
    @SuppressLint({ "WakelockTimeout" })
    private void a() {
        final PowerManager$WakeLock b = this.b;
        if (b == null) {
            return;
        }
        if (this.c && this.d) {
            b.acquire();
        }
        else {
            b.release();
        }
    }
    
    public void a(final boolean c) {
        if (c && this.b == null) {
            final PowerManager a = this.a;
            if (a == null) {
                q.c("WakeLockManager", "PowerManager is null, therefore not creating the WakeLock.");
                return;
            }
            (this.b = a.newWakeLock(1, "ExoPlayer:WakeLockManager")).setReferenceCounted(false);
        }
        this.c = c;
        this.a();
    }
    
    public void b(final boolean d) {
        this.d = d;
        this.a();
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.applovin.exoplayer2;

import android.os.BaseBundle;
import androidx.annotation.CheckResult;
import com.applovin.exoplayer2.l.ai;
import android.text.TextUtils;
import java.io.IOException;
import com.applovin.exoplayer2.l.a;
import com.applovin.exoplayer2.l.c;
import android.os.Bundle;
import android.os.SystemClock;
import com.applovin.exoplayer2.h.o;
import androidx.annotation.Nullable;

public final class p extends ak
{
    public static final a<p> h;
    public final int a;
    @Nullable
    public final String b;
    public final int c;
    @Nullable
    public final v d;
    public final int e;
    @Nullable
    public final o f;
    final boolean g;
    
    static {
        h = new \u300700\u30078();
    }
    
    private p(final int n, final Throwable t, final int n2) {
        this(n, t, null, n2, null, -1, null, 4, false);
    }
    
    private p(final int n, @Nullable final Throwable t, @Nullable final String s, final int n2, @Nullable final String s2, final int n3, @Nullable final v v, final int n4, final boolean b) {
        this(a(n, s, s2, n3, v, n4), t, n2, n, s2, n3, v, n4, null, SystemClock.elapsedRealtime(), b);
    }
    
    private p(final Bundle bundle) {
        super(bundle);
        this.a = ((BaseBundle)bundle).getInt(ak.a(1001), 2);
        this.b = ((BaseBundle)bundle).getString(ak.a(1002));
        this.c = ((BaseBundle)bundle).getInt(ak.a(1003), -1);
        this.d = (v)com.applovin.exoplayer2.l.c.a((a)v.F, bundle.getBundle(ak.a(1004)));
        this.e = ((BaseBundle)bundle).getInt(ak.a(1005), 4);
        this.g = bundle.getBoolean(ak.a(1006), false);
        this.f = null;
    }
    
    private p(final String s, @Nullable final Throwable t, final int n, final int a, @Nullable final String b, final int c, @Nullable final v d, final int e, @Nullable final o f, final long n2, final boolean g) {
        super(s, t, n, n2);
        final boolean b2 = false;
        com.applovin.exoplayer2.l.a.a(!g || a == 1);
        boolean b3 = false;
        Label_0057: {
            if (t == null) {
                b3 = b2;
                if (a != 3) {
                    break Label_0057;
                }
            }
            b3 = true;
        }
        com.applovin.exoplayer2.l.a.a(b3);
        this.a = a;
        this.b = b;
        this.c = c;
        this.d = d;
        this.e = e;
        this.f = f;
        this.g = g;
    }
    
    public static p a(final IOException ex, final int n) {
        return new p(0, ex, n);
    }
    
    @Deprecated
    public static p a(final RuntimeException ex) {
        return a(ex, 1000);
    }
    
    public static p a(final RuntimeException ex, final int n) {
        return new p(2, ex, n);
    }
    
    public static p a(final Throwable t, final String s, final int n, @Nullable final v v, int n2, final boolean b, final int n3) {
        if (v == null) {
            n2 = 4;
        }
        return new p(1, t, null, n3, s, n, v, n2, b);
    }
    
    private static String a(final int n, @Nullable final String str, @Nullable String string, final int i, @Nullable final v obj, final int n2) {
        if (n != 0) {
            if (n != 1) {
                if (n != 3) {
                    string = "Unexpected runtime error";
                }
                else {
                    string = "Remote error";
                }
            }
            else {
                final StringBuilder sb = new StringBuilder();
                sb.append(string);
                sb.append(" error, index=");
                sb.append(i);
                sb.append(", format=");
                sb.append(obj);
                sb.append(", format_supported=");
                sb.append(com.applovin.exoplayer2.h.a(n2));
                string = sb.toString();
            }
        }
        else {
            string = "Source error";
        }
        String string2 = string;
        if (!TextUtils.isEmpty((CharSequence)str)) {
            final StringBuilder sb2 = new StringBuilder();
            sb2.append(string);
            sb2.append(": ");
            sb2.append(str);
            string2 = sb2.toString();
        }
        return string2;
    }
    
    @CheckResult
    p a(@Nullable final o o) {
        return new p((String)ai.a((Object)this.getMessage()), this.getCause(), super.i, this.a, this.b, this.c, this.d, this.e, o, super.j, this.g);
    }
}

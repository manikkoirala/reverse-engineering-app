// 
// Decompiled by Procyon v0.6.0
// 

package com.applovin.exoplayer2;

import android.view.View;
import android.media.MediaFormat;
import com.applovin.exoplayer2.b.O8;
import com.applovin.exoplayer2.e.f;
import com.applovin.exoplayer2.m.k;
import android.view.SurfaceView;
import com.applovin.exoplayer2.h.ad;
import android.graphics.Rect;
import java.util.concurrent.TimeoutException;
import java.util.ArrayList;
import android.graphics.SurfaceTexture;
import java.util.Iterator;
import android.view.SurfaceHolder$Callback;
import android.view.TextureView$SurfaceTextureListener;
import com.applovin.exoplayer2.m.a.i$a;
import android.os.Looper;
import com.applovin.exoplayer2.j.j;
import java.util.Collections;
import com.applovin.exoplayer2.l.ai;
import com.applovin.exoplayer2.i.l;
import com.applovin.exoplayer2.m.n;
import android.os.Handler;
import android.view.TextureView;
import com.applovin.exoplayer2.m.a.i;
import android.view.SurfaceHolder;
import android.view.Surface;
import android.media.AudioTrack;
import java.util.concurrent.CopyOnWriteArraySet;
import android.content.Context;
import com.applovin.exoplayer2.l.g;
import com.applovin.exoplayer2.l.aa;
import com.applovin.exoplayer2.i.a;
import java.util.List;
import androidx.annotation.Nullable;
import com.applovin.exoplayer2.c.e;

public class aw extends d implements q
{
    private int A;
    private int B;
    @Nullable
    private com.applovin.exoplayer2.c.e C;
    @Nullable
    private com.applovin.exoplayer2.c.e D;
    private int E;
    private com.applovin.exoplayer2.b.d F;
    private float G;
    private boolean H;
    private List<com.applovin.exoplayer2.i.a> I;
    private boolean J;
    private boolean K;
    @Nullable
    private aa L;
    private boolean M;
    private boolean N;
    private o O;
    private com.applovin.exoplayer2.m.o P;
    protected final ar[] b;
    private final g c;
    private final Context d;
    private final r e;
    private final b f;
    private final c g;
    private final CopyOnWriteArraySet<an.d> h;
    private final com.applovin.exoplayer2.a.a i;
    private final com.applovin.exoplayer2.b j;
    private final com.applovin.exoplayer2.c k;
    private final ay l;
    private final bb m;
    private final bc n;
    private final long o;
    @Nullable
    private v p;
    @Nullable
    private v q;
    @Nullable
    private AudioTrack r;
    @Nullable
    private Object s;
    @Nullable
    private Surface t;
    @Nullable
    private SurfaceHolder u;
    @Nullable
    private i v;
    private boolean w;
    @Nullable
    private TextureView x;
    private int y;
    private int z;
    
    protected aw(final a a) {
        final g c = new g();
        this.c = c;
        try {
            final Context applicationContext = a.a.getApplicationContext();
            this.d = applicationContext;
            final com.applovin.exoplayer2.a.a b = a.i;
            this.i = b;
            this.L = a.k;
            this.F = a.l;
            this.y = a.q;
            this.z = a.r;
            this.H = a.p;
            this.o = a.y;
            final b f = new b();
            this.f = f;
            final c g = new c();
            this.g = g;
            this.h = new CopyOnWriteArraySet<an.d>();
            final Handler handler = new Handler(a.j);
            final ar[] a2 = a.b.a(handler, (n)f, f, (l)f, f);
            this.b = a2;
            this.G = 1.0f;
            if (ai.a < 21) {
                this.E = this.d(0);
            }
            else {
                this.E = com.applovin.exoplayer2.h.a(applicationContext);
            }
            this.I = Collections.emptyList();
            this.J = true;
            final an.a a3 = new an.a.a().a(20, 21, 22, 23, 24, 25, 26, 27).a();
            final j k = a.e;
            final com.applovin.exoplayer2.h.r l = a.f;
            final com.applovin.exoplayer2.aa m = a.g;
            final com.applovin.exoplayer2.k.d n = a.h;
            final boolean o = a.s;
            final av p = a.t;
            final long q = a.u;
            final long r = a.v;
            final z s = a.w;
            final long t = a.x;
            final boolean u = a.z;
            final com.applovin.exoplayer2.l.d v = a.c;
            final Looper i = a.j;
            try {
                final r e = new r(a2, k, l, m, n, b, o, p, q, r, s, t, u, v, i, this, a3);
                try {
                    (this.e = e).a((an.b)f);
                    e.a((q.a)f);
                    if (a.d > 0L) {
                        e.b(a.d);
                    }
                    (this.j = new com.applovin.exoplayer2.b(a.a, handler, (com.applovin.exoplayer2.b.b)f)).a(a.o);
                    final com.applovin.exoplayer2.c j = new com.applovin.exoplayer2.c(a.a, handler, (com.applovin.exoplayer2.c.b)f);
                    this.k = j;
                    com.applovin.exoplayer2.b.d f2;
                    if (a.m) {
                        f2 = this.F;
                    }
                    else {
                        f2 = null;
                    }
                    j.a(f2);
                    final ay l2 = new ay(a.a, handler, (ay.a)f);
                    (this.l = l2).a(ai.g(this.F.d));
                    (this.m = new bb(a.a)).a(a.n != 0);
                    (this.n = new bc(a.a)).a(a.n == 2);
                    this.O = b(l2);
                    this.P = com.applovin.exoplayer2.m.o.a;
                    this.a(1, 10, this.E);
                    this.a(2, 10, this.E);
                    this.a(1, 3, this.F);
                    this.a(2, 4, this.y);
                    this.a(2, 5, this.z);
                    this.a(1, 9, this.H);
                    this.a(2, 7, g);
                    this.a(6, 8, g);
                    c.a();
                    return;
                }
                finally {}
            }
            finally {}
        }
        finally {}
        this.c.a();
    }
    
    private void U() {
        if (this.v != null) {
            this.e.a(this.g).a(10000).a(null).i();
            this.v.b((i$a)this.f);
            this.v = null;
        }
        final TextureView x = this.x;
        if (x != null) {
            if (x.getSurfaceTextureListener() != this.f) {
                com.applovin.exoplayer2.l.q.c("SimpleExoPlayer", "SurfaceTextureListener already unset or replaced.");
            }
            else {
                this.x.setSurfaceTextureListener((TextureView$SurfaceTextureListener)null);
            }
            this.x = null;
        }
        final SurfaceHolder u = this.u;
        if (u != null) {
            u.removeCallback((SurfaceHolder$Callback)this.f);
            this.u = null;
        }
    }
    
    private void W() {
        this.a(1, 2, this.G * this.k.a());
    }
    
    private void X() {
        this.i.a_(this.H);
        final Iterator<an.d> iterator = this.h.iterator();
        while (iterator.hasNext()) {
            iterator.next().a_(this.H);
        }
    }
    
    private void Y() {
        final int t = this.t();
        boolean b = true;
        if (t != 1) {
            if (t == 2 || t == 3) {
                final boolean q = this.q();
                final bb m = this.m;
                if (!this.x() || q) {
                    b = false;
                }
                m.b(b);
                this.n.b(this.x());
                return;
            }
            if (t != 4) {
                throw new IllegalStateException();
            }
        }
        this.m.b(false);
        this.n.b(false);
    }
    
    private void Z() {
        this.c.d();
        if (Thread.currentThread() != this.r().getThread()) {
            final String a = ai.a("Player is accessed on the wrong thread.\nCurrent thread: '%s'\nExpected thread: '%s'\nSee https://exoplayer.dev/issues/player-accessed-on-wrong-thread", new Object[] { Thread.currentThread().getName(), this.r().getThread().getName() });
            if (this.J) {
                throw new IllegalStateException(a);
            }
            Throwable t;
            if (this.K) {
                t = null;
            }
            else {
                t = new IllegalStateException();
            }
            com.applovin.exoplayer2.l.q.b("SimpleExoPlayer", a, t);
            this.K = true;
        }
    }
    
    private void a(final int a, final int b) {
        if (a != this.A || b != this.B) {
            this.A = a;
            this.B = b;
            this.i.a(a, b);
            final Iterator<an.d> iterator = this.h.iterator();
            while (iterator.hasNext()) {
                iterator.next().a(a, b);
            }
        }
    }
    
    private void a(final int n, final int n2, @Nullable final Object o) {
        for (final ar ar : this.b) {
            if (ar.a() == n) {
                this.e.a(ar).a(n2).a(o).i();
            }
        }
    }
    
    private void a(final SurfaceTexture surfaceTexture) {
        final Surface t = new Surface(surfaceTexture);
        this.a(t);
        this.t = t;
    }
    
    private void a(@Nullable final Object s) {
        final ArrayList list = new ArrayList();
        final ar[] b = this.b;
        final int length = b.length;
        int n = 0;
        int n2;
        while (true) {
            n2 = 1;
            if (n >= length) {
                break;
            }
            final ar ar = b[n];
            if (ar.a() == 2) {
                list.add(this.e.a(ar).a(1).a(s).i());
            }
            ++n;
        }
        final Object s2 = this.s;
        int n4;
        if (s2 != null && s2 != s) {
            int n3 = 0;
            Label_0155: {
                try {
                    final Iterator iterator = list.iterator();
                    while (iterator.hasNext()) {
                        ((ao)iterator.next()).a(this.o);
                    }
                }
                catch (final TimeoutException ex) {
                    n3 = n2;
                    break Label_0155;
                }
                catch (final InterruptedException ex2) {
                    Thread.currentThread().interrupt();
                }
                n3 = 0;
            }
            final Object s3 = this.s;
            final Surface t = this.t;
            n4 = n3;
            if (s3 == t) {
                t.release();
                this.t = null;
                n4 = n3;
            }
        }
        else {
            n4 = 0;
        }
        this.s = s;
        if (n4 != 0) {
            this.e.a(false, com.applovin.exoplayer2.p.a(new u(3), 1003));
        }
    }
    
    private void a(final boolean b, final int n, final int n2) {
        final int n3 = 0;
        final boolean b2 = b && n != -1;
        int n4 = n3;
        if (b2) {
            n4 = n3;
            if (n != 1) {
                n4 = 1;
            }
        }
        this.e.a(b2, n4, n2);
    }
    
    private static int b(final boolean b, final int n) {
        int n2 = 1;
        if (b) {
            n2 = n2;
            if (n != 1) {
                n2 = 2;
            }
        }
        return n2;
    }
    
    private static o b(final ay ay) {
        return new o(0, ay.a(), ay.b());
    }
    
    private void c(final SurfaceHolder u) {
        this.w = false;
        (this.u = u).addCallback((SurfaceHolder$Callback)this.f);
        final Surface surface = this.u.getSurface();
        if (surface != null && surface.isValid()) {
            final Rect surfaceFrame = this.u.getSurfaceFrame();
            this.a(surfaceFrame.width(), surfaceFrame.height());
        }
        else {
            this.a(0, 0);
        }
    }
    
    private int d(final int n) {
        final AudioTrack r = this.r;
        if (r != null && r.getAudioSessionId() != n) {
            this.r.release();
            this.r = null;
        }
        if (this.r == null) {
            this.r = new AudioTrack(3, 4000, 4, 2, 2, 0, n);
        }
        return this.r.getAudioSessionId();
    }
    
    @Override
    public long A() {
        this.Z();
        return this.e.A();
    }
    
    @Override
    public long B() {
        this.Z();
        return this.e.B();
    }
    
    @Override
    public long C() {
        this.Z();
        return this.e.C();
    }
    
    @Override
    public am D() {
        this.Z();
        return this.e.D();
    }
    
    public void E() {
        this.Z();
        if (ai.a < 21) {
            final AudioTrack r = this.r;
            if (r != null) {
                r.release();
                this.r = null;
            }
        }
        this.j.a(false);
        this.l.c();
        this.m.b(false);
        this.n.b(false);
        this.k.b();
        this.e.E();
        this.i.c();
        this.U();
        final Surface t = this.t;
        if (t != null) {
            t.release();
            this.t = null;
        }
        if (this.M) {
            ((aa)com.applovin.exoplayer2.l.a.b((Object)this.L)).b(0);
            this.M = false;
        }
        this.I = Collections.emptyList();
        this.N = true;
    }
    
    @Override
    public int F() {
        this.Z();
        return this.e.F();
    }
    
    @Override
    public int G() {
        this.Z();
        return this.e.G();
    }
    
    @Override
    public long H() {
        this.Z();
        return this.e.H();
    }
    
    @Override
    public long I() {
        this.Z();
        return this.e.I();
    }
    
    @Override
    public long J() {
        this.Z();
        return this.e.J();
    }
    
    @Override
    public boolean K() {
        this.Z();
        return this.e.K();
    }
    
    @Override
    public int L() {
        this.Z();
        return this.e.L();
    }
    
    @Override
    public int M() {
        this.Z();
        return this.e.M();
    }
    
    @Override
    public long N() {
        this.Z();
        return this.e.N();
    }
    
    @Override
    public long O() {
        this.Z();
        return this.e.O();
    }
    
    @Override
    public ad P() {
        this.Z();
        return this.e.P();
    }
    
    @Override
    public com.applovin.exoplayer2.j.h Q() {
        this.Z();
        return this.e.Q();
    }
    
    @Override
    public ac R() {
        return this.e.R();
    }
    
    @Override
    public ba S() {
        this.Z();
        return this.e.S();
    }
    
    @Override
    public com.applovin.exoplayer2.m.o T() {
        return this.P;
    }
    
    @Override
    public List<com.applovin.exoplayer2.i.a> V() {
        this.Z();
        return this.I;
    }
    
    public void a(float a) {
        this.Z();
        a = ai.a(a, 0.0f, 1.0f);
        if (this.G == a) {
            return;
        }
        this.G = a;
        this.W();
        this.i.a(a);
        final Iterator<an.d> iterator = this.h.iterator();
        while (iterator.hasNext()) {
            iterator.next().a(a);
        }
    }
    
    @Override
    public void a(final int n, final long n2) {
        this.Z();
        this.i.d();
        this.e.a(n, n2);
    }
    
    public void a(@Nullable final SurfaceHolder u) {
        this.Z();
        if (u == null) {
            this.v();
        }
        else {
            this.U();
            this.w = true;
            (this.u = u).addCallback((SurfaceHolder$Callback)this.f);
            final Surface surface = u.getSurface();
            if (surface != null && surface.isValid()) {
                this.a(surface);
                final Rect surfaceFrame = u.getSurfaceFrame();
                this.a(surfaceFrame.width(), surfaceFrame.height());
            }
            else {
                this.a((Object)null);
                this.a(0, 0);
            }
        }
    }
    
    @Override
    public void a(@Nullable final SurfaceView surfaceView) {
        this.Z();
        if (surfaceView instanceof k) {
            this.U();
            this.a((Object)surfaceView);
            this.c(surfaceView.getHolder());
        }
        else if (surfaceView instanceof i) {
            this.U();
            this.v = (i)surfaceView;
            this.e.a(this.g).a(10000).a(this.v).i();
            this.v.a((i$a)this.f);
            this.a(this.v.getVideoSurface());
            this.c(surfaceView.getHolder());
        }
        else {
            SurfaceHolder holder;
            if (surfaceView == null) {
                holder = null;
            }
            else {
                holder = surfaceView.getHolder();
            }
            this.a(holder);
        }
    }
    
    @Override
    public void a(@Nullable final TextureView x) {
        this.Z();
        if (x == null) {
            this.v();
        }
        else {
            this.U();
            this.x = x;
            if (x.getSurfaceTextureListener() != null) {
                com.applovin.exoplayer2.l.q.c("SimpleExoPlayer", "Replacing existing SurfaceTextureListener.");
            }
            x.setSurfaceTextureListener((TextureView$SurfaceTextureListener)this.f);
            SurfaceTexture surfaceTexture;
            if (x.isAvailable()) {
                surfaceTexture = x.getSurfaceTexture();
            }
            else {
                surfaceTexture = null;
            }
            if (surfaceTexture == null) {
                this.a((Object)null);
                this.a(0, 0);
            }
            else {
                this.a(surfaceTexture);
                this.a(((View)x).getWidth(), ((View)x).getHeight());
            }
        }
    }
    
    @Deprecated
    public void a(final an.b b) {
        com.applovin.exoplayer2.l.a.b((Object)b);
        this.e.a(b);
    }
    
    @Override
    public void a(final an.d e) {
        com.applovin.exoplayer2.l.a.b((Object)e);
        this.h.add(e);
        this.a((an.b)e);
    }
    
    public void a(final com.applovin.exoplayer2.h.p p) {
        this.Z();
        this.e.a(p);
    }
    
    @Override
    public void a(final boolean b) {
        this.Z();
        final int a = this.k.a(b, this.t());
        this.a(b, a, b(b, a));
    }
    
    @Nullable
    @Override
    public p b() {
        this.Z();
        return this.e.v();
    }
    
    public void b(@Nullable final SurfaceHolder surfaceHolder) {
        this.Z();
        if (surfaceHolder != null && surfaceHolder == this.u) {
            this.v();
        }
    }
    
    @Override
    public void b(@Nullable final SurfaceView surfaceView) {
        this.Z();
        SurfaceHolder holder;
        if (surfaceView == null) {
            holder = null;
        }
        else {
            holder = surfaceView.getHolder();
        }
        this.b(holder);
    }
    
    @Override
    public void b(@Nullable final TextureView textureView) {
        this.Z();
        if (textureView != null && textureView == this.x) {
            this.v();
        }
    }
    
    @Deprecated
    public void b(final an.b b) {
        this.e.b(b);
    }
    
    @Override
    public void b(final an.d o) {
        com.applovin.exoplayer2.l.a.b((Object)o);
        this.h.remove(o);
        this.b((an.b)o);
    }
    
    @Override
    public void b(final boolean b) {
        this.Z();
        this.e.b(b);
    }
    
    @Override
    public void c(final int n) {
        this.Z();
        this.e.c(n);
    }
    
    public boolean q() {
        this.Z();
        return this.e.q();
    }
    
    @Override
    public Looper r() {
        return this.e.r();
    }
    
    @Override
    public an.a s() {
        this.Z();
        return this.e.s();
    }
    
    @Override
    public int t() {
        this.Z();
        return this.e.t();
    }
    
    @Override
    public int u() {
        this.Z();
        return this.e.u();
    }
    
    public void v() {
        this.Z();
        this.U();
        this.a((Object)null);
        this.a(0, 0);
    }
    
    @Override
    public void w() {
        this.Z();
        final boolean x = this.x();
        final int a = this.k.a(x, 2);
        this.a(x, a, b(x, a));
        this.e.w();
    }
    
    @Override
    public boolean x() {
        this.Z();
        return this.e.x();
    }
    
    @Override
    public int y() {
        this.Z();
        return this.e.y();
    }
    
    @Override
    public boolean z() {
        this.Z();
        return this.e.z();
    }
    
    @Deprecated
    public static final class a
    {
        private boolean A;
        private final Context a;
        private final au b;
        private com.applovin.exoplayer2.l.d c;
        private long d;
        private j e;
        private com.applovin.exoplayer2.h.r f;
        private com.applovin.exoplayer2.aa g;
        private com.applovin.exoplayer2.k.d h;
        private com.applovin.exoplayer2.a.a i;
        private Looper j;
        @Nullable
        private aa k;
        private com.applovin.exoplayer2.b.d l;
        private boolean m;
        private int n;
        private boolean o;
        private boolean p;
        private int q;
        private int r;
        private boolean s;
        private av t;
        private long u;
        private long v;
        private z w;
        private long x;
        private long y;
        private boolean z;
        
        @Deprecated
        public a(final Context context) {
            this(context, new com.applovin.exoplayer2.n(context), new f());
        }
        
        @Deprecated
        public a(final Context context, final au au, final com.applovin.exoplayer2.e.l l) {
            this(context, au, (j)new com.applovin.exoplayer2.j.c(context), new com.applovin.exoplayer2.h.f(context, l), new com.applovin.exoplayer2.l(), (com.applovin.exoplayer2.k.d)com.applovin.exoplayer2.k.n.a(context), new com.applovin.exoplayer2.a.a(com.applovin.exoplayer2.l.d.a));
        }
        
        @Deprecated
        public a(final Context a, final au b, final j e, final com.applovin.exoplayer2.h.r f, final com.applovin.exoplayer2.aa g, final com.applovin.exoplayer2.k.d h, final com.applovin.exoplayer2.a.a i) {
            this.a = a;
            this.b = b;
            this.e = e;
            this.f = f;
            this.g = g;
            this.h = h;
            this.i = i;
            this.j = ai.c();
            this.l = com.applovin.exoplayer2.b.d.a;
            this.n = 0;
            this.q = 1;
            this.r = 0;
            this.s = true;
            this.t = av.e;
            this.u = 5000L;
            this.v = 15000L;
            this.w = new com.applovin.exoplayer2.k.a().a();
            this.c = com.applovin.exoplayer2.l.d.a;
            this.x = 500L;
            this.y = 2000L;
        }
        
        @Deprecated
        public aw a() {
            com.applovin.exoplayer2.l.a.b(this.A ^ true);
            this.A = true;
            return new aw(this);
        }
    }
    
    private final class b implements SurfaceHolder$Callback, TextureView$SurfaceTextureListener, an.b, ay.a, com.applovin.exoplayer2.b.b, g, com.applovin.exoplayer2.c.b, com.applovin.exoplayer2.g.e, l, i$a, n, q.a
    {
        final aw a;
        
        private b(final aw a) {
            this.a = a;
        }
        
        public void a() {
            this.a.a(false, -1, 3);
        }
        
        public void a(final float n) {
            this.a.W();
        }
        
        public void a(final int n) {
            final boolean x = this.a.x();
            this.a.a(x, n, b(x, n));
        }
        
        public void a(final int n, final long n2) {
            this.a.i.a(n, n2);
        }
        
        public void a(final int n, final long n2, final long n3) {
            this.a.i.a(n, n2, n3);
        }
        
        public void a(final int n, final boolean b) {
            final Iterator iterator = this.a.h.iterator();
            while (iterator.hasNext()) {
                ((an.d)iterator.next()).a(n, b);
            }
        }
        
        public void a(final long n) {
            this.a.i.a(n);
        }
        
        public void a(final long n, final int n2) {
            this.a.i.a(n, n2);
        }
        
        public void a(final Surface surface) {
            this.a.a((Object)null);
        }
        
        public void a(final e e) {
            this.a.C = e;
            this.a.i.a(e);
        }
        
        public void a(final com.applovin.exoplayer2.g.a a) {
            this.a.i.a(a);
            this.a.e.a(a);
            final Iterator iterator = this.a.h.iterator();
            while (iterator.hasNext()) {
                ((an.d)iterator.next()).a(a);
            }
        }
        
        public void a(final com.applovin.exoplayer2.m.o o) {
            this.a.P = o;
            this.a.i.a(o);
            final Iterator iterator = this.a.h.iterator();
            while (iterator.hasNext()) {
                ((an.d)iterator.next()).a(o);
            }
        }
        
        public void a(final v v, @Nullable final com.applovin.exoplayer2.c.h h) {
            this.a.p = v;
            this.a.i.a(v, h);
        }
        
        public void a(final Exception ex) {
            this.a.i.a(ex);
        }
        
        public void a(final Object o, final long n) {
            this.a.i.a(o, n);
            if (this.a.s == o) {
                final Iterator iterator = this.a.h.iterator();
                while (iterator.hasNext()) {
                    ((an.d)iterator.next()).a();
                }
            }
        }
        
        public void a(final String s) {
            this.a.i.a(s);
        }
        
        public void a(final String s, final long n, final long n2) {
            this.a.i.a(s, n, n2);
        }
        
        public void a(final List<a> list) {
            this.a.I = list;
            final Iterator iterator = this.a.h.iterator();
            while (iterator.hasNext()) {
                ((an.d)iterator.next()).a(list);
            }
        }
        
        public void a_(final boolean b) {
            if (this.a.H == b) {
                return;
            }
            this.a.H = b;
            this.a.X();
        }
        
        public void b(final int n) {
            this.a.Y();
        }
        
        public void b(final e e) {
            this.a.i.b(e);
            this.a.p = null;
            this.a.C = null;
        }
        
        public void b(final v v, @Nullable final com.applovin.exoplayer2.c.h h) {
            this.a.q = v;
            this.a.i.b(v, h);
        }
        
        public void b(final Exception ex) {
            this.a.i.b(ex);
        }
        
        public void b(final String s) {
            this.a.i.b(s);
        }
        
        public void b(final String s, final long n, final long n2) {
            this.a.i.b(s, n, n2);
        }
        
        public void b(final boolean b) {
            this.a.Y();
        }
        
        public void b(final boolean b, final int n) {
            this.a.Y();
        }
        
        public void b_(final boolean b) {
            if (this.a.L != null) {
                if (b && !this.a.M) {
                    this.a.L.a(0);
                    this.a.M = true;
                }
                else if (!b && this.a.M) {
                    this.a.L.b(0);
                    this.a.M = false;
                }
            }
        }
        
        public void c(final e e) {
            this.a.D = e;
            this.a.i.c(e);
        }
        
        public void c(final Exception ex) {
            this.a.i.c(ex);
        }
        
        public void d(final e e) {
            this.a.i.d(e);
            this.a.q = null;
            this.a.D = null;
        }
        
        public void f(final int n) {
            final o a = b(this.a.l);
            if (!a.equals(this.a.O)) {
                this.a.O = a;
                final Iterator iterator = this.a.h.iterator();
                while (iterator.hasNext()) {
                    ((an.d)iterator.next()).a(a);
                }
            }
        }
        
        public void onSurfaceTextureAvailable(final SurfaceTexture surfaceTexture, final int n, final int n2) {
            this.a.a(surfaceTexture);
            this.a.a(n, n2);
        }
        
        public boolean onSurfaceTextureDestroyed(final SurfaceTexture surfaceTexture) {
            this.a.a((Object)null);
            this.a.a(0, 0);
            return true;
        }
        
        public void onSurfaceTextureSizeChanged(final SurfaceTexture surfaceTexture, final int n, final int n2) {
            this.a.a(n, n2);
        }
        
        public void onSurfaceTextureUpdated(final SurfaceTexture surfaceTexture) {
        }
        
        public void surfaceChanged(final SurfaceHolder surfaceHolder, final int n, final int n2, final int n3) {
            this.a.a(n2, n3);
        }
        
        public void surfaceCreated(final SurfaceHolder surfaceHolder) {
            if (this.a.w) {
                this.a.a(surfaceHolder.getSurface());
            }
        }
        
        public void surfaceDestroyed(final SurfaceHolder surfaceHolder) {
            if (this.a.w) {
                this.a.a((Object)null);
            }
            this.a.a(0, 0);
        }
    }
    
    private static final class c implements ao.b, com.applovin.exoplayer2.m.a.a, com.applovin.exoplayer2.m.l
    {
        @Nullable
        private com.applovin.exoplayer2.m.l a;
        @Nullable
        private com.applovin.exoplayer2.m.a.a b;
        @Nullable
        private com.applovin.exoplayer2.m.l c;
        @Nullable
        private com.applovin.exoplayer2.m.a.a d;
        
        public void a() {
            final com.applovin.exoplayer2.m.a.a d = this.d;
            if (d != null) {
                d.a();
            }
            final com.applovin.exoplayer2.m.a.a b = this.b;
            if (b != null) {
                b.a();
            }
        }
        
        @Override
        public void a(final int n, @Nullable final Object o) {
            if (n != 7) {
                if (n != 8) {
                    if (n == 10000) {
                        final i i = (i)o;
                        if (i == null) {
                            this.c = null;
                            this.d = null;
                        }
                        else {
                            this.c = i.getVideoFrameMetadataListener();
                            this.d = i.getCameraMotionListener();
                        }
                    }
                }
                else {
                    this.b = (com.applovin.exoplayer2.m.a.a)o;
                }
            }
            else {
                this.a = (com.applovin.exoplayer2.m.l)o;
            }
        }
        
        public void a(final long n, final long n2, final v v, @Nullable final MediaFormat mediaFormat) {
            final com.applovin.exoplayer2.m.l c = this.c;
            if (c != null) {
                c.a(n, n2, v, mediaFormat);
            }
            final com.applovin.exoplayer2.m.l a = this.a;
            if (a != null) {
                a.a(n, n2, v, mediaFormat);
            }
        }
        
        public void a(final long n, final float[] array) {
            final com.applovin.exoplayer2.m.a.a d = this.d;
            if (d != null) {
                d.a(n, array);
            }
            final com.applovin.exoplayer2.m.a.a b = this.b;
            if (b != null) {
                b.a(n, array);
            }
        }
    }
}

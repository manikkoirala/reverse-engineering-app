// 
// Decompiled by Procyon v0.6.0
// 

package com.applovin.exoplayer2;

import com.applovin.exoplayer2.k.b;
import com.applovin.exoplayer2.l.q;
import com.applovin.exoplayer2.l.ai;
import com.applovin.exoplayer2.h.ad;
import com.applovin.exoplayer2.j.d;
import com.applovin.exoplayer2.l.a;
import com.applovin.exoplayer2.k.m;

public class l implements aa
{
    private final m a;
    private final long b;
    private final long c;
    private final long d;
    private final long e;
    private final int f;
    private final boolean g;
    private final long h;
    private final boolean i;
    private int j;
    private boolean k;
    
    public l() {
        this(new m(true, 65536), 50000, 50000, 2500, 5000, -1, false, 0, false);
    }
    
    protected l(final m a, final int n, final int n2, final int n3, final int n4, int n5, final boolean g, final int n6, final boolean i) {
        a(n3, 0, "bufferForPlaybackMs", "0");
        a(n4, 0, "bufferForPlaybackAfterRebufferMs", "0");
        a(n, n3, "minBufferMs", "bufferForPlaybackMs");
        a(n, n4, "minBufferMs", "bufferForPlaybackAfterRebufferMs");
        a(n2, n, "maxBufferMs", "minBufferMs");
        a(n6, 0, "backBufferDurationMs", "0");
        this.a = a;
        this.b = com.applovin.exoplayer2.h.b((long)n);
        this.c = com.applovin.exoplayer2.h.b((long)n2);
        this.d = com.applovin.exoplayer2.h.b((long)n3);
        this.e = com.applovin.exoplayer2.h.b((long)n4);
        this.f = n5;
        if (n5 == -1) {
            n5 = 13107200;
        }
        this.j = n5;
        this.g = g;
        this.h = com.applovin.exoplayer2.h.b((long)n6);
        this.i = i;
    }
    
    private static int a(final int n) {
        switch (n) {
            default: {
                throw new IllegalArgumentException();
            }
            case 3:
            case 4:
            case 5:
            case 6: {
                return 131072;
            }
            case 2: {
                return 131072000;
            }
            case 1: {
                return 13107200;
            }
            case 0: {
                return 144310272;
            }
            case -2: {
                return 0;
            }
        }
    }
    
    private static void a(final int n, final int n2, final String str, final String str2) {
        final boolean b = n >= n2;
        final StringBuilder sb = new StringBuilder();
        sb.append(str);
        sb.append(" cannot be less than ");
        sb.append(str2);
        a.a(b, (Object)sb.toString());
    }
    
    private void a(final boolean b) {
        int f;
        if ((f = this.f) == -1) {
            f = 13107200;
        }
        this.j = f;
        this.k = false;
        if (b) {
            this.a.d();
        }
    }
    
    protected int a(final ar[] array, final d[] array2) {
        int i = 0;
        int b = 0;
        while (i < array.length) {
            int n = b;
            if (array2[i] != null) {
                n = b + a(array[i].a());
            }
            ++i;
            b = n;
        }
        return Math.max(13107200, b);
    }
    
    @Override
    public void a() {
        this.a(false);
    }
    
    @Override
    public void a(final ar[] array, final ad ad, final d[] array2) {
        int j;
        if ((j = this.f) == -1) {
            j = this.a(array, array2);
        }
        this.j = j;
        this.a.a(j);
    }
    
    @Override
    public boolean a(long b, final float n, final boolean b2, final long n2) {
        final long b3 = ai.b(b, n);
        if (b2) {
            b = this.e;
        }
        else {
            b = this.d;
        }
        long min = b;
        if (n2 != -9223372036854775807L) {
            min = Math.min(n2 / 2L, b);
        }
        return min <= 0L || b3 >= min || (!this.g && this.a.e() >= this.j);
    }
    
    @Override
    public boolean a(long a, final long n, final float n2) {
        final int e = this.a.e();
        final int j = this.j;
        final boolean b = true;
        final boolean b2 = e >= j;
        a = this.b;
        if (n2 > 1.0f) {
            a = Math.min(ai.a(a, n2), this.c);
        }
        if (n < Math.max(a, 500000L)) {
            boolean k = b;
            if (!this.g) {
                k = (!b2 && b);
            }
            this.k = k;
            if (!k && n < 500000L) {
                q.c("DefaultLoadControl", "Target buffer size reached with less than 500ms of buffered media data.");
            }
        }
        else if (n >= this.c || b2) {
            this.k = false;
        }
        return this.k;
    }
    
    @Override
    public void b() {
        this.a(true);
    }
    
    @Override
    public void c() {
        this.a(true);
    }
    
    @Override
    public b d() {
        return (b)this.a;
    }
    
    @Override
    public long e() {
        return this.h;
    }
    
    @Override
    public boolean f() {
        return this.i;
    }
}

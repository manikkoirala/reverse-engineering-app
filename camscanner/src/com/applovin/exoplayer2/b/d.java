// 
// Decompiled by Procyon v0.6.0
// 

package com.applovin.exoplayer2.b;

import android.os.BaseBundle;
import androidx.annotation.RequiresApi;
import com.applovin.exoplayer2.l.ai;
import android.media.AudioAttributes$Builder;
import android.os.Bundle;
import androidx.annotation.Nullable;
import android.media.AudioAttributes;
import com.applovin.exoplayer2.g;

public final class d implements g
{
    public static final d a;
    public static final g.a<d> f;
    public final int b;
    public final int c;
    public final int d;
    public final int e;
    @Nullable
    private AudioAttributes g;
    
    static {
        a = new a().a();
        f = new \u3007o00\u3007\u3007Oo();
    }
    
    private d(final int b, final int c, final int d, final int e) {
        this.b = b;
        this.c = c;
        this.d = d;
        this.e = e;
    }
    
    private static String a(final int i) {
        return Integer.toString(i, 36);
    }
    
    @RequiresApi(21)
    public AudioAttributes a() {
        if (this.g == null) {
            final AudioAttributes$Builder setUsage = new AudioAttributes$Builder().setContentType(this.b).setFlags(this.c).setUsage(this.d);
            if (ai.a >= 29) {
                \u3007080.\u3007080(setUsage, this.e);
            }
            this.g = setUsage.build();
        }
        return this.g;
    }
    
    @Override
    public boolean equals(@Nullable final Object o) {
        boolean b = true;
        if (this == o) {
            return true;
        }
        if (o != null && d.class == o.getClass()) {
            final d d = (d)o;
            if (this.b != d.b || this.c != d.c || this.d != d.d || this.e != d.e) {
                b = false;
            }
            return b;
        }
        return false;
    }
    
    @Override
    public int hashCode() {
        return (((527 + this.b) * 31 + this.c) * 31 + this.d) * 31 + this.e;
    }
    
    public static final class a
    {
        private int a;
        private int b;
        private int c;
        private int d;
        
        public a() {
            this.a = 0;
            this.b = 0;
            this.c = 1;
            this.d = 1;
        }
        
        public a a(final int a) {
            this.a = a;
            return this;
        }
        
        public d a() {
            return new d(this.a, this.b, this.c, this.d, null);
        }
        
        public a b(final int b) {
            this.b = b;
            return this;
        }
        
        public a c(final int c) {
            this.c = c;
            return this;
        }
        
        public a d(final int d) {
            this.d = d;
            return this;
        }
    }
}

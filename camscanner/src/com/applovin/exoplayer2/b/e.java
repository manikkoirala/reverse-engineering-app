// 
// Decompiled by Procyon v0.6.0
// 

package com.applovin.exoplayer2.b;

import java.util.Collection;
import com.applovin.exoplayer2.common.b.c;
import android.media.AudioAttributes$Builder;
import android.media.AudioFormat$Builder;
import com.applovin.exoplayer2.common.a.s;
import androidx.annotation.RequiresApi;
import android.annotation.SuppressLint;
import com.applovin.exoplayer2.l.ai;
import android.provider.Settings$Global;
import android.content.Intent;
import android.content.BroadcastReceiver;
import android.content.IntentFilter;
import android.content.Context;
import java.util.Arrays;
import androidx.annotation.Nullable;

public final class e
{
    public static final e a;
    private static final e b;
    private static final int[] c;
    private final int[] d;
    private final int e;
    
    static {
        a = new e(new int[] { 2 }, 8);
        b = new e(new int[] { 2, 5, 6 }, 8);
        c = new int[] { 5, 6, 18, 17, 14, 7, 8 };
    }
    
    public e(@Nullable int[] copy, final int e) {
        if (copy != null) {
            copy = Arrays.copyOf(copy, copy.length);
            Arrays.sort(this.d = copy);
        }
        else {
            this.d = new int[0];
        }
        this.e = e;
    }
    
    public static e a(final Context context) {
        return a(context, context.registerReceiver((BroadcastReceiver)null, new IntentFilter("android.media.action.HDMI_AUDIO_PLUG")));
    }
    
    @SuppressLint({ "InlinedApi" })
    static e a(final Context context, @Nullable final Intent intent) {
        if (c() && Settings$Global.getInt(context.getContentResolver(), "external_surround_sound_enabled", 0) == 1) {
            return e.b;
        }
        if (ai.a >= 29 && ai.c(context)) {
            return new e(e.a.a(), 8);
        }
        if (intent != null && intent.getIntExtra("android.media.extra.AUDIO_PLUG_STATE", 0) != 0) {
            return new e(intent.getIntArrayExtra("android.media.extra.ENCODINGS"), intent.getIntExtra("android.media.extra.MAX_CHANNEL_COUNT", 8));
        }
        return e.a;
    }
    
    private static boolean c() {
        if (ai.a >= 17) {
            final String c = ai.c;
            if ("Amazon".equals(c) || "Xiaomi".equals(c)) {
                return true;
            }
        }
        return false;
    }
    
    public int a() {
        return this.e;
    }
    
    public boolean a(final int key) {
        return Arrays.binarySearch(this.d, key) >= 0;
    }
    
    @Override
    public boolean equals(@Nullable final Object o) {
        boolean b = true;
        if (this == o) {
            return true;
        }
        if (!(o instanceof e)) {
            return false;
        }
        final e e = (e)o;
        if (!Arrays.equals(this.d, e.d) || this.e != e.e) {
            b = false;
        }
        return b;
    }
    
    @Override
    public int hashCode() {
        return this.e + Arrays.hashCode(this.d) * 31;
    }
    
    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append("AudioCapabilities[maxChannelCount=");
        sb.append(this.e);
        sb.append(", supportedEncodings=");
        sb.append(Arrays.toString(this.d));
        sb.append("]");
        return sb.toString();
    }
    
    @RequiresApi(29)
    private static final class a
    {
        public static int[] a() {
            final s.a<Integer> i = s.i();
            for (final int n : e.c) {
                if (\u3007o\u3007.\u3007080(new AudioFormat$Builder().setChannelMask(12).setEncoding(n).setSampleRate(48000).build(), new AudioAttributes$Builder().setUsage(1).setContentType(3).setFlags(0).build())) {
                    i.b(n);
                }
            }
            i.b(2);
            return com.applovin.exoplayer2.common.b.c.a(i.a());
        }
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.applovin.exoplayer2.b;

import com.applovin.exoplayer2.v;
import androidx.annotation.Nullable;
import com.applovin.exoplayer2.d.e;
import com.applovin.exoplayer2.l.y;
import java.nio.ByteBuffer;
import com.applovin.exoplayer2.l.x;

public final class c
{
    private static final int[] a;
    
    static {
        a = new int[] { 2002, 2000, 1920, 1601, 1600, 1001, 1000, 960, 800, 800, 480, 400, 400, 2048 };
    }
    
    private static int a(final x x, final int n) {
        int n2 = 0;
        int n3;
        while (true) {
            n3 = n2 + x.c(n);
            if (!x.e()) {
                break;
            }
            n2 = n3 + 1 << n;
        }
        return n3;
    }
    
    public static int a(final ByteBuffer byteBuffer) {
        final byte[] dst = new byte[16];
        final int position = byteBuffer.position();
        byteBuffer.get(dst);
        byteBuffer.position(position);
        return a(new x(dst)).e;
    }
    
    public static int a(final byte[] array, final int n) {
        final int length = array.length;
        int n2 = 7;
        if (length < 7) {
            return -1;
        }
        int n3 = (array[2] & 0xFF) << 8 | (array[3] & 0xFF);
        if (n3 == 65535) {
            n3 = ((array[4] & 0xFF) << 16 | (array[5] & 0xFF) << 8 | (array[6] & 0xFF));
        }
        else {
            n2 = 4;
        }
        int n4 = n2;
        if (n == 44097) {
            n4 = n2 + 2;
        }
        return n3 + n4;
    }
    
    public static a a(final x x) {
        final int c = x.c(16);
        int n = x.c(16);
        int n2;
        if (n == 65535) {
            n = x.c(24);
            n2 = 7;
        }
        else {
            n2 = 4;
        }
        int n3 = n + n2;
        if (c == 44097) {
            n3 += 2;
        }
        final int c2 = x.c(2);
        int n4;
        if ((n4 = c2) == 3) {
            n4 = c2 + a(x, 2);
        }
        final int c3 = x.c(10);
        if (x.e() && x.c(3) > 0) {
            x.b(2);
        }
        int n5;
        if (x.e()) {
            n5 = 48000;
        }
        else {
            n5 = 44100;
        }
        final int c4 = x.c(4);
        int n6;
        if (n5 == 44100 && c4 == 13) {
            n6 = com.applovin.exoplayer2.b.c.a[c4];
        }
        else {
            if (n5 == 48000) {
                final int[] a = com.applovin.exoplayer2.b.c.a;
                if (c4 < a.length) {
                    final int n7 = a[c4];
                    final int n8 = c3 % 5;
                    Label_0270: {
                        if (n8 != 1) {
                            if (n8 != 2) {
                                if (n8 != 3) {
                                    if (n8 != 4) {
                                        n6 = n7;
                                        return new a(n4, 2, n5, n3, n6);
                                    }
                                    if (c4 == 3 || c4 == 8) {
                                        break Label_0270;
                                    }
                                    n6 = n7;
                                    if (c4 == 11) {
                                        break Label_0270;
                                    }
                                    return new a(n4, 2, n5, n3, n6);
                                }
                            }
                            else {
                                if (c4 == 8) {
                                    break Label_0270;
                                }
                                n6 = n7;
                                if (c4 == 11) {
                                    break Label_0270;
                                }
                                return new a(n4, 2, n5, n3, n6);
                            }
                        }
                        if (c4 == 3) {
                            break Label_0270;
                        }
                        n6 = n7;
                        if (c4 == 8) {
                            break Label_0270;
                        }
                        return new a(n4, 2, n5, n3, n6);
                    }
                    n6 = n7 + 1;
                    return new a(n4, 2, n5, n3, n6);
                }
            }
            n6 = 0;
        }
        return new a(n4, 2, n5, n3, n6);
    }
    
    public static v a(final y y, final String s, final String s2, @Nullable final e e) {
        y.e(1);
        int n;
        if ((y.h() & 0x20) >> 5 == 1) {
            n = 48000;
        }
        else {
            n = 44100;
        }
        return new v.a().a(s).f("audio/ac4").k(2).l(n).a(e).c(s2).a();
    }
    
    public static void a(final int n, final y y) {
        y.a(7);
        final byte[] d = y.d();
        d[0] = -84;
        d[1] = 64;
        d[3] = (d[2] = -1);
        d[4] = (byte)(n >> 16 & 0xFF);
        d[5] = (byte)(n >> 8 & 0xFF);
        d[6] = (byte)(n & 0xFF);
    }
    
    public static final class a
    {
        public final int a;
        public final int b;
        public final int c;
        public final int d;
        public final int e;
        
        private a(final int a, final int c, final int b, final int d, final int e) {
            this.a = a;
            this.c = c;
            this.b = b;
            this.d = d;
            this.e = e;
        }
    }
}

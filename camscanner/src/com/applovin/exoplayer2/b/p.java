// 
// Decompiled by Procyon v0.6.0
// 

package com.applovin.exoplayer2.b;

import com.applovin.exoplayer2.l.ai;
import java.nio.ByteBuffer;

final class p extends l
{
    private static final int d;
    
    static {
        d = Float.floatToIntBits(Float.NaN);
    }
    
    private static void a(int n, final ByteBuffer byteBuffer) {
        if ((n = Float.floatToIntBits((float)(n * 4.656612875245797E-10))) == p.d) {
            n = Float.floatToIntBits(0.0f);
        }
        byteBuffer.putInt(n);
    }
    
    @Override
    public void a(final ByteBuffer byteBuffer) {
        int position = byteBuffer.position();
        final int limit = byteBuffer.limit();
        final int n = limit - position;
        final int d = super.b.d;
        ByteBuffer byteBuffer2;
        if (d != 536870912) {
            if (d != 805306368) {
                throw new IllegalStateException();
            }
            final ByteBuffer a = this.a(n);
            while (true) {
                byteBuffer2 = a;
                if (position >= limit) {
                    break;
                }
                a((byteBuffer.get(position) & 0xFF) | (byteBuffer.get(position + 1) & 0xFF) << 8 | (byteBuffer.get(position + 2) & 0xFF) << 16 | (byteBuffer.get(position + 3) & 0xFF) << 24, a);
                position += 4;
            }
        }
        else {
            final ByteBuffer a2 = this.a(n / 3 * 4);
            while (true) {
                byteBuffer2 = a2;
                if (position >= limit) {
                    break;
                }
                a((byteBuffer.get(position) & 0xFF) << 8 | (byteBuffer.get(position + 1) & 0xFF) << 16 | (byteBuffer.get(position + 2) & 0xFF) << 24, a2);
                position += 3;
            }
        }
        byteBuffer.position(byteBuffer.limit());
        byteBuffer2.flip();
    }
    
    public a b(a a) throws b {
        final int d = a.d;
        if (ai.e(d)) {
            if (d != 4) {
                a = new f.a(a.b, a.c, 4);
            }
            else {
                a = com.applovin.exoplayer2.b.f.a.a;
            }
            return a;
        }
        throw new f.b(a);
    }
}

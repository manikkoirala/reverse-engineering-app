// 
// Decompiled by Procyon v0.6.0
// 

package com.applovin.exoplayer2.b;

import com.applovin.exoplayer2.l.ai;
import java.nio.ByteOrder;
import java.nio.ByteBuffer;

public interface f
{
    public static final ByteBuffer a = ByteBuffer.allocateDirect(0).order(ByteOrder.nativeOrder());
    
    a a(final a p0) throws b;
    
    void a(final ByteBuffer p0);
    
    boolean a();
    
    void b();
    
    ByteBuffer c();
    
    boolean d();
    
    void e();
    
    void f();
    
    public static final class a
    {
        public static final a a;
        public final int b;
        public final int c;
        public final int d;
        public final int e;
        
        static {
            a = new a(-1, -1, -1);
        }
        
        public a(int c, final int c2, final int d) {
            this.b = c;
            this.c = c2;
            this.d = d;
            if (ai.d(d)) {
                c = ai.c(d, c2);
            }
            else {
                c = -1;
            }
            this.e = c;
        }
        
        @Override
        public String toString() {
            final StringBuilder sb = new StringBuilder();
            sb.append("AudioFormat[sampleRate=");
            sb.append(this.b);
            sb.append(", channelCount=");
            sb.append(this.c);
            sb.append(", encoding=");
            sb.append(this.d);
            sb.append(']');
            return sb.toString();
        }
    }
    
    public static final class b extends Exception
    {
        public b(final a obj) {
            final StringBuilder sb = new StringBuilder();
            sb.append("Unhandled format: ");
            sb.append(obj);
            super(sb.toString());
        }
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.applovin.exoplayer2.b;

import com.applovin.exoplayer2.l.q;
import com.applovin.exoplayer2.ai;
import com.applovin.exoplayer2.l.x;

public final class a
{
    private static final int[] a;
    private static final int[] b;
    
    static {
        a = new int[] { 96000, 88200, 64000, 48000, 44100, 32000, 24000, 22050, 16000, 12000, 11025, 8000, 7350 };
        b = new int[] { 0, 1, 2, 3, 4, 5, 6, 8, -1, -1, -1, 7, 8, -1, 8, -1 };
    }
    
    private static int a(final x x) {
        int c;
        if ((c = x.c(5)) == 31) {
            c = x.c(6) + 32;
        }
        return c;
    }
    
    public static a a(final x x, final boolean b) throws ai {
        final int a = a(x);
        int b2 = b(x);
        final int c = x.c(4);
        final StringBuilder sb = new StringBuilder();
        sb.append("mp4a.40.");
        sb.append(a);
        final String string = sb.toString();
        int a2 = 0;
        int c2 = 0;
        Label_0112: {
            if (a != 5) {
                a2 = a;
                c2 = c;
                if (a != 29) {
                    break Label_0112;
                }
            }
            final int b3 = b(x);
            final int n = a2 = a(x);
            b2 = b3;
            c2 = c;
            if (n == 22) {
                c2 = x.c(4);
                b2 = b3;
                a2 = n;
            }
        }
        if (b) {
            if (a2 != 1 && a2 != 2 && a2 != 3 && a2 != 4 && a2 != 6 && a2 != 7 && a2 != 17) {
                switch (a2) {
                    default: {
                        final StringBuilder sb2 = new StringBuilder();
                        sb2.append("Unsupported audio object type: ");
                        sb2.append(a2);
                        throw ai.a(sb2.toString());
                    }
                    case 19:
                    case 20:
                    case 21:
                    case 22:
                    case 23: {
                        break;
                    }
                }
            }
            a(x, a2, c2);
            switch (a2) {
                case 17:
                case 19:
                case 20:
                case 21:
                case 22:
                case 23: {
                    final int c3 = x.c(2);
                    if (c3 != 2 && c3 != 3) {
                        break;
                    }
                    final StringBuilder sb3 = new StringBuilder();
                    sb3.append("Unsupported epConfig: ");
                    sb3.append(c3);
                    throw ai.a(sb3.toString());
                }
            }
        }
        final int n2 = com.applovin.exoplayer2.b.a.b[c2];
        if (n2 != -1) {
            return new a(b2, n2, string);
        }
        throw ai.b(null, null);
    }
    
    public static a a(final byte[] array) throws ai {
        return a(new x(array), false);
    }
    
    private static void a(final x x, final int n, final int n2) {
        if (x.e()) {
            q.c("AacUtil", "Unexpected frameLengthFlag = 1");
        }
        if (x.e()) {
            x.b(14);
        }
        final boolean e = x.e();
        if (n2 != 0) {
            if (n == 6 || n == 20) {
                x.b(3);
            }
            if (e) {
                if (n == 22) {
                    x.b(16);
                }
                if (n == 17 || n == 19 || n == 20 || n == 23) {
                    x.b(3);
                }
                x.b(1);
            }
            return;
        }
        throw new UnsupportedOperationException();
    }
    
    public static byte[] a(final int n, final int n2, final int n3) {
        return new byte[] { (byte)((n << 3 & 0xF8) | (n2 >> 1 & 0x7)), (byte)((n2 << 7 & 0x80) | (n3 << 3 & 0x78)) };
    }
    
    private static int b(final x x) throws ai {
        final int c = x.c(4);
        int c2;
        if (c == 15) {
            c2 = x.c(24);
        }
        else {
            if (c >= 13) {
                throw ai.b(null, null);
            }
            c2 = com.applovin.exoplayer2.b.a.a[c];
        }
        return c2;
    }
    
    public static final class a
    {
        public final int a;
        public final int b;
        public final String c;
        
        private a(final int a, final int b, final String c) {
            this.a = a;
            this.b = b;
            this.c = c;
        }
    }
}

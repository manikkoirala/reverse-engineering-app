// 
// Decompiled by Procyon v0.6.0
// 

package com.applovin.exoplayer2.b;

import com.applovin.exoplayer2.h;
import android.os.SystemClock;
import com.applovin.exoplayer2.l.ai;
import com.applovin.exoplayer2.l.a;
import java.lang.reflect.Method;
import androidx.annotation.Nullable;
import android.media.AudioTrack;

final class j
{
    private long A;
    private long B;
    private long C;
    private boolean D;
    private long E;
    private long F;
    private final a a;
    private final long[] b;
    @Nullable
    private AudioTrack c;
    private int d;
    private int e;
    @Nullable
    private i f;
    private int g;
    private boolean h;
    private long i;
    private float j;
    private boolean k;
    private long l;
    private long m;
    @Nullable
    private Method n;
    private long o;
    private boolean p;
    private boolean q;
    private long r;
    private long s;
    private long t;
    private long u;
    private int v;
    private int w;
    private long x;
    private long y;
    private long z;
    
    public j(final a a) {
        this.a = (a)com.applovin.exoplayer2.l.a.b((Object)a);
        while (true) {
            if (ai.a < 18) {
                break Label_0035;
            }
            try {
                this.n = AudioTrack.class.getMethod("getLatency", (Class<?>[])null);
                this.b = new long[10];
            }
            catch (final NoSuchMethodException ex) {
                continue;
            }
            break;
        }
    }
    
    private void a(final long n, final long n2) {
        final i i = (i)com.applovin.exoplayer2.l.a.b((Object)this.f);
        if (!i.a(n)) {
            return;
        }
        final long e = i.e();
        final long f = i.f();
        if (Math.abs(e - n) > 5000000L) {
            this.a.b(f, e, n, n2);
            i.a();
        }
        else if (Math.abs(this.h(f) - n2) > 5000000L) {
            this.a.a(f, e, n, n2);
            i.a();
        }
        else {
            i.b();
        }
    }
    
    private static boolean a(final int n) {
        return ai.a < 23 && (n == 5 || n == 6);
    }
    
    private void e() {
        final long h = this.h();
        if (h == 0L) {
            return;
        }
        final long m = System.nanoTime() / 1000L;
        if (m - this.m >= 30000L) {
            final long[] b = this.b;
            final int v = this.v;
            b[v] = h - m;
            this.v = (v + 1) % 10;
            final int w = this.w;
            if (w < 10) {
                this.w = w + 1;
            }
            this.m = m;
            this.l = 0L;
            int n = 0;
            while (true) {
                final int w2 = this.w;
                if (n >= w2) {
                    break;
                }
                this.l += this.b[n] / w2;
                ++n;
            }
        }
        if (this.h) {
            return;
        }
        this.a(m, h);
        this.g(m);
    }
    
    private void f() {
        this.l = 0L;
        this.w = 0;
        this.v = 0;
        this.m = 0L;
        this.C = 0L;
        this.F = 0L;
        this.k = false;
    }
    
    private void g(final long r) {
        if (this.q) {
            final Method n = this.n;
            if (n != null && r - this.r >= 500000L) {
                try {
                    final long n2 = (int)ai.a((Object)n.invoke(com.applovin.exoplayer2.l.a.b((Object)this.c), new Object[0])) * 1000L - this.i;
                    this.o = n2;
                    final long max = Math.max(n2, 0L);
                    this.o = max;
                    if (max > 5000000L) {
                        this.a.b(max);
                        this.o = 0L;
                    }
                }
                catch (final Exception ex) {
                    this.n = null;
                }
                this.r = r;
            }
        }
    }
    
    private boolean g() {
        return this.h && ((AudioTrack)com.applovin.exoplayer2.l.a.b((Object)this.c)).getPlayState() == 2 && this.i() == 0L;
    }
    
    private long h() {
        return this.h(this.i());
    }
    
    private long h(final long n) {
        return n * 1000000L / this.g;
    }
    
    private long i() {
        final AudioTrack audioTrack = (AudioTrack)com.applovin.exoplayer2.l.a.b((Object)this.c);
        if (this.x != -9223372036854775807L) {
            return Math.min(this.A, this.z + (SystemClock.elapsedRealtime() * 1000L - this.x) * this.g / 1000000L);
        }
        final int playState = audioTrack.getPlayState();
        if (playState == 1) {
            return 0L;
        }
        long s;
        final long n = s = ((long)audioTrack.getPlaybackHeadPosition() & 0xFFFFFFFFL);
        if (this.h) {
            if (playState == 2 && n == 0L) {
                this.u = this.s;
            }
            s = n + this.u;
        }
        if (ai.a <= 29) {
            if (s == 0L && this.s > 0L && playState == 3) {
                if (this.y == -9223372036854775807L) {
                    this.y = SystemClock.elapsedRealtime();
                }
                return this.s;
            }
            this.y = -9223372036854775807L;
        }
        if (this.s > s) {
            ++this.t;
        }
        this.s = s;
        return s + (this.t << 32);
    }
    
    public long a(final boolean b) {
        if (((AudioTrack)com.applovin.exoplayer2.l.a.b((Object)this.c)).getPlayState() == 3) {
            this.e();
        }
        final long c = System.nanoTime() / 1000L;
        final i i = (i)com.applovin.exoplayer2.l.a.b((Object)this.f);
        final boolean c2 = i.c();
        long max;
        if (c2) {
            max = this.h(i.f()) + ai.a(c - i.e(), this.j);
        }
        else {
            long h;
            if (this.w == 0) {
                h = this.h();
            }
            else {
                h = this.l + c;
            }
            max = h;
            if (!b) {
                max = Math.max(0L, h - this.o);
            }
        }
        if (this.D != c2) {
            this.F = this.C;
            this.E = this.B;
        }
        final long n = c - this.F;
        long b2 = max;
        if (n < 1000000L) {
            final long e = this.E;
            final long a = ai.a(n, this.j);
            final long n2 = n * 1000L / 1000000L;
            b2 = (max * n2 + (1000L - n2) * (e + a)) / 1000L;
        }
        if (!this.k) {
            final long b3 = this.B;
            if (b2 > b3) {
                this.k = true;
                this.a.a(System.currentTimeMillis() - com.applovin.exoplayer2.h.a(ai.b(com.applovin.exoplayer2.h.a(b2 - b3), this.j)));
            }
        }
        this.C = c;
        this.B = b2;
        this.D = c2;
        return b2;
    }
    
    public void a() {
        ((i)com.applovin.exoplayer2.l.a.b((Object)this.f)).d();
    }
    
    public void a(final float j) {
        this.j = j;
        final i f = this.f;
        if (f != null) {
            f.d();
        }
    }
    
    public void a(final AudioTrack c, final boolean b, final int n, final int d, final int e) {
        this.c = c;
        this.d = d;
        this.e = e;
        this.f = new i(c);
        this.g = c.getSampleRate();
        this.h = (b && a(n));
        final boolean d2 = ai.d(n);
        this.q = d2;
        long h;
        if (d2) {
            h = this.h(e / d);
        }
        else {
            h = -9223372036854775807L;
        }
        this.i = h;
        this.s = 0L;
        this.t = 0L;
        this.u = 0L;
        this.p = false;
        this.x = -9223372036854775807L;
        this.y = -9223372036854775807L;
        this.r = 0L;
        this.o = 0L;
        this.j = 1.0f;
    }
    
    public boolean a(final long n) {
        final int playState = ((AudioTrack)com.applovin.exoplayer2.l.a.b((Object)this.c)).getPlayState();
        if (this.h) {
            if (playState == 2) {
                return this.p = false;
            }
            if (playState == 1 && this.i() == 0L) {
                return false;
            }
        }
        final boolean p = this.p;
        final boolean f = this.f(n);
        this.p = f;
        if (p && !f && playState != 1) {
            this.a.a(this.e, com.applovin.exoplayer2.h.a(this.i));
        }
        return true;
    }
    
    public int b(final long n) {
        return this.e - (int)(n - this.i() * this.d);
    }
    
    public boolean b() {
        return ((AudioTrack)com.applovin.exoplayer2.l.a.b((Object)this.c)).getPlayState() == 3;
    }
    
    public long c(final long n) {
        return com.applovin.exoplayer2.h.a(this.h(n - this.i()));
    }
    
    public boolean c() {
        this.f();
        if (this.x == -9223372036854775807L) {
            ((i)com.applovin.exoplayer2.l.a.b((Object)this.f)).d();
            return true;
        }
        return false;
    }
    
    public void d() {
        this.f();
        this.c = null;
        this.f = null;
    }
    
    public boolean d(final long n) {
        return this.y != -9223372036854775807L && n > 0L && SystemClock.elapsedRealtime() - this.y >= 200L;
    }
    
    public void e(final long a) {
        this.z = this.i();
        this.x = SystemClock.elapsedRealtime() * 1000L;
        this.A = a;
    }
    
    public boolean f(final long n) {
        return n > this.i() || this.g();
    }
    
    public interface a
    {
        void a(final int p0, final long p1);
        
        void a(final long p0);
        
        void a(final long p0, final long p1, final long p2, final long p3);
        
        void b(final long p0);
        
        void b(final long p0, final long p1, final long p2, final long p3);
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.applovin.exoplayer2.b;

import java.util.ArrayList;
import java.util.List;
import java.nio.ByteOrder;
import java.nio.ByteBuffer;

public class s
{
    public static int a(final byte[] array) {
        return array[9] & 0xFF;
    }
    
    private static byte[] a(final long n) {
        return ByteBuffer.allocate(8).order(ByteOrder.nativeOrder()).putLong(n).array();
    }
    
    private static long b(final long n) {
        return n * 1000000000L / 48000L;
    }
    
    public static List<byte[]> b(final byte[] array) {
        final long b = b(c(array));
        final long b2 = b(3840L);
        final ArrayList list = new ArrayList(3);
        list.add(array);
        list.add(a(b));
        list.add(a(b2));
        return list;
    }
    
    private static int c(final byte[] array) {
        return (array[10] & 0xFF) | (array[11] & 0xFF) << 8;
    }
}

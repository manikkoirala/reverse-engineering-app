// 
// Decompiled by Procyon v0.6.0
// 

package com.applovin.exoplayer2.b;

import com.applovin.exoplayer2.l.ai;
import java.nio.ByteBuffer;

final class t extends l
{
    @Override
    public void a(final ByteBuffer byteBuffer) {
        int i = byteBuffer.position();
        final int limit = byteBuffer.limit();
        final int n = limit - i;
        final int d = super.b.d;
        int n2 = n;
        int n3 = 0;
        Label_0096: {
            Label_0092: {
                if (d != 3) {
                    if (d != 4) {
                        n3 = n;
                        if (d == 268435456) {
                            break Label_0096;
                        }
                        if (d == 536870912) {
                            n2 = n / 3;
                            break Label_0092;
                        }
                        if (d != 805306368) {
                            throw new IllegalStateException();
                        }
                    }
                    n3 = n / 2;
                    break Label_0096;
                }
            }
            n3 = n2 * 2;
        }
        final ByteBuffer a = this.a(n3);
        final int d2 = super.b.d;
        int j = i;
        if (d2 != 3) {
            int k = i;
            if (d2 != 4) {
                int l = i;
                if (d2 != 268435456) {
                    int n4 = i;
                    if (d2 != 536870912) {
                        if (d2 != 805306368) {
                            throw new IllegalStateException();
                        }
                        while (i < limit) {
                            a.put(byteBuffer.get(i + 2));
                            a.put(byteBuffer.get(i + 3));
                            i += 4;
                        }
                    }
                    else {
                        while (n4 < limit) {
                            a.put(byteBuffer.get(n4 + 1));
                            a.put(byteBuffer.get(n4 + 2));
                            n4 += 3;
                        }
                    }
                }
                else {
                    while (l < limit) {
                        a.put(byteBuffer.get(l + 1));
                        a.put(byteBuffer.get(l));
                        l += 2;
                    }
                }
            }
            else {
                while (k < limit) {
                    final short n5 = (short)(ai.a(byteBuffer.getFloat(k), -1.0f, 1.0f) * 32767.0f);
                    a.put((byte)(n5 & 0xFF));
                    a.put((byte)(n5 >> 8 & 0xFF));
                    k += 4;
                }
            }
        }
        else {
            while (j < limit) {
                a.put((byte)0);
                a.put((byte)((byteBuffer.get(j) & 0xFF) - 128));
                ++j;
            }
        }
        byteBuffer.position(byteBuffer.limit());
        a.flip();
    }
    
    public a b(a a) throws b {
        final int d = a.d;
        if (d != 3 && d != 2 && d != 268435456 && d != 536870912 && d != 805306368 && d != 4) {
            throw new f.b(a);
        }
        if (d != 2) {
            a = new f.a(a.b, a.c, 2);
        }
        else {
            a = com.applovin.exoplayer2.b.f.a.a;
        }
        return a;
    }
}

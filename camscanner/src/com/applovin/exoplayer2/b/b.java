// 
// Decompiled by Procyon v0.6.0
// 

package com.applovin.exoplayer2.b;

import com.applovin.exoplayer2.l.ai;
import com.applovin.exoplayer2.v;
import androidx.annotation.Nullable;
import com.applovin.exoplayer2.d.e;
import com.applovin.exoplayer2.l.y;
import com.applovin.exoplayer2.l.x;
import java.nio.ByteBuffer;

public final class b
{
    private static final int[] a;
    private static final int[] b;
    private static final int[] c;
    private static final int[] d;
    private static final int[] e;
    private static final int[] f;
    
    static {
        a = new int[] { 1, 2, 3, 6 };
        b = new int[] { 48000, 44100, 32000 };
        c = new int[] { 24000, 22050, 16000 };
        d = new int[] { 2, 1, 2, 3, 3, 4, 4, 5 };
        e = new int[] { 32, 40, 48, 56, 64, 80, 96, 112, 128, 160, 192, 224, 256, 320, 384, 448, 512, 576, 640 };
        f = new int[] { 69, 87, 104, 121, 139, 174, 208, 243, 278, 348, 417, 487, 557, 696, 835, 975, 1114, 1253, 1393 };
    }
    
    private static int a(int n, int n2) {
        final int n3 = n2 / 2;
        if (n >= 0) {
            final int[] b = com.applovin.exoplayer2.b.b.b;
            if (n < b.length && n2 >= 0) {
                final int[] f = com.applovin.exoplayer2.b.b.f;
                if (n3 < f.length) {
                    n = b[n];
                    if (n == 44100) {
                        return (f[n3] + n2 % 2) * 2;
                    }
                    n2 = com.applovin.exoplayer2.b.b.e[n3];
                    if (n == 32000) {
                        return n2 * 6;
                    }
                    return n2 * 4;
                }
            }
        }
        return -1;
    }
    
    public static int a(final ByteBuffer byteBuffer) {
        final byte value = byteBuffer.get(byteBuffer.position() + 5);
        final int n = 3;
        if ((value & 0xF8) >> 3 > 10) {
            int n2;
            if ((byteBuffer.get(byteBuffer.position() + 4) & 0xC0) >> 6 == 3) {
                n2 = n;
            }
            else {
                n2 = (byteBuffer.get(byteBuffer.position() + 4) & 0x30) >> 4;
            }
            return com.applovin.exoplayer2.b.b.a[n2] * 256;
        }
        return 1536;
    }
    
    public static int a(final ByteBuffer byteBuffer, final int n) {
        final boolean b = (byteBuffer.get(byteBuffer.position() + n + 7) & 0xFF) == 0xBB;
        final int position = byteBuffer.position();
        int n2;
        if (b) {
            n2 = 9;
        }
        else {
            n2 = 8;
        }
        return 40 << (byteBuffer.get(position + n + n2) >> 4 & 0x7);
    }
    
    public static int a(final byte[] array) {
        if (array.length < 6) {
            return -1;
        }
        if ((array[5] & 0xF8) >> 3 > 10) {
            return (((array[3] & 0xFF) | (array[2] & 0x7) << 8) + 1) * 2;
        }
        final byte b = array[4];
        return a((b & 0xC0) >> 6, b & 0x3F);
    }
    
    public static a a(final x x) {
        final int b = x.b();
        x.b(40);
        final boolean b2 = x.c(5) > 10;
        x.a(b);
        final int n = -1;
        int n2;
        int n3;
        String s;
        int n6;
        int n7;
        int n8;
        if (b2) {
            x.b(16);
            final int c = x.c(2);
            if (c != 0) {
                if (c != 1) {
                    if (c != 2) {
                        n2 = n;
                    }
                    else {
                        n2 = 2;
                    }
                }
                else {
                    n2 = 1;
                }
            }
            else {
                n2 = 0;
            }
            x.b(3);
            final int c2 = x.c(11);
            final int c3 = x.c(2);
            int c4;
            int n4;
            if (c3 == 3) {
                n3 = com.applovin.exoplayer2.b.b.c[x.c(2)];
                c4 = 3;
                n4 = 6;
            }
            else {
                c4 = x.c(2);
                n4 = com.applovin.exoplayer2.b.b.a[c4];
                n3 = com.applovin.exoplayer2.b.b.b[c3];
            }
            final int c5 = x.c(3);
            final int e = x.e() ? 1 : 0;
            final int n5 = com.applovin.exoplayer2.b.b.d[c5];
            x.b(10);
            if (x.e()) {
                x.b(8);
            }
            if (c5 == 0) {
                x.b(5);
                if (x.e()) {
                    x.b(8);
                }
            }
            if (n2 == 1 && x.e()) {
                x.b(16);
            }
            if (x.e()) {
                if (c5 > 2) {
                    x.b(2);
                }
                if ((c5 & 0x1) != 0x0 && c5 > 2) {
                    x.b(6);
                }
                if ((c5 & 0x4) != 0x0) {
                    x.b(6);
                }
                if (e != 0 && x.e()) {
                    x.b(5);
                }
                if (n2 == 0) {
                    if (x.e()) {
                        x.b(6);
                    }
                    if (c5 == 0 && x.e()) {
                        x.b(6);
                    }
                    if (x.e()) {
                        x.b(6);
                    }
                    final int c6 = x.c(2);
                    if (c6 == 1) {
                        x.b(5);
                    }
                    else if (c6 == 2) {
                        x.b(12);
                    }
                    else if (c6 == 3) {
                        final int c7 = x.c(5);
                        if (x.e()) {
                            x.b(5);
                            if (x.e()) {
                                x.b(4);
                            }
                            if (x.e()) {
                                x.b(4);
                            }
                            if (x.e()) {
                                x.b(4);
                            }
                            if (x.e()) {
                                x.b(4);
                            }
                            if (x.e()) {
                                x.b(4);
                            }
                            if (x.e()) {
                                x.b(4);
                            }
                            if (x.e()) {
                                x.b(4);
                            }
                            if (x.e()) {
                                if (x.e()) {
                                    x.b(4);
                                }
                                if (x.e()) {
                                    x.b(4);
                                }
                            }
                        }
                        if (x.e()) {
                            x.b(5);
                            if (x.e()) {
                                x.b(7);
                                if (x.e()) {
                                    x.b(8);
                                }
                            }
                        }
                        x.b((c7 + 2) * 8);
                        x.f();
                    }
                    if (c5 < 2) {
                        if (x.e()) {
                            x.b(14);
                        }
                        if (c5 == 0 && x.e()) {
                            x.b(14);
                        }
                    }
                    if (x.e()) {
                        if (c4 == 0) {
                            x.b(5);
                        }
                        else {
                            for (int i = 0; i < n4; ++i) {
                                if (x.e()) {
                                    x.b(5);
                                }
                            }
                        }
                    }
                }
            }
            if (x.e()) {
                x.b(5);
                if (c5 == 2) {
                    x.b(4);
                }
                if (c5 >= 6) {
                    x.b(2);
                }
                if (x.e()) {
                    x.b(8);
                }
                if (c5 == 0 && x.e()) {
                    x.b(8);
                }
                if (c3 < 3) {
                    x.d();
                }
            }
            if (n2 == 0 && c4 != 3) {
                x.d();
            }
            if (n2 == 2 && (c4 == 3 || x.e())) {
                x.b(6);
            }
            if (x.e() && x.c(6) == 1 && x.c(8) == 1) {
                s = "audio/eac3-joc";
            }
            else {
                s = "audio/eac3";
            }
            n6 = n4 * 256;
            n7 = (c2 + 1) * 2;
            n8 = n5 + e;
        }
        else {
            x.b(32);
            final int c8 = x.c(2);
            String s2;
            if (c8 == 3) {
                s2 = null;
            }
            else {
                s2 = "audio/ac3";
            }
            final int a = a(c8, x.c(6));
            x.b(8);
            final int c9 = x.c(3);
            if ((c9 & 0x1) != 0x0 && c9 != 1) {
                x.b(2);
            }
            if ((c9 & 0x4) != 0x0) {
                x.b(2);
            }
            if (c9 == 2) {
                x.b(2);
            }
            final int[] b3 = com.applovin.exoplayer2.b.b.b;
            int n9;
            if (c8 < b3.length) {
                n9 = b3[c8];
            }
            else {
                n9 = -1;
            }
            n8 = com.applovin.exoplayer2.b.b.d[c9] + (x.e() ? 1 : 0);
            final int n10 = -1;
            n6 = 1536;
            n7 = a;
            n3 = n9;
            n2 = n10;
            s = s2;
        }
        return new a(s, n2, n8, n3, n7, n6);
    }
    
    public static v a(final y y, final String s, final String s2, @Nullable final e e) {
        final int n = com.applovin.exoplayer2.b.b.b[(y.h() & 0xC0) >> 6];
        final int h = y.h();
        int n2 = com.applovin.exoplayer2.b.b.d[(h & 0x38) >> 3];
        if ((h & 0x4) != 0x0) {
            ++n2;
        }
        return new v.a().a(s).f("audio/ac3").k(n2).l(n).a(e).c(s2).a();
    }
    
    public static int b(final ByteBuffer byteBuffer) {
        final int position = byteBuffer.position();
        for (int limit = byteBuffer.limit(), i = position; i <= limit - 10; ++i) {
            if ((ai.a(byteBuffer, i + 4) & 0xFFFFFFFE) == 0xF8726FBA) {
                return i - position;
            }
        }
        return -1;
    }
    
    public static int b(final byte[] array) {
        final byte b = array[4];
        boolean b2 = false;
        if (b == -8 && array[5] == 114 && array[6] == 111) {
            final byte b3 = array[7];
            if ((b3 & 0xFE) == 0xBA) {
                if ((b3 & 0xFF) == 0xBB) {
                    b2 = true;
                }
                int n;
                if (b2) {
                    n = 9;
                }
                else {
                    n = 8;
                }
                return 40 << (array[n] >> 4 & 0x7);
            }
        }
        return 0;
    }
    
    public static v b(final y y, final String s, final String s2, @Nullable final e e) {
        y.e(2);
        final int n = com.applovin.exoplayer2.b.b.b[(y.h() & 0xC0) >> 6];
        final int h = y.h();
        int n3;
        final int n2 = n3 = com.applovin.exoplayer2.b.b.d[(h & 0xE) >> 1];
        if ((h & 0x1) != 0x0) {
            n3 = n2 + 1;
        }
        int n4 = n3;
        if ((y.h() & 0x1E) >> 1 > 0) {
            n4 = n3;
            if ((0x2 & y.h()) != 0x0) {
                n4 = n3 + 2;
            }
        }
        String s3;
        if (y.a() > 0 && (y.h() & 0x1) != 0x0) {
            s3 = "audio/eac3-joc";
        }
        else {
            s3 = "audio/eac3";
        }
        return new v.a().a(s).f(s3).k(n4).l(n).a(e).c(s2).a();
    }
    
    public static final class a
    {
        @Nullable
        public final String a;
        public final int b;
        public final int c;
        public final int d;
        public final int e;
        public final int f;
        
        private a(@Nullable final String a, final int b, final int d, final int c, final int e, final int f) {
            this.a = a;
            this.b = b;
            this.d = d;
            this.c = c;
            this.e = e;
            this.f = f;
        }
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.applovin.exoplayer2.b;

import androidx.annotation.CallSuper;
import java.nio.ByteOrder;
import java.nio.ByteBuffer;

public abstract class l implements f
{
    protected a b;
    protected a c;
    private a d;
    private a e;
    private ByteBuffer f;
    private ByteBuffer g;
    private boolean h;
    
    public l() {
        final ByteBuffer a = com.applovin.exoplayer2.b.f.a;
        this.f = a;
        this.g = a;
        final a a2 = com.applovin.exoplayer2.b.f.a.a;
        this.d = a2;
        this.e = a2;
        this.b = a2;
        this.c = a2;
    }
    
    @Override
    public final a a(a d) throws b {
        this.d = d;
        this.e = this.b(d);
        if (this.a()) {
            d = this.e;
        }
        else {
            d = com.applovin.exoplayer2.b.f.a.a;
        }
        return d;
    }
    
    protected final ByteBuffer a(final int capacity) {
        if (this.f.capacity() < capacity) {
            this.f = ByteBuffer.allocateDirect(capacity).order(ByteOrder.nativeOrder());
        }
        else {
            this.f.clear();
        }
        return this.g = this.f;
    }
    
    @Override
    public boolean a() {
        return this.e != com.applovin.exoplayer2.b.f.a.a;
    }
    
    protected a b(final a a) throws b {
        return com.applovin.exoplayer2.b.f.a.a;
    }
    
    @Override
    public final void b() {
        this.h = true;
        this.h();
    }
    
    @CallSuper
    @Override
    public ByteBuffer c() {
        final ByteBuffer g = this.g;
        this.g = com.applovin.exoplayer2.b.f.a;
        return g;
    }
    
    @CallSuper
    @Override
    public boolean d() {
        return this.h && this.g == com.applovin.exoplayer2.b.f.a;
    }
    
    @Override
    public final void e() {
        this.g = com.applovin.exoplayer2.b.f.a;
        this.h = false;
        this.b = this.d;
        this.c = this.e;
        this.i();
    }
    
    @Override
    public final void f() {
        this.e();
        this.f = com.applovin.exoplayer2.b.f.a;
        final a a = com.applovin.exoplayer2.b.f.a.a;
        this.d = a;
        this.e = a;
        this.b = a;
        this.c = a;
        this.j();
    }
    
    protected final boolean g() {
        return this.g.hasRemaining();
    }
    
    protected void h() {
    }
    
    protected void i() {
    }
    
    protected void j() {
    }
}

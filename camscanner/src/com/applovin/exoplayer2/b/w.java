// 
// Decompiled by Procyon v0.6.0
// 

package com.applovin.exoplayer2.b;

import java.nio.ByteOrder;
import com.applovin.exoplayer2.l.ai;
import com.applovin.exoplayer2.l.a;
import java.nio.ShortBuffer;
import java.nio.ByteBuffer;
import androidx.annotation.Nullable;

public final class w implements f
{
    private int b;
    private float c;
    private float d;
    private a e;
    private a f;
    private a g;
    private a h;
    private boolean i;
    @Nullable
    private v j;
    private ByteBuffer k;
    private ShortBuffer l;
    private ByteBuffer m;
    private long n;
    private long o;
    private boolean p;
    
    public w() {
        this.c = 1.0f;
        this.d = 1.0f;
        final a a = com.applovin.exoplayer2.b.f.a.a;
        this.e = a;
        this.f = a;
        this.g = a;
        this.h = a;
        final ByteBuffer a2 = com.applovin.exoplayer2.b.f.a;
        this.k = a2;
        this.l = a2.asShortBuffer();
        this.m = a2;
        this.b = -1;
    }
    
    public long a(long n) {
        if (this.o >= 1024L) {
            final long n2 = this.n - ((v)com.applovin.exoplayer2.l.a.b((Object)this.j)).a();
            final int b = this.h.b;
            final int b2 = this.g.b;
            if (b == b2) {
                n = ai.d(n, n2, this.o);
            }
            else {
                n = ai.d(n, n2 * b, this.o * b2);
            }
            return n;
        }
        return (long)(this.c * (double)n);
    }
    
    @Override
    public a a(final a e) throws b {
        if (e.d == 2) {
            int n;
            if ((n = this.b) == -1) {
                n = e.b;
            }
            this.e = e;
            final a f = new a(n, e.c, 2);
            this.f = f;
            this.i = true;
            return f;
        }
        throw new b(e);
    }
    
    public void a(final float c) {
        if (this.c != c) {
            this.c = c;
            this.i = true;
        }
    }
    
    @Override
    public void a(final ByteBuffer byteBuffer) {
        if (!byteBuffer.hasRemaining()) {
            return;
        }
        final v v = (v)com.applovin.exoplayer2.l.a.b((Object)this.j);
        final ShortBuffer shortBuffer = byteBuffer.asShortBuffer();
        final int remaining = byteBuffer.remaining();
        this.n += remaining;
        v.a(shortBuffer);
        byteBuffer.position(byteBuffer.position() + remaining);
    }
    
    @Override
    public boolean a() {
        return this.f.b != -1 && (Math.abs(this.c - 1.0f) >= 1.0E-4f || Math.abs(this.d - 1.0f) >= 1.0E-4f || this.f.b != this.e.b);
    }
    
    @Override
    public void b() {
        final v j = this.j;
        if (j != null) {
            j.b();
        }
        this.p = true;
    }
    
    public void b(final float d) {
        if (this.d != d) {
            this.d = d;
            this.i = true;
        }
    }
    
    @Override
    public ByteBuffer c() {
        final v j = this.j;
        if (j != null) {
            final int d = j.d();
            if (d > 0) {
                if (this.k.capacity() < d) {
                    final ByteBuffer order = ByteBuffer.allocateDirect(d).order(ByteOrder.nativeOrder());
                    this.k = order;
                    this.l = order.asShortBuffer();
                }
                else {
                    this.k.clear();
                    this.l.clear();
                }
                j.b(this.l);
                this.o += d;
                this.k.limit(d);
                this.m = this.k;
            }
        }
        final ByteBuffer m = this.m;
        this.m = com.applovin.exoplayer2.b.f.a;
        return m;
    }
    
    @Override
    public boolean d() {
        if (this.p) {
            final v j = this.j;
            if (j == null || j.d() == 0) {
                return true;
            }
        }
        return false;
    }
    
    @Override
    public void e() {
        if (this.a()) {
            final a e = this.e;
            this.g = e;
            final a f = this.f;
            this.h = f;
            if (this.i) {
                this.j = new v(e.b, e.c, this.c, this.d, f.b);
            }
            else {
                final v j = this.j;
                if (j != null) {
                    j.c();
                }
            }
        }
        this.m = com.applovin.exoplayer2.b.f.a;
        this.n = 0L;
        this.o = 0L;
        this.p = false;
    }
    
    @Override
    public void f() {
        this.c = 1.0f;
        this.d = 1.0f;
        final a a = com.applovin.exoplayer2.b.f.a.a;
        this.e = a;
        this.f = a;
        this.g = a;
        this.h = a;
        final ByteBuffer a2 = com.applovin.exoplayer2.b.f.a;
        this.k = a2;
        this.l = a2.asShortBuffer();
        this.m = a2;
        this.b = -1;
        this.i = false;
        this.j = null;
        this.n = 0L;
        this.o = 0L;
        this.p = false;
    }
}

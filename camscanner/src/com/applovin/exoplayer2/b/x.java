// 
// Decompiled by Procyon v0.6.0
// 

package com.applovin.exoplayer2.b;

import java.nio.ByteBuffer;
import com.applovin.exoplayer2.l.ai;

final class x extends l
{
    private int d;
    private int e;
    private boolean f;
    private int g;
    private byte[] h;
    private int i;
    private long j;
    
    public x() {
        this.h = ai.f;
    }
    
    public void a(final int d, final int e) {
        this.d = d;
        this.e = e;
    }
    
    @Override
    public void a(final ByteBuffer src) {
        final int position = src.position();
        final int limit = src.limit();
        final int a = limit - position;
        if (a == 0) {
            return;
        }
        final int min = Math.min(a, this.g);
        this.j += min / super.b.e;
        this.g -= min;
        src.position(position + min);
        if (this.g > 0) {
            return;
        }
        final int n = a - min;
        final int n2 = this.i + n - this.h.length;
        final ByteBuffer a2 = this.a(n2);
        final int a3 = ai.a(n2, 0, this.i);
        a2.put(this.h, 0, a3);
        final int a4 = ai.a(n2 - a3, 0, n);
        src.limit(src.position() + a4);
        a2.put(src);
        src.limit(limit);
        final int length = n - a4;
        final int i = this.i - a3;
        this.i = i;
        final byte[] h = this.h;
        System.arraycopy(h, a3, h, 0, i);
        src.get(this.h, this.i, length);
        this.i += length;
        a2.flip();
    }
    
    public a b(final a a) throws b {
        if (a.d == 2) {
            this.f = true;
            Object a2 = a;
            if (this.d == 0) {
                if (this.e != 0) {
                    a2 = a;
                }
                else {
                    a2 = com.applovin.exoplayer2.b.f.a.a;
                }
            }
            return (a)a2;
        }
        throw new f.b(a);
    }
    
    @Override
    public ByteBuffer c() {
        if (super.d()) {
            final int i = this.i;
            if (i > 0) {
                this.a(i).put(this.h, 0, this.i).flip();
                this.i = 0;
            }
        }
        return super.c();
    }
    
    @Override
    public boolean d() {
        return super.d() && this.i == 0;
    }
    
    @Override
    protected void h() {
        if (this.f) {
            final int i = this.i;
            if (i > 0) {
                this.j += i / super.b.e;
            }
            this.i = 0;
        }
    }
    
    @Override
    protected void i() {
        if (this.f) {
            this.f = false;
            final int e = this.e;
            final int e2 = super.b.e;
            this.h = new byte[e * e2];
            this.g = this.d * e2;
        }
        this.i = 0;
    }
    
    @Override
    protected void j() {
        this.h = ai.f;
    }
    
    public void k() {
        this.j = 0L;
    }
    
    public long l() {
        return this.j;
    }
}

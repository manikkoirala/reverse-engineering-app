// 
// Decompiled by Procyon v0.6.0
// 

package com.applovin.exoplayer2.b;

import java.nio.ByteBuffer;
import androidx.annotation.Nullable;
import com.applovin.exoplayer2.v;
import com.applovin.exoplayer2.am;

public interface h
{
    long a(final boolean p0);
    
    void a();
    
    void a(final float p0);
    
    void a(final int p0);
    
    void a(final am p0);
    
    void a(final com.applovin.exoplayer2.b.d p0);
    
    void a(final c p0);
    
    void a(final k p0);
    
    void a(final v p0, final int p1, @Nullable final int[] p2) throws a;
    
    boolean a(final v p0);
    
    boolean a(final ByteBuffer p0, final long p1, final int p2) throws b, e;
    
    int b(final v p0);
    
    void b();
    
    void b(final boolean p0);
    
    void c() throws e;
    
    boolean d();
    
    boolean e();
    
    am f();
    
    void g();
    
    void h();
    
    void i();
    
    void j();
    
    void k();
    
    void l();
    
    public static final class e extends Exception
    {
        public final int a;
        public final boolean b;
        public final v c;
        
        public e(final int n, final v c, final boolean b) {
            final StringBuilder sb = new StringBuilder();
            sb.append("AudioTrack write failed: ");
            sb.append(n);
            super(sb.toString());
            this.b = b;
            this.a = n;
            this.c = c;
        }
    }
    
    public static final class b extends Exception
    {
        public final int a;
        public final boolean b;
        public final v c;
        
        public b(final int n, final int i, final int j, final int k, final v c, final boolean b, @Nullable final Exception cause) {
            final StringBuilder sb = new StringBuilder();
            sb.append("AudioTrack init failed ");
            sb.append(n);
            sb.append(" ");
            sb.append("Config(");
            sb.append(i);
            sb.append(", ");
            sb.append(j);
            sb.append(", ");
            sb.append(k);
            sb.append(")");
            String str;
            if (b) {
                str = " (recoverable)";
            }
            else {
                str = "";
            }
            sb.append(str);
            super(sb.toString(), cause);
            this.a = n;
            this.b = b;
            this.c = c;
        }
    }
    
    public static final class a extends Exception
    {
        public final v a;
        
        public a(final String message, final v a) {
            super(message);
            this.a = a;
        }
        
        public a(final Throwable cause, final v a) {
            super(cause);
            this.a = a;
        }
    }
    
    public interface c
    {
        void a();
        
        void a(final int p0, final long p1, final long p2);
        
        void a(final long p0);
        
        void a(final Exception p0);
        
        void a(final boolean p0);
        
        void b();
        
        void b(final long p0);
    }
    
    public static final class d extends Exception
    {
        public final long a;
        public final long b;
        
        public d(final long n, final long n2) {
            final StringBuilder sb = new StringBuilder();
            sb.append("Unexpected audio track timestamp discontinuity: expected ");
            sb.append(n2);
            sb.append(", got ");
            sb.append(n);
            super(sb.toString());
            this.a = n;
            this.b = n2;
        }
    }
}

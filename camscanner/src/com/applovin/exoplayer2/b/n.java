// 
// Decompiled by Procyon v0.6.0
// 

package com.applovin.exoplayer2.b;

import java.util.concurrent.Executor;
import androidx.emoji2.text.\u3007o00\u3007\u3007Oo;
import java.util.Objects;
import androidx.annotation.NonNull;
import android.media.AudioTrack$StreamEventCallback;
import android.os.Handler;
import android.media.AudioTrack$Builder;
import b.a.a.a.a.a.b.e.O8;
import android.media.PlaybackParams;
import com.applovin.exoplayer2.l.q;
import android.util.Pair;
import com.applovin.exoplayer2.l.u;
import com.applovin.exoplayer2.v;
import android.os.SystemClock;
import java.nio.ByteOrder;
import android.media.AudioFormat;
import androidx.annotation.RequiresApi;
import android.media.AudioAttributes;
import android.media.AudioFormat$Builder;
import android.media.AudioAttributes$Builder;
import java.util.Collection;
import java.util.Collections;
import java.util.ArrayList;
import com.applovin.exoplayer2.l.ai;
import com.applovin.exoplayer2.l.a;
import com.applovin.exoplayer2.am;
import android.media.AudioTrack;
import java.util.ArrayDeque;
import android.os.ConditionVariable;
import androidx.annotation.Nullable;
import java.nio.ByteBuffer;

public final class n implements com.applovin.exoplayer2.b.h
{
    public static boolean a = false;
    private long A;
    private long B;
    private long C;
    private long D;
    private int E;
    private boolean F;
    private boolean G;
    private long H;
    private float I;
    private com.applovin.exoplayer2.b.f[] J;
    private ByteBuffer[] K;
    @Nullable
    private ByteBuffer L;
    private int M;
    @Nullable
    private ByteBuffer N;
    private byte[] O;
    private int P;
    private int Q;
    private boolean R;
    private boolean S;
    private boolean T;
    private boolean U;
    private int V;
    private k W;
    private boolean X;
    private long Y;
    private boolean Z;
    private boolean aa;
    @Nullable
    private final com.applovin.exoplayer2.b.e b;
    private final a c;
    private final boolean d;
    private final m e;
    private final x f;
    private final com.applovin.exoplayer2.b.f[] g;
    private final com.applovin.exoplayer2.b.f[] h;
    private final ConditionVariable i;
    private final j j;
    private final ArrayDeque<e> k;
    private final boolean l;
    private final int m;
    private h n;
    private final f<com.applovin.exoplayer2.b.h.b> o;
    private final f<com.applovin.exoplayer2.b.h.e> p;
    @Nullable
    private com.applovin.exoplayer2.b.h.c q;
    @Nullable
    private b r;
    private b s;
    @Nullable
    private AudioTrack t;
    private com.applovin.exoplayer2.b.d u;
    @Nullable
    private e v;
    private e w;
    private am x;
    @Nullable
    private ByteBuffer y;
    private int z;
    
    public n(@Nullable final com.applovin.exoplayer2.b.e b, final a a, final boolean b2, final boolean b3, int m) {
        this.b = b;
        this.c = (a)com.applovin.exoplayer2.l.a.b((Object)a);
        final int a2 = ai.a;
        this.d = (a2 >= 21 && b2);
        this.l = (a2 >= 23 && b3);
        if (a2 < 29) {
            m = 0;
        }
        this.m = m;
        this.i = new ConditionVariable(true);
        this.j = new j((j.a)new g());
        final m e = new m();
        this.e = e;
        final x f = new x();
        this.f = f;
        final ArrayList<Object> list = new ArrayList<Object>();
        Collections.addAll(list, new t(), e, f);
        Collections.addAll(list, a.a());
        this.g = list.toArray(new com.applovin.exoplayer2.b.f[0]);
        this.h = new com.applovin.exoplayer2.b.f[] { new p() };
        this.I = 1.0f;
        this.u = com.applovin.exoplayer2.b.d.a;
        this.V = 0;
        this.W = new k(0, 0.0f);
        final am a3 = am.a;
        this.w = new e(a3, false, 0L, 0L);
        this.x = a3;
        this.Q = -1;
        this.J = new com.applovin.exoplayer2.b.f[0];
        this.K = new ByteBuffer[0];
        this.k = new ArrayDeque<e>();
        this.o = new f<com.applovin.exoplayer2.b.h.b>(100L);
        this.p = new f<com.applovin.exoplayer2.b.h.e>(100L);
    }
    
    private long A() {
        final b s = this.s;
        long d;
        if (s.c == 0) {
            d = this.C / s.d;
        }
        else {
            d = this.D;
        }
        return d;
    }
    
    private void B() {
        if (!this.S) {
            this.S = true;
            this.j.e(this.A());
            this.t.stop();
            this.z = 0;
        }
    }
    
    @RequiresApi(29)
    private static int a(final int encoding, final int sampleRate) {
        final AudioAttributes build = new AudioAttributes$Builder().setUsage(1).setContentType(3).build();
        for (int i = 8; i > 0; --i) {
            if (\u3007o\u3007.\u3007080(new AudioFormat$Builder().setEncoding(encoding).setSampleRate(sampleRate).setChannelMask(ai.f(i)).build(), build)) {
                return i;
            }
        }
        return 0;
    }
    
    private static int a(int i, final ByteBuffer byteBuffer) {
        switch (i) {
            default: {
                final StringBuilder sb = new StringBuilder();
                sb.append("Unexpected audio encoding: ");
                sb.append(i);
                throw new IllegalStateException(sb.toString());
            }
            case 17: {
                return com.applovin.exoplayer2.b.c.a(byteBuffer);
            }
            case 16: {
                return 1024;
            }
            case 15: {
                return 512;
            }
            case 14: {
                i = com.applovin.exoplayer2.b.b.b(byteBuffer);
                if (i == -1) {
                    i = 0;
                }
                else {
                    i = com.applovin.exoplayer2.b.b.a(byteBuffer, i) * 16;
                }
                return i;
            }
            case 11:
            case 12: {
                return 2048;
            }
            case 10: {
                return 1024;
            }
            case 9: {
                i = r.b(ai.a(byteBuffer, byteBuffer.position()));
                if (i != -1) {
                    return i;
                }
                throw new IllegalArgumentException();
            }
            case 7:
            case 8: {
                return o.a(byteBuffer);
            }
            case 5:
            case 6:
            case 18: {
                return com.applovin.exoplayer2.b.b.a(byteBuffer);
            }
        }
    }
    
    private int a(final AudioFormat audioFormat, final AudioAttributes audioAttributes) {
        final int a = ai.a;
        if (a >= 31) {
            return \u30070\u3007O0088o.\u3007080(audioFormat, audioAttributes);
        }
        if (!OoO8.\u3007080(audioFormat, audioAttributes)) {
            return 0;
        }
        if (a == 30 && ai.d.startsWith("Pixel")) {
            return 2;
        }
        return 1;
    }
    
    @RequiresApi(21)
    private static int a(final AudioTrack audioTrack, final ByteBuffer byteBuffer, final int n) {
        return audioTrack.write(byteBuffer, n, 1);
    }
    
    @RequiresApi(21)
    private int a(final AudioTrack audioTrack, final ByteBuffer byteBuffer, int a, final long n) {
        if (ai.a >= 26) {
            return \u3007O888o0o.\u3007080(audioTrack, byteBuffer, a, 1, n * 1000L);
        }
        if (this.y == null) {
            (this.y = ByteBuffer.allocate(16)).order(ByteOrder.BIG_ENDIAN);
            this.y.putInt(1431633921);
        }
        if (this.z == 0) {
            this.y.putInt(4, a);
            this.y.putLong(8, n * 1000L);
            this.y.position(0);
            this.z = a;
        }
        final int remaining = this.y.remaining();
        if (remaining > 0) {
            final int write = audioTrack.write(this.y, remaining, 1);
            if (write < 0) {
                this.z = 0;
                return write;
            }
            if (write < remaining) {
                return 0;
            }
        }
        a = a(audioTrack, byteBuffer, a);
        if (a < 0) {
            this.z = 0;
            return a;
        }
        this.z -= a;
        return a;
    }
    
    private void a(final long n) throws com.applovin.exoplayer2.b.h.e {
        int i;
        final int n2 = i = this.J.length;
        while (i >= 0) {
            ByteBuffer byteBuffer;
            if (i > 0) {
                byteBuffer = this.K[i - 1];
            }
            else {
                byteBuffer = this.L;
                if (byteBuffer == null) {
                    byteBuffer = com.applovin.exoplayer2.b.f.a;
                }
            }
            if (i == n2) {
                this.a(byteBuffer, n);
            }
            else {
                final com.applovin.exoplayer2.b.f f = this.J[i];
                if (i > this.Q) {
                    f.a(byteBuffer);
                }
                final ByteBuffer c = f.c();
                this.K[i] = c;
                if (c.hasRemaining()) {
                    ++i;
                    continue;
                }
            }
            if (byteBuffer.hasRemaining()) {
                return;
            }
            --i;
        }
    }
    
    @RequiresApi(29)
    private void a(final AudioTrack audioTrack) {
        if (this.n == null) {
            this.n = new h();
        }
        this.n.a(audioTrack);
    }
    
    @RequiresApi(21)
    private static void a(final AudioTrack audioTrack, final float volume) {
        audioTrack.setVolume(volume);
    }
    
    private void a(final am am, final boolean b) {
        final e w = this.w();
        if (!am.equals(w.a) || b != w.b) {
            final e e = new e(am, b, -9223372036854775807L, -9223372036854775807L);
            if (this.y()) {
                this.v = e;
            }
            else {
                this.w = e;
            }
        }
    }
    
    private void a(final ByteBuffer n, long n2) throws com.applovin.exoplayer2.b.h.e {
        if (!n.hasRemaining()) {
            return;
        }
        final ByteBuffer n3 = this.N;
        final boolean b = true;
        if (n3 != null) {
            com.applovin.exoplayer2.l.a.a(n3 == n);
        }
        else {
            this.N = n;
            if (ai.a < 21) {
                final int remaining = n.remaining();
                final byte[] o = this.O;
                if (o == null || o.length < remaining) {
                    this.O = new byte[remaining];
                }
                final int position = n.position();
                n.get(this.O, 0, remaining);
                n.position(position);
                this.P = 0;
            }
        }
        final int remaining2 = n.remaining();
        int n4;
        if (ai.a < 21) {
            final int b2 = this.j.b(this.C);
            if (b2 > 0) {
                final int write = this.t.write(this.O, this.P, Math.min(remaining2, b2));
                if ((n4 = write) > 0) {
                    this.P += write;
                    n.position(n.position() + write);
                    n4 = write;
                }
            }
            else {
                n4 = 0;
            }
        }
        else if (this.X) {
            com.applovin.exoplayer2.l.a.b(n2 != -9223372036854775807L);
            n4 = this.a(this.t, n, remaining2, n2);
        }
        else {
            n4 = a(this.t, n, remaining2);
        }
        this.Y = SystemClock.elapsedRealtime();
        if (n4 >= 0) {
            this.p.a();
            if (b(this.t)) {
                n2 = this.D;
                if (n2 > 0L) {
                    this.aa = false;
                }
                if (this.T && this.q != null && n4 < remaining2 && !this.aa) {
                    n2 = this.j.c(n2);
                    this.q.b(n2);
                }
            }
            final int c = this.s.c;
            if (c == 0) {
                this.C += n4;
            }
            if (n4 == remaining2) {
                if (c != 0) {
                    com.applovin.exoplayer2.l.a.b(n == this.L && b);
                    this.D += this.E * this.M;
                }
                this.N = null;
            }
            return;
        }
        final boolean c2 = c(n4);
        if (c2) {
            this.r();
        }
        final com.applovin.exoplayer2.b.h.e e = new com.applovin.exoplayer2.b.h.e(n4, this.s.a, c2);
        final com.applovin.exoplayer2.b.h.c q = this.q;
        if (q != null) {
            q.a(e);
        }
        if (!e.b) {
            this.p.a(e);
            return;
        }
        throw e;
    }
    
    private boolean a(final v v, final com.applovin.exoplayer2.b.d d) {
        final int a = ai.a;
        boolean b2;
        final boolean b = b2 = false;
        if (a >= 29) {
            if (this.m == 0) {
                b2 = b;
            }
            else {
                final int b3 = com.applovin.exoplayer2.l.u.b((String)com.applovin.exoplayer2.l.a.b((Object)v.l), v.i);
                if (b3 == 0) {
                    return false;
                }
                final int f = ai.f(v.y);
                if (f == 0) {
                    return false;
                }
                final int a2 = this.a(b(v.z, f, b3), d.a());
                b2 = b;
                if (a2 != 0) {
                    if (a2 != 1) {
                        if (a2 == 2) {
                            return true;
                        }
                        throw new IllegalStateException();
                    }
                    else {
                        final boolean b4 = v.B != 0 || v.C != 0;
                        final boolean b5 = this.m == 1;
                        if (b4) {
                            b2 = b;
                            if (b5) {
                                return b2;
                            }
                        }
                        b2 = true;
                    }
                }
            }
        }
        return b2;
    }
    
    private static boolean a(final v v, @Nullable final com.applovin.exoplayer2.b.e e) {
        return b(v, e) != null;
    }
    
    @RequiresApi(21)
    private static AudioFormat b(final int sampleRate, final int channelMask, final int encoding) {
        return new AudioFormat$Builder().setSampleRate(sampleRate).setChannelMask(channelMask).setEncoding(encoding).build();
    }
    
    @Nullable
    private static Pair<Integer, Integer> b(final v v, @Nullable final com.applovin.exoplayer2.b.e e) {
        if (e == null) {
            return null;
        }
        final int b = u.b((String)com.applovin.exoplayer2.l.a.b((Object)v.l), v.i);
        int n = 6;
        if (b != 5 && b != 6 && b != 18 && b != 17 && b != 7 && b != 8 && b != 14) {
            return null;
        }
        int i;
        if (b == 18 && !e.a(18)) {
            i = 6;
        }
        else if ((i = b) == 8) {
            i = b;
            if (!e.a(8)) {
                i = 7;
            }
        }
        if (!e.a(i)) {
            return null;
        }
        if (i == 18) {
            if (ai.a >= 29 && (n = a(18, v.z)) == 0) {
                q.c("DefaultAudioSink", "E-AC3 JOC encoding supported but no channel count supported");
                return null;
            }
        }
        else if ((n = v.y) > e.a()) {
            return null;
        }
        final int e2 = e(n);
        if (e2 == 0) {
            return null;
        }
        return (Pair<Integer, Integer>)Pair.create((Object)i, (Object)e2);
    }
    
    private void b(final long b) {
        am am;
        if (this.x()) {
            am = this.c.a(this.v());
        }
        else {
            am = com.applovin.exoplayer2.am.a;
        }
        final boolean b2 = this.x() && this.c.a(this.m());
        this.k.add(new e(am, b2, Math.max(0L, b), this.s.b(this.A())));
        this.n();
        final com.applovin.exoplayer2.b.h.c q = this.q;
        if (q != null) {
            q.a(b2);
        }
    }
    
    private static void b(final AudioTrack audioTrack, final float n) {
        audioTrack.setStereoVolume(n, n);
    }
    
    @RequiresApi(23)
    private void b(final am am) {
        am x = am;
        if (this.y()) {
            final PlaybackParams \u3007080 = \u300700.\u3007080(o\u3007O8\u3007\u3007o.\u3007080(O8.\u3007080(\u3007oo\u3007.\u3007080(new PlaybackParams()), am.b), am.c), 2);
            try {
                O\u30078O8\u3007008.\u3007080(this.t, \u3007080);
            }
            catch (final IllegalArgumentException ex) {
                com.applovin.exoplayer2.l.q.b("DefaultAudioSink", "Failed to set playback params", (Throwable)ex);
            }
            x = new am(\u3007oOO8O8.\u3007080(O8ooOoo\u3007.\u3007080(this.t)), \u3007\u30078O0\u30078.\u3007080(O8ooOoo\u3007.\u3007080(this.t)));
            this.j.a(x.b);
        }
        this.x = x;
    }
    
    private static boolean b(final AudioTrack audioTrack) {
        return ai.a >= 29 && oo88o8O.\u3007080(audioTrack);
    }
    
    private long c(long n) {
        while (!this.k.isEmpty() && n >= this.k.getFirst().d) {
            this.w = this.k.remove();
        }
        final e w = this.w;
        final long n2 = n - w.d;
        if (w.a.equals(am.a)) {
            return this.w.c + n2;
        }
        if (this.k.isEmpty()) {
            n = this.c.a(n2);
            return this.w.c + n;
        }
        final e e = this.k.getFirst();
        n = ai.a(e.d - n, this.w.a.b);
        return e.c - n;
    }
    
    private static boolean c(final int n) {
        return (ai.a >= 24 && n == -6) || n == -32;
    }
    
    private long d(final long n) {
        return n + this.s.b(this.c.b());
    }
    
    private boolean d(final int n) {
        return this.d && ai.e(n);
    }
    
    private static int e(int n) {
        final int a = ai.a;
        int n2 = n;
        if (a <= 28) {
            if (n == 7) {
                n2 = 8;
            }
            else if (n == 3 || n == 4 || (n2 = n) == 5) {
                n2 = 6;
            }
        }
        n = n2;
        if (a <= 26) {
            n = n2;
            if ("fugu".equals(ai.b) && (n = n2) == 1) {
                n = 2;
            }
        }
        return ai.f(n);
    }
    
    private static int f(final int n) {
        switch (n) {
            default: {
                throw new IllegalArgumentException();
            }
            case 17: {
                return 336000;
            }
            case 16: {
                return 256000;
            }
            case 15: {
                return 8000;
            }
            case 14: {
                return 3062500;
            }
            case 12: {
                return 7000;
            }
            case 11: {
                return 16000;
            }
            case 10: {
                return 100000;
            }
            case 9: {
                return 40000;
            }
            case 8: {
                return 2250000;
            }
            case 7: {
                return 192000;
            }
            case 6:
            case 18: {
                return 768000;
            }
            case 5: {
                return 80000;
            }
        }
    }
    
    private void n() {
        final com.applovin.exoplayer2.b.f[] i = this.s.i;
        final ArrayList list = new ArrayList();
        for (final com.applovin.exoplayer2.b.f e : i) {
            if (e.a()) {
                list.add(e);
            }
            else {
                e.e();
            }
        }
        final int size = list.size();
        this.J = list.toArray(new com.applovin.exoplayer2.b.f[size]);
        this.K = new ByteBuffer[size];
        this.o();
    }
    
    private void o() {
        int n = 0;
        while (true) {
            final com.applovin.exoplayer2.b.f[] j = this.J;
            if (n >= j.length) {
                break;
            }
            final com.applovin.exoplayer2.b.f f = j[n];
            f.e();
            this.K[n] = f.c();
            ++n;
        }
    }
    
    private void p() throws com.applovin.exoplayer2.b.h.b {
        this.i.block();
        final AudioTrack q = this.q();
        this.t = q;
        if (b(q)) {
            this.a(this.t);
            if (this.m != 3) {
                final AudioTrack t = this.t;
                final v a = this.s.a;
                o800o8O.\u3007080(t, a.B, a.C);
            }
        }
        this.V = this.t.getAudioSessionId();
        final j j = this.j;
        final AudioTrack t2 = this.t;
        final b s = this.s;
        j.a(t2, s.c == 2, s.g, s.d, s.h);
        this.t();
        final int a2 = this.W.a;
        if (a2 != 0) {
            this.t.attachAuxEffect(a2);
            this.t.setAuxEffectSendLevel(this.W.b);
        }
        this.G = true;
    }
    
    private AudioTrack q() throws com.applovin.exoplayer2.b.h.b {
        try {
            return ((b)com.applovin.exoplayer2.l.a.b((Object)this.s)).a(this.X, this.u, this.V);
        }
        catch (final com.applovin.exoplayer2.b.h.b b) {
            this.r();
            final com.applovin.exoplayer2.b.h.c q = this.q;
            if (q != null) {
                q.a(b);
            }
            throw b;
        }
    }
    
    private void r() {
        if (!this.s.a()) {
            return;
        }
        this.Z = true;
    }
    
    private boolean s() throws com.applovin.exoplayer2.b.h.e {
        while (true) {
            boolean b = false;
            Label_0020: {
                if (this.Q != -1) {
                    b = false;
                    break Label_0020;
                }
                this.Q = 0;
                b = true;
            }
            final int q = this.Q;
            final com.applovin.exoplayer2.b.f[] j = this.J;
            if (q >= j.length) {
                final ByteBuffer n = this.N;
                if (n != null) {
                    this.a(n, -9223372036854775807L);
                    if (this.N != null) {
                        return false;
                    }
                }
                this.Q = -1;
                return true;
            }
            final com.applovin.exoplayer2.b.f f = j[q];
            if (b) {
                f.b();
            }
            this.a(-9223372036854775807L);
            if (!f.d()) {
                return false;
            }
            ++this.Q;
            continue;
        }
    }
    
    private void t() {
        if (this.y()) {
            if (ai.a >= 21) {
                a(this.t, this.I);
            }
            else {
                b(this.t, this.I);
            }
        }
    }
    
    private void u() {
        this.A = 0L;
        this.B = 0L;
        this.C = 0L;
        this.D = 0L;
        this.aa = false;
        this.E = 0;
        this.w = new e(this.v(), this.m(), 0L, 0L);
        this.H = 0L;
        this.v = null;
        this.k.clear();
        this.L = null;
        this.M = 0;
        this.N = null;
        this.S = false;
        this.R = false;
        this.Q = -1;
        this.y = null;
        this.z = 0;
        this.f.k();
        this.o();
    }
    
    private am v() {
        return this.w().a;
    }
    
    private e w() {
        e e = this.v;
        if (e == null) {
            if (!this.k.isEmpty()) {
                e = this.k.getLast();
            }
            else {
                e = this.w;
            }
        }
        return e;
    }
    
    private boolean x() {
        return !this.X && "audio/raw".equals(this.s.a.l) && !this.d(this.s.a.A);
    }
    
    private boolean y() {
        return this.t != null;
    }
    
    private long z() {
        final b s = this.s;
        long b;
        if (s.c == 0) {
            b = this.A / s.b;
        }
        else {
            b = this.B;
        }
        return b;
    }
    
    @Override
    public long a(final boolean b) {
        if (this.y() && !this.G) {
            return this.d(this.c(Math.min(this.j.a(b), this.s.b(this.A()))));
        }
        return Long.MIN_VALUE;
    }
    
    @Override
    public void a() {
        this.T = true;
        if (this.y()) {
            this.j.a();
            this.t.play();
        }
    }
    
    @Override
    public void a(final float i) {
        if (this.I != i) {
            this.I = i;
            this.t();
        }
    }
    
    @Override
    public void a(final int v) {
        if (this.V != v) {
            this.V = v;
            this.U = (v != 0);
            this.j();
        }
    }
    
    @Override
    public void a(am am) {
        am = new am(ai.a(am.b, 0.1f, 8.0f), ai.a(am.c, 0.1f, 8.0f));
        if (this.l && ai.a >= 23) {
            this.b(am);
        }
        else {
            this.a(am, this.m());
        }
    }
    
    @Override
    public void a(final com.applovin.exoplayer2.b.d u) {
        if (this.u.equals(u)) {
            return;
        }
        this.u = u;
        if (this.X) {
            return;
        }
        this.j();
    }
    
    @Override
    public void a(final com.applovin.exoplayer2.b.h.c q) {
        this.q = q;
    }
    
    @Override
    public void a(final k w) {
        if (this.W.equals(w)) {
            return;
        }
        final int a = w.a;
        final float b = w.b;
        final AudioTrack t = this.t;
        if (t != null) {
            if (this.W.a != a) {
                t.attachAuxEffect(a);
            }
            if (a != 0) {
                this.t.setAuxEffectSendLevel(b);
            }
        }
        this.W = w;
    }
    
    @Override
    public void a(final v obj, final int n, @Nullable int[] array) throws com.applovin.exoplayer2.b.h.a {
        int c;
        int n3;
        int n4;
        int n5;
        int c2;
        int n6;
        com.applovin.exoplayer2.b.f[] array4;
        if ("audio/raw".equals(obj.l)) {
            com.applovin.exoplayer2.l.a.a(ai.d(obj.A));
            c = ai.c(obj.A, obj.y);
            com.applovin.exoplayer2.b.f[] array2;
            if (this.d(obj.A)) {
                array2 = this.h;
            }
            else {
                array2 = this.g;
            }
            this.f.a(obj.B, obj.C);
            if (ai.a < 21 && obj.y == 8 && array == null) {
                final int[] array3 = new int[6];
                int n2 = 0;
                while (true) {
                    array = array3;
                    if (n2 >= 6) {
                        break;
                    }
                    array3[n2] = n2;
                    ++n2;
                }
            }
            this.e.a(array);
            com.applovin.exoplayer2.b.f.a a = new com.applovin.exoplayer2.b.f.a(obj.z, obj.y, obj.A);
            final int length = array2.length;
            int i = 0;
            while (i < length) {
                final com.applovin.exoplayer2.b.f f = array2[i];
                try {
                    final com.applovin.exoplayer2.b.f.a a2 = f.a(a);
                    if (f.a()) {
                        a = a2;
                    }
                    ++i;
                    continue;
                }
                catch (final com.applovin.exoplayer2.b.f.b b) {
                    throw new com.applovin.exoplayer2.b.h.a(b, obj);
                }
                break;
            }
            n3 = a.d;
            n4 = a.b;
            n5 = ai.f(a.c);
            c2 = ai.c(n3, a.c);
            n6 = 0;
            array4 = array2;
        }
        else {
            array4 = new com.applovin.exoplayer2.b.f[0];
            n4 = obj.z;
            final boolean a3 = this.a(obj, this.u);
            c2 = -1;
            if (a3) {
                n3 = com.applovin.exoplayer2.l.u.b((String)com.applovin.exoplayer2.l.a.b((Object)obj.l), obj.i);
                n5 = ai.f(obj.y);
                n6 = 1;
            }
            else {
                final Pair<Integer, Integer> b2 = b(obj, this.b);
                if (b2 == null) {
                    final StringBuilder sb = new StringBuilder();
                    sb.append("Unable to configure passthrough for: ");
                    sb.append(obj);
                    throw new com.applovin.exoplayer2.b.h.a(sb.toString(), obj);
                }
                n3 = (int)b2.first;
                n5 = (int)b2.second;
                n6 = 2;
            }
            c = -1;
        }
        if (n3 == 0) {
            final StringBuilder sb2 = new StringBuilder();
            sb2.append("Invalid output encoding (mode=");
            sb2.append(n6);
            sb2.append(") for: ");
            sb2.append(obj);
            throw new com.applovin.exoplayer2.b.h.a(sb2.toString(), obj);
        }
        if (n5 != 0) {
            this.Z = false;
            final b b3 = new b(obj, c, n6, c2, n4, n5, n3, n, this.l, array4);
            if (this.y()) {
                this.r = b3;
            }
            else {
                this.s = b3;
            }
            return;
        }
        final StringBuilder sb3 = new StringBuilder();
        sb3.append("Invalid output channel config (mode=");
        sb3.append(n6);
        sb3.append(") for: ");
        sb3.append(obj);
        throw new com.applovin.exoplayer2.b.h.a(sb3.toString(), obj);
    }
    
    @Override
    public boolean a(final v v) {
        return this.b(v) != 0;
    }
    
    @Override
    public boolean a(final ByteBuffer l, final long b, final int m) throws com.applovin.exoplayer2.b.h.b, com.applovin.exoplayer2.b.h.e {
        final ByteBuffer i = this.L;
        com.applovin.exoplayer2.l.a.a(i == null || l == i);
        if (this.r != null) {
            if (!this.s()) {
                return false;
            }
            if (!this.r.a(this.s)) {
                this.B();
                if (this.e()) {
                    return false;
                }
                this.j();
            }
            else {
                this.s = this.r;
                this.r = null;
                if (b(this.t) && this.m != 3) {
                    \u3007O00.\u3007080(this.t);
                    final AudioTrack t = this.t;
                    final v a = this.s.a;
                    o800o8O.\u3007080(t, a.B, a.C);
                    this.aa = true;
                }
            }
            this.b(b);
        }
        if (!this.y()) {
            try {
                this.p();
            }
            catch (final com.applovin.exoplayer2.b.h.b b2) {
                if (!b2.b) {
                    this.o.a(b2);
                    return false;
                }
                throw b2;
            }
        }
        this.o.a();
        if (this.G) {
            this.H = Math.max(0L, b);
            this.F = false;
            this.G = false;
            if (this.l && ai.a >= 23) {
                this.b(this.x);
            }
            this.b(b);
            if (this.T) {
                this.a();
            }
        }
        if (!this.j.a(this.A())) {
            return false;
        }
        if (this.L == null) {
            com.applovin.exoplayer2.l.a.a(l.order() == ByteOrder.LITTLE_ENDIAN);
            if (!l.hasRemaining()) {
                return true;
            }
            final b s = this.s;
            if (s.c != 0 && this.E == 0 && (this.E = a(s.g, l)) == 0) {
                return true;
            }
            if (this.v != null) {
                if (!this.s()) {
                    return false;
                }
                this.b(b);
                this.v = null;
            }
            final long n = this.H + this.s.a(this.z() - this.f.l());
            if (!this.F && Math.abs(n - b) > 200000L) {
                this.q.a(new com.applovin.exoplayer2.b.h.d(b, n));
                this.F = true;
            }
            if (this.F) {
                if (!this.s()) {
                    return false;
                }
                final long n2 = b - n;
                this.H += n2;
                this.F = false;
                this.b(b);
                final com.applovin.exoplayer2.b.h.c q = this.q;
                if (q != null && n2 != 0L) {
                    q.a();
                }
            }
            if (this.s.c == 0) {
                this.A += l.remaining();
            }
            else {
                this.B += this.E * m;
            }
            this.L = l;
            this.M = m;
        }
        this.a(b);
        if (!this.L.hasRemaining()) {
            this.L = null;
            this.M = 0;
            return true;
        }
        if (this.j.d(this.A())) {
            com.applovin.exoplayer2.l.q.c("DefaultAudioSink", "Resetting stalled audio track");
            this.j();
            return true;
        }
        return false;
    }
    
    @Override
    public int b(final v v) {
        if ("audio/raw".equals(v.l)) {
            if (!ai.d(v.A)) {
                final StringBuilder sb = new StringBuilder();
                sb.append("Invalid PCM encoding: ");
                sb.append(v.A);
                com.applovin.exoplayer2.l.q.c("DefaultAudioSink", sb.toString());
                return 0;
            }
            final int a = v.A;
            if (a != 2 && (!this.d || a != 4)) {
                return 1;
            }
            return 2;
        }
        else {
            if (!this.Z && this.a(v, this.u)) {
                return 2;
            }
            if (a(v, this.b)) {
                return 2;
            }
            return 0;
        }
    }
    
    @Override
    public void b() {
        this.F = true;
    }
    
    @Override
    public void b(final boolean b) {
        this.a(this.v(), b);
    }
    
    @Override
    public void c() throws com.applovin.exoplayer2.b.h.e {
        if (!this.R && this.y() && this.s()) {
            this.B();
            this.R = true;
        }
    }
    
    @Override
    public boolean d() {
        return !this.y() || (this.R && !this.e());
    }
    
    @Override
    public boolean e() {
        return this.y() && this.j.f(this.A());
    }
    
    @Override
    public am f() {
        am am;
        if (this.l) {
            am = this.x;
        }
        else {
            am = this.v();
        }
        return am;
    }
    
    @Override
    public void g() {
        com.applovin.exoplayer2.l.a.b(ai.a >= 21);
        com.applovin.exoplayer2.l.a.b(this.U);
        if (!this.X) {
            this.X = true;
            this.j();
        }
    }
    
    @Override
    public void h() {
        if (this.X) {
            this.X = false;
            this.j();
        }
    }
    
    @Override
    public void i() {
        this.T = false;
        if (this.y() && this.j.c()) {
            this.t.pause();
        }
    }
    
    @Override
    public void j() {
        if (this.y()) {
            this.u();
            if (this.j.b()) {
                this.t.pause();
            }
            if (b(this.t)) {
                ((h)com.applovin.exoplayer2.l.a.b((Object)this.n)).b(this.t);
            }
            final AudioTrack t = this.t;
            this.t = null;
            if (ai.a < 21 && !this.U) {
                this.V = 0;
            }
            final b r = this.r;
            if (r != null) {
                this.s = r;
                this.r = null;
            }
            this.j.d();
            this.i.close();
            new Thread(this, "ExoPlayer:AudioTrackReleaseThread", t) {
                final AudioTrack a;
                final n b;
                
                @Override
                public void run() {
                    try {
                        this.a.flush();
                        this.a.release();
                    }
                    finally {
                        this.b.i.open();
                    }
                }
            }.start();
        }
        this.p.a();
        this.o.a();
    }
    
    @Override
    public void k() {
        if (ai.a < 25) {
            this.j();
            return;
        }
        this.p.a();
        this.o.a();
        if (!this.y()) {
            return;
        }
        this.u();
        if (this.j.b()) {
            this.t.pause();
        }
        this.t.flush();
        this.j.d();
        final j j = this.j;
        final AudioTrack t = this.t;
        final b s = this.s;
        j.a(t, s.c == 2, s.g, s.d, s.h);
        this.G = true;
    }
    
    @Override
    public void l() {
        this.j();
        final com.applovin.exoplayer2.b.f[] g = this.g;
        for (int length = g.length, i = 0; i < length; ++i) {
            g[i].f();
        }
        final com.applovin.exoplayer2.b.f[] h = this.h;
        for (int length2 = h.length, j = 0; j < length2; ++j) {
            h[j].f();
        }
        this.T = false;
        this.Z = false;
    }
    
    public boolean m() {
        return this.w().b;
    }
    
    public interface a
    {
        long a(final long p0);
        
        am a(final am p0);
        
        boolean a(final boolean p0);
        
        com.applovin.exoplayer2.b.f[] a();
        
        long b();
    }
    
    private static final class b
    {
        public final v a;
        public final int b;
        public final int c;
        public final int d;
        public final int e;
        public final int f;
        public final int g;
        public final int h;
        public final com.applovin.exoplayer2.b.f[] i;
        
        public b(final v a, final int b, final int c, final int d, final int e, final int f, final int g, final int n, final boolean b2, final com.applovin.exoplayer2.b.f[] i) {
            this.a = a;
            this.b = b;
            this.c = c;
            this.d = d;
            this.e = e;
            this.f = f;
            this.g = g;
            this.i = i;
            this.h = this.a(n, b2);
        }
        
        private int a(final float n) {
            final int minBufferSize = AudioTrack.getMinBufferSize(this.e, this.f, this.g);
            com.applovin.exoplayer2.l.a.b(minBufferSize != -2);
            int n2 = ai.a(minBufferSize * 4, (int)this.c(250000L) * this.d, Math.max(minBufferSize, (int)this.c(750000L) * this.d));
            if (n != 1.0f) {
                n2 = Math.round(n2 * n);
            }
            return n2;
        }
        
        private int a(int c, final boolean b) {
            if (c != 0) {
                return c;
            }
            c = this.c;
            if (c == 0) {
                float n;
                if (b) {
                    n = 8.0f;
                }
                else {
                    n = 1.0f;
                }
                return this.a(n);
            }
            if (c == 1) {
                return this.d(50000000L);
            }
            if (c == 2) {
                return this.d(250000L);
            }
            throw new IllegalStateException();
        }
        
        @RequiresApi(21)
        private static AudioAttributes a(final com.applovin.exoplayer2.b.d d, final boolean b) {
            if (b) {
                return b();
            }
            return d.a();
        }
        
        private AudioTrack a(final com.applovin.exoplayer2.b.d d, final int n) {
            final int g = ai.g(d.d);
            if (n == 0) {
                return new AudioTrack(g, this.e, this.f, this.g, this.h, 1);
            }
            return new AudioTrack(g, this.e, this.f, this.g, this.h, 1, n);
        }
        
        @RequiresApi(21)
        private static AudioAttributes b() {
            return new AudioAttributes$Builder().setContentType(3).setFlags(16).setUsage(1).build();
        }
        
        private AudioTrack b(final boolean b, final com.applovin.exoplayer2.b.d d, final int n) {
            final int a = ai.a;
            if (a >= 29) {
                return this.c(b, d, n);
            }
            if (a >= 21) {
                return this.d(b, d, n);
            }
            return this.a(d, n);
        }
        
        @RequiresApi(29)
        private AudioTrack c(final boolean b, final com.applovin.exoplayer2.b.d d, final int n) {
            final AudioTrack$Builder \u3007080 = o\u3007\u30070\u3007.\u3007080(\u30070000OOO.\u3007080(new AudioTrack$Builder(), a(d, b)), b(this.e, this.f, this.g));
            boolean b2 = true;
            final AudioTrack$Builder \u300781 = O8\u3007o.\u3007080(oo\u3007.\u3007080(OOO\u3007O0.\u3007080(\u3007080, 1), this.h), n);
            if (this.c != 1) {
                b2 = false;
            }
            return \u3007o.\u3007080(\u300700\u30078.\u3007080(\u300781, b2));
        }
        
        private int d(final long n) {
            int b = f(this.g);
            if (this.g == 5) {
                b *= 2;
            }
            return (int)(n * b / 1000000L);
        }
        
        @RequiresApi(21)
        private AudioTrack d(final boolean b, final com.applovin.exoplayer2.b.d d, final int n) {
            return new AudioTrack(a(d, b), b(this.e, this.f, this.g), this.h, 1, n);
        }
        
        public long a(final long n) {
            return n * 1000000L / this.a.z;
        }
        
        public AudioTrack a(final boolean p0, final com.applovin.exoplayer2.b.d p1, final int p2) throws com.applovin.exoplayer2.b.h.b {
            // 
            // This method could not be decompiled.
            // 
            // Original Bytecode:
            // 
            //     1: iload_1        
            //     2: aload_2        
            //     3: iload_3        
            //     4: invokespecial   com/applovin/exoplayer2/b/n$b.b:(ZLcom/applovin/exoplayer2/b/d;I)Landroid/media/AudioTrack;
            //     7: astore_2       
            //     8: aload_2        
            //     9: invokevirtual   android/media/AudioTrack.getState:()I
            //    12: istore_3       
            //    13: iload_3        
            //    14: iconst_1       
            //    15: if_icmpne       20
            //    18: aload_2        
            //    19: areturn        
            //    20: aload_2        
            //    21: invokevirtual   android/media/AudioTrack.release:()V
            //    24: new             Lcom/applovin/exoplayer2/b/h$b;
            //    27: dup            
            //    28: iload_3        
            //    29: aload_0        
            //    30: getfield        com/applovin/exoplayer2/b/n$b.e:I
            //    33: aload_0        
            //    34: getfield        com/applovin/exoplayer2/b/n$b.f:I
            //    37: aload_0        
            //    38: getfield        com/applovin/exoplayer2/b/n$b.h:I
            //    41: aload_0        
            //    42: getfield        com/applovin/exoplayer2/b/n$b.a:Lcom/applovin/exoplayer2/v;
            //    45: aload_0        
            //    46: invokevirtual   com/applovin/exoplayer2/b/n$b.a:()Z
            //    49: aconst_null    
            //    50: invokespecial   com/applovin/exoplayer2/b/h$b.<init>:(IIIILcom/applovin/exoplayer2/v;ZLjava/lang/Exception;)V
            //    53: athrow         
            //    54: astore_2       
            //    55: goto            59
            //    58: astore_2       
            //    59: new             Lcom/applovin/exoplayer2/b/h$b;
            //    62: dup            
            //    63: iconst_0       
            //    64: aload_0        
            //    65: getfield        com/applovin/exoplayer2/b/n$b.e:I
            //    68: aload_0        
            //    69: getfield        com/applovin/exoplayer2/b/n$b.f:I
            //    72: aload_0        
            //    73: getfield        com/applovin/exoplayer2/b/n$b.h:I
            //    76: aload_0        
            //    77: getfield        com/applovin/exoplayer2/b/n$b.a:Lcom/applovin/exoplayer2/v;
            //    80: aload_0        
            //    81: invokevirtual   com/applovin/exoplayer2/b/n$b.a:()Z
            //    84: aload_2        
            //    85: invokespecial   com/applovin/exoplayer2/b/h$b.<init>:(IIIILcom/applovin/exoplayer2/v;ZLjava/lang/Exception;)V
            //    88: athrow         
            //    89: astore_2       
            //    90: goto            24
            //    Exceptions:
            //  throws com.applovin.exoplayer2.b.h.b
            //    Exceptions:
            //  Try           Handler
            //  Start  End    Start  End    Type                                     
            //  -----  -----  -----  -----  -----------------------------------------
            //  0      8      58     59     Ljava/lang/UnsupportedOperationException;
            //  0      8      54     58     Ljava/lang/IllegalArgumentException;
            //  20     24     89     93     Ljava/lang/Exception;
            // 
            // The error that occurred was:
            // 
            // java.lang.IllegalStateException: Expression is linked from several locations: Label_0020:
            //     at com.strobel.decompiler.ast.Error.expressionLinkedFromMultipleLocations(Error.java:27)
            //     at com.strobel.decompiler.ast.AstOptimizer.mergeDisparateObjectInitializations(AstOptimizer.java:2604)
            //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:235)
            //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:42)
            //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:206)
            //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:93)
            //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethodBody(AstBuilder.java:868)
            //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethod(AstBuilder.java:761)
            //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addTypeMembers(AstBuilder.java:638)
            //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeCore(AstBuilder.java:605)
            //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeNoCache(AstBuilder.java:195)
            //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addTypeMembers(AstBuilder.java:662)
            //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeCore(AstBuilder.java:605)
            //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeNoCache(AstBuilder.java:195)
            //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createType(AstBuilder.java:162)
            //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addType(AstBuilder.java:137)
            //     at com.strobel.decompiler.languages.java.JavaLanguage.buildAst(JavaLanguage.java:71)
            //     at com.strobel.decompiler.languages.java.JavaLanguage.decompileType(JavaLanguage.java:59)
            //     at com.strobel.decompiler.DecompilerDriver.decompileType(DecompilerDriver.java:333)
            //     at com.strobel.decompiler.DecompilerDriver.decompileJar(DecompilerDriver.java:254)
            //     at com.strobel.decompiler.DecompilerDriver.main(DecompilerDriver.java:144)
            // 
            throw new IllegalStateException("An error occurred while decompiling this method.");
        }
        
        public boolean a() {
            final int c = this.c;
            boolean b = true;
            if (c != 1) {
                b = false;
            }
            return b;
        }
        
        public boolean a(final b b) {
            return b.c == this.c && b.g == this.g && b.e == this.e && b.f == this.f && b.d == this.d;
        }
        
        public long b(final long n) {
            return n * 1000000L / this.e;
        }
        
        public long c(final long n) {
            return n * this.e / 1000000L;
        }
    }
    
    public static class c implements a
    {
        private final com.applovin.exoplayer2.b.f[] a;
        private final com.applovin.exoplayer2.b.u b;
        private final w c;
        
        public c(final com.applovin.exoplayer2.b.f... array) {
            this(array, new com.applovin.exoplayer2.b.u(), new w());
        }
        
        public c(final com.applovin.exoplayer2.b.f[] array, final com.applovin.exoplayer2.b.u b, final w c) {
            final com.applovin.exoplayer2.b.f[] a = new com.applovin.exoplayer2.b.f[array.length + 2];
            System.arraycopy(array, 0, this.a = a, 0, array.length);
            this.b = b;
            this.c = c;
            a[array.length] = b;
            a[array.length + 1] = c;
        }
        
        @Override
        public long a(final long n) {
            return this.c.a(n);
        }
        
        @Override
        public am a(final am am) {
            this.c.a(am.b);
            this.c.b(am.c);
            return am;
        }
        
        @Override
        public boolean a(final boolean b) {
            this.b.a(b);
            return b;
        }
        
        @Override
        public com.applovin.exoplayer2.b.f[] a() {
            return this.a;
        }
        
        @Override
        public long b() {
            return this.b.k();
        }
    }
    
    public static final class d extends RuntimeException
    {
        private d(final String message) {
            super(message);
        }
    }
    
    private static final class e
    {
        public final am a;
        public final boolean b;
        public final long c;
        public final long d;
        
        private e(final am a, final boolean b, final long c, final long d) {
            this.a = a;
            this.b = b;
            this.c = c;
            this.d = d;
        }
    }
    
    private static final class f<T extends Exception>
    {
        private final long a;
        @Nullable
        private T b;
        private long c;
        
        public f(final long a) {
            this.a = a;
        }
        
        public void a() {
            this.b = null;
        }
        
        public void a(final T t) throws T, Exception {
            final long elapsedRealtime = SystemClock.elapsedRealtime();
            if (this.b == null) {
                this.b = t;
                this.c = this.a + elapsedRealtime;
            }
            if (elapsedRealtime >= this.c) {
                final Exception b = this.b;
                if (b != t) {
                    b.addSuppressed(t);
                }
                final Exception b2 = this.b;
                this.a();
                throw b2;
            }
        }
    }
    
    private final class g implements j.a
    {
        final n a;
        
        private g(final n a) {
            this.a = a;
        }
        
        @Override
        public void a(final int n, final long n2) {
            if (this.a.q != null) {
                this.a.q.a(n, n2, SystemClock.elapsedRealtime() - this.a.Y);
            }
        }
        
        @Override
        public void a(final long n) {
            if (this.a.q != null) {
                this.a.q.a(n);
            }
        }
        
        @Override
        public void a(final long lng, final long lng2, final long lng3, final long lng4) {
            final StringBuilder sb = new StringBuilder();
            sb.append("Spurious audio timestamp (frame position mismatch): ");
            sb.append(lng);
            sb.append(", ");
            sb.append(lng2);
            sb.append(", ");
            sb.append(lng3);
            sb.append(", ");
            sb.append(lng4);
            sb.append(", ");
            sb.append(this.a.z());
            sb.append(", ");
            sb.append(this.a.A());
            final String string = sb.toString();
            if (!n.a) {
                q.c("DefaultAudioSink", string);
                return;
            }
            throw new d(string);
        }
        
        @Override
        public void b(final long lng) {
            final StringBuilder sb = new StringBuilder();
            sb.append("Ignoring impossibly large audio latency: ");
            sb.append(lng);
            q.c("DefaultAudioSink", sb.toString());
        }
        
        @Override
        public void b(final long lng, final long lng2, final long lng3, final long lng4) {
            final StringBuilder sb = new StringBuilder();
            sb.append("Spurious audio timestamp (system clock mismatch): ");
            sb.append(lng);
            sb.append(", ");
            sb.append(lng2);
            sb.append(", ");
            sb.append(lng3);
            sb.append(", ");
            sb.append(lng4);
            sb.append(", ");
            sb.append(this.a.z());
            sb.append(", ");
            sb.append(this.a.A());
            final String string = sb.toString();
            if (!n.a) {
                q.c("DefaultAudioSink", string);
                return;
            }
            throw new d(string);
        }
    }
    
    @RequiresApi(29)
    private final class h
    {
        final n a;
        private final Handler b;
        private final AudioTrack$StreamEventCallback c;
        
        public h(final n a) {
            this.a = a;
            this.b = new Handler();
            this.c = new AudioTrack$StreamEventCallback(this, a) {
                final n a;
                final h b;
                
                public void onDataRequest(final AudioTrack audioTrack, final int n) {
                    com.applovin.exoplayer2.l.a.b(audioTrack == this.b.a.t);
                    if (this.b.a.q != null && this.b.a.T) {
                        this.b.a.q.b();
                    }
                }
                
                public void onTearDown(@NonNull final AudioTrack audioTrack) {
                    com.applovin.exoplayer2.l.a.b(audioTrack == this.b.a.t);
                    if (this.b.a.q != null && this.b.a.T) {
                        this.b.a.q.b();
                    }
                }
            };
        }
        
        public void a(final AudioTrack audioTrack) {
            final Handler b = this.b;
            Objects.requireNonNull(b);
            o0ooO.\u3007080(audioTrack, (Executor)new \u3007o00\u3007\u3007Oo(b), this.c);
        }
        
        public void b(final AudioTrack audioTrack) {
            o\u30078.\u3007080(audioTrack, this.c);
            this.b.removeCallbacksAndMessages((Object)null);
        }
    }
}

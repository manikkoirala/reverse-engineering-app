// 
// Decompiled by Procyon v0.6.0
// 

package com.applovin.exoplayer2.b;

import androidx.annotation.Nullable;

public final class k
{
    public final int a;
    public final float b;
    
    public k(final int a, final float b) {
        this.a = a;
        this.b = b;
    }
    
    @Override
    public boolean equals(@Nullable final Object o) {
        boolean b = true;
        if (this == o) {
            return true;
        }
        if (o != null && k.class == o.getClass()) {
            final k k = (k)o;
            if (this.a != k.a || Float.compare(k.b, this.b) != 0) {
                b = false;
            }
            return b;
        }
        return false;
    }
    
    @Override
    public int hashCode() {
        return (527 + this.a) * 31 + Float.floatToIntBits(this.b);
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.applovin.exoplayer2.b;

import com.applovin.exoplayer2.l.ai;
import com.applovin.exoplayer2.l.a;
import android.os.Handler;
import com.applovin.exoplayer2.c.e;
import androidx.annotation.Nullable;
import com.applovin.exoplayer2.c.h;
import com.applovin.exoplayer2.v;

public interface g
{
    void a(final int p0, final long p1, final long p2);
    
    void a(final long p0);
    
    @Deprecated
    void a(final v p0);
    
    void a_(final boolean p0);
    
    void b(final v p0, @Nullable final h p1);
    
    void b(final Exception p0);
    
    void b(final String p0);
    
    void b(final String p0, final long p1, final long p2);
    
    void c(final e p0);
    
    void c(final Exception p0);
    
    void d(final e p0);
    
    public static final class a
    {
        @Nullable
        private final Handler a;
        @Nullable
        private final g b;
        
        public a(@Nullable Handler a, @Nullable final g b) {
            if (b != null) {
                a = (Handler)com.applovin.exoplayer2.l.a.b((Object)a);
            }
            else {
                a = null;
            }
            this.a = a;
            this.b = b;
        }
        
        public void a(final int n, final long n2, final long n3) {
            final Handler a = this.a;
            if (a != null) {
                a.post((Runnable)new \u300780\u3007808\u3007O(this, n, n2, n3));
            }
        }
        
        public void a(final long n) {
            final Handler a = this.a;
            if (a != null) {
                a.post((Runnable)new \u3007\u3007888(this, n));
            }
        }
        
        public void a(final e e) {
            final Handler a = this.a;
            if (a != null) {
                a.post((Runnable)new oO80(this, e));
            }
        }
        
        public void a(final v v, @Nullable final h h) {
            final Handler a = this.a;
            if (a != null) {
                a.post((Runnable)new OO0o\u3007\u3007(this, v, h));
            }
        }
        
        public void a(final Exception ex) {
            final Handler a = this.a;
            if (a != null) {
                a.post((Runnable)new \u3007O8o08O(this, ex));
            }
        }
        
        public void a(final String s) {
            final Handler a = this.a;
            if (a != null) {
                a.post((Runnable)new \u30078o8o\u3007(this, s));
            }
        }
        
        public void a(final String s, final long n, final long n2) {
            final Handler a = this.a;
            if (a != null) {
                a.post((Runnable)new o\u30070(this, s, n, n2));
            }
        }
        
        public void a(final boolean b) {
            final Handler a = this.a;
            if (a != null) {
                a.post((Runnable)new Oo08(this, b));
            }
        }
        
        public void b(final e e) {
            e.a();
            final Handler a = this.a;
            if (a != null) {
                a.post((Runnable)new OO0o\u3007\u3007\u3007\u30070(this, e));
            }
        }
        
        public void b(final Exception ex) {
            final Handler a = this.a;
            if (a != null) {
                a.post((Runnable)new Oooo8o0\u3007(this, ex));
            }
        }
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.applovin.exoplayer2.b;

import android.media.AudioTimestamp;
import androidx.annotation.RequiresApi;
import android.annotation.TargetApi;
import com.applovin.exoplayer2.l.ai;
import android.media.AudioTrack;
import androidx.annotation.Nullable;

final class i
{
    @Nullable
    private final a a;
    private int b;
    private long c;
    private long d;
    private long e;
    private long f;
    
    public i(final AudioTrack audioTrack) {
        if (ai.a >= 19) {
            this.a = new a(audioTrack);
            this.d();
        }
        else {
            this.a = null;
            this.a(3);
        }
    }
    
    private void a(final int b) {
        this.b = b;
        if (b != 0) {
            if (b != 1) {
                if (b != 2 && b != 3) {
                    if (b != 4) {
                        throw new IllegalStateException();
                    }
                    this.d = 500000L;
                }
                else {
                    this.d = 10000000L;
                }
            }
            else {
                this.d = 10000L;
            }
        }
        else {
            this.e = 0L;
            this.f = -1L;
            this.c = System.nanoTime() / 1000L;
            this.d = 10000L;
        }
    }
    
    public void a() {
        this.a(4);
    }
    
    @TargetApi(19)
    public boolean a(final long e) {
        final a a = this.a;
        boolean b2;
        final boolean b = b2 = false;
        if (a != null) {
            if (e - this.e < this.d) {
                b2 = b;
            }
            else {
                this.e = e;
                final boolean a2 = a.a();
                final int b3 = this.b;
                if (b3 != 0) {
                    if (b3 != 1) {
                        if (b3 != 2) {
                            if (b3 != 3) {
                                if (b3 != 4) {
                                    throw new IllegalStateException();
                                }
                            }
                            else if (a2) {
                                this.d();
                            }
                        }
                        else if (!a2) {
                            this.d();
                        }
                    }
                    else if (a2) {
                        if (this.a.c() > this.f) {
                            this.a(2);
                        }
                    }
                    else {
                        this.d();
                    }
                }
                else if (a2) {
                    b2 = b;
                    if (this.a.b() < this.c) {
                        return b2;
                    }
                    this.f = this.a.c();
                    this.a(1);
                }
                else if (e - this.c > 500000L) {
                    this.a(3);
                }
                b2 = a2;
            }
        }
        return b2;
    }
    
    public void b() {
        if (this.b == 4) {
            this.d();
        }
    }
    
    public boolean c() {
        return this.b == 2;
    }
    
    public void d() {
        if (this.a != null) {
            this.a(0);
        }
    }
    
    @TargetApi(19)
    public long e() {
        final a a = this.a;
        long b;
        if (a != null) {
            b = a.b();
        }
        else {
            b = -9223372036854775807L;
        }
        return b;
    }
    
    @TargetApi(19)
    public long f() {
        final a a = this.a;
        long c;
        if (a != null) {
            c = a.c();
        }
        else {
            c = -1L;
        }
        return c;
    }
    
    @RequiresApi(19)
    private static final class a
    {
        private final AudioTrack a;
        private final AudioTimestamp b;
        private long c;
        private long d;
        private long e;
        
        public a(final AudioTrack a) {
            this.a = a;
            this.b = new AudioTimestamp();
        }
        
        public boolean a() {
            final boolean timestamp = this.a.getTimestamp(this.b);
            if (timestamp) {
                final long framePosition = this.b.framePosition;
                if (this.d > framePosition) {
                    ++this.c;
                }
                this.d = framePosition;
                this.e = framePosition + (this.c << 32);
            }
            return timestamp;
        }
        
        public long b() {
            return this.b.nanoTime / 1000L;
        }
        
        public long c() {
            return this.e;
        }
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.applovin.exoplayer2.b;

import java.nio.ByteBuffer;
import com.applovin.exoplayer2.l.ai;
import com.applovin.exoplayer2.l.a;

public final class u extends l
{
    private final long d;
    private final long e;
    private final short f;
    private int g;
    private boolean h;
    private byte[] i;
    private byte[] j;
    private int k;
    private int l;
    private int m;
    private boolean n;
    private long o;
    
    public u() {
        this(150000L, 20000L, (short)1024);
    }
    
    public u(final long d, final long e, final short f) {
        com.applovin.exoplayer2.l.a.a(e <= d);
        this.d = d;
        this.e = e;
        this.f = f;
        final byte[] f2 = ai.f;
        this.i = f2;
        this.j = f2;
    }
    
    private int a(final long n) {
        return (int)(n * super.b.b / 1000000L);
    }
    
    private void a(final ByteBuffer byteBuffer, final byte[] array, final int n) {
        final int min = Math.min(byteBuffer.remaining(), this.m);
        final int offset = this.m - min;
        System.arraycopy(array, n - offset, this.j, 0, offset);
        byteBuffer.position(byteBuffer.limit() - min);
        byteBuffer.get(this.j, offset, min);
    }
    
    private void a(final byte[] src, final int length) {
        this.a(length).put(src, 0, length).flip();
        if (length > 0) {
            this.n = true;
        }
    }
    
    private void b(final ByteBuffer byteBuffer) {
        final int limit = byteBuffer.limit();
        byteBuffer.limit(Math.min(limit, byteBuffer.position() + this.i.length));
        final int g = this.g(byteBuffer);
        if (g == byteBuffer.position()) {
            this.k = 1;
        }
        else {
            byteBuffer.limit(g);
            this.e(byteBuffer);
        }
        byteBuffer.limit(limit);
    }
    
    private void c(final ByteBuffer byteBuffer) {
        final int limit = byteBuffer.limit();
        final int f = this.f(byteBuffer);
        final int a = f - byteBuffer.position();
        final byte[] i = this.i;
        final int length = i.length;
        final int l = this.l;
        final int b = length - l;
        if (f < limit && a < b) {
            this.a(i, l);
            this.l = 0;
            this.k = 0;
        }
        else {
            final int min = Math.min(a, b);
            byteBuffer.limit(byteBuffer.position() + min);
            byteBuffer.get(this.i, this.l, min);
            final int j = this.l + min;
            this.l = j;
            final byte[] k = this.i;
            if (j == k.length) {
                if (this.n) {
                    this.a(k, this.m);
                    this.o += (this.l - this.m * 2) / this.g;
                }
                else {
                    this.o += (j - this.m) / this.g;
                }
                this.a(byteBuffer, this.i, this.l);
                this.l = 0;
                this.k = 2;
            }
            byteBuffer.limit(limit);
        }
    }
    
    private void d(final ByteBuffer byteBuffer) {
        final int limit = byteBuffer.limit();
        final int f = this.f(byteBuffer);
        byteBuffer.limit(f);
        this.o += byteBuffer.remaining() / this.g;
        this.a(byteBuffer, this.j, this.m);
        if (f < limit) {
            this.a(this.j, this.m);
            this.k = 0;
            byteBuffer.limit(limit);
        }
    }
    
    private void e(final ByteBuffer src) {
        final int remaining = src.remaining();
        this.a(remaining).put(src).flip();
        if (remaining > 0) {
            this.n = true;
        }
    }
    
    private int f(final ByteBuffer byteBuffer) {
        for (int i = byteBuffer.position(); i < byteBuffer.limit(); i += 2) {
            if (Math.abs(byteBuffer.getShort(i)) > this.f) {
                final int g = this.g;
                return g * (i / g);
            }
        }
        return byteBuffer.limit();
    }
    
    private int g(final ByteBuffer byteBuffer) {
        for (int i = byteBuffer.limit() - 2; i >= byteBuffer.position(); i -= 2) {
            if (Math.abs(byteBuffer.getShort(i)) > this.f) {
                final int g = this.g;
                return i / g * g + g;
            }
        }
        return byteBuffer.position();
    }
    
    @Override
    public void a(final ByteBuffer byteBuffer) {
        while (byteBuffer.hasRemaining() && !this.g()) {
            final int k = this.k;
            if (k != 0) {
                if (k != 1) {
                    if (k != 2) {
                        throw new IllegalStateException();
                    }
                    this.d(byteBuffer);
                }
                else {
                    this.c(byteBuffer);
                }
            }
            else {
                this.b(byteBuffer);
            }
        }
    }
    
    public void a(final boolean h) {
        this.h = h;
    }
    
    @Override
    public boolean a() {
        return this.h;
    }
    
    public a b(a a) throws b {
        if (a.d == 2) {
            if (!this.h) {
                a = com.applovin.exoplayer2.b.f.a.a;
            }
            return a;
        }
        throw new f.b(a);
    }
    
    @Override
    protected void h() {
        final int l = this.l;
        if (l > 0) {
            this.a(this.i, l);
        }
        if (!this.n) {
            this.o += this.m / this.g;
        }
    }
    
    @Override
    protected void i() {
        if (this.h) {
            this.g = super.b.e;
            final int n = this.a(this.d) * this.g;
            if (this.i.length != n) {
                this.i = new byte[n];
            }
            final int m = this.a(this.e) * this.g;
            if (this.j.length != (this.m = m)) {
                this.j = new byte[m];
            }
        }
        this.k = 0;
        this.o = 0L;
        this.l = 0;
        this.n = false;
    }
    
    @Override
    protected void j() {
        this.h = false;
        this.m = 0;
        final byte[] f = ai.f;
        this.i = f;
        this.j = f;
    }
    
    public long k() {
        return this.o;
    }
}

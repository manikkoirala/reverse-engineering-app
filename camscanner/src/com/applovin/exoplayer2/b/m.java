// 
// Decompiled by Procyon v0.6.0
// 

package com.applovin.exoplayer2.b;

import com.applovin.exoplayer2.l.a;
import java.nio.ByteBuffer;
import androidx.annotation.Nullable;

final class m extends l
{
    @Nullable
    private int[] d;
    @Nullable
    private int[] e;
    
    @Override
    public void a(final ByteBuffer byteBuffer) {
        final int[] array = (int[])com.applovin.exoplayer2.l.a.b((Object)this.e);
        int i = byteBuffer.position();
        final int limit = byteBuffer.limit();
        final ByteBuffer a = this.a((limit - i) / super.b.e * super.c.e);
        while (i < limit) {
            for (int length = array.length, j = 0; j < length; ++j) {
                a.putShort(byteBuffer.getShort(array[j] * 2 + i));
            }
            i += super.b.e;
        }
        byteBuffer.position(limit);
        a.flip();
    }
    
    public void a(@Nullable final int[] d) {
        this.d = d;
    }
    
    public a b(a a) throws b {
        final int[] d = this.d;
        if (d == null) {
            return com.applovin.exoplayer2.b.f.a.a;
        }
        if (a.d == 2) {
            boolean b;
            if (a.c != d.length) {
                b = true;
            }
            else {
                b = false;
            }
            for (int i = 0; i < d.length; ++i) {
                final int n = d[i];
                if (n >= a.c) {
                    throw new f.b(a);
                }
                b |= (n != i);
            }
            if (b) {
                a = new f.a(a.b, d.length, 2);
            }
            else {
                a = com.applovin.exoplayer2.b.f.a.a;
            }
            return a;
        }
        throw new f.b(a);
    }
    
    @Override
    protected void i() {
        this.e = this.d;
    }
    
    @Override
    protected void j() {
        this.e = null;
        this.d = null;
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.applovin.exoplayer2.b;

import com.applovin.exoplayer2.c.e;
import com.applovin.exoplayer2.l.a;
import java.nio.ByteBuffer;
import com.applovin.exoplayer2.am;
import java.util.Collection;
import java.util.ArrayList;
import java.util.Collections;
import android.media.MediaCrypto;
import com.applovin.exoplayer2.w;
import android.annotation.SuppressLint;
import com.applovin.exoplayer2.l.t;
import android.media.MediaFormat;
import java.util.List;
import com.applovin.exoplayer2.f.l;
import com.applovin.exoplayer2.\u3007O00;
import com.applovin.exoplayer2.l.u;
import com.applovin.exoplayer2.p;
import androidx.annotation.CallSuper;
import com.applovin.exoplayer2.f.i;
import com.applovin.exoplayer2.l.ai;
import android.os.Handler;
import com.applovin.exoplayer2.f.k;
import com.applovin.exoplayer2.ar;
import androidx.annotation.Nullable;
import com.applovin.exoplayer2.v;
import android.content.Context;
import com.applovin.exoplayer2.l.s;
import com.applovin.exoplayer2.f.j;

public class q extends j implements s
{
    private final Context b;
    private final g.a c;
    private final h d;
    private int e;
    private boolean f;
    @Nullable
    private v g;
    private long h;
    private boolean i;
    private boolean j;
    private boolean k;
    private boolean l;
    @Nullable
    private ar.a m;
    
    public q(final Context context, final com.applovin.exoplayer2.f.g.b b, final k k, final boolean b2, @Nullable final Handler handler, @Nullable final g g, final h d) {
        super(1, b, k, b2, 44100.0f);
        this.b = context.getApplicationContext();
        this.d = d;
        this.c = new g.a(handler, g);
        d.a((h.c)new a());
    }
    
    public q(final Context context, final k k, final boolean b, @Nullable final Handler handler, @Nullable final g g, final h h) {
        this(context, com.applovin.exoplayer2.f.g.b.a, k, b, handler, g, h);
    }
    
    private void R() {
        long n = this.d.a(this.A());
        if (n != Long.MIN_VALUE) {
            if (!this.j) {
                n = Math.max(this.h, n);
            }
            this.h = n;
            this.j = false;
        }
    }
    
    private static boolean S() {
        if (ai.a == 23) {
            final String d = ai.d;
            if ("ZTE B2017G".equals(d) || "AXON 7 mini".equals(d)) {
                return true;
            }
        }
        return false;
    }
    
    private int a(final i i, final v v) {
        if ("OMX.google.raw.decoder".equals(i.a)) {
            final int a = com.applovin.exoplayer2.l.ai.a;
            if (a < 24 && (a != 23 || !com.applovin.exoplayer2.l.ai.c(this.b))) {
                return -1;
            }
        }
        return v.m;
    }
    
    private static boolean b(String b) {
        if (ai.a < 24 && "OMX.SEC.aac.dec".equals(b) && "samsung".equals(ai.c)) {
            b = ai.b;
            if (b.startsWith("zeroflte") || b.startsWith("herolte") || b.startsWith("heroqlte")) {
                return true;
            }
        }
        return false;
    }
    
    @Override
    public boolean A() {
        return super.A() && this.d.d();
    }
    
    @CallSuper
    protected void B() {
        this.j = true;
    }
    
    @Override
    protected void C() {
        super.C();
        this.d.b();
    }
    
    @Override
    protected void D() throws p {
        try {
            this.d.c();
        }
        catch (final h.e e) {
            throw this.a(e, e.c, e.b, 5002);
        }
    }
    
    @Override
    protected float a(float n, final v v, final v[] array) {
        final int length = array.length;
        int i = 0;
        int a = -1;
        while (i < length) {
            final int z = array[i].z;
            int max = a;
            if (z != -1) {
                max = Math.max(a, z);
            }
            ++i;
            a = max;
        }
        if (a == -1) {
            n = -1.0f;
        }
        else {
            n *= a;
        }
        return n;
    }
    
    protected int a(final i i, final v v, final v[] array) {
        int a = this.a(i, v);
        if (array.length == 1) {
            return a;
        }
        int max;
        for (int length = array.length, j = 0; j < length; ++j, a = max) {
            final v v2 = array[j];
            max = a;
            if (i.a(v, v2).d != 0) {
                max = Math.max(a, this.a(i, v2));
            }
        }
        return a;
    }
    
    @Override
    protected int a(final k k, final v v) throws l.b {
        if (!com.applovin.exoplayer2.l.u.a(v.l)) {
            return \u3007O00.\u3007o00\u3007\u3007Oo(0);
        }
        int n;
        if (com.applovin.exoplayer2.l.ai.a >= 21) {
            n = 32;
        }
        else {
            n = 0;
        }
        final boolean b = v.E != 0;
        final boolean c = com.applovin.exoplayer2.f.j.c(v);
        final int n2 = 8;
        int n3 = 4;
        if (c && this.d.a(v) && (!b || com.applovin.exoplayer2.f.l.a() != null)) {
            return \u3007O00.\u3007080(4, 8, n);
        }
        if ("audio/raw".equals(v.l) && !this.d.a(v)) {
            return \u3007O00.\u3007o00\u3007\u3007Oo(1);
        }
        if (!this.d.a(com.applovin.exoplayer2.l.ai.b(2, v.y, v.z))) {
            return \u3007O00.\u3007o00\u3007\u3007Oo(1);
        }
        final List<i> a = this.a(k, v, false);
        if (a.isEmpty()) {
            return \u3007O00.\u3007o00\u3007\u3007Oo(1);
        }
        if (!c) {
            return \u3007O00.\u3007o00\u3007\u3007Oo(2);
        }
        final i i = a.get(0);
        final boolean a2 = i.a(v);
        int n4 = n2;
        if (a2) {
            n4 = n2;
            if (i.c(v)) {
                n4 = 16;
            }
        }
        if (!a2) {
            n3 = 3;
        }
        return \u3007O00.\u3007080(n3, n4, n);
    }
    
    @SuppressLint({ "InlinedApi" })
    protected MediaFormat a(final v v, final String s, int a, final float n) {
        final MediaFormat mediaFormat = new MediaFormat();
        mediaFormat.setString("mime", s);
        mediaFormat.setInteger("channel-count", v.y);
        mediaFormat.setInteger("sample-rate", v.z);
        com.applovin.exoplayer2.l.t.a(mediaFormat, (List)v.n);
        com.applovin.exoplayer2.l.t.a(mediaFormat, "max-input-size", a);
        a = com.applovin.exoplayer2.l.ai.a;
        if (a >= 23) {
            mediaFormat.setInteger("priority", 0);
            if (n != -1.0f && !S()) {
                mediaFormat.setFloat("operating-rate", n);
            }
        }
        if (a <= 28 && "audio/ac4".equals(v.l)) {
            mediaFormat.setInteger("ac4-is-sync", 1);
        }
        if (a >= 24 && this.d.b(com.applovin.exoplayer2.l.ai.b(4, v.y, v.z)) == 2) {
            mediaFormat.setInteger("pcm-encoding", 4);
        }
        return mediaFormat;
    }
    
    @Override
    protected com.applovin.exoplayer2.c.h a(final i i, final v v, final v v2) {
        final com.applovin.exoplayer2.c.h a = i.a(v, v2);
        int e;
        final int n = e = a.e;
        if (this.a(i, v2) > this.e) {
            e = (n | 0x40);
        }
        final String a2 = i.a;
        int d;
        if (e != 0) {
            d = 0;
        }
        else {
            d = a.d;
        }
        return new com.applovin.exoplayer2.c.h(a2, v, v2, d, e);
    }
    
    @Nullable
    @Override
    protected com.applovin.exoplayer2.c.h a(final w w) throws p {
        final com.applovin.exoplayer2.c.h a = super.a(w);
        this.c.a(w.b, a);
        return a;
    }
    
    @Override
    protected com.applovin.exoplayer2.f.g.a a(final i i, final v v, @Nullable final MediaCrypto mediaCrypto, final float n) {
        this.e = this.a(i, v, this.u());
        this.f = b(i.a);
        final MediaFormat a = this.a(v, i.c, this.e, n);
        v g;
        if ("audio/raw".equals(i.b) && !"audio/raw".equals(v.l)) {
            g = v;
        }
        else {
            g = null;
        }
        this.g = g;
        return com.applovin.exoplayer2.f.g.a.a(i, a, v, mediaCrypto);
    }
    
    @Override
    protected List<i> a(final k k, final v v, final boolean b) throws l.b {
        final String l = v.l;
        if (l == null) {
            return Collections.emptyList();
        }
        if (this.d.a(v)) {
            final i a = com.applovin.exoplayer2.f.l.a();
            if (a != null) {
                return Collections.singletonList(a);
            }
        }
        List<i> a2;
        final List<i> c = a2 = com.applovin.exoplayer2.f.l.a(k.getDecoderInfos(l, b, (boolean)(0 != 0)), v);
        if ("audio/eac3-joc".equals(l)) {
            a2 = new ArrayList<i>((Collection<? extends T>)c);
            a2.addAll(k.getDecoderInfos("audio/eac3", b, false));
        }
        return (List<i>)Collections.unmodifiableList((List<?>)a2);
    }
    
    public void a(final int n, @Nullable final Object o) throws p {
        if (n != 2) {
            if (n != 3) {
                if (n != 6) {
                    switch (n) {
                        default: {
                            super.a(n, o);
                            break;
                        }
                        case 11: {
                            this.m = (ar.a)o;
                            break;
                        }
                        case 10: {
                            this.d.a((int)o);
                            break;
                        }
                        case 9: {
                            this.d.b((boolean)o);
                            break;
                        }
                    }
                }
                else {
                    this.d.a((com.applovin.exoplayer2.b.k)o);
                }
            }
            else {
                this.d.a((d)o);
            }
        }
        else {
            this.d.a((float)o);
        }
    }
    
    @Override
    protected void a(final long h, final boolean b) throws p {
        super.a(h, b);
        if (this.l) {
            this.d.k();
        }
        else {
            this.d.j();
        }
        this.h = h;
        this.i = true;
        this.j = true;
    }
    
    public void a(final am am) {
        this.d.a(am);
    }
    
    @Override
    protected void a(final com.applovin.exoplayer2.c.g g) {
        if (this.i && !g.b()) {
            if (Math.abs(g.d - this.h) > 500000L) {
                this.h = g.d;
            }
            this.i = false;
        }
    }
    
    @Override
    protected void a(v v, @Nullable final MediaFormat mediaFormat) throws p {
        final v g = this.g;
        final int[] array = null;
        final int[] array2 = null;
        int[] array3;
        if (g != null) {
            v = g;
            array3 = array;
        }
        else if (this.G() == null) {
            array3 = array;
        }
        else {
            int n;
            if ("audio/raw".equals(v.l)) {
                n = v.A;
            }
            else if (com.applovin.exoplayer2.l.ai.a >= 24 && mediaFormat.containsKey("pcm-encoding")) {
                n = mediaFormat.getInteger("pcm-encoding");
            }
            else if (mediaFormat.containsKey("v-bits-per-sample")) {
                n = com.applovin.exoplayer2.l.ai.c(mediaFormat.getInteger("v-bits-per-sample"));
            }
            else if ("audio/raw".equals(v.l)) {
                n = v.A;
            }
            else {
                n = 2;
            }
            final v a = new v.a().f("audio/raw").m(n).n(v.B).o(v.C).k(mediaFormat.getInteger("channel-count")).l(mediaFormat.getInteger("sample-rate")).a();
            array3 = array2;
            if (this.f) {
                array3 = array2;
                if (a.y == 6) {
                    final int y = v.y;
                    array3 = array2;
                    if (y < 6) {
                        final int[] array4 = new int[y];
                        int n2 = 0;
                        while (true) {
                            array3 = array4;
                            if (n2 >= v.y) {
                                break;
                            }
                            array4[n2] = n2;
                            ++n2;
                        }
                    }
                }
            }
            v = a;
        }
        try {
            this.d.a(v, 0, array3);
        }
        catch (final h.a a2) {
            throw this.a(a2, a2.a, 5001);
        }
    }
    
    @Override
    protected void a(final Exception ex) {
        com.applovin.exoplayer2.l.q.c("MediaCodecAudioRenderer", "Audio codec error", (Throwable)ex);
        this.c.b(ex);
    }
    
    @Override
    protected void a(final String s) {
        this.c.a(s);
    }
    
    @Override
    protected void a(final String s, final long n, final long n2) {
        this.c.a(s, n, n2);
    }
    
    @Override
    protected void a(final boolean b, final boolean b2) throws p {
        super.a(b, b2);
        this.c.a(super.a);
        if (this.v().b) {
            this.d.g();
        }
        else {
            this.d.h();
        }
    }
    
    @Override
    protected boolean a(final long n, final long n2, @Nullable final com.applovin.exoplayer2.f.g g, @Nullable final ByteBuffer byteBuffer, final int n3, final int n4, final int n5, final long n6, final boolean b, final boolean b2, final v v) throws p {
        com.applovin.exoplayer2.l.a.b((Object)byteBuffer);
        if (this.g != null && (n4 & 0x2) != 0x0) {
            ((com.applovin.exoplayer2.f.g)com.applovin.exoplayer2.l.a.b((Object)g)).a(n3, false);
            return true;
        }
        if (b) {
            if (g != null) {
                g.a(n3, false);
            }
            final com.applovin.exoplayer2.c.e a = super.a;
            a.f += n5;
            this.d.b();
            return true;
        }
        try {
            if (this.d.a(byteBuffer, n6, n5)) {
                if (g != null) {
                    g.a(n3, false);
                }
                final com.applovin.exoplayer2.c.e a2 = super.a;
                a2.e += n5;
                return true;
            }
            return false;
        }
        catch (final h.e e) {
            throw this.a(e, v, e.b, 5002);
        }
        catch (final h.b b3) {
            throw this.a(b3, b3.c, b3.b, 5001);
        }
    }
    
    @Override
    protected boolean b(final v v) {
        return this.d.a(v);
    }
    
    @Nullable
    public s c() {
        return (s)this;
    }
    
    public long c_() {
        if (this.d_() == 2) {
            this.R();
        }
        return this.h;
    }
    
    public am d() {
        return this.d.f();
    }
    
    @Override
    protected void p() {
        super.p();
        this.d.a();
    }
    
    @Override
    protected void q() {
        this.R();
        this.d.i();
        super.q();
    }
    
    @Override
    protected void r() {
        this.k = true;
        try {
            this.d.j();
            try {
                super.r();
            }
            finally {
                this.c.b(super.a);
            }
        }
        finally {
            try {
                super.r();
                this.c.b(super.a);
            }
            finally {
                this.c.b(super.a);
            }
        }
    }
    
    @Override
    protected void s() {
        try {
            super.s();
        }
        finally {
            if (this.k) {
                this.k = false;
                this.d.l();
            }
        }
    }
    
    public String y() {
        return "MediaCodecAudioRenderer";
    }
    
    @Override
    public boolean z() {
        return this.d.e() || super.z();
    }
    
    private final class a implements c
    {
        final q a;
        
        private a(final q a) {
            this.a = a;
        }
        
        @Override
        public void a() {
            this.a.B();
        }
        
        @Override
        public void a(final int n, final long n2, final long n3) {
            this.a.c.a(n, n2, n3);
        }
        
        @Override
        public void a(final long n) {
            this.a.c.a(n);
        }
        
        @Override
        public void a(final Exception ex) {
            com.applovin.exoplayer2.l.q.c("MediaCodecAudioRenderer", "Audio sink error", (Throwable)ex);
            this.a.c.a(ex);
        }
        
        @Override
        public void a(final boolean b) {
            this.a.c.a(b);
        }
        
        @Override
        public void b() {
            if (this.a.m != null) {
                this.a.m.a();
            }
        }
        
        @Override
        public void b(final long n) {
            if (this.a.m != null) {
                this.a.m.a(n);
            }
        }
    }
}

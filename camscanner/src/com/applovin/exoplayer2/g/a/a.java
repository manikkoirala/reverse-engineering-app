// 
// Decompiled by Procyon v0.6.0
// 

package com.applovin.exoplayer2.g.a;

import com.applovin.exoplayer2.ac;
import com.applovin.exoplayer2.g.\u3007080;
import com.applovin.exoplayer2.v;
import android.os.Parcel;
import android.os.Parcelable$Creator;

public final class a implements com.applovin.exoplayer2.g.a.a
{
    public static final Parcelable$Creator<a> CREATOR;
    public final int a;
    public final String b;
    
    static {
        CREATOR = (Parcelable$Creator)new Parcelable$Creator<a>() {
            public a a(final Parcel parcel) {
                return new a(parcel.readInt(), (String)com.applovin.exoplayer2.l.a.b((Object)parcel.readString()));
            }
            
            public a[] a(final int n) {
                return new a[n];
            }
        };
    }
    
    public a(final int a, final String b) {
        this.a = a;
        this.b = b;
    }
    
    public int describeContents() {
        return 0;
    }
    
    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append("Ait(controlCode=");
        sb.append(this.a);
        sb.append(",url=");
        sb.append(this.b);
        sb.append(")");
        return sb.toString();
    }
    
    public void writeToParcel(final Parcel parcel, final int n) {
        parcel.writeString(this.b);
        parcel.writeInt(this.a);
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.applovin.exoplayer2.g.a;

import java.nio.ByteBuffer;
import com.applovin.exoplayer2.g.d;
import androidx.annotation.Nullable;
import java.util.List;
import com.applovin.exoplayer2.common.base.Charsets;
import java.util.ArrayList;
import com.applovin.exoplayer2.g.a;
import com.applovin.exoplayer2.l.x;
import com.applovin.exoplayer2.g.g;

public final class b extends g
{
    @Nullable
    private static a a(final x x) {
        x.b(12);
        final int c = x.c(12);
        final int c2 = x.c();
        x.b(44);
        x.e(x.c(12));
        x.b(16);
        final ArrayList list = new ArrayList();
        a a;
        while (true) {
            final int c3 = x.c();
            a = null;
            String str = null;
            if (c3 >= c2 + c - 4) {
                break;
            }
            x.b(48);
            final int c4 = x.c(8);
            x.b(4);
            final int n = x.c() + x.c(12);
            String str2 = null;
            while (x.c() < n) {
                final int c5 = x.c(8);
                final int c6 = x.c(8);
                final int n2 = x.c() + c6;
                String s;
                String a2;
                if (c5 == 2) {
                    final int c7 = x.c(16);
                    x.b(8);
                    s = str;
                    a2 = str2;
                    if (c7 == 3) {
                        while (true) {
                            s = str;
                            a2 = str2;
                            if (x.c() >= n2) {
                                break;
                            }
                            final String a3 = x.a(x.c(8), Charsets.US_ASCII);
                            final int c8 = x.c(8);
                            int n3 = 0;
                            while (true) {
                                str = a3;
                                if (n3 >= c8) {
                                    break;
                                }
                                x.e(x.c(8));
                                ++n3;
                            }
                        }
                    }
                }
                else {
                    s = str;
                    a2 = str2;
                    if (c5 == 21) {
                        a2 = x.a(c6, Charsets.US_ASCII);
                        s = str;
                    }
                }
                x.a(n2 * 8);
                str = s;
                str2 = a2;
            }
            x.a(n * 8);
            if (str == null || str2 == null) {
                continue;
            }
            final StringBuilder sb = new StringBuilder();
            sb.append(str);
            sb.append(str2);
            list.add(new com.applovin.exoplayer2.g.a.a(c4, sb.toString()));
        }
        a a4;
        if (list.isEmpty()) {
            a4 = a;
        }
        else {
            a4 = new a(list);
        }
        return a4;
    }
    
    @Nullable
    @Override
    protected a a(final d d, final ByteBuffer byteBuffer) {
        a a;
        if (byteBuffer.get() == 116) {
            a = a(new x(byteBuffer.array(), byteBuffer.limit()));
        }
        else {
            a = null;
        }
        return a;
    }
}

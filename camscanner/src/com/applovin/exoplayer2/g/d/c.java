// 
// Decompiled by Procyon v0.6.0
// 

package com.applovin.exoplayer2.g.d;

import java.util.Arrays;
import com.applovin.exoplayer2.ac;
import com.applovin.exoplayer2.g.\u3007080;
import com.applovin.exoplayer2.v;
import android.os.Parcel;
import androidx.annotation.Nullable;
import android.os.Parcelable$Creator;
import com.applovin.exoplayer2.g.a;

public final class c implements a
{
    public static final Parcelable$Creator<c> CREATOR;
    public final byte[] a;
    @Nullable
    public final String b;
    @Nullable
    public final String c;
    
    static {
        CREATOR = (Parcelable$Creator)new Parcelable$Creator<c>() {
            public c a(final Parcel parcel) {
                return new c(parcel);
            }
            
            public c[] a(final int n) {
                return new c[n];
            }
        };
    }
    
    c(final Parcel parcel) {
        this.a = (byte[])com.applovin.exoplayer2.l.a.b((Object)parcel.createByteArray());
        this.b = parcel.readString();
        this.c = parcel.readString();
    }
    
    public c(final byte[] a, @Nullable final String b, @Nullable final String c) {
        this.a = a;
        this.b = b;
        this.c = c;
    }
    
    @Override
    public void a(final ac.a a) {
        final String b = this.b;
        if (b != null) {
            a.a(b);
        }
    }
    
    public int describeContents() {
        return 0;
    }
    
    @Override
    public boolean equals(@Nullable final Object o) {
        return this == o || (o != null && c.class == o.getClass() && Arrays.equals(this.a, ((c)o).a));
    }
    
    @Override
    public int hashCode() {
        return Arrays.hashCode(this.a);
    }
    
    @Override
    public String toString() {
        return String.format("ICY: title=\"%s\", url=\"%s\", rawMetadata.length=\"%s\"", this.b, this.c, this.a.length);
    }
    
    public void writeToParcel(final Parcel parcel, final int n) {
        parcel.writeByteArray(this.a);
        parcel.writeString(this.b);
        parcel.writeString(this.c);
    }
}

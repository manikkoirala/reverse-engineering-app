// 
// Decompiled by Procyon v0.6.0
// 

package com.applovin.exoplayer2.g.d;

import com.applovin.exoplayer2.ac;
import com.applovin.exoplayer2.g.\u3007080;
import com.applovin.exoplayer2.v;
import java.util.List;
import java.util.Map;
import com.applovin.exoplayer2.l.ai;
import android.os.Parcel;
import androidx.annotation.Nullable;
import android.os.Parcelable$Creator;
import com.applovin.exoplayer2.g.a;

public final class b implements a
{
    public static final Parcelable$Creator<b> CREATOR;
    public final int a;
    @Nullable
    public final String b;
    @Nullable
    public final String c;
    @Nullable
    public final String d;
    public final boolean e;
    public final int f;
    
    static {
        CREATOR = (Parcelable$Creator)new Parcelable$Creator<b>() {
            public b a(final Parcel parcel) {
                return new b(parcel);
            }
            
            public b[] a(final int n) {
                return new b[n];
            }
        };
    }
    
    public b(final int a, @Nullable final String b, @Nullable final String c, @Nullable final String d, final boolean e, final int f) {
        com.applovin.exoplayer2.l.a.a(f == -1 || f > 0);
        this.a = a;
        this.b = b;
        this.c = c;
        this.d = d;
        this.e = e;
        this.f = f;
    }
    
    b(final Parcel parcel) {
        this.a = parcel.readInt();
        this.b = parcel.readString();
        this.c = parcel.readString();
        this.d = parcel.readString();
        this.e = ai.a(parcel);
        this.f = parcel.readInt();
    }
    
    @Nullable
    public static b a(final Map<String, List<String>> p0) {
        // 
        // This method could not be decompiled.
        // 
        // Original Bytecode:
        // 
        //     1: ldc             "icy-br"
        //     3: invokeinterface java/util/Map.get:(Ljava/lang/Object;)Ljava/lang/Object;
        //     8: checkcast       Ljava/util/List;
        //    11: astore          8
        //    13: iconst_1       
        //    14: istore          6
        //    16: iconst_m1      
        //    17: istore_2       
        //    18: aload           8
        //    20: ifnull          146
        //    23: aload           8
        //    25: iconst_0       
        //    26: invokeinterface java/util/List.get:(I)Ljava/lang/Object;
        //    31: checkcast       Ljava/lang/String;
        //    34: astore          8
        //    36: aload           8
        //    38: invokestatic    java/lang/Integer.parseInt:(Ljava/lang/String;)I
        //    41: istore_1       
        //    42: iload_1        
        //    43: sipush          1000
        //    46: imul           
        //    47: istore_3       
        //    48: iload_3        
        //    49: ifle            57
        //    52: iconst_1       
        //    53: istore_1       
        //    54: goto            97
        //    57: new             Ljava/lang/StringBuilder;
        //    60: astore          9
        //    62: aload           9
        //    64: invokespecial   java/lang/StringBuilder.<init>:()V
        //    67: aload           9
        //    69: ldc             "Invalid bitrate: "
        //    71: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //    74: pop            
        //    75: aload           9
        //    77: aload           8
        //    79: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //    82: pop            
        //    83: ldc             "IcyHeaders"
        //    85: aload           9
        //    87: invokevirtual   java/lang/StringBuilder.toString:()Ljava/lang/String;
        //    90: invokestatic    com/applovin/exoplayer2/l/q.c:(Ljava/lang/String;Ljava/lang/String;)V
        //    93: iconst_0       
        //    94: istore_1       
        //    95: iconst_m1      
        //    96: istore_3       
        //    97: goto            150
        //   100: astore          9
        //   102: iconst_m1      
        //   103: istore_1       
        //   104: new             Ljava/lang/StringBuilder;
        //   107: dup            
        //   108: invokespecial   java/lang/StringBuilder.<init>:()V
        //   111: astore          9
        //   113: aload           9
        //   115: ldc             "Invalid bitrate header: "
        //   117: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   120: pop            
        //   121: aload           9
        //   123: aload           8
        //   125: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   128: pop            
        //   129: ldc             "IcyHeaders"
        //   131: aload           9
        //   133: invokevirtual   java/lang/StringBuilder.toString:()Ljava/lang/String;
        //   136: invokestatic    com/applovin/exoplayer2/l/q.c:(Ljava/lang/String;Ljava/lang/String;)V
        //   139: iload_1        
        //   140: istore_3       
        //   141: iconst_0       
        //   142: istore_1       
        //   143: goto            150
        //   146: iconst_0       
        //   147: istore_1       
        //   148: iconst_m1      
        //   149: istore_3       
        //   150: aload_0        
        //   151: ldc             "icy-genre"
        //   153: invokeinterface java/util/Map.get:(Ljava/lang/Object;)Ljava/lang/Object;
        //   158: checkcast       Ljava/util/List;
        //   161: astore          8
        //   163: aconst_null    
        //   164: astore          11
        //   166: aload           8
        //   168: ifnull          189
        //   171: aload           8
        //   173: iconst_0       
        //   174: invokeinterface java/util/List.get:(I)Ljava/lang/Object;
        //   179: checkcast       Ljava/lang/String;
        //   182: astore          8
        //   184: iconst_1       
        //   185: istore_1       
        //   186: goto            192
        //   189: aconst_null    
        //   190: astore          8
        //   192: aload_0        
        //   193: ldc             "icy-name"
        //   195: invokeinterface java/util/Map.get:(Ljava/lang/Object;)Ljava/lang/Object;
        //   200: checkcast       Ljava/util/List;
        //   203: astore          9
        //   205: aload           9
        //   207: ifnull          228
        //   210: aload           9
        //   212: iconst_0       
        //   213: invokeinterface java/util/List.get:(I)Ljava/lang/Object;
        //   218: checkcast       Ljava/lang/String;
        //   221: astore          9
        //   223: iconst_1       
        //   224: istore_1       
        //   225: goto            231
        //   228: aconst_null    
        //   229: astore          9
        //   231: aload_0        
        //   232: ldc             "icy-url"
        //   234: invokeinterface java/util/Map.get:(Ljava/lang/Object;)Ljava/lang/Object;
        //   239: checkcast       Ljava/util/List;
        //   242: astore          10
        //   244: aload           10
        //   246: ifnull          267
        //   249: aload           10
        //   251: iconst_0       
        //   252: invokeinterface java/util/List.get:(I)Ljava/lang/Object;
        //   257: checkcast       Ljava/lang/String;
        //   260: astore          10
        //   262: iconst_1       
        //   263: istore_1       
        //   264: goto            270
        //   267: aconst_null    
        //   268: astore          10
        //   270: aload_0        
        //   271: ldc             "icy-pub"
        //   273: invokeinterface java/util/Map.get:(Ljava/lang/Object;)Ljava/lang/Object;
        //   278: checkcast       Ljava/util/List;
        //   281: astore          12
        //   283: aload           12
        //   285: ifnull          311
        //   288: aload           12
        //   290: iconst_0       
        //   291: invokeinterface java/util/List.get:(I)Ljava/lang/Object;
        //   296: checkcast       Ljava/lang/String;
        //   299: ldc             "1"
        //   301: invokevirtual   java/lang/String.equals:(Ljava/lang/Object;)Z
        //   304: istore          7
        //   306: iconst_1       
        //   307: istore_1       
        //   308: goto            314
        //   311: iconst_0       
        //   312: istore          7
        //   314: aload_0        
        //   315: ldc             "icy-metaint"
        //   317: invokeinterface java/util/Map.get:(Ljava/lang/Object;)Ljava/lang/Object;
        //   322: checkcast       Ljava/util/List;
        //   325: astore_0       
        //   326: iload_1        
        //   327: istore          5
        //   329: iload_2        
        //   330: istore          4
        //   332: aload_0        
        //   333: ifnull          456
        //   336: aload_0        
        //   337: iconst_0       
        //   338: invokeinterface java/util/List.get:(I)Ljava/lang/Object;
        //   343: checkcast       Ljava/lang/String;
        //   346: astore_0       
        //   347: aload_0        
        //   348: invokestatic    java/lang/Integer.parseInt:(Ljava/lang/String;)I
        //   351: istore          4
        //   353: iload           4
        //   355: ifle            367
        //   358: iload           4
        //   360: istore_2       
        //   361: iload           6
        //   363: istore_1       
        //   364: goto            402
        //   367: new             Ljava/lang/StringBuilder;
        //   370: astore          12
        //   372: aload           12
        //   374: invokespecial   java/lang/StringBuilder.<init>:()V
        //   377: aload           12
        //   379: ldc             "Invalid metadata interval: "
        //   381: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   384: pop            
        //   385: aload           12
        //   387: aload_0        
        //   388: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   391: pop            
        //   392: ldc             "IcyHeaders"
        //   394: aload           12
        //   396: invokevirtual   java/lang/StringBuilder.toString:()Ljava/lang/String;
        //   399: invokestatic    com/applovin/exoplayer2/l/q.c:(Ljava/lang/String;Ljava/lang/String;)V
        //   402: iload_1        
        //   403: istore          5
        //   405: iload_2        
        //   406: istore          4
        //   408: goto            456
        //   411: astore          12
        //   413: iload           4
        //   415: istore_2       
        //   416: new             Ljava/lang/StringBuilder;
        //   419: dup            
        //   420: invokespecial   java/lang/StringBuilder.<init>:()V
        //   423: astore          12
        //   425: aload           12
        //   427: ldc             "Invalid metadata interval: "
        //   429: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   432: pop            
        //   433: aload           12
        //   435: aload_0        
        //   436: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   439: pop            
        //   440: ldc             "IcyHeaders"
        //   442: aload           12
        //   444: invokevirtual   java/lang/StringBuilder.toString:()Ljava/lang/String;
        //   447: invokestatic    com/applovin/exoplayer2/l/q.c:(Ljava/lang/String;Ljava/lang/String;)V
        //   450: iload_2        
        //   451: istore          4
        //   453: iload_1        
        //   454: istore          5
        //   456: aload           11
        //   458: astore_0       
        //   459: iload           5
        //   461: ifeq            483
        //   464: new             Lcom/applovin/exoplayer2/g/d/b;
        //   467: dup            
        //   468: iload_3        
        //   469: aload           8
        //   471: aload           9
        //   473: aload           10
        //   475: iload           7
        //   477: iload           4
        //   479: invokespecial   com/applovin/exoplayer2/g/d/b.<init>:(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;ZI)V
        //   482: astore_0       
        //   483: aload_0        
        //   484: areturn        
        //   485: astore          9
        //   487: iload_3        
        //   488: istore_1       
        //   489: goto            104
        //   492: astore          12
        //   494: goto            416
        //    Signature:
        //  (Ljava/util/Map<Ljava/lang/String;Ljava/util/List<Ljava/lang/String;>;>;)Lcom/applovin/exoplayer2/g/d/b;
        //    Exceptions:
        //  Try           Handler
        //  Start  End    Start  End    Type                             
        //  -----  -----  -----  -----  ---------------------------------
        //  36     42     100    104    Ljava/lang/NumberFormatException;
        //  57     93     485    492    Ljava/lang/NumberFormatException;
        //  347    353    492    497    Ljava/lang/NumberFormatException;
        //  367    402    411    416    Ljava/lang/NumberFormatException;
        // 
        // The error that occurred was:
        // 
        // java.lang.IllegalStateException: Expression is linked from several locations: Label_0057:
        //     at com.strobel.decompiler.ast.Error.expressionLinkedFromMultipleLocations(Error.java:27)
        //     at com.strobel.decompiler.ast.AstOptimizer.mergeDisparateObjectInitializations(AstOptimizer.java:2604)
        //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:235)
        //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:42)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:206)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:93)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethodBody(AstBuilder.java:868)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethod(AstBuilder.java:761)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addTypeMembers(AstBuilder.java:638)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeCore(AstBuilder.java:605)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeNoCache(AstBuilder.java:195)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createType(AstBuilder.java:162)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addType(AstBuilder.java:137)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.buildAst(JavaLanguage.java:71)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.decompileType(JavaLanguage.java:59)
        //     at com.strobel.decompiler.DecompilerDriver.decompileType(DecompilerDriver.java:333)
        //     at com.strobel.decompiler.DecompilerDriver.decompileJar(DecompilerDriver.java:254)
        //     at com.strobel.decompiler.DecompilerDriver.main(DecompilerDriver.java:144)
        // 
        throw new IllegalStateException("An error occurred while decompiling this method.");
    }
    
    public int describeContents() {
        return 0;
    }
    
    @Override
    public boolean equals(@Nullable final Object o) {
        boolean b = true;
        if (this == o) {
            return true;
        }
        if (o != null && b.class == o.getClass()) {
            final b b2 = (b)o;
            if (this.a != b2.a || !ai.a((Object)this.b, (Object)b2.b) || !ai.a((Object)this.c, (Object)b2.c) || !ai.a((Object)this.d, (Object)b2.d) || this.e != b2.e || this.f != b2.f) {
                b = false;
            }
            return b;
        }
        return false;
    }
    
    @Override
    public int hashCode() {
        final int a = this.a;
        final String b = this.b;
        int hashCode = 0;
        int hashCode2;
        if (b != null) {
            hashCode2 = b.hashCode();
        }
        else {
            hashCode2 = 0;
        }
        final String c = this.c;
        int hashCode3;
        if (c != null) {
            hashCode3 = c.hashCode();
        }
        else {
            hashCode3 = 0;
        }
        final String d = this.d;
        if (d != null) {
            hashCode = d.hashCode();
        }
        return (((((527 + a) * 31 + hashCode2) * 31 + hashCode3) * 31 + hashCode) * 31 + (this.e ? 1 : 0)) * 31 + this.f;
    }
    
    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append("IcyHeaders: name=\"");
        sb.append(this.c);
        sb.append("\", genre=\"");
        sb.append(this.b);
        sb.append("\", bitrate=");
        sb.append(this.a);
        sb.append(", metadataInterval=");
        sb.append(this.f);
        return sb.toString();
    }
    
    public void writeToParcel(final Parcel parcel, final int n) {
        parcel.writeInt(this.a);
        parcel.writeString(this.b);
        parcel.writeString(this.c);
        parcel.writeString(this.d);
        ai.a(parcel, this.e);
        parcel.writeInt(this.f);
    }
}

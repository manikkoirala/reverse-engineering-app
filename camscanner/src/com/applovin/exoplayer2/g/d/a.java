// 
// Decompiled by Procyon v0.6.0
// 

package com.applovin.exoplayer2.g.d;

import java.util.regex.Matcher;
import com.applovin.exoplayer2.common.base.Ascii;
import com.applovin.exoplayer2.g.d;
import androidx.annotation.Nullable;
import java.nio.charset.CharacterCodingException;
import java.nio.ByteBuffer;
import com.applovin.exoplayer2.common.base.Charsets;
import java.nio.charset.CharsetDecoder;
import java.util.regex.Pattern;
import com.applovin.exoplayer2.g.g;

public final class a extends g
{
    private static final Pattern a;
    private final CharsetDecoder b;
    private final CharsetDecoder c;
    
    static {
        a = Pattern.compile("(.+?)='(.*?)';", 32);
    }
    
    public a() {
        this.b = Charsets.UTF_8.newDecoder();
        this.c = Charsets.ISO_8859_1.newDecoder();
    }
    
    @Nullable
    private String a(final ByteBuffer byteBuffer) {
        try {
            return this.b.decode(byteBuffer).toString();
        }
        catch (final CharacterCodingException ex) {
            this.b.reset();
            byteBuffer.rewind();
            try {
                return this.c.decode(byteBuffer).toString();
            }
            catch (final CharacterCodingException ex2) {
                return null;
            }
            finally {
                this.c.reset();
                byteBuffer.rewind();
            }
        }
        finally {
            this.b.reset();
            byteBuffer.rewind();
        }
    }
    
    @Override
    protected com.applovin.exoplayer2.g.a a(final d d, final ByteBuffer byteBuffer) {
        final String a = this.a(byteBuffer);
        final byte[] dst = new byte[byteBuffer.limit()];
        byteBuffer.get(dst);
        String s = null;
        if (a == null) {
            return new com.applovin.exoplayer2.g.a(new com.applovin.exoplayer2.g.a.a[] { new c(dst, null, null) });
        }
        final Matcher matcher = com.applovin.exoplayer2.g.d.a.a.matcher(a);
        String s2 = null;
        String s3;
        String s4;
        for (int end = 0; matcher.find(end); end = matcher.end(), s = s3, s2 = s4) {
            final String group = matcher.group(1);
            final String group2 = matcher.group(2);
            s3 = s;
            s4 = s2;
            if (group != null) {
                final String lowerCase = Ascii.toLowerCase(group);
                lowerCase.hashCode();
                if (!lowerCase.equals("streamurl")) {
                    if (!lowerCase.equals("streamtitle")) {
                        s3 = s;
                        s4 = s2;
                    }
                    else {
                        s3 = group2;
                        s4 = s2;
                    }
                }
                else {
                    s4 = group2;
                    s3 = s;
                }
            }
        }
        return new com.applovin.exoplayer2.g.a(new com.applovin.exoplayer2.g.a.a[] { new c(dst, s, s2) });
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.applovin.exoplayer2.g;

import android.os.Message;
import com.applovin.exoplayer2.\u3007O00;
import java.nio.ByteBuffer;
import com.applovin.exoplayer2.w;
import com.applovin.exoplayer2.v;
import java.util.List;
import java.util.ArrayList;
import com.applovin.exoplayer2.c.g;
import com.applovin.exoplayer2.l.ai;
import android.os.Looper;
import androidx.annotation.Nullable;
import android.os.Handler;
import android.os.Handler$Callback;
import com.applovin.exoplayer2.e;

public final class f extends e implements Handler$Callback
{
    private final c a;
    private final com.applovin.exoplayer2.g.e b;
    @Nullable
    private final Handler c;
    private final d d;
    @Nullable
    private com.applovin.exoplayer2.g.b e;
    private boolean f;
    private boolean g;
    private long h;
    private long i;
    @Nullable
    private com.applovin.exoplayer2.g.a j;
    
    public f(final com.applovin.exoplayer2.g.e e, @Nullable final Looper looper) {
        this(e, looper, com.applovin.exoplayer2.g.c.a);
    }
    
    public f(final com.applovin.exoplayer2.g.e e, @Nullable final Looper looper, final c c) {
        super(5);
        this.b = (com.applovin.exoplayer2.g.e)com.applovin.exoplayer2.l.a.b((Object)e);
        Handler a;
        if (looper == null) {
            a = null;
        }
        else {
            a = ai.a(looper, (Handler$Callback)this);
        }
        this.c = a;
        this.a = (c)com.applovin.exoplayer2.l.a.b((Object)c);
        this.d = new d();
        this.i = -9223372036854775807L;
    }
    
    private void B() {
        if (!this.f && this.j == null) {
            this.d.a();
            final w t = this.t();
            final int a = this.a(t, this.d, 0);
            if (a == -4) {
                if (this.d.c()) {
                    this.f = true;
                }
                else {
                    final d d = this.d;
                    d.f = this.h;
                    d.h();
                    final com.applovin.exoplayer2.g.a a2 = ((com.applovin.exoplayer2.g.b)ai.a((Object)this.e)).a(this.d);
                    if (a2 != null) {
                        final ArrayList list = new ArrayList<com.applovin.exoplayer2.g.a.a>(a2.a());
                        this.a(a2, (List<com.applovin.exoplayer2.g.a.a>)list);
                        if (!list.isEmpty()) {
                            this.j = new com.applovin.exoplayer2.g.a((List<? extends com.applovin.exoplayer2.g.a.a>)list);
                            this.i = this.d.d;
                        }
                    }
                }
            }
            else if (a == -5) {
                this.h = ((v)com.applovin.exoplayer2.l.a.b((Object)t.b)).p;
            }
        }
    }
    
    private void a(final com.applovin.exoplayer2.g.a a) {
        final Handler c = this.c;
        if (c != null) {
            c.obtainMessage(0, (Object)a).sendToTarget();
        }
        else {
            this.b(a);
        }
    }
    
    private void a(final com.applovin.exoplayer2.g.a a, final List<com.applovin.exoplayer2.g.a.a> list) {
        for (int i = 0; i < a.a(); ++i) {
            final v a2 = a.a(i).a();
            if (a2 != null && this.a.a(a2)) {
                final com.applovin.exoplayer2.g.b b = this.a.b(a2);
                final byte[] src = (byte[])com.applovin.exoplayer2.l.a.b((Object)a.a(i).b());
                this.d.a();
                this.d.f(src.length);
                ((ByteBuffer)ai.a((Object)this.d.b)).put(src);
                this.d.h();
                final com.applovin.exoplayer2.g.a a3 = b.a(this.d);
                if (a3 != null) {
                    this.a(a3, list);
                }
            }
            else {
                list.add(a.a(i));
            }
        }
    }
    
    private void b(final com.applovin.exoplayer2.g.a a) {
        this.b.a(a);
    }
    
    private boolean c(final long n) {
        final com.applovin.exoplayer2.g.a j = this.j;
        boolean b;
        if (j != null && this.i <= n) {
            this.a(j);
            this.j = null;
            this.i = -9223372036854775807L;
            b = true;
        }
        else {
            b = false;
        }
        if (this.f && this.j == null) {
            this.g = true;
        }
        return b;
    }
    
    public boolean A() {
        return this.g;
    }
    
    public int a(final v v) {
        if (this.a.a(v)) {
            int n;
            if (v.E == 0) {
                n = 4;
            }
            else {
                n = 2;
            }
            return \u3007O00.\u3007o00\u3007\u3007Oo(n);
        }
        return \u3007O00.\u3007o00\u3007\u3007Oo(0);
    }
    
    public void a(final long n, final long n2) {
        for (boolean c = true; c; c = this.c(n)) {
            this.B();
        }
    }
    
    @Override
    protected void a(final long n, final boolean b) {
        this.j = null;
        this.i = -9223372036854775807L;
        this.f = false;
        this.g = false;
    }
    
    @Override
    protected void a(final v[] array, final long n, final long n2) {
        this.e = this.a.b(array[0]);
    }
    
    public boolean handleMessage(final Message message) {
        if (message.what == 0) {
            this.b((com.applovin.exoplayer2.g.a)message.obj);
            return true;
        }
        throw new IllegalStateException();
    }
    
    @Override
    protected void r() {
        this.j = null;
        this.i = -9223372036854775807L;
        this.e = null;
    }
    
    public String y() {
        return "MetadataRenderer";
    }
    
    public boolean z() {
        return true;
    }
}

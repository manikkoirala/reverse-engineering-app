// 
// Decompiled by Procyon v0.6.0
// 

package com.applovin.exoplayer2.g.f;

import com.applovin.exoplayer2.l.ai;
import com.applovin.exoplayer2.common.base.Objects;
import com.applovin.exoplayer2.common.a.n;
import java.util.Comparator;
import android.os.Parcelable;
import androidx.annotation.Nullable;
import com.applovin.exoplayer2.ac;
import com.applovin.exoplayer2.g.\u3007080;
import com.applovin.exoplayer2.v;
import java.util.ArrayList;
import android.os.Parcel;
import java.util.List;
import android.os.Parcelable$Creator;
import com.applovin.exoplayer2.g.a;

public final class c implements com.applovin.exoplayer2.g.a.a
{
    public static final Parcelable$Creator<c> CREATOR;
    public final List<a> a;
    
    static {
        CREATOR = (Parcelable$Creator)new Parcelable$Creator<c>() {
            public c a(final Parcel parcel) {
                final ArrayList list = new ArrayList();
                parcel.readList((List)list, a.class.getClassLoader());
                return new c(list);
            }
            
            public c[] a(final int n) {
                return new c[n];
            }
        };
    }
    
    public c(final List<a> a) {
        this.a = a;
        com.applovin.exoplayer2.l.a.a(a(a) ^ true);
    }
    
    private static boolean a(final List<a> list) {
        if (list.isEmpty()) {
            return false;
        }
        long n = list.get(0).c;
        for (int i = 1; i < list.size(); ++i) {
            if (((a)list.get(i)).b < n) {
                return true;
            }
            n = ((a)list.get(i)).c;
        }
        return false;
    }
    
    public int describeContents() {
        return 0;
    }
    
    @Override
    public boolean equals(@Nullable final Object o) {
        return this == o || (o != null && c.class == o.getClass() && this.a.equals(((c)o).a));
    }
    
    @Override
    public int hashCode() {
        return this.a.hashCode();
    }
    
    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append("SlowMotion: segments=");
        sb.append(this.a);
        return sb.toString();
    }
    
    public void writeToParcel(final Parcel parcel, final int n) {
        parcel.writeList((List)this.a);
    }
    
    public static final class a implements Parcelable
    {
        public static final Parcelable$Creator<a> CREATOR;
        public static final Comparator<a> a;
        public final long b;
        public final long c;
        public final int d;
        
        static {
            a = new com.applovin.exoplayer2.g.f.\u3007080();
            CREATOR = (Parcelable$Creator)new Parcelable$Creator<a>() {
                public a a(final Parcel parcel) {
                    return new a(parcel.readLong(), parcel.readLong(), parcel.readInt());
                }
                
                public a[] a(final int n) {
                    return new a[n];
                }
            };
        }
        
        public a(final long b, final long c, final int d) {
            com.applovin.exoplayer2.l.a.a(b < c);
            this.b = b;
            this.c = c;
            this.d = d;
        }
        
        public int describeContents() {
            return 0;
        }
        
        @Override
        public boolean equals(@Nullable final Object o) {
            boolean b = true;
            if (this == o) {
                return true;
            }
            if (o != null && a.class == o.getClass()) {
                final a a = (a)o;
                if (this.b != a.b || this.c != a.c || this.d != a.d) {
                    b = false;
                }
                return b;
            }
            return false;
        }
        
        @Override
        public int hashCode() {
            return Objects.hashCode(this.b, this.c, this.d);
        }
        
        @Override
        public String toString() {
            return ai.a("Segment: startTimeMs=%d, endTimeMs=%d, speedDivisor=%d", new Object[] { this.b, this.c, this.d });
        }
        
        public void writeToParcel(final Parcel parcel, final int n) {
            parcel.writeLong(this.b);
            parcel.writeLong(this.c);
            parcel.writeInt(this.d);
        }
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.applovin.exoplayer2.g.f;

import com.applovin.exoplayer2.common.b.b;
import androidx.annotation.Nullable;
import com.applovin.exoplayer2.ac;
import com.applovin.exoplayer2.g.\u3007080;
import com.applovin.exoplayer2.v;
import android.os.Parcel;
import android.os.Parcelable$Creator;
import com.applovin.exoplayer2.g.a;

public final class d implements a
{
    public static final Parcelable$Creator<d> CREATOR;
    public final float a;
    public final int b;
    
    static {
        CREATOR = (Parcelable$Creator)new Parcelable$Creator<d>() {
            public d a(final Parcel parcel) {
                return new d(parcel, null);
            }
            
            public d[] a(final int n) {
                return new d[n];
            }
        };
    }
    
    public d(final float a, final int b) {
        this.a = a;
        this.b = b;
    }
    
    private d(final Parcel parcel) {
        this.a = parcel.readFloat();
        this.b = parcel.readInt();
    }
    
    public int describeContents() {
        return 0;
    }
    
    @Override
    public boolean equals(@Nullable final Object o) {
        boolean b = true;
        if (this == o) {
            return true;
        }
        if (o != null && d.class == o.getClass()) {
            final d d = (d)o;
            if (this.a != d.a || this.b != d.b) {
                b = false;
            }
            return b;
        }
        return false;
    }
    
    @Override
    public int hashCode() {
        return (527 + com.applovin.exoplayer2.common.b.b.a(this.a)) * 31 + this.b;
    }
    
    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append("smta: captureFrameRate=");
        sb.append(this.a);
        sb.append(", svcTemporalLayerCount=");
        sb.append(this.b);
        return sb.toString();
    }
    
    public void writeToParcel(final Parcel parcel, final int n) {
        parcel.writeFloat(this.a);
        parcel.writeInt(this.b);
    }
}

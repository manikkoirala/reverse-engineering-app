// 
// Decompiled by Procyon v0.6.0
// 

package com.applovin.exoplayer2.g.f;

import java.util.Arrays;
import androidx.annotation.Nullable;
import com.applovin.exoplayer2.ac;
import com.applovin.exoplayer2.g.\u3007080;
import com.applovin.exoplayer2.v;
import com.applovin.exoplayer2.l.ai;
import android.os.Parcel;
import android.os.Parcelable$Creator;

public final class a implements com.applovin.exoplayer2.g.a.a
{
    public static final Parcelable$Creator<a> CREATOR;
    public final String a;
    public final byte[] b;
    public final int c;
    public final int d;
    
    static {
        CREATOR = (Parcelable$Creator)new Parcelable$Creator<a>() {
            public a a(final Parcel parcel) {
                return new a(parcel, null);
            }
            
            public a[] a(final int n) {
                return new a[n];
            }
        };
    }
    
    private a(final Parcel parcel) {
        this.a = (String)ai.a((Object)parcel.readString());
        this.b = (byte[])ai.a((Object)parcel.createByteArray());
        this.c = parcel.readInt();
        this.d = parcel.readInt();
    }
    
    public a(final String a, final byte[] b, final int c, final int d) {
        this.a = a;
        this.b = b;
        this.c = c;
        this.d = d;
    }
    
    public int describeContents() {
        return 0;
    }
    
    @Override
    public boolean equals(@Nullable final Object o) {
        boolean b = true;
        if (this == o) {
            return true;
        }
        if (o != null && a.class == o.getClass()) {
            final a a = (a)o;
            if (!this.a.equals(a.a) || !Arrays.equals(this.b, a.b) || this.c != a.c || this.d != a.d) {
                b = false;
            }
            return b;
        }
        return false;
    }
    
    @Override
    public int hashCode() {
        return (((527 + this.a.hashCode()) * 31 + Arrays.hashCode(this.b)) * 31 + this.c) * 31 + this.d;
    }
    
    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append("mdta: key=");
        sb.append(this.a);
        return sb.toString();
    }
    
    public void writeToParcel(final Parcel parcel, final int n) {
        parcel.writeString(this.a);
        parcel.writeByteArray(this.b);
        parcel.writeInt(this.c);
        parcel.writeInt(this.d);
    }
}

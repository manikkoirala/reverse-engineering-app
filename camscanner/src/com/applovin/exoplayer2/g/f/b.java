// 
// Decompiled by Procyon v0.6.0
// 

package com.applovin.exoplayer2.g.f;

import com.applovin.exoplayer2.common.b.d;
import androidx.annotation.Nullable;
import com.applovin.exoplayer2.ac;
import com.applovin.exoplayer2.g.\u3007080;
import com.applovin.exoplayer2.v;
import android.os.Parcel;
import android.os.Parcelable$Creator;
import com.applovin.exoplayer2.g.a;

public final class b implements a
{
    public static final Parcelable$Creator<b> CREATOR;
    public final long a;
    public final long b;
    public final long c;
    public final long d;
    public final long e;
    
    static {
        CREATOR = (Parcelable$Creator)new Parcelable$Creator<b>() {
            public b a(final Parcel parcel) {
                return new b(parcel, null);
            }
            
            public b[] a(final int n) {
                return new b[n];
            }
        };
    }
    
    public b(final long a, final long b, final long c, final long d, final long e) {
        this.a = a;
        this.b = b;
        this.c = c;
        this.d = d;
        this.e = e;
    }
    
    private b(final Parcel parcel) {
        this.a = parcel.readLong();
        this.b = parcel.readLong();
        this.c = parcel.readLong();
        this.d = parcel.readLong();
        this.e = parcel.readLong();
    }
    
    public int describeContents() {
        return 0;
    }
    
    @Override
    public boolean equals(@Nullable final Object o) {
        boolean b = true;
        if (this == o) {
            return true;
        }
        if (o != null && b.class == o.getClass()) {
            final b b2 = (b)o;
            if (this.a != b2.a || this.b != b2.b || this.c != b2.c || this.d != b2.d || this.e != b2.e) {
                b = false;
            }
            return b;
        }
        return false;
    }
    
    @Override
    public int hashCode() {
        return ((((527 + com.applovin.exoplayer2.common.b.d.a(this.a)) * 31 + com.applovin.exoplayer2.common.b.d.a(this.b)) * 31 + com.applovin.exoplayer2.common.b.d.a(this.c)) * 31 + com.applovin.exoplayer2.common.b.d.a(this.d)) * 31 + com.applovin.exoplayer2.common.b.d.a(this.e);
    }
    
    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append("Motion photo metadata: photoStartPosition=");
        sb.append(this.a);
        sb.append(", photoSize=");
        sb.append(this.b);
        sb.append(", photoPresentationTimestampUs=");
        sb.append(this.c);
        sb.append(", videoStartPosition=");
        sb.append(this.d);
        sb.append(", videoSize=");
        sb.append(this.e);
        return sb.toString();
    }
    
    public void writeToParcel(final Parcel parcel, final int n) {
        parcel.writeLong(this.a);
        parcel.writeLong(this.b);
        parcel.writeLong(this.c);
        parcel.writeLong(this.d);
        parcel.writeLong(this.e);
    }
}

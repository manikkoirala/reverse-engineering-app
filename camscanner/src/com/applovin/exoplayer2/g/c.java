// 
// Decompiled by Procyon v0.6.0
// 

package com.applovin.exoplayer2.g;

import com.applovin.exoplayer2.g.d.a;
import com.applovin.exoplayer2.g.e.g;
import com.applovin.exoplayer2.v;

public interface c
{
    public static final c a = new c() {
        @Override
        public boolean a(final v v) {
            final String l = v.l;
            return "application/id3".equals(l) || "application/x-emsg".equals(l) || "application/x-scte35".equals(l) || "application/x-icy".equals(l) || "application/vnd.dvb.ait".equals(l);
        }
        
        @Override
        public b b(final v v) {
            final String l = v.l;
            if (l != null) {
                final int hashCode = l.hashCode();
                int n = -1;
                switch (hashCode) {
                    case 1652648887: {
                        if (!l.equals("application/x-scte35")) {
                            break;
                        }
                        n = 4;
                        break;
                    }
                    case 1154383568: {
                        if (!l.equals("application/x-emsg")) {
                            break;
                        }
                        n = 3;
                        break;
                    }
                    case -1248341703: {
                        if (!l.equals("application/id3")) {
                            break;
                        }
                        n = 2;
                        break;
                    }
                    case -1348231605: {
                        if (!l.equals("application/x-icy")) {
                            break;
                        }
                        n = 1;
                        break;
                    }
                    case -1354451219: {
                        if (!l.equals("application/vnd.dvb.ait")) {
                            break;
                        }
                        n = 0;
                        break;
                    }
                }
                switch (n) {
                    case 4: {
                        return new com.applovin.exoplayer2.g.g.c();
                    }
                    case 3: {
                        return new com.applovin.exoplayer2.g.b.b();
                    }
                    case 2: {
                        return new g();
                    }
                    case 1: {
                        return new a();
                    }
                    case 0: {
                        return new com.applovin.exoplayer2.g.a.b();
                    }
                }
            }
            final StringBuilder sb = new StringBuilder();
            sb.append("Attempted to create decoder for unsupported MIME type: ");
            sb.append(l);
            throw new IllegalArgumentException(sb.toString());
        }
    };
    
    boolean a(final v p0);
    
    b b(final v p0);
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.applovin.exoplayer2.g.c;

import java.util.Arrays;
import androidx.annotation.Nullable;
import com.applovin.exoplayer2.ac;
import com.applovin.exoplayer2.g.\u3007080;
import com.applovin.exoplayer2.v;
import com.applovin.exoplayer2.l.ai;
import android.os.Parcel;
import android.os.Parcelable$Creator;

public final class a implements com.applovin.exoplayer2.g.a.a
{
    public static final Parcelable$Creator<a> CREATOR;
    public final int a;
    public final String b;
    public final String c;
    public final int d;
    public final int e;
    public final int f;
    public final int g;
    public final byte[] h;
    
    static {
        CREATOR = (Parcelable$Creator)new Parcelable$Creator<a>() {
            public a a(final Parcel parcel) {
                return new a(parcel);
            }
            
            public a[] a(final int n) {
                return new a[n];
            }
        };
    }
    
    public a(final int a, final String b, final String c, final int d, final int e, final int f, final int g, final byte[] h) {
        this.a = a;
        this.b = b;
        this.c = c;
        this.d = d;
        this.e = e;
        this.f = f;
        this.g = g;
        this.h = h;
    }
    
    a(final Parcel parcel) {
        this.a = parcel.readInt();
        this.b = (String)ai.a((Object)parcel.readString());
        this.c = (String)ai.a((Object)parcel.readString());
        this.d = parcel.readInt();
        this.e = parcel.readInt();
        this.f = parcel.readInt();
        this.g = parcel.readInt();
        this.h = (byte[])ai.a((Object)parcel.createByteArray());
    }
    
    @Override
    public void a(final ac.a a) {
        a.a(this.h, this.a);
    }
    
    public int describeContents() {
        return 0;
    }
    
    @Override
    public boolean equals(@Nullable final Object o) {
        boolean b = true;
        if (this == o) {
            return true;
        }
        if (o != null && a.class == o.getClass()) {
            final a a = (a)o;
            if (this.a != a.a || !this.b.equals(a.b) || !this.c.equals(a.c) || this.d != a.d || this.e != a.e || this.f != a.f || this.g != a.g || !Arrays.equals(this.h, a.h)) {
                b = false;
            }
            return b;
        }
        return false;
    }
    
    @Override
    public int hashCode() {
        return (((((((527 + this.a) * 31 + this.b.hashCode()) * 31 + this.c.hashCode()) * 31 + this.d) * 31 + this.e) * 31 + this.f) * 31 + this.g) * 31 + Arrays.hashCode(this.h);
    }
    
    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append("Picture: mimeType=");
        sb.append(this.b);
        sb.append(", description=");
        sb.append(this.c);
        return sb.toString();
    }
    
    public void writeToParcel(final Parcel parcel, final int n) {
        parcel.writeInt(this.a);
        parcel.writeString(this.b);
        parcel.writeString(this.c);
        parcel.writeInt(this.d);
        parcel.writeInt(this.e);
        parcel.writeInt(this.f);
        parcel.writeInt(this.g);
        parcel.writeByteArray(this.h);
    }
}

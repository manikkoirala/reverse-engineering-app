// 
// Decompiled by Procyon v0.6.0
// 

package com.applovin.exoplayer2.g.c;

import androidx.annotation.Nullable;
import com.applovin.exoplayer2.ac;
import com.applovin.exoplayer2.g.\u3007080;
import com.applovin.exoplayer2.v;
import com.applovin.exoplayer2.l.ai;
import android.os.Parcel;
import android.os.Parcelable$Creator;
import com.applovin.exoplayer2.g.a;

public final class b implements a
{
    public static final Parcelable$Creator<b> CREATOR;
    public final String a;
    public final String b;
    
    static {
        CREATOR = (Parcelable$Creator)new Parcelable$Creator<b>() {
            public b a(final Parcel parcel) {
                return new b(parcel);
            }
            
            public b[] a(final int n) {
                return new b[n];
            }
        };
    }
    
    b(final Parcel parcel) {
        this.a = (String)ai.a((Object)parcel.readString());
        this.b = (String)ai.a((Object)parcel.readString());
    }
    
    public b(final String a, final String b) {
        this.a = a;
        this.b = b;
    }
    
    @Override
    public void a(final ac.a a) {
        final String a2 = this.a;
        a2.hashCode();
        final int hashCode = a2.hashCode();
        int n = -1;
        switch (hashCode) {
            case 1939198791: {
                if (!a2.equals("ARTIST")) {
                    break;
                }
                n = 4;
                break;
            }
            case 1746739798: {
                if (!a2.equals("ALBUMARTIST")) {
                    break;
                }
                n = 3;
                break;
            }
            case 428414940: {
                if (!a2.equals("DESCRIPTION")) {
                    break;
                }
                n = 2;
                break;
            }
            case 79833656: {
                if (!a2.equals("TITLE")) {
                    break;
                }
                n = 1;
                break;
            }
            case 62359119: {
                if (!a2.equals("ALBUM")) {
                    break;
                }
                n = 0;
                break;
            }
        }
        switch (n) {
            case 4: {
                a.b(this.b);
                break;
            }
            case 3: {
                a.d(this.b);
                break;
            }
            case 2: {
                a.g(this.b);
                break;
            }
            case 1: {
                a.a(this.b);
                break;
            }
            case 0: {
                a.c(this.b);
                break;
            }
        }
    }
    
    public int describeContents() {
        return 0;
    }
    
    @Override
    public boolean equals(@Nullable final Object o) {
        boolean b = true;
        if (this == o) {
            return true;
        }
        if (o != null && b.class == o.getClass()) {
            final b b2 = (b)o;
            if (!this.a.equals(b2.a) || !this.b.equals(b2.b)) {
                b = false;
            }
            return b;
        }
        return false;
    }
    
    @Override
    public int hashCode() {
        return (527 + this.a.hashCode()) * 31 + this.b.hashCode();
    }
    
    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append("VC: ");
        sb.append(this.a);
        sb.append("=");
        sb.append(this.b);
        return sb.toString();
    }
    
    public void writeToParcel(final Parcel parcel, final int n) {
        parcel.writeString(this.a);
        parcel.writeString(this.b);
    }
}

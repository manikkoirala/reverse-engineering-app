// 
// Decompiled by Procyon v0.6.0
// 

package com.applovin.exoplayer2.g.b;

import java.io.IOException;
import java.io.OutputStream;
import java.io.DataOutputStream;
import java.io.ByteArrayOutputStream;

public final class c
{
    private final ByteArrayOutputStream a;
    private final DataOutputStream b;
    
    public c() {
        final ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream(512);
        this.a = byteArrayOutputStream;
        this.b = new DataOutputStream(byteArrayOutputStream);
    }
    
    private static void a(final DataOutputStream dataOutputStream, final String s) throws IOException {
        dataOutputStream.writeBytes(s);
        dataOutputStream.writeByte(0);
    }
    
    public byte[] a(final a a) {
        this.a.reset();
        try {
            a(this.b, a.a);
            String b = a.b;
            if (b == null) {
                b = "";
            }
            a(this.b, b);
            this.b.writeLong(a.c);
            this.b.writeLong(a.d);
            this.b.write(a.e);
            this.b.flush();
            return this.a.toByteArray();
        }
        catch (final IOException cause) {
            throw new RuntimeException(cause);
        }
    }
}

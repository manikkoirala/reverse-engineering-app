// 
// Decompiled by Procyon v0.6.0
// 

package com.applovin.exoplayer2.g.b;

import java.util.Arrays;
import com.applovin.exoplayer2.g.\u3007080;
import com.applovin.exoplayer2.ac;
import androidx.annotation.Nullable;
import com.applovin.exoplayer2.l.ai;
import android.os.Parcel;
import com.applovin.exoplayer2.v;
import android.os.Parcelable$Creator;

public final class a implements com.applovin.exoplayer2.g.a.a
{
    public static final Parcelable$Creator<a> CREATOR;
    private static final v f;
    private static final v g;
    public final String a;
    public final String b;
    public final long c;
    public final long d;
    public final byte[] e;
    private int h;
    
    static {
        f = new v.a().f("application/id3").a();
        g = new v.a().f("application/x-scte35").a();
        CREATOR = (Parcelable$Creator)new Parcelable$Creator<a>() {
            public a a(final Parcel parcel) {
                return new a(parcel);
            }
            
            public a[] a(final int n) {
                return new a[n];
            }
        };
    }
    
    a(final Parcel parcel) {
        this.a = (String)ai.a((Object)parcel.readString());
        this.b = (String)ai.a((Object)parcel.readString());
        this.c = parcel.readLong();
        this.d = parcel.readLong();
        this.e = (byte[])ai.a((Object)parcel.createByteArray());
    }
    
    public a(final String a, final String b, final long c, final long d, final byte[] e) {
        this.a = a;
        this.b = b;
        this.c = c;
        this.d = d;
        this.e = e;
    }
    
    @Nullable
    @Override
    public v a() {
        final String a = this.a;
        a.hashCode();
        final int hashCode = a.hashCode();
        int n = -1;
        switch (hashCode) {
            case 1303648457: {
                if (!a.equals("https://developer.apple.com/streaming/emsg-id3")) {
                    break;
                }
                n = 2;
                break;
            }
            case -795945609: {
                if (!a.equals("https://aomedia.org/emsg/ID3")) {
                    break;
                }
                n = 1;
                break;
            }
            case -1468477611: {
                if (!a.equals("urn:scte:scte35:2014:bin")) {
                    break;
                }
                n = 0;
                break;
            }
        }
        switch (n) {
            default: {
                return null;
            }
            case 1:
            case 2: {
                return com.applovin.exoplayer2.g.b.a.f;
            }
            case 0: {
                return com.applovin.exoplayer2.g.b.a.g;
            }
        }
    }
    
    @Nullable
    @Override
    public byte[] b() {
        byte[] e;
        if (this.a() != null) {
            e = this.e;
        }
        else {
            e = null;
        }
        return e;
    }
    
    public int describeContents() {
        return 0;
    }
    
    @Override
    public boolean equals(@Nullable final Object o) {
        boolean b = true;
        if (this == o) {
            return true;
        }
        if (o != null && a.class == o.getClass()) {
            final a a = (a)o;
            if (this.c != a.c || this.d != a.d || !ai.a((Object)this.a, (Object)a.a) || !ai.a((Object)this.b, (Object)a.b) || !Arrays.equals(this.e, a.e)) {
                b = false;
            }
            return b;
        }
        return false;
    }
    
    @Override
    public int hashCode() {
        if (this.h == 0) {
            final String a = this.a;
            int hashCode = 0;
            int hashCode2;
            if (a != null) {
                hashCode2 = a.hashCode();
            }
            else {
                hashCode2 = 0;
            }
            final String b = this.b;
            if (b != null) {
                hashCode = b.hashCode();
            }
            final long c = this.c;
            final int n = (int)(c ^ c >>> 32);
            final long d = this.d;
            this.h = ((((527 + hashCode2) * 31 + hashCode) * 31 + n) * 31 + (int)(d ^ d >>> 32)) * 31 + Arrays.hashCode(this.e);
        }
        return this.h;
    }
    
    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append("EMSG: scheme=");
        sb.append(this.a);
        sb.append(", id=");
        sb.append(this.d);
        sb.append(", durationMs=");
        sb.append(this.c);
        sb.append(", value=");
        sb.append(this.b);
        return sb.toString();
    }
    
    public void writeToParcel(final Parcel parcel, final int n) {
        parcel.writeString(this.a);
        parcel.writeString(this.b);
        parcel.writeLong(this.c);
        parcel.writeLong(this.d);
        parcel.writeByteArray(this.e);
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.applovin.exoplayer2.g;

import androidx.annotation.Nullable;
import java.nio.ByteBuffer;

public abstract class g implements b
{
    @Nullable
    @Override
    public final a a(final d d) {
        final ByteBuffer byteBuffer = (ByteBuffer)com.applovin.exoplayer2.l.a.b((Object)d.b);
        com.applovin.exoplayer2.l.a.a(byteBuffer.position() == 0 && byteBuffer.hasArray() && byteBuffer.arrayOffset() == 0);
        a a;
        if (d.b()) {
            a = null;
        }
        else {
            a = this.a(d, byteBuffer);
        }
        return a;
    }
    
    @Nullable
    protected abstract a a(final d p0, final ByteBuffer p1);
}

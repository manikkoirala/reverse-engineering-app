// 
// Decompiled by Procyon v0.6.0
// 

package com.applovin.exoplayer2.g.e;

import com.applovin.exoplayer2.ac;
import java.util.ArrayList;
import java.util.List;
import com.applovin.exoplayer2.l.ai;
import android.os.Parcel;
import androidx.annotation.Nullable;
import android.os.Parcelable$Creator;

public final class l extends h
{
    public static final Parcelable$Creator<l> CREATOR;
    @Nullable
    public final String a;
    public final String b;
    
    static {
        CREATOR = (Parcelable$Creator)new Parcelable$Creator<l>() {
            public l a(final Parcel parcel) {
                return new l(parcel);
            }
            
            public l[] a(final int n) {
                return new l[n];
            }
        };
    }
    
    l(final Parcel parcel) {
        super((String)ai.a((Object)parcel.readString()));
        this.a = parcel.readString();
        this.b = (String)ai.a((Object)parcel.readString());
    }
    
    public l(final String s, @Nullable final String a, final String b) {
        super(s);
        this.a = a;
        this.b = b;
    }
    
    private static List<Integer> a(final String s) {
        final ArrayList list = new ArrayList();
        try {
            if (s.length() >= 10) {
                list.add(Integer.parseInt(s.substring(0, 4)));
                list.add(Integer.parseInt(s.substring(5, 7)));
                list.add(Integer.parseInt(s.substring(8, 10)));
            }
            else if (s.length() >= 7) {
                list.add(Integer.parseInt(s.substring(0, 4)));
                list.add(Integer.parseInt(s.substring(5, 7)));
            }
            else if (s.length() >= 4) {
                list.add(Integer.parseInt(s.substring(0, 4)));
            }
            return list;
        }
        catch (final NumberFormatException ex) {
            return new ArrayList<Integer>();
        }
    }
    
    @Override
    public void a(final ac.a a) {
        final String f = super.f;
        f.hashCode();
        final int hashCode = f.hashCode();
        int n = -1;
        switch (hashCode) {
            case 2590194: {
                if (!f.equals("TYER")) {
                    break;
                }
                n = 21;
                break;
            }
            case 2583398: {
                if (!f.equals("TRCK")) {
                    break;
                }
                n = 20;
                break;
            }
            case 2581514: {
                if (!f.equals("TPE3")) {
                    break;
                }
                n = 19;
                break;
            }
            case 2581513: {
                if (!f.equals("TPE2")) {
                    break;
                }
                n = 18;
                break;
            }
            case 2581512: {
                if (!f.equals("TPE1")) {
                    break;
                }
                n = 17;
                break;
            }
            case 2575251: {
                if (!f.equals("TIT2")) {
                    break;
                }
                n = 16;
                break;
            }
            case 2571565: {
                if (!f.equals("TEXT")) {
                    break;
                }
                n = 15;
                break;
            }
            case 2570410: {
                if (!f.equals("TDRL")) {
                    break;
                }
                n = 14;
                break;
            }
            case 2570401: {
                if (!f.equals("TDRC")) {
                    break;
                }
                n = 13;
                break;
            }
            case 2569891: {
                if (!f.equals("TDAT")) {
                    break;
                }
                n = 12;
                break;
            }
            case 2569357: {
                if (!f.equals("TCOM")) {
                    break;
                }
                n = 11;
                break;
            }
            case 2567331: {
                if (!f.equals("TALB")) {
                    break;
                }
                n = 10;
                break;
            }
            case 83552: {
                if (!f.equals("TYE")) {
                    break;
                }
                n = 9;
                break;
            }
            case 83536: {
                if (!f.equals("TXT")) {
                    break;
                }
                n = 8;
                break;
            }
            case 83378: {
                if (!f.equals("TT2")) {
                    break;
                }
                n = 7;
                break;
            }
            case 83341: {
                if (!f.equals("TRK")) {
                    break;
                }
                n = 6;
                break;
            }
            case 83255: {
                if (!f.equals("TP3")) {
                    break;
                }
                n = 5;
                break;
            }
            case 83254: {
                if (!f.equals("TP2")) {
                    break;
                }
                n = 4;
                break;
            }
            case 83253: {
                if (!f.equals("TP1")) {
                    break;
                }
                n = 3;
                break;
            }
            case 82897: {
                if (!f.equals("TDA")) {
                    break;
                }
                n = 2;
                break;
            }
            case 82878: {
                if (!f.equals("TCM")) {
                    break;
                }
                n = 1;
                break;
            }
            case 82815: {
                if (!f.equals("TAL")) {
                    break;
                }
                n = 0;
                break;
            }
        }
        Label_1083: {
            switch (n) {
                default: {
                    return;
                }
                case 14: {
                    final List<Integer> a2 = a(this.b);
                    final int size = a2.size();
                    if (size != 1) {
                        if (size != 2) {
                            if (size != 3) {
                                return;
                            }
                            a.i((Integer)a2.get(2));
                        }
                        a.h((Integer)a2.get(1));
                    }
                    a.g((Integer)a2.get(0));
                    return;
                }
                case 13: {
                    final List<Integer> a3 = a(this.b);
                    final int size2 = a3.size();
                    if (size2 != 1) {
                        if (size2 != 2) {
                            if (size2 != 3) {
                                return;
                            }
                            a.f((Integer)a3.get(2));
                        }
                        a.e((Integer)a3.get(1));
                    }
                    a.d((Integer)a3.get(0));
                    return;
                }
                case 9:
                case 21: {
                    break Label_1083;
                }
                case 8:
                case 15: {
                    break Label_1083;
                }
                case 7:
                case 16: {
                    break Label_1083;
                }
                case 6:
                case 20: {
                    break Label_1083;
                }
                case 5:
                case 19: {
                    break Label_1083;
                }
                case 4:
                case 18: {
                    break Label_1083;
                }
                case 3:
                case 17: {
                    break Label_1083;
                }
                case 2:
                case 12: {
                    break Label_1083;
                }
                case 1:
                case 11: {
                    break Label_1083;
                }
                case 0:
                case 10: {
                    Label_1095: {
                        break Label_1095;
                        try {
                            a.d(Integer.parseInt(this.b));
                            return;
                            a.d(this.b);
                            return;
                            a.b(this.b);
                            return;
                            a.h(this.b);
                            return;
                            a.i(this.b);
                            return;
                            while (true) {
                                while (true) {
                                    final String[] a4;
                                    final Integer value = Integer.parseInt(a4[1]);
                                    final int int1;
                                    a.a(int1).b(value);
                                    return;
                                    a.j(this.b);
                                    return;
                                    a.c(this.b);
                                    return;
                                    a.a(this.b);
                                    return;
                                    a4 = ai.a(this.b, "/");
                                    int1 = Integer.parseInt(a4[0]);
                                    iftrue(Label_0982:)(a4.length <= 1);
                                    continue;
                                }
                                Label_0982: {
                                    final Integer value = null;
                                }
                                continue;
                            }
                            a.e(Integer.parseInt(this.b.substring(2, 4))).f(Integer.parseInt(this.b.substring(0, 2)));
                            return;
                        }
                        catch (final NumberFormatException | StringIndexOutOfBoundsException ex) {
                            return;
                        }
                    }
                    break;
                }
            }
        }
    }
    
    @Override
    public boolean equals(@Nullable final Object o) {
        boolean b = true;
        if (this == o) {
            return true;
        }
        if (o != null && l.class == o.getClass()) {
            final l l = (l)o;
            if (!ai.a((Object)super.f, (Object)l.f) || !ai.a((Object)this.a, (Object)l.a) || !ai.a((Object)this.b, (Object)l.b)) {
                b = false;
            }
            return b;
        }
        return false;
    }
    
    @Override
    public int hashCode() {
        final int hashCode = super.f.hashCode();
        final String a = this.a;
        int hashCode2 = 0;
        int hashCode3;
        if (a != null) {
            hashCode3 = a.hashCode();
        }
        else {
            hashCode3 = 0;
        }
        final String b = this.b;
        if (b != null) {
            hashCode2 = b.hashCode();
        }
        return ((527 + hashCode) * 31 + hashCode3) * 31 + hashCode2;
    }
    
    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append(super.f);
        sb.append(": description=");
        sb.append(this.a);
        sb.append(": value=");
        sb.append(this.b);
        return sb.toString();
    }
    
    public void writeToParcel(final Parcel parcel, final int n) {
        parcel.writeString(super.f);
        parcel.writeString(this.a);
        parcel.writeString(this.b);
    }
}

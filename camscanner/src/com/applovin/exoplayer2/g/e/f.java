// 
// Decompiled by Procyon v0.6.0
// 

package com.applovin.exoplayer2.g.e;

import java.util.Arrays;
import androidx.annotation.Nullable;
import com.applovin.exoplayer2.l.ai;
import android.os.Parcel;
import android.os.Parcelable$Creator;

public final class f extends h
{
    public static final Parcelable$Creator<f> CREATOR;
    public final String a;
    public final String b;
    public final String c;
    public final byte[] d;
    
    static {
        CREATOR = (Parcelable$Creator)new Parcelable$Creator<f>() {
            public f a(final Parcel parcel) {
                return new f(parcel);
            }
            
            public f[] a(final int n) {
                return new f[n];
            }
        };
    }
    
    f(final Parcel parcel) {
        super("GEOB");
        this.a = (String)ai.a((Object)parcel.readString());
        this.b = (String)ai.a((Object)parcel.readString());
        this.c = (String)ai.a((Object)parcel.readString());
        this.d = (byte[])ai.a((Object)parcel.createByteArray());
    }
    
    public f(final String a, final String b, final String c, final byte[] d) {
        super("GEOB");
        this.a = a;
        this.b = b;
        this.c = c;
        this.d = d;
    }
    
    @Override
    public boolean equals(@Nullable final Object o) {
        boolean b = true;
        if (this == o) {
            return true;
        }
        if (o != null && f.class == o.getClass()) {
            final f f = (f)o;
            if (!ai.a((Object)this.a, (Object)f.a) || !ai.a((Object)this.b, (Object)f.b) || !ai.a((Object)this.c, (Object)f.c) || !Arrays.equals(this.d, f.d)) {
                b = false;
            }
            return b;
        }
        return false;
    }
    
    @Override
    public int hashCode() {
        final String a = this.a;
        int hashCode = 0;
        int hashCode2;
        if (a != null) {
            hashCode2 = a.hashCode();
        }
        else {
            hashCode2 = 0;
        }
        final String b = this.b;
        int hashCode3;
        if (b != null) {
            hashCode3 = b.hashCode();
        }
        else {
            hashCode3 = 0;
        }
        final String c = this.c;
        if (c != null) {
            hashCode = c.hashCode();
        }
        return (((527 + hashCode2) * 31 + hashCode3) * 31 + hashCode) * 31 + Arrays.hashCode(this.d);
    }
    
    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append(super.f);
        sb.append(": mimeType=");
        sb.append(this.a);
        sb.append(", filename=");
        sb.append(this.b);
        sb.append(", description=");
        sb.append(this.c);
        return sb.toString();
    }
    
    public void writeToParcel(final Parcel parcel, final int n) {
        parcel.writeString(this.a);
        parcel.writeString(this.b);
        parcel.writeString(this.c);
        parcel.writeByteArray(this.d);
    }
}

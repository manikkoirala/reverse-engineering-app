// 
// Decompiled by Procyon v0.6.0
// 

package com.applovin.exoplayer2.g.e;

import java.util.Arrays;
import com.applovin.exoplayer2.ac;
import com.applovin.exoplayer2.l.ai;
import android.os.Parcel;
import androidx.annotation.Nullable;
import android.os.Parcelable$Creator;

public final class a extends h
{
    public static final Parcelable$Creator<a> CREATOR;
    public final String a;
    @Nullable
    public final String b;
    public final int c;
    public final byte[] d;
    
    static {
        CREATOR = (Parcelable$Creator)new Parcelable$Creator<a>() {
            public a a(final Parcel parcel) {
                return new a(parcel);
            }
            
            public a[] a(final int n) {
                return new a[n];
            }
        };
    }
    
    a(final Parcel parcel) {
        super("APIC");
        this.a = (String)ai.a((Object)parcel.readString());
        this.b = parcel.readString();
        this.c = parcel.readInt();
        this.d = (byte[])ai.a((Object)parcel.createByteArray());
    }
    
    public a(final String a, @Nullable final String b, final int c, final byte[] d) {
        super("APIC");
        this.a = a;
        this.b = b;
        this.c = c;
        this.d = d;
    }
    
    @Override
    public void a(final ac.a a) {
        a.a(this.d, this.c);
    }
    
    @Override
    public boolean equals(@Nullable final Object o) {
        boolean b = true;
        if (this == o) {
            return true;
        }
        if (o != null && a.class == o.getClass()) {
            final a a = (a)o;
            if (this.c != a.c || !ai.a((Object)this.a, (Object)a.a) || !ai.a((Object)this.b, (Object)a.b) || !Arrays.equals(this.d, a.d)) {
                b = false;
            }
            return b;
        }
        return false;
    }
    
    @Override
    public int hashCode() {
        final int c = this.c;
        final String a = this.a;
        int hashCode = 0;
        int hashCode2;
        if (a != null) {
            hashCode2 = a.hashCode();
        }
        else {
            hashCode2 = 0;
        }
        final String b = this.b;
        if (b != null) {
            hashCode = b.hashCode();
        }
        return (((527 + c) * 31 + hashCode2) * 31 + hashCode) * 31 + Arrays.hashCode(this.d);
    }
    
    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append(super.f);
        sb.append(": mimeType=");
        sb.append(this.a);
        sb.append(", description=");
        sb.append(this.b);
        return sb.toString();
    }
    
    public void writeToParcel(final Parcel parcel, final int n) {
        parcel.writeString(this.a);
        parcel.writeString(this.b);
        parcel.writeInt(this.c);
        parcel.writeByteArray(this.d);
    }
}

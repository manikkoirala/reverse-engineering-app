// 
// Decompiled by Procyon v0.6.0
// 

package com.applovin.exoplayer2.g.e;

import android.os.Parcelable;
import java.util.Arrays;
import androidx.annotation.Nullable;
import com.applovin.exoplayer2.l.ai;
import android.os.Parcel;
import android.os.Parcelable$Creator;

public final class c extends h
{
    public static final Parcelable$Creator<c> CREATOR;
    public final String a;
    public final int b;
    public final int c;
    public final long d;
    public final long e;
    private final h[] g;
    
    static {
        CREATOR = (Parcelable$Creator)new Parcelable$Creator<c>() {
            public c a(final Parcel parcel) {
                return new c(parcel);
            }
            
            public c[] a(final int n) {
                return new c[n];
            }
        };
    }
    
    c(final Parcel parcel) {
        super("CHAP");
        this.a = (String)ai.a((Object)parcel.readString());
        this.b = parcel.readInt();
        this.c = parcel.readInt();
        this.d = parcel.readLong();
        this.e = parcel.readLong();
        final int int1 = parcel.readInt();
        this.g = new h[int1];
        for (int i = 0; i < int1; ++i) {
            this.g[i] = (h)parcel.readParcelable(h.class.getClassLoader());
        }
    }
    
    public c(final String a, final int b, final int c, final long d, final long e, final h[] g) {
        super("CHAP");
        this.a = a;
        this.b = b;
        this.c = c;
        this.d = d;
        this.e = e;
        this.g = g;
    }
    
    @Override
    public int describeContents() {
        return 0;
    }
    
    @Override
    public boolean equals(@Nullable final Object o) {
        boolean b = true;
        if (this == o) {
            return true;
        }
        if (o != null && c.class == o.getClass()) {
            final c c = (c)o;
            if (this.b != c.b || this.c != c.c || this.d != c.d || this.e != c.e || !ai.a((Object)this.a, (Object)c.a) || !Arrays.equals(this.g, c.g)) {
                b = false;
            }
            return b;
        }
        return false;
    }
    
    @Override
    public int hashCode() {
        final int b = this.b;
        final int c = this.c;
        final int n = (int)this.d;
        final int n2 = (int)this.e;
        final String a = this.a;
        int hashCode;
        if (a != null) {
            hashCode = a.hashCode();
        }
        else {
            hashCode = 0;
        }
        return ((((527 + b) * 31 + c) * 31 + n) * 31 + n2) * 31 + hashCode;
    }
    
    public void writeToParcel(final Parcel parcel, int i) {
        parcel.writeString(this.a);
        parcel.writeInt(this.b);
        parcel.writeInt(this.c);
        parcel.writeLong(this.d);
        parcel.writeLong(this.e);
        parcel.writeInt(this.g.length);
        final h[] g = this.g;
        int length;
        for (length = g.length, i = 0; i < length; ++i) {
            parcel.writeParcelable((Parcelable)g[i], 0);
        }
    }
}

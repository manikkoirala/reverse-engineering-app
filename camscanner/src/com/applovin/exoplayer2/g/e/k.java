// 
// Decompiled by Procyon v0.6.0
// 

package com.applovin.exoplayer2.g.e;

import java.util.Arrays;
import androidx.annotation.Nullable;
import com.applovin.exoplayer2.l.ai;
import android.os.Parcel;
import android.os.Parcelable$Creator;

public final class k extends h
{
    public static final Parcelable$Creator<k> CREATOR;
    public final String a;
    public final byte[] b;
    
    static {
        CREATOR = (Parcelable$Creator)new Parcelable$Creator<k>() {
            public k a(final Parcel parcel) {
                return new k(parcel);
            }
            
            public k[] a(final int n) {
                return new k[n];
            }
        };
    }
    
    k(final Parcel parcel) {
        super("PRIV");
        this.a = (String)ai.a((Object)parcel.readString());
        this.b = (byte[])ai.a((Object)parcel.createByteArray());
    }
    
    public k(final String a, final byte[] b) {
        super("PRIV");
        this.a = a;
        this.b = b;
    }
    
    @Override
    public boolean equals(@Nullable final Object o) {
        boolean b = true;
        if (this == o) {
            return true;
        }
        if (o != null && k.class == o.getClass()) {
            final k k = (k)o;
            if (!ai.a((Object)this.a, (Object)k.a) || !Arrays.equals(this.b, k.b)) {
                b = false;
            }
            return b;
        }
        return false;
    }
    
    @Override
    public int hashCode() {
        final String a = this.a;
        int hashCode;
        if (a != null) {
            hashCode = a.hashCode();
        }
        else {
            hashCode = 0;
        }
        return (527 + hashCode) * 31 + Arrays.hashCode(this.b);
    }
    
    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append(super.f);
        sb.append(": owner=");
        sb.append(this.a);
        return sb.toString();
    }
    
    public void writeToParcel(final Parcel parcel, final int n) {
        parcel.writeString(this.a);
        parcel.writeByteArray(this.b);
    }
}

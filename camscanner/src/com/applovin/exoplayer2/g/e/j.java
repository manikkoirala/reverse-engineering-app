// 
// Decompiled by Procyon v0.6.0
// 

package com.applovin.exoplayer2.g.e;

import java.util.Arrays;
import androidx.annotation.Nullable;
import com.applovin.exoplayer2.l.ai;
import android.os.Parcel;
import android.os.Parcelable$Creator;

public final class j extends h
{
    public static final Parcelable$Creator<j> CREATOR;
    public final int a;
    public final int b;
    public final int c;
    public final int[] d;
    public final int[] e;
    
    static {
        CREATOR = (Parcelable$Creator)new Parcelable$Creator<j>() {
            public j a(final Parcel parcel) {
                return new j(parcel);
            }
            
            public j[] a(final int n) {
                return new j[n];
            }
        };
    }
    
    public j(final int a, final int b, final int c, final int[] d, final int[] e) {
        super("MLLT");
        this.a = a;
        this.b = b;
        this.c = c;
        this.d = d;
        this.e = e;
    }
    
    j(final Parcel parcel) {
        super("MLLT");
        this.a = parcel.readInt();
        this.b = parcel.readInt();
        this.c = parcel.readInt();
        this.d = (int[])ai.a((Object)parcel.createIntArray());
        this.e = (int[])ai.a((Object)parcel.createIntArray());
    }
    
    @Override
    public int describeContents() {
        return 0;
    }
    
    @Override
    public boolean equals(@Nullable final Object o) {
        boolean b = true;
        if (this == o) {
            return true;
        }
        if (o != null && j.class == o.getClass()) {
            final j j = (j)o;
            if (this.a != j.a || this.b != j.b || this.c != j.c || !Arrays.equals(this.d, j.d) || !Arrays.equals(this.e, j.e)) {
                b = false;
            }
            return b;
        }
        return false;
    }
    
    @Override
    public int hashCode() {
        return ((((527 + this.a) * 31 + this.b) * 31 + this.c) * 31 + Arrays.hashCode(this.d)) * 31 + Arrays.hashCode(this.e);
    }
    
    public void writeToParcel(final Parcel parcel, final int n) {
        parcel.writeInt(this.a);
        parcel.writeInt(this.b);
        parcel.writeInt(this.c);
        parcel.writeIntArray(this.d);
        parcel.writeIntArray(this.e);
    }
}

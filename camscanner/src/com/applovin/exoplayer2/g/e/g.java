// 
// Decompiled by Procyon v0.6.0
// 

package com.applovin.exoplayer2.g.e;

import java.util.List;
import java.nio.ByteBuffer;
import com.applovin.exoplayer2.l.x;
import java.util.Arrays;
import com.applovin.exoplayer2.l.ai;
import java.util.Locale;
import com.applovin.exoplayer2.l.q;
import java.util.ArrayList;
import java.io.UnsupportedEncodingException;
import com.applovin.exoplayer2.common.base.Ascii;
import com.applovin.exoplayer2.l.y;
import androidx.annotation.Nullable;

public final class g extends com.applovin.exoplayer2.g.g
{
    public static final a a;
    @Nullable
    private final a b;
    
    static {
        a = (a)new \u3007080();
    }
    
    public g() {
        this(null);
    }
    
    public g(@Nullable final a b) {
        this.b = b;
    }
    
    private static int a(final byte[] array, final int n, final int n2) {
        final int b = b(array, n);
        if (n2 != 0) {
            int i = b;
            if (n2 != 3) {
                while (i < array.length - 1) {
                    if ((i - n) % 2 == 0 && array[i + 1] == 0) {
                        return i;
                    }
                    i = b(array, i + 1);
                }
                return array.length;
            }
        }
        return b;
    }
    
    private static com.applovin.exoplayer2.g.e.a a(final y y, int b, int n) throws UnsupportedEncodingException {
        final int h = y.h();
        final String a = a(h);
        final int n2 = b - 1;
        final byte[] bytes = new byte[n2];
        y.a(bytes, 0, n2);
        String str;
        if (n == 2) {
            final StringBuilder sb = new StringBuilder();
            sb.append("image/");
            sb.append(Ascii.toLowerCase(new String(bytes, 0, 3, "ISO-8859-1")));
            if ("image/jpg".equals(str = sb.toString())) {
                str = "image/jpeg";
            }
            b = 2;
        }
        else {
            b = b(bytes, 0);
            str = Ascii.toLowerCase(new String(bytes, 0, b, "ISO-8859-1"));
            if (str.indexOf(47) == -1) {
                final StringBuilder sb2 = new StringBuilder();
                sb2.append("image/");
                sb2.append(str);
                str = sb2.toString();
            }
        }
        n = bytes[b + 1];
        b += 2;
        final int a2 = a(bytes, b, h);
        return new com.applovin.exoplayer2.g.e.a(str, new String(bytes, b, a2 - b, a), n & 0xFF, b(bytes, a2 + b(h), n2));
    }
    
    private static c a(final y y, final int n, final int n2, final boolean b, final int n3, @Nullable final a a) throws UnsupportedEncodingException {
        final int c = y.c();
        final int b2 = b(y.d(), c);
        final String s = new String(y.d(), c, b2 - c, "ISO-8859-1");
        y.d(b2 + 1);
        final int q = y.q();
        final int q2 = y.q();
        long o = y.o();
        if (o == 4294967295L) {
            o = -1L;
        }
        long o2 = y.o();
        if (o2 == 4294967295L) {
            o2 = -1L;
        }
        final ArrayList<h> list = new ArrayList<h>();
        while (y.c() < c + n) {
            final h a2 = a(n2, y, b, n3, a);
            if (a2 != null) {
                list.add(a2);
            }
        }
        return new c(s, q, q2, o, o2, list.toArray(new h[0]));
    }
    
    @Nullable
    private static b a(final y y) {
        if (y.a() < 10) {
            q.c("Id3Decoder", "Data too short to be an ID3 tag");
            return null;
        }
        final int m = y.m();
        final boolean b = false;
        if (m != 4801587) {
            final StringBuilder sb = new StringBuilder();
            sb.append("Unexpected first three bytes of ID3 tag header: 0x");
            sb.append(String.format("%06X", m));
            q.c("Id3Decoder", sb.toString());
            return null;
        }
        final int h = y.h();
        y.e(1);
        final int h2 = y.h();
        final int v = y.v();
        int n;
        if (h == 2) {
            final boolean b2 = (h2 & 0x40) != 0x0;
            n = v;
            if (b2) {
                q.c("Id3Decoder", "Skipped ID3 tag with majorVersion=2 and undefined compression scheme");
                return null;
            }
        }
        else if (h == 3) {
            final boolean b3 = (h2 & 0x40) != 0x0;
            n = v;
            if (b3) {
                final int q = y.q();
                y.e(q);
                n = v - (q + 4);
            }
        }
        else {
            if (h != 4) {
                final StringBuilder sb2 = new StringBuilder();
                sb2.append("Skipped ID3 tag with unsupported majorVersion=");
                sb2.append(h);
                q.c("Id3Decoder", sb2.toString());
                return null;
            }
            final boolean b4 = (h2 & 0x40) != 0x0;
            int n2 = v;
            if (b4) {
                final int v2 = y.v();
                y.e(v2 - 4);
                n2 = v - v2;
            }
            final boolean b5 = (h2 & 0x10) != 0x0;
            n = n2;
            if (b5) {
                n = n2 - 10;
            }
        }
        boolean b6 = b;
        if (h < 4) {
            b6 = b;
            if ((h2 & 0x80) != 0x0) {
                b6 = true;
            }
        }
        return new b(h, b6, n);
    }
    
    @Nullable
    private static h a(final int n, final y y, final boolean b, final int n2, @Nullable final a a) {
        final int h = y.h();
        final int h2 = y.h();
        final int h3 = y.h();
        int h4;
        if (n >= 3) {
            h4 = y.h();
        }
        else {
            h4 = 0;
        }
        int n4;
        if (n == 4) {
            final int n3 = n4 = y.w();
            if (!b) {
                n4 = ((n3 >> 24 & 0xFF) << 21 | ((n3 & 0xFF) | (n3 >> 8 & 0xFF) << 7 | (n3 >> 16 & 0xFF) << 14));
            }
        }
        else if (n == 3) {
            n4 = y.w();
        }
        else {
            n4 = y.m();
        }
        final int n5 = n4;
        int i;
        if (n >= 3) {
            i = y.i();
        }
        else {
            i = 0;
        }
        if (h == 0 && h2 == 0 && h3 == 0 && h4 == 0 && n5 == 0 && i == 0) {
            y.d(y.b());
            return null;
        }
        final int n6 = y.c() + n5;
        if (n6 > y.b()) {
            q.c("Id3Decoder", "Frame size exceeds remaining tag data");
            y.d(y.b());
            return null;
        }
        if (a != null && !a.evaluate(n, h, h2, h3, h4)) {
            y.d(n6);
            return null;
        }
        int n7;
        boolean b2;
        boolean b3;
        int n8;
        boolean b4;
        if (n == 3) {
            if ((i & 0x80) != 0x0) {
                n7 = 1;
            }
            else {
                n7 = 0;
            }
            b2 = ((i & 0x40) != 0x0);
            b3 = ((i & 0x20) != 0x0);
            n8 = n7;
            b4 = false;
        }
        else if (n == 4) {
            b3 = ((i & 0x40) != 0x0);
            final boolean b5 = (i & 0x8) != 0x0;
            b2 = ((i & 0x4) != 0x0);
            b4 = ((i & 0x2) != 0x0);
            final boolean b6 = (i & 0x1) != 0x0;
            n8 = (b5 ? 1 : 0);
            n7 = (b6 ? 1 : 0);
        }
        else {
            b3 = false;
            n7 = 0;
            b2 = false;
            b4 = false;
            n8 = 0;
        }
        if (n8 != 0 || b2) {
            q.c("Id3Decoder", "Skipping unsupported compressed or encrypted frame");
            y.d(n6);
            return null;
        }
        int n9 = n5;
        if (b3) {
            n9 = n5 - 1;
            y.e(1);
        }
        int n10 = n9;
        if (n7 != 0) {
            n10 = n9 - 4;
            y.e(4);
        }
        int g = n10;
        if (b4) {
            g = g(y, n10);
        }
        Label_0553: {
            if (h != 84 || h2 != 88 || h3 != 88) {
                break Label_0553;
            }
            if (n != 2) {
                if (h4 != 88) {
                    break Label_0553;
                }
            }
            try {
                h h5 = null;
                Label_1005: {
                    try {
                        a(y, g);
                        break Label_1005;
                        iftrue(Label_0588:)(h != 84);
                        a(y, g, a(n, h, h2, h3, h4));
                        break Label_1005;
                    }
                    finally {
                        break Label_0553;
                    }
                    Label_0588: {
                        if (h == 87 && h2 == 88 && h3 == 88 && (n == 2 || h4 == 88)) {
                            h5 = b(y, g);
                        }
                        else if (h == 87) {
                            h5 = b(y, g, a(n, h, h2, h3, h4));
                        }
                        else if (h == 80 && h2 == 82 && h3 == 73 && h4 == 86) {
                            h5 = c(y, g);
                        }
                        else if (h == 71 && h2 == 69 && h3 == 79 && (h4 == 66 || n == 2)) {
                            h5 = d(y, g);
                        }
                        else {
                            Label_0814: {
                                if (n == 2) {
                                    if (h != 80 || h2 != 73 || h3 != 67) {
                                        break Label_0814;
                                    }
                                }
                                else if (h != 65 || h2 != 80 || h3 != 73 || h4 != 67) {
                                    break Label_0814;
                                }
                                h5 = a(y, g, n);
                                break Label_1005;
                            }
                            if (h == 67 && h2 == 79 && h3 == 77 && (h4 == 77 || n == 2)) {
                                h5 = e(y, g);
                            }
                            else if (h == 67 && h2 == 72 && h3 == 65 && h4 == 80) {
                                h5 = a(y, g, n, b, n2, (a)h5);
                            }
                            else if (h == 67 && h2 == 84 && h3 == 79 && h4 == 67) {
                                h5 = b(y, g, n, b, n2, (a)h5);
                            }
                            else if (h == 77 && h2 == 76 && h3 == 76 && h4 == 84) {
                                h5 = f(y, g);
                            }
                            else {
                                h5 = c(y, g, a(n, h, h2, h3, h4));
                            }
                        }
                    }
                }
                if (h5 == null) {
                    final StringBuilder sb = new StringBuilder();
                    sb.append("Failed to decode frame: id=");
                    sb.append(a(n, h, h2, h3, h4));
                    sb.append(", frameSize=");
                    sb.append(g);
                    q.c("Id3Decoder", sb.toString());
                }
                y.d(n6);
                return h5;
            }
            catch (final UnsupportedEncodingException ex) {
                q.c("Id3Decoder", "Unsupported character encoding");
                y.d(n6);
                return null;
            }
        }
        y.d(n6);
    }
    
    @Nullable
    private static l a(final y y, int a) throws UnsupportedEncodingException {
        if (a < 1) {
            return null;
        }
        final int h = y.h();
        final String a2 = a(h);
        final byte[] bytes = new byte[--a];
        y.a(bytes, 0, a);
        a = a(bytes, 0, h);
        final String s = new String(bytes, 0, a, a2);
        a += b(h);
        return new l("TXXX", s, a(bytes, a, a(bytes, a, h), a2));
    }
    
    @Nullable
    private static l a(final y y, int n, final String s) throws UnsupportedEncodingException {
        if (n < 1) {
            return null;
        }
        final int h = y.h();
        final String a = a(h);
        final byte[] bytes = new byte[--n];
        y.a(bytes, 0, n);
        return new l(s, null, new String(bytes, 0, a(bytes, 0, h), a));
    }
    
    private static String a(final int n) {
        if (n == 1) {
            return "UTF-16";
        }
        if (n == 2) {
            return "UTF-16BE";
        }
        if (n != 3) {
            return "ISO-8859-1";
        }
        return "UTF-8";
    }
    
    private static String a(final int n, final int n2, final int n3, final int n4, final int i) {
        String s;
        if (n == 2) {
            s = String.format(Locale.US, "%c%c%c", n2, n3, n4);
        }
        else {
            s = String.format(Locale.US, "%c%c%c%c", n2, n3, n4, i);
        }
        return s;
    }
    
    private static String a(final byte[] bytes, final int offset, final int n, final String charsetName) throws UnsupportedEncodingException {
        if (n > offset && n <= bytes.length) {
            return new String(bytes, offset, n - offset, charsetName);
        }
        return "";
    }
    
    private static boolean a(final y y, final int n, final int n2, final boolean b) {
        final int c = y.c();
        try {
            while (true) {
                final int a = y.a();
                final boolean b2 = true;
                if (a < n2) {
                    return true;
                }
                int n3;
                long o;
                int i;
                if (n >= 3) {
                    n3 = y.q();
                    o = y.o();
                    i = y.i();
                }
                else {
                    n3 = y.m();
                    o = y.m();
                    i = 0;
                }
                if (n3 == 0 && o == 0L && i == 0) {
                    return true;
                }
                long n4 = o;
                if (n == 4) {
                    n4 = o;
                    if (!b) {
                        if ((0x808080L & o) != 0x0L) {
                            return false;
                        }
                        n4 = ((o >> 24 & 0xFFL) << 21 | ((o & 0xFFL) | (o >> 8 & 0xFFL) << 7 | (o >> 16 & 0xFFL) << 14));
                    }
                }
                int n6 = 0;
                int n7 = 0;
                Label_0277: {
                    if (n == 4) {
                        final int n5 = n6 = (((i & 0x40) != 0x0) ? 1 : 0);
                        if ((i & 0x1) != 0x0) {
                            n6 = n5;
                            n7 = (b2 ? 1 : 0);
                            break Label_0277;
                        }
                    }
                    else if (n == 3) {
                        int n8;
                        if ((i & 0x20) != 0x0) {
                            n8 = 1;
                        }
                        else {
                            n8 = 0;
                        }
                        n6 = n8;
                        if ((i & 0x80) != 0x0) {
                            n6 = n8;
                            n7 = (b2 ? 1 : 0);
                            break Label_0277;
                        }
                    }
                    else {
                        n6 = 0;
                    }
                    n7 = 0;
                }
                int n9 = n6;
                if (n7 != 0) {
                    n9 = n6 + 4;
                }
                if (n4 < n9) {
                    return false;
                }
                if (y.a() < n4) {
                    return false;
                }
                y.e((int)n4);
            }
        }
        finally {
            y.d(c);
        }
    }
    
    private static int b(int n) {
        if (n != 0 && n != 3) {
            n = 2;
        }
        else {
            n = 1;
        }
        return n;
    }
    
    private static int b(final byte[] array, int i) {
        while (i < array.length) {
            if (array[i] == 0) {
                return i;
            }
            ++i;
        }
        return array.length;
    }
    
    private static d b(final y y, final int n, final int n2, final boolean b, final int n3, @Nullable final a a) throws UnsupportedEncodingException {
        final int c = y.c();
        final int b2 = b(y.d(), c);
        final String s = new String(y.d(), c, b2 - c, "ISO-8859-1");
        y.d(b2 + 1);
        final int h = y.h();
        final boolean b3 = (h & 0x2) != 0x0;
        final boolean b4 = (h & 0x1) != 0x0;
        final int h2 = y.h();
        final String[] array = new String[h2];
        for (int i = 0; i < h2; ++i) {
            final int c2 = y.c();
            final int b5 = b(y.d(), c2);
            array[i] = new String(y.d(), c2, b5 - c2, "ISO-8859-1");
            y.d(b5 + 1);
        }
        final ArrayList<h> list = new ArrayList<h>();
        while (y.c() < c + n) {
            final h a2 = a(n2, y, b, n3, a);
            if (a2 != null) {
                list.add(a2);
            }
        }
        return new d(s, b3, b4, array, list.toArray(new h[0]));
    }
    
    @Nullable
    private static m b(final y y, int a) throws UnsupportedEncodingException {
        if (a < 1) {
            return null;
        }
        final int h = y.h();
        final String a2 = a(h);
        final byte[] bytes = new byte[--a];
        y.a(bytes, 0, a);
        a = a(bytes, 0, h);
        final String s = new String(bytes, 0, a, a2);
        a += b(h);
        return new m("WXXX", s, a(bytes, a, b(bytes, a), "ISO-8859-1"));
    }
    
    private static m b(final y y, final int n, final String s) throws UnsupportedEncodingException {
        final byte[] bytes = new byte[n];
        y.a(bytes, 0, n);
        return new m(s, null, new String(bytes, 0, b(bytes, 0), "ISO-8859-1"));
    }
    
    private static byte[] b(final byte[] original, final int from, final int to) {
        if (to <= from) {
            return ai.f;
        }
        return Arrays.copyOfRange(original, from, to);
    }
    
    private static com.applovin.exoplayer2.g.e.b c(final y y, final int n, final String s) {
        final byte[] array = new byte[n];
        y.a(array, 0, n);
        return new com.applovin.exoplayer2.g.e.b(s, array);
    }
    
    private static k c(final y y, final int n) throws UnsupportedEncodingException {
        final byte[] bytes = new byte[n];
        y.a(bytes, 0, n);
        final int b = b(bytes, 0);
        return new k(new String(bytes, 0, b, "ISO-8859-1"), b(bytes, b + 1, n));
    }
    
    private static f d(final y y, int n) throws UnsupportedEncodingException {
        final int h = y.h();
        final String a = a(h);
        final byte[] bytes = new byte[--n];
        y.a(bytes, 0, n);
        int b = b(bytes, 0);
        final String s = new String(bytes, 0, b, "ISO-8859-1");
        ++b;
        final int a2 = a(bytes, b, h);
        final String a3 = a(bytes, b, a2, a);
        final int n2 = a2 + b(h);
        final int a4 = a(bytes, n2, h);
        return new f(s, a3, a(bytes, n2, a4, a), b(bytes, a4 + b(h), n));
    }
    
    @Nullable
    private static e e(final y y, int a) throws UnsupportedEncodingException {
        if (a < 4) {
            return null;
        }
        final int h = y.h();
        final String a2 = a(h);
        final byte[] bytes = new byte[3];
        y.a(bytes, 0, 3);
        final String s = new String(bytes, 0, 3);
        a -= 4;
        final byte[] bytes2 = new byte[a];
        y.a(bytes2, 0, a);
        a = a(bytes2, 0, h);
        final String s2 = new String(bytes2, 0, a, a2);
        a += b(h);
        return new e(s, s2, a(bytes2, a, a(bytes2, a, h), a2));
    }
    
    private static j f(final y y, int i) {
        final int j = y.i();
        final int m = y.m();
        final int k = y.m();
        final int h = y.h();
        final int h2 = y.h();
        final x x = new x();
        x.a(y);
        final int n = (i - 10) * 8 / (h + h2);
        final int[] array = new int[n];
        final int[] array2 = new int[n];
        int c;
        int c2;
        for (i = 0; i < n; ++i) {
            c = x.c(h);
            c2 = x.c(h2);
            array[i] = c;
            array2[i] = c2;
        }
        return new j(j, m, k, array, array2);
    }
    
    private static int g(final y y, int n) {
        final byte[] d = y.d();
        int c;
        final int n2 = c = y.c();
        int n3 = n;
        while (true) {
            final int n4 = c + 1;
            if (n4 >= n2 + n3) {
                break;
            }
            n = n3;
            if ((d[c] & 0xFF) == 0xFF) {
                n = n3;
                if (d[n4] == 0) {
                    System.arraycopy(d, c + 2, d, n4, n3 - (c - n2) - 2);
                    n = n3 - 1;
                }
            }
            c = n4;
            n3 = n;
        }
        return n3;
    }
    
    @Nullable
    @Override
    protected com.applovin.exoplayer2.g.a a(final com.applovin.exoplayer2.g.d d, final ByteBuffer byteBuffer) {
        return this.a(byteBuffer.array(), byteBuffer.limit());
    }
    
    @Nullable
    public com.applovin.exoplayer2.g.a a(final byte[] array, int n) {
        final ArrayList list = new ArrayList();
        final y y = new y(array, n);
        final b a = a(y);
        if (a == null) {
            return null;
        }
        final int c = y.c();
        if (a.a == 2) {
            n = 6;
        }
        else {
            n = 10;
        }
        int n2 = a.c;
        if (a.b) {
            n2 = g(y, a.c);
        }
        y.c(c + n2);
        final int a2 = a.a;
        boolean b = false;
        if (!a(y, a2, n, false)) {
            if (a.a != 4 || !a(y, 4, n, true)) {
                final StringBuilder sb = new StringBuilder();
                sb.append("Failed to validate ID3 tag with majorVersion=");
                sb.append(a.a);
                q.c("Id3Decoder", sb.toString());
                return null;
            }
            b = true;
        }
        while (y.a() >= n) {
            final h a3 = a(a.a, y, b, n, this.b);
            if (a3 != null) {
                list.add(a3);
            }
        }
        return new com.applovin.exoplayer2.g.a(list);
    }
    
    public interface a
    {
        boolean evaluate(final int p0, final int p1, final int p2, final int p3, final int p4);
    }
    
    private static final class b
    {
        private final int a;
        private final boolean b;
        private final int c;
        
        public b(final int a, final boolean b, final int c) {
            this.a = a;
            this.b = b;
            this.c = c;
        }
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.applovin.exoplayer2.g.e;

import java.util.Arrays;
import androidx.annotation.Nullable;
import com.applovin.exoplayer2.l.ai;
import android.os.Parcel;
import android.os.Parcelable$Creator;

public final class b extends h
{
    public static final Parcelable$Creator<b> CREATOR;
    public final byte[] a;
    
    static {
        CREATOR = (Parcelable$Creator)new Parcelable$Creator<b>() {
            public b a(final Parcel parcel) {
                return new b(parcel);
            }
            
            public b[] a(final int n) {
                return new b[n];
            }
        };
    }
    
    b(final Parcel parcel) {
        super((String)ai.a((Object)parcel.readString()));
        this.a = (byte[])ai.a((Object)parcel.createByteArray());
    }
    
    public b(final String s, final byte[] a) {
        super(s);
        this.a = a;
    }
    
    @Override
    public boolean equals(@Nullable final Object o) {
        boolean b = true;
        if (this == o) {
            return true;
        }
        if (o != null && b.class == o.getClass()) {
            final b b2 = (b)o;
            if (!super.f.equals(b2.f) || !Arrays.equals(this.a, b2.a)) {
                b = false;
            }
            return b;
        }
        return false;
    }
    
    @Override
    public int hashCode() {
        return (527 + super.f.hashCode()) * 31 + Arrays.hashCode(this.a);
    }
    
    public void writeToParcel(final Parcel parcel, final int n) {
        parcel.writeString(super.f);
        parcel.writeByteArray(this.a);
    }
}

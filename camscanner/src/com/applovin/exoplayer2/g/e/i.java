// 
// Decompiled by Procyon v0.6.0
// 

package com.applovin.exoplayer2.g.e;

import androidx.annotation.Nullable;
import com.applovin.exoplayer2.l.ai;
import android.os.Parcel;
import android.os.Parcelable$Creator;

public final class i extends h
{
    public static final Parcelable$Creator<i> CREATOR;
    public final String a;
    public final String b;
    public final String c;
    
    static {
        CREATOR = (Parcelable$Creator)new Parcelable$Creator<i>() {
            public i a(final Parcel parcel) {
                return new i(parcel);
            }
            
            public i[] a(final int n) {
                return new i[n];
            }
        };
    }
    
    i(final Parcel parcel) {
        super("----");
        this.a = (String)ai.a((Object)parcel.readString());
        this.b = (String)ai.a((Object)parcel.readString());
        this.c = (String)ai.a((Object)parcel.readString());
    }
    
    public i(final String a, final String b, final String c) {
        super("----");
        this.a = a;
        this.b = b;
        this.c = c;
    }
    
    @Override
    public boolean equals(@Nullable final Object o) {
        boolean b = true;
        if (this == o) {
            return true;
        }
        if (o != null && i.class == o.getClass()) {
            final i i = (i)o;
            if (!ai.a((Object)this.b, (Object)i.b) || !ai.a((Object)this.a, (Object)i.a) || !ai.a((Object)this.c, (Object)i.c)) {
                b = false;
            }
            return b;
        }
        return false;
    }
    
    @Override
    public int hashCode() {
        final String a = this.a;
        int hashCode = 0;
        int hashCode2;
        if (a != null) {
            hashCode2 = a.hashCode();
        }
        else {
            hashCode2 = 0;
        }
        final String b = this.b;
        int hashCode3;
        if (b != null) {
            hashCode3 = b.hashCode();
        }
        else {
            hashCode3 = 0;
        }
        final String c = this.c;
        if (c != null) {
            hashCode = c.hashCode();
        }
        return ((527 + hashCode2) * 31 + hashCode3) * 31 + hashCode;
    }
    
    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append(super.f);
        sb.append(": domain=");
        sb.append(this.a);
        sb.append(", description=");
        sb.append(this.b);
        return sb.toString();
    }
    
    public void writeToParcel(final Parcel parcel, final int n) {
        parcel.writeString(super.f);
        parcel.writeString(this.a);
        parcel.writeString(this.c);
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.applovin.exoplayer2.g.e;

import android.os.Parcelable;
import java.util.Arrays;
import androidx.annotation.Nullable;
import com.applovin.exoplayer2.l.ai;
import android.os.Parcel;
import android.os.Parcelable$Creator;

public final class d extends h
{
    public static final Parcelable$Creator<d> CREATOR;
    public final String a;
    public final boolean b;
    public final boolean c;
    public final String[] d;
    private final h[] e;
    
    static {
        CREATOR = (Parcelable$Creator)new Parcelable$Creator<d>() {
            public d a(final Parcel parcel) {
                return new d(parcel);
            }
            
            public d[] a(final int n) {
                return new d[n];
            }
        };
    }
    
    d(final Parcel parcel) {
        super("CTOC");
        this.a = (String)ai.a((Object)parcel.readString());
        final byte byte1 = parcel.readByte();
        final boolean b = true;
        int i = 0;
        this.b = (byte1 != 0);
        this.c = (parcel.readByte() != 0 && b);
        this.d = (String[])ai.a((Object)parcel.createStringArray());
        final int int1 = parcel.readInt();
        this.e = new h[int1];
        while (i < int1) {
            this.e[i] = (h)parcel.readParcelable(h.class.getClassLoader());
            ++i;
        }
    }
    
    public d(final String a, final boolean b, final boolean c, final String[] d, final h[] e) {
        super("CTOC");
        this.a = a;
        this.b = b;
        this.c = c;
        this.d = d;
        this.e = e;
    }
    
    @Override
    public boolean equals(@Nullable final Object o) {
        boolean b = true;
        if (this == o) {
            return true;
        }
        if (o != null && d.class == o.getClass()) {
            final d d = (d)o;
            if (this.b != d.b || this.c != d.c || !ai.a((Object)this.a, (Object)d.a) || !Arrays.equals(this.d, d.d) || !Arrays.equals(this.e, d.e)) {
                b = false;
            }
            return b;
        }
        return false;
    }
    
    @Override
    public int hashCode() {
        final int b = this.b ? 1 : 0;
        final int c = this.c ? 1 : 0;
        final String a = this.a;
        int hashCode;
        if (a != null) {
            hashCode = a.hashCode();
        }
        else {
            hashCode = 0;
        }
        return ((527 + b) * 31 + c) * 31 + hashCode;
    }
    
    public void writeToParcel(final Parcel parcel, int i) {
        parcel.writeString(this.a);
        parcel.writeByte((byte)(byte)(this.b ? 1 : 0));
        parcel.writeByte((byte)(byte)(this.c ? 1 : 0));
        parcel.writeStringArray(this.d);
        parcel.writeInt(this.e.length);
        final h[] e = this.e;
        int length;
        for (length = e.length, i = 0; i < length; ++i) {
            parcel.writeParcelable((Parcelable)e[i], 0);
        }
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.applovin.exoplayer2.g.e;

import com.applovin.exoplayer2.l.ai;
import android.os.Parcel;
import androidx.annotation.Nullable;
import android.os.Parcelable$Creator;

public final class m extends h
{
    public static final Parcelable$Creator<m> CREATOR;
    @Nullable
    public final String a;
    public final String b;
    
    static {
        CREATOR = (Parcelable$Creator)new Parcelable$Creator<m>() {
            public m a(final Parcel parcel) {
                return new m(parcel);
            }
            
            public m[] a(final int n) {
                return new m[n];
            }
        };
    }
    
    m(final Parcel parcel) {
        super((String)ai.a((Object)parcel.readString()));
        this.a = parcel.readString();
        this.b = (String)ai.a((Object)parcel.readString());
    }
    
    public m(final String s, @Nullable final String a, final String b) {
        super(s);
        this.a = a;
        this.b = b;
    }
    
    @Override
    public boolean equals(@Nullable final Object o) {
        boolean b = true;
        if (this == o) {
            return true;
        }
        if (o != null && m.class == o.getClass()) {
            final m m = (m)o;
            if (!super.f.equals(m.f) || !ai.a((Object)this.a, (Object)m.a) || !ai.a((Object)this.b, (Object)m.b)) {
                b = false;
            }
            return b;
        }
        return false;
    }
    
    @Override
    public int hashCode() {
        final int hashCode = super.f.hashCode();
        final String a = this.a;
        int hashCode2 = 0;
        int hashCode3;
        if (a != null) {
            hashCode3 = a.hashCode();
        }
        else {
            hashCode3 = 0;
        }
        final String b = this.b;
        if (b != null) {
            hashCode2 = b.hashCode();
        }
        return ((527 + hashCode) * 31 + hashCode3) * 31 + hashCode2;
    }
    
    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append(super.f);
        sb.append(": url=");
        sb.append(this.b);
        return sb.toString();
    }
    
    public void writeToParcel(final Parcel parcel, final int n) {
        parcel.writeString(super.f);
        parcel.writeString(this.a);
        parcel.writeString(this.b);
    }
}

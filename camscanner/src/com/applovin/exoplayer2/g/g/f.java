// 
// Decompiled by Procyon v0.6.0
// 

package com.applovin.exoplayer2.g.g;

import com.applovin.exoplayer2.l.y;
import java.util.Collections;
import java.util.ArrayList;
import android.os.Parcel;
import java.util.List;
import android.os.Parcelable$Creator;

public final class f extends com.applovin.exoplayer2.g.g.b
{
    public static final Parcelable$Creator<f> CREATOR;
    public final List<b> a;
    
    static {
        CREATOR = (Parcelable$Creator)new Parcelable$Creator<f>() {
            public f a(final Parcel parcel) {
                return new f(parcel, null);
            }
            
            public f[] a(final int n) {
                return new f[n];
            }
        };
    }
    
    private f(final Parcel parcel) {
        final int int1 = parcel.readInt();
        final ArrayList list = new ArrayList<b>(int1);
        for (int i = 0; i < int1; ++i) {
            list.add(c(parcel));
        }
        this.a = Collections.unmodifiableList((List<? extends b>)list);
    }
    
    private f(final List<b> list) {
        this.a = Collections.unmodifiableList((List<? extends b>)list);
    }
    
    static f a(final y y) {
        final int h = y.h();
        final ArrayList list = new ArrayList<b>(h);
        for (int i = 0; i < h; ++i) {
            list.add(b(y));
        }
        return new f((List<b>)list);
    }
    
    public void writeToParcel(final Parcel parcel, int i) {
        final int size = this.a.size();
        parcel.writeInt(size);
        for (i = 0; i < size; ++i) {
            this.a.get(i).b(parcel);
        }
    }
    
    public static final class a
    {
        public final int a;
        public final long b;
        
        private a(final int a, final long b) {
            this.a = a;
            this.b = b;
        }
        
        private static a b(final Parcel parcel) {
            return new a(parcel.readInt(), parcel.readLong());
        }
        
        private void c(final Parcel parcel) {
            parcel.writeInt(this.a);
            parcel.writeLong(this.b);
        }
    }
    
    public static final class b
    {
        public final long a;
        public final boolean b;
        public final boolean c;
        public final boolean d;
        public final long e;
        public final List<a> f;
        public final boolean g;
        public final long h;
        public final int i;
        public final int j;
        public final int k;
        
        private b(final long a, final boolean b, final boolean c, final boolean d, final List<a> list, final long e, final boolean g, final long h, final int i, final int j, final int k) {
            this.a = a;
            this.b = b;
            this.c = c;
            this.d = d;
            this.f = Collections.unmodifiableList((List<? extends a>)list);
            this.e = e;
            this.g = g;
            this.h = h;
            this.i = i;
            this.j = j;
            this.k = k;
        }
        
        private b(final Parcel parcel) {
            this.a = parcel.readLong();
            final byte byte1 = parcel.readByte();
            final boolean b = false;
            this.b = (byte1 == 1);
            this.c = (parcel.readByte() == 1);
            this.d = (parcel.readByte() == 1);
            final int int1 = parcel.readInt();
            final ArrayList list = new ArrayList<a>(int1);
            for (int i = 0; i < int1; ++i) {
                list.add(b(parcel));
            }
            this.f = Collections.unmodifiableList((List<? extends a>)list);
            this.e = parcel.readLong();
            boolean g = b;
            if (parcel.readByte() == 1) {
                g = true;
            }
            this.g = g;
            this.h = parcel.readLong();
            this.i = parcel.readInt();
            this.j = parcel.readInt();
            this.k = parcel.readInt();
        }
        
        private static b b(final y y) {
            final long o = y.o();
            final boolean b = (y.h() & 0x80) != 0x0;
            ArrayList<a> list = new ArrayList<a>();
            boolean b2;
            boolean b3;
            long o2;
            boolean b5;
            long n2;
            int j;
            int h3;
            int h4;
            if (!b) {
                final int h = y.h();
                b2 = ((h & 0x80) != 0x0);
                b3 = ((h & 0x40) != 0x0);
                final boolean b4 = (h & 0x20) != 0x0;
                if (b3) {
                    o2 = y.o();
                }
                else {
                    o2 = -9223372036854775807L;
                }
                if (!b3) {
                    final int h2 = y.h();
                    list = new ArrayList<a>(h2);
                    for (int i = 0; i < h2; ++i) {
                        list.add(new a(y.h(), y.o()));
                    }
                }
                if (b4) {
                    final long n = y.h();
                    b5 = ((0x80L & n) != 0x0L);
                    n2 = ((n & 0x1L) << 32 | y.o()) * 1000L / 90L;
                }
                else {
                    b5 = false;
                    n2 = -9223372036854775807L;
                }
                j = y.i();
                h3 = y.h();
                h4 = y.h();
            }
            else {
                b2 = false;
                o2 = -9223372036854775807L;
                b5 = false;
                n2 = -9223372036854775807L;
                j = 0;
                h3 = 0;
                h4 = 0;
                b3 = false;
            }
            return new b(o, b, b2, b3, list, o2, b5, n2, j, h3, h4);
        }
        
        private void b(final Parcel parcel) {
            parcel.writeLong(this.a);
            parcel.writeByte((byte)(byte)(this.b ? 1 : 0));
            parcel.writeByte((byte)(byte)(this.c ? 1 : 0));
            parcel.writeByte((byte)(byte)(this.d ? 1 : 0));
            final int size = this.f.size();
            parcel.writeInt(size);
            for (int i = 0; i < size; ++i) {
                this.f.get(i).c(parcel);
            }
            parcel.writeLong(this.e);
            parcel.writeByte((byte)(byte)(this.g ? 1 : 0));
            parcel.writeLong(this.h);
            parcel.writeInt(this.i);
            parcel.writeInt(this.j);
            parcel.writeInt(this.k);
        }
        
        private static b c(final Parcel parcel) {
            return new b(parcel);
        }
    }
}

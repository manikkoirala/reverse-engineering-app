// 
// Decompiled by Procyon v0.6.0
// 

package com.applovin.exoplayer2.g.g;

import com.applovin.exoplayer2.g.a;
import java.nio.ByteBuffer;
import com.applovin.exoplayer2.g.d;
import com.applovin.exoplayer2.l.ag;
import com.applovin.exoplayer2.l.x;
import com.applovin.exoplayer2.l.y;
import com.applovin.exoplayer2.g.g;

public final class c extends g
{
    private final y a;
    private final x b;
    private ag c;
    
    public c() {
        this.a = new y();
        this.b = new x();
    }
    
    @Override
    protected a a(final d d, final ByteBuffer byteBuffer) {
        final ag c = this.c;
        if (c == null || d.f != c.c()) {
            (this.c = new ag(d.d)).c(d.d - d.f);
        }
        final byte[] array = byteBuffer.array();
        final int limit = byteBuffer.limit();
        this.a.a(array, limit);
        this.b.a(array, limit);
        this.b.b(39);
        final long n = (long)this.b.c(1) << 32 | (long)this.b.c(32);
        this.b.b(20);
        final int c2 = this.b.c(12);
        final int c3 = this.b.c(8);
        this.a.e(14);
        a.a a;
        if (c3 != 0) {
            if (c3 != 255) {
                if (c3 != 4) {
                    if (c3 != 5) {
                        if (c3 != 6) {
                            a = null;
                        }
                        else {
                            a = com.applovin.exoplayer2.g.g.g.a(this.a, n, this.c);
                        }
                    }
                    else {
                        a = d.a(this.a, n, this.c);
                    }
                }
                else {
                    a = f.a(this.a);
                }
            }
            else {
                a = com.applovin.exoplayer2.g.g.a.a(this.a, c2, n);
            }
        }
        else {
            a = new e();
        }
        a a2;
        if (a == null) {
            a2 = new a(new a.a[0]);
        }
        else {
            a2 = new a(new a.a[] { a });
        }
        return a2;
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.applovin.exoplayer2.g.g;

import com.applovin.exoplayer2.l.ag;
import com.applovin.exoplayer2.l.y;
import java.util.ArrayList;
import java.util.Collections;
import android.os.Parcel;
import java.util.List;
import android.os.Parcelable$Creator;

public final class d extends b
{
    public static final Parcelable$Creator<d> CREATOR;
    public final long a;
    public final boolean b;
    public final boolean c;
    public final boolean d;
    public final boolean e;
    public final long f;
    public final long g;
    public final List<a> h;
    public final boolean i;
    public final long j;
    public final int k;
    public final int l;
    public final int m;
    
    static {
        CREATOR = (Parcelable$Creator)new Parcelable$Creator<d>() {
            public d a(final Parcel parcel) {
                return new d(parcel, null);
            }
            
            public d[] a(final int n) {
                return new d[n];
            }
        };
    }
    
    private d(final long a, final boolean b, final boolean c, final boolean d, final boolean e, final long f, final long g, final List<a> list, final boolean i, final long j, final int k, final int l, final int m) {
        this.a = a;
        this.b = b;
        this.c = c;
        this.d = d;
        this.e = e;
        this.f = f;
        this.g = g;
        this.h = Collections.unmodifiableList((List<? extends a>)list);
        this.i = i;
        this.j = j;
        this.k = k;
        this.l = l;
        this.m = m;
    }
    
    private d(final Parcel parcel) {
        this.a = parcel.readLong();
        final byte byte1 = parcel.readByte();
        final boolean b = false;
        this.b = (byte1 == 1);
        this.c = (parcel.readByte() == 1);
        this.d = (parcel.readByte() == 1);
        this.e = (parcel.readByte() == 1);
        this.f = parcel.readLong();
        this.g = parcel.readLong();
        final int int1 = parcel.readInt();
        final ArrayList list = new ArrayList(int1);
        for (int i = 0; i < int1; ++i) {
            list.add((Object)com.applovin.exoplayer2.g.g.d.a.b(parcel));
        }
        this.h = Collections.unmodifiableList((List<? extends a>)list);
        boolean j = b;
        if (parcel.readByte() == 1) {
            j = true;
        }
        this.i = j;
        this.j = parcel.readLong();
        this.k = parcel.readInt();
        this.l = parcel.readInt();
        this.m = parcel.readInt();
    }
    
    static d a(final y y, long n, final ag ag) {
        final long o = y.o();
        final boolean b = (y.h() & 0x80) != 0x0;
        List<a> emptyList = Collections.emptyList();
        boolean b2;
        boolean b3;
        boolean b5;
        long a;
        boolean b6;
        int j;
        int h4;
        int h5;
        if (!b) {
            final int h = y.h();
            b2 = ((h & 0x80) != 0x0);
            b3 = ((h & 0x40) != 0x0);
            final boolean b4 = (h & 0x20) != 0x0;
            b5 = ((h & 0x10) != 0x0);
            if (b3 && !b5) {
                a = g.a(y, n);
            }
            else {
                a = -9223372036854775807L;
            }
            if (!b3) {
                final int h2 = y.h();
                emptyList = new ArrayList<a>(h2);
                for (int i = 0; i < h2; ++i) {
                    final int h3 = y.h();
                    long a2;
                    if (!b5) {
                        a2 = g.a(y, n);
                    }
                    else {
                        a2 = -9223372036854775807L;
                    }
                    emptyList.add(new a(h3, a2, ag.b(a2)));
                }
            }
            if (b4) {
                n = y.h();
                b6 = ((0x80L & n) != 0x0L);
                n = ((n & 0x1L) << 32 | y.o()) * 1000L / 90L;
            }
            else {
                b6 = false;
                n = -9223372036854775807L;
            }
            j = y.i();
            h4 = y.h();
            h5 = y.h();
        }
        else {
            b2 = false;
            b5 = false;
            a = -9223372036854775807L;
            b6 = false;
            n = -9223372036854775807L;
            j = 0;
            h4 = 0;
            h5 = 0;
            b3 = false;
        }
        return new d(o, b, b2, b3, b5, a, ag.b(a), emptyList, b6, n, j, h4, h5);
    }
    
    public void writeToParcel(final Parcel parcel, int i) {
        parcel.writeLong(this.a);
        parcel.writeByte((byte)(byte)(this.b ? 1 : 0));
        parcel.writeByte((byte)(byte)(this.c ? 1 : 0));
        parcel.writeByte((byte)(byte)(this.d ? 1 : 0));
        parcel.writeByte((byte)(byte)(this.e ? 1 : 0));
        parcel.writeLong(this.f);
        parcel.writeLong(this.g);
        final int size = this.h.size();
        parcel.writeInt(size);
        for (i = 0; i < size; ++i) {
            this.h.get(i).a(parcel);
        }
        parcel.writeByte((byte)(byte)(this.i ? 1 : 0));
        parcel.writeLong(this.j);
        parcel.writeInt(this.k);
        parcel.writeInt(this.l);
        parcel.writeInt(this.m);
    }
    
    public static final class a
    {
        public final int a;
        public final long b;
        public final long c;
        
        private a(final int a, final long b, final long c) {
            this.a = a;
            this.b = b;
            this.c = c;
        }
        
        public static a b(final Parcel parcel) {
            return new a(parcel.readInt(), parcel.readLong(), parcel.readLong());
        }
        
        public void a(final Parcel parcel) {
            parcel.writeInt(this.a);
            parcel.writeLong(this.b);
            parcel.writeLong(this.c);
        }
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.applovin.exoplayer2.g.g;

import com.applovin.exoplayer2.l.ag;
import com.applovin.exoplayer2.l.y;
import android.os.Parcel;
import android.os.Parcelable$Creator;

public final class g extends b
{
    public static final Parcelable$Creator<g> CREATOR;
    public final long a;
    public final long b;
    
    static {
        CREATOR = (Parcelable$Creator)new Parcelable$Creator<g>() {
            public g a(final Parcel parcel) {
                return new g(parcel.readLong(), parcel.readLong(), null);
            }
            
            public g[] a(final int n) {
                return new g[n];
            }
        };
    }
    
    private g(final long a, final long b) {
        this.a = a;
        this.b = b;
    }
    
    static long a(final y y, long n) {
        final long n2 = y.h();
        if ((0x80L & n2) != 0x0L) {
            n = (0x1FFFFFFFFL & ((n2 & 0x1L) << 32 | y.o()) + n);
        }
        else {
            n = -9223372036854775807L;
        }
        return n;
    }
    
    static g a(final y y, long a, final ag ag) {
        a = a(y, a);
        return new g(a, ag.b(a));
    }
    
    public void writeToParcel(final Parcel parcel, final int n) {
        parcel.writeLong(this.a);
        parcel.writeLong(this.b);
    }
}

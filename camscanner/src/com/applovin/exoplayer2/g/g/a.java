// 
// Decompiled by Procyon v0.6.0
// 

package com.applovin.exoplayer2.g.g;

import com.applovin.exoplayer2.l.y;
import com.applovin.exoplayer2.l.ai;
import android.os.Parcel;
import android.os.Parcelable$Creator;

public final class a extends b
{
    public static final Parcelable$Creator<a> CREATOR;
    public final long a;
    public final long b;
    public final byte[] c;
    
    static {
        CREATOR = (Parcelable$Creator)new Parcelable$Creator<a>() {
            public a a(final Parcel parcel) {
                return new a(parcel, null);
            }
            
            public a[] a(final int n) {
                return new a[n];
            }
        };
    }
    
    private a(final long b, final byte[] c, final long a) {
        this.a = a;
        this.b = b;
        this.c = c;
    }
    
    private a(final Parcel parcel) {
        this.a = parcel.readLong();
        this.b = parcel.readLong();
        this.c = (byte[])ai.a((Object)parcel.createByteArray());
    }
    
    static a a(final y y, int n, final long n2) {
        final long o = y.o();
        n -= 4;
        final byte[] array = new byte[n];
        y.a(array, 0, n);
        return new a(o, array, n2);
    }
    
    public void writeToParcel(final Parcel parcel, final int n) {
        parcel.writeLong(this.a);
        parcel.writeLong(this.b);
        parcel.writeByteArray(this.c);
    }
}

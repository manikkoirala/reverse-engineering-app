// 
// Decompiled by Procyon v0.6.0
// 

package com.applovin.exoplayer2.g;

import com.applovin.exoplayer2.ac;
import com.applovin.exoplayer2.v;
import java.util.Arrays;
import com.applovin.exoplayer2.l.ai;
import androidx.annotation.Nullable;
import java.util.List;
import android.os.Parcel;
import android.os.Parcelable$Creator;
import android.os.Parcelable;

public final class a implements Parcelable
{
    public static final Parcelable$Creator<a> CREATOR;
    private final a[] a;
    
    static {
        CREATOR = (Parcelable$Creator)new Parcelable$Creator<a>() {
            public a a(final Parcel parcel) {
                return new a(parcel);
            }
            
            public a[] a(final int n) {
                return new a[n];
            }
        };
    }
    
    a(final Parcel parcel) {
        this.a = new a[parcel.readInt()];
        int n = 0;
        while (true) {
            final a[] a = this.a;
            if (n >= a.length) {
                break;
            }
            a[n] = (a)parcel.readParcelable(a.class.getClassLoader());
            ++n;
        }
    }
    
    public a(final List<? extends a> list) {
        this.a = list.toArray(new a[0]);
    }
    
    public a(final a... a) {
        this.a = a;
    }
    
    public int a() {
        return this.a.length;
    }
    
    public a a(final int n) {
        return this.a[n];
    }
    
    public a a(@Nullable final a a) {
        if (a == null) {
            return this;
        }
        return this.a(a.a);
    }
    
    public a a(final a... array) {
        if (array.length == 0) {
            return this;
        }
        return new a((a[])ai.a((Object[])this.a, (Object[])array));
    }
    
    public int describeContents() {
        return 0;
    }
    
    @Override
    public boolean equals(@Nullable final Object o) {
        return this == o || (o != null && a.class == o.getClass() && Arrays.equals(this.a, ((a)o).a));
    }
    
    @Override
    public int hashCode() {
        return Arrays.hashCode(this.a);
    }
    
    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append("entries=");
        sb.append(Arrays.toString(this.a));
        return sb.toString();
    }
    
    public void writeToParcel(final Parcel parcel, int i) {
        parcel.writeInt(this.a.length);
        final a[] a = this.a;
        int length;
        for (length = a.length, i = 0; i < length; ++i) {
            parcel.writeParcelable((Parcelable)a[i], 0);
        }
    }
    
    public interface a extends Parcelable
    {
        @Nullable
        v a();
        
        void a(final ac.a p0);
        
        @Nullable
        byte[] b();
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.applovin.exoplayer2;

import android.content.Intent;
import com.applovin.exoplayer2.l.ai;
import com.applovin.exoplayer2.l.q;
import android.content.BroadcastReceiver;
import android.content.IntentFilter;
import com.applovin.exoplayer2.l.a;
import androidx.annotation.Nullable;
import android.media.AudioManager;
import android.os.Handler;
import android.content.Context;

final class ay
{
    private final Context a;
    private final Handler b;
    private final a c;
    private final AudioManager d;
    @Nullable
    private b e;
    private int f;
    private int g;
    private boolean h;
    
    public ay(Context applicationContext, final Handler b, final a c) {
        applicationContext = applicationContext.getApplicationContext();
        this.a = applicationContext;
        this.b = b;
        this.c = c;
        final AudioManager d = (AudioManager)com.applovin.exoplayer2.l.a.a((Object)applicationContext.getSystemService("audio"));
        this.d = d;
        this.f = 3;
        this.g = a(d, 3);
        this.h = b(d, this.f);
        final b e = new b();
        final IntentFilter intentFilter = new IntentFilter("android.media.VOLUME_CHANGED_ACTION");
        try {
            applicationContext.registerReceiver((BroadcastReceiver)e, intentFilter);
            this.e = e;
        }
        catch (final RuntimeException ex) {
            q.b("StreamVolumeManager", "Error registering stream volume receiver", (Throwable)ex);
        }
    }
    
    private static int a(final AudioManager audioManager, final int i) {
        try {
            return audioManager.getStreamVolume(i);
        }
        catch (final RuntimeException ex) {
            final StringBuilder sb = new StringBuilder();
            sb.append("Could not retrieve stream volume for stream type ");
            sb.append(i);
            q.b("StreamVolumeManager", sb.toString(), (Throwable)ex);
            return audioManager.getStreamMaxVolume(i);
        }
    }
    
    private static boolean b(final AudioManager audioManager, final int n) {
        if (ai.a >= 23) {
            return \u30070\u3007O0088o.\u3007080(audioManager, n);
        }
        return a(audioManager, n) == 0;
    }
    
    private void d() {
        final int a = a(this.d, this.f);
        final boolean b = b(this.d, this.f);
        if (this.g != a || this.h != b) {
            this.g = a;
            this.h = b;
            this.c.a(a, b);
        }
    }
    
    public int a() {
        int \u3007080;
        if (ai.a >= 28) {
            \u3007080 = OoO8.\u3007080(this.d, this.f);
        }
        else {
            \u3007080 = 0;
        }
        return \u3007080;
    }
    
    public void a(final int f) {
        if (this.f == f) {
            return;
        }
        this.f = f;
        this.d();
        this.c.f(f);
    }
    
    public int b() {
        return this.d.getStreamMaxVolume(this.f);
    }
    
    public void c() {
        final b e = this.e;
        if (e != null) {
            try {
                this.a.unregisterReceiver((BroadcastReceiver)e);
            }
            catch (final RuntimeException ex) {
                q.b("StreamVolumeManager", "Error unregistering stream volume receiver", (Throwable)ex);
            }
            this.e = null;
        }
    }
    
    public interface a
    {
        void a(final int p0, final boolean p1);
        
        void f(final int p0);
    }
    
    private final class b extends BroadcastReceiver
    {
        final ay a;
        
        private b(final ay a) {
            this.a = a;
        }
        
        public void onReceive(final Context context, final Intent intent) {
            this.a.b.post((Runnable)new o800o8O(this.a));
        }
    }
}

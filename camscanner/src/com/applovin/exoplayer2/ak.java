// 
// Decompiled by Procyon v0.6.0
// 

package com.applovin.exoplayer2;

import android.os.BaseBundle;
import android.text.TextUtils;
import android.os.RemoteException;
import androidx.annotation.Nullable;
import android.os.SystemClock;
import android.os.Bundle;

public class ak extends Exception implements g
{
    public static final a<ak> k;
    public final int i;
    public final long j;
    
    static {
        k = new oO80();
    }
    
    protected ak(final Bundle bundle) {
        this(((BaseBundle)bundle).getString(a(2)), a(bundle), ((BaseBundle)bundle).getInt(a(0), 1000), ((BaseBundle)bundle).getLong(a(1), SystemClock.elapsedRealtime()));
    }
    
    protected ak(@Nullable final String message, @Nullable final Throwable cause, final int i, final long j) {
        super(message, cause);
        this.i = i;
        this.j = j;
    }
    
    private static RemoteException a(@Nullable final String s) {
        return new RemoteException(s);
    }
    
    protected static String a(final int i) {
        return Integer.toString(i, 36);
    }
    
    @Nullable
    private static Throwable a(final Bundle bundle) {
        final String string = ((BaseBundle)bundle).getString(a(3));
        final String string2 = ((BaseBundle)bundle).getString(a(4));
        final boolean empty = TextUtils.isEmpty((CharSequence)string);
        Object a = null;
        Throwable a2 = null;
        if (empty) {
            return (Throwable)a;
        }
        while (true) {
            try {
                final Class<?> forName = Class.forName(string, true, ak.class.getClassLoader());
                if (Throwable.class.isAssignableFrom(forName)) {
                    a2 = a(forName, string2);
                }
                if ((a = a2) == null) {
                    a = a(string2);
                }
                return (Throwable)a;
            }
            finally {
                continue;
            }
            break;
        }
    }
    
    private static Throwable a(final Class<?> clazz, @Nullable final String s) throws Exception {
        return (Throwable)clazz.getConstructor(String.class).newInstance(s);
    }
}

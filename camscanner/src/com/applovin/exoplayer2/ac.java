// 
// Decompiled by Procyon v0.6.0
// 

package com.applovin.exoplayer2;

import android.os.BaseBundle;
import androidx.annotation.IntRange;
import java.util.List;
import com.applovin.exoplayer2.g.a;
import com.applovin.exoplayer2.common.base.Objects;
import java.util.Arrays;
import com.applovin.exoplayer2.l.ai;
import android.net.Uri;
import android.os.Bundle;
import androidx.annotation.Nullable;

public final class ac implements g
{
    public static final g.a<ac> H;
    public static final ac a;
    @Nullable
    public final CharSequence A;
    @Nullable
    public final CharSequence B;
    @Nullable
    public final Integer C;
    @Nullable
    public final Integer D;
    @Nullable
    public final CharSequence E;
    @Nullable
    public final CharSequence F;
    @Nullable
    public final Bundle G;
    @Nullable
    public final CharSequence b;
    @Nullable
    public final CharSequence c;
    @Nullable
    public final CharSequence d;
    @Nullable
    public final CharSequence e;
    @Nullable
    public final CharSequence f;
    @Nullable
    public final CharSequence g;
    @Nullable
    public final CharSequence h;
    @Nullable
    public final Uri i;
    @Nullable
    public final aq j;
    @Nullable
    public final aq k;
    @Nullable
    public final byte[] l;
    @Nullable
    public final Integer m;
    @Nullable
    public final Uri n;
    @Nullable
    public final Integer o;
    @Nullable
    public final Integer p;
    @Nullable
    public final Integer q;
    @Nullable
    public final Boolean r;
    @Deprecated
    @Nullable
    public final Integer s;
    @Nullable
    public final Integer t;
    @Nullable
    public final Integer u;
    @Nullable
    public final Integer v;
    @Nullable
    public final Integer w;
    @Nullable
    public final Integer x;
    @Nullable
    public final Integer y;
    @Nullable
    public final CharSequence z;
    
    static {
        a = new a().a();
        H = new O8();
    }
    
    private ac(final a a) {
        this.b = a.a;
        this.c = a.b;
        this.d = a.c;
        this.e = a.d;
        this.f = a.e;
        this.g = a.f;
        this.h = a.g;
        this.i = a.h;
        this.j = a.i;
        this.k = a.j;
        this.l = a.k;
        this.m = a.l;
        this.n = a.m;
        this.o = a.n;
        this.p = a.o;
        this.q = a.p;
        this.r = a.q;
        this.s = a.r;
        this.t = a.r;
        this.u = a.s;
        this.v = a.t;
        this.w = a.u;
        this.x = a.v;
        this.y = a.w;
        this.z = a.x;
        this.A = a.y;
        this.B = a.z;
        this.C = a.A;
        this.D = a.B;
        this.E = a.C;
        this.F = a.D;
        this.G = a.E;
    }
    
    private static ac a(final Bundle bundle) {
        final a a = new a();
        final a a2 = a.a(bundle.getCharSequence(a(0))).b(bundle.getCharSequence(a(1))).c(bundle.getCharSequence(a(2))).d(bundle.getCharSequence(a(3))).e(bundle.getCharSequence(a(4))).f(bundle.getCharSequence(a(5))).g(bundle.getCharSequence(a(6))).a((Uri)bundle.getParcelable(a(7)));
        final byte[] byteArray = bundle.getByteArray(a(10));
        Integer value;
        if (((BaseBundle)bundle).containsKey(a(29))) {
            value = ((BaseBundle)bundle).getInt(a(29));
        }
        else {
            value = null;
        }
        a2.a(byteArray, value).b((Uri)bundle.getParcelable(a(11))).h(bundle.getCharSequence(a(22))).i(bundle.getCharSequence(a(23))).j(bundle.getCharSequence(a(24))).k(bundle.getCharSequence(a(27))).l(bundle.getCharSequence(a(28))).a(bundle.getBundle(a(1000)));
        if (((BaseBundle)bundle).containsKey(a(8))) {
            final Bundle bundle2 = bundle.getBundle(a(8));
            if (bundle2 != null) {
                a.a(aq.b.fromBundle(bundle2));
            }
        }
        if (((BaseBundle)bundle).containsKey(a(9))) {
            final Bundle bundle3 = bundle.getBundle(a(9));
            if (bundle3 != null) {
                a.b(aq.b.fromBundle(bundle3));
            }
        }
        if (((BaseBundle)bundle).containsKey(a(12))) {
            a.a(((BaseBundle)bundle).getInt(a(12)));
        }
        if (((BaseBundle)bundle).containsKey(a(13))) {
            a.b(((BaseBundle)bundle).getInt(a(13)));
        }
        if (((BaseBundle)bundle).containsKey(a(14))) {
            a.c(((BaseBundle)bundle).getInt(a(14)));
        }
        if (((BaseBundle)bundle).containsKey(a(15))) {
            a.a(bundle.getBoolean(a(15)));
        }
        if (((BaseBundle)bundle).containsKey(a(16))) {
            a.d(((BaseBundle)bundle).getInt(a(16)));
        }
        if (((BaseBundle)bundle).containsKey(a(17))) {
            a.e(((BaseBundle)bundle).getInt(a(17)));
        }
        if (((BaseBundle)bundle).containsKey(a(18))) {
            a.f(((BaseBundle)bundle).getInt(a(18)));
        }
        if (((BaseBundle)bundle).containsKey(a(19))) {
            a.g(((BaseBundle)bundle).getInt(a(19)));
        }
        if (((BaseBundle)bundle).containsKey(a(20))) {
            a.h(((BaseBundle)bundle).getInt(a(20)));
        }
        if (((BaseBundle)bundle).containsKey(a(21))) {
            a.i(((BaseBundle)bundle).getInt(a(21)));
        }
        if (((BaseBundle)bundle).containsKey(a(25))) {
            a.j(((BaseBundle)bundle).getInt(a(25)));
        }
        if (((BaseBundle)bundle).containsKey(a(26))) {
            a.k(((BaseBundle)bundle).getInt(a(26)));
        }
        return a.a();
    }
    
    private static String a(final int i) {
        return Integer.toString(i, 36);
    }
    
    public a a() {
        return new a(this);
    }
    
    @Override
    public boolean equals(@Nullable final Object o) {
        boolean b = true;
        if (this == o) {
            return true;
        }
        if (o != null && ac.class == o.getClass()) {
            final ac ac = (ac)o;
            if (!ai.a((Object)this.b, (Object)ac.b) || !ai.a((Object)this.c, (Object)ac.c) || !ai.a((Object)this.d, (Object)ac.d) || !ai.a((Object)this.e, (Object)ac.e) || !ai.a((Object)this.f, (Object)ac.f) || !ai.a((Object)this.g, (Object)ac.g) || !ai.a((Object)this.h, (Object)ac.h) || !ai.a((Object)this.i, (Object)ac.i) || !ai.a((Object)this.j, (Object)ac.j) || !ai.a((Object)this.k, (Object)ac.k) || !Arrays.equals(this.l, ac.l) || !ai.a((Object)this.m, (Object)ac.m) || !ai.a((Object)this.n, (Object)ac.n) || !ai.a((Object)this.o, (Object)ac.o) || !ai.a((Object)this.p, (Object)ac.p) || !ai.a((Object)this.q, (Object)ac.q) || !ai.a((Object)this.r, (Object)ac.r) || !ai.a((Object)this.t, (Object)ac.t) || !ai.a((Object)this.u, (Object)ac.u) || !ai.a((Object)this.v, (Object)ac.v) || !ai.a((Object)this.w, (Object)ac.w) || !ai.a((Object)this.x, (Object)ac.x) || !ai.a((Object)this.y, (Object)ac.y) || !ai.a((Object)this.z, (Object)ac.z) || !ai.a((Object)this.A, (Object)ac.A) || !ai.a((Object)this.B, (Object)ac.B) || !ai.a((Object)this.C, (Object)ac.C) || !ai.a((Object)this.D, (Object)ac.D) || !ai.a((Object)this.E, (Object)ac.E) || !ai.a((Object)this.F, (Object)ac.F)) {
                b = false;
            }
            return b;
        }
        return false;
    }
    
    @Override
    public int hashCode() {
        return Objects.hashCode(this.b, this.c, this.d, this.e, this.f, this.g, this.h, this.i, this.j, this.k, Arrays.hashCode(this.l), this.m, this.n, this.o, this.p, this.q, this.r, this.t, this.u, this.v, this.w, this.x, this.y, this.z, this.A, this.B, this.C, this.D, this.E, this.F);
    }
    
    public static final class a
    {
        @Nullable
        private Integer A;
        @Nullable
        private Integer B;
        @Nullable
        private CharSequence C;
        @Nullable
        private CharSequence D;
        @Nullable
        private Bundle E;
        @Nullable
        private CharSequence a;
        @Nullable
        private CharSequence b;
        @Nullable
        private CharSequence c;
        @Nullable
        private CharSequence d;
        @Nullable
        private CharSequence e;
        @Nullable
        private CharSequence f;
        @Nullable
        private CharSequence g;
        @Nullable
        private Uri h;
        @Nullable
        private aq i;
        @Nullable
        private aq j;
        @Nullable
        private byte[] k;
        @Nullable
        private Integer l;
        @Nullable
        private Uri m;
        @Nullable
        private Integer n;
        @Nullable
        private Integer o;
        @Nullable
        private Integer p;
        @Nullable
        private Boolean q;
        @Nullable
        private Integer r;
        @Nullable
        private Integer s;
        @Nullable
        private Integer t;
        @Nullable
        private Integer u;
        @Nullable
        private Integer v;
        @Nullable
        private Integer w;
        @Nullable
        private CharSequence x;
        @Nullable
        private CharSequence y;
        @Nullable
        private CharSequence z;
        
        public a() {
        }
        
        private a(final ac ac) {
            this.a = ac.b;
            this.b = ac.c;
            this.c = ac.d;
            this.d = ac.e;
            this.e = ac.f;
            this.f = ac.g;
            this.g = ac.h;
            this.h = ac.i;
            this.i = ac.j;
            this.j = ac.k;
            this.k = ac.l;
            this.l = ac.m;
            this.m = ac.n;
            this.n = ac.o;
            this.o = ac.p;
            this.p = ac.q;
            this.q = ac.r;
            this.r = ac.t;
            this.s = ac.u;
            this.t = ac.v;
            this.u = ac.w;
            this.v = ac.x;
            this.w = ac.y;
            this.x = ac.z;
            this.y = ac.A;
            this.z = ac.B;
            this.A = ac.C;
            this.B = ac.D;
            this.C = ac.E;
            this.D = ac.F;
            this.E = ac.G;
        }
        
        public a a(@Nullable final Uri h) {
            this.h = h;
            return this;
        }
        
        public a a(@Nullable final Bundle e) {
            this.E = e;
            return this;
        }
        
        public a a(@Nullable final aq i) {
            this.i = i;
            return this;
        }
        
        public a a(final com.applovin.exoplayer2.g.a a) {
            for (int i = 0; i < a.a(); ++i) {
                a.a(i).a(this);
            }
            return this;
        }
        
        public a a(@Nullable final Boolean q) {
            this.q = q;
            return this;
        }
        
        public a a(@Nullable final CharSequence a) {
            this.a = a;
            return this;
        }
        
        public a a(@Nullable final Integer n) {
            this.n = n;
            return this;
        }
        
        public a a(final List<com.applovin.exoplayer2.g.a> list) {
            for (int i = 0; i < list.size(); ++i) {
                final com.applovin.exoplayer2.g.a a = list.get(i);
                for (int j = 0; j < a.a(); ++j) {
                    a.a(j).a(this);
                }
            }
            return this;
        }
        
        public a a(final byte[] array, final int n) {
            if (this.k == null || ai.a((Object)n, (Object)3) || !ai.a((Object)this.l, (Object)3)) {
                this.k = array.clone();
                this.l = n;
            }
            return this;
        }
        
        public a a(@Nullable byte[] k, @Nullable final Integer l) {
            if (k == null) {
                k = null;
            }
            else {
                k = k.clone();
            }
            this.k = k;
            this.l = l;
            return this;
        }
        
        public ac a() {
            return new ac(this, null);
        }
        
        public a b(@Nullable final Uri m) {
            this.m = m;
            return this;
        }
        
        public a b(@Nullable final aq j) {
            this.j = j;
            return this;
        }
        
        public a b(@Nullable final CharSequence b) {
            this.b = b;
            return this;
        }
        
        public a b(@Nullable final Integer o) {
            this.o = o;
            return this;
        }
        
        public a c(@Nullable final CharSequence c) {
            this.c = c;
            return this;
        }
        
        public a c(@Nullable final Integer p) {
            this.p = p;
            return this;
        }
        
        public a d(@Nullable final CharSequence d) {
            this.d = d;
            return this;
        }
        
        public a d(@Nullable final Integer r) {
            this.r = r;
            return this;
        }
        
        public a e(@Nullable final CharSequence e) {
            this.e = e;
            return this;
        }
        
        public a e(@IntRange(from = 1L, to = 12L) @Nullable final Integer s) {
            this.s = s;
            return this;
        }
        
        public a f(@Nullable final CharSequence f) {
            this.f = f;
            return this;
        }
        
        public a f(@IntRange(from = 1L, to = 31L) @Nullable final Integer t) {
            this.t = t;
            return this;
        }
        
        public a g(@Nullable final CharSequence g) {
            this.g = g;
            return this;
        }
        
        public a g(@Nullable final Integer u) {
            this.u = u;
            return this;
        }
        
        public a h(@Nullable final CharSequence x) {
            this.x = x;
            return this;
        }
        
        public a h(@IntRange(from = 1L, to = 12L) @Nullable final Integer v) {
            this.v = v;
            return this;
        }
        
        public a i(@Nullable final CharSequence y) {
            this.y = y;
            return this;
        }
        
        public a i(@IntRange(from = 1L, to = 31L) @Nullable final Integer w) {
            this.w = w;
            return this;
        }
        
        public a j(@Nullable final CharSequence z) {
            this.z = z;
            return this;
        }
        
        public a j(@Nullable final Integer a) {
            this.A = a;
            return this;
        }
        
        public a k(@Nullable final CharSequence c) {
            this.C = c;
            return this;
        }
        
        public a k(@Nullable final Integer b) {
            this.B = b;
            return this;
        }
        
        public a l(@Nullable final CharSequence d) {
            this.D = d;
            return this;
        }
    }
}

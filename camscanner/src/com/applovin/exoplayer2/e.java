// 
// Decompiled by Procyon v0.6.0
// 

package com.applovin.exoplayer2;

import java.io.IOException;
import com.applovin.exoplayer2.l.s;
import com.applovin.exoplayer2.l.a;
import com.applovin.exoplayer2.c.g;
import com.applovin.exoplayer2.h.x;
import androidx.annotation.Nullable;

public abstract class e implements ar, as
{
    private final int a;
    private final w b;
    @Nullable
    private at c;
    private int d;
    private int e;
    @Nullable
    private x f;
    @Nullable
    private v[] g;
    private long h;
    private long i;
    private long j;
    private boolean k;
    private boolean l;
    
    public e(final int a) {
        this.a = a;
        this.b = new w();
        this.j = Long.MIN_VALUE;
    }
    
    @Override
    public final int a() {
        return this.a;
    }
    
    protected final int a(final w w, final g g, int n) {
        final int a = ((x)com.applovin.exoplayer2.l.a.b((Object)this.f)).a(w, g, n);
        n = -4;
        if (a == -4) {
            if (g.c()) {
                this.j = Long.MIN_VALUE;
                if (!this.k) {
                    n = -3;
                }
                return n;
            }
            final long n2 = g.d + this.h;
            g.d = n2;
            this.j = Math.max(this.j, n2);
        }
        else if (a == -5) {
            final v v = (v)com.applovin.exoplayer2.l.a.b((Object)w.b);
            if (v.p != Long.MAX_VALUE) {
                w.b = v.a().a(v.p + this.h).a();
            }
        }
        return a;
    }
    
    protected final p a(final Throwable t, @Nullable final v v, final int n) {
        return this.a(t, v, false, n);
    }
    
    protected final p a(final Throwable t, @Nullable final v v, final boolean b, final int n) {
        if (v != null && !this.l) {
            this.l = true;
            try {
                final int \u3007o\u3007 = \u3007O00.\u3007o\u3007(this.a(v));
                return p.a(t, this.y(), this.w(), v, \u3007o\u3007, b, n);
            }
            catch (final p p4) {
                this.l = false;
            }
            finally {
                this.l = false;
            }
        }
        final int \u3007o\u3007 = 4;
        return p.a(t, this.y(), this.w(), v, \u3007o\u3007, b, n);
    }
    
    @Override
    public final void a(final int d) {
        this.d = d;
    }
    
    @Override
    public void a(final int n, @Nullable final Object o) throws p {
    }
    
    @Override
    public final void a(final long n) throws p {
        this.k = false;
        this.i = n;
        this.a(this.j = n, false);
    }
    
    protected void a(final long n, final boolean b) throws p {
    }
    
    @Override
    public final void a(final at c, final v[] array, final x x, final long i, final boolean b, final boolean b2, final long n, final long n2) throws p {
        com.applovin.exoplayer2.l.a.b(this.e == 0);
        this.c = c;
        this.e = 1;
        this.i = i;
        this.a(b, b2);
        this.a(array, x, n, n2);
        this.a(i, b);
    }
    
    protected void a(final boolean b, final boolean b2) throws p {
    }
    
    protected void a(final v[] array, final long n, final long n2) throws p {
    }
    
    @Override
    public final void a(final v[] g, final x f, final long j, final long h) throws p {
        com.applovin.exoplayer2.l.a.b(this.k ^ true);
        this.f = f;
        if (this.j == Long.MIN_VALUE) {
            this.j = j;
        }
        this.a(this.g = g, j, this.h = h);
    }
    
    protected int b(final long n) {
        return ((x)com.applovin.exoplayer2.l.a.b((Object)this.f)).a(n - this.h);
    }
    
    @Override
    public final as b() {
        return this;
    }
    
    @Nullable
    @Override
    public s c() {
        return null;
    }
    
    @Override
    public final int d_() {
        return this.e;
    }
    
    @Override
    public final void e() throws p {
        final int e = this.e;
        boolean b = true;
        if (e != 1) {
            b = false;
        }
        com.applovin.exoplayer2.l.a.b(b);
        this.e = 2;
        this.p();
    }
    
    @Nullable
    @Override
    public final x f() {
        return this.f;
    }
    
    @Override
    public final boolean g() {
        return this.j == Long.MIN_VALUE;
    }
    
    @Override
    public final long h() {
        return this.j;
    }
    
    @Override
    public final void i() {
        this.k = true;
    }
    
    @Override
    public final boolean j() {
        return this.k;
    }
    
    @Override
    public final void k() throws IOException {
        ((x)com.applovin.exoplayer2.l.a.b((Object)this.f)).c();
    }
    
    @Override
    public final void l() {
        com.applovin.exoplayer2.l.a.b(this.e == 2);
        this.e = 1;
        this.q();
    }
    
    @Override
    public final void m() {
        final int e = this.e;
        boolean b = true;
        if (e != 1) {
            b = false;
        }
        com.applovin.exoplayer2.l.a.b(b);
        this.b.a();
        this.e = 0;
        this.f = null;
        this.g = null;
        this.k = false;
        this.r();
    }
    
    @Override
    public final void n() {
        com.applovin.exoplayer2.l.a.b(this.e == 0);
        this.b.a();
        this.s();
    }
    
    @Override
    public int o() throws p {
        return 0;
    }
    
    protected void p() throws p {
    }
    
    protected void q() {
    }
    
    protected void r() {
    }
    
    protected void s() {
    }
    
    protected final w t() {
        this.b.a();
        return this.b;
    }
    
    protected final v[] u() {
        return (v[])com.applovin.exoplayer2.l.a.b((Object)this.g);
    }
    
    protected final at v() {
        return (at)com.applovin.exoplayer2.l.a.b((Object)this.c);
    }
    
    protected final int w() {
        return this.d;
    }
    
    protected final boolean x() {
        boolean b;
        if (this.g()) {
            b = this.k;
        }
        else {
            b = ((x)com.applovin.exoplayer2.l.a.b((Object)this.f)).b();
        }
        return b;
    }
}

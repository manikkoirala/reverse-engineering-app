// 
// Decompiled by Procyon v0.6.0
// 

package com.applovin.exoplayer2;

import com.applovin.exoplayer2.l.q;
import android.content.Context;
import android.net.wifi.WifiManager$WifiLock;
import androidx.annotation.Nullable;
import android.net.wifi.WifiManager;

final class bc
{
    @Nullable
    private final WifiManager a;
    @Nullable
    private WifiManager$WifiLock b;
    private boolean c;
    private boolean d;
    
    public bc(final Context context) {
        this.a = (WifiManager)context.getApplicationContext().getSystemService("wifi");
    }
    
    private void a() {
        final WifiManager$WifiLock b = this.b;
        if (b == null) {
            return;
        }
        if (this.c && this.d) {
            b.acquire();
        }
        else {
            b.release();
        }
    }
    
    public void a(final boolean c) {
        if (c && this.b == null) {
            final WifiManager a = this.a;
            if (a == null) {
                q.c("WifiLockManager", "WifiManager is null, therefore not creating the WifiLock.");
                return;
            }
            (this.b = a.createWifiLock(3, "ExoPlayer:WifiLockManager")).setReferenceCounted(false);
        }
        this.c = c;
        this.a();
    }
    
    public void b(final boolean d) {
        this.d = d;
        this.a();
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.applovin.exoplayer2.d;

import java.util.Collections;
import com.applovin.exoplayer2.common.a.u;
import com.applovin.exoplayer2.h;
import java.util.UUID;
import android.net.Uri;
import java.io.Closeable;
import java.io.InputStream;
import com.applovin.exoplayer2.l.ai;
import com.applovin.exoplayer2.k.l;
import com.applovin.exoplayer2.k.k;
import com.applovin.exoplayer2.k.l$a;
import com.applovin.exoplayer2.k.i;
import com.applovin.exoplayer2.k.z;
import java.util.List;
import com.applovin.exoplayer2.k.t$e;
import java.util.HashMap;
import com.applovin.exoplayer2.l.a;
import android.text.TextUtils;
import java.util.Map;
import androidx.annotation.Nullable;
import com.applovin.exoplayer2.k.t$b;

public final class p implements r
{
    private final t$b a;
    @Nullable
    private final String b;
    private final boolean c;
    private final Map<String, String> d;
    
    public p(@Nullable final String b, final boolean c, final t$b a) {
        com.applovin.exoplayer2.l.a.a(!c || !TextUtils.isEmpty((CharSequence)b));
        this.a = a;
        this.b = b;
        this.c = c;
        this.d = new HashMap<String, String>();
    }
    
    @Nullable
    private static String a(final t$e t$e, int n) {
        final int d = t$e.d;
        if ((d == 307 || d == 308) && n < 5) {
            n = 1;
        }
        else {
            n = 0;
        }
        if (n == 0) {
            return null;
        }
        final Map f = t$e.f;
        if (f != null) {
            final List list = f.get("Location");
            if (list != null && !list.isEmpty()) {
                return (String)list.get(0);
            }
        }
        return null;
    }
    
    private static byte[] a(final t$b t$b, String a, @Nullable byte[] array, final Map<String, String> map) throws s {
        final z z = new z((i)t$b.c());
        a = (String)new l$a().a(a).a((Map)map).a(2).a(array).b(1).a();
        int n = 0;
        Object a2 = a;
        try {
            while (true) {
                array = (byte[])(Object)new k((i)z, (l)a2);
                try {
                    try {
                        final byte[] a3 = ai.a((InputStream)(Object)array);
                        ai.a((Closeable)(Object)array);
                        return a3;
                    }
                    finally {}
                }
                catch (final t$e t$e) {
                    final String a4 = a(t$e, n);
                    if (a4 != null) {
                        ++n;
                        a2 = ((l)t$b).b().a(a4).a();
                        ai.a((Closeable)(Object)array);
                        continue;
                    }
                    throw t$e;
                }
                break;
            }
            ai.a((Closeable)(Object)array);
        }
        catch (final Exception ex) {
            throw new s((l)a, (Uri)a.b((Object)z.e()), z.b(), z.d(), ex);
        }
    }
    
    public void a(final String s, final String s2) {
        com.applovin.exoplayer2.l.a.b((Object)s);
        com.applovin.exoplayer2.l.a.b((Object)s2);
        synchronized (this.d) {
            this.d.put(s, s2);
        }
    }
    
    @Override
    public byte[] a(final UUID obj, final m.a a) throws s {
        final String b = a.b();
        String b2 = null;
        Label_0028: {
            if (!this.c) {
                b2 = b;
                if (!TextUtils.isEmpty((CharSequence)b)) {
                    break Label_0028;
                }
            }
            b2 = this.b;
        }
        if (!TextUtils.isEmpty((CharSequence)b2)) {
            final HashMap hashMap = new HashMap();
            final UUID e = h.e;
            String s;
            if (e.equals(obj)) {
                s = "text/xml";
            }
            else if (h.c.equals(obj)) {
                s = "application/json";
            }
            else {
                s = "application/octet-stream";
            }
            hashMap.put("Content-Type", s);
            if (e.equals(obj)) {
                hashMap.put("SOAPAction", "http://schemas.microsoft.com/DRM/2007/03/protocols/AcquireLicense");
            }
            synchronized (this.d) {
                hashMap.putAll(this.d);
                monitorexit(this.d);
                return a(this.a, b2, a.a(), hashMap);
            }
        }
        throw new s(new l$a().a(Uri.EMPTY).a(), Uri.EMPTY, (Map<String, List<String>>)u.a(), 0L, new IllegalStateException("No license URL"));
    }
    
    @Override
    public byte[] a(final UUID uuid, final m.d d) throws s {
        final StringBuilder sb = new StringBuilder();
        sb.append(d.b());
        sb.append("&signedRequest=");
        sb.append(ai.a(d.a()));
        return a(this.a, sb.toString(), null, Collections.emptyMap());
    }
}

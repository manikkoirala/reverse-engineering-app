// 
// Decompiled by Procyon v0.6.0
// 

package com.applovin.exoplayer2.d;

import androidx.annotation.Nullable;
import android.os.Looper;
import com.applovin.exoplayer2.v;

public interface h
{
    public static final h b;
    @Deprecated
    public static final h c = b = new h() {
        @Override
        public int a(final v v) {
            return (v.o != null) ? 1 : 0;
        }
        
        @Nullable
        @Override
        public f b(final Looper looper, @Nullable final g.a a, final v v) {
            if (v.o == null) {
                return null;
            }
            return new l(new f.a(new t(1), 6001));
        }
    };
    
    int a(final v p0);
    
    a a(final Looper p0, @Nullable final g.a p1, final v p2);
    
    void a();
    
    @Nullable
    f b(final Looper p0, @Nullable final g.a p1, final v p2);
    
    void b();
    
    public interface a
    {
        public static final a b = new \u30070\u3007O0088o();
        
        void release();
    }
}

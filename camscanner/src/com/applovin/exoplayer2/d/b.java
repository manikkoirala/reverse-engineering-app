// 
// Decompiled by Procyon v0.6.0
// 

package com.applovin.exoplayer2.d;

import com.applovin.exoplayer2.k.v$a;
import java.io.IOException;
import android.os.SystemClock;
import android.os.Message;
import androidx.annotation.GuardedBy;
import android.annotation.SuppressLint;
import android.os.Handler;
import java.util.Map;
import java.util.Arrays;
import android.util.Pair;
import android.media.NotProvisionedException;
import com.applovin.exoplayer2.l.ai;
import com.applovin.exoplayer2.l.q;
import java.util.Iterator;
import com.applovin.exoplayer2.l.h;
import java.util.Collections;
import com.applovin.exoplayer2.l.a;
import android.os.Looper;
import android.os.HandlerThread;
import com.applovin.exoplayer2.k.v;
import com.applovin.exoplayer2.l.i;
import java.util.HashMap;
import java.util.UUID;
import androidx.annotation.Nullable;
import java.util.List;
import androidx.annotation.RequiresApi;

@RequiresApi(18)
class b implements com.applovin.exoplayer2.d.f
{
    @Nullable
    public final List<com.applovin.exoplayer2.d.e.a> a;
    final r b;
    final UUID c;
    final e d;
    private final m e;
    private final a f;
    private final b g;
    private final int h;
    private final boolean i;
    private final boolean j;
    private final HashMap<String, String> k;
    private final i<g.a> l;
    private final v m;
    private int n;
    private int o;
    @Nullable
    private HandlerThread p;
    @Nullable
    private c q;
    @Nullable
    private b r;
    @Nullable
    private com.applovin.exoplayer2.d.f.a s;
    @Nullable
    private byte[] t;
    private byte[] u;
    @Nullable
    private m.a v;
    @Nullable
    private m.d w;
    
    public b(final UUID c, final m e, final a f, final b g, @Nullable final List<com.applovin.exoplayer2.d.e.a> list, final int h, final boolean i, final boolean j, @Nullable final byte[] u, final HashMap<String, String> k, final r b, final Looper looper, final v m) {
        if (h == 1 || h == 3) {
            com.applovin.exoplayer2.l.a.b((Object)u);
        }
        this.c = c;
        this.f = f;
        this.g = g;
        this.e = e;
        this.h = h;
        this.i = i;
        this.j = j;
        if (u != null) {
            this.u = u;
            this.a = null;
        }
        else {
            this.a = Collections.unmodifiableList((List<? extends com.applovin.exoplayer2.d.e.a>)com.applovin.exoplayer2.l.a.b((Object)list));
        }
        this.k = k;
        this.b = b;
        this.l = (i<g.a>)new i();
        this.m = m;
        this.n = 2;
        this.d = new e(looper);
    }
    
    private void a(final h<g.a> h) {
        final Iterator iterator = this.l.a().iterator();
        while (iterator.hasNext()) {
            h.accept((Object)iterator.next());
        }
    }
    
    private void a(final Exception ex, final int n) {
        this.s = new com.applovin.exoplayer2.d.f.a(ex, com.applovin.exoplayer2.d.j.a(ex, n));
        com.applovin.exoplayer2.l.q.c("DefaultDrmSession", "DRM session error", (Throwable)ex);
        this.a((h<g.a>)new Oo08(ex));
        if (this.n != 4) {
            this.n = 1;
        }
    }
    
    private void a(final Object o, final Object o2) {
        if (o == this.w) {
            if (this.n == 2 || this.m()) {
                this.w = null;
                if (o2 instanceof Exception) {
                    this.f.a((Exception)o2, false);
                    return;
                }
                try {
                    this.e.b((byte[])o2);
                    this.f.a();
                }
                catch (final Exception ex) {
                    this.f.a(ex, true);
                }
            }
        }
    }
    
    private void a(final boolean b) {
        if (this.j) {
            return;
        }
        final byte[] array = (byte[])ai.a((Object)this.t);
        final int h = this.h;
        if (h != 0 && h != 1) {
            if (h != 2) {
                if (h == 3) {
                    com.applovin.exoplayer2.l.a.b((Object)this.u);
                    com.applovin.exoplayer2.l.a.b((Object)this.t);
                    this.a(this.u, 3, b);
                }
            }
            else if (this.u == null || this.j()) {
                this.a(array, 2, b);
            }
        }
        else if (this.u == null) {
            this.a(array, 1, b);
        }
        else if (this.n == 4 || this.j()) {
            final long k = this.k();
            if (this.h == 0 && k <= 60L) {
                final StringBuilder sb = new StringBuilder();
                sb.append("Offline license has expired or will expire soon. Remaining seconds: ");
                sb.append(k);
                com.applovin.exoplayer2.l.q.a("DefaultDrmSession", sb.toString());
                this.a(array, 2, b);
            }
            else if (k <= 0L) {
                this.a(new com.applovin.exoplayer2.d.q(), 2);
            }
            else {
                this.n = 4;
                this.a((h<g.a>)new \u3007o00\u3007\u3007Oo());
            }
        }
    }
    
    private void a(final byte[] array, final int n, final boolean b) {
        try {
            this.v = this.e.a(array, this.a, n, this.k);
            ((c)ai.a((Object)this.q)).a(1, com.applovin.exoplayer2.l.a.b((Object)this.v), b);
        }
        catch (final Exception ex) {
            this.b(ex, true);
        }
    }
    
    private void b(final Exception ex, final boolean b) {
        if (ex instanceof NotProvisionedException) {
            this.f.a(this);
        }
        else {
            int n;
            if (b) {
                n = 1;
            }
            else {
                n = 2;
            }
            this.a(ex, n);
        }
    }
    
    private void b(Object o, final Object o2) {
        if (o == this.v) {
            if (this.m()) {
                this.v = null;
                if (o2 instanceof Exception) {
                    this.b((Exception)o2, false);
                    return;
                }
                try {
                    final byte[] array = (byte[])o2;
                    if (this.h == 3) {
                        this.e.a((byte[])ai.a((Object)this.u), array);
                        o = new \u3007o\u3007();
                        this.a((h<g.a>)o);
                    }
                    else {
                        final byte[] a = this.e.a(this.t, array);
                        final int h = this.h;
                        if ((h == 2 || (h == 0 && this.u != null)) && a != null && a.length != 0) {
                            this.u = a;
                        }
                        this.n = 4;
                        o = new O8();
                        this.a((h<g.a>)o);
                    }
                }
                catch (final Exception ex) {
                    this.b(ex, true);
                }
            }
        }
    }
    
    private boolean i() {
        if (this.m()) {
            return true;
        }
        try {
            final byte[] a = this.e.a();
            this.t = a;
            this.r = this.e.d(a);
            this.n = 3;
            this.a((h<g.a>)new \u3007080(3));
            com.applovin.exoplayer2.l.a.b((Object)this.t);
            return true;
        }
        catch (final Exception ex) {
            this.a(ex, 1);
        }
        catch (final NotProvisionedException ex2) {
            this.f.a(this);
        }
        return false;
    }
    
    private boolean j() {
        try {
            this.e.b(this.t, this.u);
            return true;
        }
        catch (final Exception ex) {
            this.a(ex, 1);
            return false;
        }
    }
    
    private long k() {
        if (!com.applovin.exoplayer2.h.d.equals(this.c)) {
            return Long.MAX_VALUE;
        }
        final Pair pair = (Pair)com.applovin.exoplayer2.l.a.b((Object)com.applovin.exoplayer2.d.u.a(this));
        return Math.min((long)pair.first, (long)pair.second);
    }
    
    private void l() {
        if (this.h == 0 && this.n == 4) {
            ai.a((Object)this.t);
            this.a(false);
        }
    }
    
    private boolean m() {
        final int n = this.n;
        return n == 3 || n == 4;
    }
    
    public void a() {
        this.w = this.e.b();
        ((c)ai.a((Object)this.q)).a(0, com.applovin.exoplayer2.l.a.b((Object)this.w), true);
    }
    
    public void a(final int n) {
        if (n == 2) {
            this.l();
        }
    }
    
    @Override
    public void a(@Nullable final g.a a) {
        final int o = this.o;
        final boolean b = false;
        a.b(o >= 0);
        if (a != null) {
            this.l.a((Object)a);
        }
        if (++this.o == 1) {
            boolean b2 = b;
            if (this.n == 2) {
                b2 = true;
            }
            a.b(b2);
            ((Thread)(this.p = new HandlerThread("ExoPlayer:DrmRequestHandler"))).start();
            this.q = new c(this.p.getLooper());
            if (this.i()) {
                this.a(true);
            }
        }
        else if (a != null && this.m() && this.l.c((Object)a) == 1) {
            a.a(this.n);
        }
        this.g.a(this, this.o);
    }
    
    public void a(final Exception ex, final boolean b) {
        int n;
        if (b) {
            n = 1;
        }
        else {
            n = 3;
        }
        this.a(ex, n);
    }
    
    @Override
    public boolean a(final String s) {
        return this.e.a((byte[])com.applovin.exoplayer2.l.a.a((Object)this.t), s);
    }
    
    public boolean a(final byte[] a2) {
        return Arrays.equals(this.t, a2);
    }
    
    public void b() {
        if (this.i()) {
            this.a(true);
        }
    }
    
    @Override
    public void b(@Nullable final g.a a) {
        a.b(this.o > 0);
        final int o = this.o - 1;
        this.o = o;
        if (o == 0) {
            this.n = 0;
            ((e)ai.a((Object)this.d)).removeCallbacksAndMessages((Object)null);
            ((c)ai.a((Object)this.q)).a();
            this.q = null;
            ((HandlerThread)ai.a((Object)this.p)).quit();
            this.p = null;
            this.r = null;
            this.s = null;
            this.v = null;
            this.w = null;
            final byte[] t = this.t;
            if (t != null) {
                this.e.a(t);
                this.t = null;
            }
        }
        if (a != null) {
            this.l.b((Object)a);
            if (this.l.c((Object)a) == 0) {
                a.d();
            }
        }
        this.g.b(this, this.o);
    }
    
    @Override
    public final int c() {
        return this.n;
    }
    
    @Override
    public boolean d() {
        return this.i;
    }
    
    @Nullable
    @Override
    public final com.applovin.exoplayer2.d.f.a e() {
        com.applovin.exoplayer2.d.f.a s;
        if (this.n == 1) {
            s = this.s;
        }
        else {
            s = null;
        }
        return s;
    }
    
    @Override
    public final UUID f() {
        return this.c;
    }
    
    @Nullable
    @Override
    public final b g() {
        return this.r;
    }
    
    @Nullable
    @Override
    public Map<String, String> h() {
        final byte[] t = this.t;
        Map<String, String> c;
        if (t == null) {
            c = null;
        }
        else {
            c = this.e.c(t);
        }
        return c;
    }
    
    public interface a
    {
        void a();
        
        void a(final b p0);
        
        void a(final Exception p0, final boolean p1);
    }
    
    public interface b
    {
        void a(final com.applovin.exoplayer2.d.b p0, final int p1);
        
        void b(final com.applovin.exoplayer2.d.b p0, final int p1);
    }
    
    @SuppressLint({ "HandlerLeak" })
    private class c extends Handler
    {
        final b a;
        @GuardedBy("this")
        private boolean b;
        
        public c(final b a, final Looper looper) {
            this.a = a;
            super(looper);
        }
        
        private boolean a(final Message message, final s s) {
            final d d = (d)message.obj;
            if (!d.b) {
                return false;
            }
            if (++d.e > this.a.m.a(3)) {
                return false;
            }
            final com.applovin.exoplayer2.h.j j = new com.applovin.exoplayer2.h.j(d.a, s.a, s.b, s.c, SystemClock.elapsedRealtime(), SystemClock.elapsedRealtime() - d.c, s.d);
            final com.applovin.exoplayer2.h.m m = new com.applovin.exoplayer2.h.m(3);
            IOException ex;
            if (s.getCause() instanceof IOException) {
                ex = (IOException)s.getCause();
            }
            else {
                ex = new f(s.getCause());
            }
            final long a = this.a.m.a(new v$a(j, m, ex, d.e));
            if (a == -9223372036854775807L) {
                return false;
            }
            synchronized (this) {
                if (!this.b) {
                    this.sendMessageDelayed(Message.obtain(message), a);
                    return true;
                }
                return false;
            }
        }
        
        public void a() {
            synchronized (this) {
                this.removeCallbacksAndMessages((Object)null);
                this.b = true;
            }
        }
        
        void a(final int n, final Object o, final boolean b) {
            this.obtainMessage(n, (Object)new d(com.applovin.exoplayer2.h.j.a(), b, SystemClock.elapsedRealtime(), o)).sendToTarget();
        }
        
        public void handleMessage(final Message message) {
            final d d = (d)message.obj;
            Object o = null;
            try {
                final int what = message.what;
                if (what != 0) {
                    if (what != 1) {
                        throw new RuntimeException();
                    }
                    final b a = this.a;
                    o = a.b.a(a.c, (m.a)d.d);
                }
                else {
                    final b a2 = this.a;
                    o = a2.b.a(a2.c, (m.d)d.d);
                }
            }
            catch (final Exception o) {
                com.applovin.exoplayer2.l.q.b("DefaultDrmSession", "Key/provisioning request produced an unexpected exception. Not retrying.", (Throwable)o);
            }
            catch (final s s) {
                o = s;
                if (this.a(message, s)) {
                    return;
                }
            }
            this.a.m.a(d.a);
            synchronized (this) {
                if (!this.b) {
                    this.a.d.obtainMessage(message.what, (Object)Pair.create(d.d, o)).sendToTarget();
                }
            }
        }
    }
    
    private static final class d
    {
        public final long a;
        public final boolean b;
        public final long c;
        public final Object d;
        public int e;
        
        public d(final long a, final boolean b, final long c, final Object d) {
            this.a = a;
            this.b = b;
            this.c = c;
            this.d = d;
        }
    }
    
    @SuppressLint({ "HandlerLeak" })
    private class e extends Handler
    {
        final b a;
        
        public e(final b a, final Looper looper) {
            this.a = a;
            super(looper);
        }
        
        public void handleMessage(final Message message) {
            final Pair pair = (Pair)message.obj;
            final Object first = pair.first;
            final Object second = pair.second;
            final int what = message.what;
            if (what != 0) {
                if (what == 1) {
                    this.a.b(first, second);
                }
            }
            else {
                this.a.a(first, second);
            }
        }
    }
    
    public static final class f extends IOException
    {
        public f(@Nullable final Throwable cause) {
            super(cause);
        }
    }
}

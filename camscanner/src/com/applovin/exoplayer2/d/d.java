// 
// Decompiled by Procyon v0.6.0
// 

package com.applovin.exoplayer2.d;

import com.applovin.exoplayer2.l.ai;
import com.applovin.exoplayer2.l.a;
import androidx.annotation.RequiresApi;
import com.applovin.exoplayer2.common.a.ax;
import android.net.Uri;
import java.util.Collection;
import java.util.Map;
import com.applovin.exoplayer2.k.q$a;
import androidx.annotation.Nullable;
import com.applovin.exoplayer2.k.t$b;
import androidx.annotation.GuardedBy;
import com.applovin.exoplayer2.ab;

public final class d implements i
{
    private final Object a;
    @GuardedBy("lock")
    private ab.d b;
    @GuardedBy("lock")
    private h c;
    @Nullable
    private t$b d;
    @Nullable
    private String e;
    
    public d() {
        this.a = new Object();
    }
    
    @RequiresApi(18)
    private h a(final ab.d d) {
        Object o = this.d;
        if (o == null) {
            o = new q$a().a(this.e);
        }
        final Uri b = d.b;
        String string;
        if (b == null) {
            string = null;
        }
        else {
            string = b.toString();
        }
        final p p = new p(string, d.f, (t$b)o);
        final ax<Map.Entry<String, String>> a = d.c.c().a();
        while (a.hasNext()) {
            final Map.Entry entry = a.next();
            p.a((String)entry.getKey(), (String)entry.getValue());
        }
        final c a2 = new c.a().a(d.a, com.applovin.exoplayer2.d.o.a).a(d.d).b(d.e).a(com.applovin.exoplayer2.common.b.c.a(d.g)).a(p);
        a2.a(0, d.a());
        return a2;
    }
    
    @Override
    public h a(final ab ab) {
        com.applovin.exoplayer2.l.a.b((Object)ab.c);
        final ab.d c = ab.c.c;
        if (c != null) {
            if (ai.a >= 18) {
                synchronized (this.a) {
                    if (!ai.a((Object)c, (Object)this.b)) {
                        this.b = c;
                        this.c = this.a(c);
                    }
                    return (h)com.applovin.exoplayer2.l.a.b((Object)this.c);
                }
            }
        }
        return h.b;
    }
}

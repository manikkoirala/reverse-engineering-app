// 
// Decompiled by Procyon v0.6.0
// 

package com.applovin.exoplayer2.d;

import java.util.List;
import java.util.Map;
import android.net.Uri;
import com.applovin.exoplayer2.k.l;
import java.io.IOException;

public final class s extends IOException
{
    public final l a;
    public final Uri b;
    public final Map<String, List<String>> c;
    public final long d;
    
    public s(final l a, final Uri b, final Map<String, List<String>> c, final long d, final Throwable cause) {
        super(cause);
        this.a = a;
        this.b = b;
        this.c = c;
        this.d = d;
    }
}

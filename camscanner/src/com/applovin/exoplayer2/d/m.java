// 
// Decompiled by Procyon v0.6.0
// 

package com.applovin.exoplayer2.d;

import java.util.UUID;
import android.media.MediaCryptoException;
import com.applovin.exoplayer2.c.b;
import java.util.Map;
import android.media.DeniedByServerException;
import android.media.MediaDrmException;
import android.media.NotProvisionedException;
import java.util.HashMap;
import androidx.annotation.Nullable;
import java.util.List;

public interface m
{
    a a(final byte[] p0, @Nullable final List<e.a> p1, final int p2, @Nullable final HashMap<String, String> p3) throws NotProvisionedException;
    
    void a(@Nullable final b p0);
    
    void a(final byte[] p0);
    
    boolean a(final byte[] p0, final String p1);
    
    byte[] a() throws MediaDrmException;
    
    @Nullable
    byte[] a(final byte[] p0, final byte[] p1) throws NotProvisionedException, DeniedByServerException;
    
    d b();
    
    void b(final byte[] p0) throws DeniedByServerException;
    
    void b(final byte[] p0, final byte[] p1);
    
    Map<String, String> c(final byte[] p0);
    
    void c();
    
    int d();
    
    com.applovin.exoplayer2.c.b d(final byte[] p0) throws MediaCryptoException;
    
    public static final class a
    {
        private final byte[] a;
        private final String b;
        private final int c;
        
        public a(final byte[] a, final String b, final int c) {
            this.a = a;
            this.b = b;
            this.c = c;
        }
        
        public byte[] a() {
            return this.a;
        }
        
        public String b() {
            return this.b;
        }
    }
    
    public interface b
    {
        void a(final m p0, @Nullable final byte[] p1, final int p2, final int p3, @Nullable final byte[] p4);
    }
    
    public interface c
    {
        m acquireExoMediaDrm(final UUID p0);
    }
    
    public static final class d
    {
        private final byte[] a;
        private final String b;
        
        public d(final byte[] a, final String b) {
            this.a = a;
            this.b = b;
        }
        
        public byte[] a() {
            return this.a;
        }
        
        public String b() {
            return this.b;
        }
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.applovin.exoplayer2.d;

import org.json.JSONArray;
import org.json.JSONException;
import com.applovin.exoplayer2.l.q;
import org.json.JSONObject;
import com.applovin.exoplayer2.l.ai;

final class a
{
    private static String a(final String s) {
        return s.replace('+', '-').replace('/', '_');
    }
    
    public static byte[] a(final byte[] array) {
        if (ai.a >= 27) {
            return array;
        }
        return ai.c(a(ai.a(array)));
    }
    
    private static String b(final String s) {
        return s.replace('-', '+').replace('_', '/');
    }
    
    public static byte[] b(final byte[] array) {
        if (ai.a >= 27) {
            return array;
        }
        try {
            final JSONObject jsonObject = new JSONObject(ai.a(array));
            final StringBuilder sb = new StringBuilder("{\"keys\":[");
            final JSONArray jsonArray = jsonObject.getJSONArray("keys");
            for (int i = 0; i < jsonArray.length(); ++i) {
                if (i != 0) {
                    sb.append(",");
                }
                final JSONObject jsonObject2 = jsonArray.getJSONObject(i);
                sb.append("{\"k\":\"");
                sb.append(b(jsonObject2.getString("k")));
                sb.append("\",\"kid\":\"");
                sb.append(b(jsonObject2.getString("kid")));
                sb.append("\",\"kty\":\"");
                sb.append(jsonObject2.getString("kty"));
                sb.append("\"}");
            }
            sb.append("]}");
            return ai.c(sb.toString());
        }
        catch (final JSONException ex) {
            final StringBuilder sb2 = new StringBuilder();
            sb2.append("Failed to adjust response data: ");
            sb2.append(ai.a(array));
            q.c("ClearKeyUtil", sb2.toString(), (Throwable)ex);
            return array;
        }
    }
}

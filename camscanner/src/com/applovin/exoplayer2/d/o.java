// 
// Decompiled by Procyon v0.6.0
// 

package com.applovin.exoplayer2.d;

import com.applovin.exoplayer2.c.b;
import java.util.Map;
import android.media.MediaDrm$ProvisionRequest;
import android.media.DeniedByServerException;
import android.media.MediaDrmException;
import android.media.MediaCryptoException;
import android.media.MediaCrypto;
import android.media.MediaDrm$OnEventListener;
import android.media.NotProvisionedException;
import android.media.MediaDrm$KeyRequest;
import android.text.TextUtils;
import java.util.HashMap;
import androidx.annotation.Nullable;
import java.nio.charset.Charset;
import java.nio.ByteOrder;
import java.nio.ByteBuffer;
import com.applovin.exoplayer2.common.base.Charsets;
import com.applovin.exoplayer2.l.y;
import com.applovin.exoplayer2.l.q;
import android.annotation.SuppressLint;
import com.applovin.exoplayer2.l.ai;
import java.util.List;
import android.media.UnsupportedSchemeException;
import com.applovin.exoplayer2.h;
import com.applovin.exoplayer2.l.a;
import android.media.MediaDrm;
import java.util.UUID;
import androidx.annotation.RequiresApi;

@RequiresApi(18)
public final class o implements m
{
    public static final c a;
    private final UUID b;
    private final MediaDrm c;
    private int d;
    
    static {
        a = new oo88o8O();
    }
    
    private o(final UUID obj) throws UnsupportedSchemeException {
        com.applovin.exoplayer2.l.a.b((Object)obj);
        com.applovin.exoplayer2.l.a.a(h.b.equals(obj) ^ true, (Object)"Use C.CLEARKEY_UUID instead");
        this.b = obj;
        final MediaDrm c = new MediaDrm(b(obj));
        this.c = c;
        this.d = 1;
        if (h.d.equals(obj) && e()) {
            a(c);
        }
    }
    
    private static e.a a(final UUID obj, final List<e.a> list) {
        if (!h.d.equals(obj)) {
            return (e.a)list.get(0);
        }
        Label_0224: {
            if (ai.a >= 28 && list.size() > 1) {
                final e.a a = (e.a)list.get(0);
                int i = 0;
                int n = 0;
                while (true) {
                    while (i < list.size()) {
                        final e.a a2 = list.get(i);
                        final byte[] array = (byte[])com.applovin.exoplayer2.l.a.b((Object)a2.d);
                        if (ai.a((Object)a2.c, (Object)a.c) && ai.a((Object)a2.b, (Object)a.b) && com.applovin.exoplayer2.e.g.h.a(array)) {
                            n += array.length;
                            ++i;
                        }
                        else {
                            final boolean b = false;
                            if (b) {
                                final byte[] array2 = new byte[n];
                                int j = 0;
                                int n2 = 0;
                                while (j < list.size()) {
                                    final byte[] array3 = (byte[])com.applovin.exoplayer2.l.a.b((Object)list.get(j).d);
                                    final int length = array3.length;
                                    System.arraycopy(array3, 0, array2, n2, length);
                                    n2 += length;
                                    ++j;
                                }
                                return a.a(array2);
                            }
                            break Label_0224;
                        }
                    }
                    final boolean b = true;
                    continue;
                }
            }
        }
        for (int k = 0; k < list.size(); ++k) {
            final e.a a3 = list.get(k);
            final int c = com.applovin.exoplayer2.e.g.h.c((byte[])com.applovin.exoplayer2.l.a.b((Object)a3.d));
            final int a4 = ai.a;
            if (a4 < 23 && c == 0) {
                return a3;
            }
            if (a4 >= 23 && c == 1) {
                return a3;
            }
        }
        return (e.a)list.get(0);
    }
    
    public static o a(final UUID uuid) throws t {
        try {
            return new o(uuid);
        }
        catch (final Exception ex) {
            throw new t(2, ex);
        }
        catch (final UnsupportedSchemeException ex2) {
            throw new t(1, (Exception)ex2);
        }
    }
    
    private static String a(final UUID obj, final String s) {
        if (ai.a < 26 && h.c.equals(obj) && ("video/mp4".equals(s) || "audio/mp4".equals(s))) {
            return "cenc";
        }
        return s;
    }
    
    @SuppressLint({ "WrongConstant" })
    private static void a(final MediaDrm mediaDrm) {
        mediaDrm.setPropertyString("securityLevel", "L3");
    }
    
    private static byte[] a(final UUID obj, byte[] array) {
        final UUID e = h.e;
        byte[] a = array;
        if (e.equals(obj)) {
            final byte[] a2 = com.applovin.exoplayer2.e.g.h.a(array, obj);
            if (a2 != null) {
                array = a2;
            }
            a = com.applovin.exoplayer2.e.g.h.a(e, f(array));
        }
        if (ai.a >= 23 || !h.d.equals(obj)) {
            if (!e.equals(obj) || !"Amazon".equals(ai.c)) {
                return a;
            }
            final String d = ai.d;
            if (!"AFTB".equals(d) && !"AFTS".equals(d) && !"AFTM".equals(d) && !"AFTT".equals(d)) {
                return a;
            }
        }
        final byte[] a3 = com.applovin.exoplayer2.e.g.h.a(a, obj);
        if (a3 != null) {
            return a3;
        }
        return a;
    }
    
    private static UUID b(final UUID obj) {
        UUID b = obj;
        if (ai.a < 27) {
            b = obj;
            if (h.c.equals(obj)) {
                b = h.b;
            }
        }
        return b;
    }
    
    private static byte[] b(final UUID obj, final byte[] array) {
        if (h.c.equals(obj)) {
            return com.applovin.exoplayer2.d.a.a(array);
        }
        return array;
    }
    
    private static boolean e() {
        return "ASUS_Z00AD".equals(ai.d);
    }
    
    private static byte[] f(final byte[] array) {
        final y y = new y(array);
        int r = y.r();
        final short l = y.l();
        final short i = y.l();
        if (l != 1 || i != 1) {
            q.b("FrameworkMediaDrm", "Unexpected record count or type. Skipping LA_URL workaround.");
            return array;
        }
        final short j = y.l();
        final Charset utf_16LE = Charsets.UTF_16LE;
        final String a = y.a((int)j, utf_16LE);
        if (a.contains("<LA_URL>")) {
            return array;
        }
        final int index = a.indexOf("</DATA>");
        if (index == -1) {
            q.c("FrameworkMediaDrm", "Could not find the </DATA> tag. Skipping LA_URL workaround.");
        }
        final StringBuilder sb = new StringBuilder();
        sb.append(a.substring(0, index));
        sb.append("<LA_URL>https://x</LA_URL>");
        sb.append(a.substring(index));
        final String string = sb.toString();
        r += 52;
        final ByteBuffer allocate = ByteBuffer.allocate(r);
        allocate.order(ByteOrder.LITTLE_ENDIAN);
        allocate.putInt(r);
        allocate.putShort(l);
        allocate.putShort(i);
        allocate.putShort((short)(string.length() * 2));
        allocate.put(string.getBytes(utf_16LE));
        return allocate.array();
    }
    
    @Override
    public m.a a(final byte[] array, @Nullable final List<e.a> list, int \u3007080, @Nullable final HashMap<String, String> hashMap) throws NotProvisionedException {
        e.a a;
        byte[] a2;
        String a3;
        if (list != null) {
            a = a(this.b, list);
            a2 = a(this.b, (byte[])com.applovin.exoplayer2.l.a.b((Object)a.d));
            a3 = a(this.b, a.c);
        }
        else {
            a = null;
            a2 = null;
            a3 = null;
        }
        final MediaDrm$KeyRequest keyRequest = this.c.getKeyRequest(array, a2, a3, \u3007080, (HashMap)hashMap);
        final byte[] b = b(this.b, keyRequest.getData());
        String defaultUrl;
        if ("https://x".equals(defaultUrl = keyRequest.getDefaultUrl())) {
            defaultUrl = "";
        }
        String b2 = defaultUrl;
        if (TextUtils.isEmpty((CharSequence)defaultUrl)) {
            b2 = defaultUrl;
            if (a != null) {
                b2 = defaultUrl;
                if (!TextUtils.isEmpty((CharSequence)a.b)) {
                    b2 = a.b;
                }
            }
        }
        if (ai.a >= 23) {
            \u3007080 = o800o8O.\u3007080(keyRequest);
        }
        else {
            \u3007080 = Integer.MIN_VALUE;
        }
        return new m.a(b, b2, \u3007080);
    }
    
    public String a(final String s) {
        return this.c.getPropertyString(s);
    }
    
    @Override
    public void a(@Nullable final b b) {
        final MediaDrm c = this.c;
        Object onEventListener;
        if (b == null) {
            onEventListener = null;
        }
        else {
            onEventListener = new \u3007O888o0o(this, b);
        }
        c.setOnEventListener((MediaDrm$OnEventListener)onEventListener);
    }
    
    @Override
    public void a(final byte[] array) {
        this.c.closeSession(array);
    }
    
    @Override
    public boolean a(byte[] array, final String s) {
        if (ai.a >= 31) {
            return o.a.a(this.c, s);
        }
        try {
            array = (byte[])(Object)new MediaCrypto(this.b, array);
            try {
                return ((MediaCrypto)(Object)array).requiresSecureDecoderComponent(s);
            }
            finally {
                ((MediaCrypto)(Object)array).release();
            }
        }
        catch (final MediaCryptoException ex) {
            return true;
        }
    }
    
    @Override
    public byte[] a() throws MediaDrmException {
        return this.c.openSession();
    }
    
    @Nullable
    @Override
    public byte[] a(final byte[] array, final byte[] array2) throws NotProvisionedException, DeniedByServerException {
        byte[] b = array2;
        if (h.c.equals(this.b)) {
            b = com.applovin.exoplayer2.d.a.b(array2);
        }
        return this.c.provideKeyResponse(array, b);
    }
    
    @Override
    public d b() {
        final MediaDrm$ProvisionRequest provisionRequest = this.c.getProvisionRequest();
        return new d(provisionRequest.getData(), provisionRequest.getDefaultUrl());
    }
    
    @Override
    public void b(final byte[] array) throws DeniedByServerException {
        this.c.provideProvisionResponse(array);
    }
    
    @Override
    public void b(final byte[] array, final byte[] array2) {
        this.c.restoreKeys(array, array2);
    }
    
    @Override
    public Map<String, String> c(final byte[] array) {
        return this.c.queryKeyStatus(array);
    }
    
    @Override
    public void c() {
        synchronized (this) {
            final int d = this.d - 1;
            this.d = d;
            if (d == 0) {
                this.c.release();
            }
        }
    }
    
    @Override
    public int d() {
        return 2;
    }
    
    public n e(final byte[] array) throws MediaCryptoException {
        return new n(b(this.b), array, ai.a < 21 && h.d.equals(this.b) && "L3".equals(this.a("securityLevel")));
    }
    
    @RequiresApi(31)
    private static class a
    {
        public static boolean a(final MediaDrm mediaDrm, final String s) {
            return \u3007oo\u3007.\u3007080(mediaDrm, s);
        }
    }
}

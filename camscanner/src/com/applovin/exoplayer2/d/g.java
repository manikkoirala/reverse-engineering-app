// 
// Decompiled by Procyon v0.6.0
// 

package com.applovin.exoplayer2.d;

import com.applovin.exoplayer2.l.a;
import android.os.Handler;
import java.util.Iterator;
import com.applovin.exoplayer2.l.ai;
import androidx.annotation.CheckResult;
import java.util.concurrent.CopyOnWriteArrayList;
import androidx.annotation.Nullable;
import com.applovin.exoplayer2.h.p;

public interface g
{
    void a(final int p0, @Nullable final p.a p1);
    
    void a(final int p0, @Nullable final p.a p1, final int p2);
    
    void a(final int p0, @Nullable final p.a p1, final Exception p2);
    
    void b(final int p0, @Nullable final p.a p1);
    
    void c(final int p0, @Nullable final p.a p1);
    
    void d(final int p0, @Nullable final p.a p1);
    
    @Deprecated
    void e(final int p0, @Nullable final p.a p1);
    
    public static class a
    {
        public final int a;
        @Nullable
        public final p.a b;
        private final CopyOnWriteArrayList<g.a.a> c;
        
        public a() {
            this(new CopyOnWriteArrayList<g.a.a>(), 0, null);
        }
        
        private a(final CopyOnWriteArrayList<g.a.a> c, final int a, @Nullable final p.a b) {
            this.c = c;
            this.a = a;
            this.b = b;
        }
        
        @CheckResult
        public g.a a(final int n, @Nullable final p.a a) {
            return new g.a(this.c, n, a);
        }
        
        public void a() {
            for (final g.a.a a : this.c) {
                ai.a(a.a, (Runnable)new Oooo8o0\u3007(this, a.b));
            }
        }
        
        public void a(final int n) {
            for (final g.a.a a : this.c) {
                ai.a(a.a, (Runnable)new OO0o\u3007\u3007(this, a.b, n));
            }
        }
        
        public void a(final Handler handler, final g g) {
            com.applovin.exoplayer2.l.a.b((Object)handler);
            com.applovin.exoplayer2.l.a.b((Object)g);
            this.c.add(new g.a.a(handler, g));
        }
        
        public void a(final g g) {
            for (final g.a.a o : this.c) {
                if (o.b == g) {
                    this.c.remove(o);
                }
            }
        }
        
        public void a(final Exception ex) {
            for (final g.a.a a : this.c) {
                ai.a(a.a, (Runnable)new \u3007O8o08O(this, a.b, ex));
            }
        }
        
        public void b() {
            for (final g.a.a a : this.c) {
                ai.a(a.a, (Runnable)new \u30078o8o\u3007(this, a.b));
            }
        }
        
        public void c() {
            for (final g.a.a a : this.c) {
                ai.a(a.a, (Runnable)new \u3007\u3007808\u3007(this, a.b));
            }
        }
        
        public void d() {
            for (final g.a.a a : this.c) {
                ai.a(a.a, (Runnable)new \u3007O00(this, a.b));
            }
        }
        
        private static final class a
        {
            public Handler a;
            public g b;
            
            public a(final Handler a, final g b) {
                this.a = a;
                this.b = b;
            }
        }
    }
}

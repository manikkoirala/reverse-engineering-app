// 
// Decompiled by Procyon v0.6.0
// 

package com.applovin.exoplayer2.d;

import java.io.IOException;
import java.util.Map;
import com.applovin.exoplayer2.c.b;
import java.util.UUID;
import androidx.annotation.Nullable;

public interface f
{
    void a(@Nullable final g.a p0);
    
    boolean a(final String p0);
    
    void b(@Nullable final g.a p0);
    
    int c();
    
    boolean d();
    
    @Nullable
    a e();
    
    UUID f();
    
    @Nullable
    b g();
    
    @Nullable
    Map<String, String> h();
    
    public static class a extends IOException
    {
        public final int a;
        
        public a(final Throwable cause, final int a) {
            super(cause);
            this.a = a;
        }
    }
}

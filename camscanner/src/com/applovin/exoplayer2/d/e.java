// 
// Decompiled by Procyon v0.6.0
// 

package com.applovin.exoplayer2.d;

import com.applovin.exoplayer2.l.a;
import java.util.UUID;
import com.applovin.exoplayer2.h;
import java.util.List;
import java.util.Arrays;
import com.applovin.exoplayer2.l.ai;
import android.os.Parcel;
import androidx.annotation.Nullable;
import android.os.Parcelable$Creator;
import java.util.Comparator;
import android.os.Parcelable;

public final class e implements Parcelable, Comparator<a>
{
    public static final Parcelable$Creator<e> CREATOR;
    @Nullable
    public final String a;
    public final int b;
    private final a[] c;
    private int d;
    
    static {
        CREATOR = (Parcelable$Creator)new Parcelable$Creator<e>() {
            public e a(final Parcel parcel) {
                return new e(parcel);
            }
            
            public e[] a(final int n) {
                return new e[n];
            }
        };
    }
    
    e(final Parcel parcel) {
        this.a = parcel.readString();
        final a[] c = (a[])ai.a((Object)parcel.createTypedArray((Parcelable$Creator)e.a.CREATOR));
        this.c = c;
        this.b = c.length;
    }
    
    private e(@Nullable final String a, final boolean b, final a... array) {
        this.a = a;
        a[] array2 = array;
        if (b) {
            array2 = array.clone();
        }
        this.c = array2;
        this.b = array2.length;
        Arrays.sort(array2, this);
    }
    
    public e(@Nullable final String s, final a... array) {
        this(s, true, array);
    }
    
    public e(final List<a> list) {
        this(null, false, (a[])list.toArray(new a[0]));
    }
    
    public e(final a... array) {
        this((String)null, array);
    }
    
    public int a(final a a, final a a2) {
        final UUID a3 = h.a;
        int compareTo;
        if (a3.equals(a.a)) {
            if (a3.equals(a2.a)) {
                compareTo = 0;
            }
            else {
                compareTo = 1;
            }
        }
        else {
            compareTo = a.a.compareTo(a2.a);
        }
        return compareTo;
    }
    
    public a a(final int n) {
        return this.c[n];
    }
    
    public e a(@Nullable final String s) {
        if (ai.a((Object)this.a, (Object)s)) {
            return this;
        }
        return new e(s, false, this.c);
    }
    
    public int describeContents() {
        return 0;
    }
    
    @Override
    public boolean equals(@Nullable final Object o) {
        boolean b = true;
        if (this == o) {
            return true;
        }
        if (o != null && e.class == o.getClass()) {
            final e e = (e)o;
            if (!ai.a((Object)this.a, (Object)e.a) || !Arrays.equals(this.c, e.c)) {
                b = false;
            }
            return b;
        }
        return false;
    }
    
    @Override
    public int hashCode() {
        if (this.d == 0) {
            final String a = this.a;
            int hashCode;
            if (a == null) {
                hashCode = 0;
            }
            else {
                hashCode = a.hashCode();
            }
            this.d = hashCode * 31 + Arrays.hashCode(this.c);
        }
        return this.d;
    }
    
    public void writeToParcel(final Parcel parcel, final int n) {
        parcel.writeString(this.a);
        parcel.writeTypedArray((Parcelable[])this.c, 0);
    }
    
    public static final class a implements Parcelable
    {
        public static final Parcelable$Creator<a> CREATOR;
        public final UUID a;
        @Nullable
        public final String b;
        public final String c;
        @Nullable
        public final byte[] d;
        private int e;
        
        static {
            CREATOR = (Parcelable$Creator)new Parcelable$Creator<a>() {
                public a a(final Parcel parcel) {
                    return new a(parcel);
                }
                
                public a[] a(final int n) {
                    return new a[n];
                }
            };
        }
        
        a(final Parcel parcel) {
            this.a = new UUID(parcel.readLong(), parcel.readLong());
            this.b = parcel.readString();
            this.c = (String)ai.a((Object)parcel.readString());
            this.d = parcel.createByteArray();
        }
        
        public a(final UUID uuid, @Nullable final String b, final String s, @Nullable final byte[] d) {
            this.a = (UUID)com.applovin.exoplayer2.l.a.b((Object)uuid);
            this.b = b;
            this.c = (String)com.applovin.exoplayer2.l.a.b((Object)s);
            this.d = d;
        }
        
        public a(final UUID uuid, final String s, @Nullable final byte[] array) {
            this(uuid, null, s, array);
        }
        
        public a a(@Nullable final byte[] array) {
            return new a(this.a, this.b, this.c, array);
        }
        
        public boolean a(final UUID uuid) {
            return h.a.equals(this.a) || uuid.equals(this.a);
        }
        
        public int describeContents() {
            return 0;
        }
        
        @Override
        public boolean equals(@Nullable final Object o) {
            final boolean b = o instanceof a;
            final boolean b2 = false;
            if (!b) {
                return false;
            }
            if (o == this) {
                return true;
            }
            final a a = (a)o;
            boolean b3 = b2;
            if (ai.a((Object)this.b, (Object)a.b)) {
                b3 = b2;
                if (ai.a((Object)this.c, (Object)a.c)) {
                    b3 = b2;
                    if (ai.a((Object)this.a, (Object)a.a)) {
                        b3 = b2;
                        if (Arrays.equals(this.d, a.d)) {
                            b3 = true;
                        }
                    }
                }
            }
            return b3;
        }
        
        @Override
        public int hashCode() {
            if (this.e == 0) {
                final int hashCode = this.a.hashCode();
                final String b = this.b;
                int hashCode2;
                if (b == null) {
                    hashCode2 = 0;
                }
                else {
                    hashCode2 = b.hashCode();
                }
                this.e = ((hashCode * 31 + hashCode2) * 31 + this.c.hashCode()) * 31 + Arrays.hashCode(this.d);
            }
            return this.e;
        }
        
        public void writeToParcel(final Parcel parcel, final int n) {
            parcel.writeLong(this.a.getMostSignificantBits());
            parcel.writeLong(this.a.getLeastSignificantBits());
            parcel.writeString(this.b);
            parcel.writeString(this.c);
            parcel.writeByteArray(this.d);
        }
    }
}

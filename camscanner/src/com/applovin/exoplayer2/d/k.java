// 
// Decompiled by Procyon v0.6.0
// 

package com.applovin.exoplayer2.d;

import com.applovin.exoplayer2.c.b;
import java.util.Map;
import android.media.MediaDrmException;
import java.util.HashMap;
import androidx.annotation.Nullable;
import java.util.List;
import androidx.annotation.RequiresApi;

@RequiresApi(18)
public final class k implements m
{
    @Override
    public a a(final byte[] array, @Nullable final List<e.a> list, final int n, @Nullable final HashMap<String, String> hashMap) {
        throw new IllegalStateException();
    }
    
    @Override
    public void a(@Nullable final b b) {
    }
    
    @Override
    public void a(final byte[] array) {
    }
    
    @Override
    public boolean a(final byte[] array, final String s) {
        throw new IllegalStateException();
    }
    
    @Override
    public byte[] a() throws MediaDrmException {
        throw new MediaDrmException("Attempting to open a session using a dummy ExoMediaDrm.");
    }
    
    @Nullable
    @Override
    public byte[] a(final byte[] array, final byte[] array2) {
        throw new IllegalStateException();
    }
    
    @Override
    public d b() {
        throw new IllegalStateException();
    }
    
    @Override
    public void b(final byte[] array) {
        throw new IllegalStateException();
    }
    
    @Override
    public void b(final byte[] array, final byte[] array2) {
        throw new IllegalStateException();
    }
    
    @Override
    public Map<String, String> c(final byte[] array) {
        throw new IllegalStateException();
    }
    
    @Override
    public void c() {
    }
    
    @Override
    public int d() {
        return 1;
    }
    
    @Override
    public com.applovin.exoplayer2.c.b d(final byte[] array) {
        throw new IllegalStateException();
    }
}

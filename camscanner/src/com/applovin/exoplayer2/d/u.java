// 
// Decompiled by Procyon v0.6.0
// 

package com.applovin.exoplayer2.d;

import androidx.annotation.Nullable;
import android.util.Pair;
import java.util.Map;

public final class u
{
    private static long a(final Map<String, String> map, final String s) {
        if (map == null) {
            return -9223372036854775807L;
        }
        try {
            final String s2 = map.get(s);
            if (s2 != null) {
                return Long.parseLong(s2);
            }
            return -9223372036854775807L;
        }
        catch (final NumberFormatException ex) {
            return -9223372036854775807L;
        }
    }
    
    @Nullable
    public static Pair<Long, Long> a(final f f) {
        final Map<String, String> h = f.h();
        if (h == null) {
            return null;
        }
        return (Pair<Long, Long>)new Pair((Object)a(h, "LicenseDurationRemaining"), (Object)a(h, "PlaybackDurationRemaining"));
    }
}

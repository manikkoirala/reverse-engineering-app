// 
// Decompiled by Procyon v0.6.0
// 

package com.applovin.exoplayer2.d;

import android.os.SystemClock;
import java.util.HashSet;
import android.os.Message;
import android.annotation.SuppressLint;
import com.applovin.exoplayer2.common.a.ax;
import java.util.Collection;
import com.applovin.exoplayer2.common.a.w;
import android.media.ResourceBusyException;
import java.util.Iterator;
import com.applovin.exoplayer2.l.q;
import com.applovin.exoplayer2.l.u;
import com.applovin.exoplayer2.common.a.s;
import com.applovin.exoplayer2.l.ai;
import com.applovin.exoplayer2.common.a.aq;
import java.util.ArrayList;
import com.applovin.exoplayer2.l.a;
import android.os.Handler;
import android.os.Looper;
import java.util.Set;
import java.util.List;
import com.applovin.exoplayer2.k.v;
import java.util.HashMap;
import java.util.UUID;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;

@RequiresApi(18)
public class c implements h
{
    @Nullable
    volatile c a;
    private final UUID d;
    private final m.c e;
    private final r f;
    private final HashMap<String, String> g;
    private final boolean h;
    private final int[] i;
    private final boolean j;
    private final f k;
    private final v l;
    private final g m;
    private final long n;
    private final List<com.applovin.exoplayer2.d.b> o;
    private final Set<e> p;
    private final Set<com.applovin.exoplayer2.d.b> q;
    private int r;
    @Nullable
    private m s;
    @Nullable
    private com.applovin.exoplayer2.d.b t;
    @Nullable
    private com.applovin.exoplayer2.d.b u;
    private Looper v;
    private Handler w;
    private int x;
    @Nullable
    private byte[] y;
    
    private c(final UUID uuid, final m.c e, final r f, final HashMap<String, String> g, final boolean h, final int[] i, final boolean j, final v l, final long n) {
        com.applovin.exoplayer2.l.a.b((Object)uuid);
        com.applovin.exoplayer2.l.a.a(com.applovin.exoplayer2.h.b.equals(uuid) ^ true, (Object)"Use C.CLEARKEY_UUID instead");
        this.d = uuid;
        this.e = e;
        this.f = f;
        this.g = g;
        this.h = h;
        this.i = i;
        this.j = j;
        this.l = l;
        this.k = new f();
        this.m = new g();
        this.x = 0;
        this.o = new ArrayList<com.applovin.exoplayer2.d.b>();
        this.p = aq.b();
        this.q = aq.b();
        this.n = n;
    }
    
    private com.applovin.exoplayer2.d.b a(@Nullable final List<com.applovin.exoplayer2.d.e.a> list, final boolean b, @Nullable final com.applovin.exoplayer2.d.g.a a) {
        a.b((Object)this.s);
        final com.applovin.exoplayer2.d.b b2 = new com.applovin.exoplayer2.d.b(this.d, this.s, (com.applovin.exoplayer2.d.b.a)this.k, (com.applovin.exoplayer2.d.b.b)this.m, list, this.x, this.j | b, b, this.y, this.g, this.f, (Looper)a.b((Object)this.v), this.l);
        b2.a(a);
        if (this.n != -9223372036854775807L) {
            b2.a((com.applovin.exoplayer2.d.g.a)null);
        }
        return b2;
    }
    
    private com.applovin.exoplayer2.d.b a(@Nullable final List<com.applovin.exoplayer2.d.e.a> list, final boolean b, @Nullable final com.applovin.exoplayer2.d.g.a a, final boolean b2) {
        com.applovin.exoplayer2.d.b b4;
        final com.applovin.exoplayer2.d.b b3 = b4 = this.a(list, b, a);
        if (a(b3)) {
            b4 = b3;
            if (!this.q.isEmpty()) {
                this.c();
                this.a(b3, a);
                b4 = this.a(list, b, a);
            }
        }
        com.applovin.exoplayer2.d.b a2 = b4;
        if (a(b4)) {
            a2 = b4;
            if (b2) {
                a2 = b4;
                if (!this.p.isEmpty()) {
                    this.d();
                    if (!this.q.isEmpty()) {
                        this.c();
                    }
                    this.a(b4, a);
                    a2 = this.a(list, b, a);
                }
            }
        }
        return a2;
    }
    
    @Nullable
    private com.applovin.exoplayer2.d.f a(final int n, final boolean b) {
        final m m = (m)com.applovin.exoplayer2.l.a.b((Object)this.s);
        if ((m.d() != 2 || !n.a) && ai.a(this.i, n) != -1 && m.d() != 1) {
            final com.applovin.exoplayer2.d.b t = this.t;
            if (t == null) {
                final com.applovin.exoplayer2.d.b a = this.a((List<com.applovin.exoplayer2.d.e.a>)com.applovin.exoplayer2.common.a.s.g(), true, null, b);
                this.o.add(a);
                this.t = a;
            }
            else {
                t.a((com.applovin.exoplayer2.d.g.a)null);
            }
            return this.t;
        }
        return null;
    }
    
    @Nullable
    private com.applovin.exoplayer2.d.f a(final Looper looper, @Nullable final com.applovin.exoplayer2.d.g.a a, final com.applovin.exoplayer2.v v, final boolean b) {
        this.b(looper);
        final com.applovin.exoplayer2.d.e o = v.o;
        if (o == null) {
            return this.a(com.applovin.exoplayer2.l.u.e(v.l), b);
        }
        final byte[] y = this.y;
        final com.applovin.exoplayer2.d.b b2 = null;
        List<com.applovin.exoplayer2.d.e.a> a2;
        if (y == null) {
            if ((a2 = a((com.applovin.exoplayer2.d.e)a.b((Object)o), this.d, false)).isEmpty()) {
                final d d = new d(this.d);
                com.applovin.exoplayer2.l.q.c("DefaultDrmSessionMgr", "DRM error", (Throwable)d);
                if (a != null) {
                    a.a(d);
                }
                return new l(new com.applovin.exoplayer2.d.f.a(d, 6003));
            }
        }
        else {
            a2 = null;
        }
        com.applovin.exoplayer2.d.b u;
        if (!this.h) {
            u = this.u;
        }
        else {
            final Iterator<com.applovin.exoplayer2.d.b> iterator = this.o.iterator();
            do {
                u = b2;
                if (!iterator.hasNext()) {
                    break;
                }
                u = iterator.next();
            } while (!ai.a((Object)u.a, (Object)a2));
        }
        if (u == null) {
            u = this.a(a2, false, a, b);
            if (!this.h) {
                this.u = u;
            }
            this.o.add(u);
        }
        else {
            u.a(a);
        }
        return u;
    }
    
    private static List<com.applovin.exoplayer2.d.e.a> a(final com.applovin.exoplayer2.d.e e, final UUID obj, final boolean b) {
        final ArrayList list = new ArrayList(e.b);
        for (int i = 0; i < e.b; ++i) {
            final com.applovin.exoplayer2.d.e.a a = e.a(i);
            if ((a.a(obj) || (com.applovin.exoplayer2.h.c.equals(obj) && a.a(com.applovin.exoplayer2.h.b))) && (a.d != null || b)) {
                list.add(a);
            }
        }
        return list;
    }
    
    private void a(final Looper v) {
        synchronized (this) {
            final Looper v2 = this.v;
            if (v2 == null) {
                this.v = v;
                this.w = new Handler(v);
            }
            else {
                com.applovin.exoplayer2.l.a.b(v2 == v);
                com.applovin.exoplayer2.l.a.b((Object)this.w);
            }
        }
    }
    
    private void a(final com.applovin.exoplayer2.d.f f, @Nullable final com.applovin.exoplayer2.d.g.a a) {
        f.b(a);
        if (this.n != -9223372036854775807L) {
            f.b(null);
        }
    }
    
    private boolean a(final com.applovin.exoplayer2.d.e e) {
        final byte[] y = this.y;
        boolean b = true;
        if (y != null) {
            return true;
        }
        if (a(e, this.d, true).isEmpty()) {
            if (e.b != 1 || !e.a(0).a(com.applovin.exoplayer2.h.b)) {
                return false;
            }
            final StringBuilder sb = new StringBuilder();
            sb.append("DrmInitData only contains common PSSH SchemeData. Assuming support for: ");
            sb.append(this.d);
            com.applovin.exoplayer2.l.q.c("DefaultDrmSessionMgr", sb.toString());
        }
        final String a = e.a;
        if (a == null || "cenc".equals(a)) {
            return true;
        }
        if ("cbcs".equals(a)) {
            if (ai.a < 25) {
                b = false;
            }
            return b;
        }
        return !"cbc1".equals(a) && !"cens".equals(a);
    }
    
    private static boolean a(final com.applovin.exoplayer2.d.f f) {
        final int c = f.c();
        final boolean b = true;
        if (c == 1) {
            boolean b2 = b;
            if (ai.a < 19) {
                return b2;
            }
            if (((com.applovin.exoplayer2.d.f.a)com.applovin.exoplayer2.l.a.b((Object)f.e())).getCause() instanceof ResourceBusyException) {
                b2 = b;
                return b2;
            }
        }
        return false;
    }
    
    private void b(final Looper looper) {
        if (this.a == null) {
            this.a = new c(looper);
        }
    }
    
    private void c() {
        final ax<Object> a = com.applovin.exoplayer2.common.a.w.a((Collection<?>)this.q).a();
        while (a.hasNext()) {
            a.next().b(null);
        }
    }
    
    private void d() {
        final ax<Object> a = com.applovin.exoplayer2.common.a.w.a((Collection<?>)this.p).a();
        while (a.hasNext()) {
            a.next().release();
        }
    }
    
    private void e() {
        if (this.s != null && this.r == 0 && this.o.isEmpty() && this.p.isEmpty()) {
            ((m)com.applovin.exoplayer2.l.a.b((Object)this.s)).c();
            this.s = null;
        }
    }
    
    @Override
    public int a(final com.applovin.exoplayer2.v v) {
        int d = ((m)com.applovin.exoplayer2.l.a.b((Object)this.s)).d();
        final com.applovin.exoplayer2.d.e o = v.o;
        if (o == null) {
            if (ai.a(this.i, com.applovin.exoplayer2.l.u.e(v.l)) == -1) {
                d = 0;
            }
            return d;
        }
        if (!this.a(o)) {
            d = 1;
        }
        return d;
    }
    
    @Override
    public h.a a(final Looper looper, @Nullable final com.applovin.exoplayer2.d.g.a a, final com.applovin.exoplayer2.v v) {
        a.b(this.r > 0);
        this.a(looper);
        final e e = new e(a);
        e.a(v);
        return e;
    }
    
    @Override
    public final void a() {
        if (this.r++ != 0) {
            return;
        }
        if (this.s == null) {
            (this.s = this.e.acquireExoMediaDrm(this.d)).a((m.b)new b());
        }
        else if (this.n != -9223372036854775807L) {
            for (int i = 0; i < this.o.size(); ++i) {
                this.o.get(i).a((com.applovin.exoplayer2.d.g.a)null);
            }
        }
    }
    
    public void a(final int x, @Nullable final byte[] y) {
        com.applovin.exoplayer2.l.a.b(this.o.isEmpty());
        if (x == 1 || x == 3) {
            com.applovin.exoplayer2.l.a.b((Object)y);
        }
        this.x = x;
        this.y = y;
    }
    
    @Nullable
    @Override
    public com.applovin.exoplayer2.d.f b(final Looper looper, @Nullable final com.applovin.exoplayer2.d.g.a a, final com.applovin.exoplayer2.v v) {
        a.b(this.r > 0);
        this.a(looper);
        return this.a(looper, a, v, true);
    }
    
    @Override
    public final void b() {
        final int r = this.r - 1;
        this.r = r;
        if (r != 0) {
            return;
        }
        if (this.n != -9223372036854775807L) {
            final ArrayList list = new ArrayList((Collection<? extends E>)this.o);
            for (int i = 0; i < list.size(); ++i) {
                ((com.applovin.exoplayer2.d.b)list.get(i)).b(null);
            }
        }
        this.d();
        this.e();
    }
    
    public static final class a
    {
        private final HashMap<String, String> a;
        private UUID b;
        private m.c c;
        private boolean d;
        private int[] e;
        private boolean f;
        private v g;
        private long h;
        
        public a() {
            this.a = new HashMap<String, String>();
            this.b = com.applovin.exoplayer2.h.d;
            this.c = o.a;
            this.g = (v)new com.applovin.exoplayer2.k.r();
            this.e = new int[0];
            this.h = 300000L;
        }
        
        public a a(final UUID uuid, final m.c c) {
            this.b = (UUID)com.applovin.exoplayer2.l.a.b((Object)uuid);
            this.c = (m.c)com.applovin.exoplayer2.l.a.b((Object)c);
            return this;
        }
        
        public a a(final boolean d) {
            this.d = d;
            return this;
        }
        
        public a a(final int... array) {
            for (final int n : array) {
                boolean b = true;
                if (n != 2) {
                    b = (n == 1 && b);
                }
                com.applovin.exoplayer2.l.a.a(b);
            }
            this.e = array.clone();
            return this;
        }
        
        public c a(final r r) {
            return new c(this.b, this.c, r, this.a, this.d, this.e, this.f, this.g, this.h, null);
        }
        
        public a b(final boolean f) {
            this.f = f;
            return this;
        }
    }
    
    private class b implements m.b
    {
        final c a;
        
        private b(final c a) {
            this.a = a;
        }
        
        @Override
        public void a(final m m, @Nullable final byte[] array, final int n, final int n2, @Nullable final byte[] array2) {
            ((c)com.applovin.exoplayer2.l.a.b((Object)this.a.a)).obtainMessage(n, (Object)array).sendToTarget();
        }
    }
    
    @SuppressLint({ "HandlerLeak" })
    private class c extends Handler
    {
        final com.applovin.exoplayer2.d.c a;
        
        public c(final com.applovin.exoplayer2.d.c a, final Looper looper) {
            this.a = a;
            super(looper);
        }
        
        public void handleMessage(final Message message) {
            final byte[] array = (byte[])message.obj;
            if (array == null) {
                return;
            }
            for (final com.applovin.exoplayer2.d.b b : this.a.o) {
                if (b.a(array)) {
                    b.a(message.what);
                    break;
                }
            }
        }
    }
    
    public static final class d extends Exception
    {
        private d(final UUID obj) {
            final StringBuilder sb = new StringBuilder();
            sb.append("Media does not support uuid: ");
            sb.append(obj);
            super(sb.toString());
        }
    }
    
    private class e implements h.a
    {
        final c a;
        @Nullable
        private final com.applovin.exoplayer2.d.g.a c;
        @Nullable
        private com.applovin.exoplayer2.d.f d;
        private boolean e;
        
        public e(@Nullable final c a, final com.applovin.exoplayer2.d.g.a c) {
            this.a = a;
            this.c = c;
        }
        
        public void a(final com.applovin.exoplayer2.v v) {
            ((Handler)com.applovin.exoplayer2.l.a.b((Object)this.a.w)).post((Runnable)new \u3007\u3007888(this, v));
        }
        
        @Override
        public void release() {
            ai.a((Handler)com.applovin.exoplayer2.l.a.b((Object)this.a.w), (Runnable)new o\u30070(this));
        }
    }
    
    private class f implements b.a
    {
        final c a;
        private final Set<b> b;
        @Nullable
        private b c;
        
        public f(final c a) {
            this.a = a;
            this.b = new HashSet<b>();
        }
        
        @Override
        public void a() {
            this.c = null;
            final s<Object> a = s.a((Collection<?>)this.b);
            this.b.clear();
            final ax<Object> a2 = a.a();
            while (a2.hasNext()) {
                a2.next().b();
            }
        }
        
        @Override
        public void a(final b c) {
            this.b.add(c);
            if (this.c != null) {
                return;
            }
            (this.c = c).a();
        }
        
        @Override
        public void a(final Exception ex, final boolean b) {
            this.c = null;
            final s<Object> a = s.a((Collection<?>)this.b);
            this.b.clear();
            final ax<Object> a2 = a.a();
            while (a2.hasNext()) {
                a2.next().a(ex, b);
            }
        }
        
        public void b(b c) {
            this.b.remove(c);
            if (this.c == c) {
                this.c = null;
                if (!this.b.isEmpty()) {
                    c = this.b.iterator().next();
                    (this.c = c).a();
                }
            }
        }
    }
    
    private class g implements b.b
    {
        final c a;
        
        private g(final c a) {
            this.a = a;
        }
        
        @Override
        public void a(final b b, final int n) {
            if (this.a.n != -9223372036854775807L) {
                this.a.q.remove(b);
                ((Handler)com.applovin.exoplayer2.l.a.b((Object)this.a.w)).removeCallbacksAndMessages((Object)b);
            }
        }
        
        @Override
        public void b(final b b, final int n) {
            if (n == 1 && this.a.r > 0 && this.a.n != -9223372036854775807L) {
                this.a.q.add(b);
                ((Handler)com.applovin.exoplayer2.l.a.b((Object)this.a.w)).postAtTime((Runnable)new oO80(b), (Object)b, SystemClock.uptimeMillis() + this.a.n);
            }
            else if (n == 0) {
                this.a.o.remove(b);
                if (this.a.t == b) {
                    this.a.t = null;
                }
                if (this.a.u == b) {
                    this.a.u = null;
                }
                this.a.k.b(b);
                if (this.a.n != -9223372036854775807L) {
                    ((Handler)com.applovin.exoplayer2.l.a.b((Object)this.a.w)).removeCallbacksAndMessages((Object)b);
                    this.a.q.remove(b);
                }
            }
            this.a.e();
        }
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.applovin.exoplayer2.d;

import com.applovin.exoplayer2.l.ai;
import java.util.UUID;
import com.applovin.exoplayer2.c.b;

public final class n implements b
{
    public static final boolean a;
    public final UUID b;
    public final byte[] c;
    public final boolean d;
    
    static {
        boolean a2 = false;
        Label_0040: {
            if ("Amazon".equals(ai.c)) {
                final String d = ai.d;
                if ("AFTM".equals(d) || "AFTB".equals(d)) {
                    a2 = true;
                    break Label_0040;
                }
            }
            a2 = false;
        }
        a = a2;
    }
    
    public n(final UUID b, final byte[] c, final boolean d) {
        this.b = b;
        this.c = c;
        this.d = d;
    }
}

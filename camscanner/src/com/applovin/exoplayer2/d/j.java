// 
// Decompiled by Procyon v0.6.0
// 

package com.applovin.exoplayer2.d;

import android.media.MediaDrmResetException;
import com.applovin.exoplayer2.h;
import android.media.MediaDrm$MediaDrmStateException;
import android.media.DeniedByServerException;
import android.media.NotProvisionedException;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import com.applovin.exoplayer2.l.ai;

public final class j
{
    public static int a(final Exception ex, final int n) {
        final int a = ai.a;
        if (a >= 21 && b.a(ex)) {
            return b.b(ex);
        }
        if (a >= 23 && c.a(ex)) {
            return 6006;
        }
        if (a >= 18 && j.a.a(ex)) {
            return 6002;
        }
        if (a >= 18 && j.a.b(ex)) {
            return 6007;
        }
        if (ex instanceof t) {
            return 6001;
        }
        if (ex instanceof com.applovin.exoplayer2.d.c.d) {
            return 6003;
        }
        if (ex instanceof q) {
            return 6008;
        }
        if (n == 1) {
            return 6006;
        }
        if (n == 2) {
            return 6004;
        }
        if (n == 3) {
            return 6002;
        }
        throw new IllegalArgumentException();
    }
    
    @RequiresApi(18)
    private static final class a
    {
        public static boolean a(@Nullable final Throwable t) {
            return t instanceof NotProvisionedException;
        }
        
        public static boolean b(@Nullable final Throwable t) {
            return t instanceof DeniedByServerException;
        }
    }
    
    @RequiresApi(21)
    private static final class b
    {
        public static boolean a(@Nullable final Throwable t) {
            return t instanceof MediaDrm$MediaDrmStateException;
        }
        
        public static int b(final Throwable t) {
            return h.b(ai.d(((MediaDrm$MediaDrmStateException)t).getDiagnosticInfo()));
        }
    }
    
    @RequiresApi(23)
    private static final class c
    {
        public static boolean a(@Nullable final Throwable t) {
            return t instanceof MediaDrmResetException;
        }
    }
}

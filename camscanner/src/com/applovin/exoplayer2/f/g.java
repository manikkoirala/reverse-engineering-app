// 
// Decompiled by Procyon v0.6.0
// 

package com.applovin.exoplayer2.f;

import java.io.IOException;
import android.media.MediaCrypto;
import com.applovin.exoplayer2.v;
import android.media.MediaFormat;
import android.os.Handler;
import android.view.Surface;
import android.os.Bundle;
import androidx.annotation.RequiresApi;
import com.applovin.exoplayer2.c.c;
import androidx.annotation.Nullable;
import java.nio.ByteBuffer;
import android.media.MediaCodec$BufferInfo;

public interface g
{
    int a(final MediaCodec$BufferInfo p0);
    
    @Nullable
    ByteBuffer a(final int p0);
    
    void a(final int p0, final int p1, final int p2, final long p3, final int p4);
    
    void a(final int p0, final int p1, final com.applovin.exoplayer2.c.c p2, final long p3, final int p4);
    
    @RequiresApi(21)
    void a(final int p0, final long p1);
    
    void a(final int p0, final boolean p1);
    
    @RequiresApi(19)
    void a(final Bundle p0);
    
    @RequiresApi(23)
    void a(final Surface p0);
    
    @RequiresApi(23)
    void a(final c p0, final Handler p1);
    
    boolean a();
    
    int b();
    
    @Nullable
    ByteBuffer b(final int p0);
    
    MediaFormat c();
    
    void c(final int p0);
    
    void d();
    
    void e();
    
    public static final class a
    {
        public final i a;
        public final MediaFormat b;
        public final v c;
        @Nullable
        public final Surface d;
        @Nullable
        public final MediaCrypto e;
        public final int f;
        public final boolean g;
        
        private a(final i a, final MediaFormat b, final v c, @Nullable final Surface d, @Nullable final MediaCrypto e, final int f, final boolean g) {
            this.a = a;
            this.b = b;
            this.c = c;
            this.d = d;
            this.e = e;
            this.f = f;
            this.g = g;
        }
        
        public static a a(final i i, final MediaFormat mediaFormat, final v v, @Nullable final MediaCrypto mediaCrypto) {
            return new a(i, mediaFormat, v, null, mediaCrypto, 0, false);
        }
        
        public static a a(final i i, final MediaFormat mediaFormat, final v v, @Nullable final Surface surface, @Nullable final MediaCrypto mediaCrypto) {
            return new a(i, mediaFormat, v, surface, mediaCrypto, 0, false);
        }
    }
    
    public interface b
    {
        public static final b a = new m.b();
        
        g b(final a p0) throws IOException;
    }
    
    public interface c
    {
        void a(final g p0, final long p1, final long p2);
    }
}

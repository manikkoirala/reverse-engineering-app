// 
// Decompiled by Procyon v0.6.0
// 

package com.applovin.exoplayer2.f;

import androidx.annotation.RequiresApi;
import android.media.MediaCodec$CodecException;
import com.applovin.exoplayer2.l.ai;
import androidx.annotation.Nullable;
import com.applovin.exoplayer2.c.f;

public class h extends f
{
    @Nullable
    public final i a;
    @Nullable
    public final String b;
    
    public h(final Throwable t, @Nullable final i a) {
        final StringBuilder sb = new StringBuilder();
        sb.append("Decoder failed: ");
        final String s = null;
        String a2;
        if (a == null) {
            a2 = null;
        }
        else {
            a2 = a.a;
        }
        sb.append(a2);
        super(sb.toString(), t);
        this.a = a;
        String a3 = s;
        if (ai.a >= 21) {
            a3 = a(t);
        }
        this.b = a3;
    }
    
    @Nullable
    @RequiresApi(21)
    private static String a(final Throwable t) {
        if (t instanceof MediaCodec$CodecException) {
            return ((MediaCodec$CodecException)t).getDiagnosticInfo();
        }
        return null;
    }
}

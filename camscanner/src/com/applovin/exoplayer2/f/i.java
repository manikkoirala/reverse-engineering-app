// 
// Decompiled by Procyon v0.6.0
// 

package com.applovin.exoplayer2.f;

import android.media.MediaCodecInfo$AudioCapabilities;
import android.util.Pair;
import com.applovin.exoplayer2.c.h;
import com.applovin.exoplayer2.v;
import android.media.MediaCodecInfo$CodecProfileLevel;
import androidx.annotation.RequiresApi;
import android.graphics.Point;
import android.media.MediaCodecInfo$VideoCapabilities;
import com.applovin.exoplayer2.l.q;
import com.applovin.exoplayer2.l.ai;
import androidx.annotation.VisibleForTesting;
import com.applovin.exoplayer2.l.u;
import com.applovin.exoplayer2.l.a;
import androidx.annotation.Nullable;
import android.media.MediaCodecInfo$CodecCapabilities;

public final class i
{
    public final String a;
    public final String b;
    public final String c;
    @Nullable
    public final MediaCodecInfo$CodecCapabilities d;
    public final boolean e;
    public final boolean f;
    public final boolean g;
    public final boolean h;
    public final boolean i;
    public final boolean j;
    private final boolean k;
    
    @VisibleForTesting
    i(final String s, final String b, final String c, @Nullable final MediaCodecInfo$CodecCapabilities d, final boolean h, final boolean i, final boolean j, final boolean e, final boolean f, final boolean g) {
        this.a = (String)com.applovin.exoplayer2.l.a.b((Object)s);
        this.b = b;
        this.c = c;
        this.d = d;
        this.h = h;
        this.i = i;
        this.j = j;
        this.e = e;
        this.f = f;
        this.g = g;
        this.k = u.b(b);
    }
    
    private static int a(final String str, final String anObject, final int i) {
        if (i <= 1) {
            if (ai.a < 26 || i <= 0) {
                if (!"audio/mpeg".equals(anObject) && !"audio/3gpp".equals(anObject) && !"audio/amr-wb".equals(anObject) && !"audio/mp4a-latm".equals(anObject) && !"audio/vorbis".equals(anObject) && !"audio/opus".equals(anObject) && !"audio/raw".equals(anObject) && !"audio/flac".equals(anObject) && !"audio/g711-alaw".equals(anObject) && !"audio/g711-mlaw".equals(anObject)) {
                    if (!"audio/gsm".equals(anObject)) {
                        int j;
                        if ("audio/ac3".equals(anObject)) {
                            j = 6;
                        }
                        else if ("audio/eac3".equals(anObject)) {
                            j = 16;
                        }
                        else {
                            j = 30;
                        }
                        final StringBuilder sb = new StringBuilder();
                        sb.append("AssumedMaxChannelAdjustment: ");
                        sb.append(str);
                        sb.append(", [");
                        sb.append(i);
                        sb.append(" to ");
                        sb.append(j);
                        sb.append("]");
                        q.c("MediaCodecInfo", sb.toString());
                        return j;
                    }
                }
            }
        }
        return i;
    }
    
    @RequiresApi(21)
    private static Point a(final MediaCodecInfo$VideoCapabilities mediaCodecInfo$VideoCapabilities, final int n, final int n2) {
        final int widthAlignment = mediaCodecInfo$VideoCapabilities.getWidthAlignment();
        final int heightAlignment = mediaCodecInfo$VideoCapabilities.getHeightAlignment();
        return new Point(ai.a(n, widthAlignment) * widthAlignment, ai.a(n2, heightAlignment) * heightAlignment);
    }
    
    public static i a(final String s, final String s2, final String s3, @Nullable final MediaCodecInfo$CodecCapabilities mediaCodecInfo$CodecCapabilities, final boolean b, final boolean b2, final boolean b3, final boolean b4, final boolean b5) {
        return new i(s, s2, s3, mediaCodecInfo$CodecCapabilities, b, b2, b3, !b4 && mediaCodecInfo$CodecCapabilities != null && a(mediaCodecInfo$CodecCapabilities) && !c(s), mediaCodecInfo$CodecCapabilities != null && c(mediaCodecInfo$CodecCapabilities), b5 || (mediaCodecInfo$CodecCapabilities != null && e(mediaCodecInfo$CodecCapabilities)));
    }
    
    private void a(final String str) {
        final StringBuilder sb = new StringBuilder();
        sb.append("NoSupport [");
        sb.append(str);
        sb.append("] [");
        sb.append(this.a);
        sb.append(", ");
        sb.append(this.b);
        sb.append("] [");
        sb.append(ai.e);
        sb.append("]");
        q.a("MediaCodecInfo", sb.toString());
    }
    
    private static boolean a(final MediaCodecInfo$CodecCapabilities mediaCodecInfo$CodecCapabilities) {
        return ai.a >= 19 && b(mediaCodecInfo$CodecCapabilities);
    }
    
    @RequiresApi(21)
    private static boolean a(final MediaCodecInfo$VideoCapabilities mediaCodecInfo$VideoCapabilities, int y, int x, final double a) {
        final Point a2 = a(mediaCodecInfo$VideoCapabilities, y, x);
        x = a2.x;
        y = a2.y;
        if (a != -1.0 && a >= 1.0) {
            return mediaCodecInfo$VideoCapabilities.areSizeAndRateSupported(x, y, Math.floor(a));
        }
        return mediaCodecInfo$VideoCapabilities.isSizeSupported(x, y);
    }
    
    private void b(final String str) {
        final StringBuilder sb = new StringBuilder();
        sb.append("AssumedSupport [");
        sb.append(str);
        sb.append("] [");
        sb.append(this.a);
        sb.append(", ");
        sb.append(this.b);
        sb.append("] [");
        sb.append(ai.e);
        sb.append("]");
        q.a("MediaCodecInfo", sb.toString());
    }
    
    @RequiresApi(19)
    private static boolean b(final MediaCodecInfo$CodecCapabilities mediaCodecInfo$CodecCapabilities) {
        return mediaCodecInfo$CodecCapabilities.isFeatureSupported("adaptive-playback");
    }
    
    private static boolean c(final MediaCodecInfo$CodecCapabilities mediaCodecInfo$CodecCapabilities) {
        return ai.a >= 21 && d(mediaCodecInfo$CodecCapabilities);
    }
    
    private static boolean c(final String s) {
        if (ai.a <= 22) {
            final String d = ai.d;
            if (("ODROID-XU3".equals(d) || "Nexus 10".equals(d)) && ("OMX.Exynos.AVC.Decoder".equals(s) || "OMX.Exynos.AVC.Decoder.secure".equals(s))) {
                return true;
            }
        }
        return false;
    }
    
    @RequiresApi(21)
    private static boolean d(final MediaCodecInfo$CodecCapabilities mediaCodecInfo$CodecCapabilities) {
        return mediaCodecInfo$CodecCapabilities.isFeatureSupported("tunneled-playback");
    }
    
    private static boolean d(final String anObject) {
        return ai.d.startsWith("SM-T230") && "OMX.MARVELL.VIDEO.HW.CODA7542DECODER".equals(anObject);
    }
    
    private static boolean e(final MediaCodecInfo$CodecCapabilities mediaCodecInfo$CodecCapabilities) {
        return ai.a >= 21 && f(mediaCodecInfo$CodecCapabilities);
    }
    
    private static boolean e(final String anObject) {
        return "audio/opus".equals(anObject);
    }
    
    @RequiresApi(21)
    private static boolean f(final MediaCodecInfo$CodecCapabilities mediaCodecInfo$CodecCapabilities) {
        return mediaCodecInfo$CodecCapabilities.isFeatureSupported("secure-playback");
    }
    
    private static final boolean f(final String anObject) {
        return !"OMX.MTK.VIDEO.DECODER.HEVC".equals(anObject) || !"mcv5a".equals(ai.b);
    }
    
    private static MediaCodecInfo$CodecProfileLevel[] g(@Nullable final MediaCodecInfo$CodecCapabilities mediaCodecInfo$CodecCapabilities) {
        int intValue = 0;
        Label_0032: {
            if (mediaCodecInfo$CodecCapabilities != null) {
                final MediaCodecInfo$VideoCapabilities videoCapabilities = mediaCodecInfo$CodecCapabilities.getVideoCapabilities();
                if (videoCapabilities != null) {
                    intValue = (int)videoCapabilities.getBitrateRange().getUpper();
                    break Label_0032;
                }
            }
            intValue = 0;
        }
        int level;
        if (intValue >= 180000000) {
            level = 1024;
        }
        else if (intValue >= 120000000) {
            level = 512;
        }
        else if (intValue >= 60000000) {
            level = 256;
        }
        else if (intValue >= 30000000) {
            level = 128;
        }
        else if (intValue >= 18000000) {
            level = 64;
        }
        else if (intValue >= 12000000) {
            level = 32;
        }
        else if (intValue >= 7200000) {
            level = 16;
        }
        else if (intValue >= 3600000) {
            level = 8;
        }
        else if (intValue >= 1800000) {
            level = 4;
        }
        else if (intValue >= 800000) {
            level = 2;
        }
        else {
            level = 1;
        }
        final MediaCodecInfo$CodecProfileLevel mediaCodecInfo$CodecProfileLevel = new MediaCodecInfo$CodecProfileLevel();
        mediaCodecInfo$CodecProfileLevel.profile = 1;
        mediaCodecInfo$CodecProfileLevel.level = level;
        return new MediaCodecInfo$CodecProfileLevel[] { mediaCodecInfo$CodecProfileLevel };
    }
    
    @Nullable
    @RequiresApi(21)
    public Point a(final int n, final int n2) {
        final MediaCodecInfo$CodecCapabilities d = this.d;
        if (d == null) {
            return null;
        }
        final MediaCodecInfo$VideoCapabilities videoCapabilities = d.getVideoCapabilities();
        if (videoCapabilities == null) {
            return null;
        }
        return a(videoCapabilities, n, n2);
    }
    
    public h a(final v v, final v v2) {
        int n;
        if (!ai.a((Object)v.l, (Object)v2.l)) {
            n = 8;
        }
        else {
            n = 0;
        }
        int n6;
        if (this.k) {
            int n2 = n;
            if (v.t != v2.t) {
                n2 = (n | 0x400);
            }
            int n3 = n2;
            Label_0092: {
                if (!this.e) {
                    if (v.q == v2.q) {
                        n3 = n2;
                        if (v.r == v2.r) {
                            break Label_0092;
                        }
                    }
                    n3 = (n2 | 0x200);
                }
            }
            int n4 = n3;
            if (!ai.a((Object)v.x, (Object)v2.x)) {
                n4 = (n3 | 0x800);
            }
            int n5 = n4;
            if (d(this.a)) {
                n5 = n4;
                if (!v.a(v2)) {
                    n5 = (n4 | 0x2);
                }
            }
            if ((n6 = n5) == 0) {
                final String a = this.a;
                int n7;
                if (v.a(v2)) {
                    n7 = 3;
                }
                else {
                    n7 = 2;
                }
                return new h(a, v, v2, n7, 0);
            }
        }
        else {
            int n8 = n;
            if (v.y != v2.y) {
                n8 = (n | 0x1000);
            }
            int n9 = n8;
            if (v.z != v2.z) {
                n9 = (n8 | 0x2000);
            }
            int n10 = n9;
            if (v.A != v2.A) {
                n10 = (n9 | 0x4000);
            }
            if (n10 == 0 && "audio/mp4a-latm".equals(this.b)) {
                final Pair<Integer, Integer> a2 = l.a(v);
                final Pair<Integer, Integer> a3 = l.a(v2);
                if (a2 != null && a3 != null) {
                    final int intValue = (int)a2.first;
                    final int intValue2 = (int)a3.first;
                    if (intValue == 42 && intValue2 == 42) {
                        return new h(this.a, v, v2, 3, 0);
                    }
                }
            }
            int n11 = n10;
            if (!v.a(v2)) {
                n11 = (n10 | 0x20);
            }
            int n12 = n11;
            if (e(this.b)) {
                n12 = (n11 | 0x2);
            }
            if ((n6 = n12) == 0) {
                return new h(this.a, v, v2, 1, 0);
            }
        }
        return new h(this.a, v, v2, 0, n6);
    }
    
    @RequiresApi(21)
    public boolean a(final int i) {
        final MediaCodecInfo$CodecCapabilities d = this.d;
        if (d == null) {
            this.a("sampleRate.caps");
            return false;
        }
        final MediaCodecInfo$AudioCapabilities audioCapabilities = d.getAudioCapabilities();
        if (audioCapabilities == null) {
            this.a("sampleRate.aCaps");
            return false;
        }
        if (!audioCapabilities.isSampleRateSupported(i)) {
            final StringBuilder sb = new StringBuilder();
            sb.append("sampleRate.support, ");
            sb.append(i);
            this.a(sb.toString());
            return false;
        }
        return true;
    }
    
    @RequiresApi(21)
    public boolean a(final int n, final int n2, final double n3) {
        final MediaCodecInfo$CodecCapabilities d = this.d;
        if (d == null) {
            this.a("sizeAndRate.caps");
            return false;
        }
        final MediaCodecInfo$VideoCapabilities videoCapabilities = d.getVideoCapabilities();
        if (videoCapabilities == null) {
            this.a("sizeAndRate.vCaps");
            return false;
        }
        if (!a(videoCapabilities, n, n2, n3)) {
            if (n >= n2 || !f(this.a) || !a(videoCapabilities, n2, n, n3)) {
                final StringBuilder sb = new StringBuilder();
                sb.append("sizeAndRate.support, ");
                sb.append(n);
                sb.append("x");
                sb.append(n2);
                sb.append("x");
                sb.append(n3);
                this.a(sb.toString());
                return false;
            }
            final StringBuilder sb2 = new StringBuilder();
            sb2.append("sizeAndRate.rotated, ");
            sb2.append(n);
            sb2.append("x");
            sb2.append(n2);
            sb2.append("x");
            sb2.append(n3);
            this.b(sb2.toString());
        }
        return true;
    }
    
    public boolean a(final v v) throws l.b {
        final boolean b = this.b(v);
        final boolean b2 = false;
        boolean b3 = false;
        if (!b) {
            return false;
        }
        if (this.k) {
            final int q = v.q;
            if (q > 0) {
                final int r = v.r;
                if (r > 0) {
                    if (ai.a >= 21) {
                        return this.a(q, r, v.s);
                    }
                    if (q * r <= l.b()) {
                        b3 = true;
                    }
                    if (!b3) {
                        final StringBuilder sb = new StringBuilder();
                        sb.append("legacyFrameSize, ");
                        sb.append(v.q);
                        sb.append("x");
                        sb.append(v.r);
                        this.a(sb.toString());
                    }
                    return b3;
                }
            }
            return true;
        }
        if (ai.a >= 21) {
            final int z = v.z;
            if (z != -1) {
                final boolean b4 = b2;
                if (!this.a(z)) {
                    return b4;
                }
            }
            final int y = v.y;
            if (y != -1) {
                final boolean b4 = b2;
                if (!this.b(y)) {
                    return b4;
                }
            }
        }
        return true;
    }
    
    public MediaCodecInfo$CodecProfileLevel[] a() {
        final MediaCodecInfo$CodecCapabilities d = this.d;
        MediaCodecInfo$CodecProfileLevel[] profileLevels;
        if (d == null || (profileLevels = d.profileLevels) == null) {
            profileLevels = new MediaCodecInfo$CodecProfileLevel[0];
        }
        return profileLevels;
    }
    
    public boolean b() {
        if (ai.a >= 29 && "video/x-vnd.on2.vp9".equals(this.b)) {
            final MediaCodecInfo$CodecProfileLevel[] a = this.a();
            for (int length = a.length, i = 0; i < length; ++i) {
                if (a[i].profile == 16384) {
                    return true;
                }
            }
        }
        return false;
    }
    
    @RequiresApi(21)
    public boolean b(final int i) {
        final MediaCodecInfo$CodecCapabilities d = this.d;
        if (d == null) {
            this.a("channelCount.caps");
            return false;
        }
        final MediaCodecInfo$AudioCapabilities audioCapabilities = d.getAudioCapabilities();
        if (audioCapabilities == null) {
            this.a("channelCount.aCaps");
            return false;
        }
        if (a(this.a, this.b, audioCapabilities.getMaxInputChannelCount()) < i) {
            final StringBuilder sb = new StringBuilder();
            sb.append("channelCount.support, ");
            sb.append(i);
            this.a(sb.toString());
            return false;
        }
        return true;
    }
    
    public boolean b(final v v) {
        final String i = v.i;
        if (i == null || this.b == null) {
            return true;
        }
        final String d = u.d(i);
        if (d == null) {
            return true;
        }
        if (!this.b.equals(d)) {
            final StringBuilder sb = new StringBuilder();
            sb.append("codec.mime ");
            sb.append(v.i);
            sb.append(", ");
            sb.append(d);
            this.a(sb.toString());
            return false;
        }
        final Pair<Integer, Integer> a = l.a(v);
        if (a == null) {
            return true;
        }
        final int intValue = (int)a.first;
        final int intValue2 = (int)a.second;
        if (!this.k && intValue != 42) {
            return true;
        }
        MediaCodecInfo$CodecProfileLevel[] array2;
        final MediaCodecInfo$CodecProfileLevel[] array = array2 = this.a();
        if (ai.a <= 23) {
            array2 = array;
            if ("video/x-vnd.on2.vp9".equals(this.b)) {
                array2 = array;
                if (array.length == 0) {
                    array2 = g(this.d);
                }
            }
        }
        for (final MediaCodecInfo$CodecProfileLevel mediaCodecInfo$CodecProfileLevel : array2) {
            if (mediaCodecInfo$CodecProfileLevel.profile == intValue && mediaCodecInfo$CodecProfileLevel.level >= intValue2) {
                return true;
            }
        }
        final StringBuilder sb2 = new StringBuilder();
        sb2.append("codec.profileLevel, ");
        sb2.append(v.i);
        sb2.append(", ");
        sb2.append(d);
        this.a(sb2.toString());
        return false;
    }
    
    public boolean c(final v v) {
        if (this.k) {
            return this.e;
        }
        final Pair<Integer, Integer> a = l.a(v);
        return a != null && (int)a.first == 42;
    }
    
    @Override
    public String toString() {
        return this.a;
    }
}

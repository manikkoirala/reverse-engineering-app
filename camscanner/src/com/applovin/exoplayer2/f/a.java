// 
// Decompiled by Procyon v0.6.0
// 

package com.applovin.exoplayer2.f;

import java.io.IOException;
import androidx.annotation.VisibleForTesting;
import com.applovin.exoplayer2.common.base.Supplier;
import java.util.Objects;
import android.media.MediaCodec$OnFrameRenderedListener;
import android.os.Handler;
import android.os.Bundle;
import java.nio.ByteBuffer;
import android.media.MediaCodec$BufferInfo;
import com.applovin.exoplayer2.l.ah;
import android.media.MediaCrypto;
import android.media.MediaFormat;
import android.os.HandlerThread;
import androidx.annotation.Nullable;
import android.view.Surface;
import android.media.MediaCodec;
import androidx.annotation.RequiresApi;

@RequiresApi(23)
final class a implements g
{
    private final MediaCodec a;
    private final com.applovin.exoplayer2.f.c b;
    private final com.applovin.exoplayer2.f.b c;
    private final boolean d;
    private boolean e;
    private int f;
    @Nullable
    private Surface g;
    
    private a(final MediaCodec a, final HandlerThread handlerThread, final HandlerThread handlerThread2, final boolean b, final boolean d) {
        this.a = a;
        this.b = new com.applovin.exoplayer2.f.c(handlerThread);
        this.c = new com.applovin.exoplayer2.f.b(a, handlerThread2, b);
        this.d = d;
        this.f = 0;
    }
    
    private static String a(final int i, final String str) {
        final StringBuilder sb = new StringBuilder(str);
        if (i == 1) {
            sb.append("Audio");
        }
        else if (i == 2) {
            sb.append("Video");
        }
        else {
            sb.append("Unknown(");
            sb.append(i);
            sb.append(")");
        }
        return sb.toString();
    }
    
    private void a(@Nullable final MediaFormat mediaFormat, @Nullable final Surface surface, @Nullable final MediaCrypto mediaCrypto, final int n, final boolean b) {
        this.b.a(this.a);
        ah.a("configureCodec");
        this.a.configure(mediaFormat, surface, mediaCrypto, n);
        ah.a();
        if (b) {
            this.g = this.a.createInputSurface();
        }
        this.c.a();
        ah.a("startCodec");
        this.a.start();
        ah.a();
        this.f = 1;
    }
    
    private static String f(final int n) {
        return a(n, "ExoPlayer:MediaCodecAsyncAdapter:");
    }
    
    private void f() {
        if (this.d) {
            try {
                this.c.d();
            }
            catch (final InterruptedException cause) {
                Thread.currentThread().interrupt();
                throw new IllegalStateException(cause);
            }
        }
    }
    
    private static String g(final int n) {
        return a(n, "ExoPlayer:MediaCodecQueueingThread:");
    }
    
    @Override
    public int a(final MediaCodec$BufferInfo mediaCodec$BufferInfo) {
        return this.b.a(mediaCodec$BufferInfo);
    }
    
    @Nullable
    @Override
    public ByteBuffer a(final int n) {
        return this.a.getInputBuffer(n);
    }
    
    @Override
    public void a(final int n, final int n2, final int n3, final long n4, final int n5) {
        this.c.a(n, n2, n3, n4, n5);
    }
    
    @Override
    public void a(final int n, final int n2, final com.applovin.exoplayer2.c.c c, final long n3, final int n4) {
        this.c.a(n, n2, c, n3, n4);
    }
    
    @Override
    public void a(final int n, final long n2) {
        this.a.releaseOutputBuffer(n, n2);
    }
    
    @Override
    public void a(final int n, final boolean b) {
        this.a.releaseOutputBuffer(n, b);
    }
    
    @Override
    public void a(final Bundle parameters) {
        this.f();
        this.a.setParameters(parameters);
    }
    
    @Override
    public void a(final Surface surface) {
        this.f();
        \u3007o00\u3007\u3007Oo.\u3007080(this.a, surface);
    }
    
    @Override
    public void a(final c c, final Handler handler) {
        this.f();
        \u3007080.\u3007080(this.a, (MediaCodec$OnFrameRenderedListener)new \u3007o\u3007(this, c), handler);
    }
    
    @Override
    public boolean a() {
        return false;
    }
    
    @Override
    public int b() {
        return this.b.b();
    }
    
    @Nullable
    @Override
    public ByteBuffer b(final int n) {
        return this.a.getOutputBuffer(n);
    }
    
    @Override
    public MediaFormat c() {
        return this.b.c();
    }
    
    @Override
    public void c(final int videoScalingMode) {
        this.f();
        this.a.setVideoScalingMode(videoScalingMode);
    }
    
    @Override
    public void d() {
        this.c.b();
        this.a.flush();
        final com.applovin.exoplayer2.f.c b = this.b;
        final MediaCodec a = this.a;
        Objects.requireNonNull(a);
        b.a(new O8(a));
    }
    
    @Override
    public void e() {
        try {
            if (this.f == 1) {
                this.c.c();
                this.b.a();
            }
            this.f = 2;
        }
        finally {
            final Surface g = this.g;
            if (g != null) {
                g.release();
            }
            if (!this.e) {
                this.a.release();
                this.e = true;
            }
        }
    }
    
    public static final class a implements b
    {
        private final Supplier<HandlerThread> b;
        private final Supplier<HandlerThread> c;
        private final boolean d;
        private final boolean e;
        
        public a(final int n, final boolean b, final boolean b2) {
            this(new Oo08(n), new o\u30070(n), b, b2);
        }
        
        @VisibleForTesting
        a(final Supplier<HandlerThread> b, final Supplier<HandlerThread> c, final boolean d, final boolean e) {
            this.b = b;
            this.c = c;
            this.d = d;
            this.e = e;
        }
        
        public com.applovin.exoplayer2.f.a a(final g.a a) throws IOException {
            final String a2 = a.a.a;
            com.applovin.exoplayer2.f.a a3 = null;
            MediaCodec byCodecName;
            try {
                final StringBuilder sb = new StringBuilder();
                sb.append("createCodec:");
                sb.append(a2);
                ah.a(sb.toString());
                byCodecName = MediaCodec.createByCodecName(a2);
                try {
                    final com.applovin.exoplayer2.f.a a4 = new com.applovin.exoplayer2.f.a(byCodecName, this.b.get(), this.c.get(), this.d, this.e, null);
                    try {
                        ah.a();
                        a4.a(a.b, a.d, a.e, a.f, a.g);
                        return a4;
                    }
                    catch (final Exception ex) {
                        a3 = a4;
                    }
                }
                catch (final Exception ex) {}
            }
            catch (final Exception ex) {
                byCodecName = null;
            }
            if (a3 == null) {
                if (byCodecName != null) {
                    byCodecName.release();
                }
            }
            else {
                a3.e();
            }
            throw;
        }
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.applovin.exoplayer2.f;

import java.io.IOException;
import com.applovin.exoplayer2.l.ah;
import com.applovin.exoplayer2.l.a;
import android.media.MediaFormat;
import android.media.MediaCodec$OnFrameRenderedListener;
import android.os.Handler;
import android.os.Bundle;
import androidx.annotation.RequiresApi;
import com.applovin.exoplayer2.c.c;
import android.media.MediaCodec$BufferInfo;
import com.applovin.exoplayer2.l.ai;
import java.nio.ByteBuffer;
import androidx.annotation.Nullable;
import android.view.Surface;
import android.media.MediaCodec;

public class m implements g
{
    private final MediaCodec a;
    @Nullable
    private final Surface b;
    @Nullable
    private ByteBuffer[] c;
    @Nullable
    private ByteBuffer[] d;
    
    private m(final MediaCodec a, @Nullable final Surface b) {
        this.a = a;
        this.b = b;
        if (ai.a < 21) {
            this.c = a.getInputBuffers();
            this.d = a.getOutputBuffers();
        }
    }
    
    @Override
    public int a(final MediaCodec$BufferInfo mediaCodec$BufferInfo) {
        int i;
        do {
            i = this.a.dequeueOutputBuffer(mediaCodec$BufferInfo, 0L);
            if (i == -3 && ai.a < 21) {
                this.d = this.a.getOutputBuffers();
            }
        } while (i == -3);
        return i;
    }
    
    @Nullable
    @Override
    public ByteBuffer a(final int n) {
        if (ai.a >= 21) {
            return this.a.getInputBuffer(n);
        }
        return ((ByteBuffer[])ai.a((Object)this.c))[n];
    }
    
    @Override
    public void a(final int n, final int n2, final int n3, final long n4, final int n5) {
        this.a.queueInputBuffer(n, n2, n3, n4, n5);
    }
    
    @Override
    public void a(final int n, final int n2, final com.applovin.exoplayer2.c.c c, final long n3, final int n4) {
        this.a.queueSecureInputBuffer(n, n2, c.a(), n3, n4);
    }
    
    @RequiresApi(21)
    @Override
    public void a(final int n, final long n2) {
        this.a.releaseOutputBuffer(n, n2);
    }
    
    @Override
    public void a(final int n, final boolean b) {
        this.a.releaseOutputBuffer(n, b);
    }
    
    @RequiresApi(19)
    @Override
    public void a(final Bundle parameters) {
        this.a.setParameters(parameters);
    }
    
    @RequiresApi(23)
    @Override
    public void a(final Surface surface) {
        \u3007o00\u3007\u3007Oo.\u3007080(this.a, surface);
    }
    
    @RequiresApi(23)
    @Override
    public void a(final c c, final Handler handler) {
        \u3007080.\u3007080(this.a, (MediaCodec$OnFrameRenderedListener)new OoO8(this, c), handler);
    }
    
    @Override
    public boolean a() {
        return false;
    }
    
    @Override
    public int b() {
        return this.a.dequeueInputBuffer(0L);
    }
    
    @Nullable
    @Override
    public ByteBuffer b(final int n) {
        if (ai.a >= 21) {
            return this.a.getOutputBuffer(n);
        }
        return ((ByteBuffer[])ai.a((Object)this.d))[n];
    }
    
    @Override
    public MediaFormat c() {
        return this.a.getOutputFormat();
    }
    
    @Override
    public void c(final int videoScalingMode) {
        this.a.setVideoScalingMode(videoScalingMode);
    }
    
    @Override
    public void d() {
        this.a.flush();
    }
    
    @Override
    public void e() {
        this.c = null;
        this.d = null;
        final Surface b = this.b;
        if (b != null) {
            b.release();
        }
        this.a.release();
    }
    
    @RequiresApi(18)
    private static final class a
    {
        public static Surface a(final MediaCodec mediaCodec) {
            return mediaCodec.createInputSurface();
        }
    }
    
    public static class b implements g.b
    {
        protected MediaCodec a(final g.a a) throws IOException {
            a.b((Object)a.a);
            final String a2 = a.a.a;
            final StringBuilder sb = new StringBuilder();
            sb.append("createCodec:");
            sb.append(a2);
            ah.a(sb.toString());
            final MediaCodec byCodecName = MediaCodec.createByCodecName(a2);
            ah.a();
            return byCodecName;
        }
        
        @RequiresApi(16)
        @Override
        public g b(g.a a) throws IOException {
            Object o = null;
            MediaCodec mediaCodec = null;
            Label_0139: {
                try {
                    final MediaCodec a2 = this.a((g.a)a);
                    try {
                        ah.a("configureCodec");
                        a2.configure(((g.a)a).b, ((g.a)a).d, ((g.a)a).e, ((g.a)a).f);
                        ah.a();
                        if (((g.a)a).g) {
                            if (ai.a < 18) {
                                throw new IllegalStateException("Encoding from a surface is only supported on API 18 and up.");
                            }
                            a = (RuntimeException)m.a.a(a2);
                        }
                        else {
                            a = null;
                        }
                        g g = null;
                        try {
                            ah.a("startCodec");
                            a2.start();
                            ah.a();
                            g = new m(a2, (Surface)a, null);
                            return g;
                        }
                        catch (final RuntimeException g) {}
                        catch (final IOException ex) {}
                        o = a;
                        a = (RuntimeException)g;
                        mediaCodec = a2;
                    }
                    catch (final RuntimeException a) {
                        mediaCodec = a2;
                    }
                    catch (final IOException a) {
                        mediaCodec = a2;
                    }
                    break Label_0139;
                }
                catch (final RuntimeException a) {}
                catch (final IOException ex2) {}
                mediaCodec = null;
            }
            if (o != null) {
                ((Surface)o).release();
            }
            if (mediaCodec != null) {
                mediaCodec.release();
            }
            throw a;
        }
    }
}

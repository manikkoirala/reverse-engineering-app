// 
// Decompiled by Procyon v0.6.0
// 

package com.applovin.exoplayer2.f;

import androidx.annotation.CheckResult;
import androidx.annotation.CallSuper;
import android.os.Bundle;
import com.applovin.exoplayer2.c.b;
import android.media.MediaCodec$CodecException;
import androidx.annotation.RequiresApi;
import android.media.MediaCryptoException;
import com.applovin.exoplayer2.d.n;
import java.util.UUID;
import com.applovin.exoplayer2.l.ah;
import android.os.SystemClock;
import com.applovin.exoplayer2.l.ai;
import com.applovin.exoplayer2.d.\u300780\u3007808\u3007O;
import java.util.List;
import com.applovin.exoplayer2.l.q;
import java.util.Collection;
import android.annotation.TargetApi;
import com.applovin.exoplayer2.w;
import android.media.MediaCodec$CryptoException;
import com.applovin.exoplayer2.h;
import java.nio.ByteOrder;
import com.applovin.exoplayer2.l.a;
import android.media.MediaCrypto;
import com.applovin.exoplayer2.d.f;
import android.media.MediaCodec$BufferInfo;
import java.util.ArrayList;
import com.applovin.exoplayer2.l.af;
import com.applovin.exoplayer2.p;
import java.nio.ByteBuffer;
import java.util.ArrayDeque;
import android.media.MediaFormat;
import androidx.annotation.Nullable;
import com.applovin.exoplayer2.v;
import com.applovin.exoplayer2.e;

public abstract class j extends e
{
    private static final byte[] b;
    @Nullable
    private v A;
    @Nullable
    private MediaFormat B;
    private boolean C;
    private float D;
    @Nullable
    private ArrayDeque<i> E;
    @Nullable
    private a F;
    @Nullable
    private i G;
    private int H;
    private boolean I;
    private boolean J;
    private boolean K;
    private boolean L;
    private boolean M;
    private boolean N;
    private boolean O;
    private boolean P;
    private boolean Q;
    private boolean R;
    @Nullable
    private com.applovin.exoplayer2.f.e S;
    private long T;
    private int U;
    private int V;
    @Nullable
    private ByteBuffer W;
    private boolean X;
    private boolean Y;
    private boolean Z;
    protected com.applovin.exoplayer2.c.e a;
    private boolean aa;
    private boolean ab;
    private boolean ac;
    private int ad;
    private int ae;
    private int af;
    private boolean ag;
    private boolean ah;
    private boolean ai;
    private long aj;
    private long ak;
    private boolean al;
    private boolean am;
    private boolean an;
    private boolean ao;
    private boolean ap;
    private boolean aq;
    private boolean ar;
    @Nullable
    private p as;
    private long at;
    private long au;
    private int av;
    private final g.b c;
    private final k d;
    private final boolean e;
    private final float f;
    private final com.applovin.exoplayer2.c.g g;
    private final com.applovin.exoplayer2.c.g h;
    private final com.applovin.exoplayer2.c.g i;
    private final d j;
    private final af<v> k;
    private final ArrayList<Long> l;
    private final MediaCodec$BufferInfo m;
    private final long[] n;
    private final long[] o;
    private final long[] p;
    @Nullable
    private v q;
    @Nullable
    private v r;
    @Nullable
    private f s;
    @Nullable
    private f t;
    @Nullable
    private MediaCrypto u;
    private boolean v;
    private long w;
    private float x;
    private float y;
    @Nullable
    private g z;
    
    static {
        b = new byte[] { 0, 0, 1, 103, 66, -64, 11, -38, 37, -112, 0, 0, 1, 104, -50, 15, 19, 32, 0, 0, 1, 101, -120, -124, 13, -50, 113, 24, -96, 0, 47, -65, 28, 49, -61, 39, 93, 120 };
    }
    
    public j(final int n, final g.b c, final k k, final boolean e, final float f) {
        super(n);
        this.c = c;
        this.d = (k)com.applovin.exoplayer2.l.a.b((Object)k);
        this.e = e;
        this.f = f;
        this.g = com.applovin.exoplayer2.c.g.f();
        this.h = new com.applovin.exoplayer2.c.g(0);
        this.i = new com.applovin.exoplayer2.c.g(2);
        final d j = new d();
        this.j = j;
        this.k = (af<v>)new af();
        this.l = new ArrayList<Long>();
        this.m = new MediaCodec$BufferInfo();
        this.x = 1.0f;
        this.y = 1.0f;
        this.w = -9223372036854775807L;
        this.n = new long[10];
        this.o = new long[10];
        this.p = new long[10];
        this.at = -9223372036854775807L;
        this.au = -9223372036854775807L;
        j.f(0);
        j.b.order(ByteOrder.nativeOrder());
        this.D = -1.0f;
        this.H = 0;
        this.ad = 0;
        this.U = -1;
        this.V = -1;
        this.T = -9223372036854775807L;
        this.aj = -9223372036854775807L;
        this.ak = -9223372036854775807L;
        this.ae = 0;
        this.af = 0;
    }
    
    private void B() {
        this.ab = false;
        this.j.a();
        this.i.a();
        this.aa = false;
        this.Z = false;
    }
    
    private void R() {
        try {
            this.z.d();
        }
        finally {
            this.M();
        }
    }
    
    private boolean S() {
        return this.V >= 0;
    }
    
    private void T() {
        this.U = -1;
        this.h.b = null;
    }
    
    private void U() {
        this.V = -1;
        this.W = null;
    }
    
    private boolean V() throws p {
        final g z = this.z;
        if (z != null && this.ae != 2) {
            if (!this.al) {
                if (this.U < 0) {
                    final int b = z.b();
                    if ((this.U = b) < 0) {
                        return false;
                    }
                    this.h.b = this.z.a(b);
                    this.h.a();
                }
                if (this.ae == 1) {
                    if (!this.R) {
                        this.ah = true;
                        this.z.a(this.U, 0, 0, 0L, 4);
                        this.T();
                    }
                    this.ae = 2;
                    return false;
                }
                if (this.P) {
                    this.P = false;
                    final ByteBuffer b2 = this.h.b;
                    final byte[] b3 = com.applovin.exoplayer2.f.j.b;
                    b2.put(b3);
                    this.z.a(this.U, 0, b3.length, 0L, 0);
                    this.T();
                    return this.ag = true;
                }
                if (this.ad == 1) {
                    for (int i = 0; i < this.A.n.size(); ++i) {
                        this.h.b.put(this.A.n.get(i));
                    }
                    this.ad = 2;
                }
                final int position = this.h.b.position();
                final w t = this.t();
                try {
                    final int a = this.a(t, this.h, 0);
                    if (this.g()) {
                        this.ak = this.aj;
                    }
                    if (a == -3) {
                        return false;
                    }
                    if (a == -5) {
                        if (this.ad == 2) {
                            this.h.a();
                            this.ad = 1;
                        }
                        this.a(t);
                        return true;
                    }
                    if (this.h.c()) {
                        if (this.ad == 2) {
                            this.h.a();
                            this.ad = 1;
                        }
                        this.al = true;
                        if (!this.ag) {
                            this.aa();
                            return false;
                        }
                        try {
                            if (!this.R) {
                                this.ah = true;
                                this.z.a(this.U, 0, 0, 0L, 4);
                                this.T();
                            }
                            return false;
                        }
                        catch (final MediaCodec$CryptoException ex) {
                            throw this.a((Throwable)ex, this.q, com.applovin.exoplayer2.h.b(ex.getErrorCode()));
                        }
                    }
                    if (!this.ag && !this.h.d()) {
                        this.h.a();
                        if (this.ad == 2) {
                            this.ad = 1;
                        }
                        return true;
                    }
                    final boolean g = this.h.g();
                    if (g) {
                        this.h.a.a(position);
                    }
                    if (this.I && !g) {
                        com.applovin.exoplayer2.l.v.a(this.h.b);
                        if (this.h.b.position() == 0) {
                            return true;
                        }
                        this.I = false;
                    }
                    final com.applovin.exoplayer2.c.g h = this.h;
                    long n = h.d;
                    final com.applovin.exoplayer2.f.e s = this.S;
                    if (s != null) {
                        n = s.a(this.q, h);
                        this.aj = Math.max(this.aj, this.S.a(this.q));
                    }
                    if (this.h.b()) {
                        this.l.add(n);
                    }
                    if (this.an) {
                        this.k.a(n, (Object)this.q);
                        this.an = false;
                    }
                    this.aj = Math.max(this.aj, n);
                    this.h.h();
                    if (this.h.e()) {
                        this.b(this.h);
                    }
                    this.a(this.h);
                    Label_0741: {
                        if (!g) {
                            break Label_0741;
                        }
                        try {
                            this.z.a(this.U, 0, this.h.a, n, 0);
                            this.T();
                            this.ag = true;
                            this.ad = 0;
                            final com.applovin.exoplayer2.c.e a2 = this.a;
                            ++a2.c;
                            return true;
                            this.z.a(this.U, 0, this.h.b.limit(), n, 0);
                        }
                        catch (final MediaCodec$CryptoException ex2) {
                            throw this.a((Throwable)ex2, this.q, com.applovin.exoplayer2.h.b(ex2.getErrorCode()));
                        }
                    }
                }
                catch (final com.applovin.exoplayer2.c.g.a a3) {
                    this.a((Exception)a3);
                    this.e(0);
                    this.R();
                    return true;
                }
            }
        }
        return false;
    }
    
    private boolean W() {
        if (this.ag) {
            this.ae = 1;
            if (this.J || this.L) {
                this.af = 3;
                return false;
            }
            this.af = 1;
        }
        return true;
    }
    
    @TargetApi(23)
    private boolean X() throws p {
        if (this.ag) {
            this.ae = 1;
            if (this.J || this.L) {
                this.af = 3;
                return false;
            }
            this.af = 2;
        }
        else {
            this.ac();
        }
        return true;
    }
    
    private void Y() throws p {
        if (this.ag) {
            this.ae = 1;
            this.af = 3;
        }
        else {
            this.ab();
        }
    }
    
    private void Z() {
        this.ai = true;
        final MediaFormat c = this.z.c();
        if (this.H != 0 && c.getInteger("width") == 32 && c.getInteger("height") == 32) {
            this.Q = true;
            return;
        }
        if (this.O) {
            c.setInteger("channel-count", 1);
        }
        this.B = c;
        this.C = true;
    }
    
    private void a(final MediaCrypto mediaCrypto, final boolean b) throws a {
        if (this.E == null) {
            try {
                final List<i> d = this.d(b);
                final ArrayDeque<Object> e = new ArrayDeque<Object>();
                this.E = (ArrayDeque<i>)e;
                if (this.e) {
                    e.addAll(d);
                }
                else if (!d.isEmpty()) {
                    this.E.add((i)d.get(0));
                }
                this.F = null;
            }
            catch (final l.b b2) {
                throw new a(this.q, b2, b, -49998);
            }
        }
        if (!this.E.isEmpty()) {
            while (this.z == null) {
                final i obj = this.E.peekFirst();
                if (!this.a(obj)) {
                    return;
                }
                try {
                    this.a(obj, mediaCrypto);
                    continue;
                }
                catch (final Exception ex) {
                    final StringBuilder sb = new StringBuilder();
                    sb.append("Failed to initialize decoder: ");
                    sb.append(obj);
                    com.applovin.exoplayer2.l.q.b("MediaCodecRenderer", sb.toString(), (Throwable)ex);
                    this.E.removeFirst();
                    final a f = new a(this.q, ex, b, obj);
                    this.a(f);
                    if (this.F == null) {
                        this.F = f;
                    }
                    else {
                        this.F = this.F.a(f);
                    }
                    if (!this.E.isEmpty()) {
                        continue;
                    }
                    throw this.F;
                }
                break;
            }
            this.E = null;
            return;
        }
        throw new a(this.q, null, b, -49999);
    }
    
    private void a(@Nullable final f t) {
        \u300780\u3007808\u3007O.\u3007o00\u3007\u3007Oo(this.t, t);
        this.t = t;
    }
    
    private void a(final i g, final MediaCrypto mediaCrypto) throws Exception {
        final String a = g.a;
        final int a2 = com.applovin.exoplayer2.l.ai.a;
        final float n = -1.0f;
        float a3;
        if (a2 < 23) {
            a3 = -1.0f;
        }
        else {
            a3 = this.a(this.y, this.q, this.u());
        }
        if (a3 <= this.f) {
            a3 = n;
        }
        final long elapsedRealtime = SystemClock.elapsedRealtime();
        final StringBuilder sb = new StringBuilder();
        sb.append("createCodec:");
        sb.append(a);
        com.applovin.exoplayer2.l.ah.a(sb.toString());
        final g.a a4 = this.a(g, this.q, mediaCrypto, a3);
        g z;
        if (this.ap && a2 >= 23) {
            z = new com.applovin.exoplayer2.f.a.a(this.a(), this.aq, this.ar).a(a4);
        }
        else {
            z = this.c.b(a4);
        }
        final long elapsedRealtime2 = SystemClock.elapsedRealtime();
        this.z = z;
        this.G = g;
        this.D = a3;
        this.A = this.q;
        this.H = this.c(a);
        this.I = a(a, this.A);
        this.J = b(a);
        this.K = d(a);
        this.L = e(a);
        this.M = g(a);
        this.N = f(a);
        this.O = b(a, this.A);
        final boolean b = b(g);
        final boolean b2 = false;
        this.R = (b || this.F());
        if (z.a()) {
            this.ac = true;
            this.ad = 1;
            boolean p2 = b2;
            if (this.H != 0) {
                p2 = true;
            }
            this.P = p2;
        }
        if ("c2.android.mp3.decoder".equals(g.a)) {
            this.S = new com.applovin.exoplayer2.f.e();
        }
        if (this.d_() == 2) {
            this.T = SystemClock.elapsedRealtime() + 1000L;
        }
        final com.applovin.exoplayer2.c.e a5 = this.a;
        ++a5.a;
        this.a(a, elapsedRealtime2, elapsedRealtime2 - elapsedRealtime);
    }
    
    private boolean a(final i i, final v v, @Nullable final f f, @Nullable final f f2) throws p {
        if (f == f2) {
            return false;
        }
        if (f2 != null) {
            if (f != null) {
                if (com.applovin.exoplayer2.l.ai.a < 23) {
                    return true;
                }
                final UUID e = com.applovin.exoplayer2.h.e;
                if (!e.equals(f.f())) {
                    if (!e.equals(f2.f())) {
                        final n c = this.c(f2);
                        if (c == null) {
                            return true;
                        }
                        final boolean b = !c.d && f2.a(v.l);
                        return !i.g && b;
                    }
                }
            }
        }
        return true;
    }
    
    private static boolean a(final IllegalStateException ex) {
        final int a = ai.a;
        boolean b = true;
        if (a >= 21 && b(ex)) {
            return true;
        }
        final StackTraceElement[] stackTrace = ex.getStackTrace();
        if (stackTrace.length <= 0 || !stackTrace[0].getClassName().equals("android.media.MediaCodec")) {
            b = false;
        }
        return b;
    }
    
    private static boolean a(final String anObject, final v v) {
        return ai.a < 21 && v.n.isEmpty() && "OMX.MTK.VIDEO.DECODER.AVC".equals(anObject);
    }
    
    @TargetApi(23)
    private void aa() throws p {
        final int af = this.af;
        if (af != 1) {
            if (af != 2) {
                if (af != 3) {
                    this.am = true;
                    this.D();
                }
                else {
                    this.ab();
                }
            }
            else {
                this.R();
                this.ac();
            }
        }
        else {
            this.R();
        }
    }
    
    private void ab() throws p {
        this.J();
        this.E();
    }
    
    @RequiresApi(23)
    private void ac() throws p {
        try {
            com.applovin.exoplayer2.f.\u300780\u3007808\u3007O.\u3007080(this.u, this.c(this.t).c);
            this.b(this.t);
            this.ae = 0;
            this.af = 0;
        }
        catch (final MediaCryptoException ex) {
            throw this.a((Throwable)ex, this.q, 6006);
        }
    }
    
    private void ad() throws p {
        com.applovin.exoplayer2.l.a.b(this.al ^ true);
        final w t = this.t();
        this.i.a();
        do {
            this.i.a();
            final int a = this.a(t, this.i, 0);
            if (a == -5) {
                this.a(t);
                return;
            }
            if (a != -4) {
                if (a == -3) {
                    return;
                }
                throw new IllegalStateException();
            }
            else {
                if (this.i.c()) {
                    this.al = true;
                    return;
                }
                if (this.an) {
                    this.a(this.r = (v)com.applovin.exoplayer2.l.a.b((Object)this.q), null);
                    this.an = false;
                }
                this.i.h();
            }
        } while (this.j.a(this.i));
        this.aa = true;
    }
    
    private void b(@Nullable final f s) {
        \u300780\u3007808\u3007O.\u3007o00\u3007\u3007Oo(this.s, s);
        this.s = s;
    }
    
    private boolean b(final long n, final long n2) throws p {
        if (!this.S()) {
            int v = 0;
            Label_0073: {
                if (this.M && this.ah) {
                    try {
                        v = this.z.a(this.m);
                        break Label_0073;
                    }
                    catch (final IllegalStateException ex) {
                        this.aa();
                        if (this.am) {
                            this.J();
                        }
                        return false;
                    }
                }
                v = this.z.a(this.m);
            }
            if (v < 0) {
                if (v == -2) {
                    this.Z();
                    return true;
                }
                if (this.R && (this.al || this.ae == 2)) {
                    this.aa();
                }
                return false;
            }
            else {
                if (this.Q) {
                    this.Q = false;
                    this.z.a(v, false);
                    return true;
                }
                final MediaCodec$BufferInfo m = this.m;
                if (m.size == 0 && (m.flags & 0x4) != 0x0) {
                    this.aa();
                    return false;
                }
                this.V = v;
                final ByteBuffer b = this.z.b(v);
                if ((this.W = b) != null) {
                    b.position(this.m.offset);
                    final ByteBuffer w = this.W;
                    final MediaCodec$BufferInfo i = this.m;
                    w.limit(i.offset + i.size);
                }
                if (this.N) {
                    final MediaCodec$BufferInfo j = this.m;
                    if (j.presentationTimeUs == 0L && (j.flags & 0x4) != 0x0) {
                        final long aj = this.aj;
                        if (aj != -9223372036854775807L) {
                            j.presentationTimeUs = aj;
                        }
                    }
                }
                this.X = this.f(this.m.presentationTimeUs);
                final long ak = this.ak;
                final long presentationTimeUs = this.m.presentationTimeUs;
                this.Y = (ak == presentationTimeUs);
                this.c(presentationTimeUs);
            }
        }
        Label_0478: {
            if (!this.M || !this.ah) {
                break Label_0478;
            }
        Label_0461_Outer:
            while (true) {
                g z = null;
                ByteBuffer w2 = null;
                int v2 = 0;
                int flags = 0;
                long presentationTimeUs2 = 0L;
                boolean x = false;
                boolean y = false;
                v r = null;
                try {
                    z = this.z;
                    w2 = this.W;
                    v2 = this.V;
                    final MediaCodec$BufferInfo k = this.m;
                    flags = k.flags;
                    presentationTimeUs2 = k.presentationTimeUs;
                    x = this.X;
                    y = this.Y;
                    r = this.r;
                    final j l = this;
                    final long n3 = n;
                    final long n4 = n2;
                    final g g = z;
                    final ByteBuffer byteBuffer = w2;
                    final int n5 = v2;
                    final int n6 = flags;
                    final int n7 = 1;
                    final long n8 = presentationTimeUs2;
                    final boolean b2 = x;
                    final boolean b3 = y;
                    final v v3 = r;
                    final boolean b4 = l.a(n3, n4, g, byteBuffer, n5, n6, n7, n8, b2, b3, v3);
                    break Label_0539;
                }
                catch (final IllegalStateException ex2) {}
                while (true) {
                    try {
                        final j l = this;
                        final long n3 = n;
                        final long n4 = n2;
                        final g g = z;
                        final ByteBuffer byteBuffer = w2;
                        final int n5 = v2;
                        final int n6 = flags;
                        final int n7 = 1;
                        final long n8 = presentationTimeUs2;
                        final boolean b2 = x;
                        final boolean b3 = y;
                        final v v3 = r;
                        boolean b4 = l.a(n3, n4, g, byteBuffer, n5, n6, n7, n8, b2, b3, v3);
                        if (b4) {
                            this.d(this.m.presentationTimeUs);
                            final boolean b5 = (this.m.flags & 0x4) != 0x0;
                            this.U();
                            if (!b5) {
                                return true;
                            }
                            this.aa();
                        }
                        return false;
                        final g z2 = this.z;
                        final ByteBuffer w3 = this.W;
                        final int v4 = this.V;
                        final MediaCodec$BufferInfo m2 = this.m;
                        b4 = this.a(n, n2, z2, w3, v4, m2.flags, 1, m2.presentationTimeUs, this.X, this.Y, this.r);
                        continue Label_0461_Outer;
                        this.aa();
                        iftrue(Label_0476:)(!this.am);
                        Block_26: {
                            break Block_26;
                            Label_0476: {
                                return false;
                            }
                        }
                        this.J();
                        return false;
                    }
                    catch (final IllegalStateException ex3) {
                        continue;
                    }
                    break;
                }
                break;
            }
        }
    }
    
    private static boolean b(final i i) {
        final String a = i.a;
        final int a2 = ai.a;
        return (a2 <= 25 && "OMX.rk.video_decoder.avc".equals(a)) || (a2 <= 17 && "OMX.allwinner.video.decoder.avc".equals(a)) || (a2 <= 29 && ("OMX.broadcom.video_decoder.tunnel".equals(a) || "OMX.broadcom.video_decoder.tunnel.secure".equals(a))) || ("Amazon".equals(ai.c) && "AFTS".equals(ai.d) && i.g);
    }
    
    @RequiresApi(21)
    private static boolean b(final IllegalStateException ex) {
        return ex instanceof MediaCodec$CodecException;
    }
    
    private static boolean b(final String s) {
        final int a = ai.a;
        if (a >= 18 && (a != 18 || (!"OMX.SEC.avc.dec".equals(s) && !"OMX.SEC.avc.dec.secure".equals(s)))) {
            if (a == 19 && ai.d.startsWith("SM-G800")) {
                if ("OMX.Exynos.avc.dec".equals(s)) {
                    return true;
                }
                if ("OMX.Exynos.avc.dec.secure".equals(s)) {
                    return true;
                }
            }
            return false;
        }
        return true;
    }
    
    private static boolean b(final String anObject, final v v) {
        if (ai.a <= 18) {
            final int y = v.y;
            final boolean b = true;
            if (y == 1 && "OMX.MTK.AUDIO.DECODER.MP3".equals(anObject)) {
                return b;
            }
        }
        return false;
    }
    
    private int c(String b) {
        final int a = com.applovin.exoplayer2.l.ai.a;
        if (a <= 25 && "OMX.Exynos.avc.dec.secure".equals(b)) {
            final String d = com.applovin.exoplayer2.l.ai.d;
            if (d.startsWith("SM-T585") || d.startsWith("SM-A510") || d.startsWith("SM-A520") || d.startsWith("SM-J700")) {
                return 2;
            }
        }
        if (a < 24 && ("OMX.Nvidia.h264.decode".equals(b) || "OMX.Nvidia.h264.decode.secure".equals(b))) {
            b = com.applovin.exoplayer2.l.ai.b;
            if ("flounder".equals(b) || "flounder_lte".equals(b) || "grouper".equals(b) || "tilapia".equals(b)) {
                return 1;
            }
        }
        return 0;
    }
    
    @Nullable
    private n c(final f f) throws p {
        final com.applovin.exoplayer2.c.b g = f.g();
        if (g != null && !(g instanceof n)) {
            final StringBuilder sb = new StringBuilder();
            sb.append("Expecting FrameworkCryptoConfig but found: ");
            sb.append(g);
            throw this.a(new IllegalArgumentException(sb.toString()), this.q, 6001);
        }
        return (n)g;
    }
    
    private boolean c(final long n, final long n2) throws p {
        com.applovin.exoplayer2.l.a.b(this.am ^ true);
        if (this.j.l()) {
            final d j = this.j;
            if (!this.a(n, n2, null, j.b, this.V, 0, j.k(), this.j.i(), this.j.b(), this.j.c(), this.r)) {
                return false;
            }
            this.d(this.j.j());
            this.j.a();
        }
        if (this.al) {
            this.am = true;
            return false;
        }
        if (this.aa) {
            com.applovin.exoplayer2.l.a.b(this.j.a(this.i));
            this.aa = false;
        }
        if (this.ab) {
            if (this.j.l()) {
                return true;
            }
            this.B();
            this.ab = false;
            this.E();
            if (!this.Z) {
                return false;
            }
        }
        this.ad();
        if (this.j.l()) {
            this.j.h();
        }
        return this.j.l() || this.al || this.ab;
    }
    
    protected static boolean c(final v v) {
        final int e = v.E;
        return e == 0 || e == 2;
    }
    
    @RequiresApi(21)
    private static boolean c(final IllegalStateException ex) {
        return ex instanceof MediaCodec$CodecException && ((MediaCodec$CodecException)ex).isRecoverable();
    }
    
    private List<i> d(final boolean b) throws l.b {
        List<i> list2;
        final List<i> list = list2 = this.a(this.d, this.q, b);
        if (list.isEmpty()) {
            list2 = list;
            if (b) {
                final List<i> obj = list2 = this.a(this.d, this.q, (boolean)(0 != 0));
                if (!obj.isEmpty()) {
                    final StringBuilder sb = new StringBuilder();
                    sb.append("Drm session requires secure decoder for ");
                    sb.append(this.q.l);
                    sb.append(", but no secure decoder available. Trying to proceed with ");
                    sb.append(obj);
                    sb.append(".");
                    com.applovin.exoplayer2.l.q.c("MediaCodecRenderer", sb.toString());
                    list2 = obj;
                }
            }
        }
        return list2;
    }
    
    private void d(final v v) {
        this.B();
        final String l = v.l;
        if (!"audio/mp4a-latm".equals(l) && !"audio/mpeg".equals(l) && !"audio/opus".equals(l)) {
            this.j.g(1);
        }
        else {
            this.j.g(32);
        }
        this.Z = true;
    }
    
    private static boolean d(final String anObject) {
        return ai.a == 29 && "c2.android.aac.decoder".equals(anObject);
    }
    
    private boolean e(int a) throws p {
        final w t = this.t();
        this.g.a();
        a = this.a(t, this.g, a | 0x4);
        if (a == -5) {
            this.a(t);
            return true;
        }
        if (a == -4 && this.g.c()) {
            this.al = true;
            this.aa();
        }
        return false;
    }
    
    private boolean e(final long n) {
        return this.w == -9223372036854775807L || SystemClock.elapsedRealtime() - n < this.w;
    }
    
    private boolean e(final v v) throws p {
        if (com.applovin.exoplayer2.l.ai.a < 23) {
            return true;
        }
        if (this.z != null && this.af != 3) {
            if (this.d_() != 0) {
                final float a = this.a(this.y, v, this.u());
                final float d = this.D;
                if (d == a) {
                    return true;
                }
                if (a == -1.0f) {
                    this.Y();
                    return false;
                }
                if (d == -1.0f && a <= this.f) {
                    return true;
                }
                final Bundle bundle = new Bundle();
                bundle.putFloat("operating-rate", a);
                this.z.a(bundle);
                this.D = a;
            }
        }
        return true;
    }
    
    private static boolean e(final String anObject) {
        final int a = ai.a;
        if (a > 23 || !"OMX.google.vorbis.decoder".equals(anObject)) {
            if (a <= 19) {
                final String b = ai.b;
                if (("hb2000".equals(b) || "stvm8".equals(b)) && ("OMX.amlogic.avc.decoder.awesome".equals(anObject) || "OMX.amlogic.avc.decoder.awesome.secure".equals(anObject))) {
                    return true;
                }
            }
            return false;
        }
        return true;
    }
    
    private boolean f(final long n) {
        for (int size = this.l.size(), i = 0; i < size; ++i) {
            if (this.l.get(i) == n) {
                this.l.remove(i);
                return true;
            }
        }
        return false;
    }
    
    private static boolean f(String b) {
        if (ai.a < 21 && "OMX.SEC.mp3.dec".equals(b) && "samsung".equals(ai.c)) {
            b = ai.b;
            if (b.startsWith("baffin") || b.startsWith("grand") || b.startsWith("fortuna") || b.startsWith("gprimelte") || b.startsWith("j2y18lte") || b.startsWith("ms01")) {
                return true;
            }
        }
        return false;
    }
    
    private static boolean g(final String anObject) {
        return ai.a == 21 && "OMX.google.aac.decoder".equals(anObject);
    }
    
    @Override
    public boolean A() {
        return this.am;
    }
    
    protected void C() {
    }
    
    protected void D() throws p {
    }
    
    protected final void E() throws p {
        if (this.z == null && !this.Z) {
            final v q = this.q;
            if (q != null) {
                if (this.t == null && this.b(q)) {
                    this.d(this.q);
                    return;
                }
                this.b(this.t);
                final String l = this.q.l;
                final f s = this.s;
                if (s != null) {
                    if (this.u == null) {
                        final n c = this.c(s);
                        if (c == null) {
                            if (this.s.e() == null) {
                                return;
                            }
                        }
                        else {
                            try {
                                final MediaCrypto u = new MediaCrypto(c.b, c.c);
                                this.u = u;
                                this.v = (!c.d && u.requiresSecureDecoderComponent(l));
                            }
                            catch (final MediaCryptoException ex) {
                                throw this.a((Throwable)ex, this.q, 6006);
                            }
                        }
                    }
                    if (com.applovin.exoplayer2.d.n.a) {
                        final int c2 = this.s.c();
                        if (c2 == 1) {
                            final f.a a = (f.a)com.applovin.exoplayer2.l.a.b((Object)this.s.e());
                            throw this.a(a, this.q, a.a);
                        }
                        if (c2 != 4) {
                            return;
                        }
                    }
                }
                try {
                    this.a(this.u, this.v);
                }
                catch (final a a2) {
                    throw this.a(a2, this.q, 4001);
                }
            }
        }
    }
    
    protected boolean F() {
        return false;
    }
    
    @Nullable
    protected final g G() {
        return this.z;
    }
    
    @Nullable
    protected final MediaFormat H() {
        return this.B;
    }
    
    @Nullable
    protected final i I() {
        return this.G;
    }
    
    protected void J() {
        try {
            final g z = this.z;
            if (z != null) {
                z.e();
                final com.applovin.exoplayer2.c.e a = this.a;
                ++a.b;
                this.a(this.G.a);
            }
            this.z = null;
            try {
                final MediaCrypto u = this.u;
                if (u != null) {
                    u.release();
                }
            }
            finally {
                this.u = null;
                this.b((f)null);
                this.N();
            }
        }
        finally {
            this.z = null;
            try {
                final MediaCrypto u2 = this.u;
                if (u2 != null) {
                    u2.release();
                }
                this.u = null;
                this.b((f)null);
                this.N();
            }
            finally {
                this.u = null;
                this.b((f)null);
                this.N();
            }
        }
    }
    
    protected final boolean K() throws p {
        final boolean l = this.L();
        if (l) {
            this.E();
        }
        return l;
    }
    
    protected boolean L() {
        if (this.z == null) {
            return false;
        }
        if (this.af != 3 && !this.J && (!this.K || this.ai) && (!this.L || !this.ah)) {
            this.R();
            return false;
        }
        this.J();
        return true;
    }
    
    @CallSuper
    protected void M() {
        this.T();
        this.U();
        this.T = -9223372036854775807L;
        this.ah = false;
        this.ag = false;
        this.P = false;
        this.Q = false;
        this.X = false;
        this.Y = false;
        this.l.clear();
        this.aj = -9223372036854775807L;
        this.ak = -9223372036854775807L;
        final com.applovin.exoplayer2.f.e s = this.S;
        if (s != null) {
            s.a();
        }
        this.ae = 0;
        this.af = 0;
        this.ad = (this.ac ? 1 : 0);
    }
    
    @CallSuper
    protected void N() {
        this.M();
        this.as = null;
        this.S = null;
        this.E = null;
        this.G = null;
        this.A = null;
        this.B = null;
        this.C = false;
        this.ai = false;
        this.D = -1.0f;
        this.H = 0;
        this.I = false;
        this.J = false;
        this.K = false;
        this.L = false;
        this.M = false;
        this.N = false;
        this.O = false;
        this.R = false;
        this.ac = false;
        this.ad = 0;
        this.v = false;
    }
    
    protected float O() {
        return this.x;
    }
    
    protected final void P() {
        this.ao = true;
    }
    
    protected final long Q() {
        return this.au;
    }
    
    protected float a(final float n, final v v, final v[] array) {
        return -1.0f;
    }
    
    protected abstract int a(final k p0, final v p1) throws l.b;
    
    @Override
    public final int a(final v v) throws p {
        try {
            return this.a(this.d, v);
        }
        catch (final l.b b) {
            throw this.a(b, v, 4002);
        }
    }
    
    protected com.applovin.exoplayer2.c.h a(final i i, final v v, final v v2) {
        return new com.applovin.exoplayer2.c.h(i.a, v, v2, 0, 1);
    }
    
    @CallSuper
    @Nullable
    protected com.applovin.exoplayer2.c.h a(final w w) throws p {
        final boolean b = true;
        this.an = true;
        final v v = (v)com.applovin.exoplayer2.l.a.b((Object)w.b);
        if (v.l == null) {
            throw this.a(new IllegalArgumentException(), v, 4005);
        }
        this.a(w.a);
        this.q = v;
        if (this.Z) {
            this.ab = true;
            return null;
        }
        final g z = this.z;
        if (z == null) {
            this.E = null;
            this.E();
            return null;
        }
        final i g = this.G;
        final v a = this.A;
        if (this.a(g, v, this.s, this.t)) {
            this.Y();
            return new com.applovin.exoplayer2.c.h(g.a, a, v, 0, 128);
        }
        final boolean b2 = this.t != this.s;
        com.applovin.exoplayer2.l.a.b(!b2 || com.applovin.exoplayer2.l.ai.a >= 23);
        final com.applovin.exoplayer2.c.h a2 = this.a(g, a, v);
        final int d = a2.d;
        int n = 0;
        Label_0412: {
            Label_0410: {
                if (d != 0) {
                    Label_0401: {
                        if (d != 1) {
                            if (d != 2) {
                                if (d != 3) {
                                    throw new IllegalStateException();
                                }
                                if (this.e(v)) {
                                    this.A = v;
                                    if (b2 && !this.X()) {
                                        break Label_0401;
                                    }
                                    break Label_0410;
                                }
                            }
                            else if (this.e(v)) {
                                this.ac = true;
                                this.ad = 1;
                                final int h = this.H;
                                boolean p = b;
                                if (h != 2) {
                                    p = (h == 1 && v.q == a.q && v.r == a.r && b);
                                }
                                this.P = p;
                                this.A = v;
                                if (b2 && !this.X()) {
                                    break Label_0401;
                                }
                                break Label_0410;
                            }
                        }
                        else if (this.e(v)) {
                            this.A = v;
                            if (!(b2 ? this.X() : this.W())) {
                                break Label_0401;
                            }
                            break Label_0410;
                        }
                        n = 16;
                        break Label_0412;
                    }
                    n = 2;
                    break Label_0412;
                }
                this.Y();
            }
            n = 0;
        }
        if (a2.d != 0 && (this.z != z || this.af == 3)) {
            return new com.applovin.exoplayer2.c.h(g.a, a, v, 0, n);
        }
        return a2;
    }
    
    @Nullable
    protected abstract g.a a(final i p0, final v p1, @Nullable final MediaCrypto p2, final float p3);
    
    protected com.applovin.exoplayer2.f.h a(final Throwable t, @Nullable final i i) {
        return new com.applovin.exoplayer2.f.h(t, i);
    }
    
    protected abstract List<i> a(final k p0, final v p1, final boolean p2) throws l.b;
    
    @Override
    public void a(final float x, final float y) throws p {
        this.x = x;
        this.y = y;
        this.e(this.A);
    }
    
    @Override
    public void a(final long n, final long n2) throws p {
        final boolean ao = this.ao;
        final boolean b = false;
        if (ao) {
            this.ao = false;
            this.aa();
        }
        final p as = this.as;
        if (as == null) {
            try {
                if (this.am) {
                    this.D();
                    return;
                }
                if (this.q == null && !this.e(2)) {
                    return;
                }
                this.E();
                if (this.Z) {
                    com.applovin.exoplayer2.l.ah.a("bypassRender");
                    while (this.c(n, n2)) {}
                    com.applovin.exoplayer2.l.ah.a();
                }
                else if (this.z != null) {
                    final long elapsedRealtime = SystemClock.elapsedRealtime();
                    com.applovin.exoplayer2.l.ah.a("drainAndFeed");
                    while (this.b(n, n2) && this.e(elapsedRealtime)) {}
                    while (this.V() && this.e(elapsedRealtime)) {}
                    com.applovin.exoplayer2.l.ah.a();
                }
                else {
                    final com.applovin.exoplayer2.c.e a = this.a;
                    a.d += this.b(n);
                    this.e(1);
                }
                this.a.a();
                return;
            }
            catch (final IllegalStateException ex) {
                if (a(ex)) {
                    this.a((Exception)ex);
                    boolean b2 = b;
                    if (com.applovin.exoplayer2.l.ai.a >= 21) {
                        b2 = b;
                        if (c(ex)) {
                            b2 = true;
                        }
                    }
                    if (b2) {
                        this.J();
                    }
                    throw this.a(this.a(ex, this.I()), this.q, b2, 4003);
                }
                throw ex;
            }
        }
        this.as = null;
        throw as;
    }
    
    @Override
    protected void a(final long n, final boolean b) throws p {
        this.al = false;
        this.am = false;
        this.ao = false;
        if (this.Z) {
            this.j.a();
            this.i.a();
            this.aa = false;
        }
        else {
            this.K();
        }
        if (this.k.b() > 0) {
            this.an = true;
        }
        this.k.a();
        final int av = this.av;
        if (av != 0) {
            this.au = this.o[av - 1];
            this.at = this.n[av - 1];
            this.av = 0;
        }
    }
    
    protected void a(final com.applovin.exoplayer2.c.g g) throws p {
    }
    
    protected final void a(final p as) {
        this.as = as;
    }
    
    protected void a(final v v, @Nullable final MediaFormat mediaFormat) throws p {
    }
    
    protected void a(final Exception ex) {
    }
    
    protected void a(final String s) {
    }
    
    protected void a(final String s, final long n, final long n2) {
    }
    
    public void a(final boolean ap) {
        this.ap = ap;
    }
    
    @Override
    protected void a(final boolean b, final boolean b2) throws p {
        this.a = new com.applovin.exoplayer2.c.e();
    }
    
    @Override
    protected void a(final v[] array, final long at, final long au) throws p {
        final long au2 = this.au;
        boolean b = true;
        if (au2 == -9223372036854775807L) {
            if (this.at != -9223372036854775807L) {
                b = false;
            }
            com.applovin.exoplayer2.l.a.b(b);
            this.at = at;
            this.au = au;
        }
        else {
            final int av = this.av;
            if (av == this.o.length) {
                final StringBuilder sb = new StringBuilder();
                sb.append("Too many stream changes, so dropping offset: ");
                sb.append(this.o[this.av - 1]);
                com.applovin.exoplayer2.l.q.c("MediaCodecRenderer", sb.toString());
            }
            else {
                this.av = av + 1;
            }
            final long[] n = this.n;
            final int av2 = this.av;
            n[av2 - 1] = at;
            this.o[av2 - 1] = au;
            this.p[av2 - 1] = this.aj;
        }
    }
    
    protected abstract boolean a(final long p0, final long p1, @Nullable final g p2, @Nullable final ByteBuffer p3, final int p4, final int p5, final int p6, final long p7, final boolean p8, final boolean p9, final v p10) throws p;
    
    protected boolean a(final i i) {
        return true;
    }
    
    protected void b(final com.applovin.exoplayer2.c.g g) throws p {
    }
    
    public void b(final boolean aq) {
        this.aq = aq;
    }
    
    protected boolean b(final v v) {
        return false;
    }
    
    protected final void c(final long n) throws p {
        v r;
        final v v = r = (v)this.k.a(n);
        if (v == null) {
            r = v;
            if (this.C) {
                r = (v)this.k.c();
            }
        }
        boolean b;
        if (r != null) {
            this.r = r;
            b = true;
        }
        else {
            b = false;
        }
        if (b || (this.C && this.r != null)) {
            this.a(this.r, this.B);
            this.C = false;
        }
    }
    
    public void c(final boolean ar) {
        this.ar = ar;
    }
    
    @CallSuper
    protected void d(final long n) {
        while (true) {
            int av = this.av;
            if (av == 0 || n < this.p[0]) {
                break;
            }
            final long[] n2 = this.n;
            this.at = n2[0];
            this.au = this.o[0];
            --av;
            System.arraycopy(n2, 1, n2, 0, this.av = av);
            final long[] o = this.o;
            System.arraycopy(o, 1, o, 0, this.av);
            final long[] p = this.p;
            System.arraycopy(p, 1, p, 0, this.av);
            this.C();
        }
    }
    
    @Override
    public final int o() {
        return 8;
    }
    
    @Override
    protected void p() {
    }
    
    @Override
    protected void q() {
    }
    
    @Override
    protected void r() {
        this.q = null;
        this.at = -9223372036854775807L;
        this.au = -9223372036854775807L;
        this.av = 0;
        this.L();
    }
    
    @Override
    protected void s() {
        try {
            this.B();
            this.J();
        }
        finally {
            this.a((f)null);
        }
    }
    
    @Override
    public boolean z() {
        return this.q != null && (this.x() || this.S() || (this.T != -9223372036854775807L && SystemClock.elapsedRealtime() < this.T));
    }
    
    public static class a extends Exception
    {
        public final String a;
        public final boolean b;
        @Nullable
        public final i c;
        @Nullable
        public final String d;
        @Nullable
        public final a e;
        
        public a(final v obj, @Nullable final Throwable t, final boolean b, final int i) {
            final StringBuilder sb = new StringBuilder();
            sb.append("Decoder init failed: [");
            sb.append(i);
            sb.append("], ");
            sb.append(obj);
            this(sb.toString(), t, obj.l, b, null, a(i), null);
        }
        
        public a(final v obj, @Nullable final Throwable t, final boolean b, final i i) {
            final StringBuilder sb = new StringBuilder();
            sb.append("Decoder init failed: ");
            sb.append(i.a);
            sb.append(", ");
            sb.append(obj);
            final String string = sb.toString();
            final String l = obj.l;
            String a;
            if (ai.a >= 21) {
                a = a(t);
            }
            else {
                a = null;
            }
            this(string, t, l, b, i, a, null);
        }
        
        private a(final String message, @Nullable final Throwable cause, final String a, final boolean b, @Nullable final i c, @Nullable final String d, @Nullable final a e) {
            super(message, cause);
            this.a = a;
            this.b = b;
            this.c = c;
            this.d = d;
            this.e = e;
        }
        
        @CheckResult
        private a a(final a a) {
            return new a(this.getMessage(), this.getCause(), this.a, this.b, this.c, this.d, a);
        }
        
        private static String a(final int a) {
            String str;
            if (a < 0) {
                str = "neg_";
            }
            else {
                str = "";
            }
            final StringBuilder sb = new StringBuilder();
            sb.append("com.applovin.exoplayer2.mediacodec.MediaCodecRenderer_");
            sb.append(str);
            sb.append(Math.abs(a));
            return sb.toString();
        }
        
        @Nullable
        @RequiresApi(21)
        private static String a(@Nullable final Throwable t) {
            if (t instanceof MediaCodec$CodecException) {
                return ((MediaCodec$CodecException)t).getDiagnosticInfo();
            }
            return null;
        }
    }
}

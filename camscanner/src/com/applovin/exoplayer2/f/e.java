// 
// Decompiled by Procyon v0.6.0
// 

package com.applovin.exoplayer2.f;

import com.applovin.exoplayer2.l.q;
import com.applovin.exoplayer2.b.r;
import com.applovin.exoplayer2.l.a;
import java.nio.ByteBuffer;
import com.applovin.exoplayer2.c.g;
import com.applovin.exoplayer2.v;

final class e
{
    private long a;
    private long b;
    private boolean c;
    
    private long a(final long n) {
        return this.a + Math.max(0L, (this.b - 529L) * 1000000L / n);
    }
    
    public long a(final v v) {
        return this.a(v.z);
    }
    
    public long a(final v v, final g g) {
        if (this.b == 0L) {
            this.a = g.d;
        }
        if (this.c) {
            return g.d;
        }
        final ByteBuffer byteBuffer = (ByteBuffer)com.applovin.exoplayer2.l.a.b((Object)g.b);
        int i = 0;
        int n = 0;
        while (i < 4) {
            n = (n << 8 | (byteBuffer.get(i) & 0xFF));
            ++i;
        }
        final int b = r.b(n);
        if (b == -1) {
            this.c = true;
            this.b = 0L;
            this.a = g.d;
            q.c("C2Mp3TimestampTracker", "MPEG audio header is invalid.");
            return g.d;
        }
        final long a = this.a(v.z);
        this.b += b;
        return a;
    }
    
    public void a() {
        this.a = 0L;
        this.b = 0L;
        this.c = false;
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.applovin.exoplayer2.f;

import android.os.Looper;
import com.applovin.exoplayer2.common.base.Ascii;
import java.util.Arrays;
import androidx.annotation.Nullable;
import com.applovin.exoplayer2.c.\u3007o00\u3007\u3007Oo;
import android.media.MediaCodec$CryptoInfo$Pattern;
import com.applovin.exoplayer2.l.ai;
import com.applovin.exoplayer2.l.a;
import com.applovin.exoplayer2.c.c;
import android.os.Message;
import android.media.MediaCodec$CryptoInfo;
import androidx.annotation.VisibleForTesting;
import com.applovin.exoplayer2.l.g;
import java.util.concurrent.atomic.AtomicReference;
import android.os.Handler;
import android.os.HandlerThread;
import android.media.MediaCodec;
import androidx.annotation.GuardedBy;
import java.util.ArrayDeque;
import androidx.annotation.RequiresApi;

@RequiresApi(23)
class b
{
    @GuardedBy("MESSAGE_PARAMS_INSTANCE_POOL")
    private static final ArrayDeque<a> a;
    private static final Object b;
    private final MediaCodec c;
    private final HandlerThread d;
    private Handler e;
    private final AtomicReference<RuntimeException> f;
    private final g g;
    private final boolean h;
    private boolean i;
    
    static {
        a = new ArrayDeque<a>();
        b = new Object();
    }
    
    public b(final MediaCodec mediaCodec, final HandlerThread handlerThread, final boolean b) {
        this(mediaCodec, handlerThread, b, new g());
    }
    
    @VisibleForTesting
    b(final MediaCodec c, final HandlerThread d, final boolean b, final g g) {
        this.c = c;
        this.d = d;
        this.g = g;
        this.f = new AtomicReference<RuntimeException>();
        this.h = (b || i());
    }
    
    private void a(final int n, final int n2, final MediaCodec$CryptoInfo mediaCodec$CryptoInfo, final long n3, final int n4) {
        try {
            if (this.h) {
                synchronized (com.applovin.exoplayer2.f.b.b) {
                    this.c.queueSecureInputBuffer(n, n2, mediaCodec$CryptoInfo, n3, n4);
                    return;
                }
            }
            this.c.queueSecureInputBuffer(n, n2, mediaCodec$CryptoInfo, n3, n4);
        }
        catch (final RuntimeException ex) {
            this.a(ex);
        }
    }
    
    private void a(final Message message) {
        final int what = message.what;
        a a;
        if (what != 0) {
            if (what != 1) {
                if (what != 2) {
                    this.a(new IllegalStateException(String.valueOf(message.what)));
                }
                else {
                    this.g.a();
                }
                a = null;
            }
            else {
                a = (a)message.obj;
                this.a(a.a, a.b, a.d, a.e, a.f);
            }
        }
        else {
            a = (a)message.obj;
            this.b(a.a, a.b, a.c, a.e, a.f);
        }
        if (a != null) {
            a(a);
        }
    }
    
    private static void a(final c c, final MediaCodec$CryptoInfo mediaCodec$CryptoInfo) {
        mediaCodec$CryptoInfo.numSubSamples = c.f;
        mediaCodec$CryptoInfo.numBytesOfClearData = a(c.d, mediaCodec$CryptoInfo.numBytesOfClearData);
        mediaCodec$CryptoInfo.numBytesOfEncryptedData = a(c.e, mediaCodec$CryptoInfo.numBytesOfEncryptedData);
        mediaCodec$CryptoInfo.key = (byte[])com.applovin.exoplayer2.l.a.b((Object)a(c.b, mediaCodec$CryptoInfo.key));
        mediaCodec$CryptoInfo.iv = (byte[])com.applovin.exoplayer2.l.a.b((Object)a(c.a, mediaCodec$CryptoInfo.iv));
        mediaCodec$CryptoInfo.mode = c.c;
        if (ai.a >= 24) {
            \u3007o00\u3007\u3007Oo.\u3007080(mediaCodec$CryptoInfo, new MediaCodec$CryptoInfo$Pattern(c.g, c.h));
        }
    }
    
    private static void a(final a e) {
        final ArrayDeque<a> a = com.applovin.exoplayer2.f.b.a;
        synchronized (a) {
            a.add(e);
        }
    }
    
    @Nullable
    private static byte[] a(@Nullable final byte[] original, @Nullable final byte[] array) {
        if (original == null) {
            return array;
        }
        if (array != null && array.length >= original.length) {
            System.arraycopy(original, 0, array, 0, original.length);
            return array;
        }
        return Arrays.copyOf(original, original.length);
    }
    
    @Nullable
    private static int[] a(@Nullable final int[] original, @Nullable final int[] array) {
        if (original == null) {
            return array;
        }
        if (array != null && array.length >= original.length) {
            System.arraycopy(original, 0, array, 0, original.length);
            return array;
        }
        return Arrays.copyOf(original, original.length);
    }
    
    private void b(final int n, final int n2, final int n3, final long n4, final int n5) {
        try {
            this.c.queueInputBuffer(n, n2, n3, n4, n5);
        }
        catch (final RuntimeException ex) {
            this.a(ex);
        }
    }
    
    private void e() {
        final RuntimeException ex = this.f.getAndSet(null);
        if (ex == null) {
            return;
        }
        throw ex;
    }
    
    private void f() throws InterruptedException {
        ((Handler)ai.a((Object)this.e)).removeCallbacksAndMessages((Object)null);
        this.g();
        this.e();
    }
    
    private void g() throws InterruptedException {
        this.g.b();
        ((Handler)ai.a((Object)this.e)).obtainMessage(2).sendToTarget();
        this.g.c();
    }
    
    private static a h() {
        final ArrayDeque<a> a = com.applovin.exoplayer2.f.b.a;
        synchronized (a) {
            if (a.isEmpty()) {
                return new a();
            }
            return a.removeFirst();
        }
    }
    
    private static boolean i() {
        final String lowerCase = Ascii.toLowerCase(ai.c);
        return lowerCase.contains("samsung") || lowerCase.contains("motorola");
    }
    
    public void a() {
        if (!this.i) {
            ((Thread)this.d).start();
            this.e = new Handler(this, this.d.getLooper()) {
                final b a;
                
                public void handleMessage(final Message message) {
                    this.a.a(message);
                }
            };
            this.i = true;
        }
    }
    
    public void a(final int n, final int n2, final int n3, final long n4, final int n5) {
        this.e();
        final a h = h();
        h.a(n, n2, n3, n4, n5);
        ((Handler)ai.a((Object)this.e)).obtainMessage(0, (Object)h).sendToTarget();
    }
    
    public void a(final int n, final int n2, final c c, final long n3, final int n4) {
        this.e();
        final a h = h();
        h.a(n, n2, 0, n3, n4);
        a(c, h.d);
        ((Handler)ai.a((Object)this.e)).obtainMessage(1, (Object)h).sendToTarget();
    }
    
    @VisibleForTesting
    void a(final RuntimeException newValue) {
        this.f.set(newValue);
    }
    
    public void b() {
        if (this.i) {
            try {
                this.f();
            }
            catch (final InterruptedException cause) {
                Thread.currentThread().interrupt();
                throw new IllegalStateException(cause);
            }
        }
    }
    
    public void c() {
        if (this.i) {
            this.b();
            this.d.quit();
        }
        this.i = false;
    }
    
    public void d() throws InterruptedException {
        this.g();
    }
    
    private static class a
    {
        public int a;
        public int b;
        public int c;
        public final MediaCodec$CryptoInfo d;
        public long e;
        public int f;
        
        a() {
            this.d = new MediaCodec$CryptoInfo();
        }
        
        public void a(final int a, final int b, final int c, final long e, final int f) {
            this.a = a;
            this.b = b;
            this.c = c;
            this.e = e;
            this.f = f;
        }
    }
}

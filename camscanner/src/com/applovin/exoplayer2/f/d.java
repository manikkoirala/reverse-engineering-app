// 
// Decompiled by Procyon v0.6.0
// 

package com.applovin.exoplayer2.f;

import androidx.annotation.IntRange;
import com.applovin.exoplayer2.l.a;
import java.nio.ByteBuffer;
import com.applovin.exoplayer2.c.g;

final class d extends g
{
    private long f;
    private int g;
    private int h;
    
    public d() {
        super(2);
        this.h = 32;
    }
    
    private boolean b(final g g) {
        if (!this.l()) {
            return true;
        }
        if (this.g >= this.h) {
            return false;
        }
        if (g.b() != this.b()) {
            return false;
        }
        final ByteBuffer b = g.b;
        if (b != null) {
            final ByteBuffer b2 = super.b;
            if (b2 != null && b2.position() + b.remaining() > 3072000) {
                return false;
            }
        }
        return true;
    }
    
    @Override
    public void a() {
        super.a();
        this.g = 0;
    }
    
    public boolean a(final g g) {
        com.applovin.exoplayer2.l.a.a(g.g() ^ true);
        com.applovin.exoplayer2.l.a.a(g.e() ^ true);
        com.applovin.exoplayer2.l.a.a(g.c() ^ true);
        if (!this.b(g)) {
            return false;
        }
        if (this.g++ == 0) {
            super.d = g.d;
            if (g.d()) {
                this.a_(1);
            }
        }
        if (g.b()) {
            this.a_(Integer.MIN_VALUE);
        }
        final ByteBuffer b = g.b;
        if (b != null) {
            this.f(b.remaining());
            super.b.put(b);
        }
        this.f = g.d;
        return true;
    }
    
    public void g(@IntRange(from = 1L) final int h) {
        com.applovin.exoplayer2.l.a.a(h > 0);
        this.h = h;
    }
    
    public long i() {
        return super.d;
    }
    
    public long j() {
        return this.f;
    }
    
    public int k() {
        return this.g;
    }
    
    public boolean l() {
        return this.g > 0;
    }
}

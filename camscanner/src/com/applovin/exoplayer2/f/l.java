// 
// Decompiled by Procyon v0.6.0
// 

package com.applovin.exoplayer2.f;

import android.media.MediaCodecList;
import android.text.TextUtils;
import com.applovin.exoplayer2.common.base.Ascii;
import com.applovin.exoplayer2.l.u;
import androidx.annotation.RequiresApi;
import android.media.MediaCodecInfo$CodecProfileLevel;
import java.util.Comparator;
import java.util.Collections;
import androidx.annotation.CheckResult;
import java.util.Collection;
import android.media.MediaCodecInfo$CodecCapabilities;
import com.applovin.exoplayer2.l.ai;
import java.util.ArrayList;
import android.media.MediaCodecInfo;
import com.applovin.exoplayer2.m.b;
import java.util.regex.Matcher;
import com.applovin.exoplayer2.l.q;
import androidx.annotation.Nullable;
import android.util.Pair;
import com.applovin.exoplayer2.v;
import androidx.annotation.GuardedBy;
import java.util.List;
import java.util.HashMap;
import java.util.regex.Pattern;
import android.annotation.SuppressLint;

@SuppressLint({ "InlinedApi" })
public final class l
{
    private static final Pattern a;
    @GuardedBy("MediaCodecUtil.class")
    private static final HashMap<a, List<i>> b;
    private static int c;
    
    static {
        a = Pattern.compile("^\\D?(\\d+)$");
        b = new HashMap<a, List<i>>();
        l.c = -1;
    }
    
    private static int a(final int n) {
        if (n == 1 || n == 2) {
            return 25344;
        }
        switch (n) {
            default: {
                return -1;
            }
            case 131072:
            case 262144:
            case 524288: {
                return 35651584;
            }
            case 32768:
            case 65536: {
                return 9437184;
            }
            case 16384: {
                return 5652480;
            }
            case 8192: {
                return 2228224;
            }
            case 2048:
            case 4096: {
                return 2097152;
            }
            case 1024: {
                return 1310720;
            }
            case 512: {
                return 921600;
            }
            case 128:
            case 256: {
                return 414720;
            }
            case 64: {
                return 202752;
            }
            case 8:
            case 16:
            case 32: {
                return 101376;
            }
        }
    }
    
    @Nullable
    public static Pair<Integer, Integer> a(final v v) {
        final String i = v.i;
        if (i == null) {
            return null;
        }
        final String[] split = i.split("\\.");
        if ("video/dolby-vision".equals(v.l)) {
            return a(v.i, split);
        }
        int n = 0;
        final String s = split[0];
        s.hashCode();
        Label_0240: {
            switch (s) {
                case "vp09": {
                    n = 6;
                    break Label_0240;
                }
                case "mp4a": {
                    n = 5;
                    break Label_0240;
                }
                case "hvc1": {
                    n = 4;
                    break Label_0240;
                }
                case "hev1": {
                    n = 3;
                    break Label_0240;
                }
                case "avc2": {
                    n = 2;
                    break Label_0240;
                }
                case "avc1": {
                    n = 1;
                    break Label_0240;
                }
                case "av01": {
                    break Label_0240;
                }
                default:
                    break;
            }
            n = -1;
        }
        switch (n) {
            default: {
                return null;
            }
            case 6: {
                return d(v.i, split);
            }
            case 5: {
                return e(v.i, split);
            }
            case 3:
            case 4: {
                return b(v.i, split);
            }
            case 1:
            case 2: {
                return c(v.i, split);
            }
            case 0: {
                return a(v.i, split, v.x);
            }
        }
    }
    
    @Nullable
    private static Pair<Integer, Integer> a(final String s, final String[] array) {
        if (array.length < 3) {
            final StringBuilder sb = new StringBuilder();
            sb.append("Ignoring malformed Dolby Vision codec string: ");
            sb.append(s);
            q.c("MediaCodecUtil", sb.toString());
            return null;
        }
        final Matcher matcher = l.a.matcher(array[1]);
        if (!matcher.matches()) {
            final StringBuilder sb2 = new StringBuilder();
            sb2.append("Ignoring malformed Dolby Vision codec string: ");
            sb2.append(s);
            q.c("MediaCodecUtil", sb2.toString());
            return null;
        }
        final String group = matcher.group(1);
        final Integer b = b(group);
        if (b == null) {
            final StringBuilder sb3 = new StringBuilder();
            sb3.append("Unknown Dolby Vision profile string: ");
            sb3.append(group);
            q.c("MediaCodecUtil", sb3.toString());
            return null;
        }
        final String str = array[2];
        final Integer c = c(str);
        if (c == null) {
            final StringBuilder sb4 = new StringBuilder();
            sb4.append("Unknown Dolby Vision level string: ");
            sb4.append(str);
            q.c("MediaCodecUtil", sb4.toString());
            return null;
        }
        return (Pair<Integer, Integer>)new Pair((Object)b, (Object)c);
    }
    
    @Nullable
    private static Pair<Integer, Integer> a(String s, final String[] array, @Nullable final com.applovin.exoplayer2.m.b b) {
        if (array.length < 4) {
            final StringBuilder sb = new StringBuilder();
            sb.append("Ignoring malformed AV1 codec string: ");
            sb.append(s);
            q.c("MediaCodecUtil", sb.toString());
            return null;
        }
        int i = 1;
        try {
            final int int1 = Integer.parseInt(array[1]);
            final int int2 = Integer.parseInt(array[2].substring(0, 2));
            final int int3 = Integer.parseInt(array[3]);
            if (int1 != 0) {
                s = (String)new StringBuilder();
                ((StringBuilder)s).append("Unknown AV1 profile: ");
                ((StringBuilder)s).append(int1);
                q.c("MediaCodecUtil", ((StringBuilder)s).toString());
                return null;
            }
            if (int3 != 8 && int3 != 10) {
                s = (String)new StringBuilder();
                ((StringBuilder)s).append("Unknown AV1 bit depth: ");
                ((StringBuilder)s).append(int3);
                q.c("MediaCodecUtil", ((StringBuilder)s).toString());
                return null;
            }
            Label_0201: {
                if (int3 != 8) {
                    Label_0199: {
                        if (b != null) {
                            if (b.d == null) {
                                final int c = b.c;
                                if (c != 7 && c != 6) {
                                    break Label_0199;
                                }
                            }
                            i = 4096;
                            break Label_0201;
                        }
                    }
                    i = 2;
                }
            }
            final int f = f(int2);
            if (f == -1) {
                s = (String)new StringBuilder();
                ((StringBuilder)s).append("Unknown AV1 level: ");
                ((StringBuilder)s).append(int2);
                q.c("MediaCodecUtil", ((StringBuilder)s).toString());
                return null;
            }
            return (Pair<Integer, Integer>)new Pair((Object)i, (Object)f);
        }
        catch (final NumberFormatException ex) {
            final StringBuilder sb2 = new StringBuilder();
            sb2.append("Ignoring malformed AV1 codec string: ");
            sb2.append(s);
            q.c("MediaCodecUtil", sb2.toString());
            return null;
        }
    }
    
    @Nullable
    public static i a() throws b {
        return a("audio/raw", false, false);
    }
    
    @Nullable
    public static i a(final String s, final boolean b, final boolean b2) throws b {
        final List<i> b3 = b(s, b, b2);
        i i;
        if (b3.isEmpty()) {
            i = null;
        }
        else {
            i = b3.get(0);
        }
        return i;
    }
    
    @Nullable
    private static Integer a(@Nullable final String s) {
        if (s == null) {
            return null;
        }
        final int hashCode = s.hashCode();
        int n = -1;
        switch (hashCode) {
            case 2312995: {
                if (!s.equals("L186")) {
                    break;
                }
                n = 25;
                break;
            }
            case 2312992: {
                if (!s.equals("L183")) {
                    break;
                }
                n = 24;
                break;
            }
            case 2312989: {
                if (!s.equals("L180")) {
                    break;
                }
                n = 23;
                break;
            }
            case 2312902: {
                if (!s.equals("L156")) {
                    break;
                }
                n = 22;
                break;
            }
            case 2312899: {
                if (!s.equals("L153")) {
                    break;
                }
                n = 21;
                break;
            }
            case 2312896: {
                if (!s.equals("L150")) {
                    break;
                }
                n = 20;
                break;
            }
            case 2312806: {
                if (!s.equals("L123")) {
                    break;
                }
                n = 19;
                break;
            }
            case 2312803: {
                if (!s.equals("L120")) {
                    break;
                }
                n = 18;
                break;
            }
            case 2193831: {
                if (!s.equals("H186")) {
                    break;
                }
                n = 17;
                break;
            }
            case 2193828: {
                if (!s.equals("H183")) {
                    break;
                }
                n = 16;
                break;
            }
            case 2193825: {
                if (!s.equals("H180")) {
                    break;
                }
                n = 15;
                break;
            }
            case 2193738: {
                if (!s.equals("H156")) {
                    break;
                }
                n = 14;
                break;
            }
            case 2193735: {
                if (!s.equals("H153")) {
                    break;
                }
                n = 13;
                break;
            }
            case 2193732: {
                if (!s.equals("H150")) {
                    break;
                }
                n = 12;
                break;
            }
            case 2193642: {
                if (!s.equals("H123")) {
                    break;
                }
                n = 11;
                break;
            }
            case 2193639: {
                if (!s.equals("H120")) {
                    break;
                }
                n = 10;
                break;
            }
            case 74854: {
                if (!s.equals("L93")) {
                    break;
                }
                n = 9;
                break;
            }
            case 74851: {
                if (!s.equals("L90")) {
                    break;
                }
                n = 8;
                break;
            }
            case 74761: {
                if (!s.equals("L63")) {
                    break;
                }
                n = 7;
                break;
            }
            case 74758: {
                if (!s.equals("L60")) {
                    break;
                }
                n = 6;
                break;
            }
            case 74665: {
                if (!s.equals("L30")) {
                    break;
                }
                n = 5;
                break;
            }
            case 71010: {
                if (!s.equals("H93")) {
                    break;
                }
                n = 4;
                break;
            }
            case 71007: {
                if (!s.equals("H90")) {
                    break;
                }
                n = 3;
                break;
            }
            case 70917: {
                if (!s.equals("H63")) {
                    break;
                }
                n = 2;
                break;
            }
            case 70914: {
                if (!s.equals("H60")) {
                    break;
                }
                n = 1;
                break;
            }
            case 70821: {
                if (!s.equals("H30")) {
                    break;
                }
                n = 0;
                break;
            }
        }
        switch (n) {
            default: {
                return null;
            }
            case 25: {
                return 16777216;
            }
            case 24: {
                return 4194304;
            }
            case 23: {
                return 1048576;
            }
            case 22: {
                return 262144;
            }
            case 21: {
                return 65536;
            }
            case 20: {
                return 16384;
            }
            case 19: {
                return 4096;
            }
            case 18: {
                return 1024;
            }
            case 17: {
                return 33554432;
            }
            case 16: {
                return 8388608;
            }
            case 15: {
                return 2097152;
            }
            case 14: {
                return 524288;
            }
            case 13: {
                return 131072;
            }
            case 12: {
                return 32768;
            }
            case 11: {
                return 8192;
            }
            case 10: {
                return 2048;
            }
            case 9: {
                return 256;
            }
            case 8: {
                return 64;
            }
            case 7: {
                return 16;
            }
            case 6: {
                return 4;
            }
            case 5: {
                return 1;
            }
            case 4: {
                return 512;
            }
            case 3: {
                return 128;
            }
            case 2: {
                return 32;
            }
            case 1: {
                return 8;
            }
            case 0: {
                return 2;
            }
        }
    }
    
    @Nullable
    private static String a(final MediaCodecInfo mediaCodecInfo, final String anObject, final String anotherString) {
        for (final String s : mediaCodecInfo.getSupportedTypes()) {
            if (s.equalsIgnoreCase(anotherString)) {
                return s;
            }
        }
        if (anotherString.equals("video/dolby-vision")) {
            if ("OMX.MS.HEVCDV.Decoder".equals(anObject)) {
                return "video/hevcdv";
            }
            if ("OMX.RTK.video.decoder".equals(anObject) || "OMX.realtek.video.decoder.tunneled".equals(anObject)) {
                return "video/dv_hevc";
            }
        }
        else {
            if (anotherString.equals("audio/alac") && "OMX.lge.alac.decoder".equals(anObject)) {
                return "audio/x-lg-alac";
            }
            if (anotherString.equals("audio/flac") && "OMX.lge.flac.decoder".equals(anObject)) {
                return "audio/x-lg-flac";
            }
        }
        return null;
    }
    
    private static ArrayList<i> a(final a a, final c c) throws b {
        try {
            final ArrayList<i> list = new ArrayList<i>();
            final String a2 = a.a;
            final int a3 = c.a();
            final boolean b = c.b();
            for (int i = 0; i < a3; ++i) {
                final MediaCodecInfo a4 = c.a(i);
                if (!a(a4)) {
                    final String name = a4.getName();
                    if (a(a4, name, b, a2)) {
                        final String a5 = a(a4, name, a2);
                        if (a5 != null) {
                            Label_0424: {
                                try {
                                    final MediaCodecInfo$CodecCapabilities capabilitiesForType = a4.getCapabilitiesForType(a5);
                                    final boolean a6 = c.a("tunneled-playback", a5, capabilitiesForType);
                                    final boolean b2 = c.b("tunneled-playback", a5, capabilitiesForType);
                                    final boolean c2 = a.c;
                                    if (!c2 && b2) {
                                        continue;
                                    }
                                    if (c2 && !a6) {
                                        continue;
                                    }
                                    final boolean a7 = c.a("secure-playback", a5, capabilitiesForType);
                                    final boolean b3 = c.b("secure-playback", a5, capabilitiesForType);
                                    final boolean b4 = a.b;
                                    if (!b4 && b3) {
                                        continue;
                                    }
                                    if (b4 && !a7) {
                                        continue;
                                    }
                                    final boolean c3 = c(a4);
                                    final boolean e = e(a4);
                                    final boolean g = g(a4);
                                    Label_0283: {
                                        if (b && a.b == a7) {
                                            break Label_0283;
                                        }
                                        if (b) {
                                            break Label_0283;
                                        }
                                        try {
                                            if (!a.b) {
                                                try {
                                                    list.add(i.a(name, a2, a5, capabilitiesForType, c3, e, g, false, false));
                                                }
                                                catch (final Exception ex) {}
                                            }
                                        }
                                        catch (final Exception ex) {
                                            break Label_0424;
                                        }
                                    }
                                    if (b || !a7) {
                                        continue;
                                    }
                                    final StringBuilder sb = new StringBuilder();
                                    try {
                                        sb.append(name);
                                        sb.append(".secure");
                                        final String string = sb.toString();
                                        try {
                                            list.add(i.a(string, a2, a5, capabilitiesForType, c3, e, g, false, true));
                                            return list;
                                        }
                                        catch (final Exception ex) {}
                                    }
                                    catch (final Exception ex) {}
                                }
                                catch (final Exception ex) {}
                            }
                            if (ai.a > 23 || list.isEmpty()) {
                                final StringBuilder sb2 = new StringBuilder();
                                sb2.append("Failed to query codec ");
                                sb2.append(name);
                                sb2.append(" (");
                                sb2.append(a5);
                                sb2.append(")");
                                q.d("MediaCodecUtil", sb2.toString());
                                throw;
                            }
                            final StringBuilder sb3 = new StringBuilder();
                            sb3.append("Skipping codec ");
                            sb3.append(name);
                            sb3.append(" (failed to query capabilities)");
                            q.d("MediaCodecUtil", sb3.toString());
                        }
                    }
                }
            }
            return list;
        }
        catch (final Exception ex2) {
            throw new b((Throwable)ex2);
        }
    }
    
    @CheckResult
    public static List<i> a(final List<i> c, final v v) {
        final ArrayList list = new ArrayList((Collection<? extends E>)c);
        a(list, (f<Object>)new \u30070\u3007O0088o(v));
        return list;
    }
    
    private static void a(String a, final List<i> list) {
        if ("audio/raw".equals(a)) {
            if (ai.a < 26 && ai.b.equals("R9") && list.size() == 1 && ((i)list.get(0)).a.equals("OMX.MTK.AUDIO.DECODER.RAW")) {
                list.add(i.a("OMX.google.raw.decoder", "audio/raw", "audio/raw", null, false, true, false, false, false));
            }
            a((List<Object>)list, new \u3007\u3007808\u3007());
        }
        final int a2 = ai.a;
        if (a2 < 21 && list.size() > 1) {
            a = list.get(0).a;
            if ("OMX.SEC.mp3.dec".equals(a) || "OMX.SEC.MP3.Decoder".equals(a) || "OMX.brcm.audio.mp3.decoder".equals(a)) {
                a((List<Object>)list, new \u3007O00());
            }
        }
        if (a2 < 32 && list.size() > 1 && "OMX.qti.audio.decoder.flac".equals(list.get(0).a)) {
            list.add(list.remove(0));
        }
    }
    
    private static <T> void a(final List<T> list, final f<T> f) {
        Collections.sort(list, new \u3007\u30078O0\u30078((f)f));
    }
    
    private static boolean a(final MediaCodecInfo mediaCodecInfo) {
        return ai.a >= 29 && b(mediaCodecInfo);
    }
    
    private static boolean a(final MediaCodecInfo mediaCodecInfo, final String s, final boolean b, final String anObject) {
        if (mediaCodecInfo.isEncoder() || (!b && s.endsWith(".secure"))) {
            return false;
        }
        final int a = ai.a;
        if (a < 21 && ("CIPAACDecoder".equals(s) || "CIPMP3Decoder".equals(s) || "CIPVorbisDecoder".equals(s) || "CIPAMRNBDecoder".equals(s) || "AACDecoder".equals(s) || "MP3Decoder".equals(s))) {
            return false;
        }
        if (a < 18 && "OMX.MTK.AUDIO.DECODER.AAC".equals(s)) {
            final String b2 = ai.b;
            if ("a70".equals(b2) || ("Xiaomi".equals(ai.c) && b2.startsWith("HM"))) {
                return false;
            }
        }
        if (a == 16 && "OMX.qcom.audio.decoder.mp3".equals(s)) {
            final String b3 = ai.b;
            if ("dlxu".equals(b3) || "protou".equals(b3) || "ville".equals(b3) || "villeplus".equals(b3) || "villec2".equals(b3) || b3.startsWith("gee") || "C6602".equals(b3) || "C6603".equals(b3) || "C6606".equals(b3) || "C6616".equals(b3) || "L36h".equals(b3) || "SO-02E".equals(b3)) {
                return false;
            }
        }
        if (a == 16 && "OMX.qcom.audio.decoder.aac".equals(s)) {
            final String b4 = ai.b;
            if ("C1504".equals(b4) || "C1505".equals(b4) || "C1604".equals(b4) || "C1605".equals(b4)) {
                return false;
            }
        }
        if (a < 24 && ("OMX.SEC.aac.dec".equals(s) || "OMX.Exynos.AAC.Decoder".equals(s)) && "samsung".equals(ai.c)) {
            final String b5 = ai.b;
            if (b5.startsWith("zeroflte") || b5.startsWith("zerolte") || b5.startsWith("zenlte") || "SC-05G".equals(b5) || "marinelteatt".equals(b5) || "404SC".equals(b5) || "SC-04G".equals(b5) || "SCV31".equals(b5)) {
                return false;
            }
        }
        if (a <= 19 && "OMX.SEC.vp8.dec".equals(s) && "samsung".equals(ai.c)) {
            final String b6 = ai.b;
            if (b6.startsWith("d2") || b6.startsWith("serrano") || b6.startsWith("jflte") || b6.startsWith("santos") || b6.startsWith("t0")) {
                return false;
            }
        }
        return (a > 19 || !ai.b.startsWith("jflte") || !"OMX.qcom.video.decoder.vp8".equals(s)) && (!"audio/eac3-joc".equals(anObject) || !"OMX.MTK.AUDIO.DECODER.DSPAC3".equals(s));
    }
    
    public static int b() throws b {
        if (l.c == -1) {
            int max = 0;
            int i = 0;
            final i a = a("video/avc", false, false);
            if (a != null) {
                final MediaCodecInfo$CodecProfileLevel[] a2 = a.a();
                final int length = a2.length;
                int max2 = 0;
                while (i < length) {
                    max2 = Math.max(a(a2[i].level), max2);
                    ++i;
                }
                int b;
                if (ai.a >= 21) {
                    b = 345600;
                }
                else {
                    b = 172800;
                }
                max = Math.max(max2, b);
            }
            l.c = max;
        }
        return l.c;
    }
    
    private static int b(final int n) {
        if (n == 66) {
            return 1;
        }
        if (n == 77) {
            return 2;
        }
        if (n == 88) {
            return 4;
        }
        if (n == 100) {
            return 8;
        }
        if (n == 110) {
            return 16;
        }
        if (n == 122) {
            return 32;
        }
        if (n != 244) {
            return -1;
        }
        return 64;
    }
    
    @Nullable
    private static Pair<Integer, Integer> b(String group, final String[] array) {
        if (array.length < 4) {
            final StringBuilder sb = new StringBuilder();
            sb.append("Ignoring malformed HEVC codec string: ");
            sb.append(group);
            q.c("MediaCodecUtil", sb.toString());
            return null;
        }
        final Pattern a = l.a;
        int i = 1;
        final Matcher matcher = a.matcher(array[1]);
        if (!matcher.matches()) {
            final StringBuilder sb2 = new StringBuilder();
            sb2.append("Ignoring malformed HEVC codec string: ");
            sb2.append(group);
            q.c("MediaCodecUtil", sb2.toString());
            return null;
        }
        group = matcher.group(1);
        if (!"1".equals(group)) {
            if (!"2".equals(group)) {
                final StringBuilder sb3 = new StringBuilder();
                sb3.append("Unknown HEVC profile string: ");
                sb3.append(group);
                q.c("MediaCodecUtil", sb3.toString());
                return null;
            }
            i = 2;
        }
        group = array[3];
        final Integer a2 = a(group);
        if (a2 == null) {
            final StringBuilder sb4 = new StringBuilder();
            sb4.append("Unknown HEVC level string: ");
            sb4.append(group);
            q.c("MediaCodecUtil", sb4.toString());
            return null;
        }
        return (Pair<Integer, Integer>)new Pair((Object)i, (Object)a2);
    }
    
    @Nullable
    private static Integer b(@Nullable final String s) {
        if (s == null) {
            return null;
        }
        final int hashCode = s.hashCode();
        int n = -1;
        switch (hashCode) {
            case 1545: {
                if (!s.equals("09")) {
                    break;
                }
                n = 9;
                break;
            }
            case 1544: {
                if (!s.equals("08")) {
                    break;
                }
                n = 8;
                break;
            }
            case 1543: {
                if (!s.equals("07")) {
                    break;
                }
                n = 7;
                break;
            }
            case 1542: {
                if (!s.equals("06")) {
                    break;
                }
                n = 6;
                break;
            }
            case 1541: {
                if (!s.equals("05")) {
                    break;
                }
                n = 5;
                break;
            }
            case 1540: {
                if (!s.equals("04")) {
                    break;
                }
                n = 4;
                break;
            }
            case 1539: {
                if (!s.equals("03")) {
                    break;
                }
                n = 3;
                break;
            }
            case 1538: {
                if (!s.equals("02")) {
                    break;
                }
                n = 2;
                break;
            }
            case 1537: {
                if (!s.equals("01")) {
                    break;
                }
                n = 1;
                break;
            }
            case 1536: {
                if (!s.equals("00")) {
                    break;
                }
                n = 0;
                break;
            }
        }
        switch (n) {
            default: {
                return null;
            }
            case 9: {
                return 512;
            }
            case 8: {
                return 256;
            }
            case 7: {
                return 128;
            }
            case 6: {
                return 64;
            }
            case 5: {
                return 32;
            }
            case 4: {
                return 16;
            }
            case 3: {
                return 8;
            }
            case 2: {
                return 4;
            }
            case 1: {
                return 2;
            }
            case 0: {
                return 1;
            }
        }
    }
    
    public static List<i> b(final String str, final boolean b, final boolean b2) throws b {
        synchronized (l.class) {
            final a a = new a(str, b, b2);
            final HashMap<a, List<i>> b3 = l.b;
            final List list = b3.get(a);
            if (list != null) {
                return list;
            }
            final int a2 = ai.a;
            c c;
            if (a2 >= 21) {
                c = new e(b, b2);
            }
            else {
                c = new d();
            }
            ArrayList<i> list3;
            final ArrayList<i> list2 = list3 = a(a, c);
            if (b) {
                list3 = list2;
                if (list2.isEmpty()) {
                    list3 = list2;
                    if (21 <= a2) {
                        list3 = list2;
                        if (a2 <= 23) {
                            final ArrayList<i> list4 = list3 = a(a, (c)new d());
                            if (!list4.isEmpty()) {
                                final StringBuilder sb = new StringBuilder();
                                sb.append("MediaCodecList API didn't list secure decoder for: ");
                                sb.append(str);
                                sb.append(". Assuming: ");
                                sb.append(list4.get(0).a);
                                q.c("MediaCodecUtil", sb.toString());
                                list3 = list4;
                            }
                        }
                    }
                }
            }
            a(str, list3);
            final List<Object> unmodifiableList = Collections.unmodifiableList((List<?>)list3);
            b3.put(a, unmodifiableList);
            return (List<i>)unmodifiableList;
        }
    }
    
    @RequiresApi(29)
    private static boolean b(final MediaCodecInfo mediaCodecInfo) {
        return Oooo8o0\u3007.\u3007080(mediaCodecInfo);
    }
    
    private static int c(final int n) {
        switch (n) {
            default: {
                switch (n) {
                    default: {
                        switch (n) {
                            default: {
                                switch (n) {
                                    default: {
                                        switch (n) {
                                            default: {
                                                return -1;
                                            }
                                            case 52: {
                                                return 65536;
                                            }
                                            case 51: {
                                                return 32768;
                                            }
                                            case 50: {
                                                return 16384;
                                            }
                                        }
                                        break;
                                    }
                                    case 42: {
                                        return 8192;
                                    }
                                    case 41: {
                                        return 4096;
                                    }
                                    case 40: {
                                        return 2048;
                                    }
                                }
                                break;
                            }
                            case 32: {
                                return 1024;
                            }
                            case 31: {
                                return 512;
                            }
                            case 30: {
                                return 256;
                            }
                        }
                        break;
                    }
                    case 22: {
                        return 128;
                    }
                    case 21: {
                        return 64;
                    }
                    case 20: {
                        return 32;
                    }
                }
                break;
            }
            case 13: {
                return 16;
            }
            case 12: {
                return 8;
            }
            case 11: {
                return 4;
            }
            case 10: {
                return 1;
            }
        }
    }
    
    @Nullable
    private static Pair<Integer, Integer> c(String str, final String[] array) {
        if (array.length < 2) {
            final StringBuilder sb = new StringBuilder();
            sb.append("Ignoring malformed AVC codec string: ");
            sb.append(str);
            q.c("MediaCodecUtil", sb.toString());
            return null;
        }
        try {
            int i;
            int j;
            if (array[1].length() == 6) {
                i = Integer.parseInt(array[1].substring(0, 2), 16);
                j = Integer.parseInt(array[1].substring(4), 16);
            }
            else {
                if (array.length < 3) {
                    final StringBuilder sb2 = new StringBuilder();
                    sb2.append("Ignoring malformed AVC codec string: ");
                    sb2.append(str);
                    q.c("MediaCodecUtil", sb2.toString());
                    return null;
                }
                i = Integer.parseInt(array[1]);
                j = Integer.parseInt(array[2]);
            }
            final int b = b(i);
            if (b == -1) {
                str = (String)new StringBuilder();
                ((StringBuilder)str).append("Unknown AVC profile: ");
                ((StringBuilder)str).append(i);
                q.c("MediaCodecUtil", ((StringBuilder)str).toString());
                return null;
            }
            final int c = c(j);
            if (c == -1) {
                str = (String)new StringBuilder();
                ((StringBuilder)str).append("Unknown AVC level: ");
                ((StringBuilder)str).append(j);
                q.c("MediaCodecUtil", ((StringBuilder)str).toString());
                return null;
            }
            return (Pair<Integer, Integer>)new Pair((Object)b, (Object)c);
        }
        catch (final NumberFormatException ex) {
            final StringBuilder sb3 = new StringBuilder();
            sb3.append("Ignoring malformed AVC codec string: ");
            sb3.append(str);
            q.c("MediaCodecUtil", sb3.toString());
            return null;
        }
    }
    
    @Nullable
    private static Integer c(@Nullable final String s) {
        if (s == null) {
            return null;
        }
        final int hashCode = s.hashCode();
        int n = -1;
        switch (hashCode) {
            case 1570: {
                if (!s.equals("13")) {
                    break;
                }
                n = 12;
                break;
            }
            case 1569: {
                if (!s.equals("12")) {
                    break;
                }
                n = 11;
                break;
            }
            case 1568: {
                if (!s.equals("11")) {
                    break;
                }
                n = 10;
                break;
            }
            case 1567: {
                if (!s.equals("10")) {
                    break;
                }
                n = 9;
                break;
            }
            case 1545: {
                if (!s.equals("09")) {
                    break;
                }
                n = 8;
                break;
            }
            case 1544: {
                if (!s.equals("08")) {
                    break;
                }
                n = 7;
                break;
            }
            case 1543: {
                if (!s.equals("07")) {
                    break;
                }
                n = 6;
                break;
            }
            case 1542: {
                if (!s.equals("06")) {
                    break;
                }
                n = 5;
                break;
            }
            case 1541: {
                if (!s.equals("05")) {
                    break;
                }
                n = 4;
                break;
            }
            case 1540: {
                if (!s.equals("04")) {
                    break;
                }
                n = 3;
                break;
            }
            case 1539: {
                if (!s.equals("03")) {
                    break;
                }
                n = 2;
                break;
            }
            case 1538: {
                if (!s.equals("02")) {
                    break;
                }
                n = 1;
                break;
            }
            case 1537: {
                if (!s.equals("01")) {
                    break;
                }
                n = 0;
                break;
            }
        }
        switch (n) {
            default: {
                return null;
            }
            case 12: {
                return 4096;
            }
            case 11: {
                return 2048;
            }
            case 10: {
                return 1024;
            }
            case 9: {
                return 512;
            }
            case 8: {
                return 256;
            }
            case 7: {
                return 128;
            }
            case 6: {
                return 64;
            }
            case 5: {
                return 32;
            }
            case 4: {
                return 16;
            }
            case 3: {
                return 8;
            }
            case 2: {
                return 4;
            }
            case 1: {
                return 2;
            }
            case 0: {
                return 1;
            }
        }
    }
    
    private static boolean c(final MediaCodecInfo mediaCodecInfo) {
        if (ai.a >= 29) {
            return d(mediaCodecInfo);
        }
        return e(mediaCodecInfo) ^ true;
    }
    
    private static int d(final int n) {
        if (n == 0) {
            return 1;
        }
        if (n == 1) {
            return 2;
        }
        if (n == 2) {
            return 4;
        }
        if (n != 3) {
            return -1;
        }
        return 8;
    }
    
    @Nullable
    private static Pair<Integer, Integer> d(String s, final String[] array) {
        if (array.length < 3) {
            final StringBuilder sb = new StringBuilder();
            sb.append("Ignoring malformed VP9 codec string: ");
            sb.append(s);
            q.c("MediaCodecUtil", sb.toString());
            return null;
        }
        try {
            final int int1 = Integer.parseInt(array[1]);
            final int int2 = Integer.parseInt(array[2]);
            final int d = d(int1);
            if (d == -1) {
                s = (String)new StringBuilder();
                ((StringBuilder)s).append("Unknown VP9 profile: ");
                ((StringBuilder)s).append(int1);
                q.c("MediaCodecUtil", ((StringBuilder)s).toString());
                return null;
            }
            final int e = e(int2);
            if (e == -1) {
                s = (String)new StringBuilder();
                ((StringBuilder)s).append("Unknown VP9 level: ");
                ((StringBuilder)s).append(int2);
                q.c("MediaCodecUtil", ((StringBuilder)s).toString());
                return null;
            }
            return (Pair<Integer, Integer>)new Pair((Object)d, (Object)e);
        }
        catch (final NumberFormatException ex) {
            final StringBuilder sb2 = new StringBuilder();
            sb2.append("Ignoring malformed VP9 codec string: ");
            sb2.append(s);
            q.c("MediaCodecUtil", sb2.toString());
            return null;
        }
    }
    
    @RequiresApi(29)
    private static boolean d(final MediaCodecInfo mediaCodecInfo) {
        return \u30078o8o\u3007.\u3007080(mediaCodecInfo);
    }
    
    private static int e(final int n) {
        if (n == 10) {
            return 1;
        }
        if (n == 11) {
            return 2;
        }
        if (n == 20) {
            return 4;
        }
        if (n == 21) {
            return 8;
        }
        if (n == 30) {
            return 16;
        }
        if (n == 31) {
            return 32;
        }
        if (n == 40) {
            return 64;
        }
        if (n == 41) {
            return 128;
        }
        if (n == 50) {
            return 256;
        }
        if (n == 51) {
            return 512;
        }
        switch (n) {
            default: {
                return -1;
            }
            case 62: {
                return 8192;
            }
            case 61: {
                return 4096;
            }
            case 60: {
                return 2048;
            }
        }
    }
    
    @Nullable
    private static Pair<Integer, Integer> e(final String s, final String[] array) {
        if (array.length != 3) {
            final StringBuilder sb = new StringBuilder();
            sb.append("Ignoring malformed MP4A codec string: ");
            sb.append(s);
            q.c("MediaCodecUtil", sb.toString());
            return null;
        }
        try {
            if ("audio/mp4a-latm".equals(u.a(Integer.parseInt(array[1], 16)))) {
                final int g = g(Integer.parseInt(array[2]));
                if (g != -1) {
                    return (Pair<Integer, Integer>)new Pair((Object)g, (Object)0);
                }
            }
        }
        catch (final NumberFormatException ex) {
            final StringBuilder sb2 = new StringBuilder();
            sb2.append("Ignoring malformed MP4A codec string: ");
            sb2.append(s);
            q.c("MediaCodecUtil", sb2.toString());
        }
        return null;
    }
    
    private static boolean e(final MediaCodecInfo mediaCodecInfo) {
        if (ai.a >= 29) {
            return f(mediaCodecInfo);
        }
        final String lowerCase = Ascii.toLowerCase(mediaCodecInfo.getName());
        final boolean startsWith = lowerCase.startsWith("arc.");
        final boolean b = false;
        if (startsWith) {
            return false;
        }
        if (!lowerCase.startsWith("omx.google.") && !lowerCase.startsWith("omx.ffmpeg.") && (!lowerCase.startsWith("omx.sec.") || !lowerCase.contains(".sw.")) && !lowerCase.equals("omx.qcom.video.decoder.hevcswvdec") && !lowerCase.startsWith("c2.android.") && !lowerCase.startsWith("c2.google.")) {
            boolean b2 = b;
            if (lowerCase.startsWith("omx.")) {
                return b2;
            }
            b2 = b;
            if (lowerCase.startsWith("c2.")) {
                return b2;
            }
        }
        return true;
    }
    
    private static int f(final int n) {
        switch (n) {
            default: {
                return -1;
            }
            case 23: {
                return 8388608;
            }
            case 22: {
                return 4194304;
            }
            case 21: {
                return 2097152;
            }
            case 20: {
                return 1048576;
            }
            case 19: {
                return 524288;
            }
            case 18: {
                return 262144;
            }
            case 17: {
                return 131072;
            }
            case 16: {
                return 65536;
            }
            case 15: {
                return 32768;
            }
            case 14: {
                return 16384;
            }
            case 13: {
                return 8192;
            }
            case 12: {
                return 4096;
            }
            case 11: {
                return 2048;
            }
            case 10: {
                return 1024;
            }
            case 9: {
                return 512;
            }
            case 8: {
                return 256;
            }
            case 7: {
                return 128;
            }
            case 6: {
                return 64;
            }
            case 5: {
                return 32;
            }
            case 4: {
                return 16;
            }
            case 3: {
                return 8;
            }
            case 2: {
                return 4;
            }
            case 1: {
                return 2;
            }
            case 0: {
                return 1;
            }
        }
    }
    
    @RequiresApi(29)
    private static boolean f(final MediaCodecInfo mediaCodecInfo) {
        return \u3007O8o08O.\u3007080(mediaCodecInfo);
    }
    
    private static int g(final int n) {
        int n2 = 17;
        if (n != 17) {
            n2 = 20;
            if (n != 20) {
                n2 = 23;
                if (n != 23) {
                    n2 = 29;
                    if (n != 29) {
                        n2 = 39;
                        if (n != 39) {
                            n2 = 42;
                            if (n != 42) {
                                switch (n) {
                                    default: {
                                        return -1;
                                    }
                                    case 6: {
                                        return 6;
                                    }
                                    case 5: {
                                        return 5;
                                    }
                                    case 4: {
                                        return 4;
                                    }
                                    case 3: {
                                        return 3;
                                    }
                                    case 2: {
                                        return 2;
                                    }
                                    case 1: {
                                        return 1;
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        return n2;
    }
    
    private static boolean g(final MediaCodecInfo mediaCodecInfo) {
        if (ai.a >= 29) {
            return h(mediaCodecInfo);
        }
        final String lowerCase = Ascii.toLowerCase(mediaCodecInfo.getName());
        return !lowerCase.startsWith("omx.google.") && !lowerCase.startsWith("c2.android.") && !lowerCase.startsWith("c2.google.");
    }
    
    @RequiresApi(29)
    private static boolean h(final MediaCodecInfo mediaCodecInfo) {
        return OO0o\u3007\u3007.\u3007080(mediaCodecInfo);
    }
    
    private static final class a
    {
        public final String a;
        public final boolean b;
        public final boolean c;
        
        public a(final String a, final boolean b, final boolean c) {
            this.a = a;
            this.b = b;
            this.c = c;
        }
        
        @Override
        public boolean equals(@Nullable final Object o) {
            boolean b = true;
            if (this == o) {
                return true;
            }
            if (o != null && o.getClass() == a.class) {
                final a a = (a)o;
                if (!TextUtils.equals((CharSequence)this.a, (CharSequence)a.a) || this.b != a.b || this.c != a.c) {
                    b = false;
                }
                return b;
            }
            return false;
        }
        
        @Override
        public int hashCode() {
            final int hashCode = this.a.hashCode();
            final boolean b = this.b;
            int n = 1231;
            int n2;
            if (b) {
                n2 = 1231;
            }
            else {
                n2 = 1237;
            }
            if (!this.c) {
                n = 1237;
            }
            return ((hashCode + 31) * 31 + n2) * 31 + n;
        }
    }
    
    public static class b extends Exception
    {
        private b(final Throwable cause) {
            super("Failed to query underlying media codecs", cause);
        }
    }
    
    private interface c
    {
        int a();
        
        MediaCodecInfo a(final int p0);
        
        boolean a(final String p0, final String p1, final MediaCodecInfo$CodecCapabilities p2);
        
        boolean b();
        
        boolean b(final String p0, final String p1, final MediaCodecInfo$CodecCapabilities p2);
    }
    
    private static final class d implements c
    {
        @Override
        public int a() {
            return MediaCodecList.getCodecCount();
        }
        
        @Override
        public MediaCodecInfo a(final int n) {
            return MediaCodecList.getCodecInfoAt(n);
        }
        
        @Override
        public boolean a(final String anObject, final String anObject2, final MediaCodecInfo$CodecCapabilities mediaCodecInfo$CodecCapabilities) {
            return "secure-playback".equals(anObject) && "video/avc".equals(anObject2);
        }
        
        @Override
        public boolean b() {
            return false;
        }
        
        @Override
        public boolean b(final String s, final String s2, final MediaCodecInfo$CodecCapabilities mediaCodecInfo$CodecCapabilities) {
            return false;
        }
    }
    
    @RequiresApi(21)
    private static final class e implements c
    {
        private final int a;
        @Nullable
        private MediaCodecInfo[] b;
        
        public e(final boolean b, final boolean b2) {
            this.a = ((b || b2) ? 1 : 0);
        }
        
        private void c() {
            if (this.b == null) {
                this.b = new MediaCodecList(this.a).getCodecInfos();
            }
        }
        
        @Override
        public int a() {
            this.c();
            return this.b.length;
        }
        
        @Override
        public MediaCodecInfo a(final int n) {
            this.c();
            return this.b[n];
        }
        
        @Override
        public boolean a(final String s, final String s2, final MediaCodecInfo$CodecCapabilities mediaCodecInfo$CodecCapabilities) {
            return mediaCodecInfo$CodecCapabilities.isFeatureSupported(s);
        }
        
        @Override
        public boolean b() {
            return true;
        }
        
        @Override
        public boolean b(final String s, final String s2, final MediaCodecInfo$CodecCapabilities mediaCodecInfo$CodecCapabilities) {
            return mediaCodecInfo$CodecCapabilities.isFeatureRequired(s);
        }
    }
    
    private interface f<T>
    {
        int getScore(final T p0);
    }
}

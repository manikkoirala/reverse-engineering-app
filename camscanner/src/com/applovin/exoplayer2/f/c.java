// 
// Decompiled by Procyon v0.6.0
// 

package com.applovin.exoplayer2.f;

import androidx.annotation.NonNull;
import com.applovin.exoplayer2.l.ai;
import android.media.MediaCodec;
import com.applovin.exoplayer2.l.a;
import android.media.MediaCodec$CodecException;
import androidx.annotation.Nullable;
import android.media.MediaFormat;
import android.media.MediaCodec$BufferInfo;
import java.util.ArrayDeque;
import androidx.annotation.GuardedBy;
import android.os.Handler;
import android.os.HandlerThread;
import androidx.annotation.RequiresApi;
import android.media.MediaCodec$Callback;

@RequiresApi(23)
final class c extends MediaCodec$Callback
{
    private final Object a;
    private final HandlerThread b;
    private Handler c;
    @GuardedBy("lock")
    private final f d;
    @GuardedBy("lock")
    private final f e;
    @GuardedBy("lock")
    private final ArrayDeque<MediaCodec$BufferInfo> f;
    @GuardedBy("lock")
    private final ArrayDeque<MediaFormat> g;
    @GuardedBy("lock")
    @Nullable
    private MediaFormat h;
    @GuardedBy("lock")
    @Nullable
    private MediaFormat i;
    @GuardedBy("lock")
    @Nullable
    private MediaCodec$CodecException j;
    @GuardedBy("lock")
    private long k;
    @GuardedBy("lock")
    private boolean l;
    @GuardedBy("lock")
    @Nullable
    private IllegalStateException m;
    
    c(final HandlerThread b) {
        this.a = new Object();
        this.b = b;
        this.d = new f();
        this.e = new f();
        this.f = new ArrayDeque<MediaCodec$BufferInfo>();
        this.g = new ArrayDeque<MediaFormat>();
    }
    
    @GuardedBy("lock")
    private void a(final MediaFormat e) {
        this.e.a(-2);
        this.g.add(e);
    }
    
    private void a(final IllegalStateException m) {
        synchronized (this.a) {
            this.m = m;
        }
    }
    
    private void b(final Runnable runnable) {
        synchronized (this.a) {
            this.c(runnable);
        }
    }
    
    @GuardedBy("lock")
    private void c(final Runnable runnable) {
        if (this.l) {
            return;
        }
        final long k = this.k - 1L;
        this.k = k;
        if (k > 0L) {
            return;
        }
        if (k < 0L) {
            this.a(new IllegalStateException());
            return;
        }
        this.d();
        try {
            runnable.run();
        }
        catch (final Exception cause) {
            this.a(new IllegalStateException(cause));
        }
        catch (final IllegalStateException ex) {
            this.a(ex);
        }
    }
    
    @GuardedBy("lock")
    private void d() {
        if (!this.g.isEmpty()) {
            this.i = this.g.getLast();
        }
        this.d.c();
        this.e.c();
        this.f.clear();
        this.g.clear();
        this.j = null;
    }
    
    @GuardedBy("lock")
    private boolean e() {
        return this.k > 0L || this.l;
    }
    
    @GuardedBy("lock")
    private void f() {
        this.g();
        this.h();
    }
    
    @GuardedBy("lock")
    private void g() {
        final IllegalStateException m = this.m;
        if (m == null) {
            return;
        }
        this.m = null;
        throw m;
    }
    
    @GuardedBy("lock")
    private void h() {
        final MediaCodec$CodecException j = this.j;
        if (j == null) {
            return;
        }
        this.j = null;
        throw j;
    }
    
    public int a(final MediaCodec$BufferInfo mediaCodec$BufferInfo) {
        synchronized (this.a) {
            if (this.e()) {
                return -1;
            }
            this.f();
            if (this.e.b()) {
                return -1;
            }
            final int a = this.e.a();
            if (a >= 0) {
                com.applovin.exoplayer2.l.a.a((Object)this.h);
                final MediaCodec$BufferInfo mediaCodec$BufferInfo2 = this.f.remove();
                mediaCodec$BufferInfo.set(mediaCodec$BufferInfo2.offset, mediaCodec$BufferInfo2.size, mediaCodec$BufferInfo2.presentationTimeUs, mediaCodec$BufferInfo2.flags);
            }
            else if (a == -2) {
                this.h = this.g.remove();
            }
            return a;
        }
    }
    
    public void a() {
        synchronized (this.a) {
            this.l = true;
            this.b.quit();
            this.d();
        }
    }
    
    public void a(final MediaCodec mediaCodec) {
        com.applovin.exoplayer2.l.a.b(this.c == null);
        ((Thread)this.b).start();
        final Handler c = new Handler(this.b.getLooper());
        \u3007\u3007888.\u3007080(mediaCodec, (MediaCodec$Callback)this, c);
        this.c = c;
    }
    
    public void a(final Runnable runnable) {
        synchronized (this.a) {
            ++this.k;
            ((Handler)ai.a((Object)this.c)).post((Runnable)new oO80(this, runnable));
        }
    }
    
    public int b() {
        synchronized (this.a) {
            final boolean e = this.e();
            int a = -1;
            if (e) {
                return -1;
            }
            this.f();
            if (!this.d.b()) {
                a = this.d.a();
            }
            return a;
        }
    }
    
    public MediaFormat c() {
        synchronized (this.a) {
            final MediaFormat h = this.h;
            if (h != null) {
                return h;
            }
            throw new IllegalStateException();
        }
    }
    
    public void onError(@NonNull final MediaCodec mediaCodec, @NonNull final MediaCodec$CodecException j) {
        synchronized (this.a) {
            this.j = j;
        }
    }
    
    public void onInputBufferAvailable(@NonNull final MediaCodec mediaCodec, final int n) {
        synchronized (this.a) {
            this.d.a(n);
        }
    }
    
    public void onOutputBufferAvailable(@NonNull final MediaCodec mediaCodec, final int n, @NonNull final MediaCodec$BufferInfo e) {
        synchronized (this.a) {
            final MediaFormat i = this.i;
            if (i != null) {
                this.a(i);
                this.i = null;
            }
            this.e.a(n);
            this.f.add(e);
        }
    }
    
    public void onOutputFormatChanged(@NonNull final MediaCodec mediaCodec, @NonNull final MediaFormat mediaFormat) {
        synchronized (this.a) {
            this.a(mediaFormat);
            this.i = null;
        }
    }
}

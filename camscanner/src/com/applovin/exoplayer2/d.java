// 
// Decompiled by Procyon v0.6.0
// 

package com.applovin.exoplayer2;

public abstract class d implements an
{
    protected final ba.c a;
    
    protected d() {
        this.a = new ba.c();
    }
    
    private void b(long min) {
        final long a = this.I() + min;
        final long h = this.H();
        min = a;
        if (h != -9223372036854775807L) {
            min = Math.min(a, h);
        }
        this.a(Math.max(min, 0L));
    }
    
    private int q() {
        int y;
        if ((y = this.y()) == 1) {
            y = 0;
        }
        return y;
    }
    
    protected an.a a(final an.a a) {
        final an.a.a a2 = new an.a.a().a(a);
        final boolean k = this.K();
        final boolean b = true;
        return a2.a(3, k ^ true).a(4, this.o() && !this.K()).a(5, this.a_() && !this.K()).a(6, !this.S().d() && (this.a_() || !this.n() || this.o()) && !this.K()).a(7, this.h() && !this.K()).a(8, !this.S().d() && (this.h() || (this.n() && this.m())) && !this.K()).a(9, this.K() ^ true).a(10, this.o() && !this.K()).a(11, this.o() && !this.K() && b).a();
    }
    
    @Override
    public final void a(final long n) {
        this.a(this.G(), n);
    }
    
    @Override
    public final boolean a() {
        return this.t() == 3 && this.x() && this.u() == 0;
    }
    
    @Override
    public final boolean a(final int n) {
        return this.s().a(n);
    }
    
    public final boolean a_() {
        return this.l() != -1;
    }
    
    public final void b(final int n) {
        this.a(n, -9223372036854775807L);
    }
    
    public final void b_() {
        this.b(this.G());
    }
    
    @Override
    public final void c() {
        this.b(-this.A());
    }
    
    @Override
    public final void d() {
        this.b(this.B());
    }
    
    public final void f() {
        final int l = this.l();
        if (l != -1) {
            this.b(l);
        }
    }
    
    @Override
    public final void g() {
        if (!this.S().d()) {
            if (!this.K()) {
                final boolean a_ = this.a_();
                if (this.n() && !this.o()) {
                    if (a_) {
                        this.f();
                    }
                }
                else if (a_ && this.I() <= this.C()) {
                    this.f();
                }
                else {
                    this.a(0L);
                }
            }
        }
    }
    
    public final boolean h() {
        return this.k() != -1;
    }
    
    public final void i() {
        final int k = this.k();
        if (k != -1) {
            this.b(k);
        }
    }
    
    @Override
    public final void j() {
        if (!this.S().d()) {
            if (!this.K()) {
                if (this.h()) {
                    this.i();
                }
                else if (this.n() && this.m()) {
                    this.b_();
                }
            }
        }
    }
    
    public final int k() {
        final ba s = this.S();
        int a;
        if (s.d()) {
            a = -1;
        }
        else {
            a = s.a(this.G(), this.q(), this.z());
        }
        return a;
    }
    
    public final int l() {
        final ba s = this.S();
        int b;
        if (s.d()) {
            b = -1;
        }
        else {
            b = s.b(this.G(), this.q(), this.z());
        }
        return b;
    }
    
    public final boolean m() {
        final ba s = this.S();
        return !s.d() && s.a(this.G(), this.a).j;
    }
    
    public final boolean n() {
        final ba s = this.S();
        return !s.d() && s.a(this.G(), this.a).e();
    }
    
    @Override
    public final boolean o() {
        final ba s = this.S();
        return !s.d() && s.a(this.G(), this.a).i;
    }
    
    public final long p() {
        final ba s = this.S();
        long c;
        if (s.d()) {
            c = -9223372036854775807L;
        }
        else {
            c = s.a(this.G(), this.a).c();
        }
        return c;
    }
}

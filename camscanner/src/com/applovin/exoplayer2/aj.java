// 
// Decompiled by Procyon v0.6.0
// 

package com.applovin.exoplayer2;

import android.os.BaseBundle;
import com.applovin.exoplayer2.common.base.Objects;
import androidx.annotation.Nullable;
import android.os.Bundle;
import com.applovin.exoplayer2.l.a;
import androidx.annotation.FloatRange;

public final class aj extends aq
{
    public static final a<aj> a;
    private final float c;
    
    static {
        a = new \u3007\u3007888();
    }
    
    public aj() {
        this.c = -1.0f;
    }
    
    public aj(@FloatRange(from = 0.0, to = 100.0) final float c) {
        com.applovin.exoplayer2.l.a.a(c >= 0.0f && c <= 100.0f, (Object)"percent must be in the range of [0, 100]");
        this.c = c;
    }
    
    private static aj a(final Bundle bundle) {
        boolean b = false;
        if (((BaseBundle)bundle).getInt(a(0), -1) == 1) {
            b = true;
        }
        com.applovin.exoplayer2.l.a.a(b);
        final float float1 = bundle.getFloat(a(1), -1.0f);
        aj aj;
        if (float1 == -1.0f) {
            aj = new aj();
        }
        else {
            aj = new aj(float1);
        }
        return aj;
    }
    
    private static String a(final int i) {
        return Integer.toString(i, 36);
    }
    
    @Override
    public boolean equals(@Nullable final Object o) {
        final boolean b = o instanceof aj;
        boolean b2 = false;
        if (!b) {
            return false;
        }
        if (this.c == ((aj)o).c) {
            b2 = true;
        }
        return b2;
    }
    
    @Override
    public int hashCode() {
        return Objects.hashCode(this.c);
    }
}

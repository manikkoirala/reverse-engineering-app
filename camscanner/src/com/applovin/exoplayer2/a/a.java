// 
// Decompiled by Procyon v0.6.0
// 

package com.applovin.exoplayer2.a;

import com.applovin.exoplayer2.common.a.x;
import java.util.Collection;
import com.applovin.exoplayer2.common.base.Objects;
import com.applovin.exoplayer2.common.a.u;
import com.applovin.exoplayer2.common.a.s;
import java.util.List;
import androidx.annotation.CallSuper;
import android.os.Handler$Callback;
import android.os.Looper;
import com.applovin.exoplayer2.l.p$a;
import com.applovin.exoplayer2.\u3007O8o08O;
import java.io.IOException;
import com.applovin.exoplayer2.h.ad;
import com.applovin.exoplayer2.am;
import com.applovin.exoplayer2.ac;
import com.applovin.exoplayer2.ab;
import androidx.annotation.Nullable;
import com.applovin.exoplayer2.c.h;
import com.applovin.exoplayer2.v;
import com.applovin.exoplayer2.h.j;
import com.applovin.exoplayer2.l.m;
import com.applovin.exoplayer2.c.e;
import com.applovin.exoplayer2.ak;
import com.applovin.exoplayer2.l.p$b;
import com.applovin.exoplayer2.l.ai;
import com.applovin.exoplayer2.l.o;
import com.applovin.exoplayer2.l.p;
import android.util.SparseArray;
import com.applovin.exoplayer2.ba;
import com.applovin.exoplayer2.l.d;
import com.applovin.exoplayer2.m.n;
import com.applovin.exoplayer2.k.d$a;
import com.applovin.exoplayer2.h.q;
import com.applovin.exoplayer2.b.g;
import com.applovin.exoplayer2.an;

public class a implements d, g, com.applovin.exoplayer2.d.g, q, d$a, n
{
    private final com.applovin.exoplayer2.l.d a;
    private final ba.a b;
    private final ba.c c;
    private final a d;
    private final SparseArray<com.applovin.exoplayer2.a.b.a> e;
    private p<com.applovin.exoplayer2.a.b> f;
    private an g;
    private o h;
    private boolean i;
    
    public a(final com.applovin.exoplayer2.l.d d) {
        this.a = (com.applovin.exoplayer2.l.d)com.applovin.exoplayer2.l.a.b((Object)d);
        this.f = (p<com.applovin.exoplayer2.a.b>)new p(ai.c(), d, (p$b)new \u30070\u3007O0088o());
        final ba.a b = new ba.a();
        this.b = b;
        this.c = new ba.c();
        this.d = new a(b);
        this.e = (SparseArray<com.applovin.exoplayer2.a.b.a>)new SparseArray();
    }
    
    private com.applovin.exoplayer2.a.b.a a(@Nullable final com.applovin.exoplayer2.h.p.a a) {
        a.b((Object)this.g);
        ba a2;
        if (a == null) {
            a2 = null;
        }
        else {
            a2 = this.d.a(a);
        }
        if (a != null && a2 != null) {
            return this.a(a2, a2.a(a.a, this.b).c, a);
        }
        final int g = this.g.G();
        ba ba = this.g.S();
        if (g >= ba.b()) {
            ba = com.applovin.exoplayer2.ba.a;
        }
        return this.a(ba, g, null);
    }
    
    private com.applovin.exoplayer2.a.b.a f() {
        return this.a(this.d.b());
    }
    
    private com.applovin.exoplayer2.a.b.a f(final int n, @Nullable final com.applovin.exoplayer2.h.p.a a) {
        a.b((Object)this.g);
        boolean b = true;
        final int n2 = 1;
        if (a != null) {
            int n3;
            if (this.d.a(a) != null) {
                n3 = n2;
            }
            else {
                n3 = 0;
            }
            com.applovin.exoplayer2.a.b.a a2;
            if (n3 != 0) {
                a2 = this.a(a);
            }
            else {
                a2 = this.a(ba.a, n, a);
            }
            return a2;
        }
        ba ba = this.g.S();
        if (n >= ba.b()) {
            b = false;
        }
        if (!b) {
            ba = com.applovin.exoplayer2.ba.a;
        }
        return this.a(ba, n, null);
    }
    
    private com.applovin.exoplayer2.a.b.a g() {
        return this.a(this.d.c());
    }
    
    private com.applovin.exoplayer2.a.b.a h() {
        return this.a(this.d.d());
    }
    
    protected final com.applovin.exoplayer2.a.b.a a(final ba ba, final int n, @Nullable com.applovin.exoplayer2.h.p.a a) {
        if (ba.d()) {
            a = null;
        }
        final long a2 = this.a.a();
        final boolean equals = ba.equals(this.g.S());
        final int n2 = 1;
        final boolean b = equals && n == this.g.G();
        long n3 = 0L;
        if (a != null && a.a()) {
            int n4;
            if (b && this.g.L() == a.b && this.g.M() == a.c) {
                n4 = n2;
            }
            else {
                n4 = 0;
            }
            if (n4 != 0) {
                n3 = this.g.I();
            }
        }
        else if (b) {
            n3 = this.g.N();
        }
        else if (!ba.d()) {
            n3 = ba.a(n, this.c).a();
        }
        return new com.applovin.exoplayer2.a.b.a(a2, ba, n, a, n3, this.g.S(), this.g.G(), this.d.a(), this.g.I(), this.g.J());
    }
    
    @Override
    public final void a(final float n) {
        final com.applovin.exoplayer2.a.b.a g = this.g();
        this.a(g, 1019, (p$a<com.applovin.exoplayer2.a.b>)new OO0o\u3007\u3007\u3007\u30070(g, n));
    }
    
    @Override
    public void a(final int n, final int n2) {
        final com.applovin.exoplayer2.a.b.a g = this.g();
        this.a(g, 1029, (p$a<com.applovin.exoplayer2.a.b>)new oo\u3007(g, n, n2));
    }
    
    public final void a(final int n, final long n2) {
        final com.applovin.exoplayer2.a.b.a f = this.f();
        this.a(f, 1023, (p$a<com.applovin.exoplayer2.a.b>)new O8ooOoo\u3007(f, n, n2));
    }
    
    @Override
    public final void a(final int n, final long n2, final long n3) {
        final com.applovin.exoplayer2.a.b.a g = this.g();
        this.a(g, 1012, (p$a<com.applovin.exoplayer2.a.b>)new OO0o\u3007\u3007(g, n, n2, n3));
    }
    
    @Override
    public final void a(final int n, @Nullable final com.applovin.exoplayer2.h.p.a a) {
        final com.applovin.exoplayer2.a.b.a f = this.f(n, a);
        this.a(f, 1031, (p$a<com.applovin.exoplayer2.a.b>)new \u30078(f));
    }
    
    @Override
    public final void a(final int n, @Nullable final com.applovin.exoplayer2.h.p.a a, final int n2) {
        final com.applovin.exoplayer2.a.b.a f = this.f(n, a);
        this.a(f, 1030, (p$a<com.applovin.exoplayer2.a.b>)new o\u3007O(f, n2));
    }
    
    @Override
    public final void a(final int n, @Nullable final com.applovin.exoplayer2.h.p.a a, final j j, final com.applovin.exoplayer2.h.m m) {
        final com.applovin.exoplayer2.a.b.a f = this.f(n, a);
        this.a(f, 1000, (p$a<com.applovin.exoplayer2.a.b>)new \u300708O8o\u30070(f, j, m));
    }
    
    @Override
    public final void a(final int n, @Nullable final com.applovin.exoplayer2.h.p.a a, final j j, final com.applovin.exoplayer2.h.m m, final IOException ex, final boolean b) {
        final com.applovin.exoplayer2.a.b.a f = this.f(n, a);
        this.a(f, 1003, (p$a<com.applovin.exoplayer2.a.b>)new Oo8Oo00oo(f, j, m, ex, b));
    }
    
    @Override
    public final void a(final int n, @Nullable final com.applovin.exoplayer2.h.p.a a, final com.applovin.exoplayer2.h.m m) {
        final com.applovin.exoplayer2.a.b.a f = this.f(n, a);
        this.a(f, 1004, (p$a<com.applovin.exoplayer2.a.b>)new \u3007\u3007\u30070\u3007\u30070(f, m));
    }
    
    @Override
    public final void a(final int n, @Nullable final com.applovin.exoplayer2.h.p.a a, final Exception ex) {
        final com.applovin.exoplayer2.a.b.a f = this.f(n, a);
        this.a(f, 1032, (p$a<com.applovin.exoplayer2.a.b>)new \u3007o00\u3007\u3007Oo(f, ex));
    }
    
    @Override
    public final void a(final long n) {
        final com.applovin.exoplayer2.a.b.a g = this.g();
        this.a(g, 1011, (p$a<com.applovin.exoplayer2.a.b>)new O\u30078O8\u3007008(g, n));
    }
    
    public final void a(final long n, final int n2) {
        final com.applovin.exoplayer2.a.b.a f = this.f();
        this.a(f, 1026, (p$a<com.applovin.exoplayer2.a.b>)new \u3007o\u3007(f, n, n2));
    }
    
    protected final void a(final com.applovin.exoplayer2.a.b.a a, final int n, final p$a<com.applovin.exoplayer2.a.b> p$a) {
        this.e.put(n, (Object)a);
        this.f.b(n, (p$a)p$a);
    }
    
    @Override
    public final void a(@Nullable final ab ab, final int n) {
        final com.applovin.exoplayer2.a.b.a e = this.e();
        this.a(e, 1, (p$a<com.applovin.exoplayer2.a.b>)new \u3007oOO8O8(e, ab, n));
    }
    
    @Override
    public void a(final ac ac) {
        final com.applovin.exoplayer2.a.b.a e = this.e();
        this.a(e, 14, (p$a<com.applovin.exoplayer2.a.b>)new o\u30070(e, ac));
    }
    
    @Override
    public final void a(final ak ak) {
        com.applovin.exoplayer2.a.b.a a = null;
        Label_0037: {
            if (ak instanceof com.applovin.exoplayer2.p) {
                final com.applovin.exoplayer2.h.o f = ((com.applovin.exoplayer2.p)ak).f;
                if (f != null) {
                    a = this.a(new com.applovin.exoplayer2.h.p.a(f));
                    break Label_0037;
                }
            }
            a = null;
        }
        com.applovin.exoplayer2.a.b.a e = a;
        if (a == null) {
            e = this.e();
        }
        this.a(e, 10, (p$a<com.applovin.exoplayer2.a.b>)new com.applovin.exoplayer2.a.\u3007O8o08O(e, ak));
    }
    
    @Override
    public final void a(final am am) {
        final com.applovin.exoplayer2.a.b.a e = this.e();
        this.a(e, 12, (p$a<com.applovin.exoplayer2.a.b>)new \u3007O888o0o(e, am));
    }
    
    @Override
    public void a(final an.a a) {
        final com.applovin.exoplayer2.a.b.a e = this.e();
        this.a(e, 13, (p$a<com.applovin.exoplayer2.a.b>)new \u3007o(e, a));
    }
    
    @Override
    public final void a(final e e, final e e2, final int n) {
        if (n == 1) {
            this.i = false;
        }
        this.d.a((an)com.applovin.exoplayer2.l.a.b((Object)this.g));
        final com.applovin.exoplayer2.a.b.a e3 = this.e();
        this.a(e3, 11, (p$a<com.applovin.exoplayer2.a.b>)new \u300700(e3, n, e, e2));
    }
    
    @CallSuper
    public void a(final an an, final Looper looper) {
        com.applovin.exoplayer2.l.a.b(this.g == null || this.d.b.isEmpty());
        this.g = (an)com.applovin.exoplayer2.l.a.b((Object)an);
        this.h = this.a.a(looper, (Handler$Callback)null);
        this.f = (p<com.applovin.exoplayer2.a.b>)this.f.a(looper, (p$b)new \u3007oo\u3007(this, an));
    }
    
    @Override
    public final void a(final ba ba, final int n) {
        this.d.b((an)com.applovin.exoplayer2.l.a.b((Object)this.g));
        final com.applovin.exoplayer2.a.b.a e = this.e();
        this.a(e, 0, (p$a<com.applovin.exoplayer2.a.b>)new o\u30078(e, n));
    }
    
    public final void a(final com.applovin.exoplayer2.c.e e) {
        final com.applovin.exoplayer2.a.b.a g = this.g();
        this.a(g, 1020, (p$a<com.applovin.exoplayer2.a.b>)new \u3007\u3007808\u3007(g, e));
    }
    
    @Override
    public final void a(final com.applovin.exoplayer2.g.a a) {
        final com.applovin.exoplayer2.a.b.a e = this.e();
        this.a(e, 1007, (p$a<com.applovin.exoplayer2.a.b>)new oO80(e, a));
    }
    
    @Override
    public final void a(final ad ad, final com.applovin.exoplayer2.j.h h) {
        final com.applovin.exoplayer2.a.b.a e = this.e();
        this.a(e, 2, (p$a<com.applovin.exoplayer2.a.b>)new O8(e, ad, h));
    }
    
    @Override
    public final void a(final com.applovin.exoplayer2.m.o o) {
        final com.applovin.exoplayer2.a.b.a g = this.g();
        this.a(g, 1028, (p$a<com.applovin.exoplayer2.a.b>)new \u30070000OOO(g, o));
    }
    
    public final void a(final v v, @Nullable final h h) {
        final com.applovin.exoplayer2.a.b.a g = this.g();
        this.a(g, 1022, (p$a<com.applovin.exoplayer2.a.b>)new o\u3007O8\u3007\u3007o(g, v, h));
    }
    
    public final void a(final Exception ex) {
        final com.applovin.exoplayer2.a.b.a g = this.g();
        this.a(g, 1038, (p$a<com.applovin.exoplayer2.a.b>)new \u3007080(g, ex));
    }
    
    public final void a(final Object o, final long n) {
        final com.applovin.exoplayer2.a.b.a g = this.g();
        this.a(g, 1027, (p$a<com.applovin.exoplayer2.a.b>)new \u3007\u3007888(g, o, n));
    }
    
    public final void a(final String s) {
        final com.applovin.exoplayer2.a.b.a g = this.g();
        this.a(g, 1024, (p$a<com.applovin.exoplayer2.a.b>)new o800o8O(g, s));
    }
    
    public final void a(final String s, final long n, final long n2) {
        final com.applovin.exoplayer2.a.b.a g = this.g();
        this.a(g, 1021, (p$a<com.applovin.exoplayer2.a.b>)new \u300780\u3007808\u3007O(g, s, n2, n));
    }
    
    public final void a(final List<com.applovin.exoplayer2.h.p.a> list, @Nullable final com.applovin.exoplayer2.h.p.a a) {
        this.d.a(list, a, (an)a.b((Object)this.g));
    }
    
    public final void a(final boolean b, final int n) {
        final com.applovin.exoplayer2.a.b.a e = this.e();
        this.a(e, -1, (p$a<com.applovin.exoplayer2.a.b>)new o0ooO(e, b, n));
    }
    
    @Override
    public final void a_(final boolean b) {
        final com.applovin.exoplayer2.a.b.a g = this.g();
        this.a(g, 1017, (p$a<com.applovin.exoplayer2.a.b>)new \u30078o8o\u3007(g, b));
    }
    
    public final void b() {
        final com.applovin.exoplayer2.a.b.a e = this.e();
        this.a(e, -1, (p$a<com.applovin.exoplayer2.a.b>)new \u3007\u30078O0\u30078(e));
    }
    
    @Override
    public final void b(final int n) {
        final com.applovin.exoplayer2.a.b.a e = this.e();
        this.a(e, 4, (p$a<com.applovin.exoplayer2.a.b>)new \u3007O\u300780o08O(e, n));
    }
    
    public final void b(final int n, final long n2, final long n3) {
        final com.applovin.exoplayer2.a.b.a h = this.h();
        this.a(h, 1006, (p$a<com.applovin.exoplayer2.a.b>)new \u30078\u30070\u3007o\u3007O(h, n, n2, n3));
    }
    
    @Override
    public final void b(final int n, @Nullable final com.applovin.exoplayer2.h.p.a a) {
        final com.applovin.exoplayer2.a.b.a f = this.f(n, a);
        this.a(f, 1033, (p$a<com.applovin.exoplayer2.a.b>)new \u300780(f));
    }
    
    @Override
    public final void b(final int n, @Nullable final com.applovin.exoplayer2.h.p.a a, final j j, final com.applovin.exoplayer2.h.m m) {
        final com.applovin.exoplayer2.a.b.a f = this.f(n, a);
        this.a(f, 1001, (p$a<com.applovin.exoplayer2.a.b>)new O000(f, j, m));
    }
    
    public final void b(final com.applovin.exoplayer2.c.e e) {
        final com.applovin.exoplayer2.a.b.a f = this.f();
        this.a(f, 1025, (p$a<com.applovin.exoplayer2.a.b>)new \u300700\u30078(f, e));
    }
    
    @Override
    public final void b(final v v, @Nullable final h h) {
        final com.applovin.exoplayer2.a.b.a g = this.g();
        this.a(g, 1010, (p$a<com.applovin.exoplayer2.a.b>)new o\u3007\u30070\u3007(g, v, h));
    }
    
    @Override
    public final void b(final Exception ex) {
        final com.applovin.exoplayer2.a.b.a g = this.g();
        this.a(g, 1018, (p$a<com.applovin.exoplayer2.a.b>)new oo88o8O(g, ex));
    }
    
    @Override
    public final void b(final String s) {
        final com.applovin.exoplayer2.a.b.a g = this.g();
        this.a(g, 1013, (p$a<com.applovin.exoplayer2.a.b>)new o\u30070OOo\u30070(g, s));
    }
    
    @Override
    public final void b(final String s, final long n, final long n2) {
        final com.applovin.exoplayer2.a.b.a g = this.g();
        this.a(g, 1009, (p$a<com.applovin.exoplayer2.a.b>)new o8oO\u3007(g, s, n2, n));
    }
    
    @Override
    public final void b(final boolean b, final int n) {
        final com.applovin.exoplayer2.a.b.a e = this.e();
        this.a(e, 5, (p$a<com.applovin.exoplayer2.a.b>)new OOO\u3007O0(e, b, n));
    }
    
    @Override
    public final void b_(final boolean b) {
        final com.applovin.exoplayer2.a.b.a e = this.e();
        this.a(e, 3, (p$a<com.applovin.exoplayer2.a.b>)new \u3007O00(e, b));
    }
    
    @CallSuper
    public void c() {
        final com.applovin.exoplayer2.a.b.a e = this.e();
        this.e.put(1036, (Object)e);
        this.a(e, 1036, (p$a<com.applovin.exoplayer2.a.b>)new ooo\u30078oO(e));
        ((o)com.applovin.exoplayer2.l.a.a((Object)this.h)).a((Runnable)new O0o\u3007\u3007Oo(this));
    }
    
    @Override
    public final void c(final int n) {
        final com.applovin.exoplayer2.a.b.a e = this.e();
        this.a(e, 6, (p$a<com.applovin.exoplayer2.a.b>)new Oo08(e, n));
    }
    
    @Override
    public final void c(final int n, @Nullable final com.applovin.exoplayer2.h.p.a a) {
        final com.applovin.exoplayer2.a.b.a f = this.f(n, a);
        this.a(f, 1034, (p$a<com.applovin.exoplayer2.a.b>)new \u3007\u30070o(f));
    }
    
    @Override
    public final void c(final int n, @Nullable final com.applovin.exoplayer2.h.p.a a, final j j, final com.applovin.exoplayer2.h.m m) {
        final com.applovin.exoplayer2.a.b.a f = this.f(n, a);
        this.a(f, 1002, (p$a<com.applovin.exoplayer2.a.b>)new OoO8(f, j, m));
    }
    
    @Override
    public final void c(final com.applovin.exoplayer2.c.e e) {
        final com.applovin.exoplayer2.a.b.a g = this.g();
        this.a(g, 1008, (p$a<com.applovin.exoplayer2.a.b>)new O\u3007O\u3007oO(g, e));
    }
    
    @Override
    public final void c(final Exception ex) {
        final com.applovin.exoplayer2.a.b.a g = this.g();
        this.a(g, 1037, (p$a<com.applovin.exoplayer2.a.b>)new Ooo(g, ex));
    }
    
    public final void d() {
        if (!this.i) {
            final com.applovin.exoplayer2.a.b.a e = this.e();
            this.i = true;
            this.a(e, -1, (p$a<com.applovin.exoplayer2.a.b>)new O8\u3007o(e));
        }
    }
    
    @Override
    public final void d(final int n) {
        final com.applovin.exoplayer2.a.b.a e = this.e();
        this.a(e, 8, (p$a<com.applovin.exoplayer2.a.b>)new O08000(e, n));
    }
    
    @Override
    public final void d(final int n, @Nullable final com.applovin.exoplayer2.h.p.a a) {
        final com.applovin.exoplayer2.a.b.a f = this.f(n, a);
        this.a(f, 1035, (p$a<com.applovin.exoplayer2.a.b>)new oO(f));
    }
    
    @Override
    public final void d(final com.applovin.exoplayer2.c.e e) {
        final com.applovin.exoplayer2.a.b.a f = this.f();
        this.a(f, 1014, (p$a<com.applovin.exoplayer2.a.b>)new oO00OOO(f, e));
    }
    
    @Override
    public void d(final boolean b) {
        final com.applovin.exoplayer2.a.b.a e = this.e();
        this.a(e, 7, (p$a<com.applovin.exoplayer2.a.b>)new o\u30078oOO88(e, b));
    }
    
    protected final com.applovin.exoplayer2.a.b.a e() {
        return this.a(this.d.a());
    }
    
    @Override
    public final void e(final boolean b) {
        final com.applovin.exoplayer2.a.b.a e = this.e();
        this.a(e, 9, (p$a<com.applovin.exoplayer2.a.b>)new Oooo8o0\u3007(e, b));
    }
    
    private static final class a
    {
        private final ba.a a;
        private s<com.applovin.exoplayer2.h.p.a> b;
        private u<com.applovin.exoplayer2.h.p.a, ba> c;
        @Nullable
        private com.applovin.exoplayer2.h.p.a d;
        private com.applovin.exoplayer2.h.p.a e;
        private com.applovin.exoplayer2.h.p.a f;
        
        public a(final ba.a a) {
            this.a = a;
            this.b = s.g();
            this.c = u.a();
        }
        
        @Nullable
        private static com.applovin.exoplayer2.h.p.a a(final an an, final s<com.applovin.exoplayer2.h.p.a> s, @Nullable final com.applovin.exoplayer2.h.p.a a, final ba.a a2) {
            final ba s2 = an.S();
            final int f = an.F();
            Object a3;
            if (s2.d()) {
                a3 = null;
            }
            else {
                a3 = s2.a(f);
            }
            int b;
            if (!an.K() && !s2.d()) {
                b = s2.a(f, a2).b(com.applovin.exoplayer2.h.b(an.I()) - a2.c());
            }
            else {
                b = -1;
            }
            for (int i = 0; i < s.size(); ++i) {
                final com.applovin.exoplayer2.h.p.a a4 = (com.applovin.exoplayer2.h.p.a)s.get(i);
                if (a(a4, a3, an.K(), an.L(), an.M(), b)) {
                    return a4;
                }
            }
            if (s.isEmpty() && a != null && a(a, a3, an.K(), an.L(), an.M(), b)) {
                return a;
            }
            return null;
        }
        
        private void a(final ba ba) {
            final u.a<com.applovin.exoplayer2.h.p.a, ba> b = u.b();
            if (this.b.isEmpty()) {
                this.a(b, this.e, ba);
                if (!Objects.equal(this.f, this.e)) {
                    this.a(b, this.f, ba);
                }
                if (!Objects.equal(this.d, this.e) && !Objects.equal(this.d, this.f)) {
                    this.a(b, this.d, ba);
                }
            }
            else {
                for (int i = 0; i < this.b.size(); ++i) {
                    this.a(b, (com.applovin.exoplayer2.h.p.a)this.b.get(i), ba);
                }
                if (!this.b.contains(this.d)) {
                    this.a(b, this.d, ba);
                }
            }
            this.c = b.a();
        }
        
        private void a(final u.a<com.applovin.exoplayer2.h.p.a, ba> a, @Nullable final com.applovin.exoplayer2.h.p.a a2, ba ba) {
            if (a2 == null) {
                return;
            }
            if (ba.c(a2.a) != -1) {
                a.a(a2, ba);
            }
            else {
                ba = this.c.get(a2);
                if (ba != null) {
                    a.a(a2, ba);
                }
            }
        }
        
        private static boolean a(final com.applovin.exoplayer2.h.p.a a, @Nullable final Object obj, final boolean b, final int n, final int n2, final int n3) {
            final boolean equals = a.a.equals(obj);
            final boolean b2 = false;
            if (!equals) {
                return false;
            }
            if (!b || a.b != n || a.c != n2) {
                boolean b3 = b2;
                if (b) {
                    return b3;
                }
                b3 = b2;
                if (a.b != -1) {
                    return b3;
                }
                b3 = b2;
                if (a.e != n3) {
                    return b3;
                }
            }
            return true;
        }
        
        @Nullable
        public ba a(final com.applovin.exoplayer2.h.p.a a) {
            return this.c.get(a);
        }
        
        @Nullable
        public com.applovin.exoplayer2.h.p.a a() {
            return this.d;
        }
        
        public void a(final an an) {
            this.d = a(an, this.b, this.e, this.a);
        }
        
        public void a(final List<com.applovin.exoplayer2.h.p.a> list, @Nullable final com.applovin.exoplayer2.h.p.a a, final an an) {
            this.b = s.a((Collection<? extends com.applovin.exoplayer2.h.p.a>)list);
            if (!list.isEmpty()) {
                this.e = list.get(0);
                this.f = (com.applovin.exoplayer2.h.p.a)a.b((Object)a);
            }
            if (this.d == null) {
                this.d = a(an, this.b, this.e, this.a);
            }
            this.a(an.S());
        }
        
        @Nullable
        public com.applovin.exoplayer2.h.p.a b() {
            return this.e;
        }
        
        public void b(final an an) {
            this.d = a(an, this.b, this.e, this.a);
            this.a(an.S());
        }
        
        @Nullable
        public com.applovin.exoplayer2.h.p.a c() {
            return this.f;
        }
        
        @Nullable
        public com.applovin.exoplayer2.h.p.a d() {
            com.applovin.exoplayer2.h.p.a a;
            if (this.b.isEmpty()) {
                a = null;
            }
            else {
                a = x.c(this.b);
            }
            return a;
        }
    }
}

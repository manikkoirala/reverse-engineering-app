// 
// Decompiled by Procyon v0.6.0
// 

package com.applovin.exoplayer2.a;

import android.util.SparseArray;
import com.applovin.exoplayer2.common.base.Objects;
import com.applovin.exoplayer2.h.p;
import com.applovin.exoplayer2.ba;
import com.applovin.exoplayer2.m.o;
import java.io.IOException;
import com.applovin.exoplayer2.h.m;
import com.applovin.exoplayer2.h.j;
import com.applovin.exoplayer2.j.h;
import com.applovin.exoplayer2.h.ad;
import com.applovin.exoplayer2.g.a;
import com.applovin.exoplayer2.an;
import com.applovin.exoplayer2.am;
import com.applovin.exoplayer2.ak;
import com.applovin.exoplayer2.ac;
import androidx.annotation.Nullable;
import com.applovin.exoplayer2.ab;
import com.applovin.exoplayer2.v;
import com.applovin.exoplayer2.c.e;

public interface b
{
    @Deprecated
    void a(final a p0);
    
    void a(final a p0, final float p1);
    
    void a(final a p0, final int p1);
    
    void a(final a p0, final int p1, final int p2);
    
    @Deprecated
    void a(final a p0, final int p1, final int p2, final int p3, final float p4);
    
    void a(final a p0, final int p1, final long p2);
    
    void a(final a p0, final int p1, final long p2, final long p3);
    
    @Deprecated
    void a(final a p0, final int p1, final e p2);
    
    @Deprecated
    void a(final a p0, final int p1, final v p2);
    
    @Deprecated
    void a(final a p0, final int p1, final String p2, final long p3);
    
    void a(final a p0, final long p1);
    
    void a(final a p0, final long p1, final int p2);
    
    void a(final a p0, @Nullable final ab p1, final int p2);
    
    void a(final a p0, final ac p1);
    
    void a(final a p0, final ak p1);
    
    void a(final a p0, final am p1);
    
    void a(final a p0, final an.a p1);
    
    void a(final a p0, final an.e p1, final an.e p2, final int p3);
    
    void a(final a p0, final e p1);
    
    void a(final a p0, final com.applovin.exoplayer2.g.a p1);
    
    void a(final a p0, final ad p1, final h p2);
    
    void a(final a p0, final j p1, final m p2);
    
    void a(final a p0, final j p1, final m p2, final IOException p3, final boolean p4);
    
    void a(final a p0, final m p1);
    
    void a(final a p0, final o p1);
    
    @Deprecated
    void a(final a p0, final v p1);
    
    void a(final a p0, final v p1, @Nullable final com.applovin.exoplayer2.c.h p2);
    
    void a(final a p0, final Exception p1);
    
    void a(final a p0, final Object p1, final long p2);
    
    void a(final a p0, final String p1);
    
    @Deprecated
    void a(final a p0, final String p1, final long p2);
    
    void a(final a p0, final String p1, final long p2, final long p3);
    
    void a(final a p0, final boolean p1);
    
    @Deprecated
    void a(final a p0, final boolean p1, final int p2);
    
    void a(final an p0, final b p1);
    
    @Deprecated
    void b(final a p0);
    
    void b(final a p0, final int p1);
    
    void b(final a p0, final int p1, final long p2, final long p3);
    
    @Deprecated
    void b(final a p0, final int p1, final e p2);
    
    void b(final a p0, final e p1);
    
    void b(final a p0, final j p1, final m p2);
    
    @Deprecated
    void b(final a p0, final v p1);
    
    void b(final a p0, final v p1, @Nullable final com.applovin.exoplayer2.c.h p2);
    
    void b(final a p0, final Exception p1);
    
    void b(final a p0, final String p1);
    
    @Deprecated
    void b(final a p0, final String p1, final long p2);
    
    void b(final a p0, final String p1, final long p2, final long p3);
    
    void b(final a p0, final boolean p1);
    
    void b(final a p0, final boolean p1, final int p2);
    
    @Deprecated
    void c(final a p0);
    
    void c(final a p0, final int p1);
    
    void c(final a p0, final e p1);
    
    void c(final a p0, final j p1, final m p2);
    
    void c(final a p0, final Exception p1);
    
    void c(final a p0, final boolean p1);
    
    void d(final a p0);
    
    @Deprecated
    void d(final a p0, final int p1);
    
    void d(final a p0, final e p1);
    
    void d(final a p0, final Exception p1);
    
    @Deprecated
    void d(final a p0, final boolean p1);
    
    void e(final a p0);
    
    void e(final a p0, final int p1);
    
    void e(final a p0, final boolean p1);
    
    void f(final a p0);
    
    void f(final a p0, final int p1);
    
    void g(final a p0);
    
    void h(final a p0);
    
    public static final class a
    {
        public final long a;
        public final ba b;
        public final int c;
        @Nullable
        public final p.a d;
        public final long e;
        public final ba f;
        public final int g;
        @Nullable
        public final p.a h;
        public final long i;
        public final long j;
        
        public a(final long a, final ba b, final int c, @Nullable final p.a d, final long e, final ba f, final int g, @Nullable final p.a h, final long i, final long j) {
            this.a = a;
            this.b = b;
            this.c = c;
            this.d = d;
            this.e = e;
            this.f = f;
            this.g = g;
            this.h = h;
            this.i = i;
            this.j = j;
        }
        
        @Override
        public boolean equals(@Nullable final Object o) {
            boolean b = true;
            if (this == o) {
                return true;
            }
            if (o != null && a.class == o.getClass()) {
                final a a = (a)o;
                if (this.a != a.a || this.c != a.c || this.e != a.e || this.g != a.g || this.i != a.i || this.j != a.j || !Objects.equal(this.b, a.b) || !Objects.equal(this.d, a.d) || !Objects.equal(this.f, a.f) || !Objects.equal(this.h, a.h)) {
                    b = false;
                }
                return b;
            }
            return false;
        }
        
        @Override
        public int hashCode() {
            return Objects.hashCode(this.a, this.b, this.c, this.d, this.e, this.f, this.g, this.h, this.i, this.j);
        }
    }
    
    public static final class b
    {
        private final com.applovin.exoplayer2.l.m a;
        private final SparseArray<a> b;
        
        public b(final com.applovin.exoplayer2.l.m a, final SparseArray<a> sparseArray) {
            this.a = a;
            final SparseArray b = new SparseArray(a.a());
            for (int i = 0; i < a.a(); ++i) {
                final int b2 = a.b(i);
                b.append(b2, (Object)com.applovin.exoplayer2.l.a.b((Object)sparseArray.get(b2)));
            }
            this.b = (SparseArray<a>)b;
        }
    }
}

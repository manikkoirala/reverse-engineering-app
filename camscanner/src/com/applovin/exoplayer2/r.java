// 
// Decompiled by Procyon v0.6.0
// 

package com.applovin.exoplayer2;

import java.util.Collections;
import android.view.TextureView;
import android.view.SurfaceView;
import com.applovin.exoplayer2.l.m;
import java.util.Iterator;
import com.applovin.exoplayer2.j.g;
import com.applovin.exoplayer2.h.ad;
import android.util.Pair;
import java.util.Collection;
import com.applovin.exoplayer2.l.p$a;
import android.annotation.SuppressLint;
import com.applovin.exoplayer2.k.d$a;
import android.os.Handler;
import android.os.Handler$Callback;
import java.util.ArrayList;
import com.applovin.exoplayer2.l.p$b;
import com.applovin.exoplayer2.l.ai;
import android.os.Looper;
import androidx.annotation.Nullable;
import com.applovin.exoplayer2.a.a;
import java.util.List;
import java.util.concurrent.CopyOnWriteArraySet;
import com.applovin.exoplayer2.l.p;
import com.applovin.exoplayer2.l.o;
import com.applovin.exoplayer2.j.j;
import com.applovin.exoplayer2.j.k;
import com.applovin.exoplayer2.h.z;

final class r extends d
{
    private av A;
    private z B;
    private boolean C;
    private an.a D;
    private ac E;
    private ac F;
    private al G;
    private int H;
    private int I;
    private long J;
    final k b;
    final an.a c;
    private final ar[] d;
    private final j e;
    private final o f;
    private final s.e g;
    private final s h;
    private final p<b> i;
    private final CopyOnWriteArraySet<q.a> j;
    private final ba.a k;
    private final List<a> l;
    private final boolean m;
    private final com.applovin.exoplayer2.h.r n;
    @Nullable
    private final com.applovin.exoplayer2.a.a o;
    private final Looper p;
    private final com.applovin.exoplayer2.k.d q;
    private final long r;
    private final long s;
    private final com.applovin.exoplayer2.l.d t;
    private int u;
    private boolean v;
    private int w;
    private int x;
    private boolean y;
    private int z;
    
    @SuppressLint({ "HandlerLeak" })
    public r(final ar[] array, final j j, final com.applovin.exoplayer2.h.r n, final aa aa, final com.applovin.exoplayer2.k.d q, @Nullable final com.applovin.exoplayer2.a.a o, final boolean m, final av a, final long r, final long s, final com.applovin.exoplayer2.z z, final long n2, final boolean c, final com.applovin.exoplayer2.l.d t, final Looper p17, @Nullable final an an, an.a a2) {
        final StringBuilder sb = new StringBuilder();
        sb.append("Init ");
        sb.append(Integer.toHexString(System.identityHashCode(this)));
        sb.append(" [");
        sb.append("ExoPlayerLib/2.15.1");
        sb.append("] [");
        sb.append(ai.e);
        sb.append("]");
        com.applovin.exoplayer2.l.q.b("ExoPlayerImpl", sb.toString());
        com.applovin.exoplayer2.l.a.b(array.length > 0);
        this.d = (ar[])com.applovin.exoplayer2.l.a.b((Object)array);
        this.e = (j)com.applovin.exoplayer2.l.a.b((Object)j);
        this.n = n;
        this.q = q;
        this.o = o;
        this.m = m;
        this.A = a;
        this.r = r;
        this.s = s;
        this.C = c;
        this.p = p17;
        this.t = t;
        this.u = 0;
        an an2;
        if (an != null) {
            an2 = an;
        }
        else {
            an2 = this;
        }
        this.i = (p<b>)new p(p17, t, (p$b)new o\u3007O(an2));
        this.j = new CopyOnWriteArraySet<q.a>();
        this.l = new ArrayList<a>();
        this.B = new z.a(0);
        final k b = new k(new at[array.length], new com.applovin.exoplayer2.j.d[array.length], (Object)null);
        this.b = b;
        this.k = new ba.a();
        a2 = new an.a.a().a(1, 2, 12, 13, 14, 15, 16, 17, 18, 19).a(28, j.a()).a(a2).a();
        this.c = a2;
        this.D = new an.a.a().a(a2).a(3).a(9).a();
        final ac a3 = ac.a;
        this.E = a3;
        this.F = a3;
        this.H = -1;
        this.f = t.a(p17, (Handler$Callback)null);
        final oO00OOO g = new oO00OOO(this);
        this.g = g;
        this.G = al.a(b);
        if (o != null) {
            o.a(an2, p17);
            this.a(o);
            q.a(new Handler(p17), (d$a)o);
        }
        this.h = new s(array, j, b, aa, q, this.u, this.v, o, a, z, n2, c, p17, t, (s.e)g);
    }
    
    private int W() {
        if (this.G.a.d()) {
            return this.H;
        }
        final al g = this.G;
        return g.a.a(g.b.a, this.k).c;
    }
    
    private void X() {
        final an.a d = this.D;
        final an.a a = this.a(this.c);
        this.D = a;
        if (!a.equals(d)) {
            this.i.a(13, (p$a)new O000(this));
        }
    }
    
    private ba Y() {
        return new ap(this.l, this.B);
    }
    
    private long a(final al al) {
        if (al.a.d()) {
            return com.applovin.exoplayer2.h.b(this.J);
        }
        if (al.b.a()) {
            return al.s;
        }
        return this.a(al.a, al.b, al.s);
    }
    
    private long a(final ba ba, final com.applovin.exoplayer2.h.p.a a, final long n) {
        ba.a(a.a, this.k);
        return n + this.k.c();
    }
    
    private Pair<Boolean, Integer> a(final al al, final al al2, final boolean b, int i, final boolean b2) {
        final ba a = al2.a;
        final ba a2 = al.a;
        final boolean d = a2.d();
        final Integer value = -1;
        if (d && a.d()) {
            return (Pair<Boolean, Integer>)new Pair((Object)Boolean.FALSE, (Object)value);
        }
        final boolean d2 = a2.d();
        final boolean d3 = a.d();
        final int n = 3;
        if (d2 != d3) {
            return (Pair<Boolean, Integer>)new Pair((Object)Boolean.TRUE, (Object)3);
        }
        if (!a.a(a.a(al2.b.a, this.k).c, super.a).b.equals(a2.a(a2.a(al.b.a, this.k).c, super.a).b)) {
            if (b && i == 0) {
                i = 1;
            }
            else if (b && i == 1) {
                i = 2;
            }
            else {
                if (!b2) {
                    throw new IllegalStateException();
                }
                i = n;
            }
            return (Pair<Boolean, Integer>)new Pair((Object)Boolean.TRUE, (Object)i);
        }
        if (b && i == 0 && al2.b.d < al.b.d) {
            return (Pair<Boolean, Integer>)new Pair((Object)Boolean.TRUE, (Object)0);
        }
        return (Pair<Boolean, Integer>)new Pair((Object)Boolean.FALSE, (Object)value);
    }
    
    @Nullable
    private Pair<Object, Long> a(final ba ba, final int h, long a) {
        if (ba.d()) {
            this.H = h;
            long j = a;
            if (a == -9223372036854775807L) {
                j = 0L;
            }
            this.J = j;
            this.I = 0;
            return null;
        }
        int b;
        if (h == -1 || (b = h) >= ba.b()) {
            b = ba.b(this.v);
            a = ba.a(b, super.a).a();
        }
        return ba.a(super.a, this.k, b, h.b(a));
    }
    
    @Nullable
    private Pair<Object, Long> a(final ba ba, final ba ba2) {
        long n = this.N();
        final boolean d = ba.d();
        int w = -1;
        if (d || ba2.d()) {
            final boolean b = !ba.d() && ba2.d();
            if (!b) {
                w = this.W();
            }
            if (b) {
                n = -9223372036854775807L;
            }
            return this.a(ba2, w, n);
        }
        final Pair<Object, Long> a = ba.a(super.a, this.k, this.G(), com.applovin.exoplayer2.h.b(n));
        final Object first = ((Pair)ai.a((Object)a)).first;
        if (ba2.c(first) != -1) {
            return a;
        }
        final Object a2 = com.applovin.exoplayer2.s.a(super.a, this.k, this.u, this.v, first, ba, ba2);
        if (a2 != null) {
            ba2.a(a2, this.k);
            final int c = this.k.c;
            return this.a(ba2, c, ba2.a(c, super.a).a());
        }
        return this.a(ba2, -1, -9223372036854775807L);
    }
    
    private al a(final int n, final int n2) {
        final boolean b = false;
        com.applovin.exoplayer2.l.a.a(n >= 0 && n2 >= n && n2 <= this.l.size());
        final int g = this.G();
        final ba s = this.S();
        final int size = this.l.size();
        ++this.w;
        this.b(n, n2);
        final ba y = this.Y();
        final al a = this.a(this.G, y, this.a(s, y));
        final int e = a.e;
        int n3 = b ? 1 : 0;
        if (e != 1) {
            n3 = (b ? 1 : 0);
            if (e != 4) {
                n3 = (b ? 1 : 0);
                if (n < n2) {
                    n3 = (b ? 1 : 0);
                    if (n2 == size) {
                        n3 = (b ? 1 : 0);
                        if (g >= a.a.b()) {
                            n3 = 1;
                        }
                    }
                }
            }
        }
        al a2 = a;
        if (n3 != 0) {
            a2 = a.a(4);
        }
        this.h.a(n, n2, this.B);
        return a2;
    }
    
    private al a(al al, final ba ba, @Nullable final Pair<Object, Long> pair) {
        com.applovin.exoplayer2.l.a.a(ba.d() || pair != null);
        final ba a = al.a;
        final al a2 = al.a(ba);
        if (ba.d()) {
            final com.applovin.exoplayer2.h.p.a a3 = al.a();
            final long b = com.applovin.exoplayer2.h.b(this.J);
            al = a2.a(a3, b, b, b, 0L, ad.a, this.b, (List<com.applovin.exoplayer2.g.a>)com.applovin.exoplayer2.common.a.s.g()).a(a3);
            al.q = al.s;
            return al;
        }
        final Object a4 = a2.b.a;
        final boolean b2 = a4.equals(((Pair)ai.a((Object)pair)).first) ^ true;
        com.applovin.exoplayer2.h.p.a b3;
        if (b2) {
            b3 = new com.applovin.exoplayer2.h.p.a(pair.first);
        }
        else {
            b3 = a2.b;
        }
        final long longValue = (long)pair.second;
        long b4;
        final long n = b4 = com.applovin.exoplayer2.h.b(this.N());
        if (!a.d()) {
            b4 = n - a.a(a4, this.k).c();
        }
        if (!b2) {
            final long n2 = lcmp(longValue, b4);
            if (n2 >= 0) {
                if (n2 == 0) {
                    final int c = ba.c(a2.k.a);
                    if (c != -1) {
                        al = a2;
                        if (ba.a(c, this.k).c == ba.a(b3.a, this.k).c) {
                            return al;
                        }
                    }
                    ba.a(b3.a, this.k);
                    long q;
                    if (b3.a()) {
                        q = this.k.b(b3.b, b3.c);
                    }
                    else {
                        q = this.k.d;
                    }
                    al = a2.a(b3, a2.s, a2.s, a2.d, q - a2.s, a2.h, a2.i, a2.j).a(b3);
                    al.q = q;
                }
                else {
                    com.applovin.exoplayer2.l.a.b(b3.a() ^ true);
                    final long max = Math.max(0L, a2.r - (longValue - b4));
                    long q2 = a2.q;
                    if (a2.k.equals(a2.b)) {
                        q2 = longValue + max;
                    }
                    al = a2.a(b3, longValue, longValue, longValue, max, a2.h, a2.i, a2.j);
                    al.q = q2;
                }
                return al;
            }
        }
        com.applovin.exoplayer2.l.a.b(b3.a() ^ true);
        ad ad;
        if (b2) {
            ad = com.applovin.exoplayer2.h.ad.a;
        }
        else {
            ad = a2.h;
        }
        k k;
        if (b2) {
            k = this.b;
        }
        else {
            k = a2.i;
        }
        Object o;
        if (b2) {
            o = com.applovin.exoplayer2.common.a.s.g();
        }
        else {
            o = a2.j;
        }
        al = a2.a(b3, longValue, longValue, longValue, 0L, ad, k, (List<com.applovin.exoplayer2.g.a>)o).a(b3);
        al.q = longValue;
        return al;
    }
    
    private e a(final int n, final al al, int c) {
        final ba.a a = new ba.a();
        Object a2;
        int c2;
        Object b;
        Object d;
        if (!al.a.d()) {
            a2 = al.b.a;
            al.a.a(a2, a);
            c2 = a.c;
            c = al.a.c(a2);
            b = al.a.a(c2, super.a).b;
            d = super.a.d;
        }
        else {
            c2 = c;
            b = null;
            d = (a2 = null);
            c = -1;
        }
        long n3 = 0L;
        long n4 = 0L;
        Label_0254: {
            long b3;
            if (n == 0) {
                final long n2 = a.e + a.d;
                if (al.b.a()) {
                    final com.applovin.exoplayer2.h.p.a b2 = al.b;
                    n3 = a.b(b2.b, b2.c);
                    n4 = b(al);
                    break Label_0254;
                }
                b3 = n2;
                if (al.b.e != -1) {
                    b3 = n2;
                    if (this.G.b.a()) {
                        b3 = b(this.G);
                    }
                }
            }
            else {
                if (al.b.a()) {
                    n3 = al.s;
                    n4 = b(al);
                    break Label_0254;
                }
                b3 = a.e + al.s;
            }
            final long n5 = b3;
            n3 = b3;
            n4 = n5;
        }
        final long a3 = com.applovin.exoplayer2.h.a(n3);
        final long a4 = com.applovin.exoplayer2.h.a(n4);
        final com.applovin.exoplayer2.h.p.a b4 = al.b;
        return new an.e(b, c2, (ab)d, a2, c, a3, a4, b4.b, b4.c);
    }
    
    private List<ah.c> a(final int n, final List<com.applovin.exoplayer2.h.p> list) {
        final ArrayList list2 = new ArrayList();
        for (int i = 0; i < list.size(); ++i) {
            final ah.c c = new ah.c(list.get(i), this.m);
            list2.add(c);
            this.l.add(i + n, new a(c.b, c.a.f()));
        }
        this.B = this.B.a(n, list2.size());
        return list2;
    }
    
    private void a(final al g, final int n, final int n2, final boolean b, final boolean b2, final int n3, final long n4, final int n5) {
        final al g2 = this.G;
        this.G = g;
        final Pair<Boolean, Integer> a = this.a(g, g2, b2, n3, g2.a.equals(g.a) ^ true);
        final boolean booleanValue = (boolean)a.first;
        final int intValue = (int)a.second;
        ac ac = this.E;
        ab d = null;
        final ab ab = null;
        if (booleanValue) {
            d = ab;
            if (!g.a.d()) {
                d = g.a.a(g.a.a(g.b.a, this.k).c, super.a).d;
            }
            if (d != null) {
                ac = d.e;
            }
            else {
                ac = com.applovin.exoplayer2.ac.a;
            }
        }
        ac a2 = ac;
        if (!g2.j.equals(g.j)) {
            a2 = ac.a().a(g.j).a();
        }
        final boolean equals = a2.equals(this.E);
        this.E = a2;
        if (!g2.a.equals(g.a)) {
            this.i.a(0, (p$a)new \u300780(g, n));
        }
        if (b2) {
            this.i.a(11, (p$a)new o0O0(n3, this.a(n3, g2, n5), this.c(n4)));
        }
        if (booleanValue) {
            this.i.a(1, (p$a)new Oo8Oo00oo(d, intValue));
        }
        if (g2.f != g.f) {
            this.i.a(10, (p$a)new \u3007\u3007\u30070\u3007\u30070(g));
            if (g.f != null) {
                this.i.a(10, (p$a)new o\u30070OOo\u30070(g));
            }
        }
        final k i = g2.i;
        final k j = g.i;
        if (i != j) {
            this.e.a(j.d);
            this.i.a(2, (p$a)new \u3007\u30070o(g, new com.applovin.exoplayer2.j.h((g[])g.i.c)));
        }
        if (equals ^ true) {
            this.i.a(14, (p$a)new \u300708O8o\u30070(this.E));
        }
        if (g2.g != g.g) {
            this.i.a(3, (p$a)new oO(g));
        }
        if (g2.e != g.e || g2.l != g.l) {
            this.i.a(-1, (p$a)new \u30078(g));
        }
        if (g2.e != g.e) {
            this.i.a(4, (p$a)new O08000(g));
        }
        if (g2.l != g.l) {
            this.i.a(5, (p$a)new Ooo(g, n2));
        }
        if (g2.m != g.m) {
            this.i.a(6, (p$a)new \u3007O\u300780o08O(g));
        }
        if (c(g2) != c(g)) {
            this.i.a(7, (p$a)new ooo\u30078oO(g));
        }
        if (!g2.n.equals(g.n)) {
            this.i.a(12, (p$a)new O0o\u3007\u3007Oo(g));
        }
        if (b) {
            this.i.a(-1, (p$a)new OO8oO0o\u3007());
        }
        this.X();
        this.i.a();
        if (g2.o != g.o) {
            final Iterator<q.a> iterator = this.j.iterator();
            while (iterator.hasNext()) {
                iterator.next().a(g.o);
            }
        }
        if (g2.p != g.p) {
            final Iterator<q.a> iterator2 = this.j.iterator();
            while (iterator2.hasNext()) {
                iterator2.next().b(g.p);
            }
        }
    }
    
    private void a(final s.d d) {
        final int w = this.w - d.b;
        this.w = w;
        final boolean c = d.c;
        final boolean b = true;
        if (c) {
            this.x = d.d;
            this.y = true;
        }
        if (d.e) {
            this.z = d.f;
        }
        if (w == 0) {
            final ba a = d.a.a;
            if (!this.G.a.d() && a.d()) {
                this.H = -1;
                this.J = 0L;
                this.I = 0;
            }
            if (!a.d()) {
                final List<ba> a2 = ((ap)a).a();
                com.applovin.exoplayer2.l.a.b(a2.size() == this.l.size());
                for (int i = 0; i < a2.size(); ++i) {
                    this.l.get(i).b = (ba)a2.get(i);
                }
            }
            boolean b2;
            long n;
            if (this.y) {
                b2 = b;
                if (d.a.b.equals(this.G.b)) {
                    b2 = (d.a.d != this.G.s && b);
                }
                if (b2) {
                    if (!a.d() && !d.a.b.a()) {
                        final al a3 = d.a;
                        n = this.a(a, a3.b, a3.d);
                    }
                    else {
                        n = d.a.d;
                    }
                }
                else {
                    n = -9223372036854775807L;
                }
            }
            else {
                n = -9223372036854775807L;
                b2 = false;
            }
            this.y = false;
            this.a(d.a, 1, this.z, false, b2, this.x, n, -1);
        }
    }
    
    private void a(final List<com.applovin.exoplayer2.h.p> list, int b, long n, final boolean b2) {
        final int w = this.W();
        final long i = this.I();
        final int w2 = this.w;
        final boolean b3 = true;
        this.w = w2 + 1;
        if (!this.l.isEmpty()) {
            this.b(0, this.l.size());
        }
        final List<ah.c> a = this.a(0, list);
        final ba y = this.Y();
        if (!y.d() && b >= y.b()) {
            throw new y(y, b, n);
        }
        if (b2) {
            b = y.b(this.v);
            n = -9223372036854775807L;
        }
        else if (b == -1) {
            b = w;
            n = i;
        }
        final al a2 = this.a(this.G, y, this.a(y, b, n));
        int e = a2.e;
        if (b != -1 && (e = e) != 1) {
            if (!y.d() && b < y.b()) {
                e = 2;
            }
            else {
                e = 4;
            }
        }
        final al a3 = a2.a(e);
        this.h.a(a, b, com.applovin.exoplayer2.h.b(n), this.B);
        this.a(a3, 0, 1, false, !this.G.b.a.equals(a3.b.a) && !this.G.a.d() && b3, 4, this.a(a3), -1);
    }
    
    private static long b(final al al) {
        final ba.c c = new ba.c();
        final ba.a a = new ba.a();
        al.a.a(al.b.a, a);
        long b;
        if (al.c == -9223372036854775807L) {
            b = al.a.a(a.c, c).b();
        }
        else {
            b = a.c() + al.c;
        }
        return b;
    }
    
    private void b(final int n, final int n2) {
        for (int i = n2 - 1; i >= n; --i) {
            this.l.remove(i);
        }
        this.B = this.B.b(n, n2);
    }
    
    private e c(long a) {
        final int g = this.G();
        Object a2;
        int c;
        Object b;
        Object d;
        if (!this.G.a.d()) {
            final al g2 = this.G;
            a2 = g2.b.a;
            g2.a.a(a2, this.k);
            c = this.G.a.c(a2);
            b = this.G.a.a(g, super.a).b;
            d = super.a.d;
        }
        else {
            b = null;
            d = (a2 = null);
            c = -1;
        }
        final long a3 = com.applovin.exoplayer2.h.a(a);
        if (this.G.b.a()) {
            a = com.applovin.exoplayer2.h.a(b(this.G));
        }
        else {
            a = a3;
        }
        final com.applovin.exoplayer2.h.p.a b2 = this.G.b;
        return new an.e(b, g, (ab)d, a2, c, a3, a, b2.b, b2.c);
    }
    
    private static boolean c(final al al) {
        return al.e == 3 && al.l && al.m == 0;
    }
    
    @Override
    public long A() {
        return this.r;
    }
    
    @Override
    public long B() {
        return this.s;
    }
    
    @Override
    public long C() {
        return 3000L;
    }
    
    @Override
    public am D() {
        return this.G.n;
    }
    
    public void E() {
        final StringBuilder sb = new StringBuilder();
        sb.append("Release ");
        sb.append(Integer.toHexString(System.identityHashCode(this)));
        sb.append(" [");
        sb.append("ExoPlayerLib/2.15.1");
        sb.append("] [");
        sb.append(ai.e);
        sb.append("] [");
        sb.append(com.applovin.exoplayer2.t.a());
        sb.append("]");
        com.applovin.exoplayer2.l.q.b("ExoPlayerImpl", sb.toString());
        if (!this.h.c()) {
            this.i.b(10, (p$a)new o\u30078oOO88());
        }
        this.i.b();
        this.f.a((Object)null);
        final com.applovin.exoplayer2.a.a o = this.o;
        if (o != null) {
            this.q.a((d$a)o);
        }
        final al a = this.G.a(1);
        this.G = a;
        final al a2 = a.a(a.b);
        this.G = a2;
        a2.q = a2.s;
        this.G.r = 0L;
    }
    
    @Override
    public int F() {
        if (this.G.a.d()) {
            return this.I;
        }
        final al g = this.G;
        return g.a.c(g.b.a);
    }
    
    @Override
    public int G() {
        int w;
        if ((w = this.W()) == -1) {
            w = 0;
        }
        return w;
    }
    
    @Override
    public long H() {
        if (this.K()) {
            final al g = this.G;
            final com.applovin.exoplayer2.h.p.a b = g.b;
            g.a.a(b.a, this.k);
            return com.applovin.exoplayer2.h.a(this.k.b(b.b, b.c));
        }
        return this.p();
    }
    
    @Override
    public long I() {
        return com.applovin.exoplayer2.h.a(this.a(this.G));
    }
    
    @Override
    public long J() {
        return com.applovin.exoplayer2.h.a(this.G.r);
    }
    
    @Override
    public boolean K() {
        return this.G.b.a();
    }
    
    @Override
    public int L() {
        int b;
        if (this.K()) {
            b = this.G.b.b;
        }
        else {
            b = -1;
        }
        return b;
    }
    
    @Override
    public int M() {
        int c;
        if (this.K()) {
            c = this.G.b.c;
        }
        else {
            c = -1;
        }
        return c;
    }
    
    @Override
    public long N() {
        if (this.K()) {
            final al g = this.G;
            g.a.a(g.b.a, this.k);
            final al g2 = this.G;
            long a;
            if (g2.c == -9223372036854775807L) {
                a = g2.a.a(this.G(), super.a).a();
            }
            else {
                a = this.k.b() + com.applovin.exoplayer2.h.a(this.G.c);
            }
            return a;
        }
        return this.I();
    }
    
    @Override
    public long O() {
        if (this.G.a.d()) {
            return this.J;
        }
        final al g = this.G;
        if (g.k.d != g.b.d) {
            return g.a.a(this.G(), super.a).c();
        }
        long n = g.q;
        if (this.G.k.a()) {
            final al g2 = this.G;
            final ba.a a = g2.a.a(g2.k.a, this.k);
            n = a.a(this.G.k.b);
            if (n == Long.MIN_VALUE) {
                n = a.d;
            }
        }
        final al g3 = this.G;
        return com.applovin.exoplayer2.h.a(this.a(g3.a, g3.k, n));
    }
    
    @Override
    public ad P() {
        return this.G.h;
    }
    
    @Override
    public com.applovin.exoplayer2.j.h Q() {
        return new com.applovin.exoplayer2.j.h((g[])this.G.i.c);
    }
    
    @Override
    public ac R() {
        return this.E;
    }
    
    @Override
    public ba S() {
        return this.G.a;
    }
    
    @Override
    public com.applovin.exoplayer2.m.o T() {
        return com.applovin.exoplayer2.m.o.a;
    }
    
    public com.applovin.exoplayer2.common.a.s<com.applovin.exoplayer2.i.a> U() {
        return com.applovin.exoplayer2.common.a.s.g();
    }
    
    public ao a(final ao.b b) {
        return new ao((ao.a)this.h, b, this.G.a, this.G(), this.t, this.h.d());
    }
    
    @Override
    public void a(final int n, final long n2) {
        final ba a = this.G.a;
        if (n < 0 || (!a.d() && n >= a.b())) {
            throw new y(a, n, n2);
        }
        final int w = this.w;
        int n3 = 1;
        this.w = w + 1;
        if (this.K()) {
            com.applovin.exoplayer2.l.q.c("ExoPlayerImpl", "seekTo ignored because an ad is playing");
            final s.d d = new s.d(this.G);
            d.a(1);
            this.g.onPlaybackInfoUpdate(d);
            return;
        }
        if (this.t() != 1) {
            n3 = 2;
        }
        final int g = this.G();
        final al a2 = this.a(this.G.a(n3), a, this.a(a, n, n2));
        this.h.a(a, n, com.applovin.exoplayer2.h.b(n2));
        this.a(a2, 0, 1, true, true, 1, this.a(a2), g);
    }
    
    @Override
    public void a(@Nullable final SurfaceView surfaceView) {
    }
    
    @Override
    public void a(@Nullable final TextureView textureView) {
    }
    
    public void a(final b b) {
        this.i.a((Object)b);
    }
    
    @Override
    public void a(final an.d d) {
        this.a((b)d);
    }
    
    public void a(final com.applovin.exoplayer2.g.a a) {
        final ac a2 = this.E.a().a(a).a();
        if (a2.equals(this.E)) {
            return;
        }
        this.E = a2;
        this.i.b(14, (p$a)new O\u3007O\u3007oO(this));
    }
    
    public void a(final com.applovin.exoplayer2.h.p o) {
        this.a(Collections.singletonList(o));
    }
    
    public void a(final q.a e) {
        this.j.add(e);
    }
    
    public void a(final List<com.applovin.exoplayer2.h.p> list) {
        this.a(list, true);
    }
    
    public void a(final List<com.applovin.exoplayer2.h.p> list, final boolean b) {
        this.a(list, -1, -9223372036854775807L, b);
    }
    
    @Override
    public void a(final boolean b) {
        this.a(b, 0, 1);
    }
    
    public void a(final boolean b, final int n, final int n2) {
        final al g = this.G;
        if (g.l == b && g.m == n) {
            return;
        }
        ++this.w;
        final al a = g.a(b, n);
        this.h.a(b, n);
        this.a(a, 0, n2, false, false, 5, -9223372036854775807L, -1);
    }
    
    public void a(final boolean b, @Nullable final com.applovin.exoplayer2.p p2) {
        al al;
        if (b) {
            al = this.a(0, this.l.size()).a((com.applovin.exoplayer2.p)null);
        }
        else {
            final al g = this.G;
            al = g.a(g.b);
            al.q = al.s;
            al.r = 0L;
        }
        al al3;
        final al al2 = al3 = al.a(1);
        if (p2 != null) {
            al3 = al2.a(p2);
        }
        ++this.w;
        this.h.b();
        this.a(al3, 0, 1, false, al3.a.d() && !this.G.a.d(), 4, this.a(al3), -1);
    }
    
    public void b(final long n) {
        this.h.a(n);
    }
    
    @Override
    public void b(@Nullable final SurfaceView surfaceView) {
    }
    
    @Override
    public void b(@Nullable final TextureView textureView) {
    }
    
    public void b(final b b) {
        this.i.b((Object)b);
    }
    
    @Override
    public void b(final an.d d) {
        this.b((b)d);
    }
    
    @Override
    public void b(final boolean v) {
        if (this.v != v) {
            this.v = v;
            this.h.a(v);
            this.i.a(9, (p$a)new o\u30078(v));
            this.X();
            this.i.a();
        }
    }
    
    @Override
    public void c(final int u) {
        if (this.u != u) {
            this.u = u;
            this.h.a(u);
            this.i.a(8, (p$a)new \u30078\u30070\u3007o\u3007O(u));
            this.X();
            this.i.a();
        }
    }
    
    public boolean q() {
        return this.G.p;
    }
    
    @Override
    public Looper r() {
        return this.p;
    }
    
    @Override
    public an.a s() {
        return this.D;
    }
    
    @Override
    public int t() {
        return this.G.e;
    }
    
    @Override
    public int u() {
        return this.G.m;
    }
    
    @Nullable
    public com.applovin.exoplayer2.p v() {
        return this.G.f;
    }
    
    @Override
    public void w() {
        final al g = this.G;
        if (g.e != 1) {
            return;
        }
        final al a = g.a((com.applovin.exoplayer2.p)null);
        int n;
        if (a.a.d()) {
            n = 4;
        }
        else {
            n = 2;
        }
        final al a2 = a.a(n);
        ++this.w;
        this.h.a();
        this.a(a2, 1, 1, false, false, 5, -9223372036854775807L, -1);
    }
    
    @Override
    public boolean x() {
        return this.G.l;
    }
    
    @Override
    public int y() {
        return this.u;
    }
    
    @Override
    public boolean z() {
        return this.v;
    }
    
    private static final class a implements ag
    {
        private final Object a;
        private ba b;
        
        public a(final Object a, final ba b) {
            this.a = a;
            this.b = b;
        }
        
        @Override
        public Object a() {
            return this.a;
        }
        
        @Override
        public ba b() {
            return this.b;
        }
    }
}

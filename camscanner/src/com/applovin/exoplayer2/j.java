// 
// Decompiled by Procyon v0.6.0
// 

package com.applovin.exoplayer2;

@Deprecated
public class j implements i
{
    private final long a;
    private final long b;
    private final boolean c;
    
    public j() {
        this.b = -9223372036854775807L;
        this.a = -9223372036854775807L;
        this.c = false;
    }
    
    private static void a(final an an, long min) {
        final long a = an.I() + min;
        final long h = an.H();
        min = a;
        if (h != -9223372036854775807L) {
            min = Math.min(a, h);
        }
        an.a(Math.max(min, 0L));
    }
    
    @Override
    public boolean a() {
        return !this.c || this.a > 0L;
    }
    
    @Override
    public boolean a(final an an) {
        an.w();
        return true;
    }
    
    @Override
    public boolean a(final an an, final int n) {
        an.c(n);
        return true;
    }
    
    @Override
    public boolean a(final an an, final int n, final long n2) {
        an.a(n, n2);
        return true;
    }
    
    @Override
    public boolean a(final an an, final boolean b) {
        an.a(b);
        return true;
    }
    
    @Override
    public boolean b() {
        return !this.c || this.b > 0L;
    }
    
    @Override
    public boolean b(final an an) {
        an.g();
        return true;
    }
    
    @Override
    public boolean b(final an an, final boolean b) {
        an.b(b);
        return true;
    }
    
    @Override
    public boolean c(final an an) {
        an.j();
        return true;
    }
    
    @Override
    public boolean d(final an an) {
        if (!this.c) {
            an.c();
        }
        else if (this.a() && an.o()) {
            a(an, -this.a);
        }
        return true;
    }
    
    @Override
    public boolean e(final an an) {
        if (!this.c) {
            an.d();
        }
        else if (this.b() && an.o()) {
            a(an, this.b);
        }
        return true;
    }
}

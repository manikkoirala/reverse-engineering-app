// 
// Decompiled by Procyon v0.6.0
// 

package com.applovin.exoplayer2.common.base;

import org.checkerframework.checker.nullness.compatqual.NullableDecl;

public interface Function<F, T>
{
    @NullableDecl
    T apply(@NullableDecl final F p0);
    
    boolean equals(@NullableDecl final Object p0);
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.applovin.exoplayer2.common.base;

import java.util.logging.Level;
import java.util.logging.Logger;
import org.checkerframework.checker.nullness.compatqual.NullableDecl;

public final class Strings
{
    private Strings() {
    }
    
    public static String commonPrefix(final CharSequence charSequence, final CharSequence charSequence2) {
        Preconditions.checkNotNull(charSequence);
        Preconditions.checkNotNull(charSequence2);
        int min;
        int n;
        for (min = Math.min(charSequence.length(), charSequence2.length()), n = 0; n < min && charSequence.charAt(n) == charSequence2.charAt(n); ++n) {}
        final int n2 = n - 1;
        if (!validSurrogatePairAt(charSequence, n2)) {
            final int n3 = n;
            if (!validSurrogatePairAt(charSequence2, n2)) {
                return charSequence.subSequence(0, n3).toString();
            }
        }
        final int n3 = n - 1;
        return charSequence.subSequence(0, n3).toString();
    }
    
    public static String commonSuffix(final CharSequence charSequence, final CharSequence charSequence2) {
        Preconditions.checkNotNull(charSequence);
        Preconditions.checkNotNull(charSequence2);
        int min;
        int n;
        for (min = Math.min(charSequence.length(), charSequence2.length()), n = 0; n < min && charSequence.charAt(charSequence.length() - n - 1) == charSequence2.charAt(charSequence2.length() - n - 1); ++n) {}
        if (!validSurrogatePairAt(charSequence, charSequence.length() - n - 1)) {
            final int n2 = n;
            if (!validSurrogatePairAt(charSequence2, charSequence2.length() - n - 1)) {
                return charSequence.subSequence(charSequence.length() - n2, charSequence.length()).toString();
            }
        }
        final int n2 = n - 1;
        return charSequence.subSequence(charSequence.length() - n2, charSequence.length()).toString();
    }
    
    public static String lenientFormat(@NullableDecl final String obj, @NullableDecl final Object... array) {
        final String value = String.valueOf(obj);
        final int n = 0;
        Object[] array2;
        if (array == null) {
            array2 = new Object[] { "(Object[])null" };
        }
        else {
            int n2 = 0;
            while (true) {
                array2 = array;
                if (n2 >= array.length) {
                    break;
                }
                array[n2] = lenientToString(array[n2]);
                ++n2;
            }
        }
        final StringBuilder sb = new StringBuilder(value.length() + array2.length * 16);
        int start = 0;
        int i;
        for (i = n; i < array2.length; ++i) {
            final int index = value.indexOf("%s", start);
            if (index == -1) {
                break;
            }
            sb.append(value, start, index);
            sb.append(array2[i]);
            start = index + 2;
        }
        sb.append(value, start, value.length());
        if (i < array2.length) {
            sb.append(" [");
            final int n3 = i + 1;
            sb.append(array2[i]);
            for (int j = n3; j < array2.length; ++j) {
                sb.append(", ");
                sb.append(array2[j]);
            }
            sb.append(']');
        }
        return sb.toString();
    }
    
    private static String lenientToString(@NullableDecl final Object obj) {
        try {
            return String.valueOf(obj);
        }
        catch (final Exception thrown) {
            final StringBuilder sb = new StringBuilder();
            sb.append(obj.getClass().getName());
            sb.append('@');
            sb.append(Integer.toHexString(System.identityHashCode(obj)));
            final String string = sb.toString();
            final Logger logger = Logger.getLogger("com.applovin.exoplayer2.common.base.Strings");
            final Level warning = Level.WARNING;
            final StringBuilder sb2 = new StringBuilder();
            sb2.append("Exception during lenientFormat for ");
            sb2.append(string);
            logger.log(warning, sb2.toString(), thrown);
            final StringBuilder sb3 = new StringBuilder();
            sb3.append("<");
            sb3.append(string);
            sb3.append(" threw ");
            sb3.append(thrown.getClass().getName());
            sb3.append(">");
            return sb3.toString();
        }
    }
    
    public static String padEnd(final String str, final int capacity, final char c) {
        Preconditions.checkNotNull(str);
        if (str.length() >= capacity) {
            return str;
        }
        final StringBuilder sb = new StringBuilder(capacity);
        sb.append(str);
        for (int i = str.length(); i < capacity; ++i) {
            sb.append(c);
        }
        return sb.toString();
    }
    
    public static String padStart(final String str, final int capacity, final char c) {
        Preconditions.checkNotNull(str);
        if (str.length() >= capacity) {
            return str;
        }
        final StringBuilder sb = new StringBuilder(capacity);
        for (int i = str.length(); i < capacity; ++i) {
            sb.append(c);
        }
        sb.append(str);
        return sb.toString();
    }
    
    public static String repeat(String s, int n) {
        Preconditions.checkNotNull(s);
        boolean b = false;
        if (n <= 1) {
            if (n >= 0) {
                b = true;
            }
            Preconditions.checkArgument(b, "invalid count: %s", n);
            if (n == 0) {
                s = "";
            }
            return s;
        }
        final int length = s.length();
        final long lng = length * (long)n;
        final int n2 = (int)lng;
        if (n2 == lng) {
            final char[] array = new char[n2];
            s.getChars(0, length, array, 0);
            n = length;
            int n3;
            while (true) {
                n3 = n2 - n;
                if (n >= n3) {
                    break;
                }
                System.arraycopy(array, 0, array, n, n);
                n <<= 1;
            }
            System.arraycopy(array, 0, array, n, n3);
            return new String(array);
        }
        final StringBuilder sb = new StringBuilder();
        sb.append("Required array size too large: ");
        sb.append(lng);
        throw new ArrayIndexOutOfBoundsException(sb.toString());
    }
    
    static boolean validSurrogatePairAt(final CharSequence charSequence, final int n) {
        if (n >= 0 && n <= charSequence.length() - 2 && Character.isHighSurrogate(charSequence.charAt(n))) {
            final boolean b = true;
            if (Character.isLowSurrogate(charSequence.charAt(n + 1))) {
                return b;
            }
        }
        return false;
    }
}

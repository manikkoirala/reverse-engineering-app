// 
// Decompiled by Procyon v0.6.0
// 

package com.applovin.exoplayer2.common.base;

import org.checkerframework.checker.nullness.compatqual.NonNullDecl;
import org.checkerframework.checker.nullness.compatqual.NullableDecl;

public final class Preconditions
{
    private Preconditions() {
    }
    
    private static String badElementIndex(final int n, final int n2, @NullableDecl final String s) {
        if (n < 0) {
            return Strings.lenientFormat("%s (%s) must not be negative", s, n);
        }
        if (n2 >= 0) {
            return Strings.lenientFormat("%s (%s) must be less than size (%s)", s, n, n2);
        }
        final StringBuilder sb = new StringBuilder();
        sb.append("negative size: ");
        sb.append(n2);
        throw new IllegalArgumentException(sb.toString());
    }
    
    private static String badPositionIndex(final int n, final int n2, @NullableDecl final String s) {
        if (n < 0) {
            return Strings.lenientFormat("%s (%s) must not be negative", s, n);
        }
        if (n2 >= 0) {
            return Strings.lenientFormat("%s (%s) must not be greater than size (%s)", s, n, n2);
        }
        final StringBuilder sb = new StringBuilder();
        sb.append("negative size: ");
        sb.append(n2);
        throw new IllegalArgumentException(sb.toString());
    }
    
    private static String badPositionIndexes(final int i, final int j, final int n) {
        if (i < 0 || i > n) {
            return badPositionIndex(i, n, "start index");
        }
        if (j >= 0 && j <= n) {
            return Strings.lenientFormat("end index (%s) must not be less than start index (%s)", j, i);
        }
        return badPositionIndex(j, n, "end index");
    }
    
    public static void checkArgument(final boolean b) {
        if (b) {
            return;
        }
        throw new IllegalArgumentException();
    }
    
    public static void checkArgument(final boolean b, @NullableDecl final Object obj) {
        if (b) {
            return;
        }
        throw new IllegalArgumentException(String.valueOf(obj));
    }
    
    public static void checkArgument(final boolean b, @NullableDecl final String s, final char c) {
        if (b) {
            return;
        }
        throw new IllegalArgumentException(Strings.lenientFormat(s, c));
    }
    
    public static void checkArgument(final boolean b, @NullableDecl final String s, final char c, final char c2) {
        if (b) {
            return;
        }
        throw new IllegalArgumentException(Strings.lenientFormat(s, c, c2));
    }
    
    public static void checkArgument(final boolean b, @NullableDecl final String s, final char c, final int i) {
        if (b) {
            return;
        }
        throw new IllegalArgumentException(Strings.lenientFormat(s, c, i));
    }
    
    public static void checkArgument(final boolean b, @NullableDecl final String s, final char c, final long l) {
        if (b) {
            return;
        }
        throw new IllegalArgumentException(Strings.lenientFormat(s, c, l));
    }
    
    public static void checkArgument(final boolean b, @NullableDecl final String s, final char c, @NullableDecl final Object o) {
        if (b) {
            return;
        }
        throw new IllegalArgumentException(Strings.lenientFormat(s, c, o));
    }
    
    public static void checkArgument(final boolean b, @NullableDecl final String s, final int i) {
        if (b) {
            return;
        }
        throw new IllegalArgumentException(Strings.lenientFormat(s, i));
    }
    
    public static void checkArgument(final boolean b, @NullableDecl final String s, final int i, final char c) {
        if (b) {
            return;
        }
        throw new IllegalArgumentException(Strings.lenientFormat(s, i, c));
    }
    
    public static void checkArgument(final boolean b, @NullableDecl final String s, final int i, final int j) {
        if (b) {
            return;
        }
        throw new IllegalArgumentException(Strings.lenientFormat(s, i, j));
    }
    
    public static void checkArgument(final boolean b, @NullableDecl final String s, final int i, final long l) {
        if (b) {
            return;
        }
        throw new IllegalArgumentException(Strings.lenientFormat(s, i, l));
    }
    
    public static void checkArgument(final boolean b, @NullableDecl final String s, final int i, @NullableDecl final Object o) {
        if (b) {
            return;
        }
        throw new IllegalArgumentException(Strings.lenientFormat(s, i, o));
    }
    
    public static void checkArgument(final boolean b, @NullableDecl final String s, final long l) {
        if (b) {
            return;
        }
        throw new IllegalArgumentException(Strings.lenientFormat(s, l));
    }
    
    public static void checkArgument(final boolean b, @NullableDecl final String s, final long l, final char c) {
        if (b) {
            return;
        }
        throw new IllegalArgumentException(Strings.lenientFormat(s, l, c));
    }
    
    public static void checkArgument(final boolean b, @NullableDecl final String s, final long l, final int i) {
        if (b) {
            return;
        }
        throw new IllegalArgumentException(Strings.lenientFormat(s, l, i));
    }
    
    public static void checkArgument(final boolean b, @NullableDecl final String s, final long l, final long i) {
        if (b) {
            return;
        }
        throw new IllegalArgumentException(Strings.lenientFormat(s, l, i));
    }
    
    public static void checkArgument(final boolean b, @NullableDecl final String s, final long l, @NullableDecl final Object o) {
        if (b) {
            return;
        }
        throw new IllegalArgumentException(Strings.lenientFormat(s, l, o));
    }
    
    public static void checkArgument(final boolean b, @NullableDecl final String s, @NullableDecl final Object o) {
        if (b) {
            return;
        }
        throw new IllegalArgumentException(Strings.lenientFormat(s, o));
    }
    
    public static void checkArgument(final boolean b, @NullableDecl final String s, @NullableDecl final Object o, final char c) {
        if (b) {
            return;
        }
        throw new IllegalArgumentException(Strings.lenientFormat(s, o, c));
    }
    
    public static void checkArgument(final boolean b, @NullableDecl final String s, @NullableDecl final Object o, final int i) {
        if (b) {
            return;
        }
        throw new IllegalArgumentException(Strings.lenientFormat(s, o, i));
    }
    
    public static void checkArgument(final boolean b, @NullableDecl final String s, @NullableDecl final Object o, final long l) {
        if (b) {
            return;
        }
        throw new IllegalArgumentException(Strings.lenientFormat(s, o, l));
    }
    
    public static void checkArgument(final boolean b, @NullableDecl final String s, @NullableDecl final Object o, @NullableDecl final Object o2) {
        if (b) {
            return;
        }
        throw new IllegalArgumentException(Strings.lenientFormat(s, o, o2));
    }
    
    public static void checkArgument(final boolean b, @NullableDecl final String s, @NullableDecl final Object o, @NullableDecl final Object o2, @NullableDecl final Object o3) {
        if (b) {
            return;
        }
        throw new IllegalArgumentException(Strings.lenientFormat(s, o, o2, o3));
    }
    
    public static void checkArgument(final boolean b, @NullableDecl final String s, @NullableDecl final Object o, @NullableDecl final Object o2, @NullableDecl final Object o3, @NullableDecl final Object o4) {
        if (b) {
            return;
        }
        throw new IllegalArgumentException(Strings.lenientFormat(s, o, o2, o3, o4));
    }
    
    public static void checkArgument(final boolean b, @NullableDecl final String s, @NullableDecl final Object... array) {
        if (b) {
            return;
        }
        throw new IllegalArgumentException(Strings.lenientFormat(s, array));
    }
    
    public static int checkElementIndex(final int n, final int n2) {
        return checkElementIndex(n, n2, "index");
    }
    
    public static int checkElementIndex(final int n, final int n2, @NullableDecl final String s) {
        if (n >= 0 && n < n2) {
            return n;
        }
        throw new IndexOutOfBoundsException(badElementIndex(n, n2, s));
    }
    
    @NonNullDecl
    public static <T> T checkNotNull(@NonNullDecl final T t) {
        t.getClass();
        return t;
    }
    
    @NonNullDecl
    public static <T> T checkNotNull(@NonNullDecl final T t, @NullableDecl final Object obj) {
        if (t != null) {
            return t;
        }
        throw new NullPointerException(String.valueOf(obj));
    }
    
    @NonNullDecl
    public static <T> T checkNotNull(@NonNullDecl final T t, @NullableDecl final String s, final char c) {
        if (t != null) {
            return t;
        }
        throw new NullPointerException(Strings.lenientFormat(s, c));
    }
    
    @NonNullDecl
    public static <T> T checkNotNull(@NonNullDecl final T t, @NullableDecl final String s, final char c, final char c2) {
        if (t != null) {
            return t;
        }
        throw new NullPointerException(Strings.lenientFormat(s, c, c2));
    }
    
    @NonNullDecl
    public static <T> T checkNotNull(@NonNullDecl final T t, @NullableDecl final String s, final char c, final int i) {
        if (t != null) {
            return t;
        }
        throw new NullPointerException(Strings.lenientFormat(s, c, i));
    }
    
    @NonNullDecl
    public static <T> T checkNotNull(@NonNullDecl final T t, @NullableDecl final String s, final char c, final long l) {
        if (t != null) {
            return t;
        }
        throw new NullPointerException(Strings.lenientFormat(s, c, l));
    }
    
    @NonNullDecl
    public static <T> T checkNotNull(@NonNullDecl final T t, @NullableDecl final String s, final char c, @NullableDecl final Object o) {
        if (t != null) {
            return t;
        }
        throw new NullPointerException(Strings.lenientFormat(s, c, o));
    }
    
    @NonNullDecl
    public static <T> T checkNotNull(@NonNullDecl final T t, @NullableDecl final String s, final int i) {
        if (t != null) {
            return t;
        }
        throw new NullPointerException(Strings.lenientFormat(s, i));
    }
    
    @NonNullDecl
    public static <T> T checkNotNull(@NonNullDecl final T t, @NullableDecl final String s, final int i, final char c) {
        if (t != null) {
            return t;
        }
        throw new NullPointerException(Strings.lenientFormat(s, i, c));
    }
    
    @NonNullDecl
    public static <T> T checkNotNull(@NonNullDecl final T t, @NullableDecl final String s, final int i, final int j) {
        if (t != null) {
            return t;
        }
        throw new NullPointerException(Strings.lenientFormat(s, i, j));
    }
    
    @NonNullDecl
    public static <T> T checkNotNull(@NonNullDecl final T t, @NullableDecl final String s, final int i, final long l) {
        if (t != null) {
            return t;
        }
        throw new NullPointerException(Strings.lenientFormat(s, i, l));
    }
    
    @NonNullDecl
    public static <T> T checkNotNull(@NonNullDecl final T t, @NullableDecl final String s, final int i, @NullableDecl final Object o) {
        if (t != null) {
            return t;
        }
        throw new NullPointerException(Strings.lenientFormat(s, i, o));
    }
    
    @NonNullDecl
    public static <T> T checkNotNull(@NonNullDecl final T t, @NullableDecl final String s, final long l) {
        if (t != null) {
            return t;
        }
        throw new NullPointerException(Strings.lenientFormat(s, l));
    }
    
    @NonNullDecl
    public static <T> T checkNotNull(@NonNullDecl final T t, @NullableDecl final String s, final long l, final char c) {
        if (t != null) {
            return t;
        }
        throw new NullPointerException(Strings.lenientFormat(s, l, c));
    }
    
    @NonNullDecl
    public static <T> T checkNotNull(@NonNullDecl final T t, @NullableDecl final String s, final long l, final int i) {
        if (t != null) {
            return t;
        }
        throw new NullPointerException(Strings.lenientFormat(s, l, i));
    }
    
    @NonNullDecl
    public static <T> T checkNotNull(@NonNullDecl final T t, @NullableDecl final String s, final long l, final long i) {
        if (t != null) {
            return t;
        }
        throw new NullPointerException(Strings.lenientFormat(s, l, i));
    }
    
    @NonNullDecl
    public static <T> T checkNotNull(@NonNullDecl final T t, @NullableDecl final String s, final long l, @NullableDecl final Object o) {
        if (t != null) {
            return t;
        }
        throw new NullPointerException(Strings.lenientFormat(s, l, o));
    }
    
    @NonNullDecl
    public static <T> T checkNotNull(@NonNullDecl final T t, @NullableDecl final String s, @NullableDecl final Object o) {
        if (t != null) {
            return t;
        }
        throw new NullPointerException(Strings.lenientFormat(s, o));
    }
    
    @NonNullDecl
    public static <T> T checkNotNull(@NonNullDecl final T t, @NullableDecl final String s, @NullableDecl final Object o, final char c) {
        if (t != null) {
            return t;
        }
        throw new NullPointerException(Strings.lenientFormat(s, o, c));
    }
    
    @NonNullDecl
    public static <T> T checkNotNull(@NonNullDecl final T t, @NullableDecl final String s, @NullableDecl final Object o, final int i) {
        if (t != null) {
            return t;
        }
        throw new NullPointerException(Strings.lenientFormat(s, o, i));
    }
    
    @NonNullDecl
    public static <T> T checkNotNull(@NonNullDecl final T t, @NullableDecl final String s, @NullableDecl final Object o, final long l) {
        if (t != null) {
            return t;
        }
        throw new NullPointerException(Strings.lenientFormat(s, o, l));
    }
    
    @NonNullDecl
    public static <T> T checkNotNull(@NonNullDecl final T t, @NullableDecl final String s, @NullableDecl final Object o, @NullableDecl final Object o2) {
        if (t != null) {
            return t;
        }
        throw new NullPointerException(Strings.lenientFormat(s, o, o2));
    }
    
    @NonNullDecl
    public static <T> T checkNotNull(@NonNullDecl final T t, @NullableDecl final String s, @NullableDecl final Object o, @NullableDecl final Object o2, @NullableDecl final Object o3) {
        if (t != null) {
            return t;
        }
        throw new NullPointerException(Strings.lenientFormat(s, o, o2, o3));
    }
    
    @NonNullDecl
    public static <T> T checkNotNull(@NonNullDecl final T t, @NullableDecl final String s, @NullableDecl final Object o, @NullableDecl final Object o2, @NullableDecl final Object o3, @NullableDecl final Object o4) {
        if (t != null) {
            return t;
        }
        throw new NullPointerException(Strings.lenientFormat(s, o, o2, o3, o4));
    }
    
    @NonNullDecl
    public static <T> T checkNotNull(@NonNullDecl final T t, @NullableDecl final String s, @NullableDecl final Object... array) {
        if (t != null) {
            return t;
        }
        throw new NullPointerException(Strings.lenientFormat(s, array));
    }
    
    public static int checkPositionIndex(final int n, final int n2) {
        return checkPositionIndex(n, n2, "index");
    }
    
    public static int checkPositionIndex(final int n, final int n2, @NullableDecl final String s) {
        if (n >= 0 && n <= n2) {
            return n;
        }
        throw new IndexOutOfBoundsException(badPositionIndex(n, n2, s));
    }
    
    public static void checkPositionIndexes(final int n, final int n2, final int n3) {
        if (n >= 0 && n2 >= n && n2 <= n3) {
            return;
        }
        throw new IndexOutOfBoundsException(badPositionIndexes(n, n2, n3));
    }
    
    public static void checkState(final boolean b) {
        if (b) {
            return;
        }
        throw new IllegalStateException();
    }
    
    public static void checkState(final boolean b, @NullableDecl final Object obj) {
        if (b) {
            return;
        }
        throw new IllegalStateException(String.valueOf(obj));
    }
    
    public static void checkState(final boolean b, @NullableDecl final String s, final char c) {
        if (b) {
            return;
        }
        throw new IllegalStateException(Strings.lenientFormat(s, c));
    }
    
    public static void checkState(final boolean b, @NullableDecl final String s, final char c, final char c2) {
        if (b) {
            return;
        }
        throw new IllegalStateException(Strings.lenientFormat(s, c, c2));
    }
    
    public static void checkState(final boolean b, @NullableDecl final String s, final char c, final int i) {
        if (b) {
            return;
        }
        throw new IllegalStateException(Strings.lenientFormat(s, c, i));
    }
    
    public static void checkState(final boolean b, @NullableDecl final String s, final char c, final long l) {
        if (b) {
            return;
        }
        throw new IllegalStateException(Strings.lenientFormat(s, c, l));
    }
    
    public static void checkState(final boolean b, @NullableDecl final String s, final char c, @NullableDecl final Object o) {
        if (b) {
            return;
        }
        throw new IllegalStateException(Strings.lenientFormat(s, c, o));
    }
    
    public static void checkState(final boolean b, @NullableDecl final String s, final int i) {
        if (b) {
            return;
        }
        throw new IllegalStateException(Strings.lenientFormat(s, i));
    }
    
    public static void checkState(final boolean b, @NullableDecl final String s, final int i, final char c) {
        if (b) {
            return;
        }
        throw new IllegalStateException(Strings.lenientFormat(s, i, c));
    }
    
    public static void checkState(final boolean b, @NullableDecl final String s, final int i, final int j) {
        if (b) {
            return;
        }
        throw new IllegalStateException(Strings.lenientFormat(s, i, j));
    }
    
    public static void checkState(final boolean b, @NullableDecl final String s, final int i, final long l) {
        if (b) {
            return;
        }
        throw new IllegalStateException(Strings.lenientFormat(s, i, l));
    }
    
    public static void checkState(final boolean b, @NullableDecl final String s, final int i, @NullableDecl final Object o) {
        if (b) {
            return;
        }
        throw new IllegalStateException(Strings.lenientFormat(s, i, o));
    }
    
    public static void checkState(final boolean b, @NullableDecl final String s, final long l) {
        if (b) {
            return;
        }
        throw new IllegalStateException(Strings.lenientFormat(s, l));
    }
    
    public static void checkState(final boolean b, @NullableDecl final String s, final long l, final char c) {
        if (b) {
            return;
        }
        throw new IllegalStateException(Strings.lenientFormat(s, l, c));
    }
    
    public static void checkState(final boolean b, @NullableDecl final String s, final long l, final int i) {
        if (b) {
            return;
        }
        throw new IllegalStateException(Strings.lenientFormat(s, l, i));
    }
    
    public static void checkState(final boolean b, @NullableDecl final String s, final long l, final long i) {
        if (b) {
            return;
        }
        throw new IllegalStateException(Strings.lenientFormat(s, l, i));
    }
    
    public static void checkState(final boolean b, @NullableDecl final String s, final long l, @NullableDecl final Object o) {
        if (b) {
            return;
        }
        throw new IllegalStateException(Strings.lenientFormat(s, l, o));
    }
    
    public static void checkState(final boolean b, @NullableDecl final String s, @NullableDecl final Object o) {
        if (b) {
            return;
        }
        throw new IllegalStateException(Strings.lenientFormat(s, o));
    }
    
    public static void checkState(final boolean b, @NullableDecl final String s, @NullableDecl final Object o, final char c) {
        if (b) {
            return;
        }
        throw new IllegalStateException(Strings.lenientFormat(s, o, c));
    }
    
    public static void checkState(final boolean b, @NullableDecl final String s, @NullableDecl final Object o, final int i) {
        if (b) {
            return;
        }
        throw new IllegalStateException(Strings.lenientFormat(s, o, i));
    }
    
    public static void checkState(final boolean b, @NullableDecl final String s, @NullableDecl final Object o, final long l) {
        if (b) {
            return;
        }
        throw new IllegalStateException(Strings.lenientFormat(s, o, l));
    }
    
    public static void checkState(final boolean b, @NullableDecl final String s, @NullableDecl final Object o, @NullableDecl final Object o2) {
        if (b) {
            return;
        }
        throw new IllegalStateException(Strings.lenientFormat(s, o, o2));
    }
    
    public static void checkState(final boolean b, @NullableDecl final String s, @NullableDecl final Object o, @NullableDecl final Object o2, @NullableDecl final Object o3) {
        if (b) {
            return;
        }
        throw new IllegalStateException(Strings.lenientFormat(s, o, o2, o3));
    }
    
    public static void checkState(final boolean b, @NullableDecl final String s, @NullableDecl final Object o, @NullableDecl final Object o2, @NullableDecl final Object o3, @NullableDecl final Object o4) {
        if (b) {
            return;
        }
        throw new IllegalStateException(Strings.lenientFormat(s, o, o2, o3, o4));
    }
    
    public static void checkState(final boolean b, @NullableDecl final String s, @NullableDecl final Object... array) {
        if (b) {
            return;
        }
        throw new IllegalStateException(Strings.lenientFormat(s, array));
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.applovin.exoplayer2.common.base;

import java.util.Arrays;
import java.util.AbstractList;
import java.io.Writer;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.lang.reflect.InvocationTargetException;
import java.util.Collections;
import java.util.ArrayList;
import java.util.List;
import java.lang.reflect.Method;

public final class Throwables
{
    private static final String JAVA_LANG_ACCESS_CLASSNAME = "sun.misc.JavaLangAccess";
    static final String SHARED_SECRETS_CLASSNAME = "sun.misc.SharedSecrets";
    private static final Method getStackTraceDepthMethod;
    private static final Method getStackTraceElementMethod;
    private static final Object jla;
    
    static {
        final Object o = jla = getJLA();
        final Method method = null;
        Method getMethod;
        if (o == null) {
            getMethod = null;
        }
        else {
            getMethod = getGetMethod();
        }
        getStackTraceElementMethod = getMethod;
        Method sizeMethod;
        if (o == null) {
            sizeMethod = method;
        }
        else {
            sizeMethod = getSizeMethod();
        }
        getStackTraceDepthMethod = sizeMethod;
    }
    
    private Throwables() {
    }
    
    public static List<Throwable> getCausalChain(Throwable t) {
        Preconditions.checkNotNull(t);
        final ArrayList list = new ArrayList(4);
        list.add(t);
        int n = 0;
        final Throwable t2 = t;
        Throwable cause = t;
        t = t2;
        while (true) {
            cause = cause.getCause();
            if (cause == null) {
                return (List<Throwable>)Collections.unmodifiableList((List<?>)list);
            }
            list.add(cause);
            if (cause == t) {
                throw new IllegalArgumentException("Loop in causal chain detected.", cause);
            }
            Throwable cause2 = t;
            if (n != 0) {
                cause2 = t.getCause();
            }
            n ^= 0x1;
            t = cause2;
        }
    }
    
    public static <X extends Throwable> X getCauseAs(final Throwable cause, final Class<X> clazz) {
        try {
            return clazz.cast(cause.getCause());
        }
        catch (final ClassCastException ex) {
            ex.initCause(cause);
            throw ex;
        }
    }
    
    private static Method getGetMethod() {
        return getJlaMethod("getStackTraceElement", Throwable.class, Integer.TYPE);
    }
    
    private static Object getJLA() {
        Object invoke = null;
        try {
            invoke = Class.forName("sun.misc.SharedSecrets", false, null).getMethod("getJavaLangAccess", (Class<?>[])new Class[0]).invoke(null, new Object[0]);
            return invoke;
        }
        catch (final ThreadDeath invoke) {
            throw invoke;
        }
        finally {
            return invoke;
        }
    }
    
    private static Method getJlaMethod(final String name, final Class<?>... parameterTypes) throws ThreadDeath {
        try {
            return Class.forName("sun.misc.JavaLangAccess", false, null).getMethod(name, parameterTypes);
        }
        catch (final ThreadDeath threadDeath) {
            throw threadDeath;
        }
        finally {
            return null;
        }
    }
    
    public static Throwable getRootCause(Throwable t) {
        int n = 0;
        final Throwable t2 = t;
        Throwable t3 = t;
        t = t2;
        while (true) {
            final Throwable cause = t3.getCause();
            if (cause == null) {
                return t3;
            }
            if (cause == t) {
                throw new IllegalArgumentException("Loop in causal chain detected.", cause);
            }
            Throwable cause2 = t;
            if (n != 0) {
                cause2 = t.getCause();
            }
            n ^= 0x1;
            t = cause2;
            t3 = cause;
        }
    }
    
    private static Method getSizeMethod() {
        try {
            final Method jlaMethod = getJlaMethod("getStackTraceDepth", Throwable.class);
            if (jlaMethod == null) {
                return null;
            }
            jlaMethod.invoke(getJLA(), new Throwable());
            return jlaMethod;
        }
        catch (final UnsupportedOperationException | IllegalAccessException | InvocationTargetException ex) {
            return null;
        }
    }
    
    public static String getStackTraceAsString(final Throwable t) {
        final StringWriter out = new StringWriter();
        t.printStackTrace(new PrintWriter(out));
        return out.toString();
    }
    
    private static Object invokeAccessibleNonThrowingMethod(final Method method, final Object obj, final Object... args) {
        try {
            return method.invoke(obj, args);
        }
        catch (final InvocationTargetException ex) {
            throw propagate(ex.getCause());
        }
        catch (final IllegalAccessException cause) {
            throw new RuntimeException(cause);
        }
    }
    
    private static List<StackTraceElement> jlaStackTrace(final Throwable t) {
        Preconditions.checkNotNull(t);
        return new AbstractList<StackTraceElement>(t) {
            final Throwable a;
            
            public StackTraceElement a(final int i) {
                return (StackTraceElement)invokeAccessibleNonThrowingMethod(Throwables.getStackTraceElementMethod, Throwables.jla, new Object[] { this.a, i });
            }
            
            @Override
            public int size() {
                return (int)invokeAccessibleNonThrowingMethod(Throwables.getStackTraceDepthMethod, Throwables.jla, new Object[] { this.a });
            }
        };
    }
    
    public static List<StackTraceElement> lazyStackTrace(final Throwable t) {
        Object o;
        if (lazyStackTraceIsLazy()) {
            o = jlaStackTrace(t);
        }
        else {
            o = Collections.unmodifiableList((List<?>)Arrays.asList((T[])t.getStackTrace()));
        }
        return (List<StackTraceElement>)o;
    }
    
    public static boolean lazyStackTraceIsLazy() {
        return Throwables.getStackTraceElementMethod != null && Throwables.getStackTraceDepthMethod != null;
    }
    
    @Deprecated
    public static RuntimeException propagate(final Throwable cause) {
        throwIfUnchecked(cause);
        throw new RuntimeException(cause);
    }
    
    @Deprecated
    public static <X extends Throwable> void propagateIfInstanceOf(final Throwable t, final Class<X> clazz) throws X, Throwable {
        if (t != null) {
            throwIfInstanceOf(t, (Class<Throwable>)clazz);
        }
    }
    
    @Deprecated
    public static void propagateIfPossible(final Throwable t) {
        if (t != null) {
            throwIfUnchecked(t);
        }
    }
    
    public static <X extends Throwable> void propagateIfPossible(final Throwable t, final Class<X> clazz) throws X, Throwable {
        propagateIfInstanceOf(t, (Class<Throwable>)clazz);
        propagateIfPossible(t);
    }
    
    public static <X1 extends Throwable, X2 extends Throwable> void propagateIfPossible(final Throwable t, final Class<X1> clazz, final Class<X2> clazz2) throws X1, X2, Throwable {
        Preconditions.checkNotNull(clazz2);
        propagateIfInstanceOf(t, clazz);
        propagateIfPossible(t, clazz2);
    }
    
    public static <X extends Throwable> void throwIfInstanceOf(final Throwable obj, final Class<X> clazz) throws X, Throwable {
        Preconditions.checkNotNull(obj);
        if (!clazz.isInstance(obj)) {
            return;
        }
        throw (Throwable)clazz.cast(obj);
    }
    
    public static void throwIfUnchecked(final Throwable t) {
        Preconditions.checkNotNull(t);
        if (t instanceof RuntimeException) {
            throw (RuntimeException)t;
        }
        if (!(t instanceof Error)) {
            return;
        }
        throw (Error)t;
    }
}

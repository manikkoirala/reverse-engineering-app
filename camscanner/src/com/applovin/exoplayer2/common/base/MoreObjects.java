// 
// Decompiled by Procyon v0.6.0
// 

package com.applovin.exoplayer2.common.base;

import java.util.Arrays;
import org.checkerframework.checker.nullness.compatqual.NullableDecl;

public final class MoreObjects
{
    private MoreObjects() {
    }
    
    public static <T> T firstNonNull(@NullableDecl final T t, @NullableDecl final T t2) {
        if (t != null) {
            return t;
        }
        if (t2 != null) {
            return t2;
        }
        throw new NullPointerException("Both parameters are null");
    }
    
    public static ToStringHelper toStringHelper(final Class<?> clazz) {
        return new ToStringHelper(clazz.getSimpleName());
    }
    
    public static ToStringHelper toStringHelper(final Object o) {
        return new ToStringHelper(o.getClass().getSimpleName());
    }
    
    public static ToStringHelper toStringHelper(final String s) {
        return new ToStringHelper(s);
    }
    
    public static final class ToStringHelper
    {
        private final String className;
        private final a holderHead;
        private a holderTail;
        private boolean omitNullValues;
        
        private ToStringHelper(final String s) {
            final a a = new a();
            this.holderHead = a;
            this.holderTail = a;
            this.omitNullValues = false;
            this.className = Preconditions.checkNotNull(s);
        }
        
        private a addHolder() {
            final a a = new a();
            this.holderTail.c = a;
            return this.holderTail = a;
        }
        
        private ToStringHelper addHolder(@NullableDecl final Object b) {
            this.addHolder().b = b;
            return this;
        }
        
        private ToStringHelper addHolder(final String s, @NullableDecl final Object b) {
            final a addHolder = this.addHolder();
            addHolder.b = b;
            addHolder.a = Preconditions.checkNotNull(s);
            return this;
        }
        
        public ToStringHelper add(final String s, final char c) {
            return this.addHolder(s, String.valueOf(c));
        }
        
        public ToStringHelper add(final String s, final double d) {
            return this.addHolder(s, String.valueOf(d));
        }
        
        public ToStringHelper add(final String s, final float f) {
            return this.addHolder(s, String.valueOf(f));
        }
        
        public ToStringHelper add(final String s, final int i) {
            return this.addHolder(s, String.valueOf(i));
        }
        
        public ToStringHelper add(final String s, final long l) {
            return this.addHolder(s, String.valueOf(l));
        }
        
        public ToStringHelper add(final String s, @NullableDecl final Object o) {
            return this.addHolder(s, o);
        }
        
        public ToStringHelper add(final String s, final boolean b) {
            return this.addHolder(s, String.valueOf(b));
        }
        
        public ToStringHelper addValue(final char c) {
            return this.addHolder(String.valueOf(c));
        }
        
        public ToStringHelper addValue(final double d) {
            return this.addHolder(String.valueOf(d));
        }
        
        public ToStringHelper addValue(final float f) {
            return this.addHolder(String.valueOf(f));
        }
        
        public ToStringHelper addValue(final int i) {
            return this.addHolder(String.valueOf(i));
        }
        
        public ToStringHelper addValue(final long l) {
            return this.addHolder(String.valueOf(l));
        }
        
        public ToStringHelper addValue(@NullableDecl final Object o) {
            return this.addHolder(o);
        }
        
        public ToStringHelper addValue(final boolean b) {
            return this.addHolder(String.valueOf(b));
        }
        
        public ToStringHelper omitNullValues() {
            this.omitNullValues = true;
            return this;
        }
        
        @Override
        public String toString() {
            final boolean omitNullValues = this.omitNullValues;
            final StringBuilder sb = new StringBuilder(32);
            sb.append(this.className);
            sb.append('{');
            a a = this.holderHead.c;
            String str = "";
            while (a != null) {
                final Object b = a.b;
                String s = null;
                Label_0156: {
                    if (omitNullValues) {
                        s = str;
                        if (b == null) {
                            break Label_0156;
                        }
                    }
                    sb.append(str);
                    final String a2 = a.a;
                    if (a2 != null) {
                        sb.append(a2);
                        sb.append('=');
                    }
                    if (b != null && b.getClass().isArray()) {
                        final String deepToString = Arrays.deepToString(new Object[] { b });
                        sb.append(deepToString, 1, deepToString.length() - 1);
                    }
                    else {
                        sb.append(b);
                    }
                    s = ", ";
                }
                a = a.c;
                str = s;
            }
            sb.append('}');
            return sb.toString();
        }
        
        private static final class a
        {
            @NullableDecl
            String a;
            @NullableDecl
            Object b;
            @NullableDecl
            a c;
        }
    }
}

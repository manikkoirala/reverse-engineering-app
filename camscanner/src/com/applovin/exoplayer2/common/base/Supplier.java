// 
// Decompiled by Procyon v0.6.0
// 

package com.applovin.exoplayer2.common.base;

public interface Supplier<T>
{
    T get();
}

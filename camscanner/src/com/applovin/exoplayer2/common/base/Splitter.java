// 
// Decompiled by Procyon v0.6.0
// 

package com.applovin.exoplayer2.common.base;

import java.util.Collections;
import java.util.ArrayList;
import java.util.List;
import java.util.Iterator;

public final class Splitter
{
    private final int limit;
    private final boolean omitEmptyStrings;
    private final b strategy;
    private final CharMatcher trimmer;
    
    private Splitter(final b b) {
        this(b, false, CharMatcher.none(), Integer.MAX_VALUE);
    }
    
    private Splitter(final b strategy, final boolean omitEmptyStrings, final CharMatcher trimmer, final int limit) {
        this.strategy = strategy;
        this.omitEmptyStrings = omitEmptyStrings;
        this.trimmer = trimmer;
        this.limit = limit;
    }
    
    public static Splitter fixedLength(final int n) {
        Preconditions.checkArgument(n > 0, (Object)"The length may not be less than 1");
        return new Splitter((b)new b(n) {
            final int a;
            
            public a a(final Splitter splitter, final CharSequence charSequence) {
                return new a(this, splitter, charSequence) {
                    final Splitter$3 a;
                    
                    public int a(int n) {
                        n += this.a.a;
                        if (n >= super.b.length()) {
                            n = -1;
                        }
                        return n;
                    }
                    
                    public int b(final int n) {
                        return n;
                    }
                };
            }
        });
    }
    
    public static Splitter on(final char c) {
        return on(CharMatcher.is(c));
    }
    
    public static Splitter on(final CharMatcher charMatcher) {
        Preconditions.checkNotNull(charMatcher);
        return new Splitter((b)new b(charMatcher) {
            final CharMatcher a;
            
            public a a(final Splitter splitter, final CharSequence charSequence) {
                return new a(this, splitter, charSequence) {
                    final Splitter$1 a;
                    
                    @Override
                    int a(final int n) {
                        return this.a.a.indexIn(super.b, n);
                    }
                    
                    @Override
                    int b(final int n) {
                        return n + 1;
                    }
                };
            }
        });
    }
    
    public static Splitter on(final String s) {
        Preconditions.checkArgument(s.length() != 0, (Object)"The separator may not be the empty string.");
        if (s.length() == 1) {
            return on(s.charAt(0));
        }
        return new Splitter((b)new b(s) {
            final String a;
            
            public a a(final Splitter splitter, final CharSequence charSequence) {
                return new a(this, splitter, charSequence) {
                    final Splitter$2 a;
                    
                    public int a(int i) {
                        final int length = this.a.a.length();
                        final int length2 = super.b.length();
                    Label_0022:
                        while (i <= length2 - length) {
                            for (int j = 0; j < length; ++j) {
                                if (super.b.charAt(j + i) != this.a.a.charAt(j)) {
                                    ++i;
                                    continue Label_0022;
                                }
                            }
                            return i;
                        }
                        return -1;
                    }
                    
                    public int b(final int n) {
                        return n + this.a.a.length();
                    }
                };
            }
        });
    }
    
    private Iterator<String> splittingIterator(final CharSequence charSequence) {
        return this.strategy.b(this, charSequence);
    }
    
    public Splitter limit(final int n) {
        Preconditions.checkArgument(n > 0, "must be greater than zero: %s", n);
        return new Splitter(this.strategy, this.omitEmptyStrings, this.trimmer, n);
    }
    
    public Splitter omitEmptyStrings() {
        return new Splitter(this.strategy, true, this.trimmer, this.limit);
    }
    
    public List<String> splitToList(final CharSequence charSequence) {
        Preconditions.checkNotNull(charSequence);
        final Iterator<String> splittingIterator = this.splittingIterator(charSequence);
        final ArrayList list = new ArrayList();
        while (splittingIterator.hasNext()) {
            list.add(splittingIterator.next());
        }
        return (List<String>)Collections.unmodifiableList((List<?>)list);
    }
    
    public Splitter trimResults() {
        return this.trimResults(CharMatcher.whitespace());
    }
    
    public Splitter trimResults(final CharMatcher charMatcher) {
        Preconditions.checkNotNull(charMatcher);
        return new Splitter(this.strategy, this.omitEmptyStrings, charMatcher, this.limit);
    }
    
    private abstract static class a extends b<String>
    {
        final CharSequence b;
        final CharMatcher c;
        final boolean d;
        int e;
        int f;
        
        protected a(final Splitter splitter, final CharSequence b) {
            this.e = 0;
            this.c = splitter.trimmer;
            this.d = splitter.omitEmptyStrings;
            this.f = splitter.limit;
            this.b = b;
        }
        
        abstract int a(final int p0);
        
        abstract int b(final int p0);
        
        protected String c() {
            int n = this.e;
            while (true) {
                final int e = this.e;
                if (e == -1) {
                    return this.b();
                }
                int n2 = this.a(e);
                if (n2 == -1) {
                    n2 = this.b.length();
                    this.e = -1;
                }
                else {
                    this.e = this.b(n2);
                }
                final int e2 = this.e;
                int i = n;
                if (e2 == n) {
                    if ((this.e = e2 + 1) <= this.b.length()) {
                        continue;
                    }
                    this.e = -1;
                }
                else {
                    int n3;
                    while (i < (n3 = n2)) {
                        n3 = n2;
                        if (!this.c.matches(this.b.charAt(i))) {
                            break;
                        }
                        ++i;
                    }
                    while (n3 > i && this.c.matches(this.b.charAt(n3 - 1))) {
                        --n3;
                    }
                    if (!this.d || i != n3) {
                        final int f = this.f;
                        int n4;
                        if (f == 1) {
                            int length = this.b.length();
                            this.e = -1;
                            while (true) {
                                n4 = length;
                                if (length <= i) {
                                    break;
                                }
                                n4 = length;
                                if (!this.c.matches(this.b.charAt(length - 1))) {
                                    break;
                                }
                                --length;
                            }
                        }
                        else {
                            this.f = f - 1;
                            n4 = n3;
                        }
                        return this.b.subSequence(i, n4).toString();
                    }
                    n = this.e;
                }
            }
        }
    }
    
    private interface b
    {
        Iterator<String> b(final Splitter p0, final CharSequence p1);
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.applovin.exoplayer2.common.base;

import java.io.Serializable;
import java.util.Iterator;
import org.checkerframework.checker.nullness.compatqual.NullableDecl;
import org.checkerframework.checker.nullness.compatqual.MonotonicNonNullDecl;

public abstract class Converter<A, B> implements Function<A, B>
{
    private final boolean handleNullAutomatically;
    @MonotonicNonNullDecl
    private transient Converter<B, A> reverse;
    
    protected Converter() {
        this(true);
    }
    
    Converter(final boolean handleNullAutomatically) {
        this.handleNullAutomatically = handleNullAutomatically;
    }
    
    public static <A, B> Converter<A, B> from(final Function<? super A, ? extends B> function, final Function<? super B, ? extends A> function2) {
        return new b<A, B>((Function)function, (Function)function2);
    }
    
    public static <T> Converter<T, T> identity() {
        return c.a;
    }
    
    public final <C> Converter<A, C> andThen(final Converter<B, C> converter) {
        return (Converter<A, C>)this.doAndThen((Converter<B, Object>)converter);
    }
    
    @Deprecated
    @NullableDecl
    @Override
    public final B apply(@NullableDecl final A a) {
        return this.convert(a);
    }
    
    @NullableDecl
    public final B convert(@NullableDecl final A a) {
        return this.correctedDoForward(a);
    }
    
    public Iterable<B> convertAll(final Iterable<? extends A> iterable) {
        Preconditions.checkNotNull(iterable, (Object)"fromIterable");
        return new Iterable<B>(this, iterable) {
            final Iterable a;
            final Converter b;
            
            @Override
            public Iterator<B> iterator() {
                return new Iterator<B>(this) {
                    final Converter$1 a;
                    private final Iterator<? extends A> b = a.a.iterator();
                    
                    @Override
                    public boolean hasNext() {
                        return this.b.hasNext();
                    }
                    
                    @Override
                    public B next() {
                        return this.a.b.convert(this.b.next());
                    }
                    
                    @Override
                    public void remove() {
                        this.b.remove();
                    }
                };
            }
        };
    }
    
    @NullableDecl
    A correctedDoBackward(@NullableDecl final B b) {
        if (this.handleNullAutomatically) {
            A checkNotNull;
            if (b == null) {
                checkNotNull = null;
            }
            else {
                checkNotNull = Preconditions.checkNotNull(this.doBackward(b));
            }
            return checkNotNull;
        }
        return this.doBackward(b);
    }
    
    @NullableDecl
    B correctedDoForward(@NullableDecl final A a) {
        if (this.handleNullAutomatically) {
            B checkNotNull;
            if (a == null) {
                checkNotNull = null;
            }
            else {
                checkNotNull = Preconditions.checkNotNull(this.doForward(a));
            }
            return checkNotNull;
        }
        return this.doForward(a);
    }
    
     <C> Converter<A, C> doAndThen(final Converter<B, C> converter) {
        return (Converter<A, C>)new a((Converter<Object, Object>)this, (Converter<Object, Object>)Preconditions.checkNotNull(converter));
    }
    
    protected abstract A doBackward(final B p0);
    
    protected abstract B doForward(final A p0);
    
    @Override
    public boolean equals(@NullableDecl final Object obj) {
        return super.equals(obj);
    }
    
    public Converter<B, A> reverse() {
        Converter<B, A> reverse;
        if ((reverse = this.reverse) == null) {
            reverse = (Converter<B, A>)new d((Converter<Object, Object>)this);
            this.reverse = reverse;
        }
        return reverse;
    }
    
    private static final class a<A, B, C> extends Converter<A, C> implements Serializable
    {
        final Converter<A, B> a;
        final Converter<B, C> b;
        
        a(final Converter<A, B> a, final Converter<B, C> b) {
            this.a = a;
            this.b = b;
        }
        
        @NullableDecl
        @Override
        A correctedDoBackward(@NullableDecl final C c) {
            return this.a.correctedDoBackward(this.b.correctedDoBackward(c));
        }
        
        @NullableDecl
        @Override
        C correctedDoForward(@NullableDecl final A a) {
            return this.b.correctedDoForward(this.a.correctedDoForward(a));
        }
        
        @Override
        protected A doBackward(final C c) {
            throw new AssertionError();
        }
        
        @Override
        protected C doForward(final A a) {
            throw new AssertionError();
        }
        
        @Override
        public boolean equals(@NullableDecl final Object o) {
            final boolean b = o instanceof a;
            boolean b3;
            final boolean b2 = b3 = false;
            if (b) {
                final a a = (a)o;
                b3 = b2;
                if (this.a.equals(a.a)) {
                    b3 = b2;
                    if (this.b.equals(a.b)) {
                        b3 = true;
                    }
                }
            }
            return b3;
        }
        
        @Override
        public int hashCode() {
            return this.a.hashCode() * 31 + this.b.hashCode();
        }
        
        @Override
        public String toString() {
            final StringBuilder sb = new StringBuilder();
            sb.append(this.a);
            sb.append(".andThen(");
            sb.append(this.b);
            sb.append(")");
            return sb.toString();
        }
    }
    
    private static final class b<A, B> extends Converter<A, B> implements Serializable
    {
        private final Function<? super A, ? extends B> a;
        private final Function<? super B, ? extends A> b;
        
        private b(final Function<? super A, ? extends B> function, final Function<? super B, ? extends A> function2) {
            this.a = Preconditions.checkNotNull(function);
            this.b = Preconditions.checkNotNull(function2);
        }
        
        @Override
        protected A doBackward(final B b) {
            return (A)this.b.apply(b);
        }
        
        @Override
        protected B doForward(final A a) {
            return (B)this.a.apply(a);
        }
        
        @Override
        public boolean equals(@NullableDecl final Object o) {
            final boolean b = o instanceof b;
            boolean b3;
            final boolean b2 = b3 = false;
            if (b) {
                final b b4 = (b)o;
                b3 = b2;
                if (this.a.equals(b4.a)) {
                    b3 = b2;
                    if (this.b.equals(b4.b)) {
                        b3 = true;
                    }
                }
            }
            return b3;
        }
        
        @Override
        public int hashCode() {
            return this.a.hashCode() * 31 + this.b.hashCode();
        }
        
        @Override
        public String toString() {
            final StringBuilder sb = new StringBuilder();
            sb.append("Converter.from(");
            sb.append(this.a);
            sb.append(", ");
            sb.append(this.b);
            sb.append(")");
            return sb.toString();
        }
    }
    
    private static final class c<T> extends Converter<T, T> implements Serializable
    {
        static final c a;
        
        static {
            a = new c();
        }
        
        public c<T> a() {
            return this;
        }
        
        @Override
         <S> Converter<T, S> doAndThen(final Converter<T, S> converter) {
            return Preconditions.checkNotNull(converter, (Object)"otherConverter");
        }
        
        @Override
        protected T doBackward(final T t) {
            return t;
        }
        
        @Override
        protected T doForward(final T t) {
            return t;
        }
        
        @Override
        public String toString() {
            return "Converter.identity()";
        }
    }
    
    private static final class d<A, B> extends Converter<B, A> implements Serializable
    {
        final Converter<A, B> a;
        
        d(final Converter<A, B> a) {
            this.a = a;
        }
        
        @NullableDecl
        @Override
        B correctedDoBackward(@NullableDecl final A a) {
            return this.a.correctedDoForward(a);
        }
        
        @NullableDecl
        @Override
        A correctedDoForward(@NullableDecl final B b) {
            return this.a.correctedDoBackward(b);
        }
        
        @Override
        protected B doBackward(final A a) {
            throw new AssertionError();
        }
        
        @Override
        protected A doForward(final B b) {
            throw new AssertionError();
        }
        
        @Override
        public boolean equals(@NullableDecl final Object o) {
            return o instanceof d && this.a.equals(((d)o).a);
        }
        
        @Override
        public int hashCode() {
            return ~this.a.hashCode();
        }
        
        @Override
        public Converter<A, B> reverse() {
            return this.a;
        }
        
        @Override
        public String toString() {
            final StringBuilder sb = new StringBuilder();
            sb.append(this.a);
            sb.append(".reverse()");
            return sb.toString();
        }
    }
}

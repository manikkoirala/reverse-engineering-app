// 
// Decompiled by Procyon v0.6.0
// 

package com.applovin.exoplayer2.common.base;

import org.checkerframework.checker.nullness.compatqual.NullableDecl;
import java.util.Collections;
import java.util.Set;

final class d<T> extends Optional<T>
{
    private final T a;
    
    d(final T a) {
        this.a = a;
    }
    
    @Override
    public Set<T> asSet() {
        return Collections.singleton(this.a);
    }
    
    @Override
    public boolean equals(@NullableDecl final Object o) {
        return o instanceof d && this.a.equals(((d)o).a);
    }
    
    @Override
    public T get() {
        return this.a;
    }
    
    @Override
    public int hashCode() {
        return this.a.hashCode() + 1502476572;
    }
    
    @Override
    public boolean isPresent() {
        return true;
    }
    
    @Override
    public Optional<T> or(final Optional<? extends T> optional) {
        Preconditions.checkNotNull(optional);
        return this;
    }
    
    @Override
    public T or(final Supplier<? extends T> supplier) {
        Preconditions.checkNotNull(supplier);
        return this.a;
    }
    
    @Override
    public T or(final T t) {
        Preconditions.checkNotNull(t, (Object)"use Optional.orNull() instead of Optional.or(null)");
        return this.a;
    }
    
    @Override
    public T orNull() {
        return this.a;
    }
    
    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append("Optional.of(");
        sb.append(this.a);
        sb.append(")");
        return sb.toString();
    }
    
    @Override
    public <V> Optional<V> transform(final Function<? super T, V> function) {
        return new d<V>(Preconditions.checkNotNull(function.apply(this.a), (Object)"the Function passed to Optional.transform() must not return null."));
    }
}

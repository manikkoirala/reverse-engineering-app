// 
// Decompiled by Procyon v0.6.0
// 

package com.applovin.exoplayer2.common.base;

import java.util.Arrays;

public final class Objects extends c
{
    private Objects() {
    }
    
    public static boolean equal(final Object o, final Object obj) {
        return o == obj || (o != null && o.equals(obj));
    }
    
    public static int hashCode(final Object... a) {
        return Arrays.hashCode(a);
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.applovin.exoplayer2.common.base;

import java.io.Serializable;
import java.util.Collection;
import org.checkerframework.checker.nullness.compatqual.NullableDecl;
import java.util.Iterator;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public final class Predicates
{
    private Predicates() {
    }
    
    public static <T> Predicate<T> alwaysFalse() {
        return g.b.a();
    }
    
    public static <T> Predicate<T> alwaysTrue() {
        return g.a.a();
    }
    
    public static <T> Predicate<T> and(final Predicate<? super T> predicate, final Predicate<? super T> predicate2) {
        return new a<T>((List)asList(Preconditions.checkNotNull(predicate), Preconditions.checkNotNull(predicate2)));
    }
    
    public static <T> Predicate<T> and(final Iterable<? extends Predicate<? super T>> iterable) {
        return new a<T>((List)defensiveCopy((Iterable<Object>)iterable));
    }
    
    @SafeVarargs
    public static <T> Predicate<T> and(final Predicate<? super T>... array) {
        return new a<T>((List)defensiveCopy(array));
    }
    
    private static <T> List<Predicate<? super T>> asList(final Predicate<? super T> predicate, final Predicate<? super T> predicate2) {
        return (List<Predicate<? super T>>)Arrays.asList(predicate, predicate2);
    }
    
    public static <A, B> Predicate<A> compose(final Predicate<B> predicate, final Function<A, ? extends B> function) {
        return new b<A, Object>((Predicate)predicate, (Function)function);
    }
    
    static <T> List<T> defensiveCopy(final Iterable<T> iterable) {
        final ArrayList list = new ArrayList();
        final Iterator<T> iterator = iterable.iterator();
        while (iterator.hasNext()) {
            list.add(Preconditions.checkNotNull(iterator.next()));
        }
        return list;
    }
    
    private static <T> List<T> defensiveCopy(final T... a) {
        return defensiveCopy(Arrays.asList(a));
    }
    
    public static <T> Predicate<T> equalTo(@NullableDecl final T t) {
        Predicate<Object> null;
        if (t == null) {
            null = isNull();
        }
        else {
            null = new e<Object>((Object)t);
        }
        return (Predicate<T>)null;
    }
    
    public static <T> Predicate<T> in(final Collection<? extends T> collection) {
        return new c<T>((Collection)collection);
    }
    
    public static Predicate<Object> instanceOf(final Class<?> clazz) {
        return new d((Class)clazz);
    }
    
    public static <T> Predicate<T> isNull() {
        return g.c.a();
    }
    
    public static <T> Predicate<T> not(final Predicate<T> predicate) {
        return new f<T>(predicate);
    }
    
    public static <T> Predicate<T> notNull() {
        return g.d.a();
    }
    
    public static <T> Predicate<T> or(final Predicate<? super T> predicate, final Predicate<? super T> predicate2) {
        return new h<T>((List)asList(Preconditions.checkNotNull(predicate), Preconditions.checkNotNull(predicate2)));
    }
    
    public static <T> Predicate<T> or(final Iterable<? extends Predicate<? super T>> iterable) {
        return new h<T>((List)defensiveCopy((Iterable<Object>)iterable));
    }
    
    @SafeVarargs
    public static <T> Predicate<T> or(final Predicate<? super T>... array) {
        return new h<T>((List)defensiveCopy(array));
    }
    
    public static Predicate<Class<?>> subtypeOf(final Class<?> clazz) {
        return new i((Class)clazz);
    }
    
    private static String toStringHelper(final String str, final Iterable<?> iterable) {
        final StringBuilder sb = new StringBuilder("Predicates.");
        sb.append(str);
        sb.append('(');
        final Iterator<?> iterator = iterable.iterator();
        int n = 1;
        while (iterator.hasNext()) {
            final Object next = iterator.next();
            if (n == 0) {
                sb.append(',');
            }
            sb.append(next);
            n = 0;
        }
        sb.append(')');
        return sb.toString();
    }
    
    private static class a<T> implements Predicate<T>, Serializable
    {
        private final List<? extends Predicate<? super T>> a;
        
        private a(final List<? extends Predicate<? super T>> a) {
            this.a = a;
        }
        
        @Override
        public boolean apply(@NullableDecl final T t) {
            for (int i = 0; i < this.a.size(); ++i) {
                if (!((Predicate)this.a.get(i)).apply(t)) {
                    return false;
                }
            }
            return true;
        }
        
        @Override
        public boolean equals(@NullableDecl final Object o) {
            return o instanceof a && this.a.equals(((a)o).a);
        }
        
        @Override
        public int hashCode() {
            return this.a.hashCode() + 306654252;
        }
        
        @Override
        public String toString() {
            return toStringHelper("and", this.a);
        }
    }
    
    private static class b<A, B> implements Predicate<A>, Serializable
    {
        final Predicate<B> a;
        final Function<A, ? extends B> b;
        
        private b(final Predicate<B> predicate, final Function<A, ? extends B> function) {
            this.a = Preconditions.checkNotNull(predicate);
            this.b = Preconditions.checkNotNull(function);
        }
        
        @Override
        public boolean apply(@NullableDecl final A a) {
            return this.a.apply((B)this.b.apply(a));
        }
        
        @Override
        public boolean equals(@NullableDecl final Object o) {
            final boolean b = o instanceof b;
            boolean b3;
            final boolean b2 = b3 = false;
            if (b) {
                final b b4 = (b)o;
                b3 = b2;
                if (this.b.equals(b4.b)) {
                    b3 = b2;
                    if (this.a.equals(b4.a)) {
                        b3 = true;
                    }
                }
            }
            return b3;
        }
        
        @Override
        public int hashCode() {
            return this.b.hashCode() ^ this.a.hashCode();
        }
        
        @Override
        public String toString() {
            final StringBuilder sb = new StringBuilder();
            sb.append(this.a);
            sb.append("(");
            sb.append(this.b);
            sb.append(")");
            return sb.toString();
        }
    }
    
    private static class c<T> implements Predicate<T>, Serializable
    {
        private final Collection<?> a;
        
        private c(final Collection<?> collection) {
            this.a = Preconditions.checkNotNull(collection);
        }
        
        @Override
        public boolean apply(@NullableDecl final T t) {
            try {
                return this.a.contains(t);
            }
            catch (final NullPointerException | ClassCastException ex) {
                return false;
            }
        }
        
        @Override
        public boolean equals(@NullableDecl final Object o) {
            return o instanceof c && this.a.equals(((c)o).a);
        }
        
        @Override
        public int hashCode() {
            return this.a.hashCode();
        }
        
        @Override
        public String toString() {
            final StringBuilder sb = new StringBuilder();
            sb.append("Predicates.in(");
            sb.append(this.a);
            sb.append(")");
            return sb.toString();
        }
    }
    
    private static class d implements Predicate<Object>, Serializable
    {
        private final Class<?> a;
        
        private d(final Class<?> clazz) {
            this.a = Preconditions.checkNotNull(clazz);
        }
        
        @Override
        public boolean apply(@NullableDecl final Object o) {
            return this.a.isInstance(o);
        }
        
        @Override
        public boolean equals(@NullableDecl final Object o) {
            final boolean b = o instanceof d;
            boolean b2 = false;
            if (b) {
                final d d = (d)o;
                b2 = b2;
                if (this.a == d.a) {
                    b2 = true;
                }
            }
            return b2;
        }
        
        @Override
        public int hashCode() {
            return this.a.hashCode();
        }
        
        @Override
        public String toString() {
            final StringBuilder sb = new StringBuilder();
            sb.append("Predicates.instanceOf(");
            sb.append(this.a.getName());
            sb.append(")");
            return sb.toString();
        }
    }
    
    private static class e<T> implements Predicate<T>, Serializable
    {
        private final T a;
        
        private e(final T a) {
            this.a = a;
        }
        
        @Override
        public boolean apply(final T obj) {
            return this.a.equals(obj);
        }
        
        @Override
        public boolean equals(@NullableDecl final Object o) {
            return o instanceof e && this.a.equals(((e)o).a);
        }
        
        @Override
        public int hashCode() {
            return this.a.hashCode();
        }
        
        @Override
        public String toString() {
            final StringBuilder sb = new StringBuilder();
            sb.append("Predicates.equalTo(");
            sb.append(this.a);
            sb.append(")");
            return sb.toString();
        }
    }
    
    private static class f<T> implements Predicate<T>, Serializable
    {
        final Predicate<T> a;
        
        f(final Predicate<T> predicate) {
            this.a = Preconditions.checkNotNull(predicate);
        }
        
        @Override
        public boolean apply(@NullableDecl final T t) {
            return this.a.apply(t) ^ true;
        }
        
        @Override
        public boolean equals(@NullableDecl final Object o) {
            return o instanceof f && this.a.equals(((f)o).a);
        }
        
        @Override
        public int hashCode() {
            return ~this.a.hashCode();
        }
        
        @Override
        public String toString() {
            final StringBuilder sb = new StringBuilder();
            sb.append("Predicates.not(");
            sb.append(this.a);
            sb.append(")");
            return sb.toString();
        }
    }
    
    enum g implements Predicate<Object>
    {
        a {
            @Override
            public boolean apply(@NullableDecl final Object o) {
                return true;
            }
            
            @Override
            public String toString() {
                return "Predicates.alwaysTrue()";
            }
        }, 
        b {
            @Override
            public boolean apply(@NullableDecl final Object o) {
                return false;
            }
            
            @Override
            public String toString() {
                return "Predicates.alwaysFalse()";
            }
        }, 
        c {
            @Override
            public boolean apply(@NullableDecl final Object o) {
                return o == null;
            }
            
            @Override
            public String toString() {
                return "Predicates.isNull()";
            }
        }, 
        d {
            @Override
            public boolean apply(@NullableDecl final Object o) {
                return o != null;
            }
            
            @Override
            public String toString() {
                return "Predicates.notNull()";
            }
        };
        
        private static final g[] e;
        
         <T> Predicate<T> a() {
            return (Predicate<T>)this;
        }
    }
    
    private static class h<T> implements Predicate<T>, Serializable
    {
        private final List<? extends Predicate<? super T>> a;
        
        private h(final List<? extends Predicate<? super T>> a) {
            this.a = a;
        }
        
        @Override
        public boolean apply(@NullableDecl final T t) {
            for (int i = 0; i < this.a.size(); ++i) {
                if (((Predicate)this.a.get(i)).apply(t)) {
                    return true;
                }
            }
            return false;
        }
        
        @Override
        public boolean equals(@NullableDecl final Object o) {
            return o instanceof h && this.a.equals(((h)o).a);
        }
        
        @Override
        public int hashCode() {
            return this.a.hashCode() + 87855567;
        }
        
        @Override
        public String toString() {
            return toStringHelper("or", this.a);
        }
    }
    
    private static class i implements Predicate<Class<?>>, Serializable
    {
        private final Class<?> a;
        
        private i(final Class<?> clazz) {
            this.a = Preconditions.checkNotNull(clazz);
        }
        
        public boolean a(final Class<?> clazz) {
            return this.a.isAssignableFrom(clazz);
        }
        
        @Override
        public boolean equals(@NullableDecl final Object o) {
            final boolean b = o instanceof i;
            boolean b2 = false;
            if (b) {
                final i i = (i)o;
                b2 = b2;
                if (this.a == i.a) {
                    b2 = true;
                }
            }
            return b2;
        }
        
        @Override
        public int hashCode() {
            return this.a.hashCode();
        }
        
        @Override
        public String toString() {
            final StringBuilder sb = new StringBuilder();
            sb.append("Predicates.subtypeOf(");
            sb.append(this.a.getName());
            sb.append(")");
            return sb.toString();
        }
    }
}

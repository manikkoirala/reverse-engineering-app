// 
// Decompiled by Procyon v0.6.0
// 

package com.applovin.exoplayer2.common.base;

import java.util.Arrays;
import java.util.BitSet;

public abstract class CharMatcher implements Predicate<Character>
{
    protected CharMatcher() {
    }
    
    public static CharMatcher any() {
        return b.a;
    }
    
    public static CharMatcher anyOf(final CharSequence charSequence) {
        final int length = charSequence.length();
        if (length == 0) {
            return none();
        }
        if (length == 1) {
            return is(charSequence.charAt(0));
        }
        if (length != 2) {
            return new c(charSequence);
        }
        return isEither(charSequence.charAt(0), charSequence.charAt(1));
    }
    
    public static CharMatcher ascii() {
        return d.a;
    }
    
    public static CharMatcher breakingWhitespace() {
        return e.a;
    }
    
    @Deprecated
    public static CharMatcher digit() {
        return f.a;
    }
    
    private String finishCollapseFrom(final CharSequence charSequence, int i, final int n, final char c, final StringBuilder sb, final boolean b) {
        int n2 = b ? 1 : 0;
        while (i < n) {
            final char char1 = charSequence.charAt(i);
            int n3;
            if (this.matches(char1)) {
                if ((n3 = n2) == 0) {
                    sb.append(c);
                    n3 = 1;
                }
            }
            else {
                sb.append(char1);
                n3 = 0;
            }
            ++i;
            n2 = n3;
        }
        return sb.toString();
    }
    
    public static CharMatcher forPredicate(final Predicate<? super Character> predicate) {
        CharMatcher charMatcher;
        if (predicate instanceof CharMatcher) {
            charMatcher = (CharMatcher)predicate;
        }
        else {
            charMatcher = new h(predicate);
        }
        return charMatcher;
    }
    
    public static CharMatcher inRange(final char c, final char c2) {
        return new i(c, c2);
    }
    
    @Deprecated
    public static CharMatcher invisible() {
        return j.a;
    }
    
    public static CharMatcher is(final char c) {
        return new k(c);
    }
    
    private static l isEither(final char c, final char c2) {
        return new l(c, c2);
    }
    
    public static CharMatcher isNot(final char c) {
        return new m(c);
    }
    
    @Deprecated
    public static CharMatcher javaDigit() {
        return n.a;
    }
    
    public static CharMatcher javaIsoControl() {
        return o.a;
    }
    
    @Deprecated
    public static CharMatcher javaLetter() {
        return p.a;
    }
    
    @Deprecated
    public static CharMatcher javaLetterOrDigit() {
        return q.a;
    }
    
    @Deprecated
    public static CharMatcher javaLowerCase() {
        return r.a;
    }
    
    @Deprecated
    public static CharMatcher javaUpperCase() {
        return s.a;
    }
    
    public static CharMatcher none() {
        return w.a;
    }
    
    public static CharMatcher noneOf(final CharSequence charSequence) {
        return anyOf(charSequence).negate();
    }
    
    private static String showCharacter(char c) {
        final char[] array;
        final char[] data = array = new char[6];
        array[0] = '\\';
        array[1] = 'u';
        array[3] = (array[2] = '\0');
        array[5] = (array[4] = '\0');
        for (int i = 0; i < 4; ++i) {
            data[5 - i] = "0123456789ABCDEF".charAt(c & '\u000f');
            c >>= 4;
        }
        return String.copyValueOf(data);
    }
    
    @Deprecated
    public static CharMatcher singleWidth() {
        return z.a;
    }
    
    public static CharMatcher whitespace() {
        return aa.b;
    }
    
    public CharMatcher and(final CharMatcher charMatcher) {
        return new a(this, charMatcher);
    }
    
    @Deprecated
    @Override
    public boolean apply(final Character c) {
        return this.matches(c);
    }
    
    public String collapseFrom(final CharSequence s, final char c) {
        int n;
        for (int length = s.length(), i = 0; i < length; i = n + 1) {
            final char char1 = s.charAt(i);
            n = i;
            if (this.matches(char1)) {
                if (char1 != c || (i != length - 1 && this.matches(s.charAt(i + 1)))) {
                    final StringBuilder sb = new StringBuilder(length);
                    sb.append(s, 0, i);
                    sb.append(c);
                    return this.finishCollapseFrom(s, i + 1, length, c, sb, true);
                }
                n = i + 1;
            }
        }
        return s.toString();
    }
    
    public int countIn(final CharSequence charSequence) {
        int i = 0;
        int n = 0;
        while (i < charSequence.length()) {
            int n2 = n;
            if (this.matches(charSequence.charAt(i))) {
                n2 = n + 1;
            }
            ++i;
            n = n2;
        }
        return n;
    }
    
    public int indexIn(final CharSequence charSequence) {
        return this.indexIn(charSequence, 0);
    }
    
    public int indexIn(final CharSequence charSequence, int i) {
        final int length = charSequence.length();
        Preconditions.checkPositionIndex(i, length);
        while (i < length) {
            if (this.matches(charSequence.charAt(i))) {
                return i;
            }
            ++i;
        }
        return -1;
    }
    
    public int lastIndexIn(final CharSequence charSequence) {
        for (int i = charSequence.length() - 1; i >= 0; --i) {
            if (this.matches(charSequence.charAt(i))) {
                return i;
            }
        }
        return -1;
    }
    
    public abstract boolean matches(final char p0);
    
    public boolean matchesAllOf(final CharSequence charSequence) {
        for (int i = charSequence.length() - 1; i >= 0; --i) {
            if (!this.matches(charSequence.charAt(i))) {
                return false;
            }
        }
        return true;
    }
    
    public boolean matchesAnyOf(final CharSequence charSequence) {
        return this.matchesNoneOf(charSequence) ^ true;
    }
    
    public boolean matchesNoneOf(final CharSequence charSequence) {
        return this.indexIn(charSequence) == -1;
    }
    
    public CharMatcher negate() {
        return new u(this);
    }
    
    public CharMatcher or(final CharMatcher charMatcher) {
        return new x(this, charMatcher);
    }
    
    public String removeFrom(final CharSequence charSequence) {
        final String string = charSequence.toString();
        int i = this.indexIn(string);
        if (i == -1) {
            return string;
        }
        final char[] charArray = string.toCharArray();
        int n = 1;
    Label_0027:
        while (true) {
            ++i;
            while (i != charArray.length) {
                if (this.matches(charArray[i])) {
                    ++n;
                    continue Label_0027;
                }
                charArray[i - n] = charArray[i];
                ++i;
            }
            break;
        }
        return new String(charArray, 0, i - n);
    }
    
    public String replaceFrom(final CharSequence charSequence, final char c) {
        final String string = charSequence.toString();
        int indexIn = this.indexIn(string);
        if (indexIn == -1) {
            return string;
        }
        final char[] charArray = string.toCharArray();
        charArray[indexIn] = c;
        while (true) {
            final int n = indexIn + 1;
            if (n >= charArray.length) {
                break;
            }
            indexIn = n;
            if (!this.matches(charArray[n])) {
                continue;
            }
            charArray[n] = c;
            indexIn = n;
        }
        return new String(charArray);
    }
    
    public String replaceFrom(final CharSequence charSequence, final CharSequence s) {
        final int length = s.length();
        if (length == 0) {
            return this.removeFrom(charSequence);
        }
        int start = 0;
        if (length == 1) {
            return this.replaceFrom(charSequence, s.charAt(0));
        }
        final String string = charSequence.toString();
        int end = this.indexIn(string);
        if (end == -1) {
            return string;
        }
        final int length2 = string.length();
        final StringBuilder sb = new StringBuilder(length2 * 3 / 2 + 16);
        int i;
        int start2;
        do {
            sb.append(string, start, end);
            sb.append(s);
            start2 = end + 1;
            i = (end = this.indexIn(string, start2));
            start = start2;
        } while (i != -1);
        sb.append(string, start2, length2);
        return sb.toString();
    }
    
    public String retainFrom(final CharSequence charSequence) {
        return this.negate().removeFrom(charSequence);
    }
    
    void setBits(final BitSet set) {
        for (int i = 65535; i >= 0; --i) {
            if (this.matches((char)i)) {
                set.set(i);
            }
        }
    }
    
    @Override
    public String toString() {
        return super.toString();
    }
    
    public String trimAndCollapseFrom(final CharSequence charSequence, final char c) {
        final int length = charSequence.length();
        final int n = length - 1;
        int n2;
        for (n2 = 0; n2 < length && this.matches(charSequence.charAt(n2)); ++n2) {}
        int n3;
        for (n3 = n; n3 > n2 && this.matches(charSequence.charAt(n3)); --n3) {}
        String s;
        if (n2 == 0 && n3 == n) {
            s = this.collapseFrom(charSequence, c);
        }
        else {
            ++n3;
            s = this.finishCollapseFrom(charSequence, n2, n3, c, new StringBuilder(n3 - n2), false);
        }
        return s;
    }
    
    public String trimFrom(final CharSequence charSequence) {
        int length;
        int n;
        for (length = charSequence.length(), n = 0; n < length && this.matches(charSequence.charAt(n)); ++n) {}
        --length;
        while (length > n && this.matches(charSequence.charAt(length))) {
            --length;
        }
        return charSequence.subSequence(n, length + 1).toString();
    }
    
    public String trimLeadingFrom(final CharSequence charSequence) {
        for (int length = charSequence.length(), i = 0; i < length; ++i) {
            if (!this.matches(charSequence.charAt(i))) {
                return charSequence.subSequence(i, length).toString();
            }
        }
        return "";
    }
    
    public String trimTrailingFrom(final CharSequence charSequence) {
        for (int i = charSequence.length() - 1; i >= 0; --i) {
            if (!this.matches(charSequence.charAt(i))) {
                return charSequence.subSequence(0, i + 1).toString();
            }
        }
        return "";
    }
    
    private static final class a extends CharMatcher
    {
        final CharMatcher a;
        final CharMatcher b;
        
        a(final CharMatcher charMatcher, final CharMatcher charMatcher2) {
            this.a = Preconditions.checkNotNull(charMatcher);
            this.b = Preconditions.checkNotNull(charMatcher2);
        }
        
        @Override
        public boolean matches(final char c) {
            return this.a.matches(c) && this.b.matches(c);
        }
        
        @Override
        void setBits(final BitSet set) {
            final BitSet set2 = new BitSet();
            this.a.setBits(set2);
            final BitSet set3 = new BitSet();
            this.b.setBits(set3);
            set2.and(set3);
            set.or(set2);
        }
        
        @Override
        public String toString() {
            final StringBuilder sb = new StringBuilder();
            sb.append("CharMatcher.and(");
            sb.append(this.a);
            sb.append(", ");
            sb.append(this.b);
            sb.append(")");
            return sb.toString();
        }
    }
    
    static final class aa extends t
    {
        static final int a;
        static final aa b;
        
        static {
            a = Integer.numberOfLeadingZeros(31);
            b = new aa();
        }
        
        aa() {
            super("CharMatcher.whitespace()");
        }
        
        @Override
        public boolean matches(final char c) {
            return "\u2002\u3000\r\u0085\u200a\u2005\u2000\u3000\u2029\u000b\u3000\u2008\u2003\u205f\u3000\u1680\t \u2006\u2001\u202f \f\u2009\u3000\u2004\u3000\u3000\u2028\n\u2007\u3000".charAt(1682554634 * c >>> aa.a) == c;
        }
        
        @Override
        void setBits(final BitSet set) {
            for (int i = 0; i < 32; ++i) {
                set.set("\u2002\u3000\r\u0085\u200a\u2005\u2000\u3000\u2029\u000b\u3000\u2008\u2003\u205f\u3000\u1680\t \u2006\u2001\u202f \f\u2009\u3000\u2004\u3000\u3000\u2028\n\u2007\u3000".charAt(i));
            }
        }
    }
    
    private static final class b extends t
    {
        static final b a;
        
        static {
            a = new b();
        }
        
        private b() {
            super("CharMatcher.any()");
        }
        
        @Override
        public CharMatcher and(final CharMatcher charMatcher) {
            return Preconditions.checkNotNull(charMatcher);
        }
        
        @Override
        public String collapseFrom(final CharSequence charSequence, final char c) {
            String value;
            if (charSequence.length() == 0) {
                value = "";
            }
            else {
                value = String.valueOf(c);
            }
            return value;
        }
        
        @Override
        public int countIn(final CharSequence charSequence) {
            return charSequence.length();
        }
        
        @Override
        public int indexIn(final CharSequence charSequence) {
            int n;
            if (charSequence.length() == 0) {
                n = -1;
            }
            else {
                n = 0;
            }
            return n;
        }
        
        @Override
        public int indexIn(final CharSequence charSequence, final int n) {
            final int length = charSequence.length();
            Preconditions.checkPositionIndex(n, length);
            int n2 = n;
            if (n == length) {
                n2 = -1;
            }
            return n2;
        }
        
        @Override
        public int lastIndexIn(final CharSequence charSequence) {
            return charSequence.length() - 1;
        }
        
        @Override
        public boolean matches(final char c) {
            return true;
        }
        
        @Override
        public boolean matchesAllOf(final CharSequence charSequence) {
            Preconditions.checkNotNull(charSequence);
            return true;
        }
        
        @Override
        public boolean matchesNoneOf(final CharSequence charSequence) {
            return charSequence.length() == 0;
        }
        
        @Override
        public CharMatcher negate() {
            return CharMatcher.none();
        }
        
        @Override
        public CharMatcher or(final CharMatcher charMatcher) {
            Preconditions.checkNotNull(charMatcher);
            return this;
        }
        
        @Override
        public String removeFrom(final CharSequence charSequence) {
            Preconditions.checkNotNull(charSequence);
            return "";
        }
        
        @Override
        public String replaceFrom(final CharSequence charSequence, final char val) {
            final char[] array = new char[charSequence.length()];
            Arrays.fill(array, val);
            return new String(array);
        }
        
        @Override
        public String replaceFrom(final CharSequence charSequence, final CharSequence s) {
            final StringBuilder sb = new StringBuilder(charSequence.length() * s.length());
            for (int i = 0; i < charSequence.length(); ++i) {
                sb.append(s);
            }
            return sb.toString();
        }
        
        @Override
        public String trimFrom(final CharSequence charSequence) {
            Preconditions.checkNotNull(charSequence);
            return "";
        }
    }
    
    private static final class c extends CharMatcher
    {
        private final char[] a;
        
        public c(final CharSequence charSequence) {
            Arrays.sort(this.a = charSequence.toString().toCharArray());
        }
        
        @Override
        public boolean matches(final char key) {
            return Arrays.binarySearch(this.a, key) >= 0;
        }
        
        @Override
        void setBits(final BitSet set) {
            final char[] a = this.a;
            for (int length = a.length, i = 0; i < length; ++i) {
                set.set(a[i]);
            }
        }
        
        @Override
        public String toString() {
            final StringBuilder sb = new StringBuilder("CharMatcher.anyOf(\"");
            final char[] a = this.a;
            for (int length = a.length, i = 0; i < length; ++i) {
                sb.append(showCharacter(a[i]));
            }
            sb.append("\")");
            return sb.toString();
        }
    }
    
    private static final class d extends t
    {
        static final d a;
        
        static {
            a = new d();
        }
        
        d() {
            super("CharMatcher.ascii()");
        }
        
        @Override
        public boolean matches(final char c) {
            return c <= '\u007f';
        }
    }
    
    private static final class e extends CharMatcher
    {
        static final CharMatcher a;
        
        static {
            a = new e();
        }
        
        @Override
        public boolean matches(final char c) {
            boolean b = true;
            if (c != ' ' && c != '\u0085' && c != '\u1680') {
                if (c == '\u2007') {
                    return false;
                }
                if (c != '\u205f' && c != '\u3000' && c != '\u2028' && c != '\u2029') {
                    switch (c) {
                        default: {
                            if (c < '\u2000' || c > '\u200a') {
                                b = false;
                            }
                            return b;
                        }
                        case '\t':
                        case '\n':
                        case '\u000b':
                        case '\f':
                        case '\r': {
                            break;
                        }
                    }
                }
            }
            return true;
        }
        
        @Override
        public String toString() {
            return "CharMatcher.breakingWhitespace()";
        }
    }
    
    private static final class f extends y
    {
        static final f a;
        
        static {
            a = new f();
        }
        
        private f() {
            super("CharMatcher.digit()", a(), b());
        }
        
        private static char[] a() {
            return "0\u0660\u06f0\u07c0\u0966\u09e6\u0a66\u0ae6\u0b66\u0be6\u0c66\u0ce6\u0d66\u0de6\u0e50\u0ed0\u0f20\u1040\u1090\u17e0\u1810\u1946\u19d0\u1a80\u1a90\u1b50\u1bb0\u1c40\u1c50\ua620\ua8d0\ua900\ua9d0\ua9f0\uaa50\uabf0\uff10".toCharArray();
        }
        
        private static char[] b() {
            final char[] array = new char[37];
            for (int i = 0; i < 37; ++i) {
                array[i] = (char)("0\u0660\u06f0\u07c0\u0966\u09e6\u0a66\u0ae6\u0b66\u0be6\u0c66\u0ce6\u0d66\u0de6\u0e50\u0ed0\u0f20\u1040\u1090\u17e0\u1810\u1946\u19d0\u1a80\u1a90\u1b50\u1bb0\u1c40\u1c50\ua620\ua8d0\ua900\ua9d0\ua9f0\uaa50\uabf0\uff10".charAt(i) + '\t');
            }
            return array;
        }
    }
    
    abstract static class g extends CharMatcher
    {
        @Override
        public CharMatcher negate() {
            return new v(this);
        }
    }
    
    private static final class h extends CharMatcher
    {
        private final Predicate<? super Character> a;
        
        h(final Predicate<? super Character> predicate) {
            this.a = Preconditions.checkNotNull(predicate);
        }
        
        @Override
        public boolean apply(final Character c) {
            return this.a.apply(Preconditions.checkNotNull(c));
        }
        
        @Override
        public boolean matches(final char c) {
            return this.a.apply(c);
        }
        
        @Override
        public String toString() {
            final StringBuilder sb = new StringBuilder();
            sb.append("CharMatcher.forPredicate(");
            sb.append(this.a);
            sb.append(")");
            return sb.toString();
        }
    }
    
    private static final class i extends g
    {
        private final char a;
        private final char b;
        
        i(final char a, final char b) {
            Preconditions.checkArgument(b >= a);
            this.a = a;
            this.b = b;
        }
        
        @Override
        public boolean matches(final char c) {
            return this.a <= c && c <= this.b;
        }
        
        @Override
        void setBits(final BitSet set) {
            set.set(this.a, this.b + '\u0001');
        }
        
        @Override
        public String toString() {
            final StringBuilder sb = new StringBuilder();
            sb.append("CharMatcher.inRange('");
            sb.append(showCharacter(this.a));
            sb.append("', '");
            sb.append(showCharacter(this.b));
            sb.append("')");
            return sb.toString();
        }
    }
    
    private static final class j extends y
    {
        static final j a;
        
        static {
            a = new j();
        }
        
        private j() {
            super("CharMatcher.invisible()", "\u0000\u007f\u00ad\u0600\u061c\u06dd\u070f\u08e2\u1680\u180e\u2000\u2028\u205f\u2066\u3000\ud800\ufeff\ufff9".toCharArray(), "  \u00ad\u0605\u061c\u06dd\u070f\u08e2\u1680\u180e\u200f\u202f\u2064\u206f\u3000\uf8ff\ufeff\ufffb".toCharArray());
        }
    }
    
    private static final class k extends g
    {
        private final char a;
        
        k(final char a) {
            this.a = a;
        }
        
        @Override
        public CharMatcher and(CharMatcher none) {
            if (none.matches(this.a)) {
                none = this;
            }
            else {
                none = CharMatcher.none();
            }
            return none;
        }
        
        @Override
        public boolean matches(final char c) {
            return c == this.a;
        }
        
        @Override
        public CharMatcher negate() {
            return CharMatcher.isNot(this.a);
        }
        
        @Override
        public CharMatcher or(CharMatcher or) {
            if (!or.matches(this.a)) {
                or = super.or(or);
            }
            return or;
        }
        
        @Override
        public String replaceFrom(final CharSequence charSequence, final char newChar) {
            return charSequence.toString().replace(this.a, newChar);
        }
        
        @Override
        void setBits(final BitSet set) {
            set.set(this.a);
        }
        
        @Override
        public String toString() {
            final StringBuilder sb = new StringBuilder();
            sb.append("CharMatcher.is('");
            sb.append(showCharacter(this.a));
            sb.append("')");
            return sb.toString();
        }
    }
    
    private static final class l extends g
    {
        private final char a;
        private final char b;
        
        l(final char a, final char b) {
            this.a = a;
            this.b = b;
        }
        
        @Override
        public boolean matches(final char c) {
            return c == this.a || c == this.b;
        }
        
        @Override
        void setBits(final BitSet set) {
            set.set(this.a);
            set.set(this.b);
        }
        
        @Override
        public String toString() {
            final StringBuilder sb = new StringBuilder();
            sb.append("CharMatcher.anyOf(\"");
            sb.append(showCharacter(this.a));
            sb.append(showCharacter(this.b));
            sb.append("\")");
            return sb.toString();
        }
    }
    
    private static final class m extends g
    {
        private final char a;
        
        m(final char a) {
            this.a = a;
        }
        
        @Override
        public CharMatcher and(final CharMatcher charMatcher) {
            CharMatcher and = charMatcher;
            if (charMatcher.matches(this.a)) {
                and = super.and(charMatcher);
            }
            return and;
        }
        
        @Override
        public boolean matches(final char c) {
            return c != this.a;
        }
        
        @Override
        public CharMatcher negate() {
            return CharMatcher.is(this.a);
        }
        
        @Override
        public CharMatcher or(CharMatcher any) {
            if (any.matches(this.a)) {
                any = CharMatcher.any();
            }
            else {
                any = this;
            }
            return any;
        }
        
        @Override
        void setBits(final BitSet set) {
            set.set(0, this.a);
            set.set(this.a + '\u0001', 65536);
        }
        
        @Override
        public String toString() {
            final StringBuilder sb = new StringBuilder();
            sb.append("CharMatcher.isNot('");
            sb.append(showCharacter(this.a));
            sb.append("')");
            return sb.toString();
        }
    }
    
    private static final class n extends CharMatcher
    {
        static final n a;
        
        static {
            a = new n();
        }
        
        @Override
        public boolean matches(final char ch) {
            return Character.isDigit(ch);
        }
        
        @Override
        public String toString() {
            return "CharMatcher.javaDigit()";
        }
    }
    
    private static final class o extends t
    {
        static final o a;
        
        static {
            a = new o();
        }
        
        private o() {
            super("CharMatcher.javaIsoControl()");
        }
        
        @Override
        public boolean matches(final char c) {
            return c <= '\u001f' || (c >= '\u007f' && c <= '\u009f');
        }
    }
    
    private static final class p extends CharMatcher
    {
        static final p a;
        
        static {
            a = new p();
        }
        
        @Override
        public boolean matches(final char ch) {
            return Character.isLetter(ch);
        }
        
        @Override
        public String toString() {
            return "CharMatcher.javaLetter()";
        }
    }
    
    private static final class q extends CharMatcher
    {
        static final q a;
        
        static {
            a = new q();
        }
        
        @Override
        public boolean matches(final char ch) {
            return Character.isLetterOrDigit(ch);
        }
        
        @Override
        public String toString() {
            return "CharMatcher.javaLetterOrDigit()";
        }
    }
    
    private static final class r extends CharMatcher
    {
        static final r a;
        
        static {
            a = new r();
        }
        
        @Override
        public boolean matches(final char ch) {
            return Character.isLowerCase(ch);
        }
        
        @Override
        public String toString() {
            return "CharMatcher.javaLowerCase()";
        }
    }
    
    private static final class s extends CharMatcher
    {
        static final s a;
        
        static {
            a = new s();
        }
        
        @Override
        public boolean matches(final char ch) {
            return Character.isUpperCase(ch);
        }
        
        @Override
        public String toString() {
            return "CharMatcher.javaUpperCase()";
        }
    }
    
    abstract static class t extends g
    {
        private final String a;
        
        t(final String s) {
            this.a = Preconditions.checkNotNull(s);
        }
        
        @Override
        public final String toString() {
            return this.a;
        }
    }
    
    private static class u extends CharMatcher
    {
        final CharMatcher a;
        
        u(final CharMatcher charMatcher) {
            this.a = Preconditions.checkNotNull(charMatcher);
        }
        
        @Override
        public int countIn(final CharSequence charSequence) {
            return charSequence.length() - this.a.countIn(charSequence);
        }
        
        @Override
        public boolean matches(final char c) {
            return this.a.matches(c) ^ true;
        }
        
        @Override
        public boolean matchesAllOf(final CharSequence charSequence) {
            return this.a.matchesNoneOf(charSequence);
        }
        
        @Override
        public boolean matchesNoneOf(final CharSequence charSequence) {
            return this.a.matchesAllOf(charSequence);
        }
        
        @Override
        public CharMatcher negate() {
            return this.a;
        }
        
        @Override
        void setBits(final BitSet set) {
            final BitSet set2 = new BitSet();
            this.a.setBits(set2);
            set2.flip(0, 65536);
            set.or(set2);
        }
        
        @Override
        public String toString() {
            final StringBuilder sb = new StringBuilder();
            sb.append(this.a);
            sb.append(".negate()");
            return sb.toString();
        }
    }
    
    static class v extends u
    {
        v(final CharMatcher charMatcher) {
            super(charMatcher);
        }
    }
    
    private static final class w extends t
    {
        static final w a;
        
        static {
            a = new w();
        }
        
        private w() {
            super("CharMatcher.none()");
        }
        
        @Override
        public CharMatcher and(final CharMatcher charMatcher) {
            Preconditions.checkNotNull(charMatcher);
            return this;
        }
        
        @Override
        public String collapseFrom(final CharSequence charSequence, final char c) {
            return charSequence.toString();
        }
        
        @Override
        public int countIn(final CharSequence charSequence) {
            Preconditions.checkNotNull(charSequence);
            return 0;
        }
        
        @Override
        public int indexIn(final CharSequence charSequence) {
            Preconditions.checkNotNull(charSequence);
            return -1;
        }
        
        @Override
        public int indexIn(final CharSequence charSequence, final int n) {
            Preconditions.checkPositionIndex(n, charSequence.length());
            return -1;
        }
        
        @Override
        public int lastIndexIn(final CharSequence charSequence) {
            Preconditions.checkNotNull(charSequence);
            return -1;
        }
        
        @Override
        public boolean matches(final char c) {
            return false;
        }
        
        @Override
        public boolean matchesAllOf(final CharSequence charSequence) {
            return charSequence.length() == 0;
        }
        
        @Override
        public boolean matchesNoneOf(final CharSequence charSequence) {
            Preconditions.checkNotNull(charSequence);
            return true;
        }
        
        @Override
        public CharMatcher negate() {
            return CharMatcher.any();
        }
        
        @Override
        public CharMatcher or(final CharMatcher charMatcher) {
            return Preconditions.checkNotNull(charMatcher);
        }
        
        @Override
        public String removeFrom(final CharSequence charSequence) {
            return charSequence.toString();
        }
        
        @Override
        public String replaceFrom(final CharSequence charSequence, final char c) {
            return charSequence.toString();
        }
        
        @Override
        public String replaceFrom(final CharSequence charSequence, final CharSequence charSequence2) {
            Preconditions.checkNotNull(charSequence2);
            return charSequence.toString();
        }
        
        @Override
        public String trimFrom(final CharSequence charSequence) {
            return charSequence.toString();
        }
        
        @Override
        public String trimLeadingFrom(final CharSequence charSequence) {
            return charSequence.toString();
        }
        
        @Override
        public String trimTrailingFrom(final CharSequence charSequence) {
            return charSequence.toString();
        }
    }
    
    private static final class x extends CharMatcher
    {
        final CharMatcher a;
        final CharMatcher b;
        
        x(final CharMatcher charMatcher, final CharMatcher charMatcher2) {
            this.a = Preconditions.checkNotNull(charMatcher);
            this.b = Preconditions.checkNotNull(charMatcher2);
        }
        
        @Override
        public boolean matches(final char c) {
            return this.a.matches(c) || this.b.matches(c);
        }
        
        @Override
        void setBits(final BitSet set) {
            this.a.setBits(set);
            this.b.setBits(set);
        }
        
        @Override
        public String toString() {
            final StringBuilder sb = new StringBuilder();
            sb.append("CharMatcher.or(");
            sb.append(this.a);
            sb.append(", ");
            sb.append(this.b);
            sb.append(")");
            return sb.toString();
        }
    }
    
    private static class y extends CharMatcher
    {
        private final String a;
        private final char[] b;
        private final char[] c;
        
        y(final String a, final char[] b, final char[] c) {
            this.a = a;
            this.b = b;
            this.c = c;
            Preconditions.checkArgument(b.length == c.length);
            int n;
            for (int i = 0; i < b.length; i = n) {
                Preconditions.checkArgument(b[i] <= c[i]);
                n = i + 1;
                if (n < b.length) {
                    Preconditions.checkArgument(c[i] < b[n]);
                }
            }
        }
        
        @Override
        public boolean matches(final char key) {
            final int binarySearch = Arrays.binarySearch(this.b, key);
            boolean b = true;
            if (binarySearch >= 0) {
                return true;
            }
            final int n = ~binarySearch - 1;
            if (n < 0 || key > this.c[n]) {
                b = false;
            }
            return b;
        }
        
        @Override
        public String toString() {
            return this.a;
        }
    }
    
    private static final class z extends y
    {
        static final z a;
        
        static {
            a = new z();
        }
        
        private z() {
            super("CharMatcher.singleWidth()", "\u0000\u05be\u05d0\u05f3\u0600\u0750\u0e00\u1e00\u2100\ufb50\ufe70\uff61".toCharArray(), "\u04f9\u05be\u05ea\u05f4\u06ff\u077f\u0e7f\u20af\u213a\ufdff\ufeff\uffdc".toCharArray());
        }
    }
}

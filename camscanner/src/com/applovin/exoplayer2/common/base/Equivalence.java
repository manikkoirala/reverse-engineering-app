// 
// Decompiled by Procyon v0.6.0
// 

package com.applovin.exoplayer2.common.base;

import java.io.Serializable;
import org.checkerframework.checker.nullness.compatqual.NullableDecl;

public abstract class Equivalence<T>
{
    protected Equivalence() {
    }
    
    public static Equivalence<Object> equals() {
        return a.a;
    }
    
    public static Equivalence<Object> identity() {
        return c.a;
    }
    
    protected abstract boolean doEquivalent(final T p0, final T p1);
    
    protected abstract int doHash(final T p0);
    
    public final boolean equivalent(@NullableDecl final T t, @NullableDecl final T t2) {
        return t == t2 || (t != null && t2 != null && this.doEquivalent(t, t2));
    }
    
    public final Predicate<T> equivalentTo(@NullableDecl final T t) {
        return new b<T>(this, t);
    }
    
    public final int hash(@NullableDecl final T t) {
        if (t == null) {
            return 0;
        }
        return this.doHash(t);
    }
    
    public final <S extends T> Wrapper<S> wrap(@NullableDecl final S n) {
        return new Wrapper<S>(this, (Object)n);
    }
    
    public static final class Wrapper<T> implements Serializable
    {
        private static final long serialVersionUID = 0L;
        private final Equivalence<? super T> equivalence;
        @NullableDecl
        private final T reference;
        
        private Wrapper(final Equivalence<? super T> equivalence, @NullableDecl final T reference) {
            this.equivalence = Preconditions.checkNotNull(equivalence);
            this.reference = reference;
        }
        
        @Override
        public boolean equals(@NullableDecl final Object o) {
            if (o == this) {
                return true;
            }
            if (o instanceof Wrapper) {
                final Wrapper wrapper = (Wrapper)o;
                if (this.equivalence.equals(wrapper.equivalence)) {
                    return this.equivalence.equivalent((Object)this.reference, (Object)wrapper.reference);
                }
            }
            return false;
        }
        
        @NullableDecl
        public T get() {
            return this.reference;
        }
        
        @Override
        public int hashCode() {
            return this.equivalence.hash(this.reference);
        }
        
        @Override
        public String toString() {
            final StringBuilder sb = new StringBuilder();
            sb.append(this.equivalence);
            sb.append(".wrap(");
            sb.append(this.reference);
            sb.append(")");
            return sb.toString();
        }
    }
    
    static final class a extends Equivalence<Object> implements Serializable
    {
        static final a a;
        
        static {
            a = new a();
        }
        
        @Override
        protected boolean doEquivalent(final Object o, final Object obj) {
            return o.equals(obj);
        }
        
        @Override
        protected int doHash(final Object o) {
            return o.hashCode();
        }
    }
    
    private static final class b<T> implements Predicate<T>, Serializable
    {
        private final Equivalence<T> a;
        @NullableDecl
        private final T b;
        
        b(final Equivalence<T> equivalence, @NullableDecl final T b) {
            this.a = Preconditions.checkNotNull(equivalence);
            this.b = b;
        }
        
        @Override
        public boolean apply(@NullableDecl final T t) {
            return this.a.equivalent(t, this.b);
        }
        
        @Override
        public boolean equals(@NullableDecl final Object o) {
            boolean b = true;
            if (this == o) {
                return true;
            }
            if (o instanceof b) {
                final b b2 = (b)o;
                if (!this.a.equals(b2.a) || !Objects.equal(this.b, b2.b)) {
                    b = false;
                }
                return b;
            }
            return false;
        }
        
        @Override
        public int hashCode() {
            return Objects.hashCode(this.a, this.b);
        }
        
        @Override
        public String toString() {
            final StringBuilder sb = new StringBuilder();
            sb.append(this.a);
            sb.append(".equivalentTo(");
            sb.append(this.b);
            sb.append(")");
            return sb.toString();
        }
    }
    
    static final class c extends Equivalence<Object> implements Serializable
    {
        static final c a;
        
        static {
            a = new c();
        }
        
        @Override
        protected boolean doEquivalent(final Object o, final Object o2) {
            return false;
        }
        
        @Override
        protected int doHash(final Object o) {
            return System.identityHashCode(o);
        }
    }
}

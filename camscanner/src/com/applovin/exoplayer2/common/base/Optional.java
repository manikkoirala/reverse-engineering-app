// 
// Decompiled by Procyon v0.6.0
// 

package com.applovin.exoplayer2.common.base;

import java.util.Set;
import java.util.Iterator;
import org.checkerframework.checker.nullness.compatqual.NullableDecl;
import java.io.Serializable;

public abstract class Optional<T> implements Serializable
{
    private static final long serialVersionUID = 0L;
    
    Optional() {
    }
    
    public static <T> Optional<T> absent() {
        return a.a();
    }
    
    public static <T> Optional<T> fromNullable(@NullableDecl final T t) {
        Optional<Object> absent;
        if (t == null) {
            absent = absent();
        }
        else {
            absent = new d<Object>(t);
        }
        return (Optional<T>)absent;
    }
    
    public static <T> Optional<T> of(final T t) {
        return new d<T>(Preconditions.checkNotNull(t));
    }
    
    public static <T> Iterable<T> presentInstances(final Iterable<? extends Optional<? extends T>> iterable) {
        Preconditions.checkNotNull(iterable);
        return new Iterable<T>(iterable) {
            final Iterable a;
            
            @Override
            public Iterator<T> iterator() {
                return (Iterator<T>)new b<T>(this) {
                    final Optional$1 a;
                    private final Iterator<? extends Optional<? extends T>> b = Preconditions.checkNotNull(a.a.iterator());
                    
                    @Override
                    protected T a() {
                        while (this.b.hasNext()) {
                            final Optional optional = (Optional)this.b.next();
                            if (optional.isPresent()) {
                                return (T)optional.get();
                            }
                        }
                        return this.b();
                    }
                };
            }
        };
    }
    
    public abstract Set<T> asSet();
    
    @Override
    public abstract boolean equals(@NullableDecl final Object p0);
    
    public abstract T get();
    
    @Override
    public abstract int hashCode();
    
    public abstract boolean isPresent();
    
    public abstract Optional<T> or(final Optional<? extends T> p0);
    
    public abstract T or(final Supplier<? extends T> p0);
    
    public abstract T or(final T p0);
    
    @NullableDecl
    public abstract T orNull();
    
    @Override
    public abstract String toString();
    
    public abstract <V> Optional<V> transform(final Function<? super T, V> p0);
}

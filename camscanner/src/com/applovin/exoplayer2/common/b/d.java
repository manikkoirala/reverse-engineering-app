// 
// Decompiled by Procyon v0.6.0
// 

package com.applovin.exoplayer2.common.b;

import com.applovin.exoplayer2.common.base.Preconditions;

public final class d
{
    public static int a(final long n) {
        return (int)(n ^ n >>> 32);
    }
    
    public static int a(final long n, final long n2) {
        final long n3 = lcmp(n, n2);
        int n4;
        if (n3 < 0) {
            n4 = -1;
        }
        else if (n3 > 0) {
            n4 = 1;
        }
        else {
            n4 = 0;
        }
        return n4;
    }
    
    public static long a(final long... array) {
        final int length = array.length;
        int i = 1;
        Preconditions.checkArgument(length > 0);
        long n = array[0];
        while (i < array.length) {
            final long n2 = array[i];
            long n3 = n;
            if (n2 > n) {
                n3 = n2;
            }
            ++i;
            n = n3;
        }
        return n;
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.applovin.exoplayer2.common.b;

import org.checkerframework.checker.nullness.compatqual.NullableDecl;
import java.util.Arrays;
import java.util.RandomAccess;
import java.io.Serializable;
import java.util.AbstractList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import com.applovin.exoplayer2.common.base.Preconditions;

public final class c
{
    public static int a(final int n) {
        return n;
    }
    
    public static int a(int n, final int n2) {
        if (n < n2) {
            n = -1;
        }
        else if (n > n2) {
            n = 1;
        }
        else {
            n = 0;
        }
        return n;
    }
    
    public static int a(final long n) {
        final int n2 = (int)n;
        Preconditions.checkArgument(n2 == n, "Out of range: %s", n);
        return n2;
    }
    
    public static List<Integer> a(final int... array) {
        if (array.length == 0) {
            return Collections.emptyList();
        }
        return new a(array);
    }
    
    public static int[] a(final Collection<? extends Number> collection) {
        if (collection instanceof a) {
            return ((a)collection).a();
        }
        final Object[] array = collection.toArray();
        final int length = array.length;
        final int[] array2 = new int[length];
        for (int i = 0; i < length; ++i) {
            array2[i] = Preconditions.checkNotNull(array[i]).intValue();
        }
        return array2;
    }
    
    private static int c(final int[] array, final int n, int i, final int n2) {
        while (i < n2) {
            if (array[i] == n) {
                return i;
            }
            ++i;
        }
        return -1;
    }
    
    private static int d(final int[] array, final int n, final int n2, int i) {
        --i;
        while (i >= n2) {
            if (array[i] == n) {
                return i;
            }
            --i;
        }
        return -1;
    }
    
    private static class a extends AbstractList<Integer> implements Serializable, RandomAccess
    {
        final int[] a;
        final int b;
        final int c;
        
        a(final int[] array) {
            this(array, 0, array.length);
        }
        
        a(final int[] a, final int b, final int c) {
            this.a = a;
            this.b = b;
            this.c = c;
        }
        
        public Integer a(final int n) {
            Preconditions.checkElementIndex(n, this.size());
            return this.a[this.b + n];
        }
        
        public Integer a(final int n, final Integer n2) {
            Preconditions.checkElementIndex(n, this.size());
            final int[] a = this.a;
            final int b = this.b;
            final int i = a[b + n];
            a[b + n] = Preconditions.checkNotNull(n2);
            return i;
        }
        
        int[] a() {
            return Arrays.copyOfRange(this.a, this.b, this.c);
        }
        
        @Override
        public boolean contains(final Object o) {
            return o instanceof Integer && c(this.a, (int)o, this.b, this.c) != -1;
        }
        
        @Override
        public boolean equals(@NullableDecl final Object o) {
            if (o == this) {
                return true;
            }
            if (!(o instanceof a)) {
                return super.equals(o);
            }
            final a a = (a)o;
            final int size = this.size();
            if (a.size() != size) {
                return false;
            }
            for (int i = 0; i < size; ++i) {
                if (this.a[this.b + i] != a.a[a.b + i]) {
                    return false;
                }
            }
            return true;
        }
        
        @Override
        public int hashCode() {
            int i = this.b;
            int n = 1;
            while (i < this.c) {
                n = n * 31 + com.applovin.exoplayer2.common.b.c.a(this.a[i]);
                ++i;
            }
            return n;
        }
        
        @Override
        public int indexOf(final Object o) {
            if (o instanceof Integer) {
                final int a = c(this.a, (int)o, this.b, this.c);
                if (a >= 0) {
                    return a - this.b;
                }
            }
            return -1;
        }
        
        @Override
        public boolean isEmpty() {
            return false;
        }
        
        @Override
        public int lastIndexOf(final Object o) {
            if (o instanceof Integer) {
                final int b = d(this.a, (int)o, this.b, this.c);
                if (b >= 0) {
                    return b - this.b;
                }
            }
            return -1;
        }
        
        @Override
        public int size() {
            return this.c - this.b;
        }
        
        @Override
        public List<Integer> subList(final int n, final int n2) {
            Preconditions.checkPositionIndexes(n, n2, this.size());
            if (n == n2) {
                return Collections.emptyList();
            }
            final int[] a = this.a;
            final int b = this.b;
            return new a(a, n + b, b + n2);
        }
        
        @Override
        public String toString() {
            final StringBuilder sb = new StringBuilder(this.size() * 5);
            sb.append('[');
            sb.append(this.a[this.b]);
            int b = this.b;
            while (++b < this.c) {
                sb.append(", ");
                sb.append(this.a[b]);
            }
            sb.append(']');
            return sb.toString();
        }
    }
}

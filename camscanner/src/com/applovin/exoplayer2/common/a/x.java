// 
// Decompiled by Procyon v0.6.0
// 

package com.applovin.exoplayer2.common.a;

import java.util.Collection;
import java.util.NoSuchElementException;
import java.util.Iterator;
import java.util.List;
import org.checkerframework.checker.nullness.compatqual.NullableDecl;

public final class x
{
    @NullableDecl
    public static <T> T a(final Iterable<? extends T> iterable, @NullableDecl final T t) {
        return y.a(iterable.iterator(), t);
    }
    
    private static <T> T a(final List<T> list) {
        return list.get(list.size() - 1);
    }
    
    public static String a(final Iterable<?> iterable) {
        return y.a(iterable.iterator());
    }
    
    static Object[] b(final Iterable<?> iterable) {
        return d(iterable).toArray();
    }
    
    public static <T> T c(final Iterable<T> iterable) {
        if (!(iterable instanceof List)) {
            return y.b((Iterator<T>)iterable.iterator());
        }
        final List list = (List)iterable;
        if (!list.isEmpty()) {
            return a((List<T>)list);
        }
        throw new NoSuchElementException();
    }
    
    private static <E> Collection<E> d(final Iterable<E> iterable) {
        Collection<Object> a;
        if (iterable instanceof Collection) {
            a = (Collection)iterable;
        }
        else {
            a = aa.a((Iterator<?>)iterable.iterator());
        }
        return (Collection<E>)a;
    }
}

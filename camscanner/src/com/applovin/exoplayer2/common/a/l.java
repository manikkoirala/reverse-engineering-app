// 
// Decompiled by Procyon v0.6.0
// 

package com.applovin.exoplayer2.common.a;

import java.util.AbstractCollection;
import java.util.NoSuchElementException;
import java.util.ConcurrentModificationException;
import java.util.AbstractSet;
import java.util.Iterator;
import com.applovin.exoplayer2.common.base.Preconditions;
import java.util.Arrays;
import com.applovin.exoplayer2.common.base.Objects;
import org.checkerframework.checker.nullness.compatqual.NullableDecl;
import java.util.Collection;
import java.util.Map;
import java.util.Set;
import org.checkerframework.checker.nullness.compatqual.MonotonicNonNullDecl;
import java.io.Serializable;
import java.util.AbstractMap;

class l<K, V> extends AbstractMap<K, V> implements Serializable
{
    @MonotonicNonNullDecl
    transient long[] a;
    @MonotonicNonNullDecl
    transient Object[] b;
    @MonotonicNonNullDecl
    transient Object[] c;
    transient float d;
    transient int e;
    @MonotonicNonNullDecl
    private transient int[] f;
    private transient int g;
    private transient int h;
    @MonotonicNonNullDecl
    private transient Set<K> i;
    @MonotonicNonNullDecl
    private transient Set<Entry<K, V>> j;
    @MonotonicNonNullDecl
    private transient Collection<V> k;
    
    l() {
        this.a(3, 1.0f);
    }
    
    l(final int n) {
        this(n, 1.0f);
    }
    
    l(final int n, final float n2) {
        this.a(n, n2);
    }
    
    private static int a(final long n) {
        return (int)(n >>> 32);
    }
    
    private int a(@NullableDecl final Object o) {
        final int a = p.a(o);
        long n;
        for (int i = this.f[this.i() & a]; i != -1; i = b(n)) {
            n = this.a[i];
            if (a(n) == a && Objects.equal(o, this.b[i])) {
                return i;
            }
        }
        return -1;
    }
    
    private static long a(final long n, final int n2) {
        return (n & 0xFFFFFFFF00000000L) | (0xFFFFFFFFL & (long)n2);
    }
    
    public static <K, V> l<K, V> a() {
        return new l<K, V>();
    }
    
    public static <K, V> l<K, V> a(final int n) {
        return new l<K, V>(n);
    }
    
    @NullableDecl
    private V a(@NullableDecl Object o, final int n) {
        final int n2 = this.i() & n;
        int n3 = this.f[n2];
        if (n3 == -1) {
            return null;
        }
        int n4 = -1;
        while (a(this.a[n3]) != n || !Objects.equal(o, this.b[n3])) {
            final int b = b(this.a[n3]);
            if (b == -1) {
                return null;
            }
            n4 = n3;
            n3 = b;
        }
        o = this.c[n3];
        if (n4 == -1) {
            this.f[n2] = b(this.a[n3]);
        }
        else {
            final long[] a = this.a;
            a[n4] = a(a[n4], b(a[n3]));
        }
        this.d(n3);
        --this.h;
        ++this.e;
        return (V)o;
    }
    
    private static int b(final long n) {
        return (int)n;
    }
    
    private static int[] f(final int n) {
        final int[] a = new int[n];
        Arrays.fill(a, -1);
        return a;
    }
    
    private static long[] g(final int n) {
        final long[] a = new long[n];
        Arrays.fill(a, -1L);
        return a;
    }
    
    private void h(int n) {
        final int length = this.a.length;
        if (n > length) {
            if ((n = Math.max(1, length >>> 1) + length) < 0) {
                n = Integer.MAX_VALUE;
            }
            if (n != length) {
                this.c(n);
            }
        }
    }
    
    private int i() {
        return this.f.length - 1;
    }
    
    private void i(int i) {
        if (this.f.length >= 1073741824) {
            this.g = Integer.MAX_VALUE;
            return;
        }
        final int n = (int)(i * this.d);
        final int[] f = f(i);
        final long[] a = this.a;
        final int length = f.length;
        int a2;
        int n2;
        for (i = 0; i < this.h; ++i) {
            a2 = a(a[i]);
            n2 = (a2 & length - 1);
            a[f[n2] = i] = ((long)a2 << 32 | ((long)f[n2] & 0xFFFFFFFFL));
        }
        this.g = n + 1;
        this.f = f;
    }
    
    private V j(final int n) {
        return this.a(this.b[n], a(this.a[n]));
    }
    
    int a(final int n, final int n2) {
        return n - 1;
    }
    
    void a(final int n, final float d) {
        final boolean b = false;
        Preconditions.checkArgument(n >= 0, (Object)"Initial capacity must be non-negative");
        boolean b2 = b;
        if (d > 0.0f) {
            b2 = true;
        }
        Preconditions.checkArgument(b2, (Object)"Illegal load factor");
        final int a = p.a(n, d);
        this.f = f(a);
        this.d = d;
        this.b = new Object[n];
        this.c = new Object[n];
        this.a = g(n);
        this.g = Math.max(1, (int)(a * d));
    }
    
    void a(final int n, @NullableDecl final K k, @NullableDecl final V v, final int n2) {
        this.a[n] = ((long)n2 << 32 | 0xFFFFFFFFL);
        this.b[n] = k;
        this.c[n] = v;
    }
    
    int b() {
        int n;
        if (this.isEmpty()) {
            n = -1;
        }
        else {
            n = 0;
        }
        return n;
    }
    
    void b(final int n) {
    }
    
    Set<K> c() {
        return new c();
    }
    
    void c(final int n) {
        this.b = Arrays.copyOf(this.b, n);
        this.c = Arrays.copyOf(this.c, n);
        final long[] a = this.a;
        final int length = a.length;
        final long[] copy = Arrays.copyOf(a, n);
        if (n > length) {
            Arrays.fill(copy, length, n, -1L);
        }
        this.a = copy;
    }
    
    @Override
    public void clear() {
        ++this.e;
        Arrays.fill(this.b, 0, this.h, null);
        Arrays.fill(this.c, 0, this.h, null);
        Arrays.fill(this.f, -1);
        Arrays.fill(this.a, -1L);
        this.h = 0;
    }
    
    @Override
    public boolean containsKey(@NullableDecl final Object o) {
        return this.a(o) != -1;
    }
    
    @Override
    public boolean containsValue(@NullableDecl final Object o) {
        for (int i = 0; i < this.h; ++i) {
            if (Objects.equal(o, this.c[i])) {
                return true;
            }
        }
        return false;
    }
    
    Iterator<K> d() {
        return new b<K>(this) {
            final l a;
            
            @Override
            K a(final int n) {
                return (K)this.a.b[n];
            }
        };
    }
    
    void d(final int n) {
        final int n2 = this.size() - 1;
        if (n < n2) {
            final Object[] b = this.b;
            b[n] = b[n2];
            final Object[] c = this.c;
            c[n] = c[n2];
            c[n2] = (b[n2] = null);
            final long[] a = this.a;
            final long n3 = a[n2];
            a[n] = n3;
            a[n2] = -1L;
            final int n4 = a(n3) & this.i();
            final int[] f = this.f;
            int n5;
            if ((n5 = f[n4]) == n2) {
                f[n4] = n;
            }
            else {
                long n6;
                while (true) {
                    n6 = this.a[n5];
                    final int b2 = b(n6);
                    if (b2 == n2) {
                        break;
                    }
                    n5 = b2;
                }
                this.a[n5] = a(n6, n);
            }
        }
        else {
            this.b[n] = null;
            this.c[n] = null;
            this.a[n] = -1L;
        }
    }
    
    int e(int n) {
        if (++n >= this.h) {
            n = -1;
        }
        return n;
    }
    
    Set<Entry<K, V>> e() {
        return new a();
    }
    
    @Override
    public Set<Entry<K, V>> entrySet() {
        Set<Entry<K, V>> j;
        if ((j = this.j) == null) {
            j = this.e();
            this.j = j;
        }
        return j;
    }
    
    Iterator<Entry<K, V>> f() {
        return new b<Entry<K, V>>(this) {
            final l a;
            
            Entry<K, V> b(final int n) {
                return this.a.new d(n);
            }
        };
    }
    
    Collection<V> g() {
        return new e();
    }
    
    @Override
    public V get(@NullableDecl Object o) {
        final int a = this.a(o);
        this.b(a);
        if (a == -1) {
            o = null;
        }
        else {
            o = this.c[a];
        }
        return (V)o;
    }
    
    Iterator<V> h() {
        return new b<V>(this) {
            final l a;
            
            @Override
            V a(final int n) {
                return (V)this.a.c[n];
            }
        };
    }
    
    @Override
    public boolean isEmpty() {
        return this.h == 0;
    }
    
    @Override
    public Set<K> keySet() {
        Set<K> i;
        if ((i = this.i) == null) {
            i = this.c();
            this.i = i;
        }
        return i;
    }
    
    @NullableDecl
    @Override
    public V put(@NullableDecl final K k, @NullableDecl final V v) {
        final long[] a = this.a;
        final Object[] b = this.b;
        final Object[] c = this.c;
        final int a2 = p.a(k);
        final int n = this.i() & a2;
        final int h = this.h;
        final int[] f = this.f;
        int n2;
        if ((n2 = f[n]) == -1) {
            f[n] = h;
        }
        else {
            while (true) {
                final long n3 = a[n2];
                if (a(n3) == a2 && Objects.equal(k, b[n2])) {
                    final Object o = c[n2];
                    c[n2] = v;
                    this.b(n2);
                    return (V)o;
                }
                final int b2 = b(n3);
                if (b2 == -1) {
                    a[n2] = a(n3, h);
                    break;
                }
                n2 = b2;
            }
        }
        if (h != Integer.MAX_VALUE) {
            final int h2 = h + 1;
            this.h(h2);
            this.a(h, k, v, a2);
            this.h = h2;
            if (h >= this.g) {
                this.i(this.f.length * 2);
            }
            ++this.e;
            return null;
        }
        throw new IllegalStateException("Cannot contain more than Integer.MAX_VALUE elements!");
    }
    
    @NullableDecl
    @Override
    public V remove(@NullableDecl final Object o) {
        return this.a(o, p.a(o));
    }
    
    @Override
    public int size() {
        return this.h;
    }
    
    @Override
    public Collection<V> values() {
        Collection<V> k;
        if ((k = this.k) == null) {
            k = this.g();
            this.k = k;
        }
        return k;
    }
    
    class a extends AbstractSet<Entry<K, V>>
    {
        final l a;
        
        a(final l a) {
            this.a = a;
        }
        
        @Override
        public void clear() {
            this.a.clear();
        }
        
        @Override
        public boolean contains(@NullableDecl final Object o) {
            final boolean b = o instanceof Entry;
            boolean b3;
            final boolean b2 = b3 = false;
            if (b) {
                final Entry entry = (Entry)o;
                final int a = this.a.a(entry.getKey());
                b3 = b2;
                if (a != -1) {
                    b3 = b2;
                    if (Objects.equal(this.a.c[a], entry.getValue())) {
                        b3 = true;
                    }
                }
            }
            return b3;
        }
        
        @Override
        public Iterator<Entry<K, V>> iterator() {
            return this.a.f();
        }
        
        @Override
        public boolean remove(@NullableDecl final Object o) {
            if (o instanceof Entry) {
                final Entry entry = (Entry)o;
                final int a = this.a.a(entry.getKey());
                if (a != -1 && Objects.equal(this.a.c[a], entry.getValue())) {
                    this.a.j(a);
                    return true;
                }
            }
            return false;
        }
        
        @Override
        public int size() {
            return this.a.h;
        }
    }
    
    private abstract class b<T> implements Iterator<T>
    {
        int b;
        int c;
        int d;
        final l e;
        
        private b(final l e) {
            this.e = e;
            this.b = e.e;
            this.c = e.b();
            this.d = -1;
        }
        
        private void a() {
            if (this.e.e == this.b) {
                return;
            }
            throw new ConcurrentModificationException();
        }
        
        abstract T a(final int p0);
        
        @Override
        public boolean hasNext() {
            return this.c >= 0;
        }
        
        @Override
        public T next() {
            this.a();
            if (this.hasNext()) {
                final int c = this.c;
                this.d = c;
                final T a = this.a(c);
                this.c = this.e.e(this.c);
                return a;
            }
            throw new NoSuchElementException();
        }
        
        @Override
        public void remove() {
            this.a();
            com.applovin.exoplayer2.common.a.j.a(this.d >= 0);
            ++this.b;
            this.e.j(this.d);
            this.c = this.e.a(this.c, this.d);
            this.d = -1;
        }
    }
    
    class c extends AbstractSet<K>
    {
        final l a;
        
        c(final l a) {
            this.a = a;
        }
        
        @Override
        public void clear() {
            this.a.clear();
        }
        
        @Override
        public boolean contains(final Object o) {
            return this.a.containsKey(o);
        }
        
        @Override
        public Iterator<K> iterator() {
            return this.a.d();
        }
        
        @Override
        public boolean remove(@NullableDecl final Object o) {
            final int a = this.a.a(o);
            if (a == -1) {
                return false;
            }
            this.a.j(a);
            return true;
        }
        
        @Override
        public int size() {
            return this.a.h;
        }
    }
    
    final class d extends e<K, V>
    {
        final l a;
        @NullableDecl
        private final K b;
        private int c;
        
        d(final l a, final int c) {
            this.a = a;
            this.b = (K)a.b[c];
            this.c = c;
        }
        
        private void a() {
            final int c = this.c;
            if (c == -1 || c >= this.a.size() || !Objects.equal(this.b, this.a.b[this.c])) {
                this.c = this.a.a(this.b);
            }
        }
        
        @Override
        public K getKey() {
            return this.b;
        }
        
        @Override
        public V getValue() {
            this.a();
            final int c = this.c;
            Object o;
            if (c == -1) {
                o = null;
            }
            else {
                o = this.a.c[c];
            }
            return (V)o;
        }
        
        @Override
        public V setValue(final V v) {
            this.a();
            final int c = this.c;
            if (c == -1) {
                this.a.put(this.b, v);
                return null;
            }
            final Object[] c2 = this.a.c;
            final Object o = c2[c];
            c2[c] = v;
            return (V)o;
        }
    }
    
    class e extends AbstractCollection<V>
    {
        final l a;
        
        e(final l a) {
            this.a = a;
        }
        
        @Override
        public void clear() {
            this.a.clear();
        }
        
        @Override
        public Iterator<V> iterator() {
            return this.a.h();
        }
        
        @Override
        public int size() {
            return this.a.h;
        }
    }
}

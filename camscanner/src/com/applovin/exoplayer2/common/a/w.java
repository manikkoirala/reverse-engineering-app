// 
// Decompiled by Procyon v0.6.0
// 

package com.applovin.exoplayer2.common.a;

import java.util.Iterator;
import java.util.SortedSet;
import java.util.Collection;
import java.util.Arrays;
import com.applovin.exoplayer2.common.base.Preconditions;
import com.google.j2objc.annotations.RetainedWith;
import org.checkerframework.checker.nullness.compatqual.NullableDecl;
import java.util.Set;

public abstract class w<E> extends q<E> implements Set<E>
{
    @NullableDecl
    @RetainedWith
    private transient s<E> a;
    
    w() {
    }
    
    static int a(int a) {
        final int max = Math.max(a, 2);
        boolean b = true;
        if (max < 751619276) {
            for (a = Integer.highestOneBit(max - 1) << 1; a * 0.7 < max; a <<= 1) {}
            return a;
        }
        if (max >= 1073741824) {
            b = false;
        }
        Preconditions.checkArgument(b, (Object)"collection too large");
        return 1073741824;
    }
    
    private static <E> w<E> a(final int toIndex, final Object... array) {
        if (toIndex == 0) {
            return g();
        }
        if (toIndex == 1) {
            return a(array[0]);
        }
        final int a = a(toIndex);
        final Object[] array2 = new Object[a];
        final int n = a - 1;
        int i = 0;
        int n2 = 0;
        int n3 = 0;
        while (i < toIndex) {
            final Object a2 = ah.a(array[i], i);
            final int hashCode = a2.hashCode();
            int a3 = p.a(hashCode);
            while (true) {
                final int n4 = a3 & n;
                final Object o = array2[n4];
                if (o == null) {
                    array2[n4] = (array[n3] = a2);
                    n2 += hashCode;
                    ++n3;
                    break;
                }
                if (o.equals(a2)) {
                    break;
                }
                ++a3;
            }
            ++i;
        }
        Arrays.fill(array, n3, toIndex, null);
        if (n3 == 1) {
            return new ar<E>(array[0], n2);
        }
        if (a(n3) < a / 2) {
            return (w<E>)a(n3, array);
        }
        Object[] copy = array;
        if (a(n3, array.length)) {
            copy = Arrays.copyOf(array, n3);
        }
        return new am<E>(copy, n2, array2, n, n3);
    }
    
    public static <E> w<E> a(final E e) {
        return new ar<E>(e);
    }
    
    public static <E> w<E> a(final E e, final E e2) {
        return a(2, new Object[] { e, e2 });
    }
    
    public static <E> w<E> a(final E e, final E e2, final E e3) {
        return a(3, e, e2, e3);
    }
    
    public static <E> w<E> a(final Collection<? extends E> collection) {
        if (collection instanceof w && !(collection instanceof SortedSet)) {
            final w w = (w)collection;
            if (!w.f()) {
                return w;
            }
        }
        final Object[] array = collection.toArray();
        return (w<E>)a(array.length, array);
    }
    
    public static <E> w<E> a(final E[] array) {
        final int length = array.length;
        if (length == 0) {
            return g();
        }
        if (length != 1) {
            return a(array.length, (Object[])array.clone());
        }
        return a(array[0]);
    }
    
    private static boolean a(final int n, final int n2) {
        return n < (n2 >> 1) + (n2 >> 2);
    }
    
    public static <E> w<E> g() {
        return (w<E>)am.a;
    }
    
    @Override
    public abstract ax<E> a();
    
    @Override
    public s<E> e() {
        s<E> a;
        if ((a = this.a) == null) {
            a = this.i();
            this.a = a;
        }
        return a;
    }
    
    @Override
    public boolean equals(@NullableDecl final Object o) {
        return o == this || ((!(o instanceof w) || !this.h() || !((w)o).h() || this.hashCode() == o.hashCode()) && aq.a(this, o));
    }
    
    boolean h() {
        return false;
    }
    
    @Override
    public int hashCode() {
        return aq.a(this);
    }
    
    s<E> i() {
        return s.b(this.toArray());
    }
}

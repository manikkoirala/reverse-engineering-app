// 
// Decompiled by Procyon v0.6.0
// 

package com.applovin.exoplayer2.common.a;

import java.util.Map;
import java.util.Collection;
import org.checkerframework.checker.nullness.compatqual.NullableDecl;

public interface ac<K, V>
{
    boolean a(@NullableDecl final K p0, @NullableDecl final V p1);
    
    Collection<V> b(@NullableDecl final K p0);
    
    Map<K, Collection<V>> b();
    
    boolean b(@NullableDecl final Object p0, @NullableDecl final Object p1);
    
    boolean c(@NullableDecl final Object p0, @NullableDecl final Object p1);
    
    int d();
    
    void e();
    
    Collection<V> h();
}

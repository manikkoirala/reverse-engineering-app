// 
// Decompiled by Procyon v0.6.0
// 

package com.applovin.exoplayer2.common.a;

import com.applovin.exoplayer2.common.base.Preconditions;
import org.checkerframework.checker.nullness.compatqual.NullableDecl;
import java.util.Collection;

public final class k
{
    static StringBuilder a(final int n) {
        j.a(n, "size");
        return new StringBuilder((int)Math.min(n * 8L, 1073741824L));
    }
    
    static boolean a(final Collection<?> collection, @NullableDecl final Object o) {
        Preconditions.checkNotNull(collection);
        try {
            return collection.contains(o);
        }
        catch (final ClassCastException | NullPointerException ex) {
            return false;
        }
    }
}

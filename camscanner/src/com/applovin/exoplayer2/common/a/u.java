// 
// Decompiled by Procyon v0.6.0
// 

package com.applovin.exoplayer2.common.a;

import com.applovin.exoplayer2.common.base.Function;
import java.util.AbstractMap;
import java.util.Iterator;
import java.util.Arrays;
import org.checkerframework.checker.nullness.compatqual.MonotonicNonNullDecl;
import java.util.Comparator;
import java.util.Set;
import org.checkerframework.checker.nullness.compatqual.NullableDecl;
import java.util.SortedMap;
import java.util.Collection;
import com.google.j2objc.annotations.RetainedWith;
import java.util.Map;
import java.io.Serializable;

public abstract class u<K, V> implements Serializable, Map<K, V>
{
    static final Entry<?, ?>[] a;
    private transient w<Entry<K, V>> b;
    @RetainedWith
    private transient w<K> c;
    @RetainedWith
    private transient q<V> d;
    
    static {
        a = new Entry[0];
    }
    
    u() {
    }
    
    public static <K, V> u<K, V> a() {
        return (u<K, V>)al.b;
    }
    
    public static <K, V> u<K, V> a(final Iterable<? extends Entry<? extends K, ? extends V>> iterable) {
        int size;
        if (iterable instanceof Collection) {
            size = ((Collection)iterable).size();
        }
        else {
            size = 4;
        }
        final a a = new a(size);
        a.a((Iterable)iterable);
        return a.a();
    }
    
    public static <K, V> u<K, V> a(final Map<? extends K, ? extends V> map) {
        if (map instanceof u && !(map instanceof SortedMap)) {
            final u u = (u)map;
            if (!u.i()) {
                return u;
            }
        }
        return a((Iterable<? extends Entry<? extends K, ? extends V>>)map.entrySet());
    }
    
    public static <K, V> a<K, V> b() {
        return new a<K, V>();
    }
    
    public w<Entry<K, V>> c() {
        w<Entry<K, V>> b;
        if ((b = this.b) == null) {
            b = this.d();
            this.b = b;
        }
        return b;
    }
    
    @Deprecated
    @Override
    public final void clear() {
        throw new UnsupportedOperationException();
    }
    
    @Override
    public boolean containsKey(@NullableDecl final Object o) {
        return this.get(o) != null;
    }
    
    @Override
    public boolean containsValue(@NullableDecl final Object o) {
        return this.g().contains(o);
    }
    
    abstract w<Entry<K, V>> d();
    
    public w<K> e() {
        w<K> c;
        if ((c = this.c) == null) {
            c = this.f();
            this.c = c;
        }
        return c;
    }
    
    @Override
    public boolean equals(@NullableDecl final Object o) {
        return ab.d(this, o);
    }
    
    abstract w<K> f();
    
    public q<V> g() {
        q<V> d;
        if ((d = this.d) == null) {
            d = this.h();
            this.d = d;
        }
        return d;
    }
    
    @Override
    public abstract V get(@NullableDecl final Object p0);
    
    @Override
    public final V getOrDefault(@NullableDecl Object value, @NullableDecl V v) {
        value = this.get(value);
        if (value != null) {
            v = (V)value;
        }
        return v;
    }
    
    abstract q<V> h();
    
    @Override
    public int hashCode() {
        return aq.a(this.c());
    }
    
    abstract boolean i();
    
    @Override
    public boolean isEmpty() {
        return this.size() == 0;
    }
    
    @Deprecated
    @Override
    public final V put(final K k, final V v) {
        throw new UnsupportedOperationException();
    }
    
    @Deprecated
    @Override
    public final void putAll(final Map<? extends K, ? extends V> map) {
        throw new UnsupportedOperationException();
    }
    
    @Deprecated
    @Override
    public final V remove(final Object o) {
        throw new UnsupportedOperationException();
    }
    
    @Override
    public String toString() {
        return ab.a(this);
    }
    
    public static class a<K, V>
    {
        @MonotonicNonNullDecl
        Comparator<? super V> a;
        Object[] b;
        int c;
        boolean d;
        
        public a() {
            this(4);
        }
        
        a(final int n) {
            this.b = new Object[n * 2];
            this.c = 0;
            this.d = false;
        }
        
        private void a(int n) {
            n *= 2;
            final Object[] b = this.b;
            if (n > b.length) {
                this.b = Arrays.copyOf(b, q.b.a(b.length, n));
                this.d = false;
            }
        }
        
        public a<K, V> a(final Iterable<? extends Entry<? extends K, ? extends V>> iterable) {
            if (iterable instanceof Collection) {
                this.a(this.c + ((Collection)iterable).size());
            }
            final Iterator iterator = iterable.iterator();
            while (iterator.hasNext()) {
                this.a((Entry<? extends K, ? extends V>)iterator.next());
            }
            return this;
        }
        
        public a<K, V> a(final K k, final V v) {
            this.a(this.c + 1);
            j.a(k, v);
            final Object[] b = this.b;
            final int c = this.c;
            b[c * 2] = k;
            b[c * 2 + 1] = v;
            this.c = c + 1;
            return this;
        }
        
        public a<K, V> a(final Entry<? extends K, ? extends V> entry) {
            return (a<K, V>)this.a(entry.getKey(), entry.getValue());
        }
        
        public u<K, V> a() {
            this.b();
            this.d = true;
            return (u<K, V>)al.a(this.c, this.b);
        }
        
        void b() {
            if (this.a != null) {
                if (this.d) {
                    this.b = Arrays.copyOf(this.b, this.c * 2);
                }
                final Entry[] a = new Entry[this.c];
                final int n = 0;
                int n2 = 0;
                int c;
                while (true) {
                    c = this.c;
                    if (n2 >= c) {
                        break;
                    }
                    final Object[] b = this.b;
                    final int n3 = n2 * 2;
                    a[n2] = (Entry)new AbstractMap.SimpleImmutableEntry(b[n3], b[n3 + 1]);
                    ++n2;
                }
                Arrays.sort((Entry[])a, 0, c, (Comparator<? super Entry>)ai.a(this.a).a((Function<Entry<?, ? extends V>, ? extends V>)ab.b()));
                for (int i = n; i < this.c; ++i) {
                    final Object[] b2 = this.b;
                    final int n4 = i * 2;
                    b2[n4] = a[i].getKey();
                    this.b[n4 + 1] = a[i].getValue();
                }
            }
        }
    }
}

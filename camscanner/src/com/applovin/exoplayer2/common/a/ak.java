// 
// Decompiled by Procyon v0.6.0
// 

package com.applovin.exoplayer2.common.a;

import com.applovin.exoplayer2.common.base.Preconditions;

class ak<E> extends s<E>
{
    static final s<Object> a;
    final transient Object[] b;
    private final transient int c;
    
    static {
        a = new ak<Object>(new Object[0], 0);
    }
    
    ak(final Object[] b, final int c) {
        this.b = b;
        this.c = c;
    }
    
    @Override
    int a(final Object[] array, final int n) {
        System.arraycopy(this.b, 0, array, n, this.c);
        return n + this.c;
    }
    
    @Override
    Object[] b() {
        return this.b;
    }
    
    @Override
    int c() {
        return 0;
    }
    
    @Override
    int d() {
        return this.c;
    }
    
    @Override
    boolean f() {
        return false;
    }
    
    @Override
    public E get(final int n) {
        Preconditions.checkElementIndex(n, this.c);
        return (E)this.b[n];
    }
    
    @Override
    public int size() {
        return this.c;
    }
}

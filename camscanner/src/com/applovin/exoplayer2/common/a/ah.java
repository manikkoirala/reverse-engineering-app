// 
// Decompiled by Procyon v0.6.0
// 

package com.applovin.exoplayer2.common.a;

public final class ah
{
    static Object a(final Object o, final int i) {
        if (o != null) {
            return o;
        }
        final StringBuilder sb = new StringBuilder();
        sb.append("at index ");
        sb.append(i);
        throw new NullPointerException(sb.toString());
    }
    
    static Object[] a(final Object... array) {
        return b(array, array.length);
    }
    
    public static <T> T[] a(final T[] array, final int n) {
        return aj.a(array, n);
    }
    
    static Object[] b(final Object[] array, final int n) {
        for (int i = 0; i < n; ++i) {
            a(array[i], i);
        }
        return array;
    }
}

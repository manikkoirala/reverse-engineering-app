// 
// Decompiled by Procyon v0.6.0
// 

package com.applovin.exoplayer2.common.a;

import java.util.AbstractSet;
import java.util.Map;
import org.checkerframework.checker.nullness.compatqual.NullableDecl;
import java.util.HashSet;
import java.util.Collections;
import java.util.Collection;
import com.applovin.exoplayer2.common.base.Preconditions;
import java.util.Iterator;
import java.util.Set;

public final class aq
{
    static int a(final Set<?> set) {
        final Iterator<?> iterator = set.iterator();
        int n = 0;
        while (iterator.hasNext()) {
            final Object next = iterator.next();
            int hashCode;
            if (next != null) {
                hashCode = next.hashCode();
            }
            else {
                hashCode = 0;
            }
            n = ~(~(n + hashCode));
        }
        return n;
    }
    
    public static <E> b<E> a(final Set<E> set, final Set<?> set2) {
        Preconditions.checkNotNull(set, (Object)"set1");
        Preconditions.checkNotNull(set2, (Object)"set2");
        return (b<E>)new b<E>(set, set2) {
            final Set a;
            final Set b;
            
            @Override
            public ax<E> a() {
                return new com.applovin.exoplayer2.common.a.b<E>(this) {
                    final Iterator<E> a = b.a.iterator();
                    final aq$2 b;
                    
                    @Override
                    protected E a() {
                        while (this.a.hasNext()) {
                            final E next = this.a.next();
                            if (this.b.b.contains(next)) {
                                return next;
                            }
                        }
                        return this.b();
                    }
                };
            }
            
            @Override
            public boolean contains(final Object o) {
                return this.a.contains(o) && this.b.contains(o);
            }
            
            @Override
            public boolean containsAll(final Collection<?> collection) {
                return this.a.containsAll(collection) && this.b.containsAll(collection);
            }
            
            @Override
            public boolean isEmpty() {
                return Collections.disjoint(this.b, this.a);
            }
            
            @Override
            public int size() {
                final Iterator iterator = this.a.iterator();
                int n = 0;
                while (iterator.hasNext()) {
                    if (this.b.contains(iterator.next())) {
                        ++n;
                    }
                }
                return n;
            }
        };
    }
    
    public static <E> HashSet<E> a() {
        return new HashSet<E>();
    }
    
    public static <E> HashSet<E> a(final int n) {
        return new HashSet<E>(ab.a(n));
    }
    
    static boolean a(final Set<?> set, @NullableDecl final Object o) {
        boolean b = true;
        if (set == o) {
            return true;
        }
        if (!(o instanceof Set)) {
            return false;
        }
        final Set set2 = (Set)o;
        try {
            if (set.size() != set2.size() || !set.containsAll(set2)) {
                b = false;
            }
            return b;
        }
        catch (final NullPointerException | ClassCastException ex) {
            return false;
        }
    }
    
    static boolean a(final Set<?> set, final Collection<?> collection) {
        Preconditions.checkNotNull(collection);
        Object a = collection;
        if (collection instanceof af) {
            a = ((af)collection).a();
        }
        if (a instanceof Set && ((Collection)a).size() > set.size()) {
            return y.a(set.iterator(), (Collection<?>)a);
        }
        return a(set, ((Collection<?>)a).iterator());
    }
    
    static boolean a(final Set<?> set, final Iterator<?> iterator) {
        boolean b = false;
        while (iterator.hasNext()) {
            b |= set.remove(iterator.next());
        }
        return b;
    }
    
    public static <E> Set<E> b() {
        return Collections.newSetFromMap((Map<E, Boolean>)ab.c());
    }
    
    abstract static class a<E> extends AbstractSet<E>
    {
        @Override
        public boolean removeAll(final Collection<?> collection) {
            return aq.a(this, collection);
        }
        
        @Override
        public boolean retainAll(final Collection<?> collection) {
            return super.retainAll(Preconditions.checkNotNull(collection));
        }
    }
    
    public abstract static class b<E> extends AbstractSet<E>
    {
        private b() {
        }
        
        public abstract ax<E> a();
        
        @Deprecated
        @Override
        public final boolean add(final E e) {
            throw new UnsupportedOperationException();
        }
        
        @Deprecated
        @Override
        public final boolean addAll(final Collection<? extends E> collection) {
            throw new UnsupportedOperationException();
        }
        
        @Deprecated
        @Override
        public final void clear() {
            throw new UnsupportedOperationException();
        }
        
        @Deprecated
        @Override
        public final boolean remove(final Object o) {
            throw new UnsupportedOperationException();
        }
        
        @Deprecated
        @Override
        public final boolean removeAll(final Collection<?> collection) {
            throw new UnsupportedOperationException();
        }
        
        @Deprecated
        @Override
        public final boolean retainAll(final Collection<?> collection) {
            throw new UnsupportedOperationException();
        }
    }
}

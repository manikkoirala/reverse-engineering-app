// 
// Decompiled by Procyon v0.6.0
// 

package com.applovin.exoplayer2.common.a;

import org.checkerframework.checker.nullness.compatqual.NullableDecl;
import com.applovin.exoplayer2.common.base.Preconditions;
import java.util.Comparator;
import java.io.Serializable;

final class m<T> extends ai<T> implements Serializable
{
    final Comparator<T> a;
    
    m(final Comparator<T> comparator) {
        this.a = Preconditions.checkNotNull(comparator);
    }
    
    @Override
    public int compare(final T t, final T t2) {
        return this.a.compare(t, t2);
    }
    
    @Override
    public boolean equals(@NullableDecl final Object o) {
        return o == this || (o instanceof m && this.a.equals(((m)o).a));
    }
    
    @Override
    public int hashCode() {
        return this.a.hashCode();
    }
    
    @Override
    public String toString() {
        return this.a.toString();
    }
}

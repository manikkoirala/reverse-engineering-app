// 
// Decompiled by Procyon v0.6.0
// 

package com.applovin.exoplayer2.common.a;

import com.google.j2objc.annotations.Weak;
import java.util.ArrayList;
import java.util.Arrays;
import org.checkerframework.checker.nullness.compatqual.MonotonicNonNullDecl;
import java.util.Comparator;
import java.util.Iterator;
import java.util.Set;
import org.checkerframework.checker.nullness.compatqual.NullableDecl;
import java.util.Map;
import java.util.Collection;
import java.io.Serializable;

public abstract class v<K, V> extends g<K, V> implements Serializable
{
    final transient u<K, ? extends q<V>> b;
    final transient int c;
    
    v(final u<K, ? extends q<V>> b, final int c) {
        this.b = b;
        this.c = c;
    }
    
    @Deprecated
    @Override
    public boolean a(final K k, final V v) {
        throw new UnsupportedOperationException();
    }
    
    @Deprecated
    @Override
    public boolean c(final Object o, final Object o2) {
        throw new UnsupportedOperationException();
    }
    
    @Override
    public int d() {
        return this.c;
    }
    
    @Override
    public boolean d(@NullableDecl final Object o) {
        return o != null && super.d(o);
    }
    
    public abstract q<V> e(final K p0);
    
    @Deprecated
    @Override
    public void e() {
        throw new UnsupportedOperationException();
    }
    
    @Override
    Set<K> f() {
        throw new AssertionError((Object)"unreachable");
    }
    
    boolean g() {
        return this.b.i();
    }
    
    @Override
    Map<K, Collection<V>> n() {
        throw new AssertionError((Object)"should never be called");
    }
    
    public w<K> o() {
        return this.b.e();
    }
    
    public u<K, Collection<V>> q() {
        return (u<K, Collection<V>>)this.b;
    }
    
    public q<Map.Entry<K, V>> r() {
        return (q)super.k();
    }
    
    q<Map.Entry<K, V>> s() {
        return (q<Map.Entry<K, V>>)new b((v<Object, Object>)this);
    }
    
    ax<Map.Entry<K, V>> t() {
        return new ax<Map.Entry<K, V>>(this) {
            final Iterator<? extends Map.Entry<K, ? extends q<V>>> a = d.b.c().a();
            K b = null;
            Iterator<V> c = y.a();
            final v d;
            
            public Map.Entry<K, V> a() {
                if (!this.c.hasNext()) {
                    final Map.Entry entry = (Map.Entry)this.a.next();
                    this.b = entry.getKey();
                    this.c = ((q)entry.getValue()).a();
                }
                return ab.a(this.b, this.c.next());
            }
            
            @Override
            public boolean hasNext() {
                return this.c.hasNext() || this.a.hasNext();
            }
        };
    }
    
    public q<V> u() {
        return (q)super.h();
    }
    
    q<V> v() {
        return (q<V>)new c((v<Object, Object>)this);
    }
    
    ax<V> w() {
        return new ax<V>(this) {
            Iterator<? extends q<V>> a = c.b.g().a();
            Iterator<V> b = y.a();
            final v c;
            
            @Override
            public boolean hasNext() {
                return this.b.hasNext() || this.a.hasNext();
            }
            
            @Override
            public V next() {
                if (!this.b.hasNext()) {
                    this.b = ((q)this.a.next()).a();
                }
                return this.b.next();
            }
        };
    }
    
    public static class a<K, V>
    {
        Map<K, Collection<V>> a;
        @MonotonicNonNullDecl
        Comparator<? super K> b;
        @MonotonicNonNullDecl
        Comparator<? super V> c;
        
        public a() {
            this.a = aj.a();
        }
        
        public a<K, V> b(final K k, final Iterable<? extends V> iterable) {
            if (k == null) {
                final StringBuilder sb = new StringBuilder();
                sb.append("null key in entry: null=");
                sb.append(x.a(iterable));
                throw new NullPointerException(sb.toString());
            }
            final Collection collection = this.a.get(k);
            if (collection != null) {
                for (final V next : iterable) {
                    j.a(k, next);
                    collection.add(next);
                }
                return this;
            }
            final Iterator<? extends V> iterator2 = iterable.iterator();
            if (!iterator2.hasNext()) {
                return this;
            }
            final Collection<V> c = this.c();
            while (iterator2.hasNext()) {
                final V next2 = iterator2.next();
                j.a(k, next2);
                c.add(next2);
            }
            this.a.put(k, c);
            return this;
        }
        
        public a<K, V> b(final K k, final V... a) {
            return this.b(k, (Iterable<? extends V>)Arrays.asList(a));
        }
        
        public v<K, V> b() {
            final Set<Map.Entry<K, Collection<V>>> entrySet = this.a.entrySet();
            final Comparator<? super K> b = this.b;
            Iterable<Object> a = (Iterable<Object>)entrySet;
            if (b != null) {
                a = ai.a(b).c().a((Iterable<Object>)entrySet);
            }
            return (v<K, V>)t.a((Collection<? extends Map.Entry<?, ? extends Collection<?>>>)a, (Comparator<? super Object>)this.c);
        }
        
        Collection<V> c() {
            return new ArrayList<V>();
        }
    }
    
    private static class b<K, V> extends q<Map.Entry<K, V>>
    {
        @Weak
        final v<K, V> a;
        
        b(final v<K, V> a) {
            this.a = a;
        }
        
        @Override
        public ax<Map.Entry<K, V>> a() {
            return this.a.t();
        }
        
        @Override
        public boolean contains(final Object o) {
            if (o instanceof Map.Entry) {
                final Map.Entry entry = (Map.Entry)o;
                return this.a.b(entry.getKey(), entry.getValue());
            }
            return false;
        }
        
        @Override
        boolean f() {
            return this.a.g();
        }
        
        @Override
        public int size() {
            return this.a.d();
        }
    }
    
    private static final class c<K, V> extends q<V>
    {
        @Weak
        private final transient v<K, V> a;
        
        c(final v<K, V> a) {
            this.a = a;
        }
        
        @Override
        int a(final Object[] array, int a) {
            final ax<? extends q<V>> a2 = this.a.b.g().a();
            while (a2.hasNext()) {
                a = a2.next().a(array, a);
            }
            return a;
        }
        
        @Override
        public ax<V> a() {
            return this.a.w();
        }
        
        @Override
        public boolean contains(@NullableDecl final Object o) {
            return this.a.d(o);
        }
        
        @Override
        boolean f() {
            return true;
        }
        
        @Override
        public int size() {
            return this.a.d();
        }
    }
}

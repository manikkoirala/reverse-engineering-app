// 
// Decompiled by Procyon v0.6.0
// 

package com.applovin.exoplayer2.common.a;

import java.util.Arrays;
import java.lang.reflect.Array;
import java.util.Map;

final class aj
{
    static <K, V> Map<K, V> a() {
        return (Map<K, V>)l.a();
    }
    
    static <K, V> Map<K, V> a(final int n) {
        return (Map<K, V>)l.a(n);
    }
    
    static <T> T[] a(final T[] array, final int length) {
        return (T[])Array.newInstance(array.getClass().getComponentType(), length);
    }
    
    static <T> T[] a(final Object[] original, final int from, final int to, final T[] array) {
        return Arrays.copyOfRange(original, from, to, (Class<? extends T[]>)array.getClass());
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.applovin.exoplayer2.common.a;

import org.checkerframework.checker.nullness.compatqual.NullableDecl;
import com.applovin.exoplayer2.common.base.Preconditions;
import java.io.Serializable;

final class ao<T> extends ai<T> implements Serializable
{
    final ai<? super T> a;
    
    ao(final ai<? super T> ai) {
        this.a = Preconditions.checkNotNull(ai);
    }
    
    @Override
    public <S extends T> ai<S> a() {
        return (ai<S>)this.a;
    }
    
    @Override
    public int compare(final T t, final T t2) {
        return this.a.compare((Object)t2, (Object)t);
    }
    
    @Override
    public boolean equals(@NullableDecl final Object o) {
        return o == this || (o instanceof ao && this.a.equals(((ao)o).a));
    }
    
    @Override
    public int hashCode() {
        return -this.a.hashCode();
    }
    
    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append(this.a);
        sb.append(".reverse()");
        return sb.toString();
    }
}

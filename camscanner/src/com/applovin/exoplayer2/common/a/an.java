// 
// Decompiled by Procyon v0.6.0
// 

package com.applovin.exoplayer2.common.a;

import com.applovin.exoplayer2.common.base.Preconditions;
import java.io.Serializable;

final class an extends ai<Comparable> implements Serializable
{
    static final an a;
    
    static {
        a = new an();
    }
    
    private an() {
    }
    
    public int a(final Comparable comparable, final Comparable comparable2) {
        Preconditions.checkNotNull(comparable);
        if (comparable == comparable2) {
            return 0;
        }
        return comparable2.compareTo(comparable);
    }
    
    @Override
    public <S extends Comparable> ai<S> a() {
        return ai.b();
    }
    
    @Override
    public String toString() {
        return "Ordering.natural().reverse()";
    }
}

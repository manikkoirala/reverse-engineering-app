// 
// Decompiled by Procyon v0.6.0
// 

package com.applovin.exoplayer2.common.a;

import java.util.Arrays;
import com.applovin.exoplayer2.common.base.Preconditions;
import java.util.Iterator;
import org.checkerframework.checker.nullness.compatqual.NullableDecl;
import java.util.Collection;
import java.io.Serializable;
import java.util.AbstractCollection;

public abstract class q<E> extends AbstractCollection<E> implements Serializable
{
    private static final Object[] a;
    
    static {
        a = new Object[0];
    }
    
    q() {
    }
    
    int a(final Object[] array, int n) {
        final ax<E> a = this.a();
        while (a.hasNext()) {
            array[n] = a.next();
            ++n;
        }
        return n;
    }
    
    public abstract ax<E> a();
    
    @Deprecated
    @Override
    public final boolean add(final E e) {
        throw new UnsupportedOperationException();
    }
    
    @Deprecated
    @Override
    public final boolean addAll(final Collection<? extends E> collection) {
        throw new UnsupportedOperationException();
    }
    
    Object[] b() {
        return null;
    }
    
    int c() {
        throw new UnsupportedOperationException();
    }
    
    @Deprecated
    @Override
    public final void clear() {
        throw new UnsupportedOperationException();
    }
    
    @Override
    public abstract boolean contains(@NullableDecl final Object p0);
    
    int d() {
        throw new UnsupportedOperationException();
    }
    
    public s<E> e() {
        s<Object> s;
        if (this.isEmpty()) {
            s = com.applovin.exoplayer2.common.a.s.g();
        }
        else {
            s = com.applovin.exoplayer2.common.a.s.b(this.toArray());
        }
        return (s<E>)s;
    }
    
    abstract boolean f();
    
    @Deprecated
    @Override
    public final boolean remove(final Object o) {
        throw new UnsupportedOperationException();
    }
    
    @Deprecated
    @Override
    public final boolean removeAll(final Collection<?> collection) {
        throw new UnsupportedOperationException();
    }
    
    @Deprecated
    @Override
    public final boolean retainAll(final Collection<?> collection) {
        throw new UnsupportedOperationException();
    }
    
    @Override
    public final Object[] toArray() {
        return this.toArray(q.a);
    }
    
    @Override
    public final <T> T[] toArray(final T[] array) {
        Preconditions.checkNotNull(array);
        final int size = this.size();
        T[] a;
        if (array.length < size) {
            final Object[] b = this.b();
            if (b != null) {
                return aj.a(b, this.c(), this.d(), array);
            }
            a = ah.a(array, size);
        }
        else {
            a = array;
            if (array.length > size) {
                array[size] = null;
                a = array;
            }
        }
        this.a(a, 0);
        return a;
    }
    
    abstract static class a<E> extends b<E>
    {
        Object[] a;
        int b;
        boolean c;
        
        a(final int n) {
            j.a(n, "initialCapacity");
            this.a = new Object[n];
            this.b = 0;
        }
        
        private void a(final int n) {
            final Object[] a = this.a;
            if (a.length < n) {
                this.a = Arrays.copyOf(a, q.b.a(a.length, n));
                this.c = false;
            }
            else if (this.c) {
                this.a = a.clone();
                this.c = false;
            }
        }
        
        public a<E> a(final E e) {
            Preconditions.checkNotNull(e);
            this.a(this.b + 1);
            this.a[this.b++] = e;
            return this;
        }
    }
    
    public abstract static class b<E>
    {
        b() {
        }
        
        static int a(int n, int n2) {
            if (n2 >= 0) {
                if ((n = n + (n >> 1) + 1) < n2) {
                    n = Integer.highestOneBit(n2 - 1) << 1;
                }
                if ((n2 = n) < 0) {
                    n2 = Integer.MAX_VALUE;
                }
                return n2;
            }
            throw new AssertionError((Object)"cannot store more than MAX_VALUE elements");
        }
    }
}

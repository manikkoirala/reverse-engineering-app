// 
// Decompiled by Procyon v0.6.0
// 

package com.applovin.exoplayer2.common.a;

import java.util.ConcurrentModificationException;
import java.util.AbstractCollection;
import java.util.ListIterator;
import java.util.Comparator;
import java.util.SortedSet;
import java.util.NavigableSet;
import org.checkerframework.checker.nullness.compatqual.MonotonicNonNullDecl;
import java.util.SortedMap;
import java.util.NavigableMap;
import java.util.Set;
import java.util.RandomAccess;
import java.util.Collections;
import org.checkerframework.checker.nullness.compatqual.NullableDecl;
import java.util.List;
import java.util.Iterator;
import com.applovin.exoplayer2.common.base.Preconditions;
import java.util.Collection;
import java.util.Map;
import java.io.Serializable;

abstract class d<K, V> extends com.applovin.exoplayer2.common.a.f<K, V> implements Serializable
{
    private transient Map<K, Collection<V>> a;
    private transient int b;
    
    protected d(final Map<K, Collection<V>> a) {
        Preconditions.checkArgument(a.isEmpty());
        this.a = a;
    }
    
    static /* synthetic */ int a(final d d, int b) {
        b += d.b;
        return d.b = b;
    }
    
    static /* synthetic */ int b(final d d, int b) {
        b = d.b - b;
        return d.b = b;
    }
    
    private static <E> Iterator<E> c(final Collection<E> collection) {
        Iterator iterator;
        if (collection instanceof List) {
            iterator = ((List)collection).listIterator();
        }
        else {
            iterator = collection.iterator();
        }
        return iterator;
    }
    
    private void e(final Object o) {
        final Collection<V> collection = ab.c(this.a, o);
        if (collection != null) {
            final int size = collection.size();
            collection.clear();
            this.b -= size;
        }
    }
    
    Collection<V> a(@NullableDecl final K k, final Collection<V> collection) {
        return new i(k, collection, null);
    }
    
     <E> Collection<E> a(final Collection<E> c) {
        return Collections.unmodifiableCollection((Collection<? extends E>)c);
    }
    
    final List<V> a(@NullableDecl final K k, final List<V> list, @NullableDecl final i i) {
        List<V> list2;
        if (list instanceof RandomAccess) {
            list2 = new f(k, list, i);
        }
        else {
            list2 = new j(k, list, i);
        }
        return list2;
    }
    
    @Override
    public boolean a(@NullableDecl final K k, @NullableDecl final V v) {
        final Collection collection = this.a.get(k);
        if (collection == null) {
            final Collection<V> c = this.c(k);
            if (c.add(v)) {
                ++this.b;
                this.a.put(k, c);
                return true;
            }
            throw new AssertionError((Object)"New Collection violated the Collection spec");
        }
        else {
            if (collection.add(v)) {
                ++this.b;
                return true;
            }
            return false;
        }
    }
    
    @Override
    public Collection<V> b(@NullableDecl final K k) {
        Collection<V> c;
        if ((c = this.a.get(k)) == null) {
            c = this.c(k);
        }
        return this.a(k, c);
    }
    
    abstract Collection<V> c();
    
    Collection<V> c(@NullableDecl final K k) {
        return this.c();
    }
    
    @Override
    public int d() {
        return this.b;
    }
    
    @Override
    public void e() {
        final Iterator<Collection<V>> iterator = this.a.values().iterator();
        while (iterator.hasNext()) {
            iterator.next().clear();
        }
        this.a.clear();
        this.b = 0;
    }
    
    @Override
    Set<K> f() {
        return (Set<K>)new c(this.a);
    }
    
    final Set<K> g() {
        final Map<K, Collection<V>> a = this.a;
        if (a instanceof NavigableMap) {
            return (Set<K>)new e((NavigableMap)this.a);
        }
        if (a instanceof SortedMap) {
            return (Set<K>)new h((SortedMap)this.a);
        }
        return (Set<K>)new c(this.a);
    }
    
    @Override
    public Collection<V> h() {
        return super.h();
    }
    
    @Override
    Collection<V> i() {
        return (Collection<V>)new com.applovin.exoplayer2.common.a.f.c();
    }
    
    @Override
    Iterator<V> j() {
        return new b<V>(this) {
            final d a;
            
            @Override
            V a(final K k, final V v) {
                return v;
            }
        };
    }
    
    @Override
    public Collection<Map.Entry<K, V>> k() {
        return super.k();
    }
    
    @Override
    Collection<Map.Entry<K, V>> l() {
        if (this instanceof ap) {
            return (Collection<Map.Entry<K, V>>)new com.applovin.exoplayer2.common.a.f.b();
        }
        return (Collection<Map.Entry<K, V>>)new com.applovin.exoplayer2.common.a.f.a();
    }
    
    @Override
    Iterator<Map.Entry<K, V>> m() {
        return new b<Map.Entry<K, V>>(this) {
            final d a;
            
            Map.Entry<K, V> b(final K k, final V v) {
                return ab.a(k, v);
            }
        };
    }
    
    @Override
    Map<K, Collection<V>> n() {
        return new a(this.a);
    }
    
    final Map<K, Collection<V>> o() {
        final Map<K, Collection<V>> a = this.a;
        if (a instanceof NavigableMap) {
            return new d((NavigableMap)this.a);
        }
        if (a instanceof SortedMap) {
            return new g((SortedMap)this.a);
        }
        return new a(this.a);
    }
    
    private class a extends ab.e<K, Collection<V>>
    {
        final transient Map<K, Collection<V>> a;
        final d b;
        
        a(final d b, final Map<K, Collection<V>> a) {
            this.b = b;
            this.a = a;
        }
        
        public Collection<V> a(final Object o) {
            final Collection<V> collection = ab.a((Map<?, Collection<V>>)this.a, o);
            if (collection == null) {
                return null;
            }
            return this.b.a(o, collection);
        }
        
        Entry<K, Collection<V>> a(final Entry<K, Collection<V>> entry) {
            final K key = entry.getKey();
            return ab.a(key, this.b.a(key, entry.getValue()));
        }
        
        protected Set<Entry<K, Collection<V>>> a() {
            return (Set<Entry<K, Collection<V>>>)new a();
        }
        
        public Collection<V> b(final Object o) {
            final Collection collection = this.a.remove(o);
            if (collection == null) {
                return null;
            }
            final Collection<V> c = this.b.c();
            c.addAll(collection);
            d.b(this.b, collection.size());
            collection.clear();
            return c;
        }
        
        @Override
        public void clear() {
            if (this.a == this.b.a) {
                this.b.e();
            }
            else {
                y.d(new b());
            }
        }
        
        @Override
        public boolean containsKey(final Object o) {
            return ab.b(this.a, o);
        }
        
        @Override
        public boolean equals(@NullableDecl final Object o) {
            return this == o || this.a.equals(o);
        }
        
        @Override
        public int hashCode() {
            return this.a.hashCode();
        }
        
        @Override
        public Set<K> keySet() {
            return this.b.p();
        }
        
        @Override
        public int size() {
            return this.a.size();
        }
        
        @Override
        public String toString() {
            return this.a.toString();
        }
        
        class a extends ab.b<K, Collection<V>>
        {
            final com.applovin.exoplayer2.common.a.d.a a;
            
            a(final com.applovin.exoplayer2.common.a.d.a a) {
                this.a = a;
            }
            
            @Override
            Map<K, Collection<V>> a() {
                return this.a;
            }
            
            @Override
            public boolean contains(final Object o) {
                return k.a(this.a.a.entrySet(), o);
            }
            
            @Override
            public Iterator<Entry<K, Collection<V>>> iterator() {
                return this.a.new b();
            }
            
            @Override
            public boolean remove(final Object o) {
                if (!this.contains(o)) {
                    return false;
                }
                this.a.b.e(((Entry)o).getKey());
                return true;
            }
        }
        
        class b implements Iterator<Entry<K, Collection<V>>>
        {
            final Iterator<Entry<K, Collection<V>>> a;
            @NullableDecl
            Collection<V> b;
            final a c;
            
            b(final a c) {
                this.c = c;
                this.a = c.a.entrySet().iterator();
            }
            
            public Entry<K, Collection<V>> a() {
                final Entry entry = this.a.next();
                this.b = entry.getValue();
                return this.c.a(entry);
            }
            
            @Override
            public boolean hasNext() {
                return this.a.hasNext();
            }
            
            @Override
            public void remove() {
                com.applovin.exoplayer2.common.a.j.a(this.b != null);
                this.a.remove();
                d.b(this.c.b, this.b.size());
                this.b.clear();
                this.b = null;
            }
        }
    }
    
    private abstract class b<T> implements Iterator<T>
    {
        final Iterator<Map.Entry<K, Collection<V>>> b;
        @NullableDecl
        K c;
        @MonotonicNonNullDecl
        Collection<V> d;
        Iterator<V> e;
        final d f;
        
        b(final d f) {
            this.f = f;
            this.b = f.a.entrySet().iterator();
            this.c = null;
            this.d = null;
            this.e = y.c();
        }
        
        abstract T a(final K p0, final V p1);
        
        @Override
        public boolean hasNext() {
            return this.b.hasNext() || this.e.hasNext();
        }
        
        @Override
        public T next() {
            if (!this.e.hasNext()) {
                final Map.Entry entry = this.b.next();
                this.c = entry.getKey();
                final Collection d = (Collection)entry.getValue();
                this.d = d;
                this.e = d.iterator();
            }
            return this.a(this.c, this.e.next());
        }
        
        @Override
        public void remove() {
            this.e.remove();
            if (this.d.isEmpty()) {
                this.b.remove();
            }
            this.f.b--;
        }
    }
    
    private class c extends ab.c<K, Collection<V>>
    {
        final d a;
        
        c(final d a, final Map<K, Collection<V>> map) {
            this.a = a;
            super(map);
        }
        
        @Override
        public void clear() {
            y.d(this.iterator());
        }
        
        @Override
        public boolean containsAll(final Collection<?> collection) {
            return ((ab.c<K, Collection<V>>)this).c().keySet().containsAll(collection);
        }
        
        @Override
        public boolean equals(@NullableDecl final Object o) {
            return this == o || ((ab.c<K, Collection<V>>)this).c().keySet().equals(o);
        }
        
        @Override
        public int hashCode() {
            return ((ab.c<K, Collection<V>>)this).c().keySet().hashCode();
        }
        
        @Override
        public Iterator<K> iterator() {
            return new Iterator<K>(this, ((ab.c<K, Collection<V>>)this).c().entrySet().iterator()) {
                @NullableDecl
                Map.Entry<K, Collection<V>> a;
                final Iterator b;
                final c c;
                
                @Override
                public boolean hasNext() {
                    return this.b.hasNext();
                }
                
                @Override
                public K next() {
                    final Map.Entry<K, Collection<V>> a = this.b.next();
                    this.a = a;
                    return a.getKey();
                }
                
                @Override
                public void remove() {
                    com.applovin.exoplayer2.common.a.j.a(this.a != null);
                    final Collection collection = this.a.getValue();
                    this.b.remove();
                    d.b(this.c.a, collection.size());
                    collection.clear();
                    this.a = null;
                }
            };
        }
        
        @Override
        public boolean remove(final Object o) {
            final Collection collection = ((ab.c<K, Collection>)this).c().remove(o);
            boolean b = false;
            int size;
            if (collection != null) {
                size = collection.size();
                collection.clear();
                d.b(this.a, size);
            }
            else {
                size = 0;
            }
            if (size > 0) {
                b = true;
            }
            return b;
        }
    }
    
    class d extends g implements NavigableMap<K, Collection<V>>
    {
        final com.applovin.exoplayer2.common.a.d c;
        
        d(final com.applovin.exoplayer2.common.a.d c, final NavigableMap<K, Collection<V>> navigableMap) {
            super(navigableMap);
        }
        
        Entry<K, Collection<V>> a(final Iterator<Entry<K, Collection<V>>> iterator) {
            if (!iterator.hasNext()) {
                return null;
            }
            final Entry entry = (Entry)iterator.next();
            final Collection<V> c = this.c.c();
            c.addAll((Collection<? extends V>)entry.getValue());
            iterator.remove();
            return ab.a(entry.getKey(), this.c.a(c));
        }
        
        public NavigableMap<K, Collection<V>> a(final K k, final K i) {
            return this.subMap(k, true, i, false);
        }
        
        NavigableMap<K, Collection<V>> b() {
            return (NavigableMap)super.g();
        }
        
        public NavigableMap<K, Collection<V>> c(final K k) {
            return this.headMap(k, false);
        }
        
        public NavigableSet<K> c() {
            return (NavigableSet)super.f();
        }
        
        @Override
        public Entry<K, Collection<V>> ceilingEntry(final K k) {
            final Map.Entry<K, Collection<V>> ceilingEntry = this.b().ceilingEntry(k);
            Entry<K, Collection<V>> a;
            if (ceilingEntry == null) {
                a = null;
            }
            else {
                a = ((com.applovin.exoplayer2.common.a.d.a)this).a(ceilingEntry);
            }
            return a;
        }
        
        @Override
        public K ceilingKey(final K k) {
            return this.b().ceilingKey(k);
        }
        
        public NavigableMap<K, Collection<V>> d(final K k) {
            return this.tailMap(k, true);
        }
        
        NavigableSet<K> d() {
            return new com.applovin.exoplayer2.common.a.d.e(this.b());
        }
        
        @Override
        public NavigableSet<K> descendingKeySet() {
            return this.descendingMap().navigableKeySet();
        }
        
        @Override
        public NavigableMap<K, Collection<V>> descendingMap() {
            return new d(this.b().descendingMap());
        }
        
        @Override
        public Entry<K, Collection<V>> firstEntry() {
            final Map.Entry<K, Collection<V>> firstEntry = this.b().firstEntry();
            Entry<K, Collection<V>> a;
            if (firstEntry == null) {
                a = null;
            }
            else {
                a = ((com.applovin.exoplayer2.common.a.d.a)this).a(firstEntry);
            }
            return a;
        }
        
        @Override
        public Entry<K, Collection<V>> floorEntry(final K k) {
            final Map.Entry<K, Collection<V>> floorEntry = this.b().floorEntry(k);
            Entry<K, Collection<V>> a;
            if (floorEntry == null) {
                a = null;
            }
            else {
                a = ((com.applovin.exoplayer2.common.a.d.a)this).a(floorEntry);
            }
            return a;
        }
        
        @Override
        public K floorKey(final K k) {
            return this.b().floorKey(k);
        }
        
        @Override
        public NavigableMap<K, Collection<V>> headMap(final K k, final boolean b) {
            return new d(this.b().headMap(k, b));
        }
        
        @Override
        public Entry<K, Collection<V>> higherEntry(final K k) {
            final Map.Entry<K, Collection<V>> higherEntry = this.b().higherEntry(k);
            Entry<K, Collection<V>> a;
            if (higherEntry == null) {
                a = null;
            }
            else {
                a = ((com.applovin.exoplayer2.common.a.d.a)this).a(higherEntry);
            }
            return a;
        }
        
        @Override
        public K higherKey(final K k) {
            return this.b().higherKey(k);
        }
        
        @Override
        public Entry<K, Collection<V>> lastEntry() {
            final Map.Entry<K, Collection<V>> lastEntry = this.b().lastEntry();
            Entry<K, Collection<V>> a;
            if (lastEntry == null) {
                a = null;
            }
            else {
                a = ((com.applovin.exoplayer2.common.a.d.a)this).a(lastEntry);
            }
            return a;
        }
        
        @Override
        public Entry<K, Collection<V>> lowerEntry(final K k) {
            final Map.Entry<K, Collection<V>> lowerEntry = this.b().lowerEntry(k);
            Entry<K, Collection<V>> a;
            if (lowerEntry == null) {
                a = null;
            }
            else {
                a = ((com.applovin.exoplayer2.common.a.d.a)this).a(lowerEntry);
            }
            return a;
        }
        
        @Override
        public K lowerKey(final K k) {
            return this.b().lowerKey(k);
        }
        
        @Override
        public NavigableSet<K> navigableKeySet() {
            return this.c();
        }
        
        @Override
        public Entry<K, Collection<V>> pollFirstEntry() {
            return this.a(((ab.e<K, Collection<V>>)this).entrySet().iterator());
        }
        
        @Override
        public Entry<K, Collection<V>> pollLastEntry() {
            return (Entry<K, Collection<V>>)this.a((Iterator<Entry<Object, Collection<V>>>)this.descendingMap().entrySet().iterator());
        }
        
        @Override
        public NavigableMap<K, Collection<V>> subMap(final K k, final boolean b, final K i, final boolean b2) {
            return new d(this.b().subMap(k, b, i, b2));
        }
        
        @Override
        public NavigableMap<K, Collection<V>> tailMap(final K k, final boolean b) {
            return new d(this.b().tailMap(k, b));
        }
    }
    
    private class g extends a implements SortedMap<K, Collection<V>>
    {
        @MonotonicNonNullDecl
        SortedSet<K> d;
        final d e;
        
        g(final d e, final SortedMap<K, Collection<V>> sortedMap) {
            super(sortedMap);
        }
        
        @Override
        public Comparator<? super K> comparator() {
            return this.g().comparator();
        }
        
        SortedSet<K> e() {
            return new h(this.g());
        }
        
        public SortedSet<K> f() {
            SortedSet<K> d;
            if ((d = this.d) == null) {
                d = this.e();
                this.d = d;
            }
            return d;
        }
        
        @Override
        public K firstKey() {
            return this.g().firstKey();
        }
        
        SortedMap<K, Collection<V>> g() {
            return (SortedMap)super.a;
        }
        
        @Override
        public SortedMap<K, Collection<V>> headMap(final K k) {
            return new g(this.g().headMap(k));
        }
        
        @Override
        public K lastKey() {
            return this.g().lastKey();
        }
        
        @Override
        public SortedMap<K, Collection<V>> subMap(final K k, final K i) {
            return new g(this.g().subMap(k, i));
        }
        
        @Override
        public SortedMap<K, Collection<V>> tailMap(final K k) {
            return new g(this.g().tailMap(k));
        }
    }
    
    class e extends h implements NavigableSet<K>
    {
        final d b;
        
        e(final d b, final NavigableMap<K, Collection<V>> navigableMap) {
            super(navigableMap);
        }
        
        NavigableMap<K, Collection<V>> a() {
            return (NavigableMap)super.b();
        }
        
        public NavigableSet<K> a(final K k) {
            return this.headSet(k, false);
        }
        
        public NavigableSet<K> a(final K k, final K i) {
            return this.subSet(k, true, i, false);
        }
        
        public NavigableSet<K> b(final K k) {
            return this.tailSet(k, true);
        }
        
        @Override
        public K ceiling(final K k) {
            return this.a().ceilingKey(k);
        }
        
        @Override
        public Iterator<K> descendingIterator() {
            return this.descendingSet().iterator();
        }
        
        @Override
        public NavigableSet<K> descendingSet() {
            return new e(this.a().descendingMap());
        }
        
        @Override
        public K floor(final K k) {
            return this.a().floorKey(k);
        }
        
        @Override
        public NavigableSet<K> headSet(final K k, final boolean b) {
            return new e(this.a().headMap(k, b));
        }
        
        @Override
        public K higher(final K k) {
            return this.a().higherKey(k);
        }
        
        @Override
        public K lower(final K k) {
            return this.a().lowerKey(k);
        }
        
        @Override
        public K pollFirst() {
            return y.c(((c)this).iterator());
        }
        
        @Override
        public K pollLast() {
            return y.c(this.descendingIterator());
        }
        
        @Override
        public NavigableSet<K> subSet(final K k, final boolean b, final K i, final boolean b2) {
            return new e(this.a().subMap(k, b, i, b2));
        }
        
        @Override
        public NavigableSet<K> tailSet(final K k, final boolean b) {
            return new e(this.a().tailMap(k, b));
        }
    }
    
    private class h extends c implements SortedSet<K>
    {
        final d c;
        
        h(final d c, final SortedMap<K, Collection<V>> sortedMap) {
            super(sortedMap);
        }
        
        SortedMap<K, Collection<V>> b() {
            return (SortedMap)super.c();
        }
        
        @Override
        public Comparator<? super K> comparator() {
            return this.b().comparator();
        }
        
        @Override
        public K first() {
            return this.b().firstKey();
        }
        
        @Override
        public SortedSet<K> headSet(final K k) {
            return new h(this.b().headMap(k));
        }
        
        @Override
        public K last() {
            return this.b().lastKey();
        }
        
        @Override
        public SortedSet<K> subSet(final K k, final K i) {
            return new h(this.b().subMap(k, i));
        }
        
        @Override
        public SortedSet<K> tailSet(final K k) {
            return new h(this.b().tailMap(k));
        }
    }
    
    private class f extends j implements RandomAccess
    {
        final d a;
        
        f(@NullableDecl final d a, final K k, @NullableDecl final List<V> list, final i i) {
            super(k, list, i);
        }
    }
    
    class j extends i implements List<V>
    {
        final d g;
        
        j(@NullableDecl final d g, final K k, @NullableDecl final List<V> list, final i i) {
            super(k, list, i);
        }
        
        @Override
        public void add(final int n, final V v) {
            ((i)this).a();
            final boolean empty = ((i)this).e().isEmpty();
            this.g().add(n, v);
            this.g.b++;
            if (empty) {
                ((i)this).d();
            }
        }
        
        @Override
        public boolean addAll(int size, final Collection<? extends V> collection) {
            if (collection.isEmpty()) {
                return false;
            }
            final int size2 = ((i)this).size();
            final boolean addAll = this.g().addAll(size, collection);
            if (addAll) {
                size = ((i)this).e().size();
                d.a(this.g, size - size2);
                if (size2 == 0) {
                    ((i)this).d();
                }
            }
            return addAll;
        }
        
        List<V> g() {
            return (List)((i)this).e();
        }
        
        @Override
        public V get(final int n) {
            ((i)this).a();
            return this.g().get(n);
        }
        
        @Override
        public int indexOf(final Object o) {
            ((i)this).a();
            return this.g().indexOf(o);
        }
        
        @Override
        public int lastIndexOf(final Object o) {
            ((i)this).a();
            return this.g().lastIndexOf(o);
        }
        
        @Override
        public ListIterator<V> listIterator() {
            ((i)this).a();
            return new a();
        }
        
        @Override
        public ListIterator<V> listIterator(final int n) {
            ((i)this).a();
            return new a(n);
        }
        
        @Override
        public V remove(final int n) {
            ((i)this).a();
            final V remove = this.g().remove(n);
            this.g.b--;
            ((i)this).b();
            return remove;
        }
        
        @Override
        public V set(final int n, final V v) {
            ((i)this).a();
            return this.g().set(n, v);
        }
        
        @Override
        public List<V> subList(final int n, final int n2) {
            ((i)this).a();
            final d g = this.g;
            final K c = ((i)this).c();
            final List<V> subList = this.g().subList(n, n2);
            AbstractCollection<V> f;
            if (((i)this).f() == null) {
                f = this;
            }
            else {
                f = ((i)this).f();
            }
            return g.a(c, subList, (i)f);
        }
        
        private class a extends i.a implements ListIterator<V>
        {
            final j d;
            
            a(final j d) {
                (i)(this.d = d).super();
            }
            
            public a(final j d, final int n) {
                super(d.g().listIterator(n));
            }
            
            private ListIterator<V> c() {
                return (ListIterator)((i.a)this).b();
            }
            
            @Override
            public void add(final V v) {
                final boolean empty = this.d.isEmpty();
                this.c().add(v);
                this.d.g.b++;
                if (empty) {
                    ((i)this.d).d();
                }
            }
            
            @Override
            public boolean hasPrevious() {
                return this.c().hasPrevious();
            }
            
            @Override
            public int nextIndex() {
                return this.c().nextIndex();
            }
            
            @Override
            public V previous() {
                return this.c().previous();
            }
            
            @Override
            public int previousIndex() {
                return this.c().previousIndex();
            }
            
            @Override
            public void set(final V v) {
                this.c().set(v);
            }
        }
    }
    
    class i extends AbstractCollection<V>
    {
        @NullableDecl
        final K b;
        Collection<V> c;
        @NullableDecl
        final i d;
        @NullableDecl
        final Collection<V> e;
        final d f;
        
        i(@NullableDecl final d f, final K b, @NullableDecl final Collection<V> c, final i d) {
            this.f = f;
            this.b = b;
            this.c = c;
            this.d = d;
            Collection<V> e;
            if (d == null) {
                e = null;
            }
            else {
                e = d.e();
            }
            this.e = e;
        }
        
        void a() {
            final i d = this.d;
            if (d != null) {
                d.a();
                if (this.d.e() != this.e) {
                    throw new ConcurrentModificationException();
                }
            }
            else if (this.c.isEmpty()) {
                final Collection c = this.f.a.get(this.b);
                if (c != null) {
                    this.c = c;
                }
            }
        }
        
        @Override
        public boolean add(final V v) {
            this.a();
            final boolean empty = this.c.isEmpty();
            final boolean add = this.c.add(v);
            if (add) {
                this.f.b++;
                if (empty) {
                    this.d();
                }
            }
            return add;
        }
        
        @Override
        public boolean addAll(final Collection<? extends V> collection) {
            if (collection.isEmpty()) {
                return false;
            }
            final int size = this.size();
            final boolean addAll = this.c.addAll(collection);
            if (addAll) {
                com.applovin.exoplayer2.common.a.d.a(this.f, this.c.size() - size);
                if (size == 0) {
                    this.d();
                }
            }
            return addAll;
        }
        
        void b() {
            final i d = this.d;
            if (d != null) {
                d.b();
            }
            else if (this.c.isEmpty()) {
                this.f.a.remove(this.b);
            }
        }
        
        K c() {
            return this.b;
        }
        
        @Override
        public void clear() {
            final int size = this.size();
            if (size == 0) {
                return;
            }
            this.c.clear();
            com.applovin.exoplayer2.common.a.d.b(this.f, size);
            this.b();
        }
        
        @Override
        public boolean contains(final Object o) {
            this.a();
            return this.c.contains(o);
        }
        
        @Override
        public boolean containsAll(final Collection<?> collection) {
            this.a();
            return this.c.containsAll(collection);
        }
        
        void d() {
            final i d = this.d;
            if (d != null) {
                d.d();
            }
            else {
                this.f.a.put(this.b, this.c);
            }
        }
        
        Collection<V> e() {
            return this.c;
        }
        
        @Override
        public boolean equals(@NullableDecl final Object o) {
            if (o == this) {
                return true;
            }
            this.a();
            return this.c.equals(o);
        }
        
        i f() {
            return this.d;
        }
        
        @Override
        public int hashCode() {
            this.a();
            return this.c.hashCode();
        }
        
        @Override
        public Iterator<V> iterator() {
            this.a();
            return new a();
        }
        
        @Override
        public boolean remove(final Object o) {
            this.a();
            final boolean remove = this.c.remove(o);
            if (remove) {
                this.f.b--;
                this.b();
            }
            return remove;
        }
        
        @Override
        public boolean removeAll(final Collection<?> collection) {
            if (collection.isEmpty()) {
                return false;
            }
            final int size = this.size();
            final boolean removeAll = this.c.removeAll(collection);
            if (removeAll) {
                com.applovin.exoplayer2.common.a.d.a(this.f, this.c.size() - size);
                this.b();
            }
            return removeAll;
        }
        
        @Override
        public boolean retainAll(final Collection<?> collection) {
            Preconditions.checkNotNull(collection);
            final int size = this.size();
            final boolean retainAll = this.c.retainAll(collection);
            if (retainAll) {
                com.applovin.exoplayer2.common.a.d.a(this.f, this.c.size() - size);
                this.b();
            }
            return retainAll;
        }
        
        @Override
        public int size() {
            this.a();
            return this.c.size();
        }
        
        @Override
        public String toString() {
            this.a();
            return this.c.toString();
        }
        
        class a implements Iterator<V>
        {
            final Iterator<V> a;
            final Collection<V> b;
            final i c;
            
            a(final i c) {
                this.c = c;
                final Collection<V> c2 = c.c;
                this.b = c2;
                this.a = (Iterator<V>)c((Collection<Object>)c2);
            }
            
            a(final i c, final Iterator<V> a) {
                this.c = c;
                this.b = c.c;
                this.a = a;
            }
            
            void a() {
                this.c.a();
                if (this.c.c == this.b) {
                    return;
                }
                throw new ConcurrentModificationException();
            }
            
            Iterator<V> b() {
                this.a();
                return this.a;
            }
            
            @Override
            public boolean hasNext() {
                this.a();
                return this.a.hasNext();
            }
            
            @Override
            public V next() {
                this.a();
                return this.a.next();
            }
            
            @Override
            public void remove() {
                this.a.remove();
                this.c.f.b--;
                this.c.b();
            }
        }
    }
}

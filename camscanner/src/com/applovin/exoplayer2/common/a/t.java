// 
// Decompiled by Procyon v0.6.0
// 

package com.applovin.exoplayer2.common.a;

import java.util.List;
import java.util.Iterator;
import org.checkerframework.checker.nullness.compatqual.NullableDecl;
import java.util.Comparator;
import java.util.Map;
import java.util.Collection;

public class t<K, V> extends v<K, V> implements z<K, V>
{
    t(final u<K, s<V>> u, final int n) {
        super((u<K, ? extends q<Object>>)u, n);
    }
    
    public static <K, V> t<K, V> a() {
        return (t<K, V>)o.a;
    }
    
    static <K, V> t<K, V> a(final Collection<? extends Map.Entry<? extends K, ? extends Collection<? extends V>>> collection, @NullableDecl final Comparator<? super V> comparator) {
        if (collection.isEmpty()) {
            return a();
        }
        final u.a<K, s<Object>> a = new u.a<K, s<Object>>(collection.size());
        final Iterator iterator = collection.iterator();
        int n = 0;
        while (iterator.hasNext()) {
            final Map.Entry<Object, V> entry = (Map.Entry<Object, V>)iterator.next();
            final Object key = entry.getKey();
            final Collection collection2 = (Collection)entry.getValue();
            s<Object> s;
            if (comparator == null) {
                s = com.applovin.exoplayer2.common.a.s.a((Collection<?>)collection2);
            }
            else {
                s = com.applovin.exoplayer2.common.a.s.a((Comparator<? super Object>)comparator, (Iterable<?>)collection2);
            }
            if (!s.isEmpty()) {
                a.a((K)key, s);
                n += s.size();
            }
        }
        return new t<K, V>((u<Object, s<Object>>)a.a(), n);
    }
    
    public static <K, V> a<K, V> c() {
        return new a<K, V>();
    }
    
    public s<V> c(@NullableDecl final K k) {
        s<Object> g;
        if ((g = (s)super.b.get(k)) == null) {
            g = s.g();
        }
        return (s<V>)g;
    }
    
    public static final class a<K, V> extends v.a<K, V>
    {
        public a<K, V> a(final K k, final Iterable<? extends V> iterable) {
            super.b(k, iterable);
            return this;
        }
        
        public a<K, V> a(final K k, final V... array) {
            super.b(k, array);
            return this;
        }
        
        public t<K, V> a() {
            return (t)super.b();
        }
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.applovin.exoplayer2.common.a;

import com.applovin.exoplayer2.common.base.Objects;
import com.applovin.exoplayer2.common.base.Preconditions;
import java.util.Collection;
import java.util.Iterator;
import java.util.NoSuchElementException;
import org.checkerframework.checker.nullness.compatqual.NullableDecl;

public final class y
{
    static <T> ax<T> a() {
        return (ax<T>)b();
    }
    
    public static <T> ax<T> a(@NullableDecl final T t) {
        return new ax<T>(t) {
            boolean a;
            final Object b;
            
            @Override
            public boolean hasNext() {
                return this.a ^ true;
            }
            
            @Override
            public T next() {
                if (!this.a) {
                    this.a = true;
                    return (T)this.b;
                }
                throw new NoSuchElementException();
            }
        };
    }
    
    @NullableDecl
    public static <T> T a(final Iterator<? extends T> iterator, @NullableDecl T next) {
        if (iterator.hasNext()) {
            next = iterator.next();
        }
        return next;
    }
    
    public static String a(final Iterator<?> iterator) {
        final StringBuilder sb = new StringBuilder();
        sb.append('[');
        int n = 1;
        while (iterator.hasNext()) {
            if (n == 0) {
                sb.append(", ");
            }
            sb.append(iterator.next());
            n = 0;
        }
        sb.append(']');
        return sb.toString();
    }
    
    public static <T> boolean a(final Collection<T> collection, final Iterator<? extends T> iterator) {
        Preconditions.checkNotNull(collection);
        Preconditions.checkNotNull(iterator);
        boolean b = false;
        while (iterator.hasNext()) {
            b |= collection.add(iterator.next());
        }
        return b;
    }
    
    public static boolean a(final Iterator<?> iterator, final Collection<?> collection) {
        Preconditions.checkNotNull(collection);
        boolean b = false;
        while (iterator.hasNext()) {
            if (collection.contains(iterator.next())) {
                iterator.remove();
                b = true;
            }
        }
        return b;
    }
    
    public static boolean a(final Iterator<?> iterator, final Iterator<?> iterator2) {
        while (iterator.hasNext()) {
            if (!iterator2.hasNext()) {
                return false;
            }
            if (!Objects.equal(iterator.next(), iterator2.next())) {
                return false;
            }
        }
        return iterator2.hasNext() ^ true;
    }
    
    static <T> ay<T> b() {
        return (ay<T>)a.a;
    }
    
    public static <T> T b(final Iterator<T> iterator) {
        T next;
        do {
            next = iterator.next();
        } while (iterator.hasNext());
        return next;
    }
    
    @NullableDecl
    static <T> T c(final Iterator<T> iterator) {
        if (iterator.hasNext()) {
            final T next = iterator.next();
            iterator.remove();
            return next;
        }
        return null;
    }
    
    static <T> Iterator<T> c() {
        return (Iterator<T>)b.a;
    }
    
    static void d(final Iterator<?> iterator) {
        Preconditions.checkNotNull(iterator);
        while (iterator.hasNext()) {
            iterator.next();
            iterator.remove();
        }
    }
    
    private static final class a<T> extends com.applovin.exoplayer2.common.a.a<T>
    {
        static final ay<Object> a;
        private final T[] b;
        private final int c;
        
        static {
            a = new a<Object>(new Object[0], 0, 0, 0);
        }
        
        a(final T[] b, final int c, final int n, final int n2) {
            super(n, n2);
            this.b = b;
            this.c = c;
        }
        
        @Override
        protected T a(final int n) {
            return this.b[this.c + n];
        }
    }
    
    private enum b implements Iterator<Object>
    {
        a;
        
        private static final b[] b;
        
        @Override
        public boolean hasNext() {
            return false;
        }
        
        @Override
        public Object next() {
            throw new NoSuchElementException();
        }
        
        @Override
        public void remove() {
            j.a(false);
        }
    }
}

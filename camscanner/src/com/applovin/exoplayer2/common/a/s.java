// 
// Decompiled by Procyon v0.6.0
// 

package com.applovin.exoplayer2.common.a;

import java.util.ListIterator;
import java.util.Iterator;
import java.util.Arrays;
import com.applovin.exoplayer2.common.base.Preconditions;
import java.util.Comparator;
import java.util.Collection;
import java.util.RandomAccess;
import java.util.List;

public abstract class s<E> extends q<E> implements List<E>, RandomAccess
{
    private static final ay<Object> a;
    
    static {
        a = new b<Object>(ak.a, 0);
    }
    
    s() {
    }
    
    public static <E> s<E> a(final E e) {
        return c(e);
    }
    
    public static <E> s<E> a(final E e, final E e2) {
        return c(e, e2);
    }
    
    public static <E> s<E> a(final E e, final E e2, final E e3, final E e4, final E e5) {
        return c(e, e2, e3, e4, e5);
    }
    
    public static <E> s<E> a(final E e, final E e2, final E e3, final E e4, final E e5, final E e6) {
        return c(e, e2, e3, e4, e5, e6);
    }
    
    public static <E> s<E> a(final Collection<? extends E> collection) {
        if (collection instanceof q) {
            s<Object> s2;
            final s s = s2 = ((q)collection).e();
            if (s.f()) {
                s2 = b(s.toArray());
            }
            return (s<E>)s2;
        }
        return c(collection.toArray());
    }
    
    public static <E> s<E> a(final Comparator<? super E> c, final Iterable<? extends E> iterable) {
        Preconditions.checkNotNull(c);
        final Object[] b = x.b(iterable);
        ah.a(b);
        Arrays.sort(b, (Comparator<? super Object>)c);
        return (s<E>)b(b);
    }
    
    public static <E> s<E> a(final E[] array) {
        s<Object> s;
        if (array.length == 0) {
            s = g();
        }
        else {
            s = c((Object[])array.clone());
        }
        return (s<E>)s;
    }
    
    static <E> s<E> b(final Object[] array) {
        return b(array, array.length);
    }
    
    static <E> s<E> b(final Object[] array, final int n) {
        if (n == 0) {
            return g();
        }
        return new ak<E>(array, n);
    }
    
    private static <E> s<E> c(final Object... array) {
        return (s<E>)b(ah.a(array));
    }
    
    public static <E> s<E> g() {
        return (s<E>)ak.a;
    }
    
    public static <E> a<E> i() {
        return new a<E>();
    }
    
    @Override
    int a(final Object[] array, final int n) {
        final int size = this.size();
        for (int i = 0; i < size; ++i) {
            array[n + i] = this.get(i);
        }
        return n + size;
    }
    
    @Override
    public ax<E> a() {
        return this.h();
    }
    
    public ay<E> a(final int n) {
        Preconditions.checkPositionIndex(n, this.size());
        if (this.isEmpty()) {
            return (ay<E>)s.a;
        }
        return new b<E>(this, n);
    }
    
    public s<E> a(final int n, final int n2) {
        Preconditions.checkPositionIndexes(n, n2, this.size());
        final int n3 = n2 - n;
        if (n3 == this.size()) {
            return this;
        }
        if (n3 == 0) {
            return g();
        }
        return this.b(n, n2);
    }
    
    @Deprecated
    @Override
    public final void add(final int n, final E e) {
        throw new UnsupportedOperationException();
    }
    
    @Deprecated
    @Override
    public final boolean addAll(final int n, final Collection<? extends E> collection) {
        throw new UnsupportedOperationException();
    }
    
    s<E> b(final int n, final int n2) {
        return new c(n, n2 - n);
    }
    
    @Override
    public boolean contains(final Object o) {
        return this.indexOf(o) >= 0;
    }
    
    @Override
    public final s<E> e() {
        return this;
    }
    
    @Override
    public boolean equals(final Object o) {
        return aa.a(this, o);
    }
    
    public ay<E> h() {
        return this.a(0);
    }
    
    @Override
    public int hashCode() {
        final int size = this.size();
        int n = 1;
        for (int i = 0; i < size; ++i) {
            n = ~(~(n * 31 + this.get(i).hashCode()));
        }
        return n;
    }
    
    @Override
    public int indexOf(final Object o) {
        int b;
        if (o == null) {
            b = -1;
        }
        else {
            b = aa.b(this, o);
        }
        return b;
    }
    
    @Override
    public int lastIndexOf(final Object o) {
        int c;
        if (o == null) {
            c = -1;
        }
        else {
            c = aa.c(this, o);
        }
        return c;
    }
    
    @Deprecated
    @Override
    public final E remove(final int n) {
        throw new UnsupportedOperationException();
    }
    
    @Deprecated
    @Override
    public final E set(final int n, final E e) {
        throw new UnsupportedOperationException();
    }
    
    public static final class a<E> extends q.a<E>
    {
        public a() {
            this(4);
        }
        
        a(final int n) {
            super(n);
        }
        
        public s<E> a() {
            super.c = true;
            return s.b(super.a, super.b);
        }
        
        public a<E> b(final E e) {
            super.a(e);
            return this;
        }
    }
    
    static class b<E> extends a<E>
    {
        private final s<E> a;
        
        b(final s<E> a, final int n) {
            super(a.size(), n);
            this.a = a;
        }
        
        @Override
        protected E a(final int n) {
            return this.a.get(n);
        }
    }
    
    class c extends s<E>
    {
        final transient int a;
        final transient int b;
        final s c;
        
        c(final s c, final int a, final int b) {
            this.c = c;
            this.a = a;
            this.b = b;
        }
        
        @Override
        public s<E> a(final int n, final int n2) {
            Preconditions.checkPositionIndexes(n, n2, this.b);
            final s c = this.c;
            final int a = this.a;
            return c.a(n + a, n2 + a);
        }
        
        @Override
        Object[] b() {
            return this.c.b();
        }
        
        @Override
        int c() {
            return this.c.c() + this.a;
        }
        
        @Override
        int d() {
            return this.c.c() + this.a + this.b;
        }
        
        @Override
        boolean f() {
            return true;
        }
        
        @Override
        public E get(final int n) {
            Preconditions.checkElementIndex(n, this.b);
            return this.c.get(n + this.a);
        }
        
        @Override
        public int size() {
            return this.b;
        }
    }
}

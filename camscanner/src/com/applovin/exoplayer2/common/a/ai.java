// 
// Decompiled by Procyon v0.6.0
// 

package com.applovin.exoplayer2.common.a;

import org.checkerframework.checker.nullness.compatqual.NullableDecl;
import java.util.Map;
import com.applovin.exoplayer2.common.base.Function;
import java.util.Comparator;

public abstract class ai<T> implements Comparator<T>
{
    protected ai() {
    }
    
    public static <T> ai<T> a(final Comparator<T> comparator) {
        ai ai;
        if (comparator instanceof ai) {
            ai = (ai)comparator;
        }
        else {
            ai = new m(comparator);
        }
        return ai;
    }
    
    public static <C extends Comparable> ai<C> b() {
        return (ai<C>)ag.a;
    }
    
    public <S extends T> ai<S> a() {
        return new ao<S>(this);
    }
    
    public <F> ai<F> a(final Function<F, ? extends T> function) {
        return (ai<F>)new i((Function<Object, ?>)function, (ai<Object>)this);
    }
    
    public <E extends T> s<E> a(final Iterable<E> iterable) {
        return s.a((Comparator<? super E>)this, (Iterable<? extends E>)iterable);
    }
    
     <T2 extends T> ai<Map.Entry<T2, ?>> c() {
        return this.a((Function<Map.Entry<T2, ?>, ? extends T>)ab.a());
    }
    
    @Override
    public abstract int compare(@NullableDecl final T p0, @NullableDecl final T p1);
}

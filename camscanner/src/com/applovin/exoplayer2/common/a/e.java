// 
// Decompiled by Procyon v0.6.0
// 

package com.applovin.exoplayer2.common.a;

import com.applovin.exoplayer2.common.base.Objects;
import org.checkerframework.checker.nullness.compatqual.NullableDecl;
import java.util.Map;

abstract class e<K, V> implements Entry<K, V>
{
    @Override
    public boolean equals(@NullableDecl final Object o) {
        final boolean b = o instanceof Entry;
        boolean b3;
        final boolean b2 = b3 = false;
        if (b) {
            final Entry entry = (Entry)o;
            b3 = b2;
            if (Objects.equal(this.getKey(), entry.getKey())) {
                b3 = b2;
                if (Objects.equal(this.getValue(), entry.getValue())) {
                    b3 = true;
                }
            }
        }
        return b3;
    }
    
    @Override
    public abstract K getKey();
    
    @Override
    public abstract V getValue();
    
    @Override
    public int hashCode() {
        final Object key = this.getKey();
        final Object value = this.getValue();
        int hashCode = 0;
        int hashCode2;
        if (key == null) {
            hashCode2 = 0;
        }
        else {
            hashCode2 = key.hashCode();
        }
        if (value != null) {
            hashCode = value.hashCode();
        }
        return hashCode2 ^ hashCode;
    }
    
    @Override
    public V setValue(final V v) {
        throw new UnsupportedOperationException();
    }
    
    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append(this.getKey());
        sb.append("=");
        sb.append(this.getValue());
        return sb.toString();
    }
}

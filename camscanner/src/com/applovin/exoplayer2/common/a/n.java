// 
// Decompiled by Procyon v0.6.0
// 

package com.applovin.exoplayer2.common.a;

import com.applovin.exoplayer2.common.b.a;
import java.util.Comparator;
import org.checkerframework.checker.nullness.compatqual.NullableDecl;
import com.applovin.exoplayer2.common.b.d;
import com.applovin.exoplayer2.common.b.c;

public abstract class n
{
    private static final n a;
    private static final n b;
    private static final n c;
    
    static {
        a = new n() {
            n a(final int n) {
                n n2;
                if (n < 0) {
                    n2 = n.b;
                }
                else if (n > 0) {
                    n2 = n.c;
                }
                else {
                    n2 = n.a;
                }
                return n2;
            }
            
            @Override
            public n a(final int n, final int n2) {
                return this.a(com.applovin.exoplayer2.common.b.c.a(n, n2));
            }
            
            @Override
            public n a(final long n, final long n2) {
                return this.a(d.a(n, n2));
            }
            
            @Override
            public <T> n a(@NullableDecl final T t, @NullableDecl final T t2, final Comparator<T> comparator) {
                return this.a(comparator.compare(t, t2));
            }
            
            @Override
            public n a(final boolean b, final boolean b2) {
                return this.a(com.applovin.exoplayer2.common.b.a.a(b2, b));
            }
            
            @Override
            public int b() {
                return 0;
            }
            
            @Override
            public n b(final boolean b, final boolean b2) {
                return this.a(com.applovin.exoplayer2.common.b.a.a(b, b2));
            }
        };
        b = new a(-1);
        c = new a(1);
    }
    
    private n() {
    }
    
    public static n a() {
        return n.a;
    }
    
    public abstract n a(final int p0, final int p1);
    
    public abstract n a(final long p0, final long p1);
    
    public abstract <T> n a(@NullableDecl final T p0, @NullableDecl final T p1, final Comparator<T> p2);
    
    public abstract n a(final boolean p0, final boolean p1);
    
    public abstract int b();
    
    public abstract n b(final boolean p0, final boolean p1);
    
    private static final class a extends n
    {
        final int a;
        
        a(final int a) {
            super(null);
            this.a = a;
        }
        
        @Override
        public n a(final int n, final int n2) {
            return this;
        }
        
        @Override
        public n a(final long n, final long n2) {
            return this;
        }
        
        @Override
        public <T> n a(@NullableDecl final T t, @NullableDecl final T t2, @NullableDecl final Comparator<T> comparator) {
            return this;
        }
        
        @Override
        public n a(final boolean b, final boolean b2) {
            return this;
        }
        
        @Override
        public int b() {
            return this.a;
        }
        
        @Override
        public n b(final boolean b, final boolean b2) {
            return this;
        }
    }
}

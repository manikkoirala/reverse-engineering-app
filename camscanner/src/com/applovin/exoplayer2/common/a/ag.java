// 
// Decompiled by Procyon v0.6.0
// 

package com.applovin.exoplayer2.common.a;

import com.applovin.exoplayer2.common.base.Preconditions;
import java.io.Serializable;

final class ag extends ai<Comparable> implements Serializable
{
    static final ag a;
    
    static {
        a = new ag();
    }
    
    private ag() {
    }
    
    public int a(final Comparable comparable, final Comparable comparable2) {
        Preconditions.checkNotNull(comparable);
        Preconditions.checkNotNull(comparable2);
        return comparable.compareTo(comparable2);
    }
    
    @Override
    public <S extends Comparable> ai<S> a() {
        return (ai<S>)an.a;
    }
    
    @Override
    public String toString() {
        return "Ordering.natural()";
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.applovin.exoplayer2.common.a;

import java.util.NoSuchElementException;
import com.applovin.exoplayer2.common.base.Preconditions;

abstract class a<E> extends ay<E>
{
    private final int a;
    private int b;
    
    protected a(final int a, final int b) {
        Preconditions.checkPositionIndex(b, a);
        this.a = a;
        this.b = b;
    }
    
    protected abstract E a(final int p0);
    
    @Override
    public final boolean hasNext() {
        return this.b < this.a;
    }
    
    @Override
    public final boolean hasPrevious() {
        return this.b > 0;
    }
    
    @Override
    public final E next() {
        if (this.hasNext()) {
            return this.a(this.b++);
        }
        throw new NoSuchElementException();
    }
    
    @Override
    public final int nextIndex() {
        return this.b;
    }
    
    @Override
    public final E previous() {
        if (this.hasPrevious()) {
            final int b = this.b - 1;
            this.b = b;
            return this.a(b);
        }
        throw new NoSuchElementException();
    }
    
    @Override
    public final int previousIndex() {
        return this.b - 1;
    }
}

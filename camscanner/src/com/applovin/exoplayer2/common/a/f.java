// 
// Decompiled by Procyon v0.6.0
// 

package com.applovin.exoplayer2.common.a;

import java.util.AbstractCollection;
import java.util.Iterator;
import org.checkerframework.checker.nullness.compatqual.NullableDecl;
import java.util.Set;
import org.checkerframework.checker.nullness.compatqual.MonotonicNonNullDecl;
import java.util.Map;
import java.util.Collection;

abstract class f<K, V> implements ac<K, V>
{
    @MonotonicNonNullDecl
    private transient Collection<Map.Entry<K, V>> a;
    @MonotonicNonNullDecl
    private transient Set<K> b;
    @MonotonicNonNullDecl
    private transient Collection<V> c;
    @MonotonicNonNullDecl
    private transient Map<K, Collection<V>> d;
    
    @Override
    public boolean a(@NullableDecl final K k, @NullableDecl final V v) {
        return this.b(k).add(v);
    }
    
    @Override
    public Map<K, Collection<V>> b() {
        Map<K, Collection<V>> d;
        if ((d = this.d) == null) {
            d = this.n();
            this.d = d;
        }
        return d;
    }
    
    @Override
    public boolean b(@NullableDecl final Object o, @NullableDecl final Object o2) {
        final Collection collection = this.b().get(o);
        return collection != null && collection.contains(o2);
    }
    
    @Override
    public boolean c(@NullableDecl final Object o, @NullableDecl final Object o2) {
        final Collection collection = this.b().get(o);
        return collection != null && collection.remove(o2);
    }
    
    public boolean d(@NullableDecl final Object o) {
        final Iterator<Collection<V>> iterator = this.b().values().iterator();
        while (iterator.hasNext()) {
            if (iterator.next().contains(o)) {
                return true;
            }
        }
        return false;
    }
    
    @Override
    public boolean equals(@NullableDecl final Object o) {
        return ae.a(this, o);
    }
    
    abstract Set<K> f();
    
    @Override
    public Collection<V> h() {
        Collection<V> c;
        if ((c = this.c) == null) {
            c = this.i();
            this.c = c;
        }
        return c;
    }
    
    @Override
    public int hashCode() {
        return this.b().hashCode();
    }
    
    abstract Collection<V> i();
    
    Iterator<V> j() {
        return ab.b(this.k().iterator());
    }
    
    public Collection<Map.Entry<K, V>> k() {
        Collection<Map.Entry<K, V>> a;
        if ((a = this.a) == null) {
            a = this.l();
            this.a = a;
        }
        return a;
    }
    
    abstract Collection<Map.Entry<K, V>> l();
    
    abstract Iterator<Map.Entry<K, V>> m();
    
    abstract Map<K, Collection<V>> n();
    
    public Set<K> p() {
        Set<K> b;
        if ((b = this.b) == null) {
            b = this.f();
            this.b = b;
        }
        return b;
    }
    
    @Override
    public String toString() {
        return this.b().toString();
    }
    
    class a extends ae.b<K, V>
    {
        final f a;
        
        a(final f a) {
            this.a = a;
        }
        
        @Override
        ac<K, V> a() {
            return this.a;
        }
        
        @Override
        public Iterator<Map.Entry<K, V>> iterator() {
            return this.a.m();
        }
    }
    
    class b extends a implements Set<Map.Entry<K, V>>
    {
        final f b;
        
        b(final f b) {
            this.b = b.super();
        }
        
        @Override
        public boolean equals(@NullableDecl final Object o) {
            return aq.a(this, o);
        }
        
        @Override
        public int hashCode() {
            return aq.a(this);
        }
    }
    
    class c extends AbstractCollection<V>
    {
        final f a;
        
        c(final f a) {
            this.a = a;
        }
        
        @Override
        public void clear() {
            this.a.e();
        }
        
        @Override
        public boolean contains(@NullableDecl final Object o) {
            return this.a.d(o);
        }
        
        @Override
        public Iterator<V> iterator() {
            return this.a.j();
        }
        
        @Override
        public int size() {
            return this.a.d();
        }
    }
}

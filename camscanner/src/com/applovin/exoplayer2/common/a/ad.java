// 
// Decompiled by Procyon v0.6.0
// 

package com.applovin.exoplayer2.common.a;

import java.util.ArrayList;
import java.io.Serializable;
import java.util.List;
import com.applovin.exoplayer2.common.base.Supplier;
import java.util.TreeMap;
import java.util.Collection;
import java.util.Map;
import com.applovin.exoplayer2.common.base.Preconditions;
import java.util.Comparator;

public abstract class ad<K0, V0>
{
    private ad() {
    }
    
    public static c<Comparable> a() {
        return (c<Comparable>)a(ai.b());
    }
    
    public static <K0> c<K0> a(final Comparator<K0> comparator) {
        Preconditions.checkNotNull(comparator);
        return (c<K0>)new c<K0>(comparator) {
            final Comparator a;
            
            @Override
             <K extends K0, V> Map<K, Collection<V>> a() {
                return new TreeMap<K, Collection<V>>(this.a);
            }
        };
    }
    
    private static final class a<V> implements Supplier<List<V>>, Serializable
    {
        private final int a;
        
        a(final int n) {
            this.a = j.a(n, "expectedValuesPerKey");
        }
        
        public List<V> a() {
            return new ArrayList<V>(this.a);
        }
    }
    
    public abstract static class b<K0, V0> extends ad<K0, V0>
    {
        b() {
            super(null);
        }
        
        public abstract <K extends K0, V extends V0> z<K, V> b();
    }
    
    public abstract static class c<K0>
    {
        c() {
        }
        
        public b<K0, Object> a(final int n) {
            j.a(n, "expectedValuesPerKey");
            return (b<K0, Object>)new b<K0, Object>(this, n) {
                final int a;
                final c b;
                
                @Override
                public <K extends K0, V> z<K, V> b() {
                    return ae.a(this.b.a(), (Supplier<? extends List<V>>)new a(this.a));
                }
            };
        }
        
        abstract <K extends K0, V> Map<K, Collection<V>> a();
        
        public b<K0, Object> b() {
            return this.a(2);
        }
    }
}

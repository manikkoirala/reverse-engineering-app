// 
// Decompiled by Procyon v0.6.0
// 

package com.applovin.exoplayer2.common.a;

import java.util.Iterator;
import org.checkerframework.checker.nullness.compatqual.NullableDecl;

final class am<E> extends w<E>
{
    static final am<Object> a;
    final transient Object[] b;
    final transient Object[] c;
    private final transient int d;
    private final transient int e;
    private final transient int f;
    
    static {
        a = new am<Object>(new Object[0], 0, null, 0, 0);
    }
    
    am(final Object[] b, final int e, final Object[] c, final int d, final int f) {
        this.b = b;
        this.c = c;
        this.d = d;
        this.e = e;
        this.f = f;
    }
    
    @Override
    int a(final Object[] array, final int n) {
        System.arraycopy(this.b, 0, array, n, this.f);
        return n + this.f;
    }
    
    @Override
    public ax<E> a() {
        return this.e().a();
    }
    
    @Override
    Object[] b() {
        return this.b;
    }
    
    @Override
    int c() {
        return 0;
    }
    
    @Override
    public boolean contains(@NullableDecl final Object obj) {
        final Object[] c = this.c;
        if (obj == null || c == null) {
            return false;
        }
        int a = p.a(obj);
        while (true) {
            a &= this.d;
            final Object o = c[a];
            if (o == null) {
                return false;
            }
            if (o.equals(obj)) {
                return true;
            }
            ++a;
        }
    }
    
    @Override
    int d() {
        return this.f;
    }
    
    @Override
    boolean f() {
        return false;
    }
    
    @Override
    boolean h() {
        return true;
    }
    
    @Override
    public int hashCode() {
        return this.e;
    }
    
    @Override
    s<E> i() {
        return s.b(this.b, this.f);
    }
    
    @Override
    public int size() {
        return this.f;
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.applovin.exoplayer2.common.a;

import java.util.NoSuchElementException;
import com.applovin.exoplayer2.common.base.Preconditions;
import org.checkerframework.checker.nullness.compatqual.NullableDecl;

public abstract class b<T> extends ax<T>
{
    private a a;
    @NullableDecl
    private T b;
    
    protected b() {
        this.a = com.applovin.exoplayer2.common.a.b.a.b;
    }
    
    private boolean c() {
        this.a = com.applovin.exoplayer2.common.a.b.a.d;
        this.b = this.a();
        if (this.a != com.applovin.exoplayer2.common.a.b.a.c) {
            this.a = com.applovin.exoplayer2.common.a.b.a.a;
            return true;
        }
        return false;
    }
    
    protected abstract T a();
    
    protected final T b() {
        this.a = com.applovin.exoplayer2.common.a.b.a.c;
        return null;
    }
    
    @Override
    public final boolean hasNext() {
        Preconditions.checkState(this.a != com.applovin.exoplayer2.common.a.b.a.d);
        final int n = b$1.a[this.a.ordinal()];
        return n != 1 && (n == 2 || this.c());
    }
    
    @Override
    public final T next() {
        if (this.hasNext()) {
            this.a = com.applovin.exoplayer2.common.a.b.a.b;
            final T b = this.b;
            this.b = null;
            return b;
        }
        throw new NoSuchElementException();
    }
    
    private enum a
    {
        a, 
        b, 
        c, 
        d;
        
        private static final a[] e;
    }
}

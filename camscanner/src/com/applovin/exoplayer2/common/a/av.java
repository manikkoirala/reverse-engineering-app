// 
// Decompiled by Procyon v0.6.0
// 

package com.applovin.exoplayer2.common.a;

import java.util.SortedSet;
import java.util.Collection;
import java.util.Set;
import org.checkerframework.checker.nullness.compatqual.NullableDecl;

public interface av<K, V> extends ap<K, V>
{
    SortedSet<V> c(@NullableDecl final K p0);
}

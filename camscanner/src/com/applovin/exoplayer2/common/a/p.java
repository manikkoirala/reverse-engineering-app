// 
// Decompiled by Procyon v0.6.0
// 

package com.applovin.exoplayer2.common.a;

import org.checkerframework.checker.nullness.compatqual.NullableDecl;

final class p
{
    static int a(final int n) {
        return (int)(Integer.rotateLeft((int)(n * -862048943L), 15) * 461845907L);
    }
    
    static int a(int max, final double n) {
        max = Math.max(max, 2);
        final int highestOneBit = Integer.highestOneBit(max);
        if (max > (int)(n * highestOneBit)) {
            max = highestOneBit << 1;
            if (max <= 0) {
                max = 1073741824;
            }
            return max;
        }
        return highestOneBit;
    }
    
    static int a(@NullableDecl final Object o) {
        int hashCode;
        if (o == null) {
            hashCode = 0;
        }
        else {
            hashCode = o.hashCode();
        }
        return a(hashCode);
    }
}

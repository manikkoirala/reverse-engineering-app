// 
// Decompiled by Procyon v0.6.0
// 

package com.applovin.exoplayer2.common.a;

import java.util.Iterator;
import com.applovin.exoplayer2.common.base.Preconditions;

final class ar<E> extends w<E>
{
    final transient E a;
    private transient int b;
    
    ar(final E e) {
        this.a = Preconditions.checkNotNull(e);
    }
    
    ar(final E a, final int b) {
        this.a = a;
        this.b = b;
    }
    
    @Override
    int a(final Object[] array, final int n) {
        array[n] = this.a;
        return n + 1;
    }
    
    @Override
    public ax<E> a() {
        return y.a(this.a);
    }
    
    @Override
    public boolean contains(final Object obj) {
        return this.a.equals(obj);
    }
    
    @Override
    boolean f() {
        return false;
    }
    
    @Override
    boolean h() {
        return this.b != 0;
    }
    
    @Override
    public final int hashCode() {
        int b;
        if ((b = this.b) == 0) {
            b = this.a.hashCode();
            this.b = b;
        }
        return b;
    }
    
    @Override
    s<E> i() {
        return s.a(this.a);
    }
    
    @Override
    public int size() {
        return 1;
    }
    
    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append('[');
        sb.append(this.a.toString());
        sb.append(']');
        return sb.toString();
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.applovin.exoplayer2.common.a;

import org.checkerframework.checker.nullness.compatqual.NullableDecl;
import java.util.Collections;
import java.util.List;
import java.util.Collection;
import java.util.Map;

abstract class c<K, V> extends d<K, V> implements z<K, V>
{
    protected c(final Map<K, Collection<V>> map) {
        super(map);
    }
    
    @Override
    Collection<V> a(final K k, final Collection<V> collection) {
        return this.a(k, (List<V>)collection, null);
    }
    
    @Override
     <E> Collection<E> a(final Collection<E> collection) {
        return (Collection<E>)Collections.unmodifiableList((List<?>)collection);
    }
    
    abstract List<V> a();
    
    @Override
    public List<V> a(@NullableDecl final K k) {
        return (List)super.b(k);
    }
    
    @Override
    public boolean a(@NullableDecl final K k, @NullableDecl final V v) {
        return super.a(k, v);
    }
    
    @Override
    public Map<K, Collection<V>> b() {
        return super.b();
    }
    
    @Override
    public boolean equals(@NullableDecl final Object o) {
        return super.equals(o);
    }
}

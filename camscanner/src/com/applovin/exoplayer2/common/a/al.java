// 
// Decompiled by Procyon v0.6.0
// 

package com.applovin.exoplayer2.common.a;

import java.util.Iterator;
import java.util.AbstractMap;
import java.util.Map;
import java.util.Arrays;
import org.checkerframework.checker.nullness.compatqual.NullableDecl;
import com.applovin.exoplayer2.common.base.Preconditions;

final class al<K, V> extends u<K, V>
{
    static final u<Object, Object> b;
    final transient Object[] c;
    private final transient int[] d;
    private final transient int e;
    
    static {
        b = new al<Object, Object>(null, new Object[0], 0);
    }
    
    private al(final int[] d, final Object[] c, final int e) {
        this.d = d;
        this.c = c;
        this.e = e;
    }
    
    static <K, V> al<K, V> a(final int n, final Object[] array) {
        if (n == 0) {
            return (al)al.b;
        }
        if (n == 1) {
            j.a(array[0], array[1]);
            return new al<K, V>(null, array, 1);
        }
        Preconditions.checkPositionIndex(n, array.length >> 1);
        return new al<K, V>(a(array, n, w.a(n), 0), array, n);
    }
    
    static Object a(@NullableDecl final int[] array, @NullableDecl final Object[] array2, int a, int length, @NullableDecl final Object o) {
        final Object o2 = null;
        if (o == null) {
            return null;
        }
        if (a == 1) {
            Object o3 = o2;
            if (array2[length].equals(o)) {
                o3 = array2[length ^ 0x1];
            }
            return o3;
        }
        if (array == null) {
            return null;
        }
        length = array.length;
        a = p.a(o.hashCode());
        while (true) {
            a &= length - 1;
            final int n = array[a];
            if (n == -1) {
                return null;
            }
            if (array2[n].equals(o)) {
                return array2[n ^ 0x1];
            }
            ++a;
        }
    }
    
    static int[] a(final Object[] array, final int n, final int n2, final int n3) {
        if (n == 1) {
            j.a(array[n3], array[n3 ^ 0x1]);
            return null;
        }
        final int[] a = new int[n2];
        Arrays.fill(a, -1);
        for (int i = 0; i < n; ++i) {
            final int n4 = i * 2;
            final int n5 = n4 + n3;
            final Object o = array[n5];
            final Object obj = array[n4 + (n3 ^ 0x1)];
            j.a(o, obj);
            int a2 = p.a(o.hashCode());
            while (true) {
                a2 &= n2 - 1;
                final int n6 = a[a2];
                if (n6 == -1) {
                    a[a2] = n5;
                    break;
                }
                if (array[n6].equals(o)) {
                    final StringBuilder sb = new StringBuilder();
                    sb.append("Multiple entries with same key: ");
                    sb.append(o);
                    sb.append("=");
                    sb.append(obj);
                    sb.append(" and ");
                    sb.append(array[n6]);
                    sb.append("=");
                    sb.append(array[n6 ^ 0x1]);
                    throw new IllegalArgumentException(sb.toString());
                }
                ++a2;
            }
        }
        return a;
    }
    
    @Override
    w<Entry<K, V>> d() {
        return (w<Entry<K, V>>)new a((u<Object, Object>)this, this.c, 0, this.e);
    }
    
    @Override
    w<K> f() {
        return new b<K>(this, (s<K>)new c(this.c, 0, this.e));
    }
    
    @NullableDecl
    @Override
    public V get(@NullableDecl final Object o) {
        return (V)a(this.d, this.c, this.e, 0, o);
    }
    
    @Override
    q<V> h() {
        return (q<V>)new c(this.c, 1, this.e);
    }
    
    @Override
    boolean i() {
        return false;
    }
    
    @Override
    public int size() {
        return this.e;
    }
    
    static class a<K, V> extends w<Entry<K, V>>
    {
        private final transient u<K, V> a;
        private final transient Object[] b;
        private final transient int c;
        private final transient int d;
        
        a(final u<K, V> a, final Object[] b, final int c, final int d) {
            this.a = a;
            this.b = b;
            this.c = c;
            this.d = d;
        }
        
        @Override
        int a(final Object[] array, final int n) {
            return this.e().a(array, n);
        }
        
        @Override
        public ax<Entry<K, V>> a() {
            return this.e().a();
        }
        
        @Override
        public boolean contains(Object key) {
            final boolean b = key instanceof Entry;
            boolean b3;
            final boolean b2 = b3 = false;
            if (b) {
                final Entry entry = (Entry)key;
                key = entry.getKey();
                final Object value = entry.getValue();
                b3 = b2;
                if (value != null) {
                    b3 = b2;
                    if (value.equals(this.a.get(key))) {
                        b3 = true;
                    }
                }
            }
            return b3;
        }
        
        @Override
        boolean f() {
            return true;
        }
        
        @Override
        s<Entry<K, V>> i() {
            return new s<Entry<K, V>>(this) {
                final al.a a;
                
                public Entry<K, V> b(int n) {
                    Preconditions.checkElementIndex(n, this.a.d);
                    final Object[] b = this.a.b;
                    n *= 2;
                    return (Entry<K, V>)new AbstractMap.SimpleImmutableEntry(b[this.a.c + n], this.a.b[n + (this.a.c ^ 0x1)]);
                }
                
                public boolean f() {
                    return true;
                }
                
                @Override
                public int size() {
                    return this.a.d;
                }
            };
        }
        
        @Override
        public int size() {
            return this.d;
        }
    }
    
    static final class b<K> extends w<K>
    {
        private final transient u<K, ?> a;
        private final transient s<K> b;
        
        b(final u<K, ?> a, final s<K> b) {
            this.a = a;
            this.b = b;
        }
        
        @Override
        int a(final Object[] array, final int n) {
            return this.e().a(array, n);
        }
        
        @Override
        public ax<K> a() {
            return this.e().a();
        }
        
        @Override
        public boolean contains(@NullableDecl final Object o) {
            return this.a.get(o) != null;
        }
        
        @Override
        public s<K> e() {
            return this.b;
        }
        
        @Override
        boolean f() {
            return true;
        }
        
        @Override
        public int size() {
            return this.a.size();
        }
    }
    
    static final class c extends s<Object>
    {
        private final transient Object[] a;
        private final transient int b;
        private final transient int c;
        
        c(final Object[] a, final int b, final int c) {
            this.a = a;
            this.b = b;
            this.c = c;
        }
        
        @Override
        boolean f() {
            return true;
        }
        
        @Override
        public Object get(final int n) {
            Preconditions.checkElementIndex(n, this.c);
            return this.a[n * 2 + this.b];
        }
        
        @Override
        public int size() {
            return this.c;
        }
    }
}

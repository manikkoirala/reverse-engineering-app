// 
// Decompiled by Procyon v0.6.0
// 

package com.applovin.exoplayer2.common.a;

import org.checkerframework.checker.nullness.compatqual.MonotonicNonNullDecl;
import java.util.AbstractMap;
import java.util.AbstractCollection;
import com.google.j2objc.annotations.Weak;
import java.util.HashSet;
import java.util.Set;
import java.util.Collection;
import com.applovin.exoplayer2.common.base.Objects;
import java.util.IdentityHashMap;
import java.util.Iterator;
import com.applovin.exoplayer2.common.base.Preconditions;
import org.checkerframework.checker.nullness.compatqual.NullableDecl;
import java.util.Map;
import com.applovin.exoplayer2.common.base.Function;

public final class ab
{
    static int a(final int n) {
        if (n < 3) {
            j.a(n, "expectedSize");
            return n + 1;
        }
        if (n < 1073741824) {
            return (int)(n / 0.75f + 1.0f);
        }
        return Integer.MAX_VALUE;
    }
    
    static <K> Function<Map.Entry<K, ?>, K> a() {
        return (Function<Map.Entry<K, ?>, K>)a.a;
    }
    
    static <V> V a(final Map<?, V> map, @NullableDecl final Object o) {
        Preconditions.checkNotNull(map);
        try {
            return map.get(o);
        }
        catch (final ClassCastException | NullPointerException ex) {
            return null;
        }
    }
    
    static String a(final Map<?, ?> map) {
        final StringBuilder a = k.a(map.size());
        a.append('{');
        final Iterator<Map.Entry<Object, V>> iterator = map.entrySet().iterator();
        int n = 1;
        while (iterator.hasNext()) {
            final Map.Entry<Object, V> entry = iterator.next();
            if (n == 0) {
                a.append(", ");
            }
            a.append(entry.getKey());
            a.append('=');
            a.append(entry.getValue());
            n = 0;
        }
        a.append('}');
        return a.toString();
    }
    
    static <K, V> Iterator<K> a(final Iterator<Map.Entry<K, V>> iterator) {
        return new aw<Map.Entry<K, V>, K>(iterator) {
            @Override
            K a(final Map.Entry<K, V> entry) {
                return entry.getKey();
            }
        };
    }
    
    public static <K, V> Map.Entry<K, V> a(@NullableDecl final K k, @NullableDecl final V v) {
        return new r<K, V>(k, v);
    }
    
    static <V> Function<Map.Entry<?, V>, V> b() {
        return (Function<Map.Entry<?, V>, V>)a.b;
    }
    
    static <K, V> Iterator<V> b(final Iterator<Map.Entry<K, V>> iterator) {
        return new aw<Map.Entry<K, V>, V>(iterator) {
            @Override
            V a(final Map.Entry<K, V> entry) {
                return entry.getValue();
            }
        };
    }
    
    static boolean b(final Map<?, ?> map, final Object o) {
        Preconditions.checkNotNull(map);
        try {
            return map.containsKey(o);
        }
        catch (final ClassCastException | NullPointerException ex) {
            return false;
        }
    }
    
    static <V> V c(final Map<?, V> map, final Object o) {
        Preconditions.checkNotNull(map);
        try {
            return map.remove(o);
        }
        catch (final ClassCastException | NullPointerException ex) {
            return null;
        }
    }
    
    public static <K, V> IdentityHashMap<K, V> c() {
        return new IdentityHashMap<K, V>();
    }
    
    static boolean d(final Map<?, ?> map, final Object o) {
        return map == o || (o instanceof Map && map.entrySet().equals(((Map)o).entrySet()));
    }
    
    private enum a implements Function<Map.Entry<?, ?>, Object>
    {
        a {
            @NullableDecl
            public Object a(final Map.Entry<?, ?> entry) {
                return entry.getKey();
            }
        }, 
        b {
            @NullableDecl
            public Object a(final Map.Entry<?, ?> entry) {
                return entry.getValue();
            }
        };
        
        private static final a[] c;
    }
    
    abstract static class b<K, V> extends aq.a<Map.Entry<K, V>>
    {
        abstract Map<K, V> a();
        
        @Override
        public void clear() {
            this.a().clear();
        }
        
        @Override
        public boolean contains(Object a) {
            final boolean b = a instanceof Map.Entry;
            boolean b3;
            final boolean b2 = b3 = false;
            if (b) {
                final Map.Entry entry = (Map.Entry)a;
                final Object key = entry.getKey();
                a = ab.a(this.a(), key);
                b3 = b2;
                if (Objects.equal(a, entry.getValue())) {
                    if (a == null) {
                        b3 = b2;
                        if (!this.a().containsKey(key)) {
                            return b3;
                        }
                    }
                    b3 = true;
                }
            }
            return b3;
        }
        
        @Override
        public boolean isEmpty() {
            return this.a().isEmpty();
        }
        
        @Override
        public boolean remove(final Object o) {
            return this.contains(o) && this.a().keySet().remove(((Map.Entry)o).getKey());
        }
        
        @Override
        public boolean removeAll(final Collection<?> collection) {
            try {
                return super.removeAll(Preconditions.checkNotNull(collection));
            }
            catch (final UnsupportedOperationException ex) {
                return aq.a(this, collection.iterator());
            }
        }
        
        @Override
        public boolean retainAll(final Collection<?> collection) {
            try {
                return super.retainAll(Preconditions.checkNotNull(collection));
            }
            catch (final UnsupportedOperationException ex) {
                final HashSet<Object> a = aq.a(collection.size());
                for (final Object next : collection) {
                    if (this.contains(next)) {
                        a.add(((Map.Entry<Object, V>)next).getKey());
                    }
                }
                return this.a().keySet().retainAll(a);
            }
        }
        
        @Override
        public int size() {
            return this.a().size();
        }
    }
    
    static class c<K, V> extends aq.a<K>
    {
        @Weak
        final Map<K, V> d;
        
        c(final Map<K, V> map) {
            this.d = Preconditions.checkNotNull(map);
        }
        
        Map<K, V> c() {
            return this.d;
        }
        
        @Override
        public void clear() {
            this.c().clear();
        }
        
        @Override
        public boolean contains(final Object o) {
            return this.c().containsKey(o);
        }
        
        @Override
        public boolean isEmpty() {
            return this.c().isEmpty();
        }
        
        @Override
        public Iterator<K> iterator() {
            return ab.a(this.c().entrySet().iterator());
        }
        
        @Override
        public boolean remove(final Object o) {
            if (this.contains(o)) {
                this.c().remove(o);
                return true;
            }
            return false;
        }
        
        @Override
        public int size() {
            return this.c().size();
        }
    }
    
    static class d<K, V> extends AbstractCollection<V>
    {
        @Weak
        final Map<K, V> a;
        
        d(final Map<K, V> map) {
            this.a = Preconditions.checkNotNull(map);
        }
        
        final Map<K, V> a() {
            return this.a;
        }
        
        @Override
        public void clear() {
            this.a().clear();
        }
        
        @Override
        public boolean contains(@NullableDecl final Object o) {
            return this.a().containsValue(o);
        }
        
        @Override
        public boolean isEmpty() {
            return this.a().isEmpty();
        }
        
        @Override
        public Iterator<V> iterator() {
            return ab.b(this.a().entrySet().iterator());
        }
        
        @Override
        public boolean remove(final Object o) {
            try {
                return super.remove(o);
            }
            catch (final UnsupportedOperationException ex) {
                for (final Map.Entry<K, Object> entry : this.a().entrySet()) {
                    if (Objects.equal(o, entry.getValue())) {
                        this.a().remove(entry.getKey());
                        return true;
                    }
                }
                return false;
            }
        }
        
        @Override
        public boolean removeAll(final Collection<?> collection) {
            try {
                return super.removeAll(Preconditions.checkNotNull(collection));
            }
            catch (final UnsupportedOperationException ex) {
                final HashSet<Object> a = aq.a();
                for (final Map.Entry<K, Object> entry : this.a().entrySet()) {
                    if (collection.contains(entry.getValue())) {
                        a.add(entry.getKey());
                    }
                }
                return this.a().keySet().removeAll(a);
            }
        }
        
        @Override
        public boolean retainAll(final Collection<?> collection) {
            try {
                return super.retainAll(Preconditions.checkNotNull(collection));
            }
            catch (final UnsupportedOperationException ex) {
                final HashSet<Object> a = aq.a();
                for (final Map.Entry<K, Object> entry : this.a().entrySet()) {
                    if (collection.contains(entry.getValue())) {
                        a.add(entry.getKey());
                    }
                }
                return this.a().keySet().retainAll(a);
            }
        }
        
        @Override
        public int size() {
            return this.a().size();
        }
    }
    
    abstract static class e<K, V> extends AbstractMap<K, V>
    {
        @MonotonicNonNullDecl
        private transient Set<Entry<K, V>> a;
        @MonotonicNonNullDecl
        private transient Set<K> b;
        @MonotonicNonNullDecl
        private transient Collection<V> c;
        
        abstract Set<Entry<K, V>> a();
        
        @Override
        public Set<Entry<K, V>> entrySet() {
            Set<Entry<K, V>> a;
            if ((a = this.a) == null) {
                a = this.a();
                this.a = a;
            }
            return a;
        }
        
        Set<K> h() {
            return (Set<K>)new c((Map<Object, Object>)this);
        }
        
        Collection<V> i() {
            return (Collection<V>)new d((Map<Object, Object>)this);
        }
        
        @Override
        public Set<K> keySet() {
            Set<K> b;
            if ((b = this.b) == null) {
                b = this.h();
                this.b = b;
            }
            return b;
        }
        
        @Override
        public Collection<V> values() {
            Collection<V> c;
            if ((c = this.c) == null) {
                c = this.i();
                this.c = c;
            }
            return c;
        }
    }
}

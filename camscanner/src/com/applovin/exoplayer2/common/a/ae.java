// 
// Decompiled by Procyon v0.6.0
// 

package com.applovin.exoplayer2.common.a;

import java.util.AbstractCollection;
import java.util.Set;
import com.applovin.exoplayer2.common.base.Preconditions;
import org.checkerframework.checker.nullness.compatqual.NullableDecl;
import java.util.List;
import com.applovin.exoplayer2.common.base.Supplier;
import java.util.Collection;
import java.util.Map;

public final class ae
{
    public static <K, V> z<K, V> a(final Map<K, Collection<V>> map, final Supplier<? extends List<V>> supplier) {
        return new a<K, V>(map, supplier);
    }
    
    static boolean a(final ac<?, ?> ac, @NullableDecl final Object o) {
        return o == ac || (o instanceof ac && ac.b().equals(((ac)o).b()));
    }
    
    private static class a<K, V> extends c<K, V>
    {
        transient Supplier<? extends List<V>> a;
        
        a(final Map<K, Collection<V>> map, final Supplier<? extends List<V>> supplier) {
            super(map);
            this.a = Preconditions.checkNotNull(supplier);
        }
        
        protected List<V> a() {
            return (List)this.a.get();
        }
        
        @Override
        Set<K> f() {
            return this.g();
        }
        
        @Override
        Map<K, Collection<V>> n() {
            return this.o();
        }
    }
    
    abstract static class b<K, V> extends AbstractCollection<Map.Entry<K, V>>
    {
        abstract ac<K, V> a();
        
        @Override
        public void clear() {
            this.a().e();
        }
        
        @Override
        public boolean contains(@NullableDecl final Object o) {
            if (o instanceof Map.Entry) {
                final Map.Entry entry = (Map.Entry)o;
                return this.a().b(entry.getKey(), entry.getValue());
            }
            return false;
        }
        
        @Override
        public boolean remove(@NullableDecl final Object o) {
            if (o instanceof Map.Entry) {
                final Map.Entry entry = (Map.Entry)o;
                return this.a().c(entry.getKey(), entry.getValue());
            }
            return false;
        }
        
        @Override
        public int size() {
            return this.a().d();
        }
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.applovin.exoplayer2.common.a;

import com.applovin.exoplayer2.common.base.Preconditions;
import java.util.Iterator;

abstract class aw<F, T> implements Iterator<T>
{
    final Iterator<? extends F> a;
    
    aw(final Iterator<? extends F> iterator) {
        this.a = Preconditions.checkNotNull(iterator);
    }
    
    abstract T a(final F p0);
    
    @Override
    public final boolean hasNext() {
        return this.a.hasNext();
    }
    
    @Override
    public final T next() {
        return this.a(this.a.next());
    }
    
    @Override
    public final void remove() {
        this.a.remove();
    }
}

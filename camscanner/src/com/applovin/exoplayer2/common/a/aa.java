// 
// Decompiled by Procyon v0.6.0
// 

package com.applovin.exoplayer2.common.a;

import java.util.ListIterator;
import com.applovin.exoplayer2.common.base.Objects;
import java.util.RandomAccess;
import com.applovin.exoplayer2.common.base.Preconditions;
import org.checkerframework.checker.nullness.compatqual.NullableDecl;
import java.util.List;
import java.util.Collection;
import java.util.Iterator;
import java.util.ArrayList;

public final class aa
{
    public static <E> ArrayList<E> a() {
        return new ArrayList<E>();
    }
    
    public static <E> ArrayList<E> a(final Iterator<? extends E> iterator) {
        final ArrayList<Object> a = a();
        y.a(a, iterator);
        return (ArrayList<E>)a;
    }
    
    static boolean a(final List<?> list, @NullableDecl final Object o) {
        if (o == Preconditions.checkNotNull(list)) {
            return true;
        }
        if (!(o instanceof List)) {
            return false;
        }
        final List list2 = (List)o;
        final int size = list.size();
        if (size != list2.size()) {
            return false;
        }
        if (list instanceof RandomAccess && list2 instanceof RandomAccess) {
            for (int i = 0; i < size; ++i) {
                if (!Objects.equal(list.get(i), list2.get(i))) {
                    return false;
                }
            }
            return true;
        }
        return y.a((Iterator<?>)list.iterator(), list2.iterator());
    }
    
    static int b(final List<?> list, @NullableDecl final Object o) {
        if (list instanceof RandomAccess) {
            return d(list, o);
        }
        final ListIterator<?> listIterator = list.listIterator();
        while (listIterator.hasNext()) {
            if (Objects.equal(o, listIterator.next())) {
                return listIterator.previousIndex();
            }
        }
        return -1;
    }
    
    static int c(final List<?> list, @NullableDecl final Object o) {
        if (list instanceof RandomAccess) {
            return e(list, o);
        }
        final ListIterator<?> listIterator = list.listIterator(list.size());
        while (listIterator.hasPrevious()) {
            if (Objects.equal(o, listIterator.previous())) {
                return listIterator.nextIndex();
            }
        }
        return -1;
    }
    
    private static int d(final List<?> list, @NullableDecl final Object o) {
        final int size = list.size();
        int i = 0;
        final int n = 0;
        if (o == null) {
            for (int j = n; j < size; ++j) {
                if (list.get(j) == null) {
                    return j;
                }
            }
        }
        else {
            while (i < size) {
                if (o.equals(list.get(i))) {
                    return i;
                }
                ++i;
            }
        }
        return -1;
    }
    
    private static int e(final List<?> list, @NullableDecl final Object o) {
        if (o == null) {
            for (int i = list.size() - 1; i >= 0; --i) {
                if (list.get(i) == null) {
                    return i;
                }
            }
        }
        else {
            for (int j = list.size() - 1; j >= 0; --j) {
                if (o.equals(list.get(j))) {
                    return j;
                }
            }
        }
        return -1;
    }
}

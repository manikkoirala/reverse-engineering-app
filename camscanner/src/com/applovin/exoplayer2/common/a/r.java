// 
// Decompiled by Procyon v0.6.0
// 

package com.applovin.exoplayer2.common.a;

import org.checkerframework.checker.nullness.compatqual.NullableDecl;
import java.io.Serializable;

class r<K, V> extends e<K, V> implements Serializable
{
    @NullableDecl
    final K a;
    @NullableDecl
    final V b;
    
    r(@NullableDecl final K a, @NullableDecl final V b) {
        this.a = a;
        this.b = b;
    }
    
    @NullableDecl
    @Override
    public final K getKey() {
        return this.a;
    }
    
    @NullableDecl
    @Override
    public final V getValue() {
        return this.b;
    }
    
    @Override
    public final V setValue(final V v) {
        throw new UnsupportedOperationException();
    }
}

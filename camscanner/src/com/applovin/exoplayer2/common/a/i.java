// 
// Decompiled by Procyon v0.6.0
// 

package com.applovin.exoplayer2.common.a;

import com.applovin.exoplayer2.common.base.Objects;
import org.checkerframework.checker.nullness.compatqual.NullableDecl;
import com.applovin.exoplayer2.common.base.Preconditions;
import com.applovin.exoplayer2.common.base.Function;
import java.io.Serializable;

final class i<F, T> extends ai<F> implements Serializable
{
    final Function<F, ? extends T> a;
    final ai<T> b;
    
    i(final Function<F, ? extends T> function, final ai<T> ai) {
        this.a = Preconditions.checkNotNull(function);
        this.b = Preconditions.checkNotNull(ai);
    }
    
    @Override
    public int compare(final F n, final F n2) {
        return this.b.compare((T)this.a.apply(n), (T)this.a.apply(n2));
    }
    
    @Override
    public boolean equals(@NullableDecl final Object o) {
        boolean b = true;
        if (o == this) {
            return true;
        }
        if (o instanceof i) {
            final i i = (i)o;
            if (!this.a.equals(i.a) || !this.b.equals(i.b)) {
                b = false;
            }
            return b;
        }
        return false;
    }
    
    @Override
    public int hashCode() {
        return Objects.hashCode(this.a, this.b);
    }
    
    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append(this.b);
        sb.append(".onResultOf(");
        sb.append(this.a);
        sb.append(")");
        return sb.toString();
    }
}

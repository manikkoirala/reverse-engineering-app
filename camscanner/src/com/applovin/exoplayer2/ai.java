// 
// Decompiled by Procyon v0.6.0
// 

package com.applovin.exoplayer2;

import androidx.annotation.Nullable;
import java.io.IOException;

public class ai extends IOException
{
    public final boolean a;
    public final int b;
    
    protected ai(@Nullable final String message, @Nullable final Throwable cause, final boolean a, final int b) {
        super(message, cause);
        this.a = a;
        this.b = b;
    }
    
    public static ai a(@Nullable final String s) {
        return new ai(s, null, false, 1);
    }
    
    public static ai a(@Nullable final String s, @Nullable final Throwable t) {
        return new ai(s, t, true, 0);
    }
    
    public static ai b(@Nullable final String s, @Nullable final Throwable t) {
        return new ai(s, t, true, 1);
    }
}

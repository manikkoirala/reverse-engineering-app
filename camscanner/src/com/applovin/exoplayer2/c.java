// 
// Decompiled by Procyon v0.6.0
// 

package com.applovin.exoplayer2;

import androidx.annotation.RequiresApi;
import android.media.AudioFocusRequest$Builder;
import android.media.AudioManager$OnAudioFocusChangeListener;
import com.applovin.exoplayer2.l.ai;
import com.applovin.exoplayer2.l.q;
import com.applovin.exoplayer2.l.a;
import android.os.Handler;
import android.content.Context;
import android.media.AudioFocusRequest;
import com.applovin.exoplayer2.b.d;
import androidx.annotation.Nullable;
import android.media.AudioManager;

final class c
{
    private final AudioManager a;
    private final a b;
    @Nullable
    private b c;
    @Nullable
    private d d;
    private int e;
    private int f;
    private float g;
    private AudioFocusRequest h;
    private boolean i;
    
    public c(final Context context, final Handler handler, final b c) {
        this.g = 1.0f;
        this.a = (AudioManager)com.applovin.exoplayer2.l.a.b((Object)context.getApplicationContext().getSystemService("audio"));
        this.c = c;
        this.b = new a(handler);
        this.e = 0;
    }
    
    private boolean a(final int n) {
        boolean b = true;
        if (n != 1) {
            b = (this.f != 1 && b);
        }
        return b;
    }
    
    private static int b(@Nullable final d d) {
        if (d == null) {
            return 0;
        }
        switch (d.d) {
            default: {
                final StringBuilder sb = new StringBuilder();
                sb.append("Unidentified audio usage: ");
                sb.append(d.d);
                q.c("AudioFocusManager", sb.toString());
                return 0;
            }
            case 16: {
                if (ai.a >= 19) {
                    return 4;
                }
                return 2;
            }
            case 11: {
                if (d.b == 1) {
                    return 2;
                }
                return 3;
            }
            case 5:
            case 6:
            case 7:
            case 8:
            case 9:
            case 10:
            case 12:
            case 13: {
                return 3;
            }
            case 3: {
                return 0;
            }
            case 2:
            case 4: {
                return 2;
            }
            case 1:
            case 14: {
                return 1;
            }
            case 0: {
                q.c("AudioFocusManager", "Specify a proper usage in the audio attributes for audio focus handling. Using AUDIOFOCUS_GAIN by default.");
                return 1;
            }
        }
    }
    
    private void b(final int e) {
        if (this.e == e) {
            return;
        }
        float g;
        if ((this.e = e) == 3) {
            g = 0.2f;
        }
        else {
            g = 1.0f;
        }
        if (this.g == g) {
            return;
        }
        this.g = g;
        final b c = this.c;
        if (c != null) {
            c.a(g);
        }
    }
    
    private int c() {
        if (this.e == 1) {
            return 1;
        }
        int n;
        if (ai.a >= 26) {
            n = this.f();
        }
        else {
            n = this.e();
        }
        if (n == 1) {
            this.b(1);
            return 1;
        }
        this.b(0);
        return -1;
    }
    
    private void c(final int i) {
        if (i == -3 || i == -2) {
            if (i != -2 && !this.i()) {
                this.b(3);
            }
            else {
                this.d(0);
                this.b(2);
            }
            return;
        }
        if (i == -1) {
            this.d(-1);
            this.d();
            return;
        }
        if (i != 1) {
            final StringBuilder sb = new StringBuilder();
            sb.append("Unknown focus change type: ");
            sb.append(i);
            q.c("AudioFocusManager", sb.toString());
            return;
        }
        this.b(1);
        this.d(1);
    }
    
    private void d() {
        if (this.e == 0) {
            return;
        }
        if (ai.a >= 26) {
            this.h();
        }
        else {
            this.g();
        }
        this.b(0);
    }
    
    private void d(final int n) {
        final b c = this.c;
        if (c != null) {
            c.a(n);
        }
    }
    
    private int e() {
        return this.a.requestAudioFocus((AudioManager$OnAudioFocusChangeListener)this.b, ai.g(((d)com.applovin.exoplayer2.l.a.b((Object)this.d)).d), this.f);
    }
    
    @RequiresApi(26)
    private int f() {
        final AudioFocusRequest h = this.h;
        if (h == null || this.i) {
            AudioFocusRequest$Builder audioFocusRequest$Builder;
            if (h == null) {
                audioFocusRequest$Builder = new AudioFocusRequest$Builder(this.f);
            }
            else {
                audioFocusRequest$Builder = new AudioFocusRequest$Builder(this.h);
            }
            this.h = \u30070000OOO.\u3007080(\u3007oOO8O8.\u3007080(O8ooOoo\u3007.\u3007080(O\u30078O8\u3007008.\u3007080(audioFocusRequest$Builder, ((d)com.applovin.exoplayer2.l.a.b((Object)this.d)).a()), this.i()), (AudioManager$OnAudioFocusChangeListener)this.b));
            this.i = false;
        }
        return o\u3007\u30070\u3007.\u3007080(this.a, this.h);
    }
    
    private void g() {
        this.a.abandonAudioFocus((AudioManager$OnAudioFocusChangeListener)this.b);
    }
    
    @RequiresApi(26)
    private void h() {
        final AudioFocusRequest h = this.h;
        if (h != null) {
            \u300700.\u3007080(this.a, h);
        }
    }
    
    private boolean i() {
        final d d = this.d;
        if (d != null) {
            final int b = d.b;
            final boolean b2 = true;
            if (b == 1) {
                return b2;
            }
        }
        return false;
    }
    
    public float a() {
        return this.g;
    }
    
    public int a(final boolean b, int c) {
        final boolean a = this.a(c);
        c = -1;
        if (a) {
            this.d();
            if (b) {
                c = 1;
            }
            return c;
        }
        if (b) {
            c = this.c();
        }
        return c;
    }
    
    public void a(@Nullable final d d) {
        if (!ai.a((Object)this.d, (Object)d)) {
            this.d = d;
            final int b = b(d);
            this.f = b;
            boolean b2 = true;
            if (b != 1) {
                b2 = (b == 0 && b2);
            }
            com.applovin.exoplayer2.l.a.a(b2, (Object)"Automatic handling of audio focus is only available for USAGE_MEDIA and USAGE_GAME.");
        }
    }
    
    public void b() {
        this.c = null;
        this.d();
    }
    
    private class a implements AudioManager$OnAudioFocusChangeListener
    {
        final c a;
        private final Handler b;
        
        public a(final c a, final Handler b) {
            this.a = a;
            this.b = b;
        }
        
        public void onAudioFocusChange(final int n) {
            this.b.post((Runnable)new OOO\u3007O0(this, n));
        }
    }
    
    public interface b
    {
        void a(final float p0);
        
        void a(final int p0);
    }
}

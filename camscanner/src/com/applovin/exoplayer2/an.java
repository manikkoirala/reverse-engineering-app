// 
// Decompiled by Procyon v0.6.0
// 

package com.applovin.exoplayer2;

import android.os.BaseBundle;
import com.applovin.exoplayer2.common.base.Objects;
import com.applovin.exoplayer2.l.c;
import com.applovin.exoplayer2.l.m$a;
import java.util.ArrayList;
import android.os.Bundle;
import com.applovin.exoplayer2.l.m;
import android.os.Looper;
import android.view.TextureView;
import androidx.annotation.Nullable;
import android.view.SurfaceView;
import androidx.annotation.IntRange;
import com.applovin.exoplayer2.i.a;
import java.util.List;
import com.applovin.exoplayer2.m.o;
import com.applovin.exoplayer2.j.h;
import com.applovin.exoplayer2.h.ad;

public interface an
{
    long A();
    
    long B();
    
    long C();
    
    am D();
    
    int F();
    
    int G();
    
    long H();
    
    long I();
    
    long J();
    
    boolean K();
    
    int L();
    
    int M();
    
    long N();
    
    long O();
    
    ad P();
    
    h Q();
    
    ac R();
    
    ba S();
    
    o T();
    
    List<com.applovin.exoplayer2.i.a> V();
    
    void a(@IntRange(from = 0L) final int p0, final long p1);
    
    void a(final long p0);
    
    void a(@Nullable final SurfaceView p0);
    
    void a(@Nullable final TextureView p0);
    
    void a(final d p0);
    
    void a(final boolean p0);
    
    boolean a();
    
    boolean a(final int p0);
    
    void b(@Nullable final SurfaceView p0);
    
    void b(@Nullable final TextureView p0);
    
    void b(final d p0);
    
    void b(final boolean p0);
    
    void c();
    
    void c(final int p0);
    
    void d();
    
    @Nullable
    ak e();
    
    void g();
    
    void j();
    
    boolean o();
    
    Looper r();
    
    a s();
    
    int t();
    
    int u();
    
    void w();
    
    boolean x();
    
    int y();
    
    boolean z();
    
    public static final class a implements g
    {
        public static final an.a a;
        public static final g.a<an.a> b;
        private final m c;
        
        static {
            a = new an.a.a().a();
            b = new OO0o\u3007\u3007\u3007\u30070();
        }
        
        private a(final m c) {
            this.c = c;
        }
        
        private static an.a a(final Bundle bundle) {
            int i = 0;
            final ArrayList integerArrayList = bundle.getIntegerArrayList(b(0));
            if (integerArrayList == null) {
                return an.a.a;
            }
            final an.a.a a = new an.a.a();
            while (i < integerArrayList.size()) {
                a.a((int)integerArrayList.get(i));
                ++i;
            }
            return a.a();
        }
        
        private static String b(final int i) {
            return Integer.toString(i, 36);
        }
        
        public boolean a(final int n) {
            return this.c.a(n);
        }
        
        @Override
        public boolean equals(@Nullable final Object o) {
            return this == o || (o instanceof an.a && this.c.equals((Object)((an.a)o).c));
        }
        
        @Override
        public int hashCode() {
            return this.c.hashCode();
        }
        
        public static final class a
        {
            private static final int[] a;
            private final m$a b;
            
            static {
                a = new int[] { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28 };
            }
            
            public a() {
                this.b = new m$a();
            }
            
            public a a(final int n) {
                this.b.a(n);
                return this;
            }
            
            public a a(final int n, final boolean b) {
                this.b.a(n, b);
                return this;
            }
            
            public a a(final an.a a) {
                this.b.a(a.c);
                return this;
            }
            
            public a a(final int... array) {
                this.b.a(array);
                return this;
            }
            
            public an.a a() {
                return new an.a(this.b.a());
            }
        }
    }
    
    @Deprecated
    public interface b
    {
        void a(@Nullable final ab p0, final int p1);
        
        void a(final ac p0);
        
        void a(final ak p0);
        
        void a(final am p0);
        
        void a(final an.a p0);
        
        void a(final e p0, final e p1, final int p2);
        
        void a(final an p0, final c p1);
        
        void a(final ba p0, final int p1);
        
        void a(final ad p0, final h p1);
        
        @Deprecated
        void a(final boolean p0, final int p1);
        
        @Deprecated
        void b();
        
        void b(final int p0);
        
        void b(@Nullable final ak p0);
        
        void b(final boolean p0, final int p1);
        
        void b_(final boolean p0);
        
        void c(final int p0);
        
        @Deprecated
        void c(final boolean p0);
        
        void d(final int p0);
        
        void d(final boolean p0);
        
        @Deprecated
        void e(final int p0);
        
        void e(final boolean p0);
    }
    
    public static final class c
    {
        private final m a;
        
        public c(final m a) {
            this.a = a;
        }
        
        public boolean a(final int n) {
            return this.a.a(n);
        }
        
        public boolean a(final int... array) {
            return this.a.a(array);
        }
        
        @Override
        public boolean equals(@Nullable final Object o) {
            return this == o || (o instanceof c && this.a.equals((Object)((c)o).a));
        }
        
        @Override
        public int hashCode() {
            return this.a.hashCode();
        }
    }
    
    public interface d extends b
    {
        void a();
        
        void a(final float p0);
        
        void a(final int p0, final int p1);
        
        void a(final int p0, final boolean p1);
        
        void a(@Nullable final ab p0, final int p1);
        
        void a(final ac p0);
        
        void a(final ak p0);
        
        void a(final am p0);
        
        void a(final an.a p0);
        
        void a(final e p0, final e p1, final int p2);
        
        void a(final an p0, final c p1);
        
        void a(final ba p0, final int p1);
        
        void a(final com.applovin.exoplayer2.g.a p0);
        
        void a(final ad p0, final h p1);
        
        void a(final o p0);
        
        void a(final com.applovin.exoplayer2.o p0);
        
        void a(final List<a> p0);
        
        void a_(final boolean p0);
        
        void b(final int p0);
        
        void b(@Nullable final ak p0);
        
        void b(final boolean p0, final int p1);
        
        void b_(final boolean p0);
        
        void c(final int p0);
        
        void d(final int p0);
        
        void d(final boolean p0);
        
        void e(final boolean p0);
    }
    
    public static final class e implements g
    {
        public static final g.a<e> j;
        @Nullable
        public final Object a;
        public final int b;
        @Nullable
        public final ab c;
        @Nullable
        public final Object d;
        public final int e;
        public final long f;
        public final long g;
        public final int h;
        public final int i;
        
        static {
            j = new OO0o\u3007\u3007();
        }
        
        public e(@Nullable final Object a, final int b, @Nullable final ab c, @Nullable final Object d, final int e, final long f, final long g, final int h, final int i) {
            this.a = a;
            this.b = b;
            this.c = c;
            this.d = d;
            this.e = e;
            this.f = f;
            this.g = g;
            this.h = h;
            this.i = i;
        }
        
        private static e a(final Bundle bundle) {
            return new e(null, ((BaseBundle)bundle).getInt(a(0), -1), (ab)com.applovin.exoplayer2.l.c.a((g.a)ab.g, bundle.getBundle(a(1))), null, ((BaseBundle)bundle).getInt(a(2), -1), ((BaseBundle)bundle).getLong(a(3), -9223372036854775807L), ((BaseBundle)bundle).getLong(a(4), -9223372036854775807L), ((BaseBundle)bundle).getInt(a(5), -1), ((BaseBundle)bundle).getInt(a(6), -1));
        }
        
        private static String a(final int i) {
            return Integer.toString(i, 36);
        }
        
        @Override
        public boolean equals(@Nullable final Object o) {
            boolean b = true;
            if (this == o) {
                return true;
            }
            if (o != null && e.class == o.getClass()) {
                final e e = (e)o;
                if (this.b != e.b || this.e != e.e || this.f != e.f || this.g != e.g || this.h != e.h || this.i != e.i || !Objects.equal(this.a, e.a) || !Objects.equal(this.d, e.d) || !Objects.equal(this.c, e.c)) {
                    b = false;
                }
                return b;
            }
            return false;
        }
        
        @Override
        public int hashCode() {
            return Objects.hashCode(this.a, this.b, this.c, this.d, this.e, this.b, this.f, this.g, this.h, this.i);
        }
    }
}

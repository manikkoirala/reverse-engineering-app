// 
// Decompiled by Procyon v0.6.0
// 

package com.applovin.exoplayer2;

import com.applovin.exoplayer2.i.m;
import com.applovin.exoplayer2.i.l;
import android.os.Looper;
import com.applovin.exoplayer2.m.a.b;
import com.applovin.exoplayer2.b.g;
import java.util.ArrayList;
import android.os.Handler;
import androidx.annotation.Nullable;
import com.applovin.exoplayer2.b.f;
import com.applovin.exoplayer2.b.e;
import com.applovin.exoplayer2.b.h;
import com.applovin.exoplayer2.f.k;
import android.content.Context;

public class n implements au
{
    private final Context a;
    private int b;
    private long c;
    private boolean d;
    private k e;
    private boolean f;
    private boolean g;
    private boolean h;
    private boolean i;
    private boolean j;
    private boolean k;
    
    public n(final Context a) {
        this.a = a;
        this.b = 0;
        this.c = 5000L;
        this.e = com.applovin.exoplayer2.f.k.a;
    }
    
    @Nullable
    protected h a(final Context context, final boolean b, final boolean b2, final boolean b3) {
        return new com.applovin.exoplayer2.b.n(com.applovin.exoplayer2.b.e.a(context), (com.applovin.exoplayer2.b.n.a)new com.applovin.exoplayer2.b.n.c(new f[0]), b, b2, b3 ? 1 : 0);
    }
    
    protected void a(final Context p0, final int p1, final k p2, final boolean p3, final Handler p4, final com.applovin.exoplayer2.m.n p5, final long p6, final ArrayList<ar> p7) {
        // 
        // This method could not be decompiled.
        // 
        // Original Bytecode:
        // 
        //     3: dup            
        //     4: aload_1        
        //     5: aload_3        
        //     6: lload           7
        //     8: iload           4
        //    10: aload           5
        //    12: aload           6
        //    14: bipush          50
        //    16: invokespecial   com/applovin/exoplayer2/m/h.<init>:(Landroid/content/Context;Lcom/applovin/exoplayer2/f/k;JZLandroid/os/Handler;Lcom/applovin/exoplayer2/m/n;I)V
        //    19: astore_1       
        //    20: aload_1        
        //    21: aload_0        
        //    22: getfield        com/applovin/exoplayer2/n.f:Z
        //    25: invokevirtual   com/applovin/exoplayer2/f/j.a:(Z)V
        //    28: aload_1        
        //    29: aload_0        
        //    30: getfield        com/applovin/exoplayer2/n.g:Z
        //    33: invokevirtual   com/applovin/exoplayer2/f/j.b:(Z)V
        //    36: aload_1        
        //    37: aload_0        
        //    38: getfield        com/applovin/exoplayer2/n.h:Z
        //    41: invokevirtual   com/applovin/exoplayer2/f/j.c:(Z)V
        //    44: aload           9
        //    46: aload_1        
        //    47: invokevirtual   java/util/ArrayList.add:(Ljava/lang/Object;)Z
        //    50: pop            
        //    51: iload_2        
        //    52: ifne            56
        //    55: return         
        //    56: aload           9
        //    58: invokevirtual   java/util/ArrayList.size:()I
        //    61: istore          11
        //    63: iload           11
        //    65: istore          10
        //    67: iload_2        
        //    68: iconst_2       
        //    69: if_icmpne       78
        //    72: iload           11
        //    74: iconst_1       
        //    75: isub           
        //    76: istore          10
        //    78: ldc             "com.applovin.exoplayer2.ext.vp9.LibvpxVideoRenderer"
        //    80: invokestatic    java/lang/Class.forName:(Ljava/lang/String;)Ljava/lang/Class;
        //    83: iconst_4       
        //    84: anewarray       Ljava/lang/Class;
        //    87: dup            
        //    88: iconst_0       
        //    89: getstatic       java/lang/Long.TYPE:Ljava/lang/Class;
        //    92: aastore        
        //    93: dup            
        //    94: iconst_1       
        //    95: ldc             Landroid/os/Handler;.class
        //    97: aastore        
        //    98: dup            
        //    99: iconst_2       
        //   100: ldc             Lcom/applovin/exoplayer2/m/n;.class
        //   102: aastore        
        //   103: dup            
        //   104: iconst_3       
        //   105: getstatic       java/lang/Integer.TYPE:Ljava/lang/Class;
        //   108: aastore        
        //   109: invokevirtual   java/lang/Class.getConstructor:([Ljava/lang/Class;)Ljava/lang/reflect/Constructor;
        //   112: iconst_4       
        //   113: anewarray       Ljava/lang/Object;
        //   116: dup            
        //   117: iconst_0       
        //   118: lload           7
        //   120: invokestatic    java/lang/Long.valueOf:(J)Ljava/lang/Long;
        //   123: aastore        
        //   124: dup            
        //   125: iconst_1       
        //   126: aload           5
        //   128: aastore        
        //   129: dup            
        //   130: iconst_2       
        //   131: aload           6
        //   133: aastore        
        //   134: dup            
        //   135: iconst_3       
        //   136: bipush          50
        //   138: invokestatic    java/lang/Integer.valueOf:(I)Ljava/lang/Integer;
        //   141: aastore        
        //   142: invokevirtual   java/lang/reflect/Constructor.newInstance:([Ljava/lang/Object;)Ljava/lang/Object;
        //   145: checkcast       Lcom/applovin/exoplayer2/ar;
        //   148: astore_1       
        //   149: iload           10
        //   151: iconst_1       
        //   152: iadd           
        //   153: istore_2       
        //   154: aload           9
        //   156: iload           10
        //   158: aload_1        
        //   159: invokevirtual   java/util/ArrayList.add:(ILjava/lang/Object;)V
        //   162: ldc             "DefaultRenderersFactory"
        //   164: ldc             "Loaded LibvpxVideoRenderer."
        //   166: invokestatic    com/applovin/exoplayer2/l/q.b:(Ljava/lang/String;Ljava/lang/String;)V
        //   169: goto            194
        //   172: astore_1       
        //   173: iload_2        
        //   174: istore          10
        //   176: goto            191
        //   179: astore_1       
        //   180: new             Ljava/lang/RuntimeException;
        //   183: dup            
        //   184: ldc             "Error instantiating VP9 extension"
        //   186: aload_1        
        //   187: invokespecial   java/lang/RuntimeException.<init>:(Ljava/lang/String;Ljava/lang/Throwable;)V
        //   190: athrow         
        //   191: iload           10
        //   193: istore_2       
        //   194: ldc             "com.applovin.exoplayer2.ext.av1.Libgav1VideoRenderer"
        //   196: invokestatic    java/lang/Class.forName:(Ljava/lang/String;)Ljava/lang/Class;
        //   199: iconst_4       
        //   200: anewarray       Ljava/lang/Class;
        //   203: dup            
        //   204: iconst_0       
        //   205: getstatic       java/lang/Long.TYPE:Ljava/lang/Class;
        //   208: aastore        
        //   209: dup            
        //   210: iconst_1       
        //   211: ldc             Landroid/os/Handler;.class
        //   213: aastore        
        //   214: dup            
        //   215: iconst_2       
        //   216: ldc             Lcom/applovin/exoplayer2/m/n;.class
        //   218: aastore        
        //   219: dup            
        //   220: iconst_3       
        //   221: getstatic       java/lang/Integer.TYPE:Ljava/lang/Class;
        //   224: aastore        
        //   225: invokevirtual   java/lang/Class.getConstructor:([Ljava/lang/Class;)Ljava/lang/reflect/Constructor;
        //   228: iconst_4       
        //   229: anewarray       Ljava/lang/Object;
        //   232: dup            
        //   233: iconst_0       
        //   234: lload           7
        //   236: invokestatic    java/lang/Long.valueOf:(J)Ljava/lang/Long;
        //   239: aastore        
        //   240: dup            
        //   241: iconst_1       
        //   242: aload           5
        //   244: aastore        
        //   245: dup            
        //   246: iconst_2       
        //   247: aload           6
        //   249: aastore        
        //   250: dup            
        //   251: iconst_3       
        //   252: bipush          50
        //   254: invokestatic    java/lang/Integer.valueOf:(I)Ljava/lang/Integer;
        //   257: aastore        
        //   258: invokevirtual   java/lang/reflect/Constructor.newInstance:([Ljava/lang/Object;)Ljava/lang/Object;
        //   261: checkcast       Lcom/applovin/exoplayer2/ar;
        //   264: astore_1       
        //   265: iload_2        
        //   266: iconst_1       
        //   267: iadd           
        //   268: istore          10
        //   270: aload           9
        //   272: iload_2        
        //   273: aload_1        
        //   274: invokevirtual   java/util/ArrayList.add:(ILjava/lang/Object;)V
        //   277: ldc             "DefaultRenderersFactory"
        //   279: ldc             "Loaded Libgav1VideoRenderer."
        //   281: invokestatic    com/applovin/exoplayer2/l/q.b:(Ljava/lang/String;Ljava/lang/String;)V
        //   284: iload           10
        //   286: istore_2       
        //   287: goto            309
        //   290: astore_1       
        //   291: iload           10
        //   293: istore_2       
        //   294: goto            309
        //   297: astore_1       
        //   298: new             Ljava/lang/RuntimeException;
        //   301: dup            
        //   302: ldc             "Error instantiating AV1 extension"
        //   304: aload_1        
        //   305: invokespecial   java/lang/RuntimeException.<init>:(Ljava/lang/String;Ljava/lang/Throwable;)V
        //   308: athrow         
        //   309: aload           9
        //   311: iload_2        
        //   312: ldc             "com.applovin.exoplayer2.ext.ffmpeg.FfmpegVideoRenderer"
        //   314: invokestatic    java/lang/Class.forName:(Ljava/lang/String;)Ljava/lang/Class;
        //   317: iconst_4       
        //   318: anewarray       Ljava/lang/Class;
        //   321: dup            
        //   322: iconst_0       
        //   323: getstatic       java/lang/Long.TYPE:Ljava/lang/Class;
        //   326: aastore        
        //   327: dup            
        //   328: iconst_1       
        //   329: ldc             Landroid/os/Handler;.class
        //   331: aastore        
        //   332: dup            
        //   333: iconst_2       
        //   334: ldc             Lcom/applovin/exoplayer2/m/n;.class
        //   336: aastore        
        //   337: dup            
        //   338: iconst_3       
        //   339: getstatic       java/lang/Integer.TYPE:Ljava/lang/Class;
        //   342: aastore        
        //   343: invokevirtual   java/lang/Class.getConstructor:([Ljava/lang/Class;)Ljava/lang/reflect/Constructor;
        //   346: iconst_4       
        //   347: anewarray       Ljava/lang/Object;
        //   350: dup            
        //   351: iconst_0       
        //   352: lload           7
        //   354: invokestatic    java/lang/Long.valueOf:(J)Ljava/lang/Long;
        //   357: aastore        
        //   358: dup            
        //   359: iconst_1       
        //   360: aload           5
        //   362: aastore        
        //   363: dup            
        //   364: iconst_2       
        //   365: aload           6
        //   367: aastore        
        //   368: dup            
        //   369: iconst_3       
        //   370: bipush          50
        //   372: invokestatic    java/lang/Integer.valueOf:(I)Ljava/lang/Integer;
        //   375: aastore        
        //   376: invokevirtual   java/lang/reflect/Constructor.newInstance:([Ljava/lang/Object;)Ljava/lang/Object;
        //   379: checkcast       Lcom/applovin/exoplayer2/ar;
        //   382: invokevirtual   java/util/ArrayList.add:(ILjava/lang/Object;)V
        //   385: ldc             "DefaultRenderersFactory"
        //   387: ldc             "Loaded FfmpegVideoRenderer."
        //   389: invokestatic    com/applovin/exoplayer2/l/q.b:(Ljava/lang/String;Ljava/lang/String;)V
        //   392: goto            407
        //   395: astore_1       
        //   396: new             Ljava/lang/RuntimeException;
        //   399: dup            
        //   400: ldc             "Error instantiating FFmpeg extension"
        //   402: aload_1        
        //   403: invokespecial   java/lang/RuntimeException.<init>:(Ljava/lang/String;Ljava/lang/Throwable;)V
        //   406: athrow         
        //   407: return         
        //   408: astore_1       
        //   409: goto            191
        //   412: astore_1       
        //   413: goto            309
        //   416: astore_1       
        //   417: goto            407
        //    Signature:
        //  (Landroid/content/Context;ILcom/applovin/exoplayer2/f/k;ZLandroid/os/Handler;Lcom/applovin/exoplayer2/m/n;JLjava/util/ArrayList<Lcom/applovin/exoplayer2/ar;>;)V
        //    Exceptions:
        //  Try           Handler
        //  Start  End    Start  End    Type                              
        //  -----  -----  -----  -----  ----------------------------------
        //  78     149    408    412    Ljava/lang/ClassNotFoundException;
        //  78     149    179    191    Ljava/lang/Exception;
        //  154    169    172    179    Ljava/lang/ClassNotFoundException;
        //  154    169    179    191    Ljava/lang/Exception;
        //  194    265    412    416    Ljava/lang/ClassNotFoundException;
        //  194    265    297    309    Ljava/lang/Exception;
        //  270    284    290    297    Ljava/lang/ClassNotFoundException;
        //  270    284    297    309    Ljava/lang/Exception;
        //  309    392    416    420    Ljava/lang/ClassNotFoundException;
        //  309    392    395    407    Ljava/lang/Exception;
        // 
        // The error that occurred was:
        // 
        // java.lang.IllegalStateException: Expression is linked from several locations: Label_0194:
        //     at com.strobel.decompiler.ast.Error.expressionLinkedFromMultipleLocations(Error.java:27)
        //     at com.strobel.decompiler.ast.AstOptimizer.mergeDisparateObjectInitializations(AstOptimizer.java:2604)
        //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:235)
        //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:42)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:206)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:93)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethodBody(AstBuilder.java:868)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethod(AstBuilder.java:761)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addTypeMembers(AstBuilder.java:638)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeCore(AstBuilder.java:605)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeNoCache(AstBuilder.java:195)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createType(AstBuilder.java:162)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addType(AstBuilder.java:137)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.buildAst(JavaLanguage.java:71)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.decompileType(JavaLanguage.java:59)
        //     at com.strobel.decompiler.DecompilerDriver.decompileType(DecompilerDriver.java:333)
        //     at com.strobel.decompiler.DecompilerDriver.decompileJar(DecompilerDriver.java:254)
        //     at com.strobel.decompiler.DecompilerDriver.main(DecompilerDriver.java:144)
        // 
        throw new IllegalStateException("An error occurred while decompiling this method.");
    }
    
    protected void a(final Context p0, final int p1, final k p2, final boolean p3, final h p4, final Handler p5, final g p6, final ArrayList<ar> p7) {
        // 
        // This method could not be decompiled.
        // 
        // Original Bytecode:
        // 
        //     3: dup            
        //     4: aload_1        
        //     5: aload_3        
        //     6: iload           4
        //     8: aload           6
        //    10: aload           7
        //    12: aload           5
        //    14: invokespecial   com/applovin/exoplayer2/b/q.<init>:(Landroid/content/Context;Lcom/applovin/exoplayer2/f/k;ZLandroid/os/Handler;Lcom/applovin/exoplayer2/b/g;Lcom/applovin/exoplayer2/b/h;)V
        //    17: astore_1       
        //    18: aload_1        
        //    19: aload_0        
        //    20: getfield        com/applovin/exoplayer2/n.f:Z
        //    23: invokevirtual   com/applovin/exoplayer2/f/j.a:(Z)V
        //    26: aload_1        
        //    27: aload_0        
        //    28: getfield        com/applovin/exoplayer2/n.g:Z
        //    31: invokevirtual   com/applovin/exoplayer2/f/j.b:(Z)V
        //    34: aload_1        
        //    35: aload_0        
        //    36: getfield        com/applovin/exoplayer2/n.h:Z
        //    39: invokevirtual   com/applovin/exoplayer2/f/j.c:(Z)V
        //    42: aload           8
        //    44: aload_1        
        //    45: invokevirtual   java/util/ArrayList.add:(Ljava/lang/Object;)Z
        //    48: pop            
        //    49: iload_2        
        //    50: ifne            54
        //    53: return         
        //    54: aload           8
        //    56: invokevirtual   java/util/ArrayList.size:()I
        //    59: istore          10
        //    61: iload           10
        //    63: istore          9
        //    65: iload_2        
        //    66: iconst_2       
        //    67: if_icmpne       76
        //    70: iload           10
        //    72: iconst_1       
        //    73: isub           
        //    74: istore          9
        //    76: ldc             "com.applovin.exoplayer2.ext.opus.LibopusAudioRenderer"
        //    78: invokestatic    java/lang/Class.forName:(Ljava/lang/String;)Ljava/lang/Class;
        //    81: iconst_3       
        //    82: anewarray       Ljava/lang/Class;
        //    85: dup            
        //    86: iconst_0       
        //    87: ldc             Landroid/os/Handler;.class
        //    89: aastore        
        //    90: dup            
        //    91: iconst_1       
        //    92: ldc             Lcom/applovin/exoplayer2/b/g;.class
        //    94: aastore        
        //    95: dup            
        //    96: iconst_2       
        //    97: ldc             Lcom/applovin/exoplayer2/b/h;.class
        //    99: aastore        
        //   100: invokevirtual   java/lang/Class.getConstructor:([Ljava/lang/Class;)Ljava/lang/reflect/Constructor;
        //   103: iconst_3       
        //   104: anewarray       Ljava/lang/Object;
        //   107: dup            
        //   108: iconst_0       
        //   109: aload           6
        //   111: aastore        
        //   112: dup            
        //   113: iconst_1       
        //   114: aload           7
        //   116: aastore        
        //   117: dup            
        //   118: iconst_2       
        //   119: aload           5
        //   121: aastore        
        //   122: invokevirtual   java/lang/reflect/Constructor.newInstance:([Ljava/lang/Object;)Ljava/lang/Object;
        //   125: checkcast       Lcom/applovin/exoplayer2/ar;
        //   128: astore_1       
        //   129: iload           9
        //   131: iconst_1       
        //   132: iadd           
        //   133: istore_2       
        //   134: aload           8
        //   136: iload           9
        //   138: aload_1        
        //   139: invokevirtual   java/util/ArrayList.add:(ILjava/lang/Object;)V
        //   142: ldc             "DefaultRenderersFactory"
        //   144: ldc             "Loaded LibopusAudioRenderer."
        //   146: invokestatic    com/applovin/exoplayer2/l/q.b:(Ljava/lang/String;Ljava/lang/String;)V
        //   149: goto            174
        //   152: astore_1       
        //   153: iload_2        
        //   154: istore          9
        //   156: goto            171
        //   159: astore_1       
        //   160: new             Ljava/lang/RuntimeException;
        //   163: dup            
        //   164: ldc             "Error instantiating Opus extension"
        //   166: aload_1        
        //   167: invokespecial   java/lang/RuntimeException.<init>:(Ljava/lang/String;Ljava/lang/Throwable;)V
        //   170: athrow         
        //   171: iload           9
        //   173: istore_2       
        //   174: ldc             "com.applovin.exoplayer2.ext.flac.LibflacAudioRenderer"
        //   176: invokestatic    java/lang/Class.forName:(Ljava/lang/String;)Ljava/lang/Class;
        //   179: iconst_3       
        //   180: anewarray       Ljava/lang/Class;
        //   183: dup            
        //   184: iconst_0       
        //   185: ldc             Landroid/os/Handler;.class
        //   187: aastore        
        //   188: dup            
        //   189: iconst_1       
        //   190: ldc             Lcom/applovin/exoplayer2/b/g;.class
        //   192: aastore        
        //   193: dup            
        //   194: iconst_2       
        //   195: ldc             Lcom/applovin/exoplayer2/b/h;.class
        //   197: aastore        
        //   198: invokevirtual   java/lang/Class.getConstructor:([Ljava/lang/Class;)Ljava/lang/reflect/Constructor;
        //   201: iconst_3       
        //   202: anewarray       Ljava/lang/Object;
        //   205: dup            
        //   206: iconst_0       
        //   207: aload           6
        //   209: aastore        
        //   210: dup            
        //   211: iconst_1       
        //   212: aload           7
        //   214: aastore        
        //   215: dup            
        //   216: iconst_2       
        //   217: aload           5
        //   219: aastore        
        //   220: invokevirtual   java/lang/reflect/Constructor.newInstance:([Ljava/lang/Object;)Ljava/lang/Object;
        //   223: checkcast       Lcom/applovin/exoplayer2/ar;
        //   226: astore_1       
        //   227: iload_2        
        //   228: iconst_1       
        //   229: iadd           
        //   230: istore          9
        //   232: aload           8
        //   234: iload_2        
        //   235: aload_1        
        //   236: invokevirtual   java/util/ArrayList.add:(ILjava/lang/Object;)V
        //   239: ldc             "DefaultRenderersFactory"
        //   241: ldc             "Loaded LibflacAudioRenderer."
        //   243: invokestatic    com/applovin/exoplayer2/l/q.b:(Ljava/lang/String;Ljava/lang/String;)V
        //   246: iload           9
        //   248: istore_2       
        //   249: goto            271
        //   252: astore_1       
        //   253: iload           9
        //   255: istore_2       
        //   256: goto            271
        //   259: astore_1       
        //   260: new             Ljava/lang/RuntimeException;
        //   263: dup            
        //   264: ldc             "Error instantiating FLAC extension"
        //   266: aload_1        
        //   267: invokespecial   java/lang/RuntimeException.<init>:(Ljava/lang/String;Ljava/lang/Throwable;)V
        //   270: athrow         
        //   271: aload           8
        //   273: iload_2        
        //   274: ldc             "com.applovin.exoplayer2.ext.ffmpeg.FfmpegAudioRenderer"
        //   276: invokestatic    java/lang/Class.forName:(Ljava/lang/String;)Ljava/lang/Class;
        //   279: iconst_3       
        //   280: anewarray       Ljava/lang/Class;
        //   283: dup            
        //   284: iconst_0       
        //   285: ldc             Landroid/os/Handler;.class
        //   287: aastore        
        //   288: dup            
        //   289: iconst_1       
        //   290: ldc             Lcom/applovin/exoplayer2/b/g;.class
        //   292: aastore        
        //   293: dup            
        //   294: iconst_2       
        //   295: ldc             Lcom/applovin/exoplayer2/b/h;.class
        //   297: aastore        
        //   298: invokevirtual   java/lang/Class.getConstructor:([Ljava/lang/Class;)Ljava/lang/reflect/Constructor;
        //   301: iconst_3       
        //   302: anewarray       Ljava/lang/Object;
        //   305: dup            
        //   306: iconst_0       
        //   307: aload           6
        //   309: aastore        
        //   310: dup            
        //   311: iconst_1       
        //   312: aload           7
        //   314: aastore        
        //   315: dup            
        //   316: iconst_2       
        //   317: aload           5
        //   319: aastore        
        //   320: invokevirtual   java/lang/reflect/Constructor.newInstance:([Ljava/lang/Object;)Ljava/lang/Object;
        //   323: checkcast       Lcom/applovin/exoplayer2/ar;
        //   326: invokevirtual   java/util/ArrayList.add:(ILjava/lang/Object;)V
        //   329: ldc             "DefaultRenderersFactory"
        //   331: ldc             "Loaded FfmpegAudioRenderer."
        //   333: invokestatic    com/applovin/exoplayer2/l/q.b:(Ljava/lang/String;Ljava/lang/String;)V
        //   336: goto            351
        //   339: astore_1       
        //   340: new             Ljava/lang/RuntimeException;
        //   343: dup            
        //   344: ldc             "Error instantiating FFmpeg extension"
        //   346: aload_1        
        //   347: invokespecial   java/lang/RuntimeException.<init>:(Ljava/lang/String;Ljava/lang/Throwable;)V
        //   350: athrow         
        //   351: return         
        //   352: astore_1       
        //   353: goto            171
        //   356: astore_1       
        //   357: goto            271
        //   360: astore_1       
        //   361: goto            351
        //    Signature:
        //  (Landroid/content/Context;ILcom/applovin/exoplayer2/f/k;ZLcom/applovin/exoplayer2/b/h;Landroid/os/Handler;Lcom/applovin/exoplayer2/b/g;Ljava/util/ArrayList<Lcom/applovin/exoplayer2/ar;>;)V
        //    Exceptions:
        //  Try           Handler
        //  Start  End    Start  End    Type                              
        //  -----  -----  -----  -----  ----------------------------------
        //  76     129    352    356    Ljava/lang/ClassNotFoundException;
        //  76     129    159    171    Ljava/lang/Exception;
        //  134    149    152    159    Ljava/lang/ClassNotFoundException;
        //  134    149    159    171    Ljava/lang/Exception;
        //  174    227    356    360    Ljava/lang/ClassNotFoundException;
        //  174    227    259    271    Ljava/lang/Exception;
        //  232    246    252    259    Ljava/lang/ClassNotFoundException;
        //  232    246    259    271    Ljava/lang/Exception;
        //  271    336    360    364    Ljava/lang/ClassNotFoundException;
        //  271    336    339    351    Ljava/lang/Exception;
        // 
        // The error that occurred was:
        // 
        // java.lang.IllegalStateException: Expression is linked from several locations: Label_0174:
        //     at com.strobel.decompiler.ast.Error.expressionLinkedFromMultipleLocations(Error.java:27)
        //     at com.strobel.decompiler.ast.AstOptimizer.mergeDisparateObjectInitializations(AstOptimizer.java:2604)
        //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:235)
        //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:42)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:206)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:93)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethodBody(AstBuilder.java:868)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethod(AstBuilder.java:761)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addTypeMembers(AstBuilder.java:638)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeCore(AstBuilder.java:605)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeNoCache(AstBuilder.java:195)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createType(AstBuilder.java:162)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addType(AstBuilder.java:137)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.buildAst(JavaLanguage.java:71)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.decompileType(JavaLanguage.java:59)
        //     at com.strobel.decompiler.DecompilerDriver.decompileType(DecompilerDriver.java:333)
        //     at com.strobel.decompiler.DecompilerDriver.decompileJar(DecompilerDriver.java:254)
        //     at com.strobel.decompiler.DecompilerDriver.main(DecompilerDriver.java:144)
        // 
        throw new IllegalStateException("An error occurred while decompiling this method.");
    }
    
    protected void a(final Context context, final int n, final ArrayList<ar> list) {
        list.add((ar)new b());
    }
    
    protected void a(final Context context, final Handler handler, final int n, final ArrayList<ar> list) {
    }
    
    protected void a(final Context context, final com.applovin.exoplayer2.g.e e, final Looper looper, final int n, final ArrayList<ar> list) {
        list.add(new com.applovin.exoplayer2.g.f(e, looper));
    }
    
    protected void a(final Context context, final l l, final Looper looper, final int n, final ArrayList<ar> list) {
        list.add((ar)new m(l, looper));
    }
    
    @Override
    public ar[] a(final Handler handler, final com.applovin.exoplayer2.m.n n, final g g, final l l, final com.applovin.exoplayer2.g.e e) {
        final ArrayList list = new ArrayList();
        this.a(this.a, this.b, this.e, this.d, handler, n, this.c, list);
        final h a = this.a(this.a, this.i, this.j, this.k);
        if (a != null) {
            this.a(this.a, this.b, this.e, this.d, a, handler, g, list);
        }
        this.a(this.a, l, handler.getLooper(), this.b, list);
        this.a(this.a, e, handler.getLooper(), this.b, list);
        this.a(this.a, this.b, list);
        this.a(this.a, handler, this.b, list);
        return list.toArray(new ar[0]);
    }
}

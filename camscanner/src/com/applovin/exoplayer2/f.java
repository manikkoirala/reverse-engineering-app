// 
// Decompiled by Procyon v0.6.0
// 

package com.applovin.exoplayer2;

import androidx.annotation.Nullable;
import android.os.RemoteException;
import com.applovin.exoplayer2.l.a;
import android.os.Parcel;
import android.os.IBinder;
import com.applovin.exoplayer2.l.ai;
import android.os.Bundle;
import com.applovin.exoplayer2.common.a.s;
import android.os.Binder;

public final class f extends Binder
{
    private static final int a;
    private final s<Bundle> b;
    
    static {
        int \u3007080;
        if (ai.a >= 30) {
            \u3007080 = oo\u3007.\u3007080();
        }
        else {
            \u3007080 = 65536;
        }
        a = \u3007080;
    }
    
    public static s<Bundle> a(final IBinder binder) {
        final s.a<Bundle> i = s.i();
        int j = 1;
        int n = 0;
        while (j != 0) {
            final Parcel obtain = Parcel.obtain();
            final Parcel obtain2 = Parcel.obtain();
            try {
                obtain.writeInt(n);
                try {
                    binder.transact(1, obtain, obtain2, 0);
                    while (true) {
                        j = obtain2.readInt();
                        if (j != 1) {
                            break;
                        }
                        i.b((Bundle)com.applovin.exoplayer2.l.a.b((Object)obtain2.readBundle()));
                        ++n;
                    }
                }
                catch (final RemoteException i) {
                    throw new RuntimeException((Throwable)i);
                }
            }
            finally {
                obtain2.recycle();
                obtain.recycle();
            }
            break;
        }
        return i.a();
    }
    
    protected boolean onTransact(int int1, final Parcel parcel, @Nullable final Parcel parcel2, int n) throws RemoteException {
        if (int1 != 1) {
            return super.onTransact(int1, parcel, parcel2, n);
        }
        n = 0;
        if (parcel2 == null) {
            return false;
        }
        int size;
        for (size = this.b.size(), int1 = parcel.readInt(); int1 < size && parcel2.dataSize() < f.a; ++int1) {
            parcel2.writeInt(1);
            parcel2.writeBundle((Bundle)this.b.get(int1));
        }
        if (int1 < size) {
            n = 2;
        }
        parcel2.writeInt(n);
        return true;
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.applovin.exoplayer2;

import androidx.annotation.Nullable;

public final class at
{
    public static final at a;
    public final boolean b;
    
    static {
        a = new at(false);
    }
    
    public at(final boolean b) {
        this.b = b;
    }
    
    @Override
    public boolean equals(@Nullable final Object o) {
        boolean b = true;
        if (this == o) {
            return true;
        }
        if (o != null && at.class == o.getClass()) {
            if (this.b != ((at)o).b) {
                b = false;
            }
            return b;
        }
        return false;
    }
    
    @Override
    public int hashCode() {
        return (this.b ^ true) ? 1 : 0;
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.applovin.array.apphub.aidl;

import android.os.Parcel;
import android.os.Binder;
import android.os.IBinder;
import android.os.RemoteException;
import android.os.Bundle;
import android.os.IInterface;

public interface IAppHubService extends IInterface
{
    void directInstall(final String p0, final Bundle p1, final IAppHubDirectDownloadServiceCallback p2) throws RemoteException;
    
    void dismissDirectDownloadAppDetails(final String p0) throws RemoteException;
    
    long getAppHubVersionCode() throws RemoteException;
    
    Bundle getEnabledFeatures() throws RemoteException;
    
    String getRandomUserToken() throws RemoteException;
    
    void showDirectDownloadAppDetails(final String p0, final IAppHubDirectDownloadServiceCallback p1) throws RemoteException;
    
    void showDirectDownloadAppDetailsWithExtra(final String p0, final Bundle p1, final IAppHubDirectDownloadServiceCallback p2) throws RemoteException;
    
    public static class Default implements IAppHubService
    {
        public IBinder asBinder() {
            return null;
        }
        
        @Override
        public void directInstall(final String s, final Bundle bundle, final IAppHubDirectDownloadServiceCallback appHubDirectDownloadServiceCallback) throws RemoteException {
        }
        
        @Override
        public void dismissDirectDownloadAppDetails(final String s) throws RemoteException {
        }
        
        @Override
        public long getAppHubVersionCode() throws RemoteException {
            return 0L;
        }
        
        @Override
        public Bundle getEnabledFeatures() throws RemoteException {
            return null;
        }
        
        @Override
        public String getRandomUserToken() throws RemoteException {
            return null;
        }
        
        @Override
        public void showDirectDownloadAppDetails(final String s, final IAppHubDirectDownloadServiceCallback appHubDirectDownloadServiceCallback) throws RemoteException {
        }
        
        @Override
        public void showDirectDownloadAppDetailsWithExtra(final String s, final Bundle bundle, final IAppHubDirectDownloadServiceCallback appHubDirectDownloadServiceCallback) throws RemoteException {
        }
    }
    
    public abstract static class Stub extends Binder implements IAppHubService
    {
        private static final String DESCRIPTOR = "com.applovin.array.apphub.aidl.IAppHubService";
        static final int TRANSACTION_directInstall = 7;
        static final int TRANSACTION_dismissDirectDownloadAppDetails = 5;
        static final int TRANSACTION_getAppHubVersionCode = 2;
        static final int TRANSACTION_getEnabledFeatures = 3;
        static final int TRANSACTION_getRandomUserToken = 1;
        static final int TRANSACTION_showDirectDownloadAppDetails = 4;
        static final int TRANSACTION_showDirectDownloadAppDetailsWithExtra = 6;
        
        public Stub() {
            this.attachInterface((IInterface)this, "com.applovin.array.apphub.aidl.IAppHubService");
        }
        
        public static IAppHubService asInterface(final IBinder binder) {
            if (binder == null) {
                return null;
            }
            final IInterface queryLocalInterface = binder.queryLocalInterface("com.applovin.array.apphub.aidl.IAppHubService");
            if (queryLocalInterface != null && queryLocalInterface instanceof IAppHubService) {
                return (IAppHubService)queryLocalInterface;
            }
            return new Proxy(binder);
        }
        
        public static IAppHubService getDefaultImpl() {
            return Proxy.sDefaultImpl;
        }
        
        public static boolean setDefaultImpl(final IAppHubService sDefaultImpl) {
            if (Proxy.sDefaultImpl != null) {
                throw new IllegalStateException("setDefaultImpl() called twice");
            }
            if (sDefaultImpl != null) {
                Proxy.sDefaultImpl = sDefaultImpl;
                return true;
            }
            return false;
        }
        
        public IBinder asBinder() {
            return (IBinder)this;
        }
        
        public boolean onTransact(final int n, final Parcel parcel, final Parcel parcel2, final int n2) throws RemoteException {
            if (n == 1598968902) {
                parcel2.writeString("com.applovin.array.apphub.aidl.IAppHubService");
                return true;
            }
            final Bundle bundle = null;
            final Bundle bundle2 = null;
            switch (n) {
                default: {
                    return super.onTransact(n, parcel, parcel2, n2);
                }
                case 7: {
                    parcel.enforceInterface("com.applovin.array.apphub.aidl.IAppHubService");
                    final String string = parcel.readString();
                    Bundle bundle3 = bundle2;
                    if (parcel.readInt() != 0) {
                        bundle3 = (Bundle)Bundle.CREATOR.createFromParcel(parcel);
                    }
                    this.directInstall(string, bundle3, IAppHubDirectDownloadServiceCallback.Stub.asInterface(parcel.readStrongBinder()));
                    return true;
                }
                case 6: {
                    parcel.enforceInterface("com.applovin.array.apphub.aidl.IAppHubService");
                    final String string2 = parcel.readString();
                    Bundle bundle4 = bundle;
                    if (parcel.readInt() != 0) {
                        bundle4 = (Bundle)Bundle.CREATOR.createFromParcel(parcel);
                    }
                    this.showDirectDownloadAppDetailsWithExtra(string2, bundle4, IAppHubDirectDownloadServiceCallback.Stub.asInterface(parcel.readStrongBinder()));
                    return true;
                }
                case 5: {
                    parcel.enforceInterface("com.applovin.array.apphub.aidl.IAppHubService");
                    this.dismissDirectDownloadAppDetails(parcel.readString());
                    parcel2.writeNoException();
                    return true;
                }
                case 4: {
                    parcel.enforceInterface("com.applovin.array.apphub.aidl.IAppHubService");
                    this.showDirectDownloadAppDetails(parcel.readString(), IAppHubDirectDownloadServiceCallback.Stub.asInterface(parcel.readStrongBinder()));
                    return true;
                }
                case 3: {
                    parcel.enforceInterface("com.applovin.array.apphub.aidl.IAppHubService");
                    final Bundle enabledFeatures = this.getEnabledFeatures();
                    parcel2.writeNoException();
                    if (enabledFeatures != null) {
                        parcel2.writeInt(1);
                        enabledFeatures.writeToParcel(parcel2, 1);
                    }
                    else {
                        parcel2.writeInt(0);
                    }
                    return true;
                }
                case 2: {
                    parcel.enforceInterface("com.applovin.array.apphub.aidl.IAppHubService");
                    final long appHubVersionCode = this.getAppHubVersionCode();
                    parcel2.writeNoException();
                    parcel2.writeLong(appHubVersionCode);
                    return true;
                }
                case 1: {
                    parcel.enforceInterface("com.applovin.array.apphub.aidl.IAppHubService");
                    final String randomUserToken = this.getRandomUserToken();
                    parcel2.writeNoException();
                    parcel2.writeString(randomUserToken);
                    return true;
                }
            }
        }
        
        private static class Proxy implements IAppHubService
        {
            public static IAppHubService sDefaultImpl;
            private IBinder mRemote;
            
            Proxy(final IBinder mRemote) {
                this.mRemote = mRemote;
            }
            
            public IBinder asBinder() {
                return this.mRemote;
            }
            
            @Override
            public void directInstall(final String s, final Bundle bundle, final IAppHubDirectDownloadServiceCallback appHubDirectDownloadServiceCallback) throws RemoteException {
                final Parcel obtain = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.applovin.array.apphub.aidl.IAppHubService");
                    obtain.writeString(s);
                    if (bundle != null) {
                        obtain.writeInt(1);
                        bundle.writeToParcel(obtain, 0);
                    }
                    else {
                        obtain.writeInt(0);
                    }
                    IBinder binder;
                    if (appHubDirectDownloadServiceCallback != null) {
                        binder = ((IInterface)appHubDirectDownloadServiceCallback).asBinder();
                    }
                    else {
                        binder = null;
                    }
                    obtain.writeStrongBinder(binder);
                    if (!this.mRemote.transact(7, obtain, (Parcel)null, 1) && Stub.getDefaultImpl() != null) {
                        Stub.getDefaultImpl().directInstall(s, bundle, appHubDirectDownloadServiceCallback);
                    }
                }
                finally {
                    obtain.recycle();
                }
            }
            
            @Override
            public void dismissDirectDownloadAppDetails(final String s) throws RemoteException {
                final Parcel obtain = Parcel.obtain();
                final Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.applovin.array.apphub.aidl.IAppHubService");
                    obtain.writeString(s);
                    if (!this.mRemote.transact(5, obtain, obtain2, 0) && Stub.getDefaultImpl() != null) {
                        Stub.getDefaultImpl().dismissDirectDownloadAppDetails(s);
                        return;
                    }
                    obtain2.readException();
                }
                finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }
            
            @Override
            public long getAppHubVersionCode() throws RemoteException {
                final Parcel obtain = Parcel.obtain();
                final Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.applovin.array.apphub.aidl.IAppHubService");
                    if (!this.mRemote.transact(2, obtain, obtain2, 0) && Stub.getDefaultImpl() != null) {
                        return Stub.getDefaultImpl().getAppHubVersionCode();
                    }
                    obtain2.readException();
                    return obtain2.readLong();
                }
                finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }
            
            @Override
            public Bundle getEnabledFeatures() throws RemoteException {
                final Parcel obtain = Parcel.obtain();
                final Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.applovin.array.apphub.aidl.IAppHubService");
                    if (!this.mRemote.transact(3, obtain, obtain2, 0) && Stub.getDefaultImpl() != null) {
                        return Stub.getDefaultImpl().getEnabledFeatures();
                    }
                    obtain2.readException();
                    Bundle bundle;
                    if (obtain2.readInt() != 0) {
                        bundle = (Bundle)Bundle.CREATOR.createFromParcel(obtain2);
                    }
                    else {
                        bundle = null;
                    }
                    return bundle;
                }
                finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }
            
            public String getInterfaceDescriptor() {
                return "com.applovin.array.apphub.aidl.IAppHubService";
            }
            
            @Override
            public String getRandomUserToken() throws RemoteException {
                final Parcel obtain = Parcel.obtain();
                final Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.applovin.array.apphub.aidl.IAppHubService");
                    if (!this.mRemote.transact(1, obtain, obtain2, 0) && Stub.getDefaultImpl() != null) {
                        return Stub.getDefaultImpl().getRandomUserToken();
                    }
                    obtain2.readException();
                    return obtain2.readString();
                }
                finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }
            
            @Override
            public void showDirectDownloadAppDetails(final String s, final IAppHubDirectDownloadServiceCallback appHubDirectDownloadServiceCallback) throws RemoteException {
                final Parcel obtain = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.applovin.array.apphub.aidl.IAppHubService");
                    obtain.writeString(s);
                    IBinder binder;
                    if (appHubDirectDownloadServiceCallback != null) {
                        binder = ((IInterface)appHubDirectDownloadServiceCallback).asBinder();
                    }
                    else {
                        binder = null;
                    }
                    obtain.writeStrongBinder(binder);
                    if (!this.mRemote.transact(4, obtain, (Parcel)null, 1) && Stub.getDefaultImpl() != null) {
                        Stub.getDefaultImpl().showDirectDownloadAppDetails(s, appHubDirectDownloadServiceCallback);
                    }
                }
                finally {
                    obtain.recycle();
                }
            }
            
            @Override
            public void showDirectDownloadAppDetailsWithExtra(final String s, final Bundle bundle, final IAppHubDirectDownloadServiceCallback appHubDirectDownloadServiceCallback) throws RemoteException {
                final Parcel obtain = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.applovin.array.apphub.aidl.IAppHubService");
                    obtain.writeString(s);
                    if (bundle != null) {
                        obtain.writeInt(1);
                        bundle.writeToParcel(obtain, 0);
                    }
                    else {
                        obtain.writeInt(0);
                    }
                    IBinder binder;
                    if (appHubDirectDownloadServiceCallback != null) {
                        binder = ((IInterface)appHubDirectDownloadServiceCallback).asBinder();
                    }
                    else {
                        binder = null;
                    }
                    obtain.writeStrongBinder(binder);
                    if (!this.mRemote.transact(6, obtain, (Parcel)null, 1) && Stub.getDefaultImpl() != null) {
                        Stub.getDefaultImpl().showDirectDownloadAppDetailsWithExtra(s, bundle, appHubDirectDownloadServiceCallback);
                    }
                }
                finally {
                    obtain.recycle();
                }
            }
        }
    }
}

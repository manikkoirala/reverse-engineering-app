// 
// Decompiled by Procyon v0.6.0
// 

package com.applovin.array.apphub.aidl;

import android.os.Parcel;
import android.os.Binder;
import android.os.IBinder;
import android.os.RemoteException;
import android.os.IInterface;

public interface IAppHubDirectDownloadServiceCallback extends IInterface
{
    void onAppDetailsDismissed(final String p0) throws RemoteException;
    
    void onAppDetailsShown(final String p0) throws RemoteException;
    
    void onDownloadStarted(final String p0) throws RemoteException;
    
    void onError(final String p0, final String p1) throws RemoteException;
    
    public static class Default implements IAppHubDirectDownloadServiceCallback
    {
        public IBinder asBinder() {
            return null;
        }
        
        @Override
        public void onAppDetailsDismissed(final String s) throws RemoteException {
        }
        
        @Override
        public void onAppDetailsShown(final String s) throws RemoteException {
        }
        
        @Override
        public void onDownloadStarted(final String s) throws RemoteException {
        }
        
        @Override
        public void onError(final String s, final String s2) throws RemoteException {
        }
    }
    
    public abstract static class Stub extends Binder implements IAppHubDirectDownloadServiceCallback
    {
        private static final String DESCRIPTOR = "com.applovin.array.apphub.aidl.IAppHubDirectDownloadServiceCallback";
        static final int TRANSACTION_onAppDetailsDismissed = 2;
        static final int TRANSACTION_onAppDetailsShown = 1;
        static final int TRANSACTION_onDownloadStarted = 3;
        static final int TRANSACTION_onError = 4;
        
        public Stub() {
            this.attachInterface((IInterface)this, "com.applovin.array.apphub.aidl.IAppHubDirectDownloadServiceCallback");
        }
        
        public static IAppHubDirectDownloadServiceCallback asInterface(final IBinder binder) {
            if (binder == null) {
                return null;
            }
            final IInterface queryLocalInterface = binder.queryLocalInterface("com.applovin.array.apphub.aidl.IAppHubDirectDownloadServiceCallback");
            if (queryLocalInterface != null && queryLocalInterface instanceof IAppHubDirectDownloadServiceCallback) {
                return (IAppHubDirectDownloadServiceCallback)queryLocalInterface;
            }
            return new Proxy(binder);
        }
        
        public static IAppHubDirectDownloadServiceCallback getDefaultImpl() {
            return Proxy.sDefaultImpl;
        }
        
        public static boolean setDefaultImpl(final IAppHubDirectDownloadServiceCallback sDefaultImpl) {
            if (Proxy.sDefaultImpl != null) {
                throw new IllegalStateException("setDefaultImpl() called twice");
            }
            if (sDefaultImpl != null) {
                Proxy.sDefaultImpl = sDefaultImpl;
                return true;
            }
            return false;
        }
        
        public IBinder asBinder() {
            return (IBinder)this;
        }
        
        public boolean onTransact(final int n, final Parcel parcel, final Parcel parcel2, final int n2) throws RemoteException {
            if (n == 1) {
                parcel.enforceInterface("com.applovin.array.apphub.aidl.IAppHubDirectDownloadServiceCallback");
                this.onAppDetailsShown(parcel.readString());
                return true;
            }
            if (n == 2) {
                parcel.enforceInterface("com.applovin.array.apphub.aidl.IAppHubDirectDownloadServiceCallback");
                this.onAppDetailsDismissed(parcel.readString());
                return true;
            }
            if (n == 3) {
                parcel.enforceInterface("com.applovin.array.apphub.aidl.IAppHubDirectDownloadServiceCallback");
                this.onDownloadStarted(parcel.readString());
                return true;
            }
            if (n == 4) {
                parcel.enforceInterface("com.applovin.array.apphub.aidl.IAppHubDirectDownloadServiceCallback");
                this.onError(parcel.readString(), parcel.readString());
                return true;
            }
            if (n != 1598968902) {
                return super.onTransact(n, parcel, parcel2, n2);
            }
            parcel2.writeString("com.applovin.array.apphub.aidl.IAppHubDirectDownloadServiceCallback");
            return true;
        }
        
        private static class Proxy implements IAppHubDirectDownloadServiceCallback
        {
            public static IAppHubDirectDownloadServiceCallback sDefaultImpl;
            private IBinder mRemote;
            
            Proxy(final IBinder mRemote) {
                this.mRemote = mRemote;
            }
            
            public IBinder asBinder() {
                return this.mRemote;
            }
            
            public String getInterfaceDescriptor() {
                return "com.applovin.array.apphub.aidl.IAppHubDirectDownloadServiceCallback";
            }
            
            @Override
            public void onAppDetailsDismissed(final String s) throws RemoteException {
                final Parcel obtain = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.applovin.array.apphub.aidl.IAppHubDirectDownloadServiceCallback");
                    obtain.writeString(s);
                    if (!this.mRemote.transact(2, obtain, (Parcel)null, 1) && Stub.getDefaultImpl() != null) {
                        Stub.getDefaultImpl().onAppDetailsDismissed(s);
                    }
                }
                finally {
                    obtain.recycle();
                }
            }
            
            @Override
            public void onAppDetailsShown(final String s) throws RemoteException {
                final Parcel obtain = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.applovin.array.apphub.aidl.IAppHubDirectDownloadServiceCallback");
                    obtain.writeString(s);
                    if (!this.mRemote.transact(1, obtain, (Parcel)null, 1) && Stub.getDefaultImpl() != null) {
                        Stub.getDefaultImpl().onAppDetailsShown(s);
                    }
                }
                finally {
                    obtain.recycle();
                }
            }
            
            @Override
            public void onDownloadStarted(final String s) throws RemoteException {
                final Parcel obtain = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.applovin.array.apphub.aidl.IAppHubDirectDownloadServiceCallback");
                    obtain.writeString(s);
                    if (!this.mRemote.transact(3, obtain, (Parcel)null, 1) && Stub.getDefaultImpl() != null) {
                        Stub.getDefaultImpl().onDownloadStarted(s);
                    }
                }
                finally {
                    obtain.recycle();
                }
            }
            
            @Override
            public void onError(final String s, final String s2) throws RemoteException {
                final Parcel obtain = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.applovin.array.apphub.aidl.IAppHubDirectDownloadServiceCallback");
                    obtain.writeString(s);
                    obtain.writeString(s2);
                    if (!this.mRemote.transact(4, obtain, (Parcel)null, 1) && Stub.getDefaultImpl() != null) {
                        Stub.getDefaultImpl().onError(s, s2);
                    }
                }
                finally {
                    obtain.recycle();
                }
            }
        }
    }
}

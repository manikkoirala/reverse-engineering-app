// 
// Decompiled by Procyon v0.6.0
// 

package com.applovin.communicator;

import java.util.Iterator;
import java.util.List;
import java.util.Collections;
import android.content.Context;
import com.applovin.impl.communicator.MessagingServiceImpl;
import com.applovin.impl.communicator.a;
import com.applovin.impl.sdk.y;
import com.applovin.impl.sdk.o;

public final class AppLovinCommunicator
{
    private static AppLovinCommunicator a;
    private static final Object b;
    private o c;
    private y d;
    private final a e;
    private final MessagingServiceImpl f;
    
    static {
        b = new Object();
    }
    
    public AppLovinCommunicator() {
        this.e = new a();
        this.f = new MessagingServiceImpl();
    }
    
    private void a(final String s) {
        if (this.d != null && y.a()) {
            this.d.b("AppLovinCommunicator", s);
        }
    }
    
    public static AppLovinCommunicator getInstance(final Context context) {
        synchronized (AppLovinCommunicator.b) {
            if (AppLovinCommunicator.a == null) {
                AppLovinCommunicator.a = new AppLovinCommunicator();
            }
            return AppLovinCommunicator.a;
        }
    }
    
    public void a(final o o) {
        this.c = o;
        this.d = o.F();
        final StringBuilder sb = new StringBuilder();
        sb.append("Attached SDK instance: ");
        sb.append(o);
        sb.append("...");
        this.a(sb.toString());
    }
    
    public AppLovinCommunicatorMessagingService getMessagingService() {
        return (AppLovinCommunicatorMessagingService)this.f;
    }
    
    public boolean hasSubscriber(final String s) {
        return this.e.a(s);
    }
    
    public boolean respondsToTopic(final String s) {
        return this.c.X().c(s);
    }
    
    public void subscribe(final AppLovinCommunicatorSubscriber appLovinCommunicatorSubscriber, final String o) {
        this.subscribe(appLovinCommunicatorSubscriber, Collections.singletonList(o));
    }
    
    public void subscribe(final AppLovinCommunicatorSubscriber obj, final List<String> list) {
        for (final String str : list) {
            if (!this.e.a(obj, str)) {
                final StringBuilder sb = new StringBuilder();
                sb.append("Unable to subscribe ");
                sb.append(obj);
                sb.append(" to topic: ");
                sb.append(str);
                this.a(sb.toString());
            }
        }
    }
    
    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append("AppLovinCommunicator{sdk=");
        sb.append(this.c);
        sb.append('}');
        return sb.toString();
    }
    
    public void unsubscribe(final AppLovinCommunicatorSubscriber appLovinCommunicatorSubscriber, final String o) {
        this.unsubscribe(appLovinCommunicatorSubscriber, Collections.singletonList(o));
    }
    
    public void unsubscribe(final AppLovinCommunicatorSubscriber obj, final List<String> list) {
        for (final String str : list) {
            final StringBuilder sb = new StringBuilder();
            sb.append("Unsubscribing ");
            sb.append(obj);
            sb.append(" from topic: ");
            sb.append(str);
            this.a(sb.toString());
            this.e.b(obj, str);
        }
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.applovin.communicator;

public interface AppLovinCommunicatorSubscriber extends AppLovinCommunicatorEntity
{
    void onMessageReceived(final AppLovinCommunicatorMessage p0);
}

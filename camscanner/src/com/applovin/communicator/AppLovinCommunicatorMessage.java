// 
// Decompiled by Procyon v0.6.0
// 

package com.applovin.communicator;

import android.content.Intent;
import android.os.Bundle;
import com.applovin.impl.communicator.CommunicatorMessageImpl;

public class AppLovinCommunicatorMessage extends CommunicatorMessageImpl
{
    public AppLovinCommunicatorMessage(final Bundle bundle, final String s, final AppLovinCommunicatorPublisher appLovinCommunicatorPublisher) {
        super(bundle, s, appLovinCommunicatorPublisher);
    }
    
    public Bundle getMessageData() {
        return super.data;
    }
    
    public String getPublisherId() {
        final AppLovinCommunicatorPublisher appLovinCommunicatorPublisher = (AppLovinCommunicatorPublisher)super.publisherRef.get();
        String communicatorId;
        if (appLovinCommunicatorPublisher != null) {
            communicatorId = appLovinCommunicatorPublisher.getCommunicatorId();
        }
        else {
            communicatorId = "";
        }
        return communicatorId;
    }
    
    public String getTopic() {
        return ((Intent)this).getAction();
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.alibaba.android.vlayout;

import java.util.Collection;
import java.util.LinkedList;
import java.util.Collections;
import java.util.Iterator;
import android.view.ViewGroup;
import java.util.ArrayList;
import androidx.annotation.Nullable;
import java.util.concurrent.atomic.AtomicInteger;
import android.util.SparseArray;
import androidx.annotation.NonNull;
import android.util.Pair;
import java.util.List;
import androidx.recyclerview.widget.RecyclerView;

public class DelegateAdapter extends VirtualLayoutAdapter<ViewHolder>
{
    @NonNull
    private final List<Pair<AdapterDataObserver, Adapter>> O8o08O8O;
    private int OO;
    private long[] oOo\u30078o008;
    private SparseArray<Adapter> o\u300700O;
    private int \u3007080OO8\u30070;
    private final boolean \u300708O\u300700\u3007o;
    private final SparseArray<Pair<AdapterDataObserver, Adapter>> \u30070O;
    @Nullable
    private AtomicInteger \u3007OOo8\u30070;
    
    public DelegateAdapter(final VirtualLayoutManager virtualLayoutManager, final boolean b) {
        this(virtualLayoutManager, b, false);
    }
    
    DelegateAdapter(final VirtualLayoutManager virtualLayoutManager, final boolean \u300708O\u300700\u3007o, final boolean b) {
        super(virtualLayoutManager);
        this.OO = 0;
        this.o\u300700O = (SparseArray<Adapter>)new SparseArray();
        this.O8o08O8O = new ArrayList<Pair<AdapterDataObserver, Adapter>>();
        this.\u3007080OO8\u30070 = 0;
        this.\u30070O = (SparseArray<Pair<AdapterDataObserver, Adapter>>)new SparseArray();
        this.oOo\u30078o008 = new long[2];
        if (b) {
            this.\u3007OOo8\u30070 = new AtomicInteger(0);
        }
        this.\u300708O\u300700\u3007o = \u300708O\u300700\u3007o;
    }
    
    public int O8ooOoo\u3007(int index) {
        final Pair pair = (Pair)this.\u30070O.get(index);
        if (pair == null) {
            index = -1;
        }
        else {
            index = this.O8o08O8O.indexOf(pair);
        }
        return index;
    }
    
    @Nullable
    public Pair<AdapterDataObserver, Adapter> O\u30078O8\u3007008(final int n) {
        final int size = this.O8o08O8O.size();
        final Pair<AdapterDataObserver, Adapter> pair = null;
        if (size == 0) {
            return null;
        }
        int n2 = size - 1;
        int n3 = 0;
        Pair pair2;
        while (true) {
            pair2 = pair;
            if (n3 > n2) {
                break;
            }
            final int n4 = (n3 + n2) / 2;
            pair2 = this.O8o08O8O.get(n4);
            final int n5 = ((AdapterDataObserver)pair2.first).\u3007080 + ((RecyclerView.Adapter)pair2.second).getItemCount() - 1;
            final Object first = pair2.first;
            if (((AdapterDataObserver)first).\u3007080 > n) {
                n2 = n4 - 1;
            }
            else if (n5 < n) {
                n3 = n4 + 1;
            }
            else {
                if (((AdapterDataObserver)first).\u3007080 <= n && n5 >= n) {
                    break;
                }
                continue;
            }
        }
        return (Pair<AdapterDataObserver, Adapter>)pair2;
    }
    
    @Override
    public int getItemCount() {
        return this.\u3007080OO8\u30070;
    }
    
    @Override
    public long getItemId(final int n) {
        final Pair<AdapterDataObserver, Adapter> o\u30078O8\u3007008 = this.O\u30078O8\u3007008(n);
        if (o\u30078O8\u3007008 == null) {
            return -1L;
        }
        final long itemId = ((RecyclerView.Adapter)o\u30078O8\u3007008.second).getItemId(n - ((AdapterDataObserver)o\u30078O8\u3007008.first).\u3007080);
        if (itemId < 0L) {
            return -1L;
        }
        return Cantor.\u3007080(((AdapterDataObserver)o\u30078O8\u3007008.first).\u3007o00\u3007\u3007Oo, itemId);
    }
    
    @Override
    public int getItemViewType(int \u3007o00\u3007\u3007Oo) {
        final Pair<AdapterDataObserver, Adapter> o\u30078O8\u3007008 = this.O\u30078O8\u3007008(\u3007o00\u3007\u3007Oo);
        if (o\u30078O8\u3007008 == null) {
            return -1;
        }
        final int itemViewType = ((RecyclerView.Adapter)o\u30078O8\u3007008.second).getItemViewType(\u3007o00\u3007\u3007Oo - ((AdapterDataObserver)o\u30078O8\u3007008.first).\u3007080);
        if (itemViewType < 0) {
            return itemViewType;
        }
        if (this.\u300708O\u300700\u3007o) {
            this.o\u300700O.put(itemViewType, o\u30078O8\u3007008.second);
            return itemViewType;
        }
        \u3007o00\u3007\u3007Oo = ((AdapterDataObserver)o\u30078O8\u3007008.first).\u3007o00\u3007\u3007Oo;
        return (int)Cantor.\u3007080(itemViewType, \u3007o00\u3007\u3007Oo);
    }
    
    @Override
    public void onBindViewHolder(final ViewHolder viewHolder, final int n) {
        final Pair<AdapterDataObserver, Adapter> o\u30078O8\u3007008 = this.O\u30078O8\u3007008(n);
        if (o\u30078O8\u3007008 == null) {
            return;
        }
        ((RecyclerView.Adapter)o\u30078O8\u3007008.second).onBindViewHolder(viewHolder, n - ((AdapterDataObserver)o\u30078O8\u3007008.first).\u3007080);
        ((Adapter)o\u30078O8\u3007008.second).\u3007O\u3007(viewHolder, n - ((AdapterDataObserver)o\u30078O8\u3007008.first).\u3007080, n);
    }
    
    @Override
    public void onBindViewHolder(final ViewHolder viewHolder, final int n, final List<Object> list) {
        final Pair<AdapterDataObserver, Adapter> o\u30078O8\u3007008 = this.O\u30078O8\u3007008(n);
        if (o\u30078O8\u3007008 == null) {
            return;
        }
        ((RecyclerView.Adapter)o\u30078O8\u3007008.second).onBindViewHolder(viewHolder, n - ((AdapterDataObserver)o\u30078O8\u3007008.first).\u3007080, list);
        ((Adapter)o\u30078O8\u3007008.second).\u3007O00(viewHolder, n - ((AdapterDataObserver)o\u30078O8\u3007008.first).\u3007080, n, list);
    }
    
    @Override
    public ViewHolder onCreateViewHolder(final ViewGroup viewGroup, int n) {
        if (this.\u300708O\u300700\u3007o) {
            final Adapter adapter = (Adapter)this.o\u300700O.get(n);
            if (adapter != null) {
                return adapter.onCreateViewHolder(viewGroup, n);
            }
            return null;
        }
        else {
            Cantor.\u3007o00\u3007\u3007Oo(n, this.oOo\u30078o008);
            final long[] oOo\u30078o008 = this.oOo\u30078o008;
            n = (int)oOo\u30078o008[1];
            final int n2 = (int)oOo\u30078o008[0];
            final Adapter \u300700 = this.\u300700(n);
            if (\u300700 == null) {
                return null;
            }
            return \u300700.onCreateViewHolder(viewGroup, n2);
        }
    }
    
    @Override
    public void onViewAttachedToWindow(final ViewHolder viewHolder) {
        super.onViewAttachedToWindow(viewHolder);
        final int position = viewHolder.getPosition();
        if (position >= 0) {
            final Pair<AdapterDataObserver, Adapter> o\u30078O8\u3007008 = this.O\u30078O8\u3007008(position);
            if (o\u30078O8\u3007008 != null) {
                ((RecyclerView.Adapter)o\u30078O8\u3007008.second).onViewAttachedToWindow(viewHolder);
            }
        }
    }
    
    @Override
    public void onViewDetachedFromWindow(final ViewHolder viewHolder) {
        super.onViewDetachedFromWindow(viewHolder);
        final int position = viewHolder.getPosition();
        if (position >= 0) {
            final Pair<AdapterDataObserver, Adapter> o\u30078O8\u3007008 = this.O\u30078O8\u3007008(position);
            if (o\u30078O8\u3007008 != null) {
                ((RecyclerView.Adapter)o\u30078O8\u3007008.second).onViewDetachedFromWindow(viewHolder);
            }
        }
    }
    
    @Override
    public void onViewRecycled(final ViewHolder viewHolder) {
        super.onViewRecycled(viewHolder);
        final int position = viewHolder.getPosition();
        if (position >= 0) {
            final Pair<AdapterDataObserver, Adapter> o\u30078O8\u3007008 = this.O\u30078O8\u3007008(position);
            if (o\u30078O8\u3007008 != null) {
                ((RecyclerView.Adapter)o\u30078O8\u3007008.second).onViewRecycled(viewHolder);
            }
        }
    }
    
    public void oo88o8O(int size, @Nullable final List<Adapter> list) {
        if (list != null) {
            if (list.size() != 0) {
                int n;
                if ((n = size) < 0) {
                    n = 0;
                }
                if ((size = n) > this.O8o08O8O.size()) {
                    size = this.O8o08O8O.size();
                }
                final ArrayList list2 = new ArrayList();
                final Iterator<Pair<AdapterDataObserver, Adapter>> iterator = this.O8o08O8O.iterator();
                while (iterator.hasNext()) {
                    list2.add(iterator.next().second);
                }
                final Iterator iterator2 = list.iterator();
                while (iterator2.hasNext()) {
                    list2.add(size, iterator2.next());
                    ++size;
                }
                this.o\u3007\u30070\u3007(list2);
            }
        }
    }
    
    public void o\u3007O8\u3007\u3007o() {
        this.\u3007080OO8\u30070 = 0;
        this.OO = 0;
        final AtomicInteger \u3007oOo8\u30070 = this.\u3007OOo8\u30070;
        if (\u3007oOo8\u30070 != null) {
            \u3007oOo8\u30070.set(0);
        }
        super.o0.o\u30078(null);
        for (final Pair pair : this.O8o08O8O) {
            ((RecyclerView.Adapter)pair.second).unregisterAdapterDataObserver((RecyclerView.AdapterDataObserver)pair.first);
        }
        this.o\u300700O.clear();
        this.O8o08O8O.clear();
        this.\u30070O.clear();
    }
    
    public void o\u3007\u30070\u3007(@Nullable final List<Adapter> list) {
        this.o\u3007O8\u3007\u3007o();
        List<Adapter> emptyList = list;
        if (list == null) {
            emptyList = Collections.emptyList();
        }
        final LinkedList list2 = new LinkedList();
        this.\u3007080OO8\u30070 = 0;
        final Iterator<Adapter> iterator = emptyList.iterator();
        boolean hasStableIds = true;
        while (iterator.hasNext()) {
            final Adapter adapter = iterator.next();
            final int \u3007080OO8\u30070 = this.\u3007080OO8\u30070;
            final AtomicInteger \u3007oOo8\u30070 = this.\u3007OOo8\u30070;
            int incrementAndGet;
            if (\u3007oOo8\u30070 == null) {
                incrementAndGet = this.OO++;
            }
            else {
                incrementAndGet = \u3007oOo8\u30070.incrementAndGet();
            }
            final AdapterDataObserver adapterDataObserver = new AdapterDataObserver(\u3007080OO8\u30070, incrementAndGet);
            ((RecyclerView.Adapter)adapter).registerAdapterDataObserver(adapterDataObserver);
            hasStableIds = (hasStableIds && ((RecyclerView.Adapter)adapter).hasStableIds());
            final LayoutHelper \u3007\u30078O0\u30078 = adapter.\u3007\u30078O0\u30078();
            \u3007\u30078O0\u30078.\u3007\u30078O0\u30078(((RecyclerView.Adapter)adapter).getItemCount());
            this.\u3007080OO8\u30070 += \u3007\u30078O0\u30078.\u3007\u3007888();
            list2.add(\u3007\u30078O0\u30078);
            final Pair create = Pair.create((Object)adapterDataObserver, (Object)adapter);
            this.\u30070O.put(adapterDataObserver.\u3007o00\u3007\u3007Oo, (Object)create);
            this.O8o08O8O.add((Pair<AdapterDataObserver, Adapter>)create);
        }
        if (!((RecyclerView.Adapter)this).hasObservers()) {
            super.setHasStableIds(hasStableIds);
        }
        super.\u3007O00(list2);
    }
    
    @Override
    public void setHasStableIds(final boolean b) {
    }
    
    public Adapter \u300700(final int n) {
        return (Adapter)((Pair)this.\u30070O.get(n)).second;
    }
    
    public void \u30070000OOO(@Nullable final List<Adapter> list) {
        if (list != null) {
            if (!list.isEmpty()) {
                final LinkedList list2 = new LinkedList((Collection<? extends E>)super.\u3007O\u3007());
                for (int size = list.size(), i = 0; i < size; ++i) {
                    final Adapter obj = list.get(i);
                    final Iterator<Pair<AdapterDataObserver, Adapter>> iterator = this.O8o08O8O.iterator();
                    while (iterator.hasNext()) {
                        final Pair pair = iterator.next();
                        final Adapter adapter = (Adapter)pair.second;
                        if (adapter.equals(obj)) {
                            ((RecyclerView.Adapter)adapter).unregisterAdapterDataObserver((RecyclerView.AdapterDataObserver)pair.first);
                            final int o8ooOoo\u3007 = this.O8ooOoo\u3007(((AdapterDataObserver)pair.first).\u3007o00\u3007\u3007Oo);
                            if (o8ooOoo\u3007 >= 0 && o8ooOoo\u3007 < list2.size()) {
                                list2.remove(o8ooOoo\u3007);
                            }
                            iterator.remove();
                            break;
                        }
                    }
                }
                final ArrayList list3 = new ArrayList();
                final Iterator<Pair<AdapterDataObserver, Adapter>> iterator2 = this.O8o08O8O.iterator();
                while (iterator2.hasNext()) {
                    list3.add(iterator2.next().second);
                }
                this.o\u3007\u30070\u3007(list3);
            }
        }
    }
    
    public void \u3007O888o0o(@Nullable final Adapter o) {
        this.\u3007oo\u3007((List<Adapter>)Collections.singletonList(o));
    }
    
    public void \u3007oOO8O8(@Nullable final Adapter o) {
        if (o == null) {
            return;
        }
        this.\u30070000OOO((List<Adapter>)Collections.singletonList(o));
    }
    
    public void \u3007oo\u3007(@Nullable final List<Adapter> list) {
        this.oo88o8O(this.O8o08O8O.size(), list);
    }
    
    public abstract static class Adapter<VH extends ViewHolder> extends RecyclerView.Adapter<VH>
    {
        protected void \u3007O00(final VH vh, final int n, final int n2, final List<Object> list) {
            this.\u3007O\u3007(vh, n, n2);
        }
        
        protected void \u3007O\u3007(final VH vh, final int n, final int n2) {
        }
        
        public abstract LayoutHelper \u3007\u30078O0\u30078();
    }
    
    protected class AdapterDataObserver extends RecyclerView.AdapterDataObserver
    {
        int \u3007080;
        int \u3007o00\u3007\u3007Oo;
        final DelegateAdapter \u3007o\u3007;
        
        public AdapterDataObserver(final DelegateAdapter \u3007o\u3007, final int \u3007080, final int \u3007o00\u3007\u3007Oo) {
            this.\u3007o\u3007 = \u3007o\u3007;
            this.\u3007080 = \u3007080;
            this.\u3007o00\u3007\u3007Oo = \u3007o00\u3007\u3007Oo;
        }
        
        private boolean \u3007080() {
            final int \u3007o00\u3007\u3007Oo = this.\u3007o00\u3007\u3007Oo;
            if (\u3007o00\u3007\u3007Oo < 0) {
                return false;
            }
            int i = this.\u3007o\u3007.O8ooOoo\u3007(\u3007o00\u3007\u3007Oo);
            if (i < 0) {
                return false;
            }
            final Pair pair = this.\u3007o\u3007.O8o08O8O.get(i);
            final LinkedList list = new LinkedList((Collection<? extends E>)this.\u3007o\u3007.\u3007O\u3007());
            final LayoutHelper layoutHelper = (LayoutHelper)list.get(i);
            if (layoutHelper.\u3007\u3007888() != ((RecyclerView.Adapter)pair.second).getItemCount()) {
                layoutHelper.\u3007\u30078O0\u30078(((RecyclerView.Adapter)pair.second).getItemCount());
                this.\u3007o\u3007.\u3007080OO8\u30070 = this.\u3007080 + ((RecyclerView.Adapter)pair.second).getItemCount();
                ++i;
                while (i < this.\u3007o\u3007.O8o08O8O.size()) {
                    final Pair pair2 = this.\u3007o\u3007.O8o08O8O.get(i);
                    ((AdapterDataObserver)pair2.first).\u3007080 = this.\u3007o\u3007.\u3007080OO8\u30070;
                    final DelegateAdapter \u3007o\u3007 = this.\u3007o\u3007;
                    \u3007o\u3007.\u3007080OO8\u30070 += ((RecyclerView.Adapter)pair2.second).getItemCount();
                    ++i;
                }
                this.\u3007o\u3007.\u3007O00(list);
            }
            return true;
        }
        
        @Override
        public void onChanged() {
            if (!this.\u3007080()) {
                return;
            }
            ((RecyclerView.Adapter)this.\u3007o\u3007).notifyDataSetChanged();
        }
        
        @Override
        public void onItemRangeChanged(final int n, final int n2) {
            if (!this.\u3007080()) {
                return;
            }
            ((RecyclerView.Adapter)this.\u3007o\u3007).notifyItemRangeChanged(this.\u3007080 + n, n2);
        }
        
        @Override
        public void onItemRangeChanged(final int n, final int n2, final Object o) {
            if (!this.\u3007080()) {
                return;
            }
            ((RecyclerView.Adapter)this.\u3007o\u3007).notifyItemRangeChanged(this.\u3007080 + n, n2, o);
        }
        
        @Override
        public void onItemRangeInserted(final int n, final int n2) {
            if (!this.\u3007080()) {
                return;
            }
            ((RecyclerView.Adapter)this.\u3007o\u3007).notifyItemRangeInserted(this.\u3007080 + n, n2);
        }
        
        @Override
        public void onItemRangeMoved(final int n, final int n2, int \u3007080) {
            if (!this.\u3007080()) {
                return;
            }
            final DelegateAdapter \u3007o\u3007 = this.\u3007o\u3007;
            \u3007080 = this.\u3007080;
            ((RecyclerView.Adapter)\u3007o\u3007).notifyItemMoved(n + \u3007080, \u3007080 + n2);
        }
        
        @Override
        public void onItemRangeRemoved(final int n, final int n2) {
            if (!this.\u3007080()) {
                return;
            }
            ((RecyclerView.Adapter)this.\u3007o\u3007).notifyItemRangeRemoved(this.\u3007080 + n, n2);
        }
    }
}

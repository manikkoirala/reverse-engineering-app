// 
// Decompiled by Procyon v0.6.0
// 

package com.alibaba.android.vlayout.layout;

import com.alibaba.android.vlayout.OrientationHelperEx;
import android.view.View;
import android.view.View$MeasureSpec;
import com.alibaba.android.vlayout.VirtualLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import com.alibaba.android.vlayout.LayoutManagerHelper;

public class LinearLayoutHelper extends BaseLayoutHelper
{
    private int OoO8;
    private boolean o800o8O;
    
    public LinearLayoutHelper() {
        this(0);
    }
    
    public LinearLayoutHelper(final int n) {
        this(n, 0);
    }
    
    public LinearLayoutHelper(final int n, final int n2) {
        this.OoO8 = 0;
        this.o800o8O = false;
        this.\u3007\u30078O0\u30078(n2);
        this.Oo8Oo00oo(n);
    }
    
    @Override
    public int Oo08(int n, final boolean b, final boolean b2, final LayoutManagerHelper layoutManagerHelper) {
        final boolean b3 = layoutManagerHelper.getOrientation() == 1;
        if (b) {
            if (n == this.\u3007\u3007888() - 1) {
                int n2;
                if (b3) {
                    n2 = super.OO0o\u3007\u3007;
                    n = super.\u300780\u3007808\u3007O;
                }
                else {
                    n2 = super.\u30078o8o\u3007;
                    n = super.\u3007\u3007888;
                }
                return n2 + n;
            }
        }
        else if (n == 0) {
            int n3;
            if (b3) {
                n3 = -super.\u3007O8o08O;
                n = super.oO80;
            }
            else {
                n3 = -super.OO0o\u3007\u3007\u3007\u30070;
                n = super.o\u30070;
            }
            return n3 - n;
        }
        return super.Oo08(n, b, b2, layoutManagerHelper);
    }
    
    public void Oo8Oo00oo(final int n) {
        int ooO8 = n;
        if (n < 0) {
            ooO8 = 0;
        }
        this.OoO8 = ooO8;
    }
    
    @Override
    public void \u3007o(final RecyclerView.Recycler recycler, final RecyclerView.State state, final VirtualLayoutManager.LayoutStateWrapper layoutStateWrapper, final LayoutChunkResult layoutChunkResult, final LayoutManagerHelper layoutManagerHelper) {
        if (this.OO0o\u3007\u3007\u3007\u30070(layoutStateWrapper.\u3007o\u3007())) {
            return;
        }
        final int \u3007o\u3007 = layoutStateWrapper.\u3007o\u3007();
        final View o0ooO = this.o0ooO(recycler, layoutStateWrapper, layoutManagerHelper, layoutChunkResult);
        if (o0ooO == null) {
            return;
        }
        final boolean enableMarginOverLap = layoutManagerHelper.isEnableMarginOverLap();
        final VirtualLayoutManager.LayoutParams layoutParams = (VirtualLayoutManager.LayoutParams)o0ooO.getLayoutParams();
        final boolean b = layoutManagerHelper.getOrientation() == 1;
        final boolean b2 = layoutStateWrapper.o\u30070() == 1;
        final boolean b3 = b2 ? (\u3007o\u3007 == this.oO80().O8()) : (\u3007o\u3007 == this.oO80().Oo08());
        final boolean b4 = b2 ? (\u3007o\u3007 == this.oO80().Oo08()) : (\u3007o\u3007 == this.oO80().O8());
        int o\u3007\u30070\u3007;
        if (b3) {
            o\u3007\u30070\u3007 = this.o\u3007\u30070\u3007(layoutManagerHelper, b, b2, enableMarginOverLap);
        }
        else {
            o\u3007\u30070\u3007 = 0;
        }
        int \u30070000OOO;
        if (b4) {
            \u30070000OOO = this.\u30070000OOO(layoutManagerHelper, b, b2, enableMarginOverLap);
        }
        else {
            \u30070000OOO = 0;
        }
        int n = 0;
        Label_0431: {
            if (!b3) {
                if (!enableMarginOverLap) {
                    if (!this.o800o8O) {
                        n = this.OoO8;
                        break Label_0431;
                    }
                }
                else if (b2) {
                    final int topMargin = layoutParams.topMargin;
                    final View viewByPosition = layoutManagerHelper.findViewByPosition(\u3007o\u3007 - 1);
                    int bottomMargin;
                    if (viewByPosition != null) {
                        bottomMargin = ((VirtualLayoutManager.LayoutParams)viewByPosition.getLayoutParams()).bottomMargin;
                    }
                    else {
                        bottomMargin = 0;
                    }
                    if (bottomMargin >= 0 && topMargin >= 0) {
                        n = Math.max(bottomMargin, topMargin);
                        break Label_0431;
                    }
                    n = bottomMargin + topMargin;
                    break Label_0431;
                }
                else {
                    final int bottomMargin2 = layoutParams.bottomMargin;
                    final View viewByPosition2 = layoutManagerHelper.findViewByPosition(\u3007o\u3007 + 1);
                    int topMargin2;
                    if (viewByPosition2 != null) {
                        topMargin2 = ((VirtualLayoutManager.LayoutParams)viewByPosition2.getLayoutParams()).topMargin;
                    }
                    else {
                        topMargin2 = 0;
                    }
                    if (bottomMargin2 >= 0 && topMargin2 >= 0) {
                        n = Math.max(bottomMargin2, topMargin2);
                        break Label_0431;
                    }
                    n = topMargin2 + bottomMargin2;
                    break Label_0431;
                }
            }
            n = 0;
        }
        final int n2 = layoutManagerHelper.\u300780\u3007808\u3007O() - layoutManagerHelper.getPaddingLeft() - layoutManagerHelper.getPaddingRight() - this.OoO8() - this.o800o8O();
        final int \u300700 = layoutManagerHelper.\u300700(n2, layoutParams.width, b ^ true);
        final float \u3007oOo8\u30070 = layoutParams.\u3007OOo8\u30070;
        int n3 = 0;
        Label_0619: {
            if (!Float.isNaN(\u3007oOo8\u30070) && \u3007oOo8\u30070 > 0.0f) {
                n3 = View$MeasureSpec.makeMeasureSpec((int)(n2 / \u3007oOo8\u30070 + 0.5f), 1073741824);
            }
            else {
                if (!Float.isNaN(super.\u3007O00)) {
                    final float \u3007o00 = super.\u3007O00;
                    if (\u3007o00 > 0.0f) {
                        n3 = View$MeasureSpec.makeMeasureSpec((int)(n2 / \u3007o00 + 0.5), 1073741824);
                        break Label_0619;
                    }
                }
                n3 = layoutManagerHelper.\u300700(layoutManagerHelper.\u3007\u3007808\u3007() - layoutManagerHelper.getPaddingTop() - layoutManagerHelper.getPaddingBottom() - this.\u300700() - this.O\u30078O8\u3007008(), layoutParams.height, b);
            }
        }
        if (!enableMarginOverLap) {
            layoutManagerHelper.measureChildWithMargins(o0ooO, \u300700, n3);
        }
        else {
            layoutManagerHelper.measureChild(o0ooO, \u300700, n3);
        }
        final OrientationHelperEx oooo8o0\u3007 = layoutManagerHelper.Oooo8o0\u3007();
        layoutChunkResult.\u3007080 = oooo8o0\u3007.Oo08(o0ooO) + o\u3007\u30070\u3007 + \u30070000OOO + n;
        int n8;
        int n9;
        int n10;
        int n11;
        if (layoutManagerHelper.getOrientation() == 1) {
            int n4;
            int n5;
            if (layoutManagerHelper.\u3007\u30078O0\u30078()) {
                n4 = layoutManagerHelper.\u300780\u3007808\u3007O() - layoutManagerHelper.getPaddingRight() - super.\u30078o8o\u3007 - super.\u3007\u3007888;
                n5 = n4 - oooo8o0\u3007.o\u30070(o0ooO);
            }
            else {
                n5 = super.o\u30070 + (layoutManagerHelper.getPaddingLeft() + super.OO0o\u3007\u3007\u3007\u30070);
                n4 = oooo8o0\u3007.o\u30070(o0ooO) + n5;
            }
            if (layoutStateWrapper.o\u30070() == -1) {
                final int \u3007\u3007888 = layoutStateWrapper.\u3007\u3007888();
                if (b3) {
                    n = 0;
                }
                final int n6 = \u3007\u3007888 - o\u3007\u30070\u3007 - n;
                final int oo08 = oooo8o0\u3007.Oo08(o0ooO);
                final int n7 = n4;
                n8 = n6 - oo08;
                n9 = n5;
                n10 = n7;
                n11 = n6;
            }
            else {
                final int \u3007\u3007889 = layoutStateWrapper.\u3007\u3007888();
                if (b3) {
                    n = 0;
                }
                final int n12 = \u3007\u3007889 + o\u3007\u30070\u3007 + n;
                final int oo9 = oooo8o0\u3007.Oo08(o0ooO);
                final int n13 = n4;
                n8 = n12;
                n9 = n5;
                final int n14 = oo9 + n12;
                n10 = n13;
                n11 = n14;
            }
        }
        else {
            n8 = layoutManagerHelper.getPaddingTop() + super.\u3007O8o08O + super.oO80;
            final int n15 = oooo8o0\u3007.o\u30070(o0ooO) + n8;
            if (layoutStateWrapper.o\u30070() == -1) {
                final int \u3007\u3007890 = layoutStateWrapper.\u3007\u3007888();
                if (b3) {
                    n = 0;
                }
                final int n16 = \u3007\u3007890 - o\u3007\u30070\u3007 - n;
                final int oo10 = oooo8o0\u3007.Oo08(o0ooO);
                n10 = n16;
                n9 = n16 - oo10;
                n11 = n15;
            }
            else {
                final int \u3007\u3007891 = layoutStateWrapper.\u3007\u3007888();
                if (b3) {
                    n = 0;
                }
                n9 = \u3007\u3007891 + o\u3007\u30070\u3007 + n;
                n10 = oooo8o0\u3007.Oo08(o0ooO) + n9;
                n11 = n15;
            }
        }
        this.O8\u3007o(o0ooO, n9, n8, n10, n11, layoutManagerHelper);
        this.OOO\u3007O0(layoutChunkResult, o0ooO);
        this.o800o8O = false;
    }
    
    @Override
    public void \u3007o\u3007(final RecyclerView.State state, final VirtualLayoutManager.AnchorInfoWrapper anchorInfoWrapper, final LayoutManagerHelper layoutManagerHelper) {
        super.\u3007o\u3007(state, anchorInfoWrapper, layoutManagerHelper);
        this.o800o8O = true;
    }
}

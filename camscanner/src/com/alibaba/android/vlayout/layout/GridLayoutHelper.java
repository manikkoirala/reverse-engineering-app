// 
// Decompiled by Procyon v0.6.0
// 

package com.alibaba.android.vlayout.layout;

import android.util.SparseIntArray;
import com.alibaba.android.vlayout.OrientationHelperEx;
import java.util.Arrays;
import android.widget.TextView;
import com.alibaba.android.vlayout.VirtualLayoutManager;
import com.alibaba.android.vlayout.LayoutManagerHelper;
import androidx.recyclerview.widget.RecyclerView;
import android.view.View$MeasureSpec;
import android.view.View;
import androidx.annotation.NonNull;

public class GridLayoutHelper extends BaseLayoutHelper
{
    private static final int O8\u3007o;
    private static boolean oo\u3007 = false;
    private float[] O8ooOoo\u3007;
    private boolean OOO\u3007O0;
    private int OoO8;
    private int O\u30078O8\u3007008;
    private int o800o8O;
    private boolean oo88o8O;
    @NonNull
    private SpanSizeLookup o\u3007O8\u3007\u3007o;
    private int[] o\u3007\u30070\u3007;
    private int \u300700;
    private int[] \u30070000OOO;
    private int \u3007O888o0o;
    private View[] \u3007oOO8O8;
    private boolean \u3007oo\u3007;
    
    static {
        O8\u3007o = View$MeasureSpec.makeMeasureSpec(0, 0);
    }
    
    public GridLayoutHelper(final int n, final int n2) {
        this(n, n2, 0);
    }
    
    public GridLayoutHelper(final int n, final int n2, final int n3) {
        this(n, n2, n3, n3);
    }
    
    public GridLayoutHelper(final int n, final int n2, final int n3, final int n4) {
        this.OoO8 = 4;
        this.o800o8O = 0;
        this.\u3007O888o0o = 0;
        this.oo88o8O = true;
        this.\u3007oo\u3007 = false;
        this.o\u3007O8\u3007\u3007o = (SpanSizeLookup)new DefaultSpanSizeLookup();
        this.\u300700 = 0;
        this.O\u30078O8\u3007008 = 0;
        this.O8ooOoo\u3007 = new float[0];
        this.OOO\u3007O0 = false;
        this.O08000(n);
        this.o\u3007O8\u3007\u3007o.Oo08(true);
        this.\u3007\u30078O0\u30078(n2);
        this.\u30078\u30070\u3007o\u3007O(n3);
        this.\u30078(n4);
    }
    
    private void Oo8Oo00oo(final RecyclerView.Recycler recycler, final RecyclerView.State state, int i, int n, final boolean b, final LayoutManagerHelper layoutManagerHelper) {
        final int n2 = 0;
        int n3;
        int n4;
        if (b) {
            n3 = i;
            i = 0;
            n4 = 1;
        }
        else {
            --i;
            n3 = -1;
            n4 = -1;
        }
        int n5;
        if (layoutManagerHelper.getOrientation() == 1 && layoutManagerHelper.\u3007\u30078O0\u30078()) {
            --n;
            n5 = -1;
        }
        else {
            n5 = 1;
            n = n2;
        }
        while (i != n3) {
            final int \u300708O8o\u30070 = this.\u300708O8o\u30070(recycler, state, layoutManagerHelper.getPosition(this.\u3007oOO8O8[i]));
            if (n5 == -1 && \u300708O8o\u30070 > 1) {
                this.\u30070000OOO[i] = n - (\u300708O8o\u30070 - 1);
            }
            else {
                this.\u30070000OOO[i] = n;
            }
            n += \u300708O8o\u30070 * n5;
            i += n4;
        }
    }
    
    private int o\u30070OOo\u30070(final int n, final int n2, final int n3, float \u3007o00) {
        if (!Float.isNaN(\u3007o00) && \u3007o00 > 0.0f && n3 > 0) {
            return View$MeasureSpec.makeMeasureSpec((int)(n3 / \u3007o00 + 0.5f), 1073741824);
        }
        if (!Float.isNaN(super.\u3007O00)) {
            \u3007o00 = super.\u3007O00;
            if (\u3007o00 > 0.0f) {
                return View$MeasureSpec.makeMeasureSpec((int)(n2 / \u3007o00 + 0.5f), 1073741824);
            }
        }
        if (n < 0) {
            return GridLayoutHelper.O8\u3007o;
        }
        return View$MeasureSpec.makeMeasureSpec(n, 1073741824);
    }
    
    private int \u300708O8o\u30070(final RecyclerView.Recycler recycler, final RecyclerView.State state, int convertPreLayoutPositionToPostLayout) {
        if (!state.isPreLayout()) {
            return this.o\u3007O8\u3007\u3007o.\u3007o\u3007(convertPreLayoutPositionToPostLayout);
        }
        convertPreLayoutPositionToPostLayout = recycler.convertPreLayoutPositionToPostLayout(convertPreLayoutPositionToPostLayout);
        if (convertPreLayoutPositionToPostLayout == -1) {
            return 0;
        }
        return this.o\u3007O8\u3007\u3007o.\u3007o\u3007(convertPreLayoutPositionToPostLayout);
    }
    
    private int \u3007\u30070o(final RecyclerView.Recycler recycler, final RecyclerView.State state, int convertPreLayoutPositionToPostLayout) {
        if (!state.isPreLayout()) {
            return this.o\u3007O8\u3007\u3007o.\u3007080(convertPreLayoutPositionToPostLayout, this.OoO8);
        }
        convertPreLayoutPositionToPostLayout = recycler.convertPreLayoutPositionToPostLayout(convertPreLayoutPositionToPostLayout);
        if (convertPreLayoutPositionToPostLayout == -1) {
            return 0;
        }
        return this.o\u3007O8\u3007\u3007o.\u3007080(convertPreLayoutPositionToPostLayout, this.OoO8);
    }
    
    private void \u3007\u3007\u30070\u3007\u30070() {
        final View[] \u3007oOO8O8 = this.\u3007oOO8O8;
        if (\u3007oOO8O8 == null || \u3007oOO8O8.length != this.OoO8) {
            this.\u3007oOO8O8 = new View[this.OoO8];
        }
        final int[] \u30070000OOO = this.\u30070000OOO;
        if (\u30070000OOO == null || \u30070000OOO.length != this.OoO8) {
            this.\u30070000OOO = new int[this.OoO8];
        }
        final int[] o\u3007\u30070\u3007 = this.o\u3007\u30070\u3007;
        if (o\u3007\u30070\u3007 == null || o\u3007\u30070\u3007.length != this.OoO8) {
            this.o\u3007\u30070\u3007 = new int[this.OoO8];
        }
    }
    
    public void O08000(final int n) {
        if (n == this.OoO8) {
            return;
        }
        if (n >= 1) {
            this.OoO8 = n;
            this.o\u3007O8\u3007\u3007o.O8();
            this.\u3007\u3007\u30070\u3007\u30070();
            return;
        }
        final StringBuilder sb = new StringBuilder();
        sb.append("Span count should be at least 1. Provided ");
        sb.append(n);
        throw new IllegalArgumentException(sb.toString());
    }
    
    @Override
    public int Oo08(int n, final boolean b, final boolean b2, final LayoutManagerHelper layoutManagerHelper) {
        final boolean b3 = layoutManagerHelper.getOrientation() == 1;
        if (b) {
            if (n == this.\u3007\u3007888() - 1) {
                int n2;
                if (b3) {
                    n = super.OO0o\u3007\u3007;
                    n2 = super.\u300780\u3007808\u3007O;
                }
                else {
                    n = super.\u30078o8o\u3007;
                    n2 = super.\u3007\u3007888;
                }
                return n + n2;
            }
        }
        else if (n == 0) {
            int n3;
            if (b3) {
                n = -super.\u3007O8o08O;
                n3 = super.oO80;
            }
            else {
                n = -super.OO0o\u3007\u3007\u3007\u30070;
                n3 = super.o\u30070;
            }
            return n - n3;
        }
        return super.Oo08(n, b, b2, layoutManagerHelper);
    }
    
    public void oO(final boolean oo88o8O) {
        this.oo88o8O = oo88o8O;
    }
    
    public void o\u30078(final LayoutManagerHelper layoutManagerHelper) {
        super.o\u30078(layoutManagerHelper);
        this.o\u3007O8\u3007\u3007o.O8();
    }
    
    public void \u30078(final int n) {
        int o\u30078O8\u3007008 = n;
        if (n < 0) {
            o\u30078O8\u3007008 = 0;
        }
        this.O\u30078O8\u3007008 = o\u30078O8\u3007008;
    }
    
    public void \u30078\u30070\u3007o\u3007O(final int n) {
        int \u300700 = n;
        if (n < 0) {
            \u300700 = 0;
        }
        this.\u300700 = \u300700;
    }
    
    @Override
    public void \u3007O8o08O(final LayoutManagerHelper layoutManagerHelper) {
        super.\u3007O8o08O(layoutManagerHelper);
        this.o\u3007O8\u3007\u3007o.O8();
    }
    
    @Override
    public void \u3007o(final RecyclerView.Recycler recycler, final RecyclerView.State state, final VirtualLayoutManager.LayoutStateWrapper layoutStateWrapper, final LayoutChunkResult layoutChunkResult, final LayoutManagerHelper layoutManagerHelper) {
        if (this.OO0o\u3007\u3007\u3007\u30070(layoutStateWrapper.\u3007o\u3007())) {
            return;
        }
        layoutStateWrapper.\u3007o\u3007();
        final boolean enableMarginOverLap = layoutManagerHelper.isEnableMarginOverLap();
        final int oo08 = layoutStateWrapper.Oo08();
        final boolean b = oo08 == 1;
        final OrientationHelperEx oooo8o0\u3007 = layoutManagerHelper.Oooo8o0\u3007();
        final boolean b2 = layoutManagerHelper.getOrientation() == 1;
        if (b2) {
            final int \u3007o888o0o = layoutManagerHelper.\u300780\u3007808\u3007O() - layoutManagerHelper.getPaddingRight() - layoutManagerHelper.getPaddingLeft() - this.OoO8() - this.o800o8O();
            this.\u3007O888o0o = \u3007o888o0o;
            final int ooO8 = this.OoO8;
            this.o800o8O = (int)((\u3007o888o0o - (ooO8 - 1) * this.O\u30078O8\u3007008) * 1.0f / ooO8 + 0.5f);
        }
        else {
            final int \u3007o888o0o2 = layoutManagerHelper.\u3007\u3007808\u3007() - layoutManagerHelper.getPaddingBottom() - layoutManagerHelper.getPaddingTop() - this.\u300700() - this.O\u30078O8\u3007008();
            this.\u3007O888o0o = \u3007o888o0o2;
            final int ooO9 = this.OoO8;
            this.o800o8O = (int)((\u3007o888o0o2 - (ooO9 - 1) * this.\u300700) * 1.0f / ooO9 + 0.5f);
        }
        int ooO10 = this.OoO8;
        this.\u3007\u3007\u30070\u3007\u30070();
        while (true) {
            Label_0761: {
                if (b) {
                    break Label_0761;
                }
                final int \u3007\u30070o = this.\u3007\u30070o(recycler, state, layoutStateWrapper.\u3007o\u3007());
                final int n = this.\u300708O8o\u30070(recycler, state, layoutStateWrapper.\u3007o\u3007()) + \u3007\u30070o;
                if (\u3007\u30070o == this.OoO8 - 1) {
                    ooO10 = n;
                    break Label_0761;
                }
                int \u3007o\u3007 = layoutStateWrapper.\u3007o\u3007();
                int n2 = this.OoO8 - n;
                int n3 = 0;
                int n4 = 0;
                int n5 = 0;
                int n6 = 0;
                int n7;
                int n8;
                while (true) {
                    n7 = n4;
                    n8 = n6;
                    if (n3 >= this.OoO8) {
                        break;
                    }
                    n7 = n4;
                    n8 = n6;
                    if (n2 <= 0) {
                        break;
                    }
                    \u3007o\u3007 -= oo08;
                    if (this.OO0o\u3007\u3007\u3007\u30070(\u3007o\u3007)) {
                        n7 = n4;
                        n8 = n6;
                        break;
                    }
                    final int \u300708O8o\u30070 = this.\u300708O8o\u30070(recycler, state, \u3007o\u3007);
                    if (\u300708O8o\u30070 > this.OoO8) {
                        final StringBuilder sb = new StringBuilder();
                        sb.append("Item at position ");
                        sb.append(\u3007o\u3007);
                        sb.append(" requires ");
                        sb.append(\u300708O8o\u30070);
                        sb.append(" spans but GridLayoutManager has only ");
                        sb.append(this.OoO8);
                        sb.append(" spans.");
                        throw new IllegalArgumentException(sb.toString());
                    }
                    final View \u3007o8o08O = layoutStateWrapper.\u3007O8o08O(recycler, \u3007o\u3007);
                    if (\u3007o8o08O == null) {
                        n7 = n4;
                        n8 = n6;
                        break;
                    }
                    if ((n7 = n4) == 0) {
                        n7 = ((layoutManagerHelper.getReverseLayout() ? (\u3007o\u3007 == this.oO80().Oo08()) : (\u3007o\u3007 == this.oO80().O8())) ? 1 : 0);
                    }
                    if ((n8 = n6) == 0) {
                        n8 = ((layoutManagerHelper.getReverseLayout() ? (\u3007o\u3007 == this.oO80().O8()) : (\u3007o\u3007 == this.oO80().Oo08())) ? 1 : 0);
                    }
                    n2 -= \u300708O8o\u30070;
                    if (n2 < 0) {
                        break;
                    }
                    n5 += \u300708O8o\u30070;
                    this.\u3007oOO8O8[n3] = \u3007o8o08O;
                    ++n3;
                    n4 = n7;
                    n6 = n8;
                }
                if (n3 > 0) {
                    for (int n9 = n3 - 1, i = 0; i < n9; ++i, --n9) {
                        final View[] \u3007oOO8O8 = this.\u3007oOO8O8;
                        final View view = \u3007oOO8O8[i];
                        \u3007oOO8O8[i] = \u3007oOO8O8[n9];
                        \u3007oOO8O8[n9] = view;
                    }
                }
                final int n10 = n3;
                int n11 = n5;
                ooO10 = n;
                int n12 = n10;
                int n13 = n8;
                OrientationHelperEx orientationHelperEx;
                int n14;
                while (true) {
                    orientationHelperEx = oooo8o0\u3007;
                    n14 = ooO10;
                    if (n12 >= this.OoO8) {
                        break;
                    }
                    n14 = ooO10;
                    if (!layoutStateWrapper.oO80(state) || (n14 = ooO10) <= 0) {
                        break;
                    }
                    final int \u3007o\u30072 = layoutStateWrapper.\u3007o\u3007();
                    if (this.OO0o\u3007\u3007\u3007\u30070(\u3007o\u30072)) {
                        n14 = ooO10;
                        if (GridLayoutHelper.oo\u3007) {
                            final StringBuilder sb2 = new StringBuilder();
                            sb2.append("pos [");
                            sb2.append(\u3007o\u30072);
                            sb2.append("] is out of range");
                            n14 = ooO10;
                            break;
                        }
                        break;
                    }
                    else {
                        final int \u300708O8o\u30072 = this.\u300708O8o\u30070(recycler, state, \u3007o\u30072);
                        if (\u300708O8o\u30072 > this.OoO8) {
                            final StringBuilder sb3 = new StringBuilder();
                            sb3.append("Item at position ");
                            sb3.append(\u3007o\u30072);
                            sb3.append(" requires ");
                            sb3.append(\u300708O8o\u30072);
                            sb3.append(" spans but GridLayoutManager has only ");
                            sb3.append(this.OoO8);
                            sb3.append(" spans.");
                            throw new IllegalArgumentException(sb3.toString());
                        }
                        n14 = ooO10 - \u300708O8o\u30072;
                        if (n14 < 0) {
                            break;
                        }
                        final View \u30078o8o\u3007 = layoutStateWrapper.\u30078o8o\u3007(recycler);
                        if (\u30078o8o\u3007 == null) {
                            break;
                        }
                        if (n7 == 0) {
                            n7 = ((layoutManagerHelper.getReverseLayout() ? (\u3007o\u30072 == this.oO80().Oo08()) : (\u3007o\u30072 == this.oO80().O8())) ? 1 : 0);
                        }
                        int n15;
                        if ((n15 = n13) == 0) {
                            n15 = ((layoutManagerHelper.getReverseLayout() ? (\u3007o\u30072 == this.oO80().O8()) : (\u3007o\u30072 == this.oO80().Oo08())) ? 1 : 0);
                        }
                        n11 += \u300708O8o\u30072;
                        this.\u3007oOO8O8[n12] = \u30078o8o\u3007;
                        ++n12;
                        ooO10 = n14;
                        n13 = n15;
                    }
                }
                if (n12 == 0) {
                    return;
                }
                this.Oo8Oo00oo(recycler, state, n12, n11, b, layoutManagerHelper);
                if (n14 > 0 && n12 == n11 && this.oo88o8O) {
                    if (b2) {
                        this.o800o8O = (this.\u3007O888o0o - (n12 - 1) * this.O\u30078O8\u3007008) / n12;
                    }
                    else {
                        this.o800o8O = (this.\u3007O888o0o - (n12 - 1) * this.\u300700) / n12;
                    }
                }
                else if (!b && n14 == 0 && n12 == n11 && this.oo88o8O) {
                    if (b2) {
                        this.o800o8O = (this.\u3007O888o0o - (n12 - 1) * this.O\u30078O8\u3007008) / n12;
                    }
                    else {
                        this.o800o8O = (this.\u3007O888o0o - (n12 - 1) * this.\u300700) / n12;
                    }
                }
                final float[] o8ooOoo\u3007 = this.O8ooOoo\u3007;
                boolean b3;
                if (o8ooOoo\u3007 != null && o8ooOoo\u3007.length > 0) {
                    int n16;
                    int n17;
                    int n18;
                    if (b2) {
                        n16 = this.\u3007O888o0o;
                        n17 = n12 - 1;
                        n18 = this.O\u30078O8\u3007008;
                    }
                    else {
                        n16 = this.\u3007O888o0o;
                        n17 = n12 - 1;
                        n18 = this.\u300700;
                    }
                    final int n19 = n16 - n17 * n18;
                    int ooO11;
                    if (n14 > 0 && this.oo88o8O) {
                        ooO11 = n12;
                    }
                    else {
                        ooO11 = this.OoO8;
                    }
                    int n20 = n19;
                    int j = 0;
                    int n21 = 0;
                    while (j < ooO11) {
                        final float[] o8ooOoo\u30072 = this.O8ooOoo\u3007;
                        Label_1545: {
                            if (j < o8ooOoo\u30072.length && !Float.isNaN(o8ooOoo\u30072[j])) {
                                final float n22 = this.O8ooOoo\u3007[j];
                                if (n22 >= 0.0f) {
                                    final int[] o\u3007\u30070\u3007 = this.o\u3007\u30070\u3007;
                                    final int n23 = (int)(n22 * 1.0f / 100.0f * n19 + 0.5f);
                                    o\u3007\u30070\u3007[j] = n23;
                                    n20 -= n23;
                                    break Label_1545;
                                }
                            }
                            ++n21;
                            this.o\u3007\u30070\u3007[j] = -1;
                        }
                        ++j;
                    }
                    if (n21 > 0) {
                        final int n24 = n20 / n21;
                        for (int k = 0; k < ooO11; ++k) {
                            final int[] o\u3007\u30070\u30072 = this.o\u3007\u30070\u3007;
                            if (o\u3007\u30070\u30072[k] < 0) {
                                o\u3007\u30070\u30072[k] = n24;
                            }
                        }
                    }
                    b3 = true;
                }
                else {
                    b3 = false;
                }
                int l = 0;
                int n25 = 0;
                while (l < n12) {
                    final View view2 = this.\u3007oOO8O8[l];
                    int n26;
                    if (b) {
                        n26 = -1;
                    }
                    else {
                        n26 = 0;
                    }
                    layoutManagerHelper.\u30070\u3007O0088o(layoutStateWrapper, view2, n26);
                    final int \u300708O8o\u30073 = this.\u300708O8o\u30070(recycler, state, layoutManagerHelper.getPosition(view2));
                    int n29;
                    if (b3) {
                        final int n27 = this.\u30070000OOO[l];
                        int b4 = 0;
                        for (int n28 = 0; n28 < \u300708O8o\u30073; ++n28) {
                            b4 += this.o\u3007\u30070\u3007[n28 + n27];
                        }
                        n29 = View$MeasureSpec.makeMeasureSpec(Math.max(0, b4), 1073741824);
                    }
                    else {
                        final int o800o8O = this.o800o8O;
                        final int max = Math.max(0, \u300708O8o\u30073 - 1);
                        int n30;
                        if (b2) {
                            n30 = this.O\u30078O8\u3007008;
                        }
                        else {
                            n30 = this.\u300700;
                        }
                        n29 = View$MeasureSpec.makeMeasureSpec(o800o8O * \u300708O8o\u30073 + max * n30, 1073741824);
                    }
                    final VirtualLayoutManager.LayoutParams layoutParams = (VirtualLayoutManager.LayoutParams)view2.getLayoutParams();
                    if (layoutManagerHelper.getOrientation() == 1) {
                        layoutManagerHelper.measureChildWithMargins(view2, n29, this.o\u30070OOo\u30070(layoutParams.height, this.\u3007O888o0o, View$MeasureSpec.getSize(n29), layoutParams.\u3007OOo8\u30070));
                    }
                    else {
                        layoutManagerHelper.measureChildWithMargins(view2, this.o\u30070OOo\u30070(layoutParams.width, this.\u3007O888o0o, View$MeasureSpec.getSize(n29), layoutParams.\u3007OOo8\u30070), View$MeasureSpec.getSize(n29));
                    }
                    final int oo9 = orientationHelperEx.Oo08(view2);
                    int n31 = n25;
                    if (oo9 > n25) {
                        n31 = oo9;
                    }
                    ++l;
                    n25 = n31;
                }
                final int o\u30070OOo\u30070 = this.o\u30070OOo\u30070(n25, this.\u3007O888o0o, 0, Float.NaN);
                for (int n32 = 0; n32 < n12; ++n32) {
                    final View view3 = this.\u3007oOO8O8[n32];
                    if (orientationHelperEx.Oo08(view3) != n25) {
                        final int \u300708O8o\u30074 = this.\u300708O8o\u30070(recycler, state, layoutManagerHelper.getPosition(view3));
                        int n35;
                        if (b3) {
                            final int n33 = this.\u30070000OOO[n32];
                            int n34 = 0;
                            int b5 = 0;
                            while (n34 < \u300708O8o\u30074) {
                                b5 += this.o\u3007\u30070\u3007[n34 + n33];
                                ++n34;
                            }
                            n35 = View$MeasureSpec.makeMeasureSpec(Math.max(0, b5), 1073741824);
                        }
                        else {
                            final int o800o8O2 = this.o800o8O;
                            final int max2 = Math.max(0, \u300708O8o\u30074 - 1);
                            int n36;
                            if (b2) {
                                n36 = this.O\u30078O8\u3007008;
                            }
                            else {
                                n36 = this.\u300700;
                            }
                            n35 = View$MeasureSpec.makeMeasureSpec(o800o8O2 * \u300708O8o\u30074 + max2 * n36, 1073741824);
                        }
                        if (layoutManagerHelper.getOrientation() == 1) {
                            layoutManagerHelper.measureChildWithMargins(view3, n35, o\u30070OOo\u30070);
                        }
                        else {
                            layoutManagerHelper.measureChildWithMargins(view3, o\u30070OOo\u30070, n35);
                        }
                    }
                }
                int o\u3007\u30070\u30073;
                if (n7 != 0) {
                    o\u3007\u30070\u30073 = this.o\u3007\u30070\u3007(layoutManagerHelper, b2, layoutManagerHelper.getReverseLayout() ^ true, enableMarginOverLap);
                }
                else {
                    o\u3007\u30070\u30073 = 0;
                }
                int \u30070000OOO;
                if (n13 != 0) {
                    \u30070000OOO = this.\u30070000OOO(layoutManagerHelper, b2, layoutManagerHelper.getReverseLayout() ^ true, enableMarginOverLap);
                }
                else {
                    \u30070000OOO = 0;
                }
                layoutChunkResult.\u3007080 = n25 + o\u3007\u30070\u30073 + \u30070000OOO;
                final boolean b6 = layoutStateWrapper.o\u30070() == -1;
                if (!this.OOO\u3007O0 && (n13 == 0 || !b6) && (n7 == 0 || b6)) {
                    final int \u3007080 = layoutChunkResult.\u3007080;
                    int n37;
                    if (b2) {
                        n37 = this.\u300700;
                    }
                    else {
                        n37 = this.O\u30078O8\u3007008;
                    }
                    layoutChunkResult.\u3007080 = \u3007080 + n37;
                }
                int m;
                int i2;
                if (b2) {
                    if (layoutStateWrapper.o\u30070() == -1) {
                        final int \u3007\u3007888 = layoutStateWrapper.\u3007\u3007888();
                        int \u300781;
                        if (!this.OOO\u3007O0 && n13 == 0) {
                            \u300781 = this.\u300700;
                        }
                        else {
                            \u300781 = 0;
                        }
                        m = \u3007\u3007888 - \u30070000OOO - \u300781;
                        i2 = m - n25;
                    }
                    else {
                        final int \u3007\u3007889 = layoutStateWrapper.\u3007\u3007888();
                        int \u300782;
                        if (!this.OOO\u3007O0 && n7 == 0) {
                            \u300782 = this.\u300700;
                        }
                        else {
                            \u300782 = 0;
                        }
                        i2 = \u300782 + (\u3007\u3007889 + o\u3007\u30070\u30073);
                        m = i2 + n25;
                    }
                    final int i3 = 0;
                    final int i4 = 0;
                }
                else if (layoutStateWrapper.o\u30070() == -1) {
                    final int \u3007\u3007890 = layoutStateWrapper.\u3007\u3007888();
                    int o\u30078O8\u3007008;
                    if (!this.OOO\u3007O0 && n13 == 0) {
                        o\u30078O8\u3007008 = this.O\u30078O8\u3007008;
                    }
                    else {
                        o\u30078O8\u3007008 = 0;
                    }
                    final int n38 = \u3007\u3007890 - \u30070000OOO - o\u30078O8\u3007008;
                    i2 = 0;
                    m = 0;
                    final int i4 = n38 - n25;
                    final int i3 = n38;
                }
                else {
                    final int \u3007\u3007891 = layoutStateWrapper.\u3007\u3007888();
                    int o\u30078O8\u30079;
                    if (!this.OOO\u3007O0 && n7 == 0) {
                        o\u30078O8\u30079 = this.O\u30078O8\u3007008;
                    }
                    else {
                        o\u30078O8\u30079 = 0;
                    }
                    final int i4 = o\u30078O8\u30079 + (\u3007\u3007891 + o\u3007\u30070\u30073);
                    final int i3 = i4 + n25;
                    i2 = 0;
                    m = 0;
                }
                for (int n39 = 0; n39 < n12; ++n39) {
                    final View view4 = this.\u3007oOO8O8[n39];
                    final int i5 = this.\u30070000OOO[n39];
                    final VirtualLayoutManager.LayoutParams layoutParams2 = (VirtualLayoutManager.LayoutParams)view4.getLayoutParams();
                    int i3;
                    int i4;
                    if (b2) {
                        int n42;
                        if (b3) {
                            int n40 = layoutManagerHelper.getPaddingLeft() + super.OO0o\u3007\u3007\u3007\u30070 + super.o\u30070;
                            int n41 = 0;
                            while (true) {
                                n42 = n40;
                                if (n41 >= i5) {
                                    break;
                                }
                                n40 += this.o\u3007\u30070\u3007[n41] + this.O\u30078O8\u3007008;
                                ++n41;
                            }
                        }
                        else {
                            n42 = layoutManagerHelper.getPaddingLeft() + super.OO0o\u3007\u3007\u3007\u30070 + super.o\u30070 + this.o800o8O * i5 + this.O\u30078O8\u3007008 * i5;
                        }
                        final int n43 = orientationHelperEx.o\u30070(view4) + n42;
                        i4 = n42;
                        i3 = n43;
                    }
                    else {
                        int n46;
                        if (b3) {
                            int n44 = layoutManagerHelper.getPaddingTop() + super.\u3007O8o08O + super.oO80;
                            int n45 = 0;
                            while (true) {
                                n46 = n44;
                                if (n45 >= i5) {
                                    break;
                                }
                                n44 += this.o\u3007\u30070\u3007[n45] + this.\u300700;
                                ++n45;
                            }
                        }
                        else {
                            n46 = layoutManagerHelper.getPaddingTop() + super.\u3007O8o08O + super.oO80 + this.o800o8O * i5 + this.\u300700 * i5;
                        }
                        final int o\u30070 = orientationHelperEx.o\u30070(view4);
                        final int n47 = n46;
                        final int n48 = o\u30070 + n46;
                        i2 = n47;
                        m = n48;
                    }
                    if (GridLayoutHelper.oo\u3007) {
                        final StringBuilder sb4 = new StringBuilder();
                        sb4.append("layout item in position: ");
                        sb4.append(((RecyclerView.LayoutParams)layoutParams2).getViewPosition());
                        sb4.append(" with text ");
                        sb4.append((Object)((TextView)view4).getText());
                        sb4.append(" with SpanIndex: ");
                        sb4.append(i5);
                        sb4.append(" into (");
                        sb4.append(i4);
                        sb4.append(", ");
                        sb4.append(i2);
                        sb4.append(", ");
                        sb4.append(i3);
                        sb4.append(", ");
                        sb4.append(m);
                        sb4.append(" )");
                    }
                    this.O8\u3007o(view4, i4, i2, i3, m, layoutManagerHelper);
                    if (((RecyclerView.LayoutParams)layoutParams2).isItemRemoved() || ((RecyclerView.LayoutParams)layoutParams2).isItemChanged()) {
                        layoutChunkResult.\u3007o\u3007 = true;
                    }
                    layoutChunkResult.O8 |= view4.isFocusable();
                }
                this.OOO\u3007O0 = false;
                Arrays.fill(this.\u3007oOO8O8, null);
                Arrays.fill(this.\u30070000OOO, 0);
                Arrays.fill(this.o\u3007\u30070\u3007, 0);
                return;
            }
            int n12 = 0;
            int n11 = 0;
            int n7 = 0;
            int n13 = 0;
            continue;
        }
    }
    
    @Override
    public void \u3007o\u3007(final RecyclerView.State state, final VirtualLayoutManager.AnchorInfoWrapper anchorInfoWrapper, final LayoutManagerHelper layoutManagerHelper) {
        if (state.getItemCount() > 0 && !state.isPreLayout()) {
            int i;
            int \u3007080 = i = this.o\u3007O8\u3007\u3007o.\u3007080(anchorInfoWrapper.\u3007080, this.OoO8);
            if (anchorInfoWrapper.\u3007o\u3007) {
                while (\u3007080 < this.OoO8 - 1 && anchorInfoWrapper.\u3007080 < this.oO80().Oo08()) {
                    final int \u300781 = anchorInfoWrapper.\u3007080 + 1;
                    anchorInfoWrapper.\u3007080 = \u300781;
                    \u3007080 = this.o\u3007O8\u3007\u3007o.\u3007080(\u300781, this.OoO8);
                }
            }
            else {
                while (i > 0) {
                    int \u300782 = anchorInfoWrapper.\u3007080;
                    if (\u300782 <= 0) {
                        break;
                    }
                    --\u300782;
                    anchorInfoWrapper.\u3007080 = \u300782;
                    i = this.o\u3007O8\u3007\u3007o.\u3007080(\u300782, this.OoO8);
                }
            }
            this.OOO\u3007O0 = true;
        }
    }
    
    @Override
    public void \u3007\u3007808\u3007(final int n, final int n2) {
        this.o\u3007O8\u3007\u3007o.o\u30070(n);
        this.o\u3007O8\u3007\u3007o.O8();
    }
    
    static final class DefaultSpanSizeLookup extends SpanSizeLookup
    {
        @Override
        public int \u3007o00\u3007\u3007Oo(final int n, final int n2) {
            return (n - super.\u3007o\u3007) % n2;
        }
        
        @Override
        public int \u3007o\u3007(final int n) {
            return 1;
        }
    }
    
    public abstract static class SpanSizeLookup
    {
        final SparseIntArray \u3007080;
        private boolean \u3007o00\u3007\u3007Oo;
        int \u3007o\u3007;
        
        public SpanSizeLookup() {
            this.\u3007080 = new SparseIntArray();
            this.\u3007o00\u3007\u3007Oo = false;
            this.\u3007o\u3007 = 0;
        }
        
        public void O8() {
            this.\u3007080.clear();
        }
        
        public void Oo08(final boolean \u3007o00\u3007\u3007Oo) {
            this.\u3007o00\u3007\u3007Oo = \u3007o00\u3007\u3007Oo;
        }
        
        public void o\u30070(final int \u3007o\u3007) {
            this.\u3007o\u3007 = \u3007o\u3007;
        }
        
        int \u3007080(final int n, int \u3007o00\u3007\u3007Oo) {
            if (!this.\u3007o00\u3007\u3007Oo) {
                return this.\u3007o00\u3007\u3007Oo(n, \u3007o00\u3007\u3007Oo);
            }
            final int value = this.\u3007080.get(n, -1);
            if (value != -1) {
                return value;
            }
            \u3007o00\u3007\u3007Oo = this.\u3007o00\u3007\u3007Oo(n, \u3007o00\u3007\u3007Oo);
            this.\u3007080.put(n, \u3007o00\u3007\u3007Oo);
            return \u3007o00\u3007\u3007Oo;
        }
        
        public abstract int \u3007o00\u3007\u3007Oo(final int p0, final int p1);
        
        public abstract int \u3007o\u3007(final int p0);
    }
}

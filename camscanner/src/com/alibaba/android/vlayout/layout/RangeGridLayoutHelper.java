// 
// Decompiled by Procyon v0.6.0
// 

package com.alibaba.android.vlayout.layout;

import com.alibaba.android.vlayout.Range;
import java.util.Iterator;
import java.util.Map;
import androidx.annotation.NonNull;
import android.view.View;
import com.alibaba.android.vlayout.OrientationHelperEx;
import java.util.Arrays;
import com.alibaba.android.vlayout.VirtualLayoutManager;
import com.alibaba.android.vlayout.LayoutManagerHelper;
import androidx.recyclerview.widget.RecyclerView;
import android.view.View$MeasureSpec;

public class RangeGridLayoutHelper extends BaseLayoutHelper
{
    private static boolean oo88o8O = false;
    private static final int \u3007oo\u3007;
    private GridRangeStyle OoO8;
    private int o800o8O;
    private boolean \u3007O888o0o;
    
    static {
        \u3007oo\u3007 = View$MeasureSpec.makeMeasureSpec(0, 0);
    }
    
    private void Oo8Oo00oo(final GridRangeStyle gridRangeStyle, final RecyclerView.Recycler recycler, final RecyclerView.State state, int i, int n, final boolean b, final LayoutManagerHelper layoutManagerHelper) {
        final int n2 = 0;
        int n3;
        int n4;
        if (b) {
            n3 = i;
            i = 0;
            n4 = 1;
        }
        else {
            --i;
            n3 = -1;
            n4 = -1;
        }
        int n5;
        if (layoutManagerHelper.getOrientation() == 1 && layoutManagerHelper.\u3007\u30078O0\u30078()) {
            --n;
            n5 = -1;
        }
        else {
            n5 = 1;
            n = n2;
        }
        while (i != n3) {
            final int oo = this.oO(gridRangeStyle.\u3007O888o0o, recycler, state, layoutManagerHelper.getPosition(gridRangeStyle.\u300700[i]));
            if (n5 == -1 && oo > 1) {
                gridRangeStyle.O\u30078O8\u3007008[i] = n - (oo - 1);
            }
            else {
                gridRangeStyle.O\u30078O8\u3007008[i] = n;
            }
            n += oo * n5;
            i += n4;
        }
    }
    
    private int oO(final GridLayoutHelper.SpanSizeLookup spanSizeLookup, final RecyclerView.Recycler recycler, final RecyclerView.State state, int convertPreLayoutPositionToPostLayout) {
        if (!state.isPreLayout()) {
            return spanSizeLookup.\u3007o\u3007(convertPreLayoutPositionToPostLayout);
        }
        convertPreLayoutPositionToPostLayout = recycler.convertPreLayoutPositionToPostLayout(convertPreLayoutPositionToPostLayout);
        if (convertPreLayoutPositionToPostLayout == -1) {
            return 0;
        }
        return spanSizeLookup.\u3007o\u3007(convertPreLayoutPositionToPostLayout);
    }
    
    private int \u300708O8o\u30070(final GridLayoutHelper.SpanSizeLookup spanSizeLookup, final int n, final RecyclerView.Recycler recycler, final RecyclerView.State state, int convertPreLayoutPositionToPostLayout) {
        if (!state.isPreLayout()) {
            return spanSizeLookup.\u3007080(convertPreLayoutPositionToPostLayout, n);
        }
        convertPreLayoutPositionToPostLayout = recycler.convertPreLayoutPositionToPostLayout(convertPreLayoutPositionToPostLayout);
        if (convertPreLayoutPositionToPostLayout == -1) {
            return 0;
        }
        return spanSizeLookup.\u3007080(convertPreLayoutPositionToPostLayout, n);
    }
    
    private int \u3007\u30070o(final GridRangeStyle gridRangeStyle, final int n, final int n2, final int n3, final float v) {
        if (!Float.isNaN(v) && v > 0.0f && n3 > 0) {
            return View$MeasureSpec.makeMeasureSpec((int)(n3 / v + 0.5f), 1073741824);
        }
        if (!Float.isNaN(gridRangeStyle.\u3007O00) && gridRangeStyle.\u3007O00 > 0.0f) {
            return View$MeasureSpec.makeMeasureSpec((int)(n2 / gridRangeStyle.\u3007O00 + 0.5f), 1073741824);
        }
        if (n < 0) {
            return RangeGridLayoutHelper.\u3007oo\u3007;
        }
        return View$MeasureSpec.makeMeasureSpec(n, 1073741824);
    }
    
    @Override
    public int Oo08(final int n, final boolean b, final boolean b2, final LayoutManagerHelper layoutManagerHelper) {
        final boolean b3 = layoutManagerHelper.getOrientation() == 1;
        if (b) {
            if (n == this.\u3007\u3007888() - 1) {
                return GridRangeStyle.O8O\u3007(this.OoO8, b3);
            }
        }
        else if (n == 0) {
            return GridRangeStyle.O0O8OO088(this.OoO8, b3);
        }
        return super.Oo08(n, b, b2, layoutManagerHelper);
    }
    
    @Override
    public boolean o8() {
        return this.OoO8.O\u3007O\u3007oO();
    }
    
    public int o\u30070OOo\u30070(final LayoutManagerHelper layoutManagerHelper) {
        final GridRangeStyle oo\u3007O = this.OoO8.Oo\u3007O(this.oO80().O8());
        int n;
        int n2;
        if (layoutManagerHelper.getOrientation() == 1) {
            n = oo\u3007O.\u3007\u3007808\u3007();
            n2 = oo\u3007O.\u30070\u3007O0088o();
        }
        else {
            n = oo\u3007O.OO0o\u3007\u3007();
            n2 = oo\u3007O.\u3007O00();
        }
        return n + n2;
    }
    
    public void o\u30078(final LayoutManagerHelper layoutManagerHelper) {
        super.o\u30078(layoutManagerHelper);
        this.OoO8.\u30078(layoutManagerHelper);
        this.OoO8.o8O\u3007();
    }
    
    @Override
    public void \u3007080(final RecyclerView.Recycler recycler, final RecyclerView.State state, final int n, final int n2, final int n3, final LayoutManagerHelper layoutManagerHelper) {
        this.OoO8.\u3007080(recycler, state, n, n2, n3, layoutManagerHelper);
    }
    
    @Override
    public void \u3007O8o08O(final LayoutManagerHelper layoutManagerHelper) {
        super.\u3007O8o08O(layoutManagerHelper);
        this.OoO8.o8O\u3007();
    }
    
    @Override
    public void \u3007o(final RecyclerView.Recycler recycler, final RecyclerView.State state, final VirtualLayoutManager.LayoutStateWrapper layoutStateWrapper, final LayoutChunkResult layoutChunkResult, final LayoutManagerHelper layoutManagerHelper) {
        if (this.OO0o\u3007\u3007\u3007\u30070(layoutStateWrapper.\u3007o\u3007())) {
            return;
        }
        final int \u3007o\u3007 = layoutStateWrapper.\u3007o\u3007();
        final GridRangeStyle oo\u3007O = this.OoO8.Oo\u3007O(\u3007o\u3007);
        final int oo08 = layoutStateWrapper.Oo08();
        final boolean b = oo08 == 1;
        final OrientationHelperEx oooo8o0\u3007 = layoutManagerHelper.Oooo8o0\u3007();
        final boolean b2 = layoutManagerHelper.getOrientation() == 1;
        if (b2) {
            final int o800o8O = layoutManagerHelper.\u300780\u3007808\u3007O() - layoutManagerHelper.getPaddingRight() - layoutManagerHelper.getPaddingLeft() - oo\u3007O.OO0o\u3007\u3007\u3007\u30070() - oo\u3007O.\u30078o8o\u3007();
            this.o800o8O = o800o8O;
            oo\u3007O.\u30070\u3007O0088o = (int)((o800o8O - (oo\u3007O.\u3007\u30078O0\u30078 - 1) * oo\u3007O.\u3007oo\u3007) * 1.0f / oo\u3007O.\u3007\u30078O0\u30078 + 0.5f);
        }
        else {
            final int o800o8O2 = layoutManagerHelper.\u3007\u3007808\u3007() - layoutManagerHelper.getPaddingBottom() - layoutManagerHelper.getPaddingTop() - oo\u3007O.OoO8() - oo\u3007O.o800o8O();
            this.o800o8O = o800o8O2;
            oo\u3007O.\u30070\u3007O0088o = (int)((o800o8O2 - (oo\u3007O.\u3007\u30078O0\u30078 - 1) * oo\u3007O.oo88o8O) * 1.0f / oo\u3007O.\u3007\u30078O0\u30078 + 0.5f);
        }
        final int o000 = oo\u3007O.\u3007\u30078O0\u30078;
        oo\u3007O.\u3007o0O0O8();
    Label_0844:
        while (true) {
            Label_0818: {
                if (b) {
                    final int n = o000;
                    break Label_0818;
                }
                final GridLayoutHelper.SpanSizeLookup o0o\u3007\u3007Oo = oo\u3007O.\u3007O888o0o;
                final int o2 = oo\u3007O.\u3007\u30078O0\u30078;
                final String str = " requires ";
                final String str2 = " spans.";
                final int \u300708O8o\u30070 = this.\u300708O8o\u30070(o0o\u3007\u3007Oo, o2, recycler, state, \u3007o\u3007);
                final int n2 = this.oO(oo\u3007O.\u3007O888o0o, recycler, state, \u3007o\u3007) + \u300708O8o\u30070;
                if (\u300708O8o\u30070 != oo\u3007O.\u3007\u30078O0\u30078 - 1) {
                    int \u3007o\u30072 = layoutStateWrapper.\u3007o\u3007();
                    int n3 = oo\u3007O.\u3007\u30078O0\u30078 - n2;
                    int n4 = 0;
                    int n5 = 0;
                    int n6 = 0;
                    int n7 = 0;
                    while (true) {
                        while (n6 < oo\u3007O.\u3007\u30078O0\u30078 && n3 > 0) {
                            final int i = \u3007o\u30072 - oo08;
                            if (oo\u3007O.o\u30070OOo\u30070(i)) {
                                break;
                            }
                            final int oo9 = this.oO(oo\u3007O.\u3007O888o0o, recycler, state, i);
                            if (oo9 > oo\u3007O.\u3007\u30078O0\u30078) {
                                final StringBuilder sb = new StringBuilder();
                                sb.append("Item at position ");
                                sb.append(i);
                                sb.append(" requires ");
                                sb.append(oo9);
                                sb.append(" spans but RangeGridLayoutHelper has only ");
                                sb.append(oo\u3007O.\u3007\u30078O0\u30078);
                                sb.append(" spans.");
                                throw new IllegalArgumentException(sb.toString());
                            }
                            final View \u3007o8o08O = layoutStateWrapper.\u3007O8o08O(recycler, i);
                            if (\u3007o8o08O == null) {
                                break;
                            }
                            int n8;
                            if ((n8 = n4) == 0) {
                                n8 = ((layoutManagerHelper.getReverseLayout() ? (i == this.OoO8.O8\u3007o().Oo08()) : (i == this.OoO8.O8\u3007o().O8())) ? 1 : 0);
                            }
                            if (n7 == 0) {
                                final boolean reverseLayout = layoutManagerHelper.getReverseLayout();
                                final GridRangeStyle ooO8 = this.OoO8;
                                n7 = ((reverseLayout ? (i == ooO8.O8\u3007o().O8()) : (i == ooO8.O8\u3007o().Oo08())) ? 1 : 0);
                            }
                            n3 -= oo9;
                            if (n3 < 0) {
                                if (n6 > 0) {
                                    for (int n9 = n6 - 1, j = 0; j < n9; ++j, --n9) {
                                        final View view = oo\u3007O.\u300700[j];
                                        oo\u3007O.\u300700[j] = oo\u3007O.\u300700[n9];
                                        oo\u3007O.\u300700[n9] = view;
                                    }
                                }
                                final int n10 = n6;
                                final int n = n2;
                                final int n11 = n8;
                                final int n12 = 0;
                                final int b3 = 0;
                                final int n13 = n5;
                                final int n14 = n7;
                                break Label_0844;
                            }
                            n5 += oo9;
                            oo\u3007O.\u300700[n6] = \u3007o8o08O;
                            ++n6;
                            n4 = n8;
                            \u3007o\u30072 = i;
                        }
                        int n8 = n4;
                        continue;
                    }
                }
                int n = n2;
                break Label_0818;
                int n10 = 0;
                int b3 = 0;
                int n13 = 0;
                OrientationHelperEx orientationHelperEx = null;
                int n17 = 0;
                int n18 = 0;
                int n22 = 0;
                int n23 = 0;
                Label_1599: {
                    int n11 = 0;
                    int n12 = 0;
                    int n14 = 0;
                    int n15;
                    while (true) {
                        orientationHelperEx = oooo8o0\u3007;
                        n15 = n;
                        if (n10 >= oo\u3007O.\u3007\u30078O0\u30078) {
                            break;
                        }
                        n15 = n;
                        if (!layoutStateWrapper.oO80(state) || (n15 = n) <= 0) {
                            break;
                        }
                        final int \u3007o\u30073 = layoutStateWrapper.\u3007o\u3007();
                        if (oo\u3007O.o\u30070OOo\u30070(\u3007o\u30073)) {
                            n15 = n;
                            if (RangeGridLayoutHelper.oo88o8O) {
                                final StringBuilder sb2 = new StringBuilder();
                                sb2.append("pos [");
                                sb2.append(\u3007o\u30073);
                                sb2.append("] is out of range");
                                n15 = n;
                                break;
                            }
                            break;
                        }
                        else {
                            final int oo10 = this.oO(oo\u3007O.\u3007O888o0o, recycler, state, \u3007o\u30073);
                            if (oo10 > oo\u3007O.\u3007\u30078O0\u30078) {
                                final StringBuilder sb3 = new StringBuilder();
                                sb3.append("Item at position ");
                                sb3.append(\u3007o\u30073);
                                sb3.append(str);
                                sb3.append(oo10);
                                sb3.append(" spans but GridLayoutManager has only ");
                                sb3.append(oo\u3007O.\u3007\u30078O0\u30078);
                                sb3.append(str2);
                                throw new IllegalArgumentException(sb3.toString());
                            }
                            final int n16 = n - oo10;
                            if (n16 < 0) {
                                n15 = n16;
                                break;
                            }
                            if ((n17 = n11) == 0) {
                                n17 = ((layoutManagerHelper.getReverseLayout() ? (\u3007o\u30073 == this.OoO8.O8\u3007o().Oo08()) : (\u3007o\u30073 == this.OoO8.O8\u3007o().O8())) ? 1 : 0);
                            }
                            if ((n18 = n12) == 0) {
                                n18 = n12;
                                if (!oo\u3007O.equals(this.OoO8)) {
                                    n18 = ((layoutManagerHelper.getReverseLayout() ? (\u3007o\u30073 == oo\u3007O.O8\u3007o().Oo08()) : (\u3007o\u30073 == oo\u3007O.O8\u3007o().O8())) ? 1 : 0);
                                }
                            }
                            int n19;
                            if ((n19 = n14) == 0) {
                                n19 = ((layoutManagerHelper.getReverseLayout() ? (\u3007o\u30073 == this.OoO8.O8\u3007o().O8()) : (\u3007o\u30073 == this.OoO8.O8\u3007o().Oo08())) ? 1 : 0);
                            }
                            int n20 = b3;
                            Label_1443: {
                                if (b3 == 0) {
                                    n20 = b3;
                                    if (!oo\u3007O.equals(this.OoO8)) {
                                        b3 = (n20 = ((layoutManagerHelper.getReverseLayout() ? (\u3007o\u30073 == oo\u3007O.O8\u3007o().O8()) : (\u3007o\u30073 == oo\u3007O.O8\u3007o().Oo08())) ? 1 : 0));
                                        if (RangeGridLayoutHelper.oo88o8O) {
                                            final StringBuilder sb4 = new StringBuilder();
                                            sb4.append("isSecondEndLineLogic:");
                                            sb4.append((boolean)(b3 != 0));
                                            sb4.append("  helper.getReverseLayout()=");
                                            sb4.append(layoutManagerHelper.getReverseLayout());
                                            sb4.append(" pos=");
                                            sb4.append(\u3007o\u30073);
                                            sb4.append(" rangeStyle.getRange().getLower()=");
                                            sb4.append(oo\u3007O.O8\u3007o().O8());
                                            sb4.append(" rangeStyle.getRange().getUpper()=");
                                            sb4.append(oo\u3007O.O8\u3007o().Oo08());
                                            break Label_1443;
                                        }
                                    }
                                }
                                b3 = n20;
                            }
                            final int n21 = n16;
                            final View \u30078o8o\u3007 = layoutStateWrapper.\u30078o8o\u3007(recycler);
                            if (\u30078o8o\u3007 == null) {
                                n22 = n19;
                                n23 = n21;
                                break Label_1599;
                            }
                            n13 += oo10;
                            oo\u3007O.\u300700[n10] = \u30078o8o\u3007;
                            ++n10;
                            n = n21;
                            n12 = n18;
                            n11 = n17;
                            n14 = n19;
                        }
                    }
                    n23 = n15;
                    n22 = n14;
                    n17 = n11;
                    n18 = n12;
                }
                if (n10 == 0) {
                    return;
                }
                this.Oo8Oo00oo(oo\u3007O, recycler, state, n10, n13, b, layoutManagerHelper);
                if (n23 > 0 && n10 == n13 && oo\u3007O.OoO8) {
                    if (b2) {
                        oo\u3007O.\u30070\u3007O0088o = (this.o800o8O - (n10 - 1) * oo\u3007O.\u3007oo\u3007) / n10;
                    }
                    else {
                        oo\u3007O.\u30070\u3007O0088o = (this.o800o8O - (n10 - 1) * oo\u3007O.oo88o8O) / n10;
                    }
                }
                else if (!b && n23 == 0 && n10 == n13 && oo\u3007O.OoO8) {
                    if (b2) {
                        oo\u3007O.\u30070\u3007O0088o = (this.o800o8O - (n10 - 1) * oo\u3007O.\u3007oo\u3007) / n10;
                    }
                    else {
                        oo\u3007O.\u30070\u3007O0088o = (this.o800o8O - (n10 - 1) * oo\u3007O.oo88o8O) / n10;
                    }
                }
                final boolean b4 = b2;
                boolean b5;
                if (oo\u3007O.o\u3007O8\u3007\u3007o != null && oo\u3007O.o\u3007O8\u3007\u3007o.length > 0) {
                    int n24;
                    int n25;
                    int n26;
                    if (b4) {
                        n24 = this.o800o8O;
                        n25 = n10 - 1;
                        n26 = oo\u3007O.\u3007oo\u3007;
                    }
                    else {
                        n24 = this.o800o8O;
                        n25 = n10 - 1;
                        n26 = oo\u3007O.oo88o8O;
                    }
                    final int n27 = n24 - n25 * n26;
                    int o3;
                    if (n23 > 0 && oo\u3007O.OoO8) {
                        o3 = n10;
                    }
                    else {
                        o3 = oo\u3007O.\u3007\u30078O0\u30078;
                    }
                    int n28 = n27;
                    int n29 = 0;
                    for (int k = 0; k < o3; ++k) {
                        if (k < oo\u3007O.o\u3007O8\u3007\u3007o.length && !Float.isNaN(oo\u3007O.o\u3007O8\u3007\u3007o[k]) && oo\u3007O.o\u3007O8\u3007\u3007o[k] >= 0.0f) {
                            oo\u3007O.O8ooOoo\u3007[k] = (int)(oo\u3007O.o\u3007O8\u3007\u3007o[k] * 1.0f / 100.0f * n27 + 0.5f);
                            n28 -= oo\u3007O.O8ooOoo\u3007[k];
                        }
                        else {
                            ++n29;
                            oo\u3007O.O8ooOoo\u3007[k] = -1;
                        }
                    }
                    if (n29 > 0) {
                        final int n30 = n28 / n29;
                        for (int l = 0; l < o3; ++l) {
                            if (oo\u3007O.O8ooOoo\u3007[l] < 0) {
                                oo\u3007O.O8ooOoo\u3007[l] = n30;
                            }
                        }
                    }
                    b5 = true;
                }
                else {
                    b5 = false;
                }
                int n31 = 0;
                final int n32 = 0;
                final OrientationHelperEx orientationHelperEx2 = orientationHelperEx;
                final boolean b6 = b5;
                final boolean b7 = b3 != 0;
                final int n33 = n18;
                final boolean b8 = b4;
                for (int n34 = n32; n34 < n10; ++n34) {
                    final View view2 = oo\u3007O.\u300700[n34];
                    int n35;
                    if (b) {
                        n35 = -1;
                    }
                    else {
                        n35 = 0;
                    }
                    layoutManagerHelper.\u30070\u3007O0088o(layoutStateWrapper, view2, n35);
                    final int oo11 = this.oO(oo\u3007O.\u3007O888o0o, recycler, state, layoutManagerHelper.getPosition(view2));
                    int n38;
                    if (b6) {
                        final int n36 = oo\u3007O.O\u30078O8\u3007008[n34];
                        int n37 = 0;
                        int b9 = 0;
                        while (n37 < oo11) {
                            b9 += oo\u3007O.O8ooOoo\u3007[n37 + n36];
                            ++n37;
                        }
                        n38 = View$MeasureSpec.makeMeasureSpec(Math.max(0, b9), 1073741824);
                    }
                    else {
                        final int o\u3007O = oo\u3007O.\u30070\u3007O0088o;
                        final int max = Math.max(0, oo11 - 1);
                        int n39;
                        if (b8) {
                            n39 = oo\u3007O.\u3007oo\u3007;
                        }
                        else {
                            n39 = oo\u3007O.oo88o8O;
                        }
                        n38 = View$MeasureSpec.makeMeasureSpec(o\u3007O * oo11 + max * n39, 1073741824);
                    }
                    final VirtualLayoutManager.LayoutParams layoutParams = (VirtualLayoutManager.LayoutParams)view2.getLayoutParams();
                    if (layoutManagerHelper.getOrientation() == 1) {
                        layoutManagerHelper.measureChildWithMargins(view2, n38, this.\u3007\u30070o(oo\u3007O, layoutParams.height, this.o800o8O, View$MeasureSpec.getSize(n38), layoutParams.\u3007OOo8\u30070));
                    }
                    else {
                        layoutManagerHelper.measureChildWithMargins(view2, this.\u3007\u30070o(oo\u3007O, layoutParams.width, this.o800o8O, View$MeasureSpec.getSize(n38), layoutParams.\u3007OOo8\u30070), View$MeasureSpec.getSize(n38));
                    }
                    final int oo12 = orientationHelperEx2.Oo08(view2);
                    if (oo12 > n31) {
                        n31 = oo12;
                    }
                }
                final int \u3007\u30070o = this.\u3007\u30070o(oo\u3007O, n31, this.o800o8O, 0, Float.NaN);
                for (int n40 = 0; n40 < n10; ++n40) {
                    final View view3 = oo\u3007O.\u300700[n40];
                    if (orientationHelperEx2.Oo08(view3) != n31) {
                        final int oo13 = this.oO(oo\u3007O.\u3007O888o0o, recycler, state, layoutManagerHelper.getPosition(view3));
                        int n43;
                        if (b6) {
                            final int n41 = oo\u3007O.O\u30078O8\u3007008[n40];
                            int n42 = 0;
                            int b10 = 0;
                            while (n42 < oo13) {
                                b10 += oo\u3007O.O8ooOoo\u3007[n42 + n41];
                                ++n42;
                            }
                            n43 = View$MeasureSpec.makeMeasureSpec(Math.max(0, b10), 1073741824);
                        }
                        else {
                            final int o\u3007O2 = oo\u3007O.\u30070\u3007O0088o;
                            final int max2 = Math.max(0, oo13 - 1);
                            int n44;
                            if (b8) {
                                n44 = oo\u3007O.\u3007oo\u3007;
                            }
                            else {
                                n44 = oo\u3007O.oo88o8O;
                            }
                            n43 = View$MeasureSpec.makeMeasureSpec(o\u3007O2 * oo13 + max2 * n44, 1073741824);
                        }
                        if (layoutManagerHelper.getOrientation() == 1) {
                            layoutManagerHelper.measureChildWithMargins(view3, n43, \u3007\u30070o);
                        }
                        else {
                            layoutManagerHelper.measureChildWithMargins(view3, \u3007\u30070o, n43);
                        }
                    }
                }
                final boolean b11 = layoutStateWrapper.o\u30070() == 1;
                final boolean enableMarginOverLap = layoutManagerHelper.isEnableMarginOverLap();
                int o\u3007\u30070\u3007;
                if (n17 != 0) {
                    o\u3007\u30070\u3007 = this.o\u3007\u30070\u3007(layoutManagerHelper, b8, b11, enableMarginOverLap);
                }
                else {
                    o\u3007\u30070\u3007 = 0;
                }
                int n47;
                if (n33 != 0) {
                    int n45;
                    int n46;
                    if (b8) {
                        n45 = oo\u3007O.O\u30078O8\u3007008();
                        n46 = oo\u3007O.oo\u3007();
                    }
                    else {
                        n45 = oo\u3007O.o\u3007O8\u3007\u3007o();
                        n46 = oo\u3007O.o\u3007\u30070\u3007();
                    }
                    n47 = n45 + n46;
                }
                else {
                    n47 = 0;
                }
                int m;
                if (n22 != 0) {
                    final GridRangeStyle ooO9 = this.OoO8;
                    int n48;
                    int n49;
                    if (b8) {
                        n48 = ooO9.\u3007oo\u3007();
                        n49 = this.OoO8.\u30070000OOO();
                    }
                    else {
                        n48 = ooO9.\u300700();
                        n49 = this.OoO8.OOO\u3007O0();
                    }
                    m = n48 + n49;
                }
                else {
                    m = 0;
                }
                int n52;
                if (b7) {
                    int n50;
                    int n51;
                    if (b8) {
                        n50 = oo\u3007O.\u3007oo\u3007();
                        n51 = oo\u3007O.\u30070000OOO();
                    }
                    else {
                        n50 = oo\u3007O.\u300700();
                        n51 = oo\u3007O.OOO\u3007O0();
                    }
                    n52 = n50 + n51;
                    if (RangeGridLayoutHelper.oo88o8O) {
                        final StringBuilder sb5 = new StringBuilder();
                        sb5.append("isSecondEndLineLogic:");
                        sb5.append(b7);
                        sb5.append(" pos=");
                        sb5.append(\u3007o\u3007);
                        sb5.append(" secondEndSpace=");
                        sb5.append(n52);
                    }
                }
                else {
                    n52 = 0;
                }
                layoutChunkResult.\u3007080 = n31 + o\u3007\u30070\u3007 + m + n47 + n52;
                final boolean b12 = layoutStateWrapper.o\u30070() == -1;
                final boolean \u3007o888o0o = this.\u3007O888o0o;
                String str3 = "\u2b06 ";
                int i4 = 0;
                Label_3380: {
                    if (!\u3007o888o0o) {
                        if (!b12) {
                            if (n17 == 0) {
                                int n53;
                                if (n33 != 0) {
                                    final GridRangeStyle gridRangeStyle = (GridRangeStyle)oo\u3007O.\u3007080;
                                    int i2;
                                    if (b8) {
                                        i2 = gridRangeStyle.oo88o8O;
                                    }
                                    else {
                                        i2 = gridRangeStyle.\u3007oo\u3007;
                                    }
                                    n53 = i2;
                                    if (RangeGridLayoutHelper.oo88o8O) {
                                        final StringBuilder sb6 = new StringBuilder();
                                        sb6.append("\u2b07 ");
                                        sb6.append(\u3007o\u3007);
                                        sb6.append(" 1 ");
                                        sb6.append(i2);
                                        sb6.append(" gap");
                                        n53 = i2;
                                    }
                                }
                                else {
                                    int i3;
                                    if (b8) {
                                        i3 = oo\u3007O.oo88o8O;
                                    }
                                    else {
                                        i3 = oo\u3007O.\u3007oo\u3007;
                                    }
                                    n53 = i3;
                                    if (RangeGridLayoutHelper.oo88o8O) {
                                        final StringBuilder sb7 = new StringBuilder();
                                        sb7.append("\u2b07 ");
                                        sb7.append(\u3007o\u3007);
                                        sb7.append(" 2 ");
                                        sb7.append(i3);
                                        sb7.append(" gap");
                                        n53 = i3;
                                    }
                                }
                                i4 = n53;
                                break Label_3380;
                            }
                        }
                        else if (n22 == 0) {
                            int i5;
                            if (b7) {
                                final GridRangeStyle gridRangeStyle2 = (GridRangeStyle)oo\u3007O.\u3007080;
                                if (b8) {
                                    i5 = gridRangeStyle2.oo88o8O;
                                }
                                else {
                                    i5 = gridRangeStyle2.\u3007oo\u3007;
                                }
                                if (RangeGridLayoutHelper.oo88o8O) {
                                    final StringBuilder sb8 = new StringBuilder();
                                    sb8.append("\u2b06 ");
                                    sb8.append(\u3007o\u3007);
                                    sb8.append(" 3 ");
                                    sb8.append(i5);
                                    sb8.append(" gap");
                                }
                            }
                            else {
                                int i6;
                                if (b8) {
                                    i6 = oo\u3007O.oo88o8O;
                                }
                                else {
                                    i6 = oo\u3007O.\u3007oo\u3007;
                                }
                                i5 = i6;
                                if (RangeGridLayoutHelper.oo88o8O) {
                                    final StringBuilder sb9 = new StringBuilder();
                                    sb9.append("\u2b06 ");
                                    sb9.append(\u3007o\u3007);
                                    sb9.append(" 4 ");
                                    sb9.append(i6);
                                    sb9.append(" gap");
                                    i5 = i6;
                                }
                            }
                            i4 = i5;
                            break Label_3380;
                        }
                    }
                    i4 = 0;
                }
                final int \u3007080 = layoutChunkResult.\u3007080 + i4;
                layoutChunkResult.\u3007080 = \u3007080;
                if (\u3007080 <= 0) {
                    layoutChunkResult.\u3007080 = 0;
                }
                int n61 = 0;
                Label_3705: {
                    Label_3702: {
                        if (!layoutStateWrapper.OO0o\u3007\u3007\u3007\u30070()) {
                            int n57 = 0;
                            Label_3559: {
                                if (b12) {
                                    final int n54 = \u3007o\u3007 + 1;
                                    if (!this.OO0o\u3007\u3007\u3007\u30070(n54)) {
                                        final GridRangeStyle oo\u3007O2 = this.OoO8.Oo\u3007O(n54);
                                        if (oo\u3007O2.Oo8Oo00oo(n54)) {
                                            int n55;
                                            int n56;
                                            if (b8) {
                                                n55 = oo\u3007O2.O\u30078O8\u3007008();
                                                n56 = oo\u3007O2.oo\u3007();
                                            }
                                            else {
                                                n55 = oo\u3007O2.o\u3007O8\u3007\u3007o();
                                                n56 = oo\u3007O2.o\u3007\u30070\u3007();
                                            }
                                            final int i7 = n57 = n55 + n56;
                                            if (RangeGridLayoutHelper.oo88o8O) {
                                                final StringBuilder sb10 = new StringBuilder();
                                                sb10.append("\u2b06 ");
                                                sb10.append(\u3007o\u3007);
                                                sb10.append(" 1 ");
                                                sb10.append(i7);
                                                sb10.append(" last");
                                                n57 = i7;
                                            }
                                            break Label_3559;
                                        }
                                    }
                                    n57 = 0;
                                }
                                else {
                                    final int n58 = \u3007o\u3007 - 1;
                                    if (this.OO0o\u3007\u3007\u3007\u30070(n58)) {
                                        break Label_3702;
                                    }
                                    final GridRangeStyle oo\u3007O3 = this.OoO8.Oo\u3007O(n58);
                                    if (!oo\u3007O3.\u3007\u3007\u30070\u3007\u30070(n58)) {
                                        break Label_3702;
                                    }
                                    int n59;
                                    int n60;
                                    if (b8) {
                                        n59 = oo\u3007O3.\u3007oo\u3007();
                                        n60 = oo\u3007O3.\u30070000OOO();
                                    }
                                    else {
                                        n59 = oo\u3007O3.\u300700();
                                        n60 = oo\u3007O3.OOO\u3007O0();
                                    }
                                    final int i8 = n57 = n59 + n60;
                                    if (RangeGridLayoutHelper.oo88o8O) {
                                        final StringBuilder sb11 = new StringBuilder();
                                        sb11.append("\u2b07 ");
                                        sb11.append(\u3007o\u3007);
                                        sb11.append(" 2 ");
                                        sb11.append(i8);
                                        sb11.append(" last");
                                        n57 = i8;
                                    }
                                }
                            }
                            n61 = n57;
                            break Label_3705;
                        }
                    }
                    n61 = 0;
                }
                if (RangeGridLayoutHelper.oo88o8O) {
                    final StringBuilder sb12 = new StringBuilder();
                    if (!b12) {
                        str3 = "\u2b07 ";
                    }
                    sb12.append(str3);
                    sb12.append(\u3007o\u3007);
                    sb12.append(" consumed ");
                    sb12.append(layoutChunkResult.\u3007080);
                    sb12.append(" startSpace ");
                    sb12.append(o\u3007\u30070\u3007);
                    sb12.append(" endSpace ");
                    sb12.append(m);
                    sb12.append(" secondStartSpace ");
                    sb12.append(n47);
                    sb12.append(" secondEndSpace ");
                    sb12.append(n52);
                    sb12.append(" lastUnconsumedSpace ");
                    sb12.append(n61);
                    sb12.append(" isSecondEndLine=");
                    sb12.append(b7);
                }
                int i9;
                int i10;
                int i11;
                int n64;
                if (b8) {
                    if (b12) {
                        final int n62 = layoutStateWrapper.\u3007\u3007888() - m - n52 - i4 - n61;
                        final int n63 = n62 - n31;
                        i9 = n62;
                        i10 = n63;
                    }
                    else {
                        i10 = layoutStateWrapper.\u3007\u3007888() + o\u3007\u30070\u3007 + n47 + i4 + n61;
                        i9 = i10 + n31;
                    }
                    i11 = 0;
                    n64 = 0;
                }
                else if (b12) {
                    n64 = layoutStateWrapper.\u3007\u3007888() - m - i4 - n61;
                    i11 = n64 - n31;
                    i10 = 0;
                    i9 = 0;
                }
                else {
                    final int n65 = i11 = layoutStateWrapper.\u3007\u3007888() + o\u3007\u30070\u3007 + i4 + n61;
                    final int n66 = 0;
                    n64 = n65 + n31;
                    final int n67 = 0;
                    i9 = n66;
                    i10 = n67;
                }
                int i13;
                for (int n68 = 0; n68 < n10; ++n68, n64 = i13) {
                    final View view4 = oo\u3007O.\u300700[n68];
                    final int i12 = oo\u3007O.O\u30078O8\u3007008[n68];
                    final VirtualLayoutManager.LayoutParams layoutParams2 = (VirtualLayoutManager.LayoutParams)view4.getLayoutParams();
                    if (b8) {
                        int n71;
                        if (b6) {
                            int n69 = layoutManagerHelper.getPaddingLeft() + oo\u3007O.OO0o\u3007\u3007() + oo\u3007O.\u3007O00();
                            int n70 = 0;
                            while (true) {
                                n71 = n69;
                                if (n70 >= i12) {
                                    break;
                                }
                                n69 += oo\u3007O.O8ooOoo\u3007[n70] + oo\u3007O.\u3007oo\u3007;
                                ++n70;
                            }
                        }
                        else {
                            n71 = layoutManagerHelper.getPaddingLeft() + oo\u3007O.OO0o\u3007\u3007() + oo\u3007O.\u3007O00() + oo\u3007O.\u30070\u3007O0088o * i12 + oo\u3007O.\u3007oo\u3007 * i12;
                        }
                        final int o\u30070 = orientationHelperEx2.o\u30070(view4);
                        i11 = n71;
                        i13 = n71 + o\u30070;
                    }
                    else {
                        int n74;
                        if (b6) {
                            int n72 = layoutManagerHelper.getPaddingTop() + oo\u3007O.\u3007\u3007808\u3007() + oo\u3007O.\u30070\u3007O0088o();
                            int n73 = 0;
                            while (true) {
                                n74 = n72;
                                if (n73 >= i12) {
                                    break;
                                }
                                n72 += oo\u3007O.O8ooOoo\u3007[n73] + oo\u3007O.oo88o8O;
                                ++n73;
                            }
                        }
                        else {
                            n74 = layoutManagerHelper.getPaddingTop() + oo\u3007O.\u3007\u3007808\u3007() + oo\u3007O.\u30070\u3007O0088o() + oo\u3007O.\u30070\u3007O0088o * i12 + oo\u3007O.oo88o8O * i12;
                        }
                        i9 = orientationHelperEx2.o\u30070(view4) + n74;
                        i10 = n74;
                        i13 = n64;
                    }
                    if (RangeGridLayoutHelper.oo88o8O) {
                        final StringBuilder sb13 = new StringBuilder();
                        sb13.append("layout item in position: ");
                        sb13.append(((RecyclerView.LayoutParams)layoutParams2).getViewPosition());
                        sb13.append(" with text with SpanIndex: ");
                        sb13.append(i12);
                        sb13.append(" into (");
                        sb13.append(i11);
                        sb13.append(", ");
                        sb13.append(i10);
                        sb13.append(", ");
                        sb13.append(i13);
                        sb13.append(", ");
                        sb13.append(i9);
                        sb13.append("), topInfo=[layoutState.getOffset()=");
                        sb13.append(layoutStateWrapper.\u3007\u3007888());
                        sb13.append(" startSpace=");
                        sb13.append(o\u3007\u30070\u3007);
                        sb13.append(" secondStartSpace=");
                        sb13.append(n47);
                        sb13.append(" consumedGap=");
                        sb13.append(i4);
                        sb13.append(" lastUnconsumedSpace=");
                        sb13.append(n61);
                        sb13.append("]");
                    }
                    oo\u3007O.oO(view4, i11, i10, i13, i9, layoutManagerHelper, false);
                    if (((RecyclerView.LayoutParams)layoutParams2).isItemRemoved() || ((RecyclerView.LayoutParams)layoutParams2).isItemChanged()) {
                        layoutChunkResult.\u3007o\u3007 = true;
                    }
                    layoutChunkResult.O8 |= view4.isFocusable();
                }
                this.\u3007O888o0o = false;
                Arrays.fill(oo\u3007O.\u300700, null);
                Arrays.fill(oo\u3007O.O\u30078O8\u3007008, 0);
                Arrays.fill(oo\u3007O.O8ooOoo\u3007, 0);
                return;
            }
            final String str2 = " spans.";
            final String str = " requires ";
            int n12 = 0;
            int b3 = false ? 1 : 0;
            int n13 = 0;
            int n11 = 0;
            int n10 = 0;
            int n14 = 0;
            continue Label_0844;
        }
    }
    
    @Override
    public void \u3007o00\u3007\u3007Oo(final RecyclerView.Recycler recycler, final RecyclerView.State state, final LayoutManagerHelper layoutManagerHelper) {
        this.OoO8.\u3007o00\u3007\u3007Oo(recycler, state, layoutManagerHelper);
    }
    
    @Override
    public void \u3007o\u3007(final RecyclerView.State state, final VirtualLayoutManager.AnchorInfoWrapper anchorInfoWrapper, final LayoutManagerHelper layoutManagerHelper) {
        if (state.getItemCount() > 0) {
            final GridRangeStyle oo\u3007O = this.OoO8.Oo\u3007O(anchorInfoWrapper.\u3007080);
            int i;
            int \u3007080 = i = oo\u3007O.\u3007O888o0o.\u3007080(anchorInfoWrapper.\u3007080, oo\u3007O.\u3007\u30078O0\u30078);
            if (anchorInfoWrapper.\u3007o\u3007) {
                while (\u3007080 < oo\u3007O.\u3007\u30078O0\u30078 - 1 && anchorInfoWrapper.\u3007080 < this.oO80().Oo08()) {
                    ++anchorInfoWrapper.\u3007080;
                    \u3007080 = oo\u3007O.\u3007O888o0o.\u3007080(anchorInfoWrapper.\u3007080, oo\u3007O.\u3007\u30078O0\u30078);
                }
            }
            else {
                while (i > 0) {
                    final int \u300781 = anchorInfoWrapper.\u3007080;
                    if (\u300781 <= 0) {
                        break;
                    }
                    anchorInfoWrapper.\u3007080 = \u300781 - 1;
                    i = oo\u3007O.\u3007O888o0o.\u3007080(anchorInfoWrapper.\u3007080, oo\u3007O.\u3007\u30078O0\u30078);
                }
            }
            this.\u3007O888o0o = true;
        }
    }
    
    @Override
    public void \u3007\u3007808\u3007(final int n, final int n2) {
        this.OoO8.o8oO\u3007(n, n2);
    }
    
    public int \u3007\u3007\u30070\u3007\u30070(final LayoutManagerHelper layoutManagerHelper) {
        final GridRangeStyle oo\u3007O = this.OoO8.Oo\u3007O(this.oO80().Oo08());
        int n;
        int n2;
        if (layoutManagerHelper.getOrientation() == 1) {
            n = oo\u3007O.\u3007O8o08O();
            n2 = oo\u3007O.\u3007O\u3007();
        }
        else {
            n = oo\u3007O.Oooo8o0\u3007();
            n2 = oo\u3007O.\u3007\u30078O0\u30078();
        }
        return n + n2;
    }
    
    public static class GridRangeStyle extends RangeStyle<GridRangeStyle>
    {
        private int[] O8ooOoo\u3007;
        private boolean OoO8;
        private int[] O\u30078O8\u3007008;
        private boolean o800o8O;
        private int oo88o8O;
        private float[] o\u3007O8\u3007\u3007o;
        private View[] \u300700;
        private int \u30070\u3007O0088o;
        private float \u3007O00;
        @NonNull
        private GridLayoutHelper.SpanSizeLookup \u3007O888o0o;
        private int \u3007oo\u3007;
        private int \u3007\u30078O0\u30078;
        
        public GridRangeStyle() {
            this.\u3007O00 = Float.NaN;
            this.\u3007\u30078O0\u30078 = 4;
            this.\u30070\u3007O0088o = 0;
            this.OoO8 = true;
            this.o800o8O = false;
            final GridLayoutHelper.DefaultSpanSizeLookup \u3007o888o0o = new GridLayoutHelper.DefaultSpanSizeLookup();
            this.\u3007O888o0o = \u3007o888o0o;
            this.oo88o8O = 0;
            this.\u3007oo\u3007 = 0;
            this.o\u3007O8\u3007\u3007o = new float[0];
            ((GridLayoutHelper.SpanSizeLookup)\u3007o888o0o).Oo08(true);
        }
        
        public static int O0O8OO088(GridRangeStyle gridRangeStyle, final boolean b) {
            int n;
            int n2;
            if (b) {
                n = -gridRangeStyle.\u3007O8o08O;
                n2 = gridRangeStyle.oO80;
            }
            else {
                n = -gridRangeStyle.OO0o\u3007\u3007\u3007\u30070;
                n2 = gridRangeStyle.o\u30070;
            }
            int n3 = n - n2;
            final int intValue = gridRangeStyle.O8\u3007o().O8();
            final Iterator<Map.Entry<Range<Integer>, T>> iterator = gridRangeStyle.Oo08.entrySet().iterator();
            int n4;
            while (true) {
                n4 = n3;
                if (!iterator.hasNext()) {
                    break;
                }
                gridRangeStyle = iterator.next().getValue();
                if (!gridRangeStyle.o8()) {
                    n3 += O0O8OO088(gridRangeStyle, b);
                }
                else {
                    if (gridRangeStyle.O8.O8() == intValue) {
                        int n5;
                        int n6;
                        if (b) {
                            n5 = -gridRangeStyle.\u3007O8o08O;
                            n6 = gridRangeStyle.oO80;
                        }
                        else {
                            n5 = -gridRangeStyle.OO0o\u3007\u3007\u3007\u30070;
                            n6 = gridRangeStyle.o\u30070;
                        }
                        n4 = n3 + (n5 - n6);
                        break;
                    }
                    continue;
                }
            }
            return n4;
        }
        
        public static int O8O\u3007(final GridRangeStyle gridRangeStyle, final boolean b) {
            int n;
            int n2;
            if (b) {
                n = gridRangeStyle.OO0o\u3007\u3007;
                n2 = gridRangeStyle.\u300780\u3007808\u3007O;
            }
            else {
                n = gridRangeStyle.\u30078o8o\u3007;
                n2 = gridRangeStyle.\u3007\u3007888;
            }
            int n3 = n + n2;
            final int intValue = gridRangeStyle.O8\u3007o().Oo08();
            final Iterator<Map.Entry<Range<Integer>, T>> iterator = gridRangeStyle.Oo08.entrySet().iterator();
            int n4;
            while (true) {
                n4 = n3;
                if (!iterator.hasNext()) {
                    break;
                }
                final GridRangeStyle gridRangeStyle2 = iterator.next().getValue();
                if (!gridRangeStyle2.o8()) {
                    n3 += O8O\u3007(gridRangeStyle2, b);
                }
                else {
                    if (gridRangeStyle2.O8.Oo08() == intValue) {
                        int n5;
                        int n6;
                        if (b) {
                            n5 = gridRangeStyle2.OO0o\u3007\u3007;
                            n6 = gridRangeStyle2.\u300780\u3007808\u3007O;
                        }
                        else {
                            n5 = gridRangeStyle2.\u30078o8o\u3007;
                            n6 = gridRangeStyle2.\u3007\u3007888;
                        }
                        n4 = n3 + (n5 + n6);
                        break;
                    }
                    continue;
                }
            }
            return n4;
        }
        
        private void \u3007o0O0O8() {
            final View[] \u300700 = this.\u300700;
            if (\u300700 == null || \u300700.length != this.\u3007\u30078O0\u30078) {
                this.\u300700 = new View[this.\u3007\u30078O0\u30078];
            }
            final int[] o\u30078O8\u3007008 = this.O\u30078O8\u3007008;
            if (o\u30078O8\u3007008 == null || o\u30078O8\u3007008.length != this.\u3007\u30078O0\u30078) {
                this.O\u30078O8\u3007008 = new int[this.\u3007\u30078O0\u30078];
            }
            final int[] o8ooOoo\u3007 = this.O8ooOoo\u3007;
            if (o8ooOoo\u3007 == null || o8ooOoo\u3007.length != this.\u3007\u30078O0\u30078) {
                this.O8ooOoo\u3007 = new int[this.\u3007\u30078O0\u30078];
            }
        }
        
        private GridRangeStyle \u3007\u3007o8(final GridRangeStyle gridRangeStyle, final int i) {
            for (final Map.Entry<K, GridRangeStyle> entry : gridRangeStyle.Oo08.entrySet()) {
                final GridRangeStyle gridRangeStyle2 = entry.getValue();
                final Range range = (Range)entry.getKey();
                if (!gridRangeStyle2.o8()) {
                    return this.\u3007\u3007o8(gridRangeStyle2, i);
                }
                if (range.\u3007o00\u3007\u3007Oo(i)) {
                    return gridRangeStyle2;
                }
            }
            return gridRangeStyle;
        }
        
        public GridRangeStyle Oo\u3007O(final int n) {
            return this.\u3007\u3007o8(this, n);
        }
        
        public void o8O\u3007() {
            this.\u3007O888o0o.O8();
            final Iterator<Map.Entry<Range<Integer>, T>> iterator = super.Oo08.entrySet().iterator();
            while (iterator.hasNext()) {
                ((Map.Entry<K, GridRangeStyle>)iterator.next()).getValue().o8O\u3007();
            }
        }
        
        @Override
        public void o8oO\u3007(final int n, final int n2) {
            super.o8oO\u3007(n, n2);
            this.\u3007O888o0o.o\u30070(n);
            this.\u3007O888o0o.O8();
        }
    }
}

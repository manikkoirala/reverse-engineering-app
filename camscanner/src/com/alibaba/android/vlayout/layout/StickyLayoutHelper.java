// 
// Decompiled by Procyon v0.6.0
// 

package com.alibaba.android.vlayout.layout;

import android.view.View$MeasureSpec;
import com.alibaba.android.vlayout.LayoutHelper;
import com.alibaba.android.vlayout.VirtualLayoutManager;
import com.alibaba.android.vlayout.LayoutManagerHelper;
import androidx.recyclerview.widget.RecyclerView;
import com.alibaba.android.vlayout.OrientationHelperEx;
import android.view.View;

public class StickyLayoutHelper extends FixAreaLayoutHelper
{
    private int o800o8O;
    private int oo88o8O;
    private boolean o\u3007O8\u3007\u3007o;
    private boolean \u300700;
    private boolean \u3007O888o0o;
    private View \u3007oo\u3007;
    
    public StickyLayoutHelper() {
        this(true);
    }
    
    public StickyLayoutHelper(final boolean \u3007o888o0o) {
        this.o800o8O = -1;
        this.oo88o8O = 0;
        this.\u3007oo\u3007 = null;
        this.o\u3007O8\u3007\u3007o = false;
        this.\u300700 = false;
        this.\u3007O888o0o = \u3007o888o0o;
        this.\u3007\u30078O0\u30078(1);
    }
    
    private void o\u30070OOo\u30070(final OrientationHelperEx orientationHelperEx, final RecyclerView.Recycler recycler, int i, int j, final LayoutManagerHelper layoutManagerHelper) {
        if (VirtualLayoutManager.\u3007\u3007o\u3007) {
            final StringBuilder sb = new StringBuilder();
            sb.append("abnormal pos: ");
            sb.append(this.o800o8O);
            sb.append(" start: ");
            sb.append(i);
            sb.append(" end: ");
            sb.append(j);
        }
        if (this.\u3007oo\u3007 != null) {
            if (this.\u3007O888o0o) {
                i = layoutManagerHelper.getChildCount() - 1;
                while (i >= 0) {
                    final View child = layoutManagerHelper.getChildAt(i);
                    j = layoutManagerHelper.getPosition(child);
                    if (j < this.o800o8O) {
                        i = orientationHelperEx.O8(child);
                        final LayoutHelper \u3007o\u3007 = layoutManagerHelper.\u3007o\u3007(j);
                        Label_0187: {
                            if (\u3007o\u3007 instanceof RangeGridLayoutHelper) {
                                j = ((RangeGridLayoutHelper)\u3007o\u3007).\u3007\u3007\u30070\u3007\u30070(layoutManagerHelper);
                            }
                            else {
                                j = i;
                                if (!(\u3007o\u3007 instanceof MarginLayoutHelper)) {
                                    break Label_0187;
                                }
                                final MarginLayoutHelper marginLayoutHelper = (MarginLayoutHelper)\u3007o\u3007;
                                i += marginLayoutHelper.\u3007O888o0o();
                                j = marginLayoutHelper.\u3007oo\u3007();
                            }
                            j += i;
                        }
                        if (j >= this.oo88o8O + super.OoO8.\u3007o00\u3007\u3007Oo) {
                            this.o\u3007O8\u3007\u3007o = true;
                            break;
                        }
                        break;
                    }
                    else {
                        --i;
                    }
                }
            }
            else {
                i = 0;
                while (i < layoutManagerHelper.getChildCount()) {
                    final View child2 = layoutManagerHelper.getChildAt(i);
                    j = layoutManagerHelper.getPosition(child2);
                    if (j > this.o800o8O) {
                        i = orientationHelperEx.\u3007\u3007888(child2);
                        final LayoutHelper \u3007o\u30072 = layoutManagerHelper.\u3007o\u3007(j);
                        Label_0333: {
                            if (\u3007o\u30072 instanceof RangeGridLayoutHelper) {
                                j = ((RangeGridLayoutHelper)\u3007o\u30072).o\u30070OOo\u30070(layoutManagerHelper);
                            }
                            else {
                                j = i;
                                if (!(\u3007o\u30072 instanceof MarginLayoutHelper)) {
                                    break Label_0333;
                                }
                                final MarginLayoutHelper marginLayoutHelper2 = (MarginLayoutHelper)\u3007o\u30072;
                                i -= marginLayoutHelper2.oo88o8O();
                                j = marginLayoutHelper2.o\u3007O8\u3007\u3007o();
                            }
                            j = i - j;
                        }
                        if (j >= this.oo88o8O + super.OoO8.O8) {
                            this.o\u3007O8\u3007\u3007o = true;
                            break;
                        }
                        break;
                    }
                    else {
                        ++i;
                    }
                }
            }
        }
    }
    
    private void \u300708O8o\u30070(final OrientationHelperEx orientationHelperEx, final RecyclerView.Recycler recycler, int i, int j, final LayoutManagerHelper layoutManagerHelper) {
        View \u3007oo\u3007;
        if ((\u3007oo\u3007 = this.\u3007oo\u3007) == null) {
            \u3007oo\u3007 = layoutManagerHelper.findViewByPosition(this.o800o8O);
        }
        final boolean b = layoutManagerHelper.getOrientation() == 1;
        final FixAreaAdjuster ooO8 = super.OoO8;
        int n;
        if (b) {
            n = ooO8.\u3007o00\u3007\u3007Oo;
        }
        else {
            n = ooO8.\u3007080;
        }
        final FixAreaAdjuster ooO9 = super.OoO8;
        int n2;
        if (b) {
            n2 = ooO9.O8;
        }
        else {
            n2 = ooO9.\u3007o\u3007;
        }
        final boolean \u3007o888o0o = this.\u3007O888o0o;
        int n3 = 0;
        Label_0286: {
            Label_0283: {
                if ((\u3007o888o0o && j >= this.o800o8O) || (!\u3007o888o0o && i <= this.o800o8O)) {
                    if (\u3007oo\u3007 == null) {
                        j = this.oo88o8O;
                        if (\u3007o888o0o) {
                            i = n;
                        }
                        else {
                            i = n2;
                        }
                        n3 = ((j + i >= 0) ? 1 : 0);
                        this.\u3007\u3007\u30070\u3007\u30070(this.\u3007oo\u3007 = recycler.getViewForPosition(this.o800o8O), layoutManagerHelper);
                        break Label_0286;
                    }
                    if (\u3007o888o0o && orientationHelperEx.\u3007\u3007888(\u3007oo\u3007) >= orientationHelperEx.\u30078o8o\u3007() + this.oo88o8O + n) {
                        this.\u3007oo\u3007 = \u3007oo\u3007;
                    }
                    else {
                        if (this.\u3007O888o0o || orientationHelperEx.O8(\u3007oo\u3007) > orientationHelperEx.\u300780\u3007808\u3007O() - this.oo88o8O - n2) {
                            this.\u3007oo\u3007 = \u3007oo\u3007;
                            break Label_0283;
                        }
                        this.\u3007oo\u3007 = \u3007oo\u3007;
                    }
                    n3 = 1;
                    break Label_0286;
                }
            }
            n3 = 0;
        }
        final View \u3007oo\u30072 = this.\u3007oo\u3007;
        boolean o\u3007O8\u3007\u3007o = n3 != 0;
        if (\u3007oo\u30072 != null) {
            if (((RecyclerView.LayoutParams)\u3007oo\u30072.getLayoutParams()).isItemRemoved()) {
                return;
            }
            final int oo08 = orientationHelperEx.Oo08(this.\u3007oo\u3007);
            int n4 = -1;
            int n9;
            int n10;
            if (b) {
                int n5;
                int paddingLeft;
                if (layoutManagerHelper.\u3007\u30078O0\u30078()) {
                    n5 = layoutManagerHelper.\u300780\u3007808\u3007O() - layoutManagerHelper.getPaddingRight();
                    paddingLeft = n5 - orientationHelperEx.o\u30070(this.\u3007oo\u3007);
                }
                else {
                    paddingLeft = layoutManagerHelper.getPaddingLeft();
                    n5 = orientationHelperEx.o\u30070(this.\u3007oo\u3007) + paddingLeft;
                }
                Label_0810: {
                    if (n3 != 0) {
                        View view = null;
                        int n7 = 0;
                        Label_0720: {
                            if (this.\u3007O888o0o) {
                                j = layoutManagerHelper.getChildCount() - 1;
                                view = null;
                                while (j >= 0) {
                                    view = layoutManagerHelper.getChildAt(j);
                                    i = layoutManagerHelper.getPosition(view);
                                    if (i < this.o800o8O) {
                                        int o8 = orientationHelperEx.O8(view);
                                        final LayoutHelper \u3007o\u3007 = layoutManagerHelper.\u3007o\u3007(i);
                                        Label_0534: {
                                            if (\u3007o\u3007 instanceof RangeGridLayoutHelper) {
                                                i = ((RangeGridLayoutHelper)\u3007o\u3007).\u3007\u3007\u30070\u3007\u30070(layoutManagerHelper);
                                            }
                                            else {
                                                i = o8;
                                                if (!(\u3007o\u3007 instanceof MarginLayoutHelper)) {
                                                    break Label_0534;
                                                }
                                                final MarginLayoutHelper marginLayoutHelper = (MarginLayoutHelper)\u3007o\u3007;
                                                o8 += marginLayoutHelper.\u3007O888o0o();
                                                i = marginLayoutHelper.\u3007oo\u3007();
                                            }
                                            i += o8;
                                        }
                                        final int n6 = i + oo08;
                                        n7 = j + 1;
                                        j = n6;
                                        break Label_0720;
                                    }
                                    --j;
                                }
                            }
                            else {
                                view = null;
                                int \u3007\u3007888;
                                LayoutHelper \u3007o\u30072;
                                MarginLayoutHelper marginLayoutHelper2;
                                int n8;
                                for (i = 0; i < layoutManagerHelper.getChildCount(); ++i) {
                                    view = layoutManagerHelper.getChildAt(i);
                                    j = layoutManagerHelper.getPosition(view);
                                    if (j > this.o800o8O) {
                                        \u3007\u3007888 = orientationHelperEx.\u3007\u3007888(view);
                                        \u3007o\u30072 = layoutManagerHelper.\u3007o\u3007(j);
                                        Label_0689: {
                                            if (\u3007o\u30072 instanceof RangeGridLayoutHelper) {
                                                j = ((RangeGridLayoutHelper)\u3007o\u30072).o\u30070OOo\u30070(layoutManagerHelper);
                                            }
                                            else {
                                                j = \u3007\u3007888;
                                                if (!(\u3007o\u30072 instanceof MarginLayoutHelper)) {
                                                    break Label_0689;
                                                }
                                                marginLayoutHelper2 = (MarginLayoutHelper)\u3007o\u30072;
                                                \u3007\u3007888 -= marginLayoutHelper2.oo88o8O();
                                                j = marginLayoutHelper2.o\u3007O8\u3007\u3007o();
                                            }
                                            j = \u3007\u3007888 - j;
                                        }
                                        n8 = j - oo08;
                                        n7 = i;
                                        i = n8;
                                        break Label_0720;
                                    }
                                }
                            }
                            j = 0;
                            i = 0;
                            n7 = n4;
                        }
                        if (view == null || n7 < 0) {
                            n3 = (false ? 1 : 0);
                        }
                        Label_0798: {
                            if (!layoutManagerHelper.getReverseLayout() && this.\u3007O888o0o) {
                                if (i >= orientationHelperEx.\u30078o8o\u3007() + this.oo88o8O + n) {
                                    break Label_0798;
                                }
                            }
                            else if (j <= orientationHelperEx.\u300780\u3007808\u3007O() - this.oo88o8O - n2) {
                                break Label_0798;
                            }
                            n3 = (false ? 1 : 0);
                            n4 = n7;
                            break Label_0810;
                        }
                        n4 = n7;
                    }
                    else {
                        j = 0;
                        i = 0;
                    }
                }
                if (n3 == 0) {
                    if (!layoutManagerHelper.getReverseLayout() && this.\u3007O888o0o) {
                        i = (j = orientationHelperEx.\u30078o8o\u3007() + this.oo88o8O + n);
                        n9 = oo08 + i;
                    }
                    else {
                        i = (n9 = orientationHelperEx.\u300780\u3007808\u3007O() - this.oo88o8O - n2);
                        j = i - oo08;
                    }
                }
                else {
                    n9 = j;
                    j = i;
                }
                n10 = n5;
                i = paddingLeft;
            }
            else {
                final int paddingTop = layoutManagerHelper.getPaddingTop();
                final int n11 = orientationHelperEx.o\u30070(this.\u3007oo\u3007) + paddingTop;
                if (n3 != 0) {
                    Label_1067: {
                        if (this.\u3007O888o0o) {
                            View child;
                            for (i = layoutManagerHelper.getChildCount() - 1; i >= 0; --i) {
                                child = layoutManagerHelper.getChildAt(i);
                                if (layoutManagerHelper.getPosition(child) < this.o800o8O) {
                                    j = orientationHelperEx.O8(child);
                                    i = j + oo08;
                                    break Label_1067;
                                }
                            }
                            i = 0;
                            j = 0;
                        }
                        else {
                            View child2;
                            for (i = 0; i < layoutManagerHelper.getChildCount(); ++i) {
                                child2 = layoutManagerHelper.getChildAt(i);
                                if (layoutManagerHelper.getPosition(child2) > this.o800o8O) {
                                    i = orientationHelperEx.\u3007\u3007888(child2);
                                    j = i - oo08;
                                    break Label_1067;
                                }
                            }
                            i = 0;
                            j = 0;
                        }
                    }
                    n9 = n11;
                    final int n12 = i;
                    i = j;
                    j = paddingTop;
                    n10 = n12;
                }
                else if (!layoutManagerHelper.getReverseLayout() && this.\u3007O888o0o) {
                    i = orientationHelperEx.\u30078o8o\u3007() + this.oo88o8O + n;
                    j = paddingTop;
                    n9 = n11;
                    n10 = oo08 + i;
                }
                else {
                    final int n13 = orientationHelperEx.\u300780\u3007808\u3007O() - this.oo88o8O - n2;
                    n9 = n11;
                    i = n13;
                    j = paddingTop;
                    final int n14 = n13 - oo08;
                    n10 = i;
                    i = n14;
                }
            }
            this.O8\u3007o(this.\u3007oo\u3007, i, j, n10, n9, layoutManagerHelper);
            if (n3 != 0) {
                if (n4 >= 0) {
                    if (this.\u3007oo\u3007.getParent() == null) {
                        layoutManagerHelper.\u30078o8o\u3007(this.\u3007oo\u3007, n4);
                    }
                    this.\u3007oo\u3007 = null;
                }
            }
            else {
                layoutManagerHelper.\u3007\u3007888(this.\u3007oo\u3007);
            }
            o\u3007O8\u3007\u3007o = (n3 != 0);
        }
        this.o\u3007O8\u3007\u3007o = o\u3007O8\u3007\u3007o;
    }
    
    private void \u3007\u30070o(final OrientationHelperEx orientationHelperEx, final RecyclerView.Recycler recycler, int i, int j, final LayoutManagerHelper layoutManagerHelper) {
        final boolean \u3007o888o0o = this.\u3007O888o0o;
        if ((\u3007o888o0o && j >= this.o800o8O) || (!\u3007o888o0o && i <= this.o800o8O)) {
            final int oo08 = orientationHelperEx.Oo08(this.\u3007oo\u3007);
            i = layoutManagerHelper.getOrientation();
            j = 0;
            if (i == 1) {
                i = 1;
            }
            else {
                i = 0;
            }
            final FixAreaAdjuster ooO8 = super.OoO8;
            int n;
            if (i != 0) {
                n = ooO8.\u3007o00\u3007\u3007Oo;
            }
            else {
                n = ooO8.\u3007080;
            }
            final FixAreaAdjuster ooO9 = super.OoO8;
            int n2;
            if (i != 0) {
                n2 = ooO9.O8;
            }
            else {
                n2 = ooO9.\u3007o\u3007;
            }
            int n3 = -1;
            int n10;
            int n11;
            if (i != 0) {
                int n4;
                int paddingLeft;
                if (layoutManagerHelper.\u3007\u30078O0\u30078()) {
                    n4 = layoutManagerHelper.\u300780\u3007808\u3007O() - layoutManagerHelper.getPaddingRight();
                    paddingLeft = n4 - orientationHelperEx.o\u30070(this.\u3007oo\u3007);
                }
                else {
                    paddingLeft = layoutManagerHelper.getPaddingLeft();
                    n4 = orientationHelperEx.o\u30070(this.\u3007oo\u3007) + paddingLeft;
                }
                View view = null;
                Label_0516: {
                    Label_0511: {
                        int n6 = 0;
                        Label_0490: {
                            if (this.\u3007O888o0o) {
                                i = layoutManagerHelper.getChildCount() - 1;
                                view = null;
                                while (i >= 0) {
                                    view = layoutManagerHelper.getChildAt(i);
                                    final int position = layoutManagerHelper.getPosition(view);
                                    if (position < this.o800o8O) {
                                        j = orientationHelperEx.O8(view);
                                        final LayoutHelper \u3007o\u3007 = layoutManagerHelper.\u3007o\u3007(position);
                                        Label_0324: {
                                            int n5;
                                            if (\u3007o\u3007 instanceof RangeGridLayoutHelper) {
                                                n5 = ((RangeGridLayoutHelper)\u3007o\u3007).\u3007\u3007\u30070\u3007\u30070(layoutManagerHelper);
                                            }
                                            else {
                                                n6 = j;
                                                if (!(\u3007o\u3007 instanceof MarginLayoutHelper)) {
                                                    break Label_0324;
                                                }
                                                final MarginLayoutHelper marginLayoutHelper = (MarginLayoutHelper)\u3007o\u3007;
                                                j += marginLayoutHelper.\u3007O888o0o();
                                                n5 = marginLayoutHelper.\u3007oo\u3007();
                                            }
                                            n6 = j + n5;
                                        }
                                        final int n7 = n6 + oo08;
                                        this.o\u3007O8\u3007\u3007o = true;
                                        j = i;
                                        i = n7;
                                        break Label_0490;
                                    }
                                    --i;
                                }
                                break Label_0511;
                            }
                            view = null;
                            int \u3007\u3007888;
                            LayoutHelper \u3007o\u30072;
                            MarginLayoutHelper marginLayoutHelper2;
                            for (j = 0; j < layoutManagerHelper.getChildCount(); ++j) {
                                view = layoutManagerHelper.getChildAt(j);
                                i = layoutManagerHelper.getPosition(view);
                                if (i > this.o800o8O) {
                                    \u3007\u3007888 = orientationHelperEx.\u3007\u3007888(view);
                                    \u3007o\u30072 = layoutManagerHelper.\u3007o\u3007(i);
                                    Label_0476: {
                                        if (\u3007o\u30072 instanceof RangeGridLayoutHelper) {
                                            i = ((RangeGridLayoutHelper)\u3007o\u30072).o\u30070OOo\u30070(layoutManagerHelper);
                                        }
                                        else {
                                            i = \u3007\u3007888;
                                            if (!(\u3007o\u30072 instanceof MarginLayoutHelper)) {
                                                break Label_0476;
                                            }
                                            marginLayoutHelper2 = (MarginLayoutHelper)\u3007o\u30072;
                                            \u3007\u3007888 -= marginLayoutHelper2.oo88o8O();
                                            i = marginLayoutHelper2.o\u3007O8\u3007\u3007o();
                                        }
                                        i = \u3007\u3007888 - i;
                                    }
                                    n6 = i - oo08;
                                    ++j;
                                    this.o\u3007O8\u3007\u3007o = true;
                                    break Label_0490;
                                }
                            }
                            break Label_0511;
                        }
                        final int n8 = n6;
                        n3 = j;
                        j = n8;
                        break Label_0516;
                    }
                    j = 0;
                    i = 0;
                }
                if (view == null || n3 < 0) {
                    this.o\u3007O8\u3007\u3007o = false;
                }
                if (!layoutManagerHelper.getReverseLayout() && this.\u3007O888o0o) {
                    if (j < orientationHelperEx.\u30078o8o\u3007() + this.oo88o8O + n) {
                        this.o\u3007O8\u3007\u3007o = false;
                    }
                }
                else if (i > orientationHelperEx.\u300780\u3007808\u3007O() - this.oo88o8O - n2) {
                    this.o\u3007O8\u3007\u3007o = false;
                }
                if (!this.o\u3007O8\u3007\u3007o) {
                    if (!layoutManagerHelper.getReverseLayout() && this.\u3007O888o0o) {
                        j = orientationHelperEx.\u30078o8o\u3007() + this.oo88o8O + n;
                        i = j + oo08;
                    }
                    else {
                        i = orientationHelperEx.\u300780\u3007808\u3007O() - this.oo88o8O - n2;
                        j = i - oo08;
                    }
                }
                final int n9 = i;
                i = paddingLeft;
                n10 = n4;
                n11 = n9;
            }
            else {
                final int paddingTop = layoutManagerHelper.getPaddingTop();
                final int n12 = orientationHelperEx.o\u30070(this.\u3007oo\u3007) + paddingTop;
                if (this.o\u3007O8\u3007\u3007o) {
                    Label_0838: {
                        if (this.\u3007O888o0o) {
                            View child;
                            for (i = layoutManagerHelper.getChildCount() - 1; i >= 0; --i) {
                                child = layoutManagerHelper.getChildAt(i);
                                if (layoutManagerHelper.getPosition(child) < this.o800o8O) {
                                    j = orientationHelperEx.O8(child);
                                    i = j + oo08;
                                    break Label_0838;
                                }
                            }
                        }
                        else {
                            View child2;
                            for (i = 0; i < layoutManagerHelper.getChildCount(); ++i) {
                                child2 = layoutManagerHelper.getChildAt(i);
                                if (layoutManagerHelper.getPosition(child2) > this.o800o8O) {
                                    i = orientationHelperEx.\u3007\u3007888(child2);
                                    j = i - oo08;
                                    break Label_0838;
                                }
                            }
                        }
                        i = 0;
                    }
                    final int n13 = paddingTop;
                    n11 = n12;
                    n10 = i;
                    i = j;
                    j = n13;
                }
                else if (!layoutManagerHelper.getReverseLayout() && this.\u3007O888o0o) {
                    i = orientationHelperEx.\u30078o8o\u3007() + this.oo88o8O + n;
                    final int n14 = oo08 + i;
                    j = paddingTop;
                    n11 = n12;
                    n10 = n14;
                }
                else {
                    final int n15 = i = orientationHelperEx.\u300780\u3007808\u3007O() - this.oo88o8O - n2;
                    j = paddingTop;
                    final int n16 = n15 - oo08;
                    n11 = n12;
                    n10 = i;
                    i = n16;
                }
            }
            this.O8\u3007o(this.\u3007oo\u3007, i, j, n10, n11, layoutManagerHelper);
            if (this.o\u3007O8\u3007\u3007o) {
                if (n3 >= 0) {
                    if (this.\u3007oo\u3007.getParent() == null) {
                        layoutManagerHelper.\u30078o8o\u3007(this.\u3007oo\u3007, n3);
                    }
                    this.\u3007oo\u3007 = null;
                }
            }
            else {
                layoutManagerHelper.showView(this.\u3007oo\u3007);
                layoutManagerHelper.\u3007\u3007888(this.\u3007oo\u3007);
            }
        }
        else {
            layoutManagerHelper.\u3007O8o08O(this.\u3007oo\u3007);
            layoutManagerHelper.\u3007O00(this.\u3007oo\u3007);
            this.\u3007oo\u3007 = null;
        }
    }
    
    private void \u3007\u3007\u30070\u3007\u30070(final View view, final LayoutManagerHelper layoutManagerHelper) {
        final VirtualLayoutManager.LayoutParams layoutParams = (VirtualLayoutManager.LayoutParams)view.getLayoutParams();
        final boolean b = layoutManagerHelper.getOrientation() == 1;
        final int n = layoutManagerHelper.\u300780\u3007808\u3007O() - layoutManagerHelper.getPaddingLeft() - layoutManagerHelper.getPaddingRight() - this.OoO8();
        final int n2 = layoutManagerHelper.\u3007\u3007808\u3007() - layoutManagerHelper.getPaddingTop() - layoutManagerHelper.getPaddingBottom() - this.\u300700();
        final float \u3007oOo8\u30070 = layoutParams.\u3007OOo8\u30070;
        if (b) {
            final int \u300700 = layoutManagerHelper.\u300700(n, layoutParams.width, false);
            int n3 = 0;
            Label_0199: {
                if (!Float.isNaN(\u3007oOo8\u30070) && \u3007oOo8\u30070 > 0.0f) {
                    n3 = View$MeasureSpec.makeMeasureSpec((int)(n / \u3007oOo8\u30070 + 0.5f), 1073741824);
                }
                else {
                    if (!Float.isNaN(super.\u3007O00)) {
                        final float \u3007o00 = super.\u3007O00;
                        if (\u3007o00 > 0.0f) {
                            n3 = View$MeasureSpec.makeMeasureSpec((int)(n / \u3007o00 + 0.5), 1073741824);
                            break Label_0199;
                        }
                    }
                    n3 = layoutManagerHelper.\u300700(n2, layoutParams.height, true);
                }
            }
            layoutManagerHelper.measureChildWithMargins(view, \u300700, n3);
        }
        else {
            final int \u30072 = layoutManagerHelper.\u300700(n2, layoutParams.height, false);
            int n4 = 0;
            Label_0321: {
                if (!Float.isNaN(\u3007oOo8\u30070) && \u3007oOo8\u30070 > 0.0f) {
                    n4 = View$MeasureSpec.makeMeasureSpec((int)(n2 * \u3007oOo8\u30070 + 0.5), 1073741824);
                }
                else {
                    if (!Float.isNaN(super.\u3007O00)) {
                        final float \u3007o2 = super.\u3007O00;
                        if (\u3007o2 > 0.0f) {
                            n4 = View$MeasureSpec.makeMeasureSpec((int)(n2 * \u3007o2 + 0.5), 1073741824);
                            break Label_0321;
                        }
                    }
                    n4 = layoutManagerHelper.\u300700(n, layoutParams.width, true);
                }
            }
            layoutManagerHelper.measureChildWithMargins(view, n4, \u30072);
        }
    }
    
    @Override
    public boolean o8() {
        return false;
    }
    
    public void oO(final int oo88o8O) {
        this.oo88o8O = oo88o8O;
    }
    
    public void o\u30078(final LayoutManagerHelper layoutManagerHelper) {
        super.o\u30078(layoutManagerHelper);
        final View \u3007oo\u3007 = this.\u3007oo\u3007;
        if (\u3007oo\u3007 != null) {
            layoutManagerHelper.\u3007O00(\u3007oo\u3007);
            layoutManagerHelper.\u3007O8o08O(this.\u3007oo\u3007);
            this.\u3007oo\u3007 = null;
        }
    }
    
    @Override
    public void \u3007080(final RecyclerView.Recycler recycler, final RecyclerView.State state, final int n, final int n2, int o800o8O, final LayoutManagerHelper layoutManagerHelper) {
        super.\u3007080(recycler, state, n, n2, o800o8O, layoutManagerHelper);
        if (this.o800o8O < 0) {
            return;
        }
        final OrientationHelperEx oooo8o0\u3007 = layoutManagerHelper.Oooo8o0\u3007();
        if (!this.o\u3007O8\u3007\u3007o) {
            o800o8O = this.o800o8O;
            if (o800o8O >= n && o800o8O <= n2) {
                this.o\u30070OOo\u30070(oooo8o0\u3007, recycler, n, n2, layoutManagerHelper);
            }
        }
        if (this.o\u3007O8\u3007\u3007o || state.isPreLayout()) {
            state.isPreLayout();
            final View \u3007oo\u3007 = this.\u3007oo\u3007;
            if (\u3007oo\u3007 == null) {
                return;
            }
            layoutManagerHelper.\u3007O8o08O(\u3007oo\u3007);
        }
        final View \u3007oo\u30072 = this.\u3007oo\u3007;
        if (!this.o\u3007O8\u3007\u3007o && \u3007oo\u30072 != null) {
            if (\u3007oo\u30072.getParent() == null) {
                layoutManagerHelper.\u3007\u3007888(this.\u3007oo\u3007);
            }
            else {
                this.\u3007\u30070o(oooo8o0\u3007, recycler, n, n2, layoutManagerHelper);
            }
        }
        else {
            this.\u300708O8o\u30070(oooo8o0\u3007, recycler, n, n2, layoutManagerHelper);
        }
    }
    
    @Override
    public void \u3007o(final RecyclerView.Recycler recycler, final RecyclerView.State state, final VirtualLayoutManager.LayoutStateWrapper layoutStateWrapper, final LayoutChunkResult layoutChunkResult, final LayoutManagerHelper layoutManagerHelper) {
        if (this.OO0o\u3007\u3007\u3007\u30070(layoutStateWrapper.\u3007o\u3007())) {
            return;
        }
        final View \u3007oo\u3007 = this.\u3007oo\u3007;
        View \u30078o8o\u3007;
        if (\u3007oo\u3007 == null) {
            \u30078o8o\u3007 = layoutStateWrapper.\u30078o8o\u3007(recycler);
        }
        else {
            layoutStateWrapper.OO0o\u3007\u3007();
            \u30078o8o\u3007 = \u3007oo\u3007;
        }
        if (\u30078o8o\u3007 == null) {
            layoutChunkResult.\u3007o00\u3007\u3007Oo = true;
            return;
        }
        this.\u3007\u3007\u30070\u3007\u30070(\u30078o8o\u3007, layoutManagerHelper);
        final boolean b = layoutManagerHelper.getOrientation() == 1;
        final OrientationHelperEx oooo8o0\u3007 = layoutManagerHelper.Oooo8o0\u3007();
        layoutChunkResult.\u3007080 = oooo8o0\u3007.Oo08(\u30078o8o\u3007);
        final RecyclerView.LayoutParams layoutParams = (RecyclerView.LayoutParams)\u30078o8o\u3007.getLayoutParams();
        this.o\u3007O8\u3007\u3007o = true;
        final int i = layoutStateWrapper.\u3007o00\u3007\u3007Oo() - layoutChunkResult.\u3007080 + layoutStateWrapper.O8();
        int \u3007080;
        int n;
        int n2;
        int paddingTop;
        if (layoutManagerHelper.getOrientation() == 1) {
            if (layoutManagerHelper.\u3007\u30078O0\u30078()) {
                \u3007080 = layoutManagerHelper.\u300780\u3007808\u3007O() - layoutManagerHelper.getPaddingRight() - super.\u30078o8o\u3007;
                n = \u3007080 - oooo8o0\u3007.o\u30070(\u30078o8o\u3007);
            }
            else {
                n = super.OO0o\u3007\u3007\u3007\u30070 + layoutManagerHelper.getPaddingLeft();
                \u3007080 = oooo8o0\u3007.o\u30070(\u30078o8o\u3007) + n;
            }
            if (layoutStateWrapper.o\u30070() == -1) {
                n2 = layoutStateWrapper.\u3007\u3007888() - super.OO0o\u3007\u3007;
                paddingTop = layoutStateWrapper.\u3007\u3007888() - layoutChunkResult.\u3007080;
            }
            else if (this.\u3007O888o0o) {
                paddingTop = super.\u3007O8o08O + layoutStateWrapper.\u3007\u3007888();
                n2 = layoutStateWrapper.\u3007\u3007888() + layoutChunkResult.\u3007080;
            }
            else {
                n2 = oooo8o0\u3007.\u300780\u3007808\u3007O() - super.OO0o\u3007\u3007 - this.oo88o8O - super.OoO8.O8;
                paddingTop = n2 - layoutChunkResult.\u3007080;
            }
            if (!layoutManagerHelper.getReverseLayout() && this.\u3007O888o0o) {
                if ((i < this.oo88o8O + super.OoO8.\u3007o00\u3007\u3007Oo && layoutStateWrapper.Oo08() == -1) || paddingTop < super.\u3007O8o08O + this.oo88o8O + super.OoO8.\u3007o00\u3007\u3007Oo) {
                    this.o\u3007O8\u3007\u3007o = false;
                    this.\u3007oo\u3007 = \u30078o8o\u3007;
                    final int n3 = oooo8o0\u3007.\u30078o8o\u3007() + super.\u3007O8o08O + this.oo88o8O + super.OoO8.\u3007o00\u3007\u3007Oo;
                    final int \u300781 = layoutChunkResult.\u3007080;
                    paddingTop = n3;
                    n2 = \u300781 + n3;
                }
                else if (VirtualLayoutManager.\u3007\u3007o\u3007) {
                    final StringBuilder sb = new StringBuilder();
                    sb.append("remainingSpace: ");
                    sb.append(i);
                    sb.append("    offset: ");
                    sb.append(this.oo88o8O);
                }
            }
            else if ((i < this.oo88o8O + super.OoO8.O8 && layoutStateWrapper.Oo08() == 1) || n2 > super.OO0o\u3007\u3007 + this.oo88o8O + super.OoO8.O8) {
                this.o\u3007O8\u3007\u3007o = false;
                this.\u3007oo\u3007 = \u30078o8o\u3007;
                final int n4 = oooo8o0\u3007.\u300780\u3007808\u3007O() - super.OO0o\u3007\u3007 - this.oo88o8O - super.OoO8.O8;
                final int \u300782 = layoutChunkResult.\u3007080;
                n2 = n4;
                paddingTop = n4 - \u300782;
            }
        }
        else {
            paddingTop = layoutManagerHelper.getPaddingTop();
            n2 = oooo8o0\u3007.o\u30070(\u30078o8o\u3007) + paddingTop + super.\u3007O8o08O;
            if (layoutStateWrapper.o\u30070() == -1) {
                \u3007080 = layoutStateWrapper.\u3007\u3007888() - super.\u30078o8o\u3007;
                n = layoutStateWrapper.\u3007\u3007888() - layoutChunkResult.\u3007080;
            }
            else {
                n = super.OO0o\u3007\u3007\u3007\u30070 + layoutStateWrapper.\u3007\u3007888();
                \u3007080 = layoutStateWrapper.\u3007\u3007888() + layoutChunkResult.\u3007080;
            }
            if (!layoutManagerHelper.getReverseLayout() && this.\u3007O888o0o) {
                if (i < this.oo88o8O + super.OoO8.\u3007080) {
                    this.o\u3007O8\u3007\u3007o = false;
                    this.\u3007oo\u3007 = \u30078o8o\u3007;
                    final int \u30078o8o\u30072 = oooo8o0\u3007.\u30078o8o\u3007();
                    final int oo88o8O = this.oo88o8O;
                    final int \u300783 = super.OoO8.\u3007080;
                    \u3007080 = layoutChunkResult.\u3007080;
                    n = \u30078o8o\u30072 + oo88o8O + \u300783;
                }
            }
            else if (i < this.oo88o8O + super.OoO8.\u3007o\u3007) {
                this.o\u3007O8\u3007\u3007o = false;
                this.\u3007oo\u3007 = \u30078o8o\u3007;
                final int n5 = oooo8o0\u3007.\u300780\u3007808\u3007O() - this.oo88o8O - super.OoO8.\u3007o\u3007;
                final int \u300784 = layoutChunkResult.\u3007080;
                \u3007080 = n5;
                n = n5 - \u300784;
            }
        }
        this.O8\u3007o(\u30078o8o\u3007, n, paddingTop, \u3007080, n2, layoutManagerHelper);
        final int \u300785 = layoutChunkResult.\u3007080;
        int n6;
        if (b) {
            n6 = this.\u300700();
        }
        else {
            n6 = this.OoO8();
        }
        layoutChunkResult.\u3007080 = \u300785 + n6;
        if (state.isPreLayout()) {
            this.o\u3007O8\u3007\u3007o = true;
        }
        if (this.o\u3007O8\u3007\u3007o) {
            layoutManagerHelper.oo88o8O(layoutStateWrapper, \u30078o8o\u3007);
            this.OOO\u3007O0(layoutChunkResult, \u30078o8o\u3007);
            this.\u3007oo\u3007 = null;
        }
    }
    
    @Override
    public void \u3007o00\u3007\u3007Oo(final RecyclerView.Recycler recycler, final RecyclerView.State state, final LayoutManagerHelper layoutManagerHelper) {
        super.\u3007o00\u3007\u3007Oo(recycler, state, layoutManagerHelper);
        final View \u3007oo\u3007 = this.\u3007oo\u3007;
        if (\u3007oo\u3007 != null && layoutManagerHelper.\u3007o00\u3007\u3007Oo(\u3007oo\u3007)) {
            layoutManagerHelper.\u3007O8o08O(this.\u3007oo\u3007);
            recycler.recycleView(this.\u3007oo\u3007);
            this.\u3007oo\u3007 = null;
        }
        this.o\u3007O8\u3007\u3007o = false;
    }
    
    @Override
    public void \u3007\u3007808\u3007(final int o800o8O, final int n) {
        this.o800o8O = o800o8O;
    }
    
    @Override
    public void \u3007\u30078O0\u30078(final int n) {
        if (n > 0) {
            super.\u3007\u30078O0\u30078(1);
        }
        else {
            super.\u3007\u30078O0\u30078(0);
        }
    }
}

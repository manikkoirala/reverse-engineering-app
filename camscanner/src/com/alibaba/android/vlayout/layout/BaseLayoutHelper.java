// 
// Decompiled by Procyon v0.6.0
// 

package com.alibaba.android.vlayout.layout;

import com.alibaba.android.vlayout.LayoutHelper;
import androidx.annotation.Nullable;
import com.alibaba.android.vlayout.VirtualLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.View$MeasureSpec;
import androidx.annotation.NonNull;
import com.alibaba.android.vlayout.LayoutManagerHelper;
import android.view.View;
import android.graphics.Rect;

public abstract class BaseLayoutHelper extends MarginLayoutHelper
{
    public static boolean \u30070\u3007O0088o = false;
    protected Rect Oooo8o0\u3007;
    float \u3007O00;
    int \u3007O\u3007;
    View \u3007\u3007808\u3007;
    private int \u3007\u30078O0\u30078;
    
    public BaseLayoutHelper() {
        this.Oooo8o0\u3007 = new Rect();
        this.\u3007O00 = Float.NaN;
        this.\u3007\u30078O0\u30078 = 0;
    }
    
    private int \u3007oOO8O8(final int n, final int n2) {
        if (n < n2) {
            return n2 - n;
        }
        return 0;
    }
    
    @Override
    public final void O8(final LayoutManagerHelper layoutManagerHelper) {
        final View \u3007\u3007808\u3007 = this.\u3007\u3007808\u3007;
        if (\u3007\u3007808\u3007 != null) {
            layoutManagerHelper.\u3007O8o08O(\u3007\u3007808\u3007);
            this.\u3007\u3007808\u3007 = null;
        }
        this.o\u30078(layoutManagerHelper);
    }
    
    public void O8ooOoo\u3007(@NonNull final View view) {
        view.measure(View$MeasureSpec.makeMeasureSpec(this.Oooo8o0\u3007.width(), 1073741824), View$MeasureSpec.makeMeasureSpec(this.Oooo8o0\u3007.height(), 1073741824));
        final Rect oooo8o0\u3007 = this.Oooo8o0\u3007;
        view.layout(oooo8o0\u3007.left, oooo8o0\u3007.top, oooo8o0\u3007.right, oooo8o0\u3007.bottom);
        view.setBackgroundColor(this.\u3007O\u3007);
        this.Oooo8o0\u3007.set(0, 0, 0, 0);
    }
    
    protected void O8\u3007o(final View view, final int n, final int n2, final int n3, final int n4, @NonNull final LayoutManagerHelper layoutManagerHelper) {
        this.\u300700\u30078(view, n, n2, n3, n4, layoutManagerHelper, false);
    }
    
    protected void OOO\u3007O0(final LayoutChunkResult layoutChunkResult, final View view) {
        if (view == null) {
            return;
        }
        final RecyclerView.LayoutParams layoutParams = (RecyclerView.LayoutParams)view.getLayoutParams();
        final boolean itemRemoved = layoutParams.isItemRemoved();
        final boolean b = true;
        if (itemRemoved || layoutParams.isItemChanged()) {
            layoutChunkResult.\u3007o\u3007 = true;
        }
        boolean o8 = b;
        if (!layoutChunkResult.O8) {
            o8 = (view.isFocusable() && b);
        }
        layoutChunkResult.O8 = o8;
    }
    
    @Nullable
    public final View o0ooO(final RecyclerView.Recycler recycler, final VirtualLayoutManager.LayoutStateWrapper layoutStateWrapper, final LayoutManagerHelper layoutManagerHelper, final LayoutChunkResult layoutChunkResult) {
        final View \u30078o8o\u3007 = layoutStateWrapper.\u30078o8o\u3007(recycler);
        if (\u30078o8o\u3007 != null) {
            layoutManagerHelper.oo88o8O(layoutStateWrapper, \u30078o8o\u3007);
            return \u30078o8o\u3007;
        }
        if (BaseLayoutHelper.\u30070\u3007O0088o && !layoutStateWrapper.\u300780\u3007808\u3007O()) {
            throw new RuntimeException("received null view when unexpected");
        }
        layoutChunkResult.\u3007o00\u3007\u3007Oo = true;
        return null;
    }
    
    public boolean o8() {
        return this.\u3007O\u3007 != 0;
    }
    
    protected boolean oo\u3007(final int n) {
        return n != Integer.MAX_VALUE && n != Integer.MIN_VALUE;
    }
    
    @Override
    public void o\u30070(final RecyclerView.Recycler recycler, final RecyclerView.State state, final VirtualLayoutManager.LayoutStateWrapper layoutStateWrapper, final LayoutChunkResult layoutChunkResult, final LayoutManagerHelper layoutManagerHelper) {
        this.\u3007o(recycler, state, layoutStateWrapper, layoutChunkResult, layoutManagerHelper);
    }
    
    protected void o\u30078(final LayoutManagerHelper layoutManagerHelper) {
    }
    
    protected int o\u3007\u30070\u3007(final LayoutManagerHelper layoutManagerHelper, final boolean b, final boolean b2, final boolean b3) {
        final boolean b4 = layoutManagerHelper instanceof VirtualLayoutManager;
        final MarginLayoutHelper marginLayoutHelper = null;
        LayoutHelper \u3007oOO8O8;
        if (b4) {
            \u3007oOO8O8 = ((VirtualLayoutManager)layoutManagerHelper).\u3007oOO8O8(this, b2);
        }
        else {
            \u3007oOO8O8 = null;
        }
        MarginLayoutHelper marginLayoutHelper2 = marginLayoutHelper;
        if (\u3007oOO8O8 != null) {
            marginLayoutHelper2 = marginLayoutHelper;
            if (\u3007oOO8O8 instanceof MarginLayoutHelper) {
                marginLayoutHelper2 = (MarginLayoutHelper)\u3007oOO8O8;
            }
        }
        if (\u3007oOO8O8 == this) {
            return 0;
        }
        int n3;
        if (!b3) {
            int n;
            int n2;
            if (b) {
                n = super.\u3007O8o08O;
                n2 = super.oO80;
            }
            else {
                n = super.OO0o\u3007\u3007\u3007\u30070;
                n2 = super.o\u30070;
            }
            n3 = n + n2;
        }
        else {
            int n6;
            if (marginLayoutHelper2 == null) {
                int n4;
                int n5;
                if (b) {
                    n4 = super.\u3007O8o08O;
                    n5 = super.oO80;
                }
                else {
                    n4 = super.OO0o\u3007\u3007\u3007\u30070;
                    n5 = super.o\u30070;
                }
                n6 = n4 + n5;
            }
            else if (b) {
                int n7;
                int n8;
                if (b2) {
                    n7 = marginLayoutHelper2.OO0o\u3007\u3007;
                    n8 = super.\u3007O8o08O;
                }
                else {
                    n7 = marginLayoutHelper2.\u3007O8o08O;
                    n8 = super.OO0o\u3007\u3007;
                }
                n6 = this.\u3007oOO8O8(n7, n8);
            }
            else {
                int n9;
                int n10;
                if (b2) {
                    n9 = marginLayoutHelper2.\u30078o8o\u3007;
                    n10 = super.OO0o\u3007\u3007\u3007\u30070;
                }
                else {
                    n9 = marginLayoutHelper2.OO0o\u3007\u3007\u3007\u30070;
                    n10 = super.\u30078o8o\u3007;
                }
                n6 = this.\u3007oOO8O8(n9, n10);
            }
            int n11;
            if (b) {
                if (b2) {
                    n11 = super.oO80;
                }
                else {
                    n11 = super.\u300780\u3007808\u3007O;
                }
            }
            else if (b2) {
                n11 = super.o\u30070;
            }
            else {
                n11 = super.\u3007\u3007888;
            }
            n3 = n6 + (n11 + 0);
        }
        return n3;
    }
    
    protected int \u30070000OOO(final LayoutManagerHelper layoutManagerHelper, final boolean b, final boolean b2, final boolean b3) {
        int n;
        int n2;
        if (b) {
            n = super.OO0o\u3007\u3007;
            n2 = super.\u300780\u3007808\u3007O;
        }
        else {
            n = super.OO0o\u3007\u3007\u3007\u30070;
            n2 = super.o\u30070;
        }
        return n + n2;
    }
    
    protected void \u300700\u30078(final View view, final int n, final int n2, final int n3, final int n4, @NonNull final LayoutManagerHelper layoutManagerHelper, final boolean b) {
        layoutManagerHelper.OO0o\u3007\u3007(view, n, n2, n3, n4);
        if (this.o8()) {
            if (b) {
                this.Oooo8o0\u3007.union(n - super.o\u30070 - super.OO0o\u3007\u3007\u3007\u30070, n2 - super.oO80 - super.\u3007O8o08O, n3 + super.\u3007\u3007888 + super.\u30078o8o\u3007, n4 + super.\u300780\u3007808\u3007O + super.OO0o\u3007\u3007);
            }
            else {
                this.Oooo8o0\u3007.union(n - super.o\u30070, n2 - super.oO80, n3 + super.\u3007\u3007888, n4 + super.\u300780\u3007808\u3007O);
            }
        }
    }
    
    @Override
    public void \u3007080(final RecyclerView.Recycler recycler, final RecyclerView.State state, int \u300780\u3007808\u3007O, int \u3007\u3007808\u3007, final int n, final LayoutManagerHelper layoutManagerHelper) {
        if (BaseLayoutHelper.\u30070\u3007O0088o) {
            final StringBuilder sb = new StringBuilder();
            sb.append("call afterLayout() on ");
            sb.append(this.getClass().getSimpleName());
        }
        if (this.o8()) {
            if (this.oo\u3007(n)) {
                final View \u3007\u3007808\u30072 = this.\u3007\u3007808\u3007;
                if (\u3007\u3007808\u30072 != null) {
                    this.Oooo8o0\u3007.union(\u3007\u3007808\u30072.getLeft(), this.\u3007\u3007808\u3007.getTop(), this.\u3007\u3007808\u3007.getRight(), this.\u3007\u3007808\u3007.getBottom());
                }
            }
            if (!this.Oooo8o0\u3007.isEmpty()) {
                if (this.oo\u3007(n)) {
                    if (layoutManagerHelper.getOrientation() == 1) {
                        this.Oooo8o0\u3007.offset(0, -n);
                    }
                    else {
                        this.Oooo8o0\u3007.offset(-n, 0);
                    }
                }
                \u300780\u3007808\u3007O = layoutManagerHelper.\u300780\u3007808\u3007O();
                \u3007\u3007808\u3007 = layoutManagerHelper.\u3007\u3007808\u3007();
                Label_0366: {
                    if (layoutManagerHelper.getOrientation() == 1) {
                        if (!this.Oooo8o0\u3007.intersects(0, -\u3007\u3007808\u3007 / 4, \u300780\u3007808\u3007O, \u3007\u3007808\u3007 + \u3007\u3007808\u3007 / 4)) {
                            break Label_0366;
                        }
                    }
                    else if (!this.Oooo8o0\u3007.intersects(-\u300780\u3007808\u3007O / 4, 0, \u300780\u3007808\u3007O + \u300780\u3007808\u3007O / 4, \u3007\u3007808\u3007)) {
                        break Label_0366;
                    }
                    if (this.\u3007\u3007808\u3007 == null) {
                        layoutManagerHelper.OoO8(this.\u3007\u3007808\u3007 = layoutManagerHelper.o\u3007O8\u3007\u3007o(), true);
                    }
                    if (layoutManagerHelper.getOrientation() == 1) {
                        this.Oooo8o0\u3007.left = layoutManagerHelper.getPaddingLeft() + super.OO0o\u3007\u3007\u3007\u30070;
                        this.Oooo8o0\u3007.right = layoutManagerHelper.\u300780\u3007808\u3007O() - layoutManagerHelper.getPaddingRight() - super.\u30078o8o\u3007;
                    }
                    else {
                        this.Oooo8o0\u3007.top = layoutManagerHelper.getPaddingTop() + super.\u3007O8o08O;
                        this.Oooo8o0\u3007.bottom = layoutManagerHelper.\u300780\u3007808\u3007O() - layoutManagerHelper.getPaddingBottom() - super.OO0o\u3007\u3007;
                    }
                    this.O8ooOoo\u3007(this.\u3007\u3007808\u3007);
                    return;
                }
                this.Oooo8o0\u3007.set(0, 0, 0, 0);
                final View \u3007\u3007808\u30073 = this.\u3007\u3007808\u3007;
                if (\u3007\u3007808\u30073 != null) {
                    \u3007\u3007808\u30073.layout(0, 0, 0, 0);
                }
            }
        }
        final View \u3007\u3007808\u30074 = this.\u3007\u3007808\u3007;
        if (\u3007\u3007808\u30074 != null) {
            layoutManagerHelper.\u3007O8o08O(\u3007\u3007808\u30074);
            this.\u3007\u3007808\u3007 = null;
        }
    }
    
    @Override
    public boolean \u300780\u3007808\u3007O() {
        return false;
    }
    
    public abstract void \u3007o(final RecyclerView.Recycler p0, final RecyclerView.State p1, final VirtualLayoutManager.LayoutStateWrapper p2, final LayoutChunkResult p3, final LayoutManagerHelper p4);
    
    @Override
    public void \u3007o00\u3007\u3007Oo(final RecyclerView.Recycler recycler, final RecyclerView.State state, final LayoutManagerHelper layoutManagerHelper) {
        if (BaseLayoutHelper.\u30070\u3007O0088o) {
            final StringBuilder sb = new StringBuilder();
            sb.append("call beforeLayout() on ");
            sb.append(this.getClass().getSimpleName());
        }
        if (!this.o8()) {
            final View \u3007\u3007808\u3007 = this.\u3007\u3007808\u3007;
            if (\u3007\u3007808\u3007 != null) {
                layoutManagerHelper.\u3007O8o08O(\u3007\u3007808\u3007);
                this.\u3007\u3007808\u3007 = null;
            }
        }
    }
    
    @Override
    public int \u3007\u3007888() {
        return this.\u3007\u30078O0\u30078;
    }
    
    @Override
    public void \u3007\u30078O0\u30078(final int \u3007\u30078O0\u30078) {
        this.\u3007\u30078O0\u30078 = \u3007\u30078O0\u30078;
    }
}

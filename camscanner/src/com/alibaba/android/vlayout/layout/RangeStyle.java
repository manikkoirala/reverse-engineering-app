// 
// Decompiled by Procyon v0.6.0
// 

package com.alibaba.android.vlayout.layout;

import android.view.View$MeasureSpec;
import androidx.recyclerview.widget.RecyclerView;
import androidx.annotation.NonNull;
import java.util.Iterator;
import java.util.Map;
import com.alibaba.android.vlayout.LayoutManagerHelper;
import android.view.View;
import android.graphics.Rect;
import java.util.HashMap;
import com.alibaba.android.vlayout.Range;

public class RangeStyle<T extends RangeStyle>
{
    protected Range<Integer> O8;
    protected int OO0o\u3007\u3007;
    protected int OO0o\u3007\u3007\u3007\u30070;
    protected HashMap<Range<Integer>, T> Oo08;
    protected Rect Oooo8o0\u3007;
    protected int oO80;
    protected int o\u30070;
    protected T \u3007080;
    protected int \u300780\u3007808\u3007O;
    protected int \u30078o8o\u3007;
    protected int \u3007O8o08O;
    private int \u3007O\u3007;
    private int \u3007o00\u3007\u3007Oo;
    private int \u3007o\u3007;
    private View \u3007\u3007808\u3007;
    protected int \u3007\u3007888;
    
    public RangeStyle() {
        this.\u3007o00\u3007\u3007Oo = 0;
        this.\u3007o\u3007 = 0;
        this.Oo08 = new HashMap<Range<Integer>, T>();
        this.Oooo8o0\u3007 = new Rect();
    }
    
    private void O08000(final LayoutManagerHelper layoutManagerHelper, final RangeStyle<T> rangeStyle) {
        if (!rangeStyle.o8()) {
            final Iterator<Map.Entry<Range<Integer>, T>> iterator = rangeStyle.Oo08.entrySet().iterator();
            while (iterator.hasNext()) {
                this.O08000(layoutManagerHelper, ((Map.Entry<K, RangeStyle<T>>)iterator.next()).getValue());
            }
        }
        final View \u3007\u3007808\u3007 = rangeStyle.\u3007\u3007808\u3007;
        if (\u3007\u3007808\u3007 != null) {
            layoutManagerHelper.\u3007O8o08O(\u3007\u3007808\u3007);
            rangeStyle.\u3007\u3007808\u3007 = null;
        }
    }
    
    private void O8(final LayoutManagerHelper layoutManagerHelper, final RangeStyle<T> rangeStyle) {
        final View \u3007\u3007808\u3007 = rangeStyle.\u3007\u3007808\u3007;
        if (\u3007\u3007808\u3007 != null) {
            layoutManagerHelper.\u3007O8o08O(\u3007\u3007808\u3007);
            rangeStyle.\u3007\u3007808\u3007 = null;
        }
        if (rangeStyle.Oo08.isEmpty()) {
            return;
        }
        final Iterator<Map.Entry<Range<Integer>, T>> iterator = rangeStyle.Oo08.entrySet().iterator();
        while (iterator.hasNext()) {
            this.O8(layoutManagerHelper, ((Map.Entry<K, RangeStyle<T>>)iterator.next()).getValue());
        }
    }
    
    private void o0ooO(final LayoutManagerHelper layoutManagerHelper, final RangeStyle<T> rangeStyle) {
        final Iterator<Map.Entry<Range<Integer>, T>> iterator = rangeStyle.Oo08.entrySet().iterator();
        while (iterator.hasNext()) {
            final RangeStyle rangeStyle2 = ((Map.Entry<K, RangeStyle>)iterator.next()).getValue();
            if (!rangeStyle2.o8()) {
                this.o0ooO(layoutManagerHelper, rangeStyle2);
            }
            final View \u3007\u3007808\u3007 = rangeStyle2.\u3007\u3007808\u3007;
            if (\u3007\u3007808\u3007 != null) {
                layoutManagerHelper.hideView(\u3007\u3007808\u3007);
            }
        }
    }
    
    private void o\u30078(final LayoutManagerHelper layoutManagerHelper) {
        if (this.\u3007\u30070o()) {
            this.o0ooO(layoutManagerHelper, this);
            final View \u3007\u3007808\u3007 = this.\u3007\u3007808\u3007;
            if (\u3007\u3007808\u3007 != null) {
                layoutManagerHelper.hideView(\u3007\u3007808\u3007);
            }
        }
    }
    
    private void o\u30078oOO88(final RangeStyle<T> rangeStyle) {
        if (!rangeStyle.o8()) {
            final Iterator<Map.Entry<Range<Integer>, T>> iterator = rangeStyle.Oo08.entrySet().iterator();
            while (iterator.hasNext()) {
                final RangeStyle rangeStyle2 = ((Map.Entry<K, RangeStyle>)iterator.next()).getValue();
                this.o\u30078oOO88(rangeStyle2);
                final View \u3007\u3007808\u3007 = rangeStyle2.\u3007\u3007808\u3007;
                if (\u3007\u3007808\u3007 != null) {
                    rangeStyle.Oooo8o0\u3007.union(\u3007\u3007808\u3007.getLeft(), rangeStyle2.\u3007\u3007808\u3007.getTop(), rangeStyle2.\u3007\u3007808\u3007.getRight(), rangeStyle2.\u3007\u3007808\u3007.getBottom());
                }
            }
        }
    }
    
    private boolean \u300708O8o\u30070(final int n) {
        return n != Integer.MAX_VALUE && n != Integer.MIN_VALUE;
    }
    
    private boolean \u30078\u30070\u3007o\u3007O(final RangeStyle<T> rangeStyle) {
        boolean b = rangeStyle.\u3007O\u3007 != 0;
        final Iterator<Map.Entry<Range<Integer>, T>> iterator = rangeStyle.Oo08.entrySet().iterator();
        while (iterator.hasNext()) {
            final RangeStyle rangeStyle2 = ((Map.Entry<K, RangeStyle>)iterator.next()).getValue();
            if (rangeStyle2.o8()) {
                return rangeStyle2.O\u3007O\u3007oO();
            }
            b |= this.\u30078\u30070\u3007o\u3007O(rangeStyle2);
        }
        return b;
    }
    
    public int O8ooOoo\u3007() {
        return this.\u3007o\u3007;
    }
    
    public Range<Integer> O8\u3007o() {
        return this.O8;
    }
    
    public int OO0o\u3007\u3007() {
        final RangeStyle \u3007080 = this.\u3007080;
        int oo0o\u3007\u3007;
        if (\u3007080 != null) {
            oo0o\u3007\u3007 = \u3007080.OO0o\u3007\u3007();
        }
        else {
            oo0o\u3007\u3007 = 0;
        }
        return oo0o\u3007\u3007 + this.OO0o\u3007\u3007\u3007\u30070;
    }
    
    public int OO0o\u3007\u3007\u3007\u30070() {
        final RangeStyle \u3007080 = this.\u3007080;
        int oo0o\u3007\u3007\u3007\u30070;
        if (\u3007080 != null) {
            oo0o\u3007\u3007\u3007\u30070 = \u3007080.OO0o\u3007\u3007\u3007\u30070();
        }
        else {
            oo0o\u3007\u3007\u3007\u30070 = 0;
        }
        return oo0o\u3007\u3007\u3007\u30070 + this.\u3007O888o0o();
    }
    
    public int OOO\u3007O0() {
        return this.\u3007\u3007888;
    }
    
    protected void Oo08(final int n, final int n2, final int n3, final int n4, final boolean b) {
        if (b) {
            this.Oooo8o0\u3007.union(n - this.o\u30070 - this.OO0o\u3007\u3007\u3007\u30070, n2 - this.oO80 - this.\u3007O8o08O, this.\u3007\u3007888 + n3 + this.\u30078o8o\u3007, this.\u300780\u3007808\u3007O + n4 + this.OO0o\u3007\u3007);
        }
        else {
            this.Oooo8o0\u3007.union(n - this.o\u30070, n2 - this.oO80, this.\u3007\u3007888 + n3, this.\u300780\u3007808\u3007O + n4);
        }
        final RangeStyle \u3007080 = this.\u3007080;
        if (\u3007080 != null) {
            final int o\u30070 = this.o\u30070;
            final int oo0o\u3007\u3007\u3007\u30070 = this.OO0o\u3007\u3007\u3007\u30070;
            \u3007080.Oo08(n - o\u30070 - oo0o\u3007\u3007\u3007\u30070, n2 - this.oO80 - oo0o\u3007\u3007\u3007\u30070, this.\u3007\u3007888 + n3 + this.\u30078o8o\u3007, this.\u300780\u3007808\u3007O + n4 + this.OO0o\u3007\u3007, b);
        }
    }
    
    public boolean Oo8Oo00oo(final int n) {
        final Range<Integer> o8 = this.O8;
        boolean b = false;
        if (o8 != null) {
            b = b;
            if (o8.O8() == n) {
                b = true;
            }
        }
        return b;
    }
    
    public int OoO8() {
        final RangeStyle \u3007080 = this.\u3007080;
        int ooO8;
        if (\u3007080 != null) {
            ooO8 = \u3007080.OoO8();
        }
        else {
            ooO8 = 0;
        }
        return ooO8 + this.\u300700\u30078();
    }
    
    public int Oooo8o0\u3007() {
        final RangeStyle \u3007080 = this.\u3007080;
        int oooo8o0\u3007;
        if (\u3007080 != null) {
            oooo8o0\u3007 = \u3007080.Oooo8o0\u3007();
        }
        else {
            oooo8o0\u3007 = 0;
        }
        return oooo8o0\u3007 + this.\u30078o8o\u3007;
    }
    
    public int O\u30078O8\u3007008() {
        return this.\u3007O8o08O;
    }
    
    public boolean O\u3007O\u3007oO() {
        boolean b = this.\u3007O\u3007 != 0;
        if (!this.o8()) {
            b |= this.\u30078\u30070\u3007o\u3007O(this);
        }
        return b;
    }
    
    public boolean o8() {
        return this.Oo08.isEmpty();
    }
    
    public int o800o8O() {
        final RangeStyle \u3007080 = this.\u3007080;
        int o800o8O;
        if (\u3007080 != null) {
            o800o8O = \u3007080.o800o8O();
        }
        else {
            o800o8O = 0;
        }
        return o800o8O + this.\u3007o();
    }
    
    public void o8oO\u3007(final int i, int n) {
        this.O8 = Range.\u3007o\u3007(i, n);
        if (!this.Oo08.isEmpty()) {
            final HashMap m = new HashMap();
            final Iterator<Map.Entry<Range<Integer>, T>> iterator = this.Oo08.entrySet().iterator();
            while (iterator.hasNext()) {
                final RangeStyle value = ((Map.Entry<K, RangeStyle>)iterator.next()).getValue();
                final int j = value.\u3007oOO8O8() + i;
                n = value.O8ooOoo\u3007() + i;
                m.put(Range.\u3007o\u3007(j, n), value);
                value.o8oO\u3007(j, n);
            }
            this.Oo08.clear();
            this.Oo08.putAll(m);
        }
    }
    
    public void oO(final View view, final int n, final int n2, final int n3, final int n4, @NonNull final LayoutManagerHelper layoutManagerHelper, final boolean b) {
        layoutManagerHelper.OO0o\u3007\u3007(view, n, n2, n3, n4);
        this.Oo08(n, n2, n3, n4, b);
    }
    
    public int oO80() {
        final RangeStyle \u3007080 = this.\u3007080;
        int n;
        if (\u3007080 != null) {
            n = \u3007080.oO80() + this.\u3007080.OOO\u3007O0();
        }
        else {
            n = 0;
        }
        return n;
    }
    
    protected int oo88o8O() {
        return this.o\u30070 + this.\u3007\u3007888;
    }
    
    public int oo\u3007() {
        return this.oO80;
    }
    
    public int o\u30070() {
        final RangeStyle \u3007080 = this.\u3007080;
        int n;
        if (\u3007080 != null) {
            n = \u3007080.o\u30070() + this.\u3007080.\u30070000OOO();
        }
        else {
            n = 0;
        }
        return n;
    }
    
    public boolean o\u30070OOo\u30070(final int i) {
        final Range<Integer> o8 = this.O8;
        boolean b = true;
        if (o8 != null) {
            b = (!o8.\u3007o00\u3007\u3007Oo(i) && b);
        }
        return b;
    }
    
    public int o\u3007O8\u3007\u3007o() {
        return this.OO0o\u3007\u3007\u3007\u30070;
    }
    
    public int o\u3007\u30070\u3007() {
        return this.o\u30070;
    }
    
    public int \u300700() {
        return this.\u30078o8o\u3007;
    }
    
    public int \u30070000OOO() {
        return this.\u300780\u3007808\u3007O;
    }
    
    protected int \u300700\u30078() {
        return this.\u3007O8o08O + this.OO0o\u3007\u3007;
    }
    
    public void \u3007080(final RecyclerView.Recycler recycler, final RecyclerView.State state, int \u3007\u3007808\u3007, int \u300780\u3007808\u3007O, final int n, final LayoutManagerHelper layoutManagerHelper) {
        if (!this.o8()) {
            final Iterator<Map.Entry<Range<Integer>, T>> iterator = this.Oo08.entrySet().iterator();
            while (iterator.hasNext()) {
                ((Map.Entry<K, RangeStyle>)iterator.next()).getValue().\u3007080(recycler, state, \u3007\u3007808\u3007, \u300780\u3007808\u3007O, n, layoutManagerHelper);
            }
        }
        if (this.O\u3007O\u3007oO()) {
            if (this.\u300708O8o\u30070(n)) {
                final View \u3007\u3007808\u30072 = this.\u3007\u3007808\u3007;
                if (\u3007\u3007808\u30072 != null) {
                    this.Oooo8o0\u3007.union(\u3007\u3007808\u30072.getLeft(), this.\u3007\u3007808\u3007.getTop(), this.\u3007\u3007808\u3007.getRight(), this.\u3007\u3007808\u3007.getBottom());
                }
            }
            if (!this.Oooo8o0\u3007.isEmpty()) {
                if (this.\u300708O8o\u30070(n)) {
                    if (layoutManagerHelper.getOrientation() == 1) {
                        this.Oooo8o0\u3007.offset(0, -n);
                    }
                    else {
                        this.Oooo8o0\u3007.offset(-n, 0);
                    }
                }
                this.o\u30078oOO88(this);
                \u300780\u3007808\u3007O = layoutManagerHelper.\u300780\u3007808\u3007O();
                \u3007\u3007808\u3007 = layoutManagerHelper.\u3007\u3007808\u3007();
                Label_0428: {
                    if (layoutManagerHelper.getOrientation() == 1) {
                        if (!this.Oooo8o0\u3007.intersects(0, -\u3007\u3007808\u3007 / 4, \u300780\u3007808\u3007O, \u3007\u3007808\u3007 + \u3007\u3007808\u3007 / 4)) {
                            break Label_0428;
                        }
                    }
                    else if (!this.Oooo8o0\u3007.intersects(-\u300780\u3007808\u3007O / 4, 0, \u300780\u3007808\u3007O + \u300780\u3007808\u3007O / 4, \u3007\u3007808\u3007)) {
                        break Label_0428;
                    }
                    if (this.\u3007\u3007808\u3007 == null) {
                        layoutManagerHelper.o800o8O(this.\u3007\u3007808\u3007 = layoutManagerHelper.o\u3007O8\u3007\u3007o(), true);
                    }
                    if (layoutManagerHelper.getOrientation() == 1) {
                        this.Oooo8o0\u3007.left = layoutManagerHelper.getPaddingLeft() + this.OO0o\u3007\u3007() + this.\u3007\u3007888();
                        this.Oooo8o0\u3007.right = layoutManagerHelper.\u300780\u3007808\u3007O() - layoutManagerHelper.getPaddingRight() - this.Oooo8o0\u3007() - this.oO80();
                    }
                    else {
                        this.Oooo8o0\u3007.top = layoutManagerHelper.getPaddingTop() + this.\u3007\u3007808\u3007() + this.\u300780\u3007808\u3007O();
                        this.Oooo8o0\u3007.bottom = layoutManagerHelper.\u300780\u3007808\u3007O() - layoutManagerHelper.getPaddingBottom() - this.\u3007O8o08O() - this.o\u30070();
                    }
                    this.\u3007o\u3007(this.\u3007\u3007808\u3007);
                    this.o\u30078(layoutManagerHelper);
                    return;
                }
                this.Oooo8o0\u3007.set(0, 0, 0, 0);
                final View \u3007\u3007808\u30073 = this.\u3007\u3007808\u3007;
                if (\u3007\u3007808\u30073 != null) {
                    \u3007\u3007808\u30073.layout(0, 0, 0, 0);
                }
                this.o\u30078(layoutManagerHelper);
            }
        }
        this.o\u30078(layoutManagerHelper);
        if (this.\u3007\u30070o()) {
            this.O08000(layoutManagerHelper, this);
        }
    }
    
    public int \u30070\u3007O0088o() {
        final RangeStyle \u3007080 = this.\u3007080;
        int \u30070\u3007O0088o;
        if (\u3007080 != null) {
            \u30070\u3007O0088o = \u3007080.\u30070\u3007O0088o();
        }
        else {
            \u30070\u3007O0088o = 0;
        }
        return \u30070\u3007O0088o + this.oO80;
    }
    
    public void \u30078(final LayoutManagerHelper layoutManagerHelper) {
        this.O8(layoutManagerHelper, this);
    }
    
    public int \u300780\u3007808\u3007O() {
        final RangeStyle \u3007080 = this.\u3007080;
        int n;
        if (\u3007080 != null) {
            n = \u3007080.\u300780\u3007808\u3007O() + this.\u3007080.oo\u3007();
        }
        else {
            n = 0;
        }
        return n;
    }
    
    public int \u30078o8o\u3007() {
        final RangeStyle \u3007080 = this.\u3007080;
        int \u30078o8o\u3007;
        if (\u3007080 != null) {
            \u30078o8o\u3007 = \u3007080.\u30078o8o\u3007();
        }
        else {
            \u30078o8o\u3007 = 0;
        }
        return \u30078o8o\u3007 + this.oo88o8O();
    }
    
    public int \u3007O00() {
        final RangeStyle \u3007080 = this.\u3007080;
        int \u3007o00;
        if (\u3007080 != null) {
            \u3007o00 = \u3007080.\u3007O00();
        }
        else {
            \u3007o00 = 0;
        }
        return \u3007o00 + this.o\u30070;
    }
    
    protected int \u3007O888o0o() {
        return this.OO0o\u3007\u3007\u3007\u30070 + this.\u30078o8o\u3007;
    }
    
    public int \u3007O8o08O() {
        final RangeStyle \u3007080 = this.\u3007080;
        int \u3007o8o08O;
        if (\u3007080 != null) {
            \u3007o8o08O = \u3007080.\u3007O8o08O();
        }
        else {
            \u3007o8o08O = 0;
        }
        return \u3007o8o08O + this.OO0o\u3007\u3007;
    }
    
    public int \u3007O\u3007() {
        final RangeStyle \u3007080 = this.\u3007080;
        int \u3007o\u3007;
        if (\u3007080 != null) {
            \u3007o\u3007 = \u3007080.\u3007O\u3007();
        }
        else {
            \u3007o\u3007 = 0;
        }
        return \u3007o\u3007 + this.\u300780\u3007808\u3007O;
    }
    
    protected int \u3007o() {
        return this.oO80 + this.\u300780\u3007808\u3007O;
    }
    
    public void \u3007o00\u3007\u3007Oo(final RecyclerView.Recycler recycler, final RecyclerView.State state, final LayoutManagerHelper layoutManagerHelper) {
        if (!this.o8()) {
            final Iterator<Map.Entry<Range<Integer>, T>> iterator = this.Oo08.entrySet().iterator();
            while (iterator.hasNext()) {
                ((Map.Entry<K, RangeStyle>)iterator.next()).getValue().\u3007o00\u3007\u3007Oo(recycler, state, layoutManagerHelper);
            }
        }
        if (!this.O\u3007O\u3007oO()) {
            final View \u3007\u3007808\u3007 = this.\u3007\u3007808\u3007;
            if (\u3007\u3007808\u3007 != null) {
                layoutManagerHelper.\u3007O8o08O(\u3007\u3007808\u3007);
                this.\u3007\u3007808\u3007 = null;
            }
        }
    }
    
    public int \u3007oOO8O8() {
        return this.\u3007o00\u3007\u3007Oo;
    }
    
    public int \u3007oo\u3007() {
        return this.OO0o\u3007\u3007;
    }
    
    public void \u3007o\u3007(@NonNull final View view) {
        view.measure(View$MeasureSpec.makeMeasureSpec(this.Oooo8o0\u3007.width(), 1073741824), View$MeasureSpec.makeMeasureSpec(this.Oooo8o0\u3007.height(), 1073741824));
        final Rect oooo8o0\u3007 = this.Oooo8o0\u3007;
        view.layout(oooo8o0\u3007.left, oooo8o0\u3007.top, oooo8o0\u3007.right, oooo8o0\u3007.bottom);
        view.setBackgroundColor(this.\u3007O\u3007);
        this.Oooo8o0\u3007.set(0, 0, 0, 0);
    }
    
    public boolean \u3007\u30070o() {
        return this.\u3007080 == null;
    }
    
    public int \u3007\u3007808\u3007() {
        final RangeStyle \u3007080 = this.\u3007080;
        int \u3007\u3007808\u3007;
        if (\u3007080 != null) {
            \u3007\u3007808\u3007 = \u3007080.\u3007\u3007808\u3007();
        }
        else {
            \u3007\u3007808\u3007 = 0;
        }
        return \u3007\u3007808\u3007 + this.\u3007O8o08O;
    }
    
    public int \u3007\u3007888() {
        final RangeStyle \u3007080 = this.\u3007080;
        int n;
        if (\u3007080 != null) {
            n = \u3007080.\u3007\u3007888() + this.\u3007080.o\u3007\u30070\u3007();
        }
        else {
            n = 0;
        }
        return n;
    }
    
    public int \u3007\u30078O0\u30078() {
        final RangeStyle \u3007080 = this.\u3007080;
        int \u3007\u30078O0\u30078;
        if (\u3007080 != null) {
            \u3007\u30078O0\u30078 = \u3007080.\u3007\u30078O0\u30078();
        }
        else {
            \u3007\u30078O0\u30078 = 0;
        }
        return \u3007\u30078O0\u30078 + this.\u3007\u3007888;
    }
    
    public boolean \u3007\u3007\u30070\u3007\u30070(final int n) {
        final Range<Integer> o8 = this.O8;
        boolean b = false;
        if (o8 != null) {
            b = b;
            if (o8.Oo08() == n) {
                b = true;
            }
        }
        return b;
    }
}

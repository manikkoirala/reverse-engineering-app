// 
// Decompiled by Procyon v0.6.0
// 

package com.alibaba.android.vlayout;

import androidx.recyclerview.widget.RecyclerView;
import android.view.View;

public abstract class OrientationHelperEx
{
    protected final ExposeLinearLayoutManagerEx \u3007080;
    private int \u3007o00\u3007\u3007Oo;
    
    private OrientationHelperEx(final ExposeLinearLayoutManagerEx \u3007080) {
        this.\u3007o00\u3007\u3007Oo = Integer.MIN_VALUE;
        this.\u3007080 = \u3007080;
    }
    
    public static OrientationHelperEx \u3007080(final ExposeLinearLayoutManagerEx exposeLinearLayoutManagerEx) {
        return new OrientationHelperEx(exposeLinearLayoutManagerEx) {
            @Override
            public int O8(final View view) {
                final RecyclerView.LayoutParams layoutParams = (RecyclerView.LayoutParams)view.getLayoutParams();
                int decoratedRight;
                if (!super.\u3007080.isEnableMarginOverLap()) {
                    decoratedRight = ((RecyclerView.LayoutManager)super.\u3007080).getDecoratedRight(view) + layoutParams.rightMargin;
                }
                else {
                    decoratedRight = ((RecyclerView.LayoutManager)super.\u3007080).getDecoratedRight(view);
                }
                return decoratedRight;
            }
            
            @Override
            public int OO0o\u3007\u3007\u3007\u30070() {
                return ((RecyclerView.LayoutManager)super.\u3007080).getPaddingRight();
            }
            
            @Override
            public int Oo08(final View view) {
                final RecyclerView.LayoutParams layoutParams = (RecyclerView.LayoutParams)view.getLayoutParams();
                int decoratedMeasuredWidth;
                if (!super.\u3007080.isEnableMarginOverLap()) {
                    decoratedMeasuredWidth = ((RecyclerView.LayoutManager)super.\u3007080).getDecoratedMeasuredWidth(view) + layoutParams.leftMargin + layoutParams.rightMargin;
                }
                else {
                    decoratedMeasuredWidth = ((RecyclerView.LayoutManager)super.\u3007080).getDecoratedMeasuredWidth(view);
                }
                return decoratedMeasuredWidth;
            }
            
            @Override
            public void Oooo8o0\u3007(final int n) {
                ((RecyclerView.LayoutManager)super.\u3007080).offsetChildrenHorizontal(n);
            }
            
            @Override
            public int oO80() {
                return ((RecyclerView.LayoutManager)super.\u3007080).getWidth();
            }
            
            @Override
            public int o\u30070(final View view) {
                final RecyclerView.LayoutParams layoutParams = (RecyclerView.LayoutParams)view.getLayoutParams();
                return ((RecyclerView.LayoutManager)super.\u3007080).getDecoratedMeasuredHeight(view) + layoutParams.topMargin + layoutParams.bottomMargin;
            }
            
            @Override
            public int \u300780\u3007808\u3007O() {
                return ((RecyclerView.LayoutManager)super.\u3007080).getWidth() - ((RecyclerView.LayoutManager)super.\u3007080).getPaddingRight();
            }
            
            @Override
            public int \u30078o8o\u3007() {
                return ((RecyclerView.LayoutManager)super.\u3007080).getPaddingLeft();
            }
            
            @Override
            public int \u3007O8o08O() {
                return ((RecyclerView.LayoutManager)super.\u3007080).getWidth() - ((RecyclerView.LayoutManager)super.\u3007080).getPaddingLeft() - ((RecyclerView.LayoutManager)super.\u3007080).getPaddingRight();
            }
            
            @Override
            public int \u3007\u3007888(final View view) {
                final RecyclerView.LayoutParams layoutParams = (RecyclerView.LayoutParams)view.getLayoutParams();
                int decoratedLeft;
                if (!super.\u3007080.isEnableMarginOverLap()) {
                    decoratedLeft = ((RecyclerView.LayoutManager)super.\u3007080).getDecoratedLeft(view) - layoutParams.leftMargin;
                }
                else {
                    decoratedLeft = ((RecyclerView.LayoutManager)super.\u3007080).getDecoratedLeft(view);
                }
                return decoratedLeft;
            }
        };
    }
    
    public static OrientationHelperEx \u3007o00\u3007\u3007Oo(final ExposeLinearLayoutManagerEx exposeLinearLayoutManagerEx, final int n) {
        if (n == 0) {
            return \u3007080(exposeLinearLayoutManagerEx);
        }
        if (n == 1) {
            return \u3007o\u3007(exposeLinearLayoutManagerEx);
        }
        throw new IllegalArgumentException("invalid orientation");
    }
    
    public static OrientationHelperEx \u3007o\u3007(final ExposeLinearLayoutManagerEx exposeLinearLayoutManagerEx) {
        return new OrientationHelperEx(exposeLinearLayoutManagerEx) {
            @Override
            public int O8(final View view) {
                final RecyclerView.LayoutParams layoutParams = (RecyclerView.LayoutParams)view.getLayoutParams();
                int decoratedBottom;
                if (!super.\u3007080.isEnableMarginOverLap()) {
                    decoratedBottom = ((RecyclerView.LayoutManager)super.\u3007080).getDecoratedBottom(view) + layoutParams.bottomMargin;
                }
                else {
                    decoratedBottom = ((RecyclerView.LayoutManager)super.\u3007080).getDecoratedBottom(view);
                }
                return decoratedBottom;
            }
            
            @Override
            public int OO0o\u3007\u3007\u3007\u30070() {
                return ((RecyclerView.LayoutManager)super.\u3007080).getPaddingBottom();
            }
            
            @Override
            public int Oo08(final View view) {
                final RecyclerView.LayoutParams layoutParams = (RecyclerView.LayoutParams)view.getLayoutParams();
                int decoratedMeasuredHeight;
                if (!super.\u3007080.isEnableMarginOverLap()) {
                    decoratedMeasuredHeight = ((RecyclerView.LayoutManager)super.\u3007080).getDecoratedMeasuredHeight(view) + layoutParams.topMargin + layoutParams.bottomMargin;
                }
                else {
                    decoratedMeasuredHeight = ((RecyclerView.LayoutManager)super.\u3007080).getDecoratedMeasuredHeight(view);
                }
                return decoratedMeasuredHeight;
            }
            
            @Override
            public void Oooo8o0\u3007(final int n) {
                ((RecyclerView.LayoutManager)super.\u3007080).offsetChildrenVertical(n);
            }
            
            @Override
            public int oO80() {
                return ((RecyclerView.LayoutManager)super.\u3007080).getHeight();
            }
            
            @Override
            public int o\u30070(final View view) {
                final RecyclerView.LayoutParams layoutParams = (RecyclerView.LayoutParams)view.getLayoutParams();
                return ((RecyclerView.LayoutManager)super.\u3007080).getDecoratedMeasuredWidth(view) + layoutParams.leftMargin + layoutParams.rightMargin;
            }
            
            @Override
            public int \u300780\u3007808\u3007O() {
                return ((RecyclerView.LayoutManager)super.\u3007080).getHeight() - ((RecyclerView.LayoutManager)super.\u3007080).getPaddingBottom();
            }
            
            @Override
            public int \u30078o8o\u3007() {
                return ((RecyclerView.LayoutManager)super.\u3007080).getPaddingTop();
            }
            
            @Override
            public int \u3007O8o08O() {
                return ((RecyclerView.LayoutManager)super.\u3007080).getHeight() - ((RecyclerView.LayoutManager)super.\u3007080).getPaddingTop() - ((RecyclerView.LayoutManager)super.\u3007080).getPaddingBottom();
            }
            
            @Override
            public int \u3007\u3007888(final View view) {
                final RecyclerView.LayoutParams layoutParams = (RecyclerView.LayoutParams)view.getLayoutParams();
                int decoratedTop;
                if (!super.\u3007080.isEnableMarginOverLap()) {
                    decoratedTop = ((RecyclerView.LayoutManager)super.\u3007080).getDecoratedTop(view) - layoutParams.topMargin;
                }
                else {
                    decoratedTop = ((RecyclerView.LayoutManager)super.\u3007080).getDecoratedTop(view);
                }
                return decoratedTop;
            }
        };
    }
    
    public abstract int O8(final View p0);
    
    public int OO0o\u3007\u3007() {
        int n;
        if (Integer.MIN_VALUE == this.\u3007o00\u3007\u3007Oo) {
            n = 0;
        }
        else {
            n = this.\u3007O8o08O() - this.\u3007o00\u3007\u3007Oo;
        }
        return n;
    }
    
    public abstract int OO0o\u3007\u3007\u3007\u30070();
    
    public abstract int Oo08(final View p0);
    
    public abstract void Oooo8o0\u3007(final int p0);
    
    public abstract int oO80();
    
    public abstract int o\u30070(final View p0);
    
    public abstract int \u300780\u3007808\u3007O();
    
    public abstract int \u30078o8o\u3007();
    
    public abstract int \u3007O8o08O();
    
    public void \u3007\u3007808\u3007() {
        this.\u3007o00\u3007\u3007Oo = this.\u3007O8o08O();
    }
    
    public abstract int \u3007\u3007888(final View p0);
}

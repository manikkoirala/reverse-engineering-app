// 
// Decompiled by Procyon v0.6.0
// 

package com.alibaba.android.vlayout;

import java.util.List;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public abstract class VirtualLayoutAdapter<VH extends ViewHolder> extends Adapter<VH>
{
    @NonNull
    protected VirtualLayoutManager o0;
    
    public VirtualLayoutAdapter(@NonNull final VirtualLayoutManager o0) {
        this.o0 = o0;
    }
    
    public void \u3007O00(final List<LayoutHelper> list) {
        this.o0.o\u30078(list);
    }
    
    @NonNull
    public List<LayoutHelper> \u3007O\u3007() {
        return this.o0.OOO\u3007O0();
    }
}

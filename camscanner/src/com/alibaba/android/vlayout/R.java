// 
// Decompiled by Procyon v0.6.0
// 

package com.alibaba.android.vlayout;

public final class R
{
    private R() {
    }
    
    public static final class attr
    {
        public static final int layoutManager = 2130969463;
        public static final int reverseLayout = 2130969860;
        public static final int spanCount = 2130969978;
        public static final int stackFromEnd = 2130969991;
        
        private attr() {
        }
    }
    
    public static final class dimen
    {
        public static final int item_touch_helper_max_drag_scroll_per_frame = 2131165732;
        public static final int item_touch_helper_swipe_escape_max_velocity = 2131165733;
        public static final int item_touch_helper_swipe_escape_velocity = 2131165734;
        
        private dimen() {
        }
    }
    
    public static final class id
    {
        public static final int item_touch_helper_previous_elevation = 2131363878;
        public static final int tag_layout_helper_bg = 2131366283;
        
        private id() {
        }
    }
    
    public static final class styleable
    {
        public static final int[] RecyclerView;
        public static final int RecyclerView_android_clipToPadding = 1;
        public static final int RecyclerView_android_descendantFocusability = 2;
        public static final int RecyclerView_android_orientation = 0;
        public static final int RecyclerView_fastScrollEnabled = 3;
        public static final int RecyclerView_fastScrollHorizontalThumbDrawable = 4;
        public static final int RecyclerView_fastScrollHorizontalTrackDrawable = 5;
        public static final int RecyclerView_fastScrollVerticalThumbDrawable = 6;
        public static final int RecyclerView_fastScrollVerticalTrackDrawable = 7;
        public static final int RecyclerView_layoutManager = 8;
        public static final int RecyclerView_reverseLayout = 9;
        public static final int RecyclerView_spanCount = 10;
        public static final int RecyclerView_stackFromEnd = 11;
        
        static {
            RecyclerView = new int[] { 16842948, 16842987, 16842993, 2130969221, 2130969222, 2130969223, 2130969237, 2130969238, 2130969463, 2130969860, 2130969978, 2130969991 };
        }
        
        private styleable() {
        }
    }
}

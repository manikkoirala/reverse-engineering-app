// 
// Decompiled by Procyon v0.6.0
// 

package com.alibaba.android.vlayout.extend;

import androidx.annotation.Keep;
import android.view.View;

public class PerformanceMonitor
{
    @Keep
    public void recordEnd(final String s, final View view) {
    }
    
    @Keep
    public void recordEnd(final String s, final String s2) {
    }
    
    @Keep
    public void recordStart(final String s, final View view) {
    }
    
    @Keep
    public void recordStart(final String s, final String s2) {
    }
}

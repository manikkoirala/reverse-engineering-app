// 
// Decompiled by Procyon v0.6.0
// 

package com.alibaba.android.vlayout.extend;

import android.view.View;
import android.util.Log;
import java.io.Closeable;
import android.util.SparseIntArray;
import androidx.recyclerview.widget.RecyclerView;

public final class InnerRecycledViewPool extends RecycledViewPool
{
    private static int O8 = 20;
    private RecycledViewPool \u3007080;
    private SparseIntArray \u3007o00\u3007\u3007Oo;
    private SparseIntArray \u3007o\u3007;
    
    public InnerRecycledViewPool() {
        this(new RecyclerView.RecycledViewPool());
    }
    
    public InnerRecycledViewPool(final RecycledViewPool \u3007080) {
        this.\u3007o00\u3007\u3007Oo = new SparseIntArray();
        this.\u3007o\u3007 = new SparseIntArray();
        this.\u3007080 = \u3007080;
    }
    
    private void \u3007080(final ViewHolder viewHolder) {
        final View itemView = viewHolder.itemView;
        if (itemView instanceof Closeable) {
            try {
                ((Closeable)itemView).close();
            }
            catch (final Exception ex) {
                Log.getStackTraceString((Throwable)ex);
            }
        }
        if (viewHolder instanceof Closeable) {
            try {
                ((Closeable)viewHolder).close();
            }
            catch (final Exception ex2) {
                Log.getStackTraceString((Throwable)ex2);
            }
        }
    }
    
    @Override
    public void clear() {
        for (int size = this.\u3007o00\u3007\u3007Oo.size(), i = 0; i < size; ++i) {
            final int key = this.\u3007o00\u3007\u3007Oo.keyAt(i);
            for (ViewHolder viewHolder = this.\u3007080.getRecycledView(key); viewHolder != null; viewHolder = this.\u3007080.getRecycledView(key)) {
                this.\u3007080(viewHolder);
            }
        }
        this.\u3007o00\u3007\u3007Oo.clear();
        super.clear();
    }
    
    @Override
    public ViewHolder getRecycledView(final int n) {
        final ViewHolder recycledView = this.\u3007080.getRecycledView(n);
        if (recycledView != null) {
            int value;
            if (this.\u3007o00\u3007\u3007Oo.indexOfKey(n) >= 0) {
                value = this.\u3007o00\u3007\u3007Oo.get(n);
            }
            else {
                value = 0;
            }
            if (value > 0) {
                this.\u3007o00\u3007\u3007Oo.put(n, value - 1);
            }
        }
        return recycledView;
    }
    
    @Override
    public void putRecycledView(final ViewHolder viewHolder) {
        final int itemViewType = viewHolder.getItemViewType();
        if (this.\u3007o\u3007.indexOfKey(itemViewType) < 0) {
            this.\u3007o\u3007.put(itemViewType, InnerRecycledViewPool.O8);
            this.setMaxRecycledViews(itemViewType, InnerRecycledViewPool.O8);
        }
        int value;
        if (this.\u3007o00\u3007\u3007Oo.indexOfKey(itemViewType) >= 0) {
            value = this.\u3007o00\u3007\u3007Oo.get(itemViewType);
        }
        else {
            value = 0;
        }
        if (this.\u3007o\u3007.get(itemViewType) > value) {
            this.\u3007080.putRecycledView(viewHolder);
            this.\u3007o00\u3007\u3007Oo.put(itemViewType, value + 1);
        }
        else {
            this.\u3007080(viewHolder);
        }
    }
    
    @Override
    public void setMaxRecycledViews(final int n, final int n2) {
        for (ViewHolder viewHolder = this.\u3007080.getRecycledView(n); viewHolder != null; viewHolder = this.\u3007080.getRecycledView(n)) {
            this.\u3007080(viewHolder);
        }
        this.\u3007o\u3007.put(n, n2);
        this.\u3007o00\u3007\u3007Oo.put(n, 0);
        this.\u3007080.setMaxRecycledViews(n, n2);
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.alibaba.android.vlayout;

import android.os.BaseBundle;
import android.annotation.SuppressLint;
import android.os.Parcelable;
import android.graphics.PointF;
import java.util.List;
import android.view.View;
import java.lang.reflect.InvocationTargetException;
import android.content.Context;
import androidx.recyclerview.widget.RecyclerView;
import android.os.Bundle;
import com.alibaba.android.vlayout.layout.LayoutChunkResult;
import java.lang.reflect.Method;
import java.lang.reflect.Field;
import androidx.recyclerview.widget.LinearLayoutManager;

class ExposeLinearLayoutManagerEx extends LinearLayoutManager
{
    private static final boolean DEBUG = false;
    static final int FLAG_INVALID = 4;
    static final int FLAG_UPDATED = 2;
    public static final int HORIZONTAL = 0;
    public static final int INVALID_OFFSET = Integer.MIN_VALUE;
    private static final float MAX_SCROLL_FACTOR = 0.33f;
    private static final String TAG = "ExposeLLManagerEx";
    public static final int VERTICAL = 1;
    private static Field vhField;
    private static Method vhSetFlags;
    private Object[] emptyArgs;
    private com.alibaba.android.vlayout.layout.LayoutChunkResult layoutChunkResultCache;
    private final AnchorInfo mAnchorInfo;
    private final ChildHelperWrapper mChildHelperWrapper;
    protected Bundle mCurrentPendingSavedState;
    private int mCurrentPendingScrollPosition;
    private final Method mEnsureLayoutStateMethod;
    private boolean mLastStackFromEnd;
    protected LayoutState mLayoutState;
    private OrientationHelperEx mOrientationHelper;
    private int mPendingScrollPositionOffset;
    private RecyclerView mRecyclerView;
    private boolean mShouldReverseLayoutExpose;
    protected int recycleOffset;
    
    public ExposeLinearLayoutManagerEx(final Context context) {
        this(context, 1, false);
    }
    
    public ExposeLinearLayoutManagerEx(final Context p0, final int p1, final boolean p2) {
        // 
        // This method could not be decompiled.
        // 
        // Original Bytecode:
        // 
        //     1: aload_1        
        //     2: iload_2        
        //     3: iload_3        
        //     4: invokespecial   androidx/recyclerview/widget/LinearLayoutManager.<init>:(Landroid/content/Context;IZ)V
        //     7: aload_0        
        //     8: iconst_0       
        //     9: putfield        com/alibaba/android/vlayout/ExposeLinearLayoutManagerEx.mShouldReverseLayoutExpose:Z
        //    12: aload_0        
        //    13: iconst_m1      
        //    14: putfield        com/alibaba/android/vlayout/ExposeLinearLayoutManagerEx.mCurrentPendingScrollPosition:I
        //    17: aload_0        
        //    18: ldc             -2147483648
        //    20: putfield        com/alibaba/android/vlayout/ExposeLinearLayoutManagerEx.mPendingScrollPositionOffset:I
        //    23: aload_0        
        //    24: aconst_null    
        //    25: putfield        com/alibaba/android/vlayout/ExposeLinearLayoutManagerEx.mCurrentPendingSavedState:Landroid/os/Bundle;
        //    28: aload_0        
        //    29: iconst_0       
        //    30: anewarray       Ljava/lang/Object;
        //    33: putfield        com/alibaba/android/vlayout/ExposeLinearLayoutManagerEx.emptyArgs:[Ljava/lang/Object;
        //    36: aload_0        
        //    37: new             Lcom/alibaba/android/vlayout/layout/LayoutChunkResult;
        //    40: dup            
        //    41: invokespecial   com/alibaba/android/vlayout/layout/LayoutChunkResult.<init>:()V
        //    44: putfield        com/alibaba/android/vlayout/ExposeLinearLayoutManagerEx.layoutChunkResultCache:Lcom/alibaba/android/vlayout/layout/LayoutChunkResult;
        //    47: aload_0        
        //    48: new             Lcom/alibaba/android/vlayout/ExposeLinearLayoutManagerEx$AnchorInfo;
        //    51: dup            
        //    52: aload_0        
        //    53: invokespecial   com/alibaba/android/vlayout/ExposeLinearLayoutManagerEx$AnchorInfo.<init>:(Lcom/alibaba/android/vlayout/ExposeLinearLayoutManagerEx;)V
        //    56: putfield        com/alibaba/android/vlayout/ExposeLinearLayoutManagerEx.mAnchorInfo:Lcom/alibaba/android/vlayout/ExposeLinearLayoutManagerEx$AnchorInfo;
        //    59: aload_0        
        //    60: iload_2        
        //    61: invokevirtual   com/alibaba/android/vlayout/ExposeLinearLayoutManagerEx.setOrientation:(I)V
        //    64: aload_0        
        //    65: iload_3        
        //    66: invokevirtual   androidx/recyclerview/widget/LinearLayoutManager.setReverseLayout:(Z)V
        //    69: aload_0        
        //    70: new             Lcom/alibaba/android/vlayout/ExposeLinearLayoutManagerEx$ChildHelperWrapper;
        //    73: dup            
        //    74: aload_0        
        //    75: aload_0        
        //    76: invokespecial   com/alibaba/android/vlayout/ExposeLinearLayoutManagerEx$ChildHelperWrapper.<init>:(Lcom/alibaba/android/vlayout/ExposeLinearLayoutManagerEx;Landroidx/recyclerview/widget/RecyclerView$LayoutManager;)V
        //    79: putfield        com/alibaba/android/vlayout/ExposeLinearLayoutManagerEx.mChildHelperWrapper:Lcom/alibaba/android/vlayout/ExposeLinearLayoutManagerEx$ChildHelperWrapper;
        //    82: ldc             Landroidx/recyclerview/widget/LinearLayoutManager;.class
        //    84: ldc             "ensureLayoutState"
        //    86: iconst_0       
        //    87: anewarray       Ljava/lang/Class;
        //    90: invokevirtual   java/lang/Class.getDeclaredMethod:(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;
        //    93: astore_1       
        //    94: aload_0        
        //    95: aload_1        
        //    96: putfield        com/alibaba/android/vlayout/ExposeLinearLayoutManagerEx.mEnsureLayoutStateMethod:Ljava/lang/reflect/Method;
        //    99: aload_1        
        //   100: iconst_1       
        //   101: invokevirtual   java/lang/reflect/AccessibleObject.setAccessible:(Z)V
        //   104: ldc             Landroidx/recyclerview/widget/RecyclerView$LayoutManager;.class
        //   106: ldc             "setItemPrefetchEnabled"
        //   108: iconst_1       
        //   109: anewarray       Ljava/lang/Class;
        //   112: dup            
        //   113: iconst_0       
        //   114: getstatic       java/lang/Boolean.TYPE:Ljava/lang/Class;
        //   117: aastore        
        //   118: invokevirtual   java/lang/Class.getDeclaredMethod:(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;
        //   121: astore_1       
        //   122: aload_1        
        //   123: ifnull          142
        //   126: aload_1        
        //   127: aload_0        
        //   128: iconst_1       
        //   129: anewarray       Ljava/lang/Object;
        //   132: dup            
        //   133: iconst_0       
        //   134: getstatic       java/lang/Boolean.FALSE:Ljava/lang/Boolean;
        //   137: aastore        
        //   138: invokevirtual   java/lang/reflect/Method.invoke:(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
        //   141: pop            
        //   142: return         
        //   143: astore_1       
        //   144: aload_1        
        //   145: invokevirtual   java/lang/Throwable.printStackTrace:()V
        //   148: new             Ljava/lang/RuntimeException;
        //   151: dup            
        //   152: aload_1        
        //   153: invokespecial   java/lang/RuntimeException.<init>:(Ljava/lang/Throwable;)V
        //   156: athrow         
        //   157: astore_1       
        //   158: goto            142
        //    Exceptions:
        //  Try           Handler
        //  Start  End    Start  End    Type                                         
        //  -----  -----  -----  -----  ---------------------------------------------
        //  82     104    143    157    Ljava/lang/NoSuchMethodException;
        //  104    122    157    161    Ljava/lang/NoSuchMethodException;
        //  104    122    157    161    Ljava/lang/reflect/InvocationTargetException;
        //  104    122    157    161    Ljava/lang/IllegalAccessException;
        //  126    142    157    161    Ljava/lang/NoSuchMethodException;
        //  126    142    157    161    Ljava/lang/reflect/InvocationTargetException;
        //  126    142    157    161    Ljava/lang/IllegalAccessException;
        // 
        // The error that occurred was:
        // 
        // java.lang.IllegalStateException: Expression is linked from several locations: Label_0142:
        //     at com.strobel.decompiler.ast.Error.expressionLinkedFromMultipleLocations(Error.java:27)
        //     at com.strobel.decompiler.ast.AstOptimizer.mergeDisparateObjectInitializations(AstOptimizer.java:2604)
        //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:235)
        //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:42)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:206)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:93)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethodBody(AstBuilder.java:868)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createConstructor(AstBuilder.java:799)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addTypeMembers(AstBuilder.java:635)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeCore(AstBuilder.java:605)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeNoCache(AstBuilder.java:195)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createType(AstBuilder.java:162)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addType(AstBuilder.java:137)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.buildAst(JavaLanguage.java:71)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.decompileType(JavaLanguage.java:59)
        //     at com.strobel.decompiler.DecompilerDriver.decompileType(DecompilerDriver.java:333)
        //     at com.strobel.decompiler.DecompilerDriver.decompileJar(DecompilerDriver.java:254)
        //     at com.strobel.decompiler.DecompilerDriver.main(DecompilerDriver.java:144)
        // 
        throw new IllegalStateException("An error occurred while decompiling this method.");
    }
    
    protected static void attachViewHolder(final LayoutParams obj, final ViewHolder viewHolder) {
        try {
            if (ExposeLinearLayoutManagerEx.vhField == null) {
                ExposeLinearLayoutManagerEx.vhField = LayoutParams.class.getDeclaredField("mViewHolder");
            }
            ExposeLinearLayoutManagerEx.vhField.setAccessible(true);
            ExposeLinearLayoutManagerEx.vhField.set(obj, viewHolder);
            if (ExposeLinearLayoutManagerEx.vhSetFlags == null) {
                final Class<Integer> type = Integer.TYPE;
                (ExposeLinearLayoutManagerEx.vhSetFlags = ViewHolder.class.getDeclaredMethod("setFlags", type, type)).setAccessible(true);
            }
            ExposeLinearLayoutManagerEx.vhSetFlags.invoke(viewHolder, 4, 4);
        }
        catch (final InvocationTargetException ex) {
            ex.printStackTrace();
        }
        catch (final NoSuchMethodException ex2) {
            ex2.printStackTrace();
        }
        catch (final IllegalAccessException ex3) {
            ex3.printStackTrace();
        }
        catch (final NoSuchFieldException ex4) {
            ex4.printStackTrace();
        }
    }
    
    private int convertFocusDirectionToLayoutDirectionExpose(int n) {
        final int orientation = this.getOrientation();
        int n2 = -1;
        final int n3 = 1;
        final int n4 = 1;
        if (n == 1) {
            return -1;
        }
        if (n == 2) {
            return 1;
        }
        if (n == 17) {
            if (orientation != 0) {
                n2 = Integer.MIN_VALUE;
            }
            return n2;
        }
        if (n == 33) {
            if (orientation != 1) {
                n2 = Integer.MIN_VALUE;
            }
            return n2;
        }
        if (n == 66) {
            if (orientation == 0) {
                n = n3;
            }
            else {
                n = Integer.MIN_VALUE;
            }
            return n;
        }
        if (n != 130) {
            return Integer.MIN_VALUE;
        }
        if (orientation == 1) {
            n = n4;
        }
        else {
            n = Integer.MIN_VALUE;
        }
        return n;
    }
    
    private View findReferenceChildInternal(int i, final int n, final int n2) {
        this.ensureLayoutStateExpose();
        final int \u30078o8o\u3007 = this.mOrientationHelper.\u30078o8o\u3007();
        final int \u300780\u3007808\u3007O = this.mOrientationHelper.\u300780\u3007808\u3007O();
        int n3;
        if (n > i) {
            n3 = 1;
        }
        else {
            n3 = -1;
        }
        View view = null;
        View view2 = null;
        while (i != n) {
            final View child = ((RecyclerView.LayoutManager)this).getChildAt(i);
            final int position = ((RecyclerView.LayoutManager)this).getPosition(child);
            View view3 = view;
            View view4 = view2;
            if (position >= 0) {
                view3 = view;
                view4 = view2;
                if (position < n2) {
                    if (((LayoutParams)child.getLayoutParams()).isItemRemoved()) {
                        view3 = view;
                        if ((view4 = view2) == null) {
                            view4 = child;
                            view3 = view;
                        }
                    }
                    else {
                        if (this.mOrientationHelper.\u3007\u3007888(child) < \u300780\u3007808\u3007O && this.mOrientationHelper.O8(child) >= \u30078o8o\u3007) {
                            return child;
                        }
                        view3 = view;
                        view4 = view2;
                        if (view == null) {
                            view3 = child;
                            view4 = view2;
                        }
                    }
                }
            }
            i += n3;
            view = view3;
            view2 = view4;
        }
        if (view == null) {
            view = view2;
        }
        return view;
    }
    
    private int fixLayoutEndGapExpose(int n, final Recycler recycler, final State state, final boolean b) {
        final int n2 = this.mOrientationHelper.\u300780\u3007808\u3007O() - n;
        if (n2 > 0) {
            final int n3 = -this.scrollInternalBy(-n2, recycler, state);
            if (b) {
                n = this.mOrientationHelper.\u300780\u3007808\u3007O() - (n + n3);
                if (n > 0) {
                    this.mOrientationHelper.Oooo8o0\u3007(n);
                    return n + n3;
                }
            }
            return n3;
        }
        return 0;
    }
    
    private int fixLayoutStartGapExpose(int n, final Recycler recycler, final State state, final boolean b) {
        final int n2 = n - this.mOrientationHelper.\u30078o8o\u3007();
        if (n2 > 0) {
            int n4;
            final int n3 = n4 = -this.scrollInternalBy(n2, recycler, state);
            if (b) {
                n = n + n3 - this.mOrientationHelper.\u30078o8o\u3007();
                n4 = n3;
                if (n > 0) {
                    this.mOrientationHelper.Oooo8o0\u3007(-n);
                    n4 = n3 - n;
                }
            }
            return n4;
        }
        return 0;
    }
    
    private View getChildClosestToEndExpose() {
        int n;
        if (this.mShouldReverseLayoutExpose) {
            n = 0;
        }
        else {
            n = ((RecyclerView.LayoutManager)this).getChildCount() - 1;
        }
        return ((RecyclerView.LayoutManager)this).getChildAt(n);
    }
    
    private View getChildClosestToStartExpose() {
        int n;
        if (this.mShouldReverseLayoutExpose) {
            n = ((RecyclerView.LayoutManager)this).getChildCount() - 1;
        }
        else {
            n = 0;
        }
        return ((RecyclerView.LayoutManager)this).getChildAt(n);
    }
    
    protected static boolean isViewHolderUpdated(final ViewHolder viewHolder) {
        return new ViewHolderWrapper(viewHolder).O8();
    }
    
    private void layoutForPredictiveAnimationsExpose(final Recycler recycler, final State state, int n, int o\u30070) {
        if (state.willRunPredictiveAnimations() && ((RecyclerView.LayoutManager)this).getChildCount() != 0 && !state.isPreLayout()) {
            if (this.supportsPredictiveItemAnimations()) {
                final List<ViewHolder> scrapList = recycler.getScrapList();
                final int size = scrapList.size();
                final int position = ((RecyclerView.LayoutManager)this).getPosition(((RecyclerView.LayoutManager)this).getChildAt(0));
                int n2 = 0;
                int oo0o\u3007\u3007\u3007\u30070 = 0;
                int oo0o\u3007\u3007\u3007\u30072 = 0;
                int n3;
                while (true) {
                    n3 = -1;
                    final int n4 = 1;
                    if (n2 >= size) {
                        break;
                    }
                    final ViewHolder viewHolder = scrapList.get(n2);
                    final boolean b = viewHolder.getPosition() < position;
                    int n5 = n4;
                    if (b != this.mShouldReverseLayoutExpose) {
                        n5 = -1;
                    }
                    if (n5 == -1) {
                        oo0o\u3007\u3007\u3007\u30070 += this.mOrientationHelper.Oo08(viewHolder.itemView);
                    }
                    else {
                        oo0o\u3007\u3007\u3007\u30072 += this.mOrientationHelper.Oo08(viewHolder.itemView);
                    }
                    ++n2;
                }
                this.mLayoutState.OO0o\u3007\u3007 = scrapList;
                if (oo0o\u3007\u3007\u3007\u30070 > 0) {
                    this.updateLayoutStateToFillStartExpose(((RecyclerView.LayoutManager)this).getPosition(this.getChildClosestToStartExpose()), n);
                    final LayoutState mLayoutState = this.mLayoutState;
                    mLayoutState.OO0o\u3007\u3007\u3007\u30070 = oo0o\u3007\u3007\u3007\u30070;
                    mLayoutState.Oo08 = 0;
                    final int o\u30072 = mLayoutState.o\u30070;
                    if (this.mShouldReverseLayoutExpose) {
                        n = 1;
                    }
                    else {
                        n = -1;
                    }
                    mLayoutState.o\u30070 = o\u30072 + n;
                    mLayoutState.\u3007o00\u3007\u3007Oo = true;
                    this.fill(recycler, mLayoutState, state, false);
                }
                if (oo0o\u3007\u3007\u3007\u30072 > 0) {
                    this.updateLayoutStateToFillEndExpose(((RecyclerView.LayoutManager)this).getPosition(this.getChildClosestToEndExpose()), o\u30070);
                    final LayoutState mLayoutState2 = this.mLayoutState;
                    mLayoutState2.OO0o\u3007\u3007\u3007\u30070 = oo0o\u3007\u3007\u3007\u30072;
                    mLayoutState2.Oo08 = 0;
                    o\u30070 = mLayoutState2.o\u30070;
                    if (this.mShouldReverseLayoutExpose) {
                        n = n3;
                    }
                    else {
                        n = 1;
                    }
                    mLayoutState2.o\u30070 = o\u30070 + n;
                    mLayoutState2.\u3007o00\u3007\u3007Oo = true;
                    this.fill(recycler, mLayoutState2, state, false);
                }
                this.mLayoutState.OO0o\u3007\u3007 = null;
            }
        }
    }
    
    private void logChildren() {
        for (int i = 0; i < ((RecyclerView.LayoutManager)this).getChildCount(); ++i) {
            final View child = ((RecyclerView.LayoutManager)this).getChildAt(i);
            final StringBuilder sb = new StringBuilder();
            sb.append("item ");
            sb.append(((RecyclerView.LayoutManager)this).getPosition(child));
            sb.append(", coord:");
            sb.append(this.mOrientationHelper.\u3007\u3007888(child));
        }
    }
    
    private View myFindFirstReferenceChild(final int n) {
        return this.findReferenceChildInternal(0, ((RecyclerView.LayoutManager)this).getChildCount(), n);
    }
    
    private View myFindLastReferenceChild(final int n) {
        return this.findReferenceChildInternal(((RecyclerView.LayoutManager)this).getChildCount() - 1, -1, n);
    }
    
    private View myFindReferenceChildClosestToEnd(final State state) {
        final boolean mShouldReverseLayoutExpose = this.mShouldReverseLayoutExpose;
        final int itemCount = state.getItemCount();
        View view;
        if (mShouldReverseLayoutExpose) {
            view = this.myFindFirstReferenceChild(itemCount);
        }
        else {
            view = this.myFindLastReferenceChild(itemCount);
        }
        return view;
    }
    
    private View myFindReferenceChildClosestToStart(final State state) {
        final boolean mShouldReverseLayoutExpose = this.mShouldReverseLayoutExpose;
        final int itemCount = state.getItemCount();
        View view;
        if (mShouldReverseLayoutExpose) {
            view = this.myFindLastReferenceChild(itemCount);
        }
        else {
            view = this.myFindFirstReferenceChild(itemCount);
        }
        return view;
    }
    
    private void myResolveShouldLayoutReverse() {
        if (this.getOrientation() != 1 && this.isLayoutRTL()) {
            this.mShouldReverseLayoutExpose = (this.getReverseLayout() ^ true);
        }
        else {
            this.mShouldReverseLayoutExpose = this.getReverseLayout();
        }
    }
    
    private void recycleByLayoutStateExpose(final Recycler recycler, final LayoutState layoutState) {
        if (!layoutState.\u3007o\u3007) {
            return;
        }
        if (layoutState.oO80 == -1) {
            this.recycleViewsFromEndExpose(recycler, layoutState.\u300780\u3007808\u3007O);
        }
        else {
            this.recycleViewsFromStartExpose(recycler, layoutState.\u300780\u3007808\u3007O);
        }
    }
    
    private void recycleViewsFromEndExpose(final Recycler recycler, int i) {
        int childCount = ((RecyclerView.LayoutManager)this).getChildCount();
        if (i < 0) {
            return;
        }
        final int n = this.mOrientationHelper.oO80() - i;
        if (this.mShouldReverseLayoutExpose) {
            for (i = 0; i < childCount; ++i) {
                if (this.mOrientationHelper.\u3007\u3007888(((RecyclerView.LayoutManager)this).getChildAt(i)) - this.recycleOffset < n) {
                    this.recycleChildren(recycler, 0, i);
                    return;
                }
            }
        }
        else {
            for (i = --childCount; i >= 0; --i) {
                if (this.mOrientationHelper.\u3007\u3007888(((RecyclerView.LayoutManager)this).getChildAt(i)) - this.recycleOffset < n) {
                    this.recycleChildren(recycler, childCount, i);
                    return;
                }
            }
        }
    }
    
    private void recycleViewsFromStartExpose(final Recycler recycler, final int n) {
        if (n < 0) {
            return;
        }
        int childCount = ((RecyclerView.LayoutManager)this).getChildCount();
        if (this.mShouldReverseLayoutExpose) {
            for (int i = --childCount; i >= 0; --i) {
                if (this.mOrientationHelper.O8(((RecyclerView.LayoutManager)this).getChildAt(i)) + this.recycleOffset > n) {
                    this.recycleChildren(recycler, childCount, i);
                    return;
                }
            }
        }
        else {
            for (int j = 0; j < childCount; ++j) {
                if (this.mOrientationHelper.O8(((RecyclerView.LayoutManager)this).getChildAt(j)) + this.recycleOffset > n) {
                    this.recycleChildren(recycler, 0, j);
                    return;
                }
            }
        }
    }
    
    private boolean updateAnchorFromChildrenExpose(final State state, final AnchorInfo anchorInfo) {
        final int childCount = ((RecyclerView.LayoutManager)this).getChildCount();
        boolean b = false;
        if (childCount == 0) {
            return false;
        }
        final View focusedChild = ((RecyclerView.LayoutManager)this).getFocusedChild();
        if (focusedChild != null && anchorInfo.\u3007o\u3007(focusedChild, state)) {
            return true;
        }
        if (this.mLastStackFromEnd != this.getStackFromEnd()) {
            return false;
        }
        View view;
        if (anchorInfo.\u3007o\u3007) {
            view = this.myFindReferenceChildClosestToEnd(state);
        }
        else {
            view = this.myFindReferenceChildClosestToStart(state);
        }
        if (view != null) {
            anchorInfo.\u3007o00\u3007\u3007Oo(view);
            if (!state.isPreLayout() && this.supportsPredictiveItemAnimations()) {
                if (this.mOrientationHelper.\u3007\u3007888(view) >= this.mOrientationHelper.\u300780\u3007808\u3007O() || this.mOrientationHelper.O8(view) < this.mOrientationHelper.\u30078o8o\u3007()) {
                    b = true;
                }
                if (b) {
                    int \u3007o00\u3007\u3007Oo;
                    if (anchorInfo.\u3007o\u3007) {
                        \u3007o00\u3007\u3007Oo = this.mOrientationHelper.\u300780\u3007808\u3007O();
                    }
                    else {
                        \u3007o00\u3007\u3007Oo = this.mOrientationHelper.\u30078o8o\u3007();
                    }
                    anchorInfo.\u3007o00\u3007\u3007Oo = \u3007o00\u3007\u3007Oo;
                }
            }
            return true;
        }
        return false;
    }
    
    private boolean updateAnchorFromPendingDataExpose(final State state, final AnchorInfo anchorInfo) {
        final boolean preLayout = state.isPreLayout();
        boolean \u3007o\u3007 = false;
        if (!preLayout) {
            final int mCurrentPendingScrollPosition = this.mCurrentPendingScrollPosition;
            if (mCurrentPendingScrollPosition != -1) {
                if (mCurrentPendingScrollPosition >= 0 && mCurrentPendingScrollPosition < state.getItemCount()) {
                    anchorInfo.\u3007080 = this.mCurrentPendingScrollPosition;
                    final Bundle mCurrentPendingSavedState = this.mCurrentPendingSavedState;
                    if (mCurrentPendingSavedState != null && ((BaseBundle)mCurrentPendingSavedState).getInt("AnchorPosition") >= 0) {
                        final boolean boolean1 = this.mCurrentPendingSavedState.getBoolean("AnchorLayoutFromEnd");
                        anchorInfo.\u3007o\u3007 = boolean1;
                        if (boolean1) {
                            anchorInfo.\u3007o00\u3007\u3007Oo = this.mOrientationHelper.\u300780\u3007808\u3007O() - ((BaseBundle)this.mCurrentPendingSavedState).getInt("AnchorOffset");
                        }
                        else {
                            anchorInfo.\u3007o00\u3007\u3007Oo = this.mOrientationHelper.\u30078o8o\u3007() + ((BaseBundle)this.mCurrentPendingSavedState).getInt("AnchorOffset");
                        }
                        return true;
                    }
                    if (this.mPendingScrollPositionOffset == Integer.MIN_VALUE) {
                        final View viewByPosition = this.findViewByPosition(this.mCurrentPendingScrollPosition);
                        if (viewByPosition != null) {
                            if (this.mOrientationHelper.Oo08(viewByPosition) > this.mOrientationHelper.\u3007O8o08O()) {
                                anchorInfo.\u3007080();
                                return true;
                            }
                            if (this.mOrientationHelper.\u3007\u3007888(viewByPosition) - this.mOrientationHelper.\u30078o8o\u3007() < 0) {
                                anchorInfo.\u3007o00\u3007\u3007Oo = this.mOrientationHelper.\u30078o8o\u3007();
                                anchorInfo.\u3007o\u3007 = false;
                                return true;
                            }
                            if (this.mOrientationHelper.\u300780\u3007808\u3007O() - this.mOrientationHelper.O8(viewByPosition) < 0) {
                                anchorInfo.\u3007o00\u3007\u3007Oo = this.mOrientationHelper.\u300780\u3007808\u3007O();
                                return anchorInfo.\u3007o\u3007 = true;
                            }
                            int \u3007\u3007888;
                            if (anchorInfo.\u3007o\u3007) {
                                \u3007\u3007888 = this.mOrientationHelper.O8(viewByPosition) + this.mOrientationHelper.OO0o\u3007\u3007();
                            }
                            else {
                                \u3007\u3007888 = this.mOrientationHelper.\u3007\u3007888(viewByPosition);
                            }
                            anchorInfo.\u3007o00\u3007\u3007Oo = \u3007\u3007888;
                        }
                        else {
                            if (((RecyclerView.LayoutManager)this).getChildCount() > 0) {
                                if (this.mCurrentPendingScrollPosition < ((RecyclerView.LayoutManager)this).getPosition(((RecyclerView.LayoutManager)this).getChildAt(0)) == this.mShouldReverseLayoutExpose) {
                                    \u3007o\u3007 = true;
                                }
                                anchorInfo.\u3007o\u3007 = \u3007o\u3007;
                            }
                            anchorInfo.\u3007080();
                        }
                        return true;
                    }
                    final boolean mShouldReverseLayoutExpose = this.mShouldReverseLayoutExpose;
                    anchorInfo.\u3007o\u3007 = mShouldReverseLayoutExpose;
                    if (mShouldReverseLayoutExpose) {
                        anchorInfo.\u3007o00\u3007\u3007Oo = this.mOrientationHelper.\u300780\u3007808\u3007O() - this.mPendingScrollPositionOffset;
                    }
                    else {
                        anchorInfo.\u3007o00\u3007\u3007Oo = this.mOrientationHelper.\u30078o8o\u3007() + this.mPendingScrollPositionOffset;
                    }
                    return true;
                }
                else {
                    this.mCurrentPendingScrollPosition = -1;
                    this.mPendingScrollPositionOffset = Integer.MIN_VALUE;
                }
            }
        }
        return false;
    }
    
    private void updateAnchorInfoForLayoutExpose(final State state, final AnchorInfo anchorInfo) {
        if (this.updateAnchorFromPendingDataExpose(state, anchorInfo)) {
            return;
        }
        if (this.updateAnchorFromChildrenExpose(state, anchorInfo)) {
            return;
        }
        anchorInfo.\u3007080();
        int \u3007080;
        if (this.getStackFromEnd()) {
            \u3007080 = state.getItemCount() - 1;
        }
        else {
            \u3007080 = 0;
        }
        anchorInfo.\u3007080 = \u3007080;
    }
    
    private void updateLayoutStateToFillEndExpose(final int o\u30070, final int o8) {
        this.mLayoutState.Oo08 = this.mOrientationHelper.\u300780\u3007808\u3007O() - o8;
        final LayoutState mLayoutState = this.mLayoutState;
        int \u3007\u3007888;
        if (this.mShouldReverseLayoutExpose) {
            \u3007\u3007888 = -1;
        }
        else {
            \u3007\u3007888 = 1;
        }
        mLayoutState.\u3007\u3007888 = \u3007\u3007888;
        mLayoutState.o\u30070 = o\u30070;
        mLayoutState.oO80 = 1;
        mLayoutState.O8 = o8;
        mLayoutState.\u300780\u3007808\u3007O = Integer.MIN_VALUE;
    }
    
    private void updateLayoutStateToFillEndExpose(final AnchorInfo anchorInfo) {
        this.updateLayoutStateToFillEndExpose(anchorInfo.\u3007080, anchorInfo.\u3007o00\u3007\u3007Oo);
    }
    
    private void updateLayoutStateToFillStartExpose(int n, final int o8) {
        this.mLayoutState.Oo08 = o8 - this.mOrientationHelper.\u30078o8o\u3007();
        final LayoutState mLayoutState = this.mLayoutState;
        mLayoutState.o\u30070 = n;
        if (this.mShouldReverseLayoutExpose) {
            n = 1;
        }
        else {
            n = -1;
        }
        mLayoutState.\u3007\u3007888 = n;
        mLayoutState.oO80 = -1;
        mLayoutState.O8 = o8;
        mLayoutState.\u300780\u3007808\u3007O = Integer.MIN_VALUE;
    }
    
    private void updateLayoutStateToFillStartExpose(final AnchorInfo anchorInfo) {
        this.updateLayoutStateToFillStartExpose(anchorInfo.\u3007080, anchorInfo.\u3007o00\u3007\u3007Oo);
    }
    
    private void validateChildOrderExpose() {
        final StringBuilder sb = new StringBuilder();
        sb.append("validating child count ");
        sb.append(((RecyclerView.LayoutManager)this).getChildCount());
        final int childCount = ((RecyclerView.LayoutManager)this).getChildCount();
        boolean b = true;
        final boolean b2 = true;
        if (childCount < 1) {
            return;
        }
        final int position = ((RecyclerView.LayoutManager)this).getPosition(((RecyclerView.LayoutManager)this).getChildAt(0));
        final int \u3007\u3007888 = this.mOrientationHelper.\u3007\u3007888(((RecyclerView.LayoutManager)this).getChildAt(0));
        if (this.mShouldReverseLayoutExpose) {
            for (int i = 1; i < ((RecyclerView.LayoutManager)this).getChildCount(); ++i) {
                final View child = ((RecyclerView.LayoutManager)this).getChildAt(i);
                final int position2 = ((RecyclerView.LayoutManager)this).getPosition(child);
                final int \u3007\u3007889 = this.mOrientationHelper.\u3007\u3007888(child);
                if (position2 < position) {
                    this.logChildren();
                    final StringBuilder sb2 = new StringBuilder();
                    sb2.append("detected invalid position. loc invalid? ");
                    sb2.append(\u3007\u3007889 < \u3007\u3007888 && b2);
                    throw new RuntimeException(sb2.toString());
                }
                if (\u3007\u3007889 > \u3007\u3007888) {
                    this.logChildren();
                    throw new RuntimeException("detected invalid location");
                }
            }
        }
        else {
            for (int j = 1; j < ((RecyclerView.LayoutManager)this).getChildCount(); ++j) {
                final View child2 = ((RecyclerView.LayoutManager)this).getChildAt(j);
                final int position3 = ((RecyclerView.LayoutManager)this).getPosition(child2);
                final int \u3007\u3007890 = this.mOrientationHelper.\u3007\u3007888(child2);
                if (position3 < position) {
                    this.logChildren();
                    final StringBuilder sb3 = new StringBuilder();
                    sb3.append("detected invalid position. loc invalid? ");
                    if (\u3007\u3007890 >= \u3007\u3007888) {
                        b = false;
                    }
                    sb3.append(b);
                    throw new RuntimeException(sb3.toString());
                }
                if (\u3007\u3007890 < \u3007\u3007888) {
                    this.logChildren();
                    throw new RuntimeException("detected invalid location");
                }
            }
        }
    }
    
    protected void addHiddenView(final View view, final boolean b) {
        int n;
        if (b) {
            n = 0;
        }
        else {
            n = -1;
        }
        ((RecyclerView.LayoutManager)this).addView(view, n);
        this.mChildHelperWrapper.\u3007o\u3007(view);
    }
    
    @Override
    public void assertNotInLayoutOrScroll(final String s) {
        if (this.mCurrentPendingSavedState == null) {
            super.assertNotInLayoutOrScroll(s);
        }
    }
    
    protected int computeAlignOffset(final int n, final boolean b, final boolean b2) {
        return 0;
    }
    
    protected int computeAlignOffset(final View view, final boolean b, final boolean b2) {
        return 0;
    }
    
    @Override
    public PointF computeScrollVectorForPosition(int n) {
        if (((RecyclerView.LayoutManager)this).getChildCount() == 0) {
            return null;
        }
        boolean b = false;
        final int position = ((RecyclerView.LayoutManager)this).getPosition(((RecyclerView.LayoutManager)this).getChildAt(0));
        final int n2 = 1;
        if (n < position) {
            b = true;
        }
        n = n2;
        if (b != this.mShouldReverseLayoutExpose) {
            n = -1;
        }
        if (this.getOrientation() == 0) {
            return new PointF((float)n, 0.0f);
        }
        return new PointF(0.0f, (float)n);
    }
    
    protected void ensureLayoutStateExpose() {
        if (this.mLayoutState == null) {
            this.mLayoutState = new LayoutState();
        }
        if (this.mOrientationHelper == null) {
            this.mOrientationHelper = OrientationHelperEx.\u3007o00\u3007\u3007Oo(this, this.getOrientation());
        }
        try {
            this.mEnsureLayoutStateMethod.invoke(this, this.emptyArgs);
        }
        catch (final InvocationTargetException ex) {
            ex.printStackTrace();
        }
        catch (final IllegalAccessException ex2) {
            ex2.printStackTrace();
        }
    }
    
    protected int fill(final Recycler recycler, final LayoutState layoutState, final State state, final boolean b) {
        final int oo08 = layoutState.Oo08;
        final int \u300780\u3007808\u3007O = layoutState.\u300780\u3007808\u3007O;
        if (\u300780\u3007808\u3007O != Integer.MIN_VALUE) {
            if (oo08 < 0) {
                layoutState.\u300780\u3007808\u3007O = \u300780\u3007808\u3007O + oo08;
            }
            this.recycleByLayoutStateExpose(recycler, layoutState);
        }
        int n = layoutState.Oo08 + layoutState.OO0o\u3007\u3007\u3007\u30070 + this.recycleOffset;
        while (n > 0 && layoutState.\u3007080(state)) {
            this.layoutChunkResultCache.\u3007080();
            this.layoutChunk(recycler, state, layoutState, this.layoutChunkResultCache);
            final com.alibaba.android.vlayout.layout.LayoutChunkResult layoutChunkResultCache = this.layoutChunkResultCache;
            if (layoutChunkResultCache.\u3007o00\u3007\u3007Oo) {
                break;
            }
            layoutState.O8 += layoutChunkResultCache.\u3007080 * layoutState.oO80;
            int n2 = 0;
            Label_0182: {
                if (layoutChunkResultCache.\u3007o\u3007 && this.mLayoutState.OO0o\u3007\u3007 == null) {
                    n2 = n;
                    if (state.isPreLayout()) {
                        break Label_0182;
                    }
                }
                final int oo9 = layoutState.Oo08;
                final int \u3007080 = this.layoutChunkResultCache.\u3007080;
                layoutState.Oo08 = oo9 - \u3007080;
                n2 = n - \u3007080;
            }
            final int \u300780\u3007808\u3007O2 = layoutState.\u300780\u3007808\u3007O;
            if (\u300780\u3007808\u3007O2 != Integer.MIN_VALUE) {
                final int \u300780\u3007808\u3007O3 = \u300780\u3007808\u3007O2 + this.layoutChunkResultCache.\u3007080;
                layoutState.\u300780\u3007808\u3007O = \u300780\u3007808\u3007O3;
                final int oo10 = layoutState.Oo08;
                if (oo10 < 0) {
                    layoutState.\u300780\u3007808\u3007O = \u300780\u3007808\u3007O3 + oo10;
                }
                this.recycleByLayoutStateExpose(recycler, layoutState);
            }
            n = n2;
            if (!b) {
                continue;
            }
            n = n2;
            if (this.layoutChunkResultCache.O8) {
                break;
            }
        }
        return oo08 - layoutState.Oo08;
    }
    
    @Override
    public int findFirstVisibleItemPosition() {
        this.ensureLayoutStateExpose();
        return super.findFirstVisibleItemPosition();
    }
    
    protected View findHiddenView(final int n) {
        return this.mChildHelperWrapper.\u3007o00\u3007\u3007Oo(n, -1);
    }
    
    @Override
    public int findLastVisibleItemPosition() {
        this.ensureLayoutStateExpose();
        try {
            return super.findLastVisibleItemPosition();
        }
        catch (final Exception ex) {
            final StringBuilder sb = new StringBuilder();
            sb.append("itemCount: ");
            sb.append(((RecyclerView.LayoutManager)this).getItemCount());
            final StringBuilder sb2 = new StringBuilder();
            sb2.append("childCount: ");
            sb2.append(((RecyclerView.LayoutManager)this).getChildCount());
            final StringBuilder sb3 = new StringBuilder();
            sb3.append("child: ");
            sb3.append(((RecyclerView.LayoutManager)this).getChildAt(((RecyclerView.LayoutManager)this).getChildCount() - 1));
            final StringBuilder sb4 = new StringBuilder();
            sb4.append("RV childCount: ");
            sb4.append(this.mRecyclerView.getChildCount());
            final StringBuilder sb5 = new StringBuilder();
            sb5.append("RV child: ");
            sb5.append(this.mRecyclerView.getChildAt(this.mRecyclerView.getChildCount() - 1));
            throw ex;
        }
    }
    
    protected void hideView(final View view) {
        this.mChildHelperWrapper.\u3007o\u3007(view);
    }
    
    public boolean isEnableMarginOverLap() {
        return false;
    }
    
    protected boolean isHidden(final View view) {
        return this.mChildHelperWrapper.O8(view);
    }
    
    protected void layoutChunk(final Recycler recycler, final State state, final LayoutState layoutState, final com.alibaba.android.vlayout.layout.LayoutChunkResult layoutChunkResult) {
        final View \u3007o00\u3007\u3007Oo = layoutState.\u3007o00\u3007\u3007Oo(recycler);
        if (\u3007o00\u3007\u3007Oo == null) {
            layoutChunkResult.\u3007o00\u3007\u3007Oo = true;
            return;
        }
        final LayoutParams layoutParams = (LayoutParams)\u3007o00\u3007\u3007Oo.getLayoutParams();
        if (layoutState.OO0o\u3007\u3007 == null) {
            if (this.mShouldReverseLayoutExpose == (layoutState.oO80 == -1)) {
                ((RecyclerView.LayoutManager)this).addView(\u3007o00\u3007\u3007Oo);
            }
            else {
                ((RecyclerView.LayoutManager)this).addView(\u3007o00\u3007\u3007Oo, 0);
            }
        }
        else if (this.mShouldReverseLayoutExpose == (layoutState.oO80 == -1)) {
            ((RecyclerView.LayoutManager)this).addDisappearingView(\u3007o00\u3007\u3007Oo);
        }
        else {
            ((RecyclerView.LayoutManager)this).addDisappearingView(\u3007o00\u3007\u3007Oo, 0);
        }
        ((RecyclerView.LayoutManager)this).measureChildWithMargins(\u3007o00\u3007\u3007Oo, 0, 0);
        layoutChunkResult.\u3007080 = this.mOrientationHelper.Oo08(\u3007o00\u3007\u3007Oo);
        int n;
        int n2;
        int n3;
        int n4;
        if (this.getOrientation() == 1) {
            int paddingLeft;
            if (this.isLayoutRTL()) {
                n = ((RecyclerView.LayoutManager)this).getWidth() - ((RecyclerView.LayoutManager)this).getPaddingRight();
                paddingLeft = n - this.mOrientationHelper.o\u30070(\u3007o00\u3007\u3007Oo);
            }
            else {
                paddingLeft = ((RecyclerView.LayoutManager)this).getPaddingLeft();
                n = this.mOrientationHelper.o\u30070(\u3007o00\u3007\u3007Oo) + paddingLeft;
            }
            if (layoutState.oO80 == -1) {
                final int o8 = layoutState.O8;
                n2 = o8 - layoutChunkResult.\u3007080;
                n3 = paddingLeft;
                n4 = o8;
            }
            else {
                n2 = layoutState.O8;
                final int n5 = layoutChunkResult.\u3007080 + n2;
                n3 = paddingLeft;
                n4 = n5;
            }
        }
        else {
            n2 = ((RecyclerView.LayoutManager)this).getPaddingTop();
            n4 = this.mOrientationHelper.o\u30070(\u3007o00\u3007\u3007Oo) + n2;
            if (layoutState.oO80 == -1) {
                final int o9 = layoutState.O8;
                final int \u3007080 = layoutChunkResult.\u3007080;
                n = o9;
                n3 = o9 - \u3007080;
            }
            else {
                final int o10 = layoutState.O8;
                final int \u300781 = layoutChunkResult.\u3007080;
                n3 = o10;
                n = \u300781 + o10;
            }
        }
        ((RecyclerView.LayoutManager)this).layoutDecorated(\u3007o00\u3007\u3007Oo, n3 + layoutParams.leftMargin, layoutParams.topMargin + n2, n - layoutParams.rightMargin, n4 - layoutParams.bottomMargin);
        if (layoutParams.isItemRemoved() || layoutParams.isItemChanged()) {
            layoutChunkResult.\u3007o\u3007 = true;
        }
        layoutChunkResult.O8 = \u3007o00\u3007\u3007Oo.isFocusable();
    }
    
    public void onAnchorReady(final State state, final AnchorInfo anchorInfo) {
    }
    
    @Override
    public void onAttachedToWindow(final RecyclerView mRecyclerView) {
        super.onAttachedToWindow(mRecyclerView);
        this.mRecyclerView = mRecyclerView;
    }
    
    @Override
    public void onDetachedFromWindow(final RecyclerView recyclerView, final Recycler recycler) {
        super.onDetachedFromWindow(recyclerView, recycler);
        this.mRecyclerView = null;
    }
    
    @Override
    public View onFocusSearchFailed(View view, int convertFocusDirectionToLayoutDirectionExpose, final Recycler recycler, final State state) {
        this.myResolveShouldLayoutReverse();
        if (((RecyclerView.LayoutManager)this).getChildCount() == 0) {
            return null;
        }
        convertFocusDirectionToLayoutDirectionExpose = this.convertFocusDirectionToLayoutDirectionExpose(convertFocusDirectionToLayoutDirectionExpose);
        if (convertFocusDirectionToLayoutDirectionExpose == Integer.MIN_VALUE) {
            return null;
        }
        if (convertFocusDirectionToLayoutDirectionExpose == -1) {
            view = this.myFindReferenceChildClosestToStart(state);
        }
        else {
            view = this.myFindReferenceChildClosestToEnd(state);
        }
        if (view == null) {
            return null;
        }
        this.ensureLayoutStateExpose();
        this.updateLayoutStateExpose(convertFocusDirectionToLayoutDirectionExpose, (int)(this.mOrientationHelper.\u3007O8o08O() * 0.33f), false, state);
        final LayoutState mLayoutState = this.mLayoutState;
        mLayoutState.\u300780\u3007808\u3007O = Integer.MIN_VALUE;
        mLayoutState.\u3007o\u3007 = false;
        mLayoutState.\u3007o00\u3007\u3007Oo = false;
        this.fill(recycler, mLayoutState, state, true);
        View view2;
        if (convertFocusDirectionToLayoutDirectionExpose == -1) {
            view2 = this.getChildClosestToStartExpose();
        }
        else {
            view2 = this.getChildClosestToEndExpose();
        }
        if (view2 != view && view2.isFocusable()) {
            return view2;
        }
        return null;
    }
    
    @Override
    public void onLayoutChildren(final Recycler recycler, final State state) {
        final Bundle mCurrentPendingSavedState = this.mCurrentPendingSavedState;
        if (mCurrentPendingSavedState != null && ((BaseBundle)mCurrentPendingSavedState).getInt("AnchorPosition") >= 0) {
            this.mCurrentPendingScrollPosition = ((BaseBundle)this.mCurrentPendingSavedState).getInt("AnchorPosition");
        }
        this.ensureLayoutStateExpose();
        this.mLayoutState.\u3007o\u3007 = false;
        this.myResolveShouldLayoutReverse();
        this.mAnchorInfo.O8();
        this.mAnchorInfo.\u3007o\u3007 = (this.mShouldReverseLayoutExpose ^ this.getStackFromEnd());
        this.updateAnchorInfoForLayoutExpose(state, this.mAnchorInfo);
        int extraLayoutSpace = this.getExtraLayoutSpace(state);
        int n;
        if (state.getTargetScrollPosition() < this.mAnchorInfo.\u3007080 == this.mShouldReverseLayoutExpose) {
            n = extraLayoutSpace;
            extraLayoutSpace = 0;
        }
        else {
            n = 0;
        }
        final int n2 = extraLayoutSpace + this.mOrientationHelper.\u30078o8o\u3007();
        final int n3 = n + this.mOrientationHelper.OO0o\u3007\u3007\u3007\u30070();
        int oo0o\u3007\u3007\u3007\u30070 = n2;
        int oo0o\u3007\u3007\u3007\u30072 = n3;
        if (state.isPreLayout()) {
            final int mCurrentPendingScrollPosition = this.mCurrentPendingScrollPosition;
            oo0o\u3007\u3007\u3007\u30070 = n2;
            oo0o\u3007\u3007\u3007\u30072 = n3;
            if (mCurrentPendingScrollPosition != -1) {
                oo0o\u3007\u3007\u3007\u30070 = n2;
                oo0o\u3007\u3007\u3007\u30072 = n3;
                if (this.mPendingScrollPositionOffset != Integer.MIN_VALUE) {
                    final View viewByPosition = this.findViewByPosition(mCurrentPendingScrollPosition);
                    oo0o\u3007\u3007\u3007\u30070 = n2;
                    oo0o\u3007\u3007\u3007\u30072 = n3;
                    if (viewByPosition != null) {
                        int mPendingScrollPositionOffset;
                        int mPendingScrollPositionOffset2;
                        if (this.mShouldReverseLayoutExpose) {
                            mPendingScrollPositionOffset = this.mOrientationHelper.\u300780\u3007808\u3007O() - this.mOrientationHelper.O8(viewByPosition);
                            mPendingScrollPositionOffset2 = this.mPendingScrollPositionOffset;
                        }
                        else {
                            mPendingScrollPositionOffset2 = this.mOrientationHelper.\u3007\u3007888(viewByPosition) - this.mOrientationHelper.\u30078o8o\u3007();
                            mPendingScrollPositionOffset = this.mPendingScrollPositionOffset;
                        }
                        final int n4 = mPendingScrollPositionOffset - mPendingScrollPositionOffset2;
                        if (n4 > 0) {
                            oo0o\u3007\u3007\u3007\u30070 = n2 + n4;
                            oo0o\u3007\u3007\u3007\u30072 = n3;
                        }
                        else {
                            oo0o\u3007\u3007\u3007\u30072 = n3 - n4;
                            oo0o\u3007\u3007\u3007\u30070 = n2;
                        }
                    }
                }
            }
        }
        this.onAnchorReady(state, this.mAnchorInfo);
        ((RecyclerView.LayoutManager)this).detachAndScrapAttachedViews(recycler);
        this.mLayoutState.\u3007O8o08O = state.isPreLayout();
        this.mLayoutState.\u3007o00\u3007\u3007Oo = true;
        final AnchorInfo mAnchorInfo = this.mAnchorInfo;
        int o9;
        int o10;
        if (mAnchorInfo.\u3007o\u3007) {
            this.updateLayoutStateToFillStartExpose(mAnchorInfo);
            final LayoutState mLayoutState = this.mLayoutState;
            mLayoutState.OO0o\u3007\u3007\u3007\u30070 = oo0o\u3007\u3007\u3007\u30070;
            this.fill(recycler, mLayoutState, state, false);
            final LayoutState mLayoutState2 = this.mLayoutState;
            final int o8 = mLayoutState2.O8;
            final int oo08 = mLayoutState2.Oo08;
            int oo0o\u3007\u3007\u3007\u30073 = oo0o\u3007\u3007\u3007\u30072;
            if (oo08 > 0) {
                oo0o\u3007\u3007\u3007\u30073 = oo0o\u3007\u3007\u3007\u30072 + oo08;
            }
            this.updateLayoutStateToFillEndExpose(this.mAnchorInfo);
            final LayoutState mLayoutState3 = this.mLayoutState;
            mLayoutState3.OO0o\u3007\u3007\u3007\u30070 = oo0o\u3007\u3007\u3007\u30073;
            mLayoutState3.o\u30070 += mLayoutState3.\u3007\u3007888;
            this.fill(recycler, mLayoutState3, state, false);
            o9 = this.mLayoutState.O8;
            o10 = o8;
        }
        else {
            this.updateLayoutStateToFillEndExpose(mAnchorInfo);
            final LayoutState mLayoutState4 = this.mLayoutState;
            mLayoutState4.OO0o\u3007\u3007\u3007\u30070 = oo0o\u3007\u3007\u3007\u30072;
            this.fill(recycler, mLayoutState4, state, false);
            final LayoutState mLayoutState5 = this.mLayoutState;
            final int o11 = mLayoutState5.O8;
            final int oo9 = mLayoutState5.Oo08;
            int oo0o\u3007\u3007\u3007\u30074 = oo0o\u3007\u3007\u3007\u30070;
            if (oo9 > 0) {
                oo0o\u3007\u3007\u3007\u30074 = oo0o\u3007\u3007\u3007\u30070 + oo9;
            }
            this.updateLayoutStateToFillStartExpose(this.mAnchorInfo);
            final LayoutState mLayoutState6 = this.mLayoutState;
            mLayoutState6.OO0o\u3007\u3007\u3007\u30070 = oo0o\u3007\u3007\u3007\u30074;
            mLayoutState6.o\u30070 += mLayoutState6.\u3007\u3007888;
            this.fill(recycler, mLayoutState6, state, false);
            o10 = this.mLayoutState.O8;
            o9 = o11;
        }
        int n5 = o9;
        int n6 = o10;
        if (((RecyclerView.LayoutManager)this).getChildCount() > 0) {
            int n7;
            int n8;
            int n9;
            if (this.mShouldReverseLayoutExpose ^ this.getStackFromEnd()) {
                final int fixLayoutEndGapExpose = this.fixLayoutEndGapExpose(o9, recycler, state, true);
                n7 = o10 + fixLayoutEndGapExpose;
                n8 = o9 + fixLayoutEndGapExpose;
                n9 = this.fixLayoutStartGapExpose(n7, recycler, state, false);
            }
            else {
                final int fixLayoutStartGapExpose = this.fixLayoutStartGapExpose(o10, recycler, state, true);
                n7 = o10 + fixLayoutStartGapExpose;
                n8 = o9 + fixLayoutStartGapExpose;
                n9 = this.fixLayoutEndGapExpose(n8, recycler, state, false);
            }
            n6 = n7 + n9;
            n5 = n8 + n9;
        }
        this.layoutForPredictiveAnimationsExpose(recycler, state, n6, n5);
        if (!state.isPreLayout()) {
            this.mCurrentPendingScrollPosition = -1;
            this.mPendingScrollPositionOffset = Integer.MIN_VALUE;
            this.mOrientationHelper.\u3007\u3007808\u3007();
        }
        this.mLastStackFromEnd = this.getStackFromEnd();
        this.mCurrentPendingSavedState = null;
    }
    
    @Override
    public void onRestoreInstanceState(final Parcelable parcelable) {
        if (parcelable instanceof Bundle) {
            this.mCurrentPendingSavedState = (Bundle)parcelable;
            ((RecyclerView.LayoutManager)this).requestLayout();
        }
    }
    
    @Override
    public Parcelable onSaveInstanceState() {
        if (this.mCurrentPendingSavedState != null) {
            return (Parcelable)new Bundle(this.mCurrentPendingSavedState);
        }
        final Bundle bundle = new Bundle();
        if (((RecyclerView.LayoutManager)this).getChildCount() > 0) {
            final boolean b = this.mLastStackFromEnd ^ this.mShouldReverseLayoutExpose;
            bundle.putBoolean("AnchorLayoutFromEnd", b);
            if (b) {
                final View childClosestToEndExpose = this.getChildClosestToEndExpose();
                ((BaseBundle)bundle).putInt("AnchorOffset", this.mOrientationHelper.\u300780\u3007808\u3007O() - this.mOrientationHelper.O8(childClosestToEndExpose));
                ((BaseBundle)bundle).putInt("AnchorPosition", ((RecyclerView.LayoutManager)this).getPosition(childClosestToEndExpose));
            }
            else {
                final View childClosestToStartExpose = this.getChildClosestToStartExpose();
                ((BaseBundle)bundle).putInt("AnchorPosition", ((RecyclerView.LayoutManager)this).getPosition(childClosestToStartExpose));
                ((BaseBundle)bundle).putInt("AnchorOffset", this.mOrientationHelper.\u3007\u3007888(childClosestToStartExpose) - this.mOrientationHelper.\u30078o8o\u3007());
            }
        }
        else {
            ((BaseBundle)bundle).putInt("AnchorPosition", -1);
        }
        return (Parcelable)bundle;
    }
    
    protected void recycleChildren(final Recycler recycler, final int n, int i) {
        if (n == i) {
            return;
        }
        int j;
        if (i > (j = n)) {
            --i;
            while (i >= n) {
                ((RecyclerView.LayoutManager)this).removeAndRecycleViewAt(i, recycler);
                --i;
            }
        }
        else {
            while (j > i) {
                ((RecyclerView.LayoutManager)this).removeAndRecycleViewAt(j, recycler);
                --j;
            }
        }
    }
    
    @Override
    public int scrollHorizontallyBy(final int n, final Recycler recycler, final State state) {
        if (this.getOrientation() == 1) {
            return 0;
        }
        return this.scrollInternalBy(n, recycler, state);
    }
    
    protected int scrollInternalBy(int a, final Recycler recycler, final State state) {
        if (((RecyclerView.LayoutManager)this).getChildCount() == 0 || a == 0) {
            return 0;
        }
        this.mLayoutState.\u3007o\u3007 = true;
        this.ensureLayoutStateExpose();
        int n;
        if (a > 0) {
            n = 1;
        }
        else {
            n = -1;
        }
        final int abs = Math.abs(a);
        this.updateLayoutStateExpose(n, abs, true, state);
        final LayoutState mLayoutState = this.mLayoutState;
        final int \u300780\u3007808\u3007O = mLayoutState.\u300780\u3007808\u3007O;
        mLayoutState.\u3007o00\u3007\u3007Oo = false;
        final int n2 = \u300780\u3007808\u3007O + this.fill(recycler, mLayoutState, state, false);
        if (n2 < 0) {
            return 0;
        }
        if (abs > n2) {
            a = n * n2;
        }
        this.mOrientationHelper.Oooo8o0\u3007(-a);
        return a;
    }
    
    @Override
    public void scrollToPosition(final int mCurrentPendingScrollPosition) {
        this.mCurrentPendingScrollPosition = mCurrentPendingScrollPosition;
        this.mPendingScrollPositionOffset = Integer.MIN_VALUE;
        final Bundle mCurrentPendingSavedState = this.mCurrentPendingSavedState;
        if (mCurrentPendingSavedState != null) {
            ((BaseBundle)mCurrentPendingSavedState).putInt("AnchorPosition", -1);
        }
        ((RecyclerView.LayoutManager)this).requestLayout();
    }
    
    @Override
    public void scrollToPositionWithOffset(final int mCurrentPendingScrollPosition, final int mPendingScrollPositionOffset) {
        this.mCurrentPendingScrollPosition = mCurrentPendingScrollPosition;
        this.mPendingScrollPositionOffset = mPendingScrollPositionOffset;
        final Bundle mCurrentPendingSavedState = this.mCurrentPendingSavedState;
        if (mCurrentPendingSavedState != null) {
            ((BaseBundle)mCurrentPendingSavedState).putInt("AnchorPosition", -1);
        }
        ((RecyclerView.LayoutManager)this).requestLayout();
    }
    
    @Override
    public int scrollVerticallyBy(final int n, final Recycler recycler, final State state) {
        if (this.getOrientation() == 0) {
            return 0;
        }
        return this.scrollInternalBy(n, recycler, state);
    }
    
    @Override
    public void setOrientation(final int orientation) {
        super.setOrientation(orientation);
        this.mOrientationHelper = null;
    }
    
    public void setRecycleOffset(final int recycleOffset) {
        this.recycleOffset = recycleOffset;
    }
    
    protected void showView(final View view) {
        this.mChildHelperWrapper.Oo08(view);
    }
    
    @Override
    public boolean supportsPredictiveItemAnimations() {
        return this.mCurrentPendingSavedState == null && this.mLastStackFromEnd == this.getStackFromEnd();
    }
    
    protected void updateLayoutStateExpose(int n, final int oo08, final boolean b, final State state) {
        this.mLayoutState.OO0o\u3007\u3007\u3007\u30070 = this.getExtraLayoutSpace(state);
        final LayoutState mLayoutState = this.mLayoutState;
        mLayoutState.oO80 = n;
        int n2 = -1;
        if (n == 1) {
            mLayoutState.OO0o\u3007\u3007\u3007\u30070 += this.mOrientationHelper.OO0o\u3007\u3007\u3007\u30070();
            final View childClosestToEndExpose = this.getChildClosestToEndExpose();
            final LayoutState mLayoutState2 = this.mLayoutState;
            if (!this.mShouldReverseLayoutExpose) {
                n2 = 1;
            }
            mLayoutState2.\u3007\u3007888 = n2;
            n = ((RecyclerView.LayoutManager)this).getPosition(childClosestToEndExpose);
            final LayoutState mLayoutState3 = this.mLayoutState;
            mLayoutState2.o\u30070 = n + mLayoutState3.\u3007\u3007888;
            mLayoutState3.O8 = this.mOrientationHelper.O8(childClosestToEndExpose) + this.computeAlignOffset(childClosestToEndExpose, true, false);
            n = this.mLayoutState.O8 - this.mOrientationHelper.\u300780\u3007808\u3007O();
        }
        else {
            final View childClosestToStartExpose = this.getChildClosestToStartExpose();
            final LayoutState mLayoutState4 = this.mLayoutState;
            mLayoutState4.OO0o\u3007\u3007\u3007\u30070 += this.mOrientationHelper.\u30078o8o\u3007();
            final LayoutState mLayoutState5 = this.mLayoutState;
            if (this.mShouldReverseLayoutExpose) {
                n2 = 1;
            }
            mLayoutState5.\u3007\u3007888 = n2;
            n = ((RecyclerView.LayoutManager)this).getPosition(childClosestToStartExpose);
            final LayoutState mLayoutState6 = this.mLayoutState;
            mLayoutState5.o\u30070 = n + mLayoutState6.\u3007\u3007888;
            mLayoutState6.O8 = this.mOrientationHelper.\u3007\u3007888(childClosestToStartExpose) + this.computeAlignOffset(childClosestToStartExpose, false, false);
            n = -this.mLayoutState.O8 + this.mOrientationHelper.\u30078o8o\u3007();
        }
        final LayoutState mLayoutState7 = this.mLayoutState;
        mLayoutState7.Oo08 = oo08;
        if (b) {
            mLayoutState7.Oo08 = oo08 - n;
        }
        mLayoutState7.\u300780\u3007808\u3007O = n;
    }
    
    protected class AnchorInfo
    {
        final ExposeLinearLayoutManagerEx O8;
        public int \u3007080;
        public int \u3007o00\u3007\u3007Oo;
        public boolean \u3007o\u3007;
        
        protected AnchorInfo(final ExposeLinearLayoutManagerEx o8) {
            this.O8 = o8;
        }
        
        void O8() {
            this.\u3007080 = -1;
            this.\u3007o00\u3007\u3007Oo = Integer.MIN_VALUE;
            this.\u3007o\u3007 = false;
        }
        
        @Override
        public String toString() {
            final StringBuilder sb = new StringBuilder();
            sb.append("AnchorInfo{mPosition=");
            sb.append(this.\u3007080);
            sb.append(", mCoordinate=");
            sb.append(this.\u3007o00\u3007\u3007Oo);
            sb.append(", mLayoutFromEnd=");
            sb.append(this.\u3007o\u3007);
            sb.append('}');
            return sb.toString();
        }
        
        void \u3007080() {
            int \u3007o00\u3007\u3007Oo;
            if (this.\u3007o\u3007) {
                \u3007o00\u3007\u3007Oo = this.O8.mOrientationHelper.\u300780\u3007808\u3007O();
            }
            else {
                \u3007o00\u3007\u3007Oo = this.O8.mOrientationHelper.\u30078o8o\u3007();
            }
            this.\u3007o00\u3007\u3007Oo = \u3007o00\u3007\u3007Oo;
        }
        
        public void \u3007o00\u3007\u3007Oo(final View view) {
            if (this.\u3007o\u3007) {
                this.\u3007o00\u3007\u3007Oo = this.O8.mOrientationHelper.O8(view) + this.O8.computeAlignOffset(view, this.\u3007o\u3007, true) + this.O8.mOrientationHelper.OO0o\u3007\u3007();
            }
            else {
                this.\u3007o00\u3007\u3007Oo = this.O8.mOrientationHelper.\u3007\u3007888(view) + this.O8.computeAlignOffset(view, this.\u3007o\u3007, true);
            }
            this.\u3007080 = ((RecyclerView.LayoutManager)this.O8).getPosition(view);
        }
        
        public boolean \u3007o\u3007(final View view, final State state) {
            final LayoutParams layoutParams = (LayoutParams)view.getLayoutParams();
            if (!layoutParams.isItemRemoved() && layoutParams.getViewPosition() >= 0 && layoutParams.getViewPosition() < state.getItemCount()) {
                this.\u3007o00\u3007\u3007Oo(view);
                return true;
            }
            return false;
        }
    }
    
    class ChildHelperWrapper
    {
        private Method O8;
        final ExposeLinearLayoutManagerEx OO0o\u3007\u3007;
        private List OO0o\u3007\u3007\u3007\u30070;
        private Method Oo08;
        private Method oO80;
        private Field o\u30070;
        private Object \u3007080;
        private Field \u300780\u3007808\u3007O;
        private LayoutManager \u30078o8o\u3007;
        private Object[] \u3007O8o08O;
        private Method \u3007o00\u3007\u3007Oo;
        private Method \u3007o\u3007;
        private Object \u3007\u3007888;
        
        ChildHelperWrapper(final ExposeLinearLayoutManagerEx oo0o\u3007\u3007, final LayoutManager \u30078o8o\u3007) {
            this.OO0o\u3007\u3007 = oo0o\u3007\u3007;
            this.\u3007O8o08O = new Object[1];
            this.\u30078o8o\u3007 = \u30078o8o\u3007;
            try {
                (this.\u300780\u3007808\u3007O = LayoutManager.class.getDeclaredField("mChildHelper")).setAccessible(true);
                this.\u3007080();
            }
            catch (final Exception ex) {
                ex.printStackTrace();
            }
        }
        
        boolean O8(final View view) {
            try {
                this.\u3007080();
                final Object[] \u3007o8o08O = this.\u3007O8o08O;
                \u3007o8o08O[0] = view;
                return (boolean)this.Oo08.invoke(this.\u3007080, \u3007o8o08O);
            }
            catch (final Exception ex) {
                ex.printStackTrace();
            }
            catch (final InvocationTargetException ex2) {
                ex2.printStackTrace();
            }
            catch (final IllegalAccessException ex3) {
                ex3.printStackTrace();
            }
            return false;
        }
        
        void Oo08(final View view) {
            try {
                this.\u3007080();
                this.\u3007O8o08O[0] = this.OO0o\u3007\u3007.mRecyclerView.indexOfChild(view);
                this.oO80.invoke(this.\u3007\u3007888, this.\u3007O8o08O);
                final List oo0o\u3007\u3007\u3007\u30070 = this.OO0o\u3007\u3007\u3007\u30070;
                if (oo0o\u3007\u3007\u3007\u30070 != null) {
                    oo0o\u3007\u3007\u3007\u30070.remove(view);
                }
            }
            catch (final Exception ex) {
                ex.printStackTrace();
            }
        }
        
        void \u3007080() {
            try {
                if (this.\u3007080 == null) {
                    final Object value = this.\u300780\u3007808\u3007O.get(this.\u30078o8o\u3007);
                    if ((this.\u3007080 = value) == null) {
                        return;
                    }
                    final Class<?> class1 = value.getClass();
                    (this.\u3007o00\u3007\u3007Oo = class1.getDeclaredMethod("hide", View.class)).setAccessible(true);
                    try {
                        final Class<Integer> type = Integer.TYPE;
                        (this.\u3007o\u3007 = class1.getDeclaredMethod("findHiddenNonRemovedView", type, type)).setAccessible(true);
                    }
                    catch (final NoSuchMethodException ex) {
                        (this.O8 = class1.getDeclaredMethod("findHiddenNonRemovedView", Integer.TYPE)).setAccessible(true);
                    }
                    (this.Oo08 = class1.getDeclaredMethod("isHidden", View.class)).setAccessible(true);
                    final Field declaredField = class1.getDeclaredField("mBucket");
                    declaredField.setAccessible(true);
                    final Object value2 = declaredField.get(this.\u3007080);
                    this.\u3007\u3007888 = value2;
                    (this.oO80 = value2.getClass().getDeclaredMethod("clear", Integer.TYPE)).setAccessible(true);
                    (this.o\u30070 = class1.getDeclaredField("mHiddenViews")).setAccessible(true);
                    this.OO0o\u3007\u3007\u3007\u30070 = (List)this.o\u30070.get(this.\u3007080);
                }
            }
            catch (final Exception ex2) {
                ex2.printStackTrace();
            }
        }
        
        View \u3007o00\u3007\u3007Oo(final int n, final int n2) {
            try {
                this.\u3007080();
                final Method \u3007o\u3007 = this.\u3007o\u3007;
                if (\u3007o\u3007 != null) {
                    return (View)\u3007o\u3007.invoke(this.\u3007080, n, -1);
                }
                final Method o8 = this.O8;
                if (o8 != null) {
                    return (View)o8.invoke(this.\u3007080, n);
                }
            }
            catch (final Exception ex) {
                ex.printStackTrace();
            }
            catch (final InvocationTargetException ex2) {
                ex2.printStackTrace();
            }
            catch (final IllegalAccessException ex3) {
                ex3.printStackTrace();
            }
            return null;
        }
        
        void \u3007o\u3007(final View view) {
            try {
                this.\u3007080();
                if (this.OO0o\u3007\u3007\u3007\u30070.indexOf(view) < 0) {
                    final Object[] \u3007o8o08O = this.\u3007O8o08O;
                    \u3007o8o08O[0] = view;
                    this.\u3007o00\u3007\u3007Oo.invoke(this.\u3007080, \u3007o8o08O);
                }
            }
            catch (final Exception ex) {
                ex.printStackTrace();
            }
        }
    }
    
    public static class LayoutState
    {
        public int O8;
        public List<ViewHolder> OO0o\u3007\u3007;
        public int OO0o\u3007\u3007\u3007\u30070;
        public int Oo08;
        public int oO80;
        public int o\u30070;
        private Method \u3007080;
        public int \u300780\u3007808\u3007O;
        public int \u30078o8o\u3007;
        public boolean \u3007O8o08O;
        public boolean \u3007o00\u3007\u3007Oo;
        public boolean \u3007o\u3007;
        public int \u3007\u3007888;
        
        public LayoutState() {
            this.\u3007080 = null;
            this.\u3007o00\u3007\u3007Oo = false;
            this.\u3007o\u3007 = true;
            this.OO0o\u3007\u3007\u3007\u30070 = 0;
            this.\u30078o8o\u3007 = 0;
            this.\u3007O8o08O = false;
            this.OO0o\u3007\u3007 = null;
            try {
                (this.\u3007080 = ViewHolder.class.getDeclaredMethod("isRemoved", (Class<?>[])new Class[0])).setAccessible(true);
            }
            catch (final NoSuchMethodException cause) {
                cause.printStackTrace();
                throw new RuntimeException(cause);
            }
        }
        
        @SuppressLint({ "LongLogTag" })
        private View \u3007o\u3007() {
            final int size = this.OO0o\u3007\u3007.size();
            int n = Integer.MAX_VALUE;
            ViewHolder viewHolder = null;
            int n2 = 0;
            ViewHolder viewHolder2;
            while (true) {
                viewHolder2 = viewHolder;
                if (n2 >= size) {
                    break;
                }
                final ViewHolder obj = this.OO0o\u3007\u3007.get(n2);
                int n3 = 0;
                Label_0166: {
                    if (!this.\u3007O8o08O) {
                        boolean booleanValue = false;
                        Label_0095: {
                            try {
                                booleanValue = (boolean)this.\u3007080.invoke(obj, new Object[0]);
                                break Label_0095;
                            }
                            catch (final InvocationTargetException ex) {
                                ex.printStackTrace();
                            }
                            catch (final IllegalAccessException ex2) {
                                ex2.printStackTrace();
                            }
                            booleanValue = false;
                        }
                        if (!this.\u3007O8o08O && booleanValue) {
                            n3 = n;
                            break Label_0166;
                        }
                    }
                    final int n4 = (obj.getPosition() - this.o\u30070) * this.\u3007\u3007888;
                    if (n4 < 0) {
                        n3 = n;
                    }
                    else if (n4 < (n3 = n)) {
                        viewHolder = obj;
                        if (n4 == 0) {
                            viewHolder2 = viewHolder;
                            break;
                        }
                        n3 = n4;
                    }
                }
                ++n2;
                n = n3;
            }
            if (viewHolder2 != null) {
                this.o\u30070 = viewHolder2.getPosition() + this.\u3007\u3007888;
                return viewHolder2.itemView;
            }
            return null;
        }
        
        public boolean \u3007080(final State state) {
            final int o\u30070 = this.o\u30070;
            return o\u30070 >= 0 && o\u30070 < state.getItemCount();
        }
        
        public View \u3007o00\u3007\u3007Oo(final Recycler recycler) {
            if (this.OO0o\u3007\u3007 != null) {
                return this.\u3007o\u3007();
            }
            final View viewForPosition = recycler.getViewForPosition(this.o\u30070);
            this.o\u30070 += this.\u3007\u3007888;
            return viewForPosition;
        }
    }
    
    static class ViewHolderWrapper
    {
        private static Method O8;
        private static Method Oo08;
        private static Method o\u30070;
        private static Method \u3007o00\u3007\u3007Oo;
        private static Method \u3007o\u3007;
        private ViewHolder \u3007080;
        
        static {
            try {
                (ViewHolderWrapper.\u3007o00\u3007\u3007Oo = ViewHolder.class.getDeclaredMethod("shouldIgnore", (Class<?>[])new Class[0])).setAccessible(true);
                (ViewHolderWrapper.\u3007o\u3007 = ViewHolder.class.getDeclaredMethod("isInvalid", (Class<?>[])new Class[0])).setAccessible(true);
                (ViewHolderWrapper.O8 = ViewHolder.class.getDeclaredMethod("isRemoved", (Class<?>[])new Class[0])).setAccessible(true);
                final Class<Integer> type = Integer.TYPE;
                (ViewHolderWrapper.o\u30070 = ViewHolder.class.getDeclaredMethod("setFlags", type, type)).setAccessible(true);
                try {
                    ViewHolderWrapper.Oo08 = ViewHolder.class.getDeclaredMethod("isChanged", (Class<?>[])new Class[0]);
                }
                catch (final NoSuchMethodException ex) {
                    ViewHolderWrapper.Oo08 = ViewHolder.class.getDeclaredMethod("isUpdated", (Class<?>[])new Class[0]);
                }
                ViewHolderWrapper.Oo08.setAccessible(true);
            }
            catch (final NoSuchMethodException ex2) {
                ex2.printStackTrace();
            }
        }
        
        public ViewHolderWrapper(final ViewHolder \u3007080) {
            this.\u3007080 = \u3007080;
        }
        
        public static void Oo08(final ViewHolder obj, final int i, final int j) {
            try {
                ViewHolderWrapper.o\u30070.invoke(obj, i, j);
            }
            catch (final InvocationTargetException ex) {
                ex.printStackTrace();
            }
            catch (final IllegalAccessException ex2) {
                ex2.printStackTrace();
            }
        }
        
        public boolean O8() {
            return this.\u3007o00\u3007\u3007Oo() || this.\u3007o\u3007() || this.\u3007080();
        }
        
        boolean \u3007080() {
            final Method oo08 = ViewHolderWrapper.Oo08;
            if (oo08 == null) {
                return true;
            }
            try {
                return (boolean)oo08.invoke(this.\u3007080, new Object[0]);
            }
            catch (final InvocationTargetException ex) {
                ex.printStackTrace();
            }
            catch (final IllegalAccessException ex2) {
                ex2.printStackTrace();
            }
            return true;
        }
        
        boolean \u3007o00\u3007\u3007Oo() {
            final Method \u3007o\u3007 = ViewHolderWrapper.\u3007o\u3007;
            if (\u3007o\u3007 == null) {
                return true;
            }
            try {
                return (boolean)\u3007o\u3007.invoke(this.\u3007080, new Object[0]);
            }
            catch (final InvocationTargetException ex) {
                ex.printStackTrace();
            }
            catch (final IllegalAccessException ex2) {
                ex2.printStackTrace();
            }
            return true;
        }
        
        boolean \u3007o\u3007() {
            final Method o8 = ViewHolderWrapper.O8;
            if (o8 == null) {
                return true;
            }
            try {
                return (boolean)o8.invoke(this.\u3007080, new Object[0]);
            }
            catch (final InvocationTargetException ex) {
                ex.printStackTrace();
            }
            catch (final IllegalAccessException ex2) {
                ex2.printStackTrace();
            }
            return true;
        }
    }
}

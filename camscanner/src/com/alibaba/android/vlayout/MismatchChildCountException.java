// 
// Decompiled by Procyon v0.6.0
// 

package com.alibaba.android.vlayout;

public class MismatchChildCountException extends RuntimeException
{
    public MismatchChildCountException(final String message) {
        super(message);
    }
    
    public MismatchChildCountException(final String message, final Throwable cause) {
        super(message, cause);
    }
}

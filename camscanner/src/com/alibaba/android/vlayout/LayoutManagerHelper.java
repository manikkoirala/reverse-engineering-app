// 
// Decompiled by Procyon v0.6.0
// 

package com.alibaba.android.vlayout;

import androidx.annotation.Nullable;
import android.view.View;

public interface LayoutManagerHelper
{
    void OO0o\u3007\u3007(final View p0, final int p1, final int p2, final int p3, final int p4);
    
    void OoO8(final View p0, final boolean p1);
    
    OrientationHelperEx Oooo8o0\u3007();
    
    @Nullable
    View findViewByPosition(final int p0);
    
    @Nullable
    View getChildAt(final int p0);
    
    int getChildCount();
    
    int getOrientation();
    
    int getPaddingBottom();
    
    int getPaddingLeft();
    
    int getPaddingRight();
    
    int getPaddingTop();
    
    int getPosition(final View p0);
    
    boolean getReverseLayout();
    
    void hideView(final View p0);
    
    boolean isEnableMarginOverLap();
    
    void measureChild(final View p0, final int p1, final int p2);
    
    void measureChildWithMargins(final View p0, final int p1, final int p2);
    
    void o800o8O(final View p0, final boolean p1);
    
    void oo88o8O(final VirtualLayoutManager.LayoutStateWrapper p0, final View p1);
    
    View o\u3007O8\u3007\u3007o();
    
    void showView(final View p0);
    
    int \u300700(final int p0, final int p1, final boolean p2);
    
    void \u30070\u3007O0088o(final VirtualLayoutManager.LayoutStateWrapper p0, final View p1, final int p2);
    
    int \u300780\u3007808\u3007O();
    
    void \u30078o8o\u3007(final View p0, final int p1);
    
    void \u3007O00(final View p0);
    
    void \u3007O8o08O(final View p0);
    
    boolean \u3007o00\u3007\u3007Oo(final View p0);
    
    LayoutHelper \u3007o\u3007(final int p0);
    
    int \u3007\u3007808\u3007();
    
    void \u3007\u3007888(final View p0);
    
    boolean \u3007\u30078O0\u30078();
}

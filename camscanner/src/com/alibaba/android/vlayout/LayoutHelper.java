// 
// Decompiled by Procyon v0.6.0
// 

package com.alibaba.android.vlayout;

import com.alibaba.android.vlayout.layout.LayoutChunkResult;
import androidx.recyclerview.widget.RecyclerView;
import java.util.LinkedList;
import android.view.View;
import java.util.List;
import androidx.annotation.NonNull;

public abstract class LayoutHelper
{
    public static final Range<Integer> O8;
    public static final Range<Integer> Oo08;
    @NonNull
    Range<Integer> \u3007080;
    int \u3007o00\u3007\u3007Oo;
    @NonNull
    protected final List<View> \u3007o\u3007;
    
    static {
        O8 = Range.\u3007o\u3007(Integer.MIN_VALUE, Integer.MAX_VALUE);
        final Integer value = -1;
        Oo08 = Range.\u3007o\u3007(value, value);
    }
    
    public LayoutHelper() {
        this.\u3007080 = LayoutHelper.Oo08;
        this.\u3007o00\u3007\u3007Oo = 0;
        this.\u3007o\u3007 = new LinkedList<View>();
    }
    
    public abstract void O8(final LayoutManagerHelper p0);
    
    public void OO0o\u3007\u3007(final int n, final LayoutManagerHelper layoutManagerHelper) {
    }
    
    public boolean OO0o\u3007\u3007\u3007\u30070(final int i) {
        return this.\u3007080.\u3007o00\u3007\u3007Oo(i) ^ true;
    }
    
    public abstract int Oo08(final int p0, final boolean p1, final boolean p2, final LayoutManagerHelper p3);
    
    public void Oooo8o0\u3007(final int n, final LayoutManagerHelper layoutManagerHelper) {
    }
    
    @NonNull
    public final Range<Integer> oO80() {
        return this.\u3007080;
    }
    
    public abstract void o\u30070(final RecyclerView.Recycler p0, final RecyclerView.State p1, final VirtualLayoutManager.LayoutStateWrapper p2, final LayoutChunkResult p3, final LayoutManagerHelper p4);
    
    public abstract void \u3007080(final RecyclerView.Recycler p0, final RecyclerView.State p1, final int p2, final int p3, final int p4, final LayoutManagerHelper p5);
    
    public void \u30070\u3007O0088o(final int i, final int j) {
        if (j < i) {
            throw new IllegalArgumentException("end should be larger or equeal then start position");
        }
        if (i == -1 && j == -1) {
            this.\u3007080 = LayoutHelper.Oo08;
            this.\u3007\u3007808\u3007(i, j);
            return;
        }
        if (j - i + 1 != this.\u3007\u3007888()) {
            final StringBuilder sb = new StringBuilder();
            sb.append("ItemCount mismatch when range: ");
            sb.append(this.\u3007080.toString());
            sb.append(" childCount: ");
            sb.append(this.\u3007\u3007888());
            throw new MismatchChildCountException(sb.toString());
        }
        if (i == this.\u3007080.Oo08() && j == this.\u3007080.O8()) {
            return;
        }
        this.\u3007080 = Range.\u3007o\u3007(i, j);
        this.\u3007\u3007808\u3007(i, j);
    }
    
    public abstract boolean \u300780\u3007808\u3007O();
    
    public boolean \u30078o8o\u3007(final int n, final int n2, final int n3, final LayoutManagerHelper layoutManagerHelper, final boolean b) {
        return true;
    }
    
    public void \u3007O00(final int n, final int n2, final int n3, final LayoutManagerHelper layoutManagerHelper) {
    }
    
    public void \u3007O8o08O(final LayoutManagerHelper layoutManagerHelper) {
    }
    
    public void \u3007O\u3007(final RecyclerView.State state, final VirtualLayoutManager.AnchorInfoWrapper anchorInfoWrapper, final LayoutManagerHelper layoutManagerHelper) {
    }
    
    public abstract void \u3007o00\u3007\u3007Oo(final RecyclerView.Recycler p0, final RecyclerView.State p1, final LayoutManagerHelper p2);
    
    public void \u3007o\u3007(final RecyclerView.State state, final VirtualLayoutManager.AnchorInfoWrapper anchorInfoWrapper, final LayoutManagerHelper layoutManagerHelper) {
    }
    
    public void \u3007\u3007808\u3007(final int n, final int n2) {
    }
    
    public abstract int \u3007\u3007888();
    
    public abstract void \u3007\u30078O0\u30078(final int p0);
}

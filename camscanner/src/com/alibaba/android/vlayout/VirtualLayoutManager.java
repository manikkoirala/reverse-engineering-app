// 
// Decompiled by Procyon v0.6.0
// 

package com.alibaba.android.vlayout;

import android.util.Log;
import java.util.Map;
import com.alibaba.android.vlayout.layout.BaseLayoutHelper;
import com.alibaba.android.vlayout.layout.FixAreaLayoutHelper;
import android.os.Parcelable;
import android.view.ViewParent;
import android.os.Trace;
import java.util.LinkedList;
import java.util.Collections;
import com.alibaba.android.vlayout.layout.LayoutChunkResult;
import android.view.ViewGroup$LayoutParams;
import android.util.AttributeSet;
import android.graphics.PointF;
import android.view.ViewGroup$MarginLayoutParams;
import java.util.Iterator;
import androidx.annotation.Nullable;
import android.view.View$MeasureSpec;
import android.view.View;
import java.util.ArrayList;
import androidx.annotation.NonNull;
import android.content.Context;
import com.alibaba.android.vlayout.layout.DefaultLayoutHelper;
import java.util.List;
import com.alibaba.android.vlayout.layout.FixAreaAdjuster;
import java.util.HashMap;
import com.alibaba.android.vlayout.extend.PerformanceMonitor;
import android.graphics.Rect;
import android.util.Pair;
import java.util.Comparator;
import androidx.recyclerview.widget.RecyclerView;

public class VirtualLayoutManager extends ExposeLinearLayoutManagerEx implements LayoutManagerHelper
{
    private static LayoutHelper Oo80;
    public static boolean \u3007\u3007o\u3007 = false;
    private AnchorInfoWrapper O0O;
    private LayoutHelper O88O;
    private boolean O8o08O8O;
    private RecyclerView OO;
    private Comparator<Pair<Range<Integer>, Integer>> OO\u300700\u30078oO;
    protected OrientationHelperEx o0;
    private Rect o8o;
    private int o8oOOo;
    private LayoutHelperFinder o8\u3007OO0\u30070o;
    private LayoutViewFactory oOO\u3007\u3007;
    private PerformanceMonitor oOo0;
    private int oOo\u30078o008;
    private boolean oo8ooo8O;
    private HashMap<Integer, LayoutHelper> ooo0\u3007\u3007O;
    private boolean o\u300700O;
    private int o\u3007oO;
    private boolean \u3007080OO8\u30070;
    private boolean \u300708O\u300700\u3007o;
    private boolean \u300708\u3007o0O;
    private boolean \u30070O;
    private FixAreaAdjuster \u30078\u3007oO\u3007\u30078o;
    protected OrientationHelperEx \u3007OOo8\u30070;
    private LayoutStateWrapper \u3007O\u3007\u3007O8;
    private List<Pair<Range<Integer>, Integer>> \u3007o0O;
    private HashMap<Integer, LayoutHelper> \u3007\u300708O;
    
    static {
        VirtualLayoutManager.Oo80 = new DefaultLayoutHelper();
    }
    
    public VirtualLayoutManager(@NonNull final Context context) {
        this(context, 1);
    }
    
    public VirtualLayoutManager(@NonNull final Context context, final int n) {
        this(context, n, false);
    }
    
    public VirtualLayoutManager(@NonNull final Context context, int n, final boolean b) {
        super(context, n, b);
        final int n2 = 0;
        this.\u300708O\u300700\u3007o = false;
        this.o\u300700O = false;
        this.\u30070O = false;
        this.oOo\u30078o008 = -1;
        this.OO\u300700\u30078oO = new Comparator<Pair<Range<Integer>, Integer>>() {
            final VirtualLayoutManager o0;
            
            public int \u3007080(final Pair<Range<Integer>, Integer> pair, final Pair<Range<Integer>, Integer> pair2) {
                if (pair == null && pair2 == null) {
                    return 0;
                }
                if (pair == null) {
                    return -1;
                }
                if (pair2 == null) {
                    return 1;
                }
                return ((Range)pair.first).O8() - ((Range)pair2.first).O8();
            }
        };
        this.\u30078\u3007oO\u3007\u30078o = FixAreaAdjuster.Oo08;
        this.ooo0\u3007\u3007O = new HashMap<Integer, LayoutHelper>();
        this.\u3007\u300708O = new HashMap<Integer, LayoutHelper>();
        this.O0O = new AnchorInfoWrapper();
        this.o8oOOo = 0;
        this.\u3007O\u3007\u3007O8 = new LayoutStateWrapper();
        this.\u3007o0O = new ArrayList<Pair<Range<Integer>, Integer>>();
        this.O88O = VirtualLayoutManager.Oo80;
        this.oOO\u3007\u3007 = new LayoutViewFactory() {
            final VirtualLayoutManager \u3007080;
            
            @Override
            public View \u3007080(@NonNull final Context context) {
                return new LayoutView(context);
            }
        };
        this.o8o = new Rect();
        this.oo8ooo8O = false;
        this.o\u3007oO = 0;
        this.\u300708\u3007o0O = false;
        this.o0 = OrientationHelperEx.\u3007o00\u3007\u3007Oo(this, n);
        if (n == 1) {
            n = n2;
        }
        else {
            n = 1;
        }
        this.\u3007OOo8\u30070 = OrientationHelperEx.\u3007o00\u3007\u3007Oo(this, n);
        this.\u3007080OO8\u30070 = super.canScrollVertically();
        this.O8o08O8O = super.canScrollHorizontally();
        this.o0ooO(new RangeLayoutHelperFinder());
    }
    
    private void O8\u3007o(final View view, int updateSpecWithExtra, final int n) {
        ((RecyclerView.LayoutManager)this).calculateItemDecorationsForChild(view, this.o8o);
        final RecyclerView.LayoutParams layoutParams = (RecyclerView.LayoutParams)view.getLayoutParams();
        int updateSpecWithExtra2 = updateSpecWithExtra;
        if (this.getOrientation() == 1) {
            final int leftMargin = layoutParams.leftMargin;
            final Rect o8o = this.o8o;
            updateSpecWithExtra2 = this.updateSpecWithExtra(updateSpecWithExtra, leftMargin + o8o.left, layoutParams.rightMargin + o8o.right);
        }
        updateSpecWithExtra = n;
        if (this.getOrientation() == 0) {
            final Rect o8o2 = this.o8o;
            updateSpecWithExtra = this.updateSpecWithExtra(n, o8o2.top, o8o2.bottom);
        }
        final PerformanceMonitor oOo0 = this.oOo0;
        if (oOo0 != null) {
            oOo0.recordStart("measure", view);
        }
        view.measure(updateSpecWithExtra2, updateSpecWithExtra);
        final PerformanceMonitor oOo2 = this.oOo0;
        if (oOo2 != null) {
            oOo2.recordEnd("measure", view);
        }
    }
    
    private void oo\u3007(final View view, int updateSpecWithExtra, int updateSpecWithExtra2) {
        ((RecyclerView.LayoutManager)this).calculateItemDecorationsForChild(view, this.o8o);
        final Rect o8o = this.o8o;
        updateSpecWithExtra = this.updateSpecWithExtra(updateSpecWithExtra, o8o.left, o8o.right);
        final Rect o8o2 = this.o8o;
        updateSpecWithExtra2 = this.updateSpecWithExtra(updateSpecWithExtra2, o8o2.top, o8o2.bottom);
        final PerformanceMonitor oOo0 = this.oOo0;
        if (oOo0 != null) {
            oOo0.recordStart("measure", view);
        }
        view.measure(updateSpecWithExtra, updateSpecWithExtra2);
        final PerformanceMonitor oOo2 = this.oOo0;
        if (oOo2 != null) {
            oOo2.recordEnd("measure", view);
        }
    }
    
    private int updateSpecWithExtra(final int n, final int n2, final int n3) {
        if (n2 == 0 && n3 == 0) {
            return n;
        }
        final int mode = View$MeasureSpec.getMode(n);
        if (mode != Integer.MIN_VALUE && mode != 1073741824) {
            return n;
        }
        if (View$MeasureSpec.getSize(n) - n2 - n3 < 0) {
            return View$MeasureSpec.makeMeasureSpec(0, mode);
        }
        return View$MeasureSpec.makeMeasureSpec(View$MeasureSpec.getSize(n) - n2 - n3, mode);
    }
    
    @Nullable
    private int \u30070000OOO(@NonNull final Range<Integer> range) {
        final int size = this.\u3007o0O.size();
        final int n = -1;
        if (size == 0) {
            return -1;
        }
        int n2 = size - 1;
        int n3 = 0;
        int n4 = -1;
        Pair pair2;
        while (true) {
            final Pair pair = pair2 = null;
            if (n3 > n2) {
                break;
            }
            final int n5 = (n3 + n2) / 2;
            pair2 = this.\u3007o0O.get(n5);
            final Range range2 = (Range)pair2.first;
            if (range2 == null) {
                n4 = n5;
                pair2 = pair;
                break;
            }
            if (range2.\u3007o00\u3007\u3007Oo(range.O8()) || range2.\u3007o00\u3007\u3007Oo(range.Oo08()) || range.\u3007080(range2)) {
                n4 = n5;
                break;
            }
            if ((int)range2.O8() > range.Oo08()) {
                n2 = n5 - 1;
                n4 = n5;
            }
            else {
                n4 = n5;
                if ((int)range2.Oo08() >= range.O8()) {
                    continue;
                }
                n3 = n5 + 1;
                n4 = n5;
            }
        }
        if (pair2 == null) {
            n4 = n;
        }
        return n4;
    }
    
    private void \u300700\u30078(final Recycler recycler, final State state, final int n) {
        final int o8oOOo = this.o8oOOo - 1;
        this.o8oOOo = o8oOOo;
        if (o8oOOo <= 0) {
            this.o8oOOo = 0;
            final int firstVisibleItemPosition = this.findFirstVisibleItemPosition();
            final int lastVisibleItemPosition = this.findLastVisibleItemPosition();
            for (final LayoutHelper layoutHelper : this.o8\u3007OO0\u30070o.\u3007o00\u3007\u3007Oo()) {
                try {
                    layoutHelper.\u3007080(recycler, state, firstVisibleItemPosition, lastVisibleItemPosition, n, this);
                    continue;
                }
                catch (final Exception ex) {
                    if (!VirtualLayoutManager.\u3007\u3007o\u3007) {
                        continue;
                    }
                    throw ex;
                }
                break;
            }
        }
    }
    
    private void \u3007o(final Recycler recycler, final State state) {
        if (this.o8oOOo == 0) {
            final Iterator<LayoutHelper> iterator = this.o8\u3007OO0\u30070o.\u3007o\u3007().iterator();
            while (iterator.hasNext()) {
                iterator.next().\u3007o00\u3007\u3007Oo(recycler, state, this);
            }
        }
        ++this.o8oOOo;
    }
    
    @Override
    public void OO0o\u3007\u3007(final View view, final int n, final int n2, final int n3, final int n4) {
        final ViewGroup$MarginLayoutParams viewGroup$MarginLayoutParams = (ViewGroup$MarginLayoutParams)view.getLayoutParams();
        final PerformanceMonitor oOo0 = this.oOo0;
        if (oOo0 != null) {
            oOo0.recordStart("layout", view);
        }
        ((RecyclerView.LayoutManager)this).layoutDecorated(view, n + viewGroup$MarginLayoutParams.leftMargin, n2 + viewGroup$MarginLayoutParams.topMargin, n3 - viewGroup$MarginLayoutParams.rightMargin, n4 - viewGroup$MarginLayoutParams.bottomMargin);
        final PerformanceMonitor oOo2 = this.oOo0;
        if (oOo2 != null) {
            oOo2.recordEnd("layout", view);
        }
    }
    
    @NonNull
    public List<LayoutHelper> OOO\u3007O0() {
        return this.o8\u3007OO0\u30070o.\u3007o00\u3007\u3007Oo();
    }
    
    @Override
    public void OoO8(final View view, final boolean b) {
        this.showView(view);
        this.addHiddenView(view, b);
    }
    
    @Override
    public OrientationHelperEx Oooo8o0\u3007() {
        return this.o0;
    }
    
    @Override
    public boolean canScrollHorizontally() {
        return this.O8o08O8O && !this.\u300708O\u300700\u3007o;
    }
    
    @Override
    public boolean canScrollVertically() {
        return this.\u3007080OO8\u30070 && !this.\u300708O\u300700\u3007o;
    }
    
    @Override
    public boolean checkLayoutParams(final RecyclerView.LayoutParams layoutParams) {
        return layoutParams instanceof LayoutParams;
    }
    
    @Override
    protected int computeAlignOffset(final int n, final boolean b, final boolean b2) {
        if (n != -1) {
            final LayoutHelper \u3007080 = this.o8\u3007OO0\u30070o.\u3007080(n);
            if (\u3007080 != null) {
                return \u3007080.Oo08(n - \u3007080.oO80().O8(), b, b2, this);
            }
        }
        return 0;
    }
    
    @Override
    protected int computeAlignOffset(final View view, final boolean b, final boolean b2) {
        return this.computeAlignOffset(((RecyclerView.LayoutManager)this).getPosition(view), b, b2);
    }
    
    @Override
    public void detachAndScrapAttachedViews(final Recycler recycler) {
        for (int i = ((RecyclerView.LayoutManager)this).getChildCount() - 1; i >= 0; --i) {
            final ViewHolder o\u3007\u30070\u3007 = this.o\u3007\u30070\u3007(((RecyclerView.LayoutManager)this).getChildAt(i));
            if (o\u3007\u30070\u3007 instanceof CacheViewHolder && ((CacheViewHolder)o\u3007\u30070\u3007).\u3007080()) {
                ViewHolderWrapper.Oo08(o\u3007\u30070\u3007, 0, 6);
            }
        }
        super.detachAndScrapAttachedViews(recycler);
    }
    
    @Override
    public void detachAndScrapView(final View view, final Recycler recycler) {
        super.detachAndScrapView(view, recycler);
    }
    
    @Override
    public void detachAndScrapViewAt(final int n, final Recycler recycler) {
        final ViewHolder o\u3007\u30070\u3007 = this.o\u3007\u30070\u3007(((RecyclerView.LayoutManager)this).getChildAt(n));
        if (o\u3007\u30070\u3007 instanceof CacheViewHolder && ((CacheViewHolder)o\u3007\u30070\u3007).\u3007080()) {
            ViewHolderWrapper.Oo08(o\u3007\u30070\u3007, 0, 4);
        }
        super.detachAndScrapViewAt(n, recycler);
    }
    
    @Override
    public View findViewByPosition(final int n) {
        final View viewByPosition = super.findViewByPosition(n);
        if (viewByPosition != null && ((RecyclerView.LayoutManager)this).getPosition(viewByPosition) == n) {
            return viewByPosition;
        }
        for (int i = 0; i < ((RecyclerView.LayoutManager)this).getChildCount(); ++i) {
            final View child = ((RecyclerView.LayoutManager)this).getChildAt(i);
            if (child != null && ((RecyclerView.LayoutManager)this).getPosition(child) == n) {
                return child;
            }
        }
        return null;
    }
    
    @Override
    public RecyclerView.LayoutParams generateDefaultLayoutParams() {
        return new LayoutParams(-2, -2);
    }
    
    @Override
    public RecyclerView.LayoutParams generateLayoutParams(final Context context, final AttributeSet set) {
        return new InflateLayoutParams(context, set);
    }
    
    @Override
    public RecyclerView.LayoutParams generateLayoutParams(final ViewGroup$LayoutParams viewGroup$LayoutParams) {
        if (viewGroup$LayoutParams instanceof LayoutParams) {
            return new LayoutParams((RecyclerView.LayoutParams)viewGroup$LayoutParams);
        }
        if (viewGroup$LayoutParams instanceof RecyclerView.LayoutParams) {
            return new LayoutParams((RecyclerView.LayoutParams)viewGroup$LayoutParams);
        }
        if (viewGroup$LayoutParams instanceof ViewGroup$MarginLayoutParams) {
            return new LayoutParams((ViewGroup$MarginLayoutParams)viewGroup$LayoutParams);
        }
        return new LayoutParams(viewGroup$LayoutParams);
    }
    
    @Override
    public int getOrientation() {
        return super.getOrientation();
    }
    
    @Override
    public void hideView(final View view) {
        super.hideView(view);
    }
    
    @Override
    public boolean isEnableMarginOverLap() {
        return this.\u30070O;
    }
    
    @Override
    protected void layoutChunk(final Recycler recycler, final State state, final LayoutState layoutState, final com.alibaba.android.vlayout.layout.LayoutChunkResult layoutChunkResult) {
        final int o\u30070 = layoutState.o\u30070;
        this.\u3007O\u3007\u3007O8.\u3007080 = layoutState;
        final LayoutHelperFinder o8\u3007OO0\u30070o = this.o8\u3007OO0\u30070o;
        LayoutHelper \u3007080;
        if (o8\u3007OO0\u30070o == null) {
            \u3007080 = null;
        }
        else {
            \u3007080 = o8\u3007OO0\u30070o.\u3007080(o\u30070);
        }
        LayoutHelper o88O = \u3007080;
        if (\u3007080 == null) {
            o88O = this.O88O;
        }
        o88O.o\u30070(recycler, state, this.\u3007O\u3007\u3007O8, layoutChunkResult, this);
        this.\u3007O\u3007\u3007O8.\u3007080 = null;
        final int o\u30072 = layoutState.o\u30070;
        if (o\u30072 == o\u30070) {
            if (VirtualLayoutManager.\u3007\u3007o\u3007) {
                final StringBuilder sb = new StringBuilder();
                sb.append("layoutHelper[");
                sb.append(o88O.getClass().getSimpleName());
                sb.append("@");
                sb.append(o88O.toString());
                sb.append("] consumes no item!");
            }
            layoutChunkResult.\u3007o00\u3007\u3007Oo = true;
        }
        else {
            final int n = o\u30072 - layoutState.\u3007\u3007888;
            int \u300781;
            if (layoutChunkResult.\u3007o\u3007) {
                \u300781 = 0;
            }
            else {
                \u300781 = layoutChunkResult.\u3007080;
            }
            final Range range = new Range<Integer>(Math.min(o\u30070, n), Math.max(o\u30070, n));
            final int \u30070000OOO = this.\u30070000OOO((Range<Integer>)range);
            if (\u30070000OOO >= 0) {
                final Pair pair = this.\u3007o0O.get(\u30070000OOO);
                if (pair != null && ((Range)pair.first).equals(range) && (int)pair.second == \u300781) {
                    return;
                }
                this.\u3007o0O.remove(\u30070000OOO);
            }
            this.\u3007o0O.add((Pair<Range<Integer>, Integer>)Pair.create((Object)range, (Object)\u300781));
            Collections.sort(this.\u3007o0O, this.OO\u300700\u30078oO);
        }
    }
    
    @Override
    public void measureChild(final View view, final int n, final int n2) {
        this.oo\u3007(view, n, n2);
    }
    
    @Override
    public void measureChildWithMargins(final View view, final int n, final int n2) {
        this.O8\u3007o(view, n, n2);
    }
    
    @Override
    public void moveView(final int n, final int n2) {
        super.moveView(n, n2);
    }
    
    public void o0ooO(@NonNull final LayoutHelperFinder o8\u3007OO0\u30070o) {
        if (o8\u3007OO0\u30070o != null) {
            final LinkedList list = new LinkedList();
            final LayoutHelperFinder o8\u3007OO0\u30070o2 = this.o8\u3007OO0\u30070o;
            if (o8\u3007OO0\u30070o2 != null) {
                final Iterator<LayoutHelper> iterator = o8\u3007OO0\u30070o2.\u3007o00\u3007\u3007Oo().iterator();
                while (iterator.hasNext()) {
                    list.add(iterator.next());
                }
            }
            this.o8\u3007OO0\u30070o = o8\u3007OO0\u30070o;
            if (list.size() > 0) {
                this.o8\u3007OO0\u30070o.O8(list);
            }
            this.oo8ooo8O = false;
            ((RecyclerView.LayoutManager)this).requestLayout();
            return;
        }
        throw new IllegalArgumentException("finder is null");
    }
    
    @Override
    public void o800o8O(final View view, final boolean b) {
        this.showView(view);
        int n;
        if (b) {
            n = 0;
        }
        else {
            n = -1;
        }
        ((RecyclerView.LayoutManager)this).addView(view, n);
    }
    
    @Override
    public void offsetChildrenHorizontal(final int n) {
        super.offsetChildrenHorizontal(n);
        final Iterator<LayoutHelper> iterator = this.o8\u3007OO0\u30070o.\u3007o00\u3007\u3007Oo().iterator();
        while (iterator.hasNext()) {
            iterator.next().OO0o\u3007\u3007(n, this);
        }
    }
    
    @Override
    public void offsetChildrenVertical(final int n) {
        super.offsetChildrenVertical(n);
        final Iterator<LayoutHelper> iterator = this.o8\u3007OO0\u30070o.\u3007o00\u3007\u3007Oo().iterator();
        while (iterator.hasNext()) {
            iterator.next().Oooo8o0\u3007(n, this);
        }
    }
    
    @Override
    public void onAdapterChanged(final Adapter adapter, final Adapter adapter2) {
        super.onAdapterChanged(adapter, adapter2);
    }
    
    @Override
    public void onAnchorReady(final State state, final AnchorInfo anchorInfo) {
        super.onAnchorReady(state, anchorInfo);
        int i = 1;
        while (i != 0) {
            final AnchorInfoWrapper o0O = this.O0O;
            final int \u3007080 = anchorInfo.\u3007080;
            o0O.\u3007080 = \u3007080;
            o0O.\u3007o00\u3007\u3007Oo = anchorInfo.\u3007o00\u3007\u3007Oo;
            o0O.\u3007o\u3007 = anchorInfo.\u3007o\u3007;
            final LayoutHelper \u300781 = this.o8\u3007OO0\u30070o.\u3007080(\u3007080);
            if (\u300781 != null) {
                \u300781.\u3007o\u3007(state, this.O0O, this);
            }
            final AnchorInfoWrapper o0O2 = this.O0O;
            final int \u300782 = o0O2.\u3007080;
            if (\u300782 == anchorInfo.\u3007080) {
                i = 0;
            }
            else {
                anchorInfo.\u3007080 = \u300782;
            }
            anchorInfo.\u3007o00\u3007\u3007Oo = o0O2.\u3007o00\u3007\u3007Oo;
            o0O2.\u3007080 = -1;
        }
        final AnchorInfoWrapper o0O3 = this.O0O;
        o0O3.\u3007080 = anchorInfo.\u3007080;
        o0O3.\u3007o00\u3007\u3007Oo = anchorInfo.\u3007o00\u3007\u3007Oo;
        final Iterator<LayoutHelper> iterator = this.o8\u3007OO0\u30070o.\u3007o00\u3007\u3007Oo().iterator();
        while (iterator.hasNext()) {
            iterator.next().\u3007O\u3007(state, this.O0O, this);
        }
    }
    
    @Override
    public void onAttachedToWindow(final RecyclerView oo) {
        super.onAttachedToWindow(oo);
        this.OO = oo;
    }
    
    @Override
    public void onDetachedFromWindow(final RecyclerView recyclerView, final Recycler recycler) {
        super.onDetachedFromWindow(recyclerView, recycler);
        final Iterator<LayoutHelper> iterator = this.o8\u3007OO0\u30070o.\u3007o00\u3007\u3007Oo().iterator();
        while (iterator.hasNext()) {
            iterator.next().O8(this);
        }
        this.OO = null;
    }
    
    @Override
    public void onItemsAdded(final RecyclerView recyclerView, final int n, final int n2) {
        this.onItemsChanged(recyclerView);
    }
    
    @Override
    public void onItemsChanged(final RecyclerView recyclerView) {
        final Iterator<LayoutHelper> iterator = this.o8\u3007OO0\u30070o.\u3007o00\u3007\u3007Oo().iterator();
        while (iterator.hasNext()) {
            iterator.next().\u3007O8o08O(this);
        }
    }
    
    @Override
    public void onItemsMoved(final RecyclerView recyclerView, final int n, final int n2, final int n3) {
        this.onItemsChanged(recyclerView);
    }
    
    @Override
    public void onItemsRemoved(final RecyclerView recyclerView, final int n, final int n2) {
        this.onItemsChanged(recyclerView);
    }
    
    @Override
    public void onItemsUpdated(final RecyclerView recyclerView, final int n, final int n2) {
        this.onItemsChanged(recyclerView);
    }
    
    @Override
    public void onLayoutChildren(Recycler recycler, State state) {
        Trace.beginSection("VLM onLayoutChildren");
        if (this.\u300708O\u300700\u3007o && state.didStructureChange()) {
            this.oo8ooo8O = false;
            this.\u300708\u3007o0O = true;
        }
        this.\u3007o(recycler, state);
        try {
            try {
                super.onLayoutChildren(recycler, state);
                this.\u300700\u30078(recycler, state, Integer.MAX_VALUE);
                if ((this.o\u300700O || this.\u300708O\u300700\u3007o) && this.\u300708\u3007o0O) {
                    this.oo8ooo8O = true;
                    recycler = (Recycler)((RecyclerView.LayoutManager)this).getChildAt(((RecyclerView.LayoutManager)this).getChildCount() - 1);
                    if (recycler != null) {
                        state = (State)((View)recycler).getLayoutParams();
                        this.o\u3007oO = ((RecyclerView.LayoutManager)this).getDecoratedBottom((View)recycler) + ((ViewGroup$MarginLayoutParams)state).bottomMargin + this.computeAlignOffset((View)recycler, true, false);
                        recycler = (Recycler)this.OO;
                        if (recycler != null && this.o\u300700O) {
                            recycler = (Recycler)((View)recycler).getParent();
                            if (recycler instanceof View) {
                                this.o\u3007oO = Math.min(this.o\u3007oO, ((View)recycler).getMeasuredHeight());
                            }
                        }
                    }
                    else {
                        this.\u300708\u3007o0O = false;
                    }
                    this.\u300708\u3007o0O = false;
                    if (this.OO != null && ((RecyclerView.LayoutManager)this).getItemCount() > 0) {
                        ((View)this.OO).post((Runnable)new Runnable(this) {
                            final VirtualLayoutManager o0;
                            
                            @Override
                            public void run() {
                                if (this.o0.OO != null) {
                                    this.o0.OO.requestLayout();
                                }
                            }
                        });
                    }
                }
                Trace.endSection();
                return;
            }
            finally {}
        }
        catch (final Exception ex) {
            ex.printStackTrace();
            throw ex;
        }
        this.\u300700\u30078(recycler, state, Integer.MAX_VALUE);
    }
    
    @Override
    public void onMeasure(final Recycler recycler, final State state, final int n, final int n2) {
        if (!this.\u300708O\u300700\u3007o && !this.o\u300700O) {
            super.onMeasure(recycler, state, n, n2);
            return;
        }
        final RecyclerView oo = this.OO;
        final int n3 = 134217727;
        int n4 = 0;
        Label_0094: {
            if (oo != null && this.o\u300700O) {
                n4 = this.oOo\u30078o008;
                if (n4 > 0) {
                    break Label_0094;
                }
                final ViewParent parent = ((View)oo).getParent();
                if (parent instanceof View) {
                    n4 = ((View)parent).getMeasuredHeight();
                    break Label_0094;
                }
            }
            n4 = 134217727;
        }
        final boolean oo8ooo8O = this.oo8ooo8O;
        if (oo8ooo8O) {
            n4 = this.o\u3007oO;
        }
        int n5 = n4;
        if (this.\u300708O\u300700\u3007o) {
            this.\u300708\u3007o0O = (oo8ooo8O ^ true);
            if (((RecyclerView.LayoutManager)this).getChildCount() <= 0 && ((RecyclerView.LayoutManager)this).getChildCount() == ((RecyclerView.LayoutManager)this).getItemCount()) {
                n5 = n4;
                if (((RecyclerView.LayoutManager)this).getItemCount() == 0) {
                    this.oo8ooo8O = true;
                    this.\u300708\u3007o0O = false;
                    n5 = 0;
                }
            }
            else {
                final View child = ((RecyclerView.LayoutManager)this).getChildAt(((RecyclerView.LayoutManager)this).getChildCount() - 1);
                int o\u3007oO = this.o\u3007oO;
                if (child != null) {
                    o\u3007oO = this.computeAlignOffset(child, true, false) + (((RecyclerView.LayoutManager)this).getDecoratedBottom(child) + ((RecyclerView.LayoutParams)child.getLayoutParams()).bottomMargin);
                }
                if (((RecyclerView.LayoutManager)this).getChildCount() != ((RecyclerView.LayoutManager)this).getItemCount() || (child != null && o\u3007oO != this.o\u3007oO)) {
                    this.oo8ooo8O = false;
                    this.\u300708\u3007o0O = true;
                    n4 = n3;
                }
                n5 = n4;
            }
        }
        if (this.getOrientation() == 1) {
            super.onMeasure(recycler, state, n, View$MeasureSpec.makeMeasureSpec(n5, Integer.MIN_VALUE));
        }
        else {
            super.onMeasure(recycler, state, View$MeasureSpec.makeMeasureSpec(n5, Integer.MIN_VALUE), n2);
        }
    }
    
    @Override
    public void onScrollStateChanged(final int n) {
        super.onScrollStateChanged(n);
        final int firstVisibleItemPosition = this.findFirstVisibleItemPosition();
        final int lastVisibleItemPosition = this.findLastVisibleItemPosition();
        final Iterator<LayoutHelper> iterator = this.o8\u3007OO0\u30070o.\u3007o00\u3007\u3007Oo().iterator();
        while (iterator.hasNext()) {
            iterator.next().\u3007O00(n, firstVisibleItemPosition, lastVisibleItemPosition, this);
        }
    }
    
    @Override
    public void oo88o8O(final LayoutStateWrapper layoutStateWrapper, final View view) {
        int n;
        if (layoutStateWrapper.Oo08() == 1) {
            n = -1;
        }
        else {
            n = 0;
        }
        this.\u30070\u3007O0088o(layoutStateWrapper, view, n);
    }
    
    public void o\u30078(@Nullable final List<LayoutHelper> list) {
        for (final LayoutHelper value : this.o8\u3007OO0\u30070o.\u3007o00\u3007\u3007Oo()) {
            this.\u3007\u300708O.put(System.identityHashCode(value), value);
        }
        if (list != null) {
            final Iterator<LayoutHelper> iterator2 = list.iterator();
            int n = 0;
            while (iterator2.hasNext()) {
                final LayoutHelper layoutHelper = iterator2.next();
                if (layoutHelper instanceof FixAreaLayoutHelper) {
                    ((FixAreaLayoutHelper)layoutHelper).Oo8Oo00oo(this.\u30078\u3007oO\u3007\u30078o);
                }
                final boolean b = layoutHelper instanceof BaseLayoutHelper;
                if (layoutHelper.\u3007\u3007888() > 0) {
                    layoutHelper.\u30070\u3007O0088o(n, layoutHelper.\u3007\u3007888() + n - 1);
                }
                else {
                    layoutHelper.\u30070\u3007O0088o(-1, -1);
                }
                n += layoutHelper.\u3007\u3007888();
            }
        }
        this.o8\u3007OO0\u30070o.O8(list);
        for (final LayoutHelper value2 : this.o8\u3007OO0\u30070o.\u3007o00\u3007\u3007Oo()) {
            this.ooo0\u3007\u3007O.put(System.identityHashCode(value2), value2);
        }
        final Iterator<Map.Entry<Integer, LayoutHelper>> iterator4 = this.\u3007\u300708O.entrySet().iterator();
        while (iterator4.hasNext()) {
            final Integer n2 = ((Map.Entry<Integer, V>)iterator4.next()).getKey();
            if (this.ooo0\u3007\u3007O.containsKey(n2)) {
                this.ooo0\u3007\u3007O.remove(n2);
                iterator4.remove();
            }
        }
        final Iterator<LayoutHelper> iterator5 = this.\u3007\u300708O.values().iterator();
        while (iterator5.hasNext()) {
            iterator5.next().O8(this);
        }
        if (!this.\u3007\u300708O.isEmpty() || !this.ooo0\u3007\u3007O.isEmpty()) {
            this.oo8ooo8O = false;
        }
        this.\u3007\u300708O.clear();
        this.ooo0\u3007\u3007O.clear();
        ((RecyclerView.LayoutManager)this).requestLayout();
    }
    
    @Override
    public final View o\u3007O8\u3007\u3007o() {
        final RecyclerView oo = this.OO;
        if (oo == null) {
            return null;
        }
        final View \u3007080 = this.oOO\u3007\u3007.\u3007080(((View)oo).getContext());
        final LayoutParams layoutParams = new LayoutParams(-2, -2);
        ExposeLinearLayoutManagerEx.attachViewHolder(layoutParams, new LayoutViewHolder(\u3007080));
        \u3007080.setLayoutParams((ViewGroup$LayoutParams)layoutParams);
        return \u3007080;
    }
    
    public ViewHolder o\u3007\u30070\u3007(final View view) {
        final RecyclerView oo = this.OO;
        if (oo != null) {
            return oo.getChildViewHolder(view);
        }
        return null;
    }
    
    @Override
    protected void recycleChildren(final Recycler recycler, int i, final int n) {
        if (i == n) {
            return;
        }
        if (VirtualLayoutManager.\u3007\u3007o\u3007) {
            final StringBuilder sb = new StringBuilder();
            sb.append("Recycling ");
            sb.append(Math.abs(i - n));
            sb.append(" items");
        }
        if (n > i) {
            final View child = ((RecyclerView.LayoutManager)this).getChildAt(n - 1);
            final int position = ((RecyclerView.LayoutManager)this).getPosition(((RecyclerView.LayoutManager)this).getChildAt(i));
            final int position2 = ((RecyclerView.LayoutManager)this).getPosition(child);
            int n2 = i;
            while (i < n) {
                final int position3 = ((RecyclerView.LayoutManager)this).getPosition(((RecyclerView.LayoutManager)this).getChildAt(n2));
                if (position3 != -1) {
                    final LayoutHelper \u3007080 = this.o8\u3007OO0\u30070o.\u3007080(position3);
                    if (\u3007080 != null && !\u3007080.\u30078o8o\u3007(position3, position, position2, this, true)) {
                        ++n2;
                    }
                    else {
                        ((RecyclerView.LayoutManager)this).removeAndRecycleViewAt(n2, recycler);
                    }
                }
                else {
                    ((RecyclerView.LayoutManager)this).removeAndRecycleViewAt(n2, recycler);
                }
                ++i;
            }
        }
        else {
            final View child2 = ((RecyclerView.LayoutManager)this).getChildAt(i);
            final int position4 = ((RecyclerView.LayoutManager)this).getPosition(((RecyclerView.LayoutManager)this).getChildAt(n + 1));
            final int position5 = ((RecyclerView.LayoutManager)this).getPosition(child2);
            while (i > n) {
                final int position6 = ((RecyclerView.LayoutManager)this).getPosition(((RecyclerView.LayoutManager)this).getChildAt(i));
                if (position6 != -1) {
                    final LayoutHelper \u300781 = this.o8\u3007OO0\u30070o.\u3007080(position6);
                    if (\u300781 == null || \u300781.\u30078o8o\u3007(position6, position4, position5, this, false)) {
                        ((RecyclerView.LayoutManager)this).removeAndRecycleViewAt(i, recycler);
                    }
                }
                else {
                    ((RecyclerView.LayoutManager)this).removeAndRecycleViewAt(i, recycler);
                }
                --i;
            }
        }
    }
    
    @Override
    protected int scrollInternalBy(int scrollInternalBy, final Recycler recycler, final State state) {
        Trace.beginSection("VLM scroll");
        this.\u3007o(recycler, state);
        int n = 0;
        while (true) {
            try {
                try {
                    if (!this.\u300708O\u300700\u3007o) {
                        scrollInternalBy = super.scrollInternalBy(scrollInternalBy, recycler, state);
                    }
                    else {
                        if (((RecyclerView.LayoutManager)this).getChildCount() == 0 || scrollInternalBy == 0) {
                            this.\u300700\u30078(recycler, state, 0);
                            return 0;
                        }
                        super.mLayoutState.\u3007o\u3007 = true;
                        this.ensureLayoutStateExpose();
                        int n2;
                        if (scrollInternalBy > 0) {
                            n2 = 1;
                        }
                        else {
                            n2 = -1;
                        }
                        final int abs = Math.abs(scrollInternalBy);
                        this.updateLayoutStateExpose(n2, abs, true, state);
                        final LayoutState mLayoutState = super.mLayoutState;
                        n = mLayoutState.\u300780\u3007808\u3007O + this.fill(recycler, mLayoutState, state, false);
                        if (n < 0) {
                            this.\u300700\u30078(recycler, state, 0);
                            return 0;
                        }
                        if (abs > n) {
                            scrollInternalBy = n2 * n;
                        }
                    }
                    this.\u300700\u30078(recycler, state, scrollInternalBy);
                }
                finally {}
            }
            catch (final Exception ex) {
                Log.getStackTraceString((Throwable)ex);
                if (!VirtualLayoutManager.\u3007\u3007o\u3007) {
                    scrollInternalBy = n;
                    continue;
                }
                throw ex;
            }
            break;
        }
        Trace.endSection();
        return scrollInternalBy;
        this.\u300700\u30078(recycler, state, 0);
    }
    
    @Override
    public void scrollToPosition(final int n) {
        super.scrollToPosition(n);
    }
    
    @Override
    public void scrollToPositionWithOffset(final int n, final int n2) {
        super.scrollToPositionWithOffset(n, n2);
    }
    
    @Override
    public void setOrientation(final int orientation) {
        this.o0 = OrientationHelperEx.\u3007o00\u3007\u3007Oo(this, orientation);
        super.setOrientation(orientation);
    }
    
    @Override
    public void setReverseLayout(final boolean b) {
        if (!b) {
            super.setReverseLayout(false);
            return;
        }
        throw new UnsupportedOperationException("VirtualLayoutManager does not support reverse layout in current version.");
    }
    
    @Override
    public void setStackFromEnd(final boolean b) {
        if (!b) {
            super.setStackFromEnd(false);
            return;
        }
        throw new UnsupportedOperationException("VirtualLayoutManager does not support stack from end.");
    }
    
    @Override
    public void showView(final View view) {
        super.showView(view);
    }
    
    @Override
    public void smoothScrollToPosition(final RecyclerView recyclerView, final State state, final int n) {
        super.smoothScrollToPosition(recyclerView, state, n);
    }
    
    @Override
    public boolean supportsPredictiveItemAnimations() {
        return super.mCurrentPendingSavedState == null;
    }
    
    @Override
    public int \u300700(final int n, final int n2, final boolean b) {
        return RecyclerView.LayoutManager.getChildMeasureSpec(n, 0, n2, b);
    }
    
    @Override
    public void \u30070\u3007O0088o(final LayoutStateWrapper layoutStateWrapper, final View view, final int n) {
        this.showView(view);
        if (!layoutStateWrapper.\u300780\u3007808\u3007O()) {
            ((RecyclerView.LayoutManager)this).addView(view, n);
        }
        else {
            ((RecyclerView.LayoutManager)this).addDisappearingView(view, n);
        }
    }
    
    @Override
    public int \u300780\u3007808\u3007O() {
        return super.getWidth();
    }
    
    @Override
    public void \u30078o8o\u3007(final View view, final int n) {
        super.addView(view, n);
    }
    
    @Override
    public void \u3007O00(final View view) {
        if (this.OO != null) {
            final ViewParent parent = view.getParent();
            if (parent != null) {
                final RecyclerView oo = this.OO;
                if (parent == oo) {
                    this.OO.getRecycledViewPool().putRecycledView(oo.getChildViewHolder(view));
                }
            }
        }
    }
    
    @Override
    public void \u3007O8o08O(final View view) {
        ((RecyclerView.LayoutManager)this).removeView(view);
    }
    
    @Override
    public boolean \u3007o00\u3007\u3007Oo(final View view) {
        final ViewHolder o\u3007\u30070\u3007 = this.o\u3007\u30070\u3007(view);
        return o\u3007\u30070\u3007 == null || ExposeLinearLayoutManagerEx.isViewHolderUpdated(o\u3007\u30070\u3007);
    }
    
    public LayoutHelper \u3007oOO8O8(LayoutHelper layoutHelper, final boolean b) {
        if (layoutHelper == null) {
            return null;
        }
        final List<LayoutHelper> \u3007o00\u3007\u3007Oo = this.o8\u3007OO0\u30070o.\u3007o00\u3007\u3007Oo();
        int index = \u3007o00\u3007\u3007Oo.indexOf(layoutHelper);
        if (index == -1) {
            return null;
        }
        if (b) {
            --index;
        }
        else {
            ++index;
        }
        if (index >= 0 && index < \u3007o00\u3007\u3007Oo.size()) {
            layoutHelper = (LayoutHelper)\u3007o00\u3007\u3007Oo.get(index);
            if (layoutHelper != null) {
                if (layoutHelper.\u300780\u3007808\u3007O()) {
                    return null;
                }
                return layoutHelper;
            }
        }
        return null;
    }
    
    @Override
    public LayoutHelper \u3007o\u3007(final int n) {
        return this.o8\u3007OO0\u30070o.\u3007080(n);
    }
    
    @Override
    public int \u3007\u3007808\u3007() {
        return super.getHeight();
    }
    
    @Override
    public void \u3007\u3007888(final View view) {
        this.OoO8(view, false);
    }
    
    @Override
    public boolean \u3007\u30078O0\u30078() {
        return this.isLayoutRTL();
    }
    
    public static class AnchorInfoWrapper
    {
        public int \u3007080;
        public int \u3007o00\u3007\u3007Oo;
        public boolean \u3007o\u3007;
        
        AnchorInfoWrapper() {
        }
    }
    
    public interface CacheViewHolder
    {
        boolean \u3007080();
    }
    
    public static class InflateLayoutParams extends LayoutParams
    {
        public InflateLayoutParams(final Context context, final AttributeSet set) {
            super(context, set);
        }
    }
    
    public static class LayoutParams extends RecyclerView.LayoutParams
    {
        private int OO;
        public int o0;
        private int \u300708O\u300700\u3007o;
        public float \u3007OOo8\u30070;
        
        public LayoutParams(final int n, final int n2) {
            super(n, n2);
            this.o0 = 0;
            this.\u3007OOo8\u30070 = Float.NaN;
            this.OO = Integer.MIN_VALUE;
            this.\u300708O\u300700\u3007o = Integer.MIN_VALUE;
        }
        
        public LayoutParams(final Context context, final AttributeSet set) {
            super(context, set);
            this.o0 = 0;
            this.\u3007OOo8\u30070 = Float.NaN;
            this.OO = Integer.MIN_VALUE;
            this.\u300708O\u300700\u3007o = Integer.MIN_VALUE;
        }
        
        public LayoutParams(final ViewGroup$LayoutParams viewGroup$LayoutParams) {
            super(viewGroup$LayoutParams);
            this.o0 = 0;
            this.\u3007OOo8\u30070 = Float.NaN;
            this.OO = Integer.MIN_VALUE;
            this.\u300708O\u300700\u3007o = Integer.MIN_VALUE;
        }
        
        public LayoutParams(final ViewGroup$MarginLayoutParams viewGroup$MarginLayoutParams) {
            super(viewGroup$MarginLayoutParams);
            this.o0 = 0;
            this.\u3007OOo8\u30070 = Float.NaN;
            this.OO = Integer.MIN_VALUE;
            this.\u300708O\u300700\u3007o = Integer.MIN_VALUE;
        }
        
        public LayoutParams(final RecyclerView.LayoutParams layoutParams) {
            super(layoutParams);
            this.o0 = 0;
            this.\u3007OOo8\u30070 = Float.NaN;
            this.OO = Integer.MIN_VALUE;
            this.\u300708O\u300700\u3007o = Integer.MIN_VALUE;
        }
    }
    
    public static class LayoutStateWrapper
    {
        private LayoutState \u3007080;
        
        LayoutStateWrapper() {
        }
        
        public int O8() {
            return this.\u3007080.OO0o\u3007\u3007\u3007\u30070;
        }
        
        public void OO0o\u3007\u3007() {
            final LayoutState \u3007080 = this.\u3007080;
            \u3007080.o\u30070 += \u3007080.\u3007\u3007888;
        }
        
        public boolean OO0o\u3007\u3007\u3007\u30070() {
            return this.\u3007080.\u3007o00\u3007\u3007Oo;
        }
        
        public int Oo08() {
            return this.\u3007080.\u3007\u3007888;
        }
        
        public boolean oO80(final State state) {
            return this.\u3007080.\u3007080(state);
        }
        
        public int o\u30070() {
            return this.\u3007080.oO80;
        }
        
        public boolean \u300780\u3007808\u3007O() {
            return this.\u3007080.OO0o\u3007\u3007 != null;
        }
        
        public View \u30078o8o\u3007(final Recycler recycler) {
            return this.\u3007080.\u3007o00\u3007\u3007Oo(recycler);
        }
        
        public View \u3007O8o08O(final Recycler recycler, final int o\u30070) {
            final LayoutState \u3007080 = this.\u3007080;
            final int o\u30072 = \u3007080.o\u30070;
            \u3007080.o\u30070 = o\u30070;
            final View \u30078o8o\u3007 = this.\u30078o8o\u3007(recycler);
            this.\u3007080.o\u30070 = o\u30072;
            return \u30078o8o\u3007;
        }
        
        public int \u3007o00\u3007\u3007Oo() {
            return this.\u3007080.Oo08;
        }
        
        public int \u3007o\u3007() {
            return this.\u3007080.o\u30070;
        }
        
        public int \u3007\u3007888() {
            return this.\u3007080.O8;
        }
    }
    
    private static class LayoutViewHolder extends ViewHolder
    {
        public LayoutViewHolder(final View view) {
            super(view);
        }
    }
}

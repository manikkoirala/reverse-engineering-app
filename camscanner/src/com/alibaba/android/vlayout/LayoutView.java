// 
// Decompiled by Procyon v0.6.0
// 

package com.alibaba.android.vlayout;

import android.util.AttributeSet;
import android.content.Context;
import android.view.View;

public final class LayoutView extends View
{
    public LayoutView(final Context context) {
        super(context);
    }
    
    public LayoutView(final Context context, final AttributeSet set) {
        super(context, set);
    }
    
    public LayoutView(final Context context, final AttributeSet set, final int n) {
        super(context, set, n);
    }
}

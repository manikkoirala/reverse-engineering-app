// 
// Decompiled by Procyon v0.6.0
// 

package com.alibaba.android.vlayout;

import java.util.Objects;
import androidx.annotation.NonNull;

public final class Range<T extends Comparable<? super T>>
{
    private final T \u3007080;
    private final T \u3007o00\u3007\u3007Oo;
    
    public Range(@NonNull final T \u3007080, @NonNull final T \u3007o00\u3007\u3007Oo) {
        if (\u3007080 == null) {
            throw new IllegalArgumentException("lower must not be null");
        }
        if (\u3007o00\u3007\u3007Oo == null) {
            throw new IllegalArgumentException("upper must not be null");
        }
        this.\u3007080 = \u3007080;
        this.\u3007o00\u3007\u3007Oo = \u3007o00\u3007\u3007Oo;
        if (\u3007080.compareTo(\u3007o00\u3007\u3007Oo) <= 0) {
            return;
        }
        throw new IllegalArgumentException("lower must be less than or equal to upper");
    }
    
    public static <T extends Comparable<? super T>> Range<T> \u3007o\u3007(final T t, final T t2) {
        return new Range<T>(t, t2);
    }
    
    public T O8() {
        return this.\u3007080;
    }
    
    public T Oo08() {
        return this.\u3007o00\u3007\u3007Oo;
    }
    
    @Override
    public boolean equals(final Object o) {
        final boolean b = false;
        if (o == null) {
            return false;
        }
        if (this == o) {
            return true;
        }
        boolean b2 = b;
        if (o instanceof Range) {
            final Range range = (Range)o;
            b2 = b;
            if (this.\u3007080.equals(range.\u3007080)) {
                b2 = b;
                if (this.\u3007o00\u3007\u3007Oo.equals(range.\u3007o00\u3007\u3007Oo)) {
                    b2 = true;
                }
            }
        }
        return b2;
    }
    
    @Override
    public int hashCode() {
        return Objects.hash(this.\u3007080, this.\u3007o00\u3007\u3007Oo);
    }
    
    @Override
    public String toString() {
        return String.format("[%s, %s]", this.\u3007080, this.\u3007o00\u3007\u3007Oo);
    }
    
    public boolean \u3007080(@NonNull final Range<T> range) {
        if (range != null) {
            final int compareTo = range.\u3007080.compareTo(this.\u3007080);
            boolean b = true;
            final boolean b2 = compareTo >= 0;
            final boolean b3 = range.\u3007o00\u3007\u3007Oo.compareTo(this.\u3007o00\u3007\u3007Oo) <= 0;
            if (!b2 || !b3) {
                b = false;
            }
            return b;
        }
        throw new IllegalArgumentException("value must not be null");
    }
    
    public boolean \u3007o00\u3007\u3007Oo(@NonNull final T t) {
        if (t != null) {
            final int compareTo = t.compareTo(this.\u3007080);
            boolean b = true;
            final boolean b2 = compareTo >= 0;
            final boolean b3 = t.compareTo(this.\u3007o00\u3007\u3007Oo) <= 0;
            if (!b2 || !b3) {
                b = false;
            }
            return b;
        }
        throw new IllegalArgumentException("value must not be null");
    }
}

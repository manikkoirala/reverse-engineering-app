// 
// Decompiled by Procyon v0.6.0
// 

package com.alibaba.android.vlayout;

import java.util.ListIterator;
import java.util.Arrays;
import androidx.annotation.Nullable;
import java.util.LinkedList;
import java.util.List;
import androidx.annotation.NonNull;
import java.util.Comparator;

public class RangeLayoutHelperFinder extends LayoutHelperFinder
{
    private LayoutHelperItem[] O8;
    @NonNull
    private Comparator<LayoutHelperItem> Oo08;
    @NonNull
    private List<LayoutHelperItem> \u3007080;
    @NonNull
    private List<LayoutHelper> \u3007o00\u3007\u3007Oo;
    @NonNull
    private List<LayoutHelper> \u3007o\u3007;
    
    public RangeLayoutHelperFinder() {
        this.\u3007080 = new LinkedList<LayoutHelperItem>();
        this.\u3007o00\u3007\u3007Oo = new LinkedList<LayoutHelper>();
        this.\u3007o\u3007 = new LinkedList<LayoutHelper>();
        this.O8 = null;
        this.Oo08 = new Comparator<LayoutHelperItem>() {
            final RangeLayoutHelperFinder o0;
            
            public int \u3007080(final LayoutHelperItem layoutHelperItem, final LayoutHelperItem layoutHelperItem2) {
                return layoutHelperItem.\u3007o00\u3007\u3007Oo() - layoutHelperItem2.\u3007o00\u3007\u3007Oo();
            }
        };
    }
    
    public void O8(@Nullable final List<LayoutHelper> list) {
        this.\u3007o00\u3007\u3007Oo.clear();
        this.\u3007o\u3007.clear();
        this.\u3007080.clear();
        if (list != null) {
            final ListIterator<LayoutHelper> listIterator = list.listIterator();
            while (listIterator.hasNext()) {
                final LayoutHelper layoutHelper = listIterator.next();
                this.\u3007o00\u3007\u3007Oo.add(layoutHelper);
                this.\u3007080.add(new LayoutHelperItem(layoutHelper));
            }
            while (listIterator.hasPrevious()) {
                this.\u3007o\u3007.add(listIterator.previous());
            }
            final List<LayoutHelperItem> \u3007080 = this.\u3007080;
            Arrays.sort(this.O8 = \u3007080.toArray(new LayoutHelperItem[\u3007080.size()]), this.Oo08);
        }
    }
    
    @Nullable
    @Override
    public LayoutHelper \u3007080(final int n) {
        final LayoutHelperItem[] o8 = this.O8;
        LayoutHelper \u3007080;
        final LayoutHelper layoutHelper = \u3007080 = null;
        if (o8 != null) {
            if (o8.length != 0) {
                int n2 = o8.length - 1;
                int i = 0;
                while (true) {
                    while (i <= n2) {
                        final int n3 = (i + n2) / 2;
                        final LayoutHelperItem layoutHelperItem = this.O8[n3];
                        if (layoutHelperItem.\u3007o00\u3007\u3007Oo() > n) {
                            n2 = n3 - 1;
                        }
                        else if (layoutHelperItem.\u3007080() < n) {
                            i = n3 + 1;
                        }
                        else {
                            if (layoutHelperItem.\u3007o00\u3007\u3007Oo() > n || layoutHelperItem.\u3007080() < n) {
                                continue;
                            }
                            if (layoutHelperItem == null) {
                                \u3007080 = layoutHelper;
                                return \u3007080;
                            }
                            \u3007080 = layoutHelperItem.\u3007080;
                            return \u3007080;
                        }
                    }
                    final LayoutHelperItem layoutHelperItem = null;
                    continue;
                }
            }
            \u3007080 = layoutHelper;
        }
        return \u3007080;
    }
    
    @NonNull
    @Override
    protected List<LayoutHelper> \u3007o00\u3007\u3007Oo() {
        return this.\u3007o00\u3007\u3007Oo;
    }
    
    @Override
    protected List<LayoutHelper> \u3007o\u3007() {
        return this.\u3007o\u3007;
    }
    
    static class LayoutHelperItem
    {
        LayoutHelper \u3007080;
        
        LayoutHelperItem(final LayoutHelper \u3007080) {
            this.\u3007080 = \u3007080;
        }
        
        public int \u3007080() {
            return this.\u3007080.oO80().Oo08();
        }
        
        public int \u3007o00\u3007\u3007Oo() {
            return this.\u3007080.oO80().O8();
        }
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.alibaba.android.vlayout;

import android.view.View;
import android.view.ViewGroup$LayoutParams;
import androidx.viewpager.widget.ViewPager;
import android.view.ViewGroup;
import com.alibaba.android.vlayout.extend.InnerRecycledViewPool;
import androidx.viewpager.widget.PagerAdapter;
import androidx.recyclerview.widget.RecyclerView;

public abstract class RecyclablePagerAdapter<VH extends RecyclerView.ViewHolder> extends PagerAdapter
{
    private RecyclerView.Adapter<VH> \u3007080;
    private InnerRecycledViewPool \u3007o00\u3007\u3007Oo;
    
    @Override
    public void destroyItem(final ViewGroup viewGroup, final int n, final Object o) {
        if (o instanceof RecyclerView.ViewHolder) {
            final RecyclerView.ViewHolder viewHolder = (RecyclerView.ViewHolder)o;
            viewGroup.removeView(viewHolder.itemView);
            this.\u3007o00\u3007\u3007Oo.putRecycledView(viewHolder);
        }
    }
    
    @Override
    public Object instantiateItem(final ViewGroup viewGroup, final int n) {
        final int \u3007080 = this.\u3007080(n);
        RecyclerView.ViewHolder viewHolder;
        if ((viewHolder = this.\u3007o00\u3007\u3007Oo.getRecycledView(\u3007080)) == null) {
            viewHolder = this.\u3007080.createViewHolder(viewGroup, \u3007080);
        }
        this.\u3007o00\u3007\u3007Oo((VH)viewHolder, n);
        viewGroup.addView(viewHolder.itemView, (ViewGroup$LayoutParams)new ViewPager.LayoutParams());
        return viewHolder;
    }
    
    @Override
    public boolean isViewFromObject(final View view, final Object o) {
        return o instanceof RecyclerView.ViewHolder && ((RecyclerView.ViewHolder)o).itemView == view;
    }
    
    public abstract int \u3007080(final int p0);
    
    public abstract void \u3007o00\u3007\u3007Oo(final VH p0, final int p1);
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.alibaba.android.vlayout;

public class Cantor
{
    public static long \u3007080(long n, final long n2) {
        n += n2;
        return n * (1L + n) / 2L + n2;
    }
    
    public static void \u3007o00\u3007\u3007Oo(long n, final long[] array) {
        long[] array2 = null;
        Label_0018: {
            if (array != null) {
                array2 = array;
                if (array.length >= 2) {
                    break Label_0018;
                }
            }
            array2 = new long[2];
        }
        final long n2 = (long)(Math.floor(Math.sqrt((double)(8L * n + 1L)) - 1.0) / 2.0);
        n -= (n2 * n2 + n2) / 2L;
        array2[0] = n2 - n;
        array2[1] = n;
    }
}

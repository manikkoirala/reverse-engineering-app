// 
// Decompiled by Procyon v0.6.0
// 

package com.alibaba.android.arouter.utils;

import java.util.LinkedHashMap;
import java.util.Collections;
import java.util.Map;
import android.net.Uri;

public class TextUtils
{
    public static String \u3007080(final StackTraceElement[] array) {
        final StringBuilder sb = new StringBuilder();
        for (final StackTraceElement stackTraceElement : array) {
            sb.append("    at ");
            sb.append(stackTraceElement.toString());
            sb.append("\n");
        }
        return sb.toString();
    }
    
    public static boolean \u3007o00\u3007\u3007Oo(final CharSequence charSequence) {
        return charSequence == null || charSequence.length() == 0;
    }
    
    public static Map<String, String> \u3007o\u3007(final Uri uri) {
        final String encodedQuery = uri.getEncodedQuery();
        if (encodedQuery == null) {
            return Collections.emptyMap();
        }
        final LinkedHashMap m = new LinkedHashMap();
        int beginIndex = 0;
        int i;
        do {
            if ((i = encodedQuery.indexOf(38, beginIndex)) == -1) {
                i = encodedQuery.length();
            }
            final int index = encodedQuery.indexOf(61, beginIndex);
            int endIndex;
            if (index > i || (endIndex = index) == -1) {
                endIndex = i;
            }
            final String substring = encodedQuery.substring(beginIndex, endIndex);
            if (!android.text.TextUtils.isEmpty((CharSequence)substring)) {
                String substring2;
                if (endIndex == i) {
                    substring2 = "";
                }
                else {
                    substring2 = encodedQuery.substring(endIndex + 1, i);
                }
                m.put(Uri.decode(substring), Uri.decode(substring2));
            }
            beginIndex = ++i;
        } while (i < encodedQuery.length());
        return (Map<String, String>)Collections.unmodifiableMap((Map<?, ?>)m);
    }
}

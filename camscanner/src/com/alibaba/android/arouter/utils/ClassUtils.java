// 
// Decompiled by Procyon v0.6.0
// 

package com.alibaba.android.arouter.utils;

import java.util.Enumeration;
import dalvik.system.DexFile;
import android.content.SharedPreferences;
import com.alibaba.android.arouter.facade.template.ILogger;
import java.util.Iterator;
import \u3007\u3007888.\u3007080;
import com.alibaba.android.arouter.thread.DefaultPoolExecutor;
import java.util.HashSet;
import java.util.concurrent.CountDownLatch;
import java.util.Set;
import java.util.Arrays;
import android.content.pm.ApplicationInfo;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import android.content.pm.PackageManager$NameNotFoundException;
import java.util.Collection;
import com.alibaba.android.arouter.launcher.ARouter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import android.content.Context;
import java.io.File;

public class ClassUtils
{
    private static final String \u3007080;
    
    static {
        final StringBuilder sb = new StringBuilder();
        sb.append("code_cache");
        sb.append(File.separator);
        sb.append("secondary-dexes");
        \u3007080 = sb.toString();
    }
    
    public static List<String> O8(final Context context) throws PackageManager$NameNotFoundException, IOException {
        final String packageCodePath = context.getPackageCodePath();
        final File file = new File(packageCodePath);
        final StringBuilder sb = new StringBuilder();
        sb.append("/data/user/0/");
        sb.append(context.getPackageName());
        final String string = sb.toString();
        final ArrayList list = new ArrayList();
        list.add(packageCodePath);
        final StringBuilder sb2 = new StringBuilder();
        sb2.append(file.getName());
        sb2.append(".classes");
        final String string2 = sb2.toString();
        if (!Oo08()) {
            final int int1 = \u3007o\u3007(context).getInt("dex.number", 1);
            final File parent = new File(string, ClassUtils.\u3007080);
            for (int i = 2; i <= int1; ++i) {
                final StringBuilder sb3 = new StringBuilder();
                sb3.append(string2);
                sb3.append(i);
                sb3.append(".zip");
                final File file2 = new File(parent, sb3.toString());
                if (!file2.isFile()) {
                    final StringBuilder sb4 = new StringBuilder();
                    sb4.append("Missing extracted secondary dex file '");
                    sb4.append(file2.getPath());
                    sb4.append("'");
                    throw new IOException(sb4.toString());
                }
                list.add(file2.getAbsolutePath());
            }
        }
        if (ARouter.\u3007o00\u3007\u3007Oo()) {
            list.addAll(oO80(context.getPackageManager().getApplicationInfo(context.getPackageName(), 0)));
        }
        return list;
    }
    
    private static boolean Oo08() {
        final boolean b = false;
        String s = null;
        boolean b2 = false;
        String str = null;
        Label_0188: {
            try {
                if (o\u30070()) {
                    final String s2 = s = "'YunOS'";
                    b2 = b;
                    str = s2;
                    if (Integer.valueOf(System.getProperty("ro.build.version.sdk")) < 21) {
                        break Label_0188;
                    }
                    str = s2;
                }
                else {
                    final String s3 = s = "'Android'";
                    final String property = System.getProperty("java.vm.version");
                    b2 = b;
                    str = s3;
                    if (property == null) {
                        break Label_0188;
                    }
                    s = s3;
                    final Matcher matcher = Pattern.compile("(\\d+)\\.(\\d+)(\\.\\d+)?").matcher(property);
                    s = s3;
                    final boolean matches = matcher.matches();
                    b2 = b;
                    str = s3;
                    if (!matches) {
                        break Label_0188;
                    }
                    s = s3;
                    final int int1 = Integer.parseInt(matcher.group(1));
                    s = s3;
                    final int int2 = Integer.parseInt(matcher.group(2));
                    str = s3;
                    if (int1 <= 2) {
                        b2 = b;
                        str = s3;
                        if (int1 != 2) {
                            break Label_0188;
                        }
                        b2 = b;
                        str = s3;
                        if (int2 < 1) {
                            break Label_0188;
                        }
                        str = s3;
                    }
                }
                b2 = true;
            }
            catch (final Exception | NumberFormatException ex) {
                str = s;
                b2 = b;
            }
        }
        final StringBuilder sb = new StringBuilder();
        sb.append("VM with name ");
        sb.append(str);
        String str2;
        if (b2) {
            str2 = " has multidex support";
        }
        else {
            str2 = " does not have multidex support";
        }
        sb.append(str2);
        return b2;
    }
    
    private static List<String> oO80(final ApplicationInfo applicationInfo) {
        final ArrayList list = new ArrayList();
        final String[] splitSourceDirs = applicationInfo.splitSourceDirs;
        if (splitSourceDirs != null) {
            list.addAll(Arrays.asList(splitSourceDirs));
        }
        else {
            try {
                final Class<?> forName = Class.forName("com.android.tools.fd.runtime.Paths");
                int i = 0;
                final File file = new File((String)forName.getMethod("getDexFileDirectory", String.class).invoke(null, applicationInfo.packageName));
                if (file.exists() && file.isDirectory()) {
                    for (File[] listFiles = file.listFiles(); i < listFiles.length; ++i) {
                        final File file2 = listFiles[i];
                        if (file2 != null && file2.exists() && file2.isFile() && file2.getName().endsWith(".dex")) {
                            list.add(file2.getAbsolutePath());
                        }
                    }
                }
            }
            catch (final Exception ex) {
                final StringBuilder sb = new StringBuilder();
                sb.append("InstantRun support error, ");
                sb.append(ex.getMessage());
            }
        }
        return list;
    }
    
    private static boolean o\u30070() {
        final boolean b = false;
        try {
            final String property = System.getProperty("ro.yunos.version");
            final String property2 = System.getProperty("java.vm.name");
            if (property2 == null || !property2.toLowerCase().contains("lemur")) {
                boolean b2 = b;
                if (property == null) {
                    return b2;
                }
                final int length = property.trim().length();
                b2 = b;
                if (length <= 0) {
                    return b2;
                }
            }
            return true;
        }
        catch (final Exception ex) {
            return b;
        }
    }
    
    public static Set<String> \u3007o00\u3007\u3007Oo(final Context context, final String str) throws PackageManager$NameNotFoundException, IOException, InterruptedException {
        final HashSet set = new HashSet();
        final List<String> o8 = O8(context);
        final CountDownLatch countDownLatch = new CountDownLatch(o8.size());
        final Iterator iterator = o8.iterator();
        while (iterator.hasNext()) {
            DefaultPoolExecutor.\u3007080().execute(new \u3007080((String)iterator.next(), str, set, countDownLatch));
        }
        countDownLatch.await();
        final ILogger \u3007o\u3007 = ARouter.\u3007o\u3007;
        final StringBuilder sb = new StringBuilder();
        sb.append("Filter ");
        sb.append(set.size());
        sb.append(" classes by packageName <");
        sb.append(str);
        sb.append(">");
        \u3007o\u3007.debug("ARouter::", sb.toString());
        return set;
    }
    
    private static SharedPreferences \u3007o\u3007(final Context context) {
        return context.getSharedPreferences("multidex.version", 4);
    }
}

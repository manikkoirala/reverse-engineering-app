// 
// Decompiled by Procyon v0.6.0
// 

package com.alibaba.android.arouter.utils;

import android.content.SharedPreferences;
import android.content.Context;

public class PackageUtils
{
    private static String \u3007080;
    private static int \u3007o00\u3007\u3007Oo;
    
    public static boolean \u3007080(final Context context, final String \u3007080, final int \u3007o00\u3007\u3007Oo) {
        final SharedPreferences sharedPreferences = context.getSharedPreferences("SP_AROUTER_CACHE", 0);
        if (\u3007080.equals(sharedPreferences.getString("LAST_VERSION_NAME", (String)null)) && \u3007o00\u3007\u3007Oo == sharedPreferences.getInt("LAST_VERSION_CODE", -1)) {
            return false;
        }
        PackageUtils.\u3007080 = \u3007080;
        PackageUtils.\u3007o00\u3007\u3007Oo = \u3007o00\u3007\u3007Oo;
        return true;
    }
    
    public static void \u3007o00\u3007\u3007Oo(final Context context) {
        if (!TextUtils.\u3007o00\u3007\u3007Oo(PackageUtils.\u3007080) && PackageUtils.\u3007o00\u3007\u3007Oo != 0) {
            context.getSharedPreferences("SP_AROUTER_CACHE", 0).edit().putString("LAST_VERSION_NAME", PackageUtils.\u3007080).putInt("LAST_VERSION_CODE", PackageUtils.\u3007o00\u3007\u3007Oo).apply();
        }
    }
}

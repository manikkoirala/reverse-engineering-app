// 
// Decompiled by Procyon v0.6.0
// 

package com.alibaba.android.arouter.utils;

import android.text.TextUtils;
import com.alibaba.android.arouter.facade.template.ILogger;

public class DefaultLogger implements ILogger
{
    private static boolean O8 = false;
    private static boolean \u3007o00\u3007\u3007Oo = false;
    private static boolean \u3007o\u3007 = false;
    private String \u3007080;
    
    public DefaultLogger(final String \u3007080) {
        this.\u3007080 = \u3007080;
    }
    
    public static String \u3007080(final StackTraceElement stackTraceElement) {
        if (DefaultLogger.\u3007o\u3007) {
            final StringBuilder sb = new StringBuilder("[");
            final String name = Thread.currentThread().getName();
            final String fileName = stackTraceElement.getFileName();
            final String className = stackTraceElement.getClassName();
            final String methodName = stackTraceElement.getMethodName();
            final long id = Thread.currentThread().getId();
            final int lineNumber = stackTraceElement.getLineNumber();
            sb.append("ThreadId=");
            sb.append(id);
            sb.append(" & ");
            sb.append("ThreadName=");
            sb.append(name);
            sb.append(" & ");
            sb.append("FileName=");
            sb.append(fileName);
            sb.append(" & ");
            sb.append("ClassName=");
            sb.append(className);
            sb.append(" & ");
            sb.append("MethodName=");
            sb.append(methodName);
            sb.append(" & ");
            sb.append("LineNumber=");
            sb.append(lineNumber);
            sb.append(" ] ");
            return sb.toString();
        }
        return "";
    }
    
    @Override
    public void debug(final String s, final String str) {
        if (DefaultLogger.\u3007o00\u3007\u3007Oo) {
            final StackTraceElement stackTraceElement = Thread.currentThread().getStackTrace()[3];
            if (TextUtils.isEmpty((CharSequence)s)) {
                this.getDefaultTag();
            }
            final StringBuilder sb = new StringBuilder();
            sb.append(str);
            sb.append(\u3007080(stackTraceElement));
        }
    }
    
    @Override
    public void error(final String s, final String str) {
        if (DefaultLogger.\u3007o00\u3007\u3007Oo) {
            final StackTraceElement stackTraceElement = Thread.currentThread().getStackTrace()[3];
            if (TextUtils.isEmpty((CharSequence)s)) {
                this.getDefaultTag();
            }
            final StringBuilder sb = new StringBuilder();
            sb.append(str);
            sb.append(\u3007080(stackTraceElement));
        }
    }
    
    @Override
    public void error(final String s, final String s2, final Throwable t) {
        if (DefaultLogger.\u3007o00\u3007\u3007Oo && TextUtils.isEmpty((CharSequence)s)) {
            this.getDefaultTag();
        }
    }
    
    @Override
    public String getDefaultTag() {
        return this.\u3007080;
    }
    
    @Override
    public void info(final String s, final String str) {
        if (DefaultLogger.\u3007o00\u3007\u3007Oo) {
            final StackTraceElement stackTraceElement = Thread.currentThread().getStackTrace()[3];
            if (TextUtils.isEmpty((CharSequence)s)) {
                this.getDefaultTag();
            }
            final StringBuilder sb = new StringBuilder();
            sb.append(str);
            sb.append(\u3007080(stackTraceElement));
        }
    }
    
    @Override
    public boolean isMonitorMode() {
        return DefaultLogger.O8;
    }
    
    @Override
    public void monitor(final String str) {
        if (DefaultLogger.\u3007o00\u3007\u3007Oo && this.isMonitorMode()) {
            final StackTraceElement stackTraceElement = Thread.currentThread().getStackTrace()[3];
            final StringBuilder sb = new StringBuilder();
            sb.append(this.\u3007080);
            sb.append("::monitor");
            final StringBuilder sb2 = new StringBuilder();
            sb2.append(str);
            sb2.append(\u3007080(stackTraceElement));
        }
    }
    
    @Override
    public void showLog(final boolean \u3007o00\u3007\u3007Oo) {
        DefaultLogger.\u3007o00\u3007\u3007Oo = \u3007o00\u3007\u3007Oo;
    }
    
    @Override
    public void showStackTrace(final boolean \u3007o\u3007) {
        DefaultLogger.\u3007o\u3007 = \u3007o\u3007;
    }
    
    @Override
    public void warning(final String s, final String str) {
        if (DefaultLogger.\u3007o00\u3007\u3007Oo) {
            final StackTraceElement stackTraceElement = Thread.currentThread().getStackTrace()[3];
            if (TextUtils.isEmpty((CharSequence)s)) {
                this.getDefaultTag();
            }
            final StringBuilder sb = new StringBuilder();
            sb.append(str);
            sb.append(\u3007080(stackTraceElement));
        }
    }
}

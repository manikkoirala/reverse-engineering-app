// 
// Decompiled by Procyon v0.6.0
// 

package com.alibaba.android.arouter.launcher;

import com.alibaba.android.arouter.facade.callback.NavigationCallback;
import com.alibaba.android.arouter.facade.Postcard;
import android.content.Context;
import com.alibaba.android.arouter.exception.InitException;
import android.app.Application;
import com.alibaba.android.arouter.facade.template.ILogger;

public final class ARouter
{
    private static volatile ARouter \u3007080;
    private static volatile boolean \u3007o00\u3007\u3007Oo = false;
    public static ILogger \u3007o\u3007;
    
    private ARouter() {
    }
    
    public static void O8(final Application application, final String s, final int n) {
        if (!ARouter.\u3007o00\u3007\u3007Oo) {
            (ARouter.\u3007o\u3007 = _ARouter.\u3007080).info("ARouter::", "ARouter init start.");
            ARouter.\u3007o00\u3007\u3007Oo = _ARouter.\u30078o8o\u3007(application, s, n);
            if (ARouter.\u3007o00\u3007\u3007Oo) {
                _ARouter.Oo08();
            }
            _ARouter.\u3007080.info("ARouter::", "ARouter init over.");
        }
    }
    
    public static boolean \u3007o00\u3007\u3007Oo() {
        return _ARouter.oO80();
    }
    
    public static ARouter \u3007o\u3007() {
        if (ARouter.\u3007o00\u3007\u3007Oo) {
            if (ARouter.\u3007080 == null) {
                synchronized (ARouter.class) {
                    if (ARouter.\u3007080 == null) {
                        ARouter.\u3007080 = new ARouter();
                    }
                }
            }
            return ARouter.\u3007080;
        }
        throw new InitException("ARouter::Init::Invoke init(context) first!");
    }
    
    public void Oo08(final Object o) {
        _ARouter.\u3007O8o08O(o);
    }
    
    public Object o\u30070(final Context context, final Postcard postcard, final int n, final NavigationCallback navigationCallback) {
        return _ARouter.OO0o\u3007\u3007\u3007\u30070().OO0o\u3007\u3007(context, postcard, n, navigationCallback);
    }
    
    public Postcard \u3007080(final String s) {
        return _ARouter.OO0o\u3007\u3007\u3007\u30070().o\u30070(s);
    }
    
    public <T> T \u3007\u3007888(final Class<? extends T> clazz) {
        return _ARouter.OO0o\u3007\u3007\u3007\u30070().Oooo8o0\u3007(clazz);
    }
}

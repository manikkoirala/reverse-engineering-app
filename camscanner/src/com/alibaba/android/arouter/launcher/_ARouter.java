// 
// Decompiled by Procyon v0.6.0
// 

package com.alibaba.android.arouter.launcher;

import com.alibaba.android.arouter.facade.service.PathReplaceService;
import com.alibaba.android.arouter.exception.NoRouteFoundException;
import com.alibaba.android.arouter.facade.service.DegradeService;
import android.widget.Toast;
import com.alibaba.android.arouter.facade.callback.InterceptorCallback;
import com.alibaba.android.arouter.facade.service.PretreatmentService;
import com.alibaba.android.arouter.facade.service.AutowiredService;
import android.os.Looper;
import com.alibaba.android.arouter.core.LogisticsCenter;
import android.app.Application;
import com.alibaba.android.arouter.exception.HandlerException;
import androidx.core.content.ContextCompat;
import androidx.core.app.ActivityCompat;
import com.alibaba.android.arouter.exception.InitException;
import android.app.Fragment;
import com.alibaba.android.arouter.utils.TextUtils;
import android.app.Activity;
import android.content.Intent;
import com.alibaba.android.arouter.facade.callback.NavigationCallback;
import com.alibaba.android.arouter.facade.Postcard;
import com.alibaba.android.arouter.thread.DefaultPoolExecutor;
import com.alibaba.android.arouter.utils.DefaultLogger;
import java.util.concurrent.ThreadPoolExecutor;
import android.content.Context;
import com.alibaba.android.arouter.facade.template.ILogger;
import android.os.Handler;
import com.alibaba.android.arouter.facade.service.InterceptorService;

final class _ARouter
{
    private static volatile boolean O8;
    private static InterceptorService OO0o\u3007\u3007\u3007\u30070;
    private static volatile _ARouter Oo08;
    private static Handler oO80;
    private static volatile boolean o\u30070;
    static ILogger \u3007080;
    private static Context \u300780\u3007808\u3007O;
    private static volatile boolean \u3007o00\u3007\u3007Oo;
    private static volatile boolean \u3007o\u3007;
    private static volatile ThreadPoolExecutor \u3007\u3007888;
    
    static {
        _ARouter.\u3007080 = new DefaultLogger("ARouter::");
        _ARouter.\u3007o00\u3007\u3007Oo = false;
        _ARouter.\u3007o\u3007 = false;
        _ARouter.O8 = false;
        _ARouter.Oo08 = null;
        _ARouter.o\u30070 = false;
        _ARouter.\u3007\u3007888 = DefaultPoolExecutor.\u3007080();
    }
    
    private _ARouter() {
    }
    
    private Object O8(final Postcard postcard, final int n, final NavigationCallback navigationCallback) {
        final Context context = postcard.getContext();
        final int n2 = _ARouter$4.\u3007080[postcard.getType().ordinal()];
        if (n2 == 1) {
            final Intent intent = new Intent(context, (Class)postcard.getDestination());
            intent.putExtras(postcard.getExtras());
            final int flags = postcard.getFlags();
            if (flags != 0) {
                intent.setFlags(flags);
            }
            if (!(context instanceof Activity)) {
                intent.addFlags(268435456);
            }
            final String action = postcard.getAction();
            if (!TextUtils.\u3007o00\u3007\u3007Oo(action)) {
                intent.setAction(action);
            }
            this.\u3007\u3007808\u3007(new Runnable(this, n, context, intent, postcard, navigationCallback) {
                final _ARouter O8o08O8O;
                final Intent OO;
                final int o0;
                final NavigationCallback o\u300700O;
                final Postcard \u300708O\u300700\u3007o;
                final Context \u3007OOo8\u30070;
                
                @Override
                public void run() {
                    this.O8o08O8O.startActivity(this.o0, this.\u3007OOo8\u30070, this.OO, this.\u300708O\u300700\u3007o, this.o\u300700O);
                }
            });
            return null;
        }
        if (n2 != 2) {
            if (n2 == 3 || n2 == 4 || n2 == 5) {
                final Class<?> destination = postcard.getDestination();
                try {
                    final Object instance = destination.getConstructor((Class<?>[])new Class[0]).newInstance(new Object[0]);
                    if (instance instanceof Fragment) {
                        ((Fragment)instance).setArguments(postcard.getExtras());
                    }
                    else if (instance instanceof androidx.fragment.app.Fragment) {
                        ((androidx.fragment.app.Fragment)instance).setArguments(postcard.getExtras());
                    }
                    return instance;
                }
                catch (final Exception ex) {
                    final ILogger \u3007080 = _ARouter.\u3007080;
                    final StringBuilder sb = new StringBuilder();
                    sb.append("Fetch fragment instance error, ");
                    sb.append(TextUtils.\u3007080(ex.getStackTrace()));
                    \u3007080.error("ARouter::", sb.toString());
                }
            }
            return null;
        }
        return postcard.getProvider();
    }
    
    protected static _ARouter OO0o\u3007\u3007\u3007\u30070() {
        if (_ARouter.o\u30070) {
            if (_ARouter.Oo08 == null) {
                synchronized (_ARouter.class) {
                    if (_ARouter.Oo08 == null) {
                        _ARouter.Oo08 = new _ARouter();
                    }
                }
            }
            return _ARouter.Oo08;
        }
        throw new InitException("ARouterCore::Init::Invoke init(context) first!");
    }
    
    static void Oo08() {
        _ARouter.OO0o\u3007\u3007\u3007\u30070 = (InterceptorService)ARouter.\u3007o\u3007().\u3007080("/arouter/service/interceptor").navigation();
    }
    
    static boolean oO80() {
        return _ARouter.\u3007o\u3007;
    }
    
    private void startActivity(final int n, final Context context, final Intent intent, final Postcard postcard, final NavigationCallback navigationCallback) {
        if (n >= 0) {
            if (context instanceof Activity) {
                ActivityCompat.startActivityForResult((Activity)context, intent, n, postcard.getOptionsBundle());
            }
            else {
                _ARouter.\u3007080.warning("ARouter::", "Must use [navigation(activity, ...)] to support [startActivityForResult]");
            }
        }
        else {
            ContextCompat.startActivity(context, intent, postcard.getOptionsBundle());
        }
        if (-1 != postcard.getEnterAnim() && -1 != postcard.getExitAnim() && context instanceof Activity) {
            ((Activity)context).overridePendingTransition(postcard.getEnterAnim(), postcard.getExitAnim());
        }
        if (navigationCallback != null) {
            navigationCallback.onArrival(postcard);
        }
    }
    
    private String \u300780\u3007808\u3007O(String substring) {
        if (!TextUtils.\u3007o00\u3007\u3007Oo(substring) && substring.startsWith("/")) {
            try {
                substring = substring.substring(1, substring.indexOf("/", 1));
                if (!TextUtils.\u3007o00\u3007\u3007Oo(substring)) {
                    return substring;
                }
                throw new HandlerException("ARouter::Extract the default group failed! There's nothing between 2 '/'!");
            }
            catch (final Exception ex) {
                final ILogger \u3007080 = _ARouter.\u3007080;
                final StringBuilder sb = new StringBuilder();
                sb.append("Failed to extract default group! ");
                sb.append(ex.getMessage());
                \u3007080.warning("ARouter::", sb.toString());
                return null;
            }
        }
        throw new HandlerException("ARouter::Extract the default group failed, the path must be start with '/' and contain more than 2 '/'!");
    }
    
    protected static boolean \u30078o8o\u3007(final Application \u300780\u3007808\u3007O, final String s, final int n) {
        synchronized (_ARouter.class) {
            LogisticsCenter.O8(_ARouter.\u300780\u3007808\u3007O = (Context)\u300780\u3007808\u3007O, s, n, _ARouter.\u3007\u3007888);
            _ARouter.\u3007080.info("ARouter::", "ARouter init success!");
            _ARouter.o\u30070 = true;
            _ARouter.oO80 = new Handler(Looper.getMainLooper());
            return true;
        }
    }
    
    static void \u3007O8o08O(final Object o) {
        final AutowiredService autowiredService = (AutowiredService)ARouter.\u3007o\u3007().\u3007080("/arouter/service/autowired").navigation();
        if (autowiredService != null) {
            autowiredService.autowire(o);
        }
    }
    
    private void \u3007\u3007808\u3007(final Runnable runnable) {
        if (Looper.getMainLooper().getThread() != Thread.currentThread()) {
            _ARouter.oO80.post(runnable);
        }
        else {
            runnable.run();
        }
    }
    
    protected Object OO0o\u3007\u3007(final Context context, final Postcard postcard, final int n, final NavigationCallback navigationCallback) {
        final PretreatmentService pretreatmentService = ARouter.\u3007o\u3007().\u3007\u3007888((Class<? extends PretreatmentService>)PretreatmentService.class);
        if (pretreatmentService != null && !pretreatmentService.onPretreatment(context, postcard)) {
            return null;
        }
        Context \u300780\u3007808\u3007O;
        if (context == null) {
            \u300780\u3007808\u3007O = _ARouter.\u300780\u3007808\u3007O;
        }
        else {
            \u300780\u3007808\u3007O = context;
        }
        postcard.setContext(\u300780\u3007808\u3007O);
        try {
            LogisticsCenter.\u3007o\u3007(postcard);
            if (navigationCallback != null) {
                navigationCallback.onFound(postcard);
            }
            if (!postcard.isGreenChannel()) {
                _ARouter.OO0o\u3007\u3007\u3007\u30070.doInterceptions(postcard, new InterceptorCallback(this, n, navigationCallback, postcard) {
                    final _ARouter O8;
                    final int \u3007080;
                    final NavigationCallback \u3007o00\u3007\u3007Oo;
                    final Postcard \u3007o\u3007;
                    
                    @Override
                    public void onContinue(final Postcard postcard) {
                        this.O8.O8(postcard, this.\u3007080, this.\u3007o00\u3007\u3007Oo);
                    }
                    
                    @Override
                    public void onInterrupt(final Throwable t) {
                        final NavigationCallback \u3007o00\u3007\u3007Oo = this.\u3007o00\u3007\u3007Oo;
                        if (\u3007o00\u3007\u3007Oo != null) {
                            \u3007o00\u3007\u3007Oo.onInterrupt(this.\u3007o\u3007);
                        }
                        final ILogger \u3007080 = _ARouter.\u3007080;
                        final StringBuilder sb = new StringBuilder();
                        sb.append("Navigation failed, termination by interceptor : ");
                        sb.append(t.getMessage());
                        \u3007080.info("ARouter::", sb.toString());
                    }
                });
                return null;
            }
            return this.O8(postcard, n, navigationCallback);
        }
        catch (final NoRouteFoundException ex) {
            _ARouter.\u3007080.warning("ARouter::", ex.getMessage());
            if (oO80()) {
                this.\u3007\u3007808\u3007(new Runnable(this, postcard) {
                    final Postcard o0;
                    final _ARouter \u3007OOo8\u30070;
                    
                    @Override
                    public void run() {
                        final Context \u3007o\u3007 = _ARouter.\u300780\u3007808\u3007O;
                        final StringBuilder sb = new StringBuilder();
                        sb.append("There's no route matched!\n Path = [");
                        sb.append(this.o0.getPath());
                        sb.append("]\n Group = [");
                        sb.append(this.o0.getGroup());
                        sb.append("]");
                        Toast.makeText(\u3007o\u3007, (CharSequence)sb.toString(), 1).show();
                    }
                });
            }
            if (navigationCallback != null) {
                navigationCallback.onLost(postcard);
            }
            else {
                final DegradeService degradeService = ARouter.\u3007o\u3007().\u3007\u3007888((Class<? extends DegradeService>)DegradeService.class);
                if (degradeService != null) {
                    degradeService.onLost(context, postcard);
                }
            }
            return null;
        }
    }
    
    protected <T> T Oooo8o0\u3007(final Class<? extends T> clazz) {
        try {
            Postcard postcard;
            if ((postcard = LogisticsCenter.\u3007o00\u3007\u3007Oo(clazz.getName())) == null) {
                postcard = LogisticsCenter.\u3007o00\u3007\u3007Oo(clazz.getSimpleName());
            }
            if (postcard == null) {
                return null;
            }
            postcard.setContext(_ARouter.\u300780\u3007808\u3007O);
            LogisticsCenter.\u3007o\u3007(postcard);
            return (T)postcard.getProvider();
        }
        catch (final NoRouteFoundException ex) {
            _ARouter.\u3007080.warning("ARouter::", ex.getMessage());
            return null;
        }
    }
    
    protected Postcard o\u30070(final String s) {
        if (!TextUtils.\u3007o00\u3007\u3007Oo(s)) {
            final PathReplaceService pathReplaceService = ARouter.\u3007o\u3007().\u3007\u3007888((Class<? extends PathReplaceService>)PathReplaceService.class);
            String forString = s;
            if (pathReplaceService != null) {
                forString = pathReplaceService.forString(s);
            }
            return this.\u3007\u3007888(forString, this.\u300780\u3007808\u3007O(forString), Boolean.TRUE);
        }
        throw new HandlerException("ARouter::Parameter is invalid!");
    }
    
    protected Postcard \u3007\u3007888(final String s, final String s2, final Boolean b) {
        if (!TextUtils.\u3007o00\u3007\u3007Oo(s) && !TextUtils.\u3007o00\u3007\u3007Oo(s2)) {
            String forString = s;
            if (!b) {
                final PathReplaceService pathReplaceService = ARouter.\u3007o\u3007().\u3007\u3007888((Class<? extends PathReplaceService>)PathReplaceService.class);
                forString = s;
                if (pathReplaceService != null) {
                    forString = pathReplaceService.forString(s);
                }
            }
            return new Postcard(forString, s2);
        }
        throw new HandlerException("ARouter::Parameter is invalid!");
    }
}

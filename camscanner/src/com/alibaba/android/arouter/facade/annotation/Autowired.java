// 
// Decompiled by Procyon v0.6.0
// 

package com.alibaba.android.arouter.facade.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Target;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Retention;

@Retention(RetentionPolicy.CLASS)
@Target({ ElementType.FIELD })
public @interface Autowired {
    String desc() default "";
    
    String name() default "";
    
    boolean required() default false;
}

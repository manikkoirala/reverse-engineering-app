// 
// Decompiled by Procyon v0.6.0
// 

package com.alibaba.android.arouter.facade.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Target;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Retention;

@Retention(RetentionPolicy.CLASS)
@Target({ ElementType.TYPE })
public @interface Route {
    int extras() default Integer.MIN_VALUE;
    
    String group() default "";
    
    String name() default "";
    
    String path();
    
    int priority() default -1;
}

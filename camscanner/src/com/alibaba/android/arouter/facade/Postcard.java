// 
// Decompiled by Procyon v0.6.0
// 

package com.alibaba.android.arouter.facade;

import android.os.BaseBundle;
import android.util.SparseArray;
import java.io.Serializable;
import android.os.Parcelable;
import androidx.annotation.RequiresApi;
import androidx.core.app.ActivityOptionsCompat;
import java.util.ArrayList;
import androidx.annotation.Nullable;
import android.app.Activity;
import com.alibaba.android.arouter.launcher.ARouter;
import com.alibaba.android.arouter.facade.callback.NavigationCallback;
import android.net.Uri;
import com.alibaba.android.arouter.facade.service.SerializationService;
import com.alibaba.android.arouter.facade.template.IProvider;
import android.os.Bundle;
import android.content.Context;
import com.alibaba.android.arouter.facade.model.RouteMeta;

public final class Postcard extends RouteMeta
{
    private String action;
    private Context context;
    private int enterAnim;
    private int exitAnim;
    private int flags;
    private boolean greenChannel;
    private Bundle mBundle;
    private Bundle optionsCompat;
    private IProvider provider;
    private SerializationService serializationService;
    private Object tag;
    private int timeout;
    private Uri uri;
    
    public Postcard() {
        this(null, null);
    }
    
    public Postcard(final String s, final String s2) {
        this(s, s2, null, null);
    }
    
    public Postcard(final String path, final String group, final Uri uri, final Bundle bundle) {
        this.flags = 0;
        this.timeout = 300;
        this.enterAnim = -1;
        this.exitAnim = -1;
        this.setPath(path);
        this.setGroup(group);
        this.setUri(uri);
        Bundle mBundle = bundle;
        if (bundle == null) {
            mBundle = new Bundle();
        }
        this.mBundle = mBundle;
    }
    
    public Postcard addFlags(final int n) {
        this.flags |= n;
        return this;
    }
    
    public String getAction() {
        return this.action;
    }
    
    public Context getContext() {
        return this.context;
    }
    
    public int getEnterAnim() {
        return this.enterAnim;
    }
    
    public int getExitAnim() {
        return this.exitAnim;
    }
    
    public Bundle getExtras() {
        return this.mBundle;
    }
    
    public int getFlags() {
        return this.flags;
    }
    
    public Bundle getOptionsBundle() {
        return this.optionsCompat;
    }
    
    public IProvider getProvider() {
        return this.provider;
    }
    
    public Object getTag() {
        return this.tag;
    }
    
    public int getTimeout() {
        return this.timeout;
    }
    
    public Uri getUri() {
        return this.uri;
    }
    
    public Postcard greenChannel() {
        this.greenChannel = true;
        return this;
    }
    
    public boolean isGreenChannel() {
        return this.greenChannel;
    }
    
    public Object navigation() {
        return this.navigation(null);
    }
    
    public Object navigation(final Context context) {
        return this.navigation(context, null);
    }
    
    public Object navigation(final Context context, final NavigationCallback navigationCallback) {
        return ARouter.\u3007o\u3007().o\u30070(context, this, -1, navigationCallback);
    }
    
    public void navigation(final Activity activity, final int n) {
        this.navigation(activity, n, null);
    }
    
    public void navigation(final Activity activity, final int n, final NavigationCallback navigationCallback) {
        ARouter.\u3007o\u3007().o\u30070((Context)activity, this, n, navigationCallback);
    }
    
    public void setContext(final Context context) {
        this.context = context;
    }
    
    public Postcard setProvider(final IProvider provider) {
        this.provider = provider;
        return this;
    }
    
    public Postcard setTag(final Object tag) {
        this.tag = tag;
        return this;
    }
    
    public Postcard setTimeout(final int timeout) {
        this.timeout = timeout;
        return this;
    }
    
    public Postcard setUri(final Uri uri) {
        this.uri = uri;
        return this;
    }
    
    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append("Postcard{uri=");
        sb.append(this.uri);
        sb.append(", tag=");
        sb.append(this.tag);
        sb.append(", mBundle=");
        sb.append(this.mBundle);
        sb.append(", flags=");
        sb.append(this.flags);
        sb.append(", timeout=");
        sb.append(this.timeout);
        sb.append(", provider=");
        sb.append(this.provider);
        sb.append(", greenChannel=");
        sb.append(this.greenChannel);
        sb.append(", optionsCompat=");
        sb.append(this.optionsCompat);
        sb.append(", enterAnim=");
        sb.append(this.enterAnim);
        sb.append(", exitAnim=");
        sb.append(this.exitAnim);
        sb.append("}\n");
        sb.append(super.toString());
        return sb.toString();
    }
    
    public Postcard with(final Bundle mBundle) {
        if (mBundle != null) {
            this.mBundle = mBundle;
        }
        return this;
    }
    
    public Postcard withAction(final String action) {
        this.action = action;
        return this;
    }
    
    public Postcard withBoolean(@Nullable final String s, final boolean b) {
        this.mBundle.putBoolean(s, b);
        return this;
    }
    
    public Postcard withBundle(@Nullable final String s, @Nullable final Bundle bundle) {
        this.mBundle.putBundle(s, bundle);
        return this;
    }
    
    public Postcard withByte(@Nullable final String s, final byte b) {
        this.mBundle.putByte(s, b);
        return this;
    }
    
    public Postcard withByteArray(@Nullable final String s, @Nullable final byte[] array) {
        this.mBundle.putByteArray(s, array);
        return this;
    }
    
    public Postcard withChar(@Nullable final String s, final char c) {
        this.mBundle.putChar(s, c);
        return this;
    }
    
    public Postcard withCharArray(@Nullable final String s, @Nullable final char[] array) {
        this.mBundle.putCharArray(s, array);
        return this;
    }
    
    public Postcard withCharSequence(@Nullable final String s, @Nullable final CharSequence charSequence) {
        this.mBundle.putCharSequence(s, charSequence);
        return this;
    }
    
    public Postcard withCharSequenceArray(@Nullable final String s, @Nullable final CharSequence[] array) {
        this.mBundle.putCharSequenceArray(s, array);
        return this;
    }
    
    public Postcard withCharSequenceArrayList(@Nullable final String s, @Nullable final ArrayList<CharSequence> list) {
        this.mBundle.putCharSequenceArrayList(s, (ArrayList)list);
        return this;
    }
    
    public Postcard withDouble(@Nullable final String s, final double n) {
        ((BaseBundle)this.mBundle).putDouble(s, n);
        return this;
    }
    
    public Postcard withFlags(final int flags) {
        this.flags = flags;
        return this;
    }
    
    public Postcard withFloat(@Nullable final String s, final float n) {
        this.mBundle.putFloat(s, n);
        return this;
    }
    
    public Postcard withFloatArray(@Nullable final String s, @Nullable final float[] array) {
        this.mBundle.putFloatArray(s, array);
        return this;
    }
    
    public Postcard withInt(@Nullable final String s, final int n) {
        ((BaseBundle)this.mBundle).putInt(s, n);
        return this;
    }
    
    public Postcard withIntegerArrayList(@Nullable final String s, @Nullable final ArrayList<Integer> list) {
        this.mBundle.putIntegerArrayList(s, (ArrayList)list);
        return this;
    }
    
    public Postcard withLong(@Nullable final String s, final long n) {
        ((BaseBundle)this.mBundle).putLong(s, n);
        return this;
    }
    
    public Postcard withObject(@Nullable final String s, @Nullable final Object o) {
        final SerializationService serializationService = ARouter.\u3007o\u3007().\u3007\u3007888((Class<? extends SerializationService>)SerializationService.class);
        this.serializationService = serializationService;
        ((BaseBundle)this.mBundle).putString(s, serializationService.object2Json(o));
        return this;
    }
    
    @RequiresApi(16)
    public Postcard withOptionsCompat(final ActivityOptionsCompat activityOptionsCompat) {
        if (activityOptionsCompat != null) {
            this.optionsCompat = activityOptionsCompat.toBundle();
        }
        return this;
    }
    
    public Postcard withParcelable(@Nullable final String s, @Nullable final Parcelable parcelable) {
        this.mBundle.putParcelable(s, parcelable);
        return this;
    }
    
    public Postcard withParcelableArray(@Nullable final String s, @Nullable final Parcelable[] array) {
        this.mBundle.putParcelableArray(s, array);
        return this;
    }
    
    public Postcard withParcelableArrayList(@Nullable final String s, @Nullable final ArrayList<? extends Parcelable> list) {
        this.mBundle.putParcelableArrayList(s, (ArrayList)list);
        return this;
    }
    
    public Postcard withSerializable(@Nullable final String s, @Nullable final Serializable s2) {
        this.mBundle.putSerializable(s, s2);
        return this;
    }
    
    public Postcard withShort(@Nullable final String s, final short n) {
        this.mBundle.putShort(s, n);
        return this;
    }
    
    public Postcard withShortArray(@Nullable final String s, @Nullable final short[] array) {
        this.mBundle.putShortArray(s, array);
        return this;
    }
    
    public Postcard withSparseParcelableArray(@Nullable final String s, @Nullable final SparseArray<? extends Parcelable> sparseArray) {
        this.mBundle.putSparseParcelableArray(s, (SparseArray)sparseArray);
        return this;
    }
    
    public Postcard withString(@Nullable final String s, @Nullable final String s2) {
        ((BaseBundle)this.mBundle).putString(s, s2);
        return this;
    }
    
    public Postcard withStringArrayList(@Nullable final String s, @Nullable final ArrayList<String> list) {
        this.mBundle.putStringArrayList(s, (ArrayList)list);
        return this;
    }
    
    public Postcard withTransition(final int enterAnim, final int exitAnim) {
        this.enterAnim = enterAnim;
        this.exitAnim = exitAnim;
        return this;
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.alibaba.android.arouter.facade.template;

import java.util.Map;

public interface IInterceptorGroup
{
    void loadInto(final Map<Integer, Class<? extends IInterceptor>> p0);
}

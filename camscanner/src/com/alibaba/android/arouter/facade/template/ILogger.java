// 
// Decompiled by Procyon v0.6.0
// 

package com.alibaba.android.arouter.facade.template;

public interface ILogger
{
    public static final String defaultTag = "ARouter::";
    public static final boolean isShowLog = false;
    public static final boolean isShowStackTrace = false;
    
    void debug(final String p0, final String p1);
    
    void error(final String p0, final String p1);
    
    void error(final String p0, final String p1, final Throwable p2);
    
    String getDefaultTag();
    
    void info(final String p0, final String p1);
    
    boolean isMonitorMode();
    
    void monitor(final String p0);
    
    void showLog(final boolean p0);
    
    void showStackTrace(final boolean p0);
    
    void warning(final String p0, final String p1);
}

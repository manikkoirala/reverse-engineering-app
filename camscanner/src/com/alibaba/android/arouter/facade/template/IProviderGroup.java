// 
// Decompiled by Procyon v0.6.0
// 

package com.alibaba.android.arouter.facade.template;

import com.alibaba.android.arouter.facade.model.RouteMeta;
import java.util.Map;

public interface IProviderGroup
{
    void loadInto(final Map<String, RouteMeta> p0);
}

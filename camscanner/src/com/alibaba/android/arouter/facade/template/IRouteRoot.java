// 
// Decompiled by Procyon v0.6.0
// 

package com.alibaba.android.arouter.facade.template;

import java.util.Map;

public interface IRouteRoot
{
    void loadInto(final Map<String, Class<? extends IRouteGroup>> p0);
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.alibaba.android.arouter.facade.template;

import com.alibaba.android.arouter.facade.callback.InterceptorCallback;
import com.alibaba.android.arouter.facade.Postcard;

public interface IInterceptor extends IProvider
{
    void process(final Postcard p0, final InterceptorCallback p1);
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.alibaba.android.arouter.facade.service;

import java.lang.reflect.Type;
import com.alibaba.android.arouter.facade.template.IProvider;

public interface SerializationService extends IProvider
{
    @Deprecated
     <T> T json2Object(final String p0, final Class<T> p1);
    
    String object2Json(final Object p0);
    
     <T> T parseObject(final String p0, final Type p1);
}

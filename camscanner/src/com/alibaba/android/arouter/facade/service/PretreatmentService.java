// 
// Decompiled by Procyon v0.6.0
// 

package com.alibaba.android.arouter.facade.service;

import com.alibaba.android.arouter.facade.Postcard;
import android.content.Context;
import com.alibaba.android.arouter.facade.template.IProvider;

public interface PretreatmentService extends IProvider
{
    boolean onPretreatment(final Context p0, final Postcard p1);
}

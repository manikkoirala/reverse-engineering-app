// 
// Decompiled by Procyon v0.6.0
// 

package com.alibaba.android.arouter.facade.service;

import android.net.Uri;
import com.alibaba.android.arouter.facade.template.IProvider;

public interface PathReplaceService extends IProvider
{
    String forString(final String p0);
    
    Uri forUri(final Uri p0);
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.alibaba.android.arouter.facade.enums;

public enum TypeKind
{
    private static final TypeKind[] $VALUES;
    
    BOOLEAN, 
    BYTE, 
    CHAR, 
    DOUBLE, 
    FLOAT, 
    INT, 
    LONG, 
    OBJECT, 
    PARCELABLE, 
    SERIALIZABLE, 
    SHORT, 
    STRING;
}

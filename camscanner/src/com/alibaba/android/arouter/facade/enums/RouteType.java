// 
// Decompiled by Procyon v0.6.0
// 

package com.alibaba.android.arouter.facade.enums;

public enum RouteType
{
    private static final RouteType[] $VALUES;
    
    ACTIVITY(0, "android.app.Activity"), 
    BOARDCAST(-1, ""), 
    CONTENT_PROVIDER(-1, "android.app.ContentProvider"), 
    FRAGMENT(-1, "android.app.Fragment"), 
    METHOD(-1, ""), 
    PROVIDER(2, "com.alibaba.android.arouter.facade.template.IProvider"), 
    SERVICE(1, "android.app.Service"), 
    UNKNOWN(-1, "Unknown route type");
    
    String className;
    int id;
    
    private RouteType(final int id, final String className) {
        this.id = id;
        this.className = className;
    }
    
    public static RouteType parse(final String anObject) {
        for (final RouteType routeType : values()) {
            if (routeType.getClassName().equals(anObject)) {
                return routeType;
            }
        }
        return RouteType.UNKNOWN;
    }
    
    public String getClassName() {
        return this.className;
    }
    
    public int getId() {
        return this.id;
    }
    
    public RouteType setClassName(final String className) {
        this.className = className;
        return this;
    }
    
    public RouteType setId(final int id) {
        this.id = id;
        return this;
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.alibaba.android.arouter.facade.callback;

import com.alibaba.android.arouter.facade.Postcard;

public interface NavigationCallback
{
    void onArrival(final Postcard p0);
    
    void onFound(final Postcard p0);
    
    void onInterrupt(final Postcard p0);
    
    void onLost(final Postcard p0);
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.alibaba.android.arouter.facade.callback;

import com.alibaba.android.arouter.facade.Postcard;

public abstract class NavCallback implements NavigationCallback
{
    @Override
    public abstract void onArrival(final Postcard p0);
    
    @Override
    public void onFound(final Postcard postcard) {
    }
    
    @Override
    public void onInterrupt(final Postcard postcard) {
    }
    
    @Override
    public void onLost(final Postcard postcard) {
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.alibaba.android.arouter.facade.callback;

import com.alibaba.android.arouter.facade.Postcard;

public interface InterceptorCallback
{
    void onContinue(final Postcard p0);
    
    void onInterrupt(final Throwable p0);
}

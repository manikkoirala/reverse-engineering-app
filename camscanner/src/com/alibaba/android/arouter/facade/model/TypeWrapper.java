// 
// Decompiled by Procyon v0.6.0
// 

package com.alibaba.android.arouter.facade.model;

import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;

public class TypeWrapper<T>
{
    protected final Type type;
    
    protected TypeWrapper() {
        this.type = ((ParameterizedType)this.getClass().getGenericSuperclass()).getActualTypeArguments()[0];
    }
    
    public Type getType() {
        return this.type;
    }
}

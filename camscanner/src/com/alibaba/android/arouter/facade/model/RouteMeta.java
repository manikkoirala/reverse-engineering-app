// 
// Decompiled by Procyon v0.6.0
// 

package com.alibaba.android.arouter.facade.model;

import com.alibaba.android.arouter.facade.annotation.Route;
import com.alibaba.android.arouter.facade.enums.RouteType;
import javax.lang.model.element.Element;
import com.alibaba.android.arouter.facade.annotation.Autowired;
import java.util.Map;

public class RouteMeta
{
    private Class<?> destination;
    private int extra;
    private String group;
    private Map<String, Autowired> injectConfig;
    private String name;
    private Map<String, Integer> paramsType;
    private String path;
    private int priority;
    private Element rawType;
    private RouteType type;
    
    public RouteMeta() {
        this.priority = -1;
    }
    
    public RouteMeta(final Route route, final Class<?> clazz, final RouteType routeType) {
        this(routeType, null, clazz, route.name(), route.path(), route.group(), null, route.priority(), route.extras());
    }
    
    public RouteMeta(final Route route, final Element element, final RouteType routeType, final Map<String, Integer> map) {
        this(routeType, element, null, route.name(), route.path(), route.group(), map, route.priority(), route.extras());
    }
    
    public RouteMeta(final RouteType type, final Element rawType, final Class<?> destination, final String name, final String path, final String group, final Map<String, Integer> paramsType, final int priority, final int extra) {
        this.type = type;
        this.name = name;
        this.destination = destination;
        this.rawType = rawType;
        this.path = path;
        this.group = group;
        this.paramsType = paramsType;
        this.priority = priority;
        this.extra = extra;
    }
    
    public static RouteMeta build(final RouteType routeType, final Class<?> clazz, final String s, final String s2, final int n, final int n2) {
        return new RouteMeta(routeType, null, clazz, null, s, s2, null, n, n2);
    }
    
    public static RouteMeta build(final RouteType routeType, final Class<?> clazz, final String s, final String s2, final Map<String, Integer> map, final int n, final int n2) {
        return new RouteMeta(routeType, null, clazz, null, s, s2, map, n, n2);
    }
    
    public Class<?> getDestination() {
        return this.destination;
    }
    
    public int getExtra() {
        return this.extra;
    }
    
    public String getGroup() {
        return this.group;
    }
    
    public Map<String, Autowired> getInjectConfig() {
        return this.injectConfig;
    }
    
    public String getName() {
        return this.name;
    }
    
    public Map<String, Integer> getParamsType() {
        return this.paramsType;
    }
    
    public String getPath() {
        return this.path;
    }
    
    public int getPriority() {
        return this.priority;
    }
    
    public Element getRawType() {
        return this.rawType;
    }
    
    public RouteType getType() {
        return this.type;
    }
    
    public RouteMeta setDestination(final Class<?> destination) {
        this.destination = destination;
        return this;
    }
    
    public RouteMeta setExtra(final int extra) {
        this.extra = extra;
        return this;
    }
    
    public RouteMeta setGroup(final String group) {
        this.group = group;
        return this;
    }
    
    public void setInjectConfig(final Map<String, Autowired> injectConfig) {
        this.injectConfig = injectConfig;
    }
    
    public void setName(final String name) {
        this.name = name;
    }
    
    public RouteMeta setParamsType(final Map<String, Integer> paramsType) {
        this.paramsType = paramsType;
        return this;
    }
    
    public RouteMeta setPath(final String path) {
        this.path = path;
        return this;
    }
    
    public RouteMeta setPriority(final int priority) {
        this.priority = priority;
        return this;
    }
    
    public RouteMeta setRawType(final Element rawType) {
        this.rawType = rawType;
        return this;
    }
    
    public RouteMeta setType(final RouteType type) {
        this.type = type;
        return this;
    }
    
    @Override
    public String toString() {
        final RouteType type = this.type;
        final Element rawType = this.rawType;
        final Class<?> destination = this.destination;
        final String path = this.path;
        final String group = this.group;
        final int priority = this.priority;
        final int extra = this.extra;
        final Map<String, Integer> paramsType = this.paramsType;
        final String name = this.name;
        final StringBuilder sb = new StringBuilder();
        sb.append("RouteMeta{type=");
        sb.append(type);
        sb.append(", rawType=");
        sb.append(rawType);
        sb.append(", destination=");
        sb.append(destination);
        sb.append(", path='");
        sb.append(path);
        sb.append("', group='");
        sb.append(group);
        sb.append("', priority=");
        sb.append(priority);
        sb.append(", extra=");
        sb.append(extra);
        sb.append(", paramsType=");
        sb.append(paramsType);
        sb.append(", name='");
        sb.append(name);
        sb.append("'}");
        return sb.toString();
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.alibaba.android.arouter.exception;

public class HandlerException extends RuntimeException
{
    public HandlerException(final String message) {
        super(message);
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.alibaba.android.arouter.exception;

public class NoRouteFoundException extends RuntimeException
{
    public NoRouteFoundException(final String message) {
        super(message);
    }
}

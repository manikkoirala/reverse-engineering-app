// 
// Decompiled by Procyon v0.6.0
// 

package com.alibaba.android.arouter.base;

import java.util.TreeMap;

public class UniqueKeyTreeMap<K, V> extends TreeMap<K, V>
{
    private String tipText;
    
    public UniqueKeyTreeMap(final String tipText) {
        this.tipText = tipText;
    }
    
    @Override
    public V put(final K k, final V value) {
        if (!this.containsKey(k)) {
            return super.put(k, value);
        }
        throw new RuntimeException(String.format(this.tipText, k));
    }
}

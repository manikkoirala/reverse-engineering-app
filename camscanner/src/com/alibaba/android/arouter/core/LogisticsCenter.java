// 
// Decompiled by Procyon v0.6.0
// 

package com.alibaba.android.arouter.core;

import android.os.BaseBundle;
import android.net.Uri;
import com.alibaba.android.arouter.facade.template.IProvider;
import java.util.Map;
import com.alibaba.android.arouter.utils.MapUtils;
import com.alibaba.android.arouter.exception.NoRouteFoundException;
import com.alibaba.android.arouter.facade.model.RouteMeta;
import com.alibaba.android.arouter.facade.enums.TypeKind;
import com.alibaba.android.arouter.utils.TextUtils;
import com.alibaba.android.arouter.facade.Postcard;
import java.lang.reflect.InvocationTargetException;
import com.alibaba.android.arouter.facade.template.IRouteGroup;
import java.util.Iterator;
import com.alibaba.android.arouter.facade.template.ILogger;
import com.alibaba.android.arouter.exception.HandlerException;
import java.util.Locale;
import com.alibaba.android.arouter.facade.template.IProviderGroup;
import com.alibaba.android.arouter.facade.template.IInterceptorGroup;
import com.alibaba.android.arouter.facade.template.IRouteRoot;
import com.alibaba.android.arouter.utils.ClassUtils;
import java.util.Collection;
import java.util.Set;
import java.util.HashSet;
import com.alibaba.android.arouter.utils.PackageUtils;
import com.alibaba.android.arouter.launcher.ARouter;
import java.util.concurrent.ThreadPoolExecutor;
import android.content.Context;

public class LogisticsCenter
{
    private static Context \u3007080;
    static ThreadPoolExecutor \u3007o00\u3007\u3007Oo;
    private static boolean \u3007o\u3007;
    
    public static void O8(final Context \u3007080, String className, final int n, final ThreadPoolExecutor \u3007o00\u3007\u3007Oo) throws HandlerException {
        synchronized (LogisticsCenter.class) {
            LogisticsCenter.\u3007080 = \u3007080;
            LogisticsCenter.\u3007o00\u3007\u3007Oo = \u3007o00\u3007\u3007Oo;
            try {
                long currentTimeMillis = System.currentTimeMillis();
                Oo08();
                if (LogisticsCenter.\u3007o\u3007) {
                    ARouter.\u3007o\u3007.info("ARouter::", "Load router map by arouter-auto-register plugin.");
                }
                else {
                    Set set;
                    if (!ARouter.\u3007o00\u3007\u3007Oo() && !PackageUtils.\u3007080(\u3007080, className, n)) {
                        ARouter.\u3007o\u3007.info("ARouter::", "Load router map from cache.");
                        set = new HashSet(\u3007080.getSharedPreferences("SP_AROUTER_CACHE", 0).getStringSet("ROUTER_MAP", (Set)new HashSet()));
                    }
                    else {
                        ARouter.\u3007o\u3007.info("ARouter::", "Run with debug mode or new install, rebuild router map.");
                        final Set<String> \u3007o00\u3007\u3007Oo2 = ClassUtils.\u3007o00\u3007\u3007Oo(LogisticsCenter.\u3007080, "com.alibaba.android.arouter.routes");
                        if (!\u3007o00\u3007\u3007Oo2.isEmpty()) {
                            \u3007080.getSharedPreferences("SP_AROUTER_CACHE", 0).edit().putStringSet("ROUTER_MAP", (Set)\u3007o00\u3007\u3007Oo2).apply();
                        }
                        PackageUtils.\u3007o00\u3007\u3007Oo(\u3007080);
                        set = \u3007o00\u3007\u3007Oo2;
                    }
                    final ILogger \u3007o\u3007 = ARouter.\u3007o\u3007;
                    final StringBuilder sb = new StringBuilder();
                    sb.append("Find router map finished, map size = ");
                    sb.append(set.size());
                    sb.append(", cost ");
                    sb.append(System.currentTimeMillis() - currentTimeMillis);
                    sb.append(" ms.");
                    \u3007o\u3007.info("ARouter::", sb.toString());
                    final long currentTimeMillis2 = System.currentTimeMillis();
                    final Iterator iterator = set.iterator();
                    while (true) {
                        currentTimeMillis = currentTimeMillis2;
                        if (!iterator.hasNext()) {
                            break;
                        }
                        className = (String)iterator.next();
                        if (className.startsWith("com.alibaba.android.arouter.routes.ARouter$$Root")) {
                            ((IRouteRoot)Class.forName(className).getConstructor((Class<?>[])new Class[0]).newInstance(new Object[0])).loadInto(Warehouse.\u3007080);
                        }
                        else if (className.startsWith("com.alibaba.android.arouter.routes.ARouter$$Interceptors")) {
                            ((IInterceptorGroup)Class.forName(className).getConstructor((Class<?>[])new Class[0]).newInstance(new Object[0])).loadInto(Warehouse.Oo08);
                        }
                        else {
                            if (!className.startsWith("com.alibaba.android.arouter.routes.ARouter$$Providers")) {
                                continue;
                            }
                            ((IProviderGroup)Class.forName(className).getConstructor((Class<?>[])new Class[0]).newInstance(new Object[0])).loadInto(Warehouse.O8);
                        }
                    }
                }
                final ILogger \u3007o\u30072 = ARouter.\u3007o\u3007;
                final StringBuilder sb2 = new StringBuilder();
                sb2.append("Load root element finished, cost ");
                sb2.append(System.currentTimeMillis() - currentTimeMillis);
                sb2.append(" ms.");
                \u3007o\u30072.info("ARouter::", sb2.toString());
                if (Warehouse.\u3007080.size() == 0) {
                    ARouter.\u3007o\u3007.error("ARouter::", "No mapping files were found, check your configuration please!");
                }
                if (ARouter.\u3007o00\u3007\u3007Oo()) {
                    ARouter.\u3007o\u3007.debug("ARouter::", String.format(Locale.getDefault(), "LogisticsCenter has already been loaded, GroupIndex[%d], InterceptorIndex[%d], ProviderIndex[%d]", Warehouse.\u3007080.size(), Warehouse.Oo08.size(), Warehouse.O8.size()));
                }
            }
            catch (final Exception ex) {
                final StringBuilder sb3 = new StringBuilder();
                sb3.append("ARouter::ARouter init logistics center exception! [");
                sb3.append(ex.getMessage());
                sb3.append("]");
                throw new HandlerException(sb3.toString());
            }
        }
    }
    
    private static void OO0o\u3007\u3007\u3007\u30070(final IRouteRoot routeRoot) {
        o\u30070();
        if (routeRoot != null) {
            routeRoot.loadInto(Warehouse.\u3007080);
        }
    }
    
    private static void Oo08() {
        LogisticsCenter.\u3007o\u3007 = false;
        \u3007\u3007888("com.alibaba.android.arouter.routes.ARouter$$Root$$arouterapi");
        \u3007\u3007888("com.alibaba.android.arouter.routes.ARouter$$Root$$account");
        \u3007\u3007888("com.alibaba.android.arouter.routes.ARouter$$Root$$branch");
        \u3007\u3007888("com.alibaba.android.arouter.routes.ARouter$$Root$$camScanner_GP_402");
        \u3007\u3007888("com.alibaba.android.arouter.routes.ARouter$$Interceptors$$camScanner_GP_402");
        \u3007\u3007888("com.alibaba.android.arouter.routes.ARouter$$Providers$$arouterapi");
        \u3007\u3007888("com.alibaba.android.arouter.routes.ARouter$$Providers$$account");
        \u3007\u3007888("com.alibaba.android.arouter.routes.ARouter$$Providers$$branch");
        \u3007\u3007888("com.alibaba.android.arouter.routes.ARouter$$Providers$$camScanner_GP_402");
    }
    
    private static void oO80(final IInterceptorGroup interceptorGroup) {
        o\u30070();
        if (interceptorGroup != null) {
            interceptorGroup.loadInto(Warehouse.Oo08);
        }
    }
    
    private static void o\u30070() {
        if (!LogisticsCenter.\u3007o\u3007) {
            LogisticsCenter.\u3007o\u3007 = true;
        }
    }
    
    public static void \u3007080(final String s, final IRouteGroup routeGroup) throws NoSuchMethodException, IllegalAccessException, InvocationTargetException, InstantiationException {
        synchronized (LogisticsCenter.class) {
            if (Warehouse.\u3007080.containsKey(s)) {
                Warehouse.\u3007080.get(s).getConstructor((Class<?>[])new Class[0]).newInstance(new Object[0]).loadInto(Warehouse.\u3007o00\u3007\u3007Oo);
                Warehouse.\u3007080.remove(s);
            }
            if (routeGroup != null) {
                routeGroup.loadInto(Warehouse.\u3007o00\u3007\u3007Oo);
            }
        }
    }
    
    private static void \u300780\u3007808\u3007O(final IProviderGroup providerGroup) {
        o\u30070();
        if (providerGroup != null) {
            providerGroup.loadInto(Warehouse.O8);
        }
    }
    
    private static void \u30078o8o\u3007(final Postcard postcard, final Integer n, final String s, final String s2) {
        if (!TextUtils.\u3007o00\u3007\u3007Oo(s)) {
            if (!TextUtils.\u3007o00\u3007\u3007Oo(s2)) {
                Label_0275: {
                    if (n == null) {
                        break Label_0275;
                    }
                    try {
                        if (n == TypeKind.BOOLEAN.ordinal()) {
                            postcard.withBoolean(s, Boolean.parseBoolean(s2));
                            return;
                        }
                        if (n == TypeKind.BYTE.ordinal()) {
                            postcard.withByte(s, Byte.parseByte(s2));
                            return;
                        }
                        if (n == TypeKind.SHORT.ordinal()) {
                            postcard.withShort(s, Short.parseShort(s2));
                            return;
                        }
                        if (n == TypeKind.INT.ordinal()) {
                            postcard.withInt(s, Integer.parseInt(s2));
                            return;
                        }
                        if (n == TypeKind.LONG.ordinal()) {
                            postcard.withLong(s, Long.parseLong(s2));
                            return;
                        }
                        if (n == TypeKind.FLOAT.ordinal()) {
                            postcard.withFloat(s, Float.parseFloat(s2));
                            return;
                        }
                        if (n == TypeKind.DOUBLE.ordinal()) {
                            postcard.withDouble(s, Double.parseDouble(s2));
                            return;
                        }
                        if (n == TypeKind.STRING.ordinal()) {
                            postcard.withString(s, s2);
                            return;
                        }
                        if (n == TypeKind.PARCELABLE.ordinal()) {
                            return;
                        }
                        if (n == TypeKind.OBJECT.ordinal()) {
                            postcard.withString(s, s2);
                            return;
                        }
                        postcard.withString(s, s2);
                        return;
                        postcard.withString(s, s2);
                    }
                    finally {
                        final ILogger \u3007o\u3007 = ARouter.\u3007o\u3007;
                        final StringBuilder sb = new StringBuilder();
                        sb.append("LogisticsCenter setValue failed! ");
                        final Throwable t;
                        sb.append(t.getMessage());
                        \u3007o\u3007.warning("ARouter::", sb.toString());
                    }
                }
            }
        }
    }
    
    public static Postcard \u3007o00\u3007\u3007Oo(final String s) {
        final RouteMeta routeMeta = Warehouse.O8.get(s);
        if (routeMeta == null) {
            return null;
        }
        return new Postcard(routeMeta.getPath(), routeMeta.getGroup());
    }
    
    public static void \u3007o\u3007(final Postcard postcard) {
        monitorenter(LogisticsCenter.class);
        if (postcard != null) {
            Label_0625: {
                try {
                    final RouteMeta routeMeta = Warehouse.\u3007o00\u3007\u3007Oo.get(postcard.getPath());
                    Label_0604: {
                        if (routeMeta == null) {
                            if (Warehouse.\u3007080.containsKey(postcard.getGroup())) {
                                try {
                                    if (ARouter.\u3007o00\u3007\u3007Oo()) {
                                        ARouter.\u3007o\u3007.debug("ARouter::", String.format(Locale.getDefault(), "The group [%s] starts loading, trigger by [%s]", postcard.getGroup(), postcard.getPath()));
                                    }
                                    \u3007080(postcard.getGroup(), null);
                                    if (ARouter.\u3007o00\u3007\u3007Oo()) {
                                        ARouter.\u3007o\u3007.debug("ARouter::", String.format(Locale.getDefault(), "The group [%s] has already been loaded, trigger by [%s]", postcard.getGroup(), postcard.getPath()));
                                    }
                                    \u3007o\u3007(postcard);
                                    break Label_0604;
                                }
                                catch (final Exception ex) {
                                    final StringBuilder sb = new StringBuilder();
                                    sb.append("ARouter::Fatal exception when loading group meta. [");
                                    sb.append(ex.getMessage());
                                    sb.append("]");
                                    throw new HandlerException(sb.toString());
                                }
                            }
                            final StringBuilder sb2 = new StringBuilder();
                            sb2.append("ARouter::There is no route match the path [");
                            sb2.append(postcard.getPath());
                            sb2.append("], in group [");
                            sb2.append(postcard.getGroup());
                            sb2.append("]");
                            throw new NoRouteFoundException(sb2.toString());
                        }
                        postcard.setDestination(routeMeta.getDestination());
                        postcard.setType(routeMeta.getType());
                        postcard.setPriority(routeMeta.getPriority());
                        postcard.setExtra(routeMeta.getExtra());
                        final Uri uri = postcard.getUri();
                        if (uri != null) {
                            final Map<String, String> \u3007o\u3007 = TextUtils.\u3007o\u3007(uri);
                            final Map<String, Integer> paramsType = routeMeta.getParamsType();
                            if (MapUtils.\u3007o00\u3007\u3007Oo(paramsType)) {
                                for (final Map.Entry<K, Integer> entry : paramsType.entrySet()) {
                                    \u30078o8o\u3007(postcard, entry.getValue(), (String)entry.getKey(), \u3007o\u3007.get(entry.getKey()));
                                }
                                ((BaseBundle)postcard.getExtras()).putStringArray("wmHzgD4lOj5o4241", (String[])paramsType.keySet().toArray(new String[0]));
                            }
                            postcard.withString("NTeRQWvye18AkPd6G", uri.toString());
                        }
                        final int n = LogisticsCenter$1.\u3007080[routeMeta.getType().ordinal()];
                        if (n != 1) {
                            if (n == 2) {
                                postcard.greenChannel();
                            }
                        }
                        else {
                            final Class<?> destination = routeMeta.getDestination();
                            IProvider provider;
                            if ((provider = Warehouse.\u3007o\u3007.get(destination)) == null) {
                                try {
                                    provider = (IProvider)destination.getConstructor((Class<?>[])new Class[0]).newInstance(new Object[0]);
                                    provider.init(LogisticsCenter.\u3007080);
                                    Warehouse.\u3007o\u3007.put(destination, provider);
                                }
                                catch (final Exception ex2) {
                                    ARouter.\u3007o\u3007.error("ARouter::", "Init provider failed!", ex2);
                                    throw new HandlerException("Init provider failed!");
                                }
                            }
                            postcard.setProvider(provider);
                            postcard.greenChannel();
                        }
                    }
                    monitorexit(LogisticsCenter.class);
                    return;
                }
                finally {
                    break Label_0625;
                }
                throw new NoRouteFoundException("ARouter::No postcard!");
            }
            monitorexit(LogisticsCenter.class);
        }
        throw new NoRouteFoundException("ARouter::No postcard!");
    }
    
    private static void \u3007\u3007888(final String str) {
        if (!TextUtils.\u3007o00\u3007\u3007Oo(str)) {
            try {
                final Object instance = Class.forName(str).getConstructor((Class<?>[])new Class[0]).newInstance(new Object[0]);
                if (instance instanceof IRouteRoot) {
                    OO0o\u3007\u3007\u3007\u30070((IRouteRoot)instance);
                }
                else if (instance instanceof IProviderGroup) {
                    \u300780\u3007808\u3007O((IProviderGroup)instance);
                }
                else if (instance instanceof IInterceptorGroup) {
                    oO80((IInterceptorGroup)instance);
                }
                else {
                    final ILogger \u3007o\u3007 = ARouter.\u3007o\u3007;
                    final StringBuilder sb = new StringBuilder();
                    sb.append("register failed, class name: ");
                    sb.append(str);
                    sb.append(" should implements one of IRouteRoot/IProviderGroup/IInterceptorGroup.");
                    \u3007o\u3007.info("ARouter::", sb.toString());
                }
            }
            catch (final Exception ex) {
                final ILogger \u3007o\u30072 = ARouter.\u3007o\u3007;
                final StringBuilder sb2 = new StringBuilder();
                sb2.append("register class error:");
                sb2.append(str);
                \u3007o\u30072.error("ARouter::", sb2.toString(), ex);
            }
        }
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.alibaba.android.arouter.core;

import java.util.ArrayList;
import android.content.Context;
import com.alibaba.android.arouter.facade.template.ISyringe;
import android.util.LruCache;
import java.util.List;
import com.alibaba.android.arouter.facade.annotation.Route;
import com.alibaba.android.arouter.facade.service.AutowiredService;

@Route(path = "/arouter/service/autowired")
public class AutowiredServiceImpl implements AutowiredService
{
    private List<String> blackList;
    private LruCache<String, ISyringe> classCache;
    
    private void doInject(final Object o, final Class<?> clazz) {
        Class<?> class1 = clazz;
        if (clazz == null) {
            class1 = o.getClass();
        }
        final ISyringe syringe = this.getSyringe(class1);
        if (syringe != null) {
            syringe.inject(o);
        }
        final Class<?> superclass = class1.getSuperclass();
        if (superclass != null && !superclass.getName().startsWith("android")) {
            this.doInject(o, superclass);
        }
    }
    
    private ISyringe getSyringe(final Class<?> clazz) {
        final String name = clazz.getName();
        try {
            if (!this.blackList.contains(name)) {
                ISyringe syringe;
                if ((syringe = (ISyringe)this.classCache.get((Object)name)) == null) {
                    final StringBuilder sb = new StringBuilder();
                    sb.append(clazz.getName());
                    sb.append("$$ARouter$$Autowired");
                    syringe = (ISyringe)Class.forName(sb.toString()).getConstructor((Class<?>[])new Class[0]).newInstance(new Object[0]);
                }
                this.classCache.put((Object)name, (Object)syringe);
                return syringe;
            }
        }
        catch (final Exception ex) {
            this.blackList.add(name);
        }
        return null;
    }
    
    @Override
    public void autowire(final Object o) {
        this.doInject(o, null);
    }
    
    @Override
    public void init(final Context context) {
        this.classCache = (LruCache<String, ISyringe>)new LruCache(50);
        this.blackList = new ArrayList<String>();
    }
}

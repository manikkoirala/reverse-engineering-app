// 
// Decompiled by Procyon v0.6.0
// 

package com.alibaba.android.arouter.core;

import java.util.ArrayList;
import com.alibaba.android.arouter.base.UniqueKeyTreeMap;
import java.util.HashMap;
import com.alibaba.android.arouter.facade.template.IProvider;
import com.alibaba.android.arouter.facade.template.IRouteGroup;
import java.util.List;
import com.alibaba.android.arouter.facade.template.IInterceptor;
import com.alibaba.android.arouter.facade.model.RouteMeta;
import java.util.Map;

class Warehouse
{
    static Map<String, RouteMeta> O8;
    static Map<Integer, Class<? extends IInterceptor>> Oo08;
    static List<IInterceptor> o\u30070;
    static Map<String, Class<? extends IRouteGroup>> \u3007080;
    static Map<String, RouteMeta> \u3007o00\u3007\u3007Oo;
    static Map<Class, IProvider> \u3007o\u3007;
    
    static {
        Warehouse.\u3007080 = new HashMap<String, Class<? extends IRouteGroup>>();
        Warehouse.\u3007o00\u3007\u3007Oo = new HashMap<String, RouteMeta>();
        Warehouse.\u3007o\u3007 = new HashMap<Class, IProvider>();
        Warehouse.O8 = new HashMap<String, RouteMeta>();
        Warehouse.Oo08 = new UniqueKeyTreeMap<Integer, Class<? extends IInterceptor>>("More than one interceptors use same priority [%s]");
        Warehouse.o\u30070 = new ArrayList<IInterceptor>();
    }
}

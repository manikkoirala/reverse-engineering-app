// 
// Decompiled by Procyon v0.6.0
// 

package com.alibaba.android.arouter.core;

import java.util.Iterator;
import com.alibaba.android.arouter.launcher.ARouter;
import android.content.Context;
import java.util.concurrent.TimeUnit;
import java.util.Map;
import com.alibaba.android.arouter.utils.MapUtils;
import com.alibaba.android.arouter.exception.HandlerException;
import com.alibaba.android.arouter.facade.callback.InterceptorCallback;
import com.alibaba.android.arouter.facade.template.IInterceptor;
import com.alibaba.android.arouter.facade.Postcard;
import com.alibaba.android.arouter.thread.CancelableCountDownLatch;
import com.alibaba.android.arouter.facade.annotation.Route;
import com.alibaba.android.arouter.facade.service.InterceptorService;

@Route(path = "/arouter/service/interceptor")
public class InterceptorServiceImpl implements InterceptorService
{
    private static boolean interceptorHasInit;
    private static final Object interceptorInitLock;
    
    static {
        interceptorInitLock = new Object();
    }
    
    private static void _execute(final int n, final CancelableCountDownLatch cancelableCountDownLatch, final Postcard postcard) {
        if (n < Warehouse.o\u30070.size()) {
            Warehouse.o\u30070.get(n).process(postcard, new InterceptorCallback(cancelableCountDownLatch, n, postcard) {
                final CancelableCountDownLatch \u3007080;
                final int \u3007o00\u3007\u3007Oo;
                final Postcard \u3007o\u3007;
                
                @Override
                public void onContinue(final Postcard postcard) {
                    this.\u3007080.countDown();
                    _execute(this.\u3007o00\u3007\u3007Oo + 1, this.\u3007080, postcard);
                }
                
                @Override
                public void onInterrupt(final Throwable t) {
                    final Postcard \u3007o\u3007 = this.\u3007o\u3007;
                    Throwable tag = t;
                    if (t == null) {
                        tag = new HandlerException("No message.");
                    }
                    \u3007o\u3007.setTag(tag);
                    this.\u3007080.\u3007080();
                }
            });
        }
    }
    
    private static void checkInterceptorsInitStatus() {
        synchronized (InterceptorServiceImpl.interceptorInitLock) {
            while (!InterceptorServiceImpl.interceptorHasInit) {
                try {
                    InterceptorServiceImpl.interceptorInitLock.wait(10000L);
                    continue;
                }
                catch (final InterruptedException ex) {
                    final StringBuilder sb = new StringBuilder();
                    sb.append("ARouter::Interceptor init cost too much time error! reason = [");
                    sb.append(ex.getMessage());
                    sb.append("]");
                    throw new HandlerException(sb.toString());
                }
                break;
            }
        }
    }
    
    @Override
    public void doInterceptions(final Postcard postcard, final InterceptorCallback interceptorCallback) {
        if (MapUtils.\u3007o00\u3007\u3007Oo(Warehouse.Oo08)) {
            checkInterceptorsInitStatus();
            if (!InterceptorServiceImpl.interceptorHasInit) {
                interceptorCallback.onInterrupt(new HandlerException("Interceptors initialization takes too much time."));
                return;
            }
            LogisticsCenter.\u3007o00\u3007\u3007Oo.execute(new Runnable(this, postcard, interceptorCallback) {
                final InterceptorServiceImpl OO;
                final Postcard o0;
                final InterceptorCallback \u3007OOo8\u30070;
                
                @Override
                public void run() {
                    final CancelableCountDownLatch cancelableCountDownLatch = new CancelableCountDownLatch(Warehouse.o\u30070.size());
                    try {
                        _execute(0, cancelableCountDownLatch, this.o0);
                        cancelableCountDownLatch.await(this.o0.getTimeout(), TimeUnit.SECONDS);
                        if (cancelableCountDownLatch.getCount() > 0L) {
                            this.\u3007OOo8\u30070.onInterrupt(new HandlerException("The interceptor processing timed out."));
                        }
                        else if (this.o0.getTag() != null) {
                            this.\u3007OOo8\u30070.onInterrupt((Throwable)this.o0.getTag());
                        }
                        else {
                            this.\u3007OOo8\u30070.onContinue(this.o0);
                        }
                    }
                    catch (final Exception ex) {
                        this.\u3007OOo8\u30070.onInterrupt(ex);
                    }
                }
            });
        }
        else {
            interceptorCallback.onContinue(postcard);
        }
    }
    
    @Override
    public void init(final Context context) {
        LogisticsCenter.\u3007o00\u3007\u3007Oo.execute(new Runnable(this, context) {
            final Context o0;
            final InterceptorServiceImpl \u3007OOo8\u30070;
            
            @Override
            public void run() {
                if (MapUtils.\u3007o00\u3007\u3007Oo(Warehouse.Oo08)) {
                    final Iterator<Map.Entry<Integer, Class<? extends IInterceptor>>> iterator = Warehouse.Oo08.entrySet().iterator();
                    while (iterator.hasNext()) {
                        final Class clazz = ((Map.Entry<K, Class>)iterator.next()).getValue();
                        try {
                            final IInterceptor interceptor = (IInterceptor)clazz.getConstructor((Class[])new Class[0]).newInstance(new Object[0]);
                            interceptor.init(this.o0);
                            Warehouse.o\u30070.add(interceptor);
                            continue;
                        }
                        catch (final Exception ex) {
                            final StringBuilder sb = new StringBuilder();
                            sb.append("ARouter::ARouter init interceptor error! name = [");
                            sb.append(clazz.getName());
                            sb.append("], reason = [");
                            sb.append(ex.getMessage());
                            sb.append("]");
                            throw new HandlerException(sb.toString());
                        }
                        break;
                    }
                    InterceptorServiceImpl.\u3007o00\u3007\u3007Oo(true);
                    ARouter.\u3007o\u3007.info("ARouter::", "ARouter interceptors init over.");
                    synchronized (InterceptorServiceImpl.interceptorInitLock) {
                        InterceptorServiceImpl.interceptorInitLock.notifyAll();
                    }
                }
            }
        });
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.alibaba.android.arouter.thread;

import com.alibaba.android.arouter.facade.template.ILogger;
import com.alibaba.android.arouter.utils.TextUtils;
import java.util.concurrent.CancellationException;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.concurrent.ArrayBlockingQueue;
import com.alibaba.android.arouter.launcher.ARouter;
import java.util.concurrent.RejectedExecutionHandler;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.ThreadPoolExecutor;

public class DefaultPoolExecutor extends ThreadPoolExecutor
{
    private static final int OO;
    private static final int o0;
    private static volatile DefaultPoolExecutor \u300708O\u300700\u3007o;
    private static final int \u3007OOo8\u30070;
    
    static {
        int oo = o0 = Runtime.getRuntime().availableProcessors();
        \u3007OOo8\u30070 = ++oo;
        OO = oo;
    }
    
    private DefaultPoolExecutor(final int corePoolSize, final int maximumPoolSize, final long keepAliveTime, final TimeUnit unit, final BlockingQueue<Runnable> workQueue, final ThreadFactory threadFactory) {
        super(corePoolSize, maximumPoolSize, keepAliveTime, unit, workQueue, threadFactory, new RejectedExecutionHandler() {
            @Override
            public void rejectedExecution(final Runnable runnable, final ThreadPoolExecutor threadPoolExecutor) {
                ARouter.\u3007o\u3007.error("ARouter::", "Task rejected, too many task!");
            }
        });
    }
    
    public static DefaultPoolExecutor \u3007080() {
        if (DefaultPoolExecutor.\u300708O\u300700\u3007o == null) {
            synchronized (DefaultPoolExecutor.class) {
                if (DefaultPoolExecutor.\u300708O\u300700\u3007o == null) {
                    DefaultPoolExecutor.\u300708O\u300700\u3007o = new DefaultPoolExecutor(DefaultPoolExecutor.\u3007OOo8\u30070, DefaultPoolExecutor.OO, 30L, TimeUnit.SECONDS, new ArrayBlockingQueue<Runnable>(64), new DefaultThreadFactory());
                }
            }
        }
        return DefaultPoolExecutor.\u300708O\u300700\u3007o;
    }
    
    @Override
    protected void afterExecute(final Runnable r, final Throwable t) {
        super.afterExecute(r, t);
        Throwable cause = t;
        if (t == null) {
            cause = t;
            if (r instanceof Future) {
                try {
                    ((Future)r).get();
                    cause = t;
                }
                catch (final InterruptedException ex) {
                    Thread.currentThread().interrupt();
                    cause = t;
                }
                catch (final ExecutionException ex2) {
                    cause = ex2.getCause();
                }
                catch (final CancellationException ex3) {}
            }
        }
        if (cause != null) {
            final ILogger \u3007o\u3007 = ARouter.\u3007o\u3007;
            final StringBuilder sb = new StringBuilder();
            sb.append("Running task appeared exception! Thread [");
            sb.append(Thread.currentThread().getName());
            sb.append("], because [");
            sb.append(cause.getMessage());
            sb.append("]\n");
            sb.append(TextUtils.\u3007080(cause.getStackTrace()));
            \u3007o\u3007.warning("ARouter::", sb.toString());
        }
    }
}

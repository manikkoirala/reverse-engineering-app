// 
// Decompiled by Procyon v0.6.0
// 

package com.alibaba.android.arouter.thread;

import com.alibaba.android.arouter.facade.template.ILogger;
import com.alibaba.android.arouter.launcher.ARouter;
import androidx.annotation.NonNull;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.ThreadFactory;

public class DefaultThreadFactory implements ThreadFactory
{
    private static final AtomicInteger O8;
    private final AtomicInteger \u3007080;
    private final ThreadGroup \u3007o00\u3007\u3007Oo;
    private final String \u3007o\u3007;
    
    static {
        O8 = new AtomicInteger(1);
    }
    
    public DefaultThreadFactory() {
        this.\u3007080 = new AtomicInteger(1);
        final SecurityManager securityManager = System.getSecurityManager();
        ThreadGroup \u3007o00\u3007\u3007Oo;
        if (securityManager != null) {
            \u3007o00\u3007\u3007Oo = securityManager.getThreadGroup();
        }
        else {
            \u3007o00\u3007\u3007Oo = Thread.currentThread().getThreadGroup();
        }
        this.\u3007o00\u3007\u3007Oo = \u3007o00\u3007\u3007Oo;
        final StringBuilder sb = new StringBuilder();
        sb.append("ARouter task pool No.");
        sb.append(DefaultThreadFactory.O8.getAndIncrement());
        sb.append(", thread No.");
        this.\u3007o\u3007 = sb.toString();
    }
    
    @Override
    public Thread newThread(@NonNull final Runnable target) {
        final StringBuilder sb = new StringBuilder();
        sb.append(this.\u3007o\u3007);
        sb.append(this.\u3007080.getAndIncrement());
        final String string = sb.toString();
        final ILogger \u3007o\u3007 = ARouter.\u3007o\u3007;
        final StringBuilder sb2 = new StringBuilder();
        sb2.append("Thread production, name is [");
        sb2.append(string);
        sb2.append("]");
        \u3007o\u3007.info("ARouter::", sb2.toString());
        final Thread thread = new Thread(this.\u3007o00\u3007\u3007Oo, target, string, 0L);
        if (thread.isDaemon()) {
            thread.setDaemon(false);
        }
        if (thread.getPriority() != 5) {
            thread.setPriority(5);
        }
        thread.setUncaughtExceptionHandler((Thread.UncaughtExceptionHandler)new Thread.UncaughtExceptionHandler(this) {
            final DefaultThreadFactory \u3007080;
            
            @Override
            public void uncaughtException(final Thread thread, final Throwable t) {
                final ILogger \u3007o\u3007 = ARouter.\u3007o\u3007;
                final StringBuilder sb = new StringBuilder();
                sb.append("Running task appeared exception! Thread [");
                sb.append(thread.getName());
                sb.append("], because [");
                sb.append(t.getMessage());
                sb.append("]");
                \u3007o\u3007.info("ARouter::", sb.toString());
            }
        });
        return thread;
    }
}

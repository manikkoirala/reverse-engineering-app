// 
// Decompiled by Procyon v0.6.0
// 

package com.alibaba.android.arouter.thread;

import java.util.concurrent.CountDownLatch;

public class CancelableCountDownLatch extends CountDownLatch
{
    public CancelableCountDownLatch(final int count) {
        super(count);
    }
    
    public void \u3007080() {
        while (this.getCount() > 0L) {
            this.countDown();
        }
    }
}

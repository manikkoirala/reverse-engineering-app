// 
// Decompiled by Procyon v0.6.0
// 

package com.alibaba.android.arouter.routes;

import com.intsig.camscanner.tsapp.RouterReferralFunction;
import com.intsig.camscanner.paper.CamExamGuidePopUpDialog;
import com.alibaba.android.arouter.facade.enums.RouteType;
import com.alibaba.android.arouter.facade.model.RouteMeta;
import java.util.Map;
import com.alibaba.android.arouter.facade.template.IRouteGroup;

public class ARouter$$Group$$referral implements IRouteGroup
{
    @Override
    public void loadInto(final Map<String, RouteMeta> map) {
        map.put("/referral/cam_exam", RouteMeta.build(RouteType.FRAGMENT, CamExamGuidePopUpDialog.class, "/referral/cam_exam", "referral", null, -1, Integer.MIN_VALUE));
        map.put("/referral/third", RouteMeta.build(RouteType.PROVIDER, RouterReferralFunction.class, "/referral/third", "referral", null, -1, Integer.MIN_VALUE));
    }
}

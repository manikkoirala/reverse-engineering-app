// 
// Decompiled by Procyon v0.6.0
// 

package com.alibaba.android.arouter.routes;

import com.intsig.tsapp.account.VerifyCodeNoneReceiveActivity;
import com.intsig.tsapp.account.LoginMainActivity;
import com.intsig.tsapp.account.CloudServiceAuthActivity;
import com.alibaba.android.arouter.facade.enums.RouteType;
import com.alibaba.android.arouter.facade.model.RouteMeta;
import java.util.Map;
import com.alibaba.android.arouter.facade.template.IRouteGroup;

public class ARouter$$Group$$account implements IRouteGroup
{
    @Override
    public void loadInto(final Map<String, RouteMeta> map) {
        final RouteType activity = RouteType.ACTIVITY;
        map.put("/account/cloud_service_auth", RouteMeta.build(activity, CloudServiceAuthActivity.class, "/account/cloud_service_auth", "account", null, -1, Integer.MIN_VALUE));
        map.put("/account/login_main", RouteMeta.build(activity, LoginMainActivity.class, "/account/login_main", "account", null, -1, Integer.MIN_VALUE));
        map.put("/account/none_receive_verify_code", RouteMeta.build(activity, VerifyCodeNoneReceiveActivity.class, "/account/none_receive_verify_code", "account", null, -1, Integer.MIN_VALUE));
    }
}

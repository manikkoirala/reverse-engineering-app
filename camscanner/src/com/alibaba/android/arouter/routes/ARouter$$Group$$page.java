// 
// Decompiled by Procyon v0.6.0
// 

package com.alibaba.android.arouter.routes;

import com.intsig.camscanner.router.TopicServiceImpl;
import com.intsig.camscanner.router.ToWordServiceImpl;
import com.intsig.camscanner.router.ToOcrServiceImpl;
import com.intsig.camscanner.DocumentActivity;
import com.alibaba.android.arouter.facade.enums.RouteType;
import com.alibaba.android.arouter.facade.model.RouteMeta;
import java.util.Map;
import com.alibaba.android.arouter.facade.template.IRouteGroup;

public class ARouter$$Group$$page implements IRouteGroup
{
    @Override
    public void loadInto(final Map<String, RouteMeta> map) {
        map.put("/page/list", RouteMeta.build(RouteType.ACTIVITY, DocumentActivity.class, "/page/list", "page", null, -1, Integer.MIN_VALUE));
        final RouteType provider = RouteType.PROVIDER;
        map.put("/page/to_ocr", RouteMeta.build(provider, ToOcrServiceImpl.class, "/page/to_ocr", "page", null, -1, Integer.MIN_VALUE));
        map.put("/page/to_word", RouteMeta.build(provider, ToWordServiceImpl.class, "/page/to_word", "page", null, -1, Integer.MIN_VALUE));
        map.put("/page/topic", RouteMeta.build(provider, TopicServiceImpl.class, "/page/topic", "page", null, -1, Integer.MIN_VALUE));
    }
}

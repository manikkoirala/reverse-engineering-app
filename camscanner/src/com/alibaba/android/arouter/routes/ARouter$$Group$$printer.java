// 
// Decompiled by Procyon v0.6.0
// 

package com.alibaba.android.arouter.routes;

import com.intsig.camscanner.printer.PrintHomeActivity;
import com.alibaba.android.arouter.facade.enums.RouteType;
import com.alibaba.android.arouter.facade.model.RouteMeta;
import java.util.Map;
import com.alibaba.android.arouter.facade.template.IRouteGroup;

public class ARouter$$Group$$printer implements IRouteGroup
{
    @Override
    public void loadInto(final Map<String, RouteMeta> map) {
        map.put("/printer/home", RouteMeta.build(RouteType.ACTIVITY, PrintHomeActivity.class, "/printer/home", "printer", null, -1, Integer.MIN_VALUE));
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.alibaba.android.arouter.routes;

import java.util.HashMap;
import com.intsig.camscanner.CaptureIntentActivity;
import com.alibaba.android.arouter.facade.enums.RouteType;
import com.alibaba.android.arouter.facade.model.RouteMeta;
import java.util.Map;
import com.alibaba.android.arouter.facade.template.IRouteGroup;

public class ARouter$$Group$$capture implements IRouteGroup
{
    @Override
    public void loadInto(final Map<String, RouteMeta> map) {
        map.put("/capture/camera", RouteMeta.build(RouteType.ACTIVITY, CaptureIntentActivity.class, "/capture/camera", "capture", new HashMap<String, Integer>(this) {
            final ARouter$$Group$$capture o0;
            
            {
                final Integer value = 8;
                this.put("edu_lottery", value);
                final Integer value2 = 0;
                this.put("key_is_retake", value2);
                this.put("capture_function_entrance", value);
                this.put("capture_dir_sync_id", value);
                this.put("mode", value);
                this.put("capture_only_one_mode", value2);
                this.put("capture_is_show_guide", value2);
                this.put("capture_scene_json", value);
                this.put("camera_ad_from_part", value);
                this.put("certificate_type", value);
                this.put("capture_doc_title", value);
                this.put("cs_internal", value2);
                this.put("capture_scene_auto_archive_id", value);
            }
        }, -1, Integer.MIN_VALUE));
    }
}

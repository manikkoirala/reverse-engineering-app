// 
// Decompiled by Procyon v0.6.0
// 

package com.alibaba.android.arouter.routes;

import com.intsig.camscanner.purchase.scanfirstdoc.ScanFirstDocPremiumActivity;
import com.intsig.camscanner.FaxChargeActivity;
import com.alibaba.android.arouter.facade.enums.RouteType;
import com.alibaba.android.arouter.facade.model.RouteMeta;
import java.util.Map;
import com.alibaba.android.arouter.facade.template.IRouteGroup;

public class ARouter$$Group$$premium implements IRouteGroup
{
    @Override
    public void loadInto(final Map<String, RouteMeta> map) {
        final RouteType activity = RouteType.ACTIVITY;
        map.put("/premium/fax_charge", RouteMeta.build(activity, FaxChargeActivity.class, "/premium/fax_charge", "premium", null, -1, Integer.MIN_VALUE));
        map.put("/premium/scan_first_doc", RouteMeta.build(activity, ScanFirstDocPremiumActivity.class, "/premium/scan_first_doc", "premium", null, -1, Integer.MIN_VALUE));
    }
}

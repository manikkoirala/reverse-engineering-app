// 
// Decompiled by Procyon v0.6.0
// 

package com.alibaba.android.arouter.routes;

import com.alibaba.android.arouter.facade.template.IRouteGroup;
import java.util.Map;
import com.alibaba.android.arouter.facade.template.IRouteRoot;

public class ARouter$$Root$$branch implements IRouteRoot
{
    @Override
    public void loadInto(final Map<String, Class<? extends IRouteGroup>> map) {
        map.put("branch", ARouter$$Group$$branch.class);
    }
}

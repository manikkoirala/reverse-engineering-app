// 
// Decompiled by Procyon v0.6.0
// 

package com.alibaba.android.arouter.routes;

import com.intsig.camscanner.tsapp.RouterToolPageServiceImpl;
import com.intsig.camscanner.purchase.utils.ProductManagerService;
import java.util.HashMap;
import com.intsig.camscanner.purchase.activity.PurchaseIntentActivity;
import com.intsig.camscanner.mainmenu.mainactivity.MainActivity;
import com.intsig.camscanner.tsapp.RouterLogoutTaskServiceImpl;
import com.intsig.camscanner.tsapp.RouterLoginTaskServiceImpl;
import com.intsig.camscanner.router.RouterMainServiceImpl;
import com.alibaba.android.arouter.facade.enums.RouteType;
import com.alibaba.android.arouter.facade.model.RouteMeta;
import java.util.Map;
import com.alibaba.android.arouter.facade.template.IRouteGroup;

public class ARouter$$Group$$main implements IRouteGroup
{
    @Override
    public void loadInto(final Map<String, RouteMeta> map) {
        final RouteType provider = RouteType.PROVIDER;
        map.put("/main/connectmain", RouteMeta.build(provider, RouterMainServiceImpl.class, "/main/connectmain", "main", null, -1, Integer.MIN_VALUE));
        map.put("/main/login_task", RouteMeta.build(provider, RouterLoginTaskServiceImpl.class, "/main/login_task", "main", null, -1, Integer.MIN_VALUE));
        map.put("/main/logout_task", RouteMeta.build(provider, RouterLogoutTaskServiceImpl.class, "/main/logout_task", "main", null, -1, Integer.MIN_VALUE));
        final RouteType activity = RouteType.ACTIVITY;
        map.put("/main/main_menu_new", RouteMeta.build(activity, MainActivity.class, "/main/main_menu_new", "main", null, -1, Integer.MIN_VALUE));
        map.put("/main/premium", RouteMeta.build(activity, PurchaseIntentActivity.class, "/main/premium", "main", new HashMap<String, Integer>(this) {
            final ARouter$$Group$$main o0;
            
            {
                final Integer value = 8;
                this.put("path", value);
                this.put("style", value);
            }
        }, -1, Integer.MIN_VALUE));
        map.put("/main/productmanager", RouteMeta.build(provider, ProductManagerService.class, "/main/productmanager", "main", null, -1, Integer.MIN_VALUE));
        map.put("/main/toolpage", RouteMeta.build(provider, RouterToolPageServiceImpl.class, "/main/toolpage", "main", null, -1, Integer.MIN_VALUE));
    }
}

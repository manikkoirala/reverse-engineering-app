// 
// Decompiled by Procyon v0.6.0
// 

package com.alibaba.android.arouter.routes;

import com.alibaba.android.arouter.facade.template.IRouteGroup;
import java.util.Map;
import com.alibaba.android.arouter.facade.template.IRouteRoot;

public class ARouter$$Root$$camScanner_GP_402 implements IRouteRoot
{
    @Override
    public void loadInto(final Map<String, Class<? extends IRouteGroup>> map) {
        map.put("activity", ARouter$$Group$$activity.class);
        map.put("album", ARouter$$Group$$album.class);
        map.put("backup", ARouter$$Group$$backup.class);
        map.put("camera", ARouter$$Group$$camera.class);
        map.put("capture", ARouter$$Group$$capture.class);
        map.put("dir", ARouter$$Group$$dir.class);
        map.put("excel", ARouter$$Group$$excel.class);
        map.put("image", ARouter$$Group$$image.class);
        map.put("invoice", ARouter$$Group$$invoice.class);
        map.put("main", ARouter$$Group$$main.class);
        map.put("me", ARouter$$Group$$me.class);
        map.put("page", ARouter$$Group$$page.class);
        map.put("pdf", ARouter$$Group$$pdf.class);
        map.put("premium", ARouter$$Group$$premium.class);
        map.put("printer", ARouter$$Group$$printer.class);
        map.put("referral", ARouter$$Group$$referral.class);
        map.put("web", ARouter$$Group$$web.class);
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.alibaba.android.arouter.routes;

import java.util.HashMap;
import com.intsig.camscanner.web.WebIntentActivity;
import com.intsig.camscanner.web.WebServiceForImpl;
import com.alibaba.android.arouter.facade.enums.RouteType;
import com.alibaba.android.arouter.facade.model.RouteMeta;
import java.util.Map;
import com.alibaba.android.arouter.facade.template.IRouteGroup;

public class ARouter$$Group$$web implements IRouteGroup
{
    @Override
    public void loadInto(final Map<String, RouteMeta> map) {
        map.put("/web/allpage", RouteMeta.build(RouteType.PROVIDER, WebServiceForImpl.class, "/web/allpage", "web", null, -1, Integer.MIN_VALUE));
        map.put("/web/page", RouteMeta.build(RouteType.ACTIVITY, WebIntentActivity.class, "/web/page", "web", new HashMap<String, Integer>(this) {
            final ARouter$$Group$$web o0;
            
            {
                final Integer value = 8;
                this.put("path", value);
                this.put("url", value);
            }
        }, -1, Integer.MIN_VALUE));
    }
}

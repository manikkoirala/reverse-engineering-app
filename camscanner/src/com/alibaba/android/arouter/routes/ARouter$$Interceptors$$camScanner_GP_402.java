// 
// Decompiled by Procyon v0.6.0
// 

package com.alibaba.android.arouter.routes;

import com.intsig.camscanner.mainmenu.MainMenuInterceptor;
import com.alibaba.android.arouter.facade.template.IInterceptor;
import java.util.Map;
import com.alibaba.android.arouter.facade.template.IInterceptorGroup;

public class ARouter$$Interceptors$$camScanner_GP_402 implements IInterceptorGroup
{
    @Override
    public void loadInto(final Map<Integer, Class<? extends IInterceptor>> map) {
        map.put(8, (Class<? extends IInterceptor>)MainMenuInterceptor.class);
    }
}

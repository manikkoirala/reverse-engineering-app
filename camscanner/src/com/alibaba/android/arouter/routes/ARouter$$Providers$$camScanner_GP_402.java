// 
// Decompiled by Procyon v0.6.0
// 

package com.alibaba.android.arouter.routes;

import com.intsig.camscanner.router.TopicServiceImpl;
import com.intsig.camscanner.router.ToWordServiceImpl;
import com.intsig.camscanner.router.ToOcrServiceImpl;
import com.intsig.camscanner.web.WebServiceForImpl;
import com.intsig.camscanner.tsapp.RouterReferralFunction;
import com.intsig.camscanner.tsapp.RouterToolPageServiceImpl;
import com.intsig.camscanner.purchase.utils.ProductManagerService;
import com.intsig.camscanner.tsapp.RouterLogoutTaskServiceImpl;
import com.intsig.camscanner.tsapp.RouterLoginTaskServiceImpl;
import com.intsig.camscanner.router.RouterMainServiceImpl;
import com.intsig.camscanner.router.AlbumServiceImpl;
import com.alibaba.android.arouter.facade.enums.RouteType;
import com.alibaba.android.arouter.facade.model.RouteMeta;
import java.util.Map;
import com.alibaba.android.arouter.facade.template.IProviderGroup;

public class ARouter$$Providers$$camScanner_GP_402 implements IProviderGroup
{
    @Override
    public void loadInto(final Map<String, RouteMeta> map) {
        final RouteType provider = RouteType.PROVIDER;
        map.put("com.intsig.router.service.RouterAlbumService", RouteMeta.build(provider, AlbumServiceImpl.class, "/album/save_album", "album", null, -1, Integer.MIN_VALUE));
        map.put("com.intsig.router.service.RouterMainService", RouteMeta.build(provider, RouterMainServiceImpl.class, "/main/connectmain", "main", null, -1, Integer.MIN_VALUE));
        map.put("com.intsig.router.service.RouterLoginTaskService", RouteMeta.build(provider, RouterLoginTaskServiceImpl.class, "/main/login_task", "main", null, -1, Integer.MIN_VALUE));
        map.put("com.intsig.router.service.RouterLogoutTaskService", RouteMeta.build(provider, RouterLogoutTaskServiceImpl.class, "/main/logout_task", "main", null, -1, Integer.MIN_VALUE));
        map.put("com.intsig.router.service.RouterProductService", RouteMeta.build(provider, ProductManagerService.class, "/main/productmanager", "main", null, -1, Integer.MIN_VALUE));
        map.put("com.intsig.router.service.RouterToolPageService", RouteMeta.build(provider, RouterToolPageServiceImpl.class, "/main/toolpage", "main", null, -1, Integer.MIN_VALUE));
        map.put("com.intsig.router.service.RouterReferralService", RouteMeta.build(provider, RouterReferralFunction.class, "/referral/third", "referral", null, -1, Integer.MIN_VALUE));
        map.put("com.intsig.router.service.RouterWebService", RouteMeta.build(provider, WebServiceForImpl.class, "/web/allpage", "web", null, -1, Integer.MIN_VALUE));
        map.put("com.intsig.router.service.RouterToOcrService", RouteMeta.build(provider, ToOcrServiceImpl.class, "/page/to_ocr", "page", null, -1, Integer.MIN_VALUE));
        map.put("com.intsig.router.service.RouterToWordService", RouteMeta.build(provider, ToWordServiceImpl.class, "/page/to_word", "page", null, -1, Integer.MIN_VALUE));
        map.put("com.intsig.router.service.RouterTopicService", RouteMeta.build(provider, TopicServiceImpl.class, "/page/topic", "page", null, -1, Integer.MIN_VALUE));
    }
}

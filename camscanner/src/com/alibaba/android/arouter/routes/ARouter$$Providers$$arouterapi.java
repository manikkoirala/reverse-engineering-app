// 
// Decompiled by Procyon v0.6.0
// 

package com.alibaba.android.arouter.routes;

import com.alibaba.android.arouter.core.InterceptorServiceImpl;
import com.alibaba.android.arouter.core.AutowiredServiceImpl;
import com.alibaba.android.arouter.facade.enums.RouteType;
import com.alibaba.android.arouter.facade.model.RouteMeta;
import java.util.Map;
import com.alibaba.android.arouter.facade.template.IProviderGroup;

public class ARouter$$Providers$$arouterapi implements IProviderGroup
{
    @Override
    public void loadInto(final Map<String, RouteMeta> map) {
        final RouteType provider = RouteType.PROVIDER;
        map.put("com.alibaba.android.arouter.facade.service.AutowiredService", RouteMeta.build(provider, AutowiredServiceImpl.class, "/arouter/service/autowired", "arouter", null, -1, Integer.MIN_VALUE));
        map.put("com.alibaba.android.arouter.facade.service.InterceptorService", RouteMeta.build(provider, InterceptorServiceImpl.class, "/arouter/service/interceptor", "arouter", null, -1, Integer.MIN_VALUE));
    }
}

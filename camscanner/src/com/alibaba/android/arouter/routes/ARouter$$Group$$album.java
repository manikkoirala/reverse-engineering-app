// 
// Decompiled by Procyon v0.6.0
// 

package com.alibaba.android.arouter.routes;

import com.intsig.camscanner.router.AlbumServiceImpl;
import com.alibaba.android.arouter.facade.enums.RouteType;
import com.alibaba.android.arouter.facade.model.RouteMeta;
import java.util.Map;
import com.alibaba.android.arouter.facade.template.IRouteGroup;

public class ARouter$$Group$$album implements IRouteGroup
{
    @Override
    public void loadInto(final Map<String, RouteMeta> map) {
        map.put("/album/save_album", RouteMeta.build(RouteType.PROVIDER, AlbumServiceImpl.class, "/album/save_album", "album", null, -1, Integer.MIN_VALUE));
    }
}

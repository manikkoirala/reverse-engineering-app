// 
// Decompiled by Procyon v0.6.0
// 

package com.alibaba.android.arouter.routes;

import com.intsig.camscanner.pagelist.ExcelPreviewActivity;
import com.alibaba.android.arouter.facade.enums.RouteType;
import com.alibaba.android.arouter.facade.model.RouteMeta;
import java.util.Map;
import com.alibaba.android.arouter.facade.template.IRouteGroup;

public class ARouter$$Group$$excel implements IRouteGroup
{
    @Override
    public void loadInto(final Map<String, RouteMeta> map) {
        map.put("/excel/preview", RouteMeta.build(RouteType.ACTIVITY, ExcelPreviewActivity.class, "/excel/preview", "excel", null, -1, Integer.MIN_VALUE));
    }
}

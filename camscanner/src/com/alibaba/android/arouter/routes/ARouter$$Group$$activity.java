// 
// Decompiled by Procyon v0.6.0
// 

package com.alibaba.android.arouter.routes;

import com.intsig.camscanner.innovationlab.InnovationLabActivity;
import com.intsig.camscanner.gallery.cloud_disk.CloudDiskActivity;
import com.alibaba.android.arouter.facade.enums.RouteType;
import com.alibaba.android.arouter.facade.model.RouteMeta;
import java.util.Map;
import com.alibaba.android.arouter.facade.template.IRouteGroup;

public class ARouter$$Group$$activity implements IRouteGroup
{
    @Override
    public void loadInto(final Map<String, RouteMeta> map) {
        final RouteType activity = RouteType.ACTIVITY;
        map.put("/activity/cloud_disk", RouteMeta.build(activity, CloudDiskActivity.class, "/activity/cloud_disk", "activity", null, -1, Integer.MIN_VALUE));
        map.put("/activity/innovation_lab", RouteMeta.build(activity, InnovationLabActivity.class, "/activity/innovation_lab", "activity", null, -1, Integer.MIN_VALUE));
    }
}

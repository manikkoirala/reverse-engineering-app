// 
// Decompiled by Procyon v0.6.0
// 

package com.alibaba.android.arouter.routes;

import com.intsig.camscanner.capture.invoice.InvoiceActivity;
import com.intsig.camscanner.capture.invoice.InvoiceDetailActivity;
import com.alibaba.android.arouter.facade.enums.RouteType;
import com.alibaba.android.arouter.facade.model.RouteMeta;
import java.util.Map;
import com.alibaba.android.arouter.facade.template.IRouteGroup;

public class ARouter$$Group$$invoice implements IRouteGroup
{
    @Override
    public void loadInto(final Map<String, RouteMeta> map) {
        final RouteType activity = RouteType.ACTIVITY;
        map.put("/invoice/detail", RouteMeta.build(activity, InvoiceDetailActivity.class, "/invoice/detail", "invoice", null, -1, Integer.MIN_VALUE));
        map.put("/invoice/result", RouteMeta.build(activity, InvoiceActivity.class, "/invoice/result", "invoice", null, -1, Integer.MIN_VALUE));
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.alibaba.android.arouter.routes;

import com.intsig.camscanner.ImportIntentActivity;
import com.intsig.camscanner.capture.count.CountNumberActivity;
import com.alibaba.android.arouter.facade.enums.RouteType;
import com.alibaba.android.arouter.facade.model.RouteMeta;
import java.util.Map;
import com.alibaba.android.arouter.facade.template.IRouteGroup;

public class ARouter$$Group$$camera implements IRouteGroup
{
    @Override
    public void loadInto(final Map<String, RouteMeta> map) {
        final RouteType activity = RouteType.ACTIVITY;
        map.put("/camera/count", RouteMeta.build(activity, CountNumberActivity.class, "/camera/count", "camera", null, -1, Integer.MIN_VALUE));
        map.put("/camera/import", RouteMeta.build(activity, ImportIntentActivity.class, "/camera/import", "camera", null, -1, Integer.MIN_VALUE));
    }
}

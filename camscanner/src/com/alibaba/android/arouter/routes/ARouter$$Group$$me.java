// 
// Decompiled by Procyon v0.6.0
// 

package com.alibaba.android.arouter.routes;

import com.intsig.camscanner.settings.thirdservice.ThirdServiceActivity;
import com.intsig.camscanner.tsapp.SyncStateActivity;
import com.intsig.camscanner.message.MessageSettingActivity;
import com.intsig.camscanner.settings.newsettings.SettingsActivity;
import com.intsig.camscanner.settings.SecuritySettingActivity;
import com.intsig.camscanner.settings.ImageScanSettingActivity;
import com.intsig.camscanner.settings.ocr.OcrSettingActivity;
import com.intsig.camscanner.message.MessageCenterActivity;
import com.intsig.camscanner.LikeActivity;
import com.intsig.camscanner.settings.FeedBackSettingActivity;
import com.intsig.camscanner.settings.DocumentManagerActivity;
import com.intsig.camscanner.settings.ExportDocumentSettingActivity;
import com.intsig.camscanner.settings.CacheCleanActivity;
import com.intsig.camscanner.tsapp.ChangePasswordActivity;
import com.intsig.camscanner.settings.newsettings.MyBenefitsActivity;
import com.intsig.camscanner.settings.HelpSettingActivity;
import com.alibaba.android.arouter.facade.enums.RouteType;
import com.alibaba.android.arouter.facade.model.RouteMeta;
import java.util.Map;
import com.alibaba.android.arouter.facade.template.IRouteGroup;

public class ARouter$$Group$$me implements IRouteGroup
{
    @Override
    public void loadInto(final Map<String, RouteMeta> map) {
        final RouteType activity = RouteType.ACTIVITY;
        map.put("/me/about", RouteMeta.build(activity, HelpSettingActivity.class, "/me/about", "me", null, -1, Integer.MIN_VALUE));
        map.put("/me/benefits", RouteMeta.build(activity, MyBenefitsActivity.class, "/me/benefits", "me", null, -1, Integer.MIN_VALUE));
        map.put("/me/change_password", RouteMeta.build(activity, ChangePasswordActivity.class, "/me/change_password", "me", null, -1, Integer.MIN_VALUE));
        map.put("/me/clear_space", RouteMeta.build(activity, CacheCleanActivity.class, "/me/clear_space", "me", null, -1, Integer.MIN_VALUE));
        map.put("/me/doc_export", RouteMeta.build(activity, ExportDocumentSettingActivity.class, "/me/doc_export", "me", null, -1, Integer.MIN_VALUE));
        map.put("/me/doc_management", RouteMeta.build(activity, DocumentManagerActivity.class, "/me/doc_management", "me", null, -1, Integer.MIN_VALUE));
        map.put("/me/feed_back", RouteMeta.build(activity, FeedBackSettingActivity.class, "/me/feed_back", "me", null, -1, Integer.MIN_VALUE));
        map.put("/me/like", RouteMeta.build(activity, LikeActivity.class, "/me/like", "me", null, -1, Integer.MIN_VALUE));
        map.put("/me/message", RouteMeta.build(activity, MessageCenterActivity.class, "/me/message", "me", null, -1, Integer.MIN_VALUE));
        map.put("/me/ocr", RouteMeta.build(activity, OcrSettingActivity.class, "/me/ocr", "me", null, -1, Integer.MIN_VALUE));
        map.put("/me/scansetting", RouteMeta.build(activity, ImageScanSettingActivity.class, "/me/scansetting", "me", null, -1, Integer.MIN_VALUE));
        map.put("/me/security_setting", RouteMeta.build(activity, SecuritySettingActivity.class, "/me/security_setting", "me", null, -1, Integer.MIN_VALUE));
        map.put("/me/setting_main", RouteMeta.build(activity, SettingsActivity.class, "/me/setting_main", "me", null, -1, Integer.MIN_VALUE));
        map.put("/me/socket_push_setting", RouteMeta.build(activity, MessageSettingActivity.class, "/me/socket_push_setting", "me", null, -1, Integer.MIN_VALUE));
        map.put("/me/sync_state", RouteMeta.build(activity, SyncStateActivity.class, "/me/sync_state", "me", null, -1, Integer.MIN_VALUE));
        map.put("/me/third_service", RouteMeta.build(activity, ThirdServiceActivity.class, "/me/third_service", "me", null, -1, Integer.MIN_VALUE));
    }
}

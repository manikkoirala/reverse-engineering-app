// 
// Decompiled by Procyon v0.6.0
// 

package com.alibaba.android.arouter.routes;

import com.intsig.camscanner.image_progress.image_editing.ImageEditingActivity;
import com.alibaba.android.arouter.facade.enums.RouteType;
import com.alibaba.android.arouter.facade.model.RouteMeta;
import java.util.Map;
import com.alibaba.android.arouter.facade.template.IRouteGroup;

public class ARouter$$Group$$image implements IRouteGroup
{
    @Override
    public void loadInto(final Map<String, RouteMeta> map) {
        map.put("/image/editing", RouteMeta.build(RouteType.ACTIVITY, ImageEditingActivity.class, "/image/editing", "image", null, -1, Integer.MIN_VALUE));
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.alibaba.android.arouter.routes;

import com.intsig.camscanner.pdf.kit.PdfKitMainActivity;
import com.alibaba.android.arouter.facade.enums.RouteType;
import com.alibaba.android.arouter.facade.model.RouteMeta;
import java.util.Map;
import com.alibaba.android.arouter.facade.template.IRouteGroup;

public class ARouter$$Group$$pdf implements IRouteGroup
{
    @Override
    public void loadInto(final Map<String, RouteMeta> map) {
        map.put("/pdf/toolpage", RouteMeta.build(RouteType.ACTIVITY, PdfKitMainActivity.class, "/pdf/toolpage", "pdf", null, -1, Integer.MIN_VALUE));
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package kotlinx.coroutines.intrinsics;

import kotlin.ResultKt;
import kotlin.Result$Companion;
import kotlinx.coroutines.internal.DispatchedContinuationKt;
import kotlin.Result;
import kotlin.coroutines.intrinsics.IntrinsicsKt;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
import org.jetbrains.annotations.NotNull;
import kotlin.coroutines.Continuation;
import kotlin.jvm.functions.Function2;
import kotlin.Metadata;

@Metadata
public final class CancellableKt
{
    public static final <R, T> void O8(@NotNull final Function2<? super R, ? super Continuation<? super T>, ?> function2, final R r, @NotNull final Continuation<? super T> continuation, final Function1<? super Throwable, Unit> function3) {
        try {
            final Continuation \u3007o\u3007 = IntrinsicsKt.\u3007o\u3007(IntrinsicsKt.\u3007o00\u3007\u3007Oo((Function2)function2, (Object)r, (Continuation)continuation));
            final Result$Companion companion = Result.Companion;
            DispatchedContinuationKt.\u3007o00\u3007\u3007Oo((kotlin.coroutines.Continuation<? super Object>)\u3007o\u3007, Result.constructor-impl((Object)Unit.\u3007080), function3);
        }
        finally {
            final Throwable t;
            \u3007080(continuation, t);
        }
    }
    
    private static final void \u3007080(final Continuation<?> continuation, final Throwable t) {
        final Result$Companion companion = Result.Companion;
        continuation.resumeWith(Result.constructor-impl(ResultKt.\u3007080(t)));
        throw t;
    }
    
    public static final void \u3007o00\u3007\u3007Oo(@NotNull final Continuation<? super Unit> continuation, @NotNull final Continuation<?> continuation2) {
        try {
            final Continuation \u3007o\u3007 = IntrinsicsKt.\u3007o\u3007((Continuation)continuation);
            final Result$Companion companion = Result.Companion;
            DispatchedContinuationKt.\u3007o\u3007(\u3007o\u3007, Result.constructor-impl((Object)Unit.\u3007080), null, 2, null);
        }
        finally {
            final Throwable t;
            \u3007080(continuation2, t);
        }
    }
    
    public static final <T> void \u3007o\u3007(@NotNull final Function1<? super Continuation<? super T>, ?> function1, @NotNull final Continuation<? super T> continuation) {
        try {
            final Continuation \u3007o\u3007 = IntrinsicsKt.\u3007o\u3007(IntrinsicsKt.\u3007080((Function1)function1, (Continuation)continuation));
            final Result$Companion companion = Result.Companion;
            DispatchedContinuationKt.\u3007o\u3007(\u3007o\u3007, Result.constructor-impl((Object)Unit.\u3007080), null, 2, null);
        }
        finally {
            final Throwable t;
            \u3007080(continuation, t);
        }
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package kotlinx.coroutines.intrinsics;

import kotlinx.coroutines.JobSupport;
import kotlin.Result$Companion;
import kotlin.ResultKt;
import kotlin.Result;
import kotlinx.coroutines.internal.ThreadContextKt;
import kotlin.coroutines.jvm.internal.DebugProbesKt;
import kotlin.jvm.functions.Function1;
import kotlinx.coroutines.TimeoutCancellationException;
import kotlinx.coroutines.JobSupportKt;
import kotlin.coroutines.intrinsics.IntrinsicsKt;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlinx.coroutines.CompletedExceptionally;
import kotlin.jvm.internal.TypeIntrinsics;
import kotlin.coroutines.Continuation;
import kotlin.jvm.functions.Function2;
import org.jetbrains.annotations.NotNull;
import kotlinx.coroutines.internal.ScopeCoroutine;
import kotlin.Metadata;

@Metadata
public final class UndispatchedKt
{
    public static final <T, R> Object O8(@NotNull final ScopeCoroutine<? super T> scopeCoroutine, final R r, @NotNull final Function2<? super R, ? super Continuation<? super T>, ?> function2) {
        boolean b = false;
        Object oo80 = null;
        try {
            ((Function2)TypeIntrinsics.\u3007o\u3007((Object)function2, 2)).invoke((Object)r, (Object)scopeCoroutine);
        }
        finally {
            final Throwable t;
            oo80 = new CompletedExceptionally(t, false, 2, (DefaultConstructorMarker)null);
        }
        Object o;
        if (oo80 == IntrinsicsKt.O8()) {
            o = IntrinsicsKt.O8();
        }
        else {
            final Object \u3007o0O0O8 = ((JobSupport)scopeCoroutine).\u3007o0O0O8(oo80);
            if (\u3007o0O0O8 == JobSupportKt.\u3007o00\u3007\u3007Oo) {
                o = IntrinsicsKt.O8();
            }
            else {
                if (\u3007o0O0O8 instanceof CompletedExceptionally) {
                    final Throwable \u3007080 = ((CompletedExceptionally)\u3007o0O0O8).\u3007080;
                    if (!(\u3007080 instanceof TimeoutCancellationException) || ((TimeoutCancellationException)\u3007080).coroutine != scopeCoroutine) {
                        b = true;
                    }
                    if (b) {
                        throw \u3007080;
                    }
                    if (oo80 instanceof CompletedExceptionally) {
                        throw ((CompletedExceptionally)oo80).\u3007080;
                    }
                }
                else {
                    oo80 = JobSupportKt.oO80(\u3007o0O0O8);
                }
                o = oo80;
            }
        }
        return o;
    }
    
    public static final <T> void \u3007080(@NotNull final Function1<? super Continuation<? super T>, ?> function1, @NotNull Continuation<? super T> context) {
        final Continuation \u3007080 = DebugProbesKt.\u3007080((Continuation)context);
        try {
            context = ((Continuation)context).getContext();
            final Object \u3007o\u3007 = ThreadContextKt.\u3007o\u3007(context, null);
            try {
                final Object invoke = ((Function1)TypeIntrinsics.\u3007o\u3007((Object)function1, 1)).invoke((Object)\u3007080);
                ThreadContextKt.\u3007080(context, \u3007o\u3007);
                if (invoke != IntrinsicsKt.O8()) {
                    \u3007080.resumeWith(Result.constructor-impl(invoke));
                }
            }
            finally {
                ThreadContextKt.\u3007080(context, \u3007o\u3007);
            }
        }
        finally {
            final Result$Companion companion = Result.Companion;
            final Throwable t;
            \u3007080.resumeWith(Result.constructor-impl(ResultKt.\u3007080(t)));
        }
    }
    
    public static final <R, T> void \u3007o00\u3007\u3007Oo(@NotNull final Function2<? super R, ? super Continuation<? super T>, ?> function2, final R r, @NotNull Continuation<? super T> context) {
        final Continuation \u3007080 = DebugProbesKt.\u3007080((Continuation)context);
        try {
            context = ((Continuation)context).getContext();
            final Object \u3007o\u3007 = ThreadContextKt.\u3007o\u3007(context, null);
            try {
                final Object invoke = ((Function2)TypeIntrinsics.\u3007o\u3007((Object)function2, 2)).invoke((Object)r, (Object)\u3007080);
                ThreadContextKt.\u3007080(context, \u3007o\u3007);
                if (invoke != IntrinsicsKt.O8()) {
                    \u3007080.resumeWith(Result.constructor-impl(invoke));
                }
            }
            finally {
                ThreadContextKt.\u3007080(context, \u3007o\u3007);
            }
        }
        finally {
            final Result$Companion companion = Result.Companion;
            final Throwable t;
            \u3007080.resumeWith(Result.constructor-impl(ResultKt.\u3007080(t)));
        }
    }
    
    public static final <T, R> Object \u3007o\u3007(@NotNull final ScopeCoroutine<? super T> scopeCoroutine, final R r, @NotNull final Function2<? super R, ? super Continuation<? super T>, ?> function2) {
        CompletedExceptionally completedExceptionally = null;
        try {
            ((Function2)TypeIntrinsics.\u3007o\u3007((Object)function2, 2)).invoke((Object)r, (Object)scopeCoroutine);
        }
        finally {
            final Throwable t;
            completedExceptionally = new CompletedExceptionally(t, false, 2, (DefaultConstructorMarker)null);
        }
        Object o;
        if (completedExceptionally == IntrinsicsKt.O8()) {
            o = IntrinsicsKt.O8();
        }
        else {
            final Object \u3007o0O0O8 = ((JobSupport)scopeCoroutine).\u3007o0O0O8((Object)completedExceptionally);
            if (\u3007o0O0O8 == JobSupportKt.\u3007o00\u3007\u3007Oo) {
                o = IntrinsicsKt.O8();
            }
            else {
                if (\u3007o0O0O8 instanceof CompletedExceptionally) {
                    throw ((CompletedExceptionally)\u3007o0O0O8).\u3007080;
                }
                o = JobSupportKt.oO80(\u3007o0O0O8);
            }
        }
        return o;
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package kotlinx.coroutines.internal;

import java.util.Iterator;
import kotlin.sequences.SequencesKt;
import java.util.ServiceLoader;
import org.jetbrains.annotations.NotNull;
import kotlinx.coroutines.CoroutineExceptionHandler;
import java.util.Collection;
import kotlin.Metadata;

@Metadata
public final class CoroutineExceptionHandlerImplKt
{
    @NotNull
    private static final Collection<CoroutineExceptionHandler> \u3007080;
    
    static {
        \u3007080 = SequencesKt.\u3007O888o0o(SequencesKt.\u3007o\u3007((Iterator)ServiceLoader.load(CoroutineExceptionHandler.class, CoroutineExceptionHandler.class.getClassLoader()).iterator()));
    }
    
    @NotNull
    public static final Collection<CoroutineExceptionHandler> \u3007080() {
        return CoroutineExceptionHandlerImplKt.\u3007080;
    }
    
    public static final void \u3007o00\u3007\u3007Oo(@NotNull final Throwable t) {
        final Thread currentThread = Thread.currentThread();
        currentThread.getUncaughtExceptionHandler().uncaughtException(currentThread, t);
    }
}

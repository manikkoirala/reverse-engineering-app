// 
// Decompiled by Procyon v0.6.0
// 

package kotlinx.coroutines.internal;

import kotlin.Result$Companion;
import kotlin.ResultKt;
import kotlin.coroutines.jvm.internal.BaseContinuationImpl;
import kotlin.Result;
import _COROUTINE.ArtificialStackFrames;
import org.jetbrains.annotations.NotNull;
import kotlin.Metadata;

@Metadata
public final class StackTraceRecoveryKt
{
    @NotNull
    private static final StackTraceElement \u3007080;
    private static final String \u3007o00\u3007\u3007Oo;
    private static final String \u3007o\u3007;
    
    static {
        \u3007080 = new ArtificialStackFrames().\u3007080();
        Object constructor-impl = null;
        try {
            final Result$Companion companion = Result.Companion;
            Result.constructor-impl((Object)BaseContinuationImpl.class.getCanonicalName());
        }
        finally {
            final Result$Companion companion2 = Result.Companion;
            final Throwable t;
            constructor-impl = Result.constructor-impl(ResultKt.\u3007080(t));
        }
        if (Result.exceptionOrNull-impl(constructor-impl) != null) {
            constructor-impl = "kotlin.coroutines.jvm.internal.BaseContinuationImpl";
        }
        \u3007o00\u3007\u3007Oo = (String)constructor-impl;
        Object constructor-impl2 = null;
        try {
            Result.constructor-impl((Object)StackTraceRecoveryKt.class.getCanonicalName());
        }
        finally {
            final Result$Companion companion3 = Result.Companion;
            final Throwable t2;
            constructor-impl2 = Result.constructor-impl(ResultKt.\u3007080(t2));
        }
        if (Result.exceptionOrNull-impl(constructor-impl2) != null) {
            constructor-impl2 = "kotlinx.coroutines.internal.StackTraceRecoveryKt";
        }
        \u3007o\u3007 = (String)constructor-impl2;
    }
    
    @NotNull
    public static final <E extends Throwable> E \u3007080(@NotNull final E e) {
        return e;
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package kotlinx.coroutines.internal;

import org.jetbrains.annotations.NotNull;
import kotlinx.coroutines.DebugStringsKt;
import kotlin.Metadata;

@Metadata
public abstract class OpDescriptor
{
    @NotNull
    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append(DebugStringsKt.\u3007080((Object)this));
        sb.append('@');
        sb.append(DebugStringsKt.\u3007o00\u3007\u3007Oo((Object)this));
        return sb.toString();
    }
    
    public abstract Object \u3007080(final Object p0);
}

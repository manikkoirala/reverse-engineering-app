// 
// Decompiled by Procyon v0.6.0
// 

package kotlinx.coroutines.internal;

import kotlin.jvm.internal.Intrinsics;
import kotlin.coroutines.CoroutineContext;
import kotlinx.coroutines.ThreadContextElement;
import org.jetbrains.annotations.NotNull;
import kotlin.coroutines.CoroutineContext$Element;
import kotlin.jvm.functions.Function2;
import kotlin.Metadata;

@Metadata
public final class ThreadContextKt
{
    @NotNull
    private static final Function2<ThreadState, CoroutineContext$Element, ThreadState> O8;
    @NotNull
    public static final Symbol \u3007080;
    @NotNull
    private static final Function2<Object, CoroutineContext$Element, Object> \u3007o00\u3007\u3007Oo;
    @NotNull
    private static final Function2<ThreadContextElement<?>, CoroutineContext$Element, ThreadContextElement<?>> \u3007o\u3007;
    
    static {
        \u3007080 = new Symbol("NO_THREAD_ELEMENTS");
        \u3007o00\u3007\u3007Oo = (Function2)ThreadContextKt$countAll.ThreadContextKt$countAll$1.o0;
        \u3007o\u3007 = (Function2)ThreadContextKt$findOne.ThreadContextKt$findOne$1.o0;
        O8 = (Function2)ThreadContextKt$updateState.ThreadContextKt$updateState$1.o0;
    }
    
    public static final void \u3007080(@NotNull final CoroutineContext coroutineContext, final Object o) {
        if (o == ThreadContextKt.\u3007080) {
            return;
        }
        if (o instanceof ThreadState) {
            ((ThreadState)o).\u3007o00\u3007\u3007Oo(coroutineContext);
        }
        else {
            final Object fold = coroutineContext.fold((Object)null, (Function2)ThreadContextKt.\u3007o\u3007);
            Intrinsics.o\u30070(fold, "null cannot be cast to non-null type kotlinx.coroutines.ThreadContextElement<kotlin.Any?>");
            ((ThreadContextElement)fold).\u3007O00(coroutineContext, o);
        }
    }
    
    @NotNull
    public static final Object \u3007o00\u3007\u3007Oo(@NotNull final CoroutineContext coroutineContext) {
        final Object fold = coroutineContext.fold((Object)0, (Function2)ThreadContextKt.\u3007o00\u3007\u3007Oo);
        Intrinsics.Oo08(fold);
        return fold;
    }
    
    public static final Object \u3007o\u3007(@NotNull final CoroutineContext coroutineContext, final Object o) {
        Object \u3007o00\u3007\u3007Oo = o;
        if (o == null) {
            \u3007o00\u3007\u3007Oo = \u3007o00\u3007\u3007Oo(coroutineContext);
        }
        Object o2;
        if (\u3007o00\u3007\u3007Oo == Integer.valueOf(0)) {
            o2 = ThreadContextKt.\u3007080;
        }
        else if (\u3007o00\u3007\u3007Oo instanceof Integer) {
            o2 = coroutineContext.fold((Object)new ThreadState(coroutineContext, ((Number)\u3007o00\u3007\u3007Oo).intValue()), (Function2)ThreadContextKt.O8);
        }
        else {
            Intrinsics.o\u30070(\u3007o00\u3007\u3007Oo, "null cannot be cast to non-null type kotlinx.coroutines.ThreadContextElement<kotlin.Any?>");
            o2 = ((ThreadContextElement)\u3007o00\u3007\u3007Oo).oO(coroutineContext);
        }
        return o2;
    }
}

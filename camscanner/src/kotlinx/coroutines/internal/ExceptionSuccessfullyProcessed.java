// 
// Decompiled by Procyon v0.6.0
// 

package kotlinx.coroutines.internal;

import org.jetbrains.annotations.NotNull;
import kotlin.Metadata;

@Metadata
public final class ExceptionSuccessfullyProcessed extends Exception
{
    @NotNull
    public static final ExceptionSuccessfullyProcessed INSTANCE;
    
    static {
        INSTANCE = new ExceptionSuccessfullyProcessed();
    }
    
    private ExceptionSuccessfullyProcessed() {
    }
}

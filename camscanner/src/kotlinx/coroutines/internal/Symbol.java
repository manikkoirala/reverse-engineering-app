// 
// Decompiled by Procyon v0.6.0
// 

package kotlinx.coroutines.internal;

import org.jetbrains.annotations.NotNull;
import kotlin.Metadata;

@Metadata
public final class Symbol
{
    @NotNull
    public final String \u3007080;
    
    public Symbol(@NotNull final String \u3007080) {
        this.\u3007080 = \u3007080;
    }
    
    @NotNull
    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append('<');
        sb.append(this.\u3007080);
        sb.append('>');
        return sb.toString();
    }
}

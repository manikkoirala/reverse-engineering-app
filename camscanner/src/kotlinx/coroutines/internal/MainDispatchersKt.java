// 
// Decompiled by Procyon v0.6.0
// 

package kotlinx.coroutines.internal;

import kotlin.KotlinNothingValueException;
import kotlinx.coroutines.MainCoroutineDispatcher;
import java.util.List;
import org.jetbrains.annotations.NotNull;
import kotlin.Metadata;

@Metadata
public final class MainDispatchersKt
{
    private static final boolean \u3007080 = true;
    
    @NotNull
    public static final Void O8() {
        throw new IllegalStateException("Module with the Main dispatcher is missing. Add dependency providing the Main dispatcher, e.g. 'kotlinx-coroutines-android' and ensure it has the same version as 'kotlinx-coroutines-core'");
    }
    
    @NotNull
    public static final MainCoroutineDispatcher Oo08(@NotNull MainDispatcherFactory \u3007o\u3007, @NotNull final List<? extends MainDispatcherFactory> list) {
        MissingMainCoroutineDispatcher \u3007080 = null;
        try {
            \u3007o\u3007 = (MainDispatcherFactory)\u3007o\u3007.\u3007o\u3007(list);
        }
        finally {
            final Throwable t;
            \u3007080 = \u3007080(t, \u3007o\u3007.\u3007o00\u3007\u3007Oo());
        }
        return \u3007080;
    }
    
    private static final MissingMainCoroutineDispatcher \u3007080(final Throwable t, final String s) {
        if (MainDispatchersKt.\u3007080) {
            return new MissingMainCoroutineDispatcher(t, s);
        }
        if (t != null) {
            throw t;
        }
        O8();
        throw new KotlinNothingValueException();
    }
    
    public static final boolean \u3007o\u3007(@NotNull final MainCoroutineDispatcher mainCoroutineDispatcher) {
        return mainCoroutineDispatcher.\u30078() instanceof MissingMainCoroutineDispatcher;
    }
}

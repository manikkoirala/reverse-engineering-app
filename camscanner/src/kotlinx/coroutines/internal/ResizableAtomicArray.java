// 
// Decompiled by Procyon v0.6.0
// 

package kotlinx.coroutines.internal;

import kotlin.ranges.RangesKt;
import org.jetbrains.annotations.NotNull;
import java.util.concurrent.atomic.AtomicReferenceArray;
import kotlin.Metadata;

@Metadata
public final class ResizableAtomicArray<T>
{
    @NotNull
    private volatile AtomicReferenceArray<T> array;
    
    public ResizableAtomicArray(final int length) {
        this.array = new AtomicReferenceArray<T>(length);
    }
    
    public final int \u3007080() {
        return this.array.length();
    }
    
    public final T \u3007o00\u3007\u3007Oo(final int i) {
        final AtomicReferenceArray<T> array = this.array;
        Object value;
        if (i < array.length()) {
            value = array.get(i);
        }
        else {
            value = null;
        }
        return (T)value;
    }
    
    public final void \u3007o\u3007(final int n, final T t) {
        final AtomicReferenceArray<T> array = this.array;
        final int length = array.length();
        if (n < length) {
            array.set(n, t);
            return;
        }
        final AtomicReferenceArray array2 = new AtomicReferenceArray<T>(RangesKt.\u3007o\u3007(n + 1, length * 2));
        for (int i = 0; i < length; ++i) {
            array2.set(i, array.get(i));
        }
        array2.set(n, t);
        this.array = (AtomicReferenceArray<T>)array2;
    }
}

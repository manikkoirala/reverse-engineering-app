// 
// Decompiled by Procyon v0.6.0
// 

package kotlinx.coroutines.internal;

import kotlinx.coroutines.CoroutineDispatcher;
import kotlin.Unit;
import kotlinx.coroutines.CancellableContinuation;
import kotlinx.coroutines.DisposableHandle;
import kotlin.coroutines.CoroutineContext;
import org.jetbrains.annotations.NotNull;
import kotlin.KotlinNothingValueException;
import kotlin.Metadata;
import kotlinx.coroutines.Delay;
import kotlinx.coroutines.MainCoroutineDispatcher;

@Metadata
final class MissingMainCoroutineDispatcher extends MainCoroutineDispatcher implements Delay
{
    private final Throwable o0;
    private final String \u3007OOo8\u30070;
    
    public MissingMainCoroutineDispatcher(final Throwable o0, final String \u3007oOo8\u30070) {
        this.o0 = o0;
        this.\u3007OOo8\u30070 = \u3007oOo8\u30070;
    }
    
    private final Void O\u3007O\u3007oO() {
        if (this.o0 != null) {
            final StringBuilder sb = new StringBuilder();
            sb.append("Module with the Main dispatcher had failed to initialize");
            final String \u3007oOo8\u30070 = this.\u3007OOo8\u30070;
            String string = null;
            Label_0066: {
                if (\u3007oOo8\u30070 != null) {
                    final StringBuilder sb2 = new StringBuilder();
                    sb2.append(". ");
                    sb2.append(\u3007oOo8\u30070);
                    if ((string = sb2.toString()) != null) {
                        break Label_0066;
                    }
                }
                string = "";
            }
            sb.append(string);
            throw new IllegalStateException(sb.toString(), this.o0);
        }
        MainDispatchersKt.O8();
        throw new KotlinNothingValueException();
    }
    
    @NotNull
    public DisposableHandle O8\u3007o(final long n, @NotNull final Runnable runnable, @NotNull final CoroutineContext coroutineContext) {
        this.O\u3007O\u3007oO();
        throw new KotlinNothingValueException();
    }
    
    public boolean isDispatchNeeded(@NotNull final CoroutineContext coroutineContext) {
        this.O\u3007O\u3007oO();
        throw new KotlinNothingValueException();
    }
    
    @NotNull
    public CoroutineDispatcher limitedParallelism(final int n) {
        this.O\u3007O\u3007oO();
        throw new KotlinNothingValueException();
    }
    
    @NotNull
    public Void o8oO\u3007(final long n, @NotNull final CancellableContinuation<? super Unit> cancellableContinuation) {
        this.O\u3007O\u3007oO();
        throw new KotlinNothingValueException();
    }
    
    @NotNull
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append("Dispatchers.Main[missing");
        String string;
        if (this.o0 != null) {
            final StringBuilder sb2 = new StringBuilder();
            sb2.append(", cause=");
            sb2.append(this.o0);
            string = sb2.toString();
        }
        else {
            string = "";
        }
        sb.append(string);
        sb.append(']');
        return sb.toString();
    }
    
    @NotNull
    public MainCoroutineDispatcher \u30078() {
        return this;
    }
    
    @NotNull
    public Void \u30078\u30070\u3007o\u3007O(@NotNull final CoroutineContext coroutineContext, @NotNull final Runnable runnable) {
        this.O\u3007O\u3007oO();
        throw new KotlinNothingValueException();
    }
}

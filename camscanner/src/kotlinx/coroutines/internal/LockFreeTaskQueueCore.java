// 
// Decompiled by Procyon v0.6.0
// 

package kotlinx.coroutines.internal;

import androidx.concurrent.futures.\u3007080;
import kotlin.jvm.internal.DefaultConstructorMarker;
import java.util.concurrent.atomic.AtomicReferenceArray;
import java.util.concurrent.atomic.AtomicLongFieldUpdater;
import java.util.concurrent.atomic.AtomicReferenceFieldUpdater;
import org.jetbrains.annotations.NotNull;
import kotlin.Metadata;

@Metadata
public final class LockFreeTaskQueueCore<E>
{
    @NotNull
    public static final Companion Oo08;
    @NotNull
    public static final Symbol oO80;
    @NotNull
    private static final AtomicReferenceFieldUpdater o\u30070;
    @NotNull
    private static final AtomicLongFieldUpdater \u3007\u3007888;
    @NotNull
    private final AtomicReferenceArray O8;
    private volatile Object _next;
    private volatile long _state;
    private final int \u3007080;
    private final boolean \u3007o00\u3007\u3007Oo;
    private final int \u3007o\u3007;
    
    static {
        Oo08 = new Companion(null);
        o\u30070 = AtomicReferenceFieldUpdater.newUpdater(LockFreeTaskQueueCore.class, Object.class, "_next");
        \u3007\u3007888 = AtomicLongFieldUpdater.newUpdater(LockFreeTaskQueueCore.class, "_state");
        oO80 = new Symbol("REMOVE_FROZEN");
    }
    
    public LockFreeTaskQueueCore(final int n, final boolean \u3007o00\u3007\u3007Oo) {
        this.\u3007080 = n;
        this.\u3007o00\u3007\u3007Oo = \u3007o00\u3007\u3007Oo;
        final int \u3007o\u3007 = n - 1;
        this.\u3007o\u3007 = \u3007o\u3007;
        this.O8 = new AtomicReferenceArray(n);
        final int n2 = 0;
        if (\u3007o\u3007 > 1073741823) {
            throw new IllegalStateException("Check failed.".toString());
        }
        int n3 = n2;
        if ((n & \u3007o\u3007) == 0x0) {
            n3 = 1;
        }
        if (n3 != 0) {
            return;
        }
        throw new IllegalStateException("Check failed.".toString());
    }
    
    private final LockFreeTaskQueueCore<E> Oo08(final int n, final E newValue) {
        final Placeholder value = this.O8.get(this.\u3007o\u3007 & n);
        if (value instanceof Placeholder && value.\u3007080 == n) {
            this.O8.set(n & this.\u3007o\u3007, newValue);
            return this;
        }
        return null;
    }
    
    private final long oO80() {
        final AtomicLongFieldUpdater \u3007\u3007888 = LockFreeTaskQueueCore.\u3007\u3007888;
        long value;
        long n;
        do {
            value = \u3007\u3007888.get(this);
            if ((value & 0x1000000000000000L) != 0x0L) {
                return value;
            }
            n = (value | 0x1000000000000000L);
        } while (!\u3007\u3007888.compareAndSet(this, value, n));
        return n;
    }
    
    private final LockFreeTaskQueueCore<E> \u30078o8o\u3007(int n, final int n2) {
        final AtomicLongFieldUpdater \u3007\u3007888 = LockFreeTaskQueueCore.\u3007\u3007888;
        long value;
        do {
            value = \u3007\u3007888.get(this);
            n = (int)((0x3FFFFFFFL & value) >> 0);
            if ((0x1000000000000000L & value) != 0x0L) {
                return this.\u300780\u3007808\u3007O();
            }
        } while (!LockFreeTaskQueueCore.\u3007\u3007888.compareAndSet(this, value, LockFreeTaskQueueCore.Oo08.\u3007o00\u3007\u3007Oo(value, n2)));
        this.O8.set(this.\u3007o\u3007 & n, null);
        return null;
    }
    
    private final LockFreeTaskQueueCore<E> \u3007o00\u3007\u3007Oo(final long n) {
        final LockFreeTaskQueueCore lockFreeTaskQueueCore = new LockFreeTaskQueueCore(this.\u3007080 * 2, this.\u3007o00\u3007\u3007Oo);
        int n2 = (int)((0x3FFFFFFFL & n) >> 0);
        final int n3 = (int)((0xFFFFFFFC0000000L & n) >> 30);
        while (true) {
            final int \u3007o\u3007 = this.\u3007o\u3007;
            if ((n2 & \u3007o\u3007) == (n3 & \u3007o\u3007)) {
                break;
            }
            Object value;
            if ((value = this.O8.get(\u3007o\u3007 & n2)) == null) {
                value = new Placeholder(n2);
            }
            lockFreeTaskQueueCore.O8.set(lockFreeTaskQueueCore.\u3007o\u3007 & n2, value);
            ++n2;
        }
        LockFreeTaskQueueCore.\u3007\u3007888.set(lockFreeTaskQueueCore, LockFreeTaskQueueCore.Oo08.O8(n, 1152921504606846976L));
        return lockFreeTaskQueueCore;
    }
    
    private final LockFreeTaskQueueCore<E> \u3007o\u3007(final long n) {
        final AtomicReferenceFieldUpdater o\u30070 = LockFreeTaskQueueCore.o\u30070;
        LockFreeTaskQueueCore lockFreeTaskQueueCore;
        while (true) {
            lockFreeTaskQueueCore = o\u30070.get(this);
            if (lockFreeTaskQueueCore != null) {
                break;
            }
            androidx.concurrent.futures.\u3007080.\u3007080(LockFreeTaskQueueCore.o\u30070, this, null, this.\u3007o00\u3007\u3007Oo(n));
        }
        return lockFreeTaskQueueCore;
    }
    
    public final boolean O8() {
        final AtomicLongFieldUpdater \u3007\u3007888 = LockFreeTaskQueueCore.\u3007\u3007888;
        long value;
        do {
            value = \u3007\u3007888.get(this);
            if ((value & 0x2000000000000000L) != 0x0L) {
                return true;
            }
            if ((0x1000000000000000L & value) != 0x0L) {
                return false;
            }
        } while (!\u3007\u3007888.compareAndSet(this, value, value | 0x2000000000000000L));
        return true;
    }
    
    public final Object OO0o\u3007\u3007\u3007\u30070() {
        final AtomicLongFieldUpdater \u3007\u3007888 = LockFreeTaskQueueCore.\u3007\u3007888;
        while (true) {
            final long value = \u3007\u3007888.get(this);
            if ((0x1000000000000000L & value) != 0x0L) {
                return LockFreeTaskQueueCore.oO80;
            }
            final int n = (int)((0x3FFFFFFFL & value) >> 0);
            final int n2 = (int)((0xFFFFFFFC0000000L & value) >> 30);
            final int \u3007o\u3007 = this.\u3007o\u3007;
            if ((n2 & \u3007o\u3007) == (n & \u3007o\u3007)) {
                return null;
            }
            final Object value2 = this.O8.get(\u3007o\u3007 & n);
            if (value2 == null) {
                if (this.\u3007o00\u3007\u3007Oo) {
                    return null;
                }
                continue;
            }
            else {
                if (value2 instanceof Placeholder) {
                    return null;
                }
                final int n3 = n + 1 & 0x3FFFFFFF;
                if (LockFreeTaskQueueCore.\u3007\u3007888.compareAndSet(this, value, LockFreeTaskQueueCore.Oo08.\u3007o00\u3007\u3007Oo(value, n3))) {
                    this.O8.set(this.\u3007o\u3007 & n, null);
                    return value2;
                }
                if (this.\u3007o00\u3007\u3007Oo) {
                    LockFreeTaskQueueCore \u30078o8o\u3007 = this;
                    while ((\u30078o8o\u3007 = \u30078o8o\u3007.\u30078o8o\u3007(n, n3)) != null) {}
                    return value2;
                }
                continue;
            }
        }
    }
    
    public final int o\u30070() {
        final long value = LockFreeTaskQueueCore.\u3007\u3007888.get(this);
        return 0x3FFFFFFF & (int)((value & 0xFFFFFFFC0000000L) >> 30) - (int)((0x3FFFFFFFL & value) >> 0);
    }
    
    public final int \u3007080(@NotNull final E newValue) {
        final AtomicLongFieldUpdater \u3007\u3007888 = LockFreeTaskQueueCore.\u3007\u3007888;
        while (true) {
            final long value = \u3007\u3007888.get(this);
            if ((0x3000000000000000L & value) != 0x0L) {
                return LockFreeTaskQueueCore.Oo08.\u3007080(value);
            }
            final int n = (int)((0x3FFFFFFFL & value) >> 0);
            final int n2 = (int)((0xFFFFFFFC0000000L & value) >> 30);
            final int \u3007o\u3007 = this.\u3007o\u3007;
            if ((n2 + 2 & \u3007o\u3007) == (n & \u3007o\u3007)) {
                return 1;
            }
            if (!this.\u3007o00\u3007\u3007Oo && this.O8.get(n2 & \u3007o\u3007) != null) {
                final int \u3007080 = this.\u3007080;
                if (\u3007080 < 1024 || (n2 - n & 0x3FFFFFFF) > \u3007080 >> 1) {
                    return 1;
                }
                continue;
            }
            else {
                if (LockFreeTaskQueueCore.\u3007\u3007888.compareAndSet(this, value, LockFreeTaskQueueCore.Oo08.\u3007o\u3007(value, n2 + 1 & 0x3FFFFFFF))) {
                    this.O8.set(n2 & \u3007o\u3007, newValue);
                    LockFreeTaskQueueCore<Object> oo08 = (LockFreeTaskQueueCore<Object>)this;
                    while ((LockFreeTaskQueueCore.\u3007\u3007888.get(oo08) & 0x1000000000000000L) != 0x0L && (oo08 = oo08.\u300780\u3007808\u3007O().Oo08(n2, newValue)) != null) {}
                    return 0;
                }
                continue;
            }
        }
    }
    
    @NotNull
    public final LockFreeTaskQueueCore<E> \u300780\u3007808\u3007O() {
        return this.\u3007o\u3007(this.oO80());
    }
    
    public final boolean \u3007\u3007888() {
        final long value = LockFreeTaskQueueCore.\u3007\u3007888.get(this);
        boolean b = false;
        if ((int)((0x3FFFFFFFL & value) >> 0) == (int)((value & 0xFFFFFFFC0000000L) >> 30)) {
            b = true;
        }
        return b;
    }
    
    @Metadata
    public static final class Companion
    {
        private Companion() {
        }
        
        public final long O8(final long n, final long n2) {
            return n & ~n2;
        }
        
        public final int \u3007080(final long n) {
            int n2;
            if ((n & 0x2000000000000000L) != 0x0L) {
                n2 = 2;
            }
            else {
                n2 = 1;
            }
            return n2;
        }
        
        public final long \u3007o00\u3007\u3007Oo(final long n, final int n2) {
            return this.O8(n, 1073741823L) | (long)n2 << 0;
        }
        
        public final long \u3007o\u3007(final long n, final int n2) {
            return this.O8(n, 1152921503533105152L) | (long)n2 << 30;
        }
    }
    
    @Metadata
    public static final class Placeholder
    {
        public final int \u3007080;
        
        public Placeholder(final int \u3007080) {
            this.\u3007080 = \u3007080;
        }
    }
}

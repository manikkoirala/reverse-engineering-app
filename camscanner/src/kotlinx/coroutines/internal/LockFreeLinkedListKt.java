// 
// Decompiled by Procyon v0.6.0
// 

package kotlinx.coroutines.internal;

import kotlin.jvm.internal.Intrinsics;
import org.jetbrains.annotations.NotNull;
import kotlin.Metadata;

@Metadata
public final class LockFreeLinkedListKt
{
    @NotNull
    private static final Object \u3007080;
    
    static {
        \u3007080 = new Symbol("CONDITION_FALSE");
    }
    
    @NotNull
    public static final Object \u3007080() {
        return LockFreeLinkedListKt.\u3007080;
    }
    
    @NotNull
    public static final LockFreeLinkedListNode \u3007o00\u3007\u3007Oo(@NotNull final Object o) {
        Removed removed;
        if (o instanceof Removed) {
            removed = (Removed)o;
        }
        else {
            removed = null;
        }
        LockFreeLinkedListNode \u3007080;
        if (removed == null || (\u3007080 = removed.\u3007080) == null) {
            Intrinsics.o\u30070(o, "null cannot be cast to non-null type kotlinx.coroutines.internal.LockFreeLinkedListNode{ kotlinx.coroutines.internal.LockFreeLinkedListKt.Node }");
            \u3007080 = (LockFreeLinkedListNode)o;
        }
        return \u3007080;
    }
}

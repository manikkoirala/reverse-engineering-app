// 
// Decompiled by Procyon v0.6.0
// 

package kotlinx.coroutines.internal;

import org.jetbrains.annotations.NotNull;
import kotlin.Metadata;

@Metadata
public final class UndeliveredElementException extends RuntimeException
{
    public UndeliveredElementException(@NotNull final String message, @NotNull final Throwable cause) {
        super(message, cause);
    }
}

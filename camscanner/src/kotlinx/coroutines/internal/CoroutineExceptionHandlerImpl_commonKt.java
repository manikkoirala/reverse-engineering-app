// 
// Decompiled by Procyon v0.6.0
// 

package kotlinx.coroutines.internal;

import java.util.Iterator;
import kotlin.ExceptionsKt;
import kotlinx.coroutines.CoroutineExceptionHandlerKt;
import kotlinx.coroutines.CoroutineExceptionHandler;
import org.jetbrains.annotations.NotNull;
import kotlin.coroutines.CoroutineContext;
import kotlin.Metadata;

@Metadata
public final class CoroutineExceptionHandlerImpl_commonKt
{
    public static final void \u3007080(@NotNull final CoroutineContext exceptionSuccessfullyProcessed, @NotNull final Throwable t) {
        for (final CoroutineExceptionHandler coroutineExceptionHandler : CoroutineExceptionHandlerImplKt.\u3007080()) {
            try {
                coroutineExceptionHandler.o800o8O((CoroutineContext)exceptionSuccessfullyProcessed, t);
                continue;
            }
            catch (final ExceptionSuccessfullyProcessed exceptionSuccessfullyProcessed) {
                return;
            }
            finally {
                final Throwable t2;
                CoroutineExceptionHandlerImplKt.\u3007o00\u3007\u3007Oo(CoroutineExceptionHandlerKt.\u3007o00\u3007\u3007Oo(t, t2));
                continue;
            }
            break;
        }
        while (true) {
            try {
                ExceptionsKt.\u3007080(t, (Throwable)new DiagnosticCoroutineContextException((CoroutineContext)exceptionSuccessfullyProcessed));
                CoroutineExceptionHandlerImplKt.\u3007o00\u3007\u3007Oo(t);
            }
            finally {
                continue;
            }
            break;
        }
    }
}

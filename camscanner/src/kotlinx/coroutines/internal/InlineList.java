// 
// Decompiled by Procyon v0.6.0
// 

package kotlinx.coroutines.internal;

import kotlin.jvm.internal.Intrinsics;
import java.util.ArrayList;
import kotlin.jvm.internal.DefaultConstructorMarker;
import org.jetbrains.annotations.NotNull;
import kotlin.Metadata;

@Metadata
public final class InlineList<E>
{
    @NotNull
    public static <E> Object \u3007080(final Object o) {
        return o;
    }
    
    @NotNull
    public static final Object \u3007o\u3007(Object e, final E e2) {
        if (e == null) {
            e = \u3007080(e2);
        }
        else if (e instanceof ArrayList) {
            Intrinsics.o\u30070(e, "null cannot be cast to non-null type java.util.ArrayList<E of kotlinx.coroutines.internal.InlineList>{ kotlin.collections.TypeAliasesKt.ArrayList<E of kotlinx.coroutines.internal.InlineList> }");
            ((ArrayList)e).add(e2);
            e = \u3007080(e);
        }
        else {
            final ArrayList list = new ArrayList(4);
            list.add(e);
            list.add(e2);
            e = \u3007080(list);
        }
        return e;
    }
}

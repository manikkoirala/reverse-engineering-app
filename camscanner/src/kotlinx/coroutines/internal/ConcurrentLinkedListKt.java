// 
// Decompiled by Procyon v0.6.0
// 

package kotlinx.coroutines.internal;

import kotlin.jvm.functions.Function2;
import org.jetbrains.annotations.NotNull;
import kotlin.Metadata;

@Metadata
public final class ConcurrentLinkedListKt
{
    @NotNull
    private static final Symbol \u3007080;
    
    static {
        \u3007080 = new Symbol("CLOSED");
    }
    
    public static final /* synthetic */ Symbol \u3007080() {
        return ConcurrentLinkedListKt.\u3007080;
    }
    
    @NotNull
    public static final <N extends ConcurrentLinkedListNode<N>> N \u3007o00\u3007\u3007Oo(@NotNull N n) {
        while (true) {
            final Object \u3007080 = ConcurrentLinkedListNode.\u3007080((ConcurrentLinkedListNode<ConcurrentLinkedListNode>)n);
            if (\u3007080 == \u3007080()) {
                return n;
            }
            final ConcurrentLinkedListNode<N> concurrentLinkedListNode = (N)\u3007080;
            if (concurrentLinkedListNode == null) {
                if (n.OO0o\u3007\u3007\u3007\u30070()) {
                    return n;
                }
                continue;
            }
            else {
                n = (N)concurrentLinkedListNode;
            }
        }
    }
    
    @NotNull
    public static final <S extends Segment<S>> Object \u3007o\u3007(@NotNull final S n, final long n2, @NotNull final Function2<? super Long, ? super S, ? extends S> function2) {
        Segment<S> segment = n;
        while (segment.OO < n2 || segment.oO80()) {
            final Object \u3007080 = ConcurrentLinkedListNode.\u3007080((ConcurrentLinkedListNode<ConcurrentLinkedListNode>)segment);
            if (\u3007080 == \u3007080()) {
                return SegmentOrClosed.\u3007080(ConcurrentLinkedListKt.\u3007080);
            }
            Segment segment2 = (Segment)\u3007080;
            if (segment2 == null) {
                final Segment segment3 = (Segment)function2.invoke((Object)(segment.OO + 1L), (Object)segment);
                if (!segment.\u3007O8o08O((ConcurrentLinkedListNode)segment3)) {
                    continue;
                }
                segment2 = segment3;
                if (segment.oO80()) {
                    segment.\u30078o8o\u3007();
                    segment2 = segment3;
                }
            }
            segment = segment2;
        }
        return SegmentOrClosed.\u3007080(segment);
    }
}

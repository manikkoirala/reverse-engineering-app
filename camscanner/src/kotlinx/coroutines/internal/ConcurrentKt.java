// 
// Decompiled by Procyon v0.6.0
// 

package kotlinx.coroutines.internal;

import org.jetbrains.annotations.NotNull;
import java.util.concurrent.Executor;
import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.lang.reflect.Method;
import kotlin.Metadata;

@Metadata
public final class ConcurrentKt
{
    private static final Method \u3007080;
    
    static {
        Method \u300781;
        try {
            ScheduledThreadPoolExecutor.class.getMethod("setRemoveOnCancelPolicy", Boolean.TYPE);
        }
        finally {
            \u300781 = null;
        }
        \u3007080 = \u300781;
    }
    
    public static final boolean \u3007080(@NotNull final Executor executor) {
        try {
            ScheduledThreadPoolExecutor obj;
            if (executor instanceof ScheduledThreadPoolExecutor) {
                obj = (ScheduledThreadPoolExecutor)executor;
            }
            else {
                obj = null;
            }
            if (obj == null) {}
            final Method \u3007080 = ConcurrentKt.\u3007080;
            if (\u3007080 == null) {}
            \u3007080.invoke(obj, Boolean.TRUE);
            return true;
        }
        finally {
            return false;
        }
    }
}

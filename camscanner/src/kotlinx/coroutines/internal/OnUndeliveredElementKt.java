// 
// Decompiled by Procyon v0.6.0
// 

package kotlinx.coroutines.internal;

import kotlin.ExceptionsKt;
import kotlinx.coroutines.CoroutineExceptionHandlerKt;
import kotlin.coroutines.CoroutineContext;
import org.jetbrains.annotations.NotNull;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
import kotlin.Metadata;

@Metadata
public final class OnUndeliveredElementKt
{
    @NotNull
    public static final <E> Function1<Throwable, Unit> \u3007080(@NotNull final Function1<? super E, Unit> function1, final E e, @NotNull final CoroutineContext coroutineContext) {
        return (Function1<Throwable, Unit>)new OnUndeliveredElementKt$bindCancellationFun.OnUndeliveredElementKt$bindCancellationFun$1((Function1)function1, (Object)e, coroutineContext);
    }
    
    public static final <E> void \u3007o00\u3007\u3007Oo(@NotNull final Function1<? super E, Unit> function1, final E e, @NotNull final CoroutineContext coroutineContext) {
        final UndeliveredElementException \u3007o\u3007 = \u3007o\u3007(function1, e, null);
        if (\u3007o\u3007 != null) {
            CoroutineExceptionHandlerKt.\u3007080(coroutineContext, (Throwable)\u3007o\u3007);
        }
    }
    
    public static final <E> UndeliveredElementException \u3007o\u3007(@NotNull final Function1<? super E, Unit> function1, final E obj, final UndeliveredElementException ex) {
        final Throwable t;
        Label_0031: {
            try {
                function1.invoke((Object)obj);
            }
            finally {
                if (ex == null || ex.getCause() == t) {
                    break Label_0031;
                }
                ExceptionsKt.\u3007080((Throwable)ex, t);
            }
            return ex;
        }
        final StringBuilder sb = new StringBuilder();
        sb.append("Exception in undelivered element handler for ");
        sb.append(obj);
        return new UndeliveredElementException(sb.toString(), t);
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package kotlinx.coroutines.internal;

import kotlin.Result$Companion;
import kotlin.ResultKt;
import kotlin.Result;
import kotlin.Metadata;

@Metadata
public final class FastServiceLoaderKt
{
    private static final boolean \u3007080;
    
    static {
        Object constructor-impl = null;
        try {
            final Result$Companion companion = Result.Companion;
            Result.constructor-impl((Object)Class.forName("android.os.Build"));
        }
        finally {
            final Result$Companion companion2 = Result.Companion;
            final Throwable t;
            constructor-impl = Result.constructor-impl(ResultKt.\u3007080(t));
        }
        \u3007080 = Result.isSuccess-impl(constructor-impl);
    }
    
    public static final boolean \u3007080() {
        return FastServiceLoaderKt.\u3007080;
    }
}

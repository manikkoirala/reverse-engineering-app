// 
// Decompiled by Procyon v0.6.0
// 

package kotlinx.coroutines.internal;

import org.jetbrains.annotations.NotNull;
import kotlin.Metadata;

@Metadata
public final class ThreadLocalKt
{
    @NotNull
    public static final <T> ThreadLocal<T> \u3007080(@NotNull final Symbol symbol) {
        return new ThreadLocal<T>();
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package kotlinx.coroutines.internal;

import kotlinx.coroutines.MainCoroutineDispatcher;
import org.jetbrains.annotations.NotNull;
import java.util.List;
import kotlin.Metadata;

@Metadata
public interface MainDispatcherFactory
{
    int \u3007080();
    
    String \u3007o00\u3007\u3007Oo();
    
    @NotNull
    MainCoroutineDispatcher \u3007o\u3007(@NotNull final List<? extends MainDispatcherFactory> p0);
}

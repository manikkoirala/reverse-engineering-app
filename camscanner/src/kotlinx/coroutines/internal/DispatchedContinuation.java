// 
// Decompiled by Procyon v0.6.0
// 

package kotlinx.coroutines.internal;

import kotlinx.coroutines.EventLoop;
import kotlin.jvm.internal.Intrinsics;
import kotlinx.coroutines.CompletedWithCancellation;
import kotlinx.coroutines.CancellableContinuation;
import kotlinx.coroutines.DebugStringsKt;
import kotlin.Unit;
import kotlinx.coroutines.ThreadLocalEventLoop;
import kotlin.jvm.functions.Function1;
import kotlinx.coroutines.CompletionStateKt;
import kotlin.coroutines.CoroutineContext;
import androidx.concurrent.futures.\u3007080;
import kotlinx.coroutines.CancellableContinuationImpl;
import kotlinx.coroutines.CoroutineDispatcher;
import org.jetbrains.annotations.NotNull;
import java.util.concurrent.atomic.AtomicReferenceFieldUpdater;
import kotlin.Metadata;
import kotlin.coroutines.Continuation;
import kotlin.coroutines.jvm.internal.CoroutineStackFrame;
import kotlinx.coroutines.DispatchedTask;

@Metadata
public final class DispatchedContinuation<T> extends DispatchedTask<T> implements CoroutineStackFrame, Continuation<T>
{
    @NotNull
    private static final AtomicReferenceFieldUpdater \u30070O;
    public Object O8o08O8O;
    private volatile Object _reusableCancellableContinuation;
    @NotNull
    public final Continuation<T> o\u300700O;
    @NotNull
    public final Object \u3007080OO8\u30070;
    @NotNull
    public final CoroutineDispatcher \u300708O\u300700\u3007o;
    
    static {
        \u30070O = AtomicReferenceFieldUpdater.newUpdater(DispatchedContinuation.class, Object.class, "_reusableCancellableContinuation");
    }
    
    public DispatchedContinuation(@NotNull final CoroutineDispatcher \u300708O\u300700\u3007o, @NotNull final Continuation<? super T> o\u300700O) {
        super(-1);
        this.\u300708O\u300700\u3007o = \u300708O\u300700\u3007o;
        this.o\u300700O = (Continuation<T>)o\u300700O;
        this.O8o08O8O = DispatchedContinuationKt.\u3007080();
        this.\u3007080OO8\u30070 = ThreadContextKt.\u3007o00\u3007\u3007Oo(this.getContext());
    }
    
    private final CancellableContinuationImpl<?> \u3007O8o08O() {
        final CancellableContinuationImpl value = DispatchedContinuation.\u30070O.get(this);
        CancellableContinuationImpl cancellableContinuationImpl;
        if (value instanceof CancellableContinuationImpl) {
            cancellableContinuationImpl = value;
        }
        else {
            cancellableContinuationImpl = null;
        }
        return (CancellableContinuationImpl<?>)cancellableContinuationImpl;
    }
    
    @NotNull
    public Continuation<T> O8() {
        return (Continuation<T>)this;
    }
    
    public final boolean OO0o\u3007\u3007() {
        return DispatchedContinuation.\u30070O.get(this) != null;
    }
    
    public final CancellableContinuationImpl<T> OO0o\u3007\u3007\u3007\u30070() {
        final AtomicReferenceFieldUpdater \u30070O = DispatchedContinuation.\u30070O;
        while (true) {
            final Object value = \u30070O.get(this);
            if (value == null) {
                DispatchedContinuation.\u30070O.set(this, DispatchedContinuationKt.\u3007o00\u3007\u3007Oo);
                return null;
            }
            if (value instanceof CancellableContinuationImpl) {
                if (\u3007080.\u3007080(DispatchedContinuation.\u30070O, this, value, DispatchedContinuationKt.\u3007o00\u3007\u3007Oo)) {
                    return (CancellableContinuationImpl<T>)value;
                }
                continue;
            }
            else {
                if (value == DispatchedContinuationKt.\u3007o00\u3007\u3007Oo) {
                    continue;
                }
                if (value instanceof Throwable) {
                    continue;
                }
                final StringBuilder sb = new StringBuilder();
                sb.append("Inconsistent state ");
                sb.append(value);
                throw new IllegalStateException(sb.toString().toString());
            }
        }
    }
    
    public CoroutineStackFrame getCallerFrame() {
        final Continuation<T> o\u300700O = this.o\u300700O;
        CoroutineStackFrame coroutineStackFrame;
        if (o\u300700O instanceof CoroutineStackFrame) {
            coroutineStackFrame = (CoroutineStackFrame)o\u300700O;
        }
        else {
            coroutineStackFrame = null;
        }
        return coroutineStackFrame;
    }
    
    @NotNull
    public CoroutineContext getContext() {
        return this.o\u300700O.getContext();
    }
    
    public StackTraceElement getStackTraceElement() {
        return null;
    }
    
    public Object oO80() {
        final Object o8o08O8O = this.O8o08O8O;
        this.O8o08O8O = DispatchedContinuationKt.\u3007080();
        return o8o08O8O;
    }
    
    public void resumeWith(@NotNull final Object o) {
        Object o2 = this.o\u300700O.getContext();
        final Object o3 = CompletionStateKt.O8(o, (Function1)null, 1, (Object)null);
        if (this.\u300708O\u300700\u3007o.isDispatchNeeded((CoroutineContext)o2)) {
            this.O8o08O8O = o3;
            super.OO = 0;
            this.\u300708O\u300700\u3007o.dispatch((CoroutineContext)o2, (Runnable)this);
            return;
        }
        o2 = ThreadLocalEventLoop.\u3007080.\u3007080();
        if (((EventLoop)o2).o\u3007O()) {
            this.O8o08O8O = o3;
            super.OO = 0;
            ((EventLoop)o2).\u30078\u30070\u3007o\u3007O((DispatchedTask)this);
            return;
        }
        ((EventLoop)o2).o8oO\u3007(true);
        final Throwable t2;
        try {
            final CoroutineContext context = this.getContext();
            final Object \u3007o\u3007 = ThreadContextKt.\u3007o\u3007(context, this.\u3007080OO8\u30070);
            try {
                this.o\u300700O.resumeWith(o);
                final Unit \u3007080 = Unit.\u3007080;
                ThreadContextKt.\u3007080(context, \u3007o\u3007);
                while (((EventLoop)o2).O000()) {}
            }
            finally {
                ThreadContextKt.\u3007080(context, \u3007o\u3007);
            }
        }
        finally {
            final DispatchedContinuation dispatchedContinuation = this;
            final Throwable t = t2;
            final Throwable t3 = null;
            dispatchedContinuation.\u3007\u3007888(t, t3);
        }
        try {
            final DispatchedContinuation dispatchedContinuation = this;
            final Throwable t = t2;
            final Throwable t3 = null;
            dispatchedContinuation.\u3007\u3007888(t, t3);
        }
        finally {
            ((EventLoop)o2).\u30078(true);
        }
    }
    
    @NotNull
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append("DispatchedContinuation[");
        sb.append(this.\u300708O\u300700\u3007o);
        sb.append(", ");
        sb.append(DebugStringsKt.\u3007o\u3007((Continuation)this.o\u300700O));
        sb.append(']');
        return sb.toString();
    }
    
    public final void \u300780\u3007808\u3007O() {
        while (DispatchedContinuation.\u30070O.get(this) == DispatchedContinuationKt.\u3007o00\u3007\u3007Oo) {}
    }
    
    public final void \u30078o8o\u3007(@NotNull final CoroutineContext coroutineContext, final T o8o08O8O) {
        this.O8o08O8O = o8o08O8O;
        super.OO = 1;
        this.\u300708O\u300700\u3007o.dispatchYield(coroutineContext, (Runnable)this);
    }
    
    public final Throwable \u3007O00(@NotNull final CancellableContinuation<?> cancellableContinuation) {
        final AtomicReferenceFieldUpdater \u30070O = DispatchedContinuation.\u30070O;
        Symbol \u3007o00\u3007\u3007Oo;
        do {
            final Object value = \u30070O.get(this);
            \u3007o00\u3007\u3007Oo = DispatchedContinuationKt.\u3007o00\u3007\u3007Oo;
            if (value == \u3007o00\u3007\u3007Oo) {
                continue;
            }
            if (!(value instanceof Throwable)) {
                final StringBuilder sb = new StringBuilder();
                sb.append("Inconsistent state ");
                sb.append(value);
                throw new IllegalStateException(sb.toString().toString());
            }
            if (\u3007080.\u3007080(DispatchedContinuation.\u30070O, this, value, null)) {
                return (Throwable)value;
            }
            throw new IllegalArgumentException("Failed requirement.".toString());
        } while (!\u3007080.\u3007080(DispatchedContinuation.\u30070O, this, \u3007o00\u3007\u3007Oo, cancellableContinuation));
        return null;
    }
    
    public final void \u3007O\u3007() {
        this.\u300780\u3007808\u3007O();
        final CancellableContinuationImpl<?> \u3007o8o08O = this.\u3007O8o08O();
        if (\u3007o8o08O != null) {
            \u3007o8o08O.\u3007O\u3007();
        }
    }
    
    public void \u3007o\u3007(final Object o, @NotNull final Throwable t) {
        if (o instanceof CompletedWithCancellation) {
            ((CompletedWithCancellation)o).\u3007o00\u3007\u3007Oo.invoke((Object)t);
        }
    }
    
    public final boolean \u3007\u3007808\u3007(@NotNull final Throwable t) {
        final AtomicReferenceFieldUpdater \u30070O = DispatchedContinuation.\u30070O;
        while (true) {
            final Object value = \u30070O.get(this);
            final Symbol \u3007o00\u3007\u3007Oo = DispatchedContinuationKt.\u3007o00\u3007\u3007Oo;
            if (Intrinsics.\u3007o\u3007(value, (Object)\u3007o00\u3007\u3007Oo)) {
                if (\u3007080.\u3007080(DispatchedContinuation.\u30070O, this, \u3007o00\u3007\u3007Oo, t)) {
                    return true;
                }
                continue;
            }
            else {
                if (value instanceof Throwable) {
                    return true;
                }
                if (\u3007080.\u3007080(DispatchedContinuation.\u30070O, this, value, null)) {
                    return false;
                }
                continue;
            }
        }
    }
}

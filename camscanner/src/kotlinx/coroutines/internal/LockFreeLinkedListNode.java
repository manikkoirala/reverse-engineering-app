// 
// Decompiled by Procyon v0.6.0
// 

package kotlinx.coroutines.internal;

import kotlinx.coroutines.DebugStringsKt;
import kotlin.jvm.internal.Intrinsics;
import androidx.concurrent.futures.\u3007080;
import org.jetbrains.annotations.NotNull;
import java.util.concurrent.atomic.AtomicReferenceFieldUpdater;
import kotlin.Metadata;

@Metadata
public class LockFreeLinkedListNode
{
    @NotNull
    private static final AtomicReferenceFieldUpdater OO;
    @NotNull
    private static final AtomicReferenceFieldUpdater o0;
    @NotNull
    private static final AtomicReferenceFieldUpdater \u3007OOo8\u30070;
    private volatile Object _next;
    private volatile Object _prev;
    private volatile Object _removedRef;
    
    static {
        o0 = AtomicReferenceFieldUpdater.newUpdater(LockFreeLinkedListNode.class, Object.class, "_next");
        \u3007OOo8\u30070 = AtomicReferenceFieldUpdater.newUpdater(LockFreeLinkedListNode.class, Object.class, "_prev");
        OO = AtomicReferenceFieldUpdater.newUpdater(LockFreeLinkedListNode.class, Object.class, "_removedRef");
    }
    
    public LockFreeLinkedListNode() {
        this._next = this;
        this._prev = this;
    }
    
    private final LockFreeLinkedListNode Oo08(final OpDescriptor opDescriptor) {
    Label_0000:
        while (true) {
            while (true) {
                LockFreeLinkedListNode lockFreeLinkedListNode2;
                final LockFreeLinkedListNode lockFreeLinkedListNode = lockFreeLinkedListNode2 = LockFreeLinkedListNode.\u3007OOo8\u30070.get(this);
                while (true) {
                    LockFreeLinkedListNode lockFreeLinkedListNode3 = null;
                    while (true) {
                        final AtomicReferenceFieldUpdater o0 = LockFreeLinkedListNode.o0;
                        final Object value = o0.get(lockFreeLinkedListNode2);
                        if (value == this) {
                            if (lockFreeLinkedListNode == lockFreeLinkedListNode2) {
                                return lockFreeLinkedListNode2;
                            }
                            if (!\u3007080.\u3007080(LockFreeLinkedListNode.\u3007OOo8\u30070, this, lockFreeLinkedListNode, lockFreeLinkedListNode2)) {
                                continue Label_0000;
                            }
                            return lockFreeLinkedListNode2;
                        }
                        else {
                            if (this.\u3007O8o08O()) {
                                return null;
                            }
                            if (value == opDescriptor) {
                                return lockFreeLinkedListNode2;
                            }
                            if (value instanceof OpDescriptor) {
                                ((OpDescriptor)value).\u3007080(lockFreeLinkedListNode2);
                                continue Label_0000;
                            }
                            if (value instanceof Removed) {
                                if (lockFreeLinkedListNode3 != null) {
                                    if (!\u3007080.\u3007080(o0, lockFreeLinkedListNode3, lockFreeLinkedListNode2, ((Removed)value).\u3007080)) {
                                        continue Label_0000;
                                    }
                                    lockFreeLinkedListNode2 = lockFreeLinkedListNode3;
                                    break;
                                }
                                else {
                                    lockFreeLinkedListNode2 = (LockFreeLinkedListNode)LockFreeLinkedListNode.\u3007OOo8\u30070.get(lockFreeLinkedListNode2);
                                }
                            }
                            else {
                                Intrinsics.o\u30070(value, "null cannot be cast to non-null type kotlinx.coroutines.internal.LockFreeLinkedListNode{ kotlinx.coroutines.internal.LockFreeLinkedListKt.Node }");
                                final LockFreeLinkedListNode lockFreeLinkedListNode4 = (LockFreeLinkedListNode)value;
                                lockFreeLinkedListNode3 = lockFreeLinkedListNode2;
                                lockFreeLinkedListNode2 = lockFreeLinkedListNode4;
                            }
                        }
                    }
                }
            }
            break;
        }
    }
    
    private final LockFreeLinkedListNode o\u30070(LockFreeLinkedListNode lockFreeLinkedListNode) {
        while (lockFreeLinkedListNode.\u3007O8o08O()) {
            lockFreeLinkedListNode = LockFreeLinkedListNode.\u3007OOo8\u30070.get(lockFreeLinkedListNode);
        }
        return lockFreeLinkedListNode;
    }
    
    public static final /* synthetic */ AtomicReferenceFieldUpdater \u3007o\u3007() {
        return LockFreeLinkedListNode.o0;
    }
    
    private final Removed \u3007\u3007808\u3007() {
        final AtomicReferenceFieldUpdater oo = LockFreeLinkedListNode.OO;
        Removed removed;
        if ((removed = oo.get(this)) == null) {
            removed = new Removed(this);
            oo.lazySet(this, removed);
        }
        return removed;
    }
    
    private final void \u3007\u3007888(final LockFreeLinkedListNode lockFreeLinkedListNode) {
        final AtomicReferenceFieldUpdater \u3007oOo8\u30070 = LockFreeLinkedListNode.\u3007OOo8\u30070;
        LockFreeLinkedListNode lockFreeLinkedListNode2;
        do {
            lockFreeLinkedListNode2 = \u3007oOo8\u30070.get(lockFreeLinkedListNode);
            if (this.oO80() != lockFreeLinkedListNode) {
                return;
            }
        } while (!\u3007080.\u3007080(LockFreeLinkedListNode.\u3007OOo8\u30070, lockFreeLinkedListNode, lockFreeLinkedListNode2, this));
        if (this.\u3007O8o08O()) {
            lockFreeLinkedListNode.Oo08(null);
        }
    }
    
    public final boolean O8(@NotNull final LockFreeLinkedListNode lockFreeLinkedListNode) {
        LockFreeLinkedListNode.\u3007OOo8\u30070.lazySet(lockFreeLinkedListNode, this);
        LockFreeLinkedListNode.o0.lazySet(lockFreeLinkedListNode, this);
        while (this.oO80() == this) {
            if (\u3007080.\u3007080(LockFreeLinkedListNode.o0, this, this, lockFreeLinkedListNode)) {
                lockFreeLinkedListNode.\u3007\u3007888(this);
                return true;
            }
        }
        return false;
    }
    
    public boolean OO0o\u3007\u3007() {
        return this.Oooo8o0\u3007() == null;
    }
    
    @NotNull
    public final LockFreeLinkedListNode OO0o\u3007\u3007\u3007\u30070() {
        return LockFreeLinkedListKt.\u3007o00\u3007\u3007Oo(this.oO80());
    }
    
    public final LockFreeLinkedListNode Oooo8o0\u3007() {
        Object oo80;
        LockFreeLinkedListNode lockFreeLinkedListNode;
        do {
            oo80 = this.oO80();
            if (oo80 instanceof Removed) {
                return ((Removed)oo80).\u3007080;
            }
            if (oo80 == this) {
                return (LockFreeLinkedListNode)oo80;
            }
            Intrinsics.o\u30070(oo80, "null cannot be cast to non-null type kotlinx.coroutines.internal.LockFreeLinkedListNode{ kotlinx.coroutines.internal.LockFreeLinkedListKt.Node }");
            lockFreeLinkedListNode = (LockFreeLinkedListNode)oo80;
        } while (!\u3007080.\u3007080(LockFreeLinkedListNode.o0, this, oo80, lockFreeLinkedListNode.\u3007\u3007808\u3007()));
        lockFreeLinkedListNode.Oo08(null);
        return null;
    }
    
    @NotNull
    public final Object oO80() {
        final AtomicReferenceFieldUpdater o0 = LockFreeLinkedListNode.o0;
        Object value;
        while (true) {
            value = o0.get(this);
            if (!(value instanceof OpDescriptor)) {
                break;
            }
            ((OpDescriptor)value).\u3007080(this);
        }
        return value;
    }
    
    @NotNull
    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append(new LockFreeLinkedListNode$toString.LockFreeLinkedListNode$toString$1((Object)this));
        sb.append('@');
        sb.append(DebugStringsKt.\u3007o00\u3007\u3007Oo((Object)this));
        return sb.toString();
    }
    
    @NotNull
    public final LockFreeLinkedListNode \u30078o8o\u3007() {
        LockFreeLinkedListNode lockFreeLinkedListNode;
        if ((lockFreeLinkedListNode = this.Oo08(null)) == null) {
            lockFreeLinkedListNode = this.o\u30070(LockFreeLinkedListNode.\u3007OOo8\u30070.get(this));
        }
        return lockFreeLinkedListNode;
    }
    
    public boolean \u3007O8o08O() {
        return this.oO80() instanceof Removed;
    }
    
    public final int \u3007O\u3007(@NotNull final LockFreeLinkedListNode lockFreeLinkedListNode, @NotNull final LockFreeLinkedListNode \u3007o\u3007, @NotNull final CondAddOp condAddOp) {
        LockFreeLinkedListNode.\u3007OOo8\u30070.lazySet(lockFreeLinkedListNode, this);
        final AtomicReferenceFieldUpdater o0 = LockFreeLinkedListNode.o0;
        o0.lazySet(lockFreeLinkedListNode, \u3007o\u3007);
        condAddOp.\u3007o\u3007 = \u3007o\u3007;
        if (!\u3007080.\u3007080(o0, this, \u3007o\u3007, condAddOp)) {
            return 0;
        }
        int n;
        if (condAddOp.\u3007080(this) == null) {
            n = 1;
        }
        else {
            n = 2;
        }
        return n;
    }
    
    @Metadata
    public abstract static class CondAddOp extends AtomicOp<LockFreeLinkedListNode>
    {
        @NotNull
        public final LockFreeLinkedListNode \u3007o00\u3007\u3007Oo;
        public LockFreeLinkedListNode \u3007o\u3007;
        
        public CondAddOp(@NotNull final LockFreeLinkedListNode \u3007o00\u3007\u3007Oo) {
            this.\u3007o00\u3007\u3007Oo = \u3007o00\u3007\u3007Oo;
        }
        
        public void Oo08(@NotNull LockFreeLinkedListNode \u3007o\u3007, final Object o) {
            final boolean b = o == null;
            LockFreeLinkedListNode lockFreeLinkedListNode;
            if (b) {
                lockFreeLinkedListNode = this.\u3007o00\u3007\u3007Oo;
            }
            else {
                lockFreeLinkedListNode = this.\u3007o\u3007;
            }
            if (lockFreeLinkedListNode != null && androidx.concurrent.futures.\u3007080.\u3007080(LockFreeLinkedListNode.\u3007o\u3007(), \u3007o\u3007, this, lockFreeLinkedListNode) && b) {
                final LockFreeLinkedListNode \u3007o00\u3007\u3007Oo = this.\u3007o00\u3007\u3007Oo;
                \u3007o\u3007 = this.\u3007o\u3007;
                Intrinsics.Oo08((Object)\u3007o\u3007);
                \u3007o00\u3007\u3007Oo.\u3007\u3007888(\u3007o\u3007);
            }
        }
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package kotlinx.coroutines.internal;

import kotlin.Metadata;

@Metadata
public final class LimitedDispatcherKt
{
    public static final void \u3007080(final int i) {
        boolean b = true;
        if (i < 1) {
            b = false;
        }
        if (b) {
            return;
        }
        final StringBuilder sb = new StringBuilder();
        sb.append("Expected positive parallelism level, but got ");
        sb.append(i);
        throw new IllegalArgumentException(sb.toString().toString());
    }
}

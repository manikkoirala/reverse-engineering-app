// 
// Decompiled by Procyon v0.6.0
// 

package kotlinx.coroutines.internal;

import kotlin.Metadata;

@Metadata
public interface ThreadSafeHeapNode
{
    int getIndex();
    
    void setIndex(final int p0);
    
    void \u3007080(final ThreadSafeHeap<?> p0);
    
    ThreadSafeHeap<?> \u3007o\u3007();
}

// 
// Decompiled by Procyon v0.6.0
// 

package kotlinx.coroutines.internal;

import org.jetbrains.annotations.NotNull;
import kotlin.Metadata;

@Metadata
final class Removed
{
    @NotNull
    public final LockFreeLinkedListNode \u3007080;
    
    public Removed(@NotNull final LockFreeLinkedListNode \u3007080) {
        this.\u3007080 = \u3007080;
    }
    
    @NotNull
    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append("Removed[");
        sb.append(this.\u3007080);
        sb.append(']');
        return sb.toString();
    }
}

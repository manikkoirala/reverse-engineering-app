// 
// Decompiled by Procyon v0.6.0
// 

package kotlinx.coroutines.internal;

import kotlin.jvm.functions.Function1;
import kotlin.coroutines.intrinsics.IntrinsicsKt;
import kotlinx.coroutines.CompletionStateKt;
import kotlin.coroutines.CoroutineContext;
import org.jetbrains.annotations.NotNull;
import kotlin.coroutines.Continuation;
import kotlin.Metadata;
import kotlin.coroutines.jvm.internal.CoroutineStackFrame;
import kotlinx.coroutines.AbstractCoroutine;

@Metadata
public class ScopeCoroutine<T> extends AbstractCoroutine<T> implements CoroutineStackFrame
{
    @NotNull
    public final Continuation<T> \u300708O\u300700\u3007o;
    
    public ScopeCoroutine(@NotNull final CoroutineContext coroutineContext, @NotNull final Continuation<? super T> \u300708O\u300700\u3007o) {
        super(coroutineContext, true, true);
        this.\u300708O\u300700\u3007o = (Continuation<T>)\u300708O\u300700\u3007o;
    }
    
    protected void O\u300708(final Object o) {
        final Continuation<T> \u300708O\u300700\u3007o = this.\u300708O\u300700\u3007o;
        \u300708O\u300700\u3007o.resumeWith(CompletionStateKt.\u3007080(o, (Continuation)\u300708O\u300700\u3007o));
    }
    
    public final CoroutineStackFrame getCallerFrame() {
        final Continuation<T> \u300708O\u300700\u3007o = this.\u300708O\u300700\u3007o;
        CoroutineStackFrame coroutineStackFrame;
        if (\u300708O\u300700\u3007o instanceof CoroutineStackFrame) {
            coroutineStackFrame = (CoroutineStackFrame)\u300708O\u300700\u3007o;
        }
        else {
            coroutineStackFrame = null;
        }
        return coroutineStackFrame;
    }
    
    public final StackTraceElement getStackTraceElement() {
        return null;
    }
    
    protected final boolean o0O0() {
        return true;
    }
    
    protected void oo\u3007(final Object o) {
        DispatchedContinuationKt.\u3007o\u3007(IntrinsicsKt.\u3007o\u3007((Continuation)this.\u300708O\u300700\u3007o), CompletionStateKt.\u3007080(o, (Continuation)this.\u300708O\u300700\u3007o), null, 2, null);
    }
}

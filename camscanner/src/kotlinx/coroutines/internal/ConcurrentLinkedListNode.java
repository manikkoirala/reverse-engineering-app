// 
// Decompiled by Procyon v0.6.0
// 

package kotlinx.coroutines.internal;

import androidx.concurrent.futures.\u3007080;
import kotlin.jvm.internal.Intrinsics;
import org.jetbrains.annotations.NotNull;
import java.util.concurrent.atomic.AtomicReferenceFieldUpdater;
import kotlin.Metadata;

@Metadata
public abstract class ConcurrentLinkedListNode<N extends ConcurrentLinkedListNode<N>>
{
    @NotNull
    private static final AtomicReferenceFieldUpdater o0;
    @NotNull
    private static final AtomicReferenceFieldUpdater \u3007OOo8\u30070;
    private volatile Object _next;
    private volatile Object _prev;
    
    static {
        o0 = AtomicReferenceFieldUpdater.newUpdater(ConcurrentLinkedListNode.class, Object.class, "_next");
        \u3007OOo8\u30070 = AtomicReferenceFieldUpdater.newUpdater(ConcurrentLinkedListNode.class, Object.class, "_prev");
    }
    
    public ConcurrentLinkedListNode(final N prev) {
        this._prev = prev;
    }
    
    private final N O8() {
        ConcurrentLinkedListNode<N> oo08 = this.Oo08();
        Intrinsics.Oo08((Object)oo08);
        while (oo08.oO80()) {
            final ConcurrentLinkedListNode oo9 = oo08.Oo08();
            if (oo9 == null) {
                return (N)oo08;
            }
            oo08 = oo9;
        }
        return (N)oo08;
    }
    
    private final Object o\u30070() {
        return ConcurrentLinkedListNode.o0.get(this);
    }
    
    private final N \u3007o\u3007() {
        ConcurrentLinkedListNode<N> \u3007\u3007888;
        for (\u3007\u3007888 = this.\u3007\u3007888(); \u3007\u3007888 != null && \u3007\u3007888.oO80(); \u3007\u3007888 = (N)ConcurrentLinkedListNode.\u3007OOo8\u30070.get(\u3007\u3007888)) {}
        return (N)\u3007\u3007888;
    }
    
    public final boolean OO0o\u3007\u3007\u3007\u30070() {
        return \u3007080.\u3007080(ConcurrentLinkedListNode.o0, this, null, ConcurrentLinkedListKt.\u3007080());
    }
    
    public final N Oo08() {
        final Object \u3007080 = this.o\u30070();
        if (\u3007080 == ConcurrentLinkedListKt.\u3007080()) {
            return null;
        }
        return (N)\u3007080;
    }
    
    public abstract boolean oO80();
    
    public final boolean \u300780\u3007808\u3007O() {
        return this.Oo08() == null;
    }
    
    public final void \u30078o8o\u3007() {
        if (this.\u300780\u3007808\u3007O()) {
            return;
        }
        ConcurrentLinkedListNode<N> o8;
        ConcurrentLinkedListNode<N> \u3007o\u3007;
        do {
            \u3007o\u3007 = this.\u3007o\u3007();
            o8 = this.O8();
            final AtomicReferenceFieldUpdater \u3007oOo8\u30070 = ConcurrentLinkedListNode.\u3007OOo8\u30070;
            Object value;
            Object o9;
            do {
                value = \u3007oOo8\u30070.get(o8);
                if (value == null) {
                    o9 = null;
                }
                else {
                    o9 = \u3007o\u3007;
                }
            } while (!\u3007080.\u3007080(\u3007oOo8\u30070, o8, value, o9));
            if (\u3007o\u3007 != null) {
                ConcurrentLinkedListNode.o0.set(\u3007o\u3007, o8);
            }
        } while ((o8.oO80() && !o8.\u300780\u3007808\u3007O()) || (\u3007o\u3007 != null && \u3007o\u3007.oO80()));
    }
    
    public final boolean \u3007O8o08O(@NotNull final N n) {
        return \u3007080.\u3007080(ConcurrentLinkedListNode.o0, this, null, n);
    }
    
    public final void \u3007o00\u3007\u3007Oo() {
        ConcurrentLinkedListNode.\u3007OOo8\u30070.lazySet(this, null);
    }
    
    public final N \u3007\u3007888() {
        return ConcurrentLinkedListNode.\u3007OOo8\u30070.get(this);
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package kotlinx.coroutines.internal;

import kotlin.jvm.internal.Intrinsics;
import kotlinx.coroutines.ThreadContextElement;
import org.jetbrains.annotations.NotNull;
import kotlin.coroutines.CoroutineContext;
import kotlin.Metadata;

@Metadata
final class ThreadState
{
    private int O8;
    @NotNull
    public final CoroutineContext \u3007080;
    @NotNull
    private final Object[] \u3007o00\u3007\u3007Oo;
    @NotNull
    private final ThreadContextElement<Object>[] \u3007o\u3007;
    
    public ThreadState(@NotNull final CoroutineContext \u3007080, final int n) {
        this.\u3007080 = \u3007080;
        this.\u3007o00\u3007\u3007Oo = new Object[n];
        this.\u3007o\u3007 = (ThreadContextElement<Object>[])new ThreadContextElement[n];
    }
    
    public final void \u3007080(@NotNull final ThreadContextElement<?> threadContextElement, final Object o) {
        final Object[] \u3007o00\u3007\u3007Oo = this.\u3007o00\u3007\u3007Oo;
        final int o2 = this.O8;
        \u3007o00\u3007\u3007Oo[o2] = o;
        final ThreadContextElement<Object>[] \u3007o\u3007 = this.\u3007o\u3007;
        this.O8 = o2 + 1;
        Intrinsics.o\u30070((Object)threadContextElement, "null cannot be cast to non-null type kotlinx.coroutines.ThreadContextElement<kotlin.Any?>");
        \u3007o\u3007[o2] = (ThreadContextElement<Object>)threadContextElement;
    }
    
    public final void \u3007o00\u3007\u3007Oo(@NotNull final CoroutineContext coroutineContext) {
        int n = this.\u3007o\u3007.length - 1;
        if (n >= 0) {
            while (true) {
                final int n2 = n - 1;
                final ThreadContextElement<Object> threadContextElement = this.\u3007o\u3007[n];
                Intrinsics.Oo08((Object)threadContextElement);
                threadContextElement.\u3007O00(coroutineContext, this.\u3007o00\u3007\u3007Oo[n]);
                if (n2 < 0) {
                    break;
                }
                n = n2;
            }
        }
    }
}

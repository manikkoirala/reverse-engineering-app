// 
// Decompiled by Procyon v0.6.0
// 

package kotlinx.coroutines.internal;

import kotlinx.coroutines.CoroutineExceptionHandlerKt;
import kotlin.coroutines.EmptyCoroutineContext;
import kotlin.Unit;
import kotlinx.coroutines.CancellableContinuation;
import kotlinx.coroutines.DisposableHandle;
import kotlin.coroutines.CoroutineContext;
import kotlinx.coroutines.DefaultExecutorKt;
import org.jetbrains.annotations.NotNull;
import java.util.concurrent.atomic.AtomicIntegerFieldUpdater;
import kotlin.Metadata;
import kotlinx.coroutines.Delay;
import kotlinx.coroutines.CoroutineDispatcher;

@Metadata
public final class LimitedDispatcher extends CoroutineDispatcher implements Delay
{
    @NotNull
    private static final AtomicIntegerFieldUpdater O8o08O8O;
    private final Delay OO;
    @NotNull
    private final CoroutineDispatcher o0;
    @NotNull
    private final Object o\u300700O;
    private volatile int runningWorkers;
    @NotNull
    private final LockFreeTaskQueue<Runnable> \u300708O\u300700\u3007o;
    private final int \u3007OOo8\u30070;
    
    static {
        O8o08O8O = AtomicIntegerFieldUpdater.newUpdater(LimitedDispatcher.class, "runningWorkers");
    }
    
    public LimitedDispatcher(@NotNull final CoroutineDispatcher o0, final int \u3007oOo8\u30070) {
        this.o0 = o0;
        this.\u3007OOo8\u30070 = \u3007oOo8\u30070;
        Delay delay;
        if (o0 instanceof Delay) {
            delay = (Delay)o0;
        }
        else {
            delay = null;
        }
        Delay \u3007080 = delay;
        if (delay == null) {
            \u3007080 = DefaultExecutorKt.\u3007080();
        }
        this.OO = \u3007080;
        this.\u300708O\u300700\u3007o = new LockFreeTaskQueue<Runnable>(false);
        this.o\u300700O = new Object();
    }
    
    private final boolean O\u3007O\u3007oO() {
        synchronized (this.o\u300700O) {
            final AtomicIntegerFieldUpdater o8o08O8O = LimitedDispatcher.O8o08O8O;
            if (o8o08O8O.get(this) >= this.\u3007OOo8\u30070) {
                return false;
            }
            o8o08O8O.incrementAndGet(this);
            return true;
        }
    }
    
    public static final /* synthetic */ CoroutineDispatcher \u30078(final LimitedDispatcher limitedDispatcher) {
        return limitedDispatcher.o0;
    }
    
    private final Runnable \u30078\u30070\u3007o\u3007O() {
        Runnable runnable;
        while (true) {
            runnable = this.\u300708O\u300700\u3007o.O8();
            if (runnable == null) {
                synchronized (this.o\u300700O) {
                    final AtomicIntegerFieldUpdater o8o08O8O = LimitedDispatcher.O8o08O8O;
                    o8o08O8O.decrementAndGet(this);
                    if (this.\u300708O\u300700\u3007o.\u3007o\u3007() == 0) {
                        return null;
                    }
                    o8o08O8O.incrementAndGet(this);
                    continue;
                }
                break;
            }
            break;
        }
        return runnable;
    }
    
    @NotNull
    public DisposableHandle O8\u3007o(final long n, @NotNull final Runnable runnable, @NotNull final CoroutineContext coroutineContext) {
        return this.OO.O8\u3007o(n, runnable, coroutineContext);
    }
    
    public void O\u30078O8\u3007008(final long n, @NotNull final CancellableContinuation<? super Unit> cancellableContinuation) {
        this.OO.O\u30078O8\u3007008(n, (CancellableContinuation)cancellableContinuation);
    }
    
    public void dispatch(@NotNull final CoroutineContext coroutineContext, @NotNull final Runnable runnable) {
        this.\u300708O\u300700\u3007o.\u3007080(runnable);
        if (LimitedDispatcher.O8o08O8O.get(this) < this.\u3007OOo8\u30070 && this.O\u3007O\u3007oO()) {
            final Runnable \u30078\u30070\u3007o\u3007O = this.\u30078\u30070\u3007o\u3007O();
            if (\u30078\u30070\u3007o\u3007O != null) {
                this.o0.dispatch((CoroutineContext)this, (Runnable)new Worker(\u30078\u30070\u3007o\u3007O));
            }
        }
    }
    
    public void dispatchYield(@NotNull final CoroutineContext coroutineContext, @NotNull final Runnable runnable) {
        this.\u300708O\u300700\u3007o.\u3007080(runnable);
        if (LimitedDispatcher.O8o08O8O.get(this) < this.\u3007OOo8\u30070 && this.O\u3007O\u3007oO()) {
            final Runnable \u30078\u30070\u3007o\u3007O = this.\u30078\u30070\u3007o\u3007O();
            if (\u30078\u30070\u3007o\u3007O != null) {
                this.o0.dispatchYield((CoroutineContext)this, (Runnable)new Worker(\u30078\u30070\u3007o\u3007O));
            }
        }
    }
    
    @NotNull
    public CoroutineDispatcher limitedParallelism(final int n) {
        LimitedDispatcherKt.\u3007080(n);
        if (n >= this.\u3007OOo8\u30070) {
            return this;
        }
        return super.limitedParallelism(n);
    }
    
    @Metadata
    private final class Worker implements Runnable
    {
        @NotNull
        private Runnable o0;
        final LimitedDispatcher \u3007OOo8\u30070;
        
        public Worker(@NotNull final LimitedDispatcher \u3007oOo8\u30070, final Runnable o0) {
            this.\u3007OOo8\u30070 = \u3007oOo8\u30070;
            this.o0 = o0;
        }
        
        @Override
        public void run() {
            int n = 0;
            while (true) {
                try {
                    this.o0.run();
                }
                finally {
                    final Throwable t;
                    CoroutineExceptionHandlerKt.\u3007080((CoroutineContext)EmptyCoroutineContext.INSTANCE, t);
                }
                final Runnable o08000 = this.\u3007OOo8\u30070.\u30078\u30070\u3007o\u3007O();
                if (o08000 == null) {
                    return;
                }
                this.o0 = o08000;
                final int n2 = n + 1;
                if ((n = n2) < 16) {
                    continue;
                }
                n = n2;
                if (LimitedDispatcher.\u30078(this.\u3007OOo8\u30070).isDispatchNeeded((CoroutineContext)this.\u3007OOo8\u30070)) {
                    LimitedDispatcher.\u30078(this.\u3007OOo8\u30070).dispatch((CoroutineContext)this.\u3007OOo8\u30070, (Runnable)this);
                }
            }
        }
    }
}

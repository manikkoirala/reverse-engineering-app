// 
// Decompiled by Procyon v0.6.0
// 

package kotlinx.coroutines.internal;

import org.jetbrains.annotations.NotNull;
import kotlin.coroutines.CoroutineContext;
import kotlin.Metadata;
import kotlinx.coroutines.CoroutineScope;

@Metadata
public final class ContextScope implements CoroutineScope
{
    @NotNull
    private final CoroutineContext o0;
    
    public ContextScope(@NotNull final CoroutineContext o0) {
        this.o0 = o0;
    }
    
    @NotNull
    public CoroutineContext getCoroutineContext() {
        return this.o0;
    }
    
    @NotNull
    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append("CoroutineScope(coroutineContext=");
        sb.append(this.getCoroutineContext());
        sb.append(')');
        return sb.toString();
    }
}

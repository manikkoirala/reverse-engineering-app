// 
// Decompiled by Procyon v0.6.0
// 

package kotlinx.coroutines.internal;

import kotlinx.coroutines.UndispatchedCoroutine;
import kotlin.coroutines.CoroutineContext;
import kotlin.Result$Companion;
import java.util.concurrent.CancellationException;
import kotlinx.coroutines.CoroutineContextKt;
import kotlin.ResultKt;
import kotlin.Result;
import kotlin.coroutines.CoroutineContext$Key;
import kotlinx.coroutines.Job;
import kotlinx.coroutines.CompletionStateKt;
import kotlin.jvm.functions.Function1;
import kotlin.coroutines.Continuation;
import kotlinx.coroutines.EventLoop;
import kotlinx.coroutines.DispatchedTask;
import kotlinx.coroutines.ThreadLocalEventLoop;
import kotlin.Unit;
import org.jetbrains.annotations.NotNull;
import kotlin.Metadata;

@Metadata
public final class DispatchedContinuationKt
{
    @NotNull
    private static final Symbol \u3007080;
    @NotNull
    public static final Symbol \u3007o00\u3007\u3007Oo;
    
    static {
        \u3007080 = new Symbol("UNDEFINED");
        \u3007o00\u3007\u3007Oo = new Symbol("REUSABLE_CLAIMED");
    }
    
    public static final boolean O8(@NotNull final DispatchedContinuation<? super Unit> dispatchedContinuation) {
        final Unit \u3007080 = Unit.\u3007080;
        final EventLoop \u300781 = ThreadLocalEventLoop.\u3007080.\u3007080();
        final boolean oo00OOO = \u300781.oO00OOO();
        boolean b = false;
        if (oo00OOO) {
            return b;
        }
        if (\u300781.o\u3007O()) {
            dispatchedContinuation.O8o08O8O = \u3007080;
            dispatchedContinuation.OO = 1;
            \u300781.\u30078\u30070\u3007o\u3007O((DispatchedTask)dispatchedContinuation);
            b = true;
            return b;
        }
        \u300781.o8oO\u3007(true);
        final Throwable t2;
        try {
            dispatchedContinuation.run();
            while (\u300781.O000()) {}
            return b;
        }
        finally {
            final DispatchedContinuation<? super Unit> dispatchedContinuation2 = dispatchedContinuation;
            final Throwable t = t2;
            final Throwable t3 = null;
            dispatchedContinuation2.\u3007\u3007888(t, t3);
        }
        try {
            final DispatchedContinuation<? super Unit> dispatchedContinuation2 = dispatchedContinuation;
            final Throwable t = t2;
            final Throwable t3 = null;
            dispatchedContinuation2.\u3007\u3007888(t, t3);
            return b;
        }
        finally {
            \u300781.\u30078(true);
        }
    }
    
    public static final <T> void \u3007o00\u3007\u3007Oo(@NotNull Continuation<? super T> \u3007\u3007888, @NotNull final Object o, Function1<? super Throwable, Unit> \u3007080) {
        if (\u3007\u3007888 instanceof DispatchedContinuation) {
            final DispatchedContinuation dispatchedContinuation = (DispatchedContinuation)\u3007\u3007888;
            final Object \u3007o00\u3007\u3007Oo = CompletionStateKt.\u3007o00\u3007\u3007Oo(o, (Function1)\u3007080);
            if (dispatchedContinuation.\u300708O\u300700\u3007o.isDispatchNeeded(dispatchedContinuation.getContext())) {
                dispatchedContinuation.O8o08O8O = \u3007o00\u3007\u3007Oo;
                dispatchedContinuation.OO = 1;
                dispatchedContinuation.\u300708O\u300700\u3007o.dispatch(dispatchedContinuation.getContext(), (Runnable)dispatchedContinuation);
                return;
            }
            \u3007080 = ThreadLocalEventLoop.\u3007080.\u3007080();
            if (\u3007080.o\u3007O()) {
                dispatchedContinuation.O8o08O8O = \u3007o00\u3007\u3007Oo;
                dispatchedContinuation.OO = 1;
                \u3007080.\u30078\u30070\u3007o\u3007O((DispatchedTask)dispatchedContinuation);
                return;
            }
            \u3007080.o8oO\u3007(true);
            final Throwable t2;
            try {
                final Job job = (Job)dispatchedContinuation.getContext().get((CoroutineContext$Key)Job.\u3007o\u3007);
                boolean b;
                if (job != null && !job.isActive()) {
                    final CancellationException ooo\u3007O0 = job.OOO\u3007O0();
                    dispatchedContinuation.\u3007o\u3007(\u3007o00\u3007\u3007Oo, ooo\u3007O0);
                    final Result$Companion companion = Result.Companion;
                    ((Continuation)dispatchedContinuation).resumeWith(Result.constructor-impl(ResultKt.\u3007080((Throwable)ooo\u3007O0)));
                    b = true;
                }
                else {
                    b = false;
                }
                if (!b) {
                    final Continuation<T> o\u300700O = dispatchedContinuation.o\u300700O;
                    final Object \u3007080OO8\u30070 = dispatchedContinuation.\u3007080OO8\u30070;
                    final CoroutineContext context = o\u300700O.getContext();
                    final Object \u3007o\u3007 = ThreadContextKt.\u3007o\u3007(context, \u3007080OO8\u30070);
                    if (\u3007o\u3007 != ThreadContextKt.\u3007080) {
                        \u3007\u3007888 = CoroutineContextKt.\u3007\u3007888((Continuation)o\u300700O, context, \u3007o\u3007);
                    }
                    else {
                        \u3007\u3007888 = null;
                    }
                    try {
                        dispatchedContinuation.o\u300700O.resumeWith(o);
                        final Unit \u300781 = Unit.\u3007080;
                    }
                    finally {
                        if (\u3007\u3007888 == null || ((UndispatchedCoroutine)\u3007\u3007888).Oo\u3007O8o\u30078()) {
                            ThreadContextKt.\u3007080(context, \u3007o\u3007);
                        }
                    }
                }
                while (\u3007080.O000()) {}
                return;
            }
            finally {
                final DispatchedContinuation dispatchedContinuation2 = dispatchedContinuation;
                final Throwable t = t2;
                final Throwable t3 = null;
                dispatchedContinuation2.\u3007\u3007888(t, t3);
            }
            try {
                final DispatchedContinuation dispatchedContinuation2 = dispatchedContinuation;
                final Throwable t = t2;
                final Throwable t3 = null;
                dispatchedContinuation2.\u3007\u3007888(t, t3);
                return;
            }
            finally {
                \u3007080.\u30078(true);
            }
        }
        ((Continuation)\u3007\u3007888).resumeWith(o);
    }
}

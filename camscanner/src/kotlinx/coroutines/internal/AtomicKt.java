// 
// Decompiled by Procyon v0.6.0
// 

package kotlinx.coroutines.internal;

import org.jetbrains.annotations.NotNull;
import kotlin.Metadata;

@Metadata
public final class AtomicKt
{
    @NotNull
    public static final Object \u3007080;
    
    static {
        \u3007080 = new Symbol("NO_DECISION");
    }
}

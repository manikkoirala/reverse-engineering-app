// 
// Decompiled by Procyon v0.6.0
// 

package kotlinx.coroutines.internal;

import org.jetbrains.annotations.NotNull;
import kotlin.Metadata;

@Metadata
public final class SystemPropsKt
{
    public static final String O8(@NotNull final String s) {
        return SystemPropsKt__SystemPropsKt.\u3007o00\u3007\u3007Oo(s);
    }
    
    @NotNull
    public static final String Oo08(@NotNull final String s, @NotNull final String s2) {
        return SystemPropsKt__SystemProps_commonKt.\u3007o\u3007(s, s2);
    }
    
    public static final boolean o\u30070(@NotNull final String s, final boolean b) {
        return SystemPropsKt__SystemProps_commonKt.O8(s, b);
    }
    
    public static final int \u3007080() {
        return SystemPropsKt__SystemPropsKt.\u3007080();
    }
    
    public static final int \u3007o00\u3007\u3007Oo(@NotNull final String s, final int n, final int n2, final int n3) {
        return SystemPropsKt__SystemProps_commonKt.\u3007080(s, n, n2, n3);
    }
    
    public static final long \u3007o\u3007(@NotNull final String s, final long n, final long n2, final long n3) {
        return SystemPropsKt__SystemProps_commonKt.\u3007o00\u3007\u3007Oo(s, n, n2, n3);
    }
}

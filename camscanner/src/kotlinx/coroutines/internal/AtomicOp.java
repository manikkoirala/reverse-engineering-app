// 
// Decompiled by Procyon v0.6.0
// 

package kotlinx.coroutines.internal;

import androidx.concurrent.futures.\u3007080;
import org.jetbrains.annotations.NotNull;
import java.util.concurrent.atomic.AtomicReferenceFieldUpdater;
import kotlin.Metadata;

@Metadata
public abstract class AtomicOp<T> extends OpDescriptor
{
    @NotNull
    private static final AtomicReferenceFieldUpdater \u3007080;
    private volatile Object _consensus;
    
    static {
        \u3007080 = AtomicReferenceFieldUpdater.newUpdater(AtomicOp.class, Object.class, "_consensus");
    }
    
    public AtomicOp() {
        this._consensus = AtomicKt.\u3007080;
    }
    
    private final Object \u3007o\u3007(final Object o) {
        final AtomicReferenceFieldUpdater \u3007080 = AtomicOp.\u3007080;
        final Object value = \u3007080.get(this);
        final Object \u300781 = AtomicKt.\u3007080;
        if (value != \u300781) {
            return value;
        }
        if (androidx.concurrent.futures.\u3007080.\u3007080(\u3007080, this, \u300781, o)) {
            return o;
        }
        return \u3007080.get(this);
    }
    
    public abstract Object O8(final T p0);
    
    @Override
    public final Object \u3007080(final Object o) {
        Object o2;
        if ((o2 = AtomicOp.\u3007080.get(this)) == AtomicKt.\u3007080) {
            o2 = this.\u3007o\u3007(this.O8(o));
        }
        this.\u3007o00\u3007\u3007Oo(o, o2);
        return o2;
    }
    
    public abstract void \u3007o00\u3007\u3007Oo(final T p0, final Object p1);
}

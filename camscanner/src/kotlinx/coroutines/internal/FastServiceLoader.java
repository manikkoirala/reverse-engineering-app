// 
// Decompiled by Procyon v0.6.0
// 

package kotlinx.coroutines.internal;

import java.util.zip.ZipFile;
import java.util.Set;
import java.util.Iterator;
import java.util.Collection;
import java.util.ArrayList;
import kotlin.jvm.internal.Intrinsics;
import java.util.Collections;
import java.util.ServiceLoader;
import kotlin.collections.CollectionsKt;
import java.util.LinkedHashSet;
import kotlin.ExceptionsKt;
import java.io.Closeable;
import kotlin.io.CloseableKt;
import java.io.Reader;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.zip.ZipEntry;
import java.util.jar.JarFile;
import kotlin.text.StringsKt;
import java.util.List;
import java.net.URL;
import org.jetbrains.annotations.NotNull;
import kotlin.Metadata;

@Metadata
public final class FastServiceLoader
{
    @NotNull
    public static final FastServiceLoader \u3007080;
    
    static {
        \u3007080 = new FastServiceLoader();
    }
    
    private FastServiceLoader() {
    }
    
    private final List<String> Oo08(URL url) {
        final String string = url.toString();
        if (StringsKt.\u300700\u30078(string, "jar", false, 2, (Object)null)) {
            final String o8O0 = StringsKt.o8O0(StringsKt.O0\u3007OO8(string, "jar:file:", (String)null, 2, (Object)null), '!', (String)null, 2, (Object)null);
            final String o0\u3007OO8 = StringsKt.O0\u3007OO8(string, "!/", (String)null, 2, (Object)null);
            url = (URL)new JarFile(o8O0, false);
            try {
                final BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(((JarFile)url).getInputStream(new ZipEntry(o0\u3007OO8)), "UTF-8"));
                try {
                    final List<String> o\u30070 = FastServiceLoader.\u3007080.o\u30070(bufferedReader);
                    CloseableKt.\u3007080((Closeable)bufferedReader, (Throwable)null);
                    try {
                        ((ZipFile)url).close();
                        return o\u30070;
                    }
                    finally {}
                }
                finally {
                    try {}
                    finally {
                        final Throwable t;
                        CloseableKt.\u3007080((Closeable)bufferedReader, t);
                    }
                }
            }
            finally {
                try {}
                finally {
                    try {
                        ((ZipFile)url).close();
                    }
                    finally {
                        final Throwable t2;
                        final Throwable t3;
                        ExceptionsKt.\u3007080(t2, t3);
                    }
                }
            }
        }
        url = (URL)new BufferedReader(new InputStreamReader(url.openStream()));
        try {
            final List<String> o\u30072 = FastServiceLoader.\u3007080.o\u30070((BufferedReader)url);
            CloseableKt.\u3007080((Closeable)url, (Throwable)null);
            return o\u30072;
        }
        finally {
            try {}
            finally {
                final Throwable t4;
                CloseableKt.\u3007080((Closeable)url, t4);
            }
        }
    }
    
    private final List<String> o\u30070(final BufferedReader bufferedReader) {
        final LinkedHashSet set = new LinkedHashSet();
    Label_0009:
        while (true) {
            final String line = bufferedReader.readLine();
            if (line == null) {
                return CollectionsKt.\u3007O((Iterable)set);
            }
            final String string = StringsKt.O0oO008((CharSequence)StringsKt.O0o(line, "#", (String)null, 2, (Object)null)).toString();
            final int n = 0;
            int i = 0;
            while (true) {
                while (i < string.length()) {
                    final char char1 = string.charAt(i);
                    if (char1 != '.' && !Character.isJavaIdentifierPart(char1)) {
                        final boolean b = false;
                        if (!b) {
                            final StringBuilder sb = new StringBuilder();
                            sb.append("Illegal service provider class name: ");
                            sb.append(string);
                            throw new IllegalArgumentException(sb.toString().toString());
                        }
                        int n2 = n;
                        if (string.length() > 0) {
                            n2 = 1;
                        }
                        if (n2 != 0) {
                            set.add(string);
                            continue Label_0009;
                        }
                        continue Label_0009;
                    }
                    else {
                        ++i;
                    }
                }
                final boolean b = true;
                continue;
            }
        }
    }
    
    private final <S> S \u3007080(final String name, final ClassLoader loader, final Class<S> obj) {
        final Class<?> forName = Class.forName(name, false, loader);
        if (obj.isAssignableFrom(forName)) {
            return (S)obj.cast(forName.getDeclaredConstructor((Class<?>[])new Class[0]).newInstance(new Object[0]));
        }
        final StringBuilder sb = new StringBuilder();
        sb.append("Expected service of class ");
        sb.append(obj);
        sb.append(", but found ");
        sb.append(forName);
        throw new IllegalArgumentException(sb.toString().toString());
    }
    
    private final <S> List<S> \u3007o00\u3007\u3007Oo(Class<S> o8, final ClassLoader loader) {
        List \u3007o;
        try {
            o8 = this.O8((Class<S>)o8, loader);
        }
        finally {
            \u3007o = CollectionsKt.\u3007O((Iterable)ServiceLoader.load((Class<S>)o8, loader));
        }
        return \u3007o;
    }
    
    @NotNull
    public final <S> List<S> O8(@NotNull final Class<S> clazz, @NotNull final ClassLoader classLoader) {
        final StringBuilder sb = new StringBuilder();
        sb.append("META-INF/services/");
        sb.append(clazz.getName());
        final ArrayList<URL> list = Collections.list(classLoader.getResources(sb.toString()));
        Intrinsics.checkNotNullExpressionValue((Object)list, "list(this)");
        final ArrayList list2 = new ArrayList();
        final Iterator<Object> iterator = list.iterator();
        while (iterator.hasNext()) {
            CollectionsKt.o\u3007O8\u3007\u3007o((Collection)list2, (Iterable)FastServiceLoader.\u3007080.Oo08(iterator.next()));
        }
        final Set \u3007000O0 = CollectionsKt.\u3007000O0((Iterable)list2);
        if (\u3007000O0.isEmpty() ^ true) {
            final Iterable iterable = \u3007000O0;
            final ArrayList list3 = new ArrayList(CollectionsKt.\u30070\u3007O0088o(iterable, 10));
            final Iterator iterator2 = iterable.iterator();
            while (iterator2.hasNext()) {
                list3.add((Object)FastServiceLoader.\u3007080.\u3007080((String)iterator2.next(), classLoader, clazz));
            }
            return (List<S>)list3;
        }
        throw new IllegalArgumentException("No providers were loaded with FastServiceLoader".toString());
    }
    
    @NotNull
    public final List<MainDispatcherFactory> \u3007o\u3007() {
        if (!FastServiceLoaderKt.\u3007080()) {
            return this.\u3007o00\u3007\u3007Oo(MainDispatcherFactory.class, MainDispatcherFactory.class.getClassLoader());
        }
        List<MainDispatcherFactory> \u3007o00\u3007\u3007Oo = null;
        try {
            final ArrayList<MainDispatcherFactory> list = new ArrayList<MainDispatcherFactory>(2);
            final MainDispatcherFactory mainDispatcherFactory = null;
            MainDispatcherFactory e;
            try {
                e = MainDispatcherFactory.class.cast(Class.forName("kotlinx.coroutines.android.AndroidDispatcherFactory", true, MainDispatcherFactory.class.getClassLoader()).getDeclaredConstructor((Class<?>[])new Class[0]).newInstance(new Object[0]));
            }
            catch (final ClassNotFoundException ex) {
                e = null;
            }
            if (e != null) {
                list.add(e);
            }
            MainDispatcherFactory e2;
            try {
                e2 = MainDispatcherFactory.class.cast(Class.forName("kotlinx.coroutines.test.internal.TestMainDispatcherFactory", true, MainDispatcherFactory.class.getClassLoader()).getDeclaredConstructor((Class<?>[])new Class[0]).newInstance(new Object[0]));
            }
            catch (final ClassNotFoundException ex2) {
                e2 = mainDispatcherFactory;
            }
            if (e2 != null) {
                list.add(e2);
            }
        }
        finally {
            \u3007o00\u3007\u3007Oo = this.\u3007o00\u3007\u3007Oo(MainDispatcherFactory.class, MainDispatcherFactory.class.getClassLoader());
        }
        return \u3007o00\u3007\u3007Oo;
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package kotlinx.coroutines.internal;

import kotlin.coroutines.CoroutineContext;
import org.jetbrains.annotations.NotNull;
import java.util.concurrent.atomic.AtomicIntegerFieldUpdater;
import kotlin.Metadata;
import kotlinx.coroutines.NotCompleted;

@Metadata
public abstract class Segment<S extends Segment<S>> extends ConcurrentLinkedListNode<S> implements NotCompleted
{
    @NotNull
    private static final AtomicIntegerFieldUpdater \u300708O\u300700\u3007o;
    public final long OO;
    private volatile int cleanedAndPointers;
    
    static {
        \u300708O\u300700\u3007o = AtomicIntegerFieldUpdater.newUpdater(Segment.class, "cleanedAndPointers");
    }
    
    public Segment(final long oo, final S n, final int n2) {
        super(n);
        this.OO = oo;
        this.cleanedAndPointers = n2 << 16;
    }
    
    public final boolean OO0o\u3007\u3007() {
        return Segment.\u300708O\u300700\u3007o.addAndGet(this, -65536) == this.Oooo8o0\u3007() && !this.\u300780\u3007808\u3007O();
    }
    
    public abstract int Oooo8o0\u3007();
    
    @Override
    public boolean oO80() {
        return Segment.\u300708O\u300700\u3007o.get(this) == this.Oooo8o0\u3007() && !this.\u300780\u3007808\u3007O();
    }
    
    public final boolean \u3007O00() {
        final AtomicIntegerFieldUpdater \u300708O\u300700\u3007o = Segment.\u300708O\u300700\u3007o;
        int value;
        do {
            value = \u300708O\u300700\u3007o.get(this);
            final int oooo8o0\u3007 = this.Oooo8o0\u3007();
            final boolean b = false;
            if (value == oooo8o0\u3007 && !this.\u300780\u3007808\u3007O()) {
                return b;
            }
        } while (!\u300708O\u300700\u3007o.compareAndSet(this, value, 65536 + value));
        return true;
    }
    
    public final void \u3007O\u3007() {
        if (Segment.\u300708O\u300700\u3007o.incrementAndGet(this) == this.Oooo8o0\u3007()) {
            this.\u30078o8o\u3007();
        }
    }
    
    public abstract void \u3007\u3007808\u3007(final int p0, final Throwable p1, @NotNull final CoroutineContext p2);
}

// 
// Decompiled by Procyon v0.6.0
// 

package kotlinx.coroutines.internal;

import kotlin.jvm.internal.Intrinsics;
import org.jetbrains.annotations.NotNull;
import kotlin.Metadata;

@Metadata
public final class SegmentOrClosed<S extends Segment<S>>
{
    @NotNull
    public static <S extends Segment<S>> Object \u3007080(final Object o) {
        return o;
    }
    
    @NotNull
    public static final S \u3007o00\u3007\u3007Oo(final Object o) {
        if (o != ConcurrentLinkedListKt.\u3007080()) {
            Intrinsics.o\u30070(o, "null cannot be cast to non-null type S of kotlinx.coroutines.internal.SegmentOrClosed");
            return (S)o;
        }
        throw new IllegalStateException("Does not contain segment".toString());
    }
    
    public static final boolean \u3007o\u3007(final Object o) {
        return o == ConcurrentLinkedListKt.\u3007080();
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package kotlinx.coroutines.internal;

import androidx.concurrent.futures.\u3007080;
import org.jetbrains.annotations.NotNull;
import java.util.concurrent.atomic.AtomicReferenceFieldUpdater;
import kotlin.Metadata;

@Metadata
public class LockFreeTaskQueue<E>
{
    @NotNull
    private static final AtomicReferenceFieldUpdater \u3007080;
    private volatile Object _cur;
    
    static {
        \u3007080 = AtomicReferenceFieldUpdater.newUpdater(LockFreeTaskQueue.class, Object.class, "_cur");
    }
    
    public LockFreeTaskQueue(final boolean b) {
        this._cur = new LockFreeTaskQueueCore(8, b);
    }
    
    public final E O8() {
        final AtomicReferenceFieldUpdater \u3007080 = LockFreeTaskQueue.\u3007080;
        Object oo0o\u3007\u3007\u3007\u30070;
        while (true) {
            final LockFreeTaskQueueCore lockFreeTaskQueueCore = \u3007080.get(this);
            oo0o\u3007\u3007\u3007\u30070 = lockFreeTaskQueueCore.OO0o\u3007\u3007\u3007\u30070();
            if (oo0o\u3007\u3007\u3007\u30070 != LockFreeTaskQueueCore.oO80) {
                break;
            }
            androidx.concurrent.futures.\u3007080.\u3007080(LockFreeTaskQueue.\u3007080, this, lockFreeTaskQueueCore, lockFreeTaskQueueCore.\u300780\u3007808\u3007O());
        }
        return (E)oo0o\u3007\u3007\u3007\u30070;
    }
    
    public final boolean \u3007080(@NotNull final E e) {
        final AtomicReferenceFieldUpdater \u3007080 = LockFreeTaskQueue.\u3007080;
        while (true) {
            final LockFreeTaskQueueCore lockFreeTaskQueueCore = \u3007080.get(this);
            final int \u300781 = lockFreeTaskQueueCore.\u3007080(e);
            if (\u300781 == 0) {
                return true;
            }
            if (\u300781 != 1) {
                if (\u300781 != 2) {
                    continue;
                }
                return false;
            }
            else {
                androidx.concurrent.futures.\u3007080.\u3007080(LockFreeTaskQueue.\u3007080, this, lockFreeTaskQueueCore, lockFreeTaskQueueCore.\u300780\u3007808\u3007O());
            }
        }
    }
    
    public final void \u3007o00\u3007\u3007Oo() {
        final AtomicReferenceFieldUpdater \u3007080 = LockFreeTaskQueue.\u3007080;
        while (true) {
            final LockFreeTaskQueueCore lockFreeTaskQueueCore = \u3007080.get(this);
            if (lockFreeTaskQueueCore.O8()) {
                break;
            }
            androidx.concurrent.futures.\u3007080.\u3007080(LockFreeTaskQueue.\u3007080, this, lockFreeTaskQueueCore, lockFreeTaskQueueCore.\u300780\u3007808\u3007O());
        }
    }
    
    public final int \u3007o\u3007() {
        return LockFreeTaskQueue.\u3007080.get(this).o\u30070();
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package kotlinx.coroutines.internal;

import java.util.List;
import java.util.Iterator;
import kotlin.sequences.SequencesKt;
import java.util.ServiceLoader;
import kotlinx.coroutines.MainCoroutineDispatcher;
import org.jetbrains.annotations.NotNull;
import kotlin.Metadata;

@Metadata
public final class MainDispatcherLoader
{
    @NotNull
    public static final MainDispatcherLoader \u3007080;
    private static final boolean \u3007o00\u3007\u3007Oo;
    @NotNull
    public static final MainCoroutineDispatcher \u3007o\u3007;
    
    static {
        final MainDispatcherLoader mainDispatcherLoader = \u3007080 = new MainDispatcherLoader();
        \u3007o00\u3007\u3007Oo = SystemPropsKt.o\u30070("kotlinx.coroutines.fast.service.loader", true);
        \u3007o\u3007 = mainDispatcherLoader.\u3007080();
    }
    
    private MainDispatcherLoader() {
    }
    
    private final MainCoroutineDispatcher \u3007080() {
        MissingMainCoroutineDispatcher \u3007o00\u3007\u3007Oo = null;
        try {
            List<MainDispatcherFactory> list;
            if (MainDispatcherLoader.\u3007o00\u3007\u3007Oo) {
                list = FastServiceLoader.\u3007080.\u3007o\u3007();
            }
            else {
                list = SequencesKt.\u3007O888o0o(SequencesKt.\u3007o\u3007((Iterator)ServiceLoader.load(MainDispatcherFactory.class, MainDispatcherFactory.class.getClassLoader()).iterator()));
            }
            final Iterator iterator = list.iterator();
            Object next;
            if (!iterator.hasNext()) {
                next = null;
            }
            else {
                next = iterator.next();
                if (iterator.hasNext()) {
                    int \u3007080 = ((MainDispatcherFactory)next).\u3007080();
                    MainDispatcherFactory mainDispatcherFactory = (MainDispatcherFactory)next;
                    do {
                        final Object next2 = iterator.next();
                        final int \u300781 = ((MainDispatcherFactory)next2).\u3007080();
                        next = mainDispatcherFactory;
                        int n;
                        if ((n = \u3007080) < \u300781) {
                            next = next2;
                            n = \u300781;
                        }
                        mainDispatcherFactory = (MainDispatcherFactory)next;
                        \u3007080 = n;
                    } while (iterator.hasNext());
                }
            }
            final MainDispatcherFactory mainDispatcherFactory2 = (MainDispatcherFactory)next;
            final MainCoroutineDispatcher oo08;
            if (mainDispatcherFactory2 == null || (oo08 = MainDispatchersKt.Oo08(mainDispatcherFactory2, list)) == null) {
                MainDispatchersKt.\u3007o00\u3007\u3007Oo(null, null, 3, null);
            }
        }
        finally {
            final Throwable t;
            \u3007o00\u3007\u3007Oo = MainDispatchersKt.\u3007o00\u3007\u3007Oo(t, null, 2, null);
        }
        return \u3007o00\u3007\u3007Oo;
    }
}

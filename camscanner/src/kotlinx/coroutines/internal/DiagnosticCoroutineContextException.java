// 
// Decompiled by Procyon v0.6.0
// 

package kotlinx.coroutines.internal;

import org.jetbrains.annotations.NotNull;
import kotlin.coroutines.CoroutineContext;
import kotlin.Metadata;

@Metadata
public final class DiagnosticCoroutineContextException extends RuntimeException
{
    @NotNull
    private final transient CoroutineContext context;
    
    public DiagnosticCoroutineContextException(@NotNull final CoroutineContext context) {
        this.context = context;
    }
    
    @NotNull
    @Override
    public Throwable fillInStackTrace() {
        this.setStackTrace(new StackTraceElement[0]);
        return this;
    }
    
    @NotNull
    @Override
    public String getLocalizedMessage() {
        return this.context.toString();
    }
}

import java.util.Iterator;
import java.util.ArrayList;
import com.ekodroid.omrevaluator.templateui.models.ExamId;
import com.ekodroid.omrevaluator.templateui.models.SheetTemplate2;

// 
// Decompiled by Procyon v0.6.0
// 

public abstract class e8
{
    public static SheetTemplate2 a;
    public static ExamId b;
    public static ArrayList c;
    
    public static ArrayList a() {
        final ArrayList list = new ArrayList();
        final ArrayList c = e8.c;
        if (c != null) {
            final Iterator iterator = c.iterator();
            while (iterator.hasNext()) {
                list.add(((d8)iterator.next()).a());
            }
        }
        return list;
    }
    
    public static void b(final ExamId b, final SheetTemplate2 a, final ArrayList c) {
        e8.b = b;
        e8.a = a;
        e8.c = c;
    }
    
    public static void c() {
        e8.b = null;
        e8.a = null;
        e8.c = null;
    }
}

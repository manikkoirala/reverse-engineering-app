import android.graphics.Rect;
import android.view.ViewGroup;
import android.view.View;
import java.util.Collection;
import java.util.ArrayList;
import java.util.List;

// 
// Decompiled by Procyon v0.6.0
// 

public class m80 extends l80
{
    public static boolean C(final qy1 qy1) {
        return !l80.l(qy1.getTargetIds()) || !l80.l(qy1.getTargetNames()) || !l80.l(qy1.getTargetTypes());
    }
    
    @Override
    public void A(final Object o, final ArrayList list, final ArrayList list2) {
        final uy1 uy1 = (uy1)o;
        if (uy1 != null) {
            uy1.getTargets().clear();
            uy1.getTargets().addAll(list2);
            this.q(uy1, list, list2);
        }
    }
    
    @Override
    public Object B(final Object o) {
        if (o == null) {
            return null;
        }
        final uy1 uy1 = new uy1();
        uy1.z((qy1)o);
        return uy1;
    }
    
    @Override
    public void a(final Object o, final View view) {
        if (o != null) {
            ((qy1)o).addTarget(view);
        }
    }
    
    @Override
    public void b(final Object o, final ArrayList list) {
        final qy1 qy1 = (qy1)o;
        if (qy1 == null) {
            return;
        }
        final boolean b = qy1 instanceof uy1;
        int i = 0;
        final int n = 0;
        if (b) {
            final uy1 uy1 = (uy1)qy1;
            for (int c = uy1.C(), j = n; j < c; ++j) {
                this.b(uy1.B(j), list);
            }
        }
        else if (!C(qy1) && l80.l(qy1.getTargets())) {
            while (i < list.size()) {
                qy1.addTarget((View)list.get(i));
                ++i;
            }
        }
    }
    
    @Override
    public void c(final ViewGroup viewGroup, final Object o) {
        sy1.a(viewGroup, (qy1)o);
    }
    
    @Override
    public boolean e(final Object o) {
        return o instanceof qy1;
    }
    
    @Override
    public Object g(final Object o) {
        qy1 clone;
        if (o != null) {
            clone = ((qy1)o).clone();
        }
        else {
            clone = null;
        }
        return clone;
    }
    
    @Override
    public Object m(final Object o, Object o2, final Object o3) {
        qy1 l = (qy1)o;
        final qy1 qy1 = (qy1)o2;
        final qy1 qy2 = (qy1)o3;
        if (l != null && qy1 != null) {
            l = new uy1().z(l).z(qy1).L(1);
        }
        else if (l == null) {
            if (qy1 != null) {
                l = qy1;
            }
            else {
                l = null;
            }
        }
        if (qy2 != null) {
            o2 = new uy1();
            if (l != null) {
                ((uy1)o2).z(l);
            }
            ((uy1)o2).z(qy2);
            return o2;
        }
        return l;
    }
    
    @Override
    public Object n(final Object o, final Object o2, final Object o3) {
        final uy1 uy1 = new uy1();
        if (o != null) {
            uy1.z((qy1)o);
        }
        if (o2 != null) {
            uy1.z((qy1)o2);
        }
        if (o3 != null) {
            uy1.z((qy1)o3);
        }
        return uy1;
    }
    
    @Override
    public void p(final Object o, final View view) {
        if (o != null) {
            ((qy1)o).removeTarget(view);
        }
    }
    
    @Override
    public void q(final Object o, final ArrayList list, final ArrayList list2) {
        final qy1 qy1 = (qy1)o;
        final boolean b = qy1 instanceof uy1;
        int i = 0;
        int j = 0;
        if (b) {
            for (uy1 uy1 = (uy1)qy1; j < uy1.C(); ++j) {
                this.q(uy1.B(j), list, list2);
            }
        }
        else if (!C(qy1)) {
            final List<View> targets = qy1.getTargets();
            if (targets.size() == list.size() && targets.containsAll(list)) {
                int size;
                if (list2 == null) {
                    size = 0;
                }
                else {
                    size = list2.size();
                }
                while (i < size) {
                    qy1.addTarget((View)list2.get(i));
                    ++i;
                }
                for (int k = list.size() - 1; k >= 0; --k) {
                    qy1.removeTarget((View)list.get(k));
                }
            }
        }
    }
    
    @Override
    public void r(final Object o, final View view, final ArrayList list) {
        ((qy1)o).addListener((qy1.g)new qy1.g(this, view, list) {
            public final View a;
            public final ArrayList b;
            public final m80 c;
            
            @Override
            public void onTransitionCancel(final qy1 qy1) {
            }
            
            @Override
            public void onTransitionEnd(final qy1 qy1) {
                qy1.removeListener((qy1.g)this);
                this.a.setVisibility(8);
                for (int size = this.b.size(), i = 0; i < size; ++i) {
                    ((View)this.b.get(i)).setVisibility(0);
                }
            }
            
            @Override
            public void onTransitionPause(final qy1 qy1) {
            }
            
            @Override
            public void onTransitionResume(final qy1 qy1) {
            }
            
            @Override
            public void onTransitionStart(final qy1 qy1) {
            }
        });
    }
    
    @Override
    public void t(final Object o, final Object o2, final ArrayList list, final Object o3, final ArrayList list2, final Object o4, final ArrayList list3) {
        ((qy1)o).addListener((qy1.g)new ry1(this, o2, list, o3, list2, o4, list3) {
            public final Object a;
            public final ArrayList b;
            public final Object c;
            public final ArrayList d;
            public final Object e;
            public final ArrayList f;
            public final m80 g;
            
            @Override
            public void onTransitionEnd(final qy1 qy1) {
                qy1.removeListener((qy1.g)this);
            }
            
            @Override
            public void onTransitionStart(final qy1 qy1) {
                final Object a = this.a;
                if (a != null) {
                    this.g.q(a, this.b, null);
                }
                final Object c = this.c;
                if (c != null) {
                    this.g.q(c, this.d, null);
                }
                final Object e = this.e;
                if (e != null) {
                    this.g.q(e, this.f, null);
                }
            }
        });
    }
    
    @Override
    public void u(final Object o, final Rect rect) {
        if (o != null) {
            ((qy1)o).setEpicenterCallback((qy1.f)new qy1.f(this, rect) {
                public final Rect a;
                public final m80 b;
                
                @Override
                public Rect a(final qy1 qy1) {
                    final Rect a = this.a;
                    if (a != null && !a.isEmpty()) {
                        return this.a;
                    }
                    return null;
                }
            });
        }
    }
    
    @Override
    public void v(final Object o, final View view) {
        if (view != null) {
            final qy1 qy1 = (qy1)o;
            final Rect rect = new Rect();
            this.k(view, rect);
            qy1.setEpicenterCallback((qy1.f)new qy1.f(this, rect) {
                public final Rect a;
                public final m80 b;
                
                @Override
                public Rect a(final qy1 qy1) {
                    return this.a;
                }
            });
        }
    }
    
    @Override
    public void z(final Object o, final View e, final ArrayList list) {
        final uy1 uy1 = (uy1)o;
        final List<View> targets = uy1.getTargets();
        targets.clear();
        for (int size = list.size(), i = 0; i < size; ++i) {
            l80.d(targets, (View)list.get(i));
        }
        targets.add(e);
        list.add(e);
        this.b(uy1, list);
    }
}

import android.widget.TextView;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.view.View;
import java.util.ArrayList;
import android.content.Context;
import android.widget.BaseAdapter;

// 
// Decompiled by Procyon v0.6.0
// 

public class b3 extends BaseAdapter
{
    public Context a;
    public ArrayList b;
    
    public b3(final Context a, final ArrayList b) {
        this.a = a;
        this.b = b;
    }
    
    public int getCount() {
        return this.b.size();
    }
    
    public Object getItem(final int index) {
        return this.b.get(index);
    }
    
    public long getItemId(final int n) {
        return n;
    }
    
    public View getView(final int index, View inflate, final ViewGroup viewGroup) {
        final String[] array = this.b.get(index);
        inflate = ((LayoutInflater)this.a.getSystemService("layout_inflater")).inflate(2131493017, (ViewGroup)null);
        final TextView textView = (TextView)inflate.findViewById(2131297275);
        final TextView textView2 = (TextView)inflate.findViewById(2131297293);
        textView.setText((CharSequence)array[2]);
        textView2.setText((CharSequence)array[3]);
        return inflate;
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

public class z51 implements x51
{
    public final Object[] a;
    public int b;
    
    public z51(final int n) {
        if (n > 0) {
            this.a = new Object[n];
            return;
        }
        throw new IllegalArgumentException("The max pool size must be > 0");
    }
    
    @Override
    public Object a() {
        final int b = this.b;
        if (b > 0) {
            final int n = b - 1;
            final Object[] a = this.a;
            final Object o = a[n];
            a[n] = null;
            this.b = b - 1;
            return o;
        }
        return null;
    }
    
    @Override
    public boolean b(final Object o) {
        final int b = this.b;
        final Object[] a = this.a;
        if (b < a.length) {
            a[b] = o;
            this.b = b + 1;
            return true;
        }
        return false;
    }
    
    @Override
    public void c(final Object[] array, int i) {
        int length = i;
        if (i > array.length) {
            length = array.length;
        }
        Object o;
        int b;
        Object[] a;
        for (i = 0; i < length; ++i) {
            o = array[i];
            b = this.b;
            a = this.a;
            if (b < a.length) {
                a[b] = o;
                this.b = b + 1;
            }
        }
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

public class eb
{
    public Object[] a;
    public int b;
    
    public eb() {
        this.b = 0;
        this.a = new Object[2];
    }
    
    public eb(final int n) {
        this.a = null;
        this.b = 0;
        this.a = new Object[n];
    }
    
    public void a(final Object o) {
        this.g(o, this.b);
    }
    
    public boolean b(final Object o) {
        boolean b = false;
        if (this.d(o, 0, true) >= 0) {
            b = true;
        }
        return b;
    }
    
    public void c(int i) {
        final Object[] a = this.a;
        if (i > a.length) {
            final int n = a.length * 2;
            if (n >= i) {
                i = n;
            }
            final Object[] a2 = new Object[i];
            for (i = 0; i < this.b; ++i) {
                a2[i] = this.a[i];
            }
            this.a = a2;
        }
    }
    
    public final int d(final Object o, int i, final boolean b) {
        if (i >= 0) {
            if (i < this.b) {
                if (b) {
                    int j = i;
                    if (o == null) {
                        while (i < this.b) {
                            if (this.a[i] == null) {
                                return i;
                            }
                            ++i;
                        }
                    }
                    else {
                        while (j < this.b) {
                            if (o.equals(this.a[j])) {
                                return j;
                            }
                            ++j;
                        }
                    }
                }
                else {
                    int k = i;
                    if (o == null) {
                        while (i >= 0) {
                            if (this.a[i] == null) {
                                return i;
                            }
                            --i;
                        }
                    }
                    else {
                        while (k >= 0) {
                            if (o.equals(this.a[k])) {
                                return k;
                            }
                            --k;
                        }
                    }
                }
            }
        }
        return -1;
    }
    
    public Object e(final int i) {
        if (i >= 0 && i < this.b) {
            return this.a[i];
        }
        final StringBuilder sb = new StringBuilder();
        sb.append("required: (index >= 0 && index < size) but: (index = ");
        sb.append(i);
        sb.append(", size = ");
        sb.append(this.b);
        sb.append(")");
        throw new IllegalArgumentException(sb.toString());
    }
    
    public int f() {
        return this.a.length;
    }
    
    public void g(Object o, final int i) {
        if (i >= 0) {
            final int b = this.b;
            if (i <= b) {
                this.c(b + 1);
                for (int j = this.b; j > i; --j) {
                    final Object[] a = this.a;
                    a[j] = a[j - 1];
                }
                this.a[i] = o;
                ++this.b;
                return;
            }
        }
        o = new StringBuilder();
        ((StringBuilder)o).append("required: (index >= 0 && index <= size) but: (index = ");
        ((StringBuilder)o).append(i);
        ((StringBuilder)o).append(", size = ");
        ((StringBuilder)o).append(this.b);
        ((StringBuilder)o).append(")");
        throw new IllegalArgumentException(((StringBuilder)o).toString());
    }
    
    public int h() {
        return this.b;
    }
}

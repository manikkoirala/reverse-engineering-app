import android.content.res.Configuration;
import android.content.Context;

// 
// Decompiled by Procyon v0.6.0
// 

public class v1
{
    public Context a;
    
    public v1(final Context a) {
        this.a = a;
    }
    
    public static v1 b(final Context context) {
        return new v1(context);
    }
    
    public boolean a() {
        return this.a.getApplicationInfo().targetSdkVersion < 14;
    }
    
    public int c() {
        return this.a.getResources().getDisplayMetrics().widthPixels / 2;
    }
    
    public int d() {
        final Configuration configuration = this.a.getResources().getConfiguration();
        final int screenWidthDp = configuration.screenWidthDp;
        final int screenHeightDp = configuration.screenHeightDp;
        if (configuration.smallestScreenWidthDp > 600 || screenWidthDp > 600 || (screenWidthDp > 960 && screenHeightDp > 720) || (screenWidthDp > 720 && screenHeightDp > 960)) {
            return 5;
        }
        if (screenWidthDp >= 500 || (screenWidthDp > 640 && screenHeightDp > 480) || (screenWidthDp > 480 && screenHeightDp > 640)) {
            return 4;
        }
        if (screenWidthDp >= 360) {
            return 3;
        }
        return 2;
    }
    
    public boolean e() {
        return this.a.getResources().getBoolean(ua1.a);
    }
    
    public boolean f() {
        return true;
    }
}

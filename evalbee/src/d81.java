import java.lang.reflect.Type;

// 
// Decompiled by Procyon v0.6.0
// 

public abstract class d81
{
    public static boolean a(final Type type) {
        return type instanceof Class && ((Class)type).isPrimitive();
    }
    
    public static Class b(final Class clazz) {
        if (clazz == Integer.TYPE) {
            return Integer.class;
        }
        if (clazz == Float.TYPE) {
            return Float.class;
        }
        if (clazz == Byte.TYPE) {
            return Byte.class;
        }
        if (clazz == Double.TYPE) {
            return Double.class;
        }
        if (clazz == Long.TYPE) {
            return Long.class;
        }
        if (clazz == Character.TYPE) {
            return Character.class;
        }
        if (clazz == Boolean.TYPE) {
            return Boolean.class;
        }
        if (clazz == Short.TYPE) {
            return Short.class;
        }
        Class<Void> clazz2;
        if ((clazz2 = clazz) == Void.TYPE) {
            clazz2 = Void.class;
        }
        return clazz2;
    }
}

import com.google.firebase.database.collection.b;
import com.google.protobuf.ByteString;
import java.util.List;

// 
// Decompiled by Procyon v0.6.0
// 

public final class yx0
{
    public final xx0 a;
    public final qo1 b;
    public final List c;
    public final ByteString d;
    public final b e;
    
    public yx0(final xx0 a, final qo1 b, final List c, final ByteString d, final b e) {
        this.a = a;
        this.b = b;
        this.c = c;
        this.d = d;
        this.e = e;
    }
    
    public static yx0 a(final xx0 xx0, final qo1 qo1, final List list, final ByteString byteString) {
        final int size = xx0.h().size();
        final int size2 = list.size();
        int i = 0;
        g9.d(size == size2, "Mutations sent %d must equal results received %d", xx0.h().size(), list.size());
        b b = au.b();
        for (List h = xx0.h(); i < h.size(); ++i) {
            b = b.l(((wx0)h.get(i)).g(), list.get(i).b());
        }
        return new yx0(xx0, qo1, list, byteString, b);
    }
    
    public xx0 b() {
        return this.a;
    }
    
    public qo1 c() {
        return this.b;
    }
    
    public b d() {
        return this.e;
    }
    
    public List e() {
        return this.c;
    }
    
    public ByteString f() {
        return this.d;
    }
}

import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.android.volley.d;
import com.ekodroid.omrevaluator.exams.sms.SmsData;

// 
// Decompiled by Procyon v0.6.0
// 

public class p61
{
    public ee1 a;
    public y01 b;
    public SmsData c;
    public String d;
    
    public p61(final SmsData c, final ee1 a, final y01 b) {
        final StringBuilder sb = new StringBuilder();
        sb.append("https://");
        sb.append(a91.u());
        sb.append(":8759/smsMessages2");
        this.d = sb.toString();
        this.a = a;
        this.b = b;
        this.c = c;
        this.f();
    }
    
    public static /* synthetic */ y01 b(final p61 p) {
        return p.b;
    }
    
    public static /* synthetic */ SmsData c(final p61 p) {
        return p.c;
    }
    
    public void d() {
        this.e(null);
        this.b = null;
    }
    
    public final void e(final Object o) {
        final y01 b = this.b;
        if (b != null) {
            b.a(o);
        }
    }
    
    public final void f() {
        final hr1 hr1 = new hr1(this, 1, this.d, new d.b(this) {
            public final p61 a;
            
            public void b(final String s) {
                try {
                    this.a.e(new gc0().j(s, SmsData.class));
                }
                catch (final Exception ex) {
                    ex.printStackTrace();
                    this.a.e(null);
                }
            }
        }, new d.a(this) {
            public final p61 a;
            
            @Override
            public void a(final VolleyError volleyError) {
                p61.b(this.a).a(null);
            }
        }) {
            public final p61 w;
            
            @Override
            public byte[] k() {
                return new gc0().s(p61.c(this.w)).getBytes();
            }
            
            @Override
            public String l() {
                return "application/json";
            }
        };
        hr1.L(new wq(60000, 0, 1.0f));
        this.a.a(hr1);
    }
}

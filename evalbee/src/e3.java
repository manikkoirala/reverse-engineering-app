import android.widget.TextView;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.view.View;
import java.util.ArrayList;
import android.content.Context;
import android.widget.BaseAdapter;

// 
// Decompiled by Procyon v0.6.0
// 

public class e3 extends BaseAdapter
{
    public Context a;
    public ArrayList b;
    
    public e3(final Context a, final ArrayList b) {
        this.a = a;
        this.b = b;
    }
    
    public int getCount() {
        return this.b.size();
    }
    
    public Object getItem(final int index) {
        return this.b.get(index);
    }
    
    public long getItemId(final int n) {
        return n;
    }
    
    public View getView(final int index, View inflate, final ViewGroup viewGroup) {
        final ye1 b = this.b.get(index).b();
        inflate = ((LayoutInflater)this.a.getSystemService("layout_inflater")).inflate(2131493020, (ViewGroup)null);
        final TextView textView = (TextView)inflate.findViewById(2131297194);
        final TextView textView2 = (TextView)inflate.findViewById(2131297192);
        final TextView textView3 = (TextView)inflate.findViewById(2131297193);
        final TextView textView4 = (TextView)inflate.findViewById(2131297195);
        final TextView textView5 = (TextView)inflate.findViewById(2131297190);
        final TextView textView6 = (TextView)inflate.findViewById(2131297191);
        final TextView textView7 = (TextView)inflate.findViewById(2131297196);
        final TextView textView8 = (TextView)inflate.findViewById(2131297297);
        final StringBuilder sb = new StringBuilder();
        sb.append(b.g());
        sb.append("");
        textView.setText((CharSequence)sb.toString());
        final StringBuilder sb2 = new StringBuilder();
        sb2.append(ve1.o(b.d()));
        sb2.append("");
        textView2.setText((CharSequence)sb2.toString());
        textView4.setText((CharSequence)b.e());
        final StringBuilder sb3 = new StringBuilder();
        sb3.append(b.f());
        sb3.append("");
        textView3.setText((CharSequence)sb3.toString());
        final StringBuilder sb4 = new StringBuilder();
        sb4.append("");
        sb4.append(b.a());
        textView5.setText((CharSequence)sb4.toString());
        final StringBuilder sb5 = new StringBuilder();
        sb5.append("");
        sb5.append(b.c());
        textView6.setText((CharSequence)sb5.toString());
        final StringBuilder sb6 = new StringBuilder();
        sb6.append("");
        sb6.append(b.h());
        textView7.setText((CharSequence)sb6.toString());
        textView8.setText((CharSequence)a91.l(b.e()));
        return inflate;
    }
}

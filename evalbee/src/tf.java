import androidx.constraintlayout.core.widgets.ConstraintAnchor;
import java.util.Iterator;
import androidx.constraintlayout.core.widgets.analyzer.DependencyNode;
import androidx.constraintlayout.core.widgets.analyzer.a;
import androidx.constraintlayout.core.widgets.d;
import androidx.constraintlayout.core.widgets.ConstraintWidget;
import java.util.ArrayList;
import androidx.constraintlayout.core.widgets.analyzer.WidgetRun;

// 
// Decompiled by Procyon v0.6.0
// 

public class tf extends WidgetRun
{
    public ArrayList k;
    public int l;
    
    public tf(final ConstraintWidget constraintWidget, final int f) {
        super(constraintWidget);
        this.k = new ArrayList();
        super.f = f;
        this.q();
    }
    
    @Override
    public void a(final ps ps) {
        if (super.h.j) {
            if (super.i.j) {
                final ConstraintWidget k = super.b.K();
                final boolean b = k instanceof d && ((d)k).Q1();
                final int n = super.i.g - super.h.g;
                final int size = this.k.size();
                int index = 0;
                int n2;
                int n3;
                while (true) {
                    n2 = -1;
                    if (index >= size) {
                        n3 = -1;
                        break;
                    }
                    n3 = index;
                    if (((WidgetRun)this.k.get(index)).b.V() != 8) {
                        break;
                    }
                    ++index;
                }
                int index2;
                final int n4 = index2 = size - 1;
                int n5;
                while (true) {
                    n5 = n2;
                    if (index2 < 0) {
                        break;
                    }
                    if (((WidgetRun)this.k.get(index2)).b.V() != 8) {
                        n5 = index2;
                        break;
                    }
                    --index2;
                }
                int i = 0;
                while (true) {
                    while (i < 2) {
                        int j = 0;
                        int n6 = 0;
                        int n7 = 0;
                        int n8 = 0;
                        float n9 = 0.0f;
                        while (j < size) {
                            final WidgetRun widgetRun = this.k.get(j);
                            int n10;
                            if (widgetRun.b.V() == 8) {
                                n10 = n7;
                            }
                            else {
                                final int n11 = n8 + 1;
                                int n12 = n6;
                                if (j > 0) {
                                    n12 = n6;
                                    if (j >= n3) {
                                        n12 = n6 + widgetRun.h.f;
                                    }
                                }
                                final a e = widgetRun.e;
                                final int g = e.g;
                                final boolean b2 = widgetRun.d != ConstraintWidget.DimensionBehaviour.MATCH_CONSTRAINT;
                                int m = 0;
                                boolean b3 = false;
                                Label_0427: {
                                    Label_0419: {
                                        if (!b2) {
                                            if (widgetRun.a == 1 && i == 0) {
                                                m = e.m;
                                                ++n7;
                                            }
                                            else {
                                                if (!e.j) {
                                                    break Label_0419;
                                                }
                                                m = g;
                                            }
                                            b3 = true;
                                            break Label_0427;
                                        }
                                        final int f = super.f;
                                        if (f == 0 && !widgetRun.b.e.e.j) {
                                            return;
                                        }
                                        if (f == 1 && !widgetRun.b.f.e.j) {
                                            return;
                                        }
                                    }
                                    b3 = b2;
                                    m = g;
                                }
                                int n15;
                                float n16;
                                if (!b3) {
                                    final int n13 = n7 + 1;
                                    final float n14 = widgetRun.b.D0[super.f];
                                    n15 = n12;
                                    n7 = n13;
                                    n16 = n9;
                                    if (n14 >= 0.0f) {
                                        n16 = n9 + n14;
                                        n15 = n12;
                                        n7 = n13;
                                    }
                                }
                                else {
                                    n15 = n12 + m;
                                    n16 = n9;
                                }
                                n6 = n15;
                                n10 = n7;
                                n8 = n11;
                                n9 = n16;
                                if (j < n4) {
                                    n6 = n15;
                                    n10 = n7;
                                    n8 = n11;
                                    n9 = n16;
                                    if (j < n5) {
                                        n6 = n15 + -widgetRun.i.f;
                                        n9 = n16;
                                        n8 = n11;
                                        n10 = n7;
                                    }
                                }
                            }
                            ++j;
                            n7 = n10;
                        }
                        if (n6 >= n && n7 != 0) {
                            ++i;
                        }
                        else {
                            final int n17 = n8;
                            int n18 = n7;
                            int n19 = super.h.g;
                            if (b) {
                                n19 = super.i.g;
                            }
                            int n20 = n19;
                            if (n6 > n) {
                                final int n21 = (int)((n6 - n) / 2.0f + 0.5f);
                                if (b) {
                                    n20 = n19 + n21;
                                }
                                else {
                                    n20 = n19 - n21;
                                }
                            }
                            int n35;
                            if (n18 > 0) {
                                final float n22 = (float)(n - n6);
                                final int n23 = (int)(n22 / n18 + 0.5f);
                                int l = 0;
                                final int n24 = 0;
                                final int n25 = n6;
                                int n26 = n24;
                                final int n27 = n20;
                                while (l < size) {
                                    final WidgetRun widgetRun2 = this.k.get(l);
                                    if (widgetRun2.b.V() != 8) {
                                        if (widgetRun2.d == ConstraintWidget.DimensionBehaviour.MATCH_CONSTRAINT) {
                                            final a e2 = widgetRun2.e;
                                            if (!e2.j) {
                                                int a;
                                                if (n9 > 0.0f) {
                                                    a = (int)(widgetRun2.b.D0[super.f] * n22 / n9 + 0.5f);
                                                }
                                                else {
                                                    a = n23;
                                                }
                                                int a2;
                                                int a3;
                                                if (super.f == 0) {
                                                    final ConstraintWidget b4 = widgetRun2.b;
                                                    a2 = b4.A;
                                                    a3 = b4.z;
                                                }
                                                else {
                                                    final ConstraintWidget b5 = widgetRun2.b;
                                                    a2 = b5.D;
                                                    a3 = b5.C;
                                                }
                                                int min;
                                                if (widgetRun2.a == 1) {
                                                    min = Math.min(a, e2.m);
                                                }
                                                else {
                                                    min = a;
                                                }
                                                int n28;
                                                final int b6 = n28 = Math.max(a3, min);
                                                if (a2 > 0) {
                                                    n28 = Math.min(a2, b6);
                                                }
                                                int n29 = a;
                                                int n30 = n26;
                                                if (n28 != a) {
                                                    n30 = n26 + 1;
                                                    n29 = n28;
                                                }
                                                widgetRun2.e.d(n29);
                                                n26 = n30;
                                            }
                                        }
                                    }
                                    ++l;
                                }
                                int n32;
                                if (n26 > 0) {
                                    final int n31 = n18 - n26;
                                    int index3 = 0;
                                    n32 = 0;
                                    while (index3 < size) {
                                        final WidgetRun widgetRun3 = this.k.get(index3);
                                        if (widgetRun3.b.V() != 8) {
                                            int n33 = n32;
                                            if (index3 > 0) {
                                                n33 = n32;
                                                if (index3 >= n3) {
                                                    n33 = n32 + widgetRun3.h.f;
                                                }
                                            }
                                            final int n34 = n32 = n33 + widgetRun3.e.g;
                                            if (index3 < n4) {
                                                n32 = n34;
                                                if (index3 < n5) {
                                                    n32 = n34 + -widgetRun3.i.f;
                                                }
                                            }
                                        }
                                        ++index3;
                                    }
                                    n18 = n31;
                                }
                                else {
                                    n32 = n25;
                                }
                                if (this.l == 2 && n26 == 0) {
                                    this.l = 0;
                                    n6 = n32;
                                    n35 = n18;
                                    n20 = n27;
                                }
                                else {
                                    n6 = n32;
                                    n35 = n18;
                                    n20 = n27;
                                }
                            }
                            else {
                                n35 = n18;
                            }
                            if (n6 > n) {
                                this.l = 2;
                            }
                            if (n17 > 0 && n35 == 0 && n3 == n5) {
                                this.l = 2;
                            }
                            final int l2 = this.l;
                            if (l2 == 1) {
                                int n36;
                                if (n17 > 1) {
                                    n36 = (n - n6) / (n17 - 1);
                                }
                                else if (n17 == 1) {
                                    n36 = (n - n6) / 2;
                                }
                                else {
                                    n36 = 0;
                                }
                                int n37 = n36;
                                if (n35 > 0) {
                                    n37 = 0;
                                }
                                int n38 = 0;
                                int n39 = n20;
                                while (n38 < size) {
                                    int index4;
                                    if (b) {
                                        index4 = size - (n38 + 1);
                                    }
                                    else {
                                        index4 = n38;
                                    }
                                    final WidgetRun widgetRun4 = this.k.get(index4);
                                    int n40;
                                    if (widgetRun4.b.V() == 8) {
                                        widgetRun4.h.d(n39);
                                        widgetRun4.i.d(n39);
                                        n40 = n39;
                                    }
                                    else {
                                        int n41 = n39;
                                        if (n38 > 0) {
                                            if (b) {
                                                n41 = n39 - n37;
                                            }
                                            else {
                                                n41 = n39 + n37;
                                            }
                                        }
                                        int n42 = n41;
                                        if (n38 > 0) {
                                            n42 = n41;
                                            if (n38 >= n3) {
                                                final int f2 = widgetRun4.h.f;
                                                if (b) {
                                                    n42 = n41 - f2;
                                                }
                                                else {
                                                    n42 = n41 + f2;
                                                }
                                            }
                                        }
                                        DependencyNode dependencyNode;
                                        if (b) {
                                            dependencyNode = widgetRun4.i;
                                        }
                                        else {
                                            dependencyNode = widgetRun4.h;
                                        }
                                        dependencyNode.d(n42);
                                        final a e3 = widgetRun4.e;
                                        int n44;
                                        final int n43 = n44 = e3.g;
                                        if (widgetRun4.d == ConstraintWidget.DimensionBehaviour.MATCH_CONSTRAINT) {
                                            n44 = n43;
                                            if (widgetRun4.a == 1) {
                                                n44 = e3.m;
                                            }
                                        }
                                        int n45;
                                        if (b) {
                                            n45 = n42 - n44;
                                        }
                                        else {
                                            n45 = n42 + n44;
                                        }
                                        DependencyNode dependencyNode2;
                                        if (b) {
                                            dependencyNode2 = widgetRun4.h;
                                        }
                                        else {
                                            dependencyNode2 = widgetRun4.i;
                                        }
                                        dependencyNode2.d(n45);
                                        widgetRun4.g = true;
                                        n40 = n45;
                                        if (n38 < n4) {
                                            n40 = n45;
                                            if (n38 < n5) {
                                                final int n46 = -widgetRun4.i.f;
                                                if (b) {
                                                    n40 = n45 - n46;
                                                }
                                                else {
                                                    n40 = n45 + n46;
                                                }
                                            }
                                        }
                                    }
                                    ++n38;
                                    n39 = n40;
                                }
                                return;
                            }
                            if (l2 == 0) {
                                int n47 = (n - n6) / (n17 + 1);
                                if (n35 > 0) {
                                    n47 = 0;
                                }
                                for (int n48 = 0; n48 < size; ++n48) {
                                    int index5;
                                    if (b) {
                                        index5 = size - (n48 + 1);
                                    }
                                    else {
                                        index5 = n48;
                                    }
                                    final WidgetRun widgetRun5 = this.k.get(index5);
                                    if (widgetRun5.b.V() == 8) {
                                        widgetRun5.h.d(n20);
                                        widgetRun5.i.d(n20);
                                    }
                                    else {
                                        int n49;
                                        if (b) {
                                            n49 = n20 - n47;
                                        }
                                        else {
                                            n49 = n20 + n47;
                                        }
                                        int n50 = n49;
                                        if (n48 > 0) {
                                            n50 = n49;
                                            if (n48 >= n3) {
                                                final int f3 = widgetRun5.h.f;
                                                if (b) {
                                                    n50 = n49 - f3;
                                                }
                                                else {
                                                    n50 = n49 + f3;
                                                }
                                            }
                                        }
                                        DependencyNode dependencyNode3;
                                        if (b) {
                                            dependencyNode3 = widgetRun5.i;
                                        }
                                        else {
                                            dependencyNode3 = widgetRun5.h;
                                        }
                                        dependencyNode3.d(n50);
                                        final a e4 = widgetRun5.e;
                                        int n51;
                                        final int a4 = n51 = e4.g;
                                        if (widgetRun5.d == ConstraintWidget.DimensionBehaviour.MATCH_CONSTRAINT) {
                                            n51 = a4;
                                            if (widgetRun5.a == 1) {
                                                n51 = Math.min(a4, e4.m);
                                            }
                                        }
                                        int n52;
                                        if (b) {
                                            n52 = n50 - n51;
                                        }
                                        else {
                                            n52 = n50 + n51;
                                        }
                                        DependencyNode dependencyNode4;
                                        if (b) {
                                            dependencyNode4 = widgetRun5.h;
                                        }
                                        else {
                                            dependencyNode4 = widgetRun5.i;
                                        }
                                        dependencyNode4.d(n52);
                                        n20 = n52;
                                        if (n48 < n4) {
                                            n20 = n52;
                                            if (n48 < n5) {
                                                final int n53 = -widgetRun5.i.f;
                                                if (b) {
                                                    n20 = n52 - n53;
                                                }
                                                else {
                                                    n20 = n52 + n53;
                                                }
                                            }
                                        }
                                    }
                                }
                                return;
                            }
                            if (l2 == 2) {
                                float n54;
                                if (super.f == 0) {
                                    n54 = super.b.y();
                                }
                                else {
                                    n54 = super.b.R();
                                }
                                float n55 = n54;
                                if (b) {
                                    n55 = 1.0f - n54;
                                }
                                int n56 = (int)((n - n6) * n55 + 0.5f);
                                if (n56 < 0 || n35 > 0) {
                                    n56 = 0;
                                }
                                int n57;
                                if (b) {
                                    n57 = n20 - n56;
                                }
                                else {
                                    n57 = n20 + n56;
                                }
                                for (int n58 = 0; n58 < size; ++n58) {
                                    int index6;
                                    if (b) {
                                        index6 = size - (n58 + 1);
                                    }
                                    else {
                                        index6 = n58;
                                    }
                                    final WidgetRun widgetRun6 = this.k.get(index6);
                                    if (widgetRun6.b.V() == 8) {
                                        widgetRun6.h.d(n57);
                                        widgetRun6.i.d(n57);
                                    }
                                    else {
                                        int n59 = n57;
                                        if (n58 > 0) {
                                            n59 = n57;
                                            if (n58 >= n3) {
                                                final int f4 = widgetRun6.h.f;
                                                if (b) {
                                                    n59 = n57 - f4;
                                                }
                                                else {
                                                    n59 = n57 + f4;
                                                }
                                            }
                                        }
                                        DependencyNode dependencyNode5;
                                        if (b) {
                                            dependencyNode5 = widgetRun6.i;
                                        }
                                        else {
                                            dependencyNode5 = widgetRun6.h;
                                        }
                                        dependencyNode5.d(n59);
                                        final a e5 = widgetRun6.e;
                                        int n60 = e5.g;
                                        if (widgetRun6.d == ConstraintWidget.DimensionBehaviour.MATCH_CONSTRAINT && widgetRun6.a == 1) {
                                            n60 = e5.m;
                                        }
                                        int n61;
                                        if (b) {
                                            n61 = n59 - n60;
                                        }
                                        else {
                                            n61 = n59 + n60;
                                        }
                                        DependencyNode dependencyNode6;
                                        if (b) {
                                            dependencyNode6 = widgetRun6.h;
                                        }
                                        else {
                                            dependencyNode6 = widgetRun6.i;
                                        }
                                        dependencyNode6.d(n61);
                                        n57 = n61;
                                        if (n58 < n4) {
                                            n57 = n61;
                                            if (n58 < n5) {
                                                final int n62 = -widgetRun6.i.f;
                                                if (b) {
                                                    n57 = n61 - n62;
                                                }
                                                else {
                                                    n57 = n61 + n62;
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                            return;
                        }
                    }
                    final int n17 = 0;
                    int n6 = 0;
                    int n18 = 0;
                    float n9 = 0.0f;
                    continue;
                }
            }
        }
    }
    
    @Override
    public void d() {
        final Iterator iterator = this.k.iterator();
        while (iterator.hasNext()) {
            ((WidgetRun)iterator.next()).d();
        }
        final int size = this.k.size();
        if (size < 1) {
            return;
        }
        final ConstraintWidget b = this.k.get(0).b;
        final ConstraintWidget b2 = this.k.get(size - 1).b;
        Label_0280: {
            DependencyNode dependencyNode;
            int n2;
            if (super.f == 0) {
                final ConstraintAnchor o = b.O;
                final ConstraintAnchor q = b2.Q;
                final DependencyNode i = this.i(o, 0);
                int n = o.f();
                final ConstraintWidget r = this.r();
                if (r != null) {
                    n = r.O.f();
                }
                if (i != null) {
                    this.b(super.h, i, n);
                }
                dependencyNode = this.i(q, 0);
                n2 = q.f();
                final ConstraintWidget s = this.s();
                if (s != null) {
                    n2 = s.Q.f();
                }
                if (dependencyNode == null) {
                    break Label_0280;
                }
            }
            else {
                final ConstraintAnchor p = b.P;
                final ConstraintAnchor r2 = b2.R;
                final DependencyNode j = this.i(p, 1);
                int n3 = p.f();
                final ConstraintWidget r3 = this.r();
                if (r3 != null) {
                    n3 = r3.P.f();
                }
                if (j != null) {
                    this.b(super.h, j, n3);
                }
                dependencyNode = this.i(r2, 1);
                n2 = r2.f();
                final ConstraintWidget s2 = this.s();
                if (s2 != null) {
                    n2 = s2.R.f();
                }
                if (dependencyNode == null) {
                    break Label_0280;
                }
            }
            this.b(super.i, dependencyNode, -n2);
        }
        super.h.a = this;
        super.i.a = this;
    }
    
    @Override
    public void e() {
        for (int i = 0; i < this.k.size(); ++i) {
            ((WidgetRun)this.k.get(i)).e();
        }
    }
    
    @Override
    public void f() {
        super.c = null;
        final Iterator iterator = this.k.iterator();
        while (iterator.hasNext()) {
            ((WidgetRun)iterator.next()).f();
        }
    }
    
    @Override
    public long j() {
        final int size = this.k.size();
        long n = 0L;
        for (int i = 0; i < size; ++i) {
            final WidgetRun widgetRun = this.k.get(i);
            n = n + widgetRun.h.f + widgetRun.j() + widgetRun.i.f;
        }
        return n;
    }
    
    @Override
    public boolean m() {
        for (int size = this.k.size(), i = 0; i < size; ++i) {
            if (!((WidgetRun)this.k.get(i)).m()) {
                return false;
            }
        }
        return true;
    }
    
    public final void q() {
        ConstraintWidget constraintWidget = super.b;
        ConstraintWidget b;
        do {
            b = constraintWidget;
            constraintWidget = b.L(super.f);
        } while (constraintWidget != null);
        super.b = b;
        this.k.add(b.N(super.f));
        for (ConstraintWidget constraintWidget2 = b.J(super.f); constraintWidget2 != null; constraintWidget2 = constraintWidget2.J(super.f)) {
            this.k.add(constraintWidget2.N(super.f));
        }
        for (final WidgetRun widgetRun : this.k) {
            final int f = super.f;
            if (f == 0) {
                widgetRun.b.c = this;
            }
            else {
                if (f != 1) {
                    continue;
                }
                widgetRun.b.d = this;
            }
        }
        if (super.f == 0 && ((d)super.b.K()).Q1() && this.k.size() > 1) {
            final ArrayList k = this.k;
            super.b = ((WidgetRun)k.get(k.size() - 1)).b;
        }
        int l;
        if (super.f == 0) {
            l = super.b.z();
        }
        else {
            l = super.b.S();
        }
        this.l = l;
    }
    
    public final ConstraintWidget r() {
        for (int i = 0; i < this.k.size(); ++i) {
            final WidgetRun widgetRun = this.k.get(i);
            if (widgetRun.b.V() != 8) {
                return widgetRun.b;
            }
        }
        return null;
    }
    
    public final ConstraintWidget s() {
        for (int i = this.k.size() - 1; i >= 0; --i) {
            final WidgetRun widgetRun = this.k.get(i);
            if (widgetRun.b.V() != 8) {
                return widgetRun.b;
            }
        }
        return null;
    }
    
    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("ChainRun ");
        String str;
        if (super.f == 0) {
            str = "horizontal : ";
        }
        else {
            str = "vertical : ";
        }
        sb.append(str);
        for (final WidgetRun obj : this.k) {
            sb.append("<");
            sb.append(obj);
            sb.append("> ");
        }
        return sb.toString();
    }
}

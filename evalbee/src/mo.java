// 
// Decompiled by Procyon v0.6.0
// 

public abstract class mo
{
    public zl a;
    public ub0 b;
    public boolean c;
    
    public mo(final zl zl, final ub0 ub0) {
        this.c = false;
        this.a(zl);
        this.b(ub0);
    }
    
    public void a(final zl a) {
        if (a != null) {
            this.a = a;
            return;
        }
        throw new IllegalArgumentException("ControlPath cannot be null.");
    }
    
    public void b(final ub0 b) {
        if (b != null) {
            this.b = b;
            return;
        }
        throw new IllegalArgumentException("GroupIterator cannot be null.");
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

public abstract class wg0
{
    public static final int a;
    
    static {
        a = a();
    }
    
    public static int a() {
        return c(System.getProperty("java.version"));
    }
    
    public static int b(final String s) {
        try {
            final StringBuilder sb = new StringBuilder();
            for (int i = 0; i < s.length(); ++i) {
                final char char1 = s.charAt(i);
                if (!Character.isDigit(char1)) {
                    break;
                }
                sb.append(char1);
            }
            return Integer.parseInt(sb.toString());
        }
        catch (final NumberFormatException ex) {
            return -1;
        }
    }
    
    public static int c(final String s) {
        int n;
        if ((n = e(s)) == -1) {
            n = b(s);
        }
        if (n == -1) {
            return 6;
        }
        return n;
    }
    
    public static boolean d() {
        return wg0.a >= 9;
    }
    
    public static int e(final String s) {
        try {
            final String[] split = s.split("[._]");
            final int int1 = Integer.parseInt(split[0]);
            if (int1 == 1 && split.length > 1) {
                return Integer.parseInt(split[1]);
            }
            return int1;
        }
        catch (final NumberFormatException ex) {
            return -1;
        }
    }
}

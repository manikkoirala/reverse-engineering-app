import java.util.HashSet;
import android.app.NotificationManager;
import android.content.Context;
import java.util.Set;

// 
// Decompiled by Procyon v0.6.0
// 

public final class i01
{
    public static final Object c;
    public static Set d;
    public static final Object e;
    public final Context a;
    public final NotificationManager b;
    
    static {
        c = new Object();
        i01.d = new HashSet();
        e = new Object();
    }
    
    public i01(final Context a) {
        this.a = a;
        this.b = (NotificationManager)a.getSystemService("notification");
    }
    
    public static i01 b(final Context context) {
        return new i01(context);
    }
    
    public boolean a() {
        return this.b.areNotificationsEnabled();
    }
}

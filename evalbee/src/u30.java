import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Tasks;
import java.util.concurrent.Executor;
import io.grpc.CallCredentials$RequestInfo;
import io.grpc.Status;
import com.google.firebase.internal.api.FirebaseNoSignedInUserException;
import com.google.firebase.FirebaseApiNotAvailableException;
import com.google.firebase.firestore.util.Logger;
import io.grpc.CallCredentials$MetadataApplier;
import com.google.android.gms.tasks.Task;
import io.grpc.Metadata$AsciiMarshaller;
import io.grpc.Metadata;
import io.grpc.Metadata$Key;
import io.grpc.CallCredentials;

// 
// Decompiled by Procyon v0.6.0
// 

public final class u30 extends CallCredentials
{
    public static final Metadata$Key c;
    public static final Metadata$Key d;
    public final eo a;
    public final eo b;
    
    static {
        final Metadata$AsciiMarshaller ascii_STRING_MARSHALLER = Metadata.ASCII_STRING_MARSHALLER;
        c = Metadata$Key.of("Authorization", ascii_STRING_MARSHALLER);
        d = Metadata$Key.of("x-firebase-appcheck", ascii_STRING_MARSHALLER);
    }
    
    public u30(final eo a, final eo b) {
        this.a = a;
        this.b = b;
    }
    
    public void applyRequestMetadata(final CallCredentials$RequestInfo callCredentials$RequestInfo, final Executor executor, final CallCredentials$MetadataApplier callCredentials$MetadataApplier) {
        final Task a = this.a.a();
        final Task a2 = this.b.a();
        Tasks.whenAll(new Task[] { a, a2 }).addOnCompleteListener(wy.b, (OnCompleteListener)new t30(a, callCredentials$MetadataApplier, a2));
    }
    
    public void thisUsesUnstableApi() {
    }
}

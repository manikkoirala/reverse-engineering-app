import com.google.protobuf.GeneratedMessageLite;
import com.google.firestore.v1.Value;

// 
// Decompiled by Procyon v0.6.0
// 

public class q01 implements oy1
{
    public Value a;
    
    public q01(final Value a) {
        g9.d(a32.A(a), "NumericIncrementTransformOperation expects a NumberValue operand", new Object[0]);
        this.a = a;
    }
    
    @Override
    public Value a(Value value) {
        if (!a32.A(value)) {
            value = (Value)((GeneratedMessageLite.a)Value.x0().G(0L)).p();
        }
        return value;
    }
    
    @Override
    public Value b(final Value value, final pw1 pw1) {
        final Value a = this.a(value);
        Value.b b;
        if (a32.v(a) && a32.v(this.a)) {
            b = Value.x0().G(this.g(a.r0(), this.f()));
        }
        else {
            double p2;
            if (a32.v(a)) {
                p2 = (double)a.r0();
            }
            else {
                g9.d(a32.u(a), "Expected NumberValue to be of type DoubleValue, but was ", value.getClass().getCanonicalName());
                p2 = a.p0();
            }
            b = Value.x0().E(p2 + this.e());
        }
        return (Value)((GeneratedMessageLite.a)b).p();
    }
    
    @Override
    public Value c(final Value value, final Value value2) {
        return value2;
    }
    
    public Value d() {
        return this.a;
    }
    
    public final double e() {
        if (a32.u(this.a)) {
            return this.a.p0();
        }
        if (a32.v(this.a)) {
            return (double)this.a.r0();
        }
        final StringBuilder sb = new StringBuilder();
        sb.append("Expected 'operand' to be of Number type, but was ");
        sb.append(this.a.getClass().getCanonicalName());
        throw g9.a(sb.toString(), new Object[0]);
    }
    
    public final long f() {
        if (a32.u(this.a)) {
            return (long)this.a.p0();
        }
        if (a32.v(this.a)) {
            return this.a.r0();
        }
        final StringBuilder sb = new StringBuilder();
        sb.append("Expected 'operand' to be of Number type, but was ");
        sb.append(this.a.getClass().getCanonicalName());
        throw g9.a(sb.toString(), new Object[0]);
    }
    
    public final long g(final long n, final long n2) {
        final long n3 = n + n2;
        if (((n ^ n3) & (n2 ^ n3)) >= 0L) {
            return n3;
        }
        if (n3 >= 0L) {
            return Long.MIN_VALUE;
        }
        return Long.MAX_VALUE;
    }
}

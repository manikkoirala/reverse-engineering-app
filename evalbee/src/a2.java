import java.util.Set;
import android.net.Uri;
import com.google.android.gms.common.internal.Preconditions;
import java.util.Map;
import java.util.HashMap;
import com.google.android.gms.internal.firebase-auth-api.zzat;

// 
// Decompiled by Procyon v0.6.0
// 

public class a2
{
    public static final zzat g;
    public final String a;
    public final String b;
    public final String c;
    public final String d;
    public final String e;
    public final String f;
    
    static {
        final HashMap hashMap = new HashMap();
        hashMap.put("recoverEmail", 2);
        hashMap.put("resetPassword", 0);
        hashMap.put("signIn", 4);
        hashMap.put("verifyEmail", 1);
        hashMap.put("verifyBeforeChangeEmail", 5);
        hashMap.put("revertSecondFactorAddition", 6);
        g = com.google.android.gms.internal.firebase_auth_api.zzat.zza((Map)hashMap);
    }
    
    public a2(final String s) {
        final String e = e(s, "apiKey");
        final String e2 = e(s, "oobCode");
        final String e3 = e(s, "mode");
        if (e != null && e2 != null && e3 != null) {
            this.a = Preconditions.checkNotEmpty(e);
            this.b = Preconditions.checkNotEmpty(e2);
            this.c = Preconditions.checkNotEmpty(e3);
            this.d = e(s, "continueUrl");
            this.e = e(s, "languageCode");
            this.f = e(s, "tenantId");
            return;
        }
        throw new IllegalArgumentException(String.format("%s, %s and %s are required in a valid action code URL", "apiKey", "oobCode", "mode"));
    }
    
    public static a2 c(final String s) {
        Preconditions.checkNotEmpty(s);
        try {
            return new a2(s);
        }
        catch (final IllegalArgumentException ex) {
            return null;
        }
    }
    
    public static String e(String queryParameter, final String s) {
        final Uri parse = Uri.parse(queryParameter);
        try {
            final Set queryParameterNames = parse.getQueryParameterNames();
            if (queryParameterNames.contains(s)) {
                return parse.getQueryParameter(s);
            }
            if (queryParameterNames.contains("link")) {
                queryParameter = Uri.parse(Preconditions.checkNotEmpty(parse.getQueryParameter("link"))).getQueryParameter(s);
                return queryParameter;
            }
            return null;
        }
        catch (final UnsupportedOperationException | NullPointerException ex) {
            return null;
        }
    }
    
    public String a() {
        return this.b;
    }
    
    public int b() {
        final zzat g = a2.g;
        if (((com.google.android.gms.internal.firebase_auth_api.zzat)g).containsKey((Object)this.c)) {
            return (int)((com.google.android.gms.internal.firebase_auth_api.zzat)g).get((Object)this.c);
        }
        return 3;
    }
    
    public final String d() {
        return this.f;
    }
}

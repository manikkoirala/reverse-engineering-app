import com.google.android.gms.internal.firebase_auth_api.zzaai;
import android.util.Log;
import android.text.TextUtils;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;

// 
// Decompiled by Procyon v0.6.0
// 

public final class qi2 extends ld2
{
    public final String a;
    public final z1 b;
    public final FirebaseAuth c;
    
    public qi2(final FirebaseAuth c, final String a, final z1 b) {
        this.c = c;
        this.a = a;
        this.b = b;
    }
    
    @Override
    public final Task c(final String s) {
        StringBuilder sb;
        String a2;
        if (TextUtils.isEmpty((CharSequence)s)) {
            final String a = this.a;
            sb = new StringBuilder("Email link sign in for ");
            sb.append(a);
            a2 = " with empty reCAPTCHA token";
        }
        else {
            a2 = this.a;
            sb = new StringBuilder("Got reCAPTCHA token for email link sign in for ");
        }
        sb.append(a2);
        Log.i("FirebaseAuth", sb.toString());
        return ((zzaai)FirebaseAuth.E(this.c)).zzb(FirebaseAuth.o(this.c), this.a, this.b, FirebaseAuth.I(this.c), s);
    }
}

import android.content.DialogInterface;
import android.content.DialogInterface$OnClickListener;
import android.view.View$OnClickListener;
import android.view.View;
import android.widget.AdapterView;
import androidx.appcompat.app.a;
import android.widget.AdapterView$OnItemClickListener;
import android.widget.Button;
import android.widget.ListAdapter;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.view.ViewGroup;
import android.view.LayoutInflater;
import com.google.android.material.dialog.MaterialAlertDialogBuilder;
import android.content.Context;

// 
// Decompiled by Procyon v0.6.0
// 

public abstract class xs
{
    public static void a(final Context context, final int text, final int text2, final int n, final i i) {
        final MaterialAlertDialogBuilder materialAlertDialogBuilder = new MaterialAlertDialogBuilder(context, 2131951953);
        final View inflate = ((LayoutInflater)context.getSystemService("layout_inflater")).inflate(2131492956, (ViewGroup)null);
        materialAlertDialogBuilder.setView(inflate);
        final a create = materialAlertDialogBuilder.create();
        final TextView textView = (TextView)inflate.findViewById(2131297204);
        final TextView textView2 = (TextView)inflate.findViewById(2131297205);
        final ListView listView = (ListView)inflate.findViewById(2131296834);
        final String[] stringArray = context.getResources().getStringArray(n);
        listView.setAdapter((ListAdapter)new ArrayAdapter(context, 2131492892, (Object[])stringArray));
        final Button button = (Button)inflate.findViewById(2131296556);
        textView2.setText(text);
        if (text2 == 0) {
            ((View)textView).setVisibility(8);
        }
        else {
            textView.setText(text2);
        }
        ((AdapterView)listView).setOnItemClickListener((AdapterView$OnItemClickListener)new AdapterView$OnItemClickListener(i, stringArray, create) {
            public final i a;
            public final String[] b;
            public final a c;
            
            public void onItemClick(final AdapterView adapterView, final View view, final int n, final long n2) {
                final i a = this.a;
                if (a != null) {
                    a.a(n, this.b[n]);
                }
                this.c.dismiss();
            }
        });
        ((View)button).setOnClickListener((View$OnClickListener)new View$OnClickListener(create) {
            public final a a;
            
            public void onClick(final View view) {
                this.a.dismiss();
            }
        });
        create.show();
    }
    
    public static void b(final Context context, final int title, final int message, final int n, final int n2, final int n3, final int icon, final y01 y01, final y01 y2) {
        final MaterialAlertDialogBuilder materialAlertDialogBuilder = new MaterialAlertDialogBuilder(context, 2131951953);
        if (title != 0) {
            materialAlertDialogBuilder.setTitle(title);
        }
        materialAlertDialogBuilder.setMessage(message).setIcon(icon);
        materialAlertDialogBuilder.setPositiveButton(n, (DialogInterface$OnClickListener)new DialogInterface$OnClickListener(y01) {
            public final y01 a;
            
            public void onClick(final DialogInterface dialogInterface, final int n) {
                final y01 a = this.a;
                if (a != null) {
                    a.a(null);
                }
                dialogInterface.dismiss();
            }
        });
        if (n2 != 0) {
            materialAlertDialogBuilder.setNegativeButton(n2, (DialogInterface$OnClickListener)new DialogInterface$OnClickListener(y2) {
                public final y01 a;
                
                public void onClick(final DialogInterface dialogInterface, final int n) {
                    final y01 a = this.a;
                    if (a != null) {
                        a.a(null);
                    }
                    dialogInterface.dismiss();
                }
            });
        }
        if (n3 != 0) {
            materialAlertDialogBuilder.setNeutralButton(n3, (DialogInterface$OnClickListener)new DialogInterface$OnClickListener() {
                public void onClick(final DialogInterface dialogInterface, final int n) {
                    dialogInterface.dismiss();
                }
            });
        }
        materialAlertDialogBuilder.create().show();
    }
    
    public static void c(final Context context, final y01 y01, final int n, final int n2, final int n3, final int n4, final int n5, final int n6) {
        b(context, n, n2, n3, n4, n5, n6, y01, null);
    }
    
    public static void d(final Context context, final y01 y01, final int title, final String message, final int n, final int n2, final int n3, final int icon) {
        final MaterialAlertDialogBuilder materialAlertDialogBuilder = new MaterialAlertDialogBuilder(context, 2131951953);
        materialAlertDialogBuilder.setTitle(title).setMessage((CharSequence)message).setIcon(icon);
        materialAlertDialogBuilder.setPositiveButton(n, (DialogInterface$OnClickListener)new DialogInterface$OnClickListener(y01) {
            public final y01 a;
            
            public void onClick(final DialogInterface dialogInterface, final int n) {
                final y01 a = this.a;
                if (a != null) {
                    a.a(null);
                }
                dialogInterface.dismiss();
            }
        });
        if (n2 != 0) {
            materialAlertDialogBuilder.setNegativeButton(n2, (DialogInterface$OnClickListener)new DialogInterface$OnClickListener() {
                public void onClick(final DialogInterface dialogInterface, final int n) {
                    dialogInterface.dismiss();
                }
            });
        }
        if (n3 != 0) {
            materialAlertDialogBuilder.setNeutralButton(n3, (DialogInterface$OnClickListener)new DialogInterface$OnClickListener() {
                public void onClick(final DialogInterface dialogInterface, final int n) {
                    dialogInterface.dismiss();
                }
            });
        }
        materialAlertDialogBuilder.create().show();
    }
    
    public interface i
    {
        void a(final int p0, final String p1);
    }
}

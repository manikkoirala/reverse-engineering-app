import com.google.firestore.v1.Value;
import com.google.firebase.firestore.core.h;
import java.util.Arrays;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Collection;
import com.google.firebase.firestore.core.FieldFilter;
import java.util.ArrayList;
import com.google.firebase.firestore.core.CompositeFilter;

// 
// Decompiled by Procyon v0.6.0
// 

public abstract class am0
{
    public static i10 a(i10 i10) {
        f(i10);
        if (m(i10)) {
            return i10;
        }
        final CompositeFilter compositeFilter = (CompositeFilter)i10;
        final List b = compositeFilter.b();
        if (b.size() == 1) {
            return a((i10)b.get(0));
        }
        if (compositeFilter.h()) {
            return compositeFilter;
        }
        final ArrayList list = new ArrayList();
        final Iterator iterator = b.iterator();
        while (iterator.hasNext()) {
            list.add(a((i10)iterator.next()));
        }
        final ArrayList list2 = new ArrayList();
        final Iterator iterator2 = list.iterator();
        while (iterator2.hasNext()) {
            i10 = (i10)iterator2.next();
            if (!(i10 instanceof FieldFilter)) {
                if (!(i10 instanceof CompositeFilter)) {
                    continue;
                }
                final i10 i11 = i10 = i10;
                if (((CompositeFilter)i11).e().equals(compositeFilter.e())) {
                    list2.addAll(((CompositeFilter)i11).b());
                    continue;
                }
            }
            list2.add(i10);
        }
        if (list2.size() == 1) {
            return (i10)list2.get(0);
        }
        return new CompositeFilter(list2, compositeFilter.e());
    }
    
    public static i10 b(i10 i10, final i10 i11) {
        f(i10);
        f(i11);
        final boolean b = i10 instanceof FieldFilter;
        if (b && i11 instanceof FieldFilter) {
            i10 = e((FieldFilter)i10, (FieldFilter)i11);
        }
        else if (b && i11 instanceof CompositeFilter) {
            i10 = d((FieldFilter)i10, (CompositeFilter)i11);
        }
        else if (i10 instanceof CompositeFilter && i11 instanceof FieldFilter) {
            i10 = d((FieldFilter)i11, (CompositeFilter)i10);
        }
        else {
            i10 = c((CompositeFilter)i10, (CompositeFilter)i11);
        }
        return a(i10);
    }
    
    public static i10 c(final CompositeFilter compositeFilter, final CompositeFilter compositeFilter2) {
        g9.d(!compositeFilter.b().isEmpty() && !compositeFilter2.b().isEmpty(), "Found an empty composite filter", new Object[0]);
        if (compositeFilter.f() && compositeFilter2.f()) {
            return compositeFilter.j(compositeFilter2.b());
        }
        CompositeFilter compositeFilter3;
        if (compositeFilter.g()) {
            compositeFilter3 = compositeFilter;
        }
        else {
            compositeFilter3 = compositeFilter2;
        }
        CompositeFilter compositeFilter4 = compositeFilter;
        if (compositeFilter.g()) {
            compositeFilter4 = compositeFilter2;
        }
        final ArrayList list = new ArrayList();
        final Iterator iterator = compositeFilter3.b().iterator();
        while (iterator.hasNext()) {
            list.add(b((i10)iterator.next(), compositeFilter4));
        }
        return new CompositeFilter(list, CompositeFilter.Operator.OR);
    }
    
    public static i10 d(final FieldFilter o, final CompositeFilter compositeFilter) {
        if (compositeFilter.f()) {
            return compositeFilter.j(Collections.singletonList(o));
        }
        final ArrayList list = new ArrayList();
        final Iterator iterator = compositeFilter.b().iterator();
        while (iterator.hasNext()) {
            list.add(b(o, (i10)iterator.next()));
        }
        return new CompositeFilter(list, CompositeFilter.Operator.OR);
    }
    
    public static i10 e(final FieldFilter fieldFilter, final FieldFilter fieldFilter2) {
        return new CompositeFilter(Arrays.asList(fieldFilter, fieldFilter2), CompositeFilter.Operator.AND);
    }
    
    public static void f(final i10 i10) {
        g9.d(i10 instanceof FieldFilter || i10 instanceof CompositeFilter, "Only field filters and composite filters are accepted.", new Object[0]);
    }
    
    public static i10 g(i10 i10) {
        f(i10);
        if (i10 instanceof FieldFilter) {
            return i10;
        }
        final CompositeFilter compositeFilter = (CompositeFilter)i10;
        final int size = compositeFilter.b().size();
        int j = 1;
        if (size == 1) {
            return g(i10.b().get(0));
        }
        final ArrayList list = new ArrayList();
        final Iterator iterator = compositeFilter.b().iterator();
        while (iterator.hasNext()) {
            list.add(g((i10)iterator.next()));
        }
        i10 = a(new CompositeFilter(list, compositeFilter.e()));
        if (k(i10)) {
            return i10;
        }
        g9.d(i10 instanceof CompositeFilter, "field filters are already in DNF form.", new Object[0]);
        final CompositeFilter compositeFilter2 = (CompositeFilter)i10;
        g9.d(compositeFilter2.f(), "Disjunction of filters all of which are already in DNF form is itself in DNF form.", new Object[0]);
        g9.d(compositeFilter2.b().size() > 1, "Single-filter composite filters are already in DNF form.", new Object[0]);
        i10 = (i10)compositeFilter2.b().get(0);
        while (j < compositeFilter2.b().size()) {
            i10 = b(i10, (i10)compositeFilter2.b().get(j));
            ++j;
        }
        return i10;
    }
    
    public static i10 h(final i10 i10) {
        f(i10);
        final ArrayList list = new ArrayList();
        if (i10 instanceof FieldFilter) {
            i10 i11 = i10;
            if (i10 instanceof h) {
                final h h = (h)i10;
                final Iterator iterator = h.h().l0().h().iterator();
                while (iterator.hasNext()) {
                    list.add(FieldFilter.e(h.f(), FieldFilter.Operator.EQUAL, (Value)iterator.next()));
                }
                i11 = new CompositeFilter(list, CompositeFilter.Operator.OR);
            }
            return i11;
        }
        final CompositeFilter compositeFilter = (CompositeFilter)i10;
        final Iterator iterator2 = compositeFilter.b().iterator();
        while (iterator2.hasNext()) {
            list.add(h((i10)iterator2.next()));
        }
        return new CompositeFilter(list, compositeFilter.e());
    }
    
    public static List i(final CompositeFilter compositeFilter) {
        if (compositeFilter.b().isEmpty()) {
            return Collections.emptyList();
        }
        final i10 g = g(h(compositeFilter));
        g9.d(k(g), "computeDistributedNormalForm did not result in disjunctive normal form", new Object[0]);
        if (!m(g) && !l(g)) {
            return g.b();
        }
        return Collections.singletonList(g);
    }
    
    public static boolean j(final i10 i10) {
        if (i10 instanceof CompositeFilter) {
            final CompositeFilter compositeFilter = (CompositeFilter)i10;
            if (compositeFilter.g()) {
                for (final i10 i11 : compositeFilter.b()) {
                    if (!m(i11) && !l(i11)) {
                        return false;
                    }
                }
                return true;
            }
        }
        return false;
    }
    
    public static boolean k(final i10 i10) {
        return m(i10) || l(i10) || j(i10);
    }
    
    public static boolean l(final i10 i10) {
        return i10 instanceof CompositeFilter && ((CompositeFilter)i10).i();
    }
    
    public static boolean m(final i10 i10) {
        return i10 instanceof FieldFilter;
    }
}

import java.nio.ByteOrder;
import java.nio.ByteBuffer;
import java.io.IOException;

// 
// Decompiled by Procyon v0.6.0
// 

public abstract class fw0
{
    public static b a(final c c) {
        c.skip(4);
        final int unsignedShort = c.readUnsignedShort();
        if (unsignedShort <= 100) {
            c.skip(6);
            final int n = 0;
            while (true) {
                for (int i = 0; i < unsignedShort; ++i) {
                    final int a = c.a();
                    c.skip(4);
                    final long b = c.b();
                    c.skip(4);
                    if (1835365473 == a) {
                        if (b != -1L) {
                            c.skip((int)(b - c.getPosition()));
                            c.skip(12);
                            final long b2 = c.b();
                            for (int n2 = n; n2 < b2; ++n2) {
                                final int a2 = c.a();
                                final long b3 = c.b();
                                final long b4 = c.b();
                                if (1164798569 == a2 || 1701669481 == a2) {
                                    return new b(b3 + b, b4);
                                }
                            }
                        }
                        throw new IOException("Cannot read metadata.");
                    }
                }
                final long b = -1L;
                continue;
            }
        }
        throw new IOException("Cannot read metadata.");
    }
    
    public static ew0 b(ByteBuffer duplicate) {
        duplicate = duplicate.duplicate();
        duplicate.position((int)a((c)new a(duplicate)).a());
        return ew0.h(duplicate);
    }
    
    public static long c(final int n) {
        return (long)n & 0xFFFFFFFFL;
    }
    
    public static int d(final short n) {
        return n & 0xFFFF;
    }
    
    public static class a implements c
    {
        public final ByteBuffer a;
        
        public a(final ByteBuffer a) {
            (this.a = a).order(ByteOrder.BIG_ENDIAN);
        }
        
        @Override
        public int a() {
            return this.a.getInt();
        }
        
        @Override
        public long b() {
            return fw0.c(this.a.getInt());
        }
        
        @Override
        public long getPosition() {
            return this.a.position();
        }
        
        @Override
        public int readUnsignedShort() {
            return fw0.d(this.a.getShort());
        }
        
        @Override
        public void skip(final int n) {
            final ByteBuffer a = this.a;
            a.position(a.position() + n);
        }
    }
    
    public interface c
    {
        int a();
        
        long b();
        
        long getPosition();
        
        int readUnsignedShort();
        
        void skip(final int p0);
    }
    
    public static class b
    {
        public final long a;
        public final long b;
        
        public b(final long a, final long b) {
            this.a = a;
            this.b = b;
        }
        
        public long a() {
            return this.a;
        }
    }
}

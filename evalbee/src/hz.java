import com.google.firestore.v1.c;

// 
// Decompiled by Procyon v0.6.0
// 

public final class hz
{
    public final int a;
    public c b;
    
    public hz(final int a, final c b) {
        this.a = a;
        this.b = b;
    }
    
    public int a() {
        return this.a;
    }
    
    public c b() {
        return this.b;
    }
    
    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append("ExistenceFilter{count=");
        sb.append(this.a);
        sb.append(", unchangedNames=");
        sb.append(this.b);
        sb.append('}');
        return sb.toString();
    }
}

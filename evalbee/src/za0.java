// 
// Decompiled by Procyon v0.6.0
// 

public final class za0
{
    public final String a;
    public final boolean b;
    
    public za0(final String a, final boolean b) {
        fg0.e((Object)a, "adsSdkName");
        this.a = a;
        this.b = b;
    }
    
    public final String a() {
        return this.a;
    }
    
    public final boolean b() {
        return this.b;
    }
    
    @Override
    public boolean equals(final Object o) {
        boolean b = true;
        if (this == o) {
            return true;
        }
        if (!(o instanceof za0)) {
            return false;
        }
        final String a = this.a;
        final za0 za0 = (za0)o;
        if (!fg0.a((Object)a, (Object)za0.a) || this.b != za0.b) {
            b = false;
        }
        return b;
    }
    
    @Override
    public int hashCode() {
        return this.a.hashCode() * 31 + Boolean.hashCode(this.b);
    }
    
    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append("GetTopicsRequest: adsSdkName=");
        sb.append(this.a);
        sb.append(", shouldRecordObservation=");
        sb.append(this.b);
        return sb.toString();
    }
    
    public static final class a
    {
        public String a;
        public boolean b;
        
        public a() {
            this.a = "";
            this.b = true;
        }
        
        public final za0 a() {
            if (this.a.length() > 0) {
                return new za0(this.a, this.b);
            }
            throw new IllegalStateException("adsSdkName must be set".toString());
        }
        
        public final a b(final String a) {
            fg0.e((Object)a, "adsSdkName");
            this.a = a;
            return this;
        }
        
        public final a c(final boolean b) {
            this.b = b;
            return this;
        }
    }
}

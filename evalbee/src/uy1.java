import java.util.Iterator;
import android.view.ViewGroup;
import android.util.AndroidRuntimeException;
import android.animation.TimeInterpolator;
import android.view.View;
import java.util.ArrayList;

// 
// Decompiled by Procyon v0.6.0
// 

public class uy1 extends qy1
{
    public ArrayList a;
    public boolean b;
    public int c;
    public boolean d;
    public int e;
    
    public uy1() {
        this.a = new ArrayList();
        this.b = true;
        this.d = false;
        this.e = 0;
    }
    
    public final void A(final qy1 e) {
        this.a.add(e);
        e.mParent = this;
    }
    
    public qy1 B(final int index) {
        if (index >= 0 && index < this.a.size()) {
            return this.a.get(index);
        }
        return null;
    }
    
    public int C() {
        return this.a.size();
    }
    
    public uy1 D(final g g) {
        return (uy1)super.removeListener(g);
    }
    
    public uy1 E(final int n) {
        for (int i = 0; i < this.a.size(); ++i) {
            ((qy1)this.a.get(i)).removeTarget(n);
        }
        return (uy1)super.removeTarget(n);
    }
    
    public uy1 F(final View view) {
        for (int i = 0; i < this.a.size(); ++i) {
            ((qy1)this.a.get(i)).removeTarget(view);
        }
        return (uy1)super.removeTarget(view);
    }
    
    public uy1 G(final Class clazz) {
        for (int i = 0; i < this.a.size(); ++i) {
            ((qy1)this.a.get(i)).removeTarget(clazz);
        }
        return (uy1)super.removeTarget(clazz);
    }
    
    public uy1 H(final String s) {
        for (int i = 0; i < this.a.size(); ++i) {
            ((qy1)this.a.get(i)).removeTarget(s);
        }
        return (uy1)super.removeTarget(s);
    }
    
    public uy1 I(final qy1 o) {
        this.a.remove(o);
        o.mParent = null;
        return this;
    }
    
    public uy1 J(final long n) {
        super.setDuration(n);
        if (super.mDuration >= 0L) {
            final ArrayList a = this.a;
            if (a != null) {
                for (int size = a.size(), i = 0; i < size; ++i) {
                    ((qy1)this.a.get(i)).setDuration(n);
                }
            }
        }
        return this;
    }
    
    public uy1 K(final TimeInterpolator timeInterpolator) {
        this.e |= 0x1;
        final ArrayList a = this.a;
        if (a != null) {
            for (int size = a.size(), i = 0; i < size; ++i) {
                ((qy1)this.a.get(i)).setInterpolator(timeInterpolator);
            }
        }
        return (uy1)super.setInterpolator(timeInterpolator);
    }
    
    public uy1 L(final int i) {
        if (i != 0) {
            if (i != 1) {
                final StringBuilder sb = new StringBuilder();
                sb.append("Invalid parameter for TransitionSet ordering: ");
                sb.append(i);
                throw new AndroidRuntimeException(sb.toString());
            }
            this.b = false;
        }
        else {
            this.b = true;
        }
        return this;
    }
    
    public uy1 M(final ViewGroup viewGroup) {
        super.setSceneRoot(viewGroup);
        for (int size = this.a.size(), i = 0; i < size; ++i) {
            ((qy1)this.a.get(i)).setSceneRoot(viewGroup);
        }
        return this;
    }
    
    public uy1 N(final long startDelay) {
        return (uy1)super.setStartDelay(startDelay);
    }
    
    public final void O() {
        final b b = new b(this);
        final Iterator iterator = this.a.iterator();
        while (iterator.hasNext()) {
            ((qy1)iterator.next()).addListener((g)b);
        }
        this.c = this.a.size();
    }
    
    @Override
    public void cancel() {
        super.cancel();
        for (int size = this.a.size(), i = 0; i < size; ++i) {
            ((qy1)this.a.get(i)).cancel();
        }
    }
    
    @Override
    public void captureEndValues(final xy1 xy1) {
        if (this.isValidTarget(xy1.b)) {
            for (final qy1 e : this.a) {
                if (e.isValidTarget(xy1.b)) {
                    e.captureEndValues(xy1);
                    xy1.c.add(e);
                }
            }
        }
    }
    
    @Override
    public void capturePropagationValues(final xy1 xy1) {
        super.capturePropagationValues(xy1);
        for (int size = this.a.size(), i = 0; i < size; ++i) {
            ((qy1)this.a.get(i)).capturePropagationValues(xy1);
        }
    }
    
    @Override
    public void captureStartValues(final xy1 xy1) {
        if (this.isValidTarget(xy1.b)) {
            for (final qy1 e : this.a) {
                if (e.isValidTarget(xy1.b)) {
                    e.captureStartValues(xy1);
                    xy1.c.add(e);
                }
            }
        }
    }
    
    @Override
    public qy1 clone() {
        final uy1 uy1 = (uy1)super.clone();
        uy1.a = new ArrayList();
        for (int size = this.a.size(), i = 0; i < size; ++i) {
            uy1.A(((qy1)this.a.get(i)).clone());
        }
        return uy1;
    }
    
    @Override
    public void createAnimators(final ViewGroup viewGroup, final yy1 yy1, final yy1 yy2, final ArrayList list, final ArrayList list2) {
        final long startDelay = this.getStartDelay();
        for (int size = this.a.size(), i = 0; i < size; ++i) {
            final qy1 qy1 = this.a.get(i);
            if (startDelay > 0L && (this.b || i == 0)) {
                final long startDelay2 = qy1.getStartDelay();
                if (startDelay2 > 0L) {
                    qy1.setStartDelay(startDelay2 + startDelay);
                }
                else {
                    qy1.setStartDelay(startDelay);
                }
            }
            qy1.createAnimators(viewGroup, yy1, yy2, list, list2);
        }
    }
    
    @Override
    public qy1 excludeTarget(final int n, final boolean b) {
        for (int i = 0; i < this.a.size(); ++i) {
            ((qy1)this.a.get(i)).excludeTarget(n, b);
        }
        return super.excludeTarget(n, b);
    }
    
    @Override
    public qy1 excludeTarget(final View view, final boolean b) {
        for (int i = 0; i < this.a.size(); ++i) {
            ((qy1)this.a.get(i)).excludeTarget(view, b);
        }
        return super.excludeTarget(view, b);
    }
    
    @Override
    public qy1 excludeTarget(final Class clazz, final boolean b) {
        for (int i = 0; i < this.a.size(); ++i) {
            ((qy1)this.a.get(i)).excludeTarget(clazz, b);
        }
        return super.excludeTarget(clazz, b);
    }
    
    @Override
    public qy1 excludeTarget(final String s, final boolean b) {
        for (int i = 0; i < this.a.size(); ++i) {
            ((qy1)this.a.get(i)).excludeTarget(s, b);
        }
        return super.excludeTarget(s, b);
    }
    
    @Override
    public void forceToEnd(final ViewGroup viewGroup) {
        super.forceToEnd(viewGroup);
        for (int size = this.a.size(), i = 0; i < size; ++i) {
            ((qy1)this.a.get(i)).forceToEnd(viewGroup);
        }
    }
    
    @Override
    public void pause(final View view) {
        super.pause(view);
        for (int size = this.a.size(), i = 0; i < size; ++i) {
            ((qy1)this.a.get(i)).pause(view);
        }
    }
    
    @Override
    public void resume(final View view) {
        super.resume(view);
        for (int size = this.a.size(), i = 0; i < size; ++i) {
            ((qy1)this.a.get(i)).resume(view);
        }
    }
    
    @Override
    public void runAnimators() {
        if (this.a.isEmpty()) {
            this.start();
            this.end();
            return;
        }
        this.O();
        if (!this.b) {
            for (int i = 1; i < this.a.size(); ++i) {
                ((qy1)this.a.get(i - 1)).addListener((g)new ry1(this, (qy1)this.a.get(i)) {
                    public final qy1 a;
                    public final uy1 b;
                    
                    @Override
                    public void onTransitionEnd(final qy1 qy1) {
                        this.a.runAnimators();
                        qy1.removeListener((g)this);
                    }
                });
            }
            final qy1 qy1 = this.a.get(0);
            if (qy1 != null) {
                qy1.runAnimators();
            }
        }
        else {
            final Iterator iterator = this.a.iterator();
            while (iterator.hasNext()) {
                ((qy1)iterator.next()).runAnimators();
            }
        }
    }
    
    @Override
    public void setCanRemoveViews(final boolean b) {
        super.setCanRemoveViews(b);
        for (int size = this.a.size(), i = 0; i < size; ++i) {
            ((qy1)this.a.get(i)).setCanRemoveViews(b);
        }
    }
    
    @Override
    public void setEpicenterCallback(final f f) {
        super.setEpicenterCallback(f);
        this.e |= 0x8;
        for (int size = this.a.size(), i = 0; i < size; ++i) {
            ((qy1)this.a.get(i)).setEpicenterCallback(f);
        }
    }
    
    @Override
    public void setPathMotion(final g31 g31) {
        super.setPathMotion(g31);
        this.e |= 0x4;
        if (this.a != null) {
            for (int i = 0; i < this.a.size(); ++i) {
                ((qy1)this.a.get(i)).setPathMotion(g31);
            }
        }
    }
    
    @Override
    public void setPropagation(final ty1 ty1) {
        super.setPropagation(ty1);
        this.e |= 0x2;
        for (int size = this.a.size(), i = 0; i < size; ++i) {
            ((qy1)this.a.get(i)).setPropagation(ty1);
        }
    }
    
    @Override
    public String toString(final String str) {
        String str2 = super.toString(str);
        for (int i = 0; i < this.a.size(); ++i) {
            final StringBuilder sb = new StringBuilder();
            sb.append(str2);
            sb.append("\n");
            final qy1 qy1 = this.a.get(i);
            final StringBuilder sb2 = new StringBuilder();
            sb2.append(str);
            sb2.append("  ");
            sb.append(qy1.toString(sb2.toString()));
            str2 = sb.toString();
        }
        return str2;
    }
    
    public uy1 u(final g g) {
        return (uy1)super.addListener(g);
    }
    
    public uy1 v(final int n) {
        for (int i = 0; i < this.a.size(); ++i) {
            ((qy1)this.a.get(i)).addTarget(n);
        }
        return (uy1)super.addTarget(n);
    }
    
    public uy1 w(final View view) {
        for (int i = 0; i < this.a.size(); ++i) {
            ((qy1)this.a.get(i)).addTarget(view);
        }
        return (uy1)super.addTarget(view);
    }
    
    public uy1 x(final Class clazz) {
        for (int i = 0; i < this.a.size(); ++i) {
            ((qy1)this.a.get(i)).addTarget(clazz);
        }
        return (uy1)super.addTarget(clazz);
    }
    
    public uy1 y(final String s) {
        for (int i = 0; i < this.a.size(); ++i) {
            ((qy1)this.a.get(i)).addTarget(s);
        }
        return (uy1)super.addTarget(s);
    }
    
    public uy1 z(final qy1 qy1) {
        this.A(qy1);
        final long mDuration = super.mDuration;
        if (mDuration >= 0L) {
            qy1.setDuration(mDuration);
        }
        if ((this.e & 0x1) != 0x0) {
            qy1.setInterpolator(this.getInterpolator());
        }
        if ((this.e & 0x2) != 0x0) {
            this.getPropagation();
            qy1.setPropagation(null);
        }
        if ((this.e & 0x4) != 0x0) {
            qy1.setPathMotion(this.getPathMotion());
        }
        if ((this.e & 0x8) != 0x0) {
            qy1.setEpicenterCallback(this.getEpicenterCallback());
        }
        return this;
    }
    
    public static class b extends ry1
    {
        public uy1 a;
        
        public b(final uy1 a) {
            this.a = a;
        }
        
        @Override
        public void onTransitionEnd(final qy1 qy1) {
            final uy1 a = this.a;
            final int c = a.c - 1;
            a.c = c;
            if (c == 0) {
                a.d = false;
                a.end();
            }
            qy1.removeListener((g)this);
        }
        
        @Override
        public void onTransitionStart(final qy1 qy1) {
            final uy1 a = this.a;
            if (!a.d) {
                a.start();
                this.a.d = true;
            }
        }
    }
}

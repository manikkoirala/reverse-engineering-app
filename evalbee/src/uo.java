import android.os.IBinder;
import android.content.ComponentName;
import android.content.Context;
import android.content.ServiceConnection;

// 
// Decompiled by Procyon v0.6.0
// 

public abstract class uo implements ServiceConnection
{
    private Context mApplicationContext;
    
    public Context getApplicationContext() {
        return this.mApplicationContext;
    }
    
    public abstract void onCustomTabsServiceConnected(final ComponentName p0, final so p1);
    
    public final void onServiceConnected(final ComponentName componentName, final IBinder binder) {
        if (this.mApplicationContext != null) {
            this.onCustomTabsServiceConnected(componentName, new so(this, td0.a.p(binder), componentName, this.mApplicationContext) {
                public final uo d;
            });
            return;
        }
        throw new IllegalStateException("Custom Tabs Service connected before an applicationcontext has been provided.");
    }
    
    public void setApplicationContext(final Context mApplicationContext) {
        this.mApplicationContext = mApplicationContext;
    }
}

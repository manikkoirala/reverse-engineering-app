import com.google.android.gms.internal.firebase_auth_api.zzxw;
import android.util.Log;
import android.text.TextUtils;
import java.util.Iterator;
import java.io.UnsupportedEncodingException;
import com.google.android.gms.common.util.Base64Utils;
import java.util.HashMap;
import com.google.android.gms.internal.firebase_auth_api.zzab;
import com.google.android.gms.common.internal.Preconditions;
import java.util.Map;
import org.json.JSONObject;
import java.util.ArrayList;
import java.util.List;
import org.json.JSONArray;
import com.google.android.gms.common.logging.Logger;

// 
// Decompiled by Procyon v0.6.0
// 

public abstract class yc2
{
    public static final Logger a;
    
    static {
        a = new Logger("JSONParser", new String[0]);
    }
    
    public static List a(final JSONArray jsonArray) {
        final ArrayList list = new ArrayList();
        for (int i = 0; i < jsonArray.length(); ++i) {
            final Object value = jsonArray.get(i);
            Object o;
            if (value instanceof JSONArray) {
                o = a((JSONArray)value);
            }
            else {
                o = value;
                if (value instanceof JSONObject) {
                    o = c((JSONObject)value);
                }
            }
            list.add(o);
        }
        return list;
    }
    
    public static Map b(final String str) {
        Preconditions.checkNotEmpty(str);
        final List zza = ((zzab)zzab.zza('.')).zza((CharSequence)str);
        if (zza.size() < 2) {
            final Logger a = yc2.a;
            final StringBuilder sb = new StringBuilder("Invalid idToken ");
            sb.append(str);
            a.e(sb.toString(), new Object[0]);
            return new HashMap();
        }
        final String s = zza.get(1);
        try {
            Map d;
            if ((d = d(new String(Base64Utils.decodeUrlSafeNoPadding(s), "UTF-8"))) == null) {
                d = new HashMap();
            }
            return d;
        }
        catch (final UnsupportedEncodingException ex) {
            yc2.a.e("Unable to decode token", ex, new Object[0]);
            return new HashMap();
        }
    }
    
    public static Map c(final JSONObject jsonObject) {
        final r8 r8 = new r8();
        final Iterator keys = jsonObject.keys();
        while (keys.hasNext()) {
            final String s = keys.next();
            final Object value = jsonObject.get(s);
            Object o;
            if (value instanceof JSONArray) {
                o = a((JSONArray)value);
            }
            else {
                o = value;
                if (value instanceof JSONObject) {
                    o = c((JSONObject)value);
                }
            }
            r8.put(s, (List)o);
        }
        return r8;
    }
    
    public static Map d(final String s) {
        if (TextUtils.isEmpty((CharSequence)s)) {
            return null;
        }
        try {
            final JSONObject jsonObject = new JSONObject(s);
            if (jsonObject != JSONObject.NULL) {
                return c(jsonObject);
            }
            return null;
        }
        catch (final Exception ex) {
            Log.d("JSONParser", "Failed to parse JSONObject into Map.");
            throw new zzxw((Throwable)ex);
        }
    }
}

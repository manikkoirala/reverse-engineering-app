// 
// Decompiled by Procyon v0.6.0
// 

public final class zy0
{
    public final boolean a;
    public final boolean b;
    public final boolean c;
    public final boolean d;
    
    public zy0(final boolean a, final boolean b, final boolean c, final boolean d) {
        this.a = a;
        this.b = b;
        this.c = c;
        this.d = d;
    }
    
    public final boolean a() {
        return this.a;
    }
    
    public final boolean b() {
        return this.c;
    }
    
    public final boolean c() {
        return this.d;
    }
    
    public final boolean d() {
        return this.b;
    }
    
    @Override
    public boolean equals(final Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof zy0)) {
            return false;
        }
        final zy0 zy0 = (zy0)o;
        return this.a == zy0.a && this.b == zy0.b && this.c == zy0.c && this.d == zy0.d;
    }
    
    @Override
    public int hashCode() {
        final int a = this.a ? 1 : 0;
        int n = 1;
        int n2 = a;
        if (a != 0) {
            n2 = 1;
        }
        int b;
        if ((b = (this.b ? 1 : 0)) != 0) {
            b = 1;
        }
        int c;
        if ((c = (this.c ? 1 : 0)) != 0) {
            c = 1;
        }
        final int d = this.d ? 1 : 0;
        if (d == 0) {
            n = d;
        }
        return ((n2 * 31 + b) * 31 + c) * 31 + n;
    }
    
    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append("NetworkState(isConnected=");
        sb.append(this.a);
        sb.append(", isValidated=");
        sb.append(this.b);
        sb.append(", isMetered=");
        sb.append(this.c);
        sb.append(", isNotRoaming=");
        sb.append(this.d);
        sb.append(')');
        return sb.toString();
    }
}

import com.google.protobuf.GeneratedMessageLite;
import com.google.protobuf.ByteString;
import java.util.TreeMap;
import com.google.protobuf.j0;
import java.util.List;
import java.util.Collections;
import java.util.Collection;
import java.util.ArrayList;
import com.google.firestore.v1.a;
import java.util.Iterator;
import java.util.Map;
import com.google.firestore.v1.k;
import com.google.protobuf.NullValue;
import com.google.firestore.v1.Value;

// 
// Decompiled by Procyon v0.6.0
// 

public abstract class a32
{
    public static final Value a;
    public static final Value b;
    public static final Value c;
    public static final Value d;
    public static final Value e;
    
    static {
        a = (Value)((GeneratedMessageLite.a)Value.x0().E(Double.NaN)).p();
        c = (b = (Value)((GeneratedMessageLite.a)Value.x0().J(NullValue.NULL_VALUE)).p());
        e = (Value)((GeneratedMessageLite.a)Value.x0().H(k.j0().C("__type__", d = (Value)((GeneratedMessageLite.a)Value.x0().L("__max__")).p()))).p();
    }
    
    public static boolean A(final Value value) {
        return v(value) || u(value);
    }
    
    public static boolean B(final Value value) {
        return value != null && value.w0() == Value.ValueTypeCase.REFERENCE_VALUE;
    }
    
    public static int C(final Value value, final boolean b, final Value value2, final boolean b2) {
        final int i = i(value, value2);
        if (i != 0) {
            return i;
        }
        if (b && !b2) {
            return -1;
        }
        if (!b && b2) {
            return 1;
        }
        return 0;
    }
    
    public static boolean D(final Value value, final Value value2) {
        final Value.ValueTypeCase w0 = value.w0();
        final Value.ValueTypeCase integer_VALUE = Value.ValueTypeCase.INTEGER_VALUE;
        boolean b = true;
        final boolean b2 = true;
        if (w0 == integer_VALUE && value2.w0() == integer_VALUE) {
            return value.r0() == value2.r0() && b2;
        }
        final Value.ValueTypeCase w2 = value.w0();
        final Value.ValueTypeCase double_VALUE = Value.ValueTypeCase.DOUBLE_VALUE;
        if (w2 == double_VALUE && value2.w0() == double_VALUE) {
            if (Double.doubleToLongBits(value.p0()) != Double.doubleToLongBits(value2.p0())) {
                b = false;
            }
            return b;
        }
        return false;
    }
    
    public static boolean E(final Value value, final Value value2) {
        final k s0 = value.s0();
        final k s2 = value2.s0();
        if (s0.c0() != s2.c0()) {
            return false;
        }
        for (final Map.Entry<Object, V> entry : s0.d0().entrySet()) {
            if (!q((Value)entry.getValue(), (Value)s2.d0().get(entry.getKey()))) {
                return false;
            }
        }
        return true;
    }
    
    public static Value F(final qp qp, final du du) {
        return (Value)((GeneratedMessageLite.a)Value.x0().K(String.format("projects/%s/databases/%s/documents/%s", qp.f(), qp.e(), du.toString()))).p();
    }
    
    public static int G(final Value value) {
        switch (a32$a.a[value.w0().ordinal()]) {
            default: {
                final StringBuilder sb = new StringBuilder();
                sb.append("Invalid value type: ");
                sb.append(value.w0());
                throw g9.a(sb.toString(), new Object[0]);
            }
            case 11: {
                if (ql1.c(value)) {
                    return 4;
                }
                if (x(value)) {
                    return Integer.MAX_VALUE;
                }
                return 10;
            }
            case 10: {
                return 9;
            }
            case 9: {
                return 8;
            }
            case 8: {
                return 7;
            }
            case 7: {
                return 6;
            }
            case 6: {
                return 5;
            }
            case 5: {
                return 3;
            }
            case 3:
            case 4: {
                return 2;
            }
            case 2: {
                return 1;
            }
            case 1: {
                return 0;
            }
        }
    }
    
    public static int H(final Value value, final boolean b, final Value value2, final boolean b2) {
        final int i = i(value, value2);
        if (i != 0) {
            return i;
        }
        if (b && !b2) {
            return 1;
        }
        if (!b && b2) {
            return -1;
        }
        return 0;
    }
    
    public static boolean a(final Value value, final Value value2) {
        final a l0 = value.l0();
        final a l2 = value2.l0();
        if (l0.i0() != l2.i0()) {
            return false;
        }
        for (int i = 0; i < l0.i0(); ++i) {
            if (!q(l0.h0(i), l2.h0(i))) {
                return false;
            }
        }
        return true;
    }
    
    public static String b(final Value value) {
        final StringBuilder sb = new StringBuilder();
        h(sb, value);
        return sb.toString();
    }
    
    public static void c(final StringBuilder sb, final a a) {
        sb.append("[");
        for (int i = 0; i < a.i0(); ++i) {
            h(sb, a.h0(i));
            if (i != a.i0() - 1) {
                sb.append(",");
            }
        }
        sb.append("]");
    }
    
    public static void d(final StringBuilder sb, final ui0 ui0) {
        sb.append(String.format("geo(%s,%s)", ui0.d0(), ui0.e0()));
    }
    
    public static void e(final StringBuilder sb, final k k) {
        final ArrayList list = new ArrayList(k.d0().keySet());
        Collections.sort((List<Comparable>)list);
        sb.append("{");
        final Iterator iterator = list.iterator();
        int n = 1;
        while (iterator.hasNext()) {
            final String str = (String)iterator.next();
            if (n == 0) {
                sb.append(",");
            }
            else {
                n = 0;
            }
            sb.append(str);
            sb.append(":");
            h(sb, k.f0(str));
        }
        sb.append("}");
    }
    
    public static void f(final StringBuilder sb, final Value value) {
        g9.d(B(value), "Value should be a ReferenceValue", new Object[0]);
        sb.append(du.f(value.t0()));
    }
    
    public static void g(final StringBuilder sb, final j0 j0) {
        sb.append(String.format("time(%s,%s)", j0.e0(), j0.d0()));
    }
    
    public static void h(StringBuilder sb, final Value value) {
        String str = null;
        switch (a32$a.a[value.w0().ordinal()]) {
            default: {
                sb = new StringBuilder();
                sb.append("Invalid value type: ");
                sb.append(value.w0());
                throw g9.a(sb.toString(), new Object[0]);
            }
            case 11: {
                e(sb, value.s0());
                return;
            }
            case 10: {
                c(sb, value.l0());
                return;
            }
            case 9: {
                d(sb, value.q0());
                return;
            }
            case 8: {
                f(sb, value);
                return;
            }
            case 7: {
                str = o22.y(value.n0());
                break;
            }
            case 6: {
                str = value.u0();
                break;
            }
            case 5: {
                g(sb, value.v0());
                return;
            }
            case 4: {
                sb.append(value.p0());
                return;
            }
            case 3: {
                sb.append(value.r0());
                return;
            }
            case 2: {
                sb.append(value.m0());
                return;
            }
            case 1: {
                str = "null";
                break;
            }
        }
        sb.append(str);
    }
    
    public static int i(final Value value, final Value value2) {
        final int g = G(value);
        final int g2 = G(value2);
        if (g != g2) {
            return o22.k(g, g2);
        }
        if (g != Integer.MAX_VALUE) {
            switch (g) {
                default: {
                    final StringBuilder sb = new StringBuilder();
                    sb.append("Invalid value type: ");
                    sb.append(g);
                    throw g9.a(sb.toString(), new Object[0]);
                }
                case 10: {
                    return l(value.s0(), value2.s0());
                }
                case 9: {
                    return j(value.l0(), value2.l0());
                }
                case 8: {
                    return k(value.q0(), value2.q0());
                }
                case 7: {
                    return n(value.t0(), value2.t0());
                }
                case 6: {
                    return o22.i(value.n0(), value2.n0());
                }
                case 5: {
                    return value.u0().compareTo(value2.u0());
                }
                case 4: {
                    return o(ql1.a(value), ql1.a(value2));
                }
                case 3: {
                    return o(value.v0(), value2.v0());
                }
                case 2: {
                    return m(value, value2);
                }
                case 1: {
                    return o22.g(value.m0(), value2.m0());
                }
                case 0: {
                    break;
                }
            }
        }
        return 0;
    }
    
    public static int j(final a a, final a a2) {
        for (int min = Math.min(a.i0(), a2.i0()), i = 0; i < min; ++i) {
            final int j = i(a.h0(i), a2.h0(i));
            if (j != 0) {
                return j;
            }
        }
        return o22.k(a.i0(), a2.i0());
    }
    
    public static int k(final ui0 ui0, final ui0 ui2) {
        final int j = o22.j(ui0.d0(), ui2.d0());
        if (j == 0) {
            return o22.j(ui0.e0(), ui2.e0());
        }
        return j;
    }
    
    public static int l(final k k, final k i) {
        final Iterator iterator = new TreeMap(k.d0()).entrySet().iterator();
        final Iterator iterator2 = new TreeMap(i.d0()).entrySet().iterator();
        while (iterator.hasNext() && iterator2.hasNext()) {
            final Map.Entry<String, V> entry = (Map.Entry<String, V>)iterator.next();
            final Map.Entry<K, Value> entry2 = (Map.Entry<K, Value>)iterator2.next();
            final int compareTo = entry.getKey().compareTo((String)entry2.getKey());
            if (compareTo != 0) {
                return compareTo;
            }
            final int j = i((Value)entry.getValue(), entry2.getValue());
            if (j != 0) {
                return j;
            }
        }
        return o22.g(iterator.hasNext(), iterator2.hasNext());
    }
    
    public static int m(final Value value, final Value value2) {
        final Value.ValueTypeCase w0 = value.w0();
        final Value.ValueTypeCase double_VALUE = Value.ValueTypeCase.DOUBLE_VALUE;
        if (w0 == double_VALUE) {
            final double p2 = value.p0();
            if (value2.w0() == double_VALUE) {
                return o22.j(p2, value2.p0());
            }
            if (value2.w0() == Value.ValueTypeCase.INTEGER_VALUE) {
                return o22.m(p2, value2.r0());
            }
        }
        else {
            final Value.ValueTypeCase w2 = value.w0();
            final Value.ValueTypeCase integer_VALUE = Value.ValueTypeCase.INTEGER_VALUE;
            if (w2 == integer_VALUE) {
                final long r0 = value.r0();
                if (value2.w0() == integer_VALUE) {
                    return o22.l(r0, value2.r0());
                }
                if (value2.w0() == double_VALUE) {
                    return o22.m(value2.p0(), r0) * -1;
                }
            }
        }
        throw g9.a("Unexpected values: %s vs %s", value, value2);
    }
    
    public static int n(final String s, final String s2) {
        final String[] split = s.split("/", -1);
        final String[] split2 = s2.split("/", -1);
        for (int min = Math.min(split.length, split2.length), i = 0; i < min; ++i) {
            final int compareTo = split[i].compareTo(split2[i]);
            if (compareTo != 0) {
                return compareTo;
            }
        }
        return o22.k(split.length, split2.length);
    }
    
    public static int o(final j0 j0, final j0 j2) {
        final int l = o22.l(j0.e0(), j2.e0());
        if (l != 0) {
            return l;
        }
        return o22.k(j0.d0(), j2.d0());
    }
    
    public static boolean p(final w8 w8, final Value value) {
        final Iterator iterator = w8.h().iterator();
        while (iterator.hasNext()) {
            if (q((Value)iterator.next(), value)) {
                return true;
            }
        }
        return false;
    }
    
    public static boolean q(final Value value, final Value value2) {
        if (value == value2) {
            return true;
        }
        if (value == null || value2 == null) {
            return false;
        }
        final int g = G(value);
        if (g != G(value2)) {
            return false;
        }
        if (g == 2) {
            return D(value, value2);
        }
        if (g == 4) {
            return ql1.a(value).equals(ql1.a(value2));
        }
        if (g == Integer.MAX_VALUE) {
            return true;
        }
        if (g == 9) {
            return a(value, value2);
        }
        if (g != 10) {
            return value.equals(value2);
        }
        return E(value, value2);
    }
    
    public static Value r(final Value.ValueTypeCase obj) {
        switch (a32$a.a[obj.ordinal()]) {
            default: {
                final StringBuilder sb = new StringBuilder();
                sb.append("Unknown value type: ");
                sb.append(obj);
                throw new IllegalArgumentException(sb.toString());
            }
            case 11: {
                return (Value)((GeneratedMessageLite.a)Value.x0().I(k.b0())).p();
            }
            case 10: {
                return (Value)((GeneratedMessageLite.a)Value.x0().B(com.google.firestore.v1.a.g0())).p();
            }
            case 9: {
                return (Value)((GeneratedMessageLite.a)Value.x0().F(ui0.f0().A(-90.0).B(-180.0))).p();
            }
            case 8: {
                return F(qp.c, du.d());
            }
            case 7: {
                return (Value)((GeneratedMessageLite.a)Value.x0().D(ByteString.EMPTY)).p();
            }
            case 6: {
                return (Value)((GeneratedMessageLite.a)Value.x0().L("")).p();
            }
            case 5: {
                return (Value)((GeneratedMessageLite.a)Value.x0().M(j0.f0().B(Long.MIN_VALUE))).p();
            }
            case 3:
            case 4: {
                return (Value)((GeneratedMessageLite.a)Value.x0().E(Double.NaN)).p();
            }
            case 2: {
                return (Value)((GeneratedMessageLite.a)Value.x0().C(false)).p();
            }
            case 1: {
                return a32.b;
            }
        }
    }
    
    public static Value s(Value.ValueTypeCase obj) {
        switch (a32$a.a[obj.ordinal()]) {
            default: {
                final StringBuilder sb = new StringBuilder();
                sb.append("Unknown value type: ");
                sb.append(obj);
                throw new IllegalArgumentException(sb.toString());
            }
            case 11: {
                return a32.e;
            }
            case 10: {
                obj = Value.ValueTypeCase.MAP_VALUE;
                break;
            }
            case 9: {
                obj = Value.ValueTypeCase.ARRAY_VALUE;
                break;
            }
            case 8: {
                obj = Value.ValueTypeCase.GEO_POINT_VALUE;
                break;
            }
            case 7: {
                obj = Value.ValueTypeCase.REFERENCE_VALUE;
                break;
            }
            case 6: {
                obj = Value.ValueTypeCase.BYTES_VALUE;
                break;
            }
            case 5: {
                obj = Value.ValueTypeCase.STRING_VALUE;
                break;
            }
            case 3:
            case 4: {
                obj = Value.ValueTypeCase.TIMESTAMP_VALUE;
                break;
            }
            case 2: {
                obj = Value.ValueTypeCase.INTEGER_VALUE;
                break;
            }
            case 1: {
                obj = Value.ValueTypeCase.BOOLEAN_VALUE;
                break;
            }
        }
        return r(obj);
    }
    
    public static boolean t(final Value value) {
        return value != null && value.w0() == Value.ValueTypeCase.ARRAY_VALUE;
    }
    
    public static boolean u(final Value value) {
        return value != null && value.w0() == Value.ValueTypeCase.DOUBLE_VALUE;
    }
    
    public static boolean v(final Value value) {
        return value != null && value.w0() == Value.ValueTypeCase.INTEGER_VALUE;
    }
    
    public static boolean w(final Value value) {
        return value != null && value.w0() == Value.ValueTypeCase.MAP_VALUE;
    }
    
    public static boolean x(final Value value) {
        return a32.d.equals(value.s0().d0().get("__type__"));
    }
    
    public static boolean y(final Value value) {
        return value != null && Double.isNaN(value.p0());
    }
    
    public static boolean z(final Value value) {
        return value != null && value.w0() == Value.ValueTypeCase.NULL_VALUE;
    }
}

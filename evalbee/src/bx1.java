import androidx.appcompat.view.menu.e;
import androidx.appcompat.view.menu.i;
import android.view.Menu;
import android.view.KeyCharacterMap;
import android.view.KeyEvent;
import android.content.res.Configuration;
import android.view.View;
import android.content.Context;
import androidx.appcompat.widget.d;
import android.view.MenuItem;
import androidx.appcompat.widget.Toolbar;
import java.util.ArrayList;
import android.view.Window$Callback;

// 
// Decompiled by Procyon v0.6.0
// 

public class bx1 extends t1
{
    public final dq a;
    public final Window$Callback b;
    public final g6.f c;
    public boolean d;
    public boolean e;
    public boolean f;
    public ArrayList g;
    public final Runnable h;
    public final Toolbar.h i;
    
    public bx1(final Toolbar toolbar, final CharSequence windowTitle, final Window$Callback windowCallback) {
        this.g = new ArrayList();
        this.h = new Runnable(this) {
            public final bx1 a;
            
            @Override
            public void run() {
                this.a.x();
            }
        };
        final Toolbar.h h = new Toolbar.h(this) {
            public final bx1 a;
            
            @Override
            public boolean onMenuItemClick(final MenuItem menuItem) {
                return this.a.b.onMenuItemSelected(0, menuItem);
            }
        };
        this.i = h;
        l71.g(toolbar);
        final androidx.appcompat.widget.d a = new androidx.appcompat.widget.d(toolbar, false);
        this.a = a;
        this.b = (Window$Callback)l71.g(windowCallback);
        a.setWindowCallback(windowCallback);
        toolbar.setOnMenuItemClickListener((Toolbar.h)h);
        a.setWindowTitle(windowTitle);
        this.c = new e();
    }
    
    @Override
    public boolean f() {
        return this.a.d();
    }
    
    @Override
    public boolean g() {
        if (this.a.h()) {
            this.a.collapseActionView();
            return true;
        }
        return false;
    }
    
    @Override
    public void h(final boolean f) {
        if (f == this.f) {
            return;
        }
        this.f = f;
        if (this.g.size() <= 0) {
            return;
        }
        zu0.a(this.g.get(0));
        throw null;
    }
    
    @Override
    public int i() {
        return this.a.o();
    }
    
    @Override
    public Context j() {
        return this.a.getContext();
    }
    
    @Override
    public boolean k() {
        ((View)this.a.t()).removeCallbacks(this.h);
        o32.i0((View)this.a.t(), this.h);
        return true;
    }
    
    @Override
    public void l(final Configuration configuration) {
        super.l(configuration);
    }
    
    @Override
    public void m() {
        ((View)this.a.t()).removeCallbacks(this.h);
    }
    
    @Override
    public boolean n(final int n, final KeyEvent keyEvent) {
        final Menu w = this.w();
        if (w != null) {
            int deviceId;
            if (keyEvent != null) {
                deviceId = keyEvent.getDeviceId();
            }
            else {
                deviceId = -1;
            }
            final int keyboardType = KeyCharacterMap.load(deviceId).getKeyboardType();
            boolean qwertyMode = true;
            if (keyboardType == 1) {
                qwertyMode = false;
            }
            w.setQwertyMode(qwertyMode);
            return w.performShortcut(n, keyEvent, 0);
        }
        return false;
    }
    
    @Override
    public boolean o(final KeyEvent keyEvent) {
        if (keyEvent.getAction() == 1) {
            this.p();
        }
        return true;
    }
    
    @Override
    public boolean p() {
        return this.a.b();
    }
    
    @Override
    public void q(final boolean b) {
    }
    
    @Override
    public void r(final boolean b) {
    }
    
    @Override
    public void s(final CharSequence charSequence) {
        this.a.q(charSequence);
    }
    
    @Override
    public void t(final CharSequence title) {
        this.a.setTitle(title);
    }
    
    @Override
    public void u(final CharSequence windowTitle) {
        this.a.setWindowTitle(windowTitle);
    }
    
    public final Menu w() {
        if (!this.e) {
            this.a.x(new c(), new d());
            this.e = true;
        }
        return this.a.r();
    }
    
    public void x() {
        final Menu w = this.w();
        androidx.appcompat.view.menu.e e;
        if (w instanceof androidx.appcompat.view.menu.e) {
            e = (androidx.appcompat.view.menu.e)w;
        }
        else {
            e = null;
        }
        if (e != null) {
            e.stopDispatchingItemsChanged();
        }
        try {
            w.clear();
            if (!this.b.onCreatePanelMenu(0, w) || !this.b.onPreparePanel(0, (View)null, w)) {
                w.clear();
            }
        }
        finally {
            if (e != null) {
                e.startDispatchingItemsChanged();
            }
        }
    }
    
    public final class c implements i.a
    {
        public boolean a;
        public final bx1 b;
        
        public c(final bx1 b) {
            this.b = b;
        }
        
        @Override
        public boolean a(final androidx.appcompat.view.menu.e e) {
            this.b.b.onMenuOpened(108, (Menu)e);
            return true;
        }
        
        @Override
        public void onCloseMenu(final androidx.appcompat.view.menu.e e, final boolean b) {
            if (this.a) {
                return;
            }
            this.a = true;
            this.b.a.m();
            this.b.b.onPanelClosed(108, (Menu)e);
            this.a = false;
        }
    }
    
    public final class d implements e.a
    {
        public final bx1 a;
        
        public d(final bx1 a) {
            this.a = a;
        }
        
        @Override
        public boolean onMenuItemSelected(final e e, final MenuItem menuItem) {
            return false;
        }
        
        @Override
        public void onMenuModeChange(final e e) {
            if (this.a.a.c()) {
                this.a.b.onPanelClosed(108, (Menu)e);
            }
            else if (this.a.b.onPreparePanel(0, (View)null, (Menu)e)) {
                this.a.b.onMenuOpened(108, (Menu)e);
            }
        }
    }
    
    public class e implements f
    {
        public final bx1 a;
        
        public e(final bx1 a) {
            this.a = a;
        }
        
        @Override
        public boolean a(final int n) {
            if (n == 0) {
                final bx1 a = this.a;
                if (!a.d) {
                    a.a.f();
                    this.a.d = true;
                }
            }
            return false;
        }
        
        @Override
        public View onCreatePanelView(final int n) {
            if (n == 0) {
                return new View(this.a.a.getContext());
            }
            return null;
        }
    }
}

import java.util.concurrent.BlockingQueue;
import java.util.Iterator;
import com.android.volley.Request;
import java.util.ArrayList;
import java.util.HashSet;
import android.os.Handler;
import android.os.Looper;
import java.util.List;
import com.android.volley.b;
import com.android.volley.c;
import com.android.volley.a;
import java.util.concurrent.PriorityBlockingQueue;
import java.util.Set;
import java.util.concurrent.atomic.AtomicInteger;

// 
// Decompiled by Procyon v0.6.0
// 

public class ee1
{
    public final AtomicInteger a;
    public final Set b;
    public final PriorityBlockingQueue c;
    public final PriorityBlockingQueue d;
    public final a e;
    public final qy0 f;
    public final re1 g;
    public final c[] h;
    public b i;
    public final List j;
    public final List k;
    
    public ee1(final a a, final qy0 qy0) {
        this(a, qy0, 4);
    }
    
    public ee1(final a a, final qy0 qy0, final int n) {
        this(a, qy0, n, new ty(new Handler(Looper.getMainLooper())));
    }
    
    public ee1(final a e, final qy0 f, final int n, final re1 g) {
        this.a = new AtomicInteger();
        this.b = new HashSet();
        this.c = new PriorityBlockingQueue();
        this.d = new PriorityBlockingQueue();
        this.j = new ArrayList();
        this.k = new ArrayList();
        this.e = e;
        this.f = f;
        this.h = new c[n];
        this.g = g;
    }
    
    public Request a(final Request request) {
        request.K(this);
        synchronized (this.b) {
            this.b.add(request);
            monitorexit(this.b);
            request.M(this.e());
            request.c("add-to-queue");
            this.f(request, 0);
            this.b(request);
            return request;
        }
    }
    
    public void b(final Request e) {
        if (!e.N()) {
            this.g(e);
        }
        else {
            this.c.add(e);
        }
    }
    
    public void c(final Request request) {
        synchronized (this.b) {
            this.b.remove(request);
            monitorexit(this.b);
            final List j = this.j;
            synchronized (this.b) {
                final Iterator iterator = this.j.iterator();
                if (!iterator.hasNext()) {
                    monitorexit(this.b);
                    this.f(request, 5);
                    return;
                }
                zu0.a(iterator.next());
                throw null;
            }
        }
    }
    
    public a d() {
        return this.e;
    }
    
    public int e() {
        return this.a.incrementAndGet();
    }
    
    public void f(final Request request, final int n) {
        synchronized (this.k) {
            final Iterator iterator = this.k.iterator();
            if (!iterator.hasNext()) {
                return;
            }
            zu0.a(iterator.next());
            throw null;
        }
    }
    
    public void g(final Request e) {
        this.d.add(e);
    }
    
    public void h() {
        this.i();
        (this.i = new b(this.c, this.d, this.e, this.g)).start();
        for (int i = 0; i < this.h.length; ++i) {
            (this.h[i] = new c(this.d, this.f, this.e, this.g)).start();
        }
    }
    
    public void i() {
        final b i = this.i;
        if (i != null) {
            i.d();
        }
        for (final c c : this.h) {
            if (c != null) {
                c.e();
            }
        }
    }
}

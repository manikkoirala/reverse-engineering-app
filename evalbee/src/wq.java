import com.android.volley.VolleyError;

// 
// Decompiled by Procyon v0.6.0
// 

public class wq implements jf1
{
    public int a;
    public int b;
    public final int c;
    public final float d;
    
    public wq() {
        this(2500, 1, 1.0f);
    }
    
    public wq(final int a, final int c, final float d) {
        this.a = a;
        this.c = c;
        this.d = d;
    }
    
    @Override
    public int a() {
        return this.b;
    }
    
    @Override
    public void b(final VolleyError volleyError) {
        ++this.b;
        final int a = this.a;
        this.a = a + (int)(a * this.d);
        if (this.d()) {
            return;
        }
        throw volleyError;
    }
    
    @Override
    public int c() {
        return this.a;
    }
    
    public boolean d() {
        return this.b <= this.c;
    }
}

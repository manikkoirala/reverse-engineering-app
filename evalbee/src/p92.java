import java.util.Iterator;
import java.util.ArrayList;
import java.util.List;
import androidx.work.OverwritingInputMerger;
import androidx.work.OutOfQuotaPolicy;
import androidx.work.BackoffPolicy;
import androidx.work.b;
import androidx.work.WorkInfo$State;

// 
// Decompiled by Procyon v0.6.0
// 

public final class p92
{
    public static final a x;
    public static final String y;
    public static final ba0 z;
    public final String a;
    public WorkInfo$State b;
    public String c;
    public String d;
    public androidx.work.b e;
    public androidx.work.b f;
    public long g;
    public long h;
    public long i;
    public zk j;
    public int k;
    public BackoffPolicy l;
    public long m;
    public long n;
    public long o;
    public long p;
    public boolean q;
    public OutOfQuotaPolicy r;
    public int s;
    public final int t;
    public long u;
    public int v;
    public final int w;
    
    static {
        x = new a(null);
        final String i = xl0.i("WorkSpec");
        fg0.d((Object)i, "tagWithPrefix(\"WorkSpec\")");
        y = i;
        z = new o92();
    }
    
    public p92(final String a, final WorkInfo$State b, final String c, final String d, final androidx.work.b e, final androidx.work.b f, final long g, final long h, final long i, final zk j, final int k, final BackoffPolicy l, final long m, final long n, final long o, final long p23, final boolean q, final OutOfQuotaPolicy r, final int s, final int t, final long u, final int v, final int w) {
        fg0.e((Object)a, "id");
        fg0.e((Object)b, "state");
        fg0.e((Object)c, "workerClassName");
        fg0.e((Object)d, "inputMergerClassName");
        fg0.e((Object)e, "input");
        fg0.e((Object)f, "output");
        fg0.e((Object)j, "constraints");
        fg0.e((Object)l, "backoffPolicy");
        fg0.e((Object)r, "outOfQuotaPolicy");
        this.a = a;
        this.b = b;
        this.c = c;
        this.d = d;
        this.e = e;
        this.f = f;
        this.g = g;
        this.h = h;
        this.i = i;
        this.j = j;
        this.k = k;
        this.l = l;
        this.m = m;
        this.n = n;
        this.o = o;
        this.p = p23;
        this.q = q;
        this.r = r;
        this.s = s;
        this.t = t;
        this.u = u;
        this.v = v;
        this.w = w;
    }
    
    public p92(final String s, final String s2) {
        fg0.e((Object)s, "id");
        fg0.e((Object)s2, "workerClassName_");
        this(s, null, s2, null, null, null, 0L, 0L, 0L, null, 0, null, 0L, 0L, 0L, 0L, false, null, 0, 0, 0L, 0, 0, 8388602, null);
    }
    
    public p92(final String s, final p92 p2) {
        fg0.e((Object)s, "newId");
        fg0.e((Object)p2, "other");
        this(s, p2.b, p2.c, p2.d, new androidx.work.b(p2.e), new androidx.work.b(p2.f), p2.g, p2.h, p2.i, new zk(p2.j), p2.k, p2.l, p2.m, p2.n, p2.o, p2.p, p2.q, p2.r, p2.s, 0, p2.u, p2.v, p2.w, 524288, null);
    }
    
    public static final List b(final List list) {
        List list2 = null;
        if (list != null) {
            final Iterable iterable = list;
            list2 = new ArrayList(oh.o(iterable, 10));
            final Iterator iterator = iterable.iterator();
            if (iterator.hasNext()) {
                zu0.a(iterator.next());
                throw null;
            }
        }
        return list2;
    }
    
    public final long c() {
        return p92.x.a(this.l(), this.k, this.l, this.m, this.n, this.s, this.m(), this.g, this.i, this.h, this.u);
    }
    
    public final p92 d(final String s, final WorkInfo$State workInfo$State, final String s2, final String s3, final androidx.work.b b, final androidx.work.b b2, final long n, final long n2, final long n3, final zk zk, final int n4, final BackoffPolicy backoffPolicy, final long n5, final long n6, final long n7, final long n8, final boolean b3, final OutOfQuotaPolicy outOfQuotaPolicy, final int n9, final int n10, final long n11, final int n12, final int n13) {
        fg0.e((Object)s, "id");
        fg0.e((Object)workInfo$State, "state");
        fg0.e((Object)s2, "workerClassName");
        fg0.e((Object)s3, "inputMergerClassName");
        fg0.e((Object)b, "input");
        fg0.e((Object)b2, "output");
        fg0.e((Object)zk, "constraints");
        fg0.e((Object)backoffPolicy, "backoffPolicy");
        fg0.e((Object)outOfQuotaPolicy, "outOfQuotaPolicy");
        return new p92(s, workInfo$State, s2, s3, b, b2, n, n2, n3, zk, n4, backoffPolicy, n5, n6, n7, n8, b3, outOfQuotaPolicy, n9, n10, n11, n12, n13);
    }
    
    @Override
    public boolean equals(final Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof p92)) {
            return false;
        }
        final p92 p = (p92)o;
        return fg0.a((Object)this.a, (Object)p.a) && this.b == p.b && fg0.a((Object)this.c, (Object)p.c) && fg0.a((Object)this.d, (Object)p.d) && fg0.a((Object)this.e, (Object)p.e) && fg0.a((Object)this.f, (Object)p.f) && this.g == p.g && this.h == p.h && this.i == p.i && fg0.a((Object)this.j, (Object)p.j) && this.k == p.k && this.l == p.l && this.m == p.m && this.n == p.n && this.o == p.o && this.p == p.p && this.q == p.q && this.r == p.r && this.s == p.s && this.t == p.t && this.u == p.u && this.v == p.v && this.w == p.w;
    }
    
    public final int f() {
        return this.t;
    }
    
    public final long g() {
        return this.u;
    }
    
    public final int h() {
        return this.v;
    }
    
    @Override
    public int hashCode() {
        final int hashCode = this.a.hashCode();
        final int hashCode2 = this.b.hashCode();
        final int hashCode3 = this.c.hashCode();
        final int hashCode4 = this.d.hashCode();
        final int hashCode5 = this.e.hashCode();
        final int hashCode6 = this.f.hashCode();
        final int hashCode7 = Long.hashCode(this.g);
        final int hashCode8 = Long.hashCode(this.h);
        final int hashCode9 = Long.hashCode(this.i);
        final int hashCode10 = this.j.hashCode();
        final int hashCode11 = Integer.hashCode(this.k);
        final int hashCode12 = this.l.hashCode();
        final int hashCode13 = Long.hashCode(this.m);
        final int hashCode14 = Long.hashCode(this.n);
        final int hashCode15 = Long.hashCode(this.o);
        final int hashCode16 = Long.hashCode(this.p);
        int q;
        if ((q = (this.q ? 1 : 0)) != 0) {
            q = 1;
        }
        return (((((((((((((((((((((hashCode * 31 + hashCode2) * 31 + hashCode3) * 31 + hashCode4) * 31 + hashCode5) * 31 + hashCode6) * 31 + hashCode7) * 31 + hashCode8) * 31 + hashCode9) * 31 + hashCode10) * 31 + hashCode11) * 31 + hashCode12) * 31 + hashCode13) * 31 + hashCode14) * 31 + hashCode15) * 31 + hashCode16) * 31 + q) * 31 + this.r.hashCode()) * 31 + Integer.hashCode(this.s)) * 31 + Integer.hashCode(this.t)) * 31 + Long.hashCode(this.u)) * 31 + Integer.hashCode(this.v)) * 31 + Integer.hashCode(this.w);
    }
    
    public final int i() {
        return this.s;
    }
    
    public final int j() {
        return this.w;
    }
    
    public final boolean k() {
        return fg0.a((Object)zk.j, (Object)this.j) ^ true;
    }
    
    public final boolean l() {
        return this.b == WorkInfo$State.ENQUEUED && this.k > 0;
    }
    
    public final boolean m() {
        return this.h != 0L;
    }
    
    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append("{WorkSpec: ");
        sb.append(this.a);
        sb.append('}');
        return sb.toString();
    }
    
    public static final class a
    {
        public final long a(final boolean b, int n, final BackoffPolicy backoffPolicy, long n2, long n3, int n4, final boolean b2, final long n5, final long n6, final long n7, long b3) {
            fg0.e((Object)backoffPolicy, "backoffPolicy");
            final long n8 = Long.MAX_VALUE;
            if (b3 != Long.MAX_VALUE && b2) {
                if (n4 != 0) {
                    b3 = ic1.b(b3, 900000L + n3);
                }
                return b3;
            }
            final int n9 = 0;
            final int n10 = 0;
            if (b) {
                n4 = n10;
                if (backoffPolicy == BackoffPolicy.LINEAR) {
                    n4 = 1;
                }
                if (n4 != 0) {
                    n2 *= n;
                }
                else {
                    n2 = (long)Math.scalb((float)n2, n - 1);
                }
                n2 = n3 + ic1.d(n2, 18000000L);
            }
            else if (b2) {
                if (n4 == 0) {
                    n3 += n5;
                }
                else {
                    n3 += n7;
                }
                n = n9;
                if (n6 != n7) {
                    n = 1;
                }
                n2 = n3;
                if (n != 0) {
                    n2 = n3;
                    if (n4 == 0) {
                        n2 = n3 + (n7 - n6);
                    }
                }
            }
            else if (n3 == -1L) {
                n2 = n8;
            }
            else {
                n2 = n3 + n5;
            }
            return n2;
        }
    }
    
    public static final class b
    {
        public String a;
        public WorkInfo$State b;
        
        public b(final String a, final WorkInfo$State b) {
            fg0.e((Object)a, "id");
            fg0.e((Object)b, "state");
            this.a = a;
            this.b = b;
        }
        
        @Override
        public boolean equals(final Object o) {
            if (this == o) {
                return true;
            }
            if (!(o instanceof b)) {
                return false;
            }
            final b b = (b)o;
            return fg0.a((Object)this.a, (Object)b.a) && this.b == b.b;
        }
        
        @Override
        public int hashCode() {
            return this.a.hashCode() * 31 + this.b.hashCode();
        }
        
        @Override
        public String toString() {
            final StringBuilder sb = new StringBuilder();
            sb.append("IdAndState(id=");
            sb.append(this.a);
            sb.append(", state=");
            sb.append(this.b);
            sb.append(')');
            return sb.toString();
        }
    }
}

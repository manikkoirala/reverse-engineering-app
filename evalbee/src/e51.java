import com.google.android.gms.common.internal.safeparcel.SafeParcelWriter;
import android.os.Parcel;
import com.google.android.gms.common.internal.Preconditions;
import android.text.TextUtils;
import android.os.Parcelable$Creator;

// 
// Decompiled by Procyon v0.6.0
// 

public class e51 extends v9 implements Cloneable
{
    public static final Parcelable$Creator<e51> CREATOR;
    public String a;
    public String b;
    public String c;
    public boolean d;
    public String e;
    
    static {
        CREATOR = (Parcelable$Creator)new tb2();
    }
    
    public e51(final String a, final String b, final String c, final boolean d, final String e) {
        Preconditions.checkArgument((!TextUtils.isEmpty((CharSequence)a) && !TextUtils.isEmpty((CharSequence)b)) || (!TextUtils.isEmpty((CharSequence)c) && !TextUtils.isEmpty((CharSequence)e)), (Object)"Cannot create PhoneAuthCredential without either sessionInfo + smsCode or temporary proof + phoneNumber.");
        this.a = a;
        this.b = b;
        this.c = c;
        this.d = d;
        this.e = e;
    }
    
    public static e51 K(final String s, final String s2) {
        return new e51(s, s2, null, true, null);
    }
    
    public static e51 R(final String s, final String s2) {
        return new e51(null, null, s, true, s2);
    }
    
    @Override
    public String E() {
        return "phone";
    }
    
    @Override
    public final v9 H() {
        return (e51)this.clone();
    }
    
    public String J() {
        return this.b;
    }
    
    public final e51 O(final boolean b) {
        this.d = false;
        return this;
    }
    
    public /* synthetic */ Object clone() {
        return new e51(this.a, this.J(), this.c, this.d, this.e);
    }
    
    @Override
    public String i() {
        return "phone";
    }
    
    public void writeToParcel(final Parcel parcel, int beginObjectHeader) {
        beginObjectHeader = SafeParcelWriter.beginObjectHeader(parcel);
        SafeParcelWriter.writeString(parcel, 1, this.a, false);
        SafeParcelWriter.writeString(parcel, 2, this.J(), false);
        SafeParcelWriter.writeString(parcel, 4, this.c, false);
        SafeParcelWriter.writeBoolean(parcel, 5, this.d);
        SafeParcelWriter.writeString(parcel, 6, this.e, false);
        SafeParcelWriter.finishObjectHeader(parcel, beginObjectHeader);
    }
    
    public final String zzb() {
        return this.c;
    }
    
    public final String zzc() {
        return this.a;
    }
    
    public final String zzd() {
        return this.e;
    }
    
    public final boolean zze() {
        return this.d;
    }
}

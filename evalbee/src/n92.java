import androidx.work.b;
import java.util.Set;
import java.util.UUID;

// 
// Decompiled by Procyon v0.6.0
// 

public abstract class n92
{
    public static final b d;
    public final UUID a;
    public final p92 b;
    public final Set c;
    
    static {
        d = new b(null);
    }
    
    public n92(final UUID a, final p92 b, final Set c) {
        fg0.e((Object)a, "id");
        fg0.e((Object)b, "workSpec");
        fg0.e((Object)c, "tags");
        this.a = a;
        this.b = b;
        this.c = c;
    }
    
    public UUID a() {
        return this.a;
    }
    
    public final String b() {
        final String string = this.a().toString();
        fg0.d((Object)string, "id.toString()");
        return string;
    }
    
    public final Set c() {
        return this.c;
    }
    
    public final p92 d() {
        return this.b;
    }
    
    public abstract static class a
    {
        public final Class a;
        public boolean b;
        public UUID c;
        public p92 d;
        public final Set e;
        
        public a(final Class a) {
            fg0.e((Object)a, "workerClass");
            this.a = a;
            final UUID randomUUID = UUID.randomUUID();
            fg0.d((Object)randomUUID, "randomUUID()");
            this.c = randomUUID;
            final String string = this.c.toString();
            fg0.d((Object)string, "id.toString()");
            final String name = a.getName();
            fg0.d((Object)name, "workerClass.name");
            this.d = new p92(string, name);
            final String name2 = a.getName();
            fg0.d((Object)name2, "workerClass.name");
            this.e = tm1.f((Object[])new String[] { name2 });
        }
        
        public final a a(final String s) {
            fg0.e((Object)s, "tag");
            this.e.add(s);
            return this.g();
        }
        
        public final n92 b() {
            final n92 c = this.c();
            final zk j = this.d.j;
            final boolean e = j.e();
            final int n = 0;
            final boolean b = e || j.f() || j.g() || j.h();
            final p92 d = this.d;
            if (d.q) {
                if (!(b ^ true)) {
                    throw new IllegalArgumentException("Expedited jobs only support network and storage constraints".toString());
                }
                int n2 = n;
                if (d.g <= 0L) {
                    n2 = 1;
                }
                if (n2 == 0) {
                    throw new IllegalArgumentException("Expedited jobs cannot be delayed".toString());
                }
            }
            final UUID randomUUID = UUID.randomUUID();
            fg0.d((Object)randomUUID, "randomUUID()");
            this.j(randomUUID);
            return c;
        }
        
        public abstract n92 c();
        
        public final boolean d() {
            return this.b;
        }
        
        public final UUID e() {
            return this.c;
        }
        
        public final Set f() {
            return this.e;
        }
        
        public abstract a g();
        
        public final p92 h() {
            return this.d;
        }
        
        public final a i(final zk j) {
            fg0.e((Object)j, "constraints");
            this.d.j = j;
            return this.g();
        }
        
        public final a j(final UUID c) {
            fg0.e((Object)c, "id");
            this.c = c;
            final String string = c.toString();
            fg0.d((Object)string, "id.toString()");
            this.d = new p92(string, this.d);
            return this.g();
        }
        
        public final a k(final androidx.work.b e) {
            fg0.e((Object)e, "inputData");
            this.d.e = e;
            return this.g();
        }
    }
    
    public static final class b
    {
    }
}

import java.lang.reflect.Array;
import java.util.ArrayList;
import com.google.gson.stream.JsonToken;
import java.lang.reflect.Type;
import com.google.gson.internal.$Gson$Types;
import java.lang.reflect.GenericArrayType;
import com.google.gson.reflect.TypeToken;

// 
// Decompiled by Procyon v0.6.0
// 

public final class v8 extends hz1
{
    public static final iz1 c;
    public final Class a;
    public final hz1 b;
    
    static {
        c = new iz1() {
            @Override
            public hz1 a(final gc0 gc0, final TypeToken typeToken) {
                final Type type = typeToken.getType();
                if (!(type instanceof GenericArrayType) && (!(type instanceof Class) || !((Class)type).isArray())) {
                    return null;
                }
                final Type g = $Gson$Types.g(type);
                return new v8(gc0, gc0.l(TypeToken.get(g)), $Gson$Types.k(g));
            }
        };
    }
    
    public v8(final gc0 gc0, final hz1 hz1, final Class a) {
        this.b = new jz1(gc0, hz1, a);
        this.a = a;
    }
    
    @Override
    public Object b(final rh0 rh0) {
        if (rh0.o0() == JsonToken.NULL) {
            rh0.R();
            return null;
        }
        final ArrayList list = new ArrayList();
        rh0.a();
        while (rh0.k()) {
            list.add(this.b.b(rh0));
        }
        rh0.f();
        final int size = list.size();
        if (this.a.isPrimitive()) {
            final Object instance = Array.newInstance(this.a, size);
            for (int i = 0; i < size; ++i) {
                Array.set(instance, i, list.get(i));
            }
            return instance;
        }
        return list.toArray((Object[])Array.newInstance(this.a, size));
    }
    
    @Override
    public void d(final vh0 vh0, final Object o) {
        if (o == null) {
            vh0.u();
            return;
        }
        vh0.c();
        for (int length = Array.getLength(o), i = 0; i < length; ++i) {
            this.b.d(vh0, Array.get(o, i));
        }
        vh0.f();
    }
}

import android.util.Log;
import org.jetbrains.annotations.Nullable;
import java.util.concurrent.Executor;
import androidx.core.os.a;
import kotlinx.coroutines.c;
import kotlin.coroutines.intrinsics.IntrinsicsKt__IntrinsicsJvmKt;
import org.jetbrains.annotations.NotNull;
import android.adservices.measurement.WebTriggerRegistrationRequest;
import android.adservices.measurement.WebSourceRegistrationRequest;
import android.adservices.measurement.DeletionRequest;
import android.content.Context;
import android.adservices.measurement.MeasurementManager;
import android.view.InputEvent;
import android.net.Uri;

// 
// Decompiled by Procyon v0.6.0
// 

public abstract class yu0
{
    public static final b a;
    
    static {
        a = new b(null);
    }
    
    public abstract Object a(final ls p0, final vl p1);
    
    public abstract Object b(final vl p0);
    
    public abstract Object c(final Uri p0, final InputEvent p1, final vl p2);
    
    public abstract Object d(final Uri p0, final vl p1);
    
    public abstract Object e(final w52 p0, final vl p1);
    
    public abstract Object f(final x52 p0, final vl p1);
    
    public static final class a extends yu0
    {
        public final MeasurementManager b;
        
        public a(final MeasurementManager b) {
            fg0.e((Object)b, "mMeasurementManager");
            this.b = b;
        }
        
        public a(final Context context) {
            fg0.e((Object)context, "context");
            final Object systemService = context.getSystemService(tu0.a());
            fg0.d(systemService, "context.getSystemService\u2026:class.java\n            )");
            this(uu0.a(systemService));
        }
        
        public static final /* synthetic */ MeasurementManager j(final a a) {
            return a.b;
        }
        
        @Nullable
        @Override
        public Object a(@NotNull final ls ls, @NotNull final vl vl) {
            final c c = new c(IntrinsicsKt__IntrinsicsJvmKt.c(vl), 1);
            c.z();
            su0.a(j(this), this.k(ls), (Executor)new xu0(), androidx.core.os.a.a((vl)c));
            final Object v = c.v();
            if (v == gg0.d()) {
                zp.c(vl);
            }
            if (v == gg0.d()) {
                return v;
            }
            return u02.a;
        }
        
        @Nullable
        @Override
        public Object b(@NotNull final vl vl) {
            final c c = new c(IntrinsicsKt__IntrinsicsJvmKt.c(vl), 1);
            c.z();
            mu0.a(j(this), (Executor)new xu0(), androidx.core.os.a.a((vl)c));
            final Object v = c.v();
            if (v == gg0.d()) {
                zp.c(vl);
            }
            return v;
        }
        
        @Nullable
        @Override
        public Object c(@NotNull final Uri uri, @Nullable final InputEvent inputEvent, @NotNull final vl vl) {
            final c c = new c(IntrinsicsKt__IntrinsicsJvmKt.c(vl), 1);
            c.z();
            ru0.a(j(this), uri, inputEvent, (Executor)new xu0(), androidx.core.os.a.a((vl)c));
            final Object v = c.v();
            if (v == gg0.d()) {
                zp.c(vl);
            }
            if (v == gg0.d()) {
                return v;
            }
            return u02.a;
        }
        
        @Nullable
        @Override
        public Object d(@NotNull final Uri uri, @NotNull final vl vl) {
            final c c = new c(IntrinsicsKt__IntrinsicsJvmKt.c(vl), 1);
            c.z();
            ou0.a(j(this), uri, (Executor)new xu0(), androidx.core.os.a.a((vl)c));
            final Object v = c.v();
            if (v == gg0.d()) {
                zp.c(vl);
            }
            if (v == gg0.d()) {
                return v;
            }
            return u02.a;
        }
        
        @Nullable
        @Override
        public Object e(@NotNull final w52 w52, @NotNull final vl vl) {
            final c c = new c(IntrinsicsKt__IntrinsicsJvmKt.c(vl), 1);
            c.z();
            qu0.a(j(this), this.l(w52), (Executor)new xu0(), androidx.core.os.a.a((vl)c));
            final Object v = c.v();
            if (v == gg0.d()) {
                zp.c(vl);
            }
            if (v == gg0.d()) {
                return v;
            }
            return u02.a;
        }
        
        @Nullable
        @Override
        public Object f(@NotNull final x52 x52, @NotNull final vl vl) {
            final c c = new c(IntrinsicsKt__IntrinsicsJvmKt.c(vl), 1);
            c.z();
            pu0.a(j(this), this.m(x52), (Executor)new xu0(), androidx.core.os.a.a((vl)c));
            final Object v = c.v();
            if (v == gg0.d()) {
                zp.c(vl);
            }
            if (v == gg0.d()) {
                return v;
            }
            return u02.a;
        }
        
        public final DeletionRequest k(final ls ls) {
            nu0.a();
            throw null;
        }
        
        public final WebSourceRegistrationRequest l(final w52 w52) {
            vu0.a();
            throw null;
        }
        
        public final WebTriggerRegistrationRequest m(final x52 x52) {
            wu0.a();
            throw null;
        }
    }
    
    public static final class b
    {
        public final yu0 a(final Context context) {
            fg0.e((Object)context, "context");
            final StringBuilder sb = new StringBuilder();
            sb.append("AdServicesInfo.version=");
            final a3 a = a3.a;
            sb.append(a.a());
            Log.d("MeasurementManager", sb.toString());
            a a2;
            if (a.a() >= 5) {
                a2 = new a(context);
            }
            else {
                a2 = null;
            }
            return a2;
        }
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

public class tf1 implements t90
{
    @Override
    public double a(final double[] array, final int n) {
        final double a = array[0];
        if (a < 9.223372036854776E18 && a > -9.223372036854776E18) {
            return (double)Math.round(a);
        }
        return a;
    }
    
    @Override
    public boolean b(final int n) {
        boolean b = true;
        if (n != 1) {
            b = false;
        }
        return b;
    }
    
    @Override
    public String toString() {
        return "round(x)";
    }
}

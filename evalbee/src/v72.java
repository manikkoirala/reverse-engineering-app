import android.view.inputmethod.InputMethodManager;
import android.view.WindowInsetsController;
import android.os.Build$VERSION;
import android.view.View;
import android.view.Window;

// 
// Decompiled by Procyon v0.6.0
// 

public final class v72
{
    public final e a;
    
    public v72(final Window window, final View view) {
        final int sdk_INT = Build$VERSION.SDK_INT;
        if (sdk_INT >= 30) {
            this.a = (e)new d(window, this);
        }
        else {
            a a;
            if (sdk_INT >= 26) {
                a = new c(window, view);
            }
            else {
                a = new b(window, view);
            }
            this.a = (e)a;
        }
    }
    
    public v72(final WindowInsetsController windowInsetsController) {
        this.a = (e)new d(windowInsetsController, this);
    }
    
    public static v72 f(final WindowInsetsController windowInsetsController) {
        return new v72(windowInsetsController);
    }
    
    public void a(final int n) {
        this.a.a(n);
    }
    
    public boolean b() {
        return this.a.b();
    }
    
    public void c(final boolean b) {
        this.a.c(b);
    }
    
    public void d(final boolean b) {
        this.a.d(b);
    }
    
    public void e(final int n) {
        this.a.e(n);
    }
    
    public abstract static class a extends e
    {
        public final Window a;
        public final View b;
        
        public a(final Window a, final View b) {
            this.a = a;
            this.b = b;
        }
        
        @Override
        public void a(final int n) {
            for (int i = 1; i <= 256; i <<= 1) {
                if ((n & i) != 0x0) {
                    this.g(i);
                }
            }
        }
        
        @Override
        public void e(final int n) {
            for (int i = 1; i <= 256; i <<= 1) {
                if ((n & i) != 0x0) {
                    this.k(i);
                }
            }
        }
        
        public final void g(final int n) {
            if (n == 1) {
                this.i(4);
                return;
            }
            if (n != 2) {
                if (n == 8) {
                    ((InputMethodManager)this.a.getContext().getSystemService("input_method")).hideSoftInputFromWindow(this.a.getDecorView().getWindowToken(), 0);
                }
                return;
            }
            this.i(2);
        }
        
        public void i(final int n) {
            final View decorView = this.a.getDecorView();
            decorView.setSystemUiVisibility(n | decorView.getSystemUiVisibility());
        }
        
        public void j(final int n) {
            this.a.addFlags(n);
        }
        
        public final void k(final int n) {
            if (n == 1) {
                this.l(4);
                this.m(1024);
                return;
            }
            if (n != 2) {
                if (n == 8) {
                    View view = this.b;
                    if (!view.isInEditMode() && !view.onCheckIsTextEditor()) {
                        view = this.a.getCurrentFocus();
                    }
                    else {
                        view.requestFocus();
                    }
                    View viewById = view;
                    if (view == null) {
                        viewById = this.a.findViewById(16908290);
                    }
                    if (viewById != null && viewById.hasWindowFocus()) {
                        viewById.post((Runnable)new u72(viewById));
                    }
                }
                return;
            }
            this.l(2);
        }
        
        public void l(final int n) {
            final View decorView = this.a.getDecorView();
            decorView.setSystemUiVisibility(~n & decorView.getSystemUiVisibility());
        }
        
        public void m(final int n) {
            this.a.clearFlags(n);
        }
    }
    
    public static class b extends a
    {
        public b(final Window window, final View view) {
            super(window, view);
        }
        
        @Override
        public boolean b() {
            return (super.a.getDecorView().getSystemUiVisibility() & 0x2000) != 0x0;
        }
        
        @Override
        public void d(final boolean b) {
            if (b) {
                ((a)this).m(67108864);
                ((a)this).j(Integer.MIN_VALUE);
                ((a)this).i(8192);
            }
            else {
                ((a)this).l(8192);
            }
        }
    }
    
    public static class c extends b
    {
        public c(final Window window, final View view) {
            super(window, view);
        }
        
        @Override
        public void c(final boolean b) {
            if (b) {
                ((a)this).m(134217728);
                ((a)this).j(Integer.MIN_VALUE);
                ((a)this).i(16);
            }
            else {
                ((a)this).l(16);
            }
        }
    }
    
    public static class d extends e
    {
        public final v72 a;
        public final WindowInsetsController b;
        public final co1 c;
        public Window d;
        
        public d(final Window d, final v72 v72) {
            this(z72.a(d), v72);
            this.d = d;
        }
        
        public d(final WindowInsetsController b, final v72 a) {
            this.c = new co1();
            this.b = b;
            this.a = a;
        }
        
        @Override
        public void a(final int n) {
            a82.a(this.b, n);
        }
        
        @Override
        public boolean b() {
            return (x72.a(this.b) & 0x8) != 0x0;
        }
        
        @Override
        public void c(final boolean b) {
            if (b) {
                if (this.d != null) {
                    this.f(16);
                }
                y72.a(this.b, 16, 16);
            }
            else {
                if (this.d != null) {
                    this.g(16);
                }
                y72.a(this.b, 0, 16);
            }
        }
        
        @Override
        public void d(final boolean b) {
            if (b) {
                if (this.d != null) {
                    this.f(8192);
                }
                y72.a(this.b, 8, 8);
            }
            else {
                if (this.d != null) {
                    this.g(8192);
                }
                y72.a(this.b, 0, 8);
            }
        }
        
        @Override
        public void e(final int n) {
            final Window d = this.d;
            if (d != null && (n & 0x8) != 0x0 && Build$VERSION.SDK_INT < 32) {
                ((InputMethodManager)d.getContext().getSystemService("input_method")).isActive();
            }
            w72.a(this.b, n);
        }
        
        public void f(final int n) {
            final View decorView = this.d.getDecorView();
            decorView.setSystemUiVisibility(n | decorView.getSystemUiVisibility());
        }
        
        public void g(final int n) {
            final View decorView = this.d.getDecorView();
            decorView.setSystemUiVisibility(~n & decorView.getSystemUiVisibility());
        }
    }
    
    public abstract static class e
    {
        public abstract void a(final int p0);
        
        public abstract boolean b();
        
        public void c(final boolean b) {
        }
        
        public abstract void d(final boolean p0);
        
        public abstract void e(final int p0);
    }
}

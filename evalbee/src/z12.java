import java.util.List;

// 
// Decompiled by Procyon v0.6.0
// 

public class z12
{
    public final a11 a;
    public final q00 b;
    public final List c;
    
    public z12(final a11 a, final q00 b, final List c) {
        this.a = a;
        this.b = b;
        this.c = c;
    }
    
    public wx0 a(final du du, final h71 h71) {
        final q00 b = this.b;
        if (b != null) {
            return new e31(du, this.a, b, h71, this.c);
        }
        return new qm1(du, this.a, h71, this.c);
    }
}

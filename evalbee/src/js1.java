import android.view.MenuItem;
import java.util.ArrayList;
import android.view.ActionMode$Callback;
import android.view.MenuInflater;
import android.view.Menu;
import android.view.View;
import android.content.Context;
import android.view.ActionMode;

// 
// Decompiled by Procyon v0.6.0
// 

public class js1 extends ActionMode
{
    public final Context a;
    public final c2 b;
    
    public js1(final Context a, final c2 b) {
        this.a = a;
        this.b = b;
    }
    
    public void finish() {
        this.b.a();
    }
    
    public View getCustomView() {
        return this.b.b();
    }
    
    public Menu getMenu() {
        return (Menu)new sv0(this.a, (ks1)this.b.c());
    }
    
    public MenuInflater getMenuInflater() {
        return this.b.d();
    }
    
    public CharSequence getSubtitle() {
        return this.b.e();
    }
    
    public Object getTag() {
        return this.b.f();
    }
    
    public CharSequence getTitle() {
        return this.b.g();
    }
    
    public boolean getTitleOptionalHint() {
        return this.b.h();
    }
    
    public void invalidate() {
        this.b.i();
    }
    
    public boolean isTitleOptional() {
        return this.b.j();
    }
    
    public void setCustomView(final View view) {
        this.b.k(view);
    }
    
    public void setSubtitle(final int n) {
        this.b.l(n);
    }
    
    public void setSubtitle(final CharSequence charSequence) {
        this.b.m(charSequence);
    }
    
    public void setTag(final Object o) {
        this.b.n(o);
    }
    
    public void setTitle(final int n) {
        this.b.o(n);
    }
    
    public void setTitle(final CharSequence charSequence) {
        this.b.p(charSequence);
    }
    
    public void setTitleOptionalHint(final boolean b) {
        this.b.q(b);
    }
    
    public static class a implements c2.a
    {
        public final ActionMode$Callback a;
        public final Context b;
        public final ArrayList c;
        public final co1 d;
        
        public a(final Context b, final ActionMode$Callback a) {
            this.b = b;
            this.a = a;
            this.c = new ArrayList();
            this.d = new co1();
        }
        
        @Override
        public boolean a(final c2 c2, final MenuItem menuItem) {
            return this.a.onActionItemClicked(this.e(c2), (MenuItem)new ov0(this.b, (ms1)menuItem));
        }
        
        @Override
        public void b(final c2 c2) {
            this.a.onDestroyActionMode(this.e(c2));
        }
        
        @Override
        public boolean c(final c2 c2, final Menu menu) {
            return this.a.onCreateActionMode(this.e(c2), this.f(menu));
        }
        
        @Override
        public boolean d(final c2 c2, final Menu menu) {
            return this.a.onPrepareActionMode(this.e(c2), this.f(menu));
        }
        
        public ActionMode e(final c2 c2) {
            for (int size = this.c.size(), i = 0; i < size; ++i) {
                final js1 js1 = this.c.get(i);
                if (js1 != null && js1.b == c2) {
                    return js1;
                }
            }
            final js1 e = new js1(this.b, c2);
            this.c.add(e);
            return e;
        }
        
        public final Menu f(final Menu menu) {
            Object o;
            if ((o = this.d.get(menu)) == null) {
                o = new sv0(this.b, (ks1)menu);
                this.d.put(menu, o);
            }
            return (Menu)o;
        }
    }
}

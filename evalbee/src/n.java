import com.google.common.hash.Funnel;
import java.nio.charset.Charset;

// 
// Decompiled by Procyon v0.6.0
// 

public abstract class n implements qc0
{
    @Override
    public qc0 c(final CharSequence charSequence) {
        for (int length = charSequence.length(), i = 0; i < length; ++i) {
            this.k(charSequence.charAt(i));
        }
        return this;
    }
    
    @Override
    public qc0 d(final CharSequence charSequence, final Charset charset) {
        return this.j(charSequence.toString().getBytes(charset));
    }
    
    @Override
    public qc0 g(final Object o, final Funnel funnel) {
        funnel.funnel(o, this);
        return this;
    }
    
    @Override
    public abstract qc0 h(final byte[] p0, final int p1, final int p2);
    
    public qc0 j(final byte[] array) {
        return this.h(array, 0, array.length);
    }
    
    public abstract qc0 k(final char p0);
}

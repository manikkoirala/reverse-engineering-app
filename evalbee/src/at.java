import java.util.List;
import java.util.HashSet;
import java.util.ArrayList;

// 
// Decompiled by Procyon v0.6.0
// 

public final class at
{
    public final w51 a;
    public final co1 b;
    public final ArrayList c;
    public final HashSet d;
    
    public at() {
        this.a = new y51(10);
        this.b = new co1();
        this.c = new ArrayList();
        this.d = new HashSet();
    }
    
    public void a(final Object o, final Object e) {
        if (this.b.containsKey(o) && this.b.containsKey(e)) {
            ArrayList f;
            if ((f = (ArrayList)this.b.get(o)) == null) {
                f = this.f();
                this.b.put(o, f);
            }
            f.add(e);
            return;
        }
        throw new IllegalArgumentException("All nodes must be present in the graph before being added as an edge");
    }
    
    public void b(final Object o) {
        if (!this.b.containsKey(o)) {
            this.b.put(o, null);
        }
    }
    
    public void c() {
        for (int size = this.b.size(), i = 0; i < size; ++i) {
            final ArrayList list = (ArrayList)this.b.m(i);
            if (list != null) {
                this.k(list);
            }
        }
        this.b.clear();
    }
    
    public boolean d(final Object o) {
        return this.b.containsKey(o);
    }
    
    public final void e(final Object e, final ArrayList list, final HashSet set) {
        if (list.contains(e)) {
            return;
        }
        if (!set.contains(e)) {
            set.add(e);
            final ArrayList list2 = (ArrayList)this.b.get(e);
            if (list2 != null) {
                for (int size = list2.size(), i = 0; i < size; ++i) {
                    this.e(list2.get(i), list, set);
                }
            }
            set.remove(e);
            list.add(e);
            return;
        }
        throw new RuntimeException("This graph contains cyclic dependencies");
    }
    
    public final ArrayList f() {
        ArrayList list;
        if ((list = (ArrayList)this.a.a()) == null) {
            list = new ArrayList();
        }
        return list;
    }
    
    public List g(final Object o) {
        return (List)this.b.get(o);
    }
    
    public List h(final Object o) {
        final int size = this.b.size();
        ArrayList<Object> list = null;
        ArrayList<Object> list3;
        for (int i = 0; i < size; ++i, list = list3) {
            final ArrayList list2 = (ArrayList)this.b.m(i);
            list3 = list;
            if (list2 != null) {
                list3 = list;
                if (list2.contains(o)) {
                    if ((list3 = list) == null) {
                        list3 = new ArrayList<Object>();
                    }
                    list3.add(this.b.i(i));
                }
            }
        }
        return list;
    }
    
    public ArrayList i() {
        this.c.clear();
        this.d.clear();
        for (int size = this.b.size(), i = 0; i < size; ++i) {
            this.e(this.b.i(i), this.c, this.d);
        }
        return this.c;
    }
    
    public boolean j(final Object o) {
        for (int size = this.b.size(), i = 0; i < size; ++i) {
            final ArrayList list = (ArrayList)this.b.m(i);
            if (list != null && list.contains(o)) {
                return true;
            }
        }
        return false;
    }
    
    public final void k(final ArrayList list) {
        list.clear();
        this.a.b(list);
    }
}

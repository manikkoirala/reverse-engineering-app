import java.util.Iterator;
import com.google.firebase.firestore.core.DocumentViewChange;
import java.util.List;
import java.util.ArrayList;
import com.google.firebase.firestore.core.ViewSnapshot;
import com.google.firebase.database.collection.c;

// 
// Decompiled by Procyon v0.6.0
// 

public final class ml0
{
    public final int a;
    public final boolean b;
    public final c c;
    public final c d;
    
    public ml0(final int a, final boolean b, final c c, final c d) {
        this.a = a;
        this.b = b;
        this.c = c;
        this.d = d;
    }
    
    public static ml0 a(final int n, final ViewSnapshot viewSnapshot) {
        c c = new c(new ArrayList(), du.a());
        c c2 = new c(new ArrayList(), du.a());
        for (final DocumentViewChange documentViewChange : viewSnapshot.d()) {
            final int n2 = ml0$a.a[documentViewChange.c().ordinal()];
            if (n2 != 1) {
                if (n2 != 2) {
                    continue;
                }
                c2 = c2.c(documentViewChange.b().getKey());
            }
            else {
                c = c.c(documentViewChange.b().getKey());
            }
        }
        return new ml0(n, viewSnapshot.k(), c, c2);
    }
    
    public c b() {
        return this.c;
    }
    
    public c c() {
        return this.d;
    }
    
    public int d() {
        return this.a;
    }
    
    public boolean e() {
        return this.b;
    }
}

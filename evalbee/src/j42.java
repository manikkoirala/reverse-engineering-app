import java.util.Iterator;
import android.view.View;
import android.view.animation.Interpolator;
import java.util.ArrayList;

// 
// Decompiled by Procyon v0.6.0
// 

public class j42
{
    public final ArrayList a;
    public long b;
    public Interpolator c;
    public k42 d;
    public boolean e;
    public final l42 f;
    
    public j42() {
        this.b = -1L;
        this.f = new l42(this) {
            public boolean a = false;
            public int b = 0;
            public final j42 c;
            
            @Override
            public void b(final View view) {
                final int b = this.b + 1;
                this.b = b;
                if (b == this.c.a.size()) {
                    final k42 d = this.c.d;
                    if (d != null) {
                        d.b(null);
                    }
                    this.d();
                }
            }
            
            @Override
            public void c(final View view) {
                if (this.a) {
                    return;
                }
                this.a = true;
                final k42 d = this.c.d;
                if (d != null) {
                    d.c(null);
                }
            }
            
            public void d() {
                this.b = 0;
                this.a = false;
                this.c.b();
            }
        };
        this.a = new ArrayList();
    }
    
    public void a() {
        if (!this.e) {
            return;
        }
        final Iterator iterator = this.a.iterator();
        while (iterator.hasNext()) {
            ((i42)iterator.next()).c();
        }
        this.e = false;
    }
    
    public void b() {
        this.e = false;
    }
    
    public j42 c(final i42 e) {
        if (!this.e) {
            this.a.add(e);
        }
        return this;
    }
    
    public j42 d(final i42 e, final i42 e2) {
        this.a.add(e);
        e2.j(e.d());
        this.a.add(e2);
        return this;
    }
    
    public j42 e(final long b) {
        if (!this.e) {
            this.b = b;
        }
        return this;
    }
    
    public j42 f(final Interpolator c) {
        if (!this.e) {
            this.c = c;
        }
        return this;
    }
    
    public j42 g(final k42 d) {
        if (!this.e) {
            this.d = d;
        }
        return this;
    }
    
    public void h() {
        if (this.e) {
            return;
        }
        for (final i42 i42 : this.a) {
            final long b = this.b;
            if (b >= 0L) {
                i42.f(b);
            }
            final Interpolator c = this.c;
            if (c != null) {
                i42.g(c);
            }
            if (this.d != null) {
                i42.h(this.f);
            }
            i42.l();
        }
        this.e = true;
    }
}

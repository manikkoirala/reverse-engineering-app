import com.google.firebase.firestore.model.MutableDocument;

// 
// Decompiled by Procyon v0.6.0
// 

public final class hs extends wx0
{
    public hs(final du du, final h71 h71) {
        super(du, h71);
    }
    
    @Override
    public q00 a(final MutableDocument mutableDocument, final q00 q00, final pw1 pw1) {
        this.n(mutableDocument);
        if (this.h().e(mutableDocument)) {
            mutableDocument.m(mutableDocument.getVersion()).u();
            return null;
        }
        return q00;
    }
    
    @Override
    public void b(final MutableDocument mutableDocument, final ay0 ay0) {
        this.n(mutableDocument);
        g9.d(ay0.a().isEmpty(), "Transform results received by DeleteMutation.", new Object[0]);
        mutableDocument.m(ay0.b()).t();
    }
    
    @Override
    public q00 e() {
        return null;
    }
    
    @Override
    public boolean equals(final Object o) {
        return this == o || (o != null && hs.class == o.getClass() && this.i((wx0)o));
    }
    
    @Override
    public int hashCode() {
        return this.j();
    }
    
    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append("DeleteMutation{");
        sb.append(this.k());
        sb.append("}");
        return sb.toString();
    }
}

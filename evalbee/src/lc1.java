import androidx.datastore.preferences.protobuf.ProtoSyntax;
import androidx.datastore.preferences.protobuf.y;

// 
// Decompiled by Procyon v0.6.0
// 

public final class lc1 implements uv0
{
    public final y a;
    public final String b;
    public final Object[] c;
    public final int d;
    
    public lc1(final y a, final String b, final Object[] c) {
        this.a = a;
        this.b = b;
        this.c = c;
        int char1 = b.charAt(0);
        if (char1 >= 55296) {
            int n = char1 & 0x1FFF;
            int n2 = 13;
            int index = 1;
            char char2;
            while (true) {
                char2 = b.charAt(index);
                if (char2 < '\ud800') {
                    break;
                }
                n |= (char2 & '\u1fff') << n2;
                n2 += 13;
                ++index;
            }
            char1 = (n | char2 << n2);
        }
        this.d = char1;
    }
    
    @Override
    public boolean a() {
        return (this.d & 0x2) == 0x2;
    }
    
    @Override
    public y b() {
        return this.a;
    }
    
    @Override
    public ProtoSyntax c() {
        ProtoSyntax protoSyntax;
        if ((this.d & 0x1) == 0x1) {
            protoSyntax = ProtoSyntax.PROTO2;
        }
        else {
            protoSyntax = ProtoSyntax.PROTO3;
        }
        return protoSyntax;
    }
    
    public Object[] d() {
        return this.c;
    }
    
    public String e() {
        return this.b;
    }
}

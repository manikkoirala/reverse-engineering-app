import java.text.ParseException;
import com.google.gson.JsonSyntaxException;
import com.google.gson.stream.JsonToken;
import java.text.SimpleDateFormat;
import java.sql.Date;
import com.google.gson.reflect.TypeToken;
import java.text.DateFormat;

// 
// Decompiled by Procyon v0.6.0
// 

public final class cp1 extends hz1
{
    public static final iz1 b;
    public final DateFormat a;
    
    static {
        b = new iz1() {
            @Override
            public hz1 a(final gc0 gc0, final TypeToken typeToken) {
                final Class rawType = typeToken.getRawType();
                hz1 hz1 = null;
                if (rawType == Date.class) {
                    hz1 = new cp1(null);
                }
                return hz1;
            }
        };
    }
    
    public cp1() {
        this.a = new SimpleDateFormat("MMM d, yyyy");
    }
    
    public Date e(final rh0 rh0) {
        if (rh0.o0() == JsonToken.NULL) {
            rh0.R();
            return null;
        }
        final String i0 = rh0.i0();
        try {
            synchronized (this) {
                final java.util.Date parse = this.a.parse(i0);
                monitorexit(this);
                return new Date(parse.getTime());
            }
        }
        catch (final ParseException ex) {
            final StringBuilder sb = new StringBuilder();
            sb.append("Failed parsing '");
            sb.append(i0);
            sb.append("' as SQL Date; at path ");
            sb.append(rh0.j());
            throw new JsonSyntaxException(sb.toString(), ex);
        }
    }
    
    public void f(final vh0 vh0, final Date date) {
        if (date == null) {
            vh0.u();
            return;
        }
        synchronized (this) {
            final String format = this.a.format(date);
            monitorexit(this);
            vh0.r0(format);
        }
    }
}

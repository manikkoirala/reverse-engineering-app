// 
// Decompiled by Procyon v0.6.0
// 

public abstract class wa extends z21
{
    public static int[] k;
    public static int[] l;
    public static double[] m;
    public z22 d;
    public double e;
    public double f;
    public int g;
    public int h;
    public int i;
    public boolean j;
    
    static {
        wa.k = new int[0];
        wa.l = new int[0];
        wa.m = new double[0];
    }
    
    public wa(final zl zl, final ub0 ub0) {
        super(zl, ub0);
        this.d = new z22(new double[] { 0.0, 0.0, 0.0, 0.0, 1.0, 1.0, 1.0, 1.0 }, 8);
        this.e = 0.0;
        this.f = 1.0;
        this.g = 1;
        this.h = 4;
        this.i = 0;
        this.j = true;
    }
    
    @Override
    public abstract void c(final double[] p0);
    
    @Override
    public int d() {
        return this.g;
    }
    
    public double e(final double n, final int n2) {
        int n3 = 0;
        double n5;
        while (true) {
            final int h = this.h;
            double n4 = n5 = 0.0;
            if (n3 >= h) {
                break;
            }
            final double[] m = wa.m;
            final int n6 = n2 + n3;
            final double n7 = m[n6];
            final double n8 = m[n6 + 1];
            if (n >= n7 && n <= n8 && n7 != n8) {
                final int n9 = h - 2;
                for (int i = h - n3 - 1; i >= 0; --i) {
                    wa.k[i] = 0;
                }
                if (n3 > 0) {
                    for (int j = 0; j < n3; ++j) {
                        wa.l[j] = j;
                    }
                    wa.l[n3] = Integer.MAX_VALUE;
                }
                else {
                    final int[] l = wa.l;
                    l[0] = n9;
                    l[1] = this.h;
                }
                while (true) {
                    int n10 = 0;
                    while (true) {
                        final int[] k = wa.l;
                        final int n11 = k[n10];
                        final int n12 = n10 + 1;
                        if (n11 < k[n12] - 1) {
                            break;
                        }
                        n10 = n12;
                    }
                    int n13 = n9 - n3;
                    int n14 = n3 - 1;
                    int h2 = this.h;
                    double n15 = 1.0;
                    int n16 = 0;
                    for (int n17 = n9; n17 >= 0; --n17, --h2) {
                        if (n14 >= 0 && wa.l[n14] == n17) {
                            final int n18 = n2 + n16;
                            final double[] m2 = wa.m;
                            final double n19 = m2[n18 + h2];
                            n15 *= (n19 - n) / (n19 - m2[n18 + 1]);
                            ++n16;
                            --n14;
                        }
                        else {
                            final int n20 = n2 + wa.k[n13];
                            final double[] m3 = wa.m;
                            final double n21 = m3[n20];
                            n15 *= (n - n21) / (m3[n20 + h2 - 1] - n21);
                            --n13;
                        }
                    }
                    if (n3 > 0) {
                        int n22 = 0;
                        boolean b = false;
                        while (true) {
                            final int[] k2 = wa.k;
                            final int n23 = k2[n22] + 1;
                            k2[n22] = n23;
                            if (n23 <= n3) {
                                break;
                            }
                            ++n22;
                            b = true;
                        }
                        if (b) {
                            for (int n24 = n22 - 1; n24 >= 0; --n24) {
                                final int[] k3 = wa.k;
                                k3[n24] = k3[n22];
                            }
                        }
                    }
                    n5 = n4 + n15;
                    final int[] l2 = wa.l;
                    if (++l2[n10] > n9) {
                        break;
                    }
                    int n25 = 0;
                    while (true) {
                        n4 = n5;
                        if (n25 >= n10) {
                            break;
                        }
                        wa.l[n25] = n25;
                        ++n25;
                    }
                }
                break;
            }
            ++n3;
        }
        return n5;
    }
    
    public void f(final px0 px0) {
        final ub0 b = super.b;
        final int c = super.a.c();
        int n = 0;
        final int n2 = 0;
        if (!b.b(0, c)) {
            throw new IllegalArgumentException("Group iterator not in range");
        }
        final int a = super.b.a();
        final int h = this.h;
        final int n3 = a - h;
        if (n3 >= 0) {
            final int i = a + h;
            if (wa.m.length < i) {
                wa.m = new double[i * 2];
            }
            final double e = this.e;
            final double f = this.f;
            final int j = this.i;
            double n5;
            double n6;
            if (j == 2) {
                if (this.d.f() != i) {
                    final StringBuilder sb = new StringBuilder();
                    sb.append("knotVector.size(");
                    sb.append(this.d.f());
                    sb.append(") != ");
                    sb.append(i);
                    throw new IllegalArgumentException(sb.toString());
                }
                wa.m[0] = this.d.c(0);
                int n4 = 1;
                while (true) {
                    n5 = e;
                    n6 = f;
                    if (n4 >= i) {
                        break;
                    }
                    wa.m[n4] = this.d.c(n4);
                    final double[] m = wa.m;
                    if (m[n4] < m[n4 - 1]) {
                        final StringBuilder sb2 = new StringBuilder();
                        sb2.append("Knot not in sorted order! (knot[");
                        sb2.append(n4);
                        sb2.append("] < knot[");
                        sb2.append(n4);
                        sb2.append("-1])");
                        throw new IllegalArgumentException(sb2.toString());
                    }
                    ++n4;
                }
            }
            else if (j == 1) {
                final double n7 = 1.0 / (i - 1);
                for (int k = n2; k < i; ++k) {
                    wa.m[k] = k * n7;
                }
                n5 = e;
                n6 = f;
                if (this.j) {
                    final int h2 = this.h;
                    n5 = (h2 - 1) * n7;
                    n6 = 1.0 - (h2 - 1) * n7;
                }
            }
            else {
                n5 = e;
                n6 = f;
                if (j == 0) {
                    final double n8 = 1.0 / (n3 + 1);
                    int h3;
                    while (true) {
                        h3 = this.h;
                        if (n >= h3) {
                            break;
                        }
                        wa.m[n] = 0.0;
                        ++n;
                    }
                    int n9 = 1;
                    int n10 = h3;
                    int l;
                    while (true) {
                        l = n10;
                        if (n9 > n3) {
                            break;
                        }
                        wa.m[n10] = n9 * n8;
                        ++n9;
                        ++n10;
                    }
                    while (l < i) {
                        wa.m[l] = 1.0;
                        ++l;
                    }
                    n5 = e;
                    n6 = f;
                    if (this.j) {
                        n6 = 1.0;
                        n5 = 0.0;
                    }
                }
            }
            final int length = wa.k.length;
            final int h4 = this.h;
            if (length < h4) {
                wa.k = new int[h4 * 2];
                wa.l = new int[h4 * 2];
            }
            final double[] array = new double[px0.c() + 1];
            array[px0.c()] = n5;
            this.c(array);
            if (super.c) {
                px0.e(array);
            }
            else {
                px0.f(array);
            }
            ac.b(this, n5, n6, px0);
            return;
        }
        throw new IllegalArgumentException("group iterator size - degree < 0");
    }
    
    public void g(final int n) {
        if (n > 0) {
            this.h = n + 1;
            return;
        }
        throw new IllegalArgumentException("Degree > 0 required.");
    }
    
    public void h(final z22 d) {
        if (d != null) {
            this.d = d;
            return;
        }
        throw new IllegalArgumentException("Knot-vector cannot be null.");
    }
    
    public void i(final int i) {
        if (i >= 0 && i <= 2) {
            this.i = i;
            return;
        }
        throw new IllegalArgumentException("Unknown knot-vector type.");
    }
}

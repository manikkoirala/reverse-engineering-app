import java.util.Collection;
import com.google.android.gms.tasks.Tasks;
import com.google.android.gms.tasks.Continuation;
import java.util.concurrent.Executor;
import java.io.File;
import java.util.SortedSet;
import java.util.Iterator;
import java.util.Comparator;
import java.util.Collections;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import android.content.Context;
import java.nio.charset.StandardCharsets;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.io.IOException;
import android.app.ApplicationExitInfo;
import com.google.firebase.crashlytics.internal.model.CrashlyticsReport;
import com.google.android.gms.tasks.Task;

// 
// Decompiled by Procyon v0.6.0
// 

public class nm1
{
    public final kn a;
    public final zn b;
    public final op c;
    public final ul0 d;
    public final f22 e;
    public final de0 f;
    
    public nm1(final kn a, final zn b, final op c, final ul0 d, final f22 e, final de0 f) {
        this.a = a;
        this.b = b;
        this.c = c;
        this.d = d;
        this.e = e;
        this.f = f;
    }
    
    public static CrashlyticsReport.a f(final ApplicationExitInfo applicationExitInfo) {
        final String s = null;
        String g;
        try {
            final InputStream a = em1.a(applicationExitInfo);
            g = s;
            if (a != null) {
                g = g(a);
            }
        }
        catch (final IOException obj) {
            final zl0 f = zl0.f();
            final StringBuilder sb = new StringBuilder();
            sb.append("Could not get input trace in application exit info: ");
            sb.append(fm1.a(applicationExitInfo));
            sb.append(" Error: ");
            sb.append(obj);
            f.k(sb.toString());
            g = s;
        }
        return CrashlyticsReport.a.a().c(gm1.a(applicationExitInfo)).e(hm1.a(applicationExitInfo)).g(n70.a(applicationExitInfo)).i(o70.a(applicationExitInfo)).d(im1.a(applicationExitInfo)).f(jm1.a(applicationExitInfo)).h(km1.a(applicationExitInfo)).j(g).a();
    }
    
    public static String g(final InputStream inputStream) {
        final ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        final byte[] array = new byte[8192];
        while (true) {
            final int read = inputStream.read(array);
            if (read == -1) {
                break;
            }
            byteArrayOutputStream.write(array, 0, read);
        }
        return byteArrayOutputStream.toString(StandardCharsets.UTF_8.name());
    }
    
    public static nm1 h(final Context context, final de0 de0, final z00 z00, final s7 s7, final ul0 ul0, final f22 f22, final lp1 lp1, final zm1 zm1, final m11 m11, final wm wm) {
        return new nm1(new kn(context, de0, s7, lp1, zm1), new zn(z00, zm1, wm), op.b(context, zm1, m11), ul0, f22, de0);
    }
    
    public static List m(final Map map) {
        final ArrayList list = new ArrayList();
        list.ensureCapacity(map.size());
        for (final Map.Entry<String, V> entry : map.entrySet()) {
            list.add(CrashlyticsReport.c.a().b(entry.getKey()).c((String)entry.getValue()).a());
        }
        Collections.sort((List<Object>)list, new lm1());
        return Collections.unmodifiableList((List<?>)list);
    }
    
    public final CrashlyticsReport.e.d c(final CrashlyticsReport.e.d d, final ul0 ul0, final f22 f22) {
        final CrashlyticsReport.e.d.b h = d.h();
        final String c = ul0.c();
        if (c != null) {
            h.d(CrashlyticsReport.e.d.d.a().b(c).a());
        }
        else {
            zl0.f().i("No log data to include with this event.");
        }
        final List m = m(f22.d());
        final List i = m(f22.e());
        if (!m.isEmpty() || !i.isEmpty()) {
            h.b(d.b().i().e(m).g(i).a());
        }
        return h.a();
    }
    
    public final CrashlyticsReport.e.d d(final CrashlyticsReport.e.d d) {
        return this.e(this.c(d, this.d, this.e), this.e);
    }
    
    public final CrashlyticsReport.e.d e(final CrashlyticsReport.e.d d, final f22 f22) {
        final List f23 = f22.f();
        if (f23.isEmpty()) {
            return d;
        }
        final CrashlyticsReport.e.d.b h = d.h();
        h.e(CrashlyticsReport.e.d.f.a().b(f23).a());
        return h.a();
    }
    
    public final ao i(final ao ao) {
        ao a = ao;
        if (ao.b().g() == null) {
            a = ao.a(ao.b().r(this.f.d()), ao.d(), ao.c());
        }
        return a;
    }
    
    public void j(final String s, final List list, final CrashlyticsReport.a a) {
        zl0.f().b("SessionReportingCoordinator#finalizeSessionWithNativeEvent");
        final ArrayList list2 = new ArrayList();
        final Iterator iterator = list.iterator();
        while (iterator.hasNext()) {
            final CrashlyticsReport.d.b b = ((ey0)iterator.next()).b();
            if (b != null) {
                list2.add(b);
            }
        }
        this.b.l(s, CrashlyticsReport.d.a().b(Collections.unmodifiableList((List<?>)list2)).a(), a);
    }
    
    public void k(final long n, final String s) {
        this.b.k(s, n);
    }
    
    public final ApplicationExitInfo l(final String s, final List list) {
        final long q = this.b.q(s);
        final Iterator iterator = list.iterator();
        while (iterator.hasNext()) {
            final ApplicationExitInfo a = m70.a(iterator.next());
            if (o70.a(a) < q) {
                return null;
            }
            if (n70.a(a) != 6) {
                continue;
            }
            return a;
        }
        return null;
    }
    
    public boolean n() {
        return this.b.r();
    }
    
    public SortedSet p() {
        return this.b.p();
    }
    
    public void q(final String s, final long n) {
        this.b.z(this.a.e(s, n));
    }
    
    public final boolean r(final Task task) {
        if (task.isSuccessful()) {
            final ao ao = (ao)task.getResult();
            final zl0 f = zl0.f();
            final StringBuilder sb = new StringBuilder();
            sb.append("Crashlytics report successfully enqueued to DataTransport: ");
            sb.append(ao.d());
            f.b(sb.toString());
            final File c = ao.c();
            if (c.delete()) {
                final zl0 f2 = zl0.f();
                final StringBuilder sb2 = new StringBuilder();
                sb2.append("Deleted report file: ");
                sb2.append(c.getPath());
                f2.b(sb2.toString());
            }
            else {
                final zl0 f3 = zl0.f();
                final StringBuilder sb3 = new StringBuilder();
                sb3.append("Crashlytics could not delete report file: ");
                sb3.append(c.getPath());
                f3.k(sb3.toString());
            }
            return true;
        }
        zl0.f().l("Crashlytics report could not be enqueued to DataTransport", task.getException());
        return false;
    }
    
    public final void s(final Throwable t, final Thread thread, final String s, final String s2, final long n, final boolean b) {
        this.b.y(this.d(this.a.d(t, thread, s2, n, 4, 8, b)), s, s2.equals("crash"));
    }
    
    public void t(final Throwable t, final Thread thread, final String str, final long n) {
        final zl0 f = zl0.f();
        final StringBuilder sb = new StringBuilder();
        sb.append("Persisting fatal event for session ");
        sb.append(str);
        f.i(sb.toString());
        this.s(t, thread, str, "crash", n, true);
    }
    
    public void u(final String s, final List list, final ul0 ul0, final f22 f22) {
        final ApplicationExitInfo l = this.l(s, list);
        if (l == null) {
            final zl0 f23 = zl0.f();
            final StringBuilder sb = new StringBuilder();
            sb.append("No relevant ApplicationExitInfo occurred during session: ");
            sb.append(s);
            f23.i(sb.toString());
            return;
        }
        final CrashlyticsReport.e.d c = this.a.c(f(l));
        final zl0 f24 = zl0.f();
        final StringBuilder sb2 = new StringBuilder();
        sb2.append("Persisting anr for session ");
        sb2.append(s);
        f24.b(sb2.toString());
        this.b.y(this.e(this.c(c, ul0, f22), f22), s, true);
    }
    
    public void v() {
        this.b.i();
    }
    
    public Task w(final Executor executor) {
        return this.x(executor, null);
    }
    
    public Task x(final Executor executor, final String s) {
        final List w = this.b.w();
        final ArrayList list = new ArrayList();
        for (final ao ao : w) {
            if (s == null || s.equals(ao.d())) {
                list.add(this.c.c(this.i(ao), s != null).continueWith(executor, (Continuation)new mm1(this)));
            }
        }
        return Tasks.whenAll((Collection)list);
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

public final class do1 implements vs1
{
    public static final a c;
    public final String a;
    public final Object[] b;
    
    static {
        c = new a(null);
    }
    
    public do1(final String s) {
        fg0.e((Object)s, "query");
        this(s, null);
    }
    
    public do1(final String a, final Object[] b) {
        fg0.e((Object)a, "query");
        this.a = a;
        this.b = b;
    }
    
    @Override
    public String a() {
        return this.a;
    }
    
    @Override
    public void b(final us1 us1) {
        fg0.e((Object)us1, "statement");
        do1.c.b(us1, this.b);
    }
    
    public static final class a
    {
        public final void a(final us1 us1, final int i, final Object obj) {
            if (obj == null) {
                us1.I(i);
            }
            else if (obj instanceof byte[]) {
                us1.D(i, (byte[])obj);
            }
            else {
                double doubleValue;
                if (obj instanceof Float) {
                    doubleValue = ((Number)obj).floatValue();
                }
                else {
                    if (!(obj instanceof Double)) {
                        long longValue = 0L;
                        Label_0099: {
                            if (obj instanceof Long) {
                                longValue = ((Number)obj).longValue();
                            }
                            else {
                                int n;
                                if (obj instanceof Integer) {
                                    n = ((Number)obj).intValue();
                                }
                                else if (obj instanceof Short) {
                                    n = ((Number)obj).shortValue();
                                }
                                else if (obj instanceof Byte) {
                                    n = ((Number)obj).byteValue();
                                }
                                else {
                                    if (obj instanceof String) {
                                        us1.y(i, (String)obj);
                                        return;
                                    }
                                    if (!(obj instanceof Boolean)) {
                                        final StringBuilder sb = new StringBuilder();
                                        sb.append("Cannot bind ");
                                        sb.append(obj);
                                        sb.append(" at index ");
                                        sb.append(i);
                                        sb.append(" Supported types: Null, ByteArray, Float, Double, Long, Int, Short, Byte, String");
                                        throw new IllegalArgumentException(sb.toString());
                                    }
                                    if (obj) {
                                        longValue = 1L;
                                        break Label_0099;
                                    }
                                    longValue = 0L;
                                    break Label_0099;
                                }
                                longValue = n;
                            }
                        }
                        us1.A(i, longValue);
                        return;
                    }
                    doubleValue = ((Number)obj).doubleValue();
                }
                us1.P(i, doubleValue);
            }
        }
        
        public final void b(final us1 us1, final Object[] array) {
            fg0.e((Object)us1, "statement");
            if (array == null) {
                return;
            }
            final int length = array.length;
            int i = 0;
            while (i < length) {
                final Object o = array[i];
                ++i;
                this.a(us1, i, o);
            }
        }
    }
}

import java.util.HashSet;
import java.util.Iterator;
import com.google.firebase.firestore.model.MutableDocument;
import java.util.HashMap;
import java.util.Set;
import java.util.Map;
import java.util.List;

// 
// Decompiled by Procyon v0.6.0
// 

public final class xx0
{
    public final int a;
    public final pw1 b;
    public final List c;
    public final List d;
    
    public xx0(final int a, final pw1 b, final List c, final List d) {
        g9.d(d.isEmpty() ^ true, "Cannot create an empty mutation batch", new Object[0]);
        this.a = a;
        this.b = b;
        this.c = c;
        this.d = d;
    }
    
    public Map a(final Map map, final Set set) {
        final HashMap hashMap = new HashMap();
        for (final du du : this.f()) {
            final MutableDocument mutableDocument = (MutableDocument)map.get(du).a();
            q00 b = this.b(mutableDocument, map.get(du).b());
            if (set.contains(du)) {
                b = null;
            }
            final wx0 c = wx0.c(mutableDocument, b);
            if (c != null) {
                hashMap.put(du, c);
            }
            if (!mutableDocument.o()) {
                mutableDocument.m(qo1.b);
            }
        }
        return hashMap;
    }
    
    public q00 b(final MutableDocument mutableDocument, q00 a) {
        final int n = 0;
        int n2 = 0;
        int i;
        q00 q00;
        while (true) {
            i = n;
            q00 = a;
            if (n2 >= this.c.size()) {
                break;
            }
            final wx0 wx0 = this.c.get(n2);
            q00 a2 = a;
            if (wx0.g().equals(mutableDocument.getKey())) {
                a2 = wx0.a(mutableDocument, a, this.b);
            }
            ++n2;
            a = a2;
        }
        while (i < this.d.size()) {
            final wx0 wx2 = this.d.get(i);
            a = q00;
            if (wx2.g().equals(mutableDocument.getKey())) {
                a = wx2.a(mutableDocument, q00, this.b);
            }
            ++i;
            q00 = a;
        }
        return q00;
    }
    
    public void c(final MutableDocument mutableDocument, final yx0 yx0) {
        final int size = this.d.size();
        final List e = yx0.e();
        final int size2 = e.size();
        int i = 0;
        g9.d(size2 == size, "Mismatch between mutations length (%d) and results length (%d)", size, e.size());
        while (i < size) {
            final wx0 wx0 = this.d.get(i);
            if (wx0.g().equals(mutableDocument.getKey())) {
                wx0.b(mutableDocument, e.get(i));
            }
            ++i;
        }
    }
    
    public List d() {
        return this.c;
    }
    
    public int e() {
        return this.a;
    }
    
    @Override
    public boolean equals(final Object o) {
        boolean b = true;
        if (this == o) {
            return true;
        }
        if (o != null && xx0.class == o.getClass()) {
            final xx0 xx0 = (xx0)o;
            if (this.a != xx0.a || !this.b.equals(xx0.b) || !this.c.equals(xx0.c) || !this.d.equals(xx0.d)) {
                b = false;
            }
            return b;
        }
        return false;
    }
    
    public Set f() {
        final HashSet set = new HashSet();
        final Iterator iterator = this.d.iterator();
        while (iterator.hasNext()) {
            set.add(((wx0)iterator.next()).g());
        }
        return set;
    }
    
    public pw1 g() {
        return this.b;
    }
    
    public List h() {
        return this.d;
    }
    
    @Override
    public int hashCode() {
        return ((this.a * 31 + this.b.hashCode()) * 31 + this.c.hashCode()) * 31 + this.d.hashCode();
    }
    
    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append("MutationBatch(batchId=");
        sb.append(this.a);
        sb.append(", localWriteTime=");
        sb.append(this.b);
        sb.append(", baseMutations=");
        sb.append(this.c);
        sb.append(", mutations=");
        sb.append(this.d);
        sb.append(')');
        return sb.toString();
    }
}

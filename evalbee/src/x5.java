import android.content.Context;
import android.util.AttributeSet;
import android.graphics.PorterDuff$Mode;
import android.content.res.ColorStateList;
import android.graphics.drawable.Drawable;
import android.view.View;

// 
// Decompiled by Procyon v0.6.0
// 

public class x5
{
    public final View a;
    public final s6 b;
    public int c;
    public rw1 d;
    public rw1 e;
    public rw1 f;
    
    public x5(final View a) {
        this.c = -1;
        this.a = a;
        this.b = s6.b();
    }
    
    public final boolean a(final Drawable drawable) {
        if (this.f == null) {
            this.f = new rw1();
        }
        final rw1 f = this.f;
        f.a();
        final ColorStateList s = o32.s(this.a);
        if (s != null) {
            f.d = true;
            f.a = s;
        }
        final PorterDuff$Mode t = o32.t(this.a);
        if (t != null) {
            f.c = true;
            f.b = t;
        }
        if (!f.d && !f.c) {
            return false;
        }
        s6.i(drawable, f, this.a.getDrawableState());
        return true;
    }
    
    public void b() {
        final Drawable background = this.a.getBackground();
        if (background != null) {
            if (this.k() && this.a(background)) {
                return;
            }
            final rw1 e = this.e;
            if (e != null) {
                s6.i(background, e, this.a.getDrawableState());
            }
            else {
                final rw1 d = this.d;
                if (d != null) {
                    s6.i(background, d, this.a.getDrawableState());
                }
            }
        }
    }
    
    public ColorStateList c() {
        final rw1 e = this.e;
        ColorStateList a;
        if (e != null) {
            a = e.a;
        }
        else {
            a = null;
        }
        return a;
    }
    
    public PorterDuff$Mode d() {
        final rw1 e = this.e;
        PorterDuff$Mode b;
        if (e != null) {
            b = e.b;
        }
        else {
            b = null;
        }
        return b;
    }
    
    public void e(final AttributeSet set, int n) {
        final Context context = this.a.getContext();
        final int[] s3 = bc1.S3;
        final tw1 v = tw1.v(context, set, s3, n, 0);
        final View a = this.a;
        o32.o0(a, a.getContext(), s3, set, v.r(), n, 0);
        try {
            n = bc1.T3;
            if (v.s(n)) {
                this.c = v.n(n, -1);
                final ColorStateList f = this.b.f(this.a.getContext(), this.c);
                if (f != null) {
                    this.h(f);
                }
            }
            n = bc1.U3;
            if (v.s(n)) {
                o32.v0(this.a, v.c(n));
            }
            n = bc1.V3;
            if (v.s(n)) {
                o32.w0(this.a, fv.e(v.k(n, -1), null));
            }
        }
        finally {
            v.w();
        }
    }
    
    public void f(final Drawable drawable) {
        this.c = -1;
        this.h(null);
        this.b();
    }
    
    public void g(final int c) {
        this.c = c;
        final s6 b = this.b;
        ColorStateList f;
        if (b != null) {
            f = b.f(this.a.getContext(), c);
        }
        else {
            f = null;
        }
        this.h(f);
        this.b();
    }
    
    public void h(final ColorStateList a) {
        if (a != null) {
            if (this.d == null) {
                this.d = new rw1();
            }
            final rw1 d = this.d;
            d.a = a;
            d.d = true;
        }
        else {
            this.d = null;
        }
        this.b();
    }
    
    public void i(final ColorStateList a) {
        if (this.e == null) {
            this.e = new rw1();
        }
        final rw1 e = this.e;
        e.a = a;
        e.d = true;
        this.b();
    }
    
    public void j(final PorterDuff$Mode b) {
        if (this.e == null) {
            this.e = new rw1();
        }
        final rw1 e = this.e;
        e.b = b;
        e.c = true;
        this.b();
    }
    
    public final boolean k() {
        return this.d != null;
    }
}

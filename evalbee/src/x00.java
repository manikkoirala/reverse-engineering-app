import java.io.OutputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.File;
import java.io.IOException;
import android.util.Log;
import android.text.TextUtils;
import com.google.firebase.storage.StorageException;
import com.google.android.gms.common.api.Status;
import android.net.Uri;

// 
// Decompiled by Procyon v0.6.0
// 

public class x00 extends zq1
{
    public final Uri l;
    public long m;
    public jq1 n;
    public mz o;
    public long p;
    public String q;
    public volatile Exception r;
    public long s;
    public int t;
    
    public x00(final jq1 n, final Uri l) {
        this.p = -1L;
        this.q = null;
        this.r = null;
        this.s = 0L;
        this.n = n;
        this.l = l;
        final o30 j = n.j();
        this.o = new mz(j.a().l(), j.c(), j.b(), j.l());
    }
    
    @Override
    public jq1 G() {
        return this.n;
    }
    
    @Override
    public void R() {
        this.o.a();
        this.r = StorageException.fromErrorStatus(Status.RESULT_CANCELED);
    }
    
    @Override
    public void b0() {
        if (this.r != null) {
            this.g0(64, false);
            return;
        }
        if (!this.g0(4, false)) {
            return;
        }
        do {
            this.m = 0L;
            this.r = null;
            this.o.c();
            final oa0 oa0 = new oa0(this.n.k(), this.n.e(), this.s);
            this.o.e(oa0, false);
            this.t = oa0.o();
            Exception r;
            if (oa0.f() != null) {
                r = oa0.f();
            }
            else {
                r = this.r;
            }
            this.r = r;
            final boolean j0 = this.j0(this.t);
            boolean b = true;
            boolean k0;
            final boolean b2 = k0 = (j0 && this.r == null && this.A() == 4);
            if (b2) {
                this.p = oa0.r() + this.s;
                final String q = oa0.q("ETag");
                if (!TextUtils.isEmpty((CharSequence)q)) {
                    final String q2 = this.q;
                    if (q2 != null && !q2.equals(q)) {
                        Log.w("FileDownloadTask", "The file at the server has changed.  Restarting from the beginning.");
                        this.s = 0L;
                        this.q = null;
                        oa0.C();
                        this.c0();
                        return;
                    }
                }
                this.q = q;
                try {
                    k0 = this.k0(oa0);
                }
                catch (final IOException r2) {
                    Log.e("FileDownloadTask", "Exception occurred during file write.  Aborting.", (Throwable)r2);
                    this.r = r2;
                    k0 = b2;
                }
            }
            oa0.C();
            if (!k0 || this.r != null || this.A() != 4) {
                b = false;
            }
            if (b) {
                this.g0(128, false);
                return;
            }
            final File file = new File(this.l.getPath());
            if (file.exists()) {
                this.s = file.length();
            }
            else {
                this.s = 0L;
            }
            if (this.A() == 8) {
                this.g0(16, false);
                return;
            }
            if (this.A() == 32) {
                if (!this.g0(256, false)) {
                    final StringBuilder sb = new StringBuilder();
                    sb.append("Unable to change download task to final state from ");
                    sb.append(this.A());
                    Log.w("FileDownloadTask", sb.toString());
                }
                return;
            }
        } while (this.m > 0L);
        this.g0(64, false);
    }
    
    @Override
    public void c0() {
        br1.a().f(this.D());
    }
    
    public final int i0(final InputStream inputStream, final byte[] b) {
        int i = 0;
        boolean b2 = false;
        try {
            while (i != b.length) {
                final int read = inputStream.read(b, i, b.length - i);
                if (read == -1) {
                    break;
                }
                i += read;
                b2 = true;
            }
        }
        catch (final IOException r) {
            this.r = r;
        }
        if (!b2) {
            i = -1;
        }
        return i;
    }
    
    public final boolean j0(final int n) {
        return n == 308 || (n >= 200 && n < 300);
    }
    
    public final boolean k0(xy0 o) {
        final InputStream t = ((xy0)o).t();
        boolean b = false;
        if (t != null) {
            final File file = new File(this.l.getPath());
            if (!file.exists()) {
                if (this.s > 0L) {
                    throw new IOException("The file to download to has been deleted.");
                }
                if (!file.createNewFile()) {
                    final StringBuilder sb = new StringBuilder();
                    sb.append("unable to create file:");
                    sb.append(file.getAbsolutePath());
                    Log.w("FileDownloadTask", sb.toString());
                }
            }
            final long s = this.s;
            b = true;
            if (s > 0L) {
                final StringBuilder sb2 = new StringBuilder();
                sb2.append("Resuming download file ");
                sb2.append(file.getAbsolutePath());
                sb2.append(" at ");
                sb2.append(this.s);
                Log.d("FileDownloadTask", sb2.toString());
                o = new FileOutputStream(file, true);
            }
            else {
                o = new FileOutputStream(file);
            }
            try {
                final byte[] b2 = new byte[262144];
                while (b) {
                    final int i0 = this.i0(t, b2);
                    if (i0 == -1) {
                        break;
                    }
                    ((OutputStream)o).write(b2, 0, i0);
                    this.m += i0;
                    if (this.r != null) {
                        Log.d("FileDownloadTask", "Exception occurred during file download. Retrying.", (Throwable)this.r);
                        this.r = null;
                        b = false;
                    }
                    if (this.g0(4, false)) {
                        continue;
                    }
                    b = false;
                }
                return b;
            }
            finally {
                ((OutputStream)o).flush();
                ((OutputStream)o).close();
                t.close();
            }
        }
        this.r = new IllegalStateException("Unable to open Firebase Storage stream.");
        return b;
    }
    
    public a l0() {
        return new a(StorageException.fromExceptionAndHttpCode(this.r, this.t), this.m + this.s);
    }
    
    public class a extends b
    {
        public final long c;
        public final x00 d;
        
        public a(final x00 d, final Exception ex, final long c) {
            this.d = d.super(ex);
            this.c = c;
        }
    }
}

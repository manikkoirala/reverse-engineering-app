import android.util.Log;
import com.google.android.datatransport.Event;
import com.google.android.datatransport.Transformer;
import com.google.android.datatransport.Encoding;
import com.google.android.datatransport.TransportFactory;

// 
// Decompiled by Procyon v0.6.0
// 

public final class ox implements px
{
    public static final a b;
    public final r91 a;
    
    static {
        b = new a(null);
    }
    
    public ox(final r91 a) {
        fg0.e((Object)a, "transportFactoryProvider");
        this.a = a;
    }
    
    @Override
    public void a(final xl1 xl1) {
        fg0.e((Object)xl1, "sessionEvent");
        ((TransportFactory)this.a.get()).getTransport("FIREBASE_APPQUALITY_SESSION", xl1.class, Encoding.of("json"), new nx(this)).send(Event.ofData(xl1));
    }
    
    public final byte[] c(final xl1 xl1) {
        final String encode = yl1.a.c().encode(xl1);
        fg0.d((Object)encode, "SessionEvents.SESSION_EVENT_ENCODER.encode(value)");
        final StringBuilder sb = new StringBuilder();
        sb.append("Session Event: ");
        sb.append(encode);
        Log.d("EventGDTLogger", sb.toString());
        final byte[] bytes = encode.getBytes(jg.b);
        fg0.d((Object)bytes, "this as java.lang.String).getBytes(charset)");
        return bytes;
    }
    
    public static final class a
    {
    }
}

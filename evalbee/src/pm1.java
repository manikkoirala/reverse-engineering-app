import java.util.Set;

// 
// Decompiled by Procyon v0.6.0
// 

public interface pm1 extends rx0
{
    Set entries();
    
    Set get(final Object p0);
    
    Set removeAll(final Object p0);
    
    Set replaceValues(final Object p0, final Iterable p1);
}

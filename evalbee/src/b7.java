import android.view.View;
import android.util.AttributeSet;
import android.content.Context;
import android.widget.PopupWindow;

// 
// Decompiled by Procyon v0.6.0
// 

public class b7 extends PopupWindow
{
    public static final boolean b;
    public boolean a;
    
    static {
        b = false;
    }
    
    public b7(final Context context, final AttributeSet set, final int n, final int n2) {
        super(context, set, n, n2);
        this.a(context, set, n, n2);
    }
    
    public final void a(final Context context, final AttributeSet set, int i2, final int n) {
        final tw1 v = tw1.v(context, set, bc1.g2, i2, n);
        i2 = bc1.i2;
        if (v.s(i2)) {
            this.b(v.a(i2, false));
        }
        this.setBackgroundDrawable(v.g(bc1.h2));
        v.w();
    }
    
    public final void b(final boolean a) {
        if (b7.b) {
            this.a = a;
        }
        else {
            c61.a(this, a);
        }
    }
    
    public void showAsDropDown(final View view, final int n, final int n2) {
        int n3 = n2;
        if (b7.b) {
            n3 = n2;
            if (this.a) {
                n3 = n2 - view.getHeight();
            }
        }
        super.showAsDropDown(view, n, n3);
    }
    
    public void showAsDropDown(final View view, final int n, final int n2, final int n3) {
        int n4 = n2;
        if (b7.b) {
            n4 = n2;
            if (this.a) {
                n4 = n2 - view.getHeight();
            }
        }
        super.showAsDropDown(view, n, n4, n3);
    }
    
    public void update(final View view, final int n, final int n2, final int n3, final int n4) {
        int n5 = n2;
        if (b7.b) {
            n5 = n2;
            if (this.a) {
                n5 = n2 - view.getHeight();
            }
        }
        super.update(view, n, n5, n3, n4);
    }
}

import com.google.gson.stream.JsonToken;
import com.google.gson.JsonSyntaxException;
import com.google.gson.reflect.TypeToken;
import com.google.gson.ToNumberPolicy;

// 
// Decompiled by Procyon v0.6.0
// 

public final class o01 extends hz1
{
    public static final iz1 b;
    public final vw1 a;
    
    static {
        b = f(ToNumberPolicy.LAZILY_PARSED_NUMBER);
    }
    
    public o01(final vw1 a) {
        this.a = a;
    }
    
    public static iz1 e(final vw1 vw1) {
        if (vw1 == ToNumberPolicy.LAZILY_PARSED_NUMBER) {
            return o01.b;
        }
        return f(vw1);
    }
    
    public static iz1 f(final vw1 vw1) {
        return new iz1(new o01(vw1)) {
            public final o01 a;
            
            @Override
            public hz1 a(final gc0 gc0, final TypeToken typeToken) {
                o01 a;
                if (typeToken.getRawType() == Number.class) {
                    a = this.a;
                }
                else {
                    a = null;
                }
                return a;
            }
        };
    }
    
    public Number g(final rh0 rh0) {
        final JsonToken o0 = rh0.o0();
        final int n = o01$b.a[o0.ordinal()];
        if (n == 1) {
            rh0.R();
            return null;
        }
        if (n != 2 && n != 3) {
            final StringBuilder sb = new StringBuilder();
            sb.append("Expecting number, got: ");
            sb.append(o0);
            sb.append("; at path ");
            sb.append(rh0.getPath());
            throw new JsonSyntaxException(sb.toString());
        }
        return this.a.readNumber(rh0);
    }
    
    public void h(final vh0 vh0, final Number n) {
        vh0.q0(n);
    }
}

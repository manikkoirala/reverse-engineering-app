import java.util.Iterator;
import java.util.ArrayList;
import java.util.List;
import com.google.android.gms.internal.firebase_auth_api.zzagr;
import com.google.android.gms.common.internal.Preconditions;
import android.text.TextUtils;
import com.google.android.gms.internal.firebase-auth-api.zzafr;

// 
// Decompiled by Procyon v0.6.0
// 

public abstract class wc2
{
    public static gx0 a(final zzafr zzafr) {
        gx0 gx0 = null;
        if (zzafr == null) {
            return null;
        }
        if (!TextUtils.isEmpty((CharSequence)((com.google.android.gms.internal.firebase_auth_api.zzafr)zzafr).zze())) {
            return new i51(((com.google.android.gms.internal.firebase_auth_api.zzafr)zzafr).zzd(), ((com.google.android.gms.internal.firebase_auth_api.zzafr)zzafr).zzc(), ((com.google.android.gms.internal.firebase_auth_api.zzafr)zzafr).zza(), Preconditions.checkNotEmpty(((com.google.android.gms.internal.firebase_auth_api.zzafr)zzafr).zze()));
        }
        if (((com.google.android.gms.internal.firebase_auth_api.zzafr)zzafr).zzb() != null) {
            gx0 = new fy1(((com.google.android.gms.internal.firebase_auth_api.zzafr)zzafr).zzd(), ((com.google.android.gms.internal.firebase_auth_api.zzafr)zzafr).zzc(), ((com.google.android.gms.internal.firebase_auth_api.zzafr)zzafr).zza(), (com.google.android.gms.internal.firebase-auth-api.zzagr)Preconditions.checkNotNull((zzagr)((com.google.android.gms.internal.firebase_auth_api.zzafr)zzafr).zzb(), "totpInfo cannot be null."));
        }
        return gx0;
    }
    
    public static List b(final List list) {
        if (list != null && !list.isEmpty()) {
            final ArrayList list2 = new ArrayList();
            final Iterator iterator = list.iterator();
            while (iterator.hasNext()) {
                final gx0 a = a((zzafr)iterator.next());
                if (a != null) {
                    list2.add(a);
                }
            }
            return list2;
        }
        return new ArrayList();
    }
}

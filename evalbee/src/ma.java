// 
// Decompiled by Procyon v0.6.0
// 

public final class ma extends pf0
{
    public final String a;
    public final long b;
    public final long c;
    
    public ma(final String a, final long b, final long c) {
        this.a = a;
        this.b = b;
        this.c = c;
    }
    
    @Override
    public String b() {
        return this.a;
    }
    
    @Override
    public long c() {
        return this.c;
    }
    
    @Override
    public long d() {
        return this.b;
    }
    
    @Override
    public boolean equals(final Object o) {
        boolean b = true;
        if (o == this) {
            return true;
        }
        if (o instanceof pf0) {
            final pf0 pf0 = (pf0)o;
            if (!this.a.equals(pf0.b()) || this.b != pf0.d() || this.c != pf0.c()) {
                b = false;
            }
            return b;
        }
        return false;
    }
    
    @Override
    public int hashCode() {
        final int hashCode = this.a.hashCode();
        final long b = this.b;
        final int n = (int)(b ^ b >>> 32);
        final long c = this.c;
        return ((hashCode ^ 0xF4243) * 1000003 ^ n) * 1000003 ^ (int)(c ^ c >>> 32);
    }
    
    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append("InstallationTokenResult{token=");
        sb.append(this.a);
        sb.append(", tokenExpirationTimestamp=");
        sb.append(this.b);
        sb.append(", tokenCreationTimestamp=");
        sb.append(this.c);
        sb.append("}");
        return sb.toString();
    }
    
    public static final class b extends a
    {
        public String a;
        public Long b;
        public Long c;
        
        @Override
        public pf0 a() {
            final String a = this.a;
            String string = "";
            if (a == null) {
                final StringBuilder sb = new StringBuilder();
                sb.append("");
                sb.append(" token");
                string = sb.toString();
            }
            String string2 = string;
            if (this.b == null) {
                final StringBuilder sb2 = new StringBuilder();
                sb2.append(string);
                sb2.append(" tokenExpirationTimestamp");
                string2 = sb2.toString();
            }
            String string3 = string2;
            if (this.c == null) {
                final StringBuilder sb3 = new StringBuilder();
                sb3.append(string2);
                sb3.append(" tokenCreationTimestamp");
                string3 = sb3.toString();
            }
            if (string3.isEmpty()) {
                return new ma(this.a, this.b, this.c, null);
            }
            final StringBuilder sb4 = new StringBuilder();
            sb4.append("Missing required properties:");
            sb4.append(string3);
            throw new IllegalStateException(sb4.toString());
        }
        
        @Override
        public a b(final String a) {
            if (a != null) {
                this.a = a;
                return this;
            }
            throw new NullPointerException("Null token");
        }
        
        @Override
        public a c(final long l) {
            this.c = l;
            return this;
        }
        
        @Override
        public a d(final long l) {
            this.b = l;
            return this;
        }
    }
}

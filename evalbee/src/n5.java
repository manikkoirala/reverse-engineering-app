import org.jetbrains.annotations.NotNull;
import android.window.BackEvent;

// 
// Decompiled by Procyon v0.6.0
// 

public final class n5
{
    public static final n5 a;
    
    static {
        a = new n5();
    }
    
    @NotNull
    public final BackEvent a(final float n, final float n2, final float n3, final int n4) {
        return new BackEvent(n, n2, n3, n4);
    }
    
    public final float b(@NotNull final BackEvent backEvent) {
        fg0.e((Object)backEvent, "backEvent");
        return backEvent.getProgress();
    }
    
    public final int c(@NotNull final BackEvent backEvent) {
        fg0.e((Object)backEvent, "backEvent");
        return backEvent.getSwipeEdge();
    }
    
    public final float d(@NotNull final BackEvent backEvent) {
        fg0.e((Object)backEvent, "backEvent");
        return backEvent.getTouchX();
    }
    
    public final float e(@NotNull final BackEvent backEvent) {
        fg0.e((Object)backEvent, "backEvent");
        return backEvent.getTouchY();
    }
}

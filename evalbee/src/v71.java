import androidx.datastore.preferences.protobuf.InvalidProtocolBufferException;
import androidx.datastore.core.CorruptionException;
import java.io.InputStream;

// 
// Decompiled by Procyon v0.6.0
// 

public abstract class v71
{
    public static final a a;
    
    static {
        a = new a(null);
    }
    
    public static final class a
    {
        public final x71 a(final InputStream inputStream) {
            fg0.e((Object)inputStream, "input");
            try {
                final x71 q = x71.Q(inputStream);
                fg0.d((Object)q, "{\n                PreferencesProto.PreferenceMap.parseFrom(input)\n            }");
                return q;
            }
            catch (final InvalidProtocolBufferException ex) {
                throw new CorruptionException("Unable to parse preferences proto.", ex);
            }
        }
    }
}

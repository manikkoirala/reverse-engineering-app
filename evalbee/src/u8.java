import com.google.protobuf.GeneratedMessageLite;
import java.util.Iterator;
import com.google.firestore.v1.a;
import com.google.firestore.v1.Value;
import java.util.Collections;
import java.util.List;

// 
// Decompiled by Procyon v0.6.0
// 

public abstract class u8 implements oy1
{
    public final List a;
    
    public u8(final List list) {
        this.a = Collections.unmodifiableList((List<?>)list);
    }
    
    public static com.google.firestore.v1.a.b e(final Value value) {
        if (a32.t(value)) {
            return (com.google.firestore.v1.a.b)value.l0().Y();
        }
        return com.google.firestore.v1.a.j0();
    }
    
    @Override
    public Value a(final Value value) {
        return null;
    }
    
    @Override
    public Value b(final Value value, final pw1 pw1) {
        return this.d(value);
    }
    
    @Override
    public Value c(final Value value, final Value value2) {
        return this.d(value);
    }
    
    public abstract Value d(final Value p0);
    
    @Override
    public boolean equals(final Object o) {
        return this == o || (o != null && this.getClass() == o.getClass() && this.a.equals(((u8)o).a));
    }
    
    public List f() {
        return this.a;
    }
    
    @Override
    public int hashCode() {
        return this.getClass().hashCode() * 31 + this.a.hashCode();
    }
    
    public static class a extends u8
    {
        public a(final List list) {
            super(list);
        }
        
        @Override
        public Value d(final Value value) {
            final com.google.firestore.v1.a.b e = u8.e(value);
            for (final Value value2 : this.f()) {
                int i = 0;
                while (i < e.D()) {
                    if (a32.q(e.C(i), value2)) {
                        e.E(i);
                    }
                    else {
                        ++i;
                    }
                }
            }
            return (Value)((GeneratedMessageLite.a)Value.x0().A(e)).p();
        }
    }
    
    public static class b extends u8
    {
        public b(final List list) {
            super(list);
        }
        
        @Override
        public Value d(final Value value) {
            final com.google.firestore.v1.a.b e = u8.e(value);
            for (final Value value2 : this.f()) {
                if (!a32.p(e, value2)) {
                    e.B(value2);
                }
            }
            return (Value)((GeneratedMessageLite.a)Value.x0().A(e)).p();
        }
    }
}

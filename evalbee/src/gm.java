// 
// Decompiled by Procyon v0.6.0
// 

public abstract class gm
{
    public static final String a = "_COROUTINE";
    
    public static final StackTraceElement b(final Throwable t, final String str) {
        final StackTraceElement stackTraceElement = t.getStackTrace()[0];
        final StringBuilder sb = new StringBuilder();
        sb.append(gm.a);
        sb.append('.');
        sb.append(str);
        return new StackTraceElement(sb.toString(), "_", stackTraceElement.getFileName(), stackTraceElement.getLineNumber());
    }
}

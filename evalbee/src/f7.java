import android.text.Selection;
import android.text.Spannable;
import android.content.Context;
import android.content.ContextWrapper;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.app.Activity;
import android.widget.TextView;
import android.util.Log;
import android.os.Build$VERSION;
import android.view.DragEvent;
import android.view.View;

// 
// Decompiled by Procyon v0.6.0
// 

public abstract class f7
{
    public static boolean a(final View obj, final DragEvent dragEvent) {
        if (Build$VERSION.SDK_INT < 31 && dragEvent.getLocalState() == null) {
            if (o32.E(obj) != null) {
                final Activity c = c(obj);
                if (c == null) {
                    final StringBuilder sb = new StringBuilder();
                    sb.append("Can't handle drop: no activity: view=");
                    sb.append(obj);
                    Log.i("ReceiveContent", sb.toString());
                    return false;
                }
                if (dragEvent.getAction() == 1) {
                    return obj instanceof TextView ^ true;
                }
                if (dragEvent.getAction() == 3) {
                    boolean b;
                    if (obj instanceof TextView) {
                        b = a.a(dragEvent, (TextView)obj, c);
                    }
                    else {
                        b = a.b(dragEvent, obj, c);
                    }
                    return b;
                }
            }
        }
        return false;
    }
    
    public static boolean b(final TextView textView, int n) {
        final int sdk_INT = Build$VERSION.SDK_INT;
        final int n2 = 0;
        if (sdk_INT < 31 && o32.E((View)textView) != null && (n == 16908322 || n == 16908337)) {
            final ClipboardManager clipboardManager = (ClipboardManager)((View)textView).getContext().getSystemService("clipboard");
            ClipData primaryClip;
            if (clipboardManager == null) {
                primaryClip = null;
            }
            else {
                primaryClip = clipboardManager.getPrimaryClip();
            }
            if (primaryClip != null && primaryClip.getItemCount() > 0) {
                final gl.a a = new gl.a(primaryClip, 1);
                if (n == 16908322) {
                    n = n2;
                }
                else {
                    n = 1;
                }
                o32.g0((View)textView, a.c(n).a());
            }
            return true;
        }
        return false;
    }
    
    public static Activity c(final View view) {
        for (Context context = view.getContext(); context instanceof ContextWrapper; context = ((ContextWrapper)context).getBaseContext()) {
            if (context instanceof Activity) {
                return (Activity)context;
            }
        }
        return null;
    }
    
    public abstract static final class a
    {
        public static boolean a(final DragEvent dragEvent, final TextView textView, final Activity activity) {
            activity.requestDragAndDropPermissions(dragEvent);
            final int offsetForPosition = textView.getOffsetForPosition(dragEvent.getX(), dragEvent.getY());
            textView.beginBatchEdit();
            try {
                Selection.setSelection((Spannable)textView.getText(), offsetForPosition);
                o32.g0((View)textView, new gl.a(dragEvent.getClipData(), 3).a());
                return true;
            }
            finally {
                textView.endBatchEdit();
            }
        }
        
        public static boolean b(final DragEvent dragEvent, final View view, final Activity activity) {
            activity.requestDragAndDropPermissions(dragEvent);
            o32.g0(view, new gl.a(dragEvent.getClipData(), 3).a());
            return true;
        }
    }
}

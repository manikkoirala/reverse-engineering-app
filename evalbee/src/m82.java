import kotlin.Pair;
import kotlin.collections.b;
import java.util.LinkedHashMap;
import java.util.Iterator;
import java.io.File;
import java.util.Map;
import android.content.Context;

// 
// Decompiled by Procyon v0.6.0
// 

public final class m82
{
    public static final m82 a;
    
    static {
        a = new m82();
    }
    
    public static final void d(final Context context) {
        fg0.e((Object)context, "context");
        final m82 a = m82.a;
        if (a.b(context).exists()) {
            xl0.e().a(n82.b(), "Migrating WorkDatabase to the no-backup directory");
            for (final Map.Entry<File, V> entry : a.e(context).entrySet()) {
                final File file = entry.getKey();
                final File file2 = (File)entry.getValue();
                if (file.exists()) {
                    if (file2.exists()) {
                        final xl0 e = xl0.e();
                        final String b = n82.b();
                        final StringBuilder sb = new StringBuilder();
                        sb.append("Over-writing contents of ");
                        sb.append(file2);
                        e.k(b, sb.toString());
                    }
                    StringBuilder sb2;
                    if (file.renameTo(file2)) {
                        sb2 = new StringBuilder();
                        sb2.append("Migrated ");
                        sb2.append(file);
                        sb2.append("to ");
                        sb2.append(file2);
                    }
                    else {
                        sb2 = new StringBuilder();
                        sb2.append("Renaming ");
                        sb2.append(file);
                        sb2.append(" to ");
                        sb2.append(file2);
                        sb2.append(" failed");
                    }
                    xl0.e().a(n82.b(), sb2.toString());
                }
            }
        }
    }
    
    public final File a(final Context context) {
        fg0.e((Object)context, "context");
        return this.c(context);
    }
    
    public final File b(final Context context) {
        fg0.e((Object)context, "context");
        final File databasePath = context.getDatabasePath("androidx.work.workdb");
        fg0.d((Object)databasePath, "context.getDatabasePath(WORK_DATABASE_NAME)");
        return databasePath;
    }
    
    public final File c(final Context context) {
        return new File(l5.a.a(context), "androidx.work.workdb");
    }
    
    public final Map e(final Context context) {
        fg0.e((Object)context, "context");
        final File b = this.b(context);
        final File a = this.a(context);
        final String[] a2 = n82.a();
        final LinkedHashMap linkedHashMap = new LinkedHashMap(ic1.a(en0.e(a2.length), 16));
        for (final String s : a2) {
            final StringBuilder sb = new StringBuilder();
            sb.append(b.getPath());
            sb.append(s);
            final File file = new File(sb.toString());
            final StringBuilder sb2 = new StringBuilder();
            sb2.append(a.getPath());
            sb2.append(s);
            final Pair a3 = dz1.a((Object)file, (Object)new File(sb2.toString()));
            linkedHashMap.put(a3.getFirst(), a3.getSecond());
        }
        return kotlin.collections.b.m((Map)linkedHashMap, dz1.a((Object)b, (Object)a));
    }
}

import android.widget.Adapter;
import android.widget.AdapterView;
import android.widget.PopupWindow$OnDismissListener;
import androidx.appcompat.view.menu.g;
import android.widget.HeaderViewListAdapter;
import androidx.appcompat.view.menu.d;
import android.view.MenuItem;
import androidx.appcompat.view.menu.e;
import android.view.View;
import android.widget.FrameLayout;
import android.view.View$MeasureSpec;
import android.content.Context;
import android.view.ViewGroup;
import android.widget.ListAdapter;
import android.graphics.Rect;
import android.widget.AdapterView$OnItemClickListener;
import androidx.appcompat.view.menu.i;

// 
// Decompiled by Procyon v0.6.0
// 

public abstract class pv0 implements wn1, i, AdapterView$OnItemClickListener
{
    public Rect a;
    
    public static int e(final ListAdapter listAdapter, final ViewGroup viewGroup, final Context context, final int n) {
        int i = 0;
        final int measureSpec = View$MeasureSpec.makeMeasureSpec(0, 0);
        final int measureSpec2 = View$MeasureSpec.makeMeasureSpec(0, 0);
        final int count = ((Adapter)listAdapter).getCount();
        int n2 = 0;
        int n3 = 0;
        final View view = null;
        ViewGroup viewGroup2 = viewGroup;
        View view2 = view;
        while (i < count) {
            final int itemViewType = ((Adapter)listAdapter).getItemViewType(i);
            int n4;
            if (itemViewType != (n4 = n3)) {
                view2 = null;
                n4 = itemViewType;
            }
            Object o;
            if ((o = viewGroup2) == null) {
                o = new FrameLayout(context);
            }
            view2 = ((Adapter)listAdapter).getView(i, view2, (ViewGroup)o);
            view2.measure(measureSpec, measureSpec2);
            final int measuredWidth = view2.getMeasuredWidth();
            if (measuredWidth >= n) {
                return n;
            }
            int n5;
            if (measuredWidth > (n5 = n2)) {
                n5 = measuredWidth;
            }
            ++i;
            n2 = n5;
            n3 = n4;
            viewGroup2 = (ViewGroup)o;
        }
        return n2;
    }
    
    public static boolean o(final e e) {
        final int size = e.size();
        final boolean b = false;
        int n = 0;
        boolean b2;
        while (true) {
            b2 = b;
            if (n >= size) {
                break;
            }
            final MenuItem item = e.getItem(n);
            if (item.isVisible() && item.getIcon() != null) {
                b2 = true;
                break;
            }
            ++n;
        }
        return b2;
    }
    
    public static d p(final ListAdapter listAdapter) {
        if (listAdapter instanceof HeaderViewListAdapter) {
            return (d)((HeaderViewListAdapter)listAdapter).getWrappedAdapter();
        }
        return (d)listAdapter;
    }
    
    public abstract void b(final e p0);
    
    public boolean c() {
        return true;
    }
    
    @Override
    public boolean collapseItemActionView(final e e, final g g) {
        return false;
    }
    
    public Rect d() {
        return this.a;
    }
    
    @Override
    public boolean expandItemActionView(final e e, final g g) {
        return false;
    }
    
    public abstract void f(final View p0);
    
    public void g(final Rect a) {
        this.a = a;
    }
    
    @Override
    public int getId() {
        return 0;
    }
    
    public abstract void i(final boolean p0);
    
    @Override
    public void initForMenu(final Context context, final e e) {
    }
    
    public abstract void j(final int p0);
    
    public abstract void k(final int p0);
    
    public abstract void l(final PopupWindow$OnDismissListener p0);
    
    public abstract void m(final boolean p0);
    
    public abstract void n(final int p0);
    
    public void onItemClick(final AdapterView adapterView, final View view, int n, final long n2) {
        final ListAdapter listAdapter = (ListAdapter)adapterView.getAdapter();
        final e a = p(listAdapter).a;
        final MenuItem menuItem = (MenuItem)((Adapter)listAdapter).getItem(n);
        if (this.c()) {
            n = 0;
        }
        else {
            n = 4;
        }
        a.performItemAction(menuItem, this, n);
    }
}

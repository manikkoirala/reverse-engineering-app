import android.content.Context;

// 
// Decompiled by Procyon v0.6.0
// 

public class qf0
{
    public String a;
    
    public static String b(final Context context) {
        String installerPackageName;
        if ((installerPackageName = context.getPackageManager().getInstallerPackageName(context.getPackageName())) == null) {
            installerPackageName = "";
        }
        return installerPackageName;
    }
    
    public String a(final Context context) {
        synchronized (this) {
            if (this.a == null) {
                this.a = b(context);
            }
            String a;
            if ("".equals(this.a)) {
                a = null;
            }
            else {
                a = this.a;
            }
            return a;
        }
    }
}

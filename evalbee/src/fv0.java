import java.util.Iterator;
import com.google.firebase.firestore.remote.j;
import java.util.Collections;
import java.util.ArrayList;
import com.google.firebase.firestore.local.e;
import com.google.protobuf.ByteString;
import com.google.firebase.database.collection.c;
import java.util.List;

// 
// Decompiled by Procyon v0.6.0
// 

public final class fv0 implements zx0
{
    public final List a;
    public c b;
    public int c;
    public ByteString d;
    public final e e;
    public final com.google.firebase.firestore.local.c f;
    
    public fv0(final e e, final u12 u12) {
        this.e = e;
        this.a = new ArrayList();
        this.b = new c(Collections.emptyList(), ku.c);
        this.c = 1;
        this.d = j.v;
        this.f = e.o(u12);
    }
    
    @Override
    public void a() {
        if (this.a.isEmpty()) {
            g9.d(this.b.isEmpty(), "Document leak -- detected dangling mutation references when queue is empty.", new Object[0]);
        }
    }
    
    @Override
    public List b(final Iterable iterable) {
        c c = new c(Collections.emptyList(), o22.f());
    Label_0021:
        for (final du du : iterable) {
            final Iterator g = this.b.g(new ku(du, 0));
            c c2 = c;
            while (true) {
                c = c2;
                if (!g.hasNext()) {
                    continue Label_0021;
                }
                final ku ku = g.next();
                if (!du.equals(ku.d())) {
                    break;
                }
                c2 = c2.c(ku.c());
            }
            c = c2;
        }
        return this.p(c);
    }
    
    @Override
    public void c(final xx0 xx0) {
        g9.d(this.n(xx0.e(), "removed") == 0, "Can only remove the first entry of the mutation queue", new Object[0]);
        this.a.remove(0);
        c b = this.b;
        final Iterator iterator = xx0.h().iterator();
        while (iterator.hasNext()) {
            final du g = ((wx0)iterator.next()).g();
            this.e.f().d(g);
            b = b.i(new ku(g, xx0.e()));
        }
        this.b = b;
    }
    
    @Override
    public xx0 d(final pw1 pw1, final List list, final List list2) {
        final boolean empty = list2.isEmpty();
        boolean b = true;
        g9.d(empty ^ true, "Mutation batches should not be empty", new Object[0]);
        final int n = this.c++;
        final int size = this.a.size();
        if (size > 0) {
            if (((xx0)this.a.get(size - 1)).e() >= n) {
                b = false;
            }
            g9.d(b, "Mutation batchIds must be monotonically increasing order", new Object[0]);
        }
        final xx0 xx0 = new xx0(n, pw1, list, list2);
        this.a.add(xx0);
        for (final wx0 wx0 : list2) {
            this.b = this.b.c(new ku(wx0.g(), n));
            this.f.a(wx0.g().k());
        }
        return xx0;
    }
    
    @Override
    public xx0 e(int m) {
        if ((m = this.m(m + 1)) < 0) {
            m = 0;
        }
        xx0 xx0;
        if (this.a.size() > m) {
            xx0 = this.a.get(m);
        }
        else {
            xx0 = null;
        }
        return xx0;
    }
    
    @Override
    public xx0 f(final int n) {
        final int m = this.m(n);
        if (m >= 0 && m < this.a.size()) {
            final xx0 xx0 = this.a.get(m);
            g9.d(xx0.e() == n, "If found batch must match", new Object[0]);
            return xx0;
        }
        return null;
    }
    
    @Override
    public ByteString g() {
        return this.d;
    }
    
    @Override
    public void h(xx0 xx0, final ByteString byteString) {
        final int e = xx0.e();
        final int n = this.n(e, "acknowledged");
        final boolean b = true;
        g9.d(n == 0, "Can only acknowledge the first batch in the mutation queue", new Object[0]);
        xx0 = (xx0)this.a.get(n);
        g9.d(e == xx0.e() && b, "Queue ordering failure: expected batch %d, got batch %d", e, xx0.e());
        this.d = (ByteString)k71.b(byteString);
    }
    
    @Override
    public void i(final ByteString byteString) {
        this.d = (ByteString)k71.b(byteString);
    }
    
    @Override
    public List j() {
        return Collections.unmodifiableList((List<?>)this.a);
    }
    
    public boolean k(final du du) {
        final Iterator g = this.b.g(new ku(du, 0));
        return g.hasNext() && ((ku)g.next()).d().equals(du);
    }
    
    public long l(final zk0 zk0) {
        final Iterator iterator = this.a.iterator();
        long n = 0L;
        while (iterator.hasNext()) {
            n += zk0.m((xx0)iterator.next()).b();
        }
        return n;
    }
    
    public final int m(final int n) {
        if (this.a.isEmpty()) {
            return 0;
        }
        return n - this.a.get(0).e();
    }
    
    public final int n(int m, final String s) {
        m = this.m(m);
        g9.d(m >= 0 && m < this.a.size(), "Batches must exist to be %s", s);
        return m;
    }
    
    public boolean o() {
        return this.a.isEmpty();
    }
    
    public final List p(final c c) {
        final ArrayList list = new ArrayList();
        final Iterator iterator = c.iterator();
        while (iterator.hasNext()) {
            final xx0 f = this.f(iterator.next());
            if (f != null) {
                list.add(f);
            }
        }
        return list;
    }
    
    @Override
    public void start() {
        if (this.o()) {
            this.c = 1;
        }
    }
}

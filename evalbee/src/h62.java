import android.animation.TimeInterpolator;
import java.util.HashMap;
import java.util.ArrayList;
import android.view.WindowInsetsAnimation$Callback;
import android.animation.Animator$AnimatorListener;
import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import java.util.Collections;
import android.animation.ValueAnimator$AnimatorUpdateListener;
import android.animation.ValueAnimator;
import android.view.animation.DecelerateInterpolator;
import java.util.Objects;
import android.view.ViewGroup;
import android.view.View$OnApplyWindowInsetsListener;
import java.util.List;
import android.view.WindowInsets;
import android.view.WindowInsetsAnimation$Bounds;
import android.view.View;
import android.view.WindowInsetsAnimation;
import android.os.Build$VERSION;
import android.view.animation.Interpolator;

// 
// Decompiled by Procyon v0.6.0
// 

public final class h62
{
    public e a;
    
    public h62(final int n, final Interpolator interpolator, final long n2) {
        e a;
        if (Build$VERSION.SDK_INT >= 30) {
            a = new d(n, interpolator, n2);
        }
        else {
            a = new c(n, interpolator, n2);
        }
        this.a = a;
    }
    
    public h62(final WindowInsetsAnimation windowInsetsAnimation) {
        this(0, null, 0L);
        if (Build$VERSION.SDK_INT >= 30) {
            this.a = (e)new d(windowInsetsAnimation);
        }
    }
    
    public static void d(final View view, final b b) {
        if (Build$VERSION.SDK_INT >= 30) {
            d.h(view, b);
        }
        else {
            c.o(view, b);
        }
    }
    
    public static h62 f(final WindowInsetsAnimation windowInsetsAnimation) {
        return new h62(windowInsetsAnimation);
    }
    
    public long a() {
        return this.a.a();
    }
    
    public float b() {
        return this.a.b();
    }
    
    public int c() {
        return this.a.c();
    }
    
    public void e(final float n) {
        this.a.d(n);
    }
    
    public static final class a
    {
        public final nf0 a;
        public final nf0 b;
        
        public a(final WindowInsetsAnimation$Bounds windowInsetsAnimation$Bounds) {
            this.a = d.g(windowInsetsAnimation$Bounds);
            this.b = d.f(windowInsetsAnimation$Bounds);
        }
        
        public a(final nf0 a, final nf0 b) {
            this.a = a;
            this.b = b;
        }
        
        public static a d(final WindowInsetsAnimation$Bounds windowInsetsAnimation$Bounds) {
            return new a(windowInsetsAnimation$Bounds);
        }
        
        public nf0 a() {
            return this.a;
        }
        
        public nf0 b() {
            return this.b;
        }
        
        public WindowInsetsAnimation$Bounds c() {
            return d.e(this);
        }
        
        @Override
        public String toString() {
            final StringBuilder sb = new StringBuilder();
            sb.append("Bounds{lower=");
            sb.append(this.a);
            sb.append(" upper=");
            sb.append(this.b);
            sb.append("}");
            return sb.toString();
        }
    }
    
    public abstract static class b
    {
        public static final int DISPATCH_MODE_CONTINUE_ON_SUBTREE = 1;
        public static final int DISPATCH_MODE_STOP = 0;
        WindowInsets mDispachedInsets;
        private final int mDispatchMode;
        
        public b(final int mDispatchMode) {
            this.mDispatchMode = mDispatchMode;
        }
        
        public final int getDispatchMode() {
            return this.mDispatchMode;
        }
        
        public abstract void onEnd(final h62 p0);
        
        public abstract void onPrepare(final h62 p0);
        
        public abstract u62 onProgress(final u62 p0, final List p1);
        
        public abstract h62.a onStart(final h62 p0, final h62.a p1);
    }
    
    public static class c extends e
    {
        public c(final int n, final Interpolator interpolator, final long n2) {
            super(n, interpolator, n2);
        }
        
        public static int e(final u62 u62, final u62 u63) {
            int n = 0;
            int n2;
            for (int i = 1; i <= 256; i <<= 1, n = n2) {
                n2 = n;
                if (!u62.f(i).equals(u63.f(i))) {
                    n2 = (n | i);
                }
            }
            return n;
        }
        
        public static h62.a f(final u62 u62, final u62 u63, final int n) {
            final nf0 f = u62.f(n);
            final nf0 f2 = u63.f(n);
            return new h62.a(nf0.b(Math.min(f.a, f2.a), Math.min(f.b, f2.b), Math.min(f.c, f2.c), Math.min(f.d, f2.d)), nf0.b(Math.max(f.a, f2.a), Math.max(f.b, f2.b), Math.max(f.c, f2.c), Math.max(f.d, f2.d)));
        }
        
        public static View$OnApplyWindowInsetsListener g(final View view, final b b) {
            return (View$OnApplyWindowInsetsListener)new a(view, b);
        }
        
        public static void h(final View view, final h62 h62) {
            final b m = m(view);
            if (m != null) {
                m.onEnd(h62);
                if (m.getDispatchMode() == 0) {
                    return;
                }
            }
            if (view instanceof ViewGroup) {
                final ViewGroup viewGroup = (ViewGroup)view;
                for (int i = 0; i < viewGroup.getChildCount(); ++i) {
                    h(viewGroup.getChildAt(i), h62);
                }
            }
        }
        
        public static void i(final View view, final h62 h62, final WindowInsets mDispachedInsets, final boolean b) {
            final b m = m(view);
            int i = 0;
            boolean b2 = b;
            if (m != null) {
                m.mDispachedInsets = mDispachedInsets;
                if (!(b2 = b)) {
                    m.onPrepare(h62);
                    b2 = (m.getDispatchMode() == 0);
                }
            }
            if (view instanceof ViewGroup) {
                for (ViewGroup viewGroup = (ViewGroup)view; i < viewGroup.getChildCount(); ++i) {
                    i(viewGroup.getChildAt(i), h62, mDispachedInsets, b2);
                }
            }
        }
        
        public static void j(final View view, final u62 u62, final List list) {
            final b m = m(view);
            u62 onProgress = u62;
            if (m != null) {
                onProgress = m.onProgress(u62, list);
                if (m.getDispatchMode() == 0) {
                    return;
                }
            }
            if (view instanceof ViewGroup) {
                final ViewGroup viewGroup = (ViewGroup)view;
                for (int i = 0; i < viewGroup.getChildCount(); ++i) {
                    j(viewGroup.getChildAt(i), onProgress, list);
                }
            }
        }
        
        public static void k(final View view, final h62 h62, final h62.a a) {
            final b m = m(view);
            if (m != null) {
                m.onStart(h62, a);
                if (m.getDispatchMode() == 0) {
                    return;
                }
            }
            if (view instanceof ViewGroup) {
                final ViewGroup viewGroup = (ViewGroup)view;
                for (int i = 0; i < viewGroup.getChildCount(); ++i) {
                    k(viewGroup.getChildAt(i), h62, a);
                }
            }
        }
        
        public static WindowInsets l(final View view, final WindowInsets windowInsets) {
            if (view.getTag(fb1.L) != null) {
                return windowInsets;
            }
            return view.onApplyWindowInsets(windowInsets);
        }
        
        public static b m(final View view) {
            final Object tag = view.getTag(fb1.S);
            b a;
            if (tag instanceof a) {
                a = ((a)tag).a;
            }
            else {
                a = null;
            }
            return a;
        }
        
        public static u62 n(final u62 u62, final u62 u63, final float n, final int n2) {
            final u62.b b = new u62.b(u62);
            for (int i = 1; i <= 256; i <<= 1) {
                nf0 nf0;
                if ((n2 & i) == 0x0) {
                    nf0 = u62.f(i);
                }
                else {
                    final nf0 f = u62.f(i);
                    final nf0 f2 = u63.f(i);
                    final float n3 = (float)(f.a - f2.a);
                    final float n4 = 1.0f - n;
                    nf0 = u62.n(f, (int)(n3 * n4 + 0.5), (int)((f.b - f2.b) * n4 + 0.5), (int)((f.c - f2.c) * n4 + 0.5), (int)((f.d - f2.d) * n4 + 0.5));
                }
                b.b(i, nf0);
            }
            return b.a();
        }
        
        public static void o(final View view, final b b) {
            final Object tag = view.getTag(fb1.L);
            if (b == null) {
                view.setTag(fb1.S, (Object)null);
                if (tag == null) {
                    view.setOnApplyWindowInsetsListener((View$OnApplyWindowInsetsListener)null);
                }
            }
            else {
                final View$OnApplyWindowInsetsListener g = g(view, b);
                view.setTag(fb1.S, (Object)g);
                if (tag == null) {
                    view.setOnApplyWindowInsetsListener(g);
                }
            }
        }
        
        public static class a implements View$OnApplyWindowInsetsListener
        {
            public final b a;
            public u62 b;
            
            public a(final View view, final b a) {
                this.a = a;
                final u62 h = o32.H(view);
                u62 a2;
                if (h != null) {
                    a2 = new u62.b(h).a();
                }
                else {
                    a2 = null;
                }
                this.b = a2;
            }
            
            public WindowInsets onApplyWindowInsets(final View view, final WindowInsets b) {
                if (!view.isLaidOut()) {
                    this.b = u62.w(b, view);
                }
                else {
                    final u62 w = u62.w(b, view);
                    if (this.b == null) {
                        this.b = o32.H(view);
                    }
                    if (this.b != null) {
                        final b m = c.m(view);
                        if (m != null && Objects.equals(m.mDispachedInsets, b)) {
                            return c.l(view, b);
                        }
                        final int e = c.e(w, this.b);
                        if (e == 0) {
                            return c.l(view, b);
                        }
                        final u62 b2 = this.b;
                        final h62 h62 = new h62(e, (Interpolator)new DecelerateInterpolator(), 160L);
                        h62.e(0.0f);
                        final ValueAnimator setDuration = ValueAnimator.ofFloat(new float[] { 0.0f, 1.0f }).setDuration(h62.a());
                        final h62.a f = c.f(w, b2, e);
                        c.i(view, h62, b, false);
                        setDuration.addUpdateListener((ValueAnimator$AnimatorUpdateListener)new ValueAnimator$AnimatorUpdateListener(this, h62, w, b2, e, view) {
                            public final h62 a;
                            public final u62 b;
                            public final u62 c;
                            public final int d;
                            public final View e;
                            public final a f;
                            
                            public void onAnimationUpdate(final ValueAnimator valueAnimator) {
                                this.a.e(valueAnimator.getAnimatedFraction());
                                h62.c.j(this.e, h62.c.n(this.b, this.c, this.a.b(), this.d), Collections.singletonList(this.a));
                            }
                        });
                        ((Animator)setDuration).addListener((Animator$AnimatorListener)new AnimatorListenerAdapter(this, h62, view) {
                            public final h62 a;
                            public final View b;
                            public final a c;
                            
                            public void onAnimationEnd(final Animator animator) {
                                this.a.e(1.0f);
                                h62.c.h(this.b, this.a);
                            }
                        });
                        s11.a(view, new Runnable(this, view, h62, f, setDuration) {
                            public final View a;
                            public final h62 b;
                            public final h62.a c;
                            public final ValueAnimator d;
                            public final a e;
                            
                            @Override
                            public void run() {
                                h62.c.k(this.a, this.b, this.c);
                                this.d.start();
                            }
                        });
                    }
                    this.b = w;
                }
                return c.l(view, b);
            }
        }
    }
    
    public abstract static class e
    {
        public final int a;
        public float b;
        public final Interpolator c;
        public final long d;
        
        public e(final int a, final Interpolator c, final long d) {
            this.a = a;
            this.c = c;
            this.d = d;
        }
        
        public long a() {
            return this.d;
        }
        
        public float b() {
            final Interpolator c = this.c;
            if (c != null) {
                return ((TimeInterpolator)c).getInterpolation(this.b);
            }
            return this.b;
        }
        
        public int c() {
            return this.a;
        }
        
        public void d(final float b) {
            this.b = b;
        }
    }
    
    public static class d extends e
    {
        public final WindowInsetsAnimation e;
        
        public d(final int n, final Interpolator interpolator, final long n2) {
            this(n62.a(n, interpolator, n2));
        }
        
        public d(final WindowInsetsAnimation e) {
            super(0, null, 0L);
            this.e = e;
        }
        
        public static WindowInsetsAnimation$Bounds e(final h62.a a) {
            p62.a();
            return o62.a(a.a().e(), a.b().e());
        }
        
        public static nf0 f(final WindowInsetsAnimation$Bounds windowInsetsAnimation$Bounds) {
            return nf0.d(r62.a(windowInsetsAnimation$Bounds));
        }
        
        public static nf0 g(final WindowInsetsAnimation$Bounds windowInsetsAnimation$Bounds) {
            return nf0.d(q62.a(windowInsetsAnimation$Bounds));
        }
        
        public static void h(final View view, final b b) {
            a a;
            if (b != null) {
                a = new a(b);
            }
            else {
                a = null;
            }
            k62.a(view, (WindowInsetsAnimation$Callback)a);
        }
        
        @Override
        public long a() {
            return i62.a(this.e);
        }
        
        @Override
        public float b() {
            return j62.a(this.e);
        }
        
        @Override
        public int c() {
            return l62.a(this.e);
        }
        
        @Override
        public void d(final float n) {
            m62.a(this.e, n);
        }
        
        public static class a extends WindowInsetsAnimation$Callback
        {
            public final b a;
            public List b;
            public ArrayList c;
            public final HashMap d;
            
            public a(final b a) {
                super(a.getDispatchMode());
                this.d = new HashMap();
                this.a = a;
            }
            
            public final h62 a(final WindowInsetsAnimation windowInsetsAnimation) {
                h62 f;
                if ((f = this.d.get(windowInsetsAnimation)) == null) {
                    f = h62.f(windowInsetsAnimation);
                    this.d.put(windowInsetsAnimation, f);
                }
                return f;
            }
            
            public void onEnd(final WindowInsetsAnimation key) {
                this.a.onEnd(this.a(key));
                this.d.remove(key);
            }
            
            public void onPrepare(final WindowInsetsAnimation windowInsetsAnimation) {
                this.a.onPrepare(this.a(windowInsetsAnimation));
            }
            
            public WindowInsets onProgress(final WindowInsets windowInsets, final List list) {
                final ArrayList c = this.c;
                if (c == null) {
                    final ArrayList list2 = new ArrayList(list.size());
                    this.c = list2;
                    this.b = Collections.unmodifiableList((List<?>)list2);
                }
                else {
                    c.clear();
                }
                for (int i = list.size() - 1; i >= 0; --i) {
                    final WindowInsetsAnimation a = s62.a(list.get(i));
                    final h62 a2 = this.a(a);
                    a2.e(t62.a(a));
                    this.c.add(a2);
                }
                return this.a.onProgress(u62.v(windowInsets), this.b).u();
            }
            
            public WindowInsetsAnimation$Bounds onStart(final WindowInsetsAnimation windowInsetsAnimation, final WindowInsetsAnimation$Bounds windowInsetsAnimation$Bounds) {
                return this.a.onStart(this.a(windowInsetsAnimation), h62.a.d(windowInsetsAnimation$Bounds)).c();
            }
        }
    }
}

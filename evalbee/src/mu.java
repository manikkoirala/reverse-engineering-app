import java.util.Iterator;
import java.util.List;
import java.util.Collections;
import java.util.Comparator;
import com.google.firebase.database.collection.c;
import com.google.firebase.database.collection.b;

// 
// Decompiled by Procyon v0.6.0
// 

public final class mu implements Iterable
{
    public final b a;
    public final c b;
    
    public mu(final b a, final c b) {
        this.a = a;
        this.b = b;
    }
    
    public static mu c(final Comparator comparator) {
        return new mu(au.a(), new c(Collections.emptyList(), new lu(comparator)));
    }
    
    public mu b(final zt zt) {
        final mu n = this.n(zt.getKey());
        return new mu(n.a.l(zt.getKey(), zt), n.b.c(zt));
    }
    
    @Override
    public boolean equals(final Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || mu.class != o.getClass()) {
            return false;
        }
        final mu mu = (mu)o;
        if (this.size() != mu.size()) {
            return false;
        }
        final Iterator iterator = this.iterator();
        final Iterator iterator2 = mu.iterator();
        while (iterator.hasNext()) {
            if (!iterator.next().equals(iterator2.next())) {
                return false;
            }
        }
        return true;
    }
    
    public zt g(final du du) {
        return (zt)this.a.b(du);
    }
    
    @Override
    public int hashCode() {
        final Iterator iterator = this.iterator();
        int n = 0;
        while (iterator.hasNext()) {
            final zt zt = iterator.next();
            n = (n * 31 + zt.getKey().hashCode()) * 31 + zt.getData().hashCode();
        }
        return n;
    }
    
    public zt i() {
        return (zt)this.b.b();
    }
    
    public boolean isEmpty() {
        return this.a.isEmpty();
    }
    
    @Override
    public Iterator iterator() {
        return this.b.iterator();
    }
    
    public zt l() {
        return (zt)this.b.a();
    }
    
    public mu n(final du du) {
        final zt zt = (zt)this.a.b(du);
        if (zt == null) {
            return this;
        }
        return new mu(this.a.n(du), this.b.i(zt));
    }
    
    public int size() {
        return this.a.size();
    }
    
    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("[");
        final Iterator iterator = this.iterator();
        int n = 1;
        while (iterator.hasNext()) {
            final zt obj = iterator.next();
            if (n != 0) {
                n = 0;
            }
            else {
                sb.append(", ");
            }
            sb.append(obj);
        }
        sb.append("]");
        return sb.toString();
    }
}

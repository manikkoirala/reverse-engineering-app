// 
// Decompiled by Procyon v0.6.0
// 

public class wz implements t90
{
    @Override
    public double a(final double[] array, int i) {
        i = (int)array[0];
        double n = 1.0;
        while (i > 1) {
            n *= i;
            --i;
        }
        return n;
    }
    
    @Override
    public boolean b(final int n) {
        boolean b = true;
        if (n != 1) {
            b = false;
        }
        return b;
    }
    
    @Override
    public String toString() {
        return "fact(n)";
    }
}

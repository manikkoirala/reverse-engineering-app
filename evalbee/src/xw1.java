import com.google.android.gms.common.internal.Preconditions;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.Executor;
import com.google.android.gms.common.api.internal.BackgroundDetector;
import android.app.Application;
import android.content.Context;

// 
// Decompiled by Procyon v0.6.0
// 

public final class xw1
{
    public final gr a;
    public final ah b;
    public volatile boolean c;
    public volatile int d;
    public volatile long e;
    public volatile boolean f;
    
    public xw1(final Context context, final gr a, final ah b) {
        this.a = a;
        this.b = b;
        this.e = -1L;
        BackgroundDetector.initialize((Application)context.getApplicationContext());
        BackgroundDetector.getInstance().addListener((BackgroundDetector.BackgroundStateChangeListener)new BackgroundDetector.BackgroundStateChangeListener(this, a, b) {
            public final gr a;
            public final ah b;
            public final xw1 c;
            
            @Override
            public void onBackgroundStateChanged(final boolean b) {
                xw1.a(this.c, b);
                if (b) {
                    this.a.c();
                }
                else if (this.c.e()) {
                    this.a.g(xw1.c(this.c) - this.b.currentTimeMillis());
                }
            }
        });
    }
    
    public xw1(final Context context, final pq pq, final Executor executor, final ScheduledExecutorService scheduledExecutorService) {
        this(Preconditions.checkNotNull(context), new gr(Preconditions.checkNotNull(pq), executor, scheduledExecutorService), new ah.a());
    }
    
    public static /* synthetic */ boolean a(final xw1 xw1, final boolean c) {
        return xw1.c = c;
    }
    
    public static /* synthetic */ long c(final xw1 xw1) {
        return xw1.e;
    }
    
    public void d(final int n) {
        if (this.d == 0 && n > 0) {
            this.d = n;
            if (this.e()) {
                this.a.g(this.e - this.b.currentTimeMillis());
            }
        }
        else if (this.d > 0 && n == 0) {
            this.a.c();
        }
        this.d = n;
    }
    
    public final boolean e() {
        return this.f && !this.c && this.d > 0 && this.e != -1L;
    }
}

import android.os.Bundle;
import java.io.PrintWriter;
import java.io.FileDescriptor;

// 
// Decompiled by Procyon v0.6.0
// 

public abstract class rk0
{
    public static rk0 b(final qj0 qj0) {
        return new sk0(qj0, ((c42)qj0).getViewModelStore());
    }
    
    public abstract void a(final String p0, final FileDescriptor p1, final PrintWriter p2, final String[] p3);
    
    public abstract qk0 c(final int p0, final Bundle p1, final a p2);
    
    public abstract void d();
    
    public interface a
    {
        qk0 onCreateLoader(final int p0, final Bundle p1);
        
        void onLoadFinished(final qk0 p0, final Object p1);
        
        void onLoaderReset(final qk0 p0);
    }
}

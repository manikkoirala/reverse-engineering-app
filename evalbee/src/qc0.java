import java.nio.ByteBuffer;
import com.google.common.hash.Funnel;
import com.google.common.hash.HashCode;
import java.nio.charset.Charset;

// 
// Decompiled by Procyon v0.6.0
// 

public interface qc0 extends c81
{
    qc0 a(final int p0);
    
    qc0 b(final long p0);
    
    qc0 c(final CharSequence p0);
    
    qc0 d(final CharSequence p0, final Charset p1);
    
    HashCode e();
    
    qc0 g(final Object p0, final Funnel p1);
    
    qc0 h(final byte[] p0, final int p1, final int p2);
    
    qc0 i(final ByteBuffer p0);
}

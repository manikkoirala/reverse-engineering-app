import java.util.Iterator;
import java.util.List;
import java.util.LinkedHashMap;
import java.util.Map;

// 
// Decompiled by Procyon v0.6.0
// 

public final class pp1
{
    public final Object a;
    public final Map b;
    
    public pp1() {
        this.a = new Object();
        this.b = new LinkedHashMap();
    }
    
    public final boolean a(final x82 x82) {
        fg0.e((Object)x82, "id");
        synchronized (this.a) {
            return this.b.containsKey(x82);
        }
    }
    
    public final op1 b(final x82 x82) {
        fg0.e((Object)x82, "id");
        synchronized (this.a) {
            return this.b.remove(x82);
        }
    }
    
    public final List c(final String s) {
        fg0.e((Object)s, "workSpecId");
        synchronized (this.a) {
            final Map b = this.b;
            final LinkedHashMap linkedHashMap = new LinkedHashMap();
            for (final Map.Entry<x82, V> entry : b.entrySet()) {
                if (fg0.a((Object)entry.getKey().b(), (Object)s)) {
                    linkedHashMap.put(entry.getKey(), entry.getValue());
                }
            }
            final Iterator iterator2 = linkedHashMap.keySet().iterator();
            while (iterator2.hasNext()) {
                this.b.remove(iterator2.next());
            }
            return vh.P((Iterable)linkedHashMap.values());
        }
    }
    
    public final op1 d(final x82 x82) {
        fg0.e((Object)x82, "id");
        synchronized (this.a) {
            final Map b = this.b;
            Object value;
            if ((value = b.get(x82)) == null) {
                value = new op1(x82);
                b.put(x82, value);
            }
            return (op1)value;
        }
    }
    
    public final op1 e(final p92 p) {
        fg0.e((Object)p, "spec");
        return this.d(u92.a(p));
    }
}

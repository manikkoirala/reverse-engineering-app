import androidx.datastore.preferences.protobuf.c0;

// 
// Decompiled by Procyon v0.6.0
// 

public abstract class iz0
{
    public static final fz0 a;
    public static final fz0 b;
    
    static {
        a = c();
        b = new c0();
    }
    
    public static fz0 a() {
        return iz0.a;
    }
    
    public static fz0 b() {
        return iz0.b;
    }
    
    public static fz0 c() {
        try {
            return (fz0)Class.forName("androidx.datastore.preferences.protobuf.NewInstanceSchemaFull").getDeclaredConstructor((Class<?>[])new Class[0]).newInstance(new Object[0]);
        }
        catch (final Exception ex) {
            return null;
        }
    }
}

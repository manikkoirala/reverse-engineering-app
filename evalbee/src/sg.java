import android.graphics.drawable.shapes.RectShape;
import android.graphics.Canvas;
import android.graphics.Shader;
import android.graphics.Shader$TileMode;
import android.graphics.Paint;
import android.graphics.RadialGradient;
import android.graphics.drawable.Drawable;
import android.view.View;
import android.graphics.drawable.shapes.Shape;
import android.graphics.drawable.ShapeDrawable;
import android.graphics.drawable.shapes.OvalShape;
import android.content.Context;
import android.view.animation.Animation$AnimationListener;
import android.widget.ImageView;

// 
// Decompiled by Procyon v0.6.0
// 

public class sg extends ImageView
{
    public Animation$AnimationListener a;
    public int b;
    
    public sg(final Context context, final int color) {
        super(context);
        final float density = ((View)this).getContext().getResources().getDisplayMetrics().density;
        final int n = (int)(1.75f * density);
        final int n2 = (int)(0.0f * density);
        this.b = (int)(3.5f * density);
        ShapeDrawable shapeDrawable;
        if (this.a()) {
            shapeDrawable = new ShapeDrawable((Shape)new OvalShape());
            o32.y0((View)this, density * 4.0f);
        }
        else {
            shapeDrawable = new ShapeDrawable((Shape)new a(this.b));
            ((View)this).setLayerType(1, shapeDrawable.getPaint());
            shapeDrawable.getPaint().setShadowLayer((float)this.b, (float)n2, (float)n, 503316480);
            final int b = this.b;
            ((View)this).setPadding(b, b, b, b);
        }
        shapeDrawable.getPaint().setColor(color);
        o32.u0((View)this, (Drawable)shapeDrawable);
    }
    
    public final boolean a() {
        return true;
    }
    
    public void b(final Animation$AnimationListener a) {
        this.a = a;
    }
    
    public void onAnimationEnd() {
        super.onAnimationEnd();
        final Animation$AnimationListener a = this.a;
        if (a != null) {
            a.onAnimationEnd(((View)this).getAnimation());
        }
    }
    
    public void onAnimationStart() {
        super.onAnimationStart();
        final Animation$AnimationListener a = this.a;
        if (a != null) {
            a.onAnimationStart(((View)this).getAnimation());
        }
    }
    
    public void onMeasure(final int n, final int n2) {
        super.onMeasure(n, n2);
        if (!this.a()) {
            ((View)this).setMeasuredDimension(((View)this).getMeasuredWidth() + this.b * 2, ((View)this).getMeasuredHeight() + this.b * 2);
        }
    }
    
    public void setBackgroundColor(final int color) {
        if (((View)this).getBackground() instanceof ShapeDrawable) {
            ((ShapeDrawable)((View)this).getBackground()).getPaint().setColor(color);
        }
    }
    
    public class a extends OvalShape
    {
        public RadialGradient a;
        public Paint b;
        public final sg c;
        
        public a(final sg c, final int b) {
            this.c = c;
            this.b = new Paint();
            c.b = b;
            this.a((int)((RectShape)this).rect().width());
        }
        
        public final void a(final int n) {
            final float n2 = (float)(n / 2);
            final RadialGradient radialGradient = new RadialGradient(n2, n2, (float)this.c.b, new int[] { 1023410176, 0 }, (float[])null, Shader$TileMode.CLAMP);
            this.a = radialGradient;
            this.b.setShader((Shader)radialGradient);
        }
        
        public void draw(final Canvas canvas, final Paint paint) {
            final int width = ((View)this.c).getWidth();
            final int height = ((View)this.c).getHeight();
            final int n = width / 2;
            final float n2 = (float)n;
            final float n3 = (float)(height / 2);
            canvas.drawCircle(n2, n3, n2, this.b);
            canvas.drawCircle(n2, n3, (float)(n - this.c.b), paint);
        }
        
        public void onResize(final float n, final float n2) {
            super.onResize(n, n2);
            this.a((int)n);
        }
    }
}

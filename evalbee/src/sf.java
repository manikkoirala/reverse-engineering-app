import android.content.Context;

// 
// Decompiled by Procyon v0.6.0
// 

public class sf extends m7
{
    public int a;
    public int b;
    public int c;
    public int d;
    
    public sf(final Context context) {
        super(context);
        this.setTextSize(16.0f);
        this.setTextColor(-16777216);
    }
    
    public int getColumnIndex() {
        return this.b;
    }
    
    public int getQuestionNo() {
        return this.c;
    }
    
    public int getRowIndex() {
        return this.a;
    }
    
    public int getSubIndex() {
        return this.d;
    }
    
    public void setColumnIndex(final int b) {
        this.b = b;
    }
    
    public void setQuestionNo(final int c) {
        this.c = c;
    }
    
    public void setRowIndex(final int a) {
        this.a = a;
    }
    
    public void setSubIndex(final int d) {
        this.d = d;
    }
}

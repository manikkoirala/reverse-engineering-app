import android.content.res.Resources;
import android.util.TypedValue;
import android.content.Context;
import android.util.Log;
import android.view.ViewConfiguration;
import android.os.Build$VERSION;
import java.lang.reflect.Method;

// 
// Decompiled by Procyon v0.6.0
// 

public abstract class q32
{
    public static Method a;
    
    static {
        if (Build$VERSION.SDK_INT == 25) {
            try {
                q32.a = ViewConfiguration.class.getDeclaredMethod("getScaledScrollFactor", (Class<?>[])new Class[0]);
            }
            catch (final Exception ex) {
                Log.i("ViewConfigCompat", "Could not find method getScaledScrollFactor() on ViewConfiguration");
            }
        }
    }
    
    public static float a(final ViewConfiguration obj, final Context context) {
        if (Build$VERSION.SDK_INT >= 25) {
            final Method a = q32.a;
            if (a != null) {
                try {
                    return (float)(int)a.invoke(obj, new Object[0]);
                }
                catch (final Exception ex) {
                    Log.i("ViewConfigCompat", "Could not find method getScaledScrollFactor() on ViewConfiguration");
                }
            }
        }
        final TypedValue typedValue = new TypedValue();
        if (context.getTheme().resolveAttribute(16842829, typedValue, true)) {
            return typedValue.getDimension(context.getResources().getDisplayMetrics());
        }
        return 0.0f;
    }
    
    public static float b(final ViewConfiguration viewConfiguration, final Context context) {
        if (Build$VERSION.SDK_INT >= 26) {
            return q32.a.a(viewConfiguration);
        }
        return a(viewConfiguration, context);
    }
    
    public static int c(final ViewConfiguration viewConfiguration) {
        if (Build$VERSION.SDK_INT >= 28) {
            return b.a(viewConfiguration);
        }
        return viewConfiguration.getScaledTouchSlop() / 2;
    }
    
    public static float d(final ViewConfiguration viewConfiguration, final Context context) {
        if (Build$VERSION.SDK_INT >= 26) {
            return q32.a.b(viewConfiguration);
        }
        return a(viewConfiguration, context);
    }
    
    public static boolean e(final ViewConfiguration viewConfiguration, final Context context) {
        if (Build$VERSION.SDK_INT >= 28) {
            return b.b(viewConfiguration);
        }
        final Resources resources = context.getResources();
        final int identifier = resources.getIdentifier("config_showMenuShortcutsWhenKeyboardPresent", "bool", "android");
        return identifier != 0 && resources.getBoolean(identifier);
    }
    
    public abstract static class a
    {
        public static float a(final ViewConfiguration viewConfiguration) {
            return viewConfiguration.getScaledHorizontalScrollFactor();
        }
        
        public static float b(final ViewConfiguration viewConfiguration) {
            return viewConfiguration.getScaledVerticalScrollFactor();
        }
    }
    
    public abstract static class b
    {
        public static int a(final ViewConfiguration viewConfiguration) {
            return viewConfiguration.getScaledHoverSlop();
        }
        
        public static boolean b(final ViewConfiguration viewConfiguration) {
            return viewConfiguration.shouldShowMenuShortcutsWhenKeyboardPresent();
        }
    }
}

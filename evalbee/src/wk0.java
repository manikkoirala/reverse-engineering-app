import java.util.Iterator;
import java.util.Map;
import com.google.firebase.database.collection.b;

// 
// Decompiled by Procyon v0.6.0
// 

public final class wk0
{
    public final int a;
    public final b b;
    
    public wk0(final int a, final b b) {
        this.a = a;
        this.b = b;
    }
    
    public static wk0 a(final int n, final Map map) {
        final b a = au.a();
        final Iterator iterator = map.entrySet().iterator();
        b l = a;
        while (iterator.hasNext()) {
            final Map.Entry<du, V> entry = (Map.Entry<du, V>)iterator.next();
            l = l.l(entry.getKey(), ((m21)entry.getValue()).a());
        }
        return new wk0(n, l);
    }
    
    public int b() {
        return this.a;
    }
    
    public b c() {
        return this.b;
    }
}

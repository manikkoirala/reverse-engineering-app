import android.app.RemoteInput$Builder;
import android.os.Bundle;
import android.content.Intent;
import android.app.RemoteInput;

// 
// Decompiled by Procyon v0.6.0
// 

public abstract class kd1
{
    public static RemoteInput a(final kd1 kd1) {
        return a.b(kd1);
    }
    
    public static RemoteInput[] b(final kd1[] array) {
        if (array == null) {
            return null;
        }
        final RemoteInput[] array2 = new RemoteInput[array.length];
        for (int i = 0; i < array.length; ++i) {
            final kd1 kd1 = array[i];
            array2[i] = a(null);
        }
        return array2;
    }
    
    public abstract static class a
    {
        public static void a(final Object o, final Intent intent, final Bundle bundle) {
            RemoteInput.addResultsToIntent((RemoteInput[])o, intent, bundle);
        }
        
        public static RemoteInput b(final kd1 kd1) {
            new(android.app.RemoteInput$Builder.class)();
            throw null;
        }
        
        public static Bundle c(final Intent intent) {
            return RemoteInput.getResultsFromIntent(intent);
        }
    }
}

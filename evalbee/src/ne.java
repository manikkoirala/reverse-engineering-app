import java.util.Map;
import com.google.common.collect.ImmutableMap;
import java.util.concurrent.Callable;
import java.util.concurrent.ConcurrentMap;

// 
// Decompiled by Procyon v0.6.0
// 

public interface ne
{
    ConcurrentMap asMap();
    
    void cleanUp();
    
    Object get(final Object p0, final Callable p1);
    
    ImmutableMap getAllPresent(final Iterable p0);
    
    Object getIfPresent(final Object p0);
    
    void invalidate(final Object p0);
    
    void invalidateAll();
    
    void invalidateAll(final Iterable p0);
    
    void put(final Object p0, final Object p1);
    
    void putAll(final Map p0);
    
    long size();
    
    pe stats();
}

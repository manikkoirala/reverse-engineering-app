import android.content.BroadcastReceiver;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.Context;

// 
// Decompiled by Procyon v0.6.0
// 

public final class rb extends uc
{
    public rb(final Context context, final hu1 hu1) {
        fg0.e((Object)context, "context");
        fg0.e((Object)hu1, "taskExecutor");
        super(context, hu1);
    }
    
    @Override
    public IntentFilter j() {
        final IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction("android.intent.action.BATTERY_OKAY");
        intentFilter.addAction("android.intent.action.BATTERY_LOW");
        return intentFilter;
    }
    
    @Override
    public void k(final Intent intent) {
        fg0.e((Object)intent, "intent");
        if (intent.getAction() == null) {
            return;
        }
        final xl0 e = xl0.e();
        final String a = sb.a();
        final StringBuilder sb = new StringBuilder();
        sb.append("Received ");
        sb.append(intent.getAction());
        e.a(a, sb.toString());
        final String action = intent.getAction();
        if (action != null) {
            final int hashCode = action.hashCode();
            Boolean b;
            if (hashCode != -1980154005) {
                if (hashCode != 490310653) {
                    return;
                }
                if (!action.equals("android.intent.action.BATTERY_LOW")) {
                    return;
                }
                b = Boolean.FALSE;
            }
            else {
                if (!action.equals("android.intent.action.BATTERY_OKAY")) {
                    return;
                }
                b = Boolean.TRUE;
            }
            this.g(b);
        }
    }
    
    public Boolean l() {
        final Intent registerReceiver = this.d().registerReceiver((BroadcastReceiver)null, new IntentFilter("android.intent.action.BATTERY_CHANGED"));
        if (registerReceiver == null) {
            xl0.e().c(sb.a(), "getInitialState - null intent received");
            return Boolean.FALSE;
        }
        final int intExtra = registerReceiver.getIntExtra("status", -1);
        final float n = registerReceiver.getIntExtra("level", -1) / (float)registerReceiver.getIntExtra("scale", -1);
        boolean b = true;
        if (intExtra != 1) {
            b = (n > 0.15f && b);
        }
        return b;
    }
}

import java.util.TreeMap;
import java.text.ParseException;
import com.android.volley.e;
import java.util.TimeZone;
import java.util.Locale;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.Collections;
import java.util.Date;
import java.util.Iterator;
import java.util.Map;
import java.util.Collection;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.TreeSet;
import com.android.volley.a;
import java.util.List;

// 
// Decompiled by Procyon v0.6.0
// 

public abstract class id0
{
    public static List a(final List c, final a.a a) {
        final TreeSet set = new TreeSet((Comparator<? super E>)String.CASE_INSENSITIVE_ORDER);
        if (!c.isEmpty()) {
            final Iterator iterator = c.iterator();
            while (iterator.hasNext()) {
                set.add(((sc0)iterator.next()).a());
            }
        }
        final ArrayList list = new ArrayList(c);
        final List h = a.h;
        if (h != null) {
            if (!h.isEmpty()) {
                for (final sc0 sc0 : a.h) {
                    if (!set.contains(sc0.a())) {
                        list.add(sc0);
                    }
                }
            }
        }
        else if (!a.g.isEmpty()) {
            for (final Map.Entry<Object, V> entry : a.g.entrySet()) {
                if (!set.contains(entry.getKey())) {
                    list.add(new sc0(entry.getKey(), (String)entry.getValue()));
                }
            }
        }
        return list;
    }
    
    public static String b(final long date) {
        return d("EEE, dd MMM yyyy HH:mm:ss 'GMT'").format(new Date(date));
    }
    
    public static Map c(final a.a a) {
        if (a == null) {
            return Collections.emptyMap();
        }
        final HashMap hashMap = new HashMap();
        final String b = a.b;
        if (b != null) {
            hashMap.put("If-None-Match", b);
        }
        final long d = a.d;
        if (d > 0L) {
            hashMap.put("If-Modified-Since", b(d));
        }
        return hashMap;
    }
    
    public static SimpleDateFormat d(final String pattern) {
        final SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern, Locale.US);
        simpleDateFormat.setTimeZone(TimeZone.getTimeZone("GMT"));
        return simpleDateFormat;
    }
    
    public static a.a e(final yy0 p0) {
        // 
        // This method could not be decompiled.
        // 
        // Original Bytecode:
        // 
        //     3: lstore          14
        //     5: aload_0        
        //     6: getfield        yy0.c:Ljava/util/Map;
        //     9: astore          16
        //    11: aload           16
        //    13: ifnonnull       18
        //    16: aconst_null    
        //    17: areturn        
        //    18: aload           16
        //    20: ldc             "Date"
        //    22: invokeinterface java/util/Map.get:(Ljava/lang/Object;)Ljava/lang/Object;
        //    27: checkcast       Ljava/lang/String;
        //    30: astore          17
        //    32: aload           17
        //    34: ifnull          47
        //    37: aload           17
        //    39: invokestatic    id0.h:(Ljava/lang/String;)J
        //    42: lstore          8
        //    44: goto            50
        //    47: lconst_0       
        //    48: lstore          8
        //    50: aload           16
        //    52: ldc             "Cache-Control"
        //    54: invokeinterface java/util/Map.get:(Ljava/lang/Object;)Ljava/lang/Object;
        //    59: checkcast       Ljava/lang/String;
        //    62: astore          17
        //    64: iconst_0       
        //    65: istore_3       
        //    66: iconst_0       
        //    67: istore_2       
        //    68: aload           17
        //    70: ifnull          247
        //    73: aload           17
        //    75: ldc             ","
        //    77: iconst_0       
        //    78: invokevirtual   java/lang/String.split:(Ljava/lang/String;I)[Ljava/lang/String;
        //    81: astore          17
        //    83: iconst_0       
        //    84: istore_1       
        //    85: lconst_0       
        //    86: lstore          6
        //    88: lconst_0       
        //    89: lstore          4
        //    91: iload_2        
        //    92: aload           17
        //    94: arraylength    
        //    95: if_icmpge       242
        //    98: aload           17
        //   100: iload_2        
        //   101: aaload         
        //   102: invokevirtual   java/lang/String.trim:()Ljava/lang/String;
        //   105: astore          18
        //   107: aload           18
        //   109: ldc             "no-cache"
        //   111: invokevirtual   java/lang/String.equals:(Ljava/lang/Object;)Z
        //   114: ifne            240
        //   117: aload           18
        //   119: ldc             "no-store"
        //   121: invokevirtual   java/lang/String.equals:(Ljava/lang/Object;)Z
        //   124: ifeq            130
        //   127: goto            240
        //   130: aload           18
        //   132: ldc             "max-age="
        //   134: invokevirtual   java/lang/String.startsWith:(Ljava/lang/String;)Z
        //   137: ifeq            159
        //   140: aload           18
        //   142: bipush          8
        //   144: invokevirtual   java/lang/String.substring:(I)Ljava/lang/String;
        //   147: invokestatic    java/lang/Long.parseLong:(Ljava/lang/String;)J
        //   150: lstore          10
        //   152: lload           4
        //   154: lstore          12
        //   156: goto            226
        //   159: aload           18
        //   161: ldc             "stale-while-revalidate="
        //   163: invokevirtual   java/lang/String.startsWith:(Ljava/lang/String;)Z
        //   166: ifeq            188
        //   169: aload           18
        //   171: bipush          23
        //   173: invokevirtual   java/lang/String.substring:(I)Ljava/lang/String;
        //   176: invokestatic    java/lang/Long.parseLong:(Ljava/lang/String;)J
        //   179: lstore          12
        //   181: lload           6
        //   183: lstore          10
        //   185: goto            226
        //   188: aload           18
        //   190: ldc             "must-revalidate"
        //   192: invokevirtual   java/lang/String.equals:(Ljava/lang/Object;)Z
        //   195: ifne            216
        //   198: lload           6
        //   200: lstore          10
        //   202: lload           4
        //   204: lstore          12
        //   206: aload           18
        //   208: ldc             "proxy-revalidate"
        //   210: invokevirtual   java/lang/String.equals:(Ljava/lang/Object;)Z
        //   213: ifeq            226
        //   216: iconst_1       
        //   217: istore_1       
        //   218: lload           4
        //   220: lstore          12
        //   222: lload           6
        //   224: lstore          10
        //   226: iinc            2, 1
        //   229: lload           10
        //   231: lstore          6
        //   233: lload           12
        //   235: lstore          4
        //   237: goto            91
        //   240: aconst_null    
        //   241: areturn        
        //   242: iconst_1       
        //   243: istore_2       
        //   244: goto            257
        //   247: iconst_0       
        //   248: istore_1       
        //   249: lconst_0       
        //   250: lstore          6
        //   252: lconst_0       
        //   253: lstore          4
        //   255: iload_3        
        //   256: istore_2       
        //   257: aload           16
        //   259: ldc             "Expires"
        //   261: invokeinterface java/util/Map.get:(Ljava/lang/Object;)Ljava/lang/Object;
        //   266: checkcast       Ljava/lang/String;
        //   269: astore          17
        //   271: aload           17
        //   273: ifnull          286
        //   276: aload           17
        //   278: invokestatic    id0.h:(Ljava/lang/String;)J
        //   281: lstore          12
        //   283: goto            289
        //   286: lconst_0       
        //   287: lstore          12
        //   289: aload           16
        //   291: ldc             "Last-Modified"
        //   293: invokeinterface java/util/Map.get:(Ljava/lang/Object;)Ljava/lang/Object;
        //   298: checkcast       Ljava/lang/String;
        //   301: astore          17
        //   303: aload           17
        //   305: ifnull          318
        //   308: aload           17
        //   310: invokestatic    id0.h:(Ljava/lang/String;)J
        //   313: lstore          10
        //   315: goto            321
        //   318: lconst_0       
        //   319: lstore          10
        //   321: aload           16
        //   323: ldc             "ETag"
        //   325: invokeinterface java/util/Map.get:(Ljava/lang/Object;)Ljava/lang/Object;
        //   330: checkcast       Ljava/lang/String;
        //   333: astore          18
        //   335: iload_2        
        //   336: ifeq            387
        //   339: lload           14
        //   341: lload           6
        //   343: ldc2_w          1000
        //   346: lmul           
        //   347: ladd           
        //   348: lstore          6
        //   350: iload_1        
        //   351: ifeq            361
        //   354: lload           6
        //   356: lstore          4
        //   358: goto            372
        //   361: lload           4
        //   363: ldc2_w          1000
        //   366: lmul           
        //   367: lload           6
        //   369: ladd           
        //   370: lstore          4
        //   372: lload           4
        //   374: lstore          12
        //   376: lload           6
        //   378: lstore          4
        //   380: lload           12
        //   382: lstore          6
        //   384: goto            425
        //   387: lconst_0       
        //   388: lstore          6
        //   390: lload           8
        //   392: lconst_0       
        //   393: lcmp           
        //   394: ifle            422
        //   397: lload           12
        //   399: lload           8
        //   401: lcmp           
        //   402: iflt            422
        //   405: lload           14
        //   407: lload           12
        //   409: lload           8
        //   411: lsub           
        //   412: ladd           
        //   413: lstore          4
        //   415: lload           4
        //   417: lstore          6
        //   419: goto            425
        //   422: lconst_0       
        //   423: lstore          4
        //   425: new             Lcom/android/volley/a$a;
        //   428: dup            
        //   429: invokespecial   com/android/volley/a$a.<init>:()V
        //   432: astore          17
        //   434: aload           17
        //   436: aload_0        
        //   437: getfield        yy0.b:[B
        //   440: putfield        com/android/volley/a$a.a:[B
        //   443: aload           17
        //   445: aload           18
        //   447: putfield        com/android/volley/a$a.b:Ljava/lang/String;
        //   450: aload           17
        //   452: lload           4
        //   454: putfield        com/android/volley/a$a.f:J
        //   457: aload           17
        //   459: lload           6
        //   461: putfield        com/android/volley/a$a.e:J
        //   464: aload           17
        //   466: lload           8
        //   468: putfield        com/android/volley/a$a.c:J
        //   471: aload           17
        //   473: lload           10
        //   475: putfield        com/android/volley/a$a.d:J
        //   478: aload           17
        //   480: aload           16
        //   482: putfield        com/android/volley/a$a.g:Ljava/util/Map;
        //   485: aload           17
        //   487: aload_0        
        //   488: getfield        yy0.d:Ljava/util/List;
        //   491: putfield        com/android/volley/a$a.h:Ljava/util/List;
        //   494: aload           17
        //   496: areturn        
        //   497: astore          18
        //   499: lload           6
        //   501: lstore          10
        //   503: lload           4
        //   505: lstore          12
        //   507: goto            226
        //    Exceptions:
        //  Try           Handler
        //  Start  End    Start  End    Type                 
        //  -----  -----  -----  -----  ---------------------
        //  140    152    497    510    Ljava/lang/Exception;
        //  169    181    497    510    Ljava/lang/Exception;
        // 
        // The error that occurred was:
        // 
        // java.lang.NullPointerException
        //     at com.strobel.assembler.ir.StackMappingVisitor.push(StackMappingVisitor.java:290)
        //     at com.strobel.assembler.ir.StackMappingVisitor$InstructionAnalyzer.execute(StackMappingVisitor.java:837)
        //     at com.strobel.assembler.ir.StackMappingVisitor$InstructionAnalyzer.visit(StackMappingVisitor.java:398)
        //     at com.strobel.decompiler.ast.AstBuilder.performStackAnalysis(AstBuilder.java:2086)
        //     at com.strobel.decompiler.ast.AstBuilder.build(AstBuilder.java:108)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:203)
        //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:93)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethodBody(AstBuilder.java:868)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethod(AstBuilder.java:761)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addTypeMembers(AstBuilder.java:638)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeCore(AstBuilder.java:605)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeNoCache(AstBuilder.java:195)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createType(AstBuilder.java:162)
        //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addType(AstBuilder.java:137)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.buildAst(JavaLanguage.java:71)
        //     at com.strobel.decompiler.languages.java.JavaLanguage.decompileType(JavaLanguage.java:59)
        //     at com.strobel.decompiler.DecompilerDriver.decompileType(DecompilerDriver.java:333)
        //     at com.strobel.decompiler.DecompilerDriver.decompileJar(DecompilerDriver.java:254)
        //     at com.strobel.decompiler.DecompilerDriver.main(DecompilerDriver.java:144)
        // 
        throw new IllegalStateException("An error occurred while decompiling this method.");
    }
    
    public static String f(final Map map) {
        return g(map, "ISO-8859-1");
    }
    
    public static String g(final Map map, final String s) {
        if (map == null) {
            return s;
        }
        final String s2 = map.get("Content-Type");
        if (s2 != null) {
            final String[] split = s2.split(";", 0);
            for (int i = 1; i < split.length; ++i) {
                final String[] split2 = split[i].trim().split("=", 0);
                if (split2.length == 2 && split2[0].equals("charset")) {
                    return split2[1];
                }
            }
        }
        return s;
    }
    
    public static long h(final String anObject) {
        try {
            return d("EEE, dd MMM yyyy HH:mm:ss zzz").parse(anObject).getTime();
        }
        catch (final ParseException ex) {
            if (!"0".equals(anObject) && !"-1".equals(anObject)) {
                e.d(ex, "Unable to parse dateStr: %s, falling back to 0", anObject);
            }
            else {
                e.e("Unable to parse dateStr: %s, falling back to 0", anObject);
            }
            return 0L;
        }
    }
    
    public static List i(final Map map) {
        final ArrayList list = new ArrayList(map.size());
        for (final Map.Entry<String, V> entry : map.entrySet()) {
            list.add(new sc0(entry.getKey(), (String)entry.getValue()));
        }
        return list;
    }
    
    public static Map j(final List list) {
        final TreeMap treeMap = new TreeMap((Comparator<? super K>)String.CASE_INSENSITIVE_ORDER);
        for (final sc0 sc0 : list) {
            treeMap.put(sc0.a(), sc0.b());
        }
        return treeMap;
    }
}

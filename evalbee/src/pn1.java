import java.util.Iterator;
import android.util.Log;
import android.text.TextUtils;
import java.util.concurrent.Executor;
import java.util.ArrayDeque;
import android.content.SharedPreferences;

// 
// Decompiled by Procyon v0.6.0
// 

public final class pn1
{
    public final SharedPreferences a;
    public final String b;
    public final String c;
    public final ArrayDeque d;
    public final Executor e;
    public boolean f;
    
    public pn1(final SharedPreferences a, final String b, final String c, final Executor e) {
        this.d = new ArrayDeque();
        this.f = false;
        this.a = a;
        this.b = b;
        this.c = c;
        this.e = e;
    }
    
    public static pn1 c(final SharedPreferences sharedPreferences, final String s, final String s2, final Executor executor) {
        final pn1 pn1 = new pn1(sharedPreferences, s, s2, executor);
        pn1.d();
        return pn1;
    }
    
    public final boolean b(final boolean b) {
        if (b && !this.f) {
            this.i();
        }
        return b;
    }
    
    public final void d() {
        synchronized (this.d) {
            this.d.clear();
            final String string = this.a.getString(this.b, "");
            if (!TextUtils.isEmpty((CharSequence)string) && string.contains(this.c)) {
                final String[] split = string.split(this.c, -1);
                if (split.length == 0) {
                    Log.e("FirebaseMessaging", "Corrupted queue. Please check the queue contents and item separator provided");
                }
                for (final String e : split) {
                    if (!TextUtils.isEmpty((CharSequence)e)) {
                        this.d.add(e);
                    }
                }
            }
        }
    }
    
    public String e() {
        synchronized (this.d) {
            return this.d.peek();
        }
    }
    
    public boolean f(final Object o) {
        synchronized (this.d) {
            return this.b(this.d.remove(o));
        }
    }
    
    public String g() {
        final StringBuilder sb = new StringBuilder();
        final Iterator iterator = this.d.iterator();
        while (iterator.hasNext()) {
            sb.append((String)iterator.next());
            sb.append(this.c);
        }
        return sb.toString();
    }
    
    public final void h() {
        synchronized (this.d) {
            this.a.edit().putString(this.b, this.g()).commit();
        }
    }
    
    public final void i() {
        this.e.execute(new on1(this));
    }
}

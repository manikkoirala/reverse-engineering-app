import java.io.IOException;
import java.util.Arrays;
import java.util.concurrent.atomic.AtomicLong;
import java.util.concurrent.atomic.AtomicInteger;
import java.math.BigInteger;
import java.math.BigDecimal;
import java.util.Objects;
import java.io.Writer;
import java.util.regex.Pattern;
import java.io.Flushable;
import java.io.Closeable;

// 
// Decompiled by Procyon v0.6.0
// 

public class vh0 implements Closeable, Flushable
{
    public static final Pattern j;
    public static final String[] k;
    public static final String[] l;
    public final Writer a;
    public int[] b;
    public int c;
    public String d;
    public String e;
    public boolean f;
    public boolean g;
    public String h;
    public boolean i;
    
    static {
        j = Pattern.compile("-?(?:0|[1-9][0-9]*)(?:\\.[0-9]+)?(?:[eE][-+]?[0-9]+)?");
        k = new String[128];
        for (int i = 0; i <= 31; ++i) {
            vh0.k[i] = String.format("\\u%04x", i);
        }
        final String[] m = vh0.k;
        m[34] = "\\\"";
        m[92] = "\\\\";
        m[9] = "\\t";
        m[8] = "\\b";
        m[10] = "\\n";
        m[13] = "\\r";
        m[12] = "\\f";
        final String[] l2 = m.clone();
        (l = l2)[60] = "\\u003c";
        l2[62] = "\\u003e";
        l2[38] = "\\u0026";
        l2[61] = "\\u003d";
        l2[39] = "\\u0027";
    }
    
    public vh0(final Writer writer) {
        this.b = new int[32];
        this.c = 0;
        this.H(6);
        this.e = ":";
        this.i = true;
        Objects.requireNonNull(writer, "out == null");
        this.a = writer;
    }
    
    public static boolean k(final Class clazz) {
        return clazz == Integer.class || clazz == Long.class || clazz == Double.class || clazz == Float.class || clazz == Byte.class || clazz == Short.class || clazz == BigDecimal.class || clazz == BigInteger.class || clazz == AtomicInteger.class || clazz == AtomicLong.class;
    }
    
    public final int E() {
        final int c = this.c;
        if (c != 0) {
            return this.b[c - 1];
        }
        throw new IllegalStateException("JsonWriter is closed.");
    }
    
    public final void H(final int n) {
        final int c = this.c;
        final int[] b = this.b;
        if (c == b.length) {
            this.b = Arrays.copyOf(b, c * 2);
        }
        this.b[this.c++] = n;
    }
    
    public final void J(final int n) {
        this.b[this.c - 1] = n;
    }
    
    public final void K(final boolean g) {
        this.g = g;
    }
    
    public final void O(String s) {
        if (s.length() == 0) {
            this.d = null;
            s = ":";
        }
        else {
            this.d = s;
            s = ": ";
        }
        this.e = s;
    }
    
    public final void R(final boolean f) {
        this.f = f;
    }
    
    public final void Z(final boolean i) {
        this.i = i;
    }
    
    public final void a() {
        final int e = this.E();
        if (e == 5) {
            this.a.write(44);
        }
        else if (e != 3) {
            throw new IllegalStateException("Nesting problem.");
        }
        this.q();
        this.J(4);
    }
    
    public final void b() {
        final int e = this.E();
        if (e != 1) {
            if (e != 2) {
                if (e != 4) {
                    if (e != 6) {
                        if (e != 7) {
                            throw new IllegalStateException("Nesting problem.");
                        }
                        if (!this.f) {
                            throw new IllegalStateException("JSON must have only one top-level value.");
                        }
                    }
                    this.J(7);
                    return;
                }
                this.a.append((CharSequence)this.e);
                this.J(5);
                return;
            }
            else {
                this.a.append(',');
            }
        }
        else {
            this.J(2);
        }
        this.q();
    }
    
    public vh0 c() {
        this.t0();
        return this.x(1, '[');
    }
    
    @Override
    public void close() {
        this.a.close();
        final int c = this.c;
        if (c <= 1 && (c != 1 || this.b[c - 1] == 7)) {
            this.c = 0;
            return;
        }
        throw new IOException("Incomplete document");
    }
    
    public vh0 d() {
        this.t0();
        return this.x(3, '{');
    }
    
    public final vh0 e(final int n, final int n2, final char c) {
        final int e = this.E();
        if (e != n2 && e != n) {
            throw new IllegalStateException("Nesting problem.");
        }
        if (this.h == null) {
            --this.c;
            if (e == n2) {
                this.q();
            }
            this.a.write(c);
            return this;
        }
        final StringBuilder sb = new StringBuilder();
        sb.append("Dangling name: ");
        sb.append(this.h);
        throw new IllegalStateException(sb.toString());
    }
    
    public vh0 f() {
        return this.e(1, 2, ']');
    }
    
    @Override
    public void flush() {
        if (this.c != 0) {
            this.a.flush();
            return;
        }
        throw new IllegalStateException("JsonWriter is closed.");
    }
    
    public vh0 g() {
        return this.e(3, 5, '}');
    }
    
    public final boolean h() {
        return this.i;
    }
    
    public final boolean i() {
        return this.g;
    }
    
    public final void i0(final String s) {
        String[] array;
        if (this.g) {
            array = vh0.l;
        }
        else {
            array = vh0.k;
        }
        this.a.write(34);
        final int length = s.length();
        int i = 0;
        int n = 0;
        while (i < length) {
            final char char1 = s.charAt(i);
            int n2 = 0;
            Label_0146: {
                String str;
                if (char1 < '\u0080') {
                    if ((str = array[char1]) == null) {
                        n2 = n;
                        break Label_0146;
                    }
                }
                else if (char1 == '\u2028') {
                    str = "\\u2028";
                }
                else {
                    n2 = n;
                    if (char1 != '\u2029') {
                        break Label_0146;
                    }
                    str = "\\u2029";
                }
                if (n < i) {
                    this.a.write(s, n, i - n);
                }
                this.a.write(str);
                n2 = i + 1;
            }
            ++i;
            n = n2;
        }
        if (n < length) {
            this.a.write(s, n, length - n);
        }
        this.a.write(34);
    }
    
    public boolean j() {
        return this.f;
    }
    
    public vh0 m0(final double n) {
        this.t0();
        if (!this.f && (Double.isNaN(n) || Double.isInfinite(n))) {
            final StringBuilder sb = new StringBuilder();
            sb.append("Numeric values must be finite, but was ");
            sb.append(n);
            throw new IllegalArgumentException(sb.toString());
        }
        this.b();
        this.a.append((CharSequence)Double.toString(n));
        return this;
    }
    
    public vh0 o(final String s) {
        Objects.requireNonNull(s, "name == null");
        if (this.h != null) {
            throw new IllegalStateException();
        }
        if (this.c != 0) {
            this.h = s;
            return this;
        }
        throw new IllegalStateException("JsonWriter is closed.");
    }
    
    public vh0 o0(final long i) {
        this.t0();
        this.b();
        this.a.write(Long.toString(i));
        return this;
    }
    
    public vh0 p0(final Boolean b) {
        if (b == null) {
            return this.u();
        }
        this.t0();
        this.b();
        final Writer a = this.a;
        String str;
        if (b) {
            str = "true";
        }
        else {
            str = "false";
        }
        a.write(str);
        return this;
    }
    
    public final void q() {
        if (this.d == null) {
            return;
        }
        this.a.write(10);
        for (int c = this.c, i = 1; i < c; ++i) {
            this.a.write(this.d);
        }
    }
    
    public vh0 q0(final Number n) {
        if (n == null) {
            return this.u();
        }
        this.t0();
        final String string = n.toString();
        if (!string.equals("-Infinity") && !string.equals("Infinity") && !string.equals("NaN")) {
            final Class<? extends Number> class1 = n.getClass();
            if (!k(class1)) {
                if (!vh0.j.matcher(string).matches()) {
                    final StringBuilder sb = new StringBuilder();
                    sb.append("String created by ");
                    sb.append(class1);
                    sb.append(" is not a valid JSON number: ");
                    sb.append(string);
                    throw new IllegalArgumentException(sb.toString());
                }
            }
        }
        else if (!this.f) {
            final StringBuilder sb2 = new StringBuilder();
            sb2.append("Numeric values must be finite, but was ");
            sb2.append(string);
            throw new IllegalArgumentException(sb2.toString());
        }
        this.b();
        this.a.append((CharSequence)string);
        return this;
    }
    
    public vh0 r0(final String s) {
        if (s == null) {
            return this.u();
        }
        this.t0();
        this.b();
        this.i0(s);
        return this;
    }
    
    public vh0 s0(final boolean b) {
        this.t0();
        this.b();
        final Writer a = this.a;
        String str;
        if (b) {
            str = "true";
        }
        else {
            str = "false";
        }
        a.write(str);
        return this;
    }
    
    public final void t0() {
        if (this.h != null) {
            this.a();
            this.i0(this.h);
            this.h = null;
        }
    }
    
    public vh0 u() {
        if (this.h != null) {
            if (!this.i) {
                this.h = null;
                return this;
            }
            this.t0();
        }
        this.b();
        this.a.write("null");
        return this;
    }
    
    public final vh0 x(final int n, final char c) {
        this.b();
        this.H(n);
        this.a.write(c);
        return this;
    }
}

import java.util.Locale;
import java.util.List;
import android.content.ContentValues;
import android.database.Cursor;
import android.os.CancellationSignal;
import java.io.Closeable;

// 
// Decompiled by Procyon v0.6.0
// 

public interface ss1 extends Closeable
{
    Cursor B(final vs1 p0, final CancellationSignal p1);
    
    long C();
    
    long G(final String p0, final int p1, final ContentValues p2);
    
    Cursor L(final vs1 p0);
    
    void M(final String p0);
    
    boolean N();
    
    void S();
    
    void T(final String p0, final Object[] p1);
    
    long U(final long p0);
    
    void X();
    
    void c0(final int p0);
    
    ws1 d0(final String p0);
    
    int f0(final String p0, final int p1, final ContentValues p2, final String p3, final Object[] p4);
    
    boolean g0();
    
    long getPageSize();
    
    String getPath();
    
    int getVersion();
    
    Cursor h0(final String p0);
    
    boolean isOpen();
    
    boolean isReadOnly();
    
    boolean j0();
    
    boolean k0();
    
    int l(final String p0, final String p1, final Object[] p2);
    
    void l0(final int p0);
    
    void m();
    
    List n();
    
    void n0(final long p0);
    
    void r();
    
    boolean s();
    
    void setLocale(final Locale p0);
    
    boolean t(final int p0);
    
    void z(final boolean p0);
}

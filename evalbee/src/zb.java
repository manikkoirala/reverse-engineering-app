import android.text.TextUtils;
import com.google.android.gms.internal.play_billing.zzx;
import java.util.Iterator;
import java.util.List;
import java.util.Collection;
import java.util.ArrayList;
import com.google.android.gms.internal.play_billing.zzaf;

// 
// Decompiled by Procyon v0.6.0
// 

public class zb
{
    public boolean a;
    public String b;
    public String c;
    public c d;
    public zzaf e;
    public ArrayList f;
    public boolean g;
    
    public static a a() {
        return new a(null);
    }
    
    public final int b() {
        return this.d.b();
    }
    
    public final int c() {
        return this.d.c();
    }
    
    public final String d() {
        return this.b;
    }
    
    public final String e() {
        return this.c;
    }
    
    public final String f() {
        return this.d.d();
    }
    
    public final String g() {
        return this.d.e();
    }
    
    public final ArrayList h() {
        final ArrayList list = new ArrayList();
        list.addAll(this.f);
        return list;
    }
    
    public final List i() {
        return (List)this.e;
    }
    
    public final boolean q() {
        return this.g;
    }
    
    public final boolean r() {
        return this.b != null || this.c != null || this.d.e() != null || this.d.b() != 0 || this.d.c() != 0 || this.a || this.g;
    }
    
    public static class a
    {
        public String a;
        public String b;
        public List c;
        public ArrayList d;
        public boolean e;
        public c.a f;
        
        public zb a() {
            final ArrayList d = this.d;
            boolean b = true;
            final boolean b2 = d != null && !d.isEmpty();
            final List c = this.c;
            final boolean b3 = c != null && !c.isEmpty();
            if (!b2 && !b3) {
                throw new IllegalArgumentException("Details of the products must be provided.");
            }
            if (b2 && b3) {
                throw new IllegalArgumentException("Set SkuDetails or ProductDetailsParams, not both.");
            }
            if (b2) {
                if (this.d.contains(null)) {
                    throw new IllegalArgumentException("SKU cannot be null.");
                }
                if (this.d.size() > 1) {
                    zu0.a(this.d.get(0));
                    throw null;
                }
            }
            else {
                final b b4 = this.c.get(0);
                for (int i = 0; i < this.c.size(); ++i) {
                    final b b5 = this.c.get(i);
                    if (b5 == null) {
                        throw new IllegalArgumentException("ProductDetailsParams cannot be null.");
                    }
                    if (i != 0 && !b5.b().c().equals(b4.b().c()) && !b5.b().c().equals("play_pass_subs")) {
                        throw new IllegalArgumentException("All products should have same ProductType.");
                    }
                }
                final String d2 = b4.b().d();
                for (final b b6 : this.c) {
                    if (!b4.b().c().equals("play_pass_subs") && !b6.b().c().equals("play_pass_subs")) {
                        if (d2.equals(b6.b().d())) {
                            continue;
                        }
                        throw new IllegalArgumentException("All products must have the same package name.");
                    }
                }
            }
            final zb zb = new zb(null);
            if (!b2) {
                if (!b3 || ((b)this.c.get(0)).b().d().isEmpty()) {
                    b = false;
                }
                zb.j(zb, b);
                zb.l(zb, this.a);
                zb.m(zb, this.b);
                zb.p(zb, this.f.a());
                final ArrayList d3 = this.d;
                ArrayList list;
                if (d3 != null) {
                    list = new ArrayList(d3);
                }
                else {
                    list = new ArrayList();
                }
                zb.o(zb, (ArrayList)list);
                zb.k(zb, this.e);
                final List c2 = this.c;
                zzaf zzaf;
                if (c2 != null) {
                    zzaf = com.google.android.gms.internal.play_billing.zzaf.zzj((Collection)c2);
                }
                else {
                    zzaf = com.google.android.gms.internal.play_billing.zzaf.zzk();
                }
                zb.n(zb, zzaf);
                return zb;
            }
            zu0.a(this.d.get(0));
            throw null;
        }
        
        public a b(final String a) {
            this.a = a;
            return this;
        }
        
        public a c(final List c) {
            this.c = new ArrayList(c);
            return this;
        }
    }
    
    public static final class b
    {
        public final t81 a = a.c(a);
        public final String b = a.d(a);
        
        public static a a() {
            return new a(null);
        }
        
        public final t81 b() {
            return this.a;
        }
        
        public final String c() {
            return this.b;
        }
        
        public static class a
        {
            public t81 a;
            public String b;
            
            public b a() {
                zzx.zzc((Object)this.a, (Object)"ProductDetails is required for constructing ProductDetailsParams.");
                zzx.zzc((Object)this.b, (Object)"offerToken is required for constructing ProductDetailsParams.");
                return new b(this, null);
            }
            
            public a b(final t81 a) {
                this.a = a;
                if (a.a() != null) {
                    a.a().getClass();
                    this.b = a.a().b();
                }
                return this;
            }
        }
    }
    
    public static class c
    {
        public String a;
        public String b;
        public int c = 0;
        public int d = 0;
        
        public static a a() {
            return new a(null);
        }
        
        public final int b() {
            return this.c;
        }
        
        public final int c() {
            return this.d;
        }
        
        public final String d() {
            return this.a;
        }
        
        public final String e() {
            return this.b;
        }
        
        public static class a
        {
            public String a;
            public String b;
            public boolean c;
            public int d = 0;
            public int e = 0;
            
            public c a() {
                final boolean b = !TextUtils.isEmpty((CharSequence)this.a) || !TextUtils.isEmpty((CharSequence)null);
                final boolean b2 = true ^ TextUtils.isEmpty((CharSequence)this.b);
                if (b && b2) {
                    throw new IllegalArgumentException("Please provide Old SKU purchase information(token/id) or original external transaction id, not both.");
                }
                if (!this.c && !b && !b2) {
                    throw new IllegalArgumentException("Old SKU purchase information(token/id) or original external transaction id must be provided.");
                }
                final c c = new c(null);
                zb.c.f(c, this.a);
                zb.c.h(c, this.d);
                zb.c.i(c, this.e);
                zb.c.g(c, this.b);
                return c;
            }
        }
    }
}

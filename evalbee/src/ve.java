import android.hardware.Camera$ShutterCallback;
import java.io.OutputStream;
import android.graphics.Rect;
import java.io.ByteArrayOutputStream;
import android.graphics.YuvImage;
import java.util.List;
import android.hardware.Camera$Parameters;
import android.hardware.Camera$AutoFocusCallback;
import android.graphics.SurfaceTexture;
import android.view.TextureView$SurfaceTextureListener;
import android.hardware.Camera$Size;
import android.hardware.Camera$PictureCallback;
import android.hardware.Camera$PreviewCallback;
import com.ekodroid.omrevaluator.components.AutofitPreview;
import android.hardware.Camera;

// 
// Decompiled by Procyon v0.6.0
// 

public class ve implements we
{
    public final boolean a;
    public boolean b;
    public boolean c;
    public Camera d;
    public AutofitPreview e;
    public boolean f;
    public byte[] g;
    public final Camera$PreviewCallback h;
    public int i;
    public byte[] j;
    public boolean k;
    public Camera$PictureCallback l;
    public Camera$Size m;
    public Camera$Size n;
    public xe o;
    public boolean p;
    public final TextureView$SurfaceTextureListener q;
    
    public ve(final AutofitPreview e, final xe o, final boolean a, final int i) {
        this.b = false;
        this.c = false;
        this.f = false;
        this.h = (Camera$PreviewCallback)new Camera$PreviewCallback(this) {
            public final ve a;
            
            public void onPreviewFrame(final byte[] array, final Camera camera) {
                if (ve.c(this.a)) {
                    final ve a = this.a;
                    ve.g(a, a.C(camera.getParameters().getPreviewSize().width, camera.getParameters().getPreviewSize().height, array));
                    ve.d(this.a, false);
                }
                this.a.c = true;
            }
        };
        this.k = false;
        this.l = (Camera$PictureCallback)new Camera$PictureCallback(this) {
            public final ve a;
            
            public void onPictureTaken(final byte[] array, final Camera camera) {
                ve.p(this.a, array);
                final ve a = this.a;
                a.c = false;
                ve.q(a).startPreview();
                ve.s(this.a, true);
            }
        };
        this.m = null;
        this.n = null;
        this.p = false;
        final TextureView$SurfaceTextureListener textureView$SurfaceTextureListener = (TextureView$SurfaceTextureListener)new TextureView$SurfaceTextureListener(this) {
            public final ve a;
            
            public void onSurfaceTextureAvailable(final SurfaceTexture ex, final int n, final int n2) {
                while (true) {
                    try {
                        ve.r(this.a, Camera.open());
                        ve.q(this.a).setPreviewCallback(ve.t(this.a));
                        final Camera$Parameters parameters = ve.q(this.a).getParameters();
                        if (ve.u(this.a)) {
                            final ve a = this.a;
                            ve.w(a, a.B(parameters));
                            final ve a2 = this.a;
                            ve.f(a2, a2.z(parameters, ve.v(a2)));
                        }
                        else {
                            final ve a3 = this.a;
                            ve.w(a3, a3.A(parameters, ve.i(a3)));
                        }
                        parameters.setPreviewSize(ve.v(this.a).width, ve.v(this.a).height);
                        if (ve.e(this.a) != null) {
                            parameters.setPictureSize(ve.e(this.a).width, ve.e(this.a).height);
                        }
                        parameters.setRotation(90);
                        final List supportedFocusModes = parameters.getSupportedFocusModes();
                        if (supportedFocusModes != null && supportedFocusModes.contains("continuous-picture")) {
                            ve.l(this.a, false);
                            parameters.setFocusMode("continuous-picture");
                        }
                        else if (supportedFocusModes != null && supportedFocusModes.contains("auto")) {
                            parameters.setFocusMode("auto");
                            ve.l(this.a, true);
                        }
                        ve.q(this.a).setParameters(parameters);
                        ve.q(this.a).setDisplayOrientation(90);
                        try {
                            ve.m(this.a).a(ve.v(this.a).height, ve.v(this.a).width);
                            ve.q(this.a).setPreviewTexture((SurfaceTexture)ex);
                            ve.q(this.a).startPreview();
                            if (ve.k(this.a)) {
                                ve.q(this.a).autoFocus((Camera$AutoFocusCallback)null);
                            }
                            if (ve.n(this.a) != null) {
                                ve.n(this.a).a(ve.v(this.a), ve.e(this.a));
                            }
                            return;
                        }
                        catch (final Exception ex2) {}
                        System.err.println(ex);
                    }
                    catch (final RuntimeException ex) {
                        continue;
                    }
                    break;
                }
            }
            
            public boolean onSurfaceTextureDestroyed(final SurfaceTexture surfaceTexture) {
                return false;
            }
            
            public void onSurfaceTextureSizeChanged(final SurfaceTexture surfaceTexture, final int n, final int n2) {
            }
            
            public void onSurfaceTextureUpdated(final SurfaceTexture surfaceTexture) {
            }
        };
        this.q = (TextureView$SurfaceTextureListener)textureView$SurfaceTextureListener;
        (this.e = e).setSurfaceTextureListener((TextureView$SurfaceTextureListener)textureView$SurfaceTextureListener);
        this.o = o;
        this.a = a;
        this.i = i;
    }
    
    public static /* synthetic */ boolean c(final ve ve) {
        return ve.f;
    }
    
    public static /* synthetic */ boolean d(final ve ve, final boolean f) {
        return ve.f = f;
    }
    
    public static /* synthetic */ Camera$Size e(final ve ve) {
        return ve.n;
    }
    
    public static /* synthetic */ Camera$Size f(final ve ve, final Camera$Size n) {
        return ve.n = n;
    }
    
    public static /* synthetic */ byte[] g(final ve ve, final byte[] g) {
        return ve.g = g;
    }
    
    public static /* synthetic */ int i(final ve ve) {
        return ve.i;
    }
    
    public static /* synthetic */ boolean k(final ve ve) {
        return ve.p;
    }
    
    public static /* synthetic */ boolean l(final ve ve, final boolean p2) {
        return ve.p = p2;
    }
    
    public static /* synthetic */ AutofitPreview m(final ve ve) {
        return ve.e;
    }
    
    public static /* synthetic */ xe n(final ve ve) {
        return ve.o;
    }
    
    public static /* synthetic */ byte[] p(final ve ve, final byte[] j) {
        return ve.j = j;
    }
    
    public static /* synthetic */ Camera q(final ve ve) {
        return ve.d;
    }
    
    public static /* synthetic */ Camera r(final ve ve, final Camera d) {
        return ve.d = d;
    }
    
    public static /* synthetic */ boolean s(final ve ve, final boolean k) {
        return ve.k = k;
    }
    
    public static /* synthetic */ Camera$PreviewCallback t(final ve ve) {
        return ve.h;
    }
    
    public static /* synthetic */ boolean u(final ve ve) {
        return ve.a;
    }
    
    public static /* synthetic */ Camera$Size v(final ve ve) {
        return ve.m;
    }
    
    public static /* synthetic */ Camera$Size w(final ve ve, final Camera$Size m) {
        return ve.m = m;
    }
    
    public final Camera$Size A(final Camera$Parameters camera$Parameters, final int n) {
        final List supportedPreviewSizes = camera$Parameters.getSupportedPreviewSizes();
        Camera$Size camera$Size = null;
        double n2 = 1000.0;
        int i = 0;
        double n3 = 0.0;
        double n4 = 0.0;
        Camera$Size camera$Size2 = null;
        Camera$Size camera$Size3 = null;
        while (i < supportedPreviewSizes.size()) {
            Camera$Size camera$Size4 = supportedPreviewSizes.get(i);
            final int height = camera$Size4.height;
            final int width = camera$Size4.width;
            final int n5 = height * width;
            Camera$Size camera$Size5 = null;
            double n7 = 0.0;
            Label_0172: {
                if (height < width) {
                    final double n6 = height * 1.0 / n;
                    camera$Size5 = camera$Size;
                    n7 = n2;
                    if (n6 > 32.0) {
                        camera$Size5 = camera$Size;
                        n7 = n2;
                        if (n6 <= n2) {
                            if (camera$Size != null) {
                                camera$Size5 = camera$Size;
                                n7 = n2;
                                if (n5 >= camera$Size.width * camera$Size.height) {
                                    break Label_0172;
                                }
                            }
                            n7 = n6;
                            camera$Size5 = camera$Size4;
                        }
                    }
                }
                else {
                    n7 = n2;
                    camera$Size5 = camera$Size;
                }
            }
            Label_0236: {
                if (height < width && this.D(camera$Size4)) {
                    final double n8 = n5;
                    if (n8 > n3) {
                        n3 = n8;
                        break Label_0236;
                    }
                }
                else {
                    final double n9 = n5;
                    if (n9 > n4) {
                        camera$Size3 = camera$Size4;
                        n4 = n9;
                        camera$Size4 = camera$Size2;
                        break Label_0236;
                    }
                }
                camera$Size4 = camera$Size2;
            }
            ++i;
            camera$Size2 = camera$Size4;
            camera$Size = camera$Size5;
            n2 = n7;
        }
        if (camera$Size != null) {
            return camera$Size;
        }
        if (camera$Size2 != null) {
            return camera$Size2;
        }
        return camera$Size3;
    }
    
    public final Camera$Size B(final Camera$Parameters camera$Parameters) {
        final List supportedPreviewSizes = camera$Parameters.getSupportedPreviewSizes();
        Camera$Size camera$Size = null;
        double n = 0.0;
        int i = 0;
        Camera$Size camera$Size2 = null;
        double n2 = 0.0;
        while (i < supportedPreviewSizes.size()) {
            final Camera$Size camera$Size3 = supportedPreviewSizes.get(i);
            final int n3 = camera$Size3.height * camera$Size3.width;
            Camera$Size camera$Size4;
            Camera$Size camera$Size5;
            double n5;
            double n6;
            if (this.D(camera$Size3)) {
                final double n4 = n3;
                camera$Size4 = camera$Size;
                camera$Size5 = camera$Size2;
                n5 = n2;
                n6 = n;
                if (n4 > n) {
                    n6 = n4;
                    camera$Size4 = camera$Size3;
                    camera$Size5 = camera$Size2;
                    n5 = n2;
                }
            }
            else {
                final double n7 = n3;
                camera$Size4 = camera$Size;
                camera$Size5 = camera$Size2;
                n5 = n2;
                n6 = n;
                if (n7 > n2) {
                    n5 = n7;
                    n6 = n;
                    camera$Size5 = camera$Size3;
                    camera$Size4 = camera$Size;
                }
            }
            ++i;
            camera$Size = camera$Size4;
            camera$Size2 = camera$Size5;
            n2 = n5;
            n = n6;
        }
        if (camera$Size != null) {
            return camera$Size;
        }
        return camera$Size2;
    }
    
    public final byte[] C(final int n, final int n2, final byte[] array) {
        final YuvImage yuvImage = new YuvImage(array, 17, n, n2, (int[])null);
        final ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        yuvImage.compressToJpeg(new Rect(0, 0, n, n2), 100, (OutputStream)byteArrayOutputStream);
        return byteArrayOutputStream.toByteArray();
    }
    
    public final boolean D(final Camera$Size camera$Size) {
        final int height = camera$Size.height;
        final int width = camera$Size.width;
        double n;
        if (height > width) {
            n = height / (double)width;
        }
        else {
            n = width / (double)height;
        }
        return n < 1.4333333333333333 && n > 1.2333333333333332;
    }
    
    @Override
    public void a() {
        final Camera d = this.d;
        if (d != null) {
            d.stopPreview();
            this.d.setPreviewCallback((Camera$PreviewCallback)null);
            this.d.release();
            this.d = null;
        }
    }
    
    @Override
    public void b() {
        final Camera d = this.d;
        if (d != null && this.p) {
            d.cancelAutoFocus();
            this.b = false;
            this.d.autoFocus((Camera$AutoFocusCallback)new Camera$AutoFocusCallback(this) {
                public final ve a;
                
                public void onAutoFocus(final boolean b, final Camera camera) {
                    this.a.b = true;
                }
            });
            while (System.currentTimeMillis() - System.currentTimeMillis() < 3000L) {
                try {
                    Thread.sleep(50L);
                }
                catch (final InterruptedException ex) {
                    ex.printStackTrace();
                }
                if (this.b) {
                    try {
                        Thread.sleep(100L);
                    }
                    catch (final InterruptedException ex2) {
                        ex2.printStackTrace();
                    }
                    break;
                }
            }
        }
    }
    
    @Override
    public byte[] getFrame() {
        synchronized (this) {
            this.f = true;
            while (System.currentTimeMillis() - System.currentTimeMillis() < 2000L) {
                try {
                    Thread.sleep(50L);
                }
                catch (final InterruptedException ex) {
                    ex.printStackTrace();
                }
                if (!this.f) {
                    return this.g;
                }
            }
            return null;
        }
    }
    
    @Override
    public byte[] getImage() {
        synchronized (this) {
            this.k = false;
            if (!this.c) {
                return null;
            }
            this.y();
            while (System.currentTimeMillis() - System.currentTimeMillis() < 4000L) {
                try {
                    Thread.sleep(50L);
                }
                catch (final InterruptedException ex) {
                    ex.printStackTrace();
                }
                if (this.k) {
                    return this.j;
                }
            }
            return null;
        }
    }
    
    public final void y() {
        this.d.takePicture((Camera$ShutterCallback)null, (Camera$PictureCallback)null, this.l);
    }
    
    public final Camera$Size z(final Camera$Parameters camera$Parameters, Camera$Size camera$Size) {
        if (!this.D(camera$Size)) {
            return null;
        }
        final List supportedPictureSizes = camera$Parameters.getSupportedPictureSizes();
        int i = 0;
        Camera$Size camera$Size2 = null;
        Camera$Size camera$Size3 = null;
        while (i < supportedPictureSizes.size()) {
            final Camera$Size camera$Size4 = supportedPictureSizes.get(i);
            final int n = camera$Size4.width * camera$Size4.height;
            Camera$Size camera$Size5 = camera$Size2;
            camera$Size = camera$Size3;
            Label_0155: {
                if (this.D(camera$Size4)) {
                    if (n < 3000000) {
                        if (camera$Size3 != null) {
                            camera$Size5 = camera$Size2;
                            camera$Size = camera$Size3;
                            if (camera$Size3.width * camera$Size3.height >= n) {
                                break Label_0155;
                            }
                        }
                        camera$Size = camera$Size4;
                        camera$Size5 = camera$Size2;
                    }
                    else {
                        if (camera$Size2 != null) {
                            camera$Size5 = camera$Size2;
                            camera$Size = camera$Size3;
                            if (camera$Size2.width * camera$Size2.height <= n) {
                                break Label_0155;
                            }
                        }
                        camera$Size5 = camera$Size4;
                        camera$Size = camera$Size3;
                    }
                }
            }
            ++i;
            camera$Size2 = camera$Size5;
            camera$Size3 = camera$Size;
        }
        if (camera$Size2 != null) {
            return camera$Size2;
        }
        if (camera$Size3 != null) {
            return camera$Size3;
        }
        return null;
    }
}

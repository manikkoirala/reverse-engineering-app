import android.graphics.Shader$TileMode;
import android.graphics.Canvas;
import android.graphics.Bitmap;
import android.graphics.Bitmap$Config;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.ColorFilter;
import android.content.res.ColorStateList;
import android.graphics.drawable.LayerDrawable;
import android.graphics.drawable.Drawable;
import android.content.Context;
import android.graphics.PorterDuffColorFilter;
import android.graphics.PorterDuff$Mode;

// 
// Decompiled by Procyon v0.6.0
// 

public final class s6
{
    public static final PorterDuff$Mode b;
    public static s6 c;
    public je1 a;
    
    static {
        b = PorterDuff$Mode.SRC_IN;
    }
    
    public static /* synthetic */ PorterDuff$Mode a() {
        return s6.b;
    }
    
    public static s6 b() {
        synchronized (s6.class) {
            if (s6.c == null) {
                h();
            }
            return s6.c;
        }
    }
    
    public static PorterDuffColorFilter e(final int n, final PorterDuff$Mode porterDuff$Mode) {
        synchronized (s6.class) {
            return je1.k(n, porterDuff$Mode);
        }
    }
    
    public static void h() {
        synchronized (s6.class) {
            if (s6.c == null) {
                (s6.c = new s6()).a = je1.g();
                s6.c.a.t((je1.c)new je1.c() {
                    public final int[] a = { cb1.R, cb1.P, cb1.a };
                    public final int[] b = { cb1.o, cb1.B, cb1.t, cb1.p, cb1.q, cb1.s, cb1.r };
                    public final int[] c = { cb1.O, cb1.Q, cb1.k, cb1.K, cb1.L, cb1.M, cb1.N };
                    public final int[] d = { cb1.w, cb1.i, cb1.v };
                    public final int[] e = { cb1.J, cb1.S };
                    public final int[] f = { cb1.c, cb1.g, cb1.d, cb1.h };
                    
                    @Override
                    public Drawable a(final je1 je1, final Context context, int n) {
                        if (n == cb1.j) {
                            return (Drawable)new LayerDrawable(new Drawable[] { je1.i(context, cb1.i), je1.i(context, cb1.k) });
                        }
                        if (n == cb1.y) {
                            n = ab1.g;
                        }
                        else if (n == cb1.x) {
                            n = ab1.h;
                        }
                        else {
                            if (n != cb1.z) {
                                return null;
                            }
                            n = ab1.i;
                        }
                        return (Drawable)this.l(je1, context, n);
                    }
                    
                    @Override
                    public ColorStateList b(final Context context, final int n) {
                        if (n == cb1.m) {
                            return g7.a(context, wa1.e);
                        }
                        if (n == cb1.I) {
                            return g7.a(context, wa1.h);
                        }
                        if (n == cb1.H) {
                            return this.k(context);
                        }
                        if (n == cb1.f) {
                            return this.j(context);
                        }
                        if (n == cb1.b) {
                            return this.g(context);
                        }
                        if (n == cb1.e) {
                            return this.i(context);
                        }
                        if (n == cb1.D || n == cb1.E) {
                            return g7.a(context, wa1.g);
                        }
                        if (this.f(this.b, n)) {
                            return pv1.e(context, sa1.u);
                        }
                        if (this.f(this.e, n)) {
                            return g7.a(context, wa1.d);
                        }
                        if (this.f(this.f, n)) {
                            return g7.a(context, wa1.c);
                        }
                        if (n == cb1.A) {
                            return g7.a(context, wa1.f);
                        }
                        return null;
                    }
                    
                    @Override
                    public PorterDuff$Mode c(final int n) {
                        PorterDuff$Mode multiply;
                        if (n == cb1.H) {
                            multiply = PorterDuff$Mode.MULTIPLY;
                        }
                        else {
                            multiply = null;
                        }
                        return multiply;
                    }
                    
                    @Override
                    public boolean d(final Context context, int n, Drawable drawable) {
                        Drawable drawable2;
                        if (n == cb1.C) {
                            final LayerDrawable layerDrawable = (LayerDrawable)drawable;
                            drawable = layerDrawable.findDrawableByLayerId(16908288);
                            n = sa1.u;
                            this.m(drawable, pv1.c(context, n), s6.a());
                            this.m(layerDrawable.findDrawableByLayerId(16908303), pv1.c(context, n), s6.a());
                            drawable = layerDrawable.findDrawableByLayerId(16908301);
                            n = pv1.c(context, sa1.s);
                            drawable2 = drawable;
                        }
                        else {
                            if (n != cb1.y && n != cb1.x && n != cb1.z) {
                                return false;
                            }
                            final LayerDrawable layerDrawable2 = (LayerDrawable)drawable;
                            this.m(layerDrawable2.findDrawableByLayerId(16908288), pv1.b(context, sa1.u), s6.a());
                            drawable = layerDrawable2.findDrawableByLayerId(16908303);
                            n = sa1.s;
                            this.m(drawable, pv1.c(context, n), s6.a());
                            drawable = layerDrawable2.findDrawableByLayerId(16908301);
                            n = pv1.c(context, n);
                            drawable2 = drawable;
                        }
                        this.m(drawable2, n, s6.a());
                        return true;
                    }
                    
                    @Override
                    public boolean e(final Context context, int round, final Drawable drawable) {
                        PorterDuff$Mode porterDuff$Mode = s6.a();
                        int n = 0;
                        boolean b = false;
                        Label_0129: {
                            Label_0029: {
                                if (this.f(this.a, round)) {
                                    n = sa1.u;
                                }
                                else {
                                    if (!this.f(this.c, round)) {
                                        if (this.f(this.d, round)) {
                                            porterDuff$Mode = PorterDuff$Mode.MULTIPLY;
                                        }
                                        else {
                                            if (round == cb1.u) {
                                                round = Math.round(40.8f);
                                                b = true;
                                                n = 16842800;
                                                break Label_0129;
                                            }
                                            if (round != cb1.l) {
                                                n = 0;
                                                b = false;
                                                break Label_0029;
                                            }
                                        }
                                        round = -1;
                                        n = 16842801;
                                        b = true;
                                        break Label_0129;
                                    }
                                    n = sa1.s;
                                }
                                b = true;
                            }
                            porterDuff$Mode = porterDuff$Mode;
                            round = -1;
                        }
                        if (b) {
                            Drawable mutate = drawable;
                            if (fv.a(drawable)) {
                                mutate = drawable.mutate();
                            }
                            mutate.setColorFilter((ColorFilter)s6.e(pv1.c(context, n), porterDuff$Mode));
                            if (round != -1) {
                                mutate.setAlpha(round);
                            }
                            return true;
                        }
                        return false;
                    }
                    
                    public final boolean f(final int[] array, final int n) {
                        for (int length = array.length, i = 0; i < length; ++i) {
                            if (array[i] == n) {
                                return true;
                            }
                        }
                        return false;
                    }
                    
                    public final ColorStateList g(final Context context) {
                        return this.h(context, 0);
                    }
                    
                    public final ColorStateList h(final Context context, final int n) {
                        final int c = pv1.c(context, sa1.t);
                        return new ColorStateList(new int[][] { pv1.b, pv1.e, pv1.c, pv1.i }, new int[] { pv1.b(context, sa1.r), ci.g(c, n), ci.g(c, n), n });
                    }
                    
                    public final ColorStateList i(final Context context) {
                        return this.h(context, pv1.c(context, sa1.q));
                    }
                    
                    public final ColorStateList j(final Context context) {
                        return this.h(context, pv1.c(context, sa1.r));
                    }
                    
                    public final ColorStateList k(final Context context) {
                        final int[][] array = new int[3][];
                        final int[] array2 = new int[3];
                        final int w = sa1.w;
                        final ColorStateList e = pv1.e(context, w);
                        if (e != null && e.isStateful()) {
                            final int[] b = pv1.b;
                            array[0] = b;
                            array2[0] = e.getColorForState(b, 0);
                            array[1] = pv1.f;
                            array2[1] = pv1.c(context, sa1.s);
                            array[2] = pv1.i;
                            array2[2] = e.getDefaultColor();
                        }
                        else {
                            array[0] = pv1.b;
                            array2[0] = pv1.b(context, w);
                            array[1] = pv1.f;
                            array2[1] = pv1.c(context, sa1.s);
                            array[2] = pv1.i;
                            array2[2] = pv1.c(context, w);
                        }
                        return new ColorStateList(array, array2);
                    }
                    
                    public final LayerDrawable l(final je1 je1, final Context context, int dimensionPixelSize) {
                        dimensionPixelSize = context.getResources().getDimensionPixelSize(dimensionPixelSize);
                        final Drawable i = je1.i(context, cb1.F);
                        final Drawable j = je1.i(context, cb1.G);
                        BitmapDrawable bitmapDrawable;
                        BitmapDrawable bitmapDrawable2;
                        if (i instanceof BitmapDrawable && i.getIntrinsicWidth() == dimensionPixelSize && i.getIntrinsicHeight() == dimensionPixelSize) {
                            bitmapDrawable = (BitmapDrawable)i;
                            bitmapDrawable2 = new BitmapDrawable(bitmapDrawable.getBitmap());
                        }
                        else {
                            final Bitmap bitmap = Bitmap.createBitmap(dimensionPixelSize, dimensionPixelSize, Bitmap$Config.ARGB_8888);
                            final Canvas canvas = new Canvas(bitmap);
                            i.setBounds(0, 0, dimensionPixelSize, dimensionPixelSize);
                            i.draw(canvas);
                            bitmapDrawable = new BitmapDrawable(bitmap);
                            bitmapDrawable2 = new BitmapDrawable(bitmap);
                        }
                        bitmapDrawable2.setTileModeX(Shader$TileMode.REPEAT);
                        BitmapDrawable bitmapDrawable3;
                        if (j instanceof BitmapDrawable && j.getIntrinsicWidth() == dimensionPixelSize && j.getIntrinsicHeight() == dimensionPixelSize) {
                            bitmapDrawable3 = (BitmapDrawable)j;
                        }
                        else {
                            final Bitmap bitmap2 = Bitmap.createBitmap(dimensionPixelSize, dimensionPixelSize, Bitmap$Config.ARGB_8888);
                            final Canvas canvas2 = new Canvas(bitmap2);
                            j.setBounds(0, 0, dimensionPixelSize, dimensionPixelSize);
                            j.draw(canvas2);
                            bitmapDrawable3 = new BitmapDrawable(bitmap2);
                        }
                        final LayerDrawable layerDrawable = new LayerDrawable(new Drawable[] { (Drawable)bitmapDrawable, (Drawable)bitmapDrawable3, (Drawable)bitmapDrawable2 });
                        layerDrawable.setId(0, 16908288);
                        layerDrawable.setId(1, 16908303);
                        layerDrawable.setId(2, 16908301);
                        return layerDrawable;
                    }
                    
                    public final void m(final Drawable drawable, final int n, final PorterDuff$Mode porterDuff$Mode) {
                        Drawable mutate = drawable;
                        if (fv.a(drawable)) {
                            mutate = drawable.mutate();
                        }
                        PorterDuff$Mode a;
                        if ((a = porterDuff$Mode) == null) {
                            a = s6.a();
                        }
                        mutate.setColorFilter((ColorFilter)s6.e(n, a));
                    }
                });
            }
        }
    }
    
    public static void i(final Drawable drawable, final rw1 rw1, final int[] array) {
        je1.v(drawable, rw1, array);
    }
    
    public Drawable c(final Context context, final int n) {
        synchronized (this) {
            return this.a.i(context, n);
        }
    }
    
    public Drawable d(final Context context, final int n, final boolean b) {
        synchronized (this) {
            return this.a.j(context, n, b);
        }
    }
    
    public ColorStateList f(final Context context, final int n) {
        synchronized (this) {
            return this.a.l(context, n);
        }
    }
    
    public void g(final Context context) {
        synchronized (this) {
            this.a.r(context);
        }
    }
}

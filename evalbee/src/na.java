// 
// Decompiled by Procyon v0.6.0
// 

public final class na extends kj0
{
    public final String a;
    public final String b;
    
    public na(final String a, final String b) {
        if (a == null) {
            throw new NullPointerException("Null libraryName");
        }
        this.a = a;
        if (b != null) {
            this.b = b;
            return;
        }
        throw new NullPointerException("Null version");
    }
    
    @Override
    public String b() {
        return this.a;
    }
    
    @Override
    public String c() {
        return this.b;
    }
    
    @Override
    public boolean equals(final Object o) {
        boolean b = true;
        if (o == this) {
            return true;
        }
        if (o instanceof kj0) {
            final kj0 kj0 = (kj0)o;
            if (!this.a.equals(kj0.b()) || !this.b.equals(kj0.c())) {
                b = false;
            }
            return b;
        }
        return false;
    }
    
    @Override
    public int hashCode() {
        return (this.a.hashCode() ^ 0xF4243) * 1000003 ^ this.b.hashCode();
    }
    
    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append("LibraryVersion{libraryName=");
        sb.append(this.a);
        sb.append(", version=");
        sb.append(this.b);
        sb.append("}");
        return sb.toString();
    }
}

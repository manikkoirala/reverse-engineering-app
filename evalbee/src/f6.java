import android.os.BaseBundle;
import android.app.LocaleManager;
import android.os.LocaleList;
import android.content.res.Configuration;
import android.view.MenuInflater;
import androidx.appcompat.widget.Toolbar;
import android.window.OnBackInvokedDispatcher;
import android.view.ViewGroup$LayoutParams;
import android.view.View;
import android.os.Bundle;
import android.content.pm.PackageManager$NameNotFoundException;
import android.util.Log;
import android.app.Dialog;
import android.app.Activity;
import android.content.Context;
import java.util.Iterator;
import java.lang.ref.WeakReference;
import java.util.concurrent.Executor;

// 
// Decompiled by Procyon v0.6.0
// 

public abstract class f6
{
    public static w7.a a;
    public static int b;
    public static nl0 c;
    public static nl0 d;
    public static Boolean e;
    public static boolean f;
    public static final s8 g;
    public static final Object h;
    public static final Object i;
    
    static {
        f6.a = new w7.a(new w7.b());
        f6.b = -100;
        f6.c = null;
        f6.d = null;
        f6.e = null;
        f6.f = false;
        g = new s8();
        h = new Object();
        i = new Object();
    }
    
    public static void C(final f6 f6) {
        synchronized (f6.h) {
            D(f6);
        }
    }
    
    public static void D(final f6 f6) {
        synchronized (f6.h) {
            final Iterator iterator = f6.g.iterator();
            while (iterator.hasNext()) {
                final f6 f7 = iterator.next().get();
                if (f7 == f6 || f7 == null) {
                    iterator.remove();
                }
            }
        }
    }
    
    public static void M(final Context context) {
        if (!s(context)) {
            return;
        }
        if (yc.c()) {
            if (!f6.f) {
                f6.a.execute(new e6(context));
            }
            return;
        }
        synchronized (f6.i) {
            final nl0 c = f6.c;
            if (c == null) {
                if (f6.d == null) {
                    f6.d = nl0.b(w7.b(context));
                }
                if (f6.d.e()) {
                    return;
                }
                f6.c = f6.d;
            }
            else if (!c.equals(f6.d)) {
                w7.a(context, (f6.d = f6.c).g());
            }
        }
    }
    
    public static void b(final f6 referent) {
        synchronized (f6.h) {
            D(referent);
            f6.g.add(new WeakReference(referent));
        }
    }
    
    public static f6 f(final Activity activity, final z5 z5) {
        return new g6(activity, z5);
    }
    
    public static f6 g(final Dialog dialog, final z5 z5) {
        return new g6(dialog, z5);
    }
    
    public static nl0 i() {
        if (yc.c()) {
            final Object m = m();
            if (m != null) {
                return nl0.h(f6.b.a(m));
            }
        }
        else {
            final nl0 c = f6.c;
            if (c != null) {
                return c;
            }
        }
        return nl0.d();
    }
    
    public static int k() {
        return f6.b;
    }
    
    public static Object m() {
        final Iterator iterator = f6.g.iterator();
        while (iterator.hasNext()) {
            final f6 f6 = iterator.next().get();
            if (f6 != null) {
                final Context j = f6.j();
                if (j != null) {
                    return j.getSystemService("locale");
                }
                continue;
            }
        }
        return null;
    }
    
    public static nl0 o() {
        return f6.c;
    }
    
    public static boolean s(final Context context) {
        if (f6.e == null) {
            try {
                final Bundle metaData = u7.a(context).metaData;
                if (metaData != null) {
                    f6.e = ((BaseBundle)metaData).getBoolean("autoStoreLocales");
                }
            }
            catch (final PackageManager$NameNotFoundException ex) {
                Log.d("AppCompatDelegate", "Checking for metadata for AppLocalesMetadataHolderService : Service not found");
                f6.e = Boolean.FALSE;
            }
        }
        return f6.e;
    }
    
    public abstract void A();
    
    public abstract void B();
    
    public abstract boolean E(final int p0);
    
    public abstract void F(final int p0);
    
    public abstract void G(final View p0);
    
    public abstract void H(final View p0, final ViewGroup$LayoutParams p1);
    
    public void I(final OnBackInvokedDispatcher onBackInvokedDispatcher) {
    }
    
    public abstract void J(final Toolbar p0);
    
    public abstract void K(final int p0);
    
    public abstract void L(final CharSequence p0);
    
    public abstract void c(final View p0, final ViewGroup$LayoutParams p1);
    
    public void d(final Context context) {
    }
    
    public Context e(final Context context) {
        this.d(context);
        return context;
    }
    
    public abstract View h(final int p0);
    
    public abstract Context j();
    
    public abstract int l();
    
    public abstract MenuInflater n();
    
    public abstract t1 p();
    
    public abstract void q();
    
    public abstract void r();
    
    public abstract void u(final Configuration p0);
    
    public abstract void v(final Bundle p0);
    
    public abstract void w();
    
    public abstract void x(final Bundle p0);
    
    public abstract void y();
    
    public abstract void z(final Bundle p0);
    
    public abstract static class a
    {
        public static LocaleList a(final String s) {
            return LocaleList.forLanguageTags(s);
        }
    }
    
    public abstract static class b
    {
        public static LocaleList a(final Object o) {
            return ((LocaleManager)o).getApplicationLocales();
        }
        
        public static void b(final Object o, final LocaleList applicationLocales) {
            ((LocaleManager)o).setApplicationLocales(applicationLocales);
        }
    }
}

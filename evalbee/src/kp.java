import androidx.datastore.core.SingleProcessDataStore;
import androidx.datastore.core.DataMigrationInitializer;
import java.util.List;

// 
// Decompiled by Procyon v0.6.0
// 

public final class kp
{
    public static final kp a;
    
    static {
        a = new kp();
    }
    
    public final jp a(final nl1 nl1, final sd1 sd1, final List list, final lm lm, final a90 a90) {
        fg0.e((Object)nl1, "serializer");
        fg0.e((Object)list, "migrations");
        fg0.e((Object)lm, "scope");
        fg0.e((Object)a90, "produceFile");
        return new SingleProcessDataStore(a90, nl1, mh.e((Object)DataMigrationInitializer.a.b(list)), new kz0(), lm);
    }
}

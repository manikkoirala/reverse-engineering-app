import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.Semaphore;
import java.util.concurrent.Executor;

// 
// Decompiled by Procyon v0.6.0
// 

public class uj0 implements Executor
{
    public final Executor a;
    public final Semaphore b;
    public final LinkedBlockingQueue c;
    
    public uj0(final Executor a, final int permits) {
        this.c = new LinkedBlockingQueue();
        j71.a(permits > 0, "concurrency must be positive.");
        this.a = a;
        this.b = new Semaphore(permits, true);
    }
    
    public final Runnable b(final Runnable runnable) {
        return new tj0(this, runnable);
    }
    
    public final void d() {
        while (this.b.tryAcquire()) {
            final Runnable runnable = this.c.poll();
            if (runnable == null) {
                this.b.release();
                break;
            }
            this.a.execute(this.b(runnable));
        }
    }
    
    @Override
    public void execute(final Runnable e) {
        this.c.offer(e);
        this.d();
    }
}

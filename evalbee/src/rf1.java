import android.database.Cursor;
import java.io.Closeable;
import java.util.Iterator;
import java.util.List;

// 
// Decompiled by Procyon v0.6.0
// 

public class rf1 extends ts1.a
{
    public static final a g;
    public pp c;
    public final b d;
    public final String e;
    public final String f;
    
    static {
        g = new a(null);
    }
    
    public rf1(final pp c, final b d, final String e, final String f) {
        fg0.e((Object)c, "configuration");
        fg0.e((Object)d, "delegate");
        fg0.e((Object)e, "identityHash");
        fg0.e((Object)f, "legacyHash");
        super(d.a);
        this.c = c;
        this.d = d;
        this.e = e;
        this.f = f;
    }
    
    @Override
    public void b(final ss1 ss1) {
        fg0.e((Object)ss1, "db");
        super.b(ss1);
    }
    
    @Override
    public void d(final ss1 ss1) {
        fg0.e((Object)ss1, "db");
        final boolean a = rf1.g.a(ss1);
        this.d.a(ss1);
        if (!a) {
            final c g = this.d.g(ss1);
            if (!g.a) {
                final StringBuilder sb = new StringBuilder();
                sb.append("Pre-packaged database has an invalid schema: ");
                sb.append(g.b);
                throw new IllegalStateException(sb.toString());
            }
        }
        this.j(ss1);
        this.d.c(ss1);
    }
    
    @Override
    public void e(final ss1 ss1, final int n, final int n2) {
        fg0.e((Object)ss1, "db");
        this.g(ss1, n, n2);
    }
    
    @Override
    public void f(final ss1 ss1) {
        fg0.e((Object)ss1, "db");
        super.f(ss1);
        this.h(ss1);
        this.d.d(ss1);
        this.c = null;
    }
    
    @Override
    public void g(final ss1 ss1, final int i, final int j) {
        fg0.e((Object)ss1, "db");
        final pp c = this.c;
        int n = 0;
        if (c != null) {
            final List d = c.d.d(i, j);
            n = n;
            if (d != null) {
                this.d.f(ss1);
                final Iterator iterator = d.iterator();
                while (iterator.hasNext()) {
                    ((kw0)iterator.next()).a(ss1);
                }
                final c g = this.d.g(ss1);
                if (!g.a) {
                    final StringBuilder sb = new StringBuilder();
                    sb.append("Migration didn't properly handle: ");
                    sb.append(g.b);
                    throw new IllegalStateException(sb.toString());
                }
                this.d.e(ss1);
                this.j(ss1);
                n = 1;
            }
        }
        if (n == 0) {
            final pp c2 = this.c;
            if (c2 == null || c2.a(i, j)) {
                final StringBuilder sb2 = new StringBuilder();
                sb2.append("A migration from ");
                sb2.append(i);
                sb2.append(" to ");
                sb2.append(j);
                sb2.append(" was required but not found. Please provide the necessary Migration path via RoomDatabase.Builder.addMigration(Migration ...) or allow for destructive migrations via one of the RoomDatabase.Builder.fallbackToDestructiveMigration* methods.");
                throw new IllegalStateException(sb2.toString());
            }
            this.d.b(ss1);
            this.d.a(ss1);
        }
    }
    
    public final void h(final ss1 ss1) {
        if (rf1.g.b(ss1)) {
            Object l = ss1.L(new do1("SELECT identity_hash FROM room_master_table WHERE id = 42 LIMIT 1"));
            try {
                String string;
                if (((Cursor)l).moveToFirst()) {
                    string = ((Cursor)l).getString(0);
                }
                else {
                    string = null;
                }
                dh.a((Closeable)l, (Throwable)null);
                if (fg0.a((Object)this.e, (Object)string)) {
                    return;
                }
                if (fg0.a((Object)this.f, (Object)string)) {
                    return;
                }
                l = new StringBuilder();
                ((StringBuilder)l).append("Room cannot verify the data integrity. Looks like you've changed schema but forgot to update the version number. You can simply fix this by increasing the version number. Expected identity hash: ");
                ((StringBuilder)l).append(this.e);
                ((StringBuilder)l).append(", found: ");
                ((StringBuilder)l).append(string);
                throw new IllegalStateException(((StringBuilder)l).toString());
            }
            finally {
                try {}
                finally {
                    dh.a((Closeable)l, (Throwable)ss1);
                }
            }
        }
        final c g = this.d.g(ss1);
        if (!g.a) {
            final StringBuilder sb = new StringBuilder();
            sb.append("Pre-packaged database has an invalid schema: ");
            sb.append(g.b);
            throw new IllegalStateException(sb.toString());
        }
        this.d.e(ss1);
        this.j(ss1);
    }
    
    public final void i(final ss1 ss1) {
        ss1.M("CREATE TABLE IF NOT EXISTS room_master_table (id INTEGER PRIMARY KEY,identity_hash TEXT)");
    }
    
    public final void j(final ss1 ss1) {
        this.i(ss1);
        ss1.M(qf1.a(this.e));
    }
    
    public static final class a
    {
        public final boolean a(final ss1 ss1) {
            fg0.e((Object)ss1, "db");
            final Cursor h0 = ss1.h0("SELECT count(*) FROM sqlite_master WHERE name != 'android_metadata'");
            try {
                final boolean moveToFirst = h0.moveToFirst();
                boolean b = false;
                if (moveToFirst) {
                    final int int1 = h0.getInt(0);
                    b = b;
                    if (int1 == 0) {
                        b = true;
                    }
                }
                dh.a((Closeable)h0, (Throwable)null);
                return b;
            }
            finally {
                try {}
                finally {
                    final Throwable t;
                    dh.a((Closeable)h0, t);
                }
            }
        }
        
        public final boolean b(ss1 h0) {
            fg0.e((Object)h0, "db");
            h0 = (ss1)h0.h0("SELECT 1 FROM sqlite_master WHERE type = 'table' AND name='room_master_table'");
            try {
                final boolean moveToFirst = ((Cursor)h0).moveToFirst();
                boolean b = false;
                if (moveToFirst) {
                    final int int1 = ((Cursor)h0).getInt(0);
                    b = b;
                    if (int1 != 0) {
                        b = true;
                    }
                }
                dh.a((Closeable)h0, (Throwable)null);
                return b;
            }
            finally {
                try {}
                finally {
                    final Throwable t;
                    dh.a((Closeable)h0, t);
                }
            }
        }
    }
    
    public abstract static class b
    {
        public final int a;
        
        public b(final int a) {
            this.a = a;
        }
        
        public abstract void a(final ss1 p0);
        
        public abstract void b(final ss1 p0);
        
        public abstract void c(final ss1 p0);
        
        public abstract void d(final ss1 p0);
        
        public abstract void e(final ss1 p0);
        
        public abstract void f(final ss1 p0);
        
        public abstract c g(final ss1 p0);
    }
    
    public static class c
    {
        public final boolean a;
        public final String b;
        
        public c(final boolean a, final String b) {
            this.a = a;
            this.b = b;
        }
    }
}

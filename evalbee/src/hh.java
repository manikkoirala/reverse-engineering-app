// 
// Decompiled by Procyon v0.6.0
// 

public abstract class hh
{
    public static void a(final Object obj, final Object obj2) {
        if (obj == null) {
            final String value = String.valueOf(obj2);
            final StringBuilder sb = new StringBuilder(value.length() + 24);
            sb.append("null key in entry: null=");
            sb.append(value);
            throw new NullPointerException(sb.toString());
        }
        if (obj2 != null) {
            return;
        }
        final String value2 = String.valueOf(obj);
        final StringBuilder sb2 = new StringBuilder(value2.length() + 26);
        sb2.append("null value in entry: ");
        sb2.append(value2);
        sb2.append("=null");
        throw new NullPointerException(sb2.toString());
    }
    
    public static int b(final int i, final String s) {
        if (i >= 0) {
            return i;
        }
        final StringBuilder sb = new StringBuilder(String.valueOf(s).length() + 40);
        sb.append(s);
        sb.append(" cannot be negative but was: ");
        sb.append(i);
        throw new IllegalArgumentException(sb.toString());
    }
    
    public static long c(final long lng, final String s) {
        if (lng >= 0L) {
            return lng;
        }
        final StringBuilder sb = new StringBuilder(String.valueOf(s).length() + 49);
        sb.append(s);
        sb.append(" cannot be negative but was: ");
        sb.append(lng);
        throw new IllegalArgumentException(sb.toString());
    }
    
    public static void d(final int i, final String s) {
        if (i > 0) {
            return;
        }
        final StringBuilder sb = new StringBuilder(String.valueOf(s).length() + 38);
        sb.append(s);
        sb.append(" must be positive but was: ");
        sb.append(i);
        throw new IllegalArgumentException(sb.toString());
    }
    
    public static void e(final boolean b) {
        i71.y(b, "no calls to next() since the last call to remove()");
    }
}

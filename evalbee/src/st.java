import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeoutException;
import com.google.android.gms.tasks.Tasks;
import java.util.concurrent.TimeUnit;
import android.graphics.Bitmap;
import android.app.NotificationManager;
import android.util.Log;
import java.util.Iterator;
import java.util.List;
import android.app.ActivityManager$RunningAppProcessInfo;
import android.app.ActivityManager;
import android.os.Process;
import android.os.SystemClock;
import com.google.android.gms.common.util.PlatformVersion;
import android.app.KeyguardManager;
import com.google.firebase.messaging.c;
import android.content.Context;
import java.util.concurrent.ExecutorService;

// 
// Decompiled by Procyon v0.6.0
// 

public class st
{
    public final ExecutorService a;
    public final Context b;
    public final c c;
    
    public st(final Context b, final c c, final ExecutorService a) {
        this.a = a;
        this.b = b;
        this.c = c;
    }
    
    public boolean a() {
        if (this.c.a("gcm.n.noui")) {
            return true;
        }
        if (this.b()) {
            return false;
        }
        final ie0 d = this.d();
        final gi.a e = gi.e(this.b, this.c);
        this.e(e.a, d);
        this.c(e);
        return true;
    }
    
    public final boolean b() {
        final boolean inKeyguardRestrictedInputMode = ((KeyguardManager)this.b.getSystemService("keyguard")).inKeyguardRestrictedInputMode();
        final boolean b = false;
        if (inKeyguardRestrictedInputMode) {
            return false;
        }
        if (!PlatformVersion.isAtLeastLollipop()) {
            SystemClock.sleep(10L);
        }
        final int myPid = Process.myPid();
        final List runningAppProcesses = ((ActivityManager)this.b.getSystemService("activity")).getRunningAppProcesses();
        boolean b2 = b;
        if (runningAppProcesses != null) {
            final Iterator iterator = runningAppProcesses.iterator();
            ActivityManager$RunningAppProcessInfo activityManager$RunningAppProcessInfo;
            do {
                b2 = b;
                if (!iterator.hasNext()) {
                    return b2;
                }
                activityManager$RunningAppProcessInfo = (ActivityManager$RunningAppProcessInfo)iterator.next();
            } while (activityManager$RunningAppProcessInfo.pid != myPid);
            b2 = b;
            if (activityManager$RunningAppProcessInfo.importance == 100) {
                b2 = true;
            }
        }
        return b2;
    }
    
    public final void c(final gi.a a) {
        if (Log.isLoggable("FirebaseMessaging", 3)) {
            Log.d("FirebaseMessaging", "Showing notification");
        }
        ((NotificationManager)this.b.getSystemService("notification")).notify(a.b, a.c, a.a.b());
    }
    
    public final ie0 d() {
        final ie0 d = ie0.d(this.c.p("gcm.n.image"));
        if (d != null) {
            d.g(this.a);
        }
        return d;
    }
    
    public final void e(final rz0.e e, final ie0 ie0) {
        if (ie0 == null) {
            return;
        }
        try {
            final Bitmap bitmap = (Bitmap)Tasks.await(ie0.e(), 5L, TimeUnit.SECONDS);
            e.o(bitmap);
            e.x(new rz0.b().i(bitmap).h(null));
        }
        catch (final TimeoutException ex) {
            Log.w("FirebaseMessaging", "Failed to download image in time, showing notification without it");
            ie0.close();
        }
        catch (final InterruptedException ex2) {
            Log.w("FirebaseMessaging", "Interrupted while downloading image, showing notification without it");
            ie0.close();
            Thread.currentThread().interrupt();
        }
        catch (final ExecutionException ex3) {
            final StringBuilder sb = new StringBuilder();
            sb.append("Failed to download image: ");
            sb.append(ex3.getCause());
            Log.w("FirebaseMessaging", sb.toString());
        }
    }
}

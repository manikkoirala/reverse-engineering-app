import android.graphics.drawable.Drawable;
import android.graphics.drawable.Animatable2$AnimationCallback;

// 
// Decompiled by Procyon v0.6.0
// 

public abstract class y4
{
    Animatable2$AnimationCallback mPlatformCallback;
    
    public Animatable2$AnimationCallback getPlatformCallback() {
        if (this.mPlatformCallback == null) {
            this.mPlatformCallback = new Animatable2$AnimationCallback(this) {
                public final y4 a;
                
                public void onAnimationEnd(final Drawable drawable) {
                    this.a.onAnimationEnd(drawable);
                }
                
                public void onAnimationStart(final Drawable drawable) {
                    this.a.onAnimationStart(drawable);
                }
            };
        }
        return this.mPlatformCallback;
    }
    
    public void onAnimationEnd(final Drawable drawable) {
    }
    
    public void onAnimationStart(final Drawable drawable) {
    }
}

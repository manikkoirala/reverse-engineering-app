import com.graphbuilder.math.ExpressionParseException;

// 
// Decompiled by Procyon v0.6.0
// 

public abstract class pz
{
    public static oz a(final hp1 hp1, final hp1 hp2) {
        final hp1 hp3 = new hp1();
        final hp1 hp4 = new hp1();
        while (!hp2.f()) {
            final Object i = hp2.i();
            final Object j = hp1.i();
            final Object k = hp1.i();
            if (i.equals("^")) {
                hp1.b(new u61((oz)j, (oz)k));
            }
            else {
                hp1.b(k);
                hp4.l(i);
                hp3.l(j);
            }
        }
        hp3.l(hp1.k());
        while (!hp4.f()) {
            final Object l = hp4.i();
            final Object m = hp3.i();
            final Object i2 = hp3.i();
            w11 w11;
            if (l.equals("*")) {
                w11 = new cx0((oz)m, (oz)i2);
            }
            else {
                if (!l.equals("/")) {
                    hp3.b(i2);
                    hp2.l(l);
                    hp1.l(m);
                    continue;
                }
                w11 = new xt((oz)m, (oz)i2);
            }
            hp3.b(w11);
        }
        hp1.l(hp3.k());
        while (!hp2.f()) {
            final Object i3 = hp2.i();
            final Object i4 = hp1.i();
            final Object i5 = hp1.i();
            w11 w12;
            if (i3.equals("+")) {
                w12 = new u3((oz)i4, (oz)i5);
            }
            else {
                if (!i3.equals("-")) {
                    final StringBuilder sb = new StringBuilder();
                    sb.append("Unknown operator: ");
                    sb.append(i3);
                    throw new ExpressionParseException(sb.toString(), -1);
                }
                w12 = new yr1((oz)i4, (oz)i5);
            }
            hp1.b(w12);
        }
        return (oz)hp1.k();
    }
    
    public static oz b(String s, final int n) {
        if (s.trim().length() == 0) {
            return null;
        }
        final hp1 hp1 = new hp1();
        final hp1 hp2 = new hp1();
        int i = 0;
        int n2 = 1;
        int char1 = 0;
        int n3 = 0;
    Label_0557_Outer:
        while (i < s.length()) {
            final char char2 = s.charAt(i);
            int char3 = 0;
            int j = 0;
            Label_1442: {
                Label_1434: {
                    if (char2 != ' ' && char2 != '\t') {
                        if (char2 != '\n') {
                            if (n2 != 0) {
                                if (char2 == '(') {
                                    if (n3 != 0) {
                                        throw new ExpressionParseException("Open bracket found after negate.", i);
                                    }
                                    hp2.l("(");
                                }
                                else if (char1 == 0 && (char2 == '+' || char2 == '-')) {
                                    final int n4 = 1;
                                    char3 = i;
                                    j = n2;
                                    char1 = n4;
                                    if (char2 == '-') {
                                        n3 = 1;
                                        char3 = i;
                                        j = n2;
                                        char1 = n4;
                                    }
                                    break Label_1442;
                                }
                                else {
                                    while (true) {
                                        Label_0612: {
                                            if ((char2 < '0' || char2 > '9') && char2 != '.') {
                                                break Label_0612;
                                            }
                                            j = i + 1;
                                            int endIndex;
                                            int n5;
                                            while (true) {
                                                endIndex = j;
                                                char3 = i;
                                                n5 = n3;
                                                if (j >= s.length()) {
                                                    break;
                                                }
                                                char1 = s.charAt(j);
                                                if ((char1 >= 48 && char1 <= 57) || char1 == 46) {
                                                    ++j;
                                                }
                                                else {
                                                    if (char1 != 101) {
                                                        endIndex = j;
                                                        char3 = i;
                                                        n5 = n3;
                                                        if (char1 != 69) {
                                                            break;
                                                        }
                                                    }
                                                    final int index = ++j;
                                                    char1 = i;
                                                    int n6 = n3;
                                                    while (true) {
                                                        Label_0430: {
                                                            if (index >= s.length()) {
                                                                break Label_0430;
                                                            }
                                                            final char char4 = s.charAt(index);
                                                            int n7 = index;
                                                            int n8 = i;
                                                            int n9 = n3;
                                                            if (char4 != '+') {
                                                                n7 = index;
                                                                n8 = i;
                                                                n9 = n3;
                                                                if (char4 != '-') {
                                                                    if (char4 < '0' || char4 > '9') {
                                                                        final StringBuilder sb = new StringBuilder();
                                                                        sb.append("Expected digit, plus sign or minus sign but found: ");
                                                                        sb.append(String.valueOf(char4));
                                                                        throw new ExpressionParseException(sb.toString(), index + n);
                                                                    }
                                                                    n7 = index;
                                                                    n8 = i;
                                                                    n9 = n3;
                                                                }
                                                            }
                                                            j = n7 + 1;
                                                            n6 = n9;
                                                            char1 = n8;
                                                        }
                                                        endIndex = j;
                                                        char3 = char1;
                                                        n5 = n6;
                                                        if (j >= s.length()) {
                                                            break;
                                                        }
                                                        final char char5 = s.charAt(j);
                                                        endIndex = j;
                                                        char3 = char1;
                                                        n5 = n6;
                                                        if (char5 < '0') {
                                                            break;
                                                        }
                                                        int n7 = j;
                                                        int n8 = char1;
                                                        int n9 = n6;
                                                        if (char5 > '9') {
                                                            n5 = n6;
                                                            char3 = char1;
                                                            endIndex = j;
                                                            break;
                                                        }
                                                        continue Label_0557_Outer;
                                                    }
                                                }
                                            }
                                            final Object substring = s.substring(char3, endIndex);
                                            try {
                                                double double1 = Double.parseDouble((String)substring);
                                                if (n5 != 0) {
                                                    double1 = -double1;
                                                }
                                                hp1.l(new w22(double1));
                                                final int n10 = endIndex - 1;
                                                j = 0;
                                                char1 = 0;
                                                n3 = 0;
                                                char3 = n10;
                                                break Label_1442;
                                            }
                                            finally {
                                                s = (String)new StringBuilder();
                                                ((StringBuilder)s).append("Improperly formatted value: ");
                                                ((StringBuilder)s).append((String)substring);
                                                throw new ExpressionParseException(((StringBuilder)s).toString(), char3 + n);
                                            }
                                        }
                                        if (char2 == ',' || char2 == ')' || char2 == '^' || char2 == '*' || char2 == '/' || char2 == '+' || char2 == '-') {
                                            final StringBuilder sb2 = new StringBuilder();
                                            sb2.append("Unexpected character: ");
                                            sb2.append(String.valueOf(char2));
                                            throw new ExpressionParseException(sb2.toString(), i + n);
                                        }
                                        j = i + 1;
                                        char1 = char2;
                                        while (j < s.length()) {
                                            char3 = s.charAt(j);
                                            if ((char1 = char3) == 44 || (char1 = char3) == 32 || (char1 = char3) == 9 || (char1 = char3) == 10 || (char1 = char3) == 40 || (char1 = char3) == 41 || (char1 = char3) == 94 || (char1 = char3) == 42 || (char1 = char3) == 47 || (char1 = char3) == 43 || (char1 = char3) == 45) {
                                                break;
                                            }
                                            ++j;
                                            char1 = char3;
                                        }
                                        if (j < s.length()) {
                                            char3 = j;
                                            int char6 = char1;
                                            while (true) {
                                                if (char6 != 32 && char6 != 9) {
                                                    char1 = char3;
                                                    if (char6 != 10) {
                                                        break;
                                                    }
                                                }
                                                char1 = char3 + 1;
                                                if (char1 == s.length()) {
                                                    break;
                                                }
                                                char6 = s.charAt(char1);
                                                char3 = char1;
                                            }
                                            if (char6 == 40) {
                                                final Object substring = new z80(s.substring(i, j), (boolean)(n3 != 0));
                                                char3 = char1 + 1;
                                                j = 1;
                                                while (j != 0) {
                                                    final int n11 = char1 + 1;
                                                    if (n11 >= s.length()) {
                                                        throw new ExpressionParseException("Missing function close bracket.", i + n);
                                                    }
                                                    final char char7 = s.charAt(n11);
                                                    if (char7 == ')') {
                                                        --j;
                                                        char1 = n11;
                                                    }
                                                    else if (char7 == '(') {
                                                        ++j;
                                                        char1 = n11;
                                                    }
                                                    else {
                                                        char1 = n11;
                                                        if (char7 != ',') {
                                                            continue Label_0557_Outer;
                                                        }
                                                        char1 = n11;
                                                        if (j != 1) {
                                                            continue Label_0557_Outer;
                                                        }
                                                        final oz b = b(s.substring(char3, n11), char3);
                                                        if (b == null) {
                                                            throw new ExpressionParseException("Incomplete function.", char3 + n);
                                                        }
                                                        ((z80)substring).m(b);
                                                        char3 = n11 + 1;
                                                        char1 = n11;
                                                    }
                                                }
                                                final oz b2 = b(s.substring(char3, char1), char3);
                                                if (b2 == null) {
                                                    if (((z80)substring).p() > 0) {
                                                        throw new ExpressionParseException("Incomplete function.", char3 + n);
                                                    }
                                                }
                                                else {
                                                    ((z80)substring).m(b2);
                                                }
                                                hp1.l(substring);
                                            }
                                            else {
                                                hp1.l(new c32(s.substring(i, j), (boolean)(n3 != 0)));
                                                --char1;
                                            }
                                            final int n10 = char1;
                                            continue;
                                        }
                                        hp1.l(new c32(s.substring(i, j), (boolean)(n3 != 0)));
                                        final int n10 = j - 1;
                                        continue;
                                    }
                                }
                            }
                            else {
                                if (char2 == ')') {
                                    final Object substring = new hp1();
                                    final hp1 hp3 = new hp1();
                                    while (!hp2.f()) {
                                        final Object k = hp2.k();
                                        if (k.equals("(")) {
                                            ((bk0)substring).b(hp1.k());
                                            hp1.l(a((hp1)substring, hp3));
                                            break Label_1434;
                                        }
                                        ((bk0)substring).b(hp1.k());
                                        hp3.b(k);
                                    }
                                    throw new ExpressionParseException("Missing open bracket.", i + n);
                                }
                                if (char2 != '^' && char2 != '*' && char2 != '/' && char2 != '+' && char2 != '-') {
                                    final StringBuilder sb3 = new StringBuilder();
                                    sb3.append("Expected operator or close bracket but found: ");
                                    sb3.append(String.valueOf(char2));
                                    throw new ExpressionParseException(sb3.toString(), i + n);
                                }
                                hp2.l(String.valueOf(char2));
                                j = 1;
                                char3 = i;
                                break Label_1442;
                            }
                        }
                    }
                }
                j = n2;
                char3 = i;
            }
            i = char3 + 1;
            n2 = j;
        }
        if (hp1.j() == hp2.j() + 1) {
            return a(hp1, hp2);
        }
        throw new ExpressionParseException("Incomplete expression.", n + s.length());
    }
    
    public static oz c(final String s) {
        if (s != null) {
            return b(s, 0);
        }
        throw new ExpressionParseException("Expression string cannot be null.", -1);
    }
}

import java.security.cert.Certificate;
import java.security.cert.CertificateException;
import java.security.NoSuchAlgorithmException;
import java.security.KeyStoreException;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.security.KeyManagementException;
import java.security.SecureRandom;
import javax.net.ssl.KeyManager;
import javax.net.ssl.SSLContext;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLSession;
import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.TrustManagerFactory;
import java.security.KeyStore;
import java.io.InputStream;
import java.security.cert.CertificateFactory;
import javax.net.ssl.SSLSocketFactory;
import android.content.Context;

// 
// Decompiled by Procyon v0.6.0
// 

public class n52
{
    public Context a;
    public String b;
    
    public n52(final Context a, final String b) {
        this.a = a;
        this.b = b;
    }
    
    public static /* synthetic */ String a(final n52 n52) {
        return n52.b;
    }
    
    public ee1 b() {
        return m52.a(this.a, new pd0(null, this.c()));
    }
    
    public final SSLSocketFactory c() {
        Object inStream = null;
        try {
            final CertificateFactory instance = CertificateFactory.getInstance("X.509");
            inStream = this.a.getResources().openRawResource(2131820547);
            try {
                final Certificate generateCertificate = instance.generateCertificate((InputStream)inStream);
                ((InputStream)inStream).close();
                final KeyStore instance2 = KeyStore.getInstance(KeyStore.getDefaultType());
                instance2.load(null, null);
                instance2.setCertificateEntry("ca", generateCertificate);
                inStream = TrustManagerFactory.getInstance(TrustManagerFactory.getDefaultAlgorithm());
                ((TrustManagerFactory)inStream).init(instance2);
                HttpsURLConnection.setDefaultHostnameVerifier(new HostnameVerifier(this) {
                    public final n52 a;
                    
                    @Override
                    public boolean verify(final String s, final SSLSession sslSession) {
                        return s.equals(n52.a(this.a)) || s.contains("omrevaluator.com") || s.contains("evalbee.com") || s.contains("googleapis.com") || s.contains("crashlytics.com");
                    }
                });
                final SSLContext instance3 = SSLContext.getInstance("TLS");
                instance3.init(null, ((TrustManagerFactory)inStream).getTrustManagers(), null);
                return instance3.getSocketFactory();
            }
            finally {
                try {
                    ((InputStream)inStream).close();
                }
                catch (final KeyManagementException inStream) {}
                catch (final IOException inStream) {}
                catch (final FileNotFoundException inStream) {}
                catch (final KeyStoreException inStream) {}
                catch (final NoSuchAlgorithmException inStream) {}
                catch (final CertificateException ex) {}
            }
        }
        catch (final KeyManagementException ex2) {}
        catch (final IOException ex3) {}
        catch (final FileNotFoundException ex4) {}
        catch (final KeyStoreException ex5) {}
        catch (final NoSuchAlgorithmException ex6) {}
        catch (final CertificateException ex7) {}
        ((Throwable)inStream).printStackTrace();
        return null;
    }
}

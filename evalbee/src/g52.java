import android.graphics.Matrix;
import android.util.Log;
import android.view.View;
import java.lang.reflect.Field;

// 
// Decompiled by Procyon v0.6.0
// 

public abstract class g52
{
    public static Field a;
    public static boolean b;
    
    public abstract void a(final View p0);
    
    public abstract float b(final View p0);
    
    public abstract void c(final View p0);
    
    public abstract void d(final View p0, final int p1, final int p2, final int p3, final int p4);
    
    public abstract void e(final View p0, final float p1);
    
    public void f(final View view, final int n) {
        if (!g52.b) {
            try {
                (g52.a = View.class.getDeclaredField("mViewFlags")).setAccessible(true);
            }
            catch (final NoSuchFieldException ex) {
                Log.i("ViewUtilsBase", "fetchViewFlagsField: ");
            }
            g52.b = true;
        }
        final Field a = g52.a;
        if (a == null) {
            return;
        }
        try {
            g52.a.setInt(view, n | (a.getInt(view) & 0xFFFFFFF3));
        }
        catch (final IllegalAccessException ex2) {}
    }
    
    public abstract void g(final View p0, final Matrix p1);
    
    public abstract void h(final View p0, final Matrix p1);
}

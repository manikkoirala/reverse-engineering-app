import com.google.android.gms.tasks.OnCompleteListener;
import java.util.concurrent.Executor;
import android.util.Log;
import android.os.IBinder;
import com.google.android.gms.tasks.Tasks;
import com.google.android.gms.tasks.TaskCompletionSource;
import com.google.android.gms.tasks.Task;
import android.content.Intent;
import android.os.Binder;
import java.util.concurrent.ExecutorService;
import android.app.Service;

// 
// Decompiled by Procyon v0.6.0
// 

public abstract class ex extends Service
{
    public final ExecutorService a;
    public Binder b;
    public final Object c;
    public int d;
    public int e;
    
    public ex() {
        this.a = j00.d();
        this.c = new Object();
        this.e = 0;
    }
    
    public final void d(final Intent intent) {
        if (intent != null) {
            r52.c(intent);
        }
        synchronized (this.c) {
            final int e = this.e - 1;
            this.e = e;
            if (e == 0) {
                this.k(this.d);
            }
        }
    }
    
    public abstract Intent e(final Intent p0);
    
    public abstract void f(final Intent p0);
    
    public boolean g(final Intent intent) {
        return false;
    }
    
    public final Task j(final Intent intent) {
        if (this.g(intent)) {
            return Tasks.forResult((Object)null);
        }
        final TaskCompletionSource taskCompletionSource = new TaskCompletionSource();
        this.a.execute(new dx(this, intent, taskCompletionSource));
        return taskCompletionSource.getTask();
    }
    
    public boolean k(final int n) {
        return this.stopSelfResult(n);
    }
    
    public final IBinder onBind(final Intent intent) {
        synchronized (this) {
            if (Log.isLoggable("EnhancedIntentService", 3)) {
                Log.d("EnhancedIntentService", "Service received bind request");
            }
            if (this.b == null) {
                this.b = new e82((e82.a)new e82.a(this) {
                    public final ex a;
                    
                    @Override
                    public Task a(final Intent intent) {
                        return this.a.j(intent);
                    }
                });
            }
            return (IBinder)this.b;
        }
    }
    
    public void onDestroy() {
        this.a.shutdown();
        super.onDestroy();
    }
    
    public final int onStartCommand(final Intent intent, final int n, final int d) {
        Object o = this.c;
        synchronized (o) {
            this.d = d;
            ++this.e;
            monitorexit(o);
            o = this.e(intent);
            if (o == null) {
                this.d(intent);
                return 2;
            }
            o = this.j((Intent)o);
            if (((Task)o).isComplete()) {
                this.d(intent);
                return 2;
            }
            ((Task)o).addOnCompleteListener((Executor)new xu0(), (OnCompleteListener)new cx(this, intent));
            return 3;
        }
    }
}

import java.util.Iterator;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import com.google.android.gms.internal.play_billing.zzaf;

// 
// Decompiled by Procyon v0.6.0
// 

public final class ha1
{
    public final zzaf a = ha1.a.c(a);
    
    public static a a() {
        return new a(null);
    }
    
    public final zzaf b() {
        return this.a;
    }
    
    public final String c() {
        return ((List<b>)this.a).get(0).c();
    }
    
    public static class a
    {
        public zzaf a;
        
        public ha1 a() {
            return new ha1(this, null);
        }
        
        public a b(final List list) {
            if (list == null || list.isEmpty()) {
                throw new IllegalArgumentException("Product list cannot be empty.");
            }
            final HashSet set = new HashSet();
            for (final b b : list) {
                if (!"play_pass_subs".equals(b.c())) {
                    set.add(b.c());
                }
            }
            if (set.size() <= 1) {
                this.a = zzaf.zzj((Collection)list);
                return this;
            }
            throw new IllegalArgumentException("All products should be of the same product type.");
        }
    }
    
    public static class b
    {
        public final String a = a.d(a);
        public final String b = a.e(a);
        
        public static a a() {
            return new a(null);
        }
        
        public final String b() {
            return this.a;
        }
        
        public final String c() {
            return this.b;
        }
        
        public static class a
        {
            public String a;
            public String b;
            
            public b a() {
                if ("first_party".equals(this.b)) {
                    throw new IllegalArgumentException("Serialized doc id must be provided for first party products.");
                }
                if (this.a == null) {
                    throw new IllegalArgumentException("Product id must be provided.");
                }
                if (this.b != null) {
                    return new b(this, null);
                }
                throw new IllegalArgumentException("Product type must be provided.");
            }
            
            public a b(final String a) {
                this.a = a;
                return this;
            }
            
            public a c(final String b) {
                this.b = b;
                return this;
            }
        }
    }
}

import java.util.HashMap;
import java.util.Map;

// 
// Decompiled by Procyon v0.6.0
// 

public class y92
{
    public static final String e;
    public final ag1 a;
    public final Map b;
    public final Map c;
    public final Object d;
    
    static {
        e = xl0.i("WorkTimer");
    }
    
    public y92(final ag1 a) {
        this.b = new HashMap();
        this.c = new HashMap();
        this.d = new Object();
        this.a = a;
    }
    
    public void a(final x82 obj, final long n, final a a) {
        synchronized (this.d) {
            final xl0 e = xl0.e();
            final String e2 = y92.e;
            final StringBuilder sb = new StringBuilder();
            sb.append("Starting timer for ");
            sb.append(obj);
            e.a(e2, sb.toString());
            this.b(obj);
            final b b = new b(this, obj);
            this.b.put(obj, b);
            this.c.put(obj, a);
            this.a.b(n, b);
        }
    }
    
    public void b(final x82 obj) {
        synchronized (this.d) {
            if (this.b.remove(obj) != null) {
                final xl0 e = xl0.e();
                final String e2 = y92.e;
                final StringBuilder sb = new StringBuilder();
                sb.append("Stopping timer for ");
                sb.append(obj);
                e.a(e2, sb.toString());
                this.c.remove(obj);
            }
        }
    }
    
    public interface a
    {
        void a(final x82 p0);
    }
    
    public static class b implements Runnable
    {
        public final y92 a;
        public final x82 b;
        
        public b(final y92 a, final x82 b) {
            this.a = a;
            this.b = b;
        }
        
        @Override
        public void run() {
            synchronized (this.a.d) {
                if (this.a.b.remove(this.b) != null) {
                    final a a = this.a.c.remove(this.b);
                    if (a != null) {
                        a.a(this.b);
                    }
                }
                else {
                    xl0.e().a("WrkTimerRunnable", String.format("Timer with %s is already marked as complete.", this.b));
                }
            }
        }
    }
}

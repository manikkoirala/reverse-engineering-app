import com.android.volley.Request;
import java.util.Iterator;
import android.content.pm.PackageManager$NameNotFoundException;
import com.ekodroid.omrevaluator.more.models.Teacher;
import com.ekodroid.omrevaluator.clients.UserProfileClients.models.UserRole;
import com.ekodroid.omrevaluator.clients.UserProfileClients.models.OrgProfile;
import com.google.firebase.auth.FirebaseAuth;
import com.android.volley.VolleyError;
import com.ekodroid.omrevaluator.serializable.SmsAccount;
import com.android.volley.d;
import android.content.SharedPreferences;
import android.content.Context;

// 
// Decompiled by Procyon v0.6.0
// 

public class va0
{
    public ee1 a;
    public y01 b;
    public Context c;
    public String d;
    public SharedPreferences e;
    
    public va0(final Context c, final ee1 a, final y01 b) {
        final StringBuilder sb = new StringBuilder();
        sb.append("https://");
        sb.append(a91.u());
        sb.append(":8759/smscredit");
        this.d = sb.toString();
        this.a = a;
        this.b = b;
        this.c = c;
        this.e = c.getApplicationContext().getSharedPreferences("MyPref", 0);
        this.c();
    }
    
    public static /* synthetic */ Context b(final va0 va0) {
        return va0.c;
    }
    
    public final void c() {
        final hr1 hr1 = new hr1(this, 1, this.d, new d.b(this) {
            public final va0 a;
            
            public void b(final String s) {
                try {
                    this.a.d(new gc0().j(s, SmsAccount.class));
                }
                catch (final Exception ex) {
                    ex.printStackTrace();
                    this.a.d(null);
                }
            }
        }, new d.a(this) {
            public final va0 a;
            
            @Override
            public void a(final VolleyError volleyError) {
                this.a.d(null);
            }
        }) {
            public final va0 w;
            
            @Override
            public byte[] k() {
                Object o = null;
                try {
                    o = b.i(FirebaseAuth.getInstance().e().getEmail(), "emailKey");
                    final OrgProfile instance = OrgProfile.getInstance(va0.b(this.w));
                    Object o2 = o;
                    if (instance != null) {
                        o2 = o;
                        if (instance.getRole() != UserRole.OWNER) {
                            final Iterator<Teacher> iterator = instance.getTeacherList().iterator();
                            while (true) {
                                o2 = o;
                                if (!iterator.hasNext()) {
                                    break;
                                }
                                final Teacher teacher = iterator.next();
                                if (teacher.getUserRole() != UserRole.OWNER) {
                                    continue;
                                }
                                o = b.i(teacher.getEmailId(), "emailKey");
                            }
                        }
                    }
                    o = new SmsAccount((String)o2, 0, 0, this.w.e.getString("user_country", "").toUpperCase(), null);
                    o = new gc0().s(o).getBytes("UTF-8");
                    return (byte[])o;
                }
                catch (final Exception o) {}
                catch (final PackageManager$NameNotFoundException ex) {}
                ((Throwable)o).printStackTrace();
                return null;
            }
            
            @Override
            public String l() {
                return "application/json";
            }
        };
        hr1.L(new wq(20000, 0, 1.0f));
        this.a.a(hr1);
    }
    
    public final void d(final Object o) {
        final y01 b = this.b;
        if (b != null) {
            b.a(o);
        }
    }
}

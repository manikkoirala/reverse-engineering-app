import com.google.protobuf.ByteString;

// 
// Decompiled by Procyon v0.6.0
// 

public class ec implements Comparable
{
    public final ByteString a;
    
    public ec(final ByteString a) {
        this.a = a;
    }
    
    public static ec c(final ByteString byteString) {
        k71.c(byteString, "Provided ByteString must not be null.");
        return new ec(byteString);
    }
    
    public int a(final ec ec) {
        return o22.i(this.a, ec.a);
    }
    
    public ByteString d() {
        return this.a;
    }
    
    @Override
    public boolean equals(final Object o) {
        return o instanceof ec && this.a.equals(((ec)o).a);
    }
    
    @Override
    public int hashCode() {
        return this.a.hashCode();
    }
    
    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append("Blob { bytes=");
        sb.append(o22.y(this.a));
        sb.append(" }");
        return sb.toString();
    }
}

import android.graphics.PorterDuff$Mode;
import android.content.res.ColorStateList;
import android.graphics.drawable.Drawable;
import android.widget.CompoundButton;

// 
// Decompiled by Procyon v0.6.0
// 

public abstract class yj
{
    public static Drawable a(final CompoundButton compoundButton) {
        return b.a(compoundButton);
    }
    
    public static ColorStateList b(final CompoundButton compoundButton) {
        return a.a(compoundButton);
    }
    
    public static PorterDuff$Mode c(final CompoundButton compoundButton) {
        return a.b(compoundButton);
    }
    
    public static void d(final CompoundButton compoundButton, final ColorStateList list) {
        a.c(compoundButton, list);
    }
    
    public static void e(final CompoundButton compoundButton, final PorterDuff$Mode porterDuff$Mode) {
        a.d(compoundButton, porterDuff$Mode);
    }
    
    public abstract static class a
    {
        public static ColorStateList a(final CompoundButton compoundButton) {
            return compoundButton.getButtonTintList();
        }
        
        public static PorterDuff$Mode b(final CompoundButton compoundButton) {
            return compoundButton.getButtonTintMode();
        }
        
        public static void c(final CompoundButton compoundButton, final ColorStateList buttonTintList) {
            compoundButton.setButtonTintList(buttonTintList);
        }
        
        public static void d(final CompoundButton compoundButton, final PorterDuff$Mode buttonTintMode) {
            compoundButton.setButtonTintMode(buttonTintMode);
        }
    }
    
    public abstract static class b
    {
        public static Drawable a(final CompoundButton compoundButton) {
            return compoundButton.getButtonDrawable();
        }
    }
}

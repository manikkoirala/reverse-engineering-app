import java.util.HashMap;
import java.util.Map;

// 
// Decompiled by Procyon v0.6.0
// 

public class or
{
    public static final String e;
    public final ij1 a;
    public final ag1 b;
    public final ch c;
    public final Map d;
    
    static {
        e = xl0.i("DelayedWorkTracker");
    }
    
    public or(final ij1 a, final ag1 b, final ch c) {
        this.a = a;
        this.b = b;
        this.c = c;
        this.d = new HashMap();
    }
    
    public void a(final p92 p2, final long n) {
        final Runnable runnable = this.d.remove(p2.a);
        if (runnable != null) {
            this.b.a(runnable);
        }
        final Runnable runnable2 = new Runnable(this, p2) {
            public final p92 a;
            public final or b;
            
            @Override
            public void run() {
                final xl0 e = xl0.e();
                final String e2 = or.e;
                final StringBuilder sb = new StringBuilder();
                sb.append("Scheduling work ");
                sb.append(this.a.a);
                e.a(e2, sb.toString());
                this.b.a.a(this.a);
            }
        };
        this.d.put(p2.a, runnable2);
        this.b.b(n - this.c.currentTimeMillis(), runnable2);
    }
    
    public void b(final String s) {
        final Runnable runnable = this.d.remove(s);
        if (runnable != null) {
            this.b.a(runnable);
        }
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

public final class rp
{
    public final qp a;
    public final String b;
    public final String c;
    public final boolean d;
    
    public rp(final qp a, final String b, final String c, final boolean d) {
        this.a = a;
        this.b = b;
        this.c = c;
        this.d = d;
    }
    
    public qp a() {
        return this.a;
    }
    
    public String b() {
        return this.c;
    }
    
    public String c() {
        return this.b;
    }
    
    public boolean d() {
        return this.d;
    }
    
    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append("DatabaseInfo(databaseId:");
        sb.append(this.a);
        sb.append(" host:");
        sb.append(this.c);
        sb.append(")");
        return sb.toString();
    }
}

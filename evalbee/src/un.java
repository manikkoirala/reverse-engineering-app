import java.io.IOException;
import java.io.Reader;
import java.io.StringReader;
import java.util.Collections;
import java.util.ArrayList;
import java.util.List;
import android.util.Base64;
import com.google.firebase.crashlytics.internal.model.CrashlyticsReport;
import android.util.JsonReader;
import com.google.firebase.crashlytics.internal.model.a;

// 
// Decompiled by Procyon v0.6.0
// 

public class un
{
    public static final hp a;
    
    static {
        a = new mh0().j(com.google.firebase.crashlytics.internal.model.a.a).k(true).i();
    }
    
    public static CrashlyticsReport.e.d.f A(final JsonReader jsonReader) {
        final CrashlyticsReport.e.d.f.a a = CrashlyticsReport.e.d.f.a();
        jsonReader.beginObject();
        while (jsonReader.hasNext()) {
            final String nextName = jsonReader.nextName();
            nextName.hashCode();
            if (!nextName.equals("assignments")) {
                jsonReader.skipValue();
            }
            else {
                a.b(n(jsonReader, (a)new nn()));
            }
        }
        jsonReader.endObject();
        return a.a();
    }
    
    public static CrashlyticsReport.e.d.a.b.d B(final JsonReader jsonReader) {
        final CrashlyticsReport.e.d.a.b.d.a a = CrashlyticsReport.e.d.a.b.d.a();
        jsonReader.beginObject();
        while (jsonReader.hasNext()) {
            final String nextName = jsonReader.nextName();
            nextName.hashCode();
            final int hashCode = nextName.hashCode();
            int n = -1;
            switch (hashCode) {
                case 3373707: {
                    if (!nextName.equals("name")) {
                        break;
                    }
                    n = 2;
                    break;
                }
                case 3059181: {
                    if (!nextName.equals("code")) {
                        break;
                    }
                    n = 1;
                    break;
                }
                case -1147692044: {
                    if (!nextName.equals("address")) {
                        break;
                    }
                    n = 0;
                    break;
                }
            }
            switch (n) {
                default: {
                    jsonReader.skipValue();
                    continue;
                }
                case 2: {
                    a.d(jsonReader.nextString());
                    continue;
                }
                case 1: {
                    a.c(jsonReader.nextString());
                    continue;
                }
                case 0: {
                    a.b(jsonReader.nextLong());
                    continue;
                }
            }
        }
        jsonReader.endObject();
        return a.a();
    }
    
    public static CrashlyticsReport.e.d.a.b.e C(final JsonReader jsonReader) {
        final CrashlyticsReport.e.d.a.b.e.a a = CrashlyticsReport.e.d.a.b.e.a();
        jsonReader.beginObject();
        while (jsonReader.hasNext()) {
            final String nextName = jsonReader.nextName();
            nextName.hashCode();
            final int hashCode = nextName.hashCode();
            int n = -1;
            switch (hashCode) {
                case 2125650548: {
                    if (!nextName.equals("importance")) {
                        break;
                    }
                    n = 2;
                    break;
                }
                case 3373707: {
                    if (!nextName.equals("name")) {
                        break;
                    }
                    n = 1;
                    break;
                }
                case -1266514778: {
                    if (!nextName.equals("frames")) {
                        break;
                    }
                    n = 0;
                    break;
                }
            }
            switch (n) {
                default: {
                    jsonReader.skipValue();
                    continue;
                }
                case 2: {
                    a.c(jsonReader.nextInt());
                    continue;
                }
                case 1: {
                    a.d(jsonReader.nextString());
                    continue;
                }
                case 0: {
                    a.b(n(jsonReader, (a)new tn()));
                    continue;
                }
            }
        }
        jsonReader.endObject();
        return a.a();
    }
    
    public static CrashlyticsReport.d.b D(final JsonReader jsonReader) {
        final CrashlyticsReport.d.b.a a = CrashlyticsReport.d.b.a();
        jsonReader.beginObject();
        while (jsonReader.hasNext()) {
            final String nextName = jsonReader.nextName();
            nextName.hashCode();
            if (!nextName.equals("filename")) {
                if (!nextName.equals("contents")) {
                    jsonReader.skipValue();
                }
                else {
                    a.b(Base64.decode(jsonReader.nextString(), 2));
                }
            }
            else {
                a.c(jsonReader.nextString());
            }
        }
        jsonReader.endObject();
        return a.a();
    }
    
    public static CrashlyticsReport.d E(final JsonReader jsonReader) {
        final CrashlyticsReport.d.a a = CrashlyticsReport.d.a();
        jsonReader.beginObject();
        while (jsonReader.hasNext()) {
            final String nextName = jsonReader.nextName();
            nextName.hashCode();
            if (!nextName.equals("files")) {
                if (!nextName.equals("orgId")) {
                    jsonReader.skipValue();
                }
                else {
                    a.c(jsonReader.nextString());
                }
            }
            else {
                a.b(n(jsonReader, (a)new mn()));
            }
        }
        jsonReader.endObject();
        return a.a();
    }
    
    public static CrashlyticsReport.e.e F(final JsonReader jsonReader) {
        final CrashlyticsReport.e.e.a a = CrashlyticsReport.e.e.a();
        jsonReader.beginObject();
        while (jsonReader.hasNext()) {
            final String nextName = jsonReader.nextName();
            nextName.hashCode();
            final int hashCode = nextName.hashCode();
            int n = -1;
            switch (hashCode) {
                case 1874684019: {
                    if (!nextName.equals("platform")) {
                        break;
                    }
                    n = 3;
                    break;
                }
                case 351608024: {
                    if (!nextName.equals("version")) {
                        break;
                    }
                    n = 2;
                    break;
                }
                case -293026577: {
                    if (!nextName.equals("jailbroken")) {
                        break;
                    }
                    n = 1;
                    break;
                }
                case -911706486: {
                    if (!nextName.equals("buildVersion")) {
                        break;
                    }
                    n = 0;
                    break;
                }
            }
            switch (n) {
                default: {
                    jsonReader.skipValue();
                    continue;
                }
                case 3: {
                    a.d(jsonReader.nextInt());
                    continue;
                }
                case 2: {
                    a.e(jsonReader.nextString());
                    continue;
                }
                case 1: {
                    a.c(jsonReader.nextBoolean());
                    continue;
                }
                case 0: {
                    a.b(jsonReader.nextString());
                    continue;
                }
            }
        }
        jsonReader.endObject();
        return a.a();
    }
    
    public static CrashlyticsReport.e.d.a.c G(final JsonReader jsonReader) {
        final CrashlyticsReport.e.d.a.c.a a = CrashlyticsReport.e.d.a.c.a();
        jsonReader.beginObject();
        while (jsonReader.hasNext()) {
            final String nextName = jsonReader.nextName();
            nextName.hashCode();
            final int hashCode = nextName.hashCode();
            int n = -1;
            switch (hashCode) {
                case 2125650548: {
                    if (!nextName.equals("importance")) {
                        break;
                    }
                    n = 3;
                    break;
                }
                case 1694598382: {
                    if (!nextName.equals("defaultProcess")) {
                        break;
                    }
                    n = 2;
                    break;
                }
                case 202325402: {
                    if (!nextName.equals("processName")) {
                        break;
                    }
                    n = 1;
                    break;
                }
                case 110987: {
                    if (!nextName.equals("pid")) {
                        break;
                    }
                    n = 0;
                    break;
                }
            }
            switch (n) {
                default: {
                    jsonReader.skipValue();
                    continue;
                }
                case 3: {
                    a.c(jsonReader.nextInt());
                    continue;
                }
                case 2: {
                    a.b(jsonReader.nextBoolean());
                    continue;
                }
                case 1: {
                    a.e(jsonReader.nextString());
                    continue;
                }
                case 0: {
                    a.d(jsonReader.nextInt());
                    continue;
                }
            }
        }
        jsonReader.endObject();
        return a.a();
    }
    
    public static CrashlyticsReport H(final JsonReader jsonReader) {
        final CrashlyticsReport.b b = CrashlyticsReport.b();
        jsonReader.beginObject();
        while (jsonReader.hasNext()) {
            final String nextName = jsonReader.nextName();
            nextName.hashCode();
            final int hashCode = nextName.hashCode();
            int n = -1;
            switch (hashCode) {
                case 1984987798: {
                    if (!nextName.equals("session")) {
                        break;
                    }
                    n = 10;
                    break;
                }
                case 1975623094: {
                    if (!nextName.equals("displayVersion")) {
                        break;
                    }
                    n = 9;
                    break;
                }
                case 1874684019: {
                    if (!nextName.equals("platform")) {
                        break;
                    }
                    n = 8;
                    break;
                }
                case 1047652060: {
                    if (!nextName.equals("firebaseInstallationId")) {
                        break;
                    }
                    n = 7;
                    break;
                }
                case 719853845: {
                    if (!nextName.equals("installationUuid")) {
                        break;
                    }
                    n = 6;
                    break;
                }
                case 344431858: {
                    if (!nextName.equals("gmpAppId")) {
                        break;
                    }
                    n = 5;
                    break;
                }
                case -911706486: {
                    if (!nextName.equals("buildVersion")) {
                        break;
                    }
                    n = 4;
                    break;
                }
                case -1375141843: {
                    if (!nextName.equals("appExitInfo")) {
                        break;
                    }
                    n = 3;
                    break;
                }
                case -1907185581: {
                    if (!nextName.equals("appQualitySessionId")) {
                        break;
                    }
                    n = 2;
                    break;
                }
                case -1962630338: {
                    if (!nextName.equals("sdkVersion")) {
                        break;
                    }
                    n = 1;
                    break;
                }
                case -2118372775: {
                    if (!nextName.equals("ndkPayload")) {
                        break;
                    }
                    n = 0;
                    break;
                }
            }
            switch (n) {
                default: {
                    jsonReader.skipValue();
                    continue;
                }
                case 10: {
                    b.l(J(jsonReader));
                    continue;
                }
                case 9: {
                    b.e(jsonReader.nextString());
                    continue;
                }
                case 8: {
                    b.j(jsonReader.nextInt());
                    continue;
                }
                case 7: {
                    b.f(jsonReader.nextString());
                    continue;
                }
                case 6: {
                    b.h(jsonReader.nextString());
                    continue;
                }
                case 5: {
                    b.g(jsonReader.nextString());
                    continue;
                }
                case 4: {
                    b.d(jsonReader.nextString());
                    continue;
                }
                case 3: {
                    b.b(m(jsonReader));
                    continue;
                }
                case 2: {
                    b.c(jsonReader.nextString());
                    continue;
                }
                case 1: {
                    b.k(jsonReader.nextString());
                    continue;
                }
                case 0: {
                    b.i(E(jsonReader));
                    continue;
                }
            }
        }
        jsonReader.endObject();
        return b.a();
    }
    
    public static CrashlyticsReport.e.d.e.b I(final JsonReader jsonReader) {
        final CrashlyticsReport.e.d.e.b.a a = CrashlyticsReport.e.d.e.b.a();
        jsonReader.beginObject();
        while (jsonReader.hasNext()) {
            final String nextName = jsonReader.nextName();
            nextName.hashCode();
            if (!nextName.equals("variantId")) {
                if (!nextName.equals("rolloutId")) {
                    jsonReader.skipValue();
                }
                else {
                    a.b(jsonReader.nextString());
                }
            }
            else {
                a.c(jsonReader.nextString());
            }
        }
        jsonReader.endObject();
        return a.a();
    }
    
    public static CrashlyticsReport.e J(final JsonReader jsonReader) {
        final CrashlyticsReport.e.b a = CrashlyticsReport.e.a();
        jsonReader.beginObject();
        while (jsonReader.hasNext()) {
            final String nextName = jsonReader.nextName();
            nextName.hashCode();
            final int hashCode = nextName.hashCode();
            int n = -1;
            switch (hashCode) {
                case 2047016109: {
                    if (!nextName.equals("generatorType")) {
                        break;
                    }
                    n = 11;
                    break;
                }
                case 1025385094: {
                    if (!nextName.equals("crashed")) {
                        break;
                    }
                    n = 10;
                    break;
                }
                case 286956243: {
                    if (!nextName.equals("generator")) {
                        break;
                    }
                    n = 9;
                    break;
                }
                case 3599307: {
                    if (!nextName.equals("user")) {
                        break;
                    }
                    n = 8;
                    break;
                }
                case 96801: {
                    if (!nextName.equals("app")) {
                        break;
                    }
                    n = 7;
                    break;
                }
                case 3556: {
                    if (!nextName.equals("os")) {
                        break;
                    }
                    n = 6;
                    break;
                }
                case -1291329255: {
                    if (!nextName.equals("events")) {
                        break;
                    }
                    n = 5;
                    break;
                }
                case -1335157162: {
                    if (!nextName.equals("device")) {
                        break;
                    }
                    n = 4;
                    break;
                }
                case -1606742899: {
                    if (!nextName.equals("endedAt")) {
                        break;
                    }
                    n = 3;
                    break;
                }
                case -1618432855: {
                    if (!nextName.equals("identifier")) {
                        break;
                    }
                    n = 2;
                    break;
                }
                case -1907185581: {
                    if (!nextName.equals("appQualitySessionId")) {
                        break;
                    }
                    n = 1;
                    break;
                }
                case -2128794476: {
                    if (!nextName.equals("startedAt")) {
                        break;
                    }
                    n = 0;
                    break;
                }
            }
            switch (n) {
                default: {
                    jsonReader.skipValue();
                    continue;
                }
                case 11: {
                    a.i(jsonReader.nextInt());
                    continue;
                }
                case 10: {
                    a.d(jsonReader.nextBoolean());
                    continue;
                }
                case 9: {
                    a.h(jsonReader.nextString());
                    continue;
                }
                case 8: {
                    a.n(K(jsonReader));
                    continue;
                }
                case 7: {
                    a.b(l(jsonReader));
                    continue;
                }
                case 6: {
                    a.l(F(jsonReader));
                    continue;
                }
                case 5: {
                    a.g(n(jsonReader, (a)new ln()));
                    continue;
                }
                case 4: {
                    a.e(q(jsonReader));
                    continue;
                }
                case 3: {
                    a.f(jsonReader.nextLong());
                    continue;
                }
                case 2: {
                    a.k(Base64.decode(jsonReader.nextString(), 2));
                    continue;
                }
                case 1: {
                    a.c(jsonReader.nextString());
                    continue;
                }
                case 0: {
                    a.m(jsonReader.nextLong());
                    continue;
                }
            }
        }
        jsonReader.endObject();
        return a.a();
    }
    
    public static CrashlyticsReport.e.f K(final JsonReader jsonReader) {
        final CrashlyticsReport.e.f.a a = CrashlyticsReport.e.f.a();
        jsonReader.beginObject();
        while (jsonReader.hasNext()) {
            if (jsonReader.nextName().equals("identifier")) {
                a.b(jsonReader.nextString());
            }
            else {
                jsonReader.skipValue();
            }
        }
        jsonReader.endObject();
        return a.a();
    }
    
    public static CrashlyticsReport.e.a l(final JsonReader jsonReader) {
        final CrashlyticsReport.e.a.a a = CrashlyticsReport.e.a.a();
        jsonReader.beginObject();
        while (jsonReader.hasNext()) {
            final String nextName = jsonReader.nextName();
            nextName.hashCode();
            final int hashCode = nextName.hashCode();
            int n = -1;
            switch (hashCode) {
                case 1975623094: {
                    if (!nextName.equals("displayVersion")) {
                        break;
                    }
                    n = 5;
                    break;
                }
                case 719853845: {
                    if (!nextName.equals("installationUuid")) {
                        break;
                    }
                    n = 4;
                    break;
                }
                case 351608024: {
                    if (!nextName.equals("version")) {
                        break;
                    }
                    n = 3;
                    break;
                }
                case 213652010: {
                    if (!nextName.equals("developmentPlatformVersion")) {
                        break;
                    }
                    n = 2;
                    break;
                }
                case -519438642: {
                    if (!nextName.equals("developmentPlatform")) {
                        break;
                    }
                    n = 1;
                    break;
                }
                case -1618432855: {
                    if (!nextName.equals("identifier")) {
                        break;
                    }
                    n = 0;
                    break;
                }
            }
            switch (n) {
                default: {
                    jsonReader.skipValue();
                    continue;
                }
                case 5: {
                    a.d(jsonReader.nextString());
                    continue;
                }
                case 4: {
                    a.f(jsonReader.nextString());
                    continue;
                }
                case 3: {
                    a.g(jsonReader.nextString());
                    continue;
                }
                case 2: {
                    a.c(jsonReader.nextString());
                    continue;
                }
                case 1: {
                    a.b(jsonReader.nextString());
                    continue;
                }
                case 0: {
                    a.e(jsonReader.nextString());
                    continue;
                }
            }
        }
        jsonReader.endObject();
        return a.a();
    }
    
    public static CrashlyticsReport.a m(final JsonReader jsonReader) {
        final CrashlyticsReport.a.b a = CrashlyticsReport.a.a();
        jsonReader.beginObject();
        while (jsonReader.hasNext()) {
            final String nextName = jsonReader.nextName();
            nextName.hashCode();
            final int hashCode = nextName.hashCode();
            int n = -1;
            switch (hashCode) {
                case 2125650548: {
                    if (!nextName.equals("importance")) {
                        break;
                    }
                    n = 8;
                    break;
                }
                case 723857505: {
                    if (!nextName.equals("traceFile")) {
                        break;
                    }
                    n = 7;
                    break;
                }
                case 722137681: {
                    if (!nextName.equals("reasonCode")) {
                        break;
                    }
                    n = 6;
                    break;
                }
                case 202325402: {
                    if (!nextName.equals("processName")) {
                        break;
                    }
                    n = 5;
                    break;
                }
                case 55126294: {
                    if (!nextName.equals("timestamp")) {
                        break;
                    }
                    n = 4;
                    break;
                }
                case 113234: {
                    if (!nextName.equals("rss")) {
                        break;
                    }
                    n = 3;
                    break;
                }
                case 111312: {
                    if (!nextName.equals("pss")) {
                        break;
                    }
                    n = 2;
                    break;
                }
                case 110987: {
                    if (!nextName.equals("pid")) {
                        break;
                    }
                    n = 1;
                    break;
                }
                case -1516200806: {
                    if (!nextName.equals("buildIdMappingForArch")) {
                        break;
                    }
                    n = 0;
                    break;
                }
            }
            switch (n) {
                default: {
                    jsonReader.skipValue();
                    continue;
                }
                case 8: {
                    a.c(jsonReader.nextInt());
                    continue;
                }
                case 7: {
                    a.j(jsonReader.nextString());
                    continue;
                }
                case 6: {
                    a.g(jsonReader.nextInt());
                    continue;
                }
                case 5: {
                    a.e(jsonReader.nextString());
                    continue;
                }
                case 4: {
                    a.i(jsonReader.nextLong());
                    continue;
                }
                case 3: {
                    a.h(jsonReader.nextLong());
                    continue;
                }
                case 2: {
                    a.f(jsonReader.nextLong());
                    continue;
                }
                case 1: {
                    a.d(jsonReader.nextInt());
                    continue;
                }
                case 0: {
                    a.b(n(jsonReader, (a)new qn()));
                    continue;
                }
            }
        }
        jsonReader.endObject();
        return a.a();
    }
    
    public static List n(final JsonReader jsonReader, final a a) {
        final ArrayList list = new ArrayList();
        jsonReader.beginArray();
        while (jsonReader.hasNext()) {
            list.add(a.a(jsonReader));
        }
        jsonReader.endArray();
        return Collections.unmodifiableList((List<?>)list);
    }
    
    public static CrashlyticsReport.a.a o(final JsonReader jsonReader) {
        final CrashlyticsReport.a.a.a a = CrashlyticsReport.a.a.a();
        jsonReader.beginObject();
        while (jsonReader.hasNext()) {
            final String nextName = jsonReader.nextName();
            nextName.hashCode();
            final int hashCode = nextName.hashCode();
            int n = -1;
            switch (hashCode) {
                case 230943785: {
                    if (!nextName.equals("buildId")) {
                        break;
                    }
                    n = 2;
                    break;
                }
                case 3002454: {
                    if (!nextName.equals("arch")) {
                        break;
                    }
                    n = 1;
                    break;
                }
                case -609862170: {
                    if (!nextName.equals("libraryName")) {
                        break;
                    }
                    n = 0;
                    break;
                }
            }
            switch (n) {
                default: {
                    jsonReader.skipValue();
                    continue;
                }
                case 2: {
                    a.c(jsonReader.nextString());
                    continue;
                }
                case 1: {
                    a.b(jsonReader.nextString());
                    continue;
                }
                case 0: {
                    a.d(jsonReader.nextString());
                    continue;
                }
            }
        }
        jsonReader.endObject();
        return a.a();
    }
    
    public static CrashlyticsReport.c p(final JsonReader jsonReader) {
        final CrashlyticsReport.c.a a = CrashlyticsReport.c.a();
        jsonReader.beginObject();
        while (jsonReader.hasNext()) {
            final String nextName = jsonReader.nextName();
            nextName.hashCode();
            if (!nextName.equals("key")) {
                if (!nextName.equals("value")) {
                    jsonReader.skipValue();
                }
                else {
                    a.c(jsonReader.nextString());
                }
            }
            else {
                a.b(jsonReader.nextString());
            }
        }
        jsonReader.endObject();
        return a.a();
    }
    
    public static CrashlyticsReport.e.c q(final JsonReader jsonReader) {
        final CrashlyticsReport.e.c.a a = CrashlyticsReport.e.c.a();
        jsonReader.beginObject();
        while (jsonReader.hasNext()) {
            final String nextName = jsonReader.nextName();
            nextName.hashCode();
            final int hashCode = nextName.hashCode();
            int n = -1;
            switch (hashCode) {
                case 2078953423: {
                    if (!nextName.equals("modelClass")) {
                        break;
                    }
                    n = 8;
                    break;
                }
                case 109757585: {
                    if (!nextName.equals("state")) {
                        break;
                    }
                    n = 7;
                    break;
                }
                case 104069929: {
                    if (!nextName.equals("model")) {
                        break;
                    }
                    n = 6;
                    break;
                }
                case 94848180: {
                    if (!nextName.equals("cores")) {
                        break;
                    }
                    n = 5;
                    break;
                }
                case 81784169: {
                    if (!nextName.equals("diskSpace")) {
                        break;
                    }
                    n = 4;
                    break;
                }
                case 3002454: {
                    if (!nextName.equals("arch")) {
                        break;
                    }
                    n = 3;
                    break;
                }
                case 112670: {
                    if (!nextName.equals("ram")) {
                        break;
                    }
                    n = 2;
                    break;
                }
                case -1969347631: {
                    if (!nextName.equals("manufacturer")) {
                        break;
                    }
                    n = 1;
                    break;
                }
                case -1981332476: {
                    if (!nextName.equals("simulator")) {
                        break;
                    }
                    n = 0;
                    break;
                }
            }
            switch (n) {
                default: {
                    jsonReader.skipValue();
                    continue;
                }
                case 8: {
                    a.g(jsonReader.nextString());
                    continue;
                }
                case 7: {
                    a.j(jsonReader.nextInt());
                    continue;
                }
                case 6: {
                    a.f(jsonReader.nextString());
                    continue;
                }
                case 5: {
                    a.c(jsonReader.nextInt());
                    continue;
                }
                case 4: {
                    a.d(jsonReader.nextLong());
                    continue;
                }
                case 3: {
                    a.b(jsonReader.nextInt());
                    continue;
                }
                case 2: {
                    a.h(jsonReader.nextLong());
                    continue;
                }
                case 1: {
                    a.e(jsonReader.nextString());
                    continue;
                }
                case 0: {
                    a.i(jsonReader.nextBoolean());
                    continue;
                }
            }
        }
        jsonReader.endObject();
        return a.a();
    }
    
    public static CrashlyticsReport.e.d r(final JsonReader jsonReader) {
        final CrashlyticsReport.e.d.b a = CrashlyticsReport.e.d.a();
        jsonReader.beginObject();
        while (jsonReader.hasNext()) {
            final String nextName = jsonReader.nextName();
            nextName.hashCode();
            final int hashCode = nextName.hashCode();
            int n = -1;
            switch (hashCode) {
                case 55126294: {
                    if (!nextName.equals("timestamp")) {
                        break;
                    }
                    n = 5;
                    break;
                }
                case 3575610: {
                    if (!nextName.equals("type")) {
                        break;
                    }
                    n = 4;
                    break;
                }
                case 107332: {
                    if (!nextName.equals("log")) {
                        break;
                    }
                    n = 3;
                    break;
                }
                case 96801: {
                    if (!nextName.equals("app")) {
                        break;
                    }
                    n = 2;
                    break;
                }
                case -259312414: {
                    if (!nextName.equals("rollouts")) {
                        break;
                    }
                    n = 1;
                    break;
                }
                case -1335157162: {
                    if (!nextName.equals("device")) {
                        break;
                    }
                    n = 0;
                    break;
                }
            }
            switch (n) {
                default: {
                    jsonReader.skipValue();
                    continue;
                }
                case 5: {
                    a.f(jsonReader.nextLong());
                    continue;
                }
                case 4: {
                    a.g(jsonReader.nextString());
                    continue;
                }
                case 3: {
                    a.d(y(jsonReader));
                    continue;
                }
                case 2: {
                    a.b(s(jsonReader));
                    continue;
                }
                case 1: {
                    a.e(A(jsonReader));
                    continue;
                }
                case 0: {
                    a.c(u(jsonReader));
                    continue;
                }
            }
        }
        jsonReader.endObject();
        return a.a();
    }
    
    public static CrashlyticsReport.e.d.a s(final JsonReader jsonReader) {
        final CrashlyticsReport.e.d.a.a a = CrashlyticsReport.e.d.a.a();
        jsonReader.beginObject();
        while (jsonReader.hasNext()) {
            final String nextName = jsonReader.nextName();
            nextName.hashCode();
            final int hashCode = nextName.hashCode();
            int n = -1;
            switch (hashCode) {
                case 1847730860: {
                    if (!nextName.equals("currentProcessDetails")) {
                        break;
                    }
                    n = 6;
                    break;
                }
                case 928737948: {
                    if (!nextName.equals("uiOrientation")) {
                        break;
                    }
                    n = 5;
                    break;
                }
                case 555169704: {
                    if (!nextName.equals("customAttributes")) {
                        break;
                    }
                    n = 4;
                    break;
                }
                case -80231855: {
                    if (!nextName.equals("internalKeys")) {
                        break;
                    }
                    n = 3;
                    break;
                }
                case -1090974952: {
                    if (!nextName.equals("execution")) {
                        break;
                    }
                    n = 2;
                    break;
                }
                case -1332194002: {
                    if (!nextName.equals("background")) {
                        break;
                    }
                    n = 1;
                    break;
                }
                case -1405314732: {
                    if (!nextName.equals("appProcessDetails")) {
                        break;
                    }
                    n = 0;
                    break;
                }
            }
            switch (n) {
                default: {
                    jsonReader.skipValue();
                    continue;
                }
                case 6: {
                    a.d(G(jsonReader));
                    continue;
                }
                case 5: {
                    a.h(jsonReader.nextInt());
                    continue;
                }
                case 4: {
                    a.e(n(jsonReader, (a)new on()));
                    continue;
                }
                case 3: {
                    a.g(n(jsonReader, (a)new on()));
                    continue;
                }
                case 2: {
                    a.f(v(jsonReader));
                    continue;
                }
                case 1: {
                    a.c(jsonReader.nextBoolean());
                    continue;
                }
                case 0: {
                    a.b(n(jsonReader, (a)new pn()));
                    continue;
                }
            }
        }
        jsonReader.endObject();
        return a.a();
    }
    
    public static CrashlyticsReport.e.d.a.b.a t(final JsonReader jsonReader) {
        final CrashlyticsReport.e.d.a.b.a.a a = CrashlyticsReport.e.d.a.b.a.a();
        jsonReader.beginObject();
        while (jsonReader.hasNext()) {
            final String nextName = jsonReader.nextName();
            nextName.hashCode();
            final int hashCode = nextName.hashCode();
            int n = -1;
            switch (hashCode) {
                case 1153765347: {
                    if (!nextName.equals("baseAddress")) {
                        break;
                    }
                    n = 3;
                    break;
                }
                case 3601339: {
                    if (!nextName.equals("uuid")) {
                        break;
                    }
                    n = 2;
                    break;
                }
                case 3530753: {
                    if (!nextName.equals("size")) {
                        break;
                    }
                    n = 1;
                    break;
                }
                case 3373707: {
                    if (!nextName.equals("name")) {
                        break;
                    }
                    n = 0;
                    break;
                }
            }
            switch (n) {
                default: {
                    jsonReader.skipValue();
                    continue;
                }
                case 3: {
                    a.b(jsonReader.nextLong());
                    continue;
                }
                case 2: {
                    a.f(Base64.decode(jsonReader.nextString(), 2));
                    continue;
                }
                case 1: {
                    a.d(jsonReader.nextLong());
                    continue;
                }
                case 0: {
                    a.c(jsonReader.nextString());
                    continue;
                }
            }
        }
        jsonReader.endObject();
        return a.a();
    }
    
    public static CrashlyticsReport.e.d.c u(final JsonReader jsonReader) {
        final CrashlyticsReport.e.d.c.a a = CrashlyticsReport.e.d.c.a();
        jsonReader.beginObject();
        while (jsonReader.hasNext()) {
            final String nextName = jsonReader.nextName();
            nextName.hashCode();
            final int hashCode = nextName.hashCode();
            int n = -1;
            switch (hashCode) {
                case 1516795582: {
                    if (!nextName.equals("proximityOn")) {
                        break;
                    }
                    n = 5;
                    break;
                }
                case 976541947: {
                    if (!nextName.equals("ramUsed")) {
                        break;
                    }
                    n = 4;
                    break;
                }
                case 279795450: {
                    if (!nextName.equals("diskUsed")) {
                        break;
                    }
                    n = 3;
                    break;
                }
                case -1439500848: {
                    if (!nextName.equals("orientation")) {
                        break;
                    }
                    n = 2;
                    break;
                }
                case -1455558134: {
                    if (!nextName.equals("batteryVelocity")) {
                        break;
                    }
                    n = 1;
                    break;
                }
                case -1708606089: {
                    if (!nextName.equals("batteryLevel")) {
                        break;
                    }
                    n = 0;
                    break;
                }
            }
            switch (n) {
                default: {
                    jsonReader.skipValue();
                    continue;
                }
                case 5: {
                    a.f(jsonReader.nextBoolean());
                    continue;
                }
                case 4: {
                    a.g(jsonReader.nextLong());
                    continue;
                }
                case 3: {
                    a.d(jsonReader.nextLong());
                    continue;
                }
                case 2: {
                    a.e(jsonReader.nextInt());
                    continue;
                }
                case 1: {
                    a.c(jsonReader.nextInt());
                    continue;
                }
                case 0: {
                    a.b(jsonReader.nextDouble());
                    continue;
                }
            }
        }
        jsonReader.endObject();
        return a.a();
    }
    
    public static CrashlyticsReport.e.d.a.b v(final JsonReader jsonReader) {
        final CrashlyticsReport.e.d.a.b.b a = CrashlyticsReport.e.d.a.b.a();
        jsonReader.beginObject();
        while (jsonReader.hasNext()) {
            final String nextName = jsonReader.nextName();
            nextName.hashCode();
            final int hashCode = nextName.hashCode();
            int n = -1;
            switch (hashCode) {
                case 1481625679: {
                    if (!nextName.equals("exception")) {
                        break;
                    }
                    n = 4;
                    break;
                }
                case 937615455: {
                    if (!nextName.equals("binaries")) {
                        break;
                    }
                    n = 3;
                    break;
                }
                case -902467928: {
                    if (!nextName.equals("signal")) {
                        break;
                    }
                    n = 2;
                    break;
                }
                case -1337936983: {
                    if (!nextName.equals("threads")) {
                        break;
                    }
                    n = 1;
                    break;
                }
                case -1375141843: {
                    if (!nextName.equals("appExitInfo")) {
                        break;
                    }
                    n = 0;
                    break;
                }
            }
            switch (n) {
                default: {
                    jsonReader.skipValue();
                    continue;
                }
                case 4: {
                    a.d(w(jsonReader));
                    continue;
                }
                case 3: {
                    a.c(n(jsonReader, (a)new sn()));
                    continue;
                }
                case 2: {
                    a.e(B(jsonReader));
                    continue;
                }
                case 1: {
                    a.f(n(jsonReader, (a)new rn()));
                    continue;
                }
                case 0: {
                    a.b(m(jsonReader));
                    continue;
                }
            }
        }
        jsonReader.endObject();
        return a.a();
    }
    
    public static CrashlyticsReport.e.d.a.b.c w(final JsonReader jsonReader) {
        final CrashlyticsReport.e.d.a.b.c.a a = CrashlyticsReport.e.d.a.b.c.a();
        jsonReader.beginObject();
        while (jsonReader.hasNext()) {
            final String nextName = jsonReader.nextName();
            nextName.hashCode();
            final int hashCode = nextName.hashCode();
            int n = -1;
            switch (hashCode) {
                case 581754413: {
                    if (!nextName.equals("overflowCount")) {
                        break;
                    }
                    n = 4;
                    break;
                }
                case 91997906: {
                    if (!nextName.equals("causedBy")) {
                        break;
                    }
                    n = 3;
                    break;
                }
                case 3575610: {
                    if (!nextName.equals("type")) {
                        break;
                    }
                    n = 2;
                    break;
                }
                case -934964668: {
                    if (!nextName.equals("reason")) {
                        break;
                    }
                    n = 1;
                    break;
                }
                case -1266514778: {
                    if (!nextName.equals("frames")) {
                        break;
                    }
                    n = 0;
                    break;
                }
            }
            switch (n) {
                default: {
                    jsonReader.skipValue();
                    continue;
                }
                case 4: {
                    a.d(jsonReader.nextInt());
                    continue;
                }
                case 3: {
                    a.b(w(jsonReader));
                    continue;
                }
                case 2: {
                    a.f(jsonReader.nextString());
                    continue;
                }
                case 1: {
                    a.e(jsonReader.nextString());
                    continue;
                }
                case 0: {
                    a.c(n(jsonReader, (a)new tn()));
                    continue;
                }
            }
        }
        jsonReader.endObject();
        return a.a();
    }
    
    public static CrashlyticsReport.e.d.a.b.e.b x(final JsonReader jsonReader) {
        final CrashlyticsReport.e.d.a.b.e.b.a a = CrashlyticsReport.e.d.a.b.e.b.a();
        jsonReader.beginObject();
        while (jsonReader.hasNext()) {
            final String nextName = jsonReader.nextName();
            nextName.hashCode();
            final int hashCode = nextName.hashCode();
            int n = -1;
            switch (hashCode) {
                case 2125650548: {
                    if (!nextName.equals("importance")) {
                        break;
                    }
                    n = 4;
                    break;
                }
                case 3143036: {
                    if (!nextName.equals("file")) {
                        break;
                    }
                    n = 3;
                    break;
                }
                case 3571: {
                    if (!nextName.equals("pc")) {
                        break;
                    }
                    n = 2;
                    break;
                }
                case -887523944: {
                    if (!nextName.equals("symbol")) {
                        break;
                    }
                    n = 1;
                    break;
                }
                case -1019779949: {
                    if (!nextName.equals("offset")) {
                        break;
                    }
                    n = 0;
                    break;
                }
            }
            switch (n) {
                default: {
                    jsonReader.skipValue();
                    continue;
                }
                case 4: {
                    a.c(jsonReader.nextInt());
                    continue;
                }
                case 3: {
                    a.b(jsonReader.nextString());
                    continue;
                }
                case 2: {
                    a.e(jsonReader.nextLong());
                    continue;
                }
                case 1: {
                    a.f(jsonReader.nextString());
                    continue;
                }
                case 0: {
                    a.d(jsonReader.nextLong());
                    continue;
                }
            }
        }
        jsonReader.endObject();
        return a.a();
    }
    
    public static CrashlyticsReport.e.d.d y(final JsonReader jsonReader) {
        final CrashlyticsReport.e.d.d.a a = CrashlyticsReport.e.d.d.a();
        jsonReader.beginObject();
        while (jsonReader.hasNext()) {
            if (jsonReader.nextName().equals("content")) {
                a.b(jsonReader.nextString());
            }
            else {
                jsonReader.skipValue();
            }
        }
        jsonReader.endObject();
        return a.a();
    }
    
    public static CrashlyticsReport.e.d.e z(final JsonReader jsonReader) {
        final CrashlyticsReport.e.d.e.a a = CrashlyticsReport.e.d.e.a();
        jsonReader.beginObject();
        while (jsonReader.hasNext()) {
            final String nextName = jsonReader.nextName();
            nextName.hashCode();
            final int hashCode = nextName.hashCode();
            int n = -1;
            switch (hashCode) {
                case 1124454216: {
                    if (!nextName.equals("parameterValue")) {
                        break;
                    }
                    n = 3;
                    break;
                }
                case 1098747284: {
                    if (!nextName.equals("rolloutVariant")) {
                        break;
                    }
                    n = 2;
                    break;
                }
                case -1027290370: {
                    if (!nextName.equals("templateVersion")) {
                        break;
                    }
                    n = 1;
                    break;
                }
                case -1536268810: {
                    if (!nextName.equals("parameterKey")) {
                        break;
                    }
                    n = 0;
                    break;
                }
            }
            switch (n) {
                default: {
                    jsonReader.skipValue();
                    continue;
                }
                case 3: {
                    a.c(jsonReader.nextString());
                    continue;
                }
                case 2: {
                    a.d(I(jsonReader));
                    continue;
                }
                case 1: {
                    a.e(jsonReader.nextLong());
                    continue;
                }
                case 0: {
                    a.b(jsonReader.nextString());
                    continue;
                }
            }
        }
        jsonReader.endObject();
        return a.a();
    }
    
    public CrashlyticsReport L(final String s) {
        try {
            final JsonReader jsonReader = new JsonReader((Reader)new StringReader(s));
            try {
                final CrashlyticsReport h = H(jsonReader);
                jsonReader.close();
                return h;
            }
            finally {
                try {
                    jsonReader.close();
                }
                finally {
                    final Throwable exception;
                    ((Throwable)s).addSuppressed(exception);
                }
            }
        }
        catch (final IllegalStateException cause) {
            throw new IOException(cause);
        }
    }
    
    public String M(final CrashlyticsReport crashlyticsReport) {
        return un.a.encode(crashlyticsReport);
    }
    
    public CrashlyticsReport.e.d j(final String s) {
        try {
            final JsonReader jsonReader = new JsonReader((Reader)new StringReader(s));
            try {
                final CrashlyticsReport.e.d r = r(jsonReader);
                jsonReader.close();
                return r;
            }
            finally {
                try {
                    jsonReader.close();
                }
                finally {
                    final Throwable exception;
                    ((Throwable)s).addSuppressed(exception);
                }
            }
        }
        catch (final IllegalStateException cause) {
            throw new IOException(cause);
        }
    }
    
    public String k(final CrashlyticsReport.e.d d) {
        return un.a.encode(d);
    }
    
    public interface a
    {
        Object a(final JsonReader p0);
    }
}

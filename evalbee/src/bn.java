import com.google.firebase.crashlytics.internal.common.CommonUtils;
import java.util.concurrent.Future;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeoutException;
import java.util.concurrent.TimeUnit;
import com.google.android.gms.tasks.Tasks;
import java.util.concurrent.Callable;
import android.util.Log;
import android.text.TextUtils;
import com.google.android.gms.tasks.Task;
import java.util.concurrent.Executor;
import java.util.concurrent.ExecutorService;
import android.content.Context;

// 
// Decompiled by Procyon v0.6.0
// 

public class bn
{
    public final Context a;
    public final r10 b;
    public final dp c;
    public final m11 d;
    public final long e;
    public cn f;
    public cn g;
    public boolean h;
    public zm i;
    public final de0 j;
    public final z00 k;
    public final sc l;
    public final m4 m;
    public final ExecutorService n;
    public final xm o;
    public final wm p;
    public final dn q;
    public final hd1 r;
    
    public bn(final r10 b, final de0 j, final dn q, final dp c, final sc l, final m4 m, final z00 k, final ExecutorService n, final wm p10, final hd1 r) {
        this.b = b;
        this.c = c;
        this.a = b.l();
        this.j = j;
        this.q = q;
        this.l = l;
        this.m = m;
        this.n = n;
        this.k = k;
        this.o = new xm(n);
        this.p = p10;
        this.r = r;
        this.e = System.currentTimeMillis();
        this.d = new m11();
    }
    
    public static /* synthetic */ cn b(final bn bn) {
        return bn.f;
    }
    
    public static /* synthetic */ zm c(final bn bn) {
        return bn.i;
    }
    
    public static String i() {
        return "18.6.0";
    }
    
    public static boolean j(final String s, final boolean b) {
        if (!b) {
            zl0.f().i("Configured not to require a build ID.");
            return true;
        }
        if (!TextUtils.isEmpty((CharSequence)s)) {
            return true;
        }
        Log.e("FirebaseCrashlytics", ".");
        Log.e("FirebaseCrashlytics", ".     |  | ");
        Log.e("FirebaseCrashlytics", ".     |  |");
        Log.e("FirebaseCrashlytics", ".     |  |");
        Log.e("FirebaseCrashlytics", ".   \\ |  | /");
        Log.e("FirebaseCrashlytics", ".    \\    /");
        Log.e("FirebaseCrashlytics", ".     \\  /");
        Log.e("FirebaseCrashlytics", ".      \\/");
        Log.e("FirebaseCrashlytics", ".");
        Log.e("FirebaseCrashlytics", "The Crashlytics build ID is missing. This occurs when the Crashlytics Gradle plugin is missing from your app's build configuration. Please review the Firebase Crashlytics onboarding instructions at https://firebase.google.com/docs/crashlytics/get-started?platform=android#add-plugin");
        Log.e("FirebaseCrashlytics", ".");
        Log.e("FirebaseCrashlytics", ".      /\\");
        Log.e("FirebaseCrashlytics", ".     /  \\");
        Log.e("FirebaseCrashlytics", ".    /    \\");
        Log.e("FirebaseCrashlytics", ".   / |  | \\");
        Log.e("FirebaseCrashlytics", ".     |  |");
        Log.e("FirebaseCrashlytics", ".     |  |");
        Log.e("FirebaseCrashlytics", ".     |  |");
        Log.e("FirebaseCrashlytics", ".");
        return false;
    }
    
    public final void d() {
        final Task g = this.o.g(new Callable(this) {
            public final bn a;
            
            public Boolean a() {
                return bn.c(this.a).s();
            }
        });
        while (true) {
            try {
                final boolean equals = Boolean.TRUE.equals(v22.f(g));
                this.h = equals;
            }
            catch (final Exception ex) {
                final boolean equals = false;
                continue;
            }
            break;
        }
    }
    
    public boolean e() {
        return this.f.c();
    }
    
    public final Task f(final zm1 zm1) {
        this.m();
        try {
            try {
                this.l.a(new an(this));
                this.i.S();
                if (!zm1.a().b.a) {
                    zl0.f().b("Collection of crash reports disabled in Crashlytics settings.");
                    final Task forException = Tasks.forException((Exception)new RuntimeException("Collection of crash reports disabled in Crashlytics settings."));
                    this.l();
                    return forException;
                }
                if (!this.i.z(zm1)) {
                    zl0.f().k("Previous sessions could not be finalized.");
                }
                final Task u = this.i.U(zm1.b());
                this.l();
                return u;
            }
            finally {}
        }
        catch (final Exception ex) {
            zl0.f().e("Crashlytics encountered a problem during asynchronous initialization.", ex);
            final Task forException2 = Tasks.forException(ex);
            this.l();
            return forException2;
        }
        this.l();
    }
    
    public Task g(final zm1 zm1) {
        return v22.h(this.n, new Callable(this, zm1) {
            public final zm1 a;
            public final bn b;
            
            public Task a() {
                return this.b.f(this.a);
            }
        });
    }
    
    public final void h(final zm1 zm1) {
        final Future<?> submit = this.n.submit(new Runnable(this, zm1) {
            public final zm1 a;
            public final bn b;
            
            @Override
            public void run() {
                this.b.f(this.a);
            }
        });
        zl0.f().b("Crashlytics detected incomplete initialization on previous app launch. Will initialize synchronously.");
        zl0 zl0;
        String s;
        try {
            submit.get(3L, TimeUnit.SECONDS);
            return;
        }
        catch (final TimeoutException ex) {
            zl0 = zl0.f();
            s = "Crashlytics timed out during initialization.";
        }
        catch (final ExecutionException ex) {
            zl0 = zl0.f();
            s = "Crashlytics encountered a problem during initialization.";
        }
        catch (final InterruptedException ex) {
            zl0 = zl0.f();
            s = "Crashlytics was interrupted during initialization.";
        }
        final TimeoutException ex;
        zl0.e(s, ex);
    }
    
    public void k(final String s) {
        this.i.X(System.currentTimeMillis() - this.e, s);
    }
    
    public void l() {
        this.o.g(new Callable(this) {
            public final bn a;
            
            public Boolean a() {
                try {
                    final boolean d = bn.b(this.a).d();
                    if (!d) {
                        zl0.f().k("Initialization marker file was not properly removed.");
                    }
                    return d;
                }
                catch (final Exception ex) {
                    zl0.f().e("Problem encountered deleting Crashlytics initialization marker.", ex);
                    return Boolean.FALSE;
                }
            }
        });
    }
    
    public void m() {
        this.o.b();
        this.f.a();
        zl0.f().i("Initialization marker file was created.");
    }
    
    public boolean n(final s7 s7, final zm1 zm1) {
        if (j(s7.b, CommonUtils.i(this.a, "com.crashlytics.RequireBuildId", true))) {
            final String string = new vd(this.j).toString();
            try {
                this.g = new cn("crash_marker", this.k);
                this.f = new cn("initialization_marker", this.k);
                final f22 f22 = new f22(string, this.k, this.o);
                final ul0 ul0 = new ul0(this.k);
                final iw0 iw0 = new iw0(1024, new lp1[] { new qd1(10) });
                this.r.c(f22);
                this.i = new zm(this.a, this.o, this.j, this.c, this.k, this.g, s7, f22, ul0, nm1.h(this.a, this.j, this.k, s7, ul0, f22, iw0, zm1, this.d, this.p), this.q, this.m, this.p);
                final boolean e = this.e();
                this.d();
                this.i.x(string, Thread.getDefaultUncaughtExceptionHandler(), zm1);
                if (e && CommonUtils.d(this.a)) {
                    zl0.f().b("Crashlytics did not finish previous background initialization. Initializing synchronously.");
                    this.h(zm1);
                    return false;
                }
                zl0.f().b("Successfully configured exception handler.");
                return true;
            }
            catch (final Exception ex) {
                zl0.f().e("Crashlytics was not started due to an exception during initialization", ex);
                this.i = null;
                return false;
            }
        }
        throw new IllegalStateException("The Crashlytics build ID is missing. This occurs when the Crashlytics Gradle plugin is missing from your app's build configuration. Please review the Firebase Crashlytics onboarding instructions at https://firebase.google.com/docs/crashlytics/get-started?platform=android#add-plugin");
    }
}

import android.content.Intent;
import com.google.android.gms.tasks.OnCompleteListener;
import java.util.concurrent.Executor;
import android.util.Log;
import android.os.Process;
import com.google.android.gms.tasks.Task;
import android.os.Binder;

// 
// Decompiled by Procyon v0.6.0
// 

public class e82 extends Binder
{
    public final a a;
    
    public e82(final a a) {
        this.a = a;
    }
    
    public void c(final h82.a a) {
        if (Binder.getCallingUid() == Process.myUid()) {
            if (Log.isLoggable("FirebaseMessaging", 3)) {
                Log.d("FirebaseMessaging", "service received new intent via bind strategy");
            }
            this.a.a(a.a).addOnCompleteListener((Executor)new xu0(), (OnCompleteListener)new d82(a));
            return;
        }
        throw new SecurityException("Binding only allowed within app");
    }
    
    public interface a
    {
        Task a(final Intent p0);
    }
}

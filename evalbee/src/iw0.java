// 
// Decompiled by Procyon v0.6.0
// 

public class iw0 implements lp1
{
    public final int a;
    public final lp1[] b;
    public final jw0 c;
    
    public iw0(final int a, final lp1... b) {
        this.a = a;
        this.b = b;
        this.c = new jw0(a);
    }
    
    @Override
    public StackTraceElement[] a(StackTraceElement[] a) {
        if (a.length <= this.a) {
            return a;
        }
        final lp1[] b = this.b;
        final int length = b.length;
        int i = 0;
        StackTraceElement[] a2 = a;
        while (i < length) {
            final lp1 lp1 = b[i];
            if (a2.length <= this.a) {
                break;
            }
            a2 = lp1.a(a);
            ++i;
        }
        a = a2;
        if (a2.length > this.a) {
            a = this.c.a(a2);
        }
        return a;
    }
}

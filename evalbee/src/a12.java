import android.text.Spanned;
import java.util.stream.IntStream;
import android.os.Build$VERSION;
import android.text.SpannableString;
import android.text.Spannable;

// 
// Decompiled by Procyon v0.6.0
// 

public class a12 implements Spannable
{
    public boolean a;
    public Spannable b;
    
    public a12(final Spannable b) {
        this.a = false;
        this.b = b;
    }
    
    public a12(final CharSequence charSequence) {
        this.a = false;
        this.b = (Spannable)new SpannableString(charSequence);
    }
    
    public static b c() {
        Object o;
        if (Build$VERSION.SDK_INT < 28) {
            o = new b();
        }
        else {
            o = new c();
        }
        return (b)o;
    }
    
    public final void a() {
        final Spannable b = this.b;
        if (!this.a && c().a((CharSequence)b)) {
            this.b = (Spannable)new SpannableString((CharSequence)b);
        }
        this.a = true;
    }
    
    public Spannable b() {
        return this.b;
    }
    
    public char charAt(final int n) {
        return ((CharSequence)this.b).charAt(n);
    }
    
    public IntStream chars() {
        return a12.a.a((CharSequence)this.b);
    }
    
    public IntStream codePoints() {
        return a12.a.b((CharSequence)this.b);
    }
    
    public int getSpanEnd(final Object o) {
        return ((Spanned)this.b).getSpanEnd(o);
    }
    
    public int getSpanFlags(final Object o) {
        return ((Spanned)this.b).getSpanFlags(o);
    }
    
    public int getSpanStart(final Object o) {
        return ((Spanned)this.b).getSpanStart(o);
    }
    
    public Object[] getSpans(final int n, final int n2, final Class clazz) {
        return ((Spanned)this.b).getSpans(n, n2, clazz);
    }
    
    public int length() {
        return ((CharSequence)this.b).length();
    }
    
    public int nextSpanTransition(final int n, final int n2, final Class clazz) {
        return ((Spanned)this.b).nextSpanTransition(n, n2, clazz);
    }
    
    public void removeSpan(final Object o) {
        this.a();
        this.b.removeSpan(o);
    }
    
    public void setSpan(final Object o, final int n, final int n2, final int n3) {
        this.a();
        this.b.setSpan(o, n, n2, n3);
    }
    
    public CharSequence subSequence(final int n, final int n2) {
        return ((CharSequence)this.b).subSequence(n, n2);
    }
    
    @Override
    public String toString() {
        return this.b.toString();
    }
    
    public abstract static class a
    {
        public static IntStream a(final CharSequence charSequence) {
            return charSequence.chars();
        }
        
        public static IntStream b(final CharSequence charSequence) {
            return charSequence.codePoints();
        }
    }
    
    public static class b
    {
        public boolean a(final CharSequence charSequence) {
            return charSequence instanceof g71;
        }
    }
    
    public static class c extends b
    {
        @Override
        public boolean a(final CharSequence charSequence) {
            return b12.a((Object)charSequence) || charSequence instanceof g71;
        }
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

public final class n71
{
    public final String a;
    public final Long b;
    
    public n71(final String a, final Long b) {
        fg0.e((Object)a, "key");
        this.a = a;
        this.b = b;
    }
    
    public n71(final String s, final boolean b) {
        fg0.e((Object)s, "key");
        long l;
        if (b) {
            l = 1L;
        }
        else {
            l = 0L;
        }
        this(s, l);
    }
    
    public final String a() {
        return this.a;
    }
    
    public final Long b() {
        return this.b;
    }
    
    @Override
    public boolean equals(final Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof n71)) {
            return false;
        }
        final n71 n71 = (n71)o;
        return fg0.a((Object)this.a, (Object)n71.a) && fg0.a((Object)this.b, (Object)n71.b);
    }
    
    @Override
    public int hashCode() {
        final int hashCode = this.a.hashCode();
        final Long b = this.b;
        int hashCode2;
        if (b == null) {
            hashCode2 = 0;
        }
        else {
            hashCode2 = b.hashCode();
        }
        return hashCode * 31 + hashCode2;
    }
    
    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append("Preference(key=");
        sb.append(this.a);
        sb.append(", value=");
        sb.append(this.b);
        sb.append(')');
        return sb.toString();
    }
}

import com.ekodroid.omrevaluator.templateui.models.AnswerSetKey;
import com.ekodroid.omrevaluator.templateui.models.Section2;
import com.ekodroid.omrevaluator.templateui.models.Subject2;
import com.ekodroid.omrevaluator.templateui.models.PageLayout;
import com.ekodroid.omrevaluator.templateui.models.InvalidQuestionSet;
import com.ekodroid.omrevaluator.templateui.models.PartialAnswerOptionKey;
import com.ekodroid.omrevaluator.templateui.scanner.AnswerValue;
import com.ekodroid.omrevaluator.templateui.models.AnswerOptionKey;
import com.ekodroid.omrevaluator.templateui.models.AnswerOption;
import java.util.Iterator;
import com.ekodroid.omrevaluator.serializable.ExmVersions.ResultDataV1;
import com.ekodroid.omrevaluator.database.ResultDataJsonModel;
import com.ekodroid.omrevaluator.templateui.models.ExamId;
import java.util.ArrayList;
import com.ekodroid.omrevaluator.templateui.models.SheetTemplate2;
import com.ekodroid.omrevaluator.serializable.ExmVersions.ExamFileModel;
import com.ekodroid.omrevaluator.serializable.ExmVersions.ExamDataV1;

// 
// Decompiled by Procyon v0.6.0
// 

public abstract class yx
{
    public static int a = 4;
    
    public static ExamDataV1 a(final byte[] array) {
        try {
            final ExamFileModel c = c(array);
            final ExamDataV1 examDataV1 = (ExamDataV1)new gc0().j(b.g(c.getJsonData(), c.getKey()), ExamDataV1.class);
            if (c.getVersion() < yx.a) {
                return f(examDataV1, c.getVersion());
            }
            return examDataV1;
        }
        catch (final Exception ex) {
            ex.printStackTrace();
            return null;
        }
    }
    
    public static byte[] b(final ExamFileModel examFileModel) {
        return b.h(new gc0().s(examFileModel).getBytes(), "examFileKeyPass");
    }
    
    public static ExamFileModel c(byte[] f) {
        f = b.f(f, "examFileKeyPass");
        return (ExamFileModel)new gc0().j(new String(f), ExamFileModel.class);
    }
    
    public static ExamFileModel d(final SheetTemplate2 sheetTemplate2, final ArrayList list, final ExamId examId) {
        final ArrayList list2 = new ArrayList();
        if (list != null && list.size() > 0) {
            for (final ResultDataJsonModel resultDataJsonModel : list) {
                list2.add(new ResultDataV1(resultDataJsonModel.getResultItem(sheetTemplate2), resultDataJsonModel.getRollNo()));
            }
        }
        final ExamDataV1 examDataV1 = new ExamDataV1(sheetTemplate2, list2, examId);
        final String e = e();
        return new ExamFileModel(yx.a, e, b.i(new gc0().s(examDataV1), e));
    }
    
    public static String e() {
        return String.valueOf(System.currentTimeMillis());
    }
    
    public static ExamDataV1 f(final ExamDataV1 examDataV1, int i) {
        final SheetTemplate2 sheetTemplate2 = examDataV1.getSheetTemplate2();
        Label_0676: {
            Label_0632: {
                if (i != 1) {
                    if (i != 2) {
                        if (i != 3) {
                            break Label_0676;
                        }
                        break Label_0632;
                    }
                }
                else {
                    final Subject2[] subjects = sheetTemplate2.getTemplateParams().getSubjects();
                    int length;
                    Section2[] sections;
                    int length2;
                    int j;
                    Section2 section2;
                    for (length = subjects.length, i = 0; i < length; ++i) {
                        sections = subjects[i].getSections();
                        for (length2 = sections.length, j = 0; j < length2; ++j) {
                            section2 = sections[j];
                            section2.setPartialAllowed(section2.getPartialMarks() > 0.0);
                        }
                    }
                    AnswerOption answerOption;
                    for (i = 1; i < sheetTemplate2.getAnswerOptions().size(); ++i) {
                        answerOption = sheetTemplate2.getAnswerOptions().get(i);
                        answerOption.partialAllowed = (answerOption.partialMarks > 0.0);
                    }
                    AnswerSetKey[] answerKeys = sheetTemplate2.getAnswerKeys();
                    if (answerKeys != null && answerKeys.length > 0) {
                        i = answerKeys.length;
                        for (int k = 0; k < i; ++k) {
                            AnswerSetKey answerSetKey = answerKeys[k];
                            ArrayList<AnswerOptionKey> answerOptionKeys = new ArrayList<AnswerOptionKey>();
                            ArrayList<AnswerValue> answerValue2s = answerSetKey.getAnswerValue2s();
                            AnswerSetKey[] array3;
                            int n3;
                            ArrayList<AnswerOptionKey> list4;
                            ArrayList<AnswerValue> list5;
                            for (int l = 0; l < answerValue2s.size(); l = i, answerKeys = array3, i = n3, answerOptionKeys = list4, answerValue2s = list5) {
                                final AnswerValue answerValue = answerValue2s.get(l);
                                final ArrayList<AnswerOption> answerOptions = sheetTemplate2.getAnswerOptions();
                                final int index = l + 1;
                                final AnswerOption answerOption2 = answerOptions.get(index);
                                ArrayList<PartialAnswerOptionKey> list;
                                AnswerSetKey answerSetKey2;
                                ArrayList<AnswerValue> list3;
                                if (answerOption2.partialMarks > 0.0) {
                                    list = new ArrayList<PartialAnswerOptionKey>();
                                    for (int n = 0; n < answerValue.getMarkedValues().length; ++n) {
                                        if (answerValue.getMarkedValues()[n]) {
                                            final boolean[] array = new boolean[answerValue.getMarkedValues().length];
                                            array[n] = true;
                                            list.add(new PartialAnswerOptionKey(answerOption2.type, answerOption2.questionNumber, array, answerOption2.subjectId, answerOption2.sectionId, answerOption2.partialMarks, answerOption2.payload, null, null));
                                        }
                                    }
                                    final AnswerSetKey[] array2 = answerKeys;
                                    answerSetKey2 = answerSetKey;
                                    final ArrayList<AnswerOptionKey> list2 = answerOptionKeys;
                                    list3 = answerValue2s;
                                    final int n2 = index;
                                    array3 = array2;
                                    n3 = i;
                                    list4 = list2;
                                    i = n2;
                                }
                                else {
                                    final AnswerSetKey[] array4 = answerKeys;
                                    final int n4 = i;
                                    answerSetKey2 = answerSetKey;
                                    list3 = answerValue2s;
                                    i = index;
                                    list = null;
                                    list4 = answerOptionKeys;
                                    n3 = n4;
                                    array3 = array4;
                                }
                                list4.add(new AnswerOptionKey(answerValue.getType(), answerValue.getQuestionNumber(), answerValue.getMarkedValues(), null, answerValue.getSubjectId(), answerValue.getSectionId(), list, answerValue.getPayload()));
                                list5 = list3;
                                answerSetKey = answerSetKey2;
                            }
                            answerSetKey.setAnswerOptionKeys(answerOptionKeys);
                        }
                    }
                }
                final int examSets = sheetTemplate2.getTemplateParams().getExamSets();
                final InvalidQuestionSet[] invalidQuestionSets = new InvalidQuestionSet[examSets];
                if (examSets > 1) {
                    int n5;
                    for (i = 0; i < examSets; i = n5) {
                        n5 = i + 1;
                        invalidQuestionSets[i] = new InvalidQuestionSet(n5, sheetTemplate2.getInvalidQuestions(), sheetTemplate2.getInvalidQueMarks());
                    }
                }
                else {
                    invalidQuestionSets[0] = new InvalidQuestionSet(0, sheetTemplate2.getInvalidQuestions(), sheetTemplate2.getInvalidQueMarks());
                }
                sheetTemplate2.setInvalidQuestionSets(invalidQuestionSets);
            }
            final PageLayout pageLayout = new PageLayout(sheetTemplate2.rows, sheetTemplate2.columns, sheetTemplate2.columnWidthInBubbles);
            sheetTemplate2.setTemplateVersion(4);
            sheetTemplate2.setPageLayouts(new PageLayout[] { pageLayout });
        }
        examDataV1.setSheetTemplate2(sheetTemplate2);
        return examDataV1;
    }
}

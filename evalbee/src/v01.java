import java.util.Arrays;
import com.google.common.collect.k;

// 
// Decompiled by Procyon v0.6.0
// 

public class v01 extends k
{
    public transient long[] i;
    public transient int j;
    public transient int k;
    
    public v01(final int n) {
        this(n, 1.0f);
    }
    
    public v01(final int n, final float n2) {
        super(n, n2);
    }
    
    public final int E(final int n) {
        return (int)(this.i[n] >>> 32);
    }
    
    public final int F(final int n) {
        return (int)this.i[n];
    }
    
    public final void G(final int n, final int n2) {
        final long[] i = this.i;
        i[n] = ((i[n] & 0xFFFFFFFFL) | (long)n2 << 32);
    }
    
    public final void H(final int k, final int j) {
        if (k == -2) {
            this.j = j;
        }
        else {
            this.I(k, j);
        }
        if (j == -2) {
            this.k = k;
        }
        else {
            this.G(j, k);
        }
    }
    
    public final void I(final int n, final int n2) {
        final long[] i = this.i;
        i[n] = ((i[n] & 0xFFFFFFFF00000000L) | ((long)n2 & 0xFFFFFFFFL));
    }
    
    @Override
    public void a() {
        super.a();
        this.j = -2;
        this.k = -2;
    }
    
    @Override
    public int e() {
        int j;
        if ((j = this.j) == -2) {
            j = -1;
        }
        return j;
    }
    
    @Override
    public void n(final int n, final float n2) {
        super.n(n, n2);
        this.j = -2;
        this.k = -2;
        Arrays.fill(this.i = new long[n], -1L);
    }
    
    @Override
    public void o(final int n, final Object o, final int n2, final int n3) {
        super.o(n, o, n2, n3);
        this.H(this.k, n);
        this.H(n, -2);
    }
    
    @Override
    public void p(final int n) {
        final int n2 = this.C() - 1;
        this.H(this.E(n), this.F(n));
        if (n < n2) {
            this.H(this.E(n2), n);
            this.H(n, this.F(n2));
        }
        super.p(n);
    }
    
    @Override
    public int s(int f) {
        if ((f = this.F(f)) == -2) {
            f = -1;
        }
        return f;
    }
    
    @Override
    public int t(final int n, final int n2) {
        int n3 = n;
        if (n == this.C()) {
            n3 = n2;
        }
        return n3;
    }
    
    @Override
    public void y(final int n) {
        super.y(n);
        final long[] i = this.i;
        Arrays.fill(this.i = Arrays.copyOf(i, n), i.length, n, -1L);
    }
}

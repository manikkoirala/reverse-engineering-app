import java.nio.Buffer;
import java.nio.ByteOrder;
import java.nio.ByteBuffer;

// 
// Decompiled by Procyon v0.6.0
// 

public abstract class f extends n
{
    public final ByteBuffer a;
    
    public f() {
        this.a = ByteBuffer.allocate(8).order(ByteOrder.LITTLE_ENDIAN);
    }
    
    @Override
    public qc0 a(final int n) {
        this.a.putInt(n);
        return this.l(4);
    }
    
    @Override
    public qc0 b(final long n) {
        this.a.putLong(n);
        return this.l(8);
    }
    
    @Override
    public qc0 h(final byte[] array, final int n, final int n2) {
        i71.w(n, n + n2, array.length);
        this.p(array, n, n2);
        return this;
    }
    
    @Override
    public qc0 i(final ByteBuffer byteBuffer) {
        this.n(byteBuffer);
        return this;
    }
    
    @Override
    public qc0 j(final byte[] array) {
        i71.r(array);
        this.o(array);
        return this;
    }
    
    @Override
    public qc0 k(final char c) {
        this.a.putChar(c);
        return this.l(2);
    }
    
    public final qc0 l(final int n) {
        try {
            this.p(this.a.array(), 0, n);
            return this;
        }
        finally {
            vg0.a(this.a);
        }
    }
    
    public abstract void m(final byte p0);
    
    public void n(final ByteBuffer byteBuffer) {
        if (byteBuffer.hasArray()) {
            this.p(byteBuffer.array(), byteBuffer.arrayOffset() + byteBuffer.position(), byteBuffer.remaining());
            vg0.c(byteBuffer, byteBuffer.limit());
        }
        else {
            for (int i = byteBuffer.remaining(); i > 0; --i) {
                this.m(byteBuffer.get());
            }
        }
    }
    
    public void o(final byte[] array) {
        this.p(array, 0, array.length);
    }
    
    public abstract void p(final byte[] p0, final int p1, final int p2);
}

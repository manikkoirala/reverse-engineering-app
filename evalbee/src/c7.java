import android.view.View;
import android.graphics.drawable.AnimationDrawable;
import android.graphics.drawable.ClipDrawable;
import android.graphics.Shader;
import android.graphics.BitmapShader;
import android.graphics.Shader$TileMode;
import android.graphics.drawable.ShapeDrawable;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.LayerDrawable;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.graphics.RectF;
import android.graphics.drawable.shapes.RoundRectShape;
import android.graphics.drawable.shapes.Shape;
import android.graphics.Bitmap;
import android.widget.ProgressBar;

// 
// Decompiled by Procyon v0.6.0
// 

public class c7
{
    public static final int[] c;
    public final ProgressBar a;
    public Bitmap b;
    
    static {
        c = new int[] { 16843067, 16843068 };
    }
    
    public c7(final ProgressBar a) {
        this.a = a;
    }
    
    public final Shape a() {
        return (Shape)new RoundRectShape(new float[] { 5.0f, 5.0f, 5.0f, 5.0f, 5.0f, 5.0f, 5.0f, 5.0f }, (RectF)null, (float[])null);
    }
    
    public Bitmap b() {
        return this.b;
    }
    
    public void c(final AttributeSet set, final int n) {
        final tw1 v = tw1.v(((View)this.a).getContext(), set, c7.c, n, 0);
        final Drawable h = v.h(0);
        if (h != null) {
            this.a.setIndeterminateDrawable(this.e(h));
        }
        final Drawable h2 = v.h(1);
        if (h2 != null) {
            this.a.setProgressDrawable(this.d(h2, false));
        }
        v.w();
    }
    
    public Drawable d(final Drawable drawable, final boolean b) {
        if (drawable instanceof da2) {
            final da2 da2 = (da2)drawable;
            final Drawable b2 = da2.b();
            if (b2 != null) {
                da2.a(this.d(b2, b));
            }
        }
        else {
            if (drawable instanceof LayerDrawable) {
                final LayerDrawable layerDrawable = (LayerDrawable)drawable;
                final int numberOfLayers = layerDrawable.getNumberOfLayers();
                final Drawable[] array = new Drawable[numberOfLayers];
                final int n = 0;
                for (int i = 0; i < numberOfLayers; ++i) {
                    final int id = layerDrawable.getId(i);
                    array[i] = this.d(layerDrawable.getDrawable(i), id == 16908301 || id == 16908303);
                }
                final LayerDrawable layerDrawable2 = new LayerDrawable(array);
                for (int j = n; j < numberOfLayers; ++j) {
                    layerDrawable2.setId(j, layerDrawable.getId(j));
                    c7.a.a(layerDrawable, layerDrawable2, j);
                }
                return (Drawable)layerDrawable2;
            }
            if (drawable instanceof BitmapDrawable) {
                final BitmapDrawable bitmapDrawable = (BitmapDrawable)drawable;
                final Bitmap bitmap = bitmapDrawable.getBitmap();
                if (this.b == null) {
                    this.b = bitmap;
                }
                final ShapeDrawable shapeDrawable = new ShapeDrawable(this.a());
                shapeDrawable.getPaint().setShader((Shader)new BitmapShader(bitmap, Shader$TileMode.REPEAT, Shader$TileMode.CLAMP));
                shapeDrawable.getPaint().setColorFilter(bitmapDrawable.getPaint().getColorFilter());
                Object o = shapeDrawable;
                if (b) {
                    o = new ClipDrawable((Drawable)shapeDrawable, 3, 1);
                }
                return (Drawable)o;
            }
        }
        return drawable;
    }
    
    public final Drawable e(Drawable d) {
        Object o = d;
        if (d instanceof AnimationDrawable) {
            final AnimationDrawable animationDrawable = (AnimationDrawable)d;
            final int numberOfFrames = animationDrawable.getNumberOfFrames();
            o = new AnimationDrawable();
            ((AnimationDrawable)o).setOneShot(animationDrawable.isOneShot());
            for (int i = 0; i < numberOfFrames; ++i) {
                d = this.d(animationDrawable.getFrame(i), true);
                d.setLevel(10000);
                ((AnimationDrawable)o).addFrame(d, animationDrawable.getDuration(i));
            }
            ((Drawable)o).setLevel(10000);
        }
        return (Drawable)o;
    }
    
    public abstract static class a
    {
        public static void a(final LayerDrawable layerDrawable, final LayerDrawable layerDrawable2, final int n) {
            layerDrawable2.setLayerGravity(n, layerDrawable.getLayerGravity(n));
            layerDrawable2.setLayerWidth(n, layerDrawable.getLayerWidth(n));
            layerDrawable2.setLayerHeight(n, layerDrawable.getLayerHeight(n));
            layerDrawable2.setLayerInsetLeft(n, layerDrawable.getLayerInsetLeft(n));
            layerDrawable2.setLayerInsetRight(n, layerDrawable.getLayerInsetRight(n));
            layerDrawable2.setLayerInsetTop(n, layerDrawable.getLayerInsetTop(n));
            layerDrawable2.setLayerInsetBottom(n, layerDrawable.getLayerInsetBottom(n));
            layerDrawable2.setLayerInsetStart(n, layerDrawable.getLayerInsetStart(n));
            layerDrawable2.setLayerInsetEnd(n, layerDrawable.getLayerInsetEnd(n));
        }
    }
}

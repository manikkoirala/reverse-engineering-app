import java.nio.Buffer;
import com.google.common.hash.HashCode;
import java.nio.ByteOrder;
import java.nio.ByteBuffer;

// 
// Decompiled by Procyon v0.6.0
// 

public abstract class l0 extends n
{
    public final ByteBuffer a;
    public final int b;
    public final int c;
    
    public l0(final int n) {
        this(n, n);
    }
    
    public l0(final int c, final int b) {
        i71.d(b % c == 0);
        this.a = ByteBuffer.allocate(b + 7).order(ByteOrder.LITTLE_ENDIAN);
        this.b = b;
        this.c = c;
    }
    
    @Override
    public final qc0 a(final int n) {
        this.a.putInt(n);
        this.n();
        return this;
    }
    
    @Override
    public final qc0 b(final long n) {
        this.a.putLong(n);
        this.n();
        return this;
    }
    
    @Override
    public final HashCode e() {
        this.m();
        vg0.b(this.a);
        if (this.a.remaining() > 0) {
            this.p(this.a);
            final ByteBuffer a = this.a;
            vg0.c(a, a.limit());
        }
        return this.l();
    }
    
    @Override
    public final qc0 h(final byte[] array, final int offset, final int length) {
        return this.q(ByteBuffer.wrap(array, offset, length).order(ByteOrder.LITTLE_ENDIAN));
    }
    
    @Override
    public final qc0 i(final ByteBuffer byteBuffer) {
        final ByteOrder order = byteBuffer.order();
        try {
            byteBuffer.order(ByteOrder.LITTLE_ENDIAN);
            return this.q(byteBuffer);
        }
        finally {
            byteBuffer.order(order);
        }
    }
    
    @Override
    public final qc0 k(final char c) {
        this.a.putChar(c);
        this.n();
        return this;
    }
    
    public abstract HashCode l();
    
    public final void m() {
        vg0.b(this.a);
        while (this.a.remaining() >= this.c) {
            this.o(this.a);
        }
        this.a.compact();
    }
    
    public final void n() {
        if (this.a.remaining() < 8) {
            this.m();
        }
    }
    
    public abstract void o(final ByteBuffer p0);
    
    public abstract void p(final ByteBuffer p0);
    
    public final qc0 q(final ByteBuffer byteBuffer) {
        if (byteBuffer.remaining() <= this.a.remaining()) {
            this.a.put(byteBuffer);
            this.n();
            return this;
        }
        for (int b = this.b, position = this.a.position(), i = 0; i < b - position; ++i) {
            this.a.put(byteBuffer.get());
        }
        this.m();
        while (byteBuffer.remaining() >= this.c) {
            this.o(byteBuffer);
        }
        this.a.put(byteBuffer);
        return this;
    }
}

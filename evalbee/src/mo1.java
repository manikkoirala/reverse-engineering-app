import com.ekodroid.omrevaluator.templateui.models.ExamId;

// 
// Decompiled by Procyon v0.6.0
// 

public class mo1
{
    public int a;
    public String b;
    public String c;
    public ExamId d;
    public double e;
    public double f;
    public int g;
    public String h;
    public String[] i;
    public double[] j;
    public double[] k;
    public int l;
    public int m;
    public int n;
    public boolean o;
    
    public mo1(final int a, final String b, final String c, final ExamId d, final double e, final double f, final int g, final String h, final String[] i, final double[] j, final double[] k, final int l, final int m, final int n, final boolean o) {
        this.a = a;
        this.b = b;
        this.c = c;
        this.d = d;
        this.e = e;
        this.f = f;
        this.g = g;
        this.h = h;
        this.i = i;
        this.j = j;
        this.k = k;
        this.l = l;
        this.m = m;
        this.n = n;
        this.o = o;
    }
    
    public int a() {
        return this.l;
    }
    
    public ExamId b() {
        return this.d;
    }
    
    public String c() {
        return this.h;
    }
    
    public int d() {
        return this.m;
    }
    
    public double e() {
        return this.f;
    }
    
    public String f() {
        return this.c;
    }
    
    public int g() {
        return this.g;
    }
    
    public int h() {
        return this.a;
    }
    
    public String i() {
        return this.b;
    }
    
    public String[] j() {
        return this.i;
    }
    
    public double[] k() {
        return this.j;
    }
    
    public double[] l() {
        return this.k;
    }
    
    public double m() {
        return this.e;
    }
    
    public int n() {
        return this.n;
    }
    
    public boolean o() {
        return this.o;
    }
}

import android.net.NetworkCapabilities;
import android.net.Network;
import android.net.ConnectivityManager$NetworkCallback;
import android.content.Context;
import android.net.ConnectivityManager;

// 
// Decompiled by Procyon v0.6.0
// 

public final class az0 extends tk
{
    public final ConnectivityManager f;
    public final az0$a g;
    
    public az0(final Context context, final hu1 hu1) {
        fg0.e((Object)context, "context");
        fg0.e((Object)hu1, "taskExecutor");
        super(context, hu1);
        final Object systemService = this.d().getSystemService("connectivity");
        fg0.c(systemService, "null cannot be cast to non-null type android.net.ConnectivityManager");
        this.f = (ConnectivityManager)systemService;
        this.g = new ConnectivityManager$NetworkCallback(this) {
            public final az0 a;
            
            public void onCapabilitiesChanged(final Network network, final NetworkCapabilities obj) {
                fg0.e((Object)network, "network");
                fg0.e((Object)obj, "capabilities");
                final xl0 e = xl0.e();
                final String b = bz0.b();
                final StringBuilder sb = new StringBuilder();
                sb.append("Network capabilities changed: ");
                sb.append(obj);
                e.a(b, sb.toString());
                final az0 a = this.a;
                a.g(bz0.c(az0.j(a)));
            }
            
            public void onLost(final Network network) {
                fg0.e((Object)network, "network");
                xl0.e().a(bz0.b(), "Network connection lost");
                final az0 a = this.a;
                a.g(bz0.c(az0.j(a)));
            }
        };
    }
    
    public static final /* synthetic */ ConnectivityManager j(final az0 az0) {
        return az0.f;
    }
    
    @Override
    public void h() {
        try {
            xl0.e().a(bz0.b(), "Registering network callback");
            ty0.a(this.f, this.g);
            return;
        }
        catch (final SecurityException ex) {}
        catch (final IllegalArgumentException ex2) {}
        final SecurityException ex;
        xl0.e().d(bz0.b(), "Received exception while registering network callback", ex);
    }
    
    @Override
    public void i() {
        try {
            xl0.e().a(bz0.b(), "Unregistering network callback");
            ry0.c(this.f, this.g);
            return;
        }
        catch (final SecurityException ex) {}
        catch (final IllegalArgumentException ex2) {}
        final SecurityException ex;
        xl0.e().d(bz0.b(), "Received exception while unregistering network callback", ex);
    }
    
    public zy0 k() {
        return bz0.c(this.f);
    }
}

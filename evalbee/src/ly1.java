import kotlin.coroutines.CoroutineContext;
import kotlin.coroutines.CoroutineContext$b;
import kotlin.coroutines.CoroutineContext$a$a;
import java.util.concurrent.CancellationException;
import kotlinx.coroutines.n$a;
import java.util.concurrent.atomic.AtomicInteger;
import kotlinx.coroutines.n;
import kotlin.coroutines.CoroutineContext$a;

// 
// Decompiled by Procyon v0.6.0
// 

public final class ly1 implements CoroutineContext$a
{
    public static final a d;
    public final n a;
    public final xl b;
    public final AtomicInteger c;
    
    static {
        d = new a(null);
    }
    
    public ly1(final n a, final xl b) {
        fg0.e((Object)a, "transactionThreadControlJob");
        fg0.e((Object)b, "transactionDispatcher");
        this.a = a;
        this.b = b;
        this.c = new AtomicInteger(0);
    }
    
    public final void c() {
        this.c.incrementAndGet();
    }
    
    public final xl e() {
        return this.b;
    }
    
    public final void f() {
        final int decrementAndGet = this.c.decrementAndGet();
        if (decrementAndGet >= 0) {
            if (decrementAndGet == 0) {
                n$a.a(this.a, (CancellationException)null, 1, (Object)null);
            }
            return;
        }
        throw new IllegalStateException("Transaction was never started or was already released.");
    }
    
    public Object fold(final Object o, final q90 q90) {
        return CoroutineContext$a$a.a((CoroutineContext$a)this, o, q90);
    }
    
    public CoroutineContext$a get(final CoroutineContext$b coroutineContext$b) {
        return CoroutineContext$a$a.b((CoroutineContext$a)this, coroutineContext$b);
    }
    
    public CoroutineContext$b getKey() {
        return (CoroutineContext$b)ly1.d;
    }
    
    public CoroutineContext minusKey(final CoroutineContext$b coroutineContext$b) {
        return CoroutineContext$a$a.c((CoroutineContext$a)this, coroutineContext$b);
    }
    
    public CoroutineContext plus(final CoroutineContext coroutineContext) {
        return CoroutineContext$a$a.d((CoroutineContext$a)this, coroutineContext);
    }
    
    public static final class a implements CoroutineContext$b
    {
    }
}

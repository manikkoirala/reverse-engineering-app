import java.util.Objects;
import java.lang.reflect.Method;
import android.view.WindowInsets$Builder;
import java.lang.reflect.Constructor;
import android.graphics.Rect;
import android.util.Log;
import java.lang.reflect.Field;
import android.view.View;
import android.view.WindowInsets;
import android.os.Build$VERSION;

// 
// Decompiled by Procyon v0.6.0
// 

public class u62
{
    public static final u62 b;
    public final l a;
    
    static {
        u62 b2;
        if (Build$VERSION.SDK_INT >= 30) {
            b2 = k.q;
        }
        else {
            b2 = l.b;
        }
        b = b2;
    }
    
    public u62(final WindowInsets windowInsets) {
        final int sdk_INT = Build$VERSION.SDK_INT;
        g a;
        if (sdk_INT >= 30) {
            a = new k(this, windowInsets);
        }
        else if (sdk_INT >= 29) {
            a = new j(this, windowInsets);
        }
        else if (sdk_INT >= 28) {
            a = new i(this, windowInsets);
        }
        else {
            a = new h(this, windowInsets);
        }
        this.a = (l)a;
    }
    
    public u62(final u62 u62) {
        if (u62 != null) {
            final l a = u62.a;
            final int sdk_INT = Build$VERSION.SDK_INT;
            Object a2;
            if (sdk_INT >= 30 && a instanceof k) {
                a2 = new k(this, (k)a);
            }
            else if (sdk_INT >= 29 && a instanceof j) {
                a2 = new j(this, (j)a);
            }
            else if (sdk_INT >= 28 && a instanceof i) {
                a2 = new i(this, (i)a);
            }
            else if (a instanceof h) {
                a2 = new h(this, (h)a);
            }
            else if (a instanceof g) {
                a2 = new g(this, (g)a);
            }
            else {
                a2 = new l(this);
            }
            this.a = (l)a2;
            a.e(this);
        }
        else {
            this.a = new l(this);
        }
    }
    
    public static nf0 n(final nf0 nf0, final int n, final int n2, final int n3, final int n4) {
        final int max = Math.max(0, nf0.a - n);
        final int max2 = Math.max(0, nf0.b - n2);
        final int max3 = Math.max(0, nf0.c - n3);
        final int max4 = Math.max(0, nf0.d - n4);
        if (max == n && max2 == n2 && max3 == n3 && max4 == n4) {
            return nf0;
        }
        return nf0.b(max, max2, max3, max4);
    }
    
    public static u62 v(final WindowInsets windowInsets) {
        return w(windowInsets, null);
    }
    
    public static u62 w(final WindowInsets windowInsets, final View view) {
        final u62 u62 = new u62((WindowInsets)l71.g(windowInsets));
        if (view != null && o32.T(view)) {
            u62.s(o32.H(view));
            u62.d(view.getRootView());
        }
        return u62;
    }
    
    public u62 a() {
        return this.a.a();
    }
    
    public u62 b() {
        return this.a.b();
    }
    
    public u62 c() {
        return this.a.c();
    }
    
    public void d(final View view) {
        this.a.d(view);
    }
    
    public rt e() {
        return this.a.f();
    }
    
    @Override
    public boolean equals(final Object o) {
        return this == o || (o instanceof u62 && c11.a(this.a, ((u62)o).a));
    }
    
    public nf0 f(final int n) {
        return this.a.g(n);
    }
    
    public nf0 g() {
        return this.a.i();
    }
    
    public int h() {
        return this.a.k().d;
    }
    
    @Override
    public int hashCode() {
        final l a = this.a;
        int hashCode;
        if (a == null) {
            hashCode = 0;
        }
        else {
            hashCode = a.hashCode();
        }
        return hashCode;
    }
    
    public int i() {
        return this.a.k().a;
    }
    
    public int j() {
        return this.a.k().c;
    }
    
    public int k() {
        return this.a.k().b;
    }
    
    public boolean l() {
        return this.a.k().equals(nf0.e) ^ true;
    }
    
    public u62 m(final int n, final int n2, final int n3, final int n4) {
        return this.a.m(n, n2, n3, n4);
    }
    
    public boolean o() {
        return this.a.n();
    }
    
    public u62 p(final int n, final int n2, final int n3, final int n4) {
        return new b(this).d(nf0.b(n, n2, n3, n4)).a();
    }
    
    public void q(final nf0[] array) {
        this.a.p(array);
    }
    
    public void r(final nf0 nf0) {
        this.a.q(nf0);
    }
    
    public void s(final u62 u62) {
        this.a.r(u62);
    }
    
    public void t(final nf0 nf0) {
        this.a.s(nf0);
    }
    
    public WindowInsets u() {
        final l a = this.a;
        WindowInsets c;
        if (a instanceof g) {
            c = ((g)a).c;
        }
        else {
            c = null;
        }
        return c;
    }
    
    public abstract static class a
    {
        public static Field a;
        public static Field b;
        public static Field c;
        public static boolean d;
        
        static {
            try {
                (u62.a.a = View.class.getDeclaredField("mAttachInfo")).setAccessible(true);
                final Class<?> forName = Class.forName("android.view.View$AttachInfo");
                (u62.a.b = forName.getDeclaredField("mStableInsets")).setAccessible(true);
                (u62.a.c = forName.getDeclaredField("mContentInsets")).setAccessible(true);
                u62.a.d = true;
            }
            catch (final ReflectiveOperationException ex) {
                final StringBuilder sb = new StringBuilder();
                sb.append("Failed to get visible insets from AttachInfo ");
                sb.append(ex.getMessage());
                Log.w("WindowInsetsCompat", sb.toString(), (Throwable)ex);
            }
        }
        
        public static u62 a(final View view) {
            if (u62.a.d) {
                if (view.isAttachedToWindow()) {
                    final View rootView = view.getRootView();
                    try {
                        final Object value = u62.a.a.get(rootView);
                        if (value != null) {
                            final Rect rect = (Rect)u62.a.b.get(value);
                            final Rect rect2 = (Rect)u62.a.c.get(value);
                            if (rect != null && rect2 != null) {
                                final u62 a = new b().c(nf0.c(rect)).d(nf0.c(rect2)).a();
                                a.s(a);
                                a.d(view.getRootView());
                                return a;
                            }
                        }
                    }
                    catch (final IllegalAccessException ex) {
                        final StringBuilder sb = new StringBuilder();
                        sb.append("Failed to get insets from AttachInfo. ");
                        sb.append(ex.getMessage());
                        Log.w("WindowInsetsCompat", sb.toString(), (Throwable)ex);
                    }
                }
            }
            return null;
        }
    }
    
    public static final class b
    {
        public final f a;
        
        public b() {
            final int sdk_INT = Build$VERSION.SDK_INT;
            f a;
            if (sdk_INT >= 30) {
                a = new e();
            }
            else if (sdk_INT >= 29) {
                a = new d();
            }
            else {
                a = new c();
            }
            this.a = a;
        }
        
        public b(final u62 u62) {
            final int sdk_INT = Build$VERSION.SDK_INT;
            f a;
            if (sdk_INT >= 30) {
                a = new e(u62);
            }
            else if (sdk_INT >= 29) {
                a = new d(u62);
            }
            else {
                a = new c(u62);
            }
            this.a = a;
        }
        
        public u62 a() {
            return this.a.b();
        }
        
        public b b(final int n, final nf0 nf0) {
            this.a.c(n, nf0);
            return this;
        }
        
        public b c(final nf0 nf0) {
            this.a.e(nf0);
            return this;
        }
        
        public b d(final nf0 nf0) {
            this.a.g(nf0);
            return this;
        }
    }
    
    public static class c extends f
    {
        public static Field e;
        public static boolean f = false;
        public static Constructor g;
        public static boolean h = false;
        public WindowInsets c;
        public nf0 d;
        
        public c() {
            this.c = i();
        }
        
        public c(final u62 u62) {
            super(u62);
            this.c = u62.u();
        }
        
        private static WindowInsets i() {
            if (!c.f) {
                try {
                    c.e = WindowInsets.class.getDeclaredField("CONSUMED");
                }
                catch (final ReflectiveOperationException ex) {
                    Log.i("WindowInsetsCompat", "Could not retrieve WindowInsets.CONSUMED field", (Throwable)ex);
                }
                c.f = true;
            }
            final Field e = c.e;
            if (e != null) {
                try {
                    final WindowInsets windowInsets = (WindowInsets)e.get(null);
                    if (windowInsets != null) {
                        return new WindowInsets(windowInsets);
                    }
                }
                catch (final ReflectiveOperationException ex2) {
                    Log.i("WindowInsetsCompat", "Could not get value from WindowInsets.CONSUMED field", (Throwable)ex2);
                }
            }
            if (!c.h) {
                try {
                    c.g = WindowInsets.class.getConstructor(Rect.class);
                }
                catch (final ReflectiveOperationException ex3) {
                    Log.i("WindowInsetsCompat", "Could not retrieve WindowInsets(Rect) constructor", (Throwable)ex3);
                }
                c.h = true;
            }
            final Constructor g = c.g;
            if (g != null) {
                try {
                    return (WindowInsets)g.newInstance(new Rect());
                }
                catch (final ReflectiveOperationException ex4) {
                    Log.i("WindowInsetsCompat", "Could not invoke WindowInsets(Rect) constructor", (Throwable)ex4);
                }
            }
            return null;
        }
        
        @Override
        public u62 b() {
            ((f)this).a();
            final u62 v = u62.v(this.c);
            v.q(super.b);
            v.t(this.d);
            return v;
        }
        
        @Override
        public void e(final nf0 d) {
            this.d = d;
        }
        
        @Override
        public void g(final nf0 nf0) {
            final WindowInsets c = this.c;
            if (c != null) {
                this.c = c.replaceSystemWindowInsets(nf0.a, nf0.b, nf0.c, nf0.d);
            }
        }
    }
    
    public abstract static class f
    {
        public final u62 a;
        public nf0[] b;
        
        public f() {
            this(new u62((u62)null));
        }
        
        public f(final u62 a) {
            this.a = a;
        }
        
        public final void a() {
            final nf0[] b = this.b;
            if (b != null) {
                final nf0 nf0 = b[m.b(1)];
                nf0 f;
                if ((f = this.b[m.b(2)]) == null) {
                    f = this.a.f(2);
                }
                nf0 f2;
                if ((f2 = nf0) == null) {
                    f2 = this.a.f(1);
                }
                this.g(nf0.a(f2, f));
                final nf0 nf2 = this.b[m.b(16)];
                if (nf2 != null) {
                    this.f(nf2);
                }
                final nf0 nf3 = this.b[m.b(32)];
                if (nf3 != null) {
                    this.d(nf3);
                }
                final nf0 nf4 = this.b[m.b(64)];
                if (nf4 != null) {
                    this.h(nf4);
                }
            }
        }
        
        public abstract u62 b();
        
        public void c(final int n, final nf0 nf0) {
            if (this.b == null) {
                this.b = new nf0[9];
            }
            for (int i = 1; i <= 256; i <<= 1) {
                if ((n & i) != 0x0) {
                    this.b[m.b(i)] = nf0;
                }
            }
        }
        
        public void d(final nf0 nf0) {
        }
        
        public abstract void e(final nf0 p0);
        
        public void f(final nf0 nf0) {
        }
        
        public abstract void g(final nf0 p0);
        
        public void h(final nf0 nf0) {
        }
    }
    
    public static class d extends f
    {
        public final WindowInsets$Builder c;
        
        public d() {
            this.c = b72.a();
        }
        
        public d(final u62 u62) {
            super(u62);
            final WindowInsets u63 = u62.u();
            WindowInsets$Builder c;
            if (u63 != null) {
                c = c72.a(u63);
            }
            else {
                c = b72.a();
            }
            this.c = c;
        }
        
        @Override
        public u62 b() {
            ((f)this).a();
            final u62 v = u62.v(a72.a(this.c));
            v.q(super.b);
            return v;
        }
        
        @Override
        public void d(final nf0 nf0) {
            y62.a(this.c, nf0.e());
        }
        
        @Override
        public void e(final nf0 nf0) {
            x62.a(this.c, nf0.e());
        }
        
        @Override
        public void f(final nf0 nf0) {
            z62.a(this.c, nf0.e());
        }
        
        @Override
        public void g(final nf0 nf0) {
            w62.a(this.c, nf0.e());
        }
        
        @Override
        public void h(final nf0 nf0) {
            v62.a(this.c, nf0.e());
        }
    }
    
    public static class e extends d
    {
        public e() {
        }
        
        public e(final u62 u62) {
            super(u62);
        }
        
        @Override
        public void c(final int n, final nf0 nf0) {
            d72.a(super.c, u62.n.a(n), nf0.e());
        }
    }
    
    public static class g extends l
    {
        public static boolean h = false;
        public static Method i;
        public static Class j;
        public static Field k;
        public static Field l;
        public final WindowInsets c;
        public nf0[] d;
        public nf0 e;
        public u62 f;
        public nf0 g;
        
        public g(final u62 u62, final WindowInsets c) {
            super(u62);
            this.e = null;
            this.c = c;
        }
        
        public g(final u62 u62, final g g) {
            this(u62, new WindowInsets(g.c));
        }
        
        private nf0 t(final int n, final boolean b) {
            nf0 nf0 = nf0.e;
            for (int i = 1; i <= 256; i <<= 1) {
                if ((n & i) != 0x0) {
                    nf0 = nf0.a(nf0, this.u(i, b));
                }
            }
            return nf0;
        }
        
        private nf0 v() {
            final u62 f = this.f;
            if (f != null) {
                return f.g();
            }
            return nf0.e;
        }
        
        private nf0 w(final View obj) {
            if (Build$VERSION.SDK_INT < 30) {
                if (!u62.g.h) {
                    x();
                }
                final Method i = u62.g.i;
                final nf0 nf0 = null;
                if (i != null && u62.g.j != null) {
                    if (u62.g.k != null) {
                        try {
                            final Object invoke = i.invoke(obj, new Object[0]);
                            if (invoke == null) {
                                Log.w("WindowInsetsCompat", "Failed to get visible insets. getViewRootImpl() returned null from the provided view. This means that the view is either not attached or the method has been overridden", (Throwable)new NullPointerException());
                                return null;
                            }
                            final Rect rect = (Rect)u62.g.k.get(u62.g.l.get(invoke));
                            nf0 c = nf0;
                            if (rect != null) {
                                c = nf0.c(rect);
                            }
                            return c;
                        }
                        catch (final ReflectiveOperationException ex) {
                            final StringBuilder sb = new StringBuilder();
                            sb.append("Failed to get visible insets. (Reflection error). ");
                            sb.append(ex.getMessage());
                            Log.e("WindowInsetsCompat", sb.toString(), (Throwable)ex);
                        }
                    }
                }
                return null;
            }
            throw new UnsupportedOperationException("getVisibleInsets() should not be called on API >= 30. Use WindowInsets.isVisible() instead.");
        }
        
        private static void x() {
            try {
                g.i = View.class.getDeclaredMethod("getViewRootImpl", (Class<?>[])new Class[0]);
                g.k = (g.j = Class.forName("android.view.View$AttachInfo")).getDeclaredField("mVisibleInsets");
                g.l = Class.forName("android.view.ViewRootImpl").getDeclaredField("mAttachInfo");
                g.k.setAccessible(true);
                g.l.setAccessible(true);
            }
            catch (final ReflectiveOperationException ex) {
                final StringBuilder sb = new StringBuilder();
                sb.append("Failed to get visible insets. (Reflection error). ");
                sb.append(ex.getMessage());
                Log.e("WindowInsetsCompat", sb.toString(), (Throwable)ex);
            }
            g.h = true;
        }
        
        @Override
        public void d(final View view) {
            nf0 nf0;
            if ((nf0 = this.w(view)) == null) {
                nf0 = nf0.e;
            }
            this.q(nf0);
        }
        
        @Override
        public void e(final u62 u62) {
            u62.s(this.f);
            u62.r(this.g);
        }
        
        @Override
        public boolean equals(final Object o) {
            return super.equals(o) && Objects.equals(this.g, ((g)o).g);
        }
        
        @Override
        public nf0 g(final int n) {
            return this.t(n, false);
        }
        
        @Override
        public final nf0 k() {
            if (this.e == null) {
                this.e = nf0.b(this.c.getSystemWindowInsetLeft(), this.c.getSystemWindowInsetTop(), this.c.getSystemWindowInsetRight(), this.c.getSystemWindowInsetBottom());
            }
            return this.e;
        }
        
        @Override
        public u62 m(final int n, final int n2, final int n3, final int n4) {
            final b b = new b(u62.v(this.c));
            b.d(u62.n(this.k(), n, n2, n3, n4));
            b.c(u62.n(((l)this).i(), n, n2, n3, n4));
            return b.a();
        }
        
        @Override
        public boolean o() {
            return this.c.isRound();
        }
        
        @Override
        public void p(final nf0[] d) {
            this.d = d;
        }
        
        @Override
        public void q(final nf0 g) {
            this.g = g;
        }
        
        @Override
        public void r(final u62 f) {
            this.f = f;
        }
        
        public nf0 u(int n, final boolean b) {
            if (n != 1) {
                nf0 g = null;
                final nf0 nf0 = null;
                if (n != 2) {
                    if (n != 8) {
                        if (n == 16) {
                            return ((l)this).j();
                        }
                        if (n == 32) {
                            return ((l)this).h();
                        }
                        if (n == 64) {
                            return ((l)this).l();
                        }
                        if (n != 128) {
                            return nf0.e;
                        }
                        final u62 f = this.f;
                        rt rt;
                        if (f != null) {
                            rt = f.e();
                        }
                        else {
                            rt = ((l)this).f();
                        }
                        if (rt != null) {
                            return nf0.b(rt.b(), rt.d(), rt.c(), rt.a());
                        }
                        return nf0.e;
                    }
                    else {
                        final nf0[] d = this.d;
                        nf0 nf2 = nf0;
                        if (d != null) {
                            nf2 = d[m.b(8)];
                        }
                        if (nf2 != null) {
                            return nf2;
                        }
                        final nf0 k = this.k();
                        final nf0 v = this.v();
                        n = k.d;
                        if (n > v.d) {
                            return nf0.b(0, 0, 0, n);
                        }
                        final nf0 g2 = this.g;
                        if (g2 != null && !g2.equals(nf0.e)) {
                            n = this.g.d;
                            if (n > v.d) {
                                return nf0.b(0, 0, 0, n);
                            }
                        }
                        return nf0.e;
                    }
                }
                else {
                    if (b) {
                        final nf0 v2 = this.v();
                        final nf0 i = ((l)this).i();
                        return nf0.b(Math.max(v2.a, i.a), 0, Math.max(v2.c, i.c), Math.max(v2.d, i.d));
                    }
                    final nf0 j = this.k();
                    final u62 f2 = this.f;
                    if (f2 != null) {
                        g = f2.g();
                    }
                    final int a = n = j.d;
                    if (g != null) {
                        n = Math.min(a, g.d);
                    }
                    return nf0.b(j.a, 0, j.c, n);
                }
            }
            else {
                if (b) {
                    return nf0.b(0, Math.max(this.v().b, this.k().b), 0, 0);
                }
                return nf0.b(0, this.k().b, 0, 0);
            }
        }
    }
    
    public static class l
    {
        public static final u62 b;
        public final u62 a;
        
        static {
            b = new b().a().a().b().c();
        }
        
        public l(final u62 a) {
            this.a = a;
        }
        
        public u62 a() {
            return this.a;
        }
        
        public u62 b() {
            return this.a;
        }
        
        public u62 c() {
            return this.a;
        }
        
        public void d(final View view) {
        }
        
        public void e(final u62 u62) {
        }
        
        @Override
        public boolean equals(final Object o) {
            boolean b = true;
            if (this == o) {
                return true;
            }
            if (!(o instanceof l)) {
                return false;
            }
            final l l = (l)o;
            if (this.o() != l.o() || this.n() != l.n() || !c11.a(this.k(), l.k()) || !c11.a(this.i(), l.i()) || !c11.a(this.f(), l.f())) {
                b = false;
            }
            return b;
        }
        
        public rt f() {
            return null;
        }
        
        public nf0 g(final int n) {
            return nf0.e;
        }
        
        public nf0 h() {
            return this.k();
        }
        
        @Override
        public int hashCode() {
            return c11.b(this.o(), this.n(), this.k(), this.i(), this.f());
        }
        
        public nf0 i() {
            return nf0.e;
        }
        
        public nf0 j() {
            return this.k();
        }
        
        public nf0 k() {
            return nf0.e;
        }
        
        public nf0 l() {
            return this.k();
        }
        
        public u62 m(final int n, final int n2, final int n3, final int n4) {
            return l.b;
        }
        
        public boolean n() {
            return false;
        }
        
        public boolean o() {
            return false;
        }
        
        public void p(final nf0[] array) {
        }
        
        public void q(final nf0 nf0) {
        }
        
        public void r(final u62 u62) {
        }
        
        public void s(final nf0 nf0) {
        }
    }
    
    public static class h extends g
    {
        public nf0 m;
        
        public h(final u62 u62, final WindowInsets windowInsets) {
            super(u62, windowInsets);
            this.m = null;
        }
        
        public h(final u62 u62, final h h) {
            super(u62, (g)h);
            this.m = null;
            this.m = h.m;
        }
        
        @Override
        public u62 b() {
            return u62.v(super.c.consumeStableInsets());
        }
        
        @Override
        public u62 c() {
            return u62.v(super.c.consumeSystemWindowInsets());
        }
        
        @Override
        public final nf0 i() {
            if (this.m == null) {
                this.m = nf0.b(super.c.getStableInsetLeft(), super.c.getStableInsetTop(), super.c.getStableInsetRight(), super.c.getStableInsetBottom());
            }
            return this.m;
        }
        
        @Override
        public boolean n() {
            return super.c.isConsumed();
        }
        
        @Override
        public void s(final nf0 m) {
            this.m = m;
        }
    }
    
    public static class i extends h
    {
        public i(final u62 u62, final WindowInsets windowInsets) {
            super(u62, windowInsets);
        }
        
        public i(final u62 u62, final i i) {
            super(u62, (h)i);
        }
        
        @Override
        public u62 a() {
            return u62.v(f72.a(super.c));
        }
        
        @Override
        public boolean equals(final Object o) {
            boolean b = true;
            if (this == o) {
                return true;
            }
            if (!(o instanceof i)) {
                return false;
            }
            final i i = (i)o;
            if (!Objects.equals(super.c, i.c) || !Objects.equals(super.g, i.g)) {
                b = false;
            }
            return b;
        }
        
        @Override
        public rt f() {
            return rt.e(e72.a(super.c));
        }
        
        @Override
        public int hashCode() {
            return super.c.hashCode();
        }
    }
    
    public static class j extends i
    {
        public nf0 n;
        public nf0 o;
        public nf0 p;
        
        public j(final u62 u62, final WindowInsets windowInsets) {
            super(u62, windowInsets);
            this.n = null;
            this.o = null;
            this.p = null;
        }
        
        public j(final u62 u62, final j j) {
            super(u62, (i)j);
            this.n = null;
            this.o = null;
            this.p = null;
        }
        
        @Override
        public nf0 h() {
            if (this.o == null) {
                this.o = nf0.d(i72.a(super.c));
            }
            return this.o;
        }
        
        @Override
        public nf0 j() {
            if (this.n == null) {
                this.n = nf0.d(g72.a(super.c));
            }
            return this.n;
        }
        
        @Override
        public nf0 l() {
            if (this.p == null) {
                this.p = nf0.d(h72.a(super.c));
            }
            return this.p;
        }
        
        @Override
        public u62 m(final int n, final int n2, final int n3, final int n4) {
            return u62.v(j72.a(super.c, n, n2, n3, n4));
        }
        
        @Override
        public void s(final nf0 nf0) {
        }
    }
    
    public static class k extends j
    {
        public static final u62 q;
        
        static {
            q = u62.v(k72.a());
        }
        
        public k(final u62 u62, final WindowInsets windowInsets) {
            super(u62, windowInsets);
        }
        
        public k(final u62 u62, final k k) {
            super(u62, (j)k);
        }
        
        @Override
        public final void d(final View view) {
        }
        
        @Override
        public nf0 g(final int n) {
            return nf0.d(l72.a(super.c, u62.n.a(n)));
        }
    }
    
    public abstract static final class m
    {
        public static int a() {
            return 8;
        }
        
        public static int b(final int i) {
            if (i == 1) {
                return 0;
            }
            if (i == 2) {
                return 1;
            }
            if (i == 4) {
                return 2;
            }
            if (i == 8) {
                return 3;
            }
            if (i == 16) {
                return 4;
            }
            if (i == 32) {
                return 5;
            }
            if (i == 64) {
                return 6;
            }
            if (i == 128) {
                return 7;
            }
            if (i == 256) {
                return 8;
            }
            final StringBuilder sb = new StringBuilder();
            sb.append("type needs to be >= FIRST and <= LAST, type=");
            sb.append(i);
            throw new IllegalArgumentException(sb.toString());
        }
        
        public static int c() {
            return 32;
        }
        
        public static int d() {
            return 7;
        }
    }
    
    public abstract static final class n
    {
        public static int a(final int n) {
            int n2 = 0;
            int n3;
            for (int i = 1; i <= 256; i <<= 1, n2 = n3) {
                n3 = n2;
                if ((n & i) != 0x0) {
                    int n4;
                    if (i != 1) {
                        if (i != 2) {
                            if (i != 4) {
                                if (i != 8) {
                                    if (i != 16) {
                                        if (i != 32) {
                                            if (i != 64) {
                                                if (i != 128) {
                                                    n3 = n2;
                                                    continue;
                                                }
                                                n4 = t72.a();
                                            }
                                            else {
                                                n4 = s72.a();
                                            }
                                        }
                                        else {
                                            n4 = r72.a();
                                        }
                                    }
                                    else {
                                        n4 = q72.a();
                                    }
                                }
                                else {
                                    n4 = p72.a();
                                }
                            }
                            else {
                                n4 = o72.a();
                            }
                        }
                        else {
                            n4 = n72.a();
                        }
                    }
                    else {
                        n4 = m72.a();
                    }
                    n3 = (n2 | n4);
                }
            }
            return n2;
        }
    }
}

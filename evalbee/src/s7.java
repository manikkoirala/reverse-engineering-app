import android.os.Build$VERSION;
import android.content.pm.PackageInfo;
import android.content.Context;
import java.util.List;

// 
// Decompiled by Procyon v0.6.0
// 

public class s7
{
    public final String a;
    public final String b;
    public final List c;
    public final String d;
    public final String e;
    public final String f;
    public final String g;
    public final ts h;
    
    public s7(final String a, final String b, final List c, final String d, final String e, final String f, final String g, final ts h) {
        this.a = a;
        this.b = b;
        this.c = c;
        this.d = d;
        this.e = e;
        this.f = f;
        this.g = g;
        this.h = h;
    }
    
    public static s7 a(final Context context, final de0 de0, final String s, final String s2, final List list, final ts ts) {
        final String packageName = context.getPackageName();
        final String g = de0.g();
        final PackageInfo packageInfo = context.getPackageManager().getPackageInfo(packageName, 0);
        final String b = b(packageInfo);
        String versionName;
        if ((versionName = packageInfo.versionName) == null) {
            versionName = "0.0";
        }
        return new s7(s, s2, list, g, packageName, b, versionName, ts);
    }
    
    public static String b(final PackageInfo packageInfo) {
        if (Build$VERSION.SDK_INT >= 28) {
            return Long.toString(li2.a(packageInfo));
        }
        return Integer.toString(packageInfo.versionCode);
    }
}

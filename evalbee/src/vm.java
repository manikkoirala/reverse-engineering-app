import java.util.Objects;
import java.util.List;
import java.util.Collection;
import java.util.Collections;
import java.io.IOException;
import java.io.File;
import java.util.Comparator;
import java.io.FilenameFilter;

// 
// Decompiled by Procyon v0.6.0
// 

public class vm
{
    public static final FilenameFilter d;
    public static final Comparator e;
    public final z00 a;
    public String b;
    public String c;
    
    static {
        d = new tm();
        e = new um();
    }
    
    public vm(final z00 a) {
        this.b = null;
        this.c = null;
        this.a = a;
    }
    
    public static void f(final z00 z00, final String s, final String str) {
        if (s != null && str != null) {
            try {
                final StringBuilder sb = new StringBuilder();
                sb.append("aqs.");
                sb.append(str);
                z00.o(s, sb.toString()).createNewFile();
            }
            catch (final IOException ex) {
                zl0.f().l("Failed to persist App Quality Sessions session id.", ex);
            }
        }
    }
    
    public static String g(final z00 z00, final String s) {
        final List p2 = z00.p(s, vm.d);
        if (p2.isEmpty()) {
            zl0.f().k("Unable to read App Quality Sessions session id.");
            return null;
        }
        return ((File)Collections.min((Collection<?>)p2, (Comparator<? super Object>)vm.e)).getName().substring(4);
    }
    
    public String c(String b) {
        synchronized (this) {
            if (Objects.equals(this.b, b)) {
                b = this.c;
                return b;
            }
            b = g(this.a, b);
            return b;
        }
    }
    
    public void h(final String s) {
        synchronized (this) {
            if (!Objects.equals(this.c, s)) {
                f(this.a, this.b, s);
                this.c = s;
            }
        }
    }
    
    public void i(final String s) {
        synchronized (this) {
            if (!Objects.equals(this.b, s)) {
                f(this.a, s, this.c);
                this.b = s;
            }
        }
    }
}

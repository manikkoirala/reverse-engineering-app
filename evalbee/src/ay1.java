import android.content.Context;
import java.util.concurrent.Executor;
import android.content.SharedPreferences;
import java.lang.ref.WeakReference;

// 
// Decompiled by Procyon v0.6.0
// 

public final class ay1
{
    public static WeakReference d;
    public final SharedPreferences a;
    public pn1 b;
    public final Executor c;
    
    public ay1(final SharedPreferences a, final Executor c) {
        this.c = c;
        this.a = a;
    }
    
    public static ay1 a(final Context context, final Executor executor) {
        synchronized (ay1.class) {
            final WeakReference d = ay1.d;
            ay1 ay1;
            if (d != null) {
                ay1 = (ay1)d.get();
            }
            else {
                ay1 = null;
            }
            ay1 referent = ay1;
            if (ay1 == null) {
                referent = new ay1(context.getSharedPreferences("com.google.android.gms.appid", 0), executor);
                referent.c();
                ay1.d = new WeakReference(referent);
            }
            return referent;
        }
    }
    
    public jx1 b() {
        synchronized (this) {
            return jx1.a(this.b.e());
        }
    }
    
    public final void c() {
        synchronized (this) {
            this.b = pn1.c(this.a, "topic_operation_queue", ",", this.c);
        }
    }
    
    public boolean d(final jx1 jx1) {
        synchronized (this) {
            return this.b.f(jx1.e());
        }
    }
}

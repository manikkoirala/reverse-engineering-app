import com.google.firebase.sessions.DataCollectionState;

// 
// Decompiled by Procyon v0.6.0
// 

public final class gp
{
    public final DataCollectionState a;
    public final DataCollectionState b;
    public final double c;
    
    public gp(final DataCollectionState a, final DataCollectionState b, final double c) {
        fg0.e((Object)a, "performance");
        fg0.e((Object)b, "crashlytics");
        this.a = a;
        this.b = b;
        this.c = c;
    }
    
    public final DataCollectionState a() {
        return this.b;
    }
    
    public final DataCollectionState b() {
        return this.a;
    }
    
    public final double c() {
        return this.c;
    }
    
    @Override
    public boolean equals(final Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof gp)) {
            return false;
        }
        final gp gp = (gp)o;
        return this.a == gp.a && this.b == gp.b && fg0.a((Object)this.c, (Object)gp.c);
    }
    
    @Override
    public int hashCode() {
        return (this.a.hashCode() * 31 + this.b.hashCode()) * 31 + Double.hashCode(this.c);
    }
    
    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append("DataCollectionStatus(performance=");
        sb.append(this.a);
        sb.append(", crashlytics=");
        sb.append(this.b);
        sb.append(", sessionSamplingRate=");
        sb.append(this.c);
        sb.append(')');
        return sb.toString();
    }
}

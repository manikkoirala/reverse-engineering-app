import android.view.View;
import android.view.ViewGroup;
import android.view.LayoutInflater;
import androidx.appcompat.app.a;
import android.widget.TextView;
import android.content.Context;
import com.google.android.material.dialog.MaterialAlertDialogBuilder;

// 
// Decompiled by Procyon v0.6.0
// 

public class aa1 extends MaterialAlertDialogBuilder
{
    public int a;
    public Context b;
    public TextView c;
    public a d;
    
    public aa1(final Context b, final int a) {
        super(b);
        this.b = b;
        this.a = a;
        this.c();
    }
    
    public void a() {
        final a d = this.d;
        if (d != null && d.isShowing()) {
            this.d.dismiss();
        }
    }
    
    public void b(final int text) {
        this.c.setText(text);
        ((a.a)this).show();
    }
    
    public final void c() {
        final MaterialAlertDialogBuilder materialAlertDialogBuilder = new MaterialAlertDialogBuilder(this.b, 2131951953);
        final View inflate = ((LayoutInflater)this.b.getSystemService("layout_inflater")).inflate(2131492980, (ViewGroup)null);
        materialAlertDialogBuilder.setView(inflate);
        materialAlertDialogBuilder.setCancelable(false);
        (this.c = (TextView)inflate.findViewById(2131297262)).setText(this.a);
        (this.d = materialAlertDialogBuilder.create()).show();
    }
}

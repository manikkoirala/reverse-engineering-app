import android.os.BaseBundle;
import com.google.android.gms.internal.play_billing.zzet;
import android.content.pm.ServiceInfo;
import android.content.ComponentName;
import android.content.pm.ResolveInfo;
import java.util.Iterator;
import java.util.concurrent.TimeoutException;
import java.util.concurrent.CancellationException;
import android.os.Parcelable;
import android.app.PendingIntent;
import android.content.Intent;
import com.android.billingclient.api.ProxyBillingActivity;
import java.util.concurrent.TimeUnit;
import android.content.pm.PackageManager$NameNotFoundException;
import java.util.Arrays;
import com.google.android.gms.internal.play_billing.zzak;
import android.app.Activity;
import android.content.ServiceConnection;
import com.google.android.gms.internal.play_billing.zzx;
import java.util.Collection;
import com.google.android.gms.internal.play_billing.zzaf;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.Executors;
import android.os.Bundle;
import org.json.JSONException;
import android.text.TextUtils;
import com.android.billingclient.api.Purchase;
import java.util.List;
import com.android.billingclient.api.b;
import com.android.billingclient.api.c;
import java.util.ArrayList;
import java.util.concurrent.Future;
import java.util.concurrent.Callable;
import com.android.billingclient.api.a;
import com.google.android.gms.internal.play_billing.zzb;
import com.google.android.gms.internal.play_billing.zzin;
import com.google.android.gms.internal.play_billing.zzio;
import android.os.Looper;
import com.google.android.gms.internal.play_billing.zzm;
import android.content.Context;
import android.os.Handler;
import java.util.concurrent.ExecutorService;

// 
// Decompiled by Procyon v0.6.0
// 

public class xb extends wb
{
    public ExecutorService A;
    public volatile int a;
    public final String b;
    public final Handler c;
    public volatile zf2 d;
    public Context e;
    public dd2 f;
    public volatile zzm g;
    public volatile rb2 h;
    public boolean i;
    public boolean j;
    public int k;
    public boolean l;
    public boolean m;
    public boolean n;
    public boolean o;
    public boolean p;
    public boolean q;
    public boolean r;
    public boolean s;
    public boolean t;
    public boolean u;
    public boolean v;
    public boolean w;
    public boolean x;
    public ce2 y;
    public boolean z;
    
    public xb(String c, final Context context, final dd2 dd2, final ExecutorService executorService) {
        this.a = 0;
        this.c = new Handler(Looper.getMainLooper());
        this.k = 0;
        c = C();
        this.b = c;
        this.e = context.getApplicationContext();
        final zzin zzv = zzio.zzv();
        zzv.zzj(c);
        zzv.zzi(this.e.getPackageName());
        this.f = new md2(this.e, (zzio)((zzet)zzv).zzc());
        this.e.getPackageName();
    }
    
    public xb(String c, final ce2 ce2, final Context context, final ca1 ca1, final d4 d4, final dd2 dd2, final ExecutorService executorService) {
        c = C();
        this.a = 0;
        this.c = new Handler(Looper.getMainLooper());
        this.k = 0;
        this.h(context, ca1, ce2, d4, this.b = c, null);
    }
    
    public xb(final String s, final ce2 y, final Context context, final sd2 sd2, final dd2 dd2, final ExecutorService executorService) {
        this.a = 0;
        this.c = new Handler(Looper.getMainLooper());
        this.k = 0;
        this.b = C();
        this.e = context.getApplicationContext();
        final zzin zzv = zzio.zzv();
        zzv.zzj(C());
        zzv.zzi(this.e.getPackageName());
        this.f = new md2(this.e, (zzio)((zzet)zzv).zzc());
        zzb.zzk("BillingClient", "Billing client should have a valid listener but the provided is null.");
        this.d = new zf2(this.e, null, this.f);
        this.y = y;
        this.e.getPackageName();
    }
    
    public static String C() {
        try {
            return (String)Class.forName("com.android.billingclient.ktx.BuildConfig").getField("VERSION_NAME").get(null);
        }
        catch (final Exception ex) {
            return "6.1.0";
        }
    }
    
    public final com.android.billingclient.api.a B() {
        com.android.billingclient.api.a a;
        if (this.a != 0 && this.a != 3) {
            a = com.android.billingclient.api.b.j;
        }
        else {
            a = com.android.billingclient.api.b.m;
        }
        return a;
    }
    
    public final Future D(final Callable callable, final long n, final Runnable runnable, final Handler handler) {
        if (this.A == null) {
            this.A = Executors.newFixedThreadPool(zzb.zza, new ab2(this));
        }
        try {
            final Future<Object> submit = this.A.submit((Callable<Object>)callable);
            handler.postDelayed((Runnable)new ig2(submit, runnable), (long)(n * 0.95));
            return submit;
        }
        catch (final Exception ex) {
            zzb.zzl("BillingClient", "Async task throws exception!", (Throwable)ex);
            return null;
        }
    }
    
    public final void E(final String s, final ba1 ba1) {
        if (!this.b()) {
            final dd2 f = this.f;
            final com.android.billingclient.api.a m = com.android.billingclient.api.b.m;
            f.a(bd2.a(2, 9, m));
            ba1.a(m, (List)zzaf.zzk());
            return;
        }
        if (TextUtils.isEmpty((CharSequence)s)) {
            zzb.zzk("BillingClient", "Please provide a valid product type.");
            final dd2 f2 = this.f;
            final com.android.billingclient.api.a g = com.android.billingclient.api.b.g;
            f2.a(bd2.a(50, 9, g));
            ba1.a(g, (List)zzaf.zzk());
            return;
        }
        if (this.D(new fb2(this, s, ba1), 30000L, new bj2(this, ba1), this.y()) == null) {
            final com.android.billingclient.api.a b = this.B();
            this.f.a(bd2.a(25, 9, b));
            ba1.a(b, (List)zzaf.zzk());
        }
    }
    
    @Override
    public final void a() {
        this.f.c(bd2.b(12));
        try {
            try {
                if (this.d != null) {
                    this.d.e();
                }
                if (this.h != null) {
                    this.h.c();
                }
                if (this.h != null && this.g != null) {
                    zzb.zzj("BillingClient", "Unbinding from service.");
                    this.e.unbindService((ServiceConnection)this.h);
                    this.h = null;
                }
                this.g = null;
                final ExecutorService a = this.A;
                if (a != null) {
                    a.shutdownNow();
                    this.A = null;
                }
            }
            finally {}
        }
        catch (final Exception ex) {
            zzb.zzl("BillingClient", "There was an exception while ending connection!", (Throwable)ex);
        }
        this.a = 3;
        return;
        this.a = 3;
    }
    
    @Override
    public final boolean b() {
        return this.a == 2 && this.g != null && this.h != null;
    }
    
    @Override
    public final com.android.billingclient.api.a c(Activity ex, zb zb) {
        Label_1735: {
            if (this.d == null || this.d.d() == null) {
                break Label_1735;
            }
            if (!this.b()) {
                final dd2 f = this.f;
                final com.android.billingclient.api.a m = com.android.billingclient.api.b.m;
                f.a(bd2.a(2, 2, m));
                this.z(m);
                return m;
            }
            final ArrayList h = zb.h();
            final List i = zb.i();
            zu0.a(zzak.zza((Iterable)h, (Object)null));
            final zb.b b = (zb.b)zzak.zza((Iterable)i, (Object)null);
            final String b2 = b.b().b();
            final String c = b.b().c();
            final boolean equals = c.equals("subs");
            Object j = "BillingClient";
            if (equals && !this.i) {
                zzb.zzk("BillingClient", "Current client doesn't support subscriptions.");
                final dd2 f2 = this.f;
                final com.android.billingclient.api.a o = com.android.billingclient.api.b.o;
                f2.a(bd2.a(9, 2, o));
                this.z(o);
                return o;
            }
            if (zb.r() && !this.l) {
                zzb.zzk("BillingClient", "Current client doesn't support extra params for buy intent.");
                final dd2 f3 = this.f;
                final com.android.billingclient.api.a h2 = com.android.billingclient.api.b.h;
                f3.a(bd2.a(18, 2, h2));
                this.z(h2);
                return h2;
            }
            if (h.size() > 1 && !this.s) {
                zzb.zzk("BillingClient", "Current client doesn't support multi-item purchases.");
                final dd2 f4 = this.f;
                final com.android.billingclient.api.a t = com.android.billingclient.api.b.t;
                f4.a(bd2.a(19, 2, t));
                this.z(t);
                return t;
            }
            if (!i.isEmpty() && !this.t) {
                zzb.zzk("BillingClient", "Current client doesn't support purchases with ProductDetails.");
                final dd2 f5 = this.f;
                final com.android.billingclient.api.a v = com.android.billingclient.api.b.v;
                f5.a(bd2.a(20, 2, v));
                this.z(v);
                return v;
            }
            Label_1427: {
                if (!this.l) {
                    break Label_1427;
                }
                final boolean n = this.n;
                final boolean z = this.z;
                final String b3 = this.b;
                final Bundle bundle = new Bundle();
                ((BaseBundle)bundle).putString("playBillingLibraryVersion", b3);
                Label_0425: {
                    int n2;
                    if (zb.c() != 0) {
                        n2 = zb.c();
                    }
                    else {
                        if (zb.b() == 0) {
                            break Label_0425;
                        }
                        n2 = zb.b();
                    }
                    ((BaseBundle)bundle).putInt("prorationMode", n2);
                }
                if (!TextUtils.isEmpty((CharSequence)zb.d())) {
                    ((BaseBundle)bundle).putString("accountId", zb.d());
                }
                if (!TextUtils.isEmpty((CharSequence)zb.e())) {
                    ((BaseBundle)bundle).putString("obfuscatedProfileId", zb.e());
                }
                if (zb.q()) {
                    ((BaseBundle)bundle).putBoolean("isOfferPersonalizedByDeveloper", true);
                }
                if (!TextUtils.isEmpty((CharSequence)null)) {
                    bundle.putStringArrayList("skusToReplace", new ArrayList((Collection<? extends E>)Arrays.asList(null)));
                }
                if (!TextUtils.isEmpty((CharSequence)zb.f())) {
                    ((BaseBundle)bundle).putString("oldSkuPurchaseToken", zb.f());
                }
                if (!TextUtils.isEmpty((CharSequence)null)) {
                    ((BaseBundle)bundle).putString("oldSkuPurchaseId", (String)null);
                }
                if (!TextUtils.isEmpty((CharSequence)zb.g())) {
                    ((BaseBundle)bundle).putString("originalExternalTransactionId", zb.g());
                }
                if (!TextUtils.isEmpty((CharSequence)null)) {
                    ((BaseBundle)bundle).putString("paymentsPurchaseParams", (String)null);
                }
                if (n) {
                    ((BaseBundle)bundle).putBoolean("enablePendingPurchases", true);
                }
                if (z) {
                    ((BaseBundle)bundle).putBoolean("enableAlternativeBilling", true);
                }
                if (!h.isEmpty()) {
                    final ArrayList list = new ArrayList();
                    new ArrayList();
                    new ArrayList();
                    new ArrayList();
                    new ArrayList();
                    final Iterator iterator = h.iterator();
                    if (iterator.hasNext()) {
                        zu0.a(iterator.next());
                        throw null;
                    }
                    if (!list.isEmpty()) {
                        bundle.putStringArrayList("skuDetailsTokens", list);
                    }
                    if (h.size() > 1) {
                        final ArrayList list2 = new ArrayList(h.size() - 1);
                        final ArrayList list3 = new ArrayList(h.size() - 1);
                        if (1 < h.size()) {
                            zu0.a(h.get(1));
                            throw null;
                        }
                        bundle.putStringArrayList("additionalSkus", list2);
                        bundle.putStringArrayList("additionalSkuTypes", list3);
                    }
                    j = "BillingClient";
                }
                else {
                    final ArrayList list4 = new ArrayList<String>(i.size() - 1);
                    final ArrayList list5 = new ArrayList<String>(i.size() - 1);
                    final ArrayList list6 = new ArrayList();
                    final ArrayList list7 = new ArrayList();
                    final ArrayList list8 = new ArrayList();
                    for (int k = 0; k < i.size(); ++k) {
                        final zb.b b4 = i.get(k);
                        final t81 b5 = b4.b();
                        if (!b5.e().isEmpty()) {
                            list6.add(b5.e());
                        }
                        list7.add(b4.c());
                        if (!TextUtils.isEmpty((CharSequence)b5.f())) {
                            list8.add(b5.f());
                        }
                        if (k > 0) {
                            list4.add(((zb.b)i.get(k)).b().b());
                            list5.add(((zb.b)i.get(k)).b().c());
                        }
                    }
                    final Object o2 = j;
                    bundle.putStringArrayList("SKU_OFFER_ID_TOKEN_LIST", list7);
                    if (!list6.isEmpty()) {
                        bundle.putStringArrayList("skuDetailsTokens", list6);
                    }
                    if (!list8.isEmpty()) {
                        bundle.putStringArrayList("SKU_SERIALIZED_DOCID_LIST", list8);
                    }
                    j = o2;
                    if (!list4.isEmpty()) {
                        bundle.putStringArrayList("additionalSkus", list4);
                        bundle.putStringArrayList("additionalSkuTypes", list5);
                        j = o2;
                    }
                }
                if (((BaseBundle)bundle).containsKey("SKU_OFFER_ID_TOKEN_LIST") && !this.q) {
                    final dd2 f6 = this.f;
                    final com.android.billingclient.api.a u = com.android.billingclient.api.b.u;
                    f6.a(bd2.a(21, 2, u));
                    this.z(u);
                    return u;
                }
                int n3;
                if (b != null && !TextUtils.isEmpty((CharSequence)b.b().d())) {
                    ((BaseBundle)bundle).putString("skuPackageName", b.b().d());
                    n3 = 1;
                }
                else {
                    n3 = 0;
                }
                if (!TextUtils.isEmpty((CharSequence)null)) {
                    ((BaseBundle)bundle).putString("accountName", (String)null);
                }
                final Intent intent = ((Activity)ex).getIntent();
                String stringExtra;
                String versionName = null;
                Bundle bundle2;
                String s;
                String s2;
                Future future;
                int zzb;
                String zzg;
                dd2 f7;
                Intent intent2;
                Label_1459_Outer:Label_1318_Outer:
                while (true) {
                    if (intent == null) {
                        com.google.android.gms.internal.play_billing.zzb.zzk((String)j, "Activity's intent is null.");
                        break Label_1329;
                    }
                    if (TextUtils.isEmpty((CharSequence)intent.getStringExtra("PROXY_PACKAGE"))) {
                        break Label_1329;
                    }
                    stringExtra = intent.getStringExtra("PROXY_PACKAGE");
                    ((BaseBundle)bundle).putString("proxyPackage", stringExtra);
                    try {
                        versionName = this.e.getPackageManager().getPackageInfo(stringExtra, 0).versionName;
                        bundle2 = bundle;
                        s = "proxyPackageVersion";
                        s2 = versionName;
                        ((BaseBundle)bundle2).putString(s, s2);
                        break Label_1329;
                    }
                    catch (final PackageManager$NameNotFoundException ex2) {}
                    while (true) {
                        try {
                            bundle2 = bundle;
                            s = "proxyPackageVersion";
                            s2 = versionName;
                            ((BaseBundle)bundle2).putString(s, s2);
                            if (this.t && !i.isEmpty()) {
                                n3 = 17;
                            }
                            else if (this.r && n3 != 0) {
                                n3 = 15;
                            }
                            else if (this.n) {
                                n3 = 9;
                            }
                            else {
                                n3 = 6;
                            }
                            future = this.D(new qa2(this, n3, b2, c, zb, bundle), 5000L, null, this.c);
                            n3 = 78;
                            zb = (zb)j;
                        Label_1498_Outer:
                            while (true) {
                                while (true) {
                                    if (future == null) {
                                        try {
                                            ex = (Exception)this.f;
                                            j = com.android.billingclient.api.b.m;
                                            ((dd2)ex).a(bd2.a(25, 2, (com.android.billingclient.api.a)j));
                                            this.z((com.android.billingclient.api.a)j);
                                            return (com.android.billingclient.api.a)j;
                                        Block_57:
                                            while (true) {
                                                ex = (Exception)new StringBuilder();
                                                ((StringBuilder)ex).append("Unable to buy item, Error response code: ");
                                                ((StringBuilder)ex).append(zzb);
                                                com.google.android.gms.internal.play_billing.zzb.zzk((String)zb, ((StringBuilder)ex).toString());
                                                ex = (Exception)com.android.billingclient.api.b.a(zzb, zzg);
                                                f7 = this.f;
                                                iftrue(Label_1590:)(j == null);
                                                break Block_57;
                                                j = future.get(5000L, TimeUnit.MILLISECONDS);
                                                zzb = com.google.android.gms.internal.play_billing.zzb.zzb((Bundle)j, (String)zb);
                                                zzg = com.google.android.gms.internal.play_billing.zzb.zzg((Bundle)j, (String)zb);
                                                iftrue(Label_1611:)(zzb == 0);
                                                continue Label_1498_Outer;
                                            }
                                            n3 = 23;
                                            Label_1590: {
                                                f7.a(bd2.a(n3, 2, (com.android.billingclient.api.a)ex));
                                            }
                                            this.z((com.android.billingclient.api.a)ex);
                                            return (com.android.billingclient.api.a)ex;
                                            Label_1611:
                                            intent2 = new Intent((Context)ex, (Class)ProxyBillingActivity.class);
                                            intent2.putExtra("BUY_INTENT", (Parcelable)((Bundle)j).getParcelable("BUY_INTENT"));
                                            ((Activity)ex).startActivity(intent2);
                                            return com.android.billingclient.api.b.l;
                                        }
                                        catch (Exception ex) {
                                            com.google.android.gms.internal.play_billing.zzb.zzl((String)zb, "Exception while launching billing flow. Try to reconnect", (Throwable)ex);
                                            ex = (Exception)this.f;
                                            zb = (zb)com.android.billingclient.api.b.m;
                                            ((dd2)ex).a(bd2.a(5, 2, (com.android.billingclient.api.a)zb));
                                            this.z((com.android.billingclient.api.a)zb);
                                            return (com.android.billingclient.api.a)zb;
                                        }
                                        catch (final CancellationException ex) {}
                                        catch (final TimeoutException ex3) {}
                                        com.google.android.gms.internal.play_billing.zzb.zzl((String)zb, "Time out while launching billing flow. Try to reconnect", (Throwable)ex);
                                        zb = (zb)this.f;
                                        ex = (Exception)com.android.billingclient.api.b.n;
                                        ((dd2)zb).a(bd2.a(4, 2, (com.android.billingclient.api.a)ex));
                                        this.z((com.android.billingclient.api.a)ex);
                                        return (com.android.billingclient.api.a)ex;
                                    }
                                    continue Label_1318_Outer;
                                }
                                zb = (zb)"BillingClient";
                                future = this.D(new sa2(this, b2, c), 5000L, null, this.c);
                                n3 = 80;
                                continue Label_1498_Outer;
                            }
                            ((BaseBundle)bundle).putString("proxyPackageVersion", "package not found");
                            continue Label_1459_Outer;
                            ex = (Exception)this.f;
                            zb = (zb)com.android.billingclient.api.b.E;
                            ((dd2)ex).a(bd2.a(12, 2, (com.android.billingclient.api.a)zb));
                            return (com.android.billingclient.api.a)zb;
                        }
                        catch (final PackageManager$NameNotFoundException ex4) {
                            continue;
                        }
                        break;
                    }
                    break;
                }
            }
        }
    }
    
    @Override
    public final void e(final ha1 ha1, final u81 u81) {
        if (!this.b()) {
            final dd2 f = this.f;
            final com.android.billingclient.api.a m = com.android.billingclient.api.b.m;
            f.a(bd2.a(2, 7, m));
            u81.a(m, new ArrayList());
            return;
        }
        if (!this.t) {
            zzb.zzk("BillingClient", "Querying product details is not supported.");
            final dd2 f2 = this.f;
            final com.android.billingclient.api.a v = com.android.billingclient.api.b.v;
            f2.a(bd2.a(20, 7, v));
            u81.a(v, new ArrayList());
            return;
        }
        if (this.D(new nj2(this, ha1, u81), 30000L, new lk2(this, u81), this.y()) == null) {
            final com.android.billingclient.api.a b = this.B();
            this.f.a(bd2.a(25, 7, b));
            u81.a(b, new ArrayList());
        }
    }
    
    @Override
    public final void f(final ia1 ia1, final ba1 ba1) {
        this.E(ia1.b(), ba1);
    }
    
    @Override
    public final void g(final yb yb) {
        if (this.b()) {
            zzb.zzj("BillingClient", "Service connection is valid. No need to re-initialize.");
            this.f.c(bd2.b(6));
            yb.a(com.android.billingclient.api.b.l);
            return;
        }
        final int a = this.a;
        int n = 1;
        if (a == 1) {
            zzb.zzk("BillingClient", "Client is already in the process of connecting to billing service.");
            final dd2 f = this.f;
            final com.android.billingclient.api.a d = com.android.billingclient.api.b.d;
            f.a(bd2.a(37, 6, d));
            yb.a(d);
            return;
        }
        if (this.a == 3) {
            zzb.zzk("BillingClient", "Client was already closed and can't be reused. Please create another instance.");
            final dd2 f2 = this.f;
            final com.android.billingclient.api.a m = com.android.billingclient.api.b.m;
            f2.a(bd2.a(38, 6, m));
            yb.a(m);
            return;
        }
        this.a = 1;
        zzb.zzj("BillingClient", "Starting in-app billing setup.");
        this.h = new rb2(this, yb, null);
        final Intent intent = new Intent("com.android.vending.billing.InAppBillingService.BIND");
        intent.setPackage("com.android.vending");
        final List queryIntentServices = this.e.getPackageManager().queryIntentServices(intent, 0);
        if (queryIntentServices != null && !queryIntentServices.isEmpty()) {
            final ServiceInfo serviceInfo = queryIntentServices.get(0).serviceInfo;
            if (serviceInfo != null) {
                final String packageName = serviceInfo.packageName;
                final String name = serviceInfo.name;
                if ("com.android.vending".equals(packageName) && name != null) {
                    final ComponentName component = new ComponentName(packageName, name);
                    final Intent intent2 = new Intent(intent);
                    intent2.setComponent(component);
                    intent2.putExtra("playBillingLibraryVersion", this.b);
                    if (this.e.bindService(intent2, (ServiceConnection)this.h, 1)) {
                        zzb.zzj("BillingClient", "Service was bonded successfully.");
                        return;
                    }
                    zzb.zzk("BillingClient", "Connection to Billing service is blocked.");
                    n = 39;
                }
                else {
                    zzb.zzk("BillingClient", "The device doesn't have valid Play Store.");
                    n = 40;
                }
            }
        }
        else {
            n = 41;
        }
        this.a = 0;
        zzb.zzj("BillingClient", "Billing service unavailable on device.");
        final dd2 f3 = this.f;
        final com.android.billingclient.api.a c = com.android.billingclient.api.b.c;
        f3.a(bd2.a(n, 6, c));
        yb.a(c);
    }
    
    public final void h(final Context context, final ca1 ca1, final ce2 y, final d4 d4, final String s, dd2 f) {
        this.e = context.getApplicationContext();
        final zzin zzv = zzio.zzv();
        zzv.zzj(s);
        zzv.zzi(this.e.getPackageName());
        if (f == null) {
            f = new md2(this.e, (zzio)((zzet)zzv).zzc());
        }
        this.f = f;
        if (ca1 == null) {
            zzb.zzk("BillingClient", "Billing client should have a valid listener but the provided is null.");
        }
        this.d = new zf2(this.e, ca1, d4, this.f);
        this.y = y;
        this.z = (d4 != null);
        this.e.getPackageName();
    }
    
    public final Handler y() {
        Handler c;
        if (Looper.myLooper() == null) {
            c = this.c;
        }
        else {
            c = new Handler(Looper.myLooper());
        }
        return c;
    }
    
    public final com.android.billingclient.api.a z(final com.android.billingclient.api.a a) {
        if (Thread.interrupted()) {
            return a;
        }
        this.c.post((Runnable)new dg2(this, a));
        return a;
    }
}

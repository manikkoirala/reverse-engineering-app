import android.graphics.drawable.Drawable;
import android.view.DragEvent;
import android.view.inputmethod.InputConnection;
import android.view.inputmethod.EditorInfo;
import android.text.method.KeyListener;
import android.os.Build$VERSION;
import android.text.Editable;
import android.graphics.PorterDuff$Mode;
import android.content.res.ColorStateList;
import android.view.ActionMode$Callback;
import android.view.textclassifier.TextClassifier;
import android.widget.TextView;
import android.view.View;
import android.util.AttributeSet;
import android.content.Context;
import android.widget.EditText;

// 
// Decompiled by Procyon v0.6.0
// 

public class t6 extends EditText implements q11
{
    private final u6 mAppCompatEmojiEditTextHelper;
    private final x5 mBackgroundTintHelper;
    private final ov1 mDefaultOnReceiveContentListener;
    private a mSuperCaller;
    private final k7 mTextClassifierHelper;
    private final l7 mTextHelper;
    
    public t6(final Context context, final AttributeSet set) {
        this(context, set, sa1.A);
    }
    
    public t6(final Context context, final AttributeSet set, final int n) {
        super(qw1.b(context), set, n);
        pv1.a((View)this, ((View)this).getContext());
        (this.mBackgroundTintHelper = new x5((View)this)).e(set, n);
        final l7 mTextHelper = new l7((TextView)this);
        (this.mTextHelper = mTextHelper).m(set, n);
        mTextHelper.b();
        this.mTextClassifierHelper = new k7((TextView)this);
        this.mDefaultOnReceiveContentListener = new ov1();
        final u6 mAppCompatEmojiEditTextHelper = new u6(this);
        (this.mAppCompatEmojiEditTextHelper = mAppCompatEmojiEditTextHelper).d(set, n);
        this.initEmojiKeyListener(mAppCompatEmojiEditTextHelper);
    }
    
    public static /* synthetic */ TextClassifier access$001(final t6 t6) {
        return t6.getTextClassifier();
    }
    
    public static /* synthetic */ void access$101(final t6 t6, final TextClassifier textClassifier) {
        t6.setTextClassifier(textClassifier);
    }
    
    private a getSuperCaller() {
        if (this.mSuperCaller == null) {
            this.mSuperCaller = new a();
        }
        return this.mSuperCaller;
    }
    
    public void drawableStateChanged() {
        super.drawableStateChanged();
        final x5 mBackgroundTintHelper = this.mBackgroundTintHelper;
        if (mBackgroundTintHelper != null) {
            mBackgroundTintHelper.b();
        }
        final l7 mTextHelper = this.mTextHelper;
        if (mTextHelper != null) {
            mTextHelper.b();
        }
    }
    
    public ActionMode$Callback getCustomSelectionActionModeCallback() {
        return nv1.q(super.getCustomSelectionActionModeCallback());
    }
    
    public ColorStateList getSupportBackgroundTintList() {
        final x5 mBackgroundTintHelper = this.mBackgroundTintHelper;
        ColorStateList c;
        if (mBackgroundTintHelper != null) {
            c = mBackgroundTintHelper.c();
        }
        else {
            c = null;
        }
        return c;
    }
    
    public PorterDuff$Mode getSupportBackgroundTintMode() {
        final x5 mBackgroundTintHelper = this.mBackgroundTintHelper;
        PorterDuff$Mode d;
        if (mBackgroundTintHelper != null) {
            d = mBackgroundTintHelper.d();
        }
        else {
            d = null;
        }
        return d;
    }
    
    public ColorStateList getSupportCompoundDrawablesTintList() {
        return this.mTextHelper.j();
    }
    
    public PorterDuff$Mode getSupportCompoundDrawablesTintMode() {
        return this.mTextHelper.k();
    }
    
    public Editable getText() {
        if (Build$VERSION.SDK_INT >= 28) {
            return super.getText();
        }
        return super.getEditableText();
    }
    
    public TextClassifier getTextClassifier() {
        if (Build$VERSION.SDK_INT < 28) {
            final k7 mTextClassifierHelper = this.mTextClassifierHelper;
            if (mTextClassifierHelper != null) {
                return mTextClassifierHelper.a();
            }
        }
        return this.getSuperCaller().a();
    }
    
    public void initEmojiKeyListener(final u6 u6) {
        final KeyListener keyListener = ((TextView)this).getKeyListener();
        if (u6.b(keyListener)) {
            final boolean focusable = super.isFocusable();
            final boolean clickable = super.isClickable();
            final boolean longClickable = super.isLongClickable();
            final int inputType = super.getInputType();
            final KeyListener a = u6.a(keyListener);
            if (a == keyListener) {
                return;
            }
            super.setKeyListener(a);
            super.setRawInputType(inputType);
            super.setFocusable(focusable);
            super.setClickable(clickable);
            super.setLongClickable(longClickable);
        }
    }
    
    public boolean isEmojiCompatEnabled() {
        return this.mAppCompatEmojiEditTextHelper.c();
    }
    
    public InputConnection onCreateInputConnection(final EditorInfo editorInfo) {
        final InputConnection onCreateInputConnection = super.onCreateInputConnection(editorInfo);
        this.mTextHelper.r((TextView)this, onCreateInputConnection, editorInfo);
        InputConnection inputConnection2;
        final InputConnection inputConnection = inputConnection2 = w6.a(onCreateInputConnection, editorInfo, (View)this);
        if (inputConnection != null) {
            inputConnection2 = inputConnection;
            if (Build$VERSION.SDK_INT <= 30) {
                final String[] e = o32.E((View)this);
                inputConnection2 = inputConnection;
                if (e != null) {
                    ew.d(editorInfo, e);
                    inputConnection2 = if0.c((View)this, inputConnection, editorInfo);
                }
            }
        }
        return this.mAppCompatEmojiEditTextHelper.e(inputConnection2, editorInfo);
    }
    
    public boolean onDragEvent(final DragEvent dragEvent) {
        return f7.a((View)this, dragEvent) || super.onDragEvent(dragEvent);
    }
    
    public gl onReceiveContent(final gl gl) {
        return this.mDefaultOnReceiveContentListener.a((View)this, gl);
    }
    
    public boolean onTextContextMenuItem(final int n) {
        return f7.b((TextView)this, n) || super.onTextContextMenuItem(n);
    }
    
    public void setBackgroundDrawable(final Drawable backgroundDrawable) {
        super.setBackgroundDrawable(backgroundDrawable);
        final x5 mBackgroundTintHelper = this.mBackgroundTintHelper;
        if (mBackgroundTintHelper != null) {
            mBackgroundTintHelper.f(backgroundDrawable);
        }
    }
    
    public void setBackgroundResource(final int backgroundResource) {
        super.setBackgroundResource(backgroundResource);
        final x5 mBackgroundTintHelper = this.mBackgroundTintHelper;
        if (mBackgroundTintHelper != null) {
            mBackgroundTintHelper.g(backgroundResource);
        }
    }
    
    public void setCompoundDrawables(final Drawable drawable, final Drawable drawable2, final Drawable drawable3, final Drawable drawable4) {
        super.setCompoundDrawables(drawable, drawable2, drawable3, drawable4);
        final l7 mTextHelper = this.mTextHelper;
        if (mTextHelper != null) {
            mTextHelper.p();
        }
    }
    
    public void setCompoundDrawablesRelative(final Drawable drawable, final Drawable drawable2, final Drawable drawable3, final Drawable drawable4) {
        super.setCompoundDrawablesRelative(drawable, drawable2, drawable3, drawable4);
        final l7 mTextHelper = this.mTextHelper;
        if (mTextHelper != null) {
            mTextHelper.p();
        }
    }
    
    public void setCustomSelectionActionModeCallback(final ActionMode$Callback actionMode$Callback) {
        super.setCustomSelectionActionModeCallback(nv1.r((TextView)this, actionMode$Callback));
    }
    
    public void setEmojiCompatEnabled(final boolean b) {
        this.mAppCompatEmojiEditTextHelper.f(b);
    }
    
    public void setKeyListener(final KeyListener keyListener) {
        super.setKeyListener(this.mAppCompatEmojiEditTextHelper.a(keyListener));
    }
    
    public void setSupportBackgroundTintList(final ColorStateList list) {
        final x5 mBackgroundTintHelper = this.mBackgroundTintHelper;
        if (mBackgroundTintHelper != null) {
            mBackgroundTintHelper.i(list);
        }
    }
    
    public void setSupportBackgroundTintMode(final PorterDuff$Mode porterDuff$Mode) {
        final x5 mBackgroundTintHelper = this.mBackgroundTintHelper;
        if (mBackgroundTintHelper != null) {
            mBackgroundTintHelper.j(porterDuff$Mode);
        }
    }
    
    public void setSupportCompoundDrawablesTintList(final ColorStateList list) {
        this.mTextHelper.w(list);
        this.mTextHelper.b();
    }
    
    public void setSupportCompoundDrawablesTintMode(final PorterDuff$Mode porterDuff$Mode) {
        this.mTextHelper.x(porterDuff$Mode);
        this.mTextHelper.b();
    }
    
    public void setTextAppearance(final Context context, final int n) {
        super.setTextAppearance(context, n);
        final l7 mTextHelper = this.mTextHelper;
        if (mTextHelper != null) {
            mTextHelper.q(context, n);
        }
    }
    
    public void setTextClassifier(final TextClassifier textClassifier) {
        if (Build$VERSION.SDK_INT < 28) {
            final k7 mTextClassifierHelper = this.mTextClassifierHelper;
            if (mTextClassifierHelper != null) {
                mTextClassifierHelper.b(textClassifier);
                return;
            }
        }
        this.getSuperCaller().b(textClassifier);
    }
    
    public class a
    {
        public final t6 a;
        
        public a(final t6 a) {
            this.a = a;
        }
        
        public TextClassifier a() {
            return t6.access$001(this.a);
        }
        
        public void b(final TextClassifier textClassifier) {
            t6.access$101(this.a, textClassifier);
        }
    }
}

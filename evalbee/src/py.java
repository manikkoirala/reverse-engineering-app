import java.util.logging.Level;
import java.util.concurrent.Executor;
import java.util.logging.Logger;

// 
// Decompiled by Procyon v0.6.0
// 

public final class py
{
    public static final Logger c;
    public a a;
    public boolean b;
    
    static {
        c = Logger.getLogger(py.class.getName());
    }
    
    public static void c(final Runnable obj, final Executor obj2) {
        try {
            obj2.execute(obj);
        }
        catch (final RuntimeException thrown) {
            final Logger c = py.c;
            final Level severe = Level.SEVERE;
            final String value = String.valueOf(obj);
            final String value2 = String.valueOf(obj2);
            final StringBuilder sb = new StringBuilder(value.length() + 57 + value2.length());
            sb.append("RuntimeException while executing runnable ");
            sb.append(value);
            sb.append(" with executor ");
            sb.append(value2);
            c.log(severe, sb.toString(), thrown);
        }
    }
    
    public void a(final Runnable runnable, final Executor executor) {
        i71.s(runnable, "Runnable was null.");
        i71.s(executor, "Executor was null.");
        synchronized (this) {
            if (!this.b) {
                this.a = new a(runnable, executor, this.a);
                return;
            }
            monitorexit(this);
            c(runnable, executor);
        }
    }
    
    public void b() {
        synchronized (this) {
            if (this.b) {
                return;
            }
            this.b = true;
            a a = this.a;
            a c = null;
            this.a = null;
            monitorexit(this);
            a c2;
            while (true) {
                c2 = c;
                if (a == null) {
                    break;
                }
                final a c3 = a.c;
                a.c = c;
                c = a;
                a = c3;
            }
            while (c2 != null) {
                c(c2.a, c2.b);
                c2 = c2.c;
            }
        }
    }
    
    public static final class a
    {
        public final Runnable a;
        public final Executor b;
        public a c;
        
        public a(final Runnable a, final Executor b, final a c) {
            this.a = a;
            this.b = b;
            this.c = c;
        }
    }
}

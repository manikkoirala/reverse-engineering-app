import android.os.BaseBundle;
import android.os.Bundle;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager$NameNotFoundException;
import android.content.SharedPreferences;
import android.content.Context;

// 
// Decompiled by Procyon v0.6.0
// 

public class ep
{
    public final Context a;
    public final SharedPreferences b;
    public final y91 c;
    public boolean d;
    
    public ep(final Context context, final String str, final y91 c) {
        final Context a = a(context);
        this.a = a;
        final StringBuilder sb = new StringBuilder();
        sb.append("com.google.firebase.common.prefs:");
        sb.append(str);
        this.b = a.getSharedPreferences(sb.toString(), 0);
        this.c = c;
        this.d = this.c();
    }
    
    public static Context a(final Context context) {
        return sl.createDeviceProtectedStorageContext(context);
    }
    
    public boolean b() {
        synchronized (this) {
            return this.d;
        }
    }
    
    public final boolean c() {
        if (this.b.contains("firebase_data_collection_default_enabled")) {
            return this.b.getBoolean("firebase_data_collection_default_enabled", true);
        }
        return this.d();
    }
    
    public final boolean d() {
        try {
            final PackageManager packageManager = this.a.getPackageManager();
            if (packageManager != null) {
                final ApplicationInfo applicationInfo = packageManager.getApplicationInfo(this.a.getPackageName(), 128);
                if (applicationInfo != null) {
                    final Bundle metaData = applicationInfo.metaData;
                    if (metaData != null && ((BaseBundle)metaData).containsKey("firebase_data_collection_default_enabled")) {
                        return ((BaseBundle)applicationInfo.metaData).getBoolean("firebase_data_collection_default_enabled");
                    }
                }
            }
            return true;
        }
        catch (final PackageManager$NameNotFoundException ex) {
            return true;
        }
    }
}

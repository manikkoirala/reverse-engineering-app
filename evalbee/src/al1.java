import android.widget.CompoundButton;
import android.content.SharedPreferences$Editor;
import android.content.SharedPreferences;
import java.io.Serializable;
import com.ekodroid.omrevaluator.exams.sms.SmsPublishService;
import android.widget.RadioGroup$OnCheckedChangeListener;
import android.widget.RadioGroup;
import android.os.Bundle;
import androidx.appcompat.widget.Toolbar;
import android.content.Intent;
import com.ekodroid.omrevaluator.more.BuySmsCreditActivity;
import android.view.View;
import android.view.View$OnClickListener;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.FileOutputStream;
import java.io.File;
import java.util.Iterator;
import com.ekodroid.omrevaluator.exams.sms.SmsMessage;
import com.ekodroid.omrevaluator.exams.sms.SmsListDataModel;
import android.widget.CheckBox;
import android.widget.Button;
import android.widget.TextView;
import android.widget.RadioButton;
import java.util.ArrayList;
import com.ekodroid.omrevaluator.templateui.models.ExamId;
import android.content.Context;

// 
// Decompiled by Procyon v0.6.0
// 

public class al1 extends u80
{
    public Context a;
    public ExamId b;
    public ArrayList c;
    public int d;
    public RadioButton e;
    public RadioButton f;
    public TextView g;
    public TextView h;
    public TextView i;
    public Button j;
    public Button k;
    public ArrayList l;
    public CheckBox m;
    public y01 n;
    public int p;
    
    public al1(final Context a, final ExamId b, final y01 n, final ArrayList c, final int d, final int p6) {
        super(a);
        this.l = new ArrayList();
        this.a = a;
        this.n = n;
        this.c = c;
        this.d = d;
        this.b = b;
        this.p = p6;
    }
    
    public static /* synthetic */ y01 c(final al1 al1) {
        return al1.n;
    }
    
    public final boolean d(final String s) {
        return s.length() == 10 || (s.length() == 12 && s.substring(0, 2).equals("91"));
    }
    
    public final void e() {
        final boolean checked = ((CompoundButton)this.e).isChecked();
        this.l = new ArrayList();
        final Iterator iterator = this.c.iterator();
        int i = 0;
        int j = 0;
        while (iterator.hasNext()) {
            final SmsListDataModel smsListDataModel = (SmsListDataModel)iterator.next();
            if (!smsListDataModel.getPhoneNo().equals("") && this.d(smsListDataModel.getPhoneNo())) {
                SmsMessage e;
                if (checked) {
                    ++i;
                    e = new SmsMessage(smsListDataModel, this.p);
                }
                else {
                    if (smsListDataModel.isSmsSent()) {
                        continue;
                    }
                    ++i;
                    e = new SmsMessage(smsListDataModel, this.p);
                }
                this.l.add(e);
                j += e.getCredits();
            }
        }
        if (i < 1 || this.d < 1) {
            ((View)this.j).setEnabled(false);
        }
        final TextView g = this.g;
        final StringBuilder sb = new StringBuilder();
        sb.append(this.a.getString(2131886892));
        sb.append(" : ");
        sb.append(i);
        g.setText((CharSequence)sb.toString());
        final TextView h = this.h;
        final StringBuilder sb2 = new StringBuilder();
        sb2.append(this.a.getString(2131886741));
        sb2.append(" : ");
        sb2.append(j);
        h.setText((CharSequence)sb2.toString());
        final TextView k = this.i;
        final StringBuilder sb3 = new StringBuilder();
        sb3.append(this.a.getString(2131886152));
        sb3.append(" : ");
        sb3.append(this.d);
        k.setText((CharSequence)sb3.toString());
    }
    
    public final boolean f(final ArrayList ex) {
        final StringBuilder sb = new StringBuilder();
        sb.append(this.a.getFilesDir());
        sb.append("/tempsms.json");
        final File file = new File(sb.toString());
        try {
            final FileOutputStream fileOutputStream = new FileOutputStream(file);
            fileOutputStream.write(new gc0().s(ex).getBytes());
            fileOutputStream.flush();
            fileOutputStream.close();
            return true;
        }
        catch (final IOException ex) {}
        catch (final FileNotFoundException ex2) {}
        ex.printStackTrace();
        return false;
    }
    
    public final void g() {
        if (this.d > 0) {
            this.findViewById(2131296420).setVisibility(8);
        }
        else {
            this.findViewById(2131296420).setOnClickListener((View$OnClickListener)new View$OnClickListener(this) {
                public final al1 a;
                
                public void onClick(final View view) {
                    final Intent intent = new Intent(this.a.a, (Class)BuySmsCreditActivity.class);
                    intent.putExtra("SMS_CREDIT", this.a.d);
                    this.a.a.startActivity(intent);
                    this.a.dismiss();
                }
            });
        }
    }
    
    public final void h() {
        ((Toolbar)this.findViewById(2131297358)).setNavigationOnClickListener((View$OnClickListener)new View$OnClickListener(this) {
            public final al1 a;
            
            public void onClick(final View view) {
                this.a.dismiss();
            }
        });
    }
    
    @Override
    public void onCreate(final Bundle bundle) {
        super.onCreate(bundle);
        this.setContentView(2131492997);
        this.h();
        this.e = (RadioButton)this.findViewById(2131296998);
        this.f = (RadioButton)this.findViewById(2131296997);
        this.g = (TextView)this.findViewById(2131297254);
        this.h = (TextView)this.findViewById(2131297221);
        this.i = (TextView)this.findViewById(2131297220);
        this.m = (CheckBox)this.findViewById(2131296514);
        this.j = (Button)this.findViewById(2131296404);
        this.k = (Button)this.findViewById(2131296393);
        ((RadioGroup)this.findViewById(2131297004)).setOnCheckedChangeListener((RadioGroup$OnCheckedChangeListener)new RadioGroup$OnCheckedChangeListener(this) {
            public final al1 a;
            
            public void onCheckedChanged(final RadioGroup radioGroup, final int n) {
                this.a.e();
            }
        });
        this.g();
        ((View)this.k).setOnClickListener((View$OnClickListener)new View$OnClickListener(this) {
            public final al1 a;
            
            public void onClick(final View view) {
                this.a.dismiss();
            }
        });
        ((View)this.j).setOnClickListener((View$OnClickListener)new View$OnClickListener(this) {
            public final al1 a;
            
            public void onClick(final View view) {
                final al1 a = this.a;
                if (a.f(a.l)) {
                    final SharedPreferences sharedPreferences = this.a.a.getSharedPreferences("MyPref", 0);
                    final SharedPreferences$Editor edit = sharedPreferences.edit();
                    final StringBuilder sb = new StringBuilder();
                    sb.append(this.a.b.getExamName());
                    sb.append("_sentSMSCount");
                    edit.putInt(sb.toString(), 0).commit();
                    final SharedPreferences$Editor edit2 = sharedPreferences.edit();
                    final StringBuilder sb2 = new StringBuilder();
                    sb2.append(this.a.b.getExamName());
                    sb2.append("_lastSMSIndexSent");
                    edit2.putInt(sb2.toString(), -1).commit();
                    final Intent intent = new Intent(this.a.a, (Class)SmsPublishService.class);
                    intent.putExtra("EXAM_ID", (Serializable)this.a.b);
                    intent.putExtra("SEND_SMS_SUMMARY", ((CompoundButton)this.a.m).isChecked());
                    this.a.a.startService(intent);
                    al1.c(this.a).a("CLOSE_ACTIVITY");
                    this.a.dismiss();
                }
                else {
                    a91.G(this.a.a, 2131886502, 2131230909, 2131231086);
                }
            }
        });
        this.e();
    }
}

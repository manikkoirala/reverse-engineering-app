import android.view.View;
import android.view.ViewGroup;
import android.database.Cursor;
import android.content.Context;
import android.view.LayoutInflater;

// 
// Decompiled by Procyon v0.6.0
// 

public abstract class ie1 extends jo
{
    public int i;
    public int j;
    public LayoutInflater k;
    
    public ie1(final Context context, final int n, final Cursor cursor, final boolean b) {
        super(context, cursor, b);
        this.j = n;
        this.i = n;
        this.k = (LayoutInflater)context.getSystemService("layout_inflater");
    }
    
    @Override
    public View g(final Context context, final Cursor cursor, final ViewGroup viewGroup) {
        return this.k.inflate(this.j, viewGroup, false);
    }
    
    @Override
    public View h(final Context context, final Cursor cursor, final ViewGroup viewGroup) {
        return this.k.inflate(this.i, viewGroup, false);
    }
}

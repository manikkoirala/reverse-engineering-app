import android.content.res.TypedArray;
import android.graphics.Color;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.content.res.ColorStateList;
import android.util.Log;
import android.content.Context;
import android.view.View;

// 
// Decompiled by Procyon v0.6.0
// 

public abstract class pv1
{
    public static final ThreadLocal a;
    public static final int[] b;
    public static final int[] c;
    public static final int[] d;
    public static final int[] e;
    public static final int[] f;
    public static final int[] g;
    public static final int[] h;
    public static final int[] i;
    public static final int[] j;
    
    static {
        a = new ThreadLocal();
        b = new int[] { -16842910 };
        c = new int[] { 16842908 };
        d = new int[] { 16843518 };
        e = new int[] { 16842919 };
        f = new int[] { 16842912 };
        g = new int[] { 16842913 };
        h = new int[] { -16842919, -16842908 };
        i = new int[0];
        j = new int[1];
    }
    
    public static void a(final View view, Context obtainStyledAttributes) {
        obtainStyledAttributes = (Context)obtainStyledAttributes.obtainStyledAttributes(bc1.y0);
        try {
            if (!((TypedArray)obtainStyledAttributes).hasValue(bc1.D0)) {
                final StringBuilder sb = new StringBuilder();
                sb.append("View ");
                sb.append(view.getClass());
                sb.append(" is an AppCompat widget that can only be used with a Theme.AppCompat theme (or descendant).");
                Log.e("ThemeUtils", sb.toString());
            }
        }
        finally {
            ((TypedArray)obtainStyledAttributes).recycle();
        }
    }
    
    public static int b(final Context context, final int n) {
        final ColorStateList e = e(context, n);
        if (e != null && e.isStateful()) {
            return e.getColorForState(pv1.b, e.getDefaultColor());
        }
        final TypedValue f = f();
        context.getTheme().resolveAttribute(16842803, f, true);
        return d(context, n, f.getFloat());
    }
    
    public static int c(Context u, int b) {
        final int[] j = pv1.j;
        j[0] = b;
        u = (Context)tw1.u(u, null, j);
        try {
            b = ((tw1)u).b(0, 0);
            return b;
        }
        finally {
            ((tw1)u).w();
        }
    }
    
    public static int d(final Context context, int c, final float n) {
        c = c(context, c);
        return ci.k(c, Math.round(Color.alpha(c) * n));
    }
    
    public static ColorStateList e(Context u, final int n) {
        final int[] j = pv1.j;
        j[0] = n;
        u = (Context)tw1.u(u, null, j);
        try {
            return ((tw1)u).c(0);
        }
        finally {
            ((tw1)u).w();
        }
    }
    
    public static TypedValue f() {
        final ThreadLocal a = pv1.a;
        TypedValue value;
        if ((value = a.get()) == null) {
            value = new TypedValue();
            a.set(value);
        }
        return value;
    }
}

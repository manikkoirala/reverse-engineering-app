import androidx.datastore.preferences.protobuf.x;
import androidx.datastore.preferences.protobuf.w;

// 
// Decompiled by Procyon v0.6.0
// 

public abstract class an0
{
    public static final w a;
    public static final w b;
    
    static {
        a = c();
        b = new x();
    }
    
    public static w a() {
        return an0.a;
    }
    
    public static w b() {
        return an0.b;
    }
    
    public static w c() {
        try {
            return (w)Class.forName("androidx.datastore.preferences.protobuf.MapFieldSchemaFull").getDeclaredConstructor((Class<?>[])new Class[0]).newInstance(new Object[0]);
        }
        catch (final Exception ex) {
            return null;
        }
    }
}

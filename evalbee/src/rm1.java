// 
// Decompiled by Procyon v0.6.0
// 

public final class rm1
{
    public static final rm1 c;
    public static final rm1 d;
    public final boolean a;
    public final q00 b;
    
    static {
        c = new rm1(false, null);
        d = new rm1(true, null);
    }
    
    public rm1(final boolean a, final q00 b) {
        k71.a(b == null || a, "Cannot specify a fieldMask for non-merge sets()", new Object[0]);
        this.a = a;
        this.b = b;
    }
    
    public static rm1 c() {
        return rm1.d;
    }
    
    public q00 a() {
        return this.b;
    }
    
    public boolean b() {
        return this.a;
    }
    
    @Override
    public boolean equals(final Object o) {
        boolean equals = true;
        if (this == o) {
            return true;
        }
        if (o == null || rm1.class != o.getClass()) {
            return false;
        }
        final rm1 rm1 = (rm1)o;
        if (this.a != rm1.a) {
            return false;
        }
        final q00 b = this.b;
        final q00 b2 = rm1.b;
        if (b != null) {
            equals = b.equals(b2);
        }
        else if (b2 != null) {
            equals = false;
        }
        return equals;
    }
    
    @Override
    public int hashCode() {
        final int a = this.a ? 1 : 0;
        final q00 b = this.b;
        int hashCode;
        if (b != null) {
            hashCode = b.hashCode();
        }
        else {
            hashCode = 0;
        }
        return a * 31 + hashCode;
    }
}

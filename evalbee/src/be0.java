import java.util.concurrent.Callable;
import androidx.work.impl.WorkDatabase;

// 
// Decompiled by Procyon v0.6.0
// 

public final class be0
{
    public final WorkDatabase a;
    
    public be0(final WorkDatabase a) {
        fg0.e((Object)a, "workDatabase");
        this.a = a;
    }
    
    public static final Integer d(final be0 be0) {
        fg0.e((Object)be0, "this$0");
        return ce0.a(be0.a, "next_alarm_manager_id");
    }
    
    public static final Integer f(final be0 be0, int i, final int n) {
        fg0.e((Object)be0, "this$0");
        final int a = ce0.a(be0.a, "next_job_scheduler_id");
        int n2 = 0;
        if (i <= a) {
            n2 = n2;
            if (a <= n) {
                n2 = 1;
            }
        }
        if (n2 == 0) {
            ce0.b(be0.a, "next_job_scheduler_id", i + 1);
        }
        else {
            i = a;
        }
        return i;
    }
    
    public final int c() {
        final Object c = this.a.C(new zd0(this));
        fg0.d(c, "workDatabase.runInTransa\u2026ANAGER_ID_KEY)\n        })");
        return ((Number)c).intValue();
    }
    
    public final int e(final int n, final int n2) {
        final Object c = this.a.C(new ae0(this, n, n2));
        fg0.d(c, "workDatabase.runInTransa\u2026            id\n        })");
        return ((Number)c).intValue();
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

public final class ia1
{
    public final String a = ia1.a.c(a);
    
    public static a a() {
        return new a(null);
    }
    
    public final String b() {
        return this.a;
    }
    
    public static class a
    {
        public String a;
        
        public ia1 a() {
            if (this.a != null) {
                return new ia1(this, null);
            }
            throw new IllegalArgumentException("Product type must be set");
        }
        
        public a b(final String a) {
            this.a = a;
            return this;
        }
    }
}

import java.util.ArrayList;
import android.view.accessibility.AccessibilityNodeInfo;
import android.view.accessibility.AccessibilityNodeProvider;
import java.util.List;
import android.os.Bundle;
import android.os.Build$VERSION;

// 
// Decompiled by Procyon v0.6.0
// 

public class o1
{
    public final Object a;
    
    public o1() {
        a a;
        if (Build$VERSION.SDK_INT >= 26) {
            a = new c(this);
        }
        else {
            a = new b(this);
        }
        this.a = a;
    }
    
    public o1(final Object a) {
        this.a = a;
    }
    
    public void a(final int n, final n1 n2, final String s, final Bundle bundle) {
    }
    
    public n1 b(final int n) {
        return null;
    }
    
    public List c(final String s, final int n) {
        return null;
    }
    
    public n1 d(final int n) {
        return null;
    }
    
    public Object e() {
        return this.a;
    }
    
    public boolean f(final int n, final int n2, final Bundle bundle) {
        return false;
    }
    
    public abstract static class a extends AccessibilityNodeProvider
    {
        public final o1 a;
        
        public a(final o1 a) {
            this.a = a;
        }
        
        public AccessibilityNodeInfo createAccessibilityNodeInfo(final int n) {
            final n1 b = this.a.b(n);
            if (b == null) {
                return null;
            }
            return b.A0();
        }
        
        public List findAccessibilityNodeInfosByText(final String s, int i) {
            final List c = this.a.c(s, i);
            if (c == null) {
                return null;
            }
            final ArrayList list = new ArrayList();
            int size;
            for (size = c.size(), i = 0; i < size; ++i) {
                list.add(c.get(i).A0());
            }
            return list;
        }
        
        public boolean performAction(final int n, final int n2, final Bundle bundle) {
            return this.a.f(n, n2, bundle);
        }
    }
    
    public static class b extends a
    {
        public b(final o1 o1) {
            super(o1);
        }
        
        public AccessibilityNodeInfo findFocus(final int n) {
            final n1 d = super.a.d(n);
            if (d == null) {
                return null;
            }
            return d.A0();
        }
    }
    
    public static class c extends b
    {
        public c(final o1 o1) {
            super(o1);
        }
        
        public void addExtraDataToAccessibilityNodeInfo(final int n, final AccessibilityNodeInfo accessibilityNodeInfo, final String s, final Bundle bundle) {
            super.a.a(n, n1.B0(accessibilityNodeInfo), s, bundle);
        }
    }
}

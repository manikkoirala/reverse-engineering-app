import java.util.Collection;
import java.util.Iterator;
import android.view.ViewTreeObserver$OnPreDrawListener;
import android.view.View$OnAttachStateChangeListener;
import java.lang.ref.WeakReference;
import android.view.View;
import android.view.ViewGroup;
import java.util.ArrayList;

// 
// Decompiled by Procyon v0.6.0
// 

public abstract class sy1
{
    public static qy1 a;
    public static ThreadLocal b;
    public static ArrayList c;
    
    static {
        sy1.a = new ha();
        sy1.b = new ThreadLocal();
        sy1.c = new ArrayList();
    }
    
    public static void a(final ViewGroup viewGroup, qy1 clone) {
        if (!sy1.c.contains(viewGroup) && o32.U((View)viewGroup)) {
            sy1.c.add(viewGroup);
            qy1 a;
            if ((a = clone) == null) {
                a = sy1.a;
            }
            clone = a.clone();
            d(viewGroup, clone);
            gj1.b(viewGroup, null);
            c(viewGroup, clone);
        }
    }
    
    public static r8 b() {
        final WeakReference weakReference = sy1.b.get();
        if (weakReference != null) {
            final r8 r8 = (r8)weakReference.get();
            if (r8 != null) {
                return r8;
            }
        }
        final r8 referent = new r8();
        sy1.b.set(new WeakReference(referent));
        return referent;
    }
    
    public static void c(final ViewGroup viewGroup, final qy1 qy1) {
        if (qy1 != null && viewGroup != null) {
            final a a = new a(qy1, viewGroup);
            ((View)viewGroup).addOnAttachStateChangeListener((View$OnAttachStateChangeListener)a);
            ((View)viewGroup).getViewTreeObserver().addOnPreDrawListener((ViewTreeObserver$OnPreDrawListener)a);
        }
    }
    
    public static void d(final ViewGroup viewGroup, final qy1 qy1) {
        final ArrayList list = (ArrayList)b().get(viewGroup);
        if (list != null && list.size() > 0) {
            final Iterator iterator = list.iterator();
            while (iterator.hasNext()) {
                ((qy1)iterator.next()).pause((View)viewGroup);
            }
        }
        if (qy1 != null) {
            qy1.captureValues(viewGroup, true);
        }
        gj1.a(viewGroup);
    }
    
    public static class a implements ViewTreeObserver$OnPreDrawListener, View$OnAttachStateChangeListener
    {
        public qy1 a;
        public ViewGroup b;
        
        public a(final qy1 a, final ViewGroup b) {
            this.a = a;
            this.b = b;
        }
        
        public final void a() {
            ((View)this.b).getViewTreeObserver().removeOnPreDrawListener((ViewTreeObserver$OnPreDrawListener)this);
            ((View)this.b).removeOnAttachStateChangeListener((View$OnAttachStateChangeListener)this);
        }
        
        public boolean onPreDraw() {
            this.a();
            if (!sy1.c.remove(this.b)) {
                return true;
            }
            final r8 b = sy1.b();
            final ArrayList c = (ArrayList)b.get(this.b);
            ArrayList list = null;
            ArrayList list2;
            if (c == null) {
                list2 = new ArrayList();
                b.put(this.b, list2);
            }
            else {
                list2 = c;
                if (c.size() > 0) {
                    list = new ArrayList(c);
                    list2 = c;
                }
            }
            list2.add(this.a);
            this.a.addListener((qy1.g)new ry1(this, b) {
                public final r8 a;
                public final a b;
                
                @Override
                public void onTransitionEnd(final qy1 o) {
                    ((ArrayList)this.a.get(this.b.b)).remove(o);
                    o.removeListener((qy1.g)this);
                }
            });
            this.a.captureValues(this.b, false);
            if (list != null) {
                final Iterator iterator = list.iterator();
                while (iterator.hasNext()) {
                    ((qy1)iterator.next()).resume((View)this.b);
                }
            }
            this.a.playTransition(this.b);
            return true;
        }
        
        public void onViewAttachedToWindow(final View view) {
        }
        
        public void onViewDetachedFromWindow(final View view) {
            this.a();
            sy1.c.remove(this.b);
            final ArrayList list = (ArrayList)sy1.b().get(this.b);
            if (list != null && list.size() > 0) {
                final Iterator iterator = list.iterator();
                while (iterator.hasNext()) {
                    ((qy1)iterator.next()).resume((View)this.b);
                }
            }
            this.a.clearValues(true);
        }
    }
}

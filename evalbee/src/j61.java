import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.android.volley.d;
import com.ekodroid.omrevaluator.serializable.EmailData;

// 
// Decompiled by Procyon v0.6.0
// 

public class j61
{
    public ee1 a;
    public y01 b;
    public EmailData c;
    public String d;
    
    public j61(final EmailData c, final ee1 a, final y01 b) {
        final StringBuilder sb = new StringBuilder();
        sb.append("https://");
        sb.append(a91.u());
        sb.append(":8759/query");
        this.d = sb.toString();
        this.a = a;
        this.b = b;
        this.c = c;
        this.d();
    }
    
    public static /* synthetic */ EmailData b(final j61 j61) {
        return j61.c;
    }
    
    public final void c(final Object o) {
        final y01 b = this.b;
        if (b != null) {
            b.a(o);
        }
    }
    
    public final void d() {
        final hr1 hr1 = new hr1(this, 1, this.d, new d.b(this) {
            public final j61 a;
            
            public void b(final String s) {
                this.a.c("Success");
            }
        }, new d.a(this) {
            public final j61 a;
            
            @Override
            public void a(final VolleyError volleyError) {
                this.a.c(null);
            }
        }) {
            public final j61 w;
            
            @Override
            public byte[] k() {
                return new gc0().s(j61.b(this.w)).getBytes();
            }
            
            @Override
            public String l() {
                return "application/json";
            }
        };
        hr1.L(new wq(30000, 0, 1.0f));
        this.a.a(hr1);
    }
}

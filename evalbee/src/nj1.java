import java.util.Collection;
import java.util.Iterator;
import androidx.work.impl.background.systemjob.SystemJobService;
import android.content.Context;
import androidx.work.impl.WorkDatabase;
import androidx.work.a;
import java.util.List;
import java.util.concurrent.Executor;

// 
// Decompiled by Procyon v0.6.0
// 

public abstract class nj1
{
    public static final String a;
    
    static {
        a = xl0.i("Schedulers");
    }
    
    public static ij1 c(final Context context, final WorkDatabase workDatabase, final a a) {
        final pt1 pt1 = new pt1(context, workDatabase, a);
        n21.c(context, SystemJobService.class, true);
        xl0.e().a(nj1.a, "Created SystemJobScheduler and enabled SystemJobService");
        return pt1;
    }
    
    public static void f(final q92 q92, final ch ch, final List list) {
        if (list.size() > 0) {
            final long currentTimeMillis = ch.currentTimeMillis();
            final Iterator iterator = list.iterator();
            while (iterator.hasNext()) {
                q92.v(((p92)iterator.next()).a, currentTimeMillis);
            }
        }
    }
    
    public static void g(final List list, final q81 q81, final Executor executor, final WorkDatabase workDatabase, final a a) {
        q81.e(new lj1(executor, list, a, workDatabase));
    }
    
    public static void h(final a a, WorkDatabase iterator, final List list) {
        if (list != null) {
            if (list.size() != 0) {
                final q92 k = iterator.K();
                iterator.e();
                try {
                    final List l = k.l();
                    f(k, a.a(), l);
                    final List x = k.x(a.h());
                    f(k, a.a(), x);
                    if (l != null) {
                        x.addAll(l);
                    }
                    final List h = k.h(200);
                    iterator.D();
                    iterator.i();
                    if (x.size() > 0) {
                        final p92[] array = x.toArray(new p92[x.size()]);
                        iterator = (WorkDatabase)list.iterator();
                        while (((Iterator)iterator).hasNext()) {
                            final ij1 ij1 = ((Iterator<ij1>)iterator).next();
                            if (ij1.b()) {
                                ij1.a(array);
                            }
                        }
                    }
                    if (h.size() > 0) {
                        final p92[] array2 = h.toArray(new p92[h.size()]);
                        final Iterator iterator2 = list.iterator();
                        while (iterator2.hasNext()) {
                            iterator = (WorkDatabase)iterator2.next();
                            if (!((ij1)iterator).b()) {
                                ((ij1)iterator).a(array2);
                            }
                        }
                    }
                }
                finally {
                    iterator.i();
                }
            }
        }
    }
}

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.TimeZone;
import java.util.Locale;
import com.google.firebase.firestore.remote.f;
import java.text.SimpleDateFormat;

// 
// Decompiled by Procyon v0.6.0
// 

public class jd
{
    public final SimpleDateFormat a;
    public final f b;
    
    public jd(final f b) {
        this.b = b;
        final SimpleDateFormat a = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss", Locale.ENGLISH);
        this.a = a;
        final GregorianCalendar calendar = new GregorianCalendar(TimeZone.getTimeZone("UTC"));
        calendar.setGregorianChange(new Date(Long.MIN_VALUE));
        a.setCalendar(calendar);
    }
}

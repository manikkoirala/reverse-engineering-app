import java.util.Objects;
import com.google.firebase.firestore.remote.i;
import com.google.protobuf.ByteString;
import com.google.firebase.firestore.local.QueryPurpose;
import com.google.firebase.firestore.core.q;

// 
// Decompiled by Procyon v0.6.0
// 

public final class au1
{
    public final q a;
    public final int b;
    public final long c;
    public final QueryPurpose d;
    public final qo1 e;
    public final qo1 f;
    public final ByteString g;
    public final Integer h;
    
    public au1(final q q, final int n, final long n2, final QueryPurpose queryPurpose) {
        final qo1 b = qo1.b;
        this(q, n, n2, queryPurpose, b, b, i.t, null);
    }
    
    public au1(final q q, final int b, final long c, final QueryPurpose d, final qo1 qo1, final qo1 f, final ByteString byteString, final Integer h) {
        this.a = (q)k71.b(q);
        this.b = b;
        this.c = c;
        this.f = f;
        this.d = d;
        this.e = (qo1)k71.b(qo1);
        this.g = (ByteString)k71.b(byteString);
        this.h = h;
    }
    
    public Integer a() {
        return this.h;
    }
    
    public qo1 b() {
        return this.f;
    }
    
    public QueryPurpose c() {
        return this.d;
    }
    
    public ByteString d() {
        return this.g;
    }
    
    public long e() {
        return this.c;
    }
    
    @Override
    public boolean equals(final Object o) {
        boolean b = true;
        if (this == o) {
            return true;
        }
        if (o != null && au1.class == o.getClass()) {
            final au1 au1 = (au1)o;
            if (!this.a.equals(au1.a) || this.b != au1.b || this.c != au1.c || !this.d.equals(au1.d) || !this.e.equals(au1.e) || !this.f.equals(au1.f) || !this.g.equals(au1.g) || !Objects.equals(this.h, au1.h)) {
                b = false;
            }
            return b;
        }
        return false;
    }
    
    public qo1 f() {
        return this.e;
    }
    
    public q g() {
        return this.a;
    }
    
    public int h() {
        return this.b;
    }
    
    @Override
    public int hashCode() {
        return ((((((this.a.hashCode() * 31 + this.b) * 31 + (int)this.c) * 31 + this.d.hashCode()) * 31 + this.e.hashCode()) * 31 + this.f.hashCode()) * 31 + this.g.hashCode()) * 31 + Objects.hashCode(this.h);
    }
    
    public au1 i(final Integer n) {
        return new au1(this.a, this.b, this.c, this.d, this.e, this.f, this.g, n);
    }
    
    public au1 j(final qo1 qo1) {
        return new au1(this.a, this.b, this.c, this.d, this.e, qo1, this.g, this.h);
    }
    
    public au1 k(final ByteString byteString, final qo1 qo1) {
        return new au1(this.a, this.b, this.c, this.d, qo1, this.f, byteString, null);
    }
    
    public au1 l(final long n) {
        return new au1(this.a, this.b, n, this.d, this.e, this.f, this.g, this.h);
    }
    
    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append("TargetData{target=");
        sb.append(this.a);
        sb.append(", targetId=");
        sb.append(this.b);
        sb.append(", sequenceNumber=");
        sb.append(this.c);
        sb.append(", purpose=");
        sb.append(this.d);
        sb.append(", snapshotVersion=");
        sb.append(this.e);
        sb.append(", lastLimboFreeSnapshotVersion=");
        sb.append(this.f);
        sb.append(", resumeToken=");
        sb.append(this.g);
        sb.append(", expectedCount=");
        sb.append(this.h);
        sb.append('}');
        return sb.toString();
    }
}

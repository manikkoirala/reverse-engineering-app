import com.google.android.gms.common.internal.safeparcel.SafeParcelWriter;
import android.os.Parcel;
import android.os.Parcelable$Creator;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;

// 
// Decompiled by Procyon v0.6.0
// 

public class f51 extends AbstractSafeParcelable
{
    public static final Parcelable$Creator<f51> CREATOR;
    
    static {
        CREATOR = (Parcelable$Creator)new df2();
    }
    
    public static f51 i() {
        return new f51();
    }
    
    public void writeToParcel(final Parcel parcel, final int n) {
        SafeParcelWriter.finishObjectHeader(parcel, SafeParcelWriter.beginObjectHeader(parcel));
    }
}

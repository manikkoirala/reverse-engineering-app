import android.os.Parcelable;
import com.google.android.gms.common.internal.safeparcel.SafeParcelWriter;
import android.os.Parcel;
import java.util.Iterator;
import java.util.ArrayList;
import java.util.List;
import android.os.Parcelable$Creator;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;

// 
// Decompiled by Procyon v0.6.0
// 

public final class cd2 extends AbstractSafeParcelable
{
    public static final Parcelable$Creator<cd2> CREATOR;
    public final List a;
    public final List b;
    
    static {
        CREATOR = (Parcelable$Creator)new ad2();
    }
    
    public cd2(List b, final List list) {
        List a = b;
        if (b == null) {
            a = new ArrayList();
        }
        this.a = a;
        if ((b = list) == null) {
            b = new ArrayList();
        }
        this.b = b;
    }
    
    public static cd2 E(final List list) {
        if (list != null && !list.isEmpty()) {
            final ArrayList list2 = new ArrayList();
            final ArrayList list3 = new ArrayList();
            for (final gx0 gx0 : list) {
                if (gx0 instanceof i51) {
                    list2.add(gx0);
                }
                else {
                    if (!(gx0 instanceof fy1)) {
                        continue;
                    }
                    list3.add(gx0);
                }
            }
            return new cd2(list2, list3);
        }
        return null;
    }
    
    public final List i() {
        final ArrayList list = new ArrayList();
        final Iterator iterator = this.a.iterator();
        while (iterator.hasNext()) {
            list.add(iterator.next());
        }
        final Iterator iterator2 = this.b.iterator();
        while (iterator2.hasNext()) {
            list.add(iterator2.next());
        }
        return list;
    }
    
    public final void writeToParcel(final Parcel parcel, int beginObjectHeader) {
        beginObjectHeader = SafeParcelWriter.beginObjectHeader(parcel);
        SafeParcelWriter.writeTypedList(parcel, 1, (List<Parcelable>)this.a, false);
        SafeParcelWriter.writeTypedList(parcel, 2, (List<Parcelable>)this.b, false);
        SafeParcelWriter.finishObjectHeader(parcel, beginObjectHeader);
    }
}

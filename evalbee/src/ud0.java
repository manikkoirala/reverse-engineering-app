import android.os.IBinder;
import android.os.Binder;
import android.os.IInterface;

// 
// Decompiled by Procyon v0.6.0
// 

public interface ud0 extends IInterface
{
    public abstract static class a extends Binder implements ud0
    {
        public static ud0 p(final IBinder binder) {
            if (binder == null) {
                return null;
            }
            final IInterface queryLocalInterface = binder.queryLocalInterface("com.google.firebase.dynamiclinks.internal.IDynamicLinksService");
            if (queryLocalInterface != null && queryLocalInterface instanceof ud0) {
                return (ud0)queryLocalInterface;
            }
            return new ud0.a.a(binder);
        }
        
        public static class a implements ud0
        {
            public IBinder a;
            
            public a(final IBinder a) {
                this.a = a;
            }
            
            public IBinder asBinder() {
                return this.a;
            }
        }
    }
}

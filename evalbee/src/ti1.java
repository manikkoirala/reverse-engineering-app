import androidx.appcompat.app.a;
import android.app.Dialog;
import android.widget.TextView;
import android.view.View$OnFocusChangeListener;
import android.widget.AdapterView;
import android.widget.AdapterView$OnItemClickListener;
import android.widget.Button;
import android.os.Bundle;
import android.content.DialogInterface;
import android.content.DialogInterface$OnClickListener;
import com.google.android.material.dialog.MaterialAlertDialogBuilder;
import androidx.appcompat.widget.Toolbar;
import android.widget.ListAdapter;
import com.ekodroid.omrevaluator.database.repositories.ClassRepository;
import com.ekodroid.omrevaluator.database.repositories.TemplateRepository;
import android.app.DatePickerDialog;
import android.widget.DatePicker;
import android.app.DatePickerDialog$OnDateSetListener;
import java.util.Calendar;
import java.text.SimpleDateFormat;
import android.view.View;
import android.view.View$OnClickListener;
import com.ekodroid.omrevaluator.templateui.models.ExamId;
import java.util.ArrayList;
import android.widget.AutoCompleteTextView;
import com.google.android.material.textfield.TextInputLayout;
import com.ekodroid.omrevaluator.templateui.models.SheetTemplate2;
import android.content.Context;

// 
// Decompiled by Procyon v0.6.0
// 

public class ti1 extends u80
{
    public Context a;
    public SheetTemplate2 b;
    public y01 c;
    public TextInputLayout d;
    public AutoCompleteTextView e;
    public TextInputLayout f;
    public ArrayList g;
    public ExamId h;
    public View$OnClickListener i;
    
    public ti1(final Context a, final y01 c, final SheetTemplate2 b, final ExamId h) {
        super(a);
        this.i = (View$OnClickListener)new View$OnClickListener(this) {
            public final ti1 a;
            
            public void onClick(final View view) {
                a91.B(this.a.a, view);
                final SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
                final Calendar instance = Calendar.getInstance();
                ((Dialog)new DatePickerDialog(this.a.a, (DatePickerDialog$OnDateSetListener)new DatePickerDialog$OnDateSetListener(this, instance, simpleDateFormat) {
                    public final Calendar a;
                    public final SimpleDateFormat b;
                    public final ti1$a c;
                    
                    public void onDateSet(final DatePicker datePicker, final int year, final int month, final int date) {
                        this.a.set(year, month, date);
                        ((TextView)this.c.a.d.getEditText()).setText((CharSequence)this.b.format(this.a.getTime()));
                    }
                }, instance.get(1), instance.get(2), instance.get(5))).show();
            }
        };
        this.a = a;
        this.c = c;
        this.b = b;
        this.h = h;
    }
    
    public static /* synthetic */ ArrayList a(final ti1 ti1) {
        return ti1.g;
    }
    
    public static /* synthetic */ AutoCompleteTextView b(final ti1 ti1) {
        return ti1.e;
    }
    
    public static /* synthetic */ y01 f(final ti1 ti1) {
        return ti1.c;
    }
    
    public final boolean g(final ExamId examId) {
        return TemplateRepository.getInstance(this.a).getTemplateJson(examId) != null;
    }
    
    public final void h() {
        (this.g = ClassRepository.getInstance(this.a).getAllClassNames()).add(this.a.getString(2131886120));
        this.e.setAdapter((ListAdapter)new h3(this.a, this.g));
    }
    
    public final void i() {
        final Toolbar toolbar = (Toolbar)this.findViewById(2131297357);
        toolbar.setTitle(2131886760);
        toolbar.setNavigationOnClickListener((View$OnClickListener)new View$OnClickListener(this) {
            public final ti1 a;
            
            public void onClick(final View view) {
                this.a.dismiss();
            }
        });
    }
    
    public final void j(final ExamId examId) {
        final MaterialAlertDialogBuilder materialAlertDialogBuilder = new MaterialAlertDialogBuilder(this.a, 2131951953);
        materialAlertDialogBuilder.setTitle(2131886129);
        final StringBuilder sb = new StringBuilder();
        sb.append(examId.getExamName());
        sb.append(" ");
        sb.append(this.a.getResources().getString(2131886534));
        materialAlertDialogBuilder.setMessage((CharSequence)sb.toString());
        materialAlertDialogBuilder.setPositiveButton((CharSequence)"Yes", (DialogInterface$OnClickListener)new DialogInterface$OnClickListener(this, examId) {
            public final ExamId a;
            public final ti1 b;
            
            public void onClick(final DialogInterface dialogInterface, final int n) {
                final ti1 b = this.b;
                if (xu1.d(b.a, this.a, b.b, null)) {
                    a91.G(this.b.a, 2131886292, 2131230927, 2131231085);
                    ti1.f(this.b).a(this.a);
                }
                dialogInterface.dismiss();
            }
        }).setNegativeButton((CharSequence)"No", (DialogInterface$OnClickListener)new DialogInterface$OnClickListener(this) {
            public final ti1 a;
            
            public void onClick(final DialogInterface dialogInterface, final int n) {
                dialogInterface.dismiss();
            }
        }).create();
        ((a.a)materialAlertDialogBuilder).show();
    }
    
    @Override
    public void onCreate(final Bundle bundle) {
        super.onCreate(bundle);
        this.setContentView(2131492987);
        this.i();
        final TextInputLayout textInputLayout = (TextInputLayout)this.findViewById(2131297175);
        this.f = (TextInputLayout)this.findViewById(2131297172);
        this.d = (TextInputLayout)this.findViewById(2131297174);
        this.e = (AutoCompleteTextView)this.f.getEditText();
        this.h();
        final Button button = (Button)this.findViewById(2131296462);
        if (this.h != null) {
            ((TextView)textInputLayout.getEditText()).setText((CharSequence)this.h.getExamName());
            ((TextView)this.d.getEditText()).setText((CharSequence)this.h.getExamDate());
        }
        this.e.setOnItemClickListener((AdapterView$OnItemClickListener)new AdapterView$OnItemClickListener(this) {
            public final ti1 a;
            
            public void onItemClick(final AdapterView adapterView, final View view, final int index, final long n) {
                if (ti1.a(this.a).get(index).equals(this.a.a.getString(2131886120))) {
                    ((TextView)ti1.b(this.a)).setText((CharSequence)null);
                    new t3(this.a.a, new y01(this) {
                        public final ti1$c a;
                        
                        @Override
                        public void a(final Object o) {
                            a91.G(this.a.a.a, 2131886653, 2131230927, 2131231085);
                            this.a.a.h();
                            ti1.b(this.a.a).showDropDown();
                        }
                    });
                }
            }
        });
        ((View)this.d.getEditText()).setOnFocusChangeListener((View$OnFocusChangeListener)new View$OnFocusChangeListener(this) {
            public final ti1 a;
            
            public void onFocusChange(final View view, final boolean b) {
                if (b) {
                    a91.B(this.a.a, view);
                }
            }
        });
        ((TextView)this.d.getEditText()).setInputType(0);
        ((View)this.d.getEditText()).setOnClickListener(this.i);
        this.d.setEndIconOnClickListener(this.i);
        ((View)button).setOnClickListener((View$OnClickListener)new View$OnClickListener(this, textInputLayout) {
            public final TextInputLayout a;
            public final ti1 b;
            
            public void onClick(final View view) {
                final String string = ((TextView)ti1.b(this.b)).getEditableText().toString();
                final String string2 = this.a.getEditText().getText().toString();
                final String string3 = this.b.d.getEditText().getText().toString();
                if (string.trim().equals("")) {
                    a91.G(this.b.a, 2131886262, 2131230909, 2131231086);
                    return;
                }
                if (string2.trim().equals("")) {
                    a91.G(this.b.a, 2131886264, 2131230909, 2131231086);
                    return;
                }
                if (string3.trim().equals("")) {
                    a91.G(this.b.a, 2131886263, 2131230909, 2131231086);
                    return;
                }
                final ExamId examId = new ExamId(string2, string, string3);
                if (this.b.g(examId)) {
                    this.b.j(examId);
                    this.b.dismiss();
                    return;
                }
                final ti1 b = this.b;
                if (xu1.d(b.a, examId, b.b, null)) {
                    a91.G(this.b.a, 2131886292, 2131230927, 2131231085);
                    ti1.f(this.b).a(new ExamId(string2, string, string3));
                }
                this.b.dismiss();
            }
        });
        this.findViewById(2131296392).setOnClickListener((View$OnClickListener)new View$OnClickListener(this) {
            public final ti1 a;
            
            public void onClick(final View view) {
                this.a.dismiss();
            }
        });
    }
}

import android.view.accessibility.AccessibilityRecord;
import android.text.InputFilter;
import android.graphics.drawable.Drawable;
import android.view.accessibility.AccessibilityNodeInfo;
import android.view.accessibility.AccessibilityEvent;
import android.graphics.PorterDuff$Mode;
import android.content.res.ColorStateList;
import android.view.ActionMode$Callback;
import android.widget.TextView;
import android.view.View;
import android.util.AttributeSet;
import android.content.Context;
import android.widget.Button;

// 
// Decompiled by Procyon v0.6.0
// 

public class y5 extends Button
{
    private v6 mAppCompatEmojiTextHelper;
    private final x5 mBackgroundTintHelper;
    private final l7 mTextHelper;
    
    public y5(final Context context, final AttributeSet set) {
        this(context, set, sa1.n);
    }
    
    public y5(final Context context, final AttributeSet set, final int n) {
        super(qw1.b(context), set, n);
        pv1.a((View)this, ((View)this).getContext());
        (this.mBackgroundTintHelper = new x5((View)this)).e(set, n);
        final l7 mTextHelper = new l7((TextView)this);
        (this.mTextHelper = mTextHelper).m(set, n);
        mTextHelper.b();
        this.getEmojiTextViewHelper().c(set, n);
    }
    
    private v6 getEmojiTextViewHelper() {
        if (this.mAppCompatEmojiTextHelper == null) {
            this.mAppCompatEmojiTextHelper = new v6((TextView)this);
        }
        return this.mAppCompatEmojiTextHelper;
    }
    
    public void drawableStateChanged() {
        super.drawableStateChanged();
        final x5 mBackgroundTintHelper = this.mBackgroundTintHelper;
        if (mBackgroundTintHelper != null) {
            mBackgroundTintHelper.b();
        }
        final l7 mTextHelper = this.mTextHelper;
        if (mTextHelper != null) {
            mTextHelper.b();
        }
    }
    
    public int getAutoSizeMaxTextSize() {
        if (u42.b) {
            return super.getAutoSizeMaxTextSize();
        }
        final l7 mTextHelper = this.mTextHelper;
        if (mTextHelper != null) {
            return mTextHelper.e();
        }
        return -1;
    }
    
    public int getAutoSizeMinTextSize() {
        if (u42.b) {
            return super.getAutoSizeMinTextSize();
        }
        final l7 mTextHelper = this.mTextHelper;
        if (mTextHelper != null) {
            return mTextHelper.f();
        }
        return -1;
    }
    
    public int getAutoSizeStepGranularity() {
        if (u42.b) {
            return super.getAutoSizeStepGranularity();
        }
        final l7 mTextHelper = this.mTextHelper;
        if (mTextHelper != null) {
            return mTextHelper.g();
        }
        return -1;
    }
    
    public int[] getAutoSizeTextAvailableSizes() {
        if (u42.b) {
            return super.getAutoSizeTextAvailableSizes();
        }
        final l7 mTextHelper = this.mTextHelper;
        if (mTextHelper != null) {
            return mTextHelper.h();
        }
        return new int[0];
    }
    
    public int getAutoSizeTextType() {
        final boolean b = u42.b;
        int n = 0;
        if (b) {
            if (super.getAutoSizeTextType() == 1) {
                n = 1;
            }
            return n;
        }
        final l7 mTextHelper = this.mTextHelper;
        if (mTextHelper != null) {
            return mTextHelper.i();
        }
        return 0;
    }
    
    public ActionMode$Callback getCustomSelectionActionModeCallback() {
        return nv1.q(super.getCustomSelectionActionModeCallback());
    }
    
    public ColorStateList getSupportBackgroundTintList() {
        final x5 mBackgroundTintHelper = this.mBackgroundTintHelper;
        ColorStateList c;
        if (mBackgroundTintHelper != null) {
            c = mBackgroundTintHelper.c();
        }
        else {
            c = null;
        }
        return c;
    }
    
    public PorterDuff$Mode getSupportBackgroundTintMode() {
        final x5 mBackgroundTintHelper = this.mBackgroundTintHelper;
        PorterDuff$Mode d;
        if (mBackgroundTintHelper != null) {
            d = mBackgroundTintHelper.d();
        }
        else {
            d = null;
        }
        return d;
    }
    
    public ColorStateList getSupportCompoundDrawablesTintList() {
        return this.mTextHelper.j();
    }
    
    public PorterDuff$Mode getSupportCompoundDrawablesTintMode() {
        return this.mTextHelper.k();
    }
    
    public boolean isEmojiCompatEnabled() {
        return this.getEmojiTextViewHelper().b();
    }
    
    public void onInitializeAccessibilityEvent(final AccessibilityEvent accessibilityEvent) {
        super.onInitializeAccessibilityEvent(accessibilityEvent);
        ((AccessibilityRecord)accessibilityEvent).setClassName((CharSequence)Button.class.getName());
    }
    
    public void onInitializeAccessibilityNodeInfo(final AccessibilityNodeInfo accessibilityNodeInfo) {
        super.onInitializeAccessibilityNodeInfo(accessibilityNodeInfo);
        accessibilityNodeInfo.setClassName((CharSequence)Button.class.getName());
    }
    
    public void onLayout(final boolean b, final int n, final int n2, final int n3, final int n4) {
        super.onLayout(b, n, n2, n3, n4);
        final l7 mTextHelper = this.mTextHelper;
        if (mTextHelper != null) {
            mTextHelper.o(b, n, n2, n3, n4);
        }
    }
    
    public void onTextChanged(final CharSequence charSequence, int n, final int n2, final int n3) {
        super.onTextChanged(charSequence, n, n2, n3);
        final l7 mTextHelper = this.mTextHelper;
        if (mTextHelper != null && !u42.b && mTextHelper.l()) {
            n = 1;
        }
        else {
            n = 0;
        }
        if (n != 0) {
            this.mTextHelper.c();
        }
    }
    
    public void setAllCaps(final boolean allCaps) {
        super.setAllCaps(allCaps);
        this.getEmojiTextViewHelper().d(allCaps);
    }
    
    public void setAutoSizeTextTypeUniformWithConfiguration(final int n, final int n2, final int n3, final int n4) {
        if (u42.b) {
            super.setAutoSizeTextTypeUniformWithConfiguration(n, n2, n3, n4);
        }
        else {
            final l7 mTextHelper = this.mTextHelper;
            if (mTextHelper != null) {
                mTextHelper.t(n, n2, n3, n4);
            }
        }
    }
    
    public void setAutoSizeTextTypeUniformWithPresetSizes(final int[] array, final int n) {
        if (u42.b) {
            super.setAutoSizeTextTypeUniformWithPresetSizes(array, n);
        }
        else {
            final l7 mTextHelper = this.mTextHelper;
            if (mTextHelper != null) {
                mTextHelper.u(array, n);
            }
        }
    }
    
    public void setAutoSizeTextTypeWithDefaults(final int autoSizeTextTypeWithDefaults) {
        if (u42.b) {
            super.setAutoSizeTextTypeWithDefaults(autoSizeTextTypeWithDefaults);
        }
        else {
            final l7 mTextHelper = this.mTextHelper;
            if (mTextHelper != null) {
                mTextHelper.v(autoSizeTextTypeWithDefaults);
            }
        }
    }
    
    public void setBackgroundDrawable(final Drawable backgroundDrawable) {
        super.setBackgroundDrawable(backgroundDrawable);
        final x5 mBackgroundTintHelper = this.mBackgroundTintHelper;
        if (mBackgroundTintHelper != null) {
            mBackgroundTintHelper.f(backgroundDrawable);
        }
    }
    
    public void setBackgroundResource(final int backgroundResource) {
        super.setBackgroundResource(backgroundResource);
        final x5 mBackgroundTintHelper = this.mBackgroundTintHelper;
        if (mBackgroundTintHelper != null) {
            mBackgroundTintHelper.g(backgroundResource);
        }
    }
    
    public void setCustomSelectionActionModeCallback(final ActionMode$Callback actionMode$Callback) {
        super.setCustomSelectionActionModeCallback(nv1.r((TextView)this, actionMode$Callback));
    }
    
    public void setEmojiCompatEnabled(final boolean b) {
        this.getEmojiTextViewHelper().e(b);
    }
    
    public void setFilters(final InputFilter[] array) {
        super.setFilters(this.getEmojiTextViewHelper().a(array));
    }
    
    public void setSupportAllCaps(final boolean b) {
        final l7 mTextHelper = this.mTextHelper;
        if (mTextHelper != null) {
            mTextHelper.s(b);
        }
    }
    
    public void setSupportBackgroundTintList(final ColorStateList list) {
        final x5 mBackgroundTintHelper = this.mBackgroundTintHelper;
        if (mBackgroundTintHelper != null) {
            mBackgroundTintHelper.i(list);
        }
    }
    
    public void setSupportBackgroundTintMode(final PorterDuff$Mode porterDuff$Mode) {
        final x5 mBackgroundTintHelper = this.mBackgroundTintHelper;
        if (mBackgroundTintHelper != null) {
            mBackgroundTintHelper.j(porterDuff$Mode);
        }
    }
    
    public void setSupportCompoundDrawablesTintList(final ColorStateList list) {
        this.mTextHelper.w(list);
        this.mTextHelper.b();
    }
    
    public void setSupportCompoundDrawablesTintMode(final PorterDuff$Mode porterDuff$Mode) {
        this.mTextHelper.x(porterDuff$Mode);
        this.mTextHelper.b();
    }
    
    public void setTextAppearance(final Context context, final int n) {
        super.setTextAppearance(context, n);
        final l7 mTextHelper = this.mTextHelper;
        if (mTextHelper != null) {
            mTextHelper.q(context, n);
        }
    }
    
    public void setTextSize(final int n, final float n2) {
        if (u42.b) {
            super.setTextSize(n, n2);
        }
        else {
            final l7 mTextHelper = this.mTextHelper;
            if (mTextHelper != null) {
                mTextHelper.A(n, n2);
            }
        }
    }
}

import java.util.Random;
import com.ekodroid.omrevaluator.templateui.models.SheetTemplate2;
import com.ekodroid.omrevaluator.templateui.models.Point2Double;
import com.ekodroid.omrevaluator.templateui.scanner.ScannerUtil;
import java.util.ArrayList;
import com.ekodroid.omrevaluator.serializable.SheetImageConfigure;
import com.ekodroid.omrevaluator.templateui.scanner.SheetDimension;
import android.graphics.Bitmap;

// 
// Decompiled by Procyon v0.6.0
// 

public class hn0
{
    public static int c = 0;
    public static int d = 0;
    public static int e = 0;
    public static int f = 0;
    public static int g = 5;
    public static double h = 0.3;
    public double a;
    public double b;
    
    public hn0() {
        this.a = 0.5;
        this.b = 0.9;
    }
    
    public static void j() {
        hn0.c = 0;
        hn0.d = 0;
        hn0.e = 0;
        hn0.f = 0;
        hn0.h = 0.3;
    }
    
    public final boolean a(final Bitmap bitmap, final SheetDimension sheetDimension, final double n, final SheetImageConfigure sheetImageConfigure, final double n2, final ArrayList list) {
        if (n2 > this.b) {
            return false;
        }
        final int squarelength = sheetImageConfigure.squarelength;
        final int n3 = (sheetImageConfigure.bottomCornerHeight - sheetImageConfigure.topCornerHeight - squarelength) / (gk.a() * ((sheetDimension.sheetMarkers.length - 1) * 5 + 1));
        if (Math.abs(a91.n() - gk.b()) > 50000) {
            return false;
        }
        int n4 = 0;
        boolean b = false;
        while (true) {
            final int n5 = squarelength * 2;
            if (n4 >= n5) {
                break;
            }
            if (n4 < squarelength) {
                int i = n4;
                final int n6 = n5;
                while (i > -1) {
                    final int n7 = sheetImageConfigure.topCornerWidth + i;
                    final int n8 = sheetImageConfigure.bottomCornerHeight - 1 - n4 + i;
                    if (ScannerUtil.k(bitmap.getPixel(n7, n8), n2)) {
                        if (ScannerUtil.j(bitmap, n7, n8, n3, this.a) && ScannerUtil.i(bitmap, n7, n8, (int)(n3 * 1.5), n2)) {
                            final Point2Double c = ScannerUtil.c(bitmap, n3, n7, n8, false, n2);
                            if (ScannerUtil.l(bitmap, c, n3, n2)) {
                                final Point2Double[][] sheetMarkers = sheetDimension.sheetMarkers;
                                sheetMarkers[sheetMarkers.length - 1][0] = c;
                                list.add(new em((int)sheetMarkers[sheetMarkers.length - 1][0].x, (int)sheetMarkers[sheetMarkers.length - 1][0].y, n3, bitmap.getWidth(), bitmap.getHeight()));
                                n4 = n6 + 1;
                                b = true;
                                break;
                            }
                        }
                    }
                    i -= (int)n;
                }
            }
            else {
                for (int j = squarelength; j > n4 - squarelength; j -= (int)n) {
                    final int n9 = sheetImageConfigure.topCornerWidth + j;
                    final int n10 = sheetImageConfigure.bottomCornerHeight - 1 - n4 + j;
                    if (ScannerUtil.k(bitmap.getPixel(n9, n10), n2) && ScannerUtil.j(bitmap, n9, n10, n3, this.a) && ScannerUtil.i(bitmap, n9, n10, (int)(n3 * 1.5), n2)) {
                        final Point2Double c2 = ScannerUtil.c(bitmap, n3, n9, n10, false, n2);
                        if (ScannerUtil.l(bitmap, c2, n3, n2)) {
                            final Point2Double[][] sheetMarkers2 = sheetDimension.sheetMarkers;
                            sheetMarkers2[sheetMarkers2.length - 1][0] = c2;
                            n4 = n5 + 1;
                            list.add(new em((int)sheetMarkers2[sheetMarkers2.length - 1][0].x, (int)sheetMarkers2[sheetMarkers2.length - 1][0].y, n3, bitmap.getWidth(), bitmap.getHeight()));
                            b = true;
                            break;
                        }
                    }
                }
            }
            n4 += (int)n;
        }
        return b;
    }
    
    public final boolean b(final Bitmap bitmap, final SheetDimension sheetDimension, final double n, final SheetImageConfigure sheetImageConfigure, final double n2, final ArrayList list) {
        if (n2 > this.b) {
            return false;
        }
        final int squarelength = sheetImageConfigure.squarelength;
        int n3 = (sheetImageConfigure.bottomCornerHeight - sheetImageConfigure.topCornerHeight - squarelength) / (gk.a() * ((sheetDimension.sheetMarkers.length - 1) * 5 + 1));
        int n4 = 0;
        boolean b = false;
        while (true) {
            final int n5 = squarelength * 2;
            if (n4 >= n5) {
                break;
            }
            int n10 = 0;
            int n11 = 0;
            Label_0509: {
                if (n4 < squarelength) {
                    int i = n4;
                    final int n6 = n5;
                    while (i > -1) {
                        final int n7 = sheetImageConfigure.topCornerWidth + i;
                        final int n8 = sheetImageConfigure.topCornerHeight + n4 - i;
                        if (ScannerUtil.k(bitmap.getPixel(n7, n8), n2)) {
                            if (ScannerUtil.j(bitmap, n7, n8, n3, this.a) && ScannerUtil.i(bitmap, n7, n8, (int)(n3 * 1.5), n2)) {
                                final Point2Double c = ScannerUtil.c(bitmap, n3, n7, n8, false, n2);
                                if (ScannerUtil.l(bitmap, c, n3, n2)) {
                                    final Point2Double[][] sheetMarkers = sheetDimension.sheetMarkers;
                                    sheetMarkers[0][0] = c;
                                    final Point2Double point2Double = sheetMarkers[0][0];
                                    list.add(new em((int)point2Double.x, (int)point2Double.y, n3, bitmap.getWidth(), bitmap.getHeight()));
                                    n4 = n6 + 1;
                                    b = true;
                                    break;
                                }
                            }
                        }
                        i -= (int)n;
                    }
                    final int n9 = n3;
                    n10 = n4;
                    n11 = n9;
                }
                else {
                    final int n12 = n3;
                    for (int j = squarelength; j > n4 - squarelength; j -= (int)n) {
                        final int n13 = sheetImageConfigure.topCornerWidth + j;
                        final int n14 = sheetImageConfigure.topCornerHeight + n4 - j;
                        if (ScannerUtil.k(bitmap.getPixel(n13, n14), n2) && ScannerUtil.j(bitmap, n13, n14, n12, this.a) && ScannerUtil.i(bitmap, n13, n14, (int)(n12 * 1.5), n2)) {
                            final Point2Double c2 = ScannerUtil.c(bitmap, n12, n13, n14, false, n2);
                            if (ScannerUtil.l(bitmap, c2, n12, n2)) {
                                final Point2Double[][] sheetMarkers2 = sheetDimension.sheetMarkers;
                                sheetMarkers2[0][0] = c2;
                                n10 = n5 + 1;
                                final Point2Double point2Double2 = sheetMarkers2[0][0];
                                list.add(new em((int)point2Double2.x, (int)point2Double2.y, n12, bitmap.getWidth(), bitmap.getHeight()));
                                b = true;
                                n11 = n12;
                                break Label_0509;
                            }
                        }
                    }
                    n10 = n4;
                    n11 = n12;
                }
            }
            final int n15 = (int)(n10 + n);
            n3 = n11;
            n4 = n15;
        }
        return b;
    }
    
    public final boolean c(final Bitmap bitmap, final SheetDimension sheetDimension, final double n, final SheetImageConfigure sheetImageConfigure, final double n2, final ArrayList list) {
        if (n2 > this.b) {
            return false;
        }
        final int squarelength = sheetImageConfigure.squarelength;
        final int a = gk.a();
        int n3 = (sheetImageConfigure.bottomCornerHeight - sheetImageConfigure.topCornerHeight - squarelength) / (gk.a() * ((sheetDimension.sheetMarkers.length - 1) * 5 + 1));
        int i = 0;
        boolean b = false;
        while (i < squarelength * a) {
            int n7 = 0;
            int n8 = 0;
            Label_0591: {
                if (i < squarelength) {
                    int j = i;
                    while (j > -1) {
                        final int n4 = sheetImageConfigure.bottomCornerWidth - 1 - j;
                        final int n5 = sheetImageConfigure.bottomCornerHeight - 1 - i + j;
                        if (ScannerUtil.k(bitmap.getPixel(n4, n5), n2) && (ScannerUtil.j(bitmap, n4, n5, n3, this.a) && ScannerUtil.i(bitmap, n4, n5, (int)(n3 * 1.5), n2))) {
                            final Point2Double c = ScannerUtil.c(bitmap, n3, n4, n5, false, n2);
                            if (ScannerUtil.l(bitmap, c, n3, n2)) {
                                final Point2Double[][] sheetMarkers = sheetDimension.sheetMarkers;
                                sheetMarkers[sheetMarkers.length - 1][sheetMarkers[0].length - 1] = c;
                                final Point2Double[] array = sheetMarkers[sheetMarkers.length - 1];
                                final Point2Double[] array2 = sheetMarkers[0];
                                list.add(new em((int)array[array2.length - 1].x, (int)sheetMarkers[sheetMarkers.length - 1][array2.length - 1].y, n3, bitmap.getWidth(), bitmap.getHeight()));
                                i = squarelength * 2 + 1;
                                b = true;
                                break;
                            }
                            goto Label_0305;
                        }
                        else {
                            j -= (int)n;
                        }
                    }
                    final int n6 = n3;
                    n7 = i;
                    n8 = n6;
                }
                else {
                    final int n9 = n3;
                    for (int k = squarelength; k > i - squarelength; k -= (int)n) {
                        final int n10 = sheetImageConfigure.bottomCornerWidth - 1 - k;
                        final int n11 = sheetImageConfigure.bottomCornerHeight - 1 - i + k;
                        if (ScannerUtil.k(bitmap.getPixel(n10, n11), n2) && ScannerUtil.j(bitmap, n10, n11, n9, this.a) && ScannerUtil.i(bitmap, n10, n11, (int)(n9 * 1.5), n2)) {
                            final Point2Double c2 = ScannerUtil.c(bitmap, n9, n10, n11, false, n2);
                            if (ScannerUtil.l(bitmap, c2, n9, n2)) {
                                final Point2Double[][] sheetMarkers2 = sheetDimension.sheetMarkers;
                                sheetMarkers2[sheetMarkers2.length - 1][sheetMarkers2[0].length - 1] = c2;
                                n7 = squarelength * 2 + 1;
                                final Point2Double[] array3 = sheetMarkers2[sheetMarkers2.length - 1];
                                final Point2Double[] array4 = sheetMarkers2[0];
                                list.add(new em((int)array3[array4.length - 1].x, (int)sheetMarkers2[sheetMarkers2.length - 1][array4.length - 1].y, n9, bitmap.getWidth(), bitmap.getHeight()));
                                b = true;
                                n8 = n9;
                                break Label_0591;
                            }
                        }
                    }
                    n7 = i;
                    n8 = n9;
                }
            }
            final int n12 = (int)(n7 + n);
            n3 = n8;
            i = n12;
        }
        return b;
    }
    
    public SheetDimension d(final j5 j5, final d91 d91, final ej1 ej1, final boolean b, final int n) {
        final int n2 = j5.c.getRows(n) / 5;
        final int columns = j5.c.getColumns(n);
        final SheetDimension sheetDimension = new SheetDimension();
        sheetDimension.sheetMarkers = new Point2Double[n2 + 1][columns + 1];
        final Bitmap a = j5.a;
        sheetDimension.setHeight(a.getHeight());
        sheetDimension.setWidth(a.getWidth());
        final ArrayList list = new ArrayList();
        SheetImageConfigure b2;
        if (j5.b.imageHeight == a.getHeight()) {
            b2 = j5.b;
        }
        else {
            b2 = new SheetImageConfigure();
            b2.imageHeight = a.getHeight();
            final int width = a.getWidth();
            b2.imageWidth = width;
            final int imageHeight = b2.imageHeight;
            final int squarelengthPercent = j5.b.squarelengthPercent;
            int n3;
            if (width > imageHeight) {
                n3 = squarelengthPercent * imageHeight;
            }
            else {
                n3 = squarelengthPercent * width;
            }
            b2.squarelength = n3 / 100;
            final SheetImageConfigure b3 = j5.b;
            b2.bottomCornerHeight = b3.bottomCornerHeightPercent * imageHeight / 100;
            b2.bottomCornerWidth = b3.bottomCornerWidthPercent * width / 100;
            b2.topCornerHeight = b3.topCornerHeightPercent * imageHeight / 100;
            b2.topCornerWidth = b3.topCornerWidthPercent * width / 100;
        }
        final boolean r = this.r(a, sheetDimension, gk.a() / 2 * ((a.getWidth() - b2.topCornerWidth * 2 - b2.squarelength) * 1.0 / ((ve1.e(j5.c.getColumnWidthInBubbles(n)) + 1) * 5.0)), b2, ej1, list);
        if (d91 != null && r) {
            d91.a(60);
            ej1.a(list);
        }
        else if (list.size() > 2) {
            ++hn0.d;
        }
        else {
            ++hn0.c;
        }
        if (r) {
            boolean b4;
            if (hn0.e <= hn0.g && !b) {
                b4 = this.p(a, sheetDimension, j5.c, ej1, list, n);
            }
            else {
                b4 = this.o(a, sheetDimension, j5.c, ej1, list, n);
            }
            if (b4) {
                if (this.i(sheetDimension)) {
                    d91.a(90);
                    return sheetDimension;
                }
                ++hn0.f;
            }
            else {
                ++hn0.e;
            }
        }
        ej1.a(list);
        return null;
    }
    
    public final boolean e(final Bitmap bitmap, final SheetDimension sheetDimension, final double n, final SheetImageConfigure sheetImageConfigure, final double n2, final ArrayList list) {
        if (n2 > this.b) {
            return false;
        }
        final int squarelength = sheetImageConfigure.squarelength;
        gk.a();
        final int n3 = (sheetImageConfigure.bottomCornerHeight - sheetImageConfigure.topCornerHeight - squarelength) / (gk.a() * ((sheetDimension.sheetMarkers.length - 1) * 5 + 1));
        int n4 = 0;
        boolean b = false;
        while (true) {
            final int n5 = squarelength * 2;
            if (n4 >= n5) {
                break;
            }
            if (n4 < squarelength) {
                int i = n4;
                final int n6 = n5;
                while (i > -1) {
                    final int n7 = sheetImageConfigure.bottomCornerWidth - 1 - i;
                    final int n8 = sheetImageConfigure.topCornerHeight + n4 - i;
                    if (ScannerUtil.k(bitmap.getPixel(n7, n8), n2)) {
                        if (ScannerUtil.j(bitmap, n7, n8, n3, this.a) && ScannerUtil.i(bitmap, n7, n8, (int)(n3 * 1.5), n2)) {
                            final Point2Double c = ScannerUtil.c(bitmap, n3, n7, n8, false, n2);
                            if (ScannerUtil.l(bitmap, c, n3, n2)) {
                                final Point2Double[][] sheetMarkers = sheetDimension.sheetMarkers;
                                final Point2Double[] array = sheetMarkers[0];
                                array[array.length - 1] = c;
                                final Point2Double[] array2 = sheetMarkers[0];
                                list.add(new em((int)array2[array2.length - 1].x, (int)array2[array2.length - 1].y, n3, bitmap.getWidth(), bitmap.getHeight()));
                                n4 = n6 + 1;
                                b = true;
                                break;
                            }
                        }
                    }
                    i -= (int)n;
                }
            }
            else {
                for (int j = squarelength; j > n4 - squarelength; j -= (int)n) {
                    final int n9 = sheetImageConfigure.bottomCornerWidth - 1 - j;
                    final int n10 = sheetImageConfigure.topCornerHeight + n4 - j;
                    if (ScannerUtil.k(bitmap.getPixel(n9, n10), n2) && ScannerUtil.j(bitmap, n9, n10, n3, this.a) && ScannerUtil.i(bitmap, n9, n10, (int)(n3 * 1.5), n2)) {
                        final Point2Double c2 = ScannerUtil.c(bitmap, n3, n9, n10, false, n2);
                        if (ScannerUtil.l(bitmap, c2, n3, n2)) {
                            final Point2Double[][] sheetMarkers2 = sheetDimension.sheetMarkers;
                            final Point2Double[] array3 = sheetMarkers2[0];
                            array3[array3.length - 1] = c2;
                            n4 = n5 + 1;
                            final Point2Double[] array4 = sheetMarkers2[0];
                            list.add(new em((int)array4[array4.length - 1].x, (int)array4[array4.length - 1].y, n3, bitmap.getWidth(), bitmap.getHeight()));
                            b = true;
                            break;
                        }
                    }
                }
            }
            n4 += (int)n;
        }
        return b;
    }
    
    public final int f(final Point2Double[][] array) {
        int i = 0;
        int n = 0;
        while (i < array.length) {
            int n2 = 0;
            while (true) {
                final Point2Double[] array2 = array[i];
                if (n2 >= array2.length) {
                    break;
                }
                int n3 = n;
                if (array2[n2] == null) {
                    n3 = n + 1;
                }
                ++n2;
                n = n3;
            }
            ++i;
        }
        return n;
    }
    
    public final double g(final Point2Double point2Double, final Point2Double point2Double2, final int n) {
        final double x = point2Double2.x;
        final double x2 = point2Double.x;
        final double y = point2Double2.y;
        final double y2 = point2Double.y;
        return x2 - (x - x2) / (y - y2) * (y2 - n);
    }
    
    public final double h(final Point2Double point2Double, final Point2Double point2Double2, final int n) {
        final double y = point2Double2.y;
        final double y2 = point2Double.y;
        final double x = point2Double2.x;
        final double x2 = point2Double.x;
        return y2 - (y - y2) / (x - x2) * (x2 - n);
    }
    
    public final boolean i(final SheetDimension sheetDimension) {
        final Point2Double[][] sheetMarkers = sheetDimension.sheetMarkers;
        int n;
        for (int i = 0; i < sheetMarkers.length - 2; i = n) {
            final double y = sheetMarkers[i][0].y;
            n = i + 1;
            final double y2 = sheetMarkers[n][0].y;
            final double n2 = (y - y2) / (y2 - sheetMarkers[i + 2][0].y);
            if (n2 > 1.2 || n2 < 0.8) {
                return false;
            }
        }
        int n4;
        for (int j = 0; j < sheetMarkers.length - 2; j = n4) {
            final int n3 = sheetMarkers[0].length - 1;
            final double y3 = sheetMarkers[j][n3].y;
            n4 = j + 1;
            final double y4 = sheetMarkers[n4][n3].y;
            final double n5 = (y3 - y4) / (y4 - sheetMarkers[j + 2][n3].y);
            if (n5 > 1.2 || n5 < 0.8) {
                return false;
            }
        }
        return true;
    }
    
    public final boolean k(final Bitmap bitmap, final SheetDimension sheetDimension, final SheetTemplate2 sheetTemplate2, final int n) {
        final int length = sheetDimension.sheetMarkers.length;
        int n2 = 1;
        while (true) {
            final int n3 = length - 1;
            if (n2 >= n3) {
                return true;
            }
            final Point2Double[][] sheetMarkers = sheetDimension.sheetMarkers;
            final int n4 = n2 - 1;
            final Point2Double g = ScannerUtil.g(sheetMarkers[n4][n], sheetMarkers[n3][n], 1, length + 1 - n2);
            final double bubbleSize = sheetDimension.getBubbleSize();
            final Point2Double[][] sheetMarkers2 = sheetDimension.sheetMarkers;
            final Point2Double s = this.s(bitmap, g, bubbleSize, sheetMarkers2[n4][n], sheetMarkers2[n3][n], true);
            if (s == null) {
                return false;
            }
            sheetDimension.sheetMarkers[n2][n] = s;
            ++n2;
        }
    }
    
    public final int l(final Bitmap bitmap, final SheetDimension sheetDimension, final SheetTemplate2 sheetTemplate2, final int n) {
        final int length = sheetDimension.sheetMarkers.length;
        int n2 = 0;
        int n3 = 1;
        while (true) {
            final int n4 = length - 1;
            if (n3 >= n4) {
                break;
            }
            final Point2Double[][] sheetMarkers = sheetDimension.sheetMarkers;
            Label_0219: {
                if (sheetMarkers[n3][n] == null) {
                    final int n5 = n3 - 1;
                    final Point2Double point2Double = sheetMarkers[n5][n];
                    if (point2Double != null) {
                        final Point2Double g = ScannerUtil.g(point2Double, sheetMarkers[n4][n], 1, length + 1 - n3);
                        final double bubbleSize = sheetDimension.getBubbleSize();
                        final Point2Double[][] sheetMarkers2 = sheetDimension.sheetMarkers;
                        final Point2Double s = this.s(bitmap, g, bubbleSize, sheetMarkers2[n5][n], sheetMarkers2[n4][n], true);
                        if (s != null) {
                            sheetDimension.sheetMarkers[n3][n] = s;
                            break Label_0219;
                        }
                    }
                    else {
                        final Point2Double g2 = ScannerUtil.g(sheetMarkers[0][n], sheetMarkers[n4][n], n3, length);
                        final double bubbleSize2 = sheetDimension.getBubbleSize();
                        final Point2Double[][] sheetMarkers3 = sheetDimension.sheetMarkers;
                        final Point2Double s2 = this.s(bitmap, g2, bubbleSize2, sheetMarkers3[0][n], sheetMarkers3[n4][n], true);
                        if (s2 != null) {
                            sheetDimension.sheetMarkers[n3][n] = s2;
                            break Label_0219;
                        }
                    }
                    ++n2;
                }
            }
            ++n3;
        }
        return n2;
    }
    
    public final boolean m(final Bitmap bitmap, final SheetDimension sheetDimension, final SheetTemplate2 sheetTemplate2, final int n, final int n2) {
        final int length = sheetDimension.sheetMarkers[0].length;
        final int e = ve1.e(sheetTemplate2.getColumnWidthInBubbles(n2));
        int n3 = 1;
        while (true) {
            final int n4 = length - 1;
            if (n3 >= n4) {
                return true;
            }
            final int[] columnWidthInBubbles = sheetTemplate2.getColumnWidthInBubbles(n2);
            final int n5 = n3 - 1;
            final int f = ve1.f(columnWidthInBubbles, n5);
            final int n6 = sheetTemplate2.getColumnWidthInBubbles(n2)[n5];
            final Point2Double[] array = sheetDimension.sheetMarkers[n];
            final Point2Double g = ScannerUtil.g(array[n5], array[n4], n6, e + 1 - f);
            final double bubbleSize = sheetDimension.getBubbleSize();
            final Point2Double[] array2 = sheetDimension.sheetMarkers[n];
            final Point2Double s = this.s(bitmap, g, bubbleSize, array2[n5], array2[n4], false);
            if (s == null) {
                return false;
            }
            sheetDimension.sheetMarkers[n][n3] = s;
            ++n3;
        }
    }
    
    public final int n(final Bitmap bitmap, final SheetDimension sheetDimension, final SheetTemplate2 sheetTemplate2, final int n, final int n2) {
        final int length = sheetDimension.sheetMarkers[0].length;
        final int n3 = ve1.e(sheetTemplate2.getColumnWidthInBubbles(n2)) + 1;
        int n4 = 0;
        int n5 = 1;
        while (true) {
            final int n6 = length - 1;
            if (n5 >= n6) {
                break;
            }
            final Point2Double[] array = sheetDimension.sheetMarkers[n];
            Label_0286: {
                if (array[n5] == null) {
                    final int n7 = n5 - 1;
                    if (array[n7] != null) {
                        final int f = ve1.f(sheetTemplate2.getColumnWidthInBubbles(n2), n7);
                        final int n8 = sheetTemplate2.getColumnWidthInBubbles(n2)[n7];
                        final Point2Double[] array2 = sheetDimension.sheetMarkers[n];
                        final Point2Double g = ScannerUtil.g(array2[n7], array2[n6], n8, n3 - f);
                        final double bubbleSize = sheetDimension.getBubbleSize();
                        final Point2Double[] array3 = sheetDimension.sheetMarkers[n];
                        final Point2Double s = this.s(bitmap, g, bubbleSize, array3[n7], array3[n6], false);
                        if (s != null) {
                            sheetDimension.sheetMarkers[n][n5] = s;
                            break Label_0286;
                        }
                    }
                    else {
                        final int f2 = ve1.f(sheetTemplate2.getColumnWidthInBubbles(n2), n5);
                        final Point2Double[] array4 = sheetDimension.sheetMarkers[n];
                        final Point2Double g2 = ScannerUtil.g(array4[0], array4[n6], f2, n3);
                        final double bubbleSize2 = sheetDimension.getBubbleSize();
                        final Point2Double[] array5 = sheetDimension.sheetMarkers[n];
                        final Point2Double s2 = this.s(bitmap, g2, bubbleSize2, array5[0], array5[n6], false);
                        if (s2 != null) {
                            sheetDimension.sheetMarkers[n][n5] = s2;
                            break Label_0286;
                        }
                    }
                    ++n4;
                }
            }
            ++n5;
        }
        return n4;
    }
    
    public final boolean o(final Bitmap bitmap, final SheetDimension sheetDimension, final SheetTemplate2 sheetTemplate2, final ej1 ej1, final ArrayList list, final int n) {
        final Point2Double[][] sheetMarkers = sheetDimension.sheetMarkers;
        final int length = sheetMarkers.length;
        final int length2 = sheetMarkers[0].length;
        this.n(bitmap, sheetDimension, sheetTemplate2, 0, n);
        final int n2 = length - 1;
        this.n(bitmap, sheetDimension, sheetTemplate2, n2, n);
        for (int i = 0; i < length2; ++i) {
            final Point2Double[][] sheetMarkers2 = sheetDimension.sheetMarkers;
            if (sheetMarkers2[0][i] != null && sheetMarkers2[n2][i] != null) {
                this.l(bitmap, sheetDimension, sheetTemplate2, i);
            }
        }
        if (this.f(sheetDimension.sheetMarkers) == 0) {
            return true;
        }
        int n3 = 1;
        Point2Double[][] sheetMarkers3;
        while (true) {
            sheetMarkers3 = sheetDimension.sheetMarkers;
            if (n3 >= n2) {
                break;
            }
            final Point2Double[] array = sheetMarkers3[n3];
            if (array[0] != null && array[length2 - 1] != null) {
                this.n(bitmap, sheetDimension, sheetTemplate2, n3, n);
            }
            ++n3;
        }
        final int f = this.f(sheetMarkers3);
        if (f == 0) {
            return true;
        }
        if (f > length * length2 * 0.25) {
            return false;
        }
        this.q(bitmap, sheetDimension, sheetTemplate2, n);
        if (this.f(sheetDimension.sheetMarkers) == 0) {
            return true;
        }
        this.q(bitmap, sheetDimension, sheetTemplate2, n);
        return this.f(sheetDimension.sheetMarkers) == 0;
    }
    
    public final boolean p(final Bitmap bitmap, final SheetDimension sheetDimension, final SheetTemplate2 sheetTemplate2, final ej1 ej1, final ArrayList list, int i) {
        final Point2Double[][] sheetMarkers = sheetDimension.sheetMarkers;
        final int length = sheetMarkers.length;
        final int length2 = sheetMarkers[0].length;
        if (!this.m(bitmap, sheetDimension, sheetTemplate2, 0, i)) {
            return false;
        }
        if (!this.m(bitmap, sheetDimension, sheetTemplate2, length - 1, i)) {
            return false;
        }
        for (i = 0; i < length2; ++i) {
            if (!this.k(bitmap, sheetDimension, sheetTemplate2, i)) {
                return false;
            }
        }
        return true;
    }
    
    public final int q(final Bitmap bitmap, final SheetDimension sheetDimension, final SheetTemplate2 sheetTemplate2, final int n) {
        final Point2Double[][] sheetMarkers = sheetDimension.sheetMarkers;
        final int length = sheetMarkers.length;
        final int length2 = sheetMarkers[0].length;
        final int n2 = ve1.e(sheetTemplate2.getColumnWidthInBubbles(n)) + 1;
        int i = 0;
        int n3 = 0;
        while (i < length) {
            int n4;
        Label_0238_Outer:
            for (int j = 0; j < length2; ++j, n3 = n4) {
                final Point2Double[][] sheetMarkers2 = sheetDimension.sheetMarkers;
                final Point2Double[] array = sheetMarkers2[i];
                n4 = n3;
                if (array[j] == null) {
                    final Point2Double point2Double = sheetMarkers2[0][j];
                    while (true) {
                        Label_0348: {
                            if (point2Double == null) {
                                break Label_0348;
                            }
                            final int n5 = length - 1;
                            final Point2Double point2Double2 = sheetMarkers2[n5][j];
                            if (point2Double2 == null) {
                                break Label_0348;
                            }
                            final int n6 = i - 1;
                            final Point2Double point2Double3 = sheetMarkers2[n6][j];
                            if (point2Double3 != null) {
                                final Point2Double g = ScannerUtil.g(point2Double3, point2Double2, 1, length + 1 - i);
                                final double bubbleSize = sheetDimension.getBubbleSize();
                                final Point2Double[][] sheetMarkers3 = sheetDimension.sheetMarkers;
                                final Point2Double s = this.s(bitmap, g, bubbleSize, sheetMarkers3[n6][j], sheetMarkers3[n5][j], true);
                                if (s != null) {
                                    sheetDimension.sheetMarkers[i][j] = s;
                                    n4 = n3;
                                    continue Label_0238_Outer;
                                }
                                sheetDimension.sheetMarkers[i][j] = g;
                                sheetDimension.predictedMarkers.add(g);
                            }
                            else {
                                final Point2Double g2 = ScannerUtil.g(point2Double, point2Double2, i, length);
                                final double bubbleSize2 = sheetDimension.getBubbleSize();
                                final Point2Double[][] sheetMarkers4 = sheetDimension.sheetMarkers;
                                final Point2Double s2 = this.s(bitmap, g2, bubbleSize2, sheetMarkers4[0][j], sheetMarkers4[n5][j], true);
                                if (s2 != null) {
                                    sheetDimension.sheetMarkers[i][j] = s2;
                                    n4 = n3;
                                    continue Label_0238_Outer;
                                }
                                sheetDimension.sheetMarkers[i][j] = g2;
                                sheetDimension.predictedMarkers.add(g2);
                            }
                            n4 = n3 + 1;
                            continue Label_0238_Outer;
                        }
                        n4 = n3;
                        if (array[0] != null) {
                            final int n7 = length2 - 1;
                            n4 = n3;
                            if (array[n7] != null) {
                                final int n8 = j - 1;
                                if (array[n8] != null) {
                                    final int f = ve1.f(sheetTemplate2.getColumnWidthInBubbles(n), n8);
                                    final int n9 = sheetTemplate2.getColumnWidthInBubbles(n)[n8];
                                    final Point2Double[] array2 = sheetDimension.sheetMarkers[i];
                                    final Point2Double g3 = ScannerUtil.g(array2[n8], array2[n7], n9, n2 - f);
                                    final double bubbleSize3 = sheetDimension.getBubbleSize();
                                    final Point2Double[] array3 = sheetDimension.sheetMarkers[i];
                                    final Point2Double s3 = this.s(bitmap, g3, bubbleSize3, array3[n8], array3[n7], false);
                                    if (s3 == null) {
                                        sheetDimension.sheetMarkers[i][j] = g3;
                                        sheetDimension.predictedMarkers.add(g3);
                                        continue;
                                    }
                                    sheetDimension.sheetMarkers[i][j] = s3;
                                    n4 = n3;
                                }
                                else {
                                    final int f2 = ve1.f(sheetTemplate2.getColumnWidthInBubbles(n), j);
                                    final Point2Double[] array4 = sheetDimension.sheetMarkers[i];
                                    final Point2Double g4 = ScannerUtil.g(array4[0], array4[n7], f2, n2);
                                    final double bubbleSize4 = sheetDimension.getBubbleSize();
                                    final Point2Double[] array5 = sheetDimension.sheetMarkers[i];
                                    final Point2Double s4 = this.s(bitmap, g4, bubbleSize4, array5[0], array5[n7], false);
                                    if (s4 != null) {
                                        sheetDimension.sheetMarkers[i][j] = s4;
                                        n4 = n3;
                                    }
                                    else {
                                        n4 = n3 + 1;
                                        sheetDimension.sheetMarkers[i][j] = g4;
                                        sheetDimension.predictedMarkers.add(g4);
                                    }
                                }
                            }
                        }
                        break;
                    }
                }
            }
            ++i;
        }
        return n3;
    }
    
    public final boolean r(final Bitmap bitmap, final SheetDimension sheetDimension, final double n, final SheetImageConfigure sheetImageConfigure, final ej1 ej1, final ArrayList list) {
        final int squarelength = sheetImageConfigure.squarelength;
        final int n2 = new Random().nextInt(4) % 4;
        if (n2 != 0) {
            if (n2 != 1) {
                if (n2 != 2) {
                    if (n2 == 3) {
                        final int topCornerWidth = sheetImageConfigure.topCornerWidth;
                        final int n3 = squarelength / 2;
                        if (!this.a(bitmap, sheetDimension, n, sheetImageConfigure, ScannerUtil.b(bitmap, topCornerWidth + n3, sheetImageConfigure.bottomCornerHeight - n3, squarelength, squarelength, 25, hn0.h), list)) {
                            return false;
                        }
                        if (!this.b(bitmap, sheetDimension, n, sheetImageConfigure, ScannerUtil.b(bitmap, sheetImageConfigure.topCornerWidth + n3, sheetImageConfigure.topCornerHeight + n3, squarelength, squarelength, 25, hn0.h), list)) {
                            return false;
                        }
                        if (!this.e(bitmap, sheetDimension, n, sheetImageConfigure, ScannerUtil.b(bitmap, sheetImageConfigure.bottomCornerWidth - n3, sheetImageConfigure.topCornerHeight + n3, squarelength, squarelength, 25, hn0.h), list)) {
                            return false;
                        }
                        if (!this.c(bitmap, sheetDimension, n, sheetImageConfigure, ScannerUtil.b(bitmap, sheetImageConfigure.bottomCornerWidth - n3, sheetImageConfigure.bottomCornerHeight - n3, squarelength, squarelength, 25, hn0.h), list)) {
                            return false;
                        }
                    }
                }
                else {
                    final int bottomCornerWidth = sheetImageConfigure.bottomCornerWidth;
                    final int n4 = squarelength / 2;
                    if (!this.c(bitmap, sheetDimension, n, sheetImageConfigure, ScannerUtil.b(bitmap, bottomCornerWidth - n4, sheetImageConfigure.bottomCornerHeight - n4, squarelength, squarelength, 25, hn0.h), list)) {
                        return false;
                    }
                    if (!this.a(bitmap, sheetDimension, n, sheetImageConfigure, ScannerUtil.b(bitmap, sheetImageConfigure.topCornerWidth + n4, sheetImageConfigure.bottomCornerHeight - n4, squarelength, squarelength, 25, hn0.h), list)) {
                        return false;
                    }
                    if (!this.b(bitmap, sheetDimension, n, sheetImageConfigure, ScannerUtil.b(bitmap, sheetImageConfigure.topCornerWidth + n4, sheetImageConfigure.topCornerHeight + n4, squarelength, squarelength, 25, hn0.h), list)) {
                        return false;
                    }
                    if (!this.e(bitmap, sheetDimension, n, sheetImageConfigure, ScannerUtil.b(bitmap, sheetImageConfigure.bottomCornerWidth - n4, sheetImageConfigure.topCornerHeight + n4, squarelength, squarelength, 25, hn0.h), list)) {
                        return false;
                    }
                }
            }
            else {
                final int bottomCornerWidth2 = sheetImageConfigure.bottomCornerWidth;
                final int n5 = squarelength / 2;
                if (!this.e(bitmap, sheetDimension, n, sheetImageConfigure, ScannerUtil.b(bitmap, bottomCornerWidth2 - n5, sheetImageConfigure.topCornerHeight + n5, squarelength, squarelength, 25, hn0.h), list)) {
                    return false;
                }
                if (!this.c(bitmap, sheetDimension, n, sheetImageConfigure, ScannerUtil.b(bitmap, sheetImageConfigure.bottomCornerWidth - n5, sheetImageConfigure.bottomCornerHeight - n5, squarelength, squarelength, 25, hn0.h), list)) {
                    return false;
                }
                if (!this.a(bitmap, sheetDimension, n, sheetImageConfigure, ScannerUtil.b(bitmap, sheetImageConfigure.topCornerWidth + n5, sheetImageConfigure.bottomCornerHeight - n5, squarelength, squarelength, 25, hn0.h), list)) {
                    return false;
                }
                if (!this.b(bitmap, sheetDimension, n, sheetImageConfigure, ScannerUtil.b(bitmap, sheetImageConfigure.topCornerWidth + n5, sheetImageConfigure.topCornerHeight + n5, squarelength, squarelength, 25, hn0.h), list)) {
                    return false;
                }
            }
        }
        else {
            final int topCornerWidth2 = sheetImageConfigure.topCornerWidth;
            final int n6 = squarelength / 2;
            if (!this.b(bitmap, sheetDimension, n, sheetImageConfigure, ScannerUtil.b(bitmap, topCornerWidth2 + n6, sheetImageConfigure.topCornerHeight + n6, squarelength, squarelength, 25, hn0.h), list)) {
                return false;
            }
            if (!this.e(bitmap, sheetDimension, n, sheetImageConfigure, ScannerUtil.b(bitmap, sheetImageConfigure.bottomCornerWidth - n6, sheetImageConfigure.topCornerHeight + n6, squarelength, squarelength, 25, hn0.h), list)) {
                return false;
            }
            if (!this.c(bitmap, sheetDimension, n, sheetImageConfigure, ScannerUtil.b(bitmap, sheetImageConfigure.bottomCornerWidth - n6, sheetImageConfigure.bottomCornerHeight - n6, squarelength, squarelength, 25, hn0.h), list)) {
                return false;
            }
            if (!this.a(bitmap, sheetDimension, n, sheetImageConfigure, ScannerUtil.b(bitmap, sheetImageConfigure.topCornerWidth + n6, sheetImageConfigure.bottomCornerHeight - n6, squarelength, squarelength, 25, hn0.h), list)) {
                return false;
            }
        }
        return true;
    }
    
    public final Point2Double s(final Bitmap bitmap, Point2Double point2Double, double b, final Point2Double point2Double2, final Point2Double point2Double3, final boolean b2) {
        final int n = (int)(b + 1.0);
        final int n2 = (int)point2Double.x;
        final int n3 = (int)point2Double.y;
        final int n4 = n * 2;
        b = ScannerUtil.b(bitmap, n2, n3, n4, n4, 50, hn0.h);
        if (!ScannerUtil.k(bitmap.getPixel((int)point2Double.x, (int)point2Double.y), b)) {
            final int n5 = n / 3;
            int j;
            int i = j = n5;
            if (b2) {
                while (i < n5 * 6) {
                    final int n6 = (int)point2Double.y + i;
                    final int n7 = (int)this.g(point2Double2, point2Double3, n6);
                    if (ScannerUtil.k(bitmap.getPixel(n7, n6), b)) {
                        point2Double = ScannerUtil.c(bitmap, n, n7, n6, false, b);
                        if (!ScannerUtil.j(bitmap, (int)point2Double.x, (int)point2Double.y, n, 0.3)) {
                            return null;
                        }
                        return point2Double;
                    }
                    else {
                        final int n8 = (int)point2Double.y - i;
                        final int n9 = (int)this.g(point2Double2, point2Double3, n8);
                        if (ScannerUtil.k(bitmap.getPixel(n9, n8), b)) {
                            point2Double = ScannerUtil.c(bitmap, n, n9, n8, false, b);
                            if (!ScannerUtil.j(bitmap, (int)point2Double.x, (int)point2Double.y, n, 0.3)) {
                                return null;
                            }
                            return point2Double;
                        }
                        else {
                            i += n5;
                        }
                    }
                }
            }
            else {
                while (j < n5 * 3) {
                    final int n10 = (int)point2Double.x + j;
                    final int n11 = (int)this.h(point2Double2, point2Double3, n10);
                    if (ScannerUtil.k(bitmap.getPixel(n10, n11), b)) {
                        point2Double = ScannerUtil.c(bitmap, n, n10, n11, false, b);
                        if (!ScannerUtil.j(bitmap, (int)point2Double.x, (int)point2Double.y, n, 0.3)) {
                            return null;
                        }
                        return point2Double;
                    }
                    else {
                        final int n12 = (int)point2Double.x - j;
                        final int n13 = (int)this.h(point2Double2, point2Double3, n12);
                        if (ScannerUtil.k(bitmap.getPixel(n12, n13), b)) {
                            point2Double = ScannerUtil.c(bitmap, n, n12, n13, false, b);
                            if (!ScannerUtil.j(bitmap, (int)point2Double.x, (int)point2Double.y, n, 0.3)) {
                                return null;
                            }
                            return point2Double;
                        }
                        else {
                            j += n5;
                        }
                    }
                }
            }
            return null;
        }
        point2Double = ScannerUtil.c(bitmap, n, (int)point2Double.x, (int)point2Double.y, false, b);
        if (!ScannerUtil.j(bitmap, (int)point2Double.x, (int)point2Double.y, n, 0.3)) {
            return null;
        }
        return point2Double;
    }
}

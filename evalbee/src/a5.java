import android.view.Choreographer$FrameCallback;
import android.view.Choreographer;
import android.os.SystemClock;
import java.util.ArrayList;

// 
// Decompiled by Procyon v0.6.0
// 

public class a5
{
    public static final ThreadLocal g;
    public final co1 a;
    public final ArrayList b;
    public final a c;
    public c d;
    public long e;
    public boolean f;
    
    static {
        g = new ThreadLocal();
    }
    
    public a5() {
        this.a = new co1();
        this.b = new ArrayList();
        this.c = new a();
        this.e = 0L;
        this.f = false;
    }
    
    public static a5 d() {
        final ThreadLocal g = a5.g;
        if (g.get() == null) {
            g.set(new a5());
        }
        return (a5)g.get();
    }
    
    public void a(final b b, final long n) {
        if (this.b.size() == 0) {
            this.e().a();
        }
        if (!this.b.contains(b)) {
            this.b.add(b);
        }
        if (n > 0L) {
            this.a.put(b, SystemClock.uptimeMillis() + n);
        }
    }
    
    public final void b() {
        if (this.f) {
            for (int i = this.b.size() - 1; i >= 0; --i) {
                if (this.b.get(i) == null) {
                    this.b.remove(i);
                }
            }
            this.f = false;
        }
    }
    
    public void c(final long n) {
        final long uptimeMillis = SystemClock.uptimeMillis();
        for (int i = 0; i < this.b.size(); ++i) {
            final b b = this.b.get(i);
            if (b != null) {
                if (this.f(b, uptimeMillis)) {
                    b.a(n);
                }
            }
        }
        this.b();
    }
    
    public c e() {
        if (this.d == null) {
            this.d = (c)new d(this.c);
        }
        return this.d;
    }
    
    public final boolean f(final b b, final long n) {
        final Long n2 = (Long)this.a.get(b);
        if (n2 == null) {
            return true;
        }
        if (n2 < n) {
            this.a.remove(b);
            return true;
        }
        return false;
    }
    
    public void g(final b o) {
        this.a.remove(o);
        final int index = this.b.indexOf(o);
        if (index >= 0) {
            this.b.set(index, null);
            this.f = true;
        }
    }
    
    public class a
    {
        public final a5 a;
        
        public a(final a5 a) {
            this.a = a;
        }
        
        public void a() {
            this.a.e = SystemClock.uptimeMillis();
            final a5 a = this.a;
            a.c(a.e);
            if (this.a.b.size() > 0) {
                this.a.e().a();
            }
        }
    }
    
    public interface b
    {
        boolean a(final long p0);
    }
    
    public abstract static class c
    {
        public final a a;
        
        public c(final a a) {
            this.a = a;
        }
        
        public abstract void a();
    }
    
    public static class d extends c
    {
        public final Choreographer b;
        public final Choreographer$FrameCallback c;
        
        public d(final a a) {
            super(a);
            this.b = Choreographer.getInstance();
            this.c = (Choreographer$FrameCallback)new Choreographer$FrameCallback(this) {
                public final d a;
                
                public void doFrame(final long n) {
                    this.a.a.a();
                }
            };
        }
        
        @Override
        public void a() {
            this.b.postFrameCallback(this.c);
        }
    }
}

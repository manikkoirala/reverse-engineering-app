import android.os.BaseBundle;
import com.google.android.gms.internal.measurement.zzdf;
import java.util.concurrent.Executor;
import android.os.Bundle;
import android.content.Context;
import java.util.concurrent.ConcurrentHashMap;
import com.google.android.gms.common.internal.Preconditions;
import java.util.Map;
import com.google.android.gms.measurement.api.AppMeasurementSdk;

// 
// Decompiled by Procyon v0.6.0
// 

public class h4 implements g4
{
    public static volatile g4 c;
    public final AppMeasurementSdk a;
    public final Map b;
    
    public h4(final AppMeasurementSdk a) {
        Preconditions.checkNotNull(a);
        this.a = a;
        this.b = new ConcurrentHashMap();
    }
    
    public static g4 d(final r10 r10, final Context context, final bs1 bs1) {
        Preconditions.checkNotNull(r10);
        Preconditions.checkNotNull(context);
        Preconditions.checkNotNull(bs1);
        Preconditions.checkNotNull(context.getApplicationContext());
        if (h4.c == null) {
            synchronized (h4.class) {
                if (h4.c == null) {
                    final Bundle bundle = new Bundle(1);
                    if (r10.w()) {
                        bs1.a(fp.class, ka2.a, kc2.a);
                        ((BaseBundle)bundle).putBoolean("dataCollectionDefaultEnabled", r10.v());
                    }
                    h4.c = new h4(zzdf.zza(context, (String)null, (String)null, (String)null, bundle).zzb());
                }
            }
        }
        return h4.c;
    }
    
    @Override
    public void a(final String s, final String s2, final Bundle bundle) {
        Bundle bundle2 = bundle;
        if (bundle == null) {
            bundle2 = new Bundle();
        }
        if (!lc2.g(s)) {
            return;
        }
        if (!lc2.c(s2, bundle2)) {
            return;
        }
        if (!lc2.e(s, s2, bundle2)) {
            return;
        }
        lc2.b(s, s2, bundle2);
        this.a.logEvent(s, s2, bundle2);
    }
    
    @Override
    public void b(final String s, final String s2, final Object o) {
        if (!lc2.g(s)) {
            return;
        }
        if (!lc2.d(s, s2)) {
            return;
        }
        this.a.setUserProperty(s, s2, o);
    }
    
    @Override
    public a c(final String s, final b b) {
        Preconditions.checkNotNull(b);
        if (!lc2.g(s)) {
            return null;
        }
        if (this.f(s)) {
            return null;
        }
        final AppMeasurementSdk a = this.a;
        Object o;
        if ("fiam".equals(s)) {
            o = new ff2(a, b);
        }
        else if ("clx".equals(s)) {
            o = new uf2(a, b);
        }
        else {
            o = null;
        }
        if (o != null) {
            this.b.put(s, o);
            return new a(this, s) {
                public final String a;
                public final h4 b;
            };
        }
        return null;
    }
    
    public final boolean f(final String s) {
        return !s.isEmpty() && this.b.containsKey(s) && this.b.get(s) != null;
    }
}

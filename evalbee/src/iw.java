import android.widget.TextView;
import android.text.method.NumberKeyListener;
import android.text.TextWatcher;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputConnection;
import android.text.method.KeyListener;
import android.widget.EditText;

// 
// Decompiled by Procyon v0.6.0
// 

public final class iw
{
    public final b a;
    public int b;
    public int c;
    
    public iw(final EditText editText, final boolean b) {
        this.b = Integer.MAX_VALUE;
        this.c = 0;
        l71.h(editText, "editText cannot be null");
        this.a = (b)new a(editText, b);
    }
    
    public KeyListener a(final KeyListener keyListener) {
        return this.a.a(keyListener);
    }
    
    public boolean b() {
        return this.a.b();
    }
    
    public InputConnection c(final InputConnection inputConnection, final EditorInfo editorInfo) {
        if (inputConnection == null) {
            return null;
        }
        return this.a.c(inputConnection, editorInfo);
    }
    
    public void d(final boolean b) {
        this.a.d(b);
    }
    
    public static class a extends b
    {
        public final EditText a;
        public final qw b;
        
        public a(final EditText a, final boolean b) {
            ((TextView)(this.a = a)).addTextChangedListener((TextWatcher)(this.b = new qw(a, b)));
            ((TextView)a).setEditableFactory(jw.getInstance());
        }
        
        @Override
        public KeyListener a(final KeyListener keyListener) {
            if (keyListener instanceof mw) {
                return keyListener;
            }
            if (keyListener == null) {
                return null;
            }
            if (keyListener instanceof NumberKeyListener) {
                return keyListener;
            }
            return (KeyListener)new mw(keyListener);
        }
        
        @Override
        public boolean b() {
            return this.b.b();
        }
        
        @Override
        public InputConnection c(final InputConnection inputConnection, final EditorInfo editorInfo) {
            if (inputConnection instanceof kw) {
                return inputConnection;
            }
            return (InputConnection)new kw((TextView)this.a, inputConnection, editorInfo);
        }
        
        @Override
        public void d(final boolean b) {
            this.b.d(b);
        }
    }
    
    public abstract static class b
    {
        public abstract KeyListener a(final KeyListener p0);
        
        public abstract boolean b();
        
        public abstract InputConnection c(final InputConnection p0, final EditorInfo p1);
        
        public abstract void d(final boolean p0);
    }
}

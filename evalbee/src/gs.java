import android.widget.CompoundButton;
import android.view.View;
import com.ekodroid.omrevaluator.database.TemplateDataJsonModel;
import java.util.Iterator;
import com.ekodroid.omrevaluator.clients.SyncClients.Models.ExamAction;
import com.google.firebase.auth.FirebaseAuth;
import com.ekodroid.omrevaluator.clients.UserProfileClients.models.OrgProfile;
import com.ekodroid.omrevaluator.database.repositories.ResultRepository;
import com.ekodroid.omrevaluator.database.repositories.TemplateRepository;
import com.ekodroid.omrevaluator.templateui.models.ExamId;
import com.google.firebase.analytics.FirebaseAnalytics;
import android.provider.Settings$Secure;
import android.content.DialogInterface;
import android.content.DialogInterface$OnClickListener;
import android.widget.RadioButton;
import android.view.ViewGroup;
import android.view.LayoutInflater;
import com.google.android.material.dialog.MaterialAlertDialogBuilder;
import java.util.ArrayList;
import android.content.Context;

// 
// Decompiled by Procyon v0.6.0
// 

public class gs
{
    public Context a;
    public y01 b;
    public ArrayList c;
    
    public gs(final Context a, final y01 b, final ArrayList c) {
        this.a = a;
        this.b = b;
        this.c = c;
        this.d();
    }
    
    public static /* synthetic */ Context a(final gs gs) {
        return gs.a;
    }
    
    public static /* synthetic */ ArrayList b(final gs gs) {
        return gs.c;
    }
    
    public static /* synthetic */ y01 c(final gs gs) {
        return gs.b;
    }
    
    public final void d() {
        final MaterialAlertDialogBuilder materialAlertDialogBuilder = new MaterialAlertDialogBuilder(this.a, 2131951953);
        final View inflate = ((LayoutInflater)this.a.getSystemService("layout_inflater")).inflate(2131492966, (ViewGroup)null);
        materialAlertDialogBuilder.setView(inflate);
        materialAlertDialogBuilder.setTitle(2131886236);
        final RadioButton radioButton = (RadioButton)inflate.findViewById(2131296986);
        final RadioButton radioButton2 = (RadioButton)inflate.findViewById(2131296985);
        materialAlertDialogBuilder.setPositiveButton(2131886231, (DialogInterface$OnClickListener)new DialogInterface$OnClickListener(this, radioButton) {
            public final RadioButton a;
            public final gs b;
            
            public void onClick(final DialogInterface dialogInterface, final int n) {
                final String string = Settings$Secure.getString(gs.a(this.b).getContentResolver(), "android_id");
                final FirebaseAnalytics instance = FirebaseAnalytics.getInstance(gs.a(this.b));
                if (((CompoundButton)this.a).isChecked()) {
                    for (final ExamId examId : gs.b(this.b)) {
                        final TemplateDataJsonModel templateJson = TemplateRepository.getInstance(gs.a(this.b)).getTemplateJson(examId);
                        if (templateJson != null) {
                            ResultRepository.getInstance(gs.a(this.b)).deleteAllSheetImageForExam(examId, templateJson.getFolderPath());
                        }
                        o4.b(instance, "DELETE_SHEET_IMAGES", string, "ExamSheetImageDelete");
                    }
                    a91.G(gs.a(this.b), 2131886571, 2131230927, 2131231085);
                }
                else {
                    final OrgProfile instance2 = OrgProfile.getInstance(gs.a(this.b));
                    final String o = FirebaseAuth.getInstance().e().O();
                    final ArrayList list = new ArrayList();
                    for (final ExamId examId2 : gs.b(this.b)) {
                        final TemplateRepository instance3 = TemplateRepository.getInstance(gs.a(this.b));
                        final TemplateDataJsonModel templateJson2 = instance3.getTemplateJson(examId2);
                        if (templateJson2 != null && !instance2.getOrgId().equals(o) && templateJson2.isCloudSyncOn() && !o.equals(templateJson2.getUserId())) {
                            final Context a = gs.a(this.b);
                            final StringBuilder sb = new StringBuilder();
                            sb.append(templateJson2.getName());
                            sb.append(gs.a(this.b).getString(2131886497));
                            a91.H(a, sb.toString(), 2131230909, 2131231086);
                        }
                        else {
                            if (templateJson2 != null) {
                                ResultRepository.getInstance(gs.a(this.b)).deleteAllSheetImageForExam(examId2, templateJson2.getFolderPath());
                                if (templateJson2.isCloudSyncOn()) {
                                    list.add(new ExamAction(templateJson2.getCloudId(), false, true));
                                }
                            }
                            instance3.deleteTemplateJson(examId2);
                            ResultRepository.getInstance(gs.a(this.b)).deleteAllResultForExam(examId2);
                            o4.b(instance, "DELETE_EXAM", string, "ExamDeleteComplete");
                        }
                    }
                    if (list.size() > 0) {
                        a91.I(gs.a(this.b), list);
                    }
                }
                ResultRepository.getInstance(gs.a(this.b)).vaccumDataBase();
                gs.c(this.b).a(null);
                dialogInterface.dismiss();
            }
        });
        materialAlertDialogBuilder.setNegativeButton(2131886163, (DialogInterface$OnClickListener)new DialogInterface$OnClickListener(this) {
            public final gs a;
            
            public void onClick(final DialogInterface dialogInterface, final int n) {
                dialogInterface.dismiss();
            }
        });
        materialAlertDialogBuilder.create().show();
    }
}

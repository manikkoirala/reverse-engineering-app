// 
// Decompiled by Procyon v0.6.0
// 

public class s9 implements t90
{
    @Override
    public double a(final double[] array, final int n) {
        return (Math.log(array[0] + 1.0) - Math.log(1.0 - array[0])) / 2.0;
    }
    
    @Override
    public boolean b(final int n) {
        boolean b = true;
        if (n != 1) {
            b = false;
        }
        return b;
    }
    
    @Override
    public String toString() {
        return "atanh(x)";
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

public class ao1 implements t90
{
    @Override
    public double a(final double[] array, final int n) {
        final double n2 = array[0];
        double n3 = 0.0;
        if (n2 > 0.0) {
            return 1.0;
        }
        if (n2 < 0.0) {
            n3 = -1.0;
        }
        return n3;
    }
    
    @Override
    public boolean b(final int n) {
        boolean b = true;
        if (n != 1) {
            b = false;
        }
        return b;
    }
    
    @Override
    public String toString() {
        return "sign(x)";
    }
}

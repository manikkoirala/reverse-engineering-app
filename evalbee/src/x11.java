import androidx.work.d;

// 
// Decompiled by Procyon v0.6.0
// 

public class x11 implements d
{
    public final tx0 c;
    public final um1 d;
    
    public x11() {
        this.c = new tx0();
        this.d = um1.s();
        this.a((d.b)androidx.work.d.b);
    }
    
    public void a(final d.b b) {
        this.c.l(b);
        if (b instanceof c) {
            this.d.o(b);
        }
        else if (b instanceof a) {
            this.d.p(((a)b).a());
        }
    }
    
    @Override
    public ik0 getResult() {
        return this.d;
    }
}

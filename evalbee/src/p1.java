import android.view.View;
import android.view.accessibility.AccessibilityRecord;

// 
// Decompiled by Procyon v0.6.0
// 

public abstract class p1
{
    public static void a(final AccessibilityRecord accessibilityRecord, final int n) {
        a.c(accessibilityRecord, n);
    }
    
    public static void b(final AccessibilityRecord accessibilityRecord, final int n) {
        a.d(accessibilityRecord, n);
    }
    
    public static void c(final AccessibilityRecord accessibilityRecord, final View view, final int n) {
        b.a(accessibilityRecord, view, n);
    }
    
    public abstract static class a
    {
        public static int a(final AccessibilityRecord accessibilityRecord) {
            return accessibilityRecord.getMaxScrollX();
        }
        
        public static int b(final AccessibilityRecord accessibilityRecord) {
            return accessibilityRecord.getMaxScrollY();
        }
        
        public static void c(final AccessibilityRecord accessibilityRecord, final int maxScrollX) {
            accessibilityRecord.setMaxScrollX(maxScrollX);
        }
        
        public static void d(final AccessibilityRecord accessibilityRecord, final int maxScrollY) {
            accessibilityRecord.setMaxScrollY(maxScrollY);
        }
    }
    
    public abstract static class b
    {
        public static void a(final AccessibilityRecord accessibilityRecord, final View view, final int n) {
            accessibilityRecord.setSource(view, n);
        }
    }
}

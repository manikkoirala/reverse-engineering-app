import java.util.Iterator;
import java.util.Collection;
import java.util.Set;
import java.util.Map;

// 
// Decompiled by Procyon v0.6.0
// 

public class r8 extends co1 implements Map
{
    public ym0 h;
    
    public r8() {
    }
    
    public r8(final int n) {
        super(n);
    }
    
    public r8(final co1 co1) {
        super(co1);
    }
    
    @Override
    public Set entrySet() {
        return this.n().l();
    }
    
    @Override
    public Set keySet() {
        return this.n().m();
    }
    
    public final ym0 n() {
        if (this.h == null) {
            this.h = new ym0(this) {
                public final r8 d;
                
                @Override
                public void a() {
                    this.d.clear();
                }
                
                @Override
                public Object b(final int n, final int n2) {
                    return this.d.b[(n << 1) + n2];
                }
                
                @Override
                public Map c() {
                    return this.d;
                }
                
                @Override
                public int d() {
                    return this.d.c;
                }
                
                @Override
                public int e(final Object o) {
                    return this.d.f(o);
                }
                
                @Override
                public int f(final Object o) {
                    return this.d.h(o);
                }
                
                @Override
                public void g(final Object o, final Object o2) {
                    this.d.put(o, o2);
                }
                
                @Override
                public void h(final int n) {
                    this.d.k(n);
                }
                
                @Override
                public Object i(final int n, final Object o) {
                    return this.d.l(n, o);
                }
            };
        }
        return this.h;
    }
    
    public boolean o(final Collection collection) {
        return ym0.p(this, collection);
    }
    
    @Override
    public void putAll(final Map map) {
        this.c(super.c + map.size());
        for (final Entry<Object, V> entry : map.entrySet()) {
            this.put(entry.getKey(), entry.getValue());
        }
    }
    
    @Override
    public Collection values() {
        return this.n().n();
    }
}

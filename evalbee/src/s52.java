import java.util.Iterator;
import android.os.PowerManager;
import android.content.Context;
import android.os.PowerManager$WakeLock;
import java.util.Map;
import java.util.LinkedHashMap;

// 
// Decompiled by Procyon v0.6.0
// 

public abstract class s52
{
    public static final String a;
    
    static {
        final String i = xl0.i("WakeLocks");
        fg0.d((Object)i, "tagWithPrefix(\"WakeLocks\")");
        a = i;
    }
    
    public static final void a() {
        final LinkedHashMap linkedHashMap = new LinkedHashMap();
        Object o = t52.a;
        synchronized (o) {
            linkedHashMap.putAll(((t52)o).a());
            final u02 a = u02.a;
            monitorexit(o);
            o = linkedHashMap.entrySet().iterator();
            while (((Iterator)o).hasNext()) {
                final Map.Entry<PowerManager$WakeLock, V> entry = (Map.Entry<PowerManager$WakeLock, V>)((Iterator)o).next();
                final PowerManager$WakeLock powerManager$WakeLock = entry.getKey();
                final String str = (String)entry.getValue();
                int n = 0;
                if (powerManager$WakeLock != null) {
                    n = n;
                    if (powerManager$WakeLock.isHeld()) {
                        n = 1;
                    }
                }
                if (n != 0) {
                    final xl0 e = xl0.e();
                    final String a2 = s52.a;
                    final StringBuilder sb = new StringBuilder();
                    sb.append("WakeLock held for ");
                    sb.append(str);
                    e.k(a2, sb.toString());
                }
            }
        }
    }
    
    public static final PowerManager$WakeLock b(Context a, final String str) {
        fg0.e((Object)a, "context");
        fg0.e((Object)str, "tag");
        final Object systemService = a.getApplicationContext().getSystemService("power");
        fg0.c(systemService, "null cannot be cast to non-null type android.os.PowerManager");
        final PowerManager powerManager = (PowerManager)systemService;
        final StringBuilder sb = new StringBuilder();
        sb.append("WorkManager: ");
        sb.append(str);
        final String string = sb.toString();
        final PowerManager$WakeLock wakeLock = powerManager.newWakeLock(1, string);
        a = (Context)t52.a;
        synchronized (a) {
            final String s = ((t52)a).a().put(wakeLock, string);
            monitorexit(a);
            fg0.d((Object)wakeLock, "wakeLock");
            return wakeLock;
        }
    }
}

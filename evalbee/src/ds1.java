import android.content.pm.ComponentInfo;
import android.os.BaseBundle;
import android.net.Uri$Builder;
import java.util.List;
import android.content.res.Resources;
import java.io.InputStream;
import java.io.IOException;
import android.content.res.Resources$NotFoundException;
import java.io.FileNotFoundException;
import android.net.Uri;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager$NameNotFoundException;
import android.content.ComponentName;
import android.text.style.TextAppearanceSpan;
import android.text.SpannableString;
import android.util.TypedValue;
import android.graphics.drawable.Drawable$ConstantState;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.text.TextUtils;
import android.view.View;
import android.graphics.drawable.Drawable;
import android.util.Log;
import android.database.Cursor;
import android.content.res.ColorStateList;
import java.util.WeakHashMap;
import android.content.Context;
import android.app.SearchableInfo;
import androidx.appcompat.widget.SearchView;
import android.view.View$OnClickListener;

// 
// Decompiled by Procyon v0.6.0
// 

public class ds1 extends ie1 implements View$OnClickListener
{
    public int A;
    public int C;
    public int D;
    public final SearchView l;
    public final SearchableInfo m;
    public final Context n;
    public final WeakHashMap p;
    public final int q;
    public boolean t;
    public int v;
    public ColorStateList w;
    public int x;
    public int y;
    public int z;
    
    public ds1(final Context n, final SearchView l, final SearchableInfo m, final WeakHashMap p4) {
        super(n, l.getSuggestionRowLayout(), null, true);
        this.t = false;
        this.v = 1;
        this.x = -1;
        this.y = -1;
        this.z = -1;
        this.A = -1;
        this.C = -1;
        this.D = -1;
        this.l = l;
        this.m = m;
        this.q = l.getSuggestionCommitIconResId();
        this.n = n;
        this.p = p4;
    }
    
    public static String o(final Cursor cursor, final String s) {
        return w(cursor, cursor.getColumnIndex(s));
    }
    
    public static String w(final Cursor cursor, final int n) {
        if (n == -1) {
            return null;
        }
        try {
            return cursor.getString(n);
        }
        catch (final Exception ex) {
            Log.e("SuggestionsAdapter", "unexpected error retrieving valid column from cursor, did the remote process die?", (Throwable)ex);
            return null;
        }
    }
    
    public final void A(final String key, final Drawable drawable) {
        if (drawable != null) {
            this.p.put(key, drawable.getConstantState());
        }
    }
    
    public final void B(final Cursor cursor) {
        Object extras;
        if (cursor != null) {
            extras = cursor.getExtras();
        }
        else {
            extras = null;
        }
        if (extras != null) {
            ((BaseBundle)extras).getBoolean("in_progress");
        }
    }
    
    public void a(final Cursor cursor) {
        if (this.t) {
            Log.w("SuggestionsAdapter", "Tried to change cursor after adapter was closed.");
            if (cursor != null) {
                cursor.close();
            }
            return;
        }
        try {
            super.a(cursor);
            if (cursor != null) {
                this.x = cursor.getColumnIndex("suggest_text_1");
                this.y = cursor.getColumnIndex("suggest_text_2");
                this.z = cursor.getColumnIndex("suggest_text_2_url");
                this.A = cursor.getColumnIndex("suggest_icon_1");
                this.C = cursor.getColumnIndex("suggest_icon_2");
                this.D = cursor.getColumnIndex("suggest_flags");
            }
        }
        catch (final Exception ex) {
            Log.e("SuggestionsAdapter", "error changing cursor and caching columns", (Throwable)ex);
        }
    }
    
    public CharSequence c(final Cursor cursor) {
        if (cursor == null) {
            return null;
        }
        final String o = o(cursor, "suggest_intent_query");
        if (o != null) {
            return o;
        }
        if (this.m.shouldRewriteQueryFromData()) {
            final String o2 = o(cursor, "suggest_intent_data");
            if (o2 != null) {
                return o2;
            }
        }
        if (this.m.shouldRewriteQueryFromText()) {
            final String o3 = o(cursor, "suggest_text_1");
            if (o3 != null) {
                return o3;
            }
        }
        return null;
    }
    
    public Cursor d(final CharSequence charSequence) {
        String string;
        if (charSequence == null) {
            string = "";
        }
        else {
            string = charSequence.toString();
        }
        if (((View)this.l).getVisibility() == 0) {
            if (((View)this.l).getWindowVisibility() == 0) {
                try {
                    final Cursor v = this.v(this.m, string, 50);
                    if (v != null) {
                        v.getCount();
                        return v;
                    }
                }
                catch (final RuntimeException ex) {
                    Log.w("SuggestionsAdapter", "Search suggestions query threw an exception.", (Throwable)ex);
                }
            }
        }
        return null;
    }
    
    public void e(final View view, final Context context, final Cursor cursor) {
        final a a = (a)view.getTag();
        final int d = this.D;
        int int1;
        if (d != -1) {
            int1 = cursor.getInt(d);
        }
        else {
            int1 = 0;
        }
        if (a.a != null) {
            this.z(a.a, w(cursor, this.x));
        }
        if (a.b != null) {
            final String w = w(cursor, this.z);
            CharSequence charSequence;
            if (w != null) {
                charSequence = this.l(w);
            }
            else {
                charSequence = w(cursor, this.y);
            }
            if (TextUtils.isEmpty(charSequence)) {
                final TextView a2 = a.a;
                if (a2 != null) {
                    a2.setSingleLine(false);
                    a.a.setMaxLines(2);
                }
            }
            else {
                final TextView a3 = a.a;
                if (a3 != null) {
                    a3.setSingleLine(true);
                    a.a.setMaxLines(1);
                }
            }
            this.z(a.b, charSequence);
        }
        final ImageView c = a.c;
        if (c != null) {
            this.y(c, this.t(cursor), 4);
        }
        final ImageView d2 = a.d;
        if (d2 != null) {
            this.y(d2, this.u(cursor), 8);
        }
        final int v = this.v;
        if (v != 2 && (v != 1 || (int1 & 0x1) == 0x0)) {
            a.e.setVisibility(8);
        }
        else {
            a.e.setVisibility(0);
            ((View)a.e).setTag((Object)a.a.getText());
            ((View)a.e).setOnClickListener((View$OnClickListener)this);
        }
    }
    
    public View getDropDownView(final int n, View dropDownView, final ViewGroup viewGroup) {
        try {
            dropDownView = super.getDropDownView(n, dropDownView, viewGroup);
            return dropDownView;
        }
        catch (final RuntimeException ex) {
            Log.w("SuggestionsAdapter", "Search suggestions cursor threw exception.", (Throwable)ex);
            final View g = this.g(this.n, this.b(), viewGroup);
            if (g != null) {
                ((a)g.getTag()).a.setText((CharSequence)ex.toString());
            }
            return g;
        }
    }
    
    public View getView(final int n, View view, final ViewGroup viewGroup) {
        try {
            view = super.getView(n, view, viewGroup);
            return view;
        }
        catch (final RuntimeException ex) {
            Log.w("SuggestionsAdapter", "Search suggestions cursor threw exception.", (Throwable)ex);
            final View h = this.h(this.n, this.b(), viewGroup);
            if (h != null) {
                ((a)h.getTag()).a.setText((CharSequence)ex.toString());
            }
            return h;
        }
    }
    
    @Override
    public View h(final Context context, final Cursor cursor, final ViewGroup viewGroup) {
        final View h = super.h(context, cursor, viewGroup);
        h.setTag((Object)new a(h));
        ((ImageView)h.findViewById(db1.q)).setImageResource(this.q);
        return h;
    }
    
    public boolean hasStableIds() {
        return false;
    }
    
    public final Drawable k(final String key) {
        final Drawable$ConstantState drawable$ConstantState = this.p.get(key);
        if (drawable$ConstantState == null) {
            return null;
        }
        return drawable$ConstantState.newDrawable();
    }
    
    public final CharSequence l(final CharSequence charSequence) {
        if (this.w == null) {
            final TypedValue typedValue = new TypedValue();
            this.n.getTheme().resolveAttribute(sa1.M, typedValue, true);
            this.w = this.n.getResources().getColorStateList(typedValue.resourceId);
        }
        final SpannableString spannableString = new SpannableString(charSequence);
        spannableString.setSpan((Object)new TextAppearanceSpan((String)null, 0, 0, this.w, (ColorStateList)null), 0, charSequence.length(), 33);
        return (CharSequence)spannableString;
    }
    
    public final Drawable m(final ComponentName componentName) {
        final PackageManager packageManager = this.n.getPackageManager();
        while (true) {
            try {
                final ActivityInfo activityInfo = packageManager.getActivityInfo(componentName, 128);
                final int iconResource = ((ComponentInfo)activityInfo).getIconResource();
                if (iconResource == 0) {
                    return null;
                }
                final Drawable drawable = packageManager.getDrawable(componentName.getPackageName(), iconResource, activityInfo.applicationInfo);
                if (drawable == null) {
                    final StringBuilder sb = new StringBuilder();
                    sb.append("Invalid icon resource ");
                    sb.append(iconResource);
                    sb.append(" for ");
                    sb.append(componentName.flattenToShortString());
                    final String s = sb.toString();
                    Log.w("SuggestionsAdapter", s);
                    return null;
                }
                return drawable;
            }
            catch (final PackageManager$NameNotFoundException ex) {
                final String s = ex.toString();
                continue;
            }
            break;
        }
    }
    
    public final Drawable n(final ComponentName componentName) {
        final String flattenToShortString = componentName.flattenToShortString();
        final boolean containsKey = this.p.containsKey(flattenToShortString);
        final Drawable$ConstantState drawable$ConstantState = null;
        final Drawable drawable = null;
        if (containsKey) {
            final Drawable$ConstantState drawable$ConstantState2 = this.p.get(flattenToShortString);
            Drawable drawable2;
            if (drawable$ConstantState2 == null) {
                drawable2 = drawable;
            }
            else {
                drawable2 = drawable$ConstantState2.newDrawable(this.n.getResources());
            }
            return drawable2;
        }
        final Drawable m = this.m(componentName);
        Drawable$ConstantState constantState;
        if (m == null) {
            constantState = drawable$ConstantState;
        }
        else {
            constantState = m.getConstantState();
        }
        this.p.put(flattenToShortString, constantState);
        return m;
    }
    
    public void notifyDataSetChanged() {
        super.notifyDataSetChanged();
        this.B(this.b());
    }
    
    public void notifyDataSetInvalidated() {
        super.notifyDataSetInvalidated();
        this.B(this.b());
    }
    
    public void onClick(final View view) {
        final Object tag = view.getTag();
        if (tag instanceof CharSequence) {
            this.l.z((CharSequence)tag);
        }
    }
    
    public final Drawable p() {
        final Drawable n = this.n(this.m.getSearchActivity());
        if (n != null) {
            return n;
        }
        return this.n.getPackageManager().getDefaultActivityIcon();
    }
    
    public final Drawable q(final Uri uri) {
        try {
            if ("android.resource".equals(uri.getScheme())) {
                try {
                    return this.r(uri);
                }
                catch (final Resources$NotFoundException ex) {
                    final StringBuilder sb = new StringBuilder();
                    sb.append("Resource does not exist: ");
                    sb.append(uri);
                    throw new FileNotFoundException(sb.toString());
                }
            }
            final InputStream openInputStream = this.n.getContentResolver().openInputStream(uri);
            if (openInputStream != null) {
                try {
                    return Drawable.createFromStream(openInputStream, (String)null);
                }
                finally {
                    try {
                        openInputStream.close();
                    }
                    catch (final IOException ex2) {
                        final StringBuilder sb2 = new StringBuilder();
                        sb2.append("Error closing icon stream for ");
                        sb2.append(uri);
                        Log.e("SuggestionsAdapter", sb2.toString(), (Throwable)ex2);
                    }
                }
            }
            final StringBuilder sb3 = new StringBuilder();
            sb3.append("Failed to open ");
            sb3.append(uri);
            throw new FileNotFoundException(sb3.toString());
        }
        catch (final FileNotFoundException ex3) {
            final StringBuilder sb4 = new StringBuilder();
            sb4.append("Icon not found: ");
            sb4.append(uri);
            sb4.append(", ");
            sb4.append(ex3.getMessage());
            Log.w("SuggestionsAdapter", sb4.toString());
            return null;
        }
    }
    
    public Drawable r(final Uri uri) {
        final String authority = uri.getAuthority();
        if (!TextUtils.isEmpty((CharSequence)authority)) {
            try {
                final Resources resourcesForApplication = this.n.getPackageManager().getResourcesForApplication(authority);
                final List pathSegments = uri.getPathSegments();
                if (pathSegments == null) {
                    final StringBuilder sb = new StringBuilder();
                    sb.append("No path: ");
                    sb.append(uri);
                    throw new FileNotFoundException(sb.toString());
                }
                final int size = pathSegments.size();
                int n = 0;
                Label_0134: {
                    if (size == 1) {
                        try {
                            n = Integer.parseInt(pathSegments.get(0));
                            break Label_0134;
                        }
                        catch (final NumberFormatException ex) {
                            final StringBuilder sb2 = new StringBuilder();
                            sb2.append("Single path segment is not a resource ID: ");
                            sb2.append(uri);
                            throw new FileNotFoundException(sb2.toString());
                        }
                    }
                    if (size != 2) {
                        final StringBuilder sb3 = new StringBuilder();
                        sb3.append("More than two path segments: ");
                        sb3.append(uri);
                        throw new FileNotFoundException(sb3.toString());
                    }
                    n = resourcesForApplication.getIdentifier((String)pathSegments.get(1), (String)pathSegments.get(0), authority);
                }
                if (n != 0) {
                    return resourcesForApplication.getDrawable(n);
                }
                final StringBuilder sb4 = new StringBuilder();
                sb4.append("No resource found for: ");
                sb4.append(uri);
                throw new FileNotFoundException(sb4.toString());
            }
            catch (final PackageManager$NameNotFoundException ex2) {
                final StringBuilder sb5 = new StringBuilder();
                sb5.append("No package found for authority: ");
                sb5.append(uri);
                throw new FileNotFoundException(sb5.toString());
            }
        }
        final StringBuilder sb6 = new StringBuilder();
        sb6.append("No authority: ");
        sb6.append(uri);
        throw new FileNotFoundException(sb6.toString());
    }
    
    public final Drawable s(final String str) {
        Drawable q;
        final Drawable drawable = q = null;
        if (str != null) {
            q = drawable;
            if (!str.isEmpty()) {
                if ("0".equals(str)) {
                    q = drawable;
                }
                else {
                    try {
                        final int int1 = Integer.parseInt(str);
                        final StringBuilder sb = new StringBuilder();
                        sb.append("android.resource://");
                        sb.append(this.n.getPackageName());
                        sb.append("/");
                        sb.append(int1);
                        final String string = sb.toString();
                        final Drawable k = this.k(string);
                        if (k != null) {
                            return k;
                        }
                        final Drawable drawable2 = sl.getDrawable(this.n, int1);
                        this.A(string, drawable2);
                        return drawable2;
                    }
                    catch (final Resources$NotFoundException ex) {
                        final StringBuilder sb2 = new StringBuilder();
                        sb2.append("Icon resource not found: ");
                        sb2.append(str);
                        Log.w("SuggestionsAdapter", sb2.toString());
                        return null;
                    }
                    catch (final NumberFormatException ex2) {
                        final Drawable i = this.k(str);
                        if (i != null) {
                            return i;
                        }
                        q = this.q(Uri.parse(str));
                        this.A(str, q);
                    }
                }
            }
        }
        return q;
    }
    
    public final Drawable t(final Cursor cursor) {
        final int a = this.A;
        if (a == -1) {
            return null;
        }
        final Drawable s = this.s(cursor.getString(a));
        if (s != null) {
            return s;
        }
        return this.p();
    }
    
    public final Drawable u(final Cursor cursor) {
        final int c = this.C;
        if (c == -1) {
            return null;
        }
        return this.s(cursor.getString(c));
    }
    
    public Cursor v(final SearchableInfo searchableInfo, final String s, final int i) {
        final String[] array = null;
        if (searchableInfo == null) {
            return null;
        }
        final String suggestAuthority = searchableInfo.getSuggestAuthority();
        if (suggestAuthority == null) {
            return null;
        }
        final Uri$Builder fragment = new Uri$Builder().scheme("content").authority(suggestAuthority).query("").fragment("");
        final String suggestPath = searchableInfo.getSuggestPath();
        if (suggestPath != null) {
            fragment.appendEncodedPath(suggestPath);
        }
        fragment.appendPath("search_suggest_query");
        final String suggestSelection = searchableInfo.getSuggestSelection();
        String[] array2;
        if (suggestSelection != null) {
            array2 = new String[] { s };
        }
        else {
            fragment.appendPath(s);
            array2 = array;
        }
        if (i > 0) {
            fragment.appendQueryParameter("limit", String.valueOf(i));
        }
        return this.n.getContentResolver().query(fragment.build(), (String[])null, suggestSelection, array2, (String)null);
    }
    
    public void x(final int v) {
        this.v = v;
    }
    
    public final void y(final ImageView imageView, final Drawable imageDrawable, final int visibility) {
        imageView.setImageDrawable(imageDrawable);
        if (imageDrawable == null) {
            imageView.setVisibility(visibility);
        }
        else {
            imageView.setVisibility(0);
            imageDrawable.setVisible(false, false);
            imageDrawable.setVisible(true, false);
        }
    }
    
    public final void z(final TextView textView, final CharSequence text) {
        textView.setText(text);
        int visibility;
        if (TextUtils.isEmpty(text)) {
            visibility = 8;
        }
        else {
            visibility = 0;
        }
        ((View)textView).setVisibility(visibility);
    }
    
    public static final class a
    {
        public final TextView a;
        public final TextView b;
        public final ImageView c;
        public final ImageView d;
        public final ImageView e;
        
        public a(final View view) {
            this.a = (TextView)view.findViewById(16908308);
            this.b = (TextView)view.findViewById(16908309);
            this.c = (ImageView)view.findViewById(16908295);
            this.d = (ImageView)view.findViewById(16908296);
            this.e = (ImageView)view.findViewById(db1.q);
        }
    }
}

import androidx.room.RoomDatabase;
import android.content.Context;

// 
// Decompiled by Procyon v0.6.0
// 

public final class pf1
{
    public static final pf1 a;
    
    static {
        a = new pf1();
    }
    
    public static final RoomDatabase.a a(final Context context, final Class clazz, final String s) {
        fg0.e((Object)context, "context");
        fg0.e((Object)clazz, "klass");
        if (true ^ (s == null || qr1.l((CharSequence)s))) {
            return new RoomDatabase.a(context, clazz, s);
        }
        throw new IllegalArgumentException("Cannot build a database with null or empty name. If you are trying to create an in memory database, use Room.inMemoryDatabaseBuilder".toString());
    }
    
    public static final Object b(final Class clazz, String string) {
        fg0.e((Object)clazz, "klass");
        fg0.e((Object)string, "suffix");
        final Package package1 = clazz.getPackage();
        fg0.b((Object)package1);
        final String name = package1.getName();
        String s = clazz.getCanonicalName();
        fg0.b((Object)s);
        fg0.d((Object)name, "fullPackage");
        final int length = name.length();
        final int n = 0;
        if (length != 0) {
            s = s.substring(name.length() + 1);
            fg0.d((Object)s, "this as java.lang.String).substring(startIndex)");
        }
        final StringBuilder sb = new StringBuilder();
        sb.append(qr1.q(s, '.', '_', false, 4, (Object)null));
        sb.append(string);
        final String string2 = sb.toString();
        int n2 = n;
        try {
            if (name.length() == 0) {
                n2 = 1;
            }
            if (n2 != 0) {
                string = string2;
            }
            else {
                final StringBuilder sb2 = new StringBuilder();
                sb2.append(name);
                sb2.append('.');
                sb2.append(string2);
                string = sb2.toString();
            }
            final Class<?> forName = Class.forName(string, true, clazz.getClassLoader());
            fg0.c((Object)forName, "null cannot be cast to non-null type java.lang.Class<T of androidx.room.Room.getGeneratedImplementation>");
            return forName.newInstance();
        }
        catch (final InstantiationException ex) {
            final StringBuilder sb3 = new StringBuilder();
            sb3.append("Failed to create an instance of ");
            sb3.append(clazz);
            sb3.append(".canonicalName");
            throw new RuntimeException(sb3.toString());
        }
        catch (final IllegalAccessException ex2) {
            final StringBuilder sb4 = new StringBuilder();
            sb4.append("Cannot access the constructor ");
            sb4.append(clazz);
            sb4.append(".canonicalName");
            throw new RuntimeException(sb4.toString());
        }
        catch (final ClassNotFoundException ex3) {
            final StringBuilder sb5 = new StringBuilder();
            sb5.append("Cannot find implementation for ");
            sb5.append(clazz.getCanonicalName());
            sb5.append(". ");
            sb5.append(string2);
            sb5.append(" does not exist");
            throw new RuntimeException(sb5.toString());
        }
    }
    
    public static final RoomDatabase.a c(final Context context, final Class clazz) {
        fg0.e((Object)context, "context");
        fg0.e((Object)clazz, "klass");
        return new RoomDatabase.a(context, clazz, null);
    }
}

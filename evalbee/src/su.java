import android.app.Dialog;
import org.apache.poi.POIXMLDocument;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import java.io.IOException;
import java.io.OutputStream;
import java.io.FileOutputStream;
import java.io.File;
import org.apache.poi.ss.usermodel.RichTextString;
import org.apache.poi.xssf.usermodel.XSSFRichTextString;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import com.ekodroid.omrevaluator.database.repositories.TemplateRepository;
import android.app.ProgressDialog;
import android.os.AsyncTask;
import com.ekodroid.omrevaluator.templateui.scanner.ResultItem;
import com.ekodroid.omrevaluator.database.ResultDataJsonModel;
import com.ekodroid.omrevaluator.database.repositories.ResultRepository;
import com.ekodroid.omrevaluator.database.StudentDataModel;
import java.util.Iterator;
import com.ekodroid.omrevaluator.database.repositories.ClassRepository;
import java.util.ArrayList;
import com.ekodroid.omrevaluator.templateui.models.SheetTemplate2;
import android.content.Context;
import com.ekodroid.omrevaluator.templateui.models.ExamId;

// 
// Decompiled by Procyon v0.6.0
// 

public class su
{
    public ExamId a;
    public Context b;
    public boolean c;
    
    public su(final Context b, final ExamId a, final boolean c) {
        this.a = a;
        this.b = b;
        this.c = c;
        new b(null).execute((Object[])new Void[0]);
    }
    
    public static /* synthetic */ ExamId a(final su su) {
        return su.a;
    }
    
    public static /* synthetic */ Context b(final su su) {
        return su.b;
    }
    
    public final void d(final Context context, final ArrayList list) {
        for (final ye1 ye1 : list) {
            final StudentDataModel student = ClassRepository.getInstance(context).getStudent(ye1.g(), this.a.getClassName());
            if (student != null) {
                ye1.m(student.getStudentName());
                ye1.p(false);
            }
            else {
                ye1.m(" - - - ");
            }
        }
    }
    
    public final ArrayList e(final Context context, final SheetTemplate2 sheetTemplate2) {
        final ArrayList<ResultDataJsonModel> allResultJson = ResultRepository.getInstance(context).getAllResultJson(this.a);
        final ArrayList list = new ArrayList();
        for (final ResultDataJsonModel resultDataJsonModel : allResultJson) {
            if (resultDataJsonModel != null) {
                final ResultItem resultItem = resultDataJsonModel.getResultItem(sheetTemplate2);
                final int p2 = ve1.p(resultItem);
                final int q = ve1.q(resultItem);
                final int s = ve1.s(resultItem);
                final double totalMarks = resultItem.getTotalMarks();
                list.add(new ye1(resultDataJsonModel.getRollNo(), totalMarks, ve1.i(totalMarks, sheetTemplate2.getGradeLevels()), p2, q, s, resultDataJsonModel.isSmsSent(), resultDataJsonModel.isSynced(), resultDataJsonModel.isPublished()));
            }
        }
        ve1.a(list, sheetTemplate2.getRankingMethod());
        this.d(context, list);
        return list;
    }
    
    public class b extends AsyncTask
    {
        public String[][] a;
        public String b;
        public String c;
        public ProgressDialog d;
        public final su e;
        
        public b(final su e) {
            this.e = e;
            this.b = null;
            final StringBuilder sb = new StringBuilder();
            sb.append(su.a(e).getExamName().replaceAll("[^a-zA-Z0-9_-]", "_"));
            sb.append(".xls");
            this.c = sb.toString();
        }
        
        public Void a(Void... externalFilesDir) {
            final SheetTemplate2 sheetTemplate = TemplateRepository.getInstance(su.b(this.e)).getTemplateJson(su.a(this.e)).getSheetTemplate();
            final su e = this.e;
            this.a = new we1().d(su.b(this.e), su.a(this.e), e.e(su.b(e), sheetTemplate), sheetTemplate, this.e.c);
            final XSSFWorkbook xssfWorkbook = new XSSFWorkbook();
            final XSSFSheet sheet = xssfWorkbook.createSheet("Reports");
            int n = 0;
            while (true) {
                final String[][] a = this.a;
                if (n >= a.length) {
                    break;
                }
                final String[] array = a[n];
                final XSSFRow row = sheet.createRow(n);
                for (int i = 0; i < array.length; ++i) {
                    row.createCell(i).setCellValue((RichTextString)new XSSFRichTextString(array[i]));
                }
                ++n;
            }
            externalFilesDir = (IOException)su.b(this.e).getExternalFilesDir(ok.c);
            if (!((File)externalFilesDir).exists()) {
                ((File)externalFilesDir).mkdirs();
            }
            final File file = new File((File)externalFilesDir, this.c);
            this.b = file.getAbsolutePath();
            IOException ex3 = null;
            Label_0329: {
                FileOutputStream fileOutputStream;
                try {
                    fileOutputStream = (FileOutputStream)(externalFilesDir = (IOException)new FileOutputStream(file));
                    try {
                        final Object o = xssfWorkbook;
                        final FileOutputStream fileOutputStream2 = fileOutputStream;
                        ((POIXMLDocument)o).write((OutputStream)fileOutputStream2);
                        externalFilesDir = (IOException)fileOutputStream;
                        final XSSFWorkbook xssfWorkbook2 = xssfWorkbook;
                        ((POIXMLDocument)xssfWorkbook2).close();
                        final FileOutputStream fileOutputStream3 = fileOutputStream;
                        fileOutputStream3.flush();
                        final FileOutputStream fileOutputStream4 = fileOutputStream;
                        fileOutputStream4.close();
                    }
                    catch (final Exception ex) {}
                }
                catch (final Exception ex) {
                    fileOutputStream = null;
                }
                finally {
                    final IOException ex2;
                    externalFilesDir = ex2;
                    break Label_0329;
                }
                try {
                    final Object o = xssfWorkbook;
                    final FileOutputStream fileOutputStream2 = fileOutputStream;
                    ((POIXMLDocument)o).write((OutputStream)fileOutputStream2);
                    externalFilesDir = (IOException)fileOutputStream;
                    final XSSFWorkbook xssfWorkbook2 = xssfWorkbook;
                    ((POIXMLDocument)xssfWorkbook2).close();
                    try {
                        final FileOutputStream fileOutputStream3 = fileOutputStream;
                        fileOutputStream3.flush();
                        final FileOutputStream fileOutputStream4 = fileOutputStream;
                        fileOutputStream4.close();
                        return null;
                        while (true) {
                            fileOutputStream.flush();
                            fileOutputStream.close();
                            return null;
                            final Exception ex;
                            ex.printStackTrace();
                            externalFilesDir = (IOException)fileOutputStream;
                            file.deleteOnExit();
                            iftrue(Label_0319:)(fileOutputStream == null);
                            continue;
                        }
                    }
                    catch (final IOException externalFilesDir) {
                        externalFilesDir.printStackTrace();
                    }
                    Label_0319: {
                        return null;
                    }
                }
                finally {
                    ex3 = externalFilesDir;
                    final IOException ex4;
                    externalFilesDir = ex4;
                }
            }
            if (ex3 != null) {
                try {
                    ((OutputStream)ex3).flush();
                    ((FileOutputStream)ex3).close();
                }
                catch (final IOException ex5) {
                    ex5.printStackTrace();
                }
            }
            throw externalFilesDir;
        }
        
        public void b(final Void void1) {
            super.onPostExecute((Object)void1);
            ((Dialog)this.d).dismiss();
            new hn1(su.b(this.e), this.b, this.c);
        }
        
        public void onPreExecute() {
            (this.d = new ProgressDialog(su.b(this.e))).setMessage((CharSequence)su.b(this.e).getString(2131886467));
            ((Dialog)this.d).show();
        }
    }
}

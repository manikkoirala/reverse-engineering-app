import com.google.android.gms.internal.play_billing.zzet;
import com.google.android.gms.internal.play_billing.zzic;
import com.google.android.gms.internal.play_billing.zziz;
import com.google.android.gms.internal.play_billing.zziu;
import com.google.android.gms.internal.play_billing.zzb;
import com.google.android.gms.internal.play_billing.zziv;
import com.google.android.gms.internal.play_billing.zzhy;
import android.content.Context;
import com.google.android.gms.internal.play_billing.zzio;

// 
// Decompiled by Procyon v0.6.0
// 

public final class md2 implements dd2
{
    public final zzio a;
    public final qd2 b;
    
    public md2(final Context context, final zzio a) {
        this.b = new qd2(context);
        this.a = a;
    }
    
    @Override
    public final void a(final zzhy zzhy) {
        if (zzhy == null) {
            return;
        }
        try {
            final zziu zzv = zziv.zzv();
            final zzio a = this.a;
            if (a != null) {
                zzv.zzk(a);
            }
            zzv.zzi(zzhy);
            this.b.a((zziv)((zzet)zzv).zzc());
        }
        finally {
            zzb.zzk("BillingLogger", "Unable to log.");
        }
    }
    
    @Override
    public final void b(final zziz zziz) {
        if (zziz == null) {
            return;
        }
        try {
            final zziu zzv = zziv.zzv();
            final zzio a = this.a;
            if (a != null) {
                zzv.zzk(a);
            }
            zzv.zzl(zziz);
            this.b.a((zziv)((zzet)zzv).zzc());
        }
        finally {
            zzb.zzk("BillingLogger", "Unable to log.");
        }
    }
    
    @Override
    public final void c(final zzic zzic) {
        if (zzic == null) {
            return;
        }
        try {
            final zziu zzv = zziv.zzv();
            final zzio a = this.a;
            if (a != null) {
                zzv.zzk(a);
            }
            zzv.zzj(zzic);
            this.b.a((zziv)((zzet)zzv).zzc());
        }
        finally {
            zzb.zzk("BillingLogger", "Unable to log.");
        }
    }
}

import android.os.Bundle;
import androidx.savedstate.Recreator;
import androidx.lifecycle.Lifecycle;
import androidx.savedstate.a;

// 
// Decompiled by Procyon v0.6.0
// 

public final class zi1
{
    public static final a d;
    public final aj1 a;
    public final androidx.savedstate.a b;
    public boolean c;
    
    static {
        d = new a(null);
    }
    
    public zi1(final aj1 a) {
        this.a = a;
        this.b = new androidx.savedstate.a();
    }
    
    public static final zi1 a(final aj1 aj1) {
        return zi1.d.a(aj1);
    }
    
    public final androidx.savedstate.a b() {
        return this.b;
    }
    
    public final void c() {
        final Lifecycle lifecycle = this.a.getLifecycle();
        if (lifecycle.b() == Lifecycle.State.INITIALIZED) {
            lifecycle.a(new Recreator(this.a));
            this.b.e(lifecycle);
            this.c = true;
            return;
        }
        throw new IllegalStateException("Restarter must be created only during owner's initialization stage".toString());
    }
    
    public final void d(final Bundle bundle) {
        if (!this.c) {
            this.c();
        }
        final Lifecycle lifecycle = this.a.getLifecycle();
        if (lifecycle.b().isAtLeast(Lifecycle.State.STARTED) ^ true) {
            this.b.f(bundle);
            return;
        }
        final StringBuilder sb = new StringBuilder();
        sb.append("performRestore cannot be called when owner is ");
        sb.append(lifecycle.b());
        throw new IllegalStateException(sb.toString().toString());
    }
    
    public final void e(final Bundle bundle) {
        fg0.e((Object)bundle, "outBundle");
        this.b.g(bundle);
    }
    
    public static final class a
    {
        public final zi1 a(final aj1 aj1) {
            fg0.e((Object)aj1, "owner");
            return new zi1(aj1, null);
        }
    }
}

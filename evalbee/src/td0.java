import android.os.Parcelable;
import android.os.Parcel;
import android.os.IBinder;
import android.os.Binder;
import android.os.Bundle;
import android.os.IInterface;

// 
// Decompiled by Procyon v0.6.0
// 

public interface td0 extends IInterface
{
    public static final String d0 = "android$support$customtabs$ICustomTabsService".replace('$', '.');
    
    boolean c(final sd0 p0);
    
    boolean d(final sd0 p0, final Bundle p1);
    
    boolean f(final long p0);
    
    public abstract static class a extends Binder implements td0
    {
        public static td0 p(final IBinder binder) {
            if (binder == null) {
                return null;
            }
            final IInterface queryLocalInterface = binder.queryLocalInterface(td0.d0);
            if (queryLocalInterface != null && queryLocalInterface instanceof td0) {
                return (td0)queryLocalInterface;
            }
            return new td0.a.a(binder);
        }
        
        public static class a implements td0
        {
            public IBinder a;
            
            public a(final IBinder a) {
                this.a = a;
            }
            
            public IBinder asBinder() {
                return this.a;
            }
            
            @Override
            public boolean c(final sd0 sd0) {
                final Parcel obtain = Parcel.obtain();
                final Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken(td0.d0);
                    obtain.writeStrongInterface((IInterface)sd0);
                    final IBinder a = this.a;
                    boolean b = false;
                    a.transact(3, obtain, obtain2, 0);
                    obtain2.readException();
                    if (obtain2.readInt() != 0) {
                        b = true;
                    }
                    return b;
                }
                finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }
            
            @Override
            public boolean d(final sd0 sd0, final Bundle bundle) {
                final Parcel obtain = Parcel.obtain();
                final Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken(td0.d0);
                    obtain.writeStrongInterface((IInterface)sd0);
                    boolean b = false;
                    b(obtain, (Parcelable)bundle, 0);
                    this.a.transact(10, obtain, obtain2, 0);
                    obtain2.readException();
                    if (obtain2.readInt() != 0) {
                        b = true;
                    }
                    return b;
                }
                finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }
            
            @Override
            public boolean f(final long n) {
                final Parcel obtain = Parcel.obtain();
                final Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken(td0.d0);
                    obtain.writeLong(n);
                    final IBinder a = this.a;
                    boolean b = false;
                    a.transact(2, obtain, obtain2, 0);
                    obtain2.readException();
                    if (obtain2.readInt() != 0) {
                        b = true;
                    }
                    return b;
                }
                finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }
        }
    }
    
    public abstract static class b
    {
        public static void b(final Parcel parcel, final Parcelable parcelable, final int n) {
            if (parcelable != null) {
                parcel.writeInt(1);
                parcelable.writeToParcel(parcel, n);
            }
            else {
                parcel.writeInt(0);
            }
        }
    }
}

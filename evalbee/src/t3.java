import com.ekodroid.omrevaluator.students.services.SyncClassListService;
import com.ekodroid.omrevaluator.database.ClassDataModel;
import com.ekodroid.omrevaluator.database.repositories.ClassRepository;
import android.view.View;
import androidx.appcompat.app.a;
import android.view.View$OnClickListener;
import android.widget.Button;
import com.google.android.material.textfield.TextInputLayout;
import android.view.ViewGroup;
import android.view.LayoutInflater;
import com.google.android.material.dialog.MaterialAlertDialogBuilder;
import android.content.Context;

// 
// Decompiled by Procyon v0.6.0
// 

public class t3
{
    public Context a;
    public y01 b;
    
    public t3(final Context a, final y01 b) {
        this.a = a;
        this.b = b;
        this.b();
    }
    
    public static /* synthetic */ y01 a(final t3 t3) {
        return t3.b;
    }
    
    public final void b() {
        final MaterialAlertDialogBuilder materialAlertDialogBuilder = new MaterialAlertDialogBuilder(this.a, 2131951953);
        final View inflate = ((LayoutInflater)this.a.getSystemService("layout_inflater")).inflate(2131492957, (ViewGroup)null);
        final TextInputLayout textInputLayout = (TextInputLayout)inflate.findViewById(2131297172);
        final Button button = (Button)inflate.findViewById(2131296410);
        final Button button2 = (Button)inflate.findViewById(2131296431);
        materialAlertDialogBuilder.setView(inflate);
        materialAlertDialogBuilder.setTitle(2131886115);
        final a create = materialAlertDialogBuilder.create();
        ((View)button).setOnClickListener((View$OnClickListener)new View$OnClickListener(this, textInputLayout, create) {
            public final TextInputLayout a;
            public final a b;
            public final t3 c;
            
            public void onClick(final View view) {
                if (this.a.getEditText().getText().toString().trim().equals("")) {
                    a91.G(this.c.a, 2131886262, 2131230909, 2131231086);
                    return;
                }
                ClassRepository.getInstance(this.c.a).saveOrUpdateClass(new ClassDataModel(this.a.getEditText().getText().toString(), null, false));
                SyncClassListService.c(this.c.a, false);
                t3.a(this.c).a(null);
                this.b.dismiss();
            }
        });
        ((View)button2).setOnClickListener((View$OnClickListener)new View$OnClickListener(this, create) {
            public final a a;
            public final t3 b;
            
            public void onClick(final View view) {
                this.a.dismiss();
            }
        });
        create.show();
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

public final class yd2
{
    public boolean a;
    
    public final yd2 a() {
        this.a = true;
        return this;
    }
    
    public final ce2 b() {
        if (this.a) {
            return new ce2(true, false, null);
        }
        throw new IllegalArgumentException("Pending purchases for one-time products must be supported.");
    }
}

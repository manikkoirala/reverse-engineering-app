// 
// Decompiled by Procyon v0.6.0
// 

public class xt1 implements t90
{
    @Override
    public double a(final double[] array, final int n) {
        final double pow = Math.pow(2.718281828459045, array[0] * 2.0);
        return (pow - 1.0) / (pow + 1.0);
    }
    
    @Override
    public boolean b(final int n) {
        boolean b = true;
        if (n != 1) {
            b = false;
        }
        return b;
    }
    
    @Override
    public String toString() {
        return "tanh(x)";
    }
}

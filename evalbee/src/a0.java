import java.util.NoSuchElementException;

// 
// Decompiled by Procyon v0.6.0
// 

public abstract class a0 extends w02
{
    public Object a;
    
    public a0(final Object a) {
        this.a = a;
    }
    
    public abstract Object b(final Object p0);
    
    @Override
    public final boolean hasNext() {
        return this.a != null;
    }
    
    @Override
    public final Object next() {
        final Object a = this.a;
        if (a != null) {
            this.a = this.b(a);
            return a;
        }
        throw new NoSuchElementException();
    }
}

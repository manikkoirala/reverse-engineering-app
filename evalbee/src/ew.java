import android.os.BaseBundle;
import android.text.TextUtils;
import android.text.SpannableStringBuilder;
import android.os.Bundle;
import android.os.Build$VERSION;
import android.view.inputmethod.EditorInfo;

// 
// Decompiled by Procyon v0.6.0
// 

public abstract class ew
{
    public static final String[] a;
    
    static {
        a = new String[0];
    }
    
    public static String[] a(final EditorInfo editorInfo) {
        if (Build$VERSION.SDK_INT >= 25) {
            String[] array = dw.a(editorInfo);
            if (array == null) {
                array = ew.a;
            }
            return array;
        }
        final Bundle extras = editorInfo.extras;
        if (extras == null) {
            return ew.a;
        }
        String[] array2;
        if ((array2 = ((BaseBundle)extras).getStringArray("androidx.core.view.inputmethod.EditorInfoCompat.CONTENT_MIME_TYPES")) == null) {
            array2 = ((BaseBundle)editorInfo.extras).getStringArray("android.support.v13.view.inputmethod.EditorInfoCompat.CONTENT_MIME_TYPES");
        }
        if (array2 == null) {
            array2 = ew.a;
        }
        return array2;
    }
    
    public static boolean b(final CharSequence charSequence, final int n, final int n2) {
        if (n2 != 0) {
            return n2 == 1 && Character.isHighSurrogate(charSequence.charAt(n));
        }
        return Character.isLowSurrogate(charSequence.charAt(n));
    }
    
    public static boolean c(int n) {
        n &= 0xFFF;
        return n == 129 || n == 225 || n == 18;
    }
    
    public static void d(final EditorInfo editorInfo, final String[] array) {
        if (Build$VERSION.SDK_INT >= 25) {
            cw.a(editorInfo, array);
        }
        else {
            if (editorInfo.extras == null) {
                editorInfo.extras = new Bundle();
            }
            ((BaseBundle)editorInfo.extras).putStringArray("androidx.core.view.inputmethod.EditorInfoCompat.CONTENT_MIME_TYPES", array);
            ((BaseBundle)editorInfo.extras).putStringArray("android.support.v13.view.inputmethod.EditorInfoCompat.CONTENT_MIME_TYPES", array);
        }
    }
    
    public static void e(final EditorInfo editorInfo, final CharSequence charSequence, final int n) {
        l71.g(charSequence);
        if (Build$VERSION.SDK_INT >= 30) {
            ew.a.a(editorInfo, charSequence, n);
            return;
        }
        final int initialSelStart = editorInfo.initialSelStart;
        final int initialSelEnd = editorInfo.initialSelEnd;
        int n2;
        if (initialSelStart > initialSelEnd) {
            n2 = initialSelEnd - n;
        }
        else {
            n2 = initialSelStart - n;
        }
        int n3;
        if (initialSelStart > initialSelEnd) {
            n3 = initialSelStart - n;
        }
        else {
            n3 = initialSelEnd - n;
        }
        final int length = charSequence.length();
        if (n < 0 || n2 < 0 || n3 > length) {
            g(editorInfo, null, 0, 0);
            return;
        }
        if (c(editorInfo.inputType)) {
            g(editorInfo, null, 0, 0);
            return;
        }
        if (length <= 2048) {
            g(editorInfo, charSequence, n2, n3);
            return;
        }
        h(editorInfo, charSequence, n2, n3);
    }
    
    public static void f(final EditorInfo editorInfo, final CharSequence charSequence) {
        if (Build$VERSION.SDK_INT >= 30) {
            ew.a.a(editorInfo, charSequence, 0);
        }
        else {
            e(editorInfo, charSequence, 0);
        }
    }
    
    public static void g(final EditorInfo editorInfo, final CharSequence charSequence, final int n, final int n2) {
        if (editorInfo.extras == null) {
            editorInfo.extras = new Bundle();
        }
        Object o;
        if (charSequence != null) {
            o = new SpannableStringBuilder(charSequence);
        }
        else {
            o = null;
        }
        editorInfo.extras.putCharSequence("androidx.core.view.inputmethod.EditorInfoCompat.CONTENT_SURROUNDING_TEXT", (CharSequence)o);
        ((BaseBundle)editorInfo.extras).putInt("androidx.core.view.inputmethod.EditorInfoCompat.CONTENT_SELECTION_HEAD", n);
        ((BaseBundle)editorInfo.extras).putInt("androidx.core.view.inputmethod.EditorInfoCompat.CONTENT_SELECTION_END", n2);
    }
    
    public static void h(final EditorInfo editorInfo, CharSequence charSequence, int n, final int n2) {
        final int n3 = n2 - n;
        int n4;
        if (n3 > 1024) {
            n4 = 0;
        }
        else {
            n4 = n3;
        }
        final int length = charSequence.length();
        final int n5 = 2048 - n4;
        final int min = Math.min(length - n2, n5 - Math.min(n, (int)(n5 * 0.8)));
        final int min2 = Math.min(n, n5 - min);
        final int n6 = n - min2;
        int n7 = min2;
        n = n6;
        if (b(charSequence, n6, 0)) {
            n = n6 + 1;
            n7 = min2 - 1;
        }
        int n8 = min;
        if (b(charSequence, n2 + min - 1, 1)) {
            n8 = min - 1;
        }
        if (n4 != n3) {
            charSequence = TextUtils.concat(new CharSequence[] { charSequence.subSequence(n, n + n7), charSequence.subSequence(n2, n8 + n2) });
        }
        else {
            charSequence = charSequence.subSequence(n, n7 + n4 + n8 + n);
        }
        n = n7 + 0;
        g(editorInfo, charSequence, n, n4 + n);
    }
    
    public abstract static class a
    {
        public static void a(final EditorInfo editorInfo, final CharSequence charSequence, final int n) {
            editorInfo.setInitialSurroundingSubText(charSequence, n);
        }
    }
}

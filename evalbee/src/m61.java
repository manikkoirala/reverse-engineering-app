import com.android.volley.Request;
import java.util.HashMap;
import java.util.Map;
import android.content.pm.PackageManager$NameNotFoundException;
import com.google.firebase.auth.FirebaseAuth;
import com.android.volley.VolleyError;
import com.ekodroid.omrevaluator.serializable.ResponseModel.SMSBuyLink;
import com.android.volley.d;
import android.content.Context;

// 
// Decompiled by Procyon v0.6.0
// 

public class m61
{
    public ee1 a;
    public y01 b;
    public Context c;
    public int d;
    public String e;
    public String f;
    
    public m61(final Context c, final ee1 a, final int d, final y01 b) {
        final StringBuilder sb = new StringBuilder();
        sb.append("https://");
        sb.append(a91.u());
        sb.append(":8759/sms/buyLink534");
        this.e = sb.toString();
        this.c = c;
        this.a = a;
        this.b = b;
        this.d = d;
        final StringBuilder sb2 = new StringBuilder();
        sb2.append("");
        sb2.append(System.currentTimeMillis());
        this.f = sb2.toString();
        this.f();
    }
    
    public static /* synthetic */ String a(final m61 m61) {
        return m61.f;
    }
    
    public static /* synthetic */ Context c(final m61 m61) {
        return m61.c;
    }
    
    public static /* synthetic */ int d(final m61 m61) {
        return m61.d;
    }
    
    public final void e(final Object o) {
        final y01 b = this.b;
        if (b != null) {
            b.a(o);
        }
    }
    
    public final void f() {
        final hr1 hr1 = new hr1(this, 1, this.e, new d.b(this) {
            public final m61 a;
            
            public void b(String g) {
                if (g != null) {
                    try {
                        g = b.g(g, m61.a(this.a));
                        this.a.e(new gc0().j(g, SMSBuyLink.class));
                    }
                    catch (final Exception ex) {
                        this.a.e(null);
                    }
                }
            }
        }, new d.a(this) {
            public final m61 a;
            
            @Override
            public void a(final VolleyError volleyError) {
                this.a.e(null);
            }
        }) {
            public final m61 w;
            
            @Override
            public byte[] k() {
                byte[] bytes = null;
                try {
                    bytes = (byte[])(Object)new SMSBuyLink(b.i(FirebaseAuth.getInstance().e().getEmail(), "emailKey"), m61.d(this.w), null, m61.c(this.w).getPackageManager().getPackageInfo(m61.c(this.w).getPackageName(), 0).versionCode);
                    bytes = b.i(new gc0().s(bytes), m61.a(this.w)).getBytes("UTF-8");
                    return bytes;
                }
                catch (final Exception bytes) {}
                catch (final PackageManager$NameNotFoundException ex) {}
                ((Throwable)(Object)bytes).printStackTrace();
                return null;
            }
            
            @Override
            public String l() {
                return "application/json";
            }
            
            @Override
            public Map o() {
                final HashMap hashMap = new HashMap();
                hashMap.put("Time", m61.a(this.w));
                return hashMap;
            }
        };
        hr1.L(new wq(30000, 0, 1.0f));
        this.a.a(hr1);
    }
}

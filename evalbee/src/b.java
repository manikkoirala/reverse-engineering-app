import android.util.Base64;
import java.security.spec.AlgorithmParameterSpec;
import javax.crypto.spec.IvParameterSpec;
import java.nio.charset.StandardCharsets;
import java.security.Key;
import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;

// 
// Decompiled by Procyon v0.6.0
// 

public abstract class b
{
    public static byte[] a(final byte[] array) {
        final int n = array.length + 1;
        final byte[] array2 = new byte[n];
        for (int i = 0; i < array.length; ++i) {
            array2[i] = array[i];
        }
        array2[n - 1] = k(array);
        return array2;
    }
    
    public static byte[] b(final byte[] key, final byte[] input) {
        final SecretKeySpec key2 = new SecretKeySpec(key, "AES");
        final Cipher instance = Cipher.getInstance("AES");
        instance.init(2, key2);
        return instance.doFinal(input);
    }
    
    public static byte[] c(final byte[] key, final byte[] input) {
        final IvParameterSpec params = new IvParameterSpec("fedcba9876543210".getBytes(StandardCharsets.UTF_8));
        final SecretKeySpec key2 = new SecretKeySpec(key, "AES");
        final Cipher instance = Cipher.getInstance("AES/CBC/NoPadding");
        instance.init(1, key2, params);
        return instance.doFinal(input);
    }
    
    public static byte[] d(final byte[] key, final byte[] input) {
        final SecretKeySpec key2 = new SecretKeySpec(key, "AES");
        final Cipher instance = Cipher.getInstance("AES");
        instance.init(1, key2);
        return instance.doFinal(input);
    }
    
    public static String e(final byte[] array) {
        return Base64.encodeToString(array, 2);
    }
    
    public static byte[] f(final byte[] array, final String s) {
        return b(j(s.getBytes(StandardCharsets.UTF_8)), array);
    }
    
    public static String g(final String s, final String s2) {
        if (!m(s)) {
            return "";
        }
        return new String(b(j(s2.getBytes(StandardCharsets.UTF_8)), n(Base64.decode(s, 2))), "UTF-8");
    }
    
    public static byte[] h(final byte[] array, final String s) {
        return d(j(s.getBytes(StandardCharsets.UTF_8)), array);
    }
    
    public static String i(final String s, final String s2) {
        return e(a(d(j(s2.getBytes(StandardCharsets.UTF_8)), s.getBytes(StandardCharsets.UTF_8))));
    }
    
    public static byte[] j(byte[] d) {
        final byte[] array = new byte[16];
        final int n = 0;
        for (int i = 0; i < 16; ++i) {
            array[i] = d[i % d.length];
        }
        d = d(array, array);
        final byte[] array2 = new byte[16];
        final byte[] array3 = new byte[16];
        for (int j = n; j < d.length / 2; ++j) {
            final int n2 = j * 2;
            array2[j] = d[n2];
            array3[j] = d[n2 + 1];
        }
        return c(array3, d(array2, array3));
    }
    
    public static byte k(final byte[] array) {
        int i = 0;
        byte b = 0;
        while (i < array.length) {
            b ^= array[i];
            ++i;
        }
        return b;
    }
    
    public static boolean l(final byte[] array) {
        boolean b = false;
        int i = 0;
        byte b2 = 0;
        while (i < array.length - 1) {
            b2 ^= array[i];
            ++i;
        }
        if (b2 == array[array.length - 1]) {
            b = true;
        }
        return b;
    }
    
    public static boolean m(final String s) {
        if (s.length() < 16) {
            return false;
        }
        try {
            return l(Base64.decode(s, 2));
        }
        catch (final Exception ex) {
            return false;
        }
    }
    
    public static byte[] n(final byte[] array) {
        final int n = array.length - 1;
        final byte[] array2 = new byte[n];
        for (int i = 0; i < n; ++i) {
            array2[i] = array[i];
        }
        return array2;
    }
}

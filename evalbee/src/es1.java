// 
// Decompiled by Procyon v0.6.0
// 

public class es1 implements t90
{
    @Override
    public double a(final double[] array, final int n) {
        double n2 = 0.0;
        for (int i = 0; i < n; ++i) {
            n2 += array[i];
        }
        return n2;
    }
    
    @Override
    public boolean b(final int n) {
        return n > 0;
    }
    
    @Override
    public String toString() {
        return "sum(x1, x2, ..., xn)";
    }
}

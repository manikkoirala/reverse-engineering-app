import com.google.android.gms.internal.firebase_auth_api.zzbx;
import java.io.UnsupportedEncodingException;
import com.google.android.gms.internal.firebase_auth_api.zzbo;
import com.google.android.gms.internal.firebase-auth-api.zzcd;
import android.util.Base64;
import java.io.OutputStream;
import com.google.android.gms.internal.firebase_auth_api.zzbi;
import java.io.ByteArrayOutputStream;
import com.google.android.gms.internal.firebase_auth_api.zzw;
import android.util.Log;
import java.security.GeneralSecurityException;
import java.io.IOException;
import com.google.android.gms.internal.firebase_auth_api.zzkt;
import com.google.android.gms.internal.firebase_auth_api.zzma$zza;
import com.google.android.gms.internal.firebase_auth_api.zzkm;
import android.content.Context;
import com.google.android.gms.internal.firebase-auth-api.zzma;

// 
// Decompiled by Procyon v0.6.0
// 

public final class oi2
{
    public static oi2 c;
    public final String a;
    public final zzma b;
    
    public oi2(Context b, final String a, final boolean b2) {
        this.a = a;
        Label_0114: {
            try {
                zzkm.zza();
                b = (IOException)((zzma$zza)new zzma$zza().zza((Context)b, "GenericIdpKeyset", String.format("com.google.firebase.auth.api.crypto.%s", a))).zza(zzkt.zza);
                ((zzma$zza)b).zza(String.format("android-keystore://firebear_master_key_id.%s", a));
                b = (IOException)((zzma$zza)b).zza();
                break Label_0114;
            }
            catch (final IOException b) {}
            catch (final GeneralSecurityException ex) {}
            final String message = b.getMessage();
            final StringBuilder sb = new StringBuilder("Exception encountered during crypto setup:\n");
            sb.append(message);
            Log.e("FirebearCryptoHelper", sb.toString());
            b = null;
        }
        this.b = (zzma)b;
    }
    
    public static oi2 c(final Context context, final String s) {
        final oi2 c = oi2.c;
        if (c == null || !zzw.zza((Object)c.a, (Object)s)) {
            oi2.c = new oi2(context, s, true);
        }
        return oi2.c;
    }
    
    public final String a() {
        if (this.b == null) {
            Log.e("FirebearCryptoHelper", "KeysetManager failed to initialize - unable to get Public key");
            return null;
        }
        final ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        final zzcd zza = zzbi.zza((OutputStream)byteArrayOutputStream);
        try {
            synchronized (this.b) {
                ((zzbx)((zzbx)((com.google.android.gms.internal.firebase_auth_api.zzma)this.b).zza()).zza()).zza(zza);
                monitorexit(this.b);
                return Base64.encodeToString(byteArrayOutputStream.toByteArray(), 8);
            }
        }
        catch (final IOException ex) {}
        catch (final GeneralSecurityException ex2) {}
        final IOException ex;
        final String message = ex.getMessage();
        final StringBuilder sb = new StringBuilder("Exception encountered when attempting to get Public Key:\n");
        sb.append(message);
        Log.e("FirebearCryptoHelper", sb.toString());
        return null;
    }
    
    public final String b(final String ex) {
        final zzma b = this.b;
        if (b == null) {
            Log.e("FirebearCryptoHelper", "KeysetManager failed to initialize - unable to decrypt payload");
            return null;
        }
        try {
            synchronized (b) {
                return new String(((zzbo)((zzbx)((com.google.android.gms.internal.firebase_auth_api.zzma)this.b).zza()).zza((Class)com.google.android.gms.internal.firebase-auth-api.zzbo.class)).zza(Base64.decode((String)ex, 8), (byte[])null), "UTF-8");
            }
        }
        catch (final UnsupportedEncodingException ex) {}
        catch (final GeneralSecurityException ex2) {}
        final String message = ex.getMessage();
        final StringBuilder sb = new StringBuilder("Exception encountered while decrypting bytes:\n");
        sb.append(message);
        Log.e("FirebearCryptoHelper", sb.toString());
        return null;
    }
}

import android.graphics.drawable.Drawable;
import android.content.res.Resources;
import android.content.Context;
import java.lang.ref.WeakReference;

// 
// Decompiled by Procyon v0.6.0
// 

public class sw1 extends qe1
{
    public final WeakReference b;
    
    public sw1(final Context referent, final Resources resources) {
        super(resources);
        this.b = new WeakReference((T)referent);
    }
    
    public Drawable getDrawable(final int n) {
        final Drawable a = this.a(n);
        final Context context = (Context)this.b.get();
        if (a != null && context != null) {
            je1.g().w(context, n, a);
        }
        return a;
    }
}

import android.os.Parcelable;
import com.google.android.gms.common.internal.safeparcel.SafeParcelWriter;
import android.os.Parcel;
import android.text.TextUtils;
import com.google.android.gms.common.internal.Preconditions;
import com.google.android.gms.internal.firebase_auth_api.zzag;
import com.google.android.gms.internal.firebase-auth-api.zzagt;
import android.os.Parcelable$Creator;

// 
// Decompiled by Procyon v0.6.0
// 

public class lf2 extends r01
{
    public static final Parcelable$Creator<lf2> CREATOR;
    public final String a;
    public final String b;
    public final String c;
    public final zzagt d;
    public final String e;
    public final String f;
    public final String g;
    
    static {
        CREATOR = (Parcelable$Creator)new gf2();
    }
    
    public lf2(final String s, final String b, final String c, final zzagt d, final String e, final String f, final String g) {
        this.a = zzag.zzb(s);
        this.b = b;
        this.c = c;
        this.d = d;
        this.e = e;
        this.f = f;
        this.g = g;
    }
    
    public static zzagt K(final lf2 lf2, final String s) {
        Preconditions.checkNotNull(lf2);
        final zzagt d = lf2.d;
        if (d != null) {
            return d;
        }
        return (zzagt)new com.google.android.gms.internal.firebase_auth_api.zzagt(lf2.getIdToken(), lf2.getAccessToken(), lf2.i(), (String)null, lf2.J(), (String)null, s, lf2.e, lf2.g);
    }
    
    public static lf2 O(final zzagt zzagt) {
        Preconditions.checkNotNull(zzagt, "Must specify a non-null webSignInCredential");
        return new lf2(null, null, null, zzagt, null, null, null);
    }
    
    public static lf2 R(final String s, final String s2, final String s3, final String s4, final String s5) {
        Preconditions.checkNotEmpty(s, "Must specify a non-empty providerId");
        if (TextUtils.isEmpty((CharSequence)s2) && TextUtils.isEmpty((CharSequence)s3)) {
            throw new IllegalArgumentException("Must specify an idToken or an accessToken.");
        }
        return new lf2(s, s2, s3, null, s4, s5, null);
    }
    
    @Override
    public String E() {
        return this.a;
    }
    
    @Override
    public final v9 H() {
        return new lf2(this.a, this.b, this.c, this.d, this.e, this.f, this.g);
    }
    
    @Override
    public String J() {
        return this.f;
    }
    
    @Override
    public String getAccessToken() {
        return this.c;
    }
    
    @Override
    public String getIdToken() {
        return this.b;
    }
    
    @Override
    public String i() {
        return this.a;
    }
    
    public void writeToParcel(final Parcel parcel, final int n) {
        final int beginObjectHeader = SafeParcelWriter.beginObjectHeader(parcel);
        SafeParcelWriter.writeString(parcel, 1, this.i(), false);
        SafeParcelWriter.writeString(parcel, 2, this.getIdToken(), false);
        SafeParcelWriter.writeString(parcel, 3, this.getAccessToken(), false);
        SafeParcelWriter.writeParcelable(parcel, 4, (Parcelable)this.d, n, false);
        SafeParcelWriter.writeString(parcel, 5, this.e, false);
        SafeParcelWriter.writeString(parcel, 6, this.J(), false);
        SafeParcelWriter.writeString(parcel, 7, this.g, false);
        SafeParcelWriter.finishObjectHeader(parcel, beginObjectHeader);
    }
}

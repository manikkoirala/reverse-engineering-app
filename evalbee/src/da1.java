// 
// Decompiled by Procyon v0.6.0
// 

public final class da1
{
    public final Class a;
    public final Class b;
    
    public da1(final Class a, final Class b) {
        this.a = a;
        this.b = b;
    }
    
    public static da1 a(final Class clazz, final Class clazz2) {
        return new da1(clazz, clazz2);
    }
    
    public static da1 b(final Class clazz) {
        return new da1(a.class, clazz);
    }
    
    @Override
    public boolean equals(final Object o) {
        if (this == o) {
            return true;
        }
        if (o != null && da1.class == o.getClass()) {
            final da1 da1 = (da1)o;
            return this.b.equals(da1.b) && this.a.equals(da1.a);
        }
        return false;
    }
    
    @Override
    public int hashCode() {
        return this.b.hashCode() * 31 + this.a.hashCode();
    }
    
    @Override
    public String toString() {
        if (this.a == a.class) {
            return this.b.getName();
        }
        final StringBuilder sb = new StringBuilder();
        sb.append("@");
        sb.append(this.a.getName());
        sb.append(" ");
        sb.append(this.b.getName());
        return sb.toString();
    }
    
    public @interface a {
    }
}

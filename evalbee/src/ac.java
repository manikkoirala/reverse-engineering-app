// 
// Decompiled by Procyon v0.6.0
// 

public abstract class ac
{
    public static double[][] a(final double[][] array, final int n) {
        if (n == array.length) {
            final double[][] array2 = new double[n * 2][];
            for (int i = 0; i < n; ++i) {
                array2[i] = array[i];
            }
            return array2;
        }
        return array;
    }
    
    public static void b(final z21 z21, double n, double n2, final px0 px0) {
        if (n > n2) {
            throw new IllegalArgumentException("t_min <= t_max required.");
        }
        final int c = px0.c();
        double[][] array = new double[10][];
        final int n3 = c + 1;
        double[] array2 = new double[n3];
        array2[c] = n;
        z21.c(array2);
        final double[] array3 = new double[n3];
        array3[c] = n2;
        z21.c(array3);
        array[0] = array3;
        final int d = z21.d();
        final double[][] array4 = new double[d][];
        final double n4 = px0.d() * px0.d();
        final double[] array5 = new double[n3];
        final double n5 = n;
        n = n2;
        int n6 = 1;
        n2 = n5;
        while (true) {
            double n7 = (n2 + n) / 2.0;
            final double[] array6 = new double[n3];
            array6[c] = n7;
            z21.c(array6);
            final double a = ja0.a(array2, array[n6 - 1], array6, array5, c);
            if (Double.isNaN(a) || Double.isInfinite(a)) {
                final StringBuilder sb = new StringBuilder();
                sb.append("NaN or infinity resulted from calling the eval method of the ");
                sb.append(z21.getClass().getName());
                sb.append(" class.");
                throw new RuntimeException(sb.toString());
            }
            boolean b = false;
            Label_0343: {
                if (a < n4) {
                    double n8;
                    int i;
                    double[] array7;
                    for (n8 = 0.0, i = 0; i < d; ++i, n7 = n8) {
                        n8 = (n2 + n7) / 2.0;
                        array7 = new double[n3];
                        (array4[i] = array7)[c] = n8;
                        z21.c(array7);
                        if (ja0.a(array2, array6, array7, array5, c) >= n4) {
                            break;
                        }
                    }
                    if (i == d) {
                        b = true;
                        break Label_0343;
                    }
                    array = a(array, n6);
                    array[n6] = array6;
                    ++n6;
                    for (int j = 0; j <= i; ++j, ++n6) {
                        array = a(array, n6);
                        array[n6] = array4[j];
                    }
                    n = n8;
                }
                b = false;
            }
            if (b) {
                px0.e(array2);
                px0.e(array6);
                --n6;
                array2 = array[n6];
                if (n6 == 0) {
                    px0.e(array2);
                    return;
                }
                final double n9 = array[n6 - 1][c];
                n2 = n;
                n = n9;
            }
            else {
                double[][] a2 = array;
                int n10 = n6;
                double n11 = n;
                if (n > n7) {
                    a2 = a(array, n6);
                    a2[n6] = array6;
                    n10 = n6 + 1;
                    n11 = n7;
                }
                array = a2;
                n6 = n10;
                n = n11;
            }
        }
    }
}

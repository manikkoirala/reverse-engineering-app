import com.google.firebase.FirebaseException;
import com.google.android.gms.common.logging.Logger;

// 
// Decompiled by Procyon v0.6.0
// 

public abstract class g51
{
    private static final Logger zza;
    
    static {
        zza = new Logger("PhoneAuthProvider", new String[0]);
    }
    
    public abstract void onCodeAutoRetrievalTimeOut(final String p0);
    
    public abstract void onCodeSent(final String p0, final f51 p1);
    
    public abstract void onVerificationCompleted(final e51 p0);
    
    public abstract void onVerificationFailed(final FirebaseException p0);
}

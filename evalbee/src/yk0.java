import android.os.BaseBundle;
import kotlin.time.DurationUnit;
import android.content.Context;
import android.os.Bundle;

// 
// Decompiled by Procyon v0.6.0
// 

public final class yk0 implements ym1
{
    public static final a b;
    public final Bundle a;
    
    static {
        b = new a(null);
    }
    
    public yk0(final Context context) {
        fg0.e((Object)context, "context");
        Bundle a;
        if ((a = context.getPackageManager().getApplicationInfo(context.getPackageName(), 128).metaData) == null) {
            a = Bundle.EMPTY;
        }
        this.a = a;
    }
    
    @Override
    public Object a(final vl vl) {
        return ym1.a.a(this, vl);
    }
    
    @Override
    public Double b() {
        Double value;
        if (((BaseBundle)this.a).containsKey("firebase_sessions_sampling_rate")) {
            value = ((BaseBundle)this.a).getDouble("firebase_sessions_sampling_rate");
        }
        else {
            value = null;
        }
        return value;
    }
    
    @Override
    public Boolean c() {
        Boolean value;
        if (((BaseBundle)this.a).containsKey("firebase_sessions_enabled")) {
            value = ((BaseBundle)this.a).getBoolean("firebase_sessions_enabled");
        }
        else {
            value = null;
        }
        return value;
    }
    
    @Override
    public lv d() {
        lv d;
        if (((BaseBundle)this.a).containsKey("firebase_sessions_sessions_restart_timeout")) {
            d = lv.d(nv.h(((BaseBundle)this.a).getInt("firebase_sessions_sessions_restart_timeout"), DurationUnit.SECONDS));
        }
        else {
            d = null;
        }
        return d;
    }
    
    public static final class a
    {
    }
}

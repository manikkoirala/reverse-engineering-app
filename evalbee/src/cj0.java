import java.util.Map;
import java.util.Iterator;

// 
// Decompiled by Procyon v0.6.0
// 

public class cj0 implements Iterator
{
    public Iterator a;
    
    public cj0(final Iterator a) {
        this.a = a;
    }
    
    public Map.Entry b() {
        final Map.Entry entry = (Map.Entry)this.a.next();
        entry.getValue();
        return entry;
    }
    
    @Override
    public boolean hasNext() {
        return this.a.hasNext();
    }
    
    @Override
    public void remove() {
        this.a.remove();
    }
}

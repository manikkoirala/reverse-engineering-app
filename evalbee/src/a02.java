import android.graphics.Canvas;
import android.graphics.Paint$Style;
import androidx.emoji2.text.c;
import android.text.TextPaint;
import android.graphics.Paint;

// 
// Decompiled by Procyon v0.6.0
// 

public final class a02 extends ow
{
    public static Paint f;
    
    public a02(final nw nw) {
        super(nw);
    }
    
    public static Paint c() {
        if (a02.f == null) {
            (a02.f = (Paint)new TextPaint()).setColor(c.b().c());
            a02.f.setStyle(Paint$Style.FILL);
        }
        return a02.f;
    }
    
    public void draw(final Canvas canvas, final CharSequence charSequence, final int n, final int n2, final float n3, final int n4, final int n5, final int n6, final Paint paint) {
        if (androidx.emoji2.text.c.b().i()) {
            canvas.drawRect(n3, (float)n4, n3 + this.b(), (float)n6, c());
        }
        this.a().a(canvas, n3, (float)n5, paint);
    }
}

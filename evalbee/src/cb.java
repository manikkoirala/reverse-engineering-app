import java.util.concurrent.Semaphore;
import java.util.concurrent.Executor;

// 
// Decompiled by Procyon v0.6.0
// 

public class cb implements Executor
{
    public Semaphore a;
    public int b;
    
    public cb() {
        this.a = new Semaphore(0);
        this.b = 0;
    }
    
    public void b() {
        try {
            this.a.acquire(this.b);
            this.b = 0;
        }
        catch (final InterruptedException ex) {
            Thread.currentThread().interrupt();
            g9.a("Interrupted while waiting for background task", ex);
        }
    }
    
    @Override
    public void execute(final Runnable runnable) {
        ++this.b;
        wy.c.execute(new bb(this, runnable));
    }
}

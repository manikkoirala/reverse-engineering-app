import java.util.ArrayDeque;
import java.util.concurrent.Executor;

// 
// Decompiled by Procyon v0.6.0
// 

public final class ny1 implements Executor
{
    public final Executor a;
    public final ArrayDeque b;
    public Runnable c;
    public final Object d;
    
    public ny1(final Executor a) {
        fg0.e((Object)a, "executor");
        this.a = a;
        this.b = new ArrayDeque();
        this.d = new Object();
    }
    
    public static final void b(final Runnable runnable, final ny1 ny1) {
        fg0.e((Object)runnable, "$command");
        fg0.e((Object)ny1, "this$0");
        try {
            runnable.run();
        }
        finally {
            ny1.c();
        }
    }
    
    public final void c() {
        synchronized (this.d) {
            final Runnable poll = this.b.poll();
            final Runnable c = poll;
            this.c = c;
            if (poll != null) {
                this.a.execute(c);
            }
            final u02 a = u02.a;
        }
    }
    
    @Override
    public void execute(final Runnable runnable) {
        fg0.e((Object)runnable, "command");
        synchronized (this.d) {
            this.b.offer(new my1(runnable, this));
            if (this.c == null) {
                this.c();
            }
            final u02 a = u02.a;
        }
    }
}

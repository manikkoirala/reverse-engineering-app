import android.os.LocaleList;
import java.util.Locale;

// 
// Decompiled by Procyon v0.6.0
// 

public final class nl0
{
    public static final nl0 b;
    public final ol0 a;
    
    static {
        b = a(new Locale[0]);
    }
    
    public nl0(final ol0 a) {
        this.a = a;
    }
    
    public static nl0 a(final Locale... array) {
        return h(nl0.b.a(array));
    }
    
    public static nl0 b(final String s) {
        if (s != null && !s.isEmpty()) {
            final String[] split = s.split(",", -1);
            final int length = split.length;
            final Locale[] array = new Locale[length];
            for (int i = 0; i < length; ++i) {
                array[i] = a.a(split[i]);
            }
            return a(array);
        }
        return d();
    }
    
    public static nl0 d() {
        return nl0.b;
    }
    
    public static nl0 h(final LocaleList list) {
        return new nl0(new pl0(list));
    }
    
    public Locale c(final int n) {
        return this.a.get(n);
    }
    
    public boolean e() {
        return this.a.isEmpty();
    }
    
    @Override
    public boolean equals(final Object o) {
        return o instanceof nl0 && this.a.equals(((nl0)o).a);
    }
    
    public int f() {
        return this.a.size();
    }
    
    public String g() {
        return this.a.a();
    }
    
    @Override
    public int hashCode() {
        return this.a.hashCode();
    }
    
    @Override
    public String toString() {
        return this.a.toString();
    }
    
    public abstract static class a
    {
        public static final Locale[] a;
        
        static {
            a = new Locale[] { new Locale("en", "XA"), new Locale("ar", "XB") };
        }
        
        public static Locale a(final String languageTag) {
            return Locale.forLanguageTag(languageTag);
        }
        
        public static boolean b(final Locale obj) {
            final Locale[] a = nl0.a.a;
            for (int length = a.length, i = 0; i < length; ++i) {
                if (a[i].equals(obj)) {
                    return true;
                }
            }
            return false;
        }
        
        public static boolean c(final Locale locale, final Locale obj) {
            final boolean equals = locale.equals(obj);
            final boolean b = true;
            if (equals) {
                return true;
            }
            if (!locale.getLanguage().equals(obj.getLanguage())) {
                return false;
            }
            if (b(locale) || b(obj)) {
                return false;
            }
            final String a = rd0.a(locale);
            if (a.isEmpty()) {
                final String country = locale.getCountry();
                boolean b2 = b;
                if (!country.isEmpty()) {
                    b2 = (country.equals(obj.getCountry()) && b);
                }
                return b2;
            }
            return a.equals(rd0.a(obj));
        }
    }
    
    public abstract static class b
    {
        public static LocaleList a(final Locale... array) {
            return new LocaleList(array);
        }
        
        public static LocaleList b() {
            return LocaleList.getAdjustedDefault();
        }
        
        public static LocaleList c() {
            return LocaleList.getDefault();
        }
    }
}

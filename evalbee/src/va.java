// 
// Decompiled by Procyon v0.6.0
// 

public class va implements t90
{
    @Override
    public double a(final double[] array, final int n) {
        double n2 = 0.0;
        for (int i = 0; i < n; ++i) {
            n2 += array[i];
        }
        return n2 / n;
    }
    
    @Override
    public boolean b(final int n) {
        return n > 0;
    }
    
    @Override
    public String toString() {
        return "avg(x1, x2, ..., xn)";
    }
}

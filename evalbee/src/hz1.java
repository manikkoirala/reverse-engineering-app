import java.io.IOException;
import com.google.gson.JsonIOException;
import com.google.gson.stream.JsonToken;

// 
// Decompiled by Procyon v0.6.0
// 

public abstract class hz1
{
    public final hz1 a() {
        return new hz1(this) {
            public final hz1 a;
            
            @Override
            public Object b(final rh0 rh0) {
                if (rh0.o0() == JsonToken.NULL) {
                    rh0.R();
                    return null;
                }
                return this.a.b(rh0);
            }
            
            @Override
            public void d(final vh0 vh0, final Object o) {
                if (o == null) {
                    vh0.u();
                }
                else {
                    this.a.d(vh0, o);
                }
            }
        };
    }
    
    public abstract Object b(final rh0 p0);
    
    public final nh0 c(final Object o) {
        try {
            final th0 th0 = new th0();
            this.d(th0, o);
            return th0.u0();
        }
        catch (final IOException ex) {
            throw new JsonIOException(ex);
        }
    }
    
    public abstract void d(final vh0 p0, final Object p1);
}

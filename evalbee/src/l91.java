import androidx.datastore.preferences.protobuf.r;
import androidx.datastore.preferences.protobuf.f0;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

// 
// Decompiled by Procyon v0.6.0
// 

public final class l91
{
    public static final l91 c;
    public final pj1 a;
    public final ConcurrentMap b;
    
    static {
        c = new l91();
    }
    
    public l91() {
        this.b = new ConcurrentHashMap();
        this.a = new tm0();
    }
    
    public static l91 a() {
        return l91.c;
    }
    
    public f0 b(final Class clazz, final f0 f0) {
        r.b(clazz, "messageType");
        r.b(f0, "schema");
        return this.b.putIfAbsent(clazz, f0);
    }
    
    public f0 c(final Class clazz) {
        r.b(clazz, "messageType");
        f0 a;
        if ((a = (f0)this.b.get(clazz)) == null) {
            a = this.a.a(clazz);
            final f0 b = this.b(clazz, a);
            if (b != null) {
                a = b;
            }
        }
        return a;
    }
    
    public f0 d(final Object o) {
        return this.c(o.getClass());
    }
}

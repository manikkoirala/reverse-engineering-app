import java.util.concurrent.Callable;
import java.util.concurrent.atomic.AtomicReference;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicMarkableReference;

// 
// Decompiled by Procyon v0.6.0
// 

public class f22
{
    public final bw0 a;
    public final xm b;
    public String c;
    public final a d;
    public final a e;
    public final of1 f;
    public final AtomicMarkableReference g;
    
    public f22(final String c, final z00 z00, final xm b) {
        this.d = new a(false);
        this.e = new a(true);
        this.f = new of1(128);
        this.g = new AtomicMarkableReference(null, false);
        this.c = c;
        this.a = new bw0(z00);
        this.b = b;
    }
    
    public static /* synthetic */ xm a(final f22 f22) {
        return f22.b;
    }
    
    public static /* synthetic */ String b(final f22 f22) {
        return f22.c;
    }
    
    public static /* synthetic */ bw0 c(final f22 f22) {
        return f22.a;
    }
    
    public static f22 h(final String s, final z00 z00, final xm xm) {
        final bw0 bw0 = new bw0(z00);
        final f22 f22 = new f22(s, z00, xm);
        ((oi0)f22.d.a.getReference()).e(bw0.i(s, false));
        ((oi0)f22.e.a.getReference()).e(bw0.i(s, true));
        f22.g.set(bw0.k(s), false);
        f22.f.c(bw0.j(s));
        return f22;
    }
    
    public static String i(final String s, final z00 z00) {
        return new bw0(z00).k(s);
    }
    
    public Map d() {
        return this.d.b();
    }
    
    public Map e() {
        return this.e.b();
    }
    
    public List f() {
        return this.f.a();
    }
    
    public String g() {
        return this.g.getReference();
    }
    
    public boolean j(final String s, final String s2) {
        return this.e.f(s, s2);
    }
    
    public void k(final String c) {
        synchronized (this.c) {
            this.c = c;
            final Map b = this.d.b();
            final List b2 = this.f.b();
            if (this.g() != null) {
                this.a.s(c, this.g());
            }
            if (!b.isEmpty()) {
                this.a.p(c, b);
            }
            if (!b2.isEmpty()) {
                this.a.r(c, b2);
            }
        }
    }
    
    public class a
    {
        public final AtomicMarkableReference a;
        public final AtomicReference b;
        public final boolean c;
        public final f22 d;
        
        public a(final f22 d, final boolean c) {
            this.d = d;
            this.b = new AtomicReference(null);
            this.c = c;
            int n;
            if (c) {
                n = 8192;
            }
            else {
                n = 1024;
            }
            this.a = new AtomicMarkableReference(new oi0(64, n), false);
        }
        
        public Map b() {
            return this.a.getReference().a();
        }
        
        public final void d() {
            final e22 e22 = new e22(this);
            if (ja2.a(this.b, null, e22)) {
                f22.a(this.d).g(e22);
            }
        }
        
        public final void e() {
            synchronized (this) {
                Map a;
                if (this.a.isMarked()) {
                    a = this.a.getReference().a();
                    final AtomicMarkableReference a2 = this.a;
                    a2.set(a2.getReference(), false);
                }
                else {
                    a = null;
                }
                monitorexit(this);
                if (a != null) {
                    f22.c(this.d).q(f22.b(this.d), a, this.c);
                }
            }
        }
        
        public boolean f(final String s, final String s2) {
            synchronized (this) {
                if (!this.a.getReference().d(s, s2)) {
                    return false;
                }
                final AtomicMarkableReference a = this.a;
                a.set(a.getReference(), true);
                monitorexit(this);
                this.d();
                return true;
            }
        }
    }
}

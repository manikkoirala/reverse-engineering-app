import android.content.SharedPreferences;
import android.content.Context;
import androidx.work.impl.WorkDatabase;

// 
// Decompiled by Procyon v0.6.0
// 

public abstract class ce0
{
    public static final void c(final Context context, final ss1 ss1) {
        fg0.e((Object)context, "context");
        fg0.e((Object)ss1, "sqLiteDatabase");
        final SharedPreferences sharedPreferences = context.getSharedPreferences("androidx.work.util.id", 0);
        if (!sharedPreferences.contains("next_job_scheduler_id") && !sharedPreferences.contains("next_job_scheduler_id")) {
            return;
        }
        final int int1 = sharedPreferences.getInt("next_job_scheduler_id", 0);
        final int int2 = sharedPreferences.getInt("next_alarm_manager_id", 0);
        ss1.m();
        try {
            ss1.T("INSERT OR REPLACE INTO `Preference` (`key`, `long_value`) VALUES (@key, @long_value)", new Object[] { "next_job_scheduler_id", int1 });
            ss1.T("INSERT OR REPLACE INTO `Preference` (`key`, `long_value`) VALUES (@key, @long_value)", new Object[] { "next_alarm_manager_id", int2 });
            sharedPreferences.edit().clear().apply();
            ss1.S();
        }
        finally {
            ss1.X();
        }
    }
    
    public static final int d(final WorkDatabase workDatabase, final String s) {
        final Long b = workDatabase.G().b(s);
        int n = 0;
        int n2;
        if (b != null) {
            n2 = (int)(long)b;
        }
        else {
            n2 = 0;
        }
        if (n2 != Integer.MAX_VALUE) {
            n = n2 + 1;
        }
        e(workDatabase, s, n);
        return n2;
    }
    
    public static final void e(final WorkDatabase workDatabase, final String s, final int n) {
        workDatabase.G().a(new n71(s, (long)n));
    }
}

import java.util.Iterator;
import java.util.concurrent.CopyOnWriteArraySet;
import android.content.Context;
import java.util.Set;

// 
// Decompiled by Procyon v0.6.0
// 

public final class ql
{
    public final Set a;
    public volatile Context b;
    
    public ql() {
        this.a = new CopyOnWriteArraySet();
    }
    
    public final void a(final l11 l11) {
        fg0.e((Object)l11, "listener");
        final Context b = this.b;
        if (b != null) {
            l11.a(b);
        }
        this.a.add(l11);
    }
    
    public final void b() {
        this.b = null;
    }
    
    public final void c(final Context b) {
        fg0.e((Object)b, "context");
        this.b = b;
        final Iterator iterator = this.a.iterator();
        while (iterator.hasNext()) {
            ((l11)iterator.next()).a(b);
        }
    }
    
    public final Context d() {
        return this.b;
    }
    
    public final void e(final l11 l11) {
        fg0.e((Object)l11, "listener");
        this.a.remove(l11);
    }
}

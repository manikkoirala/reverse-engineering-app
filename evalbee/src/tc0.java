import android.content.DialogInterface;
import android.content.DialogInterface$OnClickListener;
import android.view.View$OnClickListener;
import android.view.ViewGroup;
import android.view.LayoutInflater;
import com.google.android.material.dialog.MaterialAlertDialogBuilder;
import java.util.Iterator;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView$OnItemSelectedListener;
import android.widget.SpinnerAdapter;
import com.ekodroid.omrevaluator.database.TemplateHeaderJsonDataModel;
import com.ekodroid.omrevaluator.database.repositories.TemplateRepository;
import java.util.ArrayList;
import java.io.Serializable;
import android.content.Intent;
import com.ekodroid.omrevaluator.templateui.createtemplate.CustomHeaderActivity;
import androidx.appcompat.app.a;
import com.ekodroid.omrevaluator.templateui.models.HeaderProfile;
import android.widget.Spinner;
import android.content.Context;

// 
// Decompiled by Procyon v0.6.0
// 

public class tc0
{
    public Context a;
    public y01 b;
    public Spinner c;
    public HeaderProfile d;
    public a e;
    
    public tc0(final Context a, final y01 b) {
        this.a = a;
        this.b = b;
        this.f();
    }
    
    public static /* synthetic */ HeaderProfile a(final tc0 tc0) {
        return tc0.d;
    }
    
    public static /* synthetic */ HeaderProfile b(final tc0 tc0, final HeaderProfile d) {
        return tc0.d = d;
    }
    
    public final void d(final HeaderProfile headerProfile) {
        this.e.dismiss();
        final Intent intent = new Intent(this.a, (Class)CustomHeaderActivity.class);
        intent.putExtra("HEADER_PROFILE", (Serializable)headerProfile);
        this.a.startActivity(intent);
    }
    
    public final void e() {
        final ArrayList list = new ArrayList();
        list.add(ok.a);
        list.add("Default Medium");
        list.add(ok.b);
        list.add("Blank");
        list.add("None");
        final Iterator<TemplateHeaderJsonDataModel> iterator = TemplateRepository.getInstance(this.a).getAllHeaderProfileJson().iterator();
        while (iterator.hasNext()) {
            list.add(iterator.next().getHeaderName());
        }
        this.c.setAdapter((SpinnerAdapter)new p3(this.a, list));
        ((AdapterView)this.c).setOnItemSelectedListener((AdapterView$OnItemSelectedListener)new AdapterView$OnItemSelectedListener(this, list) {
            public final ArrayList a;
            public final tc0 b;
            
            public void onItemSelected(final AdapterView adapterView, final View view, final int index, final long n) {
                HeaderProfile headerProfile = null;
                if (index == 2) {
                    this.b.d(null);
                    return;
                }
                tc0 tc0;
                if (index > 4) {
                    tc0 = this.b;
                    headerProfile = TemplateRepository.getInstance(tc0.a).getHeaderProfileJsonModel(this.a.get(index)).getHeaderProfile();
                }
                else if (index == 0) {
                    tc0 = this.b;
                    headerProfile = HeaderProfile.getDefaultHeaderprofile();
                }
                else if (index == 1) {
                    tc0 = this.b;
                    headerProfile = HeaderProfile.getDefaultMediumHeaderprofile();
                }
                else if (index == 3) {
                    tc0 = this.b;
                    headerProfile = HeaderProfile.getBlankHeaderprofile();
                }
                else {
                    tc0 = this.b;
                }
                tc0.b(tc0, headerProfile);
            }
            
            public void onNothingSelected(final AdapterView adapterView) {
            }
        });
        ((AdapterView)this.c).setSelection(0);
    }
    
    public final void f() {
        final MaterialAlertDialogBuilder materialAlertDialogBuilder = new MaterialAlertDialogBuilder(this.a, 2131951953);
        final View inflate = ((LayoutInflater)this.a.getSystemService("layout_inflater")).inflate(2131492995, (ViewGroup)null);
        materialAlertDialogBuilder.setView(inflate);
        materialAlertDialogBuilder.setTitle(2131886814);
        this.c = (Spinner)inflate.findViewById(2131297094);
        this.e();
        inflate.findViewById(2131296661).setOnClickListener((View$OnClickListener)new View$OnClickListener(this) {
            public final tc0 a;
            
            public void onClick(final View view) {
                final tc0 a = this.a;
                a.d(tc0.a(a));
            }
        });
        materialAlertDialogBuilder.setPositiveButton(2131886759, (DialogInterface$OnClickListener)new DialogInterface$OnClickListener(this) {
            public final tc0 a;
            
            public void onClick(final DialogInterface dialogInterface, final int n) {
                final tc0 a = this.a;
                a.b.a(tc0.a(a));
                dialogInterface.dismiss();
            }
        });
        materialAlertDialogBuilder.setNegativeButton(2131886163, (DialogInterface$OnClickListener)new DialogInterface$OnClickListener(this) {
            public final tc0 a;
            
            public void onClick(final DialogInterface dialogInterface, final int n) {
                dialogInterface.dismiss();
            }
        });
        (this.e = materialAlertDialogBuilder.create()).show();
    }
}

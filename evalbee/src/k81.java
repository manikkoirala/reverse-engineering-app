import java.util.concurrent.locks.ReentrantLock;
import java.io.IOException;
import android.util.Log;
import java.io.FileOutputStream;
import java.util.HashMap;
import java.nio.channels.FileChannel;
import java.util.concurrent.locks.Lock;
import java.io.File;
import java.util.Map;

// 
// Decompiled by Procyon v0.6.0
// 

public final class k81
{
    public static final a e;
    public static final Map f;
    public final boolean a;
    public final File b;
    public final Lock c;
    public FileChannel d;
    
    static {
        e = new a(null);
        f = new HashMap();
    }
    
    public k81(final String str, File file, final boolean a) {
        fg0.e((Object)str, "name");
        fg0.e((Object)file, "lockDir");
        this.a = a;
        final StringBuilder sb = new StringBuilder();
        sb.append(str);
        sb.append(".lck");
        file = new File(file, sb.toString());
        this.b = file;
        final a e = k81.e;
        final String absolutePath = file.getAbsolutePath();
        fg0.d((Object)absolutePath, "lockFile.absolutePath");
        this.c = e.b(absolutePath);
    }
    
    public static final /* synthetic */ Map a() {
        return k81.f;
    }
    
    public final void b(final boolean b) {
        this.c.lock();
        if (b) {
            try {
                final File parentFile = this.b.getParentFile();
                if (parentFile != null) {
                    parentFile.mkdirs();
                }
                final FileChannel channel = new FileOutputStream(this.b).getChannel();
                channel.lock();
                this.d = channel;
            }
            catch (final IOException ex) {
                this.d = null;
                Log.w("SupportSQLiteLock", "Unable to grab file lock.", (Throwable)ex);
            }
        }
    }
    
    public final void d() {
        while (true) {
            try {
                try (final FileChannel d = this.d) {}
                this.c.unlock();
            }
            catch (final IOException ex) {
                continue;
            }
            break;
        }
    }
    
    public static final class a
    {
        public final Lock b(final String s) {
            synchronized (k81.a()) {
                final Map a = k81.a();
                Object value;
                if ((value = a.get(s)) == null) {
                    value = new ReentrantLock();
                    a.put(s, value);
                }
                return (Lock)value;
            }
        }
    }
}

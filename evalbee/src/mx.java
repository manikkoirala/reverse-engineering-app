import java.util.Set;
import java.util.Iterator;
import java.util.concurrent.ConcurrentHashMap;
import java.util.ArrayDeque;
import java.util.HashMap;
import java.util.concurrent.Executor;
import java.util.Queue;
import java.util.Map;

// 
// Decompiled by Procyon v0.6.0
// 

public class mx implements bs1, y91
{
    public final Map a;
    public Queue b;
    public final Executor c;
    
    public mx(final Executor c) {
        this.a = new HashMap();
        this.b = new ArrayDeque();
        this.c = c;
    }
    
    @Override
    public void a(final Class clazz, final Executor value, final qx key) {
        synchronized (this) {
            j71.b(clazz);
            j71.b(key);
            j71.b(value);
            if (!this.a.containsKey(clazz)) {
                this.a.put(clazz, new ConcurrentHashMap());
            }
            this.a.get(clazz).put(key, value);
        }
    }
    
    @Override
    public void b(final Class clazz, final qx qx) {
        this.a(clazz, this.c, qx);
    }
    
    public void d() {
        synchronized (this) {
            Queue b = this.b;
            if (b != null) {
                this.b = null;
            }
            else {
                b = null;
            }
            monitorexit(this);
            if (b != null) {
                final Iterator iterator = b.iterator();
                while (iterator.hasNext()) {
                    zu0.a(iterator.next());
                    this.g(null);
                }
            }
        }
    }
    
    public final Set e(final kx kx) {
        synchronized (this) {
            throw null;
        }
    }
    
    public void g(final kx kx) {
        j71.b(kx);
        synchronized (this) {
            final Queue b = this.b;
            if (b != null) {
                b.add(kx);
                return;
            }
            monitorexit(this);
            for (final Map.Entry<K, Executor> entry : this.e(kx)) {
                entry.getValue().execute(new lx((Map.Entry)entry, kx));
            }
        }
    }
}

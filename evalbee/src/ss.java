import java.util.Collection;
import androidx.constraintlayout.core.widgets.analyzer.b;
import java.util.HashSet;
import androidx.constraintlayout.core.widgets.f;
import androidx.constraintlayout.core.widgets.ConstraintAnchor;
import androidx.constraintlayout.core.widgets.analyzer.a;
import androidx.constraintlayout.core.widgets.analyzer.c;
import androidx.constraintlayout.core.widgets.ConstraintWidget;
import java.util.Iterator;
import androidx.constraintlayout.core.widgets.analyzer.WidgetRun;
import androidx.constraintlayout.core.widgets.analyzer.DependencyNode;
import java.util.ArrayList;
import androidx.constraintlayout.core.widgets.d;

// 
// Decompiled by Procyon v0.6.0
// 

public class ss
{
    public d a;
    public boolean b;
    public boolean c;
    public d d;
    public ArrayList e;
    public ArrayList f;
    public lb.b g;
    public lb.a h;
    public ArrayList i;
    
    public ss(final d d) {
        this.b = true;
        this.c = true;
        this.e = new ArrayList();
        this.f = new ArrayList();
        this.g = null;
        this.h = new lb.a();
        this.i = new ArrayList();
        this.a = d;
        this.d = d;
    }
    
    public final void a(final DependencyNode dependencyNode, final int n, final int n2, final DependencyNode dependencyNode2, final ArrayList list, final zf1 zf1) {
        final WidgetRun d = dependencyNode.d;
        if (d.c == null) {
            final d a = this.a;
            if (d != a.e) {
                if (d != a.f) {
                    zf1 zf2;
                    if ((zf2 = zf1) == null) {
                        zf2 = new zf1(d, n2);
                        list.add(zf2);
                    }
                    (d.c = zf2).a(d);
                    for (final ps ps : d.h.k) {
                        if (ps instanceof DependencyNode) {
                            this.a((DependencyNode)ps, n, 0, dependencyNode2, list, zf2);
                        }
                    }
                    for (final ps ps2 : d.i.k) {
                        if (ps2 instanceof DependencyNode) {
                            this.a((DependencyNode)ps2, n, 1, dependencyNode2, list, zf2);
                        }
                    }
                    if (n == 1 && d instanceof androidx.constraintlayout.core.widgets.analyzer.d) {
                        for (final ps ps3 : ((androidx.constraintlayout.core.widgets.analyzer.d)d).k.k) {
                            if (ps3 instanceof DependencyNode) {
                                this.a((DependencyNode)ps3, n, 2, dependencyNode2, list, zf2);
                            }
                        }
                    }
                    for (final DependencyNode dependencyNode3 : d.h.l) {
                        if (dependencyNode3 == dependencyNode2) {
                            zf2.b = true;
                        }
                        this.a(dependencyNode3, n, 0, dependencyNode2, list, zf2);
                    }
                    for (final DependencyNode dependencyNode4 : d.i.l) {
                        if (dependencyNode4 == dependencyNode2) {
                            zf2.b = true;
                        }
                        this.a(dependencyNode4, n, 1, dependencyNode2, list, zf2);
                    }
                    if (n == 1 && d instanceof androidx.constraintlayout.core.widgets.analyzer.d) {
                        final Iterator iterator6 = ((androidx.constraintlayout.core.widgets.analyzer.d)d).k.l.iterator();
                        while (iterator6.hasNext()) {
                            this.a((DependencyNode)iterator6.next(), n, 2, dependencyNode2, list, zf2);
                        }
                    }
                }
            }
        }
    }
    
    public final boolean b(final d d) {
        ConstraintWidget.DimensionBehaviour[] z;
        ConstraintWidget.DimensionBehaviour dimensionBehaviour;
        ConstraintWidget.DimensionBehaviour dimensionBehaviour2;
        ConstraintWidget.DimensionBehaviour match_CONSTRAINT;
        ConstraintWidget.DimensionBehaviour match_CONSTRAINT2;
        Enum<ConstraintWidget.DimensionBehaviour> wrap_CONTENT = null;
        Enum<ConstraintWidget.DimensionBehaviour> wrap_CONTENT2 = null;
        c e;
        int w;
        androidx.constraintlayout.core.widgets.analyzer.d f;
        int x;
        ConstraintWidget.DimensionBehaviour match_PARENT;
        int n = 0;
        int w2;
        int g;
        int g2;
        Enum<ConstraintWidget.DimensionBehaviour> fixed;
        int n2 = 0;
        int x2;
        int g3;
        int g4;
        Enum<ConstraintWidget.DimensionBehaviour> enum1 = null;
        Enum<ConstraintWidget.DimensionBehaviour> enum2 = null;
        ConstraintWidget.DimensionBehaviour wrap_CONTENT3;
        a a;
        int m;
        ConstraintWidget.DimensionBehaviour dimensionBehaviour3;
        ConstraintWidget.DimensionBehaviour fixed2;
        ConstraintWidget.DimensionBehaviour dimensionBehaviour4;
        ConstraintAnchor[] w3;
        ConstraintWidget.DimensionBehaviour dimensionBehaviour5;
        ConstraintWidget.DimensionBehaviour wrap_CONTENT4;
        float d2;
        ConstraintWidget.DimensionBehaviour dimensionBehaviour6;
        ConstraintWidget.DimensionBehaviour fixed3;
        float e2;
        ConstraintAnchor[] w4;
        ConstraintWidget.DimensionBehaviour wrap_CONTENT5;
        ConstraintWidget.DimensionBehaviour[] z2;
        ConstraintWidget.DimensionBehaviour dimensionBehaviour7;
        float b;
        float e3;
        Label_0581_Outer:Label_0539_Outer:
        for (final ConstraintWidget constraintWidget : d.L0) {
            z = constraintWidget.Z;
            dimensionBehaviour = z[0];
            dimensionBehaviour2 = z[1];
            if (constraintWidget.V() != 8) {
                if (constraintWidget.B < 1.0f && dimensionBehaviour == ConstraintWidget.DimensionBehaviour.MATCH_CONSTRAINT) {
                    constraintWidget.w = 2;
                }
                if (constraintWidget.E < 1.0f && dimensionBehaviour2 == ConstraintWidget.DimensionBehaviour.MATCH_CONSTRAINT) {
                    constraintWidget.x = 2;
                }
                Label_0235: {
                    if (constraintWidget.v() > 0.0f) {
                        match_CONSTRAINT = ConstraintWidget.DimensionBehaviour.MATCH_CONSTRAINT;
                        if (dimensionBehaviour == match_CONSTRAINT && (dimensionBehaviour2 == ConstraintWidget.DimensionBehaviour.WRAP_CONTENT || dimensionBehaviour2 == ConstraintWidget.DimensionBehaviour.FIXED)) {
                            constraintWidget.w = 3;
                        }
                        else {
                            if (dimensionBehaviour2 != match_CONSTRAINT || (dimensionBehaviour != ConstraintWidget.DimensionBehaviour.WRAP_CONTENT && dimensionBehaviour != ConstraintWidget.DimensionBehaviour.FIXED)) {
                                if (dimensionBehaviour != match_CONSTRAINT || dimensionBehaviour2 != match_CONSTRAINT) {
                                    break Label_0235;
                                }
                                if (constraintWidget.w == 0) {
                                    constraintWidget.w = 3;
                                }
                                if (constraintWidget.x != 0) {
                                    break Label_0235;
                                }
                            }
                            constraintWidget.x = 3;
                        }
                    }
                }
                match_CONSTRAINT2 = ConstraintWidget.DimensionBehaviour.MATCH_CONSTRAINT;
                Label_0295: {
                    if ((wrap_CONTENT = dimensionBehaviour) == match_CONSTRAINT2) {
                        wrap_CONTENT = dimensionBehaviour;
                        if (constraintWidget.w == 1) {
                            if (constraintWidget.O.f != null) {
                                wrap_CONTENT = dimensionBehaviour;
                                if (constraintWidget.Q.f != null) {
                                    break Label_0295;
                                }
                            }
                            wrap_CONTENT = ConstraintWidget.DimensionBehaviour.WRAP_CONTENT;
                        }
                    }
                }
                Label_0350: {
                    if ((wrap_CONTENT2 = dimensionBehaviour2) == match_CONSTRAINT2) {
                        wrap_CONTENT2 = dimensionBehaviour2;
                        if (constraintWidget.x == 1) {
                            if (constraintWidget.P.f != null) {
                                wrap_CONTENT2 = dimensionBehaviour2;
                                if (constraintWidget.R.f != null) {
                                    break Label_0350;
                                }
                            }
                            wrap_CONTENT2 = ConstraintWidget.DimensionBehaviour.WRAP_CONTENT;
                        }
                    }
                }
                e = constraintWidget.e;
                e.d = (ConstraintWidget.DimensionBehaviour)wrap_CONTENT;
                w = constraintWidget.w;
                e.a = w;
                f = constraintWidget.f;
                f.d = (ConstraintWidget.DimensionBehaviour)wrap_CONTENT2;
                x = constraintWidget.x;
                f.a = x;
                match_PARENT = ConstraintWidget.DimensionBehaviour.MATCH_PARENT;
                Label_1228: {
                    if ((wrap_CONTENT == match_PARENT || wrap_CONTENT == ConstraintWidget.DimensionBehaviour.FIXED || wrap_CONTENT == ConstraintWidget.DimensionBehaviour.WRAP_CONTENT) && (wrap_CONTENT2 == match_PARENT || wrap_CONTENT2 == ConstraintWidget.DimensionBehaviour.FIXED || wrap_CONTENT2 == ConstraintWidget.DimensionBehaviour.WRAP_CONTENT)) {
                        n = constraintWidget.W();
                        if (wrap_CONTENT == match_PARENT) {
                            w2 = d.W();
                            g = constraintWidget.O.g;
                            g2 = constraintWidget.Q.g;
                            fixed = ConstraintWidget.DimensionBehaviour.FIXED;
                            n = w2 - g - g2;
                        }
                        else {
                            fixed = wrap_CONTENT;
                        }
                        n2 = constraintWidget.x();
                        if (wrap_CONTENT2 == match_PARENT) {
                            x2 = d.x();
                            g3 = constraintWidget.P.g;
                            g4 = constraintWidget.R.g;
                            enum1 = ConstraintWidget.DimensionBehaviour.FIXED;
                            n2 = x2 - g3 - g4;
                        }
                        else {
                            enum1 = wrap_CONTENT2;
                        }
                        enum2 = fixed;
                    }
                    else {
                    Label_0819_Outer:
                        while (true) {
                            while (true) {
                            Label_0692_Outer:
                                while (true) {
                                Label_0534_Outer:
                                    while (true) {
                                        while (true) {
                                            Label_0707: {
                                                if (wrap_CONTENT != match_CONSTRAINT2) {
                                                    break Label_0707;
                                                }
                                                wrap_CONTENT3 = ConstraintWidget.DimensionBehaviour.WRAP_CONTENT;
                                                if (wrap_CONTENT2 != wrap_CONTENT3 && wrap_CONTENT2 != ConstraintWidget.DimensionBehaviour.FIXED) {
                                                    break Label_0707;
                                                }
                                                if (w == 3) {
                                                    if (wrap_CONTENT2 == wrap_CONTENT3) {
                                                        this.l(constraintWidget, wrap_CONTENT3, 0, wrap_CONTENT3, 0);
                                                    }
                                                    n2 = constraintWidget.x();
                                                    n = (int)(n2 * constraintWidget.d0 + 0.5f);
                                                }
                                                else {
                                                    if (w == 1) {
                                                        this.l(constraintWidget, wrap_CONTENT3, 0, (ConstraintWidget.DimensionBehaviour)wrap_CONTENT2, 0);
                                                        a = constraintWidget.e.e;
                                                        m = constraintWidget.W();
                                                        break Label_0581;
                                                    }
                                                    if (w == 2) {
                                                        dimensionBehaviour3 = d.Z[0];
                                                        fixed2 = ConstraintWidget.DimensionBehaviour.FIXED;
                                                        if (dimensionBehaviour3 == fixed2 || dimensionBehaviour3 == match_PARENT) {
                                                            n = (int)(constraintWidget.B * d.W() + 0.5f);
                                                            n2 = constraintWidget.x();
                                                            dimensionBehaviour4 = fixed2;
                                                            break Label_0692;
                                                        }
                                                        break Label_0707;
                                                    }
                                                    else {
                                                        w3 = constraintWidget.W;
                                                        if (w3[0].f == null || w3[1].f == null) {
                                                            n = 0;
                                                            n2 = 0;
                                                            dimensionBehaviour4 = wrap_CONTENT3;
                                                            break Label_0692;
                                                        }
                                                        break Label_0707;
                                                    }
                                                }
                                                enum1 = ConstraintWidget.DimensionBehaviour.FIXED;
                                                enum2 = enum1;
                                                break Label_1228;
                                                a.m = m;
                                                continue Label_0581_Outer;
                                                dimensionBehaviour5 = (ConstraintWidget.DimensionBehaviour)wrap_CONTENT2;
                                                enum2 = dimensionBehaviour4;
                                                enum1 = dimensionBehaviour5;
                                                break Label_1228;
                                            }
                                            if (wrap_CONTENT2 != match_CONSTRAINT2) {
                                                break Label_0692_Outer;
                                            }
                                            wrap_CONTENT4 = ConstraintWidget.DimensionBehaviour.WRAP_CONTENT;
                                            if (wrap_CONTENT != wrap_CONTENT4 && wrap_CONTENT != ConstraintWidget.DimensionBehaviour.FIXED) {
                                                break Label_0692_Outer;
                                            }
                                            if (x == 3) {
                                                if (wrap_CONTENT == wrap_CONTENT4) {
                                                    this.l(constraintWidget, wrap_CONTENT4, 0, wrap_CONTENT4, 0);
                                                }
                                                n = constraintWidget.W();
                                                d2 = constraintWidget.d0;
                                                if (constraintWidget.w() == -1) {
                                                    d2 = 1.0f / d2;
                                                }
                                                n2 = (int)(n * d2 + 0.5f);
                                                continue Label_0539_Outer;
                                            }
                                            break;
                                        }
                                        if (x == 1) {
                                            this.l(constraintWidget, (ConstraintWidget.DimensionBehaviour)wrap_CONTENT, 0, wrap_CONTENT4, 0);
                                        }
                                        else if (x == 2) {
                                            dimensionBehaviour6 = d.Z[1];
                                            fixed3 = ConstraintWidget.DimensionBehaviour.FIXED;
                                            if (dimensionBehaviour6 == fixed3 || dimensionBehaviour6 == match_PARENT) {
                                                e2 = constraintWidget.E;
                                                n = constraintWidget.W();
                                                n2 = (int)(e2 * d.x() + 0.5f);
                                                enum2 = wrap_CONTENT;
                                                enum1 = fixed3;
                                                break Label_1228;
                                            }
                                            break Label_0692_Outer;
                                        }
                                        else {
                                            w4 = constraintWidget.W;
                                            if (w4[2].f == null || w4[3].f == null) {
                                                n = 0;
                                                n2 = 0;
                                                dimensionBehaviour4 = wrap_CONTENT4;
                                                continue Label_0534_Outer;
                                            }
                                            break Label_0692_Outer;
                                        }
                                        break;
                                    }
                                    a = constraintWidget.f.e;
                                    m = constraintWidget.x();
                                    continue Label_0692_Outer;
                                }
                                if (wrap_CONTENT != match_CONSTRAINT2 || wrap_CONTENT2 != match_CONSTRAINT2) {
                                    continue Label_0581_Outer;
                                }
                                if (w == 1 || x == 1) {
                                    wrap_CONTENT5 = ConstraintWidget.DimensionBehaviour.WRAP_CONTENT;
                                    this.l(constraintWidget, wrap_CONTENT5, 0, wrap_CONTENT5, 0);
                                    constraintWidget.e.e.m = constraintWidget.W();
                                    continue;
                                }
                                break;
                            }
                            if (x != 2 || w != 2) {
                                continue Label_0581_Outer;
                            }
                            z2 = d.Z;
                            dimensionBehaviour7 = z2[0];
                            enum1 = ConstraintWidget.DimensionBehaviour.FIXED;
                            if (dimensionBehaviour7 == enum1 && z2[1] == enum1) {
                                b = constraintWidget.B;
                                e3 = constraintWidget.E;
                                n = (int)(b * d.W() + 0.5f);
                                n2 = (int)(e3 * d.x() + 0.5f);
                                continue Label_0819_Outer;
                            }
                            break;
                        }
                        continue Label_0581_Outer;
                    }
                }
                this.l(constraintWidget, (ConstraintWidget.DimensionBehaviour)enum2, n, (ConstraintWidget.DimensionBehaviour)enum1, n2);
                constraintWidget.e.e.d(constraintWidget.W());
                constraintWidget.f.e.d(constraintWidget.x());
            }
            constraintWidget.a = true;
        }
        return false;
    }
    
    public void c() {
        this.d(this.e);
        this.i.clear();
        zf1.h = 0;
        this.i(this.a.e, 0, this.i);
        this.i(this.a.f, 1, this.i);
        this.b = false;
    }
    
    public void d(final ArrayList list) {
        list.clear();
        this.d.e.f();
        this.d.f.f();
        list.add(this.d.e);
        list.add(this.d.f);
        final Iterator iterator = this.d.L0.iterator();
        HashSet<tf> c = null;
        while (iterator.hasNext()) {
            final ConstraintWidget constraintWidget = (ConstraintWidget)iterator.next();
            Object e;
            if (constraintWidget instanceof f) {
                e = new hc0(constraintWidget);
            }
            else {
                if (constraintWidget.i0()) {
                    if (constraintWidget.c == null) {
                        constraintWidget.c = new tf(constraintWidget, 0);
                    }
                    HashSet<tf> set;
                    if ((set = c) == null) {
                        set = new HashSet<tf>();
                    }
                    set.add(constraintWidget.c);
                    c = set;
                }
                else {
                    list.add(constraintWidget.e);
                }
                HashSet<tf> set2;
                if (constraintWidget.k0()) {
                    if (constraintWidget.d == null) {
                        constraintWidget.d = new tf(constraintWidget, 1);
                    }
                    if ((set2 = c) == null) {
                        set2 = new HashSet<tf>();
                    }
                    set2.add(constraintWidget.d);
                }
                else {
                    list.add(constraintWidget.f);
                    set2 = c;
                }
                c = set2;
                if (!(constraintWidget instanceof gd0)) {
                    continue;
                }
                final b b = new b(constraintWidget);
                c = set2;
                e = b;
            }
            list.add(e);
        }
        if (c != null) {
            list.addAll(c);
        }
        final Iterator iterator2 = list.iterator();
        while (iterator2.hasNext()) {
            ((WidgetRun)iterator2.next()).f();
        }
        for (final WidgetRun widgetRun : list) {
            if (widgetRun.b == this.d) {
                continue;
            }
            widgetRun.d();
        }
    }
    
    public final int e(final d d, final int n) {
        final int size = this.i.size();
        long max = 0L;
        for (int i = 0; i < size; ++i) {
            max = Math.max(max, ((zf1)this.i.get(i)).b(d, n));
        }
        return (int)max;
    }
    
    public boolean f(final boolean b) {
        final boolean b2 = true;
        final boolean b3 = b & true;
        if (this.b || this.c) {
            for (final ConstraintWidget constraintWidget : this.a.L0) {
                constraintWidget.n();
                constraintWidget.a = false;
                constraintWidget.e.r();
                constraintWidget.f.q();
            }
            this.a.n();
            final d a = this.a;
            a.a = false;
            a.e.r();
            this.a.f.q();
            this.c = false;
        }
        if (this.b(this.d)) {
            return false;
        }
        this.a.m1(0);
        this.a.n1(0);
        final ConstraintWidget.DimensionBehaviour u = this.a.u(0);
        final ConstraintWidget.DimensionBehaviour u2 = this.a.u(1);
        if (this.b) {
            this.c();
        }
        final int x = this.a.X();
        final int y = this.a.Y();
        this.a.e.h.d(x);
        this.a.f.h.d(y);
        this.m();
        final ConstraintWidget.DimensionBehaviour wrap_CONTENT = ConstraintWidget.DimensionBehaviour.WRAP_CONTENT;
        if (u == wrap_CONTENT || u2 == wrap_CONTENT) {
            boolean b4 = false;
            Label_0303: {
                if (b4 = b3) {
                    final Iterator iterator2 = this.e.iterator();
                    do {
                        b4 = b3;
                        if (iterator2.hasNext()) {
                            continue;
                        }
                        break Label_0303;
                    } while (((WidgetRun)iterator2.next()).m());
                    b4 = false;
                }
            }
            if (b4 && u == ConstraintWidget.DimensionBehaviour.WRAP_CONTENT) {
                this.a.P0(ConstraintWidget.DimensionBehaviour.FIXED);
                final d a2 = this.a;
                a2.k1(this.e(a2, 0));
                final d a3 = this.a;
                a3.e.e.d(a3.W());
            }
            if (b4 && u2 == ConstraintWidget.DimensionBehaviour.WRAP_CONTENT) {
                this.a.g1(ConstraintWidget.DimensionBehaviour.FIXED);
                final d a4 = this.a;
                a4.L0(this.e(a4, 1));
                final d a5 = this.a;
                a5.f.e.d(a5.x());
            }
        }
        final d a6 = this.a;
        final ConstraintWidget.DimensionBehaviour dimensionBehaviour = a6.Z[0];
        final ConstraintWidget.DimensionBehaviour fixed = ConstraintWidget.DimensionBehaviour.FIXED;
        boolean b5;
        if (dimensionBehaviour != fixed && dimensionBehaviour != ConstraintWidget.DimensionBehaviour.MATCH_PARENT) {
            b5 = false;
        }
        else {
            final int n = a6.W() + x;
            this.a.e.i.d(n);
            this.a.e.e.d(n - x);
            this.m();
            final d a7 = this.a;
            final ConstraintWidget.DimensionBehaviour dimensionBehaviour2 = a7.Z[1];
            if (dimensionBehaviour2 == fixed || dimensionBehaviour2 == ConstraintWidget.DimensionBehaviour.MATCH_PARENT) {
                final int n2 = a7.x() + y;
                this.a.f.i.d(n2);
                this.a.f.e.d(n2 - y);
            }
            this.m();
            b5 = true;
        }
        for (final WidgetRun widgetRun : this.e) {
            if (widgetRun.b == this.a && !widgetRun.g) {
                continue;
            }
            widgetRun.e();
        }
        final Iterator iterator4 = this.e.iterator();
        boolean b6 = false;
        Label_0773: {
            while (true) {
                b6 = b2;
                if (!iterator4.hasNext()) {
                    break Label_0773;
                }
                final WidgetRun widgetRun2 = (WidgetRun)iterator4.next();
                if (!b5 && widgetRun2.b == this.a) {
                    continue;
                }
                if (!widgetRun2.h.j) {
                    break;
                }
                if (!widgetRun2.i.j && !(widgetRun2 instanceof hc0)) {
                    break;
                }
                if (!widgetRun2.e.j && !(widgetRun2 instanceof tf) && !(widgetRun2 instanceof hc0)) {
                    break;
                }
            }
            b6 = false;
        }
        this.a.P0(u);
        this.a.g1(u2);
        return b6;
    }
    
    public boolean g(final boolean b) {
        if (this.b) {
            for (final ConstraintWidget constraintWidget : this.a.L0) {
                constraintWidget.n();
                constraintWidget.a = false;
                final c e = constraintWidget.e;
                e.e.j = false;
                e.g = false;
                e.r();
                final androidx.constraintlayout.core.widgets.analyzer.d f = constraintWidget.f;
                f.e.j = false;
                f.g = false;
                f.q();
            }
            this.a.n();
            final d a = this.a;
            a.a = false;
            final c e2 = a.e;
            e2.e.j = false;
            e2.g = false;
            e2.r();
            final androidx.constraintlayout.core.widgets.analyzer.d f2 = this.a.f;
            f2.e.j = false;
            f2.g = false;
            f2.q();
            this.c();
        }
        if (this.b(this.d)) {
            return false;
        }
        this.a.m1(0);
        this.a.n1(0);
        this.a.e.h.d(0);
        this.a.f.h.d(0);
        return true;
    }
    
    public boolean h(final boolean b, final int n) {
        final boolean b2 = true;
        final boolean b3 = b & true;
        final ConstraintWidget.DimensionBehaviour u = this.a.u(0);
        final ConstraintWidget.DimensionBehaviour u2 = this.a.u(1);
        final int x = this.a.X();
        final int y = this.a.Y();
        Label_0260: {
            if (b3) {
                final ConstraintWidget.DimensionBehaviour wrap_CONTENT = ConstraintWidget.DimensionBehaviour.WRAP_CONTENT;
                if (u == wrap_CONTENT || u2 == wrap_CONTENT) {
                    final Iterator iterator = this.e.iterator();
                    while (true) {
                        WidgetRun widgetRun;
                        do {
                            final boolean b4 = b3;
                            if (!iterator.hasNext()) {
                                a a3;
                                int n2;
                                if (n == 0) {
                                    if (!b4 || u != ConstraintWidget.DimensionBehaviour.WRAP_CONTENT) {
                                        break Label_0260;
                                    }
                                    this.a.P0(ConstraintWidget.DimensionBehaviour.FIXED);
                                    final d a = this.a;
                                    a.k1(this.e(a, 0));
                                    final d a2 = this.a;
                                    a3 = a2.e.e;
                                    n2 = a2.W();
                                }
                                else {
                                    if (!b4 || u2 != ConstraintWidget.DimensionBehaviour.WRAP_CONTENT) {
                                        break Label_0260;
                                    }
                                    this.a.g1(ConstraintWidget.DimensionBehaviour.FIXED);
                                    final d a4 = this.a;
                                    a4.L0(this.e(a4, 1));
                                    final d a5 = this.a;
                                    a3 = a5.f.e;
                                    n2 = a5.x();
                                }
                                a3.d(n2);
                                break Label_0260;
                            }
                            widgetRun = (WidgetRun)iterator.next();
                        } while (widgetRun.f != n || widgetRun.m());
                        final boolean b4 = false;
                        continue;
                    }
                }
            }
        }
        final d a6 = this.a;
        boolean b5 = false;
        Label_0413: {
            Label_0411: {
                if (n == 0) {
                    final ConstraintWidget.DimensionBehaviour dimensionBehaviour = a6.Z[0];
                    if (dimensionBehaviour == ConstraintWidget.DimensionBehaviour.FIXED || dimensionBehaviour == ConstraintWidget.DimensionBehaviour.MATCH_PARENT) {
                        final int n3 = a6.W() + x;
                        this.a.e.i.d(n3);
                        this.a.e.e.d(n3 - x);
                        break Label_0411;
                    }
                }
                else {
                    final ConstraintWidget.DimensionBehaviour dimensionBehaviour2 = a6.Z[1];
                    if (dimensionBehaviour2 == ConstraintWidget.DimensionBehaviour.FIXED || dimensionBehaviour2 == ConstraintWidget.DimensionBehaviour.MATCH_PARENT) {
                        final int n4 = a6.x() + y;
                        this.a.f.i.d(n4);
                        this.a.f.e.d(n4 - y);
                        break Label_0411;
                    }
                }
                b5 = false;
                break Label_0413;
            }
            b5 = true;
        }
        this.m();
        for (final WidgetRun widgetRun2 : this.e) {
            if (widgetRun2.f != n) {
                continue;
            }
            if (widgetRun2.b == this.a && !widgetRun2.g) {
                continue;
            }
            widgetRun2.e();
        }
        final Iterator iterator3 = this.e.iterator();
        boolean b6 = false;
        Label_0608: {
            while (true) {
                b6 = b2;
                if (!iterator3.hasNext()) {
                    break Label_0608;
                }
                final WidgetRun widgetRun3 = (WidgetRun)iterator3.next();
                if (widgetRun3.f != n) {
                    continue;
                }
                if (!b5 && widgetRun3.b == this.a) {
                    continue;
                }
                if (!widgetRun3.h.j) {
                    break;
                }
                if (!widgetRun3.i.j) {
                    break;
                }
                if (!(widgetRun3 instanceof tf) && !widgetRun3.e.j) {
                    break;
                }
            }
            b6 = false;
        }
        this.a.P0(u);
        this.a.g1(u2);
        return b6;
    }
    
    public final void i(final WidgetRun widgetRun, final int n, final ArrayList list) {
        for (final ps ps : widgetRun.h.k) {
            if (ps instanceof DependencyNode) {
                this.a((DependencyNode)ps, n, 0, widgetRun.i, list, null);
            }
            else {
                if (!(ps instanceof WidgetRun)) {
                    continue;
                }
                this.a(((WidgetRun)ps).h, n, 0, widgetRun.i, list, null);
            }
        }
        for (final ps ps2 : widgetRun.i.k) {
            if (ps2 instanceof DependencyNode) {
                this.a((DependencyNode)ps2, n, 1, widgetRun.h, list, null);
            }
            else {
                if (!(ps2 instanceof WidgetRun)) {
                    continue;
                }
                this.a(((WidgetRun)ps2).i, n, 1, widgetRun.h, list, null);
            }
        }
        if (n == 1) {
            for (final ps ps3 : ((androidx.constraintlayout.core.widgets.analyzer.d)widgetRun).k.k) {
                if (ps3 instanceof DependencyNode) {
                    this.a((DependencyNode)ps3, n, 2, null, list, null);
                }
            }
        }
    }
    
    public void j() {
        this.b = true;
    }
    
    public void k() {
        this.c = true;
    }
    
    public final void l(final ConstraintWidget constraintWidget, final ConstraintWidget.DimensionBehaviour a, final int c, final ConstraintWidget.DimensionBehaviour b, final int d) {
        final lb.a h = this.h;
        h.a = a;
        h.b = b;
        h.c = c;
        h.d = d;
        this.g.a(constraintWidget, h);
        constraintWidget.k1(this.h.e);
        constraintWidget.L0(this.h.f);
        constraintWidget.K0(this.h.h);
        constraintWidget.A0(this.h.g);
    }
    
    public void m() {
        for (final ConstraintWidget constraintWidget : this.a.L0) {
            if (constraintWidget.a) {
                continue;
            }
            final ConstraintWidget.DimensionBehaviour[] z = constraintWidget.Z;
            final int n = 0;
            final ConstraintWidget.DimensionBehaviour dimensionBehaviour = z[0];
            final ConstraintWidget.DimensionBehaviour dimensionBehaviour2 = z[1];
            final int w = constraintWidget.w;
            final int x = constraintWidget.x;
            final ConstraintWidget.DimensionBehaviour wrap_CONTENT = ConstraintWidget.DimensionBehaviour.WRAP_CONTENT;
            final boolean b = dimensionBehaviour == wrap_CONTENT || (dimensionBehaviour == ConstraintWidget.DimensionBehaviour.MATCH_CONSTRAINT && w == 1);
            int n2 = 0;
            Label_0141: {
                if (dimensionBehaviour2 != wrap_CONTENT) {
                    n2 = n;
                    if (dimensionBehaviour2 != ConstraintWidget.DimensionBehaviour.MATCH_CONSTRAINT) {
                        break Label_0141;
                    }
                    n2 = n;
                    if (x != 1) {
                        break Label_0141;
                    }
                }
                n2 = 1;
            }
            final a e = constraintWidget.e.e;
            final boolean j = e.j;
            final a e2 = constraintWidget.f.e;
            final boolean i = e2.j;
            Label_0383: {
                if (j && i) {
                    final ConstraintWidget.DimensionBehaviour fixed = ConstraintWidget.DimensionBehaviour.FIXED;
                    this.l(constraintWidget, fixed, e.g, fixed, e2.g);
                }
                else {
                    a a = null;
                    int n3 = 0;
                    Label_0298: {
                        a a2;
                        int m;
                        if (j && n2 != 0) {
                            this.l(constraintWidget, ConstraintWidget.DimensionBehaviour.FIXED, e.g, wrap_CONTENT, e2.g);
                            if (dimensionBehaviour2 != ConstraintWidget.DimensionBehaviour.MATCH_CONSTRAINT) {
                                a = constraintWidget.f.e;
                                n3 = constraintWidget.x();
                                break Label_0298;
                            }
                            a2 = constraintWidget.f.e;
                            m = constraintWidget.x();
                        }
                        else {
                            if (!i || !b) {
                                break Label_0383;
                            }
                            this.l(constraintWidget, wrap_CONTENT, e.g, ConstraintWidget.DimensionBehaviour.FIXED, e2.g);
                            if (dimensionBehaviour != ConstraintWidget.DimensionBehaviour.MATCH_CONSTRAINT) {
                                a = constraintWidget.e.e;
                                n3 = constraintWidget.W();
                                break Label_0298;
                            }
                            a2 = constraintWidget.e.e;
                            m = constraintWidget.W();
                        }
                        a2.m = m;
                        break Label_0383;
                    }
                    a.d(n3);
                }
                constraintWidget.a = true;
            }
            if (!constraintWidget.a) {
                continue;
            }
            final a l = constraintWidget.f.l;
            if (l == null) {
                continue;
            }
            l.d(constraintWidget.p());
        }
    }
    
    public void n(final lb.b g) {
        this.g = g;
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.android.billingclient.api;

import org.json.JSONArray;
import java.util.ArrayList;
import java.util.List;
import android.text.TextUtils;
import org.json.JSONObject;

public class Purchase
{
    public final String a;
    public final String b;
    public final JSONObject c;
    
    public Purchase(final String a, final String b) {
        this.a = a;
        this.b = b;
        this.c = new JSONObject(a);
    }
    
    public String a() {
        String optString;
        if (TextUtils.isEmpty((CharSequence)(optString = this.c.optString("orderId")))) {
            optString = null;
        }
        return optString;
    }
    
    public String b() {
        return this.a;
    }
    
    public List c() {
        return this.g();
    }
    
    public int d() {
        if (this.c.optInt("purchaseState", 1) != 4) {
            return 1;
        }
        return 2;
    }
    
    public String e() {
        final JSONObject c = this.c;
        return c.optString("token", c.optString("purchaseToken"));
    }
    
    @Override
    public boolean equals(final Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Purchase)) {
            return false;
        }
        final Purchase purchase = (Purchase)o;
        return TextUtils.equals((CharSequence)this.a, (CharSequence)purchase.b()) && TextUtils.equals((CharSequence)this.b, (CharSequence)purchase.f());
    }
    
    public String f() {
        return this.b;
    }
    
    public final ArrayList g() {
        final ArrayList list = new ArrayList();
        if (this.c.has("productIds")) {
            final JSONArray optJSONArray = this.c.optJSONArray("productIds");
            if (optJSONArray != null) {
                for (int i = 0; i < optJSONArray.length(); ++i) {
                    list.add(optJSONArray.optString(i));
                }
            }
        }
        else if (this.c.has("productId")) {
            list.add(this.c.optString("productId"));
        }
        return list;
    }
    
    @Override
    public int hashCode() {
        return this.a.hashCode();
    }
    
    @Override
    public String toString() {
        return "Purchase. Json: ".concat(String.valueOf(this.a));
    }
}

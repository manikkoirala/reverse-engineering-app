// 
// Decompiled by Procyon v0.6.0
// 

package com.android.billingclient.api;

import com.google.android.gms.internal.play_billing.zzdg;
import android.os.BaseBundle;
import android.content.Context;
import android.os.Parcelable;
import android.content.IntentSender$SendIntentException;
import android.app.PendingIntent;
import android.os.Bundle;
import com.google.android.gms.internal.play_billing.zzb;
import android.content.Intent;
import android.os.ResultReceiver;
import com.google.android.apps.common.proguard.UsedByReflection;
import androidx.activity.ComponentActivity;

@UsedByReflection("PlatformActivityProxy")
public class ProxyBillingActivity extends ComponentActivity
{
    public r2 a;
    public ResultReceiver b;
    public ResultReceiver c;
    public ResultReceiver d;
    public boolean e;
    public boolean f;
    
    public final Intent k(final String s) {
        final Intent intent = new Intent("com.android.vending.billing.ALTERNATIVE_BILLING");
        intent.setPackage(((Context)this).getApplicationContext().getPackageName());
        intent.putExtra("ALTERNATIVE_BILLING_USER_CHOICE_DATA", s);
        return intent;
    }
    
    public final Intent l() {
        ((Context)this).getApplicationContext().getPackageName();
        final Intent intent = new Intent("com.android.vending.billing.PURCHASES_UPDATED");
        intent.setPackage(((Context)this).getApplicationContext().getPackageName());
        return intent;
    }
    
    public void m(final l2 l2) {
        final Intent b = l2.b();
        final int zzc = zzb.zzc(b, "ProxyBillingActivity");
        final ResultReceiver d = this.d;
        if (d != null) {
            Bundle extras;
            if (b == null) {
                extras = null;
            }
            else {
                extras = b.getExtras();
            }
            d.send(zzc, extras);
        }
        if (l2.c() != -1 || zzc != 0) {
            final int c = l2.c();
            final StringBuilder sb = new StringBuilder();
            sb.append("Alternative billing only dialog finished with resultCode ");
            sb.append(c);
            sb.append(" and billing's responseCode: ");
            sb.append(zzc);
            zzb.zzk("ProxyBillingActivity", sb.toString());
        }
        this.finish();
    }
    
    @Override
    public void onActivityResult(int zza, int n, Intent intent) {
        super.onActivityResult(zza, n, intent);
        final Bundle bundle = null;
        final Bundle bundle2 = null;
        if (zza != 100 && zza != 110) {
            if (zza == 101) {
                zza = zzb.zza(intent, "ProxyBillingActivity");
                final ResultReceiver c = this.c;
                if (c != null) {
                    Bundle extras;
                    if (intent == null) {
                        extras = bundle2;
                    }
                    else {
                        extras = intent.getExtras();
                    }
                    c.send(zza, extras);
                }
            }
            else {
                final StringBuilder sb = new StringBuilder();
                sb.append("Got onActivityResult with wrong requestCode: ");
                sb.append(zza);
                sb.append("; skipping...");
                zzb.zzk("ProxyBillingActivity", sb.toString());
            }
        }
        else {
            final int zzc = zzb.zzc(intent, "ProxyBillingActivity");
            Label_0203: {
                int i;
                if ((i = n) == -1) {
                    if (zzc == 0) {
                        n = 0;
                        break Label_0203;
                    }
                    i = -1;
                }
                final StringBuilder sb2 = new StringBuilder();
                sb2.append("Activity finished with resultCode ");
                sb2.append(i);
                sb2.append(" and billing's responseCode: ");
                sb2.append(zzc);
                zzb.zzk("ProxyBillingActivity", sb2.toString());
                n = zzc;
            }
            final ResultReceiver b = this.b;
            if (b != null) {
                Bundle extras2;
                if (intent == null) {
                    extras2 = bundle;
                }
                else {
                    extras2 = intent.getExtras();
                }
                b.send(n, extras2);
            }
            else {
                if (intent != null) {
                    if (intent.getExtras() != null) {
                        final String string = ((BaseBundle)intent.getExtras()).getString("ALTERNATIVE_BILLING_USER_CHOICE_DATA");
                        if (string != null) {
                            intent = this.k(string);
                        }
                        else {
                            final Intent l = this.l();
                            l.putExtras(intent.getExtras());
                            intent = l;
                        }
                    }
                    else {
                        intent = this.l();
                        zzb.zzk("ProxyBillingActivity", "Got null bundle!");
                        intent.putExtra("RESPONSE_CODE", 6);
                        intent.putExtra("DEBUG_MESSAGE", "An internal error occurred.");
                        final com.android.billingclient.api.a.a c2 = com.android.billingclient.api.a.c();
                        c2.c(6);
                        c2.b("An internal error occurred.");
                        intent.putExtra("FAILURE_LOGGING_PAYLOAD", ((zzdg)bd2.a(22, 2, c2.a())).zzc());
                    }
                    intent.putExtra("INTENT_SOURCE", "LAUNCH_BILLING_FLOW");
                }
                else {
                    intent = this.l();
                }
                if (zza == 110) {
                    intent.putExtra("IS_FIRST_PARTY_PURCHASE", true);
                }
                ((Context)this).sendBroadcast(intent);
            }
        }
        this.e = false;
        this.finish();
    }
    
    @Override
    public void onCreate(final Bundle bundle) {
        super.onCreate(bundle);
        this.a = this.registerForActivityResult(new q2(), new pe2(this));
        if (bundle == null) {
            zzb.zzj("ProxyBillingActivity", "Launching Play Store billing flow");
            if (this.getIntent().hasExtra("ALTERNATIVE_BILLING_ONLY_DIALOG_INTENT")) {
                final PendingIntent pendingIntent = (PendingIntent)this.getIntent().getParcelableExtra("ALTERNATIVE_BILLING_ONLY_DIALOG_INTENT");
                this.d = (ResultReceiver)this.getIntent().getParcelableExtra("alternative_billing_only_dialog_result_receiver");
                this.a.a(new yf0.a(pendingIntent).a());
                return;
            }
            PendingIntent pendingIntent3 = null;
            int n = 0;
            Label_0263: {
                if (this.getIntent().hasExtra("BUY_INTENT")) {
                    final PendingIntent pendingIntent2 = pendingIntent3 = (PendingIntent)this.getIntent().getParcelableExtra("BUY_INTENT");
                    if (this.getIntent().hasExtra("IS_FLOW_FROM_FIRST_PARTY_CLIENT")) {
                        pendingIntent3 = pendingIntent2;
                        if (this.getIntent().getBooleanExtra("IS_FLOW_FROM_FIRST_PARTY_CLIENT", false)) {
                            this.f = true;
                            n = 110;
                            pendingIntent3 = pendingIntent2;
                            break Label_0263;
                        }
                    }
                }
                else if (this.getIntent().hasExtra("SUBS_MANAGEMENT_INTENT")) {
                    pendingIntent3 = (PendingIntent)this.getIntent().getParcelableExtra("SUBS_MANAGEMENT_INTENT");
                    this.b = (ResultReceiver)this.getIntent().getParcelableExtra("result_receiver");
                }
                else {
                    if (this.getIntent().hasExtra("IN_APP_MESSAGE_INTENT")) {
                        pendingIntent3 = (PendingIntent)this.getIntent().getParcelableExtra("IN_APP_MESSAGE_INTENT");
                        this.c = (ResultReceiver)this.getIntent().getParcelableExtra("in_app_message_result_receiver");
                        n = 101;
                        break Label_0263;
                    }
                    n = 100;
                    pendingIntent3 = null;
                    break Label_0263;
                }
                n = 100;
            }
            try {
                this.e = true;
                this.startIntentSenderForResult(pendingIntent3.getIntentSender(), n, new Intent(), 0, 0, 0);
                return;
            }
            catch (final IntentSender$SendIntentException ex) {
                zzb.zzl("ProxyBillingActivity", "Got exception while trying to start a purchase flow.", (Throwable)ex);
                final ResultReceiver b = this.b;
                if (b != null) {
                    b.send(6, (Bundle)null);
                }
                else {
                    final ResultReceiver c = this.c;
                    if (c != null) {
                        c.send(0, (Bundle)null);
                    }
                    else {
                        final Intent l = this.l();
                        if (this.f) {
                            l.putExtra("IS_FIRST_PARTY_PURCHASE", true);
                        }
                        l.putExtra("RESPONSE_CODE", 6);
                        l.putExtra("DEBUG_MESSAGE", "An internal error occurred.");
                        ((Context)this).sendBroadcast(l);
                    }
                }
                this.e = false;
                this.finish();
                return;
            }
        }
        zzb.zzj("ProxyBillingActivity", "Launching Play Store billing flow from savedInstanceState");
        this.e = ((BaseBundle)bundle).getBoolean("send_cancelled_broadcast_if_finished", false);
        if (((BaseBundle)bundle).containsKey("result_receiver")) {
            this.b = (ResultReceiver)bundle.getParcelable("result_receiver");
        }
        else if (((BaseBundle)bundle).containsKey("in_app_message_result_receiver")) {
            this.c = (ResultReceiver)bundle.getParcelable("in_app_message_result_receiver");
        }
        else if (((BaseBundle)bundle).containsKey("alternative_billing_only_dialog_result_receiver")) {
            this.d = (ResultReceiver)bundle.getParcelable("alternative_billing_only_dialog_result_receiver");
        }
        this.f = ((BaseBundle)bundle).getBoolean("IS_FLOW_FROM_FIRST_PARTY_CLIENT", false);
    }
    
    public void onDestroy() {
        super.onDestroy();
        if (this.isFinishing()) {
            if (this.e) {
                final Intent l = this.l();
                l.putExtra("RESPONSE_CODE", 1);
                l.putExtra("DEBUG_MESSAGE", "Billing dialog closed.");
                ((Context)this).sendBroadcast(l);
            }
        }
    }
    
    @Override
    public void onSaveInstanceState(final Bundle bundle) {
        super.onSaveInstanceState(bundle);
        final ResultReceiver b = this.b;
        if (b != null) {
            bundle.putParcelable("result_receiver", (Parcelable)b);
        }
        final ResultReceiver c = this.c;
        if (c != null) {
            bundle.putParcelable("in_app_message_result_receiver", (Parcelable)c);
        }
        final ResultReceiver d = this.d;
        if (d != null) {
            bundle.putParcelable("alternative_billing_only_dialog_result_receiver", (Parcelable)d);
        }
        ((BaseBundle)bundle).putBoolean("send_cancelled_broadcast_if_finished", this.e);
        ((BaseBundle)bundle).putBoolean("IS_FLOW_FROM_FIRST_PARTY_CLIENT", this.f);
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.android.billingclient.api;

import com.google.android.gms.internal.play_billing.zzb;

public final class a
{
    public int a;
    public String b;
    
    public static a c() {
        return new a(null);
    }
    
    public String a() {
        return this.b;
    }
    
    public int b() {
        return this.a;
    }
    
    @Override
    public String toString() {
        final String zzh = zzb.zzh(this.a);
        final String b = this.b;
        final StringBuilder sb = new StringBuilder();
        sb.append("Response Code: ");
        sb.append(zzh);
        sb.append(", Debug Message: ");
        sb.append(b);
        return sb.toString();
    }
    
    public static class a
    {
        public int a;
        public String b = "";
        
        public com.android.billingclient.api.a a() {
            final com.android.billingclient.api.a a = new com.android.billingclient.api.a();
            com.android.billingclient.api.a.e(a, this.a);
            com.android.billingclient.api.a.d(a, this.b);
            return a;
        }
        
        public a b(final String b) {
            this.b = b;
            return this;
        }
        
        public a c(final int a) {
            this.a = a;
            return this;
        }
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.android.billingclient.api;

import android.os.BaseBundle;
import java.util.ArrayList;
import com.google.android.gms.internal.play_billing.zzb;
import android.os.Bundle;

public abstract class c
{
    public static se2 a(final Bundle bundle, final String s, final String s2) {
        final a j = b.j;
        if (bundle == null) {
            zzb.zzk("BillingClient", String.format("%s got null owned items list", s2));
            return new se2(j, 54);
        }
        final int zzb = com.google.android.gms.internal.play_billing.zzb.zzb(bundle, "BillingClient");
        final String zzg = com.google.android.gms.internal.play_billing.zzb.zzg(bundle, "BillingClient");
        final a.a c = a.c();
        c.c(zzb);
        c.b(zzg);
        final a a = c.a();
        if (zzb != 0) {
            com.google.android.gms.internal.play_billing.zzb.zzk("BillingClient", String.format("%s failed. Response code: %s", s2, zzb));
            return new se2(a, 23);
        }
        if (!((BaseBundle)bundle).containsKey("INAPP_PURCHASE_ITEM_LIST") || !((BaseBundle)bundle).containsKey("INAPP_PURCHASE_DATA_LIST") || !((BaseBundle)bundle).containsKey("INAPP_DATA_SIGNATURE_LIST")) {
            com.google.android.gms.internal.play_billing.zzb.zzk("BillingClient", String.format("Bundle returned from %s doesn't contain required fields.", s2));
            return new se2(j, 55);
        }
        final ArrayList stringArrayList = bundle.getStringArrayList("INAPP_PURCHASE_ITEM_LIST");
        final ArrayList stringArrayList2 = bundle.getStringArrayList("INAPP_PURCHASE_DATA_LIST");
        final ArrayList stringArrayList3 = bundle.getStringArrayList("INAPP_DATA_SIGNATURE_LIST");
        if (stringArrayList == null) {
            com.google.android.gms.internal.play_billing.zzb.zzk("BillingClient", String.format("Bundle returned from %s contains null SKUs list.", s2));
            return new se2(j, 56);
        }
        if (stringArrayList2 == null) {
            com.google.android.gms.internal.play_billing.zzb.zzk("BillingClient", String.format("Bundle returned from %s contains null purchases list.", s2));
            return new se2(j, 57);
        }
        if (stringArrayList3 == null) {
            com.google.android.gms.internal.play_billing.zzb.zzk("BillingClient", String.format("Bundle returned from %s contains null signatures list.", s2));
            return new se2(j, 58);
        }
        return new se2(b.l, 1);
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.android.volley;

import java.util.Collections;
import android.os.Handler;
import android.os.Looper;
import java.util.Iterator;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.Map;
import android.net.Uri;
import android.text.TextUtils;

public abstract class Request implements Comparable
{
    public final e.a a;
    public final int b;
    public final String c;
    public final int d;
    public final Object e;
    public d.a f;
    public Integer g;
    public ee1 h;
    public boolean i;
    public boolean j;
    public boolean k;
    public boolean l;
    public boolean m;
    public jf1 n;
    public a.a p;
    public b q;
    
    public Request(final int b, final String c, final d.a f) {
        e.a a;
        if (com.android.volley.e.a.c) {
            a = new e.a();
        }
        else {
            a = null;
        }
        this.a = a;
        this.e = new Object();
        this.i = true;
        this.j = false;
        this.k = false;
        this.l = false;
        this.m = false;
        this.p = null;
        this.b = b;
        this.c = c;
        this.f = f;
        this.L(new wq());
        this.d = h(c);
    }
    
    public static /* synthetic */ e.a a(final Request request) {
        return request.a;
    }
    
    public static int h(String host) {
        if (!TextUtils.isEmpty((CharSequence)host)) {
            final Uri parse = Uri.parse(host);
            if (parse != null) {
                host = parse.getHost();
                if (host != null) {
                    return host.hashCode();
                }
            }
        }
        return 0;
    }
    
    public boolean A() {
        synchronized (this.e) {
            return this.k;
        }
    }
    
    public boolean B() {
        synchronized (this.e) {
            return this.j;
        }
    }
    
    public void C() {
        synchronized (this.e) {
            this.k = true;
        }
    }
    
    public void D() {
        synchronized (this.e) {
            final b q = this.q;
            monitorexit(this.e);
            if (q != null) {
                q.a(this);
            }
        }
    }
    
    public void E(final d d) {
        synchronized (this.e) {
            final b q = this.q;
            monitorexit(this.e);
            if (q != null) {
                q.b(this, d);
            }
        }
    }
    
    public VolleyError F(final VolleyError volleyError) {
        return volleyError;
    }
    
    public abstract d G(final yy0 p0);
    
    public void H(final int n) {
        final ee1 h = this.h;
        if (h != null) {
            h.f(this, n);
        }
    }
    
    public Request I(final a.a p) {
        this.p = p;
        return this;
    }
    
    public void J(final b q) {
        synchronized (this.e) {
            this.q = q;
        }
    }
    
    public Request K(final ee1 h) {
        this.h = h;
        return this;
    }
    
    public Request L(final jf1 n) {
        this.n = n;
        return this;
    }
    
    public final Request M(final int i) {
        this.g = i;
        return this;
    }
    
    public final boolean N() {
        return this.i;
    }
    
    public final boolean O() {
        return this.m;
    }
    
    public final boolean P() {
        return this.l;
    }
    
    public void c(final String s) {
        if (com.android.volley.e.a.c) {
            this.a.a(s, Thread.currentThread().getId());
        }
    }
    
    public int d(final Request request) {
        final Priority v = this.v();
        final Priority v2 = request.v();
        int n;
        if (v == v2) {
            n = this.g - request.g;
        }
        else {
            n = v2.ordinal() - v.ordinal();
        }
        return n;
    }
    
    public void e(final VolleyError volleyError) {
        synchronized (this.e) {
            final d.a f = this.f;
            monitorexit(this.e);
            if (f != null) {
                f.a(volleyError);
            }
        }
    }
    
    public abstract void f(final Object p0);
    
    public final byte[] g(final Map map, final String s) {
        final StringBuilder sb = new StringBuilder();
        try {
            for (final Map.Entry<String, V> entry : map.entrySet()) {
                if (entry.getKey() == null || entry.getValue() == null) {
                    throw new IllegalArgumentException(String.format("Request#getParams() or Request#getPostParams() returned a map containing a null key or value: (%s, %s). All keys and values must be non-null.", entry.getKey(), entry.getValue()));
                }
                sb.append(URLEncoder.encode(entry.getKey(), s));
                sb.append('=');
                sb.append(URLEncoder.encode((String)entry.getValue(), s));
                sb.append('&');
            }
            return sb.toString().getBytes(s);
        }
        catch (final UnsupportedEncodingException cause) {
            final StringBuilder sb2 = new StringBuilder();
            sb2.append("Encoding not supported: ");
            sb2.append(s);
            throw new RuntimeException(sb2.toString(), cause);
        }
    }
    
    public void j(final String s) {
        final ee1 h = this.h;
        if (h != null) {
            h.c(this);
        }
        if (com.android.volley.e.a.c) {
            final long id = Thread.currentThread().getId();
            if (Looper.myLooper() != Looper.getMainLooper()) {
                new Handler(Looper.getMainLooper()).post((Runnable)new Runnable(this, s, id) {
                    public final String a;
                    public final long b;
                    public final Request c;
                    
                    @Override
                    public void run() {
                        Request.a(this.c).a(this.a, this.b);
                        Request.a(this.c).b(this.c.toString());
                    }
                });
                return;
            }
            this.a.a(s, id);
            this.a.b(this.toString());
        }
    }
    
    public byte[] k() {
        final Map q = this.q();
        if (q != null && q.size() > 0) {
            return this.g(q, this.r());
        }
        return null;
    }
    
    public String l() {
        final StringBuilder sb = new StringBuilder();
        sb.append("application/x-www-form-urlencoded; charset=");
        sb.append(this.r());
        return sb.toString();
    }
    
    public a.a m() {
        return this.p;
    }
    
    public String n() {
        final String z = this.z();
        final int p = this.p();
        String string = z;
        if (p != 0) {
            if (p == -1) {
                string = z;
            }
            else {
                final StringBuilder sb = new StringBuilder();
                sb.append(Integer.toString(p));
                sb.append('-');
                sb.append(z);
                string = sb.toString();
            }
        }
        return string;
    }
    
    public Map o() {
        return Collections.emptyMap();
    }
    
    public int p() {
        return this.b;
    }
    
    public Map q() {
        return null;
    }
    
    public String r() {
        return "UTF-8";
    }
    
    public byte[] s() {
        final Map t = this.t();
        if (t != null && t.size() > 0) {
            return this.g(t, this.u());
        }
        return null;
    }
    
    public Map t() {
        return this.q();
    }
    
    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append("0x");
        sb.append(Integer.toHexString(this.y()));
        final String string = sb.toString();
        final StringBuilder sb2 = new StringBuilder();
        String str;
        if (this.B()) {
            str = "[X] ";
        }
        else {
            str = "[ ] ";
        }
        sb2.append(str);
        sb2.append(this.z());
        sb2.append(" ");
        sb2.append(string);
        sb2.append(" ");
        sb2.append(this.v());
        sb2.append(" ");
        sb2.append(this.g);
        return sb2.toString();
    }
    
    public String u() {
        return this.r();
    }
    
    public Priority v() {
        return Priority.NORMAL;
    }
    
    public jf1 w() {
        return this.n;
    }
    
    public final int x() {
        return this.w().c();
    }
    
    public int y() {
        return this.d;
    }
    
    public String z() {
        return this.c;
    }
    
    public enum Priority
    {
        private static final Priority[] $VALUES;
        
        HIGH, 
        IMMEDIATE, 
        LOW, 
        NORMAL;
    }
    
    public interface b
    {
        void a(final Request p0);
        
        void b(final Request p0, final d p1);
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.android.volley;

public class NetworkError extends VolleyError
{
    public NetworkError() {
    }
    
    public NetworkError(final Throwable t) {
        super(t);
    }
    
    public NetworkError(final yy0 yy0) {
        super(yy0);
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.android.volley;

import java.util.Iterator;
import android.os.SystemClock;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import android.util.Log;

public abstract class e
{
    public static String a = "Volley";
    public static boolean b;
    public static final String c;
    
    static {
        e.b = Log.isLoggable("Volley", 2);
        c = e.class.getName();
    }
    
    public static String a(String format, final Object... args) {
        if (args != null) {
            format = String.format(Locale.US, format, args);
        }
        final StackTraceElement[] stackTrace = new Throwable().fillInStackTrace().getStackTrace();
        for (int i = 2; i < stackTrace.length; ++i) {
            if (!stackTrace[i].getClassName().equals(e.c)) {
                final String className = stackTrace[i].getClassName();
                final String substring = className.substring(className.lastIndexOf(46) + 1);
                final String substring2 = substring.substring(substring.lastIndexOf(36) + 1);
                final StringBuilder sb = new StringBuilder();
                sb.append(substring2);
                sb.append(".");
                sb.append(stackTrace[i].getMethodName());
                final String string = sb.toString();
                return String.format(Locale.US, "[%d] %s: %s", Thread.currentThread().getId(), string, format);
            }
        }
        final String string = "<unknown>";
        return String.format(Locale.US, "[%d] %s: %s", Thread.currentThread().getId(), string, format);
    }
    
    public static void b(final String s, final Object... array) {
        Log.d(e.a, a(s, array));
    }
    
    public static void c(final String s, final Object... array) {
        Log.e(e.a, a(s, array));
    }
    
    public static void d(final Throwable t, final String s, final Object... array) {
        Log.e(e.a, a(s, array), t);
    }
    
    public static void e(final String s, final Object... array) {
        if (e.b) {
            Log.v(e.a, a(s, array));
        }
    }
    
    public static class a
    {
        public static final boolean c;
        public final List a;
        public boolean b;
        
        static {
            c = e.b;
        }
        
        public a() {
            this.a = new ArrayList();
            this.b = false;
        }
        
        public void a(final String s, final long n) {
            synchronized (this) {
                if (!this.b) {
                    this.a.add(new e.a.a(s, n, SystemClock.elapsedRealtime()));
                    return;
                }
                throw new IllegalStateException("Marker added to finished log");
            }
        }
        
        public void b(final String s) {
            synchronized (this) {
                this.b = true;
                final long c = this.c();
                if (c <= 0L) {
                    return;
                }
                long c2 = this.a.get(0).c;
                e.b("(%-4d ms) %s", c, s);
                for (final e.a.a a : this.a) {
                    final long c3 = a.c;
                    e.b("(+%-4d) [%2d] %s", c3 - c2, a.b, a.a);
                    c2 = c3;
                }
            }
        }
        
        public final long c() {
            if (this.a.size() == 0) {
                return 0L;
            }
            final long c = this.a.get(0).c;
            final List a = this.a;
            return ((e.a.a)a.get(a.size() - 1)).c - c;
        }
        
        public void finalize() {
            if (!this.b) {
                this.b("Request on the loose");
                e.c("Marker log finalized without finish() - uncaught exit point for request", new Object[0]);
            }
        }
        
        public static class a
        {
            public final String a;
            public final long b;
            public final long c;
            
            public a(final String a, final long b, final long c) {
                this.a = a;
                this.b = b;
                this.c = c;
            }
        }
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.android.volley;

import java.util.Collections;
import java.util.List;
import java.util.Map;

public interface a
{
    void a(final String p0, final boolean p1);
    
    void b(final String p0, final a p1);
    
    void clear();
    
    a get(final String p0);
    
    void initialize();
    
    public static class a
    {
        public byte[] a;
        public String b;
        public long c;
        public long d;
        public long e;
        public long f;
        public Map g;
        public List h;
        
        public a() {
            this.g = Collections.emptyMap();
        }
        
        public boolean a() {
            return this.b(System.currentTimeMillis());
        }
        
        public boolean b(final long n) {
            return this.e < n;
        }
        
        public boolean c(final long n) {
            return this.f < n;
        }
    }
}

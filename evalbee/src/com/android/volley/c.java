// 
// Decompiled by Procyon v0.6.0
// 

package com.android.volley;

import android.os.Process;
import android.os.SystemClock;
import android.net.TrafficStats;
import java.util.concurrent.BlockingQueue;

public class c extends Thread
{
    public final BlockingQueue a;
    public final qy0 b;
    public final a c;
    public final re1 d;
    public volatile boolean e;
    
    public c(final BlockingQueue a, final qy0 b, final a c, final re1 d) {
        this.e = false;
        this.a = a;
        this.b = b;
        this.c = c;
        this.d = d;
    }
    
    private void c() {
        this.d(this.a.take());
    }
    
    public final void a(final Request request) {
        TrafficStats.setThreadStatsTag(request.y());
    }
    
    public final void b(final Request request, VolleyError f) {
        f = request.F(f);
        this.d.c(request, f);
    }
    
    public void d(final Request request) {
        final long elapsedRealtime = SystemClock.elapsedRealtime();
        request.H(3);
        try {
            try {
                request.c("network-queue-take");
                if (request.B()) {
                    request.j("network-discard-cancelled");
                    request.D();
                    request.H(4);
                    return;
                }
                this.a(request);
                final yy0 a = this.b.a(request);
                request.c("network-http-complete");
                if (a.e && request.A()) {
                    request.j("not-modified");
                    request.D();
                    request.H(4);
                    return;
                }
                final d g = request.G(a);
                request.c("network-parse-complete");
                if (request.N() && g.b != null) {
                    this.c.b(request.n(), g.b);
                    request.c("network-cache-written");
                }
                request.C();
                this.d.a(request, g);
                request.E(g);
            }
            finally {}
        }
        catch (final Exception ex) {
            com.android.volley.e.d(ex, "Unhandled exception %s", ex.toString());
            final VolleyError volleyError = new VolleyError(ex);
            volleyError.setNetworkTimeMs(SystemClock.elapsedRealtime() - elapsedRealtime);
            this.d.c(request, volleyError);
        }
        catch (final VolleyError volleyError2) {
            volleyError2.setNetworkTimeMs(SystemClock.elapsedRealtime() - elapsedRealtime);
            this.b(request, volleyError2);
            goto Label_0230;
        }
        request.H(4);
        return;
        request.H(4);
    }
    
    public void e() {
        this.e = true;
        this.interrupt();
    }
    
    @Override
    public void run() {
        Process.setThreadPriority(10);
    Label_0005_Outer:
        while (true) {
            while (true) {
                try {
                    while (true) {
                        this.c();
                    }
                }
                catch (final InterruptedException ex) {
                    if (this.e) {
                        Thread.currentThread().interrupt();
                        return;
                    }
                    com.android.volley.e.c("Ignoring spurious interrupt of NetworkDispatcher thread; use quit() to terminate it", new Object[0]);
                    continue Label_0005_Outer;
                }
                continue;
            }
        }
    }
}

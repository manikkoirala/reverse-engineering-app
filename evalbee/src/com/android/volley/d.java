// 
// Decompiled by Procyon v0.6.0
// 

package com.android.volley;

public class d
{
    public final Object a;
    public final com.android.volley.a.a b;
    public final VolleyError c;
    public boolean d;
    
    public d(final VolleyError c) {
        this.d = false;
        this.a = null;
        this.b = null;
        this.c = c;
    }
    
    public d(final Object a, final com.android.volley.a.a b) {
        this.d = false;
        this.a = a;
        this.b = b;
        this.c = null;
    }
    
    public static d a(final VolleyError volleyError) {
        return new d(volleyError);
    }
    
    public static d c(final Object o, final com.android.volley.a.a a) {
        return new d(o, a);
    }
    
    public boolean b() {
        return this.c == null;
    }
    
    public interface a
    {
        void a(final VolleyError p0);
    }
    
    public interface b
    {
        void a(final Object p0);
    }
}

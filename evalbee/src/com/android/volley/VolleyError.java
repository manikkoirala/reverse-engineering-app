// 
// Decompiled by Procyon v0.6.0
// 

package com.android.volley;

public class VolleyError extends Exception
{
    public final yy0 networkResponse;
    private long networkTimeMs;
    
    public VolleyError() {
        this.networkResponse = null;
    }
    
    public VolleyError(final String message) {
        super(message);
        this.networkResponse = null;
    }
    
    public VolleyError(final String message, final Throwable cause) {
        super(message, cause);
        this.networkResponse = null;
    }
    
    public VolleyError(final Throwable cause) {
        super(cause);
        this.networkResponse = null;
    }
    
    public VolleyError(final yy0 networkResponse) {
        this.networkResponse = networkResponse;
    }
    
    public long getNetworkTimeMs() {
        return this.networkTimeMs;
    }
    
    public void setNetworkTimeMs(final long networkTimeMs) {
        this.networkTimeMs = networkTimeMs;
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.android.volley;

public class ParseError extends VolleyError
{
    public ParseError() {
    }
    
    public ParseError(final Throwable t) {
        super(t);
    }
    
    public ParseError(final yy0 yy0) {
        super(yy0);
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.android.volley;

import android.os.Process;
import java.util.concurrent.BlockingQueue;

public class b extends Thread
{
    public static final boolean g;
    public final BlockingQueue a;
    public final BlockingQueue b;
    public final a c;
    public final re1 d;
    public volatile boolean e;
    public final f f;
    
    static {
        g = e.b;
    }
    
    public b(final BlockingQueue a, final BlockingQueue b, final a c, final re1 d) {
        this.e = false;
        this.a = a;
        this.b = b;
        this.c = c;
        this.d = d;
        this.f = new f(this, b, d);
    }
    
    public static /* synthetic */ BlockingQueue a(final b b) {
        return b.b;
    }
    
    private void b() {
        this.c(this.a.take());
    }
    
    public void c(final Request request) {
        request.c("cache-queue-take");
        request.H(1);
        try {
            if (request.B()) {
                request.j("cache-discard-canceled");
                return;
            }
            final a.a value = this.c.get(request.n());
            if (value == null) {
                request.c("cache-miss");
                if (!this.f.c(request)) {
                    this.b.put(request);
                }
                return;
            }
            final long currentTimeMillis = System.currentTimeMillis();
            if (value.b(currentTimeMillis)) {
                request.c("cache-hit-expired");
                request.I(value);
                if (!this.f.c(request)) {
                    this.b.put(request);
                }
                return;
            }
            request.c("cache-hit");
            final d g = request.G(new yy0(value.a, value.g));
            request.c("cache-hit-parsed");
            if (!g.b()) {
                request.c("cache-parsing-failed");
                this.c.a(request.n(), true);
                request.I(null);
                if (!this.f.c(request)) {
                    this.b.put(request);
                }
                return;
            }
            re1 re1;
            if (!value.c(currentTimeMillis)) {
                re1 = this.d;
            }
            else {
                request.c("cache-hit-refresh-needed");
                request.I(value);
                g.d = true;
                if (!this.f.c(request)) {
                    this.d.b(request, g, new Runnable(this, request) {
                        public final Request a;
                        public final b b;
                        
                        @Override
                        public void run() {
                            try {
                                com.android.volley.b.a(this.b).put(this.a);
                            }
                            catch (final InterruptedException ex) {
                                Thread.currentThread().interrupt();
                            }
                        }
                    });
                    return;
                }
                re1 = this.d;
            }
            re1.a(request, g);
        }
        finally {
            request.H(2);
        }
    }
    
    public void d() {
        this.e = true;
        this.interrupt();
    }
    
    @Override
    public void run() {
        if (com.android.volley.b.g) {
            com.android.volley.e.e("start new dispatcher", new Object[0]);
        }
        Process.setThreadPriority(10);
        this.c.initialize();
    Label_0029_Outer:
        while (true) {
            while (true) {
                try {
                    while (true) {
                        this.b();
                    }
                }
                catch (final InterruptedException ex) {
                    if (this.e) {
                        Thread.currentThread().interrupt();
                        return;
                    }
                    com.android.volley.e.c("Ignoring spurious interrupt of CacheDispatcher thread; use quit() to terminate it", new Object[0]);
                    continue Label_0029_Outer;
                }
                continue;
            }
        }
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.android.volley;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.HashMap;
import java.util.concurrent.BlockingQueue;
import java.util.Map;

public class f implements b
{
    public final Map a;
    public final re1 b;
    public final ee1 c;
    public final com.android.volley.b d;
    public final BlockingQueue e;
    
    public f(final com.android.volley.b d, final BlockingQueue e, final re1 b) {
        this.a = new HashMap();
        this.c = null;
        this.b = b;
        this.d = d;
        this.e = e;
    }
    
    @Override
    public void a(Request request) {
        synchronized (this) {
            final String n = request.n();
            final List list = this.a.remove(n);
            if (list != null && !list.isEmpty()) {
                if (com.android.volley.e.b) {
                    com.android.volley.e.e("%d waiting requests for cacheKey=%s; resend to network", list.size(), n);
                }
                request = (Request)list.remove(0);
                this.a.put(n, list);
                request.J((Request.b)this);
                final ee1 c = this.c;
                if (c != null) {
                    c.g(request);
                }
                else if (this.d != null) {
                    final BlockingQueue e = this.e;
                    if (e != null) {
                        try {
                            e.put(request);
                        }
                        catch (final InterruptedException ex) {
                            com.android.volley.e.c("Couldn't add request to queue. %s", ex.toString());
                            Thread.currentThread().interrupt();
                            this.d.d();
                        }
                    }
                }
            }
        }
    }
    
    @Override
    public void b(Request request, final d d) {
        final a.a b = d.b;
        if (b != null) {
            if (!b.a()) {
                final String n = request.n();
                synchronized (this) {
                    final List list = this.a.remove(n);
                    monitorexit(this);
                    if (list != null) {
                        if (com.android.volley.e.b) {
                            com.android.volley.e.e("Releasing %d waiting requests for cacheKey=%s.", list.size(), n);
                        }
                        final Iterator iterator = list.iterator();
                        while (iterator.hasNext()) {
                            request = (Request)iterator.next();
                            this.b.a(request, d);
                        }
                    }
                    return;
                }
            }
        }
        this.a(request);
    }
    
    public boolean c(final Request request) {
        synchronized (this) {
            final String n = request.n();
            if (this.a.containsKey(n)) {
                List list;
                if ((list = this.a.get(n)) == null) {
                    list = new ArrayList();
                }
                request.c("waiting-for-response");
                list.add(request);
                this.a.put(n, list);
                if (com.android.volley.e.b) {
                    com.android.volley.e.b("Request for cacheKey=%s is in flight, putting on hold.", n);
                }
                return true;
            }
            this.a.put(n, null);
            request.J((Request.b)this);
            if (com.android.volley.e.b) {
                com.android.volley.e.b("new request, sending to network %s", n);
            }
            return false;
        }
    }
}

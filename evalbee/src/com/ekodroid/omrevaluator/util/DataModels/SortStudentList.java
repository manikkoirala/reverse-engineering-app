// 
// Decompiled by Procyon v0.6.0
// 

package com.ekodroid.omrevaluator.util.DataModels;

import java.util.List;
import java.util.Collections;
import com.ekodroid.omrevaluator.database.StudentDataModel;
import java.util.Comparator;
import java.util.ArrayList;

public class SortStudentList
{
    public final void a(final ArrayList list, final boolean b) {
        Collections.sort((List<Object>)list, new Comparator(this, b) {
            public final boolean a;
            public final SortStudentList b;
            
            public int a(final StudentDataModel studentDataModel, final StudentDataModel studentDataModel2) {
                if (this.a) {
                    return studentDataModel.getStudentName().compareTo(studentDataModel2.getStudentName());
                }
                return studentDataModel2.getStudentName().compareTo(studentDataModel.getStudentName());
            }
        });
    }
    
    public final void b(final ArrayList list, final boolean b) {
        Collections.sort((List<Object>)list, new Comparator(this, b) {
            public final boolean a;
            public final SortStudentList b;
            
            public int a(final StudentDataModel studentDataModel, final StudentDataModel studentDataModel2) {
                if (this.a) {
                    return Integer.compare(studentDataModel.getRollNO(), studentDataModel2.getRollNO());
                }
                return Integer.compare(studentDataModel2.getRollNO(), studentDataModel.getRollNO());
            }
        });
    }
    
    public void c(final SortBy sortBy, final ArrayList list, final boolean b) {
        if (SortStudentList$c.a[sortBy.ordinal()] != 1) {
            this.b(list, b);
        }
        else {
            this.a(list, b);
        }
    }
    
    public enum SortBy
    {
        private static final SortBy[] $VALUES;
        
        NAME, 
        ROLLNO;
        
        private static /* synthetic */ SortBy[] $values() {
            return new SortBy[] { SortBy.ROLLNO, SortBy.NAME };
        }
        
        static {
            $VALUES = $values();
        }
    }
}

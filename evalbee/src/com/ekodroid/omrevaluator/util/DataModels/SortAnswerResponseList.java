// 
// Decompiled by Procyon v0.6.0
// 

package com.ekodroid.omrevaluator.util.DataModels;

import java.util.List;
import java.util.Collections;
import java.util.Comparator;
import java.util.ArrayList;

public class SortAnswerResponseList
{
    public final void a(final ArrayList list, final boolean b) {
        Collections.sort((List<Object>)list, new Comparator(this, b) {
            public final boolean a;
            public final SortAnswerResponseList b;
            
            public int a(final h5 h5, final h5 h6) {
                if (this.a) {
                    return Integer.compare(h5.a(), h6.a());
                }
                return Integer.compare(h6.a(), h5.a());
            }
        });
    }
    
    public final void b(final ArrayList list, final boolean b) {
        Collections.sort((List<Object>)list, new Comparator(this, b) {
            public final boolean a;
            public final SortAnswerResponseList b;
            
            public int a(final h5 h5, final h5 h6) {
                if (this.a) {
                    return Integer.compare(h5.b(), h6.b());
                }
                return Integer.compare(h6.b(), h5.b());
            }
        });
    }
    
    public final void c(final ArrayList list, final boolean b) {
        Collections.sort((List<Object>)list, new Comparator(this, b) {
            public final boolean a;
            public final SortAnswerResponseList b;
            
            public int a(final h5 h5, final h5 h6) {
                if (this.a) {
                    return Integer.compare(h5.c(), h6.c());
                }
                return Integer.compare(h6.c(), h5.c());
            }
        });
    }
    
    public final void d(final ArrayList list, final boolean b) {
        Collections.sort((List<Object>)list, new Comparator(this, b) {
            public final boolean a;
            public final SortAnswerResponseList b;
            
            public int a(final h5 h5, final h5 h6) {
                if (this.a) {
                    return Integer.compare(h5.d(), h6.d());
                }
                return Integer.compare(h6.d(), h5.d());
            }
        });
    }
    
    public void e(final AnswerResponseSortBy answerResponseSortBy, final ArrayList list, final boolean b) {
        final int n = SortAnswerResponseList$e.a[answerResponseSortBy.ordinal()];
        if (n != 1) {
            if (n == 2) {
                this.a(list, b);
                return;
            }
            if (n == 3) {
                this.b(list, b);
                return;
            }
            if (n == 4) {
                this.d(list, b);
                return;
            }
        }
        this.c(list, b);
    }
    
    public enum AnswerResponseSortBy
    {
        private static final AnswerResponseSortBy[] $VALUES;
        
        CORRECT, 
        INCORRECT, 
        QUESTION, 
        UNATTEMPTED;
        
        private static /* synthetic */ AnswerResponseSortBy[] $values() {
            return new AnswerResponseSortBy[] { AnswerResponseSortBy.QUESTION, AnswerResponseSortBy.CORRECT, AnswerResponseSortBy.INCORRECT, AnswerResponseSortBy.UNATTEMPTED };
        }
        
        static {
            $VALUES = $values();
        }
    }
}

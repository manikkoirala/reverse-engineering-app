// 
// Decompiled by Procyon v0.6.0
// 

package com.ekodroid.omrevaluator.util.DataModels;

import java.util.List;
import java.util.Collections;
import java.util.Comparator;
import java.util.ArrayList;

public class SortArchiveExamList
{
    public final void a(final ArrayList list, final boolean b) {
        Collections.sort((List<Object>)list, new Comparator(this, b) {
            public final boolean a;
            public final SortArchiveExamList b;
            
            public int a(final c8 c8, final c8 c9) {
                if (this.a) {
                    return c8.b().compareTo(c9.b());
                }
                return c9.b().compareTo(c8.b());
            }
        });
    }
    
    public final void b(final ArrayList list, final boolean b) {
        Collections.sort((List<Object>)list, new Comparator(this, b) {
            public final boolean a;
            public final SortArchiveExamList b;
            
            public int a(final c8 c8, final c8 c9) {
                if (this.a) {
                    return c8.c().compareTo(c9.c());
                }
                return c9.c().compareTo(c8.c());
            }
        });
    }
    
    public final void c(final ArrayList list, final boolean b) {
        Collections.sort((List<Object>)list, new Comparator(this, b) {
            public final boolean a;
            public final SortArchiveExamList b;
            
            public int a(final c8 c8, final c8 c9) {
                if (this.a) {
                    return c8.d().compareTo(c9.d());
                }
                return c9.d().compareTo(c8.d());
            }
        });
    }
    
    public void d(final SortBy sortBy, final ArrayList list, final boolean b) {
        final int n = SortArchiveExamList$d.a[sortBy.ordinal()];
        if (n != 1) {
            if (n != 2 && n == 3) {
                this.a(list, b);
            }
            else {
                this.b(list, b);
            }
        }
        else {
            this.c(list, b);
        }
    }
    
    public enum SortBy
    {
        private static final SortBy[] $VALUES;
        
        CLASS, 
        DATE, 
        NAME;
        
        private static /* synthetic */ SortBy[] $values() {
            return new SortBy[] { SortBy.DATE, SortBy.NAME, SortBy.CLASS };
        }
        
        static {
            $VALUES = $values();
        }
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.ekodroid.omrevaluator.util.DataModels;

import com.ekodroid.omrevaluator.database.ResultDataJsonModel;
import java.util.List;
import java.util.Collections;
import java.util.Comparator;
import java.util.ArrayList;

public class SortResultList
{
    public void a(final ResultSortBy resultSortBy, final ArrayList list, final boolean b) {
        final int n = SortResultList$a.a[resultSortBy.ordinal()];
        if (n != 1) {
            if (n != 2) {
                if (n == 3) {
                    this.b(list, b);
                    return;
                }
                if (n == 4) {
                    this.d(list, b);
                    return;
                }
            }
            this.e(list, b);
        }
        else {
            this.c(list, b);
        }
    }
    
    public final void b(final ArrayList list, final boolean b) {
        Collections.sort((List<Object>)list, new Comparator(this, b) {
            public final boolean a;
            public final SortResultList b;
            
            public int a(final d8 d8, final d8 d9) {
                double d10;
                ye1 ye1;
                if (this.a) {
                    d10 = d8.b().d();
                    ye1 = d9.b();
                }
                else {
                    d10 = d9.b().d();
                    ye1 = d8.b();
                }
                return Double.compare(d10, ye1.d());
            }
        });
    }
    
    public final void c(final ArrayList list, final boolean b) {
        Collections.sort((List<Object>)list, new Comparator(this, b) {
            public final boolean a;
            public final SortResultList b;
            
            public int a(final d8 d8, final d8 d9) {
                if (this.a) {
                    return d8.b().e().compareTo(d9.b().e());
                }
                return d9.b().e().compareTo(d8.b().e());
            }
        });
    }
    
    public final void d(final ArrayList list, final boolean b) {
        Collections.sort((List<Object>)list, new Comparator(this, b) {
            public final boolean a;
            public final SortResultList b;
            
            public int a(final d8 d8, final d8 d9) {
                if (this.a) {
                    return Integer.compare(d8.b().f(), d9.b().f());
                }
                return Integer.compare(d9.b().f(), d8.b().f());
            }
        });
    }
    
    public final void e(final ArrayList list, final boolean b) {
        Collections.sort((List<Object>)list, new Comparator(this, b) {
            public final boolean a;
            public final SortResultList b;
            
            public int a(final d8 d8, final d8 d9) {
                if (this.a) {
                    return Integer.compare(d8.b().g(), d9.b().g());
                }
                return Integer.compare(d9.b().g(), d8.b().g());
            }
        });
    }
    
    public final void f(final ArrayList list, final boolean b) {
        Collections.sort((List<Object>)list, new Comparator(this, b) {
            public final boolean a;
            public final SortResultList b;
            
            public int a(final ye1 ye1, final ye1 ye2) {
                double d1;
                double d2;
                if (this.a) {
                    d1 = ye1.d();
                    d2 = ye2.d();
                }
                else {
                    d1 = ye2.d();
                    d2 = ye1.d();
                }
                return Double.compare(d1, d2);
            }
        });
    }
    
    public final void g(final ArrayList list, final boolean b) {
        Collections.sort((List<Object>)list, new Comparator(this, b) {
            public final boolean a;
            public final SortResultList b;
            
            public int a(final ye1 ye1, final ye1 ye2) {
                if (this.a) {
                    return ye1.e().compareTo(ye2.e());
                }
                return ye2.e().compareTo(ye1.e());
            }
        });
    }
    
    public final void h(final ArrayList list, final boolean b) {
        Collections.sort((List<Object>)list, new Comparator(this, b) {
            public final boolean a;
            public final SortResultList b;
            
            public int a(final ye1 ye1, final ye1 ye2) {
                if (this.a) {
                    return Integer.compare(ye1.f(), ye2.f());
                }
                return Integer.compare(ye2.f(), ye1.f());
            }
        });
    }
    
    public final void i(final ArrayList list, final boolean b) {
        Collections.sort((List<Object>)list, new Comparator(this, b) {
            public final boolean a;
            public final SortResultList b;
            
            public int a(final ye1 ye1, final ye1 ye2) {
                if (this.a) {
                    return Integer.compare(ye1.g(), ye2.g());
                }
                return Integer.compare(ye2.g(), ye1.g());
            }
        });
    }
    
    public void j(final ResultSortBy resultSortBy, final ArrayList list, final boolean b) {
        final int n = SortResultList$a.a[resultSortBy.ordinal()];
        if (n != 1) {
            if (n != 2) {
                if (n == 3) {
                    this.f(list, b);
                    return;
                }
                if (n == 4) {
                    this.h(list, b);
                    return;
                }
            }
            this.i(list, b);
        }
        else {
            this.g(list, b);
        }
    }
    
    public void k(final ArrayList list, final boolean b) {
        Collections.sort((List<Object>)list, new Comparator(this, b) {
            public final boolean a;
            public final SortResultList b;
            
            public int a(final ResultDataJsonModel resultDataJsonModel, final ResultDataJsonModel resultDataJsonModel2) {
                if (this.a) {
                    return Integer.compare(resultDataJsonModel.getRollNo(), resultDataJsonModel2.getRollNo());
                }
                return Integer.compare(resultDataJsonModel2.getRollNo(), resultDataJsonModel.getRollNo());
            }
        });
    }
    
    public enum ResultSortBy
    {
        private static final ResultSortBy[] $VALUES;
        
        MARK, 
        NAME, 
        RANK, 
        ROLLNO;
        
        private static /* synthetic */ ResultSortBy[] $values() {
            return new ResultSortBy[] { ResultSortBy.MARK, ResultSortBy.RANK, ResultSortBy.ROLLNO, ResultSortBy.NAME };
        }
        
        static {
            $VALUES = $values();
        }
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.ekodroid.omrevaluator.more;

import android.app.Dialog;
import com.ekodroid.omrevaluator.clients.UserProfileClients.models.UserProfileDto;
import androidx.appcompat.app.a;
import android.widget.EditText;
import android.widget.Button;
import android.content.DialogInterface;
import android.content.DialogInterface$OnClickListener;
import com.google.android.material.textfield.TextInputLayout;
import android.view.ViewGroup;
import android.view.LayoutInflater;
import com.google.android.material.dialog.MaterialAlertDialogBuilder;
import java.util.Iterator;
import android.view.View$OnClickListener;
import android.widget.TextView;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView$OnItemClickListener;
import android.widget.ListAdapter;
import java.util.Comparator;
import com.google.firebase.auth.FirebaseAuth;
import android.os.Bundle;
import com.google.firebase.analytics.FirebaseAnalytics;
import android.app.ProgressDialog;
import com.ekodroid.omrevaluator.serializable.ResponseModel.PurchaseAccount;
import com.ekodroid.omrevaluator.clients.UserProfileClients.models.UserRole;
import com.ekodroid.omrevaluator.clients.UserProfileClients.models.OrgProfile;
import com.ekodroid.omrevaluator.more.models.Teacher;
import androidx.appcompat.widget.Toolbar;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import java.util.ArrayList;
import android.content.Context;
import android.widget.ListView;

public class TeachersActivity extends v5
{
    public ListView c;
    public Context d;
    public ArrayList e;
    public SwipeRefreshLayout f;
    public Toolbar g;
    
    public TeachersActivity() {
        this.d = (Context)this;
        this.e = new ArrayList();
    }
    
    public final void L(final OrgProfile orgProfile) {
        final PurchaseAccount r = a91.r(this.d);
        if (r == null) {
            return;
        }
        int teachersCount;
        if ((teachersCount = r.getTeachersCount()) == 0) {
            teachersCount = 1;
        }
        if (teachersCount < orgProfile.getTeacherList().size() && orgProfile.getRole() == UserRole.OWNER) {
            this.R(orgProfile);
        }
    }
    
    public final void M() {
        this.f.setRefreshing(true);
        new pa0(this.d, new zg(this) {
            public final TeachersActivity a;
            
            @Override
            public void a(final boolean b, final int n, final Object o) {
                this.a.f.setRefreshing(false);
                this.a.Q();
                final OrgProfile instance = OrgProfile.getInstance(this.a.d);
                if (instance == null) {
                    return;
                }
                if (instance.getRequest() != null) {
                    new mg0(this.a.d, new y01(this) {
                        public final TeachersActivity$l a;
                        
                        @Override
                        public void a(final Object o) {
                            this.a.a.M();
                        }
                    });
                }
                else {
                    if (instance.getDisplayName() != null) {
                        if (instance.getDisplayName().length() != 0) {
                            if (instance.getRole() != UserRole.OWNER || (instance.getOrganization() != null && instance.getOrganization().length() != 0)) {
                                return;
                            }
                        }
                    }
                    this.a.Z();
                }
            }
        });
    }
    
    public final void N(final String s) {
        final ProgressDialog progressDialog = new ProgressDialog(this.d);
        progressDialog.setMessage((CharSequence)((Context)this).getString(2131886803));
        ((Dialog)progressDialog).show();
        new e61(new Teacher(UserRole.TEACHER, s), this.d, new zg(this, progressDialog) {
            public final ProgressDialog a;
            public final TeachersActivity b;
            
            @Override
            public void a(final boolean b, final int n, final Object o) {
                ((Dialog)this.a).dismiss();
                if (b) {
                    final Teacher e = (Teacher)o;
                    final OrgProfile instance = OrgProfile.getInstance(this.b.d);
                    instance.getTeacherList().add(e);
                    instance.save(this.b.d);
                    this.b.Q();
                    final StringBuilder sb = new StringBuilder();
                    sb.append(((Context)this.b).getString(2131886544));
                    sb.append(" ");
                    sb.append(e.getEmailId());
                    sb.append(((Context)this.b).getString(2131886445));
                    sb.append(" ");
                    sb.append(instance.getOrganization());
                    sb.append(" ");
                    sb.append(((Context)this.b).getString(2131886109));
                    xs.d(this.b.d, null, 2131886368, sb.toString(), 2131886176, 0, 0, 0);
                    FirebaseAnalytics.getInstance(this.b.d).a("ADD_TEACHER_SUCCESS", null);
                }
                else {
                    FirebaseAnalytics.getInstance(this.b.d).a("ADD_TEACHER_FAIL", null);
                    final TeachersActivity b2 = this.b;
                    a91.H(b2.d, ((Context)b2).getString(2131886580), 2131230909, 2131231086);
                }
            }
        }).f();
    }
    
    public final void O() {
        final ProgressDialog progressDialog = new ProgressDialog(this.d);
        progressDialog.setMessage((CharSequence)"Please wait, exiting organization account...");
        ((Dialog)progressDialog).show();
        FirebaseAnalytics.getInstance(this.d).a("TEACHER_LEFT", null);
        new h61(this.d, new zg(this, progressDialog) {
            public final ProgressDialog a;
            public final TeachersActivity b;
            
            @Override
            public void a(final boolean b, final int n, final Object o) {
                ((Dialog)this.a).dismiss();
                if (b) {
                    OrgProfile.reset(this.b.d);
                    this.b.M();
                }
                else {
                    a91.H(this.b.d, "Update failed, please try again", 2131230909, 2131231086);
                }
            }
        }).f();
    }
    
    public final void P() {
        final OrgProfile instance = OrgProfile.getInstance(this.d);
        if (instance == null) {
            return;
        }
        if (instance.getRole() == UserRole.OWNER) {
            final PurchaseAccount r = a91.r(this.d);
            if (r != null && instance.getTeacherList().size() >= r.getTeachersCount()) {
                xs.c(this.d, null, 0, 2131886460, 2131886176, 0, 0, 0);
            }
            else {
                this.W();
            }
        }
        else {
            xs.c(this.d, null, 0, 2131886543, 2131886176, 0, 0, 0);
        }
    }
    
    public final void Q() {
        final OrgProfile instance = OrgProfile.getInstance(this.d);
        if (instance == null) {
            return;
        }
        this.e = new ArrayList();
        final ArrayList<Teacher> teacherList = instance.getTeacherList();
        final r30 e = FirebaseAuth.getInstance().e();
        for (final Teacher teacher : teacherList) {
            if (e.getEmail().equals(teacher.getEmailId())) {
                this.e.add(new tu1(true, teacher));
            }
            else {
                this.e.add(new tu1(false, teacher));
            }
        }
        this.e.sort(new Comparator(this) {
            public final TeachersActivity a;
            
            public int a(final tu1 tu1, final tu1 tu2) {
                return Boolean.compare(tu2.a(), tu1.a());
            }
        });
        this.c.setAdapter((ListAdapter)new r3(this.e, this.d));
        ((AdapterView)this.c).setOnItemClickListener((AdapterView$OnItemClickListener)new AdapterView$OnItemClickListener(this, instance) {
            public final OrgProfile a;
            public final TeachersActivity b;
            
            public void onItemClick(final AdapterView adapterView, final View view, final int index, long n) {
                n = view.getId();
                final tu1 tu1 = this.b.e.get(index);
                if (n == 2131296657L) {
                    this.b.Z();
                    return;
                }
                if (this.a.getRole() == UserRole.OWNER && index > 0) {
                    this.b.V(tu1);
                }
            }
        });
        ((TextView)this.findViewById(2131297379)).setText((CharSequence)instance.getOrganization());
        if (instance.getRole() == UserRole.OWNER) {
            this.findViewById(2131296692).setVisibility(8);
        }
        else {
            this.findViewById(2131296692).setVisibility(0);
            this.findViewById(2131296692).setOnClickListener((View$OnClickListener)new View$OnClickListener(this) {
                public final TeachersActivity a;
                
                public void onClick(final View view) {
                    this.a.Y();
                }
            });
        }
    }
    
    public final void R(final OrgProfile orgProfile) {
        for (final Teacher teacher : orgProfile.getTeacherList()) {
            if (teacher.getUserRole() == UserRole.TEACHER) {
                this.S(teacher);
                break;
            }
        }
    }
    
    public final void S(final Teacher teacher) {
        final ProgressDialog progressDialog = new ProgressDialog(this.d);
        progressDialog.setMessage((CharSequence)"Please wait, removing teacher...");
        ((Dialog)progressDialog).show();
        new k61(teacher, this.d, new zg(this, progressDialog, teacher) {
            public final ProgressDialog a;
            public final Teacher b;
            public final TeachersActivity c;
            
            @Override
            public void a(final boolean b, final int n, final Object o) {
                ((Dialog)this.a).dismiss();
                if (b) {
                    final OrgProfile instance = OrgProfile.getInstance(this.c.d);
                    final ArrayList<Teacher> teacherList = instance.getTeacherList();
                    final ArrayList teacherList2 = new ArrayList();
                    for (final Teacher e : teacherList) {
                        if (!e.getEmailId().equals(this.b.getEmailId())) {
                            teacherList2.add(e);
                        }
                    }
                    instance.setTeacherList(teacherList2);
                    instance.save(this.c.d);
                    this.c.Q();
                    FirebaseAnalytics.getInstance(this.c.d).a("TEACHER_REMOVED", null);
                }
                else {
                    FirebaseAnalytics.getInstance(this.c.d).a("TEACHER_REMOVED_FAIL", null);
                    a91.H(this.c.d, "Update failed, please try again", 2131230909, 2131231086);
                }
            }
        }).f();
    }
    
    public final void T(final String s, final String s2) {
        this.d.getSharedPreferences("MyPref", 0).edit().putString("user_organization", s2).commit();
        final r30 e = FirebaseAuth.getInstance().e();
        if (e != null) {
            e.Z(new g22.a().b(s).a());
        }
    }
    
    public final void U() {
        this.x(this.g = (Toolbar)this.findViewById(2131297361));
        this.g.setNavigationOnClickListener((View$OnClickListener)new View$OnClickListener(this) {
            public final TeachersActivity a;
            
            public void onClick(final View view) {
                this.a.finish();
            }
        });
    }
    
    public final void V(final tu1 tu1) {
        xs.a(this.d, 2131886780, 0, 2130903040, (xs.i)new xs.i(this, tu1) {
            public final tu1 a;
            public final TeachersActivity b;
            
            @Override
            public void a(final int n, final String s) {
                if (n == 0) {
                    this.b.X(this.a);
                }
            }
        });
    }
    
    public final void W() {
        final MaterialAlertDialogBuilder materialAlertDialogBuilder = new MaterialAlertDialogBuilder(this.d, 2131951953);
        final View inflate = ((LayoutInflater)this.d.getSystemService("layout_inflater")).inflate(2131492962, (ViewGroup)null);
        materialAlertDialogBuilder.setView(inflate);
        materialAlertDialogBuilder.setTitle(2131886126);
        materialAlertDialogBuilder.setPositiveButton(2131886367, (DialogInterface$OnClickListener)new DialogInterface$OnClickListener(this, (TextInputLayout)inflate.findViewById(2131297182)) {
            public final TextInputLayout a;
            public final TeachersActivity b;
            
            public void onClick(final DialogInterface dialogInterface, final int n) {
                final String trim = this.a.getEditText().getText().toString().trim();
                if (trim.length() < 6 || !trim.contains("@")) {
                    a91.H(this.b.d, "Please enter valid email", 2131230909, 2131231086);
                    return;
                }
                if (trim.equals(FirebaseAuth.getInstance().e().getEmail())) {
                    a91.H(this.b.d, "Please enter different email id then yours", 2131230909, 2131231086);
                    return;
                }
                this.b.N(trim);
                dialogInterface.dismiss();
            }
        });
        materialAlertDialogBuilder.setNegativeButton(2131886163, (DialogInterface$OnClickListener)new DialogInterface$OnClickListener(this) {
            public final TeachersActivity a;
            
            public void onClick(final DialogInterface dialogInterface, final int n) {
                dialogInterface.dismiss();
            }
        });
        ((a.a)materialAlertDialogBuilder).show();
    }
    
    public final void X(final tu1 tu1) {
        xs.c(this.d, new y01(this, tu1) {
            public final tu1 a;
            public final TeachersActivity b;
            
            @Override
            public void a(final Object o) {
                this.b.S(this.a.b);
            }
        }, 2131886129, 2131886541, 2131886903, 2131886657, 0, 0);
    }
    
    public final void Y() {
        xs.c(this.d, new y01(this) {
            public final TeachersActivity a;
            
            @Override
            public void a(final Object o) {
                this.a.O();
            }
        }, 2131886391, 2131886519, 2131886390, 2131886163, 0, 0);
    }
    
    public final void Z() {
        final OrgProfile instance = OrgProfile.getInstance(this.d);
        if (instance == null) {
            return;
        }
        final MaterialAlertDialogBuilder materialAlertDialogBuilder = new MaterialAlertDialogBuilder(this.d, 2131951953);
        final View inflate = ((LayoutInflater)this.d.getSystemService("layout_inflater")).inflate(2131492968, (ViewGroup)null);
        materialAlertDialogBuilder.setView(inflate);
        materialAlertDialogBuilder.setTitle(2131886252);
        final TextInputLayout textInputLayout = (TextInputLayout)inflate.findViewById(2131297177);
        final TextInputLayout textInputLayout2 = (TextInputLayout)inflate.findViewById(2131297178);
        final EditText editText = textInputLayout.getEditText();
        ((TextView)editText).setText((CharSequence)instance.getDisplayName());
        final EditText editText2 = textInputLayout2.getEditText();
        ((TextView)editText2).setText((CharSequence)instance.getOrganization());
        if (instance.getDisplayName() == null || instance.getDisplayName().length() < 1) {
            ((TextView)editText).setText((CharSequence)FirebaseAuth.getInstance().e().getDisplayName());
        }
        if (instance.getOrganization() == null || instance.getOrganization().length() < 1) {
            ((TextView)editText2).setText((CharSequence)this.d.getSharedPreferences("MyPref", 0).getString("user_organization", ""));
        }
        final Button button = (Button)inflate.findViewById(2131296400);
        final Button button2 = (Button)inflate.findViewById(2131296388);
        if (instance.getRole() != UserRole.OWNER) {
            ((View)editText2).setActivated(false);
            ((View)editText2).setFocusable(false);
        }
        final a create = materialAlertDialogBuilder.create();
        ((View)button).setOnClickListener((View$OnClickListener)new View$OnClickListener(this, editText, editText2, instance, create) {
            public final EditText a;
            public final EditText b;
            public final OrgProfile c;
            public final a d;
            public final TeachersActivity e;
            
            public void onClick(final View view) {
                final String trim = this.a.getText().toString().trim();
                if (trim.length() < 1) {
                    a91.G(this.e.d, 2131886487, 2131230909, 2131231086);
                    return;
                }
                final String trim2 = this.b.getText().toString().trim();
                if (this.c.getRole() == UserRole.OWNER && trim2.length() < 1) {
                    a91.G(this.e.d, 2131886489, 2131230909, 2131231086);
                    return;
                }
                this.e.a0(trim, trim2);
                this.d.dismiss();
            }
        });
        ((View)button2).setOnClickListener((View$OnClickListener)new View$OnClickListener(this, create) {
            public final a a;
            public final TeachersActivity b;
            
            public void onClick(final View view) {
                this.a.dismiss();
            }
        });
        create.show();
    }
    
    public final void a0(final String s, final String s2) {
        this.T(s, s2);
        final ProgressDialog progressDialog = new ProgressDialog(this.d);
        progressDialog.setMessage((CharSequence)"Please wait, updating profile...");
        ((Dialog)progressDialog).show();
        new s61(new UserProfileDto(s, s2), this.d, new zg(this, progressDialog, s, s2) {
            public final ProgressDialog a;
            public final String b;
            public final String c;
            public final TeachersActivity d;
            
            @Override
            public void a(final boolean b, final int n, final Object o) {
                ((Dialog)this.a).dismiss();
                if (b) {
                    final OrgProfile instance = OrgProfile.getInstance(this.d.d);
                    instance.setDisplayName(this.b);
                    instance.setOrganization(this.c);
                    instance.save(this.d.d);
                    this.d.Q();
                }
                else {
                    a91.G(this.d.d, 2131886548, 2131230909, 2131231086);
                }
            }
        });
    }
    
    @Override
    public void onCreate(final Bundle bundle) {
        super.onCreate(bundle);
        this.setContentView(2131492931);
        this.U();
        this.c = (ListView)this.findViewById(2131296847);
        (this.f = (SwipeRefreshLayout)this.findViewById(2131297134)).setColorSchemeColors(this.getResources().getIntArray(2130903043));
        this.f.setOnRefreshListener((SwipeRefreshLayout.j)new SwipeRefreshLayout.j(this) {
            public final TeachersActivity a;
            
            @Override
            public void a() {
                this.a.M();
            }
        });
        this.findViewById(2131296415).setOnClickListener((View$OnClickListener)new View$OnClickListener(this) {
            public final TeachersActivity a;
            
            public void onClick(final View view) {
                this.a.P();
            }
        });
        final OrgProfile instance = OrgProfile.getInstance(this.d);
        if (instance != null) {
            this.L(instance);
        }
    }
    
    @Override
    public void onResume() {
        super.onResume();
        this.Q();
        this.M();
    }
}

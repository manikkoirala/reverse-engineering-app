// 
// Decompiled by Procyon v0.6.0
// 

package com.ekodroid.omrevaluator.more;

import android.content.Context;
import android.app.Activity;
import com.google.firebase.auth.FirebaseAuth;
import android.view.View;
import android.view.View$OnClickListener;
import android.webkit.WebView;
import androidx.appcompat.widget.Toolbar;
import android.os.Bundle;

public class InstructionsActivity extends v5
{
    @Override
    public void onBackPressed() {
        super.onBackPressed();
        this.finish();
    }
    
    @Override
    public void onCreate(final Bundle bundle) {
        super.onCreate(bundle);
        this.setContentView(2131492912);
        this.x((Toolbar)this.findViewById(2131297334));
        ((WebView)this.findViewById(2131297406)).loadDataWithBaseURL((String)null, ((Context)this).getString(2131886345), "text/html", "utf-8", (String)null);
        this.findViewById(2131296761).setOnClickListener((View$OnClickListener)new View$OnClickListener(this, this) {
            public final InstructionsActivity a;
            public final InstructionsActivity b;
            
            public void onClick(final View view) {
                final r30 e = FirebaseAuth.getInstance().e();
                String string;
                if (e != null) {
                    final StringBuilder sb = new StringBuilder();
                    sb.append("from: ");
                    sb.append(e.getEmail());
                    string = sb.toString();
                }
                else {
                    string = "";
                }
                a91.F(this.a, "919860519186", string);
            }
        });
    }
    
    @Override
    public boolean v() {
        this.finish();
        return true;
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.ekodroid.omrevaluator.more;

import com.google.android.material.snackbar.BaseTransientBottomBar;
import android.app.Activity;
import com.google.firebase.auth.FirebaseAuth;
import com.google.android.material.snackbar.Snackbar$SnackbarLayout;
import android.view.ViewGroup;
import com.google.android.material.snackbar.Snackbar;
import com.ekodroid.omrevaluator.clients.UserProfileClients.models.ReportStorageDto;
import android.content.SharedPreferences;
import java.util.Collection;
import com.google.common.collect.ImmutableList;
import java.util.ArrayList;
import java.util.Iterator;
import com.android.billingclient.api.Purchase;
import com.android.billingclient.api.a;
import android.os.Bundle;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.ekodroid.omrevaluator.serializable.ResponseModel.PurchaseAccount;
import com.ekodroid.omrevaluator.serializable.SmsAccount;
import android.content.Intent;
import android.view.View$OnClickListener;
import com.ekodroid.omrevaluator.clients.UserProfileClients.models.UserRole;
import android.content.Context;
import com.ekodroid.omrevaluator.clients.UserProfileClients.models.OrgProfile;
import com.ekodroid.omrevaluator.serializable.ResponseModel.PurchaseTransaction;
import android.view.View;
import androidx.appcompat.widget.Toolbar;
import android.os.Handler;
import java.util.List;
import android.widget.ProgressBar;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

public class ProductAndServicesActivity extends v5
{
    public ProductAndServicesActivity c;
    public TextView d;
    public TextView e;
    public TextView f;
    public TextView g;
    public TextView h;
    public LinearLayout i;
    public Button j;
    public Button k;
    public Button l;
    public ProgressBar m;
    public aa1 n;
    public String p;
    public String q;
    public wb t;
    public List v;
    public int w;
    public Handler x;
    public boolean y;
    public Toolbar z;
    
    public ProductAndServicesActivity() {
        this.c = this;
        this.w = -1;
        this.y = false;
    }
    
    public static /* synthetic */ wb E(final ProductAndServicesActivity productAndServicesActivity) {
        return productAndServicesActivity.t;
    }
    
    public static /* synthetic */ int F(final ProductAndServicesActivity productAndServicesActivity) {
        return productAndServicesActivity.w;
    }
    
    public static /* synthetic */ int G(final ProductAndServicesActivity productAndServicesActivity, final int w) {
        return productAndServicesActivity.w = w;
    }
    
    public final boolean O() {
        final OrgProfile instance = OrgProfile.getInstance((Context)this.c);
        if (instance != null && instance.getRole() != UserRole.OWNER) {
            xs.c((Context)this.c, null, 2131886666, 2131886540, 2131886176, 0, 0, 0);
            return false;
        }
        return true;
    }
    
    public final void P() {
        this.findViewById(2131296423).setOnClickListener((View$OnClickListener)new View$OnClickListener(this) {
            public final ProductAndServicesActivity a;
            
            public void onClick(final View view) {
                final Intent intent = new Intent((Context)this.a.c, (Class)BuySmsCreditActivity.class);
                if (ProductAndServicesActivity.F(this.a) > -1) {
                    intent.putExtra("SMS_CREDIT", ProductAndServicesActivity.F(this.a));
                }
                ((Context)this.a).startActivity(intent);
            }
        });
        new va0((Context)this, new n52((Context)this, a91.u()).b(), new y01(this) {
            public final ProductAndServicesActivity a;
            
            @Override
            public void a(final Object o) {
                TextView textView;
                String string;
                if (o == null) {
                    textView = (TextView)this.a.findViewById(2131297285);
                    string = "Available credits : --";
                }
                else {
                    ProductAndServicesActivity.G(this.a, ((SmsAccount)o).getCredit());
                    textView = (TextView)this.a.findViewById(2131297285);
                    final StringBuilder sb = new StringBuilder();
                    sb.append("Available credits : ");
                    sb.append(ProductAndServicesActivity.F(this.a));
                    string = sb.toString();
                }
                textView.setText((CharSequence)string);
            }
        });
    }
    
    public final void Q() {
        ((Context)this).startActivity(new Intent((Context)this.c, (Class)SubscriptionDetailActivity.class));
    }
    
    public final void R(final PurchaseTransaction purchaseTransaction) {
        this.e0(2131886569);
        new i32(purchaseTransaction, (Context)this.c, new zg(this, purchaseTransaction) {
            public final PurchaseTransaction a;
            public final ProductAndServicesActivity b;
            
            @Override
            public void a(final boolean b, final int n, final Object o) {
                this.b.U();
                if (o != null && o instanceof PurchaseAccount) {
                    new z91((Context)this.b.c);
                    this.b.X();
                    FirebaseAnalytics.getInstance((Context)this.b.c).a("PURCHASE_GRANT_COMPLETED", null);
                }
                else {
                    final y01 y01 = new y01(this) {
                        public final ProductAndServicesActivity$f a;
                        
                        @Override
                        public void a(final Object o) {
                            final zg a = this.a;
                            a.b.R(a.a);
                        }
                    };
                    FirebaseAnalytics.getInstance((Context)this.b.c).a("PURCHASE_GRANT_FAILED", null);
                    xs.c((Context)this.b.c, y01, 0, 2131886575, 2131886746, 2131886176, 0, 0);
                }
            }
        });
    }
    
    public final void S() {
        if (this.t.b()) {
            this.t.f(ia1.a().b("inapp").a(), new ba1(this) {
                public final ProductAndServicesActivity a;
                
                @Override
                public void a(final a a, final List list) {
                    if (a.b() == 0) {
                        for (final Purchase purchase : list) {
                            FirebaseAnalytics.getInstance((Context)this.a.c).a("PURCHASE_OWNED_FOUND", null);
                            this.a.T(purchase);
                        }
                    }
                }
            });
        }
    }
    
    public void T(final Purchase purchase) {
        if (purchase.d() == 2) {
            this.U();
            xs.c((Context)this.c, null, 2131886704, 2131886537, 2131886176, 0, 0, 0);
        }
        else {
            final Iterator iterator = purchase.c().iterator();
            while (iterator.hasNext()) {
                this.R(new PurchaseTransaction(purchase.a(), this.q, this.p, (String)iterator.next(), purchase.e(), null, a91.n(), this.y));
            }
        }
    }
    
    public final void U() {
        this.n.a();
    }
    
    public final void V() {
        (this.t = wb.d((Context)this).c(new ca1(this) {
            public final ProductAndServicesActivity a;
            
            @Override
            public void a(final a a, final List list) {
                FirebaseAnalytics firebaseAnalytics;
                String s;
                if (a.b() == 0 && list != null) {
                    final Iterator iterator = list.iterator();
                    while (iterator.hasNext()) {
                        this.a.T((Purchase)iterator.next());
                    }
                    firebaseAnalytics = FirebaseAnalytics.getInstance((Context)this.a.c);
                    s = "PURCHASE_OK";
                }
                else if (a.b() == 7) {
                    this.a.U();
                    this.a.S();
                    firebaseAnalytics = FirebaseAnalytics.getInstance((Context)this.a.c);
                    s = "PURCHASE_OWNED";
                }
                else {
                    this.a.U();
                    this.a.c0();
                    firebaseAnalytics = FirebaseAnalytics.getInstance((Context)this.a.c);
                    s = "PURCHASE_OTHER";
                }
                firebaseAnalytics.a(s, null);
            }
        }).b().a()).g(new yb(this) {
            public final ProductAndServicesActivity a;
            
            @Override
            public void a(final a a) {
                if (a.b() == 0) {
                    final ArrayList list = new ArrayList();
                    list.add("teachers_01");
                    list.add("enterprise_05");
                    list.add("enterprise_10");
                    list.add("enterprise_15");
                    list.add("enterprise_20");
                    final ArrayList list2 = new ArrayList();
                    final Iterator iterator = list.iterator();
                    while (iterator.hasNext()) {
                        list2.add(ha1.b.a().b((String)iterator.next()).c("inapp").a());
                    }
                    ProductAndServicesActivity.E(this.a).e(ha1.a().b(ImmutableList.copyOf((Collection<?>)list2)).a(), new u81(this) {
                        public final ProductAndServicesActivity$h a;
                        
                        @Override
                        public void a(final a a, final List list) {
                            if (a.b() == 0) {
                                this.a.a.x.post((Runnable)new Runnable(this, list) {
                                    public final List a;
                                    public final ProductAndServicesActivity$h$a b;
                                    
                                    @Override
                                    public void run() {
                                        this.b.a.a.S();
                                        this.b.a.a.a0(this.a);
                                    }
                                });
                            }
                        }
                    });
                }
                else {
                    FirebaseAnalytics.getInstance((Context)this.a.c).a("BILLING_CLIENT_START_FAILED", null);
                    xs.c((Context)this.a.c, new y01(this) {
                        public final ProductAndServicesActivity$h a;
                        
                        @Override
                        public void a(final Object o) {
                            this.a.a.V();
                        }
                    }, 2131886199, 2131886566, 2131886746, 2131886176, 0, 2131230930);
                }
            }
            
            @Override
            public void b() {
                FirebaseAnalytics.getInstance((Context)this.a.c).a("BILLING_CLIENT_DISCONNECTED", null);
                this.a.finish();
            }
        });
    }
    
    public final void W() {
        this.i = (LinearLayout)this.findViewById(2131296783);
        this.m = (ProgressBar)this.findViewById(2131296974);
        this.f = (TextView)this.findViewById(2131297259);
        this.k = (Button)this.findViewById(2131296421);
        this.j = (Button)this.findViewById(2131296422);
        this.l = (Button)this.findViewById(2131296441);
        this.g = (TextView)this.findViewById(2131297298);
        this.h = (TextView)this.findViewById(2131297299);
        this.d = (TextView)this.findViewById(2131297382);
        this.e = (TextView)this.findViewById(2131297381);
        ((View)this.k).setOnClickListener((View$OnClickListener)new View$OnClickListener(this) {
            public final ProductAndServicesActivity a;
            
            public void onClick(final View view) {
                if (!this.a.O()) {
                    return;
                }
                final y01 y01 = new y01(this) {
                    public final ProductAndServicesActivity$q a;
                    
                    @Override
                    public void a(final Object o) {
                        if (o != null && o instanceof t81) {
                            ((View)this.a.a.m).setVisibility(0);
                            this.a.a.x.post((Runnable)new Runnable(this, o) {
                                public final Object a;
                                public final ProductAndServicesActivity$q$a b;
                                
                                @Override
                                public void run() {
                                    this.b.a.a.f0((t81)this.a);
                                }
                            });
                        }
                    }
                };
                final ArrayList list = new ArrayList();
                for (final t81 e : this.a.v) {
                    if (!e.b().equals("teachers_01")) {
                        list.add(e);
                    }
                }
                new kd((Context)this.a.c, y01, list);
            }
        });
        ((View)this.l).setOnClickListener((View$OnClickListener)new View$OnClickListener(this) {
            public final ProductAndServicesActivity a;
            
            public void onClick(final View view) {
                ((Context)this.a).startActivity(new Intent((Context)this.a.c, (Class)ContactUsActivity.class));
            }
        });
        ((View)this.j).setOnClickListener((View$OnClickListener)new View$OnClickListener(this) {
            public final ProductAndServicesActivity a;
            
            public void onClick(final View view) {
                if (!this.a.O()) {
                    return;
                }
                for (final t81 t81 : this.a.v) {
                    if (t81.b().equals("teachers_01")) {
                        this.a.f0(t81);
                        break;
                    }
                }
            }
        });
        this.findViewById(2131296760).setOnClickListener((View$OnClickListener)new View$OnClickListener(this) {
            public final ProductAndServicesActivity a;
            
            public void onClick(final View view) {
                this.a.d0(view);
            }
        });
        this.findViewById(2131296399).setOnClickListener((View$OnClickListener)new View$OnClickListener(this) {
            public final ProductAndServicesActivity a;
            
            public void onClick(final View view) {
                this.a.Q();
            }
        });
        this.findViewById(2131296398).setOnClickListener((View$OnClickListener)new View$OnClickListener(this) {
            public final ProductAndServicesActivity a;
            
            public void onClick(final View view) {
                this.a.Q();
            }
        });
        this.findViewById(2131296397).setOnClickListener((View$OnClickListener)new View$OnClickListener(this) {
            public final ProductAndServicesActivity a;
            
            public void onClick(final View view) {
                this.a.Q();
            }
        });
    }
    
    public final void X() {
        new sa0((Context)this, new zg(this) {
            public final ProductAndServicesActivity a;
            
            @Override
            public void a(final boolean b, final int n, final Object o) {
                this.a.Z();
            }
        });
        new ta0((Context)this, new zg(this) {
            public final ProductAndServicesActivity a;
            
            @Override
            public void a(final boolean b, final int n, final Object o) {
                this.a.Z();
            }
        });
    }
    
    public final void Y() {
        final SharedPreferences sharedPreferences = ((Context)this).getApplicationContext().getSharedPreferences("MyPref", 0);
        if (!sharedPreferences.getString("user_country", "").toUpperCase().equals("INDIA")) {
            this.findViewById(2131296737).setVisibility(8);
        }
        else {
            this.findViewById(2131296737).setVisibility(0);
            if (sharedPreferences.getInt("SMS_LINK_EXP", 0) > a91.n()) {
                this.g0();
            }
            else {
                this.P();
            }
        }
    }
    
    public final void Z() {
        final PurchaseAccount r = a91.r((Context)this.c);
        Object o;
        if (r != null) {
            final String expiryDate = r.getExpiryDate();
            ((View)this.i).setVisibility(0);
            final TextView f = this.f;
            final StringBuilder sb = new StringBuilder();
            sb.append(((Context)this).getString(2131886113));
            sb.append(" : ");
            sb.append(r.getAccountType());
            f.setText((CharSequence)sb.toString());
            final TextView g = this.g;
            final StringBuilder sb2 = new StringBuilder();
            sb2.append(((Context)this).getString(2131886438));
            sb2.append(" : ");
            sb2.append(r.getTeachersCount());
            g.setText((CharSequence)sb2.toString());
            if ("FREE".equals(r.getAccountType())) {
                ((View)this.h).setVisibility(8);
            }
            else {
                final TextView h = this.h;
                final StringBuilder sb3 = new StringBuilder();
                sb3.append(((Context)this).getString(2131886893));
                sb3.append(" : ");
                sb3.append(expiryDate);
                h.setText((CharSequence)sb3.toString());
            }
            if (r.isRenewDue()) {
                this.y = true;
                if (r.getAccountType().equals(ok.R)) {
                    ((TextView)this.j).setText((CharSequence)((Context)this).getString(2131886729));
                }
                if (r.getAccountType().equals(ok.S)) {
                    ((TextView)this.k).setText((CharSequence)((Context)this).getString(2131886729));
                }
            }
            final ReportStorageDto instance = ReportStorageDto.getInstance((Context)this.c);
            if (instance != null && r.getTeachersCount() > 0) {
                this.findViewById(2131296789).setVisibility(0);
                final TextView textView = (TextView)this.findViewById(2131297271);
                final StringBuilder sb4 = new StringBuilder();
                sb4.append("");
                sb4.append(instance.reportsCount);
                sb4.append("/");
                sb4.append(r.getTeachersCount() * 1000);
                textView.setText((CharSequence)sb4.toString());
                ((ProgressBar)this.findViewById(2131296980)).setProgress((int)(instance.reportsCount * 100L / (r.getTeachersCount() * 1000)));
                return;
            }
            o = this.findViewById(2131296789);
        }
        else {
            o = this.i;
        }
        ((View)o).setVisibility(8);
    }
    
    public void a0(final List v) {
        ((View)this.m).setVisibility(8);
        this.v = v;
        if (v != null && v.size() > 0) {
            ((View)this.j).setEnabled(true);
            for (final t81 t81 : this.v) {
                if (t81.b().equals("teachers_01")) {
                    this.d.setText((CharSequence)t81.a().a());
                }
                if (t81.b().equals("enterprise_05")) {
                    this.e.setText((CharSequence)t81.a().a());
                }
            }
            this.Z();
        }
        else {
            xs.c((Context)this.c, new y01(this) {
                public final ProductAndServicesActivity a;
                
                @Override
                public void a(final Object o) {
                    this.a.V();
                }
            }, 2131886199, 2131886566, 2131886746, 2131886176, 0, 2131230930);
        }
    }
    
    public final void b0() {
        this.x(this.z = (Toolbar)this.findViewById(2131297353));
        this.z.setNavigationOnClickListener((View$OnClickListener)new View$OnClickListener(this) {
            public final ProductAndServicesActivity a;
            
            public void onClick(final View view) {
                this.a.finish();
            }
        });
    }
    
    public final void c0() {
        xs.c((Context)this.c, new y01(this) {
            public final ProductAndServicesActivity a;
            
            @Override
            public void a(final Object o) {
                ((Context)this.a).startActivity(new Intent((Context)this.a.c, (Class)ContactUsActivity.class));
            }
        }, 0, 2131886464, 2131886202, 2131886176, 0, 0);
    }
    
    public final void d0(View inflate) {
        final Snackbar make = Snackbar.make(inflate, (CharSequence)"", 0);
        inflate = this.getLayoutInflater().inflate(2131493014, (ViewGroup)null);
        final Snackbar$SnackbarLayout snackbar$SnackbarLayout = (Snackbar$SnackbarLayout)((BaseTransientBottomBar)make).getView();
        ((View)snackbar$SnackbarLayout).setPadding(0, 0, 0, 0);
        inflate.findViewById(2131296787).setOnClickListener((View$OnClickListener)new View$OnClickListener(this) {
            public final ProductAndServicesActivity a;
            
            public void onClick(final View view) {
                final String email = FirebaseAuth.getInstance().e().getEmail();
                final ProductAndServicesActivity c = this.a.c;
                final StringBuilder sb = new StringBuilder();
                sb.append("from :");
                sb.append(email);
                a91.F(c, "919860519186", sb.toString());
            }
        });
        ((ViewGroup)snackbar$SnackbarLayout).addView(inflate, 0);
        make.show();
    }
    
    public final void e0(final int n) {
        final aa1 n2 = this.n;
        if (n2 == null) {
            this.n = new aa1((Context)this.c, n);
        }
        else {
            n2.b(n);
        }
    }
    
    public void f0(final t81 t81) {
        this.e0(2131886583);
        this.t.c(this, zb.a().c(ImmutableList.of(zb.b.a().b(t81).a())).b(this.p).a());
    }
    
    public final void g0() {
        new q61(new n52((Context)this, a91.u()).b(), new y01(this) {
            public final ProductAndServicesActivity a;
            
            @Override
            public void a(final Object o) {
                this.a.P();
            }
        });
    }
    
    @Override
    public void onBackPressed() {
        super.onBackPressed();
        this.finish();
    }
    
    @Override
    public void onCreate(final Bundle bundle) {
        super.onCreate(bundle);
        this.setContentView(2131492918);
        this.b0();
        this.x = new Handler();
        final r30 e = FirebaseAuth.getInstance().e();
        this.p = e.O();
        try {
            this.q = b.i(e.getEmail(), "emailKey");
        }
        catch (final Exception ex) {
            ex.printStackTrace();
        }
        this.W();
        this.V();
    }
    
    @Override
    public void onDestroy() {
        final wb t = this.t;
        if (t != null) {
            t.a();
        }
        super.onDestroy();
    }
    
    @Override
    public void onResume() {
        super.onResume();
        this.Y();
        this.X();
        this.S();
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.ekodroid.omrevaluator.more;

import android.widget.AdapterView;
import android.app.Dialog;
import android.app.Activity;
import android.text.TextUtils;
import android.view.View;
import android.view.View$OnClickListener;
import com.google.firebase.auth.FirebaseAuth;
import android.widget.SpinnerAdapter;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.EditText;
import android.widget.TextView;
import androidx.appcompat.widget.Toolbar;
import android.os.Bundle;
import com.ekodroid.omrevaluator.serializable.EmailData;
import android.app.ProgressDialog;
import android.content.Context;
import android.widget.Button;

public class ContactUsActivity extends v5
{
    public ContactUsActivity c;
    public Button d;
    
    public ContactUsActivity() {
        this.c = this;
    }
    
    public final boolean C(final String s) {
        return s.length() > 5 && s.contains("@");
    }
    
    public final void D(final String s, final String s2, final String s3) {
        ((View)this.d).setEnabled(false);
        final ee1 b = new n52((Context)this.c, a91.u()).b();
        final String string = ((Context)this).getSharedPreferences("MyPref", 0).getString("user_country", "");
        final ProgressDialog progressDialog = new ProgressDialog((Context)this.c);
        progressDialog.setMessage((CharSequence)"Please wait, sending email...");
        ((Dialog)progressDialog).show();
        new j61(new EmailData(s2, s, s3, string), b, new y01(this, progressDialog) {
            public final ProgressDialog a;
            public final ContactUsActivity b;
            
            @Override
            public void a(final Object o) {
                ((Dialog)this.a).dismiss();
                ContactUsActivity contactUsActivity;
                int n;
                int n2;
                int n3;
                if (o != null) {
                    contactUsActivity = this.b.c;
                    n = 2131230927;
                    n2 = 2131231085;
                    n3 = 2131886723;
                }
                else {
                    contactUsActivity = this.b.c;
                    n = 2131230909;
                    n2 = 2131231086;
                    n3 = 2131886722;
                }
                a91.G((Context)contactUsActivity, n3, n, n2);
                ((View)this.b.d).setEnabled(true);
            }
        });
    }
    
    @Override
    public void onBackPressed() {
        super.onBackPressed();
        this.finish();
    }
    
    @Override
    public void onCreate(final Bundle bundle) {
        super.onCreate(bundle);
        this.setContentView(2131492900);
        this.x((Toolbar)this.findViewById(2131297334));
        this.getWindow().setSoftInputMode(2);
        final TextView textView = (TextView)this.findViewById(2131297214);
        final EditText editText = (EditText)this.findViewById(2131296579);
        final EditText editText2 = (EditText)this.findViewById(2131296584);
        final Spinner spinner = (Spinner)this.findViewById(2131297076);
        spinner.setAdapter((SpinnerAdapter)new ArrayAdapter((Context)this.c, 2131493104, (Object[])this.getResources().getStringArray(2130903041)));
        this.d = (Button)this.findViewById(2131296479);
        final r30 e = FirebaseAuth.getInstance().e();
        if (e != null) {
            ((View)textView).setVisibility(8);
            ((View)editText).setVisibility(8);
        }
        ((View)this.d).setOnClickListener((View$OnClickListener)new View$OnClickListener(this, editText2, editText, spinner, e) {
            public final EditText a;
            public final EditText b;
            public final Spinner c;
            public final r30 d;
            public final ContactUsActivity e;
            
            public void onClick(final View view) {
                final EditText a = this.a;
                Object b = null;
                ((TextView)a).setError((CharSequence)null);
                ((TextView)this.b).setError((CharSequence)null);
                final String s = (String)((AdapterView)this.c).getSelectedItem();
                final r30 d = this.d;
                String s2;
                if (d != null) {
                    s2 = d.getEmail();
                }
                else {
                    s2 = this.b.getText().toString();
                }
                final boolean empty = TextUtils.isEmpty((CharSequence)s2);
                int n = 2131886310;
                final int n2 = 1;
                int n3 = 0;
                Label_0170: {
                    EditText editText = null;
                    ContactUsActivity contactUsActivity = null;
                    Label_0095: {
                        if (empty) {
                            editText = this.b;
                        }
                        else {
                            if (!this.e.C(s2)) {
                                editText = this.b;
                                contactUsActivity = this.e;
                                n = 2131886271;
                                break Label_0095;
                            }
                            if (!TextUtils.isEmpty((CharSequence)this.a.getText().toString())) {
                                n3 = 0;
                                break Label_0170;
                            }
                            editText = this.a;
                        }
                        contactUsActivity = this.e;
                    }
                    ((TextView)editText).setError((CharSequence)((Context)contactUsActivity).getString(n));
                    b = this.b;
                    n3 = n2;
                }
                if (n3 != 0) {
                    ((View)b).requestFocus();
                }
                else {
                    this.e.D(s, s2, this.a.getText().toString());
                }
            }
        });
        this.findViewById(2131296739).setOnClickListener((View$OnClickListener)new View$OnClickListener(this, e, editText) {
            public final r30 a;
            public final EditText b;
            public final ContactUsActivity c;
            
            public void onClick(final View view) {
                final r30 a = this.a;
                String str;
                if (a != null) {
                    str = a.getEmail();
                }
                else {
                    str = this.b.getText().toString();
                }
                final ContactUsActivity c = this.c.c;
                final StringBuilder sb = new StringBuilder();
                sb.append("from :");
                sb.append(str);
                a91.F(c, "919860519186", sb.toString());
            }
        });
    }
    
    @Override
    public boolean v() {
        this.finish();
        return true;
    }
}

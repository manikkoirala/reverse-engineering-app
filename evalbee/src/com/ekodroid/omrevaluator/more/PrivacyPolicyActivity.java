// 
// Decompiled by Procyon v0.6.0
// 

package com.ekodroid.omrevaluator.more;

import android.webkit.WebView;
import androidx.appcompat.widget.Toolbar;
import android.os.Bundle;

public class PrivacyPolicyActivity extends v5
{
    @Override
    public void onBackPressed() {
        super.onBackPressed();
        this.finish();
    }
    
    @Override
    public void onCreate(final Bundle bundle) {
        super.onCreate(bundle);
        this.setContentView(2131492917);
        this.x((Toolbar)this.findViewById(2131297334));
        ((WebView)this.findViewById(2131297408)).loadUrl("file:///android_asset/privacy_policy.html");
    }
    
    @Override
    public boolean v() {
        this.finish();
        return true;
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.ekodroid.omrevaluator.more;

import android.widget.AdapterView;
import androidx.appcompat.widget.Toolbar;
import android.os.Bundle;
import android.widget.RadioGroup$OnCheckedChangeListener;
import com.ekodroid.omrevaluator.templateui.createtemplate.TemplateLabelsActivity;
import com.ekodroid.omrevaluator.templateui.createtemplate.TemplateHeadersActivity;
import android.view.View;
import android.view.View$OnClickListener;
import android.widget.CompoundButton;
import android.widget.CompoundButton$OnCheckedChangeListener;
import android.widget.SpinnerAdapter;
import java.io.Serializable;
import android.content.Intent;
import com.ekodroid.omrevaluator.templateui.scansheet.ScanPaperActivity;
import android.content.Context;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.RadioGroup;
import android.widget.RadioButton;
import androidx.appcompat.widget.SwitchCompat;
import android.widget.LinearLayout;
import com.ekodroid.omrevaluator.clients.UserProfileClients.models.UserAccountDeleteRequest;
import android.content.SharedPreferences;
import com.ekodroid.omrevaluator.templateui.models.ExamId;
import android.widget.TextView;

public class SettingsActivity extends v5
{
    public TextView A;
    public Integer[] C;
    public ExamId D;
    public SharedPreferences F;
    public UserAccountDeleteRequest G;
    public TextView H;
    public SettingsActivity c;
    public LinearLayout d;
    public LinearLayout e;
    public LinearLayout f;
    public SwitchCompat g;
    public SwitchCompat h;
    public SwitchCompat i;
    public RadioButton j;
    public RadioButton k;
    public RadioButton l;
    public RadioButton m;
    public RadioGroup n;
    public Spinner p;
    public ArrayAdapter q;
    public boolean t;
    public boolean v;
    public boolean w;
    public boolean x;
    public boolean y;
    public int z;
    
    public SettingsActivity() {
        this.c = this;
        this.C = new Integer[] { 2, 3, 4, 5, 6, 7, 8, 9, 10 };
    }
    
    public static /* synthetic */ UserAccountDeleteRequest B(final SettingsActivity settingsActivity) {
        return settingsActivity.G;
    }
    
    public static /* synthetic */ UserAccountDeleteRequest C(final SettingsActivity settingsActivity, final UserAccountDeleteRequest g) {
        return settingsActivity.G = g;
    }
    
    public static /* synthetic */ SharedPreferences G(final SettingsActivity settingsActivity) {
        return settingsActivity.F;
    }
    
    public final void I() {
        new ka0((Context)this.c, new zg(this) {
            public final SettingsActivity a;
            
            @Override
            public void a(final boolean b, final int n, final Object o) {
                if (b && n == 200 && o instanceof UserAccountDeleteRequest) {
                    SettingsActivity.C(this.a, (UserAccountDeleteRequest)o);
                    this.a.T();
                }
            }
        });
    }
    
    public final void J() {
        new es((Context)this.c, new zg(this) {
            public final SettingsActivity a;
            
            @Override
            public void a(final boolean b, final int n, final Object o) {
                if (b && n == 200) {
                    SettingsActivity.C(this.a, null);
                    this.a.T();
                }
                else {
                    final SettingsActivity a = this.a;
                    a91.H((Context)a.c, ((Context)a).getString(2131886542), 2131230909, 2131231086);
                }
            }
        });
    }
    
    public final void K() {
        if (this.D != null) {
            final Intent intent = new Intent((Context)this.c, (Class)ScanPaperActivity.class);
            intent.putExtra("EXAM_ID", (Serializable)this.D);
            ((Context)this).startActivity(intent);
        }
        this.c.finish();
    }
    
    public final void L() {
        this.t = this.F.getBoolean("save_images", true);
        this.v = this.F.getBoolean("scan_sound", true);
        this.y = this.F.getBoolean("scan_resolution_high", false);
        this.w = this.F.getBoolean("option_mark_with_pen", false);
        this.x = this.F.getBoolean("AUTO_SAVE_SCAN", false);
        this.z = this.F.getInt("AUTO_SAVE_DELAY", 3);
        this.n = (RadioGroup)this.findViewById(2131297002);
        this.A = (TextView)this.findViewById(2131297272);
        this.g = (SwitchCompat)this.findViewById(2131296507);
        this.h = (SwitchCompat)this.findViewById(2131296508);
        this.i = (SwitchCompat)this.findViewById(2131296500);
        this.j = (RadioButton)this.findViewById(2131296992);
        this.k = (RadioButton)this.findViewById(2131296993);
        this.m = (RadioButton)this.findViewById(2131296989);
        this.l = (RadioButton)this.findViewById(2131296988);
        this.p = (Spinner)this.findViewById(2131297074);
        final LinearLayout linearLayout = (LinearLayout)this.findViewById(2131296791);
        final ArrayAdapter arrayAdapter = new ArrayAdapter((Context)this, 2131493104, (Object[])this.C);
        this.q = arrayAdapter;
        this.p.setAdapter((SpinnerAdapter)arrayAdapter);
        ((AdapterView)this.p).setSelection(this.z - 2);
        RadioButton radioButton;
        if (this.y) {
            radioButton = this.l;
        }
        else {
            radioButton = this.m;
        }
        ((CompoundButton)radioButton).setChecked(true);
        ((CompoundButton)this.j).setChecked(this.w);
        ((CompoundButton)this.k).setChecked(this.w ^ true);
        this.g.setChecked(this.t);
        this.h.setChecked(this.v);
        this.i.setOnCheckedChangeListener((CompoundButton$OnCheckedChangeListener)new CompoundButton$OnCheckedChangeListener(this) {
            public final SettingsActivity a;
            
            public void onCheckedChanged(final CompoundButton compoundButton, final boolean enabled) {
                this.a.p.setEnabled(enabled);
            }
        });
        this.i.setChecked(this.x);
        this.d = (LinearLayout)this.findViewById(2131296758);
        this.e = (LinearLayout)this.findViewById(2131296768);
        this.f = (LinearLayout)this.findViewById(2131296743);
        this.H = (TextView)this.findViewById(2131297376);
    }
    
    public final void M() {
        this.findViewById(2131296465).setOnClickListener((View$OnClickListener)new View$OnClickListener(this) {
            public final SettingsActivity a;
            
            public void onClick(final View view) {
                SettingsActivity.G(this.a).edit().putBoolean("save_images", this.a.g.isChecked()).commit();
                SettingsActivity.G(this.a).edit().putBoolean("scan_sound", this.a.h.isChecked()).commit();
                SettingsActivity.G(this.a).edit().putBoolean("scan_resolution_high", this.a.y).commit();
                SettingsActivity.G(this.a).edit().putBoolean("option_mark_with_pen", ((CompoundButton)this.a.j).isChecked()).commit();
                SettingsActivity.G(this.a).edit().putBoolean("AUTO_SAVE_SCAN", this.a.i.isChecked()).commit();
                SettingsActivity.G(this.a).edit().putInt("AUTO_SAVE_DELAY", ((AdapterView)this.a.p).getSelectedItemPosition() + 2).commit();
                this.a.K();
            }
        });
    }
    
    public final void N() {
        ((View)this.f).setOnClickListener((View$OnClickListener)new View$OnClickListener(this) {
            public final SettingsActivity a;
            
            public void onClick(final View view) {
                if (SettingsActivity.B(this.a) == null) {
                    this.a.S();
                }
                else {
                    this.a.J();
                }
            }
        });
    }
    
    public final void O() {
        ((View)this.d).setOnClickListener((View$OnClickListener)new View$OnClickListener(this) {
            public final SettingsActivity a;
            
            public void onClick(final View view) {
                ((Context)this.a).startActivity(new Intent((Context)this.a.c, (Class)TemplateHeadersActivity.class));
            }
        });
    }
    
    public final void P() {
        ((View)this.e).setOnClickListener((View$OnClickListener)new View$OnClickListener(this) {
            public final SettingsActivity a;
            
            public void onClick(final View view) {
                ((Context)this.a).startActivity(new Intent((Context)this.a.c, (Class)TemplateLabelsActivity.class));
            }
        });
    }
    
    public final void Q() {
        this.R();
        this.n.setOnCheckedChangeListener((RadioGroup$OnCheckedChangeListener)new RadioGroup$OnCheckedChangeListener(this) {
            public final SettingsActivity a;
            
            public void onCheckedChanged(final RadioGroup radioGroup, final int n) {
                this.a.R();
            }
        });
    }
    
    public final void R() {
        TextView textView;
        int text;
        if (((CompoundButton)this.l).isChecked()) {
            this.y = true;
            textView = this.A;
            text = 2131886507;
        }
        else {
            this.y = false;
            textView = this.A;
            text = 2131886521;
        }
        textView.setText(text);
    }
    
    public final void S() {
        xs.c((Context)this.c, new y01(this) {
            public final SettingsActivity a;
            
            @Override
            public void a(final Object o) {
                new d61((Context)this.a.c, new zg(this) {
                    public final SettingsActivity$f a;
                    
                    @Override
                    public void a(final boolean b, final int n, final Object o) {
                        if (b && n == 200 && o instanceof UserAccountDeleteRequest) {
                            SettingsActivity.C(this.a.a, (UserAccountDeleteRequest)o);
                            this.a.a.T();
                        }
                        else {
                            final SettingsActivity a = this.a.a;
                            a91.H((Context)a.c, ((Context)a).getString(2131886542), 2131230909, 2131231086);
                        }
                    }
                });
            }
        }, 2131886232, 2131886474, 2131886231, 2131886163, 0, 2131230945);
    }
    
    public final void T() {
        TextView textView;
        int n;
        if (this.G != null) {
            textView = this.H;
            n = 2131886164;
        }
        else {
            textView = this.H;
            n = 2131886232;
        }
        textView.setText((CharSequence)((Context)this).getString(n));
    }
    
    @Override
    public void onCreate(final Bundle bundle) {
        super.onCreate(bundle);
        this.setContentView(2131492924);
        this.x((Toolbar)this.findViewById(2131297334));
        final Serializable serializableExtra = this.getIntent().getSerializableExtra("EXAM_ID");
        if (serializableExtra != null && serializableExtra instanceof ExamId) {
            this.D = (ExamId)serializableExtra;
        }
        this.F = ((Context)this).getSharedPreferences("MyPref", 0);
        this.L();
        this.Q();
        this.M();
        this.O();
        this.P();
        this.I();
        this.N();
        this.findViewById(2131296429).setOnClickListener((View$OnClickListener)new View$OnClickListener(this) {
            public final SettingsActivity a;
            
            public void onClick(final View view) {
                this.a.K();
            }
        });
    }
    
    @Override
    public boolean v() {
        this.finish();
        return true;
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.ekodroid.omrevaluator.more.models;

import com.ekodroid.omrevaluator.clients.UserProfileClients.models.UserRole;
import java.io.Serializable;

public class Teacher implements Serializable
{
    String displayName;
    String emailId;
    MemberStatus memberStatus;
    String userId;
    UserRole userRole;
    
    public Teacher(final UserRole userRole, final String emailId) {
        this.userRole = userRole;
        this.emailId = emailId;
    }
    
    public String getDisplayName() {
        return this.displayName;
    }
    
    public String getEmailId() {
        return this.emailId;
    }
    
    public MemberStatus getMemberStatus() {
        return this.memberStatus;
    }
    
    public String getUserId() {
        return this.userId;
    }
    
    public UserRole getUserRole() {
        return this.userRole;
    }
    
    public void setDisplayName(final String displayName) {
        this.displayName = displayName;
    }
    
    public void setEmailId(final String emailId) {
        this.emailId = emailId;
    }
    
    public void setMemberStatus(final MemberStatus memberStatus) {
        this.memberStatus = memberStatus;
    }
    
    public void setUserId(final String userId) {
        this.userId = userId;
    }
    
    public void setUserRole(final UserRole userRole) {
        this.userRole = userRole;
    }
    
    public enum MemberStatus implements Serializable
    {
        private static final MemberStatus[] $VALUES;
        
        ACCEPTED, 
        EXPIRED, 
        LEFT, 
        PENDING, 
        REJECTED, 
        REQUEST_SENT;
        
        private static /* synthetic */ MemberStatus[] $values() {
            return new MemberStatus[] { MemberStatus.REQUEST_SENT, MemberStatus.PENDING, MemberStatus.ACCEPTED, MemberStatus.REJECTED, MemberStatus.LEFT, MemberStatus.EXPIRED };
        }
        
        static {
            $VALUES = $values();
        }
    }
}

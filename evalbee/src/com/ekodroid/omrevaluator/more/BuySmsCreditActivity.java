// 
// Decompiled by Procyon v0.6.0
// 

package com.ekodroid.omrevaluator.more;

import android.os.Bundle;
import android.content.ActivityNotFoundException;
import android.widget.Toast;
import android.content.Intent;
import android.net.Uri;
import com.ekodroid.omrevaluator.serializable.ResponseModel.SMSBuyLink;
import androidx.appcompat.widget.Toolbar;
import com.google.firebase.auth.FirebaseAuth;
import android.view.View$OnClickListener;
import android.view.KeyEvent;
import android.view.View;
import android.view.View$OnKeyListener;
import com.ekodroid.omrevaluator.clients.UserProfileClients.models.UserRole;
import com.ekodroid.omrevaluator.clients.UserProfileClients.models.OrgProfile;
import android.os.Handler;
import android.widget.ProgressBar;
import android.widget.Button;
import android.widget.TextView;
import android.widget.EditText;
import android.content.Context;

public class BuySmsCreditActivity extends v5
{
    public Context c;
    public EditText d;
    public TextView e;
    public TextView f;
    public Button g;
    public ProgressBar h;
    public TextView i;
    public Handler j;
    
    public BuySmsCreditActivity() {
        this.c = (Context)this;
    }
    
    public final boolean D() {
        final OrgProfile instance = OrgProfile.getInstance(this.c);
        if (instance != null && instance.getRole() != UserRole.OWNER) {
            xs.c(this.c, null, 2131886666, 2131886540, 2131886176, 0, 0, 0);
            return false;
        }
        return true;
    }
    
    public final String E(final int n) {
        int n2;
        if (n > 9999) {
            n2 = 22;
        }
        else {
            n2 = 25;
        }
        final double d = n2 * n * 1.0 / 100.0;
        final StringBuilder sb = new StringBuilder();
        sb.append(((Context)this).getString(2131886875));
        sb.append(" : ");
        sb.append(d);
        sb.append(" INR ");
        return sb.toString();
    }
    
    public final String F(int i) {
        if (i > 9999) {
            i = 22;
        }
        else {
            i = 25;
        }
        final StringBuilder sb = new StringBuilder();
        sb.append(i);
        sb.append(((Context)this).getString(2131886535));
        return sb.toString();
    }
    
    public final void G() {
        this.d = (EditText)this.findViewById(2131296586);
        this.e = (TextView)this.findViewById(2131297260);
        this.f = (TextView)this.findViewById(2131297206);
        this.d = (EditText)this.findViewById(2131296586);
        this.g = (Button)this.findViewById(2131296473);
        this.h = (ProgressBar)this.findViewById(2131296976);
        final TextView i = (TextView)this.findViewById(2131297284);
        this.i = i;
        final StringBuilder sb = new StringBuilder();
        sb.append(this.getIntent().getIntExtra("SMS_CREDIT", 0));
        sb.append("");
        i.setText((CharSequence)sb.toString());
        ((View)this.d).setOnKeyListener((View$OnKeyListener)new View$OnKeyListener(this) {
            public final BuySmsCreditActivity a;
            
            public boolean onKey(final View view, final int n, final KeyEvent keyEvent) {
                this.a.j.postDelayed((Runnable)new Runnable(this) {
                    public final BuySmsCreditActivity$a a;
                    
                    @Override
                    public void run() {
                        this.a.a.K();
                    }
                }, 500L);
                return false;
            }
        });
        this.findViewById(2131296682).setOnClickListener((View$OnClickListener)new View$OnClickListener(this) {
            public final BuySmsCreditActivity a;
            
            public void onClick(final View view) {
                this.a.K();
            }
        });
    }
    
    public final void H() {
        ((View)this.g).setOnClickListener((View$OnClickListener)new View$OnClickListener(this) {
            public final BuySmsCreditActivity a;
            
            public void onClick(final View view) {
                if (!this.a.D()) {
                    return;
                }
                final String string = this.a.d.getText().toString();
                if (!string.trim().equals("")) {
                    final int int1 = Integer.parseInt(string);
                    if (int1 >= 100) {
                        if (FirebaseAuth.getInstance().e() != null) {
                            try {
                                ((View)this.a.h).setVisibility(8);
                                ((View)this.a.g).setEnabled(false);
                                final BuySmsCreditActivity a = this.a;
                                ((TextView)a.g).setTextColor(a.c.getResources().getColor(2131099716));
                            }
                            catch (final Exception ex) {
                                ex.printStackTrace();
                            }
                            this.a.J(int1);
                        }
                        return;
                    }
                }
                a91.G(this.a.c, 2131886488, 2131230909, 2131231086);
            }
        });
    }
    
    public final void I() {
        final Toolbar toolbar = (Toolbar)this.findViewById(2131297338);
        this.x(toolbar);
        toolbar.setNavigationOnClickListener((View$OnClickListener)new View$OnClickListener(this) {
            public final BuySmsCreditActivity a;
            
            public void onClick(final View view) {
                this.a.finish();
            }
        });
    }
    
    public final void J(final int n) {
        new m61(this.c, new n52(this.c, a91.u()).b(), n, new y01(this) {
            public final BuySmsCreditActivity a;
            
            @Override
            public void a(final Object o) {
                if (o != null && o instanceof SMSBuyLink) {
                    final SMSBuyLink smsBuyLink = (SMSBuyLink)o;
                    ((Context)this.a).getApplicationContext().getSharedPreferences("MyPref", 0).edit().putInt("SMS_LINK_EXP", a91.n() + 25).commit();
                    try {
                        ((Context)this.a).startActivity(new Intent("android.intent.action.VIEW", Uri.parse(smsBuyLink.getShortUrl())));
                    }
                    catch (final ActivityNotFoundException ex) {
                        Toast.makeText(this.a.c, (CharSequence)"No application can handle this request. Please install a webbrowser", 1).show();
                        ((Throwable)ex).printStackTrace();
                    }
                    this.a.finish();
                }
                else {
                    a91.G(this.a.c, 2131886542, 2131230909, 2131231086);
                    ((TextView)this.a.g).setText(2131886746);
                }
                ((View)this.a.h).setVisibility(8);
                ((View)this.a.g).setEnabled(true);
                final BuySmsCreditActivity a = this.a;
                ((TextView)a.g).setTextColor(a.c.getResources().getColor(2131099740));
            }
        });
    }
    
    public final void K() {
        final String string = this.d.getText().toString();
        int int1;
        try {
            int1 = Integer.parseInt(string);
        }
        catch (final Exception ex) {
            int1 = 0;
        }
        final String f = this.F(int1);
        this.f.setText((CharSequence)this.E(int1));
        this.e.setText((CharSequence)f);
    }
    
    @Override
    public void onCreate(final Bundle bundle) {
        super.onCreate(bundle);
        this.setContentView(2131492897);
        this.j = new Handler();
        this.G();
        this.I();
        this.H();
    }
}

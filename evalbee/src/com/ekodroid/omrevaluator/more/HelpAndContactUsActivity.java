// 
// Decompiled by Procyon v0.6.0
// 

package com.ekodroid.omrevaluator.more;

import androidx.appcompat.widget.Toolbar;
import android.os.Bundle;
import android.content.Intent;
import android.view.View;
import android.view.View$OnClickListener;
import android.content.Context;
import android.widget.LinearLayout;

public class HelpAndContactUsActivity extends v5
{
    public LinearLayout c;
    public LinearLayout d;
    public LinearLayout e;
    public LinearLayout f;
    public Context g;
    
    public HelpAndContactUsActivity() {
        this.g = (Context)this;
    }
    
    public final void A() {
        ((View)this.d).setOnClickListener((View$OnClickListener)new View$OnClickListener(this) {
            public final HelpAndContactUsActivity a;
            
            public void onClick(final View view) {
                ((Context)this.a).startActivity(new Intent(this.a.g, (Class)ContactUsActivity.class));
            }
        });
    }
    
    public final void B() {
        ((View)this.c).setOnClickListener((View$OnClickListener)new View$OnClickListener(this) {
            public final HelpAndContactUsActivity a;
            
            public void onClick(final View view) {
                ((Context)this.a).startActivity(new Intent(this.a.g, (Class)InstructionsActivity.class));
            }
        });
    }
    
    public final void C() {
        ((View)this.e).setOnClickListener((View$OnClickListener)new View$OnClickListener(this) {
            public final HelpAndContactUsActivity a;
            
            public void onClick(final View view) {
                ((Context)this.a).startActivity(new Intent(this.a.g, (Class)PrivacyPolicyActivity.class));
            }
        });
    }
    
    public final void D() {
        ((View)this.f).setOnClickListener((View$OnClickListener)new View$OnClickListener(this) {
            public final HelpAndContactUsActivity a;
            
            public void onClick(final View view) {
                ((Context)this.a).startActivity(new Intent(this.a.g, (Class)TermsAndConditionsActivity.class));
            }
        });
    }
    
    @Override
    public void onCreate(final Bundle bundle) {
        super.onCreate(bundle);
        this.setContentView(2131492909);
        this.x((Toolbar)this.findViewById(2131297334));
        this.c = (LinearLayout)this.findViewById(2131296760);
        this.d = (LinearLayout)this.findViewById(2131296738);
        this.e = (LinearLayout)this.findViewById(2131296779);
        this.f = (LinearLayout)this.findViewById(2131296808);
        this.B();
        this.A();
        this.C();
        this.D();
    }
    
    @Override
    public boolean v() {
        this.finish();
        return true;
    }
}

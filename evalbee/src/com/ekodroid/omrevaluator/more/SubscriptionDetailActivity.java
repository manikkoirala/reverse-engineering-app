// 
// Decompiled by Procyon v0.6.0
// 

package com.ekodroid.omrevaluator.more;

import android.content.Context;
import android.webkit.WebView;
import android.os.Bundle;
import android.view.View;
import android.view.View$OnClickListener;
import androidx.appcompat.widget.Toolbar;

public class SubscriptionDetailActivity extends v5
{
    public Toolbar c;
    
    public final void A() {
        this.x(this.c = (Toolbar)this.findViewById(2131297360));
        this.c.setNavigationOnClickListener((View$OnClickListener)new View$OnClickListener(this) {
            public final SubscriptionDetailActivity a;
            
            public void onClick(final View view) {
                this.a.finish();
            }
        });
    }
    
    @Override
    public void onBackPressed() {
        super.onBackPressed();
        this.finish();
    }
    
    @Override
    public void onCreate(final Bundle bundle) {
        super.onCreate(bundle);
        this.setContentView(2131492930);
        this.A();
        ((WebView)this.findViewById(2131297407)).loadDataWithBaseURL((String)null, ((Context)this).getString(2131886858), "text/html", "utf-8", (String)null);
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.ekodroid.omrevaluator.components;

import android.view.View;
import android.view.View$MeasureSpec;
import android.util.AttributeSet;
import android.content.Context;
import android.view.TextureView;

public class AutofitPreview extends TextureView
{
    public int a;
    public int b;
    
    public AutofitPreview(final Context context, final AttributeSet set) {
        this(context, set, 0);
    }
    
    public AutofitPreview(final Context context, final AttributeSet set, final int n) {
        super(context, set, n);
        this.a = 0;
        this.b = 0;
    }
    
    public void a(final int a, final int b) {
        if (a >= 0 && b >= 0) {
            this.a = a;
            this.b = b;
            ((View)this).requestLayout();
            return;
        }
        throw new IllegalArgumentException("Size cannot be negative.");
    }
    
    public void onMeasure(int size, int b) {
        super.onMeasure(size, b);
        size = View$MeasureSpec.getSize(size);
        final int size2 = View$MeasureSpec.getSize(b);
        final int a = this.a;
        if (a != 0) {
            b = this.b;
            if (b != 0) {
                if (size < size2 * a / b) {
                    ((View)this).setMeasuredDimension(size, b * size / a);
                    return;
                }
                ((View)this).setMeasuredDimension(a * size2 / b, size2);
                return;
            }
        }
        ((View)this).setMeasuredDimension(size, size2);
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.ekodroid.omrevaluator.exams.models;

import java.io.Serializable;

public class RollNoListDataModel implements Serializable
{
    private int rank;
    private int rollNo;
    
    public RollNoListDataModel(final int rollNo, final int rank) {
        this.rollNo = rollNo;
        this.rank = rank;
    }
    
    public int getRank() {
        return this.rank;
    }
    
    public int getRollNo() {
        return this.rollNo;
    }
    
    public void setRank(final int rank) {
        this.rank = rank;
    }
    
    public void setRollNo(final int rollNo) {
        this.rollNo = rollNo;
    }
}

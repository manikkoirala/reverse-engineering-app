// 
// Decompiled by Procyon v0.6.0
// 

package com.ekodroid.omrevaluator.exams.models;

import java.io.Serializable;

public class MessageResultExcel implements Serializable
{
    public int TYPE;
    public int appVersion;
    public String[][] data;
    public String emailId;
    public String subjectName;
    
    public MessageResultExcel(final String emailId, final int type, final String[][] data, final String subjectName, final int appVersion) {
        this.emailId = emailId;
        this.TYPE = type;
        this.data = data;
        this.subjectName = subjectName;
        this.appVersion = appVersion;
    }
}

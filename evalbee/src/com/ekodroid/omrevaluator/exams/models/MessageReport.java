// 
// Decompiled by Procyon v0.6.0
// 

package com.ekodroid.omrevaluator.exams.models;

import java.io.Serializable;

public class MessageReport implements Serializable
{
    public int TYPE;
    public int appVersion;
    public String ccEmailID;
    public byte[] data;
    public String emailId;
    public String mailSubject;
    public String studentName;
    
    public MessageReport(final String emailId, final String ccEmailID, final int type, final byte[] data, final String mailSubject, final int appVersion, final String studentName) {
        this.emailId = emailId;
        this.ccEmailID = ccEmailID;
        this.TYPE = type;
        this.data = data;
        this.mailSubject = mailSubject;
        this.appVersion = appVersion;
        this.studentName = studentName;
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.ekodroid.omrevaluator.exams.analysis;

import android.content.Context;
import android.os.BaseBundle;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.k;
import androidx.appcompat.widget.Toolbar;
import java.io.Serializable;
import android.os.Bundle;
import com.ekodroid.omrevaluator.templateui.models.ExamId;
import androidx.viewpager.widget.ViewPager;
import com.google.android.material.tabs.TabLayout;

public class AnalysisActivity extends v5
{
    public a c;
    public TabLayout d;
    public ViewPager e;
    public a21 f;
    public g5 g;
    public ExamId h;
    public String i;
    
    public AnalysisActivity() {
        this.f = null;
        this.g = null;
    }
    
    public static /* synthetic */ g5 A(final AnalysisActivity analysisActivity) {
        return analysisActivity.g;
    }
    
    public static /* synthetic */ a21 B(final AnalysisActivity analysisActivity) {
        return analysisActivity.f;
    }
    
    public static a21 C(final ExamId examId, final String s) {
        final Bundle arguments = new Bundle();
        arguments.putSerializable("EXAM_ID", (Serializable)examId);
        ((BaseBundle)arguments).putString("EXAM_TYPE", s);
        final a21 a21 = new a21();
        a21.setArguments(arguments);
        return a21;
    }
    
    public static g5 D(final ExamId examId, final String s) {
        final Bundle arguments = new Bundle();
        arguments.putSerializable("EXAM_ID", (Serializable)examId);
        ((BaseBundle)arguments).putString("EXAM_TYPE", s);
        final g5 g5 = new g5();
        g5.setArguments(arguments);
        return g5;
    }
    
    @Override
    public void onCreate(final Bundle bundle) {
        super.onCreate(bundle);
        this.setContentView(2131492894);
        this.x((Toolbar)this.findViewById(2131297334));
        this.c = new a(this.getSupportFragmentManager());
        (this.e = (ViewPager)this.findViewById(2131296529)).setAdapter(this.c);
        (this.d = (TabLayout)this.findViewById(2131297149)).setupWithViewPager(this.e);
        this.h = (ExamId)this.getIntent().getSerializableExtra("EXAM_ID");
        final String stringExtra = this.getIntent().getStringExtra("EXAM_TYPE");
        this.i = stringExtra;
        this.f = C(this.h, stringExtra);
        this.g = D(this.h, this.i);
    }
    
    @Override
    public boolean v() {
        this.finish();
        return true;
    }
    
    public class a extends j80
    {
        public final AnalysisActivity h;
        
        public a(final AnalysisActivity h, final k k) {
            this.h = h;
            super(k);
        }
        
        @Override
        public int c() {
            return 2;
        }
        
        @Override
        public CharSequence e(int n) {
            AnalysisActivity analysisActivity;
            if (n != 1) {
                analysisActivity = this.h;
                n = 2131886684;
            }
            else {
                analysisActivity = this.h;
                n = 2131886137;
            }
            return ((Context)analysisActivity).getString(n);
        }
        
        @Override
        public Fragment p(final int n) {
            if (n != 1) {
                return AnalysisActivity.B(this.h);
            }
            return AnalysisActivity.A(this.h);
        }
    }
}

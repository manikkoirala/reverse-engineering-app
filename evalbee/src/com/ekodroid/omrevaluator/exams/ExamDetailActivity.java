// 
// Decompiled by Procyon v0.6.0
// 

package com.ekodroid.omrevaluator.exams;

import java.io.OutputStream;
import android.app.AlertDialog;
import java.io.FileOutputStream;
import android.os.AsyncTask;
import com.ekodroid.omrevaluator.templateui.ViewTemplateActivity;
import com.ekodroid.omrevaluator.activities.SplashActivity;
import com.ekodroid.omrevaluator.exams.sms.SmsActivity;
import com.ekodroid.omrevaluator.templateui.ExamSettingsActivity;
import java.util.HashSet;
import com.ekodroid.omrevaluator.database.StudentDataModel;
import com.ekodroid.omrevaluator.clients.SyncClients.Models.ReportPendingActionFile;
import com.ekodroid.omrevaluator.clients.SyncClients.DeleteSyncReports;
import com.ekodroid.omrevaluator.activities.Services.SyncImageService;
import com.ekodroid.omrevaluator.templateui.scanner.ResultItem;
import com.ekodroid.omrevaluator.clients.SyncClients.PostPendingReports;
import com.ekodroid.omrevaluator.clients.SyncClients.Models.StudentResultPartialResponse;
import com.ekodroid.omrevaluator.clients.SyncClients.Models.ExamPartialResponse;
import com.ekodroid.omrevaluator.clients.SyncClients.Models.Published;
import com.ekodroid.omrevaluator.clients.SyncClients.Models.SyncExamFile;
import com.google.android.play.core.review.ReviewManagerFactory;
import com.ekodroid.omrevaluator.templateui.models.AnswerOption;
import android.view.MenuItem;
import android.view.Menu;
import java.io.FileNotFoundException;
import android.content.pm.PackageManager$NameNotFoundException;
import android.provider.Settings$Secure;
import java.util.ArrayList;
import android.app.Dialog;
import android.widget.ListAdapter;
import android.widget.ListView;
import com.ekodroid.omrevaluator.activities.Services.ArchiveSyncService;
import android.os.Parcelable;
import androidx.core.content.FileProvider;
import java.io.File;
import com.ekodroid.omrevaluator.activities.Services.ServicePublishResult;
import androidx.appcompat.app.a;
import android.widget.Button;
import android.widget.RadioButton;
import android.content.SharedPreferences$Editor;
import android.widget.CheckBox;
import android.view.ViewGroup;
import android.view.LayoutInflater;
import android.content.DialogInterface;
import android.content.DialogInterface$OnClickListener;
import com.google.android.material.dialog.MaterialAlertDialogBuilder;
import com.google.android.gms.ads.LoadAdError;
import com.google.android.gms.ads.rewarded.RewardItem;
import com.google.android.gms.ads.OnUserEarnedRewardListener;
import com.ekodroid.omrevaluator.more.ProductAndServicesActivity;
import com.ekodroid.omrevaluator.templateui.SetAnswerKeyActivity;
import com.ekodroid.omrevaluator.exams.analysis.AnalysisActivity;
import android.view.View$OnClickListener;
import com.ekodroid.omrevaluator.clients.SyncClients.CreateSyncExam;
import com.ekodroid.omrevaluator.clients.SyncClients.Models.ExamFile;
import com.ekodroid.omrevaluator.clients.SyncClients.Models.SharedExamPartial;
import android.widget.CompoundButton;
import android.widget.CompoundButton$OnCheckedChangeListener;
import java.util.Iterator;
import java.io.IOException;
import com.ekodroid.omrevaluator.database.ResultDataJsonModel;
import com.ekodroid.omrevaluator.serializable.ExmVersions.ResultDataV1;
import com.ekodroid.omrevaluator.serializable.ExmVersions.ExamDataV1;
import com.google.android.gms.common.util.IOUtils;
import android.net.Uri;
import java.io.InputStream;
import java.util.Date;
import android.view.View;
import java.text.ParseException;
import android.text.format.DateFormat;
import java.text.SimpleDateFormat;
import com.ekodroid.omrevaluator.database.repositories.ClassRepository;
import android.widget.ProgressBar;
import android.widget.TextView;
import com.ekodroid.omrevaluator.database.TemplateDataJsonModel;
import com.ekodroid.omrevaluator.clients.SyncClients.Models.PublishRequest;
import com.ekodroid.omrevaluator.clients.SyncClients.PostPublishExam;
import com.ekodroid.omrevaluator.database.repositories.ResultRepository;
import com.ekodroid.omrevaluator.clients.SyncClients.Models.PublishResponse;
import com.google.firebase.auth.FirebaseAuth;
import java.io.Serializable;
import android.content.Intent;
import com.ekodroid.omrevaluator.templateui.scansheet.ScanPaperActivity;
import com.ekodroid.omrevaluator.database.repositories.TemplateRepository;
import com.ekodroid.omrevaluator.clients.SyncClients.Models.SharedExamDetailsDto;
import java.util.Set;
import com.google.android.play.core.tasks.OnCompleteListener;
import android.app.Activity;
import com.google.android.play.core.review.ReviewInfo;
import android.os.Bundle;
import android.content.Context;
import com.google.android.play.core.tasks.Task;
import com.google.android.play.core.review.ReviewManager;
import androidx.appcompat.widget.Toolbar;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import androidx.appcompat.widget.SwitchCompat;
import android.content.SharedPreferences;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.ekodroid.omrevaluator.templateui.models.ExamId;
import android.app.ProgressDialog;
import android.widget.LinearLayout;
import com.ekodroid.omrevaluator.templateui.models.SheetTemplate2;

public class ExamDetailActivity extends v5
{
    public final int c;
    public int d;
    public SheetTemplate2 e;
    public LinearLayout f;
    public ProgressDialog g;
    public String h;
    public ExamId i;
    public FirebaseAnalytics j;
    public int k;
    public ExamDetailActivity l;
    public SharedPreferences m;
    public SwitchCompat n;
    public SwipeRefreshLayout p;
    public long q;
    public Toolbar t;
    public boolean v;
    
    public ExamDetailActivity() {
        this.c = 2;
        this.d = 0;
        this.h = "";
        this.k = 999;
        this.l = this;
    }
    
    public static /* synthetic */ SwitchCompat F(final ExamDetailActivity examDetailActivity) {
        return examDetailActivity.n;
    }
    
    public static /* synthetic */ SharedPreferences N(final ExamDetailActivity examDetailActivity) {
        return examDetailActivity.m;
    }
    
    public static /* synthetic */ ExamDetailActivity g0(final ExamDetailActivity examDetailActivity) {
        return examDetailActivity.l;
    }
    
    public final void C0() {
        final ProgressDialog progressDialog = new ProgressDialog((Context)this.l);
        progressDialog.setMessage((CharSequence)((Context)this).getString(2131886503));
        ((Dialog)progressDialog).show();
        new ua0(TemplateRepository.getInstance((Context)this.l).getTemplateJson(this.i).getCloudId(), (Context)this.l, new zg(this, progressDialog) {
            public final ProgressDialog a;
            public final ExamDetailActivity b;
            
            @Override
            public void a(final boolean b, final int n, final Object o) {
                ((Dialog)this.a).dismiss();
                if (b && n == 200) {
                    this.b.j1((SharedExamDetailsDto)o);
                }
                else {
                    a91.H((Context)ExamDetailActivity.g0(this.b), ((Context)this.b).getString(2131886501), 2131230909, 2131231086);
                }
            }
        });
    }
    
    public final void D0() {
        this.g1();
    }
    
    public final void E0() {
        if (a91.D(30, this.m, this.k, this.h) && this.l0()) {
            FirebaseAnalytics.getInstance((Context)this.l).a("CONNECTION_SUCCESS_VALID", null);
            if (a91.a((Context)this.l)) {
                final Intent intent = new Intent((Context)this.l, (Class)ScanPaperActivity.class);
                intent.putExtra("EXAM_ID", (Serializable)this.i);
                intent.putExtra("START_SCAN", true);
                ((Context)this).startActivity(intent);
            }
            else {
                this.a1();
            }
        }
        else {
            FirebaseAnalytics.getInstance((Context)this.l).a("CONNECTION_SUCCESS_INVALID", null);
            this.v0();
        }
    }
    
    public final void F0(final boolean b, final boolean b2, final Long n) {
        final r30 e = FirebaseAuth.getInstance().e();
        final PublishRequest a = x91.a((Context)this.l, this.i, b, b2, n, e.getEmail(), e.getDisplayName());
        if (a.publishMetaDataDtos.size() == 0) {
            xs.c((Context)this.l, null, 0, 2131886449, 2131886176, 0, 0, 0);
            return;
        }
        final PostPublishExam postPublishExam = new PostPublishExam(a, (Context)this.l, new zg(this) {
            public final ExamDetailActivity a;
            
            @Override
            public void a(final boolean b, final int n, final Object o) {
                ((Dialog)this.a.g).dismiss();
                FirebaseAnalytics firebaseAnalytics;
                String s;
                if (b) {
                    final PublishResponse publishResponse = (PublishResponse)o;
                    ResultRepository.getInstance((Context)ExamDetailActivity.g0(this.a)).markResultAsPublished(this.a.i, publishResponse.rollSet);
                    TemplateRepository.getInstance((Context)ExamDetailActivity.g0(this.a)).markExamAsPublished(this.a.i, publishResponse.cloudId);
                    final ExamDetailActivity g0 = ExamDetailActivity.g0(this.a);
                    final StringBuilder sb = new StringBuilder();
                    sb.append(((Context)this.a).getString(2131886718));
                    sb.append(" ");
                    sb.append(publishResponse.rollSet.size());
                    sb.append(" ");
                    sb.append(((Context)this.a).getString(2131886736));
                    a91.H((Context)g0, sb.toString(), 2131230927, 2131231085);
                    firebaseAnalytics = FirebaseAnalytics.getInstance((Context)ExamDetailActivity.g0(this.a));
                    s = "WebPublishSuccess";
                }
                else {
                    a91.G((Context)ExamDetailActivity.g0(this.a), 2131886291, 2131230909, 2131231086);
                    firebaseAnalytics = FirebaseAnalytics.getInstance((Context)ExamDetailActivity.g0(this.a));
                    s = "WebPublishFail";
                }
                firebaseAnalytics.a(s, null);
            }
        });
        this.o1(postPublishExam);
        postPublishExam.f();
    }
    
    public final void G0() {
        final TemplateDataJsonModel templateJson = TemplateRepository.getInstance((Context)this.l).getTemplateJson(this.i);
        if (templateJson != null) {
            this.H0(templateJson, this.e = templateJson.getSheetTemplate());
        }
        this.q = ResultRepository.getInstance((Context)this.l).getAllResultCount(this.i);
        this.f = (LinearLayout)this.findViewById(2131296807);
        if (templateJson.isCloudSyncOn()) {
            ((View)this.f).setVisibility(8);
            this.v = true;
        }
        else {
            ((View)this.f).setVisibility(0);
            this.v = false;
        }
        this.invalidateOptionsMenu();
    }
    
    public final void H0(final TemplateDataJsonModel templateDataJsonModel, final SheetTemplate2 sheetTemplate2) {
        final TextView textView = (TextView)this.findViewById(2131297229);
        final TextView textView2 = (TextView)this.findViewById(2131297306);
        final TextView textView3 = (TextView)this.findViewById(2131297278);
        final TextView textView4 = (TextView)this.findViewById(2131297377);
        final TextView textView5 = (TextView)this.findViewById(2131297378);
        final TextView textView6 = (TextView)this.findViewById(2131297212);
        final TextView textView7 = (TextView)this.findViewById(2131297314);
        final ProgressBar progressBar = (ProgressBar)this.findViewById(2131296975);
        final long numberOfStudentForAClass = ClassRepository.getInstance((Context)this.l).getNumberOfStudentForAClass(this.i.getClassName());
        final long allResultCount = ResultRepository.getInstance((Context)this.l).getAllResultCount(this.i);
        final StringBuilder sb = new StringBuilder();
        sb.append(allResultCount);
        sb.append("");
        final long n = lcmp(numberOfStudentForAClass, 0L);
        String string;
        if (n > 0) {
            final StringBuilder sb2 = new StringBuilder();
            sb2.append("/");
            sb2.append(numberOfStudentForAClass);
            string = sb2.toString();
        }
        else {
            string = "";
        }
        sb.append(string);
        textView3.setText((CharSequence)sb.toString());
        int progress;
        if (n > 0) {
            progress = (int)(100L * allResultCount / numberOfStudentForAClass);
        }
        else if (allResultCount > 0L) {
            progress = 50;
        }
        else {
            progress = 0;
        }
        final View viewById = this.findViewById(2131296478);
        final long n2 = lcmp(allResultCount, 0L);
        int visibility;
        if (n2 > 0) {
            visibility = 8;
        }
        else {
            visibility = 0;
        }
        viewById.setVisibility(visibility);
        final View viewById2 = this.findViewById(2131296485);
        int visibility2;
        if (n2 > 0) {
            visibility2 = 0;
        }
        else {
            visibility2 = 8;
        }
        viewById2.setVisibility(visibility2);
        progressBar.setProgress(progress);
        textView.setText((CharSequence)templateDataJsonModel.getExamId().getExamName());
        final StringBuilder sb3 = new StringBuilder();
        sb3.append(sheetTemplate2.getAnswerOptions().size() - 1);
        sb3.append("");
        textView2.setText((CharSequence)sb3.toString());
        textView6.setText((CharSequence)templateDataJsonModel.getExamId().getClassName());
        final String examDate = templateDataJsonModel.getExamId().getExamDate();
        int n3;
        if (sheetTemplate2.getAnswerKeys() != null) {
            n3 = 2131886151;
        }
        else {
            n3 = 2131886665;
        }
        textView7.setText((CharSequence)((Context)this).getString(n3));
        final SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
        try {
            final Date parse = simpleDateFormat.parse(examDate);
            textView4.setText((CharSequence)DateFormat.format((CharSequence)"dd", parse));
            textView5.setText((CharSequence)DateFormat.format((CharSequence)"MMM", parse));
        }
        catch (final ParseException ex) {
            ex.printStackTrace();
        }
        this.findViewById(2131296829).setVisibility(8);
        this.findViewById(2131296828).setVisibility(8);
        this.findViewById(2131296830).setVisibility(8);
        this.findViewById(2131296827).setVisibility(8);
        View view = null;
        Label_0615: {
            if (templateDataJsonModel.isCloudSyncOn()) {
                final int n4 = ExamDetailActivity$l0.a[templateDataJsonModel.getSharedType().ordinal()];
                if (n4 == 1) {
                    view = this.findViewById(2131296829);
                    break Label_0615;
                }
                if (n4 == 2) {
                    view = this.findViewById(2131296828);
                    break Label_0615;
                }
                if (n4 == 3) {
                    view = this.findViewById(2131296830);
                    break Label_0615;
                }
            }
            view = this.findViewById(2131296827);
        }
        view.setVisibility(0);
    }
    
    public final void I0(InputStream ex, final Uri uri) {
        try {
            ex = (Exception)yx.a(IOUtils.toByteArray((InputStream)ex));
            if (ex == null) {
                this.k1(2131886282, ((Context)this.l).getString(2131886515));
                return;
            }
            if (this.p0(this.i, (ExamDataV1)ex)) {
                ex = (Exception)((ExamDataV1)ex).getResults();
                for (final ResultDataV1 resultDataV1 : ex) {
                    final ResultDataJsonModel resultDataJsonModel = new ResultDataJsonModel(this.i.getExamName(), resultDataV1.getRollno(), this.i.getClassName(), this.i.getExamDate(), resultDataV1.getResultItem2(), false, false);
                    final ResultRepository instance = ResultRepository.getInstance((Context)this.l);
                    instance.deleteStudentResult(this.i, resultDataJsonModel.getRollNo());
                    instance.deleteSheetImages(this.i, resultDataJsonModel.getRollNo());
                    if (instance.saveOrUpdateResultJsonAsNotSynced(resultDataJsonModel)) {
                        FirebaseAnalytics.getInstance((Context)this.l).a("ResultMergeSave", null);
                    }
                }
                final StringBuilder sb = new StringBuilder();
                sb.append(((ArrayList)ex).size());
                sb.append(" ");
                sb.append(((Context)this).getString(2131886576));
                this.k1(2131886360, sb.toString());
                return;
            }
            this.k1(2131886282, ((Context)this.l).getString(2131886563));
            return;
        }
        catch (final Exception ex) {}
        catch (final IOException ex2) {}
        ex.printStackTrace();
    }
    
    public final void J0() {
        this.n.setOnCheckedChangeListener((CompoundButton$OnCheckedChangeListener)new CompoundButton$OnCheckedChangeListener(this) {
            public final ExamDetailActivity a;
            
            public void onCheckedChanged(final CompoundButton compoundButton, final boolean b) {
                if (b) {
                    new vx((Context)ExamDetailActivity.g0(this.a), null, new y01(this) {
                        public final ExamDetailActivity$q0 a;
                        
                        @Override
                        public void a(final Object o) {
                            if (o != null) {
                                final SharedExamPartial sharedExamPartial = (SharedExamPartial)o;
                                if (!a91.c((Context)ExamDetailActivity.g0(this.a.a))) {
                                    this.a.a.b1();
                                    return;
                                }
                                final TemplateDataJsonModel templateJson = TemplateRepository.getInstance((Context)ExamDetailActivity.g0(this.a.a)).getTemplateJson(this.a.a.i);
                                final SheetTemplate2 sheetTemplate = templateJson.getSheetTemplate();
                                final ExamFile examFile = new ExamFile(this.a.a.i, sheetTemplate.getTemplateVersion(), new gc0().s(sheetTemplate), 31536000000L, sharedExamPartial.getSharedType(), sharedExamPartial.getTeachers(), sharedExamPartial.isSyncImages());
                                final ProgressDialog progressDialog = new ProgressDialog((Context)ExamDetailActivity.g0(this.a.a), 2131951921);
                                progressDialog.setMessage((CharSequence)((Context)this.a.a).getString(2131886709));
                                progressDialog.setProgressStyle(0);
                                ((Dialog)progressDialog).setCanceledOnTouchOutside(false);
                                ((Dialog)progressDialog).show();
                                new CreateSyncExam(examFile, (Context)ExamDetailActivity.g0(this.a.a), new zg(this, progressDialog, templateJson) {
                                    public final ProgressDialog a;
                                    public final TemplateDataJsonModel b;
                                    public final ExamDetailActivity$q0$a c;
                                    
                                    @Override
                                    public void a(final boolean b, int n, final Object o) {
                                        ((Dialog)this.a).dismiss();
                                        if (b) {
                                            final CreateSyncExam.ResponseCreateExam responseCreateExam = (CreateSyncExam.ResponseCreateExam)o;
                                            this.b.setCloudId(responseCreateExam.getId());
                                            this.b.setLastSyncedOn(0L);
                                            this.b.setLastUpdatedOn(responseCreateExam.getUpdatedOn());
                                            this.b.setCloudSyncOn(true);
                                            this.b.setSynced(true);
                                            this.b.setUserId(responseCreateExam.getUserId());
                                            this.b.setSharedType(responseCreateExam.getSharedType());
                                            this.b.setSyncImages(responseCreateExam.isSyncImages());
                                            TemplateRepository.getInstance((Context)ExamDetailActivity.g0(this.c.a.a)).saveOrUpdateTemplateJsonAsSynced(this.b);
                                            ((View)this.c.a.a.f).setVisibility(8);
                                            this.c.a.a.r1(false);
                                            this.c.a.a.G0();
                                        }
                                        else {
                                            ExamDetailActivity.F(this.c.a.a).setChecked(false);
                                            ExamDetailActivity examDetailActivity;
                                            ExamDetailActivity examDetailActivity2;
                                            if (n == 500) {
                                                examDetailActivity = ExamDetailActivity.g0(this.c.a.a);
                                                examDetailActivity2 = this.c.a.a;
                                                n = 2131886496;
                                            }
                                            else {
                                                examDetailActivity = ExamDetailActivity.g0(this.c.a.a);
                                                examDetailActivity2 = this.c.a.a;
                                                n = 2131886860;
                                            }
                                            a91.H((Context)examDetailActivity, ((Context)examDetailActivity2).getString(n), 2131230909, 2131231086);
                                        }
                                    }
                                });
                            }
                            else {
                                ExamDetailActivity.F(this.a.a).setChecked(false);
                            }
                        }
                    }).show();
                }
            }
        });
    }
    
    public final void K0() {
        this.findViewById(2131296754).setOnClickListener((View$OnClickListener)new View$OnClickListener(this) {
            public final ExamDetailActivity a;
            
            public void onClick(final View view) {
                final ExamDetailActivity a = this.a;
                a.t0(a.i);
            }
        });
    }
    
    public final void L0() {
        this.findViewById(2131296727).setOnClickListener((View$OnClickListener)new View$OnClickListener(this) {
            public final ExamDetailActivity a;
            
            public void onClick(final View view) {
                this.a.i1();
            }
        });
    }
    
    public final void M0() {
        this.findViewById(2131296817).setOnClickListener((View$OnClickListener)new View$OnClickListener(this) {
            public final ExamDetailActivity a;
            
            public void onClick(final View view) {
                if (!this.a.m0()) {
                    return;
                }
                final Intent intent = new Intent((Context)ExamDetailActivity.g0(this.a), (Class)AnalysisActivity.class);
                intent.putExtra("EXAM_ID", (Serializable)this.a.i);
                ((Context)this.a).startActivity(intent);
            }
        });
    }
    
    public final void N0() {
        this.findViewById(2131296744).setOnClickListener((View$OnClickListener)new View$OnClickListener(this) {
            public final ExamDetailActivity a;
            
            public void onClick(final View view) {
                this.a.q0();
            }
        });
    }
    
    public final void O0() {
        this.findViewById(2131296756).setOnClickListener((View$OnClickListener)new View$OnClickListener(this) {
            public final ExamDetailActivity a;
            
            public void onClick(final View view) {
                this.a.l1();
            }
        });
    }
    
    public final void P0() {
        this.findViewById(2131296764).setOnClickListener((View$OnClickListener)new View$OnClickListener(this) {
            public final ExamDetailActivity a;
            
            public void onClick(final View view) {
                final ExamDetailActivity a = this.a;
                a.m1(a.i);
            }
        });
    }
    
    public final void Q0() {
        this.findViewById(2131296765).setOnClickListener((View$OnClickListener)new View$OnClickListener(this) {
            public final ExamDetailActivity a;
            
            public void onClick(final View view) {
                this.a.n1();
            }
        });
    }
    
    public final void R0() {
        this.findViewById(2131296733).setOnClickListener((View$OnClickListener)new View$OnClickListener(this) {
            public final ExamDetailActivity a;
            
            public void onClick(final View view) {
                final Intent intent = new Intent((Context)ExamDetailActivity.g0(this.a), (Class)SetAnswerKeyActivity.class);
                intent.putExtra("EXAM_ID", (Serializable)this.a.i);
                ((Context)this.a).startActivity(intent);
            }
        });
    }
    
    public final void S0() {
        this.findViewById(2131296782).setOnClickListener((View$OnClickListener)new View$OnClickListener(this) {
            public final ExamDetailActivity a;
            
            public void onClick(final View view) {
                if (!this.a.m0()) {
                    return;
                }
                this.a.D0();
            }
        });
    }
    
    public final void T0() {
        this.findViewById(2131296796).setOnClickListener((View$OnClickListener)new View$OnClickListener(this) {
            public final ExamDetailActivity a;
            
            public void onClick(final View view) {
                if (this.a.k0()) {
                    this.a.E0();
                }
            }
        });
    }
    
    public final void U0() {
        this.findViewById(2131296818).setOnClickListener((View$OnClickListener)new View$OnClickListener(this) {
            public final ExamDetailActivity a;
            
            public void onClick(final View view) {
                this.a.w0();
            }
        });
    }
    
    public final void V0() {
        this.findViewById(2131296819).setOnClickListener((View$OnClickListener)new View$OnClickListener(this) {
            public final ExamDetailActivity a;
            
            public void onClick(final View view) {
                this.a.x0();
            }
        });
    }
    
    public final void W0() {
        (this.p = (SwipeRefreshLayout)this.findViewById(2131297134)).setColorSchemeColors(this.getResources().getIntArray(2130903043));
        this.p.setOnRefreshListener((SwipeRefreshLayout.j)new SwipeRefreshLayout.j(this) {
            public final ExamDetailActivity a;
            
            @Override
            public void a() {
                final TemplateDataJsonModel templateJson = TemplateRepository.getInstance((Context)ExamDetailActivity.g0(this.a)).getTemplateJson(this.a.i);
                if (templateJson != null && templateJson.isCloudSyncOn()) {
                    this.a.r1(false);
                }
                this.a.G0();
                this.a.p.setRefreshing(false);
            }
        });
    }
    
    public final void X0() {
        this.x(this.t = (Toolbar)this.findViewById(2131297344));
        this.t.setNavigationOnClickListener((View$OnClickListener)new View$OnClickListener(this) {
            public final ExamDetailActivity a;
            
            public void onClick(final View view) {
                this.a.finish();
            }
        });
    }
    
    public final void Y0(final boolean b) {
        xs.c((Context)this.l, new y01(this, b) {
            public final boolean a;
            public final ExamDetailActivity b;
            
            @Override
            public void a(final Object o) {
                this.b.o0(this.a);
            }
        }, 2131886282, 2131886494, 2131886746, 2131886163, 0, 2131230930);
    }
    
    public final void Z0(final boolean b) {
        FirebaseAnalytics.getInstance((Context)this.l).a("REWARD_EXCEL_DIALOG_SHOWN", null);
        new kf1((Context)this.l, ((Context)this).getString(2131886582), ((Context)this).getString(2131886585), new y01(this, b) {
            public final boolean a;
            public final ExamDetailActivity b;
            
            @Override
            public void a(final Object o) {
                if (o != null && o.equals("UPGRADE")) {
                    ((Context)this.b).startActivity(new Intent((Context)ExamDetailActivity.g0(this.b), (Class)ProductAndServicesActivity.class));
                }
                if (o != null && o.equals("WATCH_VIDEO")) {
                    y3.d(ExamDetailActivity.g0(this.b), new OnUserEarnedRewardListener(this) {
                        public final ExamDetailActivity$h0 a;
                        
                        @Override
                        public void onUserEarnedReward(final RewardItem rewardItem) {
                            final ExamDetailActivity g0 = ExamDetailActivity.g0(this.a.b);
                            final y01 a = this.a;
                            new su((Context)g0, a.b.i, a.a);
                            FirebaseAnalytics.getInstance((Context)ExamDetailActivity.g0(this.a.b)).a("REWARD_EXCEL_GRANTED", null);
                            a91.H((Context)ExamDetailActivity.g0(this.a.b), ((Context)this.a.b).getString(2131886303), 2131230927, 2131231085);
                        }
                    }, (y3.c)new y3.c(this) {
                        public final ExamDetailActivity$h0 a;
                        
                        @Override
                        public void a(final LoadAdError loadAdError) {
                            final ExamDetailActivity b = this.a.b;
                            ++b.d;
                            FirebaseAnalytics.getInstance((Context)ExamDetailActivity.g0(b)).a("REWARD_EXCEL_LOAD_FAIL", null);
                            final ExamDetailActivity b2 = this.a.b;
                            if (b2.d > 1) {
                                final ExamDetailActivity g0 = ExamDetailActivity.g0(b2);
                                final y01 a = this.a;
                                new su((Context)g0, a.b.i, a.a);
                            }
                            else {
                                a91.H((Context)ExamDetailActivity.g0(b2), ((Context)this.a.b).getString(2131886446), 2131230909, 2131231086);
                            }
                        }
                    });
                }
            }
        });
    }
    
    public final void a1() {
        FirebaseAnalytics.getInstance((Context)this.l).a("REWARD_SCAN_DIALOG_SHOWN", null);
        new kf1((Context)this.l, ((Context)this).getString(2131886581), ((Context)this).getString(2131886586), new y01(this) {
            public final ExamDetailActivity a;
            
            @Override
            public void a(final Object o) {
                if (o != null && o.equals("UPGRADE")) {
                    ((Context)this.a).startActivity(new Intent((Context)ExamDetailActivity.g0(this.a), (Class)ProductAndServicesActivity.class));
                }
                if (o != null && o.equals("WATCH_VIDEO")) {
                    y3.d(ExamDetailActivity.g0(this.a), new OnUserEarnedRewardListener(this) {
                        public final ExamDetailActivity$u a;
                        
                        @Override
                        public void onUserEarnedReward(final RewardItem rewardItem) {
                            dj1.a((Context)ExamDetailActivity.g0(this.a.a), 50);
                            FirebaseAnalytics.getInstance((Context)ExamDetailActivity.g0(this.a.a)).a("REWARD_SCAN_GRANTED", null);
                            a91.H((Context)ExamDetailActivity.g0(this.a.a), "Extra scan rewarded", 2131230927, 2131231085);
                        }
                    }, (y3.c)new y3.c(this) {
                        public final ExamDetailActivity$u a;
                        
                        @Override
                        public void a(final LoadAdError loadAdError) {
                            final ExamDetailActivity a = this.a.a;
                            final int d = a.d + 1;
                            a.d = d;
                            final ExamDetailActivity g0 = ExamDetailActivity.g0(a);
                            if (d > 1) {
                                dj1.a((Context)g0, 20);
                                FirebaseAnalytics.getInstance((Context)ExamDetailActivity.g0(this.a.a)).a("REWARD_SCAN_LOAD_FAIL_SKIP", null);
                                return;
                            }
                            FirebaseAnalytics.getInstance((Context)g0).a("REWARD_SCAN_LOAD_FAIL", null);
                            a91.H((Context)ExamDetailActivity.g0(this.a.a), ((Context)this.a.a).getString(2131886446), 2131230909, 2131231086);
                        }
                    });
                }
            }
        });
    }
    
    public final void b1() {
        this.n.setChecked(false);
        xs.c((Context)this.l, new y01(this) {
            public final ExamDetailActivity a;
            
            @Override
            public void a(final Object o) {
                ((Context)this.a).startActivity(new Intent((Context)ExamDetailActivity.g0(this.a), (Class)ProductAndServicesActivity.class));
            }
        }, 2131886177, 2131886462, 2131886160, 2131886163, 0, 0);
    }
    
    public final void c1(final boolean b) {
        final MaterialAlertDialogBuilder materialAlertDialogBuilder = new MaterialAlertDialogBuilder((Context)this.l, 2131951953);
        materialAlertDialogBuilder.setTitle(2131886161).setMessage(2131886447);
        materialAlertDialogBuilder.setPositiveButton(2131886160, (DialogInterface$OnClickListener)new DialogInterface$OnClickListener(this) {
            public final ExamDetailActivity a;
            
            public void onClick(final DialogInterface dialogInterface, final int n) {
                ((Context)this.a).startActivity(new Intent((Context)ExamDetailActivity.g0(this.a), (Class)ProductAndServicesActivity.class));
                dialogInterface.dismiss();
            }
        });
        materialAlertDialogBuilder.setNegativeButton(2131886888, (DialogInterface$OnClickListener)new DialogInterface$OnClickListener(this, b) {
            public final boolean a;
            public final ExamDetailActivity b;
            
            public void onClick(final DialogInterface dialogInterface, final int n) {
                this.b.f1(this.a);
                dialogInterface.dismiss();
            }
        });
        materialAlertDialogBuilder.create().show();
    }
    
    public final void d1() {
        final MaterialAlertDialogBuilder materialAlertDialogBuilder = new MaterialAlertDialogBuilder((Context)this.l, 2131951953);
        final View inflate = ((LayoutInflater)((Context)this).getSystemService("layout_inflater")).inflate(2131492978, (ViewGroup)null);
        materialAlertDialogBuilder.setView(inflate);
        materialAlertDialogBuilder.setTitle(2131886799);
        final TextView textView = (TextView)inflate.findViewById(2131297210);
        final SwitchCompat switchCompat = (SwitchCompat)inflate.findViewById(2131297138);
        final CheckBox checkBox = (CheckBox)inflate.findViewById(2131296503);
        ((CompoundButton)checkBox).setChecked(this.m.getBoolean("include_rank", false));
        switchCompat.setOnCheckedChangeListener((CompoundButton$OnCheckedChangeListener)new CompoundButton$OnCheckedChangeListener(this, FirebaseAuth.getInstance().e(), textView) {
            public final r30 a;
            public final TextView b;
            public final ExamDetailActivity c;
            
            public void onCheckedChanged(final CompoundButton compoundButton, final boolean b) {
                if (b && this.a != null) {
                    ((View)this.b).setVisibility(0);
                    this.b.setText((CharSequence)this.a.getEmail());
                }
                else {
                    ((View)this.b).setVisibility(8);
                }
            }
        });
        materialAlertDialogBuilder.setPositiveButton(2131886717, (DialogInterface$OnClickListener)new DialogInterface$OnClickListener(this, textView, checkBox, switchCompat) {
            public final TextView a;
            public final CheckBox b;
            public final SwitchCompat c;
            public final ExamDetailActivity d;
            
            public void onClick(final DialogInterface dialogInterface, final int n) {
                this.a.getText().toString();
                final SharedPreferences$Editor edit = ExamDetailActivity.N(this.d).edit();
                edit.putBoolean("include_rank", ((CompoundButton)this.b).isChecked());
                edit.commit();
                this.d.F0(((CompoundButton)this.b).isChecked(), this.c.isChecked(), 0L);
                dialogInterface.dismiss();
            }
        });
        materialAlertDialogBuilder.setNegativeButton(2131886163, (DialogInterface$OnClickListener)new DialogInterface$OnClickListener(this) {
            public final ExamDetailActivity a;
            
            public void onClick(final DialogInterface dialogInterface, final int n) {
                dialogInterface.dismiss();
            }
        });
        materialAlertDialogBuilder.create().show();
    }
    
    public final void e1() {
        final MaterialAlertDialogBuilder materialAlertDialogBuilder = new MaterialAlertDialogBuilder((Context)this.l, 2131951953);
        final View inflate = ((LayoutInflater)((Context)this).getSystemService("layout_inflater")).inflate(2131492977, (ViewGroup)null);
        materialAlertDialogBuilder.setView(inflate);
        materialAlertDialogBuilder.setTitle(2131886799);
        final TextView textView = (TextView)inflate.findViewById(2131297210);
        final SwitchCompat switchCompat = (SwitchCompat)inflate.findViewById(2131297138);
        final RadioButton radioButton = (RadioButton)inflate.findViewById(2131296994);
        final RadioButton radioButton2 = (RadioButton)inflate.findViewById(2131296995);
        final CheckBox checkBox = (CheckBox)inflate.findViewById(2131296503);
        final CheckBox checkBox2 = (CheckBox)inflate.findViewById(2131296502);
        final Button button = (Button)inflate.findViewById(2131296459);
        ((CompoundButton)checkBox).setChecked(this.m.getBoolean("include_rank", false));
        ((CompoundButton)checkBox2).setChecked(this.m.getBoolean("include_sheet_img", false));
        switchCompat.setOnCheckedChangeListener((CompoundButton$OnCheckedChangeListener)new CompoundButton$OnCheckedChangeListener(this, FirebaseAuth.getInstance().e(), textView) {
            public final r30 a;
            public final TextView b;
            public final ExamDetailActivity c;
            
            public void onCheckedChanged(final CompoundButton compoundButton, final boolean b) {
                if (b && this.a != null) {
                    ((View)this.b).setVisibility(0);
                    this.b.setText((CharSequence)this.a.getEmail());
                }
                else {
                    ((View)this.b).setVisibility(8);
                }
            }
        });
        final a create = materialAlertDialogBuilder.create();
        ((View)button).setOnClickListener((View$OnClickListener)new View$OnClickListener(this, textView, checkBox, checkBox2, radioButton, radioButton2, create) {
            public final TextView a;
            public final CheckBox b;
            public final CheckBox c;
            public final RadioButton d;
            public final RadioButton e;
            public final a f;
            public final ExamDetailActivity g;
            
            public void onClick(final View view) {
                final String string = this.a.getText().toString();
                final SharedPreferences$Editor edit = ExamDetailActivity.N(this.g).edit();
                final StringBuilder sb = new StringBuilder();
                sb.append(this.g.i.getExamName());
                sb.append("_sentCount");
                edit.putInt(sb.toString(), 0);
                final StringBuilder sb2 = new StringBuilder();
                sb2.append(this.g.i.getExamName());
                sb2.append("_lastIndexSent");
                edit.putInt(sb2.toString(), -1);
                edit.putBoolean("include_rank", ((CompoundButton)this.b).isChecked());
                edit.putBoolean("include_sheet_img", ((CompoundButton)this.c).isChecked());
                edit.commit();
                final Intent intent = new Intent((Context)ExamDetailActivity.g0(this.g), (Class)ServicePublishResult.class);
                Label_0206: {
                    String s;
                    if (((CompoundButton)this.d).isChecked()) {
                        s = "send_all";
                    }
                    else {
                        if (!((CompoundButton)this.e).isChecked()) {
                            break Label_0206;
                        }
                        s = "send_pending";
                    }
                    intent.putExtra("SEND_PATTERN", s);
                }
                intent.putExtra("CC_EMAIL", string);
                intent.putExtra("EXAM_ID", (Serializable)this.g.i);
                intent.putExtra("SEND_EMAIL", true);
                intent.putExtra("SEND_SMS", false);
                intent.putExtra("INCLUDE_RANK", ((CompoundButton)this.b).isChecked());
                intent.putExtra("INCLUDE_SHEET_IMG", ((CompoundButton)this.c).isChecked());
                ((Context)this.g).startService(intent);
                this.f.dismiss();
            }
        });
        ((View)inflate.findViewById(2131296427)).setOnClickListener((View$OnClickListener)new View$OnClickListener(this, create) {
            public final a a;
            public final ExamDetailActivity b;
            
            public void onClick(final View view) {
                this.a.dismiss();
            }
        });
        create.show();
    }
    
    public final void f1(final boolean b) {
        final MaterialAlertDialogBuilder materialAlertDialogBuilder = new MaterialAlertDialogBuilder((Context)this.l, 2131951953);
        final View inflate = ((LayoutInflater)((Context)this).getSystemService("layout_inflater")).inflate(2131492993, (ViewGroup)null);
        materialAlertDialogBuilder.setView(inflate);
        materialAlertDialogBuilder.setTitle(2131886245);
        materialAlertDialogBuilder.setPositiveButton(2131886655, (DialogInterface$OnClickListener)new DialogInterface$OnClickListener(this, b, (CheckBox)inflate.findViewById(2131296511)) {
            public final boolean a;
            public final CheckBox b;
            public final ExamDetailActivity c;
            
            public void onClick(final DialogInterface dialogInterface, final int n) {
                if (this.a) {
                    this.c.o0(((CompoundButton)this.b).isChecked());
                }
                else {
                    new su((Context)ExamDetailActivity.g0(this.c), this.c.i, ((CompoundButton)this.b).isChecked());
                }
                dialogInterface.dismiss();
            }
        });
        materialAlertDialogBuilder.create().show();
    }
    
    public final void g1() {
        final MaterialAlertDialogBuilder materialAlertDialogBuilder = new MaterialAlertDialogBuilder((Context)this.l, 2131951953);
        final View inflate = ((LayoutInflater)((Context)this).getSystemService("layout_inflater")).inflate(2131492994, (ViewGroup)null);
        materialAlertDialogBuilder.setView(inflate);
        materialAlertDialogBuilder.setTitle(2131886798);
        materialAlertDialogBuilder.setPositiveButton(2131886655, (DialogInterface$OnClickListener)new DialogInterface$OnClickListener(this, (RadioButton)inflate.findViewById(2131296996), (RadioButton)inflate.findViewById(2131296987), (RadioButton)inflate.findViewById(2131297001)) {
            public final RadioButton a;
            public final RadioButton b;
            public final RadioButton c;
            public final ExamDetailActivity d;
            
            public void onClick(final DialogInterface dialogInterface, final int n) {
                if (((CompoundButton)this.a).isChecked()) {
                    this.d.u0();
                    dialogInterface.dismiss();
                    return;
                }
                if (((CompoundButton)this.b).isChecked()) {
                    this.d.e1();
                    dialogInterface.dismiss();
                }
                if (((CompoundButton)this.c).isChecked()) {
                    this.d.d1();
                    dialogInterface.dismiss();
                }
            }
        });
        materialAlertDialogBuilder.create().show();
    }
    
    public final void h1() {
        final StringBuilder sb = new StringBuilder();
        final StringBuilder sb2 = new StringBuilder();
        sb2.append(this.i.getExamName());
        sb2.append("_");
        sb2.append(this.i.getExamDate());
        sb.append(sb2.toString().replaceAll("[^a-zA-Z0-9_-]", "_"));
        sb.append(".exm");
        final String string = sb.toString();
        final y01 y01 = new y01(this, new File(((Context)this).getApplicationContext().getExternalFilesDir(ok.c), string)) {
            public final File a;
            public final ExamDetailActivity b;
            
            @Override
            public void a(final Object o) {
                if (this.a.exists()) {
                    final Uri f = FileProvider.f((Context)ExamDetailActivity.g0(this.b), "com.ekodroid.omrevaluator.fileprovider", this.a);
                    final Intent intent = new Intent("android.intent.action.SEND");
                    intent.setFlags(268435456);
                    intent.putExtra("android.intent.extra.STREAM", (Parcelable)f);
                    intent.setDataAndType(f, "application/octet-stream");
                    intent.setType("application/octet-stream");
                    intent.setFlags(1073741824);
                    intent.setFlags(1);
                    ((Context)this.b).startActivity(Intent.createChooser(intent, (CharSequence)"Share with"));
                }
            }
        };
        final ExamDetailActivity l = this.l;
        final StringBuilder sb3 = new StringBuilder();
        sb3.append(((Context)this).getString(2131886311));
        sb3.append(" : ");
        sb3.append(ok.c);
        sb3.append("/");
        sb3.append(string);
        xs.d((Context)l, y01, 2131886312, sb3.toString(), 2131886824, 2131886176, 0, 0);
    }
    
    public final void i0(final ExamId examId) {
        if (ResultRepository.getInstance((Context)this.l).getAllResultCount(examId) < 1L) {
            final ExamDetailActivity l = this.l;
            final StringBuilder sb = new StringBuilder();
            sb.append(((Context)this).getString(2131886524));
            sb.append(" ");
            sb.append(examId.getExamName());
            a91.H((Context)l, sb.toString(), 2131230909, 2131231086);
            return;
        }
        xs.c((Context)this.l, new y01(this, examId) {
            public final ExamId a;
            public final ExamDetailActivity b;
            
            @Override
            public void a(final Object o) {
                final TemplateRepository instance = TemplateRepository.getInstance((Context)ExamDetailActivity.g0(this.b));
                final TemplateDataJsonModel templateJson = instance.getTemplateJson(this.a);
                templateJson.setArchiving(true);
                if (templateJson.getArchiveId() == null) {
                    final StringBuilder sb = new StringBuilder();
                    sb.append("");
                    sb.append(System.currentTimeMillis());
                    templateJson.setArchiveId(sb.toString());
                }
                instance.saveOrUpdateTemplateJson(templateJson);
                ArchiveSyncService.n((Context)ExamDetailActivity.g0(this.b));
                ExamDetailActivity.g0(this.b).finish();
            }
        }, 2131886143, 2131886453, 2131886142, 2131886163, 0, 0);
    }
    
    public final void i1() {
        final u80 u80 = new u80((Context)this.l);
        u80.setContentView(2131493002);
        final ListView listView = (ListView)u80.findViewById(2131296833);
        final ArrayList s0 = this.s0();
        final TextView textView = (TextView)u80.findViewById(2131297203);
        final StringBuilder sb = new StringBuilder();
        sb.append(((Context)this).getString(2131886107));
        sb.append(" - ");
        sb.append(s0.size());
        textView.setText((CharSequence)sb.toString());
        listView.setAdapter((ListAdapter)new b3((Context)this.l, s0));
        u80.findViewById(2131296438).setOnClickListener((View$OnClickListener)new View$OnClickListener(this, u80) {
            public final Dialog a;
            public final ExamDetailActivity b;
            
            public void onClick(final View view) {
                this.a.dismiss();
            }
        });
        u80.create();
        u80.show();
    }
    
    public final void j0() {
        final TemplateDataJsonModel templateJson = TemplateRepository.getInstance((Context)this.l).getTemplateJson(this.i);
        if (templateJson.isCloudSyncOn() && (a91.t((Context)this.l) != null || !templateJson.isSynced() || System.currentTimeMillis() > templateJson.getLastSyncedOn() + 180000L || ResultRepository.getInstance((Context)this.l).getAllNonSyncResultCount(this.i) > 0L)) {
            this.r1(false);
        }
    }
    
    public final void j1(final SharedExamDetailsDto sharedExamDetailsDto) {
        new vx((Context)this.l, sharedExamDetailsDto, new y01(this) {
            public final ExamDetailActivity a;
            
            @Override
            public void a(final Object o) {
                if (o == null) {
                    return;
                }
                final SharedExamPartial sharedExamPartial = (SharedExamPartial)o;
                final ProgressDialog progressDialog = new ProgressDialog((Context)ExamDetailActivity.g0(this.a));
                progressDialog.setMessage((CharSequence)((Context)this.a).getString(2131886503));
                ((Dialog)progressDialog).show();
                new com.ekodroid.omrevaluator.clients.SyncClients.a(sharedExamPartial, (Context)ExamDetailActivity.g0(this.a), new zg(this, progressDialog) {
                    public final ProgressDialog a;
                    public final ExamDetailActivity$y b;
                    
                    @Override
                    public void a(final boolean b, final int n, final Object o) {
                        ((Dialog)this.a).dismiss();
                        if (!b) {
                            a91.H((Context)ExamDetailActivity.g0(this.b.a), ((Context)this.b.a).getString(2131886885), 2131230909, 2131231086);
                        }
                        this.b.a.G0();
                    }
                });
            }
        }).show();
    }
    
    public final boolean k0() {
        if (sl.checkSelfPermission((Context)this, "android.permission.CAMERA") == 0) {
            return true;
        }
        h2.g(this, new String[] { "android.permission.CAMERA" }, 2);
        return false;
    }
    
    public final void k1(final int n, final String s) {
        xs.d((Context)this.l, null, n, s, 2131886176, 0, 0, 0);
    }
    
    public final boolean l0() {
        final String string = Settings$Secure.getString(((Context)this).getContentResolver(), "android_id");
        final boolean b = false;
        int versionCode;
        try {
            versionCode = ((Context)this).getPackageManager().getPackageInfo(((Context)this).getPackageName(), 0).versionCode;
        }
        catch (final PackageManager$NameNotFoundException ex) {
            ((Throwable)ex).printStackTrace();
            versionCode = 999;
        }
        boolean b2 = b;
        if (a91.E(((Context)this).getSharedPreferences("MyPref", 0), string, versionCode)) {
            b2 = b;
            if (gk.c()) {
                b2 = true;
            }
        }
        return b2;
    }
    
    public final void l1() {
        new s0(null).execute((Object[])new Void[0]);
    }
    
    public final boolean m0() {
        ExamDetailActivity examDetailActivity;
        int n;
        if (this.q < 1L) {
            examDetailActivity = this.l;
            n = 2131886549;
        }
        else {
            final SheetTemplate2 sheetTemplate = TemplateRepository.getInstance((Context)this.l).getTemplateJson(this.i).getSheetTemplate();
            if (sheetTemplate.getAnswerKeys() != null && sheetTemplate.getAnswerKeys()[0] != null) {
                return true;
            }
            examDetailActivity = this.l;
            n = 2131886567;
        }
        xs.c((Context)examDetailActivity, null, 0, n, 2131886176, 0, 0, 0);
        return false;
    }
    
    public final void m1(final ExamId examId) {
        xs.c((Context)this.l, new y01(this) {
            public final ExamDetailActivity a;
            
            @Override
            public void a(final Object o) {
                this.a.y0();
            }
        }, 2131886353, 2131886509, 2131886385, 2131886176, 0, 0);
    }
    
    public final void n0(final Set set, final boolean b) {
        final ResultRepository instance = ResultRepository.getInstance((Context)this.l);
        final long allResultCount = instance.getAllResultCount(this.i);
        if (set.size() == allResultCount) {
            return;
        }
        if (allResultCount > set.size()) {
            for (final ResultDataJsonModel resultDataJsonModel : instance.getAllResultJson(this.i)) {
                if (!set.contains(resultDataJsonModel.getRollNo())) {
                    instance.deleteStudentResult(this.i, resultDataJsonModel.getRollNo());
                    instance.deleteSheetImages(this.i, resultDataJsonModel.getRollNo());
                }
            }
            return;
        }
        if (set.size() > allResultCount && !b) {
            final TemplateRepository instance2 = TemplateRepository.getInstance((Context)this.l);
            final TemplateDataJsonModel templateJson = instance2.getTemplateJson(this.i);
            templateJson.setLastSyncedOn(0L);
            instance2.saveOrUpdateTemplateJsonAsSynced(templateJson);
            this.r1(true);
        }
    }
    
    public final void n1() {
        xs.c((Context)this.l, null, 2131886771, 2131886561, 2131886176, 0, 0, 0);
    }
    
    public final void o0(final boolean b) {
        final ProgressDialog progressDialog = new ProgressDialog((Context)this.l);
        progressDialog.setMessage((CharSequence)((Context)this).getString(2131886467));
        ((Dialog)progressDialog).show();
        new ma0((Context)this.l, new zg(this, progressDialog, b) {
            public final ProgressDialog a;
            public final boolean b;
            public final ExamDetailActivity c;
            
            @Override
            public void a(final boolean b, final int n, final Object o) {
                ((Dialog)this.a).dismiss();
                if (b && n == 200) {
                    new su((Context)ExamDetailActivity.g0(this.c), this.c.i, this.b);
                }
                else {
                    FirebaseAnalytics firebaseAnalytics;
                    String s;
                    if (b && n == 218) {
                        this.c.Z0(this.b);
                        firebaseAnalytics = FirebaseAnalytics.getInstance((Context)ExamDetailActivity.g0(this.c));
                        s = "ExcelLimit";
                    }
                    else {
                        this.c.Y0(this.b);
                        firebaseAnalytics = FirebaseAnalytics.getInstance((Context)ExamDetailActivity.g0(this.c));
                        s = "ExcelDownloadError";
                    }
                    firebaseAnalytics.a(s, null);
                }
            }
        });
    }
    
    public final void o1(final PostPublishExam postPublishExam) {
        this.g.setMessage((CharSequence)((Context)this).getString(2131886719));
        this.g.setProgressStyle(0);
        ((Dialog)this.g).setCanceledOnTouchOutside(false);
        ((AlertDialog)this.g).setButton(-2, (CharSequence)((Context)this).getString(2131886163), (DialogInterface$OnClickListener)new DialogInterface$OnClickListener(this, postPublishExam) {
            public final PostPublishExam a;
            public final ExamDetailActivity b;
            
            public void onClick(final DialogInterface dialogInterface, final int n) {
                final PostPublishExam a = this.a;
                if (a != null) {
                    a.g();
                }
                dialogInterface.dismiss();
            }
        });
        ((Dialog)this.g).show();
    }
    
    @Override
    public void onActivityResult(final int n, final int n2, final Intent intent) {
        super.onActivityResult(n, n2, intent);
        if (n == 11 && n2 == -1) {
            final Uri data = intent.getData();
            try {
                this.I0(((Context)this.l).getContentResolver().openInputStream(data), data);
            }
            catch (final FileNotFoundException ex) {
                ex.printStackTrace();
            }
        }
    }
    
    @Override
    public void onBackPressed() {
        super.onBackPressed();
        this.finish();
    }
    
    @Override
    public void onCreate(Bundle serializableExtra) {
        super.onCreate(serializableExtra);
        this.setContentView(2131492905);
        this.X0();
        this.j = o4.a((Context)this);
        this.m = ((Context)this).getApplicationContext().getSharedPreferences("MyPref", 0);
        this.g = new ProgressDialog((Context)this.l, 2131951921);
        this.h = Settings$Secure.getString(((Context)this).getContentResolver(), "android_id");
        serializableExtra = (Bundle)this.getIntent().getSerializableExtra("EXAM_ID");
        if (serializableExtra == null) {
            o4.c(this.j, "RA_EXAMID_NULL", this.h);
            this.finish();
            return;
        }
        try {
            this.k = ((Context)this).getPackageManager().getPackageInfo(((Context)this).getPackageName(), 0).versionCode;
        }
        catch (final PackageManager$NameNotFoundException ex) {
            ((Throwable)ex).printStackTrace();
        }
        this.i = (ExamId)serializableExtra;
        final TemplateDataJsonModel templateJson = TemplateRepository.getInstance((Context)this.l).getTemplateJson(this.i);
        if (templateJson == null) {
            o4.c(this.j, "RA_OC_TEMPLATE_NULL", this.h);
            this.finish();
            return;
        }
        this.e = templateJson.getSheetTemplate();
        this.n = (SwitchCompat)this.findViewById(2131297140);
        this.p1();
        this.J0();
        this.W0();
        this.z0();
    }
    
    public boolean onCreateOptionsMenu(final Menu menu) {
        this.getMenuInflater().inflate(2131623946, menu);
        menu.findItem(2131296711).setVisible(this.v);
        return true;
    }
    
    public boolean onOptionsItemSelected(final MenuItem menuItem) {
        final int itemId = menuItem.getItemId();
        if (itemId != 2131296706) {
            if (itemId == 2131296711) {
                this.C0();
            }
        }
        else {
            this.i0(this.i);
        }
        return false;
    }
    
    @Override
    public void onPause() {
        super.onPause();
    }
    
    @Override
    public void onRequestPermissionsResult(final int n, final String[] array, final int[] array2) {
        super.onRequestPermissionsResult(n, array, array2);
        if (array2 != null && array2.length > 0 && array2[0] == 0 && 2 == n) {
            this.E0();
        }
    }
    
    @Override
    public void onResume() {
        super.onResume();
        this.j0();
        this.G0();
    }
    
    public final boolean p0(final ExamId examId, final ExamDataV1 examDataV1) {
        final SheetTemplate2 sheetTemplate = TemplateRepository.getInstance((Context)this.l).getTemplateJson(examId).getSheetTemplate();
        final SheetTemplate2 sheetTemplate2 = examDataV1.getSheetTemplate2();
        if (sheetTemplate.getAnswerOptions().size() != sheetTemplate2.getAnswerOptions().size()) {
            return false;
        }
        for (int i = 1; i < sheetTemplate.getAnswerOptions().size(); ++i) {
            if (sheetTemplate.getAnswerOptions().get(i).type != sheetTemplate2.getAnswerOptions().get(i).type) {
                return false;
            }
        }
        return true;
    }
    
    public final void p1() {
        final int int1 = this.m.getInt("count_saveScan", 0);
        final int int2 = this.m.getInt("count_cancelScan", 0);
        final int int3 = this.m.getInt("last_rate_minutes", 0);
        final int n = a91.n();
        if (n - int3 >= 86400) {
            if (int1 + int2 >= a91.A()) {
                final ReviewManager create = ReviewManagerFactory.create((Context)this);
                create.requestReviewFlow().addOnCompleteListener((OnCompleteListener)new wx(this, create, n));
            }
        }
    }
    
    public final void q0() {
        if (!this.m0()) {
            return;
        }
        final boolean b = a91.c((Context)this.l) ^ true;
        if (!b) {
            this.f1(b);
        }
        else {
            this.c1(b);
        }
    }
    
    public final void q1(final boolean b) {
        final ResultRepository instance = ResultRepository.getInstance((Context)this.l);
        final TemplateRepository instance2 = TemplateRepository.getInstance((Context)this.l);
        final TemplateDataJsonModel templateJson = instance2.getTemplateJson(this.i);
        final Long cloudId = templateJson.getCloudId();
        final SyncExamFile syncExamFile = new SyncExamFile();
        syncExamFile.setExamId(cloudId);
        if (!templateJson.isSynced()) {
            final SheetTemplate2 sheetTemplate = templateJson.getSheetTemplate();
            Published published;
            if (templateJson.isPublished()) {
                published = Published.PUBLISHED;
            }
            else {
                published = Published.NOT_PUBLISHED;
            }
            syncExamFile.setExam(new ExamPartialResponse(cloudId, templateJson.getLastUpdatedOn(), this.i, new gc0().s(sheetTemplate), sheetTemplate.getTemplateVersion(), published, templateJson.getSharedType(), null, templateJson.isSyncImages()));
        }
        final ArrayList<ResultDataJsonModel> allNotSyncedResults = instance.getAllNotSyncedResults(this.i);
        final ArrayList<StudentResultPartialResponse> studentResults = new ArrayList<StudentResultPartialResponse>();
        for (final ResultDataJsonModel resultDataJsonModel : allNotSyncedResults) {
            Published published2;
            if (resultDataJsonModel.isPublished()) {
                published2 = Published.PUBLISHED;
            }
            else {
                published2 = Published.NOT_PUBLISHED;
            }
            studentResults.add(new StudentResultPartialResponse(resultDataJsonModel.getRollNo(), 0L, resultDataJsonModel.getResultItemString(), 0, published2));
        }
        syncExamFile.setStudentResults(studentResults);
        new PostPendingReports(cloudId, syncExamFile, (Context)this.l, new zg(this, templateJson, instance2, allNotSyncedResults, instance, b) {
            public final TemplateDataJsonModel a;
            public final TemplateRepository b;
            public final ArrayList c;
            public final ResultRepository d;
            public final boolean e;
            public final ExamDetailActivity f;
            
            @Override
            public void a(final boolean b, final int n, Object o) {
                if (b) {
                    final SyncExamFile syncExamFile = (SyncExamFile)o;
                    if (!this.a.isSynced()) {
                        this.a.setLastUpdatedOn(syncExamFile.getSyncedOn());
                    }
                    this.a.setLastSyncedOn(syncExamFile.getSyncedOn());
                    this.a.setSynced(true);
                    this.b.saveOrUpdateTemplateJsonAsSynced(this.a);
                    for (final ResultDataJsonModel resultDataJsonModel : this.c) {
                        resultDataJsonModel.setSynced(true);
                        this.d.saveOrUpdateResultJsonAsSynced(resultDataJsonModel);
                    }
                    this.f.n0(syncExamFile.getRollSet(), this.e);
                    o = new Intent("UPDATE_REPORT_LIST");
                    ((Intent)o).putExtra("data_changed", true);
                    ((Context)ExamDetailActivity.g0(this.f)).sendBroadcast((Intent)o);
                }
            }
        });
    }
    
    public final void r0(final boolean b) {
        final TemplateDataJsonModel templateJson = TemplateRepository.getInstance((Context)this.l).getTemplateJson(this.i);
        new qa0(templateJson.getCloudId(), templateJson.getLastSyncedOn(), (Context)this.l, new zg(this, templateJson, b) {
            public final TemplateDataJsonModel a;
            public final boolean b;
            public final ExamDetailActivity c;
            
            @Override
            public void a(final boolean b, final int n, final Object o) {
                if (b) {
                    final SyncExamFile syncExamFile = (SyncExamFile)o;
                    final TemplateDataJsonModel templateJson = TemplateRepository.getInstance((Context)ExamDetailActivity.g0(this.c)).getTemplateJson(this.c.i);
                    if (syncExamFile.getExam() != null && templateJson.isSynced()) {
                        final ExamPartialResponse exam = syncExamFile.getExam();
                        this.a.setSheetTemplate((SheetTemplate2)new gc0().j(exam.getTemplate(), SheetTemplate2.class));
                        this.a.setPublished(exam.getPublished() == Published.PUBLISHED);
                        this.a.setLastUpdatedOn(exam.getUpdatedOn());
                        TemplateRepository.getInstance((Context)ExamDetailActivity.g0(this.c)).saveOrUpdateTemplateJsonAsSynced(this.a);
                    }
                    new ya(new Runnable(this, syncExamFile) {
                        public final SyncExamFile a;
                        public final ExamDetailActivity$a b;
                        
                        @Override
                        public void run() {
                            if (this.a.getStudentResults() != null) {
                                final ResultRepository instance = ResultRepository.getInstance((Context)ExamDetailActivity.g0(this.b.c));
                                for (final StudentResultPartialResponse studentResultPartialResponse : this.a.getStudentResults()) {
                                    final ResultDataJsonModel result = instance.getResult(this.b.c.i, studentResultPartialResponse.getRollNo());
                                    if (result != null && result.isSynced()) {
                                        result.setResultItem((ResultItem)new gc0().j(studentResultPartialResponse.getReport(), ResultItem.class));
                                        result.setSynced(true);
                                        instance.saveOrUpdateResultJsonAsSynced(result);
                                    }
                                    if (result == null) {
                                        final ResultDataJsonModel resultDataJsonModel = new ResultDataJsonModel(this.b.c.i.getExamName(), studentResultPartialResponse.getRollNo(), this.b.c.i.getClassName(), this.b.c.i.getExamDate(), (ResultItem)new gc0().j(studentResultPartialResponse.getReport(), ResultItem.class), false, false);
                                        resultDataJsonModel.setSynced(true);
                                        instance.saveOrUpdateResultJsonAsSynced(resultDataJsonModel);
                                    }
                                }
                            }
                        }
                    }, new Runnable(this) {
                        public final ExamDetailActivity$a a;
                        
                        @Override
                        public void run() {
                            final zg a = this.a;
                            a.c.q1(a.b);
                        }
                    }).execute((Object[])new Void[0]);
                }
            }
        });
    }
    
    public final void r1(final boolean b) {
        SyncImageService.b((Context)this.l);
        final ReportPendingActionFile t = a91.t((Context)this.l);
        if (t == null) {
            this.r0(b);
        }
        else {
            new DeleteSyncReports(t, (Context)this.l, new zg(this, b) {
                public final boolean a;
                public final ExamDetailActivity b;
                
                @Override
                public void a(final boolean b, final int n, final Object o) {
                    if (b) {
                        a91.K((Context)ExamDetailActivity.g0(this.b), null);
                        this.b.r0(this.a);
                    }
                }
            });
        }
    }
    
    public final ArrayList s0() {
        final ArrayList list = new ArrayList();
        final HashSet<Integer> allUniqueRollNumberForExam = ResultRepository.getInstance((Context)this.l).getAllUniqueRollNumberForExam(this.i);
        for (final StudentDataModel studentDataModel : ClassRepository.getInstance((Context)this.l).getStudents(this.i.getClassName())) {
            if (!allUniqueRollNumberForExam.contains(studentDataModel.getRollNO())) {
                final String[] e = new String[4];
                for (int i = 0; i < 4; ++i) {
                    e[i] = "";
                }
                e[0] = this.i.getExamName();
                final StringBuilder sb = new StringBuilder();
                sb.append(studentDataModel.getRollNO());
                sb.append("");
                e[2] = sb.toString();
                e[3] = studentDataModel.getStudentName();
                list.add(e);
            }
        }
        return list;
    }
    
    public final void t0(final ExamId examId) {
        final Intent intent = new Intent((Context)this.l, (Class)ExamSettingsActivity.class);
        intent.putExtra("EXAM_ID", (Serializable)examId);
        ((Context)this).startActivity(intent);
    }
    
    public final void u0() {
        final Intent intent = new Intent((Context)this.l, (Class)SmsActivity.class);
        intent.putExtra("EXAM_ID", (Serializable)this.i);
        ((Context)this).startActivity(intent);
    }
    
    public final void v0() {
        final Intent intent = new Intent((Context)this.l, (Class)SplashActivity.class);
        intent.putExtra("BLOCK_NO_SYNC", true);
        intent.setFlags(268468224);
        ((Context)this).startActivity(intent);
    }
    
    public final void w0() {
        final Intent intent = new Intent((Context)this.l, (Class)ResultActivity.class);
        intent.putExtra("EXAM_ID", (Serializable)this.i);
        ((Context)this).startActivity(intent);
    }
    
    public final void x0() {
        final Intent intent = new Intent((Context)this.l, (Class)ViewTemplateActivity.class);
        intent.putExtra("EXAM_ID", (Serializable)this.i);
        ((Context)this).startActivity(intent);
    }
    
    public final void y0() {
        final Intent intent = new Intent("android.intent.action.GET_CONTENT");
        intent.setType("*/*");
        intent.addCategory("android.intent.category.OPENABLE");
        intent.putExtra("android.intent.extra.LOCAL_ONLY", true);
        this.startActivityForResult(Intent.createChooser(intent, (CharSequence)"Choose a file"), 11);
    }
    
    public final void z0() {
        this.R0();
        this.T0();
        this.K0();
        this.V0();
        this.U0();
        this.N0();
        this.M0();
        this.S0();
        this.L0();
        this.O0();
        this.P0();
        this.Q0();
        this.findViewById(2131296478).setOnClickListener((View$OnClickListener)new View$OnClickListener(this) {
            public final ExamDetailActivity a;
            
            public void onClick(final View view) {
                if (this.a.k0()) {
                    this.a.E0();
                }
            }
        });
        this.findViewById(2131296485).setOnClickListener((View$OnClickListener)new View$OnClickListener(this) {
            public final ExamDetailActivity a;
            
            public void onClick(final View view) {
                this.a.w0();
            }
        });
    }
    
    public class s0 extends AsyncTask
    {
        public File a;
        public final ExamDetailActivity b;
        
        public s0(final ExamDetailActivity b) {
            this.b = b;
        }
        
        public Void a(Void... sheetTemplate) {
            final File externalFilesDir = ((Context)this.b).getApplicationContext().getExternalFilesDir(ok.c);
            if (!externalFilesDir.exists()) {
                externalFilesDir.mkdirs();
            }
            final StringBuilder sb = new StringBuilder();
            final StringBuilder sb2 = new StringBuilder();
            sb2.append(this.b.i.getExamName());
            sb2.append("_");
            sb2.append(this.b.i.getExamDate());
            sb.append(sb2.toString().replaceAll("[^a-zA-Z0-9_-]", "_"));
            sb.append(".exm");
            this.a = new File(externalFilesDir, sb.toString());
            sheetTemplate = (IOException)TemplateRepository.getInstance((Context)ExamDetailActivity.g0(this.b)).getTemplateJson(this.b.i).getSheetTemplate();
            final ArrayList<ResultDataJsonModel> allResultJson = ResultRepository.getInstance((Context)ExamDetailActivity.g0(this.b)).getAllResultJson(this.b.i);
            IOException ex5 = null;
            Label_0272: {
                byte[] b = null;
                Object o;
                try {
                    b = yx.b(yx.d((SheetTemplate2)sheetTemplate, allResultJson, this.b.i));
                    o = (sheetTemplate = (IOException)new FileOutputStream(this.a));
                    try {
                        final Object o2 = o;
                        final byte[] array = b;
                        ((FileOutputStream)o2).write(array);
                        sheetTemplate = (IOException)o;
                        final IOException ex = (IOException)o;
                        ((OutputStream)ex).flush();
                        sheetTemplate = (IOException)o;
                        final IOException ex2 = (IOException)o;
                        ((FileOutputStream)ex2).close();
                    }
                    catch (final Exception ex3) {}
                }
                catch (final Exception ex3) {
                    o = null;
                }
                finally {
                    final IOException ex4;
                    sheetTemplate = ex4;
                    break Label_0272;
                }
                try {
                    final Object o2 = o;
                    final byte[] array = b;
                    ((FileOutputStream)o2).write(array);
                    sheetTemplate = (IOException)o;
                    final IOException ex = (IOException)o;
                    ((OutputStream)ex).flush();
                    sheetTemplate = (IOException)o;
                    final IOException ex2 = (IOException)o;
                    ((FileOutputStream)ex2).close();
                    Label_0265: {
                        return null;
                    }
                    sheetTemplate = (IOException)o;
                    final Exception ex3;
                    ex3.printStackTrace();
                    sheetTemplate = (IOException)o;
                    this.a.deleteOnExit();
                    iftrue(Label_0265:)(o == null);
                    try {
                        ((OutputStream)o).flush();
                        ((FileOutputStream)o).close();
                    }
                    catch (final IOException sheetTemplate) {
                        sheetTemplate.printStackTrace();
                    }
                    return null;
                }
                finally {
                    ex5 = sheetTemplate;
                    final IOException ex6;
                    sheetTemplate = ex6;
                }
            }
            if (ex5 != null) {
                try {
                    ((OutputStream)ex5).flush();
                    ((FileOutputStream)ex5).close();
                }
                catch (final IOException ex7) {
                    ex7.printStackTrace();
                }
            }
            throw sheetTemplate;
        }
        
        public void b(final Void void1) {
            super.onPostExecute((Object)void1);
            if (this.a.exists()) {
                this.b.h1();
            }
        }
    }
}

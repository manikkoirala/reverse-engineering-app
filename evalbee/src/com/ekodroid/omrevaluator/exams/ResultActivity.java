// 
// Decompiled by Procyon v0.6.0
// 

package com.ekodroid.omrevaluator.exams;

import android.os.AsyncTask;
import android.content.IntentFilter;
import androidx.appcompat.widget.Toolbar;
import com.ekodroid.omrevaluator.clients.SyncClients.Models.ReportPendingActionFile;
import com.ekodroid.omrevaluator.clients.SyncClients.DeleteSyncReports;
import com.ekodroid.omrevaluator.activities.Services.SyncImageService;
import com.ekodroid.omrevaluator.clients.SyncClients.PostPendingReports;
import com.google.android.play.core.review.ReviewManagerFactory;
import com.google.android.gms.ads.LoadAdError;
import com.google.android.gms.ads.rewarded.RewardItem;
import com.google.android.gms.ads.OnUserEarnedRewardListener;
import com.ekodroid.omrevaluator.more.ProductAndServicesActivity;
import android.view.View$OnClickListener;
import com.ekodroid.omrevaluator.database.FileDataModel;
import com.google.gson.reflect.TypeToken;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView$OnItemClickListener;
import android.content.res.Resources;
import com.ekodroid.omrevaluator.util.DataModels.SortResultList;
import android.widget.ListAdapter;
import java.io.Serializable;
import com.ekodroid.omrevaluator.templateui.scansheet.ScanPaperActivity;
import android.content.Intent;
import com.ekodroid.omrevaluator.activities.SplashActivity;
import com.ekodroid.omrevaluator.exams.models.RollNoListDataModel;
import com.ekodroid.omrevaluator.clients.SyncClients.Models.ExamPartialResponse;
import com.ekodroid.omrevaluator.templateui.scanner.ResultItem;
import com.ekodroid.omrevaluator.clients.SyncClients.Models.StudentResultPartialResponse;
import com.ekodroid.omrevaluator.clients.SyncClients.Models.Published;
import com.ekodroid.omrevaluator.clients.SyncClients.Models.SyncExamFile;
import com.ekodroid.omrevaluator.database.TemplateDataJsonModel;
import com.ekodroid.omrevaluator.database.repositories.TemplateRepository;
import com.ekodroid.omrevaluator.database.ResultDataJsonModel;
import com.ekodroid.omrevaluator.database.repositories.ResultRepository;
import android.content.pm.PackageManager$NameNotFoundException;
import android.provider.Settings$Secure;
import com.ekodroid.omrevaluator.database.StudentDataModel;
import java.util.Iterator;
import com.ekodroid.omrevaluator.database.repositories.ClassRepository;
import com.google.android.play.core.tasks.OnCompleteListener;
import android.app.Activity;
import com.google.android.play.core.review.ReviewInfo;
import android.os.Bundle;
import android.content.Context;
import java.util.Set;
import com.google.android.play.core.tasks.Task;
import com.google.android.play.core.review.ReviewManager;
import android.content.SharedPreferences;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.ekodroid.omrevaluator.templateui.models.ExamId;
import android.content.BroadcastReceiver;
import android.app.ProgressDialog;
import android.widget.TextView;
import com.ekodroid.omrevaluator.templateui.models.SheetTemplate2;
import java.util.ArrayList;
import android.view.ViewGroup;
import android.widget.ListView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

public class ResultActivity extends v5
{
    public SwipeRefreshLayout A;
    public final int c;
    public int d;
    public ListView e;
    public ViewGroup f;
    public n3 g;
    public ArrayList h;
    public SheetTemplate2 i;
    public TextView j;
    public TextView k;
    public ProgressDialog l;
    public String m;
    public BroadcastReceiver n;
    public BroadcastReceiver p;
    public ud1 q;
    public double t;
    public ExamId v;
    public FirebaseAnalytics w;
    public int x;
    public ResultActivity y;
    public SharedPreferences z;
    
    public ResultActivity() {
        this.c = 2;
        this.d = 0;
        this.m = "";
        this.x = 999;
        this.y = this;
    }
    
    public static /* synthetic */ ResultActivity J(final ResultActivity resultActivity) {
        return resultActivity.y;
    }
    
    public final void P(final int i, final String s) {
        final y01 y01 = new y01(this) {
            public final ResultActivity a;
            
            @Override
            public void a(final Object o) {
                this.a.d0();
            }
        };
        final ResultActivity y2 = this.y;
        final StringBuilder sb = new StringBuilder();
        sb.append(i);
        sb.append("");
        new w3((Context)y2, "res", y01, sb.toString(), "", s, "", "").show();
    }
    
    public final void Q(final ArrayList list) {
        for (final ye1 ye1 : list) {
            final StudentDataModel student = ClassRepository.getInstance((Context)this.y).getStudent(ye1.g(), this.v.getClassName());
            if (student != null) {
                ye1.m(student.getStudentName());
                ye1.p(false);
            }
            else {
                ye1.m(" - - - ");
            }
        }
    }
    
    public final boolean R() {
        if (sl.checkSelfPermission((Context)this, "android.permission.CAMERA") == 0) {
            return true;
        }
        h2.g(this, new String[] { "android.permission.CAMERA" }, 2);
        return false;
    }
    
    public final boolean S() {
        final String string = Settings$Secure.getString(((Context)this).getContentResolver(), "android_id");
        final boolean b = false;
        int versionCode;
        try {
            versionCode = ((Context)this).getPackageManager().getPackageInfo(((Context)this).getPackageName(), 0).versionCode;
        }
        catch (final PackageManager$NameNotFoundException ex) {
            ((Throwable)ex).printStackTrace();
            versionCode = 999;
        }
        boolean b2 = b;
        if (a91.E(((Context)this).getSharedPreferences("MyPref", 0), string, versionCode)) {
            b2 = b;
            if (gk.c()) {
                b2 = true;
            }
        }
        return b2;
    }
    
    public final void T(final Set set, final boolean b) {
        final ResultRepository instance = ResultRepository.getInstance((Context)this.y);
        final long allResultCount = instance.getAllResultCount(this.v);
        if (set.size() == allResultCount) {
            return;
        }
        if (allResultCount > set.size()) {
            for (final ResultDataJsonModel resultDataJsonModel : instance.getAllResultJson(this.v)) {
                if (!set.contains(resultDataJsonModel.getRollNo())) {
                    instance.deleteStudentResult(this.v, resultDataJsonModel.getRollNo());
                    instance.deleteSheetImages(this.v, resultDataJsonModel.getRollNo());
                }
            }
            return;
        }
        if (set.size() > allResultCount && !b) {
            final TemplateRepository instance2 = TemplateRepository.getInstance((Context)this.y);
            final TemplateDataJsonModel templateJson = instance2.getTemplateJson(this.v);
            templateJson.setLastSyncedOn(0L);
            instance2.saveOrUpdateTemplateJsonAsSynced(templateJson);
            this.l0(true);
        }
    }
    
    public final void U(final boolean b) {
        final TemplateDataJsonModel templateJson = TemplateRepository.getInstance((Context)this.y).getTemplateJson(this.v);
        new qa0(templateJson.getCloudId(), templateJson.getLastSyncedOn(), (Context)this.y, new zg(this, templateJson, b) {
            public final TemplateDataJsonModel a;
            public final boolean b;
            public final ResultActivity c;
            
            @Override
            public void a(final boolean b, final int n, final Object o) {
                if (b) {
                    final SyncExamFile syncExamFile = (SyncExamFile)o;
                    final TemplateDataJsonModel templateJson = TemplateRepository.getInstance((Context)ResultActivity.J(this.c)).getTemplateJson(this.c.v);
                    if (syncExamFile.getExam() != null && templateJson.isSynced()) {
                        final ExamPartialResponse exam = syncExamFile.getExam();
                        this.a.setSheetTemplate((SheetTemplate2)new gc0().j(exam.getTemplate(), SheetTemplate2.class));
                        this.a.setPublished(exam.getPublished() == Published.PUBLISHED);
                        this.a.setLastUpdatedOn(exam.getUpdatedOn());
                        TemplateRepository.getInstance((Context)ResultActivity.J(this.c)).saveOrUpdateTemplateJsonAsSynced(this.a);
                    }
                    new ya(new Runnable(this, syncExamFile) {
                        public final SyncExamFile a;
                        public final ResultActivity$l b;
                        
                        @Override
                        public void run() {
                            if (this.a.getStudentResults() != null) {
                                final ResultRepository instance = ResultRepository.getInstance((Context)ResultActivity.J(this.b.c));
                                for (final StudentResultPartialResponse studentResultPartialResponse : this.a.getStudentResults()) {
                                    final ResultDataJsonModel result = instance.getResult(this.b.c.v, studentResultPartialResponse.getRollNo());
                                    if (result != null && result.isSynced()) {
                                        result.setResultItem((ResultItem)new gc0().j(studentResultPartialResponse.getReport(), ResultItem.class));
                                        result.setSynced(true);
                                        instance.saveOrUpdateResultJsonAsSynced(result);
                                    }
                                    if (result == null) {
                                        final ResultDataJsonModel resultDataJsonModel = new ResultDataJsonModel(this.b.c.v.getExamName(), studentResultPartialResponse.getRollNo(), this.b.c.v.getClassName(), this.b.c.v.getExamDate(), (ResultItem)new gc0().j(studentResultPartialResponse.getReport(), ResultItem.class), false, false);
                                        resultDataJsonModel.setSynced(true);
                                        instance.saveOrUpdateResultJsonAsSynced(resultDataJsonModel);
                                    }
                                }
                            }
                        }
                    }, new Runnable(this) {
                        public final ResultActivity$l a;
                        
                        @Override
                        public void run() {
                            final zg a = this.a;
                            a.c.k0(a.b);
                        }
                    }).execute((Object[])new Void[0]);
                }
            }
        });
    }
    
    public final ArrayList V(final SheetTemplate2 sheetTemplate2) {
        final ArrayList<ResultDataJsonModel> allResultJson = ResultRepository.getInstance((Context)this.y).getAllResultJson(this.v);
        final ArrayList list = new ArrayList();
        for (final ResultDataJsonModel resultDataJsonModel : allResultJson) {
            if (resultDataJsonModel != null) {
                final ResultItem resultItem = resultDataJsonModel.getResultItem(sheetTemplate2);
                final int p = ve1.p(resultItem);
                final int q = ve1.q(resultItem);
                final int s = ve1.s(resultItem);
                final double totalMarks = resultItem.getTotalMarks();
                list.add(new ye1(resultDataJsonModel.getRollNo(), totalMarks, ve1.i(totalMarks, sheetTemplate2.getGradeLevels()), p, q, s, resultDataJsonModel.isSmsSent(), resultDataJsonModel.isSynced(), resultDataJsonModel.isPublished()));
            }
        }
        ve1.a(list, sheetTemplate2.getRankingMethod());
        this.Q(list);
        return list;
    }
    
    public final ArrayList W(final ArrayList list) {
        final ArrayList list2 = new ArrayList();
        if (list != null && list.size() > 0) {
            for (final ye1 ye1 : list) {
                list2.add(new RollNoListDataModel(ye1.g(), ye1.f()));
            }
        }
        return list2;
    }
    
    public final void X() {
        final Intent intent = new Intent((Context)this.y, (Class)SplashActivity.class);
        intent.putExtra("BLOCK_NO_SYNC", true);
        intent.setFlags(268468224);
        ((Context)this).startActivity(intent);
    }
    
    public final void Y() {
        this.n = new BroadcastReceiver(this) {
            public final ResultActivity a;
            
            public void onReceive(final Context context, final Intent intent) {
                final ResultActivity a = this.a;
                a.m0(a.h);
                this.a.c0();
            }
        };
        this.p = new BroadcastReceiver(this) {
            public final ResultActivity a;
            
            public void onReceive(final Context context, final Intent intent) {
                this.a.d0();
            }
        };
    }
    
    public final void b0() {
        if (a91.D(30, this.z, this.x, this.m) && this.S()) {
            FirebaseAnalytics.getInstance((Context)this.y).a("CONNECTION_SUCCESS_VALID", null);
            if (a91.a((Context)this.y)) {
                final Intent intent = new Intent((Context)this.y, (Class)ScanPaperActivity.class);
                intent.putExtra("EXAM_ID", (Serializable)this.v);
                intent.putExtra("START_SCAN", true);
                ((Context)this).startActivity(intent);
            }
            else {
                this.i0();
            }
        }
        else {
            FirebaseAnalytics.getInstance((Context)this.y).a("CONNECTION_SUCCESS_INVALID", null);
            this.X();
        }
    }
    
    public final void c0() {
        this.e0();
        final ArrayList h = this.h;
        TextView textView;
        String string;
        if (h != null && h.size() > 0) {
            this.findViewById(2131296747).setVisibility(4);
            ((View)this.e).setVisibility(0);
            final n3 n3 = new n3(this.y, this.v, this.h);
            this.g = n3;
            this.e.setAdapter((ListAdapter)n3);
            textView = this.k;
            final StringBuilder sb = new StringBuilder();
            sb.append(this.h.size());
            sb.append("");
            string = sb.toString();
        }
        else {
            this.findViewById(2131296747).setVisibility(0);
            ((View)this.e).setVisibility(4);
            textView = this.k;
            string = "---";
        }
        textView.setText((CharSequence)string);
        final TextView j = this.j;
        final StringBuilder sb2 = new StringBuilder();
        sb2.append(this.t);
        sb2.append("");
        j.setText((CharSequence)sb2.toString());
    }
    
    public final void d0() {
        if (this.v != null && this.n() != null) {
            final TemplateDataJsonModel templateJson = TemplateRepository.getInstance((Context)this.y).getTemplateJson(this.v);
            if (templateJson == null) {
                o4.c(this.w, "RA_OC_TEMPLATE_NULL", this.m);
                this.finish();
                return;
            }
            this.i = templateJson.getSheetTemplate();
            this.n().t(this.v.getExamName());
            new m(null).execute((Object[])new Void[0]);
        }
    }
    
    public final void e0() {
        Enum<SortResultList.ResultSortBy> rank = SortResultList.ResultSortBy.RANK;
        final ud1 q = this.q;
        boolean b;
        if (q != null) {
            b = q.b;
            final SortResultList.ResultSortBy a = q.a;
            ((View)this.f).findViewById(2131296734).setVisibility(0);
            final TextView textView = (TextView)((View)this.f).findViewById(2131297312);
            final StringBuilder sb = new StringBuilder();
            final SortResultList.ResultSortBy a2 = this.q.a;
            Resources resources;
            int n;
            if (a2 == rank) {
                resources = this.getResources();
                n = 2131886726;
            }
            else if (a2 == SortResultList.ResultSortBy.MARK) {
                resources = this.getResources();
                n = 2131886412;
            }
            else if (a2 == SortResultList.ResultSortBy.NAME) {
                resources = this.getResources();
                n = 2131886649;
            }
            else {
                resources = this.getResources();
                n = 2131886747;
            }
            sb.append(resources.getString(n));
            sb.append(" - ");
            Resources resources2;
            int n2;
            if (this.q.b) {
                resources2 = this.getResources();
                n2 = 2131886146;
            }
            else {
                resources2 = this.getResources();
                n2 = 2131886243;
            }
            sb.append(resources2.getString(n2));
            textView.setText((CharSequence)sb.toString());
            rank = a;
        }
        else {
            ((View)this.f).findViewById(2131296734).setVisibility(8);
            b = true;
        }
        final ArrayList h = this.h;
        if (h != null && h.size() > 0) {
            new SortResultList().j((SortResultList.ResultSortBy)rank, this.h, b);
        }
    }
    
    public final void f0() {
        (this.A = (SwipeRefreshLayout)this.findViewById(2131297134)).setColorSchemeColors(this.getResources().getIntArray(2130903043));
        this.A.setOnRefreshListener((SwipeRefreshLayout.j)new SwipeRefreshLayout.j(this) {
            public final ResultActivity a;
            
            @Override
            public void a() {
                final TemplateDataJsonModel templateJson = TemplateRepository.getInstance((Context)ResultActivity.J(this.a)).getTemplateJson(this.a.v);
                if (templateJson != null && templateJson.isCloudSyncOn()) {
                    this.a.l0(false);
                }
                this.a.d0();
            }
        });
        ((AdapterView)this.e).setOnItemClickListener((AdapterView$OnItemClickListener)new AdapterView$OnItemClickListener(this) {
            public final ResultActivity a;
            
            public void onItemClick(final AdapterView adapterView, final View view, int index, final long n) {
                --index;
                if (view.getId() == 2131296409L) {
                    this.a.P(this.a.h.get(index).g(), this.a.v.getClassName());
                }
                else {
                    final ResultActivity a = this.a;
                    final ArrayList g = a.W(a.h);
                    if (g.size() < 1) {
                        return;
                    }
                    if (ResultRepository.getInstance((Context)ResultActivity.J(this.a)).saveOrUpdateFile(new FileDataModel("rollno_list_temp_file", new gc0().t(g, new TypeToken<ArrayList<RollNoListDataModel>>(this) {
                        public final ResultActivity$12 a;
                    }.getType()).getBytes()))) {
                        final Intent intent = new Intent((Context)ResultActivity.J(this.a), (Class)StudentReportActivity.class);
                        intent.putExtra("SELECTED_POSITION", index);
                        intent.putExtra("ROLLNO_LIST_FILE_NAME", "rollno_list_temp_file");
                        intent.putExtra("EXAM_ID", (Serializable)this.a.v);
                        ((Context)this.a).startActivity(intent);
                    }
                }
            }
        });
    }
    
    public final void g0() {
        ((View)this.f).findViewById(2131296436).setOnClickListener((View$OnClickListener)new View$OnClickListener(this) {
            public final ResultActivity a;
            
            public void onClick(final View view) {
                final ResultActivity a = this.a;
                a.q = null;
                a.c0();
            }
        });
        ((View)this.f).findViewById(2131296665).setOnClickListener((View$OnClickListener)new View$OnClickListener(this) {
            public final ResultActivity a;
            
            public void onClick(final View view) {
                new l10((Context)ResultActivity.J(this.a), this.a.q, new j10(this) {
                    public final ResultActivity$h a;
                    
                    @Override
                    public void a(final Object o) {
                        if (o instanceof ud1) {
                            final ResultActivity a = this.a.a;
                            a.q = (ud1)o;
                            a.c0();
                        }
                    }
                }).show();
            }
        });
    }
    
    public final void h0() {
        this.findViewById(2131296472).setOnClickListener((View$OnClickListener)new View$OnClickListener(this) {
            public final ResultActivity a;
            
            public void onClick(final View view) {
                if (this.a.R()) {
                    this.a.b0();
                }
            }
        });
    }
    
    public final void i0() {
        FirebaseAnalytics.getInstance((Context)this.y).a("REWARD_SCAN_DIALOG_SHOWN", null);
        new kf1((Context)this.y, ((Context)this).getString(2131886581), ((Context)this).getString(2131886586), new y01(this) {
            public final ResultActivity a;
            
            @Override
            public void a(final Object o) {
                if (o != null && o.equals("UPGRADE")) {
                    ((Context)this.a).startActivity(new Intent((Context)ResultActivity.J(this.a), (Class)ProductAndServicesActivity.class));
                }
                if (o != null && o.equals("WATCH_VIDEO")) {
                    y3.d(ResultActivity.J(this.a), new OnUserEarnedRewardListener(this) {
                        public final ResultActivity$d a;
                        
                        @Override
                        public void onUserEarnedReward(final RewardItem rewardItem) {
                            dj1.a((Context)ResultActivity.J(this.a.a), 50);
                            FirebaseAnalytics.getInstance((Context)ResultActivity.J(this.a.a)).a("REWARD_SCAN_GRANTED", null);
                            a91.H((Context)ResultActivity.J(this.a.a), "Extra scan rewarded", 2131230927, 2131231085);
                        }
                    }, (y3.c)new y3.c(this) {
                        public final ResultActivity$d a;
                        
                        @Override
                        public void a(final LoadAdError loadAdError) {
                            final ResultActivity a = this.a.a;
                            final int d = a.d + 1;
                            a.d = d;
                            final ResultActivity j = ResultActivity.J(a);
                            if (d > 1) {
                                dj1.a((Context)j, 20);
                                FirebaseAnalytics.getInstance((Context)ResultActivity.J(this.a.a)).a("REWARD_SCAN_LOAD_FAIL_SKIP", null);
                                return;
                            }
                            FirebaseAnalytics.getInstance((Context)j).a("REWARD_SCAN_LOAD_FAIL", null);
                            a91.H((Context)ResultActivity.J(this.a.a), ((Context)this.a.a).getString(2131886446), 2131230909, 2131231086);
                        }
                    });
                }
            }
        });
    }
    
    public final void j0() {
        final int int1 = this.z.getInt("count_saveScan", 0);
        final int int2 = this.z.getInt("count_cancelScan", 0);
        final int int3 = this.z.getInt("last_rate_minutes", 0);
        final int n = a91.n();
        if (n - int3 >= 86400) {
            if (int1 + int2 >= a91.A()) {
                final ReviewManager create = ReviewManagerFactory.create((Context)this);
                create.requestReviewFlow().addOnCompleteListener((OnCompleteListener)new te1(this, create, n));
            }
        }
    }
    
    public final void k0(final boolean b) {
        final ResultRepository instance = ResultRepository.getInstance((Context)this.y);
        final TemplateRepository instance2 = TemplateRepository.getInstance((Context)this.y);
        final TemplateDataJsonModel templateJson = instance2.getTemplateJson(this.v);
        final Long cloudId = templateJson.getCloudId();
        final SyncExamFile syncExamFile = new SyncExamFile();
        syncExamFile.setExamId(cloudId);
        if (!templateJson.isSynced()) {
            final SheetTemplate2 sheetTemplate = templateJson.getSheetTemplate();
            Published published;
            if (templateJson.isPublished()) {
                published = Published.PUBLISHED;
            }
            else {
                published = Published.NOT_PUBLISHED;
            }
            syncExamFile.setExam(new ExamPartialResponse(cloudId, templateJson.getLastUpdatedOn(), this.v, new gc0().s(sheetTemplate), sheetTemplate.getTemplateVersion(), published, templateJson.getSharedType(), null, templateJson.isSyncImages()));
        }
        final ArrayList<ResultDataJsonModel> allNotSyncedResults = instance.getAllNotSyncedResults(this.v);
        final ArrayList<StudentResultPartialResponse> studentResults = new ArrayList<StudentResultPartialResponse>();
        for (final ResultDataJsonModel resultDataJsonModel : allNotSyncedResults) {
            Published published2;
            if (resultDataJsonModel.isPublished()) {
                published2 = Published.PUBLISHED;
            }
            else {
                published2 = Published.NOT_PUBLISHED;
            }
            studentResults.add(new StudentResultPartialResponse(resultDataJsonModel.getRollNo(), 0L, resultDataJsonModel.getResultItemString(), 0, published2));
        }
        syncExamFile.setStudentResults(studentResults);
        new PostPendingReports(cloudId, syncExamFile, (Context)this.y, new zg(this, templateJson, instance2, allNotSyncedResults, instance, b) {
            public final TemplateDataJsonModel a;
            public final TemplateRepository b;
            public final ArrayList c;
            public final ResultRepository d;
            public final boolean e;
            public final ResultActivity f;
            
            @Override
            public void a(final boolean b, final int n, Object o) {
                if (b) {
                    final SyncExamFile syncExamFile = (SyncExamFile)o;
                    if (!this.a.isSynced()) {
                        this.a.setLastUpdatedOn(syncExamFile.getSyncedOn());
                    }
                    this.a.setLastSyncedOn(syncExamFile.getSyncedOn());
                    this.a.setSynced(true);
                    this.b.saveOrUpdateTemplateJsonAsSynced(this.a);
                    for (final ResultDataJsonModel resultDataJsonModel : this.c) {
                        resultDataJsonModel.setSynced(true);
                        this.d.saveOrUpdateResultJsonAsSynced(resultDataJsonModel);
                    }
                    this.f.T(syncExamFile.getRollSet(), this.e);
                    o = new Intent("UPDATE_REPORT_LIST");
                    ((Intent)o).putExtra("data_changed", true);
                    ((Context)ResultActivity.J(this.f)).sendBroadcast((Intent)o);
                }
            }
        });
    }
    
    public final void l0(final boolean b) {
        SyncImageService.b((Context)this.y);
        final ReportPendingActionFile t = a91.t((Context)this.y);
        if (t == null) {
            this.U(b);
        }
        else {
            new DeleteSyncReports(t, (Context)this.y, new zg(this, b) {
                public final boolean a;
                public final ResultActivity b;
                
                @Override
                public void a(final boolean b, final int n, final Object o) {
                    if (b) {
                        a91.K((Context)ResultActivity.J(this.b), null);
                        this.b.U(this.a);
                    }
                }
            });
        }
    }
    
    public final void m0(final ArrayList list) {
        for (final ye1 ye1 : list) {
            final ResultDataJsonModel result = ResultRepository.getInstance((Context)this.y).getResult(this.v, ye1.g());
            if (result != null) {
                ye1.n(result.isPublished());
                ye1.q(result.isSmsSent());
            }
        }
    }
    
    @Override
    public void onBackPressed() {
        super.onBackPressed();
        this.finish();
    }
    
    @Override
    public void onCreate(Bundle serializableExtra) {
        super.onCreate(serializableExtra);
        this.setContentView(2131492919);
        final Toolbar toolbar = (Toolbar)this.findViewById(2131297355);
        this.x(toolbar);
        toolbar.setNavigationOnClickListener((View$OnClickListener)new View$OnClickListener(this) {
            public final ResultActivity a;
            
            public void onClick(final View view) {
                this.a.finish();
            }
        });
        this.w = o4.a((Context)this);
        this.z = ((Context)this).getApplicationContext().getSharedPreferences("MyPref", 0);
        this.l = new ProgressDialog((Context)this.y, 2131951921);
        this.m = Settings$Secure.getString(((Context)this).getContentResolver(), "android_id");
        serializableExtra = (Bundle)this.getIntent().getSerializableExtra("EXAM_ID");
        if (serializableExtra == null) {
            o4.c(this.w, "RA_EXAMID_NULL", this.m);
            this.finish();
            return;
        }
        try {
            this.x = ((Context)this).getPackageManager().getPackageInfo(((Context)this).getPackageName(), 0).versionCode;
        }
        catch (final PackageManager$NameNotFoundException ex) {
            ((Throwable)ex).printStackTrace();
        }
        this.v = (ExamId)serializableExtra;
        final TemplateDataJsonModel templateJson = TemplateRepository.getInstance((Context)this.y).getTemplateJson(this.v);
        if (templateJson == null) {
            o4.c(this.w, "RA_OC_TEMPLATE_NULL", this.m);
            this.finish();
            return;
        }
        this.i = templateJson.getSheetTemplate();
        this.e = (ListView)this.findViewById(2131296844);
        final ViewGroup f = (ViewGroup)this.getLayoutInflater().inflate(2131493012, (ViewGroup)null);
        this.f = f;
        this.e.addHeaderView((View)f);
        this.j = (TextView)((View)this.f).findViewById(2131297250);
        this.k = (TextView)((View)this.f).findViewById(2131297283);
        this.h = new ArrayList();
        this.j0();
        this.Y();
        this.f0();
        this.h0();
        this.g0();
    }
    
    @Override
    public void onPause() {
        super.onPause();
        ((Context)this).unregisterReceiver(this.n);
        ((Context)this).unregisterReceiver(this.p);
    }
    
    @Override
    public void onRequestPermissionsResult(final int n, final String[] array, final int[] array2) {
        super.onRequestPermissionsResult(n, array, array2);
        if (array2 != null && array2.length > 0 && array2[0] == 0 && 2 == n) {
            this.b0();
        }
    }
    
    @Override
    public void onResume() {
        super.onResume();
        final TemplateDataJsonModel templateJson = TemplateRepository.getInstance((Context)this.y).getTemplateJson(this.v);
        if (templateJson != null) {
            this.i = templateJson.getSheetTemplate();
        }
        if (templateJson.isCloudSyncOn() && (a91.t((Context)this.y) != null || !templateJson.isSynced() || System.currentTimeMillis() > templateJson.getLastSyncedOn() + 180000L || ResultRepository.getInstance((Context)this.y).getAllNonSyncResultCount(this.v) > 0L)) {
            this.l0(false);
        }
        this.d0();
        sl.registerReceiver((Context)this.y, this.n, new IntentFilter("REPORT_SENT"), 4);
        sl.registerReceiver((Context)this.y, this.p, new IntentFilter("UPDATE_REPORT_LIST"), 4);
    }
    
    public class m extends AsyncTask
    {
        public final ResultActivity a;
        
        public m(final ResultActivity a) {
            this.a = a;
        }
        
        public Void a(final Void... array) {
            final ResultActivity a = this.a;
            a.h = a.V(a.i);
            final ResultActivity a2 = this.a;
            a2.t = ve1.l(a2.i);
            return null;
        }
        
        public void b(final Void void1) {
            super.onPostExecute((Object)void1);
            this.a.c0();
            this.a.A.setRefreshing(false);
        }
        
        public void onPreExecute() {
            this.a.A.setRefreshing(true);
        }
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.ekodroid.omrevaluator.exams.archived;

import android.app.AlertDialog;
import android.widget.CompoundButton;
import android.app.Dialog;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import java.io.IOException;
import java.io.OutputStream;
import java.io.FileOutputStream;
import org.apache.poi.ss.usermodel.RichTextString;
import org.apache.poi.hssf.usermodel.HSSFRichTextString;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import android.os.AsyncTask;
import android.view.MenuItem;
import android.view.Menu;
import android.provider.Settings$Secure;
import androidx.appcompat.widget.Toolbar;
import com.ekodroid.omrevaluator.templateui.ViewTemplateActivity;
import android.widget.RadioButton;
import android.content.DialogInterface;
import android.content.DialogInterface$OnClickListener;
import android.widget.CheckBox;
import android.view.LayoutInflater;
import com.google.android.material.dialog.MaterialAlertDialogBuilder;
import android.view.View$OnClickListener;
import android.widget.ListAdapter;
import android.content.res.Resources;
import java.io.Serializable;
import com.ekodroid.omrevaluator.exams.analysis.AnalysisActivity;
import com.ekodroid.omrevaluator.serializable.ExmVersions.ExamDataV1;
import com.ekodroid.omrevaluator.serializable.ExmVersions.ResultDataV1;
import java.io.InputStream;
import com.google.android.gms.common.util.IOUtils;
import java.io.FileInputStream;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView$OnItemClickListener;
import com.ekodroid.omrevaluator.templateui.scanner.ResultItem;
import com.ekodroid.omrevaluator.serializable.ResultData;
import com.ekodroid.omrevaluator.exams.models.MessageResultExcel;
import android.content.pm.PackageManager$NameNotFoundException;
import com.ekodroid.omrevaluator.database.ResultDataJsonModel;
import com.ekodroid.omrevaluator.util.DataModels.SortResultList;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.OnFailureListener;
import java.io.File;
import com.google.firebase.auth.FirebaseAuth;
import android.os.Bundle;
import com.google.firebase.analytics.FirebaseAnalytics;
import android.content.Intent;
import com.ekodroid.omrevaluator.more.ProductAndServicesActivity;
import java.util.Iterator;
import android.content.Context;
import com.ekodroid.omrevaluator.database.repositories.ClassRepository;
import com.ekodroid.omrevaluator.database.StudentDataModel;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import android.app.ProgressDialog;
import com.ekodroid.omrevaluator.templateui.models.SheetTemplate2;
import com.ekodroid.omrevaluator.templateui.models.ExamId;
import android.widget.TextView;
import android.view.ViewGroup;
import android.widget.ListView;
import java.util.ArrayList;

public class ResultArchivedActivity extends v5
{
    public ArrayList c;
    public ListView d;
    public ViewGroup e;
    public e3 f;
    public TextView g;
    public TextView h;
    public ExamId i;
    public ud1 j;
    public double k;
    public SheetTemplate2 l;
    public ProgressDialog m;
    public String n;
    public ResultArchivedActivity p;
    public String q;
    public boolean t;
    public SwipeRefreshLayout v;
    
    public ResultArchivedActivity() {
        this.c = new ArrayList();
        this.i = null;
        this.n = "";
        this.p = this;
        this.q = null;
        this.t = false;
    }
    
    public static /* synthetic */ ResultArchivedActivity B(final ResultArchivedActivity resultArchivedActivity) {
        return resultArchivedActivity.p;
    }
    
    public static /* synthetic */ String E(final ResultArchivedActivity resultArchivedActivity) {
        return resultArchivedActivity.q;
    }
    
    public final void J(final ArrayList list, final ArrayList list2, final int n, final int n2, final String s) {
        if (list2.size() < 1) {
            return;
        }
        if (list2.get(0).getRollNO() == n) {
            list2.remove(0);
            return;
        }
        if (list2.get(0).getRollNO() < n) {
            final String[] e = new String[n2];
            for (int i = 0; i < n2; ++i) {
                e[i] = "";
            }
            e[0] = s;
            final StringBuilder sb = new StringBuilder();
            sb.append(list2.get(0).getRollNO());
            sb.append("");
            e[2] = sb.toString();
            e[3] = list2.get(0).getStudentName();
            list.add(e);
            list2.remove(0);
            this.J(list, list2, n, n2, s);
        }
    }
    
    public final void K(final ArrayList list, final ArrayList list2, final int n, final String s) {
        for (int i = 0; i < list2.size(); ++i) {
            final String[] e = new String[n];
            for (int j = 0; j < n; ++j) {
                e[j] = "";
            }
            e[0] = s;
            final StringBuilder sb = new StringBuilder();
            sb.append(list2.get(i).getRollNO());
            sb.append("");
            e[2] = sb.toString();
            e[3] = list2.get(i).getStudentName();
            list.add(e);
        }
    }
    
    public final void L(final ArrayList list) {
        for (final ye1 ye1 : list) {
            final StudentDataModel student = ClassRepository.getInstance((Context)this.p).getStudent(ye1.g(), this.i.getClassName());
            if (student != null) {
                ye1.m(student.getStudentName());
                ye1.p(false);
            }
            else {
                ye1.m(" - - - ");
            }
        }
    }
    
    public final boolean M() {
        ResultArchivedActivity resultArchivedActivity;
        int n;
        if (this.c.size() < 1) {
            resultArchivedActivity = this.p;
            n = 2131886525;
        }
        else {
            if (this.l.getAnswerKeys() != null && this.l.getAnswerKeys()[0] != null) {
                return true;
            }
            resultArchivedActivity = this.p;
            n = 2131886523;
        }
        xs.c((Context)resultArchivedActivity, null, 0, n, 2131886176, 0, 0, 0);
        return false;
    }
    
    public final void N(final boolean b) {
        final ProgressDialog progressDialog = new ProgressDialog((Context)this.p);
        progressDialog.setMessage((CharSequence)((Context)this).getString(2131886467));
        ((Dialog)progressDialog).show();
        new ma0((Context)this.p, new zg(this, progressDialog, b) {
            public final ProgressDialog a;
            public final boolean b;
            public final ResultArchivedActivity c;
            
            @Override
            public void a(final boolean b, final int n, final Object o) {
                ((Dialog)this.a).dismiss();
                if (b && n == 200) {
                    final p p3 = this.c.new p(null);
                    p3.a = true;
                    p3.d = this.b;
                    p3.execute((Object[])new Void[0]);
                }
                else {
                    FirebaseAnalytics firebaseAnalytics;
                    String s;
                    if (b && n == 218) {
                        xs.c((Context)ResultArchivedActivity.B(this.c), new y01(this) {
                            public final ResultArchivedActivity$c a;
                            
                            @Override
                            public void a(final Object anObject) {
                                if ("BUY".equals(anObject)) {
                                    ((Context)this.a.c).startActivity(new Intent((Context)ResultArchivedActivity.B(this.a.c), (Class)ProductAndServicesActivity.class));
                                }
                            }
                        }, 2131886161, 2131886499, 2131886160, 2131886163, 0, 0);
                        firebaseAnalytics = FirebaseAnalytics.getInstance((Context)ResultArchivedActivity.B(this.c));
                        s = "ExcelLimit";
                    }
                    else {
                        this.c.d0(this.b);
                        firebaseAnalytics = FirebaseAnalytics.getInstance((Context)ResultArchivedActivity.B(this.c));
                        s = "ExcelDownloadError";
                    }
                    firebaseAnalytics.a(s, null);
                }
            }
        });
    }
    
    public final void O(final String str) {
        final o30 f = o30.f();
        final r30 e = FirebaseAuth.getInstance().e();
        if (e != null) {
            if (f != null) {
                final File parent = new File(((Context)this).getApplicationContext().getExternalFilesDir(ok.c), ok.d);
                if (!parent.exists()) {
                    parent.mkdirs();
                }
                final StringBuilder sb = new StringBuilder();
                sb.append(str);
                sb.append(".exm");
                final String string = sb.toString();
                final File file = new File(parent, string);
                final jq1 n = f.n();
                final StringBuilder sb2 = new StringBuilder();
                sb2.append(ok.d);
                sb2.append("/");
                sb2.append(e.O());
                sb2.append("/");
                sb2.append(string);
                final x00 g = n.a(sb2.toString()).g(file);
                this.v.setRefreshing(true);
                g.p((OnFailureListener)new OnFailureListener(this) {
                    public final ResultArchivedActivity a;
                    
                    public void onFailure(final Exception ex) {
                        this.a.v.setRefreshing(false);
                        a91.G((Context)ResultArchivedActivity.B(this.a), 2131886246, 2131230909, 2131231086);
                    }
                }).u((OnSuccessListener)new OnSuccessListener(this, str) {
                    public final String a;
                    public final ResultArchivedActivity b;
                    
                    public void a(final x00.a a) {
                        this.b.v.setRefreshing(false);
                        this.b.X(this.a);
                    }
                }).s(new o11(this) {
                    public final ResultArchivedActivity a;
                    
                    public void b(final x00.a a) {
                    }
                });
            }
        }
    }
    
    public final void P() {
        if (this.l.getAnswerKeys() != null && this.l.getAnswerKeys().length > 0) {
            new tu((Context)this.p, this.i, this.l).execute((Object[])new Void[0]);
            FirebaseAnalytics.getInstance((Context)this.p).a("DownloadKey_Archived_Csv", null);
        }
        else {
            a91.G((Context)this.p, 2131886136, 2131230909, 2131231086);
        }
    }
    
    public final void Q() {
        if (this.l.getAnswerKeys() != null && this.l.getAnswerKeys().length > 0) {
            new uu((Context)this.p, this.i, this.l).execute((Object[])new Void[0]);
            FirebaseAnalytics.getInstance((Context)this.p).a("DownloadKey_Archived", null);
        }
        else {
            a91.G((Context)this.p, 2131886136, 2131230909, 2131231086);
        }
    }
    
    public final void R() {
        if (!this.M()) {
            return;
        }
        final r30 e = FirebaseAuth.getInstance().e();
        if (e != null) {
            this.e0(e.getEmail(), e.getDisplayName());
        }
    }
    
    public final ArrayList S() {
        final ArrayList list = new ArrayList();
        final ArrayList list2 = new ArrayList();
        final Iterator iterator = this.c.iterator();
        while (iterator.hasNext()) {
            list2.add(iterator.next());
        }
        new SortResultList().a(SortResultList.ResultSortBy.ROLLNO, list2, true);
        final ArrayList<StudentDataModel> students = ClassRepository.getInstance((Context)this.p).getStudents(this.i.getClassName());
        for (int i = 0; i < list2.size(); ++i) {
            this.J(list, students, ((d8)list2.get(i)).b().g(), 4, "");
        }
        this.K(list, students, 4, "");
        return list;
    }
    
    public final ArrayList T(final ArrayList list, final ArrayList list2) {
        final ArrayList list3 = new ArrayList();
        final SortResultList list4 = new SortResultList();
        list4.j(SortResultList.ResultSortBy.ROLLNO, list2, true);
        list4.k(list, true);
        if (list.size() != list2.size()) {
            return list3;
        }
        for (int i = 0; i < list.size(); ++i) {
            list3.add(new d8((ResultDataJsonModel)list.get(i), (ye1)list2.get(i)));
        }
        return list3;
    }
    
    public final void U(final String[][] array, final String s, final String s2, final boolean b) {
        this.h0(null);
        final StringBuilder sb = new StringBuilder();
        sb.append(this.i.getExamName().replaceAll("[^a-zA-Z0-9_-]", "_"));
        sb.append("_");
        sb.append(this.i.getExamDate().replaceAll("[^a-zA-Z0-9_]", "_"));
        sb.append("_Result");
        final String string = sb.toString();
        final int n = 0;
        int versionCode;
        try {
            versionCode = ((Context)this.p).getPackageManager().getPackageInfo(((Context)this).getPackageName(), 0).versionCode;
        }
        catch (final PackageManager$NameNotFoundException ex) {
            ((Throwable)ex).printStackTrace();
            versionCode = n;
        }
        final MessageResultExcel messageResultExcel = new MessageResultExcel(s, 0, array, string, versionCode);
        this.h0(new g61(new ResultData(messageResultExcel.emailId, messageResultExcel.data, messageResultExcel.subjectName, messageResultExcel.appVersion, s2), new n52((Context)this, a91.w()).b(), this.n, new zg(this) {
            public final ResultArchivedActivity a;
            
            @Override
            public void a(final boolean b, final int n, final Object o) {
                ((Dialog)this.a.m).dismiss();
                FirebaseAnalytics firebaseAnalytics;
                String s;
                if (o != null && n == 200) {
                    a91.G((Context)ResultArchivedActivity.B(this.a), 2131886260, 2131230927, 2131231085);
                    firebaseAnalytics = FirebaseAnalytics.getInstance((Context)ResultArchivedActivity.B(this.a));
                    s = "ExcelEmailSuccessArchive";
                }
                else {
                    if (o != null && n == 218) {
                        xs.c((Context)ResultArchivedActivity.B(this.a), new y01(this) {
                            public final ResultArchivedActivity$d a;
                            
                            @Override
                            public void a(final Object anObject) {
                                if ("BUY".equals(anObject)) {
                                    ((Context)this.a.a).startActivity(new Intent((Context)ResultArchivedActivity.B(this.a.a), (Class)ProductAndServicesActivity.class));
                                }
                            }
                        }, 0, 2131886499, 2131886160, 2131886163, 0, 0);
                        return;
                    }
                    a91.G((Context)ResultArchivedActivity.B(this.a), 2131886259, 2131230909, 2131231086);
                    ((Dialog)this.a.m).dismiss();
                    firebaseAnalytics = FirebaseAnalytics.getInstance((Context)ResultArchivedActivity.B(this.a));
                    s = "ExcelEmailErrorArchive";
                }
                firebaseAnalytics.a(s, null);
            }
        }));
    }
    
    public final ArrayList V(final SheetTemplate2 sheetTemplate2, final ArrayList list) {
        final ArrayList list2 = new ArrayList();
        for (final ResultDataJsonModel resultDataJsonModel : list) {
            if (resultDataJsonModel != null) {
                final ResultItem resultItem = resultDataJsonModel.getResultItem(sheetTemplate2);
                final int p2 = ve1.p(resultItem);
                final int q = ve1.q(resultItem);
                final int s = ve1.s(resultItem);
                final double totalMarks = resultItem.getTotalMarks();
                list2.add(new ye1(resultDataJsonModel.getRollNo(), totalMarks, ve1.i(totalMarks, sheetTemplate2.getGradeLevels()), p2, q, s, resultDataJsonModel.isSmsSent(), resultDataJsonModel.isSynced(), resultDataJsonModel.isPublished()));
            }
        }
        ve1.a(list2, sheetTemplate2.getRankingMethod());
        this.L(list2);
        return list2;
    }
    
    public final void W() {
        this.d = (ListView)this.findViewById(2131296837);
        this.m = new ProgressDialog((Context)this.p, 2131951921);
        (this.v = (SwipeRefreshLayout)this.findViewById(2131297134)).setColorSchemeColors(this.getResources().getIntArray(2130903043));
        this.v.setOnRefreshListener((SwipeRefreshLayout.j)new SwipeRefreshLayout.j(this) {
            public final ResultArchivedActivity a;
            
            @Override
            public void a() {
                final ResultArchivedActivity a = this.a;
                a.X(ResultArchivedActivity.E(a));
            }
        });
        ((AdapterView)this.d).setOnItemClickListener((AdapterView$OnItemClickListener)new AdapterView$OnItemClickListener(this) {
            public final ResultArchivedActivity a;
            
            public void onItemClick(final AdapterView adapterView, final View view, final int n, final long n2) {
                final ResultArchivedActivity a = this.a;
                e8.b(a.i, a.l, a.c);
                final Intent intent = new Intent((Context)ResultArchivedActivity.B(this.a), (Class)StudentReportArchiveActivity.class);
                intent.putExtra("SELECTED_POSITION", n - 1);
                ((Context)this.a).startActivity(intent);
            }
        });
        final ViewGroup e = (ViewGroup)this.getLayoutInflater().inflate(2131493012, (ViewGroup)null);
        this.e = e;
        this.d.addHeaderView((View)e);
        this.g = (TextView)((View)this.e).findViewById(2131297250);
        this.h = (TextView)((View)this.e).findViewById(2131297283);
    }
    
    public final void X(final String str) {
        this.v.setRefreshing(true);
        final StringBuilder sb = new StringBuilder();
        sb.append(str);
        sb.append(".exm");
        final String string = sb.toString();
        final File parent = new File(((Context)this).getApplicationContext().getExternalFilesDir(ok.c), ok.d);
        if (!parent.exists()) {
            parent.mkdirs();
        }
        final File file = new File(parent, string);
        if (file.exists()) {
            this.Y(file);
        }
        else if (this.t) {
            this.O(str);
        }
        else {
            xs.c((Context)this.p, new y01(this) {
                public final ResultArchivedActivity a;
                
                @Override
                public void a(final Object o) {
                    ResultArchivedActivity.B(this.a).finish();
                }
            }, 2131886282, 2131886454, 2131886176, 0, 0, 2131230930);
        }
    }
    
    public final void Y(final File file) {
        try {
            final ExamDataV1 a = yx.a(IOUtils.toByteArray(new FileInputStream(file)));
            if (a == null) {
                a91.G((Context)this.p, 2131886737, 2131230909, 2131231086);
                return;
            }
            this.l = a.getSheetTemplate2();
            final ArrayList<ResultDataV1> results = a.getResults();
            this.i = a.getExamId();
            final ArrayList<ResultDataJsonModel> list = new ArrayList<ResultDataJsonModel>();
            for (final ResultDataV1 resultDataV1 : results) {
                list.add(new ResultDataJsonModel(this.i.getExamName(), resultDataV1.getRollno(), this.i.getClassName(), this.i.getExamDate(), resultDataV1.getResultItem2(), false, false));
            }
            this.c = this.T(list, this.V(a.getSheetTemplate2(), list));
            this.k = ve1.l(this.l);
        }
        catch (final Exception ex) {
            a91.G((Context)this.p, 2131886737, 2131230909, 2131231086);
        }
        this.v.setRefreshing(false);
        this.b0();
    }
    
    public final void Z() {
        e8.b(this.i, this.l, this.c);
        final Intent intent = new Intent((Context)this.p, (Class)AnalysisActivity.class);
        intent.putExtra("EXAM_ID", (Serializable)this.i);
        intent.putExtra("EXAM_TYPE", "ARCHIVED");
        ((Context)this).startActivity(intent);
    }
    
    public final void a0() {
        Enum<SortResultList.ResultSortBy> rank = SortResultList.ResultSortBy.RANK;
        final ud1 j = this.j;
        boolean b;
        if (j != null) {
            b = j.b;
            final SortResultList.ResultSortBy a = j.a;
            ((View)this.e).findViewById(2131296734).setVisibility(0);
            final TextView textView = (TextView)((View)this.e).findViewById(2131297312);
            final StringBuilder sb = new StringBuilder();
            final SortResultList.ResultSortBy a2 = this.j.a;
            Resources resources;
            int n;
            if (a2 == rank) {
                resources = this.getResources();
                n = 2131886726;
            }
            else if (a2 == SortResultList.ResultSortBy.MARK) {
                resources = this.getResources();
                n = 2131886412;
            }
            else if (a2 == SortResultList.ResultSortBy.NAME) {
                resources = this.getResources();
                n = 2131886649;
            }
            else {
                resources = this.getResources();
                n = 2131886747;
            }
            sb.append(resources.getString(n));
            sb.append(" - ");
            Resources resources2;
            int n2;
            if (this.j.b) {
                resources2 = this.getResources();
                n2 = 2131886146;
            }
            else {
                resources2 = this.getResources();
                n2 = 2131886243;
            }
            sb.append(resources2.getString(n2));
            textView.setText((CharSequence)sb.toString());
            rank = a;
        }
        else {
            ((View)this.e).findViewById(2131296734).setVisibility(8);
            b = true;
        }
        final ArrayList c = this.c;
        if (c != null && c.size() > 0) {
            new SortResultList().a((SortResultList.ResultSortBy)rank, this.c, b);
        }
    }
    
    public final void b0() {
        this.a0();
        final TextView h = this.h;
        final StringBuilder sb = new StringBuilder();
        sb.append(((Context)this).getString(2131886828));
        sb.append(" : 0");
        h.setText((CharSequence)sb.toString());
        final TextView g = this.g;
        final StringBuilder sb2 = new StringBuilder();
        sb2.append(((Context)this).getString(2131886876));
        sb2.append(" : ");
        sb2.append(this.k);
        g.setText((CharSequence)sb2.toString());
        final ArrayList c = this.c;
        TextView textView;
        String string;
        if (c != null && c.size() > 0) {
            final e3 e3 = new e3((Context)this.p, this.c);
            this.f = e3;
            this.d.setAdapter((ListAdapter)e3);
            textView = this.h;
            final StringBuilder sb3 = new StringBuilder();
            sb3.append(this.c.size());
            sb3.append("");
            string = sb3.toString();
        }
        else {
            textView = this.h;
            string = "---";
        }
        textView.setText((CharSequence)string);
        final TextView g2 = this.g;
        final StringBuilder sb4 = new StringBuilder();
        sb4.append(this.k);
        sb4.append("");
        g2.setText((CharSequence)sb4.toString());
    }
    
    public final void c0() {
        ((View)this.e).findViewById(2131296436).setOnClickListener((View$OnClickListener)new View$OnClickListener(this) {
            public final ResultArchivedActivity a;
            
            public void onClick(final View view) {
                final ResultArchivedActivity a = this.a;
                a.j = null;
                a.b0();
            }
        });
        ((View)this.e).findViewById(2131296665).setOnClickListener((View$OnClickListener)new View$OnClickListener(this) {
            public final ResultArchivedActivity a;
            
            public void onClick(final View view) {
                new l10((Context)ResultArchivedActivity.B(this.a), this.a.j, new j10(this) {
                    public final ResultArchivedActivity$h a;
                    
                    @Override
                    public void a(final Object o) {
                        if (o instanceof ud1) {
                            final ResultArchivedActivity a = this.a.a;
                            a.j = (ud1)o;
                            a.b0();
                        }
                    }
                }).show();
            }
        });
    }
    
    public final void d0(final boolean b) {
        xs.c((Context)this.p, new y01(this, b) {
            public final boolean a;
            public final ResultArchivedActivity b;
            
            @Override
            public void a(final Object o) {
                this.b.N(this.a);
            }
        }, 2131886282, 2131886494, 2131886746, 2131886163, 0, 2131230930);
    }
    
    public final void e0(final String s, final String s2) {
        final MaterialAlertDialogBuilder materialAlertDialogBuilder = new MaterialAlertDialogBuilder((Context)this.p, 2131951953);
        final View inflate = ((LayoutInflater)((Context)this).getSystemService("layout_inflater")).inflate(2131492993, (ViewGroup)null);
        materialAlertDialogBuilder.setView(inflate);
        materialAlertDialogBuilder.setTitle(2131886245);
        materialAlertDialogBuilder.setPositiveButton(2131886655, (DialogInterface$OnClickListener)new DialogInterface$OnClickListener(this, (CheckBox)inflate.findViewById(2131296511)) {
            public final CheckBox a;
            public final ResultArchivedActivity b;
            
            public void onClick(final DialogInterface dialogInterface, final int n) {
                this.b.N(((CompoundButton)this.a).isChecked());
                dialogInterface.dismiss();
            }
        });
        materialAlertDialogBuilder.create().show();
    }
    
    public final void f0() {
        final MaterialAlertDialogBuilder materialAlertDialogBuilder = new MaterialAlertDialogBuilder((Context)this.p, 2131951953);
        final View inflate = ((LayoutInflater)((Context)this).getSystemService("layout_inflater")).inflate(2131493002, (ViewGroup)null);
        materialAlertDialogBuilder.setView(inflate);
        final ListView listView = (ListView)inflate.findViewById(2131296833);
        final ArrayList s = this.S();
        final StringBuilder sb = new StringBuilder();
        sb.append(this.getResources().getString(2131886107));
        sb.append(" - ");
        sb.append(s.size());
        materialAlertDialogBuilder.setTitle((CharSequence)sb.toString());
        listView.setAdapter((ListAdapter)new b3((Context)this.p, s));
        materialAlertDialogBuilder.setPositiveButton(2131886176, (DialogInterface$OnClickListener)new DialogInterface$OnClickListener(this) {
            public final ResultArchivedActivity a;
            
            public void onClick(final DialogInterface dialogInterface, final int n) {
                dialogInterface.dismiss();
            }
        });
        materialAlertDialogBuilder.create().show();
    }
    
    public final void g0() {
        final MaterialAlertDialogBuilder materialAlertDialogBuilder = new MaterialAlertDialogBuilder((Context)this.p, 2131951953);
        final View inflate = ((LayoutInflater)((Context)this).getSystemService("layout_inflater")).inflate(2131492967, (ViewGroup)null);
        materialAlertDialogBuilder.setView(inflate);
        materialAlertDialogBuilder.setTitle(2131886783);
        materialAlertDialogBuilder.setPositiveButton(2131886655, (DialogInterface$OnClickListener)new DialogInterface$OnClickListener(this, (RadioButton)inflate.findViewById(2131296991)) {
            public final RadioButton a;
            public final ResultArchivedActivity b;
            
            public void onClick(final DialogInterface dialogInterface, final int n) {
                if (((CompoundButton)this.a).isChecked()) {
                    this.b.Q();
                }
                else {
                    this.b.P();
                }
                dialogInterface.dismiss();
            }
        });
        materialAlertDialogBuilder.create().show();
    }
    
    public final void h0(final g61 g61) {
        this.m.setMessage((CharSequence)((Context)this).getString(2131886802));
        this.m.setProgressStyle(0);
        ((Dialog)this.m).setCanceledOnTouchOutside(false);
        ((AlertDialog)this.m).setButton(-2, (CharSequence)((Context)this).getString(2131886163), (DialogInterface$OnClickListener)new DialogInterface$OnClickListener(this, g61) {
            public final g61 a;
            public final ResultArchivedActivity b;
            
            public void onClick(final DialogInterface dialogInterface, final int n) {
                final g61 a = this.a;
                if (a != null) {
                    a.g();
                }
                dialogInterface.dismiss();
            }
        });
        ((Dialog)this.m).show();
    }
    
    public final void i0() {
        e8.b(this.i, this.l, this.c);
        final Intent intent = new Intent((Context)this.p, (Class)ViewTemplateActivity.class);
        intent.putExtra("EXAM_ID", (Serializable)this.i);
        intent.putExtra("EXAM_TYPE", "ARCHIVED");
        ((Context)this).startActivity(intent);
    }
    
    @Override
    public void onBackPressed() {
        super.onBackPressed();
        this.finish();
    }
    
    @Override
    public void onCreate(final Bundle bundle) {
        super.onCreate(bundle);
        this.setContentView(2131492896);
        final Toolbar toolbar = (Toolbar)this.findViewById(2131297337);
        this.x(toolbar);
        this.n = Settings$Secure.getString(((Context)this).getContentResolver(), "android_id");
        final String stringExtra = this.getIntent().getStringExtra("ARCHIVE_ID");
        this.q = stringExtra;
        if (stringExtra == null) {
            this.finish();
        }
        if ((this.i = (ExamId)this.getIntent().getSerializableExtra("EXAM_ID")) == null) {
            this.finish();
        }
        this.t = this.getIntent().getBooleanExtra("CLOUD_SYNCED", false);
        toolbar.setSubtitle(this.i.getClassName());
        this.W();
        this.X(this.q);
        this.c0();
    }
    
    public boolean onCreateOptionsMenu(final Menu menu) {
        this.getMenuInflater().inflate(2131623941, menu);
        if (!a91.c((Context)this.p)) {
            menu.findItem(2131296321).setVisible(false);
        }
        return true;
    }
    
    @Override
    public void onDestroy() {
        super.onDestroy();
        e8.c();
    }
    
    public boolean onOptionsItemSelected(final MenuItem menuItem) {
        switch (menuItem.getItemId()) {
            default: {
                return super.onOptionsItemSelected(menuItem);
            }
            case 2131296345: {
                this.i0();
                return true;
            }
            case 2131296343: {
                this.Z();
                return true;
            }
            case 2131296342: {
                this.f0();
                return true;
            }
            case 2131296322: {
                this.g0();
                return true;
            }
            case 2131296321: {
                this.R();
                return true;
            }
            case 2131296317: {
                new bx0((Context)this.p, this.q);
                return true;
            }
        }
    }
    
    @Override
    public void onResume() {
        super.onResume();
        this.b0();
    }
    
    @Override
    public boolean v() {
        this.finish();
        return true;
    }
    
    public class p extends AsyncTask
    {
        public boolean a;
        public String b;
        public String c;
        public boolean d;
        public String[][] e;
        public String f;
        public String g;
        public ProgressDialog h;
        public final ResultArchivedActivity i;
        
        public p(final ResultArchivedActivity i) {
            this.i = i;
            this.f = null;
            final StringBuilder sb = new StringBuilder();
            sb.append(i.i.getExamName().replaceAll("[^a-zA-Z0-9_-]", "_"));
            sb.append(".xls");
            this.g = sb.toString();
        }
        
        public Void a(Void... externalFilesDir) {
            final we1 we1 = new we1();
            final ResultArchivedActivity b = ResultArchivedActivity.B(this.i);
            final ResultArchivedActivity i = this.i;
            this.e = we1.c((Context)b, i.i, i.c, i.l, this.d);
            final boolean a = this.a;
            if (a) {
                final HSSFWorkbook hssfWorkbook = new HSSFWorkbook();
                final HSSFSheet sheet = hssfWorkbook.createSheet("Reports");
                int n = 0;
                while (true) {
                    final String[][] e = this.e;
                    if (n >= e.length) {
                        break;
                    }
                    final String[] array = e[n];
                    final HSSFRow row = sheet.createRow(n);
                    for (int j = 0; j < array.length; ++j) {
                        row.createCell(j).setCellValue((RichTextString)new HSSFRichTextString(array[j]));
                    }
                    ++n;
                }
                externalFilesDir = (IOException)((Context)ResultArchivedActivity.B(this.i)).getExternalFilesDir(ok.c);
                if (!((File)externalFilesDir).exists()) {
                    ((File)externalFilesDir).mkdirs();
                }
                final File file = new File((File)externalFilesDir, this.g);
                this.f = file.getAbsolutePath();
                IOException ex5 = null;
                Label_0315: {
                    FileOutputStream fileOutputStream;
                    try {
                        fileOutputStream = (FileOutputStream)(externalFilesDir = (IOException)new FileOutputStream(file));
                        try {
                            final HSSFWorkbook hssfWorkbook2 = hssfWorkbook;
                            final FileOutputStream fileOutputStream2 = fileOutputStream;
                            hssfWorkbook2.write((OutputStream)fileOutputStream2);
                            externalFilesDir = (IOException)fileOutputStream;
                            final HSSFWorkbook hssfWorkbook3 = hssfWorkbook;
                            hssfWorkbook3.close();
                            final FileOutputStream fileOutputStream3 = fileOutputStream;
                            fileOutputStream3.flush();
                            final FileOutputStream fileOutputStream4 = fileOutputStream;
                            fileOutputStream4.close();
                        }
                        catch (final Exception ex) {}
                    }
                    catch (final Exception ex) {
                        fileOutputStream = null;
                    }
                    finally {
                        final IOException ex2;
                        externalFilesDir = ex2;
                        break Label_0315;
                    }
                    try {
                        final HSSFWorkbook hssfWorkbook2 = hssfWorkbook;
                        final FileOutputStream fileOutputStream2 = fileOutputStream;
                        hssfWorkbook2.write((OutputStream)fileOutputStream2);
                        externalFilesDir = (IOException)fileOutputStream;
                        final HSSFWorkbook hssfWorkbook3 = hssfWorkbook;
                        hssfWorkbook3.close();
                        try {
                            final FileOutputStream fileOutputStream3 = fileOutputStream;
                            fileOutputStream3.flush();
                            final FileOutputStream fileOutputStream4 = fileOutputStream;
                            fileOutputStream4.close();
                            return null;
                            while (true) {
                                fileOutputStream.flush();
                                fileOutputStream.close();
                                return null;
                                final Exception ex;
                                ex.printStackTrace();
                                externalFilesDir = (IOException)fileOutputStream;
                                file.deleteOnExit();
                                iftrue(Label_0301:)(fileOutputStream == null);
                                continue;
                            }
                        }
                        catch (final IOException externalFilesDir) {
                            externalFilesDir.printStackTrace();
                        }
                        Label_0301: {
                            return null;
                        }
                    }
                    finally {
                        final IOException ex3 = externalFilesDir;
                        final IOException ex4;
                        externalFilesDir = ex4;
                        ex5 = ex3;
                    }
                }
                if (ex5 != null) {
                    try {
                        ((OutputStream)ex5).flush();
                        ((FileOutputStream)ex5).close();
                    }
                    catch (final IOException ex6) {
                        ex6.printStackTrace();
                    }
                }
                throw externalFilesDir;
            }
            return null;
        }
        
        public void b(final Void void1) {
            super.onPostExecute((Object)void1);
            ((Dialog)this.h).dismiss();
            if (this.a) {
                new hn1((Context)ResultArchivedActivity.B(this.i), this.f, this.g);
            }
            else {
                this.i.U(this.e, this.b, this.c, this.d);
            }
        }
        
        public void onPreExecute() {
            (this.h = new ProgressDialog((Context)ResultArchivedActivity.B(this.i))).setMessage((CharSequence)"Please wait, creating excel report...");
            ((Dialog)this.h).show();
        }
    }
}

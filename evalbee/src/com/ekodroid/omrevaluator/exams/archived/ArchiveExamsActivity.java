// 
// Decompiled by Procyon v0.6.0
// 

package com.ekodroid.omrevaluator.exams.archived;

import android.widget.AbsListView;
import android.content.IntentFilter;
import android.os.Bundle;
import androidx.appcompat.widget.Toolbar;
import android.widget.AdapterView$OnItemSelectedListener;
import android.view.View$OnClickListener;
import android.widget.SpinnerAdapter;
import java.util.List;
import java.util.HashSet;
import android.content.res.Resources;
import android.widget.TextView;
import com.ekodroid.omrevaluator.util.DataModels.SortArchiveExamList;
import android.widget.ListAdapter;
import java.util.Collection;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.android.gms.tasks.Task;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.FirebaseFirestore;
import com.ekodroid.omrevaluator.database.repositories.TemplateRepository;
import java.util.Date;
import java.util.Calendar;
import java.text.SimpleDateFormat;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView$OnItemClickListener;
import android.view.Menu;
import android.util.SparseBooleanArray;
import android.view.MenuItem;
import android.widget.AbsListView$MultiChoiceModeListener;
import java.io.Serializable;
import android.content.Intent;
import com.ekodroid.omrevaluator.activities.Services.ArchiveSyncService;
import android.content.Context;
import com.ekodroid.omrevaluator.serializable.ArchivedResult.ArchivedPendingDelete;
import java.util.Iterator;
import com.ekodroid.omrevaluator.database.ArchivedLocalDataModel;
import com.ekodroid.omrevaluator.templateui.models.ExamId;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.view.ActionMode;
import android.content.BroadcastReceiver;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import android.view.ViewGroup;
import android.widget.ListView;
import java.util.ArrayList;

public class ArchiveExamsActivity extends v5
{
    public ArchiveExamsActivity c;
    public ArrayList d;
    public ArrayList e;
    public ListView f;
    public ViewGroup g;
    public SwipeRefreshLayout h;
    public f3 i;
    public BroadcastReceiver j;
    public boolean k;
    public ActionMode l;
    public Spinner m;
    public ArrayList n;
    public ArrayAdapter p;
    public String q;
    public zx t;
    
    public ArchiveExamsActivity() {
        this.c = this;
        this.d = new ArrayList();
        this.e = new ArrayList();
        this.k = false;
        this.l = null;
        this.q = "All";
    }
    
    public static /* synthetic */ zx A(final ArchiveExamsActivity archiveExamsActivity) {
        return archiveExamsActivity.t;
    }
    
    public static /* synthetic */ zx B(final ArchiveExamsActivity archiveExamsActivity, final zx t) {
        return archiveExamsActivity.t = t;
    }
    
    public static /* synthetic */ String K(final ArchiveExamsActivity archiveExamsActivity, final String q) {
        return archiveExamsActivity.q = q;
    }
    
    public final void L(final ArrayList list, final ArrayList list2, final boolean b) {
        if (list2 != null && list2.size() > 0) {
            for (final ArchivedLocalDataModel archivedLocalDataModel : list2) {
                list.add(new c8(archivedLocalDataModel.getName(), archivedLocalDataModel.getClassName(), archivedLocalDataModel.getExamDate(), archivedLocalDataModel.getTemplateId(), archivedLocalDataModel.getArchivedOn(), archivedLocalDataModel.getSizeKb(), archivedLocalDataModel.getVersion(), archivedLocalDataModel.getAttendees(), archivedLocalDataModel.getNoOfQue(), false, b, this.k, 0));
            }
        }
    }
    
    public final void M(final ArrayList list) {
        xs.c((Context)this.c, new y01(this, list) {
            public final ArrayList a;
            public final ArchiveExamsActivity b;
            
            @Override
            public void a(final Object o) {
                for (final c8 c8 : this.a) {
                    c8.k(true);
                    ArchivedPendingDelete.addArchiveId((Context)this.b.c, c8.g());
                }
                ArchiveSyncService.n((Context)this.b.c);
            }
        }, 2131886234, 2131886476, 2131886231, 2131886163, 0, 2131230945);
    }
    
    public final void N(final String s, final ExamId examId, final boolean b) {
        final Intent intent = new Intent((Context)this.c, (Class)ResultArchivedActivity.class);
        intent.putExtra("EXAM_ID", (Serializable)examId);
        intent.putExtra("ARCHIVE_ID", s);
        intent.putExtra("CLOUD_SYNCED", b);
        ((Context)this).startActivity(intent);
    }
    
    public final void O() {
        ((AbsListView)this.f).setChoiceMode(3);
        ((AbsListView)this.f).setMultiChoiceModeListener((AbsListView$MultiChoiceModeListener)new AbsListView$MultiChoiceModeListener(this) {
            public final ArchiveExamsActivity a;
            
            public boolean onActionItemClicked(final ActionMode actionMode, final MenuItem menuItem) {
                final SparseBooleanArray a = this.a.i.a();
                final int itemId = menuItem.getItemId();
                if (itemId == 2131296708) {
                    final ArrayList list = new ArrayList();
                    for (int i = a.size() - 1; i >= 0; --i) {
                        if (a.valueAt(i)) {
                            list.add(this.a.d.get(a.keyAt(i)));
                        }
                    }
                    this.a.M(list);
                    actionMode.finish();
                    this.a.i.notifyDataSetChanged();
                    return true;
                }
                if (itemId != 2131296710) {
                    return false;
                }
                if (a.size() == 1 && a.valueAt(0)) {
                    final ArchiveExamsActivity a2 = this.a;
                    a2.U(((c8)a2.d.get(a.keyAt(0))).g());
                }
                actionMode.finish();
                return true;
            }
            
            public boolean onCreateActionMode(final ActionMode l, final Menu menu) {
                this.a.getMenuInflater().inflate(2131623936, menu);
                final ArchiveExamsActivity a = this.a;
                a.l = l;
                ((View)a.g).findViewById(2131296758).setVisibility(8);
                return true;
            }
            
            public void onDestroyActionMode(final ActionMode actionMode) {
                this.a.i.b();
                actionMode.finish();
                final ArchiveExamsActivity a = this.a;
                a.l = null;
                ((View)a.g).findViewById(2131296758).setVisibility(0);
            }
            
            public void onItemCheckedStateChanged(final ActionMode actionMode, final int n, final long n2, final boolean b) {
                boolean visible = true;
                final int checkedItemCount = ((AbsListView)this.a.f).getCheckedItemCount();
                final StringBuilder sb = new StringBuilder();
                sb.append(checkedItemCount);
                sb.append(" Selected");
                actionMode.setTitle((CharSequence)sb.toString());
                final MenuItem item = actionMode.getMenu().findItem(2131296710);
                if (checkedItemCount != 1) {
                    visible = false;
                }
                item.setVisible(visible);
                this.a.i.d(n - 1);
                this.a.i.notifyDataSetChanged();
            }
            
            public boolean onPrepareActionMode(final ActionMode actionMode, final Menu menu) {
                return false;
            }
        });
    }
    
    public final void P() {
        (this.h = (SwipeRefreshLayout)this.findViewById(2131297134)).setColorSchemeColors(this.getResources().getIntArray(2130903043));
        this.h.setOnRefreshListener((SwipeRefreshLayout.j)new SwipeRefreshLayout.j(this) {
            public final ArchiveExamsActivity a;
            
            @Override
            public void a() {
                this.a.S();
            }
        });
        ((AdapterView)(this.f = (ListView)this.findViewById(2131296848))).setOnItemClickListener((AdapterView$OnItemClickListener)new AdapterView$OnItemClickListener(this) {
            public final ArchiveExamsActivity a;
            
            public void onItemClick(final AdapterView adapterView, final View view, final int n, final long n2) {
                view.setBackgroundResource(2131230860);
                final c8 c8 = this.a.d.get(n - 1);
                this.a.N(c8.g(), new ExamId(c8.d(), c8.b(), c8.c()), c8.j());
            }
        });
        final ViewGroup g = (ViewGroup)this.getLayoutInflater().inflate(2131493011, (ViewGroup)null);
        this.g = g;
        this.f.addHeaderView((View)g);
        this.m = (Spinner)((View)this.g).findViewById(2131297075);
    }
    
    public final void Q() {
        this.j = new BroadcastReceiver(this) {
            public final ArchiveExamsActivity a;
            
            public void onReceive(final Context context, final Intent intent) {
                final String stringExtra = intent.getStringExtra("file_id");
                final String stringExtra2 = intent.getStringExtra("type");
                final int intExtra = intent.getIntExtra("progress", 0);
                while (true) {
                    for (final c8 o : this.a.e) {
                        if (stringExtra.equals(o.g())) {
                            if (o != null) {
                                Label_0138: {
                                    if ("DELETE".equals(stringExtra2)) {
                                        if (intExtra > 95) {
                                            this.a.e.remove(o);
                                            break Label_0138;
                                        }
                                        o.k(true);
                                    }
                                    else {
                                        if (intExtra > 95) {
                                            o.m(true);
                                            break Label_0138;
                                        }
                                        o.m(false);
                                    }
                                    o.l(intExtra);
                                }
                                this.a.W();
                            }
                            return;
                        }
                    }
                    c8 o = null;
                    continue;
                }
            }
        };
    }
    
    public final boolean R(final String source, final int n) {
        try {
            final Date parse = new SimpleDateFormat("yyyy-MM-dd").parse(source);
            final Calendar instance = Calendar.getInstance();
            instance.setTime(parse);
            instance.add(5, n);
            return !new Date().after(instance.getTime());
        }
        catch (final Exception ex) {
            return false;
        }
    }
    
    public final void S() {
        this.h.setRefreshing(true);
        if (this.k) {
            this.T();
        }
        else {
            this.L(this.e = new ArrayList(), TemplateRepository.getInstance((Context)this.c).getArchiveLocalDataModels(), false);
            final ArrayList<String> pendingArchiveIds = ArchivedPendingDelete.getPendingArchiveIds((Context)this.c);
            if (pendingArchiveIds != null && pendingArchiveIds.size() > 0) {
                for (final c8 c8 : this.e) {
                    if (pendingArchiveIds.indexOf(c8.g()) > -1) {
                        c8.k(true);
                    }
                }
            }
            this.h.setRefreshing(false);
            this.Y();
            this.W();
        }
    }
    
    public final void T() {
        FirebaseFirestore.f().a("ArchivedExams").g(FirebaseAuth.getInstance().e().O()).h().addOnCompleteListener((OnCompleteListener)new OnCompleteListener(this) {
            public final ArchiveExamsActivity a;
            
            public void onComplete(final Task task) {
                this.a.e = new ArrayList();
                if (task.isSuccessful()) {
                    final DocumentSnapshot documentSnapshot = (DocumentSnapshot)task.getResult();
                    if (documentSnapshot.a()) {
                        final Collection values = documentSnapshot.d().values();
                        final ArrayList list = new ArrayList();
                        final Iterator iterator = values.iterator();
                        while (iterator.hasNext()) {
                            final ArchivedLocalDataModel e = (ArchivedLocalDataModel)new gc0().j(new gc0().s(iterator.next()), ArchivedLocalDataModel.class);
                            if (this.a.R(e.getArchivedOn(), 365)) {
                                list.add(e);
                            }
                            else {
                                ArchivedPendingDelete.addArchiveId((Context)this.a.c, e.getTemplateId());
                            }
                        }
                        final ArrayList<String> pendingArchiveIds = ArchivedPendingDelete.getPendingArchiveIds((Context)this.a.c);
                        if (pendingArchiveIds != null && pendingArchiveIds.size() > 0) {
                            ArchiveSyncService.n((Context)this.a.c);
                        }
                        final ArchiveExamsActivity a = this.a;
                        a.L(a.e, list, true);
                    }
                }
                final ArrayList<ArchivedLocalDataModel> archiveLocalDataModels = TemplateRepository.getInstance((Context)this.a.c).getArchiveLocalDataModels();
                final ArchiveExamsActivity a2 = this.a;
                a2.L(a2.e, archiveLocalDataModels, false);
                final ArrayList<String> pendingArchiveIds2 = ArchivedPendingDelete.getPendingArchiveIds((Context)this.a.c);
                if (pendingArchiveIds2 != null && pendingArchiveIds2.size() > 0) {
                    for (final c8 c8 : this.a.e) {
                        if (pendingArchiveIds2.indexOf(c8.g()) > -1) {
                            c8.k(true);
                        }
                    }
                }
                this.a.h.setRefreshing(false);
                this.a.Y();
                this.a.W();
            }
        });
    }
    
    public final void U(final String s) {
        new bx0((Context)this.c, s);
    }
    
    public final void V() {
        if (this.q.toUpperCase().equals("ALL")) {
            this.d = this.e;
        }
        else {
            this.d = new ArrayList();
            for (final c8 e : this.e) {
                if (e.b().equals(this.q)) {
                    this.d.add(e);
                }
            }
        }
    }
    
    public final void W() {
        ((Context)this.c).getSharedPreferences("MyPref", 0).edit().putInt("ARCHIVE_COUNT", this.e.size()).commit();
        this.k = a91.c((Context)this.c);
        if (this.e != null) {
            this.V();
        }
        else {
            this.d = new ArrayList();
        }
        if (this.d != null) {
            this.X();
            final f3 f3 = new f3(this.d, (Context)this.c);
            this.i = f3;
            this.f.setAdapter((ListAdapter)f3);
        }
    }
    
    public final void X() {
        Enum<SortArchiveExamList.SortBy> date = SortArchiveExamList.SortBy.DATE;
        final zx t = this.t;
        boolean b = false;
        if (t != null) {
            b = t.b;
            final SortArchiveExamList.SortBy a = t.a;
            ((View)this.g).findViewById(2131296734).setVisibility(0);
            final TextView textView = (TextView)this.findViewById(2131297312);
            final StringBuilder sb = new StringBuilder();
            final SortArchiveExamList.SortBy a2 = this.t.a;
            Resources resources;
            int n;
            if (a2 == date) {
                resources = this.getResources();
                n = 2131886225;
            }
            else if (a2 == SortArchiveExamList.SortBy.NAME) {
                resources = this.getResources();
                n = 2131886649;
            }
            else {
                resources = this.getResources();
                n = 2131886382;
            }
            sb.append(resources.getString(n));
            sb.append(" - ");
            Resources resources2;
            int n2;
            if (this.t.b) {
                resources2 = this.getResources();
                n2 = 2131886146;
            }
            else {
                resources2 = this.getResources();
                n2 = 2131886243;
            }
            sb.append(resources2.getString(n2));
            textView.setText((CharSequence)sb.toString());
            date = a;
        }
        else {
            ((View)this.g).findViewById(2131296734).setVisibility(8);
        }
        new SortArchiveExamList().d((SortArchiveExamList.SortBy)date, this.d, b);
    }
    
    public final void Y() {
        (this.n = new ArrayList()).add("All");
        final HashSet c = new HashSet();
        final Iterator iterator = this.e.iterator();
        while (iterator.hasNext()) {
            c.add(((c8)iterator.next()).b());
        }
        this.n.addAll(c);
        final ArrayAdapter arrayAdapter = new ArrayAdapter((Context)this.c, 2131493104, (List)this.n);
        this.p = arrayAdapter;
        this.m.setAdapter((SpinnerAdapter)arrayAdapter);
        int index;
        if ((index = this.n.indexOf(this.q)) < 0) {
            index = 0;
        }
        ((AdapterView)this.m).setSelection(index);
    }
    
    public final void Z() {
        this.findViewById(2131296436).setOnClickListener((View$OnClickListener)new View$OnClickListener(this) {
            public final ArchiveExamsActivity a;
            
            public void onClick(final View view) {
                ArchiveExamsActivity.B(this.a, null);
                this.a.W();
            }
        });
        this.findViewById(2131296664).setOnClickListener((View$OnClickListener)new View$OnClickListener(this) {
            public final ArchiveExamsActivity a;
            
            public void onClick(final View view) {
                final ArchiveExamsActivity a = this.a;
                new k10((Context)a.c, ArchiveExamsActivity.A(a), new j10(this) {
                    public final ArchiveExamsActivity$d a;
                    
                    @Override
                    public void a(final Object o) {
                        if (o instanceof zx) {
                            ArchiveExamsActivity.B(this.a.a, (zx)o);
                            this.a.a.W();
                        }
                    }
                }).show();
            }
        });
    }
    
    public final void a0() {
        this.Y();
        ((AdapterView)this.m).setOnItemSelectedListener((AdapterView$OnItemSelectedListener)new AdapterView$OnItemSelectedListener(this) {
            public final ArchiveExamsActivity a;
            
            public void onItemSelected(final AdapterView adapterView, final View view, final int index, final long n) {
                final ArchiveExamsActivity a = this.a;
                ArchiveExamsActivity.K(a, (String)a.n.get(index));
                this.a.W();
            }
            
            public void onNothingSelected(final AdapterView adapterView) {
            }
        });
    }
    
    public final void b0() {
        final Toolbar toolbar = (Toolbar)this.findViewById(2131297336);
        this.x(toolbar);
        toolbar.setNavigationOnClickListener((View$OnClickListener)new View$OnClickListener(this) {
            public final ArchiveExamsActivity a;
            
            public void onClick(final View view) {
                this.a.finish();
            }
        });
    }
    
    @Override
    public void onBackPressed() {
        super.onBackPressed();
        this.finish();
    }
    
    @Override
    public void onCreate(final Bundle bundle) {
        super.onCreate(bundle);
        this.setContentView(2131492895);
        this.b0();
        this.P();
        this.O();
        this.Z();
        this.a0();
        this.Q();
        this.k = a91.c((Context)this.c);
    }
    
    @Override
    public void onPause() {
        super.onPause();
        ((Context)this).unregisterReceiver(this.j);
        final ActionMode l = this.l;
        if (l != null) {
            l.finish();
            this.l = null;
        }
        this.findViewById(2131296758).setVisibility(0);
    }
    
    @Override
    public void onResume() {
        super.onResume();
        this.W();
        this.S();
        sl.registerReceiver((Context)this.c, this.j, new IntentFilter("UPDATE_ARCHIVE_EXAM_LIST"), 4);
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.ekodroid.omrevaluator.exams.archived;

import android.view.ViewGroup$MarginLayoutParams;
import android.provider.Settings$Secure;
import androidx.appcompat.widget.Toolbar;
import android.os.Bundle;
import com.ekodroid.omrevaluator.database.StudentDataModel;
import com.ekodroid.omrevaluator.database.ResultDataJsonModel;
import com.ekodroid.omrevaluator.exams.models.RollNoListDataModel;
import android.view.ViewGroup$LayoutParams;
import android.graphics.Typeface;
import android.widget.TableRow;
import android.widget.TableRow$LayoutParams;
import android.widget.TableLayout;
import com.ekodroid.omrevaluator.database.repositories.ClassRepository;
import android.view.View$OnClickListener;
import android.content.res.Resources$Theme;
import android.content.Context;
import com.ekodroid.omrevaluator.templateui.models.AnswerOptionKey;
import com.ekodroid.omrevaluator.templateui.scanner.AnswerValue;
import java.util.Iterator;
import android.view.View;
import android.view.ViewGroup;
import android.view.LayoutInflater;
import com.ekodroid.omrevaluator.templateui.scanner.ResultItem;
import android.widget.Toast;
import android.widget.ImageButton;
import com.ekodroid.omrevaluator.templateui.models.TemplateParams2;
import com.ekodroid.omrevaluator.templateui.models.LabelProfile;
import com.ekodroid.omrevaluator.templateui.models.SheetTemplate2;
import com.ekodroid.omrevaluator.templateui.models.ExamId;
import android.widget.TextView;
import com.google.firebase.analytics.FirebaseAnalytics;
import java.util.ArrayList;
import android.content.SharedPreferences;
import android.widget.LinearLayout;

public class StudentReportArchiveActivity extends v5
{
    public StudentReportArchiveActivity c;
    public LinearLayout d;
    public SharedPreferences e;
    public ArrayList f;
    public int g;
    public FirebaseAnalytics h;
    public TextView i;
    public String j;
    public ExamId k;
    public SheetTemplate2 l;
    public LabelProfile m;
    public TemplateParams2 n;
    public ImageButton p;
    public ImageButton q;
    public Toast t;
    
    public StudentReportArchiveActivity() {
        this.c = this;
        this.g = 0;
    }
    
    public static /* synthetic */ Toast B(final StudentReportArchiveActivity studentReportArchiveActivity) {
        return studentReportArchiveActivity.t;
    }
    
    public final void A(final ResultItem resultItem, final SheetTemplate2 sheetTemplate2, final LinearLayout linearLayout) {
        ((ViewGroup)linearLayout).removeAllViews();
        final ArrayList e = this.E(resultItem, sheetTemplate2);
        final LayoutInflater layoutInflater = (LayoutInflater)((Context)this.c).getSystemService("layout_inflater");
        if (layoutInflater != null) {
            for (final i5 i5 : e) {
                final LinearLayout linearLayout2 = (LinearLayout)layoutInflater.inflate(2131493018, (ViewGroup)null);
                final TextView textView = (TextView)((View)linearLayout2).findViewById(2131297263);
                final TextView textView2 = (TextView)((View)linearLayout2).findViewById(2131297248);
                final TextView textView3 = (TextView)((View)linearLayout2).findViewById(2131297217);
                final TextView textView4 = (TextView)((View)linearLayout2).findViewById(2131297209);
                textView.setText((CharSequence)i5.d());
                textView2.setText((CharSequence)i5.c());
                textView3.setText((CharSequence)i5.b());
                textView4.setText((CharSequence)i5.a());
                ((ViewGroup)linearLayout).addView((View)linearLayout2);
            }
        }
    }
    
    public final ArrayList E(final ResultItem resultItem, final SheetTemplate2 sheetTemplate2) {
        final ArrayList list = new ArrayList();
        for (int i = 0; i < resultItem.getAnswerValue2s().size(); ++i) {
            final AnswerValue answerValue = resultItem.getAnswerValue2s().get(i);
            final StringBuilder sb = new StringBuilder();
            final String s = "";
            sb.append("");
            sb.append(answerValue.getQuestionNumber());
            final String string = sb.toString();
            final String r = e5.r(answerValue, sheetTemplate2.getLabelProfile());
            final StringBuilder sb2 = new StringBuilder();
            sb2.append("");
            sb2.append(ve1.o(answerValue.getMarksForAnswer()));
            final String string2 = sb2.toString();
            final ArrayList b = ve1.b(sheetTemplate2, resultItem.getExamSet());
            String q = s;
            if (b != null) {
                q = s;
                if (b.size() > 0) {
                    q = e5.q(b.get(i), sheetTemplate2.getLabelProfile());
                }
            }
            list.add(new i5(string, r, q, string2));
        }
        return list;
    }
    
    public final int F() {
        return this.f.get(this.g).c().getRank();
    }
    
    public final TextView G() {
        final TextView textView = new TextView((Context)this);
        textView.setMinWidth(a91.d(90, (Context)this.c));
        textView.setGravity(16);
        textView.setPadding(a91.d(8, (Context)this.c), 0, a91.d(8, (Context)this.c), 0);
        textView.setTextAppearance(2131952181);
        ((View)textView).setBackgroundColor(this.getResources().getColor(2131099740, (Resources$Theme)null));
        return textView;
    }
    
    public final void H() {
        ((View)(this.p = (ImageButton)this.findViewById(2131296670))).setOnClickListener((View$OnClickListener)new View$OnClickListener(this) {
            public final StudentReportArchiveActivity a;
            
            public void onClick(final View view) {
                if (StudentReportArchiveActivity.B(this.a) != null) {
                    StudentReportArchiveActivity.B(this.a).cancel();
                }
                final StudentReportArchiveActivity a = this.a;
                if (a.g < a.f.size() - 1) {
                    final StudentReportArchiveActivity a2 = this.a;
                    ++a2.g;
                    a2.K();
                }
                this.a.J();
            }
        });
    }
    
    public final void I() {
        ((View)(this.q = (ImageButton)this.findViewById(2131296672))).setOnClickListener((View$OnClickListener)new View$OnClickListener(this) {
            public final StudentReportArchiveActivity a;
            
            public void onClick(final View view) {
                if (StudentReportArchiveActivity.B(this.a) != null) {
                    StudentReportArchiveActivity.B(this.a).cancel();
                }
                final StudentReportArchiveActivity a = this.a;
                final int g = a.g;
                if (g > 0) {
                    a.g = g - 1;
                    a.K();
                }
                this.a.J();
            }
        });
    }
    
    public final void J() {
        ((View)this.q).setEnabled(true);
        ((View)this.p).setEnabled(true);
        final int g = this.g;
        if (g == 0) {
            this.i.setText(2131886318);
            ((View)this.q).setEnabled(false);
        }
        else if (g == this.f.size() - 1) {
            ((View)this.p).setEnabled(false);
            this.i.setText(2131886376);
        }
        else {
            final TextView i = this.i;
            final StringBuilder sb = new StringBuilder();
            sb.append(((Context)this).getString(2131886730));
            sb.append(" ");
            sb.append(this.g + 1);
            sb.append("/");
            sb.append(this.f.size());
            i.setText((CharSequence)sb.toString());
        }
    }
    
    public final void K() {
        if (this.g > this.f.size() - 1) {
            this.g = 0;
        }
        final RollNoListDataModel c = this.f.get(this.g).c();
        final int rollNo = c.getRollNo();
        final ResultDataJsonModel a = this.f.get(this.g).a();
        if (a == null) {
            o4.c(this.h, "SRA_RESULT_NULL_ARCHIVE", this.j);
            ++this.g;
            this.K();
        }
        if (this.n() != null) {
            final t1 n = this.n();
            final StringBuilder sb = new StringBuilder();
            sb.append(this.m.getRollNoString());
            sb.append(" : ");
            sb.append(rollNo);
            n.t(sb.toString());
        }
        final StudentDataModel student = ClassRepository.getInstance((Context)this.c).getStudent(rollNo, this.k.getClassName());
        final ResultItem resultItem = a.getResultItem(this.l);
        final double[] marksForEachSubject = resultItem.getMarksForEachSubject();
        final double[][] k = ve1.k(resultItem, this.l);
        final int[][] c2 = ve1.c(resultItem, this.l);
        final int[][] j = ve1.j(resultItem, this.l);
        final int[] h = ve1.h(c2);
        final int[] h2 = ve1.h(j);
        final double[][] r = ve1.r(this.l);
        final double[][] n2 = ve1.n(k, r);
        final double[] g = ve1.g(r);
        final double[] m = ve1.m(marksForEachSubject, g);
        final double totalMarks = resultItem.getTotalMarks();
        final double n3 = 100.0 * totalMarks / ve1.d(g);
        String i;
        if (this.l.getGradeLevels() != null) {
            i = ve1.i(totalMarks, this.l.getGradeLevels());
        }
        else {
            i = null;
        }
        final TextView textView = (TextView)this.findViewById(2131297247);
        final TextView textView2 = (TextView)this.findViewById(2131297242);
        final TextView textView3 = (TextView)this.findViewById(2131297243);
        final TextView textView4 = (TextView)this.findViewById(2131297244);
        final TextView textView5 = (TextView)this.findViewById(2131297245);
        final TextView textView6 = (TextView)this.findViewById(2131297246);
        textView4.setText((CharSequence)this.m.getExamSetString());
        textView3.setText((CharSequence)this.m.getExamNameString());
        textView6.setText((CharSequence)this.m.getRankString());
        textView5.setText((CharSequence)this.m.getGradeString());
        textView2.setText((CharSequence)this.m.getClassString());
        textView.setText((CharSequence)this.m.getNameString());
        final TextView textView7 = (TextView)this.findViewById(2131297292);
        final TextView textView8 = (TextView)this.findViewById(2131297287);
        final TextView textView9 = (TextView)this.findViewById(2131297289);
        final TextView textView10 = (TextView)this.findViewById(2131297290);
        final TextView textView11 = (TextView)this.findViewById(2131297294);
        final TextView textView12 = (TextView)this.findViewById(2131297291);
        final TableLayout tableLayout = (TableLayout)this.findViewById(2131297148);
        ((ViewGroup)tableLayout).removeViews(1, ((ViewGroup)tableLayout).getChildCount() - 1);
        final LinearLayout linearLayout = (LinearLayout)this.findViewById(2131296730);
        final LinearLayout linearLayout2 = (LinearLayout)this.findViewById(2131296731);
        final LinearLayout linearLayout3 = (LinearLayout)this.findViewById(2131296762);
        if (this.F() > 0) {
            textView11.setText((CharSequence)String.valueOf(c.getRank()));
        }
        else {
            this.findViewById(2131296788).setVisibility(8);
        }
        if (i != null) {
            textView12.setText((CharSequence)i);
        }
        else {
            this.findViewById(2131296757).setVisibility(8);
        }
        textView8.setText((CharSequence)this.k.getClassName());
        String studentName;
        if (student != null) {
            studentName = student.getStudentName();
        }
        else {
            studentName = "---";
        }
        textView7.setText((CharSequence)studentName);
        textView9.setText((CharSequence)this.k.getExamName());
        if (this.n.getExamSets() > 1) {
            final String[] examSetLabels = this.l.getLabelProfile().getExamSetLabels();
            int n4;
            if ((n4 = resultItem.getExamSet() - 1) < 0) {
                n4 = 0;
            }
            textView10.setText((CharSequence)examSetLabels[n4]);
        }
        else {
            this.findViewById(2131296750).setVisibility(8);
        }
        ((TextView)this.findViewById(2131297164)).setText((CharSequence)this.m.getCorrectAnswerString());
        ((TextView)this.findViewById(2131297165)).setText((CharSequence)this.m.getIncorrectAnswerString());
        ((TextView)this.findViewById(2131297168)).setText((CharSequence)this.m.getSubjectString());
        ((TextView)this.findViewById(2131297166)).setText((CharSequence)this.m.getMarksString());
        ((TextView)this.findViewById(2131297167)).setText((CharSequence)this.m.getPercentageString());
        final TableRow$LayoutParams tableRow$LayoutParams = new TableRow$LayoutParams(-2, a91.d(32, (Context)this.c));
        final int d = a91.d(1, (Context)this.c);
        ((ViewGroup$MarginLayoutParams)tableRow$LayoutParams).setMargins(d, d, d, d);
        int l = 0;
        final LinearLayout linearLayout4 = linearLayout3;
        while (l < marksForEachSubject.length) {
            final TableRow tableRow = new TableRow((Context)this.c);
            final TextView g2 = this.G();
            g2.setTypeface((Typeface)null, 1);
            g2.setText((CharSequence)this.n.getSubjects()[l].getSubName());
            final TextView g3 = this.G();
            g3.setTypeface((Typeface)null, 1);
            final StringBuilder sb2 = new StringBuilder();
            sb2.append("");
            sb2.append(ve1.o(marksForEachSubject[l]));
            g3.setText((CharSequence)sb2.toString());
            ((ViewGroup)tableRow).addView((View)g2, (ViewGroup$LayoutParams)tableRow$LayoutParams);
            ((ViewGroup)tableRow).addView((View)g3, (ViewGroup$LayoutParams)tableRow$LayoutParams);
            final TextView g4 = this.G();
            g4.setTypeface((Typeface)null, 1);
            final StringBuilder sb3 = new StringBuilder();
            sb3.append(ve1.o(m[l]));
            sb3.append("%");
            g4.setText((CharSequence)sb3.toString());
            ((ViewGroup)tableRow).addView((View)g4, (ViewGroup$LayoutParams)tableRow$LayoutParams);
            final TextView g5 = this.G();
            g5.setTypeface((Typeface)null, 1);
            final StringBuilder sb4 = new StringBuilder();
            sb4.append("");
            sb4.append(h[l]);
            g5.setText((CharSequence)sb4.toString());
            ((ViewGroup)tableRow).addView((View)g5, (ViewGroup$LayoutParams)tableRow$LayoutParams);
            final TextView g6 = this.G();
            g6.setTypeface((Typeface)null, 1);
            final StringBuilder sb5 = new StringBuilder();
            sb5.append("");
            sb5.append(h2[l]);
            g6.setText((CharSequence)sb5.toString());
            ((ViewGroup)tableRow).addView((View)g6, (ViewGroup$LayoutParams)tableRow$LayoutParams);
            tableLayout.addView((View)tableRow);
            for (int n5 = 0; n5 < k[l].length; ++n5) {
                final TableRow tableRow2 = new TableRow((Context)this.c);
                final TextView g7 = this.G();
                g7.setText((CharSequence)this.n.getSubjects()[l].getSections()[n5].getName());
                final TextView g8 = this.G();
                final StringBuilder sb6 = new StringBuilder();
                sb6.append("");
                sb6.append(ve1.o(k[l][n5]));
                g8.setText((CharSequence)sb6.toString());
                final TextView g9 = this.G();
                final StringBuilder sb7 = new StringBuilder();
                sb7.append(ve1.o(n2[l][n5]));
                sb7.append("%");
                g9.setText((CharSequence)sb7.toString());
                final TextView g10 = this.G();
                final StringBuilder sb8 = new StringBuilder();
                sb8.append("");
                sb8.append(c2[l][n5]);
                g10.setText((CharSequence)sb8.toString());
                final TextView g11 = this.G();
                final StringBuilder sb9 = new StringBuilder();
                sb9.append("");
                sb9.append(j[l][n5]);
                g11.setText((CharSequence)sb9.toString());
                ((ViewGroup)tableRow2).addView((View)g7, (ViewGroup$LayoutParams)tableRow$LayoutParams);
                ((ViewGroup)tableRow2).addView((View)g8, (ViewGroup$LayoutParams)tableRow$LayoutParams);
                ((ViewGroup)tableRow2).addView((View)g9, (ViewGroup$LayoutParams)tableRow$LayoutParams);
                ((ViewGroup)tableRow2).addView((View)g10, (ViewGroup$LayoutParams)tableRow$LayoutParams);
                ((ViewGroup)tableRow2).addView((View)g11, (ViewGroup$LayoutParams)tableRow$LayoutParams);
                tableLayout.addView((View)tableRow2);
            }
            ++l;
        }
        final TableRow tableRow3 = new TableRow((Context)this.c);
        final TextView g12 = this.G();
        g12.setTypeface((Typeface)null, 1);
        g12.setText((CharSequence)this.m.getTotalMarksString());
        final TextView g13 = this.G();
        g13.setTypeface((Typeface)null, 1);
        final StringBuilder sb10 = new StringBuilder();
        sb10.append("");
        sb10.append(ve1.o(totalMarks));
        g13.setText((CharSequence)sb10.toString());
        final TextView g14 = this.G();
        g14.setTypeface((Typeface)null, 1);
        final StringBuilder sb11 = new StringBuilder();
        sb11.append(ve1.o(n3));
        sb11.append("%");
        g14.setText((CharSequence)sb11.toString());
        final TextView g15 = this.G();
        g15.setTypeface((Typeface)null, 1);
        final StringBuilder sb12 = new StringBuilder();
        sb12.append("");
        sb12.append(ve1.e(h));
        g15.setText((CharSequence)sb12.toString());
        final TextView g16 = this.G();
        g16.setTypeface((Typeface)null, 1);
        final StringBuilder sb13 = new StringBuilder();
        sb13.append("");
        sb13.append(ve1.e(h2));
        g16.setText((CharSequence)sb13.toString());
        ((ViewGroup)tableRow3).addView((View)g12, (ViewGroup$LayoutParams)tableRow$LayoutParams);
        ((ViewGroup)tableRow3).addView((View)g13, (ViewGroup$LayoutParams)tableRow$LayoutParams);
        ((ViewGroup)tableRow3).addView((View)g14, (ViewGroup$LayoutParams)tableRow$LayoutParams);
        ((ViewGroup)tableRow3).addView((View)g15, (ViewGroup$LayoutParams)tableRow$LayoutParams);
        ((ViewGroup)tableRow3).addView((View)g16, (ViewGroup$LayoutParams)tableRow$LayoutParams);
        tableLayout.addView((View)tableRow3);
        ((View)linearLayout4).setVisibility(8);
        this.A(resultItem, this.l, linearLayout);
    }
    
    @Override
    public void onCreate(final Bundle bundle) {
        super.onCreate(bundle);
        this.setContentView(2131492929);
        this.x((Toolbar)this.findViewById(2131297334));
        this.h = FirebaseAnalytics.getInstance((Context)this);
        this.e = ((Context)this).getApplicationContext().getSharedPreferences("MyPref", 0);
        this.g = this.getIntent().getIntExtra("SELECTED_POSITION", 0);
        final ExamId b = e8.b;
        this.k = b;
        this.l = e8.a;
        this.f = e8.c;
        if (b == null || b.getExamName().equals("")) {
            this.finish();
        }
        this.m = this.l.getLabelProfile();
        this.n = this.l.getTemplateParams();
        this.d = (LinearLayout)this.findViewById(2131296805);
        this.i = (TextView)this.findViewById(2131297270);
        this.H();
        this.I();
        this.j = Settings$Secure.getString(((Context)this).getContentResolver(), "android_id");
        this.J();
    }
    
    @Override
    public void onResume() {
        super.onResume();
        this.K();
    }
    
    @Override
    public boolean v() {
        this.finish();
        return true;
    }
}

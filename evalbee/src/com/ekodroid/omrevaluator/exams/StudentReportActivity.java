// 
// Decompiled by Procyon v0.6.0
// 

package com.ekodroid.omrevaluator.exams;

import android.os.BaseBundle;
import android.view.ViewGroup$MarginLayoutParams;
import android.app.Dialog;
import android.content.IntentFilter;
import android.view.MenuItem;
import android.view.Menu;
import android.provider.Settings$Secure;
import com.google.gson.reflect.TypeToken;
import androidx.appcompat.widget.Toolbar;
import android.content.SharedPreferences$Editor;
import android.content.DialogInterface;
import android.content.DialogInterface$OnClickListener;
import android.widget.CompoundButton;
import android.widget.CompoundButton$OnCheckedChangeListener;
import android.widget.CheckBox;
import androidx.appcompat.widget.SwitchCompat;
import com.google.android.material.dialog.MaterialAlertDialogBuilder;
import com.ekodroid.omrevaluator.database.ResultDataJsonModel;
import android.widget.ImageView;
import com.ekodroid.omrevaluator.templateui.scanner.SheetDimension;
import com.ekodroid.omrevaluator.templateui.scanner.b;
import android.graphics.BitmapFactory;
import com.ekodroid.omrevaluator.database.SheetImageModel;
import android.view.ViewGroup$LayoutParams;
import android.graphics.Typeface;
import android.widget.TableRow;
import android.widget.TableRow$LayoutParams;
import android.widget.TableLayout;
import com.ekodroid.omrevaluator.clients.SyncClients.Models.PublishRequest;
import com.ekodroid.omrevaluator.clients.SyncClients.PostPublishReport;
import android.os.Bundle;
import com.ekodroid.omrevaluator.clients.SyncClients.Models.PublishResponse;
import com.google.firebase.auth.FirebaseAuth;
import com.ekodroid.omrevaluator.database.StudentDataModel;
import com.ekodroid.omrevaluator.database.repositories.ClassRepository;
import java.io.Serializable;
import android.graphics.Bitmap;
import com.ekodroid.omrevaluator.database.repositories.TemplateRepository;
import com.ekodroid.omrevaluator.templateui.scansheet.EditResultActivity;
import android.view.View$OnClickListener;
import android.content.Intent;
import android.content.res.Resources$Theme;
import com.ekodroid.omrevaluator.templateui.models.AnswerOptionKey;
import com.ekodroid.omrevaluator.templateui.scanner.AnswerValue;
import com.ekodroid.omrevaluator.activities.Services.DownloadSheetImageService;
import android.content.Context;
import com.ekodroid.omrevaluator.clients.SyncClients.Models.ReportAction;
import java.util.Iterator;
import android.view.View;
import android.view.ViewGroup;
import android.view.LayoutInflater;
import com.ekodroid.omrevaluator.templateui.scanner.ResultItem;
import com.ekodroid.omrevaluator.exams.models.RollNoListDataModel;
import android.widget.TextView;
import android.widget.ImageButton;
import com.ekodroid.omrevaluator.templateui.models.TemplateParams2;
import com.ekodroid.omrevaluator.database.repositories.ResultRepository;
import com.ekodroid.omrevaluator.templateui.models.LabelProfile;
import com.ekodroid.omrevaluator.templateui.models.SheetTemplate2;
import com.ekodroid.omrevaluator.database.TemplateDataJsonModel;
import com.ekodroid.omrevaluator.templateui.models.ExamId;
import android.app.ProgressDialog;
import com.google.firebase.analytics.FirebaseAnalytics;
import java.util.ArrayList;
import android.content.SharedPreferences;
import android.widget.LinearLayout;
import android.content.BroadcastReceiver;
import android.widget.Toast;

public class StudentReportActivity extends v5
{
    public Toast A;
    public BroadcastReceiver C;
    public StudentReportActivity c;
    public LinearLayout d;
    public SharedPreferences e;
    public ArrayList f;
    public int g;
    public FirebaseAnalytics h;
    public ArrayList i;
    public ArrayList j;
    public ProgressDialog k;
    public String l;
    public ExamId m;
    public TemplateDataJsonModel n;
    public SheetTemplate2 p;
    public LabelProfile q;
    public ResultRepository t;
    public TemplateParams2 v;
    public boolean w;
    public ImageButton x;
    public ImageButton y;
    public TextView z;
    
    public StudentReportActivity() {
        this.c = this;
        this.g = 0;
        this.w = false;
    }
    
    public static /* synthetic */ Toast C(final StudentReportActivity studentReportActivity) {
        return studentReportActivity.A;
    }
    
    public static /* synthetic */ ExamId D(final StudentReportActivity studentReportActivity) {
        return studentReportActivity.m;
    }
    
    public static /* synthetic */ ProgressDialog H(final StudentReportActivity studentReportActivity) {
        return studentReportActivity.k;
    }
    
    public final void A(final ResultItem resultItem, final SheetTemplate2 sheetTemplate2, final LinearLayout linearLayout) {
        ((ViewGroup)linearLayout).removeAllViews();
        final ArrayList l = this.L(resultItem, sheetTemplate2);
        final LayoutInflater layoutInflater = (LayoutInflater)((Context)this.c).getSystemService("layout_inflater");
        if (layoutInflater != null) {
            for (final i5 i5 : l) {
                final LinearLayout linearLayout2 = (LinearLayout)layoutInflater.inflate(2131493018, (ViewGroup)null);
                final TextView textView = (TextView)((View)linearLayout2).findViewById(2131297263);
                final TextView textView2 = (TextView)((View)linearLayout2).findViewById(2131297248);
                final TextView textView3 = (TextView)((View)linearLayout2).findViewById(2131297217);
                final TextView textView4 = (TextView)((View)linearLayout2).findViewById(2131297209);
                textView.setText((CharSequence)i5.d());
                textView2.setText((CharSequence)i5.c());
                textView3.setText((CharSequence)i5.b());
                textView4.setText((CharSequence)i5.a());
                ((ViewGroup)linearLayout).addView((View)linearLayout2);
            }
        }
    }
    
    public final void J() {
        final int rollNo = this.f.get(this.g).getRollNo();
        if (this.n.isCloudSyncOn()) {
            final ArrayList list = new ArrayList();
            list.add(new ReportAction(this.n.getCloudId(), rollNo, true));
            a91.K((Context)this.c, list);
        }
        final boolean deleteStudentResult = ResultRepository.getInstance((Context)this.c).deleteStudentResult(this.m, rollNo);
        final boolean deleteSheetImages = ResultRepository.getInstance((Context)this.c).deleteSheetImages(this.m, rollNo);
        if (deleteStudentResult && deleteSheetImages) {
            this.f.remove(this.g);
            if (this.f.size() < 1) {
                this.c.finish();
                return;
            }
            if (this.f.size() - 1 < this.g) {
                this.g = this.f.size() - 1;
            }
            a91.G((Context)this.c, 2131886744, 2131230927, 2131231085);
            this.W();
        }
        else {
            a91.G((Context)this.c, 2131886745, 2131230909, 2131231086);
        }
    }
    
    public final void K(final ExamId examId, final int n) {
        if (this.n.isCloudSyncOn() && this.n.isSyncImages()) {
            DownloadSheetImageService.h((Context)this.c, examId, n);
        }
    }
    
    public final ArrayList L(final ResultItem resultItem, final SheetTemplate2 sheetTemplate2) {
        final ArrayList list = new ArrayList();
        for (int i = 0; i < resultItem.getAnswerValue2s().size(); ++i) {
            final AnswerValue answerValue = resultItem.getAnswerValue2s().get(i);
            final StringBuilder sb = new StringBuilder();
            final String s = "";
            sb.append("");
            sb.append(answerValue.getQuestionNumber());
            final String string = sb.toString();
            final String r = e5.r(answerValue, sheetTemplate2.getLabelProfile());
            final StringBuilder sb2 = new StringBuilder();
            sb2.append("");
            sb2.append(ve1.o(answerValue.getMarksForAnswer()));
            final String string2 = sb2.toString();
            final ArrayList b = ve1.b(sheetTemplate2, resultItem.getExamSet());
            String q = s;
            if (b != null) {
                q = s;
                if (b.size() > 0) {
                    q = e5.q(b.get(i), sheetTemplate2.getLabelProfile());
                }
            }
            list.add(new i5(string, r, q, string2));
        }
        return list;
    }
    
    public final int M() {
        if (this.w) {
            return -1;
        }
        return this.f.get(this.g).getRank();
    }
    
    public final RollNoListDataModel N() {
        if (this.g > this.f.size() - 1) {
            this.g = 0;
        }
        return this.f.get(this.g);
    }
    
    public final TextView O() {
        final TextView textView = new TextView((Context)this);
        textView.setMinWidth(a91.d(90, (Context)this.c));
        textView.setGravity(16);
        textView.setPadding(a91.d(8, (Context)this.c), 0, a91.d(8, (Context)this.c), 0);
        textView.setTextAppearance(2131952181);
        ((View)textView).setBackgroundColor(this.getResources().getColor(2131099740, (Resources$Theme)null));
        return textView;
    }
    
    public final void P() {
        this.C = new BroadcastReceiver(this) {
            public final StudentReportActivity a;
            
            public void onReceive(final Context context, final Intent intent) {
                this.a.W();
            }
        };
    }
    
    public final void Q() {
        ((View)(this.x = (ImageButton)this.findViewById(2131296670))).setOnClickListener((View$OnClickListener)new View$OnClickListener(this) {
            public final StudentReportActivity a;
            
            public void onClick(final View view) {
                if (StudentReportActivity.C(this.a) != null) {
                    StudentReportActivity.C(this.a).cancel();
                }
                final StudentReportActivity a = this.a;
                if (a.g < a.f.size() - 1) {
                    final StudentReportActivity a2 = this.a;
                    ++a2.g;
                    a2.K(StudentReportActivity.D(a2), this.a.N().getRollNo());
                    this.a.W();
                }
            }
        });
    }
    
    public final void R() {
        ((View)(this.y = (ImageButton)this.findViewById(2131296672))).setOnClickListener((View$OnClickListener)new View$OnClickListener(this) {
            public final StudentReportActivity a;
            
            public void onClick(final View view) {
                if (StudentReportActivity.C(this.a) != null) {
                    StudentReportActivity.C(this.a).cancel();
                }
                final StudentReportActivity a = this.a;
                final int g = a.g;
                if (g > 0) {
                    a.g = g - 1;
                    a.K(StudentReportActivity.D(a), this.a.N().getRollNo());
                    this.a.W();
                }
            }
        });
    }
    
    public final void S() {
        final int rollNo = this.f.get(this.g).getRollNo();
        final Intent intent = new Intent((Context)this.c, (Class)EditResultActivity.class);
        final ResultItem resultItem = ResultRepository.getInstance((Context)this.c).getResult(this.m, rollNo).getResultItem(TemplateRepository.getInstance((Context)this.c).getTemplateJson(this.m).getSheetTemplate());
        final ArrayList i = this.i;
        if (i != null && i.size() > 0) {
            jx0.b = new ArrayList();
            final Iterator iterator = this.i.iterator();
            while (iterator.hasNext()) {
                jx0.b.add(bc.a((Bitmap)iterator.next()));
            }
            jx0.a = this.j;
        }
        intent.putExtra("ROLL_NUMBER", rollNo);
        intent.putExtra("RESULT_ITEM", (Serializable)resultItem);
        intent.putExtra("EXAM_ID", (Serializable)this.m);
        this.c.startActivityForResult(intent, 5);
    }
    
    public final void T() {
        final TemplateDataJsonModel templateJson = TemplateRepository.getInstance((Context)this.c).getTemplateJson(this.m);
        final ArrayList b = ve1.b(templateJson.getSheetTemplate(), 0);
        if (b == null || b.size() <= 0) {
            a91.G((Context)this.c, 2131886568, 2131230909, 2131231086);
            return;
        }
        final StudentDataModel student = ClassRepository.getInstance((Context)this.c).getStudent(this.f.get(this.g).getRollNo(), this.m.getClassName());
        if (student == null || student.getEmailId() == null || student.getEmailId().length() < 5) {
            a91.G((Context)this.c, 2131886125, 2131230909, 2131231086);
            return;
        }
        if (!templateJson.isCloudSyncOn()) {
            xs.c((Context)this.c, null, 0, 2131886577, 2131886176, 0, 0, 0);
            return;
        }
        this.Z(student.getStudentName(), student.getEmailId(), student.getPhoneNo());
    }
    
    public final void U(final boolean b, final boolean b2, final Long n) {
        final r30 e = FirebaseAuth.getInstance().e();
        final PublishRequest b3 = x91.b((Context)this.c, this.m, b, b2, n, e.getEmail(), e.getDisplayName(), this.f.get(this.g).getRollNo());
        if (b3.publishMetaDataDtos.size() == 0) {
            xs.c((Context)this.c, null, 0, 2131886450, 2131886176, 0, 0, 0);
            return;
        }
        final PostPublishReport postPublishReport = new PostPublishReport(b3, (Context)this.c, new zg(this) {
            public final StudentReportActivity a;
            
            @Override
            public void a(final boolean b, final int n, final Object o) {
                ((Dialog)StudentReportActivity.H(this.a)).dismiss();
                FirebaseAnalytics firebaseAnalytics;
                String s;
                if (b) {
                    final PublishResponse publishResponse = (PublishResponse)o;
                    ResultRepository.getInstance((Context)this.a.c).markResultAsPublished(StudentReportActivity.D(this.a), publishResponse.rollSet);
                    TemplateRepository.getInstance((Context)this.a.c).markExamAsPublished(StudentReportActivity.D(this.a), publishResponse.cloudId);
                    a91.G((Context)this.a.c, 2131886733, 2131230927, 2131231085);
                    firebaseAnalytics = FirebaseAnalytics.getInstance((Context)this.a.c);
                    s = "WebPublishIndividualSuccess";
                }
                else {
                    a91.G((Context)this.a.c, 2131886732, 2131230909, 2131231086);
                    firebaseAnalytics = FirebaseAnalytics.getInstance((Context)this.a.c);
                    s = "WebPublishIndividualFail";
                }
                firebaseAnalytics.a(s, null);
            }
        });
        this.Y(true);
        postPublishReport.f();
    }
    
    public final void V() {
        ((View)this.y).setEnabled(true);
        ((View)this.x).setEnabled(true);
        if (this.g == 0) {
            ((View)this.y).setEnabled(false);
        }
        if (this.g == this.f.size() - 1) {
            ((View)this.x).setEnabled(false);
        }
        final TextView z = this.z;
        final StringBuilder sb = new StringBuilder();
        sb.append(((Context)this).getString(2131886730));
        sb.append(" ");
        sb.append(this.g + 1);
        sb.append("/");
        sb.append(this.f.size());
        z.setText((CharSequence)sb.toString());
    }
    
    public final void W() {
        this.V();
        final RollNoListDataModel n = this.N();
        final int rollNo = n.getRollNo();
        final ResultDataJsonModel result = this.t.getResult(this.m, rollNo);
        if (result == null) {
            o4.c(this.h, "SRA_RESULT_NULL", this.l);
            ++this.g;
            this.W();
        }
        if (this.n() != null) {
            final t1 n2 = this.n();
            final StringBuilder sb = new StringBuilder();
            sb.append(this.q.getRollNoString());
            sb.append(" : ");
            sb.append(rollNo);
            n2.t(sb.toString());
        }
        final StudentDataModel student = ClassRepository.getInstance((Context)this.c).getStudent(rollNo, this.m.getClassName());
        final ResultItem resultItem = result.getResultItem(this.p);
        final double[] marksForEachSubject = resultItem.getMarksForEachSubject();
        final double[][] k = ve1.k(resultItem, this.p);
        final int[][] c = ve1.c(resultItem, this.p);
        final int[][] j = ve1.j(resultItem, this.p);
        final int[] h = ve1.h(c);
        final int[] h2 = ve1.h(j);
        final double[][] r = ve1.r(this.p);
        final double[][] n3 = ve1.n(k, r);
        final double[] g = ve1.g(r);
        final double[] m = ve1.m(marksForEachSubject, g);
        final double totalMarks = resultItem.getTotalMarks();
        final double n4 = 100.0 * totalMarks / ve1.d(g);
        String i;
        if (this.p.getGradeLevels() != null) {
            i = ve1.i(totalMarks, this.p.getGradeLevels());
        }
        else {
            i = null;
        }
        final TextView textView = (TextView)this.findViewById(2131297247);
        final TextView textView2 = (TextView)this.findViewById(2131297242);
        final TextView textView3 = (TextView)this.findViewById(2131297243);
        final TextView textView4 = (TextView)this.findViewById(2131297244);
        final TextView textView5 = (TextView)this.findViewById(2131297245);
        final TextView textView6 = (TextView)this.findViewById(2131297246);
        textView4.setText((CharSequence)this.q.getExamSetString());
        textView3.setText((CharSequence)this.q.getExamNameString());
        textView6.setText((CharSequence)this.q.getRankString());
        textView5.setText((CharSequence)this.q.getGradeString());
        textView2.setText((CharSequence)this.q.getClassString());
        textView.setText((CharSequence)this.q.getNameString());
        final TextView textView7 = (TextView)this.findViewById(2131297292);
        final TextView textView8 = (TextView)this.findViewById(2131297287);
        final TextView textView9 = (TextView)this.findViewById(2131297289);
        final TextView textView10 = (TextView)this.findViewById(2131297290);
        final TextView textView11 = (TextView)this.findViewById(2131297294);
        final TextView textView12 = (TextView)this.findViewById(2131297291);
        final TableLayout tableLayout = (TableLayout)this.findViewById(2131297148);
        ((ViewGroup)tableLayout).removeViews(1, ((ViewGroup)tableLayout).getChildCount() - 1);
        Object o = this.findViewById(2131296730);
        final LinearLayout linearLayout = (LinearLayout)this.findViewById(2131296731);
        final LinearLayout linearLayout2 = (LinearLayout)this.findViewById(2131296762);
        if (this.M() > 0) {
            textView11.setText((CharSequence)String.valueOf(n.getRank()));
        }
        else {
            this.findViewById(2131296788).setVisibility(8);
        }
        if (i != null) {
            textView12.setText((CharSequence)i);
        }
        else {
            this.findViewById(2131296757).setVisibility(8);
        }
        textView8.setText((CharSequence)this.m.getClassName());
        String studentName;
        if (student != null) {
            studentName = student.getStudentName();
        }
        else {
            studentName = "---";
        }
        textView7.setText((CharSequence)studentName);
        textView9.setText((CharSequence)this.m.getExamName());
        if (this.v.getExamSets() - 1 != 0) {
            final String[] examSetLabels = this.p.getLabelProfile().getExamSetLabels();
            int n5;
            if ((n5 = resultItem.getExamSet() - 1) < 0) {
                n5 = 0;
            }
            textView10.setText((CharSequence)examSetLabels[n5]);
        }
        else {
            this.findViewById(2131296750).setVisibility(8);
        }
        ((TextView)this.findViewById(2131297164)).setText((CharSequence)this.q.getCorrectAnswerString());
        ((TextView)this.findViewById(2131297165)).setText((CharSequence)this.q.getIncorrectAnswerString());
        ((TextView)this.findViewById(2131297168)).setText((CharSequence)this.q.getSubjectString());
        ((TextView)this.findViewById(2131297166)).setText((CharSequence)this.q.getMarksString());
        ((TextView)this.findViewById(2131297167)).setText((CharSequence)this.q.getPercentageString());
        final TableRow$LayoutParams tableRow$LayoutParams = new TableRow$LayoutParams(-2, a91.d(32, (Context)this.c));
        final int d = a91.d(1, (Context)this.c);
        ((ViewGroup$MarginLayoutParams)tableRow$LayoutParams).setMargins(d, d, d, d);
        int l = 0;
        final double[] array = m;
        while (l < marksForEachSubject.length) {
            final TableRow tableRow = new TableRow((Context)this.c);
            final TextView o2 = this.O();
            o2.setTypeface((Typeface)null, 1);
            o2.setText((CharSequence)this.v.getSubjects()[l].getSubName());
            final TextView o3 = this.O();
            o3.setTypeface((Typeface)null, 1);
            final StringBuilder sb2 = new StringBuilder();
            sb2.append("");
            sb2.append(ve1.o(marksForEachSubject[l]));
            o3.setText((CharSequence)sb2.toString());
            ((ViewGroup)tableRow).addView((View)o2, (ViewGroup$LayoutParams)tableRow$LayoutParams);
            ((ViewGroup)tableRow).addView((View)o3, (ViewGroup$LayoutParams)tableRow$LayoutParams);
            final TextView o4 = this.O();
            o4.setTypeface((Typeface)null, 1);
            final StringBuilder sb3 = new StringBuilder();
            sb3.append(ve1.o(array[l]));
            sb3.append("%");
            o4.setText((CharSequence)sb3.toString());
            ((ViewGroup)tableRow).addView((View)o4, (ViewGroup$LayoutParams)tableRow$LayoutParams);
            final TextView o5 = this.O();
            o5.setTypeface((Typeface)null, 1);
            final StringBuilder sb4 = new StringBuilder();
            sb4.append("");
            sb4.append(h[l]);
            o5.setText((CharSequence)sb4.toString());
            ((ViewGroup)tableRow).addView((View)o5, (ViewGroup$LayoutParams)tableRow$LayoutParams);
            final TextView o6 = this.O();
            o6.setTypeface((Typeface)null, 1);
            final StringBuilder sb5 = new StringBuilder();
            sb5.append("");
            sb5.append(h2[l]);
            o6.setText((CharSequence)sb5.toString());
            ((ViewGroup)tableRow).addView((View)o6, (ViewGroup$LayoutParams)tableRow$LayoutParams);
            tableLayout.addView((View)tableRow);
            for (int n6 = 0; n6 < k[l].length; ++n6) {
                final TableRow tableRow2 = new TableRow((Context)this.c);
                final TextView o7 = this.O();
                o7.setText((CharSequence)this.v.getSubjects()[l].getSections()[n6].getName());
                final TextView o8 = this.O();
                final StringBuilder sb6 = new StringBuilder();
                sb6.append("");
                sb6.append(ve1.o(k[l][n6]));
                o8.setText((CharSequence)sb6.toString());
                final TextView o9 = this.O();
                final StringBuilder sb7 = new StringBuilder();
                sb7.append(ve1.o(n3[l][n6]));
                sb7.append("%");
                o9.setText((CharSequence)sb7.toString());
                final TextView o10 = this.O();
                final StringBuilder sb8 = new StringBuilder();
                sb8.append("");
                sb8.append(c[l][n6]);
                o10.setText((CharSequence)sb8.toString());
                final TextView o11 = this.O();
                final StringBuilder sb9 = new StringBuilder();
                sb9.append("");
                sb9.append(j[l][n6]);
                o11.setText((CharSequence)sb9.toString());
                ((ViewGroup)tableRow2).addView((View)o7, (ViewGroup$LayoutParams)tableRow$LayoutParams);
                ((ViewGroup)tableRow2).addView((View)o8, (ViewGroup$LayoutParams)tableRow$LayoutParams);
                ((ViewGroup)tableRow2).addView((View)o9, (ViewGroup$LayoutParams)tableRow$LayoutParams);
                ((ViewGroup)tableRow2).addView((View)o10, (ViewGroup$LayoutParams)tableRow$LayoutParams);
                ((ViewGroup)tableRow2).addView((View)o11, (ViewGroup$LayoutParams)tableRow$LayoutParams);
                tableLayout.addView((View)tableRow2);
            }
            ++l;
        }
        final TableRow tableRow3 = new TableRow((Context)this.c);
        final TextView o12 = this.O();
        o12.setTypeface((Typeface)null, 1);
        o12.setText((CharSequence)this.q.getTotalMarksString());
        final TextView o13 = this.O();
        o13.setTypeface((Typeface)null, 1);
        final StringBuilder sb10 = new StringBuilder();
        sb10.append("");
        sb10.append(ve1.o(totalMarks));
        o13.setText((CharSequence)sb10.toString());
        final TextView o14 = this.O();
        o14.setTypeface((Typeface)null, 1);
        final StringBuilder sb11 = new StringBuilder();
        sb11.append(ve1.o(n4));
        sb11.append("%");
        o14.setText((CharSequence)sb11.toString());
        final TextView o15 = this.O();
        o15.setTypeface((Typeface)null, 1);
        final StringBuilder sb12 = new StringBuilder();
        sb12.append("");
        sb12.append(ve1.e(h));
        o15.setText((CharSequence)sb12.toString());
        final TextView o16 = this.O();
        o16.setTypeface((Typeface)null, 1);
        final StringBuilder sb13 = new StringBuilder();
        sb13.append("");
        sb13.append(ve1.e(h2));
        o16.setText((CharSequence)sb13.toString());
        ((ViewGroup)tableRow3).addView((View)o12, (ViewGroup$LayoutParams)tableRow$LayoutParams);
        ((ViewGroup)tableRow3).addView((View)o13, (ViewGroup$LayoutParams)tableRow$LayoutParams);
        ((ViewGroup)tableRow3).addView((View)o14, (ViewGroup$LayoutParams)tableRow$LayoutParams);
        ((ViewGroup)tableRow3).addView((View)o15, (ViewGroup$LayoutParams)tableRow$LayoutParams);
        ((ViewGroup)tableRow3).addView((View)o16, (ViewGroup$LayoutParams)tableRow$LayoutParams);
        tableLayout.addView((View)tableRow3);
        final ArrayList<SheetImageModel> sheetImages = this.t.getSheetImages(this.m, rollNo);
        ((ViewGroup)linearLayout2).removeAllViews();
        ((View)linearLayout2).setVisibility(8);
        final ArrayList i2 = this.i;
        if (i2 != null && i2.size() > 0) {
            for (final Bitmap bitmap : this.i) {
                if (bitmap != null && !bitmap.isRecycled()) {
                    bitmap.recycle();
                }
            }
        }
        this.i = null;
        Object o17 = o;
        Object i3 = o;
        Label_2424: {
            Label_2419: {
                try {
                    Label_2406: {
                        if (sheetImages.size() > 0) {
                            o17 = o;
                            i3 = o;
                            if (sheetImages.size() == this.p.getPageLayouts().length) {
                                i3 = o;
                                e5.E(sheetImages);
                                i3 = o;
                                i3 = o;
                                final ArrayList i4 = new ArrayList();
                                i3 = o;
                                this.i = i4;
                                i3 = o;
                                i3 = o;
                                final ArrayList j2 = new ArrayList();
                                i3 = o;
                                this.j = j2;
                                i3 = o;
                                final Iterator<SheetImageModel> iterator2 = sheetImages.iterator();
                                while (true) {
                                    i3 = o;
                                    if (iterator2.hasNext()) {
                                        i3 = o;
                                        final SheetImageModel sheetImageModel = iterator2.next();
                                        i3 = o;
                                        sheetImageModel.getImagePath();
                                        i3 = o;
                                        final Bitmap decodeFile = BitmapFactory.decodeFile(sheetImageModel.getImagePath());
                                        if (decodeFile == null) {
                                            i3 = o;
                                            i3 = o;
                                            final Exception ex = new Exception("Image Missing");
                                            i3 = o;
                                            throw ex;
                                        }
                                        i3 = o;
                                        this.i.add(decodeFile);
                                        i3 = o;
                                        final SheetDimension sheetDimension = sheetImageModel.getSheetDimension();
                                        if (sheetDimension == null) {
                                            i3 = o;
                                            i3 = o;
                                            final Exception ex2 = new Exception("Image Missing");
                                            i3 = o;
                                            throw ex2;
                                        }
                                        i3 = o;
                                        this.j.add(sheetDimension);
                                    }
                                    else {
                                        int index = 0;
                                        i3 = o;
                                        ((View)linearLayout2).setVisibility(0);
                                        i3 = o;
                                        i3 = o;
                                        final com.ekodroid.omrevaluator.templateui.scanner.b b = new com.ekodroid.omrevaluator.templateui.scanner.b();
                                        while (true) {
                                            o17 = o;
                                            i3 = o;
                                            if (index < this.i.size()) {
                                                i3 = o;
                                                final Bitmap bitmap2 = this.i.get(index);
                                                i3 = o;
                                                final SheetTemplate2 p = this.p;
                                                i3 = o;
                                                final SheetDimension sheetDimension2 = this.j.get(index);
                                                i3 = o;
                                                final int pageIndex = sheetImages.get(index).getPageIndex();
                                                try {
                                                    i3 = b.i(bitmap2, p, resultItem, sheetDimension2, rollNo, pageIndex);
                                                    final ImageView imageView = new ImageView((Context)this.c);
                                                    imageView.setAdjustViewBounds(true);
                                                    ((ViewGroup)linearLayout2).addView((View)imageView);
                                                    imageView.setImageBitmap((Bitmap)i3);
                                                    ++index;
                                                    continue;
                                                }
                                                catch (final Exception ex3) {
                                                    break Label_2419;
                                                }
                                                break Label_2406;
                                            }
                                            break Label_2406;
                                        }
                                    }
                                }
                            }
                        }
                    }
                    o = o17;
                    break Label_2424;
                }
                catch (final Exception ex3) {
                    o = i3;
                }
            }
            final Exception ex3;
            ex3.printStackTrace();
        }
        this.A(resultItem, this.p, (LinearLayout)o);
    }
    
    public final void X() {
        xs.c((Context)this.c, new y01(this) {
            public final StudentReportActivity a;
            
            @Override
            public void a(final Object o) {
                this.a.J();
            }
        }, 2131886238, 2131886584, 2131886903, 2131886657, 0, 2131230930);
    }
    
    public final void Y(final boolean b) {
        if (b) {
            (this.k = new ProgressDialog((Context)this.c)).setMessage((CharSequence)((Context)this).getString(2131886565));
            ((Dialog)this.k).setCancelable(true);
            ((Dialog)this.k).show();
        }
    }
    
    public final void Z(final String s, final String str, final String s2) {
        final MaterialAlertDialogBuilder materialAlertDialogBuilder = new MaterialAlertDialogBuilder((Context)this.c, 2131951953);
        final View inflate = ((LayoutInflater)((Context)this).getSystemService("layout_inflater")).inflate(2131492996, (ViewGroup)null);
        materialAlertDialogBuilder.setView(inflate);
        materialAlertDialogBuilder.setTitle(2131886797);
        final TextView textView = (TextView)inflate.findViewById(2131297211);
        final SwitchCompat switchCompat = (SwitchCompat)inflate.findViewById(2131297139);
        final CheckBox checkBox = (CheckBox)inflate.findViewById(2131296503);
        final TextView textView2 = (TextView)inflate.findViewById(2131297288);
        ((CompoundButton)checkBox).setChecked(this.e.getBoolean("include_rank", false));
        switchCompat.setOnCheckedChangeListener((CompoundButton$OnCheckedChangeListener)new CompoundButton$OnCheckedChangeListener(this, FirebaseAuth.getInstance().e(), textView) {
            public final r30 a;
            public final TextView b;
            public final StudentReportActivity c;
            
            public void onCheckedChanged(final CompoundButton compoundButton, final boolean b) {
                if (b && this.a != null) {
                    ((View)this.b).setVisibility(0);
                    this.b.setText((CharSequence)this.a.getEmail());
                }
                else {
                    ((View)this.b).setVisibility(8);
                }
            }
        });
        final StringBuilder sb = new StringBuilder();
        sb.append(((Context)this).getString(2131886258));
        sb.append(" : ");
        sb.append(str);
        textView2.setText((CharSequence)sb.toString());
        materialAlertDialogBuilder.setPositiveButton(2131886717, (DialogInterface$OnClickListener)new DialogInterface$OnClickListener(this, checkBox, switchCompat) {
            public final CheckBox a;
            public final SwitchCompat b;
            public final StudentReportActivity c;
            
            public void onClick(final DialogInterface dialogInterface, final int n) {
                final SharedPreferences$Editor edit = this.c.e.edit();
                edit.putBoolean("include_rank", ((CompoundButton)this.a).isChecked());
                edit.commit();
                this.c.U(((CompoundButton)this.a).isChecked(), this.b.isChecked(), 0L);
                dialogInterface.dismiss();
                FirebaseAnalytics.getInstance((Context)this.c.c).a("reportPublishIndividual", null);
            }
        });
        materialAlertDialogBuilder.setNegativeButton(2131886163, (DialogInterface$OnClickListener)new DialogInterface$OnClickListener(this) {
            public final StudentReportActivity a;
            
            public void onClick(final DialogInterface dialogInterface, final int n) {
                dialogInterface.dismiss();
            }
        });
        materialAlertDialogBuilder.create().show();
    }
    
    @Override
    public void onActivityResult(int int1, int int2, final Intent intent) {
        super.onActivityResult(int1, int2, intent);
        if (int1 == 5 && intent != null) {
            final int rollNo = this.f.get(this.g).getRollNo();
            if (intent.getExtras() == null) {
                return;
            }
            final ResultItem resultItem = (ResultItem)intent.getExtras().getSerializable("RESULT_ITEM");
            if (resultItem == null) {
                return;
            }
            int1 = ((BaseBundle)intent.getExtras()).getInt("ROLL_NUMBER");
            int2 = ((BaseBundle)intent.getExtras()).getInt("EXAM_SET");
            final SheetTemplate2 sheetTemplate = TemplateRepository.getInstance((Context)this.c).getTemplateJson(this.m).getSheetTemplate();
            final ResultDataJsonModel result = ResultRepository.getInstance((Context)this.c).getResult(this.m, rollNo);
            final ArrayList<SheetImageModel> sheetImages = ResultRepository.getInstance((Context)this.c).getSheetImages(this.m, rollNo);
            if (int1 != rollNo) {
                result.setRollNo(int1);
                final ResultItem resultItem2 = result.getResultItem(sheetTemplate);
                resultItem2.setExamSet(int2);
                resultItem2.setAnswerValues(resultItem.getAnswerValue2s());
                result.setResultItem(resultItem2);
                result.setId(null);
                result.setSynced(false);
                ResultRepository.getInstance((Context)this.c).deleteStudentResult(this.m, rollNo);
                ResultRepository.getInstance((Context)this.c).deleteStudentResult(this.m, int1);
                ResultRepository.getInstance((Context)this.c).saveOrUpdateResultJsonAsNotSynced(result);
                if (this.n.isCloudSyncOn()) {
                    final ArrayList list = new ArrayList();
                    list.add(new ReportAction(this.n.getCloudId(), rollNo, true));
                    a91.K((Context)this.c, list);
                }
                if (sheetImages.size() > 0) {
                    ResultRepository.getInstance((Context)this.c).deleteSheetImages(this.m, int1);
                    for (final SheetImageModel sheetImageModel : sheetImages) {
                        if (sheetImageModel != null && sheetImageModel.getSheetImage() != null) {
                            sheetImageModel.setRollNo(int1);
                            sheetImageModel.setId(null);
                            ResultRepository.getInstance((Context)this.c).saveOrUpdateSheetImage(sheetImageModel);
                        }
                    }
                    ResultRepository.getInstance((Context)this.c).deleteSheetModelKeepingImages(this.m, rollNo);
                }
                this.f.get(this.g).setRollNo(int1);
            }
            else {
                final ResultItem resultItem3 = result.getResultItem(sheetTemplate);
                resultItem3.setExamSet(int2);
                resultItem3.setAnswerValues(resultItem.getAnswerValue2s());
                result.setResultItem(resultItem3);
                result.setSynced(false);
                ResultRepository.getInstance((Context)this.c).saveOrUpdateResultJsonAsNotSynced(result);
            }
            this.w = true;
            FirebaseAnalytics.getInstance((Context)this.c).a("ResultEdit", null);
            this.W();
        }
    }
    
    @Override
    public void onCreate(final Bundle bundle) {
        super.onCreate(bundle);
        this.setContentView(2131492928);
        this.x((Toolbar)this.findViewById(2131297334));
        this.z = (TextView)this.findViewById(2131297270);
        this.h = FirebaseAnalytics.getInstance((Context)this);
        this.e = ((Context)this).getApplicationContext().getSharedPreferences("MyPref", 0);
        this.g = this.getIntent().getIntExtra("SELECTED_POSITION", 0);
        final ExamId m = (ExamId)this.getIntent().getSerializableExtra("EXAM_ID");
        this.m = m;
        if (m == null || m.getExamName().equals("")) {
            this.finish();
        }
        final TemplateDataJsonModel templateJson = TemplateRepository.getInstance((Context)this.c).getTemplateJson(this.m);
        this.n = templateJson;
        final SheetTemplate2 sheetTemplate = templateJson.getSheetTemplate();
        this.p = sheetTemplate;
        this.q = sheetTemplate.getLabelProfile();
        this.t = ResultRepository.getInstance((Context)this.c);
        this.v = this.p.getTemplateParams();
        this.f = (ArrayList)new gc0().k(new String(this.t.getFile(this.getIntent().getStringExtra("ROLLNO_LIST_FILE_NAME")).getFileData()), new TypeToken<ArrayList<RollNoListDataModel>>(this) {
            public final StudentReportActivity a;
        }.getType());
        this.d = (LinearLayout)this.findViewById(2131296805);
        this.Q();
        this.R();
        this.l = Settings$Secure.getString(((Context)this).getContentResolver(), "android_id");
        this.P();
        this.K(this.m, this.N().getRollNo());
    }
    
    public boolean onCreateOptionsMenu(final Menu menu) {
        this.getMenuInflater().inflate(2131623947, menu);
        return true;
    }
    
    public boolean onOptionsItemSelected(final MenuItem menuItem) {
        final int itemId = menuItem.getItemId();
        if (itemId != 2131296319) {
            if (itemId != 2131296324) {
                if (itemId == 2131296334) {
                    this.T();
                }
            }
            else {
                this.S();
            }
        }
        else {
            this.X();
        }
        return false;
    }
    
    @Override
    public void onPause() {
        super.onPause();
        ((Context)this).unregisterReceiver(this.C);
    }
    
    @Override
    public void onResume() {
        super.onResume();
        sl.registerReceiver((Context)this.c, this.C, new IntentFilter("UPDATE_SHEET_IMAGE"), 4);
        this.W();
    }
    
    @Override
    public boolean v() {
        this.finish();
        return true;
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.ekodroid.omrevaluator.exams.sms;

import com.ekodroid.omrevaluator.serializable.SmsAccount;
import java.io.Serializable;

public class SmsData implements Serializable
{
    boolean sendEmailSummary;
    SmsAccount smsAccount;
    SmsMessage[] smsMessages;
    SmsStatus[] status;
    
    public SmsData() {
    }
    
    public SmsData(final SmsMessage[] smsMessages, final SmsAccount smsAccount, final SmsStatus[] status, final boolean sendEmailSummary) {
        this.smsMessages = smsMessages;
        this.smsAccount = smsAccount;
        this.status = status;
        this.sendEmailSummary = sendEmailSummary;
    }
    
    public SmsAccount getSmsAccount() {
        return this.smsAccount;
    }
    
    public SmsMessage[] getSmsMessages() {
        return this.smsMessages;
    }
    
    public SmsStatus[] getStatus() {
        return this.status;
    }
    
    public boolean isSendEmailSummary() {
        return this.sendEmailSummary;
    }
    
    public void setSendEmailSummary(final boolean sendEmailSummary) {
        this.sendEmailSummary = sendEmailSummary;
    }
    
    public void setSmsAccount(final SmsAccount smsAccount) {
        this.smsAccount = smsAccount;
    }
    
    public void setSmsMessages(final SmsMessage[] smsMessages) {
        this.smsMessages = smsMessages;
    }
    
    public void setStatus(final SmsStatus[] status) {
        this.status = status;
    }
}

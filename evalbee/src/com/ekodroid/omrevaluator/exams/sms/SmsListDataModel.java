// 
// Decompiled by Procyon v0.6.0
// 

package com.ekodroid.omrevaluator.exams.sms;

import java.io.Serializable;

public class SmsListDataModel implements Serializable
{
    boolean isSmsSent;
    String message;
    String phoneNo;
    int rollNo;
    
    public SmsListDataModel(final String phoneNo, final int rollNo, final String message, final boolean isSmsSent) {
        this.phoneNo = phoneNo;
        this.message = message;
        this.rollNo = rollNo;
        this.isSmsSent = isSmsSent;
    }
    
    public String getMessage() {
        return this.message;
    }
    
    public String getPhoneNo() {
        return this.phoneNo;
    }
    
    public int getRollNo() {
        return this.rollNo;
    }
    
    public boolean isSmsSent() {
        return this.isSmsSent;
    }
}

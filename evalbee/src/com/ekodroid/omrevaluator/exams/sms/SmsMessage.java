// 
// Decompiled by Procyon v0.6.0
// 

package com.ekodroid.omrevaluator.exams.sms;

import java.io.Serializable;

public class SmsMessage implements Serializable
{
    boolean isSmsSent;
    String message;
    String phoneNo;
    int rollNo;
    int templateId;
    boolean unicode;
    
    public SmsMessage(final SmsListDataModel smsListDataModel, final int templateId) {
        this.templateId = templateId;
        this.phoneNo = smsListDataModel.getPhoneNo();
        this.message = smsListDataModel.getMessage();
        this.rollNo = smsListDataModel.getRollNo();
        this.isSmsSent = smsListDataModel.isSmsSent();
        this.unicode = (isEnglish(this.message) ^ true);
    }
    
    private static boolean isEnglish(final String s) {
        if (s == null) {
            return false;
        }
        final char[] charArray = s.toCharArray();
        for (int length = charArray.length, i = 0; i < length; ++i) {
            if (charArray[i] > '\u007f') {
                return false;
            }
        }
        return true;
    }
    
    public int getCredits() {
        int n;
        if (this.unicode) {
            n = this.message.length() / 70;
        }
        else {
            n = this.message.length() / 160;
        }
        return n + 1;
    }
    
    public String getMessage() {
        return this.message;
    }
    
    public String getPhoneNo() {
        return this.phoneNo;
    }
    
    public int getRollNo() {
        return this.rollNo;
    }
    
    public boolean isSmsSent() {
        return this.isSmsSent;
    }
    
    public boolean isUnicode() {
        return this.unicode;
    }
    
    public void setMessage(final String message) {
        this.message = message;
    }
    
    public void setPhoneNo(final String phoneNo) {
        this.phoneNo = phoneNo;
    }
    
    public void setUnicode(final boolean unicode) {
        this.unicode = unicode;
    }
}

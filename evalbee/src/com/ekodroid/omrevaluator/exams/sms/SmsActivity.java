// 
// Decompiled by Procyon v0.6.0
// 

package com.ekodroid.omrevaluator.exams.sms;

import android.app.AlertDialog;
import android.app.Dialog;
import android.provider.Settings$Secure;
import android.content.SharedPreferences;
import android.content.DialogInterface;
import android.content.DialogInterface$OnClickListener;
import androidx.appcompat.widget.Toolbar;
import android.widget.AdapterView$OnItemClickListener;
import com.ekodroid.omrevaluator.more.BuySmsCreditActivity;
import android.view.View$OnClickListener;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView$OnItemSelectedListener;
import android.widget.SpinnerAdapter;
import com.ekodroid.omrevaluator.database.SmsTemplateDataModel;
import com.ekodroid.omrevaluator.database.repositories.SmsTemplateRepository;
import android.widget.ListAdapter;
import android.content.Intent;
import com.ekodroid.omrevaluator.templateui.models.Subject2;
import com.google.firebase.auth.FirebaseAuth;
import com.ekodroid.omrevaluator.templateui.scanner.ResultItem;
import com.ekodroid.omrevaluator.templateui.models.SheetTemplate2;
import com.ekodroid.omrevaluator.database.ResultDataJsonModel;
import com.ekodroid.omrevaluator.database.repositories.ResultRepository;
import com.ekodroid.omrevaluator.database.repositories.TemplateRepository;
import android.os.Bundle;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.ekodroid.omrevaluator.serializable.ResultData;
import com.ekodroid.omrevaluator.exams.models.MessageResultExcel;
import android.content.pm.PackageManager$NameNotFoundException;
import com.ekodroid.omrevaluator.database.StudentDataModel;
import java.util.Iterator;
import com.ekodroid.omrevaluator.database.repositories.ClassRepository;
import android.content.Context;
import android.app.ProgressDialog;
import com.ekodroid.omrevaluator.templateui.models.ExamId;
import com.ekodroid.omrevaluator.serializable.SmsAccount;
import android.widget.LinearLayout;
import java.util.ArrayList;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.ImageButton;
import android.widget.Button;
import android.widget.Spinner;

public class SmsActivity extends v5
{
    public SmsActivity c;
    public Spinner d;
    public Button e;
    public Button f;
    public ImageButton g;
    public TextView h;
    public ImageButton i;
    public ProgressBar j;
    public ListView k;
    public ArrayList l;
    public ArrayList m;
    public ArrayList n;
    public LinearLayout p;
    public int q;
    public SmsAccount t;
    public ExamId v;
    public ProgressDialog w;
    public String x;
    
    public SmsActivity() {
        this.c = this;
        this.q = 0;
        this.x = "";
    }
    
    public static /* synthetic */ ProgressDialog C(final SmsActivity smsActivity) {
        return smsActivity.w;
    }
    
    public static /* synthetic */ ExamId F(final SmsActivity smsActivity) {
        return smsActivity.v;
    }
    
    public final void J(final Context context, final ArrayList list, final String s) {
        for (final ye1 ye1 : list) {
            final StudentDataModel student = ClassRepository.getInstance(context).getStudent(ye1.g(), s);
            String studentName;
            if (student != null) {
                studentName = student.getStudentName();
            }
            else {
                studentName = " - - - ";
            }
            ye1.m(studentName);
        }
    }
    
    public final String[][] K(final ArrayList list) {
        final int n = list.size() + 1;
        final String[][] array = new String[n][];
        array[0] = this.L();
        for (int i = 1; i < n; ++i) {
            final SmsListDataModel smsListDataModel = list.get(i - 1);
            final String[] array2 = new String[3];
            array[i] = array2;
            final StringBuilder sb = new StringBuilder();
            sb.append(smsListDataModel.getRollNo());
            sb.append("");
            array2[0] = sb.toString();
            array[i][1] = smsListDataModel.getPhoneNo();
            array[i][2] = smsListDataModel.getMessage();
        }
        return array;
    }
    
    public final String[] L() {
        return new String[] { "Roll No", "Phone No", "Message" };
    }
    
    public final void M(final String s, final String s2) {
        this.e0(null);
        final String[][] k = this.K(this.n);
        final StringBuilder sb = new StringBuilder();
        sb.append(this.v.getExamName().replaceAll("[^a-zA-Z0-9_-]", "_"));
        sb.append("_");
        sb.append(this.v.getExamDate().replaceAll("[^a-zA-Z0-9_]", "_"));
        sb.append("_Result");
        final String string = sb.toString();
        int versionCode;
        try {
            versionCode = ((Context)this.c).getPackageManager().getPackageInfo(((Context)this).getPackageName(), 0).versionCode;
        }
        catch (final PackageManager$NameNotFoundException ex) {
            ((Throwable)ex).printStackTrace();
            versionCode = 0;
        }
        final MessageResultExcel messageResultExcel = new MessageResultExcel(s, 0, k, string, versionCode);
        this.e0(new f61(((Context)this).getApplicationContext(), new ResultData(messageResultExcel.emailId, messageResultExcel.data, messageResultExcel.subjectName, messageResultExcel.appVersion, s2), new n52((Context)this, a91.w()).b(), this.x, new zg(this, string, s, s2) {
            public final String a;
            public final String b;
            public final String c;
            public final SmsActivity d;
            
            @Override
            public void a(final boolean b, final int n, final Object o) {
                FirebaseAnalytics firebaseAnalytics;
                String s2;
                if (o != null && n == 200) {
                    ((Dialog)SmsActivity.C(this.d)).dismiss();
                    final SmsActivity c = this.d.c;
                    final String s = (String)o;
                    final StringBuilder sb = new StringBuilder();
                    sb.append(this.a);
                    sb.append(".xls");
                    new hn1((Context)c, s, sb.toString());
                    firebaseAnalytics = FirebaseAnalytics.getInstance((Context)this.d.c);
                    s2 = "ExcelSMSDownloadSuccess";
                }
                else {
                    ((Dialog)SmsActivity.C(this.d)).dismiss();
                    this.d.d0(this.b, this.c);
                    firebaseAnalytics = FirebaseAnalytics.getInstance((Context)this.d.c);
                    s2 = "ExcelSMSDownloadError";
                }
                firebaseAnalytics.a(s2, null);
            }
        }));
    }
    
    public final ArrayList N(final ExamId examId) {
        final SheetTemplate2 sheetTemplate = TemplateRepository.getInstance((Context)this.c).getTemplateJson(examId).getSheetTemplate();
        final ArrayList<ResultDataJsonModel> allResultJson = ResultRepository.getInstance((Context)this.c).getAllResultJson(examId);
        final ArrayList list = new ArrayList();
        for (final ResultDataJsonModel resultDataJsonModel : allResultJson) {
            final ResultItem resultItem = resultDataJsonModel.getResultItem(sheetTemplate);
            final int p = ve1.p(resultItem);
            final int q = ve1.q(resultItem);
            final int s = ve1.s(resultItem);
            final double totalMarks = resultItem.getTotalMarks();
            list.add(new ye1(resultDataJsonModel.getRollNo(), totalMarks, ve1.i(totalMarks, sheetTemplate.getGradeLevels()), p, q, s, resultDataJsonModel.isSmsSent(), resultDataJsonModel.isSynced(), resultDataJsonModel.isPublished()));
        }
        ve1.a(list, sheetTemplate.getRankingMethod());
        this.J((Context)this.c, list, examId.getClassName());
        return list;
    }
    
    public final ArrayList O(final ArrayList list, final String s) {
        final ArrayList list2 = new ArrayList();
        final Iterator iterator = list.iterator();
        while (iterator.hasNext()) {
            list2.add(this.P((mo1)iterator.next(), s));
        }
        return list2;
    }
    
    public final SmsListDataModel P(final mo1 mo1, String target) {
        final String f = mo1.f();
        final String f2 = ok.f;
        final StringBuilder sb = new StringBuilder();
        final String s = "";
        sb.append("");
        sb.append(mo1.h());
        final String replace = target.replace(f2, sb.toString());
        target = ok.g;
        final StringBuilder sb2 = new StringBuilder();
        sb2.append("");
        sb2.append(mo1.i());
        final String replace2 = replace.replace(target, sb2.toString());
        final String h = ok.h;
        final StringBuilder sb3 = new StringBuilder();
        sb3.append("");
        sb3.append(mo1.b().getExamName());
        target = replace2.replace(h, sb3.toString());
        final String i = ok.i;
        final StringBuilder sb4 = new StringBuilder();
        sb4.append("");
        sb4.append(mo1.b().getClassName());
        final String replace3 = target.replace(i, sb4.toString());
        final String j = ok.j;
        final StringBuilder sb5 = new StringBuilder();
        sb5.append("");
        sb5.append(mo1.b().getExamDate());
        target = replace3.replace(j, sb5.toString());
        final String k = ok.k;
        final StringBuilder sb6 = new StringBuilder();
        sb6.append("");
        sb6.append(mo1.c());
        final String replace4 = target.replace(k, sb6.toString());
        final String l = ok.l;
        final StringBuilder sb7 = new StringBuilder();
        sb7.append("");
        sb7.append(mo1.g());
        target = replace4.replace(l, sb7.toString());
        final String m = ok.m;
        final StringBuilder sb8 = new StringBuilder();
        sb8.append("");
        sb8.append(ve1.o(mo1.e() * 100.0 / mo1.m()));
        final String replace5 = target.replace(m, sb8.toString());
        target = ok.n;
        final StringBuilder sb9 = new StringBuilder();
        sb9.append("");
        sb9.append(ve1.o(mo1.e()));
        target = replace5.replace(target, sb9.toString());
        final String o = ok.o;
        final StringBuilder sb10 = new StringBuilder();
        sb10.append("");
        sb10.append(ve1.o(mo1.m()));
        target = target.replace(o, sb10.toString());
        final String p2 = ok.p;
        final StringBuilder sb11 = new StringBuilder();
        sb11.append("");
        sb11.append(mo1.a());
        final String replace6 = target.replace(p2, sb11.toString());
        target = ok.q;
        final StringBuilder sb12 = new StringBuilder();
        sb12.append("");
        sb12.append(mo1.d());
        target = replace6.replace(target, sb12.toString());
        final String r = ok.r;
        final StringBuilder sb13 = new StringBuilder();
        sb13.append("");
        sb13.append(mo1.n());
        target = target.replace(r, sb13.toString());
        final int n = 0;
        int n2 = 0;
        int n3;
        String string;
        while (true) {
            n3 = n;
            string = s;
            if (n2 >= mo1.j().length) {
                break;
            }
            final StringBuilder sb14 = new StringBuilder();
            sb14.append(ok.v);
            final int i2 = n2 + 1;
            sb14.append(i2);
            final String string2 = sb14.toString();
            final StringBuilder sb15 = new StringBuilder();
            sb15.append("");
            sb15.append(mo1.j()[n2]);
            target = target.replace(string2, sb15.toString());
            final StringBuilder sb16 = new StringBuilder();
            sb16.append(ok.s);
            sb16.append(i2);
            final String string3 = sb16.toString();
            final StringBuilder sb17 = new StringBuilder();
            sb17.append("");
            sb17.append(ve1.o(mo1.k()[n2]));
            target = target.replace(string3, sb17.toString());
            final StringBuilder sb18 = new StringBuilder();
            sb18.append(ok.w);
            sb18.append(i2);
            final String string4 = sb18.toString();
            final StringBuilder sb19 = new StringBuilder();
            sb19.append("");
            sb19.append(ve1.o(mo1.k()[n2] * 100.0 / mo1.l()[n2]));
            target = target.replace(string4, sb19.toString());
            n2 = i2;
        }
        while (n3 < mo1.j().length) {
            final StringBuilder sb20 = new StringBuilder();
            sb20.append(string);
            sb20.append(", ");
            sb20.append(mo1.j()[n3]);
            sb20.append(" ");
            final String string5 = sb20.toString();
            final StringBuilder sb21 = new StringBuilder();
            sb21.append(string5);
            sb21.append(ve1.o(mo1.k()[n3]));
            sb21.append("/");
            sb21.append(mo1.l()[n3]);
            string = sb21.toString();
            ++n3;
        }
        target = target.replace(ok.t, string).replace(ok.u, FirebaseAuth.getInstance().e().getDisplayName());
        return new SmsListDataModel(f, mo1.h(), target, mo1.o());
    }
    
    public final ArrayList Q(final ArrayList list) {
        final ArrayList list2 = new ArrayList();
        final SheetTemplate2 sheetTemplate = TemplateRepository.getInstance((Context)this.c).getTemplateJson(this.v).getSheetTemplate();
        final Subject2[] subjects = sheetTemplate.getTemplateParams().getSubjects();
        final int length = subjects.length;
        final String[] array = new String[length];
        for (int i = 0; i < length; ++i) {
            array[i] = subjects[i].getSubName();
        }
        final ResultRepository instance = ResultRepository.getInstance((Context)this.c);
        final Iterator iterator = list.iterator();
        final String[] array2 = array;
        while (iterator.hasNext()) {
            final ye1 ye1 = (ye1)iterator.next();
            final StudentDataModel student = ClassRepository.getInstance((Context)this.c).getStudent(ye1.g(), this.v.getClassName());
            final ResultItem resultItem = instance.getResult(this.v, ye1.g()).getResultItem(sheetTemplate);
            final double[] marksForEachSubject = resultItem.getMarksForEachSubject();
            final double[] g = ve1.g(ve1.r(sheetTemplate));
            final double totalMarks = resultItem.getTotalMarks();
            final double d = ve1.d(g);
            StudentDataModel studentDataModel;
            if ((studentDataModel = student) == null) {
                studentDataModel = new StudentDataModel(ye1.g(), "", "", "", this.v.getClassName());
            }
            list2.add(new mo1(ye1.g(), studentDataModel.getStudentName(), studentDataModel.getPhoneNo(), this.v, d, totalMarks, ye1.f(), ye1.b(), array2, marksForEachSubject, g, ye1.a(), ye1.c(), ye1.h(), ye1.k()));
        }
        return list2;
    }
    
    public final void R(final String s) {
        final Intent intent = new Intent((Context)this.c, (Class)SmsTemplateActivity.class);
        intent.putExtra("SMS_TEMPLATE_NAME", s);
        ((Context)this).startActivity(intent);
    }
    
    public final void S() {
        this.w = new ProgressDialog((Context)this.c, 2131951921);
        this.p = (LinearLayout)this.findViewById(2131296742);
        this.d = (Spinner)this.findViewById(2131297099);
        this.g = (ImageButton)this.findViewById(2131296662);
        this.h = (TextView)this.findViewById(2131297219);
        this.j = (ProgressBar)this.findViewById(2131296973);
        this.i = (ImageButton)this.findViewById(2131296681);
        this.k = (ListView)this.findViewById(2131296845);
        this.e = (Button)this.findViewById(2131296474);
        this.f = (Button)this.findViewById(2131296477);
    }
    
    public final void T(final ArrayList list) {
        this.k.setAdapter((ListAdapter)new o3((Context)this.c, list));
    }
    
    public final void U() {
        final ArrayList<SmsTemplateDataModel> allSmsTemplates = SmsTemplateRepository.getInstance((Context)this.c).getAllSmsTemplates();
        final ArrayList list = new ArrayList();
        list.add(ok.a);
        list.add("Default Medium");
        list.add("Default Subject Wise");
        list.add(ok.b);
        final Iterator<SmsTemplateDataModel> iterator = allSmsTemplates.iterator();
        while (iterator.hasNext()) {
            list.add(iterator.next().getSmsTemplateName());
        }
        this.d.setAdapter((SpinnerAdapter)new p3((Context)this.c, list));
        ((AdapterView)this.d).setOnItemSelectedListener((AdapterView$OnItemSelectedListener)new AdapterView$OnItemSelectedListener(this, list) {
            public final ArrayList a;
            public final SmsActivity b;
            
            public void onItemSelected(final AdapterView adapterView, final View view, final int index, final long n) {
                if (index == 0) {
                    final String f = ok.F;
                    final SmsActivity b = this.b;
                    b.n = b.O(b.m, f);
                    final SmsActivity b2 = this.b;
                    b2.T(b2.n);
                    return;
                }
                if (index == 1) {
                    final String h = ok.H;
                    final SmsActivity b3 = this.b;
                    b3.n = b3.O(b3.m, h);
                    final SmsActivity b4 = this.b;
                    b4.T(b4.n);
                    return;
                }
                if (index == 2) {
                    final String g = ok.G;
                    final SmsActivity b5 = this.b;
                    b5.n = b5.O(b5.m, g);
                    final SmsActivity b6 = this.b;
                    b6.T(b6.n);
                    return;
                }
                if (index == 3) {
                    this.b.R(ok.a);
                    return;
                }
                final SmsTemplateDataModel smsTemplate = SmsTemplateRepository.getInstance((Context)this.b.c).getSmsTemplate(this.a.get(index));
                final SmsActivity b7 = this.b;
                b7.n = b7.O(b7.m, smsTemplate.getSmsString());
                final SmsActivity b8 = this.b;
                b8.T(b8.n);
            }
            
            public void onNothingSelected(final AdapterView adapterView) {
            }
        });
        ((AdapterView)this.d).setSelection(0);
    }
    
    public final void V() {
        ((View)this.f).setOnClickListener((View$OnClickListener)new View$OnClickListener(this) {
            public final SmsActivity a;
            
            public void onClick(final View view) {
                final r30 e = FirebaseAuth.getInstance().e();
                if (e != null) {
                    this.a.M(e.getEmail(), e.getDisplayName());
                }
            }
        });
    }
    
    public final void W() {
        ((View)this.e).setOnClickListener((View$OnClickListener)new View$OnClickListener(this, new y01(this) {
            public final SmsActivity a;
            
            @Override
            public void a(final Object o) {
                if (o != null) {
                    this.a.finish();
                }
            }
        }) {
            public final y01 a;
            public final SmsActivity b;
            
            public void onClick(final View view) {
                final SmsActivity b = this.b;
                if (b.t != null) {
                    if (((AdapterView)b.d).getSelectedItemPosition() > 2) {
                        xs.c((Context)this.b.c, null, 0, 2131886483, 2131886679, 0, 0, 0);
                        return;
                    }
                    final SmsActivity b2 = this.b;
                    final SmsActivity c = b2.c;
                    final ExamId f = SmsActivity.F(b2);
                    final y01 a = this.a;
                    final SmsActivity b3 = this.b;
                    new al1((Context)c, f, a, b3.n, b3.q, ((AdapterView)b3.d).getSelectedItemPosition()).show();
                }
                else {
                    b.a0();
                }
            }
        });
    }
    
    public final void X() {
        this.findViewById(2131296419).setOnClickListener((View$OnClickListener)new View$OnClickListener(this) {
            public final SmsActivity a;
            
            public void onClick(final View view) {
                final Intent intent = new Intent((Context)this.a.c, (Class)BuySmsCreditActivity.class);
                intent.putExtra("SMS_CREDIT", this.a.q);
                ((Context)this.a).startActivity(intent);
            }
        });
    }
    
    public final void Y() {
        ((View)this.g).setOnClickListener((View$OnClickListener)new View$OnClickListener(this) {
            public final SmsActivity a;
            
            public void onClick(final View view) {
                final SmsActivity a = this.a;
                a.R((String)((AdapterView)a.d).getSelectedItem());
            }
        });
    }
    
    public final void Z() {
        ((AdapterView)this.k).setOnItemClickListener((AdapterView$OnItemClickListener)new AdapterView$OnItemClickListener(this) {
            public final SmsActivity a;
            
            public void onItemClick(final AdapterView adapterView, final View view, final int index, final long n) {
                if (view.getId() == 2131296674L) {
                    final SmsActivity a = this.a;
                    a.c0((SmsListDataModel)a.n.get(index));
                }
            }
        });
    }
    
    public final void a0() {
        final y01 y01 = new y01(this) {
            public final SmsActivity a;
            
            @Override
            public void a(final Object o) {
                if (o == null) {
                    xs.c((Context)this.a.c, null, 0, 2131886504, 2131886679, 0, 0, 0);
                    this.a.h.setText((CharSequence)"---");
                    ((View)this.a.i).setVisibility(0);
                }
                else {
                    final SmsActivity a = this.a;
                    final SmsAccount t = (SmsAccount)o;
                    a.t = t;
                    a.q = t.getCredit();
                    ((View)this.a.i).setVisibility(8);
                    final TextView h = this.a.h;
                    final StringBuilder sb = new StringBuilder();
                    sb.append("");
                    sb.append(this.a.q);
                    h.setText((CharSequence)sb.toString());
                }
                ((View)this.a.j).setVisibility(8);
            }
        };
        ((View)this.i).setVisibility(8);
        ((View)this.j).setVisibility(0);
        new va0((Context)this, new n52((Context)this, a91.u()).b(), y01);
    }
    
    public final void b0() {
        final Toolbar toolbar = (Toolbar)this.findViewById(2131297358);
        this.x(toolbar);
        toolbar.setNavigationOnClickListener((View$OnClickListener)new View$OnClickListener(this) {
            public final SmsActivity a;
            
            public void onClick(final View view) {
                this.a.finish();
            }
        });
    }
    
    public final void c0(final SmsListDataModel smsListDataModel) {
        try {
            final Intent intent = new Intent("android.intent.action.SEND");
            intent.setType("text/plain");
            final StringBuilder sb = new StringBuilder();
            sb.append(this.v.getExamName());
            sb.append(" Report");
            intent.putExtra("android.intent.extra.SUBJECT", sb.toString());
            intent.putExtra("android.intent.extra.TEXT", smsListDataModel.getMessage());
            final StringBuilder sb2 = new StringBuilder();
            sb2.append(smsListDataModel.getPhoneNo());
            sb2.append("@s.whatsapp.net");
            intent.putExtra("jid", sb2.toString());
            intent.putExtra("address", smsListDataModel.getPhoneNo());
            ((Context)this).startActivity(Intent.createChooser(intent, (CharSequence)"Share via"));
        }
        catch (final Exception ex) {}
    }
    
    public final void d0(final String s, final String s2) {
        xs.c((Context)this.c, new y01(this, s, s2) {
            public final String a;
            public final String b;
            public final SmsActivity c;
            
            @Override
            public void a(final Object o) {
                this.c.M(this.a, this.b);
            }
        }, 2131886282, 2131886494, 2131886746, 2131886163, 0, 2131230930);
    }
    
    public final void e0(final f61 f61) {
        this.w.setMessage((CharSequence)((Context)this).getString(2131886250));
        this.w.setProgressStyle(0);
        ((Dialog)this.w).setCanceledOnTouchOutside(false);
        ((AlertDialog)this.w).setButton(-2, (CharSequence)((Context)this).getString(2131886163), (DialogInterface$OnClickListener)new DialogInterface$OnClickListener(this, f61) {
            public final f61 a;
            public final SmsActivity b;
            
            public void onClick(final DialogInterface dialogInterface, final int n) {
                final f61 a = this.a;
                if (a != null) {
                    a.h();
                }
                dialogInterface.dismiss();
            }
        });
        ((Dialog)this.w).show();
    }
    
    public final void f0() {
        ((View)this.i).setOnClickListener((View$OnClickListener)new View$OnClickListener(this) {
            public final SmsActivity a;
            
            public void onClick(final View view) {
                this.a.a0();
            }
        });
    }
    
    public final void g0() {
        new q61(new n52((Context)this, a91.u()).b(), new y01(this) {
            public final SmsActivity a;
            
            @Override
            public void a(final Object o) {
                this.a.a0();
            }
        });
    }
    
    public final void h0() {
        final SharedPreferences sharedPreferences = ((Context)this).getApplicationContext().getSharedPreferences("MyPref", 0);
        if (!sharedPreferences.getString("user_country", "").toUpperCase().equals("INDIA")) {
            ((View)this.e).setVisibility(8);
            ((View)this.p).setVisibility(8);
        }
        else {
            ((View)this.e).setVisibility(0);
            ((View)this.p).setVisibility(0);
            if (sharedPreferences.getInt("SMS_LINK_EXP", 0) > a91.n()) {
                this.g0();
            }
            else {
                this.a0();
            }
        }
    }
    
    @Override
    public void onCreate(final Bundle bundle) {
        super.onCreate(bundle);
        this.setContentView(2131492925);
        this.v = (ExamId)this.getIntent().getSerializableExtra("EXAM_ID");
        this.x = Settings$Secure.getString(((Context)this).getContentResolver(), "android_id");
        final ArrayList n = this.N(this.v);
        this.l = n;
        this.m = this.Q(n);
        this.S();
        this.b0();
        this.h0();
        this.U();
        this.Y();
        this.W();
        this.f0();
        this.Z();
        this.X();
        this.V();
    }
    
    @Override
    public void onResume() {
        super.onResume();
        this.h0();
        this.U();
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.ekodroid.omrevaluator.exams.sms;

import android.media.RingtoneManager;
import android.os.Build$VERSION;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.IntentFilter;
import com.ekodroid.omrevaluator.database.repositories.ResultRepository;
import android.os.IBinder;
import android.os.Bundle;
import com.google.firebase.analytics.FirebaseAnalytics;
import java.io.InputStream;
import java.io.BufferedInputStream;
import java.io.FileInputStream;
import java.io.File;
import java.util.Iterator;
import com.ekodroid.omrevaluator.more.models.Teacher;
import com.ekodroid.omrevaluator.clients.UserProfileClients.models.UserRole;
import com.ekodroid.omrevaluator.clients.UserProfileClients.models.OrgProfile;
import com.google.firebase.auth.FirebaseAuth;
import com.ekodroid.omrevaluator.serializable.SmsAccount;
import android.content.Intent;
import android.content.Context;
import com.ekodroid.omrevaluator.templateui.models.ExamId;
import android.content.BroadcastReceiver;
import android.content.SharedPreferences$Editor;
import android.content.SharedPreferences;
import android.app.Service;

public class SmsPublishService extends Service
{
    public SmsPublishService a;
    public SmsMessage[] b;
    public SharedPreferences c;
    public SharedPreferences$Editor d;
    public int e;
    public boolean f;
    public p61 g;
    public boolean h;
    public BroadcastReceiver i;
    public boolean j;
    public y01 k;
    public ExamId l;
    
    public SmsPublishService() {
        this.a = this;
        this.e = 10;
        this.f = false;
        this.h = false;
        this.i = new BroadcastReceiver(this) {
            public final SmsPublishService a;
            
            public void onReceive(final Context context, final Intent intent) {
                SmsPublishService.a(this.a, true);
                final p61 g = this.a.g;
                if (g != null) {
                    g.d();
                }
            }
        };
        this.j = false;
    }
    
    public static /* synthetic */ boolean a(final SmsPublishService smsPublishService, final boolean h) {
        return smsPublishService.h = h;
    }
    
    public static /* synthetic */ ExamId b(final SmsPublishService smsPublishService) {
        return smsPublishService.l;
    }
    
    public final void i() {
        ((Context)this).sendBroadcast(new Intent("REPORT_SENT"));
    }
    
    public final void j(final int n, final SmsAccount smsAccount) {
        final StringBuilder sb = new StringBuilder();
        sb.append("Sent Sms : ");
        final SharedPreferences c = this.c;
        final StringBuilder sb2 = new StringBuilder();
        sb2.append(this.l.getExamName());
        sb2.append("_sentSMSCount");
        sb.append(c.getInt(sb2.toString(), 0));
        final String string = sb.toString();
        final StringBuilder sb3 = new StringBuilder();
        sb3.append("Pending Sms : ");
        final SharedPreferences c2 = this.c;
        final StringBuilder sb4 = new StringBuilder();
        sb4.append(this.l.getExamName());
        sb4.append("_sentSMSCount");
        sb3.append(n - c2.getInt(sb4.toString(), 0));
        final String[] array = { string, sb3.toString() };
        final StringBuilder sb5 = new StringBuilder();
        sb5.append(this.l.getExamName());
        sb5.append(" - Publish Complete");
        this.q(3, array, sb5.toString());
        final SmsPublishService a = this.a;
        final StringBuilder sb6 = new StringBuilder();
        sb6.append(this.l.getExamName());
        sb6.append(" - Publish Complete, ");
        sb6.append(array[0]);
        sb6.append(", ");
        sb6.append(array[1]);
        a91.H((Context)a, sb6.toString(), 2131230927, 2131231085);
        this.a.stopSelf();
    }
    
    public final int k() {
        final SharedPreferences c = this.c;
        final StringBuilder sb = new StringBuilder();
        sb.append(this.l.getExamName());
        sb.append("_lastSMSIndexSent");
        return c.getInt(sb.toString(), -1) + 1;
    }
    
    public final SmsData l(final int n, final SmsMessage[] array, int n2, final boolean b) {
        final int n3 = array.length - n;
        if (n3 <= n2) {
            n2 = n3;
        }
        final SmsMessage[] array2 = new SmsMessage[n2];
        for (int i = 0; i < n2; ++i) {
            array2[i] = array[n + i];
        }
        SmsAccount smsAccount;
        try {
            String s = b.i(FirebaseAuth.getInstance().e().getEmail(), "emailKey");
            final OrgProfile instance = OrgProfile.getInstance((Context)this.a);
            String s2 = s;
            if (instance != null) {
                s2 = s;
                if (instance.getRole() != UserRole.OWNER) {
                    final Iterator<Teacher> iterator = instance.getTeacherList().iterator();
                    while (true) {
                        s2 = s;
                        if (!iterator.hasNext()) {
                            break;
                        }
                        final Teacher teacher = iterator.next();
                        if (teacher.getUserRole() != UserRole.OWNER) {
                            continue;
                        }
                        s = b.i(teacher.getEmailId(), "emailKey");
                    }
                }
            }
            smsAccount = new SmsAccount(s2, 0, 0, this.c.getString("user_country", "").toUpperCase(), null);
        }
        catch (final Exception ex) {
            ex.printStackTrace();
            smsAccount = null;
        }
        return new SmsData(array2, smsAccount, null, b);
    }
    
    public final SmsMessage[] m() {
        try {
            final StringBuilder sb = new StringBuilder();
            sb.append(((Context)this).getFilesDir());
            sb.append("/tempsms.json");
            final File file = new File(sb.toString());
            final int len = (int)file.length();
            final byte[] array = new byte[len];
            final BufferedInputStream bufferedInputStream = new BufferedInputStream(new FileInputStream(file));
            bufferedInputStream.read(array, 0, len);
            bufferedInputStream.close();
            return (SmsMessage[])new gc0().j(new String(array), SmsMessage[].class);
        }
        catch (final Exception ex) {
            ex.printStackTrace();
            return null;
        }
    }
    
    public final void n(final int n) {
        final SharedPreferences c = this.c;
        final StringBuilder sb = new StringBuilder();
        sb.append(this.l.getExamName());
        sb.append("_lastSMSIndexSent");
        final int int1 = c.getInt(sb.toString(), -1);
        final SharedPreferences$Editor d = this.d;
        final StringBuilder sb2 = new StringBuilder();
        sb2.append(this.l.getExamName());
        sb2.append("_lastSMSIndexSent");
        d.putInt(sb2.toString(), int1 + n);
        this.d.commit();
    }
    
    public final void o(final int n) {
        final SharedPreferences c = this.c;
        final StringBuilder sb = new StringBuilder();
        sb.append(this.l.getExamName());
        sb.append("_sentSMSCount");
        final int int1 = c.getInt(sb.toString(), 0);
        final SharedPreferences$Editor d = this.d;
        final StringBuilder sb2 = new StringBuilder();
        sb2.append(this.l.getExamName());
        sb2.append("_sentSMSCount");
        d.putInt(sb2.toString(), int1 + n).commit();
        this.c.edit().putInt("count_sms", this.c.getInt("count_sms", 0) + n).commit();
        FirebaseAnalytics.getInstance((Context)this).a("SMS_SENT", null);
    }
    
    public IBinder onBind(final Intent intent) {
        throw new UnsupportedOperationException("Not yet implemented");
    }
    
    public void onCreate() {
        super.onCreate();
        final SharedPreferences sharedPreferences = ((Context)this).getApplicationContext().getSharedPreferences("MyPref", 0);
        this.c = sharedPreferences;
        this.d = sharedPreferences.edit();
        this.k = new y01(this) {
            public final SmsPublishService a;
            
            @Override
            public void a(final Object o) {
                if (o != null && o instanceof SmsData) {
                    final SmsData smsData = (SmsData)o;
                    final SmsStatus[] status = smsData.getStatus();
                    for (final SmsStatus smsStatus : status) {
                        if (smsStatus.isStatus()) {
                            ResultRepository.getInstance((Context)this.a.a).updateSmsSendStatus(SmsPublishService.b(this.a), smsStatus.getRollNo(), true);
                            this.a.o(1);
                        }
                    }
                    final SmsPublishService a = this.a;
                    a.p(3, a.b.length, a.k());
                    this.a.n(status.length);
                    final SmsPublishService a2 = this.a;
                    a2.r(a2.b, smsData.getSmsAccount());
                    this.a.i();
                }
                else {
                    final SmsPublishService a3 = this.a;
                    a3.p(3, a3.b.length, a3.k());
                    this.a.n(1);
                    final SmsPublishService a4 = this.a;
                    a4.r(a4.b, null);
                }
            }
        };
        sl.registerReceiver((Context)this, this.i, new IntentFilter("STOP_SERVICE_PUBLISH_SMS_RESULT"), 2);
    }
    
    public void onDestroy() {
        ((Context)this).unregisterReceiver(this.i);
        super.onDestroy();
    }
    
    public int onStartCommand(final Intent intent, final int n, final int n2) {
        super.onStartCommand(intent, n, n2);
        if (this.j) {
            return 1;
        }
        this.j = true;
        this.l = (ExamId)intent.getSerializableExtra("EXAM_ID");
        this.f = intent.getBooleanExtra("SEND_SMS_SUMMARY", false);
        if (this.l != null) {
            final SmsMessage[] m = this.m();
            if ((this.b = m) != null) {
                this.p(3, m.length, this.k());
                this.r(this.b, null);
            }
        }
        return 1;
    }
    
    public final void p(final int n, final int n2, final int i) {
        final PendingIntent broadcast = PendingIntent.getBroadcast((Context)this, 0, new Intent("STOP_SERVICE_PUBLISH_SMS_RESULT"), 67108864);
        final NotificationManager notificationManager = (NotificationManager)((Context)this).getSystemService("notification");
        if (notificationManager == null) {
            return;
        }
        if (Build$VERSION.SDK_INT >= 26) {
            fh.a(notificationManager, eh.a("channel1", "publish result", 4));
        }
        final rz0.e k = new rz0.e((Context)this.a, "channel1").k("EvalBee");
        final StringBuilder sb = new StringBuilder();
        sb.append("Sent Sms : ");
        sb.append(i);
        notificationManager.notify(n, k.j(sb.toString()).B(System.currentTimeMillis()).f(true).v(2131230924).t(n2, i, true).a(2131230906, "Cancel", broadcast).b());
    }
    
    public final void q(final int n, final String[] array, final String s) {
        final NotificationManager notificationManager = (NotificationManager)((Context)this).getSystemService("notification");
        if (Build$VERSION.SDK_INT >= 26) {
            fh.a(notificationManager, eh.a("channel1", "publish result", 4));
        }
        final rz0.f f = new rz0.f();
        for (int length = array.length, i = 0; i < length; ++i) {
            f.h(array[i]);
        }
        f.i(s);
        notificationManager.notify(n, new rz0.e((Context)this.a, "channel1").k("EvalBee").j(s).B(System.currentTimeMillis()).f(true).x(f).v(2131230924).w(RingtoneManager.getDefaultUri(2)).b());
    }
    
    public final void r(final SmsMessage[] array, final SmsAccount smsAccount) {
        final int k = this.k();
        if (k < array.length && !this.h) {
            this.g = new p61(this.l(k, array, this.e, this.f), new n52((Context)this, a91.u()).b(), this.k);
            return;
        }
        this.j(array.length, smsAccount);
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.ekodroid.omrevaluator.exams.sms;

import android.widget.TextView;
import androidx.appcompat.app.a;
import android.view.ViewGroup;
import android.os.Bundle;
import android.content.DialogInterface;
import android.content.DialogInterface$OnClickListener;
import com.google.android.material.dialog.MaterialAlertDialogBuilder;
import com.ekodroid.omrevaluator.database.repositories.SmsTemplateRepository;
import androidx.appcompat.widget.Toolbar;
import android.view.View$OnClickListener;
import android.view.ViewGroup$LayoutParams;
import android.view.View;
import android.content.Context;
import android.widget.LinearLayout$LayoutParams;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Button;

public class SmsTemplateActivity extends v5
{
    public SmsTemplateActivity c;
    public String d;
    public Button e;
    public Button f;
    public LinearLayout g;
    public EditText h;
    public String i;
    public boolean j;
    
    public SmsTemplateActivity() {
        this.c = this;
        this.j = false;
    }
    
    public final void D() {
        final LinearLayout$LayoutParams linearLayout$LayoutParams = new LinearLayout$LayoutParams(-1, -2);
        linearLayout$LayoutParams.bottomMargin = a91.d(12, (Context)this.c);
        final LinearLayout g = this.g;
        final SmsTemplateActivity c = this.c;
        final StringBuilder sb = new StringBuilder();
        sb.append(ok.f);
        sb.append(" : Roll no");
        ((ViewGroup)g).addView((View)yo.c((Context)c, sb.toString(), 100), (ViewGroup$LayoutParams)linearLayout$LayoutParams);
        final LinearLayout g2 = this.g;
        final SmsTemplateActivity c2 = this.c;
        final StringBuilder sb2 = new StringBuilder();
        sb2.append(ok.g);
        sb2.append(" : Student name");
        ((ViewGroup)g2).addView((View)yo.c((Context)c2, sb2.toString(), 100), (ViewGroup$LayoutParams)linearLayout$LayoutParams);
        final LinearLayout g3 = this.g;
        final SmsTemplateActivity c3 = this.c;
        final StringBuilder sb3 = new StringBuilder();
        sb3.append(ok.h);
        sb3.append(" : Exam name");
        ((ViewGroup)g3).addView((View)yo.c((Context)c3, sb3.toString(), 100), (ViewGroup$LayoutParams)linearLayout$LayoutParams);
        final LinearLayout g4 = this.g;
        final SmsTemplateActivity c4 = this.c;
        final StringBuilder sb4 = new StringBuilder();
        sb4.append(ok.i);
        sb4.append(" : Class name");
        ((ViewGroup)g4).addView((View)yo.c((Context)c4, sb4.toString(), 100), (ViewGroup$LayoutParams)linearLayout$LayoutParams);
        final LinearLayout g5 = this.g;
        final SmsTemplateActivity c5 = this.c;
        final StringBuilder sb5 = new StringBuilder();
        sb5.append(ok.j);
        sb5.append(" : Exam date");
        ((ViewGroup)g5).addView((View)yo.c((Context)c5, sb5.toString(), 100), (ViewGroup$LayoutParams)linearLayout$LayoutParams);
        final LinearLayout g6 = this.g;
        final SmsTemplateActivity c6 = this.c;
        final StringBuilder sb6 = new StringBuilder();
        sb6.append(ok.k);
        sb6.append(" : Grade");
        ((ViewGroup)g6).addView((View)yo.c((Context)c6, sb6.toString(), 100), (ViewGroup$LayoutParams)linearLayout$LayoutParams);
        final LinearLayout g7 = this.g;
        final SmsTemplateActivity c7 = this.c;
        final StringBuilder sb7 = new StringBuilder();
        sb7.append(ok.l);
        sb7.append(" : Rank");
        ((ViewGroup)g7).addView((View)yo.c((Context)c7, sb7.toString(), 100), (ViewGroup$LayoutParams)linearLayout$LayoutParams);
        final LinearLayout g8 = this.g;
        final SmsTemplateActivity c8 = this.c;
        final StringBuilder sb8 = new StringBuilder();
        sb8.append(ok.m);
        sb8.append(" : Percentage");
        ((ViewGroup)g8).addView((View)yo.c((Context)c8, sb8.toString(), 100), (ViewGroup$LayoutParams)linearLayout$LayoutParams);
        final LinearLayout g9 = this.g;
        final SmsTemplateActivity c9 = this.c;
        final StringBuilder sb9 = new StringBuilder();
        sb9.append(ok.n);
        sb9.append(" : Total marks");
        ((ViewGroup)g9).addView((View)yo.c((Context)c9, sb9.toString(), 100), (ViewGroup$LayoutParams)linearLayout$LayoutParams);
        final LinearLayout g10 = this.g;
        final SmsTemplateActivity c10 = this.c;
        final StringBuilder sb10 = new StringBuilder();
        sb10.append(ok.p);
        sb10.append(" : Correct answers");
        ((ViewGroup)g10).addView((View)yo.c((Context)c10, sb10.toString(), 100), (ViewGroup$LayoutParams)linearLayout$LayoutParams);
        final LinearLayout g11 = this.g;
        final SmsTemplateActivity c11 = this.c;
        final StringBuilder sb11 = new StringBuilder();
        sb11.append(ok.q);
        sb11.append(" : Incorrect answers");
        ((ViewGroup)g11).addView((View)yo.c((Context)c11, sb11.toString(), 100), (ViewGroup$LayoutParams)linearLayout$LayoutParams);
        final LinearLayout g12 = this.g;
        final SmsTemplateActivity c12 = this.c;
        final StringBuilder sb12 = new StringBuilder();
        sb12.append(ok.r);
        sb12.append(" : Unattempted questions");
        ((ViewGroup)g12).addView((View)yo.c((Context)c12, sb12.toString(), 100), (ViewGroup$LayoutParams)linearLayout$LayoutParams);
        final LinearLayout g13 = this.g;
        final SmsTemplateActivity c13 = this.c;
        final StringBuilder sb13 = new StringBuilder();
        sb13.append(ok.s);
        sb13.append("n : Marks in nth subject");
        ((ViewGroup)g13).addView((View)yo.c((Context)c13, sb13.toString(), 100), (ViewGroup$LayoutParams)linearLayout$LayoutParams);
        final LinearLayout g14 = this.g;
        final SmsTemplateActivity c14 = this.c;
        final StringBuilder sb14 = new StringBuilder();
        sb14.append(ok.w);
        sb14.append("n : Percentage in nth subject");
        ((ViewGroup)g14).addView((View)yo.c((Context)c14, sb14.toString(), 100), (ViewGroup$LayoutParams)linearLayout$LayoutParams);
    }
    
    public final void E() {
        this.h = (EditText)this.findViewById(2131296590);
        this.g = (LinearLayout)this.findViewById(2131296814);
        this.f = (Button)this.findViewById(2131296449);
        this.e = (Button)this.findViewById(2131296470);
    }
    
    public final void F() {
        ((View)this.f).setOnClickListener((View$OnClickListener)new View$OnClickListener(this) {
            public final SmsTemplateActivity a;
            
            public void onClick(final View view) {
                final SmsTemplateActivity a = this.a;
                a.I(a.d);
            }
        });
    }
    
    public final void G() {
        ((View)this.e).setOnClickListener((View$OnClickListener)new View$OnClickListener(this) {
            public final SmsTemplateActivity a;
            
            public void onClick(final View view) {
                this.a.K();
            }
        });
    }
    
    public final void H() {
        final Toolbar toolbar = (Toolbar)this.findViewById(2131297359);
        this.x(toolbar);
        toolbar.setNavigationOnClickListener((View$OnClickListener)new View$OnClickListener(this) {
            public final SmsTemplateActivity a;
            
            public void onClick(final View view) {
                final SmsTemplateActivity a = this.a;
                if (!a.i.equals(a.h.getText().toString())) {
                    this.a.j = true;
                }
                final SmsTemplateActivity a2 = this.a;
                if (a2.j) {
                    a2.J();
                }
                else {
                    a2.finish();
                }
            }
        });
    }
    
    public final void I(final String str) {
        final y01 y01 = new y01(this, str) {
            public final String a;
            public final SmsTemplateActivity b;
            
            @Override
            public void a(final Object o) {
                if (SmsTemplateRepository.getInstance((Context)this.b.c).deleteSmsTemplate(this.a)) {
                    final SmsTemplateActivity c = this.b.c;
                    final StringBuilder sb = new StringBuilder();
                    sb.append(this.a);
                    sb.append(" ");
                    sb.append(this.b.c.getResources().getString(2131886242));
                    a91.H((Context)c, sb.toString(), 2131230927, 2131231085);
                    this.b.finish();
                }
            }
        };
        final SmsTemplateActivity c = this.c;
        final StringBuilder sb = new StringBuilder();
        sb.append(((Context)this).getString(2131886455));
        sb.append(" ");
        sb.append(str);
        sb.append("?");
        xs.d((Context)c, y01, 2131886129, sb.toString(), 2131886903, 2131886657, 0, 2131230945);
    }
    
    public final void J() {
        final MaterialAlertDialogBuilder materialAlertDialogBuilder = new MaterialAlertDialogBuilder((Context)this.c, 2131951953);
        materialAlertDialogBuilder.setMessage(2131886547).setPositiveButton(2131886903, (DialogInterface$OnClickListener)new DialogInterface$OnClickListener(this) {
            public final SmsTemplateActivity a;
            
            public void onClick(final DialogInterface dialogInterface, final int n) {
                this.a.K();
                dialogInterface.dismiss();
            }
        }).setNegativeButton(2131886657, (DialogInterface$OnClickListener)new DialogInterface$OnClickListener(this) {
            public final SmsTemplateActivity a;
            
            public void onClick(final DialogInterface dialogInterface, final int n) {
                dialogInterface.dismiss();
                this.a.c.finish();
            }
        }).create();
        ((a.a)materialAlertDialogBuilder).show();
    }
    
    public final void K() {
        final y01 y01 = new y01(this) {
            public final SmsTemplateActivity a;
            
            @Override
            public void a(final Object o) {
                this.a.finish();
            }
        };
        if (this.h.getText().toString().trim().equals("")) {
            a91.G((Context)this.c, 2131886277, 2131230909, 2131231086);
            return;
        }
        new vi1((Context)this.c, y01, this.d, this.h.getText().toString());
    }
    
    @Override
    public void onBackPressed() {
        if (!this.i.equals(this.h.getText().toString())) {
            this.j = true;
        }
        if (this.j) {
            this.J();
        }
        else {
            super.onBackPressed();
        }
    }
    
    @Override
    public void onCreate(final Bundle bundle) {
        super.onCreate(bundle);
        this.setContentView(2131492926);
        this.E();
        this.H();
        final String stringExtra = this.getIntent().getStringExtra("SMS_TEMPLATE_NAME");
        this.d = stringExtra;
        Label_0169: {
            String i;
            if (stringExtra != null && stringExtra.equals(ok.a)) {
                ((View)this.f).setVisibility(8);
                i = ok.F;
            }
            else {
                final String d = this.d;
                if (d != null && d.equals("Default Medium")) {
                    ((View)this.f).setVisibility(8);
                    i = ok.H;
                }
                else {
                    final String d2 = this.d;
                    if (d2 == null || !d2.equals("Default Subject Wise")) {
                        this.i = SmsTemplateRepository.getInstance((Context)this.c).getSmsTemplate(this.d).getSmsString();
                        break Label_0169;
                    }
                    ((View)this.f).setVisibility(8);
                    i = ok.G;
                }
            }
            this.i = i;
            this.d = "";
        }
        this.setTitle((CharSequence)this.d);
        ((TextView)this.h).setText((CharSequence)this.i);
        this.h.setSelection(this.i.length());
        this.D();
        this.F();
        this.G();
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.ekodroid.omrevaluator.exams.sms;

import java.io.Serializable;

public class SmsStatus implements Serializable
{
    int rollNo;
    boolean status;
    
    public SmsStatus() {
    }
    
    public SmsStatus(final int rollNo, final boolean status) {
        this.rollNo = rollNo;
        this.status = status;
    }
    
    public int getRollNo() {
        return this.rollNo;
    }
    
    public boolean isStatus() {
        return this.status;
    }
    
    public void setRollNo(final int rollNo) {
        this.rollNo = rollNo;
    }
    
    public void setStatus(final boolean status) {
        this.status = status;
    }
}

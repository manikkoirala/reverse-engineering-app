// 
// Decompiled by Procyon v0.6.0
// 

package com.ekodroid.omrevaluator.serializable;

import java.io.Serializable;

public class UserAccount implements Serializable
{
    private String androidVersion;
    private String appVersion;
    private String city;
    private String country;
    private String deviceId;
    private int emailSent;
    private String fcmId;
    private String id;
    private int imageScan;
    private int imageScanError;
    private String name;
    private String organization;
    private int scanCancel;
    private int scanSave;
    private int smsSent;
    private int syncMinutes;
    
    public UserAccount(final String id, final int scanSave, final int scanCancel, final int emailSent, final int smsSent, final int syncMinutes, final int imageScan, final int imageScanError, final String country, final String name, final String androidVersion, final String appVersion, final String fcmId, final String deviceId, final String city, final String organization) {
        this.id = id;
        this.scanSave = scanSave;
        this.scanCancel = scanCancel;
        this.emailSent = emailSent;
        this.smsSent = smsSent;
        this.syncMinutes = syncMinutes;
        this.imageScan = imageScan;
        this.imageScanError = imageScanError;
        this.country = country;
        this.name = name;
        this.androidVersion = androidVersion;
        this.appVersion = appVersion;
        this.fcmId = fcmId;
        this.deviceId = deviceId;
        this.city = city;
        this.organization = organization;
    }
    
    public String getAndroidVersion() {
        return this.androidVersion;
    }
    
    public String getAppVersion() {
        return this.appVersion;
    }
    
    public String getCity() {
        return this.city;
    }
    
    public String getCountry() {
        return this.country;
    }
    
    public String getDeviceId() {
        return this.deviceId;
    }
    
    public int getEmailSent() {
        return this.emailSent;
    }
    
    public String getFcmId() {
        return this.fcmId;
    }
    
    public String getId() {
        return this.id;
    }
    
    public int getImageScan() {
        return this.imageScan;
    }
    
    public int getImageScanError() {
        return this.imageScanError;
    }
    
    public String getName() {
        return this.name;
    }
    
    public String getOrganization() {
        return this.organization;
    }
    
    public int getScanCancel() {
        return this.scanCancel;
    }
    
    public int getScanSave() {
        return this.scanSave;
    }
    
    public int getSmsSent() {
        return this.smsSent;
    }
    
    public int getSyncMinutes() {
        return this.syncMinutes;
    }
}

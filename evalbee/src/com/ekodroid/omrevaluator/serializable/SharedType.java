// 
// Decompiled by Procyon v0.6.0
// 

package com.ekodroid.omrevaluator.serializable;

public enum SharedType
{
    private static final SharedType[] $VALUES;
    
    PRIVATE, 
    PUBLIC, 
    SHARED;
    
    private static /* synthetic */ SharedType[] $values() {
        return new SharedType[] { SharedType.PRIVATE, SharedType.PUBLIC, SharedType.SHARED };
    }
    
    static {
        $VALUES = $values();
    }
}

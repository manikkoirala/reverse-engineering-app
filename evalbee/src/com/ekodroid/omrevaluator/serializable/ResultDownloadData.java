// 
// Decompiled by Procyon v0.6.0
// 

package com.ekodroid.omrevaluator.serializable;

import java.io.Serializable;

public class ResultDownloadData implements Serializable
{
    String data;
    String id;
    String idData;
    
    public ResultDownloadData() {
    }
    
    public ResultDownloadData(final String data, final String idData, final String id) {
        this.data = data;
        this.idData = idData;
        this.id = id;
    }
    
    public String getData() {
        return this.data;
    }
    
    public String getId() {
        return this.id;
    }
    
    public String getIdData() {
        return this.idData;
    }
}

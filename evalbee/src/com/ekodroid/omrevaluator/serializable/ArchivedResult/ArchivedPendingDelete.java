// 
// Decompiled by Procyon v0.6.0
// 

package com.ekodroid.omrevaluator.serializable.ArchivedResult;

import android.content.SharedPreferences;
import android.content.Context;
import java.util.ArrayList;
import java.io.Serializable;

public class ArchivedPendingDelete implements Serializable
{
    ArrayList<String> examList;
    
    public ArchivedPendingDelete() {
        this.examList = new ArrayList<String>();
        this.examList = new ArrayList<String>();
    }
    
    public static void addArchiveId(Context sharedPreferences, final String s) {
        sharedPreferences = (Context)sharedPreferences.getSharedPreferences("MyPref", 0);
        final String string = ((SharedPreferences)sharedPreferences).getString("PENDING_DELETE", "");
        while (true) {
            try {
                final ArchivedPendingDelete archivedPendingDelete = (ArchivedPendingDelete)new gc0().j(string, ArchivedPendingDelete.class);
                if (archivedPendingDelete != null) {
                    final ArrayList<String> examList = archivedPendingDelete.examList;
                    if (examList != null) {
                        examList.add(s);
                        ((SharedPreferences)sharedPreferences).edit().putString("PENDING_DELETE", new gc0().s(archivedPendingDelete)).commit();
                        return;
                    }
                }
                final ArchivedPendingDelete archivedPendingDelete2 = new ArchivedPendingDelete();
                archivedPendingDelete2.examList.add(s);
                ((SharedPreferences)sharedPreferences).edit().putString("PENDING_DELETE", new gc0().s(archivedPendingDelete2)).commit();
            }
            catch (final Exception ex) {
                continue;
            }
            break;
        }
    }
    
    public static void deletArchiveId(final Context context, final String o) {
        final SharedPreferences sharedPreferences = context.getSharedPreferences("MyPref", 0);
        final String string = sharedPreferences.getString("PENDING_DELETE", "");
        try {
            final ArchivedPendingDelete archivedPendingDelete = (ArchivedPendingDelete)new gc0().j(string, ArchivedPendingDelete.class);
            if (archivedPendingDelete != null) {
                final ArrayList<String> examList = archivedPendingDelete.examList;
                if (examList != null) {
                    examList.remove(o);
                    sharedPreferences.edit().putString("PENDING_DELETE", new gc0().s(archivedPendingDelete)).commit();
                }
            }
        }
        catch (final Exception ex) {}
    }
    
    public static ArrayList<String> getPendingArchiveIds(final Context context) {
        final String string = context.getSharedPreferences("MyPref", 0).getString("PENDING_DELETE", "");
        try {
            final ArchivedPendingDelete archivedPendingDelete = (ArchivedPendingDelete)new gc0().j(string, ArchivedPendingDelete.class);
            if (archivedPendingDelete != null) {
                return archivedPendingDelete.examList;
            }
            return new ArrayList<String>();
        }
        catch (final Exception ex) {
            return new ArrayList<String>();
        }
    }
}

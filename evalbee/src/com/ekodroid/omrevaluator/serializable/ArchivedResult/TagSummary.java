// 
// Decompiled by Procyon v0.6.0
// 

package com.ekodroid.omrevaluator.serializable.ArchivedResult;

import java.io.Serializable;

public class TagSummary implements Serializable
{
    int correct;
    int incorrect;
    double marksObtain;
    int numOfQue;
    int percentile;
    int rank;
    String tag;
    double totalMarks;
    int unAttempted;
}

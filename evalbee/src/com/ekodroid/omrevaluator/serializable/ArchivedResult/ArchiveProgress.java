// 
// Decompiled by Procyon v0.6.0
// 

package com.ekodroid.omrevaluator.serializable.ArchivedResult;

import java.io.Serializable;

public class ArchiveProgress implements Serializable
{
    private boolean expExm;
    private boolean expRes;
    private int progress;
    private boolean uploadExm;
    private boolean uploadMeta;
    
    public int getProgress() {
        return this.progress;
    }
    
    public boolean isExpExm() {
        return this.expExm;
    }
    
    public boolean isExpRes() {
        return this.expRes;
    }
    
    public boolean isUploadExm() {
        return this.uploadExm;
    }
    
    public boolean isUploadMeta() {
        return this.uploadMeta;
    }
    
    public void setExpExm(final boolean expExm) {
        this.expExm = expExm;
    }
    
    public void setExpRes(final boolean expRes) {
        this.expRes = expRes;
    }
    
    public void setProgress(final int progress) {
        this.progress = progress;
    }
    
    public void setUploadExm(final boolean uploadExm) {
        this.uploadExm = uploadExm;
    }
    
    public void setUploadMeta(final boolean uploadMeta) {
        this.uploadMeta = uploadMeta;
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.ekodroid.omrevaluator.serializable.ArchivedResult;

import java.util.ArrayList;
import java.io.Serializable;

public class ArchivedResultItem implements Serializable
{
    private int examSet;
    String grade;
    int rollNo;
    private int[] sectionsInSubjects;
    private ArrayList<TagSummary> tagSummaries;
}

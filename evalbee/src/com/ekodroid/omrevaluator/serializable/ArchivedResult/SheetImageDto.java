// 
// Decompiled by Procyon v0.6.0
// 

package com.ekodroid.omrevaluator.serializable.ArchivedResult;

import java.io.Serializable;

public class SheetImageDto implements Serializable
{
    public long examId;
    public int pageIndex;
    public int rollNumber;
    public String sheetDimensionJson;
    public long uploadId;
    
    public SheetImageDto(final long examId, final int rollNumber, final int pageIndex, final long uploadId, final String sheetDimensionJson) {
        this.examId = examId;
        this.rollNumber = rollNumber;
        this.pageIndex = pageIndex;
        this.uploadId = uploadId;
        this.sheetDimensionJson = sheetDimensionJson;
    }
    
    public long getExamId() {
        return this.examId;
    }
    
    public int getPageIndex() {
        return this.pageIndex;
    }
    
    public int getRollNumber() {
        return this.rollNumber;
    }
    
    public String getSheetDimensionJson() {
        return this.sheetDimensionJson;
    }
    
    public long getUploadId() {
        return this.uploadId;
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.ekodroid.omrevaluator.serializable.ArchivedResult;

import com.ekodroid.omrevaluator.templateui.models.SheetTemplate2;
import java.util.ArrayList;
import com.ekodroid.omrevaluator.templateui.models.ExamId;
import java.io.Serializable;

public class ArchivedExam implements Serializable
{
    private int archivedVersion;
    private int attendees;
    private ExamId examId;
    private ArrayList<ArchivedResultItem> results;
    private SheetTemplate2 sheetTemplate2;
    
    public ArchivedExam(final ExamId examId, final SheetTemplate2 sheetTemplate2, final ArrayList<ArchivedResultItem> results) {
        this.archivedVersion = 1;
        this.examId = examId;
        this.attendees = results.size();
        this.sheetTemplate2 = sheetTemplate2;
        this.results = results;
    }
}

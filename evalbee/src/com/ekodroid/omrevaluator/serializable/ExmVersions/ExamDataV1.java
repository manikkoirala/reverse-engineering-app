// 
// Decompiled by Procyon v0.6.0
// 

package com.ekodroid.omrevaluator.serializable.ExmVersions;

import com.ekodroid.omrevaluator.templateui.models.SheetTemplate2;
import java.util.ArrayList;
import com.ekodroid.omrevaluator.templateui.models.ExamId;
import java.io.Serializable;

public class ExamDataV1 implements Serializable
{
    private ExamId examId;
    private ArrayList<ResultDataV1> results;
    private SheetTemplate2 sheetTemplate2;
    
    public ExamDataV1(final SheetTemplate2 sheetTemplate2, final ArrayList<ResultDataV1> results, final ExamId examId) {
        this.sheetTemplate2 = sheetTemplate2;
        this.results = results;
        this.examId = examId;
    }
    
    public ExamId getExamId() {
        return this.examId;
    }
    
    public ArrayList<ResultDataV1> getResults() {
        return this.results;
    }
    
    public SheetTemplate2 getSheetTemplate2() {
        return this.sheetTemplate2;
    }
    
    public void setSheetTemplate2(final SheetTemplate2 sheetTemplate2) {
        this.sheetTemplate2 = sheetTemplate2;
    }
}

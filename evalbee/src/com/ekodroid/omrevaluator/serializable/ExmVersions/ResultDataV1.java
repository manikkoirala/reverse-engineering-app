// 
// Decompiled by Procyon v0.6.0
// 

package com.ekodroid.omrevaluator.serializable.ExmVersions;

import com.ekodroid.omrevaluator.templateui.scanner.ResultItem;
import java.io.Serializable;

public class ResultDataV1 implements Serializable
{
    private ResultItem resultItem2;
    private int rollno;
    
    public ResultDataV1(final ResultItem resultItem2, final int rollno) {
        this.resultItem2 = resultItem2;
        this.rollno = rollno;
    }
    
    public ResultItem getResultItem2() {
        return this.resultItem2;
    }
    
    public int getRollno() {
        return this.rollno;
    }
}

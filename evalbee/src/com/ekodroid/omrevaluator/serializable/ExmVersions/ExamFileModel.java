// 
// Decompiled by Procyon v0.6.0
// 

package com.ekodroid.omrevaluator.serializable.ExmVersions;

import java.io.Serializable;

public class ExamFileModel implements Serializable
{
    String jsonData;
    String key;
    int version;
    
    public ExamFileModel(final int version, final String key, final String jsonData) {
        this.version = version;
        this.key = key;
        this.jsonData = jsonData;
    }
    
    public String getJsonData() {
        return this.jsonData;
    }
    
    public String getKey() {
        return this.key;
    }
    
    public int getVersion() {
        return this.version;
    }
}

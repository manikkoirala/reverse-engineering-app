// 
// Decompiled by Procyon v0.6.0
// 

package com.ekodroid.omrevaluator.serializable;

import java.io.Serializable;

public class AppConfig implements Serializable
{
    boolean blockLowerVersion;
    int blockedVersion;
    int latestVersion;
    int syncMinutes;
    String syncdate;
    
    public AppConfig(final String syncdate, final int latestVersion, final boolean blockLowerVersion, final int syncMinutes, final int blockedVersion) {
        this.syncdate = syncdate;
        this.latestVersion = latestVersion;
        this.blockLowerVersion = blockLowerVersion;
        this.syncMinutes = syncMinutes;
        this.blockedVersion = blockedVersion;
    }
    
    public int getBlockedVersion() {
        return this.blockedVersion;
    }
    
    public int getLatestVersion() {
        return this.latestVersion;
    }
    
    public String getSyncdate() {
        return this.syncdate;
    }
    
    public boolean isBlockLowerVersion() {
        return this.blockLowerVersion;
    }
}

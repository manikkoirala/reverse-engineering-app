// 
// Decompiled by Procyon v0.6.0
// 

package com.ekodroid.omrevaluator.serializable;

import java.io.Serializable;

public class ReportData implements Serializable
{
    int appVersion;
    String ccMail;
    String mailSubject;
    String studentMail;
    String studentName;
    
    public ReportData(final String studentMail, final String ccMail, final String mailSubject, final int appVersion, final String studentName) {
        this.studentMail = studentMail;
        this.ccMail = ccMail;
        this.mailSubject = mailSubject;
        this.appVersion = appVersion;
        this.studentName = studentName;
    }
    
    public int getAppVersion() {
        return this.appVersion;
    }
    
    public String getCcMail() {
        return this.ccMail;
    }
    
    public String getMailSubject() {
        return this.mailSubject;
    }
    
    public String getStudentMail() {
        return this.studentMail;
    }
    
    public String getStudentName() {
        return this.studentName;
    }
}

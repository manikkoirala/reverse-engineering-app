// 
// Decompiled by Procyon v0.6.0
// 

package com.ekodroid.omrevaluator.serializable;

import java.io.Serializable;

public class ResultData implements Serializable
{
    private int appVersion;
    private String[][] data;
    private String emailId;
    private String subjectName;
    private String userName;
    
    public ResultData() {
    }
    
    public ResultData(final String emailId, final String[][] data, final String subjectName, final int appVersion, final String userName) {
        this.emailId = emailId;
        this.data = data;
        this.subjectName = subjectName;
        this.appVersion = appVersion;
        this.userName = userName;
    }
    
    public int getAppVersion() {
        return this.appVersion;
    }
    
    public String[][] getData() {
        return this.data;
    }
    
    public String getEmailId() {
        return this.emailId;
    }
    
    public String getSubjectName() {
        return this.subjectName;
    }
    
    public String getUserName() {
        return this.userName;
    }
}

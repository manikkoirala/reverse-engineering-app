// 
// Decompiled by Procyon v0.6.0
// 

package com.ekodroid.omrevaluator.serializable.ResponseModel;

import java.io.Serializable;

public class SMSBuyLink implements Serializable
{
    private int appVersion;
    private String emailId;
    private int numberOfSms;
    private String shortUrl;
    
    public SMSBuyLink(final String emailId, final int numberOfSms, final String shortUrl, final int appVersion) {
        this.emailId = emailId;
        this.numberOfSms = numberOfSms;
        this.shortUrl = shortUrl;
        this.appVersion = appVersion;
    }
    
    public String getEmailId() {
        return this.emailId;
    }
    
    public int getNumberOfSms() {
        return this.numberOfSms;
    }
    
    public String getShortUrl() {
        return this.shortUrl;
    }
    
    public void setEmailId(final String emailId) {
        this.emailId = emailId;
    }
    
    public void setNumberOfSms(final int numberOfSms) {
        this.numberOfSms = numberOfSms;
    }
    
    public void setShortUrl(final String shortUrl) {
        this.shortUrl = shortUrl;
    }
}

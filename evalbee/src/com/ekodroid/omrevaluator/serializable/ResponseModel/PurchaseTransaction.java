// 
// Decompiled by Procyon v0.6.0
// 

package com.ekodroid.omrevaluator.serializable.ResponseModel;

import java.io.Serializable;

public class PurchaseTransaction implements Serializable
{
    private String id;
    private boolean isRenew;
    private String orderId;
    private String productId;
    private String purchaseToken;
    private String uid;
    private long updateTime;
    private String vendor;
    
    public PurchaseTransaction() {
    }
    
    public PurchaseTransaction(final String orderId, final String id, final String uid, final String productId, final String purchaseToken, final String vendor, final long updateTime, final boolean isRenew) {
        this.orderId = orderId;
        this.id = id;
        this.uid = uid;
        this.productId = productId;
        this.purchaseToken = purchaseToken;
        this.vendor = vendor;
        this.updateTime = updateTime;
        this.isRenew = isRenew;
    }
    
    public String getId() {
        return this.id;
    }
    
    public String getOrderId() {
        return this.orderId;
    }
    
    public String getProductId() {
        return this.productId;
    }
    
    public String getPurchaseToken() {
        return this.purchaseToken;
    }
    
    public String getUid() {
        return this.uid;
    }
    
    public long getUpdateTime() {
        return this.updateTime;
    }
    
    public String getVendor() {
        return this.vendor;
    }
    
    public boolean isRenew() {
        return this.isRenew;
    }
    
    public void setId(final String id) {
        this.id = id;
    }
    
    public void setOrderId(final String orderId) {
        this.orderId = orderId;
    }
    
    public void setProductId(final String productId) {
        this.productId = productId;
    }
    
    public void setPurchaseToken(final String purchaseToken) {
        this.purchaseToken = purchaseToken;
    }
    
    public void setRenew(final boolean isRenew) {
        this.isRenew = isRenew;
    }
    
    public void setUid(final String uid) {
        this.uid = uid;
    }
    
    public void setUpdateTime(final long updateTime) {
        this.updateTime = updateTime;
    }
    
    public void setVendor(final String vendor) {
        this.vendor = vendor;
    }
}

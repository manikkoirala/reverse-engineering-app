// 
// Decompiled by Procyon v0.6.0
// 

package com.ekodroid.omrevaluator.serializable.ResponseModel;

import java.util.Date;
import java.text.SimpleDateFormat;
import java.io.Serializable;

public class PurchaseAccount implements Serializable
{
    private String accountType;
    private String id;
    private int storageExpMinutes;
    private int teachersCount;
    private String uid;
    
    public PurchaseAccount() {
    }
    
    public PurchaseAccount(final String id, final String uid, final int storageExpMinutes, final int teachersCount, final String accountType) {
        this.id = id;
        this.uid = uid;
        this.storageExpMinutes = storageExpMinutes;
        this.teachersCount = teachersCount;
        this.accountType = accountType;
    }
    
    public String getAccountType() {
        return this.accountType;
    }
    
    public String getExpiryDate() {
        return new SimpleDateFormat("dd-MM-yyyy").format(new Date(this.storageExpMinutes * 60000L));
    }
    
    public String getId() {
        return this.id;
    }
    
    public int getStorageExpMinutes() {
        return this.storageExpMinutes;
    }
    
    public int getTeachersCount() {
        return this.teachersCount;
    }
    
    public String getUid() {
        return this.uid;
    }
    
    public boolean isRenewDue() {
        final int n = this.storageExpMinutes - a91.n();
        return n > 0 && n < 43200;
    }
    
    public void setAccountType(final String accountType) {
        this.accountType = accountType;
    }
    
    public void setId(final String id) {
        this.id = id;
    }
    
    public void setStorageExpMinutes(final int storageExpMinutes) {
        this.storageExpMinutes = storageExpMinutes;
    }
    
    public void setTeachersCount(final int teachersCount) {
        this.teachersCount = teachersCount;
    }
    
    public void setUid(final String uid) {
        this.uid = uid;
    }
}

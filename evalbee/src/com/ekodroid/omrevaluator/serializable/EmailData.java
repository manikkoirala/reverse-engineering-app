// 
// Decompiled by Procyon v0.6.0
// 

package com.ekodroid.omrevaluator.serializable;

import java.io.Serializable;

public class EmailData implements Serializable
{
    String country;
    String mailBody;
    String mailSubject;
    String userMailId;
    
    public EmailData() {
    }
    
    public EmailData(final String userMailId, final String mailSubject, final String mailBody, final String country) {
        this.userMailId = userMailId;
        this.mailSubject = mailSubject;
        this.mailBody = mailBody;
        this.country = country;
    }
    
    public String getCountry() {
        return this.country;
    }
    
    public String getMailBody() {
        return this.mailBody;
    }
    
    public String getMailSubject() {
        return this.mailSubject;
    }
    
    public String getUserMailId() {
        return this.userMailId;
    }
    
    public void setCountry(final String country) {
        this.country = country;
    }
    
    public void setMailBody(final String mailBody) {
        this.mailBody = mailBody;
    }
    
    public void setMailSubject(final String mailSubject) {
        this.mailSubject = mailSubject;
    }
    
    public void setUserMailId(final String userMailId) {
        this.userMailId = userMailId;
    }
}

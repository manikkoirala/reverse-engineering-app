// 
// Decompiled by Procyon v0.6.0
// 

package com.ekodroid.omrevaluator.serializable;

import java.io.Serializable;

public class SmsAccount implements Serializable
{
    private String country;
    private int credit;
    private int cumulativeCredit;
    private String id;
    private String type;
    
    public SmsAccount() {
    }
    
    public SmsAccount(final String id, final int credit, final int cumulativeCredit, final String country, final String type) {
        this.id = id;
        this.credit = credit;
        this.cumulativeCredit = cumulativeCredit;
        this.country = country;
        this.type = type;
    }
    
    public String getCountry() {
        return this.country;
    }
    
    public int getCredit() {
        return this.credit;
    }
    
    public int getCumulativeCredit() {
        return this.cumulativeCredit;
    }
    
    public String getId() {
        return this.id;
    }
    
    public String getType() {
        return this.type;
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.ekodroid.omrevaluator.serializable;

import java.io.Serializable;

public class UserProfile implements Serializable
{
    private String configData;
    private String data;
    private String id;
    private String userData;
    
    public UserProfile() {
    }
    
    public UserProfile(final String id, final String data, final String userData, final String configData) {
        this.id = id;
        this.data = data;
        this.userData = userData;
        this.configData = configData;
    }
    
    public String getConfigData() {
        return this.configData;
    }
    
    public String getData() {
        return this.data;
    }
    
    public String getId() {
        return this.id;
    }
    
    public String getUserData() {
        return this.userData;
    }
}

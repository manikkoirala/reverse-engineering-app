// 
// Decompiled by Procyon v0.6.0
// 

package com.ekodroid.omrevaluator.serializable;

import java.io.Serializable;

public class SerialData implements Serializable
{
    private int id;
    private int[] stream;
    
    public SerialData() {
    }
    
    public SerialData(final int id, final int[] stream) {
        this.id = id;
        this.stream = stream;
    }
    
    public int getId() {
        return this.id;
    }
    
    public int[] getStream() {
        return this.stream;
    }
    
    public void setId(final int id) {
        this.id = id;
    }
    
    public void setStream(final int[] stream) {
        this.stream = stream;
    }
}

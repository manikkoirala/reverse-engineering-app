// 
// Decompiled by Procyon v0.6.0
// 

package com.ekodroid.omrevaluator.serializable;

import java.io.Serializable;

public class SheetImageConfigure implements Serializable
{
    public int bottomCornerHeight;
    public int bottomCornerHeightPercent;
    public int bottomCornerWidth;
    public int bottomCornerWidthPercent;
    public int imageHeight;
    public int imageRotation;
    public int imageWidth;
    public int squarelength;
    public int squarelengthPercent;
    public int topCornerHeight;
    public int topCornerHeightPercent;
    public int topCornerWidth;
    public int topCornerWidthPercent;
}

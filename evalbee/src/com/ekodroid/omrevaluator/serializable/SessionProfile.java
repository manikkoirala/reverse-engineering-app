// 
// Decompiled by Procyon v0.6.0
// 

package com.ekodroid.omrevaluator.serializable;

import java.io.Serializable;

public class SessionProfile implements Serializable
{
    private String configData;
    private String configId;
    private String data;
    private String id;
    
    public SessionProfile() {
    }
    
    public SessionProfile(final String id, final String data, final String configData, final String configId) {
        this.id = id;
        this.data = data;
        this.configData = configData;
        this.configId = configId;
    }
    
    public String getConfigData() {
        return this.configData;
    }
    
    public String getConfigId() {
        return this.configId;
    }
    
    public String getData() {
        return this.data;
    }
    
    public String getId() {
        return this.id;
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.ekodroid.omrevaluator.database;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;
import java.io.Serializable;

@DatabaseTable(tableName = "tb_students_v2")
public class StudentDataModel implements Serializable
{
    @DatabaseField(canBeNull = false, uniqueCombo = true)
    private String className;
    @DatabaseField
    private String emailId;
    @DatabaseField(allowGeneratedIdInsert = true, generatedId = true)
    private int id;
    @DatabaseField
    private boolean isSynced;
    @DatabaseField
    private String phoneNo;
    @DatabaseField(canBeNull = false, uniqueCombo = true)
    private Integer rollNO;
    @DatabaseField(canBeNull = false)
    private String studentName;
    
    public StudentDataModel() {
    }
    
    public StudentDataModel(final Integer rollNO, final String studentName, final String emailId, final String phoneNo, final String className) {
        this.rollNO = rollNO;
        this.studentName = studentName;
        this.emailId = emailId;
        this.phoneNo = phoneNo;
        this.className = className;
    }
    
    public String getClassName() {
        return this.className;
    }
    
    public String getEmailId() {
        return this.emailId;
    }
    
    public String getPhoneNo() {
        return this.phoneNo;
    }
    
    public Integer getRollNO() {
        return this.rollNO;
    }
    
    public String getStudentName() {
        return this.studentName;
    }
    
    public boolean isSynced() {
        return this.isSynced;
    }
    
    public void setEmailId(final String emailId) {
        this.emailId = emailId;
    }
    
    public void setPhoneNo(final String phoneNo) {
        this.phoneNo = phoneNo;
    }
    
    public void setRollNO(final Integer rollNO) {
        this.rollNO = rollNO;
    }
    
    public void setStudentName(final String studentName) {
        this.studentName = studentName;
    }
    
    public void setSynced(final boolean isSynced) {
        this.isSynced = isSynced;
    }
}

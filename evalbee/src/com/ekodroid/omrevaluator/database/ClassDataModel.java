// 
// Decompiled by Procyon v0.6.0
// 

package com.ekodroid.omrevaluator.database;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

@DatabaseTable(tableName = "tb_class_v2")
public class ClassDataModel
{
    @DatabaseField(id = true)
    private String className;
    @DatabaseField
    private String subGroups;
    @DatabaseField
    private boolean synced;
    
    public ClassDataModel() {
    }
    
    public ClassDataModel(final String className, final String subGroups, final boolean synced) {
        this.className = className;
        this.subGroups = subGroups;
        this.synced = synced;
    }
    
    public String getClassName() {
        return this.className;
    }
    
    public String getSubGroups() {
        return this.subGroups;
    }
    
    public boolean isSynced() {
        return this.synced;
    }
    
    public void setClassName(final String className) {
        this.className = className;
    }
    
    public void setSubGroups(final String subGroups) {
        this.subGroups = subGroups;
    }
    
    public void setSynced(final boolean b) {
    }
}

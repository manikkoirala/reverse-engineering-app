// 
// Decompiled by Procyon v0.6.0
// 

package com.ekodroid.omrevaluator.database.repositories;

import com.j256.ormlite.stmt.StatementBuilder;
import java.util.List;
import com.j256.ormlite.stmt.QueryBuilder;
import com.ekodroid.omrevaluator.database.SmsTemplateDataModel;
import java.util.ArrayList;
import com.j256.ormlite.stmt.DeleteBuilder;
import com.j256.ormlite.stmt.SelectArg;
import android.content.Context;
import com.ekodroid.omrevaluator.database.DatabaseHelper;

public class SmsTemplateRepository
{
    private static SmsTemplateRepository instance;
    private DatabaseHelper helper;
    
    private SmsTemplateRepository(final Context context) {
        this.helper = DatabaseHelper.getInstance(context);
    }
    
    public static SmsTemplateRepository getInstance(final Context context) {
        init(context);
        return SmsTemplateRepository.instance;
    }
    
    private static void init(final Context context) {
        if (SmsTemplateRepository.instance == null) {
            SmsTemplateRepository.instance = new SmsTemplateRepository(context);
        }
    }
    
    public boolean deleteSmsTemplate(final String s) {
        try {
            final DeleteBuilder deleteBuilder = this.getHelper().getSmsTemplateDAO().deleteBuilder();
            ((StatementBuilder)deleteBuilder).where().eq("smsTemplateName", (Object)new SelectArg((Object)s));
            deleteBuilder.delete();
            return true;
        }
        catch (final Exception ex) {
            ex.printStackTrace();
            return false;
        }
    }
    
    public ArrayList<SmsTemplateDataModel> getAllSmsTemplates() {
        ArrayList list = new ArrayList();
        try {
            list = (ArrayList)this.getHelper().getSmsTemplateDAO().queryForAll();
        }
        catch (final Exception ex) {
            ex.printStackTrace();
        }
        return list;
    }
    
    public DatabaseHelper getHelper() {
        return this.helper;
    }
    
    public SmsTemplateDataModel getSmsTemplate(final String s) {
        try {
            final QueryBuilder queryBuilder = this.getHelper().getSmsTemplateDAO().queryBuilder();
            ((StatementBuilder)queryBuilder).where().eq("smsTemplateName", (Object)new SelectArg((Object)s));
            final List query = queryBuilder.query();
            if (query != null && query.size() == 1) {
                return (SmsTemplateDataModel)query.get(0);
            }
            return null;
        }
        catch (final Exception ex) {
            ex.printStackTrace();
            return null;
        }
    }
    
    public boolean saveOrUpdateSmsTemplate(final SmsTemplateDataModel smsTemplateDataModel) {
        try {
            this.getHelper().getSmsTemplateDAO().createOrUpdate((Object)smsTemplateDataModel);
            return true;
        }
        catch (final Exception ex) {
            ex.printStackTrace();
            return false;
        }
    }
}

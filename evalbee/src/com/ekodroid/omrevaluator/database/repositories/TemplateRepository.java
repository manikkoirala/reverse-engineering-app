// 
// Decompiled by Procyon v0.6.0
// 

package com.ekodroid.omrevaluator.database.repositories;

import com.j256.ormlite.stmt.StatementBuilder;
import java.util.Iterator;
import com.j256.ormlite.dao.Dao;
import java.util.List;
import com.ekodroid.omrevaluator.database.ArchivedLocalDataModel;
import com.ekodroid.omrevaluator.database.LabelProfileJsonModel;
import com.j256.ormlite.stmt.Where;
import com.j256.ormlite.stmt.QueryBuilder;
import com.ekodroid.omrevaluator.database.TemplateDataJsonModel;
import com.ekodroid.omrevaluator.database.TemplateHeaderJsonDataModel;
import java.util.ArrayList;
import com.ekodroid.omrevaluator.templateui.models.ExamId;
import com.j256.ormlite.stmt.DeleteBuilder;
import java.sql.SQLException;
import com.j256.ormlite.stmt.SelectArg;
import android.content.Context;
import com.ekodroid.omrevaluator.database.DatabaseHelper;

public class TemplateRepository
{
    private static TemplateRepository instance;
    private DatabaseHelper helper;
    
    private TemplateRepository(final Context context) {
        this.helper = DatabaseHelper.getInstance(context);
    }
    
    public static TemplateRepository getInstance(final Context context) {
        init(context);
        return TemplateRepository.instance;
    }
    
    private static void init(final Context context) {
        if (TemplateRepository.instance == null) {
            TemplateRepository.instance = new TemplateRepository(context);
        }
    }
    
    public boolean deleteAllTemplateForClass(final String s) {
        try {
            final DeleteBuilder deleteBuilder = this.getHelper().getTemplateJsonDAO().deleteBuilder();
            ((StatementBuilder)deleteBuilder).where().eq("className", (Object)new SelectArg((Object)s));
            deleteBuilder.delete();
            return true;
        }
        catch (final SQLException ex) {
            ex.printStackTrace();
            return false;
        }
    }
    
    public boolean deleteArchiveLocalModel(final String s) {
        try {
            this.getHelper().getArchiveLocalDAO().deleteById((Object)s);
            return true;
        }
        catch (final SQLException ex) {
            ex.printStackTrace();
            return false;
        }
    }
    
    public boolean deleteHeaderProfileJson(final String s) {
        try {
            final DeleteBuilder deleteBuilder = this.getHelper().getHeaderProfileJsonDAO().deleteBuilder();
            ((StatementBuilder)deleteBuilder).where().eq("headerName", (Object)new SelectArg((Object)s));
            deleteBuilder.delete();
            return true;
        }
        catch (final SQLException ex) {
            ex.printStackTrace();
            return false;
        }
    }
    
    public boolean deleteLabelProfileJson(final String s) {
        try {
            final DeleteBuilder deleteBuilder = this.getHelper().getLabelProfileJsonDAO().deleteBuilder();
            ((StatementBuilder)deleteBuilder).where().eq("profileName", (Object)new SelectArg((Object)s));
            deleteBuilder.delete();
            return true;
        }
        catch (final SQLException ex) {
            ex.printStackTrace();
            return false;
        }
    }
    
    public boolean deleteTemplateJson(final ExamId examId) {
        try {
            final DeleteBuilder deleteBuilder = this.getHelper().getTemplateJsonDAO().deleteBuilder();
            ((StatementBuilder)deleteBuilder).where().eq("name", (Object)new SelectArg((Object)examId.getExamName())).and().eq("className", (Object)new SelectArg((Object)examId.getClassName())).and().eq("examDate", (Object)new SelectArg((Object)examId.getExamDate()));
            deleteBuilder.delete();
            return true;
        }
        catch (final SQLException ex) {
            ex.printStackTrace();
            return false;
        }
    }
    
    public ArrayList<TemplateHeaderJsonDataModel> getAllHeaderProfileJson() {
        ArrayList list = new ArrayList();
        try {
            list = (ArrayList)this.getHelper().getHeaderProfileJsonDAO().queryForAll();
        }
        catch (final SQLException ex) {
            ex.printStackTrace();
        }
        return list;
    }
    
    public ArrayList<TemplateDataJsonModel> getAllImageSyncedActivatedExams() {
        ArrayList list = new ArrayList();
        try {
            final QueryBuilder queryBuilder = this.getHelper().getTemplateJsonDAO().queryBuilder();
            final Where where = ((StatementBuilder)queryBuilder).where();
            final Boolean true = Boolean.TRUE;
            where.eq("isCloudSyncOn", (Object)true).and().eq("syncImages", (Object)true);
            list = (ArrayList)queryBuilder.query();
        }
        catch (final SQLException ex) {
            ex.printStackTrace();
        }
        return list;
    }
    
    public ArrayList<LabelProfileJsonModel> getAllLabelProfileJson() {
        ArrayList list = new ArrayList();
        try {
            list = (ArrayList)this.getHelper().getLabelProfileJsonDAO().queryForAll();
        }
        catch (final SQLException ex) {
            ex.printStackTrace();
        }
        return list;
    }
    
    public ArrayList<TemplateDataJsonModel> getAllSyncedExams() {
        ArrayList list = new ArrayList();
        try {
            final QueryBuilder queryBuilder = this.getHelper().getTemplateJsonDAO().queryBuilder();
            ((StatementBuilder)queryBuilder).where().eq("isCloudSyncOn", (Object)Boolean.TRUE);
            list = (ArrayList)queryBuilder.query();
        }
        catch (final SQLException ex) {
            ex.printStackTrace();
        }
        return list;
    }
    
    public ArchivedLocalDataModel getArchiveLocalDataModel(final String s) {
        try {
            return (ArchivedLocalDataModel)this.getHelper().getArchiveLocalDAO().queryForId((Object)s);
        }
        catch (final SQLException ex) {
            ex.printStackTrace();
            return null;
        }
    }
    
    public ArrayList<ArchivedLocalDataModel> getArchiveLocalDataModels() {
        ArrayList list;
        try {
            list = (ArrayList)this.getHelper().getArchiveLocalDAO().queryForAll();
        }
        catch (final SQLException ex) {
            ex.printStackTrace();
            list = null;
        }
        return list;
    }
    
    public ArrayList<TemplateDataJsonModel> getArchivingTemplatesJson() {
        ArrayList list = new ArrayList();
        try {
            final QueryBuilder queryBuilder = this.getHelper().getTemplateJsonDAO().queryBuilder();
            ((StatementBuilder)queryBuilder).where().eq("isArchiving", (Object)Boolean.TRUE);
            list = (ArrayList)queryBuilder.query();
        }
        catch (final SQLException ex) {
            ex.printStackTrace();
        }
        return list;
    }
    
    public TemplateHeaderJsonDataModel getHeaderProfileJsonModel(final String s) {
        try {
            final QueryBuilder queryBuilder = this.getHelper().getHeaderProfileJsonDAO().queryBuilder();
            ((StatementBuilder)queryBuilder).where().eq("headerName", (Object)new SelectArg((Object)s));
            final List query = queryBuilder.query();
            if (query != null && query.size() > 0) {
                return (TemplateHeaderJsonDataModel)query.get(0);
            }
            return null;
        }
        catch (final SQLException ex) {
            ex.printStackTrace();
            return null;
        }
    }
    
    public DatabaseHelper getHelper() {
        return this.helper;
    }
    
    public LabelProfileJsonModel getLabelProfileJsonModel(final String s) {
        try {
            final QueryBuilder queryBuilder = this.getHelper().getLabelProfileJsonDAO().queryBuilder();
            ((StatementBuilder)queryBuilder).where().eq("profileName", (Object)new SelectArg((Object)s));
            final List query = queryBuilder.query();
            if (query != null && query.size() > 0) {
                return (LabelProfileJsonModel)query.get(0);
            }
            return null;
        }
        catch (final SQLException ex) {
            ex.printStackTrace();
            return null;
        }
    }
    
    public long getNumberOfExamForAClass(final String s) {
        long count;
        try {
            count = ((StatementBuilder)this.getHelper().getTemplateJsonDAO().queryBuilder()).where().eq("className", (Object)new SelectArg((Object)s)).countOf();
        }
        catch (final SQLException ex) {
            ex.printStackTrace();
            count = 0L;
        }
        return count;
    }
    
    public TemplateDataJsonModel getTemplateJson(final ExamId examId) {
        try {
            final QueryBuilder queryBuilder = this.getHelper().getTemplateJsonDAO().queryBuilder();
            ((StatementBuilder)queryBuilder).where().eq("name", (Object)new SelectArg((Object)examId.getExamName())).and().eq("className", (Object)new SelectArg((Object)examId.getClassName())).and().eq("examDate", (Object)new SelectArg((Object)examId.getExamDate()));
            final List query = queryBuilder.query();
            if (query != null && query.size() > 0) {
                return (TemplateDataJsonModel)query.get(0);
            }
            return null;
        }
        catch (final SQLException ex) {
            ex.printStackTrace();
            return null;
        }
    }
    
    public TemplateDataJsonModel getTemplateJsonForCloudId(final Long n) {
        try {
            final QueryBuilder queryBuilder = this.getHelper().getTemplateJsonDAO().queryBuilder();
            ((StatementBuilder)queryBuilder).where().eq("cloudId", (Object)n);
            final List query = queryBuilder.query();
            if (query != null && query.size() > 0) {
                return (TemplateDataJsonModel)query.get(0);
            }
            return null;
        }
        catch (final SQLException ex) {
            ex.printStackTrace();
            return null;
        }
    }
    
    public ArrayList<TemplateDataJsonModel> getTemplatesJson() {
        ArrayList list = new ArrayList();
        try {
            list = (ArrayList)this.getHelper().getTemplateJsonDAO().queryForAll();
        }
        catch (final Exception ex) {
            ex.printStackTrace();
        }
        return list;
    }
    
    public ArrayList<TemplateDataJsonModel> getTemplatesJson(final String s) {
        ArrayList list;
        try {
            final QueryBuilder queryBuilder = this.getHelper().getTemplateJsonDAO().queryBuilder();
            ((StatementBuilder)queryBuilder).where().eq("className", (Object)new SelectArg((Object)s));
            list = (ArrayList)queryBuilder.query();
        }
        catch (final SQLException ex) {
            ex.printStackTrace();
            list = null;
        }
        return list;
    }
    
    public void markExamAsPublished(final ExamId examId, final Long n) {
        final TemplateDataJsonModel templateJson = this.getTemplateJson(examId);
        if (templateJson != null) {
            templateJson.setPublished(true);
            try {
                this.getHelper().getTemplateJsonDAO().createOrUpdate((Object)templateJson);
            }
            catch (final SQLException ex) {
                ex.printStackTrace();
            }
        }
    }
    
    public void migrateScopStorage(final String regex, final String replacement) {
        try {
            final Dao<TemplateDataJsonModel, Integer> templateJsonDAO = this.getHelper().getTemplateJsonDAO();
            for (final TemplateDataJsonModel templateDataJsonModel : templateJsonDAO.queryForAll()) {
                final String folderPath = templateDataJsonModel.getFolderPath();
                if (folderPath != null) {
                    templateDataJsonModel.setFolderPath(folderPath.replaceFirst(regex, replacement));
                    templateJsonDAO.update((Object)templateDataJsonModel);
                }
            }
        }
        catch (final SQLException ex) {
            ex.printStackTrace();
        }
    }
    
    public boolean saveOrUpdateArchiveLocalDataModel(final ArchivedLocalDataModel archivedLocalDataModel) {
        try {
            this.getHelper().getArchiveLocalDAO().createOrUpdate((Object)archivedLocalDataModel);
            return true;
        }
        catch (final SQLException ex) {
            ex.printStackTrace();
            return false;
        }
    }
    
    public boolean saveOrUpdateHeaderProfileJson(final TemplateHeaderJsonDataModel templateHeaderJsonDataModel) {
        try {
            this.getHelper().getHeaderProfileJsonDAO().createOrUpdate((Object)templateHeaderJsonDataModel);
            return true;
        }
        catch (final SQLException ex) {
            ex.printStackTrace();
            return false;
        }
    }
    
    public boolean saveOrUpdateLabelProfileJson(final LabelProfileJsonModel labelProfileJsonModel) {
        try {
            this.getHelper().getLabelProfileJsonDAO().createOrUpdate((Object)labelProfileJsonModel);
            return true;
        }
        catch (final SQLException ex) {
            ex.printStackTrace();
            return false;
        }
    }
    
    public boolean saveOrUpdateTemplateJson(final TemplateDataJsonModel templateDataJsonModel) {
        try {
            templateDataJsonModel.setSynced(false);
            this.getHelper().getTemplateJsonDAO().createOrUpdate((Object)templateDataJsonModel);
            return true;
        }
        catch (final SQLException ex) {
            ex.printStackTrace();
            return false;
        }
    }
    
    public boolean saveOrUpdateTemplateJsonAsSynced(final TemplateDataJsonModel templateDataJsonModel) {
        try {
            templateDataJsonModel.setSynced(true);
            this.getHelper().getTemplateJsonDAO().createOrUpdate((Object)templateDataJsonModel);
            return true;
        }
        catch (final SQLException ex) {
            ex.printStackTrace();
            return false;
        }
    }
}

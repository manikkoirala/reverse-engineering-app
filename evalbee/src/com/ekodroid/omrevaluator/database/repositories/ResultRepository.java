// 
// Decompiled by Procyon v0.6.0
// 

package com.ekodroid.omrevaluator.database.repositories;

import com.j256.ormlite.stmt.StatementBuilder;
import com.j256.ormlite.dao.Dao;
import com.ekodroid.omrevaluator.database.FileDataModel;
import java.util.Collection;
import java.util.HashSet;
import com.ekodroid.omrevaluator.database.ResultDataJsonModel;
import com.j256.ormlite.stmt.QueryBuilder;
import java.util.ArrayList;
import java.util.List;
import java.util.Iterator;
import com.ekodroid.omrevaluator.database.SheetImageModel;
import com.ekodroid.omrevaluator.templateui.models.ExamId;
import com.j256.ormlite.stmt.DeleteBuilder;
import java.sql.SQLException;
import com.j256.ormlite.stmt.SelectArg;
import android.content.Context;
import com.ekodroid.omrevaluator.database.DatabaseHelper;

public class ResultRepository
{
    private static ResultRepository instance;
    private DatabaseHelper helper;
    
    private ResultRepository(final Context context) {
        this.helper = DatabaseHelper.getInstance(context);
    }
    
    public static ResultRepository getInstance(final Context context) {
        init(context);
        return ResultRepository.instance;
    }
    
    private static void init(final Context context) {
        if (ResultRepository.instance == null) {
            ResultRepository.instance = new ResultRepository(context);
        }
    }
    
    public boolean deleteAllResultForClass(final String s) {
        try {
            final DeleteBuilder deleteBuilder = this.getHelper().getResultJsonDAO().deleteBuilder();
            ((StatementBuilder)deleteBuilder).where().eq("className", (Object)new SelectArg((Object)s));
            deleteBuilder.delete();
            return true;
        }
        catch (final SQLException ex) {
            ex.printStackTrace();
            return false;
        }
    }
    
    public boolean deleteAllResultForExam(final ExamId examId) {
        try {
            final DeleteBuilder deleteBuilder = this.getHelper().getResultJsonDAO().deleteBuilder();
            ((StatementBuilder)deleteBuilder).where().eq("examName", (Object)new SelectArg((Object)examId.getExamName())).and().eq("className", (Object)new SelectArg((Object)examId.getClassName())).and().eq("examDate", (Object)new SelectArg((Object)examId.getExamDate()));
            deleteBuilder.delete();
            return true;
        }
        catch (final SQLException ex) {
            ex.printStackTrace();
            return false;
        }
    }
    
    public boolean deleteAllSheetImageForClass(final String s) {
        try {
            final DeleteBuilder deleteBuilder = this.getHelper().getSheetImageDAO().deleteBuilder();
            ((StatementBuilder)deleteBuilder).where().eq("className", (Object)new SelectArg((Object)s));
            deleteBuilder.delete();
            return true;
        }
        catch (final SQLException ex) {
            ex.printStackTrace();
            return false;
        }
    }
    
    public boolean deleteAllSheetImageForExam(final ExamId examId, final String s) {
        if (s != null) {
            b10.b(s);
        }
        try {
            final DeleteBuilder deleteBuilder = this.getHelper().getSheetImageDAO().deleteBuilder();
            ((StatementBuilder)deleteBuilder).where().eq("examName", (Object)new SelectArg((Object)examId.getExamName())).and().eq("className", (Object)new SelectArg((Object)examId.getClassName())).and().eq("examDate", (Object)new SelectArg((Object)examId.getExamDate()));
            deleteBuilder.delete();
            return true;
        }
        catch (final SQLException ex) {
            ex.printStackTrace();
            return false;
        }
    }
    
    public boolean deleteSheetImage(final ExamId examId, final int i, final int j) {
        final SheetImageModel sheetImage = this.getSheetImage(examId, i, j);
        if (sheetImage != null && sheetImage.getImagePath() != null) {
            b10.b(sheetImage.getImagePath());
        }
        try {
            final DeleteBuilder deleteBuilder = this.getHelper().getSheetImageDAO().deleteBuilder();
            ((StatementBuilder)deleteBuilder).where().eq("examName", (Object)new SelectArg((Object)examId.getExamName())).and().eq("className", (Object)new SelectArg((Object)examId.getClassName())).and().eq("examDate", (Object)new SelectArg((Object)examId.getExamDate())).and().eq("rollNo", (Object)i).and().eq("pageIndex", (Object)j);
            deleteBuilder.delete();
            return true;
        }
        catch (final SQLException ex) {
            ex.printStackTrace();
            return false;
        }
    }
    
    public boolean deleteSheetImages(final ExamId examId, final int i) {
        for (final SheetImageModel sheetImageModel : this.getSheetImages(examId, i)) {
            if (sheetImageModel != null && sheetImageModel.getImagePath() != null) {
                b10.b(sheetImageModel.getImagePath());
            }
        }
        try {
            final DeleteBuilder deleteBuilder = this.getHelper().getSheetImageDAO().deleteBuilder();
            ((StatementBuilder)deleteBuilder).where().eq("examName", (Object)new SelectArg((Object)examId.getExamName())).and().eq("className", (Object)new SelectArg((Object)examId.getClassName())).and().eq("examDate", (Object)new SelectArg((Object)examId.getExamDate())).and().eq("rollNo", (Object)i);
            deleteBuilder.delete();
            return true;
        }
        catch (final SQLException ex) {
            ex.printStackTrace();
            return false;
        }
    }
    
    public boolean deleteSheetModelKeepingImages(final ExamId examId, final int i) {
        try {
            final DeleteBuilder deleteBuilder = this.getHelper().getSheetImageDAO().deleteBuilder();
            ((StatementBuilder)deleteBuilder).where().eq("examName", (Object)new SelectArg((Object)examId.getExamName())).and().eq("className", (Object)new SelectArg((Object)examId.getClassName())).and().eq("examDate", (Object)new SelectArg((Object)examId.getExamDate())).and().eq("rollNo", (Object)i);
            deleteBuilder.delete();
            return true;
        }
        catch (final SQLException ex) {
            ex.printStackTrace();
            return false;
        }
    }
    
    public boolean deleteStudentResult(final ExamId examId, final int i) {
        try {
            final DeleteBuilder deleteBuilder = this.getHelper().getResultJsonDAO().deleteBuilder();
            ((StatementBuilder)deleteBuilder).where().eq("examName", (Object)new SelectArg((Object)examId.getExamName())).and().eq("className", (Object)new SelectArg((Object)examId.getClassName())).and().eq("examDate", (Object)new SelectArg((Object)examId.getExamDate())).and().eq("rollNo", (Object)i);
            deleteBuilder.delete();
            return true;
        }
        catch (final SQLException ex) {
            ex.printStackTrace();
            return false;
        }
    }
    
    public List<SheetImageModel> getAllNonSyncImages(final ExamId examId) {
        try {
            final QueryBuilder queryBuilder = this.getHelper().getSheetImageDAO().queryBuilder();
            ((StatementBuilder)queryBuilder).where().eq("examName", (Object)new SelectArg((Object)examId.getExamName())).and().eq("className", (Object)new SelectArg((Object)examId.getClassName())).and().eq("examDate", (Object)new SelectArg((Object)examId.getExamDate())).and().eq("synced", (Object)Boolean.FALSE);
            return queryBuilder.query();
        }
        catch (final SQLException ex) {
            ex.printStackTrace();
            return new ArrayList<SheetImageModel>();
        }
    }
    
    public long getAllNonSyncResultCount(final ExamId examId) {
        try {
            final QueryBuilder queryBuilder = this.getHelper().getResultJsonDAO().queryBuilder();
            ((StatementBuilder)queryBuilder).where().eq("examName", (Object)new SelectArg((Object)examId.getExamName())).and().eq("className", (Object)new SelectArg((Object)examId.getClassName())).and().eq("examDate", (Object)new SelectArg((Object)examId.getExamDate())).and().eq("isSynced", (Object)Boolean.FALSE);
            return queryBuilder.countOf();
        }
        catch (final SQLException ex) {
            ex.printStackTrace();
            return 0L;
        }
    }
    
    public ArrayList<ResultDataJsonModel> getAllNotSyncedResults(final ExamId examId) {
        try {
            final QueryBuilder queryBuilder = this.getHelper().getResultJsonDAO().queryBuilder();
            ((StatementBuilder)queryBuilder).where().eq("examName", (Object)new SelectArg((Object)examId.getExamName())).and().eq("className", (Object)new SelectArg((Object)examId.getClassName())).and().eq("examDate", (Object)new SelectArg((Object)examId.getExamDate())).and().eq("isSynced", (Object)Boolean.FALSE);
            queryBuilder.orderBy("rollNo", true);
            final List query = queryBuilder.query();
            if (query != null && query.size() > 0) {
                return (ArrayList<ResultDataJsonModel>)query;
            }
        }
        catch (final SQLException ex) {
            ex.printStackTrace();
        }
        return new ArrayList<ResultDataJsonModel>();
    }
    
    public long getAllResultCount(final ExamId examId) {
        try {
            final QueryBuilder queryBuilder = this.getHelper().getResultJsonDAO().queryBuilder();
            ((StatementBuilder)queryBuilder).where().eq("examName", (Object)new SelectArg((Object)examId.getExamName())).and().eq("className", (Object)new SelectArg((Object)examId.getClassName())).and().eq("examDate", (Object)new SelectArg((Object)examId.getExamDate()));
            return queryBuilder.countOf();
        }
        catch (final SQLException ex) {
            ex.printStackTrace();
            return 0L;
        }
    }
    
    public ArrayList<ResultDataJsonModel> getAllResultJson(final ExamId examId) {
        try {
            final QueryBuilder queryBuilder = this.getHelper().getResultJsonDAO().queryBuilder();
            ((StatementBuilder)queryBuilder).where().eq("examName", (Object)new SelectArg((Object)examId.getExamName())).and().eq("className", (Object)new SelectArg((Object)examId.getClassName())).and().eq("examDate", (Object)new SelectArg((Object)examId.getExamDate()));
            queryBuilder.orderBy("rollNo", true);
            final List query = queryBuilder.query();
            if (query != null && query.size() > 0) {
                return (ArrayList<ResultDataJsonModel>)query;
            }
        }
        catch (final SQLException ex) {
            ex.printStackTrace();
        }
        return new ArrayList<ResultDataJsonModel>();
    }
    
    public HashSet<Integer> getAllUniqueRollNumberForExam(final ExamId examId) {
        try {
            final List query = ((StatementBuilder)this.getHelper().getResultJsonDAO().queryBuilder().distinct().selectColumns(new String[] { "rollNo" })).where().eq("examName", (Object)new SelectArg((Object)examId.getExamName())).and().eq("className", (Object)new SelectArg((Object)examId.getClassName())).and().eq("examDate", (Object)new SelectArg((Object)examId.getExamDate())).query();
            final ArrayList<Integer> c = new ArrayList<Integer>();
            final Iterator iterator = query.iterator();
            while (iterator.hasNext()) {
                c.add(((ResultDataJsonModel)iterator.next()).getRollNo());
            }
            return new HashSet<Integer>(c);
        }
        catch (final SQLException ex) {
            ex.printStackTrace();
            return new HashSet<Integer>();
        }
    }
    
    public FileDataModel getFile(final String s) {
        try {
            final QueryBuilder queryBuilder = this.getHelper().getFileDAO().queryBuilder();
            ((StatementBuilder)queryBuilder).where().eq("fileName", (Object)s);
            final List query = queryBuilder.query();
            if (query != null && query.size() == 1) {
                return (FileDataModel)query.get(0);
            }
            return null;
        }
        catch (final SQLException ex) {
            ex.printStackTrace();
            return null;
        }
    }
    
    public DatabaseHelper getHelper() {
        return this.helper;
    }
    
    public ResultDataJsonModel getResult(final ExamId examId, final int i) {
        try {
            final QueryBuilder queryBuilder = this.getHelper().getResultJsonDAO().queryBuilder();
            ((StatementBuilder)queryBuilder).where().eq("examName", (Object)new SelectArg((Object)examId.getExamName())).and().eq("className", (Object)new SelectArg((Object)examId.getClassName())).and().eq("examDate", (Object)new SelectArg((Object)examId.getExamDate())).and().eq("rollNo", (Object)i);
            final List query = queryBuilder.query();
            if (query != null && query.size() > 0) {
                return (ResultDataJsonModel)query.get(0);
            }
            return null;
        }
        catch (final SQLException ex) {
            ex.printStackTrace();
            return null;
        }
    }
    
    public boolean getResultEmailSendStatus(final ExamId examId, final int n) {
        final ResultDataJsonModel result = this.getResult(examId, n);
        return result != null && result.isPublished();
    }
    
    public SheetImageModel getSheetImage(final ExamId examId, final int i, final int j) {
        final ArrayList list = new ArrayList();
        try {
            final QueryBuilder queryBuilder = this.getHelper().getSheetImageDAO().queryBuilder();
            ((StatementBuilder)queryBuilder).where().eq("examName", (Object)new SelectArg((Object)examId.getExamName())).and().eq("className", (Object)new SelectArg((Object)examId.getClassName())).and().eq("examDate", (Object)new SelectArg((Object)examId.getExamDate())).and().eq("rollNo", (Object)i).and().eq("pageIndex", (Object)j);
            if (queryBuilder.query().size() > 0) {
                list.get(0);
            }
        }
        catch (final SQLException ex) {
            ex.printStackTrace();
        }
        return null;
    }
    
    public long getSheetImageCountForExam(final ExamId examId) {
        long count;
        try {
            count = ((StatementBuilder)this.getHelper().getSheetImageDAO().queryBuilder()).where().eq("examName", (Object)new SelectArg((Object)examId.getExamName())).and().eq("className", (Object)new SelectArg((Object)examId.getClassName())).and().eq("examDate", (Object)new SelectArg((Object)examId.getExamDate())).and().isNotNull("sheetDimension").countOf();
        }
        catch (final SQLException ex) {
            ex.printStackTrace();
            count = 0L;
        }
        return count;
    }
    
    public ArrayList<SheetImageModel> getSheetImages(final ExamId examId, final int i) {
        final ArrayList list = new ArrayList();
        try {
            final QueryBuilder queryBuilder = this.getHelper().getSheetImageDAO().queryBuilder();
            ((StatementBuilder)queryBuilder).where().eq("examName", (Object)new SelectArg((Object)examId.getExamName())).and().eq("className", (Object)new SelectArg((Object)examId.getClassName())).and().eq("examDate", (Object)new SelectArg((Object)examId.getExamDate())).and().eq("rollNo", (Object)i);
            final List query = queryBuilder.query();
            if (query.size() > 0) {
                list.addAll(query);
            }
        }
        catch (final SQLException ex) {
            ex.printStackTrace();
        }
        return list;
    }
    
    public boolean getSmsSendStatus(final ExamId examId, final int n) {
        final ResultDataJsonModel result = this.getResult(examId, n);
        return result != null && result.isSmsSent();
    }
    
    public boolean markResultAsPublished(final ExamId examId, ArrayList<Integer> iterator) {
        iterator = ((ArrayList<Integer>)iterator).iterator();
        while (iterator.hasNext()) {
            final ResultDataJsonModel result = this.getResult(examId, iterator.next());
            if (result != null) {
                result.setPublished(true);
                try {
                    this.getHelper().getResultJsonDAO().createOrUpdate((Object)result);
                }
                catch (final SQLException ex) {
                    ex.printStackTrace();
                }
            }
        }
        return true;
    }
    
    public void migrateScopStorage(final String regex, final String replacement) {
        try {
            final Dao<SheetImageModel, Integer> sheetImageDAO = this.getHelper().getSheetImageDAO();
            for (final SheetImageModel sheetImageModel : sheetImageDAO.queryForAll()) {
                final String imagePath = sheetImageModel.getImagePath();
                if (imagePath != null) {
                    sheetImageModel.setImagePath(imagePath.replaceFirst(regex, replacement));
                    sheetImageDAO.update((Object)sheetImageModel);
                }
            }
        }
        catch (final SQLException ex) {
            ex.printStackTrace();
        }
    }
    
    public boolean saveOrUpdateFile(final FileDataModel fileDataModel) {
        try {
            this.getHelper().getFileDAO().createOrUpdate((Object)fileDataModel);
            return true;
        }
        catch (final SQLException ex) {
            ex.printStackTrace();
            return false;
        }
    }
    
    public boolean saveOrUpdateResultJsonAsNotSynced(final ResultDataJsonModel resultDataJsonModel) {
        try {
            resultDataJsonModel.setSynced(false);
            this.getHelper().getResultJsonDAO().createOrUpdate((Object)resultDataJsonModel);
            return true;
        }
        catch (final SQLException ex) {
            ex.printStackTrace();
            return false;
        }
    }
    
    public boolean saveOrUpdateResultJsonAsSynced(final ResultDataJsonModel resultDataJsonModel) {
        try {
            resultDataJsonModel.setSynced(true);
            this.getHelper().getResultJsonDAO().createOrUpdate((Object)resultDataJsonModel);
            return true;
        }
        catch (final SQLException ex) {
            ex.printStackTrace();
            return false;
        }
    }
    
    public boolean saveOrUpdateSheetImage(final SheetImageModel sheetImageModel) {
        try {
            this.getHelper().getSheetImageDAO().createOrUpdate((Object)sheetImageModel);
            return true;
        }
        catch (final SQLException ex) {
            ex.printStackTrace();
            return false;
        }
    }
    
    public boolean updateResultSent(final ExamId examId, final int n, final boolean published) {
        final ResultDataJsonModel result = this.getResult(examId, n);
        if (result != null) {
            result.setPublished(published);
            return this.saveOrUpdateResultJsonAsNotSynced(result);
        }
        return false;
    }
    
    public boolean updateSmsSendStatus(final ExamId examId, final int n, final boolean smsSent) {
        final ResultDataJsonModel result = this.getResult(examId, n);
        if (result != null) {
            result.setSmsSent(smsSent);
            return this.saveOrUpdateResultJsonAsNotSynced(result);
        }
        return false;
    }
    
    public boolean vaccumDataBase() {
        try {
            this.getHelper().getSheetImageDAO().executeRaw("VACUUM;", new String[0]);
            return true;
        }
        catch (final SQLException ex) {
            ex.printStackTrace();
            return false;
        }
    }
}

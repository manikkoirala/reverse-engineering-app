// 
// Decompiled by Procyon v0.6.0
// 

package com.ekodroid.omrevaluator.database.repositories;

import com.j256.ormlite.stmt.StatementBuilder;
import com.j256.ormlite.stmt.QueryBuilder;
import com.ekodroid.omrevaluator.database.StudentDataModel;
import java.util.Iterator;
import java.util.List;
import com.ekodroid.omrevaluator.database.ClassDataModel;
import java.util.ArrayList;
import com.j256.ormlite.stmt.DeleteBuilder;
import com.j256.ormlite.stmt.SelectArg;
import java.sql.SQLException;
import android.content.Context;
import com.ekodroid.omrevaluator.database.DatabaseHelper;

public class ClassRepository
{
    private static ClassRepository instance;
    private DatabaseHelper helper;
    
    private ClassRepository(final Context context) {
        this.helper = DatabaseHelper.getInstance(context);
    }
    
    public static ClassRepository getInstance(final Context context) {
        init(context);
        return ClassRepository.instance;
    }
    
    private static void init(final Context context) {
        if (ClassRepository.instance == null) {
            ClassRepository.instance = new ClassRepository(context);
        }
    }
    
    public boolean deleteAllStudent() {
        try {
            this.getHelper().getStudentDAO().deleteBuilder().delete();
            return true;
        }
        catch (final SQLException ex) {
            ex.printStackTrace();
            return false;
        }
    }
    
    public boolean deleteAllStudentForClass(final String s) {
        try {
            final DeleteBuilder deleteBuilder = this.getHelper().getStudentDAO().deleteBuilder();
            ((StatementBuilder)deleteBuilder).where().eq("className", (Object)new SelectArg((Object)s));
            deleteBuilder.delete();
            return true;
        }
        catch (final SQLException ex) {
            ex.printStackTrace();
            return false;
        }
    }
    
    public boolean deleteAllSyncedClass() {
        try {
            final DeleteBuilder deleteBuilder = this.getHelper().getClassDAO().deleteBuilder();
            ((StatementBuilder)deleteBuilder).where().eq("synced", (Object)Boolean.TRUE);
            deleteBuilder.delete();
            return true;
        }
        catch (final SQLException ex) {
            ex.printStackTrace();
            return false;
        }
    }
    
    public boolean deleteClass(final String s) {
        try {
            final DeleteBuilder deleteBuilder = this.getHelper().getClassDAO().deleteBuilder();
            ((StatementBuilder)deleteBuilder).where().eq("className", (Object)new SelectArg((Object)s));
            deleteBuilder.delete();
            return true;
        }
        catch (final SQLException ex) {
            ex.printStackTrace();
            return false;
        }
    }
    
    public boolean deleteStudent(final int i, final String s) {
        try {
            final DeleteBuilder deleteBuilder = this.getHelper().getStudentDAO().deleteBuilder();
            ((StatementBuilder)deleteBuilder).where().eq("rollNo", (Object)i).and().eq("className", (Object)new SelectArg((Object)s));
            deleteBuilder.delete();
            return true;
        }
        catch (final SQLException ex) {
            ex.printStackTrace();
            return false;
        }
    }
    
    public ArrayList<String> getAllClassNames() {
        final ArrayList list = new ArrayList();
        try {
            final List query = this.getHelper().getClassDAO().queryBuilder().distinct().selectColumns(new String[] { "className" }).query();
            if (query != null) {
                final Iterator iterator = query.iterator();
                while (iterator.hasNext()) {
                    list.add(((ClassDataModel)iterator.next()).getClassName());
                }
            }
        }
        catch (final SQLException ex) {
            ex.printStackTrace();
        }
        return list;
    }
    
    public ArrayList<ClassDataModel> getAllClasses() {
        final ArrayList list = new ArrayList();
        try {
            final List queryForAll = this.getHelper().getClassDAO().queryForAll();
            if (queryForAll != null) {
                final Iterator iterator = queryForAll.iterator();
                while (iterator.hasNext()) {
                    list.add(iterator.next());
                }
            }
        }
        catch (final SQLException ex) {
            ex.printStackTrace();
        }
        return list;
    }
    
    public ArrayList<StudentDataModel> getAllNonSyncStudents() {
        try {
            final QueryBuilder queryBuilder = this.getHelper().getStudentDAO().queryBuilder();
            ((StatementBuilder)queryBuilder).where().eq("isSynced", (Object)Boolean.FALSE);
            final List query = queryBuilder.query();
            if (query != null && query.size() > 0) {
                return (ArrayList<StudentDataModel>)query;
            }
        }
        catch (final SQLException ex) {
            ex.printStackTrace();
        }
        return new ArrayList<StudentDataModel>();
    }
    
    public ArrayList<StudentDataModel> getAllStudents() {
        try {
            final List query = this.getHelper().getStudentDAO().queryBuilder().orderBy("rollNO", true).orderBy("className", true).query();
            if (query != null && query.size() > 0) {
                return (ArrayList<StudentDataModel>)query;
            }
        }
        catch (final SQLException ex) {
            ex.printStackTrace();
        }
        return new ArrayList<StudentDataModel>();
    }
    
    public DatabaseHelper getHelper() {
        return this.helper;
    }
    
    public List<ClassDataModel> getNonSyncClasses() {
        try {
            final QueryBuilder queryBuilder = this.getHelper().getClassDAO().queryBuilder();
            ((StatementBuilder)queryBuilder).where().eq("synced", (Object)Boolean.FALSE);
            return queryBuilder.query();
        }
        catch (final SQLException ex) {
            ex.printStackTrace();
            return null;
        }
    }
    
    public long getNumberOfStudentForAClass(final String s) {
        long count;
        try {
            count = ((StatementBuilder)this.getHelper().getStudentDAO().queryBuilder()).where().eq("className", (Object)new SelectArg((Object)s)).countOf();
        }
        catch (final SQLException ex) {
            ex.printStackTrace();
            count = 0L;
        }
        return count;
    }
    
    public StudentDataModel getStudent(final int i, final String s) {
        try {
            final QueryBuilder queryBuilder = this.getHelper().getStudentDAO().queryBuilder();
            ((StatementBuilder)queryBuilder).where().eq("rollNo", (Object)i).and().eq("className", (Object)new SelectArg((Object)s));
            final List query = queryBuilder.query();
            if (query != null && query.size() == 1) {
                return (StudentDataModel)query.get(0);
            }
            return null;
        }
        catch (final SQLException ex) {
            ex.printStackTrace();
            return null;
        }
    }
    
    public ArrayList<StudentDataModel> getStudents(final String s) {
        try {
            final QueryBuilder queryBuilder = this.getHelper().getStudentDAO().queryBuilder();
            ((StatementBuilder)queryBuilder).where().eq("className", (Object)new SelectArg((Object)s));
            queryBuilder.orderBy("rollNO", true);
            final List query = queryBuilder.query();
            if (query != null && query.size() > 0) {
                return (ArrayList<StudentDataModel>)query;
            }
        }
        catch (final SQLException ex) {
            ex.printStackTrace();
        }
        return new ArrayList<StudentDataModel>();
    }
    
    public boolean saveOrUpdateClass(final ClassDataModel classDataModel) {
        try {
            this.getHelper().getClassJsonDAO().createOrUpdate((Object)classDataModel);
            return true;
        }
        catch (final SQLException ex) {
            ex.printStackTrace();
            return false;
        }
    }
    
    public boolean saveOrUpdateStudent(final StudentDataModel studentDataModel) {
        try {
            this.getHelper().getStudentDAO().createOrUpdate((Object)studentDataModel);
            return true;
        }
        catch (final SQLException ex) {
            ex.printStackTrace();
            return false;
        }
    }
    
    public boolean saveOrUpdateStudentAsSynced(final StudentDataModel studentDataModel) {
        try {
            studentDataModel.setSynced(true);
            this.getHelper().getStudentDAO().createOrUpdate((Object)studentDataModel);
            return true;
        }
        catch (final SQLException ex) {
            ex.printStackTrace();
            return false;
        }
    }
}

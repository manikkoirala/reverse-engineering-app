// 
// Decompiled by Procyon v0.6.0
// 

package com.ekodroid.omrevaluator.database;

import com.ekodroid.omrevaluator.templateui.models.ExamId;
import com.ekodroid.omrevaluator.serializable.ArchivedResult.ArchiveProgress;
import com.ekodroid.omrevaluator.templateui.models.SheetTemplate2;
import com.ekodroid.omrevaluator.serializable.SharedType;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

@DatabaseTable(tableName = "tb_templates_v2")
public class TemplateDataJsonModel
{
    @DatabaseField
    private String archiveId;
    @DatabaseField
    private String archivePayload;
    @DatabaseField(canBeNull = false, uniqueCombo = true)
    private String className;
    @DatabaseField
    private Long cloudId;
    @DatabaseField(canBeNull = false, uniqueCombo = true)
    private String examDate;
    @DatabaseField
    private String folderPath;
    @DatabaseField(allowGeneratedIdInsert = true, generatedId = true)
    private Integer id;
    @DatabaseField
    private boolean isArchiving;
    @DatabaseField
    private boolean isCloudSyncOn;
    @DatabaseField
    private boolean isPublished;
    @DatabaseField
    private boolean isSynced;
    @DatabaseField
    private Long lastSyncedOn;
    @DatabaseField
    private Long lastUpdatedOn;
    @DatabaseField(canBeNull = false, uniqueCombo = true)
    private String name;
    @DatabaseField
    SharedType sharedType;
    @DatabaseField
    private String sheetTemplate;
    @DatabaseField
    private boolean syncImages;
    @DatabaseField
    private String userId;
    
    public TemplateDataJsonModel() {
    }
    
    public TemplateDataJsonModel(final String name, final String className, final String examDate, final SheetTemplate2 sheetTemplate2, final String folderPath, final String archiveId) {
        this.name = name;
        this.examDate = examDate;
        this.sheetTemplate = new gc0().s(sheetTemplate2);
        this.className = className;
        this.folderPath = folderPath;
        this.isArchiving = false;
        this.archivePayload = null;
        this.archiveId = archiveId;
    }
    
    public String getArchiveId() {
        return this.archiveId;
    }
    
    public ArchiveProgress getArchivePayload() {
        final String archivePayload = this.archivePayload;
        if (archivePayload == null || archivePayload.length() < 2) {
            return new ArchiveProgress();
        }
        final ArchiveProgress archiveProgress = (ArchiveProgress)new gc0().j(this.archivePayload, ArchiveProgress.class);
        if (archiveProgress != null) {
            return archiveProgress;
        }
        return new ArchiveProgress();
    }
    
    public String getClassName() {
        return this.className;
    }
    
    public Long getCloudId() {
        return this.cloudId;
    }
    
    public String getExamDate() {
        return this.examDate;
    }
    
    public ExamId getExamId() {
        return new ExamId(this.name, this.className, this.examDate);
    }
    
    public String getFolderPath() {
        return this.folderPath;
    }
    
    public Long getLastSyncedOn() {
        return this.lastSyncedOn;
    }
    
    public Long getLastUpdatedOn() {
        return this.lastUpdatedOn;
    }
    
    public String getName() {
        return this.name;
    }
    
    public SharedType getSharedType() {
        return this.sharedType;
    }
    
    public SheetTemplate2 getSheetTemplate() {
        return (SheetTemplate2)new gc0().j(this.sheetTemplate, SheetTemplate2.class);
    }
    
    public String getUserId() {
        return this.userId;
    }
    
    public boolean isArchiving() {
        return this.isArchiving;
    }
    
    public boolean isCloudSyncOn() {
        return this.isCloudSyncOn;
    }
    
    public boolean isPublished() {
        return this.isPublished;
    }
    
    public boolean isSyncImages() {
        return this.syncImages;
    }
    
    public boolean isSynced() {
        return this.isSynced;
    }
    
    public void setArchiveId(final String archiveId) {
        this.archiveId = archiveId;
    }
    
    public void setArchivePayload(final ArchiveProgress archiveProgress) {
        this.archivePayload = new gc0().s(archiveProgress);
    }
    
    public void setArchiving(final boolean isArchiving) {
        this.isArchiving = isArchiving;
    }
    
    public void setCloudId(final Long cloudId) {
        this.cloudId = cloudId;
    }
    
    public void setCloudSyncOn(final boolean isCloudSyncOn) {
        this.isCloudSyncOn = isCloudSyncOn;
    }
    
    public void setFolderPath(final String folderPath) {
        this.folderPath = folderPath;
    }
    
    public void setLastSyncedOn(final Long lastSyncedOn) {
        this.lastSyncedOn = lastSyncedOn;
    }
    
    public void setLastUpdatedOn(final Long lastUpdatedOn) {
        this.lastUpdatedOn = lastUpdatedOn;
    }
    
    public void setName(final String name) {
        this.name = name;
    }
    
    public void setPublished(final boolean isPublished) {
        this.isPublished = isPublished;
    }
    
    public void setSharedType(final SharedType sharedType) {
        this.sharedType = sharedType;
    }
    
    public void setSheetTemplate(final SheetTemplate2 sheetTemplate2) {
        this.sheetTemplate = new gc0().s(sheetTemplate2);
    }
    
    public void setSyncImages(final boolean syncImages) {
        this.syncImages = syncImages;
    }
    
    public void setSynced(final boolean isSynced) {
        this.isSynced = isSynced;
    }
    
    public void setUserId(final String userId) {
        this.userId = userId;
    }
}

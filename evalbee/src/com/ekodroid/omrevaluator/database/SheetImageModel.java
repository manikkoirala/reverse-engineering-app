// 
// Decompiled by Procyon v0.6.0
// 

package com.ekodroid.omrevaluator.database;

import android.graphics.BitmapFactory;
import android.graphics.Bitmap;
import com.ekodroid.omrevaluator.templateui.scanner.SheetDimension;
import com.j256.ormlite.field.DataType;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

@DatabaseTable(tableName = "sheet_image")
public class SheetImageModel
{
    @DatabaseField(canBeNull = false)
    private String className;
    @DatabaseField(canBeNull = false)
    private String examDate;
    @DatabaseField(canBeNull = false)
    private String examName;
    @DatabaseField(allowGeneratedIdInsert = true, generatedId = true)
    private Integer id;
    @DatabaseField
    private String imagePath;
    @DatabaseField(canBeNull = false)
    private boolean isSent;
    @DatabaseField(canBeNull = false)
    private boolean isSmsSent;
    @DatabaseField
    private int pageIndex;
    @DatabaseField(canBeNull = false)
    private int rollNo;
    @DatabaseField
    private String sheetDimension;
    @DatabaseField(dataType = DataType.BYTE_ARRAY)
    private byte[] sheetImage;
    @DatabaseField(canBeNull = false)
    private boolean synced;
    @DatabaseField(canBeNull = false)
    private long uploadId;
    
    public SheetImageModel() {
    }
    
    public SheetImageModel(final String examName, final int rollNo, final byte[] sheetImage, final String className, final String examDate, final SheetDimension sheetDimension, final String imagePath, final int pageIndex) {
        this.examName = examName;
        this.rollNo = rollNo;
        this.sheetImage = sheetImage;
        this.className = className;
        this.examDate = examDate;
        this.sheetDimension = new gc0().s(sheetDimension);
        this.pageIndex = pageIndex;
        this.imagePath = imagePath;
    }
    
    public String getClassName() {
        return this.className;
    }
    
    public String getExamDate() {
        return this.examDate;
    }
    
    public String getExamName() {
        return this.examName;
    }
    
    public Integer getId() {
        return this.id;
    }
    
    public String getImagePath() {
        return this.imagePath;
    }
    
    public int getPageIndex() {
        return this.pageIndex;
    }
    
    public int getRollNo() {
        return this.rollNo;
    }
    
    public SheetDimension getSheetDimension() {
        return (SheetDimension)new gc0().j(this.sheetDimension, SheetDimension.class);
    }
    
    public String getSheetDimensionJson() {
        return this.sheetDimension;
    }
    
    public Bitmap getSheetImage() {
        final byte[] sheetImage = this.sheetImage;
        if (sheetImage != null && sheetImage.length > 10) {
            return BitmapFactory.decodeByteArray(sheetImage, 0, sheetImage.length);
        }
        final String imagePath = this.imagePath;
        if (imagePath != null && imagePath.length() > 5) {
            return BitmapFactory.decodeFile(this.imagePath);
        }
        return null;
    }
    
    public long getUploadId() {
        return this.uploadId;
    }
    
    public boolean isSynced() {
        return this.synced;
    }
    
    public void setExamName(final String examName) {
        this.examName = examName;
    }
    
    public void setId(final Integer id) {
        this.id = id;
    }
    
    public void setImagePath(final String imagePath) {
        this.imagePath = imagePath;
    }
    
    public void setPageIndex(final int pageIndex) {
        this.pageIndex = pageIndex;
    }
    
    public void setRollNo(final int rollNo) {
        this.rollNo = rollNo;
    }
    
    public void setSynced(final boolean synced) {
        this.synced = synced;
    }
    
    public void setUploadId(final long uploadId) {
        this.uploadId = uploadId;
    }
}

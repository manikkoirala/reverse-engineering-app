// 
// Decompiled by Procyon v0.6.0
// 

package com.ekodroid.omrevaluator.database;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

@DatabaseTable(tableName = "tb_sms_template")
public class SmsTemplateDataModel
{
    @DatabaseField(allowGeneratedIdInsert = true, generatedId = true)
    private Integer id;
    @DatabaseField(canBeNull = false)
    private String smsString;
    @DatabaseField(canBeNull = false, unique = true)
    private String smsTemplateName;
    
    public SmsTemplateDataModel() {
    }
    
    public SmsTemplateDataModel(final String smsTemplateName, final String smsString) {
        this.smsString = smsString;
        this.smsTemplateName = smsTemplateName;
    }
    
    public String getSmsString() {
        return this.smsString;
    }
    
    public String getSmsTemplateName() {
        return this.smsTemplateName;
    }
}

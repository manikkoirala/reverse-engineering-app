// 
// Decompiled by Procyon v0.6.0
// 

package com.ekodroid.omrevaluator.database;

import com.ekodroid.omrevaluator.templateui.models.SheetTemplate2;
import com.ekodroid.omrevaluator.templateui.scanner.ResultItem;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;
import java.io.Serializable;

@DatabaseTable(tableName = "result_json")
public class ResultDataJsonModel implements Serializable
{
    @DatabaseField(canBeNull = false)
    private String className;
    @DatabaseField(canBeNull = false)
    private String examDate;
    @DatabaseField(canBeNull = false)
    private String examName;
    @DatabaseField(allowGeneratedIdInsert = true, generatedId = true)
    private Integer id;
    @DatabaseField
    private boolean isEmailSent;
    @DatabaseField
    private boolean isPublished;
    @DatabaseField
    private boolean isSmsSent;
    @DatabaseField
    private boolean isSynced;
    @DatabaseField
    private String resultItem;
    @DatabaseField(canBeNull = false)
    private int rollNo;
    
    public ResultDataJsonModel() {
    }
    
    public ResultDataJsonModel(final String examName, final int rollNo, final String className, final String examDate, final ResultItem resultItem, final boolean isEmailSent, final boolean isSmsSent) {
        this.examName = examName;
        this.rollNo = rollNo;
        this.examDate = examDate;
        this.className = className;
        this.resultItem = new gc0().s(resultItem);
        this.isEmailSent = isEmailSent;
        this.isSmsSent = isSmsSent;
    }
    
    public String getClassName() {
        return this.className;
    }
    
    public String getExamDate() {
        return this.examDate;
    }
    
    public String getExamName() {
        return this.examName;
    }
    
    public Integer getId() {
        return this.id;
    }
    
    public ResultItem getResultItem(final SheetTemplate2 sheetTemplate2) {
        final ResultItem resultItem = (ResultItem)new gc0().j(this.resultItem, ResultItem.class);
        if (resultItem.getSectionsInSubjects() == null) {
            resultItem.setSectionsInSubjects(sheetTemplate2.getSectionsInSubject());
        }
        ve1.w(resultItem, sheetTemplate2);
        return resultItem;
    }
    
    public String getResultItemString() {
        return this.resultItem;
    }
    
    public int getRollNo() {
        return this.rollNo;
    }
    
    public boolean isPublished() {
        return this.isPublished;
    }
    
    public boolean isSmsSent() {
        return this.isSmsSent;
    }
    
    public boolean isSynced() {
        return this.isSynced;
    }
    
    public void setExamName(final String examName) {
        this.examName = examName;
    }
    
    public void setId(final Integer id) {
        this.id = id;
    }
    
    public void setPublished(final boolean isPublished) {
        this.isPublished = isPublished;
    }
    
    public void setResultItem(final ResultItem resultItem) {
        this.resultItem = new gc0().s(resultItem);
    }
    
    public void setRollNo(final int rollNo) {
        this.rollNo = rollNo;
    }
    
    public void setSmsSent(final boolean isSmsSent) {
        this.isSmsSent = isSmsSent;
    }
    
    public void setSynced(final boolean isSynced) {
        this.isSynced = isSynced;
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.ekodroid.omrevaluator.database;

import com.ekodroid.omrevaluator.templateui.models.LabelProfile;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

@DatabaseTable(tableName = "tb_label")
public class LabelProfileJsonModel
{
    @DatabaseField(allowGeneratedIdInsert = true, generatedId = true)
    private Integer id;
    @DatabaseField(canBeNull = false)
    private String labelProfileJson;
    @DatabaseField(canBeNull = false)
    private String profileName;
    
    public LabelProfileJsonModel() {
    }
    
    public LabelProfileJsonModel(final String profileName, final LabelProfile labelProfile) {
        this.labelProfileJson = new gc0().s(labelProfile);
        this.profileName = profileName;
    }
    
    public Integer getId() {
        return this.id;
    }
    
    public LabelProfile getLabelProfile() {
        return (LabelProfile)new gc0().j(this.labelProfileJson, LabelProfile.class);
    }
    
    public String getProfileName() {
        return this.profileName;
    }
    
    public void setId(final Integer id) {
        this.id = id;
    }
    
    public void setLabelProfile(final LabelProfile labelProfile) {
        this.labelProfileJson = new gc0().s(labelProfile);
    }
    
    public void setProfileName(final String profileName) {
        this.profileName = profileName;
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.ekodroid.omrevaluator.database;

import com.google.firebase.auth.FirebaseAuth;
import android.database.SQLException;
import android.util.Log;
import com.j256.ormlite.table.TableUtils;
import com.j256.ormlite.support.ConnectionSource;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteDatabase$CursorFactory;
import android.content.Context;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.android.apptools.OrmLiteSqliteOpenHelper;

public class DatabaseHelper extends OrmLiteSqliteOpenHelper
{
    private static final String DATABASE_NAME = "templates_database.db";
    private static final int DATABASE_VERSION = 28;
    private static String TAG = "TAG_OMR";
    private static DatabaseHelper instance;
    private String DEFALUT_CLASS;
    private Dao<ArchivedLocalDataModel, String> archivedLocalDataModelDao;
    private Dao<ClassDataModel, String> classJsonDAO;
    private Dao<FileDataModel, Integer> fileDAO;
    private Dao<TemplateHeaderJsonDataModel, Integer> headerProfileJsonDAO;
    private Dao<LabelProfileJsonModel, Integer> labelProfileJsonDAO;
    private Dao<ResultDataJsonModel, Integer> resultJsonDAO;
    private Dao<SheetImageModel, Integer> sheetImageDAO;
    private Dao<SmsTemplateDataModel, Integer> smsTemplateDAO;
    private Dao<StudentDataModel, Integer> studentDAO;
    private Dao<TemplateDataJsonModel, Integer> templateJsonDAO;
    
    private DatabaseHelper(final Context context) {
        super(context, "templates_database.db", (SQLiteDatabase$CursorFactory)null, 28);
        this.DEFALUT_CLASS = "Default";
        this.sheetImageDAO = null;
        this.studentDAO = null;
        this.fileDAO = null;
        this.templateJsonDAO = null;
        this.resultJsonDAO = null;
        this.classJsonDAO = null;
        this.labelProfileJsonDAO = null;
        this.smsTemplateDAO = null;
        this.headerProfileJsonDAO = null;
        this.archivedLocalDataModelDao = null;
    }
    
    public static DatabaseHelper getInstance(final Context context) {
        init(context);
        return DatabaseHelper.instance;
    }
    
    private static void init(final Context context) {
        if (DatabaseHelper.instance == null) {
            DatabaseHelper.instance = new DatabaseHelper(context);
        }
    }
    
    public Dao<ArchivedLocalDataModel, String> getArchiveLocalDAO() {
        if (this.archivedLocalDataModelDao == null) {
            try {
                this.archivedLocalDataModelDao = (Dao<ArchivedLocalDataModel, String>)this.getDao((Class)ArchivedLocalDataModel.class);
            }
            catch (final Exception ex) {
                ex.printStackTrace();
            }
        }
        return this.archivedLocalDataModelDao;
    }
    
    public Dao<ClassDataModel, String> getClassDAO() {
        if (this.classJsonDAO == null) {
            try {
                this.classJsonDAO = (Dao<ClassDataModel, String>)this.getDao((Class)ClassDataModel.class);
            }
            catch (final Exception ex) {
                ex.printStackTrace();
            }
        }
        return this.classJsonDAO;
    }
    
    public Dao<ClassDataModel, String> getClassJsonDAO() {
        if (this.classJsonDAO == null) {
            try {
                this.classJsonDAO = (Dao<ClassDataModel, String>)this.getDao((Class)ClassDataModel.class);
            }
            catch (final Exception ex) {
                ex.printStackTrace();
            }
        }
        return this.classJsonDAO;
    }
    
    public Dao<FileDataModel, Integer> getFileDAO() {
        if (this.fileDAO == null) {
            try {
                this.fileDAO = (Dao<FileDataModel, Integer>)this.getDao((Class)FileDataModel.class);
            }
            catch (final Exception ex) {
                ex.printStackTrace();
            }
        }
        return this.fileDAO;
    }
    
    public Dao<TemplateHeaderJsonDataModel, Integer> getHeaderProfileJsonDAO() {
        if (this.headerProfileJsonDAO == null) {
            try {
                this.headerProfileJsonDAO = (Dao<TemplateHeaderJsonDataModel, Integer>)this.getDao((Class)TemplateHeaderJsonDataModel.class);
            }
            catch (final Exception ex) {
                ex.printStackTrace();
            }
        }
        return this.headerProfileJsonDAO;
    }
    
    public Dao<LabelProfileJsonModel, Integer> getLabelProfileJsonDAO() {
        if (this.labelProfileJsonDAO == null) {
            try {
                this.labelProfileJsonDAO = (Dao<LabelProfileJsonModel, Integer>)this.getDao((Class)LabelProfileJsonModel.class);
            }
            catch (final Exception ex) {
                ex.printStackTrace();
            }
        }
        return this.labelProfileJsonDAO;
    }
    
    public Dao<ResultDataJsonModel, Integer> getResultJsonDAO() {
        if (this.resultJsonDAO == null) {
            try {
                this.resultJsonDAO = (Dao<ResultDataJsonModel, Integer>)this.getDao((Class)ResultDataJsonModel.class);
            }
            catch (final Exception ex) {
                ex.printStackTrace();
            }
        }
        return this.resultJsonDAO;
    }
    
    public Dao<SheetImageModel, Integer> getSheetImageDAO() {
        if (this.sheetImageDAO == null) {
            try {
                this.sheetImageDAO = (Dao<SheetImageModel, Integer>)this.getDao((Class)SheetImageModel.class);
            }
            catch (final Exception ex) {
                ex.printStackTrace();
            }
        }
        return this.sheetImageDAO;
    }
    
    public Dao<SmsTemplateDataModel, Integer> getSmsTemplateDAO() {
        if (this.smsTemplateDAO == null) {
            try {
                this.smsTemplateDAO = (Dao<SmsTemplateDataModel, Integer>)this.getDao((Class)SmsTemplateDataModel.class);
            }
            catch (final Exception ex) {
                ex.printStackTrace();
            }
        }
        return this.smsTemplateDAO;
    }
    
    public Dao<StudentDataModel, Integer> getStudentDAO() {
        if (this.studentDAO == null) {
            try {
                this.studentDAO = (Dao<StudentDataModel, Integer>)this.getDao((Class)StudentDataModel.class);
            }
            catch (final Exception ex) {
                ex.printStackTrace();
            }
        }
        return this.studentDAO;
    }
    
    public Dao<TemplateDataJsonModel, Integer> getTemplateJsonDAO() {
        if (this.templateJsonDAO == null) {
            try {
                this.templateJsonDAO = (Dao<TemplateDataJsonModel, Integer>)this.getDao((Class)TemplateDataJsonModel.class);
            }
            catch (final Exception ex) {
                ex.printStackTrace();
            }
        }
        return this.templateJsonDAO;
    }
    
    public void onCreate(final SQLiteDatabase sqLiteDatabase, final ConnectionSource connectionSource) {
        try {
            TableUtils.createTableIfNotExists(connectionSource, (Class)SheetImageModel.class);
            TableUtils.createTableIfNotExists(connectionSource, (Class)StudentDataModel.class);
            TableUtils.createTableIfNotExists(connectionSource, (Class)FileDataModel.class);
            TableUtils.createTableIfNotExists(connectionSource, (Class)TemplateDataJsonModel.class);
            TableUtils.createTableIfNotExists(connectionSource, (Class)ResultDataJsonModel.class);
            TableUtils.createTableIfNotExists(connectionSource, (Class)ClassDataModel.class);
            TableUtils.createTableIfNotExists(connectionSource, (Class)LabelProfileJsonModel.class);
            TableUtils.createTableIfNotExists(connectionSource, (Class)SmsTemplateDataModel.class);
            TableUtils.createTableIfNotExists(connectionSource, (Class)TemplateHeaderJsonDataModel.class);
            TableUtils.createTableIfNotExists(connectionSource, (Class)ArchivedLocalDataModel.class);
            Log.d(DatabaseHelper.TAG, "table class created");
            goto Label_0087;
        }
        catch (final Exception ex) {
            ex.printStackTrace();
        }
        catch (final SQLException cause) {
            ((Throwable)cause).printStackTrace();
            throw new RuntimeException((Throwable)cause);
        }
    }
    
    public void onUpgrade(final SQLiteDatabase sqLiteDatabase, final ConnectionSource connectionSource, final int n, final int n2) {
        switch (n) {
            case 22: {
                try {
                    TableUtils.createTableIfNotExists(connectionSource, (Class)TemplateDataJsonModel.class);
                    final Dao<TemplateDataJsonModel, Integer> templateJsonDAO = this.getTemplateJsonDAO();
                    templateJsonDAO.executeRaw("ALTER TABLE 'tb_templates_v2' ADD COLUMN 'isCloudSyncOn' boolean NOT NULL default 0;", new String[0]);
                    templateJsonDAO.executeRaw("ALTER TABLE 'tb_templates_v2' ADD COLUMN 'isSynced' boolean NOT NULL default 0;", new String[0]);
                    templateJsonDAO.executeRaw("ALTER TABLE 'tb_templates_v2' ADD COLUMN 'isPublished' boolean NOT NULL default 0;", new String[0]);
                    templateJsonDAO.executeRaw("ALTER TABLE 'tb_templates_v2' ADD COLUMN 'cloudId' INTEGER;", new String[0]);
                    templateJsonDAO.executeRaw("ALTER TABLE 'tb_templates_v2' ADD COLUMN 'lastUpdatedOn' INTEGER;", new String[0]);
                    templateJsonDAO.executeRaw("ALTER TABLE 'tb_templates_v2' ADD COLUMN 'lastSyncedOn' INTEGER;", new String[0]);
                }
                catch (final Exception ex) {
                    ex.printStackTrace();
                }
                try {
                    TableUtils.createTableIfNotExists(connectionSource, (Class)ResultDataJsonModel.class);
                    final Dao<ResultDataJsonModel, Integer> resultJsonDAO = this.getResultJsonDAO();
                    resultJsonDAO.executeRaw("ALTER TABLE 'result_json' ADD COLUMN 'isSynced' boolean NOT NULL default 0;", new String[0]);
                    resultJsonDAO.executeRaw("ALTER TABLE 'result_json' ADD COLUMN 'isPublished' boolean NOT NULL default 0;", new String[0]);
                }
                catch (final Exception ex2) {
                    ex2.printStackTrace();
                }
            }
            case 23: {
                try {
                    TableUtils.createTableIfNotExists(connectionSource, (Class)TemplateDataJsonModel.class);
                    final Dao<TemplateDataJsonModel, Integer> templateJsonDAO2 = this.getTemplateJsonDAO();
                    templateJsonDAO2.executeRaw("ALTER TABLE 'tb_templates_v2' ADD COLUMN 'sharedType' TEXT;", new String[0]);
                    templateJsonDAO2.executeRaw("ALTER TABLE 'tb_templates_v2' ADD COLUMN 'userId' TEXT;", new String[0]);
                    final String o = FirebaseAuth.getInstance().e().O();
                    final StringBuilder sb = new StringBuilder();
                    sb.append("UPDATE tb_templates_v2 SET sharedType='PUBLIC', userId='");
                    sb.append(o);
                    sb.append("' WHERE isCloudSyncOn=1;");
                    templateJsonDAO2.executeRaw(sb.toString(), new String[0]);
                }
                catch (final Exception ex3) {
                    ex3.printStackTrace();
                }
            }
            case 24: {
                try {
                    this.getStudentDAO().executeRaw("ALTER TABLE 'tb_students_v2' ADD COLUMN 'isSynced' boolean NOT NULL default 0;", new String[0]);
                }
                catch (final Exception ex4) {
                    ex4.printStackTrace();
                }
            }
            case 25: {
                try {
                    TableUtils.createTableIfNotExists(connectionSource, (Class)TemplateDataJsonModel.class);
                    this.getTemplateJsonDAO().executeRaw("ALTER TABLE 'tb_templates_v2' ADD COLUMN 'syncImages' boolean NOT NULL default 0;", new String[0]);
                }
                catch (final Exception ex5) {
                    ex5.printStackTrace();
                }
                try {
                    TableUtils.createTableIfNotExists(connectionSource, (Class)SheetImageModel.class);
                    this.getSheetImageDAO().executeRaw("ALTER TABLE 'sheet_image' ADD COLUMN 'synced' boolean NOT NULL default 0;", new String[0]);
                }
                catch (final Exception ex6) {
                    ex6.printStackTrace();
                }
            }
            case 26: {
                try {
                    TableUtils.createTableIfNotExists(connectionSource, (Class)SheetImageModel.class);
                    this.getSheetImageDAO().executeRaw("ALTER TABLE 'sheet_image' ADD COLUMN 'uploadId' INTEGER;", new String[0]);
                }
                catch (final Exception ex7) {
                    ex7.printStackTrace();
                }
            }
            case 27: {
                try {
                    TableUtils.createTableIfNotExists(connectionSource, (Class)ClassDataModel.class);
                }
                catch (final Exception ex8) {
                    ex8.printStackTrace();
                }
                break;
            }
            case 1:
            case 2:
            case 3:
            case 4:
            case 5:
            case 6:
            case 7:
            case 8:
            case 9:
            case 10:
            case 11:
            case 12:
            case 13:
            case 14:
            case 15:
            case 16:
            case 17:
            case 18:
            case 19:
            case 20:
            case 21: {
                try {
                    TableUtils.dropTable(connectionSource, (Class)SheetImageModel.class, true);
                    TableUtils.dropTable(connectionSource, (Class)StudentDataModel.class, true);
                    TableUtils.dropTable(connectionSource, (Class)FileDataModel.class, true);
                    TableUtils.dropTable(connectionSource, (Class)TemplateDataJsonModel.class, true);
                    TableUtils.dropTable(connectionSource, (Class)ResultDataJsonModel.class, true);
                    TableUtils.dropTable(connectionSource, (Class)ClassDataModel.class, true);
                    TableUtils.dropTable(connectionSource, (Class)LabelProfileJsonModel.class, true);
                    TableUtils.dropTable(connectionSource, (Class)SmsTemplateDataModel.class, true);
                    TableUtils.dropTable(connectionSource, (Class)TemplateHeaderJsonDataModel.class, true);
                    TableUtils.dropTable(connectionSource, (Class)ArchivedLocalDataModel.class, true);
                    TableUtils.createTableIfNotExists(connectionSource, (Class)SheetImageModel.class);
                    TableUtils.createTableIfNotExists(connectionSource, (Class)StudentDataModel.class);
                    TableUtils.createTableIfNotExists(connectionSource, (Class)FileDataModel.class);
                    TableUtils.createTableIfNotExists(connectionSource, (Class)TemplateDataJsonModel.class);
                    TableUtils.createTableIfNotExists(connectionSource, (Class)ResultDataJsonModel.class);
                    TableUtils.createTableIfNotExists(connectionSource, (Class)ClassDataModel.class);
                    TableUtils.createTableIfNotExists(connectionSource, (Class)LabelProfileJsonModel.class);
                    TableUtils.createTableIfNotExists(connectionSource, (Class)SmsTemplateDataModel.class);
                    TableUtils.createTableIfNotExists(connectionSource, (Class)TemplateHeaderJsonDataModel.class);
                    TableUtils.createTableIfNotExists(connectionSource, (Class)ArchivedLocalDataModel.class);
                }
                catch (final Exception ex9) {
                    ex9.printStackTrace();
                }
                break;
            }
        }
    }
}

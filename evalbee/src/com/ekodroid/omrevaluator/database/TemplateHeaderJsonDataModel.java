// 
// Decompiled by Procyon v0.6.0
// 

package com.ekodroid.omrevaluator.database;

import com.ekodroid.omrevaluator.templateui.models.HeaderProfile;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

@DatabaseTable(tableName = "tb_template_header")
public class TemplateHeaderJsonDataModel
{
    @DatabaseField(id = true)
    private String headerName;
    @DatabaseField(canBeNull = false)
    private String headerProfile;
    
    public TemplateHeaderJsonDataModel() {
    }
    
    public TemplateHeaderJsonDataModel(final String headerName, final HeaderProfile headerProfile) {
        this.headerProfile = new gc0().s(headerProfile);
        this.headerName = headerName;
    }
    
    public String getHeaderName() {
        return this.headerName;
    }
    
    public HeaderProfile getHeaderProfile() {
        return (HeaderProfile)new gc0().j(this.headerProfile, HeaderProfile.class);
    }
}

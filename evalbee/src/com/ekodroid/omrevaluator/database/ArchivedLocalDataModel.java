// 
// Decompiled by Procyon v0.6.0
// 

package com.ekodroid.omrevaluator.database;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;
import java.io.Serializable;

@DatabaseTable(tableName = "tb_archived_exam_v1")
public class ArchivedLocalDataModel implements Serializable
{
    @DatabaseField(canBeNull = false)
    private String archivedOn;
    @DatabaseField(canBeNull = false)
    private int attendees;
    @DatabaseField(canBeNull = false)
    private String className;
    @DatabaseField(canBeNull = false)
    private String examDate;
    @DatabaseField(canBeNull = false)
    private String name;
    @DatabaseField(canBeNull = false)
    private int noOfQue;
    @DatabaseField(canBeNull = false)
    private int sizeKb;
    @DatabaseField(canBeNull = false, id = true)
    private String templateId;
    @DatabaseField(canBeNull = false)
    private int version;
    
    public ArchivedLocalDataModel() {
    }
    
    public ArchivedLocalDataModel(final String templateId, final String name, final String className, final String examDate, final String archivedOn, final int sizeKb, final int version, final int attendees, final int noOfQue) {
        this.templateId = templateId;
        this.name = name;
        this.className = className;
        this.examDate = examDate;
        this.archivedOn = archivedOn;
        this.sizeKb = sizeKb;
        this.version = version;
        this.attendees = attendees;
        this.noOfQue = noOfQue;
    }
    
    public String getArchivedOn() {
        return this.archivedOn;
    }
    
    public int getAttendees() {
        return this.attendees;
    }
    
    public String getClassName() {
        return this.className;
    }
    
    public String getExamDate() {
        return this.examDate;
    }
    
    public String getName() {
        return this.name;
    }
    
    public int getNoOfQue() {
        return this.noOfQue;
    }
    
    public int getSizeKb() {
        return this.sizeKb;
    }
    
    public String getTemplateId() {
        return this.templateId;
    }
    
    public int getVersion() {
        return this.version;
    }
    
    public void setArchivedOn(final String archivedOn) {
        this.archivedOn = archivedOn;
    }
    
    public void setAttendees(final int attendees) {
        this.attendees = attendees;
    }
    
    public void setClassName(final String className) {
        this.className = className;
    }
    
    public void setExamDate(final String examDate) {
        this.examDate = examDate;
    }
    
    public void setName(final String name) {
        this.name = name;
    }
    
    public void setNoOfQue(final int noOfQue) {
        this.noOfQue = noOfQue;
    }
    
    public void setSizeKb(final int sizeKb) {
        this.sizeKb = sizeKb;
    }
    
    public void setTemplateId(final String templateId) {
        this.templateId = templateId;
    }
    
    public void setVersion(final int version) {
        this.version = version;
    }
}

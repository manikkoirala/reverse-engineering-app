// 
// Decompiled by Procyon v0.6.0
// 

package com.ekodroid.omrevaluator.database;

import com.j256.ormlite.field.DataType;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

@DatabaseTable(tableName = "file_data")
public class FileDataModel
{
    @DatabaseField(dataType = DataType.BYTE_ARRAY)
    private byte[] fileData;
    @DatabaseField(canBeNull = false, id = true)
    private String fileName;
    
    public FileDataModel() {
    }
    
    public FileDataModel(final String fileName, final byte[] fileData) {
        this.fileName = fileName;
        this.fileData = fileData;
    }
    
    public byte[] getFileData() {
        return this.fileData;
    }
    
    public String getFileName() {
        return this.fileName;
    }
    
    public void setFileData(final byte[] fileData) {
        this.fileData = fileData;
    }
    
    public void setFileName(final String fileName) {
        this.fileName = fileName;
    }
}

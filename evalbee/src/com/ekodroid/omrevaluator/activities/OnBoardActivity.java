// 
// Decompiled by Procyon v0.6.0
// 

package com.ekodroid.omrevaluator.activities;

import android.content.Context;
import android.content.Intent;
import android.view.View;
import android.view.View$OnClickListener;
import android.os.Bundle;

public class OnBoardActivity extends v5
{
    @Override
    public void onCreate(final Bundle bundle) {
        super.onCreate(bundle);
        this.setContentView(2131492916);
        this.findViewById(2131296455).setOnClickListener((View$OnClickListener)new View$OnClickListener(this) {
            public final OnBoardActivity a;
            
            public void onClick(final View view) {
                ((Context)this.a).getApplicationContext().getSharedPreferences("MyPref", 0).edit().putBoolean("first_time_user", false).commit();
                ((Context)this.a).startActivity(new Intent(((Context)this.a).getApplicationContext(), (Class)SplashActivity.class));
                this.a.finish();
            }
        });
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.ekodroid.omrevaluator.activities;

import android.content.Context;
import com.google.android.material.navigation.NavigationBarView;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.q;
import android.view.MenuItem;
import android.content.Intent;
import com.ekodroid.omrevaluator.activities.Services.SyncSerivice;
import android.os.Bundle;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.android.material.navigation.NavigationBarView$OnItemSelectedListener;

public class HomeBottomNavActivity extends v5 implements NavigationBarView$OnItemSelectedListener
{
    public BottomNavigationView c;
    public f2 d;
    public wg e;
    public zw0 f;
    
    public HomeBottomNavActivity() {
        this.d = new f2();
        this.e = new wg();
        this.f = new zw0();
    }
    
    public void onCreate(final Bundle bundle) {
        super.onCreate(bundle);
        this.setContentView(2131492910);
        ((NavigationBarView)(this.c = (BottomNavigationView)this.findViewById(2131296378))).setOnItemSelectedListener((NavigationBarView$OnItemSelectedListener)this);
        ((NavigationBarView)this.c).setSelectedItemId(2131296913);
        y3.c(((Context)this).getApplicationContext());
        ((Context)this).startService(new Intent(((Context)this).getApplicationContext(), (Class)SyncSerivice.class));
    }
    
    public boolean onNavigationItemSelected(final MenuItem menuItem) {
        q q = null;
        Fragment fragment = null;
        switch (menuItem.getItemId()) {
            default: {
                return false;
            }
            case 2131296915: {
                q = this.getSupportFragmentManager().m();
                fragment = this.e;
                break;
            }
            case 2131296914: {
                q = this.getSupportFragmentManager().m();
                fragment = this.f;
                break;
            }
            case 2131296913: {
                q = this.getSupportFragmentManager().m();
                fragment = this.d;
                break;
            }
        }
        q.o(2131296564, fragment).g();
        return true;
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.ekodroid.omrevaluator.activities;

import androidx.appcompat.app.a;
import android.widget.TextView;
import android.view.View$OnClickListener;
import android.os.Bundle;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import android.content.DialogInterface;
import android.content.DialogInterface$OnClickListener;
import com.google.android.material.dialog.MaterialAlertDialogBuilder;
import android.content.Intent;
import android.app.Activity;
import android.content.Context;
import com.google.android.gms.tasks.Task;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import android.text.TextUtils;
import com.google.android.material.textfield.TextInputLayout;
import android.widget.Button;
import android.widget.LinearLayout;
import com.google.firebase.auth.FirebaseAuth;
import android.view.View;
import android.widget.EditText;
import android.content.SharedPreferences;

public class LoginActivity extends v5
{
    public SharedPreferences c;
    public LoginActivity d;
    public EditText e;
    public View f;
    public FirebaseAuth g;
    public LinearLayout h;
    public Button i;
    public TextInputLayout j;
    
    public LoginActivity() {
        this.d = this;
    }
    
    public static /* synthetic */ EditText D(final LoginActivity loginActivity) {
        return loginActivity.e;
    }
    
    public static /* synthetic */ LinearLayout E(final LoginActivity loginActivity) {
        return loginActivity.h;
    }
    
    public static /* synthetic */ Button F(final LoginActivity loginActivity) {
        return loginActivity.i;
    }
    
    public static /* synthetic */ FirebaseAuth H(final LoginActivity loginActivity) {
        return loginActivity.g;
    }
    
    public final void J() {
        final EditText e = this.e;
        Object e2 = null;
        ((TextView)e).setError((CharSequence)null);
        final String string = this.e.getText().toString();
        boolean b = false;
        Label_0078: {
            EditText editText;
            int n;
            if (TextUtils.isEmpty((CharSequence)string)) {
                editText = this.e;
                n = 2131886310;
            }
            else {
                if (this.M(string)) {
                    b = false;
                    break Label_0078;
                }
                editText = this.e;
                n = 2131886271;
            }
            ((TextView)editText).setError((CharSequence)((Context)this).getString(n));
            e2 = this.e;
            b = true;
        }
        if (b) {
            ((View)e2).requestFocus();
        }
        else {
            this.P(true);
            ((View)this.e).setEnabled(false);
            ((View)this.i).setEnabled(false);
            ((View)this.h).setEnabled(false);
            this.c.edit().putString("uId", string).commit();
            this.N(string);
        }
    }
    
    public final void K(final GoogleSignInAccount googleSignInAccount) {
        this.P(true);
        ((View)this.i).setEnabled(false);
        this.g.l(mb0.a(googleSignInAccount.getIdToken(), null)).addOnCompleteListener((Activity)this, (OnCompleteListener)new OnCompleteListener(this) {
            public final LoginActivity a;
            
            public void onComplete(final Task task) {
                if (task.isSuccessful()) {
                    if (LoginActivity.H(this.a).e() != null) {
                        a91.G((Context)this.a.d, 2131886833, 2131230927, 2131231085);
                        this.a.L();
                    }
                }
                else {
                    a91.G((Context)this.a.d, 2131886831, 2131230909, 2131231086);
                }
                ((View)LoginActivity.F(this.a)).setEnabled(true);
                this.a.P(false);
            }
        });
    }
    
    public final void L() {
        a91.b(((Context)this).getApplicationContext());
        new bw((Context)this, false, new y01(this) {
            public final LoginActivity a;
            
            @Override
            public void a(final Object o) {
                ((Context)this.a).startActivity(new Intent(((Context)this.a).getApplicationContext(), (Class)HomeBottomNavActivity.class));
                this.a.finish();
            }
        }).show();
    }
    
    public final boolean M(final String s) {
        final int length = s.length();
        boolean b = true;
        if (length <= 1) {
            b = false;
        }
        return b;
    }
    
    public final void N(final String s) {
        this.g.j(s, z1.R().e("https://evalbee.com/finishSignUp").c(true).d("com.ekodroid.omrevaluator").b("com.ekodroid.omrevaluator", true, "0").a()).addOnCompleteListener((OnCompleteListener)new OnCompleteListener(this) {
            public final LoginActivity a;
            
            public void onComplete(final Task task) {
                LoginActivity loginActivity;
                int n;
                if (task.isSuccessful()) {
                    loginActivity = this.a;
                    n = 2131886520;
                }
                else {
                    loginActivity = this.a;
                    n = 2131886393;
                }
                loginActivity.O(n);
                ((View)LoginActivity.D(this.a)).setEnabled(true);
                ((View)LoginActivity.E(this.a)).setEnabled(true);
                ((View)LoginActivity.F(this.a)).setEnabled(true);
                this.a.P(false);
            }
        });
    }
    
    public final void O(final int message) {
        final MaterialAlertDialogBuilder materialAlertDialogBuilder = new MaterialAlertDialogBuilder((Context)this, 2131951953);
        materialAlertDialogBuilder.setMessage(message).setCancelable(false).setPositiveButton(2131886679, (DialogInterface$OnClickListener)new DialogInterface$OnClickListener(this) {
            public final LoginActivity a;
            
            public void onClick(final DialogInterface dialogInterface, final int n) {
                dialogInterface.dismiss();
            }
        }).create();
        ((a.a)materialAlertDialogBuilder).show();
    }
    
    public final void P(final boolean b) {
        final View f = this.f;
        int visibility;
        if (b) {
            visibility = 0;
        }
        else {
            visibility = 4;
        }
        f.setVisibility(visibility);
    }
    
    public final void Q() {
        this.startActivityForResult(GoogleSignIn.getClient(this, new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN).requestIdToken(((Context)this).getString(2131886229)).requestEmail().build()).getSignInIntent(), 1);
        this.P(true);
    }
    
    @Override
    public void onActivityResult(final int n, final int n2, final Intent intent) {
        super.onActivityResult(n, n2, intent);
        this.P(false);
        if (n == 1) {
            final Task<GoogleSignInAccount> signedInAccountFromIntent = GoogleSignIn.getSignedInAccountFromIntent(intent);
            try {
                this.K((GoogleSignInAccount)signedInAccountFromIntent.getResult((Class)ApiException.class));
            }
            catch (final ApiException ex) {
                ex.printStackTrace();
            }
        }
    }
    
    @Override
    public void onCreate(final Bundle bundle) {
        super.onCreate(bundle);
        this.setContentView(2131492915);
        this.g = FirebaseAuth.getInstance();
        final TextInputLayout j = (TextInputLayout)this.findViewById(2131297173);
        this.j = j;
        this.e = j.getEditText();
        final SharedPreferences sharedPreferences = ((Context)this).getApplicationContext().getSharedPreferences("MyPref", 0);
        this.c = sharedPreferences;
        ((TextView)this.e).setText((CharSequence)sharedPreferences.getString("uId", ""));
        this.h = (LinearLayout)this.findViewById(2131296803);
        this.i = (Button)this.findViewById(2131296452);
        ((View)this.h).setOnClickListener((View$OnClickListener)new View$OnClickListener(this) {
            public final LoginActivity a;
            
            public void onClick(final View view) {
                this.a.Q();
            }
        });
        ((View)this.i).setOnClickListener((View$OnClickListener)new View$OnClickListener(this) {
            public final LoginActivity a;
            
            public void onClick(final View view) {
                this.a.J();
            }
        });
        this.f = this.findViewById(2131296851);
        this.P(false);
    }
}

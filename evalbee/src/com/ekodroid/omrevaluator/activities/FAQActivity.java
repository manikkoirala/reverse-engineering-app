// 
// Decompiled by Procyon v0.6.0
// 

package com.ekodroid.omrevaluator.activities;

import android.view.View;
import android.view.View$OnClickListener;
import android.os.Bundle;
import android.widget.TextView;

public class FAQActivity extends v5
{
    public TextView c;
    public TextView d;
    public TextView e;
    public TextView f;
    public TextView g;
    public TextView h;
    public TextView i;
    public TextView j;
    public boolean k;
    public boolean l;
    public boolean m;
    public boolean n;
    
    @Override
    public void onCreate(final Bundle bundle) {
        super.onCreate(bundle);
        this.setContentView(2131492907);
        this.c = (TextView)this.findViewById(2131296607);
        this.g = (TextView)this.findViewById(2131296358);
        this.d = (TextView)this.findViewById(2131296608);
        this.h = (TextView)this.findViewById(2131296359);
        this.e = (TextView)this.findViewById(2131296609);
        this.i = (TextView)this.findViewById(2131296360);
        this.f = (TextView)this.findViewById(2131296610);
        this.j = (TextView)this.findViewById(2131296361);
        ((View)this.c).setOnClickListener((View$OnClickListener)new View$OnClickListener(this) {
            public final FAQActivity a;
            
            public void onClick(final View view) {
                final FAQActivity a = this.a;
                final boolean k = a.k;
                final TextView g = a.g;
                int visibility;
                if (k) {
                    visibility = 8;
                }
                else {
                    visibility = 0;
                }
                ((View)g).setVisibility(visibility);
                final FAQActivity a2 = this.a;
                a2.k ^= true;
            }
        });
        ((View)this.d).setOnClickListener((View$OnClickListener)new View$OnClickListener(this) {
            public final FAQActivity a;
            
            public void onClick(final View view) {
                final FAQActivity a = this.a;
                final boolean l = a.l;
                final TextView h = a.h;
                int visibility;
                if (l) {
                    visibility = 8;
                }
                else {
                    visibility = 0;
                }
                ((View)h).setVisibility(visibility);
                final FAQActivity a2 = this.a;
                a2.l ^= true;
            }
        });
        ((View)this.e).setOnClickListener((View$OnClickListener)new View$OnClickListener(this) {
            public final FAQActivity a;
            
            public void onClick(final View view) {
                final FAQActivity a = this.a;
                final boolean m = a.m;
                final TextView i = a.i;
                int visibility;
                if (m) {
                    visibility = 8;
                }
                else {
                    visibility = 0;
                }
                ((View)i).setVisibility(visibility);
                final FAQActivity a2 = this.a;
                a2.m ^= true;
            }
        });
        ((View)this.f).setOnClickListener((View$OnClickListener)new View$OnClickListener(this) {
            public final FAQActivity a;
            
            public void onClick(final View view) {
                final FAQActivity a = this.a;
                final boolean n = a.n;
                final TextView j = a.j;
                int visibility;
                if (n) {
                    visibility = 8;
                }
                else {
                    visibility = 0;
                }
                ((View)j).setVisibility(visibility);
                final FAQActivity a2 = this.a;
                a2.n ^= true;
            }
        });
    }
}

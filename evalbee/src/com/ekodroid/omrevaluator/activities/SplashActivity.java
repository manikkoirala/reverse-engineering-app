// 
// Decompiled by Procyon v0.6.0
// 

package com.ekodroid.omrevaluator.activities;

import java.lang.reflect.Field;
import android.database.CursorWindow;
import android.os.Build$VERSION;
import android.content.pm.PackageManager$NameNotFoundException;
import android.provider.Settings$Secure;
import android.content.ActivityNotFoundException;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import androidx.appcompat.app.a;
import android.view.View$OnClickListener;
import android.widget.Button;
import android.view.ViewGroup;
import android.view.LayoutInflater;
import android.content.DialogInterface;
import android.content.DialogInterface$OnClickListener;
import com.google.android.material.dialog.MaterialAlertDialogBuilder;
import java.util.Date;
import java.util.Calendar;
import android.content.Context;
import android.content.Intent;
import com.google.firebase.auth.FirebaseAuth;
import android.content.SharedPreferences;
import com.google.firebase.analytics.FirebaseAnalytics;
import android.os.Handler;
import java.text.SimpleDateFormat;

public class SplashActivity extends v5
{
    public SimpleDateFormat c;
    public Handler d;
    public FirebaseAnalytics e;
    public SplashActivity f;
    public int g;
    public String h;
    public SharedPreferences i;
    public boolean j;
    public Runnable k;
    public String l;
    public y01 m;
    
    public SplashActivity() {
        this.d = new Handler();
        this.f = this;
        this.g = 999;
        this.h = "";
        this.j = false;
        this.k = new Runnable(this) {
            public final SplashActivity a;
            
            @Override
            public void run() {
                Intent intent;
                if (FirebaseAuth.getInstance().e() != null) {
                    intent = new Intent(((Context)this.a).getApplicationContext(), (Class)HomeBottomNavActivity.class);
                }
                else {
                    intent = new Intent(((Context)this.a).getApplicationContext(), (Class)LoginActivity.class);
                }
                ((Context)this.a).startActivity(intent);
                this.a.finish();
            }
        };
        this.l = "";
    }
    
    public static /* synthetic */ SharedPreferences E(final SplashActivity splashActivity) {
        return splashActivity.i;
    }
    
    public static /* synthetic */ int F(final SplashActivity splashActivity) {
        return splashActivity.g;
    }
    
    public static /* synthetic */ String G(final SplashActivity splashActivity) {
        return splashActivity.h;
    }
    
    public static /* synthetic */ boolean I(final SplashActivity splashActivity) {
        return splashActivity.j;
    }
    
    public static /* synthetic */ String J(final SplashActivity splashActivity) {
        return splashActivity.l;
    }
    
    public static /* synthetic */ SplashActivity K(final SplashActivity splashActivity) {
        return splashActivity.f;
    }
    
    public final void M() {
        if (!this.R()) {
            this.T(true);
        }
        else {
            this.N();
        }
    }
    
    public final void N() {
        if (this.Q(10, this.i, this.g, this.h)) {
            this.d.post(this.k);
        }
        else {
            this.O();
        }
    }
    
    public final void O() {
        new ra0((Context)this, new n52((Context)this, a91.u()).b(), this.m, this.h);
    }
    
    public final void P() {
        if (this.i.getBoolean("first_time_user", true)) {
            ((Context)this).startActivity(new Intent((Context)this.f, (Class)OnBoardActivity.class));
            this.finish();
        }
    }
    
    public final boolean Q(final int n, final SharedPreferences sharedPreferences, final int i, final String s) {
        try {
            final SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
            final StringBuilder sb = new StringBuilder();
            sb.append(ok.x);
            sb.append(i);
            final Date parse = simpleDateFormat.parse(b.g(sharedPreferences.getString(sb.toString(), ""), s));
            final Calendar instance = Calendar.getInstance();
            instance.setTime(parse);
            instance.add(5, n);
            return !new Date().after(instance.getTime());
        }
        catch (final Exception ex) {
            ex.printStackTrace();
            return false;
        }
    }
    
    public final boolean R() {
        try {
            return this.g >= this.i.getInt(ok.C, 0);
        }
        catch (final Exception ex) {
            ex.printStackTrace();
            return false;
        }
    }
    
    public final void S() {
        final MaterialAlertDialogBuilder materialAlertDialogBuilder = new MaterialAlertDialogBuilder((Context)this.f, 2131951953);
        materialAlertDialogBuilder.setTitle(2131886518).setMessage(2131886514).setCancelable(false).setPositiveButton(2131886746, (DialogInterface$OnClickListener)new DialogInterface$OnClickListener(this) {
            public final SplashActivity a;
            
            public void onClick(final DialogInterface dialogInterface, final int n) {
                this.a.O();
                dialogInterface.dismiss();
            }
        }).create();
        ((a.a)materialAlertDialogBuilder).show();
    }
    
    public final void T(final boolean b) {
        final int int1 = this.i.getInt(ok.D, 0);
        final MaterialAlertDialogBuilder materialAlertDialogBuilder = new MaterialAlertDialogBuilder((Context)this.f, 2131951953);
        final View inflate = ((LayoutInflater)((Context)this).getSystemService("layout_inflater")).inflate(2131493001, (ViewGroup)null);
        materialAlertDialogBuilder.setView(inflate);
        materialAlertDialogBuilder.setTitle(2131886654);
        materialAlertDialogBuilder.setCancelable(b ^ true);
        final a create = materialAlertDialogBuilder.create();
        ((View)inflate.findViewById(2131296481)).setOnClickListener((View$OnClickListener)new View$OnClickListener(this, create) {
            public final a a;
            public final SplashActivity b;
            
            public void onClick(final View view) {
                this.b.U();
                this.a.dismiss();
            }
        });
        final Button button = (Button)inflate.findViewById(2131296480);
        ((View)button).setOnClickListener((View$OnClickListener)new View$OnClickListener(this, create) {
            public final a a;
            public final SplashActivity b;
            
            public void onClick(final View view) {
                FirebaseAnalytics.getInstance((Context)SplashActivity.K(this.b)).a("UpdateLater", null);
                final SplashActivity b = this.b;
                b.d.post(b.k);
                this.a.dismiss();
            }
        });
        if (int1 > this.g) {
            ((View)button).setVisibility(4);
        }
        create.show();
    }
    
    public final void U() {
        final StringBuilder sb = new StringBuilder();
        sb.append("market://details?id=");
        sb.append(((Context)this).getPackageName());
        final Intent intent = new Intent("android.intent.action.VIEW", Uri.parse(sb.toString()));
        intent.addFlags(1208483840);
        try {
            ((Context)this).startActivity(intent);
            o4.b(this.e, "APP_UPDATE_NOW", this.h, "update_now");
        }
        catch (final ActivityNotFoundException ex) {
            final StringBuilder sb2 = new StringBuilder();
            sb2.append("http://play.google.com/store/apps/details?id=");
            sb2.append(((Context)this).getPackageName());
            ((Context)this).startActivity(new Intent("android.intent.action.VIEW", Uri.parse(sb2.toString())));
        }
    }
    
    @Override
    public void onCreate(final Bundle bundle) {
        super.onCreate(bundle);
        this.setContentView(2131492927);
        this.i = ((Context)this).getApplicationContext().getSharedPreferences("MyPref", 0);
        dj1.d((Context)this);
        this.j = this.getIntent().getBooleanExtra("BLOCK_NO_SYNC", false);
        this.e = FirebaseAnalytics.getInstance((Context)this);
        this.P();
        this.c = new SimpleDateFormat("yyyy-MM-dd");
        this.h = Settings$Secure.getString(((Context)this).getContentResolver(), "android_id");
        try {
            this.g = ((Context)this).getPackageManager().getPackageInfo(((Context)this).getPackageName(), 0).versionCode;
        }
        catch (final PackageManager$NameNotFoundException ex) {
            ((Throwable)ex).printStackTrace();
        }
        this.m = new y01(this) {
            public final SplashActivity a;
            
            @Override
            public void a(final Object o) {
                if (!this.a.R()) {
                    this.a.T(true);
                    return;
                }
                final SplashActivity a = this.a;
                if (!a.Q(20, SplashActivity.E(a), SplashActivity.F(this.a), SplashActivity.G(this.a)) && SplashActivity.I(this.a)) {
                    final FirebaseAnalytics instance = FirebaseAnalytics.getInstance((Context)SplashActivity.K(this.a));
                    final StringBuilder sb = new StringBuilder();
                    sb.append("CONNECTION_ERROR");
                    sb.append(SplashActivity.J(this.a));
                    instance.a(sb.toString(), null);
                    this.a.S();
                }
                else {
                    final FirebaseAnalytics instance2 = FirebaseAnalytics.getInstance((Context)SplashActivity.K(this.a));
                    final StringBuilder sb2 = new StringBuilder();
                    sb2.append("CONNECTION_SUCCESS");
                    sb2.append(SplashActivity.J(this.a));
                    instance2.a(sb2.toString(), null);
                    final SplashActivity a2 = this.a;
                    a2.d.post(a2.k);
                }
            }
        };
        if (Build$VERSION.SDK_INT >= 28) {
            try {
                final Field declaredField = CursorWindow.class.getDeclaredField("sCursorWindowSize");
                declaredField.setAccessible(true);
                declaredField.set(null, 20971520);
            }
            catch (final Exception ex2) {
                ex2.printStackTrace();
            }
        }
    }
    
    @Override
    public void onResume() {
        super.onResume();
        this.M();
    }
}

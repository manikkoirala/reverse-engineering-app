// 
// Decompiled by Procyon v0.6.0
// 

package com.ekodroid.omrevaluator.activities;

import android.util.Log;
import com.google.android.gms.tasks.Task;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.firebase.auth.FirebaseAuth;
import android.os.Bundle;
import android.content.Context;
import android.content.Intent;

public class LinkVerification extends v5
{
    public static String c = "tag";
    
    public static /* synthetic */ String B() {
        return LinkVerification.c;
    }
    
    public final void C() {
        new bw((Context)this, false, new y01(this) {
            public final LinkVerification a;
            
            @Override
            public void a(final Object o) {
                a91.b(((Context)this.a).getApplicationContext());
                ((Context)this.a).startActivity(new Intent(((Context)this.a).getApplicationContext(), (Class)HomeBottomNavActivity.class));
                this.a.finish();
            }
        }).show();
    }
    
    @Override
    public void onCreate(final Bundle bundle) {
        super.onCreate(bundle);
        this.setContentView(2131492914);
        final FirebaseAuth instance = FirebaseAuth.getInstance();
        final String string = this.getIntent().getData().toString();
        if (instance.i(string)) {
            instance.m(((Context)this).getApplicationContext().getSharedPreferences("MyPref", 0).getString("uId", ""), string).addOnCompleteListener((OnCompleteListener)new OnCompleteListener(this) {
                public final LinkVerification a;
                
                public void onComplete(final Task task) {
                    if (task.isSuccessful()) {
                        this.a.C();
                    }
                    else {
                        Log.e(LinkVerification.B(), "Error signing in with email link", (Throwable)task.getException());
                    }
                }
            });
        }
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.ekodroid.omrevaluator.activities.Services;

import android.media.RingtoneManager;
import android.os.Build$VERSION;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.IntentFilter;
import android.os.IBinder;
import com.ekodroid.omrevaluator.templateui.scanner.ResultItem;
import com.ekodroid.omrevaluator.templateui.models.SheetTemplate2;
import com.ekodroid.omrevaluator.database.ResultDataJsonModel;
import com.ekodroid.omrevaluator.database.repositories.ResultRepository;
import com.ekodroid.omrevaluator.database.repositories.TemplateRepository;
import com.ekodroid.omrevaluator.database.StudentDataModel;
import java.util.Iterator;
import com.ekodroid.omrevaluator.database.repositories.ClassRepository;
import android.content.Intent;
import android.content.Context;
import com.ekodroid.omrevaluator.templateui.models.ExamId;
import android.content.BroadcastReceiver;
import android.content.SharedPreferences$Editor;
import android.content.SharedPreferences;
import java.util.ArrayList;
import android.app.Service;

public class ServicePublishResult extends Service
{
    public ServicePublishResult a;
    public String b;
    public String c;
    public boolean d;
    public boolean e;
    public boolean f;
    public boolean g;
    public ArrayList h;
    public SharedPreferences i;
    public SharedPreferences$Editor j;
    public ze1 k;
    public boolean l;
    public BroadcastReceiver m;
    public boolean n;
    public y01 p;
    public ExamId q;
    
    public ServicePublishResult() {
        this.a = this;
        this.k = null;
        this.l = false;
        this.m = new BroadcastReceiver(this) {
            public final ServicePublishResult a;
            
            public void onReceive(final Context context, final Intent intent) {
                ServicePublishResult.a(this.a, true);
                final ze1 k = this.a.k;
                if (k != null) {
                    k.l();
                }
            }
        };
        this.n = false;
    }
    
    public static /* synthetic */ boolean a(final ServicePublishResult servicePublishResult, final boolean l) {
        return servicePublishResult.l = l;
    }
    
    public static /* synthetic */ ExamId b(final ServicePublishResult servicePublishResult) {
        return servicePublishResult.q;
    }
    
    public final void i(final Context context, final ArrayList list, final String s) {
        for (final ye1 ye1 : list) {
            final StudentDataModel student = ClassRepository.getInstance(context).getStudent(ye1.g(), s);
            String studentName;
            if (student != null) {
                studentName = student.getStudentName();
            }
            else {
                studentName = " - - - ";
            }
            ye1.m(studentName);
        }
    }
    
    public final void j() {
        ((Context)this).sendBroadcast(new Intent("REPORT_SENT"));
    }
    
    public final void k(final int n) {
        final StringBuilder sb = new StringBuilder();
        sb.append("Sent Reports : ");
        final SharedPreferences i = this.i;
        final StringBuilder sb2 = new StringBuilder();
        sb2.append(this.q.getExamName());
        sb2.append("_sentCount");
        sb.append(i.getInt(sb2.toString(), 0));
        final String string = sb.toString();
        final StringBuilder sb3 = new StringBuilder();
        sb3.append("Pending Reports : ");
        final SharedPreferences j = this.i;
        final StringBuilder sb4 = new StringBuilder();
        sb4.append(this.q.getExamName());
        sb4.append("_sentCount");
        sb3.append(n - j.getInt(sb4.toString(), 0));
        final String[] array = { string, sb3.toString() };
        final StringBuilder sb5 = new StringBuilder();
        sb5.append(this.q.getExamName());
        sb5.append(" - Publish Complete");
        this.q(1, array, sb5.toString());
        final ServicePublishResult a = this.a;
        final StringBuilder sb6 = new StringBuilder();
        sb6.append(this.q.getExamName());
        sb6.append(" - Publish Complete, ");
        sb6.append(array[0]);
        sb6.append(", ");
        sb6.append(array[1]);
        a91.H((Context)a, sb6.toString(), 0, 2131231085);
        this.a.stopSelf();
    }
    
    public final int l() {
        final SharedPreferences i = this.i;
        final StringBuilder sb = new StringBuilder();
        sb.append(this.q.getExamName());
        sb.append("_lastIndexSent");
        return i.getInt(sb.toString(), -1) + 1;
    }
    
    public final ArrayList m(final ExamId examId) {
        final SheetTemplate2 sheetTemplate = TemplateRepository.getInstance((Context)this.a).getTemplateJson(examId).getSheetTemplate();
        final ArrayList<ResultDataJsonModel> allResultJson = ResultRepository.getInstance((Context)this.a).getAllResultJson(examId);
        final ArrayList list = new ArrayList();
        for (final ResultDataJsonModel resultDataJsonModel : allResultJson) {
            final ResultItem resultItem = resultDataJsonModel.getResultItem(sheetTemplate);
            final int p = ve1.p(resultItem);
            final int q = ve1.q(resultItem);
            final int s = ve1.s(resultItem);
            final double totalMarks = resultItem.getTotalMarks();
            list.add(new ye1(resultDataJsonModel.getRollNo(), totalMarks, ve1.i(totalMarks, sheetTemplate.getGradeLevels()), p, q, s, resultDataJsonModel.isSmsSent(), resultDataJsonModel.isSynced(), resultDataJsonModel.isPublished()));
        }
        ve1.a(list, sheetTemplate.getRankingMethod());
        this.i((Context)this.a, list, examId.getClassName());
        return list;
    }
    
    public final void n() {
        final SharedPreferences i = this.i;
        final StringBuilder sb = new StringBuilder();
        sb.append(this.q.getExamName());
        sb.append("_lastIndexSent");
        final int int1 = i.getInt(sb.toString(), -1);
        final SharedPreferences$Editor j = this.j;
        final StringBuilder sb2 = new StringBuilder();
        sb2.append(this.q.getExamName());
        sb2.append("_lastIndexSent");
        j.putInt(sb2.toString(), int1 + 1);
        this.j.commit();
    }
    
    public final void o() {
        final SharedPreferences i = this.i;
        final StringBuilder sb = new StringBuilder();
        sb.append(this.q.getExamName());
        sb.append("_sentCount");
        final int int1 = i.getInt(sb.toString(), 0);
        final SharedPreferences$Editor j = this.j;
        final StringBuilder sb2 = new StringBuilder();
        sb2.append(this.q.getExamName());
        sb2.append("_sentCount");
        j.putInt(sb2.toString(), int1 + 1);
        this.j.commit();
    }
    
    public IBinder onBind(final Intent intent) {
        throw new UnsupportedOperationException("Not yet implemented");
    }
    
    public void onCreate() {
        super.onCreate();
        final SharedPreferences sharedPreferences = ((Context)this).getApplicationContext().getSharedPreferences("MyPref", 0);
        this.i = sharedPreferences;
        this.j = sharedPreferences.edit();
        this.p = new y01(this) {
            public final ServicePublishResult a;
            
            @Override
            public void a(final Object o) {
                if (o != null && o instanceof Boolean && (boolean)o) {
                    final ResultRepository instance = ResultRepository.getInstance((Context)this.a.a);
                    final ExamId b = ServicePublishResult.b(this.a);
                    final ServicePublishResult a = this.a;
                    instance.updateResultSent(b, ((ye1)a.h.get(a.l())).g(), true);
                    this.a.o();
                    final ServicePublishResult a2 = this.a;
                    a2.p(1, a2.h.size(), this.a.l());
                    this.a.n();
                    final ServicePublishResult a3 = this.a;
                    a3.r(a3.h);
                    this.a.j();
                }
                else {
                    final ServicePublishResult a4 = this.a;
                    a4.p(1, a4.h.size(), this.a.l());
                    this.a.n();
                    final ServicePublishResult a5 = this.a;
                    a5.r(a5.h);
                }
            }
        };
        sl.registerReceiver((Context)this, this.m, new IntentFilter("STOP_SERVICE_PUBLISH_RESULT"), 2);
    }
    
    public void onDestroy() {
        ((Context)this).unregisterReceiver(this.m);
        super.onDestroy();
    }
    
    public int onStartCommand(final Intent intent, final int n, final int n2) {
        super.onStartCommand(intent, n, n2);
        if (this.n) {
            return 1;
        }
        this.n = true;
        this.b = intent.getStringExtra("SEND_PATTERN");
        this.d = intent.getBooleanExtra("SEND_SMS", false);
        this.c = intent.getStringExtra("CC_EMAIL");
        this.e = intent.getBooleanExtra("SEND_EMAIL", false);
        this.f = intent.getBooleanExtra("INCLUDE_RANK", false);
        this.g = intent.getBooleanExtra("INCLUDE_SHEET_IMG", false);
        final ExamId q = (ExamId)intent.getSerializableExtra("EXAM_ID");
        if ((this.q = q) != null) {
            final ArrayList m = this.m(q);
            this.h = m;
            this.p(1, m.size(), this.l());
            this.r(this.h);
        }
        return 1;
    }
    
    public final void p(final int n, final int n2, final int i) {
        final PendingIntent broadcast = PendingIntent.getBroadcast((Context)this, 0, new Intent("STOP_SERVICE_PUBLISH_RESULT"), 67108864);
        final NotificationManager notificationManager = (NotificationManager)((Context)this).getSystemService("notification");
        if (notificationManager == null) {
            return;
        }
        if (Build$VERSION.SDK_INT >= 26) {
            fh.a(notificationManager, eh.a("channel1", "publish result", 4));
        }
        final rz0.e k = new rz0.e((Context)this.a, "channel1").k("EvalBee");
        final StringBuilder sb = new StringBuilder();
        sb.append("Sent Reports : ");
        sb.append(i);
        notificationManager.notify(n, k.j(sb.toString()).B(System.currentTimeMillis()).f(true).v(2131230924).t(n2, i, true).a(2131230906, "Cancel", broadcast).b());
    }
    
    public final void q(final int n, final String[] array, final String s) {
        final NotificationManager notificationManager = (NotificationManager)((Context)this).getSystemService("notification");
        if (notificationManager == null) {
            return;
        }
        if (Build$VERSION.SDK_INT >= 26) {
            fh.a(notificationManager, eh.a("channel1", "publish result", 4));
        }
        final rz0.f f = new rz0.f();
        for (int length = array.length, i = 0; i < length; ++i) {
            f.h(array[i]);
        }
        f.i(s);
        notificationManager.notify(n, new rz0.e((Context)this.a, "channel1").k("EvalBee").j(s).B(System.currentTimeMillis()).f(true).x(f).v(2131230924).w(RingtoneManager.getDefaultUri(2)).b());
    }
    
    public final void r(final ArrayList list) {
        final int l = this.l();
        if (l >= list.size() || this.l) {
            this.k(list.size());
            return;
        }
        final ye1 ye1 = list.get(l);
        final StudentDataModel student = ClassRepository.getInstance((Context)this.a).getStudent(ye1.g(), this.q.getClassName());
        if (student == null) {
            this.n();
            this.r(list);
            return;
        }
        if (student.getEmailId().length() < 3 && student.getPhoneNo().length() < 8) {
            this.n();
            this.r(list);
            return;
        }
        if (this.b.equals("send_pending") && ResultRepository.getInstance((Context)this.a).getResultEmailSendStatus(this.q, student.getRollNO())) {
            this.n();
            this.o();
            this.r(list);
            return;
        }
        this.k = new ze1((Context)this.a, new td1(student.getStudentName(), this.q.getExamName(), this.q.getClassName(), student.getEmailId(), this.c, student.getPhoneNo(), this.e, this.d, student.getRollNO(), ye1.f(), this.q.getExamDate(), this.g, this.f), this.p);
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.ekodroid.omrevaluator.activities.Services;

import android.content.Context;
import android.content.Intent;
import android.app.IntentService;

public class ScanSyncService extends IntentService
{
    public boolean a;
    
    public ScanSyncService() {
        super("ScanSyncService");
        this.a = false;
    }
    
    public static /* synthetic */ boolean a(final ScanSyncService scanSyncService, final boolean a) {
        return scanSyncService.a = a;
    }
    
    public final void b() {
        new n61(new n52(((Context)this).getApplicationContext(), a91.u()).b(), new y01(this) {
            public final ScanSyncService a;
            
            @Override
            public void a(final Object o) {
                ScanSyncService.a(this.a, true);
            }
        });
    }
    
    public void onCreate() {
        super.onCreate();
    }
    
    public void onDestroy() {
        super.onDestroy();
    }
    
    public void onHandleIntent(final Intent intent) {
        this.b();
        while (!this.a) {
            try {
                Thread.sleep(2000L);
            }
            catch (final InterruptedException ex) {
                ex.printStackTrace();
            }
        }
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.ekodroid.omrevaluator.activities.Services;

import android.content.SharedPreferences;
import java.util.Map;
import android.media.RingtoneManager;
import android.os.Build$VERSION;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Intent;
import android.content.Context;
import com.ekodroid.omrevaluator.clients.UserProfileClients.models.Device;
import android.provider.Settings$Secure;
import com.google.firebase.messaging.d;
import com.google.firebase.messaging.FirebaseMessagingService;

public class CloudMessageService extends FirebaseMessagingService
{
    @Override
    public void r(final d d) {
        if (d.i() != null) {
            this.w(d.i().c(), d.i().a());
        }
        if (d.getData() != null) {
            this.x(d.getData());
        }
    }
    
    @Override
    public void t(final String s) {
        try {
            final String string = Settings$Secure.getString(((Context)this).getContentResolver(), "android_id");
            final StringBuilder sb = new StringBuilder();
            sb.append("");
            sb.append(a91.f());
            final r61 r61 = new r61(new Device(string, System.currentTimeMillis(), "Android", sb.toString(), s), (Context)this, new zg(this) {
                public final CloudMessageService a;
                
                @Override
                public void a(final boolean b, final int n, final Object o) {
                }
            });
        }
        catch (final Exception ex) {}
    }
    
    public final void w(final String s, final String s2) {
        final Context applicationContext = ((Context)this).getApplicationContext();
        final PendingIntent activity = PendingIntent.getActivity(applicationContext, 1001, new Intent(), 201326592);
        final NotificationManager notificationManager = (NotificationManager)applicationContext.getSystemService("notification");
        if (notificationManager == null) {
            return;
        }
        if (Build$VERSION.SDK_INT >= 26) {
            fh.a(notificationManager, eh.a("channel_release", "Information and release notes", 4));
        }
        final rz0.e w = new rz0.e(applicationContext, "channel_release").f(true).k(s).j(s2).s(0).x(new rz0.c().h(s2)).i(activity).B(System.currentTimeMillis()).w(RingtoneManager.getDefaultUri(2));
        w.v(2131230921);
        w.h(((Context)this).getResources().getColor(2131099698));
        notificationManager.notify(1001, w.b());
    }
    
    public final void x(final Map map) {
        if (map == null) {
            return;
        }
        final String s = map.get("MESSAGETYPE");
        if (s != null && s.equals(ok.E)) {
            this.y(map);
        }
    }
    
    public final void y(final Map map) {
        try {
            final SharedPreferences sharedPreferences = ((Context)this).getApplicationContext().getSharedPreferences("MyPref", 0);
            final int int1 = Integer.parseInt(map.get("LATEST_VERSION"));
            final int int2 = Integer.parseInt(map.get("BLOCKED_VERSION"));
            sharedPreferences.edit().putInt(ok.C, int1).commit();
            sharedPreferences.edit().putInt(ok.D, int2).commit();
        }
        catch (final Exception ex) {}
    }
}

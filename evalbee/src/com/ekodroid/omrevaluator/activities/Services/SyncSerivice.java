// 
// Decompiled by Procyon v0.6.0
// 

package com.ekodroid.omrevaluator.activities.Services;

import com.ekodroid.omrevaluator.students.services.SyncClassListService;
import com.ekodroid.omrevaluator.students.services.SyncStudentService;
import com.google.firebase.messaging.FirebaseMessaging;
import com.ekodroid.omrevaluator.clients.UserProfileClients.models.Device;
import android.provider.Settings$Secure;
import android.content.Intent;
import com.google.android.gms.tasks.Task;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.firebase.auth.FirebaseAuth;
import android.content.Context;
import android.app.IntentService;

public class SyncSerivice extends IntentService
{
    public boolean a;
    public boolean b;
    public final Context c;
    
    public SyncSerivice() {
        super("SyncSerivice");
        this.a = false;
        this.b = false;
        this.c = (Context)this;
    }
    
    public static /* synthetic */ Context c(final SyncSerivice syncSerivice) {
        return syncSerivice.c;
    }
    
    public static /* synthetic */ boolean d(final SyncSerivice syncSerivice, final boolean a) {
        return syncSerivice.a = a;
    }
    
    public static /* synthetic */ boolean e(final SyncSerivice syncSerivice, final boolean b) {
        return syncSerivice.b = b;
    }
    
    public final void f() {
        FirebaseAuth.getInstance().e().i(false).addOnCompleteListener((OnCompleteListener)new OnCompleteListener(this) {
            public final SyncSerivice a;
            
            public void onComplete(final Task task) {
                new pa0(SyncSerivice.c(this.a), new zg(this) {
                    public final SyncSerivice$d a;
                    
                    @Override
                    public void a(final boolean b, final int n, final Object o) {
                        FirebaseAuth.getInstance().e().i(true).addOnCompleteListener((OnCompleteListener)new OnCompleteListener(this) {
                            public final SyncSerivice$d$a a;
                            
                            public void onComplete(final Task task) {
                                new sa0(SyncSerivice.c(this.a.a.a), new zg(this, a91.c(SyncSerivice.c(this.a.a.a))) {
                                    public final boolean a;
                                    public final SyncSerivice$d$a$a b;
                                    
                                    @Override
                                    public void a(final boolean b, final int n, final Object o) {
                                        SyncSerivice.e(this.b.a.a.a, true);
                                        if (!this.a && a91.c(SyncSerivice.c(this.b.a.a.a))) {
                                            ((Context)this.b.a.a.a).sendBroadcast(new Intent("UPDATE_PURCHASE"));
                                        }
                                    }
                                });
                            }
                        });
                    }
                });
            }
        });
    }
    
    public final void g(final String s) {
        try {
            final String string = Settings$Secure.getString(((Context)this).getContentResolver(), "android_id");
            final StringBuilder sb = new StringBuilder();
            sb.append("");
            sb.append(a91.f());
            final r61 r61 = new r61(new Device(string, System.currentTimeMillis(), "Android", sb.toString(), s), (Context)this, new zg(this) {
                public final SyncSerivice a;
                
                @Override
                public void a(final boolean b, final int n, final Object o) {
                }
            });
        }
        catch (final Exception ex) {}
    }
    
    public final void h() {
        final String string = Settings$Secure.getString(((Context)this).getContentResolver(), "android_id");
        final ee1 b = new n52(((Context)this).getApplicationContext(), a91.u()).b();
        new ra0((Context)this, b, new y01(this, b, string) {
            public final ee1 a;
            public final String b;
            public final SyncSerivice c;
            
            @Override
            public void a(final Object o) {
                new cb0(SyncSerivice.c(this.c), this.a, new y01(this) {
                    public final SyncSerivice$c a;
                    
                    @Override
                    public void a(final Object o) {
                        SyncSerivice.d(this.a.c, true);
                    }
                }, this.b);
            }
        }, string);
    }
    
    public void onCreate() {
        super.onCreate();
        FirebaseMessaging.l().o().addOnCompleteListener((OnCompleteListener)new OnCompleteListener(this) {
            public final SyncSerivice a;
            
            public void onComplete(final Task task) {
                if (!task.isSuccessful()) {
                    return;
                }
                final String s = (String)task.getResult();
                ((Context)this.a).getApplicationContext().getSharedPreferences("MyPref", 0).edit().putString("fcmId", s).commit();
                this.a.h();
                this.a.g(s);
            }
        });
        if (FirebaseAuth.getInstance().e() != null) {
            this.f();
        }
    }
    
    public void onDestroy() {
        super.onDestroy();
    }
    
    public void onHandleIntent(final Intent intent) {
        while (true) {
            if (this.a) {
                if (this.b) {
                    break;
                }
            }
            try {
                Thread.sleep(2000L);
            }
            catch (final InterruptedException ex) {
                ex.printStackTrace();
            }
        }
        SyncStudentService.h(this.c, false);
        SyncClassListService.c(this.c, false);
        SyncImageService.b(this.c);
    }
}

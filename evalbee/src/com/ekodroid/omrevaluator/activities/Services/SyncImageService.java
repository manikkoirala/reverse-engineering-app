// 
// Decompiled by Procyon v0.6.0
// 

package com.ekodroid.omrevaluator.activities.Services;

import com.ekodroid.omrevaluator.serializable.ResponseModel.PurchaseAccount;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.OnFailureListener;
import android.net.Uri;
import java.io.File;
import com.ekodroid.omrevaluator.serializable.ArchivedResult.SheetImageDto;
import com.ekodroid.omrevaluator.database.SheetImageModel;
import com.ekodroid.omrevaluator.database.repositories.ResultRepository;
import com.ekodroid.omrevaluator.clients.UserProfileClients.models.OrgProfile;
import java.util.Iterator;
import com.ekodroid.omrevaluator.database.TemplateDataJsonModel;
import com.ekodroid.omrevaluator.database.repositories.TemplateRepository;
import android.content.Intent;
import android.content.Context;
import android.app.IntentService;

public class SyncImageService extends IntentService
{
    public Context a;
    
    public SyncImageService() {
        super("SyncImageService");
        this.a = (Context)this;
    }
    
    public static void b(final Context context) {
        final Intent intent = new Intent(context, (Class)SyncImageService.class);
        intent.setAction("ACTION_SYNC_IMAGE");
        context.startService(intent);
    }
    
    public final void a() {
        final Iterator<TemplateDataJsonModel> iterator = TemplateRepository.getInstance(this.a).getAllImageSyncedActivatedExams().iterator();
        while (iterator.hasNext()) {
            this.c(iterator.next());
        }
    }
    
    public final void c(final TemplateDataJsonModel templateDataJsonModel) {
        final String orgId = OrgProfile.getInstance((Context)this).getOrgId();
        final ResultRepository instance = ResultRepository.getInstance((Context)this);
        for (final SheetImageModel sheetImageModel : instance.getAllNonSyncImages(templateDataJsonModel.getExamId())) {
            final long currentTimeMillis = System.currentTimeMillis();
            if (this.d(sheetImageModel, templateDataJsonModel.getCloudId(), orgId, currentTimeMillis)) {
                if (!this.e(new SheetImageDto(templateDataJsonModel.getCloudId(), sheetImageModel.getRollNo(), sheetImageModel.getPageIndex(), currentTimeMillis, sheetImageModel.getSheetDimensionJson()))) {
                    continue;
                }
                sheetImageModel.setSynced(true);
                sheetImageModel.setUploadId(currentTimeMillis);
                instance.saveOrUpdateSheetImage(sheetImageModel);
            }
        }
    }
    
    public final boolean d(SheetImageModel sheetImageModel, final long lng, final String str, final long lng2) {
        final o30 i = o30.i("gs://evalbee-scan-images");
        if (str == null || i == null) {
            return false;
        }
        final File file = new File(sheetImageModel.getImagePath());
        if (!file.exists()) {
            return false;
        }
        final StringBuilder sb = new StringBuilder();
        sb.append(lng2);
        sb.append("_");
        sb.append(sheetImageModel.getRollNo());
        sb.append("_");
        sb.append(sheetImageModel.getPageIndex());
        sb.append(".jpg");
        final String string = sb.toString();
        final jq1 n = i.n();
        final StringBuilder sb2 = new StringBuilder();
        sb2.append(ok.e);
        sb2.append("/");
        sb2.append(str);
        sb2.append("/");
        sb2.append(lng);
        sb2.append("/");
        sb2.append(string);
        final i12 l = n.a(sb2.toString()).l(Uri.fromFile(file));
        sheetImageModel = (SheetImageModel)new zs1();
        l.p((OnFailureListener)new OnFailureListener(this, sheetImageModel) {
            public final zs1 a;
            public final SyncImageService b;
            
            public void onFailure(final Exception ex) {
                final zs1 a = this.a;
                a.b = true;
                a.a = false;
            }
        }).u((OnSuccessListener)new OnSuccessListener(this, sheetImageModel) {
            public final zs1 a;
            public final SyncImageService b;
            
            public void a(final i12.b b) {
                final zs1 a = this.a;
                a.b = true;
                a.a = true;
            }
        }).s(new o11(this) {
            public final SyncImageService a;
            
            public void b(final i12.b b) {
            }
        });
        while (System.currentTimeMillis() - ((zs1)sheetImageModel).c < 600000L && !((zs1)sheetImageModel).b) {
            try {
                Thread.sleep(100L);
            }
            catch (final InterruptedException ex) {
                ex.printStackTrace();
            }
        }
        return ((zs1)sheetImageModel).a;
    }
    
    public final boolean e(final SheetImageDto sheetImageDto) {
        final zs1 zs1 = new zs1();
        new o61((Context)this, sheetImageDto, new zg(this, zs1) {
            public final zs1 a;
            public final SyncImageService b;
            
            @Override
            public void a(final boolean b, final int n, final Object o) {
                if (b && n == 200) {
                    final zs1 a = this.a;
                    a.b = true;
                    a.a = true;
                }
                else {
                    final zs1 a2 = this.a;
                    a2.b = true;
                    a2.a = false;
                }
            }
        });
        while (System.currentTimeMillis() - zs1.c < 600000L && !zs1.b) {
            try {
                Thread.sleep(100L);
            }
            catch (final InterruptedException ex) {
                ex.printStackTrace();
            }
        }
        return zs1.a;
    }
    
    public void onHandleIntent(final Intent intent) {
        final boolean c = a91.c((Context)this);
        final PurchaseAccount r = a91.r((Context)this);
        if (intent != null && r != null && r.getAccountType().equals(ok.S) && c && "ACTION_SYNC_IMAGE".equals(intent.getAction())) {
            this.a();
        }
    }
}

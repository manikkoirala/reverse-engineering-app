// 
// Decompiled by Procyon v0.6.0
// 

package com.ekodroid.omrevaluator.activities.Services;

import com.ekodroid.omrevaluator.serializable.ResponseModel.PurchaseAccount;
import com.ekodroid.omrevaluator.clients.SyncClients.GetSheetImagesRequest;
import java.util.Iterator;
import com.ekodroid.omrevaluator.database.TemplateDataJsonModel;
import com.ekodroid.omrevaluator.database.SheetImageModel;
import com.ekodroid.omrevaluator.templateui.scanner.SheetDimension;
import com.ekodroid.omrevaluator.database.repositories.ResultRepository;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.ekodroid.omrevaluator.templateui.scansheet.CheckedSheetImageActivity;
import java.io.File;
import com.ekodroid.omrevaluator.clients.UserProfileClients.models.OrgProfile;
import com.ekodroid.omrevaluator.database.repositories.TemplateRepository;
import com.ekodroid.omrevaluator.serializable.ArchivedResult.SheetImageDto;
import java.io.Serializable;
import android.content.Intent;
import java.util.ArrayList;
import com.ekodroid.omrevaluator.templateui.models.ExamId;
import android.content.Context;
import android.app.IntentService;

public class DownloadSheetImageService extends IntentService
{
    public Context a;
    
    public DownloadSheetImageService() {
        super("DownloadImageService");
        this.a = (Context)this;
    }
    
    public static /* synthetic */ Context b(final DownloadSheetImageService downloadSheetImageService) {
        return downloadSheetImageService.a;
    }
    
    public static void h(final Context context, final ExamId examId, final int n) {
        final Intent intent = new Intent(context, (Class)DownloadSheetImageService.class);
        intent.setAction("ACTION_DOWNLOAD_IMAGES");
        intent.putExtra("EXAM_ID", (Serializable)examId);
        intent.putExtra("ROLL_NUMBER", n);
        context.startService(intent);
    }
    
    public final void d(final ExamId examId, final int n, final int n2) {
        final Intent intent = new Intent("UPDATE_SHEET_IMAGE");
        intent.putExtra("EXAM_ID", (Serializable)examId);
        intent.putExtra("ROLL_NUMBER", n);
        intent.putExtra("PAGE_INDEX", n2);
        ((Context)this).sendBroadcast(intent);
    }
    
    public final void e(final ExamId examId, final SheetImageDto sheetImageDto) {
        final TemplateDataJsonModel templateJson = TemplateRepository.getInstance((Context)this).getTemplateJson(examId);
        final String folderPath = templateJson.getFolderPath();
        final String orgId = OrgProfile.getInstance((Context)this).getOrgId();
        try {
            final File file = new File(folderPath);
            if (!file.exists()) {
                file.mkdirs();
            }
            final o30 i = o30.i("gs://evalbee-scan-images");
            final String p2 = CheckedSheetImageActivity.P(folderPath, sheetImageDto.rollNumber, sheetImageDto.pageIndex);
            final jq1 n = i.n();
            final StringBuilder sb = new StringBuilder();
            sb.append(sheetImageDto.uploadId);
            sb.append("_");
            sb.append(sheetImageDto.rollNumber);
            sb.append("_");
            sb.append(sheetImageDto.pageIndex);
            sb.append(".jpg");
            final String string = sb.toString();
            final StringBuilder sb2 = new StringBuilder();
            sb2.append(ok.e);
            sb2.append("/");
            sb2.append(orgId);
            sb2.append("/");
            sb2.append(templateJson.getCloudId());
            sb2.append("/");
            sb2.append(string);
            n.a(sb2.toString()).g(new File(p2)).p((OnFailureListener)new OnFailureListener(this) {
                public final DownloadSheetImageService a;
                
                public void onFailure(final Exception ex) {
                }
            }).u((OnSuccessListener)new OnSuccessListener(this, examId, sheetImageDto, p2) {
                public final ExamId a;
                public final SheetImageDto b;
                public final String c;
                public final DownloadSheetImageService d;
                
                public void a(final x00.a a) {
                    final ResultRepository instance = ResultRepository.getInstance(DownloadSheetImageService.b(this.d));
                    final ExamId a2 = this.a;
                    final SheetImageDto b = this.b;
                    instance.deleteSheetImage(a2, b.rollNumber, b.pageIndex);
                    final SheetImageModel sheetImageModel = new SheetImageModel(this.a.getExamName(), this.b.rollNumber, null, this.a.getClassName(), this.a.getExamDate(), (SheetDimension)new gc0().j(this.b.sheetDimensionJson, SheetDimension.class), this.c, this.b.pageIndex);
                    sheetImageModel.setUploadId(this.b.uploadId);
                    sheetImageModel.setSynced(true);
                    instance.saveOrUpdateSheetImage(sheetImageModel);
                    final DownloadSheetImageService d = this.d;
                    final ExamId a3 = this.a;
                    final SheetImageDto b2 = this.b;
                    d.d(a3, b2.rollNumber, b2.pageIndex);
                }
            }).s(new o11(this) {
                public final DownloadSheetImageService a;
                
                public void b(final x00.a a) {
                }
            });
        }
        catch (final Exception ex) {
            ex.printStackTrace();
        }
    }
    
    public final void f(final ExamId examId, final ArrayList list) {
        for (final SheetImageDto sheetImageDto : list) {
            final SheetImageModel sheetImage = ResultRepository.getInstance((Context)this).getSheetImage(examId, sheetImageDto.rollNumber, sheetImageDto.pageIndex);
            if (sheetImage == null || (sheetImage.isSynced() && sheetImage.getUploadId() != sheetImageDto.uploadId)) {
                this.e(examId, sheetImageDto);
            }
        }
    }
    
    public final void g(final ExamId examId, final int n) {
        final TemplateDataJsonModel templateJson = TemplateRepository.getInstance((Context)this).getTemplateJson(examId);
        if (templateJson.isCloudSyncOn()) {
            if (templateJson.isSyncImages()) {
                new GetSheetImagesRequest((Context)this, templateJson.getCloudId(), n, new zg(this, templateJson.getSheetTemplate().getPageLayouts().length, examId) {
                    public final int a;
                    public final ExamId b;
                    public final DownloadSheetImageService c;
                    
                    @Override
                    public void a(final boolean b, final int n, final Object o) {
                        if (b && o != null) {
                            final ArrayList list = (ArrayList)o;
                            if (list.size() == this.a) {
                                this.c.f(this.b, list);
                            }
                        }
                    }
                });
            }
        }
    }
    
    public void onHandleIntent(final Intent intent) {
        final boolean c = a91.c((Context)this);
        final PurchaseAccount r = a91.r((Context)this);
        if (intent != null && r != null && r.getAccountType().equals(ok.S) && c) {
            final ExamId examId = (ExamId)intent.getSerializableExtra("EXAM_ID");
            final int intExtra = intent.getIntExtra("ROLL_NUMBER", -1);
            if ("ACTION_DOWNLOAD_IMAGES".equals(intent.getAction()) && examId != null && intExtra > -1) {
                this.g(examId, intExtra);
            }
        }
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.ekodroid.omrevaluator.activities.Services;

import java.io.OutputStream;
import android.net.Uri;
import java.util.Iterator;
import com.ekodroid.omrevaluator.clients.SyncClients.Models.ExamAction;
import java.util.Map;
import java.util.HashMap;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.android.gms.tasks.Task;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.firebase.auth.FirebaseAuth;
import com.ekodroid.omrevaluator.serializable.ArchivedResult.ArchivedPendingDelete;
import java.io.IOException;
import java.io.FileOutputStream;
import java.io.File;
import com.ekodroid.omrevaluator.serializable.ArchivedResult.ArchiveProgress;
import com.ekodroid.omrevaluator.database.ResultDataJsonModel;
import com.ekodroid.omrevaluator.templateui.models.SheetTemplate2;
import java.util.ArrayList;
import com.ekodroid.omrevaluator.database.repositories.ResultRepository;
import com.ekodroid.omrevaluator.templateui.models.ExamId;
import com.ekodroid.omrevaluator.database.TemplateDataJsonModel;
import android.os.Bundle;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.ekodroid.omrevaluator.database.repositories.TemplateRepository;
import com.ekodroid.omrevaluator.database.ArchivedLocalDataModel;
import android.content.Intent;
import android.content.Context;
import android.app.IntentService;

public class ArchiveSyncService extends IntentService
{
    public ArchiveSyncService() {
        super("ArchiveSyncService");
    }
    
    public static void n(final Context context) {
        final Intent intent = new Intent(context, (Class)ArchiveSyncService.class);
        intent.setAction("ACTION_SYNC_ARCHIVE");
        context.startService(intent);
    }
    
    public final void b(final ArchivedLocalDataModel archivedLocalDataModel) {
        final String templateId = archivedLocalDataModel.getTemplateId();
        if (!this.o(templateId)) {
            return;
        }
        this.d(60, templateId, "CLOUD_SYNC");
        if (!this.p(archivedLocalDataModel)) {
            return;
        }
        this.d(80, templateId, "CLOUD_SYNC");
        final TemplateRepository instance = TemplateRepository.getInstance((Context)this);
        instance.deleteArchiveLocalModel(archivedLocalDataModel.getTemplateId());
        if (instance.getArchiveLocalDataModel(archivedLocalDataModel.getTemplateId()) == null) {
            this.d(100, templateId, "CLOUD_SYNC");
            FirebaseAnalytics.getInstance((Context)this).a("ARCHIVED_EXAM_CLOUD", null);
        }
    }
    
    public final void c(final TemplateDataJsonModel templateDataJsonModel) {
        String archiveId;
        if ((archiveId = templateDataJsonModel.getArchiveId()) == null) {
            final StringBuilder sb = new StringBuilder();
            sb.append("");
            sb.append(System.currentTimeMillis());
            archiveId = sb.toString();
            templateDataJsonModel.setArchiveId(archiveId);
        }
        final ExamId examId = new ExamId(templateDataJsonModel.getName(), templateDataJsonModel.getClassName(), templateDataJsonModel.getExamDate());
        final SheetTemplate2 sheetTemplate = templateDataJsonModel.getSheetTemplate();
        final ArrayList<ResultDataJsonModel> allResultJson = ResultRepository.getInstance((Context)this).getAllResultJson(examId);
        final ArchiveProgress archivePayload = templateDataJsonModel.getArchivePayload();
        this.e(20, archiveId);
        if (archivePayload == null || !archivePayload.isExpExm()) {
            if (!this.f(archiveId, examId, sheetTemplate, allResultJson)) {
                return;
            }
            final ArchiveProgress archivePayload2 = templateDataJsonModel.getArchivePayload();
            archivePayload2.setExpExm(true);
            archivePayload2.setProgress(70);
            templateDataJsonModel.setArchivePayload(archivePayload2);
            TemplateRepository.getInstance((Context)this).saveOrUpdateTemplateJson(templateDataJsonModel);
            this.e(70, archiveId);
        }
        if (!this.g(archiveId, examId, allResultJson.size(), sheetTemplate.getAnswerOptions().size() - 1)) {
            return;
        }
        if (!this.h(examId)) {
            return;
        }
        this.e(100, archiveId);
        FirebaseAnalytics.getInstance((Context)this).a("ARCHIVED_EXAM_LOCAL", null);
    }
    
    public final void d(final int n, final String s, final String s2) {
        final Intent intent = new Intent("UPDATE_ARCHIVE_EXAM_LIST");
        intent.putExtra("progress", n);
        intent.putExtra("file_id", s);
        intent.putExtra("type", s2);
        ((Context)this).sendBroadcast(intent);
    }
    
    public final void e(final int n, final String s) {
        final Intent intent = new Intent("UPDATE_EXAM_LIST");
        intent.putExtra("progress", n);
        intent.putExtra("file_id", s);
        ((Context)this).sendBroadcast(intent);
    }
    
    public final boolean f(String str, ExamId examId, final SheetTemplate2 sheetTemplate2, final ArrayList list) {
        final File parent = new File(((Context)this).getApplicationContext().getExternalFilesDir(ok.c), ok.d);
        if (!parent.exists()) {
            parent.mkdirs();
        }
        final StringBuilder sb = new StringBuilder();
        sb.append((String)str);
        sb.append(".exm");
        final File file = new File(parent, sb.toString());
        IOException ex3 = null;
        Label_0172: {
            byte[] b = null;
            try {
                b = yx.b(yx.d(sheetTemplate2, list, examId));
                examId = (ExamId)(str = (IOException)new FileOutputStream(file));
                try {
                    final ExamId examId2 = examId;
                    final byte[] array = b;
                    ((FileOutputStream)examId2).write(array);
                    str = (IOException)examId;
                    final ExamId examId3 = examId;
                    ((OutputStream)examId3).flush();
                    str = (IOException)examId;
                    final ExamId examId4 = examId;
                    ((FileOutputStream)examId4).close();
                    return true;
                }
                catch (final Exception ex) {}
            }
            catch (final Exception ex) {
                examId = null;
            }
            finally {
                final IOException ex2;
                str = ex2;
                break Label_0172;
            }
            try {
                final ExamId examId2 = examId;
                final byte[] array = b;
                ((FileOutputStream)examId2).write(array);
                str = (IOException)examId;
                final ExamId examId3 = examId;
                ((OutputStream)examId3).flush();
                str = (IOException)examId;
                final ExamId examId4 = examId;
                ((FileOutputStream)examId4).close();
                return true;
                str = (IOException)examId;
                final Exception ex;
                ex.printStackTrace();
                str = (IOException)examId;
                file.deleteOnExit();
                iftrue(Label_0165:)(examId == null);
                Block_15: {
                    break Block_15;
                    Label_0165: {
                        return false;
                    }
                }
                try {
                    ((OutputStream)examId).flush();
                    ((FileOutputStream)examId).close();
                }
                catch (final IOException str) {
                    str.printStackTrace();
                }
                return false;
            }
            finally {
                ex3 = str;
                final IOException ex4;
                str = ex4;
            }
        }
        if (ex3 != null) {
            try {
                ((OutputStream)ex3).flush();
                ((FileOutputStream)ex3).close();
            }
            catch (final IOException ex5) {
                ex5.printStackTrace();
            }
        }
        throw str;
    }
    
    public final boolean g(final String str, final ExamId examId, final int n, final int n2) {
        final File parent = new File(((Context)this).getApplicationContext().getExternalFilesDir(ok.c), ok.d);
        if (!parent.exists()) {
            parent.mkdirs();
        }
        final StringBuilder sb = new StringBuilder();
        sb.append(str);
        sb.append(".exm");
        final ArchivedLocalDataModel archivedLocalDataModel = new ArchivedLocalDataModel(str, examId.getExamName(), examId.getClassName(), examId.getExamDate(), a91.g(), (int)(new File(parent, sb.toString()).length() / 1024L) * 2, 1, n, n2);
        final TemplateRepository instance = TemplateRepository.getInstance((Context)this);
        instance.saveOrUpdateArchiveLocalDataModel(archivedLocalDataModel);
        return instance.getArchiveLocalDataModel(str) != null;
    }
    
    public final boolean h(final ExamId examId) {
        return TemplateRepository.getInstance((Context)this).deleteTemplateJson(examId);
    }
    
    public final void i(final String s) {
        final TemplateRepository instance = TemplateRepository.getInstance((Context)this);
        if (instance.getArchiveLocalDataModel(s) == null) {
            if (!this.k(s)) {
                return;
            }
            this.d(40, s, "DELETE");
            this.j(s);
            this.d(70, s, "DELETE");
            if (!this.l(s)) {
                return;
            }
        }
        else {
            this.j(s);
            this.d(40, s, "DELETE");
            instance.deleteArchiveLocalModel(s);
            this.d(70, s, "DELETE");
        }
        ArchivedPendingDelete.deletArchiveId((Context)this, s);
        this.d(100, s, "DELETE");
        FirebaseAnalytics.getInstance((Context)this).a("ARCHIVED_EXAM_DELETE", null);
    }
    
    public final boolean j(final String str) {
        final File parent = new File(((Context)this).getApplicationContext().getExternalFilesDir(ok.c), ok.d);
        if (!parent.exists()) {
            parent.mkdirs();
        }
        final StringBuilder sb = new StringBuilder();
        sb.append(str);
        sb.append(".exm");
        final File file = new File(parent, sb.toString());
        return !file.exists() || file.delete();
    }
    
    public final boolean k(String string) {
        final o30 f = o30.f();
        final r30 e = FirebaseAuth.getInstance().e();
        if (e != null && f != null) {
            final StringBuilder sb = new StringBuilder();
            sb.append((String)string);
            sb.append(".exm");
            string = sb.toString();
            final jq1 n = f.n();
            final StringBuilder sb2 = new StringBuilder();
            sb2.append(ok.d);
            sb2.append("/");
            sb2.append(e.O());
            sb2.append("/");
            sb2.append((String)string);
            final Task d = n.a(sb2.toString()).d();
            string = new zs1();
            d.addOnFailureListener((OnFailureListener)new OnFailureListener(this, string) {
                public final zs1 a;
                public final ArchiveSyncService b;
                
                public void onFailure(final Exception ex) {
                    if (!ex.getMessage().equals("Object does not exist at location.")) {
                        FirebaseAnalytics.getInstance(((Context)this.b).getApplicationContext()).a("ERROR_STORAGE_DELETE", null);
                    }
                    final zs1 a = this.a;
                    a.b = true;
                    a.a = true;
                }
            }).addOnSuccessListener((OnSuccessListener)new OnSuccessListener(this, string) {
                public final zs1 a;
                public final ArchiveSyncService b;
                
                public void a(final Void void1) {
                    final zs1 a = this.a;
                    a.b = true;
                    a.a = true;
                }
            });
            while (System.currentTimeMillis() - ((zs1)string).c < 600000L && !((zs1)string).b) {
                try {
                    Thread.sleep(500L);
                }
                catch (final InterruptedException ex) {
                    ex.printStackTrace();
                }
            }
            return ((zs1)string).a;
        }
        return false;
    }
    
    public final boolean l(final String s) {
        final FirebaseFirestore f = FirebaseFirestore.f();
        final r30 e = FirebaseAuth.getInstance().e();
        final zs1 zs1 = new zs1();
        final HashMap hashMap = new HashMap();
        hashMap.put(s, v00.a());
        f.a("ArchivedExams").g(e.O()).s(hashMap).addOnSuccessListener((OnSuccessListener)new OnSuccessListener(this, zs1) {
            public final zs1 a;
            public final ArchiveSyncService b;
            
            public void a(final Void void1) {
                final zs1 a = this.a;
                a.b = true;
                a.a = true;
            }
        }).addOnFailureListener((OnFailureListener)new OnFailureListener(this, zs1) {
            public final zs1 a;
            public final ArchiveSyncService b;
            
            public void onFailure(final Exception ex) {
                final zs1 a = this.a;
                a.b = true;
                a.a = false;
            }
        });
        while (System.currentTimeMillis() - zs1.c < 600000L && !zs1.b) {
            try {
                Thread.sleep(500L);
            }
            catch (final InterruptedException ex) {
                ex.printStackTrace();
            }
        }
        return zs1.a;
    }
    
    public final void m() {
        final TemplateRepository instance = TemplateRepository.getInstance((Context)this);
        final ArrayList<TemplateDataJsonModel> archivingTemplatesJson = instance.getArchivingTemplatesJson();
        if (archivingTemplatesJson != null && archivingTemplatesJson.size() > 0) {
            for (final TemplateDataJsonModel templateDataJsonModel : archivingTemplatesJson) {
                this.c(templateDataJsonModel);
                if (templateDataJsonModel.isCloudSyncOn()) {
                    final ArrayList<ExamAction> list = new ArrayList<ExamAction>();
                    list.add(new ExamAction(templateDataJsonModel.getCloudId(), true, false));
                    a91.I(((Context)this).getApplicationContext(), list);
                }
            }
        }
        if (a91.c((Context)this)) {
            final ArrayList<ArchivedLocalDataModel> archiveLocalDataModels = instance.getArchiveLocalDataModels();
            if (archiveLocalDataModels != null && archiveLocalDataModels.size() > 0) {
                final Iterator iterator2 = archiveLocalDataModels.iterator();
                while (iterator2.hasNext()) {
                    this.b((ArchivedLocalDataModel)iterator2.next());
                }
            }
        }
        final ArrayList<String> pendingArchiveIds = ArchivedPendingDelete.getPendingArchiveIds((Context)this);
        if (pendingArchiveIds != null && pendingArchiveIds.size() > 0) {
            final Iterator iterator3 = pendingArchiveIds.iterator();
            while (iterator3.hasNext()) {
                this.i((String)iterator3.next());
            }
        }
    }
    
    public final boolean o(final String str) {
        final o30 f = o30.f();
        final r30 e = FirebaseAuth.getInstance().e();
        if (e == null || f == null) {
            return false;
        }
        final File parent = new File(((Context)this).getApplicationContext().getExternalFilesDir(ok.c), ok.d);
        if (!parent.exists()) {
            parent.mkdirs();
        }
        final StringBuilder sb = new StringBuilder();
        sb.append(str);
        sb.append(".exm");
        final String string = sb.toString();
        final File file = new File(parent, string);
        if (!file.exists()) {
            return false;
        }
        final jq1 n = f.n();
        final StringBuilder sb2 = new StringBuilder();
        sb2.append(ok.d);
        sb2.append("/");
        sb2.append(e.O());
        sb2.append("/");
        sb2.append(string);
        final i12 l = n.a(sb2.toString()).l(Uri.fromFile(file));
        final zs1 zs1 = new zs1();
        l.p((OnFailureListener)new OnFailureListener(this, zs1) {
            public final zs1 a;
            public final ArchiveSyncService b;
            
            public void onFailure(final Exception ex) {
                final zs1 a = this.a;
                a.b = true;
                a.a = false;
            }
        }).u((OnSuccessListener)new OnSuccessListener(this, zs1) {
            public final zs1 a;
            public final ArchiveSyncService b;
            
            public void a(final i12.b b) {
                final zs1 a = this.a;
                a.b = true;
                a.a = true;
            }
        }).s(new o11(this, str) {
            public final String a;
            public final ArchiveSyncService b;
            
            public void b(final i12.b b) {
                this.b.e((int)(b.a() * 1.0 / 1.0 * b.b() * 70.0 + 20.0), this.a);
            }
        });
        while (System.currentTimeMillis() - zs1.c < 600000L && !zs1.b) {
            try {
                Thread.sleep(500L);
            }
            catch (final InterruptedException ex) {
                ex.printStackTrace();
            }
        }
        return zs1.a;
    }
    
    public void onHandleIntent(final Intent intent) {
        if (intent != null && "ACTION_SYNC_ARCHIVE".equals(intent.getAction())) {
            this.m();
        }
    }
    
    public final boolean p(ArchivedLocalDataModel archivedLocalDataModel) {
        final FirebaseFirestore f = FirebaseFirestore.f();
        final r30 e = FirebaseAuth.getInstance().e();
        final HashMap hashMap = new HashMap();
        hashMap.put(archivedLocalDataModel.getTemplateId(), archivedLocalDataModel);
        archivedLocalDataModel = (ArchivedLocalDataModel)new zs1();
        f.a("ArchivedExams").g(e.O()).q(hashMap, rm1.c()).addOnFailureListener((OnFailureListener)new OnFailureListener(this, archivedLocalDataModel) {
            public final zs1 a;
            public final ArchiveSyncService b;
            
            public void onFailure(final Exception ex) {
                final zs1 a = this.a;
                a.b = true;
                a.a = false;
            }
        }).addOnSuccessListener((OnSuccessListener)new OnSuccessListener(this, archivedLocalDataModel) {
            public final zs1 a;
            public final ArchiveSyncService b;
            
            public void a(final Void void1) {
                final zs1 a = this.a;
                a.b = true;
                a.a = true;
            }
        });
        while (System.currentTimeMillis() - ((zs1)archivedLocalDataModel).c < 600000L && !((zs1)archivedLocalDataModel).b) {
            try {
                Thread.sleep(500L);
            }
            catch (final InterruptedException ex) {
                ex.printStackTrace();
            }
        }
        return ((zs1)archivedLocalDataModel).a;
    }
}

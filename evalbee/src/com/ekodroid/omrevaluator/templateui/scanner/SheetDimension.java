// 
// Decompiled by Procyon v0.6.0
// 

package com.ekodroid.omrevaluator.templateui.scanner;

import com.ekodroid.omrevaluator.templateui.models.Point2Double;
import java.util.ArrayList;
import java.io.Serializable;

public class SheetDimension implements Serializable
{
    private double bubbleSize;
    private int height;
    public ArrayList<Point2Double> predictedMarkers;
    public Point2Double[][] sheetMarkers;
    private int width;
    
    public SheetDimension() {
        this.predictedMarkers = new ArrayList<Point2Double>();
    }
    
    public double getBubbleSize() {
        final Point2Double[][] sheetMarkers = this.sheetMarkers;
        return this.bubbleSize = (sheetMarkers[sheetMarkers.length - 1][0].y - sheetMarkers[0][0].y) / ((sheetMarkers.length - 1) * 5 + 1) / 2.0;
    }
    
    public ArrayList<Point2Double> getCorners() {
        final ArrayList list = new ArrayList();
        final Point2Double[][] sheetMarkers = this.sheetMarkers;
        if (sheetMarkers == null) {
            return list;
        }
        list.add(sheetMarkers[0][0]);
        final Point2Double[] array = this.sheetMarkers[0];
        list.add(array[array.length - 1]);
        final Point2Double[][] sheetMarkers2 = this.sheetMarkers;
        list.add(sheetMarkers2[sheetMarkers2.length - 1][sheetMarkers2[0].length - 1]);
        final Point2Double[][] sheetMarkers3 = this.sheetMarkers;
        list.add(sheetMarkers3[sheetMarkers3.length - 1][0]);
        return list;
    }
    
    public ArrayList<Point2Double> getPredictedMarkers() {
        return this.predictedMarkers;
    }
    
    public void setHeight(final int height) {
        this.height = height;
    }
    
    public void setWidth(final int width) {
        this.width = width;
    }
}

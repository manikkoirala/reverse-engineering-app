// 
// Decompiled by Procyon v0.6.0
// 

package com.ekodroid.omrevaluator.templateui.scanner;

import java.util.Iterator;
import java.util.ArrayList;
import java.io.Serializable;

public class ResultItem implements Serializable
{
    private ArrayList<AnswerValue> answerValues;
    private int examSet;
    private int[] sectionsInSubjects;
    
    public ResultItem(final ArrayList<AnswerValue> answerValues, final int[] sectionsInSubjects, final int examSet) {
        this.answerValues = answerValues;
        this.sectionsInSubjects = sectionsInSubjects;
        this.examSet = examSet;
    }
    
    public ArrayList<AnswerValue> getAnswerValue2s() {
        return this.answerValues;
    }
    
    public int getExamSet() {
        return this.examSet;
    }
    
    public double[] getMarksForEachSubject() {
        final double[] array = new double[this.sectionsInSubjects.length];
        for (final AnswerValue answerValue : this.answerValues) {
            final int subjectId = answerValue.getSubjectId();
            array[subjectId] += ve1.o(answerValue.getMarksForAnswer());
        }
        return array;
    }
    
    public int[] getSectionsInSubjects() {
        return this.sectionsInSubjects;
    }
    
    public double getTotalMarks() {
        final Iterator<AnswerValue> iterator = this.answerValues.iterator();
        double n = 0.0;
        while (iterator.hasNext()) {
            n += iterator.next().getMarksForAnswer();
        }
        return n;
    }
    
    public void setAnswerValues(final ArrayList<AnswerValue> answerValues) {
        this.answerValues = answerValues;
    }
    
    public void setExamSet(final int examSet) {
        this.examSet = examSet;
    }
    
    public void setSectionsInSubjects(final int[] sectionsInSubjects) {
        this.sectionsInSubjects = sectionsInSubjects;
    }
}

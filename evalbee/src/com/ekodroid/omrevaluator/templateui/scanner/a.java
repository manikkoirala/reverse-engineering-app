// 
// Decompiled by Procyon v0.6.0
// 

package com.ekodroid.omrevaluator.templateui.scanner;

import java.util.Iterator;
import java.util.ArrayList;

public class a
{
    public ArrayList a;
    public d91 b;
    
    public a(final d91 b) {
        this.a = new ArrayList();
        this.b = b;
    }
    
    public SheetResult a(SheetResult e) {
        monitorenter(this);
        if (e == null) {
            monitorexit(this);
            return null;
        }
        try {
            if (this.a.size() > 0) {
                if (this.f(e)) {
                    this.a.clear();
                    return e;
                }
                if (this.a.size() > 2) {
                    e = this.e(this.a.get(1), this.a.get(2), e);
                    this.a.clear();
                    return e;
                }
            }
            e.checkedBitmap.recycle();
            e.checkedBitmap = null;
            this.a.add(e);
            final d91 b = this.b;
            if (b != null) {
                b.a(this.a.size());
            }
            return null;
        }
        finally {
            monitorexit(this);
        }
    }
    
    public final boolean b(final boolean[] array, final boolean[] array2) {
        if (array.length != array2.length) {
            return false;
        }
        boolean b = true;
        for (int i = 0; i < array.length; ++i) {
            b = (b && array[i] == array2[i]);
            if (!b) {
                return false;
            }
        }
        return b;
    }
    
    public final boolean c(final SheetResult sheetResult, final SheetResult sheetResult2) {
        final ArrayList<AnswerValue> markedAnswers = sheetResult.getMarkedAnswers();
        final ArrayList<AnswerValue> markedAnswers2 = sheetResult2.getMarkedAnswers();
        if (sheetResult.getRollNumber() == sheetResult2.getRollNumber() && markedAnswers.size() == markedAnswers2.size()) {
            boolean b = true;
            for (int i = 0; i < markedAnswers.size(); ++i) {
                b = (b && this.b(((AnswerValue)markedAnswers.get(i)).markedValues, ((AnswerValue)markedAnswers2.get(i)).markedValues));
                if (!b) {
                    return false;
                }
            }
            return b;
        }
        return false;
    }
    
    public int d() {
        return this.a.size();
    }
    
    public final SheetResult e(final SheetResult sheetResult, final SheetResult sheetResult2, final SheetResult sheetResult3) {
        if (sheetResult.getRollNumber() == sheetResult2.getRollNumber()) {
            sheetResult3.rollNumber = sheetResult.getRollNumber();
        }
        for (int size = sheetResult3.getMarkedAnswers().size(), i = 0; i < size; ++i) {
            for (int j = 0; j < sheetResult3.markedAnswers.get(i).markedValues.length; ++j) {
                if (sheetResult.markedAnswers.get(i).markedValues[j] == sheetResult2.markedAnswers.get(i).markedValues[j]) {
                    sheetResult3.markedAnswers.get(i).markedValues[j] = sheetResult.markedAnswers.get(i).markedValues[j];
                }
            }
        }
        return sheetResult3;
    }
    
    public final boolean f(final SheetResult sheetResult) {
        final Iterator iterator = this.a.iterator();
        while (iterator.hasNext()) {
            if (this.c((SheetResult)iterator.next(), sheetResult)) {
                return true;
            }
        }
        return false;
    }
}

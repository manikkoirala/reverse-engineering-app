// 
// Decompiled by Procyon v0.6.0
// 

package com.ekodroid.omrevaluator.templateui.scanner;

import java.util.ArrayList;
import android.graphics.Bitmap;
import java.io.Serializable;

public class SheetResult implements Serializable
{
    Bitmap checkedBitmap;
    int examSet;
    ArrayList<AnswerValue> markedAnswers;
    int pageIndex;
    int rollNumber;
    SheetDimension sheetDimension;
    
    public SheetResult(final Bitmap checkedBitmap, final int rollNumber, final int examSet, final ArrayList<AnswerValue> markedAnswers, final SheetDimension sheetDimension, final int pageIndex) {
        this.checkedBitmap = checkedBitmap;
        this.rollNumber = rollNumber;
        this.markedAnswers = markedAnswers;
        this.sheetDimension = sheetDimension;
        this.examSet = examSet;
        this.pageIndex = pageIndex;
    }
    
    public Bitmap getCheckedBitmap() {
        return this.checkedBitmap;
    }
    
    public int getExamSet() {
        return this.examSet;
    }
    
    public ArrayList<AnswerValue> getMarkedAnswers() {
        return this.markedAnswers;
    }
    
    public int getRollNumber() {
        return this.rollNumber;
    }
    
    public SheetDimension getSheetDimension() {
        return this.sheetDimension;
    }
}

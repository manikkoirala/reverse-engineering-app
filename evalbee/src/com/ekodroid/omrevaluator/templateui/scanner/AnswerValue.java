// 
// Decompiled by Procyon v0.6.0
// 

package com.ekodroid.omrevaluator.templateui.scanner;

import com.ekodroid.omrevaluator.templateui.models.AnswerOption;
import java.io.Serializable;

public class AnswerValue implements Serializable
{
    MarkedState markedState;
    boolean[] markedValues;
    double marksForAnswer;
    String payload;
    int questionNumber;
    int sectionId;
    int subjectId;
    AnswerOption.AnswerOptionType type;
    
    public AnswerValue(final AnswerOption.AnswerOptionType type, final int questionNumber, final boolean[] markedValues, final int subjectId, final int sectionId, final String payload) {
        this.type = type;
        this.questionNumber = questionNumber;
        this.markedValues = markedValues;
        this.subjectId = subjectId;
        this.sectionId = sectionId;
        this.payload = payload;
    }
    
    public MarkedState getMarkedState() {
        return this.markedState;
    }
    
    public boolean[] getMarkedValues() {
        return this.markedValues;
    }
    
    public double getMarksForAnswer() {
        return this.marksForAnswer;
    }
    
    public String getPayload() {
        return this.payload;
    }
    
    public int getQuestionNumber() {
        return this.questionNumber;
    }
    
    public int getSectionId() {
        return this.sectionId;
    }
    
    public int getSubjectId() {
        return this.subjectId;
    }
    
    public AnswerOption.AnswerOptionType getType() {
        return this.type;
    }
    
    public void setMarkedState(final MarkedState markedState) {
        this.markedState = markedState;
    }
    
    public void setMarkedValues(final boolean[] markedValues) {
        this.markedValues = markedValues;
    }
    
    public void setMarksForAnswer(final double marksForAnswer) {
        this.marksForAnswer = marksForAnswer;
    }
    
    public void setPayload(final String payload) {
        this.payload = payload;
    }
    
    public enum MarkedState
    {
        private static final MarkedState[] $VALUES;
        
        ATTEMPTED, 
        CORRECT, 
        INCORRECT, 
        INVALID, 
        PARTIAL_CORRECT, 
        UNATTEMPTED;
        
        private static /* synthetic */ MarkedState[] $values() {
            return new MarkedState[] { MarkedState.CORRECT, MarkedState.INCORRECT, MarkedState.UNATTEMPTED, MarkedState.ATTEMPTED, MarkedState.PARTIAL_CORRECT, MarkedState.INVALID };
        }
        
        static {
            $VALUES = $values();
        }
    }
}

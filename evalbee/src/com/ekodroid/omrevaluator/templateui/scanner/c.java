// 
// Decompiled by Procyon v0.6.0
// 

package com.ekodroid.omrevaluator.templateui.scanner;

import com.ekodroid.omrevaluator.templateui.models.Point2Double;
import com.ekodroid.omrevaluator.templateui.models.Point2Integer;
import java.util.ArrayList;
import com.ekodroid.omrevaluator.templateui.models.SheetTemplate2;
import com.ekodroid.omrevaluator.templateui.models.AnswerOption;
import android.graphics.Bitmap;

public class c
{
    public boolean a;
    public ScannerUtil.AlgoType b;
    
    public c() {
        this.a = false;
        this.b = ScannerUtil.AlgoType.DEFAULT;
    }
    
    public SheetResult a(final j5 j5, final hn0 hn0, final d91 d91, final ej1 ej1, final boolean b, final boolean b2, final int n) {
        final SheetDimension d92 = hn0.d(j5, d91, ej1, b, n);
        if (d92 == null) {
            j5.a.recycle();
            j5.a = null;
            return null;
        }
        final sn1 sn1 = new sn1(d92);
        try {
            return this.d(j5, sn1, d92, b2, n);
        }
        catch (final ScanningException ex) {
            ex.printStackTrace();
            return null;
        }
    }
    
    public final AnswerValue b(final Bitmap bitmap, final sn1 sn1, final AnswerOption answerOption, final int[] array, final double n, final boolean b) {
        return new AnswerValue(answerOption.type, answerOption.questionNumber, ScannerUtil.e(sn1.b(answerOption, array), bitmap, (int)n, this.b, b, answerOption.questionNumber), answerOption.subjectId, answerOption.sectionId, answerOption.payload);
    }
    
    public final int c(final Bitmap bitmap, final sn1 sn1, final SheetTemplate2 sheetTemplate2, final SheetDimension sheetDimension, final boolean b, int i) {
        final AnswerOption examSetOption = sheetTemplate2.getExamSetOption();
        final int n = 0;
        if (examSetOption == null || sheetTemplate2.getTemplateParams().getExamSets() == 1) {
            return 0;
        }
        if (sheetTemplate2.getExamSetOption().pageIndex != i) {
            return -1;
        }
        boolean[] e;
        for (e = ScannerUtil.e(sn1.b(sheetTemplate2.getExamSetOption(), sheetTemplate2.getColumnWidthInBubbles(i)), bitmap, (int)sheetDimension.getBubbleSize(), this.b, b, -1), i = n; i < e.length; ++i) {
            if (e[i]) {
                return i + 1;
            }
        }
        return -1;
    }
    
    public final SheetResult d(final j5 j5, final sn1 sn1, final SheetDimension sheetDimension, final boolean b, final int n) {
        final ArrayList<AnswerOption> answerOptions = j5.c.getAnswerOptions();
        final Bitmap a = j5.a;
        final int c = this.c(a, sn1, j5.c, sheetDimension, b, n);
        final int rollStartDigit = j5.c.getLabelProfile().getRollStartDigit();
        final AnswerOption answerOption = (AnswerOption)answerOptions.get(0);
        final int subIndex = answerOption.subIndex;
        final int[] columnWidthInBubbles = j5.c.getColumnWidthInBubbles(n);
        final int column = answerOption.column;
        int i = 1;
        final int e = this.e(a, sn1, answerOption, sheetDimension, subIndex, columnWidthInBubbles[column - 1], rollStartDigit, b, n);
        final ArrayList list = new ArrayList();
        while (i < answerOptions.size()) {
            final AnswerOption answerOption2 = answerOptions.get(i);
            if (answerOption2.pageIndex == n) {
                list.add(this.b(a, sn1, answerOption2, j5.c.getColumnWidthInBubbles(n), sheetDimension.getBubbleSize(), b));
            }
            ++i;
        }
        return new SheetResult(a, e, c, list, sheetDimension, n);
    }
    
    public final int e(final Bitmap bitmap, final sn1 sn1, final AnswerOption answerOption, final SheetDimension sheetDimension, final int n, int i, int n2, final boolean b, int j) {
        if (answerOption.type != AnswerOption.AnswerOptionType.ID_BLOCK) {
            throw new ScanningException();
        }
        final int pageIndex = answerOption.pageIndex;
        final int n3 = 0;
        if (pageIndex != j) {
            return 0;
        }
        final int[] array = new int[n];
        final Point2Double[][] h = sn1.h(new Point2Integer(answerOption.column, answerOption.row), n, i);
        boolean[] e;
        for (i = 0; i < 10; ++i) {
            e = ScannerUtil.e(h[i], bitmap, (int)sheetDimension.getBubbleSize(), this.b, b, answerOption.questionNumber);
            for (j = 0; j < h[i].length; ++j) {
                if (e[j]) {
                    array[j] = (i + n2) % 10;
                }
            }
        }
        n2 = 0;
        for (i = n3; i < n; ++i) {
            n2 += array[n - 1 - i] * (int)Math.pow(10.0, i);
        }
        return n2;
    }
}

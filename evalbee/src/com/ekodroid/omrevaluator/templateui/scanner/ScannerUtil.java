// 
// Decompiled by Procyon v0.6.0
// 

package com.ekodroid.omrevaluator.templateui.scanner;

import android.graphics.Color;
import com.ekodroid.omrevaluator.templateui.models.Point2Double;
import android.graphics.Bitmap;

public abstract class ScannerUtil
{
    public static double a(final Bitmap bitmap, int p6, int o, int n, int p7, int o2) {
        final double n2 = 100.0 / o2;
        final int n3 = 0;
        if (o < 1) {
            o2 = 1;
        }
        else {
            o2 = 0;
        }
        final boolean b = o > bitmap.getHeight() - 2;
        final boolean b2 = p6 < 1;
        final boolean b3 = p6 > bitmap.getWidth() - 2;
        double n4 = 0.0;
        if ((o2 | (b ? 1 : 0) | (b2 ? 1 : 0) | (b3 ? 1 : 0)) != 0x0) {
            return 0.0;
        }
        o2 = o(o - p7 / 2, 1);
        p7 = p(o + (p7 + 1) / 2, bitmap.getHeight() - 2);
        o = o(p6 - n / 2, 1);
        p6 = p(p6 + (n + 1) / 2, bitmap.getWidth() - 2);
        double n5 = o;
        double n6 = 1.0;
        double n7 = 0.75;
        n = o2;
        o = n3;
        while (n5 < p6) {
            final double n8 = n;
            final double n9 = n6;
            double n10 = n7;
            double n11 = n9;
            double n13;
            double n14;
            for (double n12 = n8; n12 < p7; n12 += n2, n11 = n14, n10 = n13) {
                final double f = f(bitmap.getPixel((int)n5, (int)n12));
                n13 = n10;
                if (f > n10) {
                    n13 = f;
                }
                n14 = n11;
                if (f < n11) {
                    n14 = f;
                }
                n4 += f;
                ++o;
            }
            n5 += n2;
            final double n15 = n10;
            n6 = n11;
            n7 = n15;
        }
        if (o == 0) {
            o = 1;
        }
        final double n16 = n4 / o;
        final double n17 = (n7 + n6) / 2.0;
        if (n16 > n17) {
            return n17;
        }
        return n16;
    }
    
    public static double b(final Bitmap bitmap, int n, int p7, int n2, int p8, int o, double n3) {
        final double n4 = 100.0 / o;
        final int n5 = 0;
        if (p7 < 1) {
            o = 1;
        }
        else {
            o = 0;
        }
        final boolean b = p7 > bitmap.getHeight() - 2;
        final boolean b2 = n < 1;
        final boolean b3 = n > bitmap.getWidth() - 2;
        double n6 = 3.0;
        if ((o | (b ? 1 : 0) | (b2 ? 1 : 0) | (b3 ? 1 : 0)) != 0x0) {
            return 3.0;
        }
        o = o(p7 - p8 / 2, 1);
        p8 = p(p7 + (p8 + 1) / 2, bitmap.getHeight() - 2);
        final int o2 = o(n - n2 / 2, 1);
        p7 = p(n + (n2 + 1) / 2, bitmap.getWidth() - 2);
        double n7 = o2;
        double n8 = 0.0;
        double n9 = 1.0;
        n2 = o;
        n = n5;
        while (n7 < p7) {
            double n11;
            double n12;
            for (double n10 = n2; n10 < p8; n10 += n4, n8 = n11, n9 = n12) {
                final double f = f(bitmap.getPixel((int)n7, (int)n10));
                n11 = n8;
                if (f > n8) {
                    n11 = f;
                }
                n12 = n9;
                if (f < n9) {
                    n12 = f;
                }
                n6 += f;
                ++n;
            }
            n7 += n4;
        }
        if (n == 0) {
            n = 1;
        }
        final double n13 = n6 / n;
        final double n14 = n13 + (n8 - n13) * (n3 + 0.2);
        n3 += 0.3;
        if (n14 < 1.0 && n14 > n3) {
            return n14;
        }
        return n3;
    }
    
    public static Point2Double c(final Bitmap bitmap, final int n, int n2, int n3, final boolean b, final double n4) {
        final int n5 = 0;
        if (n3 < n | n3 > bitmap.getHeight() - n | n2 < n | n2 > bitmap.getWidth() - n) {
            return new Point2Double(n2, n3);
        }
        final Point2Double point2Double = new Point2Double();
        int i = 0;
        int n6 = 0;
        while (i < n) {
            final int n7 = n3 + i;
            if (n7 + 2 > bitmap.getHeight()) {
                break;
            }
            if (!k(bitmap.getPixel(n2, n7), n4) && !k(bitmap.getPixel(n2, n7 + 1), n4)) {
                break;
            }
            n6 = i;
            ++i;
        }
        int j = 0;
        int n8 = 0;
        while (j < n) {
            final int n9 = n3 - j;
            if (n9 - 2 < 0) {
                break;
            }
            if (!k(bitmap.getPixel(n2, n9), n4) && !k(bitmap.getPixel(n2, n9 - 1), n4)) {
                break;
            }
            n8 = j;
            ++j;
        }
        int k = 0;
        int n10 = 0;
        while (k < n) {
            final int n11 = n2 - k;
            if (n11 - 2 < 0) {
                break;
            }
            if (!k(bitmap.getPixel(n11, n3), n4) && !k(bitmap.getPixel(n11 - 1, n3), n4)) {
                break;
            }
            n10 = k;
            ++k;
        }
        int n12 = 0;
        for (int l = n5; l < n; ++l) {
            final int n13 = n2 + l;
            if (n13 + 2 > bitmap.getWidth()) {
                break;
            }
            if (!k(bitmap.getPixel(n13, n3), n4) && !k(bitmap.getPixel(n13 + 1, n3), n4)) {
                break;
            }
            n12 = l;
        }
        final int n14 = (n6 - n8) / 2;
        final int n15 = (n12 - n10) / 2;
        final int o = o(n6 + n8 + 1, n12 + n10 + 1);
        int n16 = n2 + n15;
        int n17 = n3 + n14;
        while (true) {
            final double d = d(bitmap, o, o, n16, n17, n4);
            n2 = n17 + 1;
            final double d2 = d(bitmap, o, o, n16, n2, n4);
            n3 = n17 - 1;
            if (d2 < d(bitmap, o, o, n3, n16, n4)) {
                n2 = n3;
            }
            n3 = n16 - 1;
            final double d3 = d(bitmap, o, o, n3, n17, n4);
            final int n18 = n16 + 1;
            if (d3 <= d(bitmap, o, o, n17, n18, n4)) {
                n3 = n18;
            }
            if (d(bitmap, o, o, n3, n2, n4) <= d) {
                break;
            }
            n16 = n3;
            n17 = n2;
        }
        point2Double.x = n16;
        point2Double.y = n17;
        if (b) {
            return point2Double;
        }
        return c(bitmap, n, n16, n17, true, n4);
    }
    
    public static double d(final Bitmap bitmap, int i, int j, int p6, int o, final double n) {
        boolean b = false;
        final boolean b2 = o < 1;
        final boolean b3 = o > bitmap.getHeight() - 2;
        final boolean b4 = p6 < 1;
        if (p6 > bitmap.getWidth() - 2) {
            b = true;
        }
        double n2 = 0.0;
        if (b | (b2 | b3 | b4)) {
            return 0.0;
        }
        final int o2 = o(o - i / 2, 1);
        final int p7 = p(o + (i + 1) / 2, bitmap.getHeight() - 2);
        o = o(p6 - j / 2, 1);
        p6 = p(p6 + (j + 1) / 2, bitmap.getWidth() - 2);
        double n3;
        for (i = o2; i < p7; ++i) {
            for (j = o; j < p6; ++j, n2 = n3) {
                n3 = n2;
                if (k(bitmap.getPixel(j, i), n)) {
                    n3 = n2 + 1.0;
                }
            }
        }
        return n2;
    }
    
    public static boolean[] e(final Point2Double[] array, final Bitmap bitmap, final int n, final AlgoType algoType, final boolean b, final int n2) {
        final boolean[] array2 = new boolean[array.length];
        for (int i = 0; i < array.length; ++i) {
            final Point2Double point2Double = array[i];
            array2[i] = m(bitmap, (int)point2Double.x, (int)point2Double.y, n, 0.5, b, n2, i);
        }
        return array2;
    }
    
    public static double f(final int n) {
        return 1.0 - (Color.red(n) * 0.299 + Color.green(n) * 0.587 + Color.blue(n) * 0.114) / 255.0;
    }
    
    public static Point2Double g(final Point2Double point2Double, final Point2Double point2Double2, final int n, final int n2) {
        final Point2Double point2Double3 = new Point2Double();
        final double x = point2Double.x;
        final double x2 = point2Double2.x;
        final double n3 = n;
        final double n4 = n2 - 1;
        point2Double3.x = x + (x2 - x) * n3 / n4;
        final double y = point2Double.y;
        point2Double3.y = y + (point2Double2.y - y) * n3 / n4;
        return point2Double3;
    }
    
    public static boolean h(final Bitmap bitmap, int n, int n2, int n3, final double n4) {
        boolean b = false;
        if (n2 < 1 | n2 > bitmap.getHeight() - 2 | n < 1 | n > bitmap.getWidth() - 2) {
            return false;
        }
        final int n5 = n3 / 2;
        final int o = o(n2 - n5, 1);
        n3 = (n3 + 1) / 2;
        final int p5 = p(n2 + n3, bitmap.getHeight() - 2);
        final int o2 = o(n - n5, 1);
        final int p6 = p(n + n3, bitmap.getWidth() - 2);
        n = 0;
        n2 = 0;
        int n6 = o2;
        int i;
        int n7;
        while (true) {
            i = o;
            n3 = n;
            n7 = n2;
            if (n6 >= p6) {
                break;
            }
            n3 = n;
            if (!k(bitmap.getPixel(n6, o), n4)) {
                n3 = n + 1;
            }
            n = n3;
            if (!k(bitmap.getPixel(n6, p5), n4)) {
                n = n3 + 1;
            }
            n2 = n2 + 1 + 1;
            ++n6;
        }
        while (i < p5) {
            n = n3;
            if (!k(bitmap.getPixel(o2, i), n4)) {
                n = n3 + 1;
            }
            n3 = n;
            if (!k(bitmap.getPixel(o2, i), n4)) {
                n3 = n + 1;
            }
            n7 = n7 + 1 + 1;
            ++i;
        }
        if (n3 * 100 / n7 > 80) {
            b = true;
        }
        return b;
    }
    
    public static boolean i(final Bitmap bitmap, final int n, int n2, int i, final double n3) {
        final int n4 = i * 2;
        final boolean b = false;
        final boolean b2 = n2 < i;
        final boolean b3 = n2 > bitmap.getHeight() - i;
        final boolean b4 = n < i;
        if (n > bitmap.getWidth() - i) {
            i = 1;
        }
        else {
            i = 0;
        }
        if ((i | ((b2 | b3 | b4) ? 1 : 0)) != 0x0) {
            return false;
        }
        i = n4 / 2;
        final int o = o(n2 - i, 1);
        final int n5 = (n4 + 1) / 2;
        final int p5 = p(n2 + n5, bitmap.getHeight() - 2);
        final int o2 = o(n - i, 1);
        final int p6 = p(n5 + n, bitmap.getWidth() - 2);
        i = n;
        while (true) {
            while (i < p6) {
                if (!k(bitmap.getPixel(i, n2), n3)) {
                    i = 1;
                    if (i == 0) {
                        return false;
                    }
                    i = n;
                    while (true) {
                        while (i > o2) {
                            if (!k(bitmap.getPixel(i, n2), n3)) {
                                i = 1;
                                if (i == 0) {
                                    return false;
                                }
                                i = n2;
                                while (true) {
                                    while (i > o) {
                                        if (!k(bitmap.getPixel(n, i), n3)) {
                                            i = 1;
                                            if (i == 0) {
                                                return false;
                                            }
                                            boolean b5;
                                            while (true) {
                                                b5 = b;
                                                if (n2 >= p5) {
                                                    break;
                                                }
                                                if (!k(bitmap.getPixel(n, n2), n3)) {
                                                    b5 = true;
                                                    break;
                                                }
                                                ++n2;
                                            }
                                            return b5;
                                        }
                                        else {
                                            --i;
                                        }
                                    }
                                    i = 0;
                                    continue;
                                }
                            }
                            else {
                                --i;
                            }
                        }
                        i = 0;
                        continue;
                    }
                }
                else {
                    ++i;
                }
            }
            i = 0;
            continue;
        }
    }
    
    public static boolean j(final Bitmap bitmap, final int n, final int n2, final int n3, final double n4) {
        final int n5 = n3 * 2;
        return d(bitmap, n3, n3, n, n2, a(bitmap, n, n2, n5, n5, 50)) > n3 * n3 * n4;
    }
    
    public static boolean k(final int n, final double n2) {
        return f(n) > n2;
    }
    
    public static boolean l(final Bitmap bitmap, final Point2Double point2Double, final int n, final double n2) {
        return d(bitmap, n, n, (int)point2Double.x, (int)point2Double.y, n2) > n * n * 0.5 && h(bitmap, (int)point2Double.x, (int)point2Double.y, (int)(n * 2.5), n2);
    }
    
    public static boolean m(final Bitmap bitmap, final int n, final int n2, final int n3, final double n4, final boolean b, final int n5, final int n6) {
        return n(bitmap, n, n2, n3, n4, b, n5, n6);
    }
    
    public static boolean n(final Bitmap bitmap, int n, int n2, int n3, double f, final boolean b, int i, int n4) {
        i = n3;
        if (n < i || n2 < i || n > bitmap.getWidth() - i || n2 > bitmap.getHeight() - i) {
            return false;
        }
        final int n5 = i / 3;
        n4 = i * 2 / 3 + 1;
        double n6 = 0.0;
        n3 = 0;
        while (i > n4) {
            final int n7 = n - i;
            final int n8 = n2 - i;
            final double f2 = f(bitmap.getPixel(n7, n8));
            final int n9 = n2 + i;
            f = f(bitmap.getPixel(n7, n9));
            final int n10 = n + i;
            n6 += f2 + 0.0 + f + f(bitmap.getPixel(n10, n9)) + f(bitmap.getPixel(n10, n8));
            n3 += 4;
            --i;
        }
        double n11 = 0.0;
        double n12 = 0.0;
        final int n13 = 0;
        i = n4;
        n4 = n13;
        while (i > n5) {
            final int n14 = n - i;
            final int n15 = n2 - i;
            final double f3 = f(bitmap.getPixel(n14, n15));
            f = n12;
            if (f3 > n12) {
                f = f3;
            }
            final int n16 = n2 + i;
            final double f4 = f(bitmap.getPixel(n14, n16));
            double n17 = f;
            if (f4 > f) {
                n17 = f4;
            }
            final int n18 = n + i;
            final double f5 = f(bitmap.getPixel(n18, n16));
            f = n17;
            if (f5 > n17) {
                f = f5;
            }
            final double f6 = f(bitmap.getPixel(n18, n15));
            n12 = f;
            if (f6 > f) {
                n12 = f6;
            }
            n11 += f3 + 0.0 + f4 + f5 + f6;
            n4 += 4;
            --i;
        }
        f = n11 / n4;
        final double n19 = ((n12 + 1.0) / 2.0 + n11) / (n4 + 1);
        final double n20 = (n6 + n11) / (n3 + n4);
        int j = n5 * 4 / 3 + 1;
        i = 0;
        n3 = 0;
        int n21 = 0;
        double n22 = 0.0;
        while (j > 0) {
            final int n23 = n - j;
            final int n24 = n2 - j;
            final double f7 = f(bitmap.getPixel(n23, n24));
            n4 = i;
            if (f7 > n19) {
                n4 = i + 1;
            }
            int n25 = n3;
            if (f7 > n20) {
                n25 = n3 + 1;
            }
            final int n26 = n2 + j;
            final double f8 = f(bitmap.getPixel(n23, n26));
            i = n4;
            if (f8 > n19) {
                i = n4 + 1;
            }
            n3 = n25;
            if (f8 > n20) {
                n3 = n25 + 1;
            }
            final int n27 = n + j;
            final double f9 = f(bitmap.getPixel(n27, n26));
            n4 = i;
            if (f9 > n19) {
                n4 = i + 1;
            }
            i = n3;
            if (f9 > n20) {
                i = n3 + 1;
            }
            final double f10 = f(bitmap.getPixel(n27, n24));
            n3 = n4;
            if (f10 > n19) {
                n3 = n4 + 1;
            }
            n4 = i;
            if (f10 > n20) {
                n4 = i + 1;
            }
            n22 += f7 + 0.0 + f8 + f9 + f10;
            n21 += 4;
            --j;
            i = n3;
            n3 = n4;
        }
        final double f11 = f(bitmap.getPixel(n, n2));
        n = i;
        if (f11 > n19) {
            n = i + 1;
        }
        n2 = n3;
        if (f11 > n20) {
            n2 = n3 + 1;
        }
        i = n21 + 1;
        final double n28 = (n22 + f11) / i;
        if (n28 < n20 && n20 < f) {
            return false;
        }
        n3 = i * 2 / 3 - 1;
        if (n28 > n19 && n > n3) {
            return true;
        }
        i /= 3;
        return n28 > n20 && n > i - 1 && n2 > n3;
    }
    
    public static int o(final int n, final int n2) {
        if (n < n2) {
            return n2;
        }
        return n;
    }
    
    public static int p(final int n, final int n2) {
        if (n < n2) {
            return n;
        }
        return n2;
    }
    
    public enum AlgoType
    {
        private static final AlgoType[] $VALUES;
        
        DEFAULT, 
        ROW_AVG;
        
        private static /* synthetic */ AlgoType[] $values() {
            return new AlgoType[] { AlgoType.DEFAULT, AlgoType.ROW_AVG };
        }
        
        static {
            $VALUES = $values();
        }
    }
}

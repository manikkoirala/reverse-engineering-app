// 
// Decompiled by Procyon v0.6.0
// 

package com.ekodroid.omrevaluator.templateui.scanner;

import android.graphics.Bitmap$Config;
import android.graphics.BitmapFactory$Options;
import android.graphics.Bitmap;
import android.graphics.Paint$Align;
import android.graphics.Paint$Style;
import android.graphics.Paint;
import java.util.ArrayList;
import com.ekodroid.omrevaluator.templateui.models.Point2Double;
import java.util.Iterator;
import com.ekodroid.omrevaluator.templateui.models.Point2Integer;
import com.ekodroid.omrevaluator.templateui.models.AnswerOptionKey;
import com.ekodroid.omrevaluator.templateui.models.AnswerOption;
import com.ekodroid.omrevaluator.templateui.models.SheetTemplate2;
import android.graphics.Canvas;
import android.graphics.Color;

public class b
{
    public static int a;
    public static int b;
    public static int c;
    public static int d;
    public static int e;
    
    static {
        com.ekodroid.omrevaluator.templateui.scanner.b.a = Color.argb(255, 0, 0, 250);
        com.ekodroid.omrevaluator.templateui.scanner.b.b = Color.argb(255, 0, 0, 250);
        com.ekodroid.omrevaluator.templateui.scanner.b.c = Color.argb(255, 0, 255, 0);
        com.ekodroid.omrevaluator.templateui.scanner.b.d = Color.argb(255, 255, 0, 0);
        com.ekodroid.omrevaluator.templateui.scanner.b.e = Color.argb(255, 255, 221, 0);
    }
    
    public final void a(final Canvas canvas, final SheetTemplate2 sheetTemplate2, final ResultItem resultItem, final SheetDimension sheetDimension, int i, final int n) {
        final sn1 sn1 = new sn1(sheetDimension);
        final int n2 = (int)(sheetDimension.getBubbleSize() / 2.0 + 1.0);
        for (final AnswerValue answerValue : resultItem.getAnswerValue2s()) {
            final AnswerOption answerOption = sheetTemplate2.getAnswerOptions().get(answerValue.getQuestionNumber());
            if (answerOption.pageIndex != n) {
                continue;
            }
            final AnswerValue.MarkedState markedState = answerValue.getMarkedState();
            final boolean[] markedValues = answerValue.getMarkedValues();
            final Point2Double[] b = sn1.b(answerOption, sheetTemplate2.getColumnWidthInBubbles(n));
            for (int j = 0; j < markedValues.length; ++j) {
                if (answerValue.getMarkedValues()[j]) {
                    final Point2Double point2Double = b[j];
                    this.f(canvas, (int)point2Double.x, (int)point2Double.y, n2, this.e(markedState));
                }
            }
            if (markedState == AnswerValue.MarkedState.INVALID) {
                for (int k = 0; k < markedValues.length; ++k) {
                    final Point2Double point2Double2 = b[k];
                    this.f(canvas, (int)point2Double2.x, (int)point2Double2.y, n2, this.e(markedState));
                }
            }
            else if (answerValue.getType() != AnswerOption.AnswerOptionType.DECIMAL) {
                final ArrayList b2 = ve1.b(sheetTemplate2, resultItem.getExamSet());
                if (b2 == null || b2.size() <= 0) {
                    continue;
                }
                for (int l = 0; l < markedValues.length; ++l) {
                    if (b2.get(answerValue.getQuestionNumber() - 1).getMarkedValues()[l] && answerValue.getMarkedState() != AnswerValue.MarkedState.CORRECT) {
                        final Point2Double point2Double3 = b[l];
                        this.g(canvas, (int)point2Double3.x, (int)point2Double3.y, com.ekodroid.omrevaluator.templateui.scanner.b.e);
                    }
                }
            }
            else {
                final ArrayList b3 = ve1.b(sheetTemplate2, resultItem.getExamSet());
                if (b3 == null || b3.size() <= 0) {
                    continue;
                }
                String s;
                int n3;
                int n4;
                int n5;
                int n6;
                if (answerValue.getMarkedState() != AnswerValue.MarkedState.CORRECT) {
                    final Point2Double d = sn1.d(answerOption);
                    s = e5.q(b3.get(answerValue.getQuestionNumber() - 1), sheetTemplate2.getLabelProfile());
                    n3 = (int)d.x;
                    n4 = (int)(d.y - n2 * 4);
                    n5 = n2 * 3;
                    n6 = com.ekodroid.omrevaluator.templateui.scanner.b.b;
                }
                else {
                    final Point2Double d2 = sn1.d(answerOption);
                    s = e5.r(answerValue, sheetTemplate2.getLabelProfile());
                    n3 = (int)d2.x;
                    n4 = (int)(d2.y - n2 * 4);
                    n5 = n2 * 3;
                    n6 = com.ekodroid.omrevaluator.templateui.scanner.b.c;
                }
                this.d(canvas, s, n3, n4, n5, n6);
            }
        }
        final ArrayList<AnswerOption> answerOptions = sheetTemplate2.getAnswerOptions();
        final int n7 = 0;
        final AnswerOption answerOption2 = answerOptions.get(0);
        if (answerOption2.pageIndex == n) {
            final Point2Double[] a = sn1.a(i, new Point2Integer(answerOption2.column, answerOption2.row), sheetTemplate2.getTemplateParams().getRollDigits(), sheetTemplate2.getColumnWidthInBubbles(n)[answerOption2.column - 1], sheetTemplate2.getLabelProfile().getRollStartDigit());
            int length;
            Point2Double point2Double4;
            for (length = a.length, i = n7; i < length; ++i) {
                point2Double4 = a[i];
                this.f(canvas, (int)point2Double4.x, (int)point2Double4.y, n2, com.ekodroid.omrevaluator.templateui.scanner.b.e);
            }
        }
        if (sheetTemplate2.getExamSetOption() != null && resultItem.getExamSet() > 0 && sheetTemplate2.getExamSetOption().pageIndex == n) {
            final Point2Double[] b4 = sn1.b(sheetTemplate2.getExamSetOption(), sheetTemplate2.getColumnWidthInBubbles(n));
            this.f(canvas, (int)b4[resultItem.getExamSet() - 1].x, (int)b4[resultItem.getExamSet() - 1].y, n2, com.ekodroid.omrevaluator.templateui.scanner.b.e);
        }
    }
    
    public final void b(final Canvas canvas, final ArrayList list) {
        for (final Point2Double point2Double : list) {
            this.g(canvas, (int)point2Double.x, (int)point2Double.y, com.ekodroid.omrevaluator.templateui.scanner.b.a);
        }
    }
    
    public final void c(final Canvas canvas, final ArrayList list, final double n) {
        for (final Point2Double point2Double : list) {
            this.h(canvas, (int)point2Double.x, (int)point2Double.y, (int)(n / 2.0 + 1.0), com.ekodroid.omrevaluator.templateui.scanner.b.d);
        }
    }
    
    public final void d(final Canvas canvas, final String s, final int n, final int n2, final int n3, final int color) {
        final Paint paint = new Paint();
        paint.setStyle(Paint$Style.FILL);
        paint.setStrokeWidth(2.0f);
        paint.setAntiAlias(true);
        paint.setColor(color);
        paint.setTextAlign(Paint$Align.CENTER);
        if (s == null) {
            return;
        }
        paint.setTextSize((float)n3);
        canvas.drawText(s, (float)n, (float)(n2 + n3 / 2 - 2), paint);
    }
    
    public final int e(final AnswerValue.MarkedState markedState) {
        final int n = b$a.a[markedState.ordinal()];
        if (n == 1) {
            return com.ekodroid.omrevaluator.templateui.scanner.b.c;
        }
        if (n == 2) {
            return com.ekodroid.omrevaluator.templateui.scanner.b.d;
        }
        if (n == 3) {
            return com.ekodroid.omrevaluator.templateui.scanner.b.e;
        }
        if (n != 4) {
            return com.ekodroid.omrevaluator.templateui.scanner.b.e;
        }
        return com.ekodroid.omrevaluator.templateui.scanner.b.b;
    }
    
    public final void f(final Canvas canvas, final int n, final int n2, final int n3, final int color) {
        final Paint paint = new Paint();
        paint.setStyle(Paint$Style.STROKE);
        paint.setStrokeWidth(2.0f);
        paint.setAntiAlias(true);
        paint.setColor(color);
        canvas.drawCircle((float)n, (float)n2, (float)n3, paint);
    }
    
    public final void g(final Canvas canvas, final int n, final int n2, final int color) {
        final Paint paint = new Paint();
        paint.setStyle(Paint$Style.FILL_AND_STROKE);
        paint.setStrokeWidth(2.0f);
        paint.setAntiAlias(true);
        paint.setColor(color);
        canvas.drawCircle((float)n, (float)n2, 2.0f, paint);
    }
    
    public final void h(final Canvas canvas, final int n, final int n2, final int n3, final int color) {
        final Paint paint = new Paint();
        paint.setStyle(Paint$Style.FILL);
        paint.setStrokeWidth(2.0f);
        paint.setAntiAlias(true);
        paint.setColor(color);
        canvas.drawRect((float)(n - n3), (float)(n2 - n3), (float)(n + n3), (float)(n2 + n3), paint);
    }
    
    public Bitmap i(final Bitmap bitmap, final SheetTemplate2 sheetTemplate2, final ResultItem resultItem, final SheetDimension sheetDimension, final int n, final int n2) {
        Bitmap copy = bitmap;
        if (bitmap != null) {
            if (sheetDimension == null) {
                copy = bitmap;
            }
            else {
                if (sheetDimension.sheetMarkers == null) {
                    return bitmap;
                }
                final BitmapFactory$Options bitmapFactory$Options = new BitmapFactory$Options();
                final Bitmap$Config argb_8888 = Bitmap$Config.ARGB_8888;
                bitmapFactory$Options.inPreferredConfig = argb_8888;
                copy = bitmap.copy(argb_8888, true);
                final Canvas canvas = new Canvas(copy);
                this.b(canvas, sheetDimension.getCorners());
                if (sheetDimension.getPredictedMarkers() != null) {
                    this.c(canvas, sheetDimension.getPredictedMarkers(), sheetDimension.getBubbleSize());
                }
                this.a(canvas, sheetTemplate2, resultItem, sheetDimension, n, n2);
            }
        }
        return copy;
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.ekodroid.omrevaluator.templateui.components;

import android.view.ScaleGestureDetector$SimpleOnScaleGestureListener;
import android.graphics.Bitmap;
import android.view.View$MeasureSpec;
import android.view.MotionEvent;
import android.view.View;
import android.view.View$OnTouchListener;
import android.widget.ImageView$ScaleType;
import android.view.ScaleGestureDetector$OnScaleGestureListener;
import android.util.AttributeSet;
import android.content.Context;
import android.view.ScaleGestureDetector;
import android.graphics.PointF;
import android.graphics.Matrix;

public class ZoomableImageView extends z6
{
    public Matrix a;
    public int b;
    public PointF c;
    public PointF d;
    public float e;
    public float f;
    public float[] g;
    public float h;
    public float i;
    public float j;
    public float k;
    public float l;
    public float m;
    public float n;
    public float p;
    public float q;
    public float t;
    public float v;
    public ScaleGestureDetector w;
    public Context x;
    
    public ZoomableImageView(final Context x, final AttributeSet set) {
        super(x, set);
        this.a = new Matrix();
        this.b = 0;
        this.c = new PointF();
        this.d = new PointF();
        this.e = 1.0f;
        this.f = 4.0f;
        this.l = 1.0f;
        super.setClickable(true);
        this.x = x;
        this.w = new ScaleGestureDetector(x, (ScaleGestureDetector$OnScaleGestureListener)new b(null));
        this.a.setTranslate(1.0f, 1.0f);
        this.g = new float[9];
        this.setImageMatrix(this.a);
        this.setScaleType(ImageView$ScaleType.MATRIX);
        ((View)this).setOnTouchListener((View$OnTouchListener)new View$OnTouchListener(this) {
            public final ZoomableImageView a;
            
            public boolean onTouch(final View view, final MotionEvent motionEvent) {
                this.a.w.onTouchEvent(motionEvent);
                final ZoomableImageView a = this.a;
                a.a.getValues(a.g);
                final float[] g = this.a.g;
                float n = g[2];
                float n2 = g[5];
                final PointF pointF = new PointF(motionEvent.getX(), motionEvent.getY());
                final int action = motionEvent.getAction();
                if (action != 0) {
                    if (action != 1) {
                        if (action != 2) {
                            if (action != 5) {
                                if (action == 6) {
                                    this.a.b = 0;
                                }
                            }
                            else {
                                this.a.c.set(motionEvent.getX(), motionEvent.getY());
                                final ZoomableImageView a2 = this.a;
                                a2.d.set(a2.c);
                                this.a.b = 2;
                            }
                        }
                        else {
                            final ZoomableImageView a3 = this.a;
                            final int b = a3.b;
                            if (b == 2 || (b == 1 && a3.l > a3.e)) {
                                final float x = pointF.x;
                                final PointF c = a3.c;
                                float n3 = x - c.x;
                                float n4 = pointF.y - c.y;
                                final float n5 = (float)Math.round(a3.p * a3.l);
                                final ZoomableImageView a4 = this.a;
                                final float n6 = (float)Math.round(a4.q * a4.l);
                                final ZoomableImageView a5 = this.a;
                                float n9 = 0.0f;
                                Label_0487: {
                                    if (n5 < a5.j) {
                                        final float n7 = n2 + n4;
                                        Label_0318: {
                                            if (n7 <= 0.0f) {
                                                final float n8 = a5.n;
                                                if (n7 >= -n8) {
                                                    break Label_0318;
                                                }
                                                n2 += n8;
                                            }
                                            n4 = -n2;
                                        }
                                        n9 = 0.0f;
                                    }
                                    else if (n6 < a5.k) {
                                        final float n10 = n + n3;
                                        Label_0382: {
                                            if (n10 <= 0.0f) {
                                                final float m = a5.m;
                                                n9 = n3;
                                                if (n10 >= -m) {
                                                    break Label_0382;
                                                }
                                                n += m;
                                            }
                                            n9 = -n;
                                        }
                                        n4 = 0.0f;
                                    }
                                    else {
                                        final float n11 = n + n3;
                                        Label_0434: {
                                            if (n11 <= 0.0f) {
                                                final float i = a5.m;
                                                if (n11 >= -i) {
                                                    break Label_0434;
                                                }
                                                n += i;
                                            }
                                            n3 = -n;
                                        }
                                        final float n12 = n2 + n4;
                                        if (n12 <= 0.0f) {
                                            final float n13 = a5.n;
                                            n9 = n3;
                                            if (n12 >= -n13) {
                                                break Label_0487;
                                            }
                                            n2 += n13;
                                        }
                                        n4 = -n2;
                                        n9 = n3;
                                    }
                                }
                                a5.a.postTranslate(n9, n4);
                                this.a.c.set(pointF.x, pointF.y);
                            }
                        }
                    }
                    else {
                        final ZoomableImageView a6 = this.a;
                        a6.b = 0;
                        final int n14 = (int)Math.abs(pointF.x - a6.d.x);
                        final int n15 = (int)Math.abs(pointF.y - this.a.d.y);
                        if (n14 < 3 && n15 < 3) {
                            ((View)this.a).performClick();
                        }
                    }
                }
                else {
                    this.a.c.set(motionEvent.getX(), motionEvent.getY());
                    final ZoomableImageView a7 = this.a;
                    a7.d.set(a7.c);
                    this.a.b = 1;
                }
                final ZoomableImageView a8 = this.a;
                a8.setImageMatrix(a8.a);
                ((View)this.a).invalidate();
                return true;
            }
        });
    }
    
    public void onMeasure(final int n, final int n2) {
        super.onMeasure(n, n2);
        this.j = (float)View$MeasureSpec.getSize(n);
        final float k = (float)View$MeasureSpec.getSize(n2);
        this.k = k;
        final float min = Math.min(this.j / this.t, k / this.v);
        this.a.setScale(min, min);
        this.setImageMatrix(this.a);
        this.l = 1.0f;
        final float i = this.k;
        final float v = this.v;
        final float j = this.j;
        final float t = this.t;
        final float l = (i - v * min) / 2.0f;
        this.i = l;
        final float h = (j - min * t) / 2.0f;
        this.h = h;
        this.a.postTranslate(h, l);
        final float m = this.j;
        final float h2 = this.h;
        this.p = m - h2 * 2.0f;
        final float k2 = this.k;
        final float i2 = this.i;
        this.q = k2 - i2 * 2.0f;
        final float l2 = this.l;
        this.m = m * l2 - m - h2 * 2.0f * l2;
        this.n = k2 * l2 - k2 - i2 * 2.0f * l2;
        this.setImageMatrix(this.a);
    }
    
    @Override
    public void setImageBitmap(final Bitmap imageBitmap) {
        super.setImageBitmap(imageBitmap);
        this.t = (float)imageBitmap.getWidth();
        this.v = (float)imageBitmap.getHeight();
    }
    
    public void setMaxZoom(final float f) {
        this.f = f;
    }
    
    public class b extends ScaleGestureDetector$SimpleOnScaleGestureListener
    {
        public final ZoomableImageView a;
        
        public b(final ZoomableImageView a) {
            this.a = a;
        }
        
        public boolean onScale(final ScaleGestureDetector scaleGestureDetector) {
            float scaleFactor = scaleGestureDetector.getScaleFactor();
            final ZoomableImageView a = this.a;
            final float l = a.l;
            final float i = l * scaleFactor;
            a.l = i;
            final float f = a.f;
            Label_0078: {
                float j;
                if (i > f) {
                    j = f;
                }
                else {
                    final float e = a.e;
                    if (i >= e) {
                        break Label_0078;
                    }
                    j = e;
                }
                a.l = j;
                scaleFactor = j / l;
            }
            final float k = a.j;
            final float m = a.l;
            a.m = k * m - k - a.h * 2.0f * m;
            final float k2 = a.k;
            a.n = k2 * m - k2 - a.i * 2.0f * m;
            Matrix matrix2;
            float n5;
            if (a.p * m > k && a.q * m > k2) {
                a.a.postScale(scaleFactor, scaleFactor, scaleGestureDetector.getFocusX(), scaleGestureDetector.getFocusY());
                final ZoomableImageView a2 = this.a;
                a2.a.getValues(a2.g);
                final ZoomableImageView a3 = this.a;
                final float[] g = a3.g;
                final float n = g[2];
                final float n2 = g[5];
                if (scaleFactor >= 1.0f) {
                    return true;
                }
                final float m2 = a3.m;
                Label_0289: {
                    Matrix matrix;
                    float n3;
                    if (n < -m2) {
                        matrix = a3.a;
                        n3 = n + m2;
                    }
                    else {
                        if (n <= 0.0f) {
                            break Label_0289;
                        }
                        matrix = a3.a;
                        n3 = n;
                    }
                    matrix.postTranslate(-n3, 0.0f);
                }
                final ZoomableImageView a4 = this.a;
                final float n4 = a4.n;
                if (n2 < -n4) {
                    matrix2 = a4.a;
                    n5 = n2 + n4;
                }
                else {
                    if (n2 <= 0.0f) {
                        return true;
                    }
                    matrix2 = a4.a;
                    n5 = n2;
                }
            }
            else {
                a.a.postScale(scaleFactor, scaleFactor, k / 2.0f, k2 / 2.0f);
                final float n6 = fcmpg(scaleFactor, 1.0f);
                if (n6 >= 0) {
                    return true;
                }
                final ZoomableImageView a5 = this.a;
                a5.a.getValues(a5.g);
                final ZoomableImageView a6 = this.a;
                final float[] g2 = a6.g;
                final float n7 = g2[2];
                n5 = g2[5];
                if (n6 >= 0) {
                    return true;
                }
                final float n8 = (float)Math.round(a6.p * a6.l);
                final ZoomableImageView a7 = this.a;
                if (n8 < a7.j) {
                    final float n9 = a7.n;
                    if (n5 < -n9) {
                        a7.a.postTranslate(0.0f, -(n5 + n9));
                        return true;
                    }
                    if (n5 <= 0.0f) {
                        return true;
                    }
                    matrix2 = a7.a;
                }
                else {
                    final float m3 = a7.m;
                    if (n7 < -m3) {
                        a7.a.postTranslate(-(n7 + m3), 0.0f);
                        return true;
                    }
                    if (n7 > 0.0f) {
                        a7.a.postTranslate(-n7, 0.0f);
                        return true;
                    }
                    return true;
                }
            }
            matrix2.postTranslate(0.0f, -n5);
            return true;
        }
        
        public boolean onScaleBegin(final ScaleGestureDetector scaleGestureDetector) {
            this.a.b = 2;
            return true;
        }
    }
}

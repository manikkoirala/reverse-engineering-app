// 
// Decompiled by Procyon v0.6.0
// 

package com.ekodroid.omrevaluator.templateui;

import android.widget.CompoundButton;
import androidx.appcompat.app.a;
import android.view.MenuItem;
import android.view.Menu;
import androidx.appcompat.widget.Toolbar;
import android.net.Uri;
import com.ekodroid.omrevaluator.templateui.scanner.ResultItem;
import java.io.FileNotFoundException;
import com.ekodroid.omrevaluator.templateui.scanner.AnswerValue;
import com.ekodroid.omrevaluator.database.ResultDataJsonModel;
import android.widget.RadioButton;
import android.view.ViewGroup;
import android.view.LayoutInflater;
import android.content.DialogInterface;
import android.content.DialogInterface$OnClickListener;
import com.google.android.material.dialog.MaterialAlertDialogBuilder;
import android.widget.SpinnerAdapter;
import android.widget.ArrayAdapter;
import android.widget.AdapterView;
import android.widget.AdapterView$OnItemSelectedListener;
import android.widget.Spinner;
import android.widget.LinearLayout;
import com.ekodroid.omrevaluator.templateui.models.LabelProfile;
import java.io.Reader;
import java.io.InputStreamReader;
import java.io.InputStream;
import com.ekodroid.omrevaluator.database.TemplateDataJsonModel;
import com.ekodroid.omrevaluator.database.repositories.TemplateRepository;
import android.view.View;
import android.view.View$OnClickListener;
import java.io.Serializable;
import android.content.Intent;
import com.ekodroid.omrevaluator.templateui.scansheet.ScanPaperActivity;
import com.ekodroid.omrevaluator.templateui.models.MatrixOptionPayload;
import com.ekodroid.omrevaluator.templateui.models.NumericalOptionPayload;
import java.util.Iterator;
import com.ekodroid.omrevaluator.templateui.models.AnswerOption;
import android.os.Bundle;
import com.google.firebase.analytics.FirebaseAnalytics;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.app.Activity;
import android.content.Context;
import com.ekodroid.omrevaluator.templateui.models.PartialAnswerOptionKey;
import java.util.ArrayList;
import com.ekodroid.omrevaluator.templateui.models.AnswerOptionKey;
import android.widget.Button;
import com.ekodroid.omrevaluator.templateui.models.TemplateParams2;
import com.ekodroid.omrevaluator.templateui.models.ExamId;
import com.ekodroid.omrevaluator.templateui.models.AnswerSetKey;
import com.ekodroid.omrevaluator.templateui.models.SheetTemplate2;

public class SetAnswerKeyActivity extends v5
{
    public final int c;
    public final int d;
    public SetAnswerKeyActivity e;
    public SheetTemplate2 f;
    public AnswerSetKey[] g;
    public ExamId h;
    public boolean i;
    public TemplateParams2 j;
    public int k;
    public int l;
    public Button m;
    public Button n;
    public k5.f p;
    
    public SetAnswerKeyActivity() {
        this.c = 2;
        this.d = 202;
        this.e = this;
        this.i = false;
        this.k = 1;
        this.l = 1;
        this.p = new k5.f(this) {
            public final SetAnswerKeyActivity a;
            
            @Override
            public void a(final c21 c21) {
                final SetAnswerKeyActivity a = this.a;
                a.i = true;
                ((View)a.n).setEnabled(true);
                if (c21.b) {
                    final SetAnswerKeyActivity a2 = this.a;
                    a2.g[SetAnswerKeyActivity.A(a2) - 1].getAnswerOptionKeys().get(c21.a - 1).setPartialAnswerOptionKeys(c21.e);
                }
                else {
                    final SetAnswerKeyActivity a3 = this.a;
                    a3.g[SetAnswerKeyActivity.A(a3) - 1].getAnswerOptionKeys().get(c21.a - 1).setMarkedValues(c21.c);
                    final SetAnswerKeyActivity a4 = this.a;
                    a4.g[SetAnswerKeyActivity.A(a4) - 1].getAnswerOptionKeys().get(c21.a - 1).setMarkedPayload(c21.d);
                }
            }
        };
    }
    
    public static /* synthetic */ int A(final SetAnswerKeyActivity setAnswerKeyActivity) {
        return setAnswerKeyActivity.l;
    }
    
    public static /* synthetic */ int B(final SetAnswerKeyActivity setAnswerKeyActivity, final int l) {
        return setAnswerKeyActivity.l = l;
    }
    
    public final boolean J() {
        if (sl.checkSelfPermission((Context)this, "android.permission.CAMERA") == 0) {
            return true;
        }
        h2.g(this, new String[] { "android.permission.CAMERA" }, 2);
        return false;
    }
    
    public final void K(final ArrayList list, final ArrayList list2) {
        ((ListView)this.findViewById(2131296840)).setAdapter((ListAdapter)new k3((Context)this.e, list, list2, this.p, this.f.getLabelProfile()));
    }
    
    public final void L() {
        if (this.f.getAnswerKeys() != null && this.f.getAnswerKeys().length > 0) {
            new tu((Context)this.e, this.h, this.f).execute((Object[])new Void[0]);
            FirebaseAnalytics.getInstance((Context)this.e).a("DownloadKeyCsv", null);
        }
        else {
            a91.G((Context)this.e, 2131886136, 2131230909, 2131231086);
        }
    }
    
    public final void M() {
        if (this.f.getAnswerKeys() != null && this.f.getAnswerKeys().length > 0) {
            new uu((Context)this.e, this.h, this.f).execute((Object[])new Void[0]);
            FirebaseAnalytics.getInstance((Context)this.e).a("DownloadKey", null);
        }
        else {
            a91.G((Context)this.e, 2131886136, 2131230909, 2131231086);
        }
    }
    
    public final ArrayList N(final ArrayList list) {
        final ArrayList list2 = new ArrayList();
        for (final AnswerOption answerOption : list) {
            final int n = SetAnswerKeyActivity$i.a[answerOption.type.ordinal()];
            final int n2 = 5;
            int primaryOptions = 4;
            final int n3 = 10;
            AnswerOptionKey e = null;
            switch (n) {
                default: {
                    continue;
                }
                case 10: {
                    e = new AnswerOptionKey(AnswerOption.AnswerOptionType.TENOPTION, answerOption.questionNumber, new boolean[10], null, answerOption.subjectId, answerOption.sectionId, null, answerOption.payload);
                    break;
                }
                case 9: {
                    e = new AnswerOptionKey(AnswerOption.AnswerOptionType.EIGHTOPTION, answerOption.questionNumber, new boolean[8], null, answerOption.subjectId, answerOption.sectionId, null, answerOption.payload);
                    break;
                }
                case 8: {
                    e = new AnswerOptionKey(AnswerOption.AnswerOptionType.SIXOPTION, answerOption.questionNumber, new boolean[6], null, answerOption.subjectId, answerOption.sectionId, null, answerOption.payload);
                    break;
                }
                case 7: {
                    e = new AnswerOptionKey(AnswerOption.AnswerOptionType.DECIMAL, answerOption.questionNumber, null, null, answerOption.subjectId, answerOption.sectionId, null, answerOption.payload);
                    break;
                }
                case 6: {
                    final NumericalOptionPayload d = bc.d(answerOption.payload);
                    int n4 = n3;
                    Label_0315: {
                        if (d != null) {
                            final boolean hasNegetive = d.hasNegetive;
                            if (!hasNegetive) {
                                n4 = n3;
                                if (d.digits <= 1) {
                                    break Label_0315;
                                }
                            }
                            n4 = 10 * d.digits;
                            if (hasNegetive) {
                                ++n4;
                            }
                        }
                    }
                    e = new AnswerOptionKey(AnswerOption.AnswerOptionType.NUMARICAL, answerOption.questionNumber, new boolean[n4], null, answerOption.subjectId, answerOption.sectionId, null, answerOption.payload);
                    break;
                }
                case 5: {
                    final MatrixOptionPayload c = bc.c(answerOption.payload);
                    int secondaryOptions = n2;
                    if (c != null) {
                        primaryOptions = c.primaryOptions;
                        secondaryOptions = c.secondaryOptions;
                    }
                    e = new AnswerOptionKey(AnswerOption.AnswerOptionType.MATRIX, answerOption.questionNumber, new boolean[secondaryOptions * primaryOptions], null, answerOption.subjectId, answerOption.sectionId, null, answerOption.payload);
                    break;
                }
                case 4: {
                    e = new AnswerOptionKey(AnswerOption.AnswerOptionType.TRUEORFALSE, answerOption.questionNumber, new boolean[2], null, answerOption.subjectId, answerOption.sectionId, null, answerOption.payload);
                    break;
                }
                case 3: {
                    e = new AnswerOptionKey(AnswerOption.AnswerOptionType.THREEOPTION, answerOption.questionNumber, new boolean[3], null, answerOption.subjectId, answerOption.sectionId, null, answerOption.payload);
                    break;
                }
                case 2: {
                    e = new AnswerOptionKey(AnswerOption.AnswerOptionType.FIVEOPTION, answerOption.questionNumber, new boolean[5], null, answerOption.subjectId, answerOption.sectionId, null, answerOption.payload);
                    break;
                }
                case 1: {
                    e = new AnswerOptionKey(AnswerOption.AnswerOptionType.FOUROPTION, answerOption.questionNumber, new boolean[4], null, answerOption.subjectId, answerOption.sectionId, null, answerOption.payload);
                    break;
                }
            }
            list2.add(e);
        }
        return list2;
    }
    
    public final AnswerSetKey[] O(final SheetTemplate2 sheetTemplate2) {
        final AnswerSetKey[] answerKeys = sheetTemplate2.getAnswerKeys();
        int i = 0;
        if (answerKeys != null && answerKeys[0].getAnswerOptionKeys() != null && answerKeys[0].getAnswerOptionKeys().size() > 0) {
            return answerKeys;
        }
        final int examSets = sheetTemplate2.getTemplateParams().getExamSets();
        final AnswerSetKey[] array = new AnswerSetKey[examSets];
        while (i < examSets) {
            final ArrayList n = this.N(sheetTemplate2.getAnswerOptions());
            final int n2 = i + 1;
            array[i] = new AnswerSetKey(n, n2);
            i = n2;
        }
        return array;
    }
    
    public final int P(final String s) {
        if (s == null) {
            return -1;
        }
        try {
            return Integer.parseInt(s);
        }
        catch (final Exception ex) {
            return -1;
        }
    }
    
    public final void Q() {
        final Intent intent = new Intent((Context)this.e, (Class)ScanPaperActivity.class);
        intent.putExtra("EXAM_ID", (Serializable)this.h);
        intent.putExtra("START_SCAN", true);
        intent.putExtra("SCAN_KEY", true);
        this.startActivityForResult(intent, 202);
    }
    
    public final void R() {
        final Intent intent = new Intent("android.intent.action.GET_CONTENT");
        intent.setType("*/*");
        intent.addCategory("android.intent.category.OPENABLE");
        intent.putExtra("android.intent.extra.LOCAL_ONLY", true);
        this.startActivityForResult(Intent.createChooser(intent, (CharSequence)"Choose a file"), 4);
    }
    
    public final void S() {
        ((View)this.m).setOnClickListener((View$OnClickListener)new View$OnClickListener(this) {
            public final SetAnswerKeyActivity a;
            
            public void onClick(final View view) {
                xs.c((Context)this.a.e, new y01(this) {
                    public final SetAnswerKeyActivity$d a;
                    
                    @Override
                    public void a(final Object o) {
                        final SetAnswerKeyActivity a = this.a.a;
                        a.i = false;
                        a.X(null);
                        this.a.a.U();
                        ((View)this.a.a.n).setEnabled(false);
                    }
                }, 2131886129, 2131886461, 2131886903, 2131886657, 0, 0);
            }
        });
    }
    
    public final void T() {
        ((View)this.n).setEnabled(false);
        ((View)this.n).setOnClickListener((View$OnClickListener)new View$OnClickListener(this) {
            public final SetAnswerKeyActivity a;
            
            public void onClick(final View view) {
                final SetAnswerKeyActivity a = this.a;
                if (!a.d0(a.g)) {
                    return;
                }
                final SetAnswerKeyActivity a2 = this.a;
                a2.X(a2.g);
                a91.G((Context)this.a.e, 2131886371, 2131230927, 2131231085);
                this.a.e.finish();
            }
        });
    }
    
    public final void U() {
        if (this.h != null) {
            final TemplateDataJsonModel templateJson = TemplateRepository.getInstance((Context)this.e).getTemplateJson(this.h);
            if (templateJson != null) {
                final SheetTemplate2 sheetTemplate = templateJson.getSheetTemplate();
                this.f = sheetTemplate;
                this.g = this.O(sheetTemplate);
                final TemplateParams2 templateParams = this.f.getTemplateParams();
                this.j = templateParams;
                this.k = templateParams.getExamSets();
                this.W();
            }
            else {
                this.finish();
            }
        }
    }
    
    public final void V(final InputStream in) {
        try {
            final yd yd = new yd((Reader)new InputStreamReader(in));
            final ArrayList<AnswerOption> answerOptions = this.f.getAnswerOptions();
            final LabelProfile labelProfile = this.f.getLabelProfile();
            while (true) {
                final String[] c = yd.c();
                if (c == null) {
                    break;
                }
                final int p = this.P(c[0]);
                if (p <= 0 || p >= answerOptions.size()) {
                    continue;
                }
                final AnswerOption answerOption = answerOptions.get(p);
                for (int i = 1; i < c.length; ++i) {
                    final String s = c[i];
                    if (s != null && s.length() > 0) {
                        final AnswerSetKey[] g = this.g;
                        if (i <= g.length) {
                            final AnswerOption.AnswerOptionType type = answerOption.type;
                            if (type == AnswerOption.AnswerOptionType.DECIMAL) {
                                g[i - 1].getAnswerOptionKeys().get(p - 1).setMarkedPayload(e5.d(c[i]));
                            }
                            else {
                                this.g[i - 1].getAnswerOptionKeys().get(p - 1).setMarkedValues(e5.c(c[i], type, answerOption.payload, labelProfile));
                            }
                        }
                    }
                }
            }
            FirebaseAnalytics.getInstance((Context)this.e).a("ImportCsvSuccess", null);
        }
        catch (final Exception ex) {
            FirebaseAnalytics.getInstance((Context)this.e).a("ImportCsvError", null);
        }
    }
    
    public final void W() {
        this.Y(this.k, this.f.getLabelProfile().getExamSetLabels());
        this.K(this.g[this.l - 1].getAnswerOptionKeys(), this.f.getAnswerOptions());
        if (this.i) {
            ((View)this.n).setEnabled(true);
        }
    }
    
    public final void X(final AnswerSetKey[] answerKeys) {
        this.f.setAnswerKeys(answerKeys);
        final TemplateDataJsonModel templateJson = TemplateRepository.getInstance((Context)this.e).getTemplateJson(this.h);
        if (templateJson != null) {
            templateJson.setSheetTemplate(this.f);
            TemplateRepository.getInstance((Context)this.e).saveOrUpdateTemplateJson(templateJson);
        }
    }
    
    public final void Y(final int n, final String[] array) {
        final LinearLayout linearLayout = (LinearLayout)this.findViewById(2131296753);
        final Spinner spinner = (Spinner)this.findViewById(2131297095);
        ((AdapterView)spinner).setOnItemSelectedListener((AdapterView$OnItemSelectedListener)new AdapterView$OnItemSelectedListener(this) {
            public final SetAnswerKeyActivity a;
            
            public void onItemSelected(final AdapterView adapterView, final View view, final int n, final long n2) {
                SetAnswerKeyActivity.B(this.a, n + 1);
                final SetAnswerKeyActivity a = this.a;
                a.K(a.g[SetAnswerKeyActivity.A(a) - 1].getAnswerOptionKeys(), this.a.f.getAnswerOptions());
            }
            
            public void onNothingSelected(final AdapterView adapterView) {
            }
        });
        if (n == 1) {
            ((View)linearLayout).setVisibility(8);
        }
        else {
            final String[] array2 = new String[n];
            for (int i = 0; i < n; ++i) {
                array2[i] = array[i];
            }
            spinner.setAdapter((SpinnerAdapter)new ArrayAdapter((Context)this.e, 2131493104, (Object[])array2));
        }
    }
    
    public final void Z() {
        final MaterialAlertDialogBuilder materialAlertDialogBuilder = new MaterialAlertDialogBuilder((Context)this.e, 2131951953);
        materialAlertDialogBuilder.setTitle(2131886129).setMessage(2131886547).setPositiveButton(2131886903, (DialogInterface$OnClickListener)new DialogInterface$OnClickListener(this) {
            public final SetAnswerKeyActivity a;
            
            public void onClick(final DialogInterface dialogInterface, final int n) {
                final SetAnswerKeyActivity a = this.a;
                if (!a.d0(a.g)) {
                    return;
                }
                final SetAnswerKeyActivity a2 = this.a;
                a2.X(a2.g);
                this.a.e.finish();
                dialogInterface.dismiss();
            }
        }).setNegativeButton(2131886657, (DialogInterface$OnClickListener)new DialogInterface$OnClickListener(this) {
            public final SetAnswerKeyActivity a;
            
            public void onClick(final DialogInterface dialogInterface, final int n) {
                dialogInterface.dismiss();
                this.a.e.finish();
            }
        }).create();
        ((a.a)materialAlertDialogBuilder).show();
    }
    
    public final void a0() {
        final MaterialAlertDialogBuilder materialAlertDialogBuilder = new MaterialAlertDialogBuilder((Context)this.e, 2131951953);
        final View inflate = ((LayoutInflater)((Context)this).getSystemService("layout_inflater")).inflate(2131492967, (ViewGroup)null);
        materialAlertDialogBuilder.setView(inflate);
        materialAlertDialogBuilder.setTitle(2131886783);
        materialAlertDialogBuilder.setPositiveButton(2131886655, (DialogInterface$OnClickListener)new DialogInterface$OnClickListener(this, (RadioButton)inflate.findViewById(2131296991)) {
            public final RadioButton a;
            public final SetAnswerKeyActivity b;
            
            public void onClick(final DialogInterface dialogInterface, final int n) {
                final SetAnswerKeyActivity b = this.b;
                b.f.setAnswerKeys(b.g);
                if (((CompoundButton)this.a).isChecked()) {
                    this.b.M();
                }
                else {
                    this.b.L();
                }
                dialogInterface.dismiss();
            }
        });
        materialAlertDialogBuilder.create().show();
    }
    
    public final void b0() {
        xs.c((Context)this.e, new y01(this) {
            public final SetAnswerKeyActivity a;
            
            @Override
            public void a(final Object o) {
                this.a.R();
            }
        }, 2131886511, 2131886512, 2131886356, 0, 0, 0);
    }
    
    public final boolean c0(final AnswerSetKey answerSetKey, final int n) {
        for (final AnswerOptionKey answerOptionKey : answerSetKey.getAnswerOptionKeys()) {
            if (answerOptionKey.getType() == AnswerOption.AnswerOptionType.DECIMAL) {
                if (answerOptionKey.getMarkedPayload() == null) {
                    final SetAnswerKeyActivity e = this.e;
                    String s;
                    if (n < 0) {
                        final StringBuilder sb = new StringBuilder();
                        sb.append(this.getResources().getString(2131886810));
                        sb.append(" ");
                        sb.append(answerOptionKey.getQuestionNumber());
                        s = sb.toString();
                    }
                    else {
                        final StringBuilder sb2 = new StringBuilder();
                        sb2.append(this.getResources().getString(2131886810));
                        sb2.append(" ");
                        sb2.append(answerOptionKey.getQuestionNumber());
                        sb2.append(" ");
                        sb2.append(this.getResources().getString(2131886320));
                        sb2.append(" ");
                        sb2.append(this.f.getLabelProfile().getExamSetLabels()[n - 1]);
                        s = sb2.toString();
                    }
                    a91.H((Context)e, s, 2131230909, 2131231086);
                    return false;
                }
                continue;
            }
            else {
                final boolean[] markedValues = answerOptionKey.getMarkedValues();
                int i = 0;
                int n2 = 0;
                while (i < markedValues.length) {
                    if (n2 == 0 && !markedValues[i]) {
                        n2 = 0;
                    }
                    else {
                        n2 = 1;
                    }
                    ++i;
                }
                if (n2 == 0) {
                    final SetAnswerKeyActivity e2 = this.e;
                    String s2;
                    if (n < 0) {
                        final StringBuilder sb3 = new StringBuilder();
                        sb3.append(this.getResources().getString(2131886810));
                        sb3.append(" ");
                        sb3.append(answerOptionKey.getQuestionNumber());
                        s2 = sb3.toString();
                    }
                    else {
                        final StringBuilder sb4 = new StringBuilder();
                        sb4.append(this.getResources().getString(2131886810));
                        sb4.append(" ");
                        sb4.append(answerOptionKey.getQuestionNumber());
                        sb4.append(" ");
                        sb4.append(this.getResources().getString(2131886320));
                        sb4.append(" ");
                        sb4.append(this.f.getLabelProfile().getExamSetLabels()[n - 1]);
                        s2 = sb4.toString();
                    }
                    a91.H((Context)e2, s2, 2131230909, 2131231086);
                    return false;
                }
                continue;
            }
        }
        return true;
    }
    
    public final boolean d0(final AnswerSetKey[] array) {
        if (array.length == 1) {
            return this.c0(array[0], -1);
        }
        final int l = this.l;
        if (!this.c0(array[l - 1], l)) {
            return false;
        }
        int i = 0;
        while (i < array.length) {
            if (!this.c0(array[i], ++i)) {
                return false;
            }
        }
        return true;
    }
    
    @Override
    public void onActivityResult(final int n, final int n2, final Intent intent) {
        super.onActivityResult(n, n2, intent);
        this.i = true;
        if (n == 202 && n2 == -1 && intent != null) {
            final ResultItem resultItem = ((ResultDataJsonModel)intent.getExtras().getSerializable("RESULT_DATA")).getResultItem(this.f);
            if ((this.l = resultItem.getExamSet()) < 1) {
                this.l = 1;
            }
            for (final AnswerValue answerValue : resultItem.getAnswerValue2s()) {
                this.g[this.l - 1].getAnswerOptionKeys().get(answerValue.getQuestionNumber() - 1).setMarkedValues(answerValue.getMarkedValues());
            }
            this.W();
        }
        if (n == 4 && n2 == -1) {
            final Uri data = intent.getData();
            try {
                this.V(((Context)this.e).getContentResolver().openInputStream(data));
            }
            catch (final FileNotFoundException ex) {
                ex.printStackTrace();
            }
            this.i = true;
            this.W();
        }
    }
    
    @Override
    public void onBackPressed() {
        if (this.i) {
            this.Z();
        }
        else {
            super.onBackPressed();
        }
    }
    
    @Override
    public void onCreate(final Bundle bundle) {
        super.onCreate(bundle);
        this.setContentView(2131492921);
        this.x((Toolbar)this.findViewById(2131297334));
        this.h = (ExamId)this.getIntent().getSerializableExtra("EXAM_ID");
        this.n = (Button)this.findViewById(2131296460);
        this.m = (Button)this.findViewById(2131296436);
        this.S();
        this.T();
        this.U();
    }
    
    public boolean onCreateOptionsMenu(final Menu menu) {
        this.getMenuInflater().inflate(2131623944, menu);
        return true;
    }
    
    public boolean onOptionsItemSelected(final MenuItem menuItem) {
        final int itemId = menuItem.getItemId();
        if (itemId == 2131296322) {
            this.a0();
            return true;
        }
        if (itemId == 2131296328) {
            this.b0();
            return true;
        }
        if (itemId != 2131296335) {
            return super.onOptionsItemSelected(menuItem);
        }
        if (this.J()) {
            this.Q();
        }
        return true;
    }
    
    @Override
    public void onRequestPermissionsResult(final int n, final String[] array, final int[] array2) {
        super.onRequestPermissionsResult(n, array, array2);
        if (array2 != null && array2.length > 0 && array2[0] == 0 && 2 == n) {
            this.Q();
        }
    }
    
    @Override
    public void onResume() {
        super.onResume();
        this.W();
    }
    
    @Override
    public boolean v() {
        if (this.i) {
            this.Z();
        }
        else {
            this.finish();
        }
        return true;
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.ekodroid.omrevaluator.templateui;

import androidx.appcompat.widget.Toolbar;
import android.os.Bundle;
import com.ekodroid.omrevaluator.templateui.models.LabelProfile;
import com.ekodroid.omrevaluator.templateui.models.HeaderProfile;
import com.ekodroid.omrevaluator.database.TemplateDataJsonModel;
import com.ekodroid.omrevaluator.database.repositories.TemplateRepository;
import android.view.View;
import android.view.View$OnClickListener;
import com.ekodroid.omrevaluator.templateui.models.InvalidQuestionSet;
import com.ekodroid.omrevaluator.templateui.models.GradeLevel;
import java.io.Serializable;
import android.content.Context;
import android.content.Intent;
import com.ekodroid.omrevaluator.templateui.models.ExamId;
import com.ekodroid.omrevaluator.templateui.models.SheetTemplate2;
import android.widget.TextView;
import android.widget.LinearLayout;

public class ExamSettingsActivity extends v5
{
    public LinearLayout c;
    public LinearLayout d;
    public LinearLayout e;
    public LinearLayout f;
    public LinearLayout g;
    public LinearLayout h;
    public TextView i;
    public TextView j;
    public TextView k;
    public TextView l;
    public TextView m;
    public TextView n;
    public SheetTemplate2 p;
    public ExamSettingsActivity q;
    public ExamId t;
    
    public ExamSettingsActivity() {
        this.q = this;
    }
    
    public static /* synthetic */ ExamId D(final ExamSettingsActivity examSettingsActivity) {
        return examSettingsActivity.t;
    }
    
    public static /* synthetic */ ExamSettingsActivity E(final ExamSettingsActivity examSettingsActivity) {
        return examSettingsActivity.q;
    }
    
    public final void G() {
        final Intent intent = new Intent((Context)this.q, (Class)EditTemplateActivity.class);
        intent.putExtra("EXAM_ID", (Serializable)this.t);
        ((Context)this).startActivity(intent);
    }
    
    public final String H(final GradeLevel[] array) {
        if (array == null) {
            return "";
        }
        final StringBuilder sb = new StringBuilder();
        sb.append(" ");
        for (int i = 0; i < array.length; ++i) {
            GradeLevel gradeLevel;
            if (i == 0) {
                gradeLevel = array[0];
            }
            else {
                sb.append(", ");
                gradeLevel = array[i];
            }
            sb.append(gradeLevel.getStringLabel());
        }
        return sb.toString();
    }
    
    public final String I(final SheetTemplate2 sheetTemplate2) {
        final InvalidQuestionSet[] invalidQuestionSets = sheetTemplate2.getInvalidQuestionSets();
        final String[] examSetLabels = sheetTemplate2.getLabelProfile().getExamSetLabels();
        if (invalidQuestionSets == null) {
            return "";
        }
        final StringBuilder sb = new StringBuilder();
        sb.append(" ");
        for (int i = 0; i < invalidQuestionSets.length; ++i) {
            if (invalidQuestionSets[i].getExamSet() > 0) {
                final StringBuilder sb2 = new StringBuilder();
                sb2.append(examSetLabels[i]);
                sb2.append(": ");
                sb.append(sb2.toString());
            }
            final int[] invalidQuestions = invalidQuestionSets[i].getInvalidQuestions();
            if (invalidQuestions != null) {
                for (int j = 0; j < invalidQuestions.length; ++j) {
                    int k;
                    if (j == 0) {
                        k = invalidQuestions[0];
                    }
                    else {
                        sb.append(", ");
                        k = invalidQuestions[j];
                    }
                    sb.append(k);
                }
            }
            sb.append("   ");
        }
        return sb.toString();
    }
    
    public final void J() {
        ((View)this.c).setOnClickListener((View$OnClickListener)new View$OnClickListener(this) {
            public final ExamSettingsActivity a;
            
            public void onClick(final View view) {
                this.a.G();
            }
        });
    }
    
    public final void K() {
        ((View)this.e).setOnClickListener((View$OnClickListener)new View$OnClickListener(this, new y01(this) {
            public final ExamSettingsActivity a;
            
            @Override
            public void a(final Object o) {
                GradeLevel[] gradeLevels;
                if (o != null) {
                    gradeLevels = (GradeLevel[])o;
                }
                else {
                    gradeLevels = null;
                }
                final TemplateDataJsonModel templateJson = TemplateRepository.getInstance((Context)ExamSettingsActivity.E(this.a)).getTemplateJson(ExamSettingsActivity.D(this.a));
                final SheetTemplate2 sheetTemplate = templateJson.getSheetTemplate();
                sheetTemplate.setGradeLevels(gradeLevels);
                templateJson.setSheetTemplate(sheetTemplate);
                TemplateRepository.getInstance((Context)ExamSettingsActivity.E(this.a)).saveOrUpdateTemplateJson(templateJson);
                this.a.Q();
            }
        }) {
            public final y01 a;
            public final ExamSettingsActivity b;
            
            public void onClick(final View view) {
                new nb0((Context)ExamSettingsActivity.E(this.b), this.a, this.b.p.getGradeLevels()).show();
            }
        });
    }
    
    public final void L() {
        ((View)this.g).setOnClickListener((View$OnClickListener)new View$OnClickListener(this) {
            public final ExamSettingsActivity a;
            
            public void onClick(final View view) {
                this.a.R();
            }
        });
    }
    
    public final void M() {
        ((View)this.f).setOnClickListener((View$OnClickListener)new View$OnClickListener(this, new ig0.f(this) {
            public final ExamSettingsActivity a;
            
            @Override
            public void a(final InvalidQuestionSet[] invalidQuestionSets) {
                final TemplateDataJsonModel templateJson = TemplateRepository.getInstance((Context)ExamSettingsActivity.E(this.a)).getTemplateJson(ExamSettingsActivity.D(this.a));
                final SheetTemplate2 sheetTemplate = templateJson.getSheetTemplate();
                sheetTemplate.setInvalidQuestionSets(invalidQuestionSets);
                templateJson.setSheetTemplate(sheetTemplate);
                TemplateRepository.getInstance((Context)ExamSettingsActivity.E(this.a)).saveOrUpdateTemplateJson(templateJson);
                this.a.Q();
            }
        }) {
            public final ig0.f a;
            public final ExamSettingsActivity b;
            
            public void onClick(final View view) {
                new ig0((Context)ExamSettingsActivity.E(this.b), this.a, this.b.p).show();
            }
        });
    }
    
    public final void N() {
        ((View)this.d).setOnClickListener((View$OnClickListener)new View$OnClickListener(this) {
            public final ExamSettingsActivity a;
            
            public void onClick(final View view) {
                this.a.S();
            }
        });
    }
    
    public final void O() {
        ((View)this.h).setOnClickListener((View$OnClickListener)new View$OnClickListener(this, new y01(this) {
            public final ExamSettingsActivity a;
            
            @Override
            public void a(final Object o) {
                String rankingMethod;
                if (o != null) {
                    rankingMethod = (String)o;
                }
                else {
                    rankingMethod = null;
                }
                final TemplateDataJsonModel templateJson = TemplateRepository.getInstance((Context)ExamSettingsActivity.E(this.a)).getTemplateJson(ExamSettingsActivity.D(this.a));
                final SheetTemplate2 sheetTemplate = templateJson.getSheetTemplate();
                sheetTemplate.setRankingMethod(rankingMethod);
                templateJson.setSheetTemplate(sheetTemplate);
                TemplateRepository.getInstance((Context)ExamSettingsActivity.E(this.a)).saveOrUpdateTemplateJson(templateJson);
                this.a.Q();
            }
        }) {
            public final y01 a;
            public final ExamSettingsActivity b;
            
            public void onClick(final View view) {
                new jc1((Context)ExamSettingsActivity.E(this.b), this.a, this.b.p.getRankingMethod());
            }
        });
    }
    
    public final void P() {
        this.c = (LinearLayout)this.findViewById(2131296745);
        this.g = (LinearLayout)this.findViewById(2131296759);
        this.d = (LinearLayout)this.findViewById(2131296769);
        this.e = (LinearLayout)this.findViewById(2131296757);
        this.h = (LinearLayout)this.findViewById(2131296788);
        this.f = (LinearLayout)this.findViewById(2131296767);
        this.i = (TextView)this.findViewById(2131297224);
        this.m = (TextView)this.findViewById(2131297234);
        this.j = (TextView)this.findViewById(2131297241);
        this.k = (TextView)this.findViewById(2131297232);
        this.l = (TextView)this.findViewById(2131297239);
        this.n = (TextView)this.findViewById(2131297268);
    }
    
    public final void Q() {
        this.p = TemplateRepository.getInstance((Context)this.q).getTemplateJson(this.t).getSheetTemplate();
        this.J();
        this.N();
        this.L();
        this.K();
        this.M();
        this.O();
        if (this.p.getHeaderProfile() != null) {
            this.m.setText((CharSequence)this.p.getHeaderProfile().getHeaderProfileName());
        }
        this.j.setText((CharSequence)this.p.getLabelProfile().getLabelProfileName());
        this.l.setText((CharSequence)this.I(this.p));
        this.k.setText((CharSequence)this.H(this.p.getGradeLevels()));
        this.n.setText((CharSequence)this.p.getRankingMethod());
    }
    
    public final void R() {
        new tc0((Context)this.q, new y01(this) {
            public final ExamSettingsActivity a;
            
            @Override
            public void a(final Object o) {
                HeaderProfile headerProfile;
                if (o != null) {
                    headerProfile = (HeaderProfile)o;
                }
                else {
                    headerProfile = null;
                }
                final TemplateDataJsonModel templateJson = TemplateRepository.getInstance((Context)ExamSettingsActivity.E(this.a)).getTemplateJson(ExamSettingsActivity.D(this.a));
                final SheetTemplate2 sheetTemplate = templateJson.getSheetTemplate();
                sheetTemplate.setHeaderProfile(headerProfile);
                templateJson.setSheetTemplate(sheetTemplate);
                TemplateRepository.getInstance((Context)ExamSettingsActivity.E(this.a)).saveOrUpdateTemplateJson(templateJson);
                this.a.Q();
            }
        });
    }
    
    public final void S() {
        new si0((Context)this.q, new y01(this) {
            public final ExamSettingsActivity a;
            
            @Override
            public void a(final Object o) {
                LabelProfile labelProfile;
                if (o != null) {
                    labelProfile = (LabelProfile)o;
                }
                else {
                    labelProfile = null;
                }
                final TemplateDataJsonModel templateJson = TemplateRepository.getInstance((Context)ExamSettingsActivity.E(this.a)).getTemplateJson(ExamSettingsActivity.D(this.a));
                final SheetTemplate2 sheetTemplate = templateJson.getSheetTemplate();
                sheetTemplate.setLabelProfile(labelProfile);
                templateJson.setSheetTemplate(sheetTemplate);
                TemplateRepository.getInstance((Context)ExamSettingsActivity.E(this.a)).saveOrUpdateTemplateJson(templateJson);
                this.a.Q();
            }
        });
    }
    
    @Override
    public void onCreate(final Bundle bundle) {
        super.onCreate(bundle);
        this.setContentView(2131492906);
        this.x((Toolbar)this.findViewById(2131297334));
        this.t = (ExamId)this.getIntent().getSerializableExtra("EXAM_ID");
        this.P();
        this.Q();
    }
    
    @Override
    public void onResume() {
        super.onResume();
    }
    
    @Override
    public boolean v() {
        this.finish();
        return true;
    }
}

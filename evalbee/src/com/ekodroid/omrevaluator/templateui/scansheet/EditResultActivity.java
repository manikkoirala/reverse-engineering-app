// 
// Decompiled by Procyon v0.6.0
// 

package com.ekodroid.omrevaluator.templateui.scansheet;

import android.widget.TextView;
import android.widget.AdapterView;
import android.os.BaseBundle;
import java.io.Serializable;
import android.content.Intent;
import android.view.View;
import android.view.View$OnClickListener;
import android.widget.Button;
import android.text.InputFilter$LengthFilter;
import android.text.InputFilter;
import android.widget.EditText;
import android.widget.SpinnerAdapter;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.LinearLayout;
import android.graphics.BitmapFactory;
import com.ekodroid.omrevaluator.database.repositories.TemplateRepository;
import android.os.Bundle;
import android.content.Context;
import android.widget.TableLayout;
import com.ekodroid.omrevaluator.templateui.scanner.AnswerValue;
import java.util.ArrayList;
import com.ekodroid.omrevaluator.templateui.models.SheetTemplate2;
import com.ekodroid.omrevaluator.templateui.models.ExamId;
import com.ekodroid.omrevaluator.templateui.scanner.ResultItem;

public class EditResultActivity extends v5
{
    public static int m = 5;
    public EditResultActivity c;
    public int d;
    public int e;
    public int f;
    public ResultItem g;
    public ExamId h;
    public SheetTemplate2 i;
    public ArrayList j;
    public ArrayList k;
    public k5.f l;
    
    public EditResultActivity() {
        this.c = this;
        this.l = new k5.f(this) {
            public final EditResultActivity a;
            
            @Override
            public void a(final c21 c21) {
                if (!c21.b) {
                    this.a.g.getAnswerValue2s().get(c21.a - 1).setMarkedValues(c21.c);
                }
            }
        };
    }
    
    public static /* synthetic */ int A() {
        return EditResultActivity.m;
    }
    
    public final void C() {
        jx0.a = null;
        jx0.b = null;
        this.finish();
    }
    
    public final void D() {
        new aw((Context)this.c, (TableLayout)this.findViewById(2131297163), this.g.getAnswerValue2s(), this.l, this.i, this.j, this.k);
    }
    
    @Override
    public void onCreate(Bundle extras) {
        super.onCreate(extras);
        this.setContentView(2131492903);
        extras = this.getIntent().getExtras();
        if (extras != null) {
            this.d = ((BaseBundle)extras).getInt("ROLL_NUMBER");
            this.g = (ResultItem)extras.getSerializable("RESULT_ITEM");
            this.h = (ExamId)this.getIntent().getSerializableExtra("EXAM_ID");
            final SheetTemplate2 sheetTemplate = TemplateRepository.getInstance((Context)this.c).getTemplateJson(this.h).getSheetTemplate();
            this.i = sheetTemplate;
            this.e = sheetTemplate.getTemplateParams().getExamSets();
            this.f = this.i.getTemplateParams().getRollDigits();
            if (jx0.b != null) {
                final ArrayList a = jx0.a;
                if (a != null && a.size() == jx0.b.size() && jx0.b.size() == this.i.getPageLayouts().length) {
                    this.j = jx0.a;
                    this.k = new ArrayList();
                    for (int i = 0; i < jx0.b.size(); ++i) {
                        this.k.add(BitmapFactory.decodeByteArray((byte[])jx0.b.get(i), 0, ((byte[])jx0.b.get(i)).length));
                    }
                }
            }
        }
        final LinearLayout linearLayout = (LinearLayout)this.findViewById(2131296751);
        final Spinner spinner = (Spinner)this.findViewById(2131297078);
        if (this.e == 1) {
            ((View)linearLayout).setVisibility(8);
        }
        else {
            final String[] examSetLabels = this.i.getLabelProfile().getExamSetLabels();
            final int e = this.e;
            final String[] array = new String[e];
            for (int j = 0; j < e; ++j) {
                array[j] = examSetLabels[j];
            }
            spinner.setAdapter((SpinnerAdapter)new ArrayAdapter((Context)this, 2131493104, (Object[])array));
            ((AdapterView)spinner).setSelection(this.g.getExamSet() - 1);
        }
        final EditText editText = (EditText)this.findViewById(2131296587);
        ((TextView)editText).setFilters(new InputFilter[] { (InputFilter)new InputFilter$LengthFilter(this.f) });
        final StringBuilder sb = new StringBuilder();
        sb.append(this.d);
        sb.append("");
        ((TextView)editText).setText((CharSequence)sb.toString());
        ((View)this.findViewById(2131296460)).setOnClickListener((View$OnClickListener)new View$OnClickListener(this, editText, spinner) {
            public final EditText a;
            public final Spinner b;
            public final EditResultActivity c;
            
            public void onClick(final View view) {
                if (this.a.getText().toString().trim().equals("")) {
                    a91.G((Context)this.c.c, 2131886267, 2131230909, 2131231086);
                    return;
                }
                final int int1 = Integer.parseInt(this.a.getText().toString());
                final Intent intent = new Intent();
                intent.putExtra("RESULT_ITEM", (Serializable)this.c.g);
                intent.putExtra("ROLL_NUMBER", int1);
                intent.putExtra("EXAM_SET", ((AdapterView)this.b).getSelectedItemPosition() + 1);
                this.c.setResult(EditResultActivity.A(), intent);
                this.c.C();
            }
        });
        this.findViewById(2131296424).setOnClickListener((View$OnClickListener)new View$OnClickListener(this) {
            public final EditResultActivity a;
            
            public void onClick(final View view) {
                this.a.C();
            }
        });
    }
    
    @Override
    public void onResume() {
        super.onResume();
        this.D();
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.ekodroid.omrevaluator.templateui.scansheet;

import android.os.BaseBundle;
import androidx.appcompat.app.a;
import android.view.ViewGroup;
import com.ekodroid.omrevaluator.database.TemplateDataJsonModel;
import com.ekodroid.omrevaluator.activities.Services.ScanSyncService;
import com.ekodroid.omrevaluator.templateui.models.AnswerSetKey;
import com.ekodroid.omrevaluator.database.repositories.TemplateRepository;
import android.provider.Settings$Secure;
import android.webkit.WebView;
import java.util.Iterator;
import android.content.DialogInterface;
import android.content.DialogInterface$OnClickListener;
import com.google.android.material.dialog.MaterialAlertDialogBuilder;
import java.io.FileNotFoundException;
import com.ekodroid.omrevaluator.database.SheetImageModel;
import java.io.OutputStream;
import java.io.FileOutputStream;
import android.graphics.Bitmap$CompressFormat;
import java.io.File;
import android.content.res.Resources;
import com.ekodroid.omrevaluator.database.StudentDataModel;
import com.ekodroid.omrevaluator.database.repositories.ClassRepository;
import android.widget.ImageView;
import com.ekodroid.omrevaluator.templateui.scanner.SheetDimension;
import android.graphics.Bitmap;
import com.ekodroid.omrevaluator.templateui.scanner.b;
import android.view.View;
import android.view.View$OnClickListener;
import android.os.Bundle;
import com.ekodroid.omrevaluator.templateui.scanner.AnswerValue;
import java.util.Collection;
import java.io.Serializable;
import android.content.Intent;
import android.content.Context;
import com.ekodroid.omrevaluator.database.repositories.ResultRepository;
import android.view.animation.Animation;
import android.view.animation.Animation$AnimationListener;
import com.ekodroid.omrevaluator.templateui.scanner.ResultItem;
import android.content.SharedPreferences$Editor;
import android.content.SharedPreferences;
import com.ekodroid.omrevaluator.templateui.models.SheetTemplate2;
import java.util.ArrayList;
import com.ekodroid.omrevaluator.database.ResultDataJsonModel;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.ProgressBar;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.ekodroid.omrevaluator.templateui.models.ExamId;

public class CheckedSheetImageActivity extends v5
{
    public static int M = 5;
    public int A;
    public boolean C;
    public CheckedSheetImageActivity D;
    public ExamId F;
    public FirebaseAnalytics G;
    public boolean H;
    public ProgressBar I;
    public c91 J;
    public boolean K;
    public TextView c;
    public TextView d;
    public TextView e;
    public ImageButton f;
    public LinearLayout g;
    public LinearLayout h;
    public LinearLayout i;
    public Button j;
    public Button k;
    public Button l;
    public String m;
    public ResultDataJsonModel n;
    public ArrayList p;
    public ArrayList q;
    public SheetTemplate2 t;
    public SharedPreferences v;
    public SharedPreferences$Editor w;
    public int x;
    public String y;
    public int z;
    
    public CheckedSheetImageActivity() {
        this.m = "";
        this.p = new ArrayList();
        this.q = new ArrayList();
        this.z = 0;
        this.A = 0;
        this.C = false;
        this.D = this;
        this.H = false;
        this.K = false;
    }
    
    public static /* synthetic */ boolean B(final CheckedSheetImageActivity checkedSheetImageActivity) {
        return checkedSheetImageActivity.K;
    }
    
    public static /* synthetic */ boolean G(final CheckedSheetImageActivity checkedSheetImageActivity) {
        return checkedSheetImageActivity.H;
    }
    
    public static /* synthetic */ CheckedSheetImageActivity I(final CheckedSheetImageActivity checkedSheetImageActivity) {
        return checkedSheetImageActivity.D;
    }
    
    public static /* synthetic */ ExamId J(final CheckedSheetImageActivity checkedSheetImageActivity) {
        return checkedSheetImageActivity.F;
    }
    
    public static String P(final String str, final int i, final int j) {
        final StringBuilder sb = new StringBuilder();
        sb.append(str);
        sb.append("/");
        sb.append(i);
        sb.append("_");
        sb.append(j);
        sb.append("_");
        sb.append(System.currentTimeMillis());
        sb.append(".jpg");
        return sb.toString();
    }
    
    public final void K() {
        if (!this.K) {
            return;
        }
        int int1;
        if ((int1 = this.v.getInt("AUTO_SAVE_DELAY", 3)) < 2) {
            int1 = 2;
        }
        (this.I = (ProgressBar)this.findViewById(2131296977)).setMax(1000);
        ((View)this.I).setVisibility(0);
        (this.J = new c91(this.I, (TextView)this.findViewById(2131297189), 0.0f, 1000.0f)).setDuration((long)(int1 * 1000));
        this.J.setAnimationListener((Animation$AnimationListener)new Animation$AnimationListener(this) {
            public final CheckedSheetImageActivity a;
            
            public void onAnimationEnd(final Animation animation) {
                if (CheckedSheetImageActivity.B(this.a)) {
                    this.a.b0();
                }
            }
            
            public void onAnimationRepeat(final Animation animation) {
            }
            
            public void onAnimationStart(final Animation animation) {
            }
        });
        ((View)this.I).startAnimation((Animation)this.J);
    }
    
    public final void L() {
        if (this.K) {
            this.K = false;
            final c91 j = this.J;
            if (j != null) {
                j.cancel();
            }
            final ProgressBar i = this.I;
            if (i != null) {
                ((View)i).setVisibility(8);
                this.findViewById(2131297189).setVisibility(8);
            }
        }
    }
    
    public final boolean M(final ResultDataJsonModel resultDataJsonModel) {
        return ResultRepository.getInstance((Context)this).getResult(new ExamId(resultDataJsonModel.getExamName(), resultDataJsonModel.getClassName(), resultDataJsonModel.getExamDate()), resultDataJsonModel.getRollNo()) != null;
    }
    
    public final void N() {
        if (((Context)this).getApplicationContext().getSharedPreferences("MyPref", 0).getLong("SCANC", 0L) > System.currentTimeMillis()) {
            this.finish();
        }
        ((Context)this).getSharedPreferences("MyPref", 0).edit().putLong("SCANC", System.currentTimeMillis()).commit();
    }
    
    public void O(final int n) {
        cj1.a();
        final Intent intent = new Intent((Context)this, (Class)ScanPaperActivity.class);
        intent.putExtra("EXAM_ID", (Serializable)this.F);
        intent.putExtra("START_SCAN", false);
        intent.putExtra("PAGE_INDEX", n);
        if (this.H) {
            intent.setFlags(33554432);
            intent.putExtra("SCAN_KEY", this.H);
        }
        ((Context)this).startActivity(intent);
        this.finish();
    }
    
    public final TextView Q(final String text) {
        final TextView textView = new TextView((Context)this);
        textView.setText((CharSequence)text);
        textView.setTextColor(this.getResources().getColor(2131099737));
        return textView;
    }
    
    public final int R() {
        return this.v.getInt("count_saveScan", 0) + this.v.getInt("count_cancelScan", 0);
    }
    
    public final void S() {
        this.w.putInt("count_cancelScan", this.v.getInt("count_cancelScan", 0) + 1);
        this.w.commit();
    }
    
    public final void T() {
        this.w.putInt("count_saveScan", this.v.getInt("count_saveScan", 0) + 1);
        this.w.commit();
    }
    
    public final ResultItem U(final ResultItem resultItem, final ResultItem resultItem2) {
        final ArrayList list = new ArrayList();
        final int examSet = resultItem.getExamSet();
        int examSet2 = -1;
        if (examSet > -1) {
            examSet2 = resultItem.getExamSet();
        }
        list.addAll(resultItem.getAnswerValue2s());
        int examSet3;
        if (resultItem2.getExamSet() > (examSet3 = examSet2)) {
            examSet3 = resultItem2.getExamSet();
        }
        list.addAll(resultItem2.getAnswerValue2s());
        e5.D(list);
        return new ResultItem(list, resultItem2.getSectionsInSubjects(), examSet3);
    }
    
    public final void V() {
        this.S();
        if (this.C) {
            FirebaseAnalytics.getInstance((Context)this.D).a("ScanCancelPredicted", null);
        }
        FirebaseAnalytics.getInstance((Context)this.D).a("ScanCancel", null);
        this.O(this.z);
    }
    
    public final void W() {
        ((View)this.f).setOnClickListener((View$OnClickListener)new View$OnClickListener(this) {
            public final CheckedSheetImageActivity a;
            
            public void onClick(final View view) {
                this.a.L();
                this.a.h0();
            }
        });
    }
    
    public final void X() {
        ((View)this.l).setOnClickListener((View$OnClickListener)new View$OnClickListener(this) {
            public final CheckedSheetImageActivity a;
            
            public void onClick(final View view) {
                if (this.a.z == 0) {
                    ox0.a();
                    ox0.d = this.a.t.getPageLayouts().length;
                }
                ox0.c.add(cj1.b);
                ox0.b.add(cj1.c);
                ox0.a.add(cj1.a);
                final int a = this.a.A;
                if (a > ox0.e) {
                    ox0.e = a;
                }
                final Intent intent = new Intent((Context)CheckedSheetImageActivity.I(this.a), (Class)ScanPaperActivity.class);
                intent.putExtra("EXAM_ID", (Serializable)CheckedSheetImageActivity.J(this.a));
                intent.putExtra("START_SCAN", false);
                intent.putExtra("PAGE_INDEX", this.a.z + 1);
                if (CheckedSheetImageActivity.G(this.a)) {
                    intent.setFlags(33554432);
                    intent.putExtra("SCAN_KEY", CheckedSheetImageActivity.G(this.a));
                }
                ((Context)this.a).startActivity(intent);
                this.a.finish();
            }
        });
    }
    
    public final void Y() {
        this.findViewById(2131296435).setOnClickListener((View$OnClickListener)new View$OnClickListener(this) {
            public final CheckedSheetImageActivity a;
            
            public void onClick(final View view) {
                this.a.L();
                if (CheckedSheetImageActivity.G(this.a)) {
                    this.a.finish();
                }
                else {
                    this.a.V();
                }
            }
        });
    }
    
    public final void Z() {
        this.findViewById(2131296451).setOnClickListener((View$OnClickListener)new View$OnClickListener(this) {
            public final CheckedSheetImageActivity a;
            
            public void onClick(final View view) {
                this.a.L();
                final CheckedSheetImageActivity a = this.a;
                final int rollNo = a.n.getRollNo();
                final CheckedSheetImageActivity a2 = this.a;
                a.g0(rollNo, a2.n.getResultItem(a2.t));
            }
        });
    }
    
    public final void a0() {
        ((View)this.k).setOnClickListener((View$OnClickListener)new View$OnClickListener(this) {
            public final CheckedSheetImageActivity a;
            
            public void onClick(final View view) {
                this.a.L();
                final CheckedSheetImageActivity a = this.a;
                if (a.n.getResultItem(a.t).getExamSet() < 0) {
                    a91.G((Context)CheckedSheetImageActivity.I(this.a), 2131886516, 2131230909, 2131231086);
                    return;
                }
                final Intent intent = new Intent();
                intent.putExtra("RESULT_DATA", (Serializable)this.a.n);
                this.a.setResult(-1, intent);
                ox0.a();
                cj1.a();
                this.a.finish();
            }
        });
    }
    
    public final void b0() {
        if (this.n.getResultItem(this.t).getExamSet() < 0) {
            a91.G((Context)this.D, 2131886516, 2131230909, 2131231086);
            return;
        }
        final ResultDataJsonModel n = this.n;
        if (n != null) {
            if (this.M(n)) {
                this.f0(this.n);
            }
            else {
                this.d0(this.n);
            }
        }
    }
    
    public final void c0() {
        final com.ekodroid.omrevaluator.templateui.scanner.b b = new com.ekodroid.omrevaluator.templateui.scanner.b();
        final ResultItem resultItem = this.n.getResultItem(this.t);
        this.i0(resultItem, this.t);
        final int size = this.q.size();
        final int n = 0;
        if (size > 1) {
            this.findViewById(2131296713).setVisibility(8);
            this.findViewById(2131297037).setVisibility(0);
            ((ViewGroup)this.i).removeAllViews();
            for (int i = 0; i < this.q.size(); ++i) {
                final Bitmap j = b.i(this.q.get(i), this.t, resultItem, this.p.get(i), this.n.getRollNo(), i);
                final ImageView imageView = new ImageView((Context)this.D);
                imageView.setAdjustViewBounds(true);
                ((ViewGroup)this.i).addView((View)imageView);
                imageView.setImageBitmap(j);
            }
        }
        else {
            final ImageView imageView2 = (ImageView)this.findViewById(2131296713);
            imageView2.setVisibility(0);
            this.findViewById(2131297037).setVisibility(8);
            imageView2.setImageBitmap(b.i(this.q.get(0), this.t, resultItem, this.p.get(0), this.n.getRollNo(), this.z));
        }
        final StudentDataModel student = ClassRepository.getInstance((Context)this.D).getStudent(this.n.getRollNo(), this.n.getClassName());
        if (student != null) {
            final TextView c = this.c;
            final StringBuilder sb = new StringBuilder();
            sb.append(((Context)this).getString(2131886747));
            sb.append(" : ");
            sb.append(this.n.getRollNo());
            sb.append("-");
            sb.append(student.getStudentName());
            c.setText((CharSequence)sb.toString());
        }
        else {
            final TextView c2 = this.c;
            final StringBuilder sb2 = new StringBuilder();
            sb2.append(((Context)this).getString(2131886747));
            sb2.append(" : ");
            sb2.append(this.n.getRollNo());
            c2.setText((CharSequence)sb2.toString());
        }
        final TextView d = this.d;
        final StringBuilder sb3 = new StringBuilder();
        sb3.append(((Context)this).getString(2131886412));
        sb3.append(" : ");
        sb3.append(ve1.o(resultItem.getTotalMarks()));
        d.setText((CharSequence)sb3.toString());
        if (this.x == 1) {
            ((View)this.e).setVisibility(8);
        }
        else {
            final String[] examSetLabels = this.t.getLabelProfile().getExamSetLabels();
            final int examSet = this.n.getResultItem(this.t).getExamSet();
            TextView textView;
            Resources resources;
            int n2;
            if (examSet > -1) {
                final TextView e = this.e;
                final StringBuilder sb4 = new StringBuilder();
                sb4.append(((Context)this).getString(2131886804));
                sb4.append(" : ");
                sb4.append(examSetLabels[examSet - 1]);
                e.setText((CharSequence)sb4.toString());
                textView = this.e;
                resources = this.getResources();
                n2 = 2131099737;
            }
            else {
                final TextView e2 = this.e;
                final StringBuilder sb5 = new StringBuilder();
                sb5.append(((Context)this).getString(2131886804));
                sb5.append(" : None");
                e2.setText((CharSequence)sb5.toString());
                textView = this.e;
                resources = this.getResources();
                n2 = 2131099731;
            }
            textView.setTextColor(resources.getColor(n2));
        }
        final double[] marksForEachSubject = resultItem.getMarksForEachSubject();
        if (marksForEachSubject.length > 1) {
            ((ViewGroup)this.g).removeAllViews();
            for (int k = n; k < marksForEachSubject.length; ++k) {
                final StringBuilder sb6 = new StringBuilder();
                sb6.append(this.t.getTemplateParams().getSubjects()[k].getSubName());
                sb6.append(" : ");
                sb6.append(ve1.o(marksForEachSubject[k]));
                ((ViewGroup)this.g).addView((View)this.Q(sb6.toString()));
            }
        }
    }
    
    public final void d0(final ResultDataJsonModel resultDataJsonModel) {
        resultDataJsonModel.setSynced(false);
        final boolean boolean1 = ((Context)this).getApplicationContext().getSharedPreferences("MyPref", 0).getBoolean("save_images", true);
        final ResultRepository instance = ResultRepository.getInstance((Context)this);
        instance.deleteStudentResult(new ExamId(resultDataJsonModel.getExamName(), resultDataJsonModel.getClassName(), resultDataJsonModel.getExamDate()), resultDataJsonModel.getRollNo());
        instance.deleteSheetImages(new ExamId(resultDataJsonModel.getExamName(), resultDataJsonModel.getClassName(), resultDataJsonModel.getExamDate()), resultDataJsonModel.getRollNo());
        if (boolean1) {
            try {
                final File file = new File(this.y);
                if (!file.exists()) {
                    file.mkdirs();
                }
                for (int i = 0; i < this.q.size(); ++i) {
                    final String p = P(this.y, resultDataJsonModel.getRollNo(), i);
                    this.q.get(i).compress(Bitmap$CompressFormat.JPEG, 50, (OutputStream)new FileOutputStream(p));
                    instance.saveOrUpdateSheetImage(new SheetImageModel(resultDataJsonModel.getExamName(), resultDataJsonModel.getRollNo(), null, resultDataJsonModel.getClassName(), resultDataJsonModel.getExamDate(), this.p.get(i), p, i));
                }
            }
            catch (final FileNotFoundException ex) {
                ex.printStackTrace();
            }
        }
        if (instance.saveOrUpdateResultJsonAsNotSynced(resultDataJsonModel)) {
            a91.G((Context)this.D, 2131886765, 2131230927, 2131231085);
        }
        this.T();
        if (this.p.get(0).getPredictedMarkers() != null && this.p.get(0).getPredictedMarkers().size() > 0) {
            FirebaseAnalytics.getInstance((Context)this.D).a("ScanSavePredicted", null);
        }
        ox0.a();
        this.O(0);
    }
    
    public final void e0() {
        ((View)this.j).setOnClickListener((View$OnClickListener)new View$OnClickListener(this) {
            public final CheckedSheetImageActivity a;
            
            public void onClick(final View view) {
                this.a.L();
                this.a.b0();
            }
        });
    }
    
    public final void f0(final ResultDataJsonModel resultDataJsonModel) {
        final MaterialAlertDialogBuilder materialAlertDialogBuilder = new MaterialAlertDialogBuilder((Context)this.D, 2131951953);
        final StringBuilder sb = new StringBuilder();
        sb.append(resultDataJsonModel.getRollNo());
        sb.append(((Context)this).getString(2131886546));
        materialAlertDialogBuilder.setMessage((CharSequence)sb.toString()).setPositiveButton(2131886903, (DialogInterface$OnClickListener)new DialogInterface$OnClickListener(this, resultDataJsonModel) {
            public final ResultDataJsonModel a;
            public final CheckedSheetImageActivity b;
            
            public void onClick(final DialogInterface dialogInterface, final int n) {
                this.b.d0(this.a);
                dialogInterface.dismiss();
            }
        }).setNegativeButton(2131886657, (DialogInterface$OnClickListener)new DialogInterface$OnClickListener(this) {
            public final CheckedSheetImageActivity a;
            
            public void onClick(final DialogInterface dialogInterface, final int n) {
                dialogInterface.dismiss();
            }
        }).create();
        ((a.a)materialAlertDialogBuilder).show();
    }
    
    public final void g0(final int n, final ResultItem resultItem) {
        jx0.b = new ArrayList();
        final Iterator iterator = this.q.iterator();
        while (iterator.hasNext()) {
            jx0.b.add(bc.a((Bitmap)iterator.next()));
        }
        jx0.a = this.p;
        final Intent intent = new Intent((Context)this.D, (Class)EditResultActivity.class);
        intent.putExtra("ROLL_NUMBER", n);
        intent.putExtra("RESULT_ITEM", (Serializable)resultItem);
        intent.putExtra("EXAM_ID", (Serializable)this.F);
        this.startActivityForResult(intent, CheckedSheetImageActivity.M);
    }
    
    public final void h0() {
        final WebView view = new WebView((Context)this);
        view.loadDataWithBaseURL((String)null, ((Context)this).getString(2131886560), "text/html", "utf-8", (String)null);
        final MaterialAlertDialogBuilder materialAlertDialogBuilder = new MaterialAlertDialogBuilder((Context)this.D, 2131951953);
        materialAlertDialogBuilder.setTitle(2131886527).setPositiveButton(2131886176, (DialogInterface$OnClickListener)new DialogInterface$OnClickListener(this) {
            public final CheckedSheetImageActivity a;
            
            public void onClick(final DialogInterface dialogInterface, final int n) {
                final CheckedSheetImageActivity a = this.a;
                a.d0(a.n);
                dialogInterface.dismiss();
            }
        }).setView((View)view).create();
        ((a.a)materialAlertDialogBuilder).show();
    }
    
    public final void i0(final ResultItem resultItem, final SheetTemplate2 sheetTemplate2) {
        if (resultItem != null && sheetTemplate2 != null) {
            ve1.w(resultItem, sheetTemplate2);
        }
    }
    
    @Override
    public void onActivityResult(int int1, int int2, final Intent intent) {
        super.onActivityResult(int1, int2, intent);
        if (int1 == CheckedSheetImageActivity.M && intent != null) {
            final ResultItem resultItem = (ResultItem)intent.getExtras().getSerializable("RESULT_ITEM");
            int2 = ((BaseBundle)intent.getExtras()).getInt("ROLL_NUMBER");
            int1 = ((BaseBundle)intent.getExtras()).getInt("EXAM_SET");
            this.n.setRollNo(int2);
            final ResultItem resultItem2 = this.n.getResultItem(this.t);
            resultItem2.setExamSet(int1);
            resultItem2.setAnswerValues(resultItem.getAnswerValue2s());
            this.n.setResultItem(resultItem2);
            this.c0();
            FirebaseAnalytics.getInstance((Context)this.D).a("ScanEdit", null);
        }
    }
    
    @Override
    public void onBackPressed() {
        this.V();
        super.onBackPressed();
    }
    
    @Override
    public void onCreate(Bundle extras) {
        super.onCreate(extras);
        this.setContentView(2131492898);
        this.G = o4.a((Context)this);
        final SharedPreferences sharedPreferences = ((Context)this).getApplicationContext().getSharedPreferences("MyPref", 0);
        this.v = sharedPreferences;
        this.w = sharedPreferences.edit();
        this.m = Settings$Secure.getString(((Context)this).getContentResolver(), "android_id");
        this.f = (ImageButton)this.findViewById(2131296673);
        this.e = (TextView)this.findViewById(2131297228);
        this.c = (TextView)this.findViewById(2131297274);
        this.d = (TextView)this.findViewById(2131297248);
        this.g = (LinearLayout)this.findViewById(2131296770);
        this.h = (LinearLayout)this.findViewById(2131296792);
        this.j = (Button)this.findViewById(2131296469);
        this.k = (Button)this.findViewById(2131296457);
        this.l = (Button)this.findViewById(2131296471);
        ((View)(this.i = (LinearLayout)this.findViewById(2131296762))).setOnClickListener((View$OnClickListener)new View$OnClickListener(this) {
            public final CheckedSheetImageActivity a;
            
            public void onClick(final View view) {
                this.a.L();
            }
        });
        this.findViewById(2131296713).setOnClickListener((View$OnClickListener)new View$OnClickListener(this) {
            public final CheckedSheetImageActivity a;
            
            public void onClick(final View view) {
                this.a.L();
            }
        });
        this.e0();
        this.a0();
        this.Y();
        this.Z();
        this.W();
        this.X();
        extras = this.getIntent().getExtras();
        if (extras != null) {
            this.A = ((BaseBundle)extras).getInt("ROLL_NUMBER");
            this.H = ((BaseBundle)extras).getBoolean("SCAN_KEY", false);
            this.F = (ExamId)this.getIntent().getSerializableExtra("EXAM_ID");
            this.z = this.getIntent().getIntExtra("PAGE_INDEX", 0);
            final TemplateDataJsonModel templateJson = TemplateRepository.getInstance((Context)this.D).getTemplateJson(this.F);
            this.t = templateJson.getSheetTemplate();
            this.y = templateJson.getFolderPath();
            this.x = this.t.getTemplateParams().getExamSets();
            if (this.z < this.t.getPageLayouts().length - 1) {
                this.t.setAnswerKeys(null);
                ((View)this.j).setVisibility(8);
                ((View)this.k).setVisibility(8);
                ((View)this.h).setVisibility(8);
                ((View)this.g).setVisibility(8);
                ((View)this.l).setVisibility(0);
                this.findViewById(2131296451).setVisibility(4);
            }
            else {
                this.findViewById(2131296451).setVisibility(0);
                ((View)this.l).setVisibility(8);
                if (this.H) {
                    this.t.setAnswerKeys(null);
                    ((View)this.j).setVisibility(8);
                    ((View)this.k).setVisibility(0);
                    ((View)this.h).setVisibility(8);
                    ((View)this.g).setVisibility(8);
                }
                else {
                    this.K = this.v.getBoolean("AUTO_SAVE_SCAN", false);
                    ((View)this.j).setVisibility(0);
                    ((View)this.k).setVisibility(8);
                    ((View)this.h).setVisibility(0);
                    ((View)this.g).setVisibility(0);
                }
            }
            if (hn0.d > 15) {
                FirebaseAnalytics.getInstance((Context)this.D).a("ScanAdjusted", null);
            }
            ResultItem b;
            if (this.t.getPageLayouts().length == 1) {
                FirebaseAnalytics.getInstance((Context)this.D).a("ScanSinglePage", null);
                b = cj1.b;
                this.p.add(cj1.c);
                this.q.add(cj1.a);
            }
            else {
                FirebaseAnalytics.getInstance((Context)this.D).a("ScanMultiPage", null);
                if (this.z == 0) {
                    ox0.a();
                }
                final int e = ox0.e;
                if (e > this.A) {
                    this.A = e;
                }
                this.p.addAll(ox0.b);
                this.p.add(cj1.c);
                this.q.addAll(ox0.a);
                this.q.add(cj1.a);
                ResultItem resultItem = cj1.b;
                final Iterator iterator = ox0.c.iterator();
                while (true) {
                    b = resultItem;
                    if (!iterator.hasNext()) {
                        break;
                    }
                    resultItem = this.U(resultItem, (ResultItem)iterator.next());
                }
            }
            this.n = new ResultDataJsonModel(this.F.getExamName(), this.A, this.F.getClassName(), this.F.getExamDate(), b, false, false);
            if (this.q.size() == 0) {
                this.O(0);
            }
            this.c0();
        }
        this.N();
        this.C = false;
        for (final SheetDimension sheetDimension : this.p) {
            if (sheetDimension.getPredictedMarkers() != null && sheetDimension.getPredictedMarkers().size() > 0) {
                this.C = true;
            }
        }
        if (this.C) {
            ((View)this.f).setVisibility(0);
            this.K = false;
        }
        else {
            ((View)this.f).setVisibility(8);
        }
        if (this.R() % 10 == 9) {
            ((Context)this).startService(new Intent(((Context)this).getApplicationContext(), (Class)ScanSyncService.class));
        }
        dj1.c((Context)this);
        this.K();
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.ekodroid.omrevaluator.templateui.scansheet;

import android.os.AsyncTask;
import java.io.IOException;
import android.media.MediaPlayer;
import android.view.MenuItem;
import android.view.Menu;
import android.view.View;
import android.view.View$OnClickListener;
import androidx.appcompat.widget.Toolbar;
import com.ekodroid.omrevaluator.exams.ResultActivity;
import android.content.DialogInterface;
import android.content.DialogInterface$OnClickListener;
import com.google.android.material.dialog.MaterialAlertDialogBuilder;
import com.ekodroid.omrevaluator.more.ContactUsActivity;
import android.content.pm.PackageManager$NameNotFoundException;
import com.ekodroid.omrevaluator.database.repositories.TemplateRepository;
import java.util.Calendar;
import java.util.Date;
import java.text.SimpleDateFormat;
import com.ekodroid.omrevaluator.more.SettingsActivity;
import com.ekodroid.omrevaluator.more.InstructionsActivity;
import android.provider.Settings$Secure;
import java.io.Serializable;
import android.content.Intent;
import com.ekodroid.omrevaluator.templateui.scanner.ResultItem;
import android.graphics.Xfermode;
import android.graphics.PorterDuffXfermode;
import android.graphics.Paint$Style;
import android.graphics.Color;
import android.graphics.Paint;
import com.ekodroid.omrevaluator.templateui.scanner.c;
import android.graphics.Bitmap;
import android.graphics.Matrix;
import android.graphics.BitmapFactory;
import com.ekodroid.omrevaluator.templateui.scanner.SheetResult;
import java.util.Iterator;
import android.graphics.Canvas;
import android.os.Bundle;
import android.content.Context;
import android.graphics.PorterDuff$Mode;
import java.util.ArrayList;
import com.ekodroid.omrevaluator.components.AutofitPreview;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.widget.TextView;
import android.os.Handler;
import com.ekodroid.omrevaluator.serializable.SheetImageConfigure;
import android.hardware.Camera$Size;
import com.ekodroid.omrevaluator.templateui.models.SheetTemplate2;
import com.google.firebase.analytics.FirebaseAnalytics;
import android.content.SharedPreferences;
import com.ekodroid.omrevaluator.templateui.models.ExamId;
import com.ekodroid.omrevaluator.templateui.scanner.a;
import android.widget.ProgressBar;

public class ScanPaperActivity extends v5 implements xe
{
    public ProgressBar[] A;
    public ProgressBar C;
    public int[] D;
    public a F;
    public ExamId G;
    public SharedPreferences H;
    public fj1 I;
    public FirebaseAnalytics J;
    public int K;
    public long M;
    public ej1 O;
    public ScanPaperActivity c;
    public SheetTemplate2 d;
    public Camera$Size e;
    public SheetImageConfigure f;
    public boolean g;
    public boolean h;
    public boolean i;
    public Handler j;
    public androidx.appcompat.app.a k;
    public TextView l;
    public TextView m;
    public z71 n;
    public boolean p;
    public boolean q;
    public boolean t;
    public boolean v;
    public SurfaceView w;
    public SurfaceHolder x;
    public we y;
    public AutofitPreview z;
    
    public ScanPaperActivity() {
        this.c = this;
        this.g = false;
        this.h = false;
        this.i = false;
        this.p = false;
        this.y = null;
        this.K = 0;
        this.M = 0L;
        this.O = new ej1(this) {
            public final ScanPaperActivity a;
            
            @Override
            public void a(final ArrayList list) {
                this.a.j.post((Runnable)new Runnable(this, list) {
                    public final ArrayList a;
                    public final ScanPaperActivity$a b;
                    
                    @Override
                    public void run() {
                        final Canvas lockCanvas = ScanPaperActivity.A(this.b.a).lockCanvas();
                        if (lockCanvas != null) {
                            lockCanvas.drawColor(0, PorterDuff$Mode.CLEAR);
                            lockCanvas.drawARGB(40, 255, 255, 255);
                            final ScanPaperActivity a = this.b.a;
                            final z71 n = a.n;
                            a.V(lockCanvas, (float)n.e, (float)n.f, n.g);
                            for (final em em : this.a) {
                                final ScanPaperActivity a2 = this.b.a;
                                a2.U(lockCanvas, em, a2.n);
                            }
                            ScanPaperActivity.A(this.b.a).unlockCanvasAndPost(lockCanvas);
                        }
                        if (hn0.d > 15 || hn0.e > 15) {
                            hn0.h *= 0.7;
                        }
                        if (System.currentTimeMillis() - ScanPaperActivity.M(this.b.a) > 4000L) {
                            FirebaseAnalytics firebaseAnalytics;
                            String s;
                            if (hn0.e > 20) {
                                hn0.e = 0;
                                hn0.h = 0.3;
                                this.b.a.g0(2131492990);
                                firebaseAnalytics = FirebaseAnalytics.getInstance((Context)this.b.a.c);
                                s = "ERROR3";
                            }
                            else if (hn0.d > 20) {
                                hn0.d = 0;
                                hn0.h = 0.3;
                                if (dj1.c > 1) {
                                    this.b.a.g0(2131492989);
                                    firebaseAnalytics = FirebaseAnalytics.getInstance((Context)this.b.a.c);
                                    s = "ERROR2";
                                }
                                else {
                                    this.b.a.g0(2131492988);
                                    firebaseAnalytics = FirebaseAnalytics.getInstance((Context)this.b.a.c);
                                    s = "ERROR1";
                                }
                            }
                            else {
                                if (hn0.f <= 10) {
                                    return;
                                }
                                hn0.f = 0;
                                this.b.a.g0(2131492991);
                                firebaseAnalytics = FirebaseAnalytics.getInstance((Context)this.b.a.c);
                                s = "ERROR4";
                            }
                            firebaseAnalytics.a(s, null);
                        }
                    }
                });
            }
        };
    }
    
    public static /* synthetic */ SurfaceHolder A(final ScanPaperActivity scanPaperActivity) {
        return scanPaperActivity.x;
    }
    
    public static /* synthetic */ boolean C(final ScanPaperActivity scanPaperActivity) {
        return scanPaperActivity.t;
    }
    
    public static /* synthetic */ a D(final ScanPaperActivity scanPaperActivity) {
        return scanPaperActivity.F;
    }
    
    public static /* synthetic */ ej1 E(final ScanPaperActivity scanPaperActivity) {
        return scanPaperActivity.O;
    }
    
    public static /* synthetic */ ProgressBar[] H(final ScanPaperActivity scanPaperActivity) {
        return scanPaperActivity.A;
    }
    
    public static /* synthetic */ boolean I(final ScanPaperActivity scanPaperActivity) {
        return scanPaperActivity.v;
    }
    
    public static /* synthetic */ long M(final ScanPaperActivity scanPaperActivity) {
        return scanPaperActivity.M;
    }
    
    public static /* synthetic */ we O(final ScanPaperActivity scanPaperActivity) {
        return scanPaperActivity.y;
    }
    
    public static /* synthetic */ int[] P(final ScanPaperActivity scanPaperActivity) {
        return scanPaperActivity.D;
    }
    
    public static /* synthetic */ int R(final ScanPaperActivity scanPaperActivity) {
        return scanPaperActivity.K;
    }
    
    public final SheetResult S(final d91 d91, final ej1 ej1, int min) {
        byte[] array;
        boolean b;
        if (this.g && this.F.d() > 0 && min == 0) {
            array = this.y.getImage();
            b = true;
        }
        else {
            array = this.y.getFrame();
            b = false;
        }
        d91.a(10);
        if (array == null) {
            return null;
        }
        final Bitmap decodeByteArray = BitmapFactory.decodeByteArray(array, 0, array.length);
        Bitmap bitmap = null;
        Label_0173: {
            if (this.i) {
                bitmap = decodeByteArray;
                if (decodeByteArray.getWidth() > decodeByteArray.getHeight()) {
                    break Label_0173;
                }
            }
            if (!this.i && decodeByteArray.getWidth() < decodeByteArray.getHeight()) {
                bitmap = decodeByteArray;
            }
            else {
                final Matrix matrix = new Matrix();
                matrix.postRotate(90.0f);
                bitmap = Bitmap.createBitmap(decodeByteArray, 0, 0, decodeByteArray.getWidth(), decodeByteArray.getHeight(), matrix, true);
                decodeByteArray.recycle();
            }
        }
        Bitmap e = bitmap;
        if (b) {
            min = Math.min(bitmap.getWidth(), bitmap.getHeight());
            e = bitmap;
            if (min > 1600) {
                e = bc.e(bitmap, (float)(1500.0 / min));
            }
        }
        d91.a(20);
        this.T(e, this.f);
        final SheetResult a = new com.ekodroid.omrevaluator.templateui.scanner.c().a(new j5(e, this.f, this.d), new hn0(), d91, ej1, b, this.h, this.K);
        if (this.g && b) {
            return a;
        }
        final SheetResult a2 = this.F.a(a);
        if (!this.g) {
            return a2;
        }
        return null;
    }
    
    public final void T(final Bitmap bitmap, final SheetImageConfigure sheetImageConfigure) {
        if (bitmap.getHeight() != sheetImageConfigure.imageHeight || bitmap.getWidth() != sheetImageConfigure.imageWidth) {
            FirebaseAnalytics.getInstance((Context)this).a("InvalidCamera", null);
        }
    }
    
    public final void U(final Canvas canvas, final em em, final z71 z71) {
        double n;
        int n2;
        int a;
        if (((View)this.m).getVisibility() == 8) {
            n = z71.d * 1.0 / em.e;
            n2 = (int)(em.b * n) + z71.b;
            a = em.a;
        }
        else {
            n = z71.d * 1.0 / em.d;
            n2 = (int)(em.a * n) + z71.b;
            a = em.e - em.b;
        }
        this.X(canvas, (int)(a * n) + z71.a, n2, (int)(n * em.c));
    }
    
    public final void V(final Canvas canvas, final float n, final float n2, final int n3) {
        final int n4 = (int)n;
        final int n5 = (int)n2;
        this.Y(canvas, n4, n5, n3);
        this.Y(canvas, (int)(canvas.getWidth() - n), n5, n3);
        this.Y(canvas, n4, (int)(canvas.getHeight() - n2), n3);
        this.Y(canvas, (int)(canvas.getWidth() - n), (int)(canvas.getHeight() - n2), n3);
        this.W(canvas, n4, n5, n3);
        this.W(canvas, (int)(canvas.getWidth() - n), n5, n3);
        this.W(canvas, n4, (int)(canvas.getHeight() - n2), n3);
        this.W(canvas, (int)(canvas.getWidth() - n), (int)(canvas.getHeight() - n2), n3);
    }
    
    public final void W(final Canvas canvas, final int n, final int n2, int n3) {
        final Paint paint = new Paint();
        paint.setColor(Color.argb(255, 0, 0, 255));
        paint.setStrokeWidth(3.0f);
        paint.setStyle(Paint$Style.STROKE);
        n3 /= 2;
        canvas.drawRect((float)(n - n3), (float)(n2 - n3), (float)(n + n3), (float)(n2 + n3), paint);
    }
    
    public final void X(final Canvas canvas, final int n, final int n2, int n3) {
        final Paint paint = new Paint();
        paint.setColor(Color.argb(255, 0, 255, 0));
        paint.setStrokeWidth(3.0f);
        paint.setStyle(Paint$Style.STROKE);
        n3 /= 2;
        canvas.drawRect((float)(n - n3), (float)(n2 - n3), (float)(n + n3), (float)(n2 + n3), paint);
    }
    
    public final void Y(final Canvas canvas, final int n, final int n2, int n3) {
        final Paint paint = new Paint();
        paint.setXfermode((Xfermode)new PorterDuffXfermode(PorterDuff$Mode.CLEAR));
        paint.setColor(Color.argb(0, 255, 255, 255));
        paint.setStyle(Paint$Style.FILL);
        n3 /= 2;
        canvas.drawRect((float)(n - n3), (float)(n2 - n3), (float)(n + n3), (float)(n2 + n3), paint);
    }
    
    public final void Z(final SheetResult sheetResult) {
        cj1.b = new ResultItem(sheetResult.getMarkedAnswers(), this.d.getSectionsInSubject(), sheetResult.getExamSet());
        cj1.c = sheetResult.getSheetDimension();
        final Intent intent = new Intent((Context)this.c, (Class)CheckedSheetImageActivity.class);
        intent.putExtra("ROLL_NUMBER", sheetResult.getRollNumber());
        intent.putExtra("EXAM_ID", (Serializable)this.G);
        intent.putExtra("PAGE_INDEX", this.K);
        if (this.p) {
            intent.putExtra("SCAN_KEY", true);
            intent.setFlags(33554432);
        }
        ((Context)this.c).startActivity(intent);
        this.c.finish();
    }
    
    @Override
    public void a(final Camera$Size e, final Camera$Size camera$Size) {
        this.e = e;
        if (camera$Size == null) {
            this.g = false;
        }
        if (Math.min(e.height, e.width) > 900) {
            this.g = false;
        }
        if (e.height > e.width) {
            o4.c(this.J, "CAMERA_FRAME_ERROR", Settings$Secure.getString(((Context)this).getContentResolver(), "android_id"));
        }
        new h(null).execute((Object[])new Void[0]);
    }
    
    public final void a0() {
        ((Context)this).startActivity(new Intent((Context)this.c, (Class)InstructionsActivity.class));
    }
    
    public final void b0() {
        final Intent intent = new Intent((Context)this.c, (Class)SettingsActivity.class);
        intent.putExtra("EXAM_ID", (Serializable)this.G);
        ((Context)this).startActivity(intent);
        this.finish();
    }
    
    public final boolean c0(final int n, final SharedPreferences sharedPreferences, final int i, final String s) {
        try {
            final SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
            final StringBuilder sb = new StringBuilder();
            sb.append(ok.x);
            sb.append(i);
            final Date parse = simpleDateFormat.parse(b.g(sharedPreferences.getString(sb.toString(), b.i(simpleDateFormat.format(new Date()), s)), s));
            final Calendar instance = Calendar.getInstance();
            instance.setTime(parse);
            instance.add(5, n);
            return !new Date().after(instance.getTime());
        }
        catch (final Exception ex) {
            ex.printStackTrace();
            return false;
        }
    }
    
    public final boolean d0() {
        if (this.G != null) {
            final SheetTemplate2 sheetTemplate = TemplateRepository.getInstance((Context)this.c).getTemplateJson(this.G).getSheetTemplate();
            this.d = sheetTemplate;
            if (ve1.e(this.d.getColumnWidthInBubbles(this.K)) + 1 > sheetTemplate.getRows(this.K) * 1.2) {
                ((View)this.m).setVisibility(0);
                ((View)this.l).setVisibility(8);
                this.i = true;
            }
            else {
                ((View)this.m).setVisibility(8);
                ((View)this.l).setVisibility(0);
                this.i = false;
            }
            if (this.n() != null) {
                this.n().t(this.G.getExamName());
                this.n().s(this.G.getClassName());
            }
            if (this.d.getPageLayouts().length > 1) {
                final TextView textView = (TextView)this.findViewById(2131297257);
                ((View)textView).setVisibility(0);
                final StringBuilder sb = new StringBuilder();
                sb.append(((Context)this).getString(2131886690));
                sb.append(" ");
                sb.append(this.K + 1);
                sb.append("/");
                sb.append(this.d.getPageLayouts().length);
                textView.setText((CharSequence)sb.toString());
            }
            else {
                this.findViewById(2131297257).setVisibility(8);
            }
        }
        return true;
    }
    
    public void e0() {
        final boolean booleanExtra = this.getIntent().getBooleanExtra("START_SCAN", true);
        final int n = 999;
        if (booleanExtra) {
            final String string = Settings$Secure.getString(((Context)this).getContentResolver(), "android_id");
            int versionCode;
            try {
                versionCode = ((Context)this).getPackageManager().getPackageInfo(((Context)this).getPackageName(), 0).versionCode;
            }
            catch (final PackageManager$NameNotFoundException ex) {
                ((Throwable)ex).printStackTrace();
                versionCode = 999;
            }
            if (!this.c0(20, ((Context)this).getApplicationContext().getSharedPreferences("MyPref", 0), versionCode, string)) {
                this.finish();
            }
            if (versionCode != a91.f()) {
                this.finish();
            }
        }
        if (((Context)this).getApplicationContext().getSharedPreferences("MyPref", 0).getLong("SCAN_MODE", 0L) > System.currentTimeMillis()) {
            this.finish();
        }
        this.M = System.currentTimeMillis();
        ((Context)this).getSharedPreferences("MyPref", 0).edit().putLong("SCAN_MODE", this.M);
        if (this.d != null) {
            this.t = true;
            final String string2 = Settings$Secure.getString(((Context)this).getContentResolver(), "android_id");
            int versionCode2;
            try {
                versionCode2 = ((Context)this).getPackageManager().getPackageInfo(((Context)this).getPackageName(), 0).versionCode;
            }
            catch (final PackageManager$NameNotFoundException ex2) {
                ((Throwable)ex2).printStackTrace();
                versionCode2 = n;
            }
            a91.E(((Context)this).getSharedPreferences("MyPref", 0), string2, versionCode2);
            if (!gk.c()) {
                this.finish();
            }
            hn0.j();
            final g d = new g().d(0);
            final g d2 = new g().d(1);
            new Thread(d).start();
            new Thread(d2).start();
        }
    }
    
    public final boolean f0(final SheetResult sheetResult) {
        if (!this.q || !this.t || sheetResult == null) {
            return false;
        }
        this.t = false;
        if (sheetResult.getCheckedBitmap() == null) {
            return false;
        }
        cj1.a = sheetResult.getCheckedBitmap();
        return true;
    }
    
    public final void g0(final int n) {
        final fj1 i = this.I;
        if (i != null && i.b()) {
            return;
        }
        this.I = new fj1((Context)this.c, new y01(this) {
            public final ScanPaperActivity a;
            
            @Override
            public void a(final Object o) {
                ((Context)this.a).startActivity(new Intent((Context)this.a.c, (Class)ContactUsActivity.class));
                this.a.c.finish();
            }
        }, n);
    }
    
    public final void h0() {
        this.j.post((Runnable)new Runnable(this) {
            public final ScanPaperActivity a;
            
            @Override
            public void run() {
                if (this.a.k == null) {
                    final MaterialAlertDialogBuilder materialAlertDialogBuilder = new MaterialAlertDialogBuilder((Context)this.a.c, 2131951953);
                    materialAlertDialogBuilder.setTitle((CharSequence)((Context)this.a).getString(2131886772)).setMessage((CharSequence)((Context)this.a).getString(2131886562)).setPositiveButton((CharSequence)((Context)this.a).getString(2131886176), (DialogInterface$OnClickListener)new DialogInterface$OnClickListener(this) {
                        public final ScanPaperActivity$e a;
                        
                        public void onClick(final DialogInterface dialogInterface, final int n) {
                            dialogInterface.dismiss();
                        }
                    });
                    this.a.k = materialAlertDialogBuilder.create();
                }
                this.a.k.show();
            }
        });
    }
    
    public final void i0(final String message) {
        final MaterialAlertDialogBuilder materialAlertDialogBuilder = new MaterialAlertDialogBuilder((Context)this.c, 2131951953);
        materialAlertDialogBuilder.setTitle((CharSequence)((Context)this).getString(2131886773)).setMessage((CharSequence)message).setCancelable(false).setPositiveButton((CharSequence)((Context)this).getString(2131886331), (DialogInterface$OnClickListener)new DialogInterface$OnClickListener(this) {
            public final ScanPaperActivity a;
            
            public void onClick(final DialogInterface dialogInterface, final int n) {
                ((Context)this.a).getApplicationContext().getSharedPreferences("MyPref", 0).edit().putBoolean("show_scan_instruction", false).commit();
                dialogInterface.dismiss();
            }
        }).create();
        materialAlertDialogBuilder.create().show();
    }
    
    public final void j0(int progress, int n) {
        final int[] d = this.D;
        d[progress + 1] = n;
        ProgressBar progressBar;
        if (d[0] > 0) {
            this.A[0].setProgress(100);
            final int[] d2 = this.D;
            progress = d2[1];
            n = d2[2];
            if (progress <= n) {
                this.A[1].setProgress(n);
                return;
            }
            progressBar = this.A[1];
        }
        else {
            this.A[1].setProgress(0);
            final int[] d3 = this.D;
            n = d3[1];
            progress = d3[2];
            if (n > progress) {
                this.A[0].setProgress(n);
                return;
            }
            progressBar = this.A[0];
        }
        progressBar.setProgress(progress);
    }
    
    public final void k0() {
        final Intent intent = new Intent((Context)this.c, (Class)ResultActivity.class);
        intent.putExtra("EXAM_ID", (Serializable)this.G);
        ((Context)this).startActivity(intent);
    }
    
    @Override
    public void onCreate(final Bundle bundle) {
        super.onCreate(bundle);
        this.setContentView(2131492920);
        this.x((Toolbar)this.findViewById(2131297334));
        this.J = FirebaseAnalytics.getInstance((Context)this);
        this.G = (ExamId)this.getIntent().getSerializableExtra("EXAM_ID");
        this.p = this.getIntent().getBooleanExtra("SCAN_KEY", false);
        this.K = this.getIntent().getIntExtra("PAGE_INDEX", 0);
        this.z = (AutofitPreview)this.findViewById(2131297133);
        final ProgressBar[] a = new ProgressBar[2];
        this.A = a;
        this.D = new int[3];
        a[0] = (ProgressBar)this.findViewById(2131297030);
        this.A[1] = (ProgressBar)this.findViewById(2131297029);
        this.C = (ProgressBar)this.findViewById(2131297031);
        this.l = (TextView)this.findViewById(2131297365);
        this.m = (TextView)this.findViewById(2131297364);
        this.j = new Handler();
        this.d0();
        final SharedPreferences sharedPreferences = ((Context)this).getApplicationContext().getSharedPreferences("MyPref", 0);
        this.H = sharedPreferences;
        this.v = sharedPreferences.getBoolean("scan_sound", true);
        this.g = this.H.getBoolean("scan_resolution_high", false);
        this.h = this.H.getBoolean("option_mark_with_pen", true);
        if (this.H.getBoolean("show_scan_instruction", true)) {
            final int int1 = Integer.parseInt(new SimpleDateFormat("HH").format(Calendar.getInstance().getTime()));
            int n;
            if (int1 >= 7 && int1 <= 18) {
                n = 2131886559;
            }
            else {
                n = 2131886558;
            }
            this.i0(((Context)this).getString(n));
        }
        this.y = new ve(this.z, this, this.g, ve1.e(this.d.getColumnWidthInBubbles(this.K)) + 1);
        (this.w = (SurfaceView)this.findViewById(2131296954)).setZOrderOnTop(true);
        ((View)this.w).setBackgroundColor(Color.argb(0, 255, 255, 255));
        this.x = this.w.getHolder();
        ((View)this.w).setOnClickListener((View$OnClickListener)new View$OnClickListener(this) {
            public final ScanPaperActivity a;
            
            public void onClick(final View view) {
                ScanPaperActivity.O(this.a).b();
            }
        });
        this.F = new a(new d91(this) {
            public final ScanPaperActivity a;
            
            @Override
            public void a(final int n) {
                ScanPaperActivity.P(this.a)[0] = n;
                if (n == 5 || (n > 5 && n % 3 == 0)) {
                    this.a.h0();
                }
            }
        });
    }
    
    public boolean onCreateOptionsMenu(final Menu menu) {
        this.getMenuInflater().inflate(2131623943, menu);
        return true;
    }
    
    public boolean onOptionsItemSelected(final MenuItem menuItem) {
        final int itemId = menuItem.getItemId();
        if (itemId == 2131296326) {
            this.a0();
            return true;
        }
        if (itemId == 2131296336) {
            this.b0();
            return true;
        }
        if (itemId != 2131296344) {
            return super.onOptionsItemSelected(menuItem);
        }
        this.k0();
        return true;
    }
    
    @Override
    public void onPause() {
        super.onPause();
        this.q = false;
        final we y = this.y;
        if (y != null) {
            y.a();
        }
        this.finish();
    }
    
    @Override
    public void onResume() {
        super.onResume();
        if (!a91.a((Context)this.c)) {
            this.finish();
        }
        this.q = true;
    }
    
    @Override
    public boolean v() {
        this.finish();
        return true;
    }
    
    public class g implements Runnable
    {
        public SheetResult a;
        public int b;
        public d91 c;
        public final ScanPaperActivity d;
        
        public g(final ScanPaperActivity d) {
            this.d = d;
            this.a = null;
            this.b = 0;
            this.c = new d91(this) {
                public final g a;
                
                @Override
                public void a(final int n) {
                    this.a.e(n);
                }
            };
        }
        
        public final void c() {
            if (this.a != null) {
                if (ScanPaperActivity.O(this.d) != null) {
                    ScanPaperActivity.O(this.d).a();
                }
                ScanPaperActivity.H(this.d)[this.b].setProgress(0);
                if (ScanPaperActivity.I(this.d)) {
                    MediaPlayer.create((Context)this.d.c, 2131820544).start();
                }
                this.d.Z(this.a);
            }
        }
        
        public g d(final int b) {
            this.b = b;
            return this;
        }
        
        public final void e(final int n) {
            this.d.j.post((Runnable)new Runnable(this, n) {
                public final int a;
                public final g b;
                
                @Override
                public void run() {
                    final g b = this.b;
                    b.d.j0(b.b, this.a);
                }
            });
        }
        
        @Override
        public void run() {
            while (this.a == null && ScanPaperActivity.C(this.d)) {
                try {
                    this.e(5);
                    final ScanPaperActivity d = this.d;
                    if (d.g && ScanPaperActivity.D(d).d() > 0 && this.b == 1) {
                        return;
                    }
                    final ScanPaperActivity d2 = this.d;
                    this.a = d2.S(this.c, ScanPaperActivity.E(d2), this.b);
                }
                catch (final IOException ex) {
                    ex.printStackTrace();
                }
            }
            Handler handler;
            Runnable runnable;
            if (ScanPaperActivity.C(this.d) && this.d.f0(this.a)) {
                handler = this.d.j;
                runnable = new Runnable(this) {
                    public final g a;
                    
                    @Override
                    public void run() {
                        this.a.c();
                    }
                };
            }
            else {
                handler = this.d.j;
                runnable = new Runnable(this) {
                    public final g a;
                    
                    @Override
                    public void run() {
                        this.a.d.c.finish();
                    }
                };
            }
            handler.post(runnable);
        }
    }
    
    public class h extends AsyncTask
    {
        public final ScanPaperActivity a;
        
        public h(final ScanPaperActivity a) {
            this.a = a;
        }
        
        public Void a(final Void... array) {
            ScanPaperActivity.O(this.a).b();
            ScanPaperActivity.A(this.a).setFormat(-2);
            final Canvas lockCanvas = ScanPaperActivity.A(this.a).lockCanvas();
            if (lockCanvas != null) {
                lockCanvas.drawColor(0);
                lockCanvas.drawARGB(40, 255, 255, 255);
                final Camera$Size e = this.a.e;
                this.b(lockCanvas, e.width, e.height);
                ScanPaperActivity.A(this.a).unlockCanvasAndPost(lockCanvas);
            }
            return null;
        }
        
        public final void b(final Canvas canvas, int bottomCornerHeightPercent, final int n) {
            final ScanPaperActivity a = this.a;
            final SheetTemplate2 d = a.d;
            float n2;
            float n3;
            if (d != null) {
                n2 = (float)d.getRows(ScanPaperActivity.R(a));
                final ScanPaperActivity a2 = this.a;
                n3 = (float)(ve1.e(a2.d.getColumnWidthInBubbles(ScanPaperActivity.R(a2))) + 1);
                final ScanPaperActivity a3 = this.a;
                if (a3.i) {
                    n2 = (float)(ve1.e(a3.d.getColumnWidthInBubbles(ScanPaperActivity.R(a3))) + 1);
                    final ScanPaperActivity a4 = this.a;
                    n3 = (float)a4.d.getRows(ScanPaperActivity.R(a4));
                }
            }
            else {
                n2 = 21.0f;
                n3 = 13.0f;
            }
            int height = canvas.getHeight();
            int width = canvas.getWidth();
            final float n4 = height / (float)width;
            final float n5 = (float)bottomCornerHeightPercent;
            final float n6 = (float)n;
            if (n4 > n5 / n6) {
                height = bottomCornerHeightPercent * width / n;
            }
            else {
                width = n * height / bottomCornerHeightPercent;
            }
            final Paint paint = new Paint();
            paint.setColor(Color.argb(255, 0, 0, 255));
            paint.setStrokeWidth(2.0f);
            paint.setStyle(Paint$Style.STROKE);
            final int n7 = (canvas.getWidth() - width) / 2;
            final int n8 = (canvas.getHeight() - height) / 2;
            final float n9 = (float)n7;
            final float n10 = (float)n8;
            canvas.drawRect(n9, n10, (float)(canvas.getWidth() - n7), (float)(canvas.getHeight() - n8), paint);
            float n11;
            if (height > width) {
                n11 = (float)width;
            }
            else {
                n11 = (float)height;
            }
            final int n12 = (int)(25 * n11 / 100.0f);
            final float n13 = (float)(height - n12);
            final float n14 = (float)(width - n12);
            float n15;
            float n16;
            if (n13 / n14 > n2 / n3) {
                n15 = n2 * n14 / n3;
                n16 = n14;
            }
            else {
                n16 = n3 * n13 / n2;
                n15 = n13;
            }
            final float n17 = (canvas.getWidth() - n16) / 2.0f;
            final float n18 = (canvas.getHeight() - n15) / 2.0f;
            final float n19 = n11 * 18 / 100.0f;
            final ScanPaperActivity a5 = this.a;
            final int n20 = (int)n19;
            a5.V(canvas, n17, n18, n20);
            this.a.n = new z71(n7, n8, width, height, (int)n17, (int)n18, n20);
            final ScanPaperActivity a6 = this.a;
            SheetImageConfigure sheetImageConfigure;
            if (a6.i) {
                a6.f = new SheetImageConfigure();
                sheetImageConfigure = this.a.f;
                sheetImageConfigure.imageHeight = n;
                sheetImageConfigure.imageWidth = bottomCornerHeightPercent;
                final float n21 = n19 / 2.0f;
                final int topCornerWidth = (int)(n5 * (n18 - n10 - n21) / height);
                sheetImageConfigure.topCornerWidth = topCornerWidth;
                final float n22 = (float)width;
                final int topCornerHeight = (int)(n6 * (n17 - n9 - n21) / n22);
                sheetImageConfigure.topCornerHeight = topCornerHeight;
                final int bottomCornerWidth = bottomCornerHeightPercent - topCornerWidth;
                sheetImageConfigure.bottomCornerWidth = bottomCornerWidth;
                final int bottomCornerHeight = n - topCornerHeight;
                sheetImageConfigure.bottomCornerHeight = bottomCornerHeight;
                final int squarelength = (int)(n19 * n6 / n22);
                sheetImageConfigure.squarelength = squarelength;
                sheetImageConfigure.squarelengthPercent = squarelength * 100 / bottomCornerHeightPercent;
                sheetImageConfigure.topCornerWidthPercent = topCornerWidth * 100 / bottomCornerHeightPercent;
                sheetImageConfigure.bottomCornerWidthPercent = bottomCornerWidth * 100 / bottomCornerHeightPercent;
                sheetImageConfigure.topCornerHeightPercent = topCornerHeight * 100 / n;
                bottomCornerHeightPercent = bottomCornerHeight * 100 / n;
            }
            else {
                a6.f = new SheetImageConfigure();
                sheetImageConfigure = this.a.f;
                sheetImageConfigure.imageHeight = bottomCornerHeightPercent;
                sheetImageConfigure.imageWidth = n;
                final float n23 = n19 / 2.0f;
                final float n24 = (float)width;
                final int topCornerWidth2 = (int)((n17 - n9 - n23) * n6 / n24);
                sheetImageConfigure.topCornerWidth = topCornerWidth2;
                final int topCornerHeight2 = (int)(n5 * (n18 - n10 - n23) / height);
                sheetImageConfigure.topCornerHeight = topCornerHeight2;
                final int bottomCornerWidth2 = n - topCornerWidth2;
                sheetImageConfigure.bottomCornerWidth = bottomCornerWidth2;
                final int bottomCornerHeight2 = bottomCornerHeightPercent - topCornerHeight2;
                sheetImageConfigure.bottomCornerHeight = bottomCornerHeight2;
                final int squarelength2 = (int)(n19 * n6 / n24);
                sheetImageConfigure.squarelength = squarelength2;
                sheetImageConfigure.squarelengthPercent = squarelength2 * 100 / n;
                sheetImageConfigure.topCornerWidthPercent = topCornerWidth2 * 100 / n;
                sheetImageConfigure.bottomCornerWidthPercent = bottomCornerWidth2 * 100 / n;
                sheetImageConfigure.topCornerHeightPercent = topCornerHeight2 * 100 / bottomCornerHeightPercent;
                bottomCornerHeightPercent = bottomCornerHeight2 * 100 / bottomCornerHeightPercent;
            }
            sheetImageConfigure.bottomCornerHeightPercent = bottomCornerHeightPercent;
            this.d(this.a.f);
        }
        
        public void c(final Void void1) {
            if (!((Context)this.a).getApplicationContext().getPackageName().equals(a91.e())) {
                this.a.finish();
            }
            final ScanPaperActivity a = this.a;
            if (a.f == null) {
                a.new h().execute((Object[])new Void[0]);
            }
            else {
                a.e0();
            }
        }
        
        public final void d(final SheetImageConfigure sheetImageConfigure) {
            final int topCornerHeight = sheetImageConfigure.topCornerHeight;
            final int n = 1;
            int n2;
            if (topCornerHeight < 2) {
                final int topCornerHeight2 = sheetImageConfigure.squarelength / 10;
                sheetImageConfigure.topCornerHeight = topCornerHeight2;
                sheetImageConfigure.bottomCornerHeight = sheetImageConfigure.imageHeight - topCornerHeight2;
                n2 = 1;
            }
            else {
                n2 = 0;
            }
            if (sheetImageConfigure.topCornerWidth < 2) {
                final int topCornerWidth = sheetImageConfigure.squarelength / 10;
                sheetImageConfigure.topCornerWidth = topCornerWidth;
                sheetImageConfigure.bottomCornerWidth = sheetImageConfigure.imageWidth - topCornerWidth;
                n2 = n;
            }
            if (n2 != 0) {
                FirebaseAnalytics.getInstance((Context)this.a.c).a("InvalidConfig", null);
            }
        }
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.ekodroid.omrevaluator.templateui;

import androidx.appcompat.app.a;
import android.os.Bundle;
import android.content.DialogInterface;
import android.content.DialogInterface$OnClickListener;
import com.google.android.material.dialog.MaterialAlertDialogBuilder;
import android.widget.AdapterView;
import android.widget.AdapterView$OnItemClickListener;
import android.widget.ListAdapter;
import android.view.View;
import android.view.View$OnClickListener;
import android.content.Context;
import com.ekodroid.omrevaluator.database.repositories.TemplateRepository;
import com.ekodroid.omrevaluator.templateui.models.Section2;
import com.ekodroid.omrevaluator.templateui.models.AnswerOption;
import com.ekodroid.omrevaluator.templateui.models.LabelProfile;
import com.ekodroid.omrevaluator.templateui.models.Subject2;
import com.ekodroid.omrevaluator.templateui.models.QueEditDataModel;
import com.ekodroid.omrevaluator.templateui.models.ExamId;
import com.ekodroid.omrevaluator.database.TemplateDataJsonModel;
import android.widget.ListView;
import java.util.ArrayList;
import com.ekodroid.omrevaluator.templateui.models.SheetTemplate2;

public class EditTemplateActivity extends v5
{
    public SheetTemplate2 c;
    public ArrayList d;
    public ListView e;
    public TemplateDataJsonModel f;
    public EditTemplateActivity g;
    public ExamId h;
    public boolean i;
    
    public EditTemplateActivity() {
        this.g = this;
        this.i = false;
    }
    
    public static /* synthetic */ EditTemplateActivity D(final EditTemplateActivity editTemplateActivity) {
        return editTemplateActivity.g;
    }
    
    public final ArrayList E(final ArrayList list, final Subject2[] array, final LabelProfile labelProfile) {
        final ArrayList list2 = new ArrayList();
        int n = -1;
        int i = 1;
        int n2 = -1;
        while (i < list.size()) {
            final AnswerOption answerOption = list.get(i);
            final int subjectId = answerOption.subjectId;
            int sectionId;
            if (n != subjectId || (sectionId = n2) != answerOption.sectionId) {
                sectionId = answerOption.sectionId;
                final Section2 section2 = array[subjectId].getSections()[sectionId];
                String str = section2.getName();
                if (section2.isOptionalAllowed()) {
                    final StringBuilder sb = new StringBuilder();
                    sb.append(str);
                    sb.append(" (");
                    sb.append(section2.getNoOfOptionalQue());
                    sb.append("/");
                    sb.append(section2.getNoOfQue());
                    sb.append(")");
                    str = sb.toString();
                }
                list2.add(new QueEditDataModel(QueEditDataModel.ViewType.LABEL, array[subjectId].getSubName(), str, 0, subjectId, sectionId, 0.0, 0.0, false, ""));
                n = subjectId;
            }
            final QueEditDataModel.ViewType question = QueEditDataModel.ViewType.QUESTION;
            final String subName = array[n].getSubName();
            final String name = array[n].getSections()[sectionId].getName();
            final int questionNumber = answerOption.questionNumber;
            list2.add(new QueEditDataModel(question, subName, name, questionNumber, answerOption.subjectId, answerOption.sectionId, answerOption.correctMarks, answerOption.incorrectMarks, answerOption.partialAllowed, labelProfile.getQuestionNumberLabel(questionNumber)));
            ++i;
            n2 = sectionId;
        }
        return list2;
    }
    
    public final void F() {
        this.f.setSheetTemplate(this.c);
        TemplateRepository.getInstance((Context)this.g).saveOrUpdateTemplateJson(this.f);
        a91.G((Context)this.g, 2131886286, 2131230927, 2131231085);
        this.g.finish();
    }
    
    public final void G() {
        this.findViewById(2131296460).setOnClickListener((View$OnClickListener)new View$OnClickListener(this) {
            public final EditTemplateActivity a;
            
            public void onClick(final View view) {
                this.a.F();
            }
        });
        this.findViewById(2131296424).setOnClickListener((View$OnClickListener)new View$OnClickListener(this) {
            public final EditTemplateActivity a;
            
            public void onClick(final View view) {
                this.a.finish();
            }
        });
    }
    
    public final void H(final SheetTemplate2 sheetTemplate2, final boolean i) {
        if (i) {
            this.i = i;
        }
        if (sheetTemplate2 == null) {
            return;
        }
        this.d = this.E(sheetTemplate2.getAnswerOptions(), sheetTemplate2.getTemplateParams().getSubjects(), sheetTemplate2.getLabelProfile());
        this.e.setAdapter((ListAdapter)new m3((Context)this.g, this.d));
    }
    
    public final void I() {
        ((AdapterView)this.e).setOnItemClickListener((AdapterView$OnItemClickListener)new AdapterView$OnItemClickListener(this) {
            public final EditTemplateActivity a;
            
            public void onItemClick(final AdapterView adapterView, final View view, final int n, final long n2) {
                if (this.a.d.get(n).i() == QueEditDataModel.ViewType.QUESTION) {
                    final EditTemplateActivity a = this.a;
                    a.K((QueEditDataModel)a.d.get(n));
                }
            }
        });
    }
    
    public final void J() {
        final MaterialAlertDialogBuilder materialAlertDialogBuilder = new MaterialAlertDialogBuilder((Context)this.g, 2131951953);
        materialAlertDialogBuilder.setMessage(2131886547).setPositiveButton(2131886903, (DialogInterface$OnClickListener)new DialogInterface$OnClickListener(this) {
            public final EditTemplateActivity a;
            
            public void onClick(final DialogInterface dialogInterface, final int n) {
                this.a.F();
                dialogInterface.dismiss();
            }
        }).setNegativeButton(2131886657, (DialogInterface$OnClickListener)new DialogInterface$OnClickListener(this) {
            public final EditTemplateActivity a;
            
            public void onClick(final DialogInterface dialogInterface, final int n) {
                dialogInterface.dismiss();
                EditTemplateActivity.D(this.a).finish();
            }
        }).create();
        ((a.a)materialAlertDialogBuilder).show();
    }
    
    public final void K(final QueEditDataModel queEditDataModel) {
        new zv((Context)this.g, (zv.d)new zv.d(this) {
            public final EditTemplateActivity a;
            
            @Override
            public void a(final QueEditDataModel queEditDataModel, final boolean b) {
                if (queEditDataModel.i() == QueEditDataModel.ViewType.LABEL) {
                    this.a.c.getTemplateParams().getSubjects()[queEditDataModel.g()].setSubName(queEditDataModel.h());
                    this.a.c.getTemplateParams().getSubjects()[queEditDataModel.g()].getSections()[queEditDataModel.e()].setName(queEditDataModel.f());
                }
                if (queEditDataModel.i() == QueEditDataModel.ViewType.QUESTION && b) {
                    final ArrayList<AnswerOption> answerOptions = this.a.c.getAnswerOptions();
                    for (int i = 0; i < answerOptions.size(); ++i) {
                        final AnswerOption answerOption = answerOptions.get(i);
                        if (answerOption.subjectId == queEditDataModel.g() && answerOption.sectionId == queEditDataModel.e()) {
                            answerOption.correctMarks = queEditDataModel.a();
                            answerOption.incorrectMarks = queEditDataModel.b();
                            answerOption.partialAllowed = queEditDataModel.j();
                        }
                    }
                }
                if (queEditDataModel.i() == QueEditDataModel.ViewType.QUESTION && !b) {
                    final AnswerOption answerOption2 = this.a.c.getAnswerOptions().get(queEditDataModel.d());
                    if (answerOption2.questionNumber == queEditDataModel.d()) {
                        answerOption2.correctMarks = queEditDataModel.a();
                        answerOption2.incorrectMarks = queEditDataModel.b();
                        answerOption2.partialAllowed = queEditDataModel.j();
                    }
                }
                final EditTemplateActivity a = this.a;
                a.H(a.c, true);
            }
        }, queEditDataModel).show();
    }
    
    @Override
    public void onBackPressed() {
        if (this.i) {
            this.J();
        }
        else {
            super.onBackPressed();
        }
    }
    
    @Override
    public void onCreate(final Bundle bundle) {
        super.onCreate(bundle);
        this.setContentView(2131492904);
        this.e = (ListView)this.findViewById(2131296843);
        final ExamId h = (ExamId)this.getIntent().getSerializableExtra("EXAM_ID");
        this.h = h;
        if (h != null) {
            final TemplateDataJsonModel templateJson = TemplateRepository.getInstance((Context)this.g).getTemplateJson(this.h);
            if ((this.f = templateJson) != null) {
                this.H(this.c = templateJson.getSheetTemplate(), false);
            }
        }
        this.I();
        this.G();
    }
}

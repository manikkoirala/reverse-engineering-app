// 
// Decompiled by Procyon v0.6.0
// 

package com.ekodroid.omrevaluator.templateui;

import android.app.Dialog;
import android.widget.CompoundButton;
import android.os.Environment;
import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.view.MenuItem;
import android.view.Menu;
import com.ekodroid.omrevaluator.database.TemplateDataJsonModel;
import com.ekodroid.omrevaluator.database.repositories.TemplateRepository;
import androidx.appcompat.widget.Toolbar;
import android.os.Bundle;
import android.net.Uri;
import android.os.Parcelable;
import android.content.Intent;
import androidx.core.content.FileProvider;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import android.view.View$OnClickListener;
import android.graphics.Paint;
import android.graphics.Canvas;
import android.graphics.Bitmap$Config;
import java.io.IOException;
import java.io.InputStream;
import org.apache.pdfbox.pdmodel.graphics.image.JPEGFactory;
import java.io.ByteArrayInputStream;
import java.io.OutputStream;
import android.graphics.Bitmap$CompressFormat;
import java.io.ByteArrayOutputStream;
import org.apache.pdfbox.pdmodel.common.PDRectangle;
import org.apache.pdfbox.pdmodel.PDDocument;
import java.io.File;
import android.view.View;
import android.content.DialogInterface;
import android.content.DialogInterface$OnClickListener;
import android.widget.RadioButton;
import android.view.ViewGroup;
import android.view.LayoutInflater;
import android.content.Context;
import com.google.android.material.dialog.MaterialAlertDialogBuilder;
import android.graphics.BitmapFactory;
import android.util.Log;
import org.apache.pdfbox.pdmodel.PDPageContentStream;
import org.apache.pdfbox.pdmodel.PDPage;
import org.apache.pdfbox.pdmodel.graphics.image.PDImageXObject;
import android.graphics.Matrix;
import com.ekodroid.omrevaluator.templateui.models.ExamId;
import android.graphics.Bitmap;
import android.widget.TextView;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import com.ekodroid.omrevaluator.templateui.models.SheetTemplate2;
import com.ekodroid.omrevaluator.templateui.models.a;
import com.ekodroid.omrevaluator.templateui.components.ZoomableImageView;

public class ViewTemplateActivity extends v5
{
    public ViewTemplateActivity c;
    public ZoomableImageView d;
    public a e;
    public SheetTemplate2 f;
    public int g;
    public LinearLayout h;
    public ImageButton i;
    public ImageButton j;
    public TextView k;
    public int l;
    public Bitmap m;
    public ExamId n;
    
    public ViewTemplateActivity() {
        this.c = this;
        this.e = new a();
        this.g = 0;
        this.l = 1;
        this.m = null;
    }
    
    public static Bitmap P(final Bitmap bitmap, final float n) {
        final Matrix matrix = new Matrix();
        matrix.postRotate(n);
        return Bitmap.createBitmap(bitmap, 0, 0, bitmap.getWidth(), bitmap.getHeight(), matrix, true);
    }
    
    public final void D(final PDImageXObject pdImageXObject, final PDPage pdPage, final PDPageContentStream pdPageContentStream) {
        final float n = pdPage.getBBox().getWidth() - 60.0f;
        final float n2 = pdPage.getBBox().getHeight() - 60.0f;
        final int width = pdImageXObject.getWidth();
        int width2 = 1000;
        if (width > 1000) {
            width2 = pdImageXObject.getWidth();
        }
        final double n3 = n / n2;
        try {
            float n4;
            if (n3 > pdImageXObject.getWidth() * 1.0 / pdImageXObject.getHeight()) {
                n4 = n2 / pdImageXObject.getHeight();
            }
            else {
                n4 = n / pdImageXObject.getWidth();
            }
            final float n5 = n4 * pdImageXObject.getWidth() / width2;
            pdPageContentStream.drawImage(pdImageXObject, (pdPage.getBBox().getWidth() - pdImageXObject.getWidth() * n5) / 2.0f, (pdPage.getBBox().getHeight() - pdImageXObject.getHeight() * n5) / 2.0f, pdImageXObject.getWidth() * n5, pdImageXObject.getHeight() * n5);
            Log.d("TAG", "image converted to pdf");
        }
        catch (final Exception ex) {
            ex.printStackTrace();
        }
    }
    
    public final void E() {
        this.d.setImageBitmap(BitmapFactory.decodeResource(this.getResources(), 2131230899));
    }
    
    public final void F() {
        final MaterialAlertDialogBuilder materialAlertDialogBuilder = new MaterialAlertDialogBuilder((Context)this.c, 2131951953);
        final View inflate = ((LayoutInflater)((Context)this).getSystemService("layout_inflater")).inflate(2131492998, (ViewGroup)null);
        materialAlertDialogBuilder.setView(inflate);
        materialAlertDialogBuilder.setTitle(2131886217);
        materialAlertDialogBuilder.setCancelable(true);
        final RadioButton radioButton = (RadioButton)inflate.findViewById(2131296990);
        final RadioButton radioButton2 = (RadioButton)inflate.findViewById(2131297000);
        materialAlertDialogBuilder.setPositiveButton(2131886804, (DialogInterface$OnClickListener)new DialogInterface$OnClickListener(this, radioButton) {
            public final RadioButton a;
            public final ViewTemplateActivity b;
            
            public void onClick(final DialogInterface dialogInterface, int n) {
                ViewTemplateActivity viewTemplateActivity;
                if (((CompoundButton)this.a).isChecked()) {
                    viewTemplateActivity = this.b;
                    n = 1;
                }
                else {
                    viewTemplateActivity = this.b;
                    n = 2;
                }
                viewTemplateActivity.M(n);
                dialogInterface.dismiss();
            }
        });
        materialAlertDialogBuilder.create().show();
    }
    
    public final boolean G(final int n, final String pathname) {
        final File file = new File(pathname);
        if (file.exists()) {
            file.delete();
        }
        try {
            final PDDocument pdDocument = new PDDocument();
            for (int length = this.f.getPageLayouts().length, i = 0; i < length; i += n) {
                final PDPage pdPage = new PDPage(PDRectangle.A4);
                pdDocument.addPage(pdPage);
                final ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
                this.H(n, i, this.f).compress(Bitmap$CompressFormat.JPEG, 100, (OutputStream)byteArrayOutputStream);
                final PDImageXObject fromStream = JPEGFactory.createFromStream(pdDocument, (InputStream)new ByteArrayInputStream(byteArrayOutputStream.toByteArray()));
                final PDPageContentStream pdPageContentStream = new PDPageContentStream(pdDocument, pdPage);
                this.D(fromStream, pdPage, pdPageContentStream);
                pdPageContentStream.close();
            }
            pdDocument.save(pathname);
            pdDocument.close();
            return true;
        }
        catch (final IOException ex) {
            ex.printStackTrace();
            return false;
        }
    }
    
    public final Bitmap H(int width, int width2, final SheetTemplate2 sheetTemplate2) {
        if (width == 1) {
            return this.e.X((Context)this.c, sheetTemplate2, width2);
        }
        if (width == 2 && sheetTemplate2.getPageLayouts().length == 1) {
            Bitmap bitmap = this.e.X((Context)this.c, sheetTemplate2, 0);
            if (bitmap.getWidth() <= bitmap.getHeight() * 1.2) {
                bitmap = P(bitmap, 90.0f);
            }
            final Bitmap bitmap2 = Bitmap.createBitmap(bitmap.getWidth(), bitmap.getHeight() * 2 + 150, Bitmap$Config.ARGB_8888);
            final Canvas canvas = new Canvas(bitmap2);
            canvas.drawARGB(255, 255, 255, 255);
            canvas.drawBitmap(bitmap, 0.0f, 0.0f, (Paint)null);
            canvas.drawBitmap(bitmap, 0.0f, (float)(bitmap.getHeight() + 149), (Paint)null);
            return bitmap2;
        }
        if (width == 2 && sheetTemplate2.getPageLayouts().length > 1) {
            Bitmap bitmap3 = this.e.X((Context)this.c, sheetTemplate2, width2);
            if (bitmap3.getWidth() <= bitmap3.getHeight() * 1.2) {
                bitmap3 = P(bitmap3, 90.0f);
            }
            width = width2 + 1;
            Bitmap bitmap4;
            if (width < sheetTemplate2.getPageLayouts().length) {
                bitmap4 = this.e.X((Context)this.c, sheetTemplate2, width);
                if (bitmap4.getWidth() <= bitmap4.getHeight() * 1.2) {
                    bitmap4 = P(bitmap4, 90.0f);
                }
            }
            else {
                bitmap4 = null;
            }
            width2 = bitmap3.getWidth();
            final int n = bitmap3.getHeight() * 2 + 150;
            width = width2;
            if (bitmap4 != null) {
                width = width2;
                if (bitmap4.getWidth() > bitmap3.getWidth()) {
                    width = bitmap4.getWidth();
                }
            }
            width2 = n;
            if (bitmap4 != null) {
                width2 = n;
                if (bitmap4.getHeight() > bitmap3.getHeight()) {
                    width2 = bitmap4.getHeight() * 2 + 150;
                }
            }
            final Bitmap bitmap5 = Bitmap.createBitmap(width, width2, Bitmap$Config.ARGB_8888);
            final Canvas canvas2 = new Canvas(bitmap5);
            canvas2.drawARGB(255, 255, 255, 255);
            canvas2.drawBitmap(bitmap3, 0.0f, 0.0f, (Paint)null);
            if (bitmap4 != null) {
                canvas2.drawBitmap(bitmap4, 0.0f, (float)(width2 / 2 - 75), (Paint)null);
            }
            return bitmap5;
        }
        return null;
    }
    
    public final void I() {
        this.finish();
    }
    
    public final void J() {
        ((View)this.j).setOnClickListener((View$OnClickListener)new View$OnClickListener(this) {
            public final ViewTemplateActivity a;
            
            public void onClick(final View view) {
                final SheetTemplate2 f = this.a.f;
                if (f != null) {
                    final int length = f.getPageLayouts().length;
                    final ViewTemplateActivity a = this.a;
                    final int g = a.g;
                    if (g < length - 1) {
                        a.g = g + 1;
                    }
                    a.O();
                }
            }
        });
    }
    
    public final void K() {
        ((View)this.i).setOnClickListener((View$OnClickListener)new View$OnClickListener(this) {
            public final ViewTemplateActivity a;
            
            public void onClick(final View view) {
                final ViewTemplateActivity a = this.a;
                if (a.f != null) {
                    final int g = a.g;
                    if (g > 0) {
                        a.g = g - 1;
                    }
                    a.O();
                }
            }
        });
    }
    
    public final void L() {
        this.j = (ImageButton)this.findViewById(2131296694);
        this.i = (ImageButton)this.findViewById(2131296696);
        this.k = (TextView)this.findViewById(2131297277);
        this.h = (LinearLayout)this.findViewById(2131296797);
        this.d = (ZoomableImageView)this.findViewById(2131297423);
    }
    
    public final void M(final int n) {
        new e().e(n).execute((Object[])new Void[0]);
    }
    
    public final void N() {
        final SheetTemplate2 f = this.f;
        if (f != null && f.getPageLayouts().length > 1) {
            final int length = this.f.getPageLayouts().length;
            ((View)this.h).setVisibility(0);
            final int g = this.g;
            Label_0100: {
                if (g == 0) {
                    ((View)this.i).setEnabled(false);
                }
                else {
                    if (g == length - 1) {
                        ((View)this.i).setEnabled(true);
                        ((View)this.j).setEnabled(false);
                        break Label_0100;
                    }
                    ((View)this.i).setEnabled(true);
                }
                ((View)this.j).setEnabled(true);
            }
            final TextView k = this.k;
            final StringBuilder sb = new StringBuilder();
            sb.append("");
            sb.append(this.g + 1);
            sb.append("/");
            sb.append(length);
            k.setText((CharSequence)sb.toString());
        }
        else {
            ((View)this.h).setVisibility(8);
        }
    }
    
    public final void O() {
        final SheetTemplate2 f = this.f;
        Bitmap bitmap;
        if (f == null) {
            bitmap = BitmapFactory.decodeResource(this.getResources(), 2131231089);
        }
        else {
            bitmap = this.e.X((Context)this.c, f, this.g);
        }
        this.m = bitmap;
        this.d.setImageBitmap(bitmap);
        this.N();
    }
    
    public final void Q() {
        final File externalFilesDir = ((Context)this).getApplicationContext().getExternalFilesDir(ok.c);
        if (!externalFilesDir.exists()) {
            externalFilesDir.mkdirs();
        }
        final StringBuilder sb = new StringBuilder();
        final StringBuilder sb2 = new StringBuilder();
        sb2.append(this.n.getExamName());
        sb2.append("_");
        sb2.append(this.n.getExamDate());
        sb.append(sb2.toString().replaceAll("[^a-zA-Z0-9_-]", "_"));
        sb.append(".jpg");
        final String string = sb.toString();
        final FileOutputStream fileOutputStream = null;
        final FileOutputStream fileOutputStream2 = null;
        FileOutputStream fileOutputStream4;
        final FileOutputStream fileOutputStream3 = fileOutputStream4 = null;
        FileOutputStream fileOutputStream5 = null;
        try {
            try {
                fileOutputStream4 = fileOutputStream3;
                final File file = new File(externalFilesDir, string);
                fileOutputStream4 = fileOutputStream3;
                fileOutputStream4 = fileOutputStream3;
                fileOutputStream5 = new FileOutputStream(file);
                try {
                    this.m.compress(Bitmap$CompressFormat.JPEG, 100, (OutputStream)fileOutputStream5);
                    fileOutputStream5.close();
                    this.R(file);
                    final FileOutputStream fileOutputStream6 = fileOutputStream5;
                    fileOutputStream6.close();
                }
                catch (final IOException ex) {}
                catch (final FileNotFoundException ex2) {}
                finally {
                    final IOException ex4;
                    final IOException ex3 = ex4;
                    fileOutputStream4 = fileOutputStream5;
                    final Object o = ex3;
                }
            }
            finally {}
        }
        catch (final IOException ex3) {
            final Object o = fileOutputStream;
        }
        catch (final FileNotFoundException ex3) {
            final Object o = fileOutputStream2;
        }
        try {
            final FileOutputStream fileOutputStream6 = fileOutputStream5;
            fileOutputStream6.close();
            return;
            final IOException ex3;
            ex3.printStackTrace();
            final Object o;
            ((FileOutputStream)o).close();
            return;
            ex3.printStackTrace();
            ((FileOutputStream)o).close();
        }
        catch (final IOException ex5) {
            ex5.printStackTrace();
        }
        return;
        try {
            fileOutputStream4.close();
        }
        catch (final IOException ex6) {
            ex6.printStackTrace();
        }
    }
    
    public final void R(final File file) {
        final StringBuilder sb = new StringBuilder();
        final StringBuilder sb2 = new StringBuilder();
        sb2.append(this.n.getExamName());
        sb2.append("_");
        sb2.append(this.n.getExamDate());
        sb.append(sb2.toString().replaceAll("[^a-zA-Z0-9_-]", "_"));
        sb.append(".jpg");
        final String string = sb.toString();
        final y01 y01 = new y01(this, file) {
            public final File a;
            public final ViewTemplateActivity b;
            
            @Override
            public void a(final Object o) {
                if (this.a.exists()) {
                    final Uri f = FileProvider.f((Context)this.b.c, "com.ekodroid.omrevaluator.fileprovider", this.a);
                    final Intent intent = new Intent("android.intent.action.SEND");
                    intent.setFlags(268435456);
                    intent.putExtra("android.intent.extra.STREAM", (Parcelable)f);
                    intent.setDataAndType(f, "application/octet-stream");
                    intent.setType("application/octet-stream");
                    intent.setFlags(1073741824);
                    intent.setFlags(1);
                    ((Context)this.b).startActivity(Intent.createChooser(intent, (CharSequence)"Share with"));
                }
            }
        };
        final ViewTemplateActivity c = this.c;
        final StringBuilder sb3 = new StringBuilder();
        sb3.append(this.getResources().getString(2131886311));
        sb3.append(" : ");
        sb3.append(ok.c);
        sb3.append("/");
        sb3.append(string);
        xs.d((Context)c, y01, 2131886312, sb3.toString(), 2131886824, 2131886176, 0, 0);
    }
    
    @Override
    public void onBackPressed() {
        super.onBackPressed();
        this.I();
    }
    
    @Override
    public void onCreate(final Bundle bundle) {
        super.onCreate(bundle);
        this.setContentView(2131492936);
        this.n = (ExamId)this.getIntent().getSerializableExtra("EXAM_ID");
        final String stringExtra = this.getIntent().getStringExtra("EXAM_TYPE");
        final Toolbar toolbar = (Toolbar)this.findViewById(2131297334);
        this.x(toolbar);
        this.L();
        this.E();
        this.J();
        this.K();
        final ExamId n = this.n;
        Label_0138: {
            if (n != null) {
                toolbar.setSubtitle(n.getExamName());
                SheetTemplate2 f;
                if ("ARCHIVED".equals(stringExtra)) {
                    f = e8.a;
                }
                else {
                    final TemplateDataJsonModel templateJson = TemplateRepository.getInstance((Context)this.c).getTemplateJson(this.n);
                    if (templateJson == null) {
                        break Label_0138;
                    }
                    f = templateJson.getSheetTemplate();
                }
                this.f = f;
            }
        }
        if (this.f == null) {
            this.finish();
        }
    }
    
    public boolean onCreateOptionsMenu(final Menu menu) {
        this.getMenuInflater().inflate(2131623948, menu);
        return true;
    }
    
    public boolean onOptionsItemSelected(final MenuItem menuItem) {
        final int itemId = menuItem.getItemId();
        if (itemId != 2131296318) {
            if (itemId != 2131296323) {
                if (itemId == 2131296325) {
                    xs.c((Context)this.c, null, 2131886253, 2131886484, 2131886176, 0, 0, 0);
                }
            }
            else {
                this.Q();
            }
        }
        else {
            this.F();
        }
        return false;
    }
    
    @Override
    public void onResume() {
        super.onResume();
        this.O();
    }
    
    @Override
    public boolean v() {
        this.I();
        return true;
    }
    
    public class e extends AsyncTask
    {
        public boolean a;
        public String b;
        public String c;
        public ProgressDialog d;
        public int e;
        public final ViewTemplateActivity f;
        
        public e(final ViewTemplateActivity f) {
            this.f = f;
            final StringBuilder sb = new StringBuilder();
            sb.append(f.f.getName());
            sb.append(System.currentTimeMillis() % 100L);
            this.b = sb.toString().replaceAll("[^a-zA-Z0-9_-]", "_");
            final StringBuilder sb2 = new StringBuilder();
            sb2.append(((Context)f.c).getExternalFilesDir(Environment.DIRECTORY_DOCUMENTS));
            sb2.append("/");
            sb2.append(this.b);
            sb2.append(".pdf");
            this.c = sb2.toString();
        }
        
        public Void b(final Void... array) {
            this.a = this.f.G(this.e, this.c);
            return null;
        }
        
        public void c(final Void void1) {
            if (((Dialog)this.d).isShowing()) {
                ((Dialog)this.d).dismiss();
            }
            if (this.a) {
                final Intent intent = new Intent("android.intent.action.VIEW");
                final Uri f = FileProvider.f((Context)this.f.c, "com.ekodroid.omrevaluator.fileprovider", new File(this.c));
                intent.putExtra("android.intent.extra.STREAM", (Parcelable)f);
                intent.setDataAndType(f, "application/pdf");
                intent.setFlags(1073741824);
                intent.setFlags(1);
                if (((Context)this.f).getPackageManager().queryIntentActivities(intent, 0).size() > 0) {
                    ((Context)this.f).startActivity(intent);
                }
                else {
                    a91.G((Context)this.f.c, 2131886699, 2131230909, 2131231086);
                }
            }
        }
        
        public void d(final Integer... array) {
        }
        
        public final e e(final int e) {
            this.e = e;
            return this;
        }
        
        public void onPreExecute() {
            (this.d = new ProgressDialog((Context)this.f.c)).setMessage((CharSequence)((Context)this.f).getString(2131886468));
            ((Dialog)this.d).setCancelable(false);
            ((Dialog)this.d).show();
        }
    }
}

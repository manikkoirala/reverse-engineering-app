// 
// Decompiled by Procyon v0.6.0
// 

package com.ekodroid.omrevaluator.templateui.createtemplate;

import android.view.ViewGroup$MarginLayoutParams;
import com.ekodroid.omrevaluator.templateui.models.PageLayout;
import android.os.Bundle;
import android.view.ViewGroup$LayoutParams;
import android.widget.ImageView;
import android.widget.TableRow;
import android.widget.TableRow$LayoutParams;
import android.content.res.Resources;
import android.app.Dialog;
import android.graphics.Bitmap;
import com.ekodroid.omrevaluator.templateui.components.ZoomableImageView;
import com.ekodroid.omrevaluator.templateui.models.a;
import android.widget.EditText;
import android.content.DialogInterface;
import android.content.DialogInterface$OnClickListener;
import android.view.ViewGroup;
import android.view.LayoutInflater;
import com.google.android.material.dialog.MaterialAlertDialogBuilder;
import android.widget.AdapterView;
import android.widget.AdapterView$OnItemSelectedListener;
import android.widget.SpinnerAdapter;
import java.util.List;
import android.widget.ArrayAdapter;
import com.ekodroid.omrevaluator.templateui.models.MatrixOptionPayload;
import com.ekodroid.omrevaluator.templateui.models.Section2;
import com.ekodroid.omrevaluator.templateui.models.Subject2;
import java.util.HashSet;
import java.io.Serializable;
import android.content.Intent;
import android.content.Context;
import java.util.Iterator;
import com.ekodroid.omrevaluator.templateui.models.SheetLabel;
import com.ekodroid.omrevaluator.templateui.models.SheetTemplate2;
import android.view.View;
import com.ekodroid.omrevaluator.templateui.models.AnswerOption;
import com.ekodroid.omrevaluator.templateui.models.TemplateParams2;
import android.widget.Spinner;
import android.widget.LinearLayout;
import android.widget.ImageButton;
import android.widget.Button;
import android.widget.TableLayout;
import com.ekodroid.omrevaluator.templateui.models.LabelProfile;
import android.view.View$OnClickListener;
import java.util.ArrayList;

public class CustomTemplateActivity extends v5
{
    public ArrayList A;
    public rb0[][] C;
    public sb0[][] D;
    public MODE F;
    public View$OnClickListener G;
    public LabelProfile c;
    public TableLayout d;
    public Button e;
    public ImageButton f;
    public LinearLayout g;
    public LinearLayout h;
    public LinearLayout i;
    public LinearLayout j;
    public LinearLayout k;
    public Spinner l;
    public Spinner m;
    public int n;
    public int p;
    public int q;
    public TemplateParams2 t;
    public CustomTemplateActivity v;
    public AnswerOption w;
    public ArrayList x;
    public ArrayList y;
    public ArrayList z;
    
    public CustomTemplateActivity() {
        this.q = 0;
        this.v = this;
        this.F = MODE.OTHER;
        this.G = (View$OnClickListener)new View$OnClickListener(this) {
            public final CustomTemplateActivity a;
            
            public void onClick(final View view) {
                final sb0 sb0 = (sb0)view;
                final int row = sb0.getRow();
                final int column = sb0.getColumn();
                if (CustomTemplateActivity.A(this.a) == MODE.ERASE) {
                    this.a.o0(row, column);
                    this.a.N0();
                    return;
                }
                if (CustomTemplateActivity.A(this.a) == MODE.ADD) {
                    if (((AdapterView)this.a.l).getSelectedItemPosition() == 0 && CustomTemplateActivity.V(this.a).size() > 0) {
                        final AnswerOption answerOption = CustomTemplateActivity.V(this.a).get(((AdapterView)this.a.m).getSelectedItemPosition());
                        if (this.a.I0(answerOption, row, column)) {
                            this.a.g0(answerOption, row, column);
                        }
                        this.a.N0();
                    }
                    if (((AdapterView)this.a.l).getSelectedItemPosition() == 1 && CustomTemplateActivity.Y(this.a).size() > 0) {
                        final AnswerOption.AnswerOptionType answerOptionType = CustomTemplateActivity.Y(this.a).get(((AdapterView)this.a.m).getSelectedItemPosition());
                        if (this.a.K0(row, column)) {
                            this.a.h0(answerOptionType, row, column);
                        }
                    }
                    if (((AdapterView)this.a.l).getSelectedItemPosition() == 2 && this.a.K0(row, column)) {
                        this.a.Y0(row, column);
                    }
                }
            }
        };
    }
    
    public static /* synthetic */ MODE A(final CustomTemplateActivity customTemplateActivity) {
        return customTemplateActivity.F;
    }
    
    public static /* synthetic */ MODE B(final CustomTemplateActivity customTemplateActivity, final MODE f) {
        return customTemplateActivity.F = f;
    }
    
    public static /* synthetic */ CustomTemplateActivity E(final CustomTemplateActivity customTemplateActivity) {
        return customTemplateActivity.v;
    }
    
    public static /* synthetic */ ArrayList F(final CustomTemplateActivity customTemplateActivity) {
        return customTemplateActivity.y;
    }
    
    public static /* synthetic */ rb0[][] G(final CustomTemplateActivity customTemplateActivity) {
        return customTemplateActivity.C;
    }
    
    public static /* synthetic */ TemplateParams2 M(final CustomTemplateActivity customTemplateActivity) {
        return customTemplateActivity.t;
    }
    
    public static /* synthetic */ sb0[][] S(final CustomTemplateActivity customTemplateActivity) {
        return customTemplateActivity.D;
    }
    
    public static /* synthetic */ ArrayList V(final CustomTemplateActivity customTemplateActivity) {
        return customTemplateActivity.z;
    }
    
    public static /* synthetic */ ArrayList Y(final CustomTemplateActivity customTemplateActivity) {
        return customTemplateActivity.A;
    }
    
    public final SheetLabel.LableType A0(final AnswerOption.AnswerOptionType answerOptionType) {
        switch (CustomTemplateActivity$h.a[answerOptionType.ordinal()]) {
            default: {
                return SheetLabel.LableType.OPTION_ABCDE;
            }
            case 6: {
                return SheetLabel.LableType.OPTION_ABCDEFGH;
            }
            case 5: {
                return SheetLabel.LableType.OPTION_ABCDEF;
            }
            case 4: {
                return SheetLabel.LableType.OPTION_TRUEORFALSE;
            }
            case 3: {
                return SheetLabel.LableType.OPTION_ABC;
            }
            case 2: {
                return SheetLabel.LableType.OPTION_ABCD;
            }
            case 1: {
                return SheetLabel.LableType.OPTION_ABCDE;
            }
        }
    }
    
    public final String B0(final AnswerOption.AnswerOptionType answerOptionType) {
        switch (CustomTemplateActivity$h.a[answerOptionType.ordinal()]) {
            default: {
                return "";
            }
            case 6: {
                return this.x0(this.c.getEightOptionLabels());
            }
            case 5: {
                return this.x0(this.c.getSixOptionLabels());
            }
            case 4: {
                return this.x0(this.c.getTrueOrFalseLabel());
            }
            case 3: {
                return this.x0(this.c.getThreeOptionLabels());
            }
            case 2: {
                return this.x0(this.c.getFourOptionLabels());
            }
            case 1: {
                return this.x0(this.c.getFiveOptionLabels());
            }
        }
    }
    
    public final ArrayList C0(final ArrayList list) {
        final ArrayList list2 = new ArrayList();
        final Iterator iterator = list.iterator();
        while (iterator.hasNext()) {
            int n = 0;
            switch (CustomTemplateActivity$h.a[((AnswerOption.AnswerOptionType)iterator.next()).ordinal()]) {
                default: {
                    continue;
                }
                case 6: {
                    n = 2131886255;
                    break;
                }
                case 5: {
                    n = 2131886837;
                    break;
                }
                case 4: {
                    n = 2131886880;
                    break;
                }
                case 3: {
                    n = 2131886870;
                    break;
                }
                case 2: {
                    n = 2131886321;
                    break;
                }
                case 1: {
                    n = 2131886319;
                    break;
                }
            }
            list2.add(((Context)this).getString(n));
        }
        return list2;
    }
    
    public final ArrayList D0(final ArrayList list) {
        final ArrayList list2 = new ArrayList();
        for (final AnswerOption answerOption : list) {
            String e;
            if (answerOption == null) {
                e = ((Context)this).getString(2131886664);
            }
            else {
                final int n = CustomTemplateActivity$h.a[answerOption.type.ordinal()];
                if (n != 10) {
                    if (n != 11) {
                        final StringBuilder sb = new StringBuilder();
                        sb.append("Q ");
                        sb.append(answerOption.questionNumber);
                        e = sb.toString();
                    }
                    else {
                        e = this.c.getRollNoString();
                    }
                }
                else {
                    e = this.c.getExamSetString();
                }
            }
            list2.add(e);
        }
        return list2;
    }
    
    public final void E0() {
        if (!this.L0()) {
            a91.G((Context)this.v, 2131886708, 2131230909, 2131231086);
            return;
        }
        final SheetTemplate2 z0 = this.z0();
        final Intent intent = new Intent((Context)this.v, (Class)ViewCustomTemplateActivity.class);
        intent.putExtra("SHEET_TEMPLATE", (Serializable)z0);
        ((Context)this).startActivity(intent);
    }
    
    public final boolean F0(final AnswerOption.AnswerOptionType answerOptionType) {
        final int n = CustomTemplateActivity$h.a[answerOptionType.ordinal()];
        if (n != 12) {
            switch (n) {
                default: {
                    return false;
                }
                case 1:
                case 2:
                case 3:
                case 4:
                case 5:
                case 6: {
                    break;
                }
            }
        }
        return true;
    }
    
    public final void G0(final TemplateParams2 templateParams2) {
        this.y = new ArrayList();
        this.x = new ArrayList();
        final HashSet set = new HashSet();
        if (templateParams2.getExamSets() > 1) {
            this.w = new AnswerOption(AnswerOption.AnswerOptionType.EXAMSET_BLOCK, 0, -1, -1, templateParams2.getExamSets(), 0, 0, 0.0, 0.0, false, null, this.q);
        }
        this.x.add(new AnswerOption(AnswerOption.AnswerOptionType.ID_BLOCK, 0, -1, -1, templateParams2.getRollDigits(), 0, 0, 0.0, 0.0, false, null, this.q));
        final Subject2[] subjects = templateParams2.getSubjects();
        int i = 0;
        int n = 1;
        while (i < subjects.length) {
            final Section2[] sections = subjects[i].getSections();
            final int n2 = 0;
            final int n3 = i;
            for (int j = n2; j < sections.length; ++j) {
                final Section2 section2 = sections[j];
                if (this.F0(section2.getType())) {
                    set.add(section2.getType());
                }
                for (int k = 0; k < section2.getNoOfQue(); ++k) {
                    this.x.add(new AnswerOption(section2.getType(), n, -1, -1, -1, n3, j, section2.getPositiveMark(), section2.getNegetiveMark(), section2.isPartialAllowed(), section2.getPayload(), this.q));
                    ++n;
                }
            }
            i = n3 + 1;
        }
        this.A = new ArrayList();
        final Iterator iterator = set.iterator();
        while (iterator.hasNext()) {
            this.A.add(iterator.next());
        }
    }
    
    public final void H0() {
        this.f = (ImageButton)this.findViewById(2131296669);
        this.e = (Button)this.findViewById(2131296437);
        this.d = (TableLayout)this.findViewById(2131297145);
        this.l = (Spinner)this.findViewById(2131297101);
        this.m = (Spinner)this.findViewById(2131297100);
        this.g = (LinearLayout)this.findViewById(2131296728);
        this.h = (LinearLayout)this.findViewById(2131296749);
        this.j = (LinearLayout)this.findViewById(2131296790);
        this.i = (LinearLayout)this.findViewById(2131296816);
        this.k = (LinearLayout)this.findViewById(2131296795);
    }
    
    public final boolean I0(final AnswerOption answerOption, final int n, final int n2) {
        final int n3 = CustomTemplateActivity$h.a[answerOption.type.ordinal()];
        int n4 = 12;
        switch (n3) {
            default: {
                return false;
            }
            case 11: {
                return this.J0(n, n2, 12);
            }
            case 10: {
                final int subIndex = answerOption.subIndex;
                int n5 = 5;
                if (subIndex <= 5) {
                    n5 = 3;
                }
                return this.J0(n, n2, n5);
            }
            case 9: {
                if (!bc.b(answerOption.payload).decimalAllowed) {
                    n4 = 11;
                }
                return this.J0(n, n2, n4);
            }
            case 8: {
                return this.J0(n, n2, 11);
            }
            case 7: {
                final MatrixOptionPayload c = bc.c(answerOption.payload);
                int primaryOptions;
                if (c != null) {
                    primaryOptions = c.primaryOptions;
                }
                else {
                    primaryOptions = 4;
                }
                return this.J0(n, n2, primaryOptions + 1);
            }
            case 6: {
                return this.K0(n, n2);
            }
            case 5: {
                return this.K0(n, n2);
            }
            case 4: {
                return this.K0(n, n2);
            }
            case 3: {
                return this.K0(n, n2);
            }
            case 2: {
                return this.K0(n, n2);
            }
            case 1: {
                return this.K0(n, n2);
            }
        }
    }
    
    public final boolean J0(final int n, final int n2, final int n3) {
        Label_0013: {
            if (this.C.length - n >= n3 - 1) {
                for (int i = 0; i < n3; ++i) {
                    if (this.C[n - 1 + i][n2 - 1] != null) {
                        break Label_0013;
                    }
                }
                return true;
            }
        }
        a91.G((Context)this.v, 2131886844, 2131230909, 2131231086);
        return false;
    }
    
    public final boolean K0(final int n, final int n2) {
        if (this.C[n - 1][n2 - 1] != null) {
            a91.G((Context)this.v, 2131886844, 2131230909, 2131231086);
            return false;
        }
        return true;
    }
    
    public final boolean L0() {
        return this.l0().size() <= 0;
    }
    
    public final void M0() {
        final sb0[][] d = this.D;
        final int length = d.length;
        final int length2 = d[0].length;
        for (int i = 1; i < length + 1; ++i) {
            for (int j = 1; j < length2 + 1; ++j) {
                this.b1(i, j);
            }
        }
    }
    
    public final void N0() {
        final int selectedItemPosition = ((AdapterView)this.l).getSelectedItemPosition();
        ArrayAdapter adapter;
        if (selectedItemPosition != 0) {
            if (selectedItemPosition != 1) {
                return;
            }
            adapter = new ArrayAdapter((Context)this, 2131493104, (List)this.C0(this.A));
        }
        else {
            this.l0();
            adapter = new ArrayAdapter((Context)this, 2131493104, (List)this.D0(this.z));
        }
        this.m.setAdapter((SpinnerAdapter)adapter);
    }
    
    public final void O0() {
        final MODE f = this.F;
        Object o;
        if (f == MODE.ADD) {
            ((View)this.g).setBackgroundResource(2131231083);
            ((View)this.g).setBackgroundColor(this.getResources().getColor(2131099726));
            ((View)this.h).setBackgroundColor(this.getResources().getColor(2131099740));
            ((View)this.l).setVisibility(0);
            ((View)this.m).setVisibility(0);
            o = this.e;
        }
        else {
            if (f == MODE.ERASE) {
                ((View)this.g).setBackgroundColor(this.getResources().getColor(2131099740));
                ((View)this.h).setBackgroundResource(2131231083);
                ((View)this.h).setBackgroundColor(this.getResources().getColor(2131099726));
                ((View)this.l).setVisibility(4);
                ((View)this.e).setVisibility(0);
            }
            else {
                if (f != MODE.OTHER) {
                    return;
                }
                ((View)this.g).setBackgroundColor(this.getResources().getColor(2131099740));
                ((View)this.h).setBackgroundColor(this.getResources().getColor(2131099740));
                ((View)this.l).setVisibility(4);
                ((View)this.e).setVisibility(4);
            }
            o = this.m;
        }
        ((View)o).setVisibility(4);
    }
    
    public final void P0() {
        ((View)this.e).setOnClickListener((View$OnClickListener)new View$OnClickListener(this) {
            public final CustomTemplateActivity a;
            
            public void onClick(final View view) {
                xs.c((Context)CustomTemplateActivity.E(this.a), new y01(this) {
                    public final CustomTemplateActivity$e a;
                    
                    @Override
                    public void a(final Object o) {
                        final int length = CustomTemplateActivity.S(this.a.a).length;
                        final int length2 = CustomTemplateActivity.S(this.a.a)[0].length;
                        for (int i = 1; i < length + 1; ++i) {
                            for (int j = 1; j < length2 + 1; ++j) {
                                this.a.a.o0(i, j);
                            }
                        }
                    }
                }, 2131886129, 2131886475, 2131886903, 2131886657, 0, 2131230930);
            }
        });
    }
    
    public final void Q0() {
        ((View)this.g).setOnClickListener((View$OnClickListener)new View$OnClickListener(this) {
            public final CustomTemplateActivity a;
            
            public void onClick(final View view) {
                CustomTemplateActivity.B(this.a, MODE.ADD);
                ((AdapterView)this.a.l).setSelection(0);
                this.a.N0();
                this.a.O0();
            }
        });
    }
    
    public final void R0() {
        ((View)this.h).setOnClickListener((View$OnClickListener)new View$OnClickListener(this) {
            public final CustomTemplateActivity a;
            
            public void onClick(final View view) {
                CustomTemplateActivity.B(this.a, MODE.ERASE);
                ((AdapterView)this.a.l).setSelection(0);
                this.a.N0();
                this.a.O0();
            }
        });
    }
    
    public final void S0() {
        ((View)this.j).setOnClickListener((View$OnClickListener)new View$OnClickListener(this) {
            public final CustomTemplateActivity a;
            
            public void onClick(final View view) {
                CustomTemplateActivity.B(this.a, MODE.OTHER);
                this.a.O0();
                xs.c((Context)CustomTemplateActivity.E(this.a), new y01(this) {
                    public final CustomTemplateActivity$p a;
                    
                    @Override
                    public void a(final Object o) {
                        final CustomTemplateActivity a = this.a.a;
                        a.Z0(CustomTemplateActivity.M(a));
                    }
                }, 2131886129, 2131886545, 2131886903, 2131886657, 0, 2131230930);
            }
        });
    }
    
    public final void T0() {
        ((View)this.k).setOnClickListener((View$OnClickListener)new View$OnClickListener(this) {
            public final CustomTemplateActivity a;
            
            public void onClick(final View view) {
                CustomTemplateActivity.B(this.a, MODE.OTHER);
                this.a.O0();
                this.a.E0();
            }
        });
    }
    
    public final void U0() {
        ((View)this.i).setOnClickListener((View$OnClickListener)new View$OnClickListener(this) {
            public final CustomTemplateActivity a;
            
            public void onClick(final View view) {
                CustomTemplateActivity.B(this.a, MODE.OTHER);
                this.a.O0();
                this.a.a1(this.a.z0());
            }
        });
    }
    
    public final void V0() {
        ((View)this.f).setOnClickListener((View$OnClickListener)new View$OnClickListener(this) {
            public final CustomTemplateActivity a;
            
            public void onClick(final View view) {
                xs.c((Context)CustomTemplateActivity.E(this.a), new y01(this) {
                    public final CustomTemplateActivity$f a;
                    
                    @Override
                    public void a(final Object o) {
                        this.a.a.onBackPressed();
                        CustomTemplateActivity.E(this.a.a).finish();
                    }
                }, 2131886129, 2131886505, 2131886903, 2131886657, 0, 2131230930);
            }
        });
    }
    
    public final void W0() {
        this.l.setAdapter((SpinnerAdapter)new ArrayAdapter((Context)this, 2131493104, (Object[])gr1.c));
        ((AdapterView)this.l).setOnItemSelectedListener((AdapterView$OnItemSelectedListener)new AdapterView$OnItemSelectedListener(this) {
            public final CustomTemplateActivity a;
            
            public void onItemSelected(final AdapterView adapterView, final View view, int selectedItemPosition, final long n) {
                if (CustomTemplateActivity.A(this.a) == MODE.ADD) {
                    selectedItemPosition = ((AdapterView)this.a.l).getSelectedItemPosition();
                    if (selectedItemPosition != 0 && selectedItemPosition != 1) {
                        if (selectedItemPosition == 2) {
                            ((View)this.a.m).setVisibility(4);
                        }
                    }
                    else {
                        ((View)this.a.m).setVisibility(0);
                        this.a.N0();
                    }
                }
            }
            
            public void onNothingSelected(final AdapterView adapterView) {
            }
        });
    }
    
    public final void X0(final AnswerOption o, final ArrayList list, final int n, final int n2) {
        final MaterialAlertDialogBuilder materialAlertDialogBuilder = new MaterialAlertDialogBuilder((Context)this.v, 2131951953);
        final View inflate = ((LayoutInflater)((Context)this).getSystemService("layout_inflater")).inflate(2131492959, (ViewGroup)null);
        materialAlertDialogBuilder.setView(inflate);
        materialAlertDialogBuilder.setTitle(2131886121);
        final Spinner[] array = { (Spinner)inflate.findViewById(2131297088), (Spinner)inflate.findViewById(2131297089) };
        for (int i = 0; i < 2; ++i) {
            array[i].setAdapter((SpinnerAdapter)new ArrayAdapter((Context)this, 2131493104, (List)this.D0(list)));
        }
        ((AdapterView)array[0]).setSelection(list.indexOf(o));
        materialAlertDialogBuilder.setPositiveButton(2131886114, (DialogInterface$OnClickListener)new DialogInterface$OnClickListener(this, array, list, n, n2) {
            public final Spinner[] a;
            public final ArrayList b;
            public final int c;
            public final int d;
            public final CustomTemplateActivity e;
            
            public void onClick(final DialogInterface dialogInterface, int n) {
                final AnswerOption[] array = new AnswerOption[2];
                n = 0;
                boolean b = false;
                while (true) {
                    final Spinner[] a = this.a;
                    if (n >= a.length) {
                        break;
                    }
                    if ((array[n] = (AnswerOption)this.b.get(((AdapterView)a[n]).getSelectedItemPosition())) != null) {
                        b = true;
                    }
                    ++n;
                }
                if (b) {
                    this.e.f0(array, this.c, this.d);
                    dialogInterface.dismiss();
                }
            }
        });
        materialAlertDialogBuilder.setNegativeButton(2131886163, (DialogInterface$OnClickListener)new DialogInterface$OnClickListener(this) {
            public final CustomTemplateActivity a;
            
            public void onClick(final DialogInterface dialogInterface, final int n) {
                dialogInterface.dismiss();
            }
        });
        materialAlertDialogBuilder.create().show();
    }
    
    public final void Y0(final int n, final int n2) {
        final MaterialAlertDialogBuilder materialAlertDialogBuilder = new MaterialAlertDialogBuilder((Context)this.v, 2131951953);
        final View inflate = ((LayoutInflater)((Context)this).getSystemService("layout_inflater")).inflate(2131492963, (ViewGroup)null);
        materialAlertDialogBuilder.setView(inflate);
        materialAlertDialogBuilder.setTitle(2131886127);
        materialAlertDialogBuilder.setPositiveButton(2131886114, (DialogInterface$OnClickListener)new DialogInterface$OnClickListener(this, (EditText)inflate.findViewById(2131296592), n, n2) {
            public final EditText a;
            public final int b;
            public final int c;
            public final CustomTemplateActivity d;
            
            public void onClick(final DialogInterface dialogInterface, final int n) {
                final String string = this.a.getText().toString();
                if (string.trim().equals("")) {
                    a91.G((Context)CustomTemplateActivity.E(this.d), 2131886274, 2131230909, 2131231086);
                    return;
                }
                final SheetLabel e = new SheetLabel(SheetLabel.LableType.TEXT, this.b, this.c, this.d.q, string);
                CustomTemplateActivity.F(this.d).add(e);
                CustomTemplateActivity.G(this.d)[this.b - 1][this.c - 1] = new rb0(string, e);
                this.d.b1(this.b, this.c);
                dialogInterface.dismiss();
            }
        });
        materialAlertDialogBuilder.setNegativeButton(2131886163, (DialogInterface$OnClickListener)new DialogInterface$OnClickListener(this) {
            public final CustomTemplateActivity a;
            
            public void onClick(final DialogInterface dialogInterface, final int n) {
                dialogInterface.dismiss();
            }
        });
        materialAlertDialogBuilder.create().show();
    }
    
    public final void Z0(final TemplateParams2 templateParams2) {
        new tb0((Context)this.v, new y01(this) {
            public final CustomTemplateActivity a;
            
            @Override
            public void a(final Object o) {
                final int[] array = (int[])o;
                final CustomTemplateActivity a = this.a;
                a.k0(a.n = array[0], a.p = array[1]);
                final CustomTemplateActivity a2 = this.a;
                a2.m0(a2.n, a2.p);
                this.a.M0();
            }
        }, templateParams2);
    }
    
    public final void a1(final SheetTemplate2 sheetTemplate2) {
        final u80 u80 = new u80((Context)this.v);
        u80.setContentView(2131493003);
        u80.setCancelable(false);
        final Bitmap x = new a().X((Context)this.v, sheetTemplate2, 0);
        final ZoomableImageView zoomableImageView = (ZoomableImageView)u80.findViewById(2131297421);
        zoomableImageView.setImageBitmap(x);
        u80.findViewById(2131296439).setOnClickListener((View$OnClickListener)new View$OnClickListener(this, zoomableImageView, x, u80) {
            public final ZoomableImageView a;
            public final Bitmap b;
            public final Dialog c;
            public final CustomTemplateActivity d;
            
            public void onClick(final View view) {
                this.a.setImageResource(17170445);
                this.b.recycle();
                this.c.dismiss();
            }
        });
        u80.create();
        u80.show();
    }
    
    public final void b1(int n, int n2) {
        final rb0[][] c = this.C;
        --n;
        final rb0[] array = c[n];
        --n2;
        final rb0 rb0 = array[n2];
        sb0 sb0;
        Resources resources;
        if (rb0 == null) {
            this.D[n][n2].setText((CharSequence)" + ");
            this.D[n][n2].setTextColor(this.getResources().getColor(2131099715));
            sb0 = this.D[n][n2];
            resources = this.getResources();
            n = 2131230855;
        }
        else {
            this.D[n][n2].setText((CharSequence)rb0.a);
            this.D[n][n2].setTextColor(this.getResources().getColor(2131099740));
            sb0 = this.D[n][n2];
            resources = this.getResources();
            n = 2131230856;
        }
        ((View)sb0).setBackground(resources.getDrawable(n));
    }
    
    public final void c0(final AnswerOption answerOption, final int row, final int column) {
        final StringBuilder sb = new StringBuilder();
        sb.append("NUM : ");
        sb.append(answerOption.questionNumber);
        final String string = sb.toString();
        answerOption.row = row;
        answerOption.column = column;
        int i = 0;
        answerOption.subIndex = 0;
        int n;
        if (bc.b(answerOption.payload).decimalAllowed) {
            n = 12;
        }
        else {
            n = 11;
        }
        while (i < n) {
            this.C[answerOption.row - 1 + i][answerOption.column - 1] = new rb0(string, answerOption);
            this.b1(row + i, column);
            ++i;
        }
    }
    
    public final void d0(final AnswerOption answerOption, final int n, final int column) {
        final SheetLabel e = new SheetLabel(SheetLabel.LableType.SET_LABEL, n, column, this.q, null);
        this.y.add(e);
        final rb0[][] c = this.C;
        final int n2 = n - 1;
        final rb0[] array = c[n2];
        final int n3 = column - 1;
        array[n3] = new rb0(((Context)this).getString(2131886293), e);
        this.b1(n, column);
        answerOption.row = n + 1;
        answerOption.column = column;
        int n4;
        if (answerOption.subIndex > 5) {
            n4 = 4;
        }
        else {
            n4 = 2;
        }
        for (int i = 0; i < n4; ++i) {
            this.C[n2 + i + 1][n3] = new rb0("E SET", answerOption);
            this.b1(n + i + 1, column);
        }
    }
    
    public final void e0(final AnswerOption answerOption, final int row, final int column) {
        final MatrixOptionPayload c = bc.c(answerOption.payload);
        int primaryOptions;
        if (c != null) {
            primaryOptions = c.primaryOptions;
        }
        else {
            primaryOptions = 4;
        }
        final StringBuilder sb = new StringBuilder();
        sb.append("QM : ");
        sb.append(answerOption.questionNumber);
        final String string = sb.toString();
        answerOption.row = row;
        answerOption.column = column;
        for (int i = 0; i < primaryOptions + 1; ++i) {
            this.C[answerOption.row - 1 + i][answerOption.column - 1] = new rb0(string, answerOption);
            this.b1(row + i, column);
        }
    }
    
    public final void f0(final AnswerOption[] array, final int row, final int column) {
        String str = "NQ:";
        AnswerOption answerOption = null;
        final int n = 0;
        String string;
        AnswerOption answerOption3;
        for (int i = 0; i < array.length; ++i, str = string, answerOption = answerOption3) {
            final AnswerOption answerOption2 = array[i];
            string = str;
            answerOption3 = answerOption;
            if (answerOption2 != null) {
                string = str;
                answerOption3 = answerOption;
                if (answerOption2.column == -1) {
                    string = str;
                    answerOption3 = answerOption;
                    if (answerOption2.row == -1) {
                        answerOption2.row = row;
                        answerOption2.column = column;
                        answerOption2.subIndex = i;
                        final StringBuilder sb = new StringBuilder();
                        sb.append(str);
                        sb.append(" ");
                        sb.append(answerOption2.questionNumber);
                        string = sb.toString();
                        answerOption3 = answerOption2;
                    }
                }
            }
        }
        if (answerOption != null) {
            for (int j = n; j < 11; ++j) {
                this.C[row - 1 + j][column - 1] = new rb0(str, answerOption);
                this.b1(row + j, column);
            }
        }
        this.N0();
    }
    
    public final void g0(final AnswerOption answerOption, final int n, final int n2) {
        switch (CustomTemplateActivity$h.a[answerOption.type.ordinal()]) {
            default: {
                return;
            }
            case 12: {
                this.j0(answerOption, n, n2);
                return;
            }
            case 11: {
                this.i0(answerOption, n, n2);
                return;
            }
            case 10: {
                this.d0(answerOption, n, n2);
                return;
            }
            case 9: {
                if (bc.b(answerOption.payload).digits > 2) {
                    this.c0(answerOption, n, n2);
                    return;
                }
                this.X0(answerOption, this.y0(this.z), n, n2);
                return;
            }
            case 7: {
                this.e0(answerOption, n, n2);
                return;
            }
            case 6: {
                this.j0(answerOption, n, n2);
                return;
            }
            case 5: {
                this.j0(answerOption, n, n2);
                return;
            }
            case 4: {
                this.j0(answerOption, n, n2);
                return;
            }
            case 3: {
                this.j0(answerOption, n, n2);
                return;
            }
            case 2: {
                this.j0(answerOption, n, n2);
                return;
            }
            case 1: {
                this.j0(answerOption, n, n2);
            }
        }
    }
    
    public final void h0(final AnswerOption.AnswerOptionType answerOptionType, final int n, final int n2) {
        final String b0 = this.B0(answerOptionType);
        final SheetLabel e = new SheetLabel(this.A0(answerOptionType), n, n2, this.q, b0);
        this.y.add(e);
        this.C[n - 1][n2 - 1] = new rb0(b0, e);
        this.b1(n, n2);
    }
    
    public final void i0(final AnswerOption answerOption, int i, final int column) {
        this.y.add(new SheetLabel(SheetLabel.LableType.ROLL_LABEL, i, column, this.q, null));
        this.b1(i, column);
        final String string = ((Context)this).getString(2131886750);
        answerOption.row = i;
        answerOption.column = column;
        for (i = 0; i < 12; ++i) {
            this.C[answerOption.row - 1 + i][answerOption.column - 1] = new rb0(string, answerOption);
            this.b1(answerOption.row + i, answerOption.column);
        }
    }
    
    public final void j0(final AnswerOption answerOption, final int row, final int column) {
        final StringBuilder sb = new StringBuilder();
        sb.append("Q : ");
        sb.append(answerOption.questionNumber);
        final String string = sb.toString();
        answerOption.row = row;
        answerOption.column = column;
        this.C[row - 1][column - 1] = new rb0(string, answerOption);
        this.b1(row, column);
    }
    
    public final void k0(final int n, int n2) {
        n2 = n2 * 5 + 1;
        this.C = new rb0[n2][n];
        this.D = new sb0[n2][n];
        this.G0(this.t);
    }
    
    public final ArrayList l0() {
        final ArrayList z = new ArrayList();
        this.z = z;
        final AnswerOption w = this.w;
        if (w != null && w.row < 0 && w.column < 0) {
            z.add(w);
        }
        for (final AnswerOption e : this.x) {
            if (e != null && e.row < 0 && e.column < 0) {
                this.z.add(e);
            }
        }
        return this.z;
    }
    
    public final void m0(final int n, final int n2) {
        ((ViewGroup)this.d).removeAllViews();
        final TableRow$LayoutParams tableRow$LayoutParams = new TableRow$LayoutParams(a91.d(30, (Context)this.v), a91.d(30, (Context)this.v));
        final TableRow$LayoutParams tableRow$LayoutParams2 = new TableRow$LayoutParams(a91.d(120, (Context)this.v), a91.d(30, (Context)this.v));
        final int d = a91.d(4, (Context)this.v);
        ((ViewGroup$MarginLayoutParams)tableRow$LayoutParams2).setMargins(d, d, d, d);
        ((ViewGroup$MarginLayoutParams)tableRow$LayoutParams).setMargins(d, d, d, d);
        for (int i = 0; i < n2 * 5 + 1; ++i) {
            final TableRow tableRow = new TableRow((Context)this.v);
            for (int j = 0; j < n + n + 1; ++j) {
                if (j % 2 == 0) {
                    final ImageView imageView = new ImageView((Context)this.v);
                    Resources resources;
                    int n3;
                    if (i % 5 == 0) {
                        resources = this.getResources();
                        n3 = 2131099702;
                    }
                    else {
                        resources = this.getResources();
                        n3 = 2131099739;
                    }
                    ((View)imageView).setBackgroundColor(resources.getColor(n3));
                    ((ViewGroup)tableRow).addView((View)imageView, (ViewGroup$LayoutParams)tableRow$LayoutParams);
                }
                else {
                    final int row = i + 1;
                    final int column = (j + 1) / 2;
                    final sb0 sb0 = new sb0((Context)this.v);
                    sb0.setColumn(column);
                    sb0.setRow(row);
                    ((View)sb0).setBackgroundColor(this.getResources().getColor(2131099716));
                    ((View)sb0).setOnClickListener(this.G);
                    ((ViewGroup)tableRow).addView((View)sb0, (ViewGroup$LayoutParams)tableRow$LayoutParams2);
                    this.D[row - 1][column - 1] = sb0;
                }
            }
            this.d.addView((View)tableRow);
        }
    }
    
    public final void n0(final AnswerOption answerOption) {
        switch (CustomTemplateActivity$h.a[answerOption.type.ordinal()]) {
            default: {
                return;
            }
            case 11: {
                this.s0(answerOption);
                return;
            }
            case 10: {
                this.p0(answerOption);
                return;
            }
            case 9: {
                this.r0(answerOption);
                return;
            }
            case 7: {
                this.q0(answerOption);
                return;
            }
            case 6: {
                this.u0(answerOption);
                return;
            }
            case 5: {
                this.u0(answerOption);
                return;
            }
            case 4: {
                this.u0(answerOption);
                return;
            }
            case 3: {
                this.u0(answerOption);
                return;
            }
            case 2: {
                this.u0(answerOption);
                return;
            }
            case 1: {
                this.u0(answerOption);
            }
        }
    }
    
    public final void o0(final int n, final int n2) {
        final rb0 rb0 = this.C[n - 1][n2 - 1];
        if (rb0 == null) {
            return;
        }
        final Object b = rb0.b;
        if (b instanceof AnswerOption) {
            this.n0((AnswerOption)b);
        }
        else {
            this.t0((SheetLabel)b);
        }
    }
    
    @Override
    public void onBackPressed() {
        xs.c((Context)this.v, new y01(this) {
            public final CustomTemplateActivity a;
            
            @Override
            public void a(final Object o) {
                this.a.onBackPressed();
                CustomTemplateActivity.E(this.a).finish();
            }
        }, 2131886129, 2131886505, 2131886903, 2131886657, 0, 2131230930);
    }
    
    @Override
    public void onCreate(final Bundle bundle) {
        super.onCreate(bundle);
        this.setContentView(2131492902);
        this.H0();
        if (this.getIntent().getExtras() != null) {
            this.c = (LabelProfile)this.getIntent().getSerializableExtra("LABEL_PROFILE");
            this.t = (TemplateParams2)this.getIntent().getSerializableExtra("TEMPLATE_PARAMS");
            if (this.c == null) {
                this.c = new LabelProfile();
            }
            this.G0(this.t);
            this.l0();
        }
        this.O0();
        this.W0();
        this.S0();
        this.Q0();
        this.R0();
        this.U0();
        this.T0();
        this.P0();
        this.V0();
        this.Z0(this.t);
    }
    
    public final void p0(final AnswerOption answerOption) {
        final int row = answerOption.row;
        final int column = answerOption.column;
        answerOption.column = -1;
        answerOption.row = -1;
        int n;
        if (answerOption.subIndex > 5) {
            n = 4;
        }
        else {
            n = 2;
        }
        for (int i = 0; i < n; ++i) {
            this.C[row - 1 + i][column - 1] = null;
            this.b1(row + i, column);
        }
    }
    
    public final void q0(final AnswerOption answerOption) {
        final MatrixOptionPayload c = bc.c(answerOption.payload);
        int primaryOptions;
        if (c != null) {
            primaryOptions = c.primaryOptions;
        }
        else {
            primaryOptions = 4;
        }
        final int row = answerOption.row;
        final int column = answerOption.column;
        answerOption.column = -1;
        answerOption.row = -1;
        for (int i = 0; i < primaryOptions + 1; ++i) {
            this.C[row - 1 + i][column - 1] = null;
            this.b1(row + i, column);
        }
    }
    
    public final void r0(final AnswerOption answerOption) {
        final int row = answerOption.row;
        final int column = answerOption.column;
        for (final AnswerOption answerOption2 : this.x) {
            if (answerOption2.column == column && answerOption2.row == row) {
                answerOption2.row = -1;
                answerOption2.column = -1;
                answerOption2.subIndex = -1;
            }
        }
        int n;
        if (bc.b(answerOption.payload).decimalAllowed) {
            n = 12;
        }
        else {
            n = 11;
        }
        for (int i = 0; i < n; ++i) {
            this.C[row - 1 + i][column - 1] = null;
            this.b1(row + i, column);
        }
    }
    
    public final void s0(final AnswerOption answerOption) {
        final int row = answerOption.row;
        final int column = answerOption.column;
        answerOption.column = -1;
        answerOption.row = -1;
        final int n = 0;
        int n2 = 0;
        int i;
        while (true) {
            i = n;
            if (n2 >= 12) {
                break;
            }
            this.C[row - 1 + n2][column - 1] = null;
            this.b1(row + n2, column);
            ++n2;
        }
        while (i < this.y.size()) {
            if (((SheetLabel)this.y.get(i)).getType() == SheetLabel.LableType.ROLL_LABEL) {
                this.y.remove(i);
                return;
            }
            ++i;
        }
    }
    
    public final void t0(final SheetLabel o) {
        final int row = o.getRow();
        final int column = o.getColumn();
        this.y.remove(o);
        this.C[o.getRow() - 1][o.getColumn() - 1] = null;
        this.b1(row, column);
    }
    
    public final void u0(final AnswerOption answerOption) {
        final int row = answerOption.row;
        final int column = answerOption.column;
        this.C[row - 1][column - 1] = null;
        answerOption.column = -1;
        answerOption.row = -1;
        this.b1(row, column);
    }
    
    public final int[] v0() {
        final int n = this.n;
        final int[] array = new int[n];
        for (int i = 0; i < n; ++i) {
            array[i] = 8;
            int n2 = 0;
            while (true) {
                final rb0[][] c = this.C;
                if (n2 >= c.length) {
                    break;
                }
                final rb0 rb0 = c[n2][i];
                if (rb0 != null) {
                    final Object b = rb0.b;
                    if (b instanceof AnswerOption) {
                        final int w0 = this.w0((AnswerOption)b);
                        if (w0 > array[i]) {
                            array[i] = w0;
                        }
                    }
                }
                ++n2;
            }
        }
        return array;
    }
    
    public final int w0(final AnswerOption answerOption) {
        final int n = CustomTemplateActivity$h.a[answerOption.type.ordinal()];
        int n2 = 5;
        if (n == 5) {
            return 9;
        }
        final int n3 = 6;
        if (n == 6) {
            return 11;
        }
        if (n != 7) {
            if (n == 9) {
                final int digits = bc.b(answerOption.payload).digits;
                if (digits > 5) {
                    n2 = digits;
                }
                if (answerOption.subIndex > 0) {
                    n2 = n3;
                }
                return n2 + 3;
            }
            if (n == 11) {
                return answerOption.subIndex + 3;
            }
            if (n != 12) {
                return 8;
            }
            return 13;
        }
        else {
            final MatrixOptionPayload c = bc.c(answerOption.payload);
            int secondaryOptions;
            if (c != null) {
                secondaryOptions = c.secondaryOptions;
            }
            else {
                secondaryOptions = 5;
            }
            if (secondaryOptions > 5) {
                return secondaryOptions + 3;
            }
            return 8;
        }
    }
    
    public final String x0(final String[] array) {
        final int length = array.length;
        String string = "";
        for (final String str : array) {
            final StringBuilder sb = new StringBuilder();
            sb.append(string);
            sb.append(str);
            string = sb.toString();
        }
        return string;
    }
    
    public final ArrayList y0(final ArrayList list) {
        final ArrayList list2 = new ArrayList();
        list2.add(null);
        for (final AnswerOption e : list) {
            if (e.type == AnswerOption.AnswerOptionType.DECIMAL) {
                list2.add(e);
            }
        }
        return list2;
    }
    
    public final SheetTemplate2 z0() {
        final int n = this.n;
        final int p = this.p;
        AnswerOption w = this.w;
        if (w == null || w.column <= 0 || w.row <= 0) {
            w = null;
        }
        final ArrayList<AnswerOption> list = new ArrayList<AnswerOption>();
        for (final AnswerOption e : this.x) {
            if (e.column > 0 && e.row > 0) {
                list.add(e);
            }
        }
        final SheetTemplate2 sheetTemplate2 = new SheetTemplate2(new PageLayout[] { new PageLayout(p * 5 + 1, n, this.v0()) }, 30, 20, w, list, this.y, this.t);
        sheetTemplate2.setLabelProfile(this.c);
        return sheetTemplate2;
    }
    
    public enum MODE
    {
        private static final MODE[] $VALUES;
        
        ADD, 
        ERASE, 
        OTHER;
        
        private static /* synthetic */ MODE[] $values() {
            return new MODE[] { MODE.ADD, MODE.ERASE, MODE.OTHER };
        }
        
        static {
            $VALUES = $values();
        }
    }
}

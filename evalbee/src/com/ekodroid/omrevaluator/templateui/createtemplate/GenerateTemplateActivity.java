// 
// Decompiled by Procyon v0.6.0
// 

package com.ekodroid.omrevaluator.templateui.createtemplate;

import android.widget.CompoundButton;
import android.os.Bundle;
import android.widget.RadioButton;
import android.widget.ArrayAdapter;
import android.view.ViewGroup;
import android.view.LayoutInflater;
import com.google.android.material.dialog.MaterialAlertDialogBuilder;
import com.google.firebase.analytics.FirebaseAnalytics;
import android.provider.Settings$Secure;
import com.ekodroid.omrevaluator.templateui.models.ExamId;
import android.graphics.Bitmap;
import com.ekodroid.omrevaluator.database.LabelProfileJsonModel;
import java.util.Iterator;
import android.widget.AdapterView;
import android.widget.AdapterView$OnItemSelectedListener;
import android.widget.SpinnerAdapter;
import com.ekodroid.omrevaluator.database.TemplateHeaderJsonDataModel;
import com.ekodroid.omrevaluator.database.repositories.TemplateRepository;
import java.util.ArrayList;
import android.view.View;
import android.view.View$OnClickListener;
import com.ekodroid.omrevaluator.activities.HomeBottomNavActivity;
import java.io.Serializable;
import android.content.Context;
import android.content.Intent;
import com.ekodroid.omrevaluator.templateui.models.TemplateSizeTooSmallException;
import android.graphics.BitmapFactory;
import android.content.SharedPreferences;
import com.ekodroid.omrevaluator.templateui.models.HeaderProfile;
import com.ekodroid.omrevaluator.templateui.models.LabelProfile;
import com.ekodroid.omrevaluator.templateui.models.a;
import com.ekodroid.omrevaluator.templateui.models.SheetTemplate2;
import com.ekodroid.omrevaluator.templateui.models.TemplateParams2;
import com.ekodroid.omrevaluator.templateui.components.ZoomableImageView;
import android.widget.Spinner;
import com.ekodroid.omrevaluator.templateui.models.TemplateBuilder;
import android.widget.TextView;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.Button;

public class GenerateTemplateActivity extends v5
{
    public Button A;
    public LinearLayout C;
    public ImageButton D;
    public ImageButton F;
    public TextView G;
    public TemplateBuilder.BuildType H;
    public int I;
    public GenerateTemplateActivity c;
    public Button d;
    public ImageButton e;
    public ImageButton f;
    public ImageButton g;
    public TextView h;
    public Spinner i;
    public Spinner j;
    public Spinner k;
    public ZoomableImageView l;
    public TemplateParams2 m;
    public TemplateBuilder n;
    public SheetTemplate2 p;
    public a q;
    public LabelProfile t;
    public HeaderProfile v;
    public String w;
    public wu1 x;
    public SharedPreferences y;
    public Button z;
    
    public GenerateTemplateActivity() {
        this.c = this;
        this.q = new a();
        this.x = new wu1(6, 61, 1, 5, true);
        this.H = TemplateBuilder.BuildType.DEFAULT;
        this.I = 0;
    }
    
    public final void I(final TemplateParams2 templateParams2, final TemplateBuilder.BuildType buildType, final LabelProfile labelProfile, final HeaderProfile headerProfile, final wu1 wu1) {
        this.n = new TemplateBuilder(buildType, templateParams2);
        try {
            if (wu1.e) {
                wu1.b = 61;
                wu1.a = 6;
                wu1.d = 5;
                if (headerProfile != null && headerProfile.getInstruction() != null) {
                    wu1.c = 3;
                }
                else {
                    wu1.c = 2;
                }
            }
            else {
                wu1.a = wu1.b;
                wu1.c = wu1.d;
            }
            (this.p = this.n.b(wu1)).setLabelProfile(labelProfile);
            this.p.setHeaderProfile(headerProfile);
            this.I = 0;
            this.c0();
            this.N();
        }
        catch (final TemplateSizeTooSmallException ex) {
            this.l.setImageBitmap(BitmapFactory.decodeResource(this.getResources(), 2131231089));
            this.f0();
            ex.printStackTrace();
        }
    }
    
    public final int J(final String[] array, final String anObject) {
        for (int i = 0; i < array.length; ++i) {
            if (array[i].equals(anObject)) {
                return i;
            }
        }
        return 0;
    }
    
    public final void K(final HeaderProfile headerProfile) {
        final Intent intent = new Intent((Context)this.c, (Class)CustomHeaderActivity.class);
        intent.putExtra("HEADER_PROFILE", (Serializable)headerProfile);
        ((Context)this).startActivity(intent);
    }
    
    public final void L() {
        final Intent intent = new Intent((Context)this.c, (Class)HomeBottomNavActivity.class);
        intent.addFlags(335544320);
        ((Context)this).startActivity(intent);
    }
    
    public final void M(final LabelProfile labelProfile) {
        final Intent intent = new Intent((Context)this.c, (Class)LabelsActivity.class);
        intent.putExtra("LABEL_PROFILE", (Serializable)labelProfile);
        ((Context)this).startActivity(intent);
    }
    
    public final void N() {
        ((View)this.h).setVisibility(4);
        ((View)this.A).setEnabled(true);
    }
    
    public final void O() {
        ((View)this.d).setOnClickListener((View$OnClickListener)new View$OnClickListener(this) {
            public final GenerateTemplateActivity a;
            
            public void onClick(final View view) {
                final Intent intent = new Intent((Context)this.a.c, (Class)CustomTemplateActivity.class);
                intent.putExtra("TEMPLATE_PARAMS", (Serializable)this.a.m);
                intent.putExtra("LABEL_PROFILE", (Serializable)this.a.t);
                ((Context)this.a).startActivity(intent);
            }
        });
    }
    
    public final void P() {
        ((View)this.F).setOnClickListener((View$OnClickListener)new View$OnClickListener(this) {
            public final GenerateTemplateActivity a;
            
            public void onClick(final View view) {
                final SheetTemplate2 p = this.a.p;
                if (p != null) {
                    final int length = p.getPageLayouts().length;
                    final GenerateTemplateActivity a = this.a;
                    final int i = a.I;
                    if (i < length - 1) {
                        a.I = i + 1;
                    }
                    a.c0();
                }
            }
        });
    }
    
    public final void Q() {
        ((View)this.D).setOnClickListener((View$OnClickListener)new View$OnClickListener(this) {
            public final GenerateTemplateActivity a;
            
            public void onClick(final View view) {
                final GenerateTemplateActivity a = this.a;
                if (a.p != null) {
                    final int i = a.I;
                    if (i > 0) {
                        a.I = i - 1;
                    }
                    a.c0();
                }
            }
        });
    }
    
    public final void R() {
        ((View)this.f).setOnClickListener((View$OnClickListener)new View$OnClickListener(this) {
            public final GenerateTemplateActivity a;
            
            public void onClick(final View view) {
                final GenerateTemplateActivity a = this.a;
                a.K(a.v);
            }
        });
    }
    
    public final void S() {
        ((View)this.g).setOnClickListener((View$OnClickListener)new View$OnClickListener(this) {
            public final GenerateTemplateActivity a;
            
            public void onClick(final View view) {
                final GenerateTemplateActivity a = this.a;
                a.e0(a.w);
            }
        });
    }
    
    public final void T() {
        ((View)this.e).setOnClickListener((View$OnClickListener)new View$OnClickListener(this) {
            public final GenerateTemplateActivity a;
            
            public void onClick(final View view) {
                final GenerateTemplateActivity a = this.a;
                a.M(a.t);
            }
        });
    }
    
    public final void U() {
        ((View)this.z).setOnClickListener((View$OnClickListener)new View$OnClickListener(this) {
            public final GenerateTemplateActivity a;
            
            public void onClick(final View view) {
                this.a.c.finish();
            }
        });
    }
    
    public final void V() {
        ((View)this.A).setEnabled(true);
        ((View)this.A).setOnClickListener((View$OnClickListener)new View$OnClickListener(this) {
            public final GenerateTemplateActivity a;
            
            public void onClick(final View view) {
                this.a.d0();
            }
        });
    }
    
    public final void W() {
        this.y = ((Context)this).getApplicationContext().getSharedPreferences("MyPref", 0);
        this.m = (TemplateParams2)this.getIntent().getSerializableExtra("TEMPLATE_DETAIL");
        ((View)(this.h = (TextView)this.findViewById(2131297226))).setVisibility(4);
        this.F = (ImageButton)this.findViewById(2131296693);
        this.D = (ImageButton)this.findViewById(2131296695);
        this.G = (TextView)this.findViewById(2131297277);
        this.z = (Button)this.findViewById(2131296458);
        this.A = (Button)this.findViewById(2131296460);
        this.C = (LinearLayout)this.findViewById(2131296773);
        this.d = (Button)this.findViewById(2131296443);
        this.l = (ZoomableImageView)this.findViewById(2131297422);
        this.i = (Spinner)this.findViewById(2131297086);
        this.j = (Spinner)this.findViewById(2131297082);
        this.k = (Spinner)this.findViewById(2131297087);
        this.g = (ImageButton)this.findViewById(2131296660);
        this.e = (ImageButton)this.findViewById(2131296659);
        this.f = (ImageButton)this.findViewById(2131296658);
    }
    
    public final void X() {
        final ArrayList list = new ArrayList();
        list.add(ok.a);
        list.add("Default Medium");
        list.add(ok.b);
        list.add("Blank");
        list.add("None");
        final Iterator<TemplateHeaderJsonDataModel> iterator = TemplateRepository.getInstance((Context)this.c).getAllHeaderProfileJson().iterator();
        while (iterator.hasNext()) {
            list.add(iterator.next().getHeaderName());
        }
        this.j.setAdapter((SpinnerAdapter)new p3((Context)this.c, list));
        ((AdapterView)this.j).setOnItemSelectedListener((AdapterView$OnItemSelectedListener)new AdapterView$OnItemSelectedListener(this, list) {
            public final ArrayList a;
            public final GenerateTemplateActivity b;
            
            public void onItemSelected(final AdapterView adapterView, final View view, final int index, final long n) {
                HeaderProfile v = null;
                if (index == 2) {
                    this.b.K(null);
                    return;
                }
                GenerateTemplateActivity generateTemplateActivity;
                if (index > 4) {
                    generateTemplateActivity = this.b;
                    v = TemplateRepository.getInstance((Context)generateTemplateActivity.c).getHeaderProfileJsonModel(this.a.get(index)).getHeaderProfile();
                }
                else if (index == 0) {
                    generateTemplateActivity = this.b;
                    v = HeaderProfile.getDefaultHeaderprofile();
                }
                else if (index == 1) {
                    generateTemplateActivity = this.b;
                    v = HeaderProfile.getDefaultMediumHeaderprofile();
                }
                else if (index == 3) {
                    generateTemplateActivity = this.b;
                    v = HeaderProfile.getBlankHeaderprofile();
                }
                else {
                    generateTemplateActivity = this.b;
                }
                generateTemplateActivity.v = v;
                final GenerateTemplateActivity b = this.b;
                b.I(b.m, b.H, b.t, b.v, b.x);
            }
            
            public void onNothingSelected(final AdapterView adapterView) {
            }
        });
        ((AdapterView)this.j).setSelection(0);
    }
    
    public final void Y() {
        final ArrayList<LabelProfileJsonModel> allLabelProfileJson = TemplateRepository.getInstance((Context)this.c).getAllLabelProfileJson();
        final ArrayList list = new ArrayList();
        list.add(ok.a);
        list.add(ok.b);
        final Iterator<LabelProfileJsonModel> iterator = allLabelProfileJson.iterator();
        while (iterator.hasNext()) {
            list.add(iterator.next().getProfileName());
        }
        this.i.setAdapter((SpinnerAdapter)new p3((Context)this.c, list));
        ((AdapterView)this.i).setOnItemSelectedListener((AdapterView$OnItemSelectedListener)new AdapterView$OnItemSelectedListener(this, list) {
            public final ArrayList a;
            public final GenerateTemplateActivity b;
            
            public void onItemSelected(final AdapterView adapterView, final View view, final int index, final long n) {
                if (index == 0) {
                    final GenerateTemplateActivity b = this.b;
                    b.t = null;
                    b.I(b.m, b.H, null, b.v, b.x);
                    return;
                }
                if (index == 1) {
                    this.b.M(null);
                    return;
                }
                this.b.t = TemplateRepository.getInstance((Context)this.b.c).getLabelProfileJsonModel(this.a.get(index)).getLabelProfile();
                final GenerateTemplateActivity b2 = this.b;
                b2.I(b2.m, b2.H, b2.t, b2.v, b2.x);
            }
            
            public void onNothingSelected(final AdapterView adapterView) {
            }
        });
        ((AdapterView)this.i).setSelection(0);
    }
    
    public final void Z() {
        final ArrayList list = new ArrayList();
        list.add(ok.L);
        list.add(ok.K);
        list.add(ok.b);
        final Iterator iterator = a91.y(this.y.getString("LAYOUT_LIST", "")).iterator();
        while (iterator.hasNext()) {
            list.add(iterator.next());
        }
        this.k.setAdapter((SpinnerAdapter)new p3((Context)this.c, list));
        ((AdapterView)this.k).setOnItemSelectedListener((AdapterView$OnItemSelectedListener)new AdapterView$OnItemSelectedListener(this) {
            public final GenerateTemplateActivity a;
            
            public void onItemSelected(final AdapterView adapterView, final View view, int int1, final long n) {
                final GenerateTemplateActivity a = this.a;
                a.w = (String)((AdapterView)a.k).getSelectedItem();
                if (int1 == 0) {
                    final GenerateTemplateActivity a2 = this.a;
                    a2.H = TemplateBuilder.BuildType.DEFAULT;
                    a2.x.e = true;
                }
                if (int1 == 1) {
                    final GenerateTemplateActivity a3 = this.a;
                    a3.H = TemplateBuilder.BuildType.SUBJECT_IN_COLUMN;
                    a3.x.e = true;
                }
                int int2 = 0;
                if (int1 == 2) {
                    this.a.e0(null);
                    ((AdapterView)this.a.k).setSelection(0);
                    return;
                }
                Label_0222: {
                    if (int1 > 2) {
                        final String[] split = this.a.w.split("-");
                        if (split.length == 2) {
                            GenerateTemplateActivity generateTemplateActivity;
                            TemplateBuilder.BuildType h;
                            if (split[1].toUpperCase().equals(ok.K.toUpperCase())) {
                                generateTemplateActivity = this.a;
                                h = TemplateBuilder.BuildType.SUBJECT_IN_COLUMN;
                            }
                            else {
                                generateTemplateActivity = this.a;
                                h = TemplateBuilder.BuildType.DEFAULT;
                            }
                            generateTemplateActivity.H = h;
                            final String[] split2 = split[0].split("X");
                            if (split2.length == 2) {
                                int2 = Integer.parseInt(split2[0]);
                                int1 = Integer.parseInt(split2[1]);
                            }
                            else {
                                int2 = 0;
                                int1 = 0;
                            }
                            this.a.x.e = false;
                            break Label_0222;
                        }
                    }
                    int1 = 0;
                }
                final GenerateTemplateActivity a4 = this.a;
                final wu1 x = a4.x;
                x.d = int2;
                x.b = int1 * 5 + 1;
                a4.I(a4.m, a4.H, a4.t, a4.v, x);
            }
            
            public void onNothingSelected(final AdapterView adapterView) {
            }
        });
        ((AdapterView)this.k).setSelection(0);
    }
    
    public final void a0() {
        final SheetTemplate2 p = this.p;
        if (p != null && p.getPageLayouts().length > 1) {
            final int length = this.p.getPageLayouts().length;
            ((View)this.C).setVisibility(0);
            final int i = this.I;
            Label_0100: {
                if (i == 0) {
                    ((View)this.D).setEnabled(false);
                }
                else {
                    if (i == length - 1) {
                        ((View)this.D).setEnabled(true);
                        ((View)this.F).setEnabled(false);
                        break Label_0100;
                    }
                    ((View)this.D).setEnabled(true);
                }
                ((View)this.F).setEnabled(true);
            }
            final TextView g = this.G;
            final StringBuilder sb = new StringBuilder();
            sb.append("");
            sb.append(this.I + 1);
            sb.append("/");
            sb.append(length);
            g.setText((CharSequence)sb.toString());
        }
        else {
            ((View)this.C).setVisibility(8);
        }
    }
    
    public final void b0() {
        this.Y();
        this.X();
        this.Z();
        this.c0();
    }
    
    public final void c0() {
        final SheetTemplate2 p = this.p;
        Bitmap imageBitmap;
        if (p == null) {
            imageBitmap = BitmapFactory.decodeResource(this.getResources(), 2131231089);
        }
        else {
            imageBitmap = this.q.X((Context)this.c, p, this.I);
        }
        this.l.setImageBitmap(imageBitmap);
        this.a0();
    }
    
    public final void d0() {
        new ti1((Context)this.c, new y01(this) {
            public final GenerateTemplateActivity a;
            
            @Override
            public void a(final Object o) {
                final ExamId examId = (ExamId)o;
                final String string = Settings$Secure.getString(((Context)this.a.c).getContentResolver(), "android_id");
                final FirebaseAnalytics instance = FirebaseAnalytics.getInstance(((Context)this.a).getApplicationContext());
                final StringBuilder sb = new StringBuilder();
                sb.append("Q ");
                sb.append(this.a.p.getAnswerOptions().size() - 1);
                o4.b(instance, "EXAM_SAVE", string, sb.toString());
                this.a.L();
            }
        }, this.p, null).show();
    }
    
    public final void e0(final String s) {
        final MaterialAlertDialogBuilder materialAlertDialogBuilder = new MaterialAlertDialogBuilder((Context)this.c, 2131951953);
        final View inflate = ((LayoutInflater)((Context)this).getSystemService("layout_inflater")).inflate(2131492965, (ViewGroup)null);
        materialAlertDialogBuilder.setView(inflate);
        materialAlertDialogBuilder.setTitle(2131886216);
        materialAlertDialogBuilder.setCancelable(true);
        final Spinner spinner = (Spinner)inflate.findViewById(2131297081);
        final Spinner spinner2 = (Spinner)inflate.findViewById(2131297080);
        final GenerateTemplateActivity c = this.c;
        final String[] d = gr1.d;
        spinner.setAdapter((SpinnerAdapter)new ArrayAdapter((Context)c, 2131493104, (Object[])d));
        final GenerateTemplateActivity c2 = this.c;
        final String[] e = gr1.e;
        spinner2.setAdapter((SpinnerAdapter)new ArrayAdapter((Context)c2, 2131493104, (Object[])e));
        final RadioButton radioButton = (RadioButton)inflate.findViewById(2131296982);
        final RadioButton radioButton2 = (RadioButton)inflate.findViewById(2131296999);
        final Button button = (Button)inflate.findViewById(2131296394);
        final Button button2 = (Button)inflate.findViewById(2131296446);
        final androidx.appcompat.app.a create = materialAlertDialogBuilder.create();
        if (s != null) {
            ((View)button).setVisibility(0);
            final String[] split = s.split("-");
            if (split.length == 2) {
                if (split[1].equals(ok.L)) {
                    ((CompoundButton)radioButton).setChecked(true);
                }
                else {
                    ((CompoundButton)radioButton2).setChecked(true);
                }
                final String[] split2 = split[0].split("X");
                if (split2.length == 2) {
                    ((AdapterView)spinner).setSelection(this.J(d, split2[0]));
                    ((AdapterView)spinner2).setSelection(this.J(e, split2[1]));
                }
            }
        }
        ((View)button).setOnClickListener((View$OnClickListener)new View$OnClickListener(this, s, create) {
            public final String a;
            public final androidx.appcompat.app.a b;
            public final GenerateTemplateActivity c;
            
            public void onClick(final View view) {
                final ArrayList y = a91.y(this.c.y.getString("LAYOUT_LIST", ""));
                if (y.contains(this.a)) {
                    y.remove(this.a);
                    this.c.y.edit().putString("LAYOUT_LIST", new gc0().s(y)).commit();
                    a91.G((Context)this.c.c, 2131886380, 2131230927, 2131231085);
                }
                this.c.Z();
                this.b.dismiss();
            }
        });
        ((View)button2).setOnClickListener((View$OnClickListener)new View$OnClickListener(this, radioButton, spinner, spinner2, create) {
            public final RadioButton a;
            public final Spinner b;
            public final Spinner c;
            public final androidx.appcompat.app.a d;
            public final GenerateTemplateActivity e;
            
            public void onClick(final View view) {
                String str;
                if (((CompoundButton)this.a).isChecked()) {
                    str = ok.L;
                }
                else {
                    str = ok.K;
                }
                final StringBuilder sb = new StringBuilder();
                sb.append(((AdapterView)this.b).getSelectedItem());
                sb.append("X");
                sb.append(((AdapterView)this.c).getSelectedItem());
                sb.append("-");
                sb.append(str);
                final String string = sb.toString();
                final ArrayList y = a91.y(this.e.y.getString("LAYOUT_LIST", ""));
                if (!y.contains(string)) {
                    y.add(string);
                    this.e.y.edit().putString("LAYOUT_LIST", new gc0().s(y)).commit();
                    a91.G((Context)this.e.c, 2131886379, 2131230927, 2131231085);
                }
                this.e.Z();
                this.d.dismiss();
            }
        });
        create.show();
    }
    
    public final void f0() {
        ((View)this.h).setVisibility(0);
        ((View)this.A).setEnabled(false);
        this.h.setText((CharSequence)"Selected layout size is too small to fit all answer options, please select suitable size");
    }
    
    @Override
    public void onCreate(final Bundle bundle) {
        super.onCreate(bundle);
        this.setContentView(2131492908);
        this.W();
        this.V();
        this.U();
        this.O();
        this.T();
        this.R();
        this.S();
        this.P();
        this.Q();
    }
    
    @Override
    public void onResume() {
        super.onResume();
        this.b0();
    }
}

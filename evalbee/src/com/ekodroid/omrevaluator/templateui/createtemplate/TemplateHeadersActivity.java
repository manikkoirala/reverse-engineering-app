// 
// Decompiled by Procyon v0.6.0
// 

package com.ekodroid.omrevaluator.templateui.createtemplate;

import android.os.Bundle;
import androidx.appcompat.widget.Toolbar;
import java.util.Iterator;
import android.widget.ListAdapter;
import com.ekodroid.omrevaluator.database.repositories.TemplateRepository;
import java.io.Serializable;
import android.widget.AdapterView;
import android.widget.AdapterView$OnItemClickListener;
import android.content.Context;
import android.content.Intent;
import android.view.View;
import android.view.View$OnClickListener;
import com.ekodroid.omrevaluator.database.TemplateHeaderJsonDataModel;
import java.util.ArrayList;
import android.widget.Button;
import android.widget.ListView;

public class TemplateHeadersActivity extends v5
{
    public TemplateHeadersActivity c;
    public ListView d;
    public Button e;
    public j3 f;
    public ArrayList g;
    
    public TemplateHeadersActivity() {
        this.c = this;
    }
    
    public static /* synthetic */ ArrayList A(final TemplateHeadersActivity templateHeadersActivity) {
        return templateHeadersActivity.g;
    }
    
    public final void D() {
        ((View)this.e).setOnClickListener((View$OnClickListener)new View$OnClickListener(this) {
            public final TemplateHeadersActivity a;
            
            public void onClick(final View view) {
                ((Context)this.a).startActivity(new Intent((Context)this.a.c, (Class)CustomHeaderActivity.class));
            }
        });
    }
    
    public final void E() {
        ((AdapterView)this.d).setOnItemClickListener((AdapterView$OnItemClickListener)new AdapterView$OnItemClickListener(this) {
            public final TemplateHeadersActivity a;
            
            public void onItemClick(final AdapterView adapterView, final View view, final int n, final long n2) {
                if (view.getId() == 2131296656L) {
                    final TemplateHeadersActivity a = this.a;
                    a.H((TemplateHeaderJsonDataModel)TemplateHeadersActivity.A(a).get(n));
                }
                else {
                    view.setBackgroundResource(2131230860);
                    final Intent intent = new Intent((Context)this.a.c, (Class)CustomHeaderActivity.class);
                    intent.putExtra("HEADER_PROFILE", (Serializable)TemplateHeadersActivity.A(this.a).get(n).getHeaderProfile());
                    ((Context)this.a).startActivity(intent);
                }
            }
        });
    }
    
    public final void F() {
        this.g = TemplateRepository.getInstance((Context)this.c).getAllHeaderProfileJson();
        final ArrayList list = new ArrayList();
        final Iterator iterator = this.g.iterator();
        while (iterator.hasNext()) {
            list.add(((TemplateHeaderJsonDataModel)iterator.next()).getHeaderName());
        }
        final j3 j3 = new j3((Context)this.c, list);
        this.f = j3;
        this.d.setAdapter((ListAdapter)j3);
    }
    
    public final void G() {
        final Toolbar toolbar = (Toolbar)this.findViewById(2131297347);
        this.x(toolbar);
        toolbar.setNavigationOnClickListener((View$OnClickListener)new View$OnClickListener(this) {
            public final TemplateHeadersActivity a;
            
            public void onClick(final View view) {
                this.a.finish();
            }
        });
    }
    
    public final void H(final TemplateHeaderJsonDataModel templateHeaderJsonDataModel) {
        xs.c((Context)this.c, new y01(this, templateHeaderJsonDataModel) {
            public final TemplateHeaderJsonDataModel a;
            public final TemplateHeadersActivity b;
            
            @Override
            public void a(final Object o) {
                TemplateRepository.getInstance((Context)this.b.c).deleteHeaderProfileJson(this.a.getHeaderName());
                a91.G((Context)this.b.c, 2131886339, 2131230927, 2131231085);
                this.b.F();
            }
        }, 2131886237, 2131886478, 2131886903, 2131886657, 0, 2131230945);
    }
    
    @Override
    public void onCreate(final Bundle bundle) {
        super.onCreate(bundle);
        this.setContentView(2131492932);
        this.G();
        this.e = (Button)this.findViewById(2131296447);
        this.d = (ListView)this.findViewById(2131296839);
        this.E();
        this.D();
    }
    
    @Override
    public void onResume() {
        super.onResume();
        this.F();
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.ekodroid.omrevaluator.templateui.createtemplate;

import android.os.Bundle;
import android.view.ViewGroup;
import android.view.MotionEvent;
import android.view.View$OnTouchListener;
import android.widget.EditText;
import java.io.Serializable;
import android.content.Intent;
import android.view.View$OnClickListener;
import android.widget.TextView;
import com.ekodroid.omrevaluator.templateui.models.Section2;
import android.view.ViewGroup$LayoutParams;
import android.view.View;
import android.widget.LinearLayout$LayoutParams;
import java.util.ArrayList;
import com.ekodroid.omrevaluator.templateui.models.Subject2;
import android.content.Context;
import android.content.SharedPreferences;
import android.widget.LinearLayout;
import com.ekodroid.omrevaluator.templateui.models.TemplateParams2;
import android.widget.Button;

public class SetTemplate2Activity extends v5
{
    public SetTemplate2Activity c;
    public Button d;
    public Button e;
    public TemplateParams2 f;
    public LinearLayout g;
    public Integer[] h;
    public SharedPreferences i;
    public jk1[][] j;
    
    public SetTemplate2Activity() {
        this.c = this;
        this.h = new Integer[225];
    }
    
    public final int A(final int n, final Context context) {
        return (int)(n * context.getResources().getDisplayMetrics().density);
    }
    
    public final void B(final Subject2[] array) {
        final ArrayList o = a91.o(this.i.getString("plus_mark_json", ""), ok.I);
        final ArrayList m = a91.m(this.i.getString("minus_mark_json", ""), ok.J);
        final y01 y01 = new y01(this) {
            public final SetTemplate2Activity a;
            
            @Override
            public void a(final Object o) {
                final ArrayList o2 = a91.o(this.a.i.getString("plus_mark_json", ""), ok.I);
                final ArrayList m = a91.m(this.a.i.getString("minus_mark_json", ""), ok.J);
                for (int i = 0; i < this.a.j.length; ++i) {
                    int n = 0;
                    while (true) {
                        final jk1[] array = this.a.j[i];
                        if (n >= array.length) {
                            break;
                        }
                        array[n].u(o2, m);
                        ++n;
                    }
                }
            }
        };
        for (int i = 0; i < array.length; ++i) {
            final LinearLayout c = this.C();
            ((ViewGroup)c).addView((View)this.D(array[i].getSubName()), (ViewGroup$LayoutParams)new LinearLayout$LayoutParams(-1, -2));
            ((ViewGroup)this.g).addView((View)c);
            final Section2[] sections = array[i].getSections();
            this.j[i] = new jk1[sections.length];
            int k;
            for (int j = 0; j < sections.length; j = k) {
                final jk1[] array2 = this.j[i];
                final SetTemplate2Activity c2 = this.c;
                final Section2 section2 = sections[j];
                final StringBuilder sb = new StringBuilder();
                sb.append(((Context)this).getString(2131886778));
                k = j + 1;
                sb.append(k);
                array2[j] = new jk1((Context)c2, section2, sb.toString(), this.h, o, m, y01);
                ((ViewGroup)this.g).addView((View)this.j[i][j].o());
            }
        }
        this.hideKeyboardForViewsExceptEditText((View)this.g);
    }
    
    public final LinearLayout C() {
        final LinearLayout linearLayout = new LinearLayout((Context)this);
        linearLayout.setOrientation(0);
        linearLayout.setWeightSum(2.0f);
        ((View)linearLayout).setBackgroundResource(2131230848);
        ((View)linearLayout).setLayoutParams((ViewGroup$LayoutParams)new LinearLayout$LayoutParams(-1, -2));
        return linearLayout;
    }
    
    public final TextView D(final String text) {
        final TextView textView = new TextView((Context)this);
        textView.setText((CharSequence)text);
        ((View)textView).setBackgroundColor(this.getResources().getColor(2131099701));
        textView.setGravity(16);
        textView.setTextAppearance(2131952185);
        textView.setPadding(this.A(12, (Context)this), this.A(8, (Context)this), this.A(12, (Context)this), this.A(8, (Context)this));
        return textView;
    }
    
    public final void E() {
        ((View)this.e).setOnClickListener((View$OnClickListener)new View$OnClickListener(this) {
            public final SetTemplate2Activity a;
            
            public void onClick(final View view) {
                this.a.c.finish();
            }
        });
    }
    
    public final void F() {
        ((View)this.d).setOnClickListener((View$OnClickListener)new View$OnClickListener(this) {
            public final SetTemplate2Activity a;
            
            public void onClick(final View view) {
                final Subject2[] subjects = this.a.f.getSubjects();
                for (int i = 0; i < subjects.length; ++i) {
                    final Section2[] sections = subjects[i].getSections();
                    for (int j = 0; j < sections.length; ++j) {
                        sections[j] = new Section2();
                        final String n = this.a.j[i][j].n();
                        if (n.trim().equals("")) {
                            a91.G((Context)this.a.c, 2131886268, 2131230909, 2131231086);
                            return;
                        }
                        sections[j].setName(n);
                        sections[j].setNoOfQue(this.a.j[i][j].k());
                        sections[j].setPositiveMark(this.a.j[i][j].r());
                        sections[j].setNegetiveMark(this.a.j[i][j].q());
                        sections[j].setPartialAllowed(this.a.j[i][j].s());
                        sections[j].setType(this.a.j[i][j].p());
                        sections[j].setPayload(this.a.j[i][j].m());
                        sections[j].setNoOfOptionalQue(this.a.j[i][j].j());
                        sections[j].setOptionalType(this.a.j[i][j].l());
                        sections[j].setOptionalAllowed(this.a.j[i][j].t());
                    }
                    subjects[i].setSections(sections);
                }
                final Intent intent = new Intent((Context)this.a.c, (Class)GenerateTemplateActivity.class);
                intent.putExtra("TEMPLATE_DETAIL", (Serializable)this.a.f);
                ((Context)this.a).startActivity(intent);
            }
        });
    }
    
    public final void G(final TemplateParams2 templateParams2) {
        this.getWindow().setSoftInputMode(3);
        int n = 0;
        this.i = ((Context)this).getSharedPreferences("MyPref", 0);
        this.g = (LinearLayout)this.findViewById(2131296720);
        this.d = (Button)this.findViewById(2131296456);
        this.e = (Button)this.findViewById(2131296458);
        final Subject2[] subjects = templateParams2.getSubjects();
        while (true) {
            final Integer[] h = this.h;
            if (n >= h.length) {
                break;
            }
            final int i = n + 1;
            h[n] = i;
            n = i;
        }
        this.j = new jk1[subjects.length][];
    }
    
    public void hideKeyboardForViewsExceptEditText(final View view) {
        if (!(view instanceof EditText)) {
            view.setOnTouchListener((View$OnTouchListener)new View$OnTouchListener(this) {
                public final SetTemplate2Activity a;
                
                public boolean onTouch(final View view, final MotionEvent motionEvent) {
                    a91.B((Context)this.a.c, view);
                    return false;
                }
            });
        }
        if (view instanceof ViewGroup) {
            int n = 0;
            while (true) {
                final ViewGroup viewGroup = (ViewGroup)view;
                if (n >= viewGroup.getChildCount()) {
                    break;
                }
                this.hideKeyboardForViewsExceptEditText(viewGroup.getChildAt(n));
                ++n;
            }
        }
    }
    
    @Override
    public void onCreate(final Bundle bundle) {
        super.onCreate(bundle);
        this.setContentView(2131492923);
        this.G(this.f = (TemplateParams2)this.getIntent().getSerializableExtra("SUBJECT_DETAIL"));
        this.F();
        this.E();
        this.B(this.f.getSubjects());
    }
}

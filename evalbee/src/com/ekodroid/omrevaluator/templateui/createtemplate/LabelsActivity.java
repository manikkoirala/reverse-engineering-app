// 
// Decompiled by Procyon v0.6.0
// 

package com.ekodroid.omrevaluator.templateui.createtemplate;

import android.os.Bundle;
import com.ekodroid.omrevaluator.database.repositories.TemplateRepository;
import android.content.Context;
import android.view.View;
import android.view.View$OnClickListener;
import android.widget.LinearLayout;
import androidx.appcompat.widget.Toolbar;
import com.ekodroid.omrevaluator.templateui.models.LabelProfile;
import android.widget.Button;
import android.widget.TextView;

public class LabelsActivity extends v5
{
    public TextView A;
    public TextView C;
    public TextView D;
    public TextView F;
    public TextView G;
    public TextView H;
    public TextView I;
    public TextView J;
    public TextView K;
    public Button M;
    public LabelProfile O;
    public boolean P;
    public Toolbar Q;
    public LabelsActivity c;
    public LinearLayout d;
    public LinearLayout e;
    public LinearLayout f;
    public LinearLayout g;
    public LinearLayout h;
    public LinearLayout i;
    public LinearLayout j;
    public LinearLayout k;
    public LinearLayout l;
    public LinearLayout m;
    public LinearLayout n;
    public LinearLayout p;
    public LinearLayout q;
    public LinearLayout t;
    public TextView v;
    public TextView w;
    public TextView x;
    public TextView y;
    public TextView z;
    
    public LabelsActivity() {
        this.c = this;
        this.P = false;
    }
    
    public final String[] D(final String[] array, final String[] array2) {
        final String[] array3 = new String[array.length + array2.length];
        System.arraycopy(array, 0, array3, 0, array.length);
        System.arraycopy(array2, 0, array3, array.length, array2.length);
        return array3;
    }
    
    public final String E(final String[] array) {
        final StringBuilder sb = new StringBuilder();
        sb.append(" ");
        for (int i = 0; i < array.length; ++i) {
            String str;
            if (i == 0) {
                str = array[0];
            }
            else {
                sb.append(", ");
                str = array[i];
            }
            sb.append(str);
        }
        return sb.toString();
    }
    
    public final String F(final String[] array, final String[] array2) {
        final StringBuilder sb = new StringBuilder();
        sb.append(" ");
        final int n = 0;
        int n2 = 0;
        int i;
        while (true) {
            i = n;
            if (n2 >= array.length) {
                break;
            }
            String str;
            if (n2 == 0) {
                str = array[0];
            }
            else {
                sb.append(", ");
                str = array[n2];
            }
            sb.append(str);
            ++n2;
        }
        while (i < array2.length) {
            sb.append(", ");
            sb.append(array2[i]);
            ++i;
        }
        return sb.toString();
    }
    
    public final void G() {
        ((View)this.M).setOnClickListener((View$OnClickListener)new View$OnClickListener(this) {
            public final LabelsActivity a;
            
            public void onClick(final View view) {
                final LabelsActivity a = this.a;
                a.Z(a.O.getLabelProfileName());
            }
        });
    }
    
    public final void H() {
        this.findViewById(2131296463).setOnClickListener((View$OnClickListener)new View$OnClickListener(this, new y01(this) {
            public final LabelsActivity a;
            
            @Override
            public void a(final Object o) {
                this.a.finish();
            }
        }) {
            public final y01 a;
            public final LabelsActivity b;
            
            public void onClick(final View view) {
                final LabelsActivity b = this.b;
                new ui1((Context)b.c, this.a, b.O);
            }
        });
    }
    
    public final void I() {
        this.M = (Button)this.findViewById(2131296448);
        this.j = (LinearLayout)this.findViewById(2131296722);
        this.C = (TextView)this.findViewById(2131297198);
        this.i = (LinearLayout)this.findViewById(2131296723);
        this.A = (TextView)this.findViewById(2131297199);
        this.d = (LinearLayout)this.findViewById(2131296793);
        this.v = (TextView)this.findViewById(2131297276);
        this.e = (LinearLayout)this.findViewById(2131296724);
        this.w = (TextView)this.findViewById(2131297200);
        this.f = (LinearLayout)this.findViewById(2131296725);
        this.x = (TextView)this.findViewById(2131297201);
        this.g = (LinearLayout)this.findViewById(2131296726);
        this.h = (LinearLayout)this.findViewById(2131296721);
        this.y = (TextView)this.findViewById(2131297202);
        this.z = (TextView)this.findViewById(2131297197);
        this.k = (LinearLayout)this.findViewById(2131296772);
        this.D = (TextView)this.findViewById(2131297249);
        this.l = (LinearLayout)this.findViewById(2131296775);
        this.F = (TextView)this.findViewById(2131297255);
        this.m = (LinearLayout)this.findViewById(2131296812);
        this.G = (TextView)this.findViewById(2131297304);
        this.n = (LinearLayout)this.findViewById(2131296732);
        this.H = (TextView)this.findViewById(2131297207);
        this.p = (LinearLayout)this.findViewById(2131296806);
        this.I = (TextView)this.findViewById(2131297295);
        this.q = (LinearLayout)this.findViewById(2131296755);
        this.J = (TextView)this.findViewById(2131297230);
        this.t = (LinearLayout)this.findViewById(2131296786);
        this.K = (TextView)this.findViewById(2131297266);
    }
    
    public final void J(final boolean p) {
        if (p) {
            this.P = p;
        }
        this.H.setText((CharSequence)this.E(new String[] { this.O.getRollNoString(), this.O.getExamSetString() }));
        this.I.setText((CharSequence)this.E(new String[] { this.O.getNameString(), this.O.getExamNameString(), this.O.getDateString(), this.O.getReportCardString(), this.O.getClassString(), this.O.getGradeString(), this.O.getRankString(), this.O.getSubjectString(), this.O.getMarksString(), this.O.getPercentageString(), this.O.getCorrectAnswerString(), this.O.getIncorrectAnswerString(), this.O.getTotalMarksString(), this.O.getQueNoString(), this.O.getAttemptedString(), this.O.getCorrectString() }));
        this.v.setText((CharSequence)this.E(this.O.getRollDigitsLabels()));
        this.w.setText((CharSequence)this.E(this.O.getFiveOptionLabels()));
        this.x.setText((CharSequence)this.E(this.O.getSixOptionLabels()));
        this.y.setText((CharSequence)this.E(this.O.getEightOptionLabels()));
        this.z.setText((CharSequence)this.E(this.O.getTenOptionLabels()));
        this.A.setText((CharSequence)this.E(this.O.getFourOptionLabels()));
        this.C.setText((CharSequence)this.E(this.O.getThreeOptionLabels()));
        this.D.setText((CharSequence)this.F(this.O.getMatrixPrimary(), this.O.getMatrixSecondary()));
        this.F.setText((CharSequence)this.E(this.O.getNumericalOptionLabels()));
        this.G.setText((CharSequence)this.E(this.O.getTrueOrFalseLabel()));
        this.J.setText((CharSequence)this.E(this.O.getExamSetLabels()));
        this.K.setText((CharSequence)this.E(this.O.getQuestionNumberLabels()));
    }
    
    public final void K() {
        ((View)this.h).setOnClickListener((View$OnClickListener)new View$OnClickListener(this, new y01(this) {
            public final LabelsActivity a;
            
            @Override
            public void a(final Object o) {
                this.a.O.setTenOptionLabels((String[])o);
                this.a.J(true);
            }
        }) {
            public final y01 a;
            public final LabelsActivity b;
            
            public void onClick(final View view) {
                final LabelsActivity c = this.b.c;
                new ti0((Context)c, ((Context)c).getString(2131886444), this.a, this.b.O.getTenOptionLabels(), 1).show();
            }
        });
    }
    
    public final void L() {
        ((View)this.j).setOnClickListener((View$OnClickListener)new View$OnClickListener(this, new y01(this) {
            public final LabelsActivity a;
            
            @Override
            public void a(final Object o) {
                this.a.O.setThreeOptionLabels((String[])o);
                this.a.J(true);
            }
        }) {
            public final y01 a;
            public final LabelsActivity b;
            
            public void onClick(final View view) {
                final LabelsActivity c = this.b.c;
                new ti0((Context)c, ((Context)c).getString(2131886805), this.a, this.b.O.getThreeOptionLabels(), 1).show();
            }
        });
    }
    
    public final void M() {
        ((View)this.i).setOnClickListener((View$OnClickListener)new View$OnClickListener(this, new y01(this) {
            public final LabelsActivity a;
            
            @Override
            public void a(final Object o) {
                this.a.O.setFourOptionLabels((String[])o);
                this.a.J(true);
            }
        }) {
            public final y01 a;
            public final LabelsActivity b;
            
            public void onClick(final View view) {
                final LabelsActivity c = this.b.c;
                new ti0((Context)c, ((Context)c).getString(2131886806), this.a, this.b.O.getFourOptionLabels(), 1).show();
            }
        });
    }
    
    public final void N() {
        ((View)this.e).setOnClickListener((View$OnClickListener)new View$OnClickListener(this, new y01(this) {
            public final LabelsActivity a;
            
            @Override
            public void a(final Object o) {
                this.a.O.setFiveOptionLabels((String[])o);
                this.a.J(true);
            }
        }) {
            public final y01 a;
            public final LabelsActivity b;
            
            public void onClick(final View view) {
                final LabelsActivity c = this.b.c;
                new ti0((Context)c, ((Context)c).getString(2131886807), this.a, this.b.O.getFiveOptionLabels(), 1).show();
            }
        });
    }
    
    public final void O() {
        ((View)this.f).setOnClickListener((View$OnClickListener)new View$OnClickListener(this, new y01(this) {
            public final LabelsActivity a;
            
            @Override
            public void a(final Object o) {
                this.a.O.setSixOptionLabels((String[])o);
                this.a.J(true);
            }
        }) {
            public final y01 a;
            public final LabelsActivity b;
            
            public void onClick(final View view) {
                final LabelsActivity c = this.b.c;
                new ti0((Context)c, ((Context)c).getString(2131886808), this.a, this.b.O.getSixOptionLabels(), 1).show();
            }
        });
    }
    
    public final void P() {
        ((View)this.g).setOnClickListener((View$OnClickListener)new View$OnClickListener(this, new y01(this) {
            public final LabelsActivity a;
            
            @Override
            public void a(final Object o) {
                this.a.O.setEightOptionLabels((String[])o);
                this.a.J(true);
            }
        }) {
            public final y01 a;
            public final LabelsActivity b;
            
            public void onClick(final View view) {
                final LabelsActivity c = this.b.c;
                new ti0((Context)c, ((Context)c).getString(2131886809), this.a, this.b.O.getEightOptionLabels(), 1).show();
            }
        });
    }
    
    public final void Q() {
        ((View)this.n).setOnClickListener((View$OnClickListener)new View$OnClickListener(this, new y01(this) {
            public final LabelsActivity a;
            
            @Override
            public void a(final Object o) {
                final String[] array = (String[])o;
                this.a.O.setRollNoString(array[0]);
                this.a.O.setExamSetString(array[1]);
                this.a.J(true);
            }
        }) {
            public final y01 a;
            public final LabelsActivity b;
            
            public void onClick(final View view) {
                final String rollNoString = this.b.O.getRollNoString();
                final String examSetString = this.b.O.getExamSetString();
                final LabelsActivity c = this.b.c;
                new ti0((Context)c, ((Context)c).getString(2131886811), this.a, new String[] { rollNoString, examSetString }, 15).show();
            }
        });
    }
    
    public final void R() {
        ((View)this.q).setOnClickListener((View$OnClickListener)new View$OnClickListener(this, new y01(this) {
            public final LabelsActivity a;
            
            @Override
            public void a(final Object o) {
                this.a.O.setExamSetLabels((String[])o);
                this.a.J(true);
            }
        }) {
            public final y01 a;
            public final LabelsActivity b;
            
            public void onClick(final View view) {
                final LabelsActivity c = this.b.c;
                new ti0((Context)c, ((Context)c).getString(2131886298), this.a, this.b.O.getExamSetLabels(), 1).show();
            }
        });
    }
    
    public final void S() {
        ((View)this.k).setOnClickListener((View$OnClickListener)new View$OnClickListener(this, new y01(this) {
            public final LabelsActivity a;
            
            @Override
            public void a(final Object o) {
                final String[] array = (String[])o;
                if (array.length == 20) {
                    final String[] matrixPrimary = new String[10];
                    final String[] matrixSecondary = new String[10];
                    System.arraycopy(array, 0, matrixPrimary, 0, 10);
                    System.arraycopy(array, 10, matrixSecondary, 0, 10);
                    this.a.O.setMatrixPrimary(matrixPrimary);
                    this.a.O.setMatrixSecondary(matrixSecondary);
                    this.a.J(true);
                }
            }
        }) {
            public final y01 a;
            public final LabelsActivity b;
            
            public void onClick(final View view) {
                final LabelsActivity c = this.b.c;
                final String string = ((Context)c).getString(2131886817);
                final y01 a = this.a;
                final LabelsActivity b = this.b;
                new ti0((Context)c, string, a, b.D(b.O.getMatrixPrimary(), this.b.O.getMatrixSecondary()), 1).show();
            }
        });
    }
    
    public final void T() {
        ((View)this.l).setOnClickListener((View$OnClickListener)new View$OnClickListener(this, new y01(this) {
            public final LabelsActivity a;
            
            @Override
            public void a(final Object o) {
                this.a.O.setNumericalOptionLabels((String[])o);
                this.a.J(true);
            }
        }) {
            public final y01 a;
            public final LabelsActivity b;
            
            public void onClick(final View view) {
                final LabelsActivity c = this.b.c;
                new ti0((Context)c, ((Context)c).getString(2131886818), this.a, this.b.O.getNumericalOptionLabels(), 2).show();
            }
        });
    }
    
    public final void U() {
        ((View)this.t).setOnClickListener((View$OnClickListener)new View$OnClickListener(this, new y01(this) {
            public final LabelsActivity a;
            
            @Override
            public void a(final Object o) {
                this.a.O.setQuestionNumberLabels((String[])o);
                this.a.J(true);
            }
        }) {
            public final y01 a;
            public final LabelsActivity b;
            
            public void onClick(final View view) {
                final LabelsActivity c = this.b.c;
                new ea1((Context)c, ((Context)c).getString(2131886819), this.a, this.b.O.getQuestionNumberLabels(), 3).show();
            }
        });
    }
    
    public final void V() {
        ((View)this.p).setOnClickListener((View$OnClickListener)new View$OnClickListener(this, new y01(this) {
            public final LabelsActivity a;
            
            @Override
            public void a(final Object o) {
                final String[] array = (String[])o;
                this.a.O.setNameString(array[0]);
                this.a.O.setExamNameString(array[1]);
                this.a.O.setDateString(array[2]);
                this.a.O.setReportCardString(array[3]);
                this.a.O.setClassString(array[4]);
                this.a.O.setGradeString(array[5]);
                this.a.O.setRankString(array[6]);
                this.a.O.setSubjectString(array[7]);
                this.a.O.setMarksString(array[8]);
                this.a.O.setPercentageString(array[9]);
                this.a.O.setCorrectAnswerString(array[10]);
                this.a.O.setIncorrectAnswerString(array[11]);
                this.a.O.setTotalMarksString(array[12]);
                this.a.O.setQueNoString(array[13]);
                this.a.O.setAttemptedString(array[14]);
                this.a.O.setCorrectString(array[15]);
                this.a.J(true);
            }
        }) {
            public final y01 a;
            public final LabelsActivity b;
            
            public void onClick(final View view) {
                final String nameString = this.b.O.getNameString();
                final String examNameString = this.b.O.getExamNameString();
                final String dateString = this.b.O.getDateString();
                final String reportCardString = this.b.O.getReportCardString();
                final String classString = this.b.O.getClassString();
                final String gradeString = this.b.O.getGradeString();
                final String rankString = this.b.O.getRankString();
                final String subjectString = this.b.O.getSubjectString();
                final String marksString = this.b.O.getMarksString();
                final String percentageString = this.b.O.getPercentageString();
                final String correctAnswerString = this.b.O.getCorrectAnswerString();
                final String incorrectAnswerString = this.b.O.getIncorrectAnswerString();
                final String totalMarksString = this.b.O.getTotalMarksString();
                final String queNoString = this.b.O.getQueNoString();
                final String attemptedString = this.b.O.getAttemptedString();
                final String correctString = this.b.O.getCorrectString();
                final LabelsActivity c = this.b.c;
                new ti0((Context)c, ((Context)c).getString(2131886820), this.a, new String[] { nameString, examNameString, dateString, reportCardString, classString, gradeString, rankString, subjectString, marksString, percentageString, correctAnswerString, incorrectAnswerString, totalMarksString, queNoString, attemptedString, correctString }, 15).show();
            }
        });
    }
    
    public final void W() {
        ((View)this.d).setOnClickListener((View$OnClickListener)new View$OnClickListener(this, new mf1.d(this) {
            public final LabelsActivity a;
            
            @Override
            public void a(final int rollStartDigit, final String[] rollDigitsLabels) {
                this.a.O.setRollDigitsLabels(rollDigitsLabels);
                this.a.O.setRollStartDigit(rollStartDigit);
                this.a.J(true);
            }
        }) {
            public final mf1.d a;
            public final LabelsActivity b;
            
            public void onClick(final View view) {
                final LabelsActivity c = this.b.c;
                new mf1((Context)c, ((Context)c).getString(2131886821), this.a, this.b.O.getRollDigitsLabels(), this.b.O.getRollStartDigit(), 1).show();
            }
        });
    }
    
    public final void X() {
        ((View)this.m).setOnClickListener((View$OnClickListener)new View$OnClickListener(this, new y01(this) {
            public final LabelsActivity a;
            
            @Override
            public void a(final Object o) {
                this.a.O.setTrueOrFalseLabel((String[])o);
                this.a.J(true);
            }
        }) {
            public final y01 a;
            public final LabelsActivity b;
            
            public void onClick(final View view) {
                final LabelsActivity c = this.b.c;
                new ti0((Context)c, ((Context)c).getString(2131886822), this.a, this.b.O.getTrueOrFalseLabel(), 5).show();
            }
        });
    }
    
    public final void Y() {
        this.x(this.Q = (Toolbar)this.findViewById(2131297340));
        this.Q.setNavigationOnClickListener((View$OnClickListener)new View$OnClickListener(this) {
            public final LabelsActivity a;
            
            public void onClick(final View view) {
                this.a.finish();
            }
        });
    }
    
    public final void Z(final String s) {
        xs.c((Context)this.c, new y01(this, s) {
            public final String a;
            public final LabelsActivity b;
            
            @Override
            public void a(final Object o) {
                if (TemplateRepository.getInstance((Context)this.b.c).deleteLabelProfileJson(this.a)) {
                    final LabelsActivity c = this.b.c;
                    final StringBuilder sb = new StringBuilder();
                    sb.append(this.a);
                    sb.append(" ");
                    sb.append(this.b.c.getResources().getString(2131886373));
                    a91.H((Context)c, sb.toString(), 2131230927, 2131231085);
                }
                this.b.c.finish();
            }
        }, 2131886129, 2131886479, 2131886903, 2131886657, 0, 2131230930);
    }
    
    public final void a0() {
        xs.b((Context)this.c, 0, 2131886547, 2131886903, 2131886657, 0, 0, new y01(this) {
            public final LabelsActivity a;
            
            @Override
            public void a(final Object o) {
                new ui1((Context)this.a.c, new y01(this) {
                    public final LabelsActivity$z a;
                    
                    @Override
                    public void a(final Object o) {
                        this.a.a.finish();
                    }
                }, this.a.O);
            }
        }, new y01(this) {
            public final LabelsActivity a;
            
            @Override
            public void a(final Object o) {
                this.a.c.finish();
            }
        });
    }
    
    @Override
    public void onBackPressed() {
        if (this.P) {
            this.a0();
        }
        else {
            super.onBackPressed();
        }
    }
    
    @Override
    public void onCreate(Bundle extras) {
        super.onCreate(extras);
        this.setContentView(2131492913);
        this.Y();
        this.I();
        extras = this.getIntent().getExtras();
        if (extras != null && extras.getSerializable("LABEL_PROFILE") != null) {
            this.O = (LabelProfile)extras.getSerializable("LABEL_PROFILE");
        }
        final LabelProfile o = this.O;
        if (o == null) {
            this.O = new LabelProfile();
            ((View)this.M).setVisibility(8);
            this.Q.setTitle(2131886375);
        }
        else {
            this.Q.setTitle(o.getLabelProfileName());
        }
        this.Q();
        this.N();
        this.W();
        this.M();
        this.L();
        this.S();
        this.T();
        this.X();
        this.O();
        this.P();
        this.K();
        this.V();
        this.R();
        this.U();
        this.J(false);
        this.H();
        this.G();
    }
}

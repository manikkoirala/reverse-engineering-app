// 
// Decompiled by Procyon v0.6.0
// 

package com.ekodroid.omrevaluator.templateui.createtemplate;

import android.os.Bundle;
import androidx.appcompat.widget.Toolbar;
import java.util.Iterator;
import android.widget.ListAdapter;
import com.ekodroid.omrevaluator.database.repositories.TemplateRepository;
import java.io.Serializable;
import android.widget.AdapterView;
import android.widget.AdapterView$OnItemClickListener;
import android.content.Context;
import android.content.Intent;
import android.view.View;
import android.view.View$OnClickListener;
import com.ekodroid.omrevaluator.database.LabelProfileJsonModel;
import java.util.ArrayList;
import android.widget.Button;
import android.widget.ListView;

public class TemplateLabelsActivity extends v5
{
    public TemplateLabelsActivity c;
    public ListView d;
    public Button e;
    public j3 f;
    public ArrayList g;
    
    public TemplateLabelsActivity() {
        this.c = this;
    }
    
    public static /* synthetic */ ArrayList A(final TemplateLabelsActivity templateLabelsActivity) {
        return templateLabelsActivity.g;
    }
    
    public final void D() {
        ((View)this.e).setOnClickListener((View$OnClickListener)new View$OnClickListener(this) {
            public final TemplateLabelsActivity a;
            
            public void onClick(final View view) {
                ((Context)this.a).startActivity(new Intent((Context)this.a.c, (Class)LabelsActivity.class));
            }
        });
    }
    
    public final void E() {
        ((AdapterView)this.d).setOnItemClickListener((AdapterView$OnItemClickListener)new AdapterView$OnItemClickListener(this) {
            public final TemplateLabelsActivity a;
            
            public void onItemClick(final AdapterView adapterView, final View view, final int n, final long n2) {
                if (view.getId() == 2131296656L) {
                    final TemplateLabelsActivity a = this.a;
                    a.H((LabelProfileJsonModel)TemplateLabelsActivity.A(a).get(n));
                }
                else {
                    view.setBackgroundResource(2131230860);
                    final Intent intent = new Intent((Context)this.a.c, (Class)LabelsActivity.class);
                    intent.putExtra("LABEL_PROFILE", (Serializable)TemplateLabelsActivity.A(this.a).get(n).getLabelProfile());
                    ((Context)this.a).startActivity(intent);
                }
            }
        });
    }
    
    public final void F() {
        this.g = TemplateRepository.getInstance((Context)this.c).getAllLabelProfileJson();
        final ArrayList list = new ArrayList();
        final Iterator iterator = this.g.iterator();
        while (iterator.hasNext()) {
            list.add(((LabelProfileJsonModel)iterator.next()).getProfileName());
        }
        final j3 j3 = new j3((Context)this.c, list);
        this.f = j3;
        this.d.setAdapter((ListAdapter)j3);
    }
    
    public final void G() {
        final Toolbar toolbar = (Toolbar)this.findViewById(2131297350);
        this.x(toolbar);
        toolbar.setNavigationOnClickListener((View$OnClickListener)new View$OnClickListener(this) {
            public final TemplateLabelsActivity a;
            
            public void onClick(final View view) {
                this.a.finish();
            }
        });
    }
    
    public final void H(final LabelProfileJsonModel labelProfileJsonModel) {
        xs.c((Context)this.c, new y01(this, labelProfileJsonModel) {
            public final LabelProfileJsonModel a;
            public final TemplateLabelsActivity b;
            
            @Override
            public void a(final Object o) {
                TemplateRepository.getInstance((Context)this.b.c).deleteLabelProfileJson(this.a.getProfileName());
                a91.G((Context)this.b.c, 2131886373, 2131230927, 2131231085);
                this.b.F();
            }
        }, 2131886129, 2131886479, 2131886903, 2131886657, 0, 2131230930);
    }
    
    @Override
    public void onCreate(final Bundle bundle) {
        super.onCreate(bundle);
        this.setContentView(2131492933);
        this.G();
        this.e = (Button)this.findViewById(2131296444);
        this.d = (ListView)this.findViewById(2131296841);
        this.E();
        this.D();
    }
    
    @Override
    public void onResume() {
        super.onResume();
        this.F();
    }
}

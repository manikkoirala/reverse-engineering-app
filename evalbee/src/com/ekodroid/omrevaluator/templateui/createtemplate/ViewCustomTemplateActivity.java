// 
// Decompiled by Procyon v0.6.0
// 

package com.ekodroid.omrevaluator.templateui.createtemplate;

import android.os.Bundle;
import com.google.firebase.analytics.FirebaseAnalytics;
import java.util.List;
import android.widget.ArrayAdapter;
import com.ekodroid.omrevaluator.database.LabelProfileJsonModel;
import java.util.Iterator;
import android.widget.AdapterView;
import android.widget.AdapterView$OnItemSelectedListener;
import android.widget.SpinnerAdapter;
import com.ekodroid.omrevaluator.database.TemplateHeaderJsonDataModel;
import com.ekodroid.omrevaluator.database.repositories.TemplateRepository;
import java.util.ArrayList;
import android.view.View;
import android.view.View$OnClickListener;
import com.ekodroid.omrevaluator.activities.HomeBottomNavActivity;
import java.io.Serializable;
import android.content.Intent;
import android.content.Context;
import com.ekodroid.omrevaluator.templateui.models.a;
import com.ekodroid.omrevaluator.templateui.models.ExamId;
import com.ekodroid.omrevaluator.templateui.models.HeaderProfile;
import com.ekodroid.omrevaluator.templateui.models.LabelProfile;
import android.widget.ImageButton;
import com.ekodroid.omrevaluator.templateui.components.ZoomableImageView;
import android.widget.Button;
import android.widget.Spinner;
import com.ekodroid.omrevaluator.templateui.models.SheetTemplate2;

public class ViewCustomTemplateActivity extends v5
{
    public ViewCustomTemplateActivity c;
    public SheetTemplate2 d;
    public Spinner e;
    public Spinner f;
    public Button g;
    public Button h;
    public ZoomableImageView i;
    public ImageButton j;
    public ImageButton k;
    public LabelProfile l;
    public HeaderProfile m;
    
    public ViewCustomTemplateActivity() {
        this.c = this;
    }
    
    public final void F(final LabelProfile labelProfile, final HeaderProfile headerProfile) {
        final a a = new a();
        this.d.setLabelProfile(labelProfile);
        this.d.setHeaderProfile(headerProfile);
        this.i.setImageBitmap(a.X((Context)this.c, this.d, 0));
    }
    
    public final void G(final HeaderProfile headerProfile) {
        final Intent intent = new Intent((Context)this.c, (Class)CustomHeaderActivity.class);
        intent.putExtra("HEADER_PROFILE", (Serializable)headerProfile);
        ((Context)this).startActivity(intent);
    }
    
    public final void H(final ExamId examId) {
        final Intent intent = new Intent((Context)this.c, (Class)HomeBottomNavActivity.class);
        intent.addFlags(335544320);
        ((Context)this).startActivity(intent);
    }
    
    public final void I(final LabelProfile labelProfile) {
        final Intent intent = new Intent((Context)this.c, (Class)LabelsActivity.class);
        intent.putExtra("LABEL_PROFILE", (Serializable)labelProfile);
        ((Context)this).startActivity(intent);
    }
    
    public final void J() {
        ((View)this.h).setEnabled(true);
        ((View)this.h).setOnClickListener((View$OnClickListener)new View$OnClickListener(this) {
            public final ViewCustomTemplateActivity a;
            
            public void onClick(final View view) {
                this.a.P();
            }
        });
    }
    
    public final void K() {
        ((View)this.k).setOnClickListener((View$OnClickListener)new View$OnClickListener(this) {
            public final ViewCustomTemplateActivity a;
            
            public void onClick(final View view) {
                final ViewCustomTemplateActivity a = this.a;
                a.G(a.m);
            }
        });
    }
    
    public final void L() {
        ((View)this.j).setOnClickListener((View$OnClickListener)new View$OnClickListener(this) {
            public final ViewCustomTemplateActivity a;
            
            public void onClick(final View view) {
                final ViewCustomTemplateActivity a = this.a;
                a.I(a.l);
            }
        });
    }
    
    public final void M() {
        final ArrayList list = new ArrayList();
        list.add(ok.a);
        list.add("Default Medium");
        list.add(ok.b);
        list.add("Blank");
        list.add("None");
        final Iterator<TemplateHeaderJsonDataModel> iterator = TemplateRepository.getInstance((Context)this.c).getAllHeaderProfileJson().iterator();
        while (iterator.hasNext()) {
            list.add(iterator.next().getHeaderName());
        }
        this.f.setAdapter((SpinnerAdapter)new p3((Context)this.c, list));
        ((AdapterView)this.f).setOnItemSelectedListener((AdapterView$OnItemSelectedListener)new AdapterView$OnItemSelectedListener(this, list) {
            public final ArrayList a;
            public final ViewCustomTemplateActivity b;
            
            public void onItemSelected(final AdapterView adapterView, final View view, final int index, final long n) {
                HeaderProfile m = null;
                if (index == 2) {
                    this.b.G(null);
                    return;
                }
                ViewCustomTemplateActivity viewCustomTemplateActivity;
                if (index > 4) {
                    viewCustomTemplateActivity = this.b;
                    m = TemplateRepository.getInstance((Context)viewCustomTemplateActivity.c).getHeaderProfileJsonModel(this.a.get(index)).getHeaderProfile();
                }
                else if (index == 0) {
                    viewCustomTemplateActivity = this.b;
                    m = HeaderProfile.getDefaultHeaderprofile();
                }
                else if (index == 1) {
                    viewCustomTemplateActivity = this.b;
                    m = HeaderProfile.getDefaultMediumHeaderprofile();
                }
                else if (index == 3) {
                    viewCustomTemplateActivity = this.b;
                    m = HeaderProfile.getBlankHeaderprofile();
                }
                else {
                    viewCustomTemplateActivity = this.b;
                }
                viewCustomTemplateActivity.m = m;
                final ViewCustomTemplateActivity b = this.b;
                b.F(b.l, b.m);
            }
            
            public void onNothingSelected(final AdapterView adapterView) {
            }
        });
        ((AdapterView)this.f).setSelection(0);
    }
    
    public final void N() {
        final ArrayList<LabelProfileJsonModel> allLabelProfileJson = TemplateRepository.getInstance((Context)this.c).getAllLabelProfileJson();
        final ArrayList list = new ArrayList();
        list.add(ok.a);
        list.add(ok.b);
        final Iterator<LabelProfileJsonModel> iterator = allLabelProfileJson.iterator();
        while (iterator.hasNext()) {
            list.add(iterator.next().getProfileName());
        }
        this.e.setAdapter((SpinnerAdapter)new ArrayAdapter((Context)this, 2131493104, (List)list));
        ((AdapterView)this.e).setOnItemSelectedListener((AdapterView$OnItemSelectedListener)new AdapterView$OnItemSelectedListener(this, list) {
            public final ArrayList a;
            public final ViewCustomTemplateActivity b;
            
            public void onItemSelected(final AdapterView adapterView, final View view, final int index, final long n) {
                if (index == 0) {
                    final ViewCustomTemplateActivity b = this.b;
                    b.F(b.l = null, b.m);
                    return;
                }
                if (index == 1) {
                    this.b.I(null);
                    return;
                }
                this.b.l = TemplateRepository.getInstance((Context)this.b.c).getLabelProfileJsonModel(this.a.get(index)).getLabelProfile();
                final ViewCustomTemplateActivity b2 = this.b;
                b2.F(b2.l, b2.m);
            }
            
            public void onNothingSelected(final AdapterView adapterView) {
            }
        });
        ((AdapterView)this.e).setSelection(0);
    }
    
    public final void O() {
        this.N();
        this.M();
    }
    
    public final void P() {
        new ti1((Context)this.c, new y01(this) {
            public final ViewCustomTemplateActivity a;
            
            @Override
            public void a(final Object o) {
                final ExamId examId = (ExamId)o;
                FirebaseAnalytics.getInstance((Context)this.a.c).a("ExamSaveCustom", null);
                this.a.H(examId);
            }
        }, this.d, null).show();
    }
    
    @Override
    public void onCreate(final Bundle bundle) {
        super.onCreate(bundle);
        this.setContentView(2131492935);
        if (this.getIntent().getExtras() != null) {
            final SheetTemplate2 d = (SheetTemplate2)this.getIntent().getSerializableExtra("SHEET_TEMPLATE");
            this.d = d;
            this.l = d.getLabelProfile();
        }
        this.e = (Spinner)this.findViewById(2131297086);
        this.f = (Spinner)this.findViewById(2131297082);
        this.k = (ImageButton)this.findViewById(2131296658);
        this.j = (ImageButton)this.findViewById(2131296659);
        this.i = (ZoomableImageView)this.findViewById(2131297421);
        this.g = (Button)this.findViewById(2131296458);
        this.h = (Button)this.findViewById(2131296460);
        ((View)this.g).setOnClickListener((View$OnClickListener)new View$OnClickListener(this) {
            public final ViewCustomTemplateActivity a;
            
            public void onClick(final View view) {
                this.a.c.finish();
            }
        });
        this.J();
        this.L();
        this.K();
    }
    
    @Override
    public void onResume() {
        super.onResume();
        this.O();
    }
}

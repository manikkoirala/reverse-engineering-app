// 
// Decompiled by Procyon v0.6.0
// 

package com.ekodroid.omrevaluator.templateui.createtemplate;

import android.widget.AdapterView;
import android.view.ViewGroup$MarginLayoutParams;
import androidx.appcompat.widget.Toolbar;
import android.os.Bundle;
import android.content.DialogInterface;
import android.content.DialogInterface$OnClickListener;
import android.view.LayoutInflater;
import com.google.android.material.dialog.MaterialAlertDialogBuilder;
import android.widget.CompoundButton;
import android.widget.CompoundButton$OnCheckedChangeListener;
import android.view.View$OnClickListener;
import com.ekodroid.omrevaluator.database.TemplateHeaderJsonDataModel;
import com.ekodroid.omrevaluator.database.repositories.TemplateRepository;
import android.view.ViewGroup;
import android.content.res.Resources$Theme;
import android.widget.SpinnerAdapter;
import android.widget.Spinner;
import android.text.InputFilter$LengthFilter;
import android.text.InputFilter;
import android.view.View;
import android.view.ViewGroup$LayoutParams;
import android.widget.LinearLayout$LayoutParams;
import android.content.Context;
import com.ekodroid.omrevaluator.templateui.models.HeaderLabel;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import com.google.android.material.textfield.TextInputLayout;
import androidx.appcompat.widget.SwitchCompat;
import java.util.ArrayList;
import android.widget.LinearLayout;
import com.ekodroid.omrevaluator.templateui.models.HeaderProfile;
import android.widget.TextView;
import android.widget.ImageButton;

public class CustomHeaderActivity extends v5
{
    public CustomHeaderActivity c;
    public ImageButton d;
    public ImageButton e;
    public TextView f;
    public int g;
    public HeaderProfile h;
    public LinearLayout i;
    public ArrayList j;
    public SwitchCompat k;
    public SwitchCompat l;
    public TextInputLayout m;
    public EditText n;
    public EditText p;
    public ArrayAdapter q;
    
    public CustomHeaderActivity() {
        this.c = this;
        this.g = 1;
        this.h = null;
        this.j = new ArrayList();
    }
    
    public final void E(final int n) {
        int n2;
        for (int i = this.j.size(); i < n; i = n2) {
            this.j.add(new LinearLayout((Context)this));
            ((LinearLayout)this.j.get(i)).setOrientation(0);
            ((LinearLayout)this.j.get(i)).setWeightSum(5.0f);
            final LinearLayout$LayoutParams layoutParams = new LinearLayout$LayoutParams(-1, -2);
            ((ViewGroup$MarginLayoutParams)layoutParams).setMargins(2, 0, 2, 0);
            ((View)this.j.get(i)).setPadding(this.F(12, (Context)this), this.F(12, (Context)this), this.F(12, (Context)this), this.F(12, (Context)this));
            ((View)this.j.get(i)).setBackgroundColor(this.getResources().getColor(2131099740));
            ((View)this.j.get(i)).setLayoutParams((ViewGroup$LayoutParams)layoutParams);
            final TextView textView = new TextView((Context)this);
            final StringBuilder sb = new StringBuilder();
            sb.append(" ");
            n2 = i + 1;
            sb.append(n2);
            textView.setText((CharSequence)sb.toString());
            textView.setTextAppearance(2131952183);
            final LinearLayout$LayoutParams linearLayout$LayoutParams = new LinearLayout$LayoutParams(this.F(30, (Context)this), -2);
            ((ViewGroup$MarginLayoutParams)linearLayout$LayoutParams).setMargins(0, 0, this.F(16, (Context)this), 0);
            ((ViewGroup)this.j.get(i)).addView((View)textView, 0, (ViewGroup$LayoutParams)linearLayout$LayoutParams);
            final EditText editText = new EditText((Context)this);
            final StringBuilder sb2 = new StringBuilder();
            sb2.append(((Context)this).getString(2131886372));
            sb2.append(" ");
            sb2.append(n2);
            ((TextView)editText).setHint((CharSequence)sb2.toString());
            ((TextView)editText).setInputType(1);
            ((TextView)editText).setTextAppearance(2131952183);
            ((View)editText).setBackgroundResource(2131230848);
            ((TextView)editText).setMaxLines(1);
            ((TextView)editText).setFilters(new InputFilter[] { (InputFilter)new InputFilter$LengthFilter(15) });
            editText.setSelection(((CharSequence)editText.getText()).length());
            ((View)editText).setPadding(this.F(12, (Context)this), 0, this.F(12, (Context)this), 0);
            final LinearLayout$LayoutParams linearLayout$LayoutParams2 = new LinearLayout$LayoutParams(-2, this.F(40, (Context)this));
            ((ViewGroup$MarginLayoutParams)linearLayout$LayoutParams2).setMargins(0, 0, this.F(16, (Context)this), 0);
            linearLayout$LayoutParams2.weight = 5.0f;
            ((ViewGroup)this.j.get(i)).addView((View)editText, 1, (ViewGroup$LayoutParams)linearLayout$LayoutParams2);
            final Spinner spinner = new Spinner((Context)this);
            spinner.setAdapter((SpinnerAdapter)this.q);
            spinner.setGravity(17);
            ((View)spinner).setBackgroundResource(2131230845);
            ((ViewGroup)this.j.get(i)).addView((View)spinner, 2, (ViewGroup$LayoutParams)new LinearLayout$LayoutParams(this.F(130, (Context)this), this.F(40, (Context)this)));
            ((ViewGroup)this.i).addView((View)this.j.get(i));
        }
    }
    
    public final int F(final int n, final Context context) {
        return (int)(n * context.getResources().getDisplayMetrics().density);
    }
    
    public final HeaderLabel.Size G(final int n) {
        if (n == 0) {
            return HeaderLabel.Size.SMALL;
        }
        if (n == 1) {
            return HeaderLabel.Size.MEDIUM;
        }
        if (n == 2) {
            return HeaderLabel.Size.LARGE;
        }
        if (n != 3) {
            return HeaderLabel.Size.SMALL;
        }
        return HeaderLabel.Size.FULLWIDTH;
    }
    
    public final int H(final HeaderLabel.Size size) {
        final int n = CustomHeaderActivity$i.a[size.ordinal()];
        if (n == 2) {
            return 1;
        }
        if (n == 3) {
            return 2;
        }
        if (n != 4) {
            return 0;
        }
        return 3;
    }
    
    public final void I() {
        this.k = (SwitchCompat)this.findViewById(2131297143);
        this.l = (SwitchCompat)this.findViewById(2131297142);
        this.n = (EditText)this.findViewById(2131296593);
        this.p = (EditText)this.findViewById(2131296581);
        final TextView f = (TextView)this.findViewById(2131297240);
        this.f = f;
        final StringBuilder sb = new StringBuilder();
        sb.append("");
        sb.append(this.g);
        f.setText((CharSequence)sb.toString());
        this.e = (ImageButton)this.findViewById(2131296678);
        this.d = (ImageButton)this.findViewById(2131296651);
        this.i = (LinearLayout)this.findViewById(2131296719);
        this.q = new ArrayAdapter((Context)this.c, 2131493104, (Object[])gr1.g);
        this.Q();
        final HeaderProfile h = this.h;
        if (h != null) {
            if (h.getInstruction() != null) {
                this.l.setChecked(true);
                ((TextView)this.p).setText((CharSequence)this.h.getInstruction());
            }
            if (this.h.getTitle() != null) {
                this.k.setChecked(true);
                ((TextView)this.n).setText((CharSequence)this.h.getTitle());
            }
            final ArrayList<HeaderLabel> labels = this.h.getLabels();
            int n;
            for (int i = 0; i < labels.size(); i = n) {
                final LinearLayout e = new LinearLayout((Context)this);
                e.setOrientation(0);
                e.setWeightSum(5.0f);
                final LinearLayout$LayoutParams layoutParams = new LinearLayout$LayoutParams(-1, -2);
                ((ViewGroup$MarginLayoutParams)layoutParams).setMargins(2, 0, 2, 0);
                ((View)e).setPadding(this.F(12, (Context)this), this.F(12, (Context)this), this.F(12, (Context)this), this.F(12, (Context)this));
                ((View)e).setBackgroundColor(this.getResources().getColor(2131099740, (Resources$Theme)null));
                ((View)e).setLayoutParams((ViewGroup$LayoutParams)layoutParams);
                final TextView textView = new TextView((Context)this);
                final StringBuilder sb2 = new StringBuilder();
                sb2.append(" ");
                n = i + 1;
                sb2.append(n);
                textView.setText((CharSequence)sb2.toString());
                textView.setTextAppearance(2131952183);
                final LinearLayout$LayoutParams linearLayout$LayoutParams = new LinearLayout$LayoutParams(this.F(30, (Context)this), -2);
                ((ViewGroup$MarginLayoutParams)linearLayout$LayoutParams).setMargins(0, 0, this.F(16, (Context)this), 0);
                ((ViewGroup)e).addView((View)textView, 0, (ViewGroup$LayoutParams)linearLayout$LayoutParams);
                final EditText editText = new EditText((Context)this);
                final StringBuilder sb3 = new StringBuilder();
                sb3.append(((Context)this).getString(2131886372));
                sb3.append(" ");
                sb3.append(n);
                ((TextView)editText).setHint((CharSequence)sb3.toString());
                ((TextView)editText).setInputType(1);
                ((TextView)editText).setTextAppearance(2131952183);
                ((View)editText).setBackground(g7.b((Context)this.c, 2131230848));
                ((TextView)editText).setMaxLines(1);
                ((TextView)editText).setFilters(new InputFilter[] { (InputFilter)new InputFilter$LengthFilter(15) });
                editText.setSelection(((CharSequence)editText.getText()).length());
                ((View)editText).setPadding(this.F(12, (Context)this), 0, this.F(12, (Context)this), 0);
                ((TextView)editText).setText((CharSequence)labels.get(i).label);
                final LinearLayout$LayoutParams linearLayout$LayoutParams2 = new LinearLayout$LayoutParams(-2, this.F(40, (Context)this));
                ((ViewGroup$MarginLayoutParams)linearLayout$LayoutParams2).setMargins(0, 0, this.F(16, (Context)this), 0);
                linearLayout$LayoutParams2.weight = 5.0f;
                ((ViewGroup)e).addView((View)editText, 1, (ViewGroup$LayoutParams)linearLayout$LayoutParams2);
                final Spinner spinner = new Spinner((Context)this);
                spinner.setAdapter((SpinnerAdapter)this.q);
                spinner.setGravity(17);
                ((View)spinner).setBackground(g7.b((Context)this.c, 2131230845));
                ((AdapterView)spinner).setSelection(this.H(labels.get(i).size));
                ((ViewGroup)e).addView((View)spinner, 2, (ViewGroup$LayoutParams)new LinearLayout$LayoutParams(this.F(130, (Context)this), this.F(40, (Context)this)));
                this.j.add(e);
                ((ViewGroup)this.i).addView((View)e);
            }
        }
    }
    
    public final void J(final int n) {
        if (n > this.j.size()) {
            this.E(n);
        }
        if (n < this.j.size()) {
            this.K(n);
        }
    }
    
    public final void K(final int n) {
        for (int i = this.j.size() - 1; i >= n; --i) {
            ((ViewGroup)this.i).removeView((View)this.j.get(i));
            this.j.remove(i);
        }
    }
    
    public final boolean L(final HeaderProfile headerProfile) {
        TemplateRepository.getInstance((Context)this.c).deleteHeaderProfileJson(headerProfile.getHeaderProfileName());
        return TemplateRepository.getInstance((Context)this.c).saveOrUpdateHeaderProfileJson(new TemplateHeaderJsonDataModel(headerProfile.getHeaderProfileName(), headerProfile));
    }
    
    public final void M() {
        ((View)this.e).setOnClickListener((View$OnClickListener)new View$OnClickListener(this) {
            public final CustomHeaderActivity a;
            
            public void onClick(final View view) {
                final CustomHeaderActivity a = this.a;
                final int g = a.g;
                if (g > 1) {
                    a.g = g - 1;
                    final TextView f = a.f;
                    final StringBuilder sb = new StringBuilder();
                    sb.append(this.a.g);
                    sb.append("");
                    f.setText((CharSequence)sb.toString());
                    final CustomHeaderActivity a2 = this.a;
                    a2.J(a2.g);
                }
            }
        });
    }
    
    public final void N() {
        ((View)this.d).setOnClickListener((View$OnClickListener)new View$OnClickListener(this) {
            public final CustomHeaderActivity a;
            
            public void onClick(final View view) {
                final CustomHeaderActivity a = this.a;
                final int g = a.g;
                if (g < 10) {
                    a.g = g + 1;
                    final TextView f = a.f;
                    final StringBuilder sb = new StringBuilder();
                    sb.append(this.a.g);
                    sb.append("");
                    f.setText((CharSequence)sb.toString());
                    final CustomHeaderActivity a2 = this.a;
                    a2.J(a2.g);
                }
            }
        });
    }
    
    public final void O() {
        this.findViewById(2131296426).setOnClickListener((View$OnClickListener)new View$OnClickListener(this) {
            public final CustomHeaderActivity a;
            
            public void onClick(final View view) {
                this.a.c.finish();
            }
        });
    }
    
    public final void P() {
        this.findViewById(2131296461).setOnClickListener((View$OnClickListener)new View$OnClickListener(this) {
            public final CustomHeaderActivity a;
            
            public void onClick(final View view) {
                if (this.a.k.isChecked() && this.a.n.getText().toString().trim().equals("")) {
                    a91.G((Context)this.a.c, 2131886278, 2131230909, 2131231086);
                    return;
                }
                if (this.a.l.isChecked() && this.a.p.getText().toString().trim().equals("")) {
                    a91.G((Context)this.a.c, 2131886272, 2131230909, 2131231086);
                    return;
                }
                final ArrayList list = new ArrayList();
                for (int i = 0; i < this.a.j.size(); ++i) {
                    final EditText editText = (EditText)((ViewGroup)this.a.j.get(i)).getChildAt(1);
                    final Spinner spinner = (Spinner)((ViewGroup)this.a.j.get(i)).getChildAt(2);
                    if (editText.getText().toString().trim().equals("")) {
                        a91.G((Context)this.a.c, 2131886273, 2131230909, 2131231086);
                        return;
                    }
                    list.add(new HeaderLabel(this.a.G(((AdapterView)spinner).getSelectedItemPosition()), editText.getText().toString()));
                }
                this.a.R(list);
            }
        });
    }
    
    public final void Q() {
        this.k.setOnCheckedChangeListener((CompoundButton$OnCheckedChangeListener)new CompoundButton$OnCheckedChangeListener(this) {
            public final CustomHeaderActivity a;
            
            public void onCheckedChanged(final CompoundButton compoundButton, final boolean b) {
                final EditText n = this.a.n;
                int visibility;
                if (b) {
                    visibility = 0;
                }
                else {
                    visibility = 8;
                }
                ((View)n).setVisibility(visibility);
            }
        });
        this.l.setOnCheckedChangeListener((CompoundButton$OnCheckedChangeListener)new CompoundButton$OnCheckedChangeListener(this) {
            public final CustomHeaderActivity a;
            
            public void onCheckedChanged(final CompoundButton compoundButton, final boolean b) {
                final EditText p2 = this.a.p;
                int visibility;
                if (b) {
                    visibility = 0;
                }
                else {
                    visibility = 8;
                }
                ((View)p2).setVisibility(visibility);
            }
        });
    }
    
    public final void R(final ArrayList list) {
        final MaterialAlertDialogBuilder materialAlertDialogBuilder = new MaterialAlertDialogBuilder((Context)this.c, 2131951953);
        final View inflate = ((LayoutInflater)((Context)this).getSystemService("layout_inflater")).inflate(2131492986, (ViewGroup)null);
        materialAlertDialogBuilder.setView(inflate);
        materialAlertDialogBuilder.setTitle(2131886761);
        final EditText editText = (EditText)inflate.findViewById(2131296595);
        final HeaderProfile h = this.h;
        if (h != null && !h.getHeaderProfileName().toUpperCase().equals("DEFAULT")) {
            ((TextView)editText).setText((CharSequence)this.h.getHeaderProfileName());
        }
        materialAlertDialogBuilder.setPositiveButton(2131886759, (DialogInterface$OnClickListener)new DialogInterface$OnClickListener(this, editText, list) {
            public final EditText a;
            public final ArrayList b;
            public final CustomHeaderActivity c;
            
            public void onClick(final DialogInterface dialogInterface, final int n) {
                final String string = this.a.getText().toString();
                String string2;
                if (this.c.k.isChecked()) {
                    string2 = this.c.n.getText().toString();
                }
                else {
                    string2 = null;
                }
                String string3;
                if (this.c.l.isChecked()) {
                    string3 = this.c.p.getText().toString();
                }
                else {
                    string3 = null;
                }
                if (string.trim().equals("")) {
                    a91.G((Context)this.c.c, 2131886274, 2131230909, 2131231086);
                    return;
                }
                if (this.c.L(new HeaderProfile(string, this.b, string2, string3, 0))) {
                    a91.G((Context)this.c.c, 2131886341, 2131230927, 2131231085);
                    this.c.c.finish();
                }
                dialogInterface.dismiss();
            }
        });
        materialAlertDialogBuilder.setNegativeButton(2131886163, (DialogInterface$OnClickListener)new DialogInterface$OnClickListener(this) {
            public final CustomHeaderActivity a;
            
            public void onClick(final DialogInterface dialogInterface, final int n) {
                dialogInterface.dismiss();
            }
        });
        materialAlertDialogBuilder.create().show();
    }
    
    @Override
    public void onCreate(Bundle extras) {
        super.onCreate(extras);
        this.setContentView(2131492901);
        this.m = (TextInputLayout)this.findViewById(2131297176);
        this.x((Toolbar)this.findViewById(2131297341));
        extras = this.getIntent().getExtras();
        if (extras != null && extras.getSerializable("HEADER_PROFILE") != null) {
            this.h = (HeaderProfile)extras.getSerializable("HEADER_PROFILE");
        }
        final HeaderProfile h = this.h;
        if (h != null) {
            ((TextView)this.m.getEditText()).setText((CharSequence)h.getHeaderProfileName());
            if ((this.g = this.h.getLabels().size()) < 1) {
                this.g = 1;
            }
        }
        this.I();
        this.O();
        this.P();
        this.M();
        this.N();
        this.J(this.g);
    }
    
    @Override
    public boolean v() {
        this.finish();
        return true;
    }
}

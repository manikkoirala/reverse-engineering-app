// 
// Decompiled by Procyon v0.6.0
// 

package com.ekodroid.omrevaluator.templateui.createtemplate;

import android.widget.AdapterView;
import android.view.ViewGroup$MarginLayoutParams;
import android.os.Bundle;
import android.view.ViewGroup;
import android.view.MotionEvent;
import android.view.View$OnTouchListener;
import java.io.Serializable;
import android.content.Intent;
import com.ekodroid.omrevaluator.templateui.models.TemplateParams2;
import com.ekodroid.omrevaluator.templateui.models.Section2;
import com.ekodroid.omrevaluator.templateui.models.Subject2;
import android.view.View$OnClickListener;
import android.view.View$OnFocusChangeListener;
import android.widget.SpinnerAdapter;
import android.widget.Spinner;
import android.text.InputFilter$LengthFilter;
import android.text.InputFilter;
import android.widget.EditText;
import android.view.View;
import android.view.ViewGroup$LayoutParams;
import android.widget.LinearLayout$LayoutParams;
import android.content.Context;
import android.content.SharedPreferences$Editor;
import android.content.SharedPreferences;
import android.widget.ArrayAdapter;
import java.util.ArrayList;
import android.widget.TextView;
import android.widget.ImageButton;
import android.widget.Button;
import android.widget.LinearLayout;

public class SetTemplateActivity extends v5
{
    public SetTemplateActivity c;
    public LinearLayout d;
    public Button e;
    public ImageButton f;
    public ImageButton g;
    public ImageButton h;
    public ImageButton i;
    public ImageButton j;
    public ImageButton k;
    public TextView l;
    public TextView m;
    public TextView n;
    public ArrayList p;
    public ArrayAdapter q;
    public int t;
    public int v;
    public int w;
    public SharedPreferences x;
    public SharedPreferences$Editor y;
    
    public SetTemplateActivity() {
        this.c = this;
        this.t = 5;
        this.v = 1;
        this.w = 1;
    }
    
    public final void B(final int n) {
        int n2;
        for (int i = this.p.size(); i < n; i = n2) {
            this.p.add(new LinearLayout((Context)this));
            ((LinearLayout)this.p.get(i)).setOrientation(0);
            ((LinearLayout)this.p.get(i)).setWeightSum(5.0f);
            final LinearLayout$LayoutParams layoutParams = new LinearLayout$LayoutParams(-1, -2);
            ((ViewGroup$MarginLayoutParams)layoutParams).setMargins(2, 0, 2, 0);
            ((View)this.p.get(i)).setPadding(this.C(12, (Context)this), this.C(8, (Context)this), this.C(12, (Context)this), this.C(8, (Context)this));
            ((View)this.p.get(i)).setBackgroundColor(this.getResources().getColor(2131099740));
            ((View)this.p.get(i)).setLayoutParams((ViewGroup$LayoutParams)layoutParams);
            final TextView textView = new TextView((Context)this);
            final StringBuilder sb = new StringBuilder();
            sb.append(" ");
            n2 = i + 1;
            sb.append(n2);
            textView.setText((CharSequence)sb.toString());
            textView.setPadding(5, 5, 5, 5);
            textView.setTextAppearance(2131952183);
            ((ViewGroup)this.p.get(i)).addView((View)textView, 0, (ViewGroup$LayoutParams)new LinearLayout$LayoutParams(this.C(60, (Context)this), -2));
            final EditText editText = new EditText((Context)this);
            final StringBuilder sb2 = new StringBuilder();
            sb2.append(((Context)this).getString(2131886854));
            sb2.append(" ");
            sb2.append(n2);
            ((TextView)editText).setText((CharSequence)sb2.toString());
            ((TextView)editText).setInputType(1);
            ((View)editText).setTextAlignment(4);
            ((TextView)editText).setTextAppearance(2131951925);
            ((TextView)editText).setMaxLines(1);
            ((View)editText).setBackgroundResource(2131230848);
            ((TextView)editText).setFilters(new InputFilter[] { (InputFilter)new InputFilter$LengthFilter(50) });
            editText.setSelection(((CharSequence)editText.getText()).length());
            ((View)editText).setPadding(this.C(12, (Context)this), 0, this.C(12, (Context)this), 0);
            final LinearLayout$LayoutParams linearLayout$LayoutParams = new LinearLayout$LayoutParams(-2, this.C(40, (Context)this));
            ((ViewGroup$MarginLayoutParams)linearLayout$LayoutParams).setMargins(0, 0, this.C(16, (Context)this), 0);
            linearLayout$LayoutParams.weight = 5.0f;
            ((ViewGroup)this.p.get(i)).addView((View)editText, 1, (ViewGroup$LayoutParams)linearLayout$LayoutParams);
            final Spinner spinner = new Spinner((Context)this);
            spinner.setAdapter((SpinnerAdapter)this.q);
            spinner.setGravity(17);
            ((View)spinner).setBackgroundResource(2131230845);
            final LinearLayout$LayoutParams linearLayout$LayoutParams2 = new LinearLayout$LayoutParams(this.C(80, (Context)this), this.C(40, (Context)this));
            ((ViewGroup$MarginLayoutParams)linearLayout$LayoutParams2).setMargins(this.C(5, (Context)this), this.C(0, (Context)this), this.C(5, (Context)this), this.C(0, (Context)this));
            linearLayout$LayoutParams2.gravity = 17;
            ((ViewGroup)this.p.get(i)).addView((View)spinner, 2, (ViewGroup$LayoutParams)linearLayout$LayoutParams2);
            ((ViewGroup)this.d).addView((View)this.p.get(i));
            ((View)editText).setOnFocusChangeListener((View$OnFocusChangeListener)new View$OnFocusChangeListener(this, editText) {
                public final EditText a;
                public final SetTemplateActivity b;
                
                public void onFocusChange(final View view, final boolean cursorVisible) {
                    ((TextView)this.a).setCursorVisible(cursorVisible);
                }
            });
        }
    }
    
    public final int C(final int n, final Context context) {
        return (int)(n * context.getResources().getDisplayMetrics().density);
    }
    
    public final void D() {
        ((View)this.e).setOnClickListener((View$OnClickListener)new View$OnClickListener(this) {
            public final SetTemplateActivity a;
            
            public void onClick(final View view) {
                final int size = this.a.p.size();
                final Subject2[] array = new Subject2[size];
                for (int i = 0; i < size; ++i) {
                    final EditText editText = (EditText)((ViewGroup)this.a.p.get(i)).getChildAt(1);
                    final Spinner spinner = (Spinner)((ViewGroup)this.a.p.get(i)).getChildAt(2);
                    if (editText.getText().toString().trim().equals("")) {
                        a91.G((Context)this.a.c, 2131886270, 2131230909, 2131231086);
                        return;
                    }
                    (array[i] = new Subject2()).setSubName(editText.getText().toString());
                    array[i].setSections(new Section2[Integer.parseInt(((AdapterView)spinner).getSelectedItem().toString())]);
                }
                final SetTemplateActivity a = this.a;
                final TemplateParams2 templateParams2 = new TemplateParams2(a.t, a.v, array);
                final Intent intent = new Intent((Context)this.a.c, (Class)SetTemplate2Activity.class);
                intent.putExtra("SUBJECT_DETAIL", (Serializable)templateParams2);
                ((Context)this.a).startActivity(intent);
            }
        });
    }
    
    public final void E() {
        final Context applicationContext = ((Context)this).getApplicationContext();
        int i = 0;
        final SharedPreferences sharedPreferences = applicationContext.getSharedPreferences("MyPref", 0);
        this.x = sharedPreferences;
        this.y = sharedPreferences.edit();
        this.l = (TextView)this.findViewById(2131297223);
        this.m = (TextView)this.findViewById(2131297282);
        this.n = (TextView)this.findViewById(2131297301);
        this.f = (ImageButton)this.findViewById(2131296650);
        this.g = (ImageButton)this.findViewById(2131296677);
        this.h = (ImageButton)this.findViewById(2131296652);
        this.i = (ImageButton)this.findViewById(2131296679);
        this.j = (ImageButton)this.findViewById(2131296653);
        this.k = (ImageButton)this.findViewById(2131296680);
        this.d = (LinearLayout)this.findViewById(2131296718);
        this.e = (Button)this.findViewById(2131296456);
        final String[] array = new String[10];
        while (i < 10) {
            final StringBuilder sb = new StringBuilder();
            sb.append("");
            final int j = i + 1;
            sb.append(j);
            array[i] = sb.toString();
            i = j;
        }
        this.p = new ArrayList();
        this.q = new ArrayAdapter((Context)this.c, 2131493104, (Object[])array);
        this.getWindow().setSoftInputMode(3);
    }
    
    public final void F(final int n) {
        for (int i = this.p.size() - 1; i >= n; --i) {
            ((ViewGroup)this.d).removeView((View)this.p.get(i));
            this.p.remove(i);
        }
    }
    
    public final void G() {
        ((View)this.f).setOnClickListener((View$OnClickListener)new View$OnClickListener(this) {
            public final SetTemplateActivity a;
            
            public void onClick(final View view) {
                final SetTemplateActivity a = this.a;
                final int t = a.t;
                if (t < 9) {
                    a.t = t + 1;
                    final TextView l = a.l;
                    final StringBuilder sb = new StringBuilder();
                    sb.append(this.a.t);
                    sb.append("");
                    l.setText((CharSequence)sb.toString());
                }
            }
        });
    }
    
    public final void H() {
        ((View)this.g).setOnClickListener((View$OnClickListener)new View$OnClickListener(this) {
            public final SetTemplateActivity a;
            
            public void onClick(final View view) {
                final SetTemplateActivity a = this.a;
                final int t = a.t;
                if (t > 2) {
                    a.t = t - 1;
                    final TextView l = a.l;
                    final StringBuilder sb = new StringBuilder();
                    sb.append(this.a.t);
                    sb.append("");
                    l.setText((CharSequence)sb.toString());
                }
            }
        });
    }
    
    public final void I() {
        ((View)this.i).setOnClickListener((View$OnClickListener)new View$OnClickListener(this) {
            public final SetTemplateActivity a;
            
            public void onClick(final View view) {
                final SetTemplateActivity a = this.a;
                final int v = a.v;
                if (v > 1) {
                    a.v = v - 1;
                    final TextView m = a.m;
                    final StringBuilder sb = new StringBuilder();
                    sb.append(this.a.v);
                    sb.append("");
                    m.setText((CharSequence)sb.toString());
                }
            }
        });
    }
    
    public final void J() {
        ((View)this.h).setOnClickListener((View$OnClickListener)new View$OnClickListener(this) {
            public final SetTemplateActivity a;
            
            public void onClick(final View view) {
                final SetTemplateActivity a = this.a;
                final int v = a.v;
                if (v < 9) {
                    a.v = v + 1;
                    final TextView m = a.m;
                    final StringBuilder sb = new StringBuilder();
                    sb.append(this.a.v);
                    sb.append("");
                    m.setText((CharSequence)sb.toString());
                }
            }
        });
    }
    
    public final void K() {
        ((View)this.k).setOnClickListener((View$OnClickListener)new View$OnClickListener(this) {
            public final SetTemplateActivity a;
            
            public void onClick(final View view) {
                final SetTemplateActivity a = this.a;
                final int w = a.w;
                if (w > 1) {
                    a.w = w - 1;
                    final TextView n = a.n;
                    final StringBuilder sb = new StringBuilder();
                    sb.append(this.a.w);
                    sb.append("");
                    n.setText((CharSequence)sb.toString());
                    final SetTemplateActivity a2 = this.a;
                    a2.M(a2.w);
                }
            }
        });
    }
    
    public final void L() {
        ((View)this.j).setOnClickListener((View$OnClickListener)new View$OnClickListener(this) {
            public final SetTemplateActivity a;
            
            public void onClick(final View view) {
                final SetTemplateActivity a = this.a;
                final int w = a.w;
                if (w < 10) {
                    a.w = w + 1;
                    final TextView n = a.n;
                    final StringBuilder sb = new StringBuilder();
                    sb.append(this.a.w);
                    sb.append("");
                    n.setText((CharSequence)sb.toString());
                    final SetTemplateActivity a2 = this.a;
                    a2.M(a2.w);
                }
            }
        });
    }
    
    public final void M(final int n) {
        if (n > this.p.size()) {
            this.B(n);
        }
        else {
            this.F(n);
        }
    }
    
    public final void N() {
        this.findViewById(2131296417).setOnClickListener((View$OnClickListener)new View$OnClickListener(this) {
            public final SetTemplateActivity a;
            
            public void onClick(final View view) {
                this.a.finish();
            }
        });
    }
    
    public void hideKeyboardForViewsExceptEditText(final View view) {
        if (!(view instanceof EditText)) {
            view.setOnTouchListener((View$OnTouchListener)new View$OnTouchListener(this) {
                public final SetTemplateActivity a;
                
                public boolean onTouch(final View view, final MotionEvent motionEvent) {
                    a91.B((Context)this.a.c, view);
                    return false;
                }
            });
        }
        if (view instanceof ViewGroup) {
            int n = 0;
            while (true) {
                final ViewGroup viewGroup = (ViewGroup)view;
                if (n >= viewGroup.getChildCount()) {
                    break;
                }
                this.hideKeyboardForViewsExceptEditText(viewGroup.getChildAt(n));
                ++n;
            }
        }
    }
    
    @Override
    public void onCreate(final Bundle bundle) {
        super.onCreate(bundle);
        this.setContentView(2131492922);
        this.E();
        this.G();
        this.H();
        this.J();
        this.I();
        this.L();
        this.K();
        this.D();
        this.N();
        this.M(this.w);
    }
}

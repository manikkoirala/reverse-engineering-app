// 
// Decompiled by Procyon v0.6.0
// 

package com.ekodroid.omrevaluator.templateui.models;

import java.io.Serializable;

public class GradeLevel implements Serializable
{
    private double maxMarks;
    private double minMarks;
    private String value;
    
    public GradeLevel(final String value, final double minMarks, final double maxMarks) {
        this.value = value;
        this.minMarks = minMarks;
        this.maxMarks = maxMarks;
    }
    
    public double getMaxMarks() {
        return this.maxMarks;
    }
    
    public double getMinMarks() {
        return this.minMarks;
    }
    
    public String getStringLabel() {
        final StringBuilder sb = new StringBuilder();
        sb.append(this.getValue());
        sb.append(" (");
        sb.append(this.getMinMarks());
        sb.append(" - ");
        sb.append(this.getMaxMarks());
        sb.append(")");
        return sb.toString();
    }
    
    public String getValue() {
        return this.value;
    }
    
    public void setMaxMarks(final int n) {
        this.maxMarks = n;
    }
    
    public void setMinMarks(final int n) {
        this.minMarks = n;
    }
    
    public void setValue(final String value) {
        this.value = value;
    }
}

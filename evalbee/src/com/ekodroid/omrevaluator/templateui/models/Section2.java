// 
// Decompiled by Procyon v0.6.0
// 

package com.ekodroid.omrevaluator.templateui.models;

import java.io.Serializable;

public class Section2 implements Serializable
{
    String name;
    double negetiveMark;
    int noOfOptionalQue;
    int noOfQue;
    boolean optionalAllowed;
    String optionalType;
    boolean partialAllowed;
    double partialMarks;
    String payload;
    double positiveMark;
    AnswerOption.AnswerOptionType type;
    
    public String getName() {
        return this.name;
    }
    
    public double getNegetiveMark() {
        return this.negetiveMark;
    }
    
    public int getNoOfOptionalQue() {
        return this.noOfOptionalQue;
    }
    
    public int getNoOfQue() {
        return this.noOfQue;
    }
    
    public String getOptionalType() {
        return this.optionalType;
    }
    
    public double getPartialMarks() {
        return this.partialMarks;
    }
    
    public String getPayload() {
        return this.payload;
    }
    
    public double getPositiveMark() {
        return this.positiveMark;
    }
    
    public AnswerOption.AnswerOptionType getType() {
        return this.type;
    }
    
    public boolean isOptionalAllowed() {
        return this.optionalAllowed;
    }
    
    public boolean isPartialAllowed() {
        return this.partialAllowed;
    }
    
    public void setName(final String name) {
        this.name = name;
    }
    
    public void setNegetiveMark(final double negetiveMark) {
        this.negetiveMark = negetiveMark;
    }
    
    public void setNoOfOptionalQue(final int noOfOptionalQue) {
        this.noOfOptionalQue = noOfOptionalQue;
    }
    
    public void setNoOfQue(final int noOfQue) {
        this.noOfQue = noOfQue;
    }
    
    public void setOptionalAllowed(final boolean optionalAllowed) {
        this.optionalAllowed = optionalAllowed;
    }
    
    public void setOptionalType(final String optionalType) {
        this.optionalType = optionalType;
    }
    
    public void setPartialAllowed(final boolean partialAllowed) {
        this.partialAllowed = partialAllowed;
    }
    
    public void setPayload(final String payload) {
        this.payload = payload;
    }
    
    public void setPositiveMark(final double positiveMark) {
        this.positiveMark = positiveMark;
    }
    
    public void setType(final AnswerOption.AnswerOptionType type) {
        this.type = type;
    }
}

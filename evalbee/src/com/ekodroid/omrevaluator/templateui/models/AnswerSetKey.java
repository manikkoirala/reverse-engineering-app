// 
// Decompiled by Procyon v0.6.0
// 

package com.ekodroid.omrevaluator.templateui.models;

import com.ekodroid.omrevaluator.templateui.scanner.AnswerValue;
import java.util.ArrayList;
import java.io.Serializable;

public class AnswerSetKey implements Serializable
{
    private ArrayList<AnswerOptionKey> answerOptionKeys;
    private ArrayList<AnswerValue> answerValues;
    private int examSet;
    
    public AnswerSetKey(final ArrayList<AnswerOptionKey> answerOptionKeys, final int examSet) {
        this.answerOptionKeys = answerOptionKeys;
        this.examSet = examSet;
    }
    
    public ArrayList<AnswerOptionKey> getAnswerOptionKeys() {
        return this.answerOptionKeys;
    }
    
    public ArrayList<AnswerValue> getAnswerValue2s() {
        return this.answerValues;
    }
    
    public int getExamSet() {
        return this.examSet;
    }
    
    public void setAnswerOptionKeys(final ArrayList<AnswerOptionKey> answerOptionKeys) {
        this.answerOptionKeys = answerOptionKeys;
    }
    
    public void setAnswerValue2s(final ArrayList<AnswerValue> answerValues) {
        this.answerValues = answerValues;
    }
    
    public void setExamSet(final int examSet) {
        this.examSet = examSet;
    }
}

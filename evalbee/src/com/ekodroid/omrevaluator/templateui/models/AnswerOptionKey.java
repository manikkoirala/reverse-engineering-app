// 
// Decompiled by Procyon v0.6.0
// 

package com.ekodroid.omrevaluator.templateui.models;

import com.ekodroid.omrevaluator.templateui.scanner.AnswerValue;
import java.util.ArrayList;
import java.io.Serializable;

public class AnswerOptionKey implements Serializable
{
    String markedPayload;
    boolean[] markedValues;
    ArrayList<PartialAnswerOptionKey> partialAnswerOptionKeys;
    String payload;
    int questionNumber;
    int sectionId;
    int subjectId;
    AnswerOption.AnswerOptionType type;
    
    public AnswerOptionKey(final AnswerOption.AnswerOptionType type, final int questionNumber, final boolean[] markedValues, final String markedPayload, final int subjectId, final int sectionId, final ArrayList<PartialAnswerOptionKey> partialAnswerOptionKeys, final String payload) {
        this.type = type;
        this.questionNumber = questionNumber;
        this.markedValues = markedValues;
        this.subjectId = subjectId;
        this.sectionId = sectionId;
        this.markedPayload = markedPayload;
        this.partialAnswerOptionKeys = partialAnswerOptionKeys;
        this.payload = payload;
    }
    
    public AnswerOptionKey(final AnswerValue answerValue) {
        this.type = answerValue.getType();
        this.questionNumber = answerValue.getQuestionNumber();
        this.markedValues = answerValue.getMarkedValues();
        this.subjectId = answerValue.getSubjectId();
        this.sectionId = answerValue.getSectionId();
        this.partialAnswerOptionKeys = null;
        this.payload = answerValue.getPayload();
    }
    
    public String getMarkedPayload() {
        return this.markedPayload;
    }
    
    public boolean[] getMarkedValues() {
        return this.markedValues;
    }
    
    public ArrayList<PartialAnswerOptionKey> getPartialAnswerOptionKeys() {
        return this.partialAnswerOptionKeys;
    }
    
    public String getPayload() {
        return this.payload;
    }
    
    public int getQuestionNumber() {
        return this.questionNumber;
    }
    
    public int getSectionId() {
        return this.sectionId;
    }
    
    public int getSubjectId() {
        return this.subjectId;
    }
    
    public AnswerOption.AnswerOptionType getType() {
        return this.type;
    }
    
    public void setMarkedPayload(final String markedPayload) {
        this.markedPayload = markedPayload;
    }
    
    public void setMarkedValues(final boolean[] markedValues) {
        this.markedValues = markedValues;
    }
    
    public void setPartialAnswerOptionKeys(final ArrayList<PartialAnswerOptionKey> partialAnswerOptionKeys) {
        this.partialAnswerOptionKeys = partialAnswerOptionKeys;
    }
}

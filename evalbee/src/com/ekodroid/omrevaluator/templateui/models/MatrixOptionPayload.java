// 
// Decompiled by Procyon v0.6.0
// 

package com.ekodroid.omrevaluator.templateui.models;

import java.io.Serializable;

public class MatrixOptionPayload implements Serializable
{
    public int primaryOptions;
    public int secondaryOptions;
    
    public MatrixOptionPayload() {
        this.primaryOptions = 4;
        this.secondaryOptions = 5;
    }
    
    public MatrixOptionPayload(final int primaryOptions, final int secondaryOptions) {
        this.primaryOptions = primaryOptions;
        this.secondaryOptions = secondaryOptions;
    }
}

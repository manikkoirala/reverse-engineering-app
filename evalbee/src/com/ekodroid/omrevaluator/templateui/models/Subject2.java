// 
// Decompiled by Procyon v0.6.0
// 

package com.ekodroid.omrevaluator.templateui.models;

import java.io.Serializable;

public class Subject2 implements Serializable
{
    private Section2[] sections;
    private String subName;
    
    public Section2[] getSections() {
        return this.sections;
    }
    
    public String getSubName() {
        return this.subName;
    }
    
    public void setSections(final Section2[] sections) {
        this.sections = sections;
    }
    
    public void setSubName(final String subName) {
        this.subName = subName;
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.ekodroid.omrevaluator.templateui.models;

import java.io.Serializable;

public class DecimalOptionPayload implements Serializable
{
    public boolean decimalAllowed;
    public int digits;
    public boolean hasNegetive;
    
    public DecimalOptionPayload() {
    }
    
    public DecimalOptionPayload(final boolean hasNegetive, final int digits, final boolean decimalAllowed) {
        this.hasNegetive = hasNegetive;
        this.digits = digits;
        this.decimalAllowed = decimalAllowed;
    }
}

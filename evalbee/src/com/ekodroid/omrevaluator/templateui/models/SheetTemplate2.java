// 
// Decompiled by Procyon v0.6.0
// 

package com.ekodroid.omrevaluator.templateui.models;

import java.util.ArrayList;
import java.io.Serializable;

public class SheetTemplate2 implements Serializable
{
    private AnswerSetKey[] answerKeys;
    private ArrayList<AnswerOption> answerOptions;
    public int[] columnWidthInBubbles;
    public int columns;
    private AnswerOption examSetOption;
    private GradeLevel[] gradeLevels;
    private HeaderProfile headerProfile;
    private String invalidQueMarks;
    private InvalidQuestionSet[] invalidQuestionSets;
    private int[] invalidQuestions;
    private LabelProfile labelProfile;
    private int markerWidth;
    private String name;
    private PageLayout[] pageLayouts;
    private int pixelsInUnit;
    private String rankingMethod;
    public int rows;
    private int[] sectionsInSubject;
    private ArrayList<SheetLabel> sheetLabels;
    private TemplateParams2 templateParams;
    public int templatePixelHeight;
    public int templatePixelWidth;
    private int templateVersion;
    
    public SheetTemplate2(final PageLayout[] pageLayouts, final int pixelsInUnit, final int markerWidth, final AnswerOption examSetOption, final ArrayList<AnswerOption> answerOptions, final ArrayList<SheetLabel> sheetLabels, final TemplateParams2 templateParams) {
        this.templateVersion = 4;
        this.name = "";
        this.pageLayouts = pageLayouts;
        this.pixelsInUnit = pixelsInUnit;
        this.markerWidth = markerWidth;
        this.answerOptions = answerOptions;
        this.sheetLabels = sheetLabels;
        this.templateParams = templateParams;
        this.sectionsInSubject = this.getSectionsArrayFromTemplateParams(templateParams);
        this.examSetOption = examSetOption;
    }
    
    private int[] getSectionsArrayFromTemplateParams(final TemplateParams2 templateParams2) {
        final int length = templateParams2.getSubjects().length;
        final int[] array = new int[length];
        for (int i = 0; i < length; ++i) {
            array[i] = templateParams2.getSubjects()[i].getSections().length;
        }
        return array;
    }
    
    private void updatePagelayout() {
        if (this.pageLayouts == null) {
            (this.pageLayouts = new PageLayout[] { null })[0] = new PageLayout(this.rows, this.columns, this.columnWidthInBubbles);
        }
    }
    
    public AnswerSetKey[] getAnswerKeys() {
        return this.answerKeys;
    }
    
    public ArrayList<AnswerOption> getAnswerOptions() {
        return this.answerOptions;
    }
    
    public int[] getColumnWidthInBubbles(final int n) {
        this.updatePagelayout();
        return this.pageLayouts[n].getColumnWidthInBubbles();
    }
    
    public int getColumns(final int n) {
        this.updatePagelayout();
        return this.pageLayouts[n].getColumns();
    }
    
    public AnswerOption getExamSetOption() {
        return this.examSetOption;
    }
    
    public GradeLevel[] getGradeLevels() {
        return this.gradeLevels;
    }
    
    public HeaderProfile getHeaderProfile() {
        return this.headerProfile;
    }
    
    public String getInvalidQueMarks() {
        return this.invalidQueMarks;
    }
    
    public InvalidQuestionSet[] getInvalidQuestionSets() {
        return this.invalidQuestionSets;
    }
    
    public int[] getInvalidQuestions() {
        return this.invalidQuestions;
    }
    
    public LabelProfile getLabelProfile() {
        if (this.labelProfile == null) {
            this.labelProfile = new LabelProfile();
        }
        return this.labelProfile;
    }
    
    public int getMarkerWidth() {
        return this.markerWidth;
    }
    
    public String getName() {
        return this.name;
    }
    
    public PageLayout[] getPageLayouts() {
        this.updatePagelayout();
        return this.pageLayouts;
    }
    
    public int getPixelsInUnit() {
        return this.pixelsInUnit;
    }
    
    public String getRankingMethod() {
        return this.rankingMethod;
    }
    
    public int getRows(final int n) {
        this.updatePagelayout();
        return this.pageLayouts[n].getRows();
    }
    
    public int[] getSectionsInSubject() {
        return this.sectionsInSubject;
    }
    
    public ArrayList<SheetLabel> getSheetLabels() {
        return this.sheetLabels;
    }
    
    public TemplateParams2 getTemplateParams() {
        return this.templateParams;
    }
    
    public int getTemplatePixelHeight(final int n) {
        this.updatePagelayout();
        return this.pageLayouts[n].getRows() * this.pixelsInUnit;
    }
    
    public int getTemplatePixelWidth(final int n) {
        this.updatePagelayout();
        return (ve1.e(this.pageLayouts[n].getColumnWidthInBubbles()) + 1) * this.pixelsInUnit;
    }
    
    public int getTemplateVersion() {
        return this.templateVersion;
    }
    
    public void setAnswerKeys(final AnswerSetKey[] answerKeys) {
        this.answerKeys = answerKeys;
    }
    
    public void setGradeLevels(final GradeLevel[] gradeLevels) {
        this.gradeLevels = gradeLevels;
    }
    
    public void setHeaderProfile(final HeaderProfile headerProfile) {
        this.headerProfile = headerProfile;
    }
    
    public void setInvalidQueMarks(final String invalidQueMarks) {
        this.invalidQueMarks = invalidQueMarks;
    }
    
    public void setInvalidQuestionSets(final InvalidQuestionSet[] invalidQuestionSets) {
        this.invalidQuestionSets = invalidQuestionSets;
    }
    
    public void setInvalidQuestions(final int[] invalidQuestions) {
        this.invalidQuestions = invalidQuestions;
    }
    
    public void setLabelProfile(final LabelProfile labelProfile) {
        this.labelProfile = labelProfile;
    }
    
    public void setName(final String name) {
        this.name = name;
    }
    
    public void setPageLayouts(final PageLayout[] pageLayouts) {
        this.pageLayouts = pageLayouts;
    }
    
    public void setRankingMethod(final String rankingMethod) {
        this.rankingMethod = rankingMethod;
    }
    
    public void setTemplateParams(final TemplateParams2 templateParams) {
        this.templateParams = templateParams;
    }
    
    public void setTemplateVersion(final int templateVersion) {
        this.templateVersion = templateVersion;
    }
}

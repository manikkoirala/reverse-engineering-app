// 
// Decompiled by Procyon v0.6.0
// 

package com.ekodroid.omrevaluator.templateui.models;

import android.text.Layout;
import android.graphics.BitmapFactory;
import android.graphics.Bitmap$Config;
import android.graphics.Paint$Align;
import android.graphics.Paint$Style;
import android.graphics.Bitmap;
import android.text.Layout$Alignment;
import android.text.StaticLayout$Builder;
import android.text.TextPaint;
import android.text.StaticLayout;
import java.util.ArrayList;
import java.util.Iterator;
import android.graphics.Canvas;
import android.content.Context;
import android.graphics.Paint;

public class a
{
    public int a;
    public int b;
    public int c;
    public Paint d;
    public Paint e;
    public Paint f;
    public Context g;
    
    public a() {
        this.a = 100;
        this.b = 0;
    }
    
    public final void A(final Canvas canvas, Point2Integer addy, final int n, final int n2, final String[] array, int i, final Paint paint) {
        final int n3 = n * 3;
        final int n4 = n3 / 5;
        final Point2Integer addy2 = addy.addx(n3 / 2).addy(n * 2);
        final int n5 = 0;
        for (int j = 0; j < 10; ++j) {
            this.H(canvas, array[(j + i) % 10], addy2.addy(j * n), n4, paint);
        }
        addy = addy.addx(n * 5 / 2).addy(n);
        for (i = n5; i < n2; ++i) {
            this.d(canvas, addy.addx(i * n), n, this.e);
        }
    }
    
    public final void B(final Canvas canvas, final AnswerOption answerOption, final SheetTemplate2 sheetTemplate2) {
        final int n = sheetTemplate2.getMarkerWidth() / 2;
        final int pixelsInUnit = sheetTemplate2.getPixelsInUnit();
        final Point2Integer t = this.T(answerOption, sheetTemplate2);
        final int subIndex = answerOption.subIndex;
        for (int i = 0; i < 10; ++i) {
            final Point2Integer addy = t.addx(pixelsInUnit * 5 / 2).addy((i + 2) * pixelsInUnit);
            for (int j = 0; j < subIndex; ++j) {
                this.e(canvas, addy.addx(j * pixelsInUnit), n, this.e);
            }
        }
        this.A(canvas, t, pixelsInUnit, subIndex, sheetTemplate2.getLabelProfile().getRollDigitsLabels(), sheetTemplate2.getLabelProfile().getRollStartDigit(), this.f);
    }
    
    public final void C(final Canvas canvas, final SheetLabel sheetLabel, final SheetTemplate2 sheetTemplate2) {
        final int pixelsInUnit = sheetTemplate2.getPixelsInUnit();
        this.H(canvas, sheetTemplate2.getLabelProfile().getExamSetString(), this.U(sheetLabel, sheetTemplate2).addx(pixelsInUnit * 8 / 2), pixelsInUnit * 3 / 5, this.f);
    }
    
    public final void D(final Canvas canvas, final AnswerOption answerOption, final SheetTemplate2 sheetTemplate2) {
        final int n = sheetTemplate2.getMarkerWidth() / 2;
        final int pixelsInUnit = sheetTemplate2.getPixelsInUnit();
        final Point2Integer t = this.T(answerOption, sheetTemplate2);
        final int n2 = pixelsInUnit * 5;
        final Point2Integer addx = t.addx(n2 / 2);
        for (int i = 0; i < 6; ++i) {
            this.e(canvas, addx.addx(i * pixelsInUnit), n, this.e);
        }
        this.H(canvas, sheetTemplate2.getLabelProfile().getQuestionNumberLabel(answerOption.questionNumber), t.addx(n2 / 4), n * 2, this.f);
    }
    
    public final void E(final Canvas canvas, final SheetLabel sheetLabel, final SheetTemplate2 sheetTemplate2) {
        final int pixelsInUnit = sheetTemplate2.getPixelsInUnit();
        final int n = pixelsInUnit * 3 / 5;
        final Point2Integer u = this.U(sheetLabel, sheetTemplate2);
        final String[] sixOptionLabels = sheetTemplate2.getLabelProfile().getSixOptionLabels();
        final Point2Integer addx = u.addx(pixelsInUnit * 5 / 2);
        for (int i = 0; i < 6; ++i) {
            this.H(canvas, sixOptionLabels[i], addx.addx(i * pixelsInUnit), n, this.f);
        }
    }
    
    public final void F(final Canvas canvas, final AnswerOption answerOption, final SheetTemplate2 sheetTemplate2) {
        final int n = sheetTemplate2.getMarkerWidth() / 2;
        final int pixelsInUnit = sheetTemplate2.getPixelsInUnit();
        final Point2Integer t = this.T(answerOption, sheetTemplate2);
        final int n2 = pixelsInUnit * 5;
        final Point2Integer addx = t.addx(n2 / 2);
        for (int i = 0; i < 10; ++i) {
            this.e(canvas, addx.addx(i * pixelsInUnit), n, this.e);
        }
        this.H(canvas, sheetTemplate2.getLabelProfile().getQuestionNumberLabel(answerOption.questionNumber), t.addx(n2 / 4), n * 2, this.f);
    }
    
    public final void G(final Canvas canvas, final SheetLabel sheetLabel, final SheetTemplate2 sheetTemplate2) {
        final int pixelsInUnit = sheetTemplate2.getPixelsInUnit();
        final int n = pixelsInUnit * 3 / 5;
        final Point2Integer u = this.U(sheetLabel, sheetTemplate2);
        final String[] tenOptionLabels = sheetTemplate2.getLabelProfile().getTenOptionLabels();
        final Point2Integer addx = u.addx(pixelsInUnit * 5 / 2);
        for (int i = 0; i < 10; ++i) {
            this.H(canvas, tenOptionLabels[i], addx.addx(i * pixelsInUnit), n, this.f);
        }
    }
    
    public final void H(final Canvas canvas, final String s, final Point2Integer point2Integer, final int n, final Paint paint) {
        if (s == null) {
            return;
        }
        paint.setTextSize((float)n);
        canvas.drawText(s, (float)point2Integer.x, (float)(point2Integer.y + n / 2 - 2), paint);
    }
    
    public final void I(final Canvas canvas, final SheetLabel sheetLabel, final SheetTemplate2 sheetTemplate2) {
        final int pixelsInUnit = sheetTemplate2.getPixelsInUnit();
        this.H(canvas, sheetLabel.text, this.U(sheetLabel, sheetTemplate2).addx(pixelsInUnit * 8 / 2), pixelsInUnit * 3 / 5, this.f);
    }
    
    public final void J(final Canvas canvas, final AnswerOption answerOption, final SheetTemplate2 sheetTemplate2) {
        final int n = sheetTemplate2.getMarkerWidth() / 2;
        final int pixelsInUnit = sheetTemplate2.getPixelsInUnit();
        final Point2Integer t = this.T(answerOption, sheetTemplate2);
        final int n2 = pixelsInUnit * 5;
        final Point2Integer addx = t.addx(n2 / 2);
        for (int i = 0; i < 3; ++i) {
            this.e(canvas, addx.addx(i * pixelsInUnit), n, this.e);
        }
        this.H(canvas, sheetTemplate2.getLabelProfile().getQuestionNumberLabel(answerOption.questionNumber), t.addx(n2 / 4), n * 2, this.f);
    }
    
    public final void K(final Canvas canvas, final SheetLabel sheetLabel, final SheetTemplate2 sheetTemplate2) {
        final int pixelsInUnit = sheetTemplate2.getPixelsInUnit();
        final int n = pixelsInUnit * 3 / 5;
        final Point2Integer u = this.U(sheetLabel, sheetTemplate2);
        final String[] threeOptionLabels = sheetTemplate2.getLabelProfile().getThreeOptionLabels();
        final Point2Integer addx = u.addx(pixelsInUnit * 5 / 2);
        for (int i = 0; i < threeOptionLabels.length; ++i) {
            this.H(canvas, threeOptionLabels[i], addx.addx(i * pixelsInUnit), n, this.f);
        }
    }
    
    public final void L(final Canvas canvas, final AnswerOption answerOption, final SheetTemplate2 sheetTemplate2) {
        final int n = sheetTemplate2.getMarkerWidth() / 2;
        final int pixelsInUnit = sheetTemplate2.getPixelsInUnit();
        final Point2Integer t = this.T(answerOption, sheetTemplate2);
        final int n2 = pixelsInUnit * 5;
        final Point2Integer addx = t.addx(n2 / 2);
        for (int i = 1; i < 4; i += 2) {
            this.e(canvas, addx.addx(i * pixelsInUnit), n, this.e);
        }
        this.H(canvas, sheetTemplate2.getLabelProfile().getQuestionNumberLabel(answerOption.questionNumber), t.addx(n2 / 4), n * 2, this.f);
    }
    
    public final void M(final Canvas canvas, final SheetLabel sheetLabel, final SheetTemplate2 sheetTemplate2) {
        final int pixelsInUnit = sheetTemplate2.getPixelsInUnit();
        final int n = pixelsInUnit * 3 / 5;
        final Point2Integer u = this.U(sheetLabel, sheetTemplate2);
        final String[] trueOrFalseLabel = sheetTemplate2.getLabelProfile().getTrueOrFalseLabel();
        final Point2Integer addx = u.addx(pixelsInUnit * 5 / 2);
        for (int i = 1; i < 4; i += 2) {
            this.H(canvas, trueOrFalseLabel[(i - 1) / 2], addx.addx(i * pixelsInUnit), n, this.f);
        }
    }
    
    public final void N(final Canvas canvas, final SheetTemplate2 sheetTemplate2, final int n) {
        for (final SheetLabel sheetLabel : sheetTemplate2.getSheetLabels()) {
            if (sheetLabel.pageIndex == n) {
                this.r(canvas, sheetLabel, sheetTemplate2);
            }
        }
    }
    
    public final HeaderProfile O(final String str, final int n) {
        final HeaderLabel.Size fullwidth = HeaderLabel.Size.FULLWIDTH;
        final StringBuilder sb = new StringBuilder();
        sb.append("Page ");
        sb.append(n + 1);
        sb.append("   ");
        sb.append(str);
        final HeaderLabel e = new HeaderLabel(fullwidth, sb.toString());
        final ArrayList list = new ArrayList();
        list.add(e);
        return new HeaderProfile("ExtraPage", list, null, null, 0);
    }
    
    public final int P(final SheetTemplate2 sheetTemplate2, final HeaderProfile headerProfile) {
        if (headerProfile != null) {
            final ArrayList<HeaderLabel> labels = headerProfile.labels;
            if (labels != null) {
                if (labels.size() >= 1) {
                    final int columns = sheetTemplate2.getColumns(this.c);
                    final Iterator<HeaderLabel> iterator = headerProfile.labels.iterator();
                    int n = columns;
                    int n2 = 1;
                    while (iterator.hasNext()) {
                        final int n3 = a$a.a[iterator.next().size.ordinal()];
                        if (n3 != 1) {
                            if (n3 != 2) {
                                if (n3 != 3) {
                                    if (n3 != 4) {
                                        continue;
                                    }
                                    int n4 = n2;
                                    if (n != columns) {
                                        n4 = n2 + 1;
                                    }
                                    n = 0;
                                    n2 = n4;
                                }
                                else if (n > 2) {
                                    n -= 3;
                                }
                                else {
                                    int n5 = n2;
                                    if (n != columns) {
                                        n5 = n2 + 1;
                                    }
                                    n = columns - 3;
                                    n2 = n5;
                                }
                            }
                            else if (n > 1) {
                                n -= 2;
                            }
                            else {
                                ++n2;
                                n = columns - 2;
                            }
                        }
                        else if (n > 0) {
                            --n;
                        }
                        else {
                            ++n2;
                            n = columns - 1;
                        }
                    }
                    return n2;
                }
            }
        }
        return 0;
    }
    
    public final int Q(final SheetTemplate2 sheetTemplate2) {
        HeaderProfile headerProfile = sheetTemplate2.getHeaderProfile();
        if (this.c > 0) {
            headerProfile = this.O(sheetTemplate2.getLabelProfile().getRollNoString(), this.c);
        }
        if (headerProfile == null) {
            return 0;
        }
        int n = headerProfile.getEmptyHeight() + 0;
        if (headerProfile.getTitle() != null) {
            n += (int)(((Layout)this.V(headerProfile.getTitle(), sheetTemplate2.getTemplatePixelWidth(this.c))).getHeight() * 1.3);
        }
        int n2 = (int)(n + this.P(sheetTemplate2, headerProfile) * 1.5 * sheetTemplate2.getPixelsInUnit());
        if (headerProfile.getInstruction() != null) {
            int height;
            if ((height = ((Layout)this.R(headerProfile.getInstruction(), (int)(sheetTemplate2.getTemplatePixelWidth(this.c) - 250 - sheetTemplate2.getPixelsInUnit() * 0.6))).getHeight()) < 110) {
                height = 110;
            }
            n2 += height + 10;
        }
        return n2;
    }
    
    public final StaticLayout R(final String s, final int n) {
        final TextPaint textPaint = new TextPaint();
        ((Paint)textPaint).setTextSize(20.0f);
        ((Paint)textPaint).setAntiAlias(true);
        return StaticLayout$Builder.obtain((CharSequence)s, 0, s.length(), textPaint, n).setAlignment(Layout$Alignment.ALIGN_NORMAL).setIncludePad(true).build();
    }
    
    public final Point2Integer S(final int n, final int n2, final SheetTemplate2 sheetTemplate2) {
        final int pixelsInUnit = sheetTemplate2.getPixelsInUnit();
        final int a = this.a;
        final int n3 = pixelsInUnit / 2;
        return new Point2Integer(a + n3 + this.W(sheetTemplate2.getColumnWidthInBubbles(this.c), n2) * pixelsInUnit, this.a + this.b + n3 + n * 5 * pixelsInUnit);
    }
    
    public final Point2Integer T(final AnswerOption answerOption, final SheetTemplate2 sheetTemplate2) {
        final int column = answerOption.column;
        final int row = answerOption.row;
        final int pixelsInUnit = sheetTemplate2.getPixelsInUnit();
        final int w = this.W(sheetTemplate2.getColumnWidthInBubbles(this.c), column - 1);
        final int a = this.a;
        final int n = pixelsInUnit / 2;
        return new Point2Integer(a + n + w * pixelsInUnit, a + this.b + n + (row - 1) * pixelsInUnit);
    }
    
    public final Point2Integer U(final SheetLabel sheetLabel, final SheetTemplate2 sheetTemplate2) {
        final int column = sheetLabel.column;
        final int row = sheetLabel.row;
        final int pixelsInUnit = sheetTemplate2.getPixelsInUnit();
        final int w = this.W(sheetTemplate2.getColumnWidthInBubbles(this.c), column - 1);
        final int a = this.a;
        final int n = pixelsInUnit / 2;
        return new Point2Integer(a + n + w * pixelsInUnit, a + this.b + n + (row - 1) * pixelsInUnit);
    }
    
    public final StaticLayout V(final String s, final int n) {
        final TextPaint textPaint = new TextPaint();
        ((Paint)textPaint).setTextSize(40.0f);
        ((Paint)textPaint).setAntiAlias(true);
        return StaticLayout$Builder.obtain((CharSequence)s, 0, s.length(), textPaint, n).setAlignment(Layout$Alignment.ALIGN_CENTER).setIncludePad(true).build();
    }
    
    public final int W(final int[] array, final int n) {
        int i = 0;
        int n2 = 0;
        while (i < n) {
            n2 += array[i];
            ++i;
        }
        return n2;
    }
    
    public Bitmap X(final Context g, final SheetTemplate2 sheetTemplate2, final int c) {
        this.c = c;
        this.g = g;
        (this.d = new Paint()).setColor(-16777216);
        this.d.setStyle(Paint$Style.FILL);
        (this.e = new Paint()).setStyle(Paint$Style.STROKE);
        this.e.setAntiAlias(true);
        this.e.setColor(-16777216);
        this.e.setStrokeWidth(1.5f);
        (this.f = new Paint()).setStyle(Paint$Style.FILL_AND_STROKE);
        this.f.setAntiAlias(true);
        this.f.setColor(-16777216);
        this.f.setTextAlign(Paint$Align.CENTER);
        final Bitmap a = this.a(sheetTemplate2);
        final Canvas canvas = new Canvas(a);
        canvas.drawARGB(255, 255, 255, 255);
        this.t(canvas, sheetTemplate2);
        this.k(canvas, sheetTemplate2, c);
        this.c(canvas, sheetTemplate2, c);
        this.N(canvas, sheetTemplate2, c);
        HeaderProfile headerProfile;
        if (c == 0) {
            headerProfile = sheetTemplate2.getHeaderProfile();
        }
        else {
            headerProfile = this.O(sheetTemplate2.getLabelProfile().getRollNoString(), c);
        }
        this.p(canvas, sheetTemplate2, headerProfile);
        return a;
    }
    
    public final Bitmap a(final SheetTemplate2 sheetTemplate2) {
        this.b = this.Q(sheetTemplate2);
        return Bitmap.createBitmap(sheetTemplate2.getTemplatePixelWidth(this.c) + this.a * 2, sheetTemplate2.getTemplatePixelHeight(this.c) + this.a * 2 + this.b, Bitmap$Config.ARGB_8888);
    }
    
    public final void b(final Canvas canvas, final AnswerOption answerOption, final SheetTemplate2 sheetTemplate2) {
        switch (a$a.c[answerOption.type.ordinal()]) {
            default: {
                return;
            }
            case 11: {
                this.F(canvas, answerOption, sheetTemplate2);
                return;
            }
            case 10: {
                this.i(canvas, answerOption, sheetTemplate2);
                return;
            }
            case 9: {
                this.D(canvas, answerOption, sheetTemplate2);
                return;
            }
            case 8: {
                this.J(canvas, answerOption, sheetTemplate2);
                return;
            }
            case 7: {
                this.L(canvas, answerOption, sheetTemplate2);
                return;
            }
            case 6: {
                this.h(canvas, answerOption, sheetTemplate2);
                return;
            }
            case 5: {
                this.x(canvas, answerOption, sheetTemplate2);
                return;
            }
            case 4: {
                this.B(canvas, answerOption, sheetTemplate2);
                return;
            }
            case 3: {
                this.u(canvas, answerOption, sheetTemplate2);
                return;
            }
            case 2: {
                this.l(canvas, answerOption, sheetTemplate2);
                return;
            }
            case 1: {
                this.n(canvas, answerOption, sheetTemplate2);
            }
        }
    }
    
    public final void c(final Canvas canvas, final SheetTemplate2 sheetTemplate2, final int n) {
        for (final AnswerOption answerOption : sheetTemplate2.getAnswerOptions()) {
            if (answerOption.pageIndex == n) {
                this.b(canvas, answerOption, sheetTemplate2);
            }
        }
    }
    
    public final void d(final Canvas canvas, final Point2Integer point2Integer, int y, final Paint paint) {
        final int n = y / 2 - 1;
        final int x = point2Integer.x;
        final float n2 = (float)(x - n);
        y = point2Integer.y;
        canvas.drawRect(n2, (float)(y - n), (float)(x + n), (float)(y + n), paint);
    }
    
    public final void e(final Canvas canvas, final Point2Integer point2Integer, final int n, final Paint paint) {
        canvas.drawCircle((float)point2Integer.x, (float)point2Integer.y, (float)n, paint);
    }
    
    public final void f(final Canvas canvas, Point2Integer point2Integer, final int n, final String[] array, final boolean b, final Paint paint) {
        final int n2 = n * 3;
        final int n3 = n2 / 5;
        final Point2Integer point2Integer2 = point2Integer = point2Integer.addx(n2 / 2).addy(n);
        if (b) {
            this.H(canvas, ".", point2Integer2.addy(-n / 2), n3 * 2, paint);
            point2Integer = point2Integer2.addy(n);
        }
        for (int i = 0; i < 10; ++i) {
            this.H(canvas, array[i], point2Integer.addy(i * n), n3, paint);
        }
    }
    
    public final void g(final Canvas canvas, final AnswerOption answerOption, final SheetTemplate2 sheetTemplate2) {
        final NumericalOptionPayload d = bc.d(answerOption.payload);
        final int n = sheetTemplate2.getMarkerWidth() / 2;
        final int pixelsInUnit = sheetTemplate2.getPixelsInUnit();
        final Point2Integer t = this.T(answerOption, sheetTemplate2);
        this.y(canvas, t, pixelsInUnit, this.f, d.digits);
        if (answerOption.subIndex == 0) {
            this.w(canvas, t, pixelsInUnit, sheetTemplate2.getLabelProfile().getNumericalOptionLabels(), this.f);
        }
        final int n2 = pixelsInUnit * 5;
        final int n3 = n2 / 2;
        final Point2Integer addy = t.addx(n3).addy(pixelsInUnit);
        for (int i = 0; i < d.digits; ++i) {
            for (int j = 0; j < 10; ++j) {
                this.e(canvas, addy.addx(i * pixelsInUnit).addy(j * pixelsInUnit), n, this.e);
            }
        }
        if (d.hasNegetive) {
            this.e(canvas, t.addx(n3 + pixelsInUnit), n, this.e);
            this.H(canvas, "-", t.addx((int)(pixelsInUnit * 6 / 2 - pixelsInUnit * 0.3)).addy((int)(pixelsInUnit * -1 * 0.3)), n * 5, this.f);
        }
        final int decimalDigits = d.decimalDigits;
        if (decimalDigits > 0) {
            final Point2Integer addy2 = t.addx(pixelsInUnit * 4 / 2 + (d.digits - decimalDigits) * pixelsInUnit).addy((int)(pixelsInUnit * 1.2));
            for (int k = 0; k < 10; ++k) {
                this.e(canvas, addy2.addy(k * pixelsInUnit), 1, this.e);
            }
        }
        this.H(canvas, sheetTemplate2.getLabelProfile().getQuestionNumberLabel(answerOption.questionNumber), t.addx(n2 / 4), n * 2, this.f);
    }
    
    public final void h(final Canvas canvas, final AnswerOption answerOption, final SheetTemplate2 sheetTemplate2) {
        DecimalOptionPayload b;
        if ((b = bc.b(answerOption.payload)) == null) {
            b = new DecimalOptionPayload(false, 2, false);
        }
        final int n = sheetTemplate2.getMarkerWidth() / 2;
        final int pixelsInUnit = sheetTemplate2.getPixelsInUnit();
        final Point2Integer addx = this.T(answerOption, sheetTemplate2).addx(answerOption.subIndex * 4 * pixelsInUnit);
        this.y(canvas, addx, pixelsInUnit, this.f, b.digits);
        this.f(canvas, addx, pixelsInUnit, sheetTemplate2.getLabelProfile().getNumericalOptionLabels(), b.decimalAllowed, this.f);
        final int n2 = pixelsInUnit * 5;
        final int n3 = n2 / 2;
        final Point2Integer addy = addx.addx(n3).addy(pixelsInUnit);
        int n4;
        if (b.decimalAllowed) {
            n4 = 11;
        }
        else {
            n4 = 10;
        }
        for (int i = 0; i < b.digits; ++i) {
            for (int j = 0; j < n4; ++j) {
                this.e(canvas, addy.addx(i * pixelsInUnit).addy(j * pixelsInUnit), n, this.e);
            }
        }
        if (b.hasNegetive) {
            this.e(canvas, addx.addx(n3 + pixelsInUnit), n, this.e);
            this.H(canvas, "-", addx.addx((int)(pixelsInUnit * 6 / 2 - pixelsInUnit * 0.3)).addy((int)(pixelsInUnit * -1 * 0.3)), n * 5, this.f);
        }
        this.H(canvas, sheetTemplate2.getLabelProfile().getQuestionNumberLabel(answerOption.questionNumber), addx.addx(n2 / 4), n * 2, this.f);
    }
    
    public final void i(final Canvas canvas, final AnswerOption answerOption, final SheetTemplate2 sheetTemplate2) {
        final int n = sheetTemplate2.getMarkerWidth() / 2;
        final int pixelsInUnit = sheetTemplate2.getPixelsInUnit();
        final Point2Integer t = this.T(answerOption, sheetTemplate2);
        final int n2 = pixelsInUnit * 5;
        final Point2Integer addx = t.addx(n2 / 2);
        for (int i = 0; i < 8; ++i) {
            this.e(canvas, addx.addx(i * pixelsInUnit), n, this.e);
        }
        this.H(canvas, sheetTemplate2.getLabelProfile().getQuestionNumberLabel(answerOption.questionNumber), t.addx(n2 / 4), n * 2, this.f);
    }
    
    public final void j(final Canvas canvas, final SheetLabel sheetLabel, final SheetTemplate2 sheetTemplate2) {
        final int pixelsInUnit = sheetTemplate2.getPixelsInUnit();
        final int n = pixelsInUnit * 3 / 5;
        final Point2Integer u = this.U(sheetLabel, sheetTemplate2);
        final String[] eightOptionLabels = sheetTemplate2.getLabelProfile().getEightOptionLabels();
        final Point2Integer addx = u.addx(pixelsInUnit * 5 / 2);
        for (int i = 0; i < 8; ++i) {
            this.H(canvas, eightOptionLabels[i], addx.addx(i * pixelsInUnit), n, this.f);
        }
    }
    
    public final void k(final Canvas canvas, final SheetTemplate2 sheetTemplate2, int i) {
        final AnswerOption examSetOption = sheetTemplate2.getExamSetOption();
        if (examSetOption != null) {
            if (examSetOption.pageIndex == i) {
                final String[] examSetLabels = sheetTemplate2.getLabelProfile().getExamSetLabels();
                final int n = sheetTemplate2.getColumnWidthInBubbles(this.c)[examSetOption.column - 1] - 3;
                final int n2 = sheetTemplate2.getMarkerWidth() / 2;
                final int pixelsInUnit = sheetTemplate2.getPixelsInUnit();
                final int n3 = pixelsInUnit * 3 / 5;
                final Point2Integer addx = this.T(examSetOption, sheetTemplate2).addx(pixelsInUnit * 5 / 2);
                Point2Integer addy;
                for (i = 0; i < examSetOption.subIndex; ++i) {
                    addy = addx.addx(i % n * pixelsInUnit).addy(i / n * pixelsInUnit * 2);
                    this.e(canvas, addy.addy(pixelsInUnit), n2, this.e);
                    this.H(canvas, examSetLabels[i], addy, n3, this.f);
                }
            }
        }
    }
    
    public final void l(final Canvas canvas, final AnswerOption answerOption, final SheetTemplate2 sheetTemplate2) {
        final int n = sheetTemplate2.getMarkerWidth() / 2;
        final int pixelsInUnit = sheetTemplate2.getPixelsInUnit();
        final Point2Integer t = this.T(answerOption, sheetTemplate2);
        final int n2 = pixelsInUnit * 5;
        final Point2Integer addx = t.addx(n2 / 2);
        for (int i = 0; i < 5; ++i) {
            this.e(canvas, addx.addx(i * pixelsInUnit), n, this.e);
        }
        this.H(canvas, sheetTemplate2.getLabelProfile().getQuestionNumberLabel(answerOption.questionNumber), t.addx(n2 / 4), n * 2, this.f);
    }
    
    public final void m(final Canvas canvas, final SheetLabel sheetLabel, final SheetTemplate2 sheetTemplate2) {
        final int pixelsInUnit = sheetTemplate2.getPixelsInUnit();
        final int n = pixelsInUnit * 3 / 5;
        final Point2Integer u = this.U(sheetLabel, sheetTemplate2);
        final String[] fiveOptionLabels = sheetTemplate2.getLabelProfile().getFiveOptionLabels();
        final Point2Integer addx = u.addx(pixelsInUnit * 5 / 2);
        for (int i = 0; i < 5; ++i) {
            this.H(canvas, fiveOptionLabels[i], addx.addx(i * pixelsInUnit), n, this.f);
        }
    }
    
    public final void n(final Canvas canvas, final AnswerOption answerOption, final SheetTemplate2 sheetTemplate2) {
        final int n = sheetTemplate2.getMarkerWidth() / 2;
        final int pixelsInUnit = sheetTemplate2.getPixelsInUnit();
        final Point2Integer t = this.T(answerOption, sheetTemplate2);
        final int n2 = pixelsInUnit * 5;
        final Point2Integer addx = t.addx(n2 / 2);
        for (int i = 0; i < 4; ++i) {
            this.e(canvas, addx.addx(i * pixelsInUnit), n, this.e);
        }
        this.H(canvas, sheetTemplate2.getLabelProfile().getQuestionNumberLabel(answerOption.questionNumber), t.addx(n2 / 4), n * 2, this.f);
    }
    
    public final void o(final Canvas canvas, final SheetLabel sheetLabel, final SheetTemplate2 sheetTemplate2) {
        final int pixelsInUnit = sheetTemplate2.getPixelsInUnit();
        final int n = pixelsInUnit * 3 / 5;
        final Point2Integer u = this.U(sheetLabel, sheetTemplate2);
        final String[] fourOptionLabels = sheetTemplate2.getLabelProfile().getFourOptionLabels();
        final Point2Integer addx = u.addx(pixelsInUnit * 5 / 2);
        for (int i = 0; i < 4; ++i) {
            this.H(canvas, fourOptionLabels[i], addx.addx(i * pixelsInUnit), n, this.f);
        }
    }
    
    public final void p(final Canvas canvas, final SheetTemplate2 sheetTemplate2, final HeaderProfile headerProfile) {
        if (headerProfile == null) {
            return;
        }
        final Paint paint = new Paint();
        paint.setStyle(Paint$Style.STROKE);
        paint.setStrokeWidth(2.0f);
        paint.setAntiAlias(true);
        paint.setColor(-7829368);
        int n2;
        final int n = n2 = this.a / 2 + headerProfile.getEmptyHeight();
        if (headerProfile.getTitle() != null) {
            final StaticLayout v = this.V(headerProfile.getTitle(), (int)(sheetTemplate2.getTemplatePixelWidth(this.c) - sheetTemplate2.getPixelsInUnit() * 0.6));
            canvas.translate((float)this.a, (float)n);
            ((Layout)v).draw(canvas);
            canvas.translate((float)(-this.a), (float)(-n));
            n2 = (int)(n + ((Layout)v).getHeight() * 1.3);
        }
        int n3 = n2;
        if (headerProfile.getInstruction() != null) {
            final Bitmap scaledBitmap = Bitmap.createScaledBitmap(BitmapFactory.decodeResource(this.g.getResources(), 2131231009), 200, 110, false);
            final StaticLayout r = this.R(headerProfile.getInstruction(), (int)(sheetTemplate2.getTemplatePixelWidth(this.c) - 250 - sheetTemplate2.getPixelsInUnit() * 0.6));
            final int n4 = (int)(this.a + sheetTemplate2.getPixelsInUnit() * 0.3);
            final float n5 = (float)n4;
            final float n6 = (float)n2;
            canvas.translate(n5, n6);
            ((Layout)r).draw(canvas);
            canvas.translate((float)(-n4), (float)(-n2));
            canvas.drawBitmap(scaledBitmap, (float)(canvas.getWidth() - this.a - 250), n6, (Paint)null);
            int height = ((Layout)r).getHeight();
            if (height < 110) {
                height = 110;
            }
            canvas.drawRect((float)this.a, n6, (float)(canvas.getWidth() - this.a), (float)(n2 + height + 10), paint);
            n3 = n2 + (height + 10);
        }
        final int pixelsInUnit = sheetTemplate2.getPixelsInUnit();
        final int p3 = this.P(sheetTemplate2, headerProfile);
        int i = 0;
        while (i < p3) {
            final float n7 = (float)(this.a * 2 / 2);
            final double n8 = n3;
            final double n9 = pixelsInUnit * 1.5;
            final float n10 = (float)(i * n9 + n8);
            final float n11 = (float)(canvas.getWidth() - this.a);
            ++i;
            canvas.drawRect(n7, n10, n11, (float)(n8 + n9 * i), paint);
        }
        final int columns = sheetTemplate2.getColumns(this.c);
        final ArrayList<HeaderLabel> labels = headerProfile.labels;
        if (labels == null) {
            return;
        }
        final Iterator<HeaderLabel> iterator = labels.iterator();
        int n12 = columns;
        int n13 = 1;
        while (iterator.hasNext()) {
            final HeaderLabel headerLabel = iterator.next();
            final int n14 = a$a.a[headerLabel.size.ordinal()];
            if (n14 != 1) {
                if (n14 != 2) {
                    if (n14 != 3) {
                        if (n14 != 4) {
                            continue;
                        }
                        int n15 = n13;
                        if (n12 != columns) {
                            n15 = n13 + 1;
                        }
                        this.q(canvas, n15, 0, n3, headerLabel.label, sheetTemplate2);
                        n12 = 0;
                        n13 = n15;
                    }
                    else if (n12 > 2) {
                        this.q(canvas, n13, columns - n12, n3, headerLabel.label, sheetTemplate2);
                        n12 -= 3;
                    }
                    else {
                        int n16 = n13;
                        if (n12 != columns) {
                            n16 = n13 + 1;
                        }
                        this.q(canvas, n16, 0, n3, headerLabel.label, sheetTemplate2);
                        n12 = columns - 3;
                        n13 = n16;
                    }
                }
                else if (n12 > 1) {
                    this.q(canvas, n13, columns - n12, n3, headerLabel.label, sheetTemplate2);
                    n12 -= 2;
                }
                else {
                    ++n13;
                    this.q(canvas, n13, 0, n3, headerLabel.label, sheetTemplate2);
                    n12 = columns - 2;
                }
            }
            else if (n12 > 0) {
                this.q(canvas, n13, columns - n12, n3, headerLabel.label, sheetTemplate2);
                --n12;
            }
            else {
                ++n13;
                this.q(canvas, n13, 0, n3, headerLabel.label, sheetTemplate2);
                n12 = columns - 1;
            }
        }
    }
    
    public final void q(final Canvas canvas, final int n, int a, final int n2, final String str, final SheetTemplate2 sheetTemplate2) {
        final Paint paint = new Paint();
        paint.setStyle(Paint$Style.FILL_AND_STROKE);
        paint.setAntiAlias(true);
        paint.setColor(-16777216);
        paint.setTextAlign(Paint$Align.LEFT);
        final int pixelsInUnit = sheetTemplate2.getPixelsInUnit();
        final int n3 = pixelsInUnit * 4 / 5;
        final double n4 = pixelsInUnit;
        final int n5 = (int)(1.5 * n4);
        final int n6 = (canvas.getWidth() - this.a * 2) / sheetTemplate2.getColumns(this.c);
        final int a2 = this.a;
        final int n7 = n6 * a;
        final Point2Integer point2Integer = new Point2Integer((int)(a2 + n7 + n4 * 0.3), (int)(n2 + n5 * (n - 0.5)));
        final StringBuilder sb = new StringBuilder();
        sb.append(str);
        sb.append(" :");
        this.H(canvas, sb.toString(), point2Integer, n3, paint);
        if (a > 0) {
            a = this.a;
            final float n8 = (float)(a + n7);
            final float n9 = (float)n2;
            canvas.drawLine(n8, (n - 1) * n5 + n9, (float)(a + n7), n9 + n5 * n, paint);
        }
    }
    
    public final void r(final Canvas canvas, final SheetLabel sheetLabel, final SheetTemplate2 sheetTemplate2) {
        switch (a$a.b[sheetLabel.type.ordinal()]) {
            default: {
                return;
            }
            case 10: {
                this.G(canvas, sheetLabel, sheetTemplate2);
                return;
            }
            case 9: {
                this.j(canvas, sheetLabel, sheetTemplate2);
                return;
            }
            case 8: {
                this.E(canvas, sheetLabel, sheetTemplate2);
                return;
            }
            case 7: {
                this.I(canvas, sheetLabel, sheetTemplate2);
                return;
            }
            case 6: {
                this.C(canvas, sheetLabel, sheetTemplate2);
                return;
            }
            case 5: {
                this.z(canvas, sheetLabel, sheetTemplate2);
                return;
            }
            case 4: {
                this.K(canvas, sheetLabel, sheetTemplate2);
                return;
            }
            case 3: {
                this.m(canvas, sheetLabel, sheetTemplate2);
                return;
            }
            case 2: {
                this.M(canvas, sheetLabel, sheetTemplate2);
                return;
            }
            case 1: {
                this.o(canvas, sheetLabel, sheetTemplate2);
            }
        }
    }
    
    public final void s(final Canvas canvas, final Point2Integer point2Integer, int n) {
        n /= 2;
        final int x = point2Integer.x;
        final float n2 = (float)(x - n);
        final int y = point2Integer.y;
        canvas.drawRect(n2, (float)(y - n), (float)(x + n), (float)(y + n), this.d);
    }
    
    public final void t(final Canvas canvas, final SheetTemplate2 sheetTemplate2) {
        final int n = sheetTemplate2.getRows(this.c) / 5;
        final int columns = sheetTemplate2.getColumns(this.c);
        final int markerWidth = sheetTemplate2.getMarkerWidth();
        for (int i = 0; i < n + 1; ++i) {
            for (int j = 0; j < columns + 1; ++j) {
                this.s(canvas, this.S(i, j, sheetTemplate2), markerWidth);
            }
        }
    }
    
    public final void u(final Canvas canvas, final AnswerOption answerOption, final SheetTemplate2 sheetTemplate2) {
        final MatrixOptionPayload c = bc.c(answerOption.payload);
        int primaryOptions;
        int secondaryOptions;
        if (c != null) {
            primaryOptions = c.primaryOptions;
            secondaryOptions = c.secondaryOptions;
        }
        else {
            secondaryOptions = 5;
            primaryOptions = 4;
        }
        final int n = sheetTemplate2.getMarkerWidth() / 2;
        final int pixelsInUnit = sheetTemplate2.getPixelsInUnit();
        final Point2Integer t = this.T(answerOption, sheetTemplate2);
        this.y(canvas, t, pixelsInUnit, this.f, secondaryOptions);
        int n2;
        for (int i = 0; i < primaryOptions; i = n2) {
            final Point2Integer addx = t.addx(pixelsInUnit * 5 / 2);
            n2 = i + 1;
            final Point2Integer addy = addx.addy(pixelsInUnit * n2);
            for (int j = 0; j < secondaryOptions; ++j) {
                this.e(canvas, addy.addx(j * pixelsInUnit), n, this.e);
            }
        }
        this.H(canvas, sheetTemplate2.getLabelProfile().getQuestionNumberLabel(answerOption.questionNumber), t.addx(pixelsInUnit * 5 / 4), n * 2, this.f);
        this.v(canvas, t, pixelsInUnit, primaryOptions, secondaryOptions, sheetTemplate2, this.f);
    }
    
    public final void v(final Canvas canvas, Point2Integer addx, final int n, final int n2, int i, final SheetTemplate2 sheetTemplate2, final Paint paint) {
        final int n3 = n * 3;
        final int n4 = n3 / 5;
        final String[] matrixSecondary = sheetTemplate2.getLabelProfile().getMatrixSecondary();
        final String[] matrixPrimary = sheetTemplate2.getLabelProfile().getMatrixPrimary();
        final Point2Integer addx2 = addx.addx(n * 5 / 2);
        final int n5 = 0;
        for (int j = 0; j < i; ++j) {
            this.H(canvas, matrixSecondary[j], addx2.addx(j * n), n4, paint);
        }
        addx = addx.addx(n3 / 2);
        i = n5;
        while (i < n2) {
            final String s = matrixPrimary[i];
            ++i;
            this.H(canvas, s, addx.addy(i * n), n4, paint);
        }
    }
    
    public final void w(final Canvas canvas, Point2Integer addy, final int n, final String[] array, final Paint paint) {
        final int n2 = n * 3;
        final int n3 = n2 / 5;
        addy = addy.addx(n2 / 2).addy(n);
        for (int i = 0; i < 10; ++i) {
            this.H(canvas, array[i], addy.addy(i * n), n3, paint);
        }
    }
    
    public final void x(final Canvas canvas, final AnswerOption answerOption, final SheetTemplate2 sheetTemplate2) {
        final NumericalOptionPayload d = bc.d(answerOption.payload);
        if (d != null && (d.digits > 1 || d.hasNegetive)) {
            this.g(canvas, answerOption, sheetTemplate2);
            return;
        }
        final int n = sheetTemplate2.getMarkerWidth() / 2;
        final int pixelsInUnit = sheetTemplate2.getPixelsInUnit();
        final Point2Integer t = this.T(answerOption, sheetTemplate2);
        this.y(canvas, t, pixelsInUnit, this.f, 5);
        if (answerOption.subIndex == 0) {
            this.w(canvas, t, pixelsInUnit, sheetTemplate2.getLabelProfile().getNumericalOptionLabels(), this.f);
        }
        final int n2 = pixelsInUnit * 5 / 2;
        final Point2Integer addy = t.addx(n2).addy(pixelsInUnit);
        for (int i = 0; i < 10; ++i) {
            this.e(canvas, addy.addx(answerOption.subIndex * pixelsInUnit).addy(i * pixelsInUnit), n, this.e);
        }
        this.H(canvas, sheetTemplate2.getLabelProfile().getQuestionNumberLabel(answerOption.questionNumber), t.addx(n2 + answerOption.subIndex * pixelsInUnit), n * 2, this.f);
    }
    
    public final void y(final Canvas canvas, Point2Integer addy, final int n, final Paint paint, final int n2) {
        addy = addy.addx(n * 4 / 2).addy(-(n / 2));
        final int x = addy.x;
        final float n3 = (float)x;
        final int y = addy.y;
        canvas.drawLine(n3, (float)y, (float)(x + n2 * n), (float)y, paint);
    }
    
    public final void z(final Canvas canvas, final SheetLabel sheetLabel, final SheetTemplate2 sheetTemplate2) {
        final int pixelsInUnit = sheetTemplate2.getPixelsInUnit();
        this.H(canvas, sheetTemplate2.getLabelProfile().getRollNoString(), this.U(sheetLabel, sheetTemplate2).addx(pixelsInUnit * 8 / 2), pixelsInUnit * 3 / 5, this.f);
    }
}

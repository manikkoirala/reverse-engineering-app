// 
// Decompiled by Procyon v0.6.0
// 

package com.ekodroid.omrevaluator.templateui.models;

import java.io.Serializable;

public class PartialAnswerOptionKey implements Serializable
{
    String markedPayload;
    boolean[] markedValues;
    double partialMarks;
    String partialType;
    String payload;
    int questionNumber;
    int sectionId;
    int subjectId;
    AnswerOption.AnswerOptionType type;
    
    public PartialAnswerOptionKey(final AnswerOption.AnswerOptionType type, final int questionNumber, final boolean[] markedValues, final int subjectId, final int sectionId, final double partialMarks, final String payload, final String markedPayload, final String partialType) {
        this.type = type;
        this.questionNumber = questionNumber;
        this.markedValues = markedValues;
        this.subjectId = subjectId;
        this.sectionId = sectionId;
        this.partialMarks = partialMarks;
        this.payload = payload;
        this.markedPayload = markedPayload;
        this.partialType = partialType;
    }
    
    public String getMarkedPayload() {
        return this.markedPayload;
    }
    
    public boolean[] getMarkedValues() {
        return this.markedValues;
    }
    
    public double getPartialMarks() {
        return this.partialMarks;
    }
    
    public String getPartialType() {
        return this.partialType;
    }
    
    public String getPayload() {
        return this.payload;
    }
    
    public int getQuestionNumber() {
        return this.questionNumber;
    }
    
    public int getSectionId() {
        return this.sectionId;
    }
    
    public int getSubjectId() {
        return this.subjectId;
    }
    
    public AnswerOption.AnswerOptionType getType() {
        return this.type;
    }
    
    public void setMarkedValues(final boolean[] markedValues) {
        this.markedValues = markedValues;
    }
    
    public void setPartialMarks(final double partialMarks) {
        this.partialMarks = partialMarks;
    }
}

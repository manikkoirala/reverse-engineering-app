// 
// Decompiled by Procyon v0.6.0
// 

package com.ekodroid.omrevaluator.templateui.models;

import java.io.Serializable;

public class Point2Double implements Serializable
{
    public double x;
    public double y;
    
    public Point2Double() {
        this.x = 0.0;
        this.y = 0.0;
    }
    
    public Point2Double(final double x, final double y) {
        this.x = x;
        this.y = y;
    }
}

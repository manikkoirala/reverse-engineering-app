// 
// Decompiled by Procyon v0.6.0
// 

package com.ekodroid.omrevaluator.templateui.models;

import java.util.ArrayList;
import java.io.Serializable;

public class HeaderProfile implements Serializable
{
    private int emptyHeight;
    private String headerProfileName;
    private String instruction;
    ArrayList<HeaderLabel> labels;
    private String title;
    
    public HeaderProfile() {
        this.labels = new ArrayList<HeaderLabel>();
        this.emptyHeight = 0;
        this.headerProfileName = "Default";
        this.title = null;
        this.instruction = null;
        this.labels = new ArrayList<HeaderLabel>();
    }
    
    public HeaderProfile(final String headerProfileName, final ArrayList<HeaderLabel> labels, final String title, final String instruction, final int emptyHeight) {
        new ArrayList();
        this.labels = labels;
        this.headerProfileName = headerProfileName;
        this.title = title;
        this.instruction = instruction;
        this.emptyHeight = emptyHeight;
    }
    
    public static HeaderProfile getBlankHeaderprofile() {
        return new HeaderProfile("Blank", new ArrayList<HeaderLabel>(), null, null, 60);
    }
    
    public static HeaderProfile getDefaultHeaderprofile() {
        final ArrayList list = new ArrayList();
        final HeaderLabel.Size medium = HeaderLabel.Size.MEDIUM;
        list.add(new HeaderLabel(medium, "NAME"));
        list.add(new HeaderLabel(medium, "EXAM"));
        list.add(new HeaderLabel(HeaderLabel.Size.SMALL, "DATE"));
        return new HeaderProfile(ok.a, list, null, null, 0);
    }
    
    public static HeaderProfile getDefaultMediumHeaderprofile() {
        final ArrayList list = new ArrayList();
        final HeaderLabel.Size medium = HeaderLabel.Size.MEDIUM;
        list.add(new HeaderLabel(medium, "NAME"));
        list.add(new HeaderLabel(medium, "EXAM"));
        list.add(new HeaderLabel(HeaderLabel.Size.SMALL, "DATE"));
        return new HeaderProfile("Default Medium", list, null, "Instruction for filling the sheet\n  1. This sheet should not be folded or crushed\n  2. Use only blue/black ball pen or 2HB pencil\n  3. Circle should darkened completely and properly\n  4. Erase marked circle completely for deselect", 0);
    }
    
    public int getEmptyHeight() {
        return this.emptyHeight;
    }
    
    public String getHeaderProfileName() {
        return this.headerProfileName;
    }
    
    public String getInstruction() {
        return this.instruction;
    }
    
    public ArrayList<HeaderLabel> getLabels() {
        return this.labels;
    }
    
    public String getTitle() {
        return this.title;
    }
    
    public void setEmptyHeight(final int emptyHeight) {
        this.emptyHeight = emptyHeight;
    }
    
    public void setInstruction(final String instruction) {
        this.instruction = instruction;
    }
    
    public void setTitle(final String title) {
        this.title = title;
    }
}

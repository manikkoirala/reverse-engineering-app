// 
// Decompiled by Procyon v0.6.0
// 

package com.ekodroid.omrevaluator.templateui.models;

import java.io.Serializable;

public class HeaderLabel implements Serializable
{
    public String label;
    public Size size;
    
    public HeaderLabel(final Size size, final String label) {
        this.size = size;
        this.label = label;
    }
    
    public enum Size
    {
        private static final Size[] $VALUES;
        
        FULLWIDTH, 
        LARGE, 
        MEDIUM, 
        SMALL;
        
        private static /* synthetic */ Size[] $values() {
            return new Size[] { Size.SMALL, Size.MEDIUM, Size.LARGE, Size.FULLWIDTH };
        }
        
        static {
            $VALUES = $values();
        }
    }
}

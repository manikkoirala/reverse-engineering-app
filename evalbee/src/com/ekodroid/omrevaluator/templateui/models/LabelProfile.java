// 
// Decompiled by Procyon v0.6.0
// 

package com.ekodroid.omrevaluator.templateui.models;

import java.io.Serializable;

public class LabelProfile implements Serializable
{
    private String ReportCardString;
    private String attemptedString;
    private String classString;
    private String correctAnswerString;
    private String correctString;
    private String dateString;
    private String[] eightOptionLabels;
    private String examNameString;
    private String[] examSetLabels;
    private String examSetString;
    private String[] fiveOptionLabels;
    private String[] fourOptionLabels;
    private String gradeString;
    private String incorrectAnswerString;
    private String labelProfileName;
    private String marksString;
    private String[] matrixPrimary;
    private String[] matrixSecondary;
    private String nameString;
    private String[] numericalOptionLabels;
    private String percentageString;
    private String queNoString;
    private String[] questionNumberLabels;
    private String rankString;
    private String[] rollDigitsLabels;
    private String rollNoString;
    private int rollStartDigit;
    private String[] sixOptionLabels;
    private String subjectString;
    private String[] tenOptionLabels;
    private String[] threeOptionLabels;
    private String totalMarksString;
    private String[] trueOrFalseLabel;
    
    public LabelProfile() {
        this.labelProfileName = "Default";
        this.rollNoString = "Roll No";
        this.examSetString = "Exam Set";
        this.examNameString = "Exam";
        this.dateString = "Date";
        this.nameString = "Name";
        this.rankString = "Rank";
        this.marksString = "Marks";
        this.gradeString = "Grade";
        this.classString = "Class";
        this.percentageString = "Percentage";
        this.correctAnswerString = "Correct Answers";
        this.incorrectAnswerString = "Incorrect Answers";
        this.subjectString = "Subject";
        this.ReportCardString = "Report Card";
        this.queNoString = "Q No";
        this.attemptedString = "Attempted";
        this.correctString = "Correct";
        this.totalMarksString = "Total Marks";
        this.fiveOptionLabels = new String[] { "A", "B", "C", "D", "E" };
        this.sixOptionLabels = new String[] { "A", "B", "C", "D", "E", "F" };
        this.eightOptionLabels = new String[] { "A", "B", "C", "D", "E", "F", "G", "H" };
        this.tenOptionLabels = new String[] { "A", "B", "C", "D", "E", "F", "G", "H", "I", "J" };
        this.fourOptionLabels = new String[] { "A", "B", "C", "D" };
        this.threeOptionLabels = new String[] { "A", "B", "C" };
        this.trueOrFalseLabel = new String[] { "True", "False" };
        this.matrixPrimary = new String[] { "A", "B", "C", "D", "E", "F", "G", "H", "I", "J" };
        this.matrixSecondary = new String[] { "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y" };
        this.examSetLabels = new String[] { "A", "B", "C", "D", "E", "F", "G", "H", "I", "J" };
        this.questionNumberLabels = this.getDefaultquestionLabels();
        this.numericalOptionLabels = new String[] { "0", "1", "2", "3", "4", "5", "6", "7", "8", "9" };
        this.rollDigitsLabels = new String[] { "0", "1", "2", "3", "4", "5", "6", "7", "8", "9" };
        this.rollStartDigit = 0;
    }
    
    private String[] getDefaultquestionLabels() {
        final String[] array = new String[310];
        int j;
        for (int i = 0; i < 310; i = j) {
            final StringBuilder sb = new StringBuilder();
            sb.append("");
            j = i + 1;
            sb.append(j);
            array[i] = sb.toString();
        }
        return array;
    }
    
    private String[] getMatrixCombineLabels(final AnswerOptionKey answerOptionKey) {
        final MatrixOptionPayload c = bc.c(answerOptionKey.payload);
        int primaryOptions;
        int secondaryOptions;
        if (c != null) {
            primaryOptions = c.primaryOptions;
            secondaryOptions = c.secondaryOptions;
        }
        else {
            primaryOptions = 4;
            secondaryOptions = 5;
        }
        final String[] matrixPrimary = this.getMatrixPrimary();
        final String[] matrixSecondary = this.getMatrixSecondary();
        final int n = primaryOptions * secondaryOptions;
        final String[] array = new String[n];
        for (int i = 0; i < n; ++i) {
            final int n2 = i / secondaryOptions;
            final StringBuilder sb = new StringBuilder();
            sb.append(matrixPrimary[n2]);
            sb.append(matrixSecondary[i % secondaryOptions]);
            array[i] = sb.toString();
        }
        return array;
    }
    
    private String[] getNumericalCombinationLabels(final AnswerOptionKey answerOptionKey) {
        final NumericalOptionPayload d = bc.d(answerOptionKey.payload);
        final int n = d.digits * 10;
        final boolean hasNegetive = d.hasNegetive;
        int n2 = n;
        if (hasNegetive) {
            n2 = n + 1;
        }
        final String[] array = new String[n2];
        if (hasNegetive) {
            array[n2 - 1] = "-";
        }
        for (int i = 0; i < n2; ++i) {
            final int digits = d.digits;
            final int n3 = i / digits;
            final StringBuilder sb = new StringBuilder();
            sb.append(this.getNumericalOptionLabels()[d.digits - i % digits]);
            sb.append("x");
            sb.append(this.getNumericalOptionLabels()[n3]);
            array[i] = sb.toString();
        }
        return array;
    }
    
    public String getAttemptedString() {
        return this.attemptedString;
    }
    
    public String getClassString() {
        return this.classString;
    }
    
    public String getCorrectAnswerString() {
        return this.correctAnswerString;
    }
    
    public String getCorrectString() {
        return this.correctString;
    }
    
    public String getDateString() {
        return this.dateString;
    }
    
    public String[] getEightOptionLabels() {
        return this.eightOptionLabels;
    }
    
    public String getExamNameString() {
        return this.examNameString;
    }
    
    public String[] getExamSetLabels() {
        return this.examSetLabels;
    }
    
    public String getExamSetString() {
        return this.examSetString;
    }
    
    public String[] getFiveOptionLabels() {
        return this.fiveOptionLabels;
    }
    
    public String[] getFourOptionLabels() {
        return this.fourOptionLabels;
    }
    
    public String getGradeString() {
        return this.gradeString;
    }
    
    public String getIncorrectAnswerString() {
        return this.incorrectAnswerString;
    }
    
    public String getLabelProfileName() {
        return this.labelProfileName;
    }
    
    public String getMarksString() {
        return this.marksString;
    }
    
    public String[] getMatrixPrimary() {
        return this.matrixPrimary;
    }
    
    public String[] getMatrixSecondary() {
        return this.matrixSecondary;
    }
    
    public String getNameString() {
        return this.nameString;
    }
    
    public String[] getNumericalOptionLabels() {
        return this.numericalOptionLabels;
    }
    
    public String[] getOptionLabels(final AnswerOptionKey answerOptionKey) {
        switch (LabelProfile$a.a[answerOptionKey.type.ordinal()]) {
            default: {
                return null;
            }
            case 9: {
                return this.getTenOptionLabels();
            }
            case 8: {
                return this.getEightOptionLabels();
            }
            case 7: {
                return this.getSixOptionLabels();
            }
            case 6: {
                return this.getNumericalCombinationLabels(answerOptionKey);
            }
            case 5: {
                return this.getMatrixCombineLabels(answerOptionKey);
            }
            case 4: {
                return this.getTrueOrFalseLabel();
            }
            case 3: {
                return this.getThreeOptionLabels();
            }
            case 2: {
                return this.getFourOptionLabels();
            }
            case 1: {
                return this.getFiveOptionLabels();
            }
        }
    }
    
    public String getPercentageString() {
        return this.percentageString;
    }
    
    public String getQueNoString() {
        return this.queNoString;
    }
    
    public String getQuestionNumberLabel(final int i) {
        final String[] questionNumberLabels = this.questionNumberLabels;
        if (i > questionNumberLabels.length) {
            final StringBuilder sb = new StringBuilder();
            sb.append("");
            sb.append(i);
            return sb.toString();
        }
        return questionNumberLabels[i - 1];
    }
    
    public String[] getQuestionNumberLabels() {
        return this.questionNumberLabels;
    }
    
    public String getRankString() {
        return this.rankString;
    }
    
    public String getReportCardString() {
        return this.ReportCardString;
    }
    
    public String[] getRollDigitsLabels() {
        return this.rollDigitsLabels;
    }
    
    public String getRollNoString() {
        return this.rollNoString;
    }
    
    public int getRollStartDigit() {
        return this.rollStartDigit;
    }
    
    public String[] getSixOptionLabels() {
        return this.sixOptionLabels;
    }
    
    public String getSubjectString() {
        return this.subjectString;
    }
    
    public String[] getTenOptionLabels() {
        return this.tenOptionLabels;
    }
    
    public String[] getThreeOptionLabels() {
        return this.threeOptionLabels;
    }
    
    public String getTotalMarksString() {
        return this.totalMarksString;
    }
    
    public String[] getTrueOrFalseLabel() {
        return this.trueOrFalseLabel;
    }
    
    public void setAttemptedString(final String attemptedString) {
        this.attemptedString = attemptedString;
    }
    
    public void setClassString(final String classString) {
        this.classString = classString;
    }
    
    public void setCorrectAnswerString(final String correctAnswerString) {
        this.correctAnswerString = correctAnswerString;
    }
    
    public void setCorrectString(final String correctString) {
        this.correctString = correctString;
    }
    
    public void setDateString(final String dateString) {
        this.dateString = dateString;
    }
    
    public void setEightOptionLabels(final String[] eightOptionLabels) {
        this.eightOptionLabels = eightOptionLabels;
    }
    
    public void setExamNameString(final String examNameString) {
        this.examNameString = examNameString;
    }
    
    public void setExamSetLabels(final String[] examSetLabels) {
        this.examSetLabels = examSetLabels;
    }
    
    public void setExamSetString(final String examSetString) {
        this.examSetString = examSetString;
    }
    
    public void setFiveOptionLabels(final String[] fiveOptionLabels) {
        this.fiveOptionLabels = fiveOptionLabels;
    }
    
    public void setFourOptionLabels(final String[] fourOptionLabels) {
        this.fourOptionLabels = fourOptionLabels;
    }
    
    public void setGradeString(final String gradeString) {
        this.gradeString = gradeString;
    }
    
    public void setIncorrectAnswerString(final String incorrectAnswerString) {
        this.incorrectAnswerString = incorrectAnswerString;
    }
    
    public void setLabelProfileName(final String labelProfileName) {
        this.labelProfileName = labelProfileName;
    }
    
    public void setMarksString(final String marksString) {
        this.marksString = marksString;
    }
    
    public void setMatrixPrimary(final String[] matrixPrimary) {
        this.matrixPrimary = matrixPrimary;
    }
    
    public void setMatrixSecondary(final String[] matrixSecondary) {
        this.matrixSecondary = matrixSecondary;
    }
    
    public void setNameString(final String nameString) {
        this.nameString = nameString;
    }
    
    public void setNumericalOptionLabels(final String[] numericalOptionLabels) {
        this.numericalOptionLabels = numericalOptionLabels;
    }
    
    public void setPercentageString(final String percentageString) {
        this.percentageString = percentageString;
    }
    
    public void setQueNoString(final String queNoString) {
        this.queNoString = queNoString;
    }
    
    public void setQuestionNumberLabels(final String[] questionNumberLabels) {
        this.questionNumberLabels = questionNumberLabels;
    }
    
    public void setRankString(final String rankString) {
        this.rankString = rankString;
    }
    
    public void setReportCardString(final String reportCardString) {
        this.ReportCardString = reportCardString;
    }
    
    public void setRollDigitsLabels(final String[] rollDigitsLabels) {
        this.rollDigitsLabels = rollDigitsLabels;
    }
    
    public void setRollNoString(final String rollNoString) {
        this.rollNoString = rollNoString;
    }
    
    public void setRollStartDigit(final int rollStartDigit) {
        this.rollStartDigit = rollStartDigit;
    }
    
    public void setSixOptionLabels(final String[] sixOptionLabels) {
        this.sixOptionLabels = sixOptionLabels;
    }
    
    public void setSubjectString(final String subjectString) {
        this.subjectString = subjectString;
    }
    
    public void setTenOptionLabels(final String[] tenOptionLabels) {
        this.tenOptionLabels = tenOptionLabels;
    }
    
    public void setThreeOptionLabels(final String[] threeOptionLabels) {
        this.threeOptionLabels = threeOptionLabels;
    }
    
    public void setTotalMarksString(final String totalMarksString) {
        this.totalMarksString = totalMarksString;
    }
    
    public void setTrueOrFalseLabel(final String[] trueOrFalseLabel) {
        this.trueOrFalseLabel = trueOrFalseLabel;
    }
}

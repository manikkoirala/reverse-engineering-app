// 
// Decompiled by Procyon v0.6.0
// 

package com.ekodroid.omrevaluator.templateui.models;

public class QueEditDataModel
{
    public ViewType a;
    public String b;
    public String c;
    public String d;
    public int e;
    public int f;
    public int g;
    public double h;
    public double i;
    public boolean j;
    
    public QueEditDataModel(final ViewType a, final String b, final String c, final int e, final int f, final int g, final double h, final double i, final boolean j, final String d) {
        this.a = a;
        this.b = b;
        this.c = c;
        this.e = e;
        this.f = f;
        this.g = g;
        this.h = h;
        this.i = i;
        this.d = d;
        this.j = j;
    }
    
    public double a() {
        return this.h;
    }
    
    public double b() {
        return this.i;
    }
    
    public String c() {
        return this.d;
    }
    
    public int d() {
        return this.e;
    }
    
    public int e() {
        return this.g;
    }
    
    public String f() {
        return this.c;
    }
    
    public int g() {
        return this.f;
    }
    
    public String h() {
        return this.b;
    }
    
    public ViewType i() {
        return this.a;
    }
    
    public boolean j() {
        return this.j;
    }
    
    public enum ViewType
    {
        private static final ViewType[] $VALUES;
        
        LABEL, 
        QUESTION;
        
        private static /* synthetic */ ViewType[] $values() {
            return new ViewType[] { ViewType.LABEL, ViewType.QUESTION };
        }
        
        static {
            $VALUES = $values();
        }
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.ekodroid.omrevaluator.templateui.models;

import java.io.Serializable;

public class SheetLabel implements Serializable
{
    int column;
    int pageIndex;
    int row;
    String text;
    LableType type;
    
    public SheetLabel(final LableType type, final int row, final int column, final int pageIndex, final String text) {
        this.text = text;
        this.row = row;
        this.column = column;
        this.type = type;
        this.pageIndex = pageIndex;
    }
    
    public int getColumn() {
        return this.column;
    }
    
    public int getRow() {
        return this.row;
    }
    
    public String getText() {
        return this.text;
    }
    
    public LableType getType() {
        return this.type;
    }
    
    public void setColumn(final int column) {
        this.column = column;
    }
    
    public void setRow(final int row) {
        this.row = row;
    }
    
    public void setText(final String text) {
        this.text = text;
    }
    
    public void setType(final LableType type) {
        this.type = type;
    }
    
    public enum LableType
    {
        private static final LableType[] $VALUES;
        
        OPTION_ABC, 
        OPTION_ABCD, 
        OPTION_ABCDE, 
        OPTION_ABCDEF, 
        OPTION_ABCDEFGH, 
        OPTION_ABCDEFGHIJ, 
        OPTION_TRUEORFALSE, 
        ROLL_LABEL, 
        SET_LABEL, 
        TEXT;
        
        private static /* synthetic */ LableType[] $values() {
            return new LableType[] { LableType.TEXT, LableType.OPTION_ABCD, LableType.OPTION_ABC, LableType.OPTION_ABCDE, LableType.OPTION_TRUEORFALSE, LableType.ROLL_LABEL, LableType.SET_LABEL, LableType.OPTION_ABCDEF, LableType.OPTION_ABCDEFGH, LableType.OPTION_ABCDEFGHIJ };
        }
        
        static {
            $VALUES = $values();
        }
    }
}

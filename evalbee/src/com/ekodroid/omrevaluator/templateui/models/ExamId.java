// 
// Decompiled by Procyon v0.6.0
// 

package com.ekodroid.omrevaluator.templateui.models;

import java.io.Serializable;

public class ExamId implements Serializable
{
    private String className;
    private String examDate;
    private String examName;
    
    public ExamId(final String examName, final String className, final String examDate) {
        this.className = className;
        this.examName = examName;
        this.examDate = examDate;
    }
    
    public String getClassName() {
        return this.className;
    }
    
    public String getExamDate() {
        return this.examDate;
    }
    
    public String getExamName() {
        return this.examName;
    }
}

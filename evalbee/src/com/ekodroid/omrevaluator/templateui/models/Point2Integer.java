// 
// Decompiled by Procyon v0.6.0
// 

package com.ekodroid.omrevaluator.templateui.models;

import java.io.Serializable;

public class Point2Integer implements Serializable
{
    int x;
    int y;
    
    public Point2Integer(final int x, final int y) {
        this.x = x;
        this.y = y;
    }
    
    public Point2Integer addx(final int n) {
        return new Point2Integer(this.x + n, this.y);
    }
    
    public Point2Integer addy(final int n) {
        return new Point2Integer(this.x, this.y + n);
    }
    
    public int getX() {
        return this.x;
    }
    
    public int getY() {
        return this.y;
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.ekodroid.omrevaluator.templateui.models;

import java.io.Serializable;

public class NumericalOptionPayload implements Serializable
{
    public int decimalDigits;
    public int digits;
    public boolean hasNegetive;
    
    public NumericalOptionPayload() {
    }
    
    public NumericalOptionPayload(final boolean hasNegetive, final int digits, final int decimalDigits) {
        this.hasNegetive = hasNegetive;
        this.digits = digits;
        this.decimalDigits = decimalDigits;
    }
}

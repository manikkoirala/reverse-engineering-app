// 
// Decompiled by Procyon v0.6.0
// 

package com.ekodroid.omrevaluator.templateui.models;

import java.io.Serializable;

public class DecimalRange implements Serializable
{
    double lower;
    double upper;
    
    public DecimalRange(final double lower, final double upper) {
        this.lower = lower;
        this.upper = upper;
    }
    
    public double getLower() {
        return this.lower;
    }
    
    public double getUpper() {
        return this.upper;
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.ekodroid.omrevaluator.templateui.models;

import java.io.Serializable;

public class AnswerOption implements Serializable
{
    public int column;
    public double correctMarks;
    public double incorrectMarks;
    public int pageIndex;
    public boolean partialAllowed;
    public double partialMarks;
    public String payload;
    public int questionNumber;
    public int row;
    public int sectionId;
    public int subIndex;
    public int subjectId;
    public AnswerOptionType type;
    
    public AnswerOption(final AnswerOptionType type, final int questionNumber, final int row, final int column, final int subIndex, final int subjectId, final int sectionId, final double correctMarks, final double incorrectMarks, final boolean partialAllowed, final String payload, final int pageIndex) {
        this.type = type;
        this.questionNumber = questionNumber;
        this.row = row;
        this.column = column;
        this.subIndex = subIndex;
        this.subjectId = subjectId;
        this.sectionId = sectionId;
        this.correctMarks = correctMarks;
        this.incorrectMarks = incorrectMarks;
        this.partialAllowed = partialAllowed;
        this.payload = payload;
        this.pageIndex = pageIndex;
    }
    
    public enum AnswerOptionType
    {
        private static final AnswerOptionType[] $VALUES;
        
        DECIMAL, 
        EIGHTOPTION, 
        EXAMSET_BLOCK, 
        FIVEOPTION, 
        FOUROPTION, 
        ID_BLOCK, 
        MATRIX, 
        NUMARICAL, 
        SIXOPTION, 
        TENOPTION, 
        THREEOPTION, 
        TRUEORFALSE;
        
        private static /* synthetic */ AnswerOptionType[] $values() {
            return new AnswerOptionType[] { AnswerOptionType.FOUROPTION, AnswerOptionType.FIVEOPTION, AnswerOptionType.THREEOPTION, AnswerOptionType.MATRIX, AnswerOptionType.NUMARICAL, AnswerOptionType.DECIMAL, AnswerOptionType.ID_BLOCK, AnswerOptionType.TRUEORFALSE, AnswerOptionType.EXAMSET_BLOCK, AnswerOptionType.SIXOPTION, AnswerOptionType.EIGHTOPTION, AnswerOptionType.TENOPTION };
        }
        
        static {
            $VALUES = $values();
        }
    }
}

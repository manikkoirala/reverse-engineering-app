// 
// Decompiled by Procyon v0.6.0
// 

package com.ekodroid.omrevaluator.templateui.models;

import java.io.Serializable;

public class InvalidQuestionSet implements Serializable
{
    int examSet;
    private String invalidQueMarks;
    private int[] invalidQuestions;
    
    public InvalidQuestionSet(final int examSet, final int[] invalidQuestions, final String invalidQueMarks) {
        this.examSet = examSet;
        this.invalidQuestions = invalidQuestions;
        this.invalidQueMarks = invalidQueMarks;
    }
    
    public int getExamSet() {
        return this.examSet;
    }
    
    public String getInvalidQueMarks() {
        return this.invalidQueMarks;
    }
    
    public int[] getInvalidQuestions() {
        return this.invalidQuestions;
    }
    
    public void setExamSet(final int examSet) {
        this.examSet = examSet;
    }
    
    public void setInvalidQueMarks(final String invalidQueMarks) {
        this.invalidQueMarks = invalidQueMarks;
    }
    
    public void setInvalidQuestions(final int[] invalidQuestions) {
        this.invalidQuestions = invalidQuestions;
    }
}

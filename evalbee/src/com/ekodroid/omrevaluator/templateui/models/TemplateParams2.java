// 
// Decompiled by Procyon v0.6.0
// 

package com.ekodroid.omrevaluator.templateui.models;

import java.io.Serializable;

public class TemplateParams2 implements Serializable
{
    private int examSets;
    private int rollDigits;
    private Subject2[] subjects;
    
    public TemplateParams2(final int rollDigits, final int examSets, final Subject2[] subjects) {
        this.rollDigits = rollDigits;
        this.examSets = examSets;
        this.subjects = subjects;
    }
    
    public int getExamSets() {
        return this.examSets;
    }
    
    public int getRollDigits() {
        return this.rollDigits;
    }
    
    public Subject2[] getSubjects() {
        return this.subjects;
    }
    
    public void setExamSets(final int examSets) {
        this.examSets = examSets;
    }
    
    public void setRollDigits(final int rollDigits) {
        this.rollDigits = rollDigits;
    }
    
    public void setSubjects(final Subject2[] subjects) {
        this.subjects = subjects;
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.ekodroid.omrevaluator.templateui.models;

import java.util.ArrayList;

public class TemplateBuilder
{
    public int a;
    public int b;
    public int c;
    public AnswerOption d;
    public ArrayList e;
    public ArrayList f;
    public int g;
    public BuildType h;
    public TemplateParams2 i;
    public ArrayList j;
    public int k;
    
    public TemplateBuilder(final BuildType h, final TemplateParams2 i) {
        this.c = 30;
        this.g = 0;
        this.k = 0;
        this.h = h;
        this.i = i;
    }
    
    public final void a() {
        final int a = this.a;
        final int[] array = new int[a];
        for (int i = 0; i < a; ++i) {
            array[i] = 8;
        }
        this.j.add(new PageLayout(this.b, this.a, array));
    }
    
    public SheetTemplate2 b(final wu1 wu1) {
        Point2Integer point2Integer = new Point2Integer(wu1.a, wu1.c);
        SheetTemplate2 c = null;
    Label_0017:
        while (true) {
            SheetTemplate2 c2;
            for (c = this.c(point2Integer, false); c == null; c = c2) {
                final Point2Integer m = this.m(point2Integer);
                final int y = m.y;
                final int d = wu1.d;
                if (y <= d) {
                    point2Integer = m;
                    if (m.x <= wu1.b) {
                        continue Label_0017;
                    }
                }
                c2 = this.c(new Point2Integer(wu1.b, d), true);
                point2Integer = m;
            }
            break;
        }
        return c;
    }
    
    public SheetTemplate2 c(Point2Integer point2Integer, final boolean b) {
        this.d = null;
        this.e = new ArrayList();
        this.f = new ArrayList();
        this.j = new ArrayList();
        int i = 0;
        this.k = 0;
        this.a = point2Integer.y;
        this.b = point2Integer.x;
        this.a();
        final Point2Integer point2Integer2 = new Point2Integer(0, 1);
        this.g = 0;
        Label_0091: {
            if (b) {
                break Label_0091;
            }
            try {
                this.d(point2Integer);
                point2Integer = point2Integer2;
                if (this.i.getExamSets() > 1) {
                    point2Integer = this.s(point2Integer2, this.i.getRollDigits(), this.i.getExamSets());
                }
                point2Integer = this.t(point2Integer, this.i.getRollDigits());
                while (i < this.i.getSubjects().length) {
                    point2Integer = this.x(this.i.getSubjects()[i], point2Integer, i, b);
                    ++i;
                }
                final PageLayout[] n = this.n(this.j);
                final int c = this.c;
                return new SheetTemplate2(n, c, c * 2 / 3, this.d, this.e, this.f, this.i);
            }
            catch (final TemplateSizeTooSmallException ex) {
                ex.printStackTrace();
                return null;
            }
        }
    }
    
    public final boolean d(final Point2Integer point2Integer) {
        final int o = this.o(this.i);
        final int y = point2Integer.y;
        this.a = y;
        final int x = point2Integer.x;
        this.b = x;
        if (o - 5 <= y * x) {
            return true;
        }
        throw new TemplateSizeTooSmallException();
    }
    
    public final int e(final Point2Integer point2Integer) {
        return this.b - point2Integer.x;
    }
    
    public final int f(final Section2 section2) {
        final int n = TemplateBuilder$a.a[section2.type.ordinal()];
        int n3;
        int n2 = n3 = 12;
        switch (n) {
            default: {
                return 0;
            }
            case 8:
            case 9:
            case 10: {
                return 3;
            }
            case 7: {
                if (bc.b(section2.payload).decimalAllowed) {
                    n2 = 13;
                }
                n3 = n2 + 1;
            }
            case 6: {
                return n3;
            }
            case 5: {
                final MatrixOptionPayload c = bc.c(section2.payload);
                int primaryOptions;
                if (c != null) {
                    primaryOptions = c.primaryOptions;
                }
                else {
                    primaryOptions = 4;
                }
                return primaryOptions + 2;
            }
            case 1:
            case 2:
            case 3:
            case 4: {
                return 3;
            }
        }
    }
    
    public final int g(final Subject2 subject2) {
        return this.f(subject2.getSections()[0]) + 1;
    }
    
    public final int h() {
        return ++this.g;
    }
    
    public final Point2Integer i(final Point2Integer point2Integer, final boolean b, final boolean b2) {
        int n;
        if (b) {
            n = 13;
        }
        else {
            n = 12;
        }
        Point2Integer r = point2Integer;
        if (this.e(point2Integer) < n) {
            r = this.r(point2Integer, b2);
        }
        return r;
    }
    
    public final Point2Integer j(final Point2Integer point2Integer, final boolean b) {
        Point2Integer r = point2Integer;
        if (this.e(point2Integer) < 1) {
            r = this.r(point2Integer, b);
        }
        return r;
    }
    
    public final Point2Integer k(final Point2Integer point2Integer, final Section2 section2, final boolean b) {
        final MatrixOptionPayload c = bc.c(section2.payload);
        int n;
        if (c != null) {
            n = c.primaryOptions + 1;
        }
        else {
            n = 5;
        }
        Point2Integer r = point2Integer;
        if (this.e(point2Integer) < n) {
            r = this.r(point2Integer, b);
        }
        return r;
    }
    
    public final Point2Integer l(final Point2Integer point2Integer, final boolean b) {
        Point2Integer r = point2Integer;
        if (this.e(point2Integer) < 11) {
            r = this.r(point2Integer, b);
        }
        return r;
    }
    
    public final Point2Integer m(final Point2Integer point2Integer) {
        final int x = point2Integer.x;
        final double n = x;
        final int y = point2Integer.y;
        final double n2 = n * 1.0 / ((y + 1) * 8.0 + 1.0);
        final double n3 = (x + 5) * 1.0 / (y * 8.0 + 1.0);
        double n4;
        if (n2 > 1.4142857142857144) {
            n4 = n2 / 1.4142857142857144;
        }
        else {
            n4 = 1.4142857142857144 / n2;
        }
        double n5;
        if (n3 > 1.4142857142857144) {
            n5 = n3 / 1.4142857142857144;
        }
        else {
            n5 = 1.4142857142857144 / n3;
        }
        if (n4 < n5) {
            return point2Integer.addy(1);
        }
        return point2Integer.addx(5);
    }
    
    public final PageLayout[] n(final ArrayList list) {
        final int size = list.size();
        final PageLayout[] array = new PageLayout[size];
        for (int i = 0; i < size; ++i) {
            array[i] = (PageLayout)list.get(i);
        }
        return array;
    }
    
    public int o(final TemplateParams2 templateParams2) {
        final Subject2[] subjects = templateParams2.getSubjects();
        final int length = subjects.length;
        int n = 12;
        for (int i = 0; i < length; ++i) {
            n += this.q(subjects[i]);
        }
        return n;
    }
    
    public final int p(final Section2 section2) {
        int n = 0;
        switch (TemplateBuilder$a.a[section2.type.ordinal()]) {
            default: {
                return 1;
            }
            case 6: {
                n = ((section2.noOfQue - 1) / 5 + 1) * 11;
                break;
            }
            case 5: {
                n = section2.noOfQue * 5;
                break;
            }
            case 1:
            case 2:
            case 3:
            case 4:
            case 8:
            case 9:
            case 10: {
                final int noOfQue = section2.noOfQue;
                return noOfQue + 1 + (noOfQue / 5 + 1);
            }
        }
        return n + 1;
    }
    
    public final int q(final Subject2 subject2) {
        final Section2[] sections = subject2.getSections();
        final int length = sections.length;
        int n = 2;
        for (int i = 0; i < length; ++i) {
            n += this.p(sections[i]);
        }
        return n;
    }
    
    public final Point2Integer r(final Point2Integer point2Integer, final boolean b) {
        point2Integer.x = 0;
        final int y = point2Integer.y + 1;
        point2Integer.y = y;
        if (y <= this.a) {
            return point2Integer;
        }
        if (b) {
            this.a();
            ++this.k;
            return new Point2Integer(0, 1);
        }
        throw new TemplateSizeTooSmallException();
    }
    
    public final Point2Integer s(Point2Integer u, int n, final int n2) {
        u = this.u(SheetLabel.LableType.SET_LABEL, u, null);
        final int n3 = 5;
        if (n < 5) {
            n = n3;
        }
        this.d = new AnswerOption(AnswerOption.AnswerOptionType.EXAMSET_BLOCK, 0, u.x + 1, u.y, n2, 0, 0, 0.0, 0.0, false, null, this.k);
        return u.addx(((n2 - 1) / n + 1) * 2);
    }
    
    public final Point2Integer t(final Point2Integer point2Integer, final int n) {
        final int x = point2Integer.x;
        if (x + 12 <= this.b) {
            this.f.add(new SheetLabel(SheetLabel.LableType.ROLL_LABEL, x + 1, point2Integer.y, this.k, null));
            this.e.add(new AnswerOption(AnswerOption.AnswerOptionType.ID_BLOCK, 0, point2Integer.x + 1, point2Integer.y, n, 0, 0, 0.0, 0.0, false, null, this.k));
            this.y(point2Integer.y, n, this.k);
            return point2Integer.addx(12);
        }
        throw new TemplateSizeTooSmallException();
    }
    
    public final Point2Integer u(final SheetLabel.LableType lableType, final Point2Integer point2Integer, final String s) {
        this.f.add(new SheetLabel(lableType, point2Integer.x + 1, point2Integer.y, this.k, s));
        return point2Integer.addx(1);
    }
    
    public final Point2Integer v(final Section2 section2, int n, Point2Integer point2Integer, final int n2, final int n3, final boolean b) {
        final int n4 = TemplateBuilder$a.a[section2.type.ordinal()];
        int n5 = 10;
        final int n6 = 4;
        int secondaryOptions = 5;
        switch (n4) {
            default: {
                return point2Integer;
            }
            case 10: {
                final Point2Integer j = this.j(point2Integer, b);
                Label_0142: {
                    if ((n % 5 != 0 || this.e(j) <= 3 || j.x <= 4) && j.x != 0) {
                        point2Integer = j;
                        if (n != 0) {
                            break Label_0142;
                        }
                    }
                    point2Integer = this.u(SheetLabel.LableType.OPTION_ABCDEFGHIJ, j, null);
                }
                point2Integer = point2Integer.addx(1);
                this.e.add(new AnswerOption(AnswerOption.AnswerOptionType.TENOPTION, this.h(), point2Integer.x, point2Integer.y, 0, n2, n3, section2.positiveMark, section2.negetiveMark, section2.partialAllowed, section2.payload, this.k));
                this.y(point2Integer.y, 10, this.k);
                return point2Integer;
            }
            case 9: {
                final Point2Integer i = this.j(point2Integer, b);
                Label_0279: {
                    if ((n % 5 != 0 || this.e(i) <= 3 || i.x <= 4) && i.x != 0) {
                        point2Integer = i;
                        if (n != 0) {
                            break Label_0279;
                        }
                    }
                    point2Integer = this.u(SheetLabel.LableType.OPTION_ABCDEFGH, i, null);
                }
                point2Integer = point2Integer.addx(1);
                this.e.add(new AnswerOption(AnswerOption.AnswerOptionType.EIGHTOPTION, this.h(), point2Integer.x, point2Integer.y, 0, n2, n3, section2.positiveMark, section2.negetiveMark, section2.partialAllowed, section2.payload, this.k));
                this.y(point2Integer.y, 8, this.k);
                return point2Integer;
            }
            case 8: {
                final Point2Integer k = this.j(point2Integer, b);
                Label_0416: {
                    if ((n % 5 != 0 || this.e(k) <= 3 || k.x <= 4) && k.x != 0) {
                        point2Integer = k;
                        if (n != 0) {
                            break Label_0416;
                        }
                    }
                    point2Integer = this.u(SheetLabel.LableType.OPTION_ABCDEF, k, null);
                }
                point2Integer = point2Integer.addx(1);
                this.e.add(new AnswerOption(AnswerOption.AnswerOptionType.SIXOPTION, this.h(), point2Integer.x, point2Integer.y, 0, n2, n3, section2.positiveMark, section2.negetiveMark, section2.partialAllowed, section2.payload, this.k));
                this.y(point2Integer.y, 6, this.k);
                return point2Integer;
            }
            case 7: {
                final DecimalOptionPayload b2 = bc.b(section2.payload);
                final boolean decimalAllowed = b2.decimalAllowed;
                if (decimalAllowed) {
                    n5 = 11;
                }
                if (b2.digits <= 2) {
                    n %= 2;
                    if (n != 0) {
                        this.e.add(new AnswerOption(AnswerOption.AnswerOptionType.DECIMAL, this.h(), point2Integer.x - n5, point2Integer.y, n, n2, n3, section2.positiveMark, section2.negetiveMark, section2.partialAllowed, section2.payload, this.k));
                        this.y(point2Integer.y, 6, this.k);
                        return point2Integer;
                    }
                }
                point2Integer = this.i(point2Integer, decimalAllowed, b).addx(2);
                n = b2.digits;
                if (n > 5) {
                    secondaryOptions = n;
                }
                this.e.add(new AnswerOption(AnswerOption.AnswerOptionType.DECIMAL, this.h(), point2Integer.x, point2Integer.y, 0, n2, n3, section2.positiveMark, section2.negetiveMark, section2.partialAllowed, section2.payload, this.k));
                this.y(point2Integer.y, secondaryOptions, this.k);
                return point2Integer.addx(n5);
            }
            case 6: {
                final NumericalOptionPayload d = bc.d(section2.payload);
                if (d != null && (d.hasNegetive || d.digits > 1)) {
                    point2Integer = this.l(point2Integer, b).addx(1);
                    n = d.digits;
                    if (n > 5) {
                        secondaryOptions = n;
                    }
                    this.e.add(new AnswerOption(AnswerOption.AnswerOptionType.NUMARICAL, this.h(), point2Integer.x, point2Integer.y, 0, n2, n3, section2.positiveMark, section2.negetiveMark, section2.partialAllowed, section2.payload, this.k));
                    this.y(point2Integer.y, secondaryOptions, this.k);
                    return point2Integer.addx(10);
                }
                n %= 5;
                if (n == 0) {
                    point2Integer = this.l(point2Integer, b).addx(1);
                    this.e.add(new AnswerOption(AnswerOption.AnswerOptionType.NUMARICAL, this.h(), point2Integer.x, point2Integer.y, n, n2, n3, section2.positiveMark, section2.negetiveMark, section2.partialAllowed, section2.payload, this.k));
                    this.y(point2Integer.y, 5, this.k);
                    return point2Integer.addx(10);
                }
                this.e.add(new AnswerOption(AnswerOption.AnswerOptionType.NUMARICAL, this.h(), point2Integer.x - 10, point2Integer.y, n, n2, n3, section2.positiveMark, section2.negetiveMark, section2.partialAllowed, section2.payload, this.k));
                this.y(point2Integer.y, 5, this.k);
                return point2Integer;
            }
            case 5: {
                point2Integer = this.k(point2Integer, section2, b);
                final MatrixOptionPayload c = bc.c(section2.payload);
                n = n6;
                if (c != null) {
                    n = c.primaryOptions;
                    secondaryOptions = c.secondaryOptions;
                }
                point2Integer = point2Integer.addx(1);
                this.e.add(new AnswerOption(AnswerOption.AnswerOptionType.MATRIX, this.h(), point2Integer.x, point2Integer.y, 0, n2, n3, section2.positiveMark, section2.negetiveMark, section2.partialAllowed, section2.payload, this.k));
                this.y(point2Integer.y, secondaryOptions, this.k);
                return point2Integer.addx(n);
            }
            case 4: {
                final Point2Integer l = this.j(point2Integer, b);
                Label_1197: {
                    if ((n % 5 != 0 || this.e(l) <= 3 || l.x <= 4) && l.x != 0) {
                        point2Integer = l;
                        if (n != 0) {
                            break Label_1197;
                        }
                    }
                    point2Integer = this.u(SheetLabel.LableType.OPTION_TRUEORFALSE, l, null);
                }
                point2Integer = point2Integer.addx(1);
                this.e.add(new AnswerOption(AnswerOption.AnswerOptionType.TRUEORFALSE, this.h(), point2Integer.x, point2Integer.y, 0, n2, n3, section2.positiveMark, section2.negetiveMark, section2.partialAllowed, section2.payload, this.k));
                this.y(point2Integer.y, 5, this.k);
                return point2Integer;
            }
            case 3: {
                final Point2Integer m = this.j(point2Integer, b);
                Label_1333: {
                    if ((n % 5 != 0 || this.e(m) <= 3 || m.x <= 4) && m.x != 0) {
                        point2Integer = m;
                        if (n != 0) {
                            break Label_1333;
                        }
                    }
                    point2Integer = this.u(SheetLabel.LableType.OPTION_ABC, m, null);
                }
                point2Integer = point2Integer.addx(1);
                this.e.add(new AnswerOption(AnswerOption.AnswerOptionType.THREEOPTION, this.h(), point2Integer.x, point2Integer.y, 0, n2, n3, section2.positiveMark, section2.negetiveMark, section2.partialAllowed, section2.payload, this.k));
                this.y(point2Integer.y, 5, this.k);
                return point2Integer;
            }
            case 2: {
                final Point2Integer j2 = this.j(point2Integer, b);
                Label_1469: {
                    if ((n % 5 != 0 || this.e(j2) <= 3 || j2.x <= 4) && j2.x != 0) {
                        point2Integer = j2;
                        if (n != 0) {
                            break Label_1469;
                        }
                    }
                    point2Integer = this.u(SheetLabel.LableType.OPTION_ABCD, j2, null);
                }
                point2Integer = point2Integer.addx(1);
                this.e.add(new AnswerOption(AnswerOption.AnswerOptionType.FOUROPTION, this.h(), point2Integer.x, point2Integer.y, 0, n2, n3, section2.positiveMark, section2.negetiveMark, section2.partialAllowed, section2.payload, this.k));
                this.y(point2Integer.y, 5, this.k);
                return point2Integer;
            }
            case 1: {
                final Point2Integer j3 = this.j(point2Integer, b);
                Label_1605: {
                    if ((n % 5 != 0 || this.e(j3) <= 3 || j3.x <= 4) && j3.x != 0) {
                        point2Integer = j3;
                        if (n != 0) {
                            break Label_1605;
                        }
                    }
                    point2Integer = this.u(SheetLabel.LableType.OPTION_ABCDE, j3, null);
                }
                point2Integer = point2Integer.addx(1);
                this.e.add(new AnswerOption(AnswerOption.AnswerOptionType.FIVEOPTION, this.h(), point2Integer.x, point2Integer.y, 0, n2, n3, section2.positiveMark, section2.negetiveMark, section2.partialAllowed, section2.payload, this.k));
                this.y(point2Integer.y, 5, this.k);
                return point2Integer;
            }
        }
    }
    
    public final Point2Integer w(final Section2 section2, Point2Integer point2Integer, final int n, final int n2, final boolean b) {
        final int f = this.f(section2);
        Point2Integer r = point2Integer;
        if (this.e(point2Integer) < f) {
            r = this.r(point2Integer, b);
        }
        point2Integer = this.u(SheetLabel.LableType.TEXT, r, section2.name);
        for (int i = 0; i < section2.noOfQue; ++i) {
            point2Integer = this.v(section2, i, point2Integer, n, n2, b);
        }
        return point2Integer;
    }
    
    public final Point2Integer x(final Subject2 subject2, Point2Integer point2Integer, final int n, final boolean b) {
        Point2Integer r = point2Integer;
        if (this.h == BuildType.SUBJECT_IN_COLUMN) {
            r = point2Integer;
            if (point2Integer.x != 0) {
                r = this.r(point2Integer, b);
            }
        }
        final int g = this.g(subject2);
        point2Integer = r;
        if (this.e(r) < g) {
            point2Integer = this.r(r, b);
        }
        point2Integer = this.u(SheetLabel.LableType.TEXT, point2Integer, subject2.getSubName());
        for (int i = 0; i < subject2.getSections().length; ++i) {
            point2Integer = this.w(subject2.getSections()[i], point2Integer, n, i, b);
        }
        return point2Integer;
    }
    
    public final void y(int n, int n2, int index) {
        final int[] columnWidthInBubbles = this.j.get(index).getColumnWidthInBubbles();
        index = n - 1;
        n = columnWidthInBubbles[index];
        n2 += 3;
        if (n < n2) {
            columnWidthInBubbles[index] = n2;
        }
    }
    
    public enum BuildType
    {
        private static final BuildType[] $VALUES;
        
        DEFAULT, 
        SUBJECT_IN_COLUMN;
        
        private static /* synthetic */ BuildType[] $values() {
            return new BuildType[] { BuildType.DEFAULT, BuildType.SUBJECT_IN_COLUMN };
        }
        
        static {
            $VALUES = $values();
        }
    }
}

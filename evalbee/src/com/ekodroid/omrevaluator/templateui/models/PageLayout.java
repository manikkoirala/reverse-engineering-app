// 
// Decompiled by Procyon v0.6.0
// 

package com.ekodroid.omrevaluator.templateui.models;

import java.io.Serializable;

public class PageLayout implements Serializable
{
    private int[] columnWidthInBubbles;
    private int columns;
    private int rows;
    
    public PageLayout(final int rows, final int columns, final int[] columnWidthInBubbles) {
        this.columns = columns;
        this.rows = rows;
        this.columnWidthInBubbles = columnWidthInBubbles;
    }
    
    public int[] getColumnWidthInBubbles() {
        return this.columnWidthInBubbles;
    }
    
    public int getColumns() {
        return this.columns;
    }
    
    public int getRows() {
        return this.rows;
    }
}

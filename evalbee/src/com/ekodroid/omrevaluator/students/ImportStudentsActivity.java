// 
// Decompiled by Procyon v0.6.0
// 

package com.ekodroid.omrevaluator.students;

import android.os.Bundle;
import android.net.Uri;
import java.io.FileNotFoundException;
import java.io.InputStream;
import com.ekodroid.omrevaluator.students.services.SyncStudentService;
import androidx.appcompat.widget.Toolbar;
import android.content.Context;
import android.view.View;
import android.view.View$OnClickListener;
import android.content.Intent;
import java.util.ArrayList;
import android.widget.TextView;

public class ImportStudentsActivity extends v5
{
    public ImportStudentsActivity c;
    public TextView d;
    public TextView e;
    public ArrayList f;
    public String g;
    public String h;
    
    public ImportStudentsActivity() {
        this.c = this;
        this.f = new ArrayList();
    }
    
    public static /* synthetic */ String A(final ImportStudentsActivity importStudentsActivity, final String g) {
        return importStudentsActivity.g = g;
    }
    
    public static /* synthetic */ String B(final ImportStudentsActivity importStudentsActivity, final String h) {
        return importStudentsActivity.h = h;
    }
    
    public static /* synthetic */ ArrayList C(final ImportStudentsActivity importStudentsActivity) {
        return importStudentsActivity.f;
    }
    
    public static /* synthetic */ ArrayList D(final ImportStudentsActivity importStudentsActivity, final ArrayList f) {
        return importStudentsActivity.f = f;
    }
    
    public final void H() {
        final Intent intent = new Intent("android.intent.action.GET_CONTENT");
        intent.setType("*/*");
        intent.addCategory("android.intent.category.OPENABLE");
        intent.putExtra("android.intent.extra.LOCAL_ONLY", true);
        this.startActivityForResult(Intent.createChooser(intent, (CharSequence)"Choose a file"), 10);
    }
    
    public final void I() {
        final ArrayList f = this.f;
        if (f != null && f.size() > 0) {
            this.findViewById(2131296454).setEnabled(true);
        }
        else {
            this.findViewById(2131296454).setEnabled(false);
        }
        final String g = this.g;
        if (g != null && g.length() > 0) {
            this.findViewById(2131296766).setVisibility(0);
            this.d.setText((CharSequence)this.g);
        }
        else {
            this.findViewById(2131296766).setVisibility(8);
        }
        final String h = this.h;
        if (h != null && h.length() > 0) {
            this.findViewById(2131296763).setVisibility(0);
            this.e.setText((CharSequence)this.h);
        }
        else {
            this.findViewById(2131296763).setVisibility(8);
        }
    }
    
    public final void J() {
        this.findViewById(2131296432).setOnClickListener((View$OnClickListener)new View$OnClickListener(this) {
            public final ImportStudentsActivity a;
            
            public void onClick(final View view) {
                this.a.c.finish();
            }
        });
    }
    
    public final void K() {
        this.findViewById(2131296450).setOnClickListener((View$OnClickListener)new View$OnClickListener(this) {
            public final ImportStudentsActivity a;
            
            public void onClick(final View view) {
                new go((Context)this.a.c, new ArrayList(), new y01(this) {
                    public final ImportStudentsActivity$b a;
                    
                    @Override
                    public void a(final Object o) {
                    }
                });
            }
        });
    }
    
    public final void L() {
        this.findViewById(2131296454).setOnClickListener((View$OnClickListener)new View$OnClickListener(this) {
            public final ImportStudentsActivity a;
            
            public void onClick(final View view) {
                final ImportStudentsActivity a = this.a;
                new dd((Context)a.c, ImportStudentsActivity.C(a), new y01(this) {
                    public final ImportStudentsActivity$d a;
                    
                    @Override
                    public void a(final Object o) {
                        this.a.a.O(false);
                        this.a.a.c.finish();
                    }
                });
            }
        });
    }
    
    public final void M() {
        this.findViewById(2131296799).setOnClickListener((View$OnClickListener)new View$OnClickListener(this) {
            public final ImportStudentsActivity a;
            
            public void onClick(final View view) {
                this.a.H();
            }
        });
    }
    
    public final void N() {
        final Toolbar toolbar = (Toolbar)this.findViewById(2131297348);
        this.x(toolbar);
        toolbar.setNavigationOnClickListener((View$OnClickListener)new View$OnClickListener(this) {
            public final ImportStudentsActivity a;
            
            public void onClick(final View view) {
                this.a.finish();
            }
        });
    }
    
    public final void O(final boolean b) {
        if (a91.c((Context)this.c)) {
            SyncStudentService.h((Context)this.c, b);
        }
    }
    
    public final void P(final InputStream inputStream) {
        this.g = null;
        this.h = null;
        new ho((Context)this.c, inputStream, new y01(this) {
            public final ImportStudentsActivity a;
            
            @Override
            public void a(final Object o) {
                if (o != null) {
                    final ho ho = (ho)o;
                    ImportStudentsActivity.A(this.a, ho.j);
                    ImportStudentsActivity.B(this.a, ho.i);
                    final ArrayList l = ho.l;
                    if (l != null && l.size() > 0) {
                        ImportStudentsActivity.D(this.a, ho.l);
                    }
                    this.a.I();
                }
            }
        });
    }
    
    @Override
    public void onActivityResult(final int n, final int n2, final Intent intent) {
        super.onActivityResult(n, n2, intent);
        if (n == 10 && n2 == -1) {
            final Uri data = intent.getData();
            try {
                this.P(((Context)this).getContentResolver().openInputStream(data));
            }
            catch (final FileNotFoundException ex) {
                ex.printStackTrace();
            }
        }
    }
    
    @Override
    public void onCreate(final Bundle bundle) {
        super.onCreate(bundle);
        this.setContentView(2131492911);
        this.N();
        this.e = (TextView)this.findViewById(2131297235);
        this.d = (TextView)this.findViewById(2131297236);
        this.J();
        this.L();
        this.M();
        this.K();
    }
    
    @Override
    public void onResume() {
        super.onResume();
        this.I();
    }
}

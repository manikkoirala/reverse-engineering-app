// 
// Decompiled by Procyon v0.6.0
// 

package com.ekodroid.omrevaluator.students.clients;

import com.google.android.gms.tasks.Task;
import com.google.android.gms.tasks.OnCompleteListener;
import com.android.volley.Request;
import java.util.HashMap;
import com.google.firebase.auth.FirebaseAuth;
import java.util.Map;
import com.android.volley.VolleyError;
import java.util.Iterator;
import com.ekodroid.omrevaluator.database.ClassDataModel;
import com.ekodroid.omrevaluator.database.repositories.ClassRepository;
import com.ekodroid.omrevaluator.students.clients.models.ClassDto;
import com.google.gson.reflect.TypeToken;
import java.util.ArrayList;
import com.android.volley.d;
import android.content.Context;

public class GetAllClassList
{
    public ee1 a;
    public zg b;
    public String c;
    public Context d;
    
    public GetAllClassList(final Context d, final zg b) {
        this.d = d;
        this.b = b;
        this.a = new n52(d, a91.x()).b();
        final StringBuilder sb = new StringBuilder();
        sb.append("https://");
        sb.append(a91.x());
        sb.append(":");
        sb.append(a91.q());
        sb.append("/api-online/class-list");
        this.c = sb.toString();
        this.f();
    }
    
    public static /* synthetic */ Context c(final GetAllClassList list) {
        return list.d;
    }
    
    public final void d(final String s) {
        final hr1 hr1 = new hr1(this, 0, this.c, new d.b(this) {
            public final GetAllClassList a;
            
            public void b(final String s) {
                try {
                    final ArrayList list = (ArrayList)new gc0().k(s, new TypeToken<ArrayList<ClassDto>>(this) {
                        public final GetAllClassList$2 a;
                    }.getType());
                    final ClassRepository instance = ClassRepository.getInstance(GetAllClassList.c(this.a));
                    instance.deleteAllSyncedClass();
                    final Iterator iterator = list.iterator();
                    while (iterator.hasNext()) {
                        instance.saveOrUpdateClass(new ClassDataModel(((ClassDto)iterator.next()).className, null, true));
                    }
                    this.a.e(true, 200, list);
                }
                catch (final Exception ex) {
                    ex.printStackTrace();
                    this.a.e(false, 400, null);
                }
            }
        }, new d.a(this) {
            public final GetAllClassList a;
            
            @Override
            public void a(final VolleyError volleyError) {
                volleyError.printStackTrace();
                final yy0 networkResponse = volleyError.networkResponse;
                int a;
                if (networkResponse != null) {
                    a = networkResponse.a;
                }
                else {
                    a = 400;
                }
                this.a.e(false, a, volleyError.getMessage());
            }
        }, s) {
            public final String w;
            public final GetAllClassList x;
            
            @Override
            public Map o() {
                final String o = FirebaseAuth.getInstance().e().O();
                final String email = FirebaseAuth.getInstance().e().getEmail();
                final HashMap hashMap = new HashMap();
                hashMap.put("x-user-id", o);
                hashMap.put("x-user-email", email);
                hashMap.put("x-user-token", this.w);
                return hashMap;
            }
        };
        hr1.L(new wq(20000, 0, 1.0f));
        this.a.d().clear();
        this.a.a(hr1);
    }
    
    public final void e(final boolean b, final int n, final Object o) {
        final zg b2 = this.b;
        if (b2 != null) {
            b2.a(b, n, o);
        }
    }
    
    public final void f() {
        FirebaseAuth.getInstance().e().i(false).addOnCompleteListener((OnCompleteListener)new OnCompleteListener(this) {
            public final GetAllClassList a;
            
            public void onComplete(final Task task) {
                if (task.isSuccessful()) {
                    this.a.d(((ya0)task.getResult()).c());
                }
                else {
                    this.a.e(false, 400, null);
                }
            }
        });
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.ekodroid.omrevaluator.students.clients.models;

import java.io.Serializable;

public class ClassDto implements Serializable
{
    public String className;
    
    public ClassDto() {
    }
    
    public ClassDto(final String className) {
        this.className = className;
    }
    
    public String getClassName() {
        return this.className;
    }
    
    public void setClassName(final String className) {
        this.className = className;
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.ekodroid.omrevaluator.students.services;

import com.ekodroid.omrevaluator.database.StudentDataModel;
import java.util.ArrayList;
import com.ekodroid.omrevaluator.clients.students.model.BulkStudentRequest;
import com.ekodroid.omrevaluator.clients.students.PostPendingStudents;
import com.ekodroid.omrevaluator.clients.students.model.StudentDto;
import com.ekodroid.omrevaluator.clients.students.GetUpdatedStudents;
import com.ekodroid.omrevaluator.database.repositories.ClassRepository;
import android.content.Intent;
import android.content.Context;
import android.app.IntentService;

public class SyncStudentService extends IntentService
{
    public Context a;
    
    public SyncStudentService() {
        super("SyncStudentService");
        this.a = (Context)this;
    }
    
    public static /* synthetic */ Context c(final SyncStudentService syncStudentService) {
        return syncStudentService.a;
    }
    
    public static void h(final Context context, final boolean b) {
        final Intent intent = new Intent(context, (Class)SyncStudentService.class);
        intent.setAction("ACTION_SYNC_STUDENT");
        intent.putExtra("force_sync", b);
        context.startService(intent);
    }
    
    public final void e(final boolean b) {
        new wa0(this.a, new zg(this, b) {
            public final boolean a;
            public final SyncStudentService b;
            
            @Override
            public void a(final boolean b, final int n, Object o) {
                if (b && o instanceof Long) {
                    Label_0083: {
                        if (ClassRepository.getInstance(SyncStudentService.c(this.b)).getAllStudents().size() != (long)o) {
                            final boolean a = this.a;
                            if (!a) {
                                if (!a) {
                                    this.b.g(true);
                                }
                                break Label_0083;
                            }
                        }
                        a91.L(SyncStudentService.c(this.b), System.currentTimeMillis());
                    }
                    o = new Intent("UPDATE_STUDENT_LIST");
                    ((Intent)o).putExtra("data_changed", true);
                    SyncStudentService.c(this.b).sendBroadcast((Intent)o);
                }
            }
        });
    }
    
    public final void f(final boolean b) {
        if (b) {
            a91.L(this.a, 0L);
            ClassRepository.getInstance(this.a).deleteAllStudent();
        }
        new GetUpdatedStudents((Context)this, a91.z((Context)this), 0, 200, new zg(this, b) {
            public final boolean a;
            public final SyncStudentService b;
            
            @Override
            public void a(final boolean b, final int n, final Object o) {
                if (b) {
                    this.b.e(this.a);
                }
            }
        });
    }
    
    public final void g(final boolean b) {
        final ArrayList<StudentDataModel> allNonSyncStudents = ClassRepository.getInstance(this.a).getAllNonSyncStudents();
        final ArrayList<StudentDto> studentDto = StudentDto.getStudentDto(allNonSyncStudents);
        if (allNonSyncStudents.size() > 0) {
            new PostPendingStudents(new BulkStudentRequest(studentDto), this.a, new zg(this, b) {
                public final boolean a;
                public final SyncStudentService b;
                
                @Override
                public void a(final boolean b, final int n, final Object o) {
                    if (b) {
                        this.b.f(this.a);
                    }
                }
            });
        }
        else {
            this.f(b);
        }
    }
    
    public void onHandleIntent(final Intent intent) {
        final boolean c = a91.c((Context)this);
        if (intent != null && c && "ACTION_SYNC_STUDENT".equals(intent.getAction())) {
            this.g(intent.getBooleanExtra("force_sync", false));
        }
    }
}

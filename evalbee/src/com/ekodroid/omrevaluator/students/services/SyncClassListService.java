// 
// Decompiled by Procyon v0.6.0
// 

package com.ekodroid.omrevaluator.students.services;

import com.ekodroid.omrevaluator.students.clients.GetAllClassList;
import android.content.Intent;
import android.content.Context;
import android.app.IntentService;

public class SyncClassListService extends IntentService
{
    public Context a;
    
    public SyncClassListService() {
        super("SyncClassListService");
        this.a = (Context)this;
    }
    
    public static /* synthetic */ Context a(final SyncClassListService syncClassListService) {
        return syncClassListService.a;
    }
    
    public static void c(final Context context, final boolean b) {
        final Intent intent = new Intent(context, (Class)SyncClassListService.class);
        intent.setAction("ACTION_SYNC_CLASS_LIST");
        context.startService(intent);
    }
    
    public final void b() {
        new ys1(this.a, new zg(this) {
            public final SyncClassListService a;
            
            @Override
            public void a(final boolean b, final int n, final Object o) {
                new GetAllClassList(SyncClassListService.a(this.a), new zg(this) {
                    public final SyncClassListService$a a;
                    
                    @Override
                    public void a(final boolean b, final int n, final Object o) {
                        final Intent intent = new Intent("UPDATE_CLASS_LIST");
                        intent.putExtra("data_changed", true);
                        SyncClassListService.a(this.a.a).sendBroadcast(intent);
                    }
                });
            }
        });
    }
    
    public void onHandleIntent(final Intent intent) {
        final boolean c = a91.c((Context)this);
        if (intent != null && c && "ACTION_SYNC_CLASS_LIST".equals(intent.getAction())) {
            this.b();
        }
    }
}

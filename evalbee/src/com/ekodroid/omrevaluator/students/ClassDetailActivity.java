// 
// Decompiled by Procyon v0.6.0
// 

package com.ekodroid.omrevaluator.students;

import android.os.BaseBundle;
import android.widget.AbsListView;
import android.app.Dialog;
import android.os.AsyncTask;
import android.content.IntentFilter;
import android.os.Bundle;
import com.ekodroid.omrevaluator.students.services.SyncStudentService;
import com.ekodroid.omrevaluator.more.ProductAndServicesActivity;
import android.widget.CompoundButton;
import android.widget.CompoundButton$OnCheckedChangeListener;
import android.widget.AdapterView;
import android.widget.AdapterView$OnItemClickListener;
import android.view.Menu;
import android.util.SparseBooleanArray;
import android.view.MenuItem;
import android.widget.AbsListView$MultiChoiceModeListener;
import android.widget.ListAdapter;
import android.content.res.Resources;
import com.ekodroid.omrevaluator.util.DataModels.SortStudentList;
import android.view.View$OnClickListener;
import java.io.Serializable;
import android.content.Intent;
import com.ekodroid.omrevaluator.clients.students.model.BulkStudentRequest;
import com.ekodroid.omrevaluator.clients.students.model.StudentDto;
import android.app.ProgressDialog;
import java.util.Iterator;
import android.content.Context;
import com.ekodroid.omrevaluator.database.repositories.ClassRepository;
import android.view.View;
import com.ekodroid.omrevaluator.database.StudentDataModel;
import androidx.appcompat.widget.Toolbar;
import android.content.BroadcastReceiver;
import androidx.appcompat.widget.SwitchCompat;
import android.view.ActionMode;
import java.util.ArrayList;
import android.widget.ListView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import android.widget.TextView;

public class ClassDetailActivity extends v5
{
    public ClassDetailActivity c;
    public TextView d;
    public SwipeRefreshLayout e;
    public ListView f;
    public ArrayList g;
    public q3 h;
    public String i;
    public ActionMode j;
    public SwitchCompat k;
    public BroadcastReceiver l;
    public boolean m;
    public Toolbar n;
    public vr1 p;
    
    public ClassDetailActivity() {
        this.c = this;
        this.g = new ArrayList();
        this.m = false;
    }
    
    public static /* synthetic */ ActionMode C(final ClassDetailActivity classDetailActivity, final ActionMode j) {
        return classDetailActivity.j = j;
    }
    
    public static /* synthetic */ String E(final ClassDetailActivity classDetailActivity) {
        return classDetailActivity.i;
    }
    
    public final void N(final ArrayList list) {
        xs.c((Context)this.c, new y01(this, list) {
            public final ArrayList a;
            public final ClassDetailActivity b;
            
            @Override
            public void a(final Object o) {
                final ClassDetailActivity b = this.b;
                if (b.m) {
                    b.O(this.a);
                }
                else {
                    for (final StudentDataModel studentDataModel : this.a) {
                        ClassRepository.getInstance((Context)this.b.c).deleteStudent(studentDataModel.getRollNO(), studentDataModel.getClassName());
                    }
                }
            }
        }, 2131886241, 2131886239, 2131886231, 2131886163, 0, 2131230945);
    }
    
    public final void O(final ArrayList list) {
        final ProgressDialog progressDialog = new ProgressDialog((Context)this.c);
        progressDialog.setMessage((CharSequence)"Please wait, deleting students...");
        ((Dialog)progressDialog).show();
        new ks(new BulkStudentRequest(StudentDto.getStudentDto(list)), (Context)this.c, new zg(this, progressDialog, list) {
            public final ProgressDialog a;
            public final ArrayList b;
            public final ClassDetailActivity c;
            
            @Override
            public void a(final boolean b, final int n, final Object o) {
                ((Dialog)this.a).dismiss();
                if (b) {
                    for (final StudentDataModel studentDataModel : this.b) {
                        ClassRepository.getInstance((Context)this.c.c).deleteStudent(studentDataModel.getRollNO(), studentDataModel.getClassName());
                    }
                    this.c.U();
                }
                else {
                    a91.H((Context)this.c.c, "Delete failed, please try again", 2131230909, 2131231086);
                }
            }
        });
    }
    
    public final void P(final StudentDataModel studentDataModel) {
        final Intent intent = new Intent((Context)this.c, (Class)AddStudentActivity.class);
        intent.putExtra("CLASS_NAME", studentDataModel.getClassName());
        intent.putExtra("STUDENT_MODEL", (Serializable)studentDataModel);
        ((Context)this).startActivity(intent);
    }
    
    public final void Q() {
        new go((Context)this.c, this.g, new y01(this) {
            public final ClassDetailActivity a;
            
            @Override
            public void a(final Object o) {
            }
        });
    }
    
    public final void R() {
        final Intent intent = new Intent((Context)this.c, (Class)ImportStudentsActivity.class);
        intent.putExtra("CLASS_NAME", this.i);
        ((Context)this).startActivity(intent);
    }
    
    public final void S() {
        this.findViewById(2131296667).setOnClickListener((View$OnClickListener)new View$OnClickListener(this) {
            public final ClassDetailActivity a;
            
            public void onClick(final View view) {
                xs.a((Context)this.a.c, 2131886355, 2131886510, 2130903042, (xs.i)new xs.i(this) {
                    public final ClassDetailActivity$n a;
                    
                    @Override
                    public void a(final int n, final String s) {
                        if (n != 0) {
                            if (n == 1) {
                                this.a.a.Q();
                            }
                        }
                        else {
                            this.a.a.R();
                        }
                    }
                });
            }
        });
    }
    
    public final void T() {
        this.l = new BroadcastReceiver(this) {
            public final ClassDetailActivity a;
            
            public void onReceive(final Context context, final Intent intent) {
                this.a.U();
            }
        };
    }
    
    public final void U() {
        if (this.m && System.currentTimeMillis() > a91.z((Context)this.c) + 180000L) {
            this.g0(false);
        }
        new q(null).execute((Object[])new Void[0]);
    }
    
    public final void V() {
        SortStudentList.SortBy rollno = SortStudentList.SortBy.ROLLNO;
        final vr1 p = this.p;
        boolean b;
        if (p != null) {
            b = p.b;
            final SortStudentList.SortBy a = p.a;
            this.findViewById(2131296734).setVisibility(0);
            final TextView textView = (TextView)this.findViewById(2131297312);
            final StringBuilder sb = new StringBuilder();
            Resources resources;
            int n;
            if (this.p.a == SortStudentList.SortBy.NAME) {
                resources = this.getResources();
                n = 2131886649;
            }
            else {
                resources = this.getResources();
                n = 2131886747;
            }
            sb.append(resources.getString(n));
            sb.append(" - ");
            Resources resources2;
            int n2;
            if (this.p.b) {
                resources2 = this.getResources();
                n2 = 2131886146;
            }
            else {
                resources2 = this.getResources();
                n2 = 2131886243;
            }
            sb.append(resources2.getString(n2));
            textView.setText((CharSequence)sb.toString());
            rollno = a;
        }
        else {
            this.findViewById(2131296734).setVisibility(8);
            b = true;
        }
        final ArrayList g = this.g;
        if (g != null && g.size() > 0) {
            new SortStudentList().c(rollno, this.g, b);
        }
    }
    
    public final void W() {
        this.n.setTitle(this.i);
        final TextView d = this.d;
        final StringBuilder sb = new StringBuilder();
        sb.append(((Context)this).getString(2131886853));
        sb.append(" (");
        sb.append(ClassRepository.getInstance((Context)this.c).getNumberOfStudentForAClass(this.i));
        sb.append(")");
        d.setText((CharSequence)sb.toString());
        this.V();
        final ArrayList g = this.g;
        if (g != null && g.size() > 0) {
            this.findViewById(2131296748).setVisibility(4);
            this.findViewById(2131297137).setVisibility(0);
            final q3 q3 = new q3(this.g, (Context)this.c);
            this.h = q3;
            this.f.setAdapter((ListAdapter)q3);
        }
        else {
            this.findViewById(2131296748).setVisibility(0);
            this.findViewById(2131297137).setVisibility(4);
        }
    }
    
    public final void X() {
        ((AbsListView)this.f).setChoiceMode(3);
        ((AbsListView)this.f).setMultiChoiceModeListener((AbsListView$MultiChoiceModeListener)new AbsListView$MultiChoiceModeListener(this) {
            public final ClassDetailActivity a;
            
            public boolean onActionItemClicked(final ActionMode actionMode, final MenuItem menuItem) {
                if (menuItem.getItemId() == 2131296708) {
                    final ArrayList list = new ArrayList();
                    final SparseBooleanArray a = this.a.h.a();
                    for (int i = a.size() - 1; i >= 0; --i) {
                        if (a.valueAt(i)) {
                            list.add(this.a.g.get(a.keyAt(i)));
                        }
                    }
                    this.a.N(list);
                    actionMode.finish();
                    return true;
                }
                return false;
            }
            
            public boolean onCreateActionMode(final ActionMode actionMode, final Menu menu) {
                this.a.c.getMenuInflater().inflate(2131623939, menu);
                ClassDetailActivity.C(this.a, actionMode);
                return true;
            }
            
            public void onDestroyActionMode(final ActionMode actionMode) {
                this.a.h.b();
                actionMode.finish();
                ClassDetailActivity.C(this.a, null);
            }
            
            public void onItemCheckedStateChanged(final ActionMode actionMode, final int n, final long n2, final boolean b) {
                final int checkedItemCount = ((AbsListView)this.a.f).getCheckedItemCount();
                final StringBuilder sb = new StringBuilder();
                sb.append(checkedItemCount);
                sb.append(" Selected");
                actionMode.setTitle((CharSequence)sb.toString());
                this.a.h.d(n);
                this.a.h.notifyDataSetChanged();
            }
            
            public boolean onPrepareActionMode(final ActionMode actionMode, final Menu menu) {
                return false;
            }
        });
    }
    
    public final void Y() {
        this.findViewById(2131296436).setOnClickListener((View$OnClickListener)new View$OnClickListener(this) {
            public final ClassDetailActivity a;
            
            public void onClick(final View view) {
                final ClassDetailActivity a = this.a;
                a.p = null;
                a.W();
            }
        });
        this.findViewById(2131296666).setOnClickListener((View$OnClickListener)new View$OnClickListener(this) {
            public final ClassDetailActivity a;
            
            public void onClick(final View view) {
                final ClassDetailActivity a = this.a;
                new m10((Context)a.c, a.p, new j10(this) {
                    public final ClassDetailActivity$j a;
                    
                    @Override
                    public void a(final Object o) {
                        if (o instanceof vr1) {
                            final ClassDetailActivity a = this.a.a;
                            a.p = (vr1)o;
                            a.W();
                        }
                    }
                }).show();
            }
        });
    }
    
    public final void Z() {
        ((AdapterView)(this.f = (ListView)this.findViewById(2131296846))).setOnItemClickListener((AdapterView$OnItemClickListener)new AdapterView$OnItemClickListener(this) {
            public final ClassDetailActivity a;
            
            public void onItemClick(final AdapterView adapterView, final View view, final int index, final long n) {
                final StudentDataModel studentDataModel = this.a.g.get(index);
                if (view.getId() == 2131296668L) {
                    this.a.f0(studentDataModel, view);
                }
                else {
                    view.setBackgroundResource(2131230860);
                    this.a.P(studentDataModel);
                }
            }
        });
    }
    
    public final void a0() {
        this.k.setOnCheckedChangeListener((CompoundButton$OnCheckedChangeListener)new CompoundButton$OnCheckedChangeListener(this) {
            public final ClassDetailActivity a;
            
            public void onCheckedChanged(final CompoundButton compoundButton, final boolean b) {
                if (b) {
                    this.a.e0();
                }
            }
        });
    }
    
    public final void b0() {
        this.findViewById(2131296414).setOnClickListener((View$OnClickListener)new View$OnClickListener(this) {
            public final ClassDetailActivity a;
            
            public void onClick(final View view) {
                this.a.d0();
            }
        });
        this.findViewById(2131296413).setOnClickListener((View$OnClickListener)new View$OnClickListener(this) {
            public final ClassDetailActivity a;
            
            public void onClick(final View view) {
                this.a.d0();
            }
        });
    }
    
    public final void c0() {
        this.x(this.n = (Toolbar)this.findViewById(2131297339));
        this.n.setNavigationOnClickListener((View$OnClickListener)new View$OnClickListener(this) {
            public final ClassDetailActivity a;
            
            public void onClick(final View view) {
                this.a.finish();
            }
        });
    }
    
    public final void d0() {
        final Intent intent = new Intent((Context)this.c, (Class)AddStudentActivity.class);
        intent.putExtra("CLASS_NAME", this.i);
        ((Context)this).startActivity(intent);
    }
    
    public final void e0() {
        this.k.setChecked(false);
        xs.c((Context)this.c, new y01(this) {
            public final ClassDetailActivity a;
            
            @Override
            public void a(final Object o) {
                ((Context)this.a).startActivity(new Intent((Context)this.a.c, (Class)ProductAndServicesActivity.class));
            }
        }, 2131886177, 2131886462, 2131886160, 2131886163, 0, 0);
    }
    
    public final void f0(final StudentDataModel studentDataModel, final View view) {
        final b61 b61 = new b61((Context)this.c, view);
        b61.b(2131623945);
        b61.c(true);
        b61.d((b61.c)new b61.c(this, studentDataModel) {
            public final StudentDataModel a;
            public final ClassDetailActivity b;
            
            @Override
            public boolean onMenuItemClick(final MenuItem menuItem) {
                switch (menuItem.getItemId()) {
                    case 2131296709: {
                        this.b.P(this.a);
                        break;
                    }
                    case 2131296708: {
                        final ArrayList list = new ArrayList();
                        list.add(this.a);
                        this.b.N(list);
                        break;
                    }
                }
                return true;
            }
        });
        b61.e();
    }
    
    public final void g0(final boolean b) {
        if (this.m) {
            SyncStudentService.h((Context)this.c, b);
        }
    }
    
    @Override
    public void onCreate(Bundle extras) {
        super.onCreate(extras);
        this.setContentView(2131492899);
        this.c0();
        extras = this.getIntent().getExtras();
        this.k = (SwitchCompat)this.findViewById(2131297141);
        this.d = (TextView)this.findViewById(2131297307);
        if (extras != null) {
            this.i = ((BaseBundle)extras).getString("CLASS_NAME");
        }
        this.Z();
        this.Y();
        this.X();
        this.T();
        this.S();
        this.a0();
        this.b0();
        (this.e = (SwipeRefreshLayout)this.findViewById(2131297137)).setColorSchemeColors(this.getResources().getIntArray(2130903043));
        this.e.setOnRefreshListener((SwipeRefreshLayout.j)new SwipeRefreshLayout.j(this) {
            public final ClassDetailActivity a;
            
            @Override
            public void a() {
                this.a.new q(null).execute((Object[])new Void[0]);
                final ClassDetailActivity a = this.a;
                if (a.m) {
                    a.g0(true);
                }
            }
        });
    }
    
    @Override
    public void onPause() {
        super.onPause();
        ((Context)this).unregisterReceiver(this.l);
    }
    
    @Override
    public void onResume() {
        super.onResume();
        sl.registerReceiver((Context)this.c, this.l, new IntentFilter("UPDATE_STUDENT_LIST"), 4);
        final boolean c = a91.c((Context)this.c);
        this.m = c;
        if (c) {
            this.findViewById(2131296807).setVisibility(8);
        }
        this.U();
    }
    
    public class q extends AsyncTask
    {
        public final ClassDetailActivity a;
        
        public q(final ClassDetailActivity a) {
            this.a = a;
        }
        
        public Void a(final Void... array) {
            final ClassDetailActivity a = this.a;
            a.g = ClassRepository.getInstance((Context)a.c).getStudents(ClassDetailActivity.E(this.a));
            return null;
        }
        
        public void b(final Void void1) {
            super.onPostExecute((Object)void1);
            this.a.e.setRefreshing(false);
            this.a.W();
        }
        
        public void onPreExecute() {
            this.a.e.setRefreshing(true);
        }
    }
}

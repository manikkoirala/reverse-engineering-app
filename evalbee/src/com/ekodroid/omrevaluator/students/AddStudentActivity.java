// 
// Decompiled by Procyon v0.6.0
// 

package com.ekodroid.omrevaluator.students;

import android.os.BaseBundle;
import android.widget.TextView;
import android.os.Bundle;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.ekodroid.omrevaluator.database.repositories.ClassRepository;
import android.content.Context;
import android.view.View;
import android.view.View$OnClickListener;
import android.text.Editable;
import android.text.TextWatcher;
import androidx.appcompat.widget.Toolbar;
import com.ekodroid.omrevaluator.database.StudentDataModel;
import android.widget.Button;
import com.google.android.material.textfield.TextInputLayout;

public class AddStudentActivity extends v5
{
    public AddStudentActivity c;
    public TextInputLayout d;
    public TextInputLayout e;
    public TextInputLayout f;
    public TextInputLayout g;
    public TextInputLayout h;
    public Button i;
    public String j;
    public String k;
    public String l;
    public String m;
    public String n;
    public StudentDataModel p;
    public Toolbar q;
    public TextWatcher t;
    
    public AddStudentActivity() {
        this.c = this;
        this.t = (TextWatcher)new TextWatcher(this) {
            public final AddStudentActivity a;
            
            public void afterTextChanged(final Editable editable) {
                final AddStudentActivity a = this.a;
                a.j = a.d.getEditText().getText().toString();
                final AddStudentActivity a2 = this.a;
                a2.k = a2.e.getEditText().getText().toString();
                Button button;
                boolean enabled;
                if (!this.a.j.trim().equals("") && !this.a.k.equals("")) {
                    button = this.a.i;
                    enabled = true;
                }
                else {
                    button = this.a.i;
                    enabled = false;
                }
                ((View)button).setEnabled(enabled);
            }
            
            public void beforeTextChanged(final CharSequence charSequence, final int n, final int n2, final int n3) {
            }
            
            public void onTextChanged(final CharSequence charSequence, final int n, final int n2, final int n3) {
            }
        };
    }
    
    public static /* synthetic */ String A(final AddStudentActivity addStudentActivity) {
        return addStudentActivity.n;
    }
    
    public final void B() {
        ((TextView)this.f.getEditText()).setText((CharSequence)this.n);
        if (this.p != null) {
            this.q.setTitle(2131886254);
            ((TextView)this.i).setText(2131886884);
            ((TextView)this.e.getEditText()).setText((CharSequence)this.p.getStudentName());
            ((TextView)this.d.getEditText()).setText((CharSequence)String.valueOf(this.p.getRollNO()));
            ((TextView)this.h.getEditText()).setText((CharSequence)this.p.getEmailId());
            ((TextView)this.g.getEditText()).setText((CharSequence)this.p.getPhoneNo());
        }
    }
    
    public final void C() {
        ((View)this.i).setOnClickListener((View$OnClickListener)new View$OnClickListener(this) {
            public final AddStudentActivity a;
            
            public void onClick(final View view) {
                final AddStudentActivity a = this.a;
                a.j = a.d.getEditText().getText().toString().trim();
                final AddStudentActivity a2 = this.a;
                a2.m = a2.h.getEditText().getText().toString().trim();
                final AddStudentActivity a3 = this.a;
                a3.k = a3.e.getEditText().getText().toString().trim();
                final AddStudentActivity a4 = this.a;
                a4.l = a4.g.getEditText().getText().toString().trim();
                if (this.a.j.trim().equals("")) {
                    a91.G((Context)this.a.c, 2131886267, 2131230909, 2131231086);
                    final AddStudentActivity a5 = this.a;
                    a5.d.setError((CharSequence)((Context)a5).getString(2131886267));
                    return;
                }
                if (this.a.k.equals("")) {
                    a91.G((Context)this.a.c, 2131886269, 2131230909, 2131231086);
                    return;
                }
                if (this.a.m.length() > 0 && this.a.m.trim().indexOf(64) < 1) {
                    a91.G((Context)this.a.c, 2131886490, 2131230909, 2131231086);
                    return;
                }
                if (this.a.l.length() > 0 && (!this.a.l.matches("[0-9]+") || this.a.l.length() <= 7)) {
                    a91.G((Context)this.a.c, 2131886491, 2131230909, 2131231086);
                    return;
                }
                final int int1 = Integer.parseInt(this.a.j);
                final AddStudentActivity a6 = this.a;
                final StudentDataModel studentDataModel = new StudentDataModel(int1, a6.k, a6.m, a6.l, AddStudentActivity.A(a6));
                ClassRepository.getInstance((Context)this.a.c).deleteStudent(Integer.parseInt(this.a.j), AddStudentActivity.A(this.a));
                ClassRepository.getInstance((Context)this.a.c).saveOrUpdateStudent(studentDataModel);
                FirebaseAnalytics.getInstance((Context)this.a.c).a("StudentAdd", null);
                this.a.c.finish();
            }
        });
    }
    
    public final void D() {
        this.findViewById(2131296418).setOnClickListener((View$OnClickListener)new View$OnClickListener(this) {
            public final AddStudentActivity a;
            
            public void onClick(final View view) {
                this.a.c.finish();
            }
        });
    }
    
    public final void E() {
        this.x(this.q = (Toolbar)this.findViewById(2131297335));
        this.q.setNavigationOnClickListener((View$OnClickListener)new View$OnClickListener(this) {
            public final AddStudentActivity a;
            
            public void onClick(final View view) {
                this.a.finish();
            }
        });
    }
    
    @Override
    public void onCreate(Bundle extras) {
        super.onCreate(extras);
        this.setContentView(2131492893);
        extras = this.getIntent().getExtras();
        this.E();
        this.d = (TextInputLayout)this.findViewById(2131297180);
        this.e = (TextInputLayout)this.findViewById(2131297181);
        this.g = (TextInputLayout)this.findViewById(2131297179);
        this.h = (TextInputLayout)this.findViewById(2131297173);
        this.f = (TextInputLayout)this.findViewById(2131297172);
        this.i = (Button)this.findViewById(2131296414);
        if (extras != null) {
            this.n = ((BaseBundle)extras).getString("CLASS_NAME");
            this.p = (StudentDataModel)this.getIntent().getSerializableExtra("STUDENT_MODEL");
        }
        ((TextView)this.e.getEditText()).addTextChangedListener(this.t);
        ((TextView)this.d.getEditText()).addTextChangedListener(this.t);
        this.D();
        this.C();
    }
    
    @Override
    public void onResume() {
        super.onResume();
        this.B();
    }
}

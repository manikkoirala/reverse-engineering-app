// 
// Decompiled by Procyon v0.6.0
// 

package com.ekodroid.omrevaluator.clients.UserProfileClients.models;

import java.io.Serializable;

public class Device implements Serializable
{
    String deviceId;
    String fcmId;
    String info;
    long lastUpdate;
    String platform;
    
    public Device() {
    }
    
    public Device(final String deviceId, final long lastUpdate, final String platform, final String info, final String fcmId) {
        this.deviceId = deviceId;
        this.lastUpdate = lastUpdate;
        this.platform = platform;
        this.info = info;
        this.fcmId = fcmId;
    }
    
    public String getDeviceId() {
        return this.deviceId;
    }
    
    public String getFcmId() {
        return this.fcmId;
    }
    
    public String getInfo() {
        return this.info;
    }
    
    public long getLastUpdate() {
        return this.lastUpdate;
    }
    
    public String getPlatform() {
        return this.platform;
    }
    
    public void setDeviceId(final String deviceId) {
        this.deviceId = deviceId;
    }
    
    public void setFcmId(final String fcmId) {
        this.fcmId = fcmId;
    }
    
    public void setInfo(final String info) {
        this.info = info;
    }
    
    public void setLastUpdate(final long lastUpdate) {
        this.lastUpdate = lastUpdate;
    }
    
    public void setPlatform(final String platform) {
        this.platform = platform;
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.ekodroid.omrevaluator.clients.UserProfileClients.models;

import android.content.Context;
import java.io.Serializable;

public class ReportStorageDto implements Serializable
{
    public long reportsCount;
    
    public ReportStorageDto(final long reportsCount) {
        this.reportsCount = reportsCount;
    }
    
    public static ReportStorageDto getInstance(final Context context) {
        return (ReportStorageDto)new gc0().j(context.getApplicationContext().getSharedPreferences("MyPref", 0).getString("report_storage_profile", (String)null), ReportStorageDto.class);
    }
    
    public long getReportsCount() {
        return this.reportsCount;
    }
    
    public void save(final Context context) {
        context.getApplicationContext().getSharedPreferences("MyPref", 0).edit().putString("report_storage_profile", new gc0().s(this)).commit();
    }
    
    public void setReportsCount(final long reportsCount) {
        this.reportsCount = reportsCount;
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.ekodroid.omrevaluator.clients.UserProfileClients.models;

import java.io.Serializable;

public class PendingJoinRequest implements Serializable
{
    String orgName;
    UserRole role;
    String senderUserId;
    String sentByEmail;
    String sentByName;
    
    public PendingJoinRequest(final String sentByName, final String senderUserId, final String sentByEmail, final UserRole role, final String orgName) {
        this.sentByName = sentByName;
        this.senderUserId = senderUserId;
        this.sentByEmail = sentByEmail;
        this.role = role;
        this.orgName = orgName;
    }
    
    public String getOrgName() {
        return this.orgName;
    }
    
    public UserRole getRole() {
        return this.role;
    }
    
    public String getSenderUserId() {
        return this.senderUserId;
    }
    
    public String getSentByEmail() {
        return this.sentByEmail;
    }
    
    public String getSentByName() {
        return this.sentByName;
    }
}

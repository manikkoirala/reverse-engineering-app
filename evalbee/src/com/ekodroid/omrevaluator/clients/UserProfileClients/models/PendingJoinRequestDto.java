// 
// Decompiled by Procyon v0.6.0
// 

package com.ekodroid.omrevaluator.clients.UserProfileClients.models;

import com.ekodroid.omrevaluator.more.models.Teacher;
import java.io.Serializable;

public class PendingJoinRequestDto implements Serializable
{
    Teacher.MemberStatus memberStatus;
    PendingJoinRequest pendingJoinRequest;
    
    public PendingJoinRequestDto(final PendingJoinRequest pendingJoinRequest, final Teacher.MemberStatus memberStatus) {
        this.pendingJoinRequest = pendingJoinRequest;
        this.memberStatus = memberStatus;
    }
    
    public Teacher.MemberStatus getMemberStatus() {
        return this.memberStatus;
    }
    
    public PendingJoinRequest getPendingJoinRequest() {
        return this.pendingJoinRequest;
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.ekodroid.omrevaluator.clients.UserProfileClients.models;

import java.io.Serializable;

public class UserAccountDeleteRequest implements Serializable
{
    boolean deleted;
    String email;
    long requestedOn;
    String uid;
    
    public UserAccountDeleteRequest(final String uid, final String email, final int n, final boolean deleted) {
        this.uid = uid;
        this.email = email;
        this.requestedOn = n;
        this.deleted = deleted;
    }
    
    public String getEmail() {
        return this.email;
    }
    
    public long getRequestedOn() {
        return this.requestedOn;
    }
    
    public String getUid() {
        return this.uid;
    }
    
    public boolean isDeleted() {
        return this.deleted;
    }
    
    public void setDeleted(final boolean deleted) {
        this.deleted = deleted;
    }
    
    public void setEmail(final String email) {
        this.email = email;
    }
    
    public void setRequestedOn(final long requestedOn) {
        this.requestedOn = requestedOn;
    }
    
    public void setUid(final String uid) {
        this.uid = uid;
    }
}

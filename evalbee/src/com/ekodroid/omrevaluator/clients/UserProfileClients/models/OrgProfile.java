// 
// Decompiled by Procyon v0.6.0
// 

package com.ekodroid.omrevaluator.clients.UserProfileClients.models;

import android.content.SharedPreferences;
import android.content.Intent;
import com.ekodroid.omrevaluator.activities.Services.SyncSerivice;
import android.content.Context;
import com.ekodroid.omrevaluator.more.models.Teacher;
import java.util.ArrayList;
import java.io.Serializable;

public class OrgProfile implements Serializable
{
    String displayName;
    String orgId;
    String organization;
    PendingJoinRequest request;
    UserRole role;
    ArrayList<Teacher> teacherList;
    
    public OrgProfile() {
    }
    
    public OrgProfile(final String displayName, final String organization, final String orgId, final UserRole role, final PendingJoinRequest request, final ArrayList<Teacher> teacherList) {
        this.displayName = displayName;
        this.organization = organization;
        this.orgId = orgId;
        this.role = role;
        this.request = request;
        this.teacherList = teacherList;
    }
    
    public static OrgProfile getInstance(final Context context) {
        return (OrgProfile)new gc0().j(context.getApplicationContext().getSharedPreferences("MyPref", 0).getString("org_profile", (String)null), OrgProfile.class);
    }
    
    public static void reset(final Context context) {
        final SharedPreferences sharedPreferences = context.getApplicationContext().getSharedPreferences("MyPref", 0);
        sharedPreferences.edit().putString("org_profile", (String)null).commit();
        sharedPreferences.edit().putString(ok.y, (String)null).commit();
        context.startService(new Intent(context.getApplicationContext(), (Class)SyncSerivice.class));
    }
    
    public String getDisplayName() {
        return this.displayName;
    }
    
    public String getOrgId() {
        return this.orgId;
    }
    
    public String getOrganization() {
        return this.organization;
    }
    
    public PendingJoinRequest getRequest() {
        return this.request;
    }
    
    public UserRole getRole() {
        return this.role;
    }
    
    public ArrayList<Teacher> getTeacherList() {
        return this.teacherList;
    }
    
    public void save(final Context context) {
        context.getApplicationContext().getSharedPreferences("MyPref", 0).edit().putString("org_profile", new gc0().s(this)).commit();
    }
    
    public void setDisplayName(final String displayName) {
        this.displayName = displayName;
    }
    
    public void setOrgId(final String orgId) {
        this.orgId = orgId;
    }
    
    public void setOrganization(final String organization) {
        this.organization = organization;
    }
    
    public void setRequest(final PendingJoinRequest request) {
        this.request = request;
    }
    
    public void setRole(final UserRole role) {
        this.role = role;
    }
    
    public void setTeacherList(final ArrayList<Teacher> teacherList) {
        this.teacherList = teacherList;
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.ekodroid.omrevaluator.clients.UserProfileClients.models;

import java.io.Serializable;

public class UserProfileDto implements Serializable
{
    String displayName;
    String orgName;
    
    public UserProfileDto(final String displayName, final String orgName) {
        this.displayName = displayName;
        this.orgName = orgName;
    }
    
    public String getDisplayName() {
        return this.displayName;
    }
    
    public String getOrgName() {
        return this.orgName;
    }
}

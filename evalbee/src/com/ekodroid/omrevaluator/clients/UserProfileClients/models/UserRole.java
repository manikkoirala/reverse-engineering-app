// 
// Decompiled by Procyon v0.6.0
// 

package com.ekodroid.omrevaluator.clients.UserProfileClients.models;

import java.io.Serializable;

public enum UserRole implements Serializable
{
    private static final UserRole[] $VALUES;
    
    ADMIN, 
    OWNER, 
    TEACHER;
    
    private static /* synthetic */ UserRole[] $values() {
        return new UserRole[] { UserRole.OWNER, UserRole.ADMIN, UserRole.TEACHER };
    }
    
    static {
        $VALUES = $values();
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.ekodroid.omrevaluator.clients.students.model;

import java.util.ArrayList;
import java.io.Serializable;

public class BulkStudentRequest implements Serializable
{
    public ArrayList<StudentDto> students;
    
    public BulkStudentRequest(final ArrayList<StudentDto> students) {
        this.students = students;
    }
}

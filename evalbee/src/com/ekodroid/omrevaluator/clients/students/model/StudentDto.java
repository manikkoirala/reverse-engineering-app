// 
// Decompiled by Procyon v0.6.0
// 

package com.ekodroid.omrevaluator.clients.students.model;

import java.util.ArrayList;
import com.ekodroid.omrevaluator.database.StudentDataModel;
import java.io.Serializable;

public class StudentDto implements Serializable
{
    public String className;
    public String emailId;
    public String phoneNo;
    public int rollNumber;
    public String studentName;
    
    public StudentDto(final int rollNumber, final String studentName, final String className, final String phoneNo, final String emailId) {
        this.rollNumber = rollNumber;
        this.studentName = studentName;
        this.className = className;
        this.phoneNo = phoneNo;
        this.emailId = emailId;
    }
    
    public StudentDto(final StudentDataModel studentDataModel) {
        this.rollNumber = studentDataModel.getRollNO();
        this.studentName = studentDataModel.getStudentName();
        this.className = studentDataModel.getClassName();
        this.phoneNo = studentDataModel.getPhoneNo();
        this.emailId = studentDataModel.getEmailId();
    }
    
    public static ArrayList<StudentDto> getStudentDto(final ArrayList<StudentDataModel> list) {
        final ArrayList list2 = new ArrayList();
        for (int i = 0; i < list.size(); ++i) {
            list2.add(new StudentDto((StudentDataModel)list.get(i)));
        }
        return list2;
    }
    
    public String getClassName() {
        return this.className;
    }
    
    public String getEmailId() {
        return this.emailId;
    }
    
    public String getPhoneNo() {
        return this.phoneNo;
    }
    
    public int getRollNumber() {
        return this.rollNumber;
    }
    
    public String getStudentName() {
        return this.studentName;
    }
    
    public void setClassName(final String className) {
        this.className = className;
    }
    
    public void setEmailId(final String emailId) {
        this.emailId = emailId;
    }
    
    public void setPhoneNo(final String phoneNo) {
        this.phoneNo = phoneNo;
    }
    
    public void setRollNumber(final int rollNumber) {
        this.rollNumber = rollNumber;
    }
    
    public void setStudentName(final String studentName) {
        this.studentName = studentName;
    }
    
    public StudentDataModel toDataModel() {
        return new StudentDataModel(this.rollNumber, this.studentName, this.emailId, this.phoneNo, this.className);
    }
}

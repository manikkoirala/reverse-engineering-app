// 
// Decompiled by Procyon v0.6.0
// 

package com.ekodroid.omrevaluator.clients.students.model;

import java.util.ArrayList;
import java.io.Serializable;

public class PageResponse<T> implements Serializable
{
    private ArrayList<T> content;
    private boolean empty;
    private Pageable pageable;
    private int size;
    private int totalElements;
    private int totalPages;
    
    public ArrayList<T> getContent() {
        return this.content;
    }
    
    public Pageable getPageable() {
        return this.pageable;
    }
    
    public int getSize() {
        return this.size;
    }
    
    public int getTotalElements() {
        return this.totalElements;
    }
    
    public int getTotalPages() {
        return this.totalPages;
    }
    
    public boolean isEmpty() {
        return this.empty;
    }
    
    public void setContent(final ArrayList<T> content) {
        this.content = content;
    }
    
    public void setEmpty(final boolean empty) {
        this.empty = empty;
    }
    
    public void setPageable(final Pageable pageable) {
        this.pageable = pageable;
    }
    
    public void setSize(final int size) {
        this.size = size;
    }
    
    public void setTotalElements(final int totalElements) {
        this.totalElements = totalElements;
    }
    
    public void setTotalPages(final int totalPages) {
        this.totalPages = totalPages;
    }
}

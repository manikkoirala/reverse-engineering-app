// 
// Decompiled by Procyon v0.6.0
// 

package com.ekodroid.omrevaluator.clients.students.model;

import java.io.Serializable;

public class Pageable implements Serializable
{
    private int offset;
    private int pageNumber;
    private int pageSize;
    
    public int getOffset() {
        return this.offset;
    }
    
    public int getPageNumber() {
        return this.pageNumber;
    }
    
    public int getPageSize() {
        return this.pageSize;
    }
}

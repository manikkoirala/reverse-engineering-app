// 
// Decompiled by Procyon v0.6.0
// 

package com.ekodroid.omrevaluator.clients.students;

import com.google.android.gms.tasks.Task;
import com.google.android.gms.tasks.OnCompleteListener;
import com.android.volley.Request;
import java.util.HashMap;
import com.google.firebase.auth.FirebaseAuth;
import java.util.Map;
import com.android.volley.VolleyError;
import java.util.Iterator;
import com.ekodroid.omrevaluator.database.repositories.ClassRepository;
import com.ekodroid.omrevaluator.clients.students.model.StudentDto;
import com.google.gson.reflect.TypeToken;
import com.ekodroid.omrevaluator.clients.students.model.PageResponse;
import com.android.volley.d;
import android.net.Uri$Builder;
import android.net.Uri;
import android.content.Context;

public class GetUpdatedStudents
{
    public ee1 a;
    public zg b;
    public String c;
    public long d;
    public int e;
    public int f;
    public Context g;
    
    public GetUpdatedStudents(final Context g, final long n, final int n2, final int n3, final zg b) {
        this.g = g;
        this.b = b;
        this.d = n;
        this.e = n2;
        this.f = n3;
        this.a = new n52(g, a91.x()).b();
        final StringBuilder sb = new StringBuilder();
        sb.append("https://");
        sb.append(a91.x());
        sb.append(":");
        sb.append(a91.q());
        sb.append("/api-online/students/sync");
        final Uri$Builder buildUpon = Uri.parse(sb.toString()).buildUpon();
        final StringBuilder sb2 = new StringBuilder();
        sb2.append("");
        sb2.append(n2);
        final Uri$Builder appendQueryParameter = buildUpon.appendQueryParameter("pageNo", sb2.toString());
        final StringBuilder sb3 = new StringBuilder();
        sb3.append("");
        sb3.append(n3);
        final Uri$Builder appendQueryParameter2 = appendQueryParameter.appendQueryParameter("pageSize", sb3.toString());
        final StringBuilder sb4 = new StringBuilder();
        sb4.append("");
        sb4.append(n);
        this.c = appendQueryParameter2.appendQueryParameter("after", sb4.toString()).build().toString();
        this.i();
    }
    
    public static /* synthetic */ Context c(final GetUpdatedStudents getUpdatedStudents) {
        return getUpdatedStudents.g;
    }
    
    public static /* synthetic */ long d(final GetUpdatedStudents getUpdatedStudents) {
        return getUpdatedStudents.d;
    }
    
    public static /* synthetic */ int e(final GetUpdatedStudents getUpdatedStudents) {
        return getUpdatedStudents.f;
    }
    
    public static /* synthetic */ zg f(final GetUpdatedStudents getUpdatedStudents) {
        return getUpdatedStudents.b;
    }
    
    public final void g(final String s) {
        final hr1 hr1 = new hr1(this, 0, this.c, new d.b(this) {
            public final GetUpdatedStudents a;
            
            public void b(final String s) {
                try {
                    final PageResponse pageResponse = (PageResponse)new gc0().k(s, new TypeToken<PageResponse<StudentDto>>(this) {
                        public final GetUpdatedStudents$2 a;
                    }.getType());
                    new ya(new Runnable(this, pageResponse) {
                        public final PageResponse a;
                        public final GetUpdatedStudents$2 b;
                        
                        @Override
                        public void run() {
                            final ClassRepository instance = ClassRepository.getInstance(GetUpdatedStudents.c(this.b.a));
                            for (final StudentDto studentDto : this.a.getContent()) {
                                instance.deleteStudent(studentDto.getRollNumber(), studentDto.getClassName());
                                instance.saveOrUpdateStudentAsSynced(studentDto.toDataModel());
                            }
                        }
                    }, new Runnable(this, pageResponse) {
                        public final PageResponse a;
                        public final GetUpdatedStudents$2 b;
                        
                        @Override
                        public void run() {
                            final PageResponse a = this.a;
                            if (a != null && a.getPageable().getPageNumber() + 1 < this.a.getTotalPages()) {
                                new GetUpdatedStudents(GetUpdatedStudents.c(this.b.a), GetUpdatedStudents.d(this.b.a), this.a.getPageable().getPageNumber() + 1, GetUpdatedStudents.e(this.b.a), GetUpdatedStudents.f(this.b.a));
                            }
                            else {
                                this.b.a.h(true, 200, this.a);
                            }
                        }
                    }).execute((Object[])new Void[0]);
                }
                catch (final Exception ex) {
                    ex.printStackTrace();
                    this.a.h(false, 400, null);
                }
            }
        }, new d.a(this) {
            public final GetUpdatedStudents a;
            
            @Override
            public void a(final VolleyError volleyError) {
                volleyError.printStackTrace();
                final yy0 networkResponse = volleyError.networkResponse;
                int a;
                if (networkResponse != null) {
                    a = networkResponse.a;
                }
                else {
                    a = 400;
                }
                this.a.h(false, a, volleyError.getMessage());
            }
        }, s) {
            public final String w;
            public final GetUpdatedStudents x;
            
            @Override
            public Map o() {
                final String o = FirebaseAuth.getInstance().e().O();
                final String email = FirebaseAuth.getInstance().e().getEmail();
                final HashMap hashMap = new HashMap();
                hashMap.put("x-user-id", o);
                hashMap.put("x-user-email", email);
                hashMap.put("x-user-token", this.w);
                return hashMap;
            }
        };
        hr1.L(new wq(20000, 0, 1.0f));
        this.a.d().clear();
        this.a.a(hr1);
    }
    
    public final void h(final boolean b, final int n, final Object o) {
        final zg b2 = this.b;
        if (b2 != null) {
            b2.a(b, n, o);
        }
    }
    
    public final void i() {
        FirebaseAuth.getInstance().e().i(false).addOnCompleteListener((OnCompleteListener)new OnCompleteListener(this) {
            public final GetUpdatedStudents a;
            
            public void onComplete(final Task task) {
                if (task.isSuccessful()) {
                    this.a.g(((ya0)task.getResult()).c());
                }
                else {
                    this.a.h(false, 400, null);
                }
            }
        });
    }
}

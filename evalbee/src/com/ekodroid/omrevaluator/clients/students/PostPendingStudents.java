// 
// Decompiled by Procyon v0.6.0
// 

package com.ekodroid.omrevaluator.clients.students;

import com.google.android.gms.tasks.Task;
import com.google.android.gms.tasks.OnCompleteListener;
import com.android.volley.Request;
import java.util.HashMap;
import com.google.firebase.auth.FirebaseAuth;
import java.util.Map;
import java.io.UnsupportedEncodingException;
import com.android.volley.VolleyError;
import java.util.Iterator;
import com.ekodroid.omrevaluator.database.repositories.ClassRepository;
import com.ekodroid.omrevaluator.clients.students.model.StudentDto;
import com.google.gson.reflect.TypeToken;
import java.util.ArrayList;
import com.android.volley.d;
import android.content.Context;
import com.ekodroid.omrevaluator.clients.students.model.BulkStudentRequest;

public class PostPendingStudents
{
    public ee1 a;
    public zg b;
    public BulkStudentRequest c;
    public Context d;
    public String e;
    
    public PostPendingStudents(final BulkStudentRequest c, final Context d, final zg b) {
        this.b = b;
        this.d = d;
        this.c = c;
        this.a = new n52(d, a91.x()).b();
        final StringBuilder sb = new StringBuilder();
        sb.append("https://");
        sb.append(a91.x());
        sb.append(":");
        sb.append(a91.q());
        sb.append("/api-online/students");
        this.e = sb.toString();
        this.f();
    }
    
    public static /* synthetic */ BulkStudentRequest c(final PostPendingStudents postPendingStudents) {
        return postPendingStudents.c;
    }
    
    public final void d(final String s) {
        final hr1 hr1 = new hr1(this, 2, this.e, new d.b(this) {
            public final PostPendingStudents a;
            
            public void b(final String s) {
                try {
                    final ArrayList list = (ArrayList)new gc0().k(s, new TypeToken<ArrayList<StudentDto>>(this) {
                        public final PostPendingStudents$2 a;
                    }.getType());
                    new ya(new Runnable(this, list) {
                        public final ArrayList a;
                        public final PostPendingStudents$2 b;
                        
                        @Override
                        public void run() {
                            final ClassRepository instance = ClassRepository.getInstance(this.b.a.d);
                            for (final StudentDto studentDto : this.a) {
                                instance.deleteStudent(studentDto.getRollNumber(), studentDto.getClassName());
                                instance.saveOrUpdateStudentAsSynced(studentDto.toDataModel());
                            }
                        }
                    }, new Runnable(this, list) {
                        public final ArrayList a;
                        public final PostPendingStudents$2 b;
                        
                        @Override
                        public void run() {
                            this.b.a.e(true, 200, this.a.size());
                        }
                    }).execute((Object[])new Void[0]);
                }
                catch (final Exception ex) {
                    ex.printStackTrace();
                    this.a.e(false, 400, null);
                }
            }
        }, new d.a(this) {
            public final PostPendingStudents a;
            
            @Override
            public void a(final VolleyError volleyError) {
                final yy0 networkResponse = volleyError.networkResponse;
                int a;
                if (networkResponse != null) {
                    a = networkResponse.a;
                }
                else {
                    a = 400;
                }
                this.a.e(false, a, volleyError.getMessage());
            }
        }, s) {
            public final String w;
            public final PostPendingStudents x;
            
            @Override
            public byte[] k() {
                try {
                    return new gc0().s(PostPendingStudents.c(this.x)).getBytes("UTF-8");
                }
                catch (final UnsupportedEncodingException ex) {
                    ex.printStackTrace();
                    return null;
                }
            }
            
            @Override
            public String l() {
                return "application/json";
            }
            
            @Override
            public Map o() {
                final String o = FirebaseAuth.getInstance().e().O();
                final String email = FirebaseAuth.getInstance().e().getEmail();
                final HashMap hashMap = new HashMap();
                hashMap.put("x-user-id", o);
                hashMap.put("x-user-email", email);
                hashMap.put("x-user-token", this.w);
                return hashMap;
            }
        };
        hr1.L(new wq(20000, 0, 1.0f));
        this.a.a(hr1);
    }
    
    public final void e(final boolean b, final int n, final Object o) {
        final zg b2 = this.b;
        if (b2 != null) {
            b2.a(b, n, o);
        }
    }
    
    public final void f() {
        FirebaseAuth.getInstance().e().i(false).addOnCompleteListener((OnCompleteListener)new OnCompleteListener(this) {
            public final PostPendingStudents a;
            
            public void onComplete(final Task task) {
                if (task.isSuccessful()) {
                    this.a.d(((ya0)task.getResult()).c());
                }
                else {
                    this.a.e(false, 400, null);
                }
            }
        });
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.ekodroid.omrevaluator.clients.SyncClients;

import com.google.android.gms.tasks.Task;
import com.google.android.gms.tasks.OnCompleteListener;
import com.android.volley.Request;
import java.util.HashMap;
import com.google.firebase.auth.FirebaseAuth;
import java.util.Map;
import java.io.UnsupportedEncodingException;
import com.android.volley.VolleyError;
import com.ekodroid.omrevaluator.database.TemplateDataJsonModel;
import com.ekodroid.omrevaluator.database.repositories.TemplateRepository;
import com.android.volley.d;
import android.content.Context;
import com.ekodroid.omrevaluator.clients.SyncClients.Models.SharedExamPartial;

public class a
{
    public ee1 a;
    public zg b;
    public SharedExamPartial c;
    public Context d;
    public String e;
    
    public a(final SharedExamPartial c, final Context d, final zg b) {
        final StringBuilder sb = new StringBuilder();
        sb.append("http://");
        sb.append(a91.v());
        sb.append(":");
        sb.append(a91.k());
        sb.append("/api/sync/shared-exam");
        this.e = sb.toString();
        this.b = b;
        this.c = c;
        this.d = d;
        this.a = new n52(d, a91.v()).b();
        this.g();
    }
    
    public static /* synthetic */ Context c(final a a) {
        return a.d;
    }
    
    public static /* synthetic */ SharedExamPartial d(final a a) {
        return a.c;
    }
    
    public final void e(final String s) {
        final hr1 hr1 = new hr1(this, 1, this.e, new d.b(this) {
            public final a a;
            
            public void b(final String s) {
                try {
                    final CreateSyncExam.ResponseCreateExam responseCreateExam = (CreateSyncExam.ResponseCreateExam)new gc0().j(s, CreateSyncExam.ResponseCreateExam.class);
                    final TemplateRepository instance = TemplateRepository.getInstance(com.ekodroid.omrevaluator.clients.SyncClients.a.c(this.a));
                    final TemplateDataJsonModel templateJsonForCloudId = instance.getTemplateJsonForCloudId(com.ekodroid.omrevaluator.clients.SyncClients.a.d(this.a).getExamId());
                    if (templateJsonForCloudId != null) {
                        templateJsonForCloudId.setSharedType(com.ekodroid.omrevaluator.clients.SyncClients.a.d(this.a).getSharedType());
                        instance.saveOrUpdateTemplateJsonAsSynced(templateJsonForCloudId);
                    }
                    this.a.f(true, 200, responseCreateExam);
                }
                catch (final Exception ex) {
                    ex.printStackTrace();
                    this.a.f(false, 400, null);
                }
            }
        }, new d.a(this) {
            public final a a;
            
            @Override
            public void a(final VolleyError volleyError) {
                final yy0 networkResponse = volleyError.networkResponse;
                int a;
                if (networkResponse != null) {
                    a = networkResponse.a;
                }
                else {
                    a = 400;
                }
                this.a.f(false, a, volleyError.getMessage());
            }
        }, s) {
            public final String w;
            public final a x;
            
            @Override
            public byte[] k() {
                try {
                    return new gc0().s(a.d(this.x)).getBytes("UTF-8");
                }
                catch (final UnsupportedEncodingException ex) {
                    ex.printStackTrace();
                    return null;
                }
            }
            
            @Override
            public String l() {
                return "application/json";
            }
            
            @Override
            public Map o() {
                final String o = FirebaseAuth.getInstance().e().O();
                final String email = FirebaseAuth.getInstance().e().getEmail();
                final HashMap hashMap = new HashMap();
                hashMap.put("x-user-id", o);
                hashMap.put("x-user-token", this.w);
                hashMap.put("x-user-email", email);
                return hashMap;
            }
        };
        hr1.L(new wq(15000, 0, 1.0f));
        this.a.a(hr1);
    }
    
    public final void f(final boolean b, final int n, final Object o) {
        final zg b2 = this.b;
        if (b2 != null) {
            b2.a(b, n, o);
        }
    }
    
    public final void g() {
        FirebaseAuth.getInstance().e().i(false).addOnCompleteListener((OnCompleteListener)new OnCompleteListener(this) {
            public final a a;
            
            public void onComplete(final Task task) {
                if (task.isSuccessful()) {
                    this.a.e(((ya0)task.getResult()).c());
                }
                else {
                    this.a.f(false, 400, null);
                }
            }
        });
    }
}

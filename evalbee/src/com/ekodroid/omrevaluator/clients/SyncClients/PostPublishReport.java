// 
// Decompiled by Procyon v0.6.0
// 

package com.ekodroid.omrevaluator.clients.SyncClients;

import java.io.Serializable;
import com.google.android.gms.tasks.Task;
import com.google.android.gms.tasks.OnCompleteListener;
import com.android.volley.Request;
import java.util.HashMap;
import com.google.firebase.auth.FirebaseAuth;
import java.util.Map;
import java.io.UnsupportedEncodingException;
import com.android.volley.VolleyError;
import com.ekodroid.omrevaluator.clients.SyncClients.Models.PublishResponse;
import com.android.volley.d;
import android.content.Context;
import com.ekodroid.omrevaluator.clients.SyncClients.Models.PublishRequest;

public class PostPublishReport
{
    public ee1 a;
    public zg b;
    public PublishRequest c;
    public String d;
    
    public PostPublishReport(final PublishRequest c, final Context context, final zg b) {
        this.b = b;
        this.c = c;
        this.a = new n52(context, a91.v()).b();
        final StringBuilder sb = new StringBuilder();
        sb.append("http://");
        sb.append(a91.v());
        sb.append(":");
        sb.append(a91.k());
        sb.append("/api/sync/publishReport");
        this.d = sb.toString();
    }
    
    public static /* synthetic */ PublishRequest c(final PostPublishReport postPublishReport) {
        return postPublishReport.c;
    }
    
    public final void d(final String s) {
        final hr1 hr1 = new hr1(this, 1, this.d, new d.b(this) {
            public final PostPublishReport a;
            
            public void b(final String s) {
                try {
                    this.a.e(true, 200, new gc0().j(s, PublishResponse.class));
                }
                catch (final Exception ex) {
                    ex.printStackTrace();
                    this.a.e(false, 400, null);
                }
            }
        }, new d.a(this) {
            public final PostPublishReport a;
            
            @Override
            public void a(final VolleyError volleyError) {
                final yy0 networkResponse = volleyError.networkResponse;
                int a;
                if (networkResponse != null) {
                    a = networkResponse.a;
                }
                else {
                    a = 400;
                }
                this.a.e(false, a, volleyError.getMessage());
            }
        }, s) {
            public final String w;
            public final PostPublishReport x;
            
            @Override
            public byte[] k() {
                try {
                    return new gc0().s(PostPublishReport.c(this.x)).getBytes("UTF-8");
                }
                catch (final UnsupportedEncodingException ex) {
                    ex.printStackTrace();
                    return null;
                }
            }
            
            @Override
            public String l() {
                return "application/json";
            }
            
            @Override
            public Map o() {
                final String o = FirebaseAuth.getInstance().e().O();
                final String email = FirebaseAuth.getInstance().e().getEmail();
                final HashMap hashMap = new HashMap();
                hashMap.put("x-user-id", o);
                hashMap.put("x-user-email", email);
                hashMap.put("x-user-token", this.w);
                return hashMap;
            }
        };
        hr1.L(new wq(20000, 0, 1.0f));
        this.a.a(hr1);
    }
    
    public final void e(final boolean b, final int n, final Object o) {
        final zg b2 = this.b;
        if (b2 != null) {
            b2.a(b, n, o);
        }
    }
    
    public void f() {
        FirebaseAuth.getInstance().e().i(false).addOnCompleteListener((OnCompleteListener)new OnCompleteListener(this) {
            public final PostPublishReport a;
            
            public void onComplete(final Task task) {
                if (task.isSuccessful()) {
                    this.a.d(((ya0)task.getResult()).c());
                }
                else {
                    this.a.e(false, 400, null);
                }
            }
        });
    }
    
    public class ResponseCreateExam implements Serializable
    {
        Long id;
        final PostPublishReport this$0;
        Long updatedOn;
        
        public ResponseCreateExam(final PostPublishReport this$0) {
            this.this$0 = this$0;
        }
        
        public Long getId() {
            return this.id;
        }
        
        public Long getUpdatedOn() {
            return this.updatedOn;
        }
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.ekodroid.omrevaluator.clients.SyncClients;

import com.ekodroid.omrevaluator.serializable.SharedType;
import java.io.Serializable;
import com.google.android.gms.tasks.Task;
import com.google.android.gms.tasks.OnCompleteListener;
import com.android.volley.Request;
import java.util.HashMap;
import com.google.firebase.auth.FirebaseAuth;
import java.util.Map;
import java.io.UnsupportedEncodingException;
import com.android.volley.VolleyError;
import com.android.volley.d;
import android.content.Context;
import com.ekodroid.omrevaluator.clients.SyncClients.Models.ExamFile;

public class CreateSyncExam
{
    public ee1 a;
    public zg b;
    public ExamFile c;
    public String d;
    
    public CreateSyncExam(final ExamFile c, final Context context, final zg b) {
        final StringBuilder sb = new StringBuilder();
        sb.append("http://");
        sb.append(a91.v());
        sb.append(":");
        sb.append(a91.k());
        sb.append("/api/sync/exam");
        this.d = sb.toString();
        this.b = b;
        this.c = c;
        this.a = new n52(context, a91.v()).b();
        this.f();
    }
    
    public static /* synthetic */ ExamFile c(final CreateSyncExam createSyncExam) {
        return createSyncExam.c;
    }
    
    public final void d(final String s) {
        final hr1 hr1 = new hr1(this, 1, this.d, new d.b(this) {
            public final CreateSyncExam a;
            
            public void b(final String s) {
                try {
                    this.a.e(true, 200, new gc0().j(s, ResponseCreateExam.class));
                }
                catch (final Exception ex) {
                    ex.printStackTrace();
                    this.a.e(false, 400, null);
                }
            }
        }, new d.a(this) {
            public final CreateSyncExam a;
            
            @Override
            public void a(final VolleyError volleyError) {
                final yy0 networkResponse = volleyError.networkResponse;
                int a;
                if (networkResponse != null) {
                    a = networkResponse.a;
                }
                else {
                    a = 400;
                }
                this.a.e(false, a, volleyError.getMessage());
            }
        }, s) {
            public final String w;
            public final CreateSyncExam x;
            
            @Override
            public byte[] k() {
                try {
                    return new gc0().s(CreateSyncExam.c(this.x)).getBytes("UTF-8");
                }
                catch (final UnsupportedEncodingException ex) {
                    ex.printStackTrace();
                    return null;
                }
            }
            
            @Override
            public String l() {
                return "application/json";
            }
            
            @Override
            public Map o() {
                final String o = FirebaseAuth.getInstance().e().O();
                final String email = FirebaseAuth.getInstance().e().getEmail();
                final HashMap hashMap = new HashMap();
                hashMap.put("x-user-id", o);
                hashMap.put("x-user-token", this.w);
                hashMap.put("x-user-email", email);
                return hashMap;
            }
        };
        hr1.L(new wq(15000, 0, 1.0f));
        this.a.a(hr1);
    }
    
    public final void e(final boolean b, final int n, final Object o) {
        final zg b2 = this.b;
        if (b2 != null) {
            b2.a(b, n, o);
        }
    }
    
    public final void f() {
        FirebaseAuth.getInstance().e().i(false).addOnCompleteListener((OnCompleteListener)new OnCompleteListener(this) {
            public final CreateSyncExam a;
            
            public void onComplete(final Task task) {
                if (task.isSuccessful()) {
                    this.a.d(((ya0)task.getResult()).c());
                }
                else {
                    this.a.e(false, 400, null);
                }
            }
        });
    }
    
    public class ResponseCreateExam implements Serializable
    {
        Long id;
        SharedType sharedType;
        boolean syncImages;
        final CreateSyncExam this$0;
        Long updatedOn;
        String userId;
        
        public ResponseCreateExam(final CreateSyncExam this$0) {
            this.this$0 = this$0;
        }
        
        public Long getId() {
            return this.id;
        }
        
        public SharedType getSharedType() {
            return this.sharedType;
        }
        
        public Long getUpdatedOn() {
            return this.updatedOn;
        }
        
        public String getUserId() {
            return this.userId;
        }
        
        public boolean isSyncImages() {
            return this.syncImages;
        }
    }
}

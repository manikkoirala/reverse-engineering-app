// 
// Decompiled by Procyon v0.6.0
// 

package com.ekodroid.omrevaluator.clients.SyncClients.Models;

import java.io.Serializable;

public class StudentResultPartialResponse implements Serializable
{
    Published published;
    String report;
    int reportVersion;
    int rollNo;
    Long updatedOn;
    
    public StudentResultPartialResponse(final int rollNo, final Long updatedOn, final String report, final int reportVersion, final Published published) {
        this.rollNo = rollNo;
        this.updatedOn = updatedOn;
        this.report = report;
        this.reportVersion = reportVersion;
        this.published = published;
    }
    
    public Published getPublished() {
        return this.published;
    }
    
    public String getReport() {
        return this.report;
    }
    
    public int getReportVersion() {
        return this.reportVersion;
    }
    
    public int getRollNo() {
        return this.rollNo;
    }
    
    public Long getUpdatedOn() {
        return this.updatedOn;
    }
    
    public void setPublished(final Published published) {
        this.published = published;
    }
    
    public void setReport(final String report) {
        this.report = report;
    }
    
    public void setReportVersion(final int reportVersion) {
        this.reportVersion = reportVersion;
    }
    
    public void setRollNo(final int rollNo) {
        this.rollNo = rollNo;
    }
    
    public void setUpdatedOn(final Long updatedOn) {
        this.updatedOn = updatedOn;
    }
}

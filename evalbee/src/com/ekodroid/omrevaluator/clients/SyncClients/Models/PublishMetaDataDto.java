// 
// Decompiled by Procyon v0.6.0
// 

package com.ekodroid.omrevaluator.clients.SyncClients.Models;

import java.io.Serializable;

public class PublishMetaDataDto implements Serializable
{
    private boolean ccEmail;
    private String emailId;
    private boolean includeRank;
    private String publishByEmail;
    private String publishByName;
    private int rollNo;
    private String studentName;
    
    public PublishMetaDataDto(final String studentName, final String emailId, final int rollNo, final String publishByEmail, final String publishByName, final boolean ccEmail, final boolean includeRank) {
        this.studentName = studentName;
        this.emailId = emailId;
        this.rollNo = rollNo;
        this.publishByEmail = publishByEmail;
        this.publishByName = publishByName;
        this.ccEmail = ccEmail;
        this.includeRank = includeRank;
    }
}

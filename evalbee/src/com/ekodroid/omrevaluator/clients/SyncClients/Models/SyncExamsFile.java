// 
// Decompiled by Procyon v0.6.0
// 

package com.ekodroid.omrevaluator.clients.SyncClients.Models;

import java.util.ArrayList;
import java.util.Set;
import java.io.Serializable;

public class SyncExamsFile implements Serializable
{
    Set<Long> examIds;
    ArrayList<ExamPartialResponse> exams;
    Long lastSyncedOn;
    
    public Set<Long> getExamIds() {
        return this.examIds;
    }
    
    public ArrayList<ExamPartialResponse> getExams() {
        return this.exams;
    }
    
    public Long getLastSyncedOn() {
        return this.lastSyncedOn;
    }
    
    public void setExamIds(final Set<Long> examIds) {
        this.examIds = examIds;
    }
    
    public void setExams(final ArrayList<ExamPartialResponse> exams) {
        this.exams = exams;
    }
    
    public void setLastSyncedOn(final Long lastSyncedOn) {
        this.lastSyncedOn = lastSyncedOn;
    }
}

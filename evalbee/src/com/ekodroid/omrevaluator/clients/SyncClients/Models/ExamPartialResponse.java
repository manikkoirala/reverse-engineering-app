// 
// Decompiled by Procyon v0.6.0
// 

package com.ekodroid.omrevaluator.clients.SyncClients.Models;

import com.ekodroid.omrevaluator.serializable.SharedType;
import com.ekodroid.omrevaluator.templateui.models.ExamId;
import java.io.Serializable;

public class ExamPartialResponse implements Serializable
{
    ExamId examId;
    Long id;
    Published published;
    SharedType sharedType;
    boolean syncImages;
    String template;
    int templateVersion;
    Long updatedOn;
    String userId;
    
    public ExamPartialResponse(final Long id, final Long updatedOn, final ExamId examId, final String template, final int templateVersion, final Published published, final SharedType sharedType, final String userId, final boolean syncImages) {
        this.id = id;
        this.updatedOn = updatedOn;
        this.examId = examId;
        this.template = template;
        this.templateVersion = templateVersion;
        this.published = published;
        this.sharedType = sharedType;
        this.userId = userId;
        this.syncImages = syncImages;
    }
    
    public ExamId getExamId() {
        return this.examId;
    }
    
    public Long getId() {
        return this.id;
    }
    
    public Published getPublished() {
        return this.published;
    }
    
    public SharedType getSharedType() {
        return this.sharedType;
    }
    
    public String getTemplate() {
        return this.template;
    }
    
    public int getTemplateVersion() {
        return this.templateVersion;
    }
    
    public Long getUpdatedOn() {
        return this.updatedOn;
    }
    
    public String getUserId() {
        return this.userId;
    }
    
    public boolean isSyncImages() {
        return this.syncImages;
    }
    
    public void setId(final Long id) {
        this.id = id;
    }
    
    public void setPublished(final Published published) {
        this.published = published;
    }
    
    public void setSyncImages(final boolean syncImages) {
        this.syncImages = syncImages;
    }
    
    public void setTemplate(final String template) {
        this.template = template;
    }
    
    public void setTemplateVersion(final int templateVersion) {
        this.templateVersion = templateVersion;
    }
    
    public void setUpdatedOn(final Long updatedOn) {
        this.updatedOn = updatedOn;
    }
}

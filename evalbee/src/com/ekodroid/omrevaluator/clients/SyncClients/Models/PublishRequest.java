// 
// Decompiled by Procyon v0.6.0
// 

package com.ekodroid.omrevaluator.clients.SyncClients.Models;

import java.util.ArrayList;
import java.io.Serializable;

public class PublishRequest implements Serializable
{
    public Long cloudId;
    public ExamPartialResponse examData;
    public Long expiresIn;
    public ArrayList<PublishMetaDataDto> publishMetaDataDtos;
    public ArrayList<StudentResultPartialResponse> studentResults;
}

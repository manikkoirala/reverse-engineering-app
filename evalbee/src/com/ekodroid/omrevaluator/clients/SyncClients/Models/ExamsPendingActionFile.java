// 
// Decompiled by Procyon v0.6.0
// 

package com.ekodroid.omrevaluator.clients.SyncClients.Models;

import java.util.ArrayList;
import java.io.Serializable;

public class ExamsPendingActionFile implements Serializable
{
    ArrayList<ExamAction> examPendingActions;
    
    public ExamsPendingActionFile() {
        this.examPendingActions = new ArrayList<ExamAction>();
    }
    
    public ArrayList<ExamAction> getExamPendingActions() {
        return this.examPendingActions;
    }
    
    public void setExamPendingActions(final ArrayList<ExamAction> examPendingActions) {
        this.examPendingActions = examPendingActions;
    }
}

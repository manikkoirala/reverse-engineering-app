// 
// Decompiled by Procyon v0.6.0
// 

package com.ekodroid.omrevaluator.clients.SyncClients.Models;

public enum Published
{
    private static final Published[] $VALUES;
    
    NOT_PUBLISHED, 
    PUBLISHED, 
    PUBLISHING;
    
    private static /* synthetic */ Published[] $values() {
        return new Published[] { Published.PUBLISHED, Published.PUBLISHING, Published.NOT_PUBLISHED };
    }
    
    static {
        $VALUES = $values();
    }
}

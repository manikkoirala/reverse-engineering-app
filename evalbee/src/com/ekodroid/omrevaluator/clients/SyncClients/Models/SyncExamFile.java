// 
// Decompiled by Procyon v0.6.0
// 

package com.ekodroid.omrevaluator.clients.SyncClients.Models;

import java.util.ArrayList;
import java.util.Set;
import java.io.Serializable;

public class SyncExamFile implements Serializable
{
    ExamPartialResponse exam;
    Long examId;
    Set<Integer> rollSet;
    ArrayList<StudentResultPartialResponse> studentResults;
    Long syncedOn;
    
    public ExamPartialResponse getExam() {
        return this.exam;
    }
    
    public Long getExamId() {
        return this.examId;
    }
    
    public Set<Integer> getRollSet() {
        return this.rollSet;
    }
    
    public ArrayList<StudentResultPartialResponse> getStudentResults() {
        return this.studentResults;
    }
    
    public Long getSyncedOn() {
        return this.syncedOn;
    }
    
    public void setExam(final ExamPartialResponse exam) {
        this.exam = exam;
    }
    
    public void setExamId(final Long examId) {
        this.examId = examId;
    }
    
    public void setRollSet(final Set<Integer> rollSet) {
        this.rollSet = rollSet;
    }
    
    public void setStudentResults(final ArrayList<StudentResultPartialResponse> studentResults) {
        this.studentResults = studentResults;
    }
    
    public void setSyncedOn(final Long syncedOn) {
        this.syncedOn = syncedOn;
    }
}

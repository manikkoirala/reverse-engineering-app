// 
// Decompiled by Procyon v0.6.0
// 

package com.ekodroid.omrevaluator.clients.SyncClients.Models;

import java.util.ArrayList;
import java.io.Serializable;

public class PublishResponse implements Serializable
{
    public Long cloudId;
    public ArrayList<Integer> rollSet;
    
    public PublishResponse() {
        this.rollSet = new ArrayList<Integer>();
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.ekodroid.omrevaluator.clients.SyncClients.Models;

import com.ekodroid.omrevaluator.more.models.Teacher;
import java.util.ArrayList;
import com.ekodroid.omrevaluator.serializable.SharedType;
import java.io.Serializable;

public class SharedExamPartial implements Serializable
{
    Long examId;
    SharedType sharedType;
    boolean syncImages;
    ArrayList<Teacher> teachers;
    
    public Long getExamId() {
        return this.examId;
    }
    
    public SharedType getSharedType() {
        return this.sharedType;
    }
    
    public ArrayList<Teacher> getTeachers() {
        return this.teachers;
    }
    
    public boolean isSyncImages() {
        return this.syncImages;
    }
    
    public void setExamId(final Long examId) {
        this.examId = examId;
    }
    
    public void setSharedType(final SharedType sharedType) {
        this.sharedType = sharedType;
    }
    
    public void setSyncImages(final boolean syncImages) {
        this.syncImages = syncImages;
    }
    
    public void setTeachers(final ArrayList<Teacher> teachers) {
        this.teachers = teachers;
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.ekodroid.omrevaluator.clients.SyncClients.Models;

import java.util.ArrayList;
import java.io.Serializable;

public class ReportPendingActionFile implements Serializable
{
    ArrayList<ReportAction> reportPendingActions;
    
    public ReportPendingActionFile() {
        this.reportPendingActions = new ArrayList<ReportAction>();
    }
    
    public ArrayList<ReportAction> getReportPendingActions() {
        return this.reportPendingActions;
    }
    
    public void setReportPendingActions(final ArrayList<ReportAction> reportPendingActions) {
        this.reportPendingActions = reportPendingActions;
    }
}

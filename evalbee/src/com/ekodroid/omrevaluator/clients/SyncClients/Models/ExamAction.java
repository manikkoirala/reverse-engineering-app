// 
// Decompiled by Procyon v0.6.0
// 

package com.ekodroid.omrevaluator.clients.SyncClients.Models;

import java.io.Serializable;

public class ExamAction implements Serializable
{
    boolean archive;
    boolean delete;
    Long id;
    
    public ExamAction(final Long id, final boolean archive, final boolean delete) {
        this.id = id;
        this.archive = archive;
        this.delete = delete;
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.ekodroid.omrevaluator.clients.SyncClients.Models;

import com.ekodroid.omrevaluator.more.models.Teacher;
import java.util.ArrayList;
import com.ekodroid.omrevaluator.serializable.SharedType;
import com.ekodroid.omrevaluator.templateui.models.ExamId;
import java.io.Serializable;

public class ExamFile implements Serializable
{
    ExamId examId;
    Long expiresIn;
    SharedType sharedType;
    String sheetTemplate;
    boolean syncImages;
    ArrayList<Teacher> teachers;
    int templateVersion;
    
    public ExamFile(final ExamId examId, final int templateVersion, final String sheetTemplate, final Long expiresIn, final SharedType sharedType, final ArrayList<Teacher> teachers, final boolean syncImages) {
        this.examId = examId;
        this.templateVersion = templateVersion;
        this.sheetTemplate = sheetTemplate;
        this.expiresIn = expiresIn;
        this.sharedType = sharedType;
        this.teachers = teachers;
        this.syncImages = syncImages;
    }
}

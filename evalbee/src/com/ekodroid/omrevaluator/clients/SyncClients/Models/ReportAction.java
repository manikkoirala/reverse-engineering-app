// 
// Decompiled by Procyon v0.6.0
// 

package com.ekodroid.omrevaluator.clients.SyncClients.Models;

import java.io.Serializable;

public class ReportAction implements Serializable
{
    boolean delete;
    Long id;
    int rollNo;
    
    public ReportAction(final Long id, final int rollNo, final boolean delete) {
        this.id = id;
        this.rollNo = rollNo;
        this.delete = delete;
    }
}

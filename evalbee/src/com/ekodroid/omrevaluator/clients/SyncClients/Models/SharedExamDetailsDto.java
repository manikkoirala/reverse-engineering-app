// 
// Decompiled by Procyon v0.6.0
// 

package com.ekodroid.omrevaluator.clients.SyncClients.Models;

import java.util.ArrayList;
import com.ekodroid.omrevaluator.serializable.SharedType;
import java.io.Serializable;

public class SharedExamDetailsDto implements Serializable
{
    public long examId;
    public String ownerUid;
    public SharedType sharedType;
    public boolean syncImages;
    public ArrayList<String> teachersUid;
}

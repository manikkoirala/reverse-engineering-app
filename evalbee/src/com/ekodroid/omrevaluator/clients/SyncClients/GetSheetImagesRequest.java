// 
// Decompiled by Procyon v0.6.0
// 

package com.ekodroid.omrevaluator.clients.SyncClients;

import com.google.android.gms.tasks.Task;
import com.google.android.gms.tasks.OnCompleteListener;
import com.android.volley.Request;
import java.util.HashMap;
import com.google.firebase.auth.FirebaseAuth;
import java.util.Map;
import com.android.volley.VolleyError;
import com.ekodroid.omrevaluator.serializable.ArchivedResult.SheetImageDto;
import java.util.ArrayList;
import com.google.gson.reflect.TypeToken;
import com.android.volley.d;
import android.content.Context;

public class GetSheetImagesRequest
{
    public ee1 a;
    public zg b;
    public String c;
    
    public GetSheetImagesRequest(final Context context, final long lng, final int i, final zg b) {
        this.b = b;
        this.a = new n52(context, a91.v()).b();
        final StringBuilder sb = new StringBuilder();
        sb.append("http://");
        sb.append(a91.v());
        sb.append(":");
        sb.append(a91.k());
        sb.append("/api/sync/exams/");
        sb.append(lng);
        sb.append("/sheet-images/");
        sb.append(i);
        this.c = sb.toString();
        this.e();
    }
    
    public final void c(final String s) {
        final hr1 hr1 = new hr1(this, 0, this.c, new d.b(this) {
            public final GetSheetImagesRequest a;
            
            public void b(final String s) {
                try {
                    this.a.d(true, 200, new gc0().k(s, new TypeToken<ArrayList<SheetImageDto>>(this) {
                        public final GetSheetImagesRequest$2 a;
                    }.getType()));
                }
                catch (final Exception ex) {
                    ex.printStackTrace();
                    this.a.d(false, 400, null);
                }
            }
        }, new d.a(this) {
            public final GetSheetImagesRequest a;
            
            @Override
            public void a(final VolleyError volleyError) {
                final yy0 networkResponse = volleyError.networkResponse;
                int a;
                if (networkResponse != null) {
                    a = networkResponse.a;
                }
                else {
                    a = 400;
                }
                this.a.d(false, a, volleyError.getMessage());
            }
        }, s) {
            public final String w;
            public final GetSheetImagesRequest x;
            
            @Override
            public byte[] k() {
                return null;
            }
            
            @Override
            public String l() {
                return "application/json";
            }
            
            @Override
            public Map o() {
                final String o = FirebaseAuth.getInstance().e().O();
                final String email = FirebaseAuth.getInstance().e().getEmail();
                final HashMap hashMap = new HashMap();
                hashMap.put("x-user-id", o);
                hashMap.put("x-user-email", email);
                hashMap.put("x-user-token", this.w);
                return hashMap;
            }
        };
        hr1.L(new wq(20000, 0, 1.0f));
        this.a.a(hr1);
    }
    
    public final void d(final boolean b, final int n, final Object o) {
        final zg b2 = this.b;
        if (b2 != null) {
            b2.a(b, n, o);
        }
    }
    
    public final void e() {
        FirebaseAuth.getInstance().e().i(false).addOnCompleteListener((OnCompleteListener)new OnCompleteListener(this) {
            public final GetSheetImagesRequest a;
            
            public void onComplete(final Task task) {
                if (task.isSuccessful()) {
                    this.a.c(((ya0)task.getResult()).c());
                }
                else {
                    this.a.d(false, 400, null);
                }
            }
        });
    }
}

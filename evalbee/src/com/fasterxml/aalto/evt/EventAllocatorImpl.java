// 
// Decompiled by Procyon v0.6.0
// 

package com.fasterxml.aalto.evt;

import javax.xml.stream.util.XMLEventAllocator;
import javax.xml.stream.events.XMLEvent;
import javax.xml.stream.XMLStreamReader;
import javax.xml.stream.Location;
import org.codehaus.stax2.ri.evt.Stax2EventAllocatorImpl;

public final class EventAllocatorImpl extends Stax2EventAllocatorImpl
{
    static final EventAllocatorImpl sStdInstance;
    protected final boolean _cfgPreserveLocation;
    protected Location _lastLocation;
    
    static {
        sStdInstance = new EventAllocatorImpl(true);
    }
    
    public EventAllocatorImpl(final boolean cfgPreserveLocation) {
        this._lastLocation = null;
        this._cfgPreserveLocation = cfgPreserveLocation;
    }
    
    public static EventAllocatorImpl getDefaultInstance() {
        return EventAllocatorImpl.sStdInstance;
    }
    
    public static EventAllocatorImpl getFastInstance() {
        return new EventAllocatorImpl(false);
    }
    
    public XMLEvent allocate(final XMLStreamReader xmlStreamReader) {
        if (xmlStreamReader.getEventType() == 257) {
            return (XMLEvent)IncompleteEvent.instance();
        }
        return super.allocate(xmlStreamReader);
    }
    
    public Location getLocation(final XMLStreamReader xmlStreamReader) {
        if (this._cfgPreserveLocation) {
            return xmlStreamReader.getLocation();
        }
        Location lastLocation;
        if ((lastLocation = this._lastLocation) == null) {
            lastLocation = xmlStreamReader.getLocation();
            this._lastLocation = lastLocation;
        }
        return lastLocation;
    }
    
    public XMLEventAllocator newInstance() {
        return (XMLEventAllocator)new EventAllocatorImpl(this._cfgPreserveLocation);
    }
}

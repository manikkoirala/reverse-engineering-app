// 
// Decompiled by Procyon v0.6.0
// 

package com.fasterxml.aalto.evt;

import com.fasterxml.aalto.impl.StreamExceptionBase;
import javax.xml.stream.Location;
import org.codehaus.stax2.XMLStreamReader2;
import javax.xml.stream.util.XMLEventAllocator;
import org.codehaus.stax2.ri.Stax2EventReaderImpl;

public final class EventReaderImpl extends Stax2EventReaderImpl
{
    public EventReaderImpl(final XMLEventAllocator xmlEventAllocator, final XMLStreamReader2 xmlStreamReader2) {
        super(xmlEventAllocator, xmlStreamReader2);
    }
    
    public String getErrorDesc(final int n, final int n2) {
        return null;
    }
    
    public boolean isPropertySupported(final String s) {
        return ((XMLStreamReader2)this.getStreamReader()).isPropertySupported(s);
    }
    
    public void reportProblem(final String s, final Location location) {
        throw new StreamExceptionBase(s, location);
    }
    
    public boolean setProperty(final String s, final Object o) {
        return ((XMLStreamReader2)this.getStreamReader()).setProperty(s, o);
    }
}

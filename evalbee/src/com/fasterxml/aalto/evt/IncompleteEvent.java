// 
// Decompiled by Procyon v0.6.0
// 

package com.fasterxml.aalto.evt;

import org.codehaus.stax2.XMLStreamWriter2;
import java.io.Writer;
import javax.xml.stream.Location;
import com.fasterxml.aalto.impl.LocationImpl;
import org.codehaus.stax2.ri.evt.BaseEventImpl;

public class IncompleteEvent extends BaseEventImpl
{
    private static final IncompleteEvent INSTANCE;
    
    static {
        INSTANCE = new IncompleteEvent();
    }
    
    public IncompleteEvent() {
        super((Location)LocationImpl.getEmptyLocation());
    }
    
    public static IncompleteEvent instance() {
        return IncompleteEvent.INSTANCE;
    }
    
    public boolean equals(final Object o) {
        return o == this;
    }
    
    public int getEventType() {
        return 257;
    }
    
    public int hashCode() {
        return 42;
    }
    
    public void writeAsEncodedUnicode(final Writer writer) {
    }
    
    public void writeUsing(final XMLStreamWriter2 xmlStreamWriter2) {
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.fasterxml.aalto;

import javax.xml.stream.XMLStreamException;
import javax.xml.stream.Location;
import org.codehaus.stax2.validation.XMLValidationProblem;
import org.codehaus.stax2.validation.XMLValidationException;

public class ValidationException extends XMLValidationException
{
    public ValidationException(final XMLValidationProblem xmlValidationProblem, final String s) {
        super(xmlValidationProblem, s);
    }
    
    public ValidationException(final XMLValidationProblem xmlValidationProblem, final String s, final Location location) {
        super(xmlValidationProblem, s, location);
    }
    
    public static ValidationException create(final XMLValidationProblem xmlValidationProblem) {
        final Location location = xmlValidationProblem.getLocation();
        if (location == null) {
            return new ValidationException(xmlValidationProblem, xmlValidationProblem.getMessage());
        }
        return new ValidationException(xmlValidationProblem, xmlValidationProblem.getMessage(), location);
    }
    
    public String getLocationDesc() {
        final Location location = ((XMLStreamException)this).getLocation();
        String string;
        if (location == null) {
            string = null;
        }
        else {
            string = location.toString();
        }
        return string;
    }
    
    public String getMessage() {
        final String locationDesc = this.getLocationDesc();
        if (locationDesc == null) {
            return super.getMessage();
        }
        final String message = this.getValidationProblem().getMessage();
        final StringBuilder sb = new StringBuilder(message.length() + locationDesc.length() + 20);
        sb.append(message);
        sb.append('\n');
        sb.append(" at ");
        sb.append(locationDesc);
        return sb.toString();
    }
    
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append(this.getClass().getName());
        sb.append(": ");
        sb.append(this.getMessage());
        return sb.toString();
    }
}

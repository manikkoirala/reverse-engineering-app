// 
// Decompiled by Procyon v0.6.0
// 

package com.fasterxml.aalto.out;

import org.codehaus.stax2.ri.typed.AsciiValueEncoder;
import javax.xml.stream.XMLStreamException;
import java.io.IOException;
import com.fasterxml.aalto.impl.IoStreamException;
import com.fasterxml.aalto.util.XmlChars;
import java.text.MessageFormat;
import com.fasterxml.aalto.impl.ErrorConsts;
import com.fasterxml.aalto.util.CharsetNames;

public abstract class XmlWriter extends WNameFactory
{
    protected static final int ATTR_MIN_ARRAYCOPY = 12;
    protected static final int DEFAULT_COPYBUFFER_LEN = 512;
    protected static final int MIN_ARRAYCOPY = 12;
    protected static final int SURR1_FIRST = 55296;
    protected static final int SURR1_LAST = 56319;
    protected static final int SURR2_FIRST = 56320;
    protected static final int SURR2_LAST = 57343;
    protected final boolean _cfgNsAware;
    protected final boolean _checkContent;
    protected final boolean _checkNames;
    protected final WriterConfig _config;
    protected char[] _copyBuffer;
    protected final int _copyBufferLen;
    protected int _locPastChars;
    protected int _locRowNr;
    protected int _locRowStartOffset;
    protected boolean _xml11;
    
    public XmlWriter(final WriterConfig config) {
        this._xml11 = false;
        this._locPastChars = 0;
        this._locRowNr = 1;
        this._locRowStartOffset = 0;
        this._config = config;
        final char[] allocMediumCBuffer = config.allocMediumCBuffer(512);
        this._copyBuffer = allocMediumCBuffer;
        this._copyBufferLen = allocMediumCBuffer.length;
        this._cfgNsAware = config.isNamespaceAware();
        this._checkContent = config.willCheckContent();
        this._checkNames = config.willCheckNames();
    }
    
    public static final int guessEncodingBitSize(final WriterConfig writerConfig) {
        final String preferredEncoding = writerConfig.getPreferredEncoding();
        if (preferredEncoding != null) {
            if (preferredEncoding.length() != 0) {
                final String normalize = CharsetNames.normalize(preferredEncoding);
                if (normalize == "UTF-8") {
                    return 16;
                }
                if (normalize == "ISO-8859-1") {
                    return 8;
                }
                if (normalize == "US-ASCII") {
                    return 7;
                }
                if (normalize != "UTF-16" && normalize != "UTF-16BE" && normalize != "UTF-16LE" && normalize != "UTF-32BE") {
                    if (normalize != "UTF-32LE") {
                        return 8;
                    }
                }
            }
        }
        return 16;
    }
    
    public abstract void _closeTarget(final boolean p0);
    
    public void _releaseBuffers() {
        final char[] copyBuffer = this._copyBuffer;
        if (copyBuffer != null) {
            this._copyBuffer = null;
            this._config.freeMediumCBuffer(copyBuffer);
        }
    }
    
    public final void close(final boolean b) {
        this.flush();
        this._releaseBuffers();
        this._closeTarget(b || this._config.willAutoCloseOutput());
    }
    
    @Override
    public abstract WName constructName(final String p0);
    
    @Override
    public abstract WName constructName(final String p0, final String p1);
    
    public void enableXml11() {
        this._xml11 = true;
    }
    
    public abstract void flush();
    
    public int getAbsOffset() {
        return this._locPastChars + this.getOutputPtr();
    }
    
    public int getColumn() {
        return this.getOutputPtr() - this._locRowStartOffset + 1;
    }
    
    public abstract int getHighestEncodable();
    
    public abstract int getOutputPtr();
    
    public int getRow() {
        return this._locRowNr;
    }
    
    public void reportFailedEscaping(final String s, final int i) {
        if (i == 65534 || i == 65535 || (i >= 55296 && i <= 57343)) {
            this.reportInvalidChar(i);
        }
        if (i < 32 && (i == 0 || !this._config.isXml11())) {
            this.reportInvalidChar(i);
        }
        this.reportNwfContent(MessageFormat.format(ErrorConsts.WERR_NO_ESCAPING, s, i));
    }
    
    public void reportInvalidChar(final int n) {
        try {
            this.flush();
            if (n == 0) {
                this.reportNwfContent("Invalid null character in text to output");
            }
            if (n < 32 || (n >= 127 && n <= 159)) {
                final StringBuilder sb = new StringBuilder();
                sb.append("Invalid white space character (0x");
                sb.append(Integer.toHexString(n));
                sb.append(") in text to output");
                String str = sb.toString();
                if (this._xml11) {
                    final StringBuilder sb2 = new StringBuilder();
                    sb2.append(str);
                    sb2.append(" (can only be output using character entity)");
                    str = sb2.toString();
                }
                this.reportNwfContent(str);
            }
            if (n > 1114111) {
                final StringBuilder sb3 = new StringBuilder();
                sb3.append("Illegal unicode character point (0x");
                sb3.append(Integer.toHexString(n));
                sb3.append(") to output; max is 0x10FFFF as per RFC 3629");
                this.reportNwfContent(sb3.toString());
            }
            if (n >= 55296 && n <= 57343) {
                this.reportNwfContent("Illegal surrogate pair -- can only be output via character entities (for current encoding), which are not allowed in this content");
            }
            final StringBuilder sb4 = new StringBuilder();
            sb4.append("Invalid XML character ");
            sb4.append(XmlChars.getCharDesc(n));
            sb4.append(" in text to output");
            this.reportNwfContent(sb4.toString());
        }
        catch (final IOException ex) {
            throw new IoStreamException(ex);
        }
    }
    
    public void reportInvalidEmptyName() {
        this.reportNwfContent("Empty String is not a valid name (local name, prefix or processing instruction target)");
    }
    
    public void reportNwfContent(final String s) {
        this.throwOutputError(s);
    }
    
    public void reportNwfContent(final String pattern, final Object o, final Object o2) {
        this.reportNwfContent(MessageFormat.format(pattern, o, o2));
    }
    
    public void reportNwfName(final String s) {
        this.throwOutputError(s);
    }
    
    public void reportNwfName(final String s, final Object o) {
        this.throwOutputError(s, o);
    }
    
    public void throwOutputError(final String msg) {
        try {
            this.flush();
            throw new XMLStreamException(msg);
        }
        catch (final IOException ex) {
            throw new IoStreamException(ex);
        }
    }
    
    public void throwOutputError(final String pattern, final Object o) {
        this.throwOutputError(MessageFormat.format(pattern, o));
    }
    
    public abstract void writeAttribute(final WName p0, final String p1);
    
    public abstract void writeAttribute(final WName p0, final AsciiValueEncoder p1);
    
    public abstract void writeAttribute(final WName p0, final char[] p1, final int p2, final int p3);
    
    public abstract int writeCData(final String p0);
    
    public abstract int writeCData(final char[] p0, final int p1, final int p2);
    
    public abstract void writeCharacters(final String p0);
    
    public abstract void writeCharacters(final char[] p0, final int p1, final int p2);
    
    public abstract int writeComment(final String p0);
    
    public abstract void writeDTD(final WName p0, final String p1, final String p2, final String p3);
    
    public abstract void writeDTD(final String p0);
    
    public abstract void writeEndTag(final WName p0);
    
    public abstract void writeEntityReference(final WName p0);
    
    public abstract int writePI(final WName p0, final String p1);
    
    public abstract void writeRaw(final String p0, final int p1, final int p2);
    
    public abstract void writeRaw(final char[] p0, final int p1, final int p2);
    
    public abstract void writeSpace(final String p0);
    
    public abstract void writeSpace(final char[] p0, final int p1, final int p2);
    
    public abstract void writeStartTagEmptyEnd();
    
    public abstract void writeStartTagEnd();
    
    public abstract void writeStartTagStart(final WName p0);
    
    public abstract void writeTypedValue(final AsciiValueEncoder p0);
    
    public abstract void writeXmlDeclaration(final String p0, final String p1, final String p2);
}

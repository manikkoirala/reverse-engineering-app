// 
// Decompiled by Procyon v0.6.0
// 

package com.fasterxml.aalto.out;

public abstract class WNameFactory
{
    public abstract WName constructName(final String p0);
    
    public abstract WName constructName(final String p0, final String p1);
}

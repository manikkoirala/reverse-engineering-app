// 
// Decompiled by Procyon v0.6.0
// 

package com.fasterxml.aalto.out;

import com.fasterxml.aalto.util.XmlChars;
import com.fasterxml.aalto.util.XmlCharTypes;

public final class OutputCharTypes extends XmlCharTypes
{
    public static final int CT_OUTPUT_MUST_QUOTE = 4;
    public static final int CT_OUTPUT_NAME_ANY_MB2 = 6;
    public static final int CT_OUTPUT_NAME_NONFIRST_MB2 = 5;
    public static final int CT_OUTPUT_NAME_UNENCODABLE = 4;
    static final int MAIN_TABLE_SIZE = 2048;
    private static XmlCharTypes sAsciiCharTypes;
    private static XmlCharTypes sLatin1CharTypes;
    private static final XmlCharTypes sUtf8CharTypes;
    
    static {
        final XmlCharTypes xmlCharTypes = sUtf8CharTypes = new XmlCharTypes(2048);
        XmlCharTypes.fillInLatin1Chars(xmlCharTypes.TEXT_CHARS, xmlCharTypes.ATTR_CHARS, xmlCharTypes.NAME_CHARS, xmlCharTypes.DTD_CHARS, xmlCharTypes.OTHER_CHARS);
        modifyForUtf8(xmlCharTypes.TEXT_CHARS);
        modifyForUtf8(xmlCharTypes.ATTR_CHARS);
        modifyForUtf8(xmlCharTypes.DTD_CHARS);
        modifyForUtf8(xmlCharTypes.OTHER_CHARS);
        modifyUtf8Names(xmlCharTypes.NAME_CHARS);
        modifyForAttrWrite(xmlCharTypes.ATTR_CHARS);
    }
    
    public static final XmlCharTypes getAsciiCharTypes() {
        if (OutputCharTypes.sAsciiCharTypes == null) {
            final XmlCharTypes xmlCharTypes = OutputCharTypes.sAsciiCharTypes = new XmlCharTypes(2048);
            XmlCharTypes.fillInLatin1Chars(xmlCharTypes.TEXT_CHARS, xmlCharTypes.ATTR_CHARS, xmlCharTypes.NAME_CHARS, xmlCharTypes.DTD_CHARS, xmlCharTypes.OTHER_CHARS);
            modifyForAscii(OutputCharTypes.sAsciiCharTypes.TEXT_CHARS);
            modifyForAscii(OutputCharTypes.sAsciiCharTypes.ATTR_CHARS);
            modifyForAscii(OutputCharTypes.sAsciiCharTypes.DTD_CHARS);
            modifyForAscii(OutputCharTypes.sAsciiCharTypes.OTHER_CHARS);
            modifyAsciiNames(OutputCharTypes.sAsciiCharTypes.NAME_CHARS);
            modifyForAttrWrite(OutputCharTypes.sAsciiCharTypes.ATTR_CHARS);
        }
        return OutputCharTypes.sAsciiCharTypes;
    }
    
    public static final XmlCharTypes getLatin1CharTypes() {
        if (OutputCharTypes.sLatin1CharTypes == null) {
            final XmlCharTypes xmlCharTypes = OutputCharTypes.sLatin1CharTypes = new XmlCharTypes(2048);
            XmlCharTypes.fillInLatin1Chars(xmlCharTypes.TEXT_CHARS, xmlCharTypes.ATTR_CHARS, xmlCharTypes.NAME_CHARS, xmlCharTypes.DTD_CHARS, xmlCharTypes.OTHER_CHARS);
            modifyForLatin1(OutputCharTypes.sLatin1CharTypes.TEXT_CHARS);
            modifyForLatin1(OutputCharTypes.sLatin1CharTypes.ATTR_CHARS);
            modifyForLatin1(OutputCharTypes.sLatin1CharTypes.DTD_CHARS);
            modifyForLatin1(OutputCharTypes.sLatin1CharTypes.OTHER_CHARS);
            modifyForAttrWrite(OutputCharTypes.sLatin1CharTypes.ATTR_CHARS);
        }
        return OutputCharTypes.sLatin1CharTypes;
    }
    
    public static final XmlCharTypes getUtf8CharTypes() {
        return OutputCharTypes.sUtf8CharTypes;
    }
    
    private static void modifyAsciiNames(final int[] array) {
        modifyLatin1Names(array);
        for (int length = array.length, i = 128; i < length; ++i) {
            final int n = array[i];
            if (n == 2 || n == 3) {
                array[i] = 4;
            }
        }
    }
    
    private static void modifyForAscii(final int[] array) {
        requireQuotingAfter(array, 127);
    }
    
    private static void modifyForAttrWrite(final int[] array) {
        array[9] = 4;
    }
    
    private static void modifyForLatin1(final int[] array) {
        for (int i = 127; i <= 159; ++i) {
            array[i] = 4;
        }
        requireQuotingAfter(array, 255);
    }
    
    private static void modifyForUtf8(final int[] array) {
        for (int length = array.length, i = 128; i < length; ++i) {
            if (array[i] == 0) {
                array[i] = 5;
            }
        }
    }
    
    private static void modifyLatin1Names(final int[] array) {
        for (int length = array.length, i = 256; i < length; ++i) {
            final int n = array[i];
            if (n == 2 || n == 3) {
                array[i] = 4;
            }
        }
    }
    
    private static void modifyUtf8Names(final int[] array) {
        for (int length = array.length, i = 128; i < length; ++i) {
            if (XmlChars.is10NameStartChar(i)) {
                array[i] = 6;
            }
            else if (XmlChars.is10NameChar(i)) {
                array[i] = 5;
            }
            else {
                array[i] = 0;
            }
        }
    }
    
    private static void requireQuotingAfter(final int[] array, int i) {
        ++i;
        while (i < array.length) {
            if (array[i] == 0) {
                array[i] = 4;
            }
            ++i;
        }
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.fasterxml.aalto.out;

import javax.xml.namespace.QName;
import com.fasterxml.aalto.util.EmptyIterator;
import java.util.List;
import java.util.ArrayList;
import java.util.Iterator;
import javax.xml.namespace.NamespaceContext;

final class OutputElement
{
    static final byte BYTE_GT = 62;
    static final byte BYTE_LT = 60;
    static final byte BYTE_SLASH = 47;
    String _defaultNsURI;
    WName _name;
    NsBinder _nsBinder;
    OutputElement _parent;
    String _uri;
    
    private OutputElement() {
        this._parent = null;
        this._name = null;
        this._uri = null;
        this._nsBinder = null;
        this._defaultNsURI = "";
    }
    
    private OutputElement(final OutputElement parent, final WName name, final String uri, final NsBinder nsBinder) {
        this._defaultNsURI = "";
        this._parent = parent;
        this._name = name;
        this._uri = uri;
        this._nsBinder = nsBinder;
        this._defaultNsURI = parent._defaultNsURI;
    }
    
    public static OutputElement createRoot() {
        return new OutputElement();
    }
    
    private void relink(final OutputElement parent, final WName name, final String uri) {
        this._parent = parent;
        this._name = name;
        this._uri = uri;
        this._nsBinder = parent._nsBinder;
        this._defaultNsURI = parent._defaultNsURI;
    }
    
    public void addPrefix(final String s, final String s2) {
        final NsBinder nsBinder = this._nsBinder;
        Label_0050: {
            NsBinder nsBinder2;
            if (nsBinder == null) {
                nsBinder2 = NsBinder.createEmpty();
            }
            else {
                final OutputElement parent = this._parent;
                if (parent == null || parent._nsBinder != nsBinder) {
                    break Label_0050;
                }
                nsBinder2 = nsBinder.createChild();
            }
            this._nsBinder = nsBinder2;
        }
        this._nsBinder.addMapping(s, s2);
    }
    
    public void addToPool(final OutputElement parent) {
        this._parent = parent;
    }
    
    public PrefixState checkPrefixValidity(final String s, final String anObject, final NamespaceContext namespaceContext) {
        if (s.equals("xml")) {
            PrefixState prefixState;
            if (anObject.equals("http://www.w3.org/XML/1998/namespace")) {
                prefixState = PrefixState.OK;
            }
            else {
                prefixState = PrefixState.MISBOUND;
            }
            return prefixState;
        }
        final NsBinder nsBinder = this._nsBinder;
        String uriByPrefix;
        if (nsBinder == null) {
            uriByPrefix = null;
        }
        else {
            uriByPrefix = nsBinder.findUriByPrefix(s);
        }
        String namespaceURI = uriByPrefix;
        if (uriByPrefix == null) {
            namespaceURI = uriByPrefix;
            if (namespaceContext != null) {
                namespaceURI = namespaceContext.getNamespaceURI(s);
            }
        }
        if (namespaceURI == null) {
            return PrefixState.UNBOUND;
        }
        PrefixState prefixState2;
        if (namespaceURI != anObject && !namespaceURI.equals(anObject)) {
            prefixState2 = PrefixState.MISBOUND;
        }
        else {
            prefixState2 = PrefixState.OK;
        }
        return prefixState2;
    }
    
    public OutputElement createChild(final WName wName) {
        return new OutputElement(this, wName, this._defaultNsURI, this._nsBinder);
    }
    
    public OutputElement createChild(final WName wName, final String s) {
        return new OutputElement(this, wName, s, this._nsBinder);
    }
    
    public String generatePrefix(final NamespaceContext namespaceContext, final String s, final int[] array) {
        if (this._nsBinder == null) {
            this._nsBinder = NsBinder.createEmpty();
        }
        return this._nsBinder.generatePrefix(s, namespaceContext, array);
    }
    
    public String getDefaultNsURI() {
        return this._defaultNsURI;
    }
    
    public String getExplicitPrefix(String prefix, final NamespaceContext namespaceContext) {
        final NsBinder nsBinder = this._nsBinder;
        if (nsBinder != null) {
            final String prefixByUri = nsBinder.findPrefixByUri(prefix);
            if (prefixByUri != null) {
                return prefixByUri;
            }
        }
        if (namespaceContext != null) {
            prefix = namespaceContext.getPrefix(prefix);
            if (prefix != null && prefix.length() > 0) {
                return prefix;
            }
        }
        return null;
    }
    
    public String getLocalName() {
        return this._name.getLocalName();
    }
    
    public WName getName() {
        return this._name;
    }
    
    public String getNameDesc() {
        return this._name.toString();
    }
    
    public String getNamespaceURI() {
        return this._uri;
    }
    
    public String getNamespaceURI(String uriByPrefix) {
        if (uriByPrefix.length() == 0) {
            return this._defaultNsURI;
        }
        final NsBinder nsBinder = this._nsBinder;
        if (nsBinder != null) {
            uriByPrefix = nsBinder.findUriByPrefix(uriByPrefix);
            if (uriByPrefix != null) {
                return uriByPrefix;
            }
        }
        return null;
    }
    
    public String getNonNullNamespaceURI() {
        String uri;
        if ((uri = this._uri) == null) {
            uri = "";
        }
        return uri;
    }
    
    public String getNonNullPrefix() {
        String s;
        if (this._name.getPrefix() == null) {
            s = "";
        }
        else {
            s = null;
        }
        return s;
    }
    
    public OutputElement getParent() {
        return this._parent;
    }
    
    public String getPrefix(String prefixByUri) {
        if (this._defaultNsURI.equals(prefixByUri)) {
            return "";
        }
        final NsBinder nsBinder = this._nsBinder;
        if (nsBinder != null) {
            prefixByUri = nsBinder.findPrefixByUri(prefixByUri);
            if (prefixByUri != null) {
                return prefixByUri;
            }
        }
        return null;
    }
    
    public Iterator<String> getPrefixes(final String anObject, final NamespaceContext namespaceContext) {
        ArrayList list;
        if (this._defaultNsURI.equals(anObject)) {
            list = new ArrayList();
            list.add("");
        }
        else {
            list = null;
        }
        final NsBinder nsBinder = this._nsBinder;
        List<String> prefixesBoundToUri = list;
        if (nsBinder != null) {
            prefixesBoundToUri = nsBinder.getPrefixesBoundToUri(anObject, list);
        }
        List<String> list2 = prefixesBoundToUri;
        if (namespaceContext != null) {
            final Iterator<String> prefixes = namespaceContext.getPrefixes(anObject);
            while (true) {
                list2 = prefixesBoundToUri;
                if (!prefixes.hasNext()) {
                    break;
                }
                final String s = prefixes.next();
                if (s.length() == 0) {
                    continue;
                }
                List<String> list3;
                if (prefixesBoundToUri == null) {
                    list3 = new ArrayList<String>();
                }
                else {
                    list3 = prefixesBoundToUri;
                    if (prefixesBoundToUri.contains(s)) {
                        continue;
                    }
                }
                list3.add(s);
                prefixesBoundToUri = list3;
            }
        }
        if (list2 == null) {
            return (Iterator<String>)EmptyIterator.getInstance();
        }
        return list2.iterator();
    }
    
    public QName getQName() {
        return new QName(this._uri, this._name.getLocalName(), this._name.getPrefix());
    }
    
    public boolean hasEmptyDefaultNs() {
        final String defaultNsURI = this._defaultNsURI;
        return defaultNsURI == null || defaultNsURI.length() == 0;
    }
    
    public boolean hasPrefix() {
        return this._name.hasPrefix();
    }
    
    @Override
    public int hashCode() {
        return this._name.hashCode();
    }
    
    public boolean isPrefixBoundTo(final String anObject, final String s, final NamespaceContext namespaceContext) {
        if (anObject == null || anObject.length() == 0) {
            return this._defaultNsURI.equals(s);
        }
        if ("xml".equals(anObject)) {
            return s.equals("http://www.w3.org/XML/1998/namespace");
        }
        final NsBinder nsBinder = this._nsBinder;
        String uriByPrefix;
        if (nsBinder == null) {
            uriByPrefix = null;
        }
        else {
            uriByPrefix = nsBinder.findUriByPrefix(anObject);
        }
        String namespaceURI = uriByPrefix;
        if (uriByPrefix == null) {
            namespaceURI = uriByPrefix;
            if (namespaceContext != null) {
                namespaceURI = namespaceContext.getNamespaceURI(anObject);
            }
        }
        return namespaceURI != null && (namespaceURI == s || namespaceURI.equals(s));
    }
    
    public boolean isPrefixUnbound(String namespaceURI, final NamespaceContext namespaceContext) {
        final NsBinder nsBinder = this._nsBinder;
        String uriByPrefix;
        if (nsBinder == null) {
            uriByPrefix = null;
        }
        else {
            uriByPrefix = nsBinder.findUriByPrefix(namespaceURI);
        }
        if (uriByPrefix != null && uriByPrefix.length() != 0) {
            return false;
        }
        if (namespaceURI.equals("xml")) {
            return false;
        }
        if (namespaceContext != null) {
            namespaceURI = namespaceContext.getNamespaceURI(namespaceURI);
            if (namespaceURI != null && namespaceURI.length() != 0) {
                return false;
            }
        }
        return true;
    }
    
    public boolean isRoot() {
        return this._parent == null;
    }
    
    public void relink(final OutputElement parent) {
        this._parent = parent;
        this._nsBinder = parent._nsBinder;
        this._defaultNsURI = parent._defaultNsURI;
    }
    
    public OutputElement reuseAsChild(final OutputElement outputElement, final WName wName) {
        final OutputElement parent = this._parent;
        this.relink(outputElement, wName, this._defaultNsURI);
        return parent;
    }
    
    public OutputElement reuseAsChild(final OutputElement outputElement, final WName wName, final String s) {
        final OutputElement parent = this._parent;
        this.relink(outputElement, wName, s);
        return parent;
    }
    
    public void setDefaultNsURI(final String defaultNsURI) {
        this._defaultNsURI = defaultNsURI;
    }
    
    public enum PrefixState
    {
        private static final PrefixState[] $VALUES;
        
        MISBOUND, 
        OK, 
        UNBOUND;
    }
}

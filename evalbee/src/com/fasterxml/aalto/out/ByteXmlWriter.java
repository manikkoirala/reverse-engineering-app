// 
// Decompiled by Procyon v0.6.0
// 

package com.fasterxml.aalto.out;

import org.codehaus.stax2.ri.typed.AsciiValueEncoder;
import com.fasterxml.aalto.util.XmlChars;
import com.fasterxml.aalto.impl.ErrorConsts;
import java.io.IOException;
import java.io.OutputStream;
import com.fasterxml.aalto.util.XmlCharTypes;

public abstract class ByteXmlWriter extends XmlWriter
{
    static final byte[] BYTES_CDATA_END;
    static final byte[] BYTES_CDATA_START;
    static final byte[] BYTES_COMMENT_END;
    static final byte[] BYTES_COMMENT_START;
    static final byte[] BYTES_XMLDECL_ENCODING;
    static final byte[] BYTES_XMLDECL_STANDALONE;
    static final byte[] BYTES_XMLDECL_START;
    static final byte BYTE_A = 97;
    static final byte BYTE_AMP = 38;
    static final byte BYTE_APOS = 39;
    static final byte BYTE_COLON = 58;
    static final byte BYTE_EQ = 61;
    static final byte BYTE_G = 103;
    static final byte BYTE_GT = 62;
    static final byte BYTE_HASH = 35;
    static final byte BYTE_HYPHEN = 45;
    static final byte BYTE_L = 108;
    static final byte BYTE_LBRACKET = 91;
    static final byte BYTE_LT = 60;
    static final byte BYTE_M = 109;
    static final byte BYTE_O = 111;
    static final byte BYTE_P = 112;
    static final byte BYTE_Q = 113;
    static final byte BYTE_QMARK = 63;
    static final byte BYTE_QUOT = 34;
    static final byte BYTE_RBRACKET = 93;
    static final byte BYTE_S = 115;
    static final byte BYTE_SEMICOLON = 59;
    static final byte BYTE_SLASH = 47;
    static final byte BYTE_SPACE = 32;
    static final byte BYTE_T = 116;
    static final byte BYTE_U = 117;
    static final byte BYTE_X = 120;
    static final int DEFAULT_COPY_BUFFER_SIZE = 1000;
    static final int DEFAULT_FULL_BUFFER_SIZE = 4000;
    static final int SMALL_WRITE = 250;
    protected final XmlCharTypes _charTypes;
    protected OutputStream _out;
    protected byte[] _outputBuffer;
    protected final int _outputBufferLen;
    protected int _outputPtr;
    protected int _surrogate;
    
    static {
        BYTES_CDATA_START = getAscii("<![CDATA[");
        BYTES_CDATA_END = getAscii("]]>");
        BYTES_COMMENT_START = getAscii("<!--");
        BYTES_COMMENT_END = getAscii("-->");
        BYTES_XMLDECL_START = getAscii("<?xml version='");
        BYTES_XMLDECL_ENCODING = getAscii(" encoding='");
        BYTES_XMLDECL_STANDALONE = getAscii(" standalone='");
    }
    
    public ByteXmlWriter(final WriterConfig writerConfig, final OutputStream out, final XmlCharTypes charTypes) {
        super(writerConfig);
        this._surrogate = 0;
        this._out = out;
        final byte[] allocFullBBuffer = writerConfig.allocFullBBuffer(4000);
        this._outputBuffer = allocFullBBuffer;
        this._outputBufferLen = allocFullBBuffer.length;
        this._outputPtr = 0;
        this._charTypes = charTypes;
    }
    
    public static final void getAscii(final String s, final byte[] array) {
        for (int length = s.length(), i = 0; i < length; ++i) {
            array[i] = (byte)s.charAt(i);
        }
    }
    
    public static final void getAscii(final String s, final byte[] array, final int n) {
        for (int length = s.length(), i = 0; i < length; ++i) {
            array[n + i] = (byte)s.charAt(i);
        }
    }
    
    public static final byte[] getAscii(final String s) {
        final byte[] array = new byte[s.length()];
        getAscii(s, array, 0);
        return array;
    }
    
    private final void longWriteCharacters(final String s) {
        int length = s.length();
        final char[] copyBuffer = super._copyBuffer;
        int srcBegin = 0;
        while (true) {
            int length2;
            if (length < (length2 = copyBuffer.length)) {
                length2 = length;
            }
            final int srcEnd = srcBegin + length2;
            s.getChars(srcBegin, srcEnd, copyBuffer, 0);
            this.writeCharacters(copyBuffer, 0, length2);
            length -= length2;
            if (length <= 0) {
                break;
            }
            srcBegin = srcEnd;
        }
    }
    
    private final void writeAttrNameEqQ(final WName wName) {
        if (this._surrogate != 0) {
            this.throwUnpairedSurrogate();
        }
        final int serializedLength = wName.serializedLength();
        int n;
        if ((n = this._outputPtr) + serializedLength + 3 >= this._outputBufferLen) {
            this.flushBuffer();
            if (serializedLength + (n = this._outputPtr) + 3 >= this._outputBufferLen) {
                this.writeName((byte)32, wName);
                this.writeRaw((byte)61);
                this.writeRaw((byte)34);
                return;
            }
        }
        final byte[] outputBuffer = this._outputBuffer;
        final int n2 = n + 1;
        outputBuffer[n] = 32;
        final int n3 = n2 + wName.appendBytes(outputBuffer, n2);
        final int n4 = n3 + 1;
        outputBuffer[n3] = 61;
        outputBuffer[n4] = 34;
        this._outputPtr = n4 + 1;
    }
    
    private final void writeSplitCharacters(final char[] array, int i, int n) {
        final int n2 = n + i;
    Label_0005:
        while (i < n2) {
            final int[] text_CHARS = this._charTypes.TEXT_CHARS;
            do {
                n = array[i];
                Label_0047: {
                    if (n < 2048) {
                        if (text_CHARS[n] != 0) {
                            if (n != 10) {
                                break Label_0047;
                            }
                            ++super._locRowNr;
                        }
                        if (this._outputPtr >= this._outputBufferLen) {
                            this.flushBuffer();
                        }
                        this._outputBuffer[this._outputPtr++] = (byte)n;
                        n = i + 1;
                        continue;
                    }
                }
                ++i;
                if (n < 2048) {
                    final int n3 = text_CHARS[n];
                    Label_0180: {
                        while (true) {
                            Label_0157: {
                                if (n3 == 1) {
                                    this.reportInvalidChar(n);
                                    break Label_0157;
                                }
                                if (n3 == 2) {
                                    break Label_0157;
                                }
                                if (n3 != 4) {
                                    if (n3 == 5) {
                                        this.output2ByteChar(n);
                                        continue Label_0005;
                                    }
                                    switch (n3) {
                                        default: {
                                            break Label_0180;
                                        }
                                        case 11: {
                                            if (i >= n2) {
                                                break;
                                            }
                                            if (array[i] == '>') {
                                                break;
                                            }
                                            break Label_0180;
                                        }
                                        case 9:
                                        case 10: {
                                            break;
                                        }
                                    }
                                }
                                this.writeAsEntity(n);
                                continue Label_0005;
                            }
                            if (super._config.willEscapeCR()) {
                                continue;
                            }
                            break;
                        }
                        ++super._locRowNr;
                    }
                    if (this._outputPtr >= this._outputBufferLen) {
                        this.flushBuffer();
                    }
                    this._outputBuffer[this._outputPtr++] = (byte)n;
                    continue Label_0005;
                }
                i = this.outputMultiByteChar(n, array, i, n2);
                continue Label_0005;
            } while ((i = n) < n2);
            break;
        }
    }
    
    @Override
    public void _closeTarget(final boolean b) {
        final OutputStream out = this._out;
        if (out != null && b) {
            out.close();
            this._out = null;
        }
    }
    
    @Override
    public void _releaseBuffers() {
        super._releaseBuffers();
        final byte[] outputBuffer = this._outputBuffer;
        if (outputBuffer != null) {
            super._config.freeFullBBuffer(outputBuffer);
            this._outputBuffer = null;
        }
        final char[] copyBuffer = super._copyBuffer;
        if (copyBuffer != null) {
            super._config.freeFullCBuffer(copyBuffer);
            super._copyBuffer = null;
        }
    }
    
    public final int calcSurrogate(int i, final int j, final String str) {
        if (j < 56320 || j > 57343) {
            final StringBuilder sb = new StringBuilder();
            sb.append("Incomplete surrogate pair");
            sb.append(str);
            sb.append(": first char 0x");
            sb.append(Integer.toHexString(i));
            sb.append(", second 0x");
            sb.append(Integer.toHexString(j));
            this.reportNwfContent(sb.toString());
        }
        i = (i - 55296 << 10) + 65536 + (j - 56320);
        if (i > 1114111) {
            this.reportInvalidChar(i);
        }
        return i;
    }
    
    @Override
    public final WName constructName(final String s) {
        this.verifyNameComponent(s);
        return this.doConstructName(s);
    }
    
    @Override
    public WName constructName(final String s, final String s2) {
        this.verifyNameComponent(s);
        this.verifyNameComponent(s2);
        return this.doConstructName(s, s2);
    }
    
    public abstract WName doConstructName(final String p0);
    
    public abstract WName doConstructName(final String p0, final String p1);
    
    public final int fastWriteAttrValue(final char[] array, int outputMultiByteChar, int n, final byte[] array2, int outputPtr) {
        final int n2 = n + outputMultiByteChar;
    Label_0005:
        while (true) {
            n = outputPtr;
            if (outputMultiByteChar < n2) {
                final int[] attr_CHARS = this._charTypes.ATTR_CHARS;
                int i;
                do {
                    n = array[outputMultiByteChar];
                    if (n < 2048) {
                        if (attr_CHARS[n] == 0) {
                            array2[outputPtr] = (byte)n;
                            i = outputMultiByteChar + 1;
                            n = outputPtr + 1;
                            outputMultiByteChar = i;
                            outputPtr = n;
                            continue;
                        }
                    }
                    this._outputPtr = outputPtr;
                    ++outputMultiByteChar;
                    if (n < 2048) {
                        outputPtr = attr_CHARS[n];
                        if (outputPtr != 1) {
                            if (outputPtr != 5) {
                                this.writeAsEntity(n);
                            }
                            else {
                                this.output2ByteChar(n);
                            }
                        }
                        else {
                            this.reportInvalidChar(n);
                        }
                    }
                    else {
                        outputMultiByteChar = this.outputMultiByteChar(n, array, outputMultiByteChar, n2);
                    }
                    if (n2 - outputMultiByteChar >= this._outputBufferLen - this._outputPtr) {
                        this.flushBuffer();
                    }
                    outputPtr = this._outputPtr;
                    continue Label_0005;
                } while (i < n2);
                break;
            }
            break;
        }
        return n;
    }
    
    @Override
    public final void flush() {
        if (this._out != null) {
            this.flushBuffer();
            this._out.flush();
        }
    }
    
    public final void flushBuffer() {
        final int outputPtr = this._outputPtr;
        if (outputPtr > 0) {
            final OutputStream out = this._out;
            if (out != null) {
                super._locPastChars += outputPtr;
                super._locRowStartOffset -= outputPtr;
                this._outputPtr = 0;
                out.write(this._outputBuffer, 0, outputPtr);
            }
        }
    }
    
    @Override
    public final int getOutputPtr() {
        return this._outputPtr;
    }
    
    public abstract void output2ByteChar(final int p0);
    
    public abstract int outputMultiByteChar(final int p0, final char[] p1, final int p2, final int p3);
    
    public abstract int outputStrictMultiByteChar(final int p0, final char[] p1, final int p2, final int p3);
    
    public abstract void outputSurrogates(final int p0, final int p1);
    
    public final void throwUnpairedSurrogate() {
        final int surrogate = this._surrogate;
        this._surrogate = 0;
        this.throwUnpairedSurrogate(surrogate);
    }
    
    public final void throwUnpairedSurrogate(final int i) {
        this.flush();
        final StringBuilder sb = new StringBuilder();
        sb.append("Unpaired surrogate character (0x");
        sb.append(Integer.toHexString(i));
        sb.append(")");
        throw new IOException(sb.toString());
    }
    
    public void verifyNameComponent(final String s) {
        if (s == null || s.length() == 0) {
            this.reportNwfName(ErrorConsts.WERR_NAME_EMPTY);
        }
        final char char1 = s.charAt(0);
        final int length = s.length();
        final int n = 1;
        int calcSurrogate = char1;
        int n2 = n;
        if (char1 >= '\ud800') {
            calcSurrogate = char1;
            n2 = n;
            if (char1 <= '\udfff') {
                if (char1 >= '\udc00') {
                    final StringBuilder sb = new StringBuilder();
                    sb.append("Illegal surrogate pairing in name: first character (");
                    sb.append(XmlChars.getCharDesc(char1));
                    sb.append(") not valid surrogate first character");
                    this.reportNwfName(sb.toString());
                }
                if (length < 2) {
                    this.reportNwfName("Illegal surrogate pairing in name: incomplete surrogate (missing second half)");
                }
                calcSurrogate = this.calcSurrogate(char1, s.charAt(1), " in name");
                n2 = 2;
            }
        }
        if (!XmlChars.is10NameStartChar(calcSurrogate)) {
            final StringBuilder sb2 = new StringBuilder();
            sb2.append("Invalid name start character ");
            sb2.append(XmlChars.getCharDesc(calcSurrogate));
            sb2.append(" (name \"");
            sb2.append(s);
            sb2.append("\")");
            this.reportNwfName(sb2.toString());
        }
        final int highestEncodable = this.getHighestEncodable();
        int i = n2;
        if (calcSurrogate > highestEncodable) {
            final StringBuilder sb3 = new StringBuilder();
            sb3.append("Illegal name start character ");
            sb3.append(XmlChars.getCharDesc(calcSurrogate));
            sb3.append(" (name \"");
            sb3.append(s);
            sb3.append("\"): can not be expressed using effective encoding (");
            sb3.append(super._config.getActualEncoding());
            sb3.append(")");
            this.reportNwfName(sb3.toString());
            i = n2;
        }
        while (i < length) {
            int n3;
            final char c = (char)(n3 = s.charAt(i));
            int j = i;
            if (c >= '\ud800') {
                n3 = c;
                j = i;
                if (c <= '\udfff') {
                    if (c >= '\udc00') {
                        final StringBuilder sb4 = new StringBuilder();
                        sb4.append("Illegal surrogate pairing in name: character at #");
                        sb4.append(i);
                        sb4.append(" (");
                        sb4.append(XmlChars.getCharDesc(c));
                        sb4.append(") not valid surrogate first character");
                        this.reportNwfName(sb4.toString());
                    }
                    j = i + 1;
                    if (j >= length) {
                        this.reportNwfName("Illegal surrogate pairing in name: name ends with incomplete surrogate pair");
                    }
                    n3 = this.calcSurrogate(c, s.charAt(j), " in name");
                }
            }
            if (n3 > highestEncodable) {
                final StringBuilder sb5 = new StringBuilder();
                sb5.append("Illegal name character ");
                sb5.append(XmlChars.getCharDesc(n3));
                sb5.append(" (name \"");
                sb5.append(s);
                sb5.append("\", index #");
                sb5.append(j);
                sb5.append("): can not be expressed using effective encoding (");
                sb5.append(super._config.getActualEncoding());
                sb5.append(")");
                this.reportNwfName(sb5.toString());
            }
            if (!XmlChars.is10NameChar(n3)) {
                final StringBuilder sb6 = new StringBuilder();
                sb6.append("Invalid name character ");
                sb6.append(XmlChars.getCharDesc(n3));
                sb6.append(") in name (\"");
                sb6.append(s);
                sb6.append("\"), index #");
                sb6.append(j);
                this.reportNwfName(sb6.toString());
            }
            i = j + 1;
        }
    }
    
    public final void writeAsEntity(int n) {
        final byte[] outputBuffer = this._outputBuffer;
        int n2;
        if ((n2 = this._outputPtr) + 10 >= outputBuffer.length) {
            this.flushBuffer();
            n2 = this._outputPtr;
        }
        final int n3 = n2 + 1;
        outputBuffer[n2] = 38;
        if (n < 256) {
            if (n == 38) {
                n = n3 + 1;
                outputBuffer[n3] = 97;
                final int n4 = n + 1;
                outputBuffer[n] = 109;
                n = n4 + 1;
                outputBuffer[n4] = 112;
            }
            else if (n == 60) {
                final int n5 = n3 + 1;
                outputBuffer[n3] = 108;
                n = n5 + 1;
                outputBuffer[n5] = 116;
            }
            else if (n == 62) {
                final int n6 = n3 + 1;
                outputBuffer[n3] = 103;
                n = n6 + 1;
                outputBuffer[n6] = 116;
            }
            else if (n == 39) {
                n = n3 + 1;
                outputBuffer[n3] = 97;
                final int n7 = n + 1;
                outputBuffer[n] = 112;
                final int n8 = n7 + 1;
                outputBuffer[n7] = 111;
                n = n8 + 1;
                outputBuffer[n8] = 115;
            }
            else if (n == 34) {
                n = n3 + 1;
                outputBuffer[n3] = 113;
                final int n9 = n + 1;
                outputBuffer[n] = 117;
                final int n10 = n9 + 1;
                outputBuffer[n9] = 111;
                n = n10 + 1;
                outputBuffer[n10] = 116;
            }
            else {
                final int n11 = n3 + 1;
                outputBuffer[n3] = 35;
                final int n12 = n11 + 1;
                outputBuffer[n11] = 120;
                int n13 = n12;
                int n14;
                if ((n14 = n) >= 16) {
                    int n15 = n >> 4;
                    if (n15 < 10) {
                        n15 += 48;
                    }
                    else {
                        n15 += 87;
                    }
                    outputBuffer[n12] = (byte)n15;
                    n14 = (n & 0xF);
                    n13 = n12 + 1;
                }
                final int n16 = n13 + 1;
                if (n14 < 10) {
                    n = n14 + 48;
                }
                else {
                    n = n14 + 87;
                }
                outputBuffer[n13] = (byte)n;
                n = n16;
            }
        }
        else {
            final int n17 = n3 + 1;
            outputBuffer[n3] = 35;
            final int n18 = n17 + 1;
            outputBuffer[n17] = 120;
            int n19 = 20;
            int n20 = n18;
            int i;
            int n22;
            do {
                final int n21 = n >> n19 & 0xF;
                if (n21 > 0 || (n22 = n20) != n18) {
                    int n23;
                    if (n21 < 10) {
                        n23 = n21 + 48;
                    }
                    else {
                        n23 = n21 + 87;
                    }
                    outputBuffer[n20] = (byte)n23;
                    n22 = n20 + 1;
                }
                i = (n19 -= 4);
                n20 = n22;
            } while (i > 0);
            n &= 0xF;
            final int n24 = n22 + 1;
            if (n < 10) {
                n += 48;
            }
            else {
                n += 87;
            }
            outputBuffer[n22] = (byte)n;
            n = n24;
        }
        outputBuffer[n] = 59;
        this._outputPtr = n + 1;
    }
    
    public final void writeAttrValue(final char[] array, int i, int n) {
        final int surrogate = this._surrogate;
        int n2 = i;
        int n3 = n;
        if (surrogate != 0) {
            this.outputSurrogates(surrogate, array[i]);
            n2 = i + 1;
            n3 = n - 1;
        }
        final int n4 = n3 + n2;
        i = n2;
    Label_0046:
        while (i < n4) {
            final int[] attr_CHARS = this._charTypes.ATTR_CHARS;
            do {
                n = array[i];
                if (n < 2048) {
                    if (attr_CHARS[n] == 0) {
                        if (this._outputPtr >= this._outputBufferLen) {
                            this.flushBuffer();
                        }
                        this._outputBuffer[this._outputPtr++] = (byte)n;
                        n = i + 1;
                        continue;
                    }
                }
                ++i;
                if (n < 2048) {
                    final int n5 = attr_CHARS[n];
                    if (n5 != 1) {
                        if (n5 != 5) {
                            this.writeAsEntity(n);
                            continue Label_0046;
                        }
                    }
                    else {
                        this.reportInvalidChar(n);
                    }
                    this.output2ByteChar(n);
                    continue Label_0046;
                }
                i = this.outputMultiByteChar(n, array, i, n4);
                continue Label_0046;
            } while ((i = n) < n4);
            break;
        }
    }
    
    @Override
    public final void writeAttribute(final WName wName, final String s) {
        final int length = s.length();
        if (length > super._copyBufferLen) {
            this.writeLongAttribute(wName, s, length);
            return;
        }
        final char[] copyBuffer = super._copyBuffer;
        if (length > 0) {
            s.getChars(0, length, copyBuffer, 0);
        }
        this.writeAttribute(wName, copyBuffer, 0, length);
    }
    
    @Override
    public final void writeAttribute(final WName wName, final AsciiValueEncoder asciiValueEncoder) {
        this.writeAttrNameEqQ(wName);
        if (asciiValueEncoder.bufferNeedsFlush(this._outputBufferLen - this._outputPtr)) {
            this.flush();
        }
        while (true) {
            this._outputPtr = asciiValueEncoder.encodeMore(this._outputBuffer, this._outputPtr, this._outputBufferLen);
            if (asciiValueEncoder.isCompleted()) {
                break;
            }
            this.flushBuffer();
        }
        if (this._outputPtr >= this._outputBufferLen) {
            this.flushBuffer();
        }
        this._outputBuffer[this._outputPtr++] = 34;
    }
    
    @Override
    public final void writeAttribute(final WName wName, final char[] array, final int n, final int n2) {
        if (this._surrogate != 0) {
            this.throwUnpairedSurrogate();
        }
        final int outputPtr = this._outputPtr;
        final byte[] outputBuffer = this._outputBuffer;
        int outputPtr2;
        if (wName.serializedLength() + outputPtr >= this._outputBufferLen) {
            this.writeName((byte)32, wName);
            outputPtr2 = this._outputPtr;
        }
        else {
            final int n3 = outputPtr + 1;
            outputBuffer[outputPtr] = 32;
            outputPtr2 = wName.appendBytes(outputBuffer, n3) + n3;
        }
        int n5;
        if (outputPtr2 + 3 + n2 > this._outputBufferLen) {
            this._outputPtr = outputPtr2;
            this.flushBuffer();
            final int outputPtr3 = this._outputPtr;
            final int n4 = outputPtr3 + 1;
            outputBuffer[outputPtr3] = 61;
            final int outputPtr4 = n4 + 1;
            this._outputPtr = outputPtr4;
            outputBuffer[n4] = 34;
            n5 = outputPtr4;
            if (outputPtr4 + n2 + 1 > this._outputBufferLen) {
                this.writeAttrValue(array, n, n2);
                this.writeRaw((byte)34);
                return;
            }
        }
        else {
            final int n6 = outputPtr2 + 1;
            outputBuffer[outputPtr2] = 61;
            n5 = n6 + 1;
            outputBuffer[n6] = 34;
        }
        int fastWriteAttrValue = n5;
        if (n2 > 0) {
            fastWriteAttrValue = this.fastWriteAttrValue(array, n, n2, outputBuffer, n5);
        }
        outputBuffer[fastWriteAttrValue] = 34;
        this._outputPtr = fastWriteAttrValue + 1;
    }
    
    @Override
    public int writeCData(final String s) {
        this.writeCDataStart();
        int i = s.length();
        int srcBegin = 0;
        while (i > 0) {
            final char[] copyBuffer = super._copyBuffer;
            int length;
            if ((length = copyBuffer.length) > i) {
                length = i;
            }
            final int srcEnd = srcBegin + length;
            s.getChars(srcBegin, srcEnd, copyBuffer, 0);
            final int writeCDataContents = this.writeCDataContents(copyBuffer, 0, length);
            if (writeCDataContents >= 0) {
                return srcBegin + writeCDataContents;
            }
            i -= length;
            srcBegin = srcEnd;
        }
        this.writeCDataEnd();
        return -1;
    }
    
    @Override
    public int writeCData(final char[] array, int writeCDataContents, final int n) {
        this.writeCDataStart();
        writeCDataContents = this.writeCDataContents(array, writeCDataContents, n);
        if (writeCDataContents < 0) {
            this.writeCDataEnd();
        }
        return writeCDataContents;
    }
    
    public int writeCDataContents(final char[] array, int i, int n) {
        final int n2 = n + i;
    Label_0005:
        while (i < n2) {
            final int[] other_CHARS = this._charTypes.OTHER_CHARS;
            do {
                final char c = array[i];
                if (c < '\u0800') {
                    if (other_CHARS[c] == 0) {
                        if (this._outputPtr >= this._outputBufferLen) {
                            this.flushBuffer();
                        }
                        final byte[] outputBuffer = this._outputBuffer;
                        n = this._outputPtr++;
                        outputBuffer[n] = (byte)c;
                        n = i + 1;
                        continue;
                    }
                }
                n = i + 1;
                if (c < '\u0800') {
                    i = other_CHARS[c];
                    Label_0200: {
                        if (i != 1) {
                            if (i != 2 && i != 3) {
                                if (i != 4) {
                                    if (i != 5) {
                                        if (i != 11) {
                                            break Label_0200;
                                        }
                                        if (n >= n2 || array[n] != ']') {
                                            break Label_0200;
                                        }
                                        final int n3 = n + 1;
                                        i = n;
                                        if (n3 >= n2) {
                                            continue Label_0005;
                                        }
                                        i = n;
                                        if (array[n3] == '>') {
                                            i = n + 2;
                                            this.writeRaw((byte)93, (byte)93);
                                            this.writeCDataEnd();
                                            this.writeCDataStart();
                                            this.writeRaw((byte)62);
                                            continue Label_0005;
                                        }
                                        continue Label_0005;
                                    }
                                }
                                else {
                                    this.reportFailedEscaping("CDATA", c);
                                }
                                this.output2ByteChar(c);
                                i = n;
                                continue Label_0005;
                            }
                        }
                        else {
                            this.reportInvalidChar(c);
                        }
                        ++super._locRowNr;
                    }
                    if (this._outputPtr >= this._outputBufferLen) {
                        this.flushBuffer();
                    }
                    final byte[] outputBuffer2 = this._outputBuffer;
                    i = this._outputPtr++;
                    outputBuffer2[i] = (byte)c;
                    i = n;
                    continue Label_0005;
                }
                i = this.outputMultiByteChar(c, array, n, n2);
                continue Label_0005;
            } while ((i = n) < n2);
            break;
        }
        return -1;
    }
    
    public final void writeCDataEnd() {
        this.writeRaw(ByteXmlWriter.BYTES_CDATA_END);
    }
    
    public final void writeCDataStart() {
        this.writeRaw(ByteXmlWriter.BYTES_CDATA_START);
    }
    
    @Override
    public final void writeCharacters(final String s) {
        final int length = s.length();
        if (length > super._copyBufferLen) {
            this.longWriteCharacters(s);
            return;
        }
        if (length > 0) {
            final char[] copyBuffer = super._copyBuffer;
            s.getChars(0, length, copyBuffer, 0);
            this.writeCharacters(copyBuffer, 0, length);
        }
    }
    
    @Override
    public final void writeCharacters(final char[] array, int outputPtr, int outputMultiByteChar) {
        final int surrogate = this._surrogate;
        int n = outputPtr;
        int n2 = outputMultiByteChar;
        if (surrogate != 0) {
            this.outputSurrogates(surrogate, array[outputPtr]);
            n = outputPtr + 1;
            n2 = outputMultiByteChar - 1;
        }
        outputPtr = this._outputPtr;
        if (outputPtr + n2 > this._outputBufferLen) {
            this.writeSplitCharacters(array, n, n2);
            return;
        }
        final int n3 = n2 + n;
        outputMultiByteChar = n;
        int outputPtr2 = 0;
    Label_0072:
        while (true) {
            outputPtr2 = outputPtr;
            if (outputMultiByteChar < n3) {
                final int[] text_CHARS = this._charTypes.TEXT_CHARS;
                int n4;
                do {
                    final char c = array[outputMultiByteChar];
                    Label_0121: {
                        if (c < '\u0800') {
                            if (text_CHARS[c] != 0) {
                                if (c != '\n') {
                                    break Label_0121;
                                }
                                ++super._locRowNr;
                            }
                            this._outputBuffer[outputPtr] = (byte)c;
                            n4 = outputMultiByteChar + 1;
                            outputPtr2 = ++outputPtr;
                            continue;
                        }
                    }
                    final int n5 = outputMultiByteChar + 1;
                    Label_0349: {
                        if (c < '\u0800') {
                            outputMultiByteChar = n5;
                            Label_0326: {
                                Label_0239: {
                                    switch (text_CHARS[c]) {
                                        case 11: {
                                            if (n5 >= n3) {
                                                break Label_0239;
                                            }
                                            if (array[n5] == '>') {
                                                break Label_0239;
                                            }
                                            break;
                                        }
                                        case 1: {
                                            this.reportInvalidChar(c);
                                        }
                                        case 2: {
                                            if (super._config.willEscapeCR()) {
                                                break Label_0239;
                                            }
                                            final byte[] outputBuffer = this._outputBuffer;
                                            outputMultiByteChar = outputPtr + 1;
                                            outputBuffer[outputPtr] = (byte)c;
                                            ++super._locRowNr;
                                            outputPtr = outputMultiByteChar;
                                            break Label_0326;
                                        }
                                        case 3: {
                                            break Label_0349;
                                        }
                                        case 5: {
                                            this._outputPtr = outputPtr;
                                            this.output2ByteChar(c);
                                            outputMultiByteChar = n5;
                                            break Label_0349;
                                        }
                                        case 4:
                                        case 9:
                                        case 10: {
                                            this._outputPtr = outputPtr;
                                            this.writeAsEntity(c);
                                            outputMultiByteChar = n5;
                                            break Label_0349;
                                        }
                                    }
                                }
                                final byte[] outputBuffer2 = this._outputBuffer;
                                outputMultiByteChar = outputPtr + 1;
                                outputBuffer2[outputPtr] = (byte)c;
                                outputPtr = outputMultiByteChar;
                            }
                            outputMultiByteChar = n5;
                            continue Label_0072;
                        }
                        this._outputPtr = outputPtr;
                        outputMultiByteChar = this.outputMultiByteChar(c, array, n5, n3);
                    }
                    if (n3 - outputMultiByteChar >= this._outputBufferLen - this._outputPtr) {
                        this.flushBuffer();
                    }
                    outputPtr = this._outputPtr;
                    continue Label_0072;
                } while ((outputMultiByteChar = n4) < n3);
                break;
            }
            break;
        }
        this._outputPtr = outputPtr2;
    }
    
    @Override
    public int writeComment(final String s) {
        this.writeCommentStart();
        int i = s.length();
        int srcBegin = 0;
        while (i > 0) {
            final char[] copyBuffer = super._copyBuffer;
            final int length = copyBuffer.length;
            int n;
            if (i < length) {
                n = i;
            }
            else {
                n = length;
            }
            s.getChars(srcBegin, srcBegin + n, copyBuffer, 0);
            final int writeCommentContents = this.writeCommentContents(copyBuffer, 0, n);
            if (writeCommentContents >= 0) {
                return srcBegin + writeCommentContents;
            }
            srcBegin += length;
            i -= length;
        }
        this.writeCommentEnd();
        return -1;
    }
    
    public int writeCommentContents(final char[] array, int i, int n) {
        final int n2 = n + i;
    Label_0005:
        while (i < n2) {
            final int[] other_CHARS = this._charTypes.OTHER_CHARS;
            do {
                n = array[i];
                if (n < 2048) {
                    if (other_CHARS[n] == 0) {
                        if (this._outputPtr >= this._outputBufferLen) {
                            this.flushBuffer();
                        }
                        this._outputBuffer[this._outputPtr++] = (byte)n;
                        n = i + 1;
                        continue;
                    }
                }
                ++i;
                if (n < 2048) {
                    final int n3 = other_CHARS[n];
                    Label_0156: {
                        if (n3 != 1) {
                            if (n3 != 2 && n3 != 3) {
                                if (n3 != 4) {
                                    if (n3 != 5) {
                                        if (n3 != 13) {
                                            break Label_0156;
                                        }
                                        if (i < n2 && array[i] != '-') {
                                            break Label_0156;
                                        }
                                        this.writeRaw((byte)45, (byte)32);
                                        continue Label_0005;
                                    }
                                }
                                else {
                                    this.reportFailedEscaping("comment", n);
                                }
                                this.output2ByteChar(n);
                                continue Label_0005;
                            }
                        }
                        else {
                            this.reportInvalidChar(n);
                        }
                        ++super._locRowNr;
                    }
                    if (this._outputPtr >= this._outputBufferLen) {
                        this.flushBuffer();
                    }
                    this._outputBuffer[this._outputPtr++] = (byte)n;
                    continue Label_0005;
                }
                i = this.outputMultiByteChar(n, array, i, n2);
                continue Label_0005;
            } while ((i = n) < n2);
            break;
        }
        return -1;
    }
    
    public final void writeCommentEnd() {
        this.writeRaw(ByteXmlWriter.BYTES_COMMENT_END);
    }
    
    public final void writeCommentStart() {
        this.writeRaw(ByteXmlWriter.BYTES_COMMENT_START);
    }
    
    @Override
    public void writeDTD(final WName wName, final String s, final String s2, final String s3) {
    }
    
    @Override
    public void writeDTD(final String s) {
        this.writeRaw(s, 0, s.length());
    }
    
    @Override
    public final void writeEndTag(final WName wName) {
        if (this._surrogate != 0) {
            this.throwUnpairedSurrogate();
        }
        final int outputPtr = this._outputPtr;
        final int serializedLength = wName.serializedLength();
        int outputPtr2 = outputPtr;
        if (outputPtr + serializedLength + 3 > this._outputBufferLen) {
            this.flushBuffer();
            if (serializedLength + 3 > this._outputBufferLen) {
                this._out.write(60);
                this._out.write(47);
                wName.writeBytes(this._out);
                this._outputBuffer[this._outputPtr++] = 62;
                return;
            }
            outputPtr2 = this._outputPtr;
        }
        final byte[] outputBuffer = this._outputBuffer;
        final int n = outputPtr2 + 1;
        outputBuffer[outputPtr2] = 60;
        final int n2 = n + 1;
        outputBuffer[n] = 47;
        final int n3 = n2 + wName.appendBytes(outputBuffer, n2);
        outputBuffer[n3] = 62;
        this._outputPtr = n3 + 1;
    }
    
    @Override
    public void writeEntityReference(final WName wName) {
        this.writeRaw((byte)38);
        this.writeName(wName);
        this.writeRaw((byte)59);
    }
    
    public final void writeLongAttribute(final WName wName, final String s, int i) {
        this.writeRaw((byte)32);
        final int serializedLength = wName.serializedLength();
        final int outputPtr = this._outputPtr;
        if (outputPtr + serializedLength > this._outputBufferLen) {
            this.flushBuffer();
            if (serializedLength > this._outputBufferLen) {
                wName.writeBytes(this._out);
            }
            else {
                final int outputPtr2 = this._outputPtr;
                this._outputPtr = outputPtr2 + wName.appendBytes(this._outputBuffer, outputPtr2);
            }
        }
        else {
            this._outputPtr = outputPtr + wName.appendBytes(this._outputBuffer, outputPtr);
        }
        this.writeRaw((byte)61, (byte)34);
        int srcBegin = 0;
        while (i > 0) {
            final char[] copyBuffer = super._copyBuffer;
            int length;
            if (i < (length = copyBuffer.length)) {
                length = i;
            }
            final int srcEnd = srcBegin + length;
            s.getChars(srcBegin, srcEnd, copyBuffer, 0);
            this.writeAttrValue(copyBuffer, 0, length);
            i -= length;
            srcBegin = srcEnd;
        }
        this.writeRaw((byte)34);
    }
    
    public final void writeName(final byte b, final WName wName) {
        this.flushBuffer();
        if (wName.serializedLength() >= this._outputBufferLen) {
            this._out.write(b);
            wName.writeBytes(this._out);
            return;
        }
        final int outputPtr = this._outputPtr;
        final byte[] outputBuffer = this._outputBuffer;
        final int n = outputPtr + 1;
        outputBuffer[outputPtr] = b;
        this._outputPtr = n + wName.appendBytes(outputBuffer, n);
    }
    
    public final void writeName(final WName wName) {
        final int outputPtr = this._outputPtr;
        final int serializedLength = wName.serializedLength();
        int outputPtr2 = outputPtr;
        if (outputPtr + serializedLength > this._outputBufferLen) {
            this.flushBuffer();
            if (serializedLength >= this._outputBufferLen) {
                wName.writeBytes(this._out);
                return;
            }
            outputPtr2 = this._outputPtr;
        }
        this._outputPtr = outputPtr2 + wName.appendBytes(this._outputBuffer, outputPtr2);
    }
    
    public final void writeName(final WName wName, final byte b) {
        this.flushBuffer();
        if (wName.serializedLength() >= this._outputBufferLen) {
            wName.writeBytes(this._out);
            this._out.write(b);
            return;
        }
        final int outputPtr = this._outputPtr;
        final byte[] outputBuffer = this._outputBuffer;
        final int n = outputPtr + wName.appendBytes(outputBuffer, outputPtr);
        outputBuffer[n] = b;
        this._outputPtr = n + 1;
    }
    
    @Override
    public int writePI(final WName wName, final String s) {
        this.writeRaw((byte)60, (byte)63);
        this.writeName(wName);
        if (s != null) {
            this.writeRaw((byte)32);
            int i = s.length();
            int srcBegin = 0;
            while (i > 0) {
                final char[] copyBuffer = super._copyBuffer;
                int length;
                if ((length = copyBuffer.length) > i) {
                    length = i;
                }
                final int srcEnd = srcBegin + length;
                s.getChars(srcBegin, srcEnd, copyBuffer, 0);
                final int writePIData = this.writePIData(copyBuffer, 0, length);
                if (writePIData >= 0) {
                    return srcBegin + writePIData;
                }
                i -= length;
                srcBegin = srcEnd;
            }
        }
        this.writeRaw((byte)63, (byte)62);
        return -1;
    }
    
    public int writePIData(final char[] array, int i, int n) {
        final int n2 = n + i;
    Label_0005:
        while (i < n2) {
            final int[] other_CHARS = this._charTypes.OTHER_CHARS;
            do {
                n = array[i];
                if (n < 2048) {
                    if (other_CHARS[n] == 0) {
                        if (this._outputPtr >= this._outputBufferLen) {
                            this.flushBuffer();
                        }
                        this._outputBuffer[this._outputPtr++] = (byte)n;
                        n = i + 1;
                        continue;
                    }
                }
                ++i;
                if (n < 2048) {
                    final int n3 = other_CHARS[n];
                    Label_0144: {
                        if (n3 != 1) {
                            if (n3 != 2 && n3 != 3) {
                                if (n3 != 4) {
                                    if (n3 != 5) {
                                        if (n3 != 12) {
                                            break Label_0144;
                                        }
                                        if (i < n2 && array[i] == '>') {
                                            return i;
                                        }
                                        break Label_0144;
                                    }
                                }
                                else {
                                    this.reportFailedEscaping("processing instruction", n);
                                }
                                this.output2ByteChar(n);
                                continue Label_0005;
                            }
                        }
                        else {
                            this.reportInvalidChar(n);
                        }
                        ++super._locRowNr;
                    }
                    if (this._outputPtr >= this._outputBufferLen) {
                        this.flushBuffer();
                    }
                    this._outputBuffer[this._outputPtr++] = (byte)n;
                    continue Label_0005;
                }
                i = this.outputMultiByteChar(n, array, i, n2);
                continue Label_0005;
            } while ((i = n) < n2);
            break;
        }
        return -1;
    }
    
    public final void writeRaw(final byte b) {
        if (this._surrogate != 0) {
            this.throwUnpairedSurrogate();
        }
        if (this._outputPtr >= this._outputBufferLen) {
            this.flushBuffer();
        }
        this._outputBuffer[this._outputPtr++] = b;
    }
    
    public final void writeRaw(final byte b, final byte b2) {
        if (this._surrogate != 0) {
            this.throwUnpairedSurrogate();
        }
        if (this._outputPtr + 1 >= this._outputBufferLen) {
            this.flushBuffer();
        }
        final byte[] outputBuffer = this._outputBuffer;
        final int outputPtr = this._outputPtr;
        final int n = outputPtr + 1;
        outputBuffer[outputPtr] = b;
        this._outputPtr = n + 1;
        outputBuffer[n] = b2;
    }
    
    @Override
    public final void writeRaw(final String s, int srcBegin, int i) {
        while (i > 0) {
            final char[] copyBuffer = super._copyBuffer;
            int length;
            if (i < (length = copyBuffer.length)) {
                length = i;
            }
            final int srcEnd = srcBegin + length;
            s.getChars(srcBegin, srcEnd, copyBuffer, 0);
            this.writeRaw(copyBuffer, 0, length);
            i -= length;
            srcBegin = srcEnd;
        }
    }
    
    public final void writeRaw(final byte[] array) {
        this.writeRaw(array, 0, array.length);
    }
    
    public final void writeRaw(final byte[] b, final int off, final int len) {
        if (this._surrogate != 0) {
            this.throwUnpairedSurrogate();
        }
        final int outputPtr = this._outputPtr;
        if (outputPtr + len <= this._outputBufferLen) {
            System.arraycopy(b, off, this._outputBuffer, outputPtr, len);
            this._outputPtr += len;
            return;
        }
        int outputPtr2;
        if ((outputPtr2 = outputPtr) > 0) {
            this.flush();
            outputPtr2 = this._outputPtr;
        }
        if (len < 250) {
            System.arraycopy(b, off, this._outputBuffer, outputPtr2, len);
            this._outputPtr += len;
        }
        else {
            this._out.write(b, off, len);
        }
    }
    
    @Override
    public abstract void writeRaw(final char[] p0, final int p1, final int p2);
    
    @Override
    public final void writeSpace(final String s) {
        int i = s.length();
        int srcBegin = 0;
        while (i > 0) {
            final char[] copyBuffer = super._copyBuffer;
            int length;
            if (i < (length = copyBuffer.length)) {
                length = i;
            }
            final int srcEnd = srcBegin + length;
            s.getChars(srcBegin, srcEnd, copyBuffer, 0);
            this.writeSpace(copyBuffer, 0, length);
            i -= length;
            srcBegin = srcEnd;
        }
    }
    
    @Override
    public void writeSpace(final char[] array, final int n, final int n2) {
        if (this._out == null) {
            return;
        }
        final int surrogate = this._surrogate;
        if (surrogate != 0) {
            this.reportNwfContent(ErrorConsts.WERR_SPACE_CONTENT, surrogate, n - 1);
        }
        int n3 = n;
        while (true) {
            final int n4 = n3;
            if (n4 >= n2 + n) {
                break;
            }
            n3 = n4 + 1;
            final char i = array[n4];
            if (i > ' ' && (!super._config.isXml11() || (i != '\u0085' && i != '\u2028'))) {
                this.reportNwfContent(ErrorConsts.WERR_SPACE_CONTENT, (int)i, n3 - 1);
            }
            if (this._outputPtr >= this._outputBufferLen) {
                this.flushBuffer();
            }
            this._outputBuffer[this._outputPtr++] = (byte)i;
        }
    }
    
    @Override
    public void writeStartTagEmptyEnd() {
        int n;
        if ((n = this._outputPtr) + 2 > this._outputBufferLen) {
            this.flushBuffer();
            n = this._outputPtr;
        }
        final byte[] outputBuffer = this._outputBuffer;
        final int n2 = n + 1;
        outputBuffer[n] = 47;
        outputBuffer[n2] = 62;
        this._outputPtr = n2 + 1;
    }
    
    @Override
    public final void writeStartTagEnd() {
        if (this._surrogate != 0) {
            this.throwUnpairedSurrogate();
        }
        if (this._outputPtr >= this._outputBufferLen) {
            this.flushBuffer();
        }
        this._outputBuffer[this._outputPtr++] = 62;
    }
    
    @Override
    public final void writeStartTagStart(final WName wName) {
        if (this._surrogate != 0) {
            this.throwUnpairedSurrogate();
        }
        final int outputPtr = this._outputPtr;
        if (wName.serializedLength() + outputPtr + 1 > this._outputBufferLen) {
            this.writeName((byte)60, wName);
            return;
        }
        final byte[] outputBuffer = this._outputBuffer;
        final int n = outputPtr + 1;
        outputBuffer[outputPtr] = 60;
        this._outputPtr = n + wName.appendBytes(outputBuffer, n);
    }
    
    @Override
    public void writeTypedValue(final AsciiValueEncoder asciiValueEncoder) {
        if (this._surrogate != 0) {
            this.throwUnpairedSurrogate();
        }
        if (asciiValueEncoder.bufferNeedsFlush(this._outputBufferLen - this._outputPtr)) {
            this.flush();
        }
        while (true) {
            this._outputPtr = asciiValueEncoder.encodeMore(this._outputBuffer, this._outputPtr, this._outputBufferLen);
            if (asciiValueEncoder.isCompleted()) {
                break;
            }
            this.flushBuffer();
        }
    }
    
    @Override
    public void writeXmlDeclaration(final String s, final String s2, final String s3) {
        this.writeRaw(ByteXmlWriter.BYTES_XMLDECL_START);
        this.writeRaw(s, 0, s.length());
        this.writeRaw((byte)39);
        if (s2 != null && s2.length() > 0) {
            this.writeRaw(ByteXmlWriter.BYTES_XMLDECL_ENCODING);
            this.writeRaw(s2, 0, s2.length());
            this.writeRaw((byte)39);
        }
        if (s3 != null) {
            this.writeRaw(ByteXmlWriter.BYTES_XMLDECL_STANDALONE);
            this.writeRaw(s3, 0, s3.length());
            this.writeRaw((byte)39);
        }
        this.writeRaw((byte)63, (byte)62);
    }
}

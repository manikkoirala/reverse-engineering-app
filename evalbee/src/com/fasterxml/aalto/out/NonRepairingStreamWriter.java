// 
// Decompiled by Procyon v0.6.0
// 

package com.fasterxml.aalto.out;

import org.codehaus.stax2.ri.typed.AsciiValueEncoder;
import com.fasterxml.aalto.impl.ErrorConsts;
import javax.xml.namespace.QName;

public final class NonRepairingStreamWriter extends StreamWriterBase
{
    public NonRepairingStreamWriter(final WriterConfig writerConfig, final XmlWriter xmlWriter, final WNameTable wNameTable) {
        super(writerConfig, xmlWriter, wNameTable);
    }
    
    @Override
    public String _serializeQName(final QName qName) {
        final String prefix = qName.getPrefix();
        String s;
        final String str = s = qName.getLocalPart();
        if (prefix != null) {
            if (prefix.length() == 0) {
                s = str;
            }
            else {
                final StringBuilder sb = new StringBuilder();
                sb.append(prefix);
                sb.append(":");
                sb.append(str);
                s = sb.toString();
            }
        }
        return s;
    }
    
    @Override
    public void _setPrefix(final String s, final String s2) {
        super._currElem.addPrefix(s, s2);
    }
    
    @Override
    public void setDefaultNamespace(final String defaultNsURI) {
        super._currElem.setDefaultNsURI(defaultNsURI);
    }
    
    @Override
    public void writeAttribute(final String str, final String s, final String s2) {
        if (!super._stateStartElementOpen) {
            StreamWriterBase.throwOutputError(ErrorConsts.WERR_ATTR_NO_ELEM);
        }
        WName wName;
        if (str != null && str.length() != 0) {
            final String explicitPrefix = super._currElem.getExplicitPrefix(str, super._rootNsContext);
            if (explicitPrefix == null) {
                final StringBuilder sb = new StringBuilder();
                sb.append("Unbound namespace URI '");
                sb.append(str);
                sb.append("'");
                StreamWriterBase.throwOutputError(sb.toString());
            }
            wName = super._symbols.findSymbol(explicitPrefix, s);
        }
        else {
            wName = super._symbols.findSymbol(s);
        }
        this._writeAttribute(wName, s2);
    }
    
    @Override
    public void writeAttribute(final String s, final String s2, final String s3, final String s4) {
        if (!super._stateStartElementOpen) {
            StreamWriterBase.throwOutputError(ErrorConsts.WERR_ATTR_NO_ELEM);
        }
        WName wName;
        if (s != null && s.length() != 0) {
            wName = super._symbols.findSymbol(s, s3);
        }
        else {
            wName = super._symbols.findSymbol(s3);
        }
        this._writeAttribute(wName, s4);
    }
    
    @Override
    public void writeDefaultNamespace(final String defaultNamespace) {
        if (!super._stateStartElementOpen) {
            StreamWriterBase.throwOutputError(ErrorConsts.WERR_NS_NO_ELEM);
        }
        this._writeDefaultNamespace(defaultNamespace);
        this.setDefaultNamespace(defaultNamespace);
    }
    
    @Override
    public void writeEmptyElement(final String str, final String s) {
        String prefix = super._currElem.getPrefix(str);
        if (prefix == null) {
            final StringBuilder sb = new StringBuilder();
            sb.append("Unbound namespace URI '");
            sb.append(str);
            sb.append("'");
            StreamWriterBase.throwOutputError(sb.toString());
        }
        WName wName;
        if (prefix.length() == 0) {
            wName = super._symbols.findSymbol(s);
            prefix = null;
        }
        else {
            wName = super._symbols.findSymbol(prefix, s);
        }
        this._verifyStartElement(prefix, s);
        this._writeStartTag(wName, true, str);
    }
    
    @Override
    public void writeEmptyElement(final String s, final String s2, final String s3) {
        this._verifyStartElement(s, s2);
        WName wName;
        if (s != null && s.length() != 0) {
            wName = super._symbols.findSymbol(s, s2);
        }
        else {
            wName = super._symbols.findSymbol(s2);
        }
        this._writeStartTag(wName, true, s3);
    }
    
    @Override
    public void writeNamespace(final String s, final String s2) {
        if (s != null && s.length() != 0) {
            if (!super._stateStartElementOpen) {
                StreamWriterBase.throwOutputError(ErrorConsts.WERR_NS_NO_ELEM);
            }
            this._writeNamespace(s, s2);
            this.setPrefix(s, s2);
            return;
        }
        this.writeDefaultNamespace(s2);
    }
    
    @Override
    public void writeStartElement(String str, final String s) {
        final String prefix = super._currElem.getPrefix(str);
        if (prefix == null) {
            final StringBuilder sb = new StringBuilder();
            sb.append("Unbound namespace URI '");
            sb.append(str);
            sb.append("'");
            StreamWriterBase.throwOutputError(sb.toString());
        }
        WName wName;
        if (prefix.length() == 0) {
            wName = super._symbols.findSymbol(s);
            str = null;
        }
        else {
            wName = super._symbols.findSymbol(prefix, s);
            str = prefix;
        }
        this._verifyStartElement(str, s);
        this._writeStartTag(wName, false);
    }
    
    @Override
    public void writeStartElement(final String s, final String s2, final String s3) {
        this._verifyStartElement(s, s2);
        WName wName;
        if (s != null && s.length() != 0) {
            wName = super._symbols.findSymbol(s, s2);
        }
        else {
            wName = super._symbols.findSymbol(s2);
        }
        this._writeStartTag(wName, false, s3);
    }
    
    @Override
    public void writeTypedAttribute(final String s, final String s2, final String s3, final AsciiValueEncoder asciiValueEncoder) {
        if (!super._stateStartElementOpen) {
            StreamWriterBase.throwOutputError(ErrorConsts.WERR_ATTR_NO_ELEM);
        }
        WName wName;
        if (s != null && s.length() != 0) {
            wName = super._symbols.findSymbol(s, s3);
        }
        else {
            wName = super._symbols.findSymbol(s3);
        }
        this._writeAttribute(wName, asciiValueEncoder);
    }
}

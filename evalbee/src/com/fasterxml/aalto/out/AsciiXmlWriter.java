// 
// Decompiled by Procyon v0.6.0
// 

package com.fasterxml.aalto.out;

import java.io.OutputStream;

public final class AsciiXmlWriter extends SingleByteXmlWriter
{
    static final int LAST_VALID_CHAR = 127;
    
    public AsciiXmlWriter(final WriterConfig writerConfig, final OutputStream outputStream) {
        super(writerConfig, outputStream, OutputCharTypes.getAsciiCharTypes());
    }
    
    @Override
    public WName doConstructName(final String s) {
        return new ByteWName(s, ByteXmlWriter.getAscii(s));
    }
    
    @Override
    public WName doConstructName(final String s, final String s2) {
        final int length = s.length();
        final int n = length + 1;
        final byte[] array = new byte[s2.length() + n];
        ByteXmlWriter.getAscii(s, array, 0);
        array[length] = 58;
        ByteXmlWriter.getAscii(s2, array, n);
        return new ByteWName(s, s2, array);
    }
    
    @Override
    public int getHighestEncodable() {
        return 127;
    }
    
    @Override
    public void writeRaw(final char[] array, int i, int n) {
        if (super._out != null) {
            if (n != 0) {
                final int surrogate = super._surrogate;
                int n2 = i;
                int n3 = n;
                if (surrogate != 0) {
                    this.outputSurrogates(surrogate, array[i]);
                    n2 = i + 1;
                    n3 = n - 1;
                }
                for (i = n2; i < n3 + n2; ++i) {
                    n = array[i];
                    if (n > 127) {
                        this.reportFailedEscaping("raw content", n);
                    }
                    if (super._outputPtr >= super._outputBufferLen) {
                        this.flushBuffer();
                    }
                    super._outputBuffer[super._outputPtr++] = (byte)n;
                }
            }
        }
    }
}

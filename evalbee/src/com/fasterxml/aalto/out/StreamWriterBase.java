// 
// Decompiled by Procyon v0.6.0
// 

package com.fasterxml.aalto.out;

import java.math.BigInteger;
import java.math.BigDecimal;
import org.codehaus.stax2.DTDInfo;
import com.fasterxml.aalto.util.TextUtil;
import org.codehaus.stax2.typed.Base64Variants;
import org.codehaus.stax2.typed.Base64Variant;
import org.codehaus.stax2.validation.ValidatorPair;
import org.codehaus.stax2.validation.XMLValidationSchema;
import com.fasterxml.aalto.ValidationException;
import javax.xml.stream.Location;
import java.util.Iterator;
import com.fasterxml.aalto.impl.LocationImpl;
import org.codehaus.stax2.XMLStreamLocation2;
import javax.xml.stream.XMLStreamException;
import javax.xml.namespace.QName;
import org.codehaus.stax2.validation.XMLValidationProblem;
import org.codehaus.stax2.ri.typed.AsciiValueEncoder;
import java.text.MessageFormat;
import com.fasterxml.aalto.impl.StreamExceptionBase;
import java.io.IOException;
import com.fasterxml.aalto.impl.IoStreamException;
import com.fasterxml.aalto.impl.ErrorConsts;
import org.codehaus.stax2.validation.ValidationProblemHandler;
import org.codehaus.stax2.ri.typed.ValueEncoderFactory;
import org.codehaus.stax2.validation.XMLValidator;
import org.codehaus.stax2.validation.ValidationContext;
import javax.xml.namespace.NamespaceContext;
import org.codehaus.stax2.ri.Stax2WriterImpl;

public abstract class StreamWriterBase extends Stax2WriterImpl implements NamespaceContext, ValidationContext
{
    static final int MAX_POOL_SIZE = 8;
    protected final boolean _cfgCDataAsText;
    protected boolean _cfgCheckAttrs;
    protected boolean _cfgCheckContent;
    protected boolean _cfgCheckStructure;
    protected final WriterConfig _config;
    protected OutputElement _currElem;
    protected String _dtdRootElemName;
    protected OutputElement _outputElemPool;
    protected int _poolSize;
    protected NamespaceContext _rootNsContext;
    protected State _state;
    protected boolean _stateAnyOutput;
    protected boolean _stateEmptyElement;
    protected boolean _stateStartElementOpen;
    protected WNameTable _symbols;
    protected XMLValidator _validator;
    protected ValueEncoderFactory _valueEncoderFactory;
    protected int _vldContent;
    protected ValidationProblemHandler _vldProblemHandler;
    protected final XmlWriter _xmlWriter;
    
    public StreamWriterBase(final WriterConfig config, final XmlWriter xmlWriter, final WNameTable symbols) {
        this._validator = null;
        this._vldContent = 4;
        this._vldProblemHandler = null;
        this._state = State.PROLOG;
        this._currElem = OutputElement.createRoot();
        this._stateAnyOutput = false;
        this._stateStartElementOpen = false;
        this._stateEmptyElement = false;
        this._dtdRootElemName = null;
        this._outputElemPool = null;
        this._poolSize = 0;
        this._config = config;
        this._xmlWriter = xmlWriter;
        this._symbols = symbols;
        this._cfgCheckStructure = config.willCheckStructure();
        this._cfgCheckContent = config.willCheckContent();
        this._cfgCheckAttrs = config.willCheckAttributes();
        this._cfgCDataAsText = false;
    }
    
    private final void _finishDocument(final boolean b) {
        final State state = this._state;
        if (state != State.EPILOG) {
            if (this._cfgCheckStructure && state == State.PROLOG) {
                _reportNwfStructure(ErrorConsts.WERR_PROLOG_NO_ROOT);
            }
            if (this._stateStartElementOpen) {
                this._closeStartElement(this._stateEmptyElement);
            }
            while (this._state != State.EPILOG) {
                this.writeEndElement();
            }
        }
        if (this._symbols.maybeDirty()) {
            this._symbols.mergeToParent();
        }
        try {
            this._xmlWriter.close(b);
        }
        catch (final IOException ex) {
            throw new IoStreamException(ex);
        }
    }
    
    public static void _reportNwfAttr(final String s) {
        throwOutputError(s);
    }
    
    public static void _reportNwfAttr(final String s, final Object o) {
        throwOutputError(s, o);
    }
    
    public static void _reportNwfContent(final String s) {
        throwOutputError(s);
    }
    
    public static void _reportNwfContent(final String s, final Object o) {
        throwOutputError(s, o);
    }
    
    public static void _reportNwfName(final String s) {
        throwOutputError(s);
    }
    
    public static void _reportNwfStructure(final String s) {
        throwOutputError(s);
    }
    
    public static void _reportNwfStructure(final String s, final Object o) {
        throwOutputError(s, o);
    }
    
    public static void reportIllegalArg(final String s) {
        throw new IllegalArgumentException(s);
    }
    
    public static void reportIllegalMethod(final String s) {
        throwOutputError(s);
    }
    
    private void resetValidationFlags() {
        this._cfgCheckStructure = this._config.willCheckStructure();
        this._cfgCheckAttrs = this._config.willCheckAttributes();
    }
    
    public static void throwFromIOE(final IOException ex) {
        throw new IoStreamException(ex);
    }
    
    public static void throwOutputError(final String s) {
        throw new StreamExceptionBase(s);
    }
    
    public static void throwOutputError(final String pattern, final Object o) {
        throwOutputError(MessageFormat.format(pattern, o));
    }
    
    private final void writeTypedElement(final AsciiValueEncoder asciiValueEncoder) {
        this._stateAnyOutput = true;
        if (this._stateStartElementOpen) {
            this._closeStartElement(this._stateEmptyElement);
        }
        try {
            this._xmlWriter.writeTypedValue(asciiValueEncoder);
        }
        catch (final IOException ex) {
            throw new IoStreamException(ex);
        }
    }
    
    public void _closeStartElement(final boolean b) {
        this._stateStartElementOpen = false;
        Label_0019: {
            if (!b) {
                break Label_0019;
            }
            try {
                this._xmlWriter.writeStartTagEmptyEnd();
                while (true) {
                    if (b) {
                        final OutputElement currElem = this._currElem;
                        final OutputElement parent = currElem.getParent();
                        this._currElem = parent;
                        if (parent.isRoot()) {
                            this._state = State.EPILOG;
                        }
                        if (this._poolSize < 8) {
                            currElem.addToPool(this._outputElemPool);
                            this._outputElemPool = currElem;
                            ++this._poolSize;
                        }
                    }
                    return;
                    this._xmlWriter.writeStartTagEnd();
                    continue;
                }
            }
            catch (final IOException ex) {
                throw new IoStreamException(ex);
            }
        }
    }
    
    public void _reportInvalidContent(final int i) {
        final int vldContent = this._vldContent;
        String s;
        if (vldContent != 0) {
            if (vldContent != 1) {
                if (vldContent != 3 && vldContent != 4) {
                    final StringBuilder sb = new StringBuilder();
                    sb.append("Internal error: trying to report invalid content for ");
                    sb.append(i);
                    s = sb.toString();
                }
                else {
                    s = MessageFormat.format(ErrorConsts.VERR_ANY, this._currElem.getNameDesc(), ErrorConsts.tokenTypeDesc(i));
                }
            }
            else {
                s = MessageFormat.format(ErrorConsts.VERR_NON_MIXED, this._currElem.getNameDesc());
            }
        }
        else {
            s = MessageFormat.format(ErrorConsts.VERR_EMPTY, this._currElem.getNameDesc(), ErrorConsts.tokenTypeDesc(i));
        }
        this._reportValidationProblem(s);
    }
    
    public void _reportValidationProblem(final String s) {
        this.reportProblem(new XMLValidationProblem(this.getValidationLocation(), s, 2));
    }
    
    public abstract String _serializeQName(final QName p0);
    
    public abstract void _setPrefix(final String p0, final String p1);
    
    public void _verifyRootElement(final String s, final String s2) {
        this._state = State.TREE;
    }
    
    public void _verifyStartElement(final String str, String string) {
        if (this._stateStartElementOpen) {
            this._closeStartElement(this._stateEmptyElement);
        }
        else {
            final State state = this._state;
            if (state == State.PROLOG) {
                this._verifyRootElement(str, string);
            }
            else if (state == State.EPILOG) {
                if (this._cfgCheckStructure) {
                    if (str != null) {
                        final StringBuilder sb = new StringBuilder();
                        sb.append(str);
                        sb.append(":");
                        sb.append(string);
                        string = sb.toString();
                    }
                    _reportNwfStructure(ErrorConsts.WERR_PROLOG_SECOND_ROOT, string);
                }
                this._state = State.TREE;
            }
        }
    }
    
    public final void _verifyWriteAttr(final WName wName) {
    }
    
    public final void _verifyWriteCData() {
        this._stateAnyOutput = true;
        if (this._stateStartElementOpen) {
            this._closeStartElement(this._stateEmptyElement);
        }
        if (this._cfgCheckStructure && this.inPrologOrEpilog()) {
            _reportNwfStructure(ErrorConsts.WERR_PROLOG_CDATA);
        }
    }
    
    public final void _verifyWriteDTD() {
        if (this._cfgCheckStructure) {
            if (this._state != State.PROLOG) {
                final StringBuilder sb = new StringBuilder();
                sb.append("Can not write DOCTYPE declaration (DTD) when not in prolog any more (state ");
                sb.append(this._state);
                sb.append("; start element(s) written)");
                throw new XMLStreamException(sb.toString());
            }
            if (this._dtdRootElemName != null) {
                throw new XMLStreamException("Trying to write multiple DOCTYPE declarations");
            }
        }
    }
    
    public final void _writeAttribute(final WName wName, final String s) {
        if (this._cfgCheckAttrs) {
            this._verifyWriteAttr(wName);
        }
        try {
            this._xmlWriter.writeAttribute(wName, s);
        }
        catch (final IOException ex) {
            throw new IoStreamException(ex);
        }
    }
    
    public final void _writeAttribute(final WName wName, final AsciiValueEncoder asciiValueEncoder) {
        if (this._cfgCheckAttrs) {
            this._verifyWriteAttr(wName);
        }
        try {
            this._xmlWriter.writeAttribute(wName, asciiValueEncoder);
        }
        catch (final IOException ex) {
            throw new IoStreamException(ex);
        }
    }
    
    public final void _writeDefaultNamespace(final String s) {
        final WName symbol = this._symbols.findSymbol("xmlns");
        try {
            this._xmlWriter.writeAttribute(symbol, s);
        }
        catch (final IOException ex) {
            throw new IoStreamException(ex);
        }
    }
    
    public final void _writeNamespace(final String s, final String s2) {
        final WName symbol = this._symbols.findSymbol("xmlns", s);
        try {
            this._xmlWriter.writeAttribute(symbol, s2);
        }
        catch (final IOException ex) {
            throw new IoStreamException(ex);
        }
    }
    
    public void _writeStartDocument(final String str, final String actualEncodingIfNotSet, final String s) {
        if (this._cfgCheckStructure && this._stateAnyOutput) {
            _reportNwfStructure(ErrorConsts.WERR_DUP_XML_DECL);
        }
        this._stateAnyOutput = true;
        if (this._cfgCheckContent && str != null && str.length() > 0 && !str.equals("1.0") && !str.equals("1.1")) {
            final StringBuilder sb = new StringBuilder();
            sb.append("Illegal version argument ('");
            sb.append(str);
            sb.append("'); should only use '");
            sb.append("1.0");
            sb.append("' or '");
            sb.append("1.1");
            sb.append("'");
            _reportNwfContent(sb.toString());
        }
        String anObject = null;
        Label_0160: {
            if (str != null) {
                anObject = str;
                if (str.length() != 0) {
                    break Label_0160;
                }
            }
            anObject = "1.0";
        }
        if ("1.1".equals(anObject)) {
            this._config.enableXml11();
            this._xmlWriter.enableXml11();
        }
        if (actualEncodingIfNotSet != null && actualEncodingIfNotSet.length() > 0) {
            this._config.setActualEncodingIfNotSet(actualEncodingIfNotSet);
        }
        try {
            this._xmlWriter.writeXmlDeclaration(anObject, actualEncodingIfNotSet, s);
        }
        catch (final IOException ex) {
            throw new IoStreamException(ex);
        }
    }
    
    public void _writeStartTag(final WName wName, final boolean stateEmptyElement) {
        this._stateAnyOutput = true;
        this._stateStartElementOpen = true;
        final OutputElement outputElemPool = this._outputElemPool;
        if (outputElemPool != null) {
            this._outputElemPool = outputElemPool.reuseAsChild(this._currElem, wName);
            --this._poolSize;
            this._currElem = outputElemPool;
        }
        else {
            this._currElem = this._currElem.createChild(wName);
        }
        try {
            this._xmlWriter.writeStartTagStart(wName);
            this._stateEmptyElement = stateEmptyElement;
        }
        catch (final IOException ex) {
            throw new IoStreamException(ex);
        }
    }
    
    public void _writeStartTag(final WName wName, final boolean stateEmptyElement, final String s) {
        this._stateAnyOutput = true;
        this._stateStartElementOpen = true;
        String s2 = s;
        if (s == null) {
            s2 = "";
        }
        final OutputElement outputElemPool = this._outputElemPool;
        if (outputElemPool != null) {
            this._outputElemPool = outputElemPool.reuseAsChild(this._currElem, wName, s2);
            --this._poolSize;
            this._currElem = outputElemPool;
        }
        else {
            this._currElem = this._currElem.createChild(wName, s2);
        }
        try {
            this._xmlWriter.writeStartTagStart(wName);
            this._stateEmptyElement = stateEmptyElement;
        }
        catch (final IOException ex) {
            throw new IoStreamException(ex);
        }
    }
    
    public int addDefaultAttribute(final String s, final String s2, final String s3, final String s4) {
        return -1;
    }
    
    public void close() {
        this._finishDocument(false);
    }
    
    public void closeCompletely() {
        this._finishDocument(true);
    }
    
    public int findAttributeIndex(final String s, final String s2) {
        return -1;
    }
    
    public void flush() {
        try {
            this._xmlWriter.flush();
        }
        catch (final IOException ex) {
            throw new IoStreamException(ex);
        }
    }
    
    public int getAttributeCount() {
        return 0;
    }
    
    public String getAttributeLocalName(final int n) {
        return null;
    }
    
    public String getAttributeNamespace(final int n) {
        return null;
    }
    
    public String getAttributePrefix(final int n) {
        return null;
    }
    
    public String getAttributeType(final int n) {
        return "";
    }
    
    public String getAttributeValue(final int n) {
        return null;
    }
    
    public String getAttributeValue(final String s, final String s2) {
        return null;
    }
    
    public String getBaseUri() {
        return null;
    }
    
    public QName getCurrentElementName() {
        return this._currElem.getQName();
    }
    
    public String getEncoding() {
        return this._config.getActualEncoding();
    }
    
    public XMLStreamLocation2 getLocation() {
        return (XMLStreamLocation2)new LocationImpl(null, null, this._xmlWriter.getAbsOffset(), this._xmlWriter.getRow(), this._xmlWriter.getColumn());
    }
    
    public final NamespaceContext getNamespaceContext() {
        return this;
    }
    
    public String getNamespaceURI(final String s) {
        String s3;
        final String s2 = s3 = this._currElem.getNamespaceURI(s);
        if (s2 == null) {
            final NamespaceContext rootNsContext = this._rootNsContext;
            s3 = s2;
            if (rootNsContext != null) {
                s3 = rootNsContext.getNamespaceURI(s);
            }
        }
        return s3;
    }
    
    public String getPrefix(final String s) {
        String s3;
        final String s2 = s3 = this._currElem.getPrefix(s);
        if (s2 == null) {
            final NamespaceContext rootNsContext = this._rootNsContext;
            s3 = s2;
            if (rootNsContext != null) {
                s3 = rootNsContext.getPrefix(s);
            }
        }
        return s3;
    }
    
    public Iterator<String> getPrefixes(final String s) {
        return this._currElem.getPrefixes(s, this._rootNsContext);
    }
    
    public Object getProperty(final String s) {
        return this._config.getProperty(s, true);
    }
    
    public Location getValidationLocation() {
        return (Location)this.getLocation();
    }
    
    public String getXmlVersion() {
        String s;
        if (this._config.isXml11()) {
            s = "1.1";
        }
        else {
            s = "1.0";
        }
        return s;
    }
    
    public final boolean inPrologOrEpilog() {
        return this._state != State.TREE;
    }
    
    public boolean isNotationDeclared(final String s) {
        return false;
    }
    
    public boolean isPropertySupported(final String s) {
        return this._config.isPropertySupported(s);
    }
    
    public boolean isUnparsedEntityDeclared(final String s) {
        return false;
    }
    
    public void reportProblem(final XMLValidationProblem xmlValidationProblem) {
        final ValidationProblemHandler vldProblemHandler = this._vldProblemHandler;
        if (vldProblemHandler != null) {
            vldProblemHandler.reportProblem(xmlValidationProblem);
            return;
        }
        if (xmlValidationProblem.getSeverity() < 2) {
            return;
        }
        throw ValidationException.create(xmlValidationProblem);
    }
    
    public abstract void setDefaultNamespace(final String p0);
    
    public void setNamespaceContext(final NamespaceContext rootNsContext) {
        if (this._state != State.PROLOG) {
            throwOutputError("Called setNamespaceContext() after having already output root element.");
        }
        this._rootNsContext = rootNsContext;
    }
    
    public final void setPrefix(final String s, final String defaultNamespace) {
        s.getClass();
        if (s.length() == 0) {
            this.setDefaultNamespace(defaultNamespace);
            return;
        }
        defaultNamespace.getClass();
        Label_0121: {
            String s2;
            if (s.equals("xml")) {
                if (defaultNamespace.equals("http://www.w3.org/XML/1998/namespace")) {
                    break Label_0121;
                }
                s2 = ErrorConsts.ERR_NS_REDECL_XML;
            }
            else {
                if (!s.equals("xmlns")) {
                    String s3;
                    if (defaultNamespace.equals("http://www.w3.org/XML/1998/namespace")) {
                        s3 = ErrorConsts.ERR_NS_REDECL_XML_URI;
                    }
                    else {
                        if (!defaultNamespace.equals("http://www.w3.org/2000/xmlns/")) {
                            break Label_0121;
                        }
                        s3 = ErrorConsts.ERR_NS_REDECL_XMLNS_URI;
                    }
                    throwOutputError(s3, s);
                    break Label_0121;
                }
                if (defaultNamespace.equals("http://www.w3.org/2000/xmlns/")) {
                    break Label_0121;
                }
                s2 = ErrorConsts.ERR_NS_REDECL_XMLNS;
            }
            throwOutputError(s2, defaultNamespace);
        }
        if (defaultNamespace.length() == 0 && !this._config.isXml11()) {
            throwOutputError(ErrorConsts.ERR_NS_EMPTY);
        }
        this._setPrefix(s, defaultNamespace);
    }
    
    public boolean setProperty(final String s, final Object o) {
        return this._config.setProperty(s, o);
    }
    
    public ValidationProblemHandler setValidationProblemHandler(final ValidationProblemHandler vldProblemHandler) {
        final ValidationProblemHandler vldProblemHandler2 = this._vldProblemHandler;
        this._vldProblemHandler = vldProblemHandler;
        return vldProblemHandler2;
    }
    
    public XMLValidator stopValidatingAgainst(final XMLValidationSchema xmlValidationSchema) {
        final XMLValidator[] array = new XMLValidator[2];
        XMLValidator xmlValidator2;
        if (ValidatorPair.removeValidator(this._validator, xmlValidationSchema, array)) {
            final XMLValidator xmlValidator = array[0];
            this._validator = array[1];
            xmlValidator.validationCompleted(false);
            xmlValidator2 = xmlValidator;
            if (this._validator == null) {
                this.resetValidationFlags();
                xmlValidator2 = xmlValidator;
            }
        }
        else {
            xmlValidator2 = null;
        }
        return xmlValidator2;
    }
    
    public XMLValidator stopValidatingAgainst(XMLValidator xmlValidator) {
        final XMLValidator[] array = new XMLValidator[2];
        if (ValidatorPair.removeValidator(this._validator, xmlValidator, array)) {
            final XMLValidator xmlValidator2 = array[0];
            this._validator = array[1];
            xmlValidator2.validationCompleted(false);
            xmlValidator = xmlValidator2;
            if (this._validator == null) {
                this.resetValidationFlags();
                xmlValidator = xmlValidator2;
            }
        }
        else {
            xmlValidator = null;
        }
        return xmlValidator;
    }
    
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append("[StreamWriter: ");
        sb.append(this.getClass());
        sb.append(", underlying outputter: ");
        final XmlWriter xmlWriter = this._xmlWriter;
        String string;
        if (xmlWriter == null) {
            string = "NULL";
        }
        else {
            string = xmlWriter.toString();
        }
        sb.append(string);
        return sb.toString();
    }
    
    public XMLValidator validateAgainst(final XMLValidationSchema xmlValidationSchema) {
        final XMLValidator validator = xmlValidationSchema.createValidator((ValidationContext)this);
        final XMLValidator validator2 = this._validator;
        if (validator2 == null) {
            this._cfgCheckStructure = true;
            this._cfgCheckAttrs = true;
            this._validator = validator;
        }
        else {
            this._validator = (XMLValidator)new ValidatorPair(validator2, validator);
        }
        return validator;
    }
    
    public final ValueEncoderFactory valueEncoderFactory() {
        if (this._valueEncoderFactory == null) {
            this._valueEncoderFactory = new ValueEncoderFactory();
        }
        return this._valueEncoderFactory;
    }
    
    public final void writeAttribute(final String s, final String s2) {
        if (!this._stateStartElementOpen) {
            throwOutputError(ErrorConsts.WERR_ATTR_NO_ELEM);
        }
        this._writeAttribute(this._symbols.findSymbol(s), s2);
    }
    
    public abstract void writeAttribute(final String p0, final String p1, final String p2);
    
    public abstract void writeAttribute(final String p0, final String p1, final String p2, final String p3);
    
    public void writeBinary(final Base64Variant base64Variant, final byte[] array, final int n, final int n2) {
        this.writeTypedElement((AsciiValueEncoder)this.valueEncoderFactory().getEncoder(base64Variant, array, n, n2));
    }
    
    public void writeBinary(final byte[] array, final int n, final int n2) {
        this.writeTypedElement((AsciiValueEncoder)this.valueEncoderFactory().getEncoder(Base64Variants.getDefaultVariant(), array, n, n2));
    }
    
    public void writeBinaryAttribute(final String s, final String s2, final String s3, final byte[] array) {
        this.writeTypedAttribute(s, s2, s3, (AsciiValueEncoder)this.valueEncoderFactory().getEncoder(Base64Variants.getDefaultVariant(), array, 0, array.length));
    }
    
    public void writeBinaryAttribute(final Base64Variant base64Variant, final String s, final String s2, final String s3, final byte[] array) {
        this.writeTypedAttribute(s, s2, s3, (AsciiValueEncoder)this.valueEncoderFactory().getEncoder(base64Variant, array, 0, array.length));
    }
    
    public void writeBoolean(final boolean b) {
        final ValueEncoderFactory valueEncoderFactory = this.valueEncoderFactory();
        String s;
        if (b) {
            s = "true";
        }
        else {
            s = "false";
        }
        this.writeTypedElement((AsciiValueEncoder)valueEncoderFactory.getScalarEncoder(s));
    }
    
    public final void writeBooleanAttribute(final String s, final String s2, final String s3, final boolean b) {
        this.writeTypedAttribute(s, s2, s3, (AsciiValueEncoder)this.valueEncoderFactory().getEncoder(b));
    }
    
    public void writeCData(final String s) {
        if (this._cfgCDataAsText) {
            this.writeCharacters(s);
            return;
        }
        this._verifyWriteCData();
        if (this._vldContent == 3) {
            final XMLValidator validator = this._validator;
            if (validator != null) {
                validator.validateText(s, false);
            }
        }
        try {
            final int writeCData = this._xmlWriter.writeCData(s);
            if (writeCData >= 0) {
                _reportNwfContent(ErrorConsts.WERR_CDATA_CONTENT, writeCData);
            }
        }
        catch (final IOException ex) {
            throw new IoStreamException(ex);
        }
    }
    
    public void writeCData(final char[] array, int writeCData, final int n) {
        if (this._cfgCDataAsText) {
            this.writeCharacters(array, writeCData, n);
            return;
        }
        this._verifyWriteCData();
        try {
            writeCData = this._xmlWriter.writeCData(array, writeCData, n);
            if (writeCData >= 0) {
                _reportNwfContent(ErrorConsts.WERR_CDATA_CONTENT, writeCData);
            }
        }
        catch (final IOException ex) {
            throw new IoStreamException(ex);
        }
    }
    
    public void writeCharacters(final String s) {
        this._stateAnyOutput = true;
        if (this._stateStartElementOpen) {
            this._closeStartElement(this._stateEmptyElement);
        }
        if (this.inPrologOrEpilog()) {
            this.writeSpace(s);
            return;
        }
        final int vldContent = this._vldContent;
        if (vldContent <= 1) {
            if (vldContent == 0 || !TextUtil.isAllWhitespace(s, this._config.isXml11())) {
                this._reportInvalidContent(4);
            }
        }
        else if (vldContent == 3) {
            final XMLValidator validator = this._validator;
            if (validator != null) {
                validator.validateText(s, false);
            }
        }
        try {
            this._xmlWriter.writeCharacters(s);
        }
        catch (final IOException ex) {
            throw new IoStreamException(ex);
        }
    }
    
    public void writeCharacters(final char[] array, final int n, final int n2) {
        this._stateAnyOutput = true;
        if (this._stateStartElementOpen) {
            this._closeStartElement(this._stateEmptyElement);
        }
        if (this.inPrologOrEpilog()) {
            this.writeSpace(array, n, n2);
            return;
        }
        final int vldContent = this._vldContent;
        if (vldContent <= 1) {
            if (vldContent == 0 || !TextUtil.isAllWhitespace(array, n, n2, this._config.isXml11())) {
                this._reportInvalidContent(4);
            }
        }
        else if (vldContent == 3) {
            final XMLValidator validator = this._validator;
            if (validator != null) {
                validator.validateText(array, n, n2, false);
            }
        }
        if (n2 > 0) {
            try {
                this._xmlWriter.writeCharacters(array, n, n2);
            }
            catch (final IOException ex) {
                throw new IoStreamException(ex);
            }
        }
    }
    
    public void writeComment(final String s) {
        this._stateAnyOutput = true;
        if (this._stateStartElementOpen) {
            this._closeStartElement(this._stateEmptyElement);
        }
        if (this._vldContent == 0) {
            this._reportInvalidContent(5);
        }
        try {
            final int writeComment = this._xmlWriter.writeComment(s);
            if (writeComment >= 0) {
                _reportNwfContent(ErrorConsts.WERR_COMMENT_CONTENT, writeComment);
            }
        }
        catch (final IOException ex) {
            throw new IoStreamException(ex);
        }
    }
    
    public final void writeDTD(final String s) {
        this._verifyWriteDTD();
        this._dtdRootElemName = "";
        try {
            this._xmlWriter.writeDTD(s);
        }
        catch (final IOException ex) {
            throw new IoStreamException(ex);
        }
    }
    
    public void writeDTD(final String dtdRootElemName, final String s, final String s2, final String s3) {
        this._verifyWriteDTD();
        this._dtdRootElemName = dtdRootElemName;
        try {
            this._xmlWriter.writeDTD(this._symbols.findSymbol(dtdRootElemName), s, s2, s3);
        }
        catch (final IOException ex) {
            throw new IoStreamException(ex);
        }
    }
    
    public void writeDTD(final DTDInfo dtdInfo) {
        this.writeDTD(dtdInfo.getDTDRootName(), dtdInfo.getDTDSystemId(), dtdInfo.getDTDPublicId(), dtdInfo.getDTDInternalSubset());
    }
    
    public void writeDecimal(final BigDecimal bigDecimal) {
        this.writeTypedElement((AsciiValueEncoder)this.valueEncoderFactory().getScalarEncoder(bigDecimal.toString()));
    }
    
    public final void writeDecimalAttribute(final String s, final String s2, final String s3, final BigDecimal bigDecimal) {
        this.writeTypedAttribute(s, s2, s3, (AsciiValueEncoder)this.valueEncoderFactory().getScalarEncoder(bigDecimal.toString()));
    }
    
    public abstract void writeDefaultNamespace(final String p0);
    
    public void writeDouble(final double n) {
        this.writeTypedElement((AsciiValueEncoder)this.valueEncoderFactory().getEncoder(n));
    }
    
    public void writeDoubleArray(final double[] array, final int n, final int n2) {
        this.writeTypedElement((AsciiValueEncoder)this.valueEncoderFactory().getEncoder(array, n, n2));
    }
    
    public void writeDoubleArrayAttribute(final String s, final String s2, final String s3, final double[] array) {
        this.writeTypedAttribute(s, s2, s3, (AsciiValueEncoder)this.valueEncoderFactory().getEncoder(array, 0, array.length));
    }
    
    public final void writeDoubleAttribute(final String s, final String s2, final String s3, final double n) {
        this.writeTypedAttribute(s, s2, s3, (AsciiValueEncoder)this.valueEncoderFactory().getEncoder(n));
    }
    
    public void writeEmptyElement(final String s) {
        this._verifyStartElement(null, s);
        final WName symbol = this._symbols.findSymbol(s);
        final XMLValidator validator = this._validator;
        if (validator != null) {
            validator.validateElementStart(s, "", "");
        }
        this._writeStartTag(symbol, true);
    }
    
    public abstract void writeEmptyElement(final String p0, final String p1);
    
    public abstract void writeEmptyElement(final String p0, final String p1, final String p2);
    
    public void writeEndDocument() {
        this._finishDocument(false);
    }
    
    public void writeEndElement() {
        if (this._stateStartElementOpen && this._stateEmptyElement) {
            this._stateEmptyElement = false;
            this._closeStartElement(true);
        }
        if (this._state != State.TREE) {
            _reportNwfStructure("No open start element, when trying to write end element");
        }
        final OutputElement currElem = this._currElem;
        this._currElem = currElem.getParent();
        if (this._poolSize < 8) {
            currElem.addToPool(this._outputElemPool);
            this._outputElemPool = currElem;
            ++this._poolSize;
        }
        try {
            if (this._stateStartElementOpen) {
                this._stateStartElementOpen = false;
                this._xmlWriter.writeStartTagEmptyEnd();
            }
            else {
                this._xmlWriter.writeEndTag(currElem.getName());
            }
            if (this._currElem.isRoot()) {
                this._state = State.EPILOG;
            }
            final XMLValidator validator = this._validator;
            if (validator != null) {
                this._vldContent = validator.validateElementEnd(currElem.getLocalName(), currElem.getNonNullPrefix(), currElem.getNonNullNamespaceURI());
            }
        }
        catch (final IOException ex) {
            throw new IoStreamException(ex);
        }
    }
    
    public void writeEntityRef(final String s) {
        this._stateAnyOutput = true;
        if (this._stateStartElementOpen) {
            this._closeStartElement(this._stateEmptyElement);
        }
        if (this._cfgCheckStructure && this.inPrologOrEpilog()) {
            _reportNwfStructure(ErrorConsts.WERR_PROLOG_ENTITY);
        }
        if (this._vldContent == 0) {
            this._reportInvalidContent(9);
        }
        try {
            this._xmlWriter.writeEntityReference(this._symbols.findSymbol(s));
        }
        catch (final IOException ex) {
            throw new IoStreamException(ex);
        }
    }
    
    public void writeFloat(final float n) {
        this.writeTypedElement((AsciiValueEncoder)this.valueEncoderFactory().getEncoder(n));
    }
    
    public void writeFloatArray(final float[] array, final int n, final int n2) {
        this.writeTypedElement((AsciiValueEncoder)this.valueEncoderFactory().getEncoder(array, n, n2));
    }
    
    public void writeFloatArrayAttribute(final String s, final String s2, final String s3, final float[] array) {
        this.writeTypedAttribute(s, s2, s3, (AsciiValueEncoder)this.valueEncoderFactory().getEncoder(array, 0, array.length));
    }
    
    public final void writeFloatAttribute(final String s, final String s2, final String s3, final float n) {
        this.writeTypedAttribute(s, s2, s3, (AsciiValueEncoder)this.valueEncoderFactory().getEncoder(n));
    }
    
    public void writeFullEndElement() {
        if (this._stateStartElementOpen && this._stateEmptyElement) {
            this._stateEmptyElement = false;
            this._closeStartElement(true);
        }
        this.writeEndElement();
    }
    
    public void writeInt(final int n) {
        this.writeTypedElement((AsciiValueEncoder)this.valueEncoderFactory().getEncoder(n));
    }
    
    public final void writeIntArray(final int[] array, final int n, final int n2) {
        this.writeTypedElement((AsciiValueEncoder)this.valueEncoderFactory().getEncoder(array, n, n2));
    }
    
    public void writeIntArrayAttribute(final String s, final String s2, final String s3, final int[] array) {
        this.writeTypedAttribute(s, s2, s3, (AsciiValueEncoder)this.valueEncoderFactory().getEncoder(array, 0, array.length));
    }
    
    public final void writeIntAttribute(final String s, final String s2, final String s3, final int n) {
        this.writeTypedAttribute(s, s2, s3, (AsciiValueEncoder)this.valueEncoderFactory().getEncoder(n));
    }
    
    public void writeInteger(final BigInteger bigInteger) {
        this.writeTypedElement((AsciiValueEncoder)this.valueEncoderFactory().getScalarEncoder(bigInteger.toString()));
    }
    
    public final void writeIntegerAttribute(final String s, final String s2, final String s3, final BigInteger bigInteger) {
        this.writeTypedAttribute(s, s2, s3, (AsciiValueEncoder)this.valueEncoderFactory().getScalarEncoder(bigInteger.toString()));
    }
    
    public void writeLong(final long n) {
        this.writeTypedElement((AsciiValueEncoder)this.valueEncoderFactory().getEncoder(n));
    }
    
    public void writeLongArray(final long[] array, final int n, final int n2) {
        this.writeTypedElement((AsciiValueEncoder)this.valueEncoderFactory().getEncoder(array, n, n2));
    }
    
    public void writeLongArrayAttribute(final String s, final String s2, final String s3, final long[] array) {
        this.writeTypedAttribute(s, s2, s3, (AsciiValueEncoder)this.valueEncoderFactory().getEncoder(array, 0, array.length));
    }
    
    public final void writeLongAttribute(final String s, final String s2, final String s3, final long n) {
        this.writeTypedAttribute(s, s2, s3, (AsciiValueEncoder)this.valueEncoderFactory().getEncoder(n));
    }
    
    public abstract void writeNamespace(final String p0, final String p1);
    
    public void writeProcessingInstruction(final String s) {
        this.writeProcessingInstruction(s, null);
    }
    
    public void writeProcessingInstruction(final String s, final String s2) {
        this._stateAnyOutput = true;
        if (this._stateStartElementOpen) {
            this._closeStartElement(this._stateEmptyElement);
        }
        if (this._vldContent == 0) {
            this._reportInvalidContent(3);
        }
        try {
            final int writePI = this._xmlWriter.writePI(this._symbols.findSymbol(s), s2);
            if (writePI >= 0) {
                _reportNwfContent(ErrorConsts.WERR_PI_CONTENT, writePI);
            }
        }
        catch (final IOException ex) {
            throw new IoStreamException(ex);
        }
    }
    
    public void writeQName(final QName qName) {
        this.writeTypedElement((AsciiValueEncoder)this.valueEncoderFactory().getScalarEncoder(this._serializeQName(qName)));
    }
    
    public final void writeQNameAttribute(final String s, final String s2, final String s3, final QName qName) {
        this.writeAttribute(s, s2, s3, this._serializeQName(qName));
    }
    
    public void writeRaw(final String s) {
        this._stateAnyOutput = true;
        if (this._stateStartElementOpen) {
            this._closeStartElement(this._stateEmptyElement);
        }
        try {
            this._xmlWriter.writeRaw(s, 0, s.length());
        }
        catch (final IOException ex) {
            throw new IoStreamException(ex);
        }
    }
    
    public void writeRaw(final String s, final int n, final int n2) {
        this._stateAnyOutput = true;
        if (this._stateStartElementOpen) {
            this._closeStartElement(this._stateEmptyElement);
        }
        try {
            this._xmlWriter.writeRaw(s, n, n2);
        }
        catch (final IOException ex) {
            throw new IoStreamException(ex);
        }
    }
    
    public void writeRaw(final char[] array, final int n, final int n2) {
        this._stateAnyOutput = true;
        if (this._stateStartElementOpen) {
            this._closeStartElement(this._stateEmptyElement);
        }
        try {
            this._xmlWriter.writeRaw(array, n, n2);
        }
        catch (final IOException ex) {
            throw new IoStreamException(ex);
        }
    }
    
    public void writeSpace(final String s) {
        try {
            this._xmlWriter.writeSpace(s);
        }
        catch (final IOException ex) {
            throw new IoStreamException(ex);
        }
    }
    
    public void writeSpace(final char[] array, final int n, final int n2) {
        try {
            this._xmlWriter.writeSpace(array, n, n2);
        }
        catch (final IOException ex) {
            throw new IoStreamException(ex);
        }
    }
    
    public void writeStartDocument() {
        String actualEncoding;
        if ((actualEncoding = this._config.getActualEncoding()) == null) {
            this._config.setActualEncodingIfNotSet("UTF-8");
            actualEncoding = "UTF-8";
        }
        this._writeStartDocument("1.0", actualEncoding, null);
    }
    
    public void writeStartDocument(final String s) {
        this._writeStartDocument(s, this._config.getActualEncoding(), null);
    }
    
    public void writeStartDocument(final String s, final String s2) {
        this._writeStartDocument(s2, s, null);
    }
    
    public void writeStartDocument(final String s, final String s2, final boolean b) {
        String s3;
        if (b) {
            s3 = "yes";
        }
        else {
            s3 = "no";
        }
        this._writeStartDocument(s, s2, s3);
    }
    
    public void writeStartElement(final String s) {
        this._verifyStartElement(null, s);
        final WName symbol = this._symbols.findSymbol(s);
        final XMLValidator validator = this._validator;
        if (validator != null) {
            validator.validateElementStart(s, "", "");
        }
        this._writeStartTag(symbol, false);
    }
    
    public abstract void writeStartElement(final String p0, final String p1);
    
    public abstract void writeStartElement(final String p0, final String p1, final String p2);
    
    public abstract void writeTypedAttribute(final String p0, final String p1, final String p2, final AsciiValueEncoder p3);
    
    public enum State
    {
        private static final State[] $VALUES;
        
        EPILOG, 
        PROLOG, 
        TREE;
    }
}

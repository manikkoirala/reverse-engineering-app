// 
// Decompiled by Procyon v0.6.0
// 

package com.fasterxml.aalto.out;

import java.util.ArrayList;
import java.util.List;
import javax.xml.namespace.NamespaceContext;
import java.util.Arrays;

final class NsBinder
{
    static final int DEFAULT_ARRAY_SIZE = 32;
    String[] _nsStrings;
    int _scopeEnd;
    final int _scopeStart;
    
    private NsBinder(final int n, final String[] nsStrings) {
        this._scopeEnd = n;
        this._scopeStart = n;
        this._nsStrings = nsStrings;
    }
    
    public static NsBinder createEmpty() {
        final String[] array = new String[32];
        array[0] = "xml";
        array[1] = "http://www.w3.org/XML/1998/namespace";
        array[2] = "xmlns";
        array[3] = "http://www.w3.org/2000/xmlns/";
        return new NsBinder(4, array);
    }
    
    public String addMapping(String anObject, final String s) {
        final String[] nsStrings = this._nsStrings;
        final int hashCode = anObject.hashCode();
        for (int i = this._scopeStart; i < this._scopeEnd; i += 2) {
            final String s2 = nsStrings[i];
            if (s2 == anObject || (s2.hashCode() == hashCode && s2.equals(anObject))) {
                ++i;
                anObject = nsStrings[i];
                nsStrings[i] = s;
                return anObject;
            }
        }
        String[] nsStrings2 = nsStrings;
        if (this._scopeEnd >= nsStrings.length) {
            nsStrings2 = Arrays.copyOf(nsStrings, nsStrings.length << 1);
            this._nsStrings = nsStrings2;
        }
        final int scopeEnd = this._scopeEnd;
        final int n = scopeEnd + 1;
        nsStrings2[scopeEnd] = anObject;
        this._scopeEnd = n + 1;
        nsStrings2[n] = s;
        return null;
    }
    
    public NsBinder createChild() {
        return new NsBinder(this._scopeEnd, this._nsStrings);
    }
    
    public String findPrefixByUri(final String anObject) {
        final String[] nsStrings = this._nsStrings;
        final int hashCode = anObject.hashCode();
    Label_0133:
        for (int i = this._scopeEnd - 1; i > 0; i -= 2) {
            final String s = nsStrings[i];
            if (s == anObject || (s.hashCode() == hashCode && s.equals(anObject))) {
                int j = i - 1;
                final String anObject2 = nsStrings[j];
                if (i < this._scopeStart) {
                    final int hashCode2 = anObject2.hashCode();
                    while (j < this._scopeEnd) {
                        final String s2 = nsStrings[i];
                        if (s2 == anObject2) {
                            continue Label_0133;
                        }
                        if (s2.hashCode() == hashCode2 && s2.equals(anObject2)) {
                            continue Label_0133;
                        }
                        j += 2;
                    }
                }
                return anObject2;
            }
        }
        return null;
    }
    
    public String findUriByPrefix(final String anObject) {
        final String[] nsStrings = this._nsStrings;
        final int hashCode = anObject.hashCode();
        for (int i = this._scopeEnd - 2; i >= 0; i -= 2) {
            final String s = nsStrings[i];
            if (s == anObject || (s.hashCode() == hashCode && s.equals(anObject))) {
                return nsStrings[i + 1];
            }
        }
        return null;
    }
    
    public String generatePrefix(final String str, final NamespaceContext namespaceContext, final int[] array) {
        final String[] nsStrings = this._nsStrings;
        int i = array[0];
        String intern = null;
        int n = 0;
    Label_0011:
        while (true) {
            final StringBuilder sb = new StringBuilder();
            sb.append(str);
            sb.append(i);
            intern = sb.toString().intern();
            n = i + 1;
            final int hashCode = intern.hashCode();
            for (int j = this._scopeEnd - 2; j >= 0; j -= 2) {
                final String s = nsStrings[j];
                i = n;
                if (s == intern) {
                    continue Label_0011;
                }
                if (s.hashCode() == hashCode && s.equals(intern)) {
                    i = n;
                    continue Label_0011;
                }
            }
            if (namespaceContext == null || namespaceContext.getNamespaceURI(intern) == null) {
                break;
            }
            i = n;
        }
        array[0] = n;
        return intern;
    }
    
    public List<String> getPrefixesBoundToUri(final String anObject, List<String> list) {
        final String[] nsStrings = this._nsStrings;
        final int hashCode = anObject.hashCode();
        List<String> list2;
    Label_0174:
        for (int i = this._scopeEnd - 1; i > 0; i -= 2, list = list2) {
            final String s = nsStrings[i];
            if (s != anObject) {
                list2 = list;
                if (s.hashCode() != hashCode) {
                    continue;
                }
                list2 = list;
                if (!s.equals(anObject)) {
                    continue;
                }
            }
            int j = i - 1;
            final String anObject2 = nsStrings[j];
            if (i < this._scopeStart) {
                final int hashCode2 = anObject2.hashCode();
                while (j < this._scopeEnd) {
                    final String s2 = nsStrings[i];
                    list2 = list;
                    if (s2 == anObject2) {
                        continue Label_0174;
                    }
                    if (s2.hashCode() == hashCode2 && s2.equals(anObject2)) {
                        list2 = list;
                        continue Label_0174;
                    }
                    j += 2;
                }
            }
            if ((list2 = list) == null) {
                list2 = new ArrayList<String>();
            }
            list2.add(anObject2);
        }
        return list;
    }
    
    public int localSize() {
        return this._scopeEnd - this._scopeStart >> 1;
    }
    
    public int size() {
        return this._scopeEnd >> 1;
    }
    
    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append("[");
        sb.append(NsBinder.class.toString());
        sb.append("; ");
        sb.append(this.size());
        sb.append(" entries; of which ");
        sb.append(this.localSize());
        sb.append(" local]");
        return sb.toString();
    }
}

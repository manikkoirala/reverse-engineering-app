// 
// Decompiled by Procyon v0.6.0
// 

package com.fasterxml.aalto.out;

import java.io.Writer;
import java.io.OutputStream;

public abstract class WName
{
    protected final String _localName;
    protected final String _prefix;
    
    public WName(final String localName) {
        this._prefix = null;
        this._localName = localName;
    }
    
    public WName(final String prefix, final String localName) {
        this._prefix = prefix;
        this._localName = localName;
    }
    
    public abstract int appendBytes(final byte[] p0, final int p1);
    
    public abstract int appendChars(final char[] p0, final int p1);
    
    @Override
    public final boolean equals(final Object o) {
        boolean b = true;
        if (o == this) {
            return true;
        }
        if (o.getClass() != this.getClass() && !(o instanceof WName)) {
            return false;
        }
        final WName wName = (WName)o;
        if (!wName._localName.equals(this._localName)) {
            return false;
        }
        final String prefix = this._prefix;
        final String prefix2 = wName._prefix;
        if (prefix == null) {
            if (prefix2 != null) {
                b = false;
            }
            return b;
        }
        return prefix.equals(prefix2);
    }
    
    public final String getLocalName() {
        return this._localName;
    }
    
    public final String getPrefix() {
        return this._prefix;
    }
    
    public final String getPrefixedName() {
        if (this._prefix == null) {
            return this._localName;
        }
        final StringBuilder sb = new StringBuilder();
        sb.append(this._prefix);
        sb.append(":");
        sb.append(this._localName);
        return sb.toString();
    }
    
    public final boolean hasName(final String anObject) {
        return this._prefix == null && this._localName.equals(anObject);
    }
    
    public final boolean hasName(String localName, String prefix) {
        final String localName2 = this._localName;
        boolean b = false;
        if (localName2 == prefix) {
            if (localName == null) {
                if (this._prefix == null) {
                    b = true;
                }
                return b;
            }
            prefix = this._prefix;
        }
        else {
            if (localName2.hashCode() != prefix.hashCode()) {
                return false;
            }
            if (localName == null) {
                if (this._prefix != null) {
                    return false;
                }
            }
            else if (!localName.equals(this._prefix)) {
                return false;
            }
            localName = this._localName;
        }
        return localName.equals(prefix);
    }
    
    public final boolean hasPrefix() {
        return this._prefix != null;
    }
    
    @Override
    public int hashCode() {
        final int hashCode = this._localName.hashCode();
        final String prefix = this._prefix;
        int n = hashCode;
        if (prefix != null) {
            n = (hashCode ^ prefix.hashCode());
        }
        return n;
    }
    
    public abstract int serializedLength();
    
    @Override
    public String toString() {
        if (this._prefix == null) {
            return this._localName;
        }
        final StringBuilder sb = new StringBuilder();
        sb.append(this._prefix);
        sb.append(":");
        sb.append(this._localName);
        return sb.toString();
    }
    
    public abstract void writeBytes(final OutputStream p0);
    
    public abstract void writeChars(final Writer p0);
}

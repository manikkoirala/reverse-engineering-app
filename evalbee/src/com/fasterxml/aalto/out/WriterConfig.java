// 
// Decompiled by Procyon v0.6.0
// 

package com.fasterxml.aalto.out;

import java.util.HashMap;
import com.fasterxml.aalto.util.BufferRecycler;
import java.lang.ref.SoftReference;
import com.fasterxml.aalto.impl.CommonConfig;

public final class WriterConfig extends CommonConfig
{
    protected static final String DEFAULT_AUTOMATIC_NS_PREFIX = "ans";
    static final int DEFAULT_FLAGS = 32;
    static final int F_AUTO_CLOSE_OUTPUT = 16;
    static final int F_AUTO_EMPTY_ELEMS = 64;
    static final int F_NS_AWARE = 32;
    static final int F_NS_REPAIRING = 1;
    static final int PROP_AUTO_NS_PREFIX = -2;
    static final ThreadLocal<SoftReference<BufferRecycler>> mRecyclerRef;
    private static final HashMap<String, Integer> sProperties;
    BufferRecycler _currRecycler;
    private String _encoding;
    private final EncodingContext _encodingContext;
    private String _propAutoNsPrefix;
    
    static {
        final HashMap sProperties2 = new HashMap();
        (sProperties = sProperties2).put("javax.xml.stream.isRepairingNamespaces", 1);
        sProperties2.put("javax.xml.stream.isNamespaceAware", 32);
        sProperties2.put("javax.xml.stream.reporter", null);
        sProperties2.put("org.codehaus.stax2.autoCloseOutput", 16);
        sProperties2.put("org.codehaus.stax2.automaticEmptyElements", 64);
        sProperties2.put("org.codehaus.stax2.automaticNsPrefix", -2);
        sProperties2.put("org.codehaus.stax2.textEscaper", null);
        sProperties2.put("org.codehaus.stax2.attrValueEscaper", null);
        mRecyclerRef = new ThreadLocal<SoftReference<BufferRecycler>>();
    }
    
    public WriterConfig() {
        this(null, 32, 0, new EncodingContext(), "ans");
    }
    
    private WriterConfig(final String encoding, final int flags, final int flagMods, final EncodingContext encodingContext, final String propAutoNsPrefix) {
        super(flags, flagMods);
        this._currRecycler = null;
        this._encoding = encoding;
        this._encodingContext = encodingContext;
        final SoftReference softReference = WriterConfig.mRecyclerRef.get();
        if (softReference != null) {
            this._currRecycler = (BufferRecycler)softReference.get();
        }
        super._flags = flags;
        super._flagMods = flagMods;
        this._propAutoNsPrefix = propAutoNsPrefix;
    }
    
    private BufferRecycler createRecycler() {
        final BufferRecycler referent = new BufferRecycler();
        WriterConfig.mRecyclerRef.set(new SoftReference<BufferRecycler>(referent));
        return referent;
    }
    
    public byte[] allocFullBBuffer(final int n) {
        final BufferRecycler currRecycler = this._currRecycler;
        if (currRecycler != null) {
            final byte[] fullBBuffer = currRecycler.getFullBBuffer(n);
            if (fullBBuffer != null) {
                return fullBBuffer;
            }
        }
        return new byte[n];
    }
    
    public char[] allocFullCBuffer(final int n) {
        final BufferRecycler currRecycler = this._currRecycler;
        if (currRecycler != null) {
            final char[] fullCBuffer = currRecycler.getFullCBuffer(n);
            if (fullCBuffer != null) {
                return fullCBuffer;
            }
        }
        return new char[n];
    }
    
    public char[] allocMediumCBuffer(final int n) {
        final BufferRecycler currRecycler = this._currRecycler;
        if (currRecycler != null) {
            final char[] mediumCBuffer = currRecycler.getMediumCBuffer(n);
            if (mediumCBuffer != null) {
                return mediumCBuffer;
            }
        }
        return new char[n];
    }
    
    public void configureForRobustness() {
    }
    
    public void configureForSpeed() {
    }
    
    public void configureForXmlConformance() {
    }
    
    public WriterConfig createNonShared() {
        return new WriterConfig(this._encoding, super._flags, super._flagMods, this._encodingContext, this._propAutoNsPrefix);
    }
    
    public void doAutoCloseOutput(final boolean b) {
        this.setFlag(16, b);
    }
    
    public void enableXml11() {
    }
    
    public void freeFullBBuffer(final byte[] array) {
        if (this._currRecycler == null) {
            this._currRecycler = this.createRecycler();
        }
        this._currRecycler.returnFullBBuffer(array);
    }
    
    public void freeFullCBuffer(final char[] array) {
        if (this._currRecycler == null) {
            this._currRecycler = this.createRecycler();
        }
        this._currRecycler.returnFullCBuffer(array);
    }
    
    public void freeMediumCBuffer(final char[] array) {
        if (this._currRecycler == null) {
            this._currRecycler = this.createRecycler();
        }
        this._currRecycler.returnMediumCBuffer(array);
    }
    
    @Override
    public String getActualEncoding() {
        return this._encoding;
    }
    
    public WNameTable getAsciiSymbols(final WNameFactory wNameFactory) {
        return this._encodingContext.getAsciiSymbols(wNameFactory);
    }
    
    public String getAutomaticNsPrefix() {
        return this._propAutoNsPrefix;
    }
    
    public WNameTable getCharSymbols(final WNameFactory wNameFactory) {
        return this._encodingContext.getCharSymbols(wNameFactory);
    }
    
    @Override
    public String getExternalEncoding() {
        return this.getActualEncoding();
    }
    
    public WNameTable getLatin1Symbols(final WNameFactory wNameFactory) {
        return this._encodingContext.getLatin1Symbols(wNameFactory);
    }
    
    public String getPreferredEncoding() {
        return this._encoding;
    }
    
    @Override
    public Object getProperty(final String s, final boolean b) {
        final HashMap<String, Integer> sProperties = WriterConfig.sProperties;
        final Integer n = sProperties.get(s);
        if (n == null) {
            if (sProperties.containsKey(s)) {
                return null;
            }
            return super.getProperty(s, b);
        }
        else {
            final int intValue = n;
            if (intValue >= 0) {
                return this.hasFlag(intValue);
            }
            if (intValue != -2) {
                return null;
            }
            return this._propAutoNsPrefix;
        }
    }
    
    public WNameTable getUtf8Symbols(final WNameFactory wNameFactory) {
        return this._encodingContext.getUtf8Symbols(wNameFactory);
    }
    
    public boolean isNamespaceAware() {
        return this.hasFlag(32);
    }
    
    @Override
    public boolean isPropertySupported(final String key) {
        return WriterConfig.sProperties.containsKey(key) || super.isPropertySupported(key);
    }
    
    @Override
    public boolean isXml11() {
        return false;
    }
    
    public void setActualEncodingIfNotSet(final String encoding) {
        final String encoding2 = this._encoding;
        if (encoding2 == null || encoding2.length() == 0) {
            this._encoding = encoding;
        }
    }
    
    @Override
    public boolean setProperty(final String s, final Object o) {
        final HashMap<String, Integer> sProperties = WriterConfig.sProperties;
        final Integer n = sProperties.get(s);
        if (n == null) {
            return !sProperties.containsKey(s) && super.setProperty(s, o);
        }
        final int intValue = n;
        if (intValue >= 0) {
            final boolean booleanValue = (boolean)o;
            if (intValue == 32 && !booleanValue) {
                return false;
            }
            this.setFlag(intValue, booleanValue);
            return true;
        }
        else {
            if (intValue != -2) {
                return false;
            }
            this._propAutoNsPrefix = o.toString();
            return true;
        }
    }
    
    public boolean willAutoCloseOutput() {
        return this.hasFlag(16);
    }
    
    public boolean willCheckAttributes() {
        return false;
    }
    
    public boolean willCheckContent() {
        return true;
    }
    
    public boolean willCheckNames() {
        return false;
    }
    
    public boolean willCheckStructure() {
        return true;
    }
    
    public boolean willEscapeCR() {
        return true;
    }
    
    public boolean willFixContent() {
        return true;
    }
    
    public boolean willRepairNamespaces() {
        return this.hasFlag(1);
    }
    
    public static final class EncodingContext
    {
        WNameTable mAsciiTable;
        WNameTable mCharTable;
        WNameTable mLatin1Table;
        WNameTable mUtf8Table;
        
        public WNameTable getAsciiSymbols(final WNameFactory wNameFactory) {
            synchronized (this) {
                if (this.mAsciiTable == null) {
                    this.mAsciiTable = new WNameTable(64);
                }
                return this.mAsciiTable.createChild(wNameFactory);
            }
        }
        
        public WNameTable getCharSymbols(final WNameFactory wNameFactory) {
            synchronized (this) {
                if (this.mCharTable == null) {
                    this.mCharTable = new WNameTable(64);
                }
                return this.mCharTable.createChild(wNameFactory);
            }
        }
        
        public WNameTable getLatin1Symbols(final WNameFactory wNameFactory) {
            synchronized (this) {
                if (this.mLatin1Table == null) {
                    this.mLatin1Table = new WNameTable(64);
                }
                return this.mLatin1Table.createChild(wNameFactory);
            }
        }
        
        public WNameTable getUtf8Symbols(final WNameFactory wNameFactory) {
            synchronized (this) {
                if (this.mUtf8Table == null) {
                    this.mUtf8Table = new WNameTable(64);
                }
                return this.mUtf8Table.createChild(wNameFactory);
            }
        }
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.fasterxml.aalto.out;

import java.io.Writer;
import java.io.OutputStream;

public final class ByteWName extends WName
{
    final byte[] _bytes;
    
    public ByteWName(final String s, final String s2, final byte[] bytes) {
        super(s, s2);
        this._bytes = bytes;
    }
    
    public ByteWName(final String s, final byte[] bytes) {
        super(s);
        this._bytes = bytes;
    }
    
    @Override
    public int appendBytes(final byte[] array, final int n) {
        final byte[] bytes = this._bytes;
        final int length = bytes.length;
        System.arraycopy(bytes, 0, array, n, length);
        return length;
    }
    
    @Override
    public int appendChars(final char[] array, final int n) {
        throw new RuntimeException("Internal error: appendChars() should never be called");
    }
    
    @Override
    public final int serializedLength() {
        return this._bytes.length;
    }
    
    @Override
    public void writeBytes(final OutputStream outputStream) {
        outputStream.write(this._bytes);
    }
    
    @Override
    public void writeChars(final Writer writer) {
        throw new RuntimeException("Internal error: writeChars() should never be called");
    }
}

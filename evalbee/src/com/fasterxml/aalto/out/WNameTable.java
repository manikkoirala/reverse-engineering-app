// 
// Decompiled by Procyon v0.6.0
// 

package com.fasterxml.aalto.out;

import com.fasterxml.aalto.util.NameTable;

public final class WNameTable extends NameTable
{
    static final int INITIAL_COLLISION_LEN = 32;
    static final int LAST_VALID_BUCKET = 254;
    static final int MIN_HASH_SIZE = 16;
    private int mCollCount;
    private int mCollEnd;
    private Bucket[] mCollList;
    private boolean mCollListShared;
    private int mCount;
    private int[] mMainHash;
    private int mMainHashMask;
    private boolean mMainHashShared;
    private WName[] mMainNames;
    private boolean mMainNamesShared;
    final WNameFactory mNameFactory;
    private transient boolean mNeedRehash;
    final WNameTable mParent;
    
    public WNameTable(final int n) {
        this.mNameFactory = null;
        this.mParent = null;
        int n2 = 16;
        int n3;
        if (n < 16) {
            n3 = n2;
        }
        else {
            n3 = n;
            if ((n - 1 & n) != 0x0) {
                while ((n3 = n2) < n) {
                    n2 += n2;
                }
            }
        }
        this.mCount = 0;
        this.mMainHashShared = false;
        this.mMainNamesShared = false;
        this.mMainHashMask = n3 - 1;
        this.mMainHash = new int[n3];
        this.mMainNames = new WName[n3];
        this.mCollListShared = true;
        this.mCollList = null;
        this.mCollEnd = 0;
        this.mNeedRehash = false;
    }
    
    private WNameTable(final WNameTable mParent, final WNameFactory mNameFactory) {
        this.mParent = mParent;
        this.mNameFactory = mNameFactory;
        this.mCount = mParent.mCount;
        this.mMainHashMask = mParent.mMainHashMask;
        this.mMainHash = mParent.mMainHash;
        this.mMainNames = mParent.mMainNames;
        this.mCollList = mParent.mCollList;
        this.mCollCount = mParent.mCollCount;
        this.mCollEnd = mParent.mCollEnd;
        this.mNeedRehash = false;
        this.mMainHashShared = true;
        this.mMainNamesShared = true;
        this.mCollListShared = true;
    }
    
    private void addSymbol(final WName wName) {
        if (this.mMainHashShared) {
            this.unshareMain();
        }
        if (this.mNeedRehash) {
            this.rehash();
        }
        final int hashCode = wName.hashCode();
        ++this.mCount;
        final int n = this.mMainHashMask & hashCode;
        if (this.mMainNames[n] == null) {
            this.mMainHash[n] = hashCode << 8;
            if (this.mMainNamesShared) {
                this.unshareNames();
            }
            this.mMainNames[n] = wName;
        }
        else {
            if (this.mCollListShared) {
                this.unshareCollision();
            }
            ++this.mCollCount;
            final int n2 = this.mMainHash[n];
            int bestBucket = n2 & 0xFF;
            if (bestBucket == 0) {
                final int mCollEnd = this.mCollEnd;
                if (mCollEnd <= 254) {
                    this.mCollEnd = mCollEnd + 1;
                    if ((bestBucket = mCollEnd) >= this.mCollList.length) {
                        this.expandCollision();
                        bestBucket = mCollEnd;
                    }
                }
                else {
                    bestBucket = this.findBestBucket();
                }
                this.mMainHash[n] = ((n2 & 0xFFFFFF00) | bestBucket + 1);
            }
            else {
                --bestBucket;
            }
            final Bucket[] mCollList = this.mCollList;
            mCollList[bestBucket] = new Bucket(wName, mCollList[bestBucket]);
        }
        final int length = this.mMainHash.length;
        final int mCount = this.mCount;
        if (mCount > length >> 1) {
            final int n3 = length >> 2;
            if (mCount > length - n3 || this.mCollCount >= n3) {
                this.mNeedRehash = true;
            }
        }
    }
    
    private void expandCollision() {
        final Bucket[] mCollList = this.mCollList;
        final int length = mCollList.length;
        System.arraycopy(mCollList, 0, this.mCollList = new Bucket[length + length], 0, length);
    }
    
    private int findBestBucket() {
        final Bucket[] mCollList = this.mCollList;
        final int mCollEnd = this.mCollEnd;
        int n = Integer.MAX_VALUE;
        int n2 = -1;
        int n3;
        for (int i = 0; i < mCollEnd; ++i, n = n3) {
            final int length = mCollList[i].length();
            if (length < (n3 = n)) {
                if (length == 1) {
                    return i;
                }
                n2 = i;
                n3 = length;
            }
        }
        return n2;
    }
    
    private boolean mergeFromChild(final WNameTable wNameTable) {
        synchronized (this) {
            final int mCount = wNameTable.mCount;
            if (mCount <= this.mCount) {
                return false;
            }
            this.mCount = mCount;
            this.mMainHashMask = wNameTable.mMainHashMask;
            this.mMainHash = wNameTable.mMainHash;
            this.mMainNames = wNameTable.mMainNames;
            this.mCollList = wNameTable.mCollList;
            this.mCollCount = wNameTable.mCollCount;
            this.mCollEnd = wNameTable.mCollEnd;
            return true;
        }
    }
    
    private void rehash() {
        final int n = 0;
        this.mNeedRehash = false;
        this.mMainNamesShared = false;
        final int length = this.mMainHash.length;
        final int n2 = length + length;
        this.mMainHash = new int[n2];
        this.mMainHashMask = n2 - 1;
        final WName[] mMainNames = this.mMainNames;
        this.mMainNames = new WName[n2];
        int i = 0;
        int j = 0;
        while (i < length) {
            final WName wName = mMainNames[i];
            int n3 = j;
            if (wName != null) {
                n3 = j + 1;
                final int hashCode = wName.hashCode();
                final int n4 = this.mMainHashMask & hashCode;
                this.mMainNames[n4] = wName;
                this.mMainHash[n4] = hashCode << 8;
            }
            ++i;
            j = n3;
        }
        final int mCollEnd = this.mCollEnd;
        if (mCollEnd == 0) {
            return;
        }
        this.mCollCount = 0;
        this.mCollEnd = 0;
        this.mCollListShared = false;
        final Bucket[] mCollList = this.mCollList;
        this.mCollList = new Bucket[mCollList.length];
        for (int k = n; k < mCollEnd; ++k) {
            int n5;
            for (Bucket mNext = mCollList[k]; mNext != null; mNext = mNext.mNext, j = n5) {
                n5 = j + 1;
                final WName mName = mNext.mName;
                final int hashCode2 = mName.hashCode();
                final int n6 = this.mMainHashMask & hashCode2;
                final int[] mMainHash = this.mMainHash;
                final int n7 = mMainHash[n6];
                final WName[] mMainNames2 = this.mMainNames;
                if (mMainNames2[n6] == null) {
                    mMainHash[n6] = hashCode2 << 8;
                    mMainNames2[n6] = mName;
                }
                else {
                    ++this.mCollCount;
                    int bestBucket = n7 & 0xFF;
                    if (bestBucket == 0) {
                        final int mCollEnd2 = this.mCollEnd;
                        if (mCollEnd2 <= 254) {
                            this.mCollEnd = mCollEnd2 + 1;
                            if ((bestBucket = mCollEnd2) >= this.mCollList.length) {
                                this.expandCollision();
                                bestBucket = mCollEnd2;
                            }
                        }
                        else {
                            bestBucket = this.findBestBucket();
                        }
                        this.mMainHash[n6] = ((n7 & 0xFFFFFF00) | bestBucket + 1);
                    }
                    else {
                        --bestBucket;
                    }
                    final Bucket[] mCollList2 = this.mCollList;
                    mCollList2[bestBucket] = new Bucket(mName, mCollList2[bestBucket]);
                }
            }
        }
        if (j == this.mCount) {
            return;
        }
        final StringBuilder sb = new StringBuilder();
        sb.append("Internal error: count after rehash ");
        sb.append(j);
        sb.append("; should be ");
        sb.append(this.mCount);
        throw new Error(sb.toString());
    }
    
    private void unshareCollision() {
        final Bucket[] mCollList = this.mCollList;
        if (mCollList == null) {
            this.mCollList = new Bucket[32];
        }
        else {
            final int length = mCollList.length;
            System.arraycopy(mCollList, 0, this.mCollList = new Bucket[length], 0, length);
        }
        this.mCollListShared = false;
    }
    
    private void unshareMain() {
        final int[] mMainHash = this.mMainHash;
        final int length = mMainHash.length;
        System.arraycopy(mMainHash, 0, this.mMainHash = new int[length], 0, length);
        this.mMainHashShared = false;
    }
    
    private void unshareNames() {
        final WName[] mMainNames = this.mMainNames;
        final int length = mMainNames.length;
        System.arraycopy(mMainNames, 0, this.mMainNames = new WName[length], 0, length);
        this.mMainNamesShared = false;
    }
    
    public WNameTable createChild(final WNameFactory wNameFactory) {
        synchronized (this) {
            return new WNameTable(this, wNameFactory);
        }
    }
    
    public WName findSymbol(final String s) {
        final int hashCode = s.hashCode();
        final int n = this.mMainHashMask & hashCode;
        final int n2 = this.mMainHash[n];
        if ((hashCode ^ n2 >> 8) << 8 == 0) {
            final WName wName = this.mMainNames[n];
            if (wName != null && wName.hasName(s)) {
                return wName;
            }
        }
        if (n2 != 0) {
            final int n3 = n2 & 0xFF;
            if (n3 > 0) {
                final Bucket bucket = this.mCollList[n3 - 1];
                if (bucket != null) {
                    final WName find = bucket.find(s);
                    if (find != null) {
                        return find;
                    }
                }
            }
        }
        final WName constructName = this.mNameFactory.constructName(s);
        this.addSymbol(constructName);
        return constructName;
    }
    
    public WName findSymbol(final String s, final String s2) {
        final int n = s2.hashCode() ^ s.hashCode();
        final int n2 = this.mMainHashMask & n;
        final int n3 = this.mMainHash[n2];
        if ((n ^ n3 >> 8) << 8 == 0) {
            final WName wName = this.mMainNames[n2];
            if (wName != null && wName.hasName(s, s2)) {
                return wName;
            }
        }
        if (n3 != 0) {
            final int n4 = n3 & 0xFF;
            if (n4 > 0) {
                final Bucket bucket = this.mCollList[n4 - 1];
                if (bucket != null) {
                    final WName find = bucket.find(s, s2);
                    if (find != null) {
                        return find;
                    }
                }
            }
        }
        final WName constructName = this.mNameFactory.constructName(s, s2);
        this.addSymbol(constructName);
        return constructName;
    }
    
    public void markAsShared() {
        this.mMainHashShared = true;
        this.mMainNamesShared = true;
        this.mCollListShared = true;
    }
    
    @Override
    public boolean maybeDirty() {
        return this.mMainHashShared ^ true;
    }
    
    public boolean mergeToParent() {
        final boolean mergeFromChild = this.mParent.mergeFromChild(this);
        this.markAsShared();
        return mergeFromChild;
    }
    
    public void nuke() {
        this.mMainHash = null;
        this.mMainNames = null;
        this.mCollList = null;
    }
    
    @Override
    public int size() {
        return this.mCount;
    }
    
    public String toDebugString() {
        final StringBuilder sb = new StringBuilder();
        sb.append("[WNameTable, size: ");
        sb.append(this.mCount);
        sb.append('/');
        sb.append(this.mMainHash.length);
        sb.append(" -> ");
        final int n = 0;
        for (int i = 0; i < this.mMainHash.length; ++i) {
            sb.append("\n#");
            sb.append(i);
            sb.append(": 0x");
            sb.append(Integer.toHexString(this.mMainHash[i]));
            sb.append(" == ");
            final WName wName = this.mMainNames[i];
            if (wName == null) {
                sb.append("null");
            }
            else {
                sb.append('\"');
                sb.append(wName.toString());
                sb.append('\"');
            }
        }
        sb.append("\nSpill(");
        sb.append(this.mCollEnd);
        sb.append("):");
        for (int j = n; j < this.mCollEnd; ++j) {
            final Bucket bucket = this.mCollList[j];
            sb.append("\nsp#");
            sb.append(j);
            sb.append(": ");
            sb.append(bucket.toDebugString());
        }
        return sb.toString();
    }
    
    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append("[WNameTable, size: ");
        sb.append(this.mCount);
        sb.append('/');
        sb.append(this.mMainHash.length);
        sb.append(", ");
        sb.append(this.mCollCount);
        sb.append(" coll; avg length: ");
        int mCount = this.mCount;
        for (int i = 0; i < this.mCollEnd; ++i) {
            for (int length = this.mCollList[i].length(), j = 1; j <= length; ++j) {
                mCount += j;
            }
        }
        final int mCount2 = this.mCount;
        double d;
        if (mCount2 == 0) {
            d = 0.0;
        }
        else {
            d = mCount / (double)mCount2;
        }
        sb.append(d);
        sb.append(']');
        return sb.toString();
    }
    
    public static final class Bucket
    {
        final WName mName;
        final Bucket mNext;
        
        public Bucket(final WName mName, final Bucket mNext) {
            this.mName = mName;
            this.mNext = mNext;
        }
        
        public WName find(final String s) {
            if (this.mName.hasName(s)) {
                return this.mName;
            }
            for (Bucket bucket = this.mNext; bucket != null; bucket = bucket.mNext) {
                final WName mName = bucket.mName;
                if (mName.hasName(s)) {
                    return mName;
                }
            }
            return null;
        }
        
        public WName find(final String s, final String s2) {
            if (this.mName.hasName(s, s2)) {
                return this.mName;
            }
            for (Bucket bucket = this.mNext; bucket != null; bucket = bucket.mNext) {
                final WName mName = bucket.mName;
                if (mName.hasName(s, s2)) {
                    return mName;
                }
            }
            return null;
        }
        
        public int length() {
            Bucket bucket = this.mNext;
            int n = 1;
            while (bucket != null) {
                ++n;
                bucket = bucket.mNext;
            }
            return n;
        }
        
        public String toDebugString() {
            final StringBuilder sb = new StringBuilder();
            sb.append("[Bucket(");
            sb.append(this.length());
            sb.append("): ");
            for (Bucket mNext = this; mNext != null; mNext = mNext.mNext) {
                sb.append('\"');
                sb.append(mNext.mName.toString());
                sb.append("\" -> ");
            }
            sb.append("NULL]");
            return sb.toString();
        }
    }
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.fasterxml.aalto.out;

import com.fasterxml.aalto.impl.ErrorConsts;
import org.codehaus.stax2.ri.typed.AsciiValueEncoder;
import com.fasterxml.aalto.io.UTF8Writer;
import com.fasterxml.aalto.util.XmlCharTypes;
import java.io.Writer;

public final class CharXmlWriter extends XmlWriter
{
    static final int DEFAULT_FULL_BUFFER_SIZE = 1000;
    static final int DEFAULT_SMALL_SIZE = 200;
    protected Writer _out;
    protected char[] _outputBuffer;
    protected final int _outputBufferLen;
    protected int _outputPtr;
    final XmlCharTypes mCharTypes;
    private final int mEncHighChar;
    protected final int mSmallWriteSize;
    private final int mTableCheckEnd;
    
    public CharXmlWriter(final WriterConfig writerConfig, final Writer out) {
        super(writerConfig);
        this._out = out;
        final char[] allocFullCBuffer = writerConfig.allocFullCBuffer(1000);
        this._outputBuffer = allocFullCBuffer;
        this._outputBufferLen = allocFullCBuffer.length;
        this.mSmallWriteSize = 200;
        this._outputPtr = 0;
        final int guessEncodingBitSize = XmlWriter.guessEncodingBitSize(writerConfig);
        int n;
        if (guessEncodingBitSize < 16) {
            n = 1 << guessEncodingBitSize;
        }
        else {
            n = 65534;
        }
        this.mEncHighChar = n;
        this.mTableCheckEnd = Math.min(256, n);
        XmlCharTypes mCharTypes;
        if (guessEncodingBitSize < 8) {
            mCharTypes = OutputCharTypes.getAsciiCharTypes();
        }
        else {
            mCharTypes = OutputCharTypes.getLatin1CharTypes();
        }
        this.mCharTypes = mCharTypes;
    }
    
    private final void fastWriteRaw(final char c) {
        if (this._outputPtr >= this._outputBufferLen) {
            if (this._out == null) {
                return;
            }
            this.flushBuffer();
        }
        this._outputBuffer[this._outputPtr++] = c;
    }
    
    private final void fastWriteRaw(final char c, final char c2) {
        if (this._outputPtr + 1 >= this._outputBufferLen) {
            if (this._out == null) {
                return;
            }
            this.flushBuffer();
        }
        final char[] outputBuffer = this._outputBuffer;
        final int outputPtr = this._outputPtr;
        final int n = outputPtr + 1;
        outputBuffer[outputPtr] = c;
        this._outputPtr = n + 1;
        outputBuffer[n] = c2;
    }
    
    private final void fastWriteRaw(final String s) {
        final int length = s.length();
        final int outputPtr = this._outputPtr;
        final int outputBufferLen = this._outputBufferLen;
        int outputPtr2 = outputPtr;
        if (outputPtr + length >= outputBufferLen) {
            if (this._out == null) {
                return;
            }
            if (length > outputBufferLen) {
                this.writeRaw(s, 0, s.length());
                return;
            }
            this.flushBuffer();
            outputPtr2 = this._outputPtr;
        }
        s.getChars(0, length, this._outputBuffer, outputPtr2);
        this._outputPtr = outputPtr2 + length;
    }
    
    private final void flushBuffer() {
        final int outputPtr = this._outputPtr;
        if (outputPtr > 0) {
            final Writer out = this._out;
            if (out != null) {
                super._locPastChars += outputPtr;
                super._locRowStartOffset -= outputPtr;
                this._outputPtr = 0;
                out.write(this._outputBuffer, 0, outputPtr);
            }
        }
    }
    
    private final void writeAttrValue(final String s, int i) {
        int srcBegin = 0;
        while (i > 0) {
            final char[] copyBuffer = super._copyBuffer;
            int length;
            if (i < (length = copyBuffer.length)) {
                length = i;
            }
            final int srcEnd = srcBegin + length;
            s.getChars(srcBegin, srcEnd, copyBuffer, 0);
            this.writeAttrValue(copyBuffer, 0, length);
            i -= length;
            srcBegin = srcEnd;
        }
    }
    
    private final void writeAttrValue(final char[] array, int i, int n) {
        if (this._out == null) {
            return;
        }
        if (this._outputPtr + n > this._outputBufferLen) {
            this.writeSplitAttrValue(array, i, n);
            return;
        }
        final int n2 = n + i;
    Label_0034:
        while (i < n2) {
            final int[] attr_CHARS = this.mCharTypes.ATTR_CHARS;
            final int mTableCheckEnd = this.mTableCheckEnd;
            do {
                final char c = array[i];
                if (c < mTableCheckEnd) {
                    if (attr_CHARS[c] == 0) {
                        final char[] outputBuffer = this._outputBuffer;
                        n = this._outputPtr++;
                        outputBuffer[n] = c;
                        n = i + 1;
                        continue;
                    }
                }
                n = i + 1;
                if (c < mTableCheckEnd) {
                    i = attr_CHARS[c];
                    if (i != 1) {
                        if (i != 2 && i != 3 && i != 4 && i != 9 && i != 10 && i != 14) {
                            final char[] outputBuffer2 = this._outputBuffer;
                            i = this._outputPtr++;
                            outputBuffer2[i] = c;
                            i = n;
                            continue Label_0034;
                        }
                    }
                    else {
                        this.reportInvalidChar(c);
                    }
                }
                else if (c < this.mEncHighChar) {
                    final char[] outputBuffer3 = this._outputBuffer;
                    i = this._outputPtr++;
                    outputBuffer3[i] = c;
                    i = n;
                    continue Label_0034;
                }
                this.writeAsEntity(c);
                i = n;
                if (n2 - n >= this._outputBufferLen - this._outputPtr) {
                    this.flushBuffer();
                    i = n;
                    continue Label_0034;
                }
                continue Label_0034;
            } while ((i = n) < n2);
            break;
        }
    }
    
    private int writeCDataContents(final char[] array, final int n, int i) {
        final int n2 = i + n;
        i = n;
    Label_0007:
        while (i < n2) {
            final int[] other_CHARS = this.mCharTypes.OTHER_CHARS;
            final int mTableCheckEnd = this.mTableCheckEnd;
            do {
                final char c = array[i];
                if (c < mTableCheckEnd) {
                    if (other_CHARS[c] == 0) {
                        if (this._outputPtr >= this._outputBufferLen) {
                            this.flushBuffer();
                        }
                        this._outputBuffer[this._outputPtr++] = c;
                        continue;
                    }
                }
                final int n3 = i + 1;
                Label_0211: {
                    if (c < mTableCheckEnd) {
                        final int n4 = other_CHARS[c];
                        if (n4 != 1) {
                            i = n3;
                            if (n4 != 2) {
                                i = n3;
                                if (n4 != 3) {
                                    if (n4 != 4) {
                                        if (n4 != 17) {
                                            i = n3;
                                            break Label_0211;
                                        }
                                    }
                                    else {
                                        this.reportFailedEscaping("CDATA block", c);
                                    }
                                    i = n3;
                                    if (n3 - n >= 3) {
                                        i = n3;
                                        if (array[n3 - 2] == ']') {
                                            i = n3;
                                            if (array[n3 - 3] == ']') {
                                                i = n3 - 1;
                                                this.writeCDataEnd();
                                                this.writeCDataStart();
                                            }
                                        }
                                    }
                                }
                            }
                        }
                        else {
                            this.reportInvalidChar(c);
                            i = n3;
                        }
                    }
                    else {
                        i = n3;
                        if (c >= this.mEncHighChar) {
                            this.reportFailedEscaping("CDATA block", c);
                            i = n3;
                        }
                    }
                }
                if (this._outputPtr >= this._outputBufferLen) {
                    this.flushBuffer();
                }
                this._outputBuffer[this._outputPtr++] = c;
                continue Label_0007;
            } while (++i < n2);
            break;
        }
        return -1;
    }
    
    private int writeCommentContents(final char[] array, int i, int n, final boolean b) {
        final int n2 = n + i;
    Label_0005:
        while (i < n2) {
            final int[] other_CHARS = this.mCharTypes.OTHER_CHARS;
            final int mTableCheckEnd = this.mTableCheckEnd;
            do {
                final char c = array[i];
                if (c < mTableCheckEnd) {
                    if (other_CHARS[c] == 0) {
                        if (this._outputPtr >= this._outputBufferLen) {
                            this.flushBuffer();
                        }
                        final char[] outputBuffer = this._outputBuffer;
                        n = this._outputPtr++;
                        outputBuffer[n] = c;
                        n = i + 1;
                        continue;
                    }
                }
                ++i;
                Label_0198: {
                    if (c < mTableCheckEnd) {
                        n = other_CHARS[c];
                        if (n != 1) {
                            if (n != 2 && n != 3) {
                                if (n != 4) {
                                    if (n != 13) {
                                        break Label_0198;
                                    }
                                }
                                else {
                                    this.reportFailedEscaping("comment", c);
                                }
                                if (i == n2 || array[i] == '-') {
                                    if (!super._config.willFixContent()) {
                                        return i - 1;
                                    }
                                    if (this._outputPtr >= this._outputBufferLen) {
                                        this.flushBuffer();
                                    }
                                    final char[] outputBuffer2 = this._outputBuffer;
                                    n = this._outputPtr++;
                                    outputBuffer2[n] = ' ';
                                }
                            }
                        }
                        else {
                            this.reportInvalidChar(c);
                        }
                    }
                    else if (c >= this.mEncHighChar) {
                        this.reportFailedEscaping("comment", c);
                    }
                }
                if (this._outputPtr >= this._outputBufferLen) {
                    this.flushBuffer();
                }
                final char[] outputBuffer3 = this._outputBuffer;
                n = this._outputPtr++;
                outputBuffer3[n] = c;
                continue Label_0005;
            } while ((i = n) < n2);
            break;
        }
        return -1;
    }
    
    private int writePIContents(final char[] array, int i, int n) {
        final int n2 = n + i;
    Label_0005:
        while (i < n2) {
            final int[] other_CHARS = this.mCharTypes.OTHER_CHARS;
            final int mTableCheckEnd = this.mTableCheckEnd;
            do {
                final char c = array[i];
                if (c < mTableCheckEnd) {
                    if (other_CHARS[c] == 0) {
                        if (this._outputPtr >= this._outputBufferLen) {
                            this.flushBuffer();
                        }
                        final char[] outputBuffer = this._outputBuffer;
                        n = this._outputPtr++;
                        outputBuffer[n] = c;
                        n = i + 1;
                        continue;
                    }
                }
                ++i;
                Label_0144: {
                    if (c < mTableCheckEnd) {
                        n = other_CHARS[c];
                        if (n != 1) {
                            if (n != 2 && n != 3) {
                                if (n != 4) {
                                    if (n != 12) {
                                        break Label_0144;
                                    }
                                }
                                else {
                                    this.reportFailedEscaping("processing instruction", c);
                                }
                                if (i < n2 && array[i] == '>') {
                                    return i;
                                }
                            }
                        }
                        else {
                            this.reportInvalidChar(c);
                        }
                    }
                    else if (c >= this.mEncHighChar) {
                        this.reportFailedEscaping("processing instruction", c);
                    }
                }
                if (this._outputPtr >= this._outputBufferLen) {
                    this.flushBuffer();
                }
                final char[] outputBuffer2 = this._outputBuffer;
                n = this._outputPtr++;
                outputBuffer2[n] = c;
                continue Label_0005;
            } while ((i = n) < n2);
            break;
        }
        return -1;
    }
    
    private final void writeSplitAttrValue(final char[] array, int i, int n) {
        final int n2 = n + i;
    Label_0005:
        while (i < n2) {
            final int[] attr_CHARS = this.mCharTypes.ATTR_CHARS;
            final int mTableCheckEnd = this.mTableCheckEnd;
            do {
                final char c = array[i];
                if (c < mTableCheckEnd) {
                    if (attr_CHARS[c] == 0) {
                        if (this._outputPtr >= this._outputBufferLen) {
                            this.flushBuffer();
                        }
                        final char[] outputBuffer = this._outputBuffer;
                        n = this._outputPtr++;
                        outputBuffer[n] = c;
                        n = i + 1;
                        continue;
                    }
                }
                ++i;
                Label_0133: {
                    if (c < mTableCheckEnd) {
                        n = attr_CHARS[c];
                        if (n != 1) {
                            if (n != 2 && n != 3 && n != 4 && n != 9 && n != 10 && n != 14) {
                                break Label_0133;
                            }
                        }
                        else {
                            this.reportInvalidChar(c);
                        }
                    }
                    else if (c < this.mEncHighChar) {
                        break Label_0133;
                    }
                    this.writeAsEntity(c);
                    continue Label_0005;
                }
                if (this._outputPtr >= this._outputBufferLen) {
                    this.flushBuffer();
                }
                final char[] outputBuffer2 = this._outputBuffer;
                n = this._outputPtr++;
                outputBuffer2[n] = c;
                continue Label_0005;
            } while ((i = n) < n2);
            break;
        }
    }
    
    @Override
    public void _closeTarget(final boolean b) {
        final Writer out = this._out;
        if (out != null && (b || out instanceof UTF8Writer)) {
            out.close();
            this._out = null;
        }
    }
    
    @Override
    public void _releaseBuffers() {
        super._releaseBuffers();
        final char[] outputBuffer = this._outputBuffer;
        if (outputBuffer != null) {
            super._config.freeFullCBuffer(outputBuffer);
            this._outputBuffer = null;
        }
    }
    
    @Override
    public WName constructName(final String s) {
        return new CharWName(s);
    }
    
    @Override
    public WName constructName(final String s, final String s2) {
        return new CharWName(s, s2);
    }
    
    @Override
    public final void flush() {
        if (this._out != null) {
            this.flushBuffer();
            this._out.flush();
        }
    }
    
    @Override
    public int getHighestEncodable() {
        return this.mEncHighChar;
    }
    
    @Override
    public int getOutputPtr() {
        return this._outputPtr;
    }
    
    public int verifyCDataContent(final String s) {
        if (s != null && s.length() >= 3) {
            final int index = s.indexOf(93);
            if (index >= 0) {
                return s.indexOf("]]>", index);
            }
        }
        return -1;
    }
    
    public int verifyCDataContent(final char[] array, int i, final int n) {
        if (array != null) {
            int n2 = i;
        Label_0007:
            while (true) {
                i = n2 + 2;
                while (i < n) {
                    final char c = array[i];
                    if (c == ']') {
                        ++i;
                    }
                    else {
                        n2 = i;
                        if (c != '>') {
                            continue Label_0007;
                        }
                        n2 = i;
                        if (array[i - 1] != ']') {
                            continue Label_0007;
                        }
                        final int n3 = i - 2;
                        n2 = i;
                        if (array[n3] == ']') {
                            return n3;
                        }
                        continue Label_0007;
                    }
                }
                break;
            }
        }
        return -1;
    }
    
    public int verifyCommentContent(final String s) {
        int n;
        final int fromIndex = n = s.indexOf(45);
        if (fromIndex >= 0 && (n = fromIndex) < s.length() - 1) {
            n = s.indexOf("--", fromIndex);
        }
        return n;
    }
    
    public final void writeAsEntity(int n) {
        final char[] outputBuffer = this._outputBuffer;
        int n2;
        if ((n2 = this._outputPtr) + 10 >= outputBuffer.length) {
            this.flushBuffer();
            n2 = this._outputPtr;
        }
        final int n3 = n2 + 1;
        outputBuffer[n2] = '&';
        if (n < 256) {
            if (n == 38) {
                n = n3 + 1;
                outputBuffer[n3] = 'a';
                final int n4 = n + 1;
                outputBuffer[n] = 'm';
                n = n4 + 1;
                outputBuffer[n4] = 'p';
            }
            else if (n == 60) {
                final int n5 = n3 + 1;
                outputBuffer[n3] = 'l';
                n = n5 + 1;
                outputBuffer[n5] = 't';
            }
            else if (n == 62) {
                final int n6 = n3 + 1;
                outputBuffer[n3] = 'g';
                n = n6 + 1;
                outputBuffer[n6] = 't';
            }
            else if (n == 39) {
                final int n7 = n3 + 1;
                outputBuffer[n3] = 'a';
                n = n7 + 1;
                outputBuffer[n7] = 'p';
                final int n8 = n + 1;
                outputBuffer[n] = 'o';
                n = n8 + 1;
                outputBuffer[n8] = 's';
            }
            else if (n == 34) {
                n = n3 + 1;
                outputBuffer[n3] = 'q';
                final int n9 = n + 1;
                outputBuffer[n] = 'u';
                final int n10 = n9 + 1;
                outputBuffer[n9] = 'o';
                n = n10 + 1;
                outputBuffer[n10] = 't';
            }
            else {
                final int n11 = n3 + 1;
                outputBuffer[n3] = '#';
                final int n12 = n11 + 1;
                outputBuffer[n11] = 'x';
                int n13 = n12;
                int n14;
                if ((n14 = n) >= 16) {
                    int n15 = n >> 4;
                    if (n15 < 10) {
                        n15 += 48;
                    }
                    else {
                        n15 += 87;
                    }
                    outputBuffer[n12] = (char)n15;
                    n14 = (n & 0xF);
                    n13 = n12 + 1;
                }
                final int n16 = n13 + 1;
                if (n14 < 10) {
                    n = n14 + 48;
                }
                else {
                    n = n14 + 87;
                }
                outputBuffer[n13] = (char)n;
                n = n16;
            }
        }
        else {
            final int n17 = n3 + 1;
            outputBuffer[n3] = '#';
            final int n18 = n17 + 1;
            outputBuffer[n17] = 'x';
            int n19 = 20;
            int n20 = n18;
            int i;
            int n22;
            do {
                final int n21 = n >> n19 & 0xF;
                if (n21 > 0 || (n22 = n20) != n18) {
                    int n23;
                    if (n21 < 10) {
                        n23 = n21 + 48;
                    }
                    else {
                        n23 = n21 + 87;
                    }
                    outputBuffer[n20] = (char)n23;
                    n22 = n20 + 1;
                }
                i = (n19 -= 4);
                n20 = n22;
            } while (i > 0);
            n &= 0xF;
            final int n24 = n22 + 1;
            if (n < 10) {
                n += 48;
            }
            else {
                n += 87;
            }
            outputBuffer[n22] = (char)n;
            n = n24;
        }
        outputBuffer[n] = ';';
        this._outputPtr = n + 1;
    }
    
    @Override
    public void writeAttribute(final WName wName, final String s) {
        if (this._out == null) {
            return;
        }
        this.fastWriteRaw(' ');
        this.writeName(wName);
        this.fastWriteRaw('=', '\"');
        int length;
        if (s == null) {
            length = 0;
        }
        else {
            length = s.length();
        }
        if (length > 0) {
            this.writeAttrValue(s, length);
        }
        this.fastWriteRaw('\"');
    }
    
    @Override
    public final void writeAttribute(final WName wName, final AsciiValueEncoder asciiValueEncoder) {
        if (this._out == null) {
            return;
        }
        this.fastWriteRaw(' ');
        this.writeName(wName);
        this.fastWriteRaw('=', '\"');
        this.writeTypedValue(asciiValueEncoder);
        this.fastWriteRaw('\"');
    }
    
    @Override
    public void writeAttribute(final WName wName, final char[] array, final int n, final int n2) {
        if (this._out == null) {
            return;
        }
        this.fastWriteRaw(' ');
        this.writeName(wName);
        this.fastWriteRaw('=', '\"');
        if (n2 > 0) {
            this.writeAttrValue(array, n, n2);
        }
        this.fastWriteRaw('\"');
    }
    
    @Override
    public int writeCData(final String s) {
        this.writeCDataStart();
        int i = s.length();
        int srcBegin = 0;
        while (i > 0) {
            final char[] copyBuffer = super._copyBuffer;
            int length;
            if (i < (length = copyBuffer.length)) {
                length = i;
            }
            final int srcEnd = srcBegin + length;
            s.getChars(srcBegin, srcEnd, copyBuffer, 0);
            final int writeCDataContents = this.writeCDataContents(copyBuffer, 0, length);
            if (writeCDataContents >= 0) {
                return srcBegin + writeCDataContents;
            }
            i -= length;
            srcBegin = srcEnd;
        }
        this.writeCDataEnd();
        return -1;
    }
    
    @Override
    public int writeCData(final char[] array, final int n, final int n2) {
        this.writeCDataStart();
        if (this.writeCDataContents(array, n, n2) < 0) {
            this.writeCDataEnd();
        }
        return -1;
    }
    
    public final void writeCDataEnd() {
        this.fastWriteRaw("]]>");
    }
    
    public final void writeCDataStart() {
        this.fastWriteRaw("<![CDATA[");
    }
    
    @Override
    public void writeCharacters(final String s) {
        if (this._out == null) {
            return;
        }
        int i = s.length();
        int srcBegin = 0;
        while (i > 0) {
            final char[] copyBuffer = super._copyBuffer;
            int length;
            if (i < (length = copyBuffer.length)) {
                length = i;
            }
            final int srcEnd = srcBegin + length;
            s.getChars(srcBegin, srcEnd, copyBuffer, 0);
            this.writeCharacters(copyBuffer, 0, length);
            i -= length;
            srcBegin = srcEnd;
        }
    }
    
    @Override
    public void writeCharacters(final char[] array, int i, int n) {
        if (this._out == null) {
            return;
        }
        final int n2 = n + i;
    Label_0013:
        while (i < n2) {
            final int[] text_CHARS = this.mCharTypes.TEXT_CHARS;
            final int mTableCheckEnd = this.mTableCheckEnd;
            do {
                final char c = array[i];
                if (c < mTableCheckEnd) {
                    if (text_CHARS[c] == 0) {
                        if (this._outputPtr >= this._outputBufferLen) {
                            this.flushBuffer();
                        }
                        final char[] outputBuffer = this._outputBuffer;
                        n = this._outputPtr++;
                        outputBuffer[n] = c;
                        n = i + 1;
                        continue;
                    }
                }
                ++i;
                Label_0173: {
                    Label_0164: {
                        if (c < mTableCheckEnd) {
                            n = text_CHARS[c];
                            if (n != 1) {
                                if (n != 2) {
                                    if (n == 4) {
                                        break Label_0164;
                                    }
                                    switch (n) {
                                        default: {
                                            break Label_0173;
                                        }
                                        case 11: {
                                            break;
                                        }
                                        case 9:
                                        case 10: {
                                            break Label_0164;
                                        }
                                    }
                                }
                                else {
                                    if (super._config.willEscapeCR()) {
                                        break Label_0164;
                                    }
                                    break Label_0173;
                                }
                            }
                            else {
                                this.reportInvalidChar(c);
                            }
                            if (i < n2 && array[i] != '>') {
                                break Label_0173;
                            }
                        }
                        else if (c < this.mEncHighChar) {
                            break Label_0173;
                        }
                    }
                    this.writeAsEntity(c);
                    continue Label_0013;
                }
                if (this._outputPtr >= this._outputBufferLen) {
                    this.flushBuffer();
                }
                final char[] outputBuffer2 = this._outputBuffer;
                n = this._outputPtr++;
                outputBuffer2[n] = c;
                continue Label_0013;
            } while ((i = n) < n2);
            break;
        }
    }
    
    @Override
    public int writeComment(final String s) {
        if (this._out == null) {
            return -1;
        }
        this.writeCommentStart();
        int i = s.length();
        int writeCommentContents = -1;
        int srcBegin = 0;
        while (i > 0) {
            final char[] copyBuffer = super._copyBuffer;
            int length;
            if ((length = copyBuffer.length) > i) {
                length = i;
            }
            final int srcEnd = srcBegin + length;
            s.getChars(srcBegin, srcEnd, copyBuffer, 0);
            writeCommentContents = this.writeCommentContents(copyBuffer, 0, length, false);
            if (writeCommentContents >= 0) {
                break;
            }
            i -= length;
            srcBegin = srcEnd;
        }
        if (writeCommentContents >= 0) {
            return srcBegin + writeCommentContents;
        }
        this.writeCommentEnd();
        return -1;
    }
    
    public final void writeCommentEnd() {
        this.fastWriteRaw("-->");
    }
    
    public final void writeCommentStart() {
        this.fastWriteRaw("<!--");
    }
    
    @Override
    public void writeDTD(final WName wName, final String s, final String s2, final String s3) {
        this.fastWriteRaw("<!DOCTYPE ");
        this.writeName(wName);
        if (s != null) {
            String s4;
            if (s2 != null) {
                this.fastWriteRaw(" PUBLIC \"");
                this.fastWriteRaw(s2);
                s4 = "\" \"";
            }
            else {
                s4 = " SYSTEM \"";
            }
            this.fastWriteRaw(s4);
            this.fastWriteRaw(s);
            this.fastWriteRaw('\"');
        }
        if (s3 != null && s3.length() > 0) {
            this.fastWriteRaw(' ', '[');
            this.fastWriteRaw(s3);
            this.fastWriteRaw(']');
        }
        this.fastWriteRaw('>');
    }
    
    @Override
    public void writeDTD(final String s) {
        this.writeRaw(s, 0, s.length());
    }
    
    @Override
    public void writeEndTag(final WName wName) {
        final int outputPtr = this._outputPtr;
        final int serializedLength = wName.serializedLength();
        int outputPtr2 = outputPtr;
        if (outputPtr + serializedLength + 3 > this._outputBufferLen) {
            this.flushBuffer();
            if (serializedLength + 3 > this._outputBufferLen) {
                this._out.write(60);
                this._out.write(47);
                wName.writeChars(this._out);
                this._outputBuffer[this._outputPtr++] = '>';
                return;
            }
            outputPtr2 = this._outputPtr;
        }
        final char[] outputBuffer = this._outputBuffer;
        final int n = outputPtr2 + 1;
        outputBuffer[outputPtr2] = '<';
        final int n2 = n + 1;
        outputBuffer[n] = '/';
        wName.appendChars(outputBuffer, n2);
        final int n3 = n2 + serializedLength;
        outputBuffer[n3] = '>';
        this._outputPtr = n3 + 1;
    }
    
    @Override
    public void writeEntityReference(final WName wName) {
        this.fastWriteRaw('&');
        this.writeName(wName);
        this.fastWriteRaw(';');
    }
    
    public final void writeName(final WName wName) {
        final int outputPtr = this._outputPtr;
        final int serializedLength = wName.serializedLength();
        int outputPtr2 = outputPtr;
        if (outputPtr + serializedLength > this._outputBufferLen) {
            this.flushBuffer();
            if (serializedLength >= this._outputBufferLen) {
                wName.writeChars(this._out);
                return;
            }
            outputPtr2 = this._outputPtr;
        }
        wName.appendChars(this._outputBuffer, outputPtr2);
        this._outputPtr += serializedLength;
    }
    
    @Override
    public int writePI(final WName wName, final String s) {
        this.fastWriteRaw('<', '?');
        this.writeName(wName);
        if (s != null && s.length() > 0) {
            int i = s.length();
            this.fastWriteRaw(' ');
            int writePIContents = -1;
            int srcBegin = 0;
            while (i > 0) {
                final char[] copyBuffer = super._copyBuffer;
                int length;
                if ((length = copyBuffer.length) > i) {
                    length = i;
                }
                final int srcEnd = srcBegin + length;
                s.getChars(srcBegin, srcEnd, copyBuffer, 0);
                writePIContents = this.writePIContents(copyBuffer, 0, length);
                if (writePIContents >= 0) {
                    break;
                }
                i -= length;
                srcBegin = srcEnd;
            }
            if (writePIContents >= 0) {
                return srcBegin + writePIContents;
            }
        }
        this.fastWriteRaw('?', '>');
        return -1;
    }
    
    @Override
    public void writeRaw(final String str, final int n, final int n2) {
        if (this._out == null) {
            return;
        }
        final int mSmallWriteSize = this.mSmallWriteSize;
        if (n2 < mSmallWriteSize) {
            if (this._outputPtr + n2 >= this._outputBufferLen) {
                this.flushBuffer();
            }
            str.getChars(n, n + n2, this._outputBuffer, this._outputPtr);
            this._outputPtr += n2;
            return;
        }
        final int outputPtr = this._outputPtr;
        int n3 = n;
        int len = n2;
        if (outputPtr > 0) {
            n3 = n;
            len = n2;
            if (outputPtr < mSmallWriteSize) {
                final int n4 = mSmallWriteSize - outputPtr;
                n3 = n + n4;
                str.getChars(n, n3, this._outputBuffer, outputPtr);
                this._outputPtr = outputPtr + n4;
                len = n2 - n4;
            }
            this.flushBuffer();
        }
        this._out.write(str, n3, len);
    }
    
    @Override
    public void writeRaw(final char[] array, final int n, final int n2) {
        if (this._out == null) {
            return;
        }
        final int mSmallWriteSize = this.mSmallWriteSize;
        if (n2 < mSmallWriteSize) {
            if (this._outputPtr + n2 > this._outputBufferLen) {
                this.flushBuffer();
            }
            System.arraycopy(array, n, this._outputBuffer, this._outputPtr, n2);
            this._outputPtr += n2;
            return;
        }
        final int outputPtr = this._outputPtr;
        int n3 = n;
        int n4 = n2;
        if (outputPtr > 0) {
            n3 = n;
            n4 = n2;
            if (outputPtr < mSmallWriteSize) {
                final int n5 = mSmallWriteSize - outputPtr;
                System.arraycopy(array, n, this._outputBuffer, outputPtr, n5);
                this._outputPtr = outputPtr + n5;
                n4 = n2 - n5;
                n3 = n + n5;
            }
            this.flushBuffer();
        }
        this._out.write(array, n3, n4);
    }
    
    public void writeSegmentedCData(final String s, int n) {
        final int n2 = 0;
        int fromIndex = n;
        n = n2;
        while (true) {
            this.fastWriteRaw("<![CDATA[");
            if (fromIndex < 0) {
                break;
            }
            fromIndex += 2;
            this.writeRaw(s, n, fromIndex - n);
            this.fastWriteRaw("]]>");
            final int index = s.indexOf("]]>", fromIndex);
            n = fromIndex;
            fromIndex = index;
        }
        this.writeRaw(s, n, s.length() - n);
        this.fastWriteRaw("]]>");
    }
    
    public void writeSegmentedCData(final char[] array, int n, int n2, int verifyCDataContent) {
        final int n3 = n2 + n;
        while (true) {
            this.fastWriteRaw("<![CDATA[");
            if (verifyCDataContent < 0) {
                break;
            }
            n2 = verifyCDataContent + 2;
            this.writeRaw(array, n, n2 - n);
            this.fastWriteRaw("]]>");
            verifyCDataContent = this.verifyCDataContent(array, n2, n3);
            n = n2;
        }
        this.writeRaw(array, n, n3 - n);
        this.fastWriteRaw("]]>");
    }
    
    public void writeSegmentedComment(String s, int fromIndex) {
        final int length = s.length();
        final int index = length - 1;
        int n = 0;
        this.fastWriteRaw("<!--");
        int i = fromIndex;
        if (fromIndex == index) {
            this.writeRaw(s, 0, s.length());
            s = " -->";
        }
        else {
            while (i >= 0) {
                fromIndex = i + 1;
                this.writeRaw(s, n, fromIndex - n);
                this.fastWriteRaw(' ');
                i = s.indexOf("--", fromIndex);
                n = fromIndex;
            }
            this.writeRaw(s, n, length - n);
            if (s.charAt(index) == '-') {
                this.fastWriteRaw(' ');
            }
            s = "-->";
        }
        this.fastWriteRaw(s);
    }
    
    @Override
    public void writeSpace(final String s) {
        if (this._out == null) {
            return;
        }
        int i = s.length();
        int srcBegin = 0;
        while (i > 0) {
            final char[] copyBuffer = super._copyBuffer;
            int length;
            if (i < (length = copyBuffer.length)) {
                length = i;
            }
            final int srcEnd = srcBegin + length;
            s.getChars(srcBegin, srcEnd, copyBuffer, 0);
            this.writeSpace(copyBuffer, 0, length);
            i -= length;
            srcBegin = srcEnd;
        }
    }
    
    @Override
    public void writeSpace(final char[] array, final int n, final int n2) {
        if (this._out == null) {
            return;
        }
        int n3 = n;
        while (true) {
            final int n4 = n3;
            if (n4 >= n2 + n) {
                break;
            }
            n3 = n4 + 1;
            final char i = array[n4];
            if (i > ' ' && (!super._config.isXml11() || (i != '\u0085' && i != '\u2028'))) {
                this.reportNwfContent(ErrorConsts.WERR_SPACE_CONTENT, (int)i, n3 - 1);
            }
            if (this._outputPtr >= this._outputBufferLen) {
                this.flushBuffer();
            }
            this._outputBuffer[this._outputPtr++] = i;
        }
    }
    
    @Override
    public void writeStartTagEmptyEnd() {
        int n;
        if ((n = this._outputPtr) + 2 > this._outputBufferLen) {
            if (this._out == null) {
                return;
            }
            this.flushBuffer();
            n = this._outputPtr;
        }
        final char[] outputBuffer = this._outputBuffer;
        final int n2 = n + 1;
        outputBuffer[n] = '/';
        outputBuffer[n2] = '>';
        this._outputPtr = n2 + 1;
    }
    
    @Override
    public void writeStartTagEnd() {
        this.fastWriteRaw('>');
    }
    
    @Override
    public void writeStartTagStart(final WName wName) {
        final int outputPtr = this._outputPtr;
        final int serializedLength = wName.serializedLength();
        int outputPtr2 = outputPtr;
        if (outputPtr + serializedLength + 1 > this._outputBufferLen) {
            if (this._out == null) {
                return;
            }
            this.flushBuffer();
            if (serializedLength >= this._outputBufferLen) {
                this._out.write(60);
                wName.writeChars(this._out);
                return;
            }
            outputPtr2 = this._outputPtr;
        }
        final char[] outputBuffer = this._outputBuffer;
        final int n = outputPtr2 + 1;
        outputBuffer[outputPtr2] = '<';
        wName.appendChars(outputBuffer, n);
        this._outputPtr = n + serializedLength;
    }
    
    @Override
    public void writeTypedValue(final AsciiValueEncoder asciiValueEncoder) {
        if (asciiValueEncoder.bufferNeedsFlush(this._outputBufferLen - this._outputPtr)) {
            this.flush();
        }
        while (true) {
            this._outputPtr = asciiValueEncoder.encodeMore(this._outputBuffer, this._outputPtr, this._outputBufferLen);
            if (asciiValueEncoder.isCompleted()) {
                break;
            }
            this.flushBuffer();
        }
    }
    
    @Override
    public void writeXmlDeclaration(final String s, final String s2, final String s3) {
        this.fastWriteRaw("<?xml version='");
        this.fastWriteRaw(s);
        this.fastWriteRaw('\'');
        if (s2 != null && s2.length() > 0) {
            this.fastWriteRaw(" encoding='");
            this.fastWriteRaw(s2);
            this.fastWriteRaw('\'');
        }
        if (s3 != null) {
            this.fastWriteRaw(" standalone='");
            this.fastWriteRaw(s3);
            this.fastWriteRaw('\'');
        }
        this.fastWriteRaw('?', '>');
    }
}

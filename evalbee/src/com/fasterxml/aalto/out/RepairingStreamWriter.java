// 
// Decompiled by Procyon v0.6.0
// 

package com.fasterxml.aalto.out;

import org.codehaus.stax2.ri.typed.AsciiValueEncoder;
import com.fasterxml.aalto.impl.ErrorConsts;
import org.codehaus.stax2.validation.XMLValidator;
import java.util.Iterator;
import java.util.Map;
import javax.xml.namespace.QName;
import java.util.HashMap;

public final class RepairingStreamWriter extends StreamWriterBase
{
    int[] _autoNsSeq;
    final String _cfgAutomaticNsPrefix;
    String _suggestedDefNs;
    HashMap<String, String> _suggestedPrefixes;
    
    public RepairingStreamWriter(final WriterConfig writerConfig, final XmlWriter xmlWriter, final WNameTable wNameTable) {
        super(writerConfig, xmlWriter, wNameTable);
        this._autoNsSeq = null;
        this._suggestedDefNs = null;
        this._suggestedPrefixes = null;
        this._cfgAutomaticNsPrefix = writerConfig.getAutomaticNsPrefix();
    }
    
    private final boolean _writeStartAndVerify(final String s, final String s2, final String s3, final boolean b) {
        if (s != null && s.length() != 0) {
            this._verifyStartElement(s, s2);
            this._writeStartTag(super._symbols.findSymbol(s, s2), b, s3);
            return false;
        }
        this._verifyStartElement(null, s2);
        this._writeStartTag(super._symbols.findSymbol(s2), b, s3);
        return true;
    }
    
    public WName _generateAttrName(String explicitPrefix, final String s, final String s2) {
        while (true) {
            Label_0081: {
                if (explicitPrefix == null || explicitPrefix.length() <= 0) {
                    break Label_0081;
                }
                final int n = RepairingStreamWriter$1.$SwitchMap$com$fasterxml$aalto$out$OutputElement$PrefixState[super._currElem.checkPrefixValidity(explicitPrefix, s2, super._rootNsContext).ordinal()];
                final String generatePrefix = explicitPrefix;
                if (n != 1) {
                    if (n != 2) {
                        break Label_0081;
                    }
                    return super._symbols.findSymbol(explicitPrefix, s);
                }
                this._writeNamespace(generatePrefix, s2);
                super._currElem.addPrefix(generatePrefix, s2);
                explicitPrefix = generatePrefix;
                return super._symbols.findSymbol(explicitPrefix, s);
            }
            if ((explicitPrefix = super._currElem.getExplicitPrefix(s2, super._rootNsContext)) == null) {
                if (this._autoNsSeq == null) {
                    (this._autoNsSeq = new int[] { 0 })[0] = 1;
                }
                final String generatePrefix = super._currElem.generatePrefix(super._rootNsContext, this._cfgAutomaticNsPrefix, this._autoNsSeq);
                continue;
            }
            break;
        }
        return super._symbols.findSymbol(explicitPrefix, s);
    }
    
    public final String _generateElemPrefix(String s) {
        final String suggestedDefNs = this._suggestedDefNs;
        if (suggestedDefNs != null && suggestedDefNs.equals(s)) {
            return null;
        }
        final HashMap<String, String> suggestedPrefixes = this._suggestedPrefixes;
        if (suggestedPrefixes != null) {
            s = suggestedPrefixes.get(s);
            if (s != null) {
                return s;
            }
        }
        if (this._autoNsSeq == null) {
            (this._autoNsSeq = new int[] { 0 })[0] = 1;
        }
        return super._currElem.generatePrefix(super._rootNsContext, this._cfgAutomaticNsPrefix, this._autoNsSeq);
    }
    
    @Override
    public String _serializeQName(final QName qName) {
        final String namespaceURI = qName.getNamespaceURI();
        final String prefix = qName.getPrefix();
        final String localPart = qName.getLocalPart();
        if (super._currElem.isPrefixBoundTo(prefix, namespaceURI, super._rootNsContext)) {
            String string = localPart;
            if (prefix != null) {
                if (prefix.length() == 0) {
                    string = localPart;
                }
                else {
                    final StringBuilder sb = new StringBuilder();
                    sb.append(prefix);
                    sb.append(":");
                    sb.append(localPart);
                    string = sb.toString();
                }
            }
            return string;
        }
        return this._generateAttrName(prefix, localPart, namespaceURI).getPrefixedName();
    }
    
    @Override
    public void _setPrefix(final String s, final String key) {
        if (key != null && key.length() != 0) {
            if (this._suggestedPrefixes == null) {
                this._suggestedPrefixes = new HashMap<String, String>(16);
            }
            this._suggestedPrefixes.put(key, s);
        }
        else {
            final HashMap<String, String> suggestedPrefixes = this._suggestedPrefixes;
            if (suggestedPrefixes != null) {
                final Iterator<Map.Entry<String, String>> iterator = suggestedPrefixes.entrySet().iterator();
                while (iterator.hasNext()) {
                    if (((Map.Entry<K, String>)iterator.next()).getValue().equals(s)) {
                        iterator.remove();
                    }
                }
            }
        }
    }
    
    public void _writeStartOrEmpty(String s, final String s2, final String defaultNsURI, final boolean b) {
        if (defaultNsURI != null && defaultNsURI.length() != 0) {
            String s3 = null;
            Label_0148: {
                Label_0087: {
                    if (s == null) {
                        s3 = super._currElem.getPrefix(defaultNsURI);
                        if (s3 != null) {
                            this._writeStartAndVerify(s3, s2, defaultNsURI, b);
                            break Label_0148;
                        }
                        s = (s3 = this._generateElemPrefix(defaultNsURI));
                        if (!this._writeStartAndVerify(s, s2, defaultNsURI, b)) {
                            break Label_0087;
                        }
                    }
                    else {
                        final boolean writeStartAndVerify = this._writeStartAndVerify(s, s2, defaultNsURI, b);
                        s3 = s;
                        if (super._currElem.isPrefixBoundTo(s, defaultNsURI, super._rootNsContext)) {
                            break Label_0148;
                        }
                        s3 = s;
                        if (!writeStartAndVerify) {
                            break Label_0087;
                        }
                    }
                    this._writeDefaultNamespace(defaultNsURI);
                    super._currElem.setDefaultNsURI(defaultNsURI);
                    s3 = s;
                    break Label_0148;
                }
                this._writeNamespace(s3, defaultNsURI);
                super._currElem.addPrefix(s3, defaultNsURI);
            }
            final XMLValidator validator = super._validator;
            if (validator != null) {
                if ((s = s3) == null) {
                    s = "";
                }
                validator.validateElementStart(s2, "", s);
            }
            return;
        }
        this._verifyStartElement(null, s2);
        this._writeStartTag(super._symbols.findSymbol(s2), b, null);
        if (!super._currElem.hasEmptyDefaultNs()) {
            this._writeDefaultNamespace(defaultNsURI);
            super._currElem.setDefaultNsURI("");
        }
        final XMLValidator validator2 = super._validator;
        if (validator2 != null) {
            validator2.validateElementStart(s2, "", "");
        }
    }
    
    @Override
    public void setDefaultNamespace(final String s) {
        String suggestedDefNs = null;
        Label_0015: {
            if (s != null) {
                suggestedDefNs = s;
                if (s.length() != 0) {
                    break Label_0015;
                }
            }
            suggestedDefNs = null;
        }
        this._suggestedDefNs = suggestedDefNs;
    }
    
    @Override
    public void writeAttribute(final String s, final String s2, final String s3) {
        if (!super._stateStartElementOpen) {
            StreamWriterBase.throwOutputError(ErrorConsts.WERR_ATTR_NO_ELEM);
        }
        WName wName;
        if (s != null && s.length() != 0) {
            wName = this._generateAttrName(null, s2, s);
        }
        else {
            wName = super._symbols.findSymbol(s2);
        }
        this._writeAttribute(wName, s3);
    }
    
    @Override
    public void writeAttribute(final String s, final String s2, final String s3, final String s4) {
        if (!super._stateStartElementOpen) {
            StreamWriterBase.throwOutputError(ErrorConsts.WERR_ATTR_NO_ELEM);
        }
        WName wName;
        if (s2 != null && s2.length() != 0) {
            wName = this._generateAttrName(s, s3, s2);
        }
        else {
            wName = super._symbols.findSymbol(s3);
        }
        this._writeAttribute(wName, s4);
    }
    
    @Override
    public void writeDefaultNamespace(final String defaultNsURI) {
        if (!super._stateStartElementOpen) {
            StreamWriterBase.throwOutputError(ErrorConsts.WERR_NS_NO_ELEM);
        }
        if (!super._currElem.hasPrefix()) {
            super._currElem.setDefaultNsURI(defaultNsURI);
            this._writeDefaultNamespace(defaultNsURI);
        }
        this._writeDefaultNamespace(defaultNsURI);
    }
    
    @Override
    public void writeEmptyElement(final String s, final String s2) {
        this._writeStartOrEmpty(null, s2, s, true);
    }
    
    @Override
    public void writeEmptyElement(final String s, final String s2, final String s3) {
        this._writeStartOrEmpty(s, s2, s3, true);
    }
    
    @Override
    public void writeNamespace(final String s, final String s2) {
        if (s != null && s.length() != 0) {
            if (!super._stateStartElementOpen) {
                StreamWriterBase.throwOutputError(ErrorConsts.WERR_NS_NO_ELEM);
            }
            if (super._currElem.isPrefixUnbound(s, super._rootNsContext)) {
                super._currElem.addPrefix(s, s2);
                this._writeNamespace(s, s2);
            }
            return;
        }
        this.writeDefaultNamespace(s2);
    }
    
    @Override
    public void writeStartElement(final String s, final String s2) {
        this._writeStartOrEmpty(null, s2, s, false);
    }
    
    @Override
    public void writeStartElement(final String s, final String s2, final String s3) {
        this._writeStartOrEmpty(s, s2, s3, false);
    }
    
    @Override
    public void writeTypedAttribute(final String s, final String s2, final String s3, final AsciiValueEncoder asciiValueEncoder) {
        if (!super._stateStartElementOpen) {
            StreamWriterBase.throwOutputError(ErrorConsts.WERR_ATTR_NO_ELEM);
        }
        WName wName;
        if (s != null && s.length() != 0) {
            wName = super._symbols.findSymbol(s, s3);
        }
        else {
            wName = super._symbols.findSymbol(s3);
        }
        this._writeAttribute(wName, asciiValueEncoder);
    }
}

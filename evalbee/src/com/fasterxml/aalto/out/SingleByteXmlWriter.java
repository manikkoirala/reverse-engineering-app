// 
// Decompiled by Procyon v0.6.0
// 

package com.fasterxml.aalto.out;

import com.fasterxml.aalto.util.XmlCharTypes;
import java.io.OutputStream;

public abstract class SingleByteXmlWriter extends ByteXmlWriter
{
    public SingleByteXmlWriter(final WriterConfig writerConfig, final OutputStream outputStream, final XmlCharTypes xmlCharTypes) {
        super(writerConfig, outputStream, xmlCharTypes);
    }
    
    @Override
    public abstract int getHighestEncodable();
    
    @Override
    public final void output2ByteChar(final int n) {
        this.reportFailedEscaping("content", n);
    }
    
    @Override
    public final int outputMultiByteChar(final int surrogate, final char[] array, int n, final int n2) {
        if (surrogate >= 55296) {
            if (surrogate <= 57343) {
                if (n >= n2) {
                    super._surrogate = surrogate;
                }
                else {
                    this.outputSurrogates(surrogate, array[n]);
                    ++n;
                }
                return n;
            }
            if (surrogate >= 65534) {
                this.reportInvalidChar(surrogate);
            }
        }
        this.writeAsEntity(surrogate);
        return n;
    }
    
    @Override
    public final int outputStrictMultiByteChar(final int n, final char[] array, final int n2, final int n3) {
        this.reportFailedEscaping("content", n);
        return 0;
    }
    
    @Override
    public final void outputSurrogates(final int n, final int n2) {
        this.writeAsEntity(this.calcSurrogate(n, n2, " in content"));
    }
    
    @Override
    public abstract void writeRaw(final char[] p0, final int p1, final int p2);
}

// 
// Decompiled by Procyon v0.6.0
// 

package com.fasterxml.aalto.out;

import java.io.IOException;
import com.fasterxml.aalto.impl.IoStreamException;
import java.io.OutputStream;

public final class Utf8XmlWriter extends ByteXmlWriter
{
    public Utf8XmlWriter(final WriterConfig writerConfig, final OutputStream outputStream) {
        super(writerConfig, outputStream, OutputCharTypes.getUtf8CharTypes());
    }
    
    @Override
    public WName doConstructName(final String s) {
        try {
            return new ByteWName(s, s.getBytes("UTF-8"));
        }
        catch (final IOException ex) {
            throw new IoStreamException(ex);
        }
    }
    
    @Override
    public WName doConstructName(final String str, final String str2) {
        try {
            final StringBuilder sb = new StringBuilder();
            sb.append(str);
            sb.append(":");
            sb.append(str2);
            return new ByteWName(str, str2, sb.toString().getBytes("UTF-8"));
        }
        catch (final IOException ex) {
            throw new IoStreamException(ex);
        }
    }
    
    @Override
    public int getHighestEncodable() {
        return 1114111;
    }
    
    @Override
    public final void output2ByteChar(final int n) {
        if (super._outputPtr + 2 > super._outputBufferLen) {
            this.flushBuffer();
        }
        final byte[] outputBuffer = super._outputBuffer;
        final int outputPtr = super._outputPtr;
        final int n2 = outputPtr + 1;
        outputBuffer[outputPtr] = (byte)(n >> 6 | 0xC0);
        super._outputPtr = n2 + 1;
        outputBuffer[n2] = (byte)((n & 0x3F) | 0x80);
    }
    
    @Override
    public final int outputMultiByteChar(final int surrogate, final char[] array, int n, int n2) {
        if (surrogate >= 55296) {
            if (surrogate <= 57343) {
                if (n >= n2) {
                    super._surrogate = surrogate;
                }
                else {
                    this.outputSurrogates(surrogate, array[n]);
                    ++n;
                }
                return n;
            }
            if (surrogate >= 65534) {
                this.reportInvalidChar(surrogate);
            }
        }
        if (super._outputPtr + 3 > super._outputBufferLen) {
            this.flushBuffer();
        }
        final byte[] outputBuffer = super._outputBuffer;
        final int outputPtr = super._outputPtr;
        n2 = outputPtr + 1;
        outputBuffer[outputPtr] = (byte)(surrogate >> 12 | 0xE0);
        final int n3 = n2 + 1;
        outputBuffer[n2] = (byte)((surrogate >> 6 & 0x3F) | 0x80);
        super._outputPtr = n3 + 1;
        outputBuffer[n3] = (byte)((surrogate & 0x3F) | 0x80);
        return n;
    }
    
    @Override
    public final int outputStrictMultiByteChar(final int surrogate, final char[] array, int n, int n2) {
        if (surrogate >= 55296) {
            if (surrogate <= 57343) {
                if (n >= n2) {
                    super._surrogate = surrogate;
                }
                else {
                    this.outputSurrogates(surrogate, array[n]);
                    ++n;
                }
                return n;
            }
            if (surrogate >= 65534) {
                this.reportInvalidChar(surrogate);
            }
        }
        if (super._outputPtr + 3 > super._outputBufferLen) {
            this.flushBuffer();
        }
        final byte[] outputBuffer = super._outputBuffer;
        final int outputPtr = super._outputPtr;
        n2 = outputPtr + 1;
        outputBuffer[outputPtr] = (byte)(surrogate >> 12 | 0xE0);
        final int n3 = n2 + 1;
        outputBuffer[n2] = (byte)((surrogate >> 6 & 0x3F) | 0x80);
        super._outputPtr = n3 + 1;
        outputBuffer[n3] = (byte)((surrogate & 0x3F) | 0x80);
        return n;
    }
    
    @Override
    public final void outputSurrogates(int calcSurrogate, int outputPtr) {
        calcSurrogate = this.calcSurrogate(calcSurrogate, outputPtr, " in content");
        if (super._outputPtr + 4 > super._outputBufferLen) {
            this.flushBuffer();
        }
        final byte[] outputBuffer = super._outputBuffer;
        outputPtr = super._outputPtr;
        final int n = outputPtr + 1;
        outputBuffer[outputPtr] = (byte)(calcSurrogate >> 18 | 0xF0);
        outputPtr = n + 1;
        outputBuffer[n] = (byte)((calcSurrogate >> 12 & 0x3F) | 0x80);
        final int n2 = outputPtr + 1;
        outputBuffer[outputPtr] = (byte)((calcSurrogate >> 6 & 0x3F) | 0x80);
        super._outputPtr = n2 + 1;
        outputBuffer[n2] = (byte)((calcSurrogate & 0x3F) | 0x80);
    }
    
    @Override
    public void writeRaw(final char[] array, int i, int n) {
        if (super._out != null) {
            if (n != 0) {
                final int surrogate = super._surrogate;
                int n2 = i;
                int n3 = n;
                if (surrogate != 0) {
                    this.outputSurrogates(surrogate, array[i]);
                    n2 = i + 1;
                    n3 = n - 1;
                }
                final int n4 = n3 + n2;
                i = n2;
            Label_0060:
                while (i < n4) {
                    do {
                        n = array[i];
                        if (n >= 128) {
                            ++i;
                            if (n < 2048) {
                                this.output2ByteChar(n);
                                continue Label_0060;
                            }
                            i = this.outputMultiByteChar(n, array, i, n4);
                            continue Label_0060;
                        }
                        else {
                            if (super._outputPtr >= super._outputBufferLen) {
                                this.flushBuffer();
                            }
                            super._outputBuffer[super._outputPtr++] = (byte)n;
                            n = i + 1;
                        }
                    } while ((i = n) < n4);
                    break;
                }
            }
        }
    }
}

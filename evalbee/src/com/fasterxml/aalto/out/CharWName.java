// 
// Decompiled by Procyon v0.6.0
// 

package com.fasterxml.aalto.out;

import java.io.Writer;
import java.io.OutputStream;

final class CharWName extends WName
{
    final char[] _chars;
    
    public CharWName(final String s) {
        super(s);
        this._chars = s.toCharArray();
    }
    
    public CharWName(final String s, final String s2) {
        super(s, s2);
        final int length = s.length();
        final int length2 = s2.length();
        final int dstBegin = length + 1;
        final char[] array = new char[dstBegin + length2];
        s.getChars(0, length, this._chars = array, 0);
        array[length] = ':';
        s2.getChars(0, length2, array, dstBegin);
    }
    
    @Override
    public int appendBytes(final byte[] array, final int n) {
        throw new RuntimeException("Internal error: appendBytes() should never be called");
    }
    
    @Override
    public int appendChars(final char[] array, final int n) {
        final char[] chars = this._chars;
        final int length = chars.length;
        System.arraycopy(chars, 0, array, n, length);
        return length;
    }
    
    @Override
    public final int serializedLength() {
        return this._chars.length;
    }
    
    @Override
    public String toString() {
        if (super._prefix != null) {
            return new String(this._chars);
        }
        return super._localName;
    }
    
    @Override
    public void writeBytes(final OutputStream outputStream) {
        throw new RuntimeException("Internal error: writeBytes() should never be called");
    }
    
    @Override
    public void writeChars(final Writer writer) {
        writer.write(this._chars);
    }
}

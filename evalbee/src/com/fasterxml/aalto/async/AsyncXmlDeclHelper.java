// 
// Decompiled by Procyon v0.6.0
// 

package com.fasterxml.aalto.async;

import com.fasterxml.aalto.in.PName;
import com.fasterxml.aalto.in.PName1;
import com.fasterxml.aalto.in.PName3;
import com.fasterxml.aalto.in.PName2;

public class AsyncXmlDeclHelper
{
    private static final PName2 NAME_ENCODING;
    private static final PName3 NAME_STANDALONE;
    private static final PName2 NAME_VERSION;
    private static final PName1 NAME_XML;
    private static final int QUAD_ENCODING1;
    private static final int QUAD_ENCODING2;
    private static final int QUAD_STANDALONE1;
    private static final int QUAD_STANDALONE2;
    private static final int QUAD_STANDALONE3;
    private static final int QUAD_VERSION1;
    private static final int QUAD_VERSION2;
    private static final int QUAD_XML;
    
    static {
        final int n = QUAD_XML = asciiQuads("xml")[0];
        final int[] asciiQuads = asciiQuads("version");
        final int n2 = QUAD_VERSION1 = asciiQuads[0];
        final int n3 = QUAD_VERSION2 = asciiQuads[1];
        final int[] asciiQuads2 = asciiQuads("standalone");
        final int n4 = QUAD_STANDALONE1 = asciiQuads2[0];
        final int n5 = QUAD_STANDALONE2 = asciiQuads2[1];
        final int n6 = QUAD_STANDALONE3 = asciiQuads2[2];
        final int[] asciiQuads3 = asciiQuads("encoding");
        final int n7 = QUAD_ENCODING1 = asciiQuads3[0];
        final int n8 = QUAD_ENCODING2 = asciiQuads3[1];
        NAME_XML = new PName1("xml", null, "xml", 0, n);
        NAME_VERSION = new PName2("version", null, "version", 0, n2, n3);
        NAME_STANDALONE = new PName3("standalone", null, "standalone", 0, n4, n5, n6);
        NAME_ENCODING = new PName2("encoding", null, "encoding", 0, n7, n8);
    }
    
    public static int[] asciiQuads(final String s) {
        final int length = s.length();
        final int[] array = new int[(length + 3) / 4];
        int n;
        for (int i = 0; i < length; i = n + 1) {
            final char char1 = s.charAt(i);
            int index = n = i + 1;
            int n2 = char1;
            if (index < length) {
                final int n3 = char1 << 8 | s.charAt(index);
                n = ++index;
                n2 = n3;
                if (index < length) {
                    final int n4 = n3 << 8 | s.charAt(index);
                    n = ++index;
                    n2 = n4;
                    if (index < length) {
                        n2 = (n4 << 8 | s.charAt(index));
                        n = index;
                    }
                }
            }
            array[n / 4] = n2;
        }
        return array;
    }
    
    public static PName find(final int n) {
        if (n == AsyncXmlDeclHelper.QUAD_XML) {
            return AsyncXmlDeclHelper.NAME_XML;
        }
        return null;
    }
    
    public static PName find(final int n, final int n2) {
        if (n == AsyncXmlDeclHelper.QUAD_VERSION1) {
            if (n2 == AsyncXmlDeclHelper.QUAD_VERSION2) {
                return AsyncXmlDeclHelper.NAME_VERSION;
            }
        }
        else if (n == AsyncXmlDeclHelper.QUAD_ENCODING1 && n2 == AsyncXmlDeclHelper.QUAD_ENCODING2) {
            return AsyncXmlDeclHelper.NAME_ENCODING;
        }
        return null;
    }
    
    public static PName find(final int n, final int n2, final int n3) {
        if (n == AsyncXmlDeclHelper.QUAD_STANDALONE1 && n2 == AsyncXmlDeclHelper.QUAD_STANDALONE2 && n3 == AsyncXmlDeclHelper.QUAD_STANDALONE3) {
            return AsyncXmlDeclHelper.NAME_STANDALONE;
        }
        return null;
    }
}

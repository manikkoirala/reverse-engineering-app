// 
// Decompiled by Procyon v0.6.0
// 

package com.fasterxml.aalto.async;

import com.fasterxml.aalto.util.TextBuilder;
import com.fasterxml.aalto.in.EntityNames;
import com.fasterxml.aalto.in.NsDeclaration;
import javax.xml.stream.XMLStreamException;
import com.fasterxml.aalto.in.ElementScope;
import com.fasterxml.aalto.util.DataUtil;
import com.fasterxml.aalto.in.PName;
import com.fasterxml.aalto.in.ReaderConfig;
import java.nio.ByteBuffer;
import com.fasterxml.aalto.AsyncByteBufferFeeder;

public class AsyncByteBufferScanner extends AsyncByteScanner implements AsyncByteBufferFeeder
{
    protected ByteBuffer _inputBuffer;
    protected int _origBufferLen;
    
    public AsyncByteBufferScanner(final ReaderConfig readerConfig) {
        super(readerConfig);
        super._state = 1;
        super._currToken = 257;
    }
    
    private int finishStartElement(final boolean isEmptyTag) {
        super._isEmptyTag = isEmptyTag;
        int attrCount;
        if ((attrCount = super._attrCollector.finishLastValue(super._elemAttrPtr)) < 0) {
            attrCount = super._attrCollector.getCount();
            this.reportInputProblem(super._attrCollector.getErrorMsg());
        }
        super._attrCount = attrCount;
        ++super._depth;
        if (!super._elemAllNsBound) {
            final boolean bound = super._tokenName.isBound();
            int i = 0;
            if (!bound) {
                this.reportUnboundPrefix(super._tokenName, false);
            }
            while (i < super._attrCount) {
                final PName name = super._attrCollector.getName(i);
                if (!name.isBound()) {
                    this.reportUnboundPrefix(name, true);
                }
                ++i;
            }
        }
        return super._currToken = 1;
    }
    
    private final boolean handleAndAppendPending() {
        final int inputPtr = super._inputPtr;
        if (inputPtr >= super._inputEnd) {
            return false;
        }
        final int pendingInput = super._pendingInput;
        super._pendingInput = 0;
        if (pendingInput < 0) {
            if (pendingInput == -1) {
                if (this._inputBuffer.get(inputPtr) == 10) {
                    ++super._inputPtr;
                }
                this.markLF();
                super._textBuilder.append('\n');
                return true;
            }
            this.throwInternal();
        }
        final int[] text_CHARS = super._charTypes.TEXT_CHARS;
        final int n = pendingInput & 0xFF;
        final int n2 = text_CHARS[n];
        if (n2 != 5) {
            int pendingInput2 = 0;
            Label_0184: {
                int n11 = 0;
                Label_0180: {
                    int n3 = 0;
                    Label_0175: {
                        if (n2 == 6) {
                            n3 = (this._inputBuffer.get(super._inputPtr++) & 0xFF);
                            final int n4 = pendingInput >> 8;
                            int n5;
                            if (n4 == 0) {
                                final int inputPtr2 = super._inputPtr;
                                if (inputPtr2 >= super._inputEnd) {
                                    break Label_0175;
                                }
                                final ByteBuffer inputBuffer = this._inputBuffer;
                                super._inputPtr = inputPtr2 + 1;
                                n5 = this.decodeUtf8_3(pendingInput, n3, inputBuffer.get(inputPtr2) & 0xFF);
                            }
                            else {
                                n5 = this.decodeUtf8_3(n, n4, n3);
                            }
                            super._textBuilder.append((char)n5);
                            return true;
                        }
                        if (n2 != 7) {
                            this.throwInternal();
                            return true;
                        }
                        n3 = (this._inputBuffer.get(super._inputPtr++) & 0xFF);
                        final int n6 = pendingInput >> 8;
                        int n8;
                        if (n6 == 0) {
                            final int inputPtr3 = super._inputPtr;
                            if (inputPtr3 >= super._inputEnd) {
                                break Label_0175;
                            }
                            final ByteBuffer inputBuffer2 = this._inputBuffer;
                            super._inputPtr = inputPtr3 + 1;
                            final int n7 = inputBuffer2.get(inputPtr3) & 0xFF;
                            final int inputPtr4 = super._inputPtr;
                            if (inputPtr4 >= super._inputEnd) {
                                pendingInput2 = (n3 << 8 | pendingInput | n7 << 16);
                                break Label_0184;
                            }
                            final ByteBuffer inputBuffer3 = this._inputBuffer;
                            super._inputPtr = inputPtr4 + 1;
                            n8 = this.decodeUtf8_4(pendingInput, n3, n7, inputBuffer3.get(inputPtr4) & 0xFF);
                        }
                        else {
                            final int n9 = n6 & 0xFF;
                            final int n10 = pendingInput >> 16;
                            if (n10 == 0) {
                                final int inputPtr5 = super._inputPtr;
                                if (inputPtr5 >= super._inputEnd) {
                                    n11 = n3 << 16;
                                    break Label_0180;
                                }
                                final ByteBuffer inputBuffer4 = this._inputBuffer;
                                super._inputPtr = inputPtr5 + 1;
                                n8 = this.decodeUtf8_4(n, n9, n3, inputBuffer4.get(inputPtr5) & 0xFF);
                            }
                            else {
                                n8 = this.decodeUtf8_4(n, n9, n10, n3);
                            }
                        }
                        super._textBuilder.appendSurrogate(n8);
                        return true;
                    }
                    n11 = n3 << 8;
                }
                pendingInput2 = (n11 | pendingInput);
            }
            super._pendingInput = pendingInput2;
            return false;
        }
        super._textBuilder.append((char)this.decodeUtf8_2(pendingInput));
        return true;
    }
    
    private final boolean handleAttrValuePending() {
        final int pendingInput = super._pendingInput;
        if (pendingInput == -1) {
            if (!this.handlePartialCR()) {
                return false;
            }
            char[] array;
            if (super._elemAttrPtr >= (array = super._attrCollector.continueValue()).length) {
                array = super._attrCollector.valueBufferFull();
            }
            array[super._elemAttrPtr++] = ' ';
            return true;
        }
        else {
            final int inputPtr = super._inputPtr;
            if (inputPtr >= super._inputEnd) {
                return false;
            }
            int n = 0;
            Label_0338: {
                PName tokenName = null;
                Label_0323: {
                    Label_0280: {
                        if (pendingInput == -60) {
                            final ByteBuffer inputBuffer = this._inputBuffer;
                            super._inputPtr = inputPtr + 1;
                            final byte value = inputBuffer.get(inputPtr);
                            if (value == 35) {
                                super._pendingInput = -61;
                                final int inputPtr2 = super._inputPtr;
                                if (inputPtr2 >= super._inputEnd) {
                                    return false;
                                }
                                if (this._inputBuffer.get(inputPtr2) == 120) {
                                    super._pendingInput = -62;
                                    if (++super._inputPtr >= super._inputEnd) {
                                        return false;
                                    }
                                    break Label_0280;
                                }
                            }
                            else {
                                tokenName = this.parseNewEntityName(value);
                                if (tokenName == null) {
                                    super._pendingInput = -63;
                                    return false;
                                }
                                final int decodeGeneralEntity = this.decodeGeneralEntity(tokenName);
                                if ((n = decodeGeneralEntity) == 0) {
                                    n = decodeGeneralEntity;
                                    break Label_0323;
                                }
                                break Label_0338;
                            }
                        }
                        else if (pendingInput == -61) {
                            if (this._inputBuffer.get(inputPtr) == 120) {
                                super._pendingInput = -62;
                                if (++super._inputPtr >= super._inputEnd) {
                                    return false;
                                }
                                break Label_0280;
                            }
                        }
                        else {
                            if (pendingInput == -62) {
                                break Label_0280;
                            }
                            if (pendingInput == -63) {
                                tokenName = this.parseEntityName();
                                if (tokenName == null) {
                                    return false;
                                }
                                final int decodeGeneralEntity2 = this.decodeGeneralEntity(tokenName);
                                if ((n = decodeGeneralEntity2) == 0) {
                                    n = decodeGeneralEntity2;
                                    break Label_0323;
                                }
                                break Label_0338;
                            }
                            else {
                                if (pendingInput == -64) {
                                    n = this.handleDecEntityInAttribute(false);
                                    break Label_0338;
                                }
                                if (pendingInput == -65) {
                                    n = this.handleHexEntityInAttribute(false);
                                    break Label_0338;
                                }
                                n = this.handleAttrValuePendingUTF8();
                                break Label_0338;
                            }
                        }
                        n = this.handleDecEntityInAttribute(true);
                        break Label_0338;
                    }
                    n = this.handleHexEntityInAttribute(true);
                    break Label_0338;
                }
                super._tokenName = tokenName;
                this.reportUnexpandedEntityInAttr(super._elemAttrName, false);
            }
            if (n == 0) {
                return false;
            }
            final char[] continueValue = super._attrCollector.continueValue();
            int n2 = n;
            char[] valueBufferFull = continueValue;
            if (n >> 16 != 0) {
                final int n3 = n - 65536;
                valueBufferFull = continueValue;
                if (super._elemAttrPtr >= continueValue.length) {
                    valueBufferFull = super._attrCollector.valueBufferFull();
                }
                valueBufferFull[super._elemAttrPtr++] = (char)(n3 >> 10 | 0xD800);
                n2 = ((n3 & 0x3FF) | 0xDC00);
            }
            char[] valueBufferFull2 = valueBufferFull;
            if (super._elemAttrPtr >= valueBufferFull.length) {
                valueBufferFull2 = super._attrCollector.valueBufferFull();
            }
            valueBufferFull2[super._elemAttrPtr++] = (char)n2;
            return true;
        }
    }
    
    private final int handleAttrValuePendingUTF8() {
        int pendingInput = super._pendingInput;
        super._pendingInput = 0;
        final int[] text_CHARS = super._charTypes.TEXT_CHARS;
        final int n = pendingInput & 0xFF;
        final int n2 = text_CHARS[n];
        if (n2 != 5) {
            int n8 = 0;
            Label_0112: {
                int n3 = 0;
                Label_0107: {
                    if (n2 == 6) {
                        n3 = (this._inputBuffer.get(super._inputPtr++) & 0xFF);
                        final int n4 = pendingInput >> 8;
                        int n5;
                        if (n4 == 0) {
                            final int inputPtr = super._inputPtr;
                            if (inputPtr >= super._inputEnd) {
                                break Label_0107;
                            }
                            final ByteBuffer inputBuffer = this._inputBuffer;
                            super._inputPtr = inputPtr + 1;
                            n5 = this.decodeUtf8_3(pendingInput, n3, inputBuffer.get(inputPtr) & 0xFF);
                        }
                        else {
                            n5 = this.decodeUtf8_3(n, n4, n3);
                        }
                        return n5;
                    }
                    if (n2 != 7) {
                        this.throwInternal();
                        return 0;
                    }
                    n3 = (this._inputBuffer.get(super._inputPtr++) & 0xFF);
                    final int n6 = pendingInput >> 8;
                    int n9;
                    if (n6 == 0) {
                        final int inputPtr2 = super._inputPtr;
                        if (inputPtr2 >= super._inputEnd) {
                            break Label_0107;
                        }
                        final ByteBuffer inputBuffer2 = this._inputBuffer;
                        super._inputPtr = inputPtr2 + 1;
                        final int n7 = inputBuffer2.get(inputPtr2) & 0xFF;
                        final int inputPtr3 = super._inputPtr;
                        if (inputPtr3 >= super._inputEnd) {
                            pendingInput |= n3 << 8;
                            n8 = n7 << 16;
                            break Label_0112;
                        }
                        final ByteBuffer inputBuffer3 = this._inputBuffer;
                        super._inputPtr = inputPtr3 + 1;
                        n9 = this.decodeUtf8_4(pendingInput, n3, n7, inputBuffer3.get(inputPtr3) & 0xFF);
                    }
                    else {
                        final int n10 = n6 & 0xFF;
                        final int n11 = pendingInput >> 16;
                        if (n11 == 0) {
                            final int inputPtr4 = super._inputPtr;
                            if (inputPtr4 >= super._inputEnd) {
                                n8 = n3 << 16;
                                break Label_0112;
                            }
                            final ByteBuffer inputBuffer4 = this._inputBuffer;
                            super._inputPtr = inputPtr4 + 1;
                            n9 = this.decodeUtf8_4(n, n10, n3, inputBuffer4.get(inputPtr4) & 0xFF);
                        }
                        else {
                            n9 = this.decodeUtf8_4(n, n10, n11, n3);
                        }
                    }
                    return n9;
                }
                n8 = n3 << 8;
            }
            super._pendingInput = (pendingInput | n8);
            return 0;
        }
        return this.decodeUtf8_2(pendingInput);
    }
    
    private int handleCData() {
        if (super._state == 1) {
            return this.parseCDataContents();
        }
        final int inputPtr = super._inputPtr;
        if (inputPtr >= super._inputEnd) {
            return 257;
        }
        final ByteBuffer inputBuffer = this._inputBuffer;
        super._inputPtr = inputPtr + 1;
        return this.handleCDataStartMarker(inputBuffer.get(inputPtr));
    }
    
    private int handleCDataStartMarker(byte value) {
        final int state = super._state;
        Label_0344: {
            byte value5 = 0;
            Label_0284: {
                byte value4 = 0;
                Label_0225: {
                    byte value3 = 0;
                    Label_0166: {
                        byte value2;
                        if (state != 0) {
                            value2 = value;
                            if (state != 2) {
                                value3 = value;
                                if (state == 3) {
                                    break Label_0166;
                                }
                                value4 = value;
                                if (state == 4) {
                                    break Label_0225;
                                }
                                value5 = value;
                                if (state == 5) {
                                    break Label_0284;
                                }
                                if (state != 6) {
                                    return this.throwInternal();
                                }
                                break Label_0344;
                            }
                        }
                        else {
                            if (value != 67) {
                                this.reportTreeUnexpChar(this.decodeCharForError(value), " (expected 'C' for CDATA)");
                            }
                            super._state = 2;
                            final int inputPtr = super._inputPtr;
                            if (inputPtr >= super._inputEnd) {
                                return 257;
                            }
                            final ByteBuffer inputBuffer = this._inputBuffer;
                            super._inputPtr = inputPtr + 1;
                            value2 = inputBuffer.get(inputPtr);
                        }
                        if (value2 != 68) {
                            this.reportTreeUnexpChar(this.decodeCharForError(value2), " (expected 'D' for CDATA)");
                        }
                        super._state = 3;
                        final int inputPtr2 = super._inputPtr;
                        if (inputPtr2 >= super._inputEnd) {
                            return 257;
                        }
                        final ByteBuffer inputBuffer2 = this._inputBuffer;
                        super._inputPtr = inputPtr2 + 1;
                        value3 = inputBuffer2.get(inputPtr2);
                    }
                    if (value3 != 65) {
                        this.reportTreeUnexpChar(this.decodeCharForError(value3), " (expected 'A' for CDATA)");
                    }
                    super._state = 4;
                    final int inputPtr3 = super._inputPtr;
                    if (inputPtr3 >= super._inputEnd) {
                        return 257;
                    }
                    final ByteBuffer inputBuffer3 = this._inputBuffer;
                    super._inputPtr = inputPtr3 + 1;
                    value4 = inputBuffer3.get(inputPtr3);
                }
                if (value4 != 84) {
                    this.reportTreeUnexpChar(this.decodeCharForError(value4), " (expected 'T' for CDATA)");
                }
                super._state = 5;
                final int inputPtr4 = super._inputPtr;
                if (inputPtr4 >= super._inputEnd) {
                    return 257;
                }
                final ByteBuffer inputBuffer4 = this._inputBuffer;
                super._inputPtr = inputPtr4 + 1;
                value5 = inputBuffer4.get(inputPtr4);
            }
            if (value5 != 65) {
                this.reportTreeUnexpChar(this.decodeCharForError(value5), " (expected 'A' for CDATA)");
            }
            super._state = 6;
            final int inputPtr5 = super._inputPtr;
            if (inputPtr5 >= super._inputEnd) {
                return 257;
            }
            final ByteBuffer inputBuffer5 = this._inputBuffer;
            super._inputPtr = inputPtr5 + 1;
            value = inputBuffer5.get(inputPtr5);
        }
        if (value != 91) {
            this.reportTreeUnexpChar(this.decodeCharForError(value), " (expected '[' for CDATA)");
        }
        super._textBuilder.resetWithEmpty();
        super._state = 1;
        if (super._inputPtr >= super._inputEnd) {
            return 257;
        }
        return this.parseCDataContents();
    }
    
    private final int handleDecEntityInAttribute(final boolean b) {
        byte b3;
        final byte b2 = b3 = this._inputBuffer.get(super._inputPtr++);
        while (true) {
            Label_0114: {
                if (!b) {
                    break Label_0114;
                }
                if (b2 < 48 || b2 > 57) {
                    this.throwUnexpectedChar(this.decodeCharForError(b2), " expected a digit (0 - 9) for character entity");
                }
                super._pendingInput = -64;
                super._entityValue = b2 - 48;
                final int n;
                if ((n = super._inputPtr) >= super._inputEnd) {
                    return 0;
                }
                final ByteBuffer inputBuffer = this._inputBuffer;
                super._inputPtr = n + 1;
                b3 = inputBuffer.get(n);
            }
            if (b3 == 59) {
                this.verifyXmlChar(super._entityValue);
                super._pendingInput = 0;
                return super._entityValue;
            }
            final int n2 = b3 - 48;
            if (n2 < 0 || n2 > 9) {
                this.throwUnexpectedChar(this.decodeCharForError(b3), " expected a digit (0 - 9) for character entity");
            }
            if ((super._entityValue = super._entityValue * 10 + n2) > 1114111) {
                this.reportEntityOverflow();
            }
            int n;
            if ((n = super._inputPtr) >= super._inputEnd) {
                return 0;
            }
            continue;
        }
    }
    
    private int handleEndElement() {
        final int state = super._state;
        Label_0268: {
            if (state == 0) {
                final PName tokenName = super._tokenName;
                while (super._quadCount < tokenName.sizeInQuads() - 1) {
                    while (super._currQuadBytes < 4) {
                        final int inputPtr = super._inputPtr;
                        if (inputPtr >= super._inputEnd) {
                            return 257;
                        }
                        final int currQuad = super._currQuad;
                        final ByteBuffer inputBuffer = this._inputBuffer;
                        super._inputPtr = inputPtr + 1;
                        super._currQuad = ((inputBuffer.get(inputPtr) & 0xFF) | currQuad << 8);
                        ++super._currQuadBytes;
                    }
                    if (super._currQuad != tokenName.getQuad(super._quadCount)) {
                        this.reportUnexpectedEndTag(tokenName.getPrefixedName());
                    }
                    super._currQuadBytes = 0;
                    super._currQuad = 0;
                    ++super._quadCount;
                }
                final int lastQuad = tokenName.getLastQuad();
                while (true) {
                    do {
                        final int inputPtr2 = super._inputPtr;
                        if (inputPtr2 >= super._inputEnd) {
                            return 257;
                        }
                        final int currQuad2 = super._currQuad;
                        final ByteBuffer inputBuffer2 = this._inputBuffer;
                        super._inputPtr = inputPtr2 + 1;
                        if ((super._currQuad = ((inputBuffer2.get(inputPtr2) & 0xFF) | currQuad2 << 8)) == lastQuad) {
                            super._state = 1;
                            break Label_0268;
                        }
                    } while (++super._currQuadBytes <= 3);
                    this.reportUnexpectedEndTag(tokenName.getPrefixedName());
                    continue;
                }
            }
            if (state != 1) {
                this.throwInternal();
            }
        }
        if (super._pendingInput != 0 && !this.handlePartialCR()) {
            return 257;
        }
        while (true) {
            final int inputPtr3 = super._inputPtr;
            if (inputPtr3 >= super._inputEnd) {
                return 257;
            }
            final ByteBuffer inputBuffer3 = this._inputBuffer;
            super._inputPtr = inputPtr3 + 1;
            final int n = inputBuffer3.get(inputPtr3) & 0xFF;
            if (n > 32) {
                if (n != 62) {
                    this.throwUnexpectedChar(this.decodeCharForError((byte)n), " expected space or closing '>'");
                }
                return super._currToken = 2;
            }
            if (n != 10) {
                if (n == 13) {
                    final int inputPtr4 = super._inputPtr;
                    if (inputPtr4 >= super._inputEnd) {
                        super._pendingInput = -1;
                        return 257;
                    }
                    if (this._inputBuffer.get(inputPtr4) == 10) {
                        ++super._inputPtr;
                    }
                }
                else {
                    if (n != 32 && n != 9) {
                        this.throwInvalidSpace(n);
                        continue;
                    }
                    continue;
                }
            }
            this.markLF();
        }
    }
    
    private int handleEndElementStart() {
        --super._depth;
        final PName name = super._currElem.getName();
        super._tokenName = name;
        int sizeInQuads = name.sizeInQuads();
        final int inputEnd = super._inputEnd;
        final int inputPtr = super._inputPtr;
        int i = 0;
        if (inputEnd - inputPtr < (sizeInQuads << 2) + 1) {
            super._nextEvent = 2;
            super._state = 0;
            super._currQuadBytes = 0;
            super._currQuad = 0;
            super._quadCount = 0;
            return this.handleEndElement();
        }
        final ByteBuffer inputBuffer = this._inputBuffer;
        --sizeInQuads;
        while (i < sizeInQuads) {
            final int inputPtr2 = super._inputPtr;
            final byte value = inputBuffer.get(inputPtr2);
            final byte value2 = inputBuffer.get(inputPtr2 + 1);
            final byte value3 = inputBuffer.get(inputPtr2 + 2);
            final byte value4 = inputBuffer.get(inputPtr2 + 3);
            super._inputPtr += 4;
            if (((value4 & 0xFF) | (value << 24 | (value2 & 0xFF) << 16 | (value3 & 0xFF) << 8)) != super._tokenName.getQuad(i)) {
                this.reportUnexpectedEndTag(super._tokenName.getPrefixedName());
            }
            ++i;
        }
        final int quad = super._tokenName.getQuad(sizeInQuads);
        final int n = inputBuffer.get(super._inputPtr++) & 0xFF;
        if (n != quad) {
            final int n2 = n << 8 | (inputBuffer.get(super._inputPtr++) & 0xFF);
            if (n2 != quad) {
                final int n3 = n2 << 8 | (inputBuffer.get(super._inputPtr++) & 0xFF);
                if (n3 != quad && ((inputBuffer.get(super._inputPtr++) & 0xFF) | n3 << 8) != quad) {
                    this.reportUnexpectedEndTag(super._tokenName.getPrefixedName());
                }
            }
        }
        byte b = this._inputBuffer.get(super._inputPtr++);
        while (true) {
            final int n4 = b & 0xFF;
            if (n4 > 32) {
                if (n4 != 62) {
                    this.throwUnexpectedChar(this.decodeCharForError((byte)n4), " expected space or closing '>'");
                }
                return super._currToken = 2;
            }
            Label_0492: {
                if (n4 != 10) {
                    if (n4 == 13) {
                        final int inputPtr3 = super._inputPtr;
                        if (inputPtr3 >= super._inputEnd) {
                            super._pendingInput = -1;
                            break;
                        }
                        if (this._inputBuffer.get(inputPtr3) == 10) {
                            ++super._inputPtr;
                        }
                    }
                    else {
                        if (n4 != 32 && n4 != 9) {
                            this.throwInvalidSpace(n4);
                        }
                        break Label_0492;
                    }
                }
                this.markLF();
            }
            final int inputPtr4 = super._inputPtr;
            if (inputPtr4 >= super._inputEnd) {
                break;
            }
            final ByteBuffer inputBuffer2 = this._inputBuffer;
            super._inputPtr = inputPtr4 + 1;
            b = inputBuffer2.get(inputPtr4);
        }
        super._nextEvent = 2;
        super._state = 1;
        return 257;
    }
    
    private final int handleHexEntityInAttribute(final boolean b) {
        byte b3;
        final byte b2 = b3 = this._inputBuffer.get(super._inputPtr++);
        while (true) {
            Label_0170: {
                if (!b) {
                    break Label_0170;
                }
                int entityValue = 0;
                Label_0115: {
                    if (b2 <= 57 && b2 >= 48) {
                        entityValue = b2 - 48;
                    }
                    else {
                        if (b2 <= 70 && b2 >= 65) {
                            entityValue = b2 - 65;
                        }
                        else {
                            if (b2 > 102 || b2 < 97) {
                                this.throwUnexpectedChar(this.decodeCharForError(b2), " expected a hex digit (0-9a-fA-F) for character entity");
                                entityValue = b2;
                                break Label_0115;
                            }
                            entityValue = b2 - 97;
                        }
                        entityValue += 10;
                    }
                }
                super._pendingInput = -65;
                super._entityValue = entityValue;
                final int n;
                if ((n = super._inputPtr) >= super._inputEnd) {
                    return 0;
                }
                final ByteBuffer inputBuffer = this._inputBuffer;
                super._inputPtr = n + 1;
                b3 = inputBuffer.get(n);
            }
            if (b3 == 59) {
                this.verifyXmlChar(super._entityValue);
                super._pendingInput = 0;
                return super._entityValue;
            }
            int n2 = 0;
            Label_0257: {
                if (b3 <= 57 && b3 >= 48) {
                    n2 = b3 - 48;
                }
                else {
                    if (b3 <= 70 && b3 >= 65) {
                        n2 = b3 - 65;
                    }
                    else {
                        if (b3 > 102 || b3 < 97) {
                            this.throwUnexpectedChar(this.decodeCharForError(b3), " expected a hex digit (0-9a-fA-F) for character entity");
                            n2 = b3;
                            break Label_0257;
                        }
                        n2 = b3 - 97;
                    }
                    n2 += 10;
                }
            }
            final int entityValue2 = (super._entityValue << 4) + n2;
            super._entityValue = entityValue2;
            if (entityValue2 > 1114111) {
                this.reportEntityOverflow();
            }
            int n;
            if ((n = super._inputPtr) >= super._inputEnd) {
                return 0;
            }
            continue;
        }
    }
    
    private final boolean handleNsValuePending() {
        final int pendingInput = super._pendingInput;
        if (pendingInput == -1) {
            if (!this.handlePartialCR()) {
                return false;
            }
            char[] nameBuffer;
            final char[] array = nameBuffer = super._nameBuffer;
            if (super._elemNsPtr >= array.length) {
                nameBuffer = DataUtil.growArrayBy(array, array.length);
                super._nameBuffer = nameBuffer;
            }
            nameBuffer[super._elemNsPtr++] = ' ';
            return true;
        }
        else {
            final int inputPtr = super._inputPtr;
            if (inputPtr >= super._inputEnd) {
                return false;
            }
            int n = 0;
            Label_0342: {
                PName tokenName = null;
                Label_0327: {
                    Label_0284: {
                        if (pendingInput == -60) {
                            final ByteBuffer inputBuffer = this._inputBuffer;
                            super._inputPtr = inputPtr + 1;
                            final byte value = inputBuffer.get(inputPtr);
                            if (value == 35) {
                                super._pendingInput = -61;
                                final int inputPtr2 = super._inputPtr;
                                if (inputPtr2 >= super._inputEnd) {
                                    return false;
                                }
                                if (this._inputBuffer.get(inputPtr2) == 120) {
                                    super._pendingInput = -62;
                                    if (++super._inputPtr >= super._inputEnd) {
                                        return false;
                                    }
                                    break Label_0284;
                                }
                            }
                            else {
                                tokenName = this.parseNewEntityName(value);
                                if (tokenName == null) {
                                    super._pendingInput = -63;
                                    return false;
                                }
                                final int decodeGeneralEntity = this.decodeGeneralEntity(tokenName);
                                if ((n = decodeGeneralEntity) == 0) {
                                    n = decodeGeneralEntity;
                                    break Label_0327;
                                }
                                break Label_0342;
                            }
                        }
                        else if (pendingInput == -61) {
                            if (this._inputBuffer.get(inputPtr) == 120) {
                                super._pendingInput = -62;
                                if (++super._inputPtr >= super._inputEnd) {
                                    return false;
                                }
                                break Label_0284;
                            }
                        }
                        else {
                            if (pendingInput == -62) {
                                break Label_0284;
                            }
                            if (pendingInput == -63) {
                                tokenName = this.parseEntityName();
                                if (tokenName == null) {
                                    return false;
                                }
                                final int decodeGeneralEntity2 = this.decodeGeneralEntity(tokenName);
                                if ((n = decodeGeneralEntity2) == 0) {
                                    n = decodeGeneralEntity2;
                                    break Label_0327;
                                }
                                break Label_0342;
                            }
                            else {
                                if (pendingInput == -64) {
                                    n = this.handleDecEntityInAttribute(false);
                                    break Label_0342;
                                }
                                if (pendingInput == -65) {
                                    n = this.handleHexEntityInAttribute(false);
                                    break Label_0342;
                                }
                                n = this.handleAttrValuePendingUTF8();
                                break Label_0342;
                            }
                        }
                        n = this.handleDecEntityInAttribute(true);
                        break Label_0342;
                    }
                    n = this.handleHexEntityInAttribute(true);
                    break Label_0342;
                }
                super._tokenName = tokenName;
                this.reportUnexpandedEntityInAttr(super._elemAttrName, false);
            }
            if (n == 0) {
                return false;
            }
            final char[] nameBuffer2 = super._nameBuffer;
            int n2 = n;
            char[] growArrayBy = nameBuffer2;
            if (n >> 16 != 0) {
                final int n3 = n - 65536;
                growArrayBy = nameBuffer2;
                if (super._elemNsPtr >= nameBuffer2.length) {
                    growArrayBy = DataUtil.growArrayBy(nameBuffer2, nameBuffer2.length);
                    super._nameBuffer = growArrayBy;
                }
                growArrayBy[super._elemNsPtr++] = (char)(n3 >> 10 | 0xD800);
                n2 = ((n3 & 0x3FF) | 0xDC00);
            }
            char[] growArrayBy2 = growArrayBy;
            if (super._elemNsPtr >= growArrayBy.length) {
                growArrayBy2 = DataUtil.growArrayBy(growArrayBy, growArrayBy.length);
                super._nameBuffer = growArrayBy2;
            }
            growArrayBy2[super._elemNsPtr++] = (char)n2;
            return true;
        }
    }
    
    private void initAttribute(final byte elemAttrQuote) {
        super._elemAttrQuote = elemAttrQuote;
        final PName elemAttrName = super._elemAttrName;
        final String prefix = elemAttrName.getPrefix();
        PName bindName = null;
        boolean b = false;
        Label_0086: {
            Label_0084: {
                if (prefix == null) {
                    bindName = elemAttrName;
                    if (elemAttrName.getLocalName() != "xmlns") {
                        break Label_0084;
                    }
                }
                else if (prefix != "xmlns") {
                    final PName pName = bindName = this.bindName(elemAttrName, prefix);
                    if (super._elemAllNsBound) {
                        super._elemAllNsBound = pName.isBound();
                        bindName = pName;
                    }
                    break Label_0084;
                }
                b = true;
                bindName = elemAttrName;
                break Label_0086;
            }
            b = false;
        }
        if (b) {
            super._state = 8;
            super._elemNsPtr = 0;
            ++super._currNsCount;
        }
        else {
            super._state = 7;
            super._attrCollector.startNewValue(bindName, super._elemAttrPtr);
        }
    }
    
    private void initStartElement(PName bindName) {
        final String prefix = bindName.getPrefix();
        boolean bound;
        if (prefix == null) {
            bound = true;
        }
        else {
            bindName = this.bindName(bindName, prefix);
            bound = bindName.isBound();
        }
        super._elemAllNsBound = bound;
        super._tokenName = bindName;
        super._currElem = new ElementScope(bindName, super._currElem);
        super._attrCount = 0;
        super._currNsCount = 0;
        super._elemAttrPtr = 0;
        super._state = 2;
    }
    
    private int skipEntityInCharacters() {
        final int inputPtr = super._inputPtr;
        if (inputPtr + 3 <= super._inputEnd) {
            final ByteBuffer inputBuffer = this._inputBuffer;
            final int n = inputPtr + 1;
            final byte value = inputBuffer.get(inputPtr);
            if (value == 35) {
                if (this._inputBuffer.get(n) == 120) {
                    return this.handleHexEntityInCharacters(n + 1);
                }
                return this.handleDecEntityInCharacters(n);
            }
            else if (value == 97) {
                final ByteBuffer inputBuffer2 = this._inputBuffer;
                final int n2 = n + 1;
                final byte value2 = inputBuffer2.get(n);
                if (value2 == 109) {
                    final int n3 = n2 + 1;
                    if (n3 < super._inputPtr && this._inputBuffer.get(n2) == 112 && this._inputBuffer.get(n3) == 59) {
                        super._inputPtr = n2 + 2;
                        return 38;
                    }
                }
                else if (value2 == 112) {
                    final int n4 = n2 + 2;
                    if (n4 < super._inputPtr && this._inputBuffer.get(n2) == 111 && this._inputBuffer.get(n2 + 1) == 115 && this._inputBuffer.get(n4) == 59) {
                        super._inputPtr = n2 + 3;
                        return 39;
                    }
                }
            }
            else if (value == 103) {
                if (this._inputBuffer.get(n) == 116 && this._inputBuffer.get(n + 1) == 59) {
                    super._inputPtr = n + 2;
                    return 62;
                }
            }
            else if (value == 108) {
                if (this._inputBuffer.get(n) == 116 && this._inputBuffer.get(n + 1) == 59) {
                    super._inputPtr = n + 2;
                    return 60;
                }
            }
            else if (value == 113) {
                final int n5 = n + 3;
                if (n5 < super._inputPtr && this._inputBuffer.get(n) == 117 && this._inputBuffer.get(n + 1) == 111 && this._inputBuffer.get(n + 2) == 116 && this._inputBuffer.get(n5) == 59) {
                    super._inputPtr = n + 4;
                    return 39;
                }
            }
        }
        return 0;
    }
    
    private final boolean skipPending() {
        final int inputPtr = super._inputPtr;
        if (inputPtr >= super._inputEnd) {
            return false;
        }
        final int pendingInput = super._pendingInput;
        int pendingInput4 = 0;
        Label_0136: {
            if (pendingInput < 0) {
                do {
                    final int pendingInput2 = super._pendingInput;
                    if (pendingInput2 == -1) {
                        super._pendingInput = 0;
                        if (this._inputBuffer.get(super._inputPtr) == 10) {
                            ++super._inputPtr;
                        }
                        this.markLF();
                        return true;
                    }
                    int pendingInput3 = 0;
                    switch (pendingInput2) {
                        default: {
                            this.throwInternal();
                            continue;
                        }
                        case -80: {
                            final byte value = this._inputBuffer.get(super._inputPtr++);
                            if (value == 35) {
                                pendingInput3 = -81;
                                break;
                            }
                            final PName newEntityName = this.parseNewEntityName(value);
                            if (newEntityName == null) {
                                pendingInput4 = -84;
                                break Label_0136;
                            }
                            if (this.decodeGeneralEntity(newEntityName) == 0) {
                                super._tokenName = newEntityName;
                                super._nextEvent = 9;
                            }
                            super._pendingInput = 0;
                            return true;
                        }
                        case -81: {
                            super._entityValue = 0;
                            if (this._inputBuffer.get(super._inputPtr) == 120) {
                                ++super._inputPtr;
                                if (this.decodeHexEntity()) {
                                    super._pendingInput = 0;
                                    return true;
                                }
                                pendingInput4 = -83;
                                break Label_0136;
                            }
                            else {
                                if (this.decodeDecEntity()) {
                                    super._pendingInput = 0;
                                    return true;
                                }
                                pendingInput4 = -82;
                                break Label_0136;
                            }
                            break;
                        }
                        case -82: {
                            if (this.decodeDecEntity()) {
                                super._pendingInput = 0;
                                return true;
                            }
                            return false;
                        }
                        case -83: {
                            if (this.decodeHexEntity()) {
                                super._pendingInput = 0;
                                return true;
                            }
                            return false;
                        }
                        case -84: {
                            final PName entityName = this.parseEntityName();
                            if (entityName == null) {
                                return false;
                            }
                            if (this.decodeGeneralEntity(entityName) == 0) {
                                super._tokenName = entityName;
                                super._nextEvent = 9;
                            }
                            super._pendingInput = 0;
                            return true;
                        }
                        case -85: {
                            if (this._inputBuffer.get(super._inputPtr) != 93) {
                                super._pendingInput = 0;
                                return true;
                            }
                            ++super._inputPtr;
                            pendingInput3 = -86;
                            break;
                        }
                        case -86: {
                            final byte value2 = this._inputBuffer.get(super._inputPtr);
                            if (value2 == 93) {
                                ++super._inputPtr;
                                continue;
                            }
                            if (value2 == 62) {
                                ++super._inputPtr;
                                this.reportInputProblem("Encountered ']]>' in text segment");
                            }
                            super._pendingInput = 0;
                            return true;
                        }
                    }
                    super._pendingInput = pendingInput3;
                } while (super._inputPtr < super._inputEnd);
                return false;
            }
            final int[] text_CHARS = super._charTypes.TEXT_CHARS;
            final int n = pendingInput & 0xFF;
            final int n2 = text_CHARS[n];
            Label_0876: {
                if (n2 != 5) {
                    int n8 = 0;
                    Label_0573: {
                        int n3;
                        if (n2 != 6) {
                            if (n2 != 7) {
                                this.throwInternal();
                                break Label_0876;
                            }
                            final ByteBuffer inputBuffer = this._inputBuffer;
                            super._inputPtr = inputPtr + 1;
                            n3 = (inputBuffer.get(inputPtr) & 0xFF);
                            final int n4 = pendingInput >> 8;
                            if (n4 == 0) {
                                final int inputPtr2 = super._inputPtr;
                                if (inputPtr2 < super._inputEnd) {
                                    final ByteBuffer inputBuffer2 = this._inputBuffer;
                                    super._inputPtr = inputPtr2 + 1;
                                    final int n5 = inputBuffer2.get(inputPtr2) & 0xFF;
                                    final int inputPtr3 = super._inputPtr;
                                    if (inputPtr3 >= super._inputEnd) {
                                        pendingInput4 = (n3 << 8 | pendingInput | n5 << 16);
                                        break Label_0136;
                                    }
                                    final ByteBuffer inputBuffer3 = this._inputBuffer;
                                    super._inputPtr = inputPtr3 + 1;
                                    this.decodeUtf8_4(pendingInput, n3, n5, inputBuffer3.get(inputPtr3) & 0xFF);
                                    break Label_0876;
                                }
                            }
                            else {
                                final int n6 = n4 & 0xFF;
                                final int n7 = pendingInput >> 16;
                                if (n7 != 0) {
                                    this.decodeUtf8_4(n, n6, n7, n3);
                                    break Label_0876;
                                }
                                final int inputPtr4 = super._inputPtr;
                                if (inputPtr4 >= super._inputEnd) {
                                    n8 = n3 << 16;
                                    break Label_0573;
                                }
                                final ByteBuffer inputBuffer4 = this._inputBuffer;
                                super._inputPtr = inputPtr4 + 1;
                                this.decodeUtf8_4(n, n6, n3, inputBuffer4.get(inputPtr4) & 0xFF);
                                break Label_0876;
                            }
                        }
                        else {
                            final ByteBuffer inputBuffer5 = this._inputBuffer;
                            super._inputPtr = inputPtr + 1;
                            n3 = (inputBuffer5.get(inputPtr) & 0xFF);
                            final int n9 = pendingInput >> 8;
                            if (n9 != 0) {
                                this.decodeUtf8_3(n, n9, n3);
                                break Label_0876;
                            }
                            final int inputPtr5 = super._inputPtr;
                            if (inputPtr5 < super._inputEnd) {
                                final ByteBuffer inputBuffer6 = this._inputBuffer;
                                super._inputPtr = inputPtr5 + 1;
                                this.decodeUtf8_3(pendingInput, n3, inputBuffer6.get(inputPtr5) & 0xFF);
                                break Label_0876;
                            }
                        }
                        n8 = n3 << 8;
                    }
                    pendingInput4 = (n8 | pendingInput);
                    break Label_0136;
                }
                this.skipUtf8_2(pendingInput);
            }
            super._pendingInput = 0;
            return true;
        }
        super._pendingInput = pendingInput4;
        return false;
    }
    
    @Override
    public final byte _currentByte() {
        return this._inputBuffer.get(super._inputPtr);
    }
    
    @Override
    public final byte _nextByte() {
        return this._inputBuffer.get(super._inputPtr++);
    }
    
    @Override
    public final byte _prevByte() {
        return this._inputBuffer.get(super._inputPtr - 1);
    }
    
    @Override
    public boolean asyncSkipSpace() {
        while (true) {
            final int inputPtr = super._inputPtr;
            if (inputPtr >= super._inputEnd) {
                break;
            }
            final byte value = this._inputBuffer.get(inputPtr);
            if ((value & 0xFF) > 32) {
                if (super._pendingInput == -1) {
                    this.markLF();
                    super._pendingInput = 0;
                }
                return true;
            }
            final int inputPtr2 = super._inputPtr + 1;
            super._inputPtr = inputPtr2;
            if (value != 10) {
                if (value == 13) {
                    if (inputPtr2 >= super._inputEnd) {
                        super._pendingInput = -1;
                        break;
                    }
                    if (this._inputBuffer.get(inputPtr2) == 10) {
                        ++super._inputPtr;
                    }
                }
                else {
                    if (value != 32 && value != 9) {
                        this.throwInvalidSpace(value);
                        continue;
                    }
                    continue;
                }
            }
            this.markLF();
        }
        return false;
    }
    
    public final boolean decodeDecEntity() {
        int entityValue = super._entityValue;
        while (true) {
            final int inputPtr = super._inputPtr;
            if (inputPtr >= super._inputEnd) {
                super._entityValue = entityValue;
                return false;
            }
            final ByteBuffer inputBuffer = this._inputBuffer;
            super._inputPtr = inputPtr + 1;
            final byte value = inputBuffer.get(inputPtr);
            if (value == 59) {
                super._entityValue = entityValue;
                return true;
            }
            final int n = value - 48;
            if (n < 0 || n > 9) {
                this.throwUnexpectedChar(this.decodeCharForError(value), " expected a digit (0 - 9) for character entity");
            }
            final int entityValue2 = entityValue * 10 + n;
            if ((entityValue = entityValue2) <= 1114111) {
                continue;
            }
            super._entityValue = entityValue2;
            this.reportEntityOverflow();
            entityValue = entityValue2;
        }
    }
    
    public final int decodeGeneralEntity(final PName pName) {
        final byte value = this._inputBuffer.get(super._inputPtr++);
        if (value != 59) {
            final int decodeCharForError = this.decodeCharForError(value);
            final StringBuilder sb = new StringBuilder();
            sb.append(" expected ';' following entity name (\"");
            sb.append(pName.getPrefixedName());
            sb.append("\")");
            this.throwUnexpectedChar(decodeCharForError, sb.toString());
        }
        final String prefixedName = pName.getPrefixedName();
        if (prefixedName == "amp") {
            return 38;
        }
        if (prefixedName == "lt") {
            return 60;
        }
        if (prefixedName == "apos") {
            return 39;
        }
        if (prefixedName == "quot") {
            return 34;
        }
        if (prefixedName == "gt") {
            return 62;
        }
        return 0;
    }
    
    public final boolean decodeHexEntity() {
        int entityValue = super._entityValue;
        while (true) {
            final int inputPtr = super._inputPtr;
            if (inputPtr >= super._inputEnd) {
                super._entityValue = entityValue;
                return false;
            }
            final ByteBuffer inputBuffer = this._inputBuffer;
            super._inputPtr = inputPtr + 1;
            final byte value = inputBuffer.get(inputPtr);
            if (value == 59) {
                super._entityValue = entityValue;
                return true;
            }
            int n = 0;
            Label_0128: {
                if (value <= 57 && value >= 48) {
                    n = value - 48;
                }
                else {
                    if (value <= 70 && value >= 65) {
                        n = value - 65;
                    }
                    else {
                        if (value > 102 || value < 97) {
                            this.throwUnexpectedChar(this.decodeCharForError(value), " expected a hex digit (0-9a-fA-F) for character entity");
                            n = value;
                            break Label_0128;
                        }
                        n = value - 97;
                    }
                    n += 10;
                }
            }
            final int entityValue2 = entityValue = (entityValue << 4) + n;
            if (entityValue2 <= 1114111) {
                continue;
            }
            super._entityValue = entityValue2;
            this.reportEntityOverflow();
            entityValue = entityValue2;
        }
    }
    
    public final int decodeUtf8_2(final int n) {
        final byte value = this._inputBuffer.get(super._inputPtr++);
        if ((value & 0xC0) != 0x80) {
            this.reportInvalidOther(value & 0xFF, super._inputPtr);
        }
        return (n & 0x1F) << 6 | (value & 0x3F);
    }
    
    public final int decodeUtf8_3(int n) {
        final int n2 = n & 0xF;
        final ByteBuffer inputBuffer = this._inputBuffer;
        n = super._inputPtr++;
        n = inputBuffer.get(n);
        if ((n & 0xC0) != 0x80) {
            this.reportInvalidOther(n & 0xFF, super._inputPtr);
        }
        final byte value = this._inputBuffer.get(super._inputPtr++);
        if ((value & 0xC0) != 0x80) {
            this.reportInvalidOther(value & 0xFF, super._inputPtr);
        }
        final int n3 = n = (((n & 0x3F) | n2 << 6) << 6 | (value & 0x3F));
        if (n2 >= 13 && (n = n3) >= 55296) {
            if (n3 >= 57344) {
                n = n3;
                if (n3 < 65534 || (n = n3) > 65535) {
                    return n;
                }
            }
            n = this.handleInvalidXmlChar(n3);
        }
        return n;
    }
    
    public final int decodeUtf8_3(final int n, int handleInvalidXmlChar, int n2) {
        if ((handleInvalidXmlChar & 0xC0) != 0x80) {
            this.reportInvalidOther(handleInvalidXmlChar & 0xFF, super._inputPtr - 1);
        }
        if ((n2 & 0xC0) != 0x80) {
            this.reportInvalidOther(n2 & 0xFF, super._inputPtr);
        }
        n2 = (handleInvalidXmlChar = ((handleInvalidXmlChar & 0x3F) << 6 | (n & 0xF) << 12 | (n2 & 0x3F)));
        if (n >= 13 && (handleInvalidXmlChar = n2) >= 55296) {
            if (n2 >= 57344) {
                handleInvalidXmlChar = n2;
                if (n2 < 65534 || (handleInvalidXmlChar = n2) > 65535) {
                    return handleInvalidXmlChar;
                }
            }
            handleInvalidXmlChar = this.handleInvalidXmlChar(n2);
        }
        return handleInvalidXmlChar;
    }
    
    public final int decodeUtf8_4(final int n) {
        final byte value = this._inputBuffer.get(super._inputPtr++);
        if ((value & 0xC0) != 0x80) {
            this.reportInvalidOther(value & 0xFF, super._inputPtr);
        }
        final byte value2 = this._inputBuffer.get(super._inputPtr++);
        if ((value2 & 0xC0) != 0x80) {
            this.reportInvalidOther(value2 & 0xFF, super._inputPtr);
        }
        final byte value3 = this._inputBuffer.get(super._inputPtr++);
        if ((value3 & 0xC0) != 0x80) {
            this.reportInvalidOther(value3 & 0xFF, super._inputPtr);
        }
        return ((((n & 0x7) << 6 | (value & 0x3F)) << 6 | (value2 & 0x3F)) << 6 | (value3 & 0x3F)) - 65536;
    }
    
    public final int decodeUtf8_4(final int n, final int n2, final int n3, final int n4) {
        if ((n2 & 0xC0) != 0x80) {
            this.reportInvalidOther(n2 & 0xFF, super._inputPtr - 2);
        }
        if ((n3 & 0xC0) != 0x80) {
            this.reportInvalidOther(n3 & 0xFF, super._inputPtr - 1);
        }
        if ((n4 & 0xC0) != 0x80) {
            this.reportInvalidOther(n4 & 0xFF, super._inputPtr);
        }
        return ((((n & 0x7) << 6 | (n2 & 0x3F)) << 6 | (n3 & 0x3F)) << 6 | (n4 & 0x3F)) - 65536;
    }
    
    @Override
    public void feedInput(final ByteBuffer inputBuffer) {
        if (super._inputPtr < super._inputEnd) {
            final StringBuilder sb = new StringBuilder();
            sb.append("Still have ");
            sb.append(super._inputEnd - super._inputPtr);
            sb.append(" unread bytes");
            throw new XMLStreamException(sb.toString());
        }
        if (!super._endOfInput) {
            final long pastBytesOrChars = super._pastBytesOrChars;
            final int origBufferLen = this._origBufferLen;
            super._pastBytesOrChars = pastBytesOrChars + origBufferLen;
            super._rowStartOffset -= origBufferLen;
            final int position = inputBuffer.position();
            final int limit = inputBuffer.limit();
            this._inputBuffer = inputBuffer;
            super._inputPtr = position;
            super._inputEnd = limit;
            this._origBufferLen = limit - position;
            return;
        }
        throw new XMLStreamException("Already closed, can not feed more input");
    }
    
    @Override
    public final void finishCharacters() {
        if (super._pendingInput != 0) {
            this.throwInternal();
        }
        final int[] text_CHARS = super._charTypes.TEXT_CHARS;
        final ByteBuffer inputBuffer = this._inputBuffer;
        char[] array = super._textBuilder.getBufferWithoutReset();
        int currentLength = super._textBuilder.getCurrentLength();
        Label_0822: {
            Block_23: {
            Label_0767:
                while (true) {
                    final int inputPtr = super._inputPtr;
                    if (inputPtr >= super._inputEnd) {
                        break Label_0822;
                    }
                    final int length = array.length;
                    final int n = 0;
                    int n2;
                    if ((n2 = currentLength) >= length) {
                        array = super._textBuilder.finishCurrentSegment();
                        n2 = 0;
                    }
                    final int inputEnd = super._inputEnd;
                    final int n3 = array.length - n2 + inputPtr;
                    currentLength = n2;
                    int i = inputPtr;
                    int n4;
                    if (n3 < (n4 = inputEnd)) {
                        n4 = n3;
                        i = inputPtr;
                        currentLength = n2;
                    }
                    while (i < n4) {
                        final int inputPtr2 = i + 1;
                        final int n5 = inputBuffer.get(i) & 0xFF;
                        final int n6 = text_CHARS[n5];
                        if (n6 != 0) {
                            super._inputPtr = inputPtr2;
                            int pendingInput = 0;
                            Label_0736: {
                                int n17 = 0;
                                int n18 = 0;
                                Label_0568: {
                                    char[] array2 = null;
                                    int n7 = 0;
                                    int n8 = 0;
                                    Label_0866: {
                                        int n15 = 0;
                                        int n16 = 0;
                                        Label_0629: {
                                            switch (n6) {
                                                default: {
                                                    array2 = array;
                                                    n7 = currentLength;
                                                    n8 = n5;
                                                    break Label_0866;
                                                }
                                                case 11: {
                                                    byte value = 0;
                                                    int n9 = 1;
                                                    byte b;
                                                    while (true) {
                                                        final int inputPtr3 = super._inputPtr;
                                                        b = value;
                                                        if (inputPtr3 >= super._inputEnd) {
                                                            break;
                                                        }
                                                        value = inputBuffer.get(inputPtr3);
                                                        if (value != 93) {
                                                            b = value;
                                                            break;
                                                        }
                                                        ++super._inputPtr;
                                                        ++n9;
                                                    }
                                                    char[] finishCurrentSegment = array;
                                                    int n10 = currentLength;
                                                    int n11 = n9;
                                                    if (b == 62) {
                                                        finishCurrentSegment = array;
                                                        n10 = currentLength;
                                                        if ((n11 = n9) > 1) {
                                                            this.reportIllegalCDataEnd();
                                                            n11 = n9;
                                                            n10 = currentLength;
                                                            finishCurrentSegment = array;
                                                        }
                                                    }
                                                    while (true) {
                                                        --n11;
                                                        array2 = finishCurrentSegment;
                                                        n7 = n10;
                                                        n8 = n5;
                                                        if (n11 <= 0) {
                                                            break Label_0866;
                                                        }
                                                        final int n12 = n10 + 1;
                                                        finishCurrentSegment[n10] = ']';
                                                        if (n12 >= finishCurrentSegment.length) {
                                                            finishCurrentSegment = super._textBuilder.finishCurrentSegment();
                                                            n10 = 0;
                                                        }
                                                        else {
                                                            n10 = n12;
                                                        }
                                                    }
                                                    break;
                                                }
                                                case 10: {
                                                    final int handleEntityInCharacters = this.handleEntityInCharacters();
                                                    if (handleEntityInCharacters == 0) {
                                                        break Label_0767;
                                                    }
                                                    array2 = array;
                                                    n7 = currentLength;
                                                    n8 = handleEntityInCharacters;
                                                    if (handleEntityInCharacters >> 16 == 0) {
                                                        break Label_0866;
                                                    }
                                                    final int n13 = handleEntityInCharacters - 65536;
                                                    final int n14 = currentLength + 1;
                                                    array[currentLength] = (char)(n13 >> 10 | 0xD800);
                                                    n15 = n13;
                                                    if ((n16 = n14) >= array.length) {
                                                        n15 = n13;
                                                        break;
                                                    }
                                                    break Label_0629;
                                                }
                                                case 7: {
                                                    final int inputEnd2 = super._inputEnd;
                                                    if (inputEnd2 - inputPtr2 < 3) {
                                                        pendingInput = n5;
                                                        if (inputEnd2 <= inputPtr2) {
                                                            break Label_0736;
                                                        }
                                                        final ByteBuffer inputBuffer2 = this._inputBuffer;
                                                        super._inputPtr = inputPtr2 + 1;
                                                        n17 = (n5 | (inputBuffer2.get(inputPtr2) & 0xFF) << 8);
                                                        final int inputEnd3 = super._inputEnd;
                                                        final int inputPtr4 = super._inputPtr;
                                                        pendingInput = n17;
                                                        if (inputEnd3 > inputPtr4) {
                                                            final ByteBuffer inputBuffer3 = this._inputBuffer;
                                                            super._inputPtr = inputPtr4 + 1;
                                                            n18 = (inputBuffer3.get(inputPtr4) & 0xFF) << 16;
                                                            break Label_0568;
                                                        }
                                                        break Label_0736;
                                                    }
                                                    else {
                                                        final int decodeUtf8_4 = this.decodeUtf8_4(n5);
                                                        final int n19 = currentLength + 1;
                                                        array[currentLength] = (char)(decodeUtf8_4 >> 10 | 0xD800);
                                                        n15 = decodeUtf8_4;
                                                        if ((n16 = n19) >= array.length) {
                                                            n15 = decodeUtf8_4;
                                                            break;
                                                        }
                                                        break Label_0629;
                                                    }
                                                    break;
                                                }
                                                case 6: {
                                                    final int inputEnd4 = super._inputEnd;
                                                    if (inputEnd4 - inputPtr2 >= 2) {
                                                        n8 = this.decodeUtf8_3(n5);
                                                        array2 = array;
                                                        n7 = currentLength;
                                                        break Label_0866;
                                                    }
                                                    pendingInput = n5;
                                                    if (inputEnd4 > inputPtr2) {
                                                        final ByteBuffer inputBuffer4 = this._inputBuffer;
                                                        super._inputPtr = inputPtr2 + 1;
                                                        n18 = (inputBuffer4.get(inputPtr2) & 0xFF) << 8;
                                                        n17 = n5;
                                                        break Label_0568;
                                                    }
                                                    break Label_0736;
                                                }
                                                case 5: {
                                                    if (inputPtr2 >= super._inputEnd) {
                                                        pendingInput = n5;
                                                        break Label_0736;
                                                    }
                                                    n8 = this.decodeUtf8_2(n5);
                                                    array2 = array;
                                                    n7 = currentLength;
                                                    break Label_0866;
                                                }
                                                case 4: {
                                                    this.reportInvalidInitial(n5);
                                                }
                                                case 9: {
                                                    break Label_0767;
                                                }
                                                case 3: {
                                                    this.markLF();
                                                    array2 = array;
                                                    n7 = currentLength;
                                                    n8 = n5;
                                                    break Label_0866;
                                                }
                                                case 1: {
                                                    this.handleInvalidXmlChar(n5);
                                                }
                                                case 2: {
                                                    final int inputPtr5 = super._inputPtr;
                                                    if (inputPtr5 >= super._inputEnd) {
                                                        break Block_23;
                                                    }
                                                    if (inputBuffer.get(inputPtr5) == 10) {
                                                        ++super._inputPtr;
                                                    }
                                                    this.markLF();
                                                    n8 = 10;
                                                    n7 = currentLength;
                                                    array2 = array;
                                                    break Label_0866;
                                                }
                                            }
                                            array = super._textBuilder.finishCurrentSegment();
                                            n16 = n;
                                        }
                                        final int n20 = (n15 & 0x3FF) | 0xDC00;
                                        array2 = array;
                                        n7 = n16;
                                        n8 = n20;
                                    }
                                    array2[n7] = (char)n8;
                                    currentLength = n7 + 1;
                                    array = array2;
                                    continue Label_0822;
                                }
                                pendingInput = (n17 | n18);
                            }
                            super._pendingInput = pendingInput;
                            break Label_0822;
                        }
                        array[currentLength] = (char)n5;
                        i = inputPtr2;
                        ++currentLength;
                    }
                    super._inputPtr = i;
                }
                --super._inputPtr;
                break Label_0822;
            }
            super._pendingInput = -1;
        }
        super._textBuilder.setCurrentLength(currentLength);
    }
    
    public final int finishCharactersCoalescing() {
        if (super._pendingInput != 0 && !this.handleAndAppendPending()) {
            return 257;
        }
        throw new UnsupportedOperationException();
    }
    
    @Override
    public boolean handleAttrValue() {
        if (super._pendingInput != 0) {
            if (!this.handleAttrValuePending()) {
                return false;
            }
            super._pendingInput = 0;
        }
        char[] array = super._attrCollector.continueValue();
        final int[] attr_CHARS = super._charTypes.ATTR_CHARS;
        final byte elemAttrQuote = super._elemAttrQuote;
    Label_0045:
        while (super._inputPtr < super._inputEnd) {
            char[] valueBufferFull = array;
            if (super._elemAttrPtr >= array.length) {
                valueBufferFull = super._attrCollector.valueBufferFull();
            }
            final int inputEnd = super._inputEnd;
            final int n = super._inputPtr + (valueBufferFull.length - super._elemAttrPtr);
            int n2;
            if (n < (n2 = inputEnd)) {
                n2 = n;
            }
            int pendingInput;
            int n3;
            while (true) {
                final int inputPtr = super._inputPtr;
                array = valueBufferFull;
                if (inputPtr >= n2) {
                    continue Label_0045;
                }
                final ByteBuffer inputBuffer = this._inputBuffer;
                super._inputPtr = inputPtr + 1;
                pendingInput = (inputBuffer.get(inputPtr) & 0xFF);
                n3 = attr_CHARS[pendingInput];
                if (n3 != 0) {
                    break;
                }
                valueBufferFull[super._elemAttrPtr++] = (char)pendingInput;
            }
            int n4 = 0;
            Label_0700: {
                if (n3 != 14) {
                    switch (n3) {
                        default: {
                            array = valueBufferFull;
                            n4 = pendingInput;
                            break Label_0700;
                        }
                        case 7: {
                            final int inputEnd2 = super._inputEnd;
                            final int inputPtr2 = super._inputPtr;
                            if (inputEnd2 - inputPtr2 < 3) {
                                int pendingInput2 = pendingInput;
                                if (inputEnd2 > inputPtr2) {
                                    final ByteBuffer inputBuffer2 = this._inputBuffer;
                                    super._inputPtr = inputPtr2 + 1;
                                    final int n5 = pendingInput | (inputBuffer2.get(inputPtr2) & 0xFF) << 8;
                                    final int inputEnd3 = super._inputEnd;
                                    final int inputPtr3 = super._inputPtr;
                                    pendingInput2 = n5;
                                    if (inputEnd3 > inputPtr3) {
                                        final ByteBuffer inputBuffer3 = this._inputBuffer;
                                        super._inputPtr = inputPtr3 + 1;
                                        pendingInput2 = (n5 | (inputBuffer3.get(inputPtr3) & 0xFF) << 16);
                                    }
                                }
                                super._pendingInput = pendingInput2;
                                return false;
                            }
                            final int decodeUtf8_4 = this.decodeUtf8_4(pendingInput);
                            final int elemAttrPtr = super._elemAttrPtr;
                            final int elemAttrPtr2 = elemAttrPtr + 1;
                            super._elemAttrPtr = elemAttrPtr2;
                            valueBufferFull[elemAttrPtr] = (char)(0xD800 | decodeUtf8_4 >> 10);
                            final int n6 = (decodeUtf8_4 & 0x3FF) | 0xDC00;
                            array = valueBufferFull;
                            n4 = n6;
                            if (elemAttrPtr2 >= valueBufferFull.length) {
                                n4 = n6;
                                break;
                            }
                            break Label_0700;
                        }
                        case 6: {
                            final int inputEnd4 = super._inputEnd;
                            final int inputPtr4 = super._inputPtr;
                            if (inputEnd4 - inputPtr4 < 2) {
                                int pendingInput3 = pendingInput;
                                if (inputEnd4 > inputPtr4) {
                                    final ByteBuffer inputBuffer4 = this._inputBuffer;
                                    super._inputPtr = inputPtr4 + 1;
                                    pendingInput3 = (pendingInput | (inputBuffer4.get(inputPtr4) & 0xFF) << 8);
                                }
                                super._pendingInput = pendingInput3;
                                return false;
                            }
                            n4 = this.decodeUtf8_3(pendingInput);
                            array = valueBufferFull;
                            break Label_0700;
                        }
                        case 5: {
                            if (super._inputPtr >= super._inputEnd) {
                                super._pendingInput = pendingInput;
                                return false;
                            }
                            n4 = this.decodeUtf8_2(pendingInput);
                            array = valueBufferFull;
                            break Label_0700;
                        }
                        case 4: {
                            this.reportInvalidInitial(pendingInput);
                        }
                        case 9: {
                            this.throwUnexpectedChar(pendingInput, "'<' not allowed in attribute value");
                        }
                        case 10: {
                            final int handleEntityInAttributeValue = this.handleEntityInAttributeValue();
                            if (handleEntityInAttributeValue <= 0) {
                                if (handleEntityInAttributeValue < 0) {
                                    return false;
                                }
                                this.reportUnexpandedEntityInAttr(super._elemAttrName, false);
                            }
                            array = valueBufferFull;
                            n4 = handleEntityInAttributeValue;
                            if (handleEntityInAttributeValue >> 16 == 0) {
                                break Label_0700;
                            }
                            final int n7 = handleEntityInAttributeValue - 65536;
                            final int elemAttrPtr3 = super._elemAttrPtr;
                            final int elemAttrPtr4 = elemAttrPtr3 + 1;
                            super._elemAttrPtr = elemAttrPtr4;
                            valueBufferFull[elemAttrPtr3] = (char)(0xD800 | n7 >> 10);
                            final int n8 = (n7 & 0x3FF) | 0xDC00;
                            array = valueBufferFull;
                            n4 = n8;
                            if (elemAttrPtr4 >= valueBufferFull.length) {
                                n4 = n8;
                                break;
                            }
                            break Label_0700;
                        }
                        case 1: {
                            this.handleInvalidXmlChar(pendingInput);
                        }
                        case 2: {
                            final int inputPtr5 = super._inputPtr;
                            if (inputPtr5 >= super._inputEnd) {
                                super._pendingInput = -1;
                                return false;
                            }
                            if (this._inputBuffer.get(inputPtr5) == 10) {
                                ++super._inputPtr;
                            }
                        }
                        case 3: {
                            this.markLF();
                        }
                        case 8: {
                            n4 = 32;
                            array = valueBufferFull;
                            break Label_0700;
                        }
                    }
                    array = super._attrCollector.valueBufferFull();
                }
                else {
                    array = valueBufferFull;
                    if ((n4 = pendingInput) == elemAttrQuote) {
                        return true;
                    }
                }
            }
            array[super._elemAttrPtr++] = (char)n4;
        }
        return false;
    }
    
    public final int handleCDataPending() {
        final int pendingInput = super._pendingInput;
        int n = 0;
        if (pendingInput == -30) {
            final int inputPtr = super._inputPtr;
            if (inputPtr >= super._inputEnd) {
                return 257;
            }
            if (this._inputBuffer.get(inputPtr) != 93) {
                super._textBuilder.append(']');
                return super._pendingInput = 0;
            }
            final int inputPtr2 = super._inputPtr + 1;
            super._inputPtr = inputPtr2;
            super._pendingInput = -31;
            if (inputPtr2 >= super._inputEnd) {
                return 257;
            }
        }
        while (super._pendingInput == -31) {
            final int inputPtr3 = super._inputPtr;
            if (inputPtr3 >= super._inputEnd) {
                return 257;
            }
            final ByteBuffer inputBuffer = this._inputBuffer;
            super._inputPtr = inputPtr3 + 1;
            final byte value = inputBuffer.get(inputPtr3);
            if (value == 62) {
                super._pendingInput = 0;
                super._state = 0;
                super._nextEvent = 257;
                return 12;
            }
            if (value != 93) {
                --super._inputPtr;
                super._textBuilder.append("]]");
                return super._pendingInput = 0;
            }
            super._textBuilder.append(']');
        }
        if (!this.handleAndAppendPending()) {
            n = 257;
        }
        return n;
    }
    
    @Override
    public final int handleComment() {
        if (super._state == 1) {
            return this.parseCommentContents();
        }
        final int inputPtr = super._inputPtr;
        if (inputPtr >= super._inputEnd) {
            return 257;
        }
        final ByteBuffer inputBuffer = this._inputBuffer;
        super._inputPtr = inputPtr + 1;
        final byte value = inputBuffer.get(inputPtr);
        final int state = super._state;
        if (state == 0) {
            if (value != 45) {
                this.reportTreeUnexpChar(this.decodeCharForError(value), " (expected '-' for COMMENT)");
            }
            super._state = 1;
            super._textBuilder.resetWithEmpty();
            return this.parseCommentContents();
        }
        if (state == 3) {
            if (value != 62) {
                this.reportDoubleHyphenInComments();
            }
            super._state = 0;
            super._nextEvent = 257;
            return 5;
        }
        return this.throwInternal();
    }
    
    public int handleCommentPending() {
        final int inputPtr = super._inputPtr;
        final int inputEnd = super._inputEnd;
        int n = 257;
        if (inputPtr >= inputEnd) {
            return 257;
        }
        if (super._pendingInput == -20) {
            if (this._inputBuffer.get(inputPtr) != 45) {
                super._pendingInput = 0;
                super._textBuilder.append("-");
                return 0;
            }
            final int inputPtr2 = super._inputPtr + 1;
            super._inputPtr = inputPtr2;
            super._pendingInput = -21;
            if (inputPtr2 >= super._inputEnd) {
                return 257;
            }
        }
        if (super._pendingInput == -21) {
            super._pendingInput = 0;
            if (this._inputBuffer.get(super._inputPtr++) != 62) {
                this.reportDoubleHyphenInComments();
            }
            super._state = 0;
            super._nextEvent = 257;
            return 5;
        }
        if (this.handleAndAppendPending()) {
            n = 0;
        }
        return n;
    }
    
    @Override
    public final boolean handleDTDInternalSubset(final boolean b) {
        char[] array;
        int currentLength;
        if (b) {
            array = super._textBuilder.resetWithEmpty();
            super._elemAttrQuote = 0;
            super._inDtdDeclaration = false;
            currentLength = 0;
        }
        else {
            if (super._pendingInput != 0 && !this.handleAndAppendPending()) {
                return false;
            }
            array = super._textBuilder.getBufferWithoutReset();
            currentLength = super._textBuilder.getCurrentLength();
        }
        final int[] dtd_CHARS = super._charTypes.DTD_CHARS;
        final ByteBuffer inputBuffer = this._inputBuffer;
    Label_0815:
        while (true) {
            while (super._inputPtr < super._inputEnd) {
                int n = currentLength;
                char[] array2 = array;
                if (currentLength >= array.length) {
                    array2 = super._textBuilder.finishCurrentSegment();
                    n = 0;
                }
                final int inputEnd = super._inputEnd;
                final int n2 = super._inputPtr + (array2.length - n);
                int n3 = n;
                int n4;
                if (n2 < (n4 = inputEnd)) {
                    n4 = n2;
                    n3 = n;
                }
                int n5;
                int n6;
                while (true) {
                    final int inputPtr = super._inputPtr;
                    currentLength = n3;
                    array = array2;
                    if (inputPtr >= n4) {
                        continue Label_0815;
                    }
                    super._inputPtr = inputPtr + 1;
                    n5 = (inputBuffer.get(inputPtr) & 0xFF);
                    n6 = dtd_CHARS[n5];
                    if (n6 != 0) {
                        break;
                    }
                    array2[n3] = (char)n5;
                    ++n3;
                }
                int pendingInput = 0;
                Label_0676: {
                    int n11 = 0;
                    Label_0506: {
                        int n7 = 0;
                        int n8 = 0;
                        switch (n6) {
                            default: {
                                n7 = n3;
                                n8 = n5;
                                array = array2;
                                break;
                            }
                            case 11: {
                                n7 = n3;
                                n8 = n5;
                                array = array2;
                                if (super._inDtdDeclaration) {
                                    break;
                                }
                                n7 = n3;
                                n8 = n5;
                                array = array2;
                                if (super._elemAttrQuote == 0) {
                                    super._textBuilder.setCurrentLength(n3);
                                    return true;
                                }
                                break;
                            }
                            case 10: {
                                n7 = n3;
                                n8 = n5;
                                array = array2;
                                if (super._elemAttrQuote == 0) {
                                    super._inDtdDeclaration = false;
                                    n7 = n3;
                                    n8 = n5;
                                    array = array2;
                                    break;
                                }
                                break;
                            }
                            case 9: {
                                n7 = n3;
                                n8 = n5;
                                array = array2;
                                if (!super._inDtdDeclaration) {
                                    super._inDtdDeclaration = true;
                                    n7 = n3;
                                    n8 = n5;
                                    array = array2;
                                    break;
                                }
                                break;
                            }
                            case 7: {
                                final int inputEnd2 = super._inputEnd;
                                final int inputPtr2 = super._inputPtr;
                                if (inputEnd2 - inputPtr2 >= 3) {
                                    final int decodeUtf8_4 = this.decodeUtf8_4(n5);
                                    final int n9 = n3 + 1;
                                    array2[n3] = (char)(decodeUtf8_4 >> 10 | 0xD800);
                                    int n10;
                                    if (n9 >= array2.length) {
                                        array2 = super._textBuilder.finishCurrentSegment();
                                        n10 = 0;
                                    }
                                    else {
                                        n10 = n9;
                                    }
                                    n8 = (0xDC00 | (decodeUtf8_4 & 0x3FF));
                                    n7 = n10;
                                    array = array2;
                                    break;
                                }
                                pendingInput = n5;
                                if (inputEnd2 <= inputPtr2) {
                                    break Label_0676;
                                }
                                final ByteBuffer inputBuffer2 = this._inputBuffer;
                                super._inputPtr = inputPtr2 + 1;
                                n5 |= (inputBuffer2.get(inputPtr2) & 0xFF) << 8;
                                final int inputEnd3 = super._inputEnd;
                                final int inputPtr3 = super._inputPtr;
                                pendingInput = n5;
                                if (inputEnd3 > inputPtr3) {
                                    final ByteBuffer inputBuffer3 = this._inputBuffer;
                                    super._inputPtr = inputPtr3 + 1;
                                    n11 = (inputBuffer3.get(inputPtr3) & 0xFF) << 16;
                                    break Label_0506;
                                }
                                break Label_0676;
                            }
                            case 6: {
                                final int inputEnd4 = super._inputEnd;
                                final int inputPtr4 = super._inputPtr;
                                if (inputEnd4 - inputPtr4 >= 2) {
                                    n8 = this.decodeUtf8_3(n5);
                                    n7 = n3;
                                    array = array2;
                                    break;
                                }
                                pendingInput = n5;
                                if (inputEnd4 > inputPtr4) {
                                    final ByteBuffer inputBuffer4 = this._inputBuffer;
                                    super._inputPtr = inputPtr4 + 1;
                                    n11 = (inputBuffer4.get(inputPtr4) & 0xFF) << 8;
                                    break Label_0506;
                                }
                                break Label_0676;
                            }
                            case 5: {
                                if (super._inputPtr >= super._inputEnd) {
                                    pendingInput = n5;
                                    break Label_0676;
                                }
                                n8 = this.decodeUtf8_2(n5);
                                n7 = n3;
                                array = array2;
                                break;
                            }
                            case 4: {
                                this.reportInvalidInitial(n5);
                            }
                            case 8: {
                                final byte elemAttrQuote = super._elemAttrQuote;
                                if (elemAttrQuote == 0) {
                                    super._elemAttrQuote = (byte)n5;
                                    n7 = n3;
                                    n8 = n5;
                                    array = array2;
                                    break;
                                }
                                n7 = n3;
                                n8 = n5;
                                array = array2;
                                if (elemAttrQuote == n5) {
                                    super._elemAttrQuote = 0;
                                    n7 = n3;
                                    n8 = n5;
                                    array = array2;
                                    break;
                                }
                                break;
                            }
                            case 3: {
                                this.markLF();
                                n7 = n3;
                                n8 = n5;
                                array = array2;
                                break;
                            }
                            case 1: {
                                this.handleInvalidXmlChar(n5);
                            }
                            case 2: {
                                final int inputPtr5 = super._inputPtr;
                                if (inputPtr5 >= super._inputEnd) {
                                    super._pendingInput = -1;
                                    break Label_0815;
                                }
                                if (inputBuffer.get(inputPtr5) == 10) {
                                    ++super._inputPtr;
                                }
                                this.markLF();
                                n8 = 10;
                                array = array2;
                                n7 = n3;
                                break;
                            }
                        }
                        array[n7] = (char)n8;
                        currentLength = n7 + 1;
                        continue;
                    }
                    pendingInput = (n5 | n11);
                }
                super._pendingInput = pendingInput;
                super._textBuilder.setCurrentLength(n3);
                return false;
            }
            int n3 = currentLength;
            continue Label_0815;
        }
    }
    
    public int handleDecEntityInCharacters(int n) {
        final ByteBuffer inputBuffer = this._inputBuffer;
        final int n2 = n + 1;
        byte b = inputBuffer.get(n);
        final int inputEnd = super._inputEnd;
        int n3 = 0;
        n = n2;
        while (true) {
            if (b > 57 || b < 48) {
                this.throwUnexpectedChar(this.decodeCharForError(b), " expected a digit (0 - 9) for character entity");
            }
            n3 = n3 * 10 + (b - 48);
            if (n3 > 1114111) {
                this.reportEntityOverflow();
            }
            if (n >= inputEnd) {
                return 0;
            }
            final ByteBuffer inputBuffer2 = this._inputBuffer;
            final int inputPtr = n + 1;
            b = inputBuffer2.get(n);
            if (b == 59) {
                super._inputPtr = inputPtr;
                this.verifyXmlChar(n3);
                return n3;
            }
            n = inputPtr;
        }
    }
    
    public int handleEntityInAttributeValue() {
        final int inputPtr = super._inputPtr;
        int pendingInput;
        if (inputPtr >= super._inputEnd) {
            pendingInput = -60;
        }
        else {
            final ByteBuffer inputBuffer = this._inputBuffer;
            super._inputPtr = inputPtr + 1;
            final byte value = inputBuffer.get(inputPtr);
            if (value == 35) {
                super._pendingInput = -61;
                final int inputPtr2 = super._inputPtr;
                if (inputPtr2 >= super._inputEnd) {
                    return -1;
                }
                int n;
                if (this._inputBuffer.get(inputPtr2) == 120) {
                    super._pendingInput = -62;
                    if (++super._inputPtr >= super._inputEnd) {
                        return -1;
                    }
                    n = this.handleHexEntityInAttribute(true);
                }
                else {
                    n = this.handleDecEntityInAttribute(true);
                }
                if (n == 0) {
                    return -1;
                }
                return n;
            }
            else {
                final PName newEntityName = this.parseNewEntityName(value);
                if (newEntityName == null) {
                    pendingInput = -63;
                }
                else {
                    final int decodeGeneralEntity = this.decodeGeneralEntity(newEntityName);
                    if (decodeGeneralEntity != 0) {
                        return decodeGeneralEntity;
                    }
                    super._tokenName = newEntityName;
                    return 0;
                }
            }
        }
        super._pendingInput = pendingInput;
        return -1;
    }
    
    public int handleEntityInCharacters() {
        final int inputPtr = super._inputPtr;
        if (inputPtr + 3 <= super._inputEnd) {
            final ByteBuffer inputBuffer = this._inputBuffer;
            final int n = inputPtr + 1;
            final byte value = inputBuffer.get(inputPtr);
            if (value == 35) {
                if (this._inputBuffer.get(n) == 120) {
                    return this.handleHexEntityInCharacters(n + 1);
                }
                return this.handleDecEntityInCharacters(n);
            }
            else if (value == 97) {
                final ByteBuffer inputBuffer2 = this._inputBuffer;
                final int n2 = n + 1;
                final byte value2 = inputBuffer2.get(n);
                if (value2 == 109) {
                    final int n3 = n2 + 1;
                    if (n3 < super._inputPtr && this._inputBuffer.get(n2) == 112 && this._inputBuffer.get(n3) == 59) {
                        super._inputPtr = n2 + 2;
                        return 38;
                    }
                }
                else if (value2 == 112) {
                    final int n4 = n2 + 2;
                    if (n4 < super._inputPtr && this._inputBuffer.get(n2) == 111 && this._inputBuffer.get(n2 + 1) == 115 && this._inputBuffer.get(n4) == 59) {
                        super._inputPtr = n2 + 3;
                        return 39;
                    }
                }
            }
            else if (value == 103) {
                if (this._inputBuffer.get(n) == 116 && this._inputBuffer.get(n + 1) == 59) {
                    super._inputPtr = n + 2;
                    return 62;
                }
            }
            else if (value == 108) {
                if (this._inputBuffer.get(n) == 116 && this._inputBuffer.get(n + 1) == 59) {
                    super._inputPtr = n + 2;
                    return 60;
                }
            }
            else if (value == 113) {
                final int n5 = n + 3;
                if (n5 < super._inputPtr && this._inputBuffer.get(n) == 117 && this._inputBuffer.get(n + 1) == 111 && this._inputBuffer.get(n + 2) == 116 && this._inputBuffer.get(n5) == 59) {
                    super._inputPtr = n + 4;
                    return 39;
                }
            }
        }
        return 0;
    }
    
    public int handleEntityStartingToken() {
        super._textBuilder.resetWithEmpty();
        final byte value = this._inputBuffer.get(super._inputPtr++);
        if (value == 35) {
            super._textBuilder.resetWithEmpty();
            super._state = 5;
            super._pendingInput = -70;
            if (super._inputPtr >= super._inputEnd) {
                return 257;
            }
            return this.handleNumericEntityStartingToken();
        }
        else {
            final PName newEntityName = this.parseNewEntityName(value);
            if (newEntityName == null) {
                super._state = 6;
                return 257;
            }
            final int decodeGeneralEntity = this.decodeGeneralEntity(newEntityName);
            if (decodeGeneralEntity == 0) {
                super._tokenName = newEntityName;
                super._currToken = 9;
                return super._nextEvent = 9;
            }
            super._textBuilder.resetWithChar((char)decodeGeneralEntity);
            super._nextEvent = 0;
            super._currToken = 4;
            if (super._cfgLazyParsing) {
                super._tokenIncomplete = true;
            }
            else {
                this.finishCharacters();
            }
            return super._currToken;
        }
    }
    
    public int handleHexEntityInCharacters(int n) {
        final ByteBuffer inputBuffer = this._inputBuffer;
        final int n2 = n + 1;
        byte b = inputBuffer.get(n);
        final int inputEnd = super._inputEnd;
        int n3 = 0;
        n = n2;
        while (true) {
            int n4 = 0;
            Label_0110: {
                if (b <= 57 && b >= 48) {
                    n4 = b - 48;
                }
                else {
                    if (b <= 70 && b >= 65) {
                        n4 = b - 65;
                    }
                    else {
                        if (b > 102 || b < 97) {
                            this.throwUnexpectedChar(this.decodeCharForError(b), " expected a hex digit (0-9a-fA-F) for character entity");
                            n4 = b;
                            break Label_0110;
                        }
                        n4 = b - 97;
                    }
                    n4 += 10;
                }
            }
            n3 = (n3 << 4) + n4;
            if (n3 > 1114111) {
                this.reportEntityOverflow();
            }
            if (n >= inputEnd) {
                return 0;
            }
            final ByteBuffer inputBuffer2 = this._inputBuffer;
            final int inputPtr = n + 1;
            b = inputBuffer2.get(n);
            if (b == 59) {
                super._inputPtr = inputPtr;
                this.verifyXmlChar(n3);
                return n3;
            }
            n = inputPtr;
        }
    }
    
    public int handleNamedEntityStartingToken() {
        final PName entityName = this.parseEntityName();
        if (entityName == null) {
            return super._nextEvent;
        }
        final int decodeGeneralEntity = this.decodeGeneralEntity(entityName);
        if (decodeGeneralEntity == 0) {
            super._tokenName = entityName;
            return super._currToken = 9;
        }
        super._textBuilder.resetWithChar((char)decodeGeneralEntity);
        super._nextEvent = 0;
        super._currToken = 4;
        if (super._cfgLazyParsing) {
            super._tokenIncomplete = true;
        }
        else {
            this.finishCharacters();
        }
        return super._currToken;
    }
    
    @Override
    public boolean handleNsDecl() {
        final int[] attr_CHARS = super._charTypes.ATTR_CHARS;
        final char[] nameBuffer = super._nameBuffer;
        final byte elemAttrQuote = super._elemAttrQuote;
        char[] array = nameBuffer;
        if (super._pendingInput != 0) {
            if (!this.handleNsValuePending()) {
                return false;
            }
            super._pendingInput = 0;
            array = nameBuffer;
        }
    Label_0050:
        while (super._inputPtr < super._inputEnd) {
            char[] growArrayBy = array;
            if (super._elemNsPtr >= array.length) {
                growArrayBy = DataUtil.growArrayBy(array, array.length);
                super._nameBuffer = growArrayBy;
            }
            final int inputEnd = super._inputEnd;
            final int n = super._inputPtr + (growArrayBy.length - super._elemNsPtr);
            int n2;
            if (n < (n2 = inputEnd)) {
                n2 = n;
            }
            int pendingInput;
            int n3;
            while (true) {
                final int inputPtr = super._inputPtr;
                array = growArrayBy;
                if (inputPtr >= n2) {
                    continue Label_0050;
                }
                final ByteBuffer inputBuffer = this._inputBuffer;
                super._inputPtr = inputPtr + 1;
                pendingInput = (inputBuffer.get(inputPtr) & 0xFF);
                n3 = attr_CHARS[pendingInput];
                if (n3 != 0) {
                    break;
                }
                growArrayBy[super._elemNsPtr++] = (char)pendingInput;
            }
            int n4 = 0;
            if (n3 != 14) {
                switch (n3) {
                    default: {
                        array = growArrayBy;
                        n4 = pendingInput;
                        break;
                    }
                    case 7: {
                        final int inputEnd2 = super._inputEnd;
                        final int inputPtr2 = super._inputPtr;
                        if (inputEnd2 - inputPtr2 < 3) {
                            int pendingInput2 = pendingInput;
                            if (inputEnd2 > inputPtr2) {
                                final ByteBuffer inputBuffer2 = this._inputBuffer;
                                super._inputPtr = inputPtr2 + 1;
                                final int n5 = pendingInput | (inputBuffer2.get(inputPtr2) & 0xFF) << 8;
                                final int inputEnd3 = super._inputEnd;
                                final int inputPtr3 = super._inputPtr;
                                pendingInput2 = n5;
                                if (inputEnd3 > inputPtr3) {
                                    final ByteBuffer inputBuffer3 = this._inputBuffer;
                                    super._inputPtr = inputPtr3 + 1;
                                    pendingInput2 = (n5 | (inputBuffer3.get(inputPtr3) & 0xFF) << 16);
                                }
                            }
                            super._pendingInput = pendingInput2;
                            return false;
                        }
                        final int decodeUtf8_4 = this.decodeUtf8_4(pendingInput);
                        final int elemNsPtr = super._elemNsPtr;
                        final int elemNsPtr2 = elemNsPtr + 1;
                        super._elemNsPtr = elemNsPtr2;
                        growArrayBy[elemNsPtr] = (char)(0xD800 | decodeUtf8_4 >> 10);
                        final int n6 = (decodeUtf8_4 & 0x3FF) | 0xDC00;
                        array = growArrayBy;
                        n4 = n6;
                        if (elemNsPtr2 >= growArrayBy.length) {
                            array = DataUtil.growArrayBy(growArrayBy, growArrayBy.length);
                            super._nameBuffer = array;
                            n4 = n6;
                            break;
                        }
                        break;
                    }
                    case 6: {
                        final int inputEnd4 = super._inputEnd;
                        final int inputPtr4 = super._inputPtr;
                        if (inputEnd4 - inputPtr4 < 2) {
                            int pendingInput3 = pendingInput;
                            if (inputEnd4 > inputPtr4) {
                                final ByteBuffer inputBuffer4 = this._inputBuffer;
                                super._inputPtr = inputPtr4 + 1;
                                pendingInput3 = (pendingInput | (inputBuffer4.get(inputPtr4) & 0xFF) << 8);
                            }
                            super._pendingInput = pendingInput3;
                            return false;
                        }
                        n4 = this.decodeUtf8_3(pendingInput);
                        array = growArrayBy;
                        break;
                    }
                    case 5: {
                        if (super._inputPtr >= super._inputEnd) {
                            super._pendingInput = pendingInput;
                            return false;
                        }
                        n4 = this.decodeUtf8_2(pendingInput);
                        array = growArrayBy;
                        break;
                    }
                    case 4: {
                        this.reportInvalidInitial(pendingInput);
                    }
                    case 9: {
                        this.throwUnexpectedChar(pendingInput, "'<' not allowed in attribute value");
                    }
                    case 10: {
                        final int handleEntityInAttributeValue = this.handleEntityInAttributeValue();
                        if (handleEntityInAttributeValue <= 0) {
                            if (handleEntityInAttributeValue < 0) {
                                return false;
                            }
                            this.reportUnexpandedEntityInAttr(super._elemAttrName, true);
                        }
                        array = growArrayBy;
                        n4 = handleEntityInAttributeValue;
                        if (handleEntityInAttributeValue >> 16 == 0) {
                            break;
                        }
                        final int n7 = handleEntityInAttributeValue - 65536;
                        final int elemNsPtr3 = super._elemNsPtr;
                        final int elemNsPtr4 = elemNsPtr3 + 1;
                        super._elemNsPtr = elemNsPtr4;
                        growArrayBy[elemNsPtr3] = (char)(0xD800 | n7 >> 10);
                        final int n8 = (n7 & 0x3FF) | 0xDC00;
                        array = growArrayBy;
                        n4 = n8;
                        if (elemNsPtr4 >= growArrayBy.length) {
                            array = DataUtil.growArrayBy(growArrayBy, growArrayBy.length);
                            super._nameBuffer = array;
                            n4 = n8;
                            break;
                        }
                        break;
                    }
                    case 1: {
                        this.handleInvalidXmlChar(pendingInput);
                    }
                    case 2: {
                        final int inputPtr5 = super._inputPtr;
                        if (inputPtr5 >= super._inputEnd) {
                            super._pendingInput = -1;
                            return false;
                        }
                        if (this._inputBuffer.get(inputPtr5) == 10) {
                            ++super._inputPtr;
                        }
                    }
                    case 3: {
                        this.markLF();
                    }
                    case 8: {
                        n4 = 32;
                        array = growArrayBy;
                        break;
                    }
                }
            }
            else {
                array = growArrayBy;
                if ((n4 = pendingInput) == elemAttrQuote) {
                    final int elemNsPtr5 = super._elemNsPtr;
                    if (elemNsPtr5 == 0) {
                        this.bindNs(super._elemAttrName, "");
                    }
                    else {
                        this.bindNs(super._elemAttrName, super._config.canonicalizeURI(growArrayBy, elemNsPtr5));
                    }
                    return true;
                }
            }
            array[super._elemNsPtr++] = (char)n4;
        }
        return false;
    }
    
    public int handleNumericEntityStartingToken() {
        if (super._pendingInput == -70) {
            final byte value = this._inputBuffer.get(super._inputPtr);
            super._entityValue = 0;
            if (value == 120) {
                super._pendingInput = -73;
                if (++super._inputPtr >= super._inputEnd) {
                    return 257;
                }
            }
            else {
                super._pendingInput = -72;
            }
        }
        if (super._pendingInput == -73) {
            if (!this.decodeHexEntity()) {
                return 257;
            }
        }
        else if (!this.decodeDecEntity()) {
            return 257;
        }
        this.verifyAndAppendEntityCharacter(super._entityValue);
        super._currToken = 4;
        if (super._cfgLazyParsing) {
            super._tokenIncomplete = true;
        }
        else {
            this.finishCharacters();
        }
        super._pendingInput = 0;
        return super._currToken;
    }
    
    @Override
    public int handlePI() {
        if (super._state == 5) {
            return this.parsePIData();
        }
        Label_0433: {
            while (true) {
                Label_0353: {
                    while (true) {
                        final int inputPtr = super._inputPtr;
                        if (inputPtr >= super._inputEnd) {
                            return 257;
                        }
                        final int state = super._state;
                        if (state != 0) {
                            if (state != 1) {
                                if (state == 2) {
                                    break;
                                }
                                if (state != 3) {
                                    if (state != 4) {
                                        return this.throwInternal();
                                    }
                                    final PName pName = this.parsePName();
                                    if ((super._tokenName = pName) == null) {
                                        return 257;
                                    }
                                    this.checkPITargetName(pName);
                                    super._state = 1;
                                    continue;
                                }
                                else {
                                    final ByteBuffer inputBuffer = this._inputBuffer;
                                    super._inputPtr = inputPtr + 1;
                                    final byte value = inputBuffer.get(inputPtr);
                                    if (value != 62) {
                                        this.reportMissingPISpace(this.decodeCharForError(value));
                                    }
                                    break Label_0433;
                                }
                            }
                        }
                        else {
                            final ByteBuffer inputBuffer2 = this._inputBuffer;
                            super._inputPtr = inputPtr + 1;
                            final PName newName = this.parseNewName(inputBuffer2.get(inputPtr));
                            if ((super._tokenName = newName) == null) {
                                super._state = 4;
                                return 257;
                            }
                            super._state = 1;
                            this.checkPITargetName(newName);
                            if (super._inputPtr >= super._inputEnd) {
                                return 257;
                            }
                        }
                        final byte value2 = this._inputBuffer.get(super._inputPtr++);
                        if (value2 == 63) {
                            final int inputPtr2 = super._inputPtr;
                            if (inputPtr2 < super._inputEnd && this._inputBuffer.get(inputPtr2) == 62) {
                                final int inputPtr3 = super._inputPtr + 1;
                                super._inputPtr = inputPtr3;
                                break Label_0433;
                            }
                            super._state = 3;
                        }
                        else {
                            if (value2 != 32 && value2 != 13 && value2 != 10 && value2 != 9) {
                                this.reportMissingPISpace(this.decodeCharForError(value2));
                                break;
                            }
                            break Label_0353;
                        }
                    }
                    if (!this.asyncSkipSpace()) {
                        return 257;
                    }
                    super._state = 5;
                    super._textBuilder.resetWithEmpty();
                    return this.parsePIData();
                }
                if (!this.asyncSkipSpace()) {
                    super._state = 2;
                    return 257;
                }
                super._textBuilder.resetWithEmpty();
                final int inputPtr4 = super._inputPtr;
                if (inputPtr4 + 1 < super._inputEnd && this._inputBuffer.get(inputPtr4) == 63 && this._inputBuffer.get(super._inputPtr + 1) == 62) {
                    final int inputPtr3 = super._inputPtr + 2;
                    continue;
                }
                break;
            }
            super._state = 5;
            return this.parsePIData();
        }
        super._state = 0;
        super._nextEvent = 257;
        return 3;
    }
    
    public int handlePIPending() {
        final int pendingInput = super._pendingInput;
        int n = 257;
        if (pendingInput != -15) {
            if (this.handleAndAppendPending()) {
                n = 0;
            }
            return n;
        }
        final int inputPtr = super._inputPtr;
        if (inputPtr >= super._inputEnd) {
            return 257;
        }
        final byte value = this._inputBuffer.get(inputPtr);
        super._pendingInput = 0;
        if (value != 62) {
            super._textBuilder.append('?');
            return 0;
        }
        ++super._inputPtr;
        super._state = 0;
        super._nextEvent = 257;
        return 3;
    }
    
    @Override
    public final boolean handlePartialCR() {
        if (super._pendingInput != -1) {
            this.throwInternal();
        }
        final int inputPtr = super._inputPtr;
        if (inputPtr >= super._inputEnd) {
            return false;
        }
        super._pendingInput = 0;
        if (this._inputBuffer.get(inputPtr) == 10) {
            ++super._inputPtr;
        }
        ++super._currRow;
        super._rowStartOffset = super._inputPtr;
        return true;
    }
    
    @Override
    public int handleStartElement() {
    Label_0000:
        while (true) {
            final int inputPtr = super._inputPtr;
            if (inputPtr >= super._inputEnd) {
                return 257;
            }
            Label_0343: {
                switch (super._state) {
                    default: {
                        this.throwInternal();
                        continue;
                    }
                    case 9: {
                        final ByteBuffer inputBuffer = this._inputBuffer;
                        super._inputPtr = inputPtr + 1;
                        final byte value = inputBuffer.get(inputPtr);
                        if (value != 62) {
                            this.throwUnexpectedChar(this.decodeCharForError(value), " expected '>'");
                        }
                        return this.finishStartElement(true);
                    }
                    case 8: {
                        if (!this.handleNsDecl()) {
                            return 257;
                        }
                        break;
                    }
                    case 7: {
                        if (!this.handleAttrValue()) {
                            return 257;
                        }
                        break;
                    }
                    case 1: {
                        final PName pName = this.parsePName();
                        if (pName == null) {
                            return 257;
                        }
                        this.initStartElement(pName);
                        if (super._inputPtr >= super._inputEnd) {
                            return 257;
                        }
                    }
                    case 2: {
                        Label_0364: {
                            if (super._pendingInput != 0) {
                                if (!this.handlePartialCR()) {
                                    return 257;
                                }
                            }
                            else {
                                final byte value2 = this._inputBuffer.get(super._inputPtr++);
                                final int n = value2 & 0xFF;
                                if (n <= 32) {
                                    if (n != 10) {
                                        if (n == 13) {
                                            final int inputPtr2 = super._inputPtr;
                                            if (inputPtr2 >= super._inputEnd) {
                                                super._pendingInput = -1;
                                                return 257;
                                            }
                                            if (this._inputBuffer.get(inputPtr2) == 10) {
                                                ++super._inputPtr;
                                            }
                                        }
                                        else {
                                            if (n != 32 && n != 9) {
                                                this.throwInvalidSpace(n);
                                            }
                                            break Label_0364;
                                        }
                                    }
                                    this.markLF();
                                }
                                else {
                                    if (n == 62) {
                                        return this.finishStartElement(false);
                                    }
                                    if (n == 47) {
                                        break Label_0343;
                                    }
                                    this.throwUnexpectedChar(this.decodeCharForError(value2), " expected space, or '>' or \"/>\"");
                                }
                            }
                        }
                        super._state = 3;
                        if (super._inputPtr >= super._inputEnd) {
                            return 257;
                        }
                    }
                    case 3:
                    case 5:
                    case 6: {
                        if (super._pendingInput != 0) {
                            if (!this.handlePartialCR()) {
                                return 257;
                            }
                            if (super._inputPtr >= super._inputEnd) {
                                return 257;
                            }
                        }
                        byte b = this._inputBuffer.get(super._inputPtr++);
                        while (true) {
                            final int n2 = b & 0xFF;
                            if (n2 <= 32) {
                                Label_0536: {
                                    if (n2 != 10) {
                                        if (n2 == 13) {
                                            final int inputPtr3 = super._inputPtr;
                                            if (inputPtr3 >= super._inputEnd) {
                                                super._pendingInput = -1;
                                                return 257;
                                            }
                                            if (this._inputBuffer.get(inputPtr3) == 10) {
                                                ++super._inputPtr;
                                            }
                                        }
                                        else {
                                            if (n2 != 32 && n2 != 9) {
                                                this.throwInvalidSpace(n2);
                                            }
                                            break Label_0536;
                                        }
                                    }
                                    this.markLF();
                                }
                                final int inputPtr4 = super._inputPtr;
                                if (inputPtr4 >= super._inputEnd) {
                                    return 257;
                                }
                                final ByteBuffer inputBuffer2 = this._inputBuffer;
                                super._inputPtr = inputPtr4 + 1;
                                b = inputBuffer2.get(inputPtr4);
                            }
                            else {
                                final int state = super._state;
                                if (state != 3) {
                                    if (state == 5) {
                                        if (b != 61) {
                                            this.throwUnexpectedChar(this.decodeCharForError(b), " expected '='");
                                        }
                                        super._state = 6;
                                        continue Label_0000;
                                    }
                                    if (state != 6) {
                                        this.throwInternal();
                                    }
                                    if (b != 34 && b != 39) {
                                        this.throwUnexpectedChar(this.decodeCharForError(b), " Expected a quote");
                                    }
                                    this.initAttribute(b);
                                    continue Label_0000;
                                }
                                else {
                                    if (b == 47) {
                                        break Label_0343;
                                    }
                                    if (b == 62) {
                                        return this.finishStartElement(false);
                                    }
                                    final PName newName = this.parseNewName(b);
                                    if (newName == null) {
                                        super._state = 4;
                                        return 257;
                                    }
                                    super._state = 5;
                                    super._elemAttrName = newName;
                                    continue Label_0000;
                                }
                            }
                        }
                        break;
                    }
                    case 4: {
                        final PName pName2 = this.parsePName();
                        if (pName2 == null) {
                            return 257;
                        }
                        super._elemAttrName = pName2;
                        super._state = 5;
                        continue;
                    }
                }
                super._state = 2;
                continue;
            }
            super._state = 9;
        }
    }
    
    @Override
    public int handleStartElementStart(final byte b) {
        final PName newName = this.parseNewName(b);
        super._nextEvent = 1;
        if (newName == null) {
            super._state = 1;
            return 257;
        }
        this.initStartElement(newName);
        return this.handleStartElement();
    }
    
    @Override
    public final boolean needMoreInput() {
        return super._inputPtr >= super._inputEnd && !super._endOfInput;
    }
    
    @Override
    public int nextFromTree() {
        final int currToken = super._currToken;
        if (currToken != 257) {
            if (currToken == 1) {
                if (super._isEmptyTag) {
                    --super._depth;
                    return super._currToken = 2;
                }
            }
            else if (currToken == 2) {
                super._currElem = super._currElem.getParent();
                while (true) {
                    final NsDeclaration lastNsDecl = super._lastNsDecl;
                    if (lastNsDecl == null || lastNsDecl.getLevel() < super._depth) {
                        break;
                    }
                    super._lastNsDecl = super._lastNsDecl.unbind();
                }
            }
            this.setStartLocation();
            if (super._tokenIncomplete) {
                if (!this.skipCharacters()) {
                    return 257;
                }
                super._tokenIncomplete = false;
            }
            super._nextEvent = 257;
            super._currToken = 257;
            super._state = 0;
        }
        Label_0482: {
            if (super._nextEvent == 257) {
                if (super._state == 0) {
                    if (super._pendingInput != 0) {
                        super._nextEvent = 4;
                        return this.startCharactersPending();
                    }
                    final int inputPtr = super._inputPtr;
                    if (inputPtr >= super._inputEnd) {
                        return super._currToken;
                    }
                    final ByteBuffer inputBuffer = this._inputBuffer;
                    super._inputPtr = inputPtr + 1;
                    final byte value = inputBuffer.get(inputPtr);
                    if (value == 60) {
                        super._state = 1;
                    }
                    else {
                        if (value != 38) {
                            super._nextEvent = 4;
                            return this.startCharacters(value);
                        }
                        super._state = 2;
                    }
                }
                final int inputPtr2 = super._inputPtr;
                if (inputPtr2 >= super._inputEnd) {
                    return super._currToken;
                }
                final int state = super._state;
                if (state == 1) {
                    final ByteBuffer inputBuffer2 = this._inputBuffer;
                    super._inputPtr = inputPtr2 + 1;
                    final byte value2 = inputBuffer2.get(inputPtr2);
                    if (value2 == 33) {
                        super._state = 3;
                    }
                    else {
                        if (value2 == 63) {
                            super._nextEvent = 3;
                            super._state = 0;
                            return this.handlePI();
                        }
                        if (value2 == 47) {
                            return this.handleEndElementStart();
                        }
                        return this.handleStartElementStart(value2);
                    }
                }
                else {
                    if (state == 2) {
                        return this.handleEntityStartingToken();
                    }
                    if (state == 6) {
                        return this.handleNamedEntityStartingToken();
                    }
                    if (state == 5) {
                        return this.handleNumericEntityStartingToken();
                    }
                }
                if (super._state == 3) {
                    final int inputPtr3 = super._inputPtr;
                    if (inputPtr3 >= super._inputEnd) {
                        return super._currToken;
                    }
                    final ByteBuffer inputBuffer3 = this._inputBuffer;
                    super._inputPtr = inputPtr3 + 1;
                    final byte value3 = inputBuffer3.get(inputPtr3);
                    if (value3 == 45) {
                        super._nextEvent = 5;
                    }
                    else {
                        if (value3 != 91) {
                            this.reportTreeUnexpChar(this.decodeCharForError(value3), " (expected either '-' for COMMENT or '[CDATA[' for CDATA section)");
                            break Label_0482;
                        }
                        super._nextEvent = 12;
                    }
                    super._state = 0;
                }
                else {
                    this.throwInternal();
                }
            }
        }
        final int nextEvent = super._nextEvent;
        if (nextEvent == 1) {
            return this.handleStartElement();
        }
        if (nextEvent == 2) {
            return this.handleEndElement();
        }
        if (nextEvent != 3) {
            if (nextEvent != 4) {
                if (nextEvent == 5) {
                    return this.handleComment();
                }
                if (nextEvent == 12) {
                    return this.handleCData();
                }
            }
            else {
                if (!super._cfgLazyParsing && super._cfgCoalescing) {
                    return this.finishCharactersCoalescing();
                }
                if (super._pendingInput != 0) {
                    return this.startCharactersPending();
                }
                this.throwInternal();
            }
            return this.throwInternal();
        }
        return this.handlePI();
    }
    
    public final int parseCDataContents() {
        if (super._pendingInput != 0) {
            final int handleCDataPending = this.handleCDataPending();
            if (handleCDataPending != 0) {
                return handleCDataPending;
            }
        }
        char[] array = super._textBuilder.getBufferWithoutReset();
        int currentLength = super._textBuilder.getCurrentLength();
        final int[] other_CHARS = super._charTypes.OTHER_CHARS;
        final ByteBuffer inputBuffer = this._inputBuffer;
    Label_0690:
        while (true) {
            while (super._inputPtr < super._inputEnd) {
                final int length = array.length;
                final int n = 0;
                final int n2 = 0;
                char[] array2 = array;
                int n3;
                if ((n3 = currentLength) >= length) {
                    array2 = super._textBuilder.finishCurrentSegment();
                    n3 = 0;
                }
                final int inputEnd = super._inputEnd;
                final int n4 = super._inputPtr + (array2.length - n3);
                int n5 = n3;
                int n6;
                if (n4 < (n6 = inputEnd)) {
                    n6 = n4;
                    n5 = n3;
                }
                int n7;
                int n8;
                while (true) {
                    final int inputPtr = super._inputPtr;
                    array = array2;
                    currentLength = n5;
                    if (inputPtr >= n6) {
                        continue Label_0690;
                    }
                    super._inputPtr = inputPtr + 1;
                    n7 = (inputBuffer.get(inputPtr) & 0xFF);
                    n8 = other_CHARS[n7];
                    if (n8 != 0) {
                        break;
                    }
                    array2[n5] = (char)n7;
                    ++n5;
                }
                int pendingInput2 = 0;
                Label_0685: {
                    int n10 = 0;
                    Label_0861: {
                        Label_0620: {
                            if (n8 != 11) {
                                int n9 = n7;
                                int pendingInput = 0;
                                Label_0516: {
                                    int n13 = 0;
                                    switch (n8) {
                                        default: {
                                            array = array2;
                                            n10 = n5;
                                            break Label_0861;
                                        }
                                        case 7: {
                                            final int inputEnd2 = super._inputEnd;
                                            final int inputPtr2 = super._inputPtr;
                                            if (inputEnd2 - inputPtr2 >= 3) {
                                                final int decodeUtf8_4 = this.decodeUtf8_4(n7);
                                                final int n11 = n5 + 1;
                                                array2[n5] = (char)(decodeUtf8_4 >> 10 | 0xD800);
                                                int n12;
                                                if (n11 >= array2.length) {
                                                    array2 = super._textBuilder.finishCurrentSegment();
                                                    n12 = n2;
                                                }
                                                else {
                                                    n12 = n11;
                                                }
                                                n7 = ((decodeUtf8_4 & 0x3FF) | 0xDC00);
                                                array = array2;
                                                n10 = n12;
                                                break Label_0861;
                                            }
                                            pendingInput = n7;
                                            if (inputEnd2 <= inputPtr2) {
                                                break Label_0516;
                                            }
                                            final ByteBuffer inputBuffer2 = this._inputBuffer;
                                            super._inputPtr = inputPtr2 + 1;
                                            n7 |= (inputBuffer2.get(inputPtr2) & 0xFF) << 8;
                                            final int inputEnd3 = super._inputEnd;
                                            final int inputPtr3 = super._inputPtr;
                                            pendingInput = n7;
                                            if (inputEnd3 > inputPtr3) {
                                                final ByteBuffer inputBuffer3 = this._inputBuffer;
                                                super._inputPtr = inputPtr3 + 1;
                                                n13 = (inputBuffer3.get(inputPtr3) & 0xFF) << 16;
                                                break;
                                            }
                                            break Label_0516;
                                        }
                                        case 6: {
                                            final int inputEnd4 = super._inputEnd;
                                            final int inputPtr4 = super._inputPtr;
                                            if (inputEnd4 - inputPtr4 >= 2) {
                                                n7 = this.decodeUtf8_3(n7);
                                                array = array2;
                                                n10 = n5;
                                                break Label_0861;
                                            }
                                            pendingInput = n7;
                                            if (inputEnd4 > inputPtr4) {
                                                final ByteBuffer inputBuffer4 = this._inputBuffer;
                                                super._inputPtr = inputPtr4 + 1;
                                                n13 = (inputBuffer4.get(inputPtr4) & 0xFF) << 8;
                                                break;
                                            }
                                            break Label_0516;
                                        }
                                        case 5: {
                                            if (super._inputPtr >= super._inputEnd) {
                                                pendingInput = n7;
                                                break Label_0516;
                                            }
                                            n7 = this.decodeUtf8_2(n7);
                                            array = array2;
                                            n10 = n5;
                                            break Label_0861;
                                        }
                                        case 4: {
                                            this.reportInvalidInitial(n7);
                                            break Label_0620;
                                        }
                                        case 1: {
                                            this.handleInvalidXmlChar(n7);
                                        }
                                        case 2: {
                                            final int inputPtr5 = super._inputPtr;
                                            if (inputPtr5 >= super._inputEnd) {
                                                pendingInput2 = -1;
                                                break Label_0685;
                                            }
                                            final byte value = inputBuffer.get(inputPtr5);
                                            n9 = 10;
                                            if (value == 10) {
                                                ++super._inputPtr;
                                                n9 = n9;
                                            }
                                        }
                                        case 3: {
                                            this.markLF();
                                            array = array2;
                                            n10 = n5;
                                            n7 = n9;
                                            break Label_0861;
                                        }
                                    }
                                    pendingInput = (n7 | n13);
                                }
                                super._pendingInput = pendingInput;
                                break Label_0690;
                            }
                        }
                        final int inputPtr6 = super._inputPtr;
                        if (inputPtr6 >= super._inputEnd) {
                            pendingInput2 = -30;
                            break Label_0685;
                        }
                        array = array2;
                        n10 = n5;
                        if (this._inputBuffer.get(inputPtr6) == 93) {
                            ++super._inputPtr;
                            while (true) {
                                final int inputPtr7 = super._inputPtr;
                                if (inputPtr7 >= super._inputEnd) {
                                    pendingInput2 = -31;
                                    break Label_0685;
                                }
                                if (this._inputBuffer.get(inputPtr7) == 62) {
                                    ++super._inputPtr;
                                    super._textBuilder.setCurrentLength(n5);
                                    super._state = 0;
                                    super._nextEvent = 257;
                                    return 12;
                                }
                                if (this._inputBuffer.get(super._inputPtr) != 93) {
                                    final int n14 = n5 + 1;
                                    array2[n5] = ']';
                                    int n15;
                                    if (n14 >= array2.length) {
                                        array = super._textBuilder.finishCurrentSegment();
                                        n15 = n;
                                    }
                                    else {
                                        n15 = n14;
                                        array = array2;
                                    }
                                    currentLength = n15 + 1;
                                    array[n15] = ']';
                                    continue Label_0690;
                                }
                                ++super._inputPtr;
                                final int n16 = n5 + 1;
                                array2[n5] = ']';
                                if (n16 >= array2.length) {
                                    array2 = super._textBuilder.finishCurrentSegment();
                                    n5 = 0;
                                }
                                else {
                                    n5 = n16;
                                }
                            }
                        }
                    }
                    array[n10] = (char)n7;
                    currentLength = n10 + 1;
                    continue;
                }
                super._pendingInput = pendingInput2;
                super._textBuilder.setCurrentLength(n5);
                return 257;
            }
            int n5 = currentLength;
            continue Label_0690;
        }
    }
    
    public int parseCommentContents() {
        if (super._pendingInput != 0) {
            final int handleCommentPending = this.handleCommentPending();
            if (handleCommentPending != 0) {
                return handleCommentPending;
            }
        }
        char[] bufferWithoutReset = super._textBuilder.getBufferWithoutReset();
        int currentLength = super._textBuilder.getCurrentLength();
        final int[] other_CHARS = super._charTypes.OTHER_CHARS;
        final ByteBuffer inputBuffer = this._inputBuffer;
    Label_0662:
        while (true) {
            while (super._inputPtr < super._inputEnd) {
                final int length = bufferWithoutReset.length;
                final int n = 0;
                char[] array = bufferWithoutReset;
                int n2;
                if ((n2 = currentLength) >= length) {
                    array = super._textBuilder.finishCurrentSegment();
                    n2 = 0;
                }
                final int inputEnd = super._inputEnd;
                final int n3 = super._inputPtr + (array.length - n2);
                int n4 = n2;
                int n5;
                if (n3 < (n5 = inputEnd)) {
                    n5 = n3;
                    n4 = n2;
                }
                int n6;
                int n7;
                while (true) {
                    final int inputPtr = super._inputPtr;
                    bufferWithoutReset = array;
                    currentLength = n4;
                    if (inputPtr >= n5) {
                        continue Label_0662;
                    }
                    super._inputPtr = inputPtr + 1;
                    n6 = (inputBuffer.get(inputPtr) & 0xFF);
                    n7 = other_CHARS[n6];
                    if (n7 != 0) {
                        break;
                    }
                    array[n4] = (char)n6;
                    ++n4;
                }
                int pendingInput2 = 0;
                Label_0549: {
                    int n9 = 0;
                    Label_0724: {
                        Label_0601: {
                            if (n7 != 13) {
                                int n8 = n6;
                                int pendingInput = 0;
                                Label_0500: {
                                    int n12 = 0;
                                    switch (n7) {
                                        default: {
                                            n9 = n4;
                                            break Label_0724;
                                        }
                                        case 7: {
                                            final int inputEnd2 = super._inputEnd;
                                            final int inputPtr2 = super._inputPtr;
                                            if (inputEnd2 - inputPtr2 >= 3) {
                                                final int decodeUtf8_4 = this.decodeUtf8_4(n6);
                                                final int n10 = n4 + 1;
                                                array[n4] = (char)(decodeUtf8_4 >> 10 | 0xD800);
                                                int n11;
                                                if (n10 >= array.length) {
                                                    array = super._textBuilder.finishCurrentSegment();
                                                    n11 = n;
                                                }
                                                else {
                                                    n11 = n10;
                                                }
                                                n6 = ((decodeUtf8_4 & 0x3FF) | 0xDC00);
                                                n9 = n11;
                                                break Label_0724;
                                            }
                                            pendingInput = n6;
                                            if (inputEnd2 <= inputPtr2) {
                                                break Label_0500;
                                            }
                                            final ByteBuffer inputBuffer2 = this._inputBuffer;
                                            super._inputPtr = inputPtr2 + 1;
                                            n6 |= (inputBuffer2.get(inputPtr2) & 0xFF) << 8;
                                            final int inputEnd3 = super._inputEnd;
                                            final int inputPtr3 = super._inputPtr;
                                            pendingInput = n6;
                                            if (inputEnd3 > inputPtr3) {
                                                final ByteBuffer inputBuffer3 = this._inputBuffer;
                                                super._inputPtr = inputPtr3 + 1;
                                                n12 = (inputBuffer3.get(inputPtr3) & 0xFF) << 16;
                                                break;
                                            }
                                            break Label_0500;
                                        }
                                        case 6: {
                                            final int inputEnd4 = super._inputEnd;
                                            final int inputPtr4 = super._inputPtr;
                                            if (inputEnd4 - inputPtr4 >= 2) {
                                                n6 = this.decodeUtf8_3(n6);
                                                n9 = n4;
                                                break Label_0724;
                                            }
                                            pendingInput = n6;
                                            if (inputEnd4 > inputPtr4) {
                                                final ByteBuffer inputBuffer4 = this._inputBuffer;
                                                super._inputPtr = inputPtr4 + 1;
                                                n12 = (inputBuffer4.get(inputPtr4) & 0xFF) << 8;
                                                break;
                                            }
                                            break Label_0500;
                                        }
                                        case 5: {
                                            if (super._inputPtr >= super._inputEnd) {
                                                pendingInput = n6;
                                                break Label_0500;
                                            }
                                            n6 = this.decodeUtf8_2(n6);
                                            n9 = n4;
                                            break Label_0724;
                                        }
                                        case 4: {
                                            this.reportInvalidInitial(n6);
                                            break Label_0601;
                                        }
                                        case 1: {
                                            this.handleInvalidXmlChar(n6);
                                        }
                                        case 2: {
                                            final int inputPtr5 = super._inputPtr;
                                            if (inputPtr5 >= super._inputEnd) {
                                                pendingInput2 = -1;
                                                break Label_0549;
                                            }
                                            final byte value = inputBuffer.get(inputPtr5);
                                            n8 = 10;
                                            if (value == 10) {
                                                ++super._inputPtr;
                                                n8 = n8;
                                            }
                                        }
                                        case 3: {
                                            this.markLF();
                                            n9 = n4;
                                            n6 = n8;
                                            break Label_0724;
                                        }
                                    }
                                    pendingInput = (n6 | n12);
                                }
                                super._pendingInput = pendingInput;
                                break Label_0662;
                            }
                        }
                        final int inputPtr6 = super._inputPtr;
                        if (inputPtr6 >= super._inputEnd) {
                            pendingInput2 = -20;
                            break Label_0549;
                        }
                        n9 = n4;
                        if (this._inputBuffer.get(inputPtr6) == 45) {
                            final int inputPtr7 = super._inputPtr + 1;
                            if ((super._inputPtr = inputPtr7) >= super._inputEnd) {
                                pendingInput2 = -21;
                                break Label_0549;
                            }
                            final ByteBuffer inputBuffer5 = this._inputBuffer;
                            super._inputPtr = inputPtr7 + 1;
                            if (inputBuffer5.get(inputPtr7) != 62) {
                                this.reportDoubleHyphenInComments();
                            }
                            super._textBuilder.setCurrentLength(n4);
                            super._state = 0;
                            super._nextEvent = 257;
                            return 5;
                        }
                    }
                    array[n9] = (char)n6;
                    currentLength = n9 + 1;
                    bufferWithoutReset = array;
                    continue;
                }
                super._pendingInput = pendingInput2;
                super._textBuilder.setCurrentLength(n4);
                return 257;
            }
            int n4 = currentLength;
            continue Label_0662;
        }
    }
    
    public final PName parseEntityName() {
        int currQuad = super._currQuad;
        while (true) {
            final int currQuadBytes = super._currQuadBytes;
            Label_0509: {
                int currQuad4 = 0;
                Label_0394: {
                    int currQuad3 = 0;
                    Label_0258: {
                        int currQuad2;
                        if (currQuadBytes != 0) {
                            currQuad2 = currQuad;
                            if (currQuadBytes != 1) {
                                currQuad3 = currQuad;
                                if (currQuadBytes == 2) {
                                    break Label_0258;
                                }
                                currQuad4 = currQuad;
                                if (currQuadBytes != 3) {
                                    break Label_0509;
                                }
                                break Label_0394;
                            }
                        }
                        else {
                            final int inputPtr = super._inputPtr;
                            if (inputPtr >= super._inputEnd) {
                                return null;
                            }
                            final ByteBuffer inputBuffer = this._inputBuffer;
                            super._inputPtr = inputPtr + 1;
                            int n = currQuad2 = (inputBuffer.get(inputPtr) & 0xFF);
                            if (n < 65 && (n < 45 || n > 58 || (currQuad2 = n) == 47)) {
                                if (super._quadCount == 1) {
                                    final int n2 = super._quadBuffer[0];
                                    if (n2 == EntityNames.ENTITY_APOS_QUAD) {
                                        --super._inputPtr;
                                        return EntityNames.ENTITY_APOS;
                                    }
                                    if ((n = n2) == EntityNames.ENTITY_QUOT_QUAD) {
                                        --super._inputPtr;
                                        return EntityNames.ENTITY_QUOT;
                                    }
                                }
                                return this.findPName(n, 0);
                            }
                        }
                        final int inputPtr2 = super._inputPtr;
                        if (inputPtr2 >= super._inputEnd) {
                            super._currQuad = currQuad2;
                            super._currQuadBytes = 1;
                            return null;
                        }
                        final ByteBuffer inputBuffer2 = this._inputBuffer;
                        super._inputPtr = inputPtr2 + 1;
                        final int n3 = inputBuffer2.get(inputPtr2) & 0xFF;
                        if (n3 < 65 && (n3 < 45 || n3 > 58 || n3 == 47)) {
                            return this.findPName(currQuad2, 1);
                        }
                        currQuad3 = (currQuad2 << 8 | n3);
                    }
                    final int inputPtr3 = super._inputPtr;
                    if (inputPtr3 >= super._inputEnd) {
                        super._currQuad = currQuad3;
                        super._currQuadBytes = 2;
                        return null;
                    }
                    final ByteBuffer inputBuffer3 = this._inputBuffer;
                    super._inputPtr = inputPtr3 + 1;
                    final int n4 = inputBuffer3.get(inputPtr3) & 0xFF;
                    if (n4 < 65 && (n4 < 45 || n4 > 58 || n4 == 47)) {
                        if (super._quadCount == 0) {
                            if (currQuad3 == EntityNames.ENTITY_GT_QUAD) {
                                --super._inputPtr;
                                return EntityNames.ENTITY_GT;
                            }
                            if (currQuad3 == EntityNames.ENTITY_LT_QUAD) {
                                --super._inputPtr;
                                return EntityNames.ENTITY_LT;
                            }
                        }
                        return this.findPName(currQuad3, 2);
                    }
                    currQuad4 = (currQuad3 << 8 | n4);
                }
                final int inputPtr4 = super._inputPtr;
                if (inputPtr4 >= super._inputEnd) {
                    super._currQuad = currQuad4;
                    super._currQuadBytes = 3;
                    return null;
                }
                final ByteBuffer inputBuffer4 = this._inputBuffer;
                super._inputPtr = inputPtr4 + 1;
                final int n5 = inputBuffer4.get(inputPtr4) & 0xFF;
                if (n5 < 65 && (n5 < 45 || n5 > 58 || n5 == 47)) {
                    if (super._quadCount == 0 && currQuad4 == EntityNames.ENTITY_AMP_QUAD) {
                        --super._inputPtr;
                        return EntityNames.ENTITY_AMP;
                    }
                    return this.findPName(currQuad4, 3);
                }
                else {
                    currQuad = (currQuad4 << 8 | n5);
                }
            }
            final int quadCount = super._quadCount;
            if (quadCount == 0) {
                super._quadBuffer[0] = currQuad;
                super._quadCount = 1;
            }
            else {
                final int[] quadBuffer = super._quadBuffer;
                if (quadCount >= quadBuffer.length) {
                    super._quadBuffer = DataUtil.growArrayBy(quadBuffer, quadBuffer.length);
                }
                super._quadBuffer[super._quadCount++] = currQuad;
            }
            super._currQuadBytes = 0;
        }
    }
    
    public final PName parseNewEntityName(final byte b) {
        final int currQuad = b & 0xFF;
        if (currQuad < 65) {
            this.throwUnexpectedChar(currQuad, "; expected a name start character");
        }
        super._quadCount = 0;
        super._currQuad = currQuad;
        super._currQuadBytes = 1;
        return this.parseEntityName();
    }
    
    @Override
    public final PName parseNewName(final byte b) {
        final int currQuad = b & 0xFF;
        if (currQuad < 65) {
            this.throwUnexpectedChar(currQuad, "; expected a name start character");
        }
        super._quadCount = 0;
        super._currQuad = currQuad;
        super._currQuadBytes = 1;
        return this.parsePName();
    }
    
    public int parsePIData() {
        if (super._pendingInput != 0) {
            final int handlePIPending = this.handlePIPending();
            if (handlePIPending != 0) {
                return handlePIPending;
            }
        }
        char[] bufferWithoutReset = super._textBuilder.getBufferWithoutReset();
        int currentLength = super._textBuilder.getCurrentLength();
        final int[] other_CHARS = super._charTypes.OTHER_CHARS;
        final ByteBuffer inputBuffer = this._inputBuffer;
    Label_0620:
        while (true) {
            while (super._inputPtr < super._inputEnd) {
                final int length = bufferWithoutReset.length;
                final int n = 0;
                char[] array = bufferWithoutReset;
                int n2;
                if ((n2 = currentLength) >= length) {
                    array = super._textBuilder.finishCurrentSegment();
                    n2 = 0;
                }
                final int inputEnd = super._inputEnd;
                final int n3 = super._inputPtr + (array.length - n2);
                int n4 = n2;
                int n5;
                if (n3 < (n5 = inputEnd)) {
                    n5 = n3;
                    n4 = n2;
                }
                int n6;
                int n7;
                while (true) {
                    final int inputPtr = super._inputPtr;
                    bufferWithoutReset = array;
                    currentLength = n4;
                    if (inputPtr >= n5) {
                        continue Label_0620;
                    }
                    super._inputPtr = inputPtr + 1;
                    n6 = (inputBuffer.get(inputPtr) & 0xFF);
                    n7 = other_CHARS[n6];
                    if (n7 != 0) {
                        break;
                    }
                    array[n4] = (char)n6;
                    ++n4;
                }
                int pendingInput2 = 0;
                Label_0549: {
                    int n9 = 0;
                    Label_0680: {
                        Label_0601: {
                            if (n7 != 12) {
                                int n8 = n6;
                                int pendingInput = 0;
                                Label_0500: {
                                    int n12 = 0;
                                    switch (n7) {
                                        default: {
                                            n9 = n4;
                                            break Label_0680;
                                        }
                                        case 7: {
                                            final int inputEnd2 = super._inputEnd;
                                            final int inputPtr2 = super._inputPtr;
                                            if (inputEnd2 - inputPtr2 >= 3) {
                                                final int decodeUtf8_4 = this.decodeUtf8_4(n6);
                                                final int n10 = n4 + 1;
                                                array[n4] = (char)(decodeUtf8_4 >> 10 | 0xD800);
                                                int n11;
                                                if (n10 >= array.length) {
                                                    array = super._textBuilder.finishCurrentSegment();
                                                    n11 = n;
                                                }
                                                else {
                                                    n11 = n10;
                                                }
                                                n6 = ((decodeUtf8_4 & 0x3FF) | 0xDC00);
                                                n9 = n11;
                                                break Label_0680;
                                            }
                                            pendingInput = n6;
                                            if (inputEnd2 <= inputPtr2) {
                                                break Label_0500;
                                            }
                                            final ByteBuffer inputBuffer2 = this._inputBuffer;
                                            super._inputPtr = inputPtr2 + 1;
                                            n6 |= (inputBuffer2.get(inputPtr2) & 0xFF) << 8;
                                            final int inputEnd3 = super._inputEnd;
                                            final int inputPtr3 = super._inputPtr;
                                            pendingInput = n6;
                                            if (inputEnd3 > inputPtr3) {
                                                final ByteBuffer inputBuffer3 = this._inputBuffer;
                                                super._inputPtr = inputPtr3 + 1;
                                                n12 = (inputBuffer3.get(inputPtr3) & 0xFF) << 16;
                                                break;
                                            }
                                            break Label_0500;
                                        }
                                        case 6: {
                                            final int inputEnd4 = super._inputEnd;
                                            final int inputPtr4 = super._inputPtr;
                                            if (inputEnd4 - inputPtr4 >= 2) {
                                                n6 = this.decodeUtf8_3(n6);
                                                n9 = n4;
                                                break Label_0680;
                                            }
                                            pendingInput = n6;
                                            if (inputEnd4 > inputPtr4) {
                                                final ByteBuffer inputBuffer4 = this._inputBuffer;
                                                super._inputPtr = inputPtr4 + 1;
                                                n12 = (inputBuffer4.get(inputPtr4) & 0xFF) << 8;
                                                break;
                                            }
                                            break Label_0500;
                                        }
                                        case 5: {
                                            if (super._inputPtr >= super._inputEnd) {
                                                pendingInput = n6;
                                                break Label_0500;
                                            }
                                            n6 = this.decodeUtf8_2(n6);
                                            n9 = n4;
                                            break Label_0680;
                                        }
                                        case 4: {
                                            this.reportInvalidInitial(n6);
                                            break Label_0601;
                                        }
                                        case 1: {
                                            this.handleInvalidXmlChar(n6);
                                        }
                                        case 2: {
                                            final int inputPtr5 = super._inputPtr;
                                            if (inputPtr5 >= super._inputEnd) {
                                                pendingInput2 = -1;
                                                break Label_0549;
                                            }
                                            final byte value = inputBuffer.get(inputPtr5);
                                            n8 = 10;
                                            if (value == 10) {
                                                ++super._inputPtr;
                                                n8 = n8;
                                            }
                                        }
                                        case 3: {
                                            this.markLF();
                                            n9 = n4;
                                            n6 = n8;
                                            break Label_0680;
                                        }
                                    }
                                    pendingInput = (n6 | n12);
                                }
                                super._pendingInput = pendingInput;
                                break Label_0620;
                            }
                        }
                        final int inputPtr6 = super._inputPtr;
                        if (inputPtr6 >= super._inputEnd) {
                            pendingInput2 = -15;
                            break Label_0549;
                        }
                        n9 = n4;
                        if (this._inputBuffer.get(inputPtr6) == 62) {
                            ++super._inputPtr;
                            super._textBuilder.setCurrentLength(n4);
                            super._state = 0;
                            super._nextEvent = 257;
                            return 3;
                        }
                    }
                    array[n9] = (char)n6;
                    currentLength = n9 + 1;
                    bufferWithoutReset = array;
                    continue;
                }
                super._pendingInput = pendingInput2;
                super._textBuilder.setCurrentLength(n4);
                return 257;
            }
            int n4 = currentLength;
            continue Label_0620;
        }
    }
    
    @Override
    public final PName parsePName() {
        int currQuad = super._currQuad;
        while (true) {
            final int currQuadBytes = super._currQuadBytes;
            Label_0373: {
                int currQuad4 = 0;
                Label_0286: {
                    int currQuad3 = 0;
                    Label_0199: {
                        int currQuad2;
                        if (currQuadBytes != 0) {
                            currQuad2 = currQuad;
                            if (currQuadBytes != 1) {
                                currQuad3 = currQuad;
                                if (currQuadBytes == 2) {
                                    break Label_0199;
                                }
                                currQuad4 = currQuad;
                                if (currQuadBytes != 3) {
                                    break Label_0373;
                                }
                                break Label_0286;
                            }
                        }
                        else {
                            final int inputPtr = super._inputPtr;
                            if (inputPtr >= super._inputEnd) {
                                return null;
                            }
                            final ByteBuffer inputBuffer = this._inputBuffer;
                            super._inputPtr = inputPtr + 1;
                            final int n = currQuad2 = (inputBuffer.get(inputPtr) & 0xFF);
                            if (n < 65 && (n < 45 || n > 58 || (currQuad2 = n) == 47)) {
                                return this.findPName(n, 0);
                            }
                        }
                        final int inputPtr2 = super._inputPtr;
                        if (inputPtr2 >= super._inputEnd) {
                            super._currQuad = currQuad2;
                            super._currQuadBytes = 1;
                            return null;
                        }
                        final ByteBuffer inputBuffer2 = this._inputBuffer;
                        super._inputPtr = inputPtr2 + 1;
                        final int n2 = inputBuffer2.get(inputPtr2) & 0xFF;
                        if (n2 < 65 && (n2 < 45 || n2 > 58 || n2 == 47)) {
                            return this.findPName(currQuad2, 1);
                        }
                        currQuad3 = (currQuad2 << 8 | n2);
                    }
                    final int inputPtr3 = super._inputPtr;
                    if (inputPtr3 >= super._inputEnd) {
                        super._currQuad = currQuad3;
                        super._currQuadBytes = 2;
                        return null;
                    }
                    final ByteBuffer inputBuffer3 = this._inputBuffer;
                    super._inputPtr = inputPtr3 + 1;
                    final int n3 = inputBuffer3.get(inputPtr3) & 0xFF;
                    if (n3 < 65 && (n3 < 45 || n3 > 58 || n3 == 47)) {
                        return this.findPName(currQuad3, 2);
                    }
                    currQuad4 = (currQuad3 << 8 | n3);
                }
                final int inputPtr4 = super._inputPtr;
                if (inputPtr4 >= super._inputEnd) {
                    super._currQuad = currQuad4;
                    super._currQuadBytes = 3;
                    return null;
                }
                final ByteBuffer inputBuffer4 = this._inputBuffer;
                super._inputPtr = inputPtr4 + 1;
                final int n4 = inputBuffer4.get(inputPtr4) & 0xFF;
                if (n4 < 65 && (n4 < 45 || n4 > 58 || n4 == 47)) {
                    return this.findPName(currQuad4, 3);
                }
                currQuad = (currQuad4 << 8 | n4);
            }
            final int quadCount = super._quadCount;
            if (quadCount == 0) {
                super._quadBuffer[0] = currQuad;
                super._quadCount = 1;
            }
            else {
                final int[] quadBuffer = super._quadBuffer;
                if (quadCount >= quadBuffer.length) {
                    super._quadBuffer = DataUtil.growArrayBy(quadBuffer, quadBuffer.length);
                }
                super._quadBuffer[super._quadCount++] = currQuad;
            }
            super._currQuadBytes = 0;
        }
    }
    
    @Override
    public boolean skipCharacters() {
        if (super._pendingInput != 0 && !this.skipPending()) {
            return false;
        }
        final int[] text_CHARS = super._charTypes.TEXT_CHARS;
        final ByteBuffer inputBuffer = this._inputBuffer;
        Block_19: {
        Label_0458:
            while (true) {
                final int inputPtr = super._inputPtr;
                final int inputEnd = super._inputEnd;
                int i;
                if ((i = inputPtr) >= inputEnd) {
                    return false;
                }
                while (i < inputEnd) {
                    final int inputPtr2 = i + 1;
                    int n = inputBuffer.get(i) & 0xFF;
                    final int n2 = text_CHARS[n];
                    if (n2 != 0) {
                        super._inputPtr = inputPtr2;
                        int pendingInput = 0;
                        Label_0437: {
                            int n4 = 0;
                            switch (n2) {
                                default: {
                                    continue Block_19;
                                }
                                case 11: {
                                    byte value = 0;
                                    int n3 = 1;
                                    while (true) {
                                        final int inputPtr3 = super._inputPtr;
                                        if (inputPtr3 >= super._inputEnd) {
                                            break;
                                        }
                                        value = inputBuffer.get(inputPtr3);
                                        if (value != 93) {
                                            break;
                                        }
                                        ++super._inputPtr;
                                        ++n3;
                                    }
                                    if (value == 62 && n3 > 1) {
                                        this.reportIllegalCDataEnd();
                                        continue Block_19;
                                    }
                                    continue Block_19;
                                }
                                case 10: {
                                    if (this.skipEntityInCharacters() == 0) {
                                        super._pendingInput = -80;
                                        return super._inputPtr < super._inputEnd && this.skipPending();
                                    }
                                    continue Block_19;
                                }
                                case 7: {
                                    final int inputEnd2 = super._inputEnd;
                                    if (inputEnd2 - inputPtr2 >= 3) {
                                        this.decodeUtf8_4(n);
                                        continue Block_19;
                                    }
                                    pendingInput = n;
                                    if (inputEnd2 <= inputPtr2) {
                                        break Label_0437;
                                    }
                                    final ByteBuffer inputBuffer2 = this._inputBuffer;
                                    super._inputPtr = inputPtr2 + 1;
                                    n |= (inputBuffer2.get(inputPtr2) & 0xFF) << 8;
                                    final int inputEnd3 = super._inputEnd;
                                    final int inputPtr4 = super._inputPtr;
                                    pendingInput = n;
                                    if (inputEnd3 > inputPtr4) {
                                        final ByteBuffer inputBuffer3 = this._inputBuffer;
                                        super._inputPtr = inputPtr4 + 1;
                                        n4 = (inputBuffer3.get(inputPtr4) & 0xFF) << 16;
                                        break;
                                    }
                                    break Label_0437;
                                }
                                case 6: {
                                    final int inputEnd4 = super._inputEnd;
                                    if (inputEnd4 - inputPtr2 >= 2) {
                                        this.decodeUtf8_3(n);
                                        continue Block_19;
                                    }
                                    pendingInput = n;
                                    if (inputEnd4 > inputPtr2) {
                                        final ByteBuffer inputBuffer4 = this._inputBuffer;
                                        super._inputPtr = inputPtr2 + 1;
                                        n4 = (inputBuffer4.get(inputPtr2) & 0xFF) << 8;
                                        break;
                                    }
                                    break Label_0437;
                                }
                                case 5: {
                                    if (inputPtr2 >= super._inputEnd) {
                                        pendingInput = n;
                                        break Label_0437;
                                    }
                                    this.skipUtf8_2(n);
                                    continue Block_19;
                                }
                                case 4: {
                                    this.reportInvalidInitial(n);
                                }
                                case 9: {
                                    break Label_0458;
                                }
                                case 2: {
                                    final int inputPtr5 = super._inputPtr;
                                    if (inputPtr5 >= super._inputEnd) {
                                        break Block_19;
                                    }
                                    if (inputBuffer.get(inputPtr5) == 10) {
                                        ++super._inputPtr;
                                    }
                                }
                                case 3: {
                                    this.markLF();
                                    continue Block_19;
                                }
                                case 1: {
                                    this.handleInvalidXmlChar(n);
                                }
                            }
                            pendingInput = (n | n4);
                        }
                        super._pendingInput = pendingInput;
                        return false;
                    }
                    i = inputPtr2;
                }
                super._inputPtr = i;
            }
            --super._inputPtr;
            return true;
        }
        super._pendingInput = -1;
        return false;
    }
    
    @Override
    public boolean skipCoalescedText() {
        this.throwInternal();
        return false;
    }
    
    public final void skipUtf8_2(int value) {
        final ByteBuffer inputBuffer = this._inputBuffer;
        value = super._inputPtr++;
        value = inputBuffer.get(value);
        if ((value & 0xC0) != 0x80) {
            this.reportInvalidOther(value & 0xFF, super._inputPtr);
        }
    }
    
    @Override
    public final int startCharacters(final byte b) {
        int pendingInput = b & 0xFF;
        Label_0394: {
            switch (super._charTypes.TEXT_CHARS[pendingInput]) {
                case 9:
                case 10: {
                    this.throwInternal();
                    break;
                }
                case 7: {
                    final int inputEnd = super._inputEnd;
                    final int inputPtr = super._inputPtr;
                    if (inputEnd - inputPtr < 3) {
                        int pendingInput2 = pendingInput;
                        if (inputEnd > inputPtr) {
                            final ByteBuffer inputBuffer = this._inputBuffer;
                            super._inputPtr = inputPtr + 1;
                            final int n = pendingInput | (inputBuffer.get(inputPtr) & 0xFF) << 8;
                            final int inputEnd2 = super._inputEnd;
                            final int inputPtr2 = super._inputPtr;
                            pendingInput2 = n;
                            if (inputEnd2 > inputPtr2) {
                                final ByteBuffer inputBuffer2 = this._inputBuffer;
                                super._inputPtr = inputPtr2 + 1;
                                pendingInput2 = (n | (inputBuffer2.get(inputPtr2) & 0xFF) << 16);
                            }
                        }
                        super._pendingInput = pendingInput2;
                        return 257;
                    }
                    super._textBuilder.resetWithSurrogate(this.decodeUtf8_4(pendingInput));
                    break Label_0394;
                }
                case 6: {
                    final int inputEnd3 = super._inputEnd;
                    final int inputPtr3 = super._inputPtr;
                    if (inputEnd3 - inputPtr3 < 2) {
                        int pendingInput3 = pendingInput;
                        if (inputEnd3 > inputPtr3) {
                            final ByteBuffer inputBuffer3 = this._inputBuffer;
                            super._inputPtr = inputPtr3 + 1;
                            pendingInput3 = (pendingInput | (inputBuffer3.get(inputPtr3) & 0xFF) << 8);
                        }
                        super._pendingInput = pendingInput3;
                        return 257;
                    }
                    pendingInput = this.decodeUtf8_3(pendingInput);
                    break;
                }
                case 5: {
                    if (super._inputPtr >= super._inputEnd) {
                        super._pendingInput = pendingInput;
                        return 257;
                    }
                    pendingInput = this.decodeUtf8_2(pendingInput);
                    break;
                }
                case 4: {
                    this.reportInvalidInitial(pendingInput);
                    break;
                }
                case 3: {
                    this.markLF();
                    break;
                }
                case 1: {
                    this.handleInvalidXmlChar(pendingInput);
                }
                case 2: {
                    final int inputPtr4 = super._inputPtr;
                    if (inputPtr4 >= super._inputEnd) {
                        super._pendingInput = -1;
                        return 257;
                    }
                    if (this._inputBuffer.get(inputPtr4) == 10) {
                        ++super._inputPtr;
                    }
                    this.markLF();
                    pendingInput = 10;
                    break;
                }
            }
            super._textBuilder.resetWithChar((char)pendingInput);
        }
        if (super._cfgCoalescing && !super._cfgLazyParsing) {
            return this.finishCharactersCoalescing();
        }
        super._currToken = 4;
        if (super._cfgLazyParsing) {
            super._tokenIncomplete = true;
        }
        else {
            this.finishCharacters();
        }
        return super._currToken;
    }
    
    public int startCharactersPending() {
        final int inputPtr = super._inputPtr;
        if (inputPtr >= super._inputEnd) {
            return 257;
        }
        final int pendingInput = super._pendingInput;
        super._pendingInput = 0;
        Label_0523: {
            char c;
            TextBuilder textBuilder;
            if (pendingInput == -1) {
                final byte value = this._inputBuffer.get(inputPtr);
                c = '\n';
                if (value == 10) {
                    ++super._inputPtr;
                }
                this.markLF();
                textBuilder = super._textBuilder;
            }
            else {
                final int[] text_CHARS = super._charTypes.TEXT_CHARS;
                final int n = pendingInput & 0xFF;
                final int n2 = text_CHARS[n];
                if (n2 != 5) {
                    int pendingInput2 = 0;
                    Label_0183: {
                        int n11 = 0;
                        Label_0179: {
                            int n3 = 0;
                            Label_0174: {
                                if (n2 == 6) {
                                    final ByteBuffer inputBuffer = this._inputBuffer;
                                    super._inputPtr = inputPtr + 1;
                                    n3 = (inputBuffer.get(inputPtr) & 0xFF);
                                    final int n4 = pendingInput >> 8;
                                    int n5;
                                    if (n4 == 0) {
                                        final int inputPtr2 = super._inputPtr;
                                        if (inputPtr2 >= super._inputEnd) {
                                            break Label_0174;
                                        }
                                        final ByteBuffer inputBuffer2 = this._inputBuffer;
                                        super._inputPtr = inputPtr2 + 1;
                                        n5 = this.decodeUtf8_3(pendingInput, n3, inputBuffer2.get(inputPtr2) & 0xFF);
                                    }
                                    else {
                                        n5 = this.decodeUtf8_3(n, n4, n3);
                                    }
                                    super._textBuilder.resetWithChar((char)n5);
                                    break Label_0523;
                                }
                                if (n2 != 7) {
                                    this.throwInternal();
                                    break Label_0523;
                                }
                                final ByteBuffer inputBuffer3 = this._inputBuffer;
                                super._inputPtr = inputPtr + 1;
                                n3 = (inputBuffer3.get(inputPtr) & 0xFF);
                                final int n6 = pendingInput >> 8;
                                int n8;
                                if (n6 == 0) {
                                    final int inputPtr3 = super._inputPtr;
                                    if (inputPtr3 >= super._inputEnd) {
                                        break Label_0174;
                                    }
                                    final ByteBuffer inputBuffer4 = this._inputBuffer;
                                    super._inputPtr = inputPtr3 + 1;
                                    final int n7 = inputBuffer4.get(inputPtr3) & 0xFF;
                                    final int inputPtr4 = super._inputPtr;
                                    if (inputPtr4 >= super._inputEnd) {
                                        pendingInput2 = (n3 << 8 | pendingInput | n7 << 16);
                                        break Label_0183;
                                    }
                                    final ByteBuffer inputBuffer5 = this._inputBuffer;
                                    super._inputPtr = inputPtr4 + 1;
                                    n8 = this.decodeUtf8_4(pendingInput, n3, n7, inputBuffer5.get(inputPtr4) & 0xFF);
                                }
                                else {
                                    final int n9 = n6 & 0xFF;
                                    final int n10 = pendingInput >> 16;
                                    if (n10 == 0) {
                                        final int inputPtr5 = super._inputPtr;
                                        if (inputPtr5 >= super._inputEnd) {
                                            n11 = n3 << 16;
                                            break Label_0179;
                                        }
                                        final ByteBuffer inputBuffer6 = this._inputBuffer;
                                        super._inputPtr = inputPtr5 + 1;
                                        n8 = this.decodeUtf8_4(n, n9, n3, inputBuffer6.get(inputPtr5) & 0xFF);
                                    }
                                    else {
                                        n8 = this.decodeUtf8_4(n, n9, n10, n3);
                                    }
                                }
                                super._textBuilder.resetWithSurrogate(n8);
                                return super._currToken = 4;
                            }
                            n11 = n3 << 8;
                        }
                        pendingInput2 = (n11 | pendingInput);
                    }
                    super._pendingInput = pendingInput2;
                    return 257;
                }
                textBuilder = super._textBuilder;
                c = (char)this.decodeUtf8_2(pendingInput);
            }
            textBuilder.resetWithChar(c);
        }
        if (super._cfgCoalescing && !super._cfgLazyParsing) {
            return this.finishCharactersCoalescing();
        }
        super._currToken = 4;
        if (super._cfgLazyParsing) {
            super._tokenIncomplete = true;
        }
        else {
            this.finishCharacters();
        }
        return super._currToken;
    }
    
    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append("asyncScanner; curr=");
        sb.append(super._currToken);
        sb.append(" next=");
        sb.append(super._nextEvent);
        sb.append(", state = ");
        sb.append(super._state);
        return sb.toString();
    }
}

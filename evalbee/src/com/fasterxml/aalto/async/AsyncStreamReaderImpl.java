// 
// Decompiled by Procyon v0.6.0
// 

package com.fasterxml.aalto.async;

import com.fasterxml.aalto.in.XmlScanner;
import com.fasterxml.aalto.AsyncXMLStreamReader;
import com.fasterxml.aalto.stax.StreamReaderImpl;
import com.fasterxml.aalto.AsyncInputFeeder;

public class AsyncStreamReaderImpl<F extends AsyncInputFeeder> extends StreamReaderImpl implements AsyncXMLStreamReader<F>
{
    protected final AsyncByteScanner _asyncScanner;
    
    public AsyncStreamReaderImpl(final AsyncByteScanner asyncScanner) {
        super(asyncScanner);
        this._asyncScanner = asyncScanner;
        super._currToken = 257;
    }
    
    @Override
    public void _reportNonTextEvent(final int n) {
        if (n == 257) {
            this.throwWfe("Can not use text-aggregating methods with non-blocking parser, as they (may) require blocking");
        }
        super._reportNonTextEvent(n);
    }
    
    @Override
    public F getInputFeeder() {
        return (F)this._asyncScanner;
    }
}
